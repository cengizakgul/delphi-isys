inherited EDIT_EE_BENEFITS: TEDIT_EE_BENEFITS
  Width = 1043
  Height = 882
  inherited Panel1: TevPanel
    Width = 1043
    inherited pnlFavoriteReport: TevPanel
      Left = 891
    end
    inherited pnlTopLeft: TevPanel
      Width = 891
    end
  end
  inherited PageControl1: TevPageControl
    Width = 1043
    Height = 795
    ActivePage = TabSheet1
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 1035
        Height = 766
        inherited pnlBorder: TevPanel
          Width = 1031
          Height = 762
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 1031
          Height = 762
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Top = 111
              BevelOuter = bvNone
              ParentColor = True
            end
            inherited pnlSubbrowse: TevPanel
              Left = 383
              Top = 164
            end
          end
          inherited Panel3: TevPanel
            Width = 983
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 983
            Height = 655
            inherited Splitter1: TevSplitter
              Height = 651
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 651
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 571
                IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 661
              Height = 651
              inherited wwDBGrid4: TevDBGrid
                Width = 611
                Height = 571
                IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        Width = 1035
        Height = 607
        VertScrollBar.Position = 105
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Top = -105
          Width = 1104
          Height = 392
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 1088
            Height = 376
            Title = 'Benefit Summary'
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Width = 558
              Height = 219
              inherited evDBGrid1: TevDBGrid
                Width = 558
                Height = 219
                ControlType.Strings = (
                  'DEDUCTION_FREQUENCY;CustomEdit;evDBComboBox12;F'
                  'ENROLLMENT_STATUS;CustomEdit;evDBComboBox13;F')
                Selected.Strings = (
                  'BenefitReference'#9'21'#9'Benefit Reference'#9'F'
                  'BenefitType'#9'20'#9'Benefit Amount Type'#9'F'
                  'CUSTOM_E_D_CODE_NUMBER'#9'8'#9'E/D Code'#9'F'
                  'DESCRIPTION'#9'20'#9'E/D Description'#9'F'
                  'AMOUNT'#9'10'#9'Amount'#9#9
                  'eAMOUNT'#9'10'#9'EE Amount'#9'F'
                  'rAMOUNT'#9'10'#9'ER Amount'#9'F'
                  'EE_PERCENTAGE'#9'10'#9'EE %'#9'F'
                  'ER_PERCENTAGE'#9'10'#9'ER %'#9'F'
                  'CALCULATION_TYPE_Desc'#9'20'#9'Calc. Type'#9'F'
                  'ReferenceType'#9'8'#9'Reference'#9'F'
                  'AmountType'#9'12'#9'Amount Type'#9'F'
                  'EFFECTIVE_START_DATE'#9'18'#9'E/D Eff. Start Date'#9'F'
                  'EFFECTIVE_END_DATE'#9'18'#9'E/D Eff. End Date'#9'F')
                IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\evDBGrid1'
                DataSource = dsBenefitAll
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgShowFooter, dgFooter3DCells, dgTrailingEllipsis, dgDblClickColSizing]
              end
            end
          end
        end
        object evPnlDB: TevPanel
          Left = 0
          Top = 287
          Width = 1113
          Height = 335
          BevelOuter = bvNone
          TabOrder = 1
          object fpBenefits: TisUIFashionPanel
            Left = 8
            Top = 0
            Width = 416
            Height = 327
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpBenefits'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Benefits'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evPnlEdits: TevPanel
              Left = 12
              Top = 35
              Width = 393
              Height = 272
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object evLabel1: TevLabel
                Left = 0
                Top = 0
                Width = 95
                Height = 16
                Caption = '~Benefit Reference'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel17: TevLabel
                Left = 0
                Top = 39
                Width = 108
                Height = 16
                Caption = '~Benefit Amount Type'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel2: TevLabel
                Left = 134
                Top = 0
                Width = 93
                Height = 13
                Caption = 'Effective Start Date'
              end
              object evLabel3: TevLabel
                Left = 266
                Top = 0
                Width = 90
                Height = 13
                Caption = 'Effective End Date'
              end
              object lablEnrollment_Status: TevLabel
                Left = 0
                Top = 78
                Width = 91
                Height = 16
                Caption = '~Enrollment Status'
                FocusControl = wwcbEnrollment_Status
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evEditCoBenefitAddInfo1: TevLabel
                Left = 0
                Top = 156
                Width = 55
                Height = 13
                Caption = 'Additional 1'
              end
              object evEditCoBenefitAddInfo2: TevLabel
                Left = 0
                Top = 195
                Width = 55
                Height = 13
                Caption = 'Additional 2'
              end
              object evEditCoBenefitAddInfo3: TevLabel
                Left = 0
                Top = 234
                Width = 55
                Height = 13
                Caption = 'Additional 3'
              end
              object evLabel38: TevLabel
                Left = 0
                Top = 117
                Width = 110
                Height = 13
                Caption = 'Primary Care Physician '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object lablDeduction_Frequency: TevLabel
                Left = 134
                Top = 39
                Width = 160
                Height = 16
                Caption = '~Payment/Deduction Frequency '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object btnSelectBenefitAmountType: TevSpeedButton
                Left = 103
                Top = 54
                Width = 18
                Height = 21
                Hint = 'Select Benefit Amount Type'
                HideHint = True
                ParentShowHint = False
                ShowHint = True
                AutoSize = False
                OnClick = btnSelectBenefitAmountTypeClick
                NumGlyphs = 2
                Glyph.Data = {
                  36060000424D3606000000000000360000002800000020000000100000000100
                  18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCC
                  CCCCCCCCE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFE2E2E2CDCCCCCDCCCCCDCCCCE2E2E2FFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E153A882008C4C008B
                  4A008C4C53A882E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFE2E2E29E9E9E7E7E7D7C7B7B7E7E7D9E9E9EE2E2E2FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF54A88100995B00BB8677E0
                  C600BB8600995C53A882FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFF9D9D9D8A8A8AACACACD7D6D6ACACAC8A8A8A9E9E9EDCDCDCCCCCCC
                  CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00894600C18D00BC83FFFF
                  FF00BC8300C18D008C4CDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
                  CCCCCDCCCC7A7979B2B2B2ACACACFFFFFFACACACB2B2B27E7E7D8CBAA36DB08F
                  6EB18F6FB18F6EB18F6EB08E6EB18E70B1907AB49500864175E6CDFFFFFFFFFF
                  FFFFFFFF77E7CE008A49B4B4B4A7A7A7A8A8A7A8A8A7A8A8A7A7A7A7A8A8A7A8
                  A8A7ABABAB777676DDDDDDFFFFFFFFFFFFFFFFFFDEDEDE7B7B7B6AAD8BCBF4DB
                  8CC7B583BEA793CBBB92CAB992CBBA95CCBCA0CFC200854000CB9700C990FFFF
                  FF00C99000CC98008A49A3A3A3EEEEEEC1C1C1B8B7B7C6C6C6C4C4C4C5C5C5C7
                  C7C7CBCBCB767675BCBCBBB9B9B9FFFFFFB9B9B9BDBCBC7B7B7B65A986CAF3DC
                  5FA27C4E9368B8EBCFB7E9CD98D1B288C4A391C8A92C9B67009F6000D39D74EE
                  D400D39C00A061309B68A09F9FEDEDED989898888888E4E3E3E2E2E2C9C8C8BB
                  BBBBC0C0C08E8E8E909090C4C3C3E4E4E4C4C3C39191918E8E8E62A581CDF2E0
                  73B593B3E7CDB0E4CAB0E4CA6BAC89B3E4C7B6E5C97AB2924AB28200843F0083
                  3E00833E56B78A72AB8A9B9B9BEDEDEDABABABE0E0E0DDDDDDDDDDDDA2A2A2DC
                  DCDCDEDEDEAAA9A9A5A5A5757474747373747373AAAAAAA2A2A25FA37ED0F2E4
                  6CAD8BABE2C851946DABE2C86DAC88B0E2C6B1E2C671AE8AB5E6CF609B76BAE8
                  D179B292DAF6E963A580999999EFEEEEA4A4A4DBDADA8A8A8ADBDADAA2A2A2DB
                  DADADBDADAA4A4A4E0DFDF929292E2E2E2AAA9A9F3F3F39B9B9B5CA07AD8F5E8
                  67AA85A7E0C6A5DEC4A5DEC461A17CB2E3C6B2E3C661A27CA7DFC6A7DFC6A9E1
                  C869AB86D9F5E95DA07A969595F2F2F1A1A0A0D9D9D9D7D6D6D7D6D6979797DC
                  DBDBDCDBDB989898D8D8D8D8D8D8DADADAA1A1A1F2F2F1969595599D75DFF7EF
                  549A705EA27D9FDBC39FDBC280BFA07EB89478B38F80BFA09FDBC29FDBC35EA2
                  7D559B73DFF7EF599D75939393F5F5F58F8F8F989898D4D4D4D4D4D4B7B6B6AF
                  AFAFAAA9A9B7B6B6D4D4D4D4D4D4989898919191F5F5F5939393569B72E5F9F4
                  448B5E468C615A9E765A9D755A9E765B9F775B9F775A9E765A9D755A9E76468C
                  61448B5EE5F9F4569B72919191F8F8F880807F82818194939393939394939395
                  959495959494939393939394939382818180807FF8F8F891919154976FE7FAF5
                  E3F6F1E3F6F1E3F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E3F5F0E3F6
                  F1E3F6F1E7FAF554976F8D8D8DFAF9F9F5F5F5F5F5F5F4F4F4F4F4F4F4F4F4F4
                  F4F4F4F4F4F4F4F4F4F4F4F4F4F4F5F5F5F5F5F5FAF9F98D8D8D8CB99E89C6A6
                  B0E7CCAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5
                  CAB0E7CC89C6A68CB99EB2B2B2BEBDBDE0DFDFDEDEDEDEDEDEDEDEDEDEDEDEDE
                  DEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEE0DFDFBEBDBDB2B2B2FFFFFF8AB79B
                  5092694F91684F91684F91684F91684F91684F91684F91684F91684F91684F91
                  685092698AB79BFFFFFFFFFFFFB0AFAF87878786868686868686868686868686
                  8686868686868686868686868686868686878787B0AFAFFFFFFF}
                ParentColor = False
                ShortCut = 115
              end
              object evBenefitReference: TevDBLookupCombo
                Left = 0
                Top = 15
                Width = 121
                Height = 21
                DropDownAlignment = taLeftJustify
                Selected.Strings = (
                  'BENEFIT_NAME'#9'40'#9'Benefit Name'#9'F')
                DataField = 'CO_BENEFITS_NBR'
                DataSource = wwdsDetail
                LookupTable = DM_CO_BENEFITS.CO_BENEFITS
                LookupField = 'CO_BENEFITS_NBR'
                Style = csDropDownList
                TabOrder = 0
                AutoDropDown = True
                ShowButton = True
                PreciseEditRegion = False
                AllowClearKey = False
                OnChange = evBenefitReferenceChange
                OnCloseUp = evBenefitReferenceCloseUp
              end
              object evBenefitSubtype: TevDBLookupCombo
                Left = 0
                Top = 54
                Width = 103
                Height = 21
                DropDownAlignment = taLeftJustify
                Selected.Strings = (
                  'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
                DataField = 'CO_BENEFIT_SUBTYPE_NBR'
                DataSource = wwdsDetail
                LookupTable = DM_CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE
                LookupField = 'CO_BENEFIT_SUBTYPE_NBR'
                Style = csDropDownList
                Color = clBtnFace
                Enabled = False
                ButtonWidth = -1
                ReadOnly = True
                TabOrder = 1
                AutoDropDown = True
                ShowButton = True
                PreciseEditRegion = False
                AllowClearKey = False
              end
              object evMedParticipant: TevDBRadioGroup
                Left = 134
                Top = 78
                Width = 124
                Height = 36
                Caption = '~Med Participant'
                Columns = 2
                DataField = 'COBRA_MEDICAL_PARTICIPANT'
                DataSource = wwdsDetail
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 6
              end
              object evEffectiveStart: TevDBDateTimePicker
                Left = 134
                Top = 15
                Width = 121
                Height = 21
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                CalendarAttributes.PopupYearOptions.StartYear = 2000
                DataField = 'BENEFIT_EFFECTIVE_DATE'
                DataSource = wwdsDetail
                Epoch = 1950
                ShowButton = True
                TabOrder = 3
              end
              object evEffectiveEnd: TevDBDateTimePicker
                Left = 266
                Top = 15
                Width = 118
                Height = 21
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                CalendarAttributes.PopupYearOptions.StartYear = 2000
                DataField = 'EXPIRATION_DATE'
                DataSource = wwdsDetail
                Epoch = 1950
                ShowButton = True
                TabOrder = 4
              end
              object wwcbEnrollment_Status: TevDBComboBox
                Left = 0
                Top = 93
                Width = 121
                Height = 21
                HelpContext = 39003
                ShowButton = True
                Style = csDropDownList
                MapList = False
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'ENROLLMENT_STATUS'
                DataSource = wwdsDetail
                DropDownCount = 8
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 2
                UnboundDataType = wwDefault
              end
              object evSSDisability: TevDBRadioGroup
                Left = 266
                Top = 78
                Width = 118
                Height = 36
                Caption = '~SS Disability'
                Columns = 2
                DataField = 'COBRA_SS_DISABILITY'
                DataSource = wwdsDetail
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 7
                Values.Strings = (
                  'Y'
                  'N')
              end
              object evAddInfo1: TevDBEdit
                Left = 0
                Top = 171
                Width = 383
                Height = 21
                HelpContext = 35068
                DataField = 'ADDITIONAL_INFO1'
                DataSource = wwdsDetail
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 10
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object evAddInfo2: TevDBEdit
                Left = 0
                Top = 210
                Width = 383
                Height = 21
                HelpContext = 35068
                DataField = 'ADDITIONAL_INFO2'
                DataSource = wwdsDetail
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 11
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object evAddInfo3: TevDBEdit
                Left = 0
                Top = 249
                Width = 383
                Height = 21
                HelpContext = 35068
                DataField = 'ADDITIONAL_INFO3'
                DataSource = wwdsDetail
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 12
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object evDBRadioGroup8: TevDBRadioGroup
                Left = 266
                Top = 117
                Width = 118
                Height = 36
                Caption = 'Existing Patient '
                Columns = 2
                DataField = 'EXISTING_PATIENT'
                DataSource = wwdsEmployee
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 9
                Values.Strings = (
                  'L'
                  'P')
              end
              object edPrimaryCare: TevDBEdit
                Left = 0
                Top = 132
                Width = 257
                Height = 21
                DataField = 'PCP'
                DataSource = wwdsEmployee
                TabOrder = 8
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object wwcbDeduction_Frequency: TevDBComboBox
                Left = 134
                Top = 54
                Width = 250
                Height = 21
                HelpContext = 39003
                ShowButton = True
                Style = csDropDownList
                MapList = True
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'DEDUCTION_FREQUENCY'
                DataSource = wwdsDetail
                DropDownCount = 8
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 5
                UnboundDataType = wwDefault
              end
            end
          end
          object fpRates: TisUIFashionPanel
            Left = 432
            Top = 0
            Width = 382
            Height = 327
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpRates'
            Color = 14737632
            TabOrder = 1
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Rates'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evGrpCL: TevGroupBox
              Left = 12
              Top = 35
              Width = 350
              Height = 127
              Caption = 'Global Amounts'
              TabOrder = 0
              object evLabel5: TevLabel
                Left = 234
                Top = 20
                Width = 79
                Height = 13
                Caption = 'Premium Amount'
              end
              object evLabel11: TevLabel
                Left = 12
                Top = 60
                Width = 53
                Height = 13
                Caption = 'EE Amount'
              end
              object evLabel12: TevLabel
                Left = 123
                Top = 60
                Width = 54
                Height = 13
                Caption = 'ER Amount'
              end
              object evLabel36: TevLabel
                Left = 12
                Top = 20
                Width = 56
                Height = 13
                Caption = 'Based On   '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel30: TevLabel
                Left = 234
                Top = 60
                Width = 82
                Height = 13
                Caption = 'COBRA Amount  '
              end
              object evLabel40: TevLabel
                Left = 123
                Top = 20
                Width = 55
                Height = 13
                Caption = 'E/D Group '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evdbCLAmount: TevDBEdit
                Left = 234
                Top = 36
                Width = 103
                Height = 21
                DataField = 'cTotalAmount'
                DataSource = dsCoBenefitRates
                Enabled = False
                TabOrder = 5
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object evdbCLEEAmount: TevDBEdit
                Left = 12
                Top = 76
                Width = 103
                Height = 21
                DataField = 'EE_RATE'
                DataSource = dsCoBenefitRates
                Enabled = False
                TabOrder = 2
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object evdbCLERAmount: TevDBEdit
                Left = 123
                Top = 76
                Width = 103
                Height = 21
                DataField = 'ER_RATE'
                DataSource = dsCoBenefitRates
                Enabled = False
                TabOrder = 3
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object evdbCOCobraAmount: TevDBEdit
                Left = 234
                Top = 76
                Width = 103
                Height = 21
                DataField = 'COBRA_RATE'
                DataSource = dsCoBenefitRates
                Enabled = False
                TabOrder = 4
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object evdbCORateType: TevDBComboBox
                Left = 12
                Top = 36
                Width = 103
                Height = 21
                HelpContext = 39003
                ShowButton = True
                Style = csDropDownList
                MapList = True
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'RATE_TYPE'
                DataSource = dsCoBenefitRates
                DropDownCount = 8
                Enabled = False
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 0
                UnboundDataType = wwDefault
              end
              object evdbCOED_Group: TevDBLookupCombo
                Left = 123
                Top = 36
                Width = 103
                Height = 21
                DropDownAlignment = taLeftJustify
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F'
                  'SUBTRACT_DEDUCTIONS'#9'1'#9'Subtract Deductions'#9'F')
                DataField = 'CL_E_D_GROUPS_NBR'
                DataSource = dsCoBenefitRates
                LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
                LookupField = 'CL_E_D_GROUPS_NBR'
                Style = csDropDownList
                Enabled = False
                TabOrder = 1
                AutoDropDown = True
                ShowButton = True
                PreciseEditRegion = False
                AllowClearKey = False
              end
            end
            object evGrpEE: TevGroupBox
              Left = 12
              Top = 177
              Width = 350
              Height = 127
              Caption = 'Individual Amounts'
              TabOrder = 1
              object evLabel15: TevLabel
                Left = 234
                Top = 20
                Width = 79
                Height = 13
                Caption = 'Premium Amount'
              end
              object evLabel32: TevLabel
                Left = 12
                Top = 60
                Width = 53
                Height = 13
                Caption = 'EE Amount'
              end
              object evLabel33: TevLabel
                Left = 123
                Top = 60
                Width = 54
                Height = 13
                Caption = 'ER Amount'
              end
              object evLabel31: TevLabel
                Left = 234
                Top = 60
                Width = 82
                Height = 13
                Caption = 'COBRA Amount  '
              end
              object evLabel35: TevLabel
                Left = 12
                Top = 20
                Width = 56
                Height = 13
                Caption = 'Based On   '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel46: TevLabel
                Left = 123
                Top = 20
                Width = 55
                Height = 13
                Caption = 'E/D Group '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evdbAmount: TevDBEdit
                Left = 234
                Top = 36
                Width = 103
                Height = 21
                DataField = 'TOTAL_PREMIUM_AMOUNT'
                DataSource = wwdsDetail
                Enabled = False
                TabOrder = 5
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object evdbEEAmount: TevDBEdit
                Left = 12
                Top = 76
                Width = 103
                Height = 21
                DataField = 'EE_RATE'
                DataSource = wwdsDetail
                TabOrder = 2
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                OnExit = evdbEEAmountExit
                Glowing = False
              end
              object evdbERAmount: TevDBEdit
                Left = 123
                Top = 76
                Width = 103
                Height = 21
                DataField = 'ER_RATE'
                DataSource = wwdsDetail
                TabOrder = 3
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                OnExit = evdbERAmountExit
                Glowing = False
              end
              object evdbEECobraAmount: TevDBEdit
                Left = 234
                Top = 76
                Width = 103
                Height = 21
                DataField = 'COBRA_RATE'
                DataSource = wwdsDetail
                TabOrder = 4
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object evdbEERateType: TevDBComboBox
                Left = 12
                Top = 36
                Width = 103
                Height = 21
                HelpContext = 39003
                ShowButton = True
                Style = csDropDownList
                MapList = True
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'RATE_TYPE'
                DataSource = wwdsDetail
                DropDownCount = 8
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 0
                UnboundDataType = wwDefault
                OnChange = evdbEERateTypeChange
              end
              object evdbEEED_Group: TevDBLookupCombo
                Left = 123
                Top = 36
                Width = 103
                Height = 21
                DropDownAlignment = taLeftJustify
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F'
                  'SUBTRACT_DEDUCTIONS'#9'1'#9'Subtract Deductions'#9'F')
                DataField = 'CL_E_D_GROUPS_NBR'
                DataSource = wwdsDetail
                LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
                LookupField = 'CL_E_D_GROUPS_NBR'
                Style = csDropDownList
                TabOrder = 1
                AutoDropDown = True
                ShowButton = True
                PreciseEditRegion = False
                AllowClearKey = False
              end
            end
          end
          object fpCobra: TisUIFashionPanel
            Left = 822
            Top = 0
            Width = 277
            Height = 131
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpCobra'
            Color = 14737632
            TabOrder = 2
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'COBRA'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evLabel26: TevLabel
              Left = 137
              Top = 35
              Width = 103
              Height = 13
              Caption = 'Qualifying Event Date'
            end
            object evLabel25: TevLabel
              Left = 137
              Top = 74
              Width = 94
              Height = 13
              Caption = 'Reason for Refusal '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel27: TevLabel
              Left = 12
              Top = 35
              Width = 80
              Height = 13
              Caption = 'Qualifying Event '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object edEEBenefitNbr: TevDBEdit
              Left = 176
              Top = 259
              Width = 57
              Height = 21
              DataField = 'EE_BENEFITS_NBR'
              DataSource = wwdsDetail
              TabOrder = 4
              UnboundDataType = wwDefault
              Visible = False
              WantReturns = False
              WordWrap = False
              OnChange = edEEBenefitNbrChange
              Glowing = False
            end
            object EdQualifyingEventDate: TevDBDateTimePicker
              Left = 137
              Top = 50
              Width = 117
              Height = 21
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'COBRA_QUALIFYING_EVENT_DATE'
              DataSource = wwdsDetail
              Epoch = 1950
              ShowButton = True
              TabOrder = 1
            end
            object edRefusal: TevDBComboBox
              Left = 137
              Top = 89
              Width = 117
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'COBRA_REASON_FOR_REFUSAL'
              DataSource = wwdsDetail
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 3
              UnboundDataType = wwDefault
            end
            object evCobraEvent: TevDBComboBox
              Left = 12
              Top = 50
              Width = 117
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = True
              AutoDropDown = True
              DataField = 'COBRA_TERMINATION_EVENT'
              DataSource = wwdsDetail
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 0
              UnboundDataType = wwDefault
            end
            object EdElected: TevDBRadioGroup
              Left = 12
              Top = 74
              Width = 117
              Height = 36
              Caption = 'Elected '
              Columns = 2
              DataField = 'COBRA_STATUS'
              DataSource = wwdsDetail
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              Values.Strings = (
                'Y'
                'N')
            end
          end
          object fpControls: TisUIFashionPanel
            Left = 822
            Top = 139
            Width = 277
            Height = 113
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpControls'
            Color = 14737632
            TabOrder = 3
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Controls'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evBtnUseClientBenefit: TevBitBtn
              Left = 12
              Top = 35
              Width = 117
              Height = 25
              Caption = 'Use CO Benefits'
              TabOrder = 0
              OnClick = evBtnUseClientBenefitClick
              Color = clBlack
              Margin = 0
            end
            object evBtnSwitchEE: TevBitBtn
              Left = 12
              Top = 68
              Width = 117
              Height = 25
              Caption = 'Switch to EE Benefit'
              TabOrder = 1
              OnClick = evBtnSwitchEEClick
              Color = clBlack
              Margin = 0
            end
            object evBtnCompanyBenefitScreen: TevBitBtn
              Left = 137
              Top = 35
              Width = 117
              Height = 25
              Caption = 'Company Benefit'
              TabOrder = 2
              OnClick = evBtnCompanyBenefitScreenClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
                CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
                CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
                5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
                A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
                5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
                FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
                B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
                6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
                FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
                B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
                64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
                CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
                B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
                5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
                00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
                BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
                57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
                00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
                BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
                82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
                60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
                BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
                83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
                00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
                C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
                4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
                FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
                C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
                4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
                FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
                C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
                49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
                FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
                CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
                45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
                FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
                DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
                3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
                FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
                EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
                8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
              NumGlyphs = 2
              Margin = 0
            end
            object evBtnEEScheduledEDScreen: TevBitBtn
              Left = 137
              Top = 68
              Width = 117
              Height = 25
              Caption = 'EE Scheduled E/D'
              TabOrder = 3
              OnClick = evBtnEEScheduledEDScreenClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
                CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
                CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
                5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
                A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
                5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
                FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
                B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
                6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
                FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
                B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
                64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
                CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
                B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
                5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
                00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
                BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
                57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
                00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
                BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
                82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
                60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
                BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
                83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
                00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
                C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
                4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
                FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
                C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
                4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
                FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
                C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
                49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
                FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
                CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
                45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
                FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
                DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
                3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
                FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
                EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
                8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
              NumGlyphs = 2
              Margin = 0
            end
          end
        end
      end
      inherited pnlDBControls: TevPanel
        Top = 607
        Width = 1035
      end
    end
    object tsDependent: TTabSheet
      Caption = 'Dependents && Beneficiaries'
      ImageIndex = 40
      object sb_HR_EE_BenefitDependent: TScrollBox
        Left = 0
        Top = 0
        Width = 1035
        Height = 766
        Align = alClient
        TabOrder = 0
        object pnl_HR_EE_BenefitDependentBorder: TevPanel
          Left = 0
          Top = 0
          Width = 833
          Height = 685
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object fp_HR_EE_ListOfBenefitDependets: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 821
            Height = 285
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fp_HR_EE_ListOfBenefitDependets'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Dependent Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnl_HR_EE_BenefitDependetsBody: TevPanel
              Left = 12
              Top = 35
              Width = 786
              Height = 229
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object evDBGrid3: TevDBGrid
                Left = 0
                Top = 0
                Width = 786
                Height = 229
                DisableThemesInTitle = False
                Selected.Strings = (
                  'FIRST_NAME'#9'20'#9'First Name'#9'F'
                  'LAST_NAME'#9'30'#9'Last Name'#9'F'
                  'MIDDLE_INITIAL'#9'5'#9'Middle Initial'#9'F'
                  'GENDER'#9'5'#9'Gender'#9'F'
                  'SOCIAL_SECURITY_NUMBER'#9'15'#9'Social Security Number'#9'F'
                  'RELATION_TYPE'#9'20'#9'Relation Type'#9'F'
                  'DATE_OF_BIRTH'#9'16'#9'Date Of Birth'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\evDBGrid3'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsDependent
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
          object fpDependentDetail: TisUIFashionPanel
            Left = 8
            Top = 301
            Width = 825
            Height = 259
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpDependentDetail'
            Color = 14737632
            TabOrder = 1
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Dependent Details'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object lblPCP: TevLabel
              Left = 606
              Top = 74
              Width = 110
              Height = 13
              Caption = 'Primary Care Physician '
            end
            object evLabel24: TevLabel
              Left = 478
              Top = 74
              Width = 62
              Height = 13
              Caption = 'Date of Birth '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel28: TevLabel
              Left = 209
              Top = 113
              Width = 53
              Height = 13
              Caption = 'Address 2  '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel29: TevLabel
              Left = 478
              Top = 152
              Width = 44
              Height = 16
              Caption = '~Gender'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel34: TevLabel
              Left = 209
              Top = 152
              Width = 29
              Height = 13
              Caption = 'City    '
            end
            object evLabel37: TevLabel
              Left = 209
              Top = 35
              Width = 75
              Height = 16
              Caption = '~Relation Type'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel39: TevLabel
              Left = 12
              Top = 74
              Width = 59
              Height = 16
              Caption = '~First Name'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel41: TevLabel
              Left = 12
              Top = 113
              Width = 60
              Height = 16
              Caption = '~Last Name'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel42: TevLabel
              Left = 478
              Top = 113
              Width = 34
              Height = 13
              Caption = 'SSN    '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel43: TevLabel
              Left = 209
              Top = 74
              Width = 62
              Height = 13
              Caption = 'Address 1     '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel44: TevLabel
              Left = 346
              Top = 152
              Width = 37
              Height = 13
              Caption = 'State    '
            end
            object evLabel45: TevLabel
              Left = 390
              Top = 152
              Width = 52
              Height = 13
              Caption = 'Zip Code   '
            end
            object Label48: TevLabel
              Left = 178
              Top = 74
              Width = 12
              Height = 13
              Caption = 'MI'
            end
            inline DependentMiniNavigation: TMiniNavigationFrame
              Left = 0
              Top = 211
              Width = 788
              Height = 25
              TabOrder = 15
              inherited SpeedButton1: TevSpeedButton
                Width = 129
                Caption = 'Create'
                Glyph.Data = {
                  36060000424D3606000000000000360000002800000020000000100000000100
                  18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
                  CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
                  E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
                  7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
                  82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
                  9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
                  55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
                  5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
                  CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
                  0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
                  76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
                  FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
                  16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
                  8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
                  FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
                  38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
                  9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
                  FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
                  51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
                  9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
                  66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
                  9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
                  76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
                  9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
                  FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
                  59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
                  9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
                  FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
                  0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
                  87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
                  FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
                  55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
                  556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
                  B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
                  88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
                  E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
                  78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              end
              inherited SpeedButton2: TevSpeedButton
                Left = 658
                Width = 129
                Caption = 'Delete'
                Glyph.Data = {
                  36060000424D3606000000000000360000002800000020000000100000000100
                  18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
                  CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
                  F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
                  6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
                  A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
                  CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
                  A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
                  2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
                  C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
                  9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
                  5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
                  F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
                  9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
                  576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
                  FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
                  97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
                  4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
                  F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
                  8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
                  3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
                  F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
                  334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
                  F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
                  2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
                  EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
                  3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
                  EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
                  84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
                  8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
                  F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
                  8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
                  9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
                  F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
                  8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
                  2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
                  C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
                  807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
                  C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
                  EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
                  CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
                  6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              end
            end
            object edDependentAdress1: TevDBEdit
              Left = 209
              Top = 89
              Width = 260
              Height = 21
              DataField = 'ADDRESS1'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 4
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edDependentAdress2: TevDBEdit
              Left = 209
              Top = 128
              Width = 261
              Height = 21
              DataField = 'ADDRESS2'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 5
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edDependentBirthDate: TevDBDateTimePicker
              Left = 478
              Top = 89
              Width = 120
              Height = 21
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.YearsPerColumn = 20
              CalendarAttributes.PopupYearOptions.NumberColumns = 5
              CalendarAttributes.PopupYearOptions.StartYear = 1930
              DataField = 'DATE_OF_BIRTH'
              DataSource = dsDependent
              Epoch = 1900
              ShowButton = True
              TabOrder = 9
            end
            object edDependentCity: TevDBEdit
              Left = 209
              Top = 167
              Width = 129
              Height = 21
              DataField = 'CITY'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 6
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edDependentExisitngPatient: TevDBRadioGroup
              Left = 606
              Top = 113
              Width = 88
              Height = 36
              Caption = 'Existing Patient '
              Columns = 2
              DataField = 'EXISTING_PATIENT'
              DataSource = dsDependent
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 13
              Values.Strings = (
                'Y'
                'N')
            end
            object edDependentFirstName: TevDBEdit
              Left = 12
              Top = 89
              Width = 158
              Height = 21
              DataField = 'FIRST_NAME'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 2
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edDependentFullTimeStudent: TevDBRadioGroup
              Left = 702
              Top = 113
              Width = 98
              Height = 36
              Caption = 'Full Time Student '
              Columns = 2
              DataField = 'FULL_TIME_STUDENT'
              DataSource = dsDependent
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 14
              Values.Strings = (
                'Y'
                'N')
            end
            object edDependentGender: TevDBComboBox
              Left = 478
              Top = 167
              Width = 120
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = True
              AutoDropDown = True
              DataField = 'GENDER'
              DataSource = dsDependent
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 11
              UnboundDataType = wwDefault
            end
            object edDependentLastName: TevDBEdit
              Left = 12
              Top = 128
              Width = 189
              Height = 21
              DataField = 'LAST_NAME'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 3
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edDependentPCP: TevDBEdit
              Left = 606
              Top = 89
              Width = 194
              Height = 21
              DataField = 'PCP'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 12
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edDependentRelation: TevDBComboBox
              Left = 209
              Top = 50
              Width = 194
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = True
              AutoDropDown = True
              DataField = 'RELATION_TYPE'
              DataSource = dsDependent
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 1
              UnboundDataType = wwDefault
            end
            object edDependentSSN: TevDBEdit
              Left = 478
              Top = 128
              Width = 120
              Height = 21
              DataField = 'SOCIAL_SECURITY_NUMBER'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*3{#}-*2{#}-*4{#}'
              TabOrder = 10
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edDependentState: TevDBEdit
              Left = 346
              Top = 167
              Width = 36
              Height = 21
              DataField = 'STATE'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 7
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object edDependentZip: TevDBEdit
              Left = 390
              Top = 167
              Width = 80
              Height = 21
              DataField = 'ZIP_CODE'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 8
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object evBtnCopyEEAddress: TevBitBtn
              Left = 12
              Top = 167
              Width = 189
              Height = 25
              Caption = 'Copy EE Address'
              TabOrder = 16
              OnClick = evBtnCopyEEAddressClick
              Color = clBlack
              Margin = 0
            end
            object evDBEdit15: TevDBEdit
              Left = 174
              Top = 89
              Width = 23
              Height = 21
              DataField = 'MIDDLE_INITIAL'
              DataSource = dsDependent
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 17
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object evPersonType: TevDBRadioGroup
              Left = 12
              Top = 35
              Width = 188
              Height = 36
              Caption = 'Person Type '
              Columns = 2
              DataField = 'PERSON_TYPE'
              DataSource = dsDependent
              TabOrder = 0
              OnChange = evPersonTypeChange
            end
          end
          object edDependentPersonType: TevDBRadioGroup
            Left = 844
            Top = 494
            Width = 187
            Height = 36
            Caption = 'Person Type '
            Columns = 2
            DataField = 'PERSON_TYPE'
            DataSource = dsDependent
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
            Visible = False
          end
        end
      end
    end
    object tsAssignDependent: TTabSheet
      Caption = 'Assign Dependents'
      ImageIndex = 4
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 1035
        Height = 766
        VertScrollBar.Position = 51
        Align = alClient
        TabOrder = 0
        object sbAddAssingDependent: TevSpeedButton
          Left = 410
          Top = 288
          Width = 74
          Height = 22
          Caption = 'Add'
          HideHint = True
          AutoSize = False
          OnClick = sbAddAssingDependentClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
            DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
            DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
            78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
            8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
            F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
            F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
            DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
            DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
          Layout = blGlyphRight
          ParentColor = False
          ShortCut = 0
        end
        object sbDelAssignDependent: TevSpeedButton
          Left = 410
          Top = 336
          Width = 74
          Height = 22
          Caption = 'Remove'
          HideHint = True
          AutoSize = False
          OnClick = sbDelAssignDependentClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
            DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
            DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
            78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
            448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
            F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
            F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
            44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
            DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
          ParentColor = False
          ShortCut = 0
        end
        object fpAssignedDependets: TisUIFashionPanel
          Left = 498
          Top = 211
          Width = 385
          Height = 275
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpAssignedDependets'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Assigned Dependents'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdAssignedDependents: TevDBGrid
            Left = 12
            Top = 35
            Width = 353
            Height = 215
            DisableThemesInTitle = False
            Selected.Strings = (
              'FIRST_NAME'#9'20'#9'First Name'#9'T'
              'LAST_NAME'#9'20'#9'Last Name'#9'T'
              'RELATION_TYPE'#9'1'#9'Relation Type'#9'T')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\grdAssignedDependents'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Color = 2
            DataSource = dsAssignedDependent
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            ReadOnly = False
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpDependentBenefitSummary: TisUIFashionPanel
          Left = 12
          Top = -39
          Width = 672
          Height = 241
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpBenefitInfo'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Benefit Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdDependentBenefitSummary: TevDBGrid
            Left = 12
            Top = 35
            Width = 637
            Height = 182
            DisableThemesInTitle = False
            Selected.Strings = (
              'Benefit'#9'40'#9'Benefit Reference'#9'F'
              'BenefitAmountType'#9'40'#9'Benefit amount type'#9'F'
              'BENEFIT_EFFECTIVE_DATE'#9'18'#9'Benefit Effective Date'#9#9)
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\grdDependentBenefitSummary'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsDependentBenefits
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpAvailableDependents: TisUIFashionPanel
          Left = 12
          Top = 211
          Width = 390
          Height = 275
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpAvailableDependents'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Available Dependents'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdAvailableDependents: TevDBGrid
            Left = 12
            Top = 35
            Width = 354
            Height = 215
            DisableThemesInTitle = False
            Selected.Strings = (
              'FIRST_NAME'#9'20'#9'First Name'#9'F'
              'LAST_NAME'#9'20'#9'Last Name'#9'F'
              'RELATION_TYPE'#9'10'#9'Relation Type'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\grdAvailableDependents'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Color = 2
            DataSource = dsAvailableDependent
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object isUIFashionPanel1: TisUIFashionPanel
          Left = 498
          Top = 494
          Width = 239
          Height = 268
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Plan Dates'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evPanel1: TevPanel
            Left = 12
            Top = 35
            Width = 213
            Height = 216
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object evLabel23: TevLabel
              Left = 0
              Top = 139
              Width = 53
              Height = 13
              Caption = 'Begin Date'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel53: TevLabel
              Left = 98
              Top = 139
              Width = 45
              Height = 13
              Caption = 'End Date'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object edtPlanBeginDate: TevDBDateTimePicker
              Left = 0
              Top = 154
              Width = 88
              Height = 21
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'ACA_DEP_PLAN_BEGIN_DATE'
              DataSource = dsBeneficiaryPlanDate
              Epoch = 1950
              ShowButton = True
              TabOrder = 0
              OnClick = edtPlanBeginDateClick
              OnEnter = edtPlanBeginDateClick
            end
            object edtPlanEndDate: TevDBDateTimePicker
              Left = 98
              Top = 153
              Width = 88
              Height = 21
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'ACA_DEP_PLAN_END_DATE'
              DataSource = dsBeneficiaryPlanDate
              Epoch = 1950
              ShowButton = True
              TabOrder = 1
              OnClick = edtPlanBeginDateClick
              OnEnter = edtPlanBeginDateClick
            end
            inline BeneficiaryPlanDateNavigation: TMiniNavigationFrame
              Left = 0
              Top = 186
              Width = 197
              Height = 25
              TabOrder = 2
              inherited SpeedButton1: TevSpeedButton
                Width = 88
                Caption = 'Create'
                Glyph.Data = {00000000}
              end
              inherited SpeedButton2: TevSpeedButton
                Left = 100
                Width = 88
                Caption = 'Delete'
                Glyph.Data = {00000000}
              end
            end
            object grdPlanDates: TevDBGrid
              Left = 0
              Top = 2
              Width = 202
              Height = 129
              DisableThemesInTitle = False
              Selected.Strings = (
                'ACA_DEP_PLAN_BEGIN_DATE'#9'13'#9'Begin Date'#9'F'
                'ACA_DEP_PLAN_END_DATE'#9'13'#9'End Date'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\grdPlanDates'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              DataSource = dsBeneficiaryPlanDate
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgShowFooter, dgFooter3DCells, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 3
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDrawFooterCell = grdPrimaryBeneficiariesDrawFooterCell
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              OnSelectionChanged = grdPrimaryBeneficiariesSelectionChanged
              NoFire = False
            end
          end
        end
      end
    end
    object tsAssignBeneficiaries: TTabSheet
      Caption = 'Assign Beneficiaries'
      ImageIndex = 5
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 1035
        Height = 766
        Align = alClient
        TabOrder = 0
        object fpABContingentBeneficiaries: TisUIFashionPanel
          Left = 409
          Top = 478
          Width = 386
          Height = 198
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpBenefitInfo'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Contingent Beneficiaries'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdContingentBeneficiaries: TevDBGrid
            Left = 12
            Top = 35
            Width = 350
            Height = 140
            DisableThemesInTitle = False
            Selected.Strings = (
              'FIRST_NAME'#9'20'#9'First Name'#9'F'
              'LAST_NAME'#9'20'#9'Last Name'#9'F'
              'PERCENTAGE'#9'10'#9'Percentage'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\grdContingentBeneficiaries'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsContingentBeneficiaries
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgShowFooter, dgFooter3DCells, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnDrawFooterCell = grdContingentBeneficiariesDrawFooterCell
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            OnSelectionChanged = grdContingentBeneficiariesSelectionChanged
            NoFire = False
          end
        end
        object fpABBenefitSummary: TisUIFashionPanel
          Left = 12
          Top = 12
          Width = 672
          Height = 241
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpBenefitInfo'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Benefit Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdABBenefitSummary: TevDBGrid
            Left = 12
            Top = 35
            Width = 637
            Height = 182
            DisableThemesInTitle = False
            Selected.Strings = (
              'Benefit'#9'40'#9'Benefit reference'#9'F'
              'BenefitAmountType'#9'40'#9'Benefit amount type'#9'F'
              'BENEFIT_EFFECTIVE_DATE'#9'18'#9'Benefit Effective Date'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\grdABBenefitSummary'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsBeneficiaryBenefits
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpABPrimaryBeneficiaries: TisUIFashionPanel
          Left = 409
          Top = 262
          Width = 386
          Height = 203
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpListOfBenefits'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Primary Beneficiaries'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdPrimaryBeneficiaries: TevDBGrid
            Left = 12
            Top = 35
            Width = 350
            Height = 140
            DisableThemesInTitle = False
            Selected.Strings = (
              'FIRST_NAME'#9'20'#9'First Name'#9'F'
              'LAST_NAME'#9'20'#9'Last name'#9'F'
              'PERCENTAGE'#9'10'#9'Percentage'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\grdPrimaryBeneficiaries'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsPrimaryBeneficiaries
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgShowFooter, dgFooter3DCells, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnDrawFooterCell = grdPrimaryBeneficiariesDrawFooterCell
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            OnSelectionChanged = grdPrimaryBeneficiariesSelectionChanged
            NoFire = False
          end
        end
        object fpABAvailableDependents: TisUIFashionPanel
          Left = 12
          Top = 262
          Width = 388
          Height = 411
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpAvailableDependents'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Available Beneficiaries'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblPercentage: TevLabel
            Left = 12
            Top = 264
            Width = 55
            Height = 13
            Caption = 'Percentage'
          end
          object sbAddAssingBeneficiaries: TevSpeedButton
            Left = 248
            Top = 277
            Width = 75
            Height = 22
            Caption = 'Add'
            HideHint = True
            AutoSize = False
            OnClick = sbAddAssingBeneficiariesClick
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
              DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
              DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
              78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
              8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
              F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
              F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
              DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
              DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
            Layout = blGlyphRight
            ParentColor = False
            ShortCut = 0
          end
          object sbRemoveAssingBeneficiaries: TevSpeedButton
            Left = 248
            Top = 333
            Width = 75
            Height = 22
            Caption = 'Remove'
            HideHint = True
            AutoSize = False
            OnClick = sbRemoveAssingBeneficiariesClick
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
              DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
              DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
              78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
              448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
              F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
              F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
              44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
              DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
            ParentColor = False
            ShortCut = 0
          end
          object grdABAvailableDependents: TevDBGrid
            Left = 12
            Top = 35
            Width = 356
            Height = 211
            DisableThemesInTitle = False
            Selected.Strings = (
              'FIRST_NAME'#9'20'#9'First Name'#9'F'
              'LAST_NAME'#9'20'#9'Last Name'#9'F'
              'RELATION_TYPE'#9'10'#9'Relation Type'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\grdABAvailableDependents'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Color = 2
            DataSource = dsABAvailableDependent
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object gbPrimary: TevGroupBox
            Left = 16
            Top = 320
            Width = 136
            Height = 41
            Caption = 'Primary'
            TabOrder = 1
            object rbPrimaryYes: TevRadioButton
              Left = 16
              Top = 16
              Width = 81
              Height = 17
              Caption = 'Yes'
              Checked = True
              TabOrder = 0
              TabStop = True
            end
            object rbPrimaryNo: TevRadioButton
              Left = 80
              Top = 16
              Width = 49
              Height = 17
              Caption = 'No'
              TabOrder = 1
            end
          end
          object edPercentage: TevSpinEdit
            Left = 16
            Top = 280
            Width = 49
            Height = 22
            MaxLength = 3
            MaxValue = 100
            MinValue = 1
            TabOrder = 2
            Value = 1
          end
        end
      end
    end
    object tsBenefitPmnt: TTabSheet
      Caption = 'Benefit Payment'
      ImageIndex = 39
      object sb_HR_EE_BenefitPayments: TScrollBox
        Left = 0
        Top = 0
        Width = 1035
        Height = 766
        Align = alClient
        TabOrder = 0
        object pnl_HR_EE_BenefitPaymentBorder: TevPanel
          Left = 0
          Top = 0
          Width = 529
          Height = 602
          BevelOuter = bvNone
          BorderWidth = 8
          ParentColor = True
          TabOrder = 0
          object fp_HR_EE_ListOfBenefitPayments: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 516
            Height = 300
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fp_HR_EE_ListOfBenefitPayments'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Payment Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnl_HR_EE_BenefitPaymentsBody: TevPanel
              Left = 12
              Top = 35
              Width = 483
              Height = 245
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object evDBGrid2: TevDBGrid
                Left = 0
                Top = 0
                Width = 483
                Height = 245
                DisableThemesInTitle = False
                Selected.Strings = (
                  'BENEFIT_NAME'#9'31'#9'Benefit Reference'#9'F'
                  'BENEFIT_SUBTYPE'#9'31'#9'Benefit  Amount Type'#9'F'
                  'PAYMENT_DATE'#9'14'#9'Payment Date'#9'F'
                  'PAYMENT_AMOUNT'#9'15'#9'Payment Amount'#9'F'
                  'PERIOD_START_DATE'#9'18'#9'Period Start Date'#9'F'
                  'PERIOD_END_DATE'#9'15'#9'Period End Date'#9'F'
                  'VOID'#9'5'#9'Void'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_EE_BENEFITS\evDBGrid2'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsBenefitPmnt
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
          object fp_HR_EE_BenefitPaymentInfo: TisUIFashionPanel
            Left = 8
            Top = 316
            Width = 516
            Height = 282
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 1
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Payment Details'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnl_HR_EE_BenefitPaymentInfoBody: TevPanel
              Left = 8
              Top = 35
              Width = 489
              Height = 231
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object evLabel16: TevLabel
                Left = 0
                Top = 0
                Width = 86
                Height = 13
                Caption = 'Benefit Reference'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel19: TevLabel
                Left = 0
                Top = 39
                Width = 99
                Height = 13
                Caption = 'Benefit Amount Type'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel4: TevLabel
                Left = 0
                Top = 78
                Width = 76
                Height = 16
                Caption = '~Payment Date'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel22: TevLabel
                Left = 0
                Top = 117
                Width = 77
                Height = 16
                Caption = '~Payment Type'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel18: TevLabel
                Left = 0
                Top = 157
                Width = 81
                Height = 13
                Caption = 'Payment Number'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel6: TevLabel
                Left = 124
                Top = 78
                Width = 89
                Height = 16
                Caption = '~Payment Amount'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel7: TevLabel
                Left = 124
                Top = 117
                Width = 90
                Height = 16
                Caption = '~Period Start Date'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel8: TevLabel
                Left = 124
                Top = 157
                Width = 87
                Height = 16
                Caption = '~Period End Date'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel14: TevLabel
                Left = 248
                Top = 78
                Width = 76
                Height = 13
                Caption = 'Agency Name   '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel20: TevLabel
                Left = 372
                Top = 0
                Width = 88
                Height = 13
                Caption = 'Coupon Sent Date'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel48: TevLabel
                Left = 372
                Top = 39
                Width = 84
                Height = 13
                Caption = 'Letter Sent Date  '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel50: TevLabel
                Left = 372
                Top = 78
                Width = 85
                Height = 16
                Caption = '~Deduction Type'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object evLabel10: TevLabel
                Left = 248
                Top = 117
                Width = 42
                Height = 13
                Caption = 'Remarks'
              end
              object evLabel9: TevLabel
                Left = 248
                Top = 157
                Width = 37
                Height = 13
                Caption = 'Reason'
              end
              object edBenefitName: TevDBEdit
                Left = 0
                Top = 15
                Width = 240
                Height = 21
                DataField = 'BENEFIT_NAME'
                DataSource = dsBenefitPmnt
                Enabled = False
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 0
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object edBenefitAmountType: TevDBEdit
                Left = 0
                Top = 54
                Width = 240
                Height = 21
                DataField = 'BENEFIT_SUBTYPE'
                DataSource = dsBenefitPmnt
                Enabled = False
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 1
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object edPaymentDate: TevDBDateTimePicker
                Left = 0
                Top = 93
                Width = 116
                Height = 21
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                CalendarAttributes.PopupYearOptions.StartYear = 2000
                DataField = 'PAYMENT_DATE'
                DataSource = dsBenefitPmnt
                Epoch = 1950
                ShowButton = True
                TabOrder = 2
              end
              object edPaymentType: TevDBComboBox
                Left = 0
                Top = 132
                Width = 116
                Height = 21
                ShowButton = True
                Style = csDropDownList
                MapList = True
                AllowClearKey = True
                AutoDropDown = True
                DataField = 'PAYMENT_TYPE'
                DataSource = dsBenefitPmnt
                DropDownCount = 8
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 3
                UnboundDataType = wwDefault
              end
              object edPaymentNumber: TevDBEdit
                Left = 0
                Top = 172
                Width = 116
                Height = 21
                DataField = 'PAYMENT_NUMBER'
                DataSource = dsBenefitPmnt
                TabOrder = 4
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object edtPymtAmount: TevDBEdit
                Left = 124
                Top = 93
                Width = 116
                Height = 21
                DataField = 'PAYMENT_AMOUNT'
                DataSource = dsBenefitPmnt
                TabOrder = 5
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object edPeriodStartDate: TevDBDateTimePicker
                Left = 124
                Top = 132
                Width = 116
                Height = 21
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                CalendarAttributes.PopupYearOptions.StartYear = 2000
                DataField = 'PERIOD_START_DATE'
                DataSource = dsBenefitPmnt
                Epoch = 1950
                ShowButton = True
                TabOrder = 6
              end
              object edPeriodEndDate: TevDBDateTimePicker
                Left = 124
                Top = 172
                Width = 116
                Height = 21
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                CalendarAttributes.PopupYearOptions.StartYear = 2000
                DataField = 'PERIOD_END_DATE'
                DataSource = dsBenefitPmnt
                Epoch = 1950
                ShowButton = True
                TabOrder = 7
              end
              object edReconciled: TevDBRadioGroup
                Left = 248
                Top = 0
                Width = 116
                Height = 36
                Caption = '~Reconciled'
                Columns = 2
                DataField = 'RECONCILED'
                DataSource = dsBenefitPmnt
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'Yes'
                  'No')
                ParentFont = False
                TabOrder = 8
                Values.Strings = (
                  'Y'
                  'N')
              end
              object evDBLookupCombo1: TevDBLookupCombo
                Left = 248
                Top = 93
                Width = 116
                Height = 21
                DropDownAlignment = taLeftJustify
                Selected.Strings = (
                  'Agency_Name'#9'40'#9'Agency name'#9'F')
                DataField = 'CL_AGENCY_NBR'
                DataSource = dsBenefitPmnt
                LookupTable = DM_CL_AGENCY.CL_AGENCY
                LookupField = 'CL_AGENCY_NBR'
                Style = csDropDownList
                TabOrder = 10
                AutoDropDown = True
                ShowButton = True
                PreciseEditRegion = False
                AllowClearKey = False
              end
              object edCouponSentDate: TevDBDateTimePicker
                Left = 372
                Top = 15
                Width = 116
                Height = 21
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                CalendarAttributes.PopupYearOptions.StartYear = 2000
                DataField = 'COUPON_SENT_DATE'
                DataSource = dsBenefitPmnt
                Epoch = 1950
                ShowButton = True
                TabOrder = 11
              end
              object edLetterSentDate: TevDBDateTimePicker
                Left = 372
                Top = 54
                Width = 116
                Height = 21
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                CalendarAttributes.PopupYearOptions.StartYear = 2000
                DataField = 'LETTER_SENT_DATE'
                DataSource = dsBenefitPmnt
                Epoch = 1950
                ShowButton = True
                TabOrder = 12
              end
              object edDeductionType: TevDBComboBox
                Left = 372
                Top = 93
                Width = 116
                Height = 21
                ShowButton = True
                Style = csDropDownList
                MapList = True
                AllowClearKey = True
                AutoDropDown = True
                DataField = 'BENEFIT_DEDUCTION_TYPE'
                DataSource = dsBenefitPmnt
                DropDownCount = 8
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 13
                UnboundDataType = wwDefault
              end
              object edVoid: TevDBRadioGroup
                Left = 248
                Top = 39
                Width = 116
                Height = 36
                Caption = '~Void'
                Columns = 2
                DataField = 'VOID'
                DataSource = dsBenefitPmnt
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'Yes'
                  'No')
                ParentFont = False
                TabOrder = 9
                Values.Strings = (
                  'Y'
                  'N')
              end
              object edRemarks: TevDBEdit
                Left = 248
                Top = 132
                Width = 240
                Height = 21
                DataField = 'REMARKS'
                DataSource = dsBenefitPmnt
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 14
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object edReason: TevDBEdit
                Left = 248
                Top = 172
                Width = 240
                Height = 21
                DataField = 'REASON'
                DataSource = dsBenefitPmnt
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 15
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              inline BenefitPmtMiniNavigation: TMiniNavigationFrame
                Left = 0
                Top = 201
                Width = 489
                Height = 25
                TabOrder = 16
                inherited SpeedButton1: TevSpeedButton
                  Width = 116
                  Caption = 'Create'
                  Glyph.Data = {
                    36060000424D3606000000000000360000002800000020000000100000000100
                    18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                    FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
                    CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
                    E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
                    7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
                    82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
                    9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
                    55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
                    5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
                    CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
                    0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
                    76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
                    FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
                    16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
                    8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
                    FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
                    38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
                    9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
                    FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
                    51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
                    9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
                    66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
                    9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
                    76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
                    9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
                    FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
                    59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
                    9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
                    FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
                    0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
                    87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
                    FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
                    55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
                    556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
                    B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
                    88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
                    E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
                    78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                end
                inherited SpeedButton2: TevSpeedButton
                  Left = 371
                  Width = 116
                  Caption = 'Delete'
                  Glyph.Data = {
                    36060000424D3606000000000000360000002800000020000000100000000100
                    18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                    FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
                    CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
                    F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
                    6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
                    A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
                    CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
                    A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
                    2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
                    C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
                    9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
                    5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
                    F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
                    9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
                    576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
                    FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
                    97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
                    4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
                    F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
                    8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
                    3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
                    F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
                    334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
                    F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
                    2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
                    EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
                    3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
                    EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
                    84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
                    8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
                    F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
                    8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
                    9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
                    F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
                    8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
                    2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
                    C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
                    807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
                    C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
                    EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
                    CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                    FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
                    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
                    6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                end
              end
            end
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 1043
    object evcbOnlyActive: TevCheckBox
      Left = 448
      Top = 8
      Width = 111
      Height = 17
      Caption = 'Only Active E/Ds'
      Checked = True
      State = cbChecked
      TabOrder = 4
      OnClick = evcbOnlyActiveClick
    end
    object cbShowPayrollDetails: TevCheckBox
      Left = 592
      Top = 8
      Width = 137
      Height = 17
      Caption = 'Show HR Benefits Only'
      Checked = True
      State = cbChecked
      TabOrder = 5
      OnClick = cbShowPayrollDetailsClick
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_BENEFITS.EE_BENEFITS
    Left = 246
    Top = 26
  end
  inherited PageControlImages: TevImageList
    Bitmap = {
      494C010129002C00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000B000000001001000000000000058
      0000000000000000000000000000000000000000FF7FBF73BF73BE6FDF77DC77
      BA73BA73BA73BA73BA73BA73BA73BA73BA730000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000BE73BC3ADC3ABC363E531057
      A742C846C846C846A846A846A846A846A7420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000DF77FD42FD3EDD3E3F53315B
      C74AE84AE84AE84AC84AC746C746C746C7420000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7F5D5B1D471D475E5B7667
      E84E09530953084FE84EE84AC846C746C8460000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FDF7B5D5B7D5F5E5FBD73
      2E5B29570A572D5B2E5B0C53E84AC746305B0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FDF771D4BDC3A3E4F
      9A6F2F5B766B986B7767996F54630F5BDB770000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7F5D57DD3ADD3ADD3A
      3E53BD73335FCA4AC846EB4A5563FE7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7F3D53FD42FD42DD3E
      3E575563C746C84AC846C746C846976BFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7F7E671D4B3E4BFD42
      7D5F0D53E84AE84EE84AE84AC7460F57FD7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF7FFF7FFF7F7E633D533D4F
      9D6B0C5309530953E94EE84AC7460E57FE7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F00000000
      FF7F525F29572A5B0953E84EC74A5563FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FDC772E5B2A5B295709532F5BDD7BFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FDD7B976B7467976BFE7BFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000396739673967DE7B0000396739673967396739673967
      396739670000FF7FFF7FFF7FFF7FFF7FFF7F000000000000000000000000BD77
      7B6F3967396739679C7300000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A6B396739673967396739673967
      39673967396739673967095609560956F662B84A550554055405540554055405
      54055505B94AFF7FFF7FFF7FFF7FFF7FFF7F39677B6FDE7BBD777B6F3967F266
      8B66466646664666CF6639679C73000000000000000000000000000000000000
      000000000000000000000000000000000000FD3EDD3ADD3ADD3ADD3ADD3AFD3A
      FE3AFE3AFE3A1F3BE6596E5EA47E32774862760D7B2EBC3ABC3A9C3A9C3ABC3A
      BC3A7B2E77093967386339673967396700004A29F75A1567F2668B664666876A
      A772E876E876E876A772666ACF6639679C733967396739673967396739673967
      396739673967396739673967396739677B6FDD3ABF67BF63BF63BF63DF67D852
      EF3DCE3DCE3DCE3D3142964E136BB57F4862F8191D4B9C327C2E7C2E7C2E7C2E
      9C321D4BB90D043A0636E635E6350636D45A6B2D7C6F666A876EC9720A770A7B
      C976876E666A676A517B4F7BEB76876EAF6610530F530F4F0F4F0F4F0F4F0F4F
      0F4F0F4F0F4F0F4F0F4F0F4F0F4F0F533357DD3ABF67DF73DF73BF5F3142524E
      5A6F9C739C735A6F734E1142BE737777486639265E5FDE3AFE3EFE7FBD7BFE3E
      FE3A5E5FDA11A846A942AA42AA428A42063A6C2D9D730B7B0C7B0C7B0C7B666A
      886E0D77737FCC6E466AEE72537BB77FED720F53FD77FC77FC77FC77FC77FC77
      FC77FC77FC77FC77FC77FC77FC77FD770F53DD3ABF67DF6FDF73B84E734E9C73
      1D4B3E437E479E579C73734EB656476600003A227E639F67FF77FF7FDE7B9E6B
      9F677F63181AA842A942873A873A0E53063A8C31BE77957F947F737F727F947F
      B87F3277CC6E256607622766CE6E4566AA6E0F53FC778F2EFA73566356635663
      566356635663566356635663F973FC770F4FDD3ABF6B7F5B9F5B10429C73FC46
      3D475E479F4FBF539E579C73113E00000000FF7B9C32FD4215676B62E7510E46
      FF423622A83EFF7FBE77A742A63E725F063AAD35DF7B1073CC6EEF720F73CC6E
      896ECB720D772E770C7708620000000000000F53FD776F2EF973F873F86FF86F
      F86FF86FF86FF86FF86FF86FF86FFB770F4FDD3ABF6B7F579F5BEF3DDE7FDC3A
      7E5B7E537E4F9F4F7E47DE7F114200000000FF7FFF7FBF73B2568C622A56CB39
      34539063DA73FF7FDF7B786792637363263ECE395B6B00000000000029620762
      0762E761E7610762076229660000000000000F53FD7B6E2AF873555F555F555F
      555F555F555F555F555F345FD76BFB770F4FDD3ABF6BBF6FDF6FF03DFF7FDC3A
      BE6B7E5B7E535E473D3FFF7F114200000000FF7FFF7F3867EF720F73AE6A4B5E
      4846EC52336B8C62E84D0B4AED528A4AFE7F000000000000000000000762517F
      937F947F717F2F7B0C7707660000000000000F53FD7B4E26F76FD76BD76BD76B
      D76BD76BD76BD76BD76BD66BD66BDC770F4FDD3ABF6FBF6BDF6F1142DD7BDC42
      BF73BE6B7E5B3D4B3D4BDD7B324600000000FF7FFF7FE234B47F517BEF6ECE6A
      0439FE7BAD5A8D622A56E845BB77FF7FFF7F00000000000000000000B06E0762
      07620762076207620762B06E0000000000000F53FD7B4D26F66FD66BD66BD66B
      D66BD66BD66BD66BD66BD66BB567DC770F4FDD3ADF735F535F53F85AB5560000
      DC42FC42FC421D4B0000B55A984200000000FF7FFF7FE134AD66CF6A3177F072
      80285A6BEF721073CE6A6B5E3867FF7FFF7F00000000000000000000DE7B7B6F
      39673967396739677B6FDE7B0000000000000F53FE7B2C1ED56B6B2D2E3E0E3E
      0E3ED5674B2D2E3E0E3E0E3EB467DD7B0F4FDD36DF735F4F5F4FDF6F744ED65A
      FF7F00000000FF7FD65A9452FE3A00000000FF7FFF7F0239C555295AEA51263D
      A12C253DB57F517BEF6ECE6AE334FF7FFF7F0000000000000000000017676C66
      07620762076207626C6617670000000000000F53FE7BFE7BFE7BFE7BFE7BFE7B
      FE7BFD7BFE7BFE7BFE7BFD7BDD7BFD7B0F53DD36DF77BF67BF6B3F4F3F4FF95A
      524A31463146524AF956FF77DD3A00000000FF7FFF7F0239E555285EE8552439
      2539E134AD66CF6A31771073A12CFF7FFF7F000000000000000000000866EE76
      937F937F937F937FEE72086600000000000075630F530F530F530F530F4F0F53
      0F530F530F530F4F0F530F530F4F0F53755FDD36FF7B9F679F673F4B3F4BBF67
      BF673F4B3F4BBF67BF67FF7BDD3600000000FF7FFF7F9C77223D834943410339
      FF7F0239E555295AEA51463DC230FF7FFF7F000000000000000000000762947F
      EC72EB72EB72EC72947F28660000000000000000000000000000000000000000
      000000000000000000000000000000000000DD3AFF7FFF7BFF7BFF7FFF7FFF7B
      FF7BFF7FFF7FFF7BFF7BFF7FDD3A00000000FF7FFF7FFF7FFF7FFF7F0000FF7F
      FF7F0239E555285EE8552439C234FF7FFF7F000000000000000000002866EE76
      2F7B2F7B2F7B2F7BEE7628660000000000000000000000000000000000000000
      000000000000000000000000000000000000FD42DD3ADD36DD36DD36DD36DD36
      DD36DD36DD36DD36DD36DD3AFD4200000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F9C77223D6349434102399C77FF7FFF7F00000000000000000000BC7B8E6A
      07620762076207628E6ABC7B0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000005A6B000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000396739677B6F0000000000000000AB3DA941496E2A6A0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000BD77422A00000000000000000000000000000000DE7B396739673967
      39673967396739673967DE7B0000000000000000DE7B39673967396739673967
      39673967202A0043683A7B6F0000000000000B4E905A527FEB7EA74D0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00003967DE7B3967396739673967
      39670000D152402E396739673967FF7F0000000000000000B84A550954055405
      54055405540554055509B84A0000000000000000B84A55095505550556055705
      5705390120268053E042883A7B6F00000000A976937F737F647E627EA74D0000
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD7720265563603680368036A036
      602EBD77803EC04680368036602E5967000000000000000076095A2E9C369C36
      9C369C369C369C365B2E7609000000000000000076095A2E9C369D36682E2026
      20262026002260536053E042883A7B6F0000C976917FE87E857E637E637EA74D
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD04EA0425663A046005320536053
      C03A335BE052004F204F40538057873A0000000000000000D819FD467B2E7B2E
      7B2A7B2A7B2E7C2EFD46B7110000000000000000D819FD467C2E7E2E0022A763
      60536053404F404F404F404FE042883A0000FF7FE661297FE77EA57E637E637E
      A74D0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F60320053F152ED4E803A8036A03A
      602E00006032C04680368036404BA036000000000000000019263D57BD36BD36
      FE7FDD7BDD36BD363D57D815000000000000000019263D57BD36DF3E001EAD67
      204F204F204F004B204F204F8C6720220000FF7FFF7FE7612A7FE77EA57E637E
      637EA74D0000FF7FFF7FFF7FFF7FFF7FFF7F60360053A03A5A6BDE7B39670000
      00000000DC77402E0000165FAF4A202600000000000000005A2E7E67FE3EDF6F
      FF7FDE7B7D671F3F9E67F91900000000000000005A2E7E671F3FFF77001EB273
      A96BA96B896BB26F004B8C63C042CA420000FF7FFF7FFF7FE7612A7FE77EA57E
      637E637EA74D0000FF7FFF7FFF7FFF7FFF7F6036E04E20536232F35600220000
      000000000000A83E9C7360320047402A00000000000000005A261D4FBF6B7677
      8B62E7516F5ADF6F1D535A2A00000000000000005A261D4FBF6B777B433A001E
      001E001E001E906B6B63A03ECB4200000000FF7FFF7FFF7FFF7FE7612A7FE77E
      A57E637E637EA74D0000FF7FFF7FFF7FFF7FEC4AE04A004F204B404B545F0000
      0000000000000000AD46E04E4053C03A00000000000000000000FD42BE32175F
      6C5EE84D5246BE36FD42000000000000000000000000FD42BE32185F6C5EE951
      544ADF3600228F6FA03ECB42000000000000FF7FFF7FFF7FFF7FFF7FE7612A7F
      E77EA57E637E627EA6510000FF7FFF7FFF7F3967402E20534053A03600000000
      0000000000000000402E0057404F85360000000000000000000000007B6FAF62
      EF6EAD664C569C73000000000000000000000000000000007B6FAF62EF6EAD6A
      4C569C732022A042CA420000000000000000FF7FFF7FFF7FFF7FFF7FFF7FE761
      2A7FE77EA57E627EAF6AF0390000FF7FFF7F202660328036A03A365F39679C73
      DE7B39673967396739676132A03A0000000000000000000000000000CB49937F
      1077CE6EAD6A0D5200000000000000000000000000000000CB49937F1077CE6E
      AD6A0E520000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      E761297FE67EF26E3242D756EF350000FF7F00000000BD7739678A3E603AAC46
      BA732022C046E042602E9C73B96F0000000000000000000000000000C030D57F
      727F30730F73A02C00000000000000000000000000000000C030D57F727F3073
      0F73812C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FE665977B123E5A673142D659B365FF7F0000000075636036004F40536036
      BD779C73402E204F404BAD4A00000000000000000000000000000000E1348449
      2A5ACF6A6C5EA12C00000000000000000000000000000000E13484492A5ACF6A
      6C5EA12C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F123EDE7730425966F759F565FF7F000000000000803A0053204F8053
      CF4AAD4AE046204F204BA03A5A6B00000000000000000000000000000239E655
      295EEA552539C230000000000000000000000000000000000239E655295EEA55
      2539C2300000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F103EFD769A6A376AFF7FFF7F000000000000986BA03EE0428436
      603AA046205300470F53873A422E00000000000000000000000000006741C455
      275EE75524396741000000000000000000000000000000006741C455275EE755
      243967410000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F386E376AFF7FFF7FFF7F0000000000000000BA6F0E4FA93E
      402A803A8036613200000000000000000000000000000000000000009B774441
      63454341443D9B77000000000000000000000000000000009B77444163454341
      443D9B7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FE7F186318631863FE7F00004539253925394539453945394539
      45394539453945392539253925394539FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000000000000000000000000000
      00000000FE7F88420022002200228842FE7F0D4E2E52CA458841884188418841
      8841884188418841AA452E522E520D4EFF7F3967396739673967396739673967
      3967396739673967396739673967396739670000000000000000000000000000
      000000009C73CD5DC454C454C454CD5D9C730000000000000000000000000000
      0000000088428032004390630043803288424F52663D4F56CA45884188458845
      8845A845A84588418841CA4545394F52FF7F5542343E343E343E343E343E343E
      343E343E343E343E343E343E343E343E55420000000000000000000000000000
      00000000AC5DE560877D877D877DE560CC5D7863186318631863186318631863
      1863186300220043004300000043004300224539883D8841A845A945EA4D8E5A
      D56FEB4DA949A949A945884188414539FF7F343E000000000000000000000000
      00000000000000000000000000000000343E7B6F396739673967396739673967
      396739678358887D877D677D877D887DC3541053104310431043104310431043
      1053105300229063000000000000906300222539A945A945A949C949B56F536B
      5267B56F3267C949C949A945CA452539FF7F343E0000734ED55A9352314A9452
      9452945293527352B4569452734E0000343EF152CD46CD46CD46CD46CD46CD46
      CD46EE466254F57E000000000000F57EA3549042786310631053106310631063
      10631863002200530053000000530053002225352D52A949CA4DCA4DEB4D6F5A
      F162B56F956FCA4DCA4DC9494E562535FF7F343E000000000000000000009C73
      00009C7300000000FF7F000000000000343EAD46D96F115BF052325F325F325F
      325F535F6254CB7DCB7DCA7DCB7DEB7DA3549042786388428832786378639063
      1053105388328032805390738053803288324539705A0C52EB4DEB51B66FB673
      3367B05E2D56EC51EB4D0C524E56A945FF7F343E0000774A3B67774E3B67774E
      3B67774E3B67FA5E56465646FA5E0000343EAC42D96F8B3E4936B767B667535B
      115331538851E5640D7E2E7E0D7E0665A84D9042786310537863786378639042
      786378631053084300220022002208439042BC77463DD4622D560C52546BB773
      756F966F336B0C522E56D466663DBC77FF7F343E0000564AFF7F574AFE7B574A
      FE7B574ADE77574AFF7FFF7F564A0000343E8C42D973CE4A966796679667AD46
      96639663EE46EB5D6254625461540D62CD3E8842FE7F90427863883278639042
      7863786390427863904278631053FE7F9042FF7F1667463DF5664F5A2E56B162
      766FD3662E5A4F5AF56A463D1667FF7FFF7F343E0000574AFE7F574ADE7B574A
      DE7B574ABD77574AFE7FFE7F5646FF7F343E8B3EDA73AD4695674A369567AD46
      96639663CE46B6678B36D663EE46FA738C3E8842FE7F90427863786378639042
      7863786390427863786378639042FE7F8842FF7FFF7F1667663DF56A4F5A505E
      AF660E525277166B673D1667FF7FFF7FFF7F343E0000574A1A63774A1A63774A
      1A63774A1A63F95A56465646D95AFF7F343E8B3EDB77AC429463746374638C3E
      966396638C3E946394639563AD42DB778B3E8842FE7F88428842906390631053
      1053104310539063906388428842FE7F8842FF7FFF7FFF7FD462EB4D386F176B
      B47FB062757BCB49D462FF7FFF7FFF7FFF7F343EFF7F9C735B6B9C735B6B9C73
      5B6B9C735B6BBD77BD77BD77BD77FF7F343E6B3ADB776A3A8B3E73637363F052
      EF4ACF46F052736373638B3E6A3ADB776B3A8842FE7F08320832884288428842
      8842884288428842884208320832FE7F8842FF7FFF7FFF7FFF7F673D88412739
      CF66737B0B520D4EFF7FFF7FFF7FFF7FFF7F343E0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000343E6A3AFC7B282E28326B3A6B3A6B3A
      6B3A6B3A6B3A6B3A6B3A2832282EFC7B6A3A8832FE7FFE7FFE7FFE7FFE7FFE7F
      FE7FFE7FFE7FFE7FFE7FFE7FFE7FFE7F8832FF7FFF7FFF7FFF7FFF7F0E4E7377
      947BB47F1567FF7FFF7FFF7FFF7FFF7FFF7FD856343E343E343E343E343E343E
      343E343E343E343E343E343E343E343ED8564A36FC7BDC7BDC7BDC7BDC7BDC7B
      DC7BDC7BDC7BDC7BDC7BDC7BDC7BFC7B4A361053105378637863786378637863
      786378637863786378637863786310531053FF7FFF7FFF7FFF7FFF7F463DEB4D
      505AEC51EC45FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000F14E115396679567956795679567
      95679567956795679567956796671153F14E0000105388328832883288328832
      883288328832883288328832883210530000FF7FFF7FFF7FFF7FFF7F596F925A
      0D4E67414639FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000D14E4A364936493649364936
      4936493649364936493649364A36D14E0000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000396739673967396739673967
      3967396739673967396739673967396739670000000000003967396700000000
      0000000000000000000000000000000000000000000039673967000000000000
      000000009C733967396739679C7300000000075607560756FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FB84A55095405550536012576266A
      076A076A076A076A076A076A076A076A276A000000000000666E876E5A6B0000
      00000000000000000000000000000000000000000000666E876E5A6B00000000
      00009C73CA46602E602E602ECA469C730000E755CD6AE755FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F760D7B2EBC3A9C369D32266E487F
      697F697F697F687F697F697F697F497F276E000000000000CC72666E886E3967
      FF7F0000000000000000000000000000000000000000CC72666E886E3967FF7F
      0000CA468036E0426E63E0428036CA460000E755CD6AE655FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF8191D4B9C327C2E7D2A563E4672
      6A7F6A7F6A7F0E1D6A7F6A7F6A7F67728F5A000000000000767B666E0C77666E
      1767FF7F000000000000000000000000000000000000767B666E0C77666E1767
      FF7F602A0047E0420000E0420047602E0000E755CC6AE659FF7FFF7FFF7FFF7F
      FF7FDE7B39670000000000007B6FDE7BFF7F39265E5FDE3AFE3EFF7FDF772B62
      097B8B7F8B7F0C6F8B7F8B7F0A7F276206320000000000000000666E507B917F
      666E1767BD77000000000000000000000000000000000000666E507B917F666E
      396760268E670000000000008E67602E0000E655CC6AE45D7B6FDE7BFF7FFF7F
      FF7FFA4E5C163C123C123C129B2EFA569C733A227E639F67FF77FF7FDF7B7C6B
      276E8C7FAC7F2D21AD7F8C7F066AED5206360000000000000000CD72EA76D57F
      6F7F666EF26ABD7700000000000000000000000000000000CD72EA76D57F6F7F
      68726026204B204700002047204F602E0000E655AC6A3D0EBB32FA5200000000
      00005C163C123C127F477F43FF2A7D1ABB36FF7B9B32FD4215678B62E74D1042
      9552C976CE7F2D21CE7FC9766652725F063A0000000039673967F46AA872D77F
      907F6E7B666EF36A9C730000000000000000000039673967F46AA872D77F907F
      6F7F624AA032404FAE6B404FA03A0D530000E655AB723E0A9F167D163C123C12
      3C123C125D163B0E7F435F3F1F2F3F3F3C12FF7FFF7FBF73B2568C622A56CC35
      354F6A6E8E7F2D21AE7F6A6A935F7363263E00000000676EA972A8728772D77F
      8F7F8F7F6E7B8872D06A7B6F0000000000000000676EA972A8728772D77F8F7F
      8F7F6E7F834A6026602A602E0D5300000000E655AA723D0A3F3BFF261F333F37
      3F3B5F3B5F471B0E7F435F3F1F2B5F473C12FF7FFF7F3867EF720F73AE6A4B5E
      4846894E6872706F68720A4AED528A4AFE7F00000000666ED37F907F8E7F6D7B
      6D7B6D7B8E7F6E7F8872AD6A7B6F000000000000666ED37F907F8E7F6D7B6D7B
      6D7F8E7F6E7F8876AF6E7B6F000000000000E655AA6E3D0A5F43DF221F2F3F3B
      7F475F3B7F4B1B0A7F435F3F1F2B7F533C0EFF7FFF7FE234B47F517BEF6ECE6A
      0539FE7B295E89722862E841BB77FF7FFF7F000000008B6E707B917F6D7B6D7B
      D77FD77FD67FD57FD57FA872AD6E0000000000008B6E707B917F6D7B6D7BD77F
      D77FD67FD57FD57FA872AD6E000000000000E655AA6E3D067F4FDF221F2F3F37
      7F433F377F531B0A7F435F3BFF2A9F5B3C0EFF7FFF7FE134AD66CF6A3177F072
      80285A6B1073CE6ECE6A6B5E3867FF7FFF7F0000000013770D77B37F4B7B4C7B
      C872466E466E466E676E676E686E00000000000013770D77B37F4B7B4C7BC872
      466E466E466E676E676E686E000000000000E655AA6E3D067F57DF1E1F2F3F37
      7F433F379F5B1B0A9F575F3BFF26BF633C0EFF7FFF7F0239C555295AEA51263D
      A12C453DD57F527BF06ECE6AE334FF7FFF7F00000000BB7FA972B67F6C7B4A7B
      B57FCC72D16AFF7F000000000000000000000000BB7FA972B67F6C7B4A7BB57F
      CC72D16AFF7F000000000000000000000000E655AA6E3D069F5FDF1EFF2A3F37
      7F433F37BF631B0ABF6FDF73DF73DF733C0EFF7FFF7F0239E555285EE8552439
      2539E134AD66CF6A31771073A12CFF7FFF7F000000000000676EB57F707B297B
      707B957F476E5A6B0000000000000000000000000000676EB57F707B297B707B
      957F476E5A6B000000000000000000000000E655AB6E3E06FF777F535F4B5F43
      7F4B5F43FF731C0A3C129C269D2A9F673C0EFF7FFF7F9B77223D834943410339
      FF7F0239E555295AEA51463DC230FF7FFF7F0000000000008B6E717BB57F277B
      287BD77FED728B6ADE7B0000000000000000000000008B6E717BB57F277B287B
      D77FED728B6ADE7B00000000000000000000E655CB6E3E06BE2A3F439F5FFF73
      FF77FF737F573C0EFF7FBF6B7E5BBD2E3C12FF7FFF7FFF7FFF7FFF7FDE7BFF7F
      FF7F0239E555285EE8552439C234FF7FFF7F00000000000013730D77FA7FD97F
      D87FFA7FD87F686EF46A00000000000000000000000013730D77FA7FD97FD87F
      FA7FD87F686EF46A00000000000000000000AE62E559D35A5E539D263C0E3C0E
      3C0E3C0E3C121E47FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F9C77223D6349434102399B77FF7FFF7F000000000000BA7B686E476E476E
      476E476E686E686E696E000000000000000000000000BA7B686E476E476E476E
      476E686E686E696E000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000000000000000000000000000
      0000000000009C733967396739679C7300000000000039673967396739673967
      3967396739673967396739673967000000006072607240724072407260766076
      607640724072407240724072407260726072000039673967BD779C7339673967
      3967396739670E6A2665276527650E669C730000000000000000000000000000
      000000007B6FCA46602E602E602ECA469C7300000000F231D129B129B129B129
      B129B129B129B129B129D129F231000000006172677B677B667B867BC762C935
      A67F867B667B667B667B667B677B877B617200008876887A136F184A152DF52C
      F52CF6280C51466D667D667D677D47690E667B6F396739673967396739673967
      39673967A9428036E0426E63E0428036CA4600000000D1290000000000000000
      000000000000000000000000D12900000000307BC376687B467B467BA7628A08
      A835667F467B467B467B467B687BC376307B0000A87AB07F3435162D572D772D
      772D7829246D877D877D677D877D887D26655726F605F605F605F605F605F605
      F705F901602E0047E0420000E0420047602E00000000B1290000FF7FFF7F0000
      D515D5150000FF7FFF7F0000B12900000000FF7F4072497B477B467B667F667F
      C910C666467F467B267B467B497B4072FF7F00007677343537297831DA39FC3D
      FC3D1D3A0469F47E000000000000F57E2665F605FE7FDE7FDE7FDE7FDE7FDE7F
      DE7FFF7F402A8E670000000000008E67602E00000000B1290000FF7FFF7FD515
      FE3AFE3AD515FF7FFF7F0000B12900000000FF7F307BA3764A7B06778662C66A
      8835C9100673257F257B4A7BA476307BFF7F0000184A372DB835FB3D3C67FD7F
      B356FE7FC364CB7DCA7DCA7DCA7DEB7D2665F605FE7FBD77BD77BD77BD77BD77
      000000004026204B204700002047204F602E00000000B1290000FE7FD515FD36
      FD3AFD3AFD36D515FE7F0000B12900000000FF7FFF7F61722A7B2877C814A90C
      A90C8908E818E576287B2A7B6172FF7FFF7F0000362DB83DFB3D3B67FB7FFC7F
      FC7FFD7F4F72476D0D7E2E7E2D7E686D5072F605FE7F9C73734E734EBD73F85E
      000000002D53A036404FAE6B404FA03A0D5300000000B1290000D5111E431E43
      1D43FD363E471E43D5110000B12900000000FF7FFF7F757B83766E7F27568808
      A73D457F257F067B4E7F8376757BFF7FFF7F0000162D3A4A1C42FA7FFB7F2A21
      366BFB7FFC7F4E72E4640469256971720000F605FE7F9C739C739C739C739452
      945294529656673A40264026602A4A1E000000000000B1290000983AD619F619
      3D4FBC2EF61DF619983A0000B12900000000FF7FFF7FFF7F61722B7B6C7FE71C
      88082756267F2B7B2C7B6172FF7FFF7FFF7F000016297B521D42704EFA7F566B
      4B251567FA7F714E1F3E7D4E182100000000F605FE7F7B6F7B6F7B6F7B6F7B6F
      7B6F7B6F7C6F7D737D739E730000F905000000000000B1290000BD7BDD7FD615
      7D5B3D53F619DD7FBD7B0000B12900000000FF7FFF7FFF7FB97F6176937F8966
      6704870CA96E937F8276B97FFF7FFF7FFF7F00001629BC5A1D42F87F356B4B29
      3567D97FD87FF87F1D42BD5A172500000000F605FE7F5B6F7B6F7B6F5B6BBD77
      0000000000000000BE775B6FFF7FF705000000000000B12900009C739C77983A
      D615D615983A9C779C730000B12900000000FF7FFF7FFF7FFF7FA5762D7B927F
      E7202600C9140C77A576FF7FFF7FFF7FFF7F7B6F36299C565D4A3A674B293467
      D87FD77FD67F19635D4A9C5636297B6F0000F605FE7F5A6B3246524A5A6B185F
      0000000000000000185F5A6BFE7FF605000000000000B1290000000000000000
      000000000000000000000000B12900000000FF7FFF7FFF7FFF7FB97F8172967F
      4E7F4E7FD87F8276B97FFF7FFF7FFF7FFF7FCE76EF51B939FE663E461967D67F
      0C46D57FF9621D42FE66B939EF51CE760000F605FE7F39673A673A673A679452
      935273527352935293523967FE7FF6050000000000003726381E171E171E171E
      171E171E171E171E171E381E162600000000FF7FFF7FFF7FFF7FFF7FEB760B7B
      977F967F0D7BEB76FF7FFF7FFF7FFF7FFF7FA97A0A7F75313B4A3F6F9E521E42
      1E421E427E523F6F3B4A75310A7FA97A0000F605FE7FDE7FDE7FFE7FFE7FFE7F
      FE7FFE7FFE7FFE7FFE7FFE7FFE7FF60500000000000058225E4FFD36FD3AFD3A
      FD3AFD3AFD3AFD3AFD365E4F582200000000FF7FFF7FFF7FFF7FFF7FFE7F6072
      B97FB97F6072FE7FFF7FFF7FFF7FFF7FFF7FCA7A4C7F4D7F9531DA3D3F6B9F7F
      9F7F9F7F3F6BDA3D95314D7F4C7FCA7A0000F6099E63BB26BB26BB26BB26BB26
      BB26BB26BB26BB26BB26BB269E63F60900000000000058261C4B9A2A9A2A9A2A
      9A2A9A2A9A2A9A2A9A2A1C4B582600000000FF7FFF7FFF7FFF7FFF7FFF7FEA76
      0E7B0E7BEA76FF7FFF7FFF7FFF7FFF7FFF7F987F0B7BCF7FEF7F3052562D5629
      56295629562D3052EF7FCF7F0B7B987F0000170A5D533D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F5D53170A0000000000005826FB46FB46FB46FB46
      FB46FB46FB46FB46FB46FB46582600000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      60726072FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000987FEA7A097F707F00000000
      000000000000707F097FEA7A987F00000000B932170A160A160A160A160A160A
      160A160A160A160A160A160A170AB932000000000000792A5826582658265826
      582658265826582658265826792A000000000000000000000000000000003967
      3967396700000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      00000000000000000000000000000000000000007B6F39673967396739673967
      39673967396739673967396739675A6B0000000000000000000000000000AB3D
      89414A6E3967000000000000000000000000453A453A453A4536433614361436
      14361436143643364536453A453A453AFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000D65A734E734E734E734E734E
      734E734E734E734E734E734E734EB55600000000FF7FBD773967396739670B52
      905A527FA64D39673967BD77FF7F000000000532C542C542A43EE229FF77E461
      2766E461FF77E229A43EC542A53E0532FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F734E00000000DE7BD65A314610421142C87A
      917F507F627EA64D5342D65ADE7B00000000DD77EA2D222E4C3E5B63DF73A059
      2866A059DF735B634C3A222EEA2DDD77FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EDE7BDE7B3146AD35DE7B
      3146AD35DE7F7C2A7C2ADD7BDE7B734E000000005A6B734EDE77BE777B6B7D6B
      C465087FA57E627EA64D954E5A6B00000000FF7F353ADF77BF6FBF6FDF6F4055
      27664055DF6FBF6FBF6FDF77353AFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EDE7BBD77DE7BDE7BDE77
      DE7BDE7BDD7BBD329C2EBD7BDE7B734E00000000F75EB556BD7711425B6B3142
      7D67C561287FA57E627EA551D65A00000000FF7F343ADF779E6BBE6F12323436
      343634361232BE6F9E6BDF77343AFF7FFF7F0000000000000000000000000000
      39673967DE7B0000000000000000000000000000734EDE7BBD733146AD35BD77
      3146AD35BD7BBE32BE32BC77DE7B734E00000000B556F75A5B6B3A6731465A6B
      32465C67E565087FA47ED06AEF3939670000FF7F333ADF77D852F952F9561A57
      1A571A57F956F952D852DF77333AFF7FFF7F0000000000000000000000003967
      8C318C31F75E0000000000000000000000000000734EBE779C739D73BD779D73
      9D77BD779C739C779C779C73BE77734E00000000524A18633967524639675246
      3A6752463B63C465757B3242F85EEE353967FF7F343ADF779E6BF9529E6FF952
      9E6FF9529E6FF9529E6BDF77343AFF7FFF7F0000000000000000000039678C31
      B556B5568C313967000000000000000000000000734EBD777C6F3246AE359C73
      3246AE359C733246AE357C6FBD77734E0000000031463963524A1963524A1963
      524A1963534A3963103EBD733142F759B365FF7F343EDF77D852F952F952F952
      F952F952F952F952D852DF77343EFF7FFF7F000000000000000039678C31F85E
      D65AD65AF85E8C31396700000000000000000000734EBD777B6B7C6B7C6B7C6F
      7C6F7C6F7C6F7C6B7C6B7B6BBD77734E00005A6B104295529452945294529452
      945294529452955295520F3EDC725966F669FF7F543EDF777D67D9529E6BF952
      9E6BF9529E6BD9527D67DF77543EFF7FFF7F00000000000000008C317B6F9D73
      9C739C739D737B6F8C3100000000000000000000734EBD775B67E37D847D7B6B
      3246AE357B6B047E847D5B67BD77734E00003146BD777B6F7B737B6F7B6F7B6B
      7B6B7B6B7B6F7B6F7B739B6F176E186E0000FF7F543EFF7BD84ED952D952D952
      D952D952D952D952D84EFF7B543EFF7FFF7F000000000000000031468C318C31
      8C318C318C318C313146000000000000000000009452DE7BBE77DE77DE77DE77
      DE7BDE7BDE77DE77DE77DE77DE7B945200001042BC77BA32B801186B1863185F
      185F185F1863186BB801BA32BC7730420000FF7F553EFF7B7D67D9527D67D952
      7D67D9527D67D9527D67FF7B553EFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000009452EF3DEF41EF41EF41EF41
      EF41EF41EF41EF41EF41EF41EF3D94520000B556BC77BF577B1AD8019B7B9C73
      9C739C739B7BD8017B1ABF57BC77524A0000FF7F5542FF7BD84ED852D852D852
      D852D852D852D852D84EFF7B5542FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000094523F471F3F1F3F1F431F43
      1F431F431F431F431F3F1F3F3F47945200000000B5560F46BF5B7B1AD8010F4A
      10460F4AD8017B1ABF5B0F46B55A00000000FF7F7542FF7F5C63D84E5D63D84E
      5D63D84E5D63D84E5C63FF7F7542FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000093525F539D329C32BC32BC32
      BC32BC32BC32BC329C329D325F53935200000000000000000000BF5B7B1AD901
      0000D9017B1ABF5B00000000000000000000FF7F7642FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F7642FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000093527F5B7F5F7F5F7F5F7F5F
      7F5F7F5F7F5F7F5F7F5F7F5F7F5B9352000000000000000000000000BF5B7B1E
      00007B1EBF5B000000000000000000000000FF7F195B76467646764276427642
      764276427642764276467646195BFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000B55693529352935293529352
      9352935293529352935293529352B55600000000396739673967396739673967
      396739670000FF7FFF7FFF7FFF7FFF7FFF7F00000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F000000000000FF7F000000007B6F3967396739673967
      39673967396739673967396739677B6F00000000000000000000000000000000
      000000000000000000000000000000000000B84A550554055405540554055405
      54055505B84AFF7FFF7FFF7FFF7FFF7FFF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000F75ED656D656D656D656
      B556B556B556B556B556B556B556F75E00000000000000000000000000000000
      000000000000000000000000000000000000760D7B2EBC3ABC3A9C3A9C3ABC3A
      BC3A7B2E960DFF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D65600000000307A0000
      0000000000000000000000000000D65600000000000000000000000000000000
      000000000000000000000000000000000000F8191D4B9C327C2E7C2E7C2E7C2E
      9C321D4FB711FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000B55600000000E6700000
      0000000000000000000000000000B556000000000000000039677B6F00000000
      00000000000000000000000000000000000039265E5FDE3AFE3EFE7FBD7BFE3E
      FE3E7E63D815FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C5FDA46845CDA46
      B846B846B846B846B846B8465C63D55600000000000039672026653600000000
      0000000000000000000000000000000000003A227E639F67FF77FF7FDE7B9E6B
      3D535D5F1A22FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000000007710000
      0000FF7FFF7FFF7FFF7FFF7F0000D55600000000396700226A5B202239673967
      396739673967396739673967396739677B6FFF7B9C32FD4215676B62E7512E46
      F71D1726D952BD77DE7BFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63DA46845CFA46
      B84AB84AB84AB84AB846B8465C63D5560000396700224C5B6053E03E00222022
      20222022202220222022202220222026873AFF7FFF7FBF73B2568C622A560E42
      BA42FF73B93EB84AF95A39673967BD77FF7FF2310000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D5560000FF7F0671FF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000D556000020266E5F204F204F204F204F4053
      405340534053405340534053405340532026FF7FFF7F3867EF720F73AD6A4962
      954ADA42FF7FBA42993A16221726D84EBD77F231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000D5565C63DA46845CFA4A
      B84AB84AB84AB84AB84AB8465C63D556000020267067004F004F6C638C678C67
      8C678C678C678C678C678C678C678C6B2026FF7FFF7FE234B47F517BEF72714E
      792E7E5FFF73FF73FF77FF779E637936D8520000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5560000FF7BE670FF7F
      FE7FDE7FDE7FDE7FDE7FDE7B0000D556000000000022936B004FA03E00220022
      20222022202220222022202220222022C942FF7FFF7FE134AD66CF6E307B3826
      9F67DF6FDF6FDF6FDF6BDF6BDF6FBF67372AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C63DA46845CFA4A
      B94AB84AB84AB84AB84AB8465C63D5560000000000000022946F202200000000
      000000000000000000000000000000000000FF7FFF7F0239C555295EC8553922
      FF73BF67BF6BBF6BBF6BBF67BF67FF733826F231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000FF77E66CFF7B
      DE7BDD7BDD7BDD7BBD7BBD7B0000D55600000000000000002026643200000000
      000000000000000000000000000000000000FF7FFF7F0239E555275EC5555A26
      DF739F63BF67BF67BF67BF679F63DF73382A0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63D946845CDA46
      B84AB84AB84AB84AB84AB8465C63D55600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7F9C77223D63492141392A
      BF67BF679F639F639F639F63BF639F67392AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000B5560000BD73C568BD73
      9C739C739C739C739C739C730000B55600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F7E67
      9B3A9E67DF73DF73DF73DF739E679A3A7D6300000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D65600000000317A0000
      0000000000000000000000000000D65600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      7E637A32592E592E592E592E7A327D63FF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231FF7FFF7F00000000F75AD656D656D656D656
      B556B556B556B556B556B556D656F75A00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000396739673967396739670000
      FF7FFF7F00003967396739673967396700000000000000000000000000000000
      0000000000000000000000000000000000000000396739673967396739673967
      39673967396739673967396739675A6BFF7F0000DE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7B7B6FCA46602E602E602ECA469C73524ACE39AE35AD358D318C311042
      FF7FFF7F524ACE39AE35AD358D318C3110423967396739673967396739673967
      3967396739673967396739673967396739678C66476627664766476A466AAB39
      89412A6A466A4666266626662666696AFF7F5A6B195F195F195F195F195F195F
      195F1A5FC9468036E0426E63E0428036CA46EF39F03D734A1142AE354B296C2D
      FF7FFF7FEF39F03D734A1142AE354B296C2D216C006C006C006C006C006C006C
      006C006C006C006C006C006C006C006C216C47666E7F4B7F297BA772CA760C4E
      905A517F8649B07F8F7F8F7F907F2666FF7F993A993279327932793279327932
      79329C32602EE046E0420000E0420047602ECE3939675B6B3A67396318636B2D
      FF7FFF7FCE393A675B6B3A67396318636B2D7B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF7B7B5B27664B7F097BA772E77A2666C976
      917F507F627E86498F7F6E7F907F2666FF7F9932FF7BFF77FF77FF77FF77FF77
      FF77FF7B402A8E670000000000008E67602ECE39D6563963B6565246CE396B2D
      39673967CE39D6563963B6565246CE396B2D0070237DC07CC07CC07CC07CC07C
      C07CC07CC07CC07CC07CC07CC07C237D00704766297BA772E776487F26668E7F
      C661087FA57E637E86458E7F907F2666FF7F7932FF773C539E63DF6FDF6FDF6F
      DF6FFF734026204B004700002047204F602ECE39D6563963B656524ACE396B2D
      EF398C31CE39D6563963B656524ACE396C2D7B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF775B5B4766A772E77A487F6C7F06666D7F
      6D7FC65D097FA57E627E8649B17F466AFF7F7932FF77FF77DB421C4BDF6FDF6F
      DF6FFF730D4BA036404FAE6B404FA0368B32CE39D6563963B6565246CE396B29
      CE396C2DCE39D6563963B6565246CE396B2D0078877D807C807C807C807C807C
      807C807C807C807C807C807C807C877D00704766CB72266626660666886E6C7F
      4C7F6C7FC65D087FA47EB06AD031456EFF7F7932FF77BF6BFF7BDB42DF6FFB46
      582A592EFE4A0D4B402A4026402A0D4F9C36CE397B6FBD777B6F3967F75E4A29
      5A6B3146CE397B6FBD777B6F3967F75E6B2D7401FF73BC01DF63BC01DF63BC01
      DF63BD01FF7BFF77FF77FF77FF77FF775B5B4666AB726C7F6C7F6C7F4C7F4B7F
      4B7F4C7F6C7FC5617677323EF85AEF3539677932FF77BF67BF67FF7F1C4BFB46
      00000000FC461E4F0000DF6BDF6BFF779A32EF39AD358C316C2D6B2D6B2D3246
      5A6B3142744EAD358C316C2D6C2D6C2D524A72019D2A9F677901BF6B7901BF6B
      7901BF63407C407C407C407C407CCC7D00702666B27F4A7F4B7F4B7F4B7F4B7F
      4B7F4B7F4B7F6A7FF2399D733142F759B3657932FF77BF63BF633D4FDB420000
      BF67BF670000DB423D4FBF67BF63FF777932FF7FCE39F75E39679452EF3D4B29
      5A6B3146CE39F75A39679452EF3D6B2DFF7F5201FF7F57019E6B57019E6B5701
      9E6B5801FF7FFF77FF77FF77FF77FF775A5B2666B37F497F4A7F4A7F4A7F4A7F
      4A7F4A7F4A7F4A7F497FF139DC6E5962F6657932FF77BF637E57FB42FF7FBF63
      BF63BF63BF63FF7FFB427E57BF63FF777932FF7FCE39D75A1963944EEF3D6B2D
      CE396C2DCE39F75A1963944EEF3D6C2DFF7F5201DC429E6736019E6B36019E6B
      36019F63007C207C207C207C007C307E00702666B37F287F297F297F297F297F
      297F297F297F297F497F477F1966196AFF7F7932FF7B9F5BDB42FF779F5F9F5F
      9F5F9F5F9F5F9F5FFF77DB429F5BFF7B7932FF7FCF39F75E39679452EF3D6B2D
      EF398C31CF39F75E39679452F03D6C2DFF7F5201000014017E6714017E671401
      7E671501FF7FFF77FF77FF77FF77FF777B5B2666B47F917F917F927F927F927F
      927F927F927F927F917F917FD37F2566FF7F7932FF7BFC46DF739F5B9F5B9F5B
      9F5B9F5B9F5B9F5B9F5BDF73FC46FF7B7932FF7F9452AE358D318C2D6C2D3146
      FF7FFF7F734EAD358D318C318C31524AFF7F72011C5700001C5300001C530000
      1C53FF7FB37EB47EB47EB47EB57ED57E006C2666B57FC676C776C776C776C776
      C776C776C776C776C776C676D57F2666FF7F9932FF7FFF7FFF7BFF7BFF7BFF7B
      FF7BFF7BFF7BFF7BFF7BFF7BFF7FFF7F9932FF7FFF7F3967CF393963734E6C2D
      FF7FFF7FCF393963534A6C2D3967FF7FFF7F5726720152015201520152015201
      720174010074006C006C006C006C006CE7702666D67F267F277F277F277F277F
      277F277F277F277F277F267FD67F2666FF7F1B4F993279327932793279327932
      79327932793279327932793279329932DA42FF7FFF7FEF39324632468C316C2D
      FF7FFF7FCF39314632468D318C2DFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000002666B07F907F907F907F907F907F
      907F907F907F907F907F907FB07F2666FF7F0000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FEF39CE35AD358D316C2D
      FF7FFF7FEF39CE35AD358D318C2DFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000686A266626662666266626662666
      2666266626662666266626662666686AFF7F0000000000000000000000005A6B
      F75EF75E5A6B00000000000000000000000000000000000000000000F75EF75E
      F75EDE7B00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B6F
      396739673967396739673967396739677B6F00005A6BF75EF75EF75EF75E734E
      10423146734EF75EF75EF75EF75E5A6B00000000BD771863F75EF75E6B2D4A29
      3146D65A1863BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B6F39673967396739673967CB51
      24412541254125412541254125412541CB4D00001042AD35AD35AD35AD35524A
      B5563967F75E524AAD35AD35AD35104200000000524A0821E71CE71CEF3D3146
      5A6B8C310821524A000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000010428C318C318C318C318D312445
      4B5EE455E459C451C451E459E4554B5E2441F75EAD353967CE39AD35DE7BBD77
      1042F75E7B6FF75E104210423146EF3D7B6F00002925EF3D31463146D65A5A6B
      5A6B94528C312925000000000000000000000000000000000000000000000000
      00000000000000009C73F75E0000000000008C31AE35CF39CF398C31AE312441
      0656824D8249413D413D8249824D06562441EF3DF75E2925EF3D8C31BD777B6F
      9C731042F75E5A6BF75E18631863734E945200006B2D945210421042EF3DCE39
      1863B55694528C31F75E00000000000000000000000000000000000000000000
      0000000000000000524A8C31F75E00000000CE39744EEF398C314A294B292445
      275AA24DA24D087F087FA24DA24D275A24410000AD356B2D10428C31BD777B6F
      7B6F7B6FEF3D396739675A6B5A6B5A6B524A0000AD35F75E524A524AFF7FBD77
      CE391863D65A94528C31F75E0000000000007B6FF75EF75EF75EF75EF75EF75E
      F75EF75EF75EF75E8C3118638C31F75E00008C3118638C31CE39C61809210441
      8C62814D814D81498149814D814D8C622441F75EAD35396731468C31BD775A6B
      5A6B7B6FEF3D7B6F7B6FAD35AD359C73524A0000EF3D5A6B9452BD770000DE7B
      5A6BCE391863B556B5568C31F75E0000000010428C318C318C318C318C318C31
      8C318C318C318C31524AF75EF75E8C31F75E1863AD35103E1863324653460341
      8D5E6C5A6C5E6C5A6C5A6C5E6C5E8D5E2441EF3DF75E2925524A8C31BD773967
      DE7B000010425A6B7B6FCE39CE397B6F734E0000CE39D65A9C735A6B524ACE39
      3146BD77EF3D5A6BCE39B5568C31F75E00008C31F75ED65AD65AD65AD65AD65A
      D65AD65AD65AD65AB556B556B55618638C310000FF7FAE35BD7BC614C614CA51
      E23CE23C0341033DE33C033DE24003412D5A0000AD356B2D734E8C31BD773967
      0000B556734E94525A6B5A6B5A6BB5561863000000009452524AD65A3146AD35
      EF3D524AB556AD359C73CE39EF3DCE3900008C315A6B39673967396739673967
      396739673967396739679452945239678C3100000000FF7F366FAD6A4A5A156B
      DE7BBE77E140DF77DF77E13C000019020000F75EAD35396794528C31BD771863
      0000AD35D65A734E524A524A734E104200000000000000005A6B734ED65A734E
      EF3D9C7300000000CE39D65A524A10420000524A8C318C318C318C318C318C31
      8C318C318C318C31314694525A6B8C310000000000005A6F31771073AD6A8C66
      3E0F7F07CC2DC040C040AC250000F7010000EF3DF75E2925B5568C31DE7B1863
      DE7B000000000000DE7B3967DE7BAD3500000000000000008C317B6FF75EB556
      9452CE39000000000000524A3146000000000000000000000000000000000000
      00000000000000008C315A6B8C3100000000000000006741B57F517BEF72CD6E
      48215F235E1F5F1F5F1F5E1B0000F60500000000AD356B2DB5568C31DE7B1863
      186318631863186318631863DE7B8C310000000000000000C618DE7B5A6B1863
      F75E841000000000000000000000000000000000000000000000000000000000
      0000000000000000734E8C3100000000000000000000E134AD6AAE6A1173CE6E
      4028000000000000000000000000F6050000F75EAD35F75EB5568C31FF7FF75E
      18636B2D314631461863F75EFF7F8C310000000000000000E71C4A29EF3DB556
      3146A51400000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000239E555295EEA510439
      802C19639C730000BF577F230000F6050000EF3DF75E0821B5568C31FF7FF75E
      F75EF75EF75EF75EF75EF75EFF7F8C310000000000000000E71CCE391042CE39
      0821A51400000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000705AE455275EE755E234
      AB411963F75ED75E9E43DC020000F60500000000AD35BD779C738C310000FF7F
      FF7F00000000FF7FFF7FFF7F0000AD3500000000000000004A29AD35EF3DAD35
      0821292500000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000004E5A624921456F5E
      0000000000000000000000000000F60500000000524AAD35AD35AD35AD358C31
      8C318C318C318C318C318C31AD35524A00000000000000009C73292529252925
      08219C7300000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000DC2E1806
      F705F605F605F605F605F605F605B93200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000039673967
      3967DE7B00000000000000000000000000000000396739673967396739673967
      3967396739673967396739673967396700000000000000000000000000003967
      3967396739670000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000DE7B396739673967CA458941
      496E166B3967DE7B000000000000000000005722F605F605F605F605F605F605
      F605F605F605F605F605F605F605F6055722000000000000000000000000EF3D
      9C73D65A10420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000B84A5509550536012A566E5A
      517FA5515701B84A00000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6057B6F396739673967396739671042
      BD77F75A1042396739673967396739677B6F000000000000F75E396700000000
      000000000000000000000000000000000000000076095A2E9C369D32C77E4F7F
      507F627EA555990100000000000000000000F605FF7F744E3146524A5A6BFF7F
      18631863FF7F1863186318631863FF7FF6053146AD358D318D31AD31AD31AE31
      8D318D318D31AD35AD35AD35AD35AD35314600000000F75E8C31EF3D00000000
      0000000000000000000000000000000000000000D819FD467C2E7C2A7E22C365
      077FA57E627EA65139670000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF605AE3539671863185F5A6B954A0060
      954E744E744E744E744E744E744EB552AE350000F75E8C3118638C31F75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E5A6B000019263D57BD36BD36FF7FDF73
      C461087FA57E627EA6513967000000000000F605FF7F534A314231463967FF7F
      F75EF75EFF7FF75EF75EF75EF75EFF7FF605CE391863D65AD65A3967D7520060
      D752B552B552B552B552B552B552F75ECE39F75E8C31F75EF75E524A8C318C31
      8C318C318C318C318C318C318C318C31EF3D00005A2E7E67FE3EDF6FFF7FDF77
      9F5FC465087FA47ED06EEF39396700000000F605FF7FFF7FFF7FFF7FFF7FDE7B
      FF7BFF7BDE7BFF7BFF7FFF7FFF7FFF7FF605CF39185FB556B5523963185B0060
      195BF75AD75AD75AD75AD75AD75A3A67CE398C311863B556B556B556D65AD65A
      D65AD65AD65AD65AD65AD65AD65AF75E8C3100005A261D4FBF6B76778B62E751
      7056DF67E369757B3242F85EEE3539670000F605FF7F524610421142185FDE7B
      D65AD65ADE7BD65AD65AD65AD65AFF7FF605EF3D3967944E744E7B6B5A630064
      5A631963186318631863186319639C6FCF398C31396794529452396739673967
      39673967396739673967396739675A6B8C3100000000FD42BE32175F6C5EE84D
      5246BF321E471042BD733142F759B3650000F605FF7FDE7BDE7BDE7BDE77BD77
      BD77BD77BD77BD77DE77DE77BD77FF7FF605F03D5A6B534A524A7B6B9C670064
      9C675A675A675A675A673A677B6BBD77EF3D00008C315A6B945231468C318C31
      8C318C318C318C318C318C318C318C31524A0000000000007B6FAF62EF6EAD66
      4C569C73000000003042DC725962F5650000F605FF7F3146F03D103EF75ABD73
      B556B556BD73B556B556B556B556FF7FF605103E9C73524A3246DE77DE6F0064
      DE6F9C6F7C6F7C6F7C6F9C6FDE77BD77F03D000000008C315A6B8C3100000000
      000000000000000000000000000000000000000000000000CB49937F1077CE6E
      AD6A0D52000000000000386E376A00000000F605FF7F9C73BD77BD739C739C6F
      9C739C739C6F9C739C739C739C73FF7FF605B656D65A9C73DE7BFF7FFF7B0068
      FF7BDF7BDE7BDE7BDE7BDE7BDE7BB656B6560000000000008C31EF3D00000000
      000000000000000000000000000000000000000000000000C030D57F727F3073
      0F73A02C0000000000000000000000000000F605FF7FF03DCE39CF39B5567B6F
      944E944E7B6F9452945294527352FF7FF6050000534A3146314231425342006C
      7342314231423142314231423142524A00000000000000000000000000000000
      000000000000000000000000000000000000000000000000E13484492A5ACF6A
      6C5EA12C0000000000000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6050000000000000000000000000070
      3967396739677B6F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000239E655295EEA55
      2539C2300000000000000000000000000000F6097E5B7B1A7B1E7B1E7B1E7B1E
      7B1E7B1E7B1E7B1E7B1E7B1E7B1A7E5BF6090000000000000000000000000070
      0070007400740871000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000006741C455275EE755
      243967410000000000000000000000000000170A5D4F3D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F3D4F5D4F170A0000000000000000000000000070
      00700068005C0070000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000009B77444163454341
      443D9B770000000000000000000000000000992A170A160A160A160A160A160A
      160A160A160A160A160A160A160A170A992A0000000000000000000000000070
      00700070006C297500000000000000000000424D3E000000000000003E000000
      2800000040000000B00000000100010000000000800500000000000000000000
      000000000000000000000000FFFFFF0080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      0600000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFF08040FC0FFFFF000000000003FFFF
      0000000100000000000000000000000000000000000000000001000000000000
      0003000000070000000300003807000000030000F807000000030000F8070000
      02130000F807000000C30000F807000000030000F807000000030000F807FFFF
      00030400F807FFFF00030000F807FFFFF000FFDFFFFFFF8F0800FF9FE0078007
      04008081E007800302000001E007800101000001E007800100800081E0078001
      00400391E0078001002003C1E0078003001003E1F00FC007000807E1F81FE00F
      00040003F81FE07F0002C003F81FE07F0000C007F81FE07F0000E003F81FE07F
      0000E003F81FE07F0000F01FF81FE07FFFFFFFFEFFFFFFFFFFC10000FFFFFFC1
      FF8000000000FF80FF8000000000FF80000800007FFE0000001C00004002001C
      000800007D6E0000000000004002000000000000400200000000000040000000
      0000000040000000000000000000000000000000400200000000000000000000
      00000000FFFF000080010000FFFF8001E0008000E7FFCF8300000000E3FFC701
      00000000E0FFC10100000000E07FC01100380000F03FE03900000000F01FE011
      07000000C00F800100000000C007800300000000C003800700000000C0038007
      00000000C003800700000000C01F803F00000000E01FC03F00000000E00FC01F
      00000000E00FC01F00000000E00FC01FFFFFFFC1FFC1C00300008000FF80C003
      000080000000DFFB000080000008D24B0000801C001CD00B000080000188D00B
      000080000180D00B000080010001D00B000080030005D00B0000800301E1D00B
      0000000101E1DFFB000000010001C003000000010001C003000000010001C003
      000000010001C003000087C30001C003FC7FFFFEFFFF8001FC3F0000FFFF8001
      80030000FFFF800180030000FFFF800180030000FFFF800180030000FE3F8001
      80010000FC3F800180000000F81F800180000000F00F800100000000F00F8001
      00010000F00F800100010000FFFF800100010000FFFF800180030000FFFF8001
      F11F0000FFFF8001F93F0000FFFF80018040DB6EC001FFFF00000000C001FFFF
      00000000DBFDFFFF00008002DBFDE7FF00000000C001C7FF00000000DB058000
      00008002C001000000005B6CD005000000000000C001000000008002D0058000
      00000000C001C7FF00000000D005E7FF00008002C001FFFF00000000D005FFFF
      0000DB6CDBFDFFFF00000000C001FFFFFFC18241FFFF80008000000000000000
      00000000000000000008000000000000001C0000000000000008000000000000
      0000000000000000000000000000000001900000000000000240000000000000
      00000000000000000000000040000000000000002A0000000000000000000000
      00000000FFFF0000FFFF0000FFFF0000FC3FF87FFFFFFC008001801FFFFF0000
      8001801FFFFF00000000801FFFE700000000800FFFE300008000800700010000
      000084030000000000808001000080008100C0010000C0050101E0610001C005
      00E1E073FFE3C0058001E07FFFE7C0FD0001E07FFFFFC0250001E07FFFFFC005
      84C5E07FFFFFE1FD8001E07FFFFFF801FFFFF87F8001FC3FFFFF801F0000FC3F
      FFFF801F00000000E7FF801F00000000C7FF800F000000008000800700000000
      000080030000000000008001000000000000C001000000008000E06100000000
      C7FFE07300000000E7FFE07F00008001FFFFE07F0000FC1FFFFFE07F0000FC1F
      FFFFE07F0000FC1FFFFFE07F0000FC1F00000000000000000000000000000000
      000000000000}
  end
  inherited dsCL: TevDataSource
    Top = 71
  end
  inherited DM_CLIENT: TDM_CLIENT
    Top = 71
  end
  inherited DM_COMPANY: TDM_COMPANY
    Top = 66
  end
  inherited wwdsEmployee: TevDataSource
    Left = 182
    Top = 16
  end
  inherited wwdsSubMaster2: TevDataSource
    Left = 509
  end
  inherited EEClone: TevClientDataSet
    Left = 570
  end
  object dsBenefitPmnt: TevDataSource
    DataSet = DM_EE_BENEFIT_PAYMENT.EE_BENEFIT_PAYMENT
    MasterDataSource = wwdsEmployee
    Left = 128
    Top = 160
  end
  object dsDependent: TevDataSource
    DataSet = DM_CL_PERSON_DEPENDENTS.CL_PERSON_DEPENDENTS
    OnDataChange = dsDependentDataChange
    MasterDataSource = wwdsSubMaster2
    Left = 688
    Top = 272
  end
  object evProxyDataSet1: TevProxyDataSet
    Left = 724
    Top = 136
  end
  object evdsCOBenefitSubtype: TevDataSource
    DataSet = DM_CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE
    Left = 696
    Top = 192
  end
  object dsCOBenefits: TevDataSource
    AutoEdit = False
    DataSet = DM_CO_BENEFITS.CO_BENEFITS
    Left = 736
    Top = 216
  end
  object evPopupMenu1: TevPopupMenu
    Left = 736
    Top = 296
    object est11: TMenuItem
      AutoCheck = True
      Caption = 'Test 1'
      Checked = True
      RadioItem = True
    end
    object est21: TMenuItem
      AutoCheck = True
      Caption = 'Test 2'
      RadioItem = True
    end
    object est31: TMenuItem
      AutoCheck = True
      Caption = 'Test 3'
      RadioItem = True
    end
    object est41: TMenuItem
      AutoCheck = True
      Caption = 'Test 4'
      RadioItem = True
    end
  end
  object dsBenefitAll: TevDataSource
    DataSet = cdBenefitAll
    OnDataChange = dsBenefitAllDataChange
    MasterDataSource = wwdsSubMaster
    Left = 632
    Top = 288
  end
  object cdBenefitAll: TevClientDataSet
    OnCalcFields = cdBenefitAllCalcFields
    Left = 752
    Top = 320
    object cdBenefitAllEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdBenefitAllCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
    end
    object cdBenefitAllEE_BENEFITS_NBR: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object cdBenefitAllCO_BENEFITS_NBR: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object cdBenefitAllCO_BENEFIT_SUBTYPE_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_SUBTYPE_NBR'
    end
    object cdBenefitAllCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
    object cdBenefitAllCUSTOM_E_D_CODE_NUMBER: TStringField
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
    end
    object cdBenefitAllCL_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'CL_E_D_GROUPS_NBR'
    end
    object cdBenefitAllDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdBenefitAllBenefitReference: TStringField
      FieldName = 'BenefitReference'
      Size = 40
    end
    object cdBenefitAllBenefitType: TStringField
      FieldName = 'BenefitType'
      Size = 40
    end
    object cdBenefitAllbCOBRA_RATE: TFloatField
      FieldName = 'bCOBRA_RATE'
    end
    object cdBenefitAllEFFECTIVE_START_DATE: TDateTimeField
      FieldName = 'EFFECTIVE_START_DATE'
    end
    object cdBenefitAllEFFECTIVE_END_DATE: TDateTimeField
      FieldName = 'EFFECTIVE_END_DATE'
    end
    object cdBenefitAllSTART_DATE: TDateTimeField
      FieldName = 'START_DATE'
    end
    object cdBenefitAllEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
    end
    object cdBenefitAllbTOTAL_PREMIUM_AMOUNT: TFloatField
      FieldName = 'bTOTAL_PREMIUM_AMOUNT'
    end
    object cdBenefitAllbEE_RATE: TFloatField
      FieldName = 'bEE_RATE'
    end
    object cdBenefitAllbER_RATE: TFloatField
      FieldName = 'bER_RATE'
    end
    object cdBenefitAllReferenceType: TStringField
      FieldName = 'ReferenceType'
      Size = 8
    end
    object cdBenefitAllEE_OR_ER_BENEFIT: TStringField
      FieldName = 'EE_OR_ER_BENEFIT'
      Size = 1
    end
    object cdBenefitAllAMOUNT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'AMOUNT'
      Calculated = True
    end
    object cdBenefitAlleAMOUNT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'eAMOUNT'
      Calculated = True
    end
    object cdBenefitAllrAMOUNT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'rAMOUNT'
      Calculated = True
    end
    object cdBenefitAllListType: TStringField
      FieldName = 'ListType'
      Size = 1
    end
    object cdBenefitAllCALCULATION_TYPE: TStringField
      FieldName = 'CALCULATION_TYPE'
      Size = 1
    end
    object cdBenefitAllCALCULATION_TYPE_Desc: TStringField
      FieldKind = fkCalculated
      FieldName = 'CALCULATION_TYPE_Desc'
      Size = 30
      Calculated = True
    end
    object cdBenefitAllRATE_TYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'RATE_TYPE'
      Size = 1
      Calculated = True
    end
    object cdBenefitAllAmountType: TStringField
      FieldName = 'AmountType'
      Size = 12
    end
    object cdBenefitAllEE_SCHEDULED_E_DS_NBR: TIntegerField
      FieldName = 'EE_SCHEDULED_E_DS_NBR'
    end
    object cdBenefitAllEE_PERCENTAGE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'EE_PERCENTAGE'
      Calculated = True
    end
    object cdBenefitAllER_PERCENTAGE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ER_PERCENTAGE'
      Calculated = True
    end
  end
  object dsCoBenefitRates: TevDataSource
    DataSet = DM_CO_BENEFIT_RATES.CO_BENEFIT_RATES
    Left = 208
    Top = 240
  end
  object cdBenefitAmountTypeRates: TevClientDataSet
    Left = 784
    Top = 232
  end
  object cdBeneficiaryBenefits: TevClientDataSet
    Left = 560
    Top = 208
    object cdBeneficiaryBenefitsBenefit: TStringField
      FieldName = 'Benefit'
      Size = 40
    end
    object cdBeneficiaryBenefitsBenefitAmountType: TStringField
      FieldName = 'BenefitAmountType'
      Size = 40
    end
    object cdBeneficiaryBenefitsBENEFIT_EFFECTIVE_DATE: TDateTimeField
      FieldName = 'BENEFIT_EFFECTIVE_DATE'
    end
    object cdBeneficiaryBenefitsEE_BENEFITS_NBR: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object cdBeneficiaryBenefitsCO_BENEFITS_NBR: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object cdBeneficiaryBenefitsCO_BENEFIT_SUBTYPE_NBR: TIntegerField
      FieldName = 'CO_BENEFIT_SUBTYPE_NBR'
    end
    object cdBeneficiaryBenefitsREQUIRES_DOB: TStringField
      FieldName = 'REQUIRES_DOB'
      Size = 1
    end
    object cdBeneficiaryBenefitsREQUIRES_SSN: TStringField
      FieldName = 'REQUIRES_SSN'
      Size = 1
    end
  end
  object dsBeneficiaryBenefits: TevDataSource
    DataSet = cdBeneficiaryBenefits
    OnDataChange = dsBeneficiaryBenefitsDataChange
    Left = 592
    Top = 208
  end
  object cdAvailableDependent: TevClientDataSet
    Left = 472
    Top = 176
    object IntegerField1: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object cdAvailableDependentCL_PERSON_DEPENDENTS_NBR: TIntegerField
      FieldName = 'CL_PERSON_DEPENDENTS_NBR'
    end
    object cdAvailableDependentFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
    end
    object cdAvailableDependentLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object cdAvailableDependentRELATION_TYPE: TStringField
      FieldName = 'RELATION_TYPE'
      Size = 10
    end
    object cdAvailableDependentDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object cdAvailableDependentSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Size = 11
    end
    object cdAvailableDependentPCP: TStringField
      FieldName = 'PCP'
      Size = 40
    end
  end
  object cdDependentBenefits: TevClientDataSet
    Left = 472
    Top = 208
    object StringField3: TStringField
      FieldName = 'Benefit'
      Size = 40
    end
    object StringField4: TStringField
      FieldName = 'BenefitAmountType'
      Size = 40
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'BENEFIT_EFFECTIVE_DATE'
    end
    object IntegerField4: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object IntegerField5: TIntegerField
      FieldName = 'CO_BENEFITS_NBR'
    end
    object IntegerField6: TIntegerField
      FieldName = 'CO_BENEFIT_SUBTYPE_NBR'
    end
    object cdDependentBenefitsREQUIRES_SSN: TStringField
      FieldName = 'REQUIRES_SSN'
      Size = 1
    end
    object cdDependentBenefitsREQUIRES_DOB: TStringField
      FieldName = 'REQUIRES_DOB'
      Size = 1
    end
    object cdDependentBenefitsREQUIRES_PCP: TStringField
      FieldName = 'REQUIRES_PCP'
      Size = 1
    end
  end
  object dsDependentBenefits: TevDataSource
    DataSet = cdDependentBenefits
    OnDataChange = dsDependentBenefitsDataChange
    Left = 504
    Top = 208
  end
  object dsAvailableDependent: TevDataSource
    DataSet = cdAvailableDependent
    Left = 504
    Top = 176
  end
  object dsAssignedDependent: TevDataSource
    DataSet = DM_EE_BENEFICIARY.EE_BENEFICIARY
    Left = 504
    Top = 146
  end
  object dsContingentBeneficiaries: TevDataSource
    DataSet = cdContingentBeneficiaries
    OnDataChange = dsContingentBeneficiariesDataChange
    Left = 592
    Top = 146
  end
  object dsPrimaryBeneficiaries: TevDataSource
    DataSet = cdPrimaryBeneficiaries
    OnDataChange = dsPrimaryBeneficiariesDataChange
    Left = 592
    Top = 176
  end
  object cdPrimaryBeneficiaries: TevClientDataSet
    Left = 560
    Top = 176
    object IntegerField2: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object IntegerField3: TIntegerField
      FieldName = 'CL_PERSON_DEPENDENTS_NBR'
    end
    object StringField1: TStringField
      FieldName = 'FIRST_NAME'
    end
    object StringField2: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object StringField5: TStringField
      FieldName = 'RELATION_TYPE'
      Size = 10
    end
    object cdPrimaryBeneficiariesPERCENTAGE: TIntegerField
      FieldName = 'PERCENTAGE'
    end
    object cdPrimaryBeneficiariesDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object cdPrimaryBeneficiariesSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Size = 11
    end
  end
  object cdContingentBeneficiaries: TevClientDataSet
    Left = 560
    Top = 144
    object IntegerField7: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object IntegerField8: TIntegerField
      FieldName = 'CL_PERSON_DEPENDENTS_NBR'
    end
    object StringField6: TStringField
      FieldName = 'FIRST_NAME'
    end
    object StringField7: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object StringField8: TStringField
      FieldName = 'RELATION_TYPE'
      Size = 10
    end
    object cdContingentBeneficiariesPERCENTAGE: TIntegerField
      FieldName = 'PERCENTAGE'
    end
    object cdContingentBeneficiariesDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object cdContingentBeneficiariesSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Size = 11
    end
  end
  object cdABAvailableDependent: TevClientDataSet
    Left = 560
    Top = 240
    object IntegerField9: TIntegerField
      FieldName = 'EE_BENEFITS_NBR'
    end
    object IntegerField10: TIntegerField
      FieldName = 'CL_PERSON_DEPENDENTS_NBR'
    end
    object StringField9: TStringField
      FieldName = 'FIRST_NAME'
    end
    object StringField10: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object StringField11: TStringField
      FieldName = 'RELATION_TYPE'
      Size = 10
    end
    object cdABAvailableDependentDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object cdABAvailableDependentSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
      Size = 11
    end
  end
  object dsABAvailableDependent: TevDataSource
    DataSet = cdABAvailableDependent
    Left = 592
    Top = 240
  end
  object dsBeneficiaryPlanDate: TevDataSource
    DataSet = DM_EE_BENEFICIARY_PLAN_DATES.EE_BENEFICIARY_PLAN_DATES
    OnStateChange = dsBeneficiaryPlanDateStateChange
    MasterDataSource = dsAssignedDependent
    Left = 976
    Top = 738
  end
end
