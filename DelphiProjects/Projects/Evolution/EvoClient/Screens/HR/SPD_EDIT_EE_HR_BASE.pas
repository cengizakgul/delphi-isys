// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_HR_BASE;

interface                                 

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_BASE, Db,   DBActns,
  ActnList, SDataStructure, Wwdatsrc, StdCtrls, wwdblook, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, EvUtils,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, EvUIComponents, EvClientDataSet, ImgList,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_EE_HR_BASE = class(TEDIT_EE_BASE)
    tbshDetails: TTabSheet;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

{ TEDIT_EE_HR_BASE }

procedure TEDIT_EE_HR_BASE.BitBtn1Click(Sender: TObject);
begin
  inherited;
  bPayrollsInQueue := False; 
end;

end.
