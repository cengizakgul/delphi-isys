// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_APPLICANT_EDUCATION;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CL_HR_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  wwdblook, wwdbdatetimepicker, Mask, wwdbedit, SPD_EDIT_CO_HR_APPLICANT_DESCR_BASE,
  Wwdotdot, Wwdbcomb, DBActns, ActnList, ISBasicClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIDBMemo, isUIwwDBComboBox, isUIwwDBDateTimePicker, isUIwwDBEdit,
  ImgList, isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CL_HR_APPLICANT_EDUCATION = class(TEDIT_CO_HR_APPLICANT_DESCR_BASE)
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evDBEdit3: TevDBEdit;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evDBDateTimePicker3: TevDBDateTimePicker;
    evDBComboBox1: TevDBComboBox;
    EvDBMemo1: TEvDBMemo;
    evLabel8: TevLabel;
    sbApplicantEd: TScrollBox;
    pnlDetailsBorder: TevPanel;
    fpEducationList: TisUIFashionPanel;
    pnlFPListBody: TevPanel;
    fpEducationDetails: TisUIFashionPanel;
    sbApplicantBrowse: TScrollBox;
    evSplitter1: TevSplitter;
    pnlLeftBorder: TevPanel;
    pnlRightBorder: TevPanel;
    pnlTopBorder: TevPanel;
    pnlBottomBorder: TevPanel;
    pnlBrowseLeft: TevPanel;
    fpBrowseLeft: TisUIFashionPanel;
    pnlFPLeftBody: TevPanel;
    pnlBrowseRight: TevPanel;
    fpBrowseRight: TisUIFashionPanel;
    pnlFPRightBody: TevPanel;
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;

  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure Activate; override;

  end;

implementation

uses SPD_EDIT_Open_BASE, SPD_EDIT_CO_HR_APPLICANT_BASE;

{$R *.DFM}

{ TEDIT_CL_HR_APPLICANT_EDUCATION }

procedure TEDIT_CL_HR_APPLICANT_EDUCATION.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  inherited;
  AddDS( DM_CLIENT.CL_HR_SCHOOL, aDS);
end;

function TEDIT_CL_HR_APPLICANT_EDUCATION.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_PERSON_EDUCATION;
end;

function TEDIT_CL_HR_APPLICANT_EDUCATION.GetInsertControl: TWinControl;
begin
  Result := evDBLookupCombo1;
end;

procedure TEDIT_CL_HR_APPLICANT_EDUCATION.PageControl1Change(
  Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePageIndex <> 1 then exit;

end;

procedure TEDIT_CL_HR_APPLICANT_EDUCATION.Activate;
begin
  inherited;

end;

initialization
  RegisterClass( TEDIT_CL_HR_APPLICANT_EDUCATION );

end.
