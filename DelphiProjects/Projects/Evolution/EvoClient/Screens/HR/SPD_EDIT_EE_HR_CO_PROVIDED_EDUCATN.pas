// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_HR_CO_PROVIDED_EDUCATN;

interface
                                            
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_HR_SUBBROWSE_BASE, Db,  
  DBActns, ActnList, SDataStructure, Wwdatsrc, StdCtrls, wwdblook, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  wwdbdatetimepicker, Mask, wwdbedit, Wwdotdot, Wwdbcomb, ISBasicClasses,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBEdit, isUIDBMemo, isUIwwDBDateTimePicker,
  ImgList, isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_EE_HR_CO_PROVIDED_EDUCATN = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    fpProvidedEducation: TisUIFashionPanel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel1: TevLabel;
    evLabel8: TevLabel;
    evLabel13: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evDBRadioGroup2: TevDBRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBEdit3: TevDBEdit;
    evDBLookupCombo1: TevDBLookupCombo;
    evcbCOURSE: TevDBLookupCombo;
    evDBEdit1: TevDBEdit;
    fpProvidedEdCheck: TisUIFashionPanel;
    evLabel9: TevLabel;
    evLabel7: TevLabel;
    evLabel6: TevLabel;
    evDBEdit5: TevDBEdit;
    evDBEdit4: TevDBEdit;
    evDBDateTimePicker3: TevDBDateTimePicker;
    fpProvidedEdRenewal: TisUIFashionPanel;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    evLabel12: TevLabel;
    evDBDateTimePicker4: TevDBDateTimePicker;
    evDBDateTimePicker5: TevDBDateTimePicker;
    wwcbRENEWAL_STATUS: TevDBComboBox;
    fpProvidedEdNotes: TisUIFashionPanel;
    evLabel5: TevLabel;
    EvDBMemo1: TEvDBMemo;
    procedure evcbCOURSEChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;

  end;

implementation

uses SPD_EDIT_Open_BASE, SPD_EDIT_EE_HR_BASE;

{$R *.DFM}

{ TEDIT_EE_HR_CO_PROVIDED_EDUCATN }

procedure TEDIT_EE_HR_CO_PROVIDED_EDUCATN.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_HR_COURSE, aDS);
  inherited;
end;

function TEDIT_EE_HR_CO_PROVIDED_EDUCATN.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_HR_CO_PROVIDED_EDUCATN;
end;

function TEDIT_EE_HR_CO_PROVIDED_EDUCATN.GetInsertControl: TWinControl;
begin
  Result := evDBRadioGroup1;
end;

procedure TEDIT_EE_HR_CO_PROVIDED_EDUCATN.evcbCOURSEChange(Sender: TObject);
begin
  inherited;
  if  evcbCOURSE.LookupTable.Active and
      evcbCOURSE.Focused and
      (Trim(evcbCOURSE.Text) <> '') then
  begin
    wwcbRENEWAL_STATUS.Text := wwcbRENEWAL_STATUS.GetComboDisplay(DM_CLIENT.CL_HR_COURSE.FieldByname('RENEWAL_STATUS').AsString);
  end;
end;

initialization
  RegisterClass( TEDIT_EE_HR_CO_PROVIDED_EDUCATN );

end.
