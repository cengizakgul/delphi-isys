// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_CAR;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_HR_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, ISBasicClasses, isUIwwDBEdit, SDDClasses,
  SDataDictclient, ImgList, SDataDicttemp, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_HR_CAR = class(TEDIT_CO_HR_BASE)
    evDBEdit1: TevDBEdit;
    evDBEdit2: TevDBEdit;
    evDBEdit4: TevDBEdit;
    evDBEdit5: TevDBEdit;
    evDBEdit6: TevDBEdit;
    evDBEdit8: TevDBEdit;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel8: TevLabel;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBComboBox1: TevDBComboBox;
    cbPersonelORCompany: TevDBComboBox;
    cbColor: TevDBComboBox;
    procedure fpBaseDBControlsResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;


  end;


implementation

{$R *.DFM}

{ TEDIT_CO_HR_CAR }

function TEDIT_CO_HR_CAR.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_HR_CAR;
end;

function TEDIT_CO_HR_CAR.GetInsertControl: TWinControl;
begin
  Result := evDBRadioGroup1;
end;

procedure TEDIT_CO_HR_CAR.fpBaseDBControlsResize(Sender: TObject);
begin
//  inherited; This needs to be turned off because the resizing is clipping
// the pnlDBControls. It's a statically sized control, so no loss.

end;

initialization
  RegisterClass( TEDIT_CO_HR_CAR );

end.
