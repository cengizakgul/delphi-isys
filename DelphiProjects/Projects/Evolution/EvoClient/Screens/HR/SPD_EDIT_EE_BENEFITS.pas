// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_BENEFITS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_HR_SUBBROWSE_BASE, Db,   TypInfo,
  DBActns, ActnList, SDataStructure, Wwdatsrc, StdCtrls, wwdblook, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Mask, wwdbedit,
  wwdbdatetimepicker, Wwdotdot, Wwdbcomb, EvConsts,
  SPD_MiniNavigationFrame, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  SDDClasses, ISDataAccessComponents, EvDataAccessComponents,
  SDataDictclient, SDataDicttemp, evUtils, SPackageEntry, EvExceptions,
  Variants, Menus, SFieldCodeValues, EvCommonInterfaces, EvDataset, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, isUIwwDBComboBox, isUIwwDBDateTimePicker, ImgList,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIEdit, Wwdbspin, Spin;

type
  TEDIT_EE_BENEFITS = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    tsBenefitPmnt: TTabSheet;
    dsBenefitPmnt: TevDataSource;
    dsDependent: TevDataSource;
    evProxyDataSet1: TevProxyDataSet;
    evdsCOBenefitSubtype: TevDataSource;
    dsCOBenefits: TevDataSource;
    evPnlDB: TevPanel;
    evPopupMenu1: TevPopupMenu;
    est11: TMenuItem;
    est21: TMenuItem;
    est31: TMenuItem;
    est41: TMenuItem;
    dsBenefitAll: TevDataSource;
    cdBenefitAll: TevClientDataSet;
    cdBenefitAllEE_NBR: TIntegerField;
    cdBenefitAllEE_OR_ER_BENEFIT: TStringField;
    cdBenefitAllbTOTAL_PREMIUM_AMOUNT: TFloatField;
    cdBenefitAllbEE_RATE: TFloatField;
    cdBenefitAllCO_BENEFIT_SUBTYPE_NBR: TIntegerField;
    cdBenefitAllbER_RATE: TFloatField;
    cdBenefitAllbCOBRA_RATE: TFloatField;
    cdBenefitAllCL_PERSON_NBR: TIntegerField;
    cdBenefitAllEE_BENEFITS_NBR: TIntegerField;
    cdBenefitAllCO_BENEFITS_NBR: TIntegerField;
    cdBenefitAllCL_E_DS_NBR: TIntegerField;
    cdBenefitAllCUSTOM_E_D_CODE_NUMBER: TStringField;
    cdBenefitAllCL_E_D_GROUPS_NBR: TIntegerField;
    cdBenefitAllDESCRIPTION: TStringField;
    cdBenefitAllEFFECTIVE_START_DATE: TDateTimeField;
    cdBenefitAllEFFECTIVE_END_DATE: TDateTimeField;
    cdBenefitAlleAMOUNT: TFloatField;
    cdBenefitAllrAMOUNT: TFloatField;
    cdBenefitAllAMOUNT: TFloatField;
    cdBenefitAllReferenceType: TStringField;
    cdBenefitAllListType: TStringField;
    cdBenefitAllBenefitReference: TStringField;
    cdBenefitAllBenefitType: TStringField;
    cdBenefitAllCALCULATION_TYPE: TStringField;
    cdBenefitAllCALCULATION_TYPE_Desc: TStringField;
    cdBenefitAllRATE_TYPE: TStringField;
    cdBenefitAllAmountType: TStringField;
    evcbOnlyActive: TevCheckBox;
    cdBenefitAllEE_SCHEDULED_E_DS_NBR: TIntegerField;
    cdBenefitAllEE_PERCENTAGE: TFloatField;
    cdBenefitAllER_PERCENTAGE: TFloatField;
    tsDependent: TTabSheet;
    dsCoBenefitRates: TevDataSource;
    cdBenefitAmountTypeRates: TevClientDataSet;
    cdBenefitAllEND_DATE: TDateTimeField;
    cdBenefitAllSTART_DATE: TDateTimeField;
    fpBenefits: TisUIFashionPanel;
    evPnlEdits: TevPanel;
    evLabel1: TevLabel;
    evLabel17: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    lablEnrollment_Status: TevLabel;
    evEditCoBenefitAddInfo1: TevLabel;
    evEditCoBenefitAddInfo2: TevLabel;
    evEditCoBenefitAddInfo3: TevLabel;
    evLabel38: TevLabel;
    lablDeduction_Frequency: TevLabel;
    btnSelectBenefitAmountType: TevSpeedButton;
    evBenefitReference: TevDBLookupCombo;
    evBenefitSubtype: TevDBLookupCombo;
    evMedParticipant: TevDBRadioGroup;
    evEffectiveStart: TevDBDateTimePicker;
    evEffectiveEnd: TevDBDateTimePicker;
    wwcbEnrollment_Status: TevDBComboBox;
    evSSDisability: TevDBRadioGroup;
    evAddInfo1: TevDBEdit;
    evAddInfo2: TevDBEdit;
    evAddInfo3: TevDBEdit;
    evDBRadioGroup8: TevDBRadioGroup;
    edPrimaryCare: TevDBEdit;
    wwcbDeduction_Frequency: TevDBComboBox;
    fpRates: TisUIFashionPanel;
    evGrpCL: TevGroupBox;
    evLabel5: TevLabel;
    evLabel11: TevLabel;
    evLabel12: TevLabel;
    evLabel36: TevLabel;
    evLabel30: TevLabel;
    evLabel40: TevLabel;
    evdbCLAmount: TevDBEdit;
    evdbCLEEAmount: TevDBEdit;
    evdbCLERAmount: TevDBEdit;
    evdbCOCobraAmount: TevDBEdit;
    evdbCORateType: TevDBComboBox;
    evdbCOED_Group: TevDBLookupCombo;
    evGrpEE: TevGroupBox;
    evLabel15: TevLabel;
    evLabel32: TevLabel;
    evLabel33: TevLabel;
    evLabel31: TevLabel;
    evLabel35: TevLabel;
    evLabel46: TevLabel;
    evdbAmount: TevDBEdit;
    evdbEEAmount: TevDBEdit;
    evdbERAmount: TevDBEdit;
    evdbEECobraAmount: TevDBEdit;
    evdbEERateType: TevDBComboBox;
    evdbEEED_Group: TevDBLookupCombo;
    fpCobra: TisUIFashionPanel;
    edEEBenefitNbr: TevDBEdit;
    sb_HR_EE_BenefitPayments: TScrollBox;
    pnl_HR_EE_BenefitPaymentBorder: TevPanel;
    fp_HR_EE_ListOfBenefitPayments: TisUIFashionPanel;
    pnl_HR_EE_BenefitPaymentsBody: TevPanel;
    fp_HR_EE_BenefitPaymentInfo: TisUIFashionPanel;
    pnl_HR_EE_BenefitPaymentInfoBody: TevPanel;
    evLabel26: TevLabel;
    evLabel25: TevLabel;
    evLabel27: TevLabel;
    EdQualifyingEventDate: TevDBDateTimePicker;
    edRefusal: TevDBComboBox;
    evCobraEvent: TevDBComboBox;
    EdElected: TevDBRadioGroup;
    cdBeneficiaryBenefits: TevClientDataSet;
    cdBeneficiaryBenefitsBenefit: TStringField;
    cdBeneficiaryBenefitsBenefitAmountType: TStringField;
    cdBeneficiaryBenefitsBENEFIT_EFFECTIVE_DATE: TDateTimeField;
    cdBeneficiaryBenefitsEE_BENEFITS_NBR: TIntegerField;
    cdBeneficiaryBenefitsCO_BENEFITS_NBR: TIntegerField;
    cdBeneficiaryBenefitsCO_BENEFIT_SUBTYPE_NBR: TIntegerField;
    fpControls: TisUIFashionPanel;
    evBtnUseClientBenefit: TevBitBtn;
    evBtnSwitchEE: TevBitBtn;
    evBtnCompanyBenefitScreen: TevBitBtn;
    evBtnEEScheduledEDScreen: TevBitBtn;
    sb_HR_EE_BenefitDependent: TScrollBox;
    pnl_HR_EE_BenefitDependentBorder: TevPanel;
    fp_HR_EE_ListOfBenefitDependets: TisUIFashionPanel;
    pnl_HR_EE_BenefitDependetsBody: TevPanel;
    fpDependentDetail: TisUIFashionPanel;
    evLabel24: TevLabel;
    evLabel34: TevLabel;
    evLabel37: TevLabel;
    evLabel39: TevLabel;
    evLabel41: TevLabel;
    evLabel42: TevLabel;
    evLabel43: TevLabel;
    evLabel44: TevLabel;
    evLabel45: TevLabel;
    Label48: TevLabel;
    lblPCP: TevLabel;
    evLabel29: TevLabel;
    evLabel28: TevLabel;
    edDependentCity: TevDBEdit;
    edDependentRelation: TevDBComboBox;
    edDependentFirstName: TevDBEdit;
    edDependentLastName: TevDBEdit;
    evDBEdit15: TevDBEdit;
    edDependentSSN: TevDBEdit;
    edDependentAdress1: TevDBEdit;
    edDependentAdress2: TevDBEdit;
    edDependentState: TevDBEdit;
    edDependentZip: TevDBEdit;
    edDependentFullTimeStudent: TevDBRadioGroup;
    edDependentExisitngPatient: TevDBRadioGroup;
    edDependentPCP: TevDBEdit;
    edDependentGender: TevDBComboBox;
    DependentMiniNavigation: TMiniNavigationFrame;
    evBtnCopyEEAddress: TevBitBtn;
    edDependentBirthDate: TevDBDateTimePicker;
    evDBGrid3: TevDBGrid;
    evDBGrid2: TevDBGrid;
    evLabel16: TevLabel;
    edBenefitName: TevDBEdit;
    evLabel19: TevLabel;
    edBenefitAmountType: TevDBEdit;
    evLabel4: TevLabel;
    edPaymentDate: TevDBDateTimePicker;
    evLabel22: TevLabel;
    edPaymentType: TevDBComboBox;
    evLabel18: TevLabel;
    edPaymentNumber: TevDBEdit;
    evLabel6: TevLabel;
    edtPymtAmount: TevDBEdit;
    evLabel7: TevLabel;
    edPeriodStartDate: TevDBDateTimePicker;
    evLabel8: TevLabel;
    edPeriodEndDate: TevDBDateTimePicker;
    edReconciled: TevDBRadioGroup;
    evLabel14: TevLabel;
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel20: TevLabel;
    edCouponSentDate: TevDBDateTimePicker;
    evLabel48: TevLabel;
    edLetterSentDate: TevDBDateTimePicker;
    evLabel50: TevLabel;
    edDeductionType: TevDBComboBox;
    edVoid: TevDBRadioGroup;
    evLabel10: TevLabel;
    edRemarks: TevDBEdit;
    evLabel9: TevLabel;
    edReason: TevDBEdit;
    BenefitPmtMiniNavigation: TMiniNavigationFrame;
    edDependentPersonType: TevDBRadioGroup;
    cbShowPayrollDetails: TevCheckBox;
    tsAssignDependent: TTabSheet;
    tsAssignBeneficiaries: TTabSheet;
    ScrollBox1: TScrollBox;
    ScrollBox2: TScrollBox;
    fpABPrimaryBeneficiaries: TisUIFashionPanel;
    grdPrimaryBeneficiaries: TevDBGrid;
    fpAvailableDependents: TisUIFashionPanel;
    grdAvailableDependents: TevDBGrid;
    fpAssignedDependets: TisUIFashionPanel;
    evPersonType: TevDBRadioGroup;
    fpDependentBenefitSummary: TisUIFashionPanel;
    grdDependentBenefitSummary: TevDBGrid;
    fpABBenefitSummary: TisUIFashionPanel;
    grdABBenefitSummary: TevDBGrid;
    fpABContingentBeneficiaries: TisUIFashionPanel;
    grdContingentBeneficiaries: TevDBGrid;
    sbAddAssingDependent: TevSpeedButton;
    sbDelAssignDependent: TevSpeedButton;
    dsBeneficiaryBenefits: TevDataSource;
    grdAssignedDependents: TevDBGrid;
    cdAvailableDependent: TevClientDataSet;
    IntegerField1: TIntegerField;
    cdDependentBenefits: TevClientDataSet;
    StringField3: TStringField;
    StringField4: TStringField;
    DateTimeField2: TDateTimeField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    dsDependentBenefits: TevDataSource;
    dsAvailableDependent: TevDataSource;
    dsAssignedDependent: TevDataSource;
    dsContingentBeneficiaries: TevDataSource;
    dsPrimaryBeneficiaries: TevDataSource;
    cdAvailableDependentFIRST_NAME: TStringField;
    cdAvailableDependentLAST_NAME: TStringField;
    cdAvailableDependentRELATION_TYPE: TStringField;
    cdAvailableDependentCL_PERSON_DEPENDENTS_NBR: TIntegerField;
    cdPrimaryBeneficiaries: TevClientDataSet;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField5: TStringField;
    cdContingentBeneficiaries: TevClientDataSet;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    cdContingentBeneficiariesPERCENTAGE: TIntegerField;
    cdPrimaryBeneficiariesPERCENTAGE: TIntegerField;
    fpABAvailableDependents: TisUIFashionPanel;
    grdABAvailableDependents: TevDBGrid;
    lblPercentage: TevLabel;
    sbAddAssingBeneficiaries: TevSpeedButton;
    sbRemoveAssingBeneficiaries: TevSpeedButton;
    gbPrimary: TevGroupBox;
    rbPrimaryYes: TevRadioButton;
    rbPrimaryNo: TevRadioButton;
    edPercentage: TevSpinEdit;
    cdABAvailableDependent: TevClientDataSet;
    IntegerField9: TIntegerField;
    IntegerField10: TIntegerField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    dsABAvailableDependent: TevDataSource;
    cdDependentBenefitsREQUIRES_SSN: TStringField;
    cdDependentBenefitsREQUIRES_DOB: TStringField;
    cdDependentBenefitsREQUIRES_PCP: TStringField;
    cdBeneficiaryBenefitsREQUIRES_DOB: TStringField;
    cdBeneficiaryBenefitsREQUIRES_SSN: TStringField;
    cdAvailableDependentDATE_OF_BIRTH: TDateTimeField;
    cdAvailableDependentSOCIAL_SECURITY_NUMBER: TStringField;
    cdAvailableDependentPCP: TStringField;
    cdContingentBeneficiariesDATE_OF_BIRTH: TDateTimeField;
    cdContingentBeneficiariesSOCIAL_SECURITY_NUMBER: TStringField;
    cdPrimaryBeneficiariesDATE_OF_BIRTH: TDateTimeField;
    cdPrimaryBeneficiariesSOCIAL_SECURITY_NUMBER: TStringField;
    cdABAvailableDependentDATE_OF_BIRTH: TDateTimeField;
    cdABAvailableDependentSOCIAL_SECURITY_NUMBER: TStringField;
    isUIFashionPanel1: TisUIFashionPanel;
    evPanel1: TevPanel;
    evLabel23: TevLabel;
    evLabel53: TevLabel;
    edtPlanBeginDate: TevDBDateTimePicker;
    edtPlanEndDate: TevDBDateTimePicker;
    BeneficiaryPlanDateNavigation: TMiniNavigationFrame;
    grdPlanDates: TevDBGrid;
    dsBeneficiaryPlanDate: TevDataSource;
    procedure BenefitPmtMiniNavigationSpeedButton1Click(
      Sender: TObject);
    procedure BenefitPmtMiniNavigationSpeedButton2Click(
      Sender: TObject);
    procedure evBenefitReferenceCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evBtnCompanyBenefitScreenClick(Sender: TObject);
    procedure evBtnEEScheduledEDScreenClick(Sender: TObject);
    procedure BenefitFilterSetup;
    procedure BenefitPaymentFilterSetup;
    procedure PageControl1Change(Sender: TObject);
    procedure wwdsEmployeeDataChange(Sender: TObject; Field: TField);
    procedure evBenefitReferenceChange(Sender: TObject);
    Procedure cdBenefitAllActivate;
    procedure evBtnUseClientBenefitClick(Sender: TObject);
    procedure evdbERAmountExit(Sender: TObject);
    Procedure SetBenefitAllFilter(SetTag : integer);
    procedure cdBenefitAllCalcFields(DataSet: TDataSet);
    procedure SetLabelsAndFields;
    procedure UpdateEEAmount;
    procedure evcbOnlyActiveClick(Sender: TObject);
    procedure evBtnSwitchEEClick(Sender: TObject);
    Procedure SetDBEnabled(aValue : Boolean);
    procedure dsBenefitAllDataChange(Sender: TObject; Field: TField);
    Procedure SetLocateBenefits(const RunAllTime:boolean=false);
    procedure DependentMiniNavigationSpeedButton1Click(Sender: TObject);
    procedure DependentMiniNavigationSpeedButton2Click(Sender: TObject);
    procedure evdbEEAmountExit(Sender: TObject);
    procedure evdbEERateTypeChange(Sender: TObject);
    procedure evBtnCopyEEAddressClick(Sender: TObject);
    procedure btnSelectBenefitAmountTypeClick(Sender: TObject);
    procedure edEEBenefitNbrChange(Sender: TObject);
    Procedure FillcdBeneficiaryBenefits;
    Procedure FillcdDependentBenefits;
    procedure evDBGrid1DrawFooterCell(Sender: TObject; Canvas: TCanvas;
      FooterCellRect: TRect; Field: TField; FooterText: String;
      var DefaultDrawing: Boolean);
    procedure cbShowPayrollDetailsClick(Sender: TObject);
    procedure dsDependentBenefitsDataChange(Sender: TObject;
      Field: TField);
    procedure sbAddAssingDependentClick(Sender: TObject);
    procedure sbDelAssignDependentClick(Sender: TObject);
    procedure dsBeneficiaryBenefitsDataChange(Sender: TObject;
      Field: TField);
    procedure dsContingentBeneficiariesDataChange(Sender: TObject;
      Field: TField);
    procedure dsPrimaryBeneficiariesDataChange(Sender: TObject;
      Field: TField);
    procedure grdPrimaryBeneficiariesDrawFooterCell(Sender: TObject;
      Canvas: TCanvas; FooterCellRect: TRect; Field: TField;
      FooterText: String; var DefaultDrawing: Boolean);
    procedure grdContingentBeneficiariesDrawFooterCell(Sender: TObject;
      Canvas: TCanvas; FooterCellRect: TRect; Field: TField;
      FooterText: String; var DefaultDrawing: Boolean);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure sbAddAssingBeneficiariesClick(Sender: TObject);
    procedure sbRemoveAssingBeneficiariesClick(Sender: TObject);
    procedure grdContingentBeneficiariesSelectionChanged(Sender: TObject);
    procedure grdPrimaryBeneficiariesSelectionChanged(Sender: TObject);
    procedure evPersonTypeChange(Sender: TObject);
    procedure dsDependentDataChange(Sender: TObject; Field: TField);
    procedure SetAssignDependentFilter;
    function CheckPercentage: boolean;
    procedure SetBenefitChange;
    procedure BeneficiaryPlanDateNavigationSpeedButton1Click(
      Sender: TObject);
    procedure BeneficiaryPlanDateNavigationSpeedButton2Click(
      Sender: TObject);
    procedure edtPlanBeginDateClick(Sender: TObject);
    procedure dsBeneficiaryPlanDateStateChange(Sender: TObject);
  private
    { Private declarations }
    FActivated: boolean;
    FAssingBeneficiary: boolean;
    procedure SetPlanDateRight(const value:Boolean);    
  protected
    function GetDataSetConditions(sName: string): string; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    procedure OnActivateParams(const AContext: Integer; const AParams: array of Variant); override;

  public
    { Public declarations }
    function CanClose: Boolean; override;
    procedure SetReadOnly(Value: Boolean); override;
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
  end;

implementation

uses SPD_EDIT_EE_BASE, SFrameEntry, SPD_EDIT_EE_HR_BASE,
  SPD_EDIT_Open_BASE, SPD_EDIT_SELECT_BENEFIT_AMOUNT;

{$R *.DFM}
{ TEDIT_EE_BENEFITS }


procedure TEDIT_EE_BENEFITS.SetReadOnly(Value: Boolean);
begin
  inherited;
  evBenefitSubtype.SecurityRO := True;
end;

function TEDIT_EE_BENEFITS.CanClose: Boolean;
begin
  Result := inherited CanClose;
  if Result and CheckPercentage then
  begin
     Result:= false;
     EvMessage('Total percentage must equal 100%.', mtInformation, [mbOK]);
  end;
end;

procedure TEDIT_EE_BENEFITS.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
   inherited;
   if PageControl1.ActivePage = tsAssignDependent then
   begin
     AllowChange := grdDependentBenefitSummary.Enabled;
   end
   else
   if PageControl1.ActivePage = tsAssignBeneficiaries then
   begin
     if CheckPercentage then
     begin
         AllowChange := false;
         EvMessage('Total percentage must equal 100%.', mtInformation, [mbOK]);
     end;

     if AllowChange then
     begin
       cdPrimaryBeneficiaries.AggregatesActive := false;
       cdContingentBeneficiaries.AggregatesActive := false;
     end;
   end;

   if PageControl1.ActivePage = tsDependent then
   begin
     if dsDependent.DataSet.State in [dsEdit,dsInsert] then
     begin
         AllowChange := false;
         EvMessage('Please post or cancel changes before going to another screen.', mtInformation, [mbOK]);
     end;
   end;

end;


function TEDIT_EE_BENEFITS.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_BENEFITS') then
    Result := 'CO_NBR = ' + wwdsList.DataSet.FieldByName('CO_NBR').AsString
  else
  if (sName = 'EE_BENEFICIARY_PLAN_DATES') or (sName = 'EE_BENEFICIARY') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_EE_BENEFITS.AfterClick(Kind: Integer);
begin
  inherited;
  if PageControl1.ActivePage = tbshDetails then
    if Kind in [NavFirst, NavPrevious, NavNext, NavLast] then
    begin
      if wwdsDetail.DataSet.FieldByName('EE_BENEFITS_NBR').AsInteger > 0 then
      begin
        if dsBenefitAll.DataSet.Locate('EE_BENEFITS_NBR', wwdsDetail.DataSet['EE_BENEFITS_NBR'], []) then
         evDBGrid1.DataSource.DataSet.GotoBookmark(dsBenefitAll.DataSet.GetBookmark);
      end;
    end;

  if Kind in [NavCommit] then
  begin
    if evBtnSwitchEE.Tag = 1 then
    begin
        DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired('ALL');
        if DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', dsBenefitAll.DataSet['EE_SCHEDULED_E_DS_NBR'], []) then
        begin
          DM_EMPLOYEE.EE_SCHEDULED_E_DS.Edit;
          DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('EE_BENEFITS_NBR').AsInteger := wwdsDetail.DataSet.FieldByName('EE_BENEFITS_NBR').AsInteger;
          DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CO_BENEFIT_SUBTYPE_NBR').Value := Null;
          DM_EMPLOYEE.EE_SCHEDULED_E_DS.Post;
          ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_SCHEDULED_E_DS]);
        end;
    end;
    cdBenefitAllActivate;
  end;
  if Kind in [NavCancel, NavAbort] then
     evBtnSwitchEE.Tag := 0;
end;

procedure TEDIT_EE_BENEFITS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
      s : String;
      Q : IevQuery;
begin
  inherited;
  if Kind in [NavInsert] then
    SetDBEnabled(True);

  if Kind in [NavOk, NavCommit] then
  begin
   if PageControl1.ActivePage = tsBenefitPmnt then
    if dsBenefitPmnt.DataSet.State in [dsEdit,dsInsert] then
     if (Trim(edtPymtAmount.Text) = '') then
       raise EUpdateError.CreateHelp('Payment Amount can''t be empty', IDH_ConsistencyViolation);

   if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
   begin
     if wwdsDetail.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger = 0 then
       raise EUpdateError.CreateHelp('Benefit Reference can''t be empty', IDH_ConsistencyViolation);

     if wwdsDetail.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger = 0 then
       raise EUpdateError.CreateHelp('Benefit Amount Type can''t be empty', IDH_ConsistencyViolation);

     UpdateEEAmount;

     s := 'Select count(*) from pr_check_lines T2, pr_check T1, ee_scheduled_e_ds T3, PR T4 ' +
           ' where {AsOfNow<T1>} and {AsOfNow<T2>} and ' +
           ' {AsOfNow<T3>} and {AsOfNow<T4>} and ' +
           ' t4.STATUS = ''W'' and t1.PR_NBR = t4.PR_NBR and ' +
           ' t1.ee_nbr = :eeNbr and t3.ee_nbr = :eeNbr and ' +
           ' T2.ee_scheduled_e_ds_NBR = T3.EE_SCHEDULED_E_DS_NBR and ' +
           ' T1.pr_check_NBR = T2.pr_check_NBR and T3.EE_BENEFITS_NBR = :eeBenefitNbr ';

     Q := TevQuery.Create(s);
     Q.Params.AddValue('eeNbr', wwdsDetail.DataSet.FieldByName('EE_NBR').AsInteger);
     Q.Params.AddValue('eeBenefitNbr', wwdsDetail.DataSet.FieldByName('EE_BENEFITS_NBR').AsInteger);
     Q.Execute;
     if Q.Result.Fields[0].AsInteger > 0 then
       if EvMessage('Are you sure you want to save these changes?'#13'You''ve changed the employee''s benefits and there are open payrolls.'#13'If you select Yes, these changes will take effect when any open payrolls are processed.'#13'If you select No, your changes will NOT be saved!', mtConfirmation, [mbYes, mbNo]) <> mrYes then
          wwdsDetail.DataSet.Cancel;
   end;

   if dsBeneficiaryPlanDate.DataSet.State in [dsEdit,dsInsert] then
     dsBeneficiaryPlanDate.DataSet.FieldByName('EE_BENEFICIARY_NBR').AsInteger :=
         DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('EE_BENEFICIARY_NBR').AsInteger;

  end;

  if PageControl1.ActivePage = tsAssignDependent then
  begin
   if Kind in [NavCommit] then
     SetPlanDateRight(True);
   if Kind in [NavCancel, NavAbort] then
   begin
     DM_EMPLOYEE.EE_BENEFICIARY_PLAN_DATES.Cancel;
     DM_EMPLOYEE.EE_BENEFICIARY.Close;
     DM_EMPLOYEE.EE_BENEFICIARY.DataRequired('ALL');
     FillcdDependentBenefits;
     SetPlanDateRight(True);
   end;
  end;

end;

procedure TEDIT_EE_BENEFITS.OnActivateParams(const AContext: Integer; const AParams: array of Variant);
begin
  inherited;
  if AContext = 1 then
  begin
    PageControl1.ActivePage := tbshDetails;
    DM_CLIENT.EE.Locate('EE_NBR', AParams[0], []);
    cdBenefitAllActivate;
  end;
  if AContext = 2 then
  begin
    PageControl1.ActivePage := tbshDetails;
    DM_CLIENT.EE.Locate('EE_NBR', AParams[0], []);
    cdBenefitAllActivate;
    wwdsDetail.DataSet.Insert;
    wwdsDetail.DataSet['EE_NBR'] := AParams[0];
    SetLocateBenefits(True);
    SetDBEnabled(True);
  end;

end;

procedure TEDIT_EE_BENEFITS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS( DM_COMPANY.CO_BENEFITS, aDS);
  AddDS( DM_COMPANY.CO_BENEFIT_SUBTYPE, aDS);
  AddDS( DM_COMPANY.CO_BENEFIT_RATES, aDS);
  AddDS( DM_EMPLOYEE.EE_BENEFIT_PAYMENT, aDS);
  AddDS( DM_EMPLOYEE.EE, aDS);
  AddDS( DM_CLIENT.CL_AGENCY, aDS);
  AddDS( DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS( DM_EMPLOYEE.EE_BENEFICIARY, aDS);
  AddDS( DM_CLIENT.CL_PERSON_DEPENDENTS, aDS);
  AddDS( DM_COMPANY.CO_BENEFIT_CATEGORY, aDS);
  AddDS( DM_CLIENT.EE_BENEFICIARY_PLAN_DATES, aDS);  

  inherited;
end;

procedure TEDIT_EE_BENEFITS.AfterDataSetsReopen;
begin
  inherited;
  DM_COMPANY.CO_BENEFITS.Filter := 'EE_BENEFIT = ''Y''';
  DM_COMPANY.CO_BENEFITS.Filtered := True;
  BenefitFilterSetup;
end;

function TEDIT_EE_BENEFITS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_BENEFITS;
end;

function TEDIT_EE_BENEFITS.GetInsertControl: TWinControl;
begin
  Result := evBenefitReference;
end;

procedure TEDIT_EE_BENEFITS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  AddDSWithCheck(DM_COMPANY.CO_BENEFITS, SupportDataSets, 'EE_BENEFIT = ''Y''');
  AddDS( DM_COMPANY.CO_BENEFIT_SUBTYPE, SupportDataSets);
  AddDS( DM_COMPANY.CO_BENEFIT_RATES, SupportDataSets);
  AddDS( DM_CLIENT.CL_AGENCY, SupportDataSets);
  AddDS( DM_CLIENT.CL_E_D_GROUPS, SupportDataSets);
  AddDS( DM_COMPANY.CO_BENEFIT_CATEGORY, SupportDataSets);
  inherited;
  AddDS( DM_EMPLOYEE.EE, EditDataSets);
  AddDS( DM_EMPLOYEE.EE_BENEFIT_PAYMENT, EditDataSets);
  AddDS( DM_EMPLOYEE.EE_BENEFICIARY_PLAN_DATES, EditDataSets);
  AddDS( DM_EMPLOYEE.EE_BENEFICIARY, EditDataSets);
  AddDS( DM_CLIENT.CL_PERSON_DEPENDENTS, EditDataSets);

end;

procedure TEDIT_EE_BENEFITS.Activate;
begin
  FActivated := true;
  inherited;

  SetDBEnabled(True);

  BenefitPmtMiniNavigation.DataSource := dsBenefitPmnt;
  BenefitPmtMiniNavigation.InsertFocusControl := edPaymentDate;

  BenefitPmtMiniNavigation.SpeedButton1.OnClick := BenefitPmtMiniNavigationSpeedButton1Click;
  BenefitPmtMiniNavigation.SpeedButton2.OnClick := BenefitPmtMiniNavigationSpeedButton2Click;

  DependentMiniNavigation.DataSource := dsDependent;
  DependentMiniNavigation.InsertFocusControl := evPersonType;

  DependentMiniNavigation.SpeedButton1.OnClick := DependentMiniNavigationSpeedButton1Click;
  DependentMiniNavigation.SpeedButton2.OnClick := DependentMiniNavigationSpeedButton2Click;


  BeneficiaryPlanDateNavigation.DataSource := dsBeneficiaryPlanDate;
  BeneficiaryPlanDateNavigation.InsertFocusControl := edtPlanBeginDate;

  BeneficiaryPlanDateNavigation.SpeedButton1.OnClick := BeneficiaryPlanDateNavigationSpeedButton1Click;
  BeneficiaryPlanDateNavigation.SpeedButton2.OnClick := BeneficiaryPlanDateNavigationSpeedButton2Click;


end;

procedure TEDIT_EE_BENEFITS.Deactivate;
begin
  FActivated := false;
  inherited;
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.Close;
  if DM_COMPANY.CO_BENEFIT_SUBTYPE.Active then
     DM_COMPANY.CO_BENEFIT_SUBTYPE.CancelRange;

  DM_CLIENT.CO_BENEFITS.Filter :=  '';
  DM_CLIENT.CO_BENEFITS.Filtered := False;
  cdBenefitAll.AggregatesActive := False;
  cdBenefitAll.Aggregates.Clear;
  cdBenefitAll.Filtered := False;
end;

procedure TEDIT_EE_BENEFITS.BenefitFilterSetup;
begin
  wwdsDetail.DataSet.DisableControls;
  try
    wwdsDetail.DataSet.Filter := 'EE_NBR = ' + String(ConvertNull(DM_EMPLOYEE.EE['EE_NBR'], -1));
    wwdsDetail.DataSet.Filtered := true;
  finally
    wwdsDetail.DataSet.EnableControls;
  end;
end;

Procedure TEDIT_EE_BENEFITS.FillcdDependentBenefits;
var
   cd, cdCLPersonDependent: TevClientDataSet;
Begin
      cdDependentBenefits.DisableControls;
      cd := TevClientDataSet.Create(nil);
      cdCLPersonDependent := TevClientDataSet.Create(nil);
      try
          cd.CloneCursor(DM_EMPLOYEE.EE_BENEFITS, True);
          cdCLPersonDependent.CloneCursor(DM_CLIENT.CL_PERSON_DEPENDENTS, True);

          DM_COMPANY.CO_BENEFIT_SUBTYPE.CancelRange;

          cdDependentBenefits.EmptyDataSet;
          if cdDependentBenefits.FieldDefs.Count = 0 then
             cdDependentBenefits.CreateDataSet;

          cdAvailableDependent.EmptyDataSet;
          if cdAvailableDependent.FieldDefs.Count = 0 then
             cdAvailableDependent.CreateDataSet;


          DM_EMPLOYEE.EE_BENEFICIARY.Filtered :=false;
          DM_EMPLOYEE.EE_BENEFICIARY.Filter := '';

          cdCLPersonDependent.Filter := 'CL_PERSON_NBR = ' + IntToStr(DM_CLIENT.EE.FieldByName('CL_PERSON_NBR').AsInteger) +
                                           ' and PERSON_TYPE = ''' + PERSON_TYPE_DEPENDENT + '''';
          cdCLPersonDependent.Filtered := True;

          cd.Filter := 'EE_NBR = ' + IntToStr(DM_CLIENT.EE.FieldByName('EE_NBR').AsInteger) + ' and CO_BENEFIT_SUBTYPE_NBR is not null ';
          cd.Filtered := True;

          cd.First;
          while not cd.Eof do
          begin
              if GetCoBenefitRateNumber(cd['CO_BENEFIT_SUBTYPE_NBR'],
                   ConvertNull(cd['BENEFIT_EFFECTIVE_DATE'],Now), ConvertNull(cd['EXPIRATION_DATE'], Now))  then
              begin
                if DM_COMPANY.CO_BENEFIT_RATES.FieldByName('MAX_DEPENDENTS').AsInteger > 0 then
                begin

                  cdDependentBenefits.Append;
                  cdDependentBenefits['EE_BENEFITS_NBR'] := cd['EE_BENEFITS_NBR'];
                  cdDependentBenefits['CO_BENEFITS_NBR'] := cd['CO_BENEFITS_NBR'];
                  cdDependentBenefits['CO_BENEFIT_SUBTYPE_NBR'] := cd['CO_BENEFIT_SUBTYPE_NBR'];
                  cdDependentBenefits['BENEFIT_EFFECTIVE_DATE'] := cd['BENEFIT_EFFECTIVE_DATE'];

                  if DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR', cd['CO_BENEFITS_NBR'], []) then
                  begin
                     cdDependentBenefits['Benefit'] := DM_COMPANY.CO_BENEFITS['BENEFIT_NAME'];
                     cdDependentBenefits['REQUIRES_SSN'] := DM_COMPANY.CO_BENEFITS['REQUIRES_SSN'];
                     cdDependentBenefits['REQUIRES_DOB'] := DM_COMPANY.CO_BENEFITS['REQUIRES_DOB'];
                     cdDependentBenefits['REQUIRES_PCP'] := DM_COMPANY.CO_BENEFITS['REQUIRES_PCP'];
                  end;

                  if DM_COMPANY.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFIT_SUBTYPE_NBR', cd['CO_BENEFIT_SUBTYPE_NBR'], []) then
                     cdDependentBenefits['BenefitAmountType'] := DM_COMPANY.CO_BENEFIT_SUBTYPE['DESCRIPTION'];

                  cdDependentBenefits.Post;

                  cdCLPersonDependent.First;
                  while not cdCLPersonDependent.Eof do
                  begin
                    if not DM_EMPLOYEE.EE_BENEFICIARY.Locate('EE_BENEFITS_NBR;CL_PERSON_DEPENDENTS_NBR',
                                                       VarArrayOf([cd['EE_BENEFITS_NBR'], cdCLPersonDependent['CL_PERSON_DEPENDENTS_NBR']]),[]) then
                       cdAvailableDependent.AppendRecord([cd['EE_BENEFITS_NBR'], cdCLPersonDependent['CL_PERSON_DEPENDENTS_NBR'],
                             cdCLPersonDependent['FIRST_NAME'], cdCLPersonDependent['LAST_NAME'],
                             ReturnDescription(Relationship_ComboChoices, cdCLPersonDependent.fieldByName('RELATION_TYPE').AsString),
                             cdCLPersonDependent['DATE_OF_BIRTH'],cdCLPersonDependent['SOCIAL_SECURITY_NUMBER'],cdCLPersonDependent['PCP']]);

                    cdCLPersonDependent.Next;
                  end;

                end;
              end;
              cd.Next;
          end;
      finally
       cd.Free;
       cdCLPersonDependent.Free;
       cdDependentBenefits.First;
       dsAvailableDependent.MasterDataSource := dsDependentBenefits;
       cdDependentBenefits.EnableControls;

       SetAssignDependentFilter;
       SetPlanDateRight(True);
      end;
End;

procedure TEDIT_EE_BENEFITS.SetAssignDependentFilter;
Begin
    dsAvailableDependent.DataSet.Filter := 'EE_BENEFITS_NBR = ' +
         VarToStr(ConvertNull(dsDependentBenefits.DataSet['EE_BENEFITS_NBR'], -1));
    dsAvailableDependent.DataSet.Filtered :=true;

    DM_EMPLOYEE.EE_BENEFICIARY.Filter := 'EE_BENEFITS_NBR = ' +
         VarToStr(ConvertNull(dsDependentBenefits.DataSet['EE_BENEFITS_NBR'], -1));
    DM_EMPLOYEE.EE_BENEFICIARY.Filtered :=true;
end;

procedure TEDIT_EE_BENEFITS.dsDependentBenefitsDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if dsDependentBenefits.Active and (dsDependentBenefits.DataSet.RecordCount > 0) then
  begin
    if Assigned(dsAvailableDependent.DataSet) then
       SetAssignDependentFilter;
  end;
end;

procedure TEDIT_EE_BENEFITS.SetPlanDateRight(const value:Boolean);
var
 bValue: Boolean;
begin
   bValue:= value;
   if DM_EMPLOYEE.EE_BENEFICIARY.RecordCount = 0 then
      bValue:= false;

   edtPlanBeginDate.ReadOnly:= not bValue;
   edtPlanEndDate.ReadOnly:= not bValue;
   BeneficiaryPlanDateNavigation.SecurityRO:= not bValue;
end;

procedure TEDIT_EE_BENEFITS.sbAddAssingDependentClick(Sender: TObject);
var
  i: integer;
  bSSN, bDOB, bPCP :boolean;
  msg, msgAnd: string;
begin

  if (DM_CLIENT.EE_BENEFICIARY_PLAN_DATES.State = dsBrowse) and
     (not DM_CLIENT.EE_BENEFICIARY_PLAN_DATES.IsDataModified) then
  begin
    inherited;
    cdAvailableDependent.DisableControls;
    DM_EMPLOYEE.EE_BENEFICIARY.DisableControls;
    try
      for i := 0 to grdAvailableDependents.SelectedList.Count - 1 do
      begin
        cdAvailableDependent.GotoBookmark(grdAvailableDependents.SelectedList[i]);

        if not DM_EMPLOYEE.EE_BENEFICIARY.Locate('EE_BENEFITS_NBR;CL_PERSON_DEPENDENTS_NBR',
                                            VarArrayOf([cdAvailableDependent['EE_BENEFITS_NBR'], cdAvailableDependent['CL_PERSON_DEPENDENTS_NBR']]),[]) then
        begin
          if (cdDependentBenefits.FieldByName('REQUIRES_SSN').Value = REQUIRES_DOB_BOTH) or
                 (cdDependentBenefits.FieldByName('REQUIRES_SSN').Value = REQUIRES_DOB_DEPENDENT) then
            bSSN:= length(Trim(cdAvailableDependent.FieldByName('SOCIAL_SECURITY_NUMBER').asString)) <> 0
          else
            bSSN:= true;

          if (cdDependentBenefits.FieldByName('REQUIRES_DOB').Value = REQUIRES_DOB_BOTH) or
                 (cdDependentBenefits.FieldByName('REQUIRES_DOB').Value = REQUIRES_DOB_DEPENDENT) then
            bDOB:= not cdAvailableDependent.FieldByName('DATE_OF_BIRTH').IsNull
          else
            bDOB:= true;

          if (cdDependentBenefits.FieldByName('REQUIRES_PCP').Value = GROUP_BOX_YES) then
            bPCP:= length(Trim(cdAvailableDependent.FieldByName('PCP').asString)) <> 0
          else
            bPCP:= true;

          if bSSN and bDOB and bPCP then
          begin
            DM_EMPLOYEE.EE_BENEFICIARY.Insert;
            DM_EMPLOYEE.EE_BENEFICIARY.FieldbyName('EE_BENEFITS_NBR').asInteger :=
                               cdAvailableDependent.fieldByName('EE_BENEFITS_NBR').AsInteger;
            DM_EMPLOYEE.EE_BENEFICIARY.FieldbyName('CL_PERSON_DEPENDENTS_NBR').asInteger :=
                               cdAvailableDependent.fieldByName('CL_PERSON_DEPENDENTS_NBR').AsInteger;
            DM_EMPLOYEE.EE_BENEFICIARY.Post;

            cdAvailableDependent.Delete;

            SetPlanDateRight(false);
          end
          else
          begin
            msgAnd:= '';
            msg:= '';
            if not bSSN then
            begin
              msg := 'SSN';
              msgAnd:=' and ';
            end;
            if not bDOB then
            begin
              msg := msg + msgAnd +  'Date of Birth';
              msgAnd:=' and ';
            end;
            if not bPCP then
              msg :=  msg + msgAnd +  'Primary Care Physician';

            EvMessage('This benefit requires '+ msg + '. Please update the dependents before assigning.', mtInformation, [mbOK]);

            break;
          end;
        end;
      end;
      grdAvailableDependents.UnselectAll;
    finally
      cdAvailableDependent.EnableControls;
      DM_EMPLOYEE.EE_BENEFICIARY.EnableControls;
    end;
  end
  else
    EvMessage('Please commit plan date changes.', mtInformation, [mbOK]);

end;

procedure TEDIT_EE_BENEFITS.sbDelAssignDependentClick(Sender: TObject);
var
  i: integer;
  bPlanDates: boolean;
begin
  if (DM_CLIENT.EE_BENEFICIARY_PLAN_DATES.State = dsBrowse) and
     (not DM_CLIENT.EE_BENEFICIARY_PLAN_DATES.IsDataModified) then
  begin
    inherited;
    cdAvailableDependent.DisableControls;
    DM_EMPLOYEE.EE_BENEFICIARY.DisableControls;
    try
      bPlanDates:= False;
      for i := 0 to grdAssignedDependents.SelectedList.Count - 1 do
      begin
        DM_EMPLOYEE.EE_BENEFICIARY.GotoBookmark(grdAssignedDependents.SelectedList[i]);

        if DM_EMPLOYEE.EE_BENEFICIARY_PLAN_DATES.RecordCount = 0 then
        begin
          cdAvailableDependent.AppendRecord([DM_EMPLOYEE.EE_BENEFICIARY['EE_BENEFITS_NBR'], DM_EMPLOYEE.EE_BENEFICIARY['CL_PERSON_DEPENDENTS_NBR'],
                         DM_EMPLOYEE.EE_BENEFICIARY['FIRST_NAME'], DM_EMPLOYEE.EE_BENEFICIARY['LAST_NAME'],
                          ReturnDescription(Relationship_ComboChoices, DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('RELATION_TYPE').AsString),
                          DM_EMPLOYEE.EE_BENEFICIARY['DATE_OF_BIRTH'],DM_EMPLOYEE.EE_BENEFICIARY['SOCIAL_SECURITY_NUMBER'],
                          DM_EMPLOYEE.EE_BENEFICIARY['PCP']]);

          DM_EMPLOYEE.EE_BENEFICIARY.Delete;
          SetPlanDateRight(False);
        end
        else
          bPlanDates:= True;
      end;
      if bPlanDates then
         EvMessage('Please delete plan dates.', mtInformation, [mbOK]);

      grdAssignedDependents.UnselectAll;
    finally
      cdAvailableDependent.EnableControls;
      DM_EMPLOYEE.EE_BENEFICIARY.EnableControls;
    end;
  end
  else
    EvMessage('Please commit plan date changes.', mtInformation, [mbOK]);

end;

Procedure TEDIT_EE_BENEFITS.FillcdBeneficiaryBenefits;

  procedure ClearDataset(const cd: TevClientDataSet);
  begin
    cd.Filter :='';
    cd.Filtered :=false;
    cd.EmptyDataSet;
    if cd.FieldDefs.Count = 0 then
       cd.CreateDataSet;
  end;

var
   cd, cdCLPersonDependent: TevClientDataSet;
Begin
      FAssingBeneficiary :=false;
      cdBeneficiaryBenefits.DisableControls;
      cdPrimaryBeneficiaries.Aggregates.Clear;
      cdContingentBeneficiaries.Aggregates.Clear;
      cd := TevClientDataSet.Create(nil);
      cdCLPersonDependent := TevClientDataSet.Create(nil);
      try
          cd.CloneCursor(DM_EMPLOYEE.EE_BENEFITS, True);
          cdCLPersonDependent.CloneCursor(DM_CLIENT.CL_PERSON_DEPENDENTS, True);

          DM_COMPANY.CO_BENEFIT_SUBTYPE.CancelRange;

          ClearDataset(cdBeneficiaryBenefits);
          ClearDataset(cdPrimaryBeneficiaries);
          ClearDataset(cdContingentBeneficiaries);
          ClearDataset(cdABAvailableDependent);

          cdCLPersonDependent.Filter := 'CL_PERSON_NBR = ' + DM_CLIENT.EE.FieldByName('CL_PERSON_NBR').AsString;
          cdCLPersonDependent.Filtered := True;

          cd.Filter := 'EE_NBR = ' + DM_CLIENT.EE.FieldByName('EE_NBR').AsString + ' and CO_BENEFIT_SUBTYPE_NBR is not null ';
          cd.Filtered := True;
          cd.First;
          while not cd.Eof do
          begin
            if DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR', cd['CO_BENEFITS_NBR'], []) and
                 (DM_COMPANY.CO_BENEFITS.FieldByName('REQUIRES_BENEFICIARIES').AsString[1] = GROUP_BOX_YES) then
            begin
                  cdBeneficiaryBenefits.Append;
                  cdBeneficiaryBenefits['EE_BENEFITS_NBR'] := cd['EE_BENEFITS_NBR'];
                  cdBeneficiaryBenefits['CO_BENEFITS_NBR'] := cd['CO_BENEFITS_NBR'];
                  cdBeneficiaryBenefits['CO_BENEFIT_SUBTYPE_NBR'] := cd['CO_BENEFIT_SUBTYPE_NBR'];
                  cdBeneficiaryBenefits['BENEFIT_EFFECTIVE_DATE'] := cd['BENEFIT_EFFECTIVE_DATE'];
                  cdBeneficiaryBenefits['Benefit'] := DM_COMPANY.CO_BENEFITS['BENEFIT_NAME'];
                  cdBeneficiaryBenefits['REQUIRES_DOB'] := DM_COMPANY.CO_BENEFITS['REQUIRES_DOB'];
                  cdBeneficiaryBenefits['REQUIRES_SSN'] := DM_COMPANY.CO_BENEFITS['REQUIRES_SSN'];


                  if DM_COMPANY.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFIT_SUBTYPE_NBR', cd['CO_BENEFIT_SUBTYPE_NBR'], []) then
                     cdBeneficiaryBenefits['BenefitAmountType'] := DM_COMPANY.CO_BENEFIT_SUBTYPE['DESCRIPTION'];
                  cdBeneficiaryBenefits.Post;

                  cdCLPersonDependent.First;
                  while not cdCLPersonDependent.Eof do
                  begin
                    cdABAvailableDependent.AppendRecord([cd['EE_BENEFITS_NBR'], cdCLPersonDependent['CL_PERSON_DEPENDENTS_NBR'],
                           cdCLPersonDependent['FIRST_NAME'], cdCLPersonDependent['LAST_NAME'],
                           ReturnDescription(Relationship_ComboChoices, cdCLPersonDependent.fieldByName('RELATION_TYPE').AsString),
                           cdCLPersonDependent['DATE_OF_BIRTH'],cdCLPersonDependent['SOCIAL_SECURITY_NUMBER']]);
                    cdCLPersonDependent.Next;
                  end;

                  DM_EMPLOYEE.EE_BENEFICIARY.Filter := 'EE_BENEFITS_NBR = ' + cd.FieldByName('EE_BENEFITS_NBR').AsString;
                  DM_EMPLOYEE.EE_BENEFICIARY.Filtered :=true;
                  DM_EMPLOYEE.EE_BENEFICIARY.First;
                  while not DM_EMPLOYEE.EE_BENEFICIARY.Eof do
                  begin

                    if DM_EMPLOYEE.EE_BENEFICIARY.FieldByName('PRIMARY_BENEFICIARY').AsString = GROUP_BOX_YES then
                       cdPrimaryBeneficiaries.AppendRecord([DM_EMPLOYEE.EE_BENEFICIARY['EE_BENEFITS_NBR'], DM_EMPLOYEE.EE_BENEFICIARY['CL_PERSON_DEPENDENTS_NBR'],
                             DM_EMPLOYEE.EE_BENEFICIARY['FIRST_NAME'], DM_EMPLOYEE.EE_BENEFICIARY['LAST_NAME'],
                             ReturnDescription(Relationship_ComboChoices, DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('RELATION_TYPE').AsString),
                             DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('PERCENTAGE').AsInteger,
                             DM_EMPLOYEE.EE_BENEFICIARY['DATE_OF_BIRTH'],DM_EMPLOYEE.EE_BENEFICIARY['SOCIAL_SECURITY_NUMBER']])
                    else
                       cdContingentBeneficiaries.AppendRecord([DM_EMPLOYEE.EE_BENEFICIARY['EE_BENEFITS_NBR'], DM_EMPLOYEE.EE_BENEFICIARY['CL_PERSON_DEPENDENTS_NBR'],
                             DM_EMPLOYEE.EE_BENEFICIARY['FIRST_NAME'], DM_EMPLOYEE.EE_BENEFICIARY['LAST_NAME'],
                             ReturnDescription(Relationship_ComboChoices, DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('RELATION_TYPE').AsString),
                             DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('PERCENTAGE').AsInteger,
                             DM_EMPLOYEE.EE_BENEFICIARY['DATE_OF_BIRTH'],DM_EMPLOYEE.EE_BENEFICIARY['SOCIAL_SECURITY_NUMBER']]);

                    if cdABAvailableDependent.Locate('EE_BENEFITS_NBR;CL_PERSON_DEPENDENTS_NBR',
                         VarArrayOf([DM_EMPLOYEE.EE_BENEFICIARY['EE_BENEFITS_NBR'], DM_EMPLOYEE.EE_BENEFICIARY['CL_PERSON_DEPENDENTS_NBR']]),[]) then
                       cdABAvailableDependent.Delete;

                    DM_EMPLOYEE.EE_BENEFICIARY.Next;
                  end;
            end;
            cd.Next;
          end;
      finally
       cd.Free;
       cdCLPersonDependent.Free;

       if cdPrimaryBeneficiaries.Aggregates.IndexOf('aPERCENTAGE') = -1 then
           with cdPrimaryBeneficiaries.Aggregates.Add do
           begin
            AggregateName := 'aPERCENTAGE';
            Expression := 'Sum(PERCENTAGE)';
            Active := True;
           end;

       if cdContingentBeneficiaries.Aggregates.IndexOf('aPERCENTAGE') = -1 then
           with cdContingentBeneficiaries.Aggregates.Add do
           begin
            AggregateName := 'aPERCENTAGE';
            Expression := 'Sum(PERCENTAGE)';
            Active := True;
           end;
       cdPrimaryBeneficiaries.AggregatesActive := True;
       cdContingentBeneficiaries.AggregatesActive := True;
       DM_EMPLOYEE.EE_BENEFICIARY.Filter := '';
       DM_EMPLOYEE.EE_BENEFICIARY.Filtered :=false;
       cdBeneficiaryBenefits.First;
       cdBeneficiaryBenefits.EnableControls;
       FAssingBeneficiary :=true;
       grdPrimaryBeneficiaries.UnselectAll;
       grdContingentBeneficiaries.UnselectAll;
      end;
end;

function TEDIT_EE_BENEFITS.CheckPercentage: boolean;
begin
 result:= false;
 if PageControl1.ActivePage = tsAssignBeneficiaries then
   if (((ConvertNull(cdContingentBeneficiaries.Aggregates.Find('aPERCENTAGE').Value, 0) > 0) or
         (ConvertNull(cdPrimaryBeneficiaries.Aggregates.Find('aPERCENTAGE').Value, 0) > 0 )) and
         (ConvertNull(cdPrimaryBeneficiaries.Aggregates.Find('aPERCENTAGE').Value, 0) <> 100))
          or
         ((ConvertNull(cdContingentBeneficiaries.Aggregates.Find('aPERCENTAGE').Value, 0) > 0) and
           (ConvertNull(cdContingentBeneficiaries.Aggregates.Find('aPERCENTAGE').Value, 0) <> 100)) then
       result := true;
end;

procedure TEDIT_EE_BENEFITS.SetBenefitChange;
begin
   grdABBenefitSummary.Enabled := not CheckPercentage;
   pnlEEChoice.Enabled := not CheckPercentage;
   (Owner as TFramePackageTmpl).pnlNavigationButtons.Enabled:= not CheckPercentage;
end;

procedure TEDIT_EE_BENEFITS.dsBeneficiaryBenefitsDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;

  if dsBeneficiaryBenefits.Active and (dsBeneficiaryBenefits.DataSet.RecordCount > 0) then
  begin
    FAssingBeneficiary:= false;
    try
      if Assigned(dsPrimaryBeneficiaries.DataSet) then
      begin
        dsPrimaryBeneficiaries.DataSet.Filter := 'EE_BENEFITS_NBR = ' +
            dsBeneficiaryBenefits.DataSet.fieldByName('EE_BENEFITS_NBR').AsString;
        dsPrimaryBeneficiaries.DataSet.Filtered :=true;
      end;

      if Assigned(dsContingentBeneficiaries.DataSet) then
      begin
        dsContingentBeneficiaries.DataSet.Filter := 'EE_BENEFITS_NBR = ' +
            dsBeneficiaryBenefits.DataSet.fieldByName('EE_BENEFITS_NBR').AsString;
        dsContingentBeneficiaries.DataSet.Filtered :=true;
      end;

      if Assigned(cdABAvailableDependent) then
      begin
        cdABAvailableDependent.Filter := 'EE_BENEFITS_NBR = ' +
            dsBeneficiaryBenefits.DataSet.fieldByName('EE_BENEFITS_NBR').AsString;
        cdABAvailableDependent.Filtered :=true;
      end;

     finally
      FAssingBeneficiary:= true;
      SetBenefitChange;
      grdPrimaryBeneficiaries.UnselectAll;
      grdContingentBeneficiaries.UnselectAll;
     end;
    end;
end;


procedure TEDIT_EE_BENEFITS.dsContingentBeneficiariesDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  if FAssingBeneficiary then
    if Assigned(dsContingentBeneficiaries.DataSet) and
       (dsContingentBeneficiaries.DataSet.RecordCount > 0) then
       DM_EMPLOYEE.EE_BENEFICIARY.Locate('EE_BENEFITS_NBR;CL_PERSON_DEPENDENTS_NBR',
                                    VarArrayOf([dsContingentBeneficiaries.DataSet['EE_BENEFITS_NBR'],
                                          dsContingentBeneficiaries.DataSet['CL_PERSON_DEPENDENTS_NBR']]),[]);
end;

procedure TEDIT_EE_BENEFITS.dsPrimaryBeneficiariesDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  if FAssingBeneficiary then
    if Assigned(dsPrimaryBeneficiaries.DataSet) and
      (dsPrimaryBeneficiaries.DataSet.RecordCount > 0) then
          DM_EMPLOYEE.EE_BENEFICIARY.Locate('EE_BENEFITS_NBR;CL_PERSON_DEPENDENTS_NBR',
                                           VarArrayOf([dsPrimaryBeneficiaries.DataSet['EE_BENEFITS_NBR'],
                                             dsPrimaryBeneficiaries.DataSet['CL_PERSON_DEPENDENTS_NBR']]),[]);
end;

procedure TEDIT_EE_BENEFITS.grdPrimaryBeneficiariesDrawFooterCell(
  Sender: TObject; Canvas: TCanvas; FooterCellRect: TRect; Field: TField;
  FooterText: String; var DefaultDrawing: Boolean);
begin
  inherited;
  if cdPrimaryBeneficiaries.AggregatesActive  and (Field.FieldName = 'PERCENTAGE') then
  begin
     grdPrimaryBeneficiaries.ColumnByName('PERCENTAGE').FooterValue :=
         FormatFloat('#,##0.##', ConvertNull(cdPrimaryBeneficiaries.Aggregates.Find('aPERCENTAGE').Value, 0));
  end;

end;

procedure TEDIT_EE_BENEFITS.grdContingentBeneficiariesDrawFooterCell(
  Sender: TObject; Canvas: TCanvas; FooterCellRect: TRect; Field: TField;
  FooterText: String; var DefaultDrawing: Boolean);
begin
  inherited;
 if cdContingentBeneficiaries.AggregatesActive and (Field.FieldName = 'PERCENTAGE') then
  begin
     grdContingentBeneficiaries.ColumnByName('PERCENTAGE').FooterValue :=
         FormatFloat('#,##0.##', ConvertNull(cdContingentBeneficiaries.Aggregates.Find('aPERCENTAGE').Value, 0));
  end;
end;

procedure TEDIT_EE_BENEFITS.grdContingentBeneficiariesSelectionChanged(
  Sender: TObject);
begin
  inherited;
   if (grdContingentBeneficiaries.SelectedList.Count> 0) then
       grdPrimaryBeneficiaries.UnselectAll;
end;

procedure TEDIT_EE_BENEFITS.grdPrimaryBeneficiariesSelectionChanged(
  Sender: TObject);
begin
  inherited;
  if (grdPrimaryBeneficiaries.SelectedList.Count> 0) then
     grdContingentBeneficiaries.UnselectAll;
end;

procedure TEDIT_EE_BENEFITS.sbAddAssingBeneficiariesClick(Sender: TObject);
var
 i: integer;
 bSSN, bDOB :boolean;
 msg, msgAnd: string;
begin
  inherited;

  if (cdBeneficiaryBenefits.FieldByName('EE_BENEFITS_NBR').AsInteger > 0) and
     (grdABAvailableDependents.SelectedList.Count > 0 ) then
  begin
    cdABAvailableDependent.DisableControls;
    DM_EMPLOYEE.EE_BENEFICIARY.DisableControls;
    try
      for i := 0 to grdABAvailableDependents.SelectedList.Count - 1 do
      begin
        cdABAvailableDependent.GotoBookmark(grdABAvailableDependents.SelectedList[i]);

        if not DM_EMPLOYEE.EE_BENEFICIARY.Locate('EE_BENEFITS_NBR;CL_PERSON_DEPENDENTS_NBR',
                                            VarArrayOf([cdABAvailableDependent['EE_BENEFITS_NBR'], cdABAvailableDependent['CL_PERSON_DEPENDENTS_NBR']]),[]) then
        begin

          if rbPrimaryYes.Checked then
          begin
           if (ConvertNull(cdPrimaryBeneficiaries.Aggregates.Find('aPERCENTAGE').Value, 0) + edPercentage.Value > 100) then
              raise EUpdateError.CreateHelp('Total percentage cannot be more then 100.', IDH_ConsistencyViolation);
          end
          else
          if (ConvertNull(cdContingentBeneficiaries.Aggregates.Find('aPERCENTAGE').Value, 0) + edPercentage.Value > 100) then
              raise EUpdateError.CreateHelp('Total percentage cannot be more then 100.', IDH_ConsistencyViolation);

          if (cdBeneficiaryBenefits.FieldByName('REQUIRES_SSN').Value = REQUIRES_DOB_BOTH) or
             (cdBeneficiaryBenefits.FieldByName('REQUIRES_SSN').Value = REQUIRES_DOB_BENEFICIARY) then
            bSSN:= length(Trim(cdABAvailableDependent.FieldByName('SOCIAL_SECURITY_NUMBER').asString)) <> 0
          else
            bSSN:= True;

          if (cdBeneficiaryBenefits.FieldByName('REQUIRES_DOB').Value = REQUIRES_DOB_BOTH) or
             (cdBeneficiaryBenefits.FieldByName('REQUIRES_DOB').Value = REQUIRES_DOB_BENEFICIARY) then
            bDOB:= not cdABAvailableDependent.FieldByName('DATE_OF_BIRTH').IsNull
          else
            bDOB:= true;

          if bSSN and bDOB then
          begin

            DM_EMPLOYEE.EE_BENEFICIARY.Insert;
            DM_EMPLOYEE.EE_BENEFICIARY.FieldbyName('EE_BENEFITS_NBR').asInteger :=
                               cdABAvailableDependent.fieldByName('EE_BENEFITS_NBR').AsInteger;
            DM_EMPLOYEE.EE_BENEFICIARY.FieldbyName('CL_PERSON_DEPENDENTS_NBR').asInteger :=
                               cdABAvailableDependent.fieldByName('CL_PERSON_DEPENDENTS_NBR').AsInteger;

            DM_EMPLOYEE.EE_BENEFICIARY.FieldbyName('PERCENTAGE').asInteger :=  edPercentage.Value;

            if rbPrimaryYes.Checked then
               DM_EMPLOYEE.EE_BENEFICIARY.FieldbyName('PRIMARY_BENEFICIARY').AsString:=  GROUP_BOX_YES
            else
               DM_EMPLOYEE.EE_BENEFICIARY.FieldbyName('PRIMARY_BENEFICIARY').AsString:=  GROUP_BOX_NO;
            DM_EMPLOYEE.EE_BENEFICIARY.Post;

            if DM_EMPLOYEE.EE_BENEFICIARY.FieldByName('PRIMARY_BENEFICIARY').AsString = GROUP_BOX_YES then
               cdPrimaryBeneficiaries.AppendRecord([DM_EMPLOYEE.EE_BENEFICIARY['EE_BENEFITS_NBR'], DM_EMPLOYEE.EE_BENEFICIARY['CL_PERSON_DEPENDENTS_NBR'],
                     DM_EMPLOYEE.EE_BENEFICIARY['FIRST_NAME'], DM_EMPLOYEE.EE_BENEFICIARY['LAST_NAME'],
                     ReturnDescription(Relationship_ComboChoices, DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('RELATION_TYPE').AsString),
                     DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('PERCENTAGE').AsInteger,
                     cdABAvailableDependent['DATE_OF_BIRTH'],cdABAvailableDependent['SOCIAL_SECURITY_NUMBER']])
            else
               cdContingentBeneficiaries.AppendRecord([DM_EMPLOYEE.EE_BENEFICIARY['EE_BENEFITS_NBR'], DM_EMPLOYEE.EE_BENEFICIARY['CL_PERSON_DEPENDENTS_NBR'],
                     DM_EMPLOYEE.EE_BENEFICIARY['FIRST_NAME'], DM_EMPLOYEE.EE_BENEFICIARY['LAST_NAME'],
                     ReturnDescription(Relationship_ComboChoices, DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('RELATION_TYPE').AsString),
                     DM_EMPLOYEE.EE_BENEFICIARY.fieldByName('PERCENTAGE').AsInteger,
                     cdABAvailableDependent['DATE_OF_BIRTH'],cdABAvailableDependent['SOCIAL_SECURITY_NUMBER']]);

            cdABAvailableDependent.Delete;
          end
          else
          begin
            msgAnd:= '';
            msg:= '';
            if not bSSN then
            begin
              msg := 'SSN';
              msgAnd:=' and ';
            end;
            if not bDOB then
              msg := msg + msgAnd +  'Date of Birth';

            EvMessage('This benefit requires '+ msg + '. Please update the dependents before assigning.', mtInformation, [mbOK]);
            break;
          end;
        end;
      end;

      grdABAvailableDependents.UnselectAll;
    finally
      SetBenefitChange;
      cdABAvailableDependent.EnableControls;
      DM_EMPLOYEE.EE_BENEFICIARY.EnableControls;
    end;
  end
  else EvMessage('Please select Benefit and Beneficiary.');

end;

procedure TEDIT_EE_BENEFITS.sbRemoveAssingBeneficiariesClick(
  Sender: TObject);

  procedure RemoveFromBeneficiary(const cdb:TevClientDataSet);
  begin
    if DM_EMPLOYEE.EE_BENEFICIARY.Locate('EE_BENEFITS_NBR;CL_PERSON_DEPENDENTS_NBR',
                                 VarArrayOf([cdb['EE_BENEFITS_NBR'], cdb['CL_PERSON_DEPENDENTS_NBR']]),[]) then
    begin
      cdABAvailableDependent.AppendRecord([cdb['EE_BENEFITS_NBR'], cdb['CL_PERSON_DEPENDENTS_NBR'],
                   cdb['FIRST_NAME'], cdb['LAST_NAME'], cdb['RELATION_TYPE'],
                   cdb['DATE_OF_BIRTH'],cdb['SOCIAL_SECURITY_NUMBER']]);

      DM_EMPLOYEE.EE_BENEFICIARY.Delete;
      cdb.Delete;
    end;
  end;

var
  i: integer;
begin
  inherited;

  cdABAvailableDependent.DisableControls;
  try
     if grdPrimaryBeneficiaries.SelectedList.Count>0 then
     begin
        for i := 0 to grdPrimaryBeneficiaries.SelectedList.Count - 1 do
        begin
            cdPrimaryBeneficiaries.GotoBookmark(grdPrimaryBeneficiaries.SelectedList[i]);
            RemoveFromBeneficiary(cdPrimaryBeneficiaries);
        end;
     end
     else
     if grdContingentBeneficiaries.SelectedList.Count>0 then
     begin
        for i := 0 to grdContingentBeneficiaries.SelectedList.Count - 1 do
        begin
            cdContingentBeneficiaries.GotoBookmark(grdContingentBeneficiaries.SelectedList[i]);
            RemoveFromBeneficiary(cdContingentBeneficiaries);
        end;
     end;

     grdPrimaryBeneficiaries.UnselectAll;
     grdContingentBeneficiaries.UnselectAll;
  finally
    SetBenefitChange;
    cdABAvailableDependent.EnableControls;
  end;
end;

procedure TEDIT_EE_BENEFITS.BenefitPaymentFilterSetup;
begin
   if  wwdsDetail.DataSet.FieldByName('EE_NBR').AsInteger > 0 then
   begin
      DM_EMPLOYEE.EE_BENEFIT_PAYMENT.Filter :=  'EE_NBR = ' + IntToStr(wwdsDetail.DataSet.FieldByName('EE_NBR').AsInteger);
      BenefitPmtMiniNavigation.DataSource := dsBenefitPmnt;
   end
   else
   begin
    DM_EMPLOYEE.EE_BENEFIT_PAYMENT.Filter :=  'EE_NBR = -1';
    BenefitPmtMiniNavigation.DataSource := nil;
   end;
   DM_EMPLOYEE.EE_BENEFIT_PAYMENT.Filtered := True;

end;

procedure TEDIT_EE_BENEFITS.BenefitPmtMiniNavigationSpeedButton1Click(
  Sender: TObject);
begin
  if wwdsDetail.DataSet.fieldByName('EE_BENEFITS_NBR').AsInteger > 0 then
  begin
      inherited;
      if dsBenefitPmnt.DataSet.State = dsBrowse then
         edPaymentDate.SetFocus;

      BenefitPmtMiniNavigation.InsertRecordExecute(Sender);
      if dsBenefitPmnt.DataSet.State in [dsinsert] then
      begin
       dsBenefitPmnt.DataSet.FieldByName('BENEFIT_NAME').AsString := evBenefitReference.Text;
       dsBenefitPmnt.DataSet.FieldByName('BENEFIT_SUBTYPE').AsString :=  evBenefitSubtype.Text;
      end;
  end;
end;

procedure TEDIT_EE_BENEFITS.BenefitPmtMiniNavigationSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  BenefitPmtMiniNavigation.DeleteRecordExecute(Sender);
end;


Procedure TEDIT_EE_BENEFITS.cdBenefitAllActivate;
var
 s : String;
Begin
   s :=
      'select ''2'' ListType, c.EE_NBR, b.EE_BENEFITS_NBR, b.CO_BENEFITS_NBR, b.CO_BENEFIT_SUBTYPE_NBR, ' +
         'null EE_SCHEDULED_E_DS_NBR, null CL_E_DS_NBR, null CUSTOM_E_D_CODE_NUMBER, null CL_E_D_GROUPS_NBR, ' +
         'null DESCRIPTION, d.BENEFIT_NAME BenefitReference, null BenefitType, null CALCULATION_TYPE, ' +
         'null EFFECTIVE_START_DATE, null EFFECTIVE_END_DATE, b.BENEFIT_EFFECTIVE_DATE START_DATE, cast (b.BENEFIT_EFFECTIVE_DATE as date) END_DATE, ' +
         'b.TOTAL_PREMIUM_AMOUNT bTOTAL_PREMIUM_AMOUNT, b.EE_RATE bEE_RATE, b.ER_RATE bER_RATE, ' +
         'b.COBRA_RATE bCOBRA_RATE, ''Employee'' ReferenceType, null EE_OR_ER_BENEFIT, b.RATE_TYPE ' +
      'from  EE_BENEFITS b, EE c, CO_BENEFITS d ' +
      'where {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
             'd.CO_BENEFITS_NBR = b.CO_BENEFITS_NBR and d.CO_NBR = :conbr and ' +
             'c.EE_NBR = b.EE_NBR and b.EE_BENEFITS_NBR not in ( ' +
                    'select t1.EE_BENEFITS_NBR from  EE_SCHEDULED_E_DS t1 where {AsOfNow<t1>} and ' +
                    't1.EE_BENEFITS_NBR is not null and t1.EE_NBR = b.EE_NBR ) ' +
      'union ' +
      'select ''3'' ListType, a.EE_NBR, b.EE_BENEFITS_NBR, b.CO_BENEFITS_NBR, b.CO_BENEFIT_SUBTYPE_NBR, ' +
      'a.EE_SCHEDULED_E_DS_NBR, a.CL_E_DS_NBR,  e.CUSTOM_E_D_CODE_NUMBER, a.CL_E_D_GROUPS_NBR, ' +
      'e.DESCRIPTION, d.BENEFIT_NAME BenefitReference, null BenefitType, a.CALCULATION_TYPE, ' +
      'a.EFFECTIVE_START_DATE, a.EFFECTIVE_END_DATE, b.BENEFIT_EFFECTIVE_DATE START_DATE, cast (b.BENEFIT_EFFECTIVE_DATE as date) END_DATE, ' +
      'b.TOTAL_PREMIUM_AMOUNT bTOTAL_PREMIUM_AMOUNT, b.EE_RATE bEE_RATE, b.ER_RATE bER_RATE, ' +
      'b.COBRA_RATE bCOBRA_RATE, ''Employee'' ReferenceType, f.EE_OR_ER_BENEFIT, b.RATE_TYPE ' +
      'from EE_SCHEDULED_E_DS a, EE_BENEFITS b, EE c, CO_BENEFITS d, CL_E_DS e, CO_E_D_CODES f ' +
      'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
             '{AsOfNow<d>} and {AsOfNow<e>} and {AsOfNow<f>} and ' +
             'd.CO_BENEFITS_NBR = b.CO_BENEFITS_NBR and d.CO_NBR = :conbr and ' +
             'c.EE_NBR = a.EE_NBR and a.EE_BENEFITS_NBR = b.EE_BENEFITS_NBR and ' +
             'e.CL_E_DS_NBR = a.CL_E_DS_NBR and ' +
             'c.CO_NBR = f.CO_NBR and e.CL_E_DS_NBR = f.CL_E_DS_NBR and a.EE_BENEFITS_NBR is not null and ' +
             '(b.COBRA_RATE is not null or b.EE_RATE is not null or b.ER_RATE is not null) ' +
      'union ' +
      'select ''4'' ListType, a.EE_NBR, b.EE_BENEFITS_NBR, b.CO_BENEFITS_NBR,  b.CO_BENEFIT_SUBTYPE_NBR, ' +
      'a.EE_SCHEDULED_E_DS_NBR, a.CL_E_DS_NBR,  e.CUSTOM_E_D_CODE_NUMBER, a.CL_E_D_GROUPS_NBR, ' +
      'e.DESCRIPTION, d.BENEFIT_NAME BenefitReference, null BenefitType, a.CALCULATION_TYPE, ' +
      'a.EFFECTIVE_START_DATE, a.EFFECTIVE_END_DATE, b.BENEFIT_EFFECTIVE_DATE START_DATE, cast (b.BENEFIT_EFFECTIVE_DATE as date) END_DATE, ' +
      'b.TOTAL_PREMIUM_AMOUNT bTOTAL_PREMIUM_AMOUNT, b.EE_RATE bEE_RATE, b.ER_RATE bER_RATE, ' +
      'b.COBRA_RATE bCOBRA_RATE, ''Employee'' ReferenceType, f.EE_OR_ER_BENEFIT, null RATE_TYPE  ' +
      'from EE_SCHEDULED_E_DS a, EE_BENEFITS b, EE c, CO_BENEFITS d, CL_E_DS e, CO_E_D_CODES f  ' +
      'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
             '{AsOfNow<d>} and {AsOfNow<e>} and {AsOfNow<f>} and ' +
             'd.CO_BENEFITS_NBR = b.CO_BENEFITS_NBR and d.CO_NBR = :conbr and ' +
             'c.EE_NBR = a.EE_NBR and a.EE_BENEFITS_NBR = b.EE_BENEFITS_NBR and ' +
             'e.CL_E_DS_NBR = a.CL_E_DS_NBR and ' +
             'c.CO_NBR = f.CO_NBR and e.CL_E_DS_NBR = f.CL_E_DS_NBR and ' +
             'b.COBRA_RATE is null and b.EE_RATE is null and b.ER_RATE is null and a.EE_BENEFITS_NBR is not null ' +
      'union ' +
      'select ''5'' ListType, a.EE_NBR, null EE_BENEFITS_NBR, t2.CO_BENEFITS_NBR, t2.CO_BENEFIT_SUBTYPE_NBR, ' +
       'a.EE_SCHEDULED_E_DS_NBR, a.CL_E_DS_NBR,  e.CUSTOM_E_D_CODE_NUMBER, a.CL_E_D_GROUPS_NBR, ' +
       'e.DESCRIPTION, d.BENEFIT_NAME BenefitReference, t2.DESCRIPTION BenefitType, a.CALCULATION_TYPE, ' +
       'a.EFFECTIVE_START_DATE, a.EFFECTIVE_END_DATE, null START_DATE, null END_DATE, ' +
       'null bTOTAL_PREMIUM_AMOUNT, null bEE_RATE, null bER_RATE, null bCOBRA_RATE, ' +
       '''Company'' ReferenceType,  f.EE_OR_ER_BENEFIT, null RATE_TYPE  ' +
      'from EE_SCHEDULED_E_DS a ' +
       'join EE c on c.EE_NBR = a.EE_NBR and  c.CO_NBR = :conbr ' +
       'join CL_E_DS e on e.CL_E_DS_NBR = a.CL_E_DS_NBR ' +
       'join CO_E_D_CODES f on f.CL_E_DS_NBR = a.CL_E_DS_NBR and c.CO_NBR = f.CO_NBR ' +
       'LEFT JOIN CO_BENEFIT_SUBTYPE t2 ON ' +
          'a.CO_BENEFIT_SUBTYPE_NBR= t2.CO_BENEFIT_SUBTYPE_NBR and {AsOfNow<t2>} ' +
       'join CO_BENEFITS d on d.CO_BENEFITS_NBR = t2.CO_BENEFITS_NBR and d.CO_NBR = :conbr ' +
       'where {AsOfNow<a>} and {AsOfNow<c>} and {AsOfNow<d>} '+
             ' and {AsOfNow<e>} and {AsOfNow<f>} and a.CO_BENEFIT_SUBTYPE_NBR is not null ';

   cdBenefitAll.DisableControls;
   try
     if cdBenefitAll.Active then
        cdBenefitAll.Close;

     if DM_COMPANY.CO_BENEFIT_SUBTYPE.Active then
       DM_COMPANY.CO_BENEFIT_SUBTYPE.CancelRange;

     with TExecDSWrapper.Create(s) do
     begin
           cdBenefitAll.Filter := '';
           cdBenefitAll.Filtered := False;
           cdBenefitAll.Close;
           cdBenefitAll.AlwaysCallCalcFields := True;
           SetParam('conbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
           ctx_DataAccess.GetCustomData(cdBenefitAll, AsVariant);

           with cdBenefitAll.Aggregates.Add do
           begin
            AggregateName := 'aSum';
            Expression := 'Sum(AMOUNT)';
            Active := True;
           end;

           with cdBenefitAll.Aggregates.Add do
           begin
            AggregateName := 'eSum';
            Expression := 'Sum(eAMOUNT)';
            Active := True;
           end;
           with cdBenefitAll.Aggregates.Add do
           begin
            AggregateName := 'rSum';
            Expression := 'Sum(rAMOUNT)';
            Active := True;
           end;

          cdBenefitAll.AggregatesActive := True;
          SetBenefitAllFilter(0);
     end;
   finally

     s:= 'select a.CO_BENEFITS_NBR, a.BENEFIT_NAME, b.CO_BENEFIT_SUBTYPE_NBR, b.DESCRIPTION, ' +
           'c.CO_BENEFIT_RATES_NBR, c.PERIOD_BEGIN, c.PeriodEnd, c.EE_RATE, c.ER_RATE, c.COBRA_RATE ' +
          'from CO_BENEFITS a ' +
             'join (select b1.CO_BENEFITS_NBR, b1.CO_BENEFIT_SUBTYPE_NBR, b1.DESCRIPTION  from CO_BENEFIT_SUBTYPE b1 where {AsOfNow<b1>}) b ' +
                     ' on a.CO_BENEFITS_NBR = b.CO_BENEFITS_NBR ' +
             'join (select c1.CO_BENEFIT_RATES_NBR, c1.PERIOD_BEGIN, IIF(c1.PERIOD_END=''1/1/2100'',null,c1.PERIOD_END) PeriodEnd, c1.EE_RATE, c1.ER_RATE, c1.COBRA_RATE, c1.CO_BENEFIT_SUBTYPE_NBR from CO_BENEFIT_RATES c1 where {AsOfNow<c1>} ) ' +
               'c on c.CO_BENEFIT_SUBTYPE_NBR = b.CO_BENEFIT_SUBTYPE_NBR ' +
           'where {AsOfNow<a>} and a.CO_NBR = :conbr order by a.CO_BENEFITS_NBR, b.CO_BENEFIT_SUBTYPE_NBR, c.PERIOD_BEGIN ';
     with TExecDSWrapper.Create(s) do
     begin
         cdBenefitAmountTypeRates.Close;
         SetParam('conbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
         ctx_DataAccess.GetCustomData(cdBenefitAmountTypeRates, AsVariant);
     end;
     cdBenefitAll.EnableControls;
   end;
End;


Procedure TEDIT_EE_BENEFITS.SetBenefitAllFilter(SetTag : integer);
var
 s : String;
Begin
  if cdBenefitAll.Active then
  begin
   cdBenefitAll.Filtered := false;
   s := 'EE_NBR = ' + String(ConvertNull(DM_EMPLOYEE.EE['EE_NBR'], -1));
   if evcbOnlyActive.Checked then
      s := s + ' and (EFFECTIVE_END_DATE is null or EFFECTIVE_END_DATE >= ''' + DateToStr(Now) + ''') and ' +
                   ' (EFFECTIVE_START_DATE is null or EFFECTIVE_START_DATE < ''' + DateToStr(Now) + ''') ';

   cdBenefitAll.Filter := s;
   cdBenefitAll.Filtered := true;
   evBtnSwitchEE.Tag := SetTag;
  end;
End;

procedure TEDIT_EE_BENEFITS.cdBenefitAllCalcFields(DataSet: TDataSet);
begin
  inherited;
   if not DM_SERVICE_BUREAU.SB_AGENCY.Active then
      DM_SERVICE_BUREAU.SB_AGENCY.DataRequired('ALL');
   if not DM_CLIENT.CL_AGENCY.Active then
      DM_CLIENT.CL_AGENCY.DataRequired('ALL');
   if not DM_COMPANY.CO_BENEFIT_SUBTYPE.Active then
      DM_COMPANY.CO_BENEFIT_SUBTYPE.DataRequired('ALL');
   if not DM_COMPANY.CO_BENEFIT_RATES.Active then
      DM_COMPANY.CO_BENEFIT_RATES.DataRequired('ALL');

   if cdBenefitAllBenefitType.IsNull and
      (cdBenefitAllCO_BENEFIT_SUBTYPE_NBR.AsInteger > 0) and
       DM_COMPANY.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFIT_SUBTYPE_NBR',cdBenefitAllCO_BENEFIT_SUBTYPE_NBR.AsInteger,[]) then
       cdBenefitAllBenefitType.AsString := DM_COMPANY.CO_BENEFIT_SUBTYPE.FieldByName('DESCRIPTION').AsString;

   if (cdBenefitAllbTOTAL_PREMIUM_AMOUNT.IsNull) and (cdBenefitAllbEE_RATE.IsNull) and
       (cdBenefitAllbER_RATE.IsNull) and (cdBenefitAllbCOBRA_RATE.IsNull) and
       (cdBenefitAllCO_BENEFIT_SUBTYPE_NBR.AsInteger > 0)  then
   begin
     cdBenefitAllAmountType.AsString := 'Global';
     if (cdBenefitAllCO_BENEFIT_SUBTYPE_NBR.AsInteger > 0)  and
         GetCoBenefitRateNumber(cdBenefitAllCO_BENEFIT_SUBTYPE_NBR.AsInteger,
                                  ConvertNull(cdBenefitAll['START_DATE'],Now),
                                    ConvertNull(cdBenefitAll['END_DATE'], Now)) then
     begin
        if cdBenefitAllEE_OR_ER_BENEFIT.AsString = BENEFIT_DEDUCTION_TYPE_ER then
        begin
          cdBenefitAllAMOUNT.AsCurrency  := DM_COMPANY.CO_BENEFIT_RATES.fieldByName('ER_RATE').AsCurrency;
          cdBenefitAllrAMOUNT.AsCurrency := DM_COMPANY.CO_BENEFIT_RATES.fieldByName('ER_RATE').AsCurrency;
        end
        else
        if cdBenefitAllEE_OR_ER_BENEFIT.AsString = BENEFIT_DEDUCTION_TYPE_EE then
        begin
          cdBenefitAllAMOUNT.AsCurrency := DM_COMPANY.CO_BENEFIT_RATES.fieldByName('EE_RATE').AsCurrency;
          cdBenefitAlleAMOUNT.AsCurrency  := DM_COMPANY.CO_BENEFIT_RATES.fieldByName('EE_RATE').AsCurrency;
        end
        else
        begin
          cdBenefitAllAMOUNT.AsCurrency :=
                DM_COMPANY.CO_BENEFIT_RATES.fieldByName('ER_RATE').AsCurrency +
                 DM_COMPANY.CO_BENEFIT_RATES.fieldByName('EE_RATE').AsCurrency;

          cdBenefitAllrAMOUNT.AsCurrency := DM_CLIENT.CO_BENEFIT_RATES.fieldByName('ER_RATE').AsCurrency;
          cdBenefitAlleAMOUNT.AsCurrency := DM_CLIENT.CO_BENEFIT_RATES.fieldByName('EE_RATE').AsCurrency;
        end;
     end;
   end
   else
   begin
      cdBenefitAllAmountType.AsString := 'Individual';

      if cdBenefitAllEE_OR_ER_BENEFIT.AsString = BENEFIT_DEDUCTION_TYPE_ER then
      begin
        cdBenefitAllAMOUNT.AsFloat := cdBenefitAllbER_RATE.AsFloat;
        cdBenefitAllrAMOUNT.AsFloat := cdBenefitAllbER_RATE.AsFloat;
      end
      else
      if cdBenefitAllEE_OR_ER_BENEFIT.AsString = BENEFIT_DEDUCTION_TYPE_EE then
      begin
        cdBenefitAllAMOUNT.AsFloat := cdBenefitAllbEE_RATE.AsFloat;
        cdBenefitAlleAMOUNT.AsFloat := cdBenefitAllbEE_RATE.AsFloat;
      end
      else
      begin
        cdBenefitAllAMOUNT.AsFloat := cdBenefitAllbTOTAL_PREMIUM_AMOUNT.AsFloat;
        cdBenefitAllrAMOUNT.AsFloat := cdBenefitAllbER_RATE.AsFloat;
        cdBenefitAlleAMOUNT.AsFloat := cdBenefitAllbEE_RATE.AsFloat;
      end;
   end;

   if not cdBenefitAllCALCULATION_TYPE.IsNull then
      cdBenefitAllCALCULATION_TYPE_Desc.AsString :=
        ReturnDescription(ScheduledCalcMethod_ComboBoxChoices, cdBenefitAllCALCULATION_TYPE.AsString);

   if cdBenefitAllRATE_TYPE.IsNull then
   begin
     if (cdBenefitAllCO_BENEFIT_SUBTYPE_NBR.AsInteger > 0) and
         GetCoBenefitRateNumber(cdBenefitAllCO_BENEFIT_SUBTYPE_NBR.AsInteger,
                                   ConvertNull(cdBenefitAll['START_DATE'],Now),
                                    ConvertNull(cdBenefitAll['END_DATE'], Now)) then
           cdBenefitAllRATE_TYPE.AsString := DM_CLIENT.CO_BENEFIT_RATES.fieldbyName('RATE_TYPE').AsString;

     if cdBenefitAllRATE_TYPE.AsString = BENEFIT_RATES_RATE_TYPE_PERCENT then
     begin
        DataSet.FieldByName('AMOUNT').Value := Null;
        DataSet.FieldByName('rAMOUNT').Value := Null;
        DataSet.FieldByName('eAMOUNT').Value := Null;

        if (cdBenefitAllCO_BENEFIT_SUBTYPE_NBR.AsInteger > 0)  and
            GetCoBenefitRateNumber(cdBenefitAllCO_BENEFIT_SUBTYPE_NBR.AsInteger,
                                    ConvertNull(cdBenefitAll['START_DATE'],Now),
                                     ConvertNull(cdBenefitAll['END_DATE'], Now)) then
        begin
          if (cdBenefitAllReferenceType.AsString = 'Employee') and
              cdBenefitAllEE_OR_ER_BENEFIT.IsNull then
          begin
              cdBenefitAllEE_PERCENTAGE.AsCurrency:= DM_CLIENT.CO_BENEFIT_RATES.fieldbyName('EE_RATE').AsCurrency;
              cdBenefitAllER_PERCENTAGE.AsCurrency:= DM_CLIENT.CO_BENEFIT_RATES.fieldbyName('ER_RATE').AsCurrency;
          end
          else
           if cdBenefitAllEE_OR_ER_BENEFIT.AsString = BENEFIT_DEDUCTION_TYPE_EE then
              cdBenefitAllEE_PERCENTAGE.AsCurrency:= DM_CLIENT.CO_BENEFIT_RATES.fieldbyName('EE_RATE').AsCurrency
           else
              cdBenefitAllER_PERCENTAGE.AsCurrency:= DM_CLIENT.CO_BENEFIT_RATES.fieldbyName( 'ER_RATE').AsCurrency;
        end;
     end;
   end;
end;


Procedure TEDIT_EE_BENEFITS.SetDBEnabled(aValue : Boolean);

 procedure SetEnabled(const C: TWinControl);
 var
  i: Integer;
 begin
  for i := 0 to Pred(C.ControlCount) do
  begin
    if IsPublishedProp(C.Controls[i], 'Enabled') then
      SetOrdProp(C.Controls[i], 'Enabled',Integer(aValue));
    if C.Controls[i] is TWinControl then
      SetEnabled(TWinControl(C.Controls[i]));
  end;
 end;

Begin
    wwdsDetail.DataSet.DisableControls;
    try
      SetEnabled(evPnlEdits);

      wwcbDeduction_Frequency.Enabled := aValue;

      evBtnSwitchEE.Enabled := not aValue;
      evGrpEE.Enabled := aValue;

      if aValue and Assigned(wwdsDetail.DataSet) then
         evBtnUseClientBenefit.Enabled :=
            not (wwdsDetail.DataSet.FieldByName('EE_RATE').IsNull and
                wwdsDetail.DataSet.FieldByName('ER_RATE').IsNull and
                 wwdsDetail.DataSet.FieldByName('COBRA_RATE').IsNull and
                  wwdsDetail.DataSet.FieldByName('TOTAL_PREMIUM_AMOUNT').IsNull)
      else
         evBtnUseClientBenefit.Enabled := aValue;

      SetLabelsAndFields;
    finally
      wwdsDetail.DataSet.EnableControls;
    end;
end;

procedure TEDIT_EE_BENEFITS.PageControl1Change(Sender: TObject);
begin
  inherited;

  if PageControl1.ActivePage = tsBenefitPmnt then
     BenefitPaymentFilterSetup;

  if PageControl1.ActivePage = tsAssignDependent then
      FillcdDependentBenefits;

  if PageControl1.ActivePage = tsAssignBeneficiaries then
      FillcdBeneficiaryBenefits;

  if PageControl1.ActivePage = TabSheet1 then
  begin
    DM_EMPLOYEE.EE_BENEFITS.Filter :=  '';
    DM_EMPLOYEE.EE_BENEFITS.Filtered := false;
  end;

  if (PageControl1.ActivePage = tbshDetails) then
  begin
      if not cdBenefitAll.Active then
         cdBenefitAllActivate;
      BenefitFilterSetup;
      SetBenefitAllFilter(0);
      evBenefitReferenceChange(Sender);

      with HorzScrollBar do
      begin
        Range := 1035;
        Position := 0;
        Visible := True;
      end;
      with VertScrollBar do
      begin
        Range := 654;
        Position := 0;
        Visible := True;
      end;
  end
  else
  begin
      with HorzScrollBar do
      begin
        Range := 0;
        Position := 0;
        Visible := True;
      end;
      with VertScrollBar do
      begin
        Range := 0;
        Position := 0;
        Visible := True;
      end;
  end;
end;

procedure TEDIT_EE_BENEFITS.evBtnCompanyBenefitScreenClick(Sender: TObject);
begin
  inherited;
  ActivateFrameAs('Company - Benefits - Benefits', [wwdsDetail.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger], 1);
end;

procedure TEDIT_EE_BENEFITS.evBtnEEScheduledEDScreenClick(Sender: TObject);
begin
  inherited;
  ActivateFrameAs('Employee - Scheduled E/Ds', [wwdsDetail.DataSet.FieldByName('EE_NBR').AsInteger], 1);
end;

procedure TEDIT_EE_BENEFITS.wwdsEmployeeDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then exit;
  if Assigned(evBenefitReference) and Assigned(wwdsDetail.DataSet) and FActivated then
  begin

    if PageControl1.ActivePage <> TabSheet1 then
    begin
      BenefitFilterSetup;
      SetBenefitAllFilter(0);
    end;

    if PageControl1.ActivePage = tsAssignDependent then
      FillcdDependentBenefits;

    if PageControl1.ActivePage = tsAssignBeneficiaries then
      FillcdBeneficiaryBenefits;

    if PageControl1.ActivePage = tsBenefitPmnt then
      BenefitPaymentFilterSetup;
  end;
end;

procedure TEDIT_EE_BENEFITS.evBenefitReferenceChange(Sender: TObject);
begin
  inherited;
  if Assigned(evBenefitReference) and Assigned(evBenefitSubtype) and
     Assigned(evdsCOBenefitSubtype) and
     DM_COMPANY.CO_BENEFITS.Active and
     DM_COMPANY.CO_BENEFIT_SUBTYPE.Active and
     (PageControl1.ActivePage <> TabSheet1) and
     wwdsDetail.DataSet.Active then
  begin
      DM_COMPANY.CO_BENEFIT_SUBTYPE.IndexFieldNames := 'CO_BENEFITS_NBR';
      DM_COMPANY.CO_BENEFIT_SUBTYPE.SetRange([ConvertNull(wwdsDetail.DataSet['CO_BENEFITS_NBR'],-1)], [ConvertNull(wwdsDetail.DataSet['CO_BENEFITS_NBR'],-1)]);

      if (not DM_COMPANY.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFIT_SUBTYPE_NBR', VarArrayOf([wwdsDetail.DataSet['CO_BENEFIT_SUBTYPE_NBR']]), [])) or
          (evBenefitReference.Value = '') then
        evBenefitSubtype.Clear;
  end;
end;

procedure TEDIT_EE_BENEFITS.evBenefitReferenceCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    if dsCOBenefits.DataSet.Active then
    begin
      dsCOBenefits.DataSet.Locate('CO_BENEFITS_NBR', wwdsDetail.DataSet['CO_BENEFITS_NBR'], []);

      DM_COMPANY.CO_BENEFIT_SUBTYPE.IndexFieldNames := 'CO_BENEFITS_NBR';
      DM_COMPANY.CO_BENEFIT_SUBTYPE.SetRange([ConvertNull(wwdsDetail.DataSet['CO_BENEFITS_NBR'],-1)], [ConvertNull(wwdsDetail.DataSet['CO_BENEFITS_NBR'],-1)]);

      if (not DM_CLIENT.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFIT_SUBTYPE_NBR', VarArrayOf([wwdsDetail.DataSet['CO_BENEFIT_SUBTYPE_NBR']]), [])) or
          (evBenefitReference.Value = '') then
        evBenefitSubtype.Clear;
    end;
end;

procedure TEDIT_EE_BENEFITS.btnSelectBenefitAmountTypeClick(Sender: TObject);
var
  SelectBenefitAmountType: TTEDIT_SELECT_BENEFIT_AMOUNT;
begin
  inherited;
  SelectBenefitAmountType := TTEDIT_SELECT_BENEFIT_AMOUNT.Create(nil);
  try
    SelectBenefitAmountType.grdBenefitAmountType.Selected.Clear;
    SelectBenefitAmountType.grdBenefitAmountType.Selected.Add('BENEFIT_NAME'+#9+'30'+#9+'Benefit Name'+#9+'T');
    SelectBenefitAmountType.grdBenefitAmountType.Selected.Add('DESCRIPTION'+#9+'30'+#9+'Benefit Amount'+#9+'T');
    SelectBenefitAmountType.grdBenefitAmountType.Selected.Add('PERIOD_BEGIN'+#9+'12'+#9+'Eff. Start Date'+#9+'T');
    SelectBenefitAmountType.grdBenefitAmountType.Selected.Add('PeriodEnd'+#9+'12'+#9+'Eff. End Date'+#9+'T');
    SelectBenefitAmountType.grdBenefitAmountType.Selected.Add('EE_RATE'+#9+'10'+#9+'EE Amount'+#9+'T');
    SelectBenefitAmountType.grdBenefitAmountType.Selected.Add('ER_RATE'+#9+'10'+#9+'ER Amount'+#9+'T');
    SelectBenefitAmountType.grdBenefitAmountType.Selected.Add('COBRA_RATE'+#9+'10'+#9+'Cobra Amount'+#9+'T');
    SelectBenefitAmountType.dsSelectBenefitAmountType.DataSet := cdBenefitAmountTypeRates;
    SelectBenefitAmountType.FFilter := 'CO_BENEFITS_NBR = ' + String(ConvertNull(wwdsDetail.DataSet['CO_BENEFITS_NBR'],-1));
    SelectBenefitAmountType.RFilter := ' and (PERIOD_BEGIN >= ''' + DateToStr(Now) + ''' or PERIODEND >= ''' + DateToStr(Now) + ''') ';

    cdBenefitAmountTypeRates.Filter := SelectBenefitAmountType.FFilter + SelectBenefitAmountType.RFilter;
    cdBenefitAmountTypeRates.Filtered := True;

    if SelectBenefitAmountType.ShowModal = mrOK then
    begin
       if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
               wwdsDetail.DataSet.Edit;
       wwdsDetail.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger :=
               cdBenefitAmountTypeRates.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger;
       wwdsDetail.DataSet.FieldByName('BENEFIT_EFFECTIVE_DATE').Value :=
               cdBenefitAmountTypeRates.FieldByName('PERIOD_BEGIN').Value;
       wwdsDetail.DataSet.FieldByName('EXPIRATION_DATE').Value :=
               cdBenefitAmountTypeRates.FieldByName('PeriodEnd').Value;
    end;
    cdBenefitAmountTypeRates.Filter := '';
    cdBenefitAmountTypeRates.Filtered := False;
  finally
    SelectBenefitAmountType.Free;
  end;
end;

procedure TEDIT_EE_BENEFITS.evBtnUseClientBenefitClick(Sender: TObject);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet)  then
  begin
    if EvMessage('The Rates associated with this benefit will now look at the Company Benefit Amounts. Are you sure you want to continue?', mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
        if wwdsDetail.DataSet.State = dsBrowse then
           wwdsDetail.DataSet.Edit;
        wwdsDetail.DataSet.FieldByName('EE_RATE').Value := Null;
        wwdsDetail.DataSet.FieldByName('TOTAL_PREMIUM_AMOUNT').Value := Null;
        wwdsDetail.DataSet.FieldByName('EE_RATE').Value := Null;
        wwdsDetail.DataSet.FieldByName('ER_RATE').Value := Null;
        wwdsDetail.DataSet.FieldByName('COBRA_RATE').Value := Null;
        wwdsDetail.DataSet.FieldByName('CL_E_D_GROUPS_NBR').Value := Null;
        wwdsDetail.DataSet.FieldByName('RATE_TYPE').Value := BENEFIT_RATES_RATE_TYPE_AMOUNT;
    end;
  end;
end;

procedure TEDIT_EE_BENEFITS.SetLabelsAndFields;
begin

  if (wwdsDetail.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger > 0) and
       dsCOBenefits.DataSet.Locate('CO_BENEFITS_NBR', wwdsDetail.DataSet['CO_BENEFITS_NBR'], []) then
  begin
     evEditCoBenefitAddInfo1.Caption := DM_CLIENT.CO_BENEFITS.fieldByName('ADDITIONAL_INFO_TITLE1').AsString;
     evAddInfo1.Visible              := Length(Trim(DM_CLIENT.CO_BENEFITS.fieldByName('ADDITIONAL_INFO_TITLE1').AsString)) > 0;
     evEditCoBenefitAddInfo2.Caption := DM_CLIENT.CO_BENEFITS.fieldByName('ADDITIONAL_INFO_TITLE2').AsString;
     evAddInfo2.Visible              := Length(Trim(DM_CLIENT.CO_BENEFITS.fieldByName('ADDITIONAL_INFO_TITLE2').AsString)) > 0;
     evEditCoBenefitAddInfo3.Caption := DM_CLIENT.CO_BENEFITS.fieldByName('ADDITIONAL_INFO_TITLE3').AsString;
     evAddInfo3.Visible              := Length(Trim(DM_CLIENT.CO_BENEFITS.fieldByName('ADDITIONAL_INFO_TITLE3').AsString)) > 0;
  end
  else
  begin
     evEditCoBenefitAddInfo1.Caption := '';
     evAddInfo1.Visible              := false;
     evEditCoBenefitAddInfo2.Caption := '';
     evAddInfo2.Visible              := false;
     evEditCoBenefitAddInfo3.Caption := '';
     evAddInfo3.Visible              := false;
  end;
end;

procedure TEDIT_EE_BENEFITS.UpdateEEAmount;
Begin
      wwdsDetail.DataSet.UpdateRecord;
      if (not wwdsDetail.DataSet.FieldByName('EE_RATE').isNull) or
         (not wwdsDetail.DataSet.FieldByName('ER_RATE').isNull) then
      begin
         if wwdsDetail.DataSet.FieldByName('EE_RATE').isNull then
            wwdsDetail.DataSet['EE_RATE'] := ConvertNull(wwdsDetail.DataSet['EE_RATE'],0);

         if wwdsDetail.DataSet.FieldByName('ER_RATE').isNull then
            wwdsDetail.DataSet['ER_RATE'] := ConvertNull(wwdsDetail.DataSet['ER_RATE'],0);

         wwdsDetail.DataSet.FieldByName('TOTAL_PREMIUM_AMOUNT').Value :=
             ConvertNull(wwdsDetail.DataSet.FieldByName('EE_RATE').Value,0) +
             ConvertNull(wwdsDetail.DataSet.FieldByName('ER_RATE').Value,0);
      end;
End;

procedure TEDIT_EE_BENEFITS.evdbERAmountExit(Sender: TObject);
begin
  inherited;
    if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    begin
       if wwdsDetail.DataSet.FieldByName('RATE_TYPE').AsString = BENEFIT_RATES_RATE_TYPE_AMOUNT then
          UpdateEEAmount
       else
       begin
          wwdsDetail.DataSet.FieldByName('EE_RATE').AsFloat := 100 - wwdsDetail.DataSet.FieldByName('ER_RATE').AsFloat;
          wwdsDetail.DataSet.FieldByName('TOTAL_PREMIUM_AMOUNT').Value :=
                      wwdsDetail.DataSet.FieldByName('EE_RATE').Value + wwdsDetail.DataSet.FieldByName('ER_RATE').Value;
       end;
    end;
end;

procedure TEDIT_EE_BENEFITS.evdbEEAmountExit(Sender: TObject);
begin
  inherited;
    if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    begin
       if wwdsDetail.DataSet.FieldByName('RATE_TYPE').AsString = BENEFIT_RATES_RATE_TYPE_AMOUNT then
          UpdateEEAmount
       else
       begin
          wwdsDetail.DataSet.FieldByName('ER_RATE').AsFloat := 100 - wwdsDetail.DataSet.FieldByName('EE_RATE').AsFloat;
          wwdsDetail.DataSet.FieldByName('TOTAL_PREMIUM_AMOUNT').Value :=
                      wwdsDetail.DataSet.FieldByName('EE_RATE').Value + wwdsDetail.DataSet.FieldByName('ER_RATE').Value;
       end;
    end;
end;
procedure TEDIT_EE_BENEFITS.evcbOnlyActiveClick(Sender: TObject);
begin
  inherited;
  SetBenefitAllFilter(1);
end;

procedure TEDIT_EE_BENEFITS.evBtnSwitchEEClick(Sender: TObject);
begin
  inherited;
    evBtnSwitchEE.Tag := 1;
    wwdsDetail.DataSet.Insert;
    wwdsDetail.DataSet['EE_NBR'] := dsBenefitAll.DataSet['EE_NBR'];
    wwdsDetail.DataSet['CO_BENEFIT_SUBTYPE_NBR'] := dsBenefitAll.DataSet['CO_BENEFIT_SUBTYPE_NBR'];
    wwdsDetail.DataSet['CO_BENEFITS_NBR'] := dsBenefitAll.DataSet['CO_BENEFITS_NBR'];
    SetDBEnabled(True);
end;

procedure TEDIT_EE_BENEFITS.evdbEERateTypeChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.State in [dsEdit] then
  begin
    wwdsDetail.DataSet.FieldByName('ER_RATE').value := null;
    wwdsDetail.DataSet.FieldByName('EE_RATE').Value := null;
    wwdsDetail.DataSet.FieldByName('TOTAL_PREMIUM_AMOUNT').Value := null;
  end;
end;

procedure TEDIT_EE_BENEFITS.evDBGrid1DrawFooterCell(Sender: TObject;
  Canvas: TCanvas; FooterCellRect: TRect; Field: TField;
  FooterText: String; var DefaultDrawing: Boolean);
begin
  inherited;
  if cdBenefitAll.AggregatesActive and cdBenefitAll.Filtered then
  begin
     evDBGrid1.ColumnByName('AMOUNT').FooterValue :=
         FormatFloat('#,##0.##', ConvertNull(cdBenefitAll.Aggregates.Find('aSum').Value, 0));
     evDBGrid1.ColumnByName('eAMOUNT').FooterValue :=
         FormatFloat('#,##0.##', ConvertNull(cdBenefitAll.Aggregates.Find('eSum').Value, 0));
     evDBGrid1.ColumnByName('rAMOUNT').FooterValue :=
         FormatFloat('#,##0.##', ConvertNull(cdBenefitAll.Aggregates.Find('rSum').Value, 0));
  end;
end;

Procedure TEDIT_EE_BENEFITS.SetLocateBenefits(const RunAllTime: boolean=false);
begin
   if (Assigned(dsBenefitAll.DataSet) and
      not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) and
      (dsBenefitAll.DataSet.RecordCount > 0) and
       (cdBenefitAll.Filtered))  or RunAllTime then
   begin
         if cdBenefitAllEE_BENEFITS_NBR.AsInteger > 0 then
         begin
          if wwdsDetail.DataSet.State = dsBrowse  then
             wwdsDetail.DataSet.Locate('EE_BENEFITS_NBR', dsBenefitAll.DataSet['EE_BENEFITS_NBR'], []);
          SetDBEnabled(True);

          if wwdsDetail.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').IsNull or
             ((wwdsDetail.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger > 0)  and
              (not GetCoBenefitRateNumber(wwdsDetail.DataSet.FieldValues['CO_BENEFIT_SUBTYPE_NBR'],
                      ConvertNull(wwdsDetail.DataSet['BENEFIT_EFFECTIVE_DATE'],Now), ConvertNull(wwdsDetail.DataSet['EXPIRATION_DATE'], Now))))  then
          begin
              evdbCLAmount.Text := '';
              evdbCLEEAmount.Text := '';
              evdbCLERAmount.Text := '';
              evdbCOCobraAmount.Text := '';
          end;
         end
         else
         begin
          SetDBEnabled(False);

          evBenefitReference.Text := '';
          evBenefitSubtype.Text := '';

          edPrimaryCare.Text := '';
          wwcbDeduction_Frequency.Text := '';
          wwcbEnrollment_Status.Text :='';
          evEffectiveStart.Text := '';
          evEffectiveEnd.Text := '';

          evdbCLAmount.Text := '';
          evdbCLEEAmount.Text := '';
          evdbCLERAmount.Text := '';
          evdbAmount.Text := '';
          evdbEEAmount.Text := '';
          evdbERAmount.Text :='';

          EdQualifyingEventDate.Text :='';
          edRefusal.Text := '';
          evCobraEvent.Text := '';

          evdbCOCobraAmount.Text := '';
          evdbEECobraAmount.Text := '';
          evdbCOED_Group.Text := '';
          evdbEEED_Group.Text := '';

          evdbCORateType.Text := '';
          evdbEERateType.Text := '';

          evAddInfo1.Text := '';
          evAddInfo2.Text := '';
          evAddInfo3.Text := '';
         end;
   end;
end;

procedure TEDIT_EE_BENEFITS.dsBenefitAllDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  SetLocateBenefits;

end;


procedure TEDIT_EE_BENEFITS.dsDependentDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then exit;
  if DM_CLIENT.CL_PERSON_DEPENDENTS.Active and
     (DM_CLIENT.CL_PERSON_DEPENDENTS.RecordCount > 0)   then
   evPersonTypeChange(Sender);
end;

procedure TEDIT_EE_BENEFITS.evPersonTypeChange(Sender: TObject);
begin
  inherited;
  if evPersonType.Value <> '' then
  begin
    lblPCP.Visible := evPersonType.Value = PERSON_TYPE_DEPENDENT;
    edDependentPCP.Visible := lblPCP.Visible;
    edDependentExisitngPatient.Visible := lblPCP.Visible;
    edDependentFullTimeStudent.Visible := lblPCP.Visible;
    if lblPCP.Visible then
    begin
      if dsDependent.DataSet.State in [ dsEdit, dsInsert ] then
         if (edDependentRelation.Value <> REL_TYPE_PARTNER) and
            (edDependentRelation.Value <> REL_TYPE_SPOUSE) and
            (edDependentRelation.Value <> REL_TYPE_CHILD) and
            (edDependentRelation.Value <> REL_TYPE_STEPCHILD) and
            (edDependentRelation.Value <> REL_TYPE_GUARDIAN) and
            (edDependentRelation.Value <> REL_TYPE_STEPCHILD) then
           edDependentRelation.Value:= '';
      edDependentRelation.Items.Text := Relationship_ComboChoices
    end
    else
      edDependentRelation.Items.Text := Relationship_Beneficiary_ComboChoices;
    edDependentRelation.MapList := True;
  end;
end;

procedure TEDIT_EE_BENEFITS.DependentMiniNavigationSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  if dsDependent.DataSet.State = dsBrowse then
     evPersonType.SetFocus;

  DependentMiniNavigation.InsertRecordExecute(Sender);
end;

procedure TEDIT_EE_BENEFITS.DependentMiniNavigationSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  DependentMiniNavigation.DeleteRecordExecute(Sender);
end;

procedure TEDIT_EE_BENEFITS.evBtnCopyEEAddressClick(Sender: TObject);
begin
  inherited;

  if (dsDependent.DataSet.FieldByName('CL_PERSON_DEPENDENTS_NBR').AsInteger > 0) or
      (dsDependent.DataSet.State in [dsEdit,dsInsert]) then
  begin
   if dsDependent.DataSet.State = dsBrowse then
      dsDependent.DataSet.Edit;

   dsDependent.DataSet.FieldByName('ADDRESS1').Value := DM_CLIENT.CL_PERSON.FieldByName('ADDRESS1').Value;
   dsDependent.DataSet.FieldByName('ADDRESS2').Value := DM_CLIENT.CL_PERSON.FieldByName('ADDRESS2').Value;
   dsDependent.DataSet.FieldByName('CITY').Value := DM_CLIENT.CL_PERSON.FieldByName('CITY').Value;
   dsDependent.DataSet.FieldByName('STATE').Value := DM_CLIENT.CL_PERSON.FieldByName('STATE').Value;
   dsDependent.DataSet.FieldByName('ZIP_CODE').Value := DM_CLIENT.CL_PERSON.FieldByName('ZIP_CODE').Value;
  end;
end;

procedure TEDIT_EE_BENEFITS.edEEBenefitNbrChange(Sender: TObject);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and
      Assigned(evBtnUseClientBenefit) then
  begin
       evBtnUseClientBenefit.Enabled :=
          not (wwdsDetail.DataSet.FieldByName('EE_RATE').IsNull and
              wwdsDetail.DataSet.FieldByName('ER_RATE').IsNull and
               wwdsDetail.DataSet.FieldByName('COBRA_RATE').IsNull and
                wwdsDetail.DataSet.FieldByName('TOTAL_PREMIUM_AMOUNT').IsNull);

        evGrpCL.Caption := 'Global Amounts';
        evGrpEE.Caption := 'Individual Amounts';

        if not evBtnUseClientBenefit.Enabled then
             evGrpCL.Caption := '~' + evGrpCL.Caption
        else evGrpEE.Caption := '~' + evGrpEE.Caption;

        if Assigned(dsCOBenefits.DataSet) and dsCOBenefits.DataSet.Active then
           SetLabelsAndFields;

  end;
end;

procedure TEDIT_EE_BENEFITS.cbShowPayrollDetailsClick(Sender: TObject);
begin
  inherited;
      cdBenefitAllActivate;
      BenefitFilterSetup;
      evcbOnlyActive.Checked := cbShowPayrollDetails.Checked;
      evcbOnlyActive.Enabled := cbShowPayrollDetails.Checked;
      SetBenefitAllFilter(-1);
      evBenefitReferenceChange(Sender);
end;

procedure TEDIT_EE_BENEFITS.BeneficiaryPlanDateNavigationSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  if DM_CLIENT.EE_BENEFICIARY.fieldByName('EE_BENEFICIARY_NBR').AsInteger > 0 then
  begin
      inherited;
      if dsBeneficiaryPlanDate.DataSet.State = dsBrowse then
         edtPlanBeginDate.SetFocus;

      BeneficiaryPlanDateNavigation.InsertRecordExecute(Sender);

      if dsBeneficiaryPlanDate.DataSet.State in [dsinsert] then
       dsBeneficiaryPlanDate.DataSet.FieldByName('EE_BENEFICIARY_NBR').AsInteger :=
           DM_CLIENT.EE_BENEFICIARY.fieldByName('EE_BENEFICIARY_NBR').AsInteger;
  end;

end;

procedure TEDIT_EE_BENEFITS.BeneficiaryPlanDateNavigationSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  BeneficiaryPlanDateNavigation.DeleteRecordExecute(Sender);

end;

procedure TEDIT_EE_BENEFITS.edtPlanBeginDateClick(Sender: TObject);
begin
  inherited;
  if edtPlanBeginDate.ReadOnly then
  begin
      EvMessage('Please commit dependent changes to activate Plan Dates grid.', mtInformation, [mbOK]);
     grdAssignedDependents.SetFocus;
  end;   
end;

procedure TEDIT_EE_BENEFITS.dsBeneficiaryPlanDateStateChange(
  Sender: TObject);
begin
  inherited;
  if dsBeneficiaryPlanDate.DataSet.State = dsInsert then
     dsBeneficiaryPlanDate.DataSet.FieldByName('EE_BENEFICIARY_NBR').AsInteger :=
               DM_CLIENT.EE_BENEFICIARY.fieldByName('EE_BENEFICIARY_NBR').AsInteger;
end;

initialization
  RegisterClass( TEDIT_EE_BENEFITS );

end.
