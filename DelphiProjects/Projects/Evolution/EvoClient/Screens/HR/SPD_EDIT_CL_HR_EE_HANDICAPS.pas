// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_EE_HANDICAPS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CL_HR_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  wwdblook, SPD_EDIT_EE_HR_SUBBROWSE_BASE,  DBActns,
  ActnList, EvUIComponents, EvClientDataSet, SDataDictsystem, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CL_HR_EE_HANDICAPS = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    DM_SYSTEM_HR: TDM_SYSTEM_HR;
    fpDisabilityDetails: TisUIFashionPanel;
    lblHandicap: TevLabel;
    evDBLookupCombo1: TevDBLookupCombo;
    { Private declarations }
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;

  end;

implementation

uses SPD_EDIT_Open_BASE, SPD_EDIT_EE_HR_BASE;

{$R *.DFM}
{ TEDIT_CL_HR_EE_HANDICAPS }

function TEDIT_CL_HR_EE_HANDICAPS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_PERSON_HANDICAPS;
end;

function TEDIT_CL_HR_EE_HANDICAPS.GetInsertControl: TWinControl;
begin
  Result := evDBLookupCombo1;
end;

initialization
  RegisterClass( TEDIT_CL_HR_EE_HANDICAPS );


end.
