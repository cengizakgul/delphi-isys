// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_SENTRY;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, SPD_EDIT_CL_BASE, SDataStructure, Db, Wwdatsrc,
   StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, Mask, wwdbedit, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, wwdblook, Wwdotdot, Wwdbcomb, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, SFrameEntry, SDataDictbureau, EvContext, evUtils, isBasicUtils;

type
  TEDIT_CO_HR_SENTRY = class(TFrameEntry)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    odlgSentry: TOpenDialog;
    ScrollBox1: TScrollBox;
    fpHRSentry: TisUIFashionPanel;
    evLabel2: TevLabel;
    Button1: TevSpeedButton;
    bbtnSelectSentry: TevBitBtn;
    evDBEditSentry2: TevDBEdit;
    evDBEditSentry: TevDBEdit;
    procedure Button1Click(Sender: TObject);
    procedure bbtnSelectSentryClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Activate; override;
    procedure Deactivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

{ TEDIT_CO_HR_SENTRY }

procedure TEDIT_CO_HR_SENTRY.Deactivate;
begin
  inherited;
  ctx_dataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_USER]);
end;

procedure TEDIT_CO_HR_SENTRY.Activate;
begin
  inherited;
  DM_SERVICE_BUREAU.SB_USER.Locate('USER_ID', Context.UserAccount.User, []);
  //evDBEditSentry.UsePictureMask := false;
end;

procedure TEDIT_CO_HR_SENTRY.Button1Click(Sender: TObject);
var
  USER_NBR :integer;
  DataLink    :string;
begin
   USER_NBR := Context.UserAccount.InternalNbr;
   DM_SERVICE_BUREAU.SB_USER.Locate('SB_USER_NBR',User_NBR,[]);
   if DM_SERVICE_BUREAU.SB_USER.State =  dsEdit then
      DM_SERVICE_BUREAU.SB_USER.Post;
   DataLink :=DM_SERVICE_BUREAU.SB_USER.LINKS_DATA.Value;
   if Trim(DataLink) <> '' then
    RunIsolatedProcess(DataLink, '', []);
end;

procedure TEDIT_CO_HR_SENTRY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SERVICE_BUREAU.SB_USER,EditDataSets);
end;

procedure TEDIT_CO_HR_SENTRY.bbtnSelectSentryClick(Sender: TObject);
begin
  inherited;
  if(odlgSentry.Execute()) then
   begin
     evDBEditSentry.DataSource.DataSet.Edit;
     evDBEditSentry.Field.AsString := odlgSentry.FileName;
     evDBEditSentry.DataSource.DataSet.Post;
   end;
end;

initialization
  RegisterClass( TEDIT_CO_HR_SENTRY );
end.
