inherited EDIT_CL_HR_COURSE: TEDIT_CL_HR_COURSE
  inherited PageControl1: TevPageControl
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 0
              Width = 800
              BevelOuter = bvNone
              object sbSkillsBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlSkillsBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 433
                    Top = 6
                    Height = 549
                  end
                  object pnlSkillsBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 427
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpSkillsLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 415
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Client'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPSkillsLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 470
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlSkillsBrowseRight: TevPanel
                    Left = 436
                    Top = 6
                    Width = 354
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpSkillsRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 342
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Courses'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPSkillsRightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Top = 120
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Visible = False
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 348
                IniAttributes.SectionName = 'TEDIT_CL_HR_COURSE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Courses'
              object evDBGrid2: TevDBGrid
                Left = 18
                Top = 48
                Width = 351
                Height = 348
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F'
                  'STATE'#9'3'#9'State'#9'F'
                  'CITY'#9'20'#9'City'#9'F'
                  'ZIP_CODE'#9'10'#9'Zip code'#9'F'
                  'ADDRESS1'#9'30'#9'Address 1'#9'F'
                  'ADDRESS2'#9'30'#9'Address 2'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_HR_COURSE\evDBGrid2'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                OnDblClick = evDBGrid2DblClick
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tbshDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object evPanel1: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 182
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
      end
      object sbReasons: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 182
        Align = alClient
        TabOrder = 1
        object fpCourseDetails: TisUIFashionPanel
          Left = 8
          Top = 298
          Width = 331
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCourseDetails'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Course Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 44
            Height = 12
            Caption = '~Course'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 164
            Top = 35
            Width = 94
            Height = 13
            Caption = '~Renewal Status'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtName: TevDBEdit
            Left = 12
            Top = 50
            Width = 144
            Height = 21
            DataField = 'NAME'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBComboBox1: TevDBComboBox
            Left = 164
            Top = 50
            Width = 144
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'RENEWAL_STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
        object fpCourses: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 331
          Height = 282
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCourses'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object pnlCourseBody: TevPanel
            Left = 16
            Top = 36
            Width = 294
            Height = 223
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object evDBGrid1: TevDBGrid
              Left = 0
              Top = 0
              Width = 294
              Height = 223
              DisableThemesInTitle = False
              Selected.Strings = (
                'NAME'#9'21'#9'Name'#9'F'
                'RENEWAL_STATUS_DESC'#9'20'#9'Renewal Status'#9'F')
              IniAttributes.Enabled = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_CL_HR_COURSE\evDBGrid1'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = wwdsDetail
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
            object evPanel2: TevPanel
              Left = 0
              Top = 223
              Width = 294
              Height = 0
              Align = alBottom
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_HR_COURSE.CL_HR_COURSE
    MasterDataSource = wwdsMaster
  end
end
