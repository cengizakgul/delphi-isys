inherited EDIT_CL_HR_BASE: TEDIT_CL_HR_BASE
  inherited PageControl1: TevPageControl
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited pnlSubbrowse: TevPanel
              object evDBGrid1: TevDBGrid
                Left = 0
                Top = 0
                Width = 477
                Height = 565
                DisableThemesInTitle = False
                Selected.Strings = (
                  'FIRST_NAME'#9'20'#9'First Name'#9'F'
                  'LAST_NAME'#9'30'#9'Last Name'#9'F'
                  'SOCIAL_SECURITY_NUMBER'#9'11'#9'SSN'#9'F'
                  'STATE'#9'2'#9'State'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_HR_BASE\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                IniAttributes.SectionName = 'TEDIT_CL_HR_BASE\wwdbgridSelectClient'
              end
            end
          end
        end
      end
    end
    object tbshDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object pnlBrowseDetails: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 249
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object evDBGrid2: TevDBGrid
          Left = 0
          Top = 0
          Width = 427
          Height = 249
          DisableThemesInTitle = False
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CL_HR_BASE\evDBGrid2'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = wwdsDetail
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
      object pnlDBControls: TevPanel
        Left = 0
        Top = 249
        Width = 427
        Height = 70
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = wwdsSubMaster
    Left = 238
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_CL_PERSON.CL_PERSON
    MasterDataSource = wwdsMaster
    Left = 192
    Top = 24
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 416
    Top = 24
  end
end
