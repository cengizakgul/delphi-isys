inherited EDIT_CO_HR_CAR: TEDIT_CO_HR_CAR
  Width = 863
  Height = 634
  inherited Panel1: TevPanel
    Width = 863
    inherited pnlFavoriteReport: TevPanel
      Left = 711
    end
    inherited pnlTopLeft: TevPanel
      Width = 711
    end
  end
  inherited PageControl1: TevPageControl
    Width = 863
    Height = 580
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 855
        Height = 551
        inherited pnlBorder: TevPanel
          Width = 851
          Height = 547
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 851
          Height = 547
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbSkillsBrowse: TScrollBox
                inherited pnlSkillsBrowseBorder: TevPanel
                  inherited pnlSkillsBrowseRight: TevPanel
                    inherited fpSkillsRight: TisUIFashionPanel
                      Title = 'Vehicles'
                    end
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 803
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 803
            Height = 440
            inherited Splitter1: TevSplitter
              Height = 436
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 436
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 356
                IniAttributes.SectionName = 'TEDIT_CO_HR_CAR\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 480
              Height = 436
              Title = 'Vehicles'
              inherited evDBGrid1: TevDBGrid
                Width = 430
                Height = 356
                Selected.Strings = (
                  'COLOR'#9'1'#9'Color'#9'F'
                  'LICENSE_PLATE'#9'10'#9'License Plate'#9'F'
                  'MAKE'#9'20'#9'Make'#9'F'
                  'MODEL'#9'20'#9'Model'#9'F'
                  'PERSONAL_OR_COMPANY'#9'1'#9'Personal Or Company'#9'F'
                  'STATE'#9'3'#9'State'#9'F'
                  'VIN_NUMBER'#9'20'#9'VIN'#9'F'
                  'YEAR_MADE'#9'4'#9'Year Made'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_HR_CAR\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sbHR_BASE_DETAIL: TScrollBox
        Width = 855
        Height = 551
        inherited pnlevBorder: TevPanel
          Width = 669
          Height = 473
          Align = alNone
          inherited pnlFashionDetails: TisUIFashionPanel
            Width = 658
            inherited pnlFashionDetailBody: TevPanel
              Width = 619
              Height = 224
              inherited pnlBrowse: TevPanel
                Width = 619
                Height = 224
                inherited evDBGrid2: TevDBGrid
                  Width = 619
                  Height = 224
                  ControlType.Strings = (
                    'PERSONAL_OR_COMPANY;CustomEdit;cbPersonelORCompany;F'
                    'COLOR;CustomEdit;cbColor;F')
                  Selected.Strings = (
                    'PERSONAL_OR_COMPANY'#9'20'#9'Personal Or Company'#9'F'
                    'MAKE'#9'20'#9'Make'#9'F'
                    'MODEL'#9'20'#9'Model'#9'F'
                    'LICENSE_PLATE'#9'10'#9'License Plate'#9'F'
                    'STATE'#9'5'#9'State'#9'F'
                    'YEAR_MADE'#9'10'#9'Year Made'#9'F'
                    'COLOR'#9'7'#9'Color'#9'F'
                    'VIN_NUMBER'#9'20'#9'VIN'#9'F'
                    'CO_NBR'#9'10'#9'Co Nbr'#9'F')
                  IniAttributes.SectionName = 'TEDIT_CO_HR_CAR\evDBGrid2'
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                end
                object cbPersonelORCompany: TevDBComboBox
                  Left = 408
                  Top = 64
                  Width = 121
                  Height = 21
                  ShowButton = True
                  Style = csDropDownList
                  MapList = False
                  AllowClearKey = False
                  AutoDropDown = True
                  DataField = 'PERSONAL_OR_COMPANY'
                  DataSource = wwdsDetail
                  DropDownCount = 8
                  ItemHeight = 0
                  Picture.PictureMaskFromDataSet = False
                  Sorted = False
                  TabOrder = 1
                  UnboundDataType = wwDefault
                end
                object cbColor: TevDBComboBox
                  Left = 408
                  Top = 96
                  Width = 121
                  Height = 21
                  ShowButton = True
                  Style = csDropDownList
                  MapList = False
                  AllowClearKey = False
                  AutoDropDown = True
                  DataField = 'COLOR'
                  DataSource = wwdsDetail
                  DropDownCount = 8
                  ItemHeight = 0
                  Picture.PictureMaskFromDataSet = False
                  Sorted = False
                  TabOrder = 2
                  UnboundDataType = wwDefault
                end
              end
            end
          end
          inherited fpBaseDBControls: TisUIFashionPanel
            Width = 658
            Height = 171
            Title = 'Vehicle Details'
            inherited pnlFPDBControlsBody: TevPanel
              Width = 636
              Height = 118
              inherited pnlDBControls: TevPanel
                Width = 636
                Height = 118
                object evLabel1: TevLabel
                  Left = 0
                  Top = 39
                  Width = 27
                  Height = 13
                  Alignment = taRightJustify
                  Caption = 'Make'
                end
                object evLabel2: TevLabel
                  Left = 0
                  Top = 78
                  Width = 29
                  Height = 13
                  Alignment = taRightJustify
                  Caption = 'Model'
                end
                object evLabel3: TevLabel
                  Left = 450
                  Top = 39
                  Width = 24
                  Height = 13
                  Alignment = taRightJustify
                  Caption = 'Color'
                end
                object evLabel4: TevLabel
                  Left = 268
                  Top = 0
                  Width = 64
                  Height = 13
                  Alignment = taRightJustify
                  Caption = 'License Plate'
                end
                object evLabel5: TevLabel
                  Left = 268
                  Top = 39
                  Width = 25
                  Height = 13
                  Alignment = taRightJustify
                  Caption = 'State'
                end
                object evLabel6: TevLabel
                  Left = 268
                  Top = 78
                  Width = 18
                  Height = 13
                  Alignment = taRightJustify
                  Caption = 'VIN'
                end
                object evLabel8: TevLabel
                  Left = 450
                  Top = 0
                  Width = 52
                  Height = 13
                  Caption = 'Year Made'
                end
                object evDBEdit1: TevDBEdit
                  Left = 0
                  Top = 54
                  Width = 260
                  Height = 21
                  DataField = 'MAKE'
                  DataSource = wwdsDetail
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                  TabOrder = 1
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                end
                object evDBEdit2: TevDBEdit
                  Left = 0
                  Top = 93
                  Width = 260
                  Height = 21
                  DataField = 'MODEL'
                  DataSource = wwdsDetail
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                  TabOrder = 2
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                end
                object evDBEdit4: TevDBEdit
                  Left = 268
                  Top = 15
                  Width = 174
                  Height = 21
                  DataField = 'LICENSE_PLATE'
                  DataSource = wwdsDetail
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                  TabOrder = 3
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                end
                object evDBEdit5: TevDBEdit
                  Left = 268
                  Top = 54
                  Width = 174
                  Height = 21
                  DataField = 'STATE'
                  DataSource = wwdsDetail
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&,@}'
                  TabOrder = 4
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                end
                object evDBEdit6: TevDBEdit
                  Left = 268
                  Top = 93
                  Width = 355
                  Height = 21
                  DataField = 'VIN_NUMBER'
                  DataSource = wwdsDetail
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                  TabOrder = 5
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                end
                object evDBEdit8: TevDBEdit
                  Left = 450
                  Top = 15
                  Width = 174
                  Height = 21
                  DataField = 'YEAR_MADE'
                  DataSource = wwdsDetail
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                  TabOrder = 6
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                end
                object evDBRadioGroup1: TevDBRadioGroup
                  Left = 0
                  Top = 1
                  Width = 260
                  Height = 36
                  HelpContext = 19002
                  Caption = '~Personal or Company'
                  Columns = 2
                  DataField = 'PERSONAL_OR_COMPANY'
                  DataSource = wwdsDetail
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  Items.Strings = (
                    'Personal'
                    'Company')
                  ParentFont = False
                  TabOrder = 0
                  Values.Strings = (
                    'P'
                    'C')
                end
                object evDBComboBox1: TevDBComboBox
                  Left = 450
                  Top = 54
                  Width = 174
                  Height = 21
                  ShowButton = True
                  Style = csDropDownList
                  MapList = True
                  AllowClearKey = True
                  AutoDropDown = True
                  DataField = 'COLOR'
                  DataSource = wwdsDetail
                  DropDownCount = 8
                  ItemHeight = 0
                  Items.Strings = (
                    'Red'#9'R'
                    'Blue'#9'U'
                    'Black'#9'B'
                    'Silver'#9'S'
                    'Green'#9'G'
                    'Yellow'#9'Y'
                    'White'#9'W'
                    'Brown'#9'N'
                    'Gold'#9'D'
                    'Other'#9'O')
                  Picture.PictureMaskFromDataSet = False
                  Sorted = False
                  TabOrder = 7
                  UnboundDataType = wwDefault
                end
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_HR_CAR.CO_HR_CAR
  end
end
