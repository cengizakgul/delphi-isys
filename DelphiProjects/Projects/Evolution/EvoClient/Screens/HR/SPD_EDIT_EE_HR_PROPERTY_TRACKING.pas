// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_HR_PROPERTY_TRACKING;

interface                    

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_HR_BASE, Db,   DBActns,
  ActnList, SDataStructure, Wwdatsrc, StdCtrls, wwdblook, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, wwdbdatetimepicker,
  SPD_EDIT_EE_HR_SUBBROWSE_BASE, ISBasicClasses, kbmMemTable,
  ISKbmMemDataSet, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictclient, SDataDicttemp, Mask, wwdbedit, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBEdit, isUIwwDBDateTimePicker, ImgList, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_EE_HR_PROPERTY_TRACKING = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel1: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evDBDateTimePicker3: TevDBDateTimePicker;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evDBDateTimePicker4: TevDBDateTimePicker;
    evLabel5: TevLabel;
    dbedSerial: TevDBEdit;
    evLabel6: TevLabel;
    fpPropertyTracking: TisUIFashionPanel;
  private
    { Private declarations }
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;

  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    
  end;

implementation

uses SPD_EDIT_Open_BASE;

{$R *.DFM}

{ TEE_HR_PROPERTY_TRACKING }

procedure TEDIT_EE_HR_PROPERTY_TRACKING.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS( DM_COMPANY.CO_HR_PROPERTY, aDS );
end;

function TEDIT_EE_HR_PROPERTY_TRACKING.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_HR_PROPERTY_TRACKING;
end;

function TEDIT_EE_HR_PROPERTY_TRACKING.GetInsertControl: TWinControl;
begin
  Result := evDBLookupCombo1;
end;

initialization
  RegisterClass( TEDIT_EE_HR_PROPERTY_TRACKING );

end.
