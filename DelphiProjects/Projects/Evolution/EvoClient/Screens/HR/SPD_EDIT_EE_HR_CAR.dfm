inherited EDIT_EE_HR_CAR: TEDIT_EE_HR_CAR
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 747
        Height = 385
        inherited pnlBorder: TevPanel
          Width = 743
          Height = 381
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 743
          Height = 381
          inherited Panel3: TevPanel
            Width = 695
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 695
            Height = 274
            inherited Splitter1: TevSplitter
              Height = 270
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 270
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 190
                IniAttributes.SectionName = 'TEDIT_EE_HR_CAR\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 458
              Height = 270
              inherited wwDBGrid4: TevDBGrid
                Width = 408
                Height = 190
                IniAttributes.SectionName = 'TEDIT_EE_HR_CAR\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        Height = 149
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 429
          Height = 221
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 413
            Height = 205
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Width = 379
              inherited evDBGrid1: TevDBGrid
                Width = 379
                Selected.Strings = (
                  'CarDescr'#9'56'#9'Vehicle'#9'F')
                IniAttributes.SectionName = 'TEDIT_EE_HR_CAR\evDBGrid1'
              end
            end
          end
        end
        object pnlHREEVehicle: TisUIFashionPanel
          Left = 14
          Top = 223
          Width = 413
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlHREEVehicle'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Vehicle Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 35
            Height = 13
            Caption = 'Vehicle'
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 381
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'MAKE'#9'20'#9'Make'#9'F'
              'MODEL'#9'20'#9'Model'#9'F'
              'VIN_NUMBER'#9'20'#9'Vin number'#9'F'
              'YEAR_MADE'#9'4'#9'Year '#9'F'
              'COLOR'#9'1'#9'Color'#9'F'
              'STATE'#9'3'#9'State'#9'F'
              'LICENSE_PLATE'#9'10'#9'License'#9'F')
            DataField = 'CO_HR_CAR_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_HR_CAR.CO_HR_CAR
            LookupField = 'CO_HR_CAR_NBR'
            Options = [loTitles]
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
      inherited pnlDBControls: TevPanel
        Left = 4000
        Top = 78
        Align = alNone
        Visible = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_HR_CAR.EE_HR_CAR
  end
end
