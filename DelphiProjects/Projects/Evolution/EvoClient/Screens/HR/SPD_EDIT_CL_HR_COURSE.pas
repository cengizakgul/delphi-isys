// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_COURSE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, SPD_EDIT_CL_BASE, SDataStructure, Db, Wwdatsrc,
   StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, Mask, wwdbedit, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, wwdblook, Wwdotdot, Wwdbcomb, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CL_HR_COURSE = class(TEDIT_CL_BASE)
    tbshDetails: TTabSheet;
    evPanel1: TevPanel;
    evDBGrid1: TevDBGrid;
    evDBGrid2: TevDBGrid;
    sbReasons: TScrollBox;
    fpCourses: TisUIFashionPanel;
    pnlCourseBody: TevPanel;
    fpCourseDetails: TisUIFashionPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    dedtName: TevDBEdit;
    evDBComboBox1: TevDBComboBox;
    sbSkillsBrowse: TScrollBox;
    pnlSkillsBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlSkillsBrowseLeft: TevPanel;
    fpSkillsLeft: TisUIFashionPanel;
    pnlFPSkillsLeftBody: TevPanel;
    pnlSkillsBrowseRight: TevPanel;
    fpSkillsRight: TisUIFashionPanel;
    pnlFPSkillsRightBody: TevPanel;
    evPanel2: TevPanel;
    procedure evDBGrid2DblClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject); private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

uses SPD_EDIT_Open_BASE;

{$R *.DFM}

{ TEDIT_CL_HR_SCHOOL }

function TEDIT_CL_HR_COURSE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_COURSE;
end;

function TEDIT_CL_HR_COURSE.GetInsertControl: TWinControl;
begin
  Result := dedtName;
end;

procedure TEDIT_CL_HR_COURSE.evDBGrid2DblClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 1;
end;

procedure TEDIT_CL_HR_COURSE.Activate;
begin
  inherited;

end;

procedure TEDIT_CL_HR_COURSE.PageControl1Change(Sender: TObject);
begin
  inherited;
  //GUI 2.0 FIXED COLUMN WIDTHS
  evDBGrid1.Columns[0].DisplayWidth := 21;
  evDBGrid1.Columns[1].DisplayWidth := 21;
end;

initialization
  RegisterClass(TEDIT_CL_HR_COURSE);

end.
