inherited EDIT_CO_HR_DESCR_BASE: TEDIT_CO_HR_DESCR_BASE
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 658
        Height = 480
        inherited pnlBorder: TevPanel
          Width = 654
          Height = 476
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 654
          Height = 476
          inherited Panel3: TevPanel
            Width = 606
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 606
            Height = 369
            inherited Splitter1: TevSplitter
              Height = 365
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 365
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 285
                IniAttributes.SectionName = 'TEDIT_CO_HR_DESCR_BASE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 283
              Height = 365
              inherited evDBGrid1: TevDBGrid
                Width = 233
                Height = 285
                Selected.Strings = (
                  'DESCRIPTION'#9'40'#9'Description'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_HR_DESCR_BASE\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sbHR_BASE_DETAIL: TScrollBox
        inherited pnlevBorder: TevPanel
          inherited pnlFashionDetails: TisUIFashionPanel
            inherited pnlFashionDetailBody: TevPanel
              inherited pnlBrowse: TevPanel
                inherited evDBGrid2: TevDBGrid
                  Selected.Strings = (
                    'DESCRIPTION'#9'40'#9'Description'#9'F')
                  IniAttributes.SectionName = 'TEDIT_CO_HR_DESCR_BASE\evDBGrid2'
                end
              end
            end
          end
          inherited fpBaseDBControls: TisUIFashionPanel
            inherited pnlFPDBControlsBody: TevPanel
              inherited pnlDBControls: TevPanel
                object evLabel1: TevLabel
                  Left = 8
                  Top = 13
                  Width = 62
                  Height = 16
                  Caption = '~Description'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                end
                object dedtDescription: TevDBEdit
                  Left = 80
                  Top = 8
                  Width = 210
                  Height = 21
                  DataField = 'DESCRIPTION'
                  DataSource = wwdsDetail
                  TabOrder = 0
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                  Glowing = False
                end
              end
            end
          end
        end
      end
    end
  end
end
