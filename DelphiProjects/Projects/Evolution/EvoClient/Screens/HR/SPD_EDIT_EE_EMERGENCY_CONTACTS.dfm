inherited EDIT_EE_EMERGENCY_CONTACTS: TEDIT_EE_EMERGENCY_CONTACTS
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 706
        Height = 550
        inherited pnlBorder: TevPanel
          Width = 702
          Height = 546
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 686
          Height = 530
          inherited Panel3: TevPanel
            inherited bOpenFilteredByDBDTG: TevBitBtn
              Left = 131
            end
            inherited bOpenFilteredByEe: TevBitBtn
              Left = 339
            end
            inherited cbShowRemovedEmployees: TevCheckBox
              Left = 551
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                IniAttributes.SectionName = 'TEDIT_EE_EMERGENCY_CONTACTS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                IniAttributes.SectionName = 'TEDIT_EE_EMERGENCY_CONTACTS\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        Height = 149
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 564
          Height = 294
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 548
            Height = 278
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              inherited evDBGrid1: TevDBGrid
                Selected.Strings = (
                  'CONTACT_NAME'#9'40'#9'Contact Name'#9'F'
                  'CONTACT_PRIORITY'#9'10'#9'Contact Priority'#9'F'
                  'EMAIL_ADDRESS'#9'40'#9'Email Address'#9'F'
                  'FAX'#9'20'#9'Fax'#9'F'
                  'PAGER'#9'20'#9'Pager'#9'F'
                  'PRIMARY_PHONE'#9'20'#9'Primary Phone'#9'F'
                  'SECONDARY_PHONE'#9'20'#9'Secondary Phone'#9'F'
                  'ADDITIONAL_INFO'#9'80'#9'Additional Info'#9'F')
                IniAttributes.SectionName = 'TEDIT_EE_EMERGENCY_CONTACTS\evDBGrid1'
              end
            end
          end
        end
        object fpEmergencyContact: TisUIFashionPanel
          Left = 8
          Top = 296
          Width = 548
          Height = 171
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Emergency Contact Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 97
            Height = 13
            Caption = '~Name'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 405
            Top = 35
            Width = 113
            Height = 13
            Caption = '~Priority'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 12
            Top = 74
            Width = 137
            Height = 13
            Caption = '~Primary Phone'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 12
            Top = 113
            Width = 85
            Height = 13
            Caption = 'Secondary Phone'
          end
          object evLabel5: TevLabel
            Left = 141
            Top = 74
            Width = 28
            Height = 13
            Caption = 'Pager'
          end
          object evLabel6: TevLabel
            Left = 141
            Top = 113
            Width = 17
            Height = 13
            Caption = 'Fax'
          end
          object evLabel7: TevLabel
            Left = 270
            Top = 74
            Width = 69
            Height = 13
            Caption = 'E-mail Address'
          end
          object evLabel8: TevLabel
            Left = 270
            Top = 113
            Width = 67
            Height = 13
            Caption = 'Additional Info'
          end
          object evDBEdit1: TevDBEdit
            Left = 12
            Top = 50
            Width = 385
            Height = 21
            DataField = 'CONTACT_NAME'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit2: TevDBEdit
            Left = 405
            Top = 50
            Width = 121
            Height = 21
            DataField = 'CONTACT_PRIORITY'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit3: TevDBEdit
            Left = 12
            Top = 89
            Width = 121
            Height = 21
            DataField = 'PRIMARY_PHONE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit4: TevDBEdit
            Left = 12
            Top = 128
            Width = 121
            Height = 21
            DataField = 'SECONDARY_PHONE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit5: TevDBEdit
            Left = 141
            Top = 89
            Width = 121
            Height = 21
            DataField = 'PAGER'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit6: TevDBEdit
            Left = 141
            Top = 128
            Width = 121
            Height = 21
            DataField = 'FAX'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit7: TevDBEdit
            Left = 270
            Top = 89
            Width = 256
            Height = 21
            DataField = 'EMAIL_ADDRESS'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit8: TevDBEdit
            Left = 270
            Top = 128
            Width = 256
            Height = 21
            DataField = 'ADDITIONAL_INFO'
            DataSource = wwdsDetail
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
      inherited pnlDBControls: TevPanel
        Left = 4000
        Align = alNone
        Visible = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_EE_EMERGENCY_CONTACTS.EE_EMERGENCY_CONTACTS
  end
end
