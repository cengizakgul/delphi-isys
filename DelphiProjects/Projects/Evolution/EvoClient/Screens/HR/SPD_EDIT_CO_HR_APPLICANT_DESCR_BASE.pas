// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_APPLICANT_DESCR_BASE;

interface

uses                                        
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_HR_APPLICANT_BASE, SDataStructure, Db, Wwdatsrc,
   StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, EvUtils, DBActns, ActnList, 
  SDDClasses, wwdblook, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, EvUIComponents, EvClientDataSet, ImgList,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_HR_APPLICANT_DESCR_BASE = class(TEDIT_CO_HR_APPLICANT_BASE)
    tshtDetails: TTabSheet;
    pnlBrowseDetails: TevPanel;
    evDBGrid2: TevDBGrid;
    pnlDBControls: TevPanel;
    wwdsSubDetail: TevDataSource;
    sbAppDescBaseDetail: TScrollBox;
    fpAppDescBaseSummary: TisUIFashionPanel;
  private
    { Private declarations }
  protected
    procedure AfterDataSetsReopen; override;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

{ TEDIT_CO_HR_APPLICANT_DESCR_BASE }

{ TEDIT_CO_HR_APPLICANT_DESCR_BASE }

{ TEDIT_CO_HR_APPLICANT_DESCR_BASE }

procedure TEDIT_CO_HR_APPLICANT_DESCR_BASE.AfterDataSetsReopen;
begin
  inherited;
  SetReadOnly(GetIfReadOnly or BlockHr);
end;

end.
