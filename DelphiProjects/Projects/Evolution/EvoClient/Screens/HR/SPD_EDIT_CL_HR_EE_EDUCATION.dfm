inherited EDIT_CL_HR_EE_EDUCATION: TEDIT_CL_HR_EE_EDUCATION
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Width = 595
              Height = 314
            end
            inherited pnlSubbrowse: TevPanel
              Width = 270
              Height = 312
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 254
                IniAttributes.SectionName = 'TEDIT_CL_HR_EE_EDUCATION\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited wwDBGrid4: TevDBGrid
                Height = 254
                IniAttributes.SectionName = 'TEDIT_CL_HR_EE_EDUCATION\wwDBGrid4'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sb_EE_HR_SUBBrowse: TScrollBox
        inherited pnl_EE_HR_SubbrowseBorderHeader: TevPanel
          Width = 695
          Height = 233
          BorderWidth = 8
          inherited fp_EE_HR_Subbrowse: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 679
            inherited pnl_EE_HR_SubbrowseBody: TevPanel
              Width = 643
              Height = 161
              inherited evDBGrid1: TevDBGrid
                Width = 643
                Height = 161
                Selected.Strings = (
                  'SchoolName'#9'42'#9'School Name'#9'F'
                  'MAJOR'#9'35'#9'Major'#9'F'
                  'DEGREE_LEVEL'#9'1'#9'Degree Level'#9'F'
                  'GPA'#9'10'#9'Gpa'#9'F'
                  'START_DATE'#9'18'#9'Start Date'#9'F'
                  'END_DATE'#9'18'#9'End Date'#9'F'
                  'GRADUATION_DATE'#9'18'#9'Graduation Date'#9'F'
                  'CL_HR_SCHOOL_NBR'#9'10'#9'Nbr Sb Hr School'#9'F')
                IniAttributes.SectionName = 'TEDIT_CL_HR_EE_EDUCATION\evDBGrid1'
              end
            end
          end
        end
        object fpEducation: TisUIFashionPanel
          Left = 8
          Top = 232
          Width = 680
          Height = 170
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpEducation'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Education Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 44
            Height = 13
            Caption = '~School'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 12
            Top = 74
            Width = 26
            Height = 13
            Caption = 'Major'
          end
          object evLabel4: TevLabel
            Left = 143
            Top = 113
            Width = 22
            Height = 13
            Caption = 'GPA'
          end
          object evLabel5: TevLabel
            Left = 240
            Top = 35
            Width = 46
            Height = 13
            Caption = 'Start date'
          end
          object evLabel6: TevLabel
            Left = 240
            Top = 74
            Width = 43
            Height = 13
            Caption = 'End date'
          end
          object evLabel7: TevLabel
            Left = 240
            Top = 113
            Width = 76
            Height = 13
            Caption = 'Graduation date'
          end
          object evLabel8: TevLabel
            Left = 12
            Top = 113
            Width = 64
            Height = 13
            Caption = 'Degree Level'
          end
          object evLabel2: TevLabel
            Left = 369
            Top = 35
            Width = 28
            Height = 13
            Caption = 'Notes'
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 220
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F')
            DataField = 'CL_HR_SCHOOL_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_HR_SCHOOL.CL_HR_SCHOOL
            LookupField = 'CL_HR_SCHOOL_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBEdit2: TevDBEdit
            Left = 12
            Top = 89
            Width = 220
            Height = 21
            DataField = 'MAJOR'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBEdit3: TevDBEdit
            Left = 143
            Top = 128
            Width = 89
            Height = 21
            DataField = 'GPA'
            DataSource = wwdsDetail
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 240
            Top = 50
            Width = 121
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'START_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 240
            Top = 89
            Width = 121
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'END_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 5
          end
          object evDBDateTimePicker3: TevDBDateTimePicker
            Left = 240
            Top = 128
            Width = 121
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'GRADUATION_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 6
          end
          object evDBComboBox1: TevDBComboBox
            Left = 12
            Top = 128
            Width = 123
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'DEGREE_LEVEL'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object EvDBMemo1: TEvDBMemo
            Left = 369
            Top = 50
            Width = 288
            Height = 99
            DataField = 'NOTES'
            DataSource = wwdsDetail
            TabOrder = 7
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_HR_PERSON_EDUCATION.CL_HR_PERSON_EDUCATION
    MasterDataSource = wwdsSubMaster2
  end
end
