object SelEEs: TSelEEs
  Left = 438
  Top = 242
  BorderStyle = bsDialog
  Caption = 'Apply rate/salary changes'
  ClientHeight = 381
  ClientWidth = 498
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  DesignSize = (
    498
    381)
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 8
    Top = 8
    Width = 30
    Height = 13
    Caption = 'Period'
  end
  object OKBtn: TButton
    Left = 171
    Top = 347
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
  end
  object CancelBtn: TButton
    Left = 251
    Top = 347
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object evDBGrid1: TevDBGrid
    Left = 8
    Top = 32
    Width = 481
    Height = 305
    DisableThemesInTitle = False
    Selected.Strings = (
      'CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'EE Code'#9'F'
      'Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F'
      'Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F'
      'SOCIAL_SECURITY_NUMBER'#9'13'#9'SSN'#9'F'
      'CURRENT_TERMINATION_CODE_DESC'#9'40'#9'Status'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TSelEEs\evDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = evDataSource1
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 2
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object dtBegin: TevDateTimePicker
    Left = 46
    Top = 5
    Width = 97
    Height = 21
    Date = 37823.000000000000000000
    Time = 37823.000000000000000000
    TabOrder = 0
  end
  object dtEnd: TevDateTimePicker
    Left = 150
    Top = 5
    Width = 97
    Height = 21
    Date = 37823.000000000000000000
    Time = 37823.000000000000000000
    TabOrder = 1
  end
  object evDataSource1: TevDataSource
    Left = 40
    Top = 344
  end
end
