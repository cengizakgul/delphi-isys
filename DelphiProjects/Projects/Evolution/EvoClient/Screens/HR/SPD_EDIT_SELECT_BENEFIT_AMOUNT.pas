// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SELECT_BENEFIT_AMOUNT;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFieldCodeValues, Mask, wwdbedit, Wwdotdot, Wwdbcomb, 
  StdCtrls, ComCtrls, EvUtils, SDataStructure, Variants, EvTypes,
  ISBasicClasses, Spin, ExtCtrls, evConsts, Grids, Wwdbigrd, Wwdbgrid, DB,
  Wwdatsrc, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, ISBasicUtils,
  EvDataAccessComponents, SDDClasses, SDataDicttemp, isBaseClasses,
  Buttons, ImgList, EvStreamUtils, EvUIComponents, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel;

type
  TTEDIT_SELECT_BENEFIT_AMOUNT = class(TForm)
    dsSelectBenefitAmountType: TDataSource;
    grdBenefitAmountType: TevDBGrid;
    fpControls: TisUIFashionPanel;
    btnApply: TevBitBtn;
    btnCancel: TevBitBtn;
    cbShowOtherYears: TevCheckBox;
    procedure cbShowOtherYearsClick(Sender: TObject);
  private
    { Private declarations }
  public
    FFilter: String;
    RFilter: String;
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TTEDIT_SELECT_BENEFIT_AMOUNT.cbShowOtherYearsClick(
  Sender: TObject);
begin

 if cbShowOtherYears.Checked then
   dsSelectBenefitAmountType.DataSet.Filter :=  FFilter
 else
   dsSelectBenefitAmountType.DataSet.Filter := FFilter +  RFilter;
 dsSelectBenefitAmountType.DataSet.Filtered := True;
end;

end.
