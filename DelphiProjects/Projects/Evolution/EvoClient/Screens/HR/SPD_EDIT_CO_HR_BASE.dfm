inherited EDIT_CO_HR_BASE: TEDIT_CO_HR_BASE
  Width = 684
  Height = 595
  inherited Panel1: TevPanel
    Width = 684
    inherited pnlFavoriteReport: TevPanel
      Left = 532
    end
    inherited pnlTopLeft: TevPanel
      Width = 532
      inherited Label5: TevLabel
        Top = 26
      end
      inherited DBText1: TevDBText
        Top = 25
      end
      inherited CompanyNameText: TevDBText
        Top = 25
      end
    end
  end
  inherited PageControl1: TevPageControl
    Width = 684
    Height = 541
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 676
        Height = 512
        inherited pnlBorder: TevPanel
          Width = 672
          Height = 508
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 672
          Height = 508
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbSkillsBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlSkillsBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlSkillsBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpSkillsLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPSkillsLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlSkillsBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpSkillsRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Employee'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPSkillsRightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Top = 118
            end
          end
          inherited Panel3: TevPanel
            Width = 624
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 624
            Height = 401
            inherited Splitter1: TevSplitter
              Left = 316
              Height = 397
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 316
              Height = 397
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 266
                Height = 317
                IniAttributes.SectionName = 'TEDIT_CO_HR_BASE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 319
              Width = 301
              Height = 397
              Align = alClient
              Visible = True
              object evDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 251
                Height = 317
                DisableThemesInTitle = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_HR_BASE\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                OnDblClick = evDBGrid1DblClick
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tbshDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbHR_BASE_DETAIL: TScrollBox
        Left = 0
        Top = 0
        Width = 676
        Height = 512
        Align = alClient
        TabOrder = 0
        object pnlevBorder: TevPanel
          Left = 0
          Top = 0
          Width = 672
          Height = 508
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          ParentColor = True
          TabOrder = 0
          object pnlFashionDetails: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 322
            Height = 282
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlFashionDetailBody: TevPanel
              Left = 12
              Top = 36
              Width = 290
              Height = 226
              Align = alClient
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object pnlBrowse: TevPanel
                Left = 0
                Top = 0
                Width = 290
                Height = 226
                Align = alClient
                BevelOuter = bvNone
                Caption = 'pnlBrowse'
                ParentColor = True
                TabOrder = 0
                object evDBGrid2: TevDBGrid
                  Left = 0
                  Top = 0
                  Width = 290
                  Height = 226
                  DisableThemesInTitle = False
                  IniAttributes.Enabled = False
                  IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                  IniAttributes.SectionName = 'TEDIT_CO_HR_BASE\evDBGrid2'
                  IniAttributes.Delimiter = ';;'
                  ExportOptions.ExportType = wwgetSYLK
                  ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                  TitleColor = clBtnFace
                  FixedCols = 0
                  ShowHorzScrollBar = True
                  Align = alClient
                  DataSource = wwdsDetail
                  TabOrder = 0
                  TitleAlignment = taLeftJustify
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  TitleLines = 1
                  PaintOptions.AlternatingRowColor = 14544093
                  PaintOptions.ActiveRecordColor = clBlack
                  NoFire = False
                end
              end
            end
          end
          object fpBaseDBControls: TisUIFashionPanel
            Left = 8
            Top = 298
            Width = 322
            Height = 156
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpBaseDBControls'
            Color = 14737632
            TabOrder = 1
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Details'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlFPDBControlsBody: TevPanel
              Left = 12
              Top = 35
              Width = 298
              Height = 97
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object pnlDBControls: TevPanel
                Left = 0
                Top = 0
                Width = 298
                Height = 97
                Align = alClient
                BevelOuter = bvNone
                ParentColor = True
                TabOrder = 0
              end
            end
          end
        end
      end
    end
  end
end
