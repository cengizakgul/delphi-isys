inherited EDIT_CO_HR_SALARY_GRADES: TEDIT_CO_HR_SALARY_GRADES
  Width = 646
  Height = 601
  inherited Panel1: TevPanel
    Width = 646
    inherited pnlFavoriteReport: TevPanel
      Left = 494
    end
    inherited pnlTopLeft: TevPanel
      Width = 494
    end
  end
  inherited PageControl1: TevPageControl
    Width = 646
    Height = 547
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 807
        Height = 569
        inherited pnlBorder: TevPanel
          Width = 803
          Height = 565
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 803
          Height = 565
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbSkillsBrowse: TScrollBox
                inherited pnlSkillsBrowseBorder: TevPanel
                  inherited pnlSkillsBrowseRight: TevPanel
                    inherited fpSkillsRight: TisUIFashionPanel
                      Title = 'Pay Grades'
                    end
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 755
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 755
            Height = 458
            inherited Splitter1: TevSplitter
              Height = 454
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 454
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 374
                IniAttributes.SectionName = 'TEDIT_CO_HR_SALARY_GRADES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 432
              Height = 454
              Title = 'Pay Grades'
              inherited evDBGrid1: TevDBGrid
                Width = 382
                Height = 374
                Selected.Strings = (
                  'DESCRIPTION'#9'40'#9'Description'#9'F'
                  'MAXIMUM_PAY'#9'10'#9'Maximum Pay'#9'F'
                  'MID_PAY'#9'10'#9'Mid Pay'#9'F'
                  'MINIMUM_PAY'#9'10'#9'Minimum Pay'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_HR_SALARY_GRADES\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sbHR_BASE_DETAIL: TScrollBox
        Width = 638
        Height = 518
        inherited pnlevBorder: TevPanel
          Width = 383
          Height = 434
          Align = alNone
          inherited pnlFashionDetails: TisUIFashionPanel
            Width = 372
            inherited pnlFashionDetailBody: TevPanel
              Width = 340
              inherited pnlBrowse: TevPanel
                Width = 340
                inherited evDBGrid2: TevDBGrid
                  Width = 340
                  Selected.Strings = (
                    'DESCRIPTION'#9'16'#9'Description'#9'F'
                    'MAXIMUM_PAY'#9'10'#9'Maximum Pay'#9'F'
                    'MID_PAY'#9'10'#9'Mid Pay'#9'F'
                    'MINIMUM_PAY'#9'10'#9'Minimum Pay'#9'F')
                  IniAttributes.SectionName = 'TEDIT_CO_HR_SALARY_GRADES\evDBGrid2'
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                end
              end
            end
          end
          inherited fpBaseDBControls: TisUIFashionPanel
            Width = 372
            Height = 132
            Title = 'Pay Grade Details'
            inherited pnlFPDBControlsBody: TevPanel
              Width = 337
              Height = 75
              inherited pnlDBControls: TevPanel
                Width = 337
                Height = 75
                inherited evLabel1: TevLabel
                  Left = 0
                  Top = 0
                end
                object evLabel2: TevLabel [1]
                  Left = 0
                  Top = 39
                  Width = 62
                  Height = 13
                  Caption = 'Minimum Pay'
                end
                object evLabel3: TevLabel [2]
                  Left = 108
                  Top = 39
                  Width = 38
                  Height = 13
                  Caption = 'Mid Pay'
                end
                object evLabel4: TevLabel [3]
                  Left = 237
                  Top = 39
                  Width = 65
                  Height = 13
                  Caption = 'Maximum Pay'
                end
                inherited dedtDescription: TevDBEdit
                  Left = 0
                  Top = 15
                  Width = 337
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                end
                object evDBEdit1: TevDBEdit
                  Left = 0
                  Top = 54
                  Width = 100
                  Height = 21
                  DataField = 'MINIMUM_PAY'
                  DataSource = wwdsDetail
                  TabOrder = 1
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                  Glowing = False
                end
                object evDBEdit2: TevDBEdit
                  Left = 108
                  Top = 54
                  Width = 100
                  Height = 21
                  DataField = 'MID_PAY'
                  DataSource = wwdsDetail
                  TabOrder = 2
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                  Glowing = False
                end
                object evDBEdit3: TevDBEdit
                  Left = 237
                  Top = 54
                  Width = 100
                  Height = 21
                  DataField = 'MAXIMUM_PAY'
                  DataSource = wwdsDetail
                  TabOrder = 3
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                  Glowing = False
                end
                object evButton1: TevBitBtn
                  Left = 208
                  Top = 54
                  Width = 21
                  Height = 21
                  TabOrder = 4
                  OnClick = evButton1Click
                  Color = clBlack
                  Glyph.Data = {
                    36060000424D3606000000000000360000002800000020000000100000000100
                    18000000000000060000120B0000120B00000000000000000000FFFFFFDCDCDC
                    CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
                    CCCCCCCCD3D3D3FFFFFFFFFFFFDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
                    CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCD4D4D4FFFFFFFFFFFFB0B0B0
                    9F9F9F9E9E9E9E9E9E9E9F9F9E9E9E9E9E9E9E9F9F9E9E9E9D9E9F9D9E9F9D9E
                    9E9F9F9FAAAAAAFFFFFFFFFFFFB1B0B0A09F9F9F9F9E9F9F9EA09F9F9F9F9E9F
                    9F9EA09F9F9F9F9E9F9F9E9F9F9E9F9F9EA09F9FAAAAAAFFFFFFFFFFFF9F9F9F
                    FFFEFEFBFAFAFEFDFDFEFDFDFDFCFCFEFDFDFEFDFEFBFCFDF9FCFFF9FCFFF9FA
                    FCFFFEFE9F9F9FFFFFFFFFFFFFA09F9FFFFFFFFCFCFBFEFEFEFEFEFEFEFDFDFE
                    FEFEFFFFFFFEFDFDFEFEFEFEFEFEFCFCFBFFFFFFA09F9FFFFFFFFFFFFF9E9E9E
                    F7F6F6F2F1F08D8A886D6B68F7F6F58D8B896C6B69F2F6F9E49F55E49F55EEF2
                    F5F7F6F79E9E9EFFFFFFFFFFFF9F9F9EF7F7F7F3F2F28A8A8A6A6A6AF7F7F78B
                    8B8B6B6B6BF8F8F8999999999999F4F4F4F8F8F89F9F9EFFFFFFFFFFFF9D9E9E
                    F4F3F2EDECEBF2F1F0F3F3F2F0F0EFF3F2F1F3F3F3EDF1F6EEAD66E4A25BE9ED
                    F2F4F4F39D9E9EFFFFFFFFFFFF9F9F9EF4F4F4EDEDEDF3F2F2F4F4F4F1F1F1F3
                    F3F3F4F4F4F3F3F3A7A7A79C9C9CF0EFEFF5F5F59F9F9EFFFFFFFFFFFF9E9E9E
                    F3F2F0E9E8E78E8C8A6E6C6AEDECEB8F8D8A6E6C6BE9ECF0F0AC62F0AC62E5E8
                    ECF2F2F19E9E9EFFFFFFFFFFFF9F9F9EF3F3F3E9E9E98C8C8C6C6C6CEDEDED8D
                    8D8D6C6C6CEEEEEEA6A6A6A6A6A6EAEAEAF3F3F39F9F9EFFFFFFFFFFFF9E9E9E
                    F0EEEEE4E2E2E9E7E7EAE8E9E8E6E6E9E7E8EAE8E9E6E5E7E5E6EBE4E6EBE2E2
                    E4F0EEEE9E9E9EFFFFFFFFFFFF9F9F9EF0EFEFE4E3E3E8E8E8EAEAEAE8E7E7E8
                    E8E8EAEAEAE7E7E6E8E8E8E8E8E8E4E3E3F0EFEF9F9F9EFFFFFFFFFFFF9E9F9F
                    EEEDEDE0DEDD918E8A716F6BE5E2E2918F8C706E6CE4E2E2918F8C706E6AE1DF
                    DEEEEDEE9E9F9FFFFFFFFFFFFFA09F9FEFEEEEE0DFDF8E8E8E6E6E6EE4E3E38F
                    8F8F6D6D6DE4E3E38F8F8F6D6D6DE0E0E0EFEEEEA09F9FFFFFFFFFFFFF9F9F9F
                    EEECECDDD9D5E4DED6E5DFD7E0DDD9E0DEDDE1DFDEE1DDD9E5DFD7E5DFD6DDD9
                    D5EEEDEC9F9F9FFFFFFFFFFFFFA09F9FEDEDEDDADADADEDEDEDFDFDEDEDEDEE0
                    DFDFE0E0E0DEDEDEDFDFDEDFDFDEDADADAEEEEEEA09F9FFFFFFFFFFFFF9F9F9F
                    EDECEADAD5CE1F7FFF2266FFDDD8D1928F8C716F6CDED8D22080FF2265FFDAD5
                    CEEEECEA9F9F9FFFFFFFFFFFFFA09F9FEDEDEDD5D5D5A6A6A69A9A9AD8D8D88F
                    8F8F6E6E6ED8D8D8A6A6A6999999D5D5D5EDEDEDA09F9FFFFFFFFFFFFFA0A1A1
                    F1F0F0F1EFEDF6F1ECF6F2ECF2F1EFF1F1F2F2F2F3F2F1EFF6F2ECF6F2ECF1F0
                    EDF1F0F0A0A1A1FFFFFFFFFFFFA1A1A1F2F2F1F0F0F0F2F2F1F3F2F2F2F2F1F3
                    F2F2F3F3F3F2F2F1F3F2F2F3F2F2F1F1F1F2F2F1A1A1A1FFFFFFFFFFFFA2A3A5
                    7C7E7F7B7D807C7E807C7E817B7E817B7E827B7E827B7E817C7E817C7E807B7D
                    807C7E7FA2A3A5FFFFFFFFFFFFA4A4A47F7E7E7E7E7D7F7E7E7F7E7E7F7E7E80
                    807F80807F7F7E7E7F7E7E7F7E7E7E7E7D7F7E7EA4A4A4FFFFFFFFFFFFA0A3A7
                    FFCA8BF9C27EF9C27FF9C280F9C280F9C280F9C280F9C280F9C280F9C27FF9C2
                    7EFFCA8BA0A3A7FFFFFFFFFFFFA4A4A4C4C3C3BBBBBBBBBBBBBBBBBBBBBBBBBB
                    BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC4C3C3A4A4A4FFFFFFFFFFFF9EA2A6
                    F9D0A4E8A760E7A762E7A863E7A863E7A863E7A863E7A863E7A863E7A762E8A7
                    60F9D0A49EA2A6FFFFFFFFFFFFA3A3A3CBCBCBA1A0A0A1A0A0A1A1A1A1A1A1A1
                    A1A1A1A1A1A1A1A1A1A1A1A1A0A0A1A0A0CBCBCBA3A3A3FFFFFFFFFFFF9FA2A5
                    FDDBB6F8DAB8F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DA
                    B8FDDBB69FA2A5FFFFFFFFFFFFA3A3A3D7D7D7D7D6D6D7D6D6D7D6D6D7D6D6D7
                    D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D7D7A3A3A3FFFFFFFFFFFFA9AAAB
                    9FA2A59EA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59EA1
                    A59FA2A5A9AAABFFFFFFFFFFFFABABABA3A3A3A2A2A2A2A2A2A2A2A2A2A2A2A2
                    A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A3A3A3ABABABFFFFFF}
                  NumGlyphs = 2
                  Margin = 0
                end
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_HR_SALARY_GRADES.CO_HR_SALARY_GRADES
  end
end
