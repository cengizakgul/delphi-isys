// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_BASE;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, EvUtils,
  SDDClasses, ISBasicClasses, SDataDictclient, SDataDicttemp, EvUIComponents,
  ImgList, LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_HR_BASE = class(TEDIT_CO_BASE)
    evDBGrid1: TevDBGrid;
    tbshDetails: TTabSheet;
    pnlDBControls: TevPanel;
    pnlBrowse: TevPanel;
    evDBGrid2: TevDBGrid;
    sbHR_BASE_DETAIL: TScrollBox;
    pnlevBorder: TevPanel;
    pnlFashionDetails: TisUIFashionPanel;
    pnlFashionDetailBody: TevPanel;
    sbSkillsBrowse: TScrollBox;
    pnlSkillsBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlSkillsBrowseLeft: TevPanel;
    fpSkillsLeft: TisUIFashionPanel;
    pnlFPSkillsLeftBody: TevPanel;
    pnlSkillsBrowseRight: TevPanel;
    fpSkillsRight: TisUIFashionPanel;
    pnlFPSkillsRightBody: TevPanel;
    fpBaseDBControls: TisUIFashionPanel;
    pnlFPDBControlsBody: TevPanel;
    procedure evDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure AfterDataSetsReopen; override;
  public
    { Public declarations }
    procedure Activate; override;
  end;

implementation

uses SPD_EDIT_Open_BASE;

{$R *.DFM}
{ TEDIT_CO_HR_BASE }

{ TEDIT_CO_HR_BASE }

procedure TEDIT_CO_HR_BASE.AfterDataSetsReopen;
begin
  inherited;
  SetReadOnly(GetIfReadOnly or BlockHr);
end;

procedure TEDIT_CO_HR_BASE.evDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 1;
end;

procedure TEDIT_CO_HR_BASE.Activate;
begin
  inherited;

end;

initialization
  RegisterClass( TEDIT_CO_HR_BASE );


end.
