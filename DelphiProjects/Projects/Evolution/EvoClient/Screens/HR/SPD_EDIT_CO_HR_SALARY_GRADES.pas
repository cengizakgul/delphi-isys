// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_SALARY_GRADES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_HR_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Mask, wwdbedit, SPD_EDIT_CO_HR_DESCR_BASE, SPackageEntry, EvUtils,
  ISBasicClasses, SDDClasses, SDataDictclient, SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  ImgList, isUIwwDBEdit, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_HR_SALARY_GRADES = class(TEDIT_CO_HR_DESCR_BASE)
    evDBEdit1: TevDBEdit;
    evDBEdit2: TevDBEdit;
    evDBEdit3: TevDBEdit;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evButton1: TevBitBtn;
    procedure evButton1Click(Sender: TObject);
    procedure fpBaseDBControlsResize(Sender: TObject);
    procedure pnlFashionDetailsResize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

{$R *.DFM}

{ TEDIT_CO_HR_SALARY_GRADES }

function TEDIT_CO_HR_SALARY_GRADES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_HR_SALARY_GRADES
end;

procedure TEDIT_CO_HR_SALARY_GRADES.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  
  Screen.ActiveForm.ActiveControl := nil;
  if (Kind = NavOk) and
     (wwdsDetail.DataSet.FieldByName('MINIMUM_PAY').AsFloat > wwdsDetail.DataSet.FieldByName('MAXIMUM_PAY').AsFloat) then
  begin
    EvMessage('Minimum Pay cannot be less than Maximum Pay', mtError, [mbOK]);
    Handled := True;
  end;
end;

procedure TEDIT_CO_HR_SALARY_GRADES.evButton1Click(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.State = dsBrowse then
    wwdsDetail.DataSet.Edit;

  wwdsDetail.DataSet.FieldByName('MID_PAY').AsFloat :=
    (wwdsDetail.DataSet.FieldByName('MINIMUM_PAY').AsFloat +
     wwdsDetail.DataSet.FieldByName('MAXIMUM_PAY').AsFloat) / 2;
end;

procedure TEDIT_CO_HR_SALARY_GRADES.fpBaseDBControlsResize(
  Sender: TObject);
begin
//  inherited;  Leave this off.

end;

procedure TEDIT_CO_HR_SALARY_GRADES.pnlFashionDetailsResize(
  Sender: TObject);
begin
//  inherited;

end;

procedure TEDIT_CO_HR_SALARY_GRADES.PageControl1Change(Sender: TObject);
begin
  inherited;
  //GUI 2.0
  evDBGrid2.Columns[0].DisplayWidth := 16;
  evDBGrid2.Columns[1].DisplayWidth := 10;
  evDBGrid2.Columns[2].DisplayWidth := 10;
  evDBGrid2.Columns[3].DisplayWidth := 10;
end;

initialization
  RegisterClass( TEDIT_CO_HR_SALARY_GRADES );


end.
