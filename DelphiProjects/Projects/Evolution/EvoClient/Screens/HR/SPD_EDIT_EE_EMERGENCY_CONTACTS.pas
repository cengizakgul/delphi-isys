// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_EE_EMERGENCY_CONTACTS;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_EE_HR_SUBBROWSE_BASE, Db,  
  DBActns, ActnList, SDataStructure, Wwdatsrc, StdCtrls, wwdblook, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Mask, wwdbedit, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses,
  SDataDictclient, ImgList, SDataDicttemp, isUIwwDBLookupCombo,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_EE_EMERGENCY_CONTACTS = class(TEDIT_EE_HR_SUBBROWSE_BASE)
    fpEmergencyContact: TisUIFashionPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBEdit2: TevDBEdit;
    evDBEdit3: TevDBEdit;
    evDBEdit4: TevDBEdit;
    evDBEdit5: TevDBEdit;
    evDBEdit6: TevDBEdit;
    evDBEdit7: TevDBEdit;
    evDBEdit8: TevDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    
  end;

implementation

uses SPD_EDIT_EE_HR_BASE;

{$R *.DFM}
{ TEDIT_EE_EMERGENCY_CONTACTS }

function TEDIT_EE_EMERGENCY_CONTACTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_EMPLOYEE.EE_EMERGENCY_CONTACTS;
end;

function TEDIT_EE_EMERGENCY_CONTACTS.GetInsertControl: TWinControl;
begin
  Result := evDBEdit1;
end;

initialization
  RegisterClass( TEDIT_EE_EMERGENCY_CONTACTS );


end.
