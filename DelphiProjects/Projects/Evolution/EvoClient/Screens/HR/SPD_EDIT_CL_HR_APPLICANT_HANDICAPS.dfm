inherited EDIT_CL_HR_APPLICANT_HANDICAPS: TEDIT_CL_HR_APPLICANT_HANDICAPS
  Width = 723
  Height = 495
  inherited Panel1: TevPanel
    Width = 723
    inherited pnlFavoriteReport: TevPanel
      Left = 571
    end
    inherited pnlTopLeft: TevPanel
      Width = 571
    end
  end
  inherited PageControl1: TevPageControl
    Width = 723
    Height = 410
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 715
        Height = 381
        inherited pnlBorder: TevPanel
          Width = 711
          Height = 377
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 711
          Height = 377
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbApplicantBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                Color = clBtnFace
                ParentColor = False
                TabOrder = 0
                object evSplitter1: TevSplitter
                  Left = 356
                  Top = 6
                  Height = 549
                end
                object pnlLeftBorder: TevPanel
                  Left = 0
                  Top = 6
                  Width = 6
                  Height = 549
                  Align = alLeft
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 0
                end
                object pnlRightBorder: TevPanel
                  Left = 790
                  Top = 6
                  Width = 6
                  Height = 549
                  Align = alRight
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 1
                end
                object pnlTopBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 6
                  Align = alTop
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 2
                end
                object pnlBottomBorder: TevPanel
                  Left = 0
                  Top = 555
                  Width = 796
                  Height = 6
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 3
                end
                object pnlBrowseLeft: TevPanel
                  Left = 6
                  Top = 6
                  Width = 350
                  Height = 549
                  Align = alLeft
                  BevelOuter = bvNone
                  BorderWidth = 6
                  ParentColor = True
                  TabOrder = 4
                  object fpBrowseLeft: TisUIFashionPanel
                    Left = 6
                    Top = 6
                    Width = 338
                    Height = 537
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 12
                    Color = 14737632
                    TabOrder = 0
                    RoundRect = True
                    ShadowDepth = 8
                    ShadowSpace = 8
                    ShowShadow = True
                    ShadowColor = clSilver
                    TitleColor = clGrayText
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWhite
                    TitleFont.Height = -13
                    TitleFont.Name = 'Arial'
                    TitleFont.Style = [fsBold]
                    Title = 'Company'
                    LineWidth = 0
                    LineColor = clWhite
                    Theme = ttCustom
                    object pnlFPLeftBody: TevPanel
                      Left = 12
                      Top = 35
                      Width = 297
                      Height = 298
                      BevelOuter = bvNone
                      Caption = 'pnlFPLeftBody'
                      ParentColor = True
                      TabOrder = 0
                    end
                  end
                end
                object pnlBrowseRight: TevPanel
                  Left = 359
                  Top = 6
                  Width = 431
                  Height = 549
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  ParentColor = True
                  TabOrder = 5
                  object fpBrowseRight: TisUIFashionPanel
                    Left = 6
                    Top = 6
                    Width = 419
                    Height = 537
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 12
                    Color = 14737632
                    TabOrder = 0
                    RoundRect = True
                    ShadowDepth = 8
                    ShadowSpace = 8
                    ShowShadow = True
                    ShadowColor = clSilver
                    TitleColor = clGrayText
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWhite
                    TitleFont.Height = -13
                    TitleFont.Name = 'Arial'
                    TitleFont.Style = [fsBold]
                    Title = 'Applicant'
                    LineWidth = 0
                    LineColor = clWhite
                    Theme = ttCustom
                    object pnlFPRightBody: TevPanel
                      Left = 12
                      Top = 35
                      Width = 297
                      Height = 298
                      BevelOuter = bvNone
                      Caption = 'pnlFPLeftBody'
                      ParentColor = True
                      TabOrder = 0
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 375
              Top = 90
            end
          end
          inherited Panel3: TevPanel
            Width = 663
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 663
            Height = 270
            inherited Splitter1: TevSplitter
              Left = 320
              Height = 266
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Height = 266
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 186
                IniAttributes.SectionName = 'TEDIT_CL_HR_APPLICANT_HANDICAPS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 336
              Height = 266
              inherited evDBGrid1: TevDBGrid
                Width = 286
                Height = 186
                IniAttributes.SectionName = 'TEDIT_CL_HR_APPLICANT_HANDICAPS\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tshtDetails: TTabSheet
      inherited pnlBrowseDetails: TevPanel
        Left = 12
        Top = 35
        Width = 278
        Height = 196
      end
      inherited pnlDBControls: TevPanel
        Left = 12
        Top = 35
        Width = 278
        Height = 38
        ParentColor = True
        TabOrder = 2
      end
      inherited sbAppDescBaseDetail: TScrollBox
        Width = 715
        Height = 381
        TabOrder = 1
        inherited fpAppDescBaseSummary: TisUIFashionPanel
          Width = 311
          inherited evDBGrid2: TevDBGrid
            Width = 279
            Selected.Strings = (
              'Handicap'#9'40'#9'Disabilities'#9'F')
            IniAttributes.SectionName = 'TEDIT_CL_HR_APPLICANT_HANDICAPS\evDBGrid2'
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            PaintOptions.AlternatingRowColor = 14544093
          end
        end
        object fpHandicapDetail: TisUIFashionPanel
          Left = 8
          Top = 322
          Width = 311
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpHandicapList'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Disability Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblHandicap: TevLabel
            Left = 12
            Top = 35
            Width = 65
            Height = 13
            Caption = '~Disability'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 278
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'SY_HR_HANDICAPS_NBR'
            DataSource = wwdsSubDetail
            LookupTable = DM_SY_HR_HANDICAPS.SY_HR_HANDICAPS
            LookupField = 'SY_HR_HANDICAPS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 723
  end
  inherited wwdsSubDetail: TevDataSource
    DataSet = DM_CL_HR_PERSON_HANDICAPS.CL_HR_PERSON_HANDICAPS
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 488
    Top = 24
  end
  object DM_SYSTEM_HR: TDM_SYSTEM_HR
    Left = 476
    Top = 105
  end
end
