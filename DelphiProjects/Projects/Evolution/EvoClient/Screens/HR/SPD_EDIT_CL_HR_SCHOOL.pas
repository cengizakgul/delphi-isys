// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_SCHOOL;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, SPD_EDIT_CL_BASE, SDataStructure, Db, Wwdatsrc,
   StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, Mask, wwdbedit, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, ISBasicClasses, SDDClasses, SDataDictclient, ImgList,
  SDataDicttemp, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CL_HR_SCHOOL = class(TEDIT_CL_BASE)
    tbshDetails: TTabSheet;
    evPanel1: TevPanel;
    evDBGrid1: TevDBGrid;
    evPanel2: TevPanel;
    ScrollBox1: TScrollBox;
    evPanel3: TevPanel;
    pnlBrowseSchool: TisUIFashionPanel;
    pnlBrowseSchoolBody: TevPanel;
    pnlEditSchool: TisUIFashionPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    dedtName: TevDBEdit;
    evDBEdit2: TevDBEdit;
    evDBEdit3: TevDBEdit;
    evDBEdit4: TevDBEdit;
    evDBEdit5: TevDBEdit;
    evDBEdit6: TevDBEdit;
    sbSchools: TScrollBox;
    pnlSchoolBorder: TevPanel;
    pnlSchoolLeft: TevPanel;
    evSplitter1: TevSplitter;
    pnlSchoolRight: TevPanel;
    fpClient: TisUIFashionPanel;
    fpSchool: TisUIFashionPanel;
    pnlFPClientBody: TevPanel;
    pnlFPSchoolBody: TevPanel;
    evDBGrid2: TevDBGrid;
    procedure evDBGrid2DblClick(Sender: TObject);
    procedure pnlBrowseSchoolResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

uses SPD_EDIT_Open_BASE;

{$R *.DFM}

{ TEDIT_CL_HR_SCHOOL }

function TEDIT_CL_HR_SCHOOL.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_SCHOOL;
end;

function TEDIT_CL_HR_SCHOOL.GetInsertControl: TWinControl;
begin
  Result := dedtName;
end;

procedure TEDIT_CL_HR_SCHOOL.evDBGrid2DblClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 1;
end;

procedure TEDIT_CL_HR_SCHOOL.pnlBrowseSchoolResize(Sender: TObject);
begin
  inherited;

  //sync the child window
  pnlEditSchool.Width := pnlBrowseSchool.Width;
end;

procedure TEDIT_CL_HR_SCHOOL.Activate;
begin
  inherited;

end;

initialization
  RegisterClass(TEDIT_CL_HR_SCHOOL);

end.
