// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_PROPERTY;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_HR_BASE, SDataStructure, Db, Wwdatsrc, 
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Mask, wwdbedit, SPD_EDIT_CO_HR_DESCR_BASE, EvUIComponents, EvClientDataSet,
  SDDClasses, SDataDictclient, ImgList, ISBasicClasses, SDataDicttemp,
  isUIwwDBEdit, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_HR_PROPERTY = class(TEDIT_CO_HR_DESCR_BASE)
    procedure fpBaseDBControlsResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;

  end;

implementation

{$R *.DFM}

{ TEDIT_CO_HR_PROPERTY }

function TEDIT_CO_HR_PROPERTY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_HR_PROPERTY
end;

procedure TEDIT_CO_HR_PROPERTY.fpBaseDBControlsResize(Sender: TObject);
begin
//  inherited; Leave this off.

end;

initialization
  RegisterClass( TEDIT_CO_HR_PROPERTY );


end.
