inherited EDIT_CO_HR_PROPERTY: TEDIT_CO_HR_PROPERTY
  Width = 867
  Height = 714
  inherited Panel1: TevPanel
    Width = 867
    inherited pnlFavoriteReport: TevPanel
      Left = 715
    end
    inherited pnlTopLeft: TevPanel
      Width = 715
    end
  end
  inherited PageControl1: TevPageControl
    Width = 867
    Height = 660
    ActivePage = tbshDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 859
        Height = 631
        inherited pnlBorder: TevPanel
          Width = 855
          Height = 627
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 855
          Height = 627
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbSkillsBrowse: TScrollBox
                inherited pnlSkillsBrowseBorder: TevPanel
                  inherited pnlSkillsBrowseRight: TevPanel
                    inherited fpSkillsRight: TisUIFashionPanel
                      Title = 'Property'
                    end
                  end
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 807
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 807
            Height = 520
            inherited Splitter1: TevSplitter
              Height = 516
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 516
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 436
                IniAttributes.SectionName = 'TEDIT_CO_HR_PROPERTY\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 484
              Height = 516
              Title = 'Property'
              inherited evDBGrid1: TevDBGrid
                Width = 434
                Height = 436
                Selected.Strings = (
                  'PROPERTY_DESCRIPTION'#9'40'#9'Property Description'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_HR_PROPERTY\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tbshDetails: TTabSheet
      inherited sbHR_BASE_DETAIL: TScrollBox
        Width = 859
        Height = 631
        inherited pnlevBorder: TevPanel
          Width = 855
          Height = 627
          inherited pnlFashionDetails: TisUIFashionPanel
            Width = 317
            inherited pnlFashionDetailBody: TevPanel
              Width = 285
              Height = 226
              Align = alClient
              inherited pnlBrowse: TevPanel
                Width = 285
                Height = 226
                inherited evDBGrid2: TevDBGrid
                  Width = 285
                  Height = 226
                  Selected.Strings = (
                    'PROPERTY_DESCRIPTION'#9'41'#9'Property Description'#9'F')
                  IniAttributes.SectionName = 'TEDIT_CO_HR_PROPERTY\evDBGrid2'
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                end
              end
            end
          end
          inherited fpBaseDBControls: TisUIFashionPanel
            Width = 317
            Height = 92
            Title = 'Property Details'
            inherited pnlFPDBControlsBody: TevPanel
              Width = 287
              Height = 46
              inherited pnlDBControls: TevPanel
                Top = 0
                Width = 287
                Height = 46
                inherited evLabel1: TevLabel
                  Left = 0
                  Top = 0
                end
                inherited dedtDescription: TevDBEdit
                  Left = 0
                  Top = 15
                  Width = 284
                  DataField = 'PROPERTY_DESCRIPTION'
                  Picture.PictureMaskFromDataSet = False
                  Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                end
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_HR_PROPERTY.CO_HR_PROPERTY
  end
end
