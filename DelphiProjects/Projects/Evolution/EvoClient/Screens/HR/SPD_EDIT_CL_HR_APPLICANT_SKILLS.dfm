inherited EDIT_CL_HR_APPLICANT_SKILLS: TEDIT_CL_HR_APPLICANT_SKILLS
  inherited PageControl1: TevPageControl
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbApplicantBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                Color = clBtnFace
                ParentColor = False
                TabOrder = 0
                object evSplitter1: TevSplitter
                  Left = 356
                  Top = 6
                  Height = 549
                end
                object pnlLeftBorder: TevPanel
                  Left = 0
                  Top = 6
                  Width = 6
                  Height = 549
                  Align = alLeft
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 0
                end
                object pnlRightBorder: TevPanel
                  Left = 790
                  Top = 6
                  Width = 6
                  Height = 549
                  Align = alRight
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 1
                end
                object pnlTopBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 6
                  Align = alTop
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 2
                end
                object pnlBottomBorder: TevPanel
                  Left = 0
                  Top = 555
                  Width = 796
                  Height = 6
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentColor = True
                  TabOrder = 3
                end
                object pnlBrowseLeft: TevPanel
                  Left = 6
                  Top = 6
                  Width = 350
                  Height = 549
                  Align = alLeft
                  BevelOuter = bvNone
                  BorderWidth = 6
                  ParentColor = True
                  TabOrder = 4
                  object fpBrowseLeft: TisUIFashionPanel
                    Left = 6
                    Top = 6
                    Width = 338
                    Height = 537
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 12
                    Color = 14737632
                    TabOrder = 0
                    RoundRect = True
                    ShadowDepth = 8
                    ShadowSpace = 8
                    ShowShadow = True
                    ShadowColor = clSilver
                    TitleColor = clGrayText
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWhite
                    TitleFont.Height = -13
                    TitleFont.Name = 'Arial'
                    TitleFont.Style = [fsBold]
                    Title = 'Company'
                    LineWidth = 0
                    LineColor = clWhite
                    Theme = ttCustom
                    object pnlFPLeftBody: TevPanel
                      Left = 12
                      Top = 35
                      Width = 297
                      Height = 298
                      BevelOuter = bvNone
                      Caption = 'pnlFPLeftBody'
                      ParentColor = True
                      TabOrder = 0
                    end
                  end
                end
                object pnlBrowseRight: TevPanel
                  Left = 359
                  Top = 6
                  Width = 431
                  Height = 549
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  ParentColor = True
                  TabOrder = 5
                  object fpBrowseRight: TisUIFashionPanel
                    Left = 6
                    Top = 6
                    Width = 419
                    Height = 537
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 12
                    Color = 14737632
                    TabOrder = 0
                    RoundRect = True
                    ShadowDepth = 8
                    ShadowSpace = 8
                    ShowShadow = True
                    ShadowColor = clSilver
                    TitleColor = clGrayText
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWhite
                    TitleFont.Height = -13
                    TitleFont.Name = 'Arial'
                    TitleFont.Style = [fsBold]
                    Title = 'Applicant'
                    LineWidth = 0
                    LineColor = clWhite
                    Theme = ttCustom
                    object pnlFPRightBody: TevPanel
                      Left = 12
                      Top = 35
                      Width = 297
                      Height = 298
                      BevelOuter = bvNone
                      Caption = 'pnlFPLeftBody'
                      ParentColor = True
                      TabOrder = 0
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 366
              Top = 98
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 320
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 311
                IniAttributes.SectionName = 'TEDIT_CL_HR_APPLICANT_SKILLS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited evDBGrid1: TevDBGrid
                Width = 395
                Height = 311
                IniAttributes.SectionName = 'TEDIT_CL_HR_APPLICANT_SKILLS\evDBGrid1'
              end
            end
          end
        end
      end
    end
    inherited tshtDetails: TTabSheet
      inherited pnlBrowseDetails: TevPanel
        Left = 12
        Top = 35
        Width = 434
      end
      inherited pnlDBControls: TevPanel
        Left = 12
        Top = 307
        Width = 436
        Height = 139
        ParentColor = True
      end
      inherited sbAppDescBaseDetail: TScrollBox
        Width = 427
        Height = 151
        inherited fpAppDescBaseSummary: TisUIFashionPanel
          inherited evDBGrid2: TevDBGrid
            Selected.Strings = (
              'Skill'#9'33'#9'Skill'#9'F'
              'YEARS_EXPERIENCE'#9'32'#9'Years Experience'#9'F')
            IniAttributes.SectionName = 'TEDIT_CL_HR_APPLICANT_SKILLS\evDBGrid2'
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            PaintOptions.AlternatingRowColor = 14544093
          end
        end
        object fpSkillDetail: TisUIFashionPanel
          Left = 8
          Top = 322
          Width = 469
          Height = 186
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSkillsSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Skill Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel3: TevLabel
            Left = 233
            Top = 35
            Width = 83
            Height = 13
            Caption = 'Years Experience'
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 74
            Width = 28
            Height = 13
            Caption = 'Notes'
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 44
            Height = 13
            Caption = '~Skill'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evDBEdit1: TevDBEdit
            Left = 233
            Top = 51
            Width = 213
            Height = 21
            DataField = 'YEARS_EXPERIENCE'
            DataSource = wwdsSubDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EvDBMemo1: TEvDBMemo
            Left = 12
            Top = 89
            Width = 434
            Height = 75
            DataField = 'NOTES'
            DataSource = wwdsSubDetail
            TabOrder = 1
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 51
            Width = 213
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'CL_HR_SKILLS_NBR'
            DataSource = wwdsSubDetail
            LookupTable = DM_CL_HR_SKILLS.CL_HR_SKILLS
            LookupField = 'CL_HR_SKILLS_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited wwdsSubDetail: TevDataSource
    DataSet = DM_CL_HR_PERSON_SKILLS.CL_HR_PERSON_SKILLS
  end
end
