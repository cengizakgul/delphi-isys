inherited EDIT_CO_HR_APPLICANT_DESCR_BASE: TEDIT_CO_HR_APPLICANT_DESCR_BASE
  Width = 716
  Height = 499
  inherited Panel1: TevPanel
    Width = 716
    inherited pnlFavoriteReport: TevPanel
      Left = 564
    end
    inherited pnlTopLeft: TevPanel
      Width = 564
    end
  end
  inherited PageControl1: TevPageControl
    Width = 716
    Height = 414
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 708
        Height = 385
        inherited pnlBorder: TevPanel
          Width = 704
          Height = 381
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 704
          Height = 381
          inherited Panel3: TevPanel
            Width = 656
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 656
            Height = 274
            inherited Splitter1: TevSplitter
              Height = 270
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 270
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 190
                IniAttributes.SectionName = 'TEDIT_CO_HR_APPLICANT_DESCR_BASE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 329
              Height = 270
              inherited evDBGrid1: TevDBGrid
                Width = 279
                Height = 190
                IniAttributes.SectionName = 'TEDIT_CO_HR_APPLICANT_DESCR_BASE\evDBGrid1'
              end
            end
          end
        end
      end
    end
    object tshtDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object pnlBrowseDetails: TevPanel
        Left = 120
        Top = 40
        Width = 747
        Height = 249
        BevelOuter = bvNone
        TabOrder = 0
      end
      object pnlDBControls: TevPanel
        Left = 496
        Top = 392
        Width = 747
        Height = 548
        BevelOuter = bvNone
        TabOrder = 1
      end
      object sbAppDescBaseDetail: TScrollBox
        Left = 0
        Top = 0
        Width = 708
        Height = 385
        Align = alClient
        TabOrder = 2
        object fpAppDescBaseSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 469
          Height = 306
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpAppDescBaseSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evDBGrid2: TevDBGrid
            Left = 12
            Top = 36
            Width = 437
            Height = 250
            DisableThemesInTitle = False
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_HR_APPLICANT_DESCR_BASE\evDBGrid2'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsSubDetail
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
  end
  inherited pnlEEChoice: TevPanel
    Width = 716
  end
  object wwdsSubDetail: TevDataSource
    MasterDataSource = wwdsSubMaster2
    Left = 332
    Top = 73
  end
end
