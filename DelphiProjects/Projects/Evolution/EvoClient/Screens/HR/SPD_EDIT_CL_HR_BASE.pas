// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_BASE;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CL_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, EvUIComponents, EvUtils, EvClientDataSet,
  SDDClasses, SDataDictclient, ImgList, ISBasicClasses, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_HR_BASE = class(TEDIT_CL_BASE)
    evDBGrid1: TevDBGrid;
    wwdsSubMaster: TevDataSource;
    tbshDetails: TTabSheet;
    pnlBrowseDetails: TevPanel;
    evDBGrid2: TevDBGrid;
    pnlDBControls: TevPanel;
    DM_COMPANY: TDM_COMPANY;
  private
    { Private declarations }
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;


  end;

implementation

{$R *.DFM}

{ TEDIT_CL_HR_BASE }



{ TEDIT_CL_HR_BASE }

procedure TEDIT_CL_HR_BASE.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS( DM_CLIENT.CL_PERSON, aDS);
end;

procedure TEDIT_CL_HR_BASE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS( DM_CLIENT.CL_PERSON, SupportDataSets);
end;

end.
