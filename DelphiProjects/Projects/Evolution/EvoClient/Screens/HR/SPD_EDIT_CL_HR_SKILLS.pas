// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_HR_SKILLS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_DESCR_BASE, Db, Wwdatsrc,  StdCtrls, Mask,
  wwdbedit, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, SPD_EDIT_CL_BASE,
  SDataStructure, Buttons, ComCtrls, DBCtrls, EvUtils, 
  SDDClasses, ISBasicClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CL_HR_SKILLS = class(TEDIT_CL_BASE)
    tbshDetails: TTabSheet;
    pnlBrowseDetails: TevPanel;
    evDBGrid2: TevDBGrid;
    evPanel1: TevPanel;
    ScrollBox1: TScrollBox;
    pnlListOfSkills: TisUIFashionPanel;
    sbSkillsBrowse: TScrollBox;
    pnlSkillsBrowseBorder: TevPanel;
    pnlSkillsBrowseLeft: TevPanel;
    splitSkills: TevSplitter;
    pnlSkillsBrowseRight: TevPanel;
    fpSkillsLeft: TisUIFashionPanel;
    pnlFPSkillsLeftBody: TevPanel;
    fpSkillsRight: TisUIFashionPanel;
    pnlFPSkillsRightBody: TevPanel;
    pnlSkillInfo: TisUIFashionPanel;
    evLabel1: TevLabel;
    dedtDescription: TevDBEdit;
    evDBGrid1: TevDBGrid;
    procedure pnlListOfSkillsResize(Sender: TObject);
    procedure evDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure AfterDataSetsReopen; override;
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

uses SPD_EDIT_Open_BASE;

{$R *.DFM}

{ TEDIT_CL_HR_SKILLS }

procedure TEDIT_CL_HR_SKILLS.AfterDataSetsReopen;
begin
  inherited;
  SetReadOnly(GetIfReadOnly or BlockHr);
end;

function TEDIT_CL_HR_SKILLS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_HR_SKILLS;
end;

function TEDIT_CL_HR_SKILLS.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

procedure TEDIT_CL_HR_SKILLS.pnlListOfSkillsResize(Sender: TObject);
begin
  inherited;
  evDBGrid2.Height := pnlListOfSkills.Height - 60;
end;

procedure TEDIT_CL_HR_SKILLS.evDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 1;
end;

procedure TEDIT_CL_HR_SKILLS.Activate;
begin
  inherited;

end;

initialization
  RegisterClass( TEDIT_CL_HR_SKILLS );

end.
