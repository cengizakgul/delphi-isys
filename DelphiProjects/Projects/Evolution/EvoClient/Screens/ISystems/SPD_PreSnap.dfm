object PreSnap: TPreSnap
  Left = 538
  Top = 137
  BorderStyle = bsDialog
  Caption = 'Pre-Snap'
  ClientHeight = 417
  ClientWidth = 627
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    627
    417)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMain: TevPanel
    Left = 8
    Top = 8
    Width = 612
    Height = 369
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      612
      369)
    object lblVersion: TevLabel
      Left = 16
      Top = 16
      Width = 35
      Height = 13
      Caption = 'Version'
    end
    object lblNewVersion: TevLabel
      Left = 286
      Top = 16
      Width = 59
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'New version'
    end
    object dbgReports: TevDBCheckGrid
      Left = 16
      Top = 45
      Width = 583
      Height = 310
      DisableThemesInTitle = False
      IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
      IniAttributes.SectionName = 'TPreSnap\dbgReports'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = dsVersions
      MultiSelectOptions = [msoShiftSelect]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
      ReadOnly = True
      TabOrder = 1
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = 14544093
      PaintOptions.ActiveRecordColor = clBlack
      OnSelectionChanged = dbgReportsSelectionChanged
      NoFire = False
    end
    object cmbVersion: TevComboBox
      Left = 61
      Top = 12
      Width = 155
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = cmbVersionChange
    end
    object edNewVersion: TevEdit
      Left = 355
      Top = 12
      Width = 155
      Height = 21
      Anchors = [akTop, akRight]
      TabOrder = 2
    end
    object btnUpdate: TevBitBtn
      Left = 523
      Top = 12
      Width = 75
      Height = 22
      Action = acUpdate
      Anchors = [akTop, akRight]
      TabOrder = 3
      Color = clBlack
      Margin = 0
    end
  end
  object btnClose: TevBitBtn
    Left = 546
    Top = 387
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 1
    Color = clBlack
    Margin = 0
    Kind = bkClose
  end
  object dsVersions: TevDataSource
    Left = 113
    Top = 176
  end
  object Actions: TActionList
    Left = 336
    Top = 88
    object acUpdate: TAction
      Caption = 'Update'
      OnExecute = acUpdateExecute
      OnUpdate = acUpdateUpdate
    end
  end
end
