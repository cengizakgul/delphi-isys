inherited EDIT_SY_STATE_MARITAL_STATUS: TEDIT_SY_STATE_MARITAL_STATUS
  Width = 557
  Height = 479
  inherited PageControl1: TevPageControl
    Width = 557
    Height = 446
    ActivePage = TabSheet2
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 549
        Height = 418
        inherited Splitter1: TevSplitter
          Height = 418
        end
        inherited wwDBGrid1: TevDBGrid
          Height = 418
          IniAttributes.SectionName = 'TEDIT_SY_STATE_MARITAL_STATUS\wwDBGrid1'
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Flags'
      object Label2: TevLabel
        Left = 8
        Top = 256
        Width = 39
        Height = 16
        Caption = '~Status'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TevLabel
        Left = 8
        Top = 316
        Width = 62
        Height = 16
        Caption = '~Description'
        FocusControl = dbedtSTATUS_DESCRIPTION
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TevLabel
        Left = 312
        Top = 316
        Width = 98
        Height = 13
        Caption = 'Personal Exemptions'
        FocusControl = DBEdit3
      end
      object Label5: TevLabel
        Left = 312
        Top = 256
        Width = 58
        Height = 13
        Caption = '% of Federal'
        FocusControl = DBEdit4
      end
      object dbedtSTATUS_DESCRIPTION: TevDBEdit
        Left = 8
        Top = 332
        Width = 244
        Height = 21
        HelpContext = 3502
        DataField = 'STATUS_DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object DBEdit3: TevDBEdit
        Left = 312
        Top = 332
        Width = 64
        Height = 21
        HelpContext = 3503
        DataField = 'PERSONAL_EXEMPTIONS'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object DBEdit4: TevDBEdit
        Left = 312
        Top = 272
        Width = 64
        Height = 21
        HelpContext = 3504
        DataField = 'STATE_PERCENT_OF_FEDERAL'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object DBRadioGroup1: TevDBRadioGroup
        Left = 464
        Top = 328
        Width = 125
        Height = 61
        HelpContext = 3508
        Caption = '~Deduct Federal'
        DataField = 'DEDUCT_FEDERAL'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 6
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup2: TevDBRadioGroup
        Left = 464
        Top = 256
        Width = 125
        Height = 61
        HelpContext = 3507
        Caption = '~Deduct FICA'
        DataField = 'DEDUCT_FICA'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 5
        Values.Strings = (
          'Y'
          'N')
      end
      object wwDBGrid2: TevDBGrid
        Left = 8
        Top = 16
        Width = 585
        Height = 225
        DisableThemesInTitle = False
        Selected.Strings = (
          'STATUS_DESCRIPTION'#9'40'#9'Marital Status'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_STATE_MARITAL_STATUS\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object dbedtStatusType: TevDBEdit
        Left = 8
        Top = 272
        Width = 64
        Height = 21
        HelpContext = 3501
        DataField = 'STATUS_TYPE'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object rgActiveStatus: TevDBRadioGroup
        Left = 464
        Top = 399
        Width = 125
        Height = 61
        HelpContext = 3507
        Caption = '~Active Status'
        DataField = 'ACTIVE_STATUS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 7
        Values.Strings = (
          'Y'
          'N')
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Values'
      object Label6: TevLabel
        Left = 32
        Top = 12
        Width = 195
        Height = 13
        Caption = 'Standard Deduction Percentage of Gross'
      end
      object Label8: TevLabel
        Left = 32
        Top = 36
        Width = 178
        Height = 13
        Caption = 'Standard Deduction Minimum Amount'
      end
      object Label9: TevLabel
        Left = 32
        Top = 60
        Width = 181
        Height = 13
        Caption = 'Standard Deduction Maximum Amount'
      end
      object Label10: TevLabel
        Left = 32
        Top = 84
        Width = 154
        Height = 13
        Caption = 'Standard Deduction Flat Amount'
      end
      object Label11: TevLabel
        Left = 32
        Top = 108
        Width = 166
        Height = 13
        Caption = 'Standard Per Exemption Allowance'
      end
      object Label13: TevLabel
        Left = 32
        Top = 132
        Width = 159
        Height = 13
        Caption = 'Deduct Federal Maximum Amount'
      end
      object Label14: TevLabel
        Left = 32
        Top = 156
        Width = 124
        Height = 13
        Caption = 'Per Dependent Allowance'
      end
      object Label15: TevLabel
        Left = 32
        Top = 180
        Width = 131
        Height = 13
        Caption = 'Personal Tax Credit Amount'
      end
      object Label16: TevLabel
        Left = 32
        Top = 204
        Width = 123
        Height = 13
        Caption = 'Tax Credit Per Dependent'
      end
      object Label17: TevLabel
        Left = 32
        Top = 228
        Width = 119
        Height = 13
        Caption = 'Tax Credit Per Allowance'
      end
      object Label18: TevLabel
        Left = 32
        Top = 252
        Width = 183
        Height = 13
        Caption = 'High Income Per Exemption Allowance'
      end
      object Label19: TevLabel
        Left = 32
        Top = 276
        Width = 139
        Height = 13
        Caption = 'Defined High Income Amount'
      end
      object Label20: TevLabel
        Left = 32
        Top = 300
        Width = 120
        Height = 13
        Caption = 'Minimum Taxable Income'
      end
      object Label21: TevLabel
        Left = 32
        Top = 324
        Width = 150
        Height = 13
        Caption = 'Additional Exemption Allowance'
      end
      object Label22: TevLabel
        Left = 32
        Top = 348
        Width = 150
        Height = 13
        Caption = 'Additional Deduction Allowance'
      end
      object Label26: TevLabel
        Left = 32
        Top = 372
        Width = 147
        Height = 13
        Caption = 'Deduct FICA Maximum Amount'
      end
      object Label27: TevLabel
        Left = 32
        Top = 396
        Width = 114
        Height = 13
        Caption = 'Blind Exemption Amount'
      end
      object wwdeStandard_Deduction_Percent_Gross: TevDBEdit
        Left = 256
        Top = 12
        Width = 70
        Height = 21
        HelpContext = 3512
        DataField = 'STANDARD_DEDUCTION_PCNT_GROSS'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeStandard_Deduction_Maximum_Amount: TevDBEdit
        Left = 256
        Top = 60
        Width = 70
        Height = 21
        HelpContext = 3514
        DataField = 'STANDARD_DEDUCTION_MAX_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeStandard_Deduct_Minimum_Amount: TevDBEdit
        Left = 256
        Top = 36
        Width = 70
        Height = 21
        HelpContext = 3513
        DataField = 'STANDARD_DEDUCTION_MIN_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeStandard_Per_Exemption_Allowance: TevDBEdit
        Left = 256
        Top = 108
        Width = 70
        Height = 21
        HelpContext = 3516
        DataField = 'STANDARD_PER_EXEMPTION_ALLOW'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeDeduct_Federal_Maximum_Amount: TevDBEdit
        Left = 256
        Top = 132
        Width = 70
        Height = 21
        HelpContext = 3517
        DataField = 'DEDUCT_FEDERAL_MAXIMUM_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeStandard_Deduction_Flat_Amount: TevDBEdit
        Left = 256
        Top = 84
        Width = 70
        Height = 21
        HelpContext = 3515
        DataField = 'STANDARD_DEDUCTION_FLAT_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePer_Dependent_Allowance: TevDBEdit
        Left = 256
        Top = 156
        Width = 70
        Height = 21
        HelpContext = 3518
        DataField = 'PER_DEPENDENT_ALLOWANCE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePersonal_Tax_Credit_Amount: TevDBEdit
        Left = 256
        Top = 180
        Width = 70
        Height = 21
        HelpContext = 3519
        DataField = 'PERSONAL_TAX_CREDIT_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeTax_Credit_Per_Dependent: TevDBEdit
        Left = 256
        Top = 204
        Width = 70
        Height = 21
        HelpContext = 3520
        DataField = 'TAX_CREDIT_PER_DEPENDENT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeTax_Credit_Per_Allowance: TevDBEdit
        Left = 256
        Top = 228
        Width = 70
        Height = 21
        HelpContext = 3521
        DataField = 'TAX_CREDIT_PER_ALLOWANCE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeHigh_Income_Per_Exempt_Allowance: TevDBEdit
        Left = 256
        Top = 252
        Width = 70
        Height = 21
        HelpContext = 3522
        DataField = 'HIGH_INCOME_PER_EXEMPT_ALLOW'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 10
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeDefined_High_Income_Amount: TevDBEdit
        Left = 256
        Top = 276
        Width = 70
        Height = 21
        HelpContext = 3523
        DataField = 'DEFINED_HIGH_INCOME_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 11
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeMinimum_Taxable_Income: TevDBEdit
        Left = 256
        Top = 300
        Width = 70
        Height = 21
        HelpContext = 3524
        DataField = 'MINIMUM_TAXABLE_INCOME'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 12
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeAdditional_Exemption_Allowance: TevDBEdit
        Left = 256
        Top = 324
        Width = 70
        Height = 21
        HelpContext = 3525
        DataField = 'ADDITIONAL_EXEMPT_ALLOWANCE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 13
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeBlind_Exemption_Amount: TevDBEdit
        Left = 256
        Top = 396
        Width = 70
        Height = 21
        HelpContext = 3528
        DataField = 'BLIND_EXEMPTION_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 16
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeDeduct_FICA_Maximum_Amount: TevDBEdit
        Left = 256
        Top = 372
        Width = 70
        Height = 21
        HelpContext = 3527
        DataField = 'DEDUCT_FICA__MAXIMUM_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 15
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeAdditional_Deduction_Allowance: TevDBEdit
        Left = 256
        Top = 348
        Width = 70
        Height = 21
        HelpContext = 3526
        DataField = 'ADDITIONAL_DEDUCTION_ALLOWANCE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 14
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
  end
  inherited Panel2: TevPanel
    Width = 557
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
  end
end
