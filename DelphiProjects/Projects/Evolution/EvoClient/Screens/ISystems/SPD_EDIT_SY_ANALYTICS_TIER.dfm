inherited EDIT_SY_ANALYTICS_TIER: TEDIT_SY_ANALYTICS_TIER
  Width = 700
  Height = 570
  object Label14: TevLabel [0]
    Left = 12
    Top = 274
    Width = 29
    Height = 13
    Caption = 'Level '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object wwDBGrid1: TevDBGrid [1]
    Left = 0
    Top = 0
    Width = 700
    Height = 253
    DisableThemesInTitle = False
    Selected.Strings = (
      'TIER_ID'#9'40'#9'Tier id'#9'F'
      'DASHBOARD_DEFAULT_COUNT'#9'10'#9'Dashboard default count'#9'F'
      'DASHBOARD_MAX_COUNT'#9'10'#9'Dashboard max count'#9'F'
      'USERS_DEFAULT_COUNT'#9'10'#9'Users default count'#9'F'
      'USERS_MAX_COUNT'#9'10'#9'Users max count'#9'F'
      'LOOKBACK_YEARS_DEFAULT'#9'10'#9'Lookback years default'#9'F'
      'LOOKBACK_YEARS_MAX'#9'10'#9'Lookback years max'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_ANALYTICS_TIER\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alTop
    DataSource = wwdsDetail
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object cbAgencyType: TevDBRadioGroup [2]
    Left = 12
    Top = 324
    Width = 140
    Height = 50
    HelpContext = 52503
    Caption = 'Mobile app '
    Columns = 2
    DataField = 'MOBILE_APP'
    DataSource = wwdsDetail
    Items.Strings = (
      'Yes'
      'No')
    TabOrder = 1
    Values.Strings = (
      'Y'
      'N')
  end
  object evDBRadioGroup1: TevDBRadioGroup [3]
    Left = 12
    Top = 382
    Width = 140
    Height = 50
    HelpContext = 52503
    Caption = 'Benchmarking '
    Columns = 2
    DataField = 'INTERNAL_BENCHMARKING'
    DataSource = wwdsDetail
    Items.Strings = (
      'Yes'
      'No')
    TabOrder = 2
    Values.Strings = (
      'Y'
      'N')
  end
  object grbxABA_Information: TevGroupBox [4]
    Left = 168
    Top = 267
    Width = 412
    Height = 50
    Caption = 'Dashboards '
    TabOrder = 3
    object lablABA_Nbr: TevLabel
      Left = 16
      Top = 23
      Width = 81
      Height = 13
      AutoSize = False
      Caption = 'Default Number'
      FocusControl = wwdeABA_Nbr
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lablTop_ABA_Nbr: TevLabel
      Left = 208
      Top = 23
      Width = 84
      Height = 13
      Caption = 'Maximum Number'
      FocusControl = wwdeTop_ABA_Nbr
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object wwdeABA_Nbr: TevDBEdit
      Left = 96
      Top = 20
      Width = 97
      Height = 21
      DataField = 'DASHBOARD_DEFAULT_COUNT'
      DataSource = wwdsDetail
      TabOrder = 0
      UnboundDataType = wwDefault
      UsePictureMask = False
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
    object wwdeTop_ABA_Nbr: TevDBEdit
      Left = 304
      Top = 20
      Width = 97
      Height = 21
      DataField = 'DASHBOARD_MAX_COUNT'
      DataSource = wwdsDetail
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
  end
  object evGroupBox1: TevGroupBox [5]
    Left = 168
    Top = 324
    Width = 412
    Height = 50
    Caption = 'Users '
    TabOrder = 4
    object evLabel1: TevLabel
      Left = 16
      Top = 23
      Width = 81
      Height = 13
      AutoSize = False
      Caption = 'Default Number'
      FocusControl = evDBEdit1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel2: TevLabel
      Left = 208
      Top = 23
      Width = 84
      Height = 13
      Caption = 'Maximum Number'
      FocusControl = evDBEdit2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evDBEdit1: TevDBEdit
      Left = 96
      Top = 20
      Width = 97
      Height = 21
      DataField = 'USERS_DEFAULT_COUNT'
      DataSource = wwdsDetail
      TabOrder = 0
      UnboundDataType = wwDefault
      UsePictureMask = False
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
    object evDBEdit2: TevDBEdit
      Left = 304
      Top = 20
      Width = 97
      Height = 21
      DataField = 'USERS_MAX_COUNT'
      DataSource = wwdsDetail
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
  end
  object evGroupBox2: TevGroupBox [6]
    Left = 168
    Top = 382
    Width = 412
    Height = 50
    Caption = 'Lookback Years'
    TabOrder = 5
    object evLabel3: TevLabel
      Left = 16
      Top = 23
      Width = 81
      Height = 13
      AutoSize = False
      Caption = 'Default Number'
      FocusControl = evDBEdit3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel4: TevLabel
      Left = 208
      Top = 23
      Width = 84
      Height = 13
      Caption = 'Maximum Number'
      FocusControl = evDBEdit4
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evDBEdit3: TevDBEdit
      Left = 96
      Top = 20
      Width = 97
      Height = 21
      DataField = 'LOOKBACK_YEARS_DEFAULT'
      DataSource = wwdsDetail
      TabOrder = 0
      UnboundDataType = wwDefault
      UsePictureMask = False
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
    object evDBEdit4: TevDBEdit
      Left = 304
      Top = 20
      Width = 97
      Height = 21
      DataField = 'LOOKBACK_YEARS_MAX'
      DataSource = wwdsDetail
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
  end
  object edtLevel: TevDBEdit [7]
    Left = 14
    Top = 292
    Width = 140
    Height = 21
    DataField = 'TIER_ID'
    DataSource = wwdsDetail
    TabOrder = 6
    UnboundDataType = wwDefault
    UsePictureMask = False
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_ANALYTICS_TIER.SY_ANALYTICS_TIER
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 304
    Top = 32
  end
end
