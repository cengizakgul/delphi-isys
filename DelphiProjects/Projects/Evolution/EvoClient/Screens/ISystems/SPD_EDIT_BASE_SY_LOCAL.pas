// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_BASE_SY_LOCAL;

interface

uses
  SFrameEntry,  Grids, Wwdbigrd,
  Wwdbgrid, ExtCtrls, Controls, DBCtrls, StdCtrls, Db, Wwdatsrc, ComCtrls,
  Classes, SDataStructure, EvUtils, SDDClasses, 
  ISBasicClasses, SDataDictsystem, EvUIComponents, EvClientDataSet;

type
  TEDIT_BASE_SY_LOCAL = class(TFrameEntry)
    Panel1: TevPanel;
    PageControl1: TevPageControl;
    Splitter1: TevSplitter;
    wwDBGrid1: TevDBGrid;
    Panel2: TevPanel;
    splCounty: TevSplitter;
    Panel2a: TevPanel;
    wwDBGridLocal: TevDBGrid;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    wwdsSubMaster: TevDataSource;
    wwdsSubMaster2: TevDataSource;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    evPanel1: TevPanel;
    Label1: TevLabel;
    DBText2: TevDBText;
    lblCounty: TevLabel;
    dbtxtCounty: TevDBText;
    Label2: TevLabel;
    DBText3: TevDBText;
  private
    { Private declarations }
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    { Public declarations }
  end;

implementation

{$R *.DFM}

{ TEDIT_BASE_SY_LOCAL }

procedure TEDIT_BASE_SY_LOCAL.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATES, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_LOCAL.SY_COUNTY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_LOCAL.SY_LOCALS, SupportDataSets, '');
end;

end.
