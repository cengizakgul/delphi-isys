// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_STATE_TAX_CHART;

interface

uses
  SDataStructure,  SPD_EDIT_SY_STATE_BASE, wwdbdatetimepicker,
  wwdbedit, Mask, Wwdotdot, Wwdbcomb, StdCtrls, wwdblook, Db, Wwdatsrc,
  DBCtrls, Grids, Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, Classes,
  ISBasicClasses, SDDClasses, SDataDictsystem, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBEdit, isUIwwDBComboBox, isUIwwDBLookupCombo;

type
  TEDIT_SY_STATE_TAX_CHART = class(TEDIT_SY_STATE_BASE)
    Label2: TevLabel;
    Label3: TevLabel;
    Label4: TevLabel;
    Label5: TevLabel;
    wwDBGrid2: TevDBGrid;
    dblkpSTATE_MARITAL_STATUS: TevDBLookupCombo;
    wwDBComboBox1: TevDBComboBox;
    Label12: TevLabel;
    wwdeMinimum: TevDBEdit;
    wwdeMaximum: TevDBEdit;
    wwdePercent: TevDBEdit;
    evMaritalStatus: TevDataSource;
    StateClone: TevDataSource;
    evcbEntryType: TevDBComboBox;
  private
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_SY_STATE_TAX_CHART.Activate;
begin
  inherited;
  StateClone.DoDataChange(Self, Nil);
end;

function TEDIT_SY_STATE_TAX_CHART.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_STATE.SY_STATE_TAX_CHART;
end;

function TEDIT_SY_STATE_TAX_CHART.GetInsertControl: TWinControl;
begin
  Result := dblkpSTATE_MARITAL_STATUS;
end;

procedure TEDIT_SY_STATE_TAX_CHART.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_STATE_TAX_CHART);

end.
