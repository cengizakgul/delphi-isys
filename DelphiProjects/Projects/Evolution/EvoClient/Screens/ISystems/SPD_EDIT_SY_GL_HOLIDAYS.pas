// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_GL_HOLIDAYS;

interface

uses
   SFrameEntry, wwdbedit, Wwdotdot, Wwdbcomb,
  wwdbdatetimepicker, StdCtrls, Mask, DBCtrls, Grids, Wwdbigrd, Wwdbgrid,
  Controls, ExtCtrls, ComCtrls, Classes, Db, Wwdatsrc, SDataStructure,
  SDDClasses, SDataDictsystem, ISBasicClasses, Buttons, Variants, SPD_ChoiceFromListQueryEx,
  evUtils, EvContext, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_GL_HOLIDAYS = class(TFrameEntry)
    PageControl1: TevPageControl;
    tsBrowse: TTabSheet;
    Panel1: TevPanel;
    wwDBGrid1: TevDBGrid;
    tsDetail: TTabSheet;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    edName: TevDBEdit;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    edDayNumber: TevDBEdit;
    evLabel5: TevLabel;
    evCalcType: TevDBComboBox;
    edMonthNumber: TevDBComboBox;
    edWeekNumber: TevDBComboBox;
    edDayofWeek: TevDBComboBox;
    edSuturdayOffset: TevDBComboBox;
    edSundayOffset: TevDBComboBox;
    Panel2: TevPanel;
    wwDBGrid2: TevDBGrid;
    btnAgencyCopy: TevBitBtn;
    cbupdate: TevCheckBox;
    procedure btnAgencyCopyClick(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

uses ISKbmMemDataSet;

{$R *.DFM}

function TEDIT_SY_GL_HOLIDAYS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS;
end;

function TEDIT_SY_GL_HOLIDAYS.GetInsertControl: TWinControl;
begin
  Result := edName;
end;

procedure TEDIT_SY_GL_HOLIDAYS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
end;

procedure TEDIT_SY_GL_HOLIDAYS.btnAgencyCopyClick(Sender: TObject);
var
  Frm: TChoiceFromListQueryEx;
  DS: TevClientDataSet;
  cdHoliday: TevClientDataSet;
  i: integer;
  tmpHOLIDAY_NAME, tmpCALC_TYPE,
  tmpWEEK_NUMBER, tmpDAY_OF_WEEK, tmpMONTH_NUMBER, tmpDAY_NUMBER,
  tmpSATURDAY_OFFSET, tmpSUNDAY_OFFSET : variant;

    procedure DoCopyHolidayTo;
    begin
       if cbupdate.Checked and
         cdHoliday.Locate('SY_GLOBAL_AGENCY_NBR;HOLIDAY_NAME', VarArrayOf([DS['SY_GLOBAL_AGENCY_NBR'],tmpHOLIDAY_NAME]), []) then
          cdHoliday.Edit
       else
       begin
          cdHoliday.Insert;
          cdHoliday['SY_GLOBAL_AGENCY_NBR'] := DS['SY_GLOBAL_AGENCY_NBR'];
       end;

       if cdHoliday.State in [dsEdit, dsInsert ] then
       begin
        cdHoliday['HOLIDAY_NAME'] := tmpHOLIDAY_NAME;
        cdHoliday['CALC_TYPE'] := tmpCALC_TYPE;
        cdHoliday['WEEK_NUMBER'] := tmpWEEK_NUMBER;
        cdHoliday['DAY_OF_WEEK'] := tmpDAY_OF_WEEK;
        cdHoliday['MONTH_NUMBER'] := tmpMONTH_NUMBER;
        cdHoliday['DAY_NUMBER'] := tmpDAY_NUMBER;
        cdHoliday['SATURDAY_OFFSET'] := tmpSATURDAY_OFFSET;
        cdHoliday['SUNDAY_OFFSET'] := tmpSUNDAY_OFFSET;

        cdHoliday.Post;
       end;
    end;
begin
  cdHoliday := TevClientDataSet.Create(nil);
  DS := TevClientDataSet.Create(Self);
  Frm := TChoiceFromListQueryEx.Create(self);
  try
    tmpHOLIDAY_NAME := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS['HOLIDAY_NAME'];
    tmpCALC_TYPE := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS['CALC_TYPE'];
    tmpWEEK_NUMBER := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS['WEEK_NUMBER'];
    tmpDAY_OF_WEEK := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS['DAY_OF_WEEK'];
    tmpMONTH_NUMBER := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS['MONTH_NUMBER'];
    tmpDAY_NUMBER := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS['DAY_NUMBER'];
    tmpSATURDAY_OFFSET := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS['SATURDAY_OFFSET'];
    tmpSUNDAY_OFFSET := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS['SUNDAY_OFFSET'];

    cdHoliday.ProviderName := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.ProviderName;
    cdHoliday.DataRequired('ALL');

    Frm.Caption := 'Global Agency List';
    DS.CloneCursor(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, True);
    Frm.dgChoiceList.Selected.Clear;
    Frm.dgChoiceList.Selected.Add('AGENCY_NAME'#9'40'#9'Agency name'#9'F');
    Frm.dgChoiceList.Selected.Add('STATE'#9'2'#9'State'#9'F');
    Frm.dgChoiceList.Selected.Add('COUNTY'#9'40'#9'County'#9'F');
    Frm.dgChoiceList.ApplySelected;
    i := DS.IndexDefs.IndexOf('SORT');
    if i <> -1 then
      DS.IndexDefs.Delete(i);
    Frm.dgChoiceList.DefaultSort := 'AGENCY_NAME';
    Frm.dgChoiceList.Sorting := True;
    Frm.dsChoiceList.DataSet := DS;
    Frm.Width := 550;
    if Frm.ShowModal = mrYes then
    begin
      DS.DisableControls;
      DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.DisableControls;
      try
        for i := 0 to Frm.dgChoiceList.SelectedList.Count - 1 do
        begin
          DS.GotoBookmark(Frm.dgChoiceList.SelectedList[i]);
          if DS.FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger > 0 then
             DoCopyHolidayTo;
        end;
      finally
        ctx_DataAccess.PostDataSets([cdHoliday]);
        DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.close;
        DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.DataRequired('ALL');
        wwdsMaster.DataSet.Refresh;
        wwdsDetail.DataSet.Refresh;
        DS.EnableControls;
        DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAYS.EnableControls;
      end;
    end;
  finally
    cdHoliday.Free;
    Frm.Free;
    DS.Free;
  end;
end;

procedure TEDIT_SY_GL_HOLIDAYS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then exit;
  btnAgencyCopy.Enabled :=  wwdsDetail.Active and (wwdsDetail.State  = dsBrowse) and
                            (not wwdsDetail.DataSet.FieldByName('HOLIDAY_NAME').IsNull);
end;

initialization
  RegisterClass(TEDIT_SY_GL_HOLIDAYS);

end.
