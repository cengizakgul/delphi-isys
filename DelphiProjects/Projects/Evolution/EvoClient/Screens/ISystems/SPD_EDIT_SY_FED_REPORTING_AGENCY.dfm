inherited EDIT_SY_FED_REPORTING_AGENCY: TEDIT_SY_FED_REPORTING_AGENCY
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 277
    ActivePage = Browse
    Align = alClient
    TabOrder = 0
    object Browse: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 435
        Height = 249
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 320
          Top = 0
          Width = 3
          Height = 249
          Cursor = crHSplit
        end
        object wwDBGrid1: TevDBGrid
          Left = 0
          Top = 0
          Width = 320
          Height = 249
          Selected.Strings = (
            'AGENCY_NAME'#9'40'#9'AGENCY_NAME'#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsDetail
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          TitleButtons = True
          UseTFields = False
          IndicatorColor = icBlack
          LocateDlg = True
          FilterDlg = True
          Sorting = True
        end
      end
    end
    object Address: TTabSheet
      Caption = 'Details'
      object lablAgency_Name: TevLabel
        Left = 16
        Top = 24
        Width = 79
        Height = 13
        Caption = '~Agency Name'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwlcAgency_Name: TevDBLookupCombo
        Left = 16
        Top = 40
        Width = 297
        Height = 21
        HelpContext = 6502
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'AGENCY_NAME'#9'F')
        DataField = 'SY_GLOBAL_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
        LookupField = 'SY_GLOBAL_AGENCY_NBR'
        Style = csDropDownList
        ReadOnly = False
        TabOrder = 0
        AutoDropDown = False
        ShowButton = True
        AllowClearKey = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_FED_REPORTING_AGENCY.SY_FED_REPORTING_AGENCY
  end
  object DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL
    Left = 279
    Top = 21
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 354
    Top = 21
  end
end
