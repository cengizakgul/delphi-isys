// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_LOCAL_DEPOSIT_FREQ;

interface

uses
   SDataStructure, SPD_EDIT_BASE_SY_LOCAL, wwdbedit, Wwdotdot,
  Wwdbcomb, StdCtrls, ExtCtrls, DBCtrls, Mask, Db, Wwdatsrc, Grids,
  Wwdbigrd, Wwdbgrid, Controls, ComCtrls, Classes, wwdblook,
  ISBasicClasses, SDDClasses, kbmMemTable, isVCLBugFix,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, SysUtils,
  SDataDictsystem, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit;

type
  TEDIT_SY_LOCAL_DEPOSIT_FREQ = class(TEDIT_BASE_SY_LOCAL)
    lablType: TevLabel;
    lablWhen_Due_Type: TevLabel;
    lablNbr_Days_For_When_Due: TevLabel;
    dedtNbr_Days_For_When_Due: TevDBEdit;
    lablDescription: TevLabel;
    dedtDescription: TevDBEdit;
    drgpThrd_Mnth_Due_End_Next_Mnth: TevDBRadioGroup;
    wwDBGrid3: TevDBGrid;
    wwcbType: TevDBComboBox;
    wwcbWhen_Due_Type: TevDBComboBox;
    dblcTaxCoupon: TevDBLookupCombo;
    evLabel2: TevLabel;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    Label5: TevLabel;
    edTypeCode: TevDBEdit;
    rgIncrementTypeCode: TevDBRadioGroup;
    rgStatusChange: TevDBRadioGroup;
    Label10: TevLabel;
    cbFirstTPeriod: TevDBComboBox;
    Label11: TevLabel;
    edFirstTAmount: TevDBEdit;
    evLookupDS: TevClientDataSet;
    evLookupDSrc: TevDataSource;
    Label9: TevLabel;
    luFirstTFreq: TevDBLookupCombo;
    luFieldOffice: TevDBLookupCombo;
    evLabel8: TevLabel;
    cbSystemReportsGroup: TevDBLookupCombo;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

uses EvUtils, EvConsts;

{$R *.DFM}

procedure TEDIT_SY_LOCAL_DEPOSIT_FREQ.Activate;
begin
  inherited;
  evLookupDS.RetrieveCondition := '';
  evLookupDS.CloneCursor(DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ, True);
  evLookupDSrc.DataSet := evLookupDS;
  with ctx_DataAccess.SY_CUSTOM_VIEW do
  begin
    Close;
    with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 't1.sy_reports_nbr, t1.description');
      SetMacro('TABLE1', 'sy_reports');
      SetMacro('TABLE2', 'SY_REPORT_WRITER_REPORTS');
      SetMacro('JOINFIELD', 'SY_REPORT_WRITER_REPORTS');
      SetMacro('CONDITION', 't2.REPORT_TYPE = :T');
      SetParam('T', SYSTEM_REPORT_TYPE_TAXCOUPON);
      DataRequest(AsVariant);
    end;
    Open;
  end;
  dblcTaxCoupon.LookupTable := CreateLookupDataset(ctx_DataAccess.SY_CUSTOM_VIEW, Self);
end;

procedure TEDIT_SY_LOCAL_DEPOSIT_FREQ.Deactivate;
begin
  inherited;
  evLookupDS.Close;
end;

function TEDIT_SY_LOCAL_DEPOSIT_FREQ.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ;
end;

function TEDIT_SY_LOCAL_DEPOSIT_FREQ.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

procedure TEDIT_SY_LOCAL_DEPOSIT_FREQ.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS_GROUP, SupportDataSets, 'SY_REPORTS_GROUP_NBR > 20000');
end;

initialization
  RegisterClass(TEDIT_SY_LOCAL_DEPOSIT_FREQ);

end.
