// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_STATE_MARITAL_STATUS;

interface

uses
  SDataStructure,  SPD_EDIT_SY_STATE_BASE, wwdbdatetimepicker,
  StdCtrls, ExtCtrls, DBCtrls, Mask, Db, Wwdatsrc, Grids, Wwdbigrd,
  Wwdbgrid, Controls, ComCtrls, Classes, wwdbedit, ISBasicClasses,
  SDDClasses, SDataDictsystem, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit;

type
  TEDIT_SY_STATE_MARITAL_STATUS = class(TEDIT_SY_STATE_BASE)
    Label2: TevLabel;
    Label3: TevLabel;
    dbedtSTATUS_DESCRIPTION: TevDBEdit;
    Label4: TevLabel;
    DBEdit3: TevDBEdit;
    Label5: TevLabel;
    DBEdit4: TevDBEdit;
    TabSheet3: TTabSheet;
    DBRadioGroup1: TevDBRadioGroup;
    DBRadioGroup2: TevDBRadioGroup;
    Label6: TevLabel;
    Label8: TevLabel;
    Label9: TevLabel;
    Label10: TevLabel;
    Label11: TevLabel;
    Label13: TevLabel;
    Label14: TevLabel;
    Label15: TevLabel;
    Label16: TevLabel;
    Label17: TevLabel;
    Label18: TevLabel;
    Label19: TevLabel;
    Label20: TevLabel;
    Label21: TevLabel;
    Label22: TevLabel;
    Label26: TevLabel;
    Label27: TevLabel;
    wwDBGrid2: TevDBGrid;
    dbedtStatusType: TevDBEdit;
    wwdeStandard_Deduction_Percent_Gross: TevDBEdit;
    wwdeStandard_Deduction_Maximum_Amount: TevDBEdit;
    wwdeStandard_Deduct_Minimum_Amount: TevDBEdit;
    wwdeStandard_Per_Exemption_Allowance: TevDBEdit;
    wwdeDeduct_Federal_Maximum_Amount: TevDBEdit;
    wwdeStandard_Deduction_Flat_Amount: TevDBEdit;
    wwdePer_Dependent_Allowance: TevDBEdit;
    wwdePersonal_Tax_Credit_Amount: TevDBEdit;
    wwdeTax_Credit_Per_Dependent: TevDBEdit;
    wwdeTax_Credit_Per_Allowance: TevDBEdit;
    wwdeHigh_Income_Per_Exempt_Allowance: TevDBEdit;
    wwdeDefined_High_Income_Amount: TevDBEdit;
    wwdeMinimum_Taxable_Income: TevDBEdit;
    wwdeAdditional_Exemption_Allowance: TevDBEdit;
    wwdeBlind_Exemption_Amount: TevDBEdit;
    wwdeDeduct_FICA_Maximum_Amount: TevDBEdit;
    wwdeAdditional_Deduction_Allowance: TevDBEdit;
    rgActiveStatus: TevDBRadioGroup;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_STATE_MARITAL_STATUS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS;
end;

function TEDIT_SY_STATE_MARITAL_STATUS.GetInsertControl: TWinControl;
begin
  Result := dbedtStatusType;
end;

initialization
  RegisterClass(TEDIT_SY_STATE_MARITAL_STATUS);

end.
