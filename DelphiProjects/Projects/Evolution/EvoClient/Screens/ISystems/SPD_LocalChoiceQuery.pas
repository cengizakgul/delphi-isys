// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_LocalChoiceQuery;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, SPD_SafeChoiceFromListQuery,
  ActnList, Db,   Wwdatsrc, Grids,
  Wwdbigrd, Wwdbgrid, StdCtrls, Buttons, ExtCtrls, SDataStructure,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  SDataDictsystem, ISDataAccessComponents, EvDataAccessComponents;


type
  TLocalChoiceQuery = class(TSafeChoiceFromListQuery)
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetFilter(flt: string);
  end;


implementation

{$R *.DFM}
procedure TLocalChoiceQuery.SetFilter(flt: string);
//var
 // i: integer;
begin
//  cdsChoiceList.Close;
//  ODS('entering set filter');
  dgChoiceList.DataSource := nil;

{
//  cdsChoiceList.FieldDefs := DM_SYSTEM_LOCAL.SY_LOCALS.FieldDefs;
  while not DM_SYSTEM_LOCAL.SY_LOCALS.Eof do
  begin
    cdsChoiceList.Insert;
    for i := 0 to DM_SYSTEM_LOCAL.SY_LOCALS.Fields.Count - 1 do
      cdsChoiceList.Fields[i].Value := DM_SYSTEM_LOCAL.SY_LOCALS[cdsChoiceList.Fields[i].FieldName];
    cdsChoiceList.Post;
    DM_SYSTEM_LOCAL.SY_LOCALS.Next;
  end;
 }

//  cdsChoiceList.Data := DM_SYSTEM_LOCAL.SY_LOCALS.Data;
  cdsChoiceList.CloneCursor( DM_SYSTEM_LOCAL.SY_LOCALS, true );

  if trim( flt ) <> '' then
  begin
    cdsChoiceList.Filter := flt;
    cdsChoiceList.Filtered := true;
  end;
//  dsChoiceList.DataSet := DM_SYSTEM_LOCAL.SY_LOCALS;

  dgChoiceList.DataSource := dsChoiceList;
end;


procedure TLocalChoiceQuery.FormDestroy(Sender: TObject);
begin
  cdsChoiceList.Close;
  inherited;
end;

end.
