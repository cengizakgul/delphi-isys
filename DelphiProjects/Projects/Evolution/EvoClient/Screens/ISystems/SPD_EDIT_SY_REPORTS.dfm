inherited EDIT_SY_REPORTS: TEDIT_SY_REPORTS
  Width = 702
  Height = 457
  object wwDBGrid1: TevDBGrid [0]
    Left = 0
    Top = 0
    Width = 702
    Height = 298
    DisableThemesInTitle = False
    Selected.Strings = (
      'NDESCRIPTION'#9'40'#9'Report Description'
      'ACTIVE_REPORT'#9'1'#9'Priority'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_REPORTS\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsMaster
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 298
    Width = 702
    Height = 159
    Align = alBottom
    BevelOuter = bvNone
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    TabOrder = 1
    object lablReport_Description: TevLabel
      Left = 12
      Top = 14
      Width = 107
      Height = 13
      Caption = '~Report Description'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TevLabel
      Left = 12
      Top = 58
      Width = 40
      Height = 13
      Caption = '~Priority'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel3: TevLabel
      Left = 12
      Top = 102
      Width = 189
      Height = 13
      Caption = '~Virtual Report Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dedtReport_Description: TevDBEdit
      Left = 12
      Top = 30
      Width = 241
      Height = 21
      HelpContext = 54001
      DataField = 'DESCRIPTION'
      DataSource = wwdsMaster
      Picture.PictureMaskFromDataSet = False
      Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
    object cbPickReport: TevGroupBox
      Left = 299
      Top = 8
      Width = 305
      Height = 55
      Caption = 'Pick report'
      TabOrder = 1
      object cbReportRw: TevDBLookupCombo
        Left = 13
        Top = 20
        Width = 281
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'REPORT_DESCRIPTION'#9'40'#9'Report Description'
          'SY_REPORT_WRITER_REPORTS_NBR'#9'10'#9'SY_REPORT_WRITER_REPORTS_NBR'#9'F')
        DataField = 'SY_REPORT_WRITER_REPORTS_NBR'
        DataSource = wwdsMaster
        LookupTable = DM_SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS
        LookupField = 'SY_REPORT_WRITER_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 0
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
    end
    object cbPriority: TevDBComboBox
      Left = 12
      Top = 74
      Width = 49
      Height = 21
      HelpContext = 54001
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'ACTIVE_REPORT'
      DataSource = wwdsMaster
      DropDownCount = 8
      ItemHeight = 0
      Items.Strings = (
        '1'#9'1'
        '2'#9'2'
        '3'#9'3'
        '4'#9'4'
        '5'#9'5'
        '6'#9'6'
        '7'#9'7'
        '8'#9'8'
        '9'#9'9')
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 2
      UnboundDataType = wwDefault
    end
    object evGroupBox1: TevGroupBox
      Left = 299
      Top = 64
      Width = 310
      Height = 57
      Caption = 'Report Active Years'
      TabOrder = 3
      object evLabel1: TevLabel
        Left = 8
        Top = 24
        Width = 23
        Height = 13
        Caption = 'From'
      end
      object evLabel2: TevLabel
        Left = 176
        Top = 24
        Width = 13
        Height = 13
        Caption = 'To'
      end
      object evDBDateTimePicker1: TevDBDateTimePicker
        Left = 48
        Top = 20
        Width = 115
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'ACTIVE_YEAR_FROM'
        DataSource = wwdsMaster
        Epoch = 1950
        ShowButton = True
        TabOrder = 0
      end
      object evDBDateTimePicker2: TevDBDateTimePicker
        Left = 200
        Top = 16
        Width = 105
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'ACTIVE_YEAR_TO'
        DataSource = wwdsMaster
        Epoch = 1950
        ShowButton = True
        TabOrder = 1
      end
    end
    object evDBLookupCombo1: TevDBLookupCombo
      Left = 13
      Top = 124
      Width = 281
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'NAME'#9'60'#9'Name'#9#9)
      DataField = 'SY_REPORTS_GROUP_NBR'
      DataSource = wwdsMaster
      LookupTable = DM_SY_REPORTS_GROUP.SY_REPORTS_GROUP
      LookupField = 'SY_REPORTS_GROUP_NBR'
      Style = csDropDownList
      TabOrder = 4
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = True
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_REPORTS.SY_REPORTS
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 332
    Top = 29
  end
end
