inherited EDIT_SY_GLOBAL_AGENCY: TEDIT_SY_GLOBAL_AGENCY
  Width = 761
  Height = 480
  object pcMain: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 761
    Height = 480
    ActivePage = tsBrowse
    Align = alClient
    TabOrder = 0
    object tsBrowse: TTabSheet
      Caption = 'Browse'
      object Splitter1: TevSplitter
        Left = 633
        Top = 0
        Height = 452
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 633
        Height = 452
        DisableThemesInTitle = False
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'Agency'
          'COUNTY'#9'40'#9'Locality'
          'STATE'#9'2'#9'State')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_GLOBAL_AGENCY\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
    end
    object tsDetail: TTabSheet
      HelpContext = 52502
      Caption = 'Details'
      object Label1: TevLabel
        Left = 8
        Top = 8
        Width = 36
        Height = 13
        Caption = 'Agency'
        FocusControl = dedtAgency_Name
      end
      object Label2: TevLabel
        Left = 280
        Top = 8
        Width = 161
        Height = 13
        Caption = 'Payment File Destination Directory'
        FocusControl = DBEdit2
      end
      object Label3: TevLabel
        Left = 280
        Top = 52
        Width = 82
        Height = 13
        Caption = 'Receiving ABA #'
        FocusControl = DBEdit3
      end
      object Label4: TevLabel
        Left = 376
        Top = 52
        Width = 101
        Height = 13
        Caption = 'Receiving Account #'
        FocusControl = DBEdit4
      end
      object Label20: TevLabel
        Left = 280
        Top = 96
        Width = 118
        Height = 13
        Caption = 'Receiving Account Type'
        FocusControl = DBEdit4
      end
      object Label5: TevLabel
        Left = 280
        Top = 253
        Width = 25
        Height = 13
        Caption = 'State'
        FocusControl = DBEdit1
      end
      object Label6: TevLabel
        Left = 320
        Top = 253
        Width = 33
        Height = 13
        Caption = 'County'
      end
      object Label7: TevLabel
        Left = 280
        Top = 296
        Width = 118
        Height = 13
        Caption = 'Tax Payment Field Office'
      end
      object evLabel1: TevLabel
        Left = 280
        Top = 336
        Width = 134
        Height = 13
        Caption = 'State ACH Key Calc Method'
      end
      object evLabel2: TevLabel
        Left = 8
        Top = 401
        Width = 28
        Height = 13
        Caption = 'Notes'
      end
      object evLabel5: TevLabel
        Left = 280
        Top = 376
        Width = 135
        Height = 13
        Caption = 'Local ACH Key Calc Method'
      end
      object dedtAgency_Name: TevDBEdit
        Left = 8
        Top = 24
        Width = 244
        Height = 21
        DataField = 'AGENCY_NAME'
        DataSource = wwdsDetail
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBRadioGroup1: TevDBRadioGroup
        Left = 8
        Top = 296
        Width = 206
        Height = 57
        HelpContext = 52507
        Caption = 'Create Zero Magmedia'
        DataField = 'MAG_MEDIA'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 5
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup5: TevDBRadioGroup
        Left = 8
        Top = 236
        Width = 205
        Height = 53
        HelpContext = 52506
        Caption = 'Negative Direct Deposit Allowed'
        DataField = 'NEGATIVE_DIRECT_DEP_ALLOWED'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 4
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup4: TevDBRadioGroup
        Left = 8
        Top = 168
        Width = 205
        Height = 57
        HelpContext = 52505
        Caption = 'Accepts Credit'
        DataField = 'ACCEPTS_CREDIT'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 3
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup6: TevDBRadioGroup
        Left = 8
        Top = 108
        Width = 205
        Height = 53
        HelpContext = 52504
        Caption = 'Accepts Debit'
        DataField = 'ACCEPTS_DEBIT'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup7: TevDBRadioGroup
        Left = 8
        Top = 52
        Width = 205
        Height = 53
        HelpContext = 52503
        Caption = 'Accepts Check'
        DataField = 'ACCEPTS_CHECK'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 1
        Values.Strings = (
          'Y'
          'N')
      end
      object DBEdit2: TevDBEdit
        Left = 280
        Top = 24
        Width = 321
        Height = 21
        HelpContext = 52508
        DataField = 'PAYMENT_FILE_DEST_DIRECT'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit3: TevDBEdit
        Left = 280
        Top = 68
        Width = 81
        Height = 21
        HelpContext = 52509
        DataField = 'RECEIVING_ABA_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 7
        UnboundDataType = wwDefault
        UsePictureMask = False
        WantReturns = False
        WordWrap = False
      end
      object DBEdit4: TevDBEdit
        Left = 376
        Top = 68
        Width = 120
        Height = 21
        HelpContext = 52510
        DataField = 'RECEIVING_ACCOUNT_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBComboBox1: TevDBComboBox
        Left = 280
        Top = 112
        Width = 217
        Height = 21
        HelpContext = 52511
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'RECEIVING_ACCOUNT_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 9
        UnboundDataType = wwDefault
      end
      object DBEdit1: TevDBEdit
        Left = 280
        Top = 269
        Width = 25
        Height = 21
        HelpContext = 52514
        DataField = 'STATE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&,@}'
        TabOrder = 10
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeCounty: TevDBEdit
        Left = 320
        Top = 269
        Width = 199
        Height = 21
        HelpContext = 52515
        DataField = 'COUNTY'
        DataSource = wwdsDetail
        TabOrder = 11
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBLookupCombo1: TevDBLookupCombo
        Left = 280
        Top = 312
        Width = 241
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'ADDITIONAL_NAME'#9'40'#9'Field Office Name'#9'F'
          'STATE'#9'2'#9'STATE'#9'F')
        DataField = 'TAX_PAYMENT_FIELD_OFFICE_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GL_AGENCY_FIELD_OFFICE.SY_GL_AGENCY_FIELD_OFFICE
        LookupField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        Style = csDropDownList
        TabOrder = 12
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object EftEndDateMethod: TevDBComboBox
        Left = 280
        Top = 352
        Width = 241
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = True
        AutoDropDown = True
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 13
        UnboundDataType = wwDefault
        OnChange = EftEndDateMethodChange
      end
      object EvDBMemo1: TEvDBMemo
        Left = 8
        Top = 419
        Width = 513
        Height = 89
        DataField = 'NOTES'
        DataSource = wwdsDetail
        TabOrder = 14
      end
      object LocalEftEndDateMethod: TevDBComboBox
        Left = 280
        Top = 392
        Width = 241
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = True
        AutoDropDown = True
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 15
        UnboundDataType = wwDefault
        OnChange = LocalEftEndDateMethodChange
      end
      object rgWeekendRule: TRadioGroup
        Left = 280
        Top = 212
        Width = 216
        Height = 36
        Caption = 'Weekend Rule'
        Columns = 2
        Items.Strings = (
          'Before'
          'After')
        TabOrder = 16
        OnClick = rgWeekendRuleClick
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 280
        Top = 136
        Width = 217
        Height = 73
        HelpContext = 52513
        Caption = 'Ignore Weekend Days for Due Dates'
        DataField = 'IGNORE_WEEKENDS'
        DataSource = wwdsDetail
        Items.Strings = (
          'Saturday-Sunday'
          'Sunday'
          'None')
        TabOrder = 17
        Values.Strings = (
          'A'
          'S'
          'N')
      end
      object evDBRadioGroup3: TevDBRadioGroup
        Left = 516
        Top = 54
        Width = 123
        Height = 35
        HelpContext = 52503
        Caption = 'Super Check Payment '
        Columns = 2
        DataField = 'CHECK_ONE_PAYMENT_TYPE'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 18
        Values.Strings = (
          'Y'
          'N')
      end
      object cbAgencyType: TevDBRadioGroup
        Left = 516
        Top = 94
        Width = 123
        Height = 35
        HelpContext = 52503
        Caption = ' Collection District '
        Columns = 2
        DataField = 'AGENCY_TYPE'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 19
        Values.Strings = (
          'Y'
          'N')
      end
    end
    object tsEftDebit: TTabSheet
      Caption = 'EFT Debit'
      ImageIndex = 2
      object evLabel3: TevLabel
        Left = 8
        Top = 8
        Width = 95
        Height = 13
        Caption = 'Payment File Report'
      end
      object evLabel4: TevLabel
        Left = 8
        Top = 136
        Width = 44
        Height = 13
        Caption = 'Status as'
      end
      object rgDebitPostToBankAccount: TevDBRadioGroup
        Left = 8
        Top = 64
        Width = 206
        Height = 57
        HelpContext = 52507
        Caption = 'Post to Bank Account Register'
        DataField = 'CUSTOM_DEBIT_POST_TO_REGISTER'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 1
        Values.Strings = (
          'Y'
          'N')
      end
      object cbluDebitPaymentReport: TevDBLookupCombo
        Left = 8
        Top = 24
        Width = 393
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
        DataField = 'CUSTOM_DEBIT_SY_REPORTS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS.SY_REPORTS
        LookupField = 'SY_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 0
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object cbDebitStatus: TevDBComboBox
        Left = 8
        Top = 152
        Width = 121
        Height = 21
        HelpContext = 43509
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'CUSTOM_DEBIT_AFTER_STATUS'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Pending'#9'X'
          'Paid'#9'P'
          'Paid-Send Notice'#9'S'
          'NSF'#9'N'
          'Voided'#9'V'
          'Send-Notice'#9'C'
          'Notice-Sent'#9'T'
          'Deleted'#9'D'
          'Manually impounded'#9'M'
          'Paid w/o funds'#9'W'
          'Impounded'#9'I')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = nil
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 252
    Top = 18
  end
end
