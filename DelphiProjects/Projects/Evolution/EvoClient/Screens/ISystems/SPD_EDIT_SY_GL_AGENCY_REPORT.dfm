inherited EDIT_SY_GL_AGENCY_REPORT: TEDIT_SY_GL_AGENCY_REPORT
  object Label16: TevLabel [0]
    Left = 9
    Top = 6
    Width = 103
    Height = 13
    Caption = 'Global Agency Name '
  end
  object DBText1: TevDBText [1]
    Left = 121
    Top = 7
    Width = 288
    Height = 17
    DataField = 'AGENCY_NAME'
    DataSource = wwdsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object PageControl1: TevPageControl [2]
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    ActivePage = tsDetail
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 803
        Height = 486
        Align = alClient
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 284
          Top = 1
          Height = 484
        end
        object wwDBGrid1: TevDBGrid
          Left = 1
          Top = 1
          Width = 283
          Height = 484
          DisableThemesInTitle = False
          Selected.Strings = (
            'AGENCY_NAME'#9'40'#9'Agency'
            'COUNTY'#9'40'#9'Locality'
            'STATE'#9'2'#9'State')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_SY_GL_AGENCY_REPORT\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsMaster
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object Panel2: TevPanel
          Left = 287
          Top = 1
          Width = 515
          Height = 484
          Align = alClient
          TabOrder = 1
          object Splitter2: TevSplitter
            Left = 265
            Top = 1
            Height = 482
          end
          object wwDBGrid3: TevDBGrid
            Left = 1
            Top = 1
            Width = 264
            Height = 482
            DisableThemesInTitle = False
            Selected.Strings = (
              'ADDITIONAL_NAME'#9'40'#9'Field Office')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_SY_GL_AGENCY_REPORT\wwDBGrid3'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alLeft
            DataSource = wwdsSubMaster
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
          end
        end
      end
    end
    object tsDetail: TTabSheet
      Caption = 'Detail'
      ImageIndex = 1
      object bevCL_1: TEvBevel
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
      end
      object Label2: TevLabel
        Left = 16
        Top = 279
        Width = 157
        Height = 13
        Caption = '~Company deposit frequency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TevLabel
        Left = 16
        Top = 429
        Width = 54
        Height = 13
        Caption = '~Tax Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TevLabel
        Left = 16
        Top = 362
        Width = 93
        Height = 13
        Caption = '~When Due Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object labNbdays: TevLabel
        Left = 16
        Top = 411
        Width = 146
        Height = 13
        Caption = 'Number of Days for When Due'
        Enabled = False
      end
      object Label5: TevLabel
        Left = 16
        Top = 319
        Width = 99
        Height = 13
        Caption = '~Return frequency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TevLabel
        Left = 298
        Top = 240
        Width = 61
        Height = 13
        Caption = '~Print when'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TevLabel
        Left = 298
        Top = 277
        Width = 147
        Height = 13
        Caption = '~Company tax service filter'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel1: TevLabel
        Left = 298
        Top = 313
        Width = 147
        Height = 13
        Caption = '~Company payment method'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel2: TevLabel
        Left = 298
        Top = 351
        Width = 233
        Height = 13
        Caption = '~Company consolidation filter'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel3: TevLabel
        Left = 298
        Top = 389
        Width = 225
        Height = 13
        Caption = '~945 Tax checks in payrolls'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel4: TevLabel
        Left = 298
        Top = 428
        Width = 225
        Height = 13
        Caption = '~943 Tax company'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel7: TevLabel
        Left = 298
        Top = 468
        Width = 225
        Height = 13
        Caption = '~944 Tax company'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel5: TevLabel
        Left = 16
        Top = 231
        Width = 185
        Height = 13
        Caption = '~Virtual Report Name'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel6: TevLabel
        Left = 556
        Top = 344
        Width = 105
        Height = 13
        Caption = 'Begin Effective Month'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel10: TevLabel
        Left = 556
        Top = 376
        Width = 97
        Height = 13
        Caption = 'End Effective Month'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel21: TevLabel
        Left = 556
        Top = 307
        Width = 89
        Height = 13
        Caption = 'Inactive Start Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwdbcbxFrequency: TevDBComboBox
        Left = 16
        Top = 295
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'DEPOSIT_FREQUENCY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object wwdbcbxTaxType: TevDBComboBox
        Left = 16
        Top = 445
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SYSTEM_TAX_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object wwdbcbxWhenDueType: TevDBComboBox
        Left = 16
        Top = 378
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'WHEN_DUE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
        OnChange = wwdbcbxWhenDueTypeChange
      end
      object dbedNbDays: TevDBEdit
        Left = 184
        Top = 403
        Width = 65
        Height = 21
        DataField = 'NUMBER_OF_DAYS_FOR_WHEN_DUE'
        DataSource = wwdsDetail
        Enabled = False
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object cdReturnFreq: TevDBComboBox
        Left = 16
        Top = 335
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'RETURN_FREQUENCY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object rgEnlistAuto: TevDBRadioGroup
        Left = 17
        Top = 466
        Width = 230
        Height = 33
        HelpContext = 5518
        Caption = '~Enlist automatically'
        Columns = 2
        DataField = 'ENLIST_AUTOMATICALLY'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object cbPrintWhen: TevDBComboBox
        Left = 298
        Top = 256
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'PRINT_WHEN'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object cbCoFilter: TevDBComboBox
        Left = 298
        Top = 293
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TAX_SERVICE_FILTER'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 8
        UnboundDataType = wwDefault
      end
      object cbCoPaymentMethod: TevDBComboBox
        Left = 298
        Top = 329
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'PAYMENT_METHOD'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 9
        UnboundDataType = wwDefault
      end
      object evDBComboBox2: TevDBComboBox
        Left = 298
        Top = 366
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'CONSOLIDATED_FILTER'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 10
        UnboundDataType = wwDefault
      end
      object cbTax945Filter: TevDBComboBox
        Left = 298
        Top = 404
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TAX945_CHECKS'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 11
        UnboundDataType = wwDefault
      end
      object cbTax943Filter: TevDBComboBox
        Left = 298
        Top = 442
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TAX943_COMPANY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 12
        UnboundDataType = wwDefault
      end
      object wwDBGrid4: TevDBGrid
        Left = 8
        Top = 7
        Width = 405
        Height = 209
        DisableThemesInTitle = False
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'Description'#9'F'
          'SY_REPORTS_GROUP_NBR'#9'10'#9'Sy Reports Group Nbr'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_GL_AGENCY_REPORT\wwDBGrid4'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 17
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object cbTax944Filter: TevDBComboBox
        Left = 298
        Top = 482
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TAX944_COMPANY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 13
        UnboundDataType = wwDefault
      end
      object cbSystemReportsGroup: TevDBLookupCombo
        Left = 16
        Top = 251
        Width = 256
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'60'#9'Name'#9#9)
        DataField = 'SY_REPORTS_GROUP_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS_GROUP.SY_REPORTS_GROUP
        LookupField = 'SY_REPORTS_GROUP_NBR'
        Style = csDropDownList
        TabOrder = 0
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = cbSystemReportsGroupCloseUp
      end
      object evDBSpinEdit1: TevDBSpinEdit
        Left = 708
        Top = 340
        Width = 49
        Height = 21
        Increment = 1.000000000000000000
        MaxValue = 12.000000000000000000
        MinValue = 1.000000000000000000
        DataField = 'BEGIN_EFFECTIVE_MONTH'
        DataSource = wwdsDetail
        MaxLength = 2
        TabOrder = 15
        UnboundDataType = wwDefault
      end
      object evDBSpinEdit2: TevDBSpinEdit
        Left = 708
        Top = 372
        Width = 49
        Height = 21
        Increment = 1.000000000000000000
        MaxValue = 12.000000000000000000
        MinValue = 1.000000000000000000
        DataField = 'END_EFFECTIVE_MONTH'
        DataSource = wwdsDetail
        MaxLength = 2
        TabOrder = 16
        UnboundDataType = wwDefault
      end
      object rgTaxReturnActive: TevDBRadioGroup
        Left = 553
        Top = 255
        Width = 230
        Height = 33
        HelpContext = 5518
        Caption = '~Tax Return Active'
        Columns = 2
        DataField = 'TAX_RETURN_ACTIVE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 14
      end
      object rgInactiveStartDate: TevDBDateTimePicker
        Left = 679
        Top = 299
        Width = 105
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'TAX_RETURN_INACTIVE_START_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 18
      end
      object evDBRadioGroup2: TevDBRadioGroup
        Left = 553
        Top = 404
        Width = 230
        Height = 33
        HelpContext = 5518
        Caption = '~Return 940'
        Columns = 2
        DataField = 'TAXRETURN940'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 19
      end
      object btnFieldoffice: TevBitBtn
        Left = 672
        Top = 224
        Width = 105
        Height = 25
        Caption = 'Field Office Set Up'
        TabOrder = 20
        OnClick = btnFieldofficeClick
      end
      object grdSyField: TevDBGrid
        Left = 417
        Top = 9
        Width = 392
        Height = 212
        DisableThemesInTitle = False
        Selected.Strings = (
          'AGENCY_NAME'#9'30'#9'Agency Name'#9'F'
          'STATE'#9'2'#9'State'#9#9
          'ADDITIONAL_NAME'#9'30'#9'Additional Name'#9'F'
          'CITY'#9'20'#9'City'#9#9
          'SY_GLOBAL_AGENCY_NBR'#9'10'#9'Sy Global Agency Nbr'#9#9
          'SY_GL_AGENCY_FIELD_OFFICE_NBR'#9'10'#9'Sy Gl Agency Field Office Nbr'#9#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_GL_AGENCY_REPORT\grdSyField'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsSYFieldOffice
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 21
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
    end
    object tsOldRecords: TTabSheet
      Caption = 'Old Records'
      ImageIndex = 2
      object EvBevel1: TEvBevel
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
      end
      object evLabel8: TevLabel
        Left = 284
        Top = 240
        Width = 61
        Height = 13
        Caption = '~Print when'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel9: TevLabel
        Left = 284
        Top = 277
        Width = 147
        Height = 13
        Caption = '~Company tax service filter'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel11: TevLabel
        Left = 284
        Top = 313
        Width = 147
        Height = 13
        Caption = '~Company payment method'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel12: TevLabel
        Left = 284
        Top = 351
        Width = 233
        Height = 13
        Caption = '~Company consolidation filter'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel13: TevLabel
        Left = 284
        Top = 389
        Width = 225
        Height = 13
        Caption = '~945 Tax checks in payrolls'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel14: TevLabel
        Left = 284
        Top = 428
        Width = 225
        Height = 13
        Caption = '~943 Tax company'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel15: TevLabel
        Left = 284
        Top = 468
        Width = 225
        Height = 13
        Caption = '~944 Tax company'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel16: TevLabel
        Left = 16
        Top = 451
        Width = 146
        Height = 13
        Caption = 'Number of Days for When Due'
        Enabled = False
      end
      object evLabel17: TevLabel
        Left = 16
        Top = 370
        Width = 54
        Height = 13
        Caption = '~Tax Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel18: TevLabel
        Left = 16
        Top = 325
        Width = 93
        Height = 13
        Caption = '~When Due Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel19: TevLabel
        Left = 16
        Top = 282
        Width = 99
        Height = 13
        Caption = '~Return frequency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel20: TevLabel
        Left = 16
        Top = 242
        Width = 157
        Height = 13
        Caption = '~Company deposit frequency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evDBGrid2: TevDBGrid
        Left = 8
        Top = 7
        Width = 825
        Height = 209
        DisableThemesInTitle = False
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'Description'#9'F'
          'SY_REPORTS_GROUP_NBR'#9'10'#9'Sy Reports Group Nbr'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_GL_AGENCY_REPORT\evDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object evDBGrid3: TevDBGrid
        Left = 536
        Top = 256
        Width = 289
        Height = 241
        DisableThemesInTitle = False
        Selected.Strings = (
          'NAME'#9'60'#9'Name'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_GL_AGENCY_REPORT\evDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object evDBComboBox1: TevDBComboBox
        Left = 284
        Top = 256
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'PRINT_WHEN'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object evDBComboBox3: TevDBComboBox
        Left = 284
        Top = 293
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TAX_SERVICE_FILTER'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object cbOldCoPaymentMethod: TevDBComboBox
        Left = 284
        Top = 329
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 4
        UnboundDataType = wwDefault
      end
      object evDBComboBox5: TevDBComboBox
        Left = 284
        Top = 366
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'CONSOLIDATED_FILTER'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object cbOldTax945Filter: TevDBComboBox
        Left = 284
        Top = 404
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object cbOldTax943Filter: TevDBComboBox
        Left = 284
        Top = 442
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object cbOldTax944Filter: TevDBComboBox
        Left = 284
        Top = 482
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 8
        UnboundDataType = wwDefault
      end
      object evDBEdit1: TevDBEdit
        Left = 184
        Top = 443
        Width = 65
        Height = 21
        DataField = 'NUMBER_OF_DAYS_FOR_WHEN_DUE'
        DataSource = wwdsDetail
        Enabled = False
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 17
        Top = 407
        Width = 230
        Height = 33
        HelpContext = 5518
        Caption = '~Enlist automatically'
        Columns = 2
        DataField = 'ENLIST_AUTOMATICALLY'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 10
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBComboBox9: TevDBComboBox
        Left = 16
        Top = 386
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SYSTEM_TAX_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 11
        UnboundDataType = wwDefault
      end
      object evDBComboBox10: TevDBComboBox
        Left = 16
        Top = 341
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'WHEN_DUE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 12
        UnboundDataType = wwDefault
        OnChange = wwdbcbxWhenDueTypeChange
      end
      object evDBComboBox11: TevDBComboBox
        Left = 16
        Top = 298
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'RETURN_FREQUENCY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 13
        UnboundDataType = wwDefault
      end
      object evDBComboBox12: TevDBComboBox
        Left = 16
        Top = 258
        Width = 233
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'DEPOSIT_FREQUENCY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 14
        UnboundDataType = wwDefault
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_GL_AGENCY_REPORT.SY_GL_AGENCY_REPORT
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = wwdsSubMaster
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 249
    Top = 18
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_SY_GL_AGENCY_FIELD_OFFICE.SY_GL_AGENCY_FIELD_OFFICE
    MasterDataSource = wwdsMaster
    Left = 111
    Top = 54
  end
  object dsSYFieldOffice: TevDataSource
    DataSet = cdSyFieldOffice
    Left = 534
    Top = 250
  end
  object cdSyFieldOffice: TevClientDataSet
    Left = 496
    Top = 248
    object cdSyFieldOfficeAGENCY_NAME: TStringField
      FieldName = 'AGENCY_NAME'
      Size = 40
    end
    object cdSyFieldOfficeSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object cdSyFieldOfficeADDITIONAL_NAME: TStringField
      FieldName = 'ADDITIONAL_NAME'
      Size = 40
    end
    object cdSyFieldOfficeCITY: TStringField
      FieldName = 'CITY'
    end
    object cdSyFieldOfficeSY_GLOBAL_AGENCY_NBR: TIntegerField
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
    end
    object cdSyFieldOfficeSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
    end
  end
end
