// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_SUI;

interface

uses
  SDataStructure,  SPD_EDIT_SY_STATE_BASE, wwdblook,
  wwdbdatetimepicker, DBCtrls, StdCtrls, Mask, wwdbedit, Wwdotdot, isVCLBugFix,
  Wwdbcomb, ExtCtrls, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls,
  ComCtrls, Classes, ISBasicClasses, SDDClasses, SysUtils, SDataDictsystem,
  EvDataAccessComponents, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, isUIwwDBLookupCombo, isUIwwDBComboBox;

type
  TEDIT_SY_SUI = class(TEDIT_SY_STATE_BASE)
    DBRadioGroup8: TevDBRadioGroup;
    DBRadioGroup7: TevDBRadioGroup;
    Detail: TTabSheet;
    Label4: TevLabel;
    dbedtSUI_TAX_NAME: TevDBEdit;
    Label6: TevLabel;
    Label9: TevLabel;
    DBRadioGroup2: TevDBRadioGroup;
    wwDBGrid2: TevDBGrid;
    DBRadioGroup4: TevDBRadioGroup;
    wwDBGrid3: TevDBGrid;
    Label14: TevLabel;
    wwDBComboBox4: TevDBComboBox;
    wwdeMaximum_Wage: TevDBEdit;
    wwdeNew_Company_Default_Rate: TevDBEdit;
    Label2: TevLabel;
    Label3: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    wwDBLookupCombo2: TevDBLookupCombo;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    DBRadioGroup5: TevDBRadioGroup;
    evLabel2: TevLabel;
    luTaxCoupon: TevDBLookupCombo;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    rgActive: TevDBRadioGroup;
    wwcdE_D_Code_Type: TevDBComboDlg;
    lablE_D_Code_Type: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evDBEdit2: TevDBEdit;
    evDBEdit3: TevDBEdit;
    evGroupBox1: TevGroupBox;
    evLabel5: TevLabel;
    evDBEdit4: TevDBEdit;
    evLabel6: TevLabel;
    evDBEdit5: TevDBEdit;
    evLabel7: TevLabel;
    evDBEdit6: TevDBEdit;
    rgAddAllButton: TevDBRadioGroup;
    evLabel8: TevLabel;
    cbSystemReportsGroup: TevDBLookupCombo;
    Label5: TLabel;
    AltTaxWageBaseEdit: TevDBEdit;
    procedure wwcdE_D_Code_TypeCustomDlg(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

uses EvUtils, EvConsts, SFrameEntry;

procedure TEDIT_SY_SUI.Activate;
begin
  inherited;
  with ctx_DataAccess.SY_CUSTOM_VIEW do
  begin
    Close;
    with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 't1.sy_reports_nbr, t1.description');
      SetMacro('TABLE1', 'sy_reports');
      SetMacro('TABLE2', 'SY_REPORT_WRITER_REPORTS');
      SetMacro('JOINFIELD', 'SY_REPORT_WRITER_REPORTS');
      SetMacro('CONDITION', 't2.REPORT_TYPE = :T');
      SetParam('T', SYSTEM_REPORT_TYPE_TAXCOUPON);
      DataRequest(AsVariant);
    end;
    Open;
  end;
  luTaxCoupon.DataSource := nil;
  luTaxCoupon.LookupTable := CreateLookupDataset(ctx_DataAccess.SY_CUSTOM_VIEW, Self);
  luTaxCoupon.DataSource := wwdsDetail;
end;

function TEDIT_SY_SUI.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_STATE.SY_SUI;
end;

function TEDIT_SY_SUI.GetInsertControl: TWinControl;
begin
  Result := dbedtSUI_TAX_NAME;
end;

procedure TEDIT_SY_SUI.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS_GROUP, SupportDataSets, 'SY_REPORTS_GROUP_NBR > 20000');
end;

procedure TEDIT_SY_SUI.wwcdE_D_Code_TypeCustomDlg(Sender: TObject);
begin
  inherited;
  TevDBComboDlg(Sender).ShowCategorizedComboDialog;
end;

initialization
  RegisterClass(TEDIT_SY_SUI);

end.


