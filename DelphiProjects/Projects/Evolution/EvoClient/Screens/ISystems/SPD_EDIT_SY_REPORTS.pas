// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_REPORTS;

interface

uses
   SFrameEntry, wwdblook, StdCtrls, wwdbedit, Wwdotdot,
  Wwdbcomb, Mask, DBCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid, Classes,
  Db, Wwdatsrc, SDataStructure, SFieldCodeValues, ExtCtrls, Wwdbspin,
  SDDClasses, SDataDictsystem, ISBasicClasses, wwdbdatetimepicker, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_REPORTS = class(TFrameEntry)
    wwDBGrid1: TevDBGrid;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evPanel1: TevPanel;
    lablReport_Description: TevLabel;
    Label2: TevLabel;
    dedtReport_Description: TevDBEdit;
    cbPickReport: TevGroupBox;
    cbReportRw: TevDBLookupCombo;
    cbPriority: TevDBComboBox;
    evGroupBox1: TevGroupBox;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel3: TevLabel;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_REPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_REPORTS;
end;

function TEDIT_SY_REPORTS.GetInsertControl: TWinControl;
begin
  Result := dedtReport_Description;
end;

procedure TEDIT_SY_REPORTS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS_GROUP, SupportDataSets, 'SY_REPORTS_GROUP_NBR > 20000');

end;

initialization
  RegisterClass(TEDIT_SY_REPORTS);

end.
