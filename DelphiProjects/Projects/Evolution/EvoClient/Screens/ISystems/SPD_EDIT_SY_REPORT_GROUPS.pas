unit SPD_EDIT_SY_REPORT_GROUPS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, SFrameEntry, DB, Wwdatsrc, ISBasicClasses, 
  SDDClasses, SDataStructure, StdCtrls, Mask, wwdbedit,
  Grids, Wwdbigrd, Wwdbgrid, ComCtrls, SDataDictsystem, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_REPORT_GROUPS = class(TFrameEntry)
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    grGroups: TevDBGrid;
    TabSheet2: TTabSheet;
    Label1: TevLabel;
    edName: TevDBEdit;
  private
  protected
    function  GetDefaultDataSet: TevClientDataSet; override;
  public
  end;

implementation

{$R *.dfm}

{ TEDIT_SY_REPORT_GROUPS }

function TEDIT_SY_REPORT_GROUPS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_REPORT_GROUPS;
end;

initialization
  RegisterClass(TEDIT_SY_REPORT_GROUPS);

end.
 