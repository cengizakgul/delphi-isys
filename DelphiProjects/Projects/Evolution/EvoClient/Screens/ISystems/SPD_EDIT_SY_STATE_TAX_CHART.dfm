inherited EDIT_SY_STATE_TAX_CHART: TEDIT_SY_STATE_TAX_CHART
  inherited PageControl1: TevPageControl
    HelpContext = 4501
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        inherited wwDBGrid1: TevDBGrid
          Selected.Strings = (
            'STATE'#9'2'#9'State'#9
            'NAME'#9'20'#9'Name'#9)
          IniAttributes.SectionName = 'TEDIT_SY_STATE_TAX_CHART\wwDBGrid1'
          PaintOptions.AlternatingRowColor = 14544093
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Details'
      object Label2: TevLabel
        Left = 96
        Top = 144
        Width = 92
        Height = 13
        Caption = 'State Marital Status'
      end
      object Label3: TevLabel
        Left = 32
        Top = 244
        Width = 50
        Height = 16
        Caption = '~Minimum'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TevLabel
        Left = 32
        Top = 276
        Width = 53
        Height = 16
        Caption = '~Maximum'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TevLabel
        Left = 64
        Top = 304
        Width = 17
        Height = 16
        Caption = '~%'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TevLabel
        Left = 328
        Top = 144
        Width = 24
        Height = 13
        Caption = 'Type'
      end
      object wwDBGrid2: TevDBGrid
        Left = 12
        Top = 12
        Width = 593
        Height = 113
        DisableThemesInTitle = False
        ControlType.Strings = (
          'ENTRY_TYPE;CustomEdit;evcbEntryType;F')
        Selected.Strings = (
          'MARITAL_STATUS_DESCRPTION'#9'20'#9'MARITAL_STATUS'
          'MINIMUM'#9'10'#9'Minimum'#9
          'MAXIMUM'#9'10'#9'Maximum'#9
          'PERCENTAGE'#9'10'#9'Percentage'#9
          'ENTRY_TYPE'#9'25'#9'Type                               '#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_STATE_TAX_CHART\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object dblkpSTATE_MARITAL_STATUS: TevDBLookupCombo
        Left = 96
        Top = 160
        Width = 194
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'STATUS_DESCRIPTION'#9'40'#9'STATUS_DESCRIPTION')
        DataField = 'SY_STATE_MARITAL_STATUS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
        LookupField = 'SY_STATE_MARITAL_STATUS_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwDBComboBox1: TevDBComboBox
        Left = 328
        Top = 160
        Width = 153
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'ENTRY_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object wwdeMinimum: TevDBEdit
        Left = 96
        Top = 240
        Width = 72
        Height = 21
        DataField = 'MINIMUM'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeMaximum: TevDBEdit
        Left = 96
        Top = 272
        Width = 72
        Height = 21
        DataField = 'MAXIMUM'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePercent: TevDBEdit
        Left = 96
        Top = 304
        Width = 72
        Height = 21
        DataField = 'PERCENTAGE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object evcbEntryType: TevDBComboBox
        Left = 408
        Top = 64
        Width = 158
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'ENTRY_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_STATE_TAX_CHART.SY_STATE_TAX_CHART
    MasterDataSource = StateClone
  end
  inherited DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 288
  end
  object StateClone: TevDataSource
    DataSet = DM_SY_STATES.SY_STATES
    Left = 153
    Top = 51
  end
  object evMaritalStatus: TevDataSource
    DataSet = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
    MasterDataSource = wwdsMaster
    Left = 246
    Top = 18
  end
end
