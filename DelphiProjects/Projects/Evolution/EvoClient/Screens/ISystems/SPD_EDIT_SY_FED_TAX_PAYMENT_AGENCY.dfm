inherited EDIT_SY_FED_TAX_PAYMENT_AGENCY: TEDIT_SY_FED_TAX_PAYMENT_AGENCY
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 277
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 435
        Height = 249
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 237
          Top = 0
          Width = 3
          Height = 249
          Cursor = crHSplit
        end
        object wwDBGrid1: TevDBGrid
          Left = 0
          Top = 0
          Width = 237
          Height = 249
          Selected.Strings = (
            'AGENCY_NAME'#9'40'#9'AGENCY_NAME'#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsDetail
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          TitleButtons = True
          UseTFields = False
          IndicatorColor = icBlack
          LocateDlg = True
          FilterDlg = True
          Sorting = True
        end
      end
    end
    object Address: TTabSheet
      Caption = 'Details'
      object lablReport_Description: TevLabel
        Left = 16
        Top = 104
        Width = 107
        Height = 13
        Caption = '~Report Description'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAgency_Name: TevLabel
        Left = 16
        Top = 48
        Width = 79
        Height = 13
        Caption = '~Agency Name'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwlcReport_Description: TevDBLookupCombo
        Left = 16
        Top = 120
        Width = 329
        Height = 21
        HelpContext = 2013
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
        DataField = 'SY_REPORTS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS.SY_REPORTS
        LookupField = 'SY_REPORTS_NBR'
        Style = csDropDownList
        ReadOnly = False
        TabOrder = 1
        AutoDropDown = False
        ShowButton = True
        AllowClearKey = False
      end
      object wwlcAgency_Name: TevDBLookupCombo
        Left = 16
        Top = 64
        Width = 297
        Height = 21
        HelpContext = 2002
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'AGENCY_NAME'#9'F')
        DataField = 'SY_GLOBAL_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
        LookupField = 'SY_GLOBAL_AGENCY_NBR'
        Style = csDropDownList
        ReadOnly = False
        TabOrder = 0
        AutoDropDown = False
        ShowButton = True
        AllowClearKey = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_FED_TAX_PAYMENT_AGENCY.SY_FED_TAX_PAYMENT_AGENCY
  end
  object DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL
    Left = 268
    Top = 27
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 319
    Top = 27
  end
end
