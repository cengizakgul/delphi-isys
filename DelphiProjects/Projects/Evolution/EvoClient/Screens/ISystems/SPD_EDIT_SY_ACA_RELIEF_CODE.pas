// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_ACA_RELIEF_CODE;

interface

uses
  SFrameEntry,  SDataStructure, DBCtrls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet, SDDClasses,
  SDataDictsystem, isUIwwDBEdit, ISBasicClasses, wwdblook,
  isUIwwDBLookupCombo, EvUtils, Wwdbcomb, isUIwwDBComboBox,
  EvCommonInterfaces, EvDataset, EvExceptions, SysUtils;

type
  TEDIT_SY_ACA_RELIEF_CODE = class(TFrameEntry)
    wwDBGrid1: TevDBGrid;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    lbCode: TevLabel;
    edtCode: TevDBEdit;
    lblDesc: TevLabel;
    edDesc: TevDBEdit;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;

    { Public declarations }
  public

  end;

implementation

uses
  SPackageEntry;

{$R *.DFM}

function TEDIT_SY_ACA_RELIEF_CODE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_FED_ACA_RELIEF_CODES;
end;

function TEDIT_SY_ACA_RELIEF_CODE.GetInsertControl: TWinControl;
begin
  Result := edtCode;
end;

initialization
  RegisterClass(TEDIT_SY_ACA_RELIEF_CODE);

end.
