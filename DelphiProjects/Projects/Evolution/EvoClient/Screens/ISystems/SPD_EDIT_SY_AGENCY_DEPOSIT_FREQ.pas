// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_AGENCY_DEPOSIT_FREQ;

interface

uses
  SFrameEntry,  StdCtrls, Mask, DBCtrls, Grids, Wwdbigrd, Wwdbgrid,
  ExtCtrls, Controls, ComCtrls, Classes, Db, Wwdatsrc, SDataStructure,
  wwdbedit, wwdblook, Wwdotdot, Wwdbcomb, ISBasicClasses, SDDClasses,
  SDataDictsystem, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_AGENCY_DEPOSIT_FREQ = class(TFrameEntry)
    PageControl1: TevPageControl;
    tsBrowse: TTabSheet;
    tsDetail: TTabSheet;
    Label16: TevLabel;
    DBText1: TevDBText;
    Panel1: TevPanel;
    wwDBGrid1: TevDBGrid;
    Splitter1: TevSplitter;
    Panel2: TevPanel;
    wwDBGrid2: TevDBGrid;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    wwDBGrid3: TevDBGrid;
    lablDescription: TevLabel;
    dedtDescription: TevDBEdit;
    lablType: TevLabel;
    wwcbType: TevDBComboBox;
    lablWhen_Due_Type: TevLabel;
    wwcbWhen_Due_Type: TevDBComboBox;
    lablNbr_Days_For_When_Due: TevLabel;
    dedtNbr_Days_For_When_Due: TevDBEdit;
    Label5: TevLabel;
    edTypeCode: TevDBEdit;
    evLabel8: TevLabel;
    cbSystemReportsGroup: TevDBLookupCombo;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_AGENCY_DEPOSIT_FREQ.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ;
end;

function TEDIT_SY_AGENCY_DEPOSIT_FREQ.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

procedure TEDIT_SY_AGENCY_DEPOSIT_FREQ.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS_GROUP, SupportDataSets, 'SY_REPORTS_GROUP_NBR > 20000');
end;

initialization
  RegisterClass(TEDIT_SY_AGENCY_DEPOSIT_FREQ);

end.
