inherited EDIT_SY_GL_AGENCY_FIELD_OFFICE: TEDIT_SY_GL_AGENCY_FIELD_OFFICE
  Width = 794
  Height = 603
  object Label16: TevLabel [0]
    Left = 8
    Top = 8
    Width = 103
    Height = 13
    Caption = 'Global Agency Name '
  end
  object DBText1: TevDBText [1]
    Left = 120
    Top = 8
    Width = 60
    Height = 16
    AutoSize = True
    DataField = 'AGENCY_NAME'
    DataSource = wwdsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object PageControl1: TevPageControl [2]
    Left = 0
    Top = 31
    Width = 628
    Height = 442
    HelpContext = 53501
    ActivePage = tsDetail
    TabOrder = 0
    object tsBrowse: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 617
        Height = 410
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 354
          Top = 1
          Height = 408
        end
        object wwDBGrid1: TevDBGrid
          Left = 1
          Top = 1
          Width = 353
          Height = 408
          DisableThemesInTitle = False
          Selected.Strings = (
            'AGENCY_NAME'#9'40'#9'Agency'
            'COUNTY'#9'40'#9'Locality'
            'STATE'#9'2'#9'State')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_SY_GL_AGENCY_FIELD_OFFICE\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsMaster
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object Panel2: TevPanel
          Left = 357
          Top = 1
          Width = 259
          Height = 408
          Align = alClient
          TabOrder = 1
          object wwDBGrid2: TevDBGrid
            Left = 1
            Top = 1
            Width = 257
            Height = 406
            DisableThemesInTitle = False
            Selected.Strings = (
              'ADDITIONAL_NAME'#9'40'#9'Field Office')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_SY_GL_AGENCY_FIELD_OFFICE\wwDBGrid2'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
    object tsDetail: TTabSheet
      Caption = 'Detail'
      object lablAdditional_Name: TevLabel
        Left = 16
        Top = 32
        Width = 86
        Height = 16
        Caption = '~Additional Name'
        FocusControl = dedtAdditional_Name
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAddress1: TevLabel
        Left = 16
        Top = 72
        Width = 56
        Height = 16
        Caption = '~Address 1'
        FocusControl = dedtAddress1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAddress2: TevLabel
        Left = 16
        Top = 112
        Width = 47
        Height = 13
        Caption = 'Address 2'
        FocusControl = dedtAddress2
      end
      object lablCity: TevLabel
        Left = 16
        Top = 152
        Width = 26
        Height = 16
        Caption = '~City'
        FocusControl = dedtCity
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablZip_Code: TevLabel
        Left = 16
        Top = 232
        Width = 52
        Height = 16
        Caption = '~Zip Code'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablState: TevLabel
        Left = 16
        Top = 192
        Width = 34
        Height = 16
        Caption = '~State'
        FocusControl = dedtState
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAttention_Name: TevLabel
        Left = 16
        Top = 272
        Width = 82
        Height = 16
        Caption = '~Attention Name'
        FocusControl = dedtAttention_Name
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablFax: TevLabel
        Left = 16
        Top = 312
        Width = 17
        Height = 13
        Caption = 'Fax'
      end
      object lablFax_Description: TevLabel
        Left = 16
        Top = 352
        Width = 73
        Height = 13
        Caption = 'Fax Description'
        FocusControl = dedtFax_Description
      end
      object evLabel1: TevLabel
        Left = 312
        Top = 32
        Width = 78
        Height = 16
        Caption = '~Category Type'
        FocusControl = evDBLookupCombo1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dedtAdditional_Name: TevDBEdit
        Left = 16
        Top = 48
        Width = 244
        Height = 21
        DataField = 'ADDITIONAL_NAME'
        DataSource = wwdsDetail
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtAddress1: TevDBEdit
        Left = 16
        Top = 88
        Width = 184
        Height = 21
        DataField = 'ADDRESS1'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtAddress2: TevDBEdit
        Left = 16
        Top = 128
        Width = 184
        Height = 21
        DataField = 'ADDRESS2'
        DataSource = wwdsDetail
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtCity: TevDBEdit
        Left = 16
        Top = 168
        Width = 124
        Height = 21
        DataField = 'CITY'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtState: TevDBEdit
        Left = 16
        Top = 208
        Width = 33
        Height = 21
        CharCase = ecUpperCase
        DataField = 'STATE'
        DataSource = wwdsDetail
        Picture.PictureMask = '*{&,@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtAttention_Name: TevDBEdit
        Left = 16
        Top = 288
        Width = 244
        Height = 21
        DataField = 'ATTENTION_NAME'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtFax_Description: TevDBEdit
        Left = 16
        Top = 368
        Width = 64
        Height = 21
        DataField = 'FAX_DESCRIPTION'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object drgpPrimary_Contact: TevGroupBox
        Left = 312
        Top = 79
        Width = 233
        Height = 153
        Caption = 'Primary Contact'
        TabOrder = 10
        object lablName1: TevLabel
          Left = 27
          Top = 20
          Width = 28
          Height = 13
          Caption = 'Name'
        end
        object lablPhone1: TevLabel
          Left = 27
          Top = 59
          Width = 31
          Height = 13
          Caption = 'Phone'
        end
        object lablDescription1: TevLabel
          Left = 27
          Top = 99
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = dedtDescription1
        end
        object dedtDescription1: TevDBEdit
          Left = 27
          Top = 115
          Width = 64
          Height = 21
          DataField = 'DESCRIPTION1'
          DataSource = wwdsDetail
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePhone1: TevDBEdit
          Left = 27
          Top = 75
          Width = 117
          Height = 21
          DataField = 'PHONE1'
          DataSource = wwdsDetail
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtContact1: TevDBEdit
          Left = 27
          Top = 35
          Width = 184
          Height = 21
          DataField = 'CONTACT1'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object gboxSecondary_Contact: TevGroupBox
        Left = 312
        Top = 255
        Width = 233
        Height = 145
        Caption = 'Secondary Contact'
        TabOrder = 11
        object lablContact2: TevLabel
          Left = 27
          Top = 19
          Width = 28
          Height = 13
          Caption = 'Name'
          FocusControl = dedtContact2
        end
        object lablPhone2: TevLabel
          Left = 27
          Top = 59
          Width = 31
          Height = 13
          Caption = 'Phone'
        end
        object lablDescription2: TevLabel
          Left = 27
          Top = 97
          Width = 56
          Height = 13
          Caption = 'Description '
          FocusControl = dedtDescription
        end
        object dedtContact2: TevDBEdit
          Left = 27
          Top = 33
          Width = 184
          Height = 21
          DataField = 'CONTACT2'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtDescription: TevDBEdit
          Left = 27
          Top = 113
          Width = 64
          Height = 21
          DataField = 'DESCRIPTION2'
          DataSource = wwdsDetail
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePhone2: TevDBEdit
          Left = 27
          Top = 73
          Width = 117
          Height = 21
          DataField = 'PHONE2'
          DataSource = wwdsDetail
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object wwdeFax: TevDBEdit
        Left = 16
        Top = 328
        Width = 121
        Height = 21
        DataField = 'FAX'
        DataSource = wwdsDetail
        Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeZip_Code: TevDBEdit
        Left = 16
        Top = 248
        Width = 105
        Height = 21
        DataField = 'ZIP_CODE'
        DataSource = wwdsDetail
        Picture.PictureMask = '*5{#}[-*4#]'
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object evDBLookupCombo1: TevDBComboBox
        Left = 312
        Top = 48
        Width = 234
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'CATEGORY_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 9
        UnboundDataType = wwDefault
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_GL_AGENCY_FIELD_OFFICE.SY_GL_AGENCY_FIELD_OFFICE
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 249
    Top = 18
  end
end
