inherited EDIT_SY_HR_REFUSAL_REASON: TEDIT_SY_HR_REFUSAL_REASON
  Width = 708
  Height = 574
  inherited pnlBrowse: TevPanel
    Width = 708
    inherited dgBrowse: TevDBGrid
      Width = 708
      ControlType.Strings = (
        'HEALTHCARE_COVERAGE;CustomEdit;cbHealthCareGrid;F')
      Selected.Strings = (
        'DESCRIPTION'#9'40'#9'Description'#9'F'
        'StateName'#9'20'#9'State'#9'F'
        'HEALTHCARE_COVERAGE'#9'30'#9'Healthcare Coverage'#9'F')
      IniAttributes.SectionName = 'TEDIT_SY_HR_REFUSAL_REASON\dgBrowse'
    end
    object cbHealthCareGrid: TevDBComboBox
      Left = 264
      Top = 245
      Width = 217
      Height = 21
      HelpContext = 52511
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'HEALTHCARE_COVERAGE'
      DataSource = wwdsDetail
      DropDownCount = 8
      ItemHeight = 0
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 1
      UnboundDataType = wwDefault
    end
  end
  inherited pnlDBControls: TevPanel
    Width = 708
    Height = 141
    inherited evLabel1: TevLabel
      Width = 105
      Caption = '~Refusal Reason'
    end
    object evLabel2: TevLabel [1]
      Left = 248
      Top = 40
      Width = 116
      Height = 13
      Caption = 'Healthcare Coverage     '
    end
    object evLabel3: TevLabel [2]
      Left = 8
      Top = 40
      Width = 43
      Height = 13
      Caption = 'State      '
    end
    inherited dedtDescription: TevDBEdit
      Left = 136
    end
    object edHealtcareCoverage: TevDBComboBox
      Left = 248
      Top = 61
      Width = 217
      Height = 21
      HelpContext = 52511
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'HEALTHCARE_COVERAGE'
      DataSource = wwdsDetail
      DropDownCount = 8
      ItemHeight = 0
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 1
      UnboundDataType = wwDefault
    end
    object edStates: TevDBLookupCombo
      Left = 8
      Top = 61
      Width = 196
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'NAME'#9'20'#9'Name'#9#9
        'STATE'#9'2'#9'State'#9'F')
      DataField = 'SY_STATES_NBR'
      DataSource = wwdsDetail
      LookupTable = DM_SY_STATES.SY_STATES
      LookupField = 'SY_STATES_NBR'
      Style = csDropDownList
      TabOrder = 2
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_HR_REFUSAL_REASON.SY_HR_REFUSAL_REASON
  end
end
