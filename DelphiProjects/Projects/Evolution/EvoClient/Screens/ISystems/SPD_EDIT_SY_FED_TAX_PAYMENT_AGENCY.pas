// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_FED_TAX_PAYMENT_AGENCY;

interface

uses
  SFrameEntry,  StdCtrls, wwdblook,
  Grids, Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, Classes, Db,
  Wwdatsrc, SDataStructure, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_FED_TAX_PAYMENT_AGENCY = class(TFrameEntry)
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    Address: TTabSheet;
    Panel1: TevPanel;
    wwDBGrid1: TevDBGrid;
    Splitter1: TevSplitter;
    lablReport_Description: TevLabel;
    wwlcReport_Description: TevDBLookupCombo;
    lablAgency_Name: TevLabel;
    wwlcAgency_Name: TevDBLookupCombo;
    DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_FED_TAX_PAYMENT_AGENCY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY;
end;

function TEDIT_SY_FED_TAX_PAYMENT_AGENCY.GetInsertControl: TWinControl;
begin
  Result := wwlcAgency_Name;
end;

procedure TEDIT_SY_FED_TAX_PAYMENT_AGENCY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_FED_TAX_PAYMENT_AGENCY);

end.
