inherited EDIT_SY_REPORTS_GROUP: TEDIT_SY_REPORTS_GROUP
  object evPanel2: TevPanel [0]
    Left = 394
    Top = 0
    Width = 41
    Height = 266
    Align = alClient
    Caption = 'evPanel2'
    TabOrder = 0
    object evPanel3: TevPanel
      Left = 1
      Top = 124
      Width = 39
      Height = 141
      Align = alBottom
      BevelOuter = bvNone
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      TabOrder = 0
      object Label2: TevLabel
        Left = 332
        Top = 59
        Width = 40
        Height = 13
        Caption = '~Priority'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel3: TevLabel
        Left = 4
        Top = 8
        Width = 124
        Height = 13
        Caption = 'Report Active Years From '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cbPriority: TevDBComboBox
        Left = 332
        Top = 75
        Width = 49
        Height = 21
        HelpContext = 54001
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'ACTIVE_REPORT'
        DataSource = dsSYReports
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          '1'#9'1'
          '2'#9'2'
          '3'#9'3'
          '4'#9'4'
          '5'#9'5'
          '6'#9'6'
          '7'#9'7'
          '8'#9'8'
          '9'#9'9')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object evGroupBox1: TevGroupBox
        Left = 3
        Top = 48
        Width = 310
        Height = 49
        Caption = 'Report Active Years'
        TabOrder = 1
        object evLabel2: TevLabel
          Left = 8
          Top = 24
          Width = 23
          Height = 13
          Caption = 'From'
        end
        object evLabel4: TevLabel
          Left = 176
          Top = 24
          Width = 13
          Height = 13
          Caption = 'To'
        end
        object evDBDateTimePicker1: TevDBDateTimePicker
          Left = 48
          Top = 20
          Width = 115
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          DataField = 'ACTIVE_YEAR_FROM'
          DataSource = dsSYReports
          Epoch = 1950
          ShowButton = True
          TabOrder = 0
        end
        object evDBDateTimePicker2: TevDBDateTimePicker
          Left = 200
          Top = 16
          Width = 105
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          DataField = 'ACTIVE_YEAR_TO'
          DataSource = dsSYReports
          Epoch = 1950
          ShowButton = True
          TabOrder = 1
        end
      end
      inline SyReportsMiniNavigationFrame: TMiniNavigationFrame
        Left = 331
        Top = 15
        Width = 68
        Height = 25
        TabOrder = 2
        inherited SpeedButton1: TevSpeedButton
          Left = 2
        end
        inherited evActionList1: TevActionList
          Left = 72
          Top = 8
        end
      end
      object cbReportRw: TevDBLookupCombo
        Left = 5
        Top = 21
        Width = 308
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'REPORT_DESCRIPTION'#9'40'#9'Report Description'
          'SY_REPORT_WRITER_REPORTS_NBR'#9'10'#9'SY_REPORT_WRITER_REPORTS_NBR'#9'F')
        DataField = 'SY_REPORT_WRITER_REPORTS_NBR'
        DataSource = dsSYReports
        LookupTable = DM_SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS
        LookupField = 'SY_REPORT_WRITER_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
        OnCloseUp = cbReportRwCloseUp
      end
      object cbVirtualFilter: TevCheckBox
        Left = 8
        Top = 104
        Width = 145
        Height = 17
        Caption = 'All Reports'
        TabOrder = 4
        OnClick = cbVirtualFilterClick
      end
      object btnVirtualSetUp: TevBitBtn
        Left = 248
        Top = 112
        Width = 105
        Height = 25
        Caption = 'Virtual Report Set Up'
        Enabled = False
        TabOrder = 5
        OnClick = btnVirtualSetUpClick
      end
      object cbNullNbr: TevCheckBox
        Left = 8
        Top = 124
        Width = 225
        Height = 17
        Caption = 'Show only Null Sy Reports Groups Nbr'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 6
        OnClick = cbVirtualFilterClick
      end
    end
    object grdSyReports: TevDBGrid
      Left = 1
      Top = 1
      Width = 39
      Height = 123
      DisableThemesInTitle = False
      ControlType.Strings = (
        'SY_REPORT_WRITER_REPORTS_NBR;CustomEdit;evRWReports;F')
      Selected.Strings = (
        'DESCRIPTION'#9'40'#9'Description'#9'F'
        'ACTIVE_YEAR_FROM'#9'15'#9'Year From'#9'F'
        'ACTIVE_YEAR_TO'#9'15'#9'Year To'#9'F'
        'SY_REPORTS_GROUP_NBR'#9'10'#9'Sy Reports Group Nbr'#9'F'
        'SY_REPORT_WRITER_REPORTS_NBR'#9'10'#9'Sy Report Writer Reports Nbr'#9'F'
        'NDescription'#9'60'#9'Ndescription'#9'F')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TEDIT_SY_REPORTS_GROUP\grdSyReports'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = dsSYReports
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
      TabOrder = 1
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
    end
  end
  object evPanel4: TevPanel [1]
    Left = 0
    Top = 0
    Width = 394
    Height = 266
    Align = alLeft
    Caption = 'evPanel4'
    TabOrder = 1
    object wwDBGrid1: TevDBGrid
      Left = 1
      Top = 1
      Width = 392
      Height = 124
      DisableThemesInTitle = False
      Selected.Strings = (
        'NAME'#9'40'#9'Name'#9'F'
        'AGENCY_COPY'#9'1'#9'Agency Copy'#9#9
        'SB_COPY'#9'1'#9'Sb Copy'#9#9
        'CL_COPY'#9'1'#9'Cl Copy'#9#9
        'MEDIA_TYPE'#9'1'#9'Media Type'#9#9)
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TEDIT_SY_REPORTS_GROUP\wwDBGrid1'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      OnRowChanged = wwDBGrid1RowChanged
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = wwdsDetail
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
    end
    object evPanel1: TevPanel
      Left = 1
      Top = 125
      Width = 392
      Height = 140
      Align = alBottom
      BevelOuter = bvNone
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      TabOrder = 1
      object lablReport_Description: TevLabel
        Left = 12
        Top = 14
        Width = 181
        Height = 13
        Caption = '~Virtual Report Description'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel6: TevLabel
        Left = 208
        Top = 68
        Width = 34
        Height = 13
        Caption = 'Use for'
      end
      object gbReportCopies: TevGroupBox
        Left = 8
        Top = 60
        Width = 185
        Height = 57
        Caption = 'Copies'
        TabOrder = 0
        object dbcAgency: TevDBCheckBox
          Left = 8
          Top = 24
          Width = 57
          Height = 17
          Caption = 'Agency'
          DataField = 'AGENCY_COPY'
          DataSource = wwdsDetail
          TabOrder = 0
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object dbcSb: TevDBCheckBox
          Left = 72
          Top = 24
          Width = 49
          Height = 17
          Caption = 'S/B'
          DataField = 'SB_COPY'
          DataSource = wwdsDetail
          TabOrder = 1
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object dbcCl: TevDBCheckBox
          Left = 120
          Top = 24
          Width = 57
          Height = 17
          Caption = 'Client'
          DataField = 'CL_COPY'
          DataSource = wwdsDetail
          TabOrder = 2
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
      end
      object evDBComboBox1: TevDBComboBox
        Left = 208
        Top = 96
        Width = 137
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'MEDIA_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object dedtReport_Name: TevDBEdit
        Left = 12
        Top = 30
        Width = 333
        Height = 21
        HelpContext = 54001
        DataField = 'NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_REPORTS_GROUP.SY_REPORTS_GROUP
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_REPORTS_GROUP.SY_REPORTS_GROUP
    MasterDataSource = nil
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 332
    Top = 29
  end
  object dsSYReports: TevDataSource
    DataSet = DM_SY_REPORTS.SY_REPORTS
    OnStateChange = dsSYReportsStateChange
    Left = 310
    Top = 186
  end
end
