// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_QUEUE_PRIORITY;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFrameEntry, Db, Wwdatsrc,  SDataStructure, Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb, SDDClasses,
  SDataDictsystem, ISBasicClasses, SFieldCodeValues, isBasicUtils, EvConsts, EvLegacy, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_QUEUE_PRIORITY = class(TFrameEntry)
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evDBGrid1: TevDBGrid;
    cbTask: TevDBComboBox;
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

{ TEDIT_SY_QUEUE_PRIORITY }

procedure TEDIT_SY_QUEUE_PRIORITY.Activate;
begin
  inherited;
  cbTask.Items.Text := QueueTaskType_ComboChoices;
end;

function TEDIT_SY_QUEUE_PRIORITY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_QUEUE_PRIORITY;
end;

procedure TEDIT_SY_QUEUE_PRIORITY.wwdsListDataChange(Sender: TObject; Field: TField);
begin
  if (TaskTypeToMethodName(QUEUE_TASK_RUN_REPORT) = DM_SYSTEM_MISC.SY_QUEUE_PRIORITY.METHOD_NAME.AsString) or
     (TaskTypeToMethodName(QUEUE_TASK_PROCESS_TAX_PAYMENTS) = DM_SYSTEM_MISC.SY_QUEUE_PRIORITY.METHOD_NAME.AsString) or
     (TaskTypeToMethodName(QUEUE_TASK_PREPROCESS_QUARTER_END) = DM_SYSTEM_MISC.SY_QUEUE_PRIORITY.METHOD_NAME.AsString) or
     (TaskTypeToMethodName(QUEUE_TASK_QEC) = DM_SYSTEM_MISC.SY_QUEUE_PRIORITY.METHOD_NAME.AsString) or
     (TaskTypeToMethodName(QUEUE_TASK_REBUILD_TMP_TABLES) = DM_SYSTEM_MISC.SY_QUEUE_PRIORITY.METHOD_NAME.AsString)
  then
    evDBGrid1.ColumnByName('THREADS').ReadOnly := False
  else
  begin
    if (Field = DM_SYSTEM_MISC.SY_QUEUE_PRIORITY.METHOD_NAME) and (DM_SYSTEM_MISC.SY_QUEUE_PRIORITY.State in [dsEdit, dsInsert]) then
      DM_SYSTEM_MISC.SY_QUEUE_PRIORITY.THREADS.AsInteger := 0;
    evDBGrid1.ColumnByName('THREADS').ReadOnly := True;
  end;
end;

initialization
  RegisterClass(TEDIT_SY_QUEUE_PRIORITY);

end.
