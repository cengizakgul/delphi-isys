// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_LOCAL_TAX_CHART;

interface

uses
   SDataStructure, SPD_EDIT_BASE_SY_LOCAL, wwdbdatetimepicker,
  DBCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb, StdCtrls, wwdblook, Db,
  Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls,
  Classes, ISBasicClasses, SDDClasses, SDataDictsystem, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_LOCAL_TAX_CHART = class(TEDIT_BASE_SY_LOCAL)
    wwDBGrid3: TevDBGrid;
    Label3: TevLabel;
    dblkpLOCAL_MARITAL_STATUS: TevDBLookupCombo;
    Label12: TevLabel;
    wwDBComboBox1: TevDBComboBox;
    Label4: TevLabel;
    Label5: TevLabel;
    Label6: TevLabel;
    wwdeMinimum: TevDBEdit;
    wwdeMaximum: TevDBEdit;
    wwdePercent: TevDBEdit;
    evMaritalStatus: TevDataSource;
    CloneLocal: TevDataSource;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_LOCAL_TAX_CHART.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_LOCAL.SY_LOCAL_TAX_CHART;
end;

function TEDIT_SY_LOCAL_TAX_CHART.GetInsertControl: TWinControl;
begin
  Result := dblkpLOCAL_MARITAL_STATUS;
end;

procedure TEDIT_SY_LOCAL_TAX_CHART.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_LOCAL.SY_LOCAL_MARITAL_STATUS, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_LOCAL_TAX_CHART);

end.
