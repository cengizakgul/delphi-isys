// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_STATE_BASE;

interface

uses
   SFrameEntry, SDataStructure, DBCtrls, StdCtrls, Grids,
  Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, Classes, Db, Wwdatsrc,
  SDDClasses, ISBasicClasses, SDataDictsystem, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_STATE_BASE = class(TFrameEntry)
    PageControl1: TevPageControl; 
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TevPanel;
    Splitter1: TevSplitter;
    wwDBGrid1: TevDBGrid;
    Panel2: TevPanel;
    Label1: TevLabel;
    DBText2: TevDBText;
    DBText1: TevDBText;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

{ TEDIT_SY_STATE_BASE }

procedure TEDIT_SY_STATE_BASE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATES, SupportDataSets, '');
end;

end.
