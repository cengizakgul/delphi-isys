inherited EDIT_SY_STATE_BASE: TEDIT_SY_STATE_BASE
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 33
    Width = 443
    Height = 237
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 435
        Height = 209
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 217
          Top = 0
          Height = 209
        end
        object wwDBGrid1: TevDBGrid
          Left = 0
          Top = 0
          Width = 217
          Height = 209
          Selected.Strings = (
            'STATE'#9'8'#9'State'#9
            'NAME'#9'20'#9'Name'#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
          IniAttributes.SectionName = 'TEDIT_SY_STATE_BASE\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsMaster
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
    end
  end
  object Panel2: TevPanel [1]
    Left = 0
    Top = 0
    Width = 443
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TevLabel
      Left = 8
      Top = 8
      Width = 25
      Height = 13
      Caption = 'State'
    end
    object DBText2: TevDBText
      Left = 76
      Top = 8
      Width = 93
      Height = 17
      DataField = 'NAME'
      DataSource = wwdsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText1: TevDBText
      Left = 44
      Top = 8
      Width = 29
      Height = 17
      DataField = 'STATE'
      DataSource = wwdsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_STATES.SY_STATES
  end
  inherited wwdsList: TevDataSource
    Left = 98
    Top = 42
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 273
    Top = 18
  end
end
