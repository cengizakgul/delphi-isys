// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_TipOfTheDay_Maintenance;

interface

uses
  Windows, Messages, SFrameEntry, Classes, Db, Wwdatsrc, 
  Controls, Forms, StdCtrls, Spin, 
  Printers, SysUtils, EvUtils, EvSendMail, SDataStructure, DBCtrls,
  SDDClasses, SDataDictsystem, ISBasicClasses, EvContext, EvUIComponents;

type
  TTipOfTheDayFrame = class(TFrameEntry)
    EvDBMemo1: TEvDBMemo;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
  public
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.DFM}

{ TSettingsFrame }

procedure TTipOfTheDayFrame.Activate;
begin
  inherited;
  DM_SYSTEM_MISC.SY_STORAGE.DataRequired('TAG=''TIP_OF_THE_DAY''');
end;

procedure TTipOfTheDayFrame.Deactivate;
begin
  inherited;
  if DM_SYSTEM_MISC.SY_STORAGE.State = dsEdit then
    DM_SYSTEM_MISC.SY_STORAGE.Post;
  ctx_DataAccess.PostDataSets([DM_SYSTEM_MISC.SY_STORAGE]);
end;

initialization
  RegisterCLass(TTipOfTheDayFrame);

end.
