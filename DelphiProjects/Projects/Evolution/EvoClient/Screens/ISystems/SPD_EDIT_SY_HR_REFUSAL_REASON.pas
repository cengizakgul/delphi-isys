unit SPD_EDIT_SY_HR_REFUSAL_REASON;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_BASE_SY_HR, Db, Wwdatsrc,  StdCtrls, Mask,
  wwdbedit, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, SDataStructure,
  SDDClasses, SDataDictsystem, ISBasicClasses, wwdblook, Wwdotdot, Wwdbcomb, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_HR_REFUSAL_REASON = class(TEDIT_BASE_SY_HR)
    edHealtcareCoverage: TevDBComboBox;
    edStates: TevDBLookupCombo;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    cbHealthCareGrid: TevDBComboBox;
  private
    { Private declarations }
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  end;

implementation

{$R *.DFM}

{ TEDIT_SY_HR_REFUSAL_REASON }

function TEDIT_SY_HR_REFUSAL_REASON.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_HR.SY_HR_REFUSAL_REASON;

end;

procedure TEDIT_SY_HR_REFUSAL_REASON.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_HR.SY_STATES, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_HR_REFUSAL_REASON);

end.
