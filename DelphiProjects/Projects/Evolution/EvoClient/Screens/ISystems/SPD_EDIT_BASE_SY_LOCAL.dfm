inherited EDIT_BASE_SY_LOCAL: TEDIT_BASE_SY_LOCAL
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 34
    Width = 443
    Height = 236
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 531
        Height = 218
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel1'
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 121
          Top = 0
          Height = 218
        end
        object wwDBGrid1: TevDBGrid
          Left = 0
          Top = 0
          Width = 121
          Height = 218
          DisableThemesInTitle = False
          Selected.Strings = (
            'STATE'#9'2'#9'State'#9
            'NAME'#9'20'#9'Name'#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
          IniAttributes.SectionName = 'TEDIT_BASE_SY_LOCAL\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsMaster
          MultiSelectOptions = [msoAutoUnselect]
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object Panel2: TevPanel
          Left = 124
          Top = 0
          Width = 407
          Height = 218
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Panel2'
          TabOrder = 1
          object splCounty: TevSplitter
            Left = 0
            Top = 0
            Height = 218
          end
          object Panel2a: TevPanel
            Left = 3
            Top = 0
            Width = 404
            Height = 218
            Align = alClient
            Alignment = taLeftJustify
            BevelOuter = bvNone
            Caption = 'Panel2a'
            TabOrder = 0
            object wwDBGridLocal: TevDBGrid
              Left = 0
              Top = 0
              Width = 404
              Height = 218
              DisableThemesInTitle = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
              IniAttributes.SectionName = 'TEDIT_BASE_SY_LOCAL\wwDBGridLocal'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = wwdsSubMaster2
              MultiSelectOptions = [msoAutoUnselect]
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              PaintOptions.AlternatingRowColor = clCream
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Detail'
    end
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 0
    Width = 443
    Height = 34
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TevLabel
      Left = 8
      Top = 7
      Width = 25
      Height = 13
      Caption = 'State'
    end
    object DBText2: TevDBText
      Left = 44
      Top = 7
      Width = 133
      Height = 17
      DataField = 'NAME'
      DataSource = wwdsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblCounty: TevLabel
      Left = 184
      Top = 7
      Width = 33
      Height = 13
      Caption = 'County'
    end
    object dbtxtCounty: TevDBText
      Left = 232
      Top = 7
      Width = 153
      Height = 17
      DataField = 'CountyName'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TevLabel
      Left = 400
      Top = 7
      Width = 26
      Height = 13
      Caption = 'Local'
    end
    object DBText3: TevDBText
      Left = 440
      Top = 7
      Width = 169
      Height = 17
      DataField = 'NAME'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_STATES.SY_STATES
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = wwdsSubMaster2
    Top = 26
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 303
    Top = 26
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 251
    Top = 51
  end
  object wwdsSubMaster2: TevDataSource
    DataSet = DM_SY_LOCALS.SY_LOCALS
    MasterDataSource = wwdsMaster
    Left = 206
    Top = 54
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_SY_COUNTY.SY_COUNTY
    MasterDataSource = wwdsSubMaster2
    Left = 155
    Top = 54
  end
end
