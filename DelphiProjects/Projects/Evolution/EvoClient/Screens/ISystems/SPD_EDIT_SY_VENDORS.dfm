inherited EDIT_SY_VENDORS: TEDIT_SY_VENDORS
  object lblName: TevLabel [0]
    Left = 20
    Top = 272
    Width = 74
    Height = 16
    Caption = '~Vendor Name'
  end
  object evLabel2: TevLabel [1]
    Left = 20
    Top = 319
    Width = 54
    Height = 16
    Caption = '~Category '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TevLabel [2]
    Left = 20
    Top = 366
    Width = 36
    Height = 16
    Caption = '~Type '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object wwDBGrid1: TevDBGrid [3]
    Left = 0
    Top = 0
    Width = 443
    Height = 253
    DisableThemesInTitle = False
    Selected.Strings = (
      'VENDOR_NAME'#9'80'#9'Vendor name'#9#9)
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_VENDORS\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alTop
    DataSource = wwdsDetail
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object edtName: TevDBEdit [4]
    Left = 20
    Top = 288
    Width = 365
    Height = 21
    HelpContext = 5524
    DataField = 'VENDOR_NAME'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    TabOrder = 1
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object lcbCategories: TevDBLookupCombo [5]
    Left = 20
    Top = 335
    Width = 261
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'DESCRIPTION'#9'30'#9'DESCRIPTION'#9'F')
    DataField = 'SY_VENDOR_CATEGORIES_NBR'
    DataSource = wwdsDetail
    LookupTable = DM_SY_VENDOR_CATEGORIES.SY_VENDOR_CATEGORIES
    LookupField = 'SY_VENDOR_CATEGORIES_NBR'
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = True
  end
  object cbType: TevDBComboBox [6]
    Left = 20
    Top = 386
    Width = 169
    Height = 21
    HelpContext = 54501
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DataField = 'VENDOR_TYPE'
    DataSource = wwdsDetail
    DropDownCount = 8
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 3
    UnboundDataType = wwDefault
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_VENDORS.SY_VENDORS
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 304
    Top = 32
  end
end
