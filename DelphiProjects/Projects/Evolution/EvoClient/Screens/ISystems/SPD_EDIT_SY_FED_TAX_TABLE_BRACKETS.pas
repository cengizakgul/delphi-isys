// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_FED_TAX_TABLE_BRACKETS;

interface

uses
  SFrameEntry,  wwdbdatetimepicker, StdCtrls, Mask, DBCtrls,
  ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid, Classes, Db, Wwdatsrc,
  SDataStructure, wwdbedit, SDDClasses, SDataDictsystem, ISBasicClasses, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit;

type
  TEDIT_SY_FED_TAX_TABLE_BRACKETS = class(TFrameEntry)
    wwDBGrid1: TevDBGrid;
    lablGreater_Than_Value: TevLabel;
    lablLess_Than_Value: TevLabel;
    lablPercentage: TevLabel;
    drgpMarital_Status: TevDBRadioGroup;
    wwdeGreater_Than_Value: TevDBEdit;
    wwdeLess_Than_Value: TevDBEdit;
    wwdpPercentage: TevDBEdit;
    DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_FED_TAX_TABLE_BRACKETS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE_BRACKETS;
end;

function TEDIT_SY_FED_TAX_TABLE_BRACKETS.GetInsertControl: TWinControl;
begin
  Result := drgpMarital_Status;
end;

initialization
  RegisterClass(TEDIT_SY_FED_TAX_TABLE_BRACKETS);

end.
