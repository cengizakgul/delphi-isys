inherited EDIT_SY_STATE_DEPOSIT_FREQ: TEDIT_SY_STATE_DEPOSIT_FREQ
  Width = 863
  Height = 598
  HelpContext = 2
  inherited PageControl1: TevPageControl
    Width = 863
    Height = 565
    ActivePage = TabSheet2
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 855
        Height = 537
        inherited Splitter1: TevSplitter
          Height = 537
        end
        inherited wwDBGrid1: TevDBGrid
          Height = 537
          IniAttributes.SectionName = 'TEDIT_SY_STATE_DEPOSIT_FREQ\wwDBGrid1'
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Details'
      object Label6: TevLabel
        Left = 208
        Top = 140
        Width = 33
        Height = 16
        Caption = '~Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TevLabel
        Left = 360
        Top = 140
        Width = 88
        Height = 16
        Caption = '~When Due Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TevLabel
        Left = 8
        Top = 188
        Width = 111
        Height = 16
        Caption = '~First Threshold Period'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TevLabel
        Left = 360
        Top = 188
        Width = 122
        Height = 13
        Caption = 'First Threshold Frequency'
      end
      object Label8: TevLabel
        Left = 554
        Top = 140
        Width = 103
        Height = 16
        Caption = '~Days for When Due'
        FocusControl = edDaysDue
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TevLabel
        Left = 210
        Top = 188
        Width = 117
        Height = 16
        Caption = '~First Threshold Amount'
        FocusControl = edFirstTAmount
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TevLabel
        Left = 8
        Top = 140
        Width = 115
        Height = 16
        Caption = '~Frequency Description'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TevLabel
        Left = 8
        Top = 230
        Width = 117
        Height = 13
        Caption = 'Tax Payment Type Code'
      end
      object evLabel2: TevLabel
        Left = 416
        Top = 326
        Width = 100
        Height = 13
        Caption = 'Tax payment coupon'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object evLabel8: TevLabel
        Left = 12
        Top = 387
        Width = 204
        Height = 13
        Caption = 'Virtual Report Name (Tax payment coupon)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object edDaysDue: TevDBEdit
        Left = 554
        Top = 156
        Width = 92
        Height = 21
        HelpContext = 5
        DataField = 'NUMBER_OF_DAYS_FOR_WHEN_DUE'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object edFirstTAmount: TevDBEdit
        Left = 210
        Top = 204
        Width = 92
        Height = 21
        HelpContext = 8
        DataField = 'THRESHOLD_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwDBGrid2: TevDBGrid
        Left = 8
        Top = 12
        Width = 657
        Height = 117
        DisableThemesInTitle = False
        Selected.Strings = (
          'DESCRIPTION'#9'20'#9'Description'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_STATE_DEPOSIT_FREQ\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object rgStatusChange: TevDBRadioGroup
        Left = 418
        Top = 278
        Width = 219
        Height = 41
        HelpContext = 9
        Caption = '~Change Status When Threshold Met'
        Columns = 2
        DataField = 'CHANGE_STATUS_ON_THRESHOLD'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 9
        Values.Strings = (
          'Y'
          'N')
      end
      object rgThirdMonthDue: TevDBRadioGroup
        Left = 8
        Top = 278
        Width = 200
        Height = 41
        HelpContext = 10
        Caption = '3rd Month Due End of Following Month'
        Columns = 2
        DataField = 'THRD_MNTH_DUE_END_OF_NEXT_MNTH'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 10
        Values.Strings = (
          'Y'
          'N')
      end
      object dbedtDescription: TevDBEdit
        Left = 8
        Top = 156
        Width = 185
        Height = 21
        HelpContext = 12
        DataField = 'DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object cbType: TevDBComboBox
        Left = 208
        Top = 156
        Width = 137
        Height = 21
        HelpContext = 3
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'FREQUENCY_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object cbDueType: TevDBComboBox
        Left = 360
        Top = 156
        Width = 185
        Height = 21
        HelpContext = 4
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'WHEN_DUE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object cbFirstTPeriod: TevDBComboBox
        Left = 8
        Top = 204
        Width = 185
        Height = 21
        HelpContext = 7
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'THRESHOLD_PERIOD'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object luFirstTFreq: TevDBLookupCombo
        Left = 360
        Top = 204
        Width = 185
        Height = 21
        HelpContext = 13
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'20'#9'DESCRIPTION')
        DataField = 'THRESHOLD_DEP_FREQUENCY_NUMBER'
        DataSource = wwdsDetail
        LookupTable = evLookupDS
        LookupField = 'SY_STATE_DEPOSIT_FREQ_NBR'
        Style = csDropDownList
        TabOrder = 7
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object rgIncrementTypeCode: TevDBRadioGroup
        Left = 216
        Top = 278
        Width = 188
        Height = 41
        HelpContext = 1553
        Caption = 'Increment Tax Payment Type Code'
        Columns = 2
        DataField = 'INC_TAX_PAYMENT_CODE'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 11
        Values.Strings = (
          'Y'
          'N')
      end
      object edTypeCode: TevDBEdit
        Left = 8
        Top = 246
        Width = 92
        Height = 21
        HelpContext = 1549
        DataField = 'TAX_PAYMENT_TYPE_CODE'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object luFieldOffice: TevDBLookupCombo
        Left = 8
        Top = 346
        Width = 398
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'30'#9'AGENCY_NAME'#9'F'
          'ADDITIONAL_NAME'#9'30'#9'ADDITIONAL_NAME'#9'F')
        DataField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GL_AGENCY_FIELD_OFFICE.SY_GL_AGENCY_FIELD_OFFICE
        LookupField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        Style = csDropDownList
        TabOrder = 12
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object luTaxCoupon: TevDBLookupCombo
        Left = 416
        Top = 346
        Width = 219
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'30'#9'DESCRIPTION'#9'F')
        DataField = 'TAX_COUPON_SY_REPORTS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS.SY_REPORTS
        LookupField = 'SY_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 13
        Visible = False
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object cbSystemReportsGroup: TevDBLookupCombo
        Left = 252
        Top = 383
        Width = 256
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'60'#9'Name'#9#9)
        DataField = 'SY_REPORTS_GROUP_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS_GROUP.SY_REPORTS_GROUP
        LookupField = 'SY_REPORTS_GROUP_NBR'
        Style = csDropDownList
        TabOrder = 14
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  inherited Panel2: TevPanel
    Width = 863
    inherited DBText2: TevDBText
      Left = 84
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_STATE_DEPOSIT_FREQ.SY_STATE_DEPOSIT_FREQ
  end
  object evLookupDS: TevClientDataSet
    ClientID = -1
    Left = 348
    Top = 18
  end
  object evLookupDSrc: TevDataSource
    DataSet = evLookupDS
    MasterDataSource = wwdsMaster
    Left = 402
    Top = 18
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 456
    Top = 16
  end
end
