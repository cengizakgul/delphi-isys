inherited EDIT_SY_STATE_EXEMPTIONS: TEDIT_SY_STATE_EXEMPTIONS
  Width = 586
  Height = 414
  inherited PageControl1: TevPageControl
    Width = 586
    Height = 381
    ActivePage = TabSheet2
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 578
        Height = 353
        inherited Splitter1: TevSplitter
          Height = 353
        end
        inherited wwDBGrid1: TevDBGrid
          Height = 353
          Selected.Strings = (
            'STATE'#9'2'#9'State'#9
            'NAME'#9'20'#9'Name'#9)
          IniAttributes.SectionName = 'TEDIT_SY_STATE_EXEMPTIONS\wwDBGrid1'
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Details'
      object Label4: TevLabel
        Left = 16
        Top = 236
        Width = 99
        Height = 13
        Caption = '~E/D Code Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evDBText1: TevDBText
        Left = 77
        Top = 258
        Width = 124
        Height = 17
        DataField = 'EDDescription'
        DataSource = wwdsDetail
      end
      object wwDBGrid2: TevDBGrid
        Left = 12
        Top = 12
        Width = 573
        Height = 213
        Selected.Strings = (
          'E_D_CODE_TYPE'#9'2'#9'E D Code Type'#9
          'EDDescription'#9'40'#9'Description'#9'F'
          'EXEMPT_STATE'#9'1'#9'Exempt State'#9
          'EXEMPT_EMPLOYEE_SDI'#9'1'#9'Exempt Employee Sdi'#9
          'EXEMPT_EMPLOYER_SDI'#9'1'#9'Exempt Employer Sdi'#9
          'EXEMPT_EMPLOYEE_SUI'#9'1'#9'Exempt Employee Sui'#9
          'EXEMPT_EMPLOYER_SUI'#9'1'#9'Exempt Employer Sui'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_STATE_EXEMPTIONS\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object DBRadioGroup1: TevDBRadioGroup
        Left = 16
        Top = 296
        Width = 202
        Height = 76
        HelpContext = 3003
        Caption = '~State'
        DataField = 'EXEMPT_STATE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Block'
          'Include')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object DBRadioGroup2: TevDBRadioGroup
        Left = 424
        Top = 240
        Width = 162
        Height = 61
        HelpContext = 3007
        Caption = '~Exempt ER SUI'
        DataField = 'EXEMPT_EMPLOYER_SUI'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 5
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup3: TevDBRadioGroup
        Left = 240
        Top = 312
        Width = 162
        Height = 61
        HelpContext = 3004
        Caption = '~Exempt EE SDI'
        DataField = 'EXEMPT_EMPLOYEE_SDI'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 4
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup4: TevDBRadioGroup
        Left = 424
        Top = 312
        Width = 162
        Height = 61
        HelpContext = 3005
        Caption = '~Exempt ER SDI'
        DataField = 'EXEMPT_EMPLOYER_SDI'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 6
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup5: TevDBRadioGroup
        Left = 240
        Top = 240
        Width = 162
        Height = 61
        HelpContext = 3006
        Caption = '~Exempt EE SUI'
        DataField = 'EXEMPT_EMPLOYEE_SUI'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'Y'
          'N')
      end
      object wwDBComboDlgEDCodeType: TevDBComboDlg
        Left = 16
        Top = 256
        Width = 57
        Height = 21
        HelpContext = 3002
        ShowButton = True
        Style = csDropDown
        DataField = 'E_D_CODE_TYPE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = 
          '[DI,DH,DG,DA,DB,DC,DD,DE,DF,D2,D3,DL,DM,DW,DK,D7,DV,ET,EU,EV,EW,' +
          'EX,EY,E2,E9,EO,EP,EQ,M1,M2,M3,M4,M5,M6,M7,ER,ES,E4,E3,E5,E6,E1,E' +
          '8,E7,E0,D1,DN,DQ,DR,DS,DO,DT,DU,DZ,DX,DY,DJ,D4,EF,EG,EH,EI,EJ,EK' +
          ',EL,EM,EB,EA,EC,ED,EE,EZ]'
        Picture.AutoFill = False
        TabOrder = 1
        WordWrap = False
        UnboundDataType = wwDefault
      end
    end
  end
  inherited Panel2: TevPanel
    Width = 586
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_STATE_EXEMPTIONS.SY_STATE_EXEMPTIONS
  end
end
