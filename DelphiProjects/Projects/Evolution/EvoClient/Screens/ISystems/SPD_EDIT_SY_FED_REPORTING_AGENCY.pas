// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_FED_REPORTING_AGENCY;

interface

uses
  SFrameEntry,  SDataStructure, StdCtrls, wwdblook, Grids,
  Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, Classes, Db, Wwdatsrc, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_FED_REPORTING_AGENCY = class(TFrameEntry)
    PageControl1: TevPageControl;
    Browse: TTabSheet;
    Address: TTabSheet;
    lablAgency_Name: TevLabel;
    Panel1: TevPanel;
    wwDBGrid1: TevDBGrid;
    Splitter1: TevSplitter;
    wwlcAgency_Name: TevDBLookupCombo;
    DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_FED_REPORTING_AGENCY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_FEDERAL.SY_FED_REPORTING_AGENCY;
end;

function TEDIT_SY_FED_REPORTING_AGENCY.GetInsertControl: TWinControl;
begin
  Result := wwlcAgency_Name;
end;

procedure TEDIT_SY_FED_REPORTING_AGENCY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_FED_REPORTING_AGENCY);

end.
