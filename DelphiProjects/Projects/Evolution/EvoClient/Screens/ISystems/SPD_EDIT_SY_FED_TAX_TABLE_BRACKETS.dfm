inherited EDIT_SY_FED_TAX_TABLE_BRACKETS: TEDIT_SY_FED_TAX_TABLE_BRACKETS
  Width = 743
  Height = 595
  object lablGreater_Than_Value: TevLabel [0]
    Left = 220
    Top = 381
    Width = 102
    Height = 16
    Caption = '~Greater Than Value'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablLess_Than_Value: TevLabel [1]
    Left = 220
    Top = 405
    Width = 89
    Height = 16
    Caption = '~Less Than Value'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablPercentage: TevLabel [2]
    Left = 220
    Top = 429
    Width = 64
    Height = 16
    Caption = '~Percentage'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object wwDBGrid1: TevDBGrid [3]
    Left = 0
    Top = 0
    Width = 743
    Height = 364
    DisableThemesInTitle = False
    Selected.Strings = (
      'MARITAL_STATUS'#9'1'#9'Marital Status'
      'GREATER_THAN_VALUE'#9'10'#9'Greater Than Value'#9'F'
      'LESS_THAN_VALUE'#9'10'#9'Less Than Value'
      'PERCENTAGE'#9'10'#9'Percentage')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_FED_TAX_TABLE_BRACKETS\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alTop
    DataSource = wwdsDetail
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = 14544093
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object drgpMarital_Status: TevDBRadioGroup [4]
    Left = 13
    Top = 384
    Width = 177
    Height = 45
    HelpContext = 6025
    Caption = '~Marital Status'
    Columns = 2
    DataField = 'MARITAL_STATUS'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      'Single'
      'Married')
    ParentFont = False
    TabOrder = 1
    Values.Strings = (
      'S'
      'M')
  end
  object wwdeGreater_Than_Value: TevDBEdit [5]
    Left = 340
    Top = 379
    Width = 64
    Height = 21
    HelpContext = 6025
    DataField = 'GREATER_THAN_VALUE'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 2
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeLess_Than_Value: TevDBEdit [6]
    Left = 340
    Top = 403
    Width = 64
    Height = 21
    HelpContext = 6025
    DataField = 'LESS_THAN_VALUE'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 3
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdpPercentage: TevDBEdit [7]
    Left = 340
    Top = 427
    Width = 64
    Height = 21
    HelpContext = 6025
    DataField = 'PERCENTAGE'
    DataSource = wwdsDetail
    TabOrder = 4
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_FED_TAX_TABLE_BRACKETS.SY_FED_TAX_TABLE_BRACKETS
  end
  object DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL
    Left = 261
    Top = 18
  end
end
