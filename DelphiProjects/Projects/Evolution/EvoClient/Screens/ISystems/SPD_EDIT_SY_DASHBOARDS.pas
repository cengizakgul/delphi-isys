// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_DASHBOARDS;

interface

uses
  SFrameEntry,  SDataStructure, DBCtrls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet, SDDClasses,
  SDataDictsystem, isUIwwDBEdit, ISBasicClasses, wwdblook,
  isUIwwDBLookupCombo, EvUtils, Wwdbcomb, isUIwwDBComboBox, isUIDBMemo,
  EvCommonInterfaces, EvDataset, EvExceptions, SysUtils;

type
  TEDIT_SY_DASHBOARDS = class(TFrameEntry)
    wwDBGrid1: TevDBGrid;
    edtDashboardID: TevDBEdit;
    lblName: TevLabel;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    lcbCategories: TevDBLookupCombo;
    evLabel2: TevLabel;
    cbType: TevDBComboBox;
    Label14: TevLabel;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    evLabel3: TevLabel;
    edNotes: TEvDBMemo;
    rbReleased: TevDBRadioGroup;
    procedure wwdsDetailStateChange(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;

    { Public declarations }
  end;

implementation

uses
  SPackageEntry;

{$R *.DFM}

function TEDIT_SY_DASHBOARDS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_DASHBOARDS;
end;

procedure TEDIT_SY_DASHBOARDS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_ANALYTICS_TIER, SupportDataSets, 'ALL');
end;

procedure TEDIT_SY_DASHBOARDS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);

  function CheckSyDashboardInUSe:boolean;
  var
     s: string;
     Q: IEvQuery;
  begin
    s:= 'select count(SB_ENABLED_DASHBOARDS_NBR) from SB_ENABLED_DASHBOARDS ' +
            'where {AsOfNow<SB_ENABLED_DASHBOARDS>} and  DASHBOARD_LEVEL = ''S'' and DASHBOARD_NBR = ' +
               IntToStr(wwdsDetail.DataSet.FieldByName('SY_DASHBOARDS_NBR').AsInteger);;
    Q := TevQuery.Create(s);
    Q.Execute;
    result := Q.Result.Fields[0].AsInteger > 0;
  end;

  function CheckSyDashboardID:boolean;
  var
     s: string;
     Q: IEvQuery;
  begin
    s:= 'select count(SY_DASHBOARDS_NBR) from SY_DASHBOARDS where {AsOfNow<SY_DASHBOARDS>} and ' +
           '     SY_DASHBOARDS_NBR <> :Nbr and DASHBOARD_ID = :DashboardID ' ;
    Q := TevQuery.Create(s);
    Q.Params.AddValue('Nbr',wwdsDetail.DataSet.FieldByName('SY_DASHBOARDS_NBR').AsInteger);
    Q.Params.AddValue('DashboardID', wwdsDetail.DataSet.FieldByName('DASHBOARD_ID').AsInteger);
   Q.Execute;
   result := Q.Result.Fields[0].AsInteger > 0;
  end;

  function CheckSyDashboardDescription:boolean;
  var
     s: string;
     Q: IEvQuery;
  begin
    s:= 'select count(SY_DASHBOARDS_NBR) from SY_DASHBOARDS where {AsOfNow<SY_DASHBOARDS>} and ' +
           '     SY_DASHBOARDS_NBR <> :Nbr and DESCRIPTION = :Description ';
    Q := TevQuery.Create(s);
    Q.Params.AddValue('Nbr',wwdsDetail.DataSet.FieldByName('SY_DASHBOARDS_NBR').AsInteger);
    Q.Params.AddValue('Description', wwdsDetail.DataSet.FieldByName('DESCRIPTION').AsString);
   Q.Execute;
   result := Q.Result.Fields[0].AsInteger > 0;
  end;

begin
  inherited;

  if Kind = NavOK then
  begin
    wwdsDetail.DataSet.UpdateRecord;
    if CheckSyDashboardID then
       raise EUpdateError.CreateHelp('Dashboard ID already exists.', IDH_ConsistencyViolation);

    if CheckSyDashboardDescription then
       raise EUpdateError.CreateHelp('Dashboard Description already exists.', IDH_ConsistencyViolation);
  end;

  if Kind = NavDelete then
  begin
    if CheckSyDashboardInUSe then
       raise EUpdateError.CreateHelp('Please delete detail records from SB level.', IDH_ConsistencyViolation);
  end;

end;

function TEDIT_SY_DASHBOARDS.GetInsertControl: TWinControl;
begin
  Result := edtDashboardID;
end;

procedure TEDIT_SY_DASHBOARDS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  wwDBGrid1.Enabled := not (wwdsDetail.DataSet.State in [dsInsert,dsEdit]);
end;

initialization
  RegisterClass(TEDIT_SY_DASHBOARDS);

end.
