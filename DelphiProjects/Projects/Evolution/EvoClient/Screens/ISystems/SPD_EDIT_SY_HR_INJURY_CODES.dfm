inherited EDIT_SY_HR_INJURY_CODES: TEDIT_SY_HR_INJURY_CODES
  Width = 581
  Height = 269
  inherited pnlBrowse: TevPanel
    Width = 581
    inherited dgBrowse: TevDBGrid
      Width = 581
    end
  end
  inherited pnlDBControls: TevPanel
    Width = 581
    Height = 76
    object evLabel2: TevLabel [1]
      Left = 24
      Top = 48
      Width = 53
      Height = 13
      Caption = 'Injury Code'
    end
    inherited dedtDescription: TevDBEdit
      Left = 88
    end
    object evDBEdit1: TevDBEdit
      Left = 88
      Top = 40
      Width = 121
      Height = 21
      DataField = 'INJURY_CODE'
      DataSource = wwdsDetail
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
  end
end
