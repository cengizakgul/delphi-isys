inherited EDIT_SY_DASHBOARDS: TEDIT_SY_DASHBOARDS
  Width = 614
  Height = 620
  object lblName: TevLabel [0]
    Left = 12
    Top = 272
    Width = 69
    Height = 13
    Caption = 'Dashboard ID '
  end
  object evLabel2: TevLabel [1]
    Left = 12
    Top = 316
    Width = 21
    Height = 13
    Caption = 'Tier '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TevLabel [2]
    Left = 140
    Top = 316
    Width = 27
    Height = 13
    Caption = 'Type '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel1: TevLabel [3]
    Left = 140
    Top = 272
    Width = 56
    Height = 13
    Caption = 'Description '
  end
  object evLabel3: TevLabel [4]
    Left = 12
    Top = 363
    Width = 31
    Height = 13
    Caption = 'Notes '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object wwDBGrid1: TevDBGrid [5]
    Left = 0
    Top = 0
    Width = 614
    Height = 253
    DisableThemesInTitle = False
    Selected.Strings = (
      'DASHBOARD_ID'#9'10'#9'Dashboard id'#9'F'
      'DESCRIPTION'#9'80'#9'Description'#9'F'
      'DASHBOARD_TYPE'#9'1'#9'Dashboard type'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_DASHBOARDS\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alTop
    DataSource = wwdsDetail
    TabOrder = 4
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object edtDashboardID: TevDBEdit [6]
    Left = 12
    Top = 288
    Width = 121
    Height = 21
    HelpContext = 5524
    DataField = 'DASHBOARD_ID'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    TabOrder = 0
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object lcbCategories: TevDBLookupCombo [7]
    Left = 12
    Top = 332
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'TIER_ID'#9'40'#9'Tier id'#9'F')
    DataField = 'SY_ANALYTICS_TIER_NBR'
    DataSource = wwdsDetail
    LookupTable = DM_SY_ANALYTICS_TIER.SY_ANALYTICS_TIER
    LookupField = 'SY_ANALYTICS_TIER_NBR'
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = True
  end
  object cbType: TevDBComboBox [8]
    Left = 140
    Top = 332
    Width = 250
    Height = 21
    HelpContext = 54501
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DataField = 'DASHBOARD_TYPE'
    DataSource = wwdsDetail
    DropDownCount = 8
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 3
    UnboundDataType = wwDefault
  end
  object evDBEdit1: TevDBEdit [9]
    Left = 140
    Top = 288
    Width = 312
    Height = 21
    HelpContext = 5524
    DataField = 'DESCRIPTION'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    TabOrder = 1
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object edNotes: TEvDBMemo [10]
    Left = 12
    Top = 376
    Width = 377
    Height = 135
    HelpContext = 10515
    DataField = 'NOTES'
    DataSource = wwdsDetail
    ScrollBars = ssVertical
    TabOrder = 5
  end
  object rbReleased: TevDBRadioGroup [11]
    Left = 12
    Top = 523
    Width = 161
    Height = 37
    HelpContext = 1572
    Caption = 'Released  '
    Columns = 2
    DataField = 'RELEASED'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      'Yes'
      'No')
    ParentFont = False
    TabOrder = 6
    Values.Strings = (
      'Y'
      'N')
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_DASHBOARDS.SY_DASHBOARDS
    OnStateChange = wwdsDetailStateChange
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 304
    Top = 32
  end
end
