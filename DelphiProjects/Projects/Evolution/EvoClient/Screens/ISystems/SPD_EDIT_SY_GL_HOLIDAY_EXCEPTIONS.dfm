inherited EDIT_SY_GL_HOLIDAY_EXCEPTIONS: TEDIT_SY_GL_HOLIDAY_EXCEPTIONS
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 18
    Width = 628
    Height = 442
    HelpContext = 53501
    ActivePage = tsBrowse
    TabOrder = 0
    object tsBrowse: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 617
        Height = 410
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 354
          Top = 1
          Width = 3
          Height = 408
          Cursor = crHSplit
        end
        object wwDBGrid1: TevDBGrid
          Left = 1
          Top = 1
          Width = 353
          Height = 408
          Selected.Strings = (
            'AGENCY_NAME'#9'40'#9'Agency'
            'COUNTY'#9'40'#9'Locality'
            'STATE'#9'2'#9'State')
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsMaster
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          IndicatorColor = icBlack
        end
        object Panel2: TevPanel
          Left = 357
          Top = 1
          Width = 259
          Height = 408
          Align = alClient
          TabOrder = 1
          object wwDBGrid2: TevDBGrid
            Left = 1
            Top = 1
            Width = 257
            Height = 406
            Selected.Strings = (
              'FEDERAL_HOLIDAY_CODE'#9'15'#9'FEDERAL_HOLIDAY_CODE')
            IniAttributes.Delimiter = ';;'
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            IndicatorColor = icBlack
          end
        end
      end
    end
    object tsDetail: TTabSheet
      Caption = 'Detail'
      object Label1: TevLabel
        Left = 16
        Top = 16
        Width = 92
        Height = 13
        AutoSize = False
        Caption = '~Federal holiday'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TevLabel
        Left = 16
        Top = 64
        Width = 73
        Height = 13
        AutoSize = False
        Caption = '~Holiday rule'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object edRule: TevDBComboBox
        Left = 16
        Top = 80
        Width = 217
        Height = 21
        ShowButton = True
        Style = csDropDown
        MapList = False
        AllowClearKey = False
        DataField = 'HOLIDAY_RULE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object edFedCode: TevDBComboBox
        Left = 16
        Top = 32
        Width = 217
        Height = 21
        ShowButton = True
        Style = csDropDown
        MapList = False
        AllowClearKey = False
        DataField = 'FEDERAL_HOLIDAY_CODE'
        DataSource = wwdsDetail
        DropDownCount = 15
        ItemHeight = 0
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_GL_AGENCY_HOLIDAY_EXPS.SY_GL_AGENCY_HOLIDAY_EXPS
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 267
    Top = 15
  end
end
