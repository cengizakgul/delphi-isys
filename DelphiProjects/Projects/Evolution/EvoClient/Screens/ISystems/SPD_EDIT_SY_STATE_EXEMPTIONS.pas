// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_STATE_EXEMPTIONS;

interface

uses
  SDataStructure,  SPD_EDIT_SY_STATE_BASE, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, DBCtrls, Db, Wwdatsrc, Grids, Wwdbigrd,
  Wwdbgrid, Controls, ComCtrls, Classes, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_STATE_EXEMPTIONS = class(TEDIT_SY_STATE_BASE)
    Label4: TevLabel;
    wwDBGrid2: TevDBGrid;
    DBRadioGroup1: TevDBRadioGroup;
    DBRadioGroup2: TevDBRadioGroup;
    DBRadioGroup3: TevDBRadioGroup;
    DBRadioGroup4: TevDBRadioGroup;
    DBRadioGroup5: TevDBRadioGroup;
    wwDBComboDlgEDCodeType: TevDBComboDlg;
    evDBText1: TevDBText;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_STATE_EXEMPTIONS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS;
end;

function TEDIT_SY_STATE_EXEMPTIONS.GetInsertControl: TWinControl;
begin
  Result := wwDBComboDlgEDCodeType;
end;

initialization
  RegisterClass(TEDIT_SY_STATE_EXEMPTIONS);

end.
