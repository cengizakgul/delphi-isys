inherited LocalChoiceQuery: TLocalChoiceQuery
  Left = 0
  Top = 108
  Width = 640
  Height = 392
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 324
    Width = 632
    inherited btnYes: TevBitBtn
      Left = 463
    end
    inherited btnNo: TevBitBtn
      Left = 551
    end
    inherited btnAll: TevBitBtn
      Left = 333
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 632
    Height = 324
    Selected.Strings = (
      'LocalState'#9'8'#9'Localstate'#9'F'
      'NAME'#9'40'#9'LOCALITY_NAME'#9'F'
      'LocalTypeDescription'#9'20'#9'Localtypedescription'#9'F'
      'CountyName'#9'40'#9'Countyname'#9'F')
    IniAttributes.SectionName = 'TLocalChoiceQuery\dgChoiceList'
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 216
    Top = 152
  end
end
