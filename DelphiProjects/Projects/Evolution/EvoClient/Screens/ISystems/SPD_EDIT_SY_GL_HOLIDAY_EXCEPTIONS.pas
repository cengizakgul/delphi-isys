// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_GL_HOLIDAY_EXCEPTIONS;

interface

uses
  SFrameEntry,  StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb,
  Grids, Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, Classes, Db,
  Wwdatsrc, SDataStructure, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_GL_HOLIDAY_EXCEPTIONS = class(TFrameEntry)
    PageControl1: TevPageControl;
    tsBrowse: TTabSheet;
    Panel1: TevPanel;
    Splitter1: TevSplitter;
    wwDBGrid1: TevDBGrid;
    Panel2: TevPanel;
    wwDBGrid2: TevDBGrid;
    tsDetail: TTabSheet;
    Label1: TevLabel;
    Label3: TevLabel;
    edRule: TevDBComboBox;
    edFedCode: TevDBComboBox;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_GL_HOLIDAY_EXCEPTIONS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_GL_AGENCY_HOLIDAY_EXPS;
end;

function TEDIT_SY_GL_HOLIDAY_EXCEPTIONS.GetInsertControl: TWinControl;
begin
  Result := edFedCode;
end;

procedure TEDIT_SY_GL_HOLIDAY_EXCEPTIONS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_GL_HOLIDAY_EXCEPTIONS);

end.
