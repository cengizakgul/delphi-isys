// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_FED_TAX_TABLE;

interface

uses
  SFrameEntry,  SDataStructure, wwdbdatetimepicker, Controls,
  StdCtrls, Mask, DBCtrls, Classes, Db, Wwdatsrc, SSecurityInterface, wwdbedit,
  SDDClasses, SDataDictsystem, ISBasicClasses, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBEdit;

type
  TEDIT_SY_FED_TAX_TABLE = class(TFrameEntry)
    lablExemption_Amount: TevLabel;
    lablFed_Minimum_Wage: TevLabel;
    lablFed_Tip_Credit: TevLabel;
    lablSupplemental_Tax_Percentage: TevLabel;
    lablOASDI_Rate: TevLabel;
    lablOASDI_Wage_Rate: TevLabel;
    lablMedicare_Rate: TevLabel;
    lablMedicare_Wage_Limit: TevLabel;
    lablFUI_Rate_Real: TevLabel;
    lablFUI_Rate_Credit: TevLabel;
    lablFUI_Wage_Limit: TevLabel;
    lablSY_401k_Limit: TevLabel;
    lablSY_403b_Limit: TevLabel;
    lablSY_457_Limit: TevLabel;
    lablSY_501c_Limit: TevLabel;
    lablSimple_Limit: TevLabel;
    lablSep_Limit: TevLabel;
    lablDeferred_Comp_Limit: TevLabel;
    lablFed_Deposit_Freq_Threshold: TevLabel;
    lablSecond_EIC_Amount: TevLabel;
    lablSecond_EIC_Limit: TevLabel;
    lablFirst_EIC_Percentage: TevLabel;
    lablThird_EIC_Additional_Percent: TevLabel;
    wwdeExemption_Amount: TevDBEdit;
    wwdeSecond_EIC_Amount: TevDBEdit;
    wwdeSecond_EIC_Limit: TevDBEdit;
    wwdeFirst_EIC_Percentage: TevDBEdit;
    wwdeThird_EIC_Additional_Percent: TevDBEdit;
    wwdeFed_Minimum_Wage: TevDBEdit;
    wwdeFederal_Tip_Credit: TevDBEdit;
    wwdeSupplemental_Tax_Percentage: TevDBEdit;
    wwdeOASDI_Rate: TevDBEdit;
    wwdeOASDI_Wage_Limit: TevDBEdit;
    wwdeMedicare_Rate: TevDBEdit;
    wwdeMedicare_Wage_Limit: TevDBEdit;
    wwdeFUI_Rate_Real: TevDBEdit;
    wwdeFUI_Rate_Credit: TevDBEdit;
    wwdeFUI_Wage_Limit: TevDBEdit;
    wwde401k_Limit: TevDBEdit;
    wwde403b_Limit: TevDBEdit;
    wwde457_Limit: TevDBEdit;
    wwde501c_Limit: TevDBEdit;
    wwdeSimple_Limit: TevDBEdit;
    wwdeSEP_Limit: TevDBEdit;
    wwdeDeferred_Comp_Limit: TevDBEdit;
    wwdeDeposit_Frequency_Threshold: TevDBEdit;
    lablFirst_EIC_Limit: TevLabel;
    wwdeFirst_EIC_Limit: TevDBEdit;
    DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    evLabel2: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel3: TevLabel;
    evDBEdit3: TevDBEdit;
    evLabel4: TevLabel;
    evDBEdit4: TevDBEdit;
    evLabel5: TevLabel;
    evDBEdit5: TevDBEdit;
    evLabel6: TevLabel;
    evDBEdit6: TevDBEdit;
    evLabel7: TevLabel;
    evDBEdit7: TevDBEdit;
    evLabel8: TevLabel;
    evDBEdit8: TevDBEdit;
    evLabel9: TevLabel;
    evDBEdit9: TevDBEdit;
    evDBEdit10: TevDBEdit;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    evDBEdit11: TevDBEdit;
    evLabel12: TevLabel;
    evDBEdit12: TevDBEdit;
    evLabel13: TevLabel;
    edPovertyLevel: TevDBEdit;
    lblAcaAfford: TevLabel;
    edtACAAfford: TevDBEdit;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_FED_TAX_TABLE.GetInsertControl: TWinControl;
begin
  Result := wwdeExemption_Amount;
end;

procedure TEDIT_SY_FED_TAX_TABLE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE, EditDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_FED_TAX_TABLE);

end.
