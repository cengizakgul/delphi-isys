// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_GL_AGENCY_FIELD_OFFICE;

interface

uses
  SFrameEntry,  StdCtrls, Mask, DBCtrls, Grids, Wwdbigrd, Wwdbgrid,
  ExtCtrls, Controls, ComCtrls, Classes, Db, Wwdatsrc, SDataStructure,
  wwdbedit, wwdblook, Wwdotdot, Wwdbcomb, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_GL_AGENCY_FIELD_OFFICE = class(TFrameEntry)
    PageControl1: TevPageControl;
    tsBrowse: TTabSheet;
    tsDetail: TTabSheet;
    lablAdditional_Name: TevLabel;
    dedtAdditional_Name: TevDBEdit;
    lablAddress1: TevLabel;
    dedtAddress1: TevDBEdit;
    lablAddress2: TevLabel;
    dedtAddress2: TevDBEdit;
    lablCity: TevLabel;
    dedtCity: TevDBEdit;
    lablZip_Code: TevLabel;
    lablState: TevLabel;
    dedtState: TevDBEdit;
    lablAttention_Name: TevLabel;
    dedtAttention_Name: TevDBEdit;
    lablFax: TevLabel;
    lablFax_Description: TevLabel;
    dedtFax_Description: TevDBEdit;
    Label16: TevLabel;
    DBText1: TevDBText;
    drgpPrimary_Contact: TevGroupBox;
    lablName1: TevLabel;
    lablPhone1: TevLabel;
    lablDescription1: TevLabel;
    dedtDescription1: TevDBEdit;
    gboxSecondary_Contact: TevGroupBox;
    lablContact2: TevLabel;
    dedtContact2: TevDBEdit;
    lablPhone2: TevLabel;
    lablDescription2: TevLabel;
    dedtDescription: TevDBEdit;
    Panel1: TevPanel;
    wwDBGrid1: TevDBGrid;
    Splitter1: TevSplitter;
    Panel2: TevPanel;
    wwDBGrid2: TevDBGrid;
    wwdeFax: TevDBEdit;
    wwdePhone1: TevDBEdit;
    dedtContact1: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    wwdePhone2: TevDBEdit;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evDBLookupCombo1: TevDBComboBox;
    evLabel1: TevLabel;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_GL_AGENCY_FIELD_OFFICE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE;
end;

function TEDIT_SY_GL_AGENCY_FIELD_OFFICE.GetInsertControl: TWinControl;
begin
  Result := dedtAdditional_Name;
end;

procedure TEDIT_SY_GL_AGENCY_FIELD_OFFICE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_GL_AGENCY_FIELD_OFFICE);

end.
