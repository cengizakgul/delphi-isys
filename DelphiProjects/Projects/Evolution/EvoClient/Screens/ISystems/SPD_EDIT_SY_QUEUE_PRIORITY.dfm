inherited EDIT_SY_QUEUE_PRIORITY: TEDIT_SY_QUEUE_PRIORITY
  object evDBGrid1: TevDBGrid [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    DisableThemesInTitle = False
    ControlType.Strings = (
      'PRIORITY;CustomEdit;;F'
      'METHOD_NAME;CustomEdit;cbTask;F')
    Selected.Strings = (
      'METHOD_NAME'#9'50'#9'Task Name'#9'F'
      'PRIORITY'#9'10'#9'Priority'#9'F'
      'THREADS'#9'10'#9'Max Threads'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_QUEUE_PRIORITY\evDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsList
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object cbTask: TevDBComboBox [1]
    Left = 75
    Top = 111
    Width = 379
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DataField = 'METHOD_NAME'
    DataSource = wwdsList
    DropDownCount = 15
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 1
    UnboundDataType = wwDefault
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_SY_QUEUE_PRIORITY.SY_QUEUE_PRIORITY
    OnDataChange = wwdsListDataChange
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 255
    Top = 18
  end
end
