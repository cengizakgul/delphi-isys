inherited EDIT_SY_SUI: TEDIT_SY_SUI
  Width = 832
  Height = 612
  inherited PageControl1: TevPageControl
    Width = 832
    Height = 579
    ActivePage = Detail
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 824
        Height = 551
        inherited Splitter1: TevSplitter
          Height = 551
        end
        inherited wwDBGrid1: TevDBGrid
          Height = 551
          Selected.Strings = (
            'STATE'#9'2'#9'State'#9
            'NAME'#9'20'#9'Name'#9)
          IniAttributes.SectionName = 'TEDIT_SY_SUI\wwDBGrid1'
        end
        object wwDBGrid3: TevDBGrid
          Left = 220
          Top = 0
          Width = 604
          Height = 551
          DisableThemesInTitle = False
          Selected.Strings = (
            'SUI_TAX_NAME'#9'40'#9'Sui Tax Name'#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_SY_SUI\wwDBGrid3'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = wwdsDetail
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'State SUI Flags'
      object Label14: TevLabel
        Left = 4
        Top = 8
        Width = 88
        Height = 16
        Caption = '~Reciprocate SUI'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel2: TevLabel
        Left = 4
        Top = 187
        Width = 100
        Height = 13
        Caption = 'Tax payment coupon'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel8: TevLabel
        Left = 292
        Top = 3
        Width = 204
        Height = 13
        Caption = 'Virtual Report Name (Tax payment coupon)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBRadioGroup8: TevDBRadioGroup
        Left = 4
        Top = 52
        Width = 189
        Height = 57
        HelpContext = 54501
        Caption = '~Print SUI Return if Zero'
        DataField = 'PRINT_SUI_RETURN_IF_ZERO'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup7: TevDBRadioGroup
        Left = 4
        Top = 120
        Width = 189
        Height = 57
        HelpContext = 54501
        Caption = '~Show S125 in Gross Wages'
        DataField = 'SHOW_S125_IN_GROSS_WAGES_SUI'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
      object wwDBComboBox4: TevDBComboBox
        Left = 4
        Top = 24
        Width = 169
        Height = 21
        HelpContext = 54501
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = True
        AutoDropDown = True
        DataField = 'SUI_RECIPROCATE'
        DataSource = wwdsMaster
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Yes'#9'Y'
          'No'#9'N'
          'Other'#9'O'
          'Only With Reciprocal States'#9'S')
        ItemIndex = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object luTaxCoupon: TevDBLookupCombo
        Left = 4
        Top = 203
        Width = 277
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'30'#9'DESCRIPTION'#9'F')
        DataField = 'TAX_COUPON_SY_REPORTS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS.SY_REPORTS
        LookupField = 'SY_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object cbSystemReportsGroup: TevDBLookupCombo
        Left = 292
        Top = 23
        Width = 256
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'60'#9'Name'#9#9)
        DataField = 'SY_REPORTS_GROUP_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS_GROUP.SY_REPORTS_GROUP
        LookupField = 'SY_REPORTS_GROUP_NBR'
        Style = csDropDownList
        TabOrder = 4
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
    object Detail: TTabSheet
      Caption = 'Details'
      object Label4: TevLabel
        Left = 8
        Top = 196
        Width = 79
        Height = 16
        Caption = '~SUI Tax Name'
        FocusControl = dbedtSUI_TAX_NAME
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TevLabel
        Left = 271
        Top = 234
        Width = 141
        Height = 16
        Caption = '~New Company Default Rate'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TevLabel
        Left = 8
        Top = 234
        Width = 85
        Height = 16
        Caption = '~Maximum Wage'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TevLabel
        Left = 8
        Top = 274
        Width = 99
        Height = 13
        Caption = 'Tax payment agency'
      end
      object Label3: TevLabel
        Left = 8
        Top = 314
        Width = 100
        Height = 13
        Caption = 'Tax reporting agency'
      end
      object evLabel1: TevLabel
        Left = 271
        Top = 274
        Width = 56
        Height = 13
        Caption = 'Global Rate'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablE_D_Code_Type: TevLabel
        Left = 271
        Top = 314
        Width = 52
        Height = 13
        Caption = 'Code Type'
        FocusControl = wwcdE_D_Code_Type
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel3: TevLabel
        Left = 271
        Top = 354
        Width = 116
        Height = 13
        Caption = 'Additional Assessment %'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel4: TevLabel
        Left = 271
        Top = 402
        Width = 126
        Height = 13
        Caption = 'Additional Assessment Amt'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 105
        Top = 235
        Width = 142
        Height = 13
        Caption = 'Alternate Taxable Wage Base'
      end
      object dbedtSUI_TAX_NAME: TevDBEdit
        Left = 8
        Top = 212
        Width = 417
        Height = 21
        HelpContext = 54502
        DataField = 'SUI_TAX_NAME'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object DBRadioGroup2: TevDBRadioGroup
        Left = 436
        Top = 200
        Width = 173
        Height = 49
        HelpContext = 54502
        Caption = '~EE or ER'
        Columns = 2
        DataField = 'EE_ER_OR_EE_OTHER_OR_ER_OTHER'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'EE'
          'ER'
          'EE-OTHER'
          'ER-OTHER')
        ParentFont = False
        TabOrder = 8
        Values.Strings = (
          'E'
          'R'
          'F'
          'S')
      end
      object wwDBGrid2: TevDBGrid
        Left = 8
        Top = 20
        Width = 601
        Height = 173
        DisableThemesInTitle = False
        Selected.Strings = (
          'SUI_TAX_NAME'#9'40'#9'Sui Tax Name'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_SUI\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object DBRadioGroup4: TevDBRadioGroup
        Left = 436
        Top = 252
        Width = 173
        Height = 49
        HelpContext = 54502
        Caption = '~Base on Hours Worked'
        DataField = 'USE_MISC_TAX_RETURN_CODE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 9
        Values.Strings = (
          'Y'
          'N')
      end
      object wwdeMaximum_Wage: TevDBEdit
        Left = 8
        Top = 250
        Width = 64
        Height = 21
        HelpContext = 54502
        DataField = 'MAXIMUM_WAGE'
        DataSource = wwdsDetail
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeNew_Company_Default_Rate: TevDBEdit
        Left = 271
        Top = 250
        Width = 64
        Height = 21
        HelpContext = 54502
        DataField = 'NEW_COMPANY_DEFAULT_RATE'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwDBLookupCombo1: TevDBLookupCombo
        Left = 8
        Top = 290
        Width = 249
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'AGENCY_NAME'#9'T')
        DataField = 'SY_SUI_TAX_PMT_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
        LookupField = 'SY_GLOBAL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 5
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwDBLookupCombo2: TevDBLookupCombo
        Left = 8
        Top = 330
        Width = 249
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'AGENCY_NAME'#9'T')
        DataField = 'SY_SUI_REPORTING_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
        LookupField = 'SY_GLOBAL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 6
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object DBRadioGroup5: TevDBRadioGroup
        Left = 436
        Top = 304
        Width = 173
        Height = 49
        HelpContext = 1573
        Caption = '~Round to the Nearest Dollar'
        DataField = 'ROUND_TO_NEAREST_DOLLAR'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 10
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBEdit1: TevDBEdit
        Left = 271
        Top = 290
        Width = 64
        Height = 21
        HelpContext = 54502
        DataField = 'GLOBAL_RATE'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object rgActive: TevDBRadioGroup
        Left = 437
        Top = 360
        Width = 173
        Height = 49
        Caption = '~Active'
        DataField = 'SUI_ACTIVE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object wwcdE_D_Code_Type: TevDBComboDlg
        Left = 271
        Top = 330
        Width = 73
        Height = 21
        HelpContext = 8124
        OnCustomDlg = wwcdE_D_Code_TypeCustomDlg
        ShowButton = True
        Style = csDropDown
        DataField = 'E_D_CODE_TYPE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = 
          '[DI,DH,DG,DA,DB,DC,DD,DE,DF,DL,DM,DW,DK,D7,DV,ET,EU,EV,EW,EX,EO,' +
          'EP,EQ,M1,M2,M3,M4,M5,M6,ER,ES,E4,E3,E5,E6,E1,E8,E7,D1,DN,DQ,DP,D' +
          'R,DS,DO,DT,DU,DZ,EF,EG,EH,EI,EJ,EK,EL,EM,EB,EN,EA,EC,ED,EE]'
        Picture.AutoFill = False
        TabOrder = 11
        WordWrap = False
        UnboundDataType = wwDefault
      end
      object evDBEdit2: TevDBEdit
        Left = 271
        Top = 370
        Width = 64
        Height = 21
        HelpContext = 54502
        DataField = 'FUTURE_DEFAULT_RATE'
        DataSource = wwdsDetail
        TabOrder = 12
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object evDBEdit3: TevDBEdit
        Left = 271
        Top = 418
        Width = 64
        Height = 21
        HelpContext = 54502
        DataField = 'FUTURE_MAXIMUM_WAGE'
        DataSource = wwdsDetail
        TabOrder = 13
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object evGroupBox1: TevGroupBox
        Left = 8
        Top = 354
        Width = 249
        Height = 71
        Caption = 'Healthcare Assessment'
        TabOrder = 14
        object evLabel5: TevLabel
          Left = 11
          Top = 20
          Width = 72
          Height = 13
          Caption = 'FTE Exemption'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel6: TevLabel
          Left = 91
          Top = 20
          Width = 78
          Height = 13
          Caption = 'FTE Weekly Hrs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel7: TevLabel
          Left = 171
          Top = 20
          Width = 64
          Height = 13
          Caption = 'FTE Multiplier'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBEdit4: TevDBEdit
          Left = 9
          Top = 36
          Width = 64
          Height = 21
          HelpContext = 54502
          DataField = 'FTE_EXEMPTION'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object evDBEdit5: TevDBEdit
          Left = 89
          Top = 36
          Width = 64
          Height = 21
          HelpContext = 54502
          DataField = 'FTE_WEEKLY_HOURS'
          DataSource = wwdsDetail
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object evDBEdit6: TevDBEdit
          Left = 169
          Top = 36
          Width = 64
          Height = 21
          HelpContext = 54502
          DataField = 'FTE_MULTIPLIER'
          DataSource = wwdsDetail
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object rgAddAllButton: TevDBRadioGroup
        Left = 406
        Top = 416
        Width = 203
        Height = 41
        HelpContext = 1573
        Caption = '~Apply To Company Add All Button'
        Columns = 2
        DataField = 'CO_ADD_ALL_BUTTON'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 15
        Values.Strings = (
          'Y'
          'N')
      end
      object AltTaxWageBaseEdit: TevDBEdit
        Left = 104
        Top = 252
        Width = 82
        Height = 21
        DataField = 'ALTERNATE_TAXABLE_WAGE_BASE'
        DataSource = wwdsDetail
        TabOrder = 16
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
  end
  inherited Panel2: TevPanel
    Width = 832
  end
  inherited wwdsMaster: TevDataSource
    Left = 173
    Top = 10
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_SUI.SY_SUI
  end
  inherited wwdsList: TevDataSource
    Left = 82
    Top = 10
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 327
    Top = 18
  end
end
