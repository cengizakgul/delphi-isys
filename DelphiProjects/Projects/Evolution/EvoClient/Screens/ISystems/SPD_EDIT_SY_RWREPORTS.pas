// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_RWREPORTS;

interface

uses
  Windows, Forms, SysUtils, Dialogs,
  SFrameEntry, StdCtrls, Mask, wwdbedit, Controls, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, Classes, Db, Wwdatsrc, SDataStructure,
  EvConsts, EvUtils, Wwdotdot, Wwdbcomb, ISZippingRoutines,
  DBCtrls, Graphics, ExtCtrls, Menus, EvSecElement, isVCLBugFix, SPD_PreSnap,
  SSecurityInterface, SPackageEntry, Buttons, SDDClasses, EvContext,
  ISBasicClasses, wwdblook, SPD_MiniNavigationFrame, Variants, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvStreamUtils,
  SDataDictsystem, isBasicUtils, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIDBMemo, isUIwwDBEdit,
  isUIwwDBLookupCombo, isUIwwDBComboBox, SDataDictbureau, ImgList, ActnList,
  EvCommonInterfaces, EvDataSet, SSBReportCollection, StrUtils;

type
  TEDIT_SY_RWREPORTS = class(TFrameEntry)
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    grReports: TevDBGrid;
    TabSheet2: TTabSheet;
    Label1: TevLabel;
    wwDBEdit1: TevDBEdit;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evLabel1: TevLabel;
    wwcbReport_Type: TevDBComboBox;
    evLabel2: TevLabel;
    EvDBMemo1: TEvDBMemo;
    PopupMenu1: TPopupMenu;
    miCompile: TMenuItem;
    btnRWDesigner: TevBitBtn;
    evLabel4: TevLabel;
    evDBComboBox4: TevDBComboBox;
    btnExportReport: TevBitBtn;
    sdExport: TSaveDialog;
    gbGroups: TevGroupBox;
    grGroup: TevDBGrid;
    cbGroups: TevDBLookupCombo;
    evLabel5: TevLabel;
    MiniNavigationFrame: TMiniNavigationFrame;
    evPanel1: TevPanel;
    evLabel6: TevLabel;
    cbGroupFilter: TevDBLookupCombo;
    cdsGroupReports: TevClientDataSet;
    miCheckReports: TMenuItem;
    tsVersions: TTabSheet;
    wwdsVersions: TevDataSource;
    dbgVersions: TevDBGrid;
    lblDescription: TevLabel;
    dbtReportDescription: TevDBText;
    cmbReportType: TevDBComboBox;
    cmbStatus: TevDBComboBox;
    cmbCurrent: TevDBComboBox;
    lblNotes: TevLabel;
    dbmNotes: TEvDBMemo;
    evBitBtn1: TevBitBtn;
    btnMakeCurrent: TevBitBtn;
    btnPromote: TevBitBtn;
    btnRollBack: TevBitBtn;
    btnReportWriter: TevBitBtn;
    btnExportReportToFile: TevBitBtn;
    Actions: TActionList;
    Images: TevImageList;
    acNewVersion: TAction;
    acMakeCurrent: TAction;
    acPromote: TAction;
    acRollback: TAction;
    acRwDesigner: TAction;
    acExportReport: TAction;
    btnPreSnap: TevBitBtn;
    bvlLeftLine: TBevel;
    acPreSnap: TAction;
    cmbStatusBy: TevDBComboBox;
    SaveSelectedReportstoFile1: TMenuItem;
    LoadReportsFromFile1: TMenuItem;
    procedure btnRWDesignerClick(Sender: TObject);
    procedure grReportsDblClick(Sender: TObject);
    procedure miCompileClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure btnExportReportClick(Sender: TObject);
    procedure wwdsListStateChange(Sender: TObject);
    procedure cbGroupFilterChange(Sender: TObject);
    procedure grReportsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure miCheckReportsClick(Sender: TObject);
    procedure acNewVersionUpdate(Sender: TObject);
    procedure acNewVersionExecute(Sender: TObject);
    procedure acRwDesignerExecute(Sender: TObject);
    procedure acRwDesignerUpdate(Sender: TObject);
    procedure acExportReportExecute(Sender: TObject);
    procedure tsVersionsShow(Sender: TObject);
    procedure dbgVersionsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbgVersionsDblClick(Sender: TObject);
    procedure acMakeCurrentExecute(Sender: TObject);
    procedure acPromoteUpdate(Sender: TObject);
    procedure acPromoteExecute(Sender: TObject);
    procedure acRollbackUpdate(Sender: TObject);
    procedure acRollbackExecute(Sender: TObject);
    procedure acPreSnapExecute(Sender: TObject);
    procedure miSaveReportCollection(Sender: TObject);

    
  private
    FPrevOnFilter: TFilterRecordEvent;
    FPrevBeforePost: TDataSetNotifyEvent;
    FPrevAfterPost: TDataSetNotifyEvent;
    FPrevAfterInsert: TDataSetNotifyEvent;
    FVersionInProress: boolean;
    FNotes: string;

    procedure BeforePost(ADataSet: TDataSet);
    procedure AfterPost(ADataSet: TDataSet);
    procedure AfterInsert(aDataSet: TDataset);
    procedure OnFilter(DataSet: TDataSet; var Accept: Boolean);
    procedure CompressReport(aDS: TDataset; const aReportField: string);
    procedure RefreshReportList;
    procedure RefreshSbUsers;
    procedure CompileReports(ASaveBack: Boolean);
    procedure UpdateVersionInProgress;
    procedure UpdateVersionSorting;
    function UC_Report: IEvDualStream;

    procedure OpenVersionsTab;
    procedure RunRwDesigner;
    procedure ExportReportToFile(ExportFromVersions: boolean);
    procedure AddNewVersion;
    procedure MakeCurrent(Force: boolean = False);
    procedure Promote;
    procedure RollBack;
    procedure CreateUnderConstructionReport;
  protected
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure SetReadOnly(Value: Boolean); override;
    function  GetIfReadOnly: Boolean; override;
    procedure OnGetSecurityStates(const States: TSecurityStateList); override;
    function  FilteredByGroup: Boolean;
  public
    function  GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure AfterClick(Kind: Integer); override;
  end;

implementation

{$R *.DFM}
{$R SPD_EDIT_SY_RWREPORTS.RES}

const
  ctReadOnlyWithExport = 'E';
  UnderConstructionAncClassName = 'TrwlUnderConstructionReport';

procedure TEDIT_SY_RWREPORTS.btnRWDesignerClick(Sender: TObject);
var
  MS1, PackedS: IEvDualStream;
  Notes, ClName, AncClName: String;
begin
  // Open RW designer for read only
  MS1 := TEvDualStreamHolder.CreateInMemory;
  ctx_StartWait('Opening report...');
  try
    if wwdsDetail.DataSet.State in [dsBrowse, dsEdit] then
      try
        ctx_RWLocalEngine.GetReportResources(
          wwdsDetail.DataSet.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').AsInteger, CH_DATABASE_SYSTEM, MS1);
      except
        on E: Exception do
        begin
          MS1.Clear;
          EvErrMessage(E.Message);
        end;
      end
    else if (wwdsDetail.DataSet.State in [dsInsert]) and not wwdsDetail.DataSet.FieldByName('REPORT_FILE').IsNull then
    begin
      PackedS := TEvDualStreamHolder.CreateInMemory;
      PackedS := TevClientDataSet(wwdsDetail.DataSet).GetBlobData('REPORT_FILE');
      PackedS.Position := 0;
      InflateStream(PackedS.RealStream, MS1.RealStream);
    end;
  finally
    ctx_EndWait;
  end;

  MS1.Position := 0;
  ctx_RWDesigner.ShowDesigner(MS1, wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString, Notes, ClName, AncClName);
end;

function TEDIT_SY_RWREPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS;
end;

procedure TEDIT_SY_RWREPORTS.SetReadOnly(Value: Boolean);
var
  c: ISecurityElement;
begin
  inherited;
  c := FindSecurityElement(Self, ClassName);
  if not(Assigned(c) and (c.SecurityState = stReadOnly)) then
    btnRWDesigner.SecurityRO := False;

  miCompile.Enabled := not Value;
  miCheckReports.Enabled := not Value;
  EvDBMemo1.SecurityRO := True;
  btnExportReport.SecurityRO := not (SecurityState[1] in [ctReadOnlyWithExport, ctEnabled]);
  cbGroupFilter.SecurityRO := False;
end;


procedure TEDIT_SY_RWREPORTS.grReportsDblClick(Sender: TObject);
begin
  inherited;
  OpenVersionsTab;
end;

procedure TEDIT_SY_RWREPORTS.miCompileClick(Sender: TObject);
begin
  CompileReports(True);
end;


procedure TEDIT_SY_RWREPORTS.Activate;
begin
  inherited;
  RefreshSbUsers;
  FPrevOnFilter := DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.OnFilterRecord;
  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.OnFilterRecord := OnFilter;

  FPrevAfterInsert := DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.AfterInsert;
  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.AfterInsert := AfterInsert;

  FPrevBeforePost := DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.BeforePost;
  DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.BeforePost := BeforePost;

  FPrevAfterPost := DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.AfterPost;
  DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.AfterPost := AfterPost;

  DM_SYSTEM_MISC.SY_REPORT_GROUPS.Open;
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Open;
  DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.Open;

  cdsGroupReports.CloneCursor(DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS, True);
  cdsGroupReports.IndexFieldNames := 'SY_REPORT_WRITER_REPORTS_NBR';

  MiniNavigationFrame.DataSource := wwdsList;
  MiniNavigationFrame.InsertFocusControl := cbGroups;

  tsVersions.TabVisible := IsQAMode;
  UpdateVersionSorting;
end;


procedure TEDIT_SY_RWREPORTS.Deactivate;
begin
  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.OnFilterRecord := FPrevOnFilter;
  DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.BeforePost := FPrevBeforePost;
  DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.AfterPost := FPrevAfterPost;
  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.AfterInsert := FPrevAfterInsert;
  inherited;
end;

procedure TEDIT_SY_RWREPORTS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.State = dsBrowse then
    grReports.Enabled := True
  else
    grReports.Enabled := False;

  gbGroups.Enabled := not (FilteredByGroup and (wwdsDetail.DataSet.State = dsInsert));
end;

procedure TEDIT_SY_RWREPORTS.CompressReport(aDS: TDataset; const aReportField: string);
var
  MS1, MS2: TMemoryStream;
begin
  MS1 := TMemoryStream.Create;
  MS2 := TMemoryStream.Create;
  try
    TBlobField(aDS.FieldByName(aReportField)).SaveToStream(MS1);
    MS1.Position := 0;
    DeflateStream(MS1, MS2);
    MS2.Position := 0;
    TBlobField(aDS.FieldByName(aReportField)).LoadFromStream(MS2);
  finally
    MS1.Free;
    MS2.Free;
  end;
end;

procedure TEDIT_SY_RWREPORTS.btnExportReportClick(Sender: TObject);
begin
  ExportReportToFile(False);
end;

function TEDIT_SY_RWREPORTS.GetIfReadOnly: Boolean;
begin
  Result := (SecurityState = ctReadOnlyWithExport) or inherited GetIfReadOnly;
end;

procedure TEDIT_SY_RWREPORTS.OnGetSecurityStates(
  const States: TSecurityStateList);
begin
  States.InsertAfter(ctEnabled, ctReadOnlyWithExport, 'Read only with export');
end;

procedure TEDIT_SY_RWREPORTS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS, EditDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS_VERSIONS, EditDataSets, '');
end;

procedure TEDIT_SY_RWREPORTS.wwdsListStateChange(Sender: TObject);
begin
  inherited;
  if wwdsList.State = dsInsert then
    DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.SY_REPORT_WRITER_REPORTS_NBR.AsInteger := DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR.AsInteger;
end;

procedure TEDIT_SY_RWREPORTS.cbGroupFilterChange(Sender: TObject);
begin
  if FilteredByGroup then
  begin
    cdsGroupReports.Filter := 'SY_REPORT_GROUPS_NBR = ' + cbGroupFilter.LookupValue;
    cdsGroupReports.Filtered := False;
    cdsGroupReports.Filtered := True;
  end;

  RefreshReportList;
end;

procedure TEDIT_SY_RWREPORTS.OnFilter(DataSet: TDataSet; var Accept: Boolean);
begin
  if Assigned(FPrevOnFilter) then
    FPrevOnFilter(DataSet, Accept)
  else
    Accept := True;
  if FilteredByGroup then
    Accept := Accept and cdsGroupReports.FindKey([DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR.AsInteger]);
end;

procedure TEDIT_SY_RWREPORTS.RefreshReportList;
begin
  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DisableControls;
  try
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Filtered := False;
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Filtered := True;
  finally
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.EnableControls;
  end;
end;

function TEDIT_SY_RWREPORTS.FilteredByGroup: Boolean;
begin
  Result := (cbGroupFilter.Value <> '') and (cbGroupFilter.LookupValue <> '');
end;

procedure TEDIT_SY_RWREPORTS.grReportsKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (Shift = [ssCtrl]) then
    OpenVersionsTab;
end;

function TEDIT_SY_RWREPORTS.GetInsertControl: TWinControl;
begin
  Result := wwDBEdit1;
end;

procedure TEDIT_SY_RWREPORTS.CompileReports(ASaveBack: Boolean);
var
  MS: IEvDualStream;
  h, err: String;

  procedure CompileRep;
  var
    ClName, AncClName: String;
  begin
    MS.Clear;
    ctx_RWLocalEngine.GetReportResources(
      wwdsDetail.DataSet.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').AsInteger,
      CH_DATABASE_SYSTEM, MS);
    MS.Position := 0;

    try
      ctx_RWLocalEngine.CompileReport(MS, ClName, AncClName);
    except
      on E: Exception do
        err := E.Message;
    end;

    if Length(err) > 0 then
      Exit;

    if ASaveBack then
    begin
      wwdsDetail.DataSet.Edit;

      TevClientDataSet(wwdsDetail.DataSet).UpdateBlobData('REPORT_FILE', MS);
      CompressReport(wwdsDetail.DataSet, 'REPORT_FILE');
      wwdsDetail.DataSet.FieldByName('Class_Name').AsString := ClName;
      wwdsDetail.DataSet.FieldByName('Ancestor_Class_Name').AsString := AncClName;
      wwdsDetail.DataSet.Post;
      ctx_DataAccess.PostDataSets([TevClientDataSet(wwdsDetail.DataSet)]);
    end;  
  end;

begin
  if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) or wwDBEdit1.ReadOnly then
    Exit;

  MS := TEvDualStreamHolder.Create;
  TevClientDataSet(wwdsDetail.DataSet).DisableControls;
  Screen.Cursor := crAppStart;

  if ASaveBack then
    h := 'Compiling reports...'
  else
    h := 'Checking reports...';
    
  ctx_StartWait(h);

  ctx_RWLocalEngine.BeginWork;
  try
    err := '';

    ctx_StartWait('');
    try
      while grReports.SelectedList.Count > 0 do
      begin
        wwdsDetail.DataSet.GotoBookmark(grReports.SelectedList[0]);
        ctx_UpdateWait('Reports left: ' + IntToStr(grReports.SelectedList.Count) + #13 +
          wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString);
        DispatchAppPaint;
        CompileRep;
        if Length(err) > 0 then
          Break;
        grReports.SelectedList.Delete(0);
      end
    finally
      ctx_EndWait;
    end;

  finally
    ctx_EndWait;
    TevClientDataSet(wwdsDetail.DataSet).EnableControls;
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.Cancel;
    Screen.Cursor := crArrow;
    ctx_RWLocalEngine.EndWork;
  end;

  if Length(err) > 0 then
  begin
    EvErrMessage(Err);
    btnRWDesigner.Click;
  end;
end;

procedure TEDIT_SY_RWREPORTS.miCheckReportsClick(Sender: TObject);
begin
  CompileReports(False);
end;

procedure TEDIT_SY_RWREPORTS.OpenVersionsTab;
begin
  if IsQAMode then
    PageControl1.ActivatePage( tsVersions );
end;

procedure TEDIT_SY_RWREPORTS.RunRwDesigner;
var
  MS1, MS2: IEvDualStream;
  ClName, AncClName: String;
begin
  // the report is packed in DB, load and unpack
  MS2 := TEvDualStreamHolder.CreateInMemory;
  MS1 := TEvDualStreamHolder.CreateInMemory;
  MS2 := TevClientDataSet(wwdsVersions.DataSet).GetBlobData('REPORT_DATA');
  MS2.Position := 0;
  if MS2.Size > 0 then
  begin
    InflateStream(MS2.RealStream, MS1.RealStream);
    MS1.Position := 0;

    // open the unpacked in RW
    MS2.Clear;
    MS2.RealStream.CopyFrom(MS1.RealStream, 0);
    MS2.Position := 0;
  end;
  ctx_RWDesigner.ShowDesigner(MS2, dbtReportDescription.Field.AsString, FNotes, ClName, AncClName);

  if not wwDBEdit1.ReadOnly and
    (wwdsVersions.DataSet.FieldByName('STATUS').AsString = REPORT_STATUS_IN_PROGRESS) then
  begin
    MS1.Position := 0;
    MS2.Position := 0;

    if (MS1.Size <> MS2.Size) or not CompareMem(MS1.Memory, MS2.Memory, MS1.Size) then
    begin  // the report was modified
      dbgVersions.Enabled := False;
      try
        if not (wwdsVersions.DataSet.State in [dsEdit, dsInsert]) then
          wwdsVersions.DataSet.Edit;

        // pack and store
        MS1.Clear;
        DeflateStream(MS2.RealStream, MS1.RealStream);
        MS1.Position := 0;
        TevClientDataSet(wwdsVersions.DataSet).UpdateBlobData('REPORT_DATA', MS1);
        wwdsVersions.DataSet.FieldByName('Class_Name').AsString := ClName;
        wwdsVersions.DataSet.FieldByName('Ancestor_Class_Name').AsString := AncClName;
        wwdsVersions.DataSet.FieldByName('MODIFIED_ON').AsDateTime := Now;

        if Length(FNotes) > 0 then
          TBlobField(wwdsVersions.DataSet.FieldByName('NOTES')).AsString := FNotes + #13#13 + 'Last update: ' + DateTimeToStr(Now)
        else
          TBlobField(wwdsVersions.DataSet.FieldByName('NOTES')).AsString := 'Last update: ' + DateTimeToStr(Now);
      finally
        dbgVersions.Enabled := True;
      end;
    end;
  end;
end;

procedure TEDIT_SY_RWREPORTS.acNewVersionUpdate(Sender: TObject);
begin
  inherited;
  (Sender as TCustomAction).Enabled := not FVersionInProress;
end;

procedure TEDIT_SY_RWREPORTS.acNewVersionExecute(Sender: TObject);
begin
  inherited;
  AddNewVersion;
end;

procedure TEDIT_SY_RWREPORTS.AddNewVersion;
var
  ReportData, Notes: IEvDualStream;
  sClassName, sAncestorClassName: string;
begin
  TevClientDataSet(wwdsVersions.DataSet).DisableControls;
  with DM_SYSTEM_MISC do
  try
    if SY_REPORTS_VERSIONS.SY_REPORTS_VERSIONS_NBR.IsNull then
    begin
      ReportData := SY_REPORT_WRITER_REPORTS.GetBlobData('REPORT_FILE');
      Notes := SY_REPORT_WRITER_REPORTS.GetBlobData('NOTES');
      sClassName := SY_REPORT_WRITER_REPORTS.CLASS_NAME.Value;
      sAncestorClassName := SY_REPORT_WRITER_REPORTS.ANCESTOR_CLASS_NAME.Value;
    end
    else begin
      if (SY_REPORTS_VERSIONS.RecordCount = 1) and (SY_REPORTS_VERSIONS.ANCESTOR_CLASS_NAME.Value = UnderConstructionAncClassName) then
      begin
        ReportData := nil;
        Notes := nil;
        sClassName := '';
        sAncestorClassName := '';
      end
      else begin
        ReportData := SY_REPORTS_VERSIONS.GetBlobData('REPORT_DATA');
        Notes := SY_REPORTS_VERSIONS.GetBlobData('NOTES');
        sClassName := SY_REPORTS_VERSIONS.CLASS_NAME.Value;
        sAncestorClassName := SY_REPORTS_VERSIONS.ANCESTOR_CLASS_NAME.Value;
      end;
    end;

    SY_REPORTS_VERSIONS .BeforePost := FPrevBeforePost;
    SY_REPORTS_VERSIONS.AfterPost := FPrevAfterPost;
    SY_REPORTS_VERSIONS.Append;
    SY_REPORTS_VERSIONS.SY_REPORT_WRITER_REPORTS_NBR.Value := SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR.Value;
    SY_REPORTS_VERSIONS.STATUS_DATE.Value := Now;
    SY_REPORTS_VERSIONS.STATUS.Value := REPORT_STATUS_IN_PROGRESS;
    SY_REPORTS_VERSIONS.MODIFIED_ON.Value := Now;
    SY_REPORTS_VERSIONS.CURRENT_REPORT.Value := REPORT_CURRENT_NO;
    SY_REPORTS_VERSIONS.STATUS_BY.Value := Context.UserAccount.InternalNBR;
    if Assigned(ReportData) then
      SY_REPORTS_VERSIONS.UpdateBlobData('REPORT_DATA', ReportData);
    if Assigned(Notes) then
      SY_REPORTS_VERSIONS.UpdateBlobData('NOTES', Notes);
    SY_REPORTS_VERSIONS.CLASS_NAME.Value := sClassName;
    SY_REPORTS_VERSIONS.ANCESTOR_CLASS_NAME.Value := sAncestorClassName;
    SY_REPORTS_VERSIONS.Post;
    SY_REPORTS_VERSIONS.BeforePost := BeforePost;
    SY_REPORTS_VERSIONS.AfterPost := AfterPost;

    SY_REPORTS_VERSIONS.Locate('STATUS', REPORT_STATUS_IN_PROGRESS, []);
    FVersionInProress := True;
  finally
    TevClientDataSet(wwdsVersions.DataSet).EnableControls;
  end;
end;

procedure TEDIT_SY_RWREPORTS.acRwDesignerExecute(Sender: TObject);
begin
  inherited;
  RunRwDesigner;
end;

procedure TEDIT_SY_RWREPORTS.acRwDesignerUpdate(Sender: TObject);
begin
  inherited;
  (Sender as TCustomAction).Enabled := Assigned(wwdsVersions.DataSet) and
    (wwdsVersions.DataSet.RecordCount > 0);
end;

procedure TEDIT_SY_RWREPORTS.ExportReportToFile(ExportFromVersions: boolean);
var
  FS, PackedS: IEvDualStream;
  fn: string;
begin
  fn := '';
  if ExportFromVersions then
  begin
    fn := fn + StringReplace(wwdsVersions.DataSet.FieldByName('DB_VERSION').AsString, '.', '_', [rfReplaceAll]);
    if fn <> '' then
      fn := '_' + fn;
    if wwdsVersions.DataSet.FieldByName('STATUS').AsString = REPORT_STATUS_PROMOTED then
      fn := '_Promoted' + fn
    else
      fn := '_InProgress' + fn;
  end;
  fn := NormalizeFileName(Trim( dbtReportDescription.Field.AsString )) + fn + '.rwr';

  sdExport.FileName := fn;
  if not sdExport.Execute then
    Exit;

  sdExport.FileName := ChangeFileExt(sdExport.FileName, '.rwr');
  FS := TEvDualStreamHolder.CreateFromNewFile(sdExport.FileName);

  ctx_StartWait('Opening report...');
  try
    if wwdsDetail.DataSet.State in [dsBrowse, dsEdit] then
    begin
      PackedS := TEvDualStreamHolder.CreateInMemory;
      if ExportFromVersions then
        PackedS := TevClientDataSet(wwdsVersions.DataSet).GetBlobData('REPORT_DATA')
      else
        PackedS := TevClientDataSet(wwdsDetail.DataSet).GetBlobData('REPORT_FILE');
      PackedS.Position := 0;
      InflateStream(PackedS.RealStream, FS.RealStream);
    end;
  finally
    ctx_EndWait;
  end;

  EvMessage('Report has been successfully exported to file');
end;

procedure TEDIT_SY_RWREPORTS.acExportReportExecute(Sender: TObject);
begin
  inherited;
  ExportReportToFile(True);
end;

procedure TEDIT_SY_RWREPORTS.UpdateVersionInProgress;
begin
  FVersionInProress := not VarIsNull( wwdsVersions.Dataset.Lookup('STATUS', REPORT_STATUS_IN_PROGRESS, 'SY_REPORT_WRITER_REPORTS_NBR') );
end;

procedure TEDIT_SY_RWREPORTS.tsVersionsShow(Sender: TObject);
begin
  inherited;
  UpdateVersionInProgress;
end;

procedure TEDIT_SY_RWREPORTS.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavFirst, NavPrevious, NavNext, NavLast:
      if (PageControl1.ActivePage = tsVersions) then
        UpdateVersionInProgress;
  end;
end;

procedure TEDIT_SY_RWREPORTS.BeforePost(ADataSet: TDataSet);
var
  n, c: Integer;
  h: string;
begin
  if Assigned(FPrevBeforePost) then
    FPrevBeforePost(ADataSet);

  if wwdsVersions.DataSet.State in [dsBrowse, dsEdit] then
  begin
    h := '';
    n := 0;
    while True do
    begin
      if not EvDialog('Change Reason', 'Ticket #', h) then
        AbortEx;
      Val(Trim(h), n, c);
      if c <> 0 then
        EvErrMessage('Ticket number is incorrect!')
      else
        break;
    end;

    if Length(FNotes) > 0 then
      FNotes := FNotes + #13#13;

    FNotes := FNotes + 'Ticket #' + IntToStr(n) + #13#13 +
      'Last update: ' + DateTimeToStr(Now);
    TBlobField(wwdsVersions.DataSet.FieldByName('NOTES')).AsString := FNotes;
    wwdsVersions.DataSet.FieldByName('TICKET').AsString := IntToStr(n);
    wwdsVersions.DataSet.FieldByName('MODIFIED_ON').AsDateTime := Now;
  end;
end;

procedure TEDIT_SY_RWREPORTS.dbgVersionsKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (Shift = [ssCtrl]) then
    RunRwDesigner;
end;

procedure TEDIT_SY_RWREPORTS.dbgVersionsDblClick(Sender: TObject);
begin
  inherited;
  RunRwDesigner;
end;

procedure TEDIT_SY_RWREPORTS.acMakeCurrentExecute(Sender: TObject);
begin
  inherited;
  MakeCurrent;
end;

procedure TEDIT_SY_RWREPORTS.MakeCurrent(Force: boolean = False);
var
  TransactionManager: TTransactionManager;
  ReportData, Notes: IEvDualStream;

  procedure UpdateCurrentReportValue;
  var
    Nbr: integer;
  begin
    DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.BeforePost := FPrevBeforePost;
    DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.AfterPost := FPrevAfterPost;
    with DM_SYSTEM_MISC do
    try
      Nbr := SY_REPORTS_VERSIONS.SY_REPORTS_VERSIONS_NBR.Value;
      if SY_REPORTS_VERSIONS.Locate('CURRENT_REPORT', REPORT_CURRENT_YES, []) then
      begin
        SY_REPORTS_VERSIONS.Edit;
        SY_REPORTS_VERSIONS.CURRENT_REPORT.Value := REPORT_CURRENT_NO;
        SY_REPORTS_VERSIONS.Post;
      end;
      SY_REPORTS_VERSIONS.Locate('SY_REPORTS_VERSIONS_NBR', Nbr, []);
      SY_REPORTS_VERSIONS.Edit;
      SY_REPORTS_VERSIONS.CURRENT_REPORT.Value := REPORT_CURRENT_Yes;
      SY_REPORTS_VERSIONS.Post;
    finally
      SY_REPORTS_VERSIONS.BeforePost := BeforePost;
      SY_REPORTS_VERSIONS.AfterPost := AfterPost;
    end;
  end;

  procedure CopyVersionToRWReports;
  begin
    with DM_SYSTEM_MISC do
    begin
      SY_REPORT_WRITER_REPORTS.Edit;
      SY_REPORT_WRITER_REPORTS.UpdateBlobData('REPORT_FILE', ReportData);
      SY_REPORT_WRITER_REPORTS.UpdateBlobData('NOTES', Notes);
      SY_REPORT_WRITER_REPORTS.CLASS_NAME.Value := SY_REPORTS_VERSIONS.CLASS_NAME.Value;
      SY_REPORT_WRITER_REPORTS.ANCESTOR_CLASS_NAME.Value := SY_REPORTS_VERSIONS.ANCESTOR_CLASS_NAME.Value;
      SY_REPORT_WRITER_REPORTS.Post;
    end;
  end;
begin
  if Force or (wwdsVersions.Dataset.FieldByName('CURRENT_REPORT').AsString <> REPORT_CURRENT_YES) then
  begin
    ctx_StartWait;
    Notes := TevClientDataSet(wwdsVersions.DataSet).GetBlobData('NOTES');
    ReportData := TevClientDataSet(wwdsVersions.DataSet).GetBlobData('REPORT_DATA');

    TransactionManager := TTransactionManager.Create(nil);
    TevClientDataSet(wwdsDetail.DataSet).DisableControls;
    TevClientDataSet(wwdsVersions.DataSet).DisableControls;
    with DM_SYSTEM_MISC do
    try
      TransactionManager.Initialize([SY_REPORT_WRITER_REPORTS, SY_REPORTS_VERSIONS]);

      if (wwdsVersions.Dataset.FieldByName('CURRENT_REPORT').AsString <> REPORT_CURRENT_YES) then
        UpdateCurrentReportValue;
      CopyVersionToRWReports;

      try
        TransactionManager.ApplyUpdates;
      except
        TransactionManager.CancelUpdates;
      end;
    finally
      TevClientDataSet(wwdsDetail.DataSet).EnableControls;
      TevClientDataSet(wwdsVersions.DataSet).EnableControls;
      TransactionManager.Free;
      ctx_EndWait;
    end;
  end;
end;

procedure TEDIT_SY_RWREPORTS.acPromoteUpdate(Sender: TObject);
begin
  inherited;
  (Sender as TCustomAction).Enabled := Assigned(wwdsVersions.DataSet) and
    (wwdsVersions.DataSet.FieldByName('STATUS').AsString = REPORT_STATUS_IN_PROGRESS);
end;

procedure TEDIT_SY_RWREPORTS.acPromoteExecute(Sender: TObject);
begin
  inherited;
  Promote;
end;

procedure TEDIT_SY_RWREPORTS.Promote;
var
  TransactionManager: TTransactionManager;
  GoNext: boolean;
  PriorUnderConstructionNbr, k: integer;
begin
  TevClientDataSet(wwdsVersions.DataSet).DisableControls;
  with DM_SYSTEM_MISC do
  try
    GoNext := True;
    PriorUnderConstructionNbr := -1;
    if SY_REPORTS_VERSIONS.RecordCount > 1 then
    begin
      k := SY_REPORTS_VERSIONS.RecNo;
      SY_REPORTS_VERSIONS.First;
      while not SY_REPORTS_VERSIONS.Eof and GoNext and (PriorUnderConstructionNbr = -1) do
      begin
        if (SY_REPORTS_VERSIONS.STATUS.Value = REPORT_STATUS_PROMOTED) and (SY_REPORTS_VERSIONS.DB_VERSION.AsString = '') then
        begin
          if SY_REPORTS_VERSIONS.ANCESTOR_CLASS_NAME.Value = UnderConstructionAncClassName then
            PriorUnderConstructionNbr := SY_REPORTS_VERSIONS.SY_REPORTS_VERSIONS_NBR.Value
          else begin
            GoNext := False;
            EvMessage('The prior "Promoted" report version does not have Version number!');
          end;
        end;
        SY_REPORTS_VERSIONS.Next;
      end;
      SY_REPORTS_VERSIONS.RecNo := k;
    end;

    if GoNext and
      (EvMessage('Are you sure you want to promote the selected report version?', mtConfirmation, [mbYes, mbNo], mbYes) = mrYes) then
    begin
      ctx_StartWait('Promoting the selected version...');
      TransactionManager := TTransactionManager.Create(nil);
      DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.BeforePost := FPrevBeforePost;
      DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.AfterPost := FPrevAfterPost;
      try
        TransactionManager.Initialize([SY_REPORTS_VERSIONS]);
        // set promoted status in the selected report version
        SY_REPORTS_VERSIONS.Edit;
        SY_REPORTS_VERSIONS.STATUS.Value := REPORT_STATUS_PROMOTED;
        SY_REPORTS_VERSIONS.STATUS_DATE.Value := Now;
        SY_REPORTS_VERSIONS.STATUS_BY.Value := Context.UserAccount.InternalNBR;
        SY_REPORTS_VERSIONS.Post;
        // if the only prior version is UnderConstruction and does not have version yet the it should be removed
        if PriorUnderConstructionNbr > -1 then
        begin
          if SY_REPORTS_VERSIONS.Locate('SY_REPORTS_VERSIONS_NBR', PriorUnderConstructionNbr, []) then
            SY_REPORTS_VERSIONS.Delete;
        end;
        try
          TransactionManager.ApplyUpdates;
        except
          TransactionManager.CancelUpdates;
        end;
      finally
        DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.BeforePost := BeforePost;
        DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.AfterPost := AfterPost;
        TransactionManager.Free;
        ctx_EndWait;
        UpdateVersionInProgress;
      end;
    end;
  finally
    TevClientDataSet(wwdsVersions.DataSet).EnableControls;
  end;
end;

procedure TEDIT_SY_RWREPORTS.UpdateVersionSorting;
begin
  with DM_SYSTEM_MISC.SY_REPORTS_VERSIONS do
  begin
    if IndexDefs.IndexOf('SORT') <> -1 then
      DeleteIndex('SORT');
    AddIndex('SORT', 'STATUS_DATE', [ixCaseInsensitive], 'STATUS_DATE');
    IndexName := 'SORT';
  end;
end;

procedure TEDIT_SY_RWREPORTS.acRollbackUpdate(Sender: TObject);
begin
  inherited;
  (Sender as TCustomAction).Enabled := Assigned(wwdsVersions.DataSet) and
    (wwdsVersions.DataSet.FieldByName('DB_VERSION').AsString = '') and
    (wwdsVersions.DataSet.FieldByName('STATUS').AsString = REPORT_STATUS_PROMOTED);
end;

procedure TEDIT_SY_RWREPORTS.acRollbackExecute(Sender: TObject);
begin
  inherited;
  Rollback;
end;

procedure TEDIT_SY_RWREPORTS.RollBack;
var
  TransactionManager: TTransactionManager;
  GoNext: boolean;
  ShouldBeCurrent: boolean;
  k: integer;

  procedure DeleteExistingInProgress;
  var
    Nbr: integer;
  begin
    with DM_SYSTEM_MISC do
    begin
      Nbr := SY_REPORTS_VERSIONS.SY_REPORTS_VERSIONS_NBR.Value;
      if SY_REPORTS_VERSIONS.Locate('STATUS', REPORT_STATUS_IN_PROGRESS, []) then
      begin
        ShouldBeCurrent := SY_REPORTS_VERSIONS.CURRENT_REPORT.Value = REPORT_CURRENT_YES;
        SY_REPORTS_VERSIONS.Delete;
      end;
      SY_REPORTS_VERSIONS.Locate('SY_REPORTS_VERSIONS_NBR', Nbr, []);
    end;
  end;
begin
  GoNext := True;
  ShouldBeCurrent := False;
  TevClientDataSet(wwdsVersions.DataSet).DisableControls;
  with DM_SYSTEM_MISC do
  try
    if (EvMessage('Are you sure you want to rollback the selected report version?', mtConfirmation, [mbYes, mbNo], mbYes) = mrYes) then
    begin
      ctx_StartWait('Rollback the selected version...');
      TransactionManager := TTransactionManager.Create(nil);
      SY_REPORTS_VERSIONS.BeforePost := FPrevBeforePost;
      SY_REPORTS_VERSIONS.AfterPost := FPrevAfterPost;
      k := SY_REPORTS_VERSIONS.RecNo;
      try
        TransactionManager.Initialize([SY_REPORTS_VERSIONS]);
        if FVersionInProress then
        begin
          GoNext := EvMessage('Do you wish to proceed by deleting the existing "In Progress" version?', mtConfirmation, [mbYes, mbNo], mbYes) = mrYes;
          if GoNext then
            DeleteExistingInProgress;
        end;

        if GoNext then
        begin
          try
            SY_REPORTS_VERSIONS.Edit;
            SY_REPORTS_VERSIONS.STATUS.Value := REPORT_STATUS_IN_PROGRESS;
            SY_REPORTS_VERSIONS.STATUS_DATE.Value := Now;
            SY_REPORTS_VERSIONS.Post;
            if SY_REPORTS_VERSIONS.RecordCount = 1 then
            begin
              //add preceding "promoted" version with default "Under construction" report
              SY_REPORTS_VERSIONS.Insert;
              SY_REPORTS_VERSIONS.SY_REPORT_WRITER_REPORTS_NBR.Value := SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR.Value;
              SY_REPORTS_VERSIONS.STATUS_DATE.Value := Now;
              SY_REPORTS_VERSIONS.STATUS.Value := REPORT_STATUS_PROMOTED;
              SY_REPORTS_VERSIONS.CURRENT_REPORT.Value := REPORT_CURRENT_NO;
              SY_REPORTS_VERSIONS.MODIFIED_ON.Value := Now;
              SY_REPORTS_VERSIONS.STATUS_BY.Value := Context.UserAccount.InternalNBR;
              SY_REPORTS_VERSIONS.UpdateBlobData('REPORT_DATA', UC_Report);
              CompressReport(SY_REPORTS_VERSIONS, 'REPORT_DATA');
              SY_REPORTS_VERSIONS.NOTES.AsString := 'Report is under construction';
              SY_REPORTS_VERSIONS.ANCESTOR_CLASS_NAME.Value := UnderConstructionAncClassName;
              SY_REPORTS_VERSIONS.Post;
            end;
            try
              TransactionManager.ApplyUpdates;
            except
              TransactionManager.CancelUpdates;
            end;
          finally
            SY_REPORTS_VERSIONS.RecNo := k; //select "In Progress" version
          end;
        end;
      finally
        DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.BeforePost := BeforePost;
        DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.AfterPost := AfterPost;
        TransactionManager.Free;
        ctx_EndWait;
        UpdateVersionInProgress;
      end;
    end;
  finally
    TevClientDataSet(wwdsVersions.DataSet).EnableControls;
  end;
  if ShouldBeCurrent then
    MakeCurrent;
end;

procedure TEDIT_SY_RWREPORTS.CreateUnderConstructionReport;
begin
  with DM_SYSTEM_MISC do
  if SY_REPORT_WRITER_REPORTS.State in [dsEdit, dsInsert] then
  begin
    SY_REPORT_WRITER_REPORTS.UpdateBlobData('REPORT_FILE', UC_Report);
    SY_REPORT_WRITER_REPORTS.ANCESTOR_CLASS_NAME.Value := UnderConstructionAncClassName;
    SY_REPORT_WRITER_REPORTS.NOTES.AsString := 'Report is under construction';
    CompressReport(SY_REPORT_WRITER_REPORTS, 'REPORT_FILE');

    SY_REPORTS_VERSIONS.Insert;
    SY_REPORTS_VERSIONS.SY_REPORT_WRITER_REPORTS_NBR.Value := SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR.Value;
    SY_REPORTS_VERSIONS.STATUS_DATE.Value := Now;
    SY_REPORTS_VERSIONS.STATUS.Value := REPORT_STATUS_PROMOTED;
    SY_REPORTS_VERSIONS.CURRENT_REPORT.Value := REPORT_CURRENT_YES;
    SY_REPORTS_VERSIONS.MODIFIED_ON.Value := Now;
    SY_REPORTS_VERSIONS.STATUS_BY.Value := Context.UserAccount.InternalNBR;
    SY_REPORTS_VERSIONS.UpdateBlobData('REPORT_DATA', UC_Report);
    CompressReport(SY_REPORTS_VERSIONS, 'REPORT_DATA');
    SY_REPORTS_VERSIONS.NOTES.AsString := 'Report is under construction';
    SY_REPORTS_VERSIONS.ANCESTOR_CLASS_NAME.Value := UnderConstructionAncClassName;
  end;
end;

procedure TEDIT_SY_RWREPORTS.AfterInsert(aDataSet: TDataset);
begin
  if Assigned(FPrevAfterInsert) then
    FPrevAfterInsert(aDataSet);

  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.REPORT_STATUS.Value := 'V';
  CreateUnderConstructionReport;
//  AddNewVersion;
end;

procedure TEDIT_SY_RWREPORTS.acPreSnapExecute(Sender: TObject);
begin
  inherited;
  if ShowPreSnapDialog then
  begin
    DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.Close;
    DM_SYSTEM_MISC.SY_REPORTS_VERSIONS.DataRequired('ALL');
    wwdsDetail.DataSet.Refresh;
    wwdsVersions.DataSet.Refresh;
  end;
end;

procedure TEDIT_SY_RWREPORTS.RefreshSbUsers;
var
  Q: IevQuery;
  s: string;
begin
  ctx_StartWait('Refresh SB user lookup list...');
  try
    s := 'select u.SB_USER_NBR, u.USER_ID ' +
         'from sb_user u ' +
         'where {AsOfNow<u>}';
    Q := TevQuery.Create(s);
    Q.Execute;
    Q.Result.First;
    cmbStatusBy.Items.Clear;
    while not Q.Result.Eof do
    begin
      cmbStatusBy.Items.Add(Q.Result.FieldByName('User_ID').AsString + ''#9'' + Q.Result.FieldByName('Sb_User_Nbr').AsString);
      Q.Result.Next;
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_SY_RWREPORTS.AfterPost(ADataSet: TDataSet);
begin
  if wwdsVersions.Dataset.FieldByName('CURRENT_REPORT').AsString = REPORT_CURRENT_YES then
    MakeCurrent(True);

  if Assigned(FPrevAfterPost) then
    FPrevAfterPost(ADataSet);
end;

procedure TEDIT_SY_RWREPORTS.miSaveReportCollection(Sender: TObject);
var
  iRepCollection : IEvReportCollectionHelper;
begin
  iRepCollection := TEvRepCollectionHelper.Create();
  // system reports pass -1 as Client number
  iRepCollection.SaveReportCollection(grReports, CH_DATABASE_SYSTEM,-1, True);
end;

function TEDIT_SY_RWREPORTS.UC_Report: IEvDualStream;
begin
  Result := GetBinaryResource('UNDER_CONSTRUCTION_REPORT', 'RWR');
  Result.Position := 0;
end;

initialization
  RegisterClass(TEDIT_SY_RWREPORTS);

end.
