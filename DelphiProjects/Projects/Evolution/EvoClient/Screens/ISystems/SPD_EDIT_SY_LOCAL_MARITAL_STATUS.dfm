inherited EDIT_SY_LOCAL_MARITAL_STATUS: TEDIT_SY_LOCAL_MARITAL_STATUS
  Width = 630
  Height = 460
  inherited PageControl1: TevPageControl
    Width = 630
    Height = 426
    ActivePage = TabSheet1
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 622
        Height = 398
        inherited Splitter1: TevSplitter
          Height = 398
        end
        inherited wwDBGrid1: TevDBGrid
          Height = 398
          IniAttributes.SectionName = 'TEDIT_SY_LOCAL_MARITAL_STATUS\wwDBGrid1'
        end
        inherited Panel2: TevPanel
          Width = 498
          Height = 398
          inherited splCounty: TevSplitter
            Height = 398
          end
          inherited Panel2a: TevPanel
            Width = 495
            Height = 398
            inherited wwDBGridLocal: TevDBGrid
              Width = 495
              Height = 398
              Selected.Strings = (
                'LOCAL_TYPE'#9'1'#9'Local Type'#9'F'
                'NAME'#9'40'#9'Locality Name'#9'F'
                'CountyName'#9'40'#9'County'#9'F')
              IniAttributes.SectionName = 'TEDIT_SY_LOCAL_MARITAL_STATUS\wwDBGridLocal'
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Flags'
      object Label3: TevLabel
        Left = 8
        Top = 256
        Width = 37
        Height = 13
        Caption = '~Status'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TevLabel
        Left = 8
        Top = 316
        Width = 65
        Height = 13
        Caption = '~Description'
        FocusControl = dbedtSTATUS_DESCRIPTION
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBGrid3: TevDBGrid
        Left = 8
        Top = 16
        Width = 585
        Height = 225
        DisableThemesInTitle = False
        Selected.Strings = (
          'STATUS_DESCRIPTION'#9'40'#9'Description')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_LOCAL_MARITAL_STATUS\wwDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object dbedtSTATUS_DESCRIPTION: TevDBEdit
        Left = 8
        Top = 332
        Width = 244
        Height = 21
        HelpContext = 3502
        DataField = 'STATUS_DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        ReadOnly = True
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdlStatus: TevDBLookupCombo
        Left = 8
        Top = 276
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'STATUS_TYPE'#9'2'#9'STATUS_TYPE'#9'F')
        DataField = 'SY_STATE_MARITAL_STATUS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
        LookupField = 'SY_STATE_MARITAL_STATUS_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Values'
      ImageIndex = 2
      object Label8: TevLabel
        Left = 55
        Top = 81
        Width = 147
        Height = 13
        Caption = 'Standard Exemption Allowance'
      end
      object Label6: TevLabel
        Left = 55
        Top = 44
        Width = 147
        Height = 13
        Caption = 'Standard Deduction Allowance'
      end
      object wwdeStandard_Deduct_Minimum_Amount: TevDBEdit
        Left = 208
        Top = 81
        Width = 64
        Height = 21
        HelpContext = 3513
        DataField = 'STANDARD_EXEMPTION_ALLOW'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeStandard_Deduction_Percent_Gross: TevDBEdit
        Left = 208
        Top = 44
        Width = 64
        Height = 21
        HelpContext = 3512
        DataField = 'STANDARD_DEDUCTION_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  inherited evPanel1: TevPanel
    Width = 630
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_LOCAL_MARITAL_STATUS.SY_LOCAL_MARITAL_STATUS
  end
  object evMaritalStatus: TevDataSource
    DataSet = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
    MasterDataSource = wwdsMaster
    Left = 252
    Top = 18
  end
end
