inherited EDIT_SY_FED_EXEMPTIONS: TEDIT_SY_FED_EXEMPTIONS
  Width = 620
  Height = 499
  object lablE_D_Code: TevLabel [0]
    Left = 12
    Top = 276
    Width = 56
    Height = 13
    Caption = '~E\D Type'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablW2_Box: TevLabel [1]
    Left = 412
    Top = 408
    Width = 38
    Height = 13
    Caption = 'W2 Box'
  end
  object evDBText1: TevDBText [2]
    Left = 77
    Top = 299
    Width = 124
    Height = 17
    DataField = 'EDDescription'
    DataSource = wwdsDetail
  end
  object wwDBGrid1: TevDBGrid [3]
    Left = 0
    Top = 0
    Width = 620
    Height = 253
    Selected.Strings = (
      'E_D_CODE_TYPE'#9'2'#9'E\D Code '
      'EDDescription'#9'40'#9'Description'#9'F'
      'EXEMPT_FEDERAL'#9'1'#9'Federal'
      'EXEMPT_FUI'#9'1'#9'Block FUI'
      'EXEMPT_EMPLOYEE_OASDI'#9'1'#9'Block EE OASDI'
      'EXEMPT_EMPLOYER_OASDI'#9'1'#9'Block ER OASDI'
      'EXEMPT_EMPLOYEE_MEDICARE'#9'1'#9'Block EE Medicare'
      'EXEMPT_EMPLOYER_MEDICARE'#9'1'#9'Block ER Medicare'
      'EXEMPT_EMPLOYEE_EIC'#9'1'#9'Block EE EIC')
    IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_FED_EXEMPTIONS\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alTop
    DataSource = wwdsDetail
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object drgpFederal: TevDBRadioGroup [4]
    Left = 12
    Top = 336
    Width = 185
    Height = 65
    HelpContext = 5517
    Caption = '~Federal'
    DataField = 'EXEMPT_FEDERAL'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      'Exempt'
      'Block'
      'Include')
    ParentFont = False
    TabOrder = 2
    Values.Strings = (
      'E'
      'X'
      'I')
  end
  object drgpExempt_FUI: TevDBRadioGroup [5]
    Left = 412
    Top = 344
    Width = 185
    Height = 57
    HelpContext = 5518
    Caption = '~Exempt FUI'
    DataField = 'EXEMPT_FUI'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      'Yes'
      'No')
    ParentFont = False
    TabOrder = 8
    Values.Strings = (
      'Y'
      'N')
  end
  object drgpExempt_EE_OASDI: TevDBRadioGroup [6]
    Left = 12
    Top = 408
    Width = 185
    Height = 57
    HelpContext = 5519
    Caption = '~Exempt EE OASDI'
    DataField = 'EXEMPT_EMPLOYEE_OASDI'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      'Yes'
      'No')
    ParentFont = False
    TabOrder = 3
    Values.Strings = (
      'Y'
      'N')
  end
  object drgpExempt_EE_Medicare: TevDBRadioGroup [7]
    Left = 210
    Top = 276
    Width = 185
    Height = 57
    HelpContext = 5520
    Caption = '~Exempt EE Medicare'
    DataField = 'EXEMPT_EMPLOYEE_MEDICARE'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      'Yes'
      'No')
    ParentFont = False
    TabOrder = 4
    Values.Strings = (
      'Y'
      'N')
  end
  object drgpExempt_EM_OASDI: TevDBRadioGroup [8]
    Left = 210
    Top = 408
    Width = 185
    Height = 57
    HelpContext = 5522
    Caption = '~Exempt ER OASDI'
    DataField = 'EXEMPT_EMPLOYER_OASDI'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      'Yes'
      'No')
    ParentFont = False
    TabOrder = 6
    Values.Strings = (
      'Y'
      'N')
  end
  object drgpExempt_ER_Medicare: TevDBRadioGroup [9]
    Left = 412
    Top = 276
    Width = 185
    Height = 57
    HelpContext = 5523
    Caption = '~Exempt ER Medicare'
    DataField = 'EXEMPT_EMPLOYER_MEDICARE'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      'Yes'
      'No')
    ParentFont = False
    TabOrder = 7
    Values.Strings = (
      'Y'
      'N')
  end
  object drgpExempt_EE_EIC: TevDBRadioGroup [10]
    Left = 210
    Top = 344
    Width = 185
    Height = 57
    HelpContext = 5521
    Caption = '~Exempt EE EIC'
    DataField = 'EXEMPT_EMPLOYEE_EIC'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      'Yes'
      'No')
    ParentFont = False
    TabOrder = 5
    Values.Strings = (
      'Y'
      'N')
  end
  object wwcbE_D_Code: TevDBComboDlg [11]
    Left = 16
    Top = 296
    Width = 57
    Height = 21
    HelpContext = 5516
    ShowButton = True
    Style = csDropDown
    DataField = 'E_D_CODE_TYPE'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    Picture.PictureMask = 
      '[DI,DH,DG,DA,DB,DC,DD,DE,DF,D2,D3,DL,DM,DW,DK,D7,DV,ET,EU,EV,EW,' +
      'EX,EY,E2,E9,EO,EP,EQ,M1,M2,M3,M4,M5,M6,M7,ER,ES,E4,E3,E5,E6,E1,E' +
      '8,E7,E0,D1,DN,DQ,DR,DS,DO,DT,DU,DZ,DX,DY,DJ,D4,EF,EG,EH,EI,EJ,EK' +
      ',EL,EM,EB,EA,EC,ED,EE,EZ]'
    Picture.AutoFill = False
    TabOrder = 1
    WordWrap = False
    UnboundDataType = wwDefault
  end
  object dedtW2_Box: TevDBEdit [12]
    Left = 412
    Top = 424
    Width = 97
    Height = 21
    HelpContext = 5524
    DataField = 'W2_BOX'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
    TabOrder = 9
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_FED_EXEMPTIONS.SY_FED_EXEMPTIONS
  end
  object DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL
    Left = 249
    Top = 18
  end
end
