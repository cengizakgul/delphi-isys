inherited EDIT_SY_LOCAL_EXEMPTIONS: TEDIT_SY_LOCAL_EXEMPTIONS
  inherited PageControl1: TevPageControl
    HelpContext = 4079
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 559
        Height = 208
        inherited Splitter1: TevSplitter
          Left = 181
          Height = 208
        end
        inherited wwDBGrid1: TevDBGrid
          Width = 181
          Height = 208
          Selected.Strings = (
            'STATE'#9'2'#9'State'
            'NAME'#9'20'#9'Name')
          IniAttributes.SectionName = 'TEDIT_SY_LOCAL_EXEMPTIONS\wwDBGrid1'
        end
        inherited Panel2: TevPanel
          Left = 184
          Width = 375
          Height = 208
          inherited splCounty: TevSplitter
            Height = 208
          end
          inherited Panel2a: TevPanel
            Width = 372
            Height = 208
            inherited wwDBGridLocal: TevDBGrid
              Width = 372
              Height = 208
              Selected.Strings = (
                'LOCAL_TYPE'#9'1'#9'Local Type'
                'NAME'#9'40'#9'Locality Name'#9'F'
                'CountyName'#9'40'#9'County'#9'F')
              IniAttributes.SectionName = 'TEDIT_SY_LOCAL_EXEMPTIONS\wwDBGridLocal'
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Details'
      object lablE_D_Code_Type: TevLabel
        Left = 16
        Top = 292
        Width = 89
        Height = 13
        Caption = '~E\D Code Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evDBText1: TevDBText
        Left = 77
        Top = 318
        Width = 124
        Height = 17
        DataField = 'EDDescription'
        DataSource = wwdsDetail
      end
      object drgpLocal: TevDBRadioGroup
        Left = 16
        Top = 340
        Width = 81
        Height = 77
        Caption = '~Local'
        DataField = 'EXEMPT'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Block'
          'Include')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object wwDBGrid3: TevDBGrid
        Left = 16
        Top = 12
        Width = 473
        Height = 269
        DisableThemesInTitle = False
        Selected.Strings = (
          'E_D_CODE_TYPE'#9'2'#9'E\D Code Type'
          'EDDescription'#9'40'#9'Description'#9'F'
          'EXEMPT'#9'1'#9'Exempt')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_LOCAL_EXEMPTIONS\wwDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        PopupMenu = evPopupMenu1
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object wwcdE_D_Code_Type: TevDBComboDlg
        Left = 16
        Top = 316
        Width = 57
        Height = 21
        ShowButton = True
        Style = csDropDown
        DataField = 'E_D_CODE_TYPE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = 
          '[DI,DH,DG,DA,DB,DC,DD,DE,DF,D2,D3,DL,DM,DW,DK,D7,DV,ET,EU,EV,EW,' +
          'EX,EY,E2,E9,EO,EP,EQ,M1,M2,M3,M4,M5,M6,M7,ER,ES,E4,E3,E5,E6,E1,E' +
          '8,E7,E0,D1,DN,DQ,DR,DS,DO,DT,DU,DZ,DX,DY,DJ,D4,EF,EG,EH,EI,EJ,EK' +
          ',EL,EM,EB,EA,EC,ED,EE,EZ]'
        Picture.AutoFill = False
        TabOrder = 1
        WordWrap = False
        UnboundDataType = wwDefault
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_LOCAL_EXEMPTIONS.SY_LOCAL_EXEMPTIONS
    Left = 238
  end
  inherited wwdsSubMaster2: TevDataSource
    Left = 190
    Top = 22
  end
  inherited wwdsSubMaster: TevDataSource
    Left = 371
    Top = 22
  end
  object evPopupMenu1: TevPopupMenu
    Left = 324
    Top = 170
    object CopyTo1: TMenuItem
      Action = CopyExemption
    end
  end
  object evActionList1: TevActionList
    Left = 396
    Top = 178
    object CopyExemption: TAction
      Caption = 'Copy To...'
      OnExecute = CopyExemptionExecute
      OnUpdate = CopyExemptionUpdate
    end
  end
end
