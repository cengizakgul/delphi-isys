// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_LOCALS;

interface

uses
  SDataStructure, SPD_EDIT_BASE_SY_LOCAL, wwdbdatetimepicker, 
  wwdblook, wwdbedit, Wwdotdot, Wwdbcomb, StdCtrls, ExtCtrls, DBCtrls,
  Mask, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls,
  Classes, evUtils, ISBasicClasses, SDDClasses, SDataDictsystem, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_LOCALS = class(TEDIT_BASE_SY_LOCAL)
    lablLocal_Name: TevLabel;
    lablLocal_Type: TevLabel;
    lablZip_Code: TevLabel;
    lablLocal_Tax_Identifier: TevLabel;
    lablAgency_Number: TevLabel;
    lablCalculation_Method: TevLabel;
    lablMinimum_Hours_Worked_Per: TevLabel;
    lablLocal_Minimum_Wage: TevLabel;
    lablTax_Rate: TevLabel;
    lablMisc_Amount: TevLabel;
    lablMinimum_Hours_Worked: TevLabel;
    lablWeekly_Tax_Cap: TevLabel;
    lablWage_Maximum: TevLabel;
    lablTax_Minimum: TevLabel;
    lablTax_Amount: TevLabel;
    lablSY_Tax_Pmt_Report: TevLabel;
    lablSY_Local_Reporting_Agency_Nbr: TevLabel;
    lablSY_Local_Tax_Pmt_Agency: TevLabel;
    dedtName: TevDBEdit;
    dedtLocal_Tax_Identifier: TevDBEdit;
    dedtAgency_Number: TevDBEdit;
    drgpPrint_Return_Zero: TevDBRadioGroup;
    drgpUse_Misc_Tax_Return_code: TevDBRadioGroup;
    wwDBComboBox1: TevDBComboBox;
    wwcbMinimum_Hours_Worked_Per: TevDBComboBox;
    wwcbCalculation_Method: TevDBComboBox;
    wwDBLookupCombo2: TevDBLookupCombo;
    wwlcSY_Local_Reporting_Agency_Nbr: TevDBLookupCombo;
    wwlcSY_Local_Tax_Pmt_Agency: TevDBLookupCombo;
    drgpTax_Deposit_Follow_State: TevDBRadioGroup;
    wwdeTax_Rate: TevDBEdit;
    wwdeTax_Amount: TevDBEdit;
    wwdeTax_Minimum: TevDBEdit;
    wwdeMinimum_Hours_Worked: TevDBEdit;
    wwdeWeekly_Tax_Cap: TevDBEdit;
    wwdeWage_Maximum: TevDBEdit;
    wwdeMiscellaneous_Amount: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    wwdeLocal_Tax_Identifier: TevDBEdit;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    rgActive: TevDBRadioGroup;
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel1: TevLabel;
    DBRadioGroup5: TevDBRadioGroup;
    Bevel1: TBevel;
    evDBRadioGroup1: TevDBRadioGroup;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evDBComboBox1: TevDBComboBox;
    lablE_D_Code_Type: TevLabel;
    wwcdE_D_Code_Type: TevDBComboDlg;
    evLabel4: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBRadioGroup2: TevDBRadioGroup;
    evLabel5: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel6: TevLabel;
    evDBEdit3: TevDBEdit;
    evDBRadioGroup3: TevDBRadioGroup;
    procedure TabSheet2Show(Sender: TObject);
    procedure wwcdE_D_Code_TypeCustomDlg(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_LOCALS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_LOCAL.SY_LOCALS;
end;

function TEDIT_SY_LOCALS.GetInsertControl: TWinControl;
begin
  Result := dedtName;
end;

procedure TEDIT_SY_LOCALS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS, SupportDataSets, '');
end;

procedure TEDIT_SY_LOCALS.TabSheet2Show(Sender: TObject);
begin
  inherited;
  DM_SYSTEM_LOCAL.SY_COUNTY.RetrieveCondition := 'SY_STATES_NBR='+DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsString;
  DM_SYSTEM_STATE.SY_LOCALS.Refresh;
end;

procedure TEDIT_SY_LOCALS.wwcdE_D_Code_TypeCustomDlg(Sender: TObject);
begin
  inherited;
  TevDBComboDlg(Sender).ShowCategorizedComboDialog;
end;

initialization
  RegisterClass(TEDIT_SY_LOCALS);

end.
