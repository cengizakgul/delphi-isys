inherited EDIT_SY_LOCAL_TAX_CHART: TEDIT_SY_LOCAL_TAX_CHART
  Width = 760
  Height = 478
  inherited PageControl1: TevPageControl
    Width = 760
    Height = 444
    ActivePage = TabSheet1
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 752
        Height = 416
        inherited Splitter1: TevSplitter
          Height = 416
        end
        inherited wwDBGrid1: TevDBGrid
          Height = 416
          IniAttributes.SectionName = 'TEDIT_SY_LOCAL_TAX_CHART\wwDBGrid1'
        end
        inherited Panel2: TevPanel
          Width = 628
          Height = 416
          inherited splCounty: TevSplitter
            Height = 416
          end
          inherited Panel2a: TevPanel
            Width = 625
            Height = 416
            inherited wwDBGridLocal: TevDBGrid
              Width = 625
              Height = 416
              Selected.Strings = (
                'LOCAL_TYPE'#9'1'#9'Local Type'#9'F'
                'NAME'#9'40'#9'Locality Name'#9'F')
              IniAttributes.SectionName = 'TEDIT_SY_LOCAL_TAX_CHART\wwDBGridLocal'
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      object Label3: TevLabel
        Left = 96
        Top = 144
        Width = 93
        Height = 13
        Caption = 'Local Marital Status'
      end
      object Label12: TevLabel
        Left = 328
        Top = 144
        Width = 24
        Height = 13
        Caption = 'Type'
      end
      object Label4: TevLabel
        Left = 35
        Top = 244
        Width = 49
        Height = 13
        Caption = '~Minimum'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TevLabel
        Left = 32
        Top = 276
        Width = 52
        Height = 13
        Caption = '~Maximum'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TevLabel
        Left = 74
        Top = 304
        Width = 10
        Height = 13
        Caption = '~%'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBGrid3: TevDBGrid
        Left = 12
        Top = 12
        Width = 593
        Height = 113
        DisableThemesInTitle = False
        Selected.Strings = (
          'MARITAL_STATUS_DESCRPTION'#9'20'#9'MARITAL_STATUS'
          'MINIMUM'#9'10'#9'Minimum'#9
          'MAXIMUM'#9'10'#9'Maximum'#9
          'PERCENTAGE'#9'10'#9'Percentage'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_LOCAL_TAX_CHART\wwDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object dblkpLOCAL_MARITAL_STATUS: TevDBLookupCombo
        Left = 96
        Top = 160
        Width = 185
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'STATUS_DESCRIPTION'#9'40'#9'STATUS_DESCRIPTION')
        DataField = 'SY_LOCAL_MARITAL_STATUS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_LOCAL_MARITAL_STATUS.SY_LOCAL_MARITAL_STATUS
        LookupField = 'SY_LOCAL_MARITAL_STATUS_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwDBComboBox1: TevDBComboBox
        Left = 328
        Top = 160
        Width = 153
        Height = 21
        ShowButton = True
        Style = csDropDown
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'LOCALTYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object wwdeMinimum: TevDBEdit
        Left = 96
        Top = 240
        Width = 64
        Height = 21
        DataField = 'MINIMUM'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeMaximum: TevDBEdit
        Left = 96
        Top = 272
        Width = 64
        Height = 21
        DataField = 'MAXIMUM'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdePercent: TevDBEdit
        Left = 96
        Top = 304
        Width = 64
        Height = 21
        DataField = 'PERCENTAGE'
        DataSource = wwdsDetail
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  inherited evPanel1: TevPanel
    Width = 760
    inherited DBText2: TevDBText
      DataField = 'STATE'
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_LOCAL_TAX_CHART.SY_LOCAL_TAX_CHART
    MasterDataSource = CloneLocal
  end
  object CloneLocal: TevDataSource
    DataSet = DM_SY_LOCALS.SY_LOCALS
    MasterDataSource = wwdsSubMaster
    Left = 215
    Top = 85
  end
  object evMaritalStatus: TevDataSource
    DataSet = DM_SY_LOCAL_MARITAL_STATUS.SY_LOCAL_MARITAL_STATUS
    MasterDataSource = wwdsSubMaster2
    Left = 249
    Top = 15
  end
end
