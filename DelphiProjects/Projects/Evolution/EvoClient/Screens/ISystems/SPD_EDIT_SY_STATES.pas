// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_STATES;

interface

uses
  SDataStructure,  SPD_EDIT_SY_STATE_BASE, wwdbedit, Wwdotdot,
  Wwdbcomb, wwdbdatetimepicker, Mask, DBCtrls, StdCtrls, wwdblook,
  ExtCtrls, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls,
  Classes, ISBasicClasses, SDDClasses, Forms,
  SPD_MiniNavigationFrame, kbmMemTable, ISKbmMemDataSet,
  EvDataAccessComponents, ISDataAccessComponents, SDataDictsystem, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBEdit, isUIwwDBLookupCombo;

type
  TEDIT_SY_STATES = class(TEDIT_SY_STATE_BASE)
    Label9: TevLabel;
    Label8: TevLabel;
    TabSheet2x: TTabSheet;
    TabSheet3: TTabSheet;
    Label19: TevLabel;
    Label22: TevLabel;
    Label25: TevLabel;
    Label28: TevLabel;
    Label31: TevLabel;
    TabSheet4: TTabSheet;
    Label36: TevLabel;
    Label39: TevLabel;
    TabSheet6: TTabSheet;
    Label51: TevLabel;
    DBRadioGroup3: TevDBRadioGroup;
    DBRadioGroup4: TevDBRadioGroup;
    Label42: TevLabel;
    DBEdit40: TevDBEdit;
    Label43: TevLabel;
    DBEdit41: TevDBEdit;
    Label44: TevLabel;
    DBEdit42: TevDBEdit;
    Label45: TevLabel;
    DBEdit43: TevDBEdit;
    Label46: TevLabel;
    DBEdit44: TevDBEdit;
    Label47: TevLabel;
    DBEdit45: TevDBEdit;
    DBRadioGroup9: TevDBRadioGroup;
    DBRadioGroup10: TevDBRadioGroup;
    DBRadioGroup11: TevDBRadioGroup;
    TabSheet7: TTabSheet;
    DBRadioGroup8: TevDBRadioGroup;
    DBRadioGroup7: TevDBRadioGroup;
    wwDBComboBox1: TevDBComboBox;
    Label1x: TevLabel;
    Label7: TevLabel;
    wwDBComboBox2: TevDBComboBox;
    wwDBComboBox3: TevDBComboBox;
    Label12: TevLabel;
    Label14: TevLabel;
    wwDBComboBox4: TevDBComboBox;
    DBRadioGroup1: TevDBRadioGroup;
    Label15: TevLabel;
    wwDBLookupCombo2: TevDBLookupCombo;
    wwDBLookupCombo1: TevDBLookupCombo;
    Label17: TevLabel;
    wwDBLookupCombo3: TevDBLookupCombo;
    Label18: TevLabel;
    DBRadioGroup2: TevDBRadioGroup;
    Label2x: TevLabel;
    dbedtSTATE: TevDBEdit;
    Label3: TevLabel;
    DBEdit3: TevDBEdit;
    wwdeState_Minimum_Wage: TevDBEdit;
    wwdeSupplemental_Wages_Percent: TevDBEdit;
    wwdeState_Tip_Credit: TevDBEdit;
    wwdeSDI_Maximum_Weekly: TevDBEdit;
    wwdeEE_SDI_Rate: TevDBEdit;
    wwdeER_SDI_Rate: TevDBEdit;
    wwdeER_SDI_Maximum_Wage: TevDBEdit;
    wwdeEE_SDI_Maximum_Wage: TevDBEdit;
    wwdeGarnish_Maximum_Percent: TevDBEdit;
    wwdeGarnish_Min_Wage_Multiplier: TevDBEdit;
    wwdeSales_Tax_Percent: TevDBEdit;
    DBRadioGroup5: TevDBRadioGroup;
    Label2: TevLabel;
    DBRadioGroup6: TevDBRadioGroup;
    wwDBEdit1: TevDBEdit;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evDBEdit1: TevDBEdit;
    evLabel1: TevLabel;
    evDBRadioGroup1: TevDBRadioGroup;
    tsReciprocation: TTabSheet;
    grRecStates: TevDBGrid;
    mnfRecStates: TMiniNavigationFrame;
    dsRecStates: TevDataSource;
    cbStateList: TevDBLookupCombo;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evDBComboBox1: TevDBComboBox;
    dsStates: TevClientDataSet;
    Label4: TevLabel;
    evDBRadioGroup2: TevDBRadioGroup;
    evDBRadioGroup3: TevDBRadioGroup;
    procedure dsRecStatesStateChange(Sender: TObject);
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
    procedure mnfRecStatesSpeedButton1Click(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.DFM}
 uses EvUtils;
procedure TEDIT_SY_STATES.Activate;
begin
  inherited;
  dsStates.CloneCursor(DM_SYSTEM_MISC.SY_STATES, True);
  mnfRecStates.DataSource := dsRecStates;
  mnfRecStates.InsertFocusControl := cbStateList;
end;

procedure TEDIT_SY_STATES.Deactivate;
begin
  inherited;
  dsRecStates.DataSet.Filter := '';
  dsRecStates.DataSet.Filtered := False;
end;


function TEDIT_SY_STATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_STATE.SY_STATES;
end;

function TEDIT_SY_STATES.GetInsertControl: TWinControl;
begin
  Result := dbedtSTATE;
end;

procedure TEDIT_SY_STATES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_RECIPROCATED_STATES, EditDataSets, '');
end;

procedure TEDIT_SY_STATES.dsRecStatesStateChange(Sender: TObject);
begin
  inherited;
  if dsRecStates.State = dsInsert then
    DM_SYSTEM_MISC.SY_RECIPROCATED_STATES.MAIN_STATE_NBR.AsInteger := DM_SYSTEM_STATE.SY_STATES.SY_STATES_NBR.AsInteger;
end;

procedure TEDIT_SY_STATES.wwdsMasterDataChange(Sender: TObject; Field: TField);
var
  h: String;
begin
  inherited;
  if DM_SYSTEM_STATE.SY_STATES.SY_STATES_NBR.IsNull then
    exit;
  h := 'MAIN_STATE_NBR = ' + DM_SYSTEM_STATE.SY_STATES.SY_STATES_NBR.AsString;
  if Assigned(dsRecStates.DataSet) and (h <> dsRecStates.DataSet.Filter) then
  begin
    dsRecStates.DataSet.Filtered := False;
    dsRecStates.DataSet.Filter := h;
    dsRecStates.DataSet.Filtered := True;
  end;
end;

procedure TEDIT_SY_STATES.mnfRecStatesSpeedButton1Click(Sender: TObject);
begin
  inherited;
  if TDataSet(DM_SYSTEM_STATE.SY_STATES).State <> dsInsert then
    mnfRecStates.InsertRecordExecute(Sender)
  else
    EvMessage('Please,post before add new value!');
end;

initialization
  RegisterClass(TEDIT_SY_STATES);

end.
