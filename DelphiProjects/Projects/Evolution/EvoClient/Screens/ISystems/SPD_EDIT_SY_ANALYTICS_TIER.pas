// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_ANALYTICS_TIER;

interface

uses
  SFrameEntry,  SDataStructure, DBCtrls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet, SDDClasses,
  SDataDictsystem, isUIwwDBEdit, ISBasicClasses, wwdblook,
  isUIwwDBLookupCombo, EvUtils, Wwdbcomb, isUIwwDBComboBox;

type
  TEDIT_SY_ANALYTICS_TIER = class(TFrameEntry)
    wwDBGrid1: TevDBGrid;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    Label14: TevLabel;
    cbAgencyType: TevDBRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    grbxABA_Information: TevGroupBox;
    lablABA_Nbr: TevLabel;
    lablTop_ABA_Nbr: TevLabel;
    wwdeABA_Nbr: TevDBEdit;
    wwdeTop_ABA_Nbr: TevDBEdit;
    evGroupBox1: TevGroupBox;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBEdit2: TevDBEdit;
    evGroupBox2: TevGroupBox;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evDBEdit3: TevDBEdit;
    evDBEdit4: TevDBEdit;
    edtLevel: TevDBEdit;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    { Public declarations }
  end;

implementation

{$R *.DFM}

function TEDIT_SY_ANALYTICS_TIER.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_ANALYTICS_TIER;
end;

function TEDIT_SY_ANALYTICS_TIER.GetInsertControl: TWinControl;
begin
  Result := edtLevel;
end;

initialization
  RegisterClass(TEDIT_SY_ANALYTICS_TIER);

end.
