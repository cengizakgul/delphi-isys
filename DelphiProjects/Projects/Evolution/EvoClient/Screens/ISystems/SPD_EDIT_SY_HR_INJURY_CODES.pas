// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_HR_INJURY_CODES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_BASE_SY_HR, Db, Wwdatsrc,  StdCtrls, Mask,
  wwdbedit, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, SDataStructure, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_HR_INJURY_CODES = class(TEDIT_BASE_SY_HR)
    evLabel2: TevLabel;
    evDBEdit1: TevDBEdit;
  private
    { Private declarations }
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  end;

implementation

{$R *.DFM}

{ TEDIT_SY_HR_INJURY_CODES }

function TEDIT_SY_HR_INJURY_CODES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_HR.SY_HR_INJURY_CODES;
end;


initialization
  RegisterClass(TEDIT_SY_HR_INJURY_CODES);

end.
