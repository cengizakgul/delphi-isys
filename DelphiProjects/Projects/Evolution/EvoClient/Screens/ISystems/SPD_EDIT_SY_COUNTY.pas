// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_COUNTY;

interface
 
uses
  SPD_EDIT_SY_STATE_BASE,
  SDataStructure,  StdCtrls, Mask, DBCtrls, Db, Wwdatsrc,
  Grids, Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, Classes,
  wwdbedit, EvUtils, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_COUNTY = class(TEDIT_SY_STATE_BASE)
    wwDBGrid2: TevDBGrid;
    wwDBGrid3: TevDBGrid;
    lablCounty_Name: TevLabel;
    dedtCounty_Name: TevDBEdit;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    evDBGrid1: TevDBGrid;
    evDataSource1: TevDataSource;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_COUNTY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_LOCAL.SY_COUNTY;
end;

function TEDIT_SY_COUNTY.GetInsertControl: TWinControl;
begin
  Result := dedtCounty_Name;
end;

procedure TEDIT_SY_COUNTY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_LOCAL.SY_LOCALS, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_COUNTY);

end.
