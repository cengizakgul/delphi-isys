inherited EDIT_SY_LOCALS: TEDIT_SY_LOCALS
  object evLabel2: TevLabel [0]
    Left = 180
    Top = 308
    Width = 64
    Height = 13
    Caption = '~Local Type'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 764
        Height = 475
        inherited Splitter1: TevSplitter
          Left = 181
          Height = 475
        end
        inherited wwDBGrid1: TevDBGrid
          Width = 181
          Height = 475
          Selected.Strings = (
            'STATE'#9'2'#9'State'
            'NAME'#9'20'#9'Name')
          IniAttributes.SectionName = 'TEDIT_SY_LOCALS\wwDBGrid1'
        end
        inherited Panel2: TevPanel
          Left = 184
          Width = 580
          Height = 475
          inherited splCounty: TevSplitter
            Height = 475
          end
          inherited Panel2a: TevPanel
            Width = 577
            Height = 475
            inherited wwDBGridLocal: TevDBGrid
              Width = 577
              Height = 475
              Selected.Strings = (
                'LOCAL_TYPE'#9'1'#9'Local Type'#9'F'
                'NAME'#9'40'#9'Name'#9'F'
                'LocalTypeDescription'#9'20'#9'Description'#9'F'
                'CountyName'#9'40'#9'County'#9'F')
              IniAttributes.SectionName = 'TEDIT_SY_LOCALS\wwDBGridLocal'
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      OnShow = TabSheet2Show
      object Bevel1: TBevel
        Left = 328
        Top = 152
        Width = 313
        Height = 153
      end
      object lablLocal_Name: TevLabel
        Left = 12
        Top = 8
        Width = 33
        Height = 13
        Caption = '~Name'
        FocusControl = dedtName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablLocal_Type: TevLabel
        Left = 12
        Top = 52
        Width = 64
        Height = 13
        Caption = '~Local Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablZip_Code: TevLabel
        Left = 240
        Top = 96
        Width = 43
        Height = 13
        Caption = 'Zip Code'
      end
      object lablLocal_Tax_Identifier: TevLabel
        Left = 12
        Top = 144
        Width = 90
        Height = 13
        Caption = 'Local Tax Identifier'
        FocusControl = dedtLocal_Tax_Identifier
      end
      object lablAgency_Number: TevLabel
        Left = 12
        Top = 192
        Width = 90
        Height = 13
        Caption = '~Agency Number'
        FocusControl = dedtAgency_Number
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablCalculation_Method: TevLabel
        Left = 12
        Top = 288
        Width = 157
        Height = 13
        Caption = '~Calculation Method'
        FocusControl = wwcbCalculation_Method
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablMinimum_Hours_Worked_Per: TevLabel
        Left = 12
        Top = 240
        Width = 125
        Height = 13
        Caption = 'Minimum Hours/Wage Per'
      end
      object lablLocal_Minimum_Wage: TevLabel
        Left = 124
        Top = 192
        Width = 102
        Height = 13
        Caption = 'Local Minimum Wage'
      end
      object lablTax_Rate: TevLabel
        Left = 340
        Top = 160
        Width = 44
        Height = 13
        Caption = 'Tax Rate'
      end
      object lablMisc_Amount: TevLabel
        Left = 340
        Top = 254
        Width = 61
        Height = 13
        Caption = 'Misc Amount'
      end
      object lablMinimum_Hours_Worked: TevLabel
        Left = 514
        Top = 208
        Width = 113
        Height = 13
        Caption = 'Minimum Hours Worked'
      end
      object lablWeekly_Tax_Cap: TevLabel
        Left = 427
        Top = 208
        Width = 79
        Height = 13
        Caption = 'Weekly Tax Cap'
      end
      object lablWage_Maximum: TevLabel
        Left = 340
        Top = 208
        Width = 76
        Height = 13
        Caption = 'Wage Maximum'
      end
      object lablTax_Minimum: TevLabel
        Left = 426
        Top = 160
        Width = 65
        Height = 13
        Caption = 'Tax Maximum'
      end
      object lablTax_Amount: TevLabel
        Left = 651
        Top = 160
        Width = 57
        Height = 13
        Caption = 'Tax Amount'
        Visible = False
      end
      object lablSY_Tax_Pmt_Report: TevLabel
        Left = 332
        Top = 99
        Width = 116
        Height = 13
        Caption = '~Tax Payment Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablSY_Local_Reporting_Agency_Nbr: TevLabel
        Left = 332
        Top = 10
        Width = 137
        Height = 13
        Caption = '~Local Reporting Agency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablSY_Local_Tax_Pmt_Agency: TevLabel
        Left = 332
        Top = 55
        Width = 155
        Height = 13
        Caption = '~Local Tax Payment Agency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel1: TevLabel
        Left = 12
        Top = 96
        Width = 33
        Height = 13
        Caption = 'County'
      end
      object evLabel3: TevLabel
        Left = 164
        Top = 240
        Width = 125
        Height = 13
        Caption = '~Local Tax Frequency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablE_D_Code_Type: TevLabel
        Left = 223
        Top = 288
        Width = 52
        Height = 13
        Caption = 'Code Type'
        FocusControl = wwcdE_D_Code_Type
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel4: TevLabel
        Left = 426
        Top = 254
        Width = 84
        Height = 13
        Caption = 'Qtr Min Threshold'
      end
      object evLabel5: TevLabel
        Left = 516
        Top = 254
        Width = 73
        Height = 13
        Caption = 'Wage Minimum'
      end
      object evLabel6: TevLabel
        Left = 148
        Top = 144
        Width = 70
        Height = 13
        Caption = 'Agency Code  '
        FocusControl = evDBEdit3
      end
      object dedtName: TevDBEdit
        Left = 12
        Top = 24
        Width = 244
        Height = 21
        HelpContext = 4059
        DataField = 'NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtLocal_Tax_Identifier: TevDBEdit
        Left = 12
        Top = 160
        Width = 124
        Height = 21
        HelpContext = 4063
        DataField = 'LOCAL_TAX_IDENTIFIER'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtAgency_Number: TevDBEdit
        Left = 12
        Top = 208
        Width = 64
        Height = 21
        HelpContext = 4064
        DataField = 'AGENCY_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object drgpPrint_Return_Zero: TevDBRadioGroup
        Left = 317
        Top = 336
        Width = 181
        Height = 49
        HelpContext = 4068
        Caption = '~Follow State E/D Exemptions'
        DataField = 'PRINT_RETURN_IF_ZERO'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 20
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpUse_Misc_Tax_Return_code: TevDBRadioGroup
        Left = 176
        Top = 336
        Width = 132
        Height = 49
        HelpContext = 4069
        Caption = '~Pay With State W/H'
        DataField = 'USE_MISC_TAX_RETURN_CODE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 9
        Values.Strings = (
          'Y'
          'N')
      end
      object wwDBComboBox1: TevDBComboBox
        Left = 12
        Top = 68
        Width = 150
        Height = 21
        HelpContext = 4060
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = True
        AutoDropDown = True
        DataField = 'LOCAL_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'School'#9'S'
          'Occupational'#9'C'
          'Income Residential'#9'R'
          'Income Non-Residential'#9'N'
          'Other'#9'O')
        ItemIndex = 2
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object wwcbMinimum_Hours_Worked_Per: TevDBComboBox
        Left = 12
        Top = 256
        Width = 141
        Height = 21
        HelpContext = 4066
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = True
        AutoDropDown = True
        DataField = 'MINIMUM_HOURS_WORKED_PER'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Week'#9'W'
          'Month'#9'M'
          'Quarter'#9'Q'
          'Annual'#9'A')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object wwcbCalculation_Method: TevDBComboBox
        Left = 12
        Top = 304
        Width = 193
        Height = 21
        HelpContext = 4067
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = True
        AutoDropDown = True
        DataField = 'CALCULATION_METHOD'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Flat'#9'F'
          'Tax Tables'#9'X'
          'Tax Tables Reversed'#9'E'
          '% of Gross'#9'G'
          '% of Gross Tax Adj by Misc Amount'#9'J'
          '% of State Tax'#9'T'
          '% of Medicare Wages'#9'I')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object wwDBLookupCombo2: TevDBLookupCombo
        Left = 332
        Top = 116
        Width = 273
        Height = 21
        HelpContext = 40084
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'DESCRIPTION')
        DataField = 'SY_TAX_PMT_REPORT_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS.SY_REPORTS
        LookupField = 'SY_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 12
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwlcSY_Local_Reporting_Agency_Nbr: TevDBLookupCombo
        Left = 332
        Top = 26
        Width = 273
        Height = 21
        HelpContext = 4082
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
        DataField = 'SY_LOCAL_REPORTING_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
        LookupField = 'SY_GLOBAL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 10
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwlcSY_Local_Tax_Pmt_Agency: TevDBLookupCombo
        Left = 332
        Top = 72
        Width = 273
        Height = 21
        HelpContext = 4083
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
        DataField = 'SY_LOCAL_TAX_PMT_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
        LookupField = 'SY_GLOBAL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 11
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object drgpTax_Deposit_Follow_State: TevDBRadioGroup
        Left = 11
        Top = 336
        Width = 156
        Height = 49
        HelpContext = 4080
        Caption = '~Tax Deposit Follow State'
        DataField = 'TAX_DEPOSIT_FOLLOW_STATE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 8
        Values.Strings = (
          'Y'
          'N')
      end
      object wwdeTax_Rate: TevDBEdit
        Left = 339
        Top = 176
        Width = 64
        Height = 21
        HelpContext = 4070
        DataField = 'TAX_RATE'
        DataSource = wwdsDetail
        Picture.PictureMask = '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*8[#]]},.#*7[#]}'
        TabOrder = 13
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeTax_Amount: TevDBEdit
        Left = 651
        Top = 176
        Width = 64
        Height = 21
        HelpContext = 4071
        DataField = 'TAX_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'
        TabOrder = 14
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object wwdeTax_Minimum: TevDBEdit
        Left = 427
        Top = 176
        Width = 64
        Height = 21
        HelpContext = 4072
        DataField = 'TAX_MAXIMUM'
        DataSource = wwdsDetail
        Picture.PictureMask = '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'
        TabOrder = 15
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeMinimum_Hours_Worked: TevDBEdit
        Left = 515
        Top = 224
        Width = 64
        Height = 21
        HelpContext = 4075
        DataField = 'MINIMUM_HOURS_WORKED'
        DataSource = wwdsDetail
        Picture.PictureMask = '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'
        TabOrder = 18
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeWeekly_Tax_Cap: TevDBEdit
        Left = 427
        Top = 224
        Width = 64
        Height = 21
        HelpContext = 4074
        DataField = 'WEEKLY_TAX_CAP'
        DataSource = wwdsDetail
        Picture.PictureMask = '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'
        TabOrder = 17
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeWage_Maximum: TevDBEdit
        Left = 339
        Top = 224
        Width = 64
        Height = 21
        HelpContext = 4073
        DataField = 'WAGE_MAXIMUM'
        DataSource = wwdsDetail
        Picture.PictureMask = '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'
        TabOrder = 16
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeMiscellaneous_Amount: TevDBEdit
        Left = 339
        Top = 272
        Width = 64
        Height = 21
        HelpContext = 4076
        DataField = 'MISCELLANEOUS_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'
        TabOrder = 19
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeZip_Code: TevDBEdit
        Left = 240
        Top = 112
        Width = 64
        Height = 21
        HelpContext = 4061
        DataField = 'ZIP_CODE'
        DataSource = wwdsDetail
        Picture.PictureMask = '*5{#}[-*4#]'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeLocal_Tax_Identifier: TevDBEdit
        Left = 123
        Top = 208
        Width = 64
        Height = 21
        HelpContext = 4065
        DataField = 'LOCAL_MINIMUM_WAGE'
        DataSource = wwdsDetail
        Picture.PictureMask = '[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object rgActive: TevDBRadioGroup
        Left = 250
        Top = 180
        Width = 54
        Height = 49
        HelpContext = 4069
        Caption = '~Active'
        DataField = 'LOCAL_ACTIVE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 21
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBLookupCombo1: TevDBLookupCombo
        Left = 12
        Top = 112
        Width = 213
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'COUNTY_NAME'#9'40'#9'COUNTY_NAME'#9'F')
        DataField = 'SY_COUNTY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_COUNTY.SY_COUNTY
        LookupField = 'SY_COUNTY_NBR'
        Style = csDropDownList
        TabOrder = 22
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object DBRadioGroup5: TevDBRadioGroup
        Left = 508
        Top = 336
        Width = 173
        Height = 49
        HelpContext = 1573
        Caption = '~Round to the Nearest Dollar'
        DataField = 'ROUND_TO_NEAREST_DOLLAR'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 23
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 173
        Top = 46
        Width = 64
        Height = 48
        HelpContext = 4069
        Caption = '~EE/ER'
        DataField = 'TAX_TYPE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'EE'
          'ER')
        ParentFont = False
        TabOrder = 24
        Values.Strings = (
          'E'
          'R')
      end
      object evDBComboBox1: TevDBComboBox
        Left = 164
        Top = 256
        Width = 133
        Height = 21
        HelpContext = 4067
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = True
        AutoDropDown = True
        DataField = 'TAX_FREQUENCY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Flat'#9'F'
          'Tax Tables'#9'X'
          'Tax Tables Reversed'#9'E'
          '% of Gross'#9'G'
          '% of Gross Tax Adj by Misc Amount'#9'J'
          '% of State Tax'#9'T')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 25
        UnboundDataType = wwDefault
      end
      object wwcdE_D_Code_Type: TevDBComboDlg
        Left = 223
        Top = 304
        Width = 73
        Height = 21
        HelpContext = 8124
        OnCustomDlg = wwcdE_D_Code_TypeCustomDlg
        ShowButton = True
        Style = csDropDown
        DataField = 'E_D_CODE_TYPE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = 
          '[DI,DH,DG,DA,DB,DC,DD,DE,DF,DL,DM,DW,DK,D7,DV,ET,EU,EV,EW,EX,EO,' +
          'EP,EQ,M1,M2,M3,M4,M5,M6,ER,ES,E4,E3,E5,E6,E1,E8,E7,D1,DN,DQ,DP,D' +
          'R,DS,DO,DT,DU,DZ,EF,EG,EH,EI,EJ,EK,EL,EM,EB,EN,EA,EC,ED,EE]'
        Picture.AutoFill = False
        TabOrder = 26
        WordWrap = False
        UnboundDataType = wwDefault
      end
      object evDBEdit1: TevDBEdit
        Left = 427
        Top = 272
        Width = 64
        Height = 21
        HelpContext = 4076
        DataField = 'QUARTERLY_MINIMUM_THRESHOLD'
        DataSource = wwdsDetail
        TabOrder = 27
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBRadioGroup2: TevDBRadioGroup
        Left = 504
        Top = 160
        Width = 129
        Height = 41
        HelpContext = 4069
        Caption = '~Monthly Maximums'
        DataField = 'PAY_WITH_STATE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 28
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBEdit2: TevDBEdit
        Left = 515
        Top = 272
        Width = 64
        Height = 21
        HelpContext = 4073
        DataField = 'WAGE_MINIMUM'
        DataSource = wwdsDetail
        TabOrder = 29
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBEdit3: TevDBEdit
        Left = 148
        Top = 160
        Width = 102
        Height = 21
        HelpContext = 4063
        DataField = 'AGENCY_CODE'
        DataSource = wwdsDetail
        TabOrder = 30
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBRadioGroup3: TevDBRadioGroup
        Left = 244
        Top = 46
        Width = 64
        Height = 48
        HelpContext = 4069
        Caption = '~Act 32'
        DataField = 'COMBINE_FOR_TAX_PAYMENTS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 31
        Values.Strings = (
          'Y'
          'N')
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_LOCALS.SY_LOCALS
    MasterDataSource = nil
  end
  inherited wwdsList: TevDataSource
    Left = 92
    Top = 26
  end
  inherited DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 275
    Top = 19
  end
  inherited wwdsSubMaster2: TevDataSource
    Left = 222
    Top = 22
  end
  inherited wwdsSubMaster: TevDataSource
    MasterDataSource = wwdsMaster
    Left = 171
    Top = 22
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 354
    Top = 21
  end
end
