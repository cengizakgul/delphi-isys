// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_GLOBAL_AGENCY;

interface

uses
   SFrameEntry, wwdblook, wwdbedit, Wwdotdot, Wwdbcomb,
  StdCtrls, ExtCtrls, DBCtrls, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, Classes, Db, Wwdatsrc, SDataStructure, SysUtils, Variants,
  SDDClasses, ISBasicClasses, SDataDictsystem, EvExceptions, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_GLOBAL_AGENCY = class(TFrameEntry)
    pcMain: TevPageControl;
    tsBrowse: TTabSheet;
    tsDetail: TTabSheet;
    wwDBGrid1: TevDBGrid;
    Splitter1: TevSplitter;
    DBEdit1: TevDBEdit;
    DBEdit2: TevDBEdit;
    DBEdit3: TevDBEdit;
    DBEdit4: TevDBEdit;
    wwdeCounty: TevDBEdit;
    DBRadioGroup7: TevDBRadioGroup;
    DBRadioGroup6: TevDBRadioGroup;
    DBRadioGroup4: TevDBRadioGroup;
    DBRadioGroup5: TevDBRadioGroup;
    DBRadioGroup1: TevDBRadioGroup;
    dedtAgency_Name: TevDBEdit;
    wwDBComboBox1: TevDBComboBox;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    Label4: TevLabel;
    Label20: TevLabel;
    Label5: TevLabel;
    Label6: TevLabel;
    Label7: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evLabel1: TevLabel;
    EftEndDateMethod: TevDBComboBox;
    evLabel2: TevLabel;
    EvDBMemo1: TEvDBMemo;
    tsEftDebit: TTabSheet;
    rgDebitPostToBankAccount: TevDBRadioGroup;
    cbluDebitPaymentReport: TevDBLookupCombo;
    evLabel3: TevLabel;
    cbDebitStatus: TevDBComboBox;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    LocalEftEndDateMethod: TevDBComboBox;
    rgWeekendRule: TRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBRadioGroup3: TevDBRadioGroup;
    cbAgencyType: TevDBRadioGroup;
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure EftEndDateMethodChange(Sender: TObject);
    procedure LocalEftEndDateMethodChange(Sender: TObject);
    procedure rgWeekendRuleClick(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

uses EvTypes, EvUtils, SFieldCodeValues, EvConsts;
{$R *.DFM}

function TEDIT_SY_GLOBAL_AGENCY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_GLOBAL_AGENCY;
end;

function TEDIT_SY_GLOBAL_AGENCY.GetInsertControl: TWinControl;
begin
  Result := dedtAgency_Name;
end;

procedure TEDIT_SY_GLOBAL_AGENCY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS, SupportDataSets, '');
end;

//SEG BEGIN
procedure TEDIT_SY_GLOBAL_AGENCY.wwdsDetailUpdateData(Sender: TObject);
var
  L : integer;
begin
  inherited;

  with TevDataSource(Sender) do
  begin
    L := Length(VarToStr(DataSet['RECEIVING_ABA_NUMBER']));
    if (L <> 9) and (L <> 0) then
      raise EUpdateError.CreateHelp('The ABA Number must have 9 digits', IDH_ConsistencyViolation);
  end;

  with TevDataSource(Sender) do
    if (VarToStr(DataSet['RECEIVING_ABA_NUMBER']) <> '') and
       (HashTotalABAOk(VarToStr(DataSet['RECEIVING_ABA_NUMBER'])) <> 0) then
      raise EInvalidHashTotal.CreateHelp('Invalid Hash Total of EE_ABA_NUMBER', IDH_ConsistencyViolation);
end;
//SEG END

procedure TEDIT_SY_GLOBAL_AGENCY.Activate;
begin
  inherited;
  EftEndDateMethod.Items.Text := EftEndDateCalcMethod_ComboChoices;
  LocalEftEndDateMethod.Items.Text := EftEndDateCalcMethod_ComboChoices;

end;

procedure TEDIT_SY_GLOBAL_AGENCY.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
var
  s: string;
begin
  inherited;
  if not Assigned(Field) and (EftEndDateMethod.Tag = 0) then
  begin
    EftEndDateMethod.Tag := 1;
    try
      s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 1);
      if (s <> '') and (ReturnDescription(EftEndDateCalcMethod_ComboChoices, s) <> '') then
      begin
        EftEndDateMethod.Clear;
        EftEndDateMethod.Value := s;
      end
      else
        EftEndDateMethod.Clear;
    finally
      EftEndDateMethod.Tag := 0;
    end;
  end;
  if not Assigned(Field) and (LocalEftEndDateMethod.Tag = 0) then
  begin
    LocalEftEndDateMethod.Tag := 1;
    try
      s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 2, 1);
      if (s <> '') and (ReturnDescription(EftEndDateCalcMethod_ComboChoices, s) <> '') then
      begin
        LocalEftEndDateMethod.Clear;
        LocalEftEndDateMethod.Value := s;
      end
      else
        LocalEftEndDateMethod.Clear;
    finally
      LocalEftEndDateMethod.Tag := 0;
    end;
  end;

  if not Assigned(Field) and (rgWeekendRule.Tag = 0) then
  begin
    rgWeekendRule.Tag := 1;
    try
      s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 3, 1);
      rgWeekendRule.ItemIndex := 1;
      if (s = HOLIDAY_RULE_BEFORE) then
        rgWeekendRule.ItemIndex := 0;
    finally
      rgWeekendRule.Tag := 0;
    end;
  end;


end;


procedure TEDIT_SY_GLOBAL_AGENCY.rgWeekendRuleClick(Sender: TObject);
begin
  inherited;

  if rgWeekendRule.Tag = 0 then
  begin
    rgWeekendRule.Tag := 1;
    try
      wwdsDetail.Edit;
      if rgWeekendRule.ItemIndex = 0 then
        wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, HOLIDAY_RULE_BEFORE, 3, 1)
      else
        wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, HOLIDAY_RULE_AFTER, 3, 1)
    finally
      rgWeekendRule.Tag := 0;
    end;
  end;
end;


procedure TEDIT_SY_GLOBAL_AGENCY.EftEndDateMethodChange(Sender: TObject);
begin
  inherited;
  if EftEndDateMethod.Tag = 0 then
  begin
    EftEndDateMethod.Tag := 1;
    try
      wwdsDetail.Edit;
      wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, EftEndDateMethod.Value, 1, 1);
    finally
      EftEndDateMethod.Tag := 0;
    end;
  end;
end;

procedure TEDIT_SY_GLOBAL_AGENCY.LocalEftEndDateMethodChange(
  Sender: TObject);
begin
  inherited;
  if LocalEftEndDateMethod.Tag = 0 then
  begin
    LocalEftEndDateMethod.Tag := 1;
    try
      wwdsDetail.Edit;
      wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, EftEndDateMethod.Value, 2, 1);
    finally
      LocalEftEndDateMethod.Tag := 0;
    end;
  end;

end;













initialization
  RegisterClass(TEDIT_SY_GLOBAL_AGENCY);

end.
