inherited EDIT_SY_LOCAL_DEPOSIT_FREQ: TEDIT_SY_LOCAL_DEPOSIT_FREQ
  Width = 847
  Height = 605
  inherited PageControl1: TevPageControl
    Width = 847
    Height = 571
    HelpContext = 53002
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 611
        Height = 380
        inherited Splitter1: TevSplitter
          Height = 380
        end
        inherited wwDBGrid1: TevDBGrid
          Height = 380
          Selected.Strings = (
            'STATE'#9'2'#9'State'
            'NAME'#9'20'#9'Name')
          IniAttributes.SectionName = 'TEDIT_SY_LOCAL_DEPOSIT_FREQ\wwDBGrid1'
        end
        inherited Panel2: TevPanel
          Width = 487
          Height = 380
          inherited splCounty: TevSplitter
            Height = 380
          end
          inherited Panel2a: TevPanel
            Width = 484
            Height = 380
            inherited wwDBGridLocal: TevDBGrid
              Width = 484
              Height = 380
              Selected.Strings = (
                'LOCAL_TYPE'#9'1'#9'Local type'
                'NAME'#9'40'#9'Locality Name'
                'CountyName'#9'40'#9'County'#9'F')
              IniAttributes.SectionName = 'TEDIT_SY_LOCAL_DEPOSIT_FREQ\wwDBGridLocal'
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Details'
      object lablType: TevLabel
        Left = 210
        Top = 148
        Width = 33
        Height = 16
        Caption = '~Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablWhen_Due_Type: TevLabel
        Left = 360
        Top = 148
        Width = 88
        Height = 16
        Caption = '~When Due Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablNbr_Days_For_When_Due: TevLabel
        Left = 556
        Top = 148
        Width = 138
        Height = 16
        Caption = '~Nbr of Days For When Due'
        FocusControl = dedtNbr_Days_For_When_Due
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablDescription: TevLabel
        Left = 8
        Top = 148
        Width = 62
        Height = 16
        Caption = '~Description'
        FocusControl = dedtDescription
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel2: TevLabel
        Left = 418
        Top = 338
        Width = 100
        Height = 13
        Caption = 'Tax payment coupon'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object Label5: TevLabel
        Left = 8
        Top = 239
        Width = 117
        Height = 13
        Caption = 'Tax Payment Type Code'
      end
      object Label10: TevLabel
        Left = 8
        Top = 196
        Width = 111
        Height = 16
        Caption = '~First Threshold Period'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TevLabel
        Left = 210
        Top = 196
        Width = 117
        Height = 16
        Caption = '~First Threshold Amount'
        FocusControl = edFirstTAmount
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TevLabel
        Left = 360
        Top = 196
        Width = 122
        Height = 13
        Caption = 'First Threshold Frequency'
      end
      object evLabel8: TevLabel
        Left = 12
        Top = 392
        Width = 204
        Height = 13
        Caption = 'Virtual Report Name (Tax payment coupon)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dedtNbr_Days_For_When_Due: TevDBEdit
        Left = 556
        Top = 164
        Width = 110
        Height = 21
        DataField = 'NUMBER_OF_DAYS_FOR_WHEN_DUE'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtDescription: TevDBEdit
        Left = 8
        Top = 164
        Width = 185
        Height = 21
        DataField = 'DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object drgpThrd_Mnth_Due_End_Next_Mnth: TevDBRadioGroup
        Left = 8
        Top = 287
        Width = 200
        Height = 41
        Caption = '3rd Month Due End of Following Month'
        Columns = 2
        DataField = 'THRD_MNTH_DUE_END_OF_NEXT_MNTH'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 10
        Values.Strings = (
          'Y'
          'N')
      end
      object wwDBGrid3: TevDBGrid
        Left = 8
        Top = 12
        Width = 665
        Height = 117
        DisableThemesInTitle = False
        Selected.Strings = (
          'DESCRIPTION'#9'20'#9'Description')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_LOCAL_DEPOSIT_FREQ\wwDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwcbType: TevDBComboBox
        Left = 210
        Top = 164
        Width = 137
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'FREQUENCY_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object wwcbWhen_Due_Type: TevDBComboBox
        Left = 360
        Top = 164
        Width = 183
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'WHEN_DUE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object dblcTaxCoupon: TevDBLookupCombo
        Left = 418
        Top = 355
        Width = 219
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'30'#9'DESCRIPTION'#9'F')
        DataField = 'TAX_COUPON_SY_REPORTS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS.SY_REPORTS
        LookupField = 'SY_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 13
        Visible = False
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object edTypeCode: TevDBEdit
        Left = 8
        Top = 255
        Width = 92
        Height = 21
        HelpContext = 1549
        DataField = 'TAX_PAYMENT_TYPE_CODE'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object rgIncrementTypeCode: TevDBRadioGroup
        Left = 220
        Top = 287
        Width = 188
        Height = 41
        HelpContext = 1553
        Caption = 'Increment Tax Payment Type Code'
        Columns = 2
        DataField = 'INC_TAX_PAYMENT_CODE'
        DataSource = wwdsDetail
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 11
        Values.Strings = (
          'Y'
          'N')
      end
      object rgStatusChange: TevDBRadioGroup
        Left = 419
        Top = 287
        Width = 219
        Height = 41
        HelpContext = 9
        Caption = '~Change Status When Threshold Met'
        Columns = 2
        DataField = 'CHANGE_FREQ_ON_THRESHOLD'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 9
        Values.Strings = (
          'Y'
          'N')
      end
      object cbFirstTPeriod: TevDBComboBox
        Left = 8
        Top = 212
        Width = 185
        Height = 21
        HelpContext = 7
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'FIRST_THRESHOLD_PERIOD'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object edFirstTAmount: TevDBEdit
        Left = 210
        Top = 212
        Width = 92
        Height = 21
        HelpContext = 8
        DataField = 'FIRST_THRESHOLD_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object luFirstTFreq: TevDBLookupCombo
        Left = 360
        Top = 212
        Width = 185
        Height = 21
        HelpContext = 13
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'20'#9'DESCRIPTION')
        DataField = 'FIRST_THRESHOLD_DEP_FREQ_NBR'
        DataSource = wwdsDetail
        LookupTable = evLookupDS
        LookupField = 'SY_LOCAL_DEPOSIT_FREQ_NBR'
        Style = csDropDownList
        TabOrder = 7
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object luFieldOffice: TevDBLookupCombo
        Left = 8
        Top = 355
        Width = 398
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'30'#9'AGENCY_NAME'#9'F'
          'ADDITIONAL_NAME'#9'30'#9'ADDITIONAL_NAME'#9'F')
        DataField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GL_AGENCY_FIELD_OFFICE.SY_GL_AGENCY_FIELD_OFFICE
        LookupField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        Style = csDropDownList
        TabOrder = 12
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object cbSystemReportsGroup: TevDBLookupCombo
        Left = 236
        Top = 384
        Width = 256
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'60'#9'Name'#9#9)
        DataField = 'SY_REPORTS_GROUP_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS_GROUP.SY_REPORTS_GROUP
        LookupField = 'SY_REPORTS_GROUP_NBR'
        Style = csDropDownList
        TabOrder = 14
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  inherited evPanel1: TevPanel
    Width = 847
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 352
    Top = 24
  end
  object evLookupDS: TevClientDataSet
    ClientID = -1
    Left = 420
    Top = 26
  end
  object evLookupDSrc: TevDataSource
    DataSet = evLookupDS
    MasterDataSource = wwdsSubMaster2
    Left = 490
    Top = 34
  end
end
