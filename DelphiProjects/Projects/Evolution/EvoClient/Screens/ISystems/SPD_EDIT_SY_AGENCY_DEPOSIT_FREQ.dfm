inherited EDIT_SY_AGENCY_DEPOSIT_FREQ: TEDIT_SY_AGENCY_DEPOSIT_FREQ
  Width = 780
  Height = 532
  object Label16: TevLabel [0]
    Left = 8
    Top = 8
    Width = 103
    Height = 13
    Caption = 'Global Agency Name '
  end
  object DBText1: TevDBText [1]
    Left = 120
    Top = 8
    Width = 60
    Height = 16
    AutoSize = True
    DataField = 'AGENCY_NAME'
    DataSource = wwdsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object PageControl1: TevPageControl [2]
    Left = 0
    Top = 31
    Width = 697
    Height = 450
    HelpContext = 53501
    ActivePage = tsBrowse
    TabOrder = 0
    object tsBrowse: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 689
        Height = 422
        Align = alClient
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 354
          Top = 1
          Height = 420
        end
        object wwDBGrid1: TevDBGrid
          Left = 1
          Top = 1
          Width = 353
          Height = 420
          DisableThemesInTitle = False
          Selected.Strings = (
            'AGENCY_NAME'#9'40'#9'Agency'
            'COUNTY'#9'40'#9'Locality'
            'STATE'#9'2'#9'State')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_SY_AGENCY_DEPOSIT_FREQ\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsMaster
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object Panel2: TevPanel
          Left = 357
          Top = 1
          Width = 331
          Height = 420
          Align = alClient
          TabOrder = 1
          object wwDBGrid2: TevDBGrid
            Left = 1
            Top = 1
            Width = 329
            Height = 418
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_SY_AGENCY_DEPOSIT_FREQ\wwDBGrid2'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
          end
        end
      end
    end
    object tsDetail: TTabSheet
      Caption = 'Detail'
      object lablDescription: TevLabel
        Left = 8
        Top = 148
        Width = 65
        Height = 13
        Caption = '~Description'
        FocusControl = dedtDescription
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablType: TevLabel
        Left = 210
        Top = 148
        Width = 29
        Height = 13
        Caption = '~Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablWhen_Due_Type: TevLabel
        Left = 328
        Top = 148
        Width = 93
        Height = 13
        Caption = '~When Due Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablNbr_Days_For_When_Due: TevLabel
        Left = 524
        Top = 148
        Width = 154
        Height = 13
        Caption = '~Nbr of Days For When Due'
        FocusControl = dedtNbr_Days_For_When_Due
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TevLabel
        Left = 8
        Top = 203
        Width = 117
        Height = 13
        Caption = 'Tax Payment Type Code'
      end
      object evLabel8: TevLabel
        Left = 210
        Top = 203
        Width = 204
        Height = 13
        Caption = 'Virtual Report Name (Tax payment coupon)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBGrid3: TevDBGrid
        Left = 11
        Top = 12
        Width = 665
        Height = 117
        DisableThemesInTitle = False
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'Description'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_AGENCY_DEPOSIT_FREQ\wwDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object dedtDescription: TevDBEdit
        Left = 8
        Top = 164
        Width = 185
        Height = 21
        DataField = 'DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwcbType: TevDBComboBox
        Left = 210
        Top = 164
        Width = 103
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'FREQUENCY_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object wwcbWhen_Due_Type: TevDBComboBox
        Left = 328
        Top = 164
        Width = 183
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'WHEN_DUE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object dedtNbr_Days_For_When_Due: TevDBEdit
        Left = 524
        Top = 164
        Width = 110
        Height = 21
        DataField = 'NUMBER_OF_DAYS_FOR_WHEN_DUE'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object edTypeCode: TevDBEdit
        Left = 8
        Top = 223
        Width = 92
        Height = 21
        HelpContext = 1549
        DataField = 'TAX_PAYMENT_TYPE_CODE'
        DataSource = wwdsDetail
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object cbSystemReportsGroup: TevDBLookupCombo
        Left = 210
        Top = 223
        Width = 299
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'60'#9'Name'#9#9)
        DataField = 'SY_REPORTS_GROUP_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_REPORTS_GROUP.SY_REPORTS_GROUP
        LookupField = 'SY_REPORTS_GROUP_NBR'
        Style = csDropDownList
        TabOrder = 6
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_AGENCY_DEPOSIT_FREQ.SY_AGENCY_DEPOSIT_FREQ
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 249
    Top = 18
  end
end
