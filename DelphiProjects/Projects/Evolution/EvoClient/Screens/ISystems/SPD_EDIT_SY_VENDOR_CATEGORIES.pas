// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_VENDOR_CATEGORIES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_BASE_SY_HR, Db, Wwdatsrc,  StdCtrls, Mask,
  wwdbedit, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, SDataStructure, EvUIComponents, EvClientDataSet,
  SDDClasses, SDataDictsystem, ISBasicClasses, isUIwwDBEdit, EvCommonInterfaces, EvDataset, EvExceptions;

type
  TEDIT_SY_VENDOR_CATEGORIES = class(TEDIT_BASE_SY_HR)
  private
    { Private declarations }
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses
  SPackageEntry;

{$R *.DFM}

{ TEDIT_SY_VENDOR_CATEGORIES }

function TEDIT_SY_VENDOR_CATEGORIES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_HR.SY_VENDOR_CATEGORIES;
end;

procedure TEDIT_SY_VENDOR_CATEGORIES.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
  function CheckCategoryInUSe:boolean;
   var
     s: string;
     Q: IEvQuery;
  begin

    s:= 'select count(SY_VENDORS_NBR) ' +
           ' from SY_VENDORS where {AsOfNow<SY_VENDORS>} and SY_VENDOR_CATEGORIES_NBR = ' + IntToStr(DM_SYSTEM_HR.SY_VENDOR_CATEGORIES.FieldByName('SY_VENDOR_CATEGORIES_NBR').AsInteger);

    Q := TevQuery.Create(s);
    Q.Execute;
    result := Q.Result.Fields[0].AsInteger > 0;
  end;

begin
  inherited;

  if Kind = NavDelete then
    if CheckCategoryInUSe then
          raise EUpdateError.CreateHelp('Category can not be deleted because it is in use by a Vendor.', IDH_ConsistencyViolation);

end;

initialization
  RegisterClass(TEDIT_SY_VENDOR_CATEGORIES);
end.
