// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_REPORTS_GROUP;

interface

uses
   SFrameEntry, wwdblook, StdCtrls, wwdbedit, Wwdotdot,
  Wwdbcomb, Mask, DBCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid, Classes,
  Db, Wwdatsrc, SDataStructure, SFieldCodeValues, ExtCtrls, Wwdbspin,
  SDDClasses, SDataDictsystem, ISBasicClasses, wwdbdatetimepicker, Forms,
  SPD_MiniNavigationFrame, Buttons, evUtils, EvContext, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_REPORTS_GROUP = class(TFrameEntry)
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evPanel2: TevPanel;
    evPanel3: TevPanel;
    Label2: TevLabel;
    cbPriority: TevDBComboBox;
    evGroupBox1: TevGroupBox;
    evLabel2: TevLabel;
    evLabel4: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    grdSyReports: TevDBGrid;
    dsSYReports: TevDataSource;
    SyReportsMiniNavigationFrame: TMiniNavigationFrame;
    evPanel4: TevPanel;
    wwDBGrid1: TevDBGrid;
    evPanel1: TevPanel;
    lablReport_Description: TevLabel;
    evLabel6: TevLabel;
    gbReportCopies: TevGroupBox;
    dbcAgency: TevDBCheckBox;
    dbcSb: TevDBCheckBox;
    dbcCl: TevDBCheckBox;
    evDBComboBox1: TevDBComboBox;
    dedtReport_Name: TevDBEdit;
    evLabel3: TevLabel;
    cbReportRw: TevDBLookupCombo;
    cbVirtualFilter: TevCheckBox;
    btnVirtualSetUp: TevBitBtn;
    cbNullNbr: TevCheckBox;
    procedure SyReportsMiniNavigationFrameSpeedButton1Click(
      Sender: TObject);
    procedure SyReportsMiniNavigationFrameSpeedButton2Click(
      Sender: TObject);
    procedure dsSYReportsStateChange(Sender: TObject);
    procedure btnVirtualSetUpClick(Sender: TObject);
    procedure cbVirtualFilterClick(Sender: TObject);
    procedure Update_SyReports(const grd: TevDBGrid);
    procedure cbReportRwCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwDBGrid1RowChanged(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}


procedure TEDIT_SY_REPORTS_GROUP.Activate;
begin
  inherited;
  SyReportsMiniNavigationFrame.DataSource := dsSYReports;
  SyReportsMiniNavigationFrame.InsertFocusControl := cbReportRw;
end;


function TEDIT_SY_REPORTS_GROUP.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_REPORTS_GROUP;
end;

function TEDIT_SY_REPORTS_GROUP.GetInsertControl: TWinControl;
begin
   Result := dedtReport_Name;
end;

procedure TEDIT_SY_REPORTS_GROUP.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  AddDS(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS, SupportDataSets);
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS_GROUP, EditDataSets, 'SY_REPORTS_GROUP_NBR > 20000');
  AddDS(DM_SYSTEM_MISC.SY_REPORTS, EditDataSets);
end;

procedure TEDIT_SY_REPORTS_GROUP.SyReportsMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  SyReportsMiniNavigationFrame.InsertRecordExecute(Sender);
end;

procedure TEDIT_SY_REPORTS_GROUP.SyReportsMiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  SyReportsMiniNavigationFrame.DeleteRecordExecute(Sender);
end;

procedure TEDIT_SY_REPORTS_GROUP.dsSYReportsStateChange(Sender: TObject);
begin
  inherited;
  if dsSYReports.DataSet.State = dsInsert then
  begin
    dsSYReports.DataSet.FieldByName('SY_REPORTS_GROUP_NBR').AsInteger := wwdsDetail.DataSet.FieldByName('SY_REPORTS_GROUP_NBR').AsInteger;
    dsSYReports.DataSet.FieldByName('DESCRIPTION').AsString := DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.REPORT_DESCRIPTION.AsString;
    dsSYReports.DataSet.FieldByName('ACTIVE_REPORT').AsInteger := 1;
  end;
end;

procedure TEDIT_SY_REPORTS_GROUP.cbReportRwCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if dsSYReports.DataSet.State = dsInsert then
    dsSYReports.DataSet.FieldByName('DESCRIPTION').AsString := DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.REPORT_DESCRIPTION.AsString;

end;


procedure TEDIT_SY_REPORTS_GROUP.Update_SyReports(const grd: TevDBGrid);
  procedure Process;
  begin
   DM_SYSTEM_MISC.SY_REPORTS.Edit;
   DM_SYSTEM_MISC.SY_REPORTS['SY_REPORTS_GROUP_NBR'] :=
     wwdsDetail.DataSet.FieldByName('SY_REPORTS_GROUP_NBR').AsInteger;
   DM_SYSTEM_MISC.SY_REPORTS.Post;
  end;
Var
  i: Integer;
begin
    try
      if grd.SelectedList.Count > 1 then
      begin
        for i := grd.SelectedList.Count-1 downto 0 do
        begin
          grd.DataSource.DataSet.GotoBookmark(grd.SelectedList[i]);
          Process;
        end;
      end
      else
        Process;
    finally
      grd.UnselectAll;
    end;
end;



procedure TEDIT_SY_REPORTS_GROUP.btnVirtualSetUpClick(Sender: TObject);
begin
  inherited;
  if cbVirtualFilter.Checked then
  begin
     Update_SyReports(grdSyReports);
     ctx_DataAccess.PostDataSets([DM_SYSTEM_MISC.SY_REPORTS]);
  end;
end;

procedure TEDIT_SY_REPORTS_GROUP.cbVirtualFilterClick(Sender: TObject);
begin
  inherited;
  if cbVirtualFilter.Checked then
  begin
    btnVirtualSetUp.Enabled := True;
    cbNullNbr.Enabled := True;
    DM_SYSTEM_MISC.SY_REPORTS.Filter:= '';
    if cbNullNbr.Checked then
     DM_SYSTEM_MISC.SY_REPORTS.Filter:= 'SY_REPORTS_GROUP_NBR  is null';
    DM_SYSTEM_MISC.SY_REPORTS.Filtered := cbNullNbr.Checked;
  end
  else
  begin
    btnVirtualSetUp.Enabled := False;
    cbNullNbr.Enabled := False;
    DM_SYSTEM_MISC.SY_REPORTS.Filter:= 'SY_REPORTS_GROUP_NBR = ' +
        String(ConvertNull(wwdsDetail.DataSet['SY_REPORTS_GROUP_NBR'], -1));
    DM_SYSTEM_MISC.SY_REPORTS.Filtered := True;
  end;

end;

procedure TEDIT_SY_REPORTS_GROUP.wwDBGrid1RowChanged(Sender: TObject);
begin
  inherited;
  if Not cbVirtualFilter.Checked then
  begin
    btnVirtualSetUp.Enabled := False;
    DM_SYSTEM_MISC.SY_REPORTS.Filter:= 'SY_REPORTS_GROUP_NBR = ' +
      String(ConvertNull(wwdsDetail.DataSet['SY_REPORTS_GROUP_NBR'], -1));
    DM_SYSTEM_MISC.SY_REPORTS.Filtered := True;
  end;
end;

initialization
  RegisterClass(TEDIT_SY_REPORTS_GROUP);

end.
