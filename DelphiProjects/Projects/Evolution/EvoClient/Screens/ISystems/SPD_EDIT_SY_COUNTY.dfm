inherited EDIT_SY_COUNTY: TEDIT_SY_COUNTY
  inherited PageControl1: TevPageControl
    HelpContext = 31501
    ActivePage = TabSheet2
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        object wwDBGrid2: TevDBGrid
          Left = 220
          Top = 0
          Width = 215
          Height = 216
          Selected.Strings = (
            'COUNTY_NAME'#9'40'#9'County Name')
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = wwdsDetail
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          IndicatorColor = icBlack
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'Details'
      object lablCounty_Name: TevLabel
        Left = 12
        Top = 356
        Width = 76
        Height = 13
        Caption = '~County Name'
        FocusControl = dedtCounty_Name
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBGrid3: TevDBGrid
        Left = 12
        Top = 28
        Width = 261
        Height = 321
        Selected.Strings = (
          'COUNTY_NAME'#9'40'#9'County Name')
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
      object dedtCounty_Name: TevDBEdit
        Left = 12
        Top = 372
        Width = 509
        Height = 21
        DataField = 'COUNTY_NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBGrid1: TevDBGrid
        Left = 288
        Top = 32
        Width = 233
        Height = 313
        Selected.Strings = (
          'NAME'#9'40'#9'Local'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = evDataSource1
        TabOrder = 2
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_COUNTY.SY_COUNTY
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 318
    Top = 15
  end
  object evDataSource1: TevDataSource
    DataSet = DM_SY_LOCALS.SY_LOCALS
    MasterDataSource = wwdsDetail
    Left = 384
    Top = 16
  end
end
