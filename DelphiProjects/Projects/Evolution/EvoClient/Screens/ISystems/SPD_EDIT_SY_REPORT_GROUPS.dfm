inherited EDIT_SY_REPORT_GROUPS: TEDIT_SY_REPORT_GROUPS
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 270
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Report Groups'
      object grGroups: TevDBGrid
        Left = 0
        Top = 0
        Width = 435
        Height = 242
        Selected.Strings = (
          'NAME'#9'40'#9'Name'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mDisableDialog]
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_REPORT_GROUPS\grGroups'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Detail'
      ImageIndex = 1
      object Label1: TevLabel
        Left = 6
        Top = 19
        Width = 71
        Height = 13
        Caption = '~Group Name'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edName: TevDBEdit
        Left = 85
        Top = 16
        Width = 209
        Height = 21
        DataField = 'NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_REPORT_GROUPS.SY_REPORT_GROUPS
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 252
    Top = 18
  end
end
