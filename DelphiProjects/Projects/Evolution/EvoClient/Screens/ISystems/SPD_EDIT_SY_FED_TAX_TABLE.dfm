inherited EDIT_SY_FED_TAX_TABLE: TEDIT_SY_FED_TAX_TABLE
  Width = 777
  Height = 682
  object lablExemption_Amount: TevLabel [0]
    Left = 28
    Top = 20
    Width = 97
    Height = 16
    Caption = '~Exemption Amount'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablFed_Minimum_Wage: TevLabel [1]
    Left = 28
    Top = 300
    Width = 82
    Height = 16
    Caption = '~Minimum Wage'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablFed_Tip_Credit: TevLabel [2]
    Left = 28
    Top = 344
    Width = 54
    Height = 16
    Caption = '~Tip Credit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablSupplemental_Tax_Percentage: TevLabel [3]
    Left = 28
    Top = 392
    Width = 105
    Height = 16
    Caption = '~Supplemental Tax %'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablOASDI_Rate: TevLabel [4]
    Left = 152
    Top = 20
    Width = 68
    Height = 16
    Caption = '~OASDI Rate'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablOASDI_Wage_Rate: TevLabel [5]
    Left = 152
    Top = 64
    Width = 89
    Height = 13
    Caption = 'OASDI Wage Limit'
  end
  object lablMedicare_Rate: TevLabel [6]
    Left = 152
    Top = 112
    Width = 79
    Height = 16
    Caption = '~Medicare Rate'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablMedicare_Wage_Limit: TevLabel [7]
    Left = 152
    Top = 256
    Width = 109
    Height = 16
    Caption = '~Medicare Wage Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablFUI_Rate_Real: TevLabel [8]
    Left = 152
    Top = 300
    Width = 77
    Height = 16
    Caption = '~FUI Rate Real'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablFUI_Rate_Credit: TevLabel [9]
    Left = 152
    Top = 348
    Width = 82
    Height = 16
    Caption = '~FUI Rate Credit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablFUI_Wage_Limit: TevLabel [10]
    Left = 152
    Top = 396
    Width = 82
    Height = 16
    Caption = '~FUI Wage Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablSY_401k_Limit: TevLabel [11]
    Left = 325
    Top = 20
    Width = 57
    Height = 16
    Caption = '~401k Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablSY_403b_Limit: TevLabel [12]
    Left = 325
    Top = 64
    Width = 57
    Height = 16
    Caption = '~403b Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablSY_457_Limit: TevLabel [13]
    Left = 325
    Top = 112
    Width = 51
    Height = 16
    Caption = '~457 Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablSY_501c_Limit: TevLabel [14]
    Left = 325
    Top = 160
    Width = 57
    Height = 16
    Caption = '~501c Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablSimple_Limit: TevLabel [15]
    Left = 325
    Top = 204
    Width = 64
    Height = 16
    Caption = '~Simple Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablSep_Limit: TevLabel [16]
    Left = 325
    Top = 252
    Width = 54
    Height = 16
    Caption = '~SEP Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablDeferred_Comp_Limit: TevLabel [17]
    Left = 325
    Top = 300
    Width = 104
    Height = 16
    Caption = '~Deferred Comp Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablFed_Deposit_Freq_Threshold: TevLabel [18]
    Left = 152
    Top = 440
    Width = 136
    Height = 16
    Caption = '~FUI Annual Threshold Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lablSecond_EIC_Amount: TevLabel [19]
    Left = 28
    Top = 160
    Width = 96
    Height = 13
    Caption = 'Second EIC Amount'
  end
  object lablSecond_EIC_Limit: TevLabel [20]
    Left = 28
    Top = 204
    Width = 81
    Height = 13
    Caption = 'Second EIC Limit'
  end
  object lablFirst_EIC_Percentage: TevLabel [21]
    Left = 28
    Top = 112
    Width = 50
    Height = 13
    Caption = 'First EIC %'
  end
  object lablThird_EIC_Additional_Percent: TevLabel [22]
    Left = 28
    Top = 252
    Width = 104
    Height = 13
    Caption = 'Third EIC Additional %'
  end
  object lablFirst_EIC_Limit: TevLabel [23]
    Left = 28
    Top = 64
    Width = 63
    Height = 13
    Caption = 'First EIC Limit'
  end
  object evLabel1: TevLabel [24]
    Left = 477
    Top = 20
    Width = 132
    Height = 16
    Caption = '~Catchup Contribution Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel2: TevLabel [25]
    Left = 477
    Top = 64
    Width = 111
    Height = 16
    Caption = '~Dependent Care Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel3: TevLabel [26]
    Left = 477
    Top = 112
    Width = 87
    Height = 16
    Caption = '~HSA Single Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel4: TevLabel [27]
    Left = 477
    Top = 160
    Width = 87
    Height = 16
    Caption = '~HSA Family Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel5: TevLabel [28]
    Left = 477
    Top = 253
    Width = 100
    Height = 16
    Caption = '~Compensation Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel6: TevLabel [29]
    Left = 477
    Top = 204
    Width = 98
    Height = 16
    Caption = '~HSA Catchup Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel7: TevLabel [30]
    Left = 477
    Top = 300
    Width = 77
    Height = 16
    Caption = '~Roth IRA Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel8: TevLabel [31]
    Left = 477
    Top = 344
    Width = 120
    Height = 16
    Caption = '~Roth IRA Catchup Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel9: TevLabel [32]
    Left = 152
    Top = 492
    Width = 86
    Height = 16
    Caption = '~ER OASDI Rate'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel10: TevLabel [33]
    Left = 328
    Top = 396
    Width = 85
    Height = 16
    Caption = '~EE OASDI Rate'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel11: TevLabel [34]
    Left = 152
    Top = 160
    Width = 144
    Height = 16
    Caption = '~EE Medicare Threshold Limit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel12: TevLabel [35]
    Left = 152
    Top = 204
    Width = 146
    Height = 16
    Caption = '~EE Medicare Threshold Rate'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel13: TevLabel [36]
    Left = 28
    Top = 440
    Width = 106
    Height = 13
    Caption = 'Federal Poverty Level '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lblAcaAfford: TevLabel [37]
    Left = 28
    Top = 492
    Width = 95
    Height = 13
    Caption = 'ACA Afford Percent '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object wwdeExemption_Amount: TevDBEdit [38]
    Left = 28
    Top = 36
    Width = 93
    Height = 21
    HelpContext = 6026
    DataField = 'EXEMPTION_AMOUNT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 0
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeSecond_EIC_Amount: TevDBEdit [39]
    Left = 28
    Top = 176
    Width = 93
    Height = 21
    HelpContext = 6027
    DataField = 'SECOND_EIC_AMOUNT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 3
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeSecond_EIC_Limit: TevDBEdit [40]
    Left = 28
    Top = 220
    Width = 93
    Height = 21
    HelpContext = 6028
    DataField = 'SECOND_EIC_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 4
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeFirst_EIC_Percentage: TevDBEdit [41]
    Left = 28
    Top = 128
    Width = 93
    Height = 21
    HelpContext = 6029
    DataField = 'FIRST_EIC_PERCENTAGE'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 2
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeThird_EIC_Additional_Percent: TevDBEdit [42]
    Left = 28
    Top = 268
    Width = 93
    Height = 21
    HelpContext = 6030
    DataField = 'THIRD_EIC_ADDITIONAL_PERCENT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 5
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeFed_Minimum_Wage: TevDBEdit [43]
    Left = 28
    Top = 316
    Width = 92
    Height = 21
    HelpContext = 6031
    DataField = 'FEDERAL_MINIMUM_WAGE'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 6
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeFederal_Tip_Credit: TevDBEdit [44]
    Left = 28
    Top = 360
    Width = 92
    Height = 21
    HelpContext = 6032
    DataField = 'FEDERAL_TIP_CREDIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 7
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeSupplemental_Tax_Percentage: TevDBEdit [45]
    Left = 28
    Top = 408
    Width = 92
    Height = 21
    HelpContext = 6033
    DataField = 'SUPPLEMENTAL_TAX_PERCENTAGE'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 8
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeOASDI_Rate: TevDBEdit [46]
    Left = 152
    Top = 36
    Width = 92
    Height = 21
    HelpContext = 6034
    DataField = 'OASDI_RATE'
    DataSource = wwdsDetail
    Enabled = False
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 9
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeOASDI_Wage_Limit: TevDBEdit [47]
    Left = 152
    Top = 80
    Width = 92
    Height = 21
    HelpContext = 6035
    DataField = 'OASDI_WAGE_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 10
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeMedicare_Rate: TevDBEdit [48]
    Left = 152
    Top = 128
    Width = 92
    Height = 21
    HelpContext = 6036
    DataField = 'MEDICARE_RATE'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 11
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeMedicare_Wage_Limit: TevDBEdit [49]
    Left = 152
    Top = 272
    Width = 92
    Height = 21
    HelpContext = 6037
    DataField = 'MEDICARE_WAGE_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 12
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeFUI_Rate_Real: TevDBEdit [50]
    Left = 152
    Top = 316
    Width = 92
    Height = 21
    HelpContext = 6014
    DataField = 'FUI_RATE_REAL'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 13
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeFUI_Rate_Credit: TevDBEdit [51]
    Left = 152
    Top = 364
    Width = 92
    Height = 21
    HelpContext = 6015
    DataField = 'FUI_RATE_CREDIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 14
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeFUI_Wage_Limit: TevDBEdit [52]
    Left = 152
    Top = 412
    Width = 92
    Height = 21
    HelpContext = 6016
    DataField = 'FUI_WAGE_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 15
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwde401k_Limit: TevDBEdit [53]
    Left = 325
    Top = 36
    Width = 92
    Height = 21
    HelpContext = 6017
    DataField = 'SY_401K_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 16
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwde403b_Limit: TevDBEdit [54]
    Left = 325
    Top = 80
    Width = 92
    Height = 21
    HelpContext = 6018
    DataField = 'SY_403B_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 17
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwde457_Limit: TevDBEdit [55]
    Left = 325
    Top = 128
    Width = 92
    Height = 21
    HelpContext = 6019
    DataField = 'SY_457_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 18
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwde501c_Limit: TevDBEdit [56]
    Left = 325
    Top = 176
    Width = 92
    Height = 21
    HelpContext = 6020
    DataField = 'SY_501C_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 19
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeSimple_Limit: TevDBEdit [57]
    Left = 325
    Top = 220
    Width = 92
    Height = 21
    HelpContext = 6021
    DataField = 'SIMPLE_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 20
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeSEP_Limit: TevDBEdit [58]
    Left = 325
    Top = 268
    Width = 92
    Height = 21
    HelpContext = 6022
    DataField = 'SEP_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 21
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeDeferred_Comp_Limit: TevDBEdit [59]
    Left = 325
    Top = 316
    Width = 92
    Height = 21
    HelpContext = 6038
    DataField = 'DEFERRED_COMP_LIMIT'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 22
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeDeposit_Frequency_Threshold: TevDBEdit [60]
    Left = 152
    Top = 456
    Width = 92
    Height = 21
    HelpContext = 6024
    DataField = 'FED_DEPOSIT_FREQ_THRESHOLD'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 23
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object wwdeFirst_EIC_Limit: TevDBEdit [61]
    Left = 28
    Top = 80
    Width = 93
    Height = 21
    HelpContext = 6027
    DataField = 'FIRST_EIC_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 1
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit1: TevDBEdit [62]
    Left = 477
    Top = 36
    Width = 92
    Height = 21
    HelpContext = 6019
    DataField = 'SY_CATCH_UP_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 24
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit2: TevDBEdit [63]
    Left = 477
    Top = 80
    Width = 92
    Height = 21
    HelpContext = 6019
    DataField = 'SY_DEPENDENT_CARE_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 25
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit3: TevDBEdit [64]
    Left = 477
    Top = 128
    Width = 92
    Height = 21
    HelpContext = 6019
    DataField = 'SY_HSA_SINGLE_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 26
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit4: TevDBEdit [65]
    Left = 477
    Top = 176
    Width = 92
    Height = 21
    HelpContext = 6019
    DataField = 'SY_HSA_FAMILY_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 27
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit5: TevDBEdit [66]
    Left = 477
    Top = 269
    Width = 92
    Height = 21
    HelpContext = 6019
    DataField = 'COMPENSATION_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 28
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit6: TevDBEdit [67]
    Left = 477
    Top = 220
    Width = 92
    Height = 21
    HelpContext = 6019
    DataField = 'SY_HSA_CATCH_UP_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 29
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit7: TevDBEdit [68]
    Left = 477
    Top = 316
    Width = 92
    Height = 21
    HelpContext = 6019
    DataField = 'ROTH_IRA_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 30
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit8: TevDBEdit [69]
    Left = 477
    Top = 360
    Width = 92
    Height = 21
    HelpContext = 6019
    DataField = 'SY_IRA_CATCHUP_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 31
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit9: TevDBEdit [70]
    Left = 152
    Top = 508
    Width = 92
    Height = 21
    HelpContext = 6034
    DataField = 'ER_OASDI_RATE'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 32
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit10: TevDBEdit [71]
    Left = 328
    Top = 412
    Width = 92
    Height = 21
    HelpContext = 6034
    DataField = 'EE_OASDI_RATE'
    DataSource = wwdsDetail
    Picture.PictureMask = 
      '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
      '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
      '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
    TabOrder = 33
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit11: TevDBEdit [72]
    Left = 152
    Top = 176
    Width = 92
    Height = 21
    HelpContext = 6037
    DataField = 'EE_MED_TH_LIMIT'
    DataSource = wwdsDetail
    TabOrder = 34
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object evDBEdit12: TevDBEdit [73]
    Left = 152
    Top = 224
    Width = 92
    Height = 21
    HelpContext = 6036
    DataField = 'EE_MED_TH_RATE'
    DataSource = wwdsDetail
    TabOrder = 35
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object edPovertyLevel: TevDBEdit [74]
    Left = 28
    Top = 456
    Width = 92
    Height = 21
    HelpContext = 6033
    DataField = 'FED_POVERTY_LEVEL'
    DataSource = wwdsDetail
    TabOrder = 36
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object edtACAAfford: TevDBEdit [75]
    Left = 28
    Top = 508
    Width = 92
    Height = 21
    HelpContext = 6033
    DataField = 'ACA_AFFORD_PERCENT'
    DataSource = wwdsDetail
    TabOrder = 37
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  inherited wwdsMaster: TevDataSource
    Left = 325
    Top = 506
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_FED_TAX_TABLE.SY_FED_TAX_TABLE
    Left = 390
    Top = 530
  end
  inherited wwdsList: TevDataSource
    Left = 482
    Top = 482
  end
  object DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL
    Left = 430
    Top = 489
  end
end
