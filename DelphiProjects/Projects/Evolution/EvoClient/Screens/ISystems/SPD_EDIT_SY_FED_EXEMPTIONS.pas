// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_FED_EXEMPTIONS;

interface

uses
  SFrameEntry,  SDataStructure, DBCtrls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_FED_EXEMPTIONS = class(TFrameEntry)
    lablE_D_Code: TevLabel;
    wwDBGrid1: TevDBGrid;
    drgpFederal: TevDBRadioGroup;
    drgpExempt_FUI: TevDBRadioGroup;
    drgpExempt_EE_OASDI: TevDBRadioGroup;
    drgpExempt_EE_Medicare: TevDBRadioGroup;
    drgpExempt_EM_OASDI: TevDBRadioGroup;
    drgpExempt_ER_Medicare: TevDBRadioGroup;
    drgpExempt_EE_EIC: TevDBRadioGroup;
    wwcbE_D_Code: TevDBComboDlg;
    dedtW2_Box: TevDBEdit;
    lablW2_Box: TevLabel;
    DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL;
    evDBText1: TevDBText;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    { Public declarations }
  end;

implementation

{$R *.DFM}

function TEDIT_SY_FED_EXEMPTIONS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS;
end;

function TEDIT_SY_FED_EXEMPTIONS.GetInsertControl: TWinControl;
begin
  Result := wwcbE_D_Code;
end;

initialization
  RegisterClass(TEDIT_SY_FED_EXEMPTIONS);

end.
