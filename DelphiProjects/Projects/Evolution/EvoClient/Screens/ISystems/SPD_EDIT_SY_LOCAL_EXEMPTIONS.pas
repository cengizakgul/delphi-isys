// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_LOCAL_EXEMPTIONS;

interface

uses
  SPD_EDIT_BASE_SY_LOCAL, StdCtrls, Mask, wwdbedit, Wwdotdot, ExtCtrls,
  DBCtrls, SDataStructure, Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, Controls, ComCtrls, Classes, ActnList, Menus, EvContext,
  ISBasicClasses, SDDClasses, SDataDictsystem, EvUIUtils, EvUIComponents, EvClientDataSet;


type
  TEDIT_SY_LOCAL_EXEMPTIONS = class(TEDIT_BASE_SY_LOCAL)
    lablE_D_Code_Type: TevLabel;
    drgpLocal: TevDBRadioGroup;
    wwDBGrid3: TevDBGrid;
    wwcdE_D_Code_Type: TevDBComboDlg;
    evPopupMenu1: TevPopupMenu;
    evActionList1: TevActionList;
    CopyExemption: TAction;
    CopyTo1: TMenuItem;
    evDBText1: TevDBText;
    procedure CopyExemptionUpdate(Sender: TObject);
    procedure CopyExemptionExecute(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

uses
  SPD_LocalChoiceQuery, sysutils, evutils, ISDataAccessComponents;

{$R *.DFM}

function TEDIT_SY_LOCAL_EXEMPTIONS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS;
end;

function TEDIT_SY_LOCAL_EXEMPTIONS.GetInsertControl: TWinControl;
begin
  Result := wwcdE_D_Code_Type;
end;

procedure TEDIT_SY_LOCAL_EXEMPTIONS.CopyExemptionUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := wwDBGrid3.SelectedList.Count > 0;;
end;

procedure TEDIT_SY_LOCAL_EXEMPTIONS.CopyExemptionExecute(Sender: TObject);
var
  CurState: integer;
  CurLocal: integer;
  CurExemption: integer;
  SelectedExemptions: TEvClientDataset;
  stateFilter, localFilter:String;
  stateFiltered, localFiltered: Boolean;

  procedure DoCopyExemptionsTo( state, local: integer );
  var
    k: integer;
    fn: string;
  begin
    SelectedExemptions.First;
    while not SelectedExemptions.Eof do
    begin
      DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR',state,[]);
      DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR',local,[]);
      if DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Locate('E_D_CODE_TYPE', SelectedExemptions.FieldByName('E_D_CODE_TYPE').AsString, []) then
        DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Edit
      else
        DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Insert;

      for k := 0 to SelectedExemptions.Fields.Count - 1 do
      begin
        fn := SelectedExemptions.Fields[k].FieldName;
        if (fn <> 'EFFECTIVE_DATE') and (fn <> 'EFFECTIVE_UNTIL') and ( fn <> 'REC_VERSION' )
           and ( fn <> 'SY_LOCAL_EXEMPTIONS_NBR' )
           and (DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Fields[k].FieldKind = fkData ) then
          DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS[fn] := SelectedExemptions[fn];
      end;
      DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS['SY_LOCALS_NBR'] := local;
      DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Post;
      SelectedExemptions.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS]);

  end;

  procedure DoCopyExemptions;
  var
    k: integer;
  begin
    with TLocalChoiceQuery.Create( Self ) do
    try
      Caption := 'Select the locals you want this exemption(s) copied to';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Visible := false;
      SetFilter( 'SY_LOCALS_NBR<>' + inttostr(CurLocal) );
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.DisableControls;
        try
            SelectedExemptions := CreateDataSetWithSelectedRows( DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS, wwDBGrid3.SelectedList );
            try
              try
                for k := 0 to dgChoiceList.SelectedList.Count - 1 do
                begin
                  cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                  DoCopyExemptionsTo( cdsChoiceList.FieldByName('SY_STATES_NBR').AsInteger, cdsChoiceList.FieldByName('SY_LOCALS_NBR').AsInteger );
                end;
              finally
              end;
            finally
              SelectedExemptions.Free;
            end;
        finally
          DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.EnableControls;
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Cancel;
  CurState := DM_SYSTEM_STATE.SY_STATES['SY_STATES_NBR'];
  stateFilter := DM_SYSTEM_STATE.SY_STATES.UserFilter;
  stateFiltered := DM_SYSTEM_STATE.SY_STATES.UserFiltered;
  CurLocal := DM_SYSTEM_LOCAL.SY_LOCALS['SY_LOCALS_NBR'];
  localFilter:= DM_SYSTEM_LOCAL.SY_LOCALS.UserFilter;
  localFiltered := DM_SYSTEM_LOCAL.SY_LOCALS.UserFiltered;
  CurExemption := DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS['SY_LOCAL_EXEMPTIONS_NBR'];
  DM_SYSTEM_STATE.SY_STATES.UserFilter := '';
  DM_SYSTEM_LOCAL.SY_LOCALS.UserFilter := '';
  DM_SYSTEM_STATE.SY_STATES.UserFiltered := false;
  DM_SYSTEM_LOCAL.SY_LOCALS.UserFiltered := false;
  DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR',CurState,[]);
  DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR',Curlocal,[]);
  DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Locate('SY_LOCAL_EXEMPTIONS_NBR',CurExemption,[]);
  try
    DoCopyExemptions;
  finally
    DM_SYSTEM_STATE.SY_STATES.UserFilter := stateFilter;
    DM_SYSTEM_STATE.SY_STATES.UserFiltered := stateFiltered;
    DM_SYSTEM_LOCAL.SY_LOCALS.UserFilter:=localFilter;
    DM_SYSTEM_LOCAL.SY_LOCALS.UserFiltered:=localFiltered;
    DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR',CurState,[]);
    DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR',Curlocal,[]);
    DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS.Locate('SY_LOCAL_EXEMPTIONS_NBR',CurExemption,[]);
  end;
end;

initialization
  RegisterClass(TEDIT_SY_LOCAL_EXEMPTIONS);

end.
