// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_VENDORS;

interface

uses
  SFrameEntry,  SDataStructure, DBCtrls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet, SDDClasses,
  SDataDictsystem, isUIwwDBEdit, ISBasicClasses, wwdblook,
  isUIwwDBLookupCombo, EvUtils, Wwdbcomb, isUIwwDBComboBox,
  EvCommonInterfaces, EvDataset, EvExceptions, SysUtils;

type
  TEDIT_SY_VENDORS = class(TFrameEntry)
    wwDBGrid1: TevDBGrid;
    edtName: TevDBEdit;
    lblName: TevLabel;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    lcbCategories: TevDBLookupCombo;
    evLabel2: TevLabel;
    cbType: TevDBComboBox;
    Label14: TevLabel;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;

    { Public declarations }
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;

  end;

implementation

uses
  SPackageEntry;
  
{$R *.DFM}

function TEDIT_SY_VENDORS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_VENDORS;
end;

procedure TEDIT_SY_VENDORS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
  function CheckCategoryInUSe:boolean;
   var
     s: string;
     Q: IEvQuery;
  begin
    s:= 'select count(SB_VENDOR_NBR) from SB_VENDOR ' +
           ' where {AsOfNow<SB_VENDOR>} and VENDORS_LEVEL= ''S'' and VENDOR_NBR = ' + IntToStr(DM_SYSTEM_MISC.SY_VENDORS.FieldByName('SY_VENDORS_NBR').AsInteger);

    Q := TevQuery.Create(s);
    Q.Execute;
    result := Q.Result.Fields[0].AsInteger > 0;
  end;

begin
  inherited;

  if Kind = NavDelete then
    if CheckCategoryInUSe then
          raise EUpdateError.CreateHelp('This Vendor is attached to the Service Bureau, it must be unselected before it can be removed.', IDH_ConsistencyViolation);

end;


procedure TEDIT_SY_VENDORS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_VENDOR_CATEGORIES, SupportDataSets, 'ALL');
end;

function TEDIT_SY_VENDORS.GetInsertControl: TWinControl;
begin
  Result := edtName;
end;

initialization
  RegisterClass(TEDIT_SY_VENDORS);

end.
