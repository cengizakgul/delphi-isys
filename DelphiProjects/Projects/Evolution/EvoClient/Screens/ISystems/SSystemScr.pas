// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SSystemScr;

interface

uses
  Windows,
  SPackageEntry, ComCtrls,  StdCtrls, Graphics,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList,
  ISBasicClasses, EvCommonInterfaces, EvMainboard, EvUIComponents,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TSystemFrm = class(TFramePackageTmpl, IevSystemScreens)
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;
  end;

implementation

uses SPD_EDIT_SY_SEC_TEMPLATES;

{$R *.DFM}


var
  SystemFrm: TSystemFrm;


function GetSystemScreens: IevSystemScreens;
begin
  if not Assigned(SystemFrm) then
    SystemFrm := TSystemFrm.Create(nil);
  Result := SystemFrm;
end;

{ TSystemFrm }
function TSystemFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);  // legacy override - we take bitmap from resource now, and it will remain if no override from resource happens
  Result.Transparent := True;
  Result.TransparentMode := tmAuto;
end;


function TSystemFrm.PackageCaption: string;
begin
  Result := '&iSystems';
end;

function TSystemFrm.PackageSortPosition: string;
begin
  Result := '002';
end;

procedure TSystemFrm.UserPackageStructure;
begin
  AddStructure('Federal|010');
  AddStructure('Federal\Tax Table|110', 'TEDIT_SY_FED_TAX_TABLE');
  AddStructure('Federal\Exemptions|120', 'TEDIT_SY_FED_EXEMPTIONS');
  AddStructure('Federal\Tax Table Brackets|130', 'TEDIT_SY_FED_TAX_TABLE_BRACKETS');
  AddStructure('Federal\Reporting Agency|140', 'TEDIT_SY_FED_REPORTING_AGENCY');
  AddStructure('Federal\Tax Payment Agency|150', 'TEDIT_SY_FED_TAX_PAYMENT_AGENCY');
  AddStructure('Federal\ACA Codes|160');
  AddStructure('Federal\ACA Codes\Offer Codes|170', 'TEDIT_SY_ACA_OFFER_CODE');
  AddStructure('Federal\ACA Codes\Relief Codes|180', 'TEDIT_SY_ACA_RELIEF_CODE');

  AddStructure('State|020');
  AddStructure('State\State table|210', 'TEDIT_SY_STATES');
  AddStructure('State\Tax Chart|220', 'TEDIT_SY_STATE_TAX_CHART');
  AddStructure('State\Marital Status|230', 'TEDIT_SY_STATE_MARITAL_STATUS');
  AddStructure('State\Exemptions|240', 'TEDIT_SY_STATE_EXEMPTIONS');
  AddStructure('State\SUI Unemployment Insurance|250', 'TEDIT_SY_SUI');
  AddStructure('State\Deposit Frequency|260', 'TEDIT_SY_STATE_DEPOSIT_FREQ');

  AddStructure('Locals|030');
  AddStructure('Locals\Locals|310', 'TEDIT_SY_LOCALS');
  AddStructure('Locals\County Table|320', 'TEDIT_SY_COUNTY');
  AddStructure('Locals\Exemptions|330', 'TEDIT_SY_LOCAL_EXEMPTIONS');
  AddStructure('Locals\Deposit Frequency|340', 'TEDIT_SY_LOCAL_DEPOSIT_FREQ');
  AddStructure('Locals\Tax Chart|350', 'TEDIT_SY_LOCAL_TAX_CHART');
  AddStructure('Locals\Marital Status|360', 'TEDIT_SY_LOCAL_MARITAL_STATUS');

  AddStructure('HR|040');
  AddStructure('HR\EEO|410', 'TEDIT_SY_HR_EEO');
  AddStructure('HR\Injury Codes|420', 'TEDIT_SY_HR_INJURY_CODES');
  //AddStructure('HR\Ethnicity|430', 'TEDIT_SY_HR_ETHNICITY');
  AddStructure('HR\OSHA Anatomic Codes|440', 'TEDIT_SY_HR_OSHA_ANATOMIC_CODES');
  AddStructure('HR\Disabilities|450', 'TEDIT_SY_HR_HANDICAPS');
  AddStructure('HR\Benefit Refusal Reasons |460', 'TEDIT_SY_HR_REFUSAL_REASON');

  AddStructure('Miscellaneous|050');
  AddStructure('Miscellaneous\System Reports|510', 'TEDIT_SY_REPORTS');
  AddStructure('Miscellaneous\System Reports Group|515', 'TEDIT_SY_REPORTS_GROUP');
  AddStructure('Miscellaneous\Global Agency|520', 'TEDIT_SY_GLOBAL_AGENCY');
  AddStructure('Miscellaneous\Global Agency Holidays|530', 'TEDIT_SY_GL_HOLIDAYS');
  AddStructure('Miscellaneous\Global Agency Field Offices|530', 'TEDIT_SY_GL_AGENCY_FIELD_OFFICE');
  AddStructure('Miscellaneous\Global Agency Deposit Frequency|535', 'TEDIT_SY_AGENCY_DEPOSIT_FREQ');
  AddStructure('Miscellaneous\Global Agency Reports|540', 'TEDIT_SY_GL_AGENCY_REPORT');
  AddStructure('Miscellaneous\RW Report Groups|560', 'TEDIT_SY_REPORT_GROUPS');
  AddStructure('Miscellaneous\RW Reports|570', 'TEDIT_SY_RWREPORTS');
  AddStructure('Miscellaneous\Maintain Tips of the Day|580', 'TTipOfTheDayFrame');
  AddStructure('Miscellaneous\Task Priorities|590', 'TEDIT_SY_QUEUE_PRIORITY');
  AddStructure('Miscellaneous\Vendor Categories|600', 'TEDIT_SY_VENDOR_CATEGORIES');
  AddStructure('Miscellaneous\Vendors|610', 'TEDIT_SY_VENDORS');

  AddStructure('Security|060');
  AddStructure('Security\Templates|100', 'TEDIT_SY_SEC_TEMPLATES');

  AddStructure('Analytics|100');
  AddStructure('Analytics\Analytics Tiers|110', 'TEDIT_SY_ANALYTICS_TIER');
  AddStructure('Analytics\Dashboards|120', 'TEDIT_SY_DASHBOARDS');


end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetSystemScreens, IevSystemScreens, 'Screens iSystems');

finalization
  SystemFrm.Free;

end.
