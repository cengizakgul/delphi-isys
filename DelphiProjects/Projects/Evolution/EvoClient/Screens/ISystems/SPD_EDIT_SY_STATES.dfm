inherited EDIT_SY_STATES: TEDIT_SY_STATES
  Width = 541
  Height = 443
  inherited PageControl1: TevPageControl
    Width = 541
    Height = 410
    ActivePage = TabSheet2x
    inherited TabSheet1: TTabSheet
      inherited Panel1: TevPanel
        Width = 533
        Height = 382
        inherited Splitter1: TevSplitter
          Height = 382
        end
        inherited wwDBGrid1: TevDBGrid
          Height = 382
          IniAttributes.SectionName = 'TEDIT_SY_STATES\wwDBGrid1'
        end
      end
    end
    inherited TabSheet2: TTabSheet
      Caption = 'General'
      object Label9: TevLabel
        Left = 8
        Top = 252
        Width = 112
        Height = 13
        Caption = 'Supplemental Wages %'
      end
      object Label8: TevLabel
        Left = 141
        Top = 252
        Width = 82
        Height = 16
        Caption = '~State Tip Credit'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label15: TevLabel
        Left = 264
        Top = 108
        Width = 41
        Height = 16
        Caption = '~Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object Label17: TevLabel
        Left = 264
        Top = 16
        Width = 94
        Height = 16
        Caption = '~Reporting Agency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label18: TevLabel
        Left = 264
        Top = 64
        Width = 110
        Height = 16
        Caption = '~Tax Payment Agency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2x: TevLabel
        Left = 8
        Top = 4
        Width = 25
        Height = 13
        Caption = 'State'
        FocusControl = dbedtSTATE
      end
      object Label3: TevLabel
        Left = 8
        Top = 44
        Width = 37
        Height = 16
        Caption = '~Name'
        FocusControl = DBEdit3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel1: TevLabel
        Left = 8
        Top = 154
        Width = 51
        Height = 13
        Caption = 'FIPS Code'
      end
      object Label4: TevLabel
        Left = 8
        Top = 204
        Width = 110
        Height = 16
        Caption = '~State Minimum Wage'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBRadioGroup3: TevDBRadioGroup
        Left = 8
        Top = 92
        Width = 145
        Height = 57
        HelpContext = 1502
        Caption = '~State Withholding'
        DataField = 'STATE_WITHHOLDING'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup4: TevDBRadioGroup
        Left = 432
        Top = 336
        Width = 185
        Height = 57
        HelpContext = 1510
        Caption = '~Print State Return if Zero'
        DataField = 'PRINT_STATE_RETURN_IF_ZERO'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 8
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup9: TevDBRadioGroup
        Left = 224
        Top = 336
        Width = 185
        Height = 57
        HelpContext = 1511
        Caption = '~Use Misc Tax Return Code'
        DataField = 'USE_STATE_MISC_TAX_RETURN_CODE'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 16
        Values.Strings = (
          'Y'
          'N')
      end
      object wwDBLookupCombo2: TevDBLookupCombo
        Left = 264
        Top = 124
        Width = 273
        Height = 21
        HelpContext = 1571
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'REPORT_DESCRIPTION'#9'40'#9'REPORT_DESCRIPTION')
        DataField = 'SY_TAX_PMT_REPORT_NBR'
        DataSource = wwdsMaster
        LookupTable = DM_SY_REPORTS.SY_REPORTS
        LookupField = 'SY_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 6
        Visible = False
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwDBLookupCombo1: TevDBLookupCombo
        Left = 264
        Top = 32
        Width = 273
        Height = 21
        HelpContext = 1569
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
        DataField = 'SY_STATE_REPORTING_AGENCY_NBR'
        DataSource = wwdsMaster
        LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
        LookupField = 'SY_GLOBAL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 4
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwDBLookupCombo3: TevDBLookupCombo
        Left = 264
        Top = 80
        Width = 273
        Height = 21
        HelpContext = 1570
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
        DataField = 'SY_STATE_TAX_PMT_AGENCY_NBR'
        DataSource = wwdsMaster
        LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
        LookupField = 'SY_GLOBAL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 5
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object DBRadioGroup2: TevDBRadioGroup
        Left = 8
        Top = 336
        Width = 185
        Height = 57
        HelpContext = 1572
        Caption = '~Tax Deposit Follow Federal'
        DataField = 'TAX_DEPOSIT_FOLLOW_FEDERAL'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 14
        Values.Strings = (
          'Y'
          'N')
        Visible = False
      end
      object dbedtSTATE: TevDBEdit
        Left = 8
        Top = 20
        Width = 33
        Height = 21
        HelpContext = 1568
        DataField = 'STATE'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&,@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit3: TevDBEdit
        Left = 8
        Top = 60
        Width = 145
        Height = 21
        HelpContext = 1568
        DataField = 'NAME'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeState_Minimum_Wage: TevDBEdit
        Left = 8
        Top = 224
        Width = 64
        Height = 21
        HelpContext = 1503
        DataField = 'STATE_MINIMUM_WAGE'
        DataSource = wwdsMaster
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeSupplemental_Wages_Percent: TevDBEdit
        Left = 8
        Top = 272
        Width = 64
        Height = 21
        HelpContext = 1506
        DataField = 'SUPPLEMENTAL_WAGES_PERCENTAGE'
        DataSource = wwdsMaster
        TabOrder = 9
        UnboundDataType = wwDefault
        UsePictureMask = False
        WantReturns = False
        WordWrap = False
      end
      object wwdeState_Tip_Credit: TevDBEdit
        Left = 144
        Top = 272
        Width = 64
        Height = 21
        HelpContext = 1509
        DataField = 'STATE_TIP_CREDIT'
        DataSource = wwdsMaster
        TabOrder = 10
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBRadioGroup5: TevDBRadioGroup
        Left = 432
        Top = 264
        Width = 185
        Height = 57
        HelpContext = 1573
        Caption = '~Round to the Nearest Dollar'
        DataField = 'ROUND_TO_NEAREST_DOLLAR'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 12
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBEdit1: TevDBEdit
        Left = 8
        Top = 170
        Width = 57
        Height = 21
        DataField = 'FIPS_CODE'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 432
        Top = 195
        Width = 185
        Height = 57
        HelpContext = 1573
        Caption = '~Print W2'
        DataField = 'PRINT_W2'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 11
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBRadioGroup2: TevDBRadioGroup
        Left = 224
        Top = 264
        Width = 185
        Height = 57
        HelpContext = 1511
        Caption = '~Cap State Tax Credit'
        DataField = 'CAP_STATE_TAX_CREDIT'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 13
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBRadioGroup3: TevDBRadioGroup
        Left = 248
        Top = 195
        Width = 161
        Height = 57
        HelpContext = 1572
        Caption = 'Inactivate marital status'
        DataField = 'INACTIVATE_MARITAL_STATUS'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 15
        Values.Strings = (
          'Y'
          'N')
        Visible = False
      end
    end
    object TabSheet2x: TTabSheet
      Caption = 'EFT'
      object Label42: TevLabel
        Left = 132
        Top = 232
        Width = 31
        Height = 13
        Caption = 'Payroll'
        FocusControl = DBEdit40
        Visible = False
      end
      object Label43: TevLabel
        Left = 132
        Top = 276
        Width = 26
        Height = 13
        Caption = 'Sales'
        FocusControl = DBEdit41
        Visible = False
      end
      object Label44: TevLabel
        Left = 132
        Top = 320
        Width = 46
        Height = 13
        Caption = 'Corporate'
        FocusControl = DBEdit42
        Visible = False
      end
      object Label45: TevLabel
        Left = 396
        Top = 232
        Width = 39
        Height = 13
        Caption = 'Railroad'
        FocusControl = DBEdit43
        Visible = False
      end
      object Label46: TevLabel
        Left = 396
        Top = 276
        Width = 70
        Height = 13
        Caption = 'Unemployment'
        FocusControl = DBEdit44
        Visible = False
      end
      object Label47: TevLabel
        Left = 396
        Top = 320
        Width = 26
        Height = 13
        Caption = 'Other'
        FocusControl = DBEdit45
        Visible = False
      end
      object Label1x: TevLabel
        Left = 8
        Top = 12
        Width = 57
        Height = 16
        Caption = '~State EFT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TevLabel
        Left = 8
        Top = 116
        Width = 50
        Height = 16
        Caption = '~SUI EFT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBEdit40: TevDBEdit
        Left = 132
        Top = 248
        Width = 52
        Height = 21
        HelpContext = 1517
        DataField = 'PAYROLL'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object DBEdit41: TevDBEdit
        Left = 132
        Top = 292
        Width = 52
        Height = 21
        HelpContext = 1518
        DataField = 'SALES'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object DBEdit42: TevDBEdit
        Left = 132
        Top = 336
        Width = 52
        Height = 21
        HelpContext = 1519
        DataField = 'CORPORATE'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object DBEdit43: TevDBEdit
        Left = 396
        Top = 248
        Width = 52
        Height = 21
        HelpContext = 1520
        DataField = 'RAILROAD'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 5
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object DBEdit44: TevDBEdit
        Left = 396
        Top = 292
        Width = 52
        Height = 21
        HelpContext = 1521
        DataField = 'UNEMPLOYMENT'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 6
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object DBEdit45: TevDBEdit
        Left = 396
        Top = 336
        Width = 52
        Height = 21
        HelpContext = 1522
        DataField = 'OTHER'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 7
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object wwDBComboBox1: TevDBComboBox
        Left = 8
        Top = 32
        Width = 105
        Height = 21
        HelpContext = 1513
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'STATE_EFT_TYPE'
        DataSource = wwdsMaster
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'EFT Credit'#9'C'
          'EFT Debit'#9'D'
          'None'#9'N'
          'Both'#9'S')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object wwDBComboBox2: TevDBComboBox
        Left = 8
        Top = 136
        Width = 105
        Height = 21
        HelpContext = 1515
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SUI_EFT_TYPE'
        DataSource = wwdsMaster
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'EFT Credit'#9'C'
          'EFT Debit'#9'D'
          'None'#9'N'
          'Both'#9'S')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'SDI'
      object Label19: TevLabel
        Left = 128
        Top = 108
        Width = 104
        Height = 13
        Caption = 'SDI Weekly Maximum'
      end
      object Label22: TevLabel
        Left = 118
        Top = 156
        Width = 114
        Height = 13
        Caption = 'EE SDI Maximum Wage'
      end
      object Label25: TevLabel
        Left = 117
        Top = 204
        Width = 115
        Height = 13
        Caption = 'ER SDI Maximum Wage'
      end
      object Label28: TevLabel
        Left = 171
        Top = 132
        Width = 61
        Height = 13
        Caption = 'EE SDI Rate'
      end
      object Label31: TevLabel
        Left = 170
        Top = 180
        Width = 62
        Height = 13
        Caption = 'ER SDI Rate'
      end
      object Label12: TevLabel
        Left = 136
        Top = 28
        Width = 95
        Height = 16
        Caption = '~Payroll Driven SDI'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBComboBox3: TevDBComboBox
        Left = 250
        Top = 24
        Width = 194
        Height = 21
        HelpContext = 1524
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SDI_RECIPROCATE'
        DataSource = wwdsMaster
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Yes'#9'Y'
          'No'#9'N'
          'Other'#9'O'
          'Only With Reciprocal States'#9'S')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object wwdeSDI_Maximum_Weekly: TevDBEdit
        Left = 237
        Top = 104
        Width = 92
        Height = 21
        HelpContext = 1525
        DataField = 'WEEKLY_SDI_TAX_CAP'
        DataSource = wwdsMaster
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeEE_SDI_Rate: TevDBEdit
        Left = 237
        Top = 128
        Width = 92
        Height = 21
        HelpContext = 1528
        DataField = 'EE_SDI_RATE'
        DataSource = wwdsMaster
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeER_SDI_Rate: TevDBEdit
        Left = 237
        Top = 176
        Width = 92
        Height = 21
        HelpContext = 1534
        DataField = 'ER_SDI_RATE'
        DataSource = wwdsMaster
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeER_SDI_Maximum_Wage: TevDBEdit
        Left = 237
        Top = 200
        Width = 92
        Height = 21
        HelpContext = 1537
        DataField = 'ER_SDI_MAXIMUM_WAGE'
        DataSource = wwdsMaster
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeEE_SDI_Maximum_Wage: TevDBEdit
        Left = 237
        Top = 152
        Width = 92
        Height = 21
        HelpContext = 1531
        DataField = 'EE_SDI_MAXIMUM_WAGE'
        DataSource = wwdsMaster
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Garnishment/Child Support'
      object Label36: TevLabel
        Left = 90
        Top = 68
        Width = 126
        Height = 16
        Caption = '~Garnishment Maximum %'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label39: TevLabel
        Left = 14
        Top = 100
        Width = 188
        Height = 16
        Caption = '~Garnishment Minimum Wage Multiplier'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBRadioGroup10: TevDBRadioGroup
        Left = 292
        Top = 188
        Width = 225
        Height = 57
        HelpContext = 1547
        Caption = '~UIFSA New Hire'
        DataField = 'UIFSA_NEW_HIRE_REPORTING'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup11: TevDBRadioGroup
        Left = 44
        Top = 188
        Width = 225
        Height = 57
        HelpContext = 1548
        Caption = '~Use State Minimum Wage'
        DataField = 'DD_CHILD_SUPPORT'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
      object wwdeGarnish_Maximum_Percent: TevDBEdit
        Left = 232
        Top = 64
        Width = 64
        Height = 21
        HelpContext = 1541
        DataField = 'MAXIMUM_GARNISHMENT_PERCENT'
        DataSource = wwdsMaster
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeGarnish_Min_Wage_Multiplier: TevDBEdit
        Left = 232
        Top = 96
        Width = 64
        Height = 21
        HelpContext = 1544
        DataField = 'GARNISH_MIN_WAGE_MULTIPLIER'
        DataSource = wwdsMaster
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Sales Tax'
      object Label51: TevLabel
        Left = 24
        Top = 32
        Width = 67
        Height = 16
        Caption = '~Sales Tax %'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwdeSales_Tax_Percent: TevDBEdit
        Left = 24
        Top = 48
        Width = 64
        Height = 21
        HelpContext = 1549
        DataField = 'SALES_TAX_PERCENTAGE'
        DataSource = wwdsMaster
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'SUI Flags'
      object Label14: TevLabel
        Left = 8
        Top = 20
        Width = 88
        Height = 16
        Caption = '~Reciprocate SUI'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TevLabel
        Left = 8
        Top = 280
        Width = 138
        Height = 13
        Caption = 'SUI Tax Payment Type Code'
      end
      object DBRadioGroup8: TevDBRadioGroup
        Left = 8
        Top = 76
        Width = 209
        Height = 57
        HelpContext = 1553
        Caption = '~Print SUI Return if Zero'
        DataField = 'PRINT_SUI_RETURN_IF_ZERO'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup7: TevDBRadioGroup
        Left = 8
        Top = 144
        Width = 209
        Height = 57
        HelpContext = 1553
        Caption = '~Show S125 in Gross Wages'
        DataField = 'SHOW_S125_IN_GROSS_WAGES_SUI'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
      object wwDBComboBox4: TevDBComboBox
        Left = 8
        Top = 40
        Width = 209
        Height = 21
        HelpContext = 1553
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SUI_RECIPROCATE'
        DataSource = wwdsMaster
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Yes'#9'Y'
          'No'#9'N'
          'Other'#9'O'
          'Only With Reciprocal States'#9'S')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object DBRadioGroup1: TevDBRadioGroup
        Left = 8
        Top = 212
        Width = 209
        Height = 57
        HelpContext = 1553
        Caption = '~Pay With SDI'
        DataField = 'PAY_SDI_WITH'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup6: TevDBRadioGroup
        Left = 8
        Top = 324
        Width = 209
        Height = 57
        HelpContext = 1553
        Caption = 'Increment SUI Tax Payment Type Code'
        DataField = 'INC_SUI_TAX_PAYMENT_CODE'
        DataSource = wwdsMaster
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 5
        Values.Strings = (
          'Y'
          'N')
        Visible = False
      end
      object wwDBEdit1: TevDBEdit
        Left = 8
        Top = 296
        Width = 89
        Height = 21
        HelpContext = 1549
        DataField = 'SUI_TAX_PAYMENT_TYPE_CODE'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
    object tsReciprocation: TTabSheet
      Caption = 'Reciprocation'
      ImageIndex = 7
      object evLabel2: TevLabel
        Left = 3
        Top = 315
        Width = 25
        Height = 13
        Caption = 'State'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel3: TevLabel
        Left = 265
        Top = 9
        Width = 93
        Height = 13
        Caption = 'Reciprocation Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object grRecStates: TevDBGrid
        Left = 3
        Top = 1
        Width = 256
        Height = 302
        DisableThemesInTitle = False
        Selected.Strings = (
          'PARTICIPANT_STATE_Lookup'#9'26'#9'Reciprocation State'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mDisableDialog]
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SY_STATES\grRecStates'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsRecStates
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      inline mnfRecStates: TMiniNavigationFrame
        Left = 190
        Top = 311
        Width = 65
        Height = 25
        TabOrder = 1
        inherited evActionList1: TevActionList
          Left = 16
          Top = 65528
        end
      end
      object cbStateList: TevDBLookupCombo
        Left = 47
        Top = 312
        Width = 81
        Height = 21
        HelpContext = 1569
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'STATE'#9'2'#9'STATE'#9'F')
        DataField = 'PARTICIPANT_STATE_NBR'
        DataSource = dsRecStates
        LookupTable = dsStates
        LookupField = 'SY_STATES_NBR'
        Style = csDropDownList
        TabOrder = 2
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object evDBComboBox1: TevDBComboBox
        Left = 265
        Top = 29
        Width = 209
        Height = 21
        HelpContext = 1553
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = True
        AutoDropDown = True
        DataField = 'RECIPROCITY_TYPE'
        DataSource = wwdsMaster
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Yes'#9'Y'
          'No'#9'N')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
    end
  end
  inherited Panel2: TevPanel
    Width = 541
  end
  inherited wwdsMaster: TevDataSource
    OnDataChange = wwdsMasterDataChange
    Left = 189
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
    Left = 230
  end
  inherited wwdsList: TevDataSource
    Left = 146
    Top = 18
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 324
    Top = 24
  end
  object dsRecStates: TevDataSource
    DataSet = DM_SY_RECIPROCATED_STATES.SY_RECIPROCATED_STATES
    OnStateChange = dsRecStatesStateChange
    Left = 296
    Top = 128
  end
  object dsStates: TevClientDataSet
    Left = 296
    Top = 160
  end
end
