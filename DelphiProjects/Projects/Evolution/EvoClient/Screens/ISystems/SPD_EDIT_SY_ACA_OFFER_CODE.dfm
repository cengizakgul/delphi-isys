inherited EDIT_SY_ACA_OFFER_CODE: TEDIT_SY_ACA_OFFER_CODE
  Width = 627
  Height = 393
  object lbCode: TevLabel [0]
    Left = 20
    Top = 272
    Width = 60
    Height = 16
    Caption = '~Offer Code'
  end
  object lblDesc: TevLabel [1]
    Left = 20
    Top = 320
    Width = 62
    Height = 16
    Caption = '~Description'
  end
  object wwDBGrid1: TevDBGrid [2]
    Left = 0
    Top = 0
    Width = 627
    Height = 253
    DisableThemesInTitle = False
    Selected.Strings = (
      'ACA_OFFER_CODE'#9'2'#9'Aca offer code'#9'F'
      'ACA_OFFER_CODE_DESCRIPTION'#9'40'#9'Aca offer code description'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_ACA_OFFER_CODE\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alTop
    DataSource = wwdsDetail
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object edtCode: TevDBEdit [3]
    Left = 20
    Top = 288
    Width = 52
    Height = 21
    HelpContext = 5524
    DataField = 'ACA_OFFER_CODE'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    TabOrder = 1
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  object edDesc: TevDBEdit [4]
    Left = 20
    Top = 336
    Width = 324
    Height = 21
    HelpContext = 5524
    DataField = 'ACA_OFFER_CODE_DESCRIPTION'
    DataSource = wwdsDetail
    Picture.PictureMaskFromDataSet = False
    TabOrder = 2
    UnboundDataType = wwDefault
    WantReturns = False
    WordWrap = False
    Glowing = False
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_FED_ACA_OFFER_CODES.SY_FED_ACA_OFFER_CODES
    MasterDataSource = nil
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 304
    Top = 32
  end
end
