// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_GL_AGENCY_REPORT;

interface

uses
  SFrameEntry,  StdCtrls, wwdblook, Buttons, ExtCtrls,
  DBCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb, Grids, Wwdbigrd, Wwdbgrid,
  Controls, ComCtrls, Classes, Db, Wwdatsrc, SDataStructure, EvConsts,
  SysUtils, SDDClasses, ISBasicClasses, SFieldCodeValues, SDataDictsystem,
  Wwdbspin, wwdbdatetimepicker, SPackageEntry, EvExceptions, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvContext, EvUIComponents, EvClientDataSet;

type
  TEDIT_SY_GL_AGENCY_REPORT = class(TFrameEntry)
    PageControl1: TevPageControl;
    Panel1: TevPanel;
    Splitter1: TevSplitter;
    wwDBGrid1: TevDBGrid;
    Panel2: TevPanel;
    Splitter2: TevSplitter;
    wwDBGrid3: TevDBGrid;
    TabSheet1: TTabSheet;
    tsDetail: TTabSheet;
    bevCL_1: TEvBevel;
    Label2: TevLabel;
    wwdbcbxFrequency: TevDBComboBox;
    Label1: TevLabel;
    wwdbcbxTaxType: TevDBComboBox;
    Label16: TevLabel;
    DBText1: TevDBText;
    Label3: TevLabel;
    wwdbcbxWhenDueType: TevDBComboBox;
    labNbdays: TevLabel;
    dbedNbDays: TevDBEdit;
    Label5: TevLabel;
    cdReturnFreq: TevDBComboBox;
    rgEnlistAuto: TevDBRadioGroup;
    cbPrintWhen: TevDBComboBox;
    Label7: TevLabel;
    Label9: TevLabel;
    cbCoFilter: TevDBComboBox;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    wwdsSubMaster: TevDataSource;
    cbCoPaymentMethod: TevDBComboBox;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBComboBox2: TevDBComboBox;
    cbTax945Filter: TevDBComboBox;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    cbTax943Filter: TevDBComboBox;
    wwDBGrid4: TevDBGrid;
    evLabel7: TevLabel;
    cbTax944Filter: TevDBComboBox;
    cbSystemReportsGroup: TevDBLookupCombo;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel10: TevLabel;
    evDBSpinEdit1: TevDBSpinEdit;
    evDBSpinEdit2: TevDBSpinEdit;
    tsOldRecords: TTabSheet;
    EvBevel1: TEvBevel;
    evDBGrid2: TevDBGrid;
    evDBGrid3: TevDBGrid;
    evLabel8: TevLabel;
    evDBComboBox1: TevDBComboBox;
    evLabel9: TevLabel;
    evDBComboBox3: TevDBComboBox;
    evLabel11: TevLabel;
    cbOldCoPaymentMethod: TevDBComboBox;
    evLabel12: TevLabel;
    evDBComboBox5: TevDBComboBox;
    evLabel13: TevLabel;
    cbOldTax945Filter: TevDBComboBox;
    evLabel14: TevLabel;
    cbOldTax943Filter: TevDBComboBox;
    evLabel15: TevLabel;
    cbOldTax944Filter: TevDBComboBox;
    evLabel16: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBComboBox9: TevDBComboBox;
    evLabel17: TevLabel;
    evDBComboBox10: TevDBComboBox;
    evLabel18: TevLabel;
    evDBComboBox11: TevDBComboBox;
    evLabel19: TevLabel;
    evDBComboBox12: TevDBComboBox;
    evLabel20: TevLabel;
    rgTaxReturnActive: TevDBRadioGroup;
    evLabel21: TevLabel;
    rgInactiveStartDate: TevDBDateTimePicker;
    evDBRadioGroup2: TevDBRadioGroup;
    btnFieldoffice: TevBitBtn;
    grdSyField: TevDBGrid;
    dsSYFieldOffice: TevDataSource;
    cdSyFieldOffice: TevClientDataSet;
    cdSyFieldOfficeAGENCY_NAME: TStringField;
    cdSyFieldOfficeSTATE: TStringField;
    cdSyFieldOfficeADDITIONAL_NAME: TStringField;
    cdSyFieldOfficeCITY: TStringField;
    cdSyFieldOfficeSY_GLOBAL_AGENCY_NBR: TIntegerField;
    cdSyFieldOfficeSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField;
    procedure wwdbcbxWhenDueTypeChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure cbSystemReportsGroupCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure btnFieldofficeClick(Sender: TObject);
    procedure Insert_SyFieldOfficeSetup(const grd: TevDBGrid);
    procedure wwdsDetailStateChange(Sender: TObject);
  protected
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

{$R *.DFM}

uses EvUtils;

function TEDIT_SY_GL_AGENCY_REPORT.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT;
end;


procedure TEDIT_SY_GL_AGENCY_REPORT.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  case Kind of
    NavOk, NavCommit:
    begin
      if wwdsDetail.DataSet.FieldByName('WHEN_DUE_TYPE').AsString = WHEN_DUE_SOME_DAYS_AFTER_THIS_EVENT then
       if wwdsDetail.DataSet.FieldByName('NUMBER_OF_DAYS_FOR_WHEN_DUE').IsNull then
         raise EUpdateError.CreateHelp('You must enter Number of Days for When Due field!', IDH_ConsistencyViolation);
    end;
  end;
  inherited;
end;

procedure TEDIT_SY_GL_AGENCY_REPORT.wwdbcbxWhenDueTypeChange(
  Sender: TObject);
var
  sDueType: string;
begin
  inherited;
  sDueType := wwdbcbxWhenDueType.GetComboValue(wwdbcbxWhenDueType.Text);
  dbedNbDays.Enabled := (sDueType = WHEN_DUE_SOME_DAYS_AFTER_THIS_EVENT) or
    (sDueType = WHEN_DUE_SOME_DAYS_BEFORE_NEXT_EVENT);
  labNbDays.Enabled := dbedNbDays.Enabled;
end;



function TEDIT_SY_GL_AGENCY_REPORT.GetInsertControl: TWinControl;
begin
  Result := cbSystemReportsGroup;
end;

procedure TEDIT_SY_GL_AGENCY_REPORT.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS_GROUP, SupportDataSets, 'SY_REPORTS_GROUP_NBR > 20000');
end;

procedure TEDIT_SY_GL_AGENCY_REPORT.Activate;
var
 s : String;
begin
  inherited;
    s := 'select ' +
      'b.AGENCY_NAME, b.STATE, a.ADDITIONAL_NAME,a.CITY, a.SY_GLOBAL_AGENCY_NBR, a.SY_GL_AGENCY_FIELD_OFFICE_NBR ' +
    'from SY_GL_AGENCY_FIELD_OFFICE a, SY_GLOBAL_AGENCY b ' +
      'where {AsOfNow<a>} and {AsOfNow<b>} and ' +
          'a.SY_GLOBAL_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR ';

     with TExecDSWrapper.Create(s) do
     begin
       cdSyFieldOffice.Close;
       ctx_DataAccess.GetSyCustomData(cdSyFieldOffice, AsVariant);
     end;

  PageControl1.ActivePage := TabSheet1;

  cbOldCoPaymentMethod.Items.Text := ReturnAgPaymentMethod_ComboChoices;
  cbOldTax945Filter.Items.Text := TAX945TaxCheckFilter_ComboChoices;
  cbOldTax943Filter.Items.Text := GroupBox_YesNoDontcare_ComboChoices;
  cbOldTax944Filter.Items.Text := GroupBox_YesNoDontcare_ComboChoices;

end;

procedure TEDIT_SY_GL_AGENCY_REPORT.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
var
  s: string;
begin
  inherited;
  if not Assigned(Field) and (cbOldCoPaymentMethod.Tag = 0) then
  begin
    cbOldCoPaymentMethod.Tag := 1;
    try
      s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 1);
      if (s <> '') and (ReturnDescription(ReturnAgPaymentMethod_ComboChoices, s) <> '') then
        cbOldCoPaymentMethod.Value := s
      else
        cbOldCoPaymentMethod.Value := AG_PAYMENT_METHOD_DONT_CARE;
      s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 2, 1);
      if (s <> '') and (ReturnDescription(TAX945TaxCheckFilter_ComboChoices, s) <> '') then
        cbOldTax945Filter.Value := s
      else
        cbOldTax945Filter.Value := TAX945_CHECKS_DONT_CARE;
      s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 3, 1);
      if (s <> '') and (ReturnDescription(GroupBox_YesNoDontcare_ComboChoices, s) <> '') then
        cbOldTax943Filter.Value := s
      else
        cbOldTax943Filter.Value := GROUP_BOX_NOT_APPLICATIVE;
      s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 4, 1);
      if (s <> '') and (ReturnDescription(GroupBox_YesNoDontcare_ComboChoices, s) <> '') then
        cbOldTax944Filter.Value := s
      else
        cbOldTax944Filter.Value := GROUP_BOX_NOT_APPLICATIVE;

    finally
      cbOldCoPaymentMethod.Tag := 0;
    end;
  end;

end;

procedure TEDIT_SY_GL_AGENCY_REPORT.PageControl1Change(Sender: TObject);
begin
  inherited;

  if PageControl1.ActivePage = tsDetail then
  begin
   DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.Filter := ' SY_GL_AGENCY_REPORT_NBR > 7000';
   DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.Filtered := True;
  end;
  if PageControl1.ActivePage = tsOldRecords then
  begin
   DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.Filter := ' SY_GL_AGENCY_REPORT_NBR <= 7000';
   DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.Filtered := True;
  end;
  if PageControl1.ActivePage = TabSheet1 then
  begin
   DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.Filter := '';
   DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.Filtered := False;
  end;

end;

procedure TEDIT_SY_GL_AGENCY_REPORT.cbSystemReportsGroupCloseUp(
  Sender: TObject; LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if wwdsDetail.DataSet.State = dsInsert then
    if Pos('940', DM_SYSTEM_MISC.SY_REPORTS_GROUP.FieldByName('NAME').AsString) <> 0 then
      wwdsDetail.DataSet.FieldByName('TAXRETURN940').AsString := 'Y';
end;

procedure TEDIT_SY_GL_AGENCY_REPORT.btnFieldofficeClick(Sender: TObject);
begin
  inherited;
  Insert_SyFieldOfficeSetup(grdSyField);
end;

procedure TEDIT_SY_GL_AGENCY_REPORT.Insert_SyFieldOfficeSetup(const grd: TevDBGrid);
var
  ds: TevClientDataSet;

  procedure Process;
  begin
   ds.Insert;
   ds['SYSTEM_TAX_TYPE'] := wwdsDetail.DataSet.FieldByName('SYSTEM_TAX_TYPE').AsString;
   ds['DEPOSIT_FREQUENCY'] := wwdsDetail.DataSet.FieldByName('DEPOSIT_FREQUENCY').AsString;
   ds['SY_GL_AGENCY_FIELD_OFFICE_NBR'] :=
        grd.DataSource.DataSet.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').AsInteger;
   ds['WHEN_DUE_TYPE'] := wwdsDetail.DataSet.FieldByName('WHEN_DUE_TYPE').AsString;
   ds['NUMBER_OF_DAYS_FOR_WHEN_DUE'] := wwdsDetail.DataSet.FieldByName('NUMBER_OF_DAYS_FOR_WHEN_DUE').AsInteger;
   ds['RETURN_FREQUENCY'] := wwdsDetail.DataSet.FieldByName('RETURN_FREQUENCY').AsString;
   ds['ENLIST_AUTOMATICALLY'] := wwdsDetail.DataSet.FieldByName('ENLIST_AUTOMATICALLY').AsString;
   ds['PRINT_WHEN'] := wwdsDetail.DataSet.FieldByName('PRINT_WHEN').AsString;
   ds['TAX_SERVICE_FILTER'] := wwdsDetail.DataSet.FieldByName('TAX_SERVICE_FILTER').AsString;
   ds['CONSOLIDATED_FILTER'] := wwdsDetail.DataSet.FieldByName('CONSOLIDATED_FILTER').AsString;
   ds['SY_REPORTS_GROUP_NBR'] := wwdsDetail.DataSet.FieldByName('SY_REPORTS_GROUP_NBR').AsInteger;
   ds['TAX_RETURN_ACTIVE'] := wwdsDetail.DataSet.FieldByName('TAX_RETURN_ACTIVE').AsString;
   ds['BEGIN_EFFECTIVE_MONTH'] := wwdsDetail.DataSet.FieldByName('BEGIN_EFFECTIVE_MONTH').AsInteger;
   ds['END_EFFECTIVE_MONTH'] := wwdsDetail.DataSet.FieldByName('END_EFFECTIVE_MONTH').AsInteger;
   ds['PAYMENT_METHOD'] := wwdsDetail.DataSet.FieldByName('PAYMENT_METHOD').AsString;
   ds['TAX945_CHECKS'] := wwdsDetail.DataSet.FieldByName('TAX945_CHECKS').AsString;
   ds['TAX943_COMPANY'] := wwdsDetail.DataSet.FieldByName('TAX943_COMPANY').AsString;
   ds['TAX944_COMPANY'] := wwdsDetail.DataSet.FieldByName('TAX944_COMPANY').AsString;
   ds['TAXRETURN940'] := wwdsDetail.DataSet.FieldByName('TAXRETURN940').AsString;
   ds['TAX_RETURN_INACTIVE_START_DATE'] := wwdsDetail.DataSet.FieldByName('TAX_RETURN_INACTIVE_START_DATE').AsDateTime;
   ds.Post;
  end;
Var
  i: Integer;
begin
    ds := TevClientDataSet.Create(nil);
    try
      ds.ProviderName := DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.ProviderName;
      ds.DataRequired('ALL');
      if grd.SelectedList.Count > 0 then
      begin
        for i := grd.SelectedList.Count-1 downto 0 do
        begin
          grd.DataSource.DataSet.GotoBookmark(grd.SelectedList[i]);
          Process;
        end;
        ctx_DataAccess.PostDataSets([ds]);
      end;
    finally
      grd.UnselectAll;
      ds.Free;
    end;
end;

procedure TEDIT_SY_GL_AGENCY_REPORT.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.State = dsBrowse then
     btnFieldoffice.Enabled := True
  else
     btnFieldoffice.Enabled := False;
end;

initialization
  RegisterClass(TEDIT_SY_GL_AGENCY_REPORT);

end.
