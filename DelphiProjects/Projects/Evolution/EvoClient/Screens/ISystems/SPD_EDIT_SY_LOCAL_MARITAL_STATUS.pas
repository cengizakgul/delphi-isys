// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_LOCAL_MARITAL_STATUS;

interface

uses
  SDataStructure,  SPD_EDIT_BASE_SY_LOCAL, wwdbdatetimepicker,
  wwdblook, StdCtrls, Mask, DBCtrls, Db, Wwdatsrc, Grids, Wwdbigrd,
  Wwdbgrid, Controls, ExtCtrls, ComCtrls, Classes, wwdbedit,
  ISBasicClasses, SDDClasses, SDataDictsystem, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_LOCAL_MARITAL_STATUS = class(TEDIT_BASE_SY_LOCAL)
    TabSheet3: TTabSheet;
    wwDBGrid3: TevDBGrid;
    Label3: TevLabel;
    Label4: TevLabel;
    dbedtSTATUS_DESCRIPTION: TevDBEdit;
    Label8: TevLabel;
    wwdeStandard_Deduct_Minimum_Amount: TevDBEdit;
    Label6: TevLabel;
    wwdeStandard_Deduction_Percent_Gross: TevDBEdit;
    wwdlStatus: TevDBLookupCombo;
    evMaritalStatus: TevDataSource;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_SY_LOCAL_MARITAL_STATUS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_LOCAL.SY_LOCAL_MARITAL_STATUS;
end;

function TEDIT_SY_LOCAL_MARITAL_STATUS.GetInsertControl: TWinControl;
begin
  Result := wwdlStatus;
end;

procedure TEDIT_SY_LOCAL_MARITAL_STATUS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SY_LOCAL_MARITAL_STATUS);

end.
