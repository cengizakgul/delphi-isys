inherited EDIT_SY_GL_HOLIDAYS: TEDIT_SY_GL_HOLIDAYS
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 18
    Width = 628
    Height = 442
    HelpContext = 53501
    ActivePage = tsBrowse
    TabOrder = 0
    object tsBrowse: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 617
        Height = 410
        TabOrder = 0
        object wwDBGrid1: TevDBGrid
          Left = 1
          Top = 1
          Width = 615
          Height = 408
          DisableThemesInTitle = False
          Selected.Strings = (
            'AGENCY_NAME'#9'40'#9'Agency'
            'COUNTY'#9'40'#9'Locality'
            'STATE'#9'2'#9'State')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_SY_GL_HOLIDAYS\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = wwdsMaster
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
      end
    end
    object tsDetail: TTabSheet
      Caption = 'Detail'
      object Label1: TevLabel
        Left = 16
        Top = 256
        Width = 82
        Height = 13
        AutoSize = False
        Caption = '~Holiday name'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TevLabel
        Left = 16
        Top = 367
        Width = 105
        Height = 13
        AutoSize = False
        Caption = 'Week number '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TevLabel
        Left = 16
        Top = 392
        Width = 73
        Height = 13
        AutoSize = False
        Caption = 'Day Of week'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel1: TevLabel
        Left = 16
        Top = 331
        Width = 105
        Height = 13
        AutoSize = False
        Caption = 'Month number  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel2: TevLabel
        Left = 328
        Top = 331
        Width = 74
        Height = 13
        AutoSize = False
        Caption = 'Day number  '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel3: TevLabel
        Left = 328
        Top = 367
        Width = 80
        Height = 13
        AutoSize = False
        Caption = 'Saturday offset'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel4: TevLabel
        Left = 328
        Top = 392
        Width = 78
        Height = 13
        AutoSize = False
        Caption = 'Sunday offset'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel5: TevLabel
        Left = 16
        Top = 280
        Width = 97
        Height = 13
        AutoSize = False
        Caption = '~Holiday Rule'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object edName: TevDBEdit
        Left = 152
        Top = 248
        Width = 217
        Height = 21
        DataField = 'HOLIDAY_NAME'
        DataSource = wwdsDetail
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object edDayNumber: TevDBEdit
        Left = 464
        Top = 323
        Width = 75
        Height = 21
        DataField = 'DAY_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evCalcType: TevDBComboBox
        Left = 152
        Top = 276
        Width = 216
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'CALC_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object edMonthNumber: TevDBComboBox
        Left = 152
        Top = 323
        Width = 142
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'MONTH_NUMBER'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'0'
          'January'#9'1'
          'February'#9'2'
          'March'#9'3'
          'April'#9'4'
          'May'#9'5'
          'June'#9'6'
          'July'#9'7'
          'August'#9'8'
          'September'#9'9'
          'October'#9'10'
          'November'#9'11'
          'December'#9'12')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object edWeekNumber: TevDBComboBox
        Left = 152
        Top = 359
        Width = 142
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'WEEK_NUMBER'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'0'
          '1. week'#9'1'
          '2. week'#9'2'
          '3. week'#9'3'
          '4. week'#9'4'
          '5. week'#9'5'
          'Reverse 1. week'#9'-1'
          'Reverse 2. week'#9'-2'
          'Reverse 3. week'#9'-3'
          'Reverse 4. week'#9'-4'
          'Reverse 5. week'#9'-5')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 4
        UnboundDataType = wwDefault
      end
      object edDayofWeek: TevDBComboBox
        Left = 152
        Top = 384
        Width = 142
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'DAY_OF_WEEK'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'0'
          'Monday'#9'1'
          'Tuesday'#9'2'
          'Wednesday'#9'3'
          'Thrusday'#9'4'
          'Friday'#9'5'
          'Saturday'#9'6'
          'Sunday'#9'7')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object edSuturdayOffset: TevDBComboBox
        Left = 464
        Top = 359
        Width = 142
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SATURDAY_OFFSET'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Pay taxes Friday'#9'-1'
          'Pay taxes Monday'#9'2'
          'Pay taxes on the date'#9'0')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object edSundayOffset: TevDBComboBox
        Left = 464
        Top = 384
        Width = 142
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SUNDAY_OFFSET'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Pay taxes  Friday'#9'-2'
          'Pay taxes  Monday'#9'1'
          'Pay taxes on the date'#9'0')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object Panel2: TevPanel
        Left = 0
        Top = 0
        Width = 620
        Height = 231
        Align = alTop
        TabOrder = 8
        object wwDBGrid2: TevDBGrid
          Left = 1
          Top = 1
          Width = 618
          Height = 229
          DisableThemesInTitle = False
          Selected.Strings = (
            'HOLIDAY_NAME'#9'40'#9'Holiday Name'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_SY_GL_HOLIDAYS\wwDBGrid2'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = wwdsDetail
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
      end
      object btnAgencyCopy: TevBitBtn
        Left = 416
        Top = 265
        Width = 161
        Height = 25
        Caption = 'Copy holiday to other Agency'
        TabOrder = 9
        OnClick = btnAgencyCopyClick
      end
      object cbupdate: TevCheckBox
        Left = 416
        Top = 240
        Width = 196
        Height = 17
        Caption = 'Update exist holiday name records'
        Checked = True
        State = cbChecked
        TabOrder = 10
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SY_GL_AGENCY_HOLIDAYS.SY_GL_AGENCY_HOLIDAYS
    OnDataChange = wwdsDetailDataChange
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 246
    Top = 18
  end
end
