unit SPD_PreSnap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Wwdbigrd, Wwdbgrid, ISBasicClasses, EvUIComponents,
  StdCtrls, ExtCtrls, LMDCustomButton, LMDButton, isUILMDButton,
  EvCommonInterfaces, EvContext, EvDataSet, DB, Wwdatsrc, isUIEdit,
  ActnList, EvTypes, SDDClasses, SDataDictsystem, SDataStructure,
  EvClasses;

type
  TPreSnap = class(TForm)
    pnlMain: TevPanel;
    dbgReports: TevDBCheckGrid;
    cmbVersion: TevComboBox;
    lblVersion: TevLabel;
    btnClose: TevBitBtn;
    dsVersions: TevDataSource;
    lblNewVersion: TevLabel;
    edNewVersion: TevEdit;
    btnUpdate: TevBitBtn;
    Actions: TActionList;
    acUpdate: TAction;
    procedure cmbVersionChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dbgReportsSelectionChanged(Sender: TObject);
    procedure acUpdateExecute(Sender: TObject);
    procedure acUpdateUpdate(Sender: TObject);
  private
    Versions: IevDataSet;
    FUpdated: boolean;

    procedure VerifyUnselectAllowed;
    procedure RefreshVersions;
    procedure RefreshReportsVersions;
    procedure UpdateVersion;
    function IsUpdated: boolean;
  end;

  function ShowPreSnapDialog: boolean;

implementation

uses isBaseClasses, isDataSet;

{$R *.dfm}

function ShowPreSnapDialog: boolean;
var
  PreSnap: TPreSnap;
begin
  PreSnap := TPreSnap.Create(nil);
  try
    PreSnap.ShowModal;
    Result := PreSnap.IsUpdated;
  finally
    PreSnap.Free;
  end;
end;

procedure TPreSnap.cmbVersionChange(Sender: TObject);
begin
  RefreshReportsVersions;
end;

procedure TPreSnap.RefreshVersions;
var
  Q: IevQuery;
  s: string;
begin
  ctx_StartWait('Refresh versions...');
  try
    s := 'select distinct v.db_version ' +
         'from sy_reports_versions v ' +
         'where {AsOfNow<v>} and not v.db_version is null and v.status = ''P''';
    Q := TevQuery.Create(s);
    Q.Execute;

    cmbVersion.Items.Clear;
    cmbVersion.Items.Add('<empty>');
    while not Q.Result.Eof do
    begin
      cmbVersion.Items.Add(Q.Result.FieldByName('db_version').AsString);
      Q.Result.Next;
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TPreSnap.FormCreate(Sender: TObject);
begin
  FUpdated := False;
  
  Versions := TevDataSet.Create;
  Versions.vclDataSet.FieldDefs.Add('Report Name', ftString, 40, true);
  Versions.vclDataSet.FieldDefs.Add('Nbr', ftInteger, 0, True);
  Versions.vclDataSet.FieldDefs.Add('Version', ftString, 20);
  Versions.vclDataSet.FieldDefs.Add('SY_REPORTS_VERSIONS_NBR', ftInteger, 0, True);
  Versions.vclDataSet.FieldDefs.Add('REC_VERSION', ftInteger, 0, True);
  Versions.vclDataSet.FieldDefs.Add('VersionCount', ftInteger, 0, True);
  dsVersions.DataSet := Versions.vclDataSet;

  dbgReports.Selected.Clear;
  dbgReports.Selected.Add('Report Name'#9'40'#9'Report Name'#9'F');
  dbgReports.Selected.Add('Nbr'#9'10'#9'Nbr'#9'F');
  dbgReports.Selected.Add('Version'#9'20'#9'Version'#9'F');
  dbgReports.ApplySelected;

//  ctx_StartWait('Refresh versions...');
//  try
    RefreshVersions;
    cmbVersion.ItemIndex := 0;
    RefreshReportsVersions;
//  finally
//    ctx_EndWait;
//  end;
end;

procedure TPreSnap.RefreshReportsVersions;
var
  Q: IevQuery;
  s: string;
begin
  if cmbVersion.ItemIndex > -1 then
  begin
    Versions.vclDataSet.Close;
    Versions.vclDataSet.DisableControls;
    ctx_StartWait('Refresh reports versions...');
    try
      s := 'select r.report_description, r.sy_report_writer_reports_nbr, v.db_version, v.sy_reports_versions_nbr, v.rec_version, c.VersionCount ' +
           'from sy_report_writer_reports r ' +
           '  join sy_reports_versions v on r.sy_report_writer_reports_nbr = v.sy_report_writer_reports_nbr ' +
           '  join (select t.sy_report_writer_reports_nbr, Count(distinct t.sy_reports_versions_nbr) VersionCount ' +
           '        from sy_reports_versions t ' +
           '        group by t.sy_report_writer_reports_nbr) c on r.sy_report_writer_reports_nbr = c.sy_report_writer_reports_nbr ' +
           'where {AsOfNow<r>} and {AsOfNow<v>} and v.status = ''P''';

      if cmbVersion.Text = '<empty>' then
        s := s + ' and v.db_version is null'
      else
        s := s + ' and v.db_version = ''' + cmbVersion.Text + '''';

      Q := TevQuery.Create(s);
      Q.Execute;
      Q.Result.First;

      Versions.vclDataSet.Open;
      while not Q.Result.eof do
      begin
        Versions.Append;
        Versions['Report Name'] := Q.Result['REPORT_DESCRIPTION'];
        Versions['Nbr'] := Q.Result['sy_report_writer_reports_nbr'];
        Versions['Version'] := Q.Result['db_version'];
        Versions['sy_reports_versions_nbr'] := Q.Result['sy_reports_versions_nbr'];
        Versions['rec_version'] := Q.Result['rec_version'];
        Versions['VersionCount'] := Q.Result['VersionCount'];
        Versions.Post;

        Q.Result.Next;
      end;
      Versions.vclDataSet.First;
    finally
      ctx_EndWait;
      dbgReports.SelectAll;
      Versions.vclDataSet.EnableControls;
    end;
  end;
end;

procedure TPreSnap.dbgReportsSelectionChanged(Sender: TObject);
begin
  if not dbgReports.IsSelectedRecord and (Versions.FieldByName('VersionCount').AsInteger < 2) then
  begin
    // this is the only report version and should go to out in the snap, do not allow to uncheck
    dbgReports.OnSelectionChanged := nil;
    dbgReports.SelectRecord;
    dbgReports.OnSelectionChanged := dbgReportsSelectionChanged;
  end;
end;

procedure TPreSnap.UpdateVersion;
var
  i: integer;
  ChangePacket: IevDataChangePacket;
  ChItem: IevDataChangeItem;
begin
  Versions.vclDataSet.DisableControls;
  ctx_StartWait('Update reports versions...');
  try
    VerifyUnselectAllowed;

    ChangePacket := TevDataChangePacket.Create;

    ctx_DataAccess.StartNestedTransaction([dbtSystem]);
    try
      for i := 0 to dbgReports.SelectedList.Count - 1 do
      begin
        Versions.vclDataSet.GotoBookmark( dbgReports.SelectedList[i] );
        ChItem := ChangePacket.NewUpdateFields('sy_reports_versions', Versions['SY_REPORTS_VERSIONS_NBR']);
        ChItem.Fields.AddValue('db_version', Trim(edNewVersion.Text));
      end;
      ctx_DBAccess.ApplyDataChangePacket(ChangePacket);
      ctx_DataAccess.CommitNestedTransaction;
      FUpdated := True;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
  finally
    ctx_EndWait;
    Versions.vclDataSet.EnableControls;
  end;
  RefreshVersions;
  cmbVersion.ItemIndex := cmbVersion.Items.IndexOf( Trim(edNewVersion.Text) );
  RefreshReportsVersions;
end;

procedure TPreSnap.acUpdateExecute(Sender: TObject);
begin
  UpdateVersion;
end;

procedure TPreSnap.acUpdateUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Trim(edNewVersion.Text) <> '';
end;

procedure TPreSnap.VerifyUnselectAllowed;
begin
  Versions.First;
  while not Versions.Eof do
  begin
    if not dbgReports.IsSelectedRecord and (Versions.FieldByName('VersionCount').AsInteger < 2) then
      dbgReports.SelectRecord;
    Versions.Next;
  end;
end;

function TPreSnap.IsUpdated: boolean;
begin
  Result := FUpdated;
end;

end.
