// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_STATE_DEPOSIT_FREQ;

interface

uses
  SDataStructure,  SPD_EDIT_SY_STATE_BASE, Db, 
   wwdblook, wwdbedit, Wwdotdot, Wwdbcomb, StdCtrls, ExtCtrls,
  DBCtrls, Mask, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls,
  Classes, SysUtils, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  SDDClasses, ISDataAccessComponents, isVCLBugFix,
  EvDataAccessComponents, SDataDictsystem, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit;

type
  TEDIT_SY_STATE_DEPOSIT_FREQ = class(TEDIT_SY_STATE_BASE)
    Label6: TevLabel;
    Label7: TevLabel;
    Label10: TevLabel;
    Label9: TevLabel;
    edDaysDue: TevDBEdit;
    Label8: TevLabel;
    Label11: TevLabel;
    edFirstTAmount: TevDBEdit;
    wwDBGrid2: TevDBGrid;
    rgStatusChange: TevDBRadioGroup;
    rgThirdMonthDue: TevDBRadioGroup;
    dbedtDescription: TevDBEdit;
    Label2: TevLabel;
    cbType: TevDBComboBox;
    cbDueType: TevDBComboBox;
    cbFirstTPeriod: TevDBComboBox;
    luFirstTFreq: TevDBLookupCombo;
    Label5: TevLabel;
    rgIncrementTypeCode: TevDBRadioGroup;
    edTypeCode: TevDBEdit;
    evLookupDS: TevClientDataSet;
    evLookupDSrc: TevDataSource;
    luFieldOffice: TevDBLookupCombo;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evLabel2: TevLabel;
    luTaxCoupon: TevDBLookupCombo;
    evLabel8: TevLabel;
    cbSystemReportsGroup: TevDBLookupCombo;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure Activate; override;
    procedure Deactivate; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure AfterClick(Kind: Integer); override;
  end;

implementation

{$R *.DFM}

uses EvConsts, EvUtils, SFrameEntry, SPackageEntry;

procedure TEDIT_SY_STATE_DEPOSIT_FREQ.Activate;
begin
  inherited;
  evLookupDS.RetrieveCondition := '';
  evLookupDS.CloneCursor(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ, True);
  evLookupDSrc.DataSet := evLookupDS;
  with ctx_DataAccess.SY_CUSTOM_VIEW do
  begin
    Close;
    with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 't1.sy_reports_nbr, t1.description');
      SetMacro('TABLE1', 'sy_reports');
      SetMacro('TABLE2', 'SY_REPORT_WRITER_REPORTS');
      SetMacro('JOINFIELD', 'SY_REPORT_WRITER_REPORTS');
      SetMacro('CONDITION', 't2.REPORT_TYPE = :T');
      SetParam('T', SYSTEM_REPORT_TYPE_TAXCOUPON);
      DataRequest(AsVariant);
    end;
    Open;
  end;
  luTaxCoupon.LookupTable := CreateLookupDataset(ctx_DataAccess.SY_CUSTOM_VIEW, Self);
end;

procedure TEDIT_SY_STATE_DEPOSIT_FREQ.Deactivate;
begin
  inherited;
  evLookupDS.Close;
end;

function TEDIT_SY_STATE_DEPOSIT_FREQ.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ;
end;

function TEDIT_SY_STATE_DEPOSIT_FREQ.GetInsertControl: TWinControl;
begin
  Result := dbedtDescription;
end;

procedure TEDIT_SY_STATE_DEPOSIT_FREQ.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_REPORTS_GROUP, SupportDataSets, 'SY_REPORTS_GROUP_NBR > 20000');
end;

procedure TEDIT_SY_STATE_DEPOSIT_FREQ.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = NavInsert then
  begin
    //SECOND_THRESHOLD_PERIOD field has been hide in GUI but requires a value
    if Assigned(wwdsDetail.DataSet) and Assigned(wwdsDetail.DataSet.FindField('SECOND_THRESHOLD_PERIOD')) and
      (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) then
      wwdsDetail.DataSet.FieldByName('SECOND_THRESHOLD_PERIOD').Value := TRESHOLD_NONE;

  end;
end;

initialization
  RegisterClass(TEDIT_SY_STATE_DEPOSIT_FREQ);

end.
