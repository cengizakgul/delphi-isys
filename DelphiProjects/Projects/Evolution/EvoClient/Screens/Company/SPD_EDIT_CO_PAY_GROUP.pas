// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_PAY_GROUP;

interface

uses
  SDataStructure, 
  wwdbedit, SPD_EDIT_CO_BASE, StdCtrls, Mask, Db, Wwdatsrc, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Controls, Classes,
  wwdblook, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, ISBasicClasses, isUIwwDBEdit, SDDClasses,
  SDataDictclient, ImgList, SDataDicttemp, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_PAY_GROUP = class(TEDIT_CO_BASE)
    tshtPay_Groups: TTabSheet;
    wwdgPay_Groups: TevDBGrid;
    tsVMR: TTabSheet;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpCOPayGroupSummary: TisUIFashionPanel;
    fpCOPayGroupDetails: TisUIFashionPanel;
    isUIFashionPanel2: TisUIFashionPanel;
    lablDescription: TevLabel;
    dedtDescription: TevDBEdit;
    evLabel20: TevLabel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel25: TevLabel;
    evDBLookupCombo7: TevDBLookupCombo;
    evDBLookupCombo5: TevDBLookupCombo;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo2: TevDBLookupCombo;
    sbPayGroupsDetail: TScrollBox;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TEDIT_CO_PAY_GROUP.Activate;
begin
  inherited;
  PageControl1.ActivePageIndex := 0;
  tsVMR.TabVisible := tsVMR.TabVisible and CheckVmrLicenseActive;
end;

procedure TEDIT_CO_PAY_GROUP.AfterDataSetsReopen;
begin
  inherited;
  tsVMR.TabVisible := CheckVmrLicenseActive and CheckVmrSetupActive;
  AssignVMRClientLookups(tsVMR);
end;

procedure TEDIT_CO_PAY_GROUP.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP, aDS);
end;

function TEDIT_CO_PAY_GROUP.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_PAY_GROUP;
end;

function TEDIT_CO_PAY_GROUP.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

initialization
  RegisterClass(TEDIT_CO_PAY_GROUP);

end.
