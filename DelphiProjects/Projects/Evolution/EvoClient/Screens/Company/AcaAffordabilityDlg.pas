unit AcaAffordabilityDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Wwdbigrd, Wwdbgrid, ISBasicClasses, EvUIComponents,
  ExtCtrls, isUIFashionPanel, StdCtrls, LMDCustomButton, LMDButton,
  isUILMDButton, ActnList, ComCtrls, DB, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvClientDataSet, Wwdatsrc,
  EvDataset, EvCommonInterfaces;

type
  TAcaAffordability = class(TForm)
    fpNewHireReport: TisUIFashionPanel;
    btnOK: TevBitBtn;
    btnClose: TevBitBtn;
    Actions: TActionList;
    acCalcAffordStatus: TAction;
    acPost: TAction;
    gbDateRange: TGroupBox;
    lblFrom: TLabel;
    lblTo: TLabel;
    edFrom: TDateTimePicker;
    edTo: TDateTimePicker;
    chbPrimaryRate: TCheckBox;
    btnCalc: TevBitBtn;
    dsAffordability: TevDataSource;
    grAffordability: TevDBGrid;
    Bevel1: TBevel;
    procedure acPostUpdate(Sender: TObject);
    procedure acCalcAffordStatusExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acPostExecute(Sender: TObject);
  private
    cdAfforabilityStatus: TevClientDataSet;
    function AppendEeInfo(const s: string): string;
  public
  end;

procedure ShowPostAcaAffordabilityDlg;

implementation

uses DateUtils, SReportSettings, EvStreamUtils, EvContext,
  SDataStructure, EvTypes, EvUIUtils, StrUtils, Math, EvUtils;

{$R *.dfm}

procedure ShowPostAcaAffordabilityDlg;
var
  dlg: TAcaAffordability;
begin
  dlg := TAcaAffordability.Create(nil);
  try
    dlg.edFrom.Date := StartOfTheYear(Today);
    dlg.edTo.Date := EndOfTheYear(Today);
    dlg.chbPrimaryRate.Checked := True;
    dlg.ShowModal;
  finally
    FreeAndNil(dlg);
  end;
end;

procedure TAcaAffordability.acPostUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := Assigned(cdAfforabilityStatus) and
    cdAfforabilityStatus.Active and (cdAfforabilityStatus.RecordCount > 0);
end;

procedure TAcaAffordability.acCalcAffordStatusExecute(Sender: TObject);
var
  ReportParams: TrwReportParams;
  ReportResult: TrwReportResults;
  ms: IisStream;
  ResText: string;
begin
  ReportParams := TrwReportParams.Create;
  try
    ReportParams.Add('Clients', VarArrayOf([ctx_DataAccess.ClientID]));
    ReportParams.Add('Companies', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger]));
    ReportParams.Add('dat_b', edFrom.Date);
    ReportParams.Add('dat_e', edTo.Date);
    ReportParams.Add('CalcMethod', 'A');
    ReportParams.Add('DetailSort', 'EE Code');
    ReportParams.Add('DisplayAsCSV', True);
    ReportParams.Add('UsePrimaryRate', chbPrimaryRate.Checked);

    ctx_RWLocalEngine.StartGroup;
    try
      ctx_RWLocalEngine.CalcPrintReport(3081, 'S', ReportParams);
    finally
      ms := ctx_RWLocalEngine.EndGroup(rdtNone);

      ReportResult := TrwReportResults.Create;
      try
        ReportResult.SetFromStream(ms);
        if (ReportResult.Count > 0) then
        begin
          if ReportResult.Items[0].ErrorMessage = '' then
          begin
//            ReportResult[0].Data.SaveToFile('C:\test1\res105119.txt');
            ResText := ReportResult[0].Data.AsString
          end
          else
            EvMessage('ACA Affordability Multiclient Report (S3081) raised an exception:'#13 + ReportResult.Items[0].ErrorMessage +
              #13#13'No affordability status info posted.');
        end;
      finally
        ReportResult.Free;
        ms := nil;
      end;
    end;
  finally
    ReportParams.Free;
  end;

  ctx_StartWait;
  cdAfforabilityStatus.DisableControls;
  try
    repeat
      ResText := AppendEeInfo(ResText);
    until (ResText = '');
  finally
    ctx_EndWait;
    cdAfforabilityStatus.EnableControls;
  end;
end;

function GetReportFieldValue(const s, sFieldName: string; var aValue: string): string;
var
  k, k2: integer;
begin
  Result := s;
  aValue := '';
  k := Pos(sFieldName, Result);
  if k <= 0 then
    Result := ''
  else begin
    Result := Copy(Result, k + Length(sFieldName), Length(Result));
    k := Pos(',', Result);
    k2 := Pos(#$D, Result);
    if (k > 0) and (k2 > 0) then
      k := Math.Min(k, k2)
    else
      k := Math.Max(k, k2);
    if k <= 0 then
    begin
      if (Result = 'Yes') or (Result = 'No') then
        aValue := Result;
      Result := ''
    end
    else begin
      aValue := Copy(Result, 1, k - 1);
      Result := Copy(Result, k + 1, Length(Result));
    end;
  end
end;

function TAcaAffordability.AppendEeInfo(const s: string): string;
const
  sEeCode = 'EE Code:,';
  sAffordabilityStatus = 'Affordability Status:,';
var
  EeCode: string;
  AffStIE: string;
  AffStFPL: string;
  AffStHR: string;
begin
  Result := Trim(GetReportFieldValue(s, sEeCode, EeCode));
  Result := GetReportFieldValue(Result, sAffordabilityStatus, AffStIE);
  Result := GetReportFieldValue(Result, sAffordabilityStatus, AffStFPL);
  Result := GetReportFieldValue(Result, sAffordabilityStatus, AffStHR);

  if (EeCode <> '') and (AffStIE <> '') and (AffStFPL <> '') and (AffStHR <> '') then
    cdAfforabilityStatus.AppendRecord([RightStr('         ' + EeCode, 9), AffStIE, AffStFPL, AffStHR]);
end;

procedure TAcaAffordability.FormDestroy(Sender: TObject);
begin
  cdAfforabilityStatus.Close;
  FreeAndNil(cdAfforabilityStatus);
end;

procedure TAcaAffordability.FormCreate(Sender: TObject);
begin
  cdAfforabilityStatus := TevClientDataSet.Create(nil);
  cdAfforabilityStatus.FieldDefs.Add('EeCode', ftString, 9, True);
  cdAfforabilityStatus.FieldDefs.Add('EarnedIncome', ftString, 3, True);
  cdAfforabilityStatus.FieldDefs.Add('FederalPovertyLevel', ftString, 3, True);
  cdAfforabilityStatus.FieldDefs.Add('HourlyRate', ftString, 3, True);
  cdAfforabilityStatus.CreateDataSet;
  cdAfforabilityStatus.LogChanges := False;
  dsAffordability.DataSet := cdAfforabilityStatus;
end;

procedure TAcaAffordability.acPostExecute(Sender: TObject);
var
  saveEE: TBookmark;
  i, k: integer;
begin
  ctx_StartWait;
  saveEE := DM_COMPANY.EE.GetBookmark;
  DM_COMPANY.EE.DisableControls;
  try
    DM_COMPANY.EE.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    DM_COMPANY.EE.First;
    k := DM_COMPANY.EE.RecordCount;
    i := 0;
    while not DM_COMPANY.EE.Eof do
    begin
      if cdAfforabilityStatus.Locate('EeCode', DM_COMPANY.EE.FieldValues['CUSTOM_EMPLOYEE_NUMBER'], []) then
      begin
        ctx_UpdateWait('Post Affordability statuses for EE #' + Trim(cdAfforabilityStatus.FieldByName('EeCode').AsString), i, k);
        DM_COMPANY.EE.Edit;
        DM_COMPANY.EE.FieldByName('ACA_EARNED_INCOME_AFFORDABILITY').AsString := cdAfforabilityStatus.FieldByName('EarnedIncome').AsString;
        DM_COMPANY.EE.FieldByName('ACA_FED_POV_AFFORDABILITY').AsString := cdAfforabilityStatus.FieldByName('FederalPovertyLevel').AsString;
        DM_COMPANY.EE.FieldByName('ACA_HOURLY_RATE_AFFORDABILITY').AsString := cdAfforabilityStatus.FieldByName('HourlyRate').AsString;
        DM_COMPANY.EE.Post;
      end;
      Inc(i);
      DM_COMPANY.EE.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_COMPANY.EE]);
  finally
    DM_COMPANY.EE.EnableControls;
    DM_COMPANY.EE.GotoBookmark( saveEE );
    DM_COMPANY.EE.FreeBookmark( saveEE );
    ctx_EndWait;
  end;
end;

end.
