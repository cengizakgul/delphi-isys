object SchedulerHelper: TSchedulerHelper
  Left = 613
  Top = 281
  BorderStyle = bsDialog
  Caption = 'Dates for Last Day of Month'
  ClientHeight = 317
  ClientWidth = 285
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 56
    Width = 284
    Height = 218
    BevelOuter = bvNone
    Caption = 'evPanel1'
    TabOrder = 0
    object evDBCheckGrid1: TevDBCheckGrid
      Left = 0
      Top = 0
      Width = 284
      Height = 218
      DisableThemesInTitle = False
      Selected.Strings = (
        'Description'#9'20'#9'Description'#9'F'
        'Date'#9'18'#9'Date'#9#9)
      IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
      IniAttributes.SectionName = 'TSchedulerHelper\evDBCheckGrid1'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = evDataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MultiSelectOptions = [msoShiftSelect]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
      PaintOptions.ActiveRecordColor = clBlack
      NoFire = False
    end
  end
  object evBitBtn1: TevBitBtn
    Left = 96
    Top = 283
    Width = 75
    Height = 25
    Caption = 'Done'
    ModalResult = 1
    TabOrder = 1
    Margin = 0
  end
  object evMemo1: TevMemo
    Left = 0
    Top = 0
    Width = 285
    Height = 57
    Align = alTop
    BevelInner = bvNone
    BevelOuter = bvNone
    BorderStyle = bsNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Lines.Strings = (
      '  Check the boxes to keep those dates as always '
      'falling on the last of the month.  If the box is not '
      'checked, the actual date chosen will be used '
      'instead.')
    ParentFont = False
    TabOrder = 2
  end
  object evDataSource1: TevDataSource
    DataSet = evClientDataSet1
    Left = 237
    Top = 72
  end
  object evClientDataSet1: TevClientDataSet
    FieldDefs = <
      item
        Name = 'Date'
        DataType = ftDateTime
      end
      item
        Name = 'Description'
        DataType = ftString
        Size = 20
      end>
    Left = 208
    Top = 64
    object evClientDataSet1Description: TStringField
      FieldName = 'Description'
    end
    object evClientDataSet1Date: TDateTimeField
      FieldName = 'Date'
    end
  end
end
