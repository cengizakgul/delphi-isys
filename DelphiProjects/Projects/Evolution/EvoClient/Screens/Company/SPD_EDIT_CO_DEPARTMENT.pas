// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_DEPARTMENT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc, StdCtrls, DBCtrls, Mask, ComCtrls,
  wwdbedit, ExtCtrls, Wwdotdot, Wwdbcomb, Grids, Wwdbigrd, Wwdbgrid,
  wwdblook, SPD_EDIT_CO_BASE, Buttons, SDataStructure,  EvConsts,
  SPD_EDIT_CO_DBDT_LOCALS, EvSecElement, SPackageEntry, EvTypes,
  sCopyHelper, sDBDTCopyHelper, Variants, SDDClasses, 
  ISBasicClasses, SDataDictclient, SDataDicttemp,sCompanyShared, EvContext,
  EvExceptions, ActnList, Menus, SPD_DBDTChoiceQuery, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, EvDataAccessComponents;

type
  TEDIT_CO_DEPARTMENT = class(TEDIT_CO_BASE)
    tshtGeneral_Info: TTabSheet;
    tshtMisc_Info: TTabSheet;
    tshtDefaults: TTabSheet;
    Panel4: TevPanel;
    Panel5: TevPanel;
    wwdsSubMaster2: TevDataSource;
    pdsDetail: TevProxyDataSet;
    tbshLocals: TTabSheet;
    TDBDTLocals1: TDBDTLocals;
    tsVmr: TTabSheet;
    pmLocalCopy: TevPopupMenu;
    MenuItem3: TMenuItem;
    LocalCopyService: TevActionList;
    CopyService: TAction;
    fpDepartmentRight: TisUIFashionPanel;
    sbDepartmentDetails: TScrollBox;
    fpDepartment: TisUIFashionPanel;
    EvBevel2: TEvBevel;
    fpDivisionContact: TisUIFashionPanel;
    EvBevel1: TEvBevel;
    fpDivisionOptions: TisUIFashionPanel;
    lablName: TevLabel;
    lablAddress1: TevLabel;
    lablAddress2: TevLabel;
    lablCity: TevLabel;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    lablBranch_Code: TevLabel;
    dedtName: TevDBEdit;
    dedtAddress1: TevDBEdit;
    dedtAddress2: TevDBEdit;
    dedtCity: TevDBEdit;
    wwdeState: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    dedtDepartment_Code: TevDBEdit;
    qlblShowAddressOnCheck: TevLabel;
    wwcbPrint_Dept_Address_Check: TevDBComboBox;
    evLabel6: TevLabel;
    evDBLookupCombo8: TevDBLookupCombo;
    Label2: TevLabel;
    evLabel4: TevLabel;
    DBEdit1: TevDBEdit;
    evDBEdit1: TevDBEdit;
    lablPrimary_Contact: TevLabel;
    lablPhone: TevLabel;
    lablDescription1: TevLabel;
    dedtPrimary_Contact: TevDBEdit;
    wwdePhone1: TevDBEdit;
    dedtDescription1: TevDBEdit;
    lablDescription: TevLabel;
    lablFax: TevLabel;
    dedtDescription: TevDBEdit;
    wwdeFax: TevDBEdit;
    lablE_Mail: TevLabel;
    dedtE_Mail: TevDBEdit;
    lablSecondary_Contact: TevLabel;
    lablDescription2: TevLabel;
    dedtSecondary_Contact: TevDBEdit;
    wwdePhone2: TevDBEdit;
    dedtDescription2: TevDBEdit;
    lablPhone2: TevLabel;
    sbDepartmentDefaults: TScrollBox;
    fpDivisionDefaults: TisUIFashionPanel;
    lablHome_State: TevLabel;
    lablOverride_Pay_Rate: TevLabel;
    lablOverride_EE_Rate: TevLabel;
    evLabel3: TevLabel;
    Label6: TevLabel;
    drgpHomeStateType: TevDBRadioGroup;
    wwlcHome_State: TevDBLookupCombo;
    dedtOverride_EE_Rate: TevDBEdit;
    wwdeOverride_Pay_Rate: TevDBEdit;
    evDBLookupCombo6: TevDBLookupCombo;
    wwlcWorkers_Comp_Number: TevDBLookupCombo;
    wwDBLookupCombo4: TevDBLookupCombo;
    wwDBLookupCombo1: TevDBLookupCombo;
    fpDivisionNotes: TisUIFashionPanel;
    dmemBranch_Notes: TEvDBMemo;
    sbDepartmentMailRoom: TScrollBox;
    fpMailRoom: TisUIFashionPanel;
    evLabel20: TevLabel;
    evLabel21: TevLabel;
    evLabel22: TevLabel;
    evLabel25: TevLabel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBLookupCombo4: TevDBLookupCombo;
    evDBLookupCombo7: TevDBLookupCombo;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo5: TevDBLookupCombo;
    sbDepartmentBanks: TScrollBox;
    fpDivisionBanks: TisUIFashionPanel;
    lablPayroll_Bank_Account: TevLabel;
    lablBilling_Bank_Account: TevLabel;
    lablTax_Bank_Account: TevLabel;
    lablDD_Bank_Account: TevLabel;
    wwlcPayroll_Bank_Account: TevDBLookupCombo;
    wwlcBilling_Bank_Account: TevDBLookupCombo;
    wwlcTax_Bank_Account: TevDBLookupCombo;
    wwlcDD_Bank_Account: TevDBLookupCombo;
    sbDepartmentLocals: TScrollBox;
    lablBRANCH_NAME: TevLabel;
    Label3: TevLabel;
    Label1: TevLabel;
    DBText3: TevDBText;
    DBText2: TevDBText;
    DBText10: TevDBText;
    evLabel5: TevLabel;
    edtAccountNumber: TevDBEdit;
    rgPrimary: TevDBRadioGroup;
    wwDBGrid2: TevDBGrid;
    Splitter2: TevSplitter;
    wwDBGrid3: TevDBGrid;
    Splitter3: TevSplitter;
    wwDBGrid1: TevDBGrid;
    CopyHelper: TDBDTCopyHelper;
    procedure pdsDetailNewRecord(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure ResultFIELDS_CL_BANK_Function;
    procedure CopyServiceExecute(Sender: TObject);
    procedure LocalTaxCopy;
    procedure CopyHelperCopyaddressto1Click(Sender: TObject);
    procedure edtAccountNumberChange(Sender: TObject);
    procedure fpDepartmentRightResize(Sender: TObject);
  private
    procedure HandleCopy(Sender: TObject; parent_nbr: integer;
      selectedDetails, details: TEvClientDataSet; params: TStringList);
    procedure HandleDelete(Sender: TObject; parent_nbr: integer;
      selectedDetails, details: TEvClientDataSet; params: TStringList);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    function GetDataSetConditions(sName: string): string; override;
  public
    GL_Setuped:boolean;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    constructor Create(AOwner: TComponent); override;   
  end;

implementation

uses
  SCopier, EvUtils;
{$R *.DFM}

procedure TEDIT_CO_DEPARTMENT.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP, aDS);
  AddDS(DM_CLIENT.CL_TIMECLOCK_IMPORTS, aDS);
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_CLIENT.CL_BILLING, aDS);
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);  //gdy
  AddDS(DM_COMPANY.CO_DEPARTMENT_LOCALS, aDS); //gdy
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);

  inherited;
end;

procedure TEDIT_CO_DEPARTMENT.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
  if ( wwdsList.DataSet.Active ) and (wwdsList.DataSet.RecordCount <= 0) then exit;
  if Kind in [NavOk] then
  begin
    wwDBGrid1.Enabled := not Value;
    wwDBGrid2.Enabled := not Value;
    wwDBGrid3.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavNext.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavPrevious.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavLast.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavFirst.Enabled := not Value;
  end;
end;


function TEDIT_CO_DEPARTMENT.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_DEPARTMENT;
end;

function TEDIT_CO_DEPARTMENT.GetInsertControl: TWinControl;
begin
  Result := dedtDepartment_Code;
end;

procedure TEDIT_CO_DEPARTMENT.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_DIVISION, SupportDataSets);
  AddDS(DM_COMPANY.CO_BRANCH, SupportDataSets);
  AddDS(DM_COMPANY.CO_DEPARTMENT_LOCALS, EditDataSets); //gdy

end;

function TEDIT_CO_DEPARTMENT.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_BRANCH') or (sName = 'CO_DEPARTMENT') or (sName = 'CO_DEPARTMENT_LOCALS') then //gdy
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_DEPARTMENT.Activate;
var
  dummy: boolean;
begin
  inherited;
  wwdsSubMaster2.DataSet := DM_COMPANY.CO_BRANCH;
  wwdsDetail.DataSet := DM_COMPANY.CO_DEPARTMENT;
  GL_Setuped := false;

  CopyHelper.Grid := wwDBGrid1; //gdy
  CopyHelper.OnCopyDetail := HandleCopy; //gdy
  CopyHelper.OnDeleteDetail := HandleDelete; //gdy
  CopyHelper.CopyAddressEnabled := True;
  CopyHelper.Copyaddressto1.OnClick := CopyHelperCopyaddressto1Click;  
  GetDataSetsToReopen( CopyHelper.FDSToSave,  dummy ); //gdy -- too much
  
  PageControl1.ActivePageIndex := 0;
  tsVMR.TabVisible := tsVMR.TabVisible and CheckVmrLicenseActive;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CO_DEPARTMENT.pdsDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  with DM_COMPANY.CO do
    if Active then
    begin
      DataSet.FindField('HOME_CO_STATES_NBR').AsInteger :=
        FindField('HOME_CO_STATES_NBR').AsInteger;
      DataSet.FindField('PAY_FREQUENCY_HOURLY').AsString :=
        FindField('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
      DataSet.FindField('PAY_FREQUENCY_SALARY').AsString :=
        FindField('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
    end;
end;

procedure TEDIT_CO_DEPARTMENT.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
 V: Variant;
 tmpDataSet: TevClientDataSet;
begin
  case Kind of
    NavOk:
    begin
      tmpDataSet := TevClientDataSet.Create(nil);
      try
        with TExecDSWrapper.Create('SelectDBDTWithName') do
        begin
          SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
          ctx_DataAccess.GetCustomData(tmpDataSet, AsVariant);
          ctx_DataAccess.OpenDataSets([tmpDataSet]);
          V := tmpDataSet.Lookup('PNAME',dedtName.Text,'CUSTOM_DEPARTMENT_NUMBER');
        end;
        if (not VarIsNull(V)) and (V <> DM_COMPANY.CO_DEPARTMENT['CUSTOM_DEPARTMENT_NUMBER']) then
           EvMessage('Department name has been used by other department, it could cause inaccurate reporting.', mtWarning, [mbOK]);
      finally
        tmpDataSet.free;
      end;

      if DM_COMPANY.CO_DEPARTMENT.FieldByName('HOME_CO_STATES_NBR').AsInteger = 0 then
        raise EUpdateError.CreateHelp('You must select a home state!', IDH_ConsistencyViolation);
      if (DM_COMPANY.CO_DEPARTMENT.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_OVERRIDE) and
         ((not DM_COMPANY.CO_DEPARTMENT.FieldByName('OVERRIDE_PAY_RATE').IsNull) or (wwdeOverride_Pay_Rate.Text <> '')) then
        EvMessage('Auto Labor Distribution will NOT use this override!!!', mtWarning, [mbOK]);
      if GL_Setuped and not CheckGlSetuped(DM_CLIENT.CO_DEPARTMENT,['GENERAL_LEDGER_TAG']) then
           EvMessage(sDontForgetSetupGL);
      GL_Setuped := false;

    end;
    NavInsert:
    begin
      lablName.Caption := '~' + lablName.Caption;
      DM_CLIENT.CO_DEPARTMENT.FieldByName('NAME').Required := true;
      GL_Setuped := CheckGlSetupExist(DM_CLIENT.CO_DEPARTMENT,['GENERAL_LEDGER_TAG'],false);
    end
  end;

  if (Kind in [NavOK,NavCommit]) and
      (trim(edtAccountNumber.Text) <> '') then
         if (Trim(rgPrimary.Value) = '') then
           raise EUpdateError.CreateHelp('Please select Primary!', IDH_ConsistencyViolation);

  inherited;
end;

procedure TEDIT_CO_DEPARTMENT.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavOk, NavCancel:
    begin
      lablName.Caption := StringReplace( lablName.Caption, '~','', [rfReplaceAll] );
      DM_CLIENT.CO_DEPARTMENT.FieldByName('NAME').Required := false;
    end;
  end;
end;

procedure TEDIT_CO_DEPARTMENT.HandleCopy(Sender: TObject; parent_nbr: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList);
var
  k: integer;
  fn: string;
begin
  if details.Locate('CUSTOM_DEPARTMENT_NUMBER', VarArrayOf([selecteddetails.FieldByNAme('CUSTOM_DEPARTMENT_NUMBER').AsString]), []) then
    details.Edit
  else
    details.Insert;
  for k := 0 to SelectedDetails.Fields.Count - 1 do
  begin
    fn := Selecteddetails.Fields[k].FieldName;
    if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) then
      details[fn] := selectedDetails[fn];
  end;
  details['CO_BRANCH_NBR'] := parent_nbr;
  details.Post;
end;

procedure TEDIT_CO_DEPARTMENT.HandleDelete(Sender: TObject; parent_nbr: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList);
begin
  if details.Locate('CUSTOM_DEPARTMENT_NUMBER', VarArrayOf([selecteddetails.FieldByNAme('CUSTOM_DEPARTMENT_NUMBER').AsString]), []) then
    details.Delete;
end;


procedure TEDIT_CO_DEPARTMENT.AfterDataSetsReopen;
begin
  inherited;
  tsVMR.TabVisible := CheckVmrLicenseActive and CheckVmrSetupActive;
  AssignVMRClientLookups(tsVMR);
end;

procedure TEDIT_CO_DEPARTMENT.ResultFIELDS_CL_BANK_Function;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_CL_BANK_') <> stEnabled then
   Begin
    wwlcPayroll_Bank_Account.Enabled := false;
    wwlcBilling_Bank_Account.Enabled := false;
    wwlcTax_Bank_Account.Enabled := false;
    wwlcDD_Bank_Account.Enabled := false;
   end;
End;


procedure TEDIT_CO_DEPARTMENT.PageControl1Change(Sender: TObject);
begin
  inherited;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CO_DEPARTMENT.CopyServiceExecute(Sender: TObject);
begin
  inherited;
   LocalTaxCopy;
end;

procedure TEDIT_CO_DEPARTMENT.LocalTaxCopy;
var
  CurrentLocalNbr, CurrentNbr, CurrentLocalTaxNbr: integer;

  procedure DoCopyLocals;
  var
    k: integer;
  begin
    with TDBDTChoiceQuery.Create( Self ) do
    try
      SetFilter(CLIENT_LEVEL_DEPT, 'DEPARTMENT_NBR <>  ' + DM_COMPANY.CO_DEPARTMENT_LOCALS.FieldByName('CO_DEPARTMENT_NBR').AsString );
      Caption := 'Select the Departments you want this Local Tax copied to';
      ConfirmAllMessage := 'This operation will copy the Local Tax to all Departments. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
            DM_COMPANY.CO_DEPARTMENT_LOCALS.DataRequired('ALL');
            if dgChoiceList.SelectedList.Count > 0 then
            begin
              DM_COMPANY.CO_DEPARTMENT_LOCALS.DisableControls;
              try
                for k := 0 to dgChoiceList.SelectedList.Count - 1 do
                begin
                  cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                  if not DM_COMPANY.CO_DEPARTMENT_LOCALS.Locate('CO_DEPARTMENT_NBR;CO_LOCAL_TAX_NBR',
                       VarArrayOf([cdsChoiceList.FieldByName('DEPARTMENT_NBR').AsInteger,CurrentLocalTaxNbr]), []) then
                  begin
                    DM_COMPANY.CO_DEPARTMENT_LOCALS.Insert;
                    DM_COMPANY.CO_DEPARTMENT_LOCALS['CO_DEPARTMENT_NBR'] := cdsChoiceList.FieldByName('DEPARTMENT_NBR').AsInteger;
                    DM_COMPANY.CO_DEPARTMENT_LOCALS['CO_LOCAL_TAX_NBR'] := CurrentLocalTaxNbr;
                    DM_COMPANY.CO_DEPARTMENT_LOCALS.Post;
                  end;
                end;
              finally
                ctx_DataAccess.PostDataSets([DM_COMPANY.CO_DEPARTMENT_LOCALS]);
                DM_COMPANY.CO_DEPARTMENT_LOCALS.EnableControls;
              end;
             end;
      end;
    finally
      Free;
    end;
  end;
begin
  CurrentNbr := DM_COMPANY.CO_DEPARTMENT_LOCALS.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
  CurrentLocalNbr := DM_COMPANY.CO_DEPARTMENT_LOCALS.FieldByName('CO_DEPARTMENT_LOCALS_NBR').AsInteger;
  CurrentLocalTaxNbr := DM_COMPANY.CO_DEPARTMENT_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
  if (CurrentLocalNbr > 0) and  (CurrentLocalTaxNbr > 0 )then
  begin
    try
      DoCopyLocals;
    finally
      DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR',CurrentNbr,[]);
      DM_COMPANY.CO_DEPARTMENT_LOCALS.Locate('CO_DEPARTMENT_LOCALS_NBR',CurrentLocalNbr,[]);
    end;
  end;
end;


procedure TEDIT_CO_DEPARTMENT.CopyHelperCopyaddressto1Click(
  Sender: TObject);
var
  CurrentNbr, CurrentParentNbr : integer;
  CurrentAddr1,Currentaddr2,CurrentCity,CurrentState,CurrentZip: string;
  procedure DoCopyAddress;
  var
    k: integer;
  begin
    with TDBDTChoiceQuery.Create( Self ) do
    try
      SetFilter(CLIENT_LEVEL_DEPT, 'Department_NBR <>  ' + DM_COMPANY.CO_Department.FieldByName('CO_Department_NBR').AsString, false);
      Caption := 'Select the Departments you want the Address copied to';
      ConfirmAllMessage := 'This operation will copy the Address to all Departments. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            DM_COMPANY.CO_Department.DataRequired('CO_Department_NBR = ' + cdsChoiceList.FieldByName('Department_NBR').AsString);
            if DM_COMPANY.CO_Department.RecordCount > 0 then
            begin
              DM_COMPANY.CO_Department.Edit;
              DM_COMPANY.CO_Department['ADDRESS1'] := CurrentAddr1;
              DM_COMPANY.CO_Department['ADDRESS2'] := Currentaddr2;
              DM_COMPANY.CO_Department['CITY']     := CurrentCity;
              DM_COMPANY.CO_Department['STATE']    := CurrentState;
              DM_COMPANY.CO_Department['ZIP_CODE'] := CurrentZip;
              DM_COMPANY.CO_Department.Post;
              ctx_DataAccess.PostDataSets([DM_COMPANY.CO_Department]);
            end;
          end;
      end;
    finally
      Free;
    end;
  end;
begin
  CurrentNbr   := DM_COMPANY.CO_Department.FieldByName('CO_Department_NBR').AsInteger;
  CurrentParentNbr := DM_COMPANY.CO_Department.FieldByName('CO_Branch_NBR').AsInteger;
  CurrentAddr1 := DM_COMPANY.CO_Department.FieldByName('ADDRESS1').AsString;
  Currentaddr2 := DM_COMPANY.CO_Department.FieldByName('ADDRESS2').AsString;
  CurrentCity  := DM_COMPANY.CO_Department.FieldByName('CITY').AsString;
  CurrentState := DM_COMPANY.CO_Department.FieldByName('STATE').AsString;
  CurrentZip   := DM_COMPANY.CO_Department.FieldByName('ZIP_CODE').AsString;
  try
    DM_COMPANY.CO_Department.DisableControls;
    wwDBGrid1.DataSource.DataSet.DisableControls;
    DoCopyAddress;
  finally
    DM_COMPANY.CO_Department.DataRequired('ALL');
    DM_COMPANY.CO_Branch.Locate('CO_Branch_NBR',CurrentParentNbr,[]);
    DM_COMPANY.CO_Department.Locate('CO_Department_NBR',CurrentNbr,[]);
    DM_COMPANY.CO_Department.EnableControls;
    wwDBGrid1.DataSource.DataSet.enableControls;
  end;
end;

procedure TEDIT_CO_DEPARTMENT.edtAccountNumberChange(Sender: TObject);
begin
  inherited;
  if Trim(edtAccountNumber.Text) <> '' then
      rgPrimary.Caption := '~Primary  '
  else
      rgPrimary.Caption := 'Primary ';
end;

constructor TEDIT_CO_DEPARTMENT.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENTS
  pnlSubbrowse.Parent := fpDepartmentRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_DEPARTMENT.fpDepartmentRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpDepartmentRight.Width - 40;
  pnlSubbrowse.Height := fpDepartmentRight.Height - 65;
end;

initialization
  RegisterClass(TEDIT_CO_DEPARTMENT);

end.
