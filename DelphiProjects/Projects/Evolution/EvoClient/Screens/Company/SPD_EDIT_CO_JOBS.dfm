inherited EDIT_CO_JOBS: TEDIT_CO_JOBS
  inherited Panel1: TevPanel
    inherited pnlTopLeft: TevPanel
      inherited dbtxClientNbr: TevDBText
        Height = 13
      end
      inherited dbtxClientName: TevDBText
        Height = 13
      end
      inherited DBText1: TevDBText
        Height = 13
      end
      inherited CompanyNameText: TevDBText
        Height = 13
      end
    end
  end
  inherited PageControl1: TevPageControl
    HelpContext = 14001
    ActivePage = tshtLocals
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 784
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 772
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 448
              Top = 96
              Width = 224
              Height = 392
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 124
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 74
                Height = 312
                IniAttributes.SectionName = 'TEDIT_CO_JOBS\wwdbgridSelectClient'
              end
            end
          end
        end
      end
    end
    object tshtCompany_Jobs: TTabSheet
      HelpContext = 14001
      Caption = 'Details'
      ImageIndex = 2
      object sbJobsDetail: TScrollBox
        Left = 0
        Top = 0
        Width = 435
        Height = 194
        Align = alClient
        TabOrder = 0
        object fpSignature: TisUIFashionPanel
          Left = 512
          Top = 374
          Width = 223
          Height = 128
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSignature'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Signature'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblUserID: TevLabel
            Left = 12
            Top = 27
            Width = 36
            Height = 13
            Caption = 'User ID'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblTitle: TevLabel
            Left = 111
            Top = 27
            Width = 20
            Height = 13
            Caption = 'Title'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object editTitle: TevEdit
            Tag = 5
            Left = 111
            Top = 42
            Width = 90
            Height = 21
            HelpContext = 14001
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnChange = editChange
          end
          object editUserID: TevEdit
            Tag = 4
            Left = 12
            Top = 42
            Width = 90
            Height = 21
            HelpContext = 14001
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnChange = editChange
          end
        end
        object fpLifeToDate: TisUIFashionPanel
          Left = 512
          Top = 237
          Width = 223
          Height = 131
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpLifeToDate'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Life to Date Info'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablAmount: TevLabel
            Left = 12
            Top = 35
            Width = 36
            Height = 13
            Caption = 'Amount'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablHours: TevLabel
            Left = 110
            Top = 35
            Width = 28
            Height = 13
            Caption = 'Hours'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablEDCode: TevLabel
            Left = 12
            Top = 74
            Width = 48
            Height = 13
            Caption = 'E/D Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object editHours: TevEdit
            Tag = 2
            Left = 110
            Top = 50
            Width = 90
            Height = 21
            HelpContext = 14001
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnChange = editChange
          end
          object editEDCode: TevEdit
            Tag = 3
            Left = 12
            Top = 89
            Width = 188
            Height = 21
            HelpContext = 14001
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnChange = editChange
          end
          object editAmount: TevEdit
            Tag = 1
            Left = 12
            Top = 50
            Width = 90
            Height = 21
            HelpContext = 14001
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnChange = editChange
          end
        end
        object fpDetails: TisUIFashionPanel
          Left = 8
          Top = 237
          Width = 243
          Height = 264
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Job Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 54
            Height = 16
            Caption = '~Job Code'
            FocusControl = dedtDescription
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDescription: TevLabel
            Left = 12
            Top = 74
            Width = 62
            Height = 16
            Caption = '~Description'
            FocusControl = dedtDescription
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablWorkers_Comp_Number: TevLabel
            Left = 12
            Top = 113
            Width = 107
            Height = 13
            Caption = 'Default Workers Comp'
          end
          object Label35: TevLabel
            Left = 129
            Top = 113
            Width = 41
            Height = 13
            Caption = 'G/L Tag'
            FocusControl = DBEdit20
          end
          object evLabel2: TevLabel
            Left = 129
            Top = 74
            Width = 90
            Height = 13
            Caption = 'Work Classification'
            FocusControl = dedtDescription
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablRate_Per_Hour: TevLabel
            Left = 129
            Top = 35
            Width = 68
            Height = 13
            Caption = 'Rate Per Hour'
          end
          object lblJobStartDate: TevLabel
            Left = 12
            Top = 152
            Width = 48
            Height = 13
            Caption = 'Start Date'
          end
          object evDBEdit1: TevDBEdit
            Left = 12
            Top = 50
            Width = 109
            Height = 21
            HelpContext = 14001
            DataField = 'DESCRIPTION'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtDescription: TevDBEdit
            Left = 12
            Top = 89
            Width = 109
            Height = 21
            HelpContext = 14001
            DataField = 'TRUE_DESCRIPTION'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcWorkersComp: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 109
            Height = 21
            HelpContext = 14001
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object DBEdit20: TevDBEdit
            Left = 129
            Top = 128
            Width = 92
            Height = 21
            HelpContext = 10668
            DataField = 'GENERAL_LEDGER_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit2: TevDBEdit
            Left = 129
            Top = 89
            Width = 92
            Height = 21
            HelpContext = 14001
            DataField = 'MISC_JOB_CODE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeRate_Per_Hour: TevDBEdit
            Left = 129
            Top = 50
            Width = 92
            Height = 21
            HelpContext = 14001
            DataField = 'RATE_PER_HOUR'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object cbJobActive: TevDBCheckBox
            Left = 129
            Top = 169
            Width = 86
            Height = 17
            Caption = 'Active'
            DataField = 'JOB_ACTIVE'
            DataSource = wwdsDetail
            TabOrder = 7
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object cbFederalCertified: TevDBCheckBox
            Left = 12
            Top = 197
            Width = 97
            Height = 17
            Caption = 'Federal Certified'
            DataField = 'CERTIFIED'
            DataSource = wwdsDetail
            TabOrder = 8
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object cbStateCertified: TevDBCheckBox
            Left = 129
            Top = 197
            Width = 88
            Height = 17
            Caption = 'State Certified'
            DataField = 'STATE_CERTIFIED'
            DataSource = wwdsDetail
            TabOrder = 9
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object editJobStartDate: TevDateTimePicker
            Tag = 6
            Left = 12
            Top = 167
            Width = 109
            Height = 21
            Date = 40908.000000000000000000
            Time = 40908.000000000000000000
            ShowCheckbox = True
            TabOrder = 6
            OnChange = editChange
          end
        end
        object fpCOJobSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 727
          Height = 221
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgCompany_Jobs: TevDBGrid
            Left = 12
            Top = 35
            Width = 692
            Height = 164
            HelpContext = 14001
            TabStop = False
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Job Code'#9'F'
              'TRUE_DESCRIPTION'#9'40'#9'Description'#9'F'
              'JOB_ACTIVE'#9'1'#9'Job Active'#9'F'
              'CERTIFIED'#9'1'#9'Certified'#9'F'
              'STATE_CERTIFIED'#9'1'#9'State Certified'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_JOBS\wwdgCompany_Jobs'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            PopupMenu = mnJobFieldsCopy
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpAddress: TisUIFashionPanel
          Left = 259
          Top = 237
          Width = 245
          Height = 264
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpAddress'
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Address'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label62: TevLabel
            Left = 12
            Top = 35
            Width = 47
            Height = 13
            Alignment = taRightJustify
            Caption = 'Address 1'
            FocusControl = DBEdit27
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label68: TevLabel
            Left = 12
            Top = 74
            Width = 47
            Height = 13
            Alignment = taRightJustify
            Caption = 'Address 2'
            FocusControl = DBEdit28
          end
          object Label67: TevLabel
            Left = 12
            Top = 113
            Width = 17
            Height = 13
            Alignment = taRightJustify
            Caption = 'City'
            FocusControl = DBEdit29
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label66: TevLabel
            Left = 118
            Top = 113
            Width = 25
            Height = 13
            Caption = 'State'
            FocusControl = DBEdit30
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label65: TevLabel
            Left = 160
            Top = 113
            Width = 15
            Height = 13
            Alignment = taRightJustify
            Caption = 'Zip'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 12
            Top = 207
            Width = 41
            Height = 13
            Caption = 'Location'
          end
          object EvBevel1: TEvBevel
            Left = 12
            Top = 158
            Width = 209
            Height = 3
            Shape = bsTopLine
          end
          object lablHome_State: TevLabel
            Left = 12
            Top = 165
            Width = 102
            Height = 13
            Caption = 'Payroll Override State'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBEdit28: TevDBEdit
            Left = 12
            Top = 89
            Width = 209
            Height = 21
            HelpContext = 18002
            DataField = 'ADDRESS2'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit27: TevDBEdit
            Left = 12
            Top = 50
            Width = 209
            Height = 21
            HelpContext = 18002
            DataField = 'ADDRESS1'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit29: TevDBEdit
            Left = 12
            Top = 128
            Width = 97
            Height = 21
            HelpContext = 18002
            DataField = 'CITY'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit30: TevDBEdit
            Left = 118
            Top = 128
            Width = 34
            Height = 21
            HelpContext = 18002
            DataField = 'STATE'
            DataSource = wwdsDetail
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBLookupCombo2: TevDBLookupCombo
            Left = 12
            Top = 222
            Width = 209
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ACCOUNT_NUMBER'#9'30'#9'Account Number'#9'F'
              'ADDRESS1'#9'30'#9'Address1'#9'F'
              'CITY'#9'20'#9'City'#9'F'
              'ZIP_CODE'#9'10'#9'Zip Code'#9'F'
              'STATE'#9'2'#9'State'#9'F')
            DataField = 'CO_LOCATIONS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_LOCATIONS.CO_LOCATIONS
            LookupField = 'CO_LOCATIONS_NBR'
            Style = csDropDownList
            TabOrder = 6
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object DBEdit31: TevDBEdit
            Left = 160
            Top = 128
            Width = 61
            Height = 21
            HelpContext = 18002
            DataField = 'ZIP_CODE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcHome_State: TevDBLookupCombo
            Left = 12
            Top = 180
            Width = 209
            Height = 21
            HelpContext = 13004
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'HOME_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
    object tshtLocals: TTabSheet
      Caption = 'Locals'
      ImageIndex = 17
      object sbJobsLocals: TScrollBox
        Left = 0
        Top = 0
        Width = 435
        Height = 194
        Align = alClient
        TabOrder = 0
        object fpCOJobsLocalSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 311
          Height = 365
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evDBGrid1: TevDBGrid
            Left = 12
            Top = 35
            Width = 278
            Height = 309
            DisableThemesInTitle = False
            Selected.Strings = (
              'LocalName'#9'40'#9'Local Name'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_JOBS\evDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsJobsLocals
            PopupMenu = pmJobCopy
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCOJobsLocalDetails: TisUIFashionPanel
          Left = 8
          Top = 381
          Width = 311
          Height = 127
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Local Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel3: TevLabel
            Left = 12
            Top = 35
            Width = 57
            Height = 13
            Caption = 'Local Name'
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 277
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'LocalName'#9'20'#9'LocalName'#9'F')
            DataField = 'CO_LOCAL_TAX_NBR'
            DataSource = dsJobsLocals
            LookupTable = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
            LookupField = 'CO_LOCAL_TAX_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          inline MiniNavigationFrame: TMiniNavigationFrame
            Left = 12
            Top = 81
            Width = 281
            Height = 31
            TabOrder = 1
            inherited SpeedButton1: TevSpeedButton
              Width = 100
              Caption = 'Create'
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
                CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
                E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
                7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
                82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
                9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
                55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
                5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
                CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
                0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
                76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
                FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
                16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
                8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
                FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
                38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
                9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
                FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
                51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
                9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
                66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
                9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
                76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
                9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
                FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
                59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
                9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
                FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
                0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
                87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
                FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
                55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
                556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
                B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
                88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
                E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
                78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            end
            inherited SpeedButton2: TevSpeedButton
              Left = 177
              Width = 100
              Caption = 'Delete'
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
                CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
                F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
                6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
                A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
                CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
                A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
                2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
                C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
                9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
                5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
                F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
                9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
                576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
                FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
                97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
                4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
                F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
                8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
                3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
                F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
                334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
                F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
                2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
                EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
                3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
                EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
                84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
                8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
                F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
                8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
                9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
                F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
                8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
                2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
                C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
                807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
                C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
                EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
                CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
                6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            end
          end
        end
        object Button1: TButton
          Left = 496
          Top = 40
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 2
          OnClick = Button1Click
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 2
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_JOBS.CO_JOBS
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDataChange
    OnUpdateData = wwdsDetailUpdateData
  end
  inherited wwdsList: TevDataSource
    Top = 2
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 323
    Top = 18
  end
  inherited DM_CLIENT: TDM_CLIENT
    Top = 26
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 400
    Top = 13
  end
  object dsJobsLocals: TevDataSource
    DataSet = DM_CO_JOBS_LOCALS.CO_JOBS_LOCALS
    MasterDataSource = wwdsDetail
    Left = 284
    Top = 17
  end
  object pmJobCopy: TevPopupMenu
    Left = 533
    Top = 269
    object MenuItem3: TMenuItem
      Action = CopyService
    end
  end
  object JobCopyService: TevActionList
    Left = 624
    Top = 283
    object CopyService: TAction
      Caption = 'Copy To...'
      OnExecute = CopyServiceExecute
    end
  end
  object mnJobFieldsCopy: TevPopupMenu
    Left = 424
    Top = 203
    object CopytoCompanyJobs: TMenuItem
      Caption = 'Copy to Company Jobs'
      OnClick = CopytoCompanyJobsClick
    end
  end
  object pdsDetail: TevProxyDataSet
    Dataset = DM_CO_JOBS.CO_JOBS
    OnNewRecord = pdsDetailNewRecord
    Left = 276
    Top = 55
  end
end
