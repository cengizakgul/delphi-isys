// Copyright � 2000-2004 iSystems LLC. All rights reserved.
//gdy
unit sCompanyCopyHelper;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  sCopyHelper, Menus,  ActnList, ImgList, sCopier,
  ISBasicClasses, EvUIComponents;

type
  TCompanyCopyHelper = class(TCopyHelper)
  private
    { Private declarations }
    FOnDeleteDetail: TProcessDetailEvent;
    FOnCopyDetail: TProcessDetailEvent;
  protected
    function CreateCopier: TCopier; override;
  public
    { Public declarations }
    property OnCopyDetail: TProcessDetailEvent read FOnCopyDetail write FOnCopyDetail;
    property OnDeleteDetail: TProcessDetailEvent read FOnDeleteDetail write FOnDeleteDetail;

  end;

implementation


function TCompanyCopyHelper.CreateCopier: TCopier;
begin
  Result := inherited CreateCopier;
  if not assigned( Result ) then
  begin
    Result := TCompanyDetailCopier.Create( DataSet, Grid.SelectedList, UserFriendlyName );
    TCompanyDetailCopier(Result).OnDeleteDetail :=  FOnDeleteDetail;
    TCompanyDetailCopier(Result).OnCopyDetail := FOnCopyDetail;
    TCompanyDetailCopier(Result).AddTablesToSave( FDSToSave );
  end;
end;

{$R *.DFM}

end.
