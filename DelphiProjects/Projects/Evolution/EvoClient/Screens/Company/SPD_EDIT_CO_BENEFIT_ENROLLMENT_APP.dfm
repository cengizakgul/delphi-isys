inherited EDIT_CO_BENEFIT_ENROLLMENT_APP: TEDIT_CO_BENEFIT_ENROLLMENT_APP
  Width = 893
  Height = 599
  inherited Panel1: TevPanel
    Width = 893
    inherited pnlFavoriteReport: TevPanel
      Left = 741
    end
    inherited pnlTopLeft: TevPanel
      Width = 741
    end
  end
  inherited PageControl1: TevPageControl
    Width = 893
    Height = 545
    HelpContext = 29501
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 885
        Height = 516
        inherited pnlBorder: TevPanel
          Width = 881
          Height = 512
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 881
          Height = 512
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Pensions'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 371
              Top = 59
              Width = 379
              Height = 520
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 833
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 833
            Height = 405
            inherited Splitter1: TevSplitter
              Left = 320
              Height = 401
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Height = 401
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 321
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_ENROLLMENT_APP\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 506
              Height = 401
              Align = alClient
              Title = 'Discounts'
            end
          end
        end
      end
    end
    object tshtOpprovalDetail: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbPensions: TScrollBox
        Left = 0
        Top = 0
        Width = 885
        Height = 516
        Align = alClient
        TabOrder = 0
        object fpApprovalButtons: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 809
          Height = 77
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Buttons'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object btnApprove: TevButton
            Left = 12
            Top = 35
            Width = 125
            Height = 25
            Caption = 'Approve Selected'
            TabOrder = 0
            OnClick = btnApproveClick
            Color = clBlack
            Margin = 0
          end
          object btnClose: TevButton
            Left = 152
            Top = 35
            Width = 125
            Height = 25
            Caption = 'Close Selected'
            TabOrder = 1
            OnClick = btnCloseClick
            Color = clBlack
            Margin = 0
          end
          object btnPending: TevButton
            Left = 292
            Top = 35
            Width = 125
            Height = 25
            Caption = 'Revert to Pending '
            TabOrder = 2
            OnClick = btnPendingClick
            Color = clBlack
            Margin = 0
          end
          object btnEmail: TevButton
            Left = 572
            Top = 35
            Width = 125
            Height = 25
            Caption = 'Email Selected'
            TabOrder = 3
            OnClick = btnEmailClick
            Color = clBlack
            Margin = 0
          end
          object btnPrint: TevButton
            Left = 432
            Top = 35
            Width = 125
            Height = 25
            Caption = 'Print Selected'
            TabOrder = 4
            OnClick = btnPrintClick
            Color = clBlack
            Margin = 0
          end
          object emailType: TevCheckBox
            Left = 709
            Top = 40
            Width = 86
            Height = 17
            Caption = 'Multiple EE'#39's'
            TabOrder = 5
          end
        end
        object fpCOPensionSummary: TisUIFashionPanel
          Left = 8
          Top = 94
          Width = 809
          Height = 361
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evpgApprove: TevPageControl
            Left = 12
            Top = 36
            Width = 777
            Height = 305
            ActivePage = tsComplete
            Align = alClient
            TabOrder = 0
            OnChange = evpgApproveChange
            object tsComplete: TTabSheet
              Caption = 'Completed'
              object wwdgCompleted: TevDBCheckGrid
                Left = 0
                Top = 0
                Width = 769
                Height = 277
                DisableThemesInTitle = False
                Selected.Strings = (
                  'SOCIAL_SECURITY_NUMBER'#9'11'#9'SSN'#9'F'
                  'CUSTOM_EMPLOYEE_NUMBER'#9'10'#9'Custom EE Code'#9'F'
                  'FIRST_NAME'#9'20'#9'First Name'#9'F'
                  'LAST_NAME'#9'20'#9'Last Name'#9'F'
                  'CATEGORY_TYPE_DESC'#9'40'#9'Benefit Category'#9'F')
                IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_ENROLLMENT_APP\wwdgCompleted'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = evdsComplete
                MultiSelectOptions = [msoShiftSelect]
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                ReadOnly = True
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                UseTFields = False
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
            object tsProgress: TTabSheet
              Caption = 'In Progress'
              ImageIndex = 1
              object wwdgProgress: TevDBCheckGrid
                Left = 0
                Top = 0
                Width = 769
                Height = 277
                DisableThemesInTitle = False
                Selected.Strings = (
                  'SOCIAL_SECURITY_NUMBER'#9'11'#9'SSN'#9'F'
                  'CUSTOM_EMPLOYEE_NUMBER'#9'10'#9'Custom EE Code'#9'F'
                  'FIRST_NAME'#9'20'#9'First Name'#9'F'
                  'LAST_NAME'#9'20'#9'Last Name'#9'F'
                  'CATEGORY_TYPE_DESC'#9'40'#9'Benefit Category'#9'F')
                IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_ENROLLMENT_APP\wwdgProgress'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = evdsInProgress
                MultiSelectOptions = [msoShiftSelect]
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                ReadOnly = True
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                UseTFields = False
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
            object tsNoElections: TTabSheet
              Caption = 'No Elections'
              ImageIndex = 2
              object wwdgNoElection: TevDBCheckGrid
                Left = 0
                Top = 0
                Width = 769
                Height = 277
                DisableThemesInTitle = False
                Selected.Strings = (
                  'SOCIAL_SECURITY_NUMBER'#9'11'#9'SSN'#9'F'
                  'CUSTOM_EMPLOYEE_NUMBER'#9'10'#9'Custom EE Code'#9'F'
                  'FIRST_NAME'#9'20'#9'First Name'#9'F'
                  'LAST_NAME'#9'20'#9'Last Name'#9'F'
                  'CATEGORY_TYPE_DESC'#9'40'#9'Benefit Category'#9'F')
                IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_ENROLLMENT_APP\wwdgNoElection'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = evdsNoElection
                MultiSelectOptions = [msoShiftSelect]
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                ReadOnly = True
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                UseTFields = False
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_BENEFIT_DISCOUNT.CO_BENEFIT_DISCOUNT
  end
  inherited PageControlImages: TevImageList
    Top = 88
  end
  inherited dsCL: TevDataSource
    Left = 264
  end
  object evdsComplete: TevDataSource
    Left = 568
    Top = 104
  end
  object evdsInProgress: TevDataSource
    Left = 608
    Top = 104
  end
  object evdsNoElection: TevDataSource
    Left = 656
    Top = 104
  end
end
