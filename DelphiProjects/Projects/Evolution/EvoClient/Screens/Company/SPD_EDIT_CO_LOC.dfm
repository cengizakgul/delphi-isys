inherited EDIT_CO_LOC: TEDIT_CO_LOC
  Width = 729
  Height = 473
  inherited Panel1: TevPanel
    Width = 729
    inherited pnlFavoriteReport: TevPanel
      Left = 614
    end
  end
  inherited PageControl1: TevPageControl
    Width = 729
    Height = 440
    HelpContext = 14001
    ActivePage = tshtCompany_Locs
    inherited TabSheet1: TTabSheet
      inherited Panel2: TevPanel
        Width = 721
        Height = 371
        inherited Splitter1: TevSplitter
          Height = 369
        end
        inherited wwdbgridSelectClient: TevDBGrid
          Height = 369
          IniAttributes.SectionName = 'TEDIT_CO_LOC\wwdbgridSelectClient'
        end
        inherited pnlSubbrowse: TevPanel
          Width = 396
          Height = 369
        end
      end
      inherited Panel3: TevPanel
        Width = 721
      end
    end
    object tshtCompany_Locs: TTabSheet
      HelpContext = 14001
      Caption = 'Details'
      object Label62: TevLabel
        Left = 9
        Top = 276
        Width = 47
        Height = 13
        Alignment = taRightJustify
        Caption = 'Address 1'
        FocusControl = DBEdit27
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label65: TevLabel
        Left = 106
        Top = 359
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zip Code'
        FocusControl = DBEdit31
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label67: TevLabel
        Left = 39
        Top = 332
        Width = 17
        Height = 13
        Alignment = taRightJustify
        Caption = 'City'
        FocusControl = DBEdit29
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label68: TevLabel
        Left = 9
        Top = 305
        Width = 47
        Height = 13
        Alignment = taRightJustify
        Caption = 'Address 2'
        FocusControl = DBEdit28
      end
      object Label66: TevLabel
        Left = 32
        Top = 359
        Width = 25
        Height = 13
        Caption = 'State'
        FocusControl = DBEdit30
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel1: TevLabel
        Left = 13
        Top = 223
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Account'
        FocusControl = evDBEdit1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwdgCompany_Locs: TevDBGrid
        Left = 0
        Top = 0
        Width = 721
        Height = 201
        HelpContext = 14001
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'ADDRESS1'#9'40'#9'Address'#9'F'
          'CITY'#9'1'#9'City'#9'F'
          'ZIP_CODE'#9'1'#9'ZIP Code'#9'F'
          'STATE'#9'40'#9'State'#9'F'
          'AGENCY'#9'1'#9'Agency'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_CO_LOC\wwdgCompany_Locs'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alTop
        DataSource = wwdsDetail
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object DBEdit27: TevDBEdit
        Left = 63
        Top = 274
        Width = 184
        Height = 21
        HelpContext = 18002
        DataField = 'ADDRESS1'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit28: TevDBEdit
        Left = 63
        Top = 302
        Width = 184
        Height = 21
        HelpContext = 18002
        DataField = 'ADDRESS2'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit29: TevDBEdit
        Left = 63
        Top = 330
        Width = 185
        Height = 21
        HelpContext = 18002
        DataField = 'CITY'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit31: TevDBEdit
        Left = 156
        Top = 356
        Width = 64
        Height = 21
        HelpContext = 18002
        DataField = 'ZIP_CODE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit30: TevDBEdit
        Left = 63
        Top = 356
        Width = 27
        Height = 21
        HelpContext = 18002
        DataField = 'STATE'
        DataSource = wwdsDetail
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBEdit1: TevDBEdit
        Left = 63
        Top = 220
        Width = 184
        Height = 21
        HelpContext = 18002
        DataField = 'ACCOUNT_NUMBER'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 408
    Top = 202
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_LOCATIONS.CO_LOCATIONS
    Left = 446
    Top = 202
  end
  inherited wwdsList: TevDataSource
    Left = 370
    Top = 202
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 323
    Top = 18
  end
  inherited DM_CLIENT: TDM_CLIENT
    Top = 26
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 400
    Top = 13
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 384
    Top = 384
  end
end
