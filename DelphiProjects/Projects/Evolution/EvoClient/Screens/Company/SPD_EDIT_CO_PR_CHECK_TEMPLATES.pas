// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_PR_CHECK_TEMPLATES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc, StdCtrls, Mask, DBCtrls, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, ExtCtrls, wwdbedit, Wwdotdot, Wwdbcomb, SPD_EDIT_CO_BASE,
  Buttons,  SDataStructure, SPD_MiniNavigationFrame, wwdblook,
  ISBasicClasses, SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_PR_CHECK_TEMPLATES = class(TEDIT_CO_BASE)
    tshtSettings: TTabSheet;
    tshtExclusions: TTabSheet;
    wwDBGrid1x: TevDBGrid;
    TabSheet2: TTabSheet;
    dsTmplEDs: TevDataSource;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    dsState: TevDataSource;
    dsLocal: TevDataSource;
    sbTemplateDetails: TScrollBox;
    fpTemplateDetails: TisUIFashionPanel;
    lablName: TevLabel;
    dedtName: TevDBEdit;
    drgpProrated_Scheduled_E_Ds: TevDBRadioGroup;
    fpOverrides: TisUIFashionPanel;
    lablFederal_Override_Value: TevLabel;
    lablFederal_Override_Type: TevLabel;
    wwdcFederal_Override_Type: TevDBComboBox;
    wwdeFederal_Override_Value: TevDBEdit;
    sbBlocks: TScrollBox;
    fpBlocks: TisUIFashionPanel;
    drgpExclude_DD: TevDBRadioGroup;
    drgpExclude_DD_Except_Net: TevDBRadioGroup;
    drgpExclude_Time_Off_Accural: TevDBRadioGroup;
    drgpExclude_Auto_Distribution: TevDBRadioGroup;
    drgpExclude_All_Scheduled_E_D_Codes: TevDBRadioGroup;
    drgpExclude_Scheduled_E_D_From_Agency_Ch: TevDBRadioGroup;
    drgpExclude_Scheduled_E_D_Except_Pension: TevDBRadioGroup;
    fpEDsToBlock: TisUIFashionPanel;
    isUIFashionPanel1: TisUIFashionPanel;
    lablE_D_Code: TevLabel;
    MiniNavigationFram: TMiniNavigationFrame;
    wwlcE_D_Code: TevDBLookupCombo;
    grClientEDs: TevDBGrid;
    sbTaxBlocks: TScrollBox;
    fpTaxBlocks: TisUIFashionPanel;
    evLabel2: TevLabel;
    drgpExclude_Federal: TevDBRadioGroup;
    drgpExclude_Employee_OASDI: TevDBRadioGroup;
    drgpExclude_Employee_Medicare: TevDBRadioGroup;
    drgpExclude_Employee_EIC: TevDBRadioGroup;
    drgpExclude_Employer_FUI: TevDBRadioGroup;
    drgpExclude_Employer_OASDI: TevDBRadioGroup;
    drgpExclude_Employer_Medicare: TevDBRadioGroup;
    evDBComboBox1: TevDBComboBox;
    sbTemplateStates: TScrollBox;
    fpStates: TisUIFashionPanel;
    evDBGrid1: TevDBGrid;
    fpState: TisUIFashionPanel;
    Label1: TevLabel;
    Label7: TevLabel;
    Label8: TevLabel;
    evDBLookupCombo1: TevDBLookupCombo;
    wwDBComboBox1: TevDBComboBox;
    wwdeOverride_Value: TevDBEdit;
    fpStateBlocks: TisUIFashionPanel;
    evLabel3: TevLabel;
    drgpEXCLUDE_STATE: TevDBRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    DBRadioGroup2: TevDBRadioGroup;
    DBRadioGroup3: TevDBRadioGroup;
    MiniNavigationFrame1: TMiniNavigationFrame;
    evDBComboBox2: TevDBComboBox;
    sbTemplateLocals: TScrollBox;
    fpLocalsSummary: TisUIFashionPanel;
    wwDBGrid1: TevDBGrid;
    fpLocalsDetail: TisUIFashionPanel;
    Label2: TevLabel;
    evLabel1: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    drgpER_OASDI: TevDBRadioGroup;
    wwdeOverride_Amount: TevDBEdit;
    MiniNavigationFrame2: TMiniNavigationFrame;
    DBRadioGroup1: TevDBRadioGroup;
    lablOverride_Frequency: TevLabel;
    wwcbOverride_Frequency: TevDBComboBox;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetDataSetConditions(sName: string): string; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CO_PR_CHECK_TEMPLATES.Activate;
begin
  MiniNavigationFram.DataSource := dsTmplEDs;
  MiniNavigationFram.InsertFocusControl := wwlcE_D_Code;
  MiniNavigationFrame1.DataSource := dsState;
  MiniNavigationFrame1.InsertFocusControl := evDBLookupCombo1;
  MiniNavigationFrame2.DataSource := dsLocal;
  MiniNavigationFrame2.InsertFocusControl := wwDBLookupCombo1;
  inherited;
end;

procedure TEDIT_CO_PR_CHECK_TEMPLATES.AfterDataSetsReopen;
begin
  inherited;
  if evDBLookupCombo1.LookupValue = '' then
    evDBLookupCombo1.Text := '';
  if wwDBLookupCombo1.LookupValue = '' then
    wwDBLookupCombo1.Text := '';
end;

function TEDIT_CO_PR_CHECK_TEMPLATES.GetDataSetConditions(
  sName: string): string;
begin
  if (sName = 'CO_PR_CHECK_TEMPLATE_E_DS') or
     (sName = 'CO_BATCH_LOCAL_OR_TEMPS') or
     (sName = 'CO_BATCH_STATES_OR_TEMPS') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_PR_CHECK_TEMPLATES.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_PR_CHECK_TEMPLATE_E_DS, aDS);
  AddDS(DM_COMPANY.CO_BATCH_LOCAL_OR_TEMPS, aDS);
  AddDS(DM_COMPANY.CO_BATCH_STATES_OR_TEMPS, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  inherited;
end;

function TEDIT_CO_PR_CHECK_TEMPLATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_PR_CHECK_TEMPLATES;
end;

function TEDIT_CO_PR_CHECK_TEMPLATES.GetInsertControl: TWinControl;
begin
  Result := dedtNAME;
end;

procedure TEDIT_CO_PR_CHECK_TEMPLATES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  AddDS(DM_COMPANY.CO_PR_CHECK_TEMPLATE_E_DS, EditDataSets);
  AddDS(DM_COMPANY.CO_BATCH_LOCAL_OR_TEMPS, EditDataSets);
  AddDS(DM_COMPANY.CO_BATCH_STATES_OR_TEMPS, EditDataSets);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, SupportDataSets);
  AddDS(DM_COMPANY.CO_STATES, SupportDataSets);
  inherited;
end;

initialization
  RegisterClass(TEDIT_CO_PR_CHECK_TEMPLATES);

end.
