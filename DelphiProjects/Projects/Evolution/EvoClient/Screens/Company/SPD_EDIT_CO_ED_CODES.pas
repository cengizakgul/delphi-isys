// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_ED_CODES;

interface

uses
  SDataStructure, 
  wwdbedit, SPD_EDIT_CO_BASE, StdCtrls, ExtCtrls, DBCtrls, Mask, wwdblook,
  Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Controls,
  Classes, SPD_EDIT_COPY, SPackageEntry, EvUtils, sysutils, EvTypes,
  ISBasicClasses, SDDClasses, SDataDictclient, SDataDicttemp,sCompanyShared,
  EvExceptions, Wwdotdot, Wwdbcomb, EvConsts, EvContext, EvDataAccessComponents, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBEdit, isUIwwDBLookupCombo, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel, Forms,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_ED_CODES = class(TEDIT_CO_BASE)
    tshtCO_ED_Codes_Detail: TTabSheet;
    drgpPrint_Branch_Address_Check: TevDBRadioGroup;
    wwDBGrid2x: TevDBGrid;
    dsCOBenefits: TevDataSource;
    dsCOBenefitsSubType: TevDataSource;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpCOEdCodesSummary: TisUIFashionPanel;
    fpCOEdCodesDetails: TisUIFashionPanel;
    lablE_D_Code: TevLabel;
    wwlcE_D_Code: TevDBLookupCombo;
    wwDBLookupCombo2: TevDBLookupCombo;
    Label1x: TevLabel;
    DBEdit1x: TevDBEdit;
    evDBEdit1: TevDBEdit;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evBenefitReference: TevDBLookupCombo;
    evDBComboBox1: TevDBComboBox;
    evLabel4: TevLabel;
    evLabel3: TevLabel;
    evLabel5: TevLabel;
    edUsedAsBenefit: TevDBComboBox;
    evBenefitSubType: TevDBLookupCombo;
    sbED: TScrollBox;
    rgShowInEss: TevDBRadioGroup;
    cbACA: TevDBRadioGroup;
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure evBenefitReferenceChange(Sender: TObject);
    procedure evBenefitReferenceCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcE_D_CodeChange(Sender: TObject);
  protected
    function GetDataSetConditions(sName: string): string; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function GetInsertControl: TWinControl; override;
    function GetEDCheckLineCount(CoNbr, EDNbr: Integer): Integer;
    procedure DeActivate; override;
    procedure Activate; override;
  end;

implementation

uses SFrameEntry;

{$R *.DFM}

procedure TEDIT_CO_ED_CODES.Activate;
begin
  inherited;

end;

procedure TEDIT_CO_ED_CODES.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  CopyForm: TEDIT_COPY;
  EdCount: Integer;
  GL_Setuped:boolean;
  RecCount :integer;
begin
  inherited;
  case Kind of
    NavInsert:
      begin
        GL_Setuped := CheckGlSetupExist(DM_COMPANY.CO_E_D_CODES,['GENERAL_LEDGER_TAG','GL_OFFSET'],false);
        RecCount := DM_COMPANY.CO_E_D_CODES.RecordCount;
        CopyForm := TEDIT_COPY.Create(Self);
        try
          CopyForm.CopyTo := 'COEDCodes';
          CopyForm.CopyFrom := 'CL_E_DS';
          DM_COMPANY.CO_E_D_CODES.Cancel;
          DM_COMPANY.CO_E_D_CODES.DisableControls;
          CopyForm.ShowModal;
          DM_COMPANY.CO_E_D_CODES.EnableControls;
        finally
          CopyForm.Free;
        end;
        Handled := True;
        if GL_Setuped and (RecCount <> DM_COMPANY.CO_E_D_CODES.RecordCount) then
           EvMessage(sDontForgetSetupGL);
      end;
    NavDelete:
      begin
        EdCount := GetEDCheckLineCount(DM_COMPANY.CO['CO_NBR'], DM_COMPANY.CO_E_D_CODES['CL_E_DS_NBR']);
        if EdCount > 0 then
        begin
          DM_COMPANY.CO_E_D_CODES.Cancel;
          raise EUpdateError.CreateHelp('Cannot delete an E/D Code that is attached to check lines.', IDH_ConsistencyViolation);
        end;
      end;
  end;
end;

procedure TEDIT_CO_ED_CODES.Deactivate;
begin
  dsCOBenefitsSubType.MasterDataSource := nil;
  dsCOBenefitsSubType.DataSet := nil;  
  DM_COMPANY.CO_BENEFIT_SUBTYPE.Filter :=  '';
  DM_COMPANY.CO_BENEFIT_SUBTYPE.Filtered :=  false;
  inherited;
end;

procedure TEDIT_CO_ED_CODES.AfterDataSetsReopen;
begin
  inherited;
  dsCOBenefitsSubType.DataSet := CreateLookupDataset(DM_CLIENT.CO_BENEFIT_SUBTYPE, Self);
  evBenefitSubType.LookupTable := dsCOBenefitsSubType.DataSet;

end;

function TEDIT_CO_ED_CODES.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_BENEFIT_SUBTYPE') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_ED_CODES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_COMPANY.CO_BENEFITS, aDS);
  AddDS(DM_COMPANY.CO_BENEFIT_SUBTYPE, aDS);

  inherited;
end;

function TEDIT_CO_ED_CODES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_E_D_CODES;
end;

function TEDIT_CO_ED_CODES.GetEDCheckLineCount(CoNbr, EDNbr: Integer): Integer;
begin
  result := 0;
  ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('GetEDCountCO') do
  begin
    SetParam('CoNbr', CoNbr);
    SetParam('CledNbr', EdNbr);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  if ctx_DataAccess.CUSTOM_VIEW.RecordCount > 0 then
    result := ctx_DataAccess.CUSTOM_VIEW.FieldByName('EDCount').AsInteger;
end;

function TEDIT_CO_ED_CODES.GetInsertControl: TWinControl;
begin
  Result := wwlcE_D_Code;
end;

procedure TEDIT_CO_ED_CODES.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  wwlcE_D_Code.Enabled := wwdsDetail.DataSet.State = dsInsert;
end;

procedure TEDIT_CO_ED_CODES.evBenefitReferenceChange(Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then exit;
  if Assigned(evBenefitReference) and Assigned(evBenefitSubType) and
     Assigned(dsCOBenefitsSubType.DataSet) and
     DM_COMPANY.CO_BENEFITS.Active and
     DM_COMPANY.CO_BENEFIT_SUBTYPE.Active and
     wwdsDetail.DataSet.Active then
  begin
      dsCOBenefitsSubType.DataSet.Filter :=  'CO_BENEFITS_NBR = ' + String(ConvertNull(wwdsDetail.DataSet['CO_BENEFITS_NBR'],-1));
      dsCOBenefitsSubType.DataSet.Filtered := True;
      if (not dsCOBenefitsSubType.DataSet.Locate('CO_BENEFIT_SUBTYPE_NBR', wwdsDetail.DataSet['CO_BENEFIT_SUBTYPE_NBR'], [])) or
          (evBenefitReference.Value = '') then
        evBenefitSubType.Clear;
  end;

end;

procedure TEDIT_CO_ED_CODES.evBenefitReferenceCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;

  if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    if dsCOBenefits.DataSet.Active then
    begin
      dsCOBenefits.DataSet.Locate('CO_BENEFITS_NBR', wwdsDetail.DataSet['CO_BENEFITS_NBR'], []);

      dsCOBenefitsSubType.DataSet.Filter :='CO_BENEFITS_NBR = ' + String(ConvertNull(wwdsDetail.DataSet['CO_BENEFITS_NBR'],-1));
      dsCOBenefitsSubType.DataSet.Filtered := True;
      if (not dsCOBenefitsSubType.DataSet.Locate('CO_BENEFIT_SUBTYPE_NBR', wwdsDetail.DataSet['CO_BENEFIT_SUBTYPE_NBR'], [])) or
          (evBenefitReference.Value = '') then
        evBenefitSubtype.Clear;
    end;

end;

procedure TEDIT_CO_ED_CODES.wwlcE_D_CodeChange(Sender: TObject);
var
  Code : String;
begin
  inherited;
       //    default values ..  moved from Cl E DS screen because of database changes about   EE_OR_ER_BENEFIT...
  Code := wwdsDetail.DataSet.fieldByName('E_D_Code_Type').AsString;
  if Context.License.HR then
  begin
    if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) and
        (length(Code) > 0)   then
    begin
        if (Code = ED_MEMO_SIMPLE) or (Code = ED_MEMO_ER_INSURANCE_PREMIUM) then
      begin
        if pos('cobra', LowerCase(wwdsDetail.DataSet['CodeDescription'])) = 0 then
         wwdsDetail.DataSet['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_ER
        else
         wwdsDetail.DataSet['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_EE;
      end
      else
      if Code = ED_MEMO_COBRA_CREDIT then
         wwdsDetail.DataSet['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_ER
      else
      if Code = ED_MEMO_PERCENT_OF_TAX_WAGES then
         wwdsDetail.DataSet['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_ER
      else
      if Code[1] = 'D' then
         wwdsDetail.DataSet['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_EE
      else
         wwdsDetail.DataSet['EE_OR_ER_BENEFIT'] := BENEFIT_DEDUCTION_TYPE_NA;
    end;
  end;

end;

initialization
  RegisterClass(TEDIT_CO_ED_CODES);

end.
