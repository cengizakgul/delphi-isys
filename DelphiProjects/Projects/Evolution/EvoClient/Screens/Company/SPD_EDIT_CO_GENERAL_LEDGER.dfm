inherited EDIT_CO_GENERAL_LEDGER: TEDIT_CO_GENERAL_LEDGER
  inherited PageControl1: TevPageControl
    HelpContext = 29002
    ActivePage = tshtGeneral_Ledger_Numbers
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 784
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    OnResize = pnlCOBrowseLeftResize
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 772
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 384
              Top = 80
              Width = 232
              Height = 392
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 170
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 120
                Height = 345
                IniAttributes.SectionName = 'TEDIT_CO_GENERAL_LEDGER\wwdbgridSelectClient'
              end
            end
          end
        end
      end
    end
    object tshtGeneral_Ledger_Numbers: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbGeneralLedger: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 182
        Align = alClient
        TabOrder = 0
        object fpCOGeneralLedgerSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 681
          Height = 276
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgGeneral_Ledger_Number: TevDBCheckGrid
            Left = 12
            Top = 35
            Width = 645
            Height = 220
            DisableThemesInTitle = False
            Selected.Strings = (
              'LevelType_Name'#9'16'#9'Level Type'#9'F'#9
              'Level_Name'#9'22'#9'Level Value'#9'F'#9
              'DataType_Name'#9'16'#9'Data Type'#9'F'#9
              'Data_Name'#9'22'#9'Data Value'#9'F'#9
              'GENERAL_LEDGER_FORMAT'#9'19'#9'General Ledger Format'#9'F'#9)
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_GENERAL_LEDGER\wwdgGeneral_Ledger_Number'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            MultiSelectOptions = [msoShiftSelect]
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            PopupMenu = evPopupMenu1
            ReadOnly = True
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCOGeneralLedgerDetails: TisUIFashionPanel
          Left = 8
          Top = 292
          Width = 681
          Height = 131
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object qlblLevelType: TevLabel
            Left = 12
            Top = 35
            Width = 64
            Height = 13
            Caption = '~Level Type'
            FocusControl = wwcbLevelType
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblDataType: TevLabel
            Left = 230
            Top = 35
            Width = 64
            Height = 13
            Caption = '~Data Type'
            FocusControl = wwcbDataType
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblFormat: TevLabel
            Left = 448
            Top = 35
            Width = 134
            Height = 13
            Caption = '~General Ledger Format'
            FocusControl = edtFormat
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblLevelValue: TevLabel
            Left = 12
            Top = 74
            Width = 56
            Height = 13
            Caption = 'Level Value'
            FocusControl = wwlcLevelValue
          end
          object lblDataValue: TevLabel
            Left = 230
            Top = 74
            Width = 53
            Height = 13
            Caption = 'Data Value'
            FocusControl = wwlcDataValue
          end
          object edtFormat: TevDBEdit
            Left = 448
            Top = 50
            Width = 210
            Height = 21
            DataField = 'GENERAL_LEDGER_FORMAT'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwcbDataType: TevDBComboBox
            Left = 230
            Top = 50
            Width = 210
            Height = 21
            HelpContext = 13015
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'DATA_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Default'#9'%'
              'E/D'#9'E'
              'Federal'#9'F'
              'EE OASDI'#9'O'
              'ER OASDI'#9'R'
              'EE Medicare'#9'M'
              'ER Medicare'#9'D'
              'FUI'#9'U'
              'EIC'#9'I'
              'Backup Withholding'#9'B'
              'State'#9'S'
              'EE SDI'#9'T'
              'ER SDI'#9'A'
              'Local'#9'L'
              'EE SUI'#9'1'
              'ER SUI'#9'2'
              'Billing'#9'G'
              'Agency Check'#9'3'
              'Net Pay'#9'5'
              'Tax Impound'#9'6'
              'Trust Impound'#9'7'
              'WC Impound'#9'8'
              '3rd Party Check'#9'9'
              '3rd Party Tax'#9'0'
              'Tax Check'#9'4'
              'Federal Offset'#9'!'
              'EE OASDI Offset'#9'@'
              'ER OASDI Offset'#9'#'
              'EE Medicare Offset'#9'$'
              'ER Medicare Offset'#9'^'
              'FUI Offset'#9'&'
              'EIC Offset'#9'*'
              'Backup Withholding Offset'#9'('
              'State Offset'#9')'
              'EE SDI Offset'#9'-'
              'ER SDI Offset'#9'+'
              'Local Offset'#9'['
              'EE SUI Offset'#9']'
              'ER SUI Offset'#9':'
              'Billing Offset'#9';'
              'Agency Check Offset'#9'~'
              'ER Match Offset'#9','
              'Tax Impound Offset'#9'.'
              'Trust Impound Offset'#9'/'
              'WC Impound Offset'#9'W'
              'E/D Offset'#9'X')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
            OnChange = wwcbDataTypeChange
          end
          object wwcbLevelType: TevDBComboBox
            Left = 12
            Top = 50
            Width = 210
            Height = 21
            HelpContext = 13015
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'LEVEL_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Default'#9'%'
              'Division'#9'D'
              'Branch'#9'B'
              'Department'#9'P'
              'Team'#9'N'
              'Employee'#9'E'
              'Job'#9'J')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
            OnChange = wwcbLevelTypeChange
          end
          object wwlcLevelValue: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 210
            Height = 21
            HelpContext = 13003
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name')
            DataField = 'LEVEL_NBR'
            DataSource = wwdsDetail
            Style = csDropDownList
            Enabled = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnDropDown = wwlcLevelValueDropDown
          end
          object wwlcDataValue: TevDBLookupCombo
            Left = 230
            Top = 89
            Width = 210
            Height = 21
            HelpContext = 13003
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name')
            DataField = 'DATA_NBR'
            DataSource = wwdsDetail
            Style = csDropDownList
            Enabled = False
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object btnMultiInsert: TevBitBtn
            Left = 448
            Top = 89
            Width = 210
            Height = 21
            Caption = 'Group Insert'
            TabOrder = 5
            OnClick = btnMultiInsertClick
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
              CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
              E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
              7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
              82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
              9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
              55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
              5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
              CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
              0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
              76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
              FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
              16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
              8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
              FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
              38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
              9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
              FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
              51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
              9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
              66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
              9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
              76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
              9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
              FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
              59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
              9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
              FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
              0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
              87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
              FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
              55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
              556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
              B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
              88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
              E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
              78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_GENERAL_LEDGER.CO_GENERAL_LEDGER
    OnDataChange = wwdsDataChange
  end
  object evPopupMenu1: TevPopupMenu
    Left = 360
    Top = 112
    object CopySelectedGLs: TMenuItem
      Caption = 'Copy Selected GL'#39's'
      OnClick = CopySelectedGLsClick
    end
  end
end
