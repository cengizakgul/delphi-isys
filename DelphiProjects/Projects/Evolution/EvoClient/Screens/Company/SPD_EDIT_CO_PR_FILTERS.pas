// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_PR_FILTERS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Mask,
  wwdbedit, wwdblook, EvUIComponents, EvUtils, EvClientDataSet,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, isUIwwDBLookupCombo, ISBasicClasses,
  isUIwwDBEdit, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_PR_FILTERS = class(TEDIT_CO_BASE)
    tsDetail: TTabSheet;
    grdFilters: TevDBGrid;
    dsDivision: TevDataSource;
    dsBranch: TevDataSource;
    dsDepartment: TevDataSource;
    dsTeam: TevDataSource;
    CO_BRANCH: TevClientDataSet;
    CO_DEPARTMENT: TevClientDataSet;
    CO_TEAM: TevClientDataSet;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpCOPayrollFilerSummary: TisUIFashionPanel;
    fpCOPayrollFilerDetails: TisUIFashionPanel;
    lblName: TevLabel;
    edtName: TevDBEdit;
    lblDivision: TevLabel;
    lcDivision: TevDBLookupCombo;
    txtDivName: TevDBText;
    txtBranchName: TevDBText;
    txtDeptName: TevDBText;
    txtTeamName: TevDBText;
    lblPayGroup: TevLabel;
    lblTeam: TevLabel;
    lblDepartment: TevLabel;
    lblBranch: TevLabel;
    lcPayGroup: TevDBLookupCombo;
    lcTeam: TevDBLookupCombo;
    lcDepartment: TevDBLookupCombo;
    lcBranch: TevDBLookupCombo;
    sbFilterDetail: TScrollBox;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure lcDivisionCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure lcBranchCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure lcDepartmentCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure AfterDataSetsReopen; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

{ TEDIT_CO_PR_FILTERS }

function TEDIT_CO_PR_FILTERS.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_BRANCH') or (sName = 'CO_DEPARTMENT') or (sName = 'CO_TEAM') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_PR_FILTERS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);
  AddDS(DM_COMPANY.CO_PAY_GROUP, aDS);
  inherited;
end;

function TEDIT_CO_PR_FILTERS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_PR_FILTERS;
end;

function TEDIT_CO_PR_FILTERS.GetInsertControl: TWinControl;
begin
  Result := edtName;
end;

procedure TEDIT_CO_PR_FILTERS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  lcBranch.Enabled := not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DIVISION_NBR').IsNull;
  lcDepartment.Enabled := not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DIVISION_NBR').IsNull and
                          not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_BRANCH_NBR').IsNull;
  lcTeam.Enabled := not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DIVISION_NBR').IsNull and
                    not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_BRANCH_NBR').IsNull and
                    not DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DEPARTMENT_NBR').IsNull;
end;

procedure TEDIT_CO_PR_FILTERS.AfterDataSetsReopen;
begin
  inherited;
  CO_BRANCH.Data := DM_COMPANY.CO_BRANCH.Data;
  CO_DEPARTMENT.Data := DM_COMPANY.CO_DEPARTMENT.Data;
  CO_TEAM.Data := DM_COMPANY.CO_TEAM.Data;
  dsDepartment.DoDataChange(Self, nil);
  dsBranch.DoDataChange(Self, nil);
  dsDivision.DoDataChange(Self, nil);
end;

procedure TEDIT_CO_PR_FILTERS.lcDivisionCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if DM_COMPANY.CO_PR_FILTERS.State in [dsInsert, dsEdit] then
  begin
    DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_BRANCH_NBR').Clear;
    DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DEPARTMENT_NBR').Clear;
    DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_TEAM_NBR').Clear;
  end;
end;

procedure TEDIT_CO_PR_FILTERS.lcBranchCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if DM_COMPANY.CO_PR_FILTERS.State in [dsInsert, dsEdit] then
  begin
    DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_DEPARTMENT_NBR').Clear;
    DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_TEAM_NBR').Clear;
  end;
end;

procedure TEDIT_CO_PR_FILTERS.lcDepartmentCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if DM_COMPANY.CO_PR_FILTERS.State in [dsInsert, dsEdit] then
    DM_COMPANY.CO_PR_FILTERS.FieldByName('CO_TEAM_NBR').Clear;
end;

procedure TEDIT_CO_PR_FILTERS.Activate;
begin
  inherited;

end;

initialization
  RegisterClass(TEDIT_CO_PR_FILTERS);

end.
