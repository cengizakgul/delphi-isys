inherited EDIT_CO_ESSTIMEOFFMANAGEMENT: TEDIT_CO_ESSTIMEOFFMANAGEMENT
  Width = 1226
  Height = 667
  inherited Panel1: TevPanel
    Width = 1226
    inherited pnlFavoriteReport: TevPanel
      Left = 1074
    end
    inherited pnlTopLeft: TevPanel
      Width = 1074
    end
  end
  inherited PageControl1: TevPageControl
    Width = 1226
    Height = 613
    HelpContext = 29501
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 1218
        Height = 584
        inherited pnlBorder: TevPanel
          Width = 1214
          Height = 580
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 1214
          Height = 580
          inherited pnlFashionBody: TevPanel
            inherited pnlSubbrowse: TevPanel
              Width = 492
              Height = 465
            end
          end
          inherited Panel3: TevPanel
            Width = 1166
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 1166
            Height = 473
            inherited Splitter1: TevSplitter
              Height = 469
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 623
              Height = 469
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 573
                Height = 389
                IniAttributes.SectionName = 'TEDIT_CO_ESSTIMEOFFMANAGEMENT\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 626
              Height = 469
            end
          end
        end
      end
    end
    object tshtDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      OnEnter = tshtDetailsEnter
      object sbESSTimeOff: TScrollBox
        Left = 0
        Top = 0
        Width = 1218
        Height = 584
        Align = alClient
        TabOrder = 0
        object Panel6: TPanel
          Left = 579
          Top = 272
          Width = 1049
          Height = 324
          BevelOuter = bvNone
          TabOrder = 2
        end
        object fpDisplayRequests: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 674
          Height = 115
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDisplayRequests'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Display Requests Based On'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel2: TevLabel
            Left = 12
            Top = 56
            Width = 49
            Height = 13
            Caption = 'Date From'
          end
          object evLabel3: TevLabel
            Left = 228
            Top = 56
            Width = 66
            Height = 13
            Caption = 'ESS Manager'
          end
          object evLabel4: TevLabel
            Left = 356
            Top = 56
            Width = 24
            Height = 13
            Caption = 'Type'
          end
          object evLabel5: TevLabel
            Left = 463
            Top = 56
            Width = 42
            Height = 13
            Caption = 'EE Code'
          end
          object evLabel1: TevLabel
            Left = 121
            Top = 56
            Width = 39
            Height = 13
            Caption = 'Date To'
          end
          object cbStatusPending: TevCheckBox
            Left = 12
            Top = 35
            Width = 70
            Height = 17
            Caption = 'Pending'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object cbStatusUsed: TevCheckBox
            Left = 228
            Top = 35
            Width = 70
            Height = 17
            Caption = 'Used'
            TabOrder = 2
          end
          object cbStatusApproved: TevCheckBox
            Left = 121
            Top = 35
            Width = 70
            Height = 17
            Caption = 'Approved'
            TabOrder = 1
          end
          object cbStatusDenied: TevCheckBox
            Left = 335
            Top = 35
            Width = 55
            Height = 17
            Caption = 'Denied'
            TabOrder = 3
          end
          object evdtFrom: TevDBDateTimePicker
            Left = 12
            Top = 71
            Width = 100
            Height = 21
            HelpContext = 10561
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object evdtTo: TevDBDateTimePicker
            Left = 121
            Top = 71
            Width = 100
            Height = 21
            HelpContext = 10561
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            Epoch = 1950
            ShowButton = True
            TabOrder = 5
          end
          object cbType: TevDBComboBox
            Left = 356
            Top = 71
            Width = 100
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
          end
          object edEECode: TevEdit
            Left = 463
            Top = 71
            Width = 100
            Height = 21
            TabOrder = 8
          end
          object cbManager: TevDBComboBox
            Left = 228
            Top = 71
            Width = 120
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 6
            UnboundDataType = wwDefault
          end
          object butnRefresh: TevBitBtn
            Left = 572
            Top = 67
            Width = 78
            Height = 25
            Caption = 'Refresh'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
            OnClick = butnRefreshClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5DADADACC
              CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFDDDDDDC8BEB1BD9768B88444B98545FFFFFFFFFFFFCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCDCDCDFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDBCBCBB9292927E
              7E7D7F7E7EFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCFFFFFFFFFFFF
              E1E1E1BF9C72BE8A4ADEAC66F4C57BB88343FFFFFFFFFFFFB98545B78343B681
              42B78242B98444BB8849FFFFFFFFFFFFFFFFFFE1E1E1979797838383A4A4A4BD
              BCBC7E7E7DFFFFFFFFFFFF7F7E7E7D7C7C7C7B7B7D7C7C7F7E7EFFFFFFF9F9F9
              C2A37EC18C4CECBC72F1C581F8D39DB78242FFFFFFFFFFFFB78343F9C97EF6C6
              7BF5CC8CD2A771FFFFFFFFFFFFFFFFFFFAF9F99F9F9E868686B3B3B3BDBCBCCD
              CCCC7D7C7CFFFFFFFFFFFF7D7C7CC0C0C0BEBDBDC4C4C4A1A1A1FFFFFFD9D3CB
              BA8646E9B86DEDC07DEFCE9CD5AC71B88343FFFFFFFFFFFFB68141F2C47EF0C4
              82EAB86FBC8947E5E5E5FFFFFFFFFFFFD2D2D180807FB0AFAFB8B8B8C8C8C7A5
              A5A57E7E7DFFFFFFFFFFFF7C7B7BBCBCBBBCBCBBB0AFAF828282FFFFFFC09E74
              D2A059E6B368EDCC9AC08F51D8B995F6EFE6FFFFFFFFFFFFB78241F5D9ACF0CF
              9FEAB86CD4A25CC3A988FFFFFFFFFFFF999999989898AAAAAAC6C6C6888888B5
              B5B5EEEEEEFFFFFFFFFFFF7C7B7BD3D3D3CAC9C9B0AFAF9A9A9AFFFFFFB88444
              DDAB61E2B167D4AB77D7BA95FFFFFFFFFFFFFFFFFFFFFFFFB98443C6995FD7B2
              82E6B266E0AE65B98547FFFFFFFFFFFF7E7E7DA3A3A3A9A8A8A5A5A5B5B5B5FF
              FFFFFFFFFFFFFFFFFFFFFF7E7E7D939393ACACACAAA9A9A6A6A6FFFFFFB88344
              DFAB5FE2B774C6995FFEFDFCFEFEFEFFFFFFFFFFFFFFFFFFBD8B4DFDFBF9C697
              5FE6BB79E5B36AB78344FFFFFFFFFFFF7E7E7DA3A3A3AFAFAF939393FEFDFDFE
              FEFEFFFFFFFFFFFFFFFFFF868585FBFBFB919191B3B3B3ABABABFFFFFFB88343
              DFB373DBA85EB78243FDFDFDCECECDFFFFFFFFFFFFFFFFFFFFFEFDFEFEFEB682
              42DFAC63E4B877B78243FFFFFFFFFFFF7E7E7DABABABA1A0A07D7C7CFEFDFDCE
              CDCDFFFFFFFFFFFFFFFFFFFEFEFEFEFEFE7C7B7BA4A4A4B1B0B0FFFFFFB88342
              E2BB83D59F52BF8B48D5D3D0BC894BFFFFFFFFFFFFFFFFFFFFFFFFE3E2E1C08C
              49D9A557E5C087B78242FFFFFFFFFFFF7D7C7CB4B4B4979797858484D3D2D283
              8383FFFFFFFFFFFFFFFFFFFFFFFFE2E2E28685859D9D9DB9B9B9FFFFFFB98547
              E1C192CF9A4AC18C48C0955EBA8545FFFFFFFFFFFFF8F8F8E2E2E2C1A481C792
              4BD39E4FE3C395B88241FFFFFFFFFFFF80807FBBBBBB9292928685858F8F8F80
              807FFFFFFFFFFFFFF9F9F8E2E2E2A09F9F8B8B8B969595BEBDBDFFFFFFDCC1A0
              D6B386D4A661C99243E4C79DB98443FFFFFFFFFFFFCAC3BAC2A581BC8746CD98
              4AD8AC6DD9B98ECFAB80FFFFFFFFFFFFBDBCBCAEAEAE9F9F9E8A8A8AC2C2C17E
              7E7DFFFFFFFFFFFFC2C2C1A1A0A0818080909090A5A5A5B4B4B4FFFFFFFFFFFF
              BD8D4FD1A668C48B39D9B580B88241FFFFFFFFFFFFBA8646C28D45C99344D09E
              56E7CEAABD8B4FF7F1E9FFFFFFFFFFFFFFFFFF8686869F9F9E838383AFAFAF7D
              7C7CFFFFFFFFFFFF80807F8686868B8B8B979696CAC9C9868585FFFFFFCDCDCD
              CCA472E6CFACE9D3B2EAD5B4B88341FFFFFFFFFFFFB98444CD9C54DAB57FEAD5
              B5C69A64D6B791FFFFFFFFFFFFFFFFFFCDCCCC9F9F9ECACACACFCFCFD1D1D17D
              7C7CFFFFFFFFFFFF7F7E7E959594AFAFAFD1D1D1959594B2B2B2FFFFFFBC8849
              B98444B88241B8813FB88241BA8545FFFFFFFFFFFFB98342EAD5B6DBBD95C090
              54D1AD81FFFFFFFFFFFFFFFFFFFFFFFF8282827F7E7E7D7C7C7C7B7B7D7C7C80
              807FFFFFFFFFFFFF7E7E7DD1D1D1B8B8B88A8A8AA7A7A7FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA8544BB8849CBA374F1E7
              DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF7F7E7E8281819E9E9EE5E5E5FFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object fpUpdateStates: TisUIFashionPanel
          Left = 690
          Top = 8
          Width = 202
          Height = 115
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Update Status To'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object BtnApprove: TevBitBtn
            Left = 12
            Top = 35
            Width = 79
            Height = 25
            Caption = 'Approved'
            Default = True
            TabOrder = 0
            OnClick = BtnApproveClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
              CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
              E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
              7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              E1E1E144A27700905001A16901AB7601AC7901AC7901AB7601A16900905055A8
              82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939D9D9D9E
              9E9E9E9E9E9D9D9D9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
              55A88200915202AC7700C38C00D79B00DA9C00DA9C00D79C01C38C01AB760092
              5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C6C6C6C9
              C8C8C9C8C8C7C7C7B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
              0090510FB48300D29800D59800D19200CF9000D09100D39600D69B00D19801AB
              76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C4C4C4C0C0C0BE
              BEBEBFBFBFC3C2C2C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
              16AB7810C99600D39700CD8CFFFFFFFFFFFFFFFFFF00CC8C00D19500D59B01C1
              8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC3C2C2BCBCBBFFFFFFFF
              FFFFFFFFFFBBBBBBC0C0C0C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
              39C49D00D19800CB8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CA8C00CF9600D2
              9B01AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BABABAFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFB9B9B9BFBFBFC3C2C29D9D9D7D7C7CFFFFFFFFFFFF008946
              52D2B000CC92FFFFFFFFFFFFFFFFFF00C484FFFFFFFFFFFFFFFFFF00C88D00D0
              9A00AD79008B4AFFFFFFFFFFFF7A7979C7C7C7BCBCBBFFFFFFFFFFFFFFFFFFB3
              B3B3FFFFFFFFFFFFFFFFFFB8B7B7C0C0C09F9F9E7C7B7BFFFFFFFFFFFF008845
              68DDBE00C991FFFFFFFFFFFF00C68C00C89100C58BFFFFFFFFFFFFFFFFFF00CC
              9600AD78008B4AFFFFFFFFFFFF797979D3D2D2B9B9B9FFFFFFFFFFFFB6B6B5B8
              B8B8B5B5B5FFFFFFFFFFFFFFFFFFBDBCBC9F9F9E7C7B7BFFFFFFFFFFFF008846
              76E0C600CB9800C59000C69100C89500C99700C89400C38CFFFFFFFFFFFF00C7
              9200AB75008C4BFFFFFFFFFFFF797979D7D6D6BCBCBBB6B6B5B7B6B6B9B9B9BA
              BABAB9B9B9B4B4B4FFFFFFFFFFFFB8B7B79D9D9D7D7C7CFFFFFFFFFFFF41A675
              59C9A449DEBC00C79400C89700C99800C99900C99800C79400C38EFFFFFF00BD
              8A00A06740A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B9B9B9BABABABB
              BBBBBABABAB8B8B8B4B4B4FFFFFFAFAFAF9292929B9B9BFFFFFFFFFFFFCCE8DB
              0A9458ADF8E918D0A700C49500C69700C69800C79800C79800C69700C59612B5
              85008F50CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B8B7B7B8
              B8B8B9B9B9B9B9B9B8B7B7B7B6B6A7A7A7818080E6E6E6FFFFFFFFFFFFFFFFFF
              55B185199C63BCFFF75EE4C900C59A00C39600C49700C59A22CAA22FC1960293
              556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB8B7B7B6
              B6B5B7B6B6B8B7B7BEBDBDB4B4B4858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF6ABB940E965974D5B6A0F4E194EFDC7CE6CC5ED6B52EB58703915255B3
              88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBEDEDEDE8
              E7E7DDDDDDCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
              78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object BtnDeny: TevBitBtn
            Left = 100
            Top = 35
            Width = 78
            Height = 25
            Caption = 'Denied'
            TabOrder = 1
            OnClick = BtnDenyClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFF6F6FAD6D6EEBCBCE5B7B7E3C7C7E8E6E6F4FFFFFEFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAECECECE0
              E0E0DDDDDDE4E4E4F3F3F3FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFBFBFDBDBDE66666CF3333C71D1DC51818C42626C54747C98F8FD9E3E3
              F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDE1E1E1C3C3C3B7B7B7B3
              B3B3B2B2B2B6B6B6BBBBBBD0D0D0F2F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              F3F3F98181D41E1EC10D0DC91313D01818D21919D41616D31010CF1010C74343
              C8C6C6EAFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9CBCBCBB0B0B0B6B6B6BDBDBDC0
              C0C0C2C2C2C0C0C0BCBCBCB3B3B3B9B9B9E5E5E5FFFFFFFFFFFFFFFFFFF9F9FC
              8080D30B0BBD0F0FCA1515CD1515CE1616CE1717CF1616CF1515CF1515CF0B0B
              C83434C3CECEECFFFFFFFFFFFFFCFCFCC8C8C8A6A6A6B4B4B4B9B9B9BABABABC
              BCBCBBBBBBBBBBBBBCBCBCBCBCBCB4B4B4B2B2B2E8E8E8FFFFFFFFFFFFBCBCE5
              1919B70C0CC11313C61515C41313C11212C61313CB1212C51515C51616CA1616
              CC0909C35E5EC9F3F3F9FFFFFFE0E0E0A1A1A1A7A7A7AFAFAFADADADA9A9A9AF
              AFAFB5B5B5AFAFAFAEAEAEB6B6B6B9B9B9ADADADBDBDBDF8F8F8F7F7FB6464C8
              0707B61010BD0D0DBD5757CDAEAEE44F4FCA0E0EBD8585DAA5A5E42B2BCB1010
              C71010C81B1BBDBFBFE7FEFEFCB9B9B99C9C9CA4A4A4A3A3A3BABABADADADAB6
              B6B6A3A3A3CBCBCBDADADAB6B6B6B1B1B1B1B1B1A8A8A8E1E1E1D6D6EF3030BD
              0C0CBE1010BD0B0BBA2A2ABFD3D3EFCACAEA7474CBFAFAFB9595E11212C31212
              C51313C50B0BBF8686D6F3F3F0AAAAA9A5A5A5A4A4A49F9F9FA7A7A7E9E9E9E3
              E3E3B8B8B8F9F9F9D6D6D6ABABABAEAEAEAFAFAFA7A7A7CCCCCCBCBCE61A1AC0
              1111C51212C31212C10404BA5151CAEFEFF9F7F7F9C8C8EC2323C20B0BBF1212
              C21212C20909C26464D0E7E7E6ACACABADADADAAAAAAA8A8A89F9F9FB5B5B5F6
              F6F6F7F7F7E5E5E5AAAAAAA7A7A7AAAAAAAAAAAAACACACC3C3C3B7B7E31616C4
              1313C81313C81414C80D0DC72323C4D4D4F0FFFFFF9292DA0A0ABD1111C21212
              C21111C10808C45E5ED0E4E4E3B1B1B0B1B1B1B2B2B2B3B3B3B1B1B1AEAEAEEB
              EBEBFFFFFFCECECEA5A5A5ACACACAAAAAAA7A7A7ADADADC3C3C3C7C7E92525C5
              1313CB1616CD1515CF0F0FCB8383DFF4F4FBDADAF6E4E4F64040D20D0DCD1515
              CD1313C60909C17272D3ECECEAB5B5B4B7B7B7B9B9B9BDBDBDB8B8B8D2D2D2F9
              F9F9F3F3F3F2F2F2C0C0C0B9B9B9B9B9B9AEAEAEAAAAAAC8C8C8E6E6F44747C9
              0F0FCB1919D21515D54646DBE1E1F7ABABEF4A4ADFE1E1FAACACEC1D1DD51515
              D51515D21010C2A0A0DEF9F9F7B9B9B9B7B7B7C1C1C1C4C4C4CDCDCDF4F4F4E9
              E9E9D2D2D2F7F7F7E5E5E5C4C4C4C4C4C4BFBFBFAEAEAED7D7D7FEFEFE9090D9
              1010C71A1AD71D1DDA3939E15C5CE93636E31C1CE14949E75858E82727DE1919
              D90E0ED23636C8DFDFF1FFFFFFD0D0D0B5B5B5C7C7C7CDCDCDD5D5D5DFDFDFD9
              D9D9D4D4D4DCDCDCDFDFDFD1D1D1CBCBCBC0C0C0B9B9B9EFEFEFFFFFFFE2E2F3
              4545CD1010D52222E12121E31C1CE32323E62626E61F1FE51E1EE42222E31B1B
              DE1414CD9595DDFEFEFEFFFFFFF1F1F1C0C0C0C5C5C5D5D5D5D7D7D7D9D9D9DC
              DCDCDDDDDDDCDCDCD9D9D9D8D8D8D0D0D0BDBDBDD5D5D5FEFEFEFFFFFFFFFFFF
              C6C6EA3838CE1818DD2525E92A2AEB2A2AEC2A2AEC2B2BED2929EB1F1FE61A1A
              D47B7BD8F4F4F9FFFFFFFFFFFFFFFFFFE7E7E7C2C2C2D1D1D1E0E0E0E4E4E4E4
              E4E4E4E4E4E4E4E4E4E4E4DDDDDDC8C8C8CFCFCFF9F9F9FFFFFFFFFFFFFFFFFF
              FFFFFFCDCDEC6464D42E2ED72727E32929EA2A2AEB2828E82727DE4141D39898
              DDF5F5FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9CBCBCBCDCDCDDBDBDBE3
              E3E3E5E5E5E0E0E0D5D5D5C9C9C9D7D7D7F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFF4F4F8C1C1E78E8EDD7171D66A6AD57C7CD9A4A4E1DEDEF0FFFF
              FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8E2E2E2D5D5D5CE
              CECECCCCCCD2D2D2DBDBDBEDEDEDFEFEFEFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object BtnUsed: TevBitBtn
            Left = 12
            Top = 67
            Width = 78
            Height = 25
            Caption = 'Used'
            TabOrder = 2
            OnClick = BtnUsedClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC3A82CA397FC8
              387DC7387CC7397CC7397CC7387DC7387EC9387EC9387DC7397CC7397CC7387C
              C7387DC7397FC83A82CA9595949292929090908F8F8F8F8F8F8F8F8F90909091
              91919191919090908F8F8F8F8F8F8F8F8F9090909292929595943B86CD55E3FF
              55E3FF56E1FF56E1FF56E2FF56E5FF55E9FF55E9FF56E5FF56E2FF56E1FF56E1
              FF55E3FF55E3FF3B86CD979797E6E6E6E6E6E6E4E4E4E4E4E4E5E5E5E7E7E6EA
              EAEAEAEAEAE7E7E6E5E5E5E4E4E4E4E4E4E6E6E6E6E6E69797978CB6E07AB8E4
              52DBFF4BD5FF4DD5FF4DD6FF4BDCFF67493F67493F4BDCFF4DD6FF4DD5FF4BD5
              FF52DBFF7AB8E48CB6E0BEBEBEC1C1C1E0DFDFDCDBDBDCDBDBDCDCDCE0E0E049
              4949494949E0E0E0DCDCDCDCDBDBDCDBDBE0DFDFC1C1C1BEBEBEFFFFFF3F82C9
              A3E0FA40CFFF44CEFF46CFFF44D6FF775C50775C5044D6FF46CFFF44CEFF40CF
              FFA3E0FA3F82C9FFFFFFFFFFFF949393E4E4E4D7D7D7D7D6D6D7D7D7DCDCDC5B
              5B5B5B5B5BDCDCDCD7D7D7D7D6D6D7D7D7E4E4E4949393FFFFFFFFFFFFCADEF1
              5B97D49EE8FF38C7FF3DC8FF3DCFFF4BBCE94BBCE93DCFFF3DC8FF38C7FF9EE8
              FF5B97D4CADEF1FFFFFFFFFFFFE2E2E2A5A5A5ECEBEBD2D2D1D3D2D2D7D7D7C4
              C4C4C4C4C4D7D7D7D3D2D2D2D2D1ECEBEBA5A5A5E2E2E2FFFFFFFFFFFFFFFFFF
              69A1D790BFE66FD7FF32C3FF34CBFF644B3F644B3F34CBFF32C3FF6FD7FF90BF
              E669A1D7FFFFFFFFFFFFFFFFFFFFFFFFADADADC8C8C7DEDEDECFCFCFD4D4D44A
              4A4A4A4A4AD4D4D4CFCFCFDEDEDEC8C8C7ADADADFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF3C82C9C1E8FD50CCFF27C5FF69554C69554C27C5FF50CCFFC1E8FD3C82
              C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF949393ECECECD6D6D6D0D0D055
              5454555454D0D0D0D6D6D6ECECEC949393FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFBAD4ED5797D5C6F3FF3ACAFF6C5B546C5B543ACAFFC6F3FF5797D5BAD4
              EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D9D9A6A6A6F5F5F5D3D3D35A
              5A5A5A5A5AD3D3D3F5F5F5A6A6A6D9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF5996D386BEEAA4ECFF6C5D536C5D53A4ECFF86BEEA5996D3FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4A4A4C8C8C7EFEEEE5C
              5C5C5C5C5CEFEEEEC8C8C7A4A4A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFF8FBFD3A83CBB2E5FF9EC8DE9EC8DEB2E5FF3A83CBF8FBFDFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFB969595EAEAEACC
              CCCCCCCCCCEAEAEA969595FCFCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFA9C9E95398D7C1F0FFC1F0FF5398D7A9C9E9FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCFA7A7A7F3
              F2F2F3F2F2A7A7A7CFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF5996D284C3F184C3F15996D2FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4A4A4CD
              CCCCCDCCCCA4A4A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFE6EFF93384CD3384CDE6EFF9FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F197
              9696979696F2F2F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object BtnUpdate: TevBitBtn
            Left = 101
            Top = 67
            Width = 78
            Height = 25
            Caption = 'Edit'
            TabOrder = 3
            OnClick = BtnUpdateClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCC
              CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF566C814A6785699FC6CCCCCCFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7170706D
              6D6DA7A7A7CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBDBDBCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCC5785AA80A7B890D6FF356A9DCCCCCCCCCCCCCCCC
              CCCCCCCCDBDBDBFFFFFFDCDBDBCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC8D8D8DAA
              A9A9DEDEDE777676CCCCCCCCCCCCCCCCCCCCCCCCDCDBDBFFFFFFBD9648B67E0E
              B47B09B47A07B57A05BD7900699FC688E5FF7FD3FF139BFF326BA4CE8501BD80
              09B87F0FBD9648FFFFFF8D8D8D767675737373737373737373737373A7A7A7E8
              E8E8DCDBDBB5B5B57979797E7E7D7878777776768D8D8DFFFFFFB67E0EFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1C6DC03DC3FF2AAAFF159CFF2A64A0FFFF
              FFFFFFFFB77F0FFFFFFF767675FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83
              8383CFCFCFBEBEBEB6B6B5747373FFFFFFFFFFFF777676FFFFFFB47B09FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF216FBF42C5FF2BABFF1099FF1F5E
              99FFFFFFB97F0BFFFFFF737373FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFF858484D0D0D0BFBFBFB4B4B46C6C6CFFFFFF777676FFFFFFB47A07FFFFFF
              FBFCFDFAFAFAFAFAFAFAFAFBFCFCFBFFFFFCFFFFFF2270C03FC5FF20A9FF7EAC
              D5747173C08406FFFFFF737373FFFFFFFEFDFDFBFBFBFBFBFBFCFCFBFEFDFDFF
              FFFFFFFFFF868585D0D0D0BEBDBDB5B5B57271717C7B7BFFFFFFB47A07FFFFFF
              F2F3F3F2F2F1F2F2F1F2F2F1F2F2F1F4F3F1FAF7F3FFFFF51C70C4ABD8EE9087
              7EBEBDBA737B76CCCCCC737373FFFFFFF4F4F4F3F3F3F3F3F3F3F3F3F3F3F3F4
              F4F4F8F8F8FEFEFE868686DCDCDC868686BEBDBD7A7979CCCCCCB47B08FFFFFF
              EAEAEBEBEAE9EBEAE9EBEAE9EBEAE9EBEAE9EDEBE9F2EEEBFBF4EEA19993E7E5
              E1878B82B979B79769CC737373FFFFFFECEBEBECEBEBECEBEBECEBEBECEBEBEC
              EBEBECECECEFEEEEF4F4F4999999E5E5E58989898F8F8F8A8A8AB47B08FFFFFF
              E1E0E1E2E1E0E2E1E0E2E1E0E2E1E0E2E1E0E2E1E0E3E2E1E6E4E3EAE9E79B9B
              95DEAEDECA95C7AE7CD0737373FFFFFFE2E2E2E2E2E2E2E2E2E2E2E2E2E2E2E2
              E2E2E2E2E2E3E3E2E5E5E5EAEAEA9A9A9ABEBEBEA6A6A6989898B47B08FFFFFF
              D5D6D6D6D6D5D7D7D5D7D7D5D7D7D5D7D7D5D7D7D5D7D7D6D7D7D6D9D9D7DCDF
              DAB97ED5BB87DFFFFFFF737373FFFFFFD7D7D7D7D6D6D7D7D7D7D7D7D7D7D7D7
              D7D7D7D7D7D8D8D8D8D8D8DADADADFDFDE9C9C9CA4A4A4FFFFFFB57C0AFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFB67F00FFFFFF747373FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF777676FFFFFFB57E0FF7E0BF
              E0A649E0A74CE0A84DE0A84DE0A84DE0A84DE0A84DE0A84DE0A84DE0A74CE0A6
              49F8E1BFB67F0EFFFFFF767675DCDCDC9D9D9D9E9E9E9F9F9E9F9F9E9F9F9E9F
              9F9E9F9F9E9F9F9E9F9F9E9E9E9E9D9D9DDDDDDD777676FFFFFFB68013F2D8AE
              D49222D49426D49427D49427D49427D49427D49427D49427D49427D49426D492
              22F2D8AEB68013FFFFFF787877D3D2D28989898B8B8B8B8B8B8B8B8B8B8B8B8B
              8B8B8B8B8B8B8B8B8B8B8B8B8B8B898989D3D2D2787877FFFFFFB88217EED09B
              ECCE98ECCE9AECCE9AECCE9AECCE9AECCE9AECCE9AECCE9AECCE9AECCE9AECCE
              98EED09BB88217FFFFFF7A7979C9C8C8C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7
              C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C7C9C8C87A7979FFFFFFCAA14FB88217
              B78116B78116B78116B78116B78116B78116B78116B78116B78116B78116B781
              16B88217CCA557FFFFFF9797977A797979797979797979797979797979797979
              79797979797979797979797979797979797A79799C9C9CFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object isUIFashionPanel1: TisUIFashionPanel
          Left = 8
          Top = 131
          Width = 884
          Height = 400
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'isUIFashionPanel1'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Matching Requests'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwwdgTOA: TevDBGrid
            Left = 12
            Top = 35
            Width = 848
            Height = 343
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'selected'#9'1'#9'Selected'#9#9
              'RequestDate'#9'18'#9'Requestdate'#9#9
              'ToDate'#9'18'#9'Todate'#9#9
              'FromDate'#9'18'#9'Fromdate'#9#9
              'CustomEENumber'#9'9'#9'Customeenumber'#9#9
              'EENbr'#9'10'#9'Eenbr'#9#9
              'LastName'#9'30'#9'Lastname'#9#9
              'FirstName'#9'30'#9'Firstname'#9#9
              'Balance'#9'10'#9'Balance'#9#9
              'Time'#9'10'#9'Time'#9#9
              'Type'#9'40'#9'Type'#9#9
              'EETimeOffAccrualNbr'#9'10'#9'Eetimeoffaccrualnbr'#9#9
              'Description'#9'40'#9'Description'#9#9
              'Notes'#9'40'#9'Notes'#9#9
              'Status'#9'20'#9'Status'#9#9
              'StatusCode'#9'1'#9'Statuscode'#9#9
              'Manager_Note'#9'40'#9'Manager Notes'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_ESSTIMEOFFMANAGEMENT\wwwdgTOA'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            OnCellChanged = wwwdgTOACellChanged
            FixedCols = 1
            ShowHorzScrollBar = True
            EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            ReadOnly = False
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnCalcCellColors = wwwdgTOACalcCellColors
            OnFieldChanged = wwwdgTOAFieldChanged
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            DefaultSort = '-'
            NoFire = False
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 976
    Top = 290
  end
  inherited wwdsDetail: TevDataSource
    DataSet = TORequests
    Left = 982
    Top = 202
  end
  inherited wwdsList: TevDataSource
    Left = 938
    Top = 258
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 307
    Top = 10
  end
  inherited PageControlImages: TevImageList
    Left = 1008
    Top = 96
  end
  inherited dsCL: TevDataSource
    Left = 336
    Top = 10
  end
  inherited DM_CLIENT: TDM_CLIENT
    Top = 10
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 400
    Top = 13
  end
  object TORequests: TevClientDataSet
    ControlType.Strings = (
      'selected;CheckBox;Y;N')
    FieldDefs = <
      item
        Name = 'selected'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'RequestDate'
        DataType = ftDateTime
      end
      item
        Name = 'ToDate'
        DataType = ftDateTime
      end
      item
        Name = 'FromDate'
        DataType = ftDateTime
      end
      item
        Name = 'CustomEENumber'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'EENbr'
        DataType = ftInteger
      end
      item
        Name = 'LastName'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'FirstName'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Balance'
        DataType = ftFloat
      end
      item
        Name = 'Time'
        DataType = ftFloat
      end
      item
        Name = 'Type'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'EETimeOffAccrualNbr'
        DataType = ftInteger
      end
      item
        Name = 'Description'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Notes'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Status'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'StatusCode'
        DataType = ftString
        Size = 1
      end>
    AfterScroll = TORequestsAfterScroll
    Left = 992
    Top = 242
    object Selected: TStringField
      DisplayWidth = 1
      FieldName = 'selected'
      Size = 1
    end
    object RequestDate: TDateTimeField
      DisplayWidth = 18
      FieldName = 'RequestDate'
    end
    object ToDate: TDateTimeField
      DisplayWidth = 18
      FieldName = 'ToDate'
    end
    object AccuralDate: TDateTimeField
      DisplayWidth = 18
      FieldName = 'FromDate'
    end
    object SetEECode: TStringField
      DisplayWidth = 9
      FieldName = 'CustomEENumber'
      Size = 9
    end
    object SetEENbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'EENbr'
    end
    object LastName: TStringField
      DisplayWidth = 30
      FieldName = 'LastName'
      Size = 30
    end
    object FirstName: TStringField
      DisplayWidth = 30
      FieldName = 'FirstName'
      Size = 30
    end
    object Balance: TFloatField
      DisplayWidth = 10
      FieldName = 'Balance'
    end
    object FTime: TFloatField
      DisplayWidth = 10
      FieldName = 'Time'
    end
    object FType: TStringField
      DisplayWidth = 40
      FieldName = 'Type'
      Size = 40
    end
    object AccrualNbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'EETimeOffAccrualNbr'
    end
    object Description: TStringField
      DisplayWidth = 40
      FieldName = 'Description'
      Size = 40
    end
    object Note: TStringField
      DisplayWidth = 40
      FieldName = 'Notes'
      Size = 40
    end
    object Status: TStringField
      DisplayWidth = 20
      FieldName = 'Status'
    end
    object StatusCode: TStringField
      DisplayWidth = 1
      FieldName = 'StatusCode'
      Size = 1
    end
    object TORequestsManager_Note: TStringField
      DisplayLabel = 'Manager Notes'
      FieldName = 'Manager_Note'
      Size = 40
    end
  end
  object TOADetails: TevClientDataSet
    ControlType.Strings = (
      'Time;CustomEdit;evDBSpinEdit1;F')
    FieldDefs = <
      item
        Name = 'EETimeOffAccrualNbr'
        DataType = ftInteger
      end
      item
        Name = 'Eetimeoffaccrualopernbr'
        DataType = ftInteger
      end
      item
        Name = 'AccrualDate'
        DataType = ftDateTime
      end
      item
        Name = 'Hours'
        DataType = ftInteger
      end
      item
        Name = 'Minutes'
        DataType = ftInteger
      end
      item
        Name = 'StatusCode'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Notes'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'Time'
        DataType = ftFloat
      end>
    Filtered = True
    Left = 928
    Top = 208
    object TOADetailsIntegerField3: TIntegerField
      DisplayWidth = 10
      FieldName = 'EETimeOffAccrualNbr'
    end
    object TOADetailsEetimeoffaccrualopernbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'Eetimeoffaccrualopernbr'
    end
    object TOADetailsDateTimeField1: TDateTimeField
      DisplayWidth = 18
      FieldName = 'AccrualDate'
    end
    object TOADetailsIntegerField2: TIntegerField
      DisplayWidth = 10
      FieldName = 'Hours'
    end
    object TOADetailsMinutes: TIntegerField
      DisplayWidth = 10
      FieldName = 'Minutes'
    end
    object TOADetailsStatusCode: TStringField
      DisplayWidth = 1
      FieldName = 'StatusCode'
      Size = 1
    end
    object TOADetailsNotes: TStringField
      DisplayWidth = 512
      FieldName = 'Notes'
      Size = 512
    end
    object TOADetailsTime: TFloatField
      FieldName = 'Time'
    end
  end
end
