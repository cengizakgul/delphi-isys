unit SPD_EDIT_CO_UPDATEESSTIMEOFF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ISBasicClasses,  Mask, wwdbedit,
  Wwdbspin, Grids, Wwdbigrd, Wwdbgrid, DBCtrls, ExtCtrls, DB, Wwdatsrc,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDDClasses, SDataDictclient, SDataStructure,
  Buttons, EvUIComponents;{, Buttons, SDDClasses, SDataDictclient,
  SDataStructure;}

type
  TEDIT_CO_UPDATEESSTIMEOFF = class(TForm)
    wwdsDetail: TevDataSource;
    dsTOADetails: TevDataSource;
    evPanel1: TevPanel;
    evlblCodeandName: TevLabel;
    evLblFromToDate: TevLabel;
    evlblType: TevLabel;
    EvDBMemo1: TEvDBMemo;
    wwwdgTOADetails: TevDBGrid;
    evDBSpinEdit1: TevDBSpinEdit;
    OKBtn: TButton;
    CancelBtn: TButton;
    BtnUsed: TevBitBtn;
   // DM_EMPLOYEE: TDM_EMPLOYEE;
    procedure FormActivate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure evDBSpinEdit1Change(Sender: TObject);
//    procedure BtnUsedClick(Sender: TObject);
//    procedure wwwdgTOADetailsRowChanged(Sender: TObject);
  private
    { Private declarations }
    FCustomCoNumber : string;
  public
    { Public declarations }
    property CustomCoNumber : string read FCustomCoNumber write FCustomCoNumber;
  end;

implementation

uses EvContext, isBaseClasses, EvCommonInterfaces, EvTOARequest, EvExceptions;{, EvUtils, EvDataset, EvConsts, EvTypes;}
{$R *.dfm}

procedure TEDIT_CO_UPDATEESSTIMEOFF.FormActivate(Sender: TObject);
begin
  evlblCodeandName.Caption := '#' + Trim(wwdsDetail.Dataset.FieldByName('CustomEENumber').asString) + ' ' +
                              wwdsDetail.Dataset.FieldByName('FirstName').asString + ' ' +
                              wwdsDetail.Dataset.FieldByName('LastName').asString;
  evLblFromToDate.Caption :=  FormatDateTime('mmmm dd yy',wwdsDetail.Dataset.FieldByName('FromDate').asDateTime) + ' - ' +
                              FormatDateTime('mmmm dd yy',wwdsDetail.Dataset.FieldByName('ToDate').asDateTime);

  evlblType.Caption := wwdsDetail.Dataset.FieldByName('Description').asString;

//  BtnUsed.Enabled  := dsTOADetails.DataSet.FieldByName('ShowUsed').asString[1] = 'Y';
  BtnUsed.Visible := False;

end;

procedure TEDIT_CO_UPDATEESSTIMEOFF.OKBtnClick(Sender: TObject);
var
  Results : IisInterfaceList;
  OneRequestStruct: IEvTOABusinessRequest;
  OneDayStruct : IEvTOARequest;
begin
  try
    dsTOADetails.Dataset.First;
    while not dsTOADetails.Dataset.Eof do
    begin
      if not assigned(OneRequestStruct) then
      begin
        OneRequestStruct := TEvTOABusinessRequest.Create;
        OneRequestStruct.Description := wwdsDetail.Dataset.FieldByName('Description').asString;
        OneRequestStruct.CustomEENumber := wwdsDetail.Dataset.FieldByName('CustomEENumber').asString;
        OneRequestStruct.LastName := wwdsDetail.Dataset.FieldByName('LastName').asString;
        OneRequestStruct.FirstName := wwdsDetail.Dataset.FieldByName('FirstName').asString;
      end;
      // day information
      OneDayStruct := TEvTOARequest.Create(wwdsDetail.Dataset.FieldByName('EENbr').asInteger,
                                           dsTOADetails.Dataset.FieldByName('EETimeOffAccrualNbr').asInteger);
      OneDayStruct.AccrualDate := dsTOADetails.Dataset.FieldByName('AccrualDate').asDateTime;
      OneDayStruct.Hours :=  dsTOADetails.Dataset.FieldByName('Hours').asInteger;
      OneDayStruct.Minutes :=  dsTOADetails.Dataset.FieldByName('Minutes').asInteger;
      OneDayStruct.EETimeOffAccrualOperNbr := dsTOADetails.Dataset.FieldByName('EETimeOffAccrualOperNbr').asInteger;
      OneDayStruct.Status := Trim(dsTOADetails.Dataset.FieldByName('StatusCode').asString);
      OneDayStruct.Notes  := EvDBMemo1.Lines.Text;
      OneRequestStruct.Items.Add(OneDayStruct);
      dsTOADetails.DataSet.Next;
    end;

    if Assigned(OneRequestStruct) then
    begin
       Results := TisInterfaceList.Create;
       Results.Add(OneRequestStruct);
       Context.RemoteMiscRoutines.UpdateTOARequestsForCO(Results, FCustomCoNumber );
    end;
  except
    raise;
  end;
end;

procedure TEDIT_CO_UPDATEESSTIMEOFF.evDBSpinEdit1Change(Sender: TObject);
var
 AHours, AMinutes: integer;
begin
  if ( dsTOADetails.DataSet.State = dsEdit ) then
  begin
    try
      ConvertDoubleToIntegerHourAndMin(evDBSpinEdit1.value, AHours, AMinutes);
    except
      raise EevException.Create('Hours must be entered in quarter hour increments');
      exit;
    end;
    dsTOADetails.Dataset.FieldByName('Hours').asInteger := AHours;
    dsTOADetails.Dataset.FieldByName('Minutes').asInteger := AMinutes;
  end;
end;

end.
