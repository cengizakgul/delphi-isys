// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit ChoseLocalDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,  Grids, Wwdbigrd, Wwdbgrid, Buttons, DB,
  Wwdatsrc, SDataStructure, wwdblook, EvUtils, EvUIComponents,
  ISBasicClasses, LMDCustomButton, LMDButton, isUILMDButton;

type
  TChoseLocal = class(TForm)
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    evDBGrid1: TevDBGrid;
    evDataSource1: TevDataSource;
    procedure evDBGrid1DblClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure evBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ChoseLocal: TChoseLocal;

implementation

{$R *.dfm}

procedure TChoseLocal.evDBGrid1DblClick(Sender: TObject);
begin
  evBitBtn1.Click;
end;

procedure TChoseLocal.evBitBtn1Click(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TChoseLocal.evBitBtn2Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
