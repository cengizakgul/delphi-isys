inherited EDIT_CO_BENEFIT_DISCOUNT: TEDIT_CO_BENEFIT_DISCOUNT
  Width = 706
  Height = 503
  inherited Panel1: TevPanel
    Width = 706
    inherited pnlFavoriteReport: TevPanel
      Left = 554
    end
    inherited pnlTopLeft: TevPanel
      Width = 554
    end
  end
  inherited PageControl1: TevPageControl
    Width = 706
    Height = 449
    HelpContext = 29501
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 698
        Height = 420
        inherited pnlBorder: TevPanel
          Width = 694
          Height = 416
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 694
          Height = 416
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Pensions'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 371
              Top = 59
              Width = 379
              Height = 520
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 646
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 646
            Height = 309
            inherited Splitter1: TevSplitter
              Left = 320
              Height = 305
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Height = 305
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 225
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'13'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
                  'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
                  'FEIN'#9'9'#9'EIN'#9
                  'DBA'#9'40'#9'DBA '#9'F'
                  'UserFirstName'#9'20'#9'CSR First Name'#9'F'
                  'UserLastName'#9'30'#9'CSR Last Name'#9'F'
                  'HomeState'#9'2'#9'Home State'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_DISCOUNT\wwdbgridSelectClient'
                IniAttributes.CheckNewFields = False
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 319
              Height = 305
              Align = alClient
              Visible = True
              Title = 'Discounts'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 269
                Height = 225
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'AMOUNT_TYPE;CustomEdit;cbDiscountType;F')
                Selected.Strings = (
                  'DESCRIPTION'#9'40'#9'Description'#9'F'
                  'AMOUNT'#9'10'#9'Amount'#9'F'
                  'AMOUNT_TYPE'#9'1'#9'Amount type'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_DISCOUNT\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object cbDiscountType: TevDBComboBox
                Left = 65
                Top = 240
                Width = 121
                Height = 21
                ShowButton = True
                Style = csDropDownList
                MapList = False
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'AMOUNT_TYPE'
                DataSource = wwdsDetail
                DropDownCount = 8
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 1
                UnboundDataType = wwDefault
              end
            end
          end
        end
      end
    end
    object tshtPensions: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbPensions: TScrollBox
        Left = 0
        Top = 0
        Width = 698
        Height = 420
        Align = alClient
        TabOrder = 0
        object fpCOPensionDetails: TisUIFashionPanel
          Left = 8
          Top = 385
          Width = 441
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Discount Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblDescription: TevLabel
            Left = 12
            Top = 35
            Width = 62
            Height = 16
            Caption = '~Description'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblValue: TevLabel
            Left = 201
            Top = 35
            Width = 36
            Height = 16
            Caption = '~Value'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edDescription: TevDBEdit
            Left = 12
            Top = 50
            Width = 181
            Height = 21
            DataField = 'DESCRIPTION'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edAmount: TevDBEdit
            Left = 201
            Top = 50
            Width = 83
            Height = 21
            DataField = 'AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object rgType: TevDBRadioGroup
            Left = 292
            Top = 35
            Width = 129
            Height = 37
            Caption = '~Type'
            Columns = 2
            DataField = 'AMOUNT_TYPE'
            DataSource = wwdsDetail
            TabOrder = 2
          end
        end
        object fpCOPensionSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 439
          Height = 369
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgPension: TevDBGrid
            Left = 12
            Top = 36
            Width = 407
            Height = 313
            TabStop = False
            DisableThemesInTitle = False
            ControlType.Strings = (
              'AMOUNT_TYPE;CustomEdit;cbType;F')
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F'
              'AMOUNT'#9'10'#9'Amount'#9'F'
              'AMOUNT_TYPE'#9'1'#9'Amount type'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_DISCOUNT\wwdgPension'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object cbType: TevDBComboBox
            Left = 208
            Top = 216
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'AMOUNT_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_BENEFIT_DISCOUNT.CO_BENEFIT_DISCOUNT
  end
  inherited PageControlImages: TevImageList
    Top = 88
  end
  inherited dsCL: TevDataSource
    Left = 264
  end
end
