object GLGroup_Insert: TGLGroup_Insert
  Left = 411
  Top = 374
  BorderStyle = bsDialog
  Caption = 'Group Insert'
  ClientHeight = 408
  ClientWidth = 533
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    533
    408)
  PixelsPerInch = 96
  TextHeight = 13
  object lblFormat: TevLabel
    Left = 184
    Top = 319
    Width = 108
    Height = 13
    Caption = 'General Ledger Format'
    FocusControl = edtGLMask
  end
  object qlblLevelType: TevLabel
    Left = 21
    Top = 4
    Width = 53
    Height = 13
    Caption = 'Level Type'
    FocusControl = lbLevelType
  end
  object lblDataType: TevLabel
    Left = 270
    Top = 4
    Width = 50
    Height = 13
    Caption = 'Data Type'
    FocusControl = lbDataType
  end
  object lblLevelValue: TevLabel
    Left = 21
    Top = 161
    Width = 56
    Height = 13
    Caption = 'Level Value'
    FocusControl = lbLevelValue
  end
  object lblDataValue: TevLabel
    Left = 270
    Top = 161
    Width = 53
    Height = 13
    Caption = 'Data Value'
    FocusControl = lbDataValue
  end
  object OKBtn: TevBitBtn
    Left = 190
    Top = 371
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 5
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
      DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
      DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
      DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
      DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
      DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
      2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
      A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
      AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
    NumGlyphs = 2
    Margin = 0
  end
  object CancelBtn: TevBitBtn
    Left = 271
    Top = 371
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
      9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
      DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
      DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
      DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
      91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
      999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
    NumGlyphs = 2
    Margin = 0
  end
  object lbLevelType: TListBox
    Left = 21
    Top = 21
    Width = 243
    Height = 134
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 0
    OnClick = lbLevelTypeClick
  end
  object lbDataType: TListBox
    Left = 270
    Top = 21
    Width = 243
    Height = 134
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 2
    OnClick = lbDataTypeClick
  end
  object lbLevelValue: TevDBGrid
    Left = 21
    Top = 177
    Width = 243
    Height = 134
    DisableThemesInTitle = False
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TGLGroup_Insert\lbLevelValue'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Color = clGray
    DataSource = dscLevelValue
    Enabled = False
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnKeyDown = lbLevelValueKeyDown
    OnMouseDown = lbLevelValueMouseDown
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object lbDataValue: TevDBGrid
    Left = 270
    Top = 177
    Width = 243
    Height = 134
    DisableThemesInTitle = False
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TGLGroup_Insert\lbDataValue'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Color = clGray
    DataSource = dscDataValue
    Enabled = False
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 3
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnKeyDown = lbLevelValueKeyDown
    OnMouseDown = lbLevelValueMouseDown
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object edtGLMask: TevEdit
    Left = 184
    Top = 335
    Width = 169
    Height = 21
    TabOrder = 4
  end
  object dscLevelValue: TevDataSource
    Left = 30
    Top = 327
  end
  object dscDataValue: TevDataSource
    Left = 426
    Top = 330
  end
end
