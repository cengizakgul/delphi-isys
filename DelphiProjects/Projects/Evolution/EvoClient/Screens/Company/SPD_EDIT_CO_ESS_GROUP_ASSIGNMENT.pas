// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_ESS_GROUP_ASSIGNMENT;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, wwdblook, EvContext,
  wwdbdatetimepicker, Wwdotdot, Wwdbcomb, StdCtrls, DBCtrls, ExtCtrls, SysUtils,
  Mask, wwdbedit, Buttons, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, EvTypes,
  ComCtrls, Controls, Classes, EvUtils, Dialogs, EvSecElement, SReportSettings,
  EvConsts, ISBasicClasses, SDDClasses, SDataDictbureau, SDataDictclient,
  SDataDicttemp, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, DBGrids, Forms, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;


type
  TEDIT_CO_ESS_GROUP_ASSIGNMENT = class(TEDIT_CO_BASE)
    tshtESSGroup: TTabSheet;
    tshtManagerAssignment: TTabSheet;
    tshtEmployeeAssignment: TTabSheet;
    dsDBDT: TevDataSource;
    wwdsDetail1: TevDataSource;
    wwdsDetail2: TevDataSource;
    csEE: TevClientDataSet;
    csEEee_nbr: TIntegerField;
    csEEfirst_name: TStringField;
    csEELast_name: TStringField;
    csManagerAvailable: TevClientDataSet;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    dsManagerAvailable: TevDataSource;
    csManagerSelected: TevClientDataSet;
    IntegerField2: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    dsManagerSelected: TevDataSource;
    csManagerSelectedSend_Email: TStringField;
    csEECURRENT_TERMINATION_CODE: TStringField;
    csManagerSelectedCO_GROUP_MANAGER_Nbr: TIntegerField;
    csManagerAvailableCurrent_Termination_Code: TStringField;
    csManagerSelectedCurrent_Termination_Code: TStringField;
    csMemberAvailable: TevClientDataSet;
    IntegerField3: TIntegerField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField8: TStringField;
    dsMemberAvailable: TevDataSource;
    csMemberSelected: TevClientDataSet;
    IntegerField4: TIntegerField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField12: TStringField;
    IntegerField5: TIntegerField;
    dsMemberSelected: TevDataSource;
    csManagerAvailableSELFSERVE_ENABLED: TStringField;
    csMemberAvailableSELFSERVE_ENABLED: TStringField;
    csManagerSelectedSELFSERVE_ENABLED: TStringField;
    csMemberSelectedSELFSERVE_ENABLED: TStringField;
    csManagerAvailablestatus: TStringField;
    csManagerAvailableAccesslevel: TStringField;
    csMemberAvailableStatus: TStringField;
    csMemberAvailableAccesslevel: TStringField;
    csManagerSelectedStatus: TStringField;
    csManagerSelectedAccessLevel: TStringField;
    csMemberSelectedAccessLevel: TStringField;
    csMemberSelectedStatus: TStringField;
    csEESELFSERVE_ENABLED: TStringField;
    csManagerlist: TevClientDataSet;
    csEEcustom_employee_number: TStringField;
    csManagerAvailablecustom_employee_number: TStringField;
    csMemberAvailablecustom_employee_number: TStringField;
    csManagerSelectedcustom_employee_number: TStringField;
    csMemberSelectedcustom_employee_number: TStringField;
    csDBDT: TevClientDataSet;
    sbGroupAssignment: TScrollBox;
    fpGroupAssignmentSummary: TisUIFashionPanel;
    evDBGrid1: TevDBGrid;
    fpGroupDetails: TisUIFashionPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    dedtGroup_Name: TevDBEdit;
    cbGroupType: TevDBComboBox;
    sbManagerAssignment: TScrollBox;
    sbAddManager: TevSpeedButton;
    sbDelManager: TevSpeedButton;
    isUIFashionPanel1: TisUIFashionPanel;
    Label3: TLabel;
    evDBText1: TevDBText;
    evLabel6: TevLabel;
    evcbDBDT: TevDBComboBox;
    grdDBDT: TevDBGrid;
    fpAvailable: TisUIFashionPanel;
    fpAssigned: TisUIFashionPanel;
    grdManagerAvailable: TevDBGrid;
    grdManagerSelected: TevDBGrid;
    edSelectedLevel: TevLabel;
    sbEmployeeAssignment: TScrollBox;
    sbAddMember: TevSpeedButton;
    sbDelMember: TevSpeedButton;
    isUIFashionPanel2: TisUIFashionPanel;
    isUIFashionPanel3: TisUIFashionPanel;
    grdMemberSelected: TevDBGrid;
    grdMemberAvailable: TevDBGrid;
    isUIFashionPanel4: TisUIFashionPanel;
    evDBText2: TevDBText;
    edSelectedLevel_Member: TevLabel;
    Label1: TLabel;
    Label2: TLabel;
    evcbDBDT_Member: TevDBComboBox;
    grdDBDT_Member: TevDBGrid;
    memManagerlist: TevMemo;
    evLabel3: TevLabel;
    tshtSecondaryManagerAssignment: TTabSheet;
    sbSecManagerAssignment: TScrollBox;
    sbAddSecondaryManager: TevSpeedButton;
    sbDelSecondaryManager: TevSpeedButton;
    isUIFashionPanel5: TisUIFashionPanel;
    Label4: TLabel;
    evDBText3: TevDBText;
    evLabel4: TevLabel;
    edSecSelectedLevel: TevLabel;
    evcbDBDTSec: TevDBComboBox;
    grdDBDTSec: TevDBGrid;
    isUIFashionPanel6: TisUIFashionPanel;
    grdSecManagerAvailable: TevDBGrid;
    isUIFashionPanel7: TisUIFashionPanel;
    grdSecondaryManagerSelected: TevDBGrid;
    csSecondaryManagerSelected: TevClientDataSet;
    IntegerField6: TIntegerField;
    StringField7: TStringField;
    StringField11: TStringField;
    StringField13: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    StringField17: TStringField;
    StringField18: TStringField;
    IntegerField7: TIntegerField;
    dsSecondaryManagerSelected: TevDataSource;
    csSecManagerAvailable: TevClientDataSet;
    IntegerField8: TIntegerField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    dsSecManagerAvailable: TevDataSource;
    procedure PageControl1Change(Sender: TObject);
    procedure sbAddManagerClick(Sender: TObject);
    procedure dsManagerSelectedDataChange(Sender: TObject; Field: TField);
    procedure sbDelManagerClick(Sender: TObject);
    procedure sbAddMemberClick(Sender: TObject);
    procedure sbDelMemberClick(Sender: TObject);
    procedure evcbDBDTChange(Sender: TObject);
    procedure evcbDBDT_MemberChange(Sender: TObject);
    procedure csDBDTAfterScroll(DataSet: TDataSet);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure sbAddSecondaryManagerClick(Sender: TObject);
    procedure sbDelSecondaryManagerClick(Sender: TObject);
    procedure evcbDBDTSecChange(Sender: TObject);
  private
    function  GetDBDTCondition(Grid: TevDBGrid; combo:TevDbComboBox) : String;
    procedure GetManagerlist (alist :tStrings);
    procedure LoadDBDT( aGrid: TevDBGrid; aCombo:TevDbComboBox; alabel : TevLabel);
    procedure LoadEEBase(cmbDBDT: TevDBComboBox; grDBDT, grAvailable, grSelected: TevDBGrid; csAvailable, csSelected: TevClientDataSet);
    procedure LoadEE;
    procedure LoadEESec;
    procedure LoadEE_member;
  protected
    function  IgnoreAutoJump: Boolean; override;
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function  GetDataSetConditions(sName: string): string; override;
    procedure AddManager(csManager, csAvailable: TevClientDataSet; grAvailable: TEvDBGrid);
    procedure DelManager(csManager, csAvailable: TevClientDataSet; grdManager: TevDBGrid);
  public
    procedure AfterClick( Kind: Integer ); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function  GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

uses
  SPackageEntry, SFrameEntry,Variants;

{$R *.DFM}

var
  CurrentFilter: string = 'STATUS like ''%ACTIVE%''';


function TEDIT_CO_ESS_GROUP_ASSIGNMENT.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_GROUP;
end;

function TEDIT_CO_ESS_GROUP_ASSIGNMENT.GetInsertControl: TWinControl;
begin
  Result := dedtGroup_Name;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.LoadDBDT( aGrid: TevDBGrid; aCombo:TevDbComboBox; alabel : TevLabel);
var
  eedbdtQuery : string;
begin
  aGrid.Visible := aCombo.ItemIndex > 0;

  case PageControl1.ActivePageIndex of
    2: begin
        case aCombo.ItemIndex of
          1: begin
               eedbdtQuery := 'SelectDIVWithName';
               aGrid.Selected.Text := 'DNAME'#9'21'#9'Division'#9'F';
             end;
          2: begin
               eedbdtQuery := 'SelectDBWithName';
               aGrid.Selected.Text := 'DNAME'#9'21'#9'Division'#9'F'
                              + #13#10 +'BNAME'#9'21'#9'Branch'#9'F';
             end;
          3:begin
              eedbdtQuery := 'SelectDBDWithName';
              aGrid.Selected.Text := 'DNAME'#9'21'#9'Division'#9'F'
                             + #13#10 +'BNAME'#9'21'#9'Branch'#9'F'
                             + #13#10 +'PNAME'#9'21'#9'Department'#9'F';
            end;
          4:begin
              eedbdtQuery := 'SelectDBDTWithName';
              aGrid.Selected.Text := 'DNAME'#9'21'#9'Division'#9'F'
                            + #13#10 +'BNAME'#9'21'#9'Branch'#9'F'
                            + #13#10 +'PNAME'#9'20'#9'Department'#9'F'
                            + #13#10 +'TNAME'#9'20'#9'Team'#9'F';
            end;
        end;
       end;
    3: begin     //employee tab needs narrower columns
        case aCombo.ItemIndex of
          1: begin
               eedbdtQuery := 'SelectDIVWithName';
               aGrid.Selected.Text := 'DNAME'#9'48'#9'Division'#9'F';
             end;
          2: begin
               eedbdtQuery := 'SelectDBWithName';
               aGrid.Selected.Text := 'DNAME'#9'23'#9'Division'#9'F'
                              + #13#10 +'BNAME'#9'24'#9'Branch'#9'F';
             end;
          3:begin
              eedbdtQuery := 'SelectDBDWithName';
              aGrid.Selected.Text := 'DNAME'#9'15'#9'Division'#9'F'
                             + #13#10 +'BNAME'#9'16'#9'Branch'#9'F'
                             + #13#10 +'PNAME'#9'16'#9'Department'#9'F';
            end;
          4:begin
              eedbdtQuery := 'SelectDBDTWithName';
              aGrid.Selected.Text := 'DNAME'#9'11'#9'Division'#9'F'
                            + #13#10 +'BNAME'#9'11'#9'Branch'#9'F'
                            + #13#10 +'PNAME'#9'12'#9'Department'#9'F'
                            + #13#10 +'TNAME'#9'12'#9'Team'#9'F';
            end;
        end;
       end;
  end;

  if aCombo.ItemIndex > 0 then
  begin
    with TExecDSWrapper.Create(eedbdtQuery) do
    begin
      SetParam('CoNbr', DM_COMPANY.CO.FieldByname('CO_NBR').asString);
      ctx_DataAccess.GetCustomData(csDBDT, AsVariant);
    end;

    if not csDBDT.IsEmpty then
    begin
      case aCombo.ItemIndex of
          1: alabel.caption :=  csDBDT.FieldByName('DNAME').asString;
          2: alabel.caption :=  csDBDT.FieldByName('DNAME').asString +  ' | ' + csDBDT.FieldByName('BNAME').asString;
          3: alabel.caption :=  csDBDT.FieldByName('DNAME').asString +  ' | ' + csDBDT.FieldByName('BNAME').asString
                                +  ' | ' + csDBDT.FieldByName('PNAME').asString;
          4: alabel.caption :=  csDBDT.FieldByName('DNAME').asString +  ' | ' + csDBDT.FieldByName('BNAME').asString
                                         +  ' | ' + csDBDT.FieldByName('PNAME').asString +  ' | ' + csDBDT.FieldByName('TNAME').asString;
      end;
    end;
  end;
  if (PageControl1.ActivePage = tshtManagerAssignment) then
    LoadEE
  else if (PageControl1.ActivePage = tshtSecondaryManagerAssignment) then
    LoadEESec
  else if PageControl1.ActivePage = tshtEmployeeAssignment then
    LoadEE_member;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.Activate;
begin
  inherited;
  csManagerSelected.UserFilter  := CurrentFilter;
  csSecondaryManagerSelected.UserFilter  := CurrentFilter;
  csManagerAvailable.UserFilter := CurrentFilter;
  csSecManagerAvailable.UserFilter := CurrentFilter;
  csMemberSelected.UserFilter   := CurrentFilter;
  csMemberAvailable.UserFilter  := CurrentFilter;

  evcbDBDT.Itemindex := -1;
  evcbDBDTSec.Itemindex := -1;
  evcbDBDT_member.Itemindex := -1;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavAbort:
      if (PageControl1.ActivePage = tshtManagerAssignment) or (PageControl1.ActivePage = tshtSecondaryManagerAssignment) then LoadEE
      else if PageControl1.ActivePage = tshtEmployeeAssignment then LoadEE_member;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_COMPANY.CO_GROUP_MANAGER, EditDataSets, '');
  AddDSWithCheck(DM_COMPANY.CO_GROUP_MEMBER, EditDataSets, '');
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  inherited;
  AddDS(DM_COMPANY.CO_GROUP_MANAGER, aDS);
  AddDS(DM_COMPANY.CO_GROUP_MEMBER, aDS);
end;

function TEDIT_CO_ESS_GROUP_ASSIGNMENT.GetDataSetConditions(
  sName: string): string;
begin
  if ( sName = 'CO_GROUP_MANAGER' )
     or ( sName = 'CO_GROUP_MEMBER' )
  then
    Result := ''
  else begin
    Result := inherited GetDataSetConditions(sName);

    if ( sName = GetDefaultDataSet.Name )
       and ( Trim(Result) <> '' ) then
       Result := Result + ' and GROUP_TYPE <> '''+ ESS_GROUPTYPE_BENEFIT +''''; //to filter out Benefits group type this is only for web
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.PageControl1Change(
  Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = tshtManagerAssignment then
  begin
    edSelectedLevel.caption := '';
    evcbDBDT.Itemindex := 0; //default - company
    evcbDBDT.OnChange(nil);
  end
  else if PageControl1.ActivePage = tshtSecondaryManagerAssignment then
  begin
    edSecSelectedLevel.caption := '';
    evcbDBDTSec.Itemindex := 0; //default - company
    evcbDBDTSec.OnChange(nil);
  end
  else if PageControl1.ActivePage = tshtEmployeeAssignment then
  begin
    GetManagerlist(memManagerlist.lines);
    edSelectedLevel_Member.caption := '';
    evcbDBDT_Member.Itemindex := 0; //default - company
    evcbDBDT_Member.OnChange(nil);
  end;
end;

function TEDIT_CO_ESS_GROUP_ASSIGNMENT.GetDBDTCondition(Grid: TevDBGrid; combo:TevDbComboBox) : String;
var
  fDiv   : TField;
  fBranch: TField;
  fDep   : TField;
  fTeam  : TField;
begin
  Result := '';
  Grid.DataSource.DataSet.DisableControls;
  try
    fBranch:= nil;
    fDep   := nil;
    fTeam  := nil;

    fDiv     := Grid.DataSource.DataSet.FieldByName('CO_DIVISION_NBR');
    if combo.ItemIndex > 1 then
      fBranch  := Grid.DataSource.DataSet.FieldByName('CO_BRANCH_NBR');
    if combo.ItemIndex > 2 then
      fDep     := Grid.DataSource.DataSet.FieldByName('CO_DEPARTMENT_NBR');
    if combo.ItemIndex > 3 then
      fTeam    := Grid.DataSource.DataSet.FieldByName('CO_TEAM_NBR');
    case combo.ItemIndex of
       1: begin
           if Trim(fDiv.AsString) <> '' then
             Result := Result + '(CO_DIVISION_NBR=' + fDiv.AsString + ' )';
          end;
       2: begin
            if Trim(fDiv.AsString) <> '' then
               Result := Result + '(CO_DIVISION_NBR=' + fDiv.AsString + ' )';
            if Trim(fBranch.AsString) <> '' then
               Result := Result + ' and (CO_BRANCH_NBR=' + fBranch.AsString + ' )';
          end;
       3: begin
            if Trim(fDiv.AsString) <> '' then
               Result := Result + '(CO_DIVISION_NBR=' + fDiv.AsString + ' )';
            if Trim(fBranch.AsString) <> '' then
               Result := Result + ' and (CO_BRANCH_NBR=' + fBranch.AsString + ' )';
            if Trim(fDep.AsString) <> '' then
               Result := Result + ' and (CO_DEPARTMENT_NBR=' + fDep.AsString + ' )';
          end;
       4: begin
            if Trim(fDiv.AsString) <> '' then
               Result := Result + '(CO_DIVISION_NBR=' + fDiv.AsString + ' )';
            if Trim(fBranch.AsString) <> '' then
               Result := Result + ' and (CO_BRANCH_NBR=' + fBranch.AsString + ' )';
            if Trim(fDep.AsString) <> '' then
               Result := Result + ' and (CO_DEPARTMENT_NBR=' + fDep.AsString + ' )';
            if Trim(fTeam.AsString) <> '' then
               Result := Result + ' and (CO_TEAM_NBR=' + fTeam.AsString + ' )';
          end;
    end;
  finally
    Grid.DataSource.DataSet.EnableControls;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.LoadEE;
begin
  LoadEEBase(evcbDBDT, grdDBDT, grdManagerAvailable, grdManagerSelected, csManagerAvailable, csManagerSelected);
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.LoadEE_Member;
var
  ConditionList : string;
begin
   ConditionList := '';
   if evcbDBDT_Member.ItemIndex > 0 then
   begin
      ConditionList:= GetDBDTCondition(grdDBDT_member,evcbDBDT_Member);
      if (Trim(ConditionList) <> '') then
        ConditionList := ' and ' + ConditionList;
   end;
   with TExecDSWrapper.Create('SelectEmployeesWithDBDT') do
   begin
     SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').asString);
     SetMacro('Condition', ConditionList);
     ctx_DataAccess.GetCustomData(csEE, AsVariant);
   end;

  if DM_COMPANY.CO_GROUP.IsEmpty then
  begin
    EvMessage('Please set up a Group at first', mtInformation, [mbOK]);
    exit;
  end;

  grdMemberAvailable.DataSource.DataSet.DisableControls;
  grdMemberSelected.DataSource.DataSet.DisableControls;
  try
    csMemberSelected.Close;
    csMemberSelected.CreateDataSet;
    csMemberAvailable.Close;
    csMemberAvailable.CreateDataSet;

    csEE.First;
    while not csEE.Eof do
    begin
      if DM_COMPANY.CO_GROUP_MEMBER.Locate('CO_Group_NBR;EE_NBR', VarArrayOf(
                             [ DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asString,
                               csEE.FieldByName('EE_NBR').asString ]), [])
      then csMemberSelected.AppendRecord([ csEE.FieldByName('EE_NBR').asString,
                                     csEE.FieldByName('custom_employee_number').asString,
                                     csEE.FieldByName('First_Name').asString,
                                     csEE.FieldByName('Last_Name').asString,
                                     ConvertCode(TEE, 'SELFSERVE_ENABLED',csEE.FieldByName('Selfserve_Enabled').asString),
                                     ConvertCode(TEE, 'CURRENT_TERMINATION_CODE',csEE.FieldByName('Current_Termination_Code').asString),
                                     csEE.FieldByName('Selfserve_Enabled').asString,
                                     csEE.FieldByName('Current_Termination_Code').asString,
                                     DM_COMPANY.CO_GROUP_MEMBER.FieldByName('Co_Group_Member_NBR').asinteger ])
      else csMemberAvailable.AppendRecord([csEE.FieldByName('EE_NBR').asString,
                                      csEE.FieldByName('custom_employee_number').asString,
                                      csEE.FieldByName('First_Name').asString,
                                      csEE.FieldByName('Last_Name').asString,
                                      ConvertCode(TEE, 'SELFSERVE_ENABLED',csEE.FieldByName('Selfserve_Enabled').asString),
                                      ConvertCode(TEE, 'CURRENT_TERMINATION_CODE',csEE.FieldByName('Current_Termination_Code').asString),
                                      csEE.FieldByName('Selfserve_Enabled').asString,
                                      csEE.FieldByName('Current_Termination_Code').asString]);
      csEE.Next;
    end;
  finally
    csMemberSelected.UserFiltered := False;
    csMemberAvailable.UserFiltered := False;

    csMemberSelected.UserFiltered := True;
    csMemberAvailable.UserFiltered := True;

    grdMemberAvailable.UnselectAll;
    grdMemberSelected.UnselectAll;
    grdMemberAvailable.DataSource.DataSet.EnableControls;
    grdMemberSelected.DataSource.DataSet.EnableControls;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.sbAddManagerClick(Sender: TObject);
begin
  inherited;
  AddManager(csManagerSelected, csManagerAvailable, grdManagerAvailable);
end;


procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.csDBDTAfterScroll(
  DataSet: TDataSet);

  procedure SetCaption(lbl: TEvLabel; cmb: TevDBComboBox);
  begin
    if cmb.ItemIndex > 0 then
    begin
      case cmb.ItemIndex of
          1: lbl.caption :=  csDBDT.FieldByName('DNAME').asString;
          2: lbl.caption :=  csDBDT.FieldByName('DNAME').asString +  ' | ' + csDBDT.FieldByName('BNAME').asString;
          3: lbl.caption :=  csDBDT.FieldByName('DNAME').asString +  ' | ' + csDBDT.FieldByName('BNAME').asString
                                         +  ' | ' + csDBDT.FieldByName('PNAME').asString;
          4: lbl.caption :=  csDBDT.FieldByName('DNAME').asString +  ' | ' + csDBDT.FieldByName('BNAME').asString
                                         +  ' | ' + csDBDT.FieldByName('PNAME').asString +  ' | ' + csDBDT.FieldByName('TNAME').asString;
      end;
    end;
  end;

begin
  inherited;
  if PageControl1.ActivePage = tshtManagerAssignment then
  begin
    edSelectedLevel.caption := '';
    if ( csDBDT.active ) and ( not csDBDT.IsEmpty ) then
      SetCaption(edSelectedLevel, evcbDBDT);
    LoadEE;
  end
  else if PageControl1.ActivePage = tshtSecondaryManagerAssignment then
  begin
    edSecSelectedLevel.Caption := '';
    if ( csDBDT.active ) and ( not csDBDT.IsEmpty ) then
      SetCaption(edSecSelectedLevel, evcbDBDTSec);
    LoadEESec;
  end
  else if PageControl1.ActivePage = tshtEmployeeAssignment then
  begin
    edSelectedLevel_Member.caption := '';
    if ( csDBDT.active )
       and ( not csDBDT.IsEmpty ) then
    begin
      SetCaption(edSelectedLevel_Member, evcbDBDT_Member);
    end;
    LoadEE_Member;
  end
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.dsManagerSelectedDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  if assigned(Field)
     and (Uppercase(Field.FieldName) = 'SEND_EMAIL')
     and DM_COMPANY.CO_GROUP_MANAGER.locate('CO_GROUP_MANAGER_Nbr',
         dsManagerSelected.DataSet.FieldByname('CO_GROUP_MANAGER_Nbr').asstring, [])
     and (Trim(DM_COMPANY.CO_GROUP_MANAGER.FieldbyName('Send_email').asstring) <> Field.Value )
  then begin
     DM_COMPANY.CO_GROUP_MANAGER.edit;
     DM_COMPANY.CO_GROUP_MANAGER.FieldbyName('Send_email').asstring :=  Field.Value;
     DM_COMPANY.CO_GROUP_MANAGER.post;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.sbDelManagerClick(Sender: TObject);
begin
  inherited;
  DelManager(csManagerSelected, csManagerAvailable, grdManagerSelected);
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.sbDelMemberClick(Sender: TObject);
var
 i : integer;
begin
  inherited;
  if DM_COMPANY.CO_GROUP.IsEmpty then
  begin
    EvMessage('Please set up a Group at first', mtInformation, [mbOK]);
    exit;
  end;

  csMemberAvailable.DisableControls;
  csMemberSelected.DisableControls;
  try
    for i := 0 to grdMemberSelected.SelectedList.Count - 1 do
    begin
      csMemberSelected.GotoBookmark(grdMemberSelected.SelectedList[i]);
      if ( DM_COMPANY.CO_GROUP_MEMBER.Locate('CO_Group_NBR;EE_NBR',
              VarArrayOf( [ DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asString,
                            csMemberSelected.FieldbyName('EE_NBR').asstring ]), [])) then
      begin
        DM_COMPANY.CO_GROUP_MEMBER.Delete;
        csMemberAvailable.AppendRecord([csMemberSelected.FieldByName('EE_NBR').asString,
                                        csMemberSelected.FieldByName('custom_employee_number').asString,
                                       csMemberSelected.FieldByName('First_Name').asString,
                                       csMemberSelected.FieldByName('Last_Name').asString,
                                       ConvertCode(TEE, 'SELFSERVE_ENABLED',csMemberSelected.FieldByName('Selfserve_Enabled').asString),
                                       ConvertCode(TEE, 'CURRENT_TERMINATION_CODE',csMemberSelected.FieldByName('Current_Termination_Code').asString),
                                       csMemberSelected.FieldByName('Selfserve_Enabled').asString,
                                       csMemberSelected.FieldByName('Current_Termination_Code').asString]);
        csMemberSelected.delete;
      end;
    end;//for
    grdMemberSelected.UnselectAll;
    if not ctx_DataAccess.DelayedCommit then
       ctx_DataAccess.PostDataSets([DM_COMPANY.CO_GROUP_Member]);
  finally
    csMemberAvailable.EnableControls;
    csMemberSelected.EnableControls;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.evcbDBDTChange(Sender: TObject);
begin
  inherited;
  LoadDBDT(grdDBDT, evcbDBDT, edSelectedLevel);
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.evcbDBDT_MemberChange(
  Sender: TObject);
begin
  inherited;
  LoadDBDT(grdDBDT_member, evcbDBDT_member, edSelectedLevel_member);
end;


procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.GetManagerlist (alist :tStrings);
begin
  with TExecDSWrapper.Create('SelectCOGroupMangerName') do
  begin
    SetParam('CoGroupNbr', DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asString);
    SetParam('CoNbr', DM_COMPANY.CO.FieldByname('CO_NBR').asString);
    ctx_DataAccess.GetCustomData(csManagerlist, AsVariant);
  end;
  alist.Text := '';
  csManagerlist.First;
  while not csManagerlist.Eof do
  begin
    alist.Add(csManagerlist.FieldByName('Name').asString);
    csManagerlist.next;
  end;
  if alist.count > 0 then
     alist.Strings[0] := alist.Strings[0];
end;

function TEDIT_CO_ESS_GROUP_ASSIGNMENT.IgnoreAutoJump: Boolean;
begin
  Result := false;
  if wwdsList.DataSet.RecordCount <= 0 then exit;

  if DM_COMPANY.CO.Active then
  begin
     tshtESSGroup.TabVisible := Context.License.SelfServe
          and (not DM_COMPANY.CO.FieldByName('ENABLE_ESS').isNull)
          and (DM_COMPANY.CO.FieldByName('ENABLE_ESS').asString <> GROUP_BOX_NO);

     tshtManagerAssignment.TabVisible := tshtESSGroup.TabVisible;
     tshtSecondaryManagerAssignment.TabVisible := tshtESSGroup.TabVisible;
     tshtEmployeeAssignment.TabVisible := tshtESSGroup.TabVisible;
     Result :=  not tshtESSGroup.TabVisible;
     if Result then
     begin
       EvMessage(DM_COMPANY.CO.FieldByName('Custom_Company_Number').asString + ', '+
                 DM_COMPANY.CO.FieldByName('Name').asString + ': This Company does not have Employee Self Serve Enabled.')
     end;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.wwdsListDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Assigned(tshtESSGroup) and
     Assigned(tshtManagerAssignment) and
     Assigned(tshtSecondaryManagerAssignment) and
     Assigned(tshtEmployeeAssignment) then
  begin
     tshtESSGroup.TabVisible := not BitBtn1.Enabled;
     tshtManagerAssignment.TabVisible := not BitBtn1.Enabled;
     tshtSecondaryManagerAssignment.TabVisible := not BitBtn1.Enabled;
     tshtEmployeeAssignment.TabVisible := not BitBtn1.Enabled;
     if not BitBtn1.Enabled then // already open Company
     begin
        tshtESSGroup.TabVisible := Context.License.SelfServe
            and (not DM_COMPANY.CO.FieldByName('ENABLE_ESS').isNull)
            and (DM_COMPANY.CO.FieldByName('ENABLE_ESS').asString <> GROUP_BOX_NO);

        tshtManagerAssignment.TabVisible := tshtESSGroup.TabVisible;
        tshtSecondaryManagerAssignment.TabVisible := tshtESSGroup.TabVisible;
        tshtEmployeeAssignment.TabVisible := tshtESSGroup.TabVisible;
     end;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.wwdsDetailDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
 if cbGroupType.Items.IndexOf('Benefit'#9'B') >= 0 then
    cbGroupType.Items.Delete(cbGroupType.Items.IndexOf('Benefit'#9'B'));
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.sbAddSecondaryManagerClick(
  Sender: TObject);
begin
  inherited;
  AddManager(csSecondaryManagerSelected, csSecManagerAvailable, grdSecManagerAvailable);
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.AddManager(csManager, csAvailable: TevClientDataSet; grAvailable: TEvDBGrid);
var
  i : integer;
begin
  if DM_COMPANY.CO_GROUP.IsEmpty then
  begin
    EvMessage('Please set up a Group at first', mtInformation, [mbOK]);
    exit;
  end;

  csAvailable.DisableControls;
  csManager.DisableControls;
  try
    for i := 0 to grAvailable.SelectedList.Count - 1 do
    begin
      csAvailable.GotoBookmark(grAvailable.SelectedList[i]);
      if (not DM_COMPANY.CO_GROUP_MANAGER.Locate('CO_Group_NBR;EE_NBR',
              VarArrayOf( [ DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asString,
                            csAvailable.FieldbyName('EE_NBR').asstring ]), [])) then
      begin
        DM_COMPANY.CO_GROUP_MANAGER.Append;
        DM_COMPANY.CO_GROUP_MANAGER.FieldbyName('CO_Group_NBR').asInteger :=  DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asInteger;
        DM_COMPANY.CO_GROUP_MANAGER.FieldbyName('EE_NBR').asInteger :=  csAvailable.FieldbyName('EE_NBR').asInteger;
        if csManager = csManagerSelected then
          DM_COMPANY.CO_GROUP_MANAGER.FieldbyName('SECONDARY').AsString := 'N'
        else
          DM_COMPANY.CO_GROUP_MANAGER.FieldbyName('SECONDARY').AsString := 'Y';
        DM_COMPANY.CO_GROUP_MANAGER.Post;

        if ( DM_COMPANY.CO_GROUP_MANAGER.Locate('CO_Group_NBR;EE_NBR',
            VarArrayOf( [ DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asString,
                          csAvailable.FieldbyName('EE_NBR').asstring ]), [])) then
        begin
           csManager.AppendRecord([ csAvailable.FieldbyName('EE_NBR').asstring ,
                                    csAvailable.FieldByName('custom_employee_number').asString,
                                    csAvailable.FieldbyName('First_Name').asString,
                                    csAvailable.FieldbyName('Last_Name').asString,
                                    DM_COMPANY.CO_GROUP_MANAGER.FieldbyName('Send_Email').asstring,
                                    ConvertCode(TEE, 'SELFSERVE_ENABLED',csAvailable.FieldByName('Selfserve_Enabled').asString),
                                    ConvertCode(TEE, 'CURRENT_TERMINATION_CODE',csAvailable.FieldByName('Current_Termination_Code').asString),
                                    csAvailable.FieldByName('Selfserve_Enabled').asString,
                                    csAvailable.FieldByName('Current_Termination_Code').asString,
                                    DM_COMPANY.CO_GROUP_MANAGER.FieldbyName('Co_Group_Manager_NBR').asInteger ]);
           csAvailable.Delete;
        end;
      end;
    end;//for
    grAvailable.UnselectAll;
    if not ctx_DataAccess.DelayedCommit then
       ctx_DataAccess.PostDataSets([DM_COMPANY.CO_GROUP_MANAGER]);
  finally
    csAvailable.EnableControls;
    csManager.EnableControls;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.DelManager(
  csManager, csAvailable: TevClientDataSet; grdManager: TevDBGrid);
var
 i : integer;
begin
  inherited;
  if DM_COMPANY.CO_GROUP.IsEmpty then
  begin
    EvMessage('Please set up a Group at first', mtInformation, [mbOK]);
    exit;
  end;

  csAvailable.DisableControls;
  csManager.DisableControls;
  try
    for i := 0 to grdManager.SelectedList.Count - 1 do
    begin
      csManager.GotoBookmark(grdManager.SelectedList[i]);
      if ( DM_COMPANY.CO_GROUP_MANAGER.Locate('CO_Group_NBR;EE_NBR',
              VarArrayOf( [ DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asString,
                            csManager.FieldbyName('EE_NBR').asstring ]), [])) then
      begin
        DM_COMPANY.CO_GROUP_MANAGER.Delete;
        csAvailable.AppendRecord([csManager.FieldByName('EE_NBR').asString,
                                       csManager.FieldByName('custom_employee_number').asString,
                                       csManager.FieldByName('First_Name').asString,
                                       csManager.FieldByName('Last_Name').asString,
                                       ConvertCode(TEE, 'SELFSERVE_ENABLED',csManager.FieldByName('Selfserve_Enabled').asString),
                                       ConvertCode(TEE, 'CURRENT_TERMINATION_CODE',csManager.FieldByName('Current_Termination_Code').asString),
                                       csManager.FieldByName('Selfserve_Enabled').asString,
                                       csManager.FieldByName('Current_Termination_Code').asString]);
        csManager.delete;
      end;
    end;//for
    if not ctx_DataAccess.DelayedCommit then
       ctx_DataAccess.PostDataSets([DM_COMPANY.CO_GROUP_MANAGER]);
    grdManager.UnselectAll;
  finally
    csAvailable.EnableControls;
    csManager.EnableControls;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.sbDelSecondaryManagerClick(
  Sender: TObject);
begin
  DelManager(csSecondaryManagerSelected, csSecManagerAvailable, grdSecondaryManagerSelected);
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.sbAddMemberClick(Sender: TObject);
var
 i : integer;
begin
  inherited;
  if DM_COMPANY.CO_GROUP.IsEmpty then
  begin
    EvMessage('Please set up a Group at first', mtInformation, [mbOK]);
    exit;
  end;

  csMemberAvailable.DisableControls;
  csMemberSelected.DisableControls;
  try
    for i := 0 to grdMemberAvailable.SelectedList.Count - 1 do
    begin
      csMemberAvailable.GotoBookmark(grdMemberAvailable.SelectedList[i]);
      if (not DM_COMPANY.CO_GROUP_Member.Locate('CO_Group_NBR;EE_NBR',
              VarArrayOf( [ DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asString,
                            csMemberAvailable.FieldbyName('EE_NBR').asstring ]), [])) then
      begin
        DM_COMPANY.CO_GROUP_MEMBER.Append;
        DM_COMPANY.CO_GROUP_MEMBER.FieldbyName('CO_Group_NBR').asInteger :=  DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asInteger;
        DM_COMPANY.CO_GROUP_MEMBER.FieldbyName('EE_NBR').asInteger :=  csMemberAvailable.FieldbyName('EE_NBR').asInteger;
        DM_COMPANY.CO_GROUP_MEMBER.Post;

        if ( DM_COMPANY.CO_GROUP_Member.Locate('CO_Group_NBR;EE_NBR',
            VarArrayOf( [ DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asString,
                          csMemberAvailable.FieldbyName('EE_NBR').asstring ]), [])) then
        begin
           csMemberSelected.AppendRecord([csMemberAvailable.FieldbyName('EE_NBR').asstring,
                                          csMemberAvailable.FieldByName('custom_employee_number').asString,
                                          csMemberAvailable.FieldbyName('First_Name').asString,
                                          csMemberAvailable.FieldbyName('Last_Name').asString,
                                          ConvertCode(TEE, 'SELFSERVE_ENABLED',csMemberAvailable.FieldByName('Selfserve_Enabled').asString),
                                          ConvertCode(TEE, 'CURRENT_TERMINATION_CODE',csMemberAvailable.FieldByName('Current_Termination_Code').asString),
                                          csMemberAvailable.FieldByName('Selfserve_Enabled').asString,
                                          csMemberAvailable.FieldByName('Current_Termination_Code').asString,
                                          DM_COMPANY.CO_GROUP_MEMBER.FieldByName('Co_Group_Member_NBR').asinteger ]);
           csMemberAvailable.Delete;
        end;
      end;
    end;//for
    grdMemberAvailable.UnselectAll;
    if not ctx_DataAccess.DelayedCommit then
       ctx_DataAccess.PostDataSets([DM_COMPANY.CO_GROUP_Member]);
  finally
    csMemberAvailable.EnableControls;
    csMemberSelected.EnableControls;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.LoadEESec;
begin
  LoadEEBase(evcbDBDTSec, grdDBDTSec, grdSecManagerAvailable, grdSecondaryManagerSelected, csSecManagerAvailable, csSecondaryManagerSelected);
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.LoadEEBase(cmbDBDT: TevDBComboBox; grDBDT, grAvailable, grSelected: TevDBGrid;
  csAvailable, csSelected: TevClientDataSet);
var
  ConditionList : string;
  SecondaryFlag: string;
begin
  if csSelected = csManagerSelected then
    SecondaryFlag := 'N'
  else
    SecondaryFlag := 'Y';

  ConditionList := '';
  if cmbDBDT.ItemIndex > 0 then
  begin
    ConditionList:= GetDBDTCondition(grDBDT, cmbDBDT);
    if (Trim(ConditionList) <> '') then
      ConditionList := ' and ' + ConditionList;
  end;
  with TExecDSWrapper.Create('SelectEmployeesWithDBDT') do
  begin
    SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').asString);
    SetMacro('Condition', ConditionList);
    ctx_DataAccess.GetCustomData(csEE, AsVariant);
  end;

  if DM_COMPANY.CO_GROUP.IsEmpty then
  begin
    EvMessage('Please set up a Group at first', mtInformation, [mbOK]);
    exit;
  end;

  grAvailable.DataSource.DataSet.DisableControls;
  grSelected.DataSource.DataSet.DisableControls;
  try
    csSelected.Close;
    csSelected.CreateDataSet;
    csAvailable.Close;
    csAvailable.CreateDataSet;

    csEE.First;
    while not csEE.Eof do
    begin
      if DM_COMPANY.CO_GROUP_MANAGER.Locate('CO_Group_NBR;EE_NBR', VarArrayOf(
                             [ DM_COMPANY.CO_GROUP.FieldByName('Co_Group_NBR').asString,
                               csEE.FieldByName('EE_NBR').asString ]), [])
      then begin
        if DM_COMPANY.CO_GROUP_MANAGER.SECONDARY.Value = SecondaryFlag then
          csSelected.AppendRecord([ csEE.FieldByName('EE_NBR').asString,
                                           csEE.FieldByName('custom_employee_number').asString,
                                           csEE.FieldByName('First_Name').asString,
                                           csEE.FieldByName('Last_Name').asString,
                                           DM_COMPANY.CO_GROUP_MANAGER.FieldByName('SEND_EMAIL').asstring,
                                           ConvertCode(TEE, 'SELFSERVE_ENABLED',csEE.FieldByName('Selfserve_Enabled').asString),
                                           ConvertCode(TEE, 'CURRENT_TERMINATION_CODE',csEE.FieldByName('Current_Termination_Code').asString),
                                           csEE.FieldByName('Selfserve_Enabled').asString,
                                           csEE.FieldByName('Current_Termination_Code').asString,
                                           DM_COMPANY.CO_GROUP_MANAGER.FieldByName('Co_Group_Manager_NBR').asinteger ])
      end
      else
        csAvailable.AppendRecord([csEE.FieldByName('EE_NBR').asString,
                                          csEE.FieldByName('custom_employee_number').asString,
                                          csEE.FieldByName('First_Name').asString,
                                          csEE.FieldByName('Last_Name').asString,
                                          ConvertCode(TEE, 'SELFSERVE_ENABLED',csEE.FieldByName('Selfserve_Enabled').asString),
                                          ConvertCode(TEE, 'CURRENT_TERMINATION_CODE',csEE.FieldByName('Current_Termination_Code').asString),
                                          csEE.FieldByName('Selfserve_Enabled').asString,
                                          csEE.FieldByName('Current_Termination_Code').asString]);
      csEE.Next;
    end;
  finally
    csSelected.UserFiltered := False;
    csAvailable.UserFiltered := False;
    csSelected.UserFiltered := True;
    csAvailable.UserFiltered := True;

    grAvailable.UnselectAll;
    grSelected.UnselectAll;
    grAvailable.DataSource.DataSet.EnableControls;
    grSelected.DataSource.DataSet.EnableControls;
  end;
end;

procedure TEDIT_CO_ESS_GROUP_ASSIGNMENT.evcbDBDTSecChange(Sender: TObject);
begin
  inherited;
  LoadDBDT(grdDBDTSec, evcbDBDTSec, edSecSelectedLevel);
end;

initialization
  RegisterClass(TEDIT_CO_ESS_GROUP_ASSIGNMENT);
end.
