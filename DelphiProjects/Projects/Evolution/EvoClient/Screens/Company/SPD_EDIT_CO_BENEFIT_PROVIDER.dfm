inherited EDIT_CO_BENEFIT_PROVIDER: TEDIT_CO_BENEFIT_PROVIDER
  Width = 637
  Height = 472
  inherited Panel1: TevPanel
    Width = 637
    inherited pnlFavoriteReport: TevPanel
      Left = 485
    end
    inherited pnlTopLeft: TevPanel
      Width = 485
    end
  end
  inherited PageControl1: TevPageControl
    Width = 637
    Height = 418
    HelpContext = 29501
    ActivePage = tshtPensions
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 629
        Height = 389
        inherited pnlBorder: TevPanel
          Width = 625
          Height = 385
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 625
          Height = 385
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Pensions'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 371
              Top = 59
              Width = 379
              Height = 520
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 577
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 577
            Height = 278
            inherited Splitter1: TevSplitter
              Left = 320
              Height = 274
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Height = 274
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 194
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'13'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
                  'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
                  'FEIN'#9'9'#9'EIN'#9
                  'DBA'#9'40'#9'DBA '#9'F'
                  'UserFirstName'#9'20'#9'CSR First Name'#9'F'
                  'UserLastName'#9'30'#9'CSR Last Name'#9'F'
                  'HomeState'#9'2'#9'Home State'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_PROVIDER\wwdbgridSelectClient'
                IniAttributes.CheckNewFields = False
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 250
              Height = 274
              Align = alClient
              Visible = True
              Title = 'Providers'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 200
                Height = 194
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'AMOUNT_TYPE;CustomEdit;cbDiscountType;F')
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F'
                  'WEBSITE'#9'80'#9'Website'#9'F'
                  'DESCRIPTION'#9'10'#9'Text'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SaveToRegistry = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_PROVIDER\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtPensions: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbPensions: TScrollBox
        Left = 0
        Top = 0
        Width = 629
        Height = 389
        VertScrollBar.Position = 193
        Align = alClient
        TabOrder = 0
        object fpCOPensionDetails: TisUIFashionPanel
          Left = 8
          Top = 192
          Width = 441
          Height = 193
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Provider Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblName: TevLabel
            Left = 12
            Top = 35
            Width = 48
            Height = 16
            Caption = '~Provider'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblWebsite: TevLabel
            Left = 145
            Top = 35
            Width = 39
            Height = 13
            Caption = 'Website'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblAgency: TevLabel
            Left = 299
            Top = 35
            Width = 36
            Height = 13
            Caption = 'Agency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 75
            Width = 122
            Height = 13
            Caption = 'Enter Text About Provider'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edName: TevDBEdit
            Left = 12
            Top = 50
            Width = 125
            Height = 21
            DataField = 'NAME'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edWebsite: TevDBEdit
            Left = 145
            Top = 50
            Width = 146
            Height = 21
            DataField = 'WEBSITE'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            UsePictureMask = False
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object lcAgency: TevDBLookupCombo
            Left = 299
            Top = 50
            Width = 121
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'Agency_Name'#9'40'#9'Agency_Name'#9#9)
            DataField = 'CL_AGENCY_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_AGENCY.CL_AGENCY
            LookupField = 'CL_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object memoText: TEvDBMemo
            Left = 12
            Top = 90
            Width = 405
            Height = 81
            DataField = 'DESCRIPTION'
            DataSource = wwdsDetail
            TabOrder = 3
          end
        end
        object fpCOPensionSummary: TisUIFashionPanel
          Left = 8
          Top = -185
          Width = 441
          Height = 369
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgPension: TevDBGrid
            Left = 12
            Top = 36
            Width = 409
            Height = 313
            TabStop = False
            DisableThemesInTitle = False
            ControlType.Strings = (
              'AMOUNT_TYPE;CustomEdit;cbType;F'
              'CL_AGENCY_NBR;CustomEdit;lcAgencyGrid;F')
            Selected.Strings = (
              'NAME'#9'31'#9'Provider'#9'F'
              'WEBSITE'#9'30'#9'Website'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_PROVIDER\wwdgPension'
            IniAttributes.Delimiter = ';;'
            IniAttributes.CheckNewFields = False
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_BENEFIT_PROVIDERS.CO_BENEFIT_PROVIDERS
  end
  inherited PageControlImages: TevImageList
    Top = 88
  end
  inherited dsCL: TevDataSource
    Left = 264
  end
end
