// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_SALESPERSON;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, EvContext,
  wwdbedit, wwdbdatetimepicker, Mask, Wwdotdot, Wwdbcomb, wwdblook, EvConsts,
  SPD_MiniNavigationFrame, EvSecElement, SSecurityInterface, SFrameEntry,
  ISBasicClasses, SDDClasses, SDataDictbureau, SDataDictclient,
  SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBLookupCombo, isUIwwDBEdit,
  isUIwwDBDateTimePicker, isUIDBMemo, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, Wwdbspin;

type
  TEDIT_CO_SALESPERSON = class(TEDIT_CO_BASE)
    tshtDetails: TTabSheet;
    pnlFlatAmount: TevPanel;
    wwdsSubMaster3: TevDataSource;
    wwdsSubMaster4: TevDataSource;
    DBMemo1: TEvDBMemo;
    evPanel3: TevPanel;
    evPanel4: TevPanel;
    lablAmount: TevLabel;
    wwdeFlat_Amount: TevDBEdit;
    lablDate: TevLabel;
    wwdpFlat_Amount_Date: TevDBDateTimePicker;
    wwdgFlatAmount: TevDBGrid;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evSimpleSecElement1: TevSimpleSecElement;
    dedtReferred_By: TevDBEdit;
    lablReferred_By: TevLabel;
    wwlcSB_Referrals_Nbr: TevDBLookupCombo;
    lablSB_Referrals_Nbr: TevLabel;
    Label26: TevLabel;
    evPanel6: TevPanel;
    MiniNavigationFrame: TMiniNavigationFrame;
    pnlSalesRightBorder: TevPanel;
    fpSalesRight: TisUIFashionPanel;
    sbCompanySales: TScrollBox;
    fpSummary: TisUIFashionPanel;
    wwDBGrid1: TevDBGrid;
    fpDetail: TisUIFashionPanel;
    Label16: TevLabel;
    Label23: TevLabel;
    dblkupSALESPERSON_NAME: TevDBLookupCombo;
    wwDBComboBox1: TevDBComboBox;
    Label22: TevLabel;
    Label24: TevLabel;
    wwdpLast_Visit_Date: TevDBDateTimePicker;
    wwdeCharge_Back: TevDBEdit;
    Label25: TevLabel;
    Label21: TevLabel;
    Label20: TevLabel;
    Label19: TevLabel;
    wwdeProjected_Annual_Commission: TevDBEdit;
    wwdpNext_Comm_Date: TevDBDateTimePicker;
    wwdeNext_Commission_Percent: TevDBEdit;
    wwdeCommision_Percent: TevDBEdit;
    evDBGrid1: TevDBGrid;
    cbFlatFreq: TevDBComboBox;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    edEndDate: TevDBDateTimePicker;
    evLabel3: TevLabel;
    cbWeekNumber: TevDBComboBox;
    edMonthNumber: TevDBSpinEdit;
    evLabel4: TevLabel;
    cbFrequency: TevDBComboBox;
    procedure dblkupSALESPERSON_NAMEBeforeDropDown(Sender: TObject);
    procedure dblkupSALESPERSON_NAMECloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure fpSalesRightResize(Sender: TObject);
    procedure MiniNavigationFrameSpeedButton1Click(Sender: TObject);
    procedure MiniNavigationFrameSpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure OnGetSecurityStates(const States: TSecurityStateList); override;
    procedure SetReadOnly(Value: Boolean); Override;
  public
    function GetInsertControl: TWinControl; override;

  public
    { Public declarations }
    procedure Activate; override;
    procedure DeActivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    constructor Create(AOwner: TComponent); override;

  end;

implementation

{$R *.DFM}

const
  ctEnabledSRo = 'S';

{ TEDIT_CO_SALESPERSON }

procedure TEDIT_CO_SALESPERSON.Activate;
begin
  inherited;
  MiniNavigationFrame.DataSource := wwdgFlatAmount.DataSource;
  MiniNavigationFrame.InsertFocusControl := wwdeFlat_Amount;

end;

function TEDIT_CO_SALESPERSON.GetDataSetConditions(sName: string): string;
begin
  if sName = 'CO_SALESPERSON_FLAT_AMT' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);

end;

procedure TEDIT_CO_SALESPERSON.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS( DM_COMPANY.CO_SALESPERSON_FLAT_AMT, aDS );
end;

function TEDIT_CO_SALESPERSON.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_SALESPERSON;
end;

function TEDIT_CO_SALESPERSON.GetInsertControl: TWinControl;
begin
  Result := dblkupSALESPERSON_NAME;
end;

procedure TEDIT_CO_SALESPERSON.OnGetSecurityStates(
  const States: TSecurityStateList);
const
  secScreenSmallRo: TAtomContextRecord = (Tag: ctEnabledSRo; Caption: 'Read only with basic fields only'; IconName: ''; Priority: 100);
begin
  with secScreenSmallRo do
    States.InsertAfter(ctReadOnly, Tag[1], Caption, IconName);
end;

procedure TEDIT_CO_SALESPERSON.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO, EditDataSets);  //RE 23555
  AddDSWithCheck(DM_COMPANY.CO_SALESPERSON_FLAT_AMT, EditDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_USER, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_REFERRALS, SupportDataSets, '');

end;

procedure TEDIT_CO_SALESPERSON.SetReadOnly(Value: Boolean);
begin
  inherited;
  if FindSecurityElement(Self, ClassName).SecurityState = ctEnabledSRo then
  begin
    SetControlsReccuring(fpDetail, 'Visible', Integer(False));
    SetControlsReccuring(pnlFlatAmount, 'Visible', Integer(False));
    fpDetail.Visible := True;
    DBMemo1.Visible := True;
    Label26.Visible := True;
    Label16.Visible := True;
    dblkupSALESPERSON_NAME.Visible := True;
    Label23.Visible := True;
    wwDBComboBox1.Visible := True;
  end;
end;

procedure TEDIT_CO_SALESPERSON.dblkupSALESPERSON_NAMEBeforeDropDown(
  Sender: TObject);
begin
  inherited;
  DM_SERVICE_BUREAU.SB_USER.Filter := '(USER_FUNCTIONS like ''%-1%'' or USER_FUNCTIONS = ''-1'')';
  DM_SERVICE_BUREAU.SB_USER.Filtered := True;
end;

procedure TEDIT_CO_SALESPERSON.dblkupSALESPERSON_NAMECloseUp(
  Sender: TObject; LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  DM_SERVICE_BUREAU.SB_USER.Filtered := False;
  DM_SERVICE_BUREAU.SB_USER.Filter := '';
end;

procedure TEDIT_CO_SALESPERSON.DeActivate;
begin
  inherited;
  DM_SERVICE_BUREAU.SB_USER.Filtered := False;
  DM_SERVICE_BUREAU.SB_USER.Filter := '';
end;

constructor TEDIT_CO_SALESPERSON.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 REASSIGNMENTS
  pnlSubbrowse.Parent := fpSalesRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_SALESPERSON.fpSalesRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpSalesRight.Width - 40;
  pnlSubbrowse.Height := fpSalesRight.Height - 65;
end;

procedure TEDIT_CO_SALESPERSON.MiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  MiniNavigationFrame.InsertRecordExecute(Sender);

end;

procedure TEDIT_CO_SALESPERSON.MiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  MiniNavigationFrame.DeleteRecordExecute(Sender);

end;

initialization
  RegisterClass( TEDIT_CO_SALESPERSON );

end.
