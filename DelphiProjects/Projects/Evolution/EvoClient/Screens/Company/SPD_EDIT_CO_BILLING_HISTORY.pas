// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BILLING_HISTORY;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, wwdblook, EvContext,
  wwdbdatetimepicker, Wwdotdot, Wwdbcomb, StdCtrls, DBCtrls, ExtCtrls, SysUtils,
  Mask, wwdbedit, Buttons, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, EvTypes,
  ComCtrls, Controls, Classes, EvUtils, Dialogs, EvSecElement, SReportSettings,
  EvConsts, ISBasicClasses, SDDClasses, SDataDictbureau, SDataDictclient,
  SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBDateTimePicker, isUIwwDBComboBox,
  isUIDBMemo, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_BILLING_HISTORY = class(TEDIT_CO_BASE)
    tshtBillingHistory: TTabSheet;
    wwDBGrid2x: TevDBGrid;
    TabSheet2: TTabSheet;
    wwDBGrid1: TevDBGrid;
    ServicesDataSource: TevDataSource;
    NameLookupCombo: TevDBLookupCombo;
    DM_PAYROLL: TDM_PAYROLL;
    SbNameLookup: TevDBLookupCombo;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpCOBillingHistSummary: TisUIFashionPanel;
    fpCOBillingHistDetails: TisUIFashionPanel;
    fpCOBillingHistServices: TisUIFashionPanel;
    dedtInvoice_Number: TevDBEdit;
    lablInvoice_Number: TevLabel;
    lablInvoice_Date: TevLabel;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    dedtPR_Run: TevDBLookupCombo;
    lablPR_Run: TevLabel;
    wwDBDateTimePicker2: TevDBDateTimePicker;
    lablCheck_Date: TevLabel;
    dedtCheck_Serial_Number: TevDBEdit;
    lablCheck_Serial_Number: TevLabel;
    lablSales_Tax_Amount: TevLabel;
    dedtSales_Tax_Amount: TevDBEdit;
    wwcbStatus: TevDBComboBox;
    lablStatus: TevLabel;
    dedtBank_Account: TevDBLookupCombo;
    lablBank_Account: TevLabel;
    drgpExported_A_R: TevDBRadioGroup;
    lablNotes: TevLabel;
    dmemNotes: TEvDBMemo;
    SpeedButton1: TevSpeedButton;
    evBitBtn1: TevBitBtn;
    sbInvoices: TScrollBox;
    sbServices: TScrollBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

uses
  SPackageEntry, SFrameEntry;

{$R *.DFM}

procedure TEDIT_CO_BILLING_HISTORY.Activate;
var
  I: Integer;
begin
  with wwdsDetail.DataSet As TevClientDataSet do
  begin

    I := IndexDefs.IndexOf('SORT');
    if I <> -1 then
      DeleteIndex('SORT');

    AddIndex('SORT', 'INVOICE_NUMBER', [ixDescending]);
    IndexName := 'SORT';
    IndexDefs.Update;
    First;
  end;
  inherited;
end;

function TEDIT_CO_BILLING_HISTORY.GetDataSetConditions(
  sName: string): string;
begin
  if sName = 'CO_BILLING_HISTORY_DETAIL' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_BILLING_HISTORY.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_PAYROLL.PR, aDS);
  AddDS(DM_COMPANY.CO_SERVICES, aDS);
  AddDS(DM_COMPANY.CO_BILLING_HISTORY_DETAIL, aDS);
  inherited;
end;

function TEDIT_CO_BILLING_HISTORY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BILLING_HISTORY;
end;

function TEDIT_CO_BILLING_HISTORY.GetInsertControl: TWinControl;
begin
  Result := dedtINVOICE_NUMBER;
end;

procedure TEDIT_CO_BILLING_HISTORY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_SERVICES, SupportDataSets);
  AddDS(DM_SERVICE_BUREAU.SB, SupportDataSets);
  AddDS(DM_SERVICE_BUREAU.SB_SERVICES, SupportDataSets);
  AddDS(DM_COMPANY.CO_BILLING_HISTORY_DETAIL, EditDataSets);
end;

procedure TEDIT_CO_BILLING_HISTORY.SpeedButton1Click(Sender: TObject);
var
  CO_BILLING_HISTORY_NBR: integer;
  PrintNote: Boolean;
  ReportParams: TrwReportParams;
  v: Variant;
  CO_INVOICE_MASTER, CO_INVOICE_DETAIL: TevClientDataSet;
begin
  inherited;
  PrintNote := (not DM_COMPANY.CO_BILLING_HISTORY.FieldByName('NOTES').IsNull or not DM_SERVICE_BUREAU.SB.FieldByName('INVOICE_NOTES').IsNull) and
    (EvMessage('Show notes in invoice?', mtConfirmation, [mbYes, mbNo]) = mrYes);
  TevClientDataSet(wwdsDetail.DataSet).DisableControls;
  CO_BILLING_HISTORY_NBR := wwdsDetail.DataSet.FieldByName('CO_BILLING_HISTORY_NBR').AsInteger;
  try
    v := ctx_PayrollProcessing{(NotQueued)}.ProcessBilling(0{wwdsDetail.DataSet.FieldByName('PR_NBR').AsInteger}, False,
              wwdsDetail.DataSet.FieldByName('INVOICE_NUMBER').AsInteger, True);

    ReportParams := TrwReportParams.Create;
    CO_INVOICE_MASTER := TevClientDataSet.Create(nil);
    CO_INVOICE_DETAIL := TevClientDataSet.Create(nil);
    try
      CO_INVOICE_MASTER.Data := v[0];
      CO_INVOICE_DETAIL.Data := v[1];

      ReportParams.Add('DataSets', DataSetsToVarArray([CO_INVOICE_MASTER, CO_INVOICE_DETAIL]), False);
      ReportParams.Add('PrintNote',  IntToStr(Integer(PrintNote)), False);

      ctx_RWLocalEngine.StartGroup;
      try
        ctx_RWLocalEngine{(NotQueued)}.CalcPrintReport('Invoice', ReportParams);
      finally
        ctx_RWLocalEngine.EndGroup(rdtPreview);
      end;

    finally
      ReportParams.Free;
      CO_INVOICE_MASTER.Free;
      CO_INVOICE_DETAIL.Free;
    end;
  finally
    DM_COMPANY.CO.DataRequired('CO_NBR = ' + wwdsList.DataSet.FieldByName('CO_NBR').AsString);
    TevClientDataSet(wwdsDetail.DataSet).DataRequired('CO_NBR = ' + wwdsList.DataSet.FieldByName('CO_NBR').AsString);
    TevClientDataSet(wwdsDetail.DataSet).Locate('CO_BILLING_HISTORY_NBR', CO_BILLING_HISTORY_NBR, []);
    TevClientDataSet(wwdsDetail.DataSet).EnableControls;
  end;

end;

procedure TEDIT_CO_BILLING_HISTORY.evBitBtn1Click(Sender: TObject);
begin
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO_SERVICES')
end;

initialization
  RegisterClass(TEDIT_CO_BILLING_HISTORY);

end.
