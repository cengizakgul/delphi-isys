inherited EDIT_CO_BRCH_PR_BATCH_DEFLT_ED: TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED
  inherited Panel1: TevPanel
    inherited Label2: TevLabel
      Width = 34
      Caption = 'Branch'
    end
    inherited DBText2: TevDBText
      Width = 241
      DataSource = wwdsSubMaster2
    end
  end
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    inherited fpDivisionRight: TisUIFashionPanel
                      Title = 'Branch'
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Width = 403
              OnResize = pnlSubbrowseResize
              object Panel4: TevPanel
                Left = 0
                Top = 0
                Width = 403
                Height = 257
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 257
                IniAttributes.SectionName = 'TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Branch'
              object Splitter2: TevSplitter [0]
                Left = 168
                Top = 48
                Height = 257
              end
              inherited wwDBGrid1: TevDBGrid
                Width = 150
                Height = 257
                IniAttributes.SectionName = 'TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED\wwDBGrid1'
                Align = alLeft
              end
              object wwDBGrid3: TevDBGrid
                Left = 171
                Top = 48
                Width = 171
                Height = 257
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'BRANCH')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED\wwDBGrid3'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster2
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited sbDivisionDetail: TScrollBox
        inherited fpSummary: TisUIFashionPanel
          inherited wwDBGrid2: TevDBGrid
            IniAttributes.SectionName = 'TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED\wwDBGrid2'
          end
        end
        inherited fpDetail: TisUIFashionPanel
          inherited wwDBLookupCombo1: TevDBLookupCombo
            Selected.Strings = (
              'ED_Lookup'#9'20'#9'ED_Lookup'#9'F'
              'CodeDescription'#9'20'#9'CodeDescription'#9'F')
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = wwdsSubMaster2
  end
  object wwdsSubMaster2: TevDataSource
    DataSet = DM_CO_BRANCH.CO_BRANCH
    MasterDataSource = wwdsSubMaster
    Left = 48
  end
end
