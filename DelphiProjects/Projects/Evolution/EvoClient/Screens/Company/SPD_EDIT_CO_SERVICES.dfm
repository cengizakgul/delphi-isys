inherited EDIT_CO_SERVICES: TEDIT_CO_SERVICES
  Width = 382
  Height = 317
  inherited Panel1: TevPanel
    Width = 382
    inherited pnlFavoriteReport: TevPanel
      Left = 230
    end
    inherited pnlTopLeft: TevPanel
      Width = 230
    end
  end
  inherited PageControl1: TevPageControl
    Width = 382
    Height = 263
    HelpContext = 33005
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 374
        Height = 234
        inherited pnlBorder: TevPanel
          Width = 370
          Height = 230
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 370
          Height = 230
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited splEDIT_CO_BASE: TevSplitter
                    Left = 387
                    Width = 3
                    Visible = True
                  end
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 381
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 369
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 390
                    Width = 400
                    Visible = True
                    object fpEDIT_CO_SERVICES_RIGHT: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 388
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Caption = 'fpEDIT_CO_SERVICES_RIGHT'
                      Color = 14737632
                      TabOrder = 0
                      OnResize = fpEDIT_CO_SERVICES_RIGHTResize
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Services'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Width = 274
              Height = 532
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 322
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 322
            Height = 123
            inherited Splitter1: TevSplitter
              Left = 307
              Height = 119
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 307
              Height = 119
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 257
                Height = 39
                IniAttributes.SectionName = 'TEDIT_CO_SERVICES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 310
              Width = 8
              Height = 119
              Align = alClient
              Caption = ''
              Visible = True
              Title = 'Services'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 394
                Height = 39
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_SERVICES\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                PopupMenu = evPopupMenu1
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                DefaultSort = '-'
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtServicesDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbCompanyServices: TScrollBox
        Left = 0
        Top = 0
        Width = 374
        Height = 234
        Align = alClient
        TabOrder = 0
        object fpServices: TisUIFashionPanel
          Left = 8
          Top = 225
          Width = 677
          Height = 211
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Service Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablService_Offered: TevLabel
            Left = 12
            Top = 35
            Width = 83
            Height = 16
            Caption = '~Service Offered'
            FocusControl = wwlcService_Offered
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablName: TevLabel
            Left = 12
            Top = 74
            Width = 37
            Height = 16
            Caption = '~Name'
            FocusControl = dedtName
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablEffective_Start_Date: TevLabel
            Left = 385
            Top = 35
            Width = 93
            Height = 13
            Caption = 'Effective Start Date'
          end
          object lablEffective_End_Date: TevLabel
            Left = 523
            Top = 35
            Width = 90
            Height = 13
            Caption = 'Effective End Date'
          end
          object lablOverride_Flat_Fee: TevLabel
            Left = 385
            Top = 113
            Width = 81
            Height = 13
            Caption = 'Override Flat Fee'
          end
          object lablFrequency: TevLabel
            Left = 12
            Top = 113
            Width = 59
            Height = 16
            Caption = '~Frequency'
            FocusControl = wwcbFrequency
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablMonth_Number: TevLabel
            Left = 385
            Top = 74
            Width = 70
            Height = 13
            Caption = 'Month Number'
            FocusControl = wwseMonth_Number
          end
          object Label2: TevLabel
            Left = 523
            Top = 74
            Width = 69
            Height = 13
            Caption = 'Week Number'
          end
          object UpBtn: TevSpeedButton
            Left = 385
            Top = 165
            Width = 130
            Height = 22
            Caption = 'Move Up'
            Enabled = False
            HideHint = True
            AutoSize = False
            OnClick = UpBtnClick
            NumGlyphs = 2
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFDDDDDDCCCCCCCCCCCCDDDDDDFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDCC
              CCCCCCCCCCDDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF43A277008A4A008A4A43A277FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9695957C
              7B7B7C7B7B969595FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00894900E7A700E7A7008949FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B7B7BD5
              D5D5D5D5D57B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884708E2A600E0A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7A7979D1
              D1D1CECECE7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00874716E0AC00DCA2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D1
              D1D1CCCCCC7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00874623E0B200D9A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D2
              D2D1CAC9C97A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884630DFB700D5A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D2
              D2D1C6C6C67A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088453EDFBD00D2A1008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D3
              D3D3C4C3C37A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088454BE0C100CFA1008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D5
              D5D5C1C1C17A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884459E1C800CBA2008946FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D7
              D7D7BEBDBD7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFD9D9D9CCCCCC0087426AE2CC00C9A1008744CCCCCCD9D9D9FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D9D9CCCCCC787877D9
              D9D9BCBCBB787877CCCCCCD9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF329C6B00894600AE7F00C39E00C39E00AE80008946329C6BFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8F7A7979A1A0A0B7
              B6B6B7B6B6A1A0A07A79798F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF008C4A82DCCA00C1A000BE9B00BE9B00C1A083DCCA008C4AFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7D7C7CD5D5D5B6B6B5B2
              B2B2B2B2B2B6B6B5D5D5D57D7C7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF0088448EDBCB00B99D00B99D8EDBCB008844FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D5D5D5AF
              AFAFAFAFAFD5D5D5797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0087429AE0D39AE0D3008742FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF787877DB
              DADADBDADA787877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008A47008A47FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B
              7B7B7B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ParentColor = False
            ShortCut = 0
          end
          object DownBtn: TevSpeedButton
            Left = 523
            Top = 165
            Width = 130
            Height = 22
            Caption = 'Move Down'
            Enabled = False
            HideHint = True
            AutoSize = False
            OnClick = DownBtnClick
            NumGlyphs = 2
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC
              CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFCCCCCC008B4B008B4BCCCCCCFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC7D
              7C7C7D7C7CCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFCCCCCC00884800C38200C382008848CCCCCCFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC7A7979B2
              B2B2B2B2B27A7979CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCCCCC00884700BD7E00DD9F00DD9F00BD7E008847CCCCCCFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC7A7979ACACACCC
              CCCCCCCCCCACACAC7A7979CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF008C4B53D8B053EAC400D59C00D59C53EAC453D8B0008C4BFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7D7C7CCCCCCCDEDEDEC5
              C5C5C5C5C5DEDEDECCCCCC7D7C7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF3AA77500884600B97E00D39D00D39E00B97E0088473AA775FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9A9A9A797979AAA9A9C4
              C3C3C4C3C3AAA9A97A79799A9A9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0086441BD9AC00D4A1008644FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF787877CB
              CBCBC5C5C5787877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088462ADBB400D1A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979CE
              CECEC3C2C27A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088453ADBBB00CEA3008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D0
              D0D0C0C0C07A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884549DCC100CAA2008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D2
              D2D1BEBDBD7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884458DDC700C7A2008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D4
              D4D4BBBBBB7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884467E0CD00C3A1008A46FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D8
              D8D8B8B7B77B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884476E1D300BFA0008A46FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979DB
              DADAB4B4B47B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884486E4DA00BBA1008A46FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979DF
              DFDEB2B1B17B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF008A4698EAE667DCD4008A47FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B7B7BE7
              E7E6D7D6D67B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF4FB183008A47008A474FB183FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4A4A47B
              7B7B7B7B7BA4A4A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ParentColor = False
            ShortCut = 0
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 152
            Width = 48
            Height = 13
            Caption = 'E/D Code'
            FocusControl = cbEDCode
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 523
            Top = 113
            Width = 51
            Height = 16
            Caption = '~Discount'
            FocusControl = cbEDCode
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 111
            Top = 152
            Width = 52
            Height = 13
            Caption = 'E/D Group'
            FocusControl = cbEDGroups
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel5: TevLabel
            Left = 214
            Top = 152
            Width = 70
            Height = 13
            Caption = 'Workers Comp'
            FocusControl = cbWorkersComp
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object EvBevel1: TEvBevel
            Left = 346
            Top = 47
            Width = 3
            Height = 141
            Shape = bsLeftLine
          end
          object dedtName: TevDBEdit
            Left = 12
            Top = 89
            Width = 294
            Height = 21
            DataField = 'NAME'
            DataSource = wwdsDetail
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcService_Offered: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 101
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'SERVICE_NAME'#9'40'#9'SERVICE_NAME')
            DataField = 'SB_SERVICES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SB_SERVICES.SB_SERVICES
            LookupField = 'SB_SERVICES_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnCloseUp = wwlcService_OfferedCloseUp
          end
          object wwdpEffective_Start_Date: TevDBDateTimePicker
            Left = 385
            Top = 50
            Width = 130
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'EFFECTIVE_START_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 7
          end
          object wwdpEffective_End_Date: TevDBDateTimePicker
            Left = 523
            Top = 50
            Width = 130
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'EFFECTIVE_END_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 8
          end
          object wwdeOverride_Flat_Fee: TevDBEdit
            Left = 385
            Top = 128
            Width = 130
            Height = 21
            DataField = 'OVERRIDE_FLAT_FEE'
            DataSource = wwdsDetail
            TabOrder = 11
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CheckBox1: TevCheckBox
            Left = 117
            Top = 52
            Width = 82
            Height = 17
            Caption = 'Tiered billing'
            TabOrder = 1
            OnClick = CheckBox1Click
          end
          object wwcbFrequency: TevDBComboBox
            Left = 12
            Top = 128
            Width = 294
            Height = 21
            HelpContext = 23527
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            AutoSize = False
            DataField = 'FREQUENCY'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object wwseMonth_Number: TevDBSpinEdit
            Left = 385
            Top = 89
            Width = 130
            Height = 21
            HelpContext = 23528
            Increment = 1.000000000000000000
            MaxValue = 12.000000000000000000
            MinValue = 1.000000000000000000
            Value = 1.000000000000000000
            DataField = 'MONTH_NUMBER'
            DataSource = wwdsDetail
            TabOrder = 9
            UnboundDataType = wwDefault
          end
          object wwcbWeek: TevDBComboBox
            Left = 523
            Top = 89
            Width = 130
            Height = 21
            HelpContext = 23527
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            AutoSize = False
            DataField = 'WEEK_NUMBER'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 10
            UnboundDataType = wwDefault
          end
          object evBitBtn1: TevBitBtn
            Left = 201
            Top = 50
            Width = 105
            Height = 21
            Caption = 'Billing History'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 13
            OnClick = evBitBtn1Click
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
              5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
              A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
              5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
              FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
              B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
              6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
              FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
              B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
              64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
              CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
              B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
              5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
              00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
              BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
              57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
              00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
              BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
              82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
              60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
              BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
              83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
              00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
              C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
              4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
              FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
              C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
              4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
              FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
              C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
              49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
              FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
              CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
              45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
              FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
              DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
              3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
              FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
              EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
              8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
            NumGlyphs = 2
            Margin = 0
          end
          object cbEDCode: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 92
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ED_Lookup'#9'12'#9'ED_Lookup'#9'F'
              'CodeDescription'#9'40'#9'CodeDescription'#9'F')
            DataField = 'CO_E_D_CODES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_E_D_CODES.CO_E_D_CODES
            LookupField = 'CO_E_D_CODES_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnCloseUp = wwlcService_OfferedCloseUp
          end
          object evDBComboBox1: TevDBComboBox
            Left = 523
            Top = 128
            Width = 130
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'DISCOUNT_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 12
            UnboundDataType = wwDefault
            OnChange = evDBComboBox1Change
          end
          object cbEDGroups: TevDBLookupCombo
            Left = 111
            Top = 167
            Width = 93
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'Name'#9'40'#9'Name'#9'F')
            DataField = 'CL_E_D_GROUPS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnCloseUp = wwlcService_OfferedCloseUp
          end
          object cbWorkersComp: TevDBLookupCombo
            Left = 214
            Top = 167
            Width = 92
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'WORKERS_COMP_CODE'#9'8'#9'WORKERS_COMP_CODE'#9'F'
              'Description'#9'40'#9'Description'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            TabOrder = 6
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnCloseUp = wwlcService_OfferedCloseUp
          end
        end
        object fpDiscounts: TisUIFashionPanel
          Left = 209
          Top = 444
          Width = 234
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDiscounts'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Company Discounts'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel32: TevLabel
            Left = 12
            Top = 35
            Width = 139
            Height = 13
            Caption = 'Discount % (from -100 to 100)'
            FocusControl = dedtInvoiceDiscount
          end
          object evLabel33: TevLabel
            Left = 117
            Top = 74
            Width = 45
            Height = 13
            Caption = 'End Date'
          end
          object evLabel34: TevLabel
            Left = 13
            Top = 74
            Width = 48
            Height = 13
            Caption = 'Start Date'
          end
          object dedtInvoiceDiscount: TevDBEdit
            Left = 12
            Top = 50
            Width = 198
            Height = 21
            HelpContext = 10648
            DataField = 'INVOICE_DISCOUNT'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 13
            Top = 89
            Width = 95
            Height = 21
            HelpContext = 10602
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DISCOUNT_START_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 117
            Top = 89
            Width = 95
            Height = 21
            HelpContext = 10602
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DISCOUNT_END_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
        end
        object fpSalesTax: TisUIFashionPanel
          Left = 8
          Top = 444
          Width = 193
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSalesTax'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Sales Tax'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel6: TevLabel
            Left = 12
            Top = 74
            Width = 98
            Height = 13
            Caption = 'Override Percentage'
          end
          object drgpSales_Tax: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 159
            Height = 37
            HelpContext = 33005
            Caption = '~Taxable'
            Columns = 2
            DataField = 'SALES_TAX'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object evOverrideTax: TevDBEdit
            Left = 12
            Top = 89
            Width = 159
            Height = 21
            DataField = 'OVERRIDE_TAX_RATE'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 677
          Height = 209
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwDBGrid2: TevDBGrid
            Left = 12
            Top = 35
            Width = 641
            Height = 150
            DisableThemesInTitle = False
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F'
              'EFFECTIVE_START_DATE'#9'18'#9'Effective Start Date'#9'F'
              'EFFECTIVE_END_DATE'#9'18'#9'Effective End Date'#9'F'
              'OVERRIDE_FLAT_FEE'#9'10'#9'Override Flat Fee'#9'F'
              'SALES_TAX'#9'1'#9'Sales Tax'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_SERVICES\wwDBGrid2'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            DefaultSort = '-'
            NoFire = False
          end
        end
        object fpServiceDiscounts: TisUIFashionPanel
          Left = 451
          Top = 444
          Width = 234
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpServiceDiscounts'
          Color = 14737632
          TabOrder = 4
          Visible = False
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Service Discounts'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablDiscount: TevLabel
            Left = 12
            Top = 35
            Width = 27
            Height = 13
            Caption = 'Value'
          end
          object lablDiscount_Start_Date: TevLabel
            Left = 12
            Top = 74
            Width = 48
            Height = 13
            Caption = 'Start Date'
          end
          object evLabel1: TevLabel
            Left = 115
            Top = 74
            Width = 45
            Height = 13
            Caption = 'End Date'
          end
          object wwdpDiscount_Start_Date: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 95
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DISCOUNT_START_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
          object wwdeDiscount: TevDBEdit
            Left = 12
            Top = 50
            Width = 198
            Height = 21
            DataField = 'DISCOUNT'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdpDiscount_End_Date: TevDBDateTimePicker
            Left = 115
            Top = 89
            Width = 95
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DISCOUNT_END_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
        end
        object fpNotes: TisUIFashionPanel
          Left = 8
          Top = 585
          Width = 677
          Height = 128
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 5
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Notes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          DesignSize = (
            677
            128)
          object lablNature_Business: TevLabel
            Left = 12
            Top = 35
            Width = 31
            Height = 13
            Caption = 'Payroll'
          end
          object dmemNotes: TEvDBMemo
            Left = 12
            Top = 35
            Width = 641
            Height = 70
            HelpContext = 10655
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataField = 'NOTES'
            DataSource = wwdsDetail
            ScrollBars = ssVertical
            TabOrder = 0
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_SERVICES.CO_SERVICES
    OnDataChange = wwdsDataChange
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 307
    Top = 18
  end
  inherited DM_CLIENT: TDM_CLIENT
    Top = 18
  end
  inherited DM_COMPANY: TDM_COMPANY
    Top = 13
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 444
    Top = 21
  end
  object evPopupMenu1: TevPopupMenu
    Left = 440
    Top = 179
    object CopyTo1: TMenuItem
      Action = CopyService
    end
    object Delete1: TMenuItem
      Action = DeleteService
    end
  end
  object evActionList1: TevActionList
    Left = 520
    Top = 195
    object CopyService: TAction
      Caption = 'Copy To...'
      OnExecute = CopyServiceExecute
      OnUpdate = CopyServiceUpdate
    end
    object DeleteService: TAction
      Caption = 'Delete From...'
      OnExecute = DeleteServiceExecute
      OnUpdate = DeleteServiceUpdate
    end
  end
end
