// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_DBDT_LOCALS; //gdy

//////////////////////
// changes required in units that use this frame:
{

 procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  //...
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);  //gdy
  AddDS(DM_COMPANY.CO_XXX_LOCALS, aDS); //gdy
  //...

 function GetDataSetConditions(sName: string): string; override;
  //...
  if sName = 'CO_XXX_LOCALS' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
  //...

    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
      var InsertDataSets, EditDataSets: TArrayDS;
      var DeleteDataSet: TevClientDataSet; var SupportDataSets: TArrayDS); override;
 // ...
  AddDS(DM_COMPANY.CO_XXX_LOCALS, EditDataSets);

 //...
}
// the only change needed in this frame after embedding is to set
// dsLocals.DataSet to DM_COMPANY.CO_XXX_LOCALS and dsLocals.MasterDataSource

//////////////////////

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc,  Buttons, StdCtrls, wwdblook, Grids, Wwdbigrd,
  Wwdbgrid, ExtCtrls, SDataStructure, ActnList, EvUtils, ISBasicClasses,
  SDDClasses, SDataDictclient, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIFashionPanel;

type
  TDBDTLocals = class(TFrame)
    pnlTop: TevPanel;
    pnlBottom: TevPanel;
    dsCoLocals: TevDataSource;
    DM_COMPANY: TDM_COMPANY;
    dsLocals: TevDataSource;
    dsCompany: TevDataSource;
    evActionList1: TevActionList;
    DeleteLocal: TAction;
    InsertLocal: TAction;
    fpDBDTSummary: TisUIFashionPanel;
    evDBGrid1: TevDBGrid;
    isUIFashionPanel1: TisUIFashionPanel;
    evLabelLocalName: TevLabel;
    SpeedButton2: TevSpeedButton;
    SpeedButton1: TevSpeedButton;
    lkupLocalName: TevDBLookupCombo;
    procedure DeleteLocalExecute(Sender: TObject);
    procedure DeleteLocalUpdate(Sender: TObject);
    procedure InsertLocalUpdate(Sender: TObject);
    procedure InsertLocalExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TDBDTLocals.DeleteLocalExecute(Sender: TObject);
begin
  if EvMessage('Delete record.  Are you sure?', mtConfirmation, [mbYes, mbNo]) = mrYes then
    dsLocals.DataSet.Delete;
end;

procedure TDBDTLocals.DeleteLocalUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := assigned(dsLocals.DataSet) and
                                 dsLocals.DataSet.Active and
                                 (dsLocals.DataSet.State in [dsBrowse]) and
                                 (dsLocals.DataSet.RecordCount > 0);

end;

procedure TDBDTLocals.InsertLocalUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := assigned(dsLocals.DataSet) and
                                 dsLocals.DataSet.Active ;
end;

procedure TDBDTLocals.InsertLocalExecute(Sender: TObject);
begin
  dsLocals.DataSet.CheckBrowseMode;
  if dsLocals.DataSet.State in [dsBrowse] then
  begin
    lkupLocalName.SetFocus;
    dsLocals.DataSet.Insert;
  end;
end;

end.
