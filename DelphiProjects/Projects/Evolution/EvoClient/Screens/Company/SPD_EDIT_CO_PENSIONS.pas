// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_PENSIONS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBLookupCombo,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_PENSIONS = class(TEDIT_CO_BASE)
    tshtPensions: TTabSheet;
    wwdgPension: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    fpCOPensionSummary: TisUIFashionPanel;
    fpCOPensionDetails: TisUIFashionPanel;
    lablPension: TevLabel;
    wwlcPension: TevDBLookupCombo;
    sbPensions: TScrollBox;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.DFM}

function TEDIT_CO_PENSIONS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_PENSIONS;
end;

function TEDIT_CO_PENSIONS.GetInsertControl: TWinControl;
begin
  Result := wwlcPension;
end;

procedure TEDIT_CO_PENSIONS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_PENSION, aDS);
  inherited;
end;

procedure TEDIT_CO_PENSIONS.Activate;
begin
  inherited;

end;

constructor TEDIT_CO_PENSIONS.Create(AOwner: TComponent);
begin
  inherited;

end;

initialization
  RegisterClass(TEDIT_CO_PENSIONS);

end.
