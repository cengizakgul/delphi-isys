inherited EDIT_CO_BENEFIT_CATEGORY: TEDIT_CO_BENEFIT_CATEGORY
  inherited PageControl1: TevPageControl
    HelpContext = 29501
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Pensions'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 371
              Top = 59
              Width = 379
              Height = 520
              Visible = True
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 320
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 259
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'13'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
                  'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
                  'FEIN'#9'9'#9'EIN'#9
                  'DBA'#9'40'#9'DBA '#9'F'
                  'UserFirstName'#9'20'#9'CSR First Name'#9'F'
                  'UserLastName'#9'30'#9'CSR Last Name'#9'F'
                  'HomeState'#9'2'#9'Home State'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_CATEGORY\wwdbgridSelectClient'
                IniAttributes.CheckNewFields = False
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 48
              Align = alClient
              Visible = True
              Title = 'Category'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 96
                Height = 259
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'CATEGORY_TYPE;CustomEdit;cbbCategoryType;F'
                  'ENROLLMENT_FREQUENCY;CustomEdit;cbbFrequency;F')
                Selected.Strings = (
                  'CATEGORY_TYPE'#9'30'#9'Category'#9'F'
                  'ENROLLMENT_FREQUENCY'#9'1'#9'Frequency'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_CATEGORY\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object cbbCategoryType: TevDBComboBox
                Left = 232
                Top = 112
                Width = 121
                Height = 21
                ShowButton = True
                Style = csDropDownList
                MapList = False
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'CATEGORY_TYPE'
                DataSource = wwdsDetail
                DropDownCount = 8
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 1
                UnboundDataType = wwDefault
              end
              object cbbFrequency: TevDBComboBox
                Left = 232
                Top = 144
                Width = 121
                Height = 21
                ShowButton = True
                Style = csDropDownList
                MapList = False
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'ENROLLMENT_FREQUENCY'
                DataSource = wwdsDetail
                DropDownCount = 8
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 2
                UnboundDataType = wwDefault
              end
            end
          end
        end
      end
    end
    object tshtCategory: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbPensions: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 183
        Align = alClient
        TabOrder = 0
        object fpIntroText: TisUIFashionPanel
          Left = 8
          Top = 468
          Width = 588
          Height = 122
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 5
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Introduction Text'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evNote: TEvDBMemo
            Left = 12
            Top = 36
            Width = 556
            Height = 66
            Align = alClient
            DataField = 'NOTES'
            DataSource = wwdsDetail
            TabOrder = 0
          end
        end
        object fpCOCategorySummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 588
          Height = 250
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgCategory: TevDBGrid
            Left = 12
            Top = 36
            Width = 556
            Height = 194
            TabStop = False
            DisableThemesInTitle = False
            ControlType.Strings = (
              'CATEGORY_TYPE;CustomEdit;cbCategoryType;F'
              'ENROLLMENT_FREQUENCY;CustomEdit;cbFrequency;F')
            Selected.Strings = (
              'CATEGORY_TYPE'#9'41'#9'Category'#9'F'
              'ENROLLMENT_FREQUENCY'#9'15'#9'Frequency'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_CATEGORY\wwdgPension'
            IniAttributes.Delimiter = ';;'
            IniAttributes.CheckNewFields = False
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object cbCategoryType: TevDBComboBox
          Left = 752
          Top = 448
          Width = 121
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = False
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'CATEGORY_TYPE'
          DataSource = wwdsDetail
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 8
          UnboundDataType = wwDefault
        end
        object cbFrequency: TevDBComboBox
          Left = 752
          Top = 480
          Width = 121
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = False
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'ENROLLMENT_FREQUENCY'
          DataSource = wwdsDetail
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 9
          UnboundDataType = wwDefault
        end
        object fpGeneral: TisUIFashionPanel
          Left = 8
          Top = 266
          Width = 145
          Height = 194
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpGeneral'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'General'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablCategory: TevLabel
            Left = 12
            Top = 35
            Width = 51
            Height = 16
            Caption = '~Category'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 12
            Top = 74
            Width = 59
            Height = 16
            Caption = '~Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evcbCategoryType: TevDBComboBox
            Left = 12
            Top = 50
            Width = 110
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CATEGORY_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
            OnChange = evcbCategoryTypeChange
            OnDropDown = evcbCategoryTypeDropDown
            OnCloseUp = evcbCategoryTypeCloseUp
          end
          object evcbFrequency: TevDBComboBox
            Left = 12
            Top = 89
            Width = 110
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ENROLLMENT_FREQUENCY'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object evrgReadonly: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 110
            Height = 33
            Caption = '~Read Only'
            Columns = 2
            DataField = 'READ_ONLY'
            DataSource = wwdsDetail
            TabOrder = 2
          end
        end
        object fpDaysPrior: TisUIFashionPanel
          Left = 161
          Top = 266
          Width = 214
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpEnrollmentDates'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Days Prior Notification'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel5: TevLabel
            Left = 12
            Top = 35
            Width = 19
            Height = 13
            Caption = 'HR '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel6: TevLabel
            Left = 106
            Top = 35
            Width = 49
            Height = 13
            Caption = 'Employee '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evDayPriorHR: TevDBSpinEdit
            Left = 13
            Top = 50
            Width = 85
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 99.000000000000000000
            DataField = 'ENROLLMENT_NOTIFY_HR_DAYS'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object evDayPriorEmployee: TevDBSpinEdit
            Left = 106
            Top = 50
            Width = 85
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 99.000000000000000000
            DataField = 'ENROLLMENT_NOTIFY_EE_DAYS'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
        object fpDaysBeforeEndReminder: TisUIFashionPanel
          Left = 161
          Top = 367
          Width = 214
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpEnrollmentDates'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Days Before End Reminder'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel2: TevLabel
            Left = 15
            Top = 35
            Width = 19
            Height = 13
            Caption = 'HR '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 106
            Top = 35
            Width = 49
            Height = 13
            Caption = 'Employee '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evDayBeforeHR: TevDBSpinEdit
            Left = 15
            Top = 56
            Width = 85
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 99.000000000000000000
            DataField = 'ENROLLMENT_REMINDER_HR_DAYS'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object evDayBeforeEmployee: TevDBSpinEdit
            Left = 106
            Top = 56
            Width = 85
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 99.000000000000000000
            DataField = 'ENROLLMENT_REMINDER_EE_DAYS'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
        object fpReoccurring: TisUIFashionPanel
          Left = 383
          Top = 266
          Width = 214
          Height = 93
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpEnrollmentDates'
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Reoccurring Reminder (Days)'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel8: TevLabel
            Left = 12
            Top = 35
            Width = 19
            Height = 13
            Caption = 'HR '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel9: TevLabel
            Left = 106
            Top = 35
            Width = 49
            Height = 13
            Caption = 'Employee '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evReoccurHR: TevDBSpinEdit
            Left = 12
            Top = 50
            Width = 85
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 99.000000000000000000
            DataField = 'REMIND_HR_DAYS'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object evReoccurEmployee: TevDBSpinEdit
            Left = 106
            Top = 50
            Width = 85
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 99.000000000000000000
            DataField = 'REMIND_EE_DAYS'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
        object fpBenefitRates: TisUIFashionPanel
          Left = 603
          Top = 8
          Width = 265
          Height = 250
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 6
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Enrollment Dates'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grEnrollmentDates: TevDBGrid
            Left = 12
            Top = 36
            Width = 233
            Height = 194
            DisableThemesInTitle = False
            Selected.Strings = (
              'ENROLLMENT_START_DATE'#9'10'#9'Enrollment start date'#9'F'
              'ENROLLMENT_END_DATE'#9'10'#9'Enrollment end date'#9'F'
              'CATEGORY_COVERAGE_START_DATE'#9'10'#9'Coverage start date'#9'F'
              'CATEGORY_COVERAGE_END_DATE'#9'10'#9'Coverage end date'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_CATEGORY\grEnrollmentDates'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsBenefitEnrollment
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpRateDetails: TisUIFashionPanel
          Left = 603
          Top = 266
          Width = 265
          Height = 203
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpRateDetails'
          Color = 14737632
          TabOrder = 7
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Enrollment Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel11: TevLabel
            Left = 15
            Top = 35
            Width = 83
            Height = 16
            Caption = '~Enrollment Start'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblEndDate: TevLabel
            Left = 118
            Top = 35
            Width = 86
            Height = 16
            Caption = '~Enrollment End  '
          end
          object evLabel1: TevLabel
            Left = 15
            Top = 98
            Width = 83
            Height = 16
            Caption = '~Coverage Start '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel7: TevLabel
            Left = 118
            Top = 98
            Width = 83
            Height = 16
            Caption = '~Coverage End  '
          end
          object evLabel10: TevLabel
            Left = 47
            Top = 82
            Width = 150
            Height = 13
            Caption = 'Qualifying Event Date Range    '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evPanel1: TevPanel
            Left = -16
            Top = 313
            Width = 678
            Height = 320
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object pnlRateFields: TevPanel
              Left = 80
              Top = 14
              Width = 561
              Height = 191
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
            end
          end
          inline EnrollmentdatesMiniNavigationFrame: TMiniNavigationFrame
            Left = 7
            Top = 153
            Width = 219
            Height = 25
            TabOrder = 1
            inherited SpeedButton1: TevSpeedButton
              Left = 10
              Width = 80
              Caption = 'Create'
              Glyph.Data = {00000000}
            end
            inherited SpeedButton2: TevSpeedButton
              Left = 128
              Width = 80
              Caption = 'Delete'
              Glyph.Data = {00000000}
            end
            inherited evActionList1: TevActionList
              Left = 80
              Top = 16
            end
          end
          object evEnrolmentStart: TevDBDateTimePicker
            Left = 15
            Top = 50
            Width = 95
            Height = 21
            HelpContext = 35078
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'ENROLLMENT_START_DATE'
            DataSource = dsBenefitEnrollment
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
          object evEnromentEnd: TevDBDateTimePicker
            Left = 118
            Top = 50
            Width = 95
            Height = 21
            HelpContext = 35078
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'ENROLLMENT_END_DATE'
            DataSource = dsBenefitEnrollment
            Epoch = 1950
            ShowButton = True
            TabOrder = 3
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 15
            Top = 113
            Width = 95
            Height = 21
            HelpContext = 35078
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'CATEGORY_COVERAGE_START_DATE'
            DataSource = dsBenefitEnrollment
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 118
            Top = 113
            Width = 95
            Height = 21
            HelpContext = 35078
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'CATEGORY_COVERAGE_END_DATE'
            DataSource = dsBenefitEnrollment
            Epoch = 1950
            ShowButton = True
            TabOrder = 5
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_BENEFIT_CATEGORY.CO_BENEFIT_CATEGORY
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
  end
  inherited dsCL: TevDataSource
    Left = 264
  end
  object dsBenefitEnrollment: TevDataSource
    DataSet = DM_CO_BENEFIT_ENROLLMENT.CO_BENEFIT_ENROLLMENT
    Left = 214
    Top = 234
  end
end
