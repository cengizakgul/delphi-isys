// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BENEFIT_PROVIDER;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBLookupCombo,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, Mask, wwdbedit,
  isUIwwDBEdit, Wwdotdot, Wwdbcomb, isUIwwDBComboBox, isUIDBMemo;

type
  TEDIT_CO_BENEFIT_PROVIDER= class(TEDIT_CO_BASE)
    tshtPensions: TTabSheet;
    wwdgPension: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    fpCOPensionSummary: TisUIFashionPanel;
    fpCOPensionDetails: TisUIFashionPanel;
    lblName: TevLabel;
    sbPensions: TScrollBox;
    edName: TevDBEdit;
    edWebsite: TevDBEdit;
    lblWebsite: TevLabel;
    lblAgency: TevLabel;
    lcAgency: TevDBLookupCombo;
    memoText: TEvDBMemo;
    evLabel1: TevLabel;
  private
    FEnableHr: boolean;
  protected
    function GetIfReadOnly: Boolean; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure AfterDataSetsReopen; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;

  end;

implementation

{$R *.DFM}

function TEDIT_CO_BENEFIT_PROVIDER.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if (not Result) then
     result:= FEnableHr;
end;

procedure TEDIT_CO_BENEFIT_PROVIDER.AfterDataSetsReopen;
begin
  inherited;
  FEnableHr := GetEnableHrBenefit(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  if GetIfReadOnly then
     SetReadOnly(True)
  else
    ClearReadOnly;
end;

function TEDIT_CO_BENEFIT_PROVIDER.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BENEFIT_PROVIDERS;
end;

function TEDIT_CO_BENEFIT_PROVIDER.GetInsertControl: TWinControl;
begin
  Result := edName;
end;

procedure TEDIT_CO_BENEFIT_PROVIDER.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  inherited;
end;


initialization
  RegisterClass(TEDIT_CO_BENEFIT_PROVIDER);

end.
