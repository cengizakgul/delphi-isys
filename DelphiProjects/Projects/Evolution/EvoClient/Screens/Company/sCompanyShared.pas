unit sCompanyShared;

interface

uses
   Classes,SysUtils,DB, SfieldCodeValues,EvConsts,EvUtils, EvUIComponents;

type
  TSetOfChar = set of char;

  procedure FilterPaymentMethodsChoices( cb: TevDBComboBox; EftType: TStringField );
  function CheckPaymentMethodsChoice(PaymentMethod: TStringField; EftType: TStringField ):boolean;
  
const
  EFT_TYPE_TAX_PAYMENT_METHODS: array [0..2] of
  record
     EftType: string;
     MethodsToRemove: TSetOfChar
  end =
  (
     (EftType:        TAX_PAYMENT_METHOD_NONE_SY_ONLY;
      MethodsToRemove:[
                      TAX_PAYMENT_METHOD_CREDIT,
                      TAX_PAYMENT_METHOD_DEBIT,
                      TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT,
                      TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT
                      ]
     ),
     (EftType:        TAX_PAYMENT_METHOD_CREDIT;
      MethodsToRemove:[
                      TAX_PAYMENT_METHOD_DEBIT,
                      TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT
                      ]
      ),
     (EftType:        TAX_PAYMENT_METHOD_DEBIT;
      MethodsToRemove:[
                      TAX_PAYMENT_METHOD_CREDIT,
                      TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT
                      ]
      )
  );

implementation

uses wwdbedit;




function FilterComboChoices( choices: string; toRemove: TSetOfChar): string;
var
  sl: TStringList;
  i: integer;
  s: string;
begin
  sl := TStringList.Create;
  try
    sl.Text := choices;
    for i:= sl.Count-1 downto 0 do
    begin
      s := sl.Strings[i];
      if s[Length(s)] in toRemove then
        sl.Delete(i);
    end;
    Result := sl.Text;
  finally
    FreeAndNil(sl);
  end;
end;

procedure FilterPaymentMethodsChoices( cb: TevDBComboBox; EftType: TStringField );
var
  m: integer;
  i: integer;
  s: string;
begin
  m := -1;
  if Not Assigned(cb.Field) then exit;
  for i := low(EFT_TYPE_TAX_PAYMENT_METHODS) to high(EFT_TYPE_TAX_PAYMENT_METHODS) do
    if EFT_TYPE_TAX_PAYMENT_METHODS[i].EftType = EftType.AsString then
    begin
      m := i;
      break;
    end;

  s := TaxPaymentMethod_ComboChoices;
  if m <> -1 then
    s := FilterComboChoices( s, EFT_TYPE_TAX_PAYMENT_METHODS[m].MethodsToRemove );
  if cb.Items.Text <> s then
  begin
    cb.Items.Text := s;
    cb.ApplyList();

    if (cb.ItemIndex = -1) and (cb.Field.AsString <> '') then
    begin
      for i:=0 to cb.Items.Count - 1 do
      begin
         s := cb.Items[i];
         if s[Length(s)] = cb.Field.AsString[1] then
         begin
            cb.ItemIndex := i;
            break;
         end;
      end;
    end;
  end;
end;

function CheckPaymentMethodsChoice(PaymentMethod: TStringField; EftType: TStringField ):boolean;
var
  i:integer;
begin
  if (Trim(PaymentMethod.AsString) ='') or (PaymentMethod.IsNull) then
  begin
     result := true;
     exit;
  end;
  for i := low(EFT_TYPE_TAX_PAYMENT_METHODS) to high(EFT_TYPE_TAX_PAYMENT_METHODS) do
  begin
    if EFT_TYPE_TAX_PAYMENT_METHODS[i].EftType = EftType.AsString then
    begin
      if(PaymentMethod.Value[1] in EFT_TYPE_TAX_PAYMENT_METHODS[i].MethodsToRemove) then
      begin
         result := false;
         exit;
      end;
    end;
  end;
  result := true;
end;
end.


