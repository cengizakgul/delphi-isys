inherited EDIT_CO_BENEFIT_PKG_ASMNT: TEDIT_CO_BENEFIT_PKG_ASMNT
  Width = 777
  Height = 620
  inherited Panel1: TevPanel
    Width = 777
    object evDBText1: TevDBText [0]
      Left = 448
      Top = 17
      Width = 64
      Height = 13
      AutoSize = True
      DataField = 'DESCRIPTION'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited pnlFavoriteReport: TevPanel
      Left = 625
    end
    inherited pnlTopLeft: TevPanel
      Width = 625
    end
  end
  inherited PageControl1: TevPageControl
    Width = 777
    Height = 566
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 769
        Height = 537
        inherited pnlBorder: TevPanel
          Width = 765
          Height = 533
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 765
          Height = 533
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Time Off Accrual Rates'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Top = 107
              Width = 367
              Height = 483
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 717
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 717
            Height = 426
            inherited Splitter1: TevSplitter
              Left = 320
              Height = 422
              Visible = True
            end
            object evSplitter2: TevSplitter [1]
              Left = 531
              Top = 0
              Height = 422
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Height = 422
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 342
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'13'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
                  'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
                  'FEIN'#9'9'#9'EIN'#9
                  'DBA'#9'40'#9'DBA '#9'F'
                  'UserFirstName'#9'20'#9'CSR First Name'#9'F'
                  'UserLastName'#9'30'#9'CSR Last Name'#9'F'
                  'HomeState'#9'2'#9'Home State'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_PKG_ASMNT\wwdbgridSelectClient'
                IniAttributes.CheckNewFields = False
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 208
              Height = 422
              Align = alLeft
              Visible = True
              Title = 'Benefit Packages'
              object wwDBGridBenefitPkgAsmnt: TevDBGrid
                Left = 18
                Top = 48
                Width = 158
                Height = 342
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'20'#9'Name'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
            object fpPackageDetails: TisUIFashionPanel
              Left = 534
              Top = 0
              Width = 179
              Height = 422
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Caption = 'fpPackageDetails'
              Color = 14737632
              Constraints.MinWidth = 179
              TabOrder = 2
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Package Assignment'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              Margins.Top = 12
              Margins.Left = 6
              Margins.Right = 12
              Margins.Bottom = 12
              object wwdgPackageAssignment: TevDBGrid
                Left = 18
                Top = 48
                Width = 129
                Height = 342
                DisableThemesInTitle = False
                Selected.Strings = (
                  'DivisionName'#9'30'#9'Division'#9'F'
                  'BranchName'#9'30'#9'Branch'#9'F'
                  'DepartmentName'#9'30'#9'Department'#9'F'
                  'TeamName'#9'30'#9'Team'#9'F'
                  'GroupName'#9'30'#9'Group'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_PKG_ASMNT\wwdgEDGROUPCODES'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object thDetail: TTabSheet
      HelpContext = 55502
      Caption = 'Details'
      ImageIndex = 2
      inline CopyHelper: TCompanyCopyHelper
        Left = 264
        Top = 56
        Width = 114
        Height = 54
        TabOrder = 0
        Visible = False
      end
      object sbAccrualRates: TScrollBox
        Left = 0
        Top = 0
        Width = 769
        Height = 537
        Align = alClient
        TabOrder = 1
        object fpPkgAssignmentSummary: TisUIFashionPanel
          Left = 8
          Top = 120
          Width = 496
          Height = 200
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object DBGridDetail: TevDBGrid
            Left = 12
            Top = 36
            Width = 464
            Height = 144
            DisableThemesInTitle = False
            Selected.Strings = (
              'DivisionName'#9'30'#9'Division Name'#9'F'
              'BranchName'#9'30'#9'Branch Name'#9'F'
              'DepartmentName'#9'30'#9'Department Name'#9'F'
              'TeamName'#9'30'#9'Team Name'#9'F'
              'GroupName'#9'30'#9'Benefit Group'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpPkgASMNTDetails: TisUIFashionPanel
          Left = 8
          Top = 328
          Width = 496
          Height = 203
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Benefit Package Assignment Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 34
            Width = 37
            Height = 13
            Caption = 'Division'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 73
            Width = 34
            Height = 13
            Caption = 'Branch'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 13
            Top = 112
            Width = 55
            Height = 13
            Caption = 'Department'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 13
            Top = 151
            Width = 27
            Height = 13
            Caption = 'Team'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel5: TevLabel
            Left = 247
            Top = 34
            Width = 65
            Height = 13
            Caption = 'Benefit Group'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evcbDivision: TevDBLookupCombo
            Left = 12
            Top = 49
            Width = 227
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F'
              'CUSTOM_DIVISION_NUMBER'#9'20'#9'Division number'#9'F')
            DataField = 'CO_DIVISION_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DIVISION.CO_DIVISION
            LookupField = 'CO_DIVISION_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = evcbDivisionChange
            OnCloseUp = evcbDivisionCloseUp
          end
          object evcbBranch: TevDBLookupCombo
            Left = 12
            Top = 88
            Width = 227
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F'
              'CUSTOM_BRANCH_NUMBER'#9'20'#9'Branch number'#9'F')
            DataField = 'CO_BRANCH_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_BRANCH.CO_BRANCH
            LookupField = 'CO_BRANCH_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = evcbBranchChange
            OnCloseUp = evcbBranchCloseUp
          end
          object evcbDeparment: TevDBLookupCombo
            Left = 13
            Top = 127
            Width = 227
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F'
              'CUSTOM_DEPARTMENT_NUMBER'#9'20'#9'Department number'#9'F')
            DataField = 'CO_DEPARTMENT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DEPARTMENT.CO_DEPARTMENT
            LookupField = 'CO_DEPARTMENT_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = evcbDeparmentChange
            OnCloseUp = evcbDeparmentCloseUp
          end
          object evcbTeam: TevDBLookupCombo
            Left = 13
            Top = 166
            Width = 227
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F'
              'CUSTOM_TEAM_NUMBER'#9'20'#9'Team number'#9'F')
            DataField = 'CO_TEAM_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_TEAM.CO_TEAM
            LookupField = 'CO_TEAM_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evcbGroup: TevDBLookupCombo
            Left = 247
            Top = 49
            Width = 227
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'GROUP_NAME'#9'50'#9'Group name'#9'F')
            DataField = 'CO_GROUP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_GROUP.CO_GROUP
            LookupField = 'CO_GROUP_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
        object fpPackage: TisUIFashionPanel
          Left = 8
          Top = 16
          Width = 496
          Height = 94
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpPackage'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Package'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel6: TevLabel
            Left = 12
            Top = 35
            Width = 52
            Height = 16
            Caption = '~Package'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evPackage: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 278
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F')
            LookupTable = DM_CO_BENEFIT_PACKAGE.CO_BENEFIT_PACKAGE
            LookupField = 'CO_BENEFIT_PACKAGE_NBR'
            Style = csDropDownList
            Navigator = True
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 34
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_BENEFIT_PKG_ASMNT.CO_BENEFIT_PKG_ASMNT
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = wwdsSubMaster
  end
  inherited wwdsList: TevDataSource
    Left = 82
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Top = 34
  end
  inherited dsCL: TevDataSource
    Left = 236
    Top = 34
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 374
    Top = 34
  end
  inherited DM_COMPANY: TDM_COMPANY
    Top = 53
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_CO_BENEFIT_PACKAGE.CO_BENEFIT_PACKAGE
    MasterDataSource = wwdsMaster
    Left = 186
    Top = 51
  end
end
