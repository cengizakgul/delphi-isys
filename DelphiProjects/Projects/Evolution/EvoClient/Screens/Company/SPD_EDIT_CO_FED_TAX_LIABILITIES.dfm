inherited EDIT_CO_FED_TAX_LIABILITIES: TEDIT_CO_FED_TAX_LIABILITIES
  inherited Panel1: TevPanel
    inherited pnlTopLeft: TevPanel
      inherited CompanyNameText: TevDBText
        Width = 281
      end
    end
  end
  inherited PageControl1: TevPageControl
    HelpContext = 43501
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited splEDIT_CO_BASE: TevSplitter
                    Left = 237
                    Width = 3
                    Visible = True
                  end
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 231
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 219
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 240
                    Width = 550
                    Visible = True
                    object fpFederalTax: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 538
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Caption = 'fpFederalTax'
                      Color = 14737632
                      TabOrder = 0
                      OnResize = fpFederalTaxResize
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Federal Tax Liabilities'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 368
              Width = 321
              Height = 166
              Visible = True
              object lcbType: TevDBComboBox
                Left = 160
                Top = 256
                Width = 121
                Height = 21
                HelpContext = 43508
                ShowButton = True
                Style = csDropDownList
                MapList = True
                AllowClearKey = True
                AutoDropDown = True
                DataField = 'TAX_TYPE'
                DataSource = wwdsDetail
                DropDownCount = 8
                Enabled = False
                ItemHeight = 0
                Items.Strings = (
                  'Federal'#9'F'
                  'EE OASDI'#9'O'
                  'EE Medicare'#9'M'
                  'EE EIC'#9'E'
                  'EE Backup Withholding'#9'B'
                  'ER OASDI'#9'A'
                  'ER Medicare'#9'C'
                  'ER FUI'#9'U')
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 0
                UnboundDataType = wwDefault
              end
            end
          end
          inherited Panel3: TevPanel
            OnResize = Panel3Resize
            inherited cbGroup: TevCheckBox
              Left = 545
              HelpContext = 10003
            end
            inherited cbShowCredit: TevCheckBox
              Left = 660
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 368
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 368
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 318
                Height = 362
                IniAttributes.SectionName = 'TEDIT_CO_FED_TAX_LIABILITIES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 371
              Width = 0
              Title = 'Federal Tax Liabilities'
              inherited Splitter2: TevSplitter
                Left = 274
                Height = 362
              end
              inherited wwDBGrid2x: TevDBGrid
                Left = 277
                Width = 142
                Height = 362
                IniAttributes.SectionName = 'TEDIT_CO_FED_TAX_LIABILITIES\wwDBGrid2x'
              end
              inherited wwDBGrid1x: TevDBGrid
                Width = 256
                Height = 362
                Selected.Strings = (
                  'CHECK_DATE'#9'10'#9'Check Date'#9'F'
                  'RUN_NUMBER'#9'5'#9'Run #'#9'F'
                  'SCHEDULED_PROCESS_DATE'#9'10'#9'Scheduled Process Date'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SaveToRegistry = False
                IniAttributes.SectionName = 'TEDIT_CO_FED_TAX_LIABILITIES\wwDBGrid1x'
              end
              inherited lcbAdjType: TevDBComboBox
                MapList = True
                Items.Strings = (
                  'Line 9'#9'9'
                  'Interest'#9'I'
                  'Penalty'#9'P'
                  'Prior Quarter'#9'C'
                  'New Client Setup'#9'N'
                  'Quarter End'#9'Q'
                  'Credit overpaid'#9'R'
                  'Other'#9'O')
              end
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited sbTaxLiabilitiesBase: TScrollBox
        inherited fpBaseLiabilitiesDetails: TisUIFashionPanel [0]
          Width = 312
          Height = 327
          TabOrder = 0
          inherited lablCheck_Date: TevLabel
            Left = 12
            Top = 74
          end
          inherited lablProcess_Date: TevLabel
            Left = 12
            Top = 269
          end
          inherited lablImpound: TevLabel
            Left = 400
            Top = 269
            Visible = False
          end
          inherited lablTax_Deposits: TevLabel
            Left = 12
            Top = 152
          end
          inherited lablPayroll_Run: TevLabel
            Left = 500
            Visible = False
          end
          object lablAdjustment_Type: TevLabel [5]
            Left = 12
            Top = 113
            Width = 79
            Height = 13
            Caption = 'Adjustment Type'
            FocusControl = wwcbAdjustment_Type
          end
          object lablAmount: TevLabel [6]
            Left = 155
            Top = 113
            Width = 46
            Height = 13
            Caption = '~Amount'
            FocusControl = wwedAmount
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDue_Date: TevLabel [7]
            Left = 155
            Top = 74
            Width = 56
            Height = 13
            Caption = '~Due Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablStatus: TevLabel [8]
            Left = 155
            Top = 152
            Width = 41
            Height = 13
            Caption = '~Status'
            FocusControl = wwcbStatus
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablTax_Type: TevLabel [9]
            Left = 12
            Top = 35
            Width = 55
            Height = 13
            Caption = '~Tax Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          inherited wwdbtpCheckDate: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 135
            TabOrder = 1
          end
          inherited dedtImpound: TevDBEdit
            Left = 400
            Top = 284
            Width = 120
            TabOrder = 13
            Visible = False
          end
          inherited dedtPayroll_Run: TevDBEdit
            Left = 500
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 14
            Visible = False
          end
          inherited wwDBDateTimePicker2: TevDBDateTimePicker
            Left = 12
            Top = 284
            Width = 135
            TabOrder = 11
          end
          inherited drgpThirdParty: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 135
            Height = 37
            HelpContext = 43510
            TabOrder = 7
          end
          inherited dedtTax_Deposits: TevEdit
            Left = 12
            Top = 167
            Width = 113
          end
          inherited buttonAssociateDeposit: TevBitBtn
            Left = 126
            Top = 167
            TabOrder = 15
            TabStop = False
          end
          inherited evDBRadioGroup1: TevDBRadioGroup
            Left = 155
            Top = 191
            Width = 135
            Height = 37
            TabOrder = 8
          end
          inherited evDBEditAchKey: TevDBEdit
            Left = 155
            Top = 284
            Width = 135
            TabOrder = 12
          end
          inherited evPanelssd1: TevPanel
            Left = 155
            Top = 269
            Height = 13
            ParentColor = True
            TabOrder = 16
            inherited evLabelssd1: TevLabel
              Align = alClient
            end
          end
          inherited evDBDateTimePicker3: TevDBDateTimePicker
            Left = 12
            Top = 245
            Width = 135
            TabOrder = 9
          end
          inherited evDBDateTimePicker4: TevDBDateTimePicker
            Left = 155
            Top = 245
            Width = 135
            TabOrder = 10
          end
          inherited evPanel2: TevPanel
            Left = 12
            Top = 230
            Height = 13
            ParentColor = True
            TabOrder = 17
            inherited evLabel6: TevLabel
              Align = alClient
            end
          end
          inherited evPanel3: TevPanel
            Left = 155
            Top = 230
            Height = 13
            ParentColor = True
            TabOrder = 18
          end
          object wwcbAdjustment_Type: TevDBComboBox
            Left = 12
            Top = 128
            Width = 135
            Height = 21
            HelpContext = 43508
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'ADJUSTMENT_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Line 9'#9'9'
              'Interest'#9'I'
              'Penalty'#9'P'
              'Prior Quarter'#9'C'
              'New Client Setup'#9'N'
              'Quarter End'#9'Q'
              'Credit overpaid'#9'R'
              'Other'#9'O')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object wwcbType: TevDBComboBox
            Left = 12
            Top = 50
            Width = 135
            Height = 21
            HelpContext = 43513
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TAX_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Federal'#9'F'
              'EE OASDI'#9'O'
              'EE Medicare'#9'M'
              'EE EIC'#9'E'
              'EE Backup Withholding'#9'B'
              'ER OASDI'#9'A'
              'ER Medicare'#9'C'
              'ER FUI'#9'U')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object wwcbStatus: TevDBComboBox
            Left = 155
            Top = 167
            Width = 135
            Height = 21
            HelpContext = 43509
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Pending'#9'X'
              'Paid'#9'P'
              'Paid-Send Notice'#9'S'
              'NSF'#9'N'
              'Voided'#9'V'
              'Send-Notice'#9'C'
              'Notice-Sent'#9'T'
              'Deleted'#9'D'
              'Manually impounded'#9'M'
              'Paid w/o funds'#9'W'
              'Impounded'#9'I')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 6
            UnboundDataType = wwDefault
          end
          object wwdpDue_Date: TevDBDateTimePicker
            Left = 155
            Top = 89
            Width = 135
            Height = 21
            HelpContext = 43505
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DUE_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
          object wwedAmount: TevDBEdit
            Left = 155
            Top = 128
            Width = 135
            Height = 21
            HelpContext = 43512
            DataField = 'AMOUNT'
            DataSource = wwdsDetail
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
        inherited fpLiabilityNotes: TisUIFashionPanel [1]
          Left = 328
          Top = 343
          Width = 465
          TabOrder = 4
          inherited dmemNotes: TEvDBMemo
            Width = 430
          end
        end
        inherited fpTaxLiabilities: TisUIFashionPanel
          Left = 328
          Width = 465
          TabOrder = 2
          inherited Label27: TevLabel
            Top = 292
          end
          inherited lablTotalTaxValue: TevLabel
            Left = 83
            Top = 292
          end
          object Label1x: TevLabel [3]
            Left = 278
            Top = 292
            Width = 91
            Height = 13
            Caption = 'Total Taxes - FUI ='
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablTotalTaxLessFUI: TevLabel [4]
            Left = 375
            Top = 292
            Width = 3
            Height = 13
          end
          inherited wwdgTaxLiabilities: TevDBGrid
            Width = 430
            IniAttributes.SectionName = 'TEDIT_CO_FED_TAX_LIABILITIES\wwdgTaxLiabilities'
          end
        end
        inherited fpTools: TisUIFashionPanel
          Top = 343
          Width = 312
          Height = 112
          TabOrder = 3
          inherited BitBtn3: TevBitBtn
            Width = 277
          end
          inherited bbtnAttach: TevBitBtn
            Left = 12
            Top = 67
            Width = 277
          end
        end
        inherited Panel4: TevPanel
          Left = 0
          Top = 0
          Width = 322
          Height = 337
          TabOrder = 1
          inherited fpTaxDeposit: TisUIFashionPanel
            Width = 312
            Height = 327
            inherited Label91: TevLabel
              Left = 155
            end
            inherited wwcbAdjustment_Type1: TevDBComboBox
              Width = 135
            end
            inherited wwcbStatus1: TevDBComboBox
              Left = 155
              Width = 135
            end
            inherited DBRadioGroup11: TevDBRadioGroup
              Width = 135
            end
            inherited Button1: TevButton
              Left = 155
              Width = 135
            end
            inherited wwDBLookupCombo11: TevDBLookupCombo
              Width = 135
            end
            inherited cbClearTaxDepRecord: TevCheckBox
              Left = 155
            end
            inherited evDBRadioGroup2: TevDBRadioGroup
              Left = 155
              Width = 135
            end
            inherited evDBEdit1: TevDBEdit
              Width = 135
            end
            inherited evDBDateTimePicker1: TevDBDateTimePicker
              Width = 135
            end
            inherited evDBDateTimePicker2: TevDBDateTimePicker
              Left = 155
              Width = 135
            end
            inherited evPanel5: TevPanel
              Left = 155
            end
          end
        end
      end
    end
    inherited tsCO_TAX_DEPOSITS: TTabSheet
      inherited sbTaxDeposits: TScrollBox
        inherited fpTaxDeposits: TisUIFashionPanel
          inherited wwDBGrid1: TevDBGrid
            IniAttributes.SectionName = 'TEDIT_CO_FED_TAX_LIABILITIES\wwDBGrid1'
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 24
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_FED_TAX_LIABILITIES.CO_FED_TAX_LIABILITIES
    Top = 16
  end
end
