inherited EDIT_CO_TIME_OFF_ACCRUAL_TIERS: TEDIT_CO_TIME_OFF_ACCRUAL_TIERS
  inherited PageControl1: TevPageControl
    ActivePage = tshtDetails
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Time Off Accrual Tiers'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCORightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 363
              Top = 72
              Width = 390
              Height = 445
              Visible = True
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 320
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 245
                IniAttributes.SectionName = 'TEDIT_CO_TIME_OFF_ACCRUAL_TIERS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 48
              Align = alClient
              Visible = True
              Title = 'Time Off Accrual Tiers'
              object evDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 194
                Height = 245
                DisableThemesInTitle = False
                Selected.Strings = (
                  'DESCRIPTION'#9'40'#9'Time Off Accrual'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_TIME_OFF_ACCRUAL_TIERS\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object evSplitter1: TevSplitter
        Left = 0
        Top = 147
        Width = 427
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        Visible = False
      end
      object pErrorMessage: TevPanel
        Left = 0
        Top = 150
        Width = 427
        Height = 32
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object evLabel1: TevLabel
          Left = 8
          Top = 8
          Width = 89
          Height = 16
          Caption = 'lErrorMessage'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
      object fpCOTOATiersSummary: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 435
        Height = 437
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'pnlSummary'
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Summary'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object gTiers: TevDBGrid
          Left = 12
          Top = 35
          Width = 400
          Height = 381
          DisableThemesInTitle = False
          Selected.Strings = (
            'MINIMUM_MONTH_NUMBER'#9'10'#9'Months Min'#9'F'
            'MAXIMUM_MONTH_NUMBER'#9'10'#9'Months Max'#9'F'
            'MINIMUM_HOURS'#9'10'#9'Hours Min'#9'F'
            'MAXIMUM_HOURS'#9'10'#9'Hours Max'#9'F'
            'RATE'#9'17'#9'Rate'#9'F')
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CO_TIME_OFF_ACCRUAL_TIERS\gTiers'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsDetail
          ReadOnly = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_TIME_OFF_ACCRUAL_TIERS.CO_TIME_OFF_ACCRUAL_TIERS
    MasterDataSource = wwdsSubMaster
  end
  inherited dsCL: TevDataSource
    Left = 268
    Top = 58
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_CO_TIME_OFF_ACCRUAL.CO_TIME_OFF_ACCRUAL
    MasterDataSource = wwdsMaster
    Left = 257
    Top = 24
  end
end
