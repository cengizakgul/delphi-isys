// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_ESSTIMEOFFMANAGEMENT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, ISBasicClasses, SDDClasses, SDataDictclient,
  SDataDicttemp, ImgList, Mask, wwdbedit, Wwdbspin, EvCommonInterfaces,
  wwdbdatetimepicker, Wwdotdot, Wwdbcomb, SSecurityInterface, SFrameEntry,
  evSecElement, evClasses, TypInfo, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, isUIwwDBComboBox, isUIwwDBDateTimePicker, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_ESSTIMEOFFMANAGEMENT = class(TEDIT_CO_BASE)
    tshtDetails: TTabSheet;
    TORequests: TevClientDataSet;
    SetEENbr: TIntegerField;
    FirstName: TStringField;
    LastName: TStringField;
    SetEECode: TStringField;
    RequestDate: TDateTimeField;
    Balance: TFloatField;
    FType: TStringField;
    AccrualNbr: TIntegerField;
    AccuralDate: TDateTimeField;
    Description: TStringField;
    Status: TStringField;
    ToDate: TDateTimeField;
    StatusCode: TStringField;
    Panel6: TPanel;
    Selected: TStringField;
    TOADetailsDateTimeField1: TDateTimeField;
    TOADetailsIntegerField2: TIntegerField;
    TOADetailsIntegerField3: TIntegerField;
    TOADetailsEetimeoffaccrualopernbr: TIntegerField;
    TOADetails: TevClientDataSet;
    TOADetailsStatusCode: TStringField;
    TOADetailsNotes: TStringField;
    Note: TStringField;
    TOADetailsMinutes: TIntegerField;
    FTime: TFloatField;
    TOADetailsTime: TFloatField;
    sbESSTimeOff: TScrollBox;
    fpDisplayRequests: TisUIFashionPanel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    cbStatusPending: TevCheckBox;
    cbStatusUsed: TevCheckBox;
    cbStatusApproved: TevCheckBox;
    cbStatusDenied: TevCheckBox;
    evdtFrom: TevDBDateTimePicker;
    evdtTo: TevDBDateTimePicker;
    cbType: TevDBComboBox;
    edEECode: TevEdit;
    cbManager: TevDBComboBox;
    butnRefresh: TevBitBtn;
    fpUpdateStates: TisUIFashionPanel;
    BtnApprove: TevBitBtn;
    BtnDeny: TevBitBtn;
    BtnUsed: TevBitBtn;
    evLabel1: TevLabel;
    BtnUpdate: TevBitBtn;
    isUIFashionPanel1: TisUIFashionPanel;
    wwwdgTOA: TevDBGrid;
    TORequestsManager_Note: TStringField;
    procedure butnRefreshClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtnUpdateClick(Sender: TObject);
    procedure BtnApproveClick(Sender: TObject);
    procedure BtnDenyClick(Sender: TObject);
    procedure tshtDetailsEnter(Sender: TObject);
    procedure BtnUsedClick(Sender: TObject);
    procedure wwwdgTOACalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure wwwdgTOACellChanged(Sender: TObject);
    procedure wwwdgTOAFieldChanged(Sender: TObject; Field: TField);
    procedure TORequestsAfterScroll(DataSet: TDataSet);
  private
    Fselectedcount : integer;

    procedure UpdateButtons;
    procedure ShowSelected;
    procedure populateDataSet;
    procedure RefreshData;
    procedure CallProcessTOARequests(const Operation: TevTOAOperation);

  protected
    function  IgnoreAutoJump: Boolean; override;
    procedure OnGetSecurityStates(const States: TSecurityStateList); override;
  public
    procedure Activate; override;
  end;

implementation

uses isBaseClasses, EvContext, Variants, EvConsts, evBasicUtils, ISBasicUtils, EvTOARequest,
     SEncryptionRoutines, SPD_EDIT_CO_UPDATEESSTIMEOFF, evUtils, evDataSet;
{$R *.DFM}

const
  ctEnabledSLA = 'L';

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.ShowSelected;
const
  seltext : string =
    'selected'       + #9 + '1'  + #9 + 'Selected'   + #9 + 'F'#13#10 +
    'status'         + #9 + '10' + #9 + 'Status'     + #9 + 'T'#13#10 +
    'RequestDate'    + #9 + '10' + #9 + 'Request Date' + #9 + 'T'#13#10 +
    'CustomEENumber' + #9 + '5'  + #9 + 'EE Code'    + #9 + 'T'#13#10 +
    'LastName'       + #9 + '15' + #9 + 'Last Name'  + #9 + 'T'#13#10 +
    'FirstName'      + #9 + '15' + #9 + 'First Name' + #9 + 'T'#13#10 +
    'FromDate'       + #9 + '10' + #9 + 'From'       + #9 + 'T'#13#10 +
    'ToDate'         + #9 + '10' + #9 + 'To' + #9    + 'T'#13#10 +
    'Time'           + #9 + '5'  + #9 + 'Hours' + #9 + 'T'#13#10 +
    'Balance'        + #9 + '5'  + #9 + 'Current Balance' + #9 + 'T'#13#10 +
    'Description'    + #9 + '15' + #9 + 'Type' + #9  + 'T'#13#10 +
    'Notes'          + #9 + '20' + #9 + 'Notes' + #9 + 'T'#13#10 +
    'Manager_Note'   + #9 + '20' + #9 + 'Manager Notes' + #9 + 'T';
begin
  wwwdgTOA.Selected.Text := seltext;
  wwwdgTOA.UnselectAll;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.populateDataSet;
var
  Requests : IisInterfaceList;
  Request : IEvTOABusinessRequest;
  DayRequest : IEvTOARequest;
  i, j : integer;
  TotHours : double;
  Manager, TOType, EECode: Variant;
  dDateFrom, dDateTo: TDatetime;

  function GetListOfRequestStatuses: String;
  begin
    Result := '';
    if cbStatusPending.Checked then
      Result := Result + EE_TOA_OPER_CODE_REQUEST_PENDING;
    if cbStatusApproved.Checked then
      Result := Result + EE_TOA_OPER_CODE_REQUEST_APPROVED;
    if cbStatusDenied.Checked then
      Result := Result + EE_TOA_OPER_CODE_REQUEST_DENIED;
    if cbStatusUsed.Checked then
      Result := Result + EE_TOA_OPER_CODE_USED;
  end;

begin
  try
    if cbManager.ItemIndex > 0 then
      Manager := cbManager.Value
    else
      Manager := Null;

    if cbType.ItemIndex > 0 then
      TOType := cbType.Value
    else
      TOType := Null;

    if Length(edEECode.Text) > 0 then
      EECode := edEECode.Text
    else
      EECode := Null;

    Fselectedcount := 0;

    dDateFrom := Trunc(evdtFrom.Date);
    dDateTo := Trunc(evdtTo.Date);
    if (dDateFrom <> 0) and (dDateTo = 0) then
      dDateTo := EncodeDate(2200, 1, 1);
    if (dDateTo <> 0) and (dDateFrom = 0) then
      dDateFrom := 1;

    Requests := Context.RemoteMiscRoutines.GetTOARequestsForCO(
      wwdsMaster.Dataset.FieldByName('CO_NBR').asInteger, GetListOfRequestStatuses, dDateFrom, dDateTo,
      Manager, TOType, EECode);
    for i := 0 to Requests.Count - 1 do
    begin
      Request := IInterface(Requests.Items[i]) as IEvTOABusinessRequest;

      TORequests.Append;

      TORequests.FieldByName('EENbr').Value := Request.EENbr;
      TORequests.FieldByName('FirstName').Value := Request.FirstName;
      TORequests.FieldByName('LastName').Value := Request.LastName;
      TORequests.FieldByName('CustomEENumber').Value := Request.CustomEENumber;
      TORequests.FieldByName('EETimeOffAccrualNbr').Value := Request.EETimeOffAccrualNbr;
      TORequests.FieldByName('Balance').Value := Request.Balance;
      TORequests.FieldByName('Description').Value := Request.Description;
      TORequests.FieldByName('RequestDate').Value := Request.Date;
      TORequests.FieldByName('Status').Value := ConvertCode(TEE_TIME_OFF_ACCRUAL_OPER, 'OPERATION_CODE',Request.Status);
      TORequests.FieldByName('StatusCode').Value := Request.Status;
      TORequests.FieldByName('Notes').Value := Request.Notes;
      TORequests.FieldByName('Manager_Note').Value := Request.Manager_Note;

      TORequests.FieldByName('selected').Value := 'N';
      totHours := 0;
      DayRequest := nil;
      for j := 0 to Request.Items.Count - 1 do
      begin
        DayRequest := IInterface(Request.Items[j]) as IEvTOARequest;

        TOADetails.Append;
        TOADetails.FieldByName('EETimeOffAccrualNbr').Value := Request.EETimeOffAccrualNbr;
        TOADetails.FieldByName('EETimeOffAccrualOperNbr').Value := DayRequest.EETimeOffAccrualOperNbr;
        TOADetails.FieldByName('StatusCode').Value  := Request.Status;
        TOADetails.FieldByName('AccrualDate').Value := DayRequest.AccrualDate;
        TOADetails.FieldByName('Hours').Value := DayRequest.Hours;
        TOADetails.FieldByName('Minutes').Value  := DayRequest.Minutes;
        TOADetails.FieldByName('Notes').Value := DayRequest.Notes;
        TOADetails.FieldByName('Time').Value  := DayRequest.HoursAndMinAsDouble;

        TOADetails.Post;

        if VarIsNull(TORequests['FromDate']) then
           TORequests.FieldByName('FromDate').Value := DayRequest.AccrualDate;
        totHours := totHours + DayRequest.HoursAndMinAsDouble;
      end;

      if assigned(DayRequest) then
      begin
        TORequests.FieldByName('Time').Value := totHours;
        TORequests.FieldByName('ToDate').Value := DayRequest.AccrualDate;
      end;

      TORequests.Post;

    end;
 finally
   ShowSelected;
   TOADetails.Filter    := '';
   TOADetails.Filtered  := True;

   if TORequests.RecordCount > 0 then
   begin
     TORequests.First;
     TOADetails.Filter := 'EETimeOffAccrualNbr = ' + IntTostr(TORequests.FieldByName('EETimeOffAccrualNbr').asInteger)
                          + ' and StatusCode = '+ QuotedStr(TORequests.FieldByName('StatusCode').asString)
                          + ' and AccrualDate >= ' + QuotedStr(TORequests.FieldByName('FromDate').AsString)
                          + ' and AccrualDate <= ' + QuotedStr(TORequests.FieldByName('ToDate').AsString);

   end;
   UpdateButtons;

 end;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.Activate;
begin
  inherited;
  cbStatusPending.Checked  := FindSecurityElement(Self, ClassName).SecurityState <> ctEnabledSLA;
  cbStatusApproved.Checked := FindSecurityElement(Self, ClassName).SecurityState = ctEnabledSLA;
  cbStatusDenied.Checked   := False;
  cbStatusUsed.Checked     := False;
  evdtFrom.Date            := 0;
  evdtTo.Date              := 0;

  cbStatusApproved.Enabled := True; // FindSecurityElement(Self, ClassName).SecurityState <> ctEnabledSLA;
  cbStatusUsed.Enabled     := True;

  RefreshData;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.butnRefreshClick(Sender: TObject);
begin
  inherited;
  RefreshData;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.BitBtn1Click(Sender: TObject);
begin
  inherited;
  RefreshData;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.TORequestsAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  if TORequests.FieldByName('EETimeOffAccrualNbr').AsInteger > 0
  then TOADetails.Filter := 'EETimeOffAccrualNbr = ' + IntToStr(TORequests.FieldByName('EETimeOffAccrualNbr').AsInteger)
                       + ' and StatusCode = '+ QuotedStr(TORequests.FieldByName('StatusCode').asString)
                       + ' and AccrualDate >= ' + QuotedStr(TORequests.FieldByName('FromDate').AsString)
                       + ' and AccrualDate <= ' + QuotedStr(TORequests.FieldByName('ToDate').AsString)
  else TOADetails.Filter := '';
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.UpdateButtons;
begin
  BtnApprove.Enabled := (FindSecurityElement(Self, ClassName).SecurityState <> ctEnabledSLA)
       and not TORequests.IsEmpty;
  BtnDeny.Enabled :=    (FindSecurityElement(Self, ClassName).SecurityState <> ctEnabledSLA)
       and not TORequests.IsEmpty;
  BtnUsed.Enabled :=   not TORequests.IsEmpty;
  BtnUpdate.Enabled := (FindSecurityElement(Self, ClassName).SecurityState <> ctEnabledSLA) and
       not TORequests.IsEmpty  and (Fselectedcount = 1);
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.BtnUpdateClick(Sender: TObject);
var
  EditESSTIMEOFF: TEDIT_CO_UPDATEESSTIMEOFF;
  oldnbr : integer;
begin
  oldnbr := 0;
  TORequests.DisableControls;
  EditESSTIMEOFF := TEDIT_CO_UPDATEESSTIMEOFF.Create(nil);

  TORequests.Filter := ' selected = ''Y'' and (StatusCode = ''W'' or StatusCode = ''G'') ';
  TORequests.Filtered := True;
  try
    if TORequests.RecordCount = 1 then
    begin
     EditESSTIMEOFF.CustomCoNumber := wwdsMaster.Dataset.FieldByName('CUSTOM_COMPANY_NUMBER').asstring;
     EditESSTIMEOFF.wwdsDetail.DataSet := TORequests;
     EditESSTIMEOFF.dsTOADetails.DataSet := TOADetails;
     if EditESSTIMEOFF.Showmodal = mrOk then
       oldnbr := TORequests.FieldByName('EETimeOffAccrualNbr').asInteger;
    end
    else
     EvMessage('Please select one record.')
  finally
    TORequests.Filter := '';
    TORequests.Filtered := false;
    TORequests.EnableControls;
    EditESSTIMEOFF.Free;

    if oldnbr > 0 then
    begin
      RefreshData;
      TORequests.Locate('EETimeOffAccrualNbr',oldnbr, []);
    end;
  end;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.BtnApproveClick(Sender: TObject);
begin
  CallProcessTOARequests(toaApprove);
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.CallProcessTOARequests(const Operation: TevTOAOperation );
   function CheckApprovedRecord:boolean;
   begin
    TORequests.Filter := ' selected = ''Y'' and StatusCode = ''G''';
    TORequests.Filtered := True;
    try
       Result := TORequests.RecordCount > 0;
    finally
      TORequests.Filter := '';
      TORequests.Filtered := False;
    end;
 end;
var
  nbrlist : IisStringList;
begin
  nbrlist := TisStringList.create;
  wwdsDetail.DataSet.DisableControls;

  if CheckApprovedRecord then
       EvMessage(' Approved items cannot be updated to the selected status.  No update to those records will be performed.');

  TORequests.Filter := ' selected = ''Y'' and StatusCode = ''W'' ';
  TORequests.Filtered := True;
  try
    if TORequests.RecordCount < 1 then
       EvMessage('Please select pending items')
    else begin
      TORequests.First;
      while not TORequests.EOF do
      begin
        nbrlist.Clear;
        while not TOADetails.Eof do
        begin
          nbrlist.Add(TOADetails.FieldByName('EETimeOffAccrualOperNbr').asString);
          TOADetails.next;
        end;
        if nbrlist.Count > 0 then
          Context.RemoteMiscRoutines.ProcessTOARequests(Operation, TORequests.FieldByName('Notes').asstring, nbrlist, False);
        TORequests.Next;
      end;
    end;
  finally
    TORequests.Filter := '';
    TORequests.Filtered := False;

    wwdsDetail.DataSet.EnableControls;
    RefreshData;
  end;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.BtnDenyClick(Sender: TObject);
begin
  CallProcessTOARequests(toaDeny);
end;

function TEDIT_CO_ESSTIMEOFFMANAGEMENT.IgnoreAutoJump: Boolean;
begin
  Result := false;
  if wwdsList.DataSet.RecordCount <= 0 then exit;

  if wwdsMaster.Dataset.Active then
  begin
     tshtDetails.TabVisible :=( Context.License.SelfServe and
                                ((wwdsMaster.Dataset.FieldByName('ENABLE_ESS').asString = GROUP_BOX_YES) or
                                 (wwdsMaster.Dataset.FieldByName('ENABLE_ESS').asString = GROUP_BOX_READONLY)));
     Result :=  not tshtDetails.TabVisible;
     if Result then
     begin
       EvMessage(wwdsMaster.Dataset.FieldByName('Custom_Company_Number').asString + ', '+
                 wwdsMaster.Dataset.FieldByName('Name').asString + ': This Company does not have Employee Self Serve Enabled.')
     end;
  end;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.tshtDetailsEnter(Sender: TObject);
var
  Q: IevQuery;
begin
  if DM_COMPANY.CO.IsEmpty then
    Exit;

  Q := TevQuery.Create(
    'select m.CO_GROUP_MANAGER_NBR, p.FIRST_NAME, p.LAST_NAME, m.Secondary ' +
    'from CO_GROUP_MANAGER m ' +
    'join EE e on e.EE_NBR=m.EE_NBR ' +
    'join CL_PERSON p on p.CL_PERSON_NBR=e.CL_PERSON_NBR ' +
    'join CO_GROUP g on g.CO_GROUP_NBR=m.CO_GROUP_NBR ' +
    'where {AsOfNow<m>} and {AsOfNow<e>} and {AsOfNow<p>} and {AsOfNow<g>} ' +
    'and g.GROUP_TYPE=''' + ESS_GROUPTYPE_TIMEOFF + ''' ' +
    'and g.CO_NBR='+DM_COMPANY.CO.CO_NBR.AsString);

  cbManager.Items.Clear;
  cbManager.Items.Add('All'#9'0');
  cbManager.ItemIndex := 0;

  while not Q.Result.Eof do
  begin
    if Q.Result.FieldByName('Secondary').AsString = 'Y' then
      cbManager.Items.Add(Q.Result.FieldByName('FIRST_NAME').AsString+' '+
        Q.Result.FieldByName('LAST_NAME').AsString + ' (Secondary Manager)' +#9+
        Q.Result.FieldByName('CO_GROUP_MANAGER_NBR').AsString)
    else
      cbManager.Items.Add(Q.Result.FieldByName('FIRST_NAME').AsString+' '+
        Q.Result.FieldByName('LAST_NAME').AsString +#9+
        Q.Result.FieldByName('CO_GROUP_MANAGER_NBR').AsString);
    Q.Result.Next;
  end;

  Q := TevQuery.Create(
    'select m.CO_TIME_OFF_ACCRUAL_NBR, m.DESCRIPTION ' +
    'from CO_TIME_OFF_ACCRUAL m ' +
    'where {AsOfNow<m>} ' +
    'and m.CO_NBR='+DM_COMPANY.CO.CO_NBR.AsString);

  cbType.Items.Clear;
  cbType.Items.Add('All'#9'0');
  cbType.ItemIndex := 0;

  while not Q.Result.Eof do
  begin
    cbType.Items.Add(Q.Result.FieldByName('DESCRIPTION').AsString +#9+
        Q.Result.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').AsString);
    Q.Result.Next;
  end;

end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.BtnUsedClick(Sender: TObject);
type
  TEmployee = record
    Number: Integer;
    TOANumber: Integer;
    MaxPrNbr: Integer;
    MaxBatchNbr: Integer;    
    MaxCheckNbr: Integer;
    MinDate: TDateTime;
    MaxDate: TDateTime;
    Requested: Real;
    Actual: Real;
    Notes: String;
    Description: String;
  end;

const

  sqlOpers =
    'select o.EE_TIME_OFF_ACCRUAL_OPER_NBR, o.USED ' +
    'from EE_TIME_OFF_ACCRUAL_OPER o ' +
    'join EE_TIME_OFF_ACCRUAL a on a.EE_TIME_OFF_ACCRUAL_NBR=o.EE_TIME_OFF_ACCRUAL_NBR ' +
    'where {AsOfNow<o>} and {AsOfNow<a>} ' +
    'and a.EE_NBR=:EENbr ' +
    'and a.EE_TIME_OFF_ACCRUAL_NBR=:TOANbr ' +
    'and o.OPERATION_CODE in (''W'',''G'') ' +
    'and o.ACCRUAL_DATE between :StartDate and :EndDate '+
    'order by o.PR_NBR desc';

  sqlTotal =
    'select sum(o.USED) USED ' +
    'from EE_TIME_OFF_ACCRUAL_OPER o ' +
    'join EE_TIME_OFF_ACCRUAL a on a.EE_TIME_OFF_ACCRUAL_NBR=o.EE_TIME_OFF_ACCRUAL_NBR ' +
    'where {AsOfNow<o>} and {AsOfNow<a>} ' +
    'and a.EE_NBR=:EENbr ' +
    'and a.EE_TIME_OFF_ACCRUAL_NBR=:TOANbr ' +
    'and o.OPERATION_CODE in (''W'',''G'') ' +
    'and o.ACCRUAL_DATE between :StartDate and :EndDate';

  sqlMinMax =
    'select min(b.PERIOD_BEGIN_DATE) MIN_DATE, max(b.PERIOD_END_DATE) MAX_DATE, MAX(c.PR_NBR) MAX_PR, MAX(c.PR_CHECK_NBR) MAX_CHECK ' +
    'from EE_TIME_OFF_ACCRUAL_OPER o ' +
    'join EE_TIME_OFF_ACCRUAL a on a.EE_TIME_OFF_ACCRUAL_NBR=o.EE_TIME_OFF_ACCRUAL_NBR ' +
    'join PR_CHECK c on c.PR_NBR = o.PR_NBR and c.EE_NBR=a.EE_NBR ' +
    'join PR_BATCH b on b.PR_BATCH_NBR = c.PR_BATCH_NBR ' +
    'where a.EE_NBR=:EENbr ' +
    'and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} ' +
    'and o.OPERATION_CODE in (''U'') ' +
    'and a.EE_TIME_OFF_ACCRUAL_NBR=:TOANbr ' +
    'and (((b.PERIOD_BEGIN_DATE >= :StartDate and b.PERIOD_BEGIN_DATE <= :EndDate) ' +
    'and (b.PERIOD_END_DATE <= :EndDate and b.PERIOD_END_DATE >= :MinDate)) or '+
    '(:StartDate between b.PERIOD_BEGIN_DATE and b.PERIOD_END_DATE))';

  sqlActual =
    'select sum(o.USED) USED, max(c.PR_NBR) PR_NBR, max(b.PR_BATCH_NBR) PR_BATCH_NBR, max(c.PR_CHECK_NBR) PR_CHECK_NBR ' +
    'from EE_TIME_OFF_ACCRUAL_OPER o ' +
    'join EE_TIME_OFF_ACCRUAL a on a.EE_TIME_OFF_ACCRUAL_NBR=o.EE_TIME_OFF_ACCRUAL_NBR ' +
    'join PR_CHECK c on c.PR_NBR = o.PR_NBR and c.EE_NBR=a.EE_NBR ' +
    'join PR_BATCH b on b.PR_BATCH_NBR = c.PR_BATCH_NBR ' +
    'where a.EE_NBR=:EENbr ' +
    'and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} ' +
    'and o.OPERATION_CODE in (''U'') ' +
    'and a.EE_TIME_OFF_ACCRUAL_NBR=:TOANbr ' +
    'and b.PERIOD_BEGIN_DATE between :StartDate and :EndDate '+
    'and b.PERIOD_END_DATE between :StartDate and :EndDate ';

var
  Bookmark: Integer;
  Employees: array of TEmployee;
  i, idx, ridx, EENbr, TOANumber: Integer;
  Q: IevQuery;
  OK, Found: Boolean;
begin
  inherited;
  Bookmark := TORequests.FieldByName('EETimeOffAccrualNbr').AsInteger;
  OK := True;
  Found := False;
  TORequests.Filter := ' selected = ''Y'' and (StatusCode = ''W'' or StatusCode = ''G'')';
  TORequests.Filtered := true;
  try
    try
      TORequests.First;
      while not TORequests.Eof do
      begin
          idx := -1;
          EENbr := TORequests.FieldByName('EENbr').AsInteger;
          TOANumber := TORequests.FieldByName('EETimeOffAccrualNbr').AsInteger;
          for i := 0 to Length(Employees) - 1 do
          begin
            if (Employees[i].Number = EENbr) and (Employees[i].TOANumber = TOANumber) then
            begin
              idx := i;
              break;
            end;
          end;
          if idx < 0 then
          begin
            idx := Length(Employees);
            SetLength(Employees, idx + 1);
            Employees[idx].Number := EENbr;
            Employees[idx].TOANumber := TOANumber;
            Employees[idx].Requested := 0;
            Employees[idx].Actual := 0;
            Employees[idx].MinDate := TORequests.FieldByName('FromDate').AsDateTime;
            Employees[idx].MaxDate := TORequests.FieldByName('ToDate').AsDateTime;
            Employees[idx].Notes := TORequests.FieldByName('Notes').AsString;
            Employees[idx].Description := TORequests.FieldByName('Description').AsString;
          end
          else
          begin
            if Employees[idx].MinDate > TORequests.FieldByName('FromDate').AsDateTime then
              Employees[idx].MinDate := TORequests.FieldByName('FromDate').AsDateTime;
            if Employees[idx].MaxDate < TORequests.FieldByName('ToDate').AsDateTime then
              Employees[idx].MaxDate := TORequests.FieldByName('ToDate').AsDateTime;
          end;
          Found := True;

        TORequests.Next;
      end;
      for i := 0 to Length(Employees) - 1 do
      begin
        Q := TevQuery.Create(sqlMinMax);
        Q.Params.AddValue('EENbr', Employees[i].Number);
        Q.Params.AddValue('TOANbr', Employees[i].TOANumber);
        Q.Params.AddValue('StartDate', Employees[i].MinDate);
        Q.Params.AddValue('EndDate', Employees[i].MaxDate);
        if not Q.Result.FieldByName('MIN_DATE').IsNull then
        begin
          Employees[i].MinDate := Q.Result.FieldByName('MIN_DATE').AsDateTime;
          Employees[i].MaxDate := Q.Result.FieldByName('MAX_DATE').AsDateTime;
          Employees[i].MaxPrNbr := Q.Result.FieldByName('MAX_PR').AsInteger;
          Employees[i].MaxCheckNbr := Q.Result.FieldByName('MAX_CHECK').AsInteger;

          Q := TevQuery.Create(sqlTotal);
          Q.Params.AddValue('EENbr', Employees[i].Number);
          Q.Params.AddValue('TOANbr', Employees[i].TOANumber);
          Q.Params.AddValue('StartDate', Employees[i].MinDate);
          Q.Params.AddValue('EndDate', Employees[i].MaxDate);
          Employees[i].Requested := Q.Result.FieldByName('USED').AsFloat;

          Q := TevQuery.Create(sqlActual);
          Q.Params.AddValue('EENbr', Employees[i].Number);
          Q.Params.AddValue('TOANbr', Employees[i].TOANumber);
          Q.Params.AddValue('StartDate', Employees[i].MinDate);
          Q.Params.AddValue('EndDate', Employees[i].MaxDate);
          Employees[i].Actual := Q.Result.FieldByName('USED').AsFloat;
          Employees[i].MaxPrNbr := Q.Result.FieldByName('PR_NBR').AsInteger;
          Employees[i].MaxBatchNbr := Q.Result.FieldByName('PR_BATCH_NBR').AsInteger;
          Employees[i].MaxCheckNbr := Q.Result.FieldByName('PR_CHECK_NBR').AsInteger;

          OK := OK and (Employees[i].Requested = Employees[i].Actual);

        end
        else
        begin
          Employees[i].MinDate := 0;
          Employees[i].MaxDate := 0;
          Found := False;
          EvMessage('No corresponding payrolls found!', mtError, [mbOk]);
        end;
      end;
      if Found then
      begin
        if not OK then
          OK := EvMessage('The hours requested don''t match payroll.  The request will be updated accordingly.', mtConfirmation, [mbOk, mbCancel]) = mrOk;
        if OK then
        begin
          for i := 0 to Length(Employees) - 1 do
          begin
            Q := TevQuery.Create(sqlOpers);
            Q.Params.AddValue('EENbr', Employees[i].Number);
            Q.Params.AddValue('TOANbr', Employees[i].TOANumber);
            Q.Params.AddValue('StartDate', Employees[i].MinDate);
            Q.Params.AddValue('EndDate', Employees[i].MaxDate);
            Q.Result.First;
            ridx := Q.Result.RecordCount;
            while not Q.Result.Eof do
            begin
              Dec(ridx);
              DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DataRequired('EE_TIME_OFF_ACCRUAL_OPER_NBR='+
                Q.Result.FieldByName('EE_TIME_OFF_ACCRUAL_OPER_NBR').AsString);
              DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Edit;
              if DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.USED.AsFloat > Employees[i].Actual then
              begin
                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.USED.AsFloat := Employees[i].Actual;
                Employees[i].Actual := 0;
              end
              else
              if ridx = 0 then
              begin
                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.USED.AsFloat := Employees[i].Actual;
                Employees[i].Actual := 0;
              end
              else
              begin
                Employees[i].Actual := Employees[i].Actual - DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.USED.AsFloat;
              end;
              if DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.PR_NBR.IsNull then
              begin
                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.PR_NBR.AsInteger := Employees[i].MaxPrNbr;
                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.PR_BATCH_NBR.AsInteger := Employees[i].MaxBatchNbr;                
                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.PR_CHECK_NBR.AsInteger := Employees[i].MaxCheckNbr;
                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.OPERATION_CODE.AsString := EE_TOA_OPER_CODE_REQUEST_APPROVED;
              end;
              DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;
              ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER]);
              Q.Result.Next;
            end;
          end;
        end;
      end;
    except
      raise;
    end;
  finally
    TORequests.Filter := '';
    TORequests.Filtered := false;
    TORequests.Locate('EETimeOffAccrualNbr', Bookmark, []);
  end;
  RefreshData;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.RefreshData;
begin
  TORequests.DisableControls;
  try
    if TORequests.Active then TORequests.Close;
    if TOADetails.Active then TOADetails.Close;

    TORequests.CreateDataSet;
    TOADetails.CreateDataSet;

    if ( cbStatusPending.Checked
         or cbStatusApproved.Checked
         or cbStatusDenied.Checked
         or cbStatusUsed.Checked ) then
      populateDataSet;

    UpdateButtons;
  finally
    TORequests.EnableControls;
  end;
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.OnGetSecurityStates(
  const States: TSecurityStateList);
const
  secScreenRo: TAtomContextRecord = (Tag: ctEnabledSLA; Caption: 'LimitedAccess'; IconName: ''; Priority: 225);
begin
  with secScreenRo do
    States.InsertAfter(ctEnabled, Tag[1], Caption, IconName);
end;



procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.wwwdgTOACalcCellColors(
  Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  inherited;

  if (wwwdgTOA.DataSource <> nil) and  wwwdgTOA.DataSource.DataSet.Active and
    ((wwwdgTOA.DataSource.DataSet.FieldByName('statuscode').AsString = EE_TOA_OPER_CODE_REQUEST_DENIED) or
     (wwwdgTOA.DataSource.DataSet.FieldByName('statuscode').AsString = EE_TOA_OPER_CODE_USED)) then
       AFont.Color := clGray

end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.wwwdgTOACellChanged(
  Sender: TObject);
begin
  inherited;
  TevDBGrid(Sender).Columns[0].ReadOnly:=
    (wwwdgTOA.DataSource <> nil) and  wwwdgTOA.DataSource.DataSet.Active and
      ((wwwdgTOA.DataSource.DataSet.FieldByName('statuscode').AsString = EE_TOA_OPER_CODE_REQUEST_DENIED) or
       (wwwdgTOA.DataSource.DataSet.FieldByName('statuscode').AsString = EE_TOA_OPER_CODE_USED));
end;

procedure TEDIT_CO_ESSTIMEOFFMANAGEMENT.wwwdgTOAFieldChanged(
  Sender: TObject; Field: TField);
begin
  inherited;
  if Assigned(Field) and (LowerCase(Field.Name) = 'selected')  and
     (wwwdgTOA.DataSource.DataSet.FieldByName('EETimeOffAccrualNbr').AsInteger > 0) and
    ((wwwdgTOA.DataSource.DataSet.FieldByName('statuscode').AsString = EE_TOA_OPER_CODE_REQUEST_PENDING) or
     (wwwdgTOA.DataSource.DataSet.FieldByName('statuscode').AsString = EE_TOA_OPER_CODE_REQUEST_APPROVED)) then
  begin
     case wwwdgTOA.DataSource.DataSet.FieldByName('selected').AsString[1] of
       'Y': Inc(Fselectedcount);
       'N': Dec(Fselectedcount);
     end;
     BtnUpdate.Enabled := (FindSecurityElement(Self, ClassName).SecurityState <> ctEnabledSLA) and
                       not TORequests.IsEmpty  and (Fselectedcount = 1);
  end;
end;


initialization
  RegisterClass(TEDIT_CO_ESSTIMEOFFMANAGEMENT);
end.
