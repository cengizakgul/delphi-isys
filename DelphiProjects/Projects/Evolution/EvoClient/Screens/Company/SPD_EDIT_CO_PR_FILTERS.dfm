inherited EDIT_CO_PR_FILTERS: TEDIT_CO_PR_FILTERS
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 784
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 772
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 472
              Top = 160
              Width = 201
              Height = 368
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 255
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 205
                Height = 408
                IniAttributes.SectionName = 'TEDIT_CO_PR_FILTERS\wwdbgridSelectClient'
              end
            end
          end
        end
      end
    end
    object tsDetail: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbFilterDetail: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 182
        Align = alClient
        TabOrder = 0
        object fpCOPayrollFilerSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 818
          Height = 258
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdFilters: TevDBGrid
            Left = 12
            Top = 35
            Width = 785
            Height = 202
            DisableThemesInTitle = False
            Selected.Strings = (
              'NAME'#9'30'#9'Name'#9'F'
              'CUSTOM_DIVISION_NUMBER'#9'15'#9'Division'#9'F'
              'CUSTOM_BRANCH_NUMBER'#9'15'#9'Branch'#9'F'
              'CUSTOM_DEPARTMENT_NUMBER'#9'15'#9'Department'#9'F'
              'CUSTOM_TEAM_NUMBER'#9'15'#9'Team'#9'F'
              'PAY_GROUP_NAME'#9'30'#9'Pay Group'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_PR_FILTERS\grdFilters'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCOPayrollFilerDetails: TisUIFashionPanel
          Left = 8
          Top = 272
          Width = 818
          Height = 210
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Payroll Filter Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblName: TevLabel
            Left = 12
            Top = 35
            Width = 38
            Height = 13
            Caption = '~Name'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblDivision: TevLabel
            Left = 265
            Top = 35
            Width = 37
            Height = 13
            Caption = 'Division'
          end
          object txtDivName: TevDBText
            Left = 398
            Top = 54
            Width = 400
            Height = 13
            DataField = 'CUSTOM_DIVISION_NAME'
            DataSource = wwdsDetail
          end
          object txtBranchName: TevDBText
            Left = 398
            Top = 93
            Width = 400
            Height = 13
            DataField = 'CUSTOM_BRANCH_NAME'
            DataSource = wwdsDetail
          end
          object txtDeptName: TevDBText
            Left = 398
            Top = 132
            Width = 400
            Height = 13
            DataField = 'CUSTOM_DEPARTMENT_NAME'
            DataSource = wwdsDetail
          end
          object txtTeamName: TevDBText
            Left = 398
            Top = 171
            Width = 400
            Height = 13
            DataField = 'CUSTOM_TEAM_NAME'
            DataSource = wwdsDetail
          end
          object lblPayGroup: TevLabel
            Left = 12
            Top = 74
            Width = 50
            Height = 13
            Caption = 'Pay Group'
          end
          object lblTeam: TevLabel
            Left = 265
            Top = 152
            Width = 27
            Height = 13
            Caption = 'Team'
          end
          object lblDepartment: TevLabel
            Left = 265
            Top = 113
            Width = 55
            Height = 13
            Caption = 'Department'
          end
          object lblBranch: TevLabel
            Left = 265
            Top = 74
            Width = 34
            Height = 13
            Caption = 'Branch'
          end
          object edtName: TevDBEdit
            Left = 12
            Top = 50
            Width = 245
            Height = 21
            DataField = 'NAME'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object lcDivision: TevDBLookupCombo
            Left = 265
            Top = 50
            Width = 125
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_DIVISION_NUMBER'#9'20'#9'CUSTOM_DIVISION_NUMBER'#9'F'
              'NAME'#9'40'#9'NAME'#9'F')
            DataField = 'CO_DIVISION_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_DIVISION.CO_DIVISION
            LookupField = 'CO_DIVISION_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = lcDivisionCloseUp
          end
          object lcPayGroup: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 245
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
            DataField = 'CO_PAY_GROUP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_PAY_GROUP.CO_PAY_GROUP
            LookupField = 'CO_PAY_GROUP_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object lcTeam: TevDBLookupCombo
            Left = 265
            Top = 167
            Width = 125
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_TEAM_NUMBER'#9'20'#9'CUSTOM_TEAM_NUMBER'#9'F'
              'NAME'#9'40'#9'NAME'#9'F')
            DataField = 'CO_TEAM_NBR'
            DataSource = wwdsDetail
            LookupTable = CO_TEAM
            LookupField = 'CO_TEAM_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object lcDepartment: TevDBLookupCombo
            Left = 265
            Top = 128
            Width = 125
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_DEPARTMENT_NUMBER'#9'20'#9'CUSTOM_DEPARTMENT_NUMBER'#9'F'
              'NAME'#9'40'#9'NAME'#9'F')
            DataField = 'CO_DEPARTMENT_NBR'
            DataSource = wwdsDetail
            LookupTable = CO_DEPARTMENT
            LookupField = 'CO_DEPARTMENT_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = lcDepartmentCloseUp
          end
          object lcBranch: TevDBLookupCombo
            Left = 265
            Top = 89
            Width = 125
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BRANCH_NUMBER'#9'20'#9'CUSTOM_BRANCH_NUMBER'#9'F'
              'NAME'#9'40'#9'NAME'#9'F')
            DataField = 'CO_BRANCH_NBR'
            DataSource = wwdsDetail
            LookupTable = CO_BRANCH
            LookupField = 'CO_BRANCH_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = lcBranchCloseUp
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_PR_FILTERS.CO_PR_FILTERS
    OnDataChange = wwdsDetailDataChange
  end
  object dsDivision: TevDataSource
    DataSet = DM_CO_DIVISION.CO_DIVISION
    Left = 313
    Top = 69
  end
  object dsBranch: TevDataSource
    DataSet = CO_BRANCH
    MasterDataSource = dsDivision
    Left = 352
    Top = 69
  end
  object dsDepartment: TevDataSource
    DataSet = CO_DEPARTMENT
    MasterDataSource = dsBranch
    Left = 394
    Top = 69
  end
  object dsTeam: TevDataSource
    DataSet = CO_TEAM
    MasterDataSource = dsDepartment
    Left = 433
    Top = 69
  end
  object CO_BRANCH: TevClientDataSet
    Left = 355
    Top = 105
  end
  object CO_DEPARTMENT: TevClientDataSet
    Left = 391
    Top = 105
  end
  object CO_TEAM: TevClientDataSet
    Left = 430
    Top = 105
  end
end
