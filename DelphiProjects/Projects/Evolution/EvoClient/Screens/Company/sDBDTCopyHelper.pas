// Copyright � 2000-2004 iSystems LLC. All rights reserved.
//gdy
unit sDBDTCopyHelper;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  sCopyHelper, Menus,  ActnList, ImgList, sCopier,
  ISBasicClasses;

type
  TDBDTCopyHelper = class(TCopyHelper)
  private
    { Private declarations }
    FOnDeleteDetail: TProcessDBDTEvent;
    FOnCopyDetail: TProcessDBDTEvent;
  protected
    function CreateCopier: TCopier; override;
  public
    { Public declarations }
    property OnCopyDetail: TProcessDBDTEvent read FOnCopyDetail write FOnCopyDetail;
    property OnDeleteDetail: TProcessDBDTEvent read FOnDeleteDetail write FOnDeleteDetail;

  end;

implementation

{$R *.DFM}


function TDBDTCopyHelper.CreateCopier: TCopier;
begin
  Result := inherited CreateCopier;
  if not assigned( Result ) then
  begin
    Result := TDBDTCopier.Create( DataSet, Grid.SelectedList, UserFriendlyName );
    TDBDTCopier(Result).OnDeleteDetail :=  FOnDeleteDetail;
    TDBDTCopier(Result).OnCopyDetail := FOnCopyDetail;
    TDBDTCopier(Result).AddTablesToSave( FDSToSave ); //move to TCopyHelper
  end;
end;


end.
