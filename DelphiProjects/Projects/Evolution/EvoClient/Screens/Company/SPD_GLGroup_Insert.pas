// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_GLGroup_Insert;

interface

uses  Db, Wwdatsrc, StdCtrls, Controls, Grids, Wwdbigrd,
  Wwdbgrid, Classes, SDataStructure, Forms, SysUtils, Graphics, SPD_DBDTSelectionListFiltr,
  Buttons, Variants, ISBasicClasses, EvUIComponents, EvClientDataSet,
  isUIEdit, LMDCustomButton, LMDButton, isUILMDButton;

type
  TGLGroup_Insert = class(TForm)
    OKBtn: TevBitBtn;
    CancelBtn: TevBitBtn;
    lbLevelType: TListBox;
    lbDataType: TListBox;
    lbLevelValue: TevDBGrid;
    lbDataValue: TevDBGrid;
    edtGLMask: TevEdit;
    lblFormat: TevLabel;
    qlblLevelType: TevLabel;                 
    lblDataType: TevLabel;
    lblLevelValue: TevLabel;
    lblDataValue: TevLabel;
    dscLevelValue: TevDataSource;
    dscDataValue: TevDataSource;
    procedure FormCreate(Sender: TObject);
    procedure lbLevelTypeClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbDataTypeClick(Sender: TObject);
    procedure lbLevelValueMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbLevelValueKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure FilterRecord(DataSet: TDataSet; var Accept: Boolean);
  public
    slLevelType, slDataType: TStringList;
    { Public declarations }
  end;

implementation

uses
  EvConsts, SFieldCodeValues;

{$R *.DFM}

procedure TGLGroup_Insert.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  slLevelType := TStringList.Create;
  slDataType := TStringList.Create;
  slLevelType.Text := StringReplace(GL_Level_ComboChoices, #9, '=', [rfReplaceAll]);
  slDataType.Text := StringReplace(GL_Data_ComboChoices, #9, '=', [rfReplaceAll]);

  lbLevelType.Items.Text := GL_Level_ComboChoices;
  for i := 0 to Pred(lbLevelType.Items.Count) do
    lbLevelType.Items[i] := Copy(lbLevelType.Items[i], 1, Pred(Pos(#9, lbLevelType.Items[i])));
  lbLevelType.Selected[0] := True;
  lbDataType.Items.Text := GL_Data_ComboChoices;
  for i := 0 to Pred(lbDataType.Items.Count) do
    lbDataType.Items[i] := Copy(lbDataType.Items[i], 1, Pred(Pos(#9, lbDataType.Items[i])));
  lbDataType.Selected[0] := True;

  DM_COMPANY.CO_BRANCH.OnFilterRecord := FilterRecord;
  DM_COMPANY.CO_BRANCH.Filtered := True;
  DM_COMPANY.CO_DEPARTMENT.OnFilterRecord := FilterRecord;
  DM_COMPANY.CO_DEPARTMENT.Filtered := True;
  DM_COMPANY.CO_TEAM.OnFilterRecord := FilterRecord;
  DM_COMPANY.CO_TEAM.Filtered := True;
end;

procedure TGLGroup_Insert.lbLevelTypeClick(Sender: TObject);
var
  sLevelValue: string;
  cds: TevClientDataSet;
begin
  lbLevelValue.Enabled := (lbLevelType.SelCount = 1) and
                          (slLevelType.Values[lbLevelType.Items[lbLevelType.ItemIndex]] <> GL_LEVEL_NONE);
  if not lbLevelValue.Enabled then
  begin
    lbLevelValue.Color := clGray;
    Exit;
  end;
  sLevelValue := slLevelType.Values[lbLevelType.Items[lbLevelType.ItemIndex]];
  if sLevelValue = GL_LEVEL_DIVISION then
  begin
    dscLevelValue.DataSet := DM_COMPANY.CO_DIVISION;
    lbLevelValue.Selected.Text := 'CUSTOM_DIVISION_NUMBER'#9'15'#9'Division';
  end
  else if sLevelValue = GL_LEVEL_BRANCH then
  begin
    dscLevelValue.DataSet := FindComponent('Branch') as TevClientDataSet;
    if not Assigned(dscLevelValue.DataSet) then
    begin
      dscLevelValue.DataSet := TevClientDataSet.Create(Self);
      dscLevelValue.DataSet.Name := 'Branch';
      cds := dscLevelValue.DataSet as TevClientDataSet;
      SetupDBDT_DS(cds, DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT,
                          DM_COMPANY.CO_TEAM, CLIENT_LEVEL_BRANCH);
    end;
    lbLevelValue.Selected.Text := 'DBDT_CUSTOM_NUMBERS'#9'15'#9'Branch';
  end
  else if sLevelValue = GL_LEVEL_DEPT then
  begin
    dscLevelValue.DataSet := FindComponent('Department') as TevClientDataSet;
    if not Assigned(dscLevelValue.DataSet) then
    begin
      dscLevelValue.DataSet := TevClientDataSet.Create(Self);
      dscLevelValue.DataSet.Name := 'Department';
      cds := dscLevelValue.DataSet as TevClientDataSet;
      SetupDBDT_DS(cds, DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT,
                          DM_COMPANY.CO_TEAM, CLIENT_LEVEL_DEPT);
    end;
    lbLevelValue.Selected.Text := 'DBDT_CUSTOM_NUMBERS'#9'15'#9'Department';
  end
  else if sLevelValue = GL_LEVEL_TEAM then
  begin
    dscLevelValue.DataSet := FindComponent('Team') as TevClientDataSet;
    if not Assigned(dscLevelValue.DataSet) then
    begin
      dscLevelValue.DataSet := TevClientDataSet.Create(Self);
      dscLevelValue.DataSet.Name := 'Team';
      cds := dscLevelValue.DataSet as TevClientDataSet;
      SetupDBDT_DS(cds, DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH, DM_COMPANY.CO_DEPARTMENT,
                          DM_COMPANY.CO_TEAM, CLIENT_LEVEL_TEAM);
    end;
    lbLevelValue.Selected.Text := 'DBDT_CUSTOM_NUMBERS'#9'15'#9'Team';
  end
  else if sLevelValue = GL_LEVEL_EMPLOYEE then
  begin
    dscLevelValue.DataSet := DM_EMPLOYEE.EE;
    lbLevelValue.Selected.Text :=
      'CUSTOM_EMPLOYEE_NUMBER'#9'13'#9'Employee Code'#13#10 +
      'Employee_Name_Calculate'#9'35'#9'Employee Name'#13#10 +
      'SOCIAL_SECURITY_NUMBER'#9'13'#9'SSN';
  end
  else if sLevelValue = GL_LEVEL_JOB then
  begin
    dscLevelValue.DataSet := DM_COMPANY.CO_JOBS;
    lbLevelValue.Selected.Text := 'Description'#9'40'#9'Description';
  end;
  lbLevelValue.ApplySelected;
  lbLevelValue.Color := clWindow;
end;

procedure TGLGroup_Insert.FormDestroy(Sender: TObject);
begin
  slLevelType.Free;
  slDataType.Free;
  DM_COMPANY.CO_BRANCH.OnFilterRecord := nil;
  DM_COMPANY.CO_BRANCH.Filtered := False;
  DM_COMPANY.CO_DEPARTMENT.OnFilterRecord := nil;
  DM_COMPANY.CO_DEPARTMENT.Filtered := False;
  DM_COMPANY.CO_TEAM.OnFilterRecord := nil;
  DM_COMPANY.CO_TEAM.Filtered := False;
end;

procedure TGLGroup_Insert.lbDataTypeClick(Sender: TObject);
type
  TDatalistType = (dlUnassigned, dlNone, dlED, dlState, dlLocal, dlWorkersComp, dlSUIEE, dlSUIER, dlAgency);
var
  i: Integer;
  t: TDatalistType;

  function ReturnType(Value: string): TDatalistType;
  begin
    Result := dlUnassigned;
    if (Value = '') or
       (Value = GL_DATA_NONE) or
       (Value = GL_DATA_FEDERAL) or
       (Value = GL_DATA_ER_OASDI) or
       (Value = GL_DATA_EE_OASDI) or
       (Value = GL_DATA_EE_MEDICARE) or
       (Value = GL_DATA_ER_MEDICARE) or
       (Value = GL_DATA_FUI) or
       (Value = GL_DATA_EIC) or
       (Value = GL_DATA_BACKUP) or
       (Value = GL_DATA_FEDERAL_OFFSET) or
       (Value = GL_DATA_ER_OASDI_OFFSET) or
       (Value = GL_DATA_EE_OASDI_OFFSET) or
       (Value = GL_DATA_EE_MEDICARE_OFFSET) or
       (Value = GL_DATA_ER_MEDICARE_OFFSET) or
       (Value = GL_DATA_FUI_OFFSET) or
       (Value = GL_DATA_EIC_OFFSET) or
       (Value = GL_DATA_BACKUP_OFFSET) or
       (Value = GL_DATA_BILLING) or
       (Value = GL_DATA_BILLING_OFFSET) or
       (Value = GL_DATA_ER_MATCH_OFFSET) or
       (Value = GL_DATA_NET_PAY) or
       (Value = GL_DATA_TAX_IMPOUND) or
       (Value = GL_DATA_TAX_IMPOUND_OFFSET) or
       (Value = GL_DATA_TRUST_IMPOUND) or
       (Value = GL_DATA_TRUST_IMPOUND_OFFSET) or
       (Value = GL_DATA_3P_CHECK) or
       (Value = GL_DATA_3P_TAX) or
       (Value = GL_DATA_TAX_CHECK_OFFSET) or
       (Value = GL_DATA_TAX_CHECK) then
      Result := dlNone
    else if (Value = GL_DATA_AGENCY_CHECK) or
            (Value = GL_DATA_AGENCY_CHECK_OFFSET) then
      Result := dlAgency
    else if (Value = GL_DATA_WC_IMPOUND) or
            (Value = GL_DATA_WC_IMPOUND_OFFSET) then
      Result := dlWorkersComp
    else if (Value = GL_DATA_ED) or
            (Value = GL_DATA_ED_OFFSET) then
      Result := dlED
    else if (Value = GL_DATA_STATE) or
            (Value = GL_DATA_EE_SDI) or
            (Value = GL_DATA_ER_SDI) or
            (Value = GL_DATA_STATE_OFFSET) or
            (Value = GL_DATA_EE_SDI_OFFSET) or
            (Value = GL_DATA_ER_SDI_OFFSET) then
      Result := dlState
    else if (Value = GL_DATA_EE_SUI) or
            (Value = GL_DATA_EE_SUI_OFFSET) then
      Result := dlSUIEE
    else if (Value = GL_DATA_ER_SUI) or
            (Value = GL_DATA_ER_SUI_OFFSET) then
      Result := dlSUIER
    else if (Value = GL_DATA_LOCAL) or
            (Value = GL_DATA_LOCAL_OFFSET) then
      Result := dlLocal;
  end;
begin
  t := dlUnassigned;
  for i := 0 to Pred(lbDataType.Items.Count) do
    if lbDataType.Selected[i] then
      if t = dlUnassigned then
        t := ReturnType(slDataType.Values[lbDataType.Items[i]])
      else if t <> ReturnType(slDataType.Values[lbDataType.Items[i]]) then
      begin
        t := dlNone;
        Break;
      end;

  lbDataValue.Enabled := (t <> dlNone) and (t <> dlUnassigned);
  if not lbDataValue.Enabled then
  begin
    lbDataValue.Color := clGray;
    Exit;
  end
  else
    lbDataValue.Color := clWindow;

  if t = dlSUIEE then
  begin
    DM_COMPANY.CO_SUI.Filter := 'EE_Or_ER in (''' + GROUP_BOX_EE + ''', ''' + GROUP_BOX_EE_OTHER + ''')';
    DM_COMPANY.CO_SUI.Filtered := True;
    dscDataValue.DataSet := DM_COMPANY.CO_SUI;
    lbDataValue.Selected.Text := 'Sui_Name'#9'40'#9'SUI Name';
  end
  else if t = dlSUIER then
  begin
    DM_COMPANY.CO_SUI.Filter := 'EE_Or_ER in (''' + GROUP_BOX_ER + ''', ''' + GROUP_BOX_ER_OTHER + ''')';
    DM_COMPANY.CO_SUI.Filtered := True;
    dscDataValue.DataSet := DM_COMPANY.CO_SUI;
    lbDataValue.Selected.Text := 'Sui_Name'#9'40'#9'SUI Name';
  end
  else if t = dlAgency then
  begin
    dscDataValue.DataSet := DM_CLIENT.CL_AGENCY;
    lbDataValue.Selected.Text := 'Agency_Name'#9'40'#9'Agency Name';
  end
  else if t = dlWorkersComp then
  begin
    dscDataValue.DataSet := DM_COMPANY.CO_WORKERS_COMP;
    lbDataValue.Selected.Text := 'WORKERS_COMP_CODE'#9'5'#9'Code'#13#10 +
                                  'Description'#9'40'#9'Description';
  end
  else if t = dlED then
  begin
    dscDataValue.DataSet := DM_COMPANY.CO_E_D_CODES;
    lbDataValue.Selected.Text := 'ED_Lookup'#9'15'#9'ED Code'#13#10 +
                                  'CodeDescription'#9'40'#9'Description';
  end
  else if t = dlState then
  begin
    dscDataValue.DataSet := DM_COMPANY.CO_STATES;
    lbDataValue.Selected.Text := 'STATE'#9'2'#9'States';
  end
  else if t = dlLocal then
  begin
    dscDataValue.DataSet := DM_COMPANY.CO_LOCAL_TAX;
    lbDataValue.Selected.Text := 'LocalName'#9'20'#9'Name'#13#10 +
                                   'LocalState'#9'2'#9'State';
  end;
  lbDataValue.ApplySelected;
end;

procedure TGLGroup_Insert.lbLevelValueMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    with Sender as TevDBGrid do
    begin
      UnselectAll;  //SelectedList.Clear;
      Invalidate;
    end;
end;

procedure TGLGroup_Insert.lbLevelValueKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = Ord('C') then
    with Sender as TevDBGrid do
    begin
      UnselectAll;  //SelectedList.Clear;
      Invalidate;
      Key := 0;
    end;
end;

procedure TGLGroup_Insert.FilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  if DataSet.Name = 'CO_BRANCH' then
    Accept := not VarIsNull(DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', DataSet['CO_DIVISION_NBR'], 'CO_DIVISION_NBR'))
  else if DataSet.Name = 'CO_DEPARTMENT' then
    Accept := not VarIsNull(DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', DataSet['CO_BRANCH_NBR'], 'CO_BRANCH_NBR'))
  else if DataSet.Name = 'CO_TEAM' then
    Accept := not VarIsNull(DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', DataSet['CO_DEPARTMENT_NBR'], 'CO_DEPARTMENT_NBR'));
end;

initialization
  RegisterClass(TGLGROUP_INSERT);

end.
