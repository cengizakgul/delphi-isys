// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_SUI_LIABILITIES;

interface

uses
  SDataStructure,  SPD_EDIT_CO_TAX_LIABILITIES_BASE, StdCtrls,
  DBCtrls, Db,  Wwdatsrc, wwdbdatetimepicker, wwdbedit,
  wwdblook, ExtCtrls, Buttons, Mask, Wwdotdot, Wwdbcomb, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, Controls, Classes, SysUtils, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDDClasses, SDataDictclient, SDataDicttemp, ISBasicClasses , variants, EvUIComponents, EvUtils, EvClientDataSet,
  ImgList, isUIwwDBLookupCombo, isUIEdit, isUIwwDBEdit,
  isUIwwDBDateTimePicker, isUIDBMemo, isUIwwDBComboBox, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_SUI_LIABILITIES = class(TEDIT_CO_TAX_LIABILITIES_BASE)
    lablDue_Date: TevLabel;
    lablAdjustment_Type: TevLabel;
    lablStatus: TevLabel;
    lablTax_Type: TevLabel;
    lablAmount: TevLabel;
    wwcbAdjustment_Type: TevDBComboBox;
    wwcbStatus: TevDBComboBox;
    wwlcSUI: TevDBLookupCombo;
    wwedAmount: TevDBEdit;
    wwdpDue_Date: TevDBDateTimePicker;
    fpSUIRight: TisUIFashionPanel;
    procedure fpSUIRightResize(Sender: TObject);
    procedure Panel3Resize(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
    procedure ReCalcTotals; override;
    procedure Activate; override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses SPD_EDIT_CO_BASE;

{$R *.DFM}

procedure TEDIT_CO_SUI_LIABILITIES.Activate;
begin
  inherited;
  wwcbStatus.OnDropDown := wwcbStatusDropDown;
  wwcbStatus.OnCloseUp := wwcbStatusCloseUp;
end;

constructor TEDIT_CO_SUI_LIABILITIES.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENTS
  pnlSubbrowse.Parent := fpSUIRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_SUI_LIABILITIES.fpSUIRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpSUIRight.Width - 40;
  pnlSubbrowse.Height := fpSUIRight.Height - 65;
end;

procedure TEDIT_CO_SUI_LIABILITIES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_SUI, aDS);
  inherited;
end;

function TEDIT_CO_SUI_LIABILITIES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_SUI_LIABILITIES;
end;

function TEDIT_CO_SUI_LIABILITIES.GetInsertControl: TWinControl;
begin
  Result := wwlcSUI;
end;

procedure TEDIT_CO_SUI_LIABILITIES.Panel3Resize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 MOVING AROUND
  cbShowCredit.Left := Panel3.Width - 140;
  cbGroup.Left      := cbShowCredit.Left - 115;
end;

procedure TEDIT_CO_SUI_LIABILITIES.ReCalcTotals;
var
  TotalTax: Currency;
begin
  TotalTax := 0;
  with wwdsDetail.DataSet do
  begin
    First;
    DisableControls;
    while not EOF do
    begin
                //  Added By CA...  If any record of AMOUNT has null,  skip it
      if not VarIsNull(wwdsDetail.DataSet.FieldByName('AMOUNT').Value) then
         TotalTax := TotalTax + wwdsDetail.DataSet.FieldByName('AMOUNT').Value;
         
      Next;
    end;
    First;
    EnableControls;
  end;
  lablTotalTaxValue.Caption := CurrToStrF(TotalTax, ffCurrency, 2);
end;

initialization
  RegisterClass(TEDIT_CO_SUI_LIABILITIES);

end.
