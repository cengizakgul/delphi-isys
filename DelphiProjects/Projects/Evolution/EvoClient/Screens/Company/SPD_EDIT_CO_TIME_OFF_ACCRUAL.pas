// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_TIME_OFF_ACCRUAL;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, wwdbdatetimepicker,
  StdCtrls, ExtCtrls, DBCtrls, Wwdbspin, wwdbedit, Mask, Wwdotdot,
  Wwdbcomb, wwdblook, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, Controls, Classes, SFieldCodeValues, EvConsts, Dialogs, EvSecElement,
  Forms, sCompanyCopyHelper, sCopyHelper, Variants, isBasicUtils,
  SDDClasses, EvTypes, CheckLst, ISBasicClasses, SDataDictclient,
  SDataDicttemp, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBEdit, isUIwwDBComboBox,
  isUIwwDBLookupCombo, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_TIME_OFF_ACCRUAL = class(TEDIT_CO_BASE)
    Tab2: TTabSheet;
    wwDBGrid1: TevDBGrid;
    DBLookGridFrequency: TevDBComboBox;
    CopyHelper: TCompanyCopyHelper;
    tsCaps: TTabSheet;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpCOTOASummary: TisUIFashionPanel;
    fpCOTOADetails: TisUIFashionPanel;
    Label8: TevLabel;
    DBEditDescription: TevDBEdit;
    Label1: TevLabel;
    DBLookCL_E_DS_NBR: TevDBLookupCombo;
    Label2: TevLabel;
    DBLookCL_E_D_GROUP_NBR: TevDBLookupCombo;
    Label3: TevLabel;
    DBCboFrequency: TevDBComboBox;
    Label6: TevLabel;
    DBCboPayrollOfTheMonth: TevDBComboBox;
    Label7: TevLabel;
    DbCboCalcMethod: TevDBComboBox;
    evDBRadioGroup1: TevDBRadioGroup;
    wwcbProcessOrder: TevDBComboBox;
    evLabel12: TevLabel;
    wwcbBalanceOnCheck: TevDBComboBox;
    lablBalanceOnCheck: TevLabel;
    evLabel2: TevLabel;
    DBSpinAccrualMonthNumber: TevDBSpinEdit;
    Label4: TevLabel;
    DBLookCoHrAttendType: TevDBLookupCombo;
    evLabel13: TevLabel;
    DBLookUSAGE_CL_E_D_GROUP_NBR: TevDBLookupCombo;
    evLabel3: TevLabel;
    DBRadioGroupAccrualActive: TevDBRadioGroup;
    DBRadioGroupAutoCreateOnNewHire: TevDBRadioGroup;
    Label18: TevLabel;
    DBEditDivisor: TevDBEdit;
    Label19: TevLabel;
    DBEditDivisorDescription: TevDBEdit;
    evBitBtn1: TevBitBtn;
    evDBRadioGroup2: TevDBRadioGroup;
    clbAutoCreateStatuses: TevCheckListBox;
    evLabel14: TevLabel;
    sbCapRollover: TScrollBox;
    fpCOTOACaps: TisUIFashionPanel;
    Label9: TevLabel;
    evLabel1: TevLabel;
    Label10: TevLabel;
    Label11: TevLabel;
    evLabel8: TevLabel;
    EvBevel1: TEvBevel;
    DBSpinAnnualMaxHours: TevDBSpinEdit;
    DBSpinMinimumHours: TevDBSpinEdit;
    DBSpinProbationPeriodMonths: TevDBSpinEdit;
    edMaxHoursToAccrueOn: TevDBSpinEdit;
    fpCOTOARollover: TisUIFashionPanel;
    lablRollover_Type: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel7: TevLabel;
    evLabel6: TevLabel;
    lablRollover_Date: TevLabel;
    wwlcRollover_Type: TevDBLookupCombo;
    edRolloverFreq: TevDBComboBox;
    edRolloverPayrollOfTheMonth: TevDBComboBox;
    edRolloverOffset: TevDBSpinEdit;
    edRolloverMonthNumber: TevDBSpinEdit;
    edRolloverDate: TevDBDateTimePicker;
    fpCOTOAReset: TisUIFashionPanel;
    Label16: TevLabel;
    evLabel9: TevLabel;
    evLabel11: TevLabel;
    evLabel10: TevLabel;
    Label17: TevLabel;
    DBCboAnnualResetCode: TevDBComboBox;
    evDBComboBox1: TevDBComboBox;
    evDBSpinEdit1: TevDBSpinEdit;
    evDBSpinEdit2: TevDBSpinEdit;
    DBDateResetDate: TevDBDateTimePicker;
    fpCOTOADesc: TisUIFashionPanel;
    lTimeOffDesc: TevLabel;
    sbDetails: TScrollBox;
    rgShowEDinESS: TevDBRadioGroup;
    edtAnnualAccural: TevDBEdit;
    Label70: TevLabel;
    evLabel15: TevLabel;
    edtAnnualUsage: TevDBEdit;
    evLabel16: TevLabel;
    edtDefaultESS: TevDBEdit;
    edMaximumHoursToAccure: TevDBSpinEdit;
    edUseBalance: TevDBRadioGroup;
    edRoundDigits: TevDBSpinEdit;
    procedure DBCboFrequencyChange(Sender: TObject);
    procedure DbCboCalcMethodChange(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure evBitBtn1Click(Sender: TObject);
    procedure clbAutoCreateStatusesClickCheck(Sender: TObject);
  private
    procedure HandleCopy(Sender: TObject; client, company: integer;
      selectedDetails, details: TEvClientDataSet);
    procedure HandleDelete(Sender: TObject; client, company: integer;
      selectedDetails, details: TEvClientDataSet);
    procedure PopulateAutoCreate;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function GetInsertControl: TWinControl; override;
    procedure AfterClick(Kind: Integer); override;
    procedure Activate; override;
  end;

implementation

uses
  SPackageEntry, evutils, sysutils, sCopier, SPD_EDIT_Open_BASE,
  SFrameEntry;
{$R *.DFM}

//gdy
procedure TEDIT_CO_TIME_OFF_ACCRUAL.Activate;
var
  dummy: boolean;
begin
  PopulateAutoCreate;

  wwlcRollover_Type.LookupTable := TevClientDataSet.Create(Self);
  TevClientDataSet(wwlcRollover_Type.LookupTable).ProviderName := DM_COMPANY.CO_TIME_OFF_ACCRUAL.ProviderName;
  inherited;
  CopyHelper.Grid := wwDBGrid1; //gdy
  CopyHelper.UserFriendlyName := 'Time off accrual'; //gdy
  CopyHelper.OnCopyDetail := HandleCopy; //gdy
  CopyHelper.OnDeleteDetail := HandleDelete; //gdy
  GetDataSetsToReopen( CopyHelper.FDSToSave,  dummy );
//  CopyHelper.SetDSToSave( [DM_CLIENT.CL_E_D_GROUPS, DM_CLIENT.CL_E_DS ] ); //gdy
end;


procedure TEDIT_CO_TIME_OFF_ACCRUAL.DBCboFrequencyChange(Sender: TObject);
var
  iMethod: string[1];

  procedure Process;
  begin
    //added the test for SecurityRO here because the code that follows
    //created undesireable behaviors on this field.
    if DBSpinAccrualMonthNumber.SecurityRO then exit;
    if (iMethod = TIME_OFF_ACCRUAL_FREQ_PER_PAY_PERIOD)
      or (iMethod = TIME_OFF_ACCRUAL_FREQ_NONTHLY) then
    begin
      DBSpinAccrualMonthNumber.Enabled := False;
      DBSpinAccrualMonthNumber.MaxValue := 1;
    end
    else
      if (iMethod = TIME_OFF_ACCRUAL_FREQ_QUARTERLY) then
    begin
      DBSpinAccrualMonthNumber.Enabled := True;
      DBSpinAccrualMonthNumber.MaxValue := 3;
    end
    else
      if (iMethod = TIME_OFF_ACCRUAL_FREQ_SEMI_ANNUAL) then
    begin
      DBSpinAccrualMonthNumber.Enabled := True;
      DBSpinAccrualMonthNumber.MaxValue := 6;
    end
    else
      if (iMethod = TIME_OFF_ACCRUAL_FREQ_ANNUAL) then
    begin
      DBSpinAccrualMonthNumber.Enabled := True;
      DBSpinAccrualMonthNumber.MaxValue := 12;
    end;
    if DBSpinAccrualMonthNumber.Value > DBSpinAccrualMonthNumber.MaxValue then
      DBSpinAccrualMonthNumber.Value := DBSpinAccrualMonthNumber.MaxValue;
    {if IsModified and ((iMethod <> TIME_OFF_ACCRUAL_FREQ_ANNUAL) and (iMethod <>
      TIME_OFF_ACCRUAL_FREQ_PER_PAY_PERIOD)) and (DBCboPayrollOfTheMonth.Value = TIME_OFF_ACCRUAL_PAYROLL_OF_YEAR_HIREDATE) then
      DBCboPayrollOfTheMonth.Value := TIME_OFF_ACCRUAL_PAYROLL_OF_MONTH_FIRST;}
  end;
begin
  iMethod := DBCboFrequency.Value;
  Process;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_COMPANY.CO_HR_ATTENDANCE_TYPES, aDS);
  AddDS(TevClientDataSet(wwlcRollover_Type.LookupTable), aDS);
  inherited;
end;

function TEDIT_CO_TIME_OFF_ACCRUAL.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_TIME_OFF_ACCRUAL;
end;

function TEDIT_CO_TIME_OFF_ACCRUAL.GetInsertControl: TWinControl;
begin
  Result := DBEditDescription;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL.DbCboCalcMethodChange(Sender: TObject);
begin
  inherited;
  edUseBalance.Visible := (DbCboCalcMethod.Value = TIME_OFF_ACCRUAL_CALC_METHOD_ACCRUE_HOURS)
    or (DbCboCalcMethod.Value = TIME_OFF_ACCRUAL_CALC_METHOD_ACCRUE_BY_FREQ)
    or (DbCboCalcMethod.Value = TIME_OFF_ACCRUAL_CALC_METHOD_COLUMBIA_EDP);
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
var
  i: Integer;
begin
  inherited;
  if not Assigned(Field)
  and Assigned(wwdsDetail.DataSet)
  and wwdsDetail.DataSet.Active then
  begin
    lTimeOffDesc.Caption := wwdsDetail.DataSet.FieldByName('DESCRIPTION').AsString;

    PopulateAutoCreate;
    if wwdsDetail.DataSet.State = dsBrowse then
      for i := 0 to Pred(clbAutoCreateStatuses.Items.Count) do
        clbAutoCreateStatuses.Checked[i] := DM_CLIENT.CO_TIME_OFF_ACCRUAL.AUTO_CREATE_FOR_STATUSES.IsNull or
                   (Pos(Char(clbAutoCreateStatuses.Items.Objects[i]), DM_CLIENT.CO_TIME_OFF_ACCRUAL.AUTO_CREATE_FOR_STATUSES.Value) > 0);
  end;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL.evBitBtn1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO_TIME_OFF_ACCRUAL_RATES')
end;

//end gdy
procedure TEDIT_CO_TIME_OFF_ACCRUAL.HandleCopy(Sender: TObject; client,
  company: integer; selectedDetails, details: TEvClientDataSet);
var
  msg: string;
  cleds: integer;
  v: Variant;
  k: integer;
  fn: string;
begin

  DM_CLIENT.CL_E_DS.EnsureDSCondition(''  );
//  ODS('looking for <%s>',[VarToStr(selectedDetails['CustomEDCodeNumber'])]);
  v := DM_CLIENT.CL_E_DS.Lookup( 'CUSTOM_E_D_CODE_NUMBER', selectedDetails['CustomEDCodeNumber'], 'CL_E_DS_NBR');
  if not VarIsNull( v ) then
  begin
    cleds := v;
//    ODS( 'found: <%s>',[Vartostr(DM_CLIENT.CL_E_DS.Lookup( 'CL_E_DS_NBR', cleds, 'CUSTOM_E_D_CODE_NUMBER')) ]);
//Andrei's db bug fix
    DM_COMPANY.CO_E_D_CODES.EnsureDSCondition('CO_NBR='+IntToStr(company) );
    v := DM_COMPANY.CO_E_D_CODES.Lookup( 'CL_E_DS_NBR', cleds, 'CO_E_D_CODES_NBR'{anything});
    if not VarIsNull( v ) then
    begin
      DM_CLIENT.CL_E_D_GROUPS.EnsureDSCondition('');
      v := DM_CLIENT.CL_E_D_GROUPS.Lookup( 'NAME', selectedDetails['EDGroupName'], 'CL_E_D_GROUPS_NBR');

      if details.Locate('DESCRIPTION', VarArrayOf([selecteddetails.FieldByNAme('DESCRIPTION').AsString]), []) then
        details.Edit
      else
        details.Insert;
      for k := 0 to SelectedDetails.Fields.Count - 1 do
      begin
        fn := Selecteddetails.Fields[k].FieldName;
        if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) and ( fn <> 'CL_E_DS_NBR' ) and ( fn <> 'CL_E_D_GROUPS_NBR' ) then
          details[fn] := selectedDetails[fn];
      end;
      details['CO_NBR'] := company;
      details['CL_E_DS_NBR'] := cleds;
      if not VarIsNull( v ) then
        details['CL_E_D_GROUPS_NBR'] := v;
      details.Post;
    end
    else
    begin
      msg := Format( 'Cannot copy %s to company %s because E/D %s doesn''t exist( it exists on client level )',
             [ VarToStr(selectedDetails['DESCRIPTION']),
               (Sender as TCompanyDetailCopier).TargetCompanyNumber,
               VarToStr( selectedDetails['CustomEDCodeNumber'] ) ] );
      EvErrMessage(msg);
    end;
  end
  else
  begin
    msg := Format( 'Cannot copy %s to company %s because E/D %s doesn''t exist',
           [ VarToStr(selectedDetails['DESCRIPTION']),
             (Sender as TCompanyDetailCopier).TargetCompanyNumber,
             VarToStr( selectedDetails['CustomEDCodeNumber'] ) ] );
    EvErrMessage(msg);
  end;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL.HandleDelete(Sender: TObject; client,
  company: integer; selectedDetails, details: TEvClientDataSet);
begin
  if details.Locate('DESCRIPTION', VarArrayOf([selecteddetails.FieldByNAme('DESCRIPTION').AsString]), []) then
    details.Delete;
end;
//end gdy

procedure TEDIT_CO_TIME_OFF_ACCRUAL.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = NavCommit then
    TevClientDataSet(wwlcRollover_Type.LookupTable).Data := DM_COMPANY.CO_TIME_OFF_ACCRUAL.Data;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind = NavOK then
  begin
    if (edRolloverFreq.Value <> TIME_OFF_ACCRUAL_FREQ_NONE) and
       (wwlcRollover_Type.Value = '') then
    begin
      wwlcRollover_Type.Show;
      if wwlcRollover_Type.CanFocus then
        wwlcRollover_Type.SetFocus;
      raise EUpdateError.CreateHelp('Rollover Into must have a value if Rollover Frequency is set.', IDH_ConsistencyViolation);
    end;

    if (Trim(edtDefaultESS.Text) <> '') then
    begin
      if  wwdsDetail.DataSet.State in [dsInsert,dsEdit] then
          wwdsDetail.DataSet.UpdateRecord;

      if (wwdsDetail.DataSet.FieldByName('DEFAULT_HOURS_FOR_ESS').AsFloat < 0) or
         (wwdsDetail.DataSet.FieldByName('DEFAULT_HOURS_FOR_ESS').AsFloat >=25) then
       raise EUpdateError.CreateHelp('Default Hours in Day for ESS must be between 0 and 24.', IDH_ConsistencyViolation);
    end;
  end;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL.clbAutoCreateStatusesClickCheck(Sender: TObject);
var
  i: Integer;
  s: string;
  bAll: Boolean;
begin
  inherited;
  if DM_CLIENT.CO_TIME_OFF_ACCRUAL.State = dsBrowse then
    DM_CLIENT.CO_TIME_OFF_ACCRUAL.Edit;
  s := '';
  bAll := True;
  for i := 0 to Pred(clbAutoCreateStatuses.Items.Count) do
    if clbAutoCreateStatuses.Checked[i] then
      s := s + Char(clbAutoCreateStatuses.Items.Objects[i])
    else
      bAll := False;
  if bAll or (s = '') then
    DM_CLIENT.CO_TIME_OFF_ACCRUAL.AUTO_CREATE_FOR_STATUSES.Clear
  else
    DM_CLIENT.CO_TIME_OFF_ACCRUAL.AUTO_CREATE_FOR_STATUSES.Value := s;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL.PopulateAutoCreate;
var
  s, s1: string;
  s2: Pointer;
begin
  if clbAutoCreateStatuses.Items.Count = 0 then
  begin
    s := PositionStatus_ComboChoices;
    repeat
      s1 := GetNextStrValue(s, #9);
      s2 := Pointer(Ord(GetNextStrValue(s, #13)[1]));
      clbAutoCreateStatuses.AddItem(s1, s2);
    until s = '';
  end;
end;

initialization
  RegisterClass(TEDIT_CO_TIME_OFF_ACCRUAL);

end.
