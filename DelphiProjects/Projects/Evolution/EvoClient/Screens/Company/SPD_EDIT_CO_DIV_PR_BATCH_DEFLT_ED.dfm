inherited EDIT_CO_DIV_PR_BATCH_DEFLT_ED: TEDIT_CO_DIV_PR_BATCH_DEFLT_ED
  inherited Panel1: TevPanel
    object Label2: TevLabel [0]
      Left = 544
      Top = 17
      Width = 37
      Height = 13
      Caption = 'Division'
    end
    object DBText2: TevDBText [1]
      Left = 584
      Top = 17
      Width = 217
      Height = 17
      DataField = 'NAME'
      DataSource = wwdsSubMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited pnlTopLeft: TevPanel
      inherited DBText1: TevDBText
        Width = 217
      end
      inherited pnlUser: TevPanel
        Left = 376
        Width = 198
      end
    end
  end
  inherited PageControl1: TevPageControl
    HelpContext = 42501
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 300
                    Align = alLeft
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 288
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 306
                    Width = 484
                    Align = alClient
                    Visible = True
                    object fpDivisionRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 472
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Caption = 'fpDivisionRight'
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Division'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 372
              Width = 347
              Height = 257
              Visible = True
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 320
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 428
                IniAttributes.SectionName = 'TEDIT_CO_DIV_PR_BATCH_DEFLT_ED\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 48
              Align = alClient
              Visible = True
              Title = 'Division'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 488
                Height = 428
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'DIVISION')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_DIV_PR_BATCH_DEFLT_ED\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      HelpContext = 42501
      Caption = 'Details'
      ImageIndex = 2
      object sbDivisionDetail: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 182
        Align = alClient
        TabOrder = 0
        object fpSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 718
          Height = 200
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwDBGrid2: TevDBGrid
            Left = 12
            Top = 35
            Width = 683
            Height = 142
            DisableThemesInTitle = False
            Selected.Strings = (
              'ED_lookup'#9'20'#9'Co E/D Codes'#9'F'
              'SALARY_HOURLY_OR_BOTH'#9'1'#9'Salary, Hourly'#9'F'
              'OVERRIDE_RATE'#9'10'#9'Override Rate'#9'F'
              'OVERRIDE_EE_RATE_NUMBER'#9'10'#9'Override EE Rate Nbr'#9'F'
              'OVERRIDE_HOURS'#9'10'#9'Override Hours'#9'F'
              'OVERRIDE_AMOUNT'#9'10'#9'Override Amount'#9'F'
              'OVERRIDE_LINE_ITEM_DATE'#9'10'#9'Override Line Item Date'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_DIV_PR_BATCH_DEFLT_ED\wwDBGrid2'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpDetail: TisUIFashionPanel
          Left = 8
          Top = 216
          Width = 375
          Height = 134
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDetail'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'E/D Code Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object UpBtn: TevSpeedButton
            Left = 12
            Top = 89
            Width = 116
            Height = 22
            Caption = 'Move Up'
            HideHint = True
            AutoSize = False
            OnClick = UpBtnClick
            NumGlyphs = 2
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFDDDDDDCCCCCCCCCCCCDDDDDDFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDCC
              CCCCCCCCCCDDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF43A277008A4A008A4A43A277FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9695957C
              7B7B7C7B7B969595FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00894900E7A700E7A7008949FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B7B7BD5
              D5D5D5D5D57B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884708E2A600E0A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7A7979D1
              D1D1CECECE7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00874716E0AC00DCA2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D1
              D1D1CCCCCC7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00874623E0B200D9A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D2
              D2D1CAC9C97A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884630DFB700D5A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D2
              D2D1C6C6C67A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088453EDFBD00D2A1008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D3
              D3D3C4C3C37A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088454BE0C100CFA1008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D5
              D5D5C1C1C17A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884459E1C800CBA2008946FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D7
              D7D7BEBDBD7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFD9D9D9CCCCCC0087426AE2CC00C9A1008744CCCCCCD9D9D9FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D9D9CCCCCC787877D9
              D9D9BCBCBB787877CCCCCCD9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF329C6B00894600AE7F00C39E00C39E00AE80008946329C6BFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8F7A7979A1A0A0B7
              B6B6B7B6B6A1A0A07A79798F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF008C4A82DCCA00C1A000BE9B00BE9B00C1A083DCCA008C4AFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7D7C7CD5D5D5B6B6B5B2
              B2B2B2B2B2B6B6B5D5D5D57D7C7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF0088448EDBCB00B99D00B99D8EDBCB008844FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D5D5D5AF
              AFAFAFAFAFD5D5D5797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0087429AE0D39AE0D3008742FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF787877DB
              DADADBDADA787877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008A47008A47FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B
              7B7B7B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ShortCut = 40998
          end
          object DownBtn: TevSpeedButton
            Left = 236
            Top = 89
            Width = 116
            Height = 22
            Caption = 'Move Down'
            HideHint = True
            AutoSize = False
            OnClick = DownBtnClick
            NumGlyphs = 2
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC
              CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFCCCCCC008B4B008B4BCCCCCCFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC7D
              7C7C7D7C7CCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFCCCCCC00884800C38200C382008848CCCCCCFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC7A7979B2
              B2B2B2B2B27A7979CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCCCCC00884700BD7E00DD9F00DD9F00BD7E008847CCCCCCFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC7A7979ACACACCC
              CCCCCCCCCCACACAC7A7979CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF008C4B53D8B053EAC400D59C00D59C53EAC453D8B0008C4BFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7D7C7CCCCCCCDEDEDEC5
              C5C5C5C5C5DEDEDECCCCCC7D7C7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF3AA77500884600B97E00D39D00D39E00B97E0088473AA775FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9A9A9A797979AAA9A9C4
              C3C3C4C3C3AAA9A97A79799A9A9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0086441BD9AC00D4A1008644FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF787877CB
              CBCBC5C5C5787877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088462ADBB400D1A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979CE
              CECEC3C2C27A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088453ADBBB00CEA3008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D0
              D0D0C0C0C07A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884549DCC100CAA2008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D2
              D2D1BEBDBD7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884458DDC700C7A2008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D4
              D4D4BBBBBB7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884467E0CD00C3A1008A46FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D8
              D8D8B8B7B77B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884476E1D300BFA0008A46FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979DB
              DADAB4B4B47B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884486E4DA00BBA1008A46FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979DF
              DFDEB2B1B17B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF008A4698EAE667DCD4008A47FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B7B7BE7
              E7E6D7D6D67B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF4FB183008A47008A474FB183FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4A4A47B
              7B7B7B7B7BA4A4A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            ShortCut = 41000
          end
          object Label1: TevLabel
            Left = 12
            Top = 35
            Width = 57
            Height = 13
            Caption = '~E/D Code'
            FocusControl = wwDBLookupCombo1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 157
            Height = 21
            HelpContext = 42501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ED_Lookup'#9'6'#9'ED_Lookup'
              'CodeDescription'#9'20'#9'CodeDescription')
            DataField = 'CO_E_D_CODES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_E_D_CODES.CO_E_D_CODES
            LookupField = 'CO_E_D_CODES_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object DBRadioGroup1: TevDBRadioGroup
            Left = 177
            Top = 35
            Width = 175
            Height = 37
            HelpContext = 42501
            Caption = '~EE Type'
            Columns = 3
            DataField = 'SALARY_HOURLY_OR_BOTH'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Salary'
              'Hourly'
              'Both')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'S'
              'H'
              'B')
          end
        end
        object fpOverrides: TisUIFashionPanel
          Left = 391
          Top = 216
          Width = 335
          Height = 134
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpOverrides'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Overrides'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblJob: TevLabel
            Left = 217
            Top = 35
            Width = 17
            Height = 13
            Caption = 'Job'
            FocusControl = lcJob
          end
          object Label3: TevLabel
            Left = 12
            Top = 74
            Width = 23
            Height = 13
            Caption = 'Rate'
            FocusControl = DBEdit3
          end
          object Label4: TevLabel
            Left = 116
            Top = 74
            Width = 60
            Height = 13
            Caption = 'EE Rate Nbr'
            FocusControl = DBEdit4
          end
          object Label6: TevLabel
            Left = 12
            Top = 35
            Width = 28
            Height = 13
            Caption = 'Hours'
            FocusControl = DBEdit5
          end
          object Label7: TevLabel
            Left = 116
            Top = 35
            Width = 36
            Height = 13
            Caption = 'Amount'
            FocusControl = DBEdit6
          end
          object Label8: TevLabel
            Left = 217
            Top = 74
            Width = 69
            Height = 13
            Caption = 'Line Item Date'
            FocusControl = DBEdit7
          end
          object lcJob: TevDBLookupCombo
            Left = 217
            Top = 50
            Width = 93
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
            DataField = 'CO_JOBS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_JOBS.CO_JOBS
            LookupField = 'CO_JOBS_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object DBEdit3: TevDBEdit
            Left = 12
            Top = 89
            Width = 93
            Height = 21
            HelpContext = 42501
            DataField = 'OVERRIDE_RATE'
            DataSource = wwdsDetail
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object DBEdit4: TevDBEdit
            Left = 116
            Top = 89
            Width = 93
            Height = 21
            HelpContext = 42501
            DataField = 'OVERRIDE_EE_RATE_NUMBER'
            DataSource = wwdsDetail
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object DBEdit5: TevDBEdit
            Left = 12
            Top = 50
            Width = 93
            Height = 21
            HelpContext = 42501
            DataField = 'OVERRIDE_HOURS'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object DBEdit6: TevDBEdit
            Left = 116
            Top = 50
            Width = 93
            Height = 21
            HelpContext = 42501
            DataField = 'OVERRIDE_AMOUNT'
            DataSource = wwdsDetail
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object DBEdit7: TevDBEdit
            Left = 217
            Top = 89
            Width = 93
            Height = 21
            HelpContext = 42501
            DataField = 'OVERRIDE_LINE_ITEM_DATE'
            DataSource = wwdsDetail
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 18
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_DIV_PR_BATCH_DEFLT_ED.CO_DIV_PR_BATCH_DEFLT_ED
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = wwdsSubMaster
    Top = 58
  end
  inherited dsCL: TevDataSource
    Top = 50
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_CO_DIVISION.CO_DIVISION
    MasterDataSource = wwdsMaster
  end
end
