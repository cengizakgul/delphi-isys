inherited EDIT_CO_LOCAL_TAX_LIABILITIES: TEDIT_CO_LOCAL_TAX_LIABILITIES
  HelpContext = 49501
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited splEDIT_CO_BASE: TevSplitter
                    Left = 237
                    Width = 3
                    Visible = True
                  end
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 231
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 219
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 240
                    Width = 550
                    Visible = True
                    object fpLocalRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 538
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Caption = 'fpLocalRight'
                      Color = 14737632
                      TabOrder = 0
                      OnResize = fpLocalRightResize
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Locals'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            OnResize = Panel3Resize
            inherited cbGroup: TevCheckBox
              Left = 435
              HelpContext = 10002
            end
            inherited cbShowCredit: TevCheckBox
              Left = 550
            end
            object cbkWorkLocation: TevCheckBox
              Left = 685
              Top = 8
              Width = 129
              Height = 17
              Caption = 'Show Location'
              TabOrder = 3
              OnClick = cbkWorkLocationClick
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 294
                IniAttributes.SectionName = 'TEDIT_CO_LOCAL_TAX_LIABILITIES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Locals'
              inherited Splitter2: TevSplitter
                Height = 294
              end
              inherited wwDBGrid2x: TevDBGrid
                Width = 130
                Height = 294
                ControlType.Strings = (
                  'ADJUSTMENT_TYPE;CustomEdit;lcbAdjType;F')
                Selected.Strings = (
                  'LOCAL_NAME'#9'10'#9'Name'#9'F'
                  'LOCAL_STATE'#9'2'#9'State'#9'F'
                  'AMOUNT'#9'12'#9'Amount'#9'F'
                  'DUE_DATE'#9'10'#9'Due date'#9'F'
                  'CHECK_DATE'#9'10'#9'Check date'#9'F'
                  'ADJUSTMENT_TYPE'#9'10'#9'Adj. type'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_LOCAL_TAX_LIABILITIES\wwDBGrid2x'
              end
              inherited wwDBGrid1x: TevDBGrid
                Height = 294
                Selected.Strings = (
                  'CHECK_DATE'#9'10'#9'Check Date'#9'F'
                  'RUN_NUMBER'#9'5'#9'Run #'#9'F'
                  'SCHEDULED_PROCESS_DATE'#9'10'#9'Scheduled Process Date'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_LOCAL_TAX_LIABILITIES\wwDBGrid1x'
              end
              object wwDBGrid3x: TevDBGrid
                Left = 174
                Top = 48
                Width = 130
                Height = 294
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'ADJUSTMENT_TYPE;CustomEdit;lcbAdjType;F')
                Selected.Strings = (
                  'LOCAL_NAME'#9'10'#9'Name'#9'F'
                  'ACCOUNT_NUMBER'#9'30'#9'Account Number'#9'F'
                  'LOCAL_STATE'#9'2'#9'State'#9'F'
                  'AMOUNT'#9'12'#9'Amount'#9'F'
                  'DUE_DATE'#9'10'#9'Due date'#9'F'
                  'CHECK_DATE'#9'10'#9'Check date'#9'F'
                  'ADJUSTMENT_TYPE'#9'10'#9'Adj. type'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_LOCAL_TAX_LIABILITIES\wwDBGrid3x'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 3
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited sbTaxLiabilitiesBase: TScrollBox
        inherited fpLiabilityNotes: TisUIFashionPanel
          Left = 328
          Top = 343
          Width = 459
          Height = 148
          TabOrder = 4
          inherited dmemNotes: TEvDBMemo
            Width = 423
            Height = 90
          end
        end
        inherited fpBaseLiabilitiesDetails: TisUIFashionPanel
          Width = 312
          Height = 327
          TabOrder = 0
          inherited lablCheck_Date: TevLabel
            Left = 12
            Top = 74
          end
          inherited lablProcess_Date: TevLabel
            Left = 12
            Top = 269
          end
          inherited lablImpound: TevLabel
            Left = 616
            Top = 208
            Visible = False
          end
          inherited lablTax_Deposits: TevLabel
            Left = 12
            Top = 152
          end
          inherited lablPayroll_Run: TevLabel
            Left = 616
            Top = 80
            Visible = False
          end
          object lablDue_Date: TevLabel [5]
            Left = 155
            Top = 74
            Width = 55
            Height = 16
            Caption = '~Due Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel5: TevLabel [6]
            Left = 155
            Top = 35
            Width = 41
            Height = 13
            Caption = 'Location'
            FocusControl = wwlcLocal
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablAmount: TevLabel [7]
            Left = 155
            Top = 113
            Width = 45
            Height = 16
            Caption = '~Amount'
            FocusControl = wwdeAmount
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablLocal: TevLabel [8]
            Left = 12
            Top = 35
            Width = 35
            Height = 16
            Caption = '~Local'
            FocusControl = wwlcLocal
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablStatus: TevLabel [9]
            Left = 155
            Top = 152
            Width = 39
            Height = 16
            Caption = '~Status'
            FocusControl = wwcbStatus
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablAdjustment_Type: TevLabel [10]
            Left = 12
            Top = 113
            Width = 79
            Height = 13
            Caption = 'Adjustment Type'
            FocusControl = wwcbAdjustment_Type
          end
          inherited wwdbtpCheckDate: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 135
            TabOrder = 2
          end
          inherited dedtImpound: TevDBEdit
            Left = 616
            Top = 224
            Width = 120
            TabOrder = 14
            Visible = False
          end
          inherited dedtPayroll_Run: TevDBEdit
            Left = 616
            Top = 96
            Width = 120
            TabOrder = 15
            Visible = False
          end
          inherited wwDBDateTimePicker2: TevDBDateTimePicker
            Left = 12
            Top = 284
            Width = 135
            TabOrder = 11
          end
          inherited drgpThirdParty: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 135
            Height = 37
            TabOrder = 8
          end
          inherited dedtTax_Deposits: TevEdit
            Left = 12
            Top = 167
            Width = 113
            TabOrder = 6
          end
          inherited buttonAssociateDeposit: TevBitBtn
            Left = 126
            Top = 167
            TabOrder = 16
            TabStop = False
          end
          inherited evDBRadioGroup1: TevDBRadioGroup
            Left = 155
            Top = 191
            Width = 135
            Height = 37
            TabOrder = 9
          end
          inherited evDBEditAchKey: TevDBEdit
            Left = 155
            Top = 284
            Width = 135
            TabOrder = 13
          end
          inherited evPanelssd1: TevPanel
            Left = 155
            Top = 269
            Height = 13
            ParentColor = True
            TabOrder = 17
          end
          inherited evDBDateTimePicker3: TevDBDateTimePicker
            Left = 12
            Top = 245
            Width = 135
          end
          inherited evDBDateTimePicker4: TevDBDateTimePicker
            Left = 155
            Top = 245
            Width = 135
            TabOrder = 12
          end
          inherited evPanel2: TevPanel
            Left = 12
            Top = 230
            Height = 13
            ParentColor = True
            TabOrder = 18
          end
          inherited evPanel3: TevPanel
            Left = 155
            Top = 230
            Height = 13
            ParentColor = True
            TabOrder = 19
          end
          object cbxWorkCoLocalTax: TevDBLookupCombo
            Left = 155
            Top = 50
            Width = 135
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ACCOUNT_NUMBER'#9'30'#9'Account Number'#9'F'
              'ADDRESS1'#9'30'#9'Address1'#9'F'
              'CITY'#9'20'#9'City'#9'F'
              'ZIP_CODE'#9'10'#9'Zip Code'#9'F'
              'STATE'#9'2'#9'State'#9'F')
            DataField = 'CO_LOCATIONS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_LOCATIONS.CO_LOCATIONS
            LookupField = 'CO_LOCATIONS_NBR'
            Options = [loTitles]
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwcbAdjustment_Type: TevDBComboBox
            Left = 12
            Top = 128
            Width = 135
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'ADJUSTMENT_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
          object wwdpDue_Date: TevDBDateTimePicker
            Left = 155
            Top = 89
            Width = 135
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DUE_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 3
          end
          object wwdeAmount: TevDBEdit
            Left = 155
            Top = 128
            Width = 135
            Height = 21
            DataField = 'AMOUNT'
            DataSource = wwdsDetail
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcLocal: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 135
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'LocalName'#9'40'#9'LocalName'#9'F'
              'localState'#9'2'#9'LocalState'#9'F')
            DataField = 'CO_LOCAL_TAX_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
            LookupField = 'CO_LOCAL_TAX_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwcbStatus: TevDBComboBox
            Left = 155
            Top = 167
            Width = 135
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
          end
        end
        inherited fpTaxLiabilities: TisUIFashionPanel
          Left = 328
          TabOrder = 2
          inherited wwdgTaxLiabilities: TevDBGrid
            Selected.Strings = (
              'LOCAL_NAME'#9'10'#9'Name'#9'F'
              'LOCAL_STATE'#9'2'#9'State'#9'F'
              'AMOUNT'#9'12'#9'Amount'#9'F'
              'DUE_DATE'#9'10'#9'Due date'#9'F'
              'CHECK_DATE'#9'10'#9'Check date'#9'F'
              'ADJUSTMENT_TYPE'#9'10'#9'Adj. type'#9'F')
            IniAttributes.SectionName = 'TEDIT_CO_LOCAL_TAX_LIABILITIES\wwdgTaxLiabilities'
            OnSelectionChanged = wwdgTaxLiabilitiesSelectionChanged
          end
        end
        inherited fpTools: TisUIFashionPanel
          Top = 343
          Width = 312
          Height = 148
          TabOrder = 3
          inherited BitBtn3: TevBitBtn
            Width = 277
          end
          inherited bbtnAttach: TevBitBtn
            Left = 12
            Top = 67
            Width = 277
          end
          object btnLocalSubTotals: TevBitBtn
            Left = 12
            Top = 99
            Width = 277
            Height = 25
            Caption = 'Local Subtotals'
            TabOrder = 2
            OnClick = btnLocalSubTotalsClick
            Color = clBlack
            NumGlyphs = 2
            Margin = 0
          end
        end
        inherited Panel4: TevPanel
          Left = 0
          Top = 0
          Width = 323
          Height = 337
          TabOrder = 1
          inherited fpTaxDeposit: TisUIFashionPanel
            Width = 312
            Height = 327
            inherited Label91: TevLabel
              Left = 155
            end
            object evLabel7: TevLabel [3]
              Left = 155
              Top = 191
              Width = 41
              Height = 13
              Caption = 'Location'
              FocusControl = wwcbAdjustment_Type1
            end
            inherited wwcbAdjustment_Type1: TevDBComboBox
              Width = 135
            end
            inherited wwcbStatus1: TevDBComboBox
              Left = 155
              Width = 135
            end
            inherited DBRadioGroup11: TevDBRadioGroup
              Width = 135
            end
            inherited Button1: TevButton
              Left = 12
              Top = 238
              Width = 278
              TabOrder = 10
            end
            inherited wwDBLookupCombo11: TevDBLookupCombo
              Width = 135
            end
            inherited cbClearTaxDepRecord: TevCheckBox
              Left = 155
              Width = 135
            end
            inherited evDBRadioGroup2: TevDBRadioGroup
              Left = 155
              Width = 135
            end
            inherited evPanel1: TevPanel
              TabOrder = 11
            end
            inherited evDBEdit1: TevDBEdit
              Width = 135
            end
            inherited evDBDateTimePicker1: TevDBDateTimePicker
              Width = 135
            end
            inherited evDBDateTimePicker2: TevDBDateTimePicker
              Left = 155
              Width = 135
            end
            inherited evPanel4: TevPanel
              TabOrder = 12
            end
            inherited evPanel5: TevPanel
              Left = 155
              Width = 135
              TabOrder = 13
            end
            object cbxWorkLocationUpdate: TevDBLookupCombo
              Left = 155
              Top = 206
              Width = 135
              Height = 21
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'ACCOUNT_NUMBER'#9'30'#9'Account Number'#9'F'
                'ADDRESS1'#9'30'#9'Address1'#9'F'
                'CITY'#9'20'#9'City'#9'F'
                'ZIP_CODE'#9'10'#9'Zip Code'#9'F'
                'STATE'#9'2'#9'State'#9'F')
              DataField = 'CO_LOCATIONS_NBR'
              DataSource = dsGroupUpdate
              LookupTable = DM_CO_LOCATIONS.CO_LOCATIONS
              LookupField = 'CO_LOCATIONS_NBR'
              Options = [loTitles]
              Style = csDropDownList
              TabOrder = 9
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = True
            end
          end
        end
      end
    end
    inherited tsCO_TAX_DEPOSITS: TTabSheet
      inherited sbTaxDeposits: TScrollBox
        inherited fpTaxDeposits: TisUIFashionPanel
          inherited wwDBGrid1: TevDBGrid
            IniAttributes.SectionName = 'TEDIT_CO_LOCAL_TAX_LIABILITIES\wwDBGrid1'
          end
        end
        inherited fpTaxDepositDetail: TisUIFashionPanel
          inherited lablTax_Type2: TevLabel
            Visible = False
          end
          inherited wwcbType2: TevDBComboBox
            DataField = ''
            Visible = False
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_LOCAL_TAX_LIABILITIES.CO_LOCAL_TAX_LIABILITIES
  end
  inherited DM_CLIENT: TDM_CLIENT
    Top = 10
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 416
    Top = 21
  end
  inherited wwdsCO_TAX_DEPOSITS: TevDataSource
    Top = 81
  end
  inherited dsGroupUpdate: TevDataSource
    Left = 172
    Top = 49
  end
  inherited zLIABILITIES: TevClientDataSet
    Left = 172
  end
  inherited wwdsSubMaster: TevDataSource
    Top = 35
  end
  inherited DM_PAYROLL: TDM_PAYROLL
    Top = 13
  end
  object csWorkLocationSubTotals: TevClientDataSet
    OnCalcFields = csWorkLocationSubTotalsCalcFields
    Left = 660
    Top = 97
    object csWorkLocationSubTotalsCO_LOCATIONS_NBR: TIntegerField
      FieldName = 'CO_LOCATIONS_NBR'
      Visible = False
    end
    object csWorkLocationSubTotalsACCOUNT_NUMBER: TStringField
      FieldKind = fkCalculated
      FieldName = 'ACCOUNT_NUMBER'
      Size = 40
      Calculated = True
    end
    object csWorkLocationSubTotalsAmount: TCurrencyField
      FieldName = 'Amount'
    end
  end
  object dsWorkLocationSubTotals: TevDataSource
    DataSet = csWorkLocationSubTotals
    Left = 700
    Top = 97
  end
end
