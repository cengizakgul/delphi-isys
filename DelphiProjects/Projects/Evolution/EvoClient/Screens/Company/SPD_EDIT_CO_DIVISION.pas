// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_DIVISION;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc, StdCtrls, DBCtrls, Mask, ComCtrls,
  ExtCtrls, Wwdotdot, Wwdbcomb, wwdbedit, Grids, Wwdbigrd, Wwdbgrid,
  wwdblook, SPD_EDIT_CO_BASE, Buttons, SDataStructure,
   EvConsts, EvSecElement, SPD_EDIT_CO_DBDT_LOCALS, SPD_DBDTChoiceQuery,
  SPackageEntry, EvTypes, SDDClasses, ISBasicClasses, SDataDictclient,
  SDataDicttemp,sCompanyShared, EvContext, EvExceptions, ActnList, Menus, Variants,
  ImgList, EvUIUtils, EvUIComponents, EvClientDataSet, isUIDBMemo,
  isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, EvDataAccessComponents;

type
  TEDIT_CO_DIVISION = class(TEDIT_CO_BASE)
    tshtGeneral_Info: TTabSheet;
    tshtBank_Accounts: TTabSheet;
    tshtDefaults: TTabSheet;
    Panel4: TevPanel;
    pdsDetail: TevProxyDataSet;
    tbshLocals: TTabSheet;
    TDBDTLocals1: TDBDTLocals;
    tsVmr: TTabSheet;
    pmLocalCopy: TevPopupMenu;
    MenuItem3: TMenuItem;
    LocalCopyService: TevActionList;
    CopyService: TAction;
    PopUpAddr: TevPopupMenu;
    mnCopyAddr: TMenuItem;
    CopyImageList: TevImageList;
    evActionList1: TevActionList;
    CopyAddressAction: TAction;
    fpDivisionRight: TisUIFashionPanel;
    sbDivisionDetails: TScrollBox;
    fpDivision: TisUIFashionPanel;
    lablName: TevLabel;
    lablAddress1: TevLabel;
    lablAddress2: TevLabel;
    lablCity: TevLabel;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    lablDivision_Code: TevLabel;
    dedtName: TevDBEdit;
    dedtAddress1: TevDBEdit;
    dedtAddress2: TevDBEdit;
    dedtCity: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    wwdeState: TevDBEdit;
    dedtDivision_Code: TevDBEdit;
    fpDivisionContact: TisUIFashionPanel;
    lablPrimary_Contact: TevLabel;
    lablPhone1: TevLabel;
    lablDescription1: TevLabel;
    lablSecondary_Contact: TevLabel;
    lablPhone2: TevLabel;
    lablDescription2: TevLabel;
    dedtPrimary_Contact: TevDBEdit;
    dedtDescription1: TevDBEdit;
    dedtSecondary_Contact: TevDBEdit;
    wwdePhone2: TevDBEdit;
    dedtDescription2: TevDBEdit;
    wwdePhone1: TevDBEdit;
    fpDivisionOptions: TisUIFashionPanel;
    Label2: TevLabel;
    evLabel4: TevLabel;
    DBEdit1: TevDBEdit;
    evDBEdit1: TevDBEdit;
    sbDivisionBanks: TScrollBox;
    fpDivisionBanks: TisUIFashionPanel;
    lablPayroll_Bank_Account: TevLabel;
    lablBilling_Bank_Account: TevLabel;
    lablTax_Bank_Account: TevLabel;
    lablDD_Bank_Account: TevLabel;
    wwlcPayroll_Bank_Account: TevDBLookupCombo;
    wwlcBilling_Bank_Account: TevDBLookupCombo;
    wwlcTax_Bank_Account: TevDBLookupCombo;
    wwlcDD_Bank_Account: TevDBLookupCombo;
    sbDivisionDefaults: TScrollBox;
    fpDivisionDefaults: TisUIFashionPanel;
    fpDivisionNotes: TisUIFashionPanel;
    dmemDivision_Notes: TEvDBMemo;
    lablHome_State: TevLabel;
    lablOverride_Pay_Rate: TevLabel;
    lablOverride_EE_Rate: TevLabel;
    Label6: TevLabel;
    evLabel3: TevLabel;
    wwlcHome_State: TevDBLookupCombo;
    dedtOverride_EE_Rate: TevDBEdit;
    wwdeOverride_Pay_Rate: TevDBEdit;
    wwlcWorkers_Comp_Number: TevDBLookupCombo;
    wwDBLookupCombo4: TevDBLookupCombo;
    wwDBLookupCombo1: TevDBLookupCombo;
    drgpHomeStateType: TevDBRadioGroup;
    evDBLookupCombo6: TevDBLookupCombo;
    sbDivisionLocals: TScrollBox;
    sbDivisionMailRoom: TScrollBox;
    fpMailRoom: TisUIFashionPanel;
    evLabel20: TevLabel;
    evLabel21: TevLabel;
    evLabel22: TevLabel;
    evLabel25: TevLabel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBLookupCombo4: TevDBLookupCombo;
    evDBLookupCombo7: TevDBLookupCombo;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo5: TevDBLookupCombo;
    qlblShowAddressOnCheck: TevLabel;
    wwcbPrint_Branch_Address_Check: TevDBComboBox;
    evLabel6: TevLabel;
    evDBLookupCombo8: TevDBLookupCombo;
    lablFax: TevLabel;
    lablFax_Description: TevLabel;
    lablE_Mail: TevLabel;
    dedtFax_Description: TevDBEdit;
    dedtE_Mail: TevDBEdit;
    wwdeFax: TevDBEdit;
    EvBevel1: TEvBevel;
    EvBevel2: TEvBevel;
    DBText3: TevDBText;
    Label1: TevLabel;
    rgPrimary: TevDBRadioGroup;
    edtAccountNumber: TevDBEdit;
    evLabel5: TevLabel;
    Splitter2: TevSplitter;
    wwDBGrid1: TevDBGrid;
    procedure pdsDetailNewRecord(DataSet: TDataSet);
    procedure ResultFIELDS_CL_BANK_Function;
    procedure PageControl1Change(Sender: TObject);
    procedure CopyServiceExecute(Sender: TObject);
    procedure LocalTaxCopy;
    procedure CopyAddressActionExecute(Sender: TObject);
    procedure CopyAddressActionUpdate(Sender: TObject);
    procedure edtAccountNumberChange(Sender: TObject);
    procedure fpDivisionRightResize(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    function GetDataSetConditions(sName: string): string; override; //gdy
  public
    GL_Setuped:boolean;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
      var InsertDataSets, EditDataSets: TArrayDS;
      var DeleteDataSet: TevClientDataSet; var SupportDataSets: TArrayDS); override; //gdy
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick( Kind: Integer ); override;
    procedure Activate; override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation
{$R *.DFM}

uses EvUtils;

function TEDIT_CO_DIVISION.GetDataSetConditions(sName: string): string;
begin
  if sName = 'CO_DIVISION_LOCALS' then
    Result := ''
  else
    Result := inherited GetDataSetConditions( sNAme );
end;

procedure TEDIT_CO_DIVISION.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
  if (wwdsList.DataSet.Active) and (wwdsList.DataSet.RecordCount <= 0) then exit;
  if Kind in [NavOk] then
  begin
    wwDBGrid1.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavNext.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavPrevious.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavLast.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavFirst.Enabled := not Value;
  end;
end;


procedure TEDIT_CO_DIVISION.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_DIVISION_LOCALS, EditDataSets);
end;

procedure TEDIT_CO_DIVISION.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP, aDS);
  AddDS(DM_CLIENT.CL_TIMECLOCK_IMPORTS, aDS);
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_CLIENT.CL_BILLING, aDS);
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS); //gdy
  AddDS(DM_COMPANY.CO_DIVISION_LOCALS, aDS); //gdy
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);

  inherited;
end;

function TEDIT_CO_DIVISION.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_DIVISION;
end;

function TEDIT_CO_DIVISION.GetInsertControl: TWinControl;
begin
  Result := dedtDivision_code;
end;

procedure TEDIT_CO_DIVISION.pdsDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  with DM_COMPANY.CO do
    if Active then
    begin
      DataSet.FindField('HOME_CO_STATES_NBR').AsInteger :=
        FindField('HOME_CO_STATES_NBR').AsInteger;
      DataSet.FindField('PAY_FREQUENCY_HOURLY').AsString :=
        FindField('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
      DataSet.FindField('PAY_FREQUENCY_SALARY').AsString :=
        FindField('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
    end;
end;

procedure TEDIT_CO_DIVISION.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
 V: Variant;
 tmpDataSet: TevClientDataSet;
begin
  case Kind of
    NavOk:
    begin
      tmpDataSet := TevClientDataSet.Create(nil);
      try
        with TExecDSWrapper.Create('SelectDBDTWithName') do
        begin
          SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
          ctx_DataAccess.GetCustomData(tmpDataSet, AsVariant);
          ctx_DataAccess.OpenDataSets([tmpDataSet]);
          V := tmpDataSet.Lookup('DNAME',dedtName.Text,'CUSTOM_DIVISION_NUMBER');
        end;
        if (not VarIsNull(V)) and (V <> DM_COMPANY.CO_DIVISION['CUSTOM_DIVISION_NUMBER']) then
           EvMessage('Division name has been used by other division, it could cause inaccurate reporting.', mtWarning, [mbOK]);
      finally
        tmpDataSet.free;
      end;

      if DM_COMPANY.CO_DIVISION.FieldByName('HOME_CO_STATES_NBR').AsInteger = 0 then
        raise EUpdateError.CreateHelp('You must select a home state!', IDH_ConsistencyViolation);

      if (DM_COMPANY.CO_DIVISION.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_OVERRIDE) and
         ((not DM_COMPANY.CO_DIVISION.FieldByName('OVERRIDE_PAY_RATE').IsNull) or (wwdeOverride_Pay_Rate.Text <> '')) then
        EvMessage('Auto Labor Distribution will NOT use this override!!!', mtWarning, [mbOK]);
      if GL_Setuped and not CheckGlSetuped(DM_CLIENT.CO_DIVISION,['GENERAL_LEDGER_TAG']) then
           EvMessage(sDontForgetSetupGL);
      GL_Setuped := false;

    end;
    NavInsert:
    begin
      lablName.Caption := '~' + lablName.Caption;
      DM_CLIENT.CO_DIVISION.FieldByName('NAME').Required := true;
      GL_Setuped := CheckGlSetupExist(DM_CLIENT.CO_DIVISION,['GENERAL_LEDGER_TAG'],false);
    end
  end;

  if (Kind in [NavOK,NavCommit]) and
      (trim(edtAccountNumber.Text) <> '') then
         if (Trim(rgPrimary.Value) = '') then
         begin
           Handled := True;
           raise EUpdateError.CreateHelp('Please select Primary!', IDH_ConsistencyViolation);
         end;

  inherited;
end;

procedure TEDIT_CO_DIVISION.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavOk, NavCancel:
    begin
      lablName.Caption := StringReplace( lablName.Caption, '~','', [rfReplaceAll] );
      DM_CLIENT.CO_DIVISION.FieldByName('NAME').Required := false;
    end;
  end;
end;

procedure TEDIT_CO_DIVISION.Activate;
begin
  inherited;
  PageControl1.ActivePageIndex := 0;
  tsVMR.TabVisible := tsVMR.TabVisible and CheckVmrLicenseActive;
  GL_Setuped := false;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CO_DIVISION.AfterDataSetsReopen;
begin
  inherited;
  tsVMR.TabVisible := CheckVmrLicenseActive and CheckVmrSetupActive;
  AssignVMRClientLookups(tsVMR);
end;

procedure TEDIT_CO_DIVISION.ResultFIELDS_CL_BANK_Function;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_CL_BANK_') <> stEnabled then
   Begin
    wwlcPayroll_Bank_Account.Enabled := false;
    wwlcBilling_Bank_Account.Enabled := false;
    wwlcTax_Bank_Account.Enabled := false;
    wwlcDD_Bank_Account.Enabled := false;
   end;
End;

procedure TEDIT_CO_DIVISION.PageControl1Change(Sender: TObject);
begin
  inherited;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CO_DIVISION.CopyServiceExecute(Sender: TObject);
begin
  inherited;
   LocalTaxCopy;
end;

procedure TEDIT_CO_DIVISION.LocalTaxCopy;
var
  CurrentLocalNbr, CurrentNbr, CurrentLocalTaxNbr: integer;

  procedure DoCopyLocals;
  var
    k: integer;
  begin
    with TDBDTChoiceQuery.Create( Self ) do
    try
      SetFilter(CLIENT_LEVEL_DIVISION, 'DIVISION_NBR <>  ' + DM_COMPANY.CO_DIVISION_LOCALS.FieldByName('CO_DIVISION_NBR').AsString );
      Caption := 'Select the Divisions you want this Local Tax copied to';
      ConfirmAllMessage := 'This operation will copy the Local Tax to all Divisions. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
            DM_COMPANY.CO_DIVISION_LOCALS.DataRequired('ALL');
            if dgChoiceList.SelectedList.Count > 0 then
            begin
              DM_COMPANY.CO_DIVISION_LOCALS.DisableControls;
              try
                for k := 0 to dgChoiceList.SelectedList.Count - 1 do
                begin
                  cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                  if not DM_COMPANY.CO_DIVISION_LOCALS.Locate('CO_DIVISION_NBR;CO_LOCAL_TAX_NBR',
                       VarArrayOf([cdsChoiceList.FieldByName('DIVISION_NBR').AsInteger,CurrentLocalTaxNbr]), []) then
                  begin
                    DM_COMPANY.CO_DIVISION_LOCALS.Insert;
                    DM_COMPANY.CO_DIVISION_LOCALS['CO_DIVISION_NBR'] := cdsChoiceList.FieldByName('DIVISION_NBR').AsInteger;
                    DM_COMPANY.CO_DIVISION_LOCALS['CO_LOCAL_TAX_NBR'] := CurrentLocalTaxNbr;
                    DM_COMPANY.CO_DIVISION_LOCALS.Post;
                  end;
                end;
              finally
                ctx_DataAccess.PostDataSets([DM_COMPANY.CO_DIVISION_LOCALS]);
                DM_COMPANY.CO_DIVISION_LOCALS.EnableControls;
              end;
             end;
      end;
    finally
      Free;
    end;
  end;
begin
  CurrentNbr := DM_COMPANY.CO_DIVISION_LOCALS.FieldByName('CO_DIVISION_NBR').AsInteger;
  CurrentLocalNbr := DM_COMPANY.CO_DIVISION_LOCALS.FieldByName('CO_DIVISION_LOCALS_NBR').AsInteger;
  CurrentLocalTaxNbr := DM_COMPANY.CO_DIVISION_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
  if (CurrentLocalNbr > 0) and  (CurrentLocalTaxNbr > 0 )then
  begin
    try
      DoCopyLocals;
    finally
      DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR',CurrentNbr,[]);
      DM_COMPANY.CO_DIVISION_LOCALS.Locate('CO_DIVISION_LOCALS_NBR',CurrentLocalNbr,[]);
    end;
  end;
end;

procedure TEDIT_CO_DIVISION.CopyAddressActionExecute(Sender: TObject);
var
  CurrentNbr : integer;
  CurrentAddr1,Currentaddr2,CurrentCity,CurrentState,CurrentZip : string;

  procedure DoCopyAddress;
  var
    k: integer;
  begin
    with TDBDTChoiceQuery.Create( Self ) do
    try
      SetFilter(CLIENT_LEVEL_DIVISION, 'DIVISION_NBR <>  ' + DM_COMPANY.CO_DIVISION.FieldByName('CO_DIVISION_NBR').AsString, false );
      Caption := 'Select the Divisions you want the Address copied to';
      ConfirmAllMessage := 'This operation will copy the Address to all Divisions. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
              try
                for k := 0 to dgChoiceList.SelectedList.Count - 1 do
                begin
                  cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                  if DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR',cdsChoiceList.FieldByName('DIVISION_NBR').AsInteger,[]) then
                  begin
                    DM_COMPANY.CO_DIVISION.Edit;
                    DM_COMPANY.CO_DIVISION['ADDRESS1'] := CurrentAddr1;
                    DM_COMPANY.CO_DIVISION['ADDRESS2'] := Currentaddr2;
                    DM_COMPANY.CO_DIVISION['CITY']     := CurrentCity;
                    DM_COMPANY.CO_DIVISION['STATE']    := CurrentState;
                    DM_COMPANY.CO_DIVISION['ZIP_CODE'] := CurrentZip;
                    DM_COMPANY.CO_DIVISION.Post;
                  end;
                end;
              finally
                ctx_DataAccess.PostDataSets([DM_COMPANY.CO_DIVISION]);
              end;
      end;
    finally
      Free;
    end;
  end;
begin
  CurrentNbr   := DM_COMPANY.CO_DIVISION.FieldByName('CO_DIVISION_NBR').AsInteger;
  CurrentAddr1 := DM_COMPANY.CO_DIVISION.FieldByName('ADDRESS1').AsString;
  Currentaddr2 := DM_COMPANY.CO_DIVISION.FieldByName('ADDRESS2').AsString;
  CurrentCity  := DM_COMPANY.CO_DIVISION.FieldByName('CITY').AsString;
  CurrentState := DM_COMPANY.CO_DIVISION.FieldByName('STATE').AsString;
  CurrentZip   := DM_COMPANY.CO_DIVISION.FieldByName('ZIP_CODE').AsString;
  try
    DoCopyAddress;
  finally
    DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR',CurrentNbr,[]);
  end;
end;

procedure TEDIT_CO_DIVISION.CopyAddressActionUpdate(Sender: TObject);
begin
  inherited;
   (Sender as TAction).Visible := Assigned(wwDBGrid1.DataSource.DataSet) and (wwDBGrid1.DataSource.DataSet.RecordCount > 0);
end;

procedure TEDIT_CO_DIVISION.edtAccountNumberChange(Sender: TObject);
begin
  inherited;
  if Trim(edtAccountNumber.Text) <> '' then
      rgPrimary.Caption := '~Primary  '
  else
      rgPrimary.Caption := 'Primary ';
end;

constructor TEDIT_CO_DIVISION.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENT
  pnlSubbrowse.Parent := fpDivisionRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_DIVISION.fpDivisionRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpDivisionRight.Width - 40;
  pnlSubbrowse.Height := fpDivisionRight.Height - 65;
end;

initialization
  RegisterClass(TEDIT_CO_DIVISION);

end.
