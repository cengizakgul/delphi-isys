object AcaAffordability: TAcaAffordability
  Left = 521
  Top = 138
  BorderStyle = bsDialog
  Caption = 'Post ACA Affordability Status'
  ClientHeight = 591
  ClientWidth = 388
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    388
    591)
  PixelsPerInch = 96
  TextHeight = 13
  object fpNewHireReport: TisUIFashionPanel
    Left = 0
    Top = 0
    Width = 388
    Height = 551
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    BorderWidth = 12
    Caption = 'fpNewHireReport'
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Calculate Affordability Status'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    Margins.Top = 8
    Margins.Left = 8
    Margins.Right = 8
    DesignSize = (
      388
      551)
    object Bevel1: TBevel
      Left = 18
      Top = 145
      Width = 344
      Height = 10
      Anchors = [akLeft, akTop, akRight]
      Shape = bsTopLine
    end
    object gbDateRange: TGroupBox
      Left = 17
      Top = 40
      Width = 347
      Height = 54
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Date Range'
      TabOrder = 0
      object lblFrom: TLabel
        Left = 8
        Top = 23
        Width = 23
        Height = 13
        Caption = 'From'
      end
      object lblTo: TLabel
        Left = 176
        Top = 23
        Width = 13
        Height = 13
        Caption = 'To'
      end
      object edFrom: TDateTimePicker
        Left = 40
        Top = 19
        Width = 120
        Height = 21
        Date = 42492.707855682870000000
        Time = 42492.707855682870000000
        TabOrder = 0
      end
      object edTo: TDateTimePicker
        Left = 199
        Top = 19
        Width = 120
        Height = 21
        Date = 42492.707985358790000000
        Time = 42492.707985358790000000
        TabOrder = 1
      end
    end
    object chbPrimaryRate: TCheckBox
      Left = 18
      Top = 102
      Width = 133
      Height = 17
      Caption = 'Use Primary Pay Rate'
      TabOrder = 1
    end
    object btnCalc: TevBitBtn
      Left = 284
      Top = 105
      Width = 79
      Height = 25
      Action = acCalcAffordStatus
      Anchors = [akTop, akRight]
      TabOrder = 2
      Color = clBlack
      NumGlyphs = 2
      Margin = 0
    end
    object grAffordability: TevDBGrid
      Left = 18
      Top = 160
      Width = 345
      Height = 370
      DisableThemesInTitle = False
      Selected.Strings = (
        'EeCode'#9'10'#9'EE Code'#9'T'
        'EarnedIncome'#9'1'#9'Earned Income'#9'T'
        'FederalPovertyLevel'#9'1'#9'Federal Poverty Level'#9'T'
        'HourlyRate'#9'1'#9'Hourly Rate'#9'T')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TAcaAffordability\grAffordability'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = dsAffordability
      TabOrder = 3
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = 14544093
      PaintOptions.ActiveRecordColor = clBlack
      NoFire = False
    end
  end
  object btnOK: TevBitBtn
    Left = 140
    Top = 560
    Width = 146
    Height = 25
    Action = acPost
    Anchors = [akRight, akBottom]
    ModalResult = 1
    TabOrder = 1
    Color = clBlack
    NumGlyphs = 2
    Margin = 0
    Kind = bkYes
  end
  object btnClose: TevBitBtn
    Left = 296
    Top = 560
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Close'
    ModalResult = 2
    TabOrder = 2
    Color = clBlack
    NumGlyphs = 2
    Margin = 0
    Kind = bkClose
  end
  object Actions: TActionList
    Left = 80
    Top = 8
    object acCalcAffordStatus: TAction
      Caption = 'Calculate'
      OnExecute = acCalcAffordStatusExecute
    end
    object acPost: TAction
      Caption = 'Post ACA Affordability'
      OnExecute = acPostExecute
      OnUpdate = acPostUpdate
    end
  end
  object dsAffordability: TevDataSource
    Left = 120
    Top = 10
  end
end
