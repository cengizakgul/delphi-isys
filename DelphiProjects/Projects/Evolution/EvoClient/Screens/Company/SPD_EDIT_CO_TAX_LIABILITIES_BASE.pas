// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_TAX_LIABILITIES_BASE;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, Db,
  StdCtrls, wwdbdatetimepicker, wwdbedit, wwdblook, ExtCtrls, DBCtrls,
  Mask, Wwdotdot, Wwdbcomb, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, Controls, Classes, SPackageEntry, SysUtils, Dialogs, EvConsts,
  EvUtils, Variants, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, SFieldCodeValues,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIDBMemo, isUIEdit, isUIwwDBEdit,
  isUIwwDBDateTimePicker, isUIwwDBComboBox, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_TAX_LIABILITIES_BASE = class(TEDIT_CO_BASE)
    Label22: TevLabel;
    DBText3: TevDBText;
    TabSheet2: TTabSheet;
    tsCO_TAX_DEPOSITS: TTabSheet;
    wwdsCO_TAX_DEPOSITS: TevDataSource;
    dsGroupUpdate: TevDataSource;
    zLIABILITIES: TevClientDataSet;
    zLIABILITIESSADJUSTMENT_TYPE: TStringField;
    zLIABILITIESSSTATUS: TStringField;
    zLIABILITIESSCO_TAX_DEPOSITS_NBR: TIntegerField;
    zLIABILITIESSTHIRD_PARTY: TStringField;
    cbGroup: TevCheckBox;
    cbShowCredit: TevCheckBox;
    wwdsSubMaster: TevDataSource;
    DM_PAYROLL: TDM_PAYROLL;
    zLIABILITIESIMPOUNDED: TStringField;
    zLIABILITIESACH_KEY: TStringField;
    zLIABILITIESPERIOD_BEGIN_DATE: TDateTimeField;
    zLIABILITIESPERIOD_END_DATE: TDateTimeField;
    sbTaxLiabilitiesBase: TScrollBox;
    Label2x: TevLabel;
    lablTotalTaxValue: TevLabel;
    Panel4: TevPanel;
    fpLiabilityNotes: TisUIFashionPanel;
    dmemNotes: TEvDBMemo;
    fpBaseLiabilitiesDetails: TisUIFashionPanel;
    lablCheck_Date: TevLabel;
    lablProcess_Date: TevLabel;
    lablImpound: TevLabel;
    lablTax_Deposits: TevLabel;
    lablPayroll_Run: TevLabel;
    wwdbtpCheckDate: TevDBDateTimePicker;
    dedtImpound: TevDBEdit;
    dedtPayroll_Run: TevDBEdit;
    wwDBDateTimePicker2: TevDBDateTimePicker;
    drgpThirdParty: TevDBRadioGroup;
    dedtTax_Deposits: TevEdit;
    buttonAssociateDeposit: TevBitBtn;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBEditAchKey: TevDBEdit;
    evPanelssd1: TevPanel;
    evLabelssd1: TevLabel;
    evDBDateTimePicker3: TevDBDateTimePicker;
    evDBDateTimePicker4: TevDBDateTimePicker;
    evPanel2: TevPanel;
    evLabel6: TevLabel;
    evPanel3: TevPanel;
    evLabel4: TevLabel;
    fpTaxLiabilities: TisUIFashionPanel;
    wwdgTaxLiabilities: TevDBGrid;
    Label27: TevLabel;
    fpTools: TisUIFashionPanel;
    BitBtn3: TevBitBtn;
    bbtnAttach: TevBitBtn;
    fpTaxDeposit: TisUIFashionPanel;
    lablAdjustment_Type1: TevLabel;
    Label91: TevLabel;
    Label101: TevLabel;
    wwcbAdjustment_Type1: TevDBComboBox;
    wwcbStatus1: TevDBComboBox;
    DBRadioGroup11: TevDBRadioGroup;
    Button1: TevButton;
    wwDBLookupCombo11: TevDBLookupCombo;
    cbClearTaxDepRecord: TevCheckBox;
    evDBRadioGroup2: TevDBRadioGroup;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evPanel4: TevPanel;
    evLabel2: TevLabel;
    evPanel5: TevPanel;
    evLabel3: TevLabel;
    sbTaxDeposits: TScrollBox;
    Label8: TevLabel;
    fpTaxDepositDetail: TisUIFashionPanel;
    Label1: TevLabel;
    lablStatus2: TevLabel;
    lablAdjustment_Type2: TevLabel;
    lablAmount2: TevLabel;
    lablDue_Date2: TevLabel;
    lablTax_Type2: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    Label4: TevLabel;
    Label6: TevLabel;
    Label7: TevLabel;
    BitBtn5: TevBitBtn;
    wwcbStatus2: TevDBComboBox;
    wwcbAdjustment_Type2: TevDBComboBox;
    wwedAmount2: TevDBEdit;
    wwdpDue_Date2: TevDBDateTimePicker;
    wwcbType2: TevDBComboBox;
    DBEdit1: TevDBEdit;
    DBEdit2: TevDBEdit;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    wwDBDateTimePicker3: TevDBDateTimePicker;
    dedtTaxDeposit2: TevEdit;
    BitBtn4: TevBitBtn;
    fpTaxDeposits: TisUIFashionPanel;
    wwDBGrid1: TevDBGrid;
    wwDBGrid2x: TevDBGrid;
    wwDBGrid1x: TevDBGrid;
    Splitter2: TevSplitter;
    lcbAdjType: TevDBComboBox;
    zLIABILITIESCO_LOCATIONS_NBR: TIntegerField;
    zLIABILITIESNONRES_CO_LOCAL_TAX_NBR: TIntegerField;
    procedure BitBtn3Click(Sender: TObject);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure buttonAssociateDepositClick(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure wwdgTaxLiabilitiesMultiSelectRecord(Grid: TwwDBGrid;
      Selecting: Boolean; var Accept: Boolean);
    procedure wwdgTaxLiabilitiesRowChanged(Sender: TObject);
    procedure cbClearTaxDepRecordClick(Sender: TObject);
    procedure cbGroupClick(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure BitBtn4Click(Sender: TObject);
    procedure bbtnAttachClick(Sender: TObject);
  private
    { Private declarations }
    FActivated: Boolean;
    procedure TurnGroupEdit(const f: Boolean);
    function GetAdjTypeCondition: string;
  protected
    FTempStatusList: string;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure AfterDataSetsReopen; override;
    procedure wwcbStatusDropDown(Sender: TObject);
    procedure wwcbStatusCloseUp(Sender: TwwDBComboBox; Select: Boolean);
    procedure SetReadOnly(Value: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ReCalcTotals; virtual;
    procedure AfterClick(Kind: Integer); override;
    procedure UpdateLiabilityDepositField;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
  end;


implementation

{$R *.DFM}

procedure TEDIT_CO_TAX_LIABILITIES_BASE.SetReadOnly(Value: Boolean);
begin
 inherited;
  cbGroup.SecurityRO := False;
  cbShowCredit.SecurityRO := False;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = NavInsert then
    if wwDBGrid1x.Visible then
    begin
      wwdsDetail.DataSet['PR_NBR'] := wwdsSubMaster.DataSet['PR_NBR'];
    end;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.BitBtn3Click(Sender: TObject);
begin
  inherited;
  ReCalcTotals;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.ReCalcTotals;
begin
  // Needs to be overriden
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.wwdsSubMasterDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and
     (wwdsDetail.MasterDataSource = wwdsSubMaster) then
    ReCalcTotals;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.UpdateLiabilityDepositField;
var
  s: string;
  d: TDateTime;
begin
  if wwdsDetail.DataSet.Active and DM_COMPANY.CO_TAX_DEPOSITS.Active and
     not wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
    with DM_COMPANY.CO_TAX_DEPOSITS do
    begin
      if not Locate('CO_TAX_DEPOSITS_NBR', wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger, []) then
        dedtTax_Deposits.Text := '<unknown-Contact Administrator>'
      else
      begin
        s := FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString;
        if Copy(s, 1, 5) = 'EFTD#' then
          d := EncodeDate(StrToInt(Copy(s, 6, 4)), StrToInt(Copy(s, 10, 2)), StrToInt(Copy(s, 12, 2)))
        else
          d := FieldByName('STATUS_DATE').AsDateTime;
        dedtTax_Deposits.Text :=
          ConvertCode(TCO_TAX_DEPOSITS, 'STATUS', FieldByName('STATUS').AsString) +
          ' '+ FormatDateTime('mm/dd/yyyy', d)+ ' '+ s;
      end;
    end
  else
    dedtTax_Deposits.Text := '<none>';

  dedtTaxDeposit2.Text := dedtTax_Deposits.Text;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.wwdsDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (csLoading in wwdsDetail.ComponentState) or not FActivated then
    Exit;

  if Sender = wwdsDetail then
    if not assigned(Field) then
    begin
      UpdateLiabilityDepositField;
      bbtnAttach.Enabled := False;
      if Assigned(wwdsDetail.DataSet) then
        if wwdsDetail.DataSet.Active then
        begin
          bbtnAttach.Enabled := wwdsDetail.DataSet.FieldByName('PR_NBR').IsNull;
          if wwdsDetail.DataSet['PR_NBR'] <> wwdsSubMaster.DataSet['PR_NBR'] then
            wwdsSubMaster.DataSet.Locate('PR_NBR', wwdsDetail.DataSet['PR_NBR'], []);
        end;
    end;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.buttonAssociateDepositClick(
  Sender: TObject);
begin
  inherited;
  tsCO_TAX_DEPOSITS.TabVisible := True;

  if not wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
    if not DM_COMPANY.CO_TAX_DEPOSITS.Locate('CO_TAX_DEPOSITS_NBR', wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger, []) then
      DM_COMPANY.CO_TAX_DEPOSITS.First;

  PageControl1.ActivePage := tsCO_TAX_DEPOSITS;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.BitBtn5Click(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  i := wwdsCO_TAX_DEPOSITS.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
  if i <> 0 then
    if EvMessage('Are you sure you want to apply tax deposit to this liability?', mtConfirmation, [mbYes, mbNo]) = mrYes then
      with wwdsDetail.DataSet do
      begin
        if not (State in [dsEdit, dsInsert]) then
          Edit;
        FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger := i;
        UpdateLiabilityDepositField;
      end;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  with wwdsDetail.DataSet as TevClientDataSet do
  begin
    DisableControls;
    try
      for i := 0 to wwdgTaxLiabilities.SelectedList.Count - 1 do
      begin
        GotoBookmark(wwdgTaxLiabilities.SelectedList[i]);
        Edit;
        if not VarIsNull(zLIABILITIES['ADJUSTMENT_TYPE']) then
            FieldValues['ADJUSTMENT_TYPE'] := zLIABILITIES['ADJUSTMENT_TYPE'];

        if not VarIsNull(zLIABILITIES['STATUS']) then
          FieldValues['STATUS'] := zLIABILITIES['STATUS'];
        if cbClearTaxDepRecord.Checked then
          FieldValues['CO_TAX_DEPOSITS_NBR'] := Null
        else
        if not VarIsNull(zLIABILITIES['CO_TAX_DEPOSITS_NBR']) then
          FieldValues['CO_TAX_DEPOSITS_NBR'] := zLIABILITIES['CO_TAX_DEPOSITS_NBR'];
        if not VarIsNull(zLIABILITIES['ACH_KEY']) then
          FieldValues['ACH_KEY'] := zLIABILITIES['ACH_KEY'];
        if not VarIsNull(zLIABILITIES['PERIOD_BEGIN_DATE']) then
          FieldValues['PERIOD_BEGIN_DATE'] := zLIABILITIES['PERIOD_BEGIN_DATE'];
        if not VarIsNull(zLIABILITIES['PERIOD_END_DATE']) then
          FieldValues['PERIOD_END_DATE'] := zLIABILITIES['PERIOD_END_DATE'];
        if DBRadioGroup11.ItemIndex <> -1 then
          FieldValues['THIRD_PARTY'] := zLIABILITIES['THIRD_PARTY'];
        if evDBRadioGroup2.ItemIndex <> -1 then
          FieldValues['IMPOUNDED'] := zLIABILITIES['IMPOUNDED'];
        if wwdsDetail.DataSet = DM_CLIENT.CO_LOCAL_TAX_LIABILITIES then
        begin
          if ( not VarIsNull(zLIABILITIES['NONRES_CO_LOCAL_TAX_NBR']) ) and
             ( FieldByName('Local_State').AsString = 'PA') and
             ( FieldByName('Local_Type').asString[1] in [LOCAL_TYPE_INC_NON_RESIDENTIAL, LOCAL_TYPE_INC_RESIDENTIAL, LOCAL_TYPE_SCHOOL])
          then
          FieldValues['CO_LOCATIONS_NBR'] := zLIABILITIES['CO_LOCATIONS_NBR'];
        end;
        CheckBrowseMode;
      end;
    finally
      EnableControls;
      zLIABILITIES.Cancel;
    end;
    wwdgTaxLiabilities.UnSelectAll;
    TurnGroupEdit(False);
  end;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.wwdgTaxLiabilitiesMultiSelectRecord(
  Grid: TwwDBGrid; Selecting: Boolean; var Accept: Boolean);
begin
  inherited;
  TurnGroupEdit(wwdgTaxLiabilities.SelectedList.Count > 0);
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.wwdgTaxLiabilitiesRowChanged(
  Sender: TObject);
begin
  inherited;
  TurnGroupEdit(wwdgTaxLiabilities.SelectedList.Count > 1);
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.TurnGroupEdit(const f: Boolean);
begin
  if f and not Panel4.Visible then
  begin
    Panel4.BringToFront;
    wwcbAdjustment_Type1.Clear;
    wwcbStatus1.Clear;
    wwDBLookupCombo11.Clear;
    DBRadioGroup11.ItemIndex := -1;
    evDBRadioGroup2.ItemIndex := -1;
    if not zLIABILITIES.Active then
      zLIABILITIES.CreateDataSet
    else
      zLIABILITIES.EmptyDataSet;
  end;
  Panel4.Visible := f;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.cbClearTaxDepRecordClick(
  Sender: TObject);
begin
  inherited;
  wwDBLookupCombo11.Enabled := not cbClearTaxDepRecord.Checked;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.cbGroupClick(Sender: TObject);
begin
  inherited;
  wwdbGrid1x.Visible := cbGroup.Checked;
  if cbGroup.Checked then
    wwdsDetail.MasterDataSource := wwdsSubMaster
  else
    wwdsDetail.MasterDataSource := wwdsMaster;
  TevClientDataSet(wwdsDetail.DataSet).Filter := GetAdjTypeCondition;
end;

function TEDIT_CO_TAX_LIABILITIES_BASE.GetAdjTypeCondition: string;
begin
  if cbShowCredit.Checked then
    Result := ''
  else
    Result := 'ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_NONE +''' or ADJUSTMENT_TYPE <> '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_CREDIT+ '''';
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  inherited;
  if PageControl1.ActivePage = tsCO_TAX_DEPOSITS then
    tsCO_TAX_DEPOSITS.TabVisible := False;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.BitBtn4Click(Sender: TObject);
begin
  inherited;
  if EvMessage('Are you sure you want to clear tax deposit to this liability?', mtConfirmation, [mbYes, mbNo]) = mrYes then
    with wwdsDetail.DataSet do
    begin
      if not (State in [dsEdit, dsInsert]) then
        Edit;
      FieldByName('CO_TAX_DEPOSITS_NBR').Clear;
      UpdateLiabilityDepositField;
    end;
end;

function TEDIT_CO_TAX_LIABILITIES_BASE.GetDataSetConditions(
  sName: string): string;
begin
  if sName = 'CO_TAX_DEPOSITS' then
    Result := ''
  else if sName = 'PR' then
    Result := '(CO_NBR = ' + IntToStr(wwdsList.DataSet.FieldByName('CO_NBR').AsInteger) + ') and ' +
              '(STATUS in (''' + PAYROLL_STATUS_PROCESSED + ''', ''' + PAYROLL_STATUS_VOIDED + '''))'
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_TAX_DEPOSITS, aDS);
  AddDS(DM_PAYROLL.PR, aDS);
  inherited;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
   DM_PAYROLL.CO_TAX_DEPOSITS.Close;
  inherited;
  AddDS(DM_COMPANY.CO_TAX_DEPOSITS, SupportDataSets);
  AddDS(DM_PAYROLL.PR, SupportDataSets);
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.AfterDataSetsReopen;
begin
  inherited;
  cbGroupClick(nil);
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.Activate;
var
  i: Integer;
begin
  inherited;
  if (wwdsList.DataSet.RecordCount <= 0) then Exit;
  wwcbAdjustment_Type1.Items.Text := TaxLiabilityAdjustmentType_ComboChoices;
  wwcbStatus1.Items.Text := TaxDepositStatus_ComboChoices;
  TevCLientDataSet(wwdsDetail.DataSet).Filtered := True;
  cbGroupClick(nil);
  with wwcbStatus1.Items do
  begin
    for i := Count - 1 downto 0 do
      if not (Strings[i][Length(Strings[i])] in [TAX_DEPOSIT_STATUS_PENDING,
        TAX_DEPOSIT_STATUS_IMPOUNDED, TAX_DEPOSIT_STATUS_PAID,
        TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE, TAX_DEPOSIT_STATUS_NSF,
        TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED, TAX_DEPOSIT_STATUS_PAID_WO_FUNDS]) then
        Delete(i);
  end;
  TurnGroupEdit(False);
  wwdsSubMaster.DoDataChange(Self, nil);
  FActivated := True;
  UpdateLiabilityDepositField;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.Deactivate;
begin
  TevCLientDataSet(wwdsDetail.DataSet).Filtered := False;
  inherited;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
  if Kind = NavOK then
  begin
    wwDBGrid1.Enabled := not Value;
    wwdgTaxLiabilities.Enabled := not Value;
  end;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.bbtnAttachClick(Sender: TObject);
var
  StrDate, StrRun: String;
begin
  inherited;
  if not EvDialog('Input required', 'Please, enter check date', StrDate) then
    Exit;
  if not EvDialog('Input required', 'Please, enter run number', StrRun) then
    Exit;
  if VarIsNull(DM_PAYROLL.PR.Lookup('CHECK_DATE;RUN_NUMBER', VarArrayOf([StrDate, StrRun]), 'PR_NBR')) then
  begin
    EvMessage('No such payroll!');
  end
  else
  begin
    if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
      wwdsDetail.Edit;
    wwdsDetail.DataSet.FieldByName('PR_NBR').Value := DM_PAYROLL.PR.Lookup('CHECK_DATE;RUN_NUMBER', VarArrayOf([StrDate, StrRun]), 'PR_NBR');
    wwdsDetail.DataSet.FieldByName('CHECK_DATE').AsString := StrDate;
  end;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.wwcbStatusCloseUp(
  Sender: TwwDBComboBox; Select: Boolean);
begin
  Sender.Items.CommaText := FTempStatusList;
end;

procedure TEDIT_CO_TAX_LIABILITIES_BASE.wwcbStatusDropDown(
  Sender: TObject);
var
  i: Integer;
begin
  FTempStatusList := TwwDBComboBox(Sender).Items.CommaText;
  with TwwDBComboBox(Sender).Items do
    for i := Count - 1 downto 0 do
      if not (Strings[i][Length(Strings[i])] in [TAX_DEPOSIT_STATUS_PENDING,
        TAX_DEPOSIT_STATUS_IMPOUNDED, TAX_DEPOSIT_STATUS_PAID,
        TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE, TAX_DEPOSIT_STATUS_NSF,
        TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED, TAX_DEPOSIT_STATUS_PAID_WO_FUNDS]) then
        Delete(i);
end;

end.
