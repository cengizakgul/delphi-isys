inherited EDIT_CO_ADDITIONAL_INFO_NAMES: TEDIT_CO_ADDITIONAL_INFO_NAMES
  Width = 646
  Height = 612
  inherited Panel1: TevPanel
    Width = 646
    inherited pnlFavoriteReport: TevPanel
      Left = 494
    end
    inherited pnlTopLeft: TevPanel
      Width = 494
    end
  end
  inherited PageControl1: TevPageControl
    Width = 646
    Height = 558
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 638
        Height = 529
        inherited pnlBorder: TevPanel
          Width = 634
          Height = 525
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 634
          Height = 525
          inherited Panel3: TevPanel
            Width = 586
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 586
            Height = 418
            inherited Splitter1: TevSplitter
              Height = 414
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 24
              Height = 414
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 129
                Height = 334
                IniAttributes.SectionName = 'TEDIT_CO_ADDITIONAL_INFO_NAMES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 27
              Width = 555
              Height = 414
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Fields'
      ImageIndex = 2
      object sbCustomFields: TScrollBox
        Left = 0
        Top = 0
        Width = 638
        Height = 529
        Align = alClient
        TabOrder = 0
        object fpCustomFields: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 315
          Height = 289
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgCompany_Jobs: TevDBGrid
            Left = 12
            Top = 35
            Width = 279
            Height = 230
            HelpContext = 14001
            TabStop = False
            DisableThemesInTitle = False
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_ADDITIONAL_INFO_NAMES\wwdgCompany_Jobs'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpFieldDetail: TisUIFashionPanel
          Left = 8
          Top = 305
          Width = 315
          Height = 170
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpFieldDetail'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label1: TevLabel
            Left = 12
            Top = 35
            Width = 37
            Height = 16
            Caption = '~Name'
            FocusControl = dedtName
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object qlblShowAddressOnCheck: TevLabel
            Left = 201
            Top = 75
            Width = 59
            Height = 16
            Caption = '~Data Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 201
            Top = 114
            Width = 98
            Height = 13
            Caption = 'Sequence Number   '
            FocusControl = edSeqNumber
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 9
            Top = 113
            Width = 128
            Height = 16
            Caption = '~Show in Evolution Payroll'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtName: TevDBEdit
            Left = 12
            Top = 50
            Width = 281
            Height = 21
            HelpContext = 14001
            DataField = 'NAME'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edCategory: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 180
            Height = 37
            HelpContext = 35076
            Caption = '~Show in Self Serve'
            Columns = 2
            DataField = 'CATEGORY'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
            OnChange = edCategoryChange
          end
          object edtDataType: TevDBComboBox
            Left = 201
            Top = 90
            Width = 92
            Height = 21
            HelpContext = 13015
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'DATA_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object edSeqNumber: TevDBEdit
            Left = 201
            Top = 129
            Width = 92
            Height = 21
            HelpContext = 14001
            DataField = 'SEQ_NUM'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object cbShowinPayroll: TevDBComboBox
            Left = 9
            Top = 128
            Width = 180
            Height = 21
            HelpContext = 13015
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'SHOW_IN_EVO_PAYROLL'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
        end
        object pnlValues: TevPanel
          Left = 331
          Top = 0
          Width = 323
          Height = 481
          BevelOuter = bvNone
          TabOrder = 2
          object fpFieldValues: TisUIFashionPanel
            Left = 2
            Top = 8
            Width = 315
            Height = 289
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpFieldValues'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Values'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evDBGrid1: TevDBGrid
              Left = 12
              Top = 35
              Width = 279
              Height = 230
              HelpContext = 14001
              TabStop = False
              DisableThemesInTitle = False
              Selected.Strings = (
                'INFO_VALUE'#9'40'#9'VALUE'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_CO_ADDITIONAL_INFO_NAMES\evDBGrid1'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              DataSource = wwdsValues
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
          object fpValueDetail: TisUIFashionPanel
            Left = 2
            Top = 305
            Width = 315
            Height = 170
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'isUIFashionPanel1'
            Color = 14737632
            TabOrder = 1
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Value'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object evLabel1: TevLabel
              Left = 12
              Top = 35
              Width = 36
              Height = 16
              Caption = '~Value'
              FocusControl = dedtValue
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object dedtValue: TevDBEdit
              Left = 12
              Top = 50
              Width = 279
              Height = 21
              HelpContext = 14001
              DataField = 'INFO_VALUE'
              DataSource = wwdsValues
              Picture.PictureMaskFromDataSet = False
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              OnChange = dedtValueChange
              Glowing = False
            end
            inline MiniNavigationFrame: TMiniNavigationFrame
              Left = 11
              Top = 101
              Width = 281
              Height = 25
              TabOrder = 1
              inherited SpeedButton1: TevSpeedButton
                Width = 115
                Caption = 'Create'
                Glyph.Data = {
                  36060000424D3606000000000000360000002800000020000000100000000100
                  18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
                  CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
                  E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
                  7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
                  82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
                  9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
                  55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
                  5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
                  CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
                  0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
                  76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
                  FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
                  16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
                  8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
                  FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
                  38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
                  9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
                  FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
                  51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
                  9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
                  66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
                  9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
                  76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
                  9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
                  FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
                  59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
                  9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
                  FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
                  0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
                  87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
                  FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
                  55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
                  556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
                  B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
                  88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
                  E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
                  78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              end
              inherited SpeedButton2: TevSpeedButton
                Left = 164
                Width = 115
                Caption = 'Delete'
                Glyph.Data = {
                  36060000424D3606000000000000360000002800000020000000100000000100
                  18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
                  CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
                  F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
                  6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
                  A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
                  CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
                  A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
                  2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
                  C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
                  9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
                  5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
                  F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
                  9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
                  576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
                  FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
                  97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
                  4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
                  F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
                  8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
                  3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
                  F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
                  334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
                  F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
                  2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
                  EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
                  3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
                  EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
                  84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
                  8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
                  F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
                  8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
                  9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
                  F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
                  8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
                  2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
                  C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
                  807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
                  C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
                  EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
                  CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
                  6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_ADDITIONAL_INFO_NAMES.CO_ADDITIONAL_INFO_NAMES
    OnDataChange = wwdsDetailDataChange
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Top = 18
  end
  inherited dsCL: TevDataSource
    Left = 292
    Top = 10
  end
  inherited DM_CLIENT: TDM_CLIENT
    Top = 2
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 400
    Top = 21
  end
  object wwdsValues: TevDataSource
    DataSet = DM_CO_ADDITIONAL_INFO_VALUES.CO_ADDITIONAL_INFO_VALUES
    MasterDataSource = wwdsDetail
    Left = 558
    Top = 82
  end
end
