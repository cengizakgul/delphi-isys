inherited EDIT_CO_HR_POSITIONS: TEDIT_CO_HR_POSITIONS
  Width = 1134
  Height = 747
  inherited Panel1: TevPanel
    Width = 1134
    inherited pnlFavoriteReport: TevPanel
      Left = 982
    end
    inherited pnlTopLeft: TevPanel
      Width = 982
    end
  end
  inherited PageControl1: TevPageControl
    Width = 1134
    Height = 693
    ActivePage = tbshDetails
    OnChanging = nil
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 1126
        Height = 664
        inherited pnlBorder: TevPanel
          Width = 1122
          Height = 660
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 1122
          Height = 660
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'HR Positions'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCORightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 400
              Top = 32
              Width = 320
              Height = 480
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 1074
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 1074
            Height = 553
            inherited Splitter1: TevSplitter
              Left = 320
              Height = 549
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Height = 549
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 469
                IniAttributes.SectionName = 'TEDIT_CO_HR_POSITIONS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 747
              Height = 549
              Align = alClient
              Visible = True
              Title = 'HR Positions'
              object evDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 697
                Height = 469
                DisableThemesInTitle = False
                Selected.Strings = (
                  'DESCRIPTION'#9'25'#9'Description'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_HR_POSITIONS\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              inline CopyHelper: TCompanyCopyHelper
                Left = 128
                Top = 112
                Width = 115
                Height = 54
                TabOrder = 1
                Visible = False
              end
            end
          end
        end
      end
    end
    object tbshDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbHRPositions: TScrollBox
        Left = 0
        Top = 0
        Width = 1126
        Height = 664
        Align = alClient
        TabOrder = 0
        object fpCOHRPosition: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 314
          Height = 455
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evDBGrid2: TevDBGrid
            Left = 12
            Top = 35
            Width = 279
            Height = 399
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Position'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_HR_POSITIONS\evDBGrid2'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCOHRPositionSelected: TisUIFashionPanel
          Left = 328
          Top = 8
          Width = 514
          Height = 281
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Selected Pay Grades   '
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evGridSelected: TevDBGrid
            Left = 12
            Top = 35
            Width = 481
            Height = 186
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F'
              'MAXIMUM_PAY'#9'10'#9'Maximum Pay'#9'F'
              'MID_PAY'#9'10'#9'Mid Pay'#9'F'
              'MINIMUM_PAY'#9'10'#9'Minimum Pay'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_HR_POSITIONS\evGridSelected'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsSalaryGradesSelected
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object ebBtnDelete: TevBitBtn
            Left = 12
            Top = 233
            Width = 116
            Height = 25
            Caption = 'Move Down'
            TabOrder = 1
            TabStop = False
            OnClick = ebBtnDeleteClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC
              CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFCCCCCC008B4B008B4BCCCCCCFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC7D
              7C7C7D7C7CCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFCCCCCC00884800C38200C382008848CCCCCCFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC7A7979B2
              B2B2B2B2B27A7979CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCCCCC00884700BD7E00DD9F00DD9F00BD7E008847CCCCCCFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC7A7979ACACACCC
              CCCCCCCCCCACACAC7A7979CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF008C4B53D8B053EAC400D59C00D59C53EAC453D8B0008C4BFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7D7C7CCCCCCCDEDEDEC5
              C5C5C5C5C5DEDEDECCCCCC7D7C7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF3AA77500884600B97E00D39D00D39E00B97E0088473AA775FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9A9A9A797979AAA9A9C4
              C3C3C4C3C3AAA9A97A79799A9A9AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0086441BD9AC00D4A1008644FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF787877CB
              CBCBC5C5C5787877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088462ADBB400D1A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979CE
              CECEC3C2C27A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088453ADBBB00CEA3008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D0
              D0D0C0C0C07A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884549DCC100CAA2008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D2
              D2D1BEBDBD7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884458DDC700C7A2008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D4
              D4D4BBBBBB7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884467E0CD00C3A1008A46FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D8
              D8D8B8B7B77B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884476E1D300BFA0008A46FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979DB
              DADAB4B4B47B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884486E4DA00BBA1008A46FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979DF
              DFDEB2B1B17B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF008A4698EAE667DCD4008A47FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B7B7BE7
              E7E6D7D6D67B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF4FB183008A47008A474FB183FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4A4A47B
              7B7B7B7B7BA4A4A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object evBtnUpdate: TevBitBtn
            Left = 377
            Top = 233
            Width = 116
            Height = 25
            Caption = 'Move Up'
            TabOrder = 2
            TabStop = False
            OnClick = evBtnUpdateClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFDDDDDDCCCCCCCCCCCCDDDDDDFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDCC
              CCCCCCCCCCDDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF43A277008A4A008A4A43A277FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9695957C
              7B7B7C7B7B969595FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00894900E7A700E7A7008949FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B7B7BD5
              D5D5D5D5D57B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884708E2A600E0A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7A7979D1
              D1D1CECECE7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00874716E0AC00DCA2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D1
              D1D1CCCCCC7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00874623E0B200D9A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D2
              D2D1CAC9C97A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884630DFB700D5A2008847FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D2
              D2D1C6C6C67A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088453EDFBD00D2A1008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D3
              D3D3C4C3C37A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0088454BE0C100CFA1008947FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D5
              D5D5C1C1C17A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF00884459E1C800CBA2008946FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D7
              D7D7BEBDBD7A7979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFD9D9D9CCCCCC0087426AE2CC00C9A1008744CCCCCCD9D9D9FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D9D9CCCCCC787877D9
              D9D9BCBCBB787877CCCCCCD9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF329C6B00894600AE7F00C39E00C39E00AE80008946329C6BFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8F7A7979A1A0A0B7
              B6B6B7B6B6A1A0A07A79798F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF008C4A82DCCA00C1A000BE9B00BE9B00C1A083DCCA008C4AFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7D7C7CD5D5D5B6B6B5B2
              B2B2B2B2B2B6B6B5D5D5D57D7C7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF0088448EDBCB00B99D00B99D8EDBCB008844FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF797979D5D5D5AF
              AFAFAFAFAFD5D5D5797979FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF0087429AE0D39AE0D3008742FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF787877DB
              DADADBDADA787877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008A47008A47FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B
              7B7B7B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object fpCOHRPositionAvailable: TisUIFashionPanel
          Left = 328
          Top = 297
          Width = 514
          Height = 268
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Available Pay Grades '
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evGridAvailable: TevDBGrid
            Left = 12
            Top = 35
            Width = 481
            Height = 210
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Pay Grade'#9'F'
              'MAXIMUM_PAY'#9'10'#9'Maximum Pay'#9#9
              'MID_PAY'#9'10'#9'Mid Pay'#9#9
              'MINIMUM_PAY'#9'10'#9'Minimum Pay'#9#9)
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_HR_POSITIONS\evGridAvailable'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsSalaryGradesAvailable
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCOHRPositionDetails: TisUIFashionPanel
          Left = 8
          Top = 471
          Width = 314
          Height = 94
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'HR Position Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 46
            Height = 16
            Caption = '~Position'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtDescription: TevDBEdit
            Left = 12
            Top = 50
            Width = 279
            Height = 21
            DataField = 'DESCRIPTION'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object Button1: TButton
          Left = 912
          Top = 120
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 4
          OnClick = Button1Click
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_HR_POSITIONS.CO_HR_POSITIONS
  end
  object dsSalaryGradesAvailable: TevDataSource
    DataSet = DM_CO_HR_SALARY_GRADES.CO_HR_SALARY_GRADES
    Left = 624
    Top = 240
  end
  object dsSalaryGradesSelected: TevDataSource
    DataSet = DM_CO_HR_POSITION_GRADES.CO_HR_POSITION_GRADES
    MasterDataSource = wwdsDetail
    Left = 520
    Top = 136
  end
end
