inherited EDIT_CO_DEPARTMENT: TEDIT_CO_DEPARTMENT
  Width = 685
  Height = 615
  inherited Panel1: TevPanel
    Width = 685
    inherited pnlFavoriteReport: TevPanel
      Left = 533
    end
    inherited pnlTopLeft: TevPanel
      Width = 533
      inherited dbtxClientName: TevDBText
        Width = 159
      end
      inherited CompanyNameText: TevDBText
        Width = 175
      end
      object lablBRANCH_NAME: TevLabel [6]
        Left = 454
        Top = 8
        Width = 29
        Height = 13
        Caption = 'DEPT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TevLabel [7]
        Left = 280
        Top = 24
        Width = 45
        Height = 13
        Caption = 'BRANCH'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TevLabel [8]
        Left = 307
        Top = 8
        Width = 18
        Height = 13
        Caption = 'DIV'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBText3: TevDBText [9]
        Left = 330
        Top = 24
        Width = 111
        Height = 13
        DataField = 'NAME'
        DataSource = wwdsSubMaster2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBText2: TevDBText [10]
        Left = 330
        Top = 8
        Width = 111
        Height = 13
        DataField = 'NAME'
        DataSource = wwdsSubMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBText10: TevDBText [11]
        Left = 488
        Top = 8
        Width = 137
        Height = 13
        DataField = 'NAME'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      inherited pnlUser: TevPanel
        Left = 608
        Width = 17
      end
    end
  end
  inherited PageControl1: TevPageControl
    Width = 685
    Height = 561
    HelpContext = 13001
    ActivePage = tshtGeneral_Info
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 677
        Height = 532
        inherited pnlBorder: TevPanel
          Width = 673
          Height = 528
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 673
          Height = 528
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 300
                    Align = alLeft
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 288
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 306
                    Width = 484
                    Align = alClient
                    Visible = True
                    object fpDepartmentRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 472
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Caption = 'fpDepartmentRight'
                      Color = 14737632
                      TabOrder = 0
                      OnResize = fpDepartmentRightResize
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Department'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 116
              Width = 542
              Height = 383
              Visible = True
              object Panel4: TevPanel
                Left = 0
                Top = 0
                Width = 542
                Height = 383
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel5: TevPanel
                  Left = 0
                  Top = 0
                  Width = 542
                  Height = 383
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                end
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 625
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 625
            Height = 421
            inherited Splitter1: TevSplitter
              Left = 268
              Height = 417
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 268
              Height = 417
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 218
                Height = 337
                IniAttributes.SectionName = 'TEDIT_CO_DEPARTMENT\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 271
              Width = 350
              Height = 417
              Align = alClient
              Visible = True
              Title = 'Department'
              object Splitter2: TevSplitter
                Left = 337
                Top = 48
                Height = 337
              end
              object Splitter3: TevSplitter
                Left = 176
                Top = 48
                Height = 337
              end
              object wwDBGrid2: TevDBGrid
                Left = 18
                Top = 48
                Width = 158
                Height = 337
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CUSTOM_DIVISION_NUMBER'#9'20'#9'Division Number'#9'F'
                  'NAME'#9'20'#9'Division'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_DEPARTMENT\wwDBGrid2'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alLeft
                DataSource = wwdsSubMaster
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object wwDBGrid3: TevDBGrid
                Left = 179
                Top = 48
                Width = 158
                Height = 337
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CUSTOM_BRANCH_NUMBER'#9'20'#9'Branch Number'#9'F'
                  'NAME'#9'40'#9'Branch'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_DEPARTMENT\wwDBGrid3'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alLeft
                DataSource = wwdsSubMaster2
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object wwDBGrid1: TevDBGrid
                Left = 340
                Top = 48
                Width = 185
                Height = 337
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CUSTOM_DEPARTMENT_NUMBER'#9'23'#9'Department Number'#9'F'
                  'NAME'#9'40'#9'Department'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SaveToRegistry = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_DEPARTMENT\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 2
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              inline CopyHelper: TDBDTCopyHelper
                Left = 80
                Top = 72
                Width = 114
                Height = 54
                TabOrder = 3
                Visible = False
              end
            end
          end
        end
      end
    end
    object tshtGeneral_Info: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbDepartmentDetails: TScrollBox
        Left = 0
        Top = 0
        Width = 677
        Height = 532
        Align = alClient
        TabOrder = 0
        object fpDepartment: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 294
          Height = 305
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDepartment'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Department'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object EvBevel2: TEvBevel
            Left = 12
            Top = 237
            Width = 260
            Height = 3
            Shape = bsTopLine
          end
          object lablName: TevLabel
            Left = 12
            Top = 74
            Width = 49
            Height = 13
            AutoSize = False
            Caption = 'Name'
            FocusControl = dedtName
          end
          object lablAddress1: TevLabel
            Left = 12
            Top = 113
            Width = 47
            Height = 13
            Caption = 'Address 1'
            FocusControl = dedtAddress1
          end
          object lablAddress2: TevLabel
            Left = 12
            Top = 152
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = dedtAddress2
          end
          object lablCity: TevLabel
            Left = 12
            Top = 191
            Width = 17
            Height = 13
            Caption = 'City'
            FocusControl = dedtCity
          end
          object lablState: TevLabel
            Left = 146
            Top = 191
            Width = 25
            Height = 13
            Caption = 'State'
            FocusControl = dedtCity
          end
          object lablZip_Code: TevLabel
            Left = 193
            Top = 191
            Width = 15
            Height = 13
            Caption = 'Zip'
            FocusControl = dedtCity
          end
          object lablBranch_Code: TevLabel
            Left = 12
            Top = 35
            Width = 92
            Height = 16
            Caption = '~Department Code'
            FocusControl = dedtDepartment_Code
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object qlblShowAddressOnCheck: TevLabel
            Left = 12
            Top = 247
            Width = 82
            Height = 16
            Caption = '~Display Options'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtName: TevDBEdit
            Left = 12
            Top = 89
            Width = 260
            Height = 21
            HelpContext = 13015
            DataField = 'NAME'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAddress1: TevDBEdit
            Left = 12
            Top = 128
            Width = 260
            Height = 21
            HelpContext = 13015
            DataField = 'ADDRESS1'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtAddress2: TevDBEdit
            Left = 12
            Top = 167
            Width = 260
            Height = 21
            HelpContext = 13015
            DataField = 'ADDRESS2'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtCity: TevDBEdit
            Left = 12
            Top = 206
            Width = 126
            Height = 21
            HelpContext = 13015
            DataField = 'CITY'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeState: TevDBEdit
            Left = 146
            Top = 206
            Width = 39
            Height = 21
            HelpContext = 13015
            CharCase = ecUpperCase
            DataField = 'STATE'
            DataSource = wwdsDetail
            Picture.PictureMask = '*{&,@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeZip_Code: TevDBEdit
            Left = 193
            Top = 206
            Width = 79
            Height = 21
            HelpContext = 13015
            DataField = 'ZIP_CODE'
            DataSource = wwdsDetail
            Picture.PictureMask = '*5{#}[-*4#]'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtDepartment_Code: TevDBEdit
            Left = 12
            Top = 50
            Width = 126
            Height = 21
            HelpContext = 13015
            DataField = 'CUSTOM_DEPARTMENT_NUMBER'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbPrint_Dept_Address_Check: TevDBComboBox
            Left = 12
            Top = 262
            Width = 260
            Height = 21
            HelpContext = 13015
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'PRINT_DEPT_ADDRESS_ON_CHECKS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Show and Override Address on Checks'#9'I'
              'Show'#9'N'
              'Hide'#9'X')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
          end
        end
        object fpDivisionContact: TisUIFashionPanel
          Left = 310
          Top = 8
          Width = 294
          Height = 305
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDivisionContact'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Contact'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object EvBevel1: TEvBevel
            Left = 12
            Top = 200
            Width = 260
            Height = 3
            Shape = bsTopLine
          end
          object lablPrimary_Contact: TevLabel
            Left = 12
            Top = 35
            Width = 34
            Height = 13
            Caption = 'Primary'
            FocusControl = dedtPrimary_Contact
          end
          object lablPhone: TevLabel
            Left = 12
            Top = 74
            Width = 31
            Height = 13
            Caption = 'Phone'
            FocusControl = wwdePhone1
          end
          object lablDescription1: TevLabel
            Left = 146
            Top = 74
            Width = 53
            Height = 13
            Caption = 'Description'
            FocusControl = dedtDescription1
          end
          object lablDescription: TevLabel
            Left = 146
            Top = 113
            Width = 53
            Height = 13
            Caption = 'Description'
            FocusControl = dedtDescription
          end
          object lablFax: TevLabel
            Left = 12
            Top = 113
            Width = 17
            Height = 13
            Caption = 'Fax'
            FocusControl = wwdeFax
          end
          object lablE_Mail: TevLabel
            Left = 12
            Top = 152
            Width = 29
            Height = 13
            Caption = 'E-Mail'
            FocusControl = dedtE_Mail
          end
          object lablSecondary_Contact: TevLabel
            Left = 12
            Top = 209
            Width = 51
            Height = 13
            Caption = 'Secondary'
            FocusControl = dedtSecondary_Contact
          end
          object lablDescription2: TevLabel
            Left = 146
            Top = 247
            Width = 53
            Height = 13
            Caption = 'Description'
            FocusControl = dedtDescription2
          end
          object lablPhone2: TevLabel
            Left = 12
            Top = 247
            Width = 31
            Height = 13
            Caption = 'Phone'
          end
          object dedtPrimary_Contact: TevDBEdit
            Left = 12
            Top = 50
            Width = 260
            Height = 21
            HelpContext = 13015
            DataField = 'CONTACT1'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdePhone1: TevDBEdit
            Left = 12
            Top = 89
            Width = 126
            Height = 21
            HelpContext = 13015
            DataField = 'PHONE1'
            DataSource = wwdsDetail
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtDescription1: TevDBEdit
            Left = 146
            Top = 89
            Width = 126
            Height = 21
            HelpContext = 13015
            DataField = 'DESCRIPTION1'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtDescription: TevDBEdit
            Left = 146
            Top = 128
            Width = 126
            Height = 21
            HelpContext = 13015
            DataField = 'FAX_DESCRIPTION'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeFax: TevDBEdit
            Left = 12
            Top = 128
            Width = 126
            Height = 21
            HelpContext = 13015
            DataField = 'FAX'
            DataSource = wwdsDetail
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtE_Mail: TevDBEdit
            Left = 12
            Top = 167
            Width = 260
            Height = 21
            HelpContext = 13015
            DataField = 'E_MAIL_ADDRESS'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtSecondary_Contact: TevDBEdit
            Left = 12
            Top = 224
            Width = 260
            Height = 21
            HelpContext = 13015
            DataField = 'CONTACT2'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdePhone2: TevDBEdit
            Left = 12
            Top = 262
            Width = 126
            Height = 21
            HelpContext = 13015
            DataField = 'PHONE2'
            DataSource = wwdsDetail
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtDescription2: TevDBEdit
            Left = 146
            Top = 262
            Width = 126
            Height = 21
            HelpContext = 13015
            DataField = 'DESCRIPTION2'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpDivisionOptions: TisUIFashionPanel
          Left = 8
          Top = 321
          Width = 596
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDivisionOptions'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Options'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel6: TevLabel
            Left = 12
            Top = 35
            Width = 41
            Height = 13
            Caption = 'Location'
          end
          object Label2: TevLabel
            Left = 242
            Top = 35
            Width = 41
            Height = 13
            Caption = 'G/L Tag'
            FocusControl = DBEdit1
          end
          object evLabel4: TevLabel
            Left = 411
            Top = 35
            Width = 56
            Height = 13
            Caption = 'Worksite ID'
            FocusControl = DBEdit1
          end
          object evLabel5: TevLabel
            Left = 12
            Top = 74
            Width = 86
            Height = 13
            Caption = 'Account Number  '
          end
          object evDBLookupCombo8: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 221
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ACCOUNT_NUMBER'#9'30'#9'Account Number'#9'F'
              'ADDRESS1'#9'30'#9'Address1'#9'F'
              'CITY'#9'20'#9'City'#9'F'
              'ZIP_CODE'#9'10'#9'Zip Code'#9'F'
              'STATE'#9'2'#9'State'#9'F')
            DataField = 'CO_LOCATIONS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_LOCATIONS.CO_LOCATIONS
            LookupField = 'CO_LOCATIONS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object DBEdit1: TevDBEdit
            Left = 242
            Top = 50
            Width = 161
            Height = 21
            HelpContext = 13018
            DataField = 'GENERAL_LEDGER_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit1: TevDBEdit
            Left = 411
            Top = 50
            Width = 161
            Height = 21
            HelpContext = 13018
            DataField = 'WORKSITE_ID'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edtAccountNumber: TevDBEdit
            Left = 12
            Top = 89
            Width = 221
            Height = 21
            HelpContext = 13015
            DataField = 'AccountNumber'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            ReadOnly = True
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = edtAccountNumberChange
            Glowing = False
          end
          object rgPrimary: TevDBRadioGroup
            Left = 242
            Top = 74
            Width = 161
            Height = 37
            Caption = 'Primary'
            Columns = 2
            DataField = 'Primary'
            DataSource = wwdsDetail
            Items.Strings = (
              'Yes'
              'No')
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
          end
        end
      end
    end
    object tshtMisc_Info: TTabSheet
      Caption = 'Bank Accounts'
      ImageIndex = 38
      object sbDepartmentBanks: TScrollBox
        Left = 0
        Top = 0
        Width = 677
        Height = 532
        Align = alClient
        TabOrder = 0
        object fpDivisionBanks: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 307
          Height = 211
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDivisionBanks'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Bank Accounts'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablPayroll_Bank_Account: TevLabel
            Left = 12
            Top = 35
            Width = 31
            Height = 13
            Caption = 'Payroll'
            FocusControl = wwlcPayroll_Bank_Account
          end
          object lablBilling_Bank_Account: TevLabel
            Left = 12
            Top = 74
            Width = 27
            Height = 13
            Caption = 'Billing'
            FocusControl = wwlcBilling_Bank_Account
          end
          object lablTax_Bank_Account: TevLabel
            Left = 12
            Top = 113
            Width = 18
            Height = 13
            Caption = 'Tax'
            FocusControl = wwlcTax_Bank_Account
          end
          object lablDD_Bank_Account: TevLabel
            Left = 12
            Top = 152
            Width = 67
            Height = 13
            Caption = 'Direct Deposit'
            FocusControl = wwlcDD_Bank_Account
          end
          object wwlcPayroll_Bank_Account: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 273
            Height = 21
            HelpContext = 13003
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name')
            DataField = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcBilling_Bank_Account: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 273
            Height = 21
            HelpContext = 13003
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name')
            DataField = 'BILLING_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcTax_Bank_Account: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 273
            Height = 21
            HelpContext = 13003
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name')
            DataField = 'TAX_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcDD_Bank_Account: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 273
            Height = 21
            HelpContext = 13003
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name')
            DataField = 'DD_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
      end
    end
    object tshtDefaults: TTabSheet
      Caption = 'Defaults'
      ImageIndex = 37
      object sbDepartmentDefaults: TScrollBox
        Left = 0
        Top = 0
        Width = 677
        Height = 532
        Align = alClient
        TabOrder = 0
        object fpDivisionDefaults: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 307
          Height = 209
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDivisionDefaults'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Defaults'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablHome_State: TevLabel
            Left = 12
            Top = 74
            Width = 65
            Height = 16
            Caption = '~Home State'
            FocusControl = wwlcHome_State
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablOverride_Pay_Rate: TevLabel
            Left = 120
            Top = 74
            Width = 44
            Height = 13
            Caption = 'Pay Rate'
          end
          object lablOverride_EE_Rate: TevLabel
            Left = 206
            Top = 74
            Width = 60
            Height = 13
            Caption = 'EE Rate Nbr'
            FocusControl = dedtOverride_EE_Rate
          end
          object evLabel3: TevLabel
            Left = 12
            Top = 113
            Width = 73
            Height = 13
            Caption = 'New Hire Local'
          end
          object Label6: TevLabel
            Left = 12
            Top = 152
            Width = 70
            Height = 13
            Caption = 'Workers Comp'
          end
          object drgpHomeStateType: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 272
            Height = 37
            HelpContext = 13019
            Caption = '~Default Type'
            Columns = 2
            DataField = 'HOME_STATE_TYPE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Default'
              'Override')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'D'
              'O')
          end
          object wwlcHome_State: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 100
            Height = 21
            HelpContext = 13004
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'HOME_CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object dedtOverride_EE_Rate: TevDBEdit
            Left = 206
            Top = 89
            Width = 78
            Height = 21
            HelpContext = 13012
            DataField = 'OVERRIDE_EE_RATE_NUMBER'
            DataSource = wwdsDetail
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeOverride_Pay_Rate: TevDBEdit
            Left = 120
            Top = 89
            Width = 78
            Height = 21
            HelpContext = 13011
            DataField = 'OVERRIDE_PAY_RATE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*12[#][.*4[#]]'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBLookupCombo6: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 272
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'LocalName'#9'20'#9'LocalName'#9'F')
            DataField = 'NEW_HIRE_CO_LOCAL_TAX_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
            LookupField = 'CO_LOCAL_TAX_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcWorkers_Comp_Number: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 73
            Height = 21
            HelpContext = 10624
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'WORKERS_COMP_CODE'#9'5'#9'W/C Code'#9'F'
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F'
              'Co_State_Lookup'#9'20'#9'Co_State_Lookup'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwDBLookupCombo4: TevDBLookupCombo
            Left = 231
            Top = 167
            Width = 53
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'Co_State_Lookup'#9'20'#9'Co_State_Lookup'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 6
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwDBLookupCombo1: TevDBLookupCombo
            Left = 93
            Top = 167
            Width = 130
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 7
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
        object fpDivisionNotes: TisUIFashionPanel
          Left = 8
          Top = 225
          Width = 307
          Height = 169
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDivisionNotes'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Notes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object dmemBranch_Notes: TEvDBMemo
            Left = 12
            Top = 35
            Width = 272
            Height = 112
            DataField = 'DEPARTMENT_NOTES'
            DataSource = wwdsDetail
            ScrollBars = ssVertical
            TabOrder = 0
          end
        end
      end
    end
    object tbshLocals: TTabSheet
      Caption = 'Locals'
      ImageIndex = 17
      object sbDepartmentLocals: TScrollBox
        Left = 0
        Top = 0
        Width = 677
        Height = 532
        Align = alClient
        TabOrder = 0
        inline TDBDTLocals1: TDBDTLocals
          Left = 0
          Top = 0
          Width = 673
          Height = 528
          Align = alClient
          TabOrder = 0
          inherited pnlTop: TevPanel
            Left = 623
            Top = 137
            Width = 1035
            Height = 607
          end
          inherited pnlBottom: TevPanel
            Top = 607
            Width = 1035
          end
          inherited fpDBDTSummary: TisUIFashionPanel
            inherited evDBGrid1: TevDBGrid
              Selected.Strings = (
                'LocalName'#9'39'#9'Local Name'#9'F')
              IniAttributes.Enabled = False
              IniAttributes.SaveToRegistry = False
              PopupMenu = pmLocalCopy
              PaintOptions.AlternatingRowColor = 14544093
            end
          end
          inherited isUIFashionPanel1: TisUIFashionPanel
            Title = 'Local Detail'
            inherited evLabelLocalName: TevLabel
              Width = 28
              Caption = 'Name'
            end
          end
          inherited dsLocals: TevDataSource
            DataSet = DM_CO_DEPARTMENT_LOCALS.CO_DEPARTMENT_LOCALS
            MasterDataSource = wwdsDetail
          end
        end
      end
    end
    object tsVmr: TTabSheet
      Caption = 'Mail Room'
      ImageIndex = 3
      object sbDepartmentMailRoom: TScrollBox
        Left = 0
        Top = 0
        Width = 677
        Height = 532
        Align = alClient
        TabOrder = 0
        object fpMailRoom: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 381
          Height = 288
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpMailRoom'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Mail Box Overrides'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel20: TevLabel
            Left = 12
            Top = 35
            Width = 65
            Height = 13
            Caption = 'Payroll Check'
          end
          object evLabel21: TevLabel
            Left = 12
            Top = 74
            Width = 65
            Height = 13
            Caption = 'DBDT Report'
          end
          object evLabel22: TevLabel
            Left = 12
            Top = 113
            Width = 86
            Height = 13
            Caption = '2nd DBDT Report'
          end
          object evLabel25: TevLabel
            Left = 12
            Top = 230
            Width = 99
            Height = 13
            Caption = 'EE Electronic Return'
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 152
            Width = 49
            Height = 13
            Caption = 'EE Report'
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 191
            Width = 70
            Height = 13
            Caption = '2nd EE Report'
          end
          object evDBLookupCombo2: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_CHECK_MB_GROUP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo3: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_DBDT_REPORT_MB_GROUP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo4: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_DBDT_REPORT_SC_MB_GROUP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo7: TevDBLookupCombo
            Left = 12
            Top = 245
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'TAX_EE_RETURN_MB_GROUP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_EE_REPORT_MB_GROUP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo5: TevDBLookupCombo
            Left = 12
            Top = 206
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 144
    Top = 18
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
    MasterDataSource = wwdsSubMaster2
    Left = 342
    Top = 22
  end
  inherited wwdsList: TevDataSource
    Left = 98
    Top = 106
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 275
    Top = 18
  end
  inherited dsCL: TevDataSource
    Left = 244
    Top = 10
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 422
    Top = 18
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 464
    Top = 53
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_CO_DIVISION.CO_DIVISION
    MasterDataSource = wwdsMaster
    Left = 376
    Top = 24
  end
  object wwdsSubMaster2: TevDataSource
    DataSet = DM_CO_BRANCH.CO_BRANCH
    MasterDataSource = wwdsSubMaster
    Left = 120
    Top = 40
  end
  object pdsDetail: TevProxyDataSet
    Dataset = DM_CO_DEPARTMENT.CO_DEPARTMENT
    OnNewRecord = pdsDetailNewRecord
    Left = 188
    Top = 13
  end
  object pmLocalCopy: TevPopupMenu
    Left = 525
    Top = 37
    object MenuItem3: TMenuItem
      Action = CopyService
    end
  end
  object LocalCopyService: TevActionList
    Left = 536
    Top = 355
    object CopyService: TAction
      Caption = 'Copy To...'
      OnExecute = CopyServiceExecute
    end
  end
end
