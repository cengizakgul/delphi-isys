inherited EDIT_CO_WORKERS_COMP: TEDIT_CO_WORKERS_COMP
  inherited Panel1: TevPanel
    inherited pnlTopLeft: TevPanel
      inherited dbtxClientNbr: TevDBText
        Left = 68
      end
      inherited dbtxClientName: TevDBText
        Left = 140
      end
      inherited DBText1: TevDBText
        Left = 68
      end
      inherited CompanyNameText: TevDBText
        Left = 140
      end
    end
  end
  inherited PageControl1: TevPageControl
    HelpContext = 43001
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Workers Compensation'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCORightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 352
              Top = 96
              Width = 267
              Height = 456
              Visible = True
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 308
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 308
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 258
                Height = 203
                IniAttributes.SectionName = 'TEDIT_CO_WORKERS_COMP\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 311
              Width = 60
              Align = alClient
              Caption = ''
              Visible = True
              Title = 'Workers Compensation'
              object wwDBGrid2: TevDBGrid
                Left = 18
                Top = 48
                Width = 10
                Height = 203
                DisableThemesInTitle = False
                Selected.Strings = (
                  'WORKERS_COMP_CODE'#9'5'#9'W/C Code'#9'F'
                  'Co_State_Lookup'#9'20'#9'State'#9'F'
                  'DESCRIPTION'#9'40'#9'Description'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_WORKERS_COMP\wwDBGrid2'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              inline CopyHelper: TCompanyCopyHelper
                Left = 112
                Top = 96
                Width = 115
                Height = 54
                TabOrder = 1
                Visible = False
              end
            end
          end
        end
      end
    end
    object Detail: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object fpCOWorkerSummary: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 649
        Height = 201
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'pnlSummary'
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Workers Compensation'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object wwDBGrid1: TevDBGrid
          Left = 12
          Top = 35
          Width = 616
          Height = 145
          DisableThemesInTitle = False
          Selected.Strings = (
            'WORKERS_COMP_CODE'#9'5'#9'W/C Code'
            'StateName'#9'20'#9'State'#9'F'
            'DESCRIPTION'#9'40'#9'Description'
            'RATE'#9'10'#9'Rate'
            'EXPERIENCE_RATING'#9'10'#9'Experience Rating'
            'CL_E_D_GROUPS_NBR'#9'10'#9'CL E/D Groups Nbr'#9'F')
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CO_WORKERS_COMP\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsDetail
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
      object fpCOWorkerDetails: TisUIFashionPanel
        Left = 12
        Top = 218
        Width = 649
        Height = 170
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'pnlSummary'
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Workers Compensation Details'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object Label1: TevLabel
          Left = 12
          Top = 35
          Width = 60
          Height = 16
          Caption = '~W/C Code'
          FocusControl = DBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TevLabel
          Left = 166
          Top = 35
          Width = 62
          Height = 16
          Caption = '~Description'
          FocusControl = DBEdit4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TevLabel
          Left = 495
          Top = 35
          Width = 34
          Height = 16
          Caption = '~State'
          FocusControl = wwDBLookupCombo1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel10: TevLabel
          Left = 12
          Top = 74
          Width = 36
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'GL Tag'
          FocusControl = evDBEdit5
          ParentBiDiMode = False
        end
        object Label6: TevLabel
          Left = 166
          Top = 74
          Width = 23
          Height = 13
          Caption = 'Rate'
        end
        object evLabel27: TevLabel
          Left = 330
          Top = 74
          Width = 67
          Height = 13
          Caption = 'GL Offset Tag'
          FocusControl = evDBEdit2
        end
        object Label7: TevLabel
          Left = 495
          Top = 74
          Width = 87
          Height = 13
          Caption = 'Experience Rating'
        end
        object Label8: TevLabel
          Left = 12
          Top = 113
          Width = 55
          Height = 13
          Caption = 'E/D Group '
          FocusControl = wwDBLookupCombo2
        end
        object DBEdit1: TevDBEdit
          Left = 12
          Top = 50
          Width = 146
          Height = 21
          DataField = 'WORKERS_COMP_CODE'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit4: TevDBEdit
          Left = 166
          Top = 50
          Width = 320
          Height = 21
          DataField = 'DESCRIPTION'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object evDBEdit5: TevDBEdit
          Left = 12
          Top = 89
          Width = 146
          Height = 21
          HelpContext = 10668
          DataField = 'GL_TAG'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBLookupCombo1: TevDBLookupCombo
          Left = 495
          Top = 50
          Width = 131
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'STATE'#9'2'#9'STATE')
          DataField = 'CO_STATES_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CO_STATES.CO_STATES
          LookupField = 'CO_STATES_NBR'
          Style = csDropDownList
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object wwdeRate: TevDBEdit
          Left = 166
          Top = 89
          Width = 156
          Height = 21
          DataField = 'RATE'
          DataSource = wwdsDetail
          Picture.PictureMask = 
            '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
            '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
            '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object evDBEdit2: TevDBEdit
          Left = 330
          Top = 89
          Width = 156
          Height = 21
          HelpContext = 10668
          DataField = 'GL_OFFSET_TAG'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwdeExperience_Rating: TevDBEdit
          Left = 495
          Top = 89
          Width = 131
          Height = 21
          DataField = 'EXPERIENCE_RATING'
          DataSource = wwdsDetail
          Picture.PictureMask = 
            '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
            '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
            '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBLookupCombo2: TevDBLookupCombo
          Left = 12
          Top = 128
          Width = 146
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME')
          DataField = 'CL_E_D_GROUPS_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
          LookupField = 'CL_E_D_GROUPS_NBR'
          Style = csDropDownList
          TabOrder = 7
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object DBRadioGroup3: TevDBRadioGroup
          Left = 166
          Top = 113
          Width = 460
          Height = 37
          HelpContext = 1502
          Caption = '~Overtime to Reduce'
          Columns = 3
          DataField = 'SUBTRACT_PREMIUM'
          DataSource = wwdsDetail
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Overtime Only'
            'Overtime and Premium'
            'None')
          ParentFont = False
          TabOrder = 8
          Values.Strings = (
            'Y'
            'N'
            'X')
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
  end
end
