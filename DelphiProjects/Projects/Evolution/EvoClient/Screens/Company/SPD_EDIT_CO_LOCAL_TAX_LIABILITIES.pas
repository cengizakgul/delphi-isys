// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_LOCAL_TAX_LIABILITIES;

interface

uses
  SDataStructure,  SPD_EDIT_CO_TAX_LIABILITIES_BASE, StdCtrls,
  DBCtrls, Db,  Wwdatsrc, wwdbdatetimepicker, wwdbedit,
  wwdblook, ExtCtrls, Buttons, Mask, Wwdotdot, Wwdbcomb, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, Controls, Classes, SysUtils, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDDClasses, SDataDictclient, SDataDicttemp, ISBasicClasses , variants,
  EvContext, EvUIComponents, EvClientDataSet, ImgList, isUIwwDBLookupCombo,
  isUIEdit, isUIwwDBEdit, isUIwwDBDateTimePicker, isUIDBMemo,
  isUIwwDBComboBox, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_LOCAL_TAX_LIABILITIES = class(TEDIT_CO_TAX_LIABILITIES_BASE)
    lablDue_Date: TevLabel;
    lablAdjustment_Type: TevLabel;
    lablStatus: TevLabel;
    lablLocal: TevLabel;
    lablAmount: TevLabel;
    wwcbAdjustment_Type: TevDBComboBox;
    wwcbStatus: TevDBComboBox;
    wwlcLocal: TevDBLookupCombo;
    wwdeAmount: TevDBEdit;
    wwdpDue_Date: TevDBDateTimePicker;
    cbxWorkCoLocalTax: TevDBLookupCombo;
    evLabel5: TevLabel;
    cbkWorkLocation: TevCheckBox;
    wwDBGrid3x: TevDBGrid;
    evLabel7: TevLabel;
    cbxWorkLocationUpdate: TevDBLookupCombo;
    btnLocalSubTotals: TevBitBtn;
    csWorkLocationSubTotals: TevClientDataSet;
    dsWorkLocationSubTotals: TevDataSource;
    csWorkLocationSubTotalsAmount: TCurrencyField;
    csWorkLocationSubTotalsACCOUNT_NUMBER: TStringField;
    csWorkLocationSubTotalsCO_LOCATIONS_NBR: TIntegerField;
    fpLocalRight: TisUIFashionPanel;
    procedure cbkWorkLocationClick(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure btnLocalSubTotalsClick(Sender: TObject);
    procedure wwdgTaxLiabilitiesSelectionChanged(Sender: TObject);
    procedure csWorkLocationSubTotalsCalcFields(DataSet: TDataSet);
    procedure Panel3Resize(Sender: TObject);
    procedure fpLocalRightResize(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;

  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure ReCalcTotals; override;
    procedure Activate; override;
    constructor Create(AOwner: TComponent); override;
  end;


implementation

uses EvConsts, EvUtils, SPD_SelectSL, SPackageEntry;

{$R *.DFM}

procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.Activate;
begin
  inherited;
  wwcbStatus.OnDropDown := wwcbStatusDropDown;
  wwcbStatus.OnCloseUp := wwcbStatusCloseUp;
  cbkWorkLocation.Checked := False;
  wwDBGrid3x.Visible := cbkWorkLocation.Checked;
  wwDBGrid2x.Visible := not cbkWorkLocation.Checked;
end;


procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
   DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.AlwaysCallCalcFields := true;
  inherited;
  AddDS(DM_PAYROLL.CO_TAX_DEPOSITS, SupportDataSets);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, SupportDataSets);
  AddDS(DM_COMPANY.CO_LOCATIONS, SupportDataSets);
end;

procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.AlwaysCallCalcFields := true;
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  AddDS(DM_COMPANY.CO_TAX_DEPOSITS, aDS);
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);
  inherited;
end;

function TEDIT_CO_LOCAL_TAX_LIABILITIES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_LOCAL_TAX_LIABILITIES;
end;

function TEDIT_CO_LOCAL_TAX_LIABILITIES.GetInsertControl: TWinControl;
begin
  Result := wwlcLocal;
end;

procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.ReCalcTotals;
var
  TotalTax: Currency;
begin
  TotalTax := 0;
  with wwdsDetail.DataSet do
  begin
    First;
    DisableControls;
    while not EOF do
    begin
                //  Added By CA...  If any record of AMOUNT has null,  skip it
      if not VarIsNull(wwdsDetail.DataSet.FieldByName('AMOUNT').Value) then
        TotalTax := TotalTax + wwdsDetail.DataSet.FieldByName('AMOUNT').Value;

      Next;
    end;
    First;
    EnableControls;
  end;
  lablTotalTaxValue.Caption := CurrToStrF(TotalTax, ffCurrency, 2);
end;

procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.cbkWorkLocationClick(
  Sender: TObject);
begin
  inherited;
  wwDBGrid3x.Visible := cbkWorkLocation.Checked;
  wwDBGrid2x.Visible := not cbkWorkLocation.Checked;

end;


procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.wwdsDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (csLoading in ComponentState) then
    exit;

  if (wwdsDetail.State = dsBrowse) and (Field = nil) then
    cbxWorkCoLocalTax.Enabled := (wwdsDetail.DataSet.FieldByName('Local_State').AsString = 'PA') and
                                 (wwdsDetail.DataSet.FieldByName('Local_Type').asString[1] in [LOCAL_TYPE_INC_NON_RESIDENTIAL, LOCAL_TYPE_INC_RESIDENTIAL, LOCAL_TYPE_SCHOOL]);


end;


procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.btnLocalSubTotalsClick(Sender: TObject);
var sql : string;
var
  tmpTotal : Currency;
begin
   sql := ' select CO_LOCATIONS_NBR, sum(Amount) AMOUNT '+
          ' from CO_LOCAL_TAX_LIABILITIES where co_nbr =:CONBR and {AsOfNow<CO_LOCAL_TAX_LIABILITIES>} '+
          ' %s '+
          ' Group by CO_LOCATIONS_NBR '+
          ' having CO_LOCATIONS_NBR is not null ';

  if cbGroup.Checked and (wwdsSubMaster.DataSet.FieldByName('PR_NBR').AsInteger > 0 ) then
    sql := Format(sql, [' and PR_NBR = '+ wwdsSubMaster.DataSet.FieldByName('PR_NBR').AsString])
  else
    sql := Format(sql, ['']);

  with TExecDSWrapper.Create(sql) do
  begin
    SetParam('CONBR', wwdsMaster.DataSet['CO_NBR']);
    ctx_DataAccess.GetCustomData(csWorkLocationSubTotals, AsVariant);
  end;
  with TSelectSLDlg.Create(Self) do
  begin
    tmpTotal := 0;
    csWorkLocationSubTotals.First;
    while not csWorkLocationSubTotals.Eof do
    begin
      tmpTotal := tmpTotal + csWorkLocationSubTotals.FieldByName('AMOUNT').asFloat;
     csWorkLocationSubTotals.next;
    end;
    Caption := 'Local Subtotals';
    CancelBtn.Visible := false;
    OKBtn.Left := OKBtn.Left + 50;
    lblTotal.Caption := 'Total Taxes  =  ' + CurrToStrF(tmpTotal, ffCurrency, 2);
    lblTotal.Visible :=  True;
    grdSelect.DataSource := dsWorkLocationSubTotals;
    grdSelect.Selected.Text := 'ACCOUNT_NUMBER'#9'30'#9'Account Number'#9'F' + #13#10 +
                               'AMOUNT'#9'12'#9'Amount'#9'F';
    if ShowModal = mrOK then
    begin
    end;
    Free;
  end;
end;

procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.csWorkLocationSubTotalsCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  if Dataset.FieldByName('CO_LOCATIONS_NBR').AsInteger > 0 then
     Dataset.FieldByName('ACCOUNT_NUMBER').AsString := DM_CLIENT.CO_LOCATIONS.ShadowDataSet.CachedLookup(Dataset.FieldByName('CO_LOCATIONS_NBR').AsInteger,'ACCOUNT_NUMBER')
  else
     Dataset.FieldByName('ACCOUNT_NUMBER').AsString := '';
end;


procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.wwdgTaxLiabilitiesSelectionChanged(
  Sender: TObject);
var
    i : integer;
    pBookMark: TBookmark;
begin

  pBookMark := wwdsDetail.DataSet.GetBookmark;
  try
    cbxWorkCoLocalTax.Enabled := False;
    for i := 0 to wwdgTaxLiabilities.SelectedList.Count - 1 do
    begin
      wwdsDetail.DataSet.GotoBookmark(wwdgTaxLiabilities.SelectedList[i]);
      cbxWorkLocationUpdate.Enabled := (wwdsDetail.DataSet.FieldByName('Local_State').AsString = 'PA') and
                                   (wwdsDetail.DataSet.FieldByName('Local_Type').asString[1] in [LOCAL_TYPE_INC_NON_RESIDENTIAL, LOCAL_TYPE_INC_RESIDENTIAL, LOCAL_TYPE_SCHOOL]);
      if cbxWorkLocationUpdate.Enabled then
        break;
    end;
   finally
    wwdsDetail.DataSet.GotoBookmark(pBookMark);
    wwdsDetail.DataSet.FreeBookmark(pBookMark);
   end;
  inherited;

end;

procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.Panel3Resize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 MOVE STUFF AROUND
  cbkWorkLocation.Left := Panel3.Width - 115;
  cbShowCredit.Left    := cbkWorkLocation.Left - 135;
  cbGroup.Left         := cbShowCredit.Left - 115;
end;

constructor TEDIT_CO_LOCAL_TAX_LIABILITIES.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENTS
  pnlSubbrowse.Parent := fpLocalRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_LOCAL_TAX_LIABILITIES.fpLocalRightResize(
  Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpLocalRight.Width - 40;
  pnlSubbrowse.Height := fpLocalRight.Height - 65;
end;

initialization
  RegisterClass(TEDIT_CO_LOCAL_TAX_LIABILITIES);

end.
