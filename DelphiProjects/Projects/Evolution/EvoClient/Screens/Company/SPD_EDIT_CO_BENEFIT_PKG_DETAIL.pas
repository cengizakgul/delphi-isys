// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BENEFIT_PKG_DETAIL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBLookupCombo,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, Mask, wwdbedit,
  isUIwwDBEdit, Wwdotdot, Wwdbcomb, isUIwwDBComboBox;

type
  TEDIT_CO_BENEFIT_PKG_DETAIL= class(TEDIT_CO_BASE)
    tshtPensions: TTabSheet;
    wwdgPension: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    fpCOPkgDetailSummary: TisUIFashionPanel;
    fpCOBenefitPkgDetails: TisUIFashionPanel;
    sbPensions: TScrollBox;
    evLabel1: TevLabel;
    evBenefit: TevDBLookupCombo;
    wwdsSubMaster: TevDataSource;
    evSplitter2: TevSplitter;
    fpPackageDetails: TisUIFashionPanel;
    wwdgEDGROUPCODES: TevDBGrid;
    fpPackage: TisUIFashionPanel;
    lblDescription: TevLabel;
    evPackage: TevDBLookupCombo;
  private
    FEnableHr: boolean;
  protected
    function GetIfReadOnly: Boolean; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetDataSetConditions(sName: string): string; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;    
  public
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;    
  end;

implementation

uses SPackageEntry;
{$R *.DFM}

function TEDIT_CO_BENEFIT_PKG_DETAIL.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if (not Result) then
     result:= FEnableHr;
end;

procedure TEDIT_CO_BENEFIT_PKG_DETAIL.AfterDataSetsReopen;
begin
  inherited;
  FEnableHr := GetEnableHrBenefit(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  if GetIfReadOnly then
     SetReadOnly(True)
  else
    ClearReadOnly;
end;

function TEDIT_CO_BENEFIT_PKG_DETAIL.GetDataSetConditions(
  sName: string): string;
begin
  if sName = 'CO_BENEFIT_PKG_DETAIL' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_BENEFIT_PKG_DETAIL.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_COMPANY.CO_BENEFITS, aDS);
  AddDS(DM_COMPANY.CO_BENEFIT_PACKAGE, aDS);
end;

function TEDIT_CO_BENEFIT_PKG_DETAIL.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BENEFIT_PKG_DETAIL;
end;

function TEDIT_CO_BENEFIT_PKG_DETAIL.GetInsertControl: TWinControl;
begin
  Result := evPackage;
end;

procedure TEDIT_CO_BENEFIT_PKG_DETAIL.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind in [NavOK, NavCommit] then
  begin
    if wwdsDetail.DataSet.State in [dsEdit, dsInsert] then
       wwdsDetail.DataSet.FieldByName('CO_BENEFIT_PACKAGE_NBR').AsInteger :=
           DM_COMPANY.CO_BENEFIT_PACKAGE.FieldByName('CO_BENEFIT_PACKAGE_NBR').AsInteger
  end;
end;

initialization
  RegisterClass(TEDIT_CO_BENEFIT_PKG_DETAIL);

end.
