// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BANK_ACCOUNT_REGISTER;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, DBCtrls, StdCtrls, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, Mask, wwriched,
  wwdbdatetimepicker, wwdbedit, SDataStructure,  EvUIComponents, EvClientDataSet,
  ISBasicClasses, isUIwwDBDateTimePicker, isUIwwDBEdit, SDDClasses,
  SDataDictclient, ImgList, SDataDicttemp, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_BANK_ACCOUNT_REGISTER = class(TEDIT_CO_BASE)
    TabSheet2: TTabSheet;
    wwDBGrid1: TevDBGrid;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    Label4: TevLabel;
    Label6: TevLabel;
    Label7: TevLabel;
    Label8: TevLabel;
    Label9: TevLabel;
    Label10: TevLabel;
    Label11: TevLabel;
    Label12: TevLabel;
    Label13: TevLabel;
    Label14: TevLabel;
    Label15: TevLabel;
    Label16: TevLabel;
    Label17: TevLabel;
    wwDBEdit1: TevDBEdit;
    wwDBEdit2: TevDBEdit;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    wwDBEdit3: TevDBEdit;
    wwDBDateTimePicker2: TevDBDateTimePicker;
    wwDBDateTimePicker3: TevDBDateTimePicker;
    wwDBEdit4: TevDBEdit;
    wwDBEdit5: TevDBEdit;
    wwDBEdit6: TevDBEdit;
    wwDBEdit7: TevDBEdit;
    wwDBEdit8: TevDBEdit;
    wwDBDateTimePicker4: TevDBDateTimePicker;
    wwDBEdit9: TevDBEdit;
    wwDBEdit10: TevDBEdit;
    wwDBEdit11: TevDBEdit;
    wwDBRichEdit1: TevDBRichEdit;
    wwDBDateTimePicker5: TevDBDateTimePicker;
    lablCloseDate: TevLabel;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  end;

implementation

{$R *.DFM}

function TEDIT_CO_BANK_ACCOUNT_REGISTER.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BANK_ACCOUNT_REGISTER; 
end;

initialization
  RegisterClass(TEDIT_CO_BANK_ACCOUNT_REGISTER);

end.
