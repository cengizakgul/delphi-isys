inherited EDIT_CO_PENSIONS: TEDIT_CO_PENSIONS
  Width = 671
  Height = 637
  inherited Panel1: TevPanel
    Width = 671
    inherited pnlFavoriteReport: TevPanel
      Left = 519
    end
    inherited pnlTopLeft: TevPanel
      Width = 519
    end
  end
  inherited PageControl1: TevPageControl
    Width = 671
    Height = 583
    HelpContext = 29501
    ActivePage = tshtPensions
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 663
        Height = 554
        inherited pnlBorder: TevPanel
          Width = 659
          Height = 550
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 659
          Height = 550
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Pensions'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 371
              Top = 59
              Width = 379
              Height = 520
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 611
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 611
            Height = 443
            inherited Splitter1: TevSplitter
              Left = 320
              Height = 439
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Height = 439
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 359
                IniAttributes.SectionName = 'TEDIT_CO_PENSIONS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 284
              Height = 439
              Align = alClient
              Visible = True
              Title = 'Pensions'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 234
                Height = 359
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CO_PENSIONS_NBR'#9'10'#9'Co Pensions Nbr'#9'F'
                  'PensionName'#9'40'#9'Pensionname'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_PENSIONS\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtPensions: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbPensions: TScrollBox
        Left = 0
        Top = 0
        Width = 663
        Height = 554
        Align = alClient
        TabOrder = 0
        object fpCOPensionDetails: TisUIFashionPanel
          Left = 8
          Top = 385
          Width = 314
          Height = 92
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Pension Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablPension: TevLabel
            Left = 12
            Top = 35
            Width = 46
            Height = 13
            Caption = '~Pension'
            FocusControl = wwlcPension
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwlcPension: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 281
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'PENSION_NAME')
            DataField = 'CL_PENSION_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_PENSION.CL_PENSION
            LookupField = 'CL_PENSION_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
        object fpCOPensionSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 314
          Height = 369
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgPension: TevDBGrid
            Left = 12
            Top = 35
            Width = 281
            Height = 313
            TabStop = False
            DisableThemesInTitle = False
            Selected.Strings = (
              'PensionName'#9'40'#9'Pensionname'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_PENSIONS\wwdgPension'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_PENSIONS.CO_PENSIONS
  end
  inherited dsCL: TevDataSource
    Left = 264
  end
end
