inherited EDIT_CO_SUI_LIABILITIES: TEDIT_CO_SUI_LIABILITIES
  inherited PageControl1: TevPageControl
    HelpContext = 52001
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited splEDIT_CO_BASE: TevSplitter
                    Left = 237
                    Width = 3
                    Visible = True
                  end
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 231
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 219
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 240
                    Width = 550
                    Visible = True
                    object fpSUIRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 538
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Caption = 'fpSUIRight'
                      Color = 14737632
                      TabOrder = 0
                      OnResize = fpSUIRightResize
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'SUI'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            OnResize = Panel3Resize
            inherited cbGroup: TevCheckBox
              Left = 545
              HelpContext = 10000
            end
            inherited cbShowCredit: TevCheckBox
              Left = 660
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 424
                IniAttributes.SectionName = 'TEDIT_CO_SUI_LIABILITIES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'SUI'
              inherited Splitter2: TevSplitter
                Height = 424
              end
              inherited wwDBGrid2x: TevDBGrid
                Width = 384
                Height = 424
                Selected.Strings = (
                  'SUI_NAME'#9'10'#9'Name'#9'F'
                  'SUI_STATE'#9'2'#9'State'#9'F'
                  'AMOUNT'#9'12'#9'Amount'#9'F'
                  'DUE_DATE'#9'10'#9'Due date'#9'F'
                  'CHECK_DATE'#9'10'#9'Check date'#9'F'
                  'ADJUSTMENT_TYPE'#9'10'#9'Adj. type'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_SUI_LIABILITIES\wwDBGrid2x'
              end
              inherited wwDBGrid1x: TevDBGrid
                Height = 424
                Selected.Strings = (
                  'RUN_NUMBER'#9'5'#9'Run #'#9'F'
                  'CHECK_DATE'#9'10'#9'Check Date'#9'F'
                  'SCHEDULED_PROCESS_DATE'#9'10'#9'Scheduled Process Date'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_SUI_LIABILITIES\wwDBGrid1x'
              end
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited sbTaxLiabilitiesBase: TScrollBox
        inherited fpLiabilityNotes: TisUIFashionPanel
          Left = 328
          Top = 343
          Width = 459
          TabOrder = 4
          inherited dmemNotes: TEvDBMemo
            Width = 423
          end
        end
        inherited fpBaseLiabilitiesDetails: TisUIFashionPanel
          Width = 312
          Height = 327
          TabOrder = 0
          inherited lablCheck_Date: TevLabel
            Left = 12
            Top = 74
          end
          inherited lablProcess_Date: TevLabel
            Left = 12
            Top = 269
          end
          inherited lablImpound: TevLabel
            Left = 600
            Top = 200
            Visible = False
          end
          inherited lablTax_Deposits: TevLabel
            Left = 12
            Top = 152
          end
          inherited lablPayroll_Run: TevLabel
            Left = 600
            Top = 56
            Visible = False
          end
          object lablDue_Date: TevLabel [5]
            Left = 155
            Top = 74
            Width = 53
            Height = 13
            Caption = '~Due Date'
          end
          object lablAmount: TevLabel [6]
            Left = 155
            Top = 113
            Width = 43
            Height = 13
            Caption = '~Amount'
            FocusControl = wwedAmount
          end
          object lablTax_Type: TevLabel [7]
            Left = 12
            Top = 35
            Width = 25
            Height = 13
            Caption = '~SUI'
            FocusControl = wwlcSUI
          end
          object lablStatus: TevLabel [8]
            Left = 155
            Top = 152
            Width = 37
            Height = 13
            Caption = '~Status'
            FocusControl = wwcbStatus
          end
          object lablAdjustment_Type: TevLabel [9]
            Left = 12
            Top = 113
            Width = 79
            Height = 13
            Caption = 'Adjustment Type'
            FocusControl = wwcbAdjustment_Type
          end
          inherited wwdbtpCheckDate: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 135
            TabOrder = 1
          end
          inherited dedtImpound: TevDBEdit
            Left = 600
            Top = 216
            Width = 120
            TabOrder = 13
            Visible = False
          end
          inherited dedtPayroll_Run: TevDBEdit
            Left = 600
            Top = 72
            Width = 120
            TabOrder = 14
            Visible = False
          end
          inherited wwDBDateTimePicker2: TevDBDateTimePicker
            Left = 12
            Top = 284
            Width = 135
            TabOrder = 11
          end
          inherited drgpThirdParty: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 135
            Height = 37
            TabOrder = 7
          end
          inherited dedtTax_Deposits: TevEdit
            Left = 12
            Top = 167
            Width = 113
          end
          inherited buttonAssociateDeposit: TevBitBtn
            Left = 126
            Top = 167
            TabOrder = 15
            TabStop = False
          end
          inherited evDBRadioGroup1: TevDBRadioGroup
            Left = 155
            Top = 191
            Width = 135
            Height = 37
            TabOrder = 8
          end
          inherited evDBEditAchKey: TevDBEdit
            Left = 155
            Top = 284
            Width = 135
            TabOrder = 12
          end
          inherited evPanelssd1: TevPanel
            Left = 155
            Top = 269
            Height = 13
            ParentColor = True
            TabOrder = 16
            inherited evLabelssd1: TevLabel
              Align = alClient
            end
          end
          inherited evDBDateTimePicker3: TevDBDateTimePicker
            Left = 12
            Top = 245
            Width = 135
            TabOrder = 9
          end
          inherited evDBDateTimePicker4: TevDBDateTimePicker
            Left = 155
            Top = 245
            Width = 135
            TabOrder = 10
          end
          inherited evPanel2: TevPanel
            Left = 12
            Top = 230
            Height = 13
            ParentColor = True
            TabOrder = 17
            inherited evLabel6: TevLabel
              Align = alClient
            end
          end
          inherited evPanel3: TevPanel
            Left = 155
            Top = 230
            Height = 13
            ParentColor = True
            TabOrder = 18
            inherited evLabel4: TevLabel
              Align = alClient
            end
          end
          object wwcbAdjustment_Type: TevDBComboBox
            Left = 12
            Top = 128
            Width = 135
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'ADJUSTMENT_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object wwcbStatus: TevDBComboBox
            Left = 155
            Top = 167
            Width = 135
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 6
            UnboundDataType = wwDefault
          end
          object wwlcSUI: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 135
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'DESCRIPTION'#9'F'
              'State'#9'2'#9'STATE'#9'F')
            DataField = 'CO_SUI_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_SUI.CO_SUI
            LookupField = 'CO_SUI_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwedAmount: TevDBEdit
            Left = 155
            Top = 128
            Width = 135
            Height = 21
            DataField = 'AMOUNT'
            DataSource = wwdsDetail
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwdpDue_Date: TevDBDateTimePicker
            Left = 155
            Top = 89
            Width = 135
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DUE_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
        end
        inherited fpTaxLiabilities: TisUIFashionPanel
          Left = 328
          TabOrder = 2
          inherited wwdgTaxLiabilities: TevDBGrid
            Selected.Strings = (
              'SUI_NAME'#9'10'#9'Name'#9'F'
              'SUI_STATE'#9'2'#9'State'#9'F'
              'AMOUNT'#9'12'#9'Amount'#9'F'
              'THIRD_PARTY'#9'1'#9'3rd prty'#9'F'
              'CHECK_DATE'#9'10'#9'Check date'#9'F'
              'ACH_KEY'#9'11'#9'ACH key'#9'F')
            IniAttributes.SectionName = 'TEDIT_CO_SUI_LIABILITIES\wwdgTaxLiabilities'
          end
        end
        inherited fpTools: TisUIFashionPanel
          Top = 343
          Width = 312
          Height = 112
          TabOrder = 3
          inherited BitBtn3: TevBitBtn
            Width = 277
          end
          inherited bbtnAttach: TevBitBtn
            Left = 12
            Top = 67
            Width = 277
          end
        end
        inherited Panel4: TevPanel
          Left = 0
          Top = 0
          Width = 322
          Height = 337
          TabOrder = 1
          inherited fpTaxDeposit: TisUIFashionPanel
            Width = 312
            Height = 327
          end
        end
      end
    end
    inherited tsCO_TAX_DEPOSITS: TTabSheet
      inherited sbTaxDeposits: TScrollBox
        inherited fpTaxDeposits: TisUIFashionPanel
          inherited wwDBGrid1: TevDBGrid
            IniAttributes.SectionName = 'TEDIT_CO_SUI_LIABILITIES\wwDBGrid1'
          end
        end
        inherited fpTaxDepositDetail: TisUIFashionPanel
          inherited lablTax_Type2: TevLabel
            Visible = False
          end
          inherited wwcbType2: TevDBComboBox
            DataField = ''
            DataSource = nil
            Visible = False
          end
        end
      end
    end
  end
end
