// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_HR_POSITIONS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Mask,
  wwdbedit, sCompanyCopyHelper, sCopyHelper, Variants, ISBasicClasses,
  SDDClasses, SDataDictclient, SDataDicttemp, SPD_MiniNavigationFrame,
  wwdblook, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvContext, SPackageEntry, EvUIComponents, EvClientDataSet, isUIwwDBEdit, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_HR_POSITIONS = class(TEDIT_CO_BASE)
    tbshDetails: TTabSheet;
    dsSalaryGradesAvailable: TevDataSource;
    dsSalaryGradesSelected: TevDataSource;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    pnlFPCORightBody: TevPanel;
    sbHRPositions: TScrollBox;
    fpCOHRPosition: TisUIFashionPanel;
    evDBGrid2: TevDBGrid;
    fpCOHRPositionSelected: TisUIFashionPanel;
    evGridSelected: TevDBGrid;
    ebBtnDelete: TevBitBtn;
    evBtnUpdate: TevBitBtn;
    fpCOHRPositionAvailable: TisUIFashionPanel;
    evGridAvailable: TevDBGrid;
    fpCOHRPositionDetails: TisUIFashionPanel;
    evLabel1: TevLabel;
    dedtDescription: TevDBEdit;
    evDBGrid1: TevDBGrid;
    CopyHelper: TCompanyCopyHelper;
    Button1: TButton;
    procedure evBtnUpdateClick(Sender: TObject);
    procedure ebBtnDeleteClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    procedure HandleCopy(Sender: TObject; client, company: integer;
      selectedDetails, details: TEvClientDataSet);
    procedure HandleDelete(Sender: TObject; client, company: integer;
      selectedDetails, details: TEvClientDataSet);
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  public
    { Public declarations }
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure Activate; override;
  end;

implementation

uses
  evutils, sCopier;
{$R *.DFM}
{ TEDIT_CO_HR_POSITIONS }



function TEDIT_CO_HR_POSITIONS.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_HR_POSITION_GRADES') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_HR_POSITIONS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS( DM_COMPANY.CO_HR_POSITION_GRADES, EditDataSets);
  AddDS( DM_COMPANY.CO_HR_SALARY_GRADES, SupportDataSets);
end;

procedure TEDIT_CO_HR_POSITIONS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS( DM_COMPANY.CO_HR_POSITION_GRADES, aDS);
  AddDS( DM_COMPANY.CO_HR_SALARY_GRADES, aDS);
  inherited;
end;

function TEDIT_CO_HR_POSITIONS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_HR_POSITIONS
end;

function TEDIT_CO_HR_POSITIONS.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

procedure TEDIT_CO_HR_POSITIONS.evBtnUpdateClick(Sender: TObject);
var
 i : integer;
begin
  inherited;
  DM_COMPANY.CO_HR_SALARY_GRADES.DisableControls;
  DM_COMPANY.CO_HR_POSITION_GRADES.DisableControls;
  try
    if evGridAvailable.SelectedList.Count > 0 then
    begin
      for i := evGridAvailable.SelectedList.Count-1 downto 0 do
      begin
        evGridAvailable.DataSource.DataSet.GotoBookmark(evGridAvailable.SelectedList[i]);

        if not DM_COMPANY.CO_HR_POSITION_GRADES.Locate('CO_HR_POSITIONS_NBR;CO_HR_SALARY_GRADES_NBR',
                       VarArrayOf([ wwdsDetail.DataSet.FieldByName('CO_HR_POSITIONS_NBR').Value,
                                     evGridAvailable.DataSource.DataSet['CO_HR_SALARY_GRADES_NBR']]),[]) then
        begin
          DM_COMPANY.CO_HR_POSITION_GRADES.Insert;
          DM_COMPANY.CO_HR_POSITION_GRADES.FieldByName('CO_HR_POSITIONS_NBR').AsInteger :=
                     wwdsDetail.DataSet.FieldByName('CO_HR_POSITIONS_NBR').asInteger;
          DM_COMPANY.CO_HR_POSITION_GRADES.FieldByName('CO_HR_SALARY_GRADES_NBR').AsInteger :=
                     evGridAvailable.DataSource.DataSet.FieldByName('CO_HR_SALARY_GRADES_NBR').asinteger;
          DM_COMPANY.CO_HR_POSITION_GRADES.Post;
        end;
      end;
    end;
   finally
     DM_COMPANY.CO_HR_SALARY_GRADES.EnableControls;
     DM_COMPANY.CO_HR_POSITION_GRADES.EnableControls;
     evGridSelected.UnselectAll;
     evGridAvailable.UnselectAll;
   end;
end;

procedure TEDIT_CO_HR_POSITIONS.ebBtnDeleteClick(Sender: TObject);
var
 i : integer;
begin
  inherited;
   DM_COMPANY.CO_HR_SALARY_GRADES.DisableControls;
  DM_COMPANY.CO_HR_POSITION_GRADES.DisableControls;
   try
      if evGridSelected.SelectedList.Count > 0 then
      begin
        for i := evGridSelected.SelectedList.Count-1 downto 0 do
        begin
          evGridSelected.DataSource.DataSet.GotoBookmark(evGridSelected.SelectedList[i]);

          if DM_COMPANY.CO_HR_POSITION_GRADES.Locate('CO_HR_POSITION_GRADES_NBR',
                                  evGridSelected.DataSource.DataSet['CO_HR_POSITION_GRADES_NBR'],[]) then
           DM_COMPANY.CO_HR_POSITION_GRADES.Delete;
        end;
      end;
   finally
     DM_COMPANY.CO_HR_SALARY_GRADES.EnableControls;
     DM_COMPANY.CO_HR_POSITION_GRADES.EnableControls;
     evGridSelected.UnselectAll;
     evGridAvailable.UnselectAll;
   end;
end;


procedure TEDIT_CO_HR_POSITIONS.Activate;
var
  dummy: boolean;
begin
  inherited;

  if not Context.License.HR then
  Begin
    evGridSelected.Enabled := False;
    evGridAvailable.Enabled := False;
    ebBtnDelete.Enabled := False;
    evBtnUpdate.Enabled := False;
  End;


  CopyHelper.Grid := evDBGrid1; //gdy
  CopyHelper.UserFriendlyName := 'Position'; //gdy
  CopyHelper.OnCopyDetail := HandleCopy; //gdy
  CopyHelper.OnDeleteDetail := HandleDelete; //gdy
  GetDataSetsToReopen( CopyHelper.FDSToSave,  dummy );
end;

procedure TEDIT_CO_HR_POSITIONS.HandleCopy(Sender: TObject; client,
  company: integer; selectedDetails, details: TEvClientDataSet);
var
  k: integer;
  fn: string;
begin
  if details.Locate('DESCRIPTION', VarArrayOf([selecteddetails.FieldByNAme('DESCRIPTION').AsString]), []) then
    details.Edit
  else
    details.Insert;
  for k := 0 to SelectedDetails.Fields.Count - 1 do
  begin
    fn := Selecteddetails.Fields[k].FieldName;
    if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) then
      details[fn] := selectedDetails[fn];
  end;
  details['CO_NBR'] := company;
  details.Post;
end;

procedure TEDIT_CO_HR_POSITIONS.HandleDelete(Sender: TObject; client,
  company: integer; selectedDetails, details: TEvClientDataSet);
begin
  if details.Locate('DESCRIPTION', VarArrayOf([selecteddetails.FieldByNAme('DESCRIPTION').AsString]), []) then
    details.Delete;
end;

procedure TEDIT_CO_HR_POSITIONS.Button1Click(Sender: TObject);
begin
  inherited;
// 111

end;

initialization
  RegisterClass( TEDIT_CO_HR_POSITIONS);


end.

