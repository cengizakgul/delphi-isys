inherited EDIT_CO_STATE_TAX_LIABILITIES: TEDIT_CO_STATE_TAX_LIABILITIES
  inherited PageControl1: TevPageControl
    HelpContext = 50001
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited splEDIT_CO_BASE: TevSplitter
                    Left = 237
                    Width = 3
                    Visible = True
                  end
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 231
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 219
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 240
                    Width = 550
                    Visible = True
                    object fpStatesRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 538
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      OnResize = fpStatesRightResize
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'States'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Width = 337
              Height = 398
              Visible = True
              object lcbType: TevDBComboBox
                Left = 160
                Top = 256
                Width = 121
                Height = 21
                HelpContext = 43508
                ShowButton = True
                Style = csDropDownList
                MapList = True
                AllowClearKey = True
                AutoDropDown = True
                DataField = 'TAX_TYPE'
                DataSource = wwdsDetail
                DropDownCount = 8
                Enabled = False
                ItemHeight = 0
                Items.Strings = (
                  'State'#9'S'
                  'EE SDI'#9'I'
                  'ER SDI'#9'R')
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 0
                UnboundDataType = wwDefault
              end
            end
          end
          inherited Panel3: TevPanel
            OnResize = Panel3Resize
            inherited cbGroup: TevCheckBox
              Left = 545
              HelpContext = 10001
            end
            inherited cbShowCredit: TevCheckBox
              Left = 660
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 211
                IniAttributes.SectionName = 'TEDIT_CO_STATE_TAX_LIABILITIES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'State Tax Liabilities'
              inherited Splitter2: TevSplitter
                Height = 211
              end
              inherited wwDBGrid2x: TevDBGrid
                Width = 95
                Height = 211
                Selected.Strings = (
                  'State'#9'2'#9'State'#9'F'
                  'TAX_TYPE'#9'15'#9'Type'#9'F'
                  'AMOUNT'#9'12'#9'Amount'#9'F'
                  'DUE_DATE'#9'10'#9'Due date'#9'F'
                  'CHECK_DATE'#9'10'#9'Check date'#9'F'
                  'ADJUSTMENT_TYPE'#9'10'#9'Adj. type'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_STATE_TAX_LIABILITIES\wwDBGrid2x'
              end
              inherited wwDBGrid1x: TevDBGrid
                Height = 211
                Selected.Strings = (
                  'RUN_NUMBER'#9'5'#9'Run #'#9'F'
                  'CHECK_DATE'#9'10'#9'Check Date'#9'F'
                  'SCHEDULED_PROCESS_DATE'#9'10'#9'Scheduled Process Date'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_STATE_TAX_LIABILITIES\wwDBGrid1x'
              end
              inherited lcbAdjType: TevDBComboBox
                MapList = True
                Items.Strings = (
                  'Line 9'#9'9'
                  'Interest'#9'I'
                  'Penalty'#9'P'
                  'Prior Quarter'#9'C'
                  'New Client Setup'#9'N'
                  'Quarter End'#9'Q'
                  'Credit overpaid'#9'R'
                  'Other'#9'O')
              end
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited sbTaxLiabilitiesBase: TScrollBox
        inherited fpLiabilityNotes: TisUIFashionPanel
          Left = 328
          Top = 343
          Width = 465
          TabOrder = 4
          inherited dmemNotes: TEvDBMemo
            Width = 430
          end
        end
        inherited fpBaseLiabilitiesDetails: TisUIFashionPanel
          Width = 312
          Height = 327
          TabOrder = 0
          inherited lablCheck_Date: TevLabel
            Left = 12
            Top = 74
          end
          inherited lablProcess_Date: TevLabel
            Left = 12
            Top = 269
          end
          inherited lablImpound: TevLabel
            Left = 640
            Top = 184
            Visible = False
          end
          inherited lablTax_Deposits: TevLabel
            Left = 12
            Top = 152
          end
          inherited lablPayroll_Run: TevLabel
            Left = 640
            Top = 40
            Visible = False
          end
          object lablDue_Date: TevLabel [5]
            Left = 155
            Top = 74
            Width = 55
            Height = 16
            Caption = '~Due Date'
          end
          object lablState: TevLabel [6]
            Left = 12
            Top = 35
            Width = 34
            Height = 16
            Caption = '~State'
            FocusControl = wwlcState
          end
          object lablAmount: TevLabel [7]
            Left = 155
            Top = 113
            Width = 45
            Height = 16
            Caption = '~Amount'
            FocusControl = wwdeAmount
          end
          object lablTax_Type: TevLabel [8]
            Left = 155
            Top = 35
            Width = 54
            Height = 16
            Caption = '~Tax Type'
          end
          object lablStatus: TevLabel [9]
            Left = 155
            Top = 152
            Width = 39
            Height = 16
            Caption = '~Status'
            FocusControl = wwcbStatus
          end
          object lablAdjustment_Type: TevLabel [10]
            Left = 12
            Top = 113
            Width = 79
            Height = 13
            Caption = 'Adjustment Type'
            FocusControl = wwcbAdjustment_Type
          end
          inherited wwdbtpCheckDate: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 135
            TabOrder = 2
            OnExit = wwdbtpCheckDateExit
          end
          inherited dedtImpound: TevDBEdit
            Left = 640
            Top = 200
            Width = 120
            TabOrder = 14
            Visible = False
          end
          inherited dedtPayroll_Run: TevDBEdit
            Left = 640
            Top = 56
            Width = 120
            TabOrder = 15
            Visible = False
          end
          inherited wwDBDateTimePicker2: TevDBDateTimePicker
            Left = 12
            Top = 284
            Width = 135
            TabOrder = 12
          end
          inherited drgpThirdParty: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 135
            Height = 37
            TabOrder = 8
          end
          inherited dedtTax_Deposits: TevEdit
            Left = 12
            Top = 167
            Width = 113
            TabOrder = 6
          end
          inherited buttonAssociateDeposit: TevBitBtn
            Left = 126
            Top = 167
            TabOrder = 16
            TabStop = False
          end
          inherited evDBRadioGroup1: TevDBRadioGroup
            Left = 155
            Top = 191
            Width = 135
            Height = 37
            TabOrder = 9
          end
          inherited evDBEditAchKey: TevDBEdit
            Left = 155
            Top = 284
            Width = 135
            TabOrder = 13
          end
          inherited evPanelssd1: TevPanel
            Left = 155
            Top = 269
            Height = 13
            ParentColor = True
            TabOrder = 17
            inherited evLabelssd1: TevLabel
              Align = alClient
            end
          end
          inherited evDBDateTimePicker3: TevDBDateTimePicker
            Left = 12
            Top = 245
            Width = 135
          end
          inherited evDBDateTimePicker4: TevDBDateTimePicker
            Left = 155
            Top = 245
            Width = 135
          end
          inherited evPanel2: TevPanel
            Left = 12
            Top = 230
            Height = 13
            ParentColor = True
            TabOrder = 18
            inherited evLabel6: TevLabel
              Align = alClient
            end
          end
          inherited evPanel3: TevPanel
            Left = 155
            Top = 230
            Height = 13
            ParentColor = True
            TabOrder = 19
            inherited evLabel4: TevLabel
              Align = alClient
            end
          end
          object wwcbAdjustment_Type: TevDBComboBox
            Left = 12
            Top = 128
            Width = 135
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'ADJUSTMENT_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Line 9'#9'9'
              'Interest'#9'I'
              'Penalty'#9'P'
              'Prior Quarter'#9'C'
              'New Client Setup'#9'N'
              'Quarter End'#9'Q'
              'Credit overpaid'#9'R'
              'Other'#9'O')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
          object wwdpDue_Date: TevDBDateTimePicker
            Left = 155
            Top = 89
            Width = 135
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DUE_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 3
          end
          object wwlcState: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 135
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'CO_STATES_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwdeAmount: TevDBEdit
            Left = 155
            Top = 128
            Width = 135
            Height = 21
            DataField = 'AMOUNT'
            DataSource = wwdsDetail
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbType: TevDBComboBox
            Left = 155
            Top = 50
            Width = 135
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TAX_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'State'#9'S'
              'EE SDI'#9'I'
              'ER SDI'#9'R')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object wwcbStatus: TevDBComboBox
            Left = 155
            Top = 167
            Width = 135
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Pending'#9'X'
              'Paid'#9'P'
              'Paid-Send Notice'#9'S'
              'NSF'#9'N'
              'Voided'#9'V'
              'Send-Notice'#9'C'
              'Notice-Sent'#9'T'
              'Deleted'#9'D'
              'Manually impounded'#9'M'
              'Paid w/o funds'#9'W'
              'Impounded'#9'I')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
          end
        end
        inherited fpTaxLiabilities: TisUIFashionPanel
          Left = 328
          TabOrder = 2
          inherited wwdgTaxLiabilities: TevDBGrid
            Selected.Strings = (
              'State'#9'6'#9'State'#9'F'
              'TAX_TYPE'#9'15'#9'Type'#9'F'
              'AMOUNT'#9'12'#9'Amount'#9'F'
              'THIRD_PARTY'#9'1'#9'3rd prty'#9'F'
              'IMPOUNDED'#9'1'#9'Collected'#9'F'
              'CHECK_DATE'#9'10'#9'Check date'#9'F'
              'ACH_KEY'#9'11'#9'ACH key'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.SectionName = 'TEDIT_CO_STATE_TAX_LIABILITIES\wwdgTaxLiabilities'
          end
        end
        inherited fpTools: TisUIFashionPanel
          Top = 343
          Width = 312
          Height = 112
          TabOrder = 3
          inherited BitBtn3: TevBitBtn
            Width = 277
          end
          inherited bbtnAttach: TevBitBtn
            Left = 12
            Top = 67
            Width = 277
          end
        end
        inherited Panel4: TevPanel
          Left = 0
          Top = 0
          Width = 322
          Height = 337
          TabOrder = 1
          inherited fpTaxDeposit: TisUIFashionPanel
            Width = 312
            Height = 327
            inherited Label91: TevLabel
              Left = 155
            end
            inherited wwcbAdjustment_Type1: TevDBComboBox
              Width = 135
            end
            inherited wwcbStatus1: TevDBComboBox
              Left = 155
              Width = 135
            end
            inherited DBRadioGroup11: TevDBRadioGroup
              Width = 135
            end
            inherited Button1: TevButton
              Left = 155
              Width = 135
            end
            inherited wwDBLookupCombo11: TevDBLookupCombo
              Width = 135
            end
            inherited cbClearTaxDepRecord: TevCheckBox
              Left = 155
              Width = 135
            end
            inherited evDBRadioGroup2: TevDBRadioGroup
              Left = 155
              Width = 135
            end
            inherited evDBEdit1: TevDBEdit
              Width = 135
            end
            inherited evDBDateTimePicker1: TevDBDateTimePicker
              Width = 135
            end
            inherited evDBDateTimePicker2: TevDBDateTimePicker
              Left = 155
              Width = 135
            end
            inherited evPanel5: TevPanel
              Left = 155
            end
          end
        end
      end
    end
    inherited tsCO_TAX_DEPOSITS: TTabSheet
      inherited sbTaxDeposits: TScrollBox
        inherited fpTaxDeposits: TisUIFashionPanel
          inherited wwDBGrid1: TevDBGrid
            IniAttributes.SectionName = 'TEDIT_CO_STATE_TAX_LIABILITIES\wwDBGrid1'
          end
        end
        inherited fpTaxDepositDetail: TisUIFashionPanel
          inherited wwcbStatus2: TevDBComboBox
            MapList = True
            Items.Strings = (
              'Pending'#9'X'
              'Paid'#9'P'
              'Paid-Send Notice'#9'S'
              'NSF'#9'N'
              'Voided'#9'V'
              'Send-Notice'#9'C'
              'Notice-Sent'#9'T'
              'Deleted'#9'D'
              'Manually impounded'#9'M'
              'Paid w/o funds'#9'W'
              'Impounded'#9'I')
          end
          inherited wwcbAdjustment_Type2: TevDBComboBox
            MapList = True
            Items.Strings = (
              'Line 9'#9'9'
              'Interest'#9'I'
              'Penalty'#9'P'
              'Prior Quarter'#9'C'
              'New Client Setup'#9'N'
              'Quarter End'#9'Q'
              'Credit overpaid'#9'R'
              'Other'#9'O')
          end
          inherited wwcbType2: TevDBComboBox
            MapList = True
            Items.Strings = (
              'State'#9'S'
              'EE SDI'#9'I'
              'ER SDI'#9'R')
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_STATE_TAX_LIABILITIES.CO_STATE_TAX_LIABILITIES
  end
end
