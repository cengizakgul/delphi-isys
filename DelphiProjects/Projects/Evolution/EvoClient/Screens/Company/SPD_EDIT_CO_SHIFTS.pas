// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_SHIFTS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  wwdblook, StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons, wwdbedit, 
  SDataStructure,  sCompanyCopyHelper, sCopyHelper, Variants, EvUIComponents, EvClientDataSet,
  ISBasicClasses, isUIwwDBLookupCombo, isUIwwDBEdit, SDDClasses,
  SDataDictclient, ImgList, SDataDicttemp, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_SHIFTS = class(TEDIT_CO_BASE)
    tshtShifts: TTabSheet;
    wwdgShifts: TevDBGrid;
    fpShiftsRight: TisUIFashionPanel;
    fpSummary: TisUIFashionPanel;
    fpDetail: TisUIFashionPanel;
    lablName: TevLabel;
    lablE_D_Group: TevLabel;
    lablDefault_Amount: TevLabel;
    lablDefault_Percentage: TevLabel;
    dedtName: TevDBEdit;
    wwlcE_D_Group: TevDBLookupCombo;
    wwdeDefault_Amount: TevDBEdit;
    wwdeDefault_Percentage: TevDBEdit;
    rgAutocreate: TevDBRadioGroup;
    wwDBGrid1: TevDBGrid;
    CopyHelper: TCompanyCopyHelper;
    procedure fpShiftsRightResize(Sender: TObject);
  private
    procedure HandleCopy( Sender: TObject; client, company: integer; selectedDetails, details: TEvClientDataSet );
    procedure HandleDelete( Sender: TObject; client, company: integer; selectedDetails, details: TEvClientDataSet );
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses
  evutils, scopier;

{$R *.DFM}

function TEDIT_CO_SHIFTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_SHIFTS;
end;

function TEDIT_CO_SHIFTS.GetInsertControl: TWinControl;
begin
  Result := dedtName;
end;

procedure TEDIT_CO_SHIFTS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  inherited;
end;

//begin gdy
procedure TEDIT_CO_SHIFTS.Activate;
var
  dummy: boolean;
begin
  inherited;
  CopyHelper.Grid := wwDBGrid1; //gdy
  CopyHelper.UserFriendlyName := 'Shift'; //gdy
  CopyHelper.OnCopyDetail := HandleCopy; //gdy
  CopyHelper.OnDeleteDetail := HandleDelete; //gdy
  GetDataSetsToReopen( CopyHelper.FDSToSave,  dummy );
//  CopyHelper.SetDSToSave( [DM_CLIENT.CL_E_D_GROUPS ] ); //gdy
end;


procedure TEDIT_CO_SHIFTS.HandleCopy(Sender: TObject; client,
  company: integer; selectedDetails, details: TEvClientDataSet);
var
  v: Variant;
  k: integer;
  fn: string;
begin
  DM_CLIENT.CL_E_D_GROUPS.EnsureDSCondition('');
  v := DM_CLIENT.CL_E_D_GROUPS.Lookup( 'NAME', selectedDetails['EDGroupName'], 'CL_E_D_GROUPS_NBR');

  if details.Locate('NAME', VarArrayOf([selecteddetails.FieldByNAme('NAME').AsString]), []) then
    details.Edit
  else
    details.Insert;
  for k := 0 to SelectedDetails.Fields.Count - 1 do
  begin
    fn := Selecteddetails.Fields[k].FieldName;
    if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) and ( fn <> 'CL_E_D_GROUPS_NBR' ) then
      details[fn] := selectedDetails[fn];
  end;
  details['CO_NBR'] := company;
  if not VarIsNull( v ) then
    details['CL_E_D_GROUPS_NBR'] := v;
  details.Post;
end;

procedure TEDIT_CO_SHIFTS.HandleDelete(Sender: TObject; client,
  company: integer; selectedDetails, details: TEvClientDataSet);
begin
  if details.Locate('NAME', VarArrayOf([selecteddetails.FieldByNAme('NAME').AsString]), []) then
    details.Delete;
end;
//end gdy

constructor TEDIT_CO_SHIFTS.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENTS
  pnlSubbrowse.Parent := fpShiftsRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_SHIFTS.fpShiftsRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpShiftsRight.Width - 40;
  pnlSubbrowse.Height := fpShiftsRight.Height - 65;
end;

initialization
  RegisterClass(TEDIT_CO_SHIFTS);

end.
