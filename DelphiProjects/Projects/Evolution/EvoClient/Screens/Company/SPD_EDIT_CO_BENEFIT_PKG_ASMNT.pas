// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BENEFIT_PKG_ASMNT;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, StdCtrls, Mask, wwdbedit,
  Wwdbspin, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, Controls, Classes, Forms, sCompanyCopyHelper,
  sCopyHelper, Variants, SDDClasses, ISBasicClasses, SDataDictclient,
  SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, wwdblook,
  isUIwwDBLookupCombo;

type
  TEDIT_CO_BENEFIT_PKG_ASMNT = class(TEDIT_CO_BASE)
    thDetail: TTabSheet;
    evDBText1: TevDBText;
    CopyHelper: TCompanyCopyHelper;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    sbAccrualRates: TScrollBox;
    fpPkgAssignmentSummary: TisUIFashionPanel;
    DBGridDetail: TevDBGrid;
    fpPkgASMNTDetails: TisUIFashionPanel;
    wwDBGridBenefitPkgAsmnt: TevDBGrid;
    evLabel1: TevLabel;
    evcbDivision: TevDBLookupCombo;
    evLabel2: TevLabel;
    evcbBranch: TevDBLookupCombo;
    evLabel3: TevLabel;
    evcbDeparment: TevDBLookupCombo;
    evLabel4: TevLabel;
    evcbTeam: TevDBLookupCombo;
    evLabel5: TevLabel;
    evcbGroup: TevDBLookupCombo;
    wwdsSubMaster: TevDataSource;
    evSplitter2: TevSplitter;
    fpPackageDetails: TisUIFashionPanel;
    wwdgPackageAssignment: TevDBGrid;
    fpPackage: TisUIFashionPanel;
    evLabel6: TevLabel;
    evPackage: TevDBLookupCombo;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure evcbBranchChange(Sender: TObject);
    procedure evcbDivisionChange(Sender: TObject);
    procedure evcbDeparmentChange(Sender: TObject);
    procedure evcbDivisionCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evcbBranchCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evcbDeparmentCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure PageControl1Change(Sender: TObject);
  private
   FEnableHr: boolean;
     
   procedure setBranchFilter;
   procedure setDepartmentFilter;
   procedure setTeamFilter;
   procedure SetSecurityRO;
  protected
    function GetIfReadOnly: Boolean; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure AfterDataSetsReopen; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses
  spackageentry, sysutils, evutils, sCopier, dialogs, SFrameEntry, EvConsts;
{$R *.DFM}


function TEDIT_CO_BENEFIT_PKG_ASMNT.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if (not Result) then
     result:= FEnableHr;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.AfterDataSetsReopen;
begin
  inherited;
  FEnableHr := GetEnableHrBenefit(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  if GetIfReadOnly then
     SetReadOnly(True)
  else
    ClearReadOnly;
end;

function TEDIT_CO_BENEFIT_PKG_ASMNT.GetDataSetConditions(
  sName: string): string;
begin
 if (sName = 'CO_BENEFIT_PKG_ASMNT') or (sName = 'CO_BRANCH') or
    (sName = 'CO_DEPARTMENT') or        (sName = 'CO_TEAM') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.Activate;
begin
   inherited;
   wwDBGridBenefitPkgAsmnt.DataSource :=  wwdsSubMaster;
   wwdgPackageAssignment.DataSource := wwdsDetail;
   setBranchFilter;
   setDepartmentFilter;
   setTeamFilter;
   SetSecurityRO;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.DeActivate;
begin
  inherited;
  DM_COMPANY.CO_BRANCH.Filter :=  '';
  DM_COMPANY.CO_BRANCH.Filtered := False;
  DM_COMPANY.CO_DEPARTMENT.Filter :=  '';
  DM_COMPANY.CO_DEPARTMENT.Filtered := False;
  DM_COMPANY.CO_TEAM.Filter :=  '';
  DM_COMPANY.CO_TEAM.Filtered := False;
  DM_COMPANY.CO_GROUP.Filter :=  '';
  DM_COMPANY.CO_GROUP.Filtered := False;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_BENEFIT_PACKAGE, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);
  AddDS(DM_COMPANY.CO_GROUP , aDS);
  inherited;
end;

function TEDIT_CO_BENEFIT_PKG_ASMNT.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BENEFIT_PKG_ASMNT;
end;

function TEDIT_CO_BENEFIT_PKG_ASMNT.GetInsertControl: TWinControl;
begin
  Result := evPackage;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_BENEFIT_PACKAGE, SupportDataSets);
  AddDS(DM_COMPANY.CO_DIVISION, SupportDataSets);
  AddDS(DM_COMPANY.CO_BRANCH, SupportDataSets);
  AddDS(DM_COMPANY.CO_DEPARTMENT, SupportDataSets);
  AddDS(DM_COMPANY.CO_TEAM, SupportDataSets);
  AddDS(DM_COMPANY.CO_GROUP, SupportDataSets);
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind in [NavOK, NavCommit] then
  begin
    if wwdsDetail.DataSet.State in [dsEdit, dsInsert] then
       wwdsDetail.DataSet.FieldByName('CO_BENEFIT_PACKAGE_NBR').AsInteger :=
           DM_COMPANY.CO_BENEFIT_PACKAGE.FieldByName('CO_BENEFIT_PACKAGE_NBR').AsInteger
  end;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.SetSecurityRO;
begin
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active then
  begin
   evcbDivision.SecurityRO := GetIfReadOnly;
   evcbGroup.SecurityRO := GetIfReadOnly;
   evcbBranch.SecurityRO := GetIfReadOnly or wwdsDetail.DataSet.fieldByName('CO_DIVISION_NBR').isnull;
   evcbDeparment.SecurityRO := GetIfReadOnly or wwdsDetail.DataSet.fieldByName('CO_BRANCH_NBR').isnull;
   evcbTeam.SecurityRO := GetIfReadOnly or wwdsDetail.DataSet.fieldByName('CO_DEPARTMENT_NBR').isnull;

   if not wwdsDetail.DataSet.fieldByName('CO_GROUP_NBR').isnull then
   begin
    evcbDivision.SecurityRO := true;
    evcbBranch.SecurityRO := true;
    evcbDeparment.SecurityRO := true;
    evcbTeam.SecurityRO := true;
   end
   else
   if not wwdsDetail.DataSet.fieldByName('CO_DIVISION_NBR').isnull then
    evcbGroup.SecurityRO := true;
  end;
end;


procedure TEDIT_CO_BENEFIT_PKG_ASMNT.setBranchFilter;
begin
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and
     (not wwdsDetail.DataSet.fieldByName('CO_DIVISION_NBR').isnull) then
  begin
     DM_COMPANY.CO_BRANCH.Filter := 'CO_DIVISION_NBR = ' + wwdsDetail.DataSet.fieldByName('CO_DIVISION_NBR').AsString;
     DM_COMPANY.CO_BRANCH.Filtered :=true;
  end;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.setDepartmentFilter;
begin
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and
    (not wwdsDetail.DataSet.fieldByName('CO_BRANCH_NBR').isnull) then
  begin
     DM_COMPANY.CO_DEPARTMENT.Filter := 'CO_BRANCH_NBR = ' + wwdsDetail.DataSet.fieldByName('CO_BRANCH_NBR').AsString;
     DM_COMPANY.CO_DEPARTMENT.Filtered :=true;
  end;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.setTeamFilter;
begin
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and
    (not wwdsDetail.DataSet.fieldByName('CO_DEPARTMENT_NBR').isnull) then
  begin
     DM_COMPANY.CO_TEAM.Filter := 'CO_DEPARTMENT_NBR = ' + wwdsDetail.DataSet.fieldByName('CO_DEPARTMENT_NBR').AsString;
     DM_COMPANY.CO_TEAM.Filtered :=true;
  end;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.evcbDivisionCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  setBRanchFilter;
  if (wwdsDetail.DataSet.State in [ dsEdit, dsInsert ]) and
     ( wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').OldValue <>
       wwdsDetail.DataSet.FieldByName('CO_DIVISION_NBR').NewValue ) then
  begin
     DM_COMPANY.CO_BENEFIT_PKG_ASMNT['CO_BRANCH_NBR'] := null;
     DM_COMPANY.CO_BENEFIT_PKG_ASMNT['CO_DEPARTMENT_NBR'] := null;
     DM_COMPANY.CO_BENEFIT_PKG_ASMNT['CO_TEAM_NBR'] := null;
  end;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.evcbDivisionChange(Sender: TObject);
begin
  inherited;
  setBRanchFilter;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.evcbBranchCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  setDepartmentFilter;
  if (wwdsDetail.DataSet.State in [ dsEdit, dsInsert ]) and
     ( wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').OldValue <>
       wwdsDetail.DataSet.FieldByName('CO_BRANCH_NBR').NewValue ) then
  begin
     DM_COMPANY.CO_BENEFIT_PKG_ASMNT['CO_DEPARTMENT_NBR'] := null;
     DM_COMPANY.CO_BENEFIT_PKG_ASMNT['CO_TEAM_NBR'] := null;
  end;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.evcbBranchChange(Sender: TObject);
begin
  inherited;
  setDepartmentFilter;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.evcbDeparmentCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  setTeamFilter;
  setDepartmentFilter;
  if (wwdsDetail.DataSet.State in [ dsEdit, dsInsert ]) and
     ( wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').OldValue <>
       wwdsDetail.DataSet.FieldByName('CO_DEPARTMENT_NBR').NewValue ) then
     DM_COMPANY.CO_BENEFIT_PKG_ASMNT['CO_TEAM_NBR'] := null;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.evcbDeparmentChange(Sender: TObject);
begin
  inherited;
  setTeamFilter;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then exit;
  SetSecurityRO;
end;

procedure TEDIT_CO_BENEFIT_PKG_ASMNT.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = thDetail then
  begin
    DM_COMPANY.CO_GROUP.Filter := 'GROUP_TYPE = ''' + ESS_GROUPTYPE_BENEFIT + '''';
    DM_COMPANY.CO_GROUP.Filtered :=true;
  end
end;

initialization
  RegisterClass(TEDIT_CO_BENEFIT_PKG_ASMNT);

end.
