// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BENEFIT_SETUP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBLookupCombo,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, isUIwwDBComboBox, isUIwwDBEdit, Wwdbspin,
  wwdbdatetimepicker, isUIwwDBDateTimePicker, isUIDBMemo,
  SPackageEntry, EvExceptions;

type
  TEDIT_CO_BENEFIT_SETUP = class(TEDIT_CO_BASE)
    tsEnrollReminder: TTabSheet;
    wwDBGrid1: TevDBGrid;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    fpInstructions: TisUIFashionPanel;
    sbSetup: TScrollBox;
    evInstructions: TEvDBMemo;
    tsReminder: TTabSheet;
    tsEnrollment: TTabSheet;
    ScrollBox1: TScrollBox;
    ScrollBox2: TScrollBox;
    fpReminderEmail: TisUIFashionPanel;
    evLabel4: TevLabel;
    memoText1: TEvDBMemo;
    fpHRReminder: TisUIFashionPanel;
    evLabel9: TevLabel;
    memoText2: TEvDBMemo;
    fpEEReminder: TisUIFashionPanel;
    evLabel15: TevLabel;
    memoText3: TEvDBMemo;
    evLabel3: TevLabel;
    edSubject1: TevDBEdit;
    evLabel5: TevLabel;
    edSubject2: TevDBEdit;
    evLabel10: TevLabel;
    edSubject3: TevDBEdit;
    fpOEAnnouncement: TisUIFashionPanel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    memT1: TEvDBMemo;
    edSub1: TevDBEdit;
    fpElected: TisUIFashionPanel;
    evLabel8: TevLabel;
    evLabel11: TevLabel;
    memT2: TEvDBMemo;
    edSub2: TevDBEdit;
    fpEventBased: TisUIFashionPanel;
    evLabel13: TevLabel;
    evLabel14: TevLabel;
    memT3: TEvDBMemo;
    edSub3: TevDBEdit;
    fpNoElection: TisUIFashionPanel;
    evLabel16: TevLabel;
    evLabel17: TevLabel;
    memT4: TEvDBMemo;
    edSub4: TevDBEdit;
    fpCertification: TisUIFashionPanel;
    fpIntroduction: TisUIFashionPanel;
    evNote: TEvDBMemo;
    EvDBMemo2: TEvDBMemo;
  private
    FEnableHr: boolean;
  protected
    function GetIfReadOnly: Boolean; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure AfterDataSetsReopen; override ;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;    
  end;

implementation

uses SFrameEntry;

{$R *.DFM}

function TEDIT_CO_BENEFIT_SETUP.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if (not Result) then
     result:= FEnableHr;
end;

procedure TEDIT_CO_BENEFIT_SETUP.Activate;
begin
  inherited;
  fpEDIT_OPEN_BASE_LEFT.Align := alClient;
end;

function TEDIT_CO_BENEFIT_SETUP.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BENEFIT_SETUP;
end;

function TEDIT_CO_BENEFIT_SETUP.GetInsertControl: TWinControl;
begin
  Result := evInstructions;
end;

procedure TEDIT_CO_BENEFIT_SETUP.AfterDataSetsReopen;
begin
  if wwdsDetail.DataSet.Active and
     (wwdsDetail.DataSet.State = dsBrowse) then
  begin
    if (wwdsDetail.DataSet.RecordCount > 0) then
      (Owner as TFramePackageTmpl).SetButton(NavInsert, false);
  end;
  
  FEnableHr := GetEnableHrBenefit(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  if GetIfReadOnly then
     SetReadOnly(True)
  else
    ClearReadOnly;
end;

initialization
  RegisterClass(TEDIT_CO_BENEFIT_SETUP);

end.
