inherited EDIT_CO_TAX_DEPOSIT: TEDIT_CO_TAX_DEPOSIT
  Width = 435
  Height = 266
  inherited Panel1: TevPanel
    Width = 435
    inherited pnlFavoriteReport: TevPanel
      Left = 283
    end
    inherited pnlTopLeft: TevPanel
      Width = 283
    end
  end
  inherited PageControl1: TevPageControl
    Width = 435
    Height = 212
    HelpContext = 30502
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 427
        Height = 183
        inherited pnlBorder: TevPanel
          Width = 423
          Height = 179
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 423
          Height = 179
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 784
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 772
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 448
              Top = 96
              Width = 312
              Height = 456
            end
          end
          inherited Panel3: TevPanel
            Width = 375
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 375
            Height = 72
            inherited Splitter1: TevSplitter
              Height = 68
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 49
              Height = 68
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 1
                Height = 315
                IniAttributes.SectionName = 'TEDIT_CO_TAX_DEPOSIT\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = -165
              Height = 68
            end
          end
        end
      end
    end
    object tshtTax_Deposit: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object lablDelivery: TevLabel
        Left = 776
        Top = 20
        Width = 38
        Height = 13
        Caption = 'Delivery'
        FocusControl = wwlcDelivery
        Visible = False
      end
      object lablPayroll_Run: TevLabel
        Left = 776
        Top = 52
        Width = 54
        Height = 13
        Caption = 'Payroll Run'
        FocusControl = dedtPayroll_Run
        Visible = False
      end
      object dedtPayroll_Run: TevDBEdit
        Left = 776
        Top = 68
        Width = 64
        Height = 21
        DataField = 'PR_NBR'
        DataSource = wwdsDetail
        ReadOnly = True
        TabOrder = 1
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object sbTaxDepositDetail: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 183
        Align = alClient
        TabOrder = 2
        object fpCOTaxDepositSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 609
          Height = 343
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgTax_Deposit: TevDBGrid
            Left = 12
            Top = 35
            Width = 551
            Height = 289
            TabStop = False
            DisableThemesInTitle = False
            Selected.Strings = (
              'TAX_PAYMENT_REFERENCE_NUMBER'#9'23'#9'Tax Payment Reference #'#9'F'
              'STATUS'#9'1'#9'Status'#9'F'
              'STATUS_DATE'#9'18'#9'Status Date'#9'F'
              'PayrollDescription'#9'37'#9'Payroll Description'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_TAX_DEPOSIT\wwdgTax_Deposit'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCOTaxDepositDetails: TisUIFashionPanel
          Left = 8
          Top = 360
          Width = 609
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Tax Deposit Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablPayment_Reference_Number: TevLabel
            Left = 12
            Top = 35
            Width = 134
            Height = 13
            Caption = 'Payment Reference Number'
            FocusControl = dedtPayment_Reference_Number
          end
          object lablStatus: TevLabel
            Left = 12
            Top = 74
            Width = 40
            Height = 13
            Caption = '~Status'
            FocusControl = wwcbStatus
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDate_Paid: TevLabel
            Left = 97
            Top = 74
            Width = 56
            Height = 13
            Caption = 'Status Date'
          end
          object lablAgency: TevLabel
            Left = 230
            Top = 35
            Width = 125
            Height = 13
            Caption = 'Global Agency Field Office'
          end
          object evLabel2: TevLabel
            Left = 348
            Top = 74
            Width = 71
            Height = 13
            Caption = 'Trace Number '
          end
          object lblTaxDepositType: TevLabel
            Left = 463
            Top = 74
            Width = 96
            Height = 13
            Caption = '~Tax Deposit Type'
            FocusControl = wwcbStatus
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 230
            Top = 74
            Width = 46
            Height = 13
            Caption = '~Amount'
            FocusControl = edBarAmount
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtPayment_Reference_Number: TevDBEdit
            Left = 12
            Top = 50
            Width = 210
            Height = 21
            DataField = 'TAX_PAYMENT_REFERENCE_NUMBER'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwcbStatus: TevDBComboBox
            Left = 12
            Top = 89
            Width = 77
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object wwdpDate_Paid: TevDBDateTimePicker
            Left = 97
            Top = 89
            Width = 125
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'STATUS_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            Enabled = False
            ReadOnly = True
            ShowButton = False
            TabOrder = 3
          end
          object edBarAmount: TevDBEdit
            Left = 230
            Top = 89
            Width = 90
            Height = 21
            DataField = 'BarAmount'
            DataSource = wwdsDetail
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwlcAgency: TevDBLookupCombo
            Left = 230
            Top = 50
            Width = 355
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ADDITIONAL_NAME'#9'40'#9'FIELD OFFICE NAME'
              'AGENCY_NAME'#9'40'#9'AGENCY_NAME'#9'F')
            DataField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SY_GL_AGENCY_FIELD_OFFICE.SY_GL_AGENCY_FIELD_OFFICE
            LookupField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object edtTraceNumber: TevDBEdit
            Left = 349
            Top = 89
            Width = 106
            Height = 21
            DataField = 'TRACE_NUMBER'
            DataSource = wwdsDetail
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object cbTaxDepositType: TevDBComboBox
            Left = 463
            Top = 89
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'DEPOSIT_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'EFT Credit'
              'EFT Debit'
              'Check'
              'Notice'
              'Manual')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
            OnChange = cbTaxDepositTypeChange
          end
          object pnlAlternateAmount: TevPanel
            Left = 230
            Top = 74
            Width = 113
            Height = 37
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 4
            Visible = False
            object evLabel1: TevLabel
              Left = 0
              Top = 0
              Width = 105
              Height = 13
              Caption = 'Amount                       '
            end
            object edAmount: TevEdit
              Left = 0
              Top = 15
              Width = 111
              Height = 21
              Enabled = False
              ReadOnly = True
              TabOrder = 0
              Text = '-'
            end
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    OnDataChange = wwdsDetailDataChange
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 453
    Top = 18
  end
  object cdAmounts: TevClientDataSet
    IndexFieldNames = 'CO_TAX_DEPOSITS_NBR'
    Left = 564
    Top = 113
    object cdAmountsCO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
    end
    object cdAmountsAMOUNT: TCurrencyField
      FieldName = 'AMOUNT'
    end
  end
  object cdBarLink: TevClientDataSet
    Left = 576
  end
end
