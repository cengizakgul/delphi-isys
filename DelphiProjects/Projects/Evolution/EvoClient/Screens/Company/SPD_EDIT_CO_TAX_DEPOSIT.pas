// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_TAX_DEPOSIT;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, StdCtrls, ExtCtrls,
  DBCtrls, wwdbdatetimepicker, wwdblook, Wwdotdot, Wwdbcomb, Mask,
  wwdbedit, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Controls, Classes,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictsystem, SDataDictclient, SDataDicttemp,
  SPackageEntry,Dialogs, EvExceptions, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, isUIwwDBLookupCombo, isUIwwDBDateTimePicker, isUIwwDBComboBox,
  isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_TAX_DEPOSIT = class(TEDIT_CO_BASE)
    tshtTax_Deposit: TTabSheet;
    wwdgTax_Deposit: TevDBGrid;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    cdAmounts: TevClientDataSet;
    cdAmountsCO_TAX_DEPOSITS_NBR: TIntegerField;
    cdAmountsAMOUNT: TCurrencyField;
    cdBarLink: TevClientDataSet;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpCOTaxDepositSummary: TisUIFashionPanel;
    fpCOTaxDepositDetails: TisUIFashionPanel;
    lablPayment_Reference_Number: TevLabel;
    dedtPayment_Reference_Number: TevDBEdit;
    lablStatus: TevLabel;
    wwcbStatus: TevDBComboBox;
    wwdpDate_Paid: TevDBDateTimePicker;
    lablDate_Paid: TevLabel;
    edBarAmount: TevDBEdit;
    wwlcAgency: TevDBLookupCombo;
    lablAgency: TevLabel;
    evLabel2: TevLabel;
    edtTraceNumber: TevDBEdit;
    lablDelivery: TevLabel;
   // wwlcDelivery: TevDBLookupCombo;
    lablPayroll_Run: TevLabel;
    dedtPayroll_Run: TevDBEdit;
    sbTaxDepositDetail: TScrollBox;
    cbTaxDepositType: TevDBComboBox;
    lblTaxDepositType: TevLabel;
    evLabel3: TevLabel;
    pnlAlternateAmount: TevPanel;
    evLabel1: TevLabel;
    edAmount: TevEdit;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure AddToBARManualDeposit;
    procedure CheckManualType;
    procedure CreateBarLink;
    procedure cbTaxDepositTypeChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick( Kind: Integer ); override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

uses EvUtils, SysUtils, EvConsts;

procedure TEDIT_CO_TAX_DEPOSIT.Activate;
begin
  inherited;

end;

procedure TEDIT_CO_TAX_DEPOSIT.ButtonClicked(Kind: Integer; var Handled: Boolean);
  Function GetMsgInsert:string;
  begin
    result := 'Would you like to post this transaction in the Bank Account Register?';
  end;
  Function GetMsgEdit:string;
  begin
    result := 'Would you like to update this transaction in the Bank Account Register?';
  end;
var
 msg : String;
begin

  inherited;

  if (Kind = NavOK) then
  begin
    if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
       wwdsDetail.DataSet.UpdateRecord;
    if (wwdsDetail.DataSet.FieldByName('DEPOSIT_TYPE').AsString = TAX_DEPOSIT_TYPE_MANUAL) then
    begin
      if (Trim(edBarAmount.Text) = '') then
            raise EUpdateError.CreateHelp('Please enter Amount.', IDH_ConsistencyViolation);

      msg :=GetMsgInsert;
      if (wwdsDetail.DataSet.State in [dsEdit]) and cdBarLink.Active then
        if (cdBarLink.Locate('CO_TAX_DEPOSITS_NBR',wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').Value,[])) then
            msg :=GetMsgEdit;

      if EvMessage(msg, mtConfirmation, [mbYes, mbNo]) = mrYes then
            AddToBARManualDeposit;

      if (DM_COMPANY.CO_TAX_DEPOSITS.State in [dsInsert, dsEdit]) then
          DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('FILLER').AsString :=
            PutIntoFiller(DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('FILLER').AsString,  CurrToStr(DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('BarAmount').AsCurrency), 1, 15);
    end;
  end;
end;

procedure TEDIT_CO_TAX_DEPOSIT.AfterClick(Kind: Integer);
begin
  inherited;
  if (Kind = NavCommit)  then
   CreateBarLink;
end;

procedure TEDIT_CO_TAX_DEPOSIT.CreateBarLink;
var
  s : String;
begin
  s := 'select a.CO_BANK_ACCOUNT_REGISTER_NBR,b.CO_TAX_DEPOSITS_NBR, a.AMOUNT ' +
                 ' from CO_BANK_ACCOUNT_REGISTER a, CO_TAX_DEPOSITS b ' +
                       ' where {AsOfNow<a>} and {AsOfNow<b>} and ' +
                         ' a.CO_TAX_DEPOSITS_NBR = b.CO_TAX_DEPOSITS_NBR and  b.DEPOSIT_TYPE = ''M'' ' ;
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if cdBarLink.Active then
       cdBarLink.Close;
    with TExecDSWrapper.Create(s) do
      DataRequest(AsVariant);
    Open;
    cdBarLink.Data := Data;
    Close;
  end;
end;

procedure TEDIT_CO_TAX_DEPOSIT.AfterDataSetsReopen;
begin
  inherited;
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    Close;
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 'CO_TAX_DEPOSITS_NBR, MISCELLANEOUS_CHECK_AMOUNT AMOUNT');
      SetMacro('TABLENAME', 'PR_MISCELLANEOUS_CHECKS');
      SetMacro('CONDITION', 'CO_TAX_DEPOSITS_NBR IS NOT NULL');
      DataRequest(AsVariant);
    end;
    Open;
    cdAmounts.Data := Data;
    Close;
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 'CO_TAX_DEPOSITS_NBR, AMOUNT');
      SetMacro('TABLENAME', 'CO_TAX_PAYMENT_ACH');
      SetMacro('CONDITION', 'CO_TAX_DEPOSITS_NBR IS NOT NULL');
      DataRequest(AsVariant);
    end;
    Open;
    cdAmounts.AppendData(Data, True);
    Close;

    with TExecDSWrapper.Create('TaxDepositAmountList') do
     DataRequest(AsVariant);
    Open;
    cdAmounts.AppendData(Data, True);
    Close;
  end;
  CreateBarLink;
  wwdsDetailDataChange(nil, nil);
end;

procedure TEDIT_CO_TAX_DEPOSIT.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER, aDS);
  inherited;
end;

function TEDIT_CO_TAX_DEPOSIT.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_TAX_DEPOSITS;
end;

function TEDIT_CO_TAX_DEPOSIT.GetInsertControl: TWinControl;
begin
  Result := dedtPayment_Reference_Number;
end;

procedure TEDIT_CO_TAX_DEPOSIT.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE, SupportDataSets, '');
  AddDSWithCheck(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER, EditDataSets, '');
end;

procedure TEDIT_CO_TAX_DEPOSIT.CheckManualType;
begin
  if cbTaxDepositType.Text = 'Manual' then
    pnlAlternateAmount.Visible := false
  else
    pnlAlternateAmount.Visible := true;
   // pnlAlternateAmount.Visible := cbTaxDepositType.ItemIndex <> 4;
 {   edBarAmount.Visible := not edAmount.Visible;

    if edBarAmount.Visible then
        lblAmount.Caption := '~Amount    '
    else
        lblAmount.Caption := 'Amount     '   }
end;


procedure TEDIT_CO_TAX_DEPOSIT.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if cdAmounts.Active and Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and not Assigned(Field) then
  begin
    if cdAmounts.FindKey([wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger]) then
      edAmount.Text := Format('%25m', [cdAmounts.FieldByName('AMOUNT').AsCurrency])
    else
      edAmount.Text := 'N/A';

    CheckManualType;

    wwdgTax_Deposit.Enabled := not (wwdsDetail.DataSet.State in [dsInsert, dsEdit ]);
  end;
end;


procedure TEDIT_CO_TAX_DEPOSIT.AddToBARManualDeposit;
begin
   if (DM_COMPANY.CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').AsInteger > 0) and
      (DM_COMPANY.CO.FieldByName('TAX_SERVICE').AsString = 'Y' )then
   begin
    if (wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger > 0) and
       (cdBarLink.Locate('CO_TAX_DEPOSITS_NBR',wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').Value,[])) and
       (DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Locate('CO_BANK_ACCOUNT_REGISTER_NBR',cdBarLink.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').Value,[])) then
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit
    else
    begin
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Append;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger :=
                            DM_COMPANY.CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').AsInteger;

      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('PROCESS_DATE').AsDateTime := Now;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_DATE').AsDateTime := Now;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').AsDateTime := Now;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger :=
                                           wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Outstanding;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('MANUAL_TYPE').AsString := BANK_REGISTER_MANUALTYPE_Other;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('REGISTER_TYPE').AsString := BANK_REGISTER_TYPE_Tax;

      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger;
    end;

      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('AMOUNT').AsCurrency :=
                                           wwdsDetail.DataSet.FieldByName('BarAmount').AsCurrency;
  end
  else
    EvMessage('This company is not on Full tax service; therefore, no Bank Account Register transaction could be created.', mtInformation, [mbOK]);
end;

procedure TEDIT_CO_TAX_DEPOSIT.cbTaxDepositTypeChange(Sender: TObject);
begin
  inherited;
  CheckManualType;
end;

procedure TEDIT_CO_TAX_DEPOSIT.PageControl1Change(Sender: TObject);
begin
  inherited;
  cbTaxDepositType.OnChange(self);
end;


initialization
  RegisterClass(TEDIT_CO_TAX_DEPOSIT);

end.
