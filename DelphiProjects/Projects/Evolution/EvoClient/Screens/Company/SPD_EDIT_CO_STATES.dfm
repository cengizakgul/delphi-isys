inherited EDIT_CO_STATES: TEDIT_CO_STATES
  Width = 776
  Height = 656
  inherited Panel1: TevPanel
    Width = 776
    inherited pnlFavoriteReport: TevPanel
      Left = 624
    end
    inherited pnlTopLeft: TevPanel
      Width = 624
    end
  end
  inherited PageControl1: TevPageControl
    Width = 776
    Height = 602
    HelpContext = 11001
    ActivePage = TabSheet2
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 768
        Height = 573
        inherited pnlBorder: TevPanel
          Width = 764
          Height = 569
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 764
          Height = 569
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited splEDIT_CO_BASE: TevSplitter
                    Left = 564
                    Width = 3
                    Visible = True
                  end
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 558
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 546
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 567
                    Width = 223
                    Visible = True
                    object fpStatesRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 211
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      OnResize = fpStatesRightResize
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'States'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Width = 299
              Height = 337
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 716
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 716
            Height = 462
            inherited Splitter1: TevSplitter
              Left = 676
              Height = 458
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 676
              Height = 458
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 626
                Height = 378
                IniAttributes.SectionName = 'TEDIT_CO_STATES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 679
              Width = 33
              Height = 458
              Align = alClient
              Visible = True
              Title = 'States'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 16
                Height = 378
                DisableThemesInTitle = False
                Selected.Strings = (
                  'STATE'#9'2'#9'State'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SaveToRegistry = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_STATES\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                DefaultSort = '-'
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtGeneral_Info: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      OnShow = tshtGeneral_InfoShow
      object sbDetails: TScrollBox
        Left = 0
        Top = 0
        Width = 768
        Height = 573
        Align = alClient
        TabOrder = 0
        object fpState: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 500
          Height = 328
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'State'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablState: TevLabel
            Left = 12
            Top = 35
            Width = 34
            Height = 16
            Caption = '~State'
            FocusControl = wwlcState
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablState_EIN: TevLabel
            Left = 12
            Top = 74
            Width = 55
            Height = 16
            Caption = '~State EIN'
            FocusControl = dedtState_EIN
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablState_SDI_EIN: TevLabel
            Left = 12
            Top = 152
            Width = 76
            Height = 16
            Caption = '~State SDI EIN'
            FocusControl = dedtState_SDI_EIN
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablState_Deposit_Frequency: TevLabel
            Left = 12
            Top = 191
            Width = 126
            Height = 16
            Caption = '~State Deposit Frequency'
            FocusControl = wwDBLookupCombo1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablState_Tax_Deposit_Method: TevLabel
            Left = 12
            Top = 113
            Width = 170
            Height = 13
            AutoSize = False
            Caption = '~State Tax Payment Method'
            FocusControl = wwcbState_Tax_Deposit_Method
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablState_EFT_Name: TevLabel
            Left = 210
            Top = 74
            Width = 141
            Height = 13
            Caption = 'Override Company EFT Name'
            FocusControl = dedtState_EFT_Name
          end
          object lablState_EFT_Enrollment_Status: TevLabel
            Left = 210
            Top = 113
            Width = 142
            Height = 16
            Caption = '~State EFT Enrollment Status'
            FocusControl = wwcbState_EFT_Enrollment_Status
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablState_EFT_PIN_Number: TevLabel
            Left = 210
            Top = 152
            Width = 109
            Height = 13
            Caption = 'State EFT PIN Number'
            FocusControl = dedtState_EFT_PIN_Number
          end
          object lablSTATE_EFT_EIN: TevLabel
            Left = 210
            Top = 191
            Width = 69
            Height = 13
            Caption = 'State EFT EIN'
          end
          object lablSTATE_SDI_EFT_EIN: TevLabel
            Left = 210
            Top = 230
            Width = 90
            Height = 13
            Caption = 'State SDI EFT EIN'
          end
          object lNYThresholdLabel: TevLabel
            Left = 106
            Top = 230
            Width = 91
            Height = 13
            Caption = 'NY Threshold Date'
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 269
            Width = 108
            Height = 13
            Caption = 'Tax Collection District  '
          end
          object evLabel5: TevLabel
            Left = 210
            Top = 269
            Width = 123
            Height = 13
            Caption = 'TCD Deposit Frequency   '
          end
          object evLabel6: TevLabel
            Left = 348
            Top = 269
            Width = 114
            Height = 13
            Caption = 'TCD Payment Method   '
          end
          object dedtState_EIN: TevDBEdit
            Left = 12
            Top = 89
            Width = 190
            Height = 21
            HelpContext = 11002
            DataField = 'STATE_EIN'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtState_SDI_EIN: TevDBEdit
            Left = 12
            Top = 167
            Width = 190
            Height = 21
            HelpContext = 11003
            DataField = 'STATE_SDI_EIN'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcState: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 96
            Height = 21
            HelpContext = 11035
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'3'#9'STATE'
              'NAME'#9'20'#9'NAME'#9'No')
            DataField = 'STATE'
            DataSource = wwdsDetail
            LookupTable = DM_SY_STATES.SY_STATES
            LookupField = 'STATE'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = wwlcStateChange
          end
          object wwcbState_Tax_Deposit_Method: TevDBComboBox
            Left = 12
            Top = 128
            Width = 190
            Height = 21
            HelpContext = 11005
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATE_TAX_DEPOSIT_METHOD'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'EFT Credit'#9'C'
              'EFT Debit'#9'D'
              'Check'#9'H'
              'Notices'#9'N'
              'Notices & Checks'#9'X'
              'Notices & EFT Credit'#9'Z'
              'Notices & EFT Debit'#9'Y')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
            OnChange = wwcbState_Tax_Deposit_MethodChange
            OnDropDown = wwcbState_Tax_Deposit_MethodDropDown
          end
          object wwDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 206
            Width = 190
            Height = 21
            HelpContext = 11006
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'DESCRIPTION')
            DataField = 'SY_STATE_DEPOSIT_FREQ_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SY_STATE_DEPOSIT_FREQ.SY_STATE_DEPOSIT_FREQ
            LookupField = 'SY_STATE_DEPOSIT_FREQ_NBR'
            Style = csDropDownList
            PopupMenu = pmStateDepositFrequency
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object dedtState_EFT_Name: TevDBEdit
            Left = 210
            Top = 89
            Width = 268
            Height = 21
            HelpContext = 11024
            DataField = 'STATE_EFT_NAME'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbState_EFT_Enrollment_Status: TevDBComboBox
            Left = 210
            Top = 128
            Width = 268
            Height = 21
            HelpContext = 11025
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATE_EFT_ENROLLMENT_STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Pending'#9'P'
              'Active'#9'A'
              'Term'#9'T')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 9
            UnboundDataType = wwDefault
          end
          object dedtState_EFT_PIN_Number: TevDBEdit
            Left = 210
            Top = 167
            Width = 268
            Height = 21
            HelpContext = 11026
            DataField = 'STATE_EFT_PIN_NUMBER'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 10
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtSTATE_EFT_EIN: TevDBEdit
            Left = 210
            Top = 206
            Width = 268
            Height = 21
            HelpContext = 11026
            DataField = 'STATE_EFT_EIN'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 11
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtSTATE_SDI_EFT_EIN: TevDBEdit
            Left = 210
            Top = 245
            Width = 268
            Height = 21
            HelpContext = 11026
            DataField = 'STATE_SDI_EFT_EIN'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 12
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object lNYThresholdPicker: TevDateTimePicker
            Left = 106
            Top = 245
            Width = 96
            Height = 21
            Date = 36970.000000000000000000
            Time = 36970.000000000000000000
            TabOrder = 6
            OnChange = lNYThresholdPickerChange
          end
          object butnAttachState: TevBitBtn
            Left = 210
            Top = 35
            Width = 268
            Height = 25
            Caption = 'Attach this State to Processed Payrolls'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 15
            OnClick = butnAttachSUIClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFD5D5D5CCCCCCCCCCCCCCCCCCD9D9D9FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5CCCCCCCC
              CCCCCCCCCCD9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCCCCC998F83978A7F978A7F978A7F9E968CCCCCCCFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC8E8E8E89898989
              8989898989959594CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFD5D5D5978A7FAEA59DC8C1BBC6BFB9C6BEB8ADA39A978A7FD5D5D5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5898989A5A5A5C1C1C1BF
              BFBFBEBEBEA2A2A2898989D5D5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC0B8B2978A7FB6ACA4FFFFFFFFFFFFFFFFFFC4BCB5ADA39A9A8F84FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8B8B8898989ABABABFFFFFFFF
              FFFFFFFFFFBCBCBBA2A2A28E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC1B9B3978A7FFFFFFFCCCCCCCCCCCCCCCCCCFFFFFFC5C0B8978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B9B9898989FFFFFFCCCCCCCC
              CCCCCCCCCCFFFFFFBFBFBF898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BEB6978A7FCCCCCC978A7F978A7F978A7FCCCCCCC6C0B8978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD898989CCCCCC89898989
              8989898989CCCCCCBFBFBF898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC3BDB5978A7FA4998EC8C2BAC4BEB7BAB2AA978A7FC6C0B9978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989989898C2C2C1BE
              BDBDB2B1B1898989C0C0C0898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC3BDB5978A7FC0B8B0968A7DFFFFFFC2BBB3978A7FC7C1B9978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989B8B7B7888888FF
              FFFFBBBBBB898989C1C1C1898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BDB5978A7FC6BFB896887BFFFFFFC2BBB3978A7FC8C0B9978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989BFBFBF868686FF
              FFFFBBBBBB898989C0C0C0898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BDB5978A7FC6BFB896897CFFFFFFCFC9C4978A7FC5BEB7978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989BFBFBF878787FF
              FFFFCAC9C9898989BEBDBD898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BDB5978A7FC7C0B9998D80FFFFFFFFFFFFFFFFFFC2BAB2978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989C0C0C08B8B8BFF
              FFFFFFFFFFFFFFFFBABABA898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFCFC9C4978A7FC8C0B99D8F82FFFFFFFFFFFFFDFDFDC2B8B1978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAC9C9898989C0C0C08E8E8EFF
              FFFFFFFFFFFEFEFEB8B8B8898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFC7BFB99D9084E7E7E7FFFFFFE9E9E9BEB4AD978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF8F8F8FE8
              E7E7FFFFFFE9E9E9B4B4B4898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFC5BCB6A4968CB1AAA4CCCCCCBCB7B2A5978EB4ACA5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBB969595AA
              A9A9CCCCCCB8B7B7979696AAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFD2CBC6C1B8B2A3968C9E8F839D8E839F9085CCC4BEFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBCBCBB8B8B896
              95958E8E8E8D8D8D8F8F8FC4C3C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFDBD6D2C4BBB4C4BCB5C5BDB6DAD5D1FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D6D6BB
              BBBBBCBCBBBDBCBCD5D5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object cbAppliedFor: TevDBCheckBox
            Left = 12
            Top = 247
            Width = 80
            Height = 17
            Caption = 'Applied For'
            DataField = 'APPLIED_FOR'
            DataSource = wwdsDetail
            TabOrder = 5
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object cbTCDPaymentMethod: TevDBComboBox
            Left = 348
            Top = 285
            Width = 130
            Height = 21
            HelpContext = 11025
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TCD_PAYMENT_METHOD'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmTCDPaymentMethod
            Sorted = False
            TabOrder = 14
            UnboundDataType = wwDefault
          end
          object cbTaxCollection: TevDBLookupCombo
            Left = 12
            Top = 285
            Width = 190
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'40'#9'Agency Name'#9'F')
            DataField = 'TaxCollectionDistrict'
            DataSource = wwdsDetail
            LookupTable = cdTCD_CollectionAgency
            LookupField = 'SY_GLOBAL_AGENCY_NBR'
            Style = csDropDownList
            PopupMenu = pmTCDDistrict
            TabOrder = 7
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnMouseDown = cbTaxCollectionMouseDown
            ForceVersioning = True
            ForceVersioningAlternateControl = cbTCDDEpositFrequency
          end
          object cbTCDDEpositFrequency: TevDBLookupCombo
            Left = 210
            Top = 285
            Width = 130
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F')
            DataField = 'TCD_DEPOSIT_FREQUENCY_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SY_AGENCY_DEPOSIT_FREQ.SY_AGENCY_DEPOSIT_FREQ
            LookupField = 'SY_AGENCY_DEPOSIT_FREQ_NBR'
            Style = csDropDownList
            PopupMenu = pmTCDDepositFrequency
            TabOrder = 13
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnEnter = cbTCDDEpositFrequencyEnter
          end
        end
        object fpNotes: TisUIFashionPanel
          Left = 12
          Top = 344
          Width = 500
          Height = 200
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Notes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablNotes: TevLabel
            Left = 12
            Top = 35
            Width = 28
            Height = 13
            Caption = 'Notes'
          end
          object dmemNotes: TEvDBMemo
            Left = 12
            Top = 50
            Width = 465
            Height = 128
            DataField = 'NOTES'
            DataSource = wwdsDetail
            ScrollBars = ssVertical
            TabOrder = 0
          end
        end
      end
    end
    object tshtFlag_And_Settings: TTabSheet
      Caption = 'Flags && Settings'
      ImageIndex = 39
      object sbFlags: TScrollBox
        Left = 0
        Top = 0
        Width = 768
        Height = 573
        Align = alClient
        TabOrder = 0
        object isUIFashionPanel1: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 438
          Height = 312
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'isUIFashionPanel1'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Flags & Settings'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablTax_Return_Code: TevLabel
            Left = 12
            Top = 35
            Width = 81
            Height = 13
            Caption = 'Tax Return Code'
            FocusControl = dedtTax_Return_Code
          end
          object lablUse_DBA_Tax_Return: TevLabel
            Left = 216
            Top = 35
            Width = 116
            Height = 16
            Caption = '~Name For Tax Returns'
            FocusControl = wwcbUse_DBA_Tax_Return
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel7: TevLabel
            Left = 87
            Top = 277
            Width = 255
            Height = 13
            Caption = 'Use Effective Dates to Change Tax Statuses'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object drgpIgnore_State_Tax_Dep_Threshold: TevDBRadioGroup
            Left = 216
            Top = 152
            Width = 200
            Height = 37
            HelpContext = 11019
            Caption = '~Ignore State Tax Deposit Threshold'
            Columns = 2
            DataField = 'IGNORE_STATE_TAX_DEP_THRESHOLD'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 9
            Values.Strings = (
              'Y'
              'N')
            OnChange = drgpIgnore_State_Tax_Dep_ThresholdChange
          end
          object drgpState_Non_Profit: TevDBRadioGroup
            Left = 216
            Top = 191
            Width = 200
            Height = 37
            HelpContext = 11020
            Caption = '~Is This State Inactive?'
            Columns = 2
            DataField = 'STATE_NON_PROFIT'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 10
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpState_Exempt: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 200
            Height = 37
            HelpContext = 11021
            Caption = '~State Exempt'
            Columns = 2
            DataField = 'STATE_EXEMPT'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object drgpSUI_Exempt: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 200
            Height = 37
            HelpContext = 11022
            Caption = '~SUI Exempt'
            Columns = 2
            DataField = 'SUI_EXEMPT'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object drgpEE_SDI_Exempt: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 200
            Height = 37
            HelpContext = 11015
            Caption = '~EE SDI Exempt'
            Columns = 2
            DataField = 'EE_SDI_EXEMPT'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object drgpUse_State_For_SUI: TevDBRadioGroup
            Left = 12
            Top = 230
            Width = 200
            Height = 37
            HelpContext = 11016
            Caption = '~Use State for SUI'
            Columns = 2
            DataField = 'USE_STATE_FOR_SUI'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
          end
          object dedtTax_Return_Code: TevDBEdit
            Left = 12
            Top = 50
            Width = 200
            Height = 21
            HelpContext = 11014
            DataField = 'TAX_RETURN_CODE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbUse_DBA_Tax_Return: TevDBComboBox
            Left = 216
            Top = 50
            Width = 200
            Height = 21
            HelpContext = 11041
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'USE_DBA_ON_TAX_RETURN'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'DBA'#9'D'
              'Legal'#9'L'
              'Primary'#9'P')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object evDBRadioGroup2: TevDBRadioGroup
            Left = 216
            Top = 74
            Width = 200
            Height = 37
            HelpContext = 11016
            Caption = '~Auto Apply State Credit '
            Columns = 2
            DataField = 'MO_TAX_CREDIT_ACTIVE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 7
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpER_SDI_Exempt: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 200
            Height = 37
            HelpContext = 11015
            Caption = '~ER SDI Exempt'
            Columns = 2
            DataField = 'ER_SDI_EXEMPT'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object evDBRadioGroup3: TevDBRadioGroup
            Left = 216
            Top = 230
            Width = 200
            Height = 37
            HelpContext = 11016
            Caption = '~Offers Healthcare'
            Columns = 2
            DataField = 'COMPANY_PAID_HEALTH_INSURANCE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 11
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpFinal_Tax_Return: TevDBRadioGroup
            Left = 216
            Top = 113
            Width = 200
            Height = 37
            HelpContext = 14516
            Caption = '~Last Tax Return'
            Columns = 2
            DataField = 'LAST_TAX_RETURN'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 8
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object fpGLTags: TisUIFashionPanel
          Left = 8
          Top = 328
          Width = 438
          Height = 158
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpGLTags'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'G/L Tags'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label1: TevLabel
            Left = 12
            Top = 61
            Width = 25
            Height = 13
            Caption = 'State'
            FocusControl = DBEdit1
          end
          object Label2: TevLabel
            Left = 12
            Top = 90
            Width = 35
            Height = 13
            Caption = 'EE SDI'
            FocusControl = DBEdit2
          end
          object Label3: TevLabel
            Left = 12
            Top = 119
            Width = 36
            Height = 13
            Caption = 'ER SDI'
            FocusControl = DBEdit3
          end
          object evLabel36: TevLabel
            Left = 12
            Top = 35
            Width = 29
            Height = 16
            Caption = 'Tags'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object evLabel55: TevLabel
            Left = 143
            Top = 35
            Width = 18
            Height = 16
            Caption = 'DR'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object evLabel56: TevLabel
            Left = 322
            Top = 35
            Width = 18
            Height = 16
            Caption = 'CR'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DBEdit1: TevDBEdit
            Left = 247
            Top = 57
            Width = 168
            Height = 21
            HelpContext = 11036
            DataField = 'STATE_GENERAL_LEDGER_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit2: TevDBEdit
            Left = 247
            Top = 86
            Width = 168
            Height = 21
            HelpContext = 11037
            DataField = 'EE_SDI_GENERAL_LEDGER_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit3: TevDBEdit
            Left = 247
            Top = 115
            Width = 168
            Height = 21
            HelpContext = 11038
            DataField = 'ER_SDI_GENERAL_LEDGER_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit6: TevDBEdit
            Left = 68
            Top = 57
            Width = 168
            Height = 21
            HelpContext = 11036
            DataField = 'STATE_OFFSET_GL_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit7: TevDBEdit
            Left = 68
            Top = 86
            Width = 168
            Height = 21
            HelpContext = 11037
            DataField = 'EE_SDI_OFFSET_GL_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit8: TevDBEdit
            Left = 68
            Top = 115
            Width = 168
            Height = 21
            HelpContext = 11038
            DataField = 'ER_SDI_OFFSET_GL_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'SUI'
      ImageIndex = 41
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 768
        Height = 573
        Align = alClient
        TabOrder = 0
        object evPanel1: TevPanel
          Left = 0
          Top = 0
          Width = 657
          Height = 568
          BevelOuter = bvNone
          TabOrder = 0
          object fpSummary: TisUIFashionPanel
            Left = 209
            Top = 8
            Width = 445
            Height = 327
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpSummary'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object wwDBGrid3: TevDBGrid
              Left = 12
              Top = 35
              Width = 410
              Height = 269
              DisableThemesInTitle = False
              Selected.Strings = (
                'DESCRIPTION'#9'16'#9'Description'#9'F'
                'State'#9'2'#9'State'#9'F'
                'TAX_RETURN_CODE'#9'15'#9'Tax Return Code'#9'F'
                'RATE'#9'10'#9'Rate'#9'F'
                'PAYMENT_METHOD'#9'1'#9'Payment Method'#9'F')
              IniAttributes.Enabled = False
              IniAttributes.SaveToRegistry = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_CO_STATES\wwDBGrid3'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              DataSource = dsSUI
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
          object isUIFashionPanel2: TisUIFashionPanel
            Left = 8
            Top = 343
            Width = 646
            Height = 223
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'isUIFashionPanel2'
            Color = 14737632
            TabOrder = 1
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'SUI Details'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object lablSY_SUI_Number: TevLabel
              Left = 12
              Top = 86
              Width = 27
              Height = 16
              Caption = '~SUI'
              FocusControl = wwlcSY_SUI_Number
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel3: TevLabel
              Left = 110
              Top = 125
              Width = 81
              Height = 13
              Caption = 'Tax Return Code'
              FocusControl = evDBEdit3
            end
            object lablDescription: TevLabel
              Left = 208
              Top = 86
              Width = 62
              Height = 16
              Caption = '~Description'
              FocusControl = dedtDescription
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object lablRate: TevLabel
              Left = 208
              Top = 125
              Width = 32
              Height = 16
              Caption = '~Rate'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel4: TevLabel
              Left = 12
              Top = 67
              Width = 334
              Height = 13
              Caption = 'Rate can be changed only as of the beginning of a quarter'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object lablPayment_Method: TevLabel
              Left = 12
              Top = 125
              Width = 89
              Height = 16
              Caption = '~Payment Method'
              FocusControl = wwcbPayment_Method
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object SpeedButton1: TevSpeedButton
              Left = 268
              Top = 35
              Width = 116
              Height = 25
              Hint = 'Insert new record'
              Caption = 'Create All'
              HideHint = True
              AutoSize = False
              OnClick = SpeedButton1Click
              NumGlyphs = 2
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                DDDDDDA3C0B3349C6C008C4B008B4A008B4A008C4B2B92638BA89BD9D9D9FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDBCBCBB8E8E8E7D7C7C7C7B7B7C
                7B7B7D7C7C868585A3A3A3D9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1
                44A27700905001A16900AA7600AB7700AB7700AA7601A169009050328660C0C0
                C0FFFFFFFFFFFFFFFFFFFFFFFFE1E1E19796968180809393939C9C9C9D9D9D9D
                9D9D9C9C9C9393938180807C7B7BC0C0C0FFFFFFFFFFFFFFFFFFF5F5F555A882
                00915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092531F75
                4EBFBFBFFFFFFFFFFFFFF6F6F59C9C9C8282829E9E9EB4B4B4C5C5C5CECECECE
                CECEC5C5C5B4B4B49D9D9D8383836A6A6ABFBFBFFFFFFFFFFFFFAECBBE009051
                0FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB760090
                502E5E48D9D9D9FFFFFFC7C7C7828181A6A6A6C2C2C1C5C5C5C0C0C0FFFFFFFF
                FFFFC0C0C0C5C5C5C1C1C19D9D9D818080585858D9D9D9FFFFFF369D6C16AB78
                11C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C18C01A1
                690F7747488067F5F5F59090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFFFFFFFF
                FFFFBCBCBBC2C2C1C4C4C4B2B2B29393936B6B6B797979F6F6F5008A4838C49C
                00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D09A00AB
                76008C4B345245AECBBE7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FFFFFFFF
                FFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7C4E4E4EC7C7C700894651D2AF
                12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF9700AD
                78008B4A464646369E6E7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7B45454592929200884566DDBE
                10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD9700AD
                78008B4A454545008C4B797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7B4444447D7C7C00884676E0C5
                00CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB9900AB
                76008C4B454545008B4A797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FFFFFFFF
                FFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7C4444447C7B7B41A67559C9A4
                49DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C19000A1
                68157D4D464646008B4A999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FFFFFFFF
                FFFFB4B4B4B9B9B9BDBCBCB2B2B29393937170704545457C7B7BCCE8DB0A9458
                ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7870090
                50405C4F335E51008C4BE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FFFFFFFF
                FFFFB5B5B5B9B9B9BABABAAAA9A98180805858585959597D7C7CFFFFFF48A579
                199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393552B7E
                574848480E8F6140A878FFFFFF9999998E8E8EFBFBFBDADADAB6B6B5B2B1B1B2
                B2B2B7B6B6BEBDBDB5B5B58584847473734848488282829B9B9BFFFFFFFFFFFF
                2E835B0E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915228865B4B4B
                4B337561009050CCE9DCFFFFFFFFFFFF797979878787CBCBCBECECECE8E7E7DC
                DBDBCBCBCBA8A8A78282827B7B7B4B4B4B6E6E6E818080E5E5E5FFFFFFFFFFFF
                FFFFFF3B775B157A49008744008743008744008946288F5E5B776A4A4A4A3F6F
                610393556ABD96FFFFFFFFFFFFFFFFFFFFFFFF6F6F6F6D6D6D78787778787778
                78777A79798282827373734A4A4A6A6A6A858484B2B1B1FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF63A484355A494343434343434343434444444B6C63329F790391
                5255B388FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9A9A9A55545443434343
                4343434343444444686868949393828282A6A6A6FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9
                DCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E4E49B9B9B78
                78777878777878777A79799D9D9DE5E5E5FFFFFFFFFFFFFFFFFF}
              Flat = True
              ParentColor = False
              ShortCut = 0
            end
            object evLabel10: TevLabel
              Left = 404
              Top = 125
              Width = 57
              Height = 13
              BiDiMode = bdLeftToRight
              Caption = 'SUI GL Tag'
              FocusControl = evDBEdit5
              ParentBiDiMode = False
            end
            object evLabel27: TevLabel
              Left = 517
              Top = 125
              Width = 88
              Height = 13
              Caption = 'SUI GL Offset Tag'
              FocusControl = evDBEdit2
            end
            object evLabel1: TevLabel
              Left = 306
              Top = 125
              Width = 56
              Height = 13
              Caption = 'Global Rate'
            end
            object lblLastReviewedDate: TevLabel
              Left = 208
              Top = 164
              Width = 123
              Height = 13
              Caption = 'Rate Last Reviewed Date'
              FocusControl = evDBEdit3
            end
            object wwlcSY_SUI_Number: TevDBLookupCombo
              Left = 12
              Top = 101
              Width = 188
              Height = 21
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'SUI_TAX_NAME'#9'40'#9'SUI_TAX_NAME')
              DataField = 'SY_SUI_NBR'
              DataSource = dsSUI
              LookupTable = DM_SY_SUI.SY_SUI
              LookupField = 'SY_SUI_NBR'
              Style = csDropDownList
              TabOrder = 0
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = False
              OnChange = wwlcSY_SUI_NumberChange
            end
            object evDBEdit3: TevDBEdit
              Left = 110
              Top = 140
              Width = 90
              Height = 21
              DataField = 'TAX_RETURN_CODE'
              DataSource = dsSUI
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 4
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object dedtDescription: TevDBEdit
              Left = 208
              Top = 101
              Width = 188
              Height = 21
              DataField = 'DESCRIPTION'
              DataSource = dsSUI
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object wwdeRate: TevDBEdit
              Left = 208
              Top = 140
              Width = 90
              Height = 21
              DataField = 'RATE'
              DataSource = dsSUI
              Picture.PictureMask = 
                '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
                '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
                '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
              TabOrder = 5
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
              AutoPopupEffectiveDateDialog = True
              AutoPopupForceQuartersOnly = True
            end
            object wwcbPayment_Method: TevDBComboBox
              Left = 12
              Top = 140
              Width = 90
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'PAYMENT_METHOD'
              DataSource = dsSUI
              DropDownCount = 8
              ItemHeight = 0
              Items.Strings = (
                'EFT Credit'#9'C'
                'EFT Debit'#9'D'
                'Check'#9'H'
                'Notices'#9'N'
                'Notices & Checks'#9'X'
                'Notices & EFT Credit'#9'Z'
                'Notices & EFT Debit'#9'Y')
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 3
              UnboundDataType = wwDefault
              OnChange = wwcbPayment_MethodChange
              OnDropDown = wwcbPayment_MethodDropDown
            end
            inline MiniNavigationFrame: TMiniNavigationFrame
              Left = 387
              Top = 35
              Width = 244
              Height = 25
              TabOrder = 14
              inherited SpeedButton1: TevSpeedButton
                Left = 1
                Width = 116
                Caption = 'Create'
                Glyph.Data = {
                  36060000424D3606000000000000360000002800000020000000100000000100
                  18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
                  CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
                  E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
                  7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
                  82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
                  9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
                  55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
                  5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
                  CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
                  0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
                  76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
                  FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
                  16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
                  8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
                  FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
                  38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
                  9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
                  FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
                  51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
                  9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
                  66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
                  9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
                  76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
                  9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
                  FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
                  59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
                  9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
                  FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
                  0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
                  87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
                  FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
                  55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
                  556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
                  B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
                  88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
                  E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
                  78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              end
              inherited SpeedButton2: TevSpeedButton
                Left = 121
                Width = 116
                Caption = 'Delete'
                Glyph.Data = {
                  36060000424D3606000000000000360000002800000020000000100000000100
                  18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
                  CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
                  F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
                  6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
                  A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
                  CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
                  A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
                  2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
                  C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
                  9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
                  5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
                  F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
                  9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
                  576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
                  FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
                  97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
                  4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
                  F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
                  8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
                  3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
                  F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
                  334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
                  F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
                  2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
                  EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
                  3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
                  EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
                  84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
                  8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
                  F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
                  8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
                  9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
                  F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
                  8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
                  2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
                  C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
                  807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
                  C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
                  EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
                  CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
                  6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              end
              inherited evActionList1: TevActionList
                Left = 96
                Top = 65520
              end
            end
            object butnAttachSUI: TevBitBtn
              Left = 12
              Top = 35
              Width = 252
              Height = 25
              Caption = 'Attach this SUI to Processed Payrolls'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 13
              OnClick = butnAttachSUIClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFD5D5D5CCCCCCCCCCCCCCCCCCD9D9D9FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5CCCCCCCC
                CCCCCCCCCCD9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFCCCCCC998F83978A7F978A7F978A7F9E968CCCCCCCFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC8E8E8E89898989
                8989898989959594CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFD5D5D5978A7FAEA59DC8C1BBC6BFB9C6BEB8ADA39A978A7FD5D5D5FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5898989A5A5A5C1C1C1BF
                BFBFBEBEBEA2A2A2898989D5D5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFC0B8B2978A7FB6ACA4FFFFFFFFFFFFFFFFFFC4BCB5ADA39A9A8F84FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8B8B8898989ABABABFFFFFFFF
                FFFFFFFFFFBCBCBBA2A2A28E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFC1B9B3978A7FFFFFFFCCCCCCCCCCCCCCCCCCFFFFFFC5C0B8978A7FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B9B9898989FFFFFFCCCCCCCC
                CCCCCCCCCCFFFFFFBFBFBF898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFC4BEB6978A7FCCCCCC978A7F978A7F978A7FCCCCCCC6C0B8978A7FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD898989CCCCCC89898989
                8989898989CCCCCCBFBFBF898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFC3BDB5978A7FA4998EC8C2BAC4BEB7BAB2AA978A7FC6C0B9978A7FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989989898C2C2C1BE
                BDBDB2B1B1898989C0C0C0898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFC3BDB5978A7FC0B8B0968A7DFFFFFFC2BBB3978A7FC7C1B9978A7FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989B8B7B7888888FF
                FFFFBBBBBB898989C1C1C1898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFC4BDB5978A7FC6BFB896887BFFFFFFC2BBB3978A7FC8C0B9978A7FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989BFBFBF868686FF
                FFFFBBBBBB898989C0C0C0898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFC4BDB5978A7FC6BFB896897CFFFFFFCFC9C4978A7FC5BEB7978A7FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989BFBFBF878787FF
                FFFFCAC9C9898989BEBDBD898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFC4BDB5978A7FC7C0B9998D80FFFFFFFFFFFFFFFFFFC2BAB2978A7FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989C0C0C08B8B8BFF
                FFFFFFFFFFFFFFFFBABABA898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFCFC9C4978A7FC8C0B99D8F82FFFFFFFFFFFFFDFDFDC2B8B1978A7FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAC9C9898989C0C0C08E8E8EFF
                FFFFFFFFFFFEFEFEB8B8B8898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFC7BFB99D9084E7E7E7FFFFFFE9E9E9BEB4AD978A7FFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF8F8F8FE8
                E7E7FFFFFFE9E9E9B4B4B4898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFC5BCB6A4968CB1AAA4CCCCCCBCB7B2A5978EB4ACA5FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBB969595AA
                A9A9CCCCCCB8B7B7979696AAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFD2CBC6C1B8B2A3968C9E8F839D8E839F9085CCC4BEFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBCBCBB8B8B896
                95958E8E8E8D8D8D8F8F8FC4C3C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFDBD6D2C4BBB4C4BCB5C5BDB6DAD5D1FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D6D6BB
                BBBBBCBCBBBDBCBCD5D5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              NumGlyphs = 2
              Margin = 0
            end
            object evDBEdit5: TevDBEdit
              Left = 404
              Top = 140
              Width = 105
              Height = 21
              HelpContext = 10668
              DataField = 'GL_TAG'
              DataSource = dsSUI
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 7
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object evDBEdit2: TevDBEdit
              Left = 517
              Top = 140
              Width = 105
              Height = 21
              HelpContext = 10668
              DataField = 'GL_OFFSET_TAG'
              DataSource = dsSUI
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 8
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object dblkupGLOBAL_RATE: TevDBLookupCombo
              Left = 306
              Top = 140
              Width = 90
              Height = 21
              HelpContext = 17717
              TabStop = False
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'GLOBAL_RATE'#9'8'#9'GLOBAL_RATE')
              DataField = 'SY_SUI_NBR'
              DataSource = dsSUI
              LookupTable = DM_SY_SUI.SY_SUI
              LookupField = 'SY_SUI_NBR'
              Style = csDropDownList
              Enabled = False
              TabOrder = 6
              AutoDropDown = True
              ShowButton = False
              PreciseEditRegion = False
              AllowClearKey = False
            end
            object evDBRadioGroup4: TevDBRadioGroup
              Left = 12
              Top = 164
              Width = 188
              Height = 37
              HelpContext = 14516
              Caption = '~Last Tax Return'
              Columns = 2
              DataField = 'LAST_TAX_RETURN'
              DataSource = dsSUI
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 10
              Values.Strings = (
                'Y'
                'N')
            end
            object rgAlternateWage: TevDBRadioGroup
              Left = 518
              Top = 86
              Width = 103
              Height = 37
              Caption = ' Alternate Wage '
              Columns = 2
              DataField = 'ALTERNATE_WAGE'
              DataSource = dsSUI
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 9
              Values.Strings = (
                'Y'
                'N')
            end
            object evDBCheckBox1: TevDBCheckBox
              Left = 404
              Top = 103
              Width = 82
              Height = 17
              Caption = 'Applied For'
              DataField = 'APPLIED_FOR'
              DataSource = dsSUI
              TabOrder = 2
              ValueChecked = 'True'
              ValueUnchecked = 'False'
            end
            object evDBRadioGroup5: TevDBRadioGroup
              Left = 517
              Top = 164
              Width = 105
              Height = 37
              HelpContext = 11021
              Caption = '~SUI Reimburser'
              Columns = 2
              DataField = 'SUI_REIMBURSER'
              DataSource = dsSUI
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 12
              Values.Strings = (
                'Y'
                'N')
              AutoPopupEffectiveDateDialog = True
              AutoPopupForceQuartersOnly = True
            end
            object evDBRadioGroup1: TevDBRadioGroup
              Left = 404
              Top = 164
              Width = 105
              Height = 37
              HelpContext = 11021
              Caption = '~SUI Inactive'
              Columns = 2
              DataField = 'FINAL_TAX_RETURN'
              DataSource = dsSUI
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 11
              Values.Strings = (
                'Y'
                'N')
            end
            object edtLastReviewedDate: TevDBDateTimePicker
              Left = 208
              Top = 180
              Width = 124
              Height = 21
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'RATE_LAST_REVIEWED_DATE'
              DataSource = dsSUI
              Epoch = 1950
              ShowButton = True
              TabOrder = 15
            end
          end
        end
        object fpSUI: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 193
          Height = 327
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSUI'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'SUI'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablSUI_Tax_Deposit_Method: TevLabel
            Left = 12
            Top = 35
            Width = 110
            Height = 16
            Caption = '~Tax Payment Method'
            FocusControl = wwcbSUI_Tax_Deposit_Method
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablSUI_EIN: TevLabel
            Left = 12
            Top = 74
            Width = 27
            Height = 16
            Caption = '~EIN'
            FocusControl = dedtSUI_EIN
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablSUI_Tax_Deposit_Frequency: TevLabel
            Left = 12
            Top = 113
            Width = 119
            Height = 16
            Caption = '~Tax Deposit Frequency'
            FocusControl = wwcbSUI_Tax_Deposit_Frequency
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablSUI_EFT_Name: TevLabel
            Left = 12
            Top = 152
            Width = 51
            Height = 13
            Caption = 'EFT Name'
            FocusControl = dedtSUI_EFT_Name
          end
          object lablSUI_EFT_Enrollment_Status: TevLabel
            Left = 12
            Top = 191
            Width = 114
            Height = 16
            Caption = '~EFT Enrollment Status'
            FocusControl = wwcbSUI_EFT_Enrollment_Status
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablSUI_EFT_PIN_Number: TevLabel
            Left = 12
            Top = 230
            Width = 81
            Height = 13
            Caption = 'EFT PIN Number'
            FocusControl = dedtSUI_EFT_PIN_Number
          end
          object lablSUI_EFT_EIN: TevLabel
            Left = 12
            Top = 269
            Width = 41
            Height = 13
            Caption = 'EFT EIN'
          end
          object wwcbSUI_Tax_Deposit_Method: TevDBComboBox
            Left = 12
            Top = 50
            Width = 162
            Height = 21
            HelpContext = 11009
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'SUI_TAX_DEPOSIT_METHOD'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'EFT Credit'#9'C'
              'EFT Debit'#9'D'
              'Check'#9'H'
              'Notices'#9'N'
              'Notices & Checks'#9'X'
              'Notices & EFT Credit'#9'Z'
              'Notices & EFT Debit'#9'Y')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
            OnChange = wwcbSUI_Tax_Deposit_MethodChange
            OnDropDown = wwcbSUI_Tax_Deposit_MethodDropDown
          end
          object dedtSUI_EIN: TevDBEdit
            Left = 12
            Top = 89
            Width = 162
            Height = 21
            HelpContext = 11004
            DataField = 'SUI_EIN'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbSUI_Tax_Deposit_Frequency: TevDBComboBox
            Left = 12
            Top = 128
            Width = 162
            Height = 21
            HelpContext = 11010
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'SUI_TAX_DEPOSIT_FREQUENCY'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Quarterly'#9'Q')
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmSUITaxDepositFrequency
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object dedtSUI_EFT_Name: TevDBEdit
            Left = 12
            Top = 167
            Width = 162
            Height = 21
            HelpContext = 11027
            DataField = 'SUI_EFT_NAME'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbSUI_EFT_Enrollment_Status: TevDBComboBox
            Left = 12
            Top = 206
            Width = 162
            Height = 21
            HelpContext = 11028
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'SUI_EFT_ENROLLMENT_STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Pending'#9'P'
              'Active'#9'A'
              'Term'#9'T')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
          object dedtSUI_EFT_PIN_Number: TevDBEdit
            Left = 12
            Top = 245
            Width = 162
            Height = 21
            HelpContext = 11029
            DataField = 'SUI_EFT_PIN_NUMBER'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtSUI_EFT_EIN: TevDBEdit
            Left = 12
            Top = 284
            Width = 162
            Height = 21
            HelpContext = 11026
            DataField = 'SUI_EFT_EIN'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'PA Locations'
      ImageIndex = 40
      object evSpeedButton1: TevSpeedButton
        Left = -27
        Top = 8
        Width = 25
        Height = 25
        Hint = 'Insert new record'
        HideHint = True
        AutoSize = False
        NumGlyphs = 2
        Flat = True
        ParentColor = False
        ShortCut = 0
      end
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 768
        Height = 573
        Align = alClient
        TabOrder = 0
        object fpPASummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 816
          Height = 235
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpPASummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgCompany_Locs: TevDBGrid
            Left = 12
            Top = 35
            Width = 780
            Height = 177
            HelpContext = 14001
            TabStop = False
            DisableThemesInTitle = False
            Selected.Strings = (
              'ACCOUNT_NUMBER'#9'30'#9'Account Number'#9'F'
              'ADDRESS1'#9'40'#9'Address'#9'F'
              'CITY'#9'1'#9'City'#9'F'
              'ZIP_CODE'#9'1'#9'ZIP Code'#9'F'
              'STATE'#9'40'#9'State'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_STATES\wwdgCompany_Locs'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsLocations
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpDetails: TisUIFashionPanel
          Left = 8
          Top = 251
          Width = 816
          Height = 131
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDetails'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Location Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel8: TevLabel
            Left = 12
            Top = 35
            Width = 40
            Height = 13
            Alignment = taRightJustify
            Caption = 'Account'
            FocusControl = evDBEdit1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label62: TevLabel
            Left = 175
            Top = 35
            Width = 47
            Height = 13
            Alignment = taRightJustify
            Caption = 'Address 1'
            FocusControl = DBEdit27
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label68: TevLabel
            Left = 338
            Top = 35
            Width = 47
            Height = 13
            Alignment = taRightJustify
            Caption = 'Address 2'
            FocusControl = DBEdit28
          end
          object Label67: TevLabel
            Left = 501
            Top = 35
            Width = 17
            Height = 13
            Alignment = taRightJustify
            Caption = 'City'
            FocusControl = DBEdit29
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label66: TevLabel
            Left = 638
            Top = 35
            Width = 25
            Height = 13
            Caption = 'State'
            FocusControl = DBEdit30
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label65: TevLabel
            Left = 698
            Top = 35
            Width = 15
            Height = 13
            Alignment = taRightJustify
            Caption = 'Zip'
            FocusControl = DBEdit31
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object AddLocationBtn: TevSpeedButton
            Left = 12
            Top = 83
            Width = 155
            Height = 25
            Hint = 'Insert new record'
            Caption = 'Create'
            HideHint = True
            AutoSize = False
            OnClick = AddLocationBtnClick
            NumGlyphs = 2
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
              CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
              E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
              7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
              82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
              9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
              55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
              5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
              CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
              0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
              76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
              FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
              16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
              8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
              FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
              38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
              9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
              FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
              51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
              9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
              66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
              9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
              76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
              9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
              FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
              59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
              9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
              FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
              0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
              87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
              FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
              55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
              556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
              B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
              88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
              E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
              78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
          object DelLocationBtn: TevSpeedButton
            Left = 638
            Top = 83
            Width = 155
            Height = 25
            Hint = 'Delete current record'
            Caption = 'Delete'
            HideHint = True
            AutoSize = False
            OnClick = DelLocationBtnClick
            NumGlyphs = 2
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
              CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
              F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
              6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
              A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
              CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
              A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
              2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
              C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
              9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
              5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
              F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
              9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
              576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
              FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
              97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
              4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
              F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
              8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
              3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
              F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
              334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
              F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
              2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
              EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
              3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
              EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
              84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
              8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
              F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
              8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
              9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
              F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
              8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
              2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
              C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
              807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
              C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
              EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
              CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
              6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
          object evDBEdit1: TevDBEdit
            Left = 12
            Top = 50
            Width = 155
            Height = 21
            HelpContext = 18002
            DataField = 'ACCOUNT_NUMBER'
            DataSource = dsLocations
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit27: TevDBEdit
            Left = 175
            Top = 50
            Width = 155
            Height = 21
            HelpContext = 18002
            DataField = 'ADDRESS1'
            DataSource = dsLocations
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit28: TevDBEdit
            Left = 338
            Top = 50
            Width = 155
            Height = 21
            HelpContext = 18002
            DataField = 'ADDRESS2'
            DataSource = dsLocations
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit29: TevDBEdit
            Left = 501
            Top = 50
            Width = 129
            Height = 21
            HelpContext = 18002
            DataField = 'CITY'
            DataSource = dsLocations
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit30: TevDBEdit
            Left = 638
            Top = 50
            Width = 51
            Height = 21
            HelpContext = 18002
            DataField = 'STATE'
            DataSource = dsLocations
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '&&'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit31: TevDBEdit
            Left = 698
            Top = 50
            Width = 95
            Height = 21
            HelpContext = 18002
            DataField = 'ZIP_CODE'
            DataSource = dsLocations
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 65530
  end
  inherited wwdsDetail: TevDataSource
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    OnUpdateData = wwdsDetailUpdateData
    OnDataChangeBeforeChildren = wwdsDetailDataChangeBeforeChildren
    Top = 65530
  end
  inherited wwdsList: TevDataSource
    Top = 65530
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 467
    Top = 82
  end
  inherited PageControlImages: TevImageList
    Left = 296
    Top = 88
    Bitmap = {
      494C01012A002C00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000B0000000010020000000000000B0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1D1D100CCCCCC00CCCC
      CC00D1D1D1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D1D1D100125AB600004EB3000051
      B300125EB700D1D1D10000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E2E2E200165BB6001AA6E40012DCFC000092
      F9000379DF001660B600E1E1E100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6E1D900DAAB9A00DDB1A100FBF2
      EF00FBF8F700FDF9F800FCF5F200F4DFD700D9AE9E00DAAB9A00DDB1A100F9EE
      EA00FBF8F700FBF8F700FBF2EF00F0D1C500000000000000000000000000D5D5
      D500CCCCCC00CCCCCC00D8D8D8005A86C3002397DF0037DFFF000EDCFF000092
      FC000B94FE000B73D9005686BF00FDFDFD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DDB1A1009B2F0A00822C02008E3F
      2400A8522C00A8522C00C5461800C8400D00BD330000A62C00008C2C02008A30
      1100A8522C00A8522C00C5461800D5653C000000000000000000D1D1D100BE7B
      6500BC6A4F00C56C4C00B6817400005FC70054C8F100055B960000336F00001E
      6D00003A90002186E800045DBE00D7DBDF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DDB1A1009E2C00007E1F00007E1F
      00008F23000092210C00A5290F005A13680035438F0076234A0069172C006E1A
      100089230000A0290000B32D0000D5653C0000000000E1E1E100BD745C00EBAE
      8400FFD89800D69668004E6692003BA3EB0003438000004F8B00025F9400003D
      920000338700052A7B002782E0005686BF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DDB1A100A12B0000802100008024
      00004347490035438F00473F6D003F5F6100407364003F5F610035438F004347
      490086260000A0290000B32D0000D5653C00FDFDFD00C4958500E5A48500FFDC
      AD00FFD89800DC9866000050B10077D2FF003DD1FF0027D2FF000ED7FC00008F
      F900008BF9000082F800479EF4000756B4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DDB1A100A32B000084220000661F
      0000662914006D463100435F2C00248D72000CBA99003A663F0076331500461C
      0900672408009D290000AD270000D8704900E0DCDB00C77A5E00895D3E009661
      3A00A66F3E00894B21000048BD0094E8FF003ACAFE0025CBFA000DCFF5000089
      F2000085F300007FF30048A1FC000051B2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DDB1A100A32B000084220000661F
      00002A151A00541D06005C6E680045BDC00043A9B5005FBAB700461C09001405
      1400661F0000A0290000AD270000D8704900C3948300E7AD9500F0C19F009D68
      410081491F00593521000049BD00A0EDFF003ACEFF0027CFFD000ED0F6000089
      F3000088F6000081F70044A1FC000051B2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DDB1A100A32B0000802100008021
      0000461C09001405140075917F0099E5C400E1FFDB008DA08400392415001A10
      0C00842200009D290000AD270000D8704900BB6B4F00FCD4C000FFCDA700FFD0
      9F00FFCE8F00D9915E000046BB00ADF2FF0000357900003676000DD3F800008A
      F500001B6A00001E740050A8FF000051B3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DDB1A100A12B0000802100007E1F
      00003515050073260000903211004284780032858D009032110091260000461C
      0900802100009D290000AD270000D8704900B9664900FFE5D400FFC9A400FFCA
      9A00FFD09100DA9361000044BA00B6F6FF0035CFFF001FCFFE0005CFF6000087
      F3000085F700007EF80059ACFF000050B3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D9AE9E009E2C0000821E00007E1F
      000092290200A32B0000AA2A0000381F3000461C0900A32B0000912600007E1F
      000089230000A0290000B32D0000D5653C00B9654900FFEADA00FFCDA700FFCF
      9E00FFD09200D89362000047C0009ED5FB0090D5F900AFF9FF007EF2FF0067BE
      FC00A0D7FF0070B4F1009CC7F3000051B4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DDB1A1009B270000781E0000781E
      000096270000A0290000AA2A0000BB3B0C00C1350000A0290000821E00007219
      000089230000A0290000B32D0000D3592D00B9664900FFF0E2008B563400703C
      1900FFD49600D4946600153A700016519E00004AAB000046B8000049B500004C
      B200004EB2000B56B6003072C2006195D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F5E6E000DDB1A100CD9C8B00A262
      4D00A8522C00A8522C00D5653C00DA876900DDB1A100DDB1A100DAAB9A00B981
      6E00A8522C00A8522C00DA7C5900EDC8BA00BA664900FFF4E800FFC8A0008D57
      3100FFD39300CD9167007F431E00D47F5200EAAA8A00D86E3F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BB694C00F6DBD000FADCCA00FFF1
      DD00FFF1CC00E2C2A900E7D1C300E2B8A400EAC9BD00C66D4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6A59300C8856E00BD6D5200BA66
      4A00BA674B00BC684C00BC694D00C0705500CA876F00D8A59300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CECECE00CECECE00CECECE00F7F7F7000000000000000000000000000000
      00000000000000000000DCDCDC00CCCCCC00CCCCCC00DDDDDD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CCCCCC00CCCCCC00CCCCCC000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6D6D600CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE004A84AD004A84AD004A84AD00B5BDC6000000000000000000000000000000
      000000000000000000006E99C2004182BD003D80BB006290BB00DEDEDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003A81AC003980AB003A81AC000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFBD7B00EFB57300EFB57300EFB5
      7300EFB57300EFB57300EFBD7300F7BD7300F7BD7300F7BD7300FFC67300317B
      B500739CBD0021ADFF0094CEEF004294C60000000000F4F4F400F5F5F500F5F5
      F500F5F5F500F8F8F8004D8DC30057A4D60059A0D3004C8FC5006490BB00DFDF
      DF00F5F5F500000000000000000000000000CFCFCF00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CFCFCF00387FAB006BB2D4003880AB000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFB57300FFEFCE00FFEFC600FFEF
      C600FFEFC600FFF7CE00C6B5A5007B7B7B0073737B0073737B0073737B008C8C
      8400B5A59C009CC6D600ADEFFF004294C600D0D0D000AFC1CE00B1C2CE00B1C2
      CD00B3C3CE00BAC6CD005291C700B7E6FA00529FD300579FD2004B8EC5006690
      BA00B3C4CF00D1D1D1000000000000000000A0A0A0009A9A9A00979797009797
      9700979797009797970097979700979797009797970097979700979797009797
      970097979700979797009A9A9A00A0A0A000387FAA0069B0D300377FAB000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFB57300FFEFCE00FFF7E700FFF7
      E700FFEFBD008C8C840094949C00D6D6DE00E7E7E700E7E7E700D6D6DE009C9C
      9C008C848400F7EFE700BDDEEF004294CE004F9ED3004398D2004094D0003E92
      CF003E92CE003E92CF00498DC5004D99CE00B4E3F900509ED300539DD3003080
      C2003D9BDA004BA0DA00D6D6D600F8F8F8009A9A9A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009A9A9A00387FAA0068B0D400337FB0000000
      000000000000000000000000000000000000F0F0F000D1D1D100CCCCCC00CDCD
      CD00E5E5E500000000000000000000000000EFB57300FFEFCE00FFF7DE00FFF7
      E700C6AD9C009C9C9C00E7E7E700EFC69400F7CE8400F7DE8C00F7E7AD00E7E7
      E7009C9C9C00B5ADAD003994CE00000000004499D2003F94D000ABFBFF009BF3
      FF0092F1FF0093F1FF0093F3FF0066AED7004B96CD00AFE1F90081AFD000BCBF
      B9008E8178008E827A009B958F00C5C4C30098989900FBFBFC00F1F5F700F1F5
      F900F1F5F900F1F5F900F1F5F900F1F5F900F1F5F900F1F5F900F1F5F900F1F5
      F900F1F5F900F1F5F700FBFBFC0098989900377FAB0065B0D7002980B900E3E3
      E300FAFAFA00000000000000000000000000C5B4A100B3804400AC753300AC77
      3700BB9F7D00D1D1D1000000000000000000EFB57300FFEFD600FFDEB500FFE7
      B50084848400E7E7E700E7BD8C00EFCE8C00F7D68C00FFE79C00FFEFA500F7E7
      AD00E7E7E7008C847B0000000000000000004397D10056ACDD008EDAF500A2ED
      FF0082E5FE0084E5FE0085E7FF0088EBFF0061A9D60057A7D700E0DCD800D4CD
      CC00D5D2D100EEEBE900EBE7E600928F8B00999A9A00F4F6F700AA7A5300AA7C
      5700A97C5700A97C5700A97C5700A97C5700A87B5600A87B5500A87B5500A97B
      5600AA7C5600AA7A5300F4F6F700999A9A00367FAC0066ADD000B5742A00C09E
      7700C9C3BC00CCCCCC00CCCCCC00CCCCCC00AE773500AC753300DD995800FEB1
      6F00DC9A5600AE7C3F00E1E1E10000000000EFB57300FFEFD600FFDEAD00FFE7
      B5007B7B7B00F7F7FF00E7B57300F7DEB500F7DEA500F7DE9C00FFE79C00F7DE
      8C00F7F7FF008C84840000000000000000004296D10071C4EA006CBCE600BBF2
      FF0075DEFD0077DEFC0078DFFD007DE1FE007EE4FF0078E6FF00C1B4AF00DCD8
      D800CFCDCC00918C890097908B009E948D009A9A9B00F0F3F500AC7E5900FFD6
      B600FFD6B700FFD7B800FFD7B800FFD5B600FFD2B100FDCEAD00FECEAC00FFD0
      AE00FFD4B200AC7E5800F0F3F5009A9A9B00367FAC005DB1DF00B8722300C584
      4000B67C3A00AA733100AA733100AA733000B87D3A00AA733000DC985800F8AC
      6A00FFB47100D7945200BA997300F9F9F900EFB57300FFEFD600FFEFDE00FFF7
      DE00847B7B00FFFFFF00E7B57300F7EFD600F7DEB500F7DEA500F7D68C00EFCE
      7B00FFFFFF008C84840000000000000000004095D00090DDF80044A0D800DDFC
      FF00DAFAFF00DBF9FF00DEFAFF0074DCFC0075DCFC0070DFFF00A1938E00D9D5
      D400C3BFBE009B95910070C8F9009EBDD2009A9A9B00EEF1F300AB7E5A00FFD5
      B600A77E5D00A87F5E00A87F5E00A57C5A00FBCDAC00FDFFFF00ECF0F300DEE1
      E300CFD3D700AD7D5600EEF1F3009A9A9B00367FAC005CB0DE00B56F2100E9B8
      8B00E69E5B00F7AB6900EDA56200EEA36000EDBE9200A7702C00DB965600F6AB
      6800FCB06C00EDA46100C0824000D7D1C900EFB57300FFEFDE00FFEFD600FFF7
      DE008C848400EFF7F700E7B58400FFEFE700F7EFD600F7DEB500EFCE9400EFCE
      9400EFF7F700948C8C0000000000000000003E93CF00B2F6FF0051ACDE00358A
      CA00358ACA00358ACA00368ACA005BBDE9006ED9FC0066DAFF00A3958F00E3DF
      DE00B1AFAC00A29D9900A69D9700ACA098009A9A9B00EEF1F400AB7E5A00FFD5
      B600FFD5B600FFD5B600FFD5B600FED2B300FACCAB00FDFFFF00ECF0F300DEE1
      E300CFD3D700AD7D5600EEF1F4009A9A9B00367FAC005CB0DD00B46D1E00EAC1
      9900E2995700F1A76400FFB37000F3A86400EDC49E00A56D2800DA935100F6A7
      6200FBAC6500ECA26000D9965500B7926600EFB57300FFF7E700FFD6A500FFD6
      A500C6BDB500ADADAD0000000000E7B58400E7BD8400E7BD8400EFC694000000
      0000ADADB500C6A5840000000000000000003D92CF00B8F3FF0077DFFE007BE0
      FE007CE1FE007CE1FF007DE2FF0052ABDD0055BAE900D9FAFF00BCC7C900D4CC
      C800F3F0EE00E3E0DD00D1CAC4009FA4A8009A9A9B00F1F4F600AC7E5900FFD7
      B800A97F5E00FFD7B800A87F5D00FFD4B500FED1B000FDCEAD00FECEAC00FFD0
      AE00FFD4B200AC7E5800F1F4F6009A9A9B00367FAC005CB0DD00B36D1D00EECA
      A800E1985500EFA66300FEB26E00F1A66100EFCDAC00A56B2500E7B68800FFE2
      C800FFF5E800FFF1E400FFF2E800AB722E00EFB56B00FFF7E700FFD69C00FFD6
      9C00FFF7DE00A59C9C00B5B5B500FFFFFF000000000000000000FFFFFF00B5B5
      B500A5A5A500F7BD730000000000000000003C92CF00C0F3FF0070D9FB0073DA
      FB0074DAFB0074DAFB0074DBFB0076DEFD004FA9DD00358CCC00388ECC00899D
      AD00B1A69F00B5AAA20098A6B00091BDDB00999A9A00F8F9FC00AC7B5400AC7E
      5900AC7F5A00AC7F5A00AC7F5A00AA7D5800A97C5700A97B5600A97B5600AA7C
      5700AB7C5700AB7B5400F8F9FC00999A9A00367FAC005CB0DD00B36C1C00F2D4
      B900E0975300EFA56200FEB16D00F0A56000F2D6BC00A56A2300F5E0CD00CAA2
      7500BD813F00B97A3400BF854300AD743100EFB56B00FFF7EF00FFEFCE00FFEF
      D600FFCE9C00FFCE9C00CEBDB500949494008C8C8C008C8C8C0094949400CEBD
      AD00FFFFEF00EFB5730000000000000000003B92CF00CAF6FF0069D5F9006CD5
      F9006AD4F90069D4F90069D5F9006AD6FA006BD8FB006BD9FD0069DAFE0063D9
      FF00D2FBFF003392D300D9E7F000000000009A9A9B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009A9A9B00367FAC005CB0DD00B36B1A00F5DE
      C800E0944E00EFA35E00FDAF6A00F0A35D00F6E0CB00A86D2600B17A3B00D0B0
      8A00EADAC800F5EFE60000000000EBDDCC00EFB56B00FFFFF700FFE7CE00FFE7
      CE00FFCE9400FFCE9400FFEFCE00FFEFCE00FFCE9400FFCE9400FFEFCE00FFEF
      CE00FFFFF700EFB56B0000000000000000003B92CF00D5F7FF0060D1F90061D0
      F800B4EBFD00D9F6FF00DAF8FF00DAF8FF00DAF9FF00DBF9FF00DAF9FF00D9FA
      FF00DDFDFF003C94D100D8EAF60000000000BCBCBC009A9A9A0098999A009899
      9A0098999A0098999A0098999A0098999A0098999A0098999A0098999A009899
      9A0098999A0098999A009A9A9A00BCBCBC00367FAC005CB0DD00B46B1A00FFF8
      F100F0C7A100F5BB8800FEBC8000F5B27700FEF4EC00A96F2900000000000000
      000000000000000000000000000000000000EFB57300FFFFFF00FFFFF700FFFF
      F700FFFFFF00FFFFFF00FFFFF700FFFFF700FFFFFF00FFFFFF00FFFFF700FFFF
      F700FFFFFF00EFB5730000000000000000003D94D000DCFCFF00D8F7FF00D8F7
      FF00DBFAFF00358ECD003991CE003A92CF003A92CF003A92CF003A92CF003A92
      CF003D94D00051A1D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003780AD0060B2DD00BB6F1C00CE9D
      6800E6C4A100FFE9D400FFF1DF00FFEEDD00EBCDAF00AB722D00000000000000
      000000000000000000000000000000000000EFBD8400EFB57300EFB56B00EFB5
      6B00EFB56B00EFB56B00EFB56B00EFB56B00EFB56B00EFB56B00EFB56B00EFB5
      6B00EFB57300EFBD840000000000000000004F9FD5003D94D0003A92CF003A92
      CF003D94D00055A2D600E0EEF800DBEBF700DAEBF600DAEBF600DAEBF600DAEB
      F600D7E9F6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004589B2003380B2005F8FA600D6B2
      8900B6824500A96F2900A86E2800A96F2900AB722D00E2CDB400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECECE00CECECE00DEDE
      DE00000000000000000000000000000000005A6B7B004A6B84004A94DE00528C
      D60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000EFEFEF00109452000000
      000000000000000000000000000000000000000000000000000000000000F7F7
      F700CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00F7F7F70000000000000000000000000000000000F7F7F700CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00008C520000C68400429C
      7300DEDEDE000000000000000000000000005A849C0084A5B50094D6FF005ABD
      FF00396B9C0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000CECECE00F7F7F700CECE
      CE00CECECE00CECECE00CECECE00CECECE00000000008CB5A50000945A00CECE
      CE00CECECE00CECECE00FFFFFF0000000000000000000000000000000000C6AD
      9400AD521000A5520800A5520800A5520800A5520800A5520800A5520800AD52
      1000C6AD940000000000000000000000000000000000C6AD9400AD521000AD52
      0800AD520800B5520800BD520800BD520800CE4A0000008C4A0000E7A50000BD
      840042A57300DEDEDE0000000000000000004AADEF009CE7FF009CDEFF00219C
      FF00109CFF00396B9C0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEFEF00008C4A00ADD6C600009C
      6B0000A56B0000A56B0000AD6B00009C5A00EFEFEF0000A57B0000B58C0000A5
      6B0000A56B00009C5A00CED6CE0000000000000000000000000000000000B55A
      1000D6945A00E7A56B00E7A56B00E7A56B00E7A56B00E7A56B00E7A56B00DE94
      5A00B55A100000000000000000000000000000000000B55A1000D6945A00E7A5
      6B00EFA56B00429C5A00008C4A00008C4A00008C4A000084420000DEA50000DE
      A50000BD840042A57300DEDEDE00000000004AB5EF008CE7FF0042BDFF0029A5
      FF00189CFF00189CFF00396B9C0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084B59C0000AD8400B5D6C60000AD
      8C0000C6A50000CEA50000DEA50000B573009CCEB50000BDA50000C69C0000CE
      9C0000D6A50000E7AD0039A5730000000000000000000000000000000000C673
      3100EFBD8C00DE9C5A00DE9C5A00DE9C5200DE9C5200DE9C5A00E79C5A00EFBD
      8C00BD6B210000000000000000000000000000000000C6733100EFBD8C00E79C
      5A00F79C5A000084420039EFC60000DEA50000DEA50000D69C0000D69C0000D6
      9C0000D69C0000BD840042A5730000000000FFFFFF00317BC6004ACEFF0039BD
      FF0029ADFF00189CFF00189CFF00396B9C0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00009C630000C6A5008CBDA5006BBD
      9C0000A5730000A56B0000AD7300009C5A0000000000009C630000B58C0000A5
      6B0000A56B0000D6940000AD6B0000000000000000000000000000000000CE84
      4A00EFCEAD00EFAD6B00EFAD6B00F7FFFF00EFF7F700EFB56B00EFAD6B00EFCE
      AD00C673290000000000000000000000000000000000CE844A00EFCEAD00EFAD
      6B00FFB57B00008439006BEFCE0000CE9C0000CE9C0000CE9C0000C6940000CE
      9C0000CE9C0063E7CE00008C420000000000FFFFFF00FFFFFF00397BC60052CE
      FF0039BDFF0029ADFF00189CFF00189CFF00396B9C0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00009C6B0000C6A50000AD7300D6D6
      D600F7F7F700CECECE00000000000000000000000000E7F7EF0000945A000000
      0000B5C6BD007BAD9400008C4A0000000000000000000000000000000000D694
      5A00F7DECE00F7BD7B00FFF7DE00FFFFFF00F7F7F700EFDECE00FFC67B00F7E7
      CE00CE7B310000000000000000000000000000000000D6945A00F7DECE00FFC6
      7B00FFFFEF000084390094EFE7004AEFD6004AEFD6004AE7D60094EFDE0000C6
      940063E7C60000B5840052B5840000000000FFFFFF00FFFFFF00FFFFFF00397B
      C60052CEFF0039BDFF0029ADFF00189CFF00189CFF00396B9C0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00009C6B0000BD9C0000CEA500109C
      63009CBDAD00008442000000000000000000000000000000000042AD7B00E7E7
      E700009C630000C68C000094520000000000000000000000000000000000D694
      4A00EFC69C00FFEFD600B5DEEF005AA5C600397BA5007B9CB500FFF7DE00EFC6
      A500D694520000000000000000000000000000000000D6944A00EFC69C00FFEF
      D600BDDEF700189473000084390000843900008439000084390084E7D6005ADE
      C60000AD7B005AB584000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00397BC60052CEFF0039BDFF0029ADFF00189CFF00189CFF00396B9C000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0063BD940000BD940000C69C0000CE
      940000D69400A5D6BD0000000000000000000000000000000000000000006BAD
      8C0000BD9C0000D6A50000B57300000000000000000000000000000000000000
      0000EFBD8400F7AD6300BDC6BD00639CBD00427B9C0094948C00F7AD6B00EFBD
      8400000000000000000000000000000000000000000000000000EFBD8400F7AD
      6300C6C6BD00639CBD004A7BA500A5949400FFB56B00008442007BE7DE0000AD
      7B005AB58400000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00397BC60052CEFF0039BDFF0029ADFF00189CFF00109CFF00316B
      A50000000000FFFFFF00FFFFFF00FFFFFF00CECECE0000945A0000CEA50000D6
      A50000AD6B000000000000000000000000000000000000000000000000000094
      5A0000C6AD0000D69C0029A56B00000000000000000000000000000000000000
      000000000000DEDEDE007BADC6007BBDDE006BADCE006394AD00E7E7E7000000
      000000000000000000000000000000000000000000000000000000000000DEDE
      DE007BADC6007BBDDE006BADD6006394AD00E7E7E700008C420000AD840052B5
      840000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00397BC60052CEFF0039BDFF0029ADFF00109CFF007BAD
      D600847B730000000000FFFFFF00FFFFFF00008C4A00009C630000A56B0000AD
      7300B5CEBD00CECECE00E7E7E700F7F7F700CECECE00CECECE00CECECE00CECE
      CE00089C630000AD730000000000000000000000000000000000000000000000
      0000000000005A7394009CE7FF0084C6EF0073B5DE006BADD6006B84A5000000
      0000000000000000000000000000000000000000000000000000000000005A73
      94009CE7FF0084C6EF0073B5DE006BADD6007384A50000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00397BC6004ACEFF0031BDFF0094BDDE00948C
      8400BDB5AD007B7B6B0000000000FFFFFF000000000000000000EFEFEF00CECE
      CE0052A57B00009C730063AD8C00D6EFE700008C420000B58C0000BD8400009C
      5A00E7E7E700CEEFDE0000000000000000000000000000000000000000000000
      00000000000000316300ADF7FF0094DEFF0084CEE7007BC6E70000295A000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300ADF7FF0094DEFF0084CEE7007BC6E70008215A0000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00317BCE00BDE7F70094847B00D6D6
      CE008C8C8400B573B5009C6BCE00FFFFFF000000000000000000ADDEC600009C
      6B0000C69C0000D6A500009C6B00EFEFEF00E7E7E70000945A0000CE9C0000D6
      94006BAD94000000000000000000000000000000000000000000000000000000
      00000000000008396B0021639400528CB5007BB5D600639CBD0008295A000000
      0000000000000000000000000000000000000000000000000000000000000839
      6B0021639400528CB5007BB5D600639CBD0008295A0000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0094847B00F7F7EF00848C
      8400CE94CE00BD7BB500AD7BCE00FFFFFF0000000000000000000000000000A5
      730000C6A50000CE9C0000E7A5007BB594006BAD940000BD8C0000CE9C0000CE
      940000AD7300D6D6D60000000000000000000000000000000000000000000000
      00000000000010427300317BAD004A8CBD00527BAD00294A7300103163000000
      0000000000000000000000000000000000000000000000000000000000001042
      7300317BAD004A8CBD00527BAD00294A73001031630000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084847B00EFBD
      EF00D6A5D600BD8CD600FFFFFF00FFFFFF00000000000000000000000000C6E7
      D60000AD7B0000BD840021A56B00009C730000AD8C0000CEA50000C68C007BC6
      A50039A5730010945A0000000000000000000000000000000000000000000000
      000000000000395A84002173AD00398CBD00397BAD00214A7300395A84000000
      000000000000000000000000000000000000000000000000000000000000395A
      84002173AD00398CBD00397BAD00214A7300395A840000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C68C
      DE00BD8CD600FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000D6EFDE0073C69C004AAD7B000094520000A5730000A56B00089C63000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DEE7EF0021528400185A8C001852840021527B00DEE7EF000000
      000000000000000000000000000000000000000000000000000000000000DEE7
      EF0021528400185A8C001852840021527B00DEE7EF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F7FFFF00C6C6
      C600C6C6C600C6C6C600F7FFFF000000000029527300294A7300294A73002952
      730029527300295273002952730029527300295273002952730029527300294A
      7300294A7300294A730029527300FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700CECE
      CE00CECECE00CECECE00E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7FFFF0042A584000084
      4200008442000084420042A58400F7FFFF006B849C00738CA50052738C004263
      840042638400426384004263840042638400426384004263840042638400526B
      8C00738CA500738CA5006B849C00FFFFFF00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E7006B73BD002131
      AD002131AD002131AD006B73BD00E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000042A5840000A5630000C6
      840084E7C60000C6840000A5630042A584007B94A500315A7B007B94AD005273
      8C004263840042638C0042638C0042638C00426B8C00426B8C00426384004263
      840052738C00295273007B94A500FFFFFF00AD948400A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00AD9484000000000000000000000000000000
      00000000000000000000000000000000000000000000636BBD002939C6003963
      FF003963FF003963FF002939C6006373BD00C6DEC600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000084420000C6840000C6
      84000000000000C6840000C68400008442002952730042637B0042638400426B
      8C004A6B8C00527B9C0073A5B500ADF7DE005A7B9C004A6B94004A6B94004A6B
      8C00426384004263840029527300FFFFFF00A58C7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A58C7B00DEDEDE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE001821B5004263FF003963
      FF00395AFF003963FF004263FF001831AD0084C6A50084C6840084C6840084C6
      840084C6840084C6840084C6840084C6A50084C6A5000084420084E7C6000000
      0000000000000000000084E7C60000844200294A73004A6B8C004A6B8C004A6B
      94004A739400ADEFDE009CD6D60094D6CE00ADEFDE0094CECE004A7394004A73
      94004A6B8C0052738C00294A7300FFFFFF00A58C7B00000000009C9C9C00ADB5
      B5009CA5A5008C8C9400A5A5A500A5A5A500A5A5A5009CA5A5009C9CA500A5AD
      AD00A5A5A5009C9C9C0000000000A58C7B008CBDA5006BB58C006BB58C006BB5
      8C006BB58C006BB58C006BB58C006BB58C0073BD8C001018AD00ADBDFF000000
      00000000000000000000ADBDFF001829AD0084A58400C6DEC60084C6C60084C6
      A50084C6C60084C6C60084C6C60084C6C600C6C6C6000084420000C6A50000C6
      A5000000000000C6A50000C6A50000844200294A6B006B8CA5004A6B94005273
      9C0052739C005A7B9C007B9CB5008CBDC600ADEFDE00ADE7DE0052739C005273
      9C004A7394007394AD00294A6B00FFFFFF00A58C7B0000000000000000000000
      00000000000000000000E7E7E70000000000E7E7E7000000000000000000FFFF
      FF00000000000000000000000000A58C7B006BAD8C00CEF7DE008CC6B50084BD
      A50094CEBD0094CEBD0094CEBD0094CEBD009CD6BD001018AD005A73FF005A73
      FF005273FF005A73FF005A7BFF001829AD0084A58400C6DEC60042A5840042A5
      6300C6DEC600C6DEC60084E7C60084C6A50084C6A50042A5630000A5630000E7
      A50084E7E70000E7A50000A5630042A5630029527300849CB5006384A5005A7B
      9C005A7BA500B5EFDE00B5EFE7009CCECE0084ADBD006B8CAD00637BA5005A7B
      9C006384A5007394AD004A6B8C00FFFFFF00A58C7B0000000000BD9C9400DECE
      CE00BD9C9C00DECECE00BD9C9C00DECECE00BD9C9C00DECECE00D6BDBD00B594
      8C00B5948C00D6BDBD0000000000A58C7B0063AD8400CEF7DE005AA57B004A94
      6B00BDEFCE00B5EFCE009CD6B5008CC6A5008CCEA5004263A5002939CE006B84
      FF00738CFF006B84FF003142CE00426B9C0084A58400C6DEC60084C6A500C6DE
      C600C6DEC600C6DEC60084A58400C6DEC600C6DEC60084C6A50042C684000084
      4200008442000084420042C6840084A58400E7EFEF0031527B00A5B5C6006B8C
      AD006384A500A5D6D600BDEFE700ADDEDE00B5E7DE009CCED6006384A500738C
      AD00A5B5CE00315A7B00E7EFEF00FFFFFF00A58C7B0000000000B5949400FFFF
      FF00BD949400F7FFF700BD949400F7FFF700BD949400F7F7EF00BD949400FFFF
      FF00FFFFFF00B594940000000000A58C7B0063A58400CEF7E70073B59400B5E7
      CE00B5E7CE00B5E7CE006BAD8C00B5E7C600B5E7C60073BD8C005A7BBD001018
      AD001018AD000818AD006B84C6006BB57B0042A58400F7FFFF0084A58400C6DE
      C60042A56300C6DEC60084A58400C6DEC600C6DEC60084A58400C6DEC60084A5
      8400C6DEC60084C6A500F7FFFF0084A58400FFFFFF00B5C6CE0031527B00ADBD
      CE007B94B500738CAD008CADC600B5DEDE009CB5CE00738CB5007B94B500ADBD
      D60031527B00B5C6CE00FFFFFF00FFFFFF00A58C7B0000000000BD949400F7FF
      FF00BD949400F7F7F700BD949400F7F7F700BD949400EFEFEF00BD949400F7FF
      FF00F7FFFF00B5948C00FFFFFF00A58C7B005AA57B00D6F7E7006BAD8C00ADE7
      CE0052946B00ADE7CE006BAD8C00B5E7C600B5E7C60073B58C00B5EFCE005AA5
      6B00B5F7C60073BD8C00D6FFE70063A57B0042A58400F7FFFF0084A58400C6DE
      C600C6DEC600C6DEC60084A58400C6DEC600C6DEC60084A58400C6DEC600C6DE
      C600C6DEC60084A58400F7FFFF0042A58400FFFFFF00FFFFFF00B5C6CE00315A
      7B00ADBDD6007B94B5008494BD007BADCE007384A50094D6EF00B5C6D600395A
      7B00B5C6CE00FFFFFF00FFFFFF00FFFFFF00A58C7B0000000000BD949400D6C6
      C600BD9C9400D6C6C600BD9C9400D6C6C600BD9C9400D6C6C600CEBDB500B594
      8C00B5948C00CEB5B500FFFFFF00A58C7B005AA57B00DEF7EF0063AD8400A5E7
      C600A5DEC600A5DEC60063A57B00B5E7C600B5E7C60063A57B00A5E7C600A5E7
      C600ADE7C6006BAD8400DEF7EF005AA57B0042A58400F7FFFF0042A5840042A5
      840084E7C60084E7C60084C6A50084C6A50084C6840084C6A50084E7C60084E7
      C60042A5840042A58400F7FFFF0042A58400FFFFFF00FFFFFF00FFFFFF00A5B5
      C6005A7B9C00C6CEDE00BDC6D600A5EFFF0084ADC600ADDEF7005A739400A5B5
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00A58C7B00FFFFFF00E7E7E700DED6
      D600E7E7E700DED6D600E7E7E700DED6D600E7E7E700DED6D600EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00FFFFFF00A58C7B005A9C7300DEF7EF00529C73005AA5
      7B009CDEC6009CDEC60084BDA5007BBD94007BB58C0084BDA5009CDEC6009CDE
      C6005AA57B00529C7300DEF7EF005A9C730042A58400F7FFFF00428463004284
      630042A5840042A5840042A5840042A5840042A5840042A5840042A5840042A5
      84004284630042846300F7FFFF0042A58400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00395A7B0042638400394A73007BB5CE009CDEF7005A84A5006B849C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A58C7B0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000A58C7B00529C7300E7FFF700428C5A00428C
      63005A9C73005A9C73005A9C73005A9C73005A9C73005A9C73005A9C73005A9C
      7300428C6300428C5A00E7FFF700529C730042A56300F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00F7FFFF0042A56300FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0073849C009CDEEF00A5E7F700A5EFFF00ADC6CE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6B5AD00A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00C6B5AD0052946B00E7FFF700E7F7F700E7F7
      F700E7F7F700E7F7F700E7F7F700E7F7F700E7F7F700E7F7F700E7F7F700E7F7
      F700E7F7F700E7F7F700E7FFF70052946B0084C6A50084C6A500C6DEC600C6DE
      C600C6DEC600C6DEC600C6DEC600C6DEC600C6DEC600C6DEC600C6DEC600C6DE
      C600C6DEC600C6DEC60084C6A50084C6A500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0031527B005A7B9C008494B500637BA500637B8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008CBD9C008CC6A500B5E7CE00ADE7
      CE00ADE7CE00ADE7CE00ADE7CE00ADE7CE00ADE7CE00ADE7CE00ADE7CE00ADE7
      CE00ADE7CE00B5E7CE008CC6A5008CBD9C000000000084C6A50042A5630042A5
      630042A5630042A5630042A5630042A5630042A5630042A5630042A5630042A5
      630042A5630042A5630084C6A50000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CED6DE0094A5B5006B849C00395A840031527300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008CB59C0052946B004A94
      6B004A946B004A946B004A946B004A946B004A946B004A946B004A946B004A94
      6B004A946B0052946B008CB59C0000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00000000000000000000000000CECE
      CE00CECECE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECECE00CECE
      CE000000000000000000000000000000000000000000E7E7E700CECECE00CECE
      CE00CECECE00E7E7E70000000000000000003984AD003984AD003984AD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6AD9400AD521000A5520800AD52
      0800B54A0000298CEF00318CD6003984D6003984D6003984D6003984D6003984
      D6003984D6003984D6003984D600398CD600000000000000000000000000319C
      DE0039A5DE00D6D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000319CDE0039A5
      DE00D6D6D600000000000000000000000000E7E7E70052B58C00009C5A00009C
      5A00009C5A0052B58C00E7E7E70000000000397BAD006BB5D600397BAD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B55A1800DE9C5A00E7AD7300E7A5
      6B00EFA56300318CDE0042D6FF004ADEFF004ADEFF004ADEFF0042DEFF004ADE
      FF004ADEFF004ADEFF004AD6FF00398CDE0000000000000000000000000063B5
      E700319CDE0042A5DE00CECECE00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000063B5E700319C
      DE0042A5DE00CECECE00FFFFFF000000000052B58C0000A56B0000BD840073DE
      C60000BD840000A56B0052B58C0000000000397BAD006BB5D600317BAD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C67B3100EFC69400E7A56300E79C
      5A00EF9C5200B5947B003194E70052DEFF0052DEFF0052DEFF007342390052DE
      FF0052DEFF0052DEFF00399CE7007BA5B500000000000000000000000000B5DE
      F700319CDE0063C6EF00319CDE00BDC6CE00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000B5DEF700319C
      DE0063C6EF00319CDE00BDC6CE00FFFFFF00009C520000C68C0000BD84000000
      000000BD840000C68C00009C5A0000000000397BAD0063B5D600317BB500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F700CECECE00000000000000
      000000000000DEDEDE00F7F7F700FFFFFF00CE8C4A00F7D6BD00F7B57300F7BD
      7B00FFFFFF00FFF7EF005A8CC6004AC6F7005AE7FF005AE7FF0063C6DE005AE7
      FF005AE7FF0052C6FF00398CC600318463000000000000000000000000000000
      0000319CDE0084D6F7008CE7FF00319CDE00BDC6CE00EFEFEF00000000000000
      000000000000000000000000000000000000000000000000000000000000319C
      DE0084D6F7008CE7FF00319CDE00CECECE00009C4A0073E7CE00000000000000
      00000000000073E7CE00009C5A0000000000317BAD0063B5D600217BBD00DEDE
      DE00F7F7F700FFFFFF00FFFFFF00FFFFFF00D6BD9C00E7942900E78C2100E78C
      2100E78C2100DEA55A00D6BDAD00E7E7E700D68C4200F7DEC600FFE7CE00FFFF
      EF00FFFFFF00FFF7F700E7DED600398CDE0063E7FF0063EFFF006B4A42006BEF
      FF0063E7FF003184D6006BBDA50031846B000000000000000000000000000000
      00006BB5E70052BDEF00ADF7FF007BDEFF00319CDE0094BDD600EFEFEF000000
      0000000000000000000000000000000000000000000000000000000000006BB5
      E70052BDEF00ADF7FF007BDEFF00429CE700009C4A0000CE940000CE8C000000
      000000CE8C0000CE9C00009C5A0000000000317BAD0063ADD600EF8C1800DEAD
      6300D6BDA500000000000000000000000000E7942900E78C2100E78C2100FFDE
      8C00FFDE8400FFBD5200EF9C3100DEAD6B00FFFFF700DEA56300EFBD8400ADC6
      CE005AA5C600397B9C0084848400ADA5A5004AB5EF0073F7FF006B4A420073F7
      FF004AB5EF00319CA50094DEBD00318473000000000000000000CECECE00CECE
      CE00A5BDD60042ADE700BDF7FF0084E7FF0073DEF700319CDE009CBDD600E7E7
      E7000000000000000000000000000000000000000000CECECE00CECECE00A5BD
      D60042ADE700BDF7FF0084E7FF007BDEFF00109C940000AD630000D69C0073EF
      D60000D69C0000AD73006BC6A50000000000317BAD005AADE700F78C1000FFA5
      2900EF9C2900E78C2100E78C2100E78C2100E78C2100EF942900DE8C1800FFDE
      8400FFD67B00FFC65A00FFCE7B00E78C2100FFFFFF00FFFFFF00FFEFE70094AD
      AD0063A5C600528CAD0063736B00ADCE9C00529CDE0073E7FF006B4A420073EF
      FF00529CD6009CE7BD009CDEC600318C7B000000000000000000399CDE004AAD
      E70042ADE70039A5E700BDF7FF007BE7FF007BE7FF0073DEF70042A5E70084B5
      D600DEDEDE0000000000000000000000000000000000399CDE004AADE70042AD
      E70039A5E700BDF7FF007BE7FF007BE7FF0073DEFF0018A59400009C4A00009C
      5200009C5A006BC6A5000000000000000000317BAD0052ADE700EF8C1000FFCE
      7300FFBD4A00FFC66300FFCE6B00FFCE7300FFD67300FFD68C00DE841800FFDE
      8400FFD67B00FFC65200FFD68C00E78C2100FFFFFF00FFFFFF00C6CECE007BBD
      E7007BC6E70073ADD6005A94BD0042948C004AA59C00429CE70084DEDE00429C
      E700528494006BBDA50052A59400F7FFFF000000000000000000319CDE009CF7
      FF0084E7FF0073E7FF006BDEF7006BDEF7006BDEF70073E7FF0073DEFF0042A5
      E7006BADD600DEDEDE00000000000000000000000000319CDE009CF7FF0084E7
      FF0073E7FF006BDEF7006BDEF7006BDEFF0073E7FF0073DEFF0042A5EF007BAD
      DE00DEDEDE00000000000000000000000000317BAD0052ADDE00EF8C1000FFD6
      8400FFB54200FFC65A00FFCE7300FFDE8C00FFD67300FFDE9400DE841000FFDE
      8400FFD67B00FFC65200FFDEA500E78C1800FFFFFF00FFFFFF0010396B00A5EF
      FF008CD6F7007BBDDE0073B5D60029427300F7FFF7004A8CBD004AA5E700428C
      C600427B8400DEEFEF00FFFFFF00FFFFFF0000000000000000005AA5DE0084DE
      F7008CE7FF006BDEF7006BDEF700BDF7FF00BDF7FF00B5F7FF00ADF7FF00ADF7
      FF0042ADE7006BADDE000000000000000000000000005AA5DE0084DEF7008CE7
      FF006BDEF7006BDEF700BDF7FF00BDF7FF00B5F7FF00ADF7FF00ADF7FF0042AD
      E7006BADDE00000000000000000000000000317BAD0052ADDE00EF8C0800FFDE
      9C00FFB54200FFC65A00FFCE6B00FFDE8400FFCE6B00FFDEA500DE841000FFDE
      8400FFD67300FFBD5200FFE7B500E78C1800FFFFFF00FFFFFF0008396B006BAD
      CE007BB5D6008CCEEF0084BDE70000215200D6D6D60084C6E70073B5DE0073B5
      D6005A9CBD00C6CECE00FFFFFF00FFFFFF0000000000000000009CC6EF006BC6
      EF009CEFFF005AD6F70063D6F70042B5E7003194DE003194DE003194DE00399C
      DE00399CDE00429CDE000000000000000000000000009CC6EF006BC6EF009CEF
      FF005AD6F70063D6F70042B5E7003194DE003194DE003194DE00399CDE00399C
      DE00429CDE00000000000000000000000000317BAD0052ADDE00EF8C0800FFDE
      AD00FFB53900FFC65A00FFCE6B00FFDE8400FFCE6B00FFE7B500DE841000FFE7
      AD00FFD67300FFBD4A00FFEFC600E78C1800FFFFFF00FFFFFF00104273002973
      AD004A8CB500527BA500314A7B0008295A0029527B00ADF7FF0094D6F70084BD
      DE0073B5D60018396B00FFFFFF00FFFFFF000000000000000000DEEFFF004AAD
      E700B5EFFF0063DEF70052D6F700ADEFFF0063B5E7008CB5D600FFFFFF000000
      00000000000000000000000000000000000000000000DEEFFF004AADE700B5EF
      FF0063DEF70052D6F700ADEFFF0063B5E7008CB5D600FFFFFF00000000000000
      000000000000000000000000000000000000317BAD0052ADDE00EF8C0800FFE7
      BD00FFB53900FFBD5200FFCE6B00FFDE8400FFCE6B00FFEFC600DE841000FFEF
      DE00FFF7E700FFF7E700FFF7E700E78C1800FFFFFF00FFFFFF0010427300297B
      AD00428CBD00427BAD00214A7300294A730008396B006BADCE007BB5D6008CCE
      EF0084C6E70008295A00FFFFFF00FFFFFF00000000000000000000000000399C
      DE00ADEFFF0084DEF7004ACEF70084DEF700ADE7FF003994DE00D6D6D6000000
      0000000000000000000000000000000000000000000000000000399CDE00ADEF
      FF0084DEF7004ACEF70084DEF700ADE7FF003994DE00D6D6D600000000000000
      000000000000000000000000000000000000317BAD005AADDE00F78C0800FFFF
      EF00FFDEA500FFD69400FFD68400FFDE9400FFD68400FFFFE700E7841000E78C
      2100E7A54A00EFA55200FFE7CE00E78C1800FFFFFF00FFFFFF00DEE7EF00104A
      7B00186394001852840018427300FFFFFF0010427300297BAD004A8CB500527B
      A50031527B0010316300FFFFFF00FFFFFF000000000000000000000000005AA5
      DE008CDEF700ADEFFF0039CEF70042CEF700BDF7FF006BBDE7005AA5D600F7F7
      F7000000000000000000000000000000000000000000000000005AA5DE008CDE
      F700ADEFFF0039CEF70042CEF700BDF7FF006BBDE7005AA5D600F7F7F7000000
      000000000000000000000000000000000000317BAD005AB5DE00F78C0800F7AD
      5200FFCE8400FFE7BD00FFFFE700FFFFEF00FFFFE700FFDEAD00E78C1800FFFF
      FF00FFEFD600F7DEB500EFAD5A00E78C2100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F7F7F700FFFFFF00FFFFFF0010427300297BAD00428CBD00427B
      AD00214A730010316B00FFFFFF00FFFFFF000000000000000000000000009CC6
      E7006BC6EF00D6FFFF00CEF7FF00C6F7FF00D6FFFF00C6F7FF00429CDE00A5BD
      D6000000000000000000000000000000000000000000000000009CC6E7006BC6
      EF00D6FFFF00CEF7FF00C6F7FF00D6FFFF00C6F7FF00429CDE00A5BDD6000000
      00000000000000000000000000000000000073ADC600297BB5009CB5B500F7D6
      A500EFA54A00E78C1800E78C1800E78C1800E78C1800E78C2100F7C68C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7EF00104A7B00185A94001852
      840010427300DEE7EF00FFFFFF00FFFFFF00000000000000000000000000D6EF
      F700429CDE003994DE003994DE003994DE003994DE00429CDE00429CDE004A9C
      DE00000000000000000000000000000000000000000000000000D6EFF700429C
      DE003994DE003994DE003994DE003994DE00429CDE00429CDE004A9CDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700CECE
      CE00CECECE00CECECE00E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700CECE
      CE00CECECE00CECECE00E7E7E700000000000000000000000000CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE000000000000000000009CE700009CE7000094E7000094
      E7000094E700009CEF00009CEF00009CEF000094E7000094E7000094E7000094
      E7000094E7000094E700009CE700009CE70000000000CECECE00CECECE00EFEF
      EF00E7E7E700CECECE00CECECE00CECECE00CECECE00CECECE007384D600314A
      CE00394ACE00394ACE007384CE00E7E7E7000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDEDE0052B58C00009C
      5A00009C5A00009C5A0052B58C00E7E7E7000000000000000000947B63008C73
      52008C6B52008C6B52008C6B52008C6B52008C6B52008C6B52008C6B52008C6B
      52008C735200947B63000000000000000000089CE70039DEF70039DEF70031DE
      F70031E7F70039B5C6004A736B0031EFFF0031E7F70031DEF70031DEF70031DE
      F70031DEF70039DEF70039E7F700089CE7000000000042A5EF0042A5F7009CC6
      DE00C6849400AD425A00AD395A00AD395A00B53952006342A5003152DE00315A
      FF00315AFF00395AFF003952D6007384CE00DEDEDE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE004AAD840000A56B0000BD
      840073DEC60000BD840000A56B0052B58C0000000000000000008C7352000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C735200000000000000000084CEF70018B5EF0042DEF70031D6
      F70031D6F70039ADC60052211000426B6B0031DEFF0031D6F70031D6F70031D6
      F70031D6F70042DEF70018B5EF0084CEF7000000000042ADF70084EFFF00A54A
      6B00B5425A00BD525A00BD5A5A00BD5A5A00C65A5200214ADE003963FF003963
      FF00395AFF003963FF004263FF00314ACE00BD944A00B57B0800B57B0800B57B
      0800B57B0800B57B0800B57B0800BD7B0800CE7B0000009C5A0000C68C0000BD
      84000000000000BD840000C68C00009C5A0000000000000000008C6B52000000
      0000FFFFFF00FFFFFF0000000000AD732900AD73290000000000FFFFFF00FFFF
      FF00000000008C6B52000000000000000000FFFFFF000094E7004AD6F70039D6
      F70031D6F70031DEFF0031DEFF004A31210031B5CE0031D6FF0031D6F70031CE
      F70031D6F7004AD6F7000094E700FFFFFF0000000000B5DEEF00A54A6B00BD4A
      5200C65A6300D6737300E77B7B00E77B7B00EF8473002142D600A5BDFF000000
      00000000000000000000ADBDFF00314ACE00B57B0800F7FFFF00F7F7FF00F7F7
      FF00F7F7FF00F7F7FF00F7F7FF00F7F7FF00FFFFFF000094520073E7CE000000
      0000000000000000000073E7CE00009C5A0000000000000000008C6B52000000
      0000FFFFFF00FFFFFF00AD732900F7BD7300F7BD7300AD732900FFFFFF00FFFF
      FF00000000008C6B52000000000000000000FFFFFF0084CEF70018ADEF0052D6
      F70031C6EF0031A5C60031B5D60042636B004A31210031C6E70029CEFF0029CE
      F70052D6F70021ADEF0084CEF700FFFFFF0000000000C6849400BD4A5A00C66B
      6B00DE7B7B00E7CECE00EFFFFF009CADAD00F7FFFF001831CE005A73FF005273
      FF005273FF005273FF005A7BFF00314ACE00B57B0800F7FFFF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00000000000000000000944A0000CE940000CE
      8C000000000000CE8C0000CE9C00009C5A0000000000000000008C6B52000000
      0000F7FFFF00AD732900EFBD6B00EFBD7300EFBD7300EFBD6B00AD732900F7FF
      FF00000000008C6B52000000000000000000FFFFFF00FFFFFF00089CE70052CE
      F70042CEEF00423129004A2918004A2918004A2110004239310029BDEF0042CE
      F70052CEF700089CE700FFFFFF00FFFFFF0000000000B54A5A00C66B7B00DE7B
      7B00DECECE00DEFFFF00E7FFFF00E7FFFF00EFFFFF007B94E7003952DE006B84
      FF00738CFF006B8CFF00425ADE008494E700B57B0800F7FFFF00E7E7E7009C9C
      9C009C9C9C00EFEFE700C6BDBD0000000000000000006BCEA50000AD6B0000D6
      9C0073EFD60000D69C0000AD73006BC6A50000000000000000008C6B52000000
      0000AD732100F7C68400F7C68400EFC68400EFBD6B00F7CE8C00F7C68400AD73
      2100000000008C6B52000000000000000000FFFFFF00FFFFFF00ADDEF70018A5
      EF0073DEFF00398CAD0042211000396B7B0029D6FF0029CEFF0031C6F70073D6
      FF0018A5EF00ADDEF700FFFFFF00FFFFFF0000000000B5425A00D68C9400E784
      8400D6FFFF00DEFFFF00524A4200B5CED600DEFFFF00E7FFFF007394E7002139
      CE002142D600294AD6008C9CE70000000000B57B0800F7FFFF00E7E7E700E7E7
      E700E7E7E700E7E7E700A5A5A500A5A5A500A5A5A500B5A5AD00399C73000094
      4A0000944A00009C5200529439000000000000000000000000008C6B52000000
      0000C6A57300B5733100B57B3100EFCE9C00E7AD5A00B57B3900B57B3100C6A5
      7300000000008C6B52000000000000000000FFFFFF00FFFFFF00FFFFFF00089C
      E7005ACEF70063DEFF003939390042211000398CAD0031CEFF005ACEF70063CE
      F700089CE700FFFFFF00FFFFFF00FFFFFF0000000000B5425200DE9CA500EF84
      8400849C9C00D6FFFF00B5D6D6005A524A00ADC6CE00D6FFFF008C9C9C00FF84
      7B00EF9C9C00C64242000000000000000000B57B0800F7FFFF00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00E7DEDE00EFDEE700EFDE
      E700F7E7E70000000000CE7B08000000000000000000000000008C6B52000000
      0000EFEFF700EFF7FF00B5732900EFDEB500EFCEA500B57B3100EFF7FF00EFEF
      F700000000008C6B52000000000000000000FFFFFF00FFFFFF00FFFFFF00CEEF
      FF00089CEF009CE7FF004AA5CE0039180800392118004AADDE009CE7FF0010A5
      EF00CEEFFF00FFFFFF00FFFFFF00FFFFFF0000000000B5425200E7ADB500EF84
      8400C6FFFF00ADCED6005A525200ADCECE00CEF7FF00C6F7FF00C6FFFF00EF84
      8400EFADB500BD424A000000000000000000B57B0800F7FFFF00DED6DE00DEDE
      DE00DEDEDE00DED6D600EFEFEF0000000000000000000000000000000000F7EF
      EF00DED6DE00FFFFFF00BD7B08000000000000000000000000008C6B52000000
      0000E7E7E700E7E7EF00C6A57300B5732900B5732900C6A57300E7E7EF00E7E7
      E700000000008C6B52000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0029ADEF006BCEF70094E7FF0039394200310800004A31290063C6EF0029AD
      EF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DEDEDE00B54A5200E7A5AD00EF94
      9400D6CECE005A525200A5CECE00C6F7FF00BDF7FF00B5F7FF00CEC6C600EF94
      9400E7A5AD00B54A5200DEDEDE0000000000B57B0800F7FFFF00D6D6D600948C
      8C0094949400D6D6D600C6C6BD0000000000000000000000000000000000C6C6
      BD00D6D6D600F7FFFF00B57B08000000000000000000000000008C6B52000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C6B52000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CEEFFF0008A5E700B5E7FF0073D6FF0073D6FF00C6F7FF0010A5EF00CEEF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0073B5EF007B7BA500CE6B7300F7BD
      CE00F78C8C00CEC6CE00B5F7FF0063848C00ADF7FF00CEBDC600EF848400F7BD
      CE00CE6B73007B7BA50073B5EF0000000000B57B0800F7FFFF00CECECE00D6CE
      CE00D6CECE00D6CECE00A5A5A5009CA5A5009C9CA5009C9CA5009CA5A5009CA5
      A500CECECE00F7FFFF00B57B0800000000000000000000000000BD8C4A00C68C
      3900BD843900BD843900BD843900BD843900BD843900BD843900BD843900BD84
      3900C68C3900B5844A000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF005ABDEF005AC6F700BDE7FF00B5E7FF006BC6F7005ABDEF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004AADF70052C6FF00AD5A6300DE8C
      9400FFCEDE00F7A5A500F7848400F7848400F7848400F79CA500FFCEDE00DE8C
      9400AD5A630052C6FF004AADF70000000000B57B0800F7FFFF00F7F7FF00F7F7
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00B57B0800000000000000000000000000C6944200F7D6
      9C00EFBD6B00EFBD7300EFBD7300EFBD7300EFBD7300EFBD7300EFBD7300EFBD
      6B00F7D69C00C69442000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F7FFFF00009CE700CEEFFF00CEEFFF00009CE700F7FFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0052B5F70063D6FF006BD6FF00AD63
      6300D6737B00FFCED600FFE7FF00FFE7FF00FFE7FF00FFCED600D6737B00AD63
      63006BD6FF0063D6FF0052B5F70000000000B57B1000F7E7C600DEAD4A00DEAD
      4A00DEAD4A00DEAD4A00DEAD4A00DEAD4A00DEAD4A00DEAD4A00DEAD4A00DEAD
      4A00DEAD4A00F7E7C600B57B1000000000000000000000000000C6944A00E7C6
      9400D6A55200D6A55200D6A55200D6A55200D6A55200D6A55200D6A55200D6A5
      5200E7C69400C6944A000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0052BDEF0073C6F70073C6F70052BDEF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6E7FF005AC6F7007BF7FF007BFF
      FF00848CA500B5525A00B5525200B5525200B5525200B5525A00848CA5007BFF
      FF007BF7FF005AC6F700C6E7FF0000000000BD841000EFD6A500EFCE9C00EFCE
      9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE
      9C00EFCE9C00EFD6A500BD841000000000000000000000000000C6944A00DEBD
      8C00DEBD8C00DEBD8C00DEBD8C00DEBD8C00DEBD8C00DEBD8C00DEBD8C00DEBD
      8C00DEBD8C00C6944A000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00009CE700009CE700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6E7FF0052BDF7004AC6
      FF0084DEFF00000000000000000000000000000000000000000084DEFF004AC6
      FF0052BDF700C6E7FF000000000000000000CEAD6300BD841000B5841000B584
      1000B5841000B5841000B5841000B5841000B5841000B5841000B5841000B584
      1000B5841000BD841000CEAD6300000000000000000000000000CE9C5200C694
      4A00C6944A00C6944A00C6944A00C6944A00C6944A00C6944A00C6944A00C694
      4A00C6944A00CE9C520000000000000000000000000000000000000000000000
      00000000000000000000CECECE00CECECE00CECECE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDEDE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00D6D6D600000000000000000000000000000000000000
      000000000000000000005A6B7B004A6384005294DE00CECECE00000000000000
      0000000000000000000000000000000000002994730029947300299473002994
      6B0018946B00A5846B00A5846B00A5846B00A5846B00A5846B0018946B002994
      6B00299473002994730029947300FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B5B5B5009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C00ADADAD000000000000000000FFFFFF00EFEFEF00CECE
      CE00CECECE00CECECE005A84A50084A5B50094D6FF00316B9C00CECECE00CECE
      CE00EFEFEF00FFFFFF0000000000000000002984630029B5840029B5840021AD
      7B00107B5200FFFFEF00217BC600398CCE00217BC600FFFFEF00107B520021AD
      7B0029B5840029AD7B0029846300FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF009C9C9C000000000000000000F7F7F700B5B5B5008C8C
      8C00848484008C84840042B5F7008CE7FF0084D6FF00109CFF00316B9C009C94
      8400B5B5B500F7F7F7000000000000000000EFF7EF00527B5A00108C5A006394
      7B00DED6C600FFF7E700006BB500428CCE00006BB500FFF7E700DED6C6006394
      7300108C5A00527B5A00EFF7EF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C9C9C00F7F7F700F7F7
      F7008C8C8C006B6B6B00F7F7F7008C8C8C006B6B6B00F7F7FF00E79C5200E79C
      5200EFF7F700F7F7F7009C9C9C000000000000000000D6D6D6009C9C9C00F7F7
      EF00F7EFEF00DEDED600EFDED6002173CE0042C6FF0029ADFF00109CFF00316B
      9C00ADA59C00D6D6D6000000000000000000FFFFFF00AD8C7300FFF7EF00FFEF
      DE00FFEFDE00FFF7DE000052AD00398CCE000052AD00FFF7DE00FFEFDE00FFEF
      DE00FFF7EF00AD8C7300FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C9C9C00F7F7F700EFEF
      EF00F7F7F700F7F7F700F7F7EF00F7F7F700F7F7F700EFF7F700EFAD6300E7A5
      5A00EFEFF700F7F7F7009C9C9C000000000000000000BDBDBD00ADADAD00EFEF
      EF008C848400DED6D6008C8C8400EFDECE002973C60042CEFF0029ADFF00109C
      FF00296BA500B5B5B5000000000000000000FFFFFF00A58C7300FFF7EF00F7E7
      D600F7EFDE0094846300A58C6B00A58C6B00A58C6B0094846300F7EFDE00F7E7
      D600FFF7EF00A58C7300FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000CECECE00CECECE00F7F7F700000000000000
      000000000000000000000000000000000000000000009C9C9C00F7F7F700EFEF
      E7008C8C8C006B6B6B00EFEFEF008C8C8C006B6B6B00EFEFF700F7AD6300F7AD
      6300E7EFEF00F7F7F7009C9C9C000000000000000000ADADAD00BDBDB500DED6
      D600D6CECE008C8C8C00D6D6D600948C8C00E7D6CE00297BCE0042C6FF0021AD
      FF0084B5D6007B7B7300CECECE0000000000FFFFFF009C8C7300FFF7EF00C6B5
      A500CEBDA500CEBDAD00D6C6AD00D6C6AD00D6C6AD00CEBDAD00CEBDA500C6B5
      A500FFF7EF009C8C7300FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000CECECE006363630063636300BDBDBD00000000000000
      000000000000000000000000000000000000000000009C9C9C00F7EFEF00E7E7
      E700EFE7E700EFEFEF00EFE7E700EFE7EF00EFEFEF00E7E7E700E7E7EF00E7E7
      EF00E7E7E700F7EFEF009C9C9C00000000000000000094949400C6C6C600CECE
      CE0094948C00CECECE0094948C00D6CECE0094948C00DECEC6002173CE00ADDE
      F700948C8400C6BDBD00737B6B00CECECE00FFFFFF00A58C7300FFF7EF00F7E7
      D600CEBDA500F7E7DE00CEBDA500F7E7DE00CEBDA500F7E7DE00CEBDA500F7E7
      D600FFF7EF00A58C7300FFFFFF00FFFFFF000000000000000000000000000000
      000000000000CECECE0063636300ADADAD00ADADAD0063636300CECECE000000
      000000000000000000000000000000000000000000009C9C9C00EFEFEF00E7DE
      DE00948C8C00736B6B00E7E7E700948C8C00736B6B00E7E7E700948C8C00736B
      6B00E7DEDE00EFEFEF009C9C9C0000000000000000008C8C8C00CECEC6009494
      9400CEC6C60094949400CEC6C60094949400CEC6C6009C949400CECEC6008484
      7B00EFEFE7008C8C8400BD7BB5009C6BCE00FFFFFF00A58C7B00FFF7EF00C6B5
      A500CEBDA500CEBDA500CEBDA500CEBDA500CEBDA500CEBDA500CEBDA500C6B5
      A500FFF7EF00A58C7B00FFFFFF00FFFFFF000000000000000000000000000000
      0000CECECE0063636300C6BDBD00B5B5B500B5B5B500C6BDBD0063636300CECE
      CE0000000000000000000000000000000000000000009C9C9C00EFEFEF00DEDE
      D600E7DED600E7DED600E7DEDE00E7DEDE00E7DEDE00E7DEDE00E7DED600E7DE
      D600DEDED600EFEFEF009C9C9C0000000000D6D6D60084848400ADA5A500A5A5
      A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500ADA5A500ADA5
      A5007B847B00E7B5E700CE94CE00B57BD600FFFFFF00A5947B00FFF7EF00EFDE
      CE00CEB5A500F7E7D600CEBDA500F7E7D600CEBDA500F7E7D600CEB5A500EFDE
      CE00FFF7EF00A5947B00FFFFFF00FFFFFF000000000000000000000000000000
      000063636300DEDEDE00EFE7E700E7E7E700E7E7E700EFE7E700DEDEDE006363
      630000000000000000000000000000000000000000009C9C9C00EFEFEF00DED6
      CE00187BFF002163FF00DEDED600948C8C00736B6B00DEDED6002184FF002163
      FF00DED6CE00EFEFEF009C9C9C00000000008C8C8C00EFEFEF00DEDEDE00DEDE
      E700DEDEDE00DEDEDE00DEDED600DEDED600DEDED600DEDEDE00DEDEDE00DEDE
      E700DEE7DE00BD84DE00C684DE0000000000FFFFFF00A5947B00FFFFF700C6B5
      9C00CEB5A500CEB5A500CEB5A500CEB5A500CEB5A500CEB5A500CEB5A500C6B5
      9C00FFFFF700A5947B00FFFFFF00FFFFFF000000000000000000000000000000
      00008C8C8C006363630063636300636363006363630063636300636363008C8C
      8C000000000000000000000000000000000000000000A5A5A500F7F7F700F7EF
      EF00F7F7EF00F7F7EF00F7F7EF00F7F7F700F7F7F700F7F7EF00F7F7EF00F7F7
      EF00F7F7EF00F7F7F700A5A5A5000000000084848400E7EFEF00D6AD6300C66B
      0000C6C6D600C6C6C600C6C6BD00C6C6BD00C6C6BD00C6C6C600C6C6D600C66B
      0000D6AD6300E7EFEF00848C840000000000FFFFFF00AD947B00FFFFF700EFDE
      CE00CEB5A500EFDECE00CEB5A500EFDECE00CEB5A500EFDECE00CEB5A500EFDE
      CE00FFFFF700AD947B00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5A5A5007B7B7B007B7B
      84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B
      84007B7B84007B7B7B00A5A5A50000000000ADADAD00E7EFEF00FFEFAD00DE9C
      3100C6730000DEE7F700E7E7E700E7E7E700E7E7E700DEE7F700C6730000DE9C
      3100FFEFAD00E7EFEF009494940000000000FFFFFF00AD948400FFFFF700C6B5
      9C00C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5
      9C00FFFFF700AD948400FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5A5A500FFCE8C00FFC6
      7B00FFC67B00FFC68400FFC68400FFC68400FFC68400FFC68400FFC68400FFC6
      7B00FFC67B00FFCE8C00A5A5A5000000000000000000ADADAD007B848C00FFEF
      B500DE9C3100C67300007B84940084848C007B849400C6730000DE9C3100FFEF
      B5007B848C00ADADB5000000000000000000FFFFFF00AD9C8400FFFFFF00E7D6
      C600C6B59C00EFD6C600C6B59C00EFD6C600C6B59C00EFD6C600C6B59C00E7D6
      C600FFFFFF00AD9C8400FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009CA5A500FFD6A500EFA5
      6300E7A56300E7AD6300E7AD6300E7AD6300E7AD6300E7AD6300E7AD6300E7A5
      6300EFA56300FFD6A5009CA5A500000000000000000000000000000000000000
      0000FFEFB500DE9C3100CE73000000000000CE730000DE9C3100FFEFB5000000
      000000000000000000000000000000000000FFFFFF00B59C8400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00B59C8400FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009CA5A500FFDEB500FFDE
      BD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDE
      BD00FFDEBD00FFDEB5009CA5A500000000000000000000000000000000000000
      000000000000FFEFB500DE9C390000000000DE9C3900FFEFB500000000000000
      000000000000000000000000000000000000FFFFFF00CEC6B500B59C8C00B59C
      8C00B59C8400B59C8400B59C8400B59C8400B59C8400B59C8400B59C8400B59C
      8C00B59C8C00CEC6B500FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADADAD009CA5A5009CA5
      A5009CA5A5009CA5A5009CA5A5009CA5A5009CA5A5009CA5A5009CA5A5009CA5
      A5009CA5A5009CA5A500ADADAD000000000000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF00000000000000000000000000FFFFFF000000000000000000DEDEDE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00DEDEDE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6AD9400AD520800A5520800A552
      0800A5520800A5520800A5520800A5520800AD520800C6AD9400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00947B6300947B6300FFFFFF00947B
      6300947B6300FFFFFF00947B6300947B6300FFFFFF00947B6300947B6300FFFF
      FF00947B6300947B6300947B6300FFFFFF000000000000000000BDBDBD00B5B5
      AD00B5B5AD00B5B5AD00B5B5AD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00ADADAD00ADADAD00BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B55A1800DE9C5A00E7AD7300E7AD
      7300E7A57300E7A57300E7AD7300E7AD7300DE9C5A00B5631800FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000B5B5AD000000
      000000000000848CF70000000000000000000000000000000000000000000000
      00000000000000000000B5B5AD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C67B3100EFC69400E7A56300E79C
      5A00E79C5A00E79C5A00E79C5A00E7A56300EFC69C00BD6B2100FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000ADADAD000000
      0000000000003139E70000000000000000000000000000000000000000000000
      00000000000000000000ADADAD0000000000000000000000000000000000CECE
      CE00DEDEDE000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE8C4A00F7D6BD00F7B57300F7BD
      7B00F7FFFF00EFEFF700F7BD7B00F7BD7B00F7DEC600C6732900FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADB5AD00E7D6
      BD00D6B58C002121BD00D6B58C00C6AD8C00C6AD8C00C6AD8C00C6AD8C00C6AD
      8C00C6AD8C00E7D6C600ADB5AD00000000000000000000000000CECECE00008C
      4A00299C6B000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D68C4200F7DEC600FFE7CE00FFFF
      EF00FFFFFF00F7F7F700F7E7D600EFCEA500EFD6BD00D6844200FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADB5AD000000
      0000000000003942E7000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000ADB5AD000000000000000000CECECE000084420052DE
      B500008C4200CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00DEDEDE00FFFFF700E7A56300EFBD8400ADC6
      CE005A9CC600397BA500738C8C00BD7B3900BD844A00CEB5A500EFEFEF00F7F7
      F700FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000ADB5AD00E7D6
      C600D6B58C002121BD00D6BD8C00C6AD9400C6AD9400C6AD9400C6AD9400C6AD
      8C00C6AD8C00E7D6C600ADB5AD0000000000CECECE000084420063D6B50000DE
      A50000BD7B0000844200008C4200008C4200008C4200008C4200008C4200008C
      4200008C4200008C4200008C4A0039A57300FFFFFF00FFFFFF00FFEFE70094AD
      AD0063A5C600528CAD0073848400D6AD8400FFFFE700CEAD7B00C6AD9400CEBD
      B500CECECE00CECECE00EFEFEF00FFFFFF00947B630000000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF000000000000000000947B6300FFFFFF000000000000000000ADB5AD000000
      0000FFFFFF003142E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000ADB5AD0000000000008C4A0073DEBD0000CE9C0000CE
      9C0000CE9C0000CE9C0000D6A50000D6A50000D6A50000D6A50000D6A50000D6
      A50000D6A50000D6A50000D6A500008C4A00FFFFFF00FFFFFF00C6CECE007BBD
      E7007BC6E7006BADD6004A94C600ADA59400D6B58400FFFFFF00D6AD8400CEA5
      7300B5844200BD844A00C6B59C00EFEFEF00947B6300947B6300FFFFFF00947B
      6300947B6300FFFFFF00947B6300947B6300FFFFFF00947B6300947B6300FFFF
      FF00947B6300947B6300947B6300FFFFFF000000000000000000ADB5AD00E7D6
      C600D6B58C002121BD00D6BD9400C6AD9400C6AD9400C6AD9400C6AD9400C6AD
      9400C6AD8C00E7D6C600ADB5AD0000000000008C4A0084DECE0000C69C0000C6
      9C0063DEC60063E7CE0063E7CE0063E7CE0063E7CE0063E7CE0063E7CE0063E7
      CE0063E7CE0063E7CE0063E7D600008C4A00FFFFFF00FFFFFF0010396B00A5EF
      FF008CD6F7007BBDE7008C9C9C00CE9C5A00F7DEBD00FFFFE700FFFFE700FFFF
      EF00FFFFEF00F7E7C600CE9C6B00C6B5A50000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000ADB5AD000000
      0000FFFFF7003139E700FFFFFF00F7FFFF00F7F7FF00F7F7FF00F7F7FF00F7F7
      FF00F7F7F70000000000ADB5AD000000000000000000008442009CE7D60000C6
      9C0000AD7B000084420000844200008C4200008C4200008C4200008C4200008C
      4200008C4200008C4200008C42004AB58400FFFFFF00FFFFFF0008396B006BAD
      CE007BB5DE0084CEF700C68C4A00FFE7CE00FFF7DE00FFF7DE00FFF7DE00FFF7
      D600FFF7D600FFF7DE00FFEFCE00BD8C5200947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADB5AD00E7D6
      C600D6B58C002121BD00D6BD9400CEAD9400C6AD9400C6AD9400C6AD9400C6AD
      9400C6AD8C00E7D6C600ADB5AD0000000000000000000000000000844200A5E7
      DE00008C42000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00104273002973
      AD004A8CBD004273AD00CE8C4200FFFFE700FFEFCE00FFEFD600FFEFD600FFEF
      D600FFEFCE00FFEFCE00FFFFE700C68C4A00947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADB5AD000000
      0000FFFFEF003139DE00FFFFF700F7F7F700EFF7F700EFF7F700EFF7F700EFEF
      F700EFEFF70000000000ADB5AD0000000000000000000000000000000000008C
      4A00219C63000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0010427300297B
      AD00398CBD002973AD00D6944A00FFF7E700FFE7C600FFEFCE00FFEFCE00FFEF
      CE00FFEFCE00FFE7C600FFF7E700C68C520000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000ADB5AD00E7D6
      C600CEB58C002121BD00D6B58C00C6AD9400C6AD9400C6AD9400C6AD9400C6AD
      9400C6AD8C00E7D6C600ADB5AD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7EF00104A
      7B00185A9400084A8400CE8C5200FFEFCE00FFEFCE00FFE7C600FFE7C600FFE7
      C600FFE7C600FFEFC600FFE7CE00CE8C5200947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADADAD000000
      0000EFEFE7002931D600EFEFE700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000ADADAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F7DECE00DEA57300F7E7CE00FFF7E700FFF7E700FFF7
      E700FFF7E700F7E7CE00D6A57300EFDEC6000000000000000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF000000000000000000947B6300FFFFFF000000000000000000B5B5AD000000
      0000000000008C8CF70000000000000000000000000000000000000000000000
      00000000000000000000B5B5AD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7DEC600D69C6300CE945A00CE945A00CE94
      5A00CE945A00D69C6300EFDEC600FFFFFF00947B6300947B6300FFFFFF00947B
      6300947B6300FFFFFF00947B6300947B6300FFFFFF00947B6300947B6300FFFF
      FF00947B6300947B6300FFFFFF00FFFFFF000000000000000000BDBDB500B5B5
      AD00B5B5AD00B5B5AD00B5B5AD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00ADADAD00B5B5AD00BDBDB500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700CECE
      CE00CECECE00CECECE00E7E7E7000000000000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE0000000000FFFFFF00FFFFFF0000000000CECECE00CECE
      CE00CECECE00CECECE00CECECE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00D6D6D600FFFFFF0000000000F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700DEDEDE0052B58C00009C
      5A00009C5A00009C5A0052B58C00E7E7E7009494940073737300736B6B006B6B
      6B006B6363006363630084848400FFFFFF00FFFFFF009494940073737300736B
      6B006B6B6B006B6363006363630084848400CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE0063A5CE003994CE00398CCE003994
      CE003994D6003194D6005A6B73004A638400528CD6003194D6003194CE00318C
      CE00318CCE00318CCE004A9CD600FFFFFF00D6D6D600CEC6BD00CEC6BD00CEC6
      BD00CEC6BD00CEC6BD00CEC6BD00CEC6BD00D6C6BD004AB58C0000A56B0000BD
      840073DEC60000BD840000A56B0052B58C007B7B7300847B7B009C9C94008C84
      8400736B6B005A525200635A5A00FFFFFF00FFFFFF007B7B7300847B7B009C9C
      94008C848400736B6B005A525200635A5A000808DE000000DE000000DE000000
      DE000000DE000000DE000000DE000000DE000000DE000000DE000000DE000000
      DE000000DE000000DE000000DE000808DE003994CE0073DEFF005AD6FF004ACE
      F70039ADE70052B5EF0063849C0084A5B5008CD6FF003163940084EFFF007BE7
      FF007BE7FF0084E7FF00318CCE00FFFFFF00CEA57300CEA56300CE9C6300CE9C
      6300CE9C6300CE9C6300CE9C6300CE9C6300E7A56300009C5A0000BD8C0000BD
      84000000000000BD840000C68C00009C5A0073737300CECECE00DED6D600D6CE
      CE00CECEC600C6C6C6005A5A5A00FFFFFF00FFFFFF0073737300D6CECE00DED6
      D600D6CECE00CECEC600C6C6C6005A5A5A00DEDEB500FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFF700DEDEB500398CCE005AD6FF004AC6F70039AD
      E70039BDF700318CCE004AB5EF008CE7FF0084D6FF00109CFF00316394007BE7
      FF0073DEFF0084E7FF00318CCE00FFFFFF00CEA56300FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFF7000094520073E7CE000000
      0000000000000000000073E7CE00009C5A0073737300B5B5AD00CECEC600B5AD
      AD0094948C00737373005A5A5A00CECECE00CECECE0073737300B5B5AD00CECE
      C600B5ADAD0094948C00737373005A5A5A000000E700184AFF000031FF000031
      FF000031FF000031FF000031FF000031FF000031FF000031FF000031FF000031
      FF000031FF000031FF00184AFF000000E7003994CE004ACEF70039ADE70039BD
      EF0042D6FF00318CCE0073E7FF003173C60042C6FF0029ADFF00189CFF003163
      8C0073E7FF0084E7FF00318CCE00FFFFFF00CE9C6300FFFFEF00E7CEA500F7E7
      C600FFF7DE00FFF7DE00FFF7DE00FFF7DE00FFFFE70000944A0000CE940000C6
      8C000000000000CE8C0000CE9C00009C5A0073737300B5B5AD00CECEC600B5AD
      AD0094949400737373005A5A5A007B7B73006363630073737300B5B5AD00CECE
      C600B5ADAD009494940073737300635A5A00DEDEB500FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00DED6B5003994CE0039ADE70039BDF70042D6
      FF0063DEFF003184CE006BDEFF006BDEFF003173BD004AC6FF0029ADFF00109C
      FF00316394008CEFFF003194D600FFFFFF00CE9C6300FFFFEF00FFFFEF00DEB5
      8400E7C69400FFF7DE00FFF7DE00FFF7DE00FFFFE7006BC6940000AD6B0000D6
      9C0073EFD60000D69C0000AD6B005AA5630073737300B5B5AD00CECEC600B5AD
      AD0094948C00737373005A5A520073737300635A5A0073737300B5B5AD00CECE
      C600B5ADAD0094948C00737373005A5A5A000000F7003963FF000021FF000021
      FF000021FF000021FF000021FF000021FF000021FF000021FF000021FF000021
      FF000021FF000021FF003963FF000000E7003994CE005AB5E700318CCE00318C
      CE003184CE0042A5DE0063DEFF0063D6FF0063DEFF003173BD0042C6FF0021AD
      FF0084ADD600847363002994DE00FFFFFF00CE9C6300FFFFEF00FFEFD600FFFF
      F700DEB58400FFF7DE00DEBD8C00C6945200CE945A00F7BD94006BC694000094
      520000944A00009452006BC69C00E7A56B0073737300DEDEDE00EFEFEF00DEDE
      DE00CECECE00BDBDBD0052525200D6D6D6008C8C8C0073737300DEDEDE00EFEF
      EF00DEDEDE00CECECE00BDBDBD005A5A5A00A55A0000FFFFE700E76B0000FFF7
      C600E76B0000FFF7C600E76B0000FFF7C600EF6B0000FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00DED6B5003194CE005AADE70063DEFF0063DE
      FF0063DEFF0063D6FF005AD6FF005AD6FF0063D6FF0063DEFF002973C600B5DE
      EF00948C7B00C6BDB5007B7B6B00CECECE00CE9C6300FFFFEF00FFEFCE00FFEF
      CE00FFFFFF00E7C69400DEBD8C000000000000000000E7BD8C00F7C69C000000
      0000FFF7D600FFF7D600FFFFEF00D6A563007B7B73006B6B6B0063636300635A
      5A005A5A5A005A5A5A00948C8C00D6D6D6008C8C8400A59C9C006B6B6B006363
      6300635A5A00635A5A00635A5A0094949400945A0000EFA55200FFE7CE00CE5A
      0000FFEFD600CE5A0000FFEFD600CE5A0000FFEFC6000010FF000010FF000010
      FF000010FF000010FF006373FF000000E700318CCE0094EFFF0052D6FF005AD6
      FF005AD6FF005AD6FF005AD6FF005AD6FF005AD6FF005AD6FF0052DEFF00947B
      7300EFE7E7008C8C8400BD7BB5009C6BCE00CE9C6300FFFFEF00FFEFC600FFEF
      C600EFCE9C00DEB5840000000000FFEFCE00FFEFCE0000000000DEB58400EFCE
      9C00FFEFCE00FFEFC600FFFFEF00CE9C6300FFFFFF0073737300BDBDBD00CECE
      CE00A5A5A5007B7B7B005A525200D6D6D6008C8C8C0073737300BDBDB500CECE
      CE00A5A5A5007B7B7B005A5A5A00FFFFFF0094520000FFFFFF00BD520000F7E7
      D600BD520000F7E7D600BD520000F7E7D600C6520000FFFFFF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00D6D6B500318CCE009CEFFF004AD6FF0052D6
      FF0052D6FF0052D6FF0052D6FF0052D6FF0052D6FF0052D6FF0052D6FF004AD6
      FF008C7B7300E7B5DE00CE94C600B57BCE00CE9C6300FFFFEF00FFEFC600F7DE
      AD00DEBD8400FFFFFF00FFEFC600FFEFC600FFEFC600FFEFC600FFFFFF00DEBD
      8400F7DEAD00FFEFC600FFFFEF00CE9C6300FFFFFF0073737300BDB5B500CEC6
      C600A5A59C007B7B7B005A5A5A0073737300635A5A0073737300BDBDB500CEC6
      C600A5A59C007B7B7B00635A5A00FFFFFF0094520000E7B58400F7E7CE00B54A
      0000F7E7D600B54A0000F7E7D600B54A0000FFE7C6000000FF000008FF000008
      FF000008FF000000FF00848CFF000000E700318CCE009CEFFF0042CEFF004ACE
      FF004ACEFF004ACEFF004ACEFF004ACEFF004ACEFF004ACEFF004ACEFF004AD6
      FF0039D6FF00CE84CE00CE84D600FFFFFF00CE9C6300FFFFF700FFE7B500DEB5
      8400FFFFEF00FFE7BD00FFE7BD00FFE7BD00FFE7BD00FFE7BD00FFE7BD00FFFF
      EF00DEB58400FFE7B500FFFFF700CE9C6300FFFFFF007B737300BDBDBD00CECE
      CE00A5A5A5007B7B7B005A5A5A007B7B7300636363007B737300BDBDBD00CECE
      CE00A5A5A500847B7B00635A5A00FFFFFF009452000000000000A5420000F7DE
      CE00A5420000F7DECE00A5420000F7DECE00AD420000FFFFFF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00DEDEB500318CCE00A5EFFF008CE7FF008CE7
      FF0094E7FF0094E7FF0094E7FF0094E7FF0094E7FF0094E7FF0094E7FF008CE7
      FF008CE7FF009CF7FF00298CCE00FFFFFF00CE9C6300FFFFF700E7BD8C00FFF7
      E700FFE7B500FFE7B500FFE7B500FFE7B500FFE7B500FFE7B500FFE7B500FFE7
      B500FFF7E700E7BD8C00FFFFF700CE9C6300FFFFFF00A5A5A500736B6B006B63
      630063635A00635A5A008C8C8C00FFFFFF00FFFFFF009C9C9C006B6B6B006B63
      6300636363006363630094949400FFFFFF00945A0000E7C6AD0000000000E7C6
      A50000000000E7C6A50000000000E7C6A500FFFFFF009CADFF00A5ADFF00A5AD
      FF00A5ADFF00ADADFF00ADB5FF000000DE00318CCE00ADEFFF0031B5EF0039B5
      EF0039B5EF0039B5EF0039B5EF0039B5EF0039B5EF0039B5EF0039B5EF0039B5
      EF0031B5EF00ADF7FF00318CCE00FFFFFF00CEA56300FFFFFF00FFFFFF00FFFF
      F700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFF
      F700FFFFF700FFFFFF00FFFFFF00CEA56300FFFFFF00FFFFFF00CECECE007B73
      7300CECEC6009C9C9C00635A5A00FFFFFF00FFFFFF007B737300CECEC6009C94
      9400635A5A00CECECE00FFFFFF00FFFFFF00BD944A00945A0000945200009452
      0000945200009452000094520000945A0000A55A00000000EF000000DE000000
      DE000000DE000000DE000000DE003939E700318CCE00B5F7FF0031CEFF0039CE
      FF0039CEFF0039CEFF0039CEFF0039CEFF0039CEFF0039CEFF0039CEFF0039CE
      FF0031CEFF00B5F7FF00318CCE00FFFFFF00DEC69C00CEA56300CE9C6300CE9C
      6300CE9C6300CE9C6300CE9C6300CE9C6300CE9C6300CE9C6300CE9C6300CE9C
      6300CE9C6300CE9C6300CEA56300D6B58400FFFFFF00FFFFFF007B7B7300948C
      8C00948C8C0063636300635A5A00FFFFFF00FFFFFF007B7373008C8C8C00948C
      8C006B63630063635A00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000318CCE0084EFFF0084E7FF0084E7
      FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7
      FF0084E7FF0084EFFF00318CCE00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF007B7B73007373
      6B006B6B6B006B636300635A5A00FFFFFF00FFFFFF007B7B730073736B006B6B
      6B006B63630063635A00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000429CD600318CCE00318CCE00318C
      CE00318CCE00318CCE00318CCE00318CCE00318CCE00318CCE00318CCE00318C
      CE00318CCE00318CCE00429CD600FFFFFF000000000000000000000000000000
      00000000000000000000D6D6D600BDBDBD00BDBDBD00D6D6D600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBDBD00BDBDBD00BDBDBD00F7F7F70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DEDEDE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00DEDEDE0000000000D6D6D600BDBDBD00BDBD
      BD00BDBDBD00BDBDBD009C9C9C00848484008C8C8C009C9C9C00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00D6D6D6000000000000000000EFEFEF00C6C6C600BDBD
      BD00BDBDBD005A5A5A00525252008C8C8C00B5B5B500C6C6C600EFEFEF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDEDE00CECECE00CECECE00CECE
      CE00CECECE00CECECE005A73A500214A8400294A8400294A8400294A8400294A
      8400294A8400294A8400294A84005A739C0000000000848484006B6B6B006B6B
      6B006B6B6B006B6B6B0094949400ADADAD00CECECE00BDBDBD00949494006B6B
      6B006B6B6B006B6B6B0084848400000000000000000094949400424242003939
      3900393939007B7B7B008C8C8C00D6D6D6006363630042424200949494000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840063636300636363006363
      6300636363006B636300214A8C005A94BD00217BAD00217BB5002173A5002173
      A500217BB500217BAD005A94BD00214A8400BDBDBD006B6B6B00CECECE007373
      73006B6B6B00F7F7F700EFEFEF0084848400BDBDBD00DEDEDE00BDBDBD008484
      8400848484008C8C8C007B7B7B00DEDEDE00000000004A4A4A007B7B7B008C8C
      8C008C8C8C00B5B5B500D6D6D600D6D6D600A5A5A500636363004A4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700BDBDBD0000000000000000000000000063636300736B6B007B7373007B73
      730063636300736B6300214A84003184AD0010639C001063940008527B000852
      7B001063940010639C003184AD00214A84007B7B7B00BDBDBD004A4A4A007B7B
      7B0063636300EFEFEF00DEDEDE00E7E7E70084848400BDBDBD00D6D6D600BDBD
      BD00C6C6C600C6C6C6009C9C9C00A5A5A500000000005A5A5A00A5A5A5008484
      8400848484007B7B7B0073737300C6C6C600ADADAD00A5A5A50063636300BDBD
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009494
      940063636300BDBDBD00000000000000000073737300A59C9C007B7B73006363
      6300525252005A525200214A8C00398CB500106B9C00106B9C0042C6FF0042C6
      FF00106B9C00106B9C00398CB500214A8400000000006B6B6B005A5A5A008484
      840063636300EFEFEF00DEDEDE00DEDEDE00DEDEDE007B7B7B00CECECE00CECE
      CE00D6D6D600D6D6D600D6D6D60094949400000000006B6B6B00BDBDBD009494
      940094949400FFFFFF00EFEFEF0073737300C6C6C600B5B5B500A5A5A5006363
      6300BDBDBD00000000000000000000000000DEDEDE00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD006363
      6300C6C6C60063636300BDBDBD000000000063636300C6C6C600636363007373
      7300313131004A4242002142840063A5C60008639C0008639C00086394000863
      940008639C0008639C0063A5C600214A8400BDBDBD006B6B6B00CECECE008C8C
      8C0063636300EFEFEF00D6D6D600D6D6D600DEDEDE007B7B7B00DEDEDE00DEDE
      DE006B6B6B006B6B6B00E7E7E70094949400000000007B7B7B00D6D6D600A5A5
      A500EFEFEF0000000000F7F7F700D6D6D60073737300C6C6C600ADADAD00ADAD
      AD0063636300BDBDBD0000000000000000008484840063636300636363006363
      6300636363006363630063636300636363006363630063636300636363009494
      9400BDBDBD00BDBDBD0063636300BDBDBD00C6C6C6006B6B6B0084847B00C6C6
      C600948C8C009C948C00184284006BA5BD00639CB500639CBD00639CB500639C
      B500639CBD00639CBD006BA5BD00214A84007B7B7B00BDBDBD004A4A4A009494
      940063636300EFEFEF00CECECE00F7F7F7000000000084848400D6D6D600DEDE
      DE007373730073737300DEDEDE009C9C9C000000000073737300B5B5B500E7E7
      E700D6D6D60094949400737373008C8C8C00EFEFEF007B7B7B00D6D6D6007373
      7300ADADAD0063636300BDBDBD000000000063636300BDBDBD00B5B5B500B5B5
      B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500ADAD
      AD00ADADAD00ADADAD00C6C6C6006363630000000000FFFFFF00736B6B00EFEF
      F70031312900313129005273A50010397B0010397B001842840018427B001839
      7B0018427B0010398400184284006B8CB500000000006B6B6B005A5A5A009C9C
      9C0063636300EFEFEF00CECECE0000000000ADADAD009C9C9C00A5A5A500D6D6
      D600D6D6D600D6D6D600ADADAD00C6C6C6000000000000000000A5A5A5009494
      9400B5B5B5008C8C8C006B6B6B007B7B7B0094949400ADADAD006B6B6B00E7E7
      E700737373007B7B7B00737373000000000063636300D6D6D600CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00A5A5A500A5A5A500CECECE00636363000000000000000000FFFFFF00B5CE
      DE006BADD6005294B500ADC6D600F7F7F700F7EFEF0008398400FFF7EF00FFF7
      EF0008397B0000000000CE84000000000000BDBDBD006B6B6B00CECECE00A5A5
      A50063636300EFEFEF00C6C6C600000000006B6B6B00B5B5B5009C9C9C009494
      9400949494009C9C9C008484840000000000000000000000000000000000D6D6
      D6009C9C9C00B5B5B5009C9C9C007B7B7B00E7E7E70000000000000000007373
      7300B5B5B5009494940084848400000000009494940063636300636363006363
      6300636363006363630063636300636363006363630063636300636363008C8C
      8C00A5A5A500D6D6D60063636300000000000000000000000000D6D6DE008CCE
      EF0084C6E7006BADD60063A5CE00F7CE1800FFDE080063735A00003184000031
      8400636B4A0000000000BD7B0000000000007B7B7B00BDBDBD004A4A4A00ADAD
      AD0063636300F7F7F700C6C6C600F7F7F700000000000000000000000000F7F7
      F700CECECE00F7F7F7006B6B6B00000000000000000000000000000000006363
      6300DEDEDE00BDBDBD00ADADAD00A5A5A5007373730000000000000000000000
      0000949494008C8C8C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006363
      6300D6D6D6006363630000000000000000000000000000000000395A8400ADEF
      FF008CD6F7007BBDE7006BB5DE0042524200FFD64200F7D63900FFD63900FFD6
      3900F7D6310000000000B57B080000000000000000006B6B6B005A5A5A00ADAD
      AD0063636300F7F7F700C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600F7F7F70063636300000000000000000000000000000000003131
      3100F7F7F700D6D6D600C6C6C600BDBDBD002121210000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C9C
      9C0063636300000000000000000000000000000000000000000008396B006BAD
      D60073ADD6008CC6E70073B5DE00001052000000000000000000000000000000
      00000000000000000000B57B080000000000BDBDBD006B6B6B00BDBDBD00ADAD
      AD0063636300FFFFFF00BDBDBD00C6C6C6005A5A5A008C8C8C008C8C8C00C6C6
      C600BDBDBD00FFFFFF0063636300000000000000000000000000000000003939
      3900525252007B7B7B00ADADAD008C8C8C002929290000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000010427300297B
      AD004A8CBD00527BA5002142730000215A00CEC6C600E7E7E70000000000FFEF
      AD00FFDE420000000000B57B0800000000007B7B7B00BDBDBD0042424200ADAD
      AD0063636300FFFFFF00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00FFFFFF0063636300000000000000000000000000000000003939
      3900737373008484840073737300424242002929290000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000849CB500217B
      AD00398CBD00397BAD0010396B005A6B8400CEC6C600BDBDBD00BDB5BD00F7E7
      8400E7B5000000000000B57B080000000000000000006B6B6B00EFEFEF00E7E7
      E7006363630000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00000000006B6B6B00000000000000000000000000000000005252
      52006B6B6B007B7B7B006B6B6B00424242004A4A4A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007394
      B500105A9400084A8C007B9CBD00000000000000000000000000000000000000
      00000000000000000000B57B08000000000000000000949494006B6B6B006B6B
      6B006B6B6B006B6B6B0063636300636363006363630063636300636363006363
      6300636363006B6B6B009494940000000000000000000000000000000000E7E7
      E7004A4A4A004A4A4A004A4A4A0042424200E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7B55A00C6840800BD7B0800B57B0800B57B0800B57B0800B57B
      0800B57B0800B57B0800CEAD6300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CECECE00CECECE00CECECE00F7F7F70000000000000000000000
      00000000000000000000000000000000000000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00000000000000000000000000000000000000
      00000000000000000000CECECE00CECECE00CECECE00CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7F7F700CECECE00CECE
      CE00CECECE0052738C004A6384004A94DE00B5C6D600CECECE00F7F7F7000000
      000000000000000000000000000000000000BD944200B57B0800B57B0800B57B
      0800B57B0800B57B0800B57B0800B57B0800B57B0800B57B0800B57B0800B57B
      0800B57B0800B57B0800B57B0800BD9442000000000000000000000000000000
      000000000000000000007B7B7B00E7E7E700B5B5B50084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AD9400AD521000AD52
      0800B54A0000528CAD00739CB5008CD6FF00296BA500BD520000C6AD94000000
      000000000000000000000000000000000000B57B0800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B57B0800DEDEDE00CECECE00CECECE00CECE
      CE00CECECE00CECECE0084848400EFEFEF00BDBDB50084848400CECECE00CECE
      CE00CECECE00CECECE00CECECE00DEDEDE00000000000000000000000000BDBD
      BD00CECECE000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B55A1000D6945A00E7A5
      6B00EFA5630039B5FF007BD6FF0084D6FF00109CFF00296BAD00CE6300000000
      000000000000000000000000000000000000B57B0800FFFFFF00A59C9C008C8C
      8C0094949400D6D6D600FFFFFF00C6C6C600C6C6C600FFFFFF00C6C6C600C6C6
      C600C6C6C600C6C6C600FFFFFF00B57B08008C8C8C006B6B6B006B6363006B63
      63006B6B63006B6B6300736B63006B6363006B6363006B6363006B6B6B006B6B
      6B006B6B6B006B6B6B006B6B6B008C8C8C000000000000000000BDBDBD006363
      63007B7B7B000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6733100EFBD8C00E79C
      5A00E79C5200F79C42001873CE0039C6FF0029ADFF00109CFF00316BA500CECE
      CE0000000000000000000000000000000000B57B0800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B57B0800736B6B00CECECE00C6C6C600C6C6
      BD00D6D6D600ADA594000000C600ADA59C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00ADADA500736B6B0000000000BDBDBD0063636300C6C6
      C60063636300BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00D6D6D60000000000CE844A00EFCEAD00EFAD
      6B00EFAD6B00FFFFFF00FFF7E7002173C60042C6FF0029ADFF00109CFF00316B
      A500CECECE00000000000000000000000000B57B0800FFFFFF009C9494008C8C
      84008C8C8C00CECECE00FFFFFF00BDBDBD00BDBDBD00FFFFFF00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00FFFFFF00B57B080073737300C6C6C600B5B5B500B5B5
      B500CECECE00BDB5A5000000C600BDB5A500ADADA500ADADA500ADADA500ADAD
      A500ADADA500ADADA500BDBDBD0073737300BDBDBD0063636300BDBDBD00BDBD
      BD00949494006363630063636300636363006363630063636300636363006363
      63006363630063636300636363007B7B7B0000000000D6945A00F7DECE00F7BD
      7B00FFF7DE00FFFFFF00FFF7EF00FFE7BD002173CE0042C6FF0021ADFF0084B5
      DE007B7B7300CECECE000000000000000000B57B0800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F7F7F700FFFFF700FFFFF700F7F7F700FFFFF700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B57B08007B737300C6C6BD00ADADAD00ADAD
      A500CECEC600C6C6B5000000C600CEC6B500BDBDB500BDB5B500BDB5B500BDB5
      B500BDB5B500BDB5B500D6CECE007373730063636300C6C6C600ADADAD00ADAD
      AD00ADADAD00B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
      B500B5B5B500B5B5B500BDBDBD006363630000000000D6944A00EFC69C00FFEF
      D600B5DEEF005AA5C600397BA500849CAD00FFF7CE00187BD600ADDEF700948C
      8400C6BDBD00737B6B00CECECE0000000000B57B0800FFFFFF0094948C008484
      84008C848400C6C6BD00F7F7F700B5B5B500B5B5B500F7F7F700B5B5B500B5B5
      B500B5B5B500B5B5B500FFFFFF00B57B08007B7B7B00CECECE00A5A59C00A59C
      9C00DEDED600D6D6C6000000CE00D6D6C600CEC6C600C6C6C600C6C6C600C6C6
      C600C6C6C600CEC6C600E7E7DE007B73730063636300CECECE00A5A5A500A5A5
      A500CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00D6D6D600636363000000000000000000EFBD8400F7AD
      6300BDC6BD00639CBD00427B9C0094948C00FFAD6300F7C68C0084848400EFEF
      E7008C8C8400BD7BB5009C6BCE0000000000B57B0800FFFFFF00F7F7F700F7F7
      F700F7F7F700F7F7EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00F7F7
      EF00F7F7EF00EFEFEF00FFFFFF00B57B0800847B7B00D6D6D6009C9494009494
      9400DEDED600E7E7CE000000CE00E7E7CE00D6D6CE00D6D6CE00D6D6CE00D6D6
      CE00D6CECE00DEDED600EFEFEF007B7B7B000000000063636300D6D6D600A5A5
      A5008C8C8C006363630063636300636363006363630063636300636363006363
      630063636300636363006363630094949400000000000000000000000000DEDE
      DE007BADC6007BBDDE006BADCE006394AD00E7E7E7000000000000000000848C
      8400E7B5E700CE94C600AD7BCE0000000000B57B0800FFFFFF008C8C8C00847B
      7B0084847B00BDBDB500EFEFE700ADADAD00ADADAD00EFEFE700ADADAD00ADAD
      AD00ADADAD00ADADAD00FFFFFF00B57B080084847B00E7E7E70094949400948C
      8C00F7F7EF00F7F7DE000000CE00F7F7DE00E7E7DE00E7DEDE00E7DEDE00E7DE
      DE00E7E7DE00F7F7EF00EFEFEF00847B7B00000000000000000063636300D6D6
      D600636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A73
      94009CE7FF0084C6EF0073B5DE006BADD6006B84A50000000000000000000000
      0000C68CDE00BD8CD6000000000000000000B57B0800FFFFFF00E7E7E700EFEF
      EF00EFEFE700E7E7E700E7E7DE00E7E7E700E7E7E700E7E7DE00E7E7E700E7E7
      E700E7E7E700E7E7E700FFFFFF00B57B0800B5ADAD00B5B5B500E7E7E700F7F7
      F700FFFFFF00FFFFF7000000D600FFFFF700FFF7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700B5ADAD00B5ADAD000000000000000000000000006363
      63007B7B7B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300ADF7FF0094DEFF0084CEE7007BC6E70000295A0000000000000000000000
      000000000000000000000000000000000000B57B0800FFFFFF00847B7B007373
      73007B737300ADADAD00DEDEDE00A5A59C00A5A59C00DEDEDE00A5A5A500A5A5
      A500A5A5A5009C9CA500FFFFFF00B57B0800000000009C9494008C8C8C008C8C
      84008C8C84009C9484000000DE009C9C84008C8C84008C8C84008C8C84008C8C
      84008C8C84008C8C840094949400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000839
      6B0021639400528CB5007BB5D600639CBD0008295A0000000000000000000000
      000000000000000000000000000000000000B57B0800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B57B08000000000000000000000000000000
      000000000000000000000000E700CECECE00CECECE00CECECE00DEDEDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      7300317BAD004A8CBD00527BAD00294A73001031630000000000000000000000
      000000000000000000000000000000000000B57B1000F7DEB500DE9C3100DE9C
      3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C
      3900DE9C3900DE9C3100F7DEB500B57B10000000000000000000000000000000
      000000000000000000000000E7000000E7000000EF000000EF004242E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000395A
      84002173AD00398CBD00397BAD00214A7300395A840000000000000000000000
      000000000000000000000000000000000000BD841000EFD69C00EFCE9C00EFCE
      9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE
      9C00EFCE9C00EFCE9C00EFD69C00BD8410000000000000000000000000000000
      000000000000000000000000E7000000E7000000D6000000BD000000E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEE7
      EF0021528400185A8C001852840021527B00DEE7EF0000000000000000000000
      000000000000000000000000000000000000CEA55200BD841000B5841000B584
      1000B5841000B5841000B5841000B5841000B5841000B5841000B5841000B584
      1000B5841000B5841000BD841000CEA552000000000000000000000000000000
      000000000000000000000000E7000000E7000000E7000000DE004A4AEF000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000B00000000100010000000000800500000000000000000000
      000000000000000000000000FFFFFF00FFFFFF8700000000FFFFFF0300000000
      FFFFFE01000000000000E000000000000000C000000000000000800000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000003F00000000
      FFFF003F00000000FFFF003F00000000FFF0FC3FFFFF1FFF0000FC1FFFFF1FFF
      0000800700001FFF0000000300001FFF000000007FFE1F070001000000000703
      0003000000000001000300000000000000030000000000000003000000000000
      021300000000000000C3000000000000000300017FFE0002000300010000003F
      00030003FFFF003F00030007FFFF003FF000FFDFFFFFFF8F0800FF9FE0078007
      04008081E007800302000001E007800101000001E007800100800081E0078001
      00400391E0078001002003C1E0078003001003E1F00FC007000807E1F81FE00F
      00040003F81FE07F0002C003F81FE07F0000C007F81FE07F0000E003F81FE07F
      0000E003F81FE07F0000F01FF81FE07FFFFFFFFEFFFFFFFFFFC10000FFFFFFC1
      FF8000000000FF80FF8000000000FF80000800007FFE0000001C00004002001C
      000800007D6E0000000000004002000000000000400200000000000040000000
      0000000040000000000000000000000000000000400200000000000000000000
      00000000FFFF000080010000FFFF8001E0008000E7FFCF8300000000E3FFC701
      00000000E0FFC10100000000E07FC01100380000F03FE03900000000F01FE011
      07000000C00F800100000000C007800300000000C003800700000000C0038007
      00000000C003800700000000C01F803F00000000E01FC03F00000000E00FC01F
      00000000E00FC01F00000000E00FC01FFFFFFFC1FFC1C00300008000FF80C003
      000080000000DFFB000080000008D24B0000801C001CD00B000080000188D00B
      000080000180D00B000080010001D00B000080030005D00B0000800301E1D00B
      0000000101E1DFFB000000010001C003000000010001C003000000010001C003
      000000010001C003000087C30001C003FC7FFFFEFFFF8001FC3F0000FFFF8001
      80030000FFFF800180030000FFFF800180030000FFFF800180030000FE3F8001
      80010000FC3F800180000000F81F800180000000F00F800100000000F00F8001
      00010000F00F800100010000FFFF800100010000FFFF800180030000FFFF8001
      F11F0000FFFF8001F93F0000FFFF80018040DB6EC001FFFF00000000C001FFFF
      00000000DBFDFFFF00008002DBFDE7FF00000000C001C7FF00000000DB058000
      00008002C001000000005B6CD005000000000000C001000000008002D0058000
      00000000C001C7FF00000000D005E7FF00008002C001FFFF00000000D005FFFF
      0000DB6CDBFDFFFF00000000C001FFFFFFC18241FFFF80008000000000000000
      00000000000000000008000000000000001C0000000000000008000000000000
      0000000000000000000000000000000001900000000000000240000000000000
      00000000000000000000000040000000000000002A0000000000000000000000
      00000000FFFF0000FFFF0000FFFF0000FC3FF87FFFFFFC008001801FFFFF0000
      8001801FFFFF00000000801FFFE700000000800FFFE300008000800700010000
      000084030000000000808001000080008100C0010000C0050101E0610001C005
      00E1E073FFE3C0058001E07FFFE7C0FD0001E07FFFFFC0250001E07FFFFFC005
      84C5E07FFFFFE1FD8001E07FFFFFF801FFFFF87F8001FC3FFFFF801F0000FC3F
      FFFF801F00000000E7FF801F00000000C7FF800F000000008000800700000000
      000080030000000000008001000000000000C001000000008000E06100000000
      C7FFE07300000000E7FFE07F00008001FFFFE07F0000FC1FFFFFE07F0000FC1F
      FFFFE07F0000FC1FFFFFE07F0000FC1F00000000000000000000000000000000
      000000000000}
  end
  inherited dsCL: TevDataSource
    Top = 65530
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 334
    Top = 26
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 447
    Top = 24
  end
  object SyStatesSrc: TevDataSource
    DataSet = DM_SY_STATES.SY_STATES
    Left = 360
    Top = 17
  end
  object SyStatesFreqSrc: TevDataSource
    DataSet = DM_SY_STATE_DEPOSIT_FREQ.SY_STATE_DEPOSIT_FREQ
    MasterDataSource = SyStatesSrc
    Left = 399
    Top = 1
  end
  object dsSUI: TevDataSource
    OnStateChange = dsSUIStateChange
    OnDataChange = dsSUIDataChange
    MasterDataSource = wwdsDetail
    Left = 504
    Top = 176
  end
  object SySUISrc: TevDataSource
    DataSet = DM_SY_SUI.SY_SUI
    MasterDataSource = SyStatesSrc
    Left = 394
    Top = 90
  end
  object evSecElement1: TevSecElement
    ElementType = otFunction
    ElementTag = 'USER_CAN_EDIT_TAX_EXEMPTIONS'
    Left = 384
    Top = 8
  end
  object pmStateDepositFrequency: TevPopupMenu
    Left = 148
    Top = 273
    object CopyTo1: TMenuItem
      Caption = 'Copy To...'
      OnClick = CopyStateTaxDeposit
    end
  end
  object pmSUITaxDepositFrequency: TevPopupMenu
    Left = 116
    Top = 273
    object MenuItem1: TMenuItem
      Caption = 'Copy To...'
      OnClick = CopySUIStateTaxDeposit
    end
  end
  object cdTCD_CollectionAgency: TevClientDataSet
    Left = 544
    Top = 88
  end
  object dsTaxCollection: TevDataSource
    DataSet = cdTCD_CollectionAgency
    OnDataChange = dsTaxCollectionDataChange
    Left = 560
    Top = 304
  end
  object pmTCDDepositFrequency: TevPopupMenu
    Left = 509
    Top = 325
    object MenuItem3: TMenuItem
      Action = CopyService
    end
  end
  object TCDCopyService: TevActionList
    Left = 592
    Top = 315
    object CopyService: TAction
      Caption = 'Copy To...'
      OnExecute = CopyServiceExecute
    end
  end
  object dsLocations: TevDataSource
    DataSet = DM_CO_LOCATIONS.CO_LOCATIONS
    MasterDataSource = wwdsDetail
    Left = 376
    Top = 312
  end
  object pmTCDDistrict: TevPopupMenu
    Left = 517
    Top = 365
    object MenuItem2: TMenuItem
      Action = CopyService
    end
  end
  object pmTCDPaymentMethod: TevPopupMenu
    Left = 557
    Top = 365
    object MenuItem4: TMenuItem
      Action = CopyService
    end
  end
end
