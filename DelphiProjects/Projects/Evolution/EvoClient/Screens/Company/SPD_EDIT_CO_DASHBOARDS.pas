// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_DASHBOARDS;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, StdCtrls, ExtCtrls,
  DBCtrls, wwdbdatetimepicker, wwdblook, Wwdotdot, Wwdbcomb, Mask,
  wwdbedit, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Controls, Classes,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents, Variants,
  EvDataAccessComponents, SDataDictsystem, SDataDictclient, SDataDicttemp,
  Forms, SPD_MiniNavigationFrame, SPackageEntry, EvConsts, SFieldCodeValues,
  EvExceptions, Menus, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBLookupCombo, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, isUIwwDBDateTimePicker, isUIwwDBEdit,
  SDataDictbureau, EvUIUtils, Dialogs, EvCommonInterfaces, EvDataset,
  isUIEdit, isBaseClasses;

type
  TEDIT_CO_DASHBOARDS = class(TEDIT_CO_BASE)
    tsDashboards: TTabSheet;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    sbEnlistGroups: TScrollBox;
    tsTier: TTabSheet;
    ScrollBox1: TScrollBox;
    fpSummary: TisUIFashionPanel;
    fpCOTier: TisUIFashionPanel;
    fpCOVendorsDetail: TisUIFashionPanel;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    dsSyAnalyticsTier: TevDataSource;
    tsUsers: TTabSheet;
    ScrollBox2: TScrollBox;
    isUIFashionPanel2: TisUIFashionPanel;
    cbAnalyticsTier: TevDBLookupCombo;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evLabel9: TevLabel;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    edtDashboardsMax: TevDBEdit;
    edtTierUserMax: TevDBEdit;
    edtTierLookbackMax: TevDBEdit;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    dsCoDashboardUsers: TevDataSource;
    grdSbDashboards: TevDBGrid;
    btnAddDashboards: TevSpeedButton;
    btnDelDashbords: TevSpeedButton;
    grdCoDashboards: TevDBGrid;
    dsEnabledDashboards: TevDataSource;
    grdAvailableUsers: TevDBGrid;
    btnAddUser: TevSpeedButton;
    btnDeleteUser: TevSpeedButton;
    grdSelectedUser: TevDBGrid;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    grdSelectedDashboards: TevDBGrid;
    evLabel3: TevLabel;
    dsUsers: TevDataSource;
    evLabel4: TevLabel;
    edtTierDefault: TevEdit;
    edtTierUserDefault: TevEdit;
    edtLookbackDefault: TevEdit;
    cdRemoteUsers: TevClientDataSet;
    cdSbEnabledDashboards: TevClientDataSet;
    cdSbEnabledDashboardsDESCRIPTION: TStringField;
    cdSbEnabledDashboardsDASHBOARD_LEVEL: TStringField;
    cdSbEnabledDashboardsDASHBOARD_NBR: TIntegerField;
    cdRemoteUsersSB_USER_NBR: TIntegerField;
    cdRemoteUsersUSER_ID: TStringField;
    cdRemoteUsersLAST_NAME: TStringField;
    cdRemoteUsersFIRST_NAME: TStringField;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure btnAddDashboardsClick(Sender: TObject);
    procedure btnDelDashbordsClick(Sender: TObject);
    procedure btnAddUserClick(Sender: TObject);
    procedure btnDeleteUserClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure cbAnalyticsTierChange(Sender: TObject);
  private
      { Private declarations }
    FSBAnalytics: boolean;
    FUserFilter: String;
    tmpUCount: integer;    
    procedure SetDefaultValues;
    procedure SetDashboardUserFilter;
    procedure SetSbEnabledDashboardFilter;
    procedure GetUserList;
    function GetUserCLList:string;
    procedure CreatecdSbEnabledDashboards;
  protected
    function GetIfReadOnly: Boolean; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick( Kind: Integer ); override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;


implementation

{$R *.DFM}

uses EvUtils, SysUtils, SFrameEntry;

function TEDIT_CO_DASHBOARDS.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;

  if (not Result) then
    result:= FSBAnalytics;

  if not Result then
    if DM_COMPANY.CO.Active then
      Result := DM_COMPANY.CO.FieldByName('ENABLE_ANALYTICS').AsString[1] = GROUP_BOX_NO;
end;

procedure TEDIT_CO_DASHBOARDS.Activate;
begin
  inherited;
  FUserFilter:= GetUserCLList;
end;

procedure TEDIT_CO_DASHBOARDS.Deactivate;
begin
  inherited;
  PageControl1.ActivePage := TabSheet1;
end;

function TEDIT_CO_DASHBOARDS.GetUserCLList:string;
 var
   s: string;
   Q: IEvQuery;
   cList: IisStringList;
begin
  result:= 'SB_USER_NBR in ( -1) ';

  s:=   'select SB_USER_NBR from SB_SEC_CLIENTS ' +
            'where {AsOfNow<SB_SEC_CLIENTS>} and CL_NBR = :CLNbr and SB_SEC_GROUPS_NBR is null ' +
        'union ' +
        'select SB_USER_NBR from SB_SEC_GROUP_MEMBERS ' +
            'where {AsOfNow<SB_SEC_GROUP_MEMBERS>} and ' +
              'SB_SEC_GROUPS_NBR in ( ' +
                         'select SB_SEC_GROUPS_NBR from  SB_SEC_CLIENTS ' +
                              'where {AsOfNow<SB_SEC_CLIENTS>} and  CL_NBR = :CLNbr and SB_USER_NBR is null) ';
  Q := TevQuery.Create(s);
  Q.Params.AddValue('CLNbr', ctx_DataAccess.ClientID);
  Q.Execute;
  if Q.Result.Fields[0].AsInteger > 0 then
  begin
    cList:= Q.Result.GetColumnValues('SB_USER_NBR');
    result:= BuildINsqlStatement('SB_USER_NBR', cList.CommaText );
  end;
end;

procedure TEDIT_CO_DASHBOARDS.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;

 (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
 (Owner as TFramePackageTmpl).btnNavDelete.Enabled := false;

end;

procedure TEDIT_CO_DASHBOARDS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
  tmpDASHBOARD_COUNT, tmpUSERS_COUNT,   tmpLOOKBACK_YEARS : integer;

  procedure GetCompanyValues;
  var
     Q: IEvQuery;
     s: string;
  begin

    tmpDASHBOARD_COUNT:= DM_CLIENT.CO.FieldByName('DASHBOARD_COUNT').AsInteger;
    tmpUSERS_COUNT:= DM_CLIENT.CO.FieldByName('USERS_COUNT').AsInteger;
    tmpLOOKBACK_YEARS:= DM_CLIENT.CO.FieldByName('LOOKBACK_YEARS').AsInteger;

    s:= 'select * from SY_ANALYTICS_TIER where {AsOfNow<SY_ANALYTICS_TIER>} and  SY_ANALYTICS_TIER_NBR = ' +
                  IntToStr(DM_CLIENT.CO.FieldByName('SY_ANALYTICS_TIER_NBR').AsInteger);
    Q := TevQuery.Create(s);
    Q.Execute;
    if Q.Result.RecordCount > 0 then
    Begin
      if (tmpDASHBOARD_COUNT = 0) then
          tmpDASHBOARD_COUNT:= Q.Result.FieldByName('DASHBOARD_MAX_COUNT').AsInteger;
      if (tmpUSERS_COUNT = 0) then
          tmpUSERS_COUNT:= Q.Result.FieldByName('USERS_MAX_COUNT').AsInteger;
      if (tmpLOOKBACK_YEARS = 0) then
          tmpLOOKBACK_YEARS:= Q.Result.FieldByName('LOOKBACK_YEARS_MAX').AsInteger;
    end;
  end;

begin
  inherited;
  if (Kind = NavOK) then
  begin
    if wwdsMaster.DataSet.State in [dsEdit] then
    begin
     wwdsMaster.DataSet.UpdateRecord;
     GetCompanyValues;

     if (tmpDASHBOARD_COUNT > DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('DASHBOARD_MAX_COUNT').AsInteger) or
        (tmpUSERS_COUNT > DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('USERS_MAX_COUNT').AsInteger) or
        (tmpLOOKBACK_YEARS > DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('LOOKBACK_YEARS_MAX').AsInteger) then
          raise EUpdateError.CreateHelp('You have exceeded the Maximum Number allowed for this Tier.  Please adjust the number.', IDH_ConsistencyViolation);

     if (DM_CLIENT.CO.FieldByName('DASHBOARD_COUNT').AsInteger > DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('DASHBOARD_DEFAULT_COUNT').AsInteger) or
        (DM_CLIENT.CO.FieldByName('USERS_COUNT').AsInteger > DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('USERS_DEFAULT_COUNT').AsInteger) or
        (DM_CLIENT.CO.FieldByName('LOOKBACK_YEARS').AsInteger > DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('LOOKBACK_YEARS_DEFAULT').AsInteger) then
     begin
          if (EvMessage('There are additional charges for using more than the Default numbers.  Are you sure you want to do this?', mtConfirmation, [mbYes, mbNo]) = mrNo) then
          begin
            Handled:=true;
            exit;
          end
     end;

     if (not DM_CLIENT.CO.FieldByName('DASHBOARD_COUNT').IsNull  and (DM_CLIENT.CO.FieldByName('DASHBOARD_COUNT').AsInteger <= DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('DASHBOARD_DEFAULT_COUNT').AsInteger)) or
        (not DM_CLIENT.CO.FieldByName('USERS_COUNT').IsNull and (DM_CLIENT.CO.FieldByName('USERS_COUNT').AsInteger <= DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('USERS_DEFAULT_COUNT').AsInteger)) or
        (not DM_CLIENT.CO.FieldByName('LOOKBACK_YEARS').IsNull and (DM_CLIENT.CO.FieldByName('LOOKBACK_YEARS').AsInteger <= DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('LOOKBACK_YEARS_DEFAULT').AsInteger)) then
             raise EUpdateError.CreateHelp('Overrides can not be less than or equal to the default values.', IDH_ConsistencyViolation);

     DM_CLIENT.CO_DASHBOARDS.RecordCount

    End;
  end;
  if (Kind = NavCommit) then
  begin
     GetCompanyValues;
     if (DM_CLIENT.CO_DASHBOARDS.RecordCount > tmpDASHBOARD_COUNT) or
        (DashboardUserCount > tmpUSERS_COUNT)  then
          raise EUpdateError.CreateHelp('You have exceeded the Maximum Number allowed for this Tier.  Please adjust the number.', IDH_ConsistencyViolation);

  end;

  if Kind in [NavAbort, NavCommit, NavCancel] then
     tmpUCount:= 0;

end;

procedure TEDIT_CO_DASHBOARDS.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind in [NavAbort] then
  begin
    if PageControl1.ActivePage = tsDashboards then
    Begin
       cdSbEnabledDashboards.DisableControls;
       DM_CLIENT.CO_DASHBOARDS.DisableControls;
       try
         DM_CLIENT.CO_DASHBOARDS.DataRequired(DM_CLIENT.CO_DASHBOARDS.RetrieveCondition);
         CreatecdSbEnabledDashboards;
       finally
         cdSbEnabledDashboards.First;
         DM_CLIENT.CO_DASHBOARDS.First;
         cdSbEnabledDashboards.EnableControls;
         DM_CLIENT.CO_DASHBOARDS.EnableControls;
       end;
    end;

    if PageControl1.ActivePage = tsUsers then
    Begin
       cdRemoteUsers.DisableControls;
       DM_CLIENT.CO_DASHBOARDS_USER.DisableControls;
       try
         DM_CLIENT.CO_DASHBOARDS_USER.UserFiltered := false;
         DM_CLIENT.CO_DASHBOARDS_USER.DataRequired(DM_CLIENT.CO_DASHBOARDS_USER.RetrieveCondition);
         GetUserList;
         SetDashboardUserFilter;
       finally
         cdRemoteUsers.First;
         DM_CLIENT.CO_DASHBOARDS_USER.First;
         cdRemoteUsers.EnableControls;
         DM_CLIENT.CO_DASHBOARDS_USER.EnableControls;
       end;
    end;

  end;
end;

procedure TEDIT_CO_DASHBOARDS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_DASHBOARDS, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_ANALYTICS_TIER, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_DASHBOARDS, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_USER, SupportDataSets, '');

  AddDSWithCheck(DM_CLIENT.CO_DASHBOARDS, EditDataSets, '');
  AddDSWithCheck(DM_CLIENT.CO_DASHBOARDS_USER, EditDataSets, '');
  AddDSWithCheck(DM_CLIENT.CO, EditDataSets, '');

end;

procedure TEDIT_CO_DASHBOARDS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_CLIENT.CO_DASHBOARDS, aDS);
  AddDS(DM_CLIENT.CO_DASHBOARDS_USER, aDS);
  AddDS(DM_CLIENT.CO, aDS);

  DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.UserFiltered:= false;

end;

procedure TEDIT_CO_DASHBOARDS.GetUserList;
var
  s: String;

  procedure CheckDashboardUsers;
   var
     s, uList, sComma: string;
     Q: IEvQuery;
  begin
    s:= 'select distinct a.SB_USER_NBR, CO_DASHBOARDS_USER_NBR from CO_DASHBOARDS_USER  a where {AsOfNow<a>} and  a.CO_NBR = :coNbr ';

    Q := TevQuery.Create(s);
    Q.Params.AddValue('coNbr', IntToStr(DM_CLIENT.CO.FieldByName('CO_NBR').AsInteger));
    Q.Execute;
    if Q.Result.Fields[0].AsInteger > 0 then
    begin
      uList:='';
      Q.Result.First;
      while not Q.Result.Eof do
      begin
        if not cdRemoteUsers.Locate('SB_USER_NBR', Q.Result.FieldValues['SB_USER_NBR'],[]) then
        begin
           if DM_SERVICE_BUREAU.SB_USER.Locate('SB_USER_NBR',Q.Result.FieldValues['SB_USER_NBR'], []) then
           begin
             if pos(DM_SERVICE_BUREAU.SB_USER.FieldByName('USER_ID').AsString, uList) = 0 then
             begin
                uList:= uList + sComma + DM_SERVICE_BUREAU.SB_USER.FieldByName('USER_ID').AsString;
               if uList <> '' then
                 sComma := ', ';
             end;
           end;
        end;
        Q.Result.Next;
      end;

      if uList <> '' then
        if (EvMessage('User ' + uList + ' is no longer available to be attached to a Dashboard.  Remove this User?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
        begin
          Q.Result.First;
          while not Q.Result.Eof do
          begin
            if (not cdRemoteUsers.Locate('SB_USER_NBR', Q.Result.FieldValues['SB_USER_NBR'],[])) and
               DM_CLIENT.CO_DASHBOARDS_USER.Locate('CO_DASHBOARDS_USER_NBR', Q.Result.FieldValues['CO_DASHBOARDS_USER_NBR'],[]) then
              DM_CLIENT.CO_DASHBOARDS_USER.Delete;
            Q.Result.Next;
          end;
          ctx_DataAccess.PostDataSets([DM_CLIENT.CO_DASHBOARDS_USER]);
        end;
    end;
  end;


  procedure RemoveSelectedUsers;
  begin
       DM_CLIENT.CO_DASHBOARDS_USER.First;
       while not DM_CLIENT.CO_DASHBOARDS_USER.Eof do
       begin
        if cdRemoteUsers.Locate('SB_USER_NBR', DM_CLIENT.CO_DASHBOARDS_USER['SB_USER_NBR'],[]) then
             cdRemoteUsers.Delete;
        DM_CLIENT.CO_DASHBOARDS_USER.Next;
       end;
  end;

begin

  DM_CLIENT.CO_DASHBOARDS_USER.DisableControls;
  try
    s := 'select u.SB_USER_NBR, u.USER_ID, u.LAST_NAME, u.FIRST_NAME  from SB_USER u where  {AsOfNow<u>} and ' +
           ' u.ANALYTICS_PERSONNEL <> ''N'' and ' +
           ' ( trim(strcopy(u.user_functions, 0, 8)) <> ''-1'' ) and ' +
           ' (trim(strcopy(user_functions, 0, 8))=''' + IntToStr(ctx_DataAccess.ClientID) + ''' or ' +  FUserFilter + ' )';


  cdRemoteUsers.EmptyDataSet;
  if cdRemoteUsers.FieldDefs.Count = 0 then
     cdRemoteUsers.CreateDataSet;

    with TExecDSWrapper.Create(s) do
        ctx_DataAccess.GetCustomData(cdRemoteUsers, AsVariant);

    CheckDashboardUsers;


    RemoveSelectedUsers;
    cdRemoteUsers.First;
    DM_CLIENT.CO_DASHBOARDS_USER.First;
  finally
    DM_CLIENT.CO_DASHBOARDS_USER.EnableControls;
  end;

end;

procedure TEDIT_CO_DASHBOARDS.CreatecdSbEnabledDashboards;
begin

  cdSbEnabledDashboards.EmptyDataSet;
  if cdSbEnabledDashboards.FieldDefs.Count = 0 then
     cdSbEnabledDashboards.CreateDataSet;

  DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.First;
  while not DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Eof do
  begin
    if not DM_CLIENT.CO_DASHBOARDS.Locate('DASHBOARD_LEVEL;DASHBOARD_NBR',
              VarArrayOf([ DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.fieldByName('DASHBOARD_LEVEL').AsString, DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.FieldByName('DASHBOARD_NBR').AsInteger]),[]) then
    begin
        cdSbEnabledDashboards.Insert;
        cdSbEnabledDashboards.FieldByName('DESCRIPTION').AsString := DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.fieldByName('DESCRIPTION').AsString;
        cdSbEnabledDashboards.FieldByName('DASHBOARD_LEVEL').AsString := DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.fieldByName('DASHBOARD_LEVEL').AsString;
        cdSbEnabledDashboards.FieldByName('DASHBOARD_NBR').AsInteger := DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.fieldByName('DASHBOARD_NBR').AsInteger;
        cdSbEnabledDashboards.post;
    end;
    DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Next;
  end;

end;

procedure TEDIT_CO_DASHBOARDS.AfterDataSetsReopen;
begin
  inherited;

  (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
  (Owner as TFramePackageTmpl).btnNavDelete.Enabled := false;

  DM_SYSTEM_MISC.SY_ANALYTICS_TIER.Locate('SY_ANALYTICS_TIER_NBR',wwdsMaster.DataSet.FieldByName('SY_ANALYTICS_TIER_NBR').AsInteger,[]);
  SetDefaultValues;
  SetSbEnabledDashboardFilter;

  FUserFilter:= GetUserCLList;

  FSBAnalytics:= GetSBAnalyticsLicense;
  if GetIfReadOnly then
     SetReadOnly(True)
  else
    ClearReadOnly;

End;

function TEDIT_CO_DASHBOARDS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_DASHBOARDS;
end;

function TEDIT_CO_DASHBOARDS.GetInsertControl: TWinControl;
begin
  Result := cbAnalyticsTier;
end;

procedure TEDIT_CO_DASHBOARDS.SetSbEnabledDashboardFilter;
begin
  if Assigned(wwdsMaster.DataSet) and wwdsMaster.DataSet.Active and
     DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Active then
  begin
       DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.UserFilter := 'SY_ANALYTICS_TIER_NBR = ' + IntToStr(DM_CLIENT.CO.FieldByName('SY_ANALYTICS_TIER_NBR').AsInteger);
       DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.UserFiltered := True;
       CreatecdSbEnabledDashboards;       
  end;
end;

procedure TEDIT_CO_DASHBOARDS.SetDashboardUserFilter;
begin
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and
     DM_CLIENT.CO_DASHBOARDS_USER.Active then
  begin
       DM_CLIENT.CO_DASHBOARDS_USER.UserFilter := 'CO_DASHBOARDS_NBR = ' + IntToStr(DM_CLIENT.CO_DASHBOARDS.FieldByName('CO_DASHBOARDS_NBR').AsInteger);
       DM_CLIENT.CO_DASHBOARDS_USER.UserFiltered := True;

       btnAddUser.Enabled:= DM_CLIENT.CO_DASHBOARDS.fieldByName('CO_DASHBOARDS_NBR').AsInteger > 0;
       btnDeleteUser.Enabled:= btnAddUser.Enabled;

       GetUserList;
       tmpUCount:= 0;
  end;

end;

procedure TEDIT_CO_DASHBOARDS.SetDefaultValues;
begin
  if cbAnalyticsTier.Value <> '' then
  begin
     edtTierDefault.Text:= DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('DASHBOARD_DEFAULT_COUNT').AsString;
     edtTierUserDefault.Text:= DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('USERS_DEFAULT_COUNT').AsString;
     edtLookbackDefault.Text:= DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('LOOKBACK_YEARS_DEFAULT').AsString;
  end
  else
  begin
   edtTierDefault.Text:= '';
   edtTierUserDefault.Text:= '';
   edtLookbackDefault.Text:= '';
  end;


end;


procedure TEDIT_CO_DASHBOARDS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then
    Exit;

  if PageControl1.ActivePage = tsUsers then
     SetDashboardUserFilter;
end;

procedure TEDIT_CO_DASHBOARDS.PageControl1Change(Sender: TObject);
begin
  inherited;

  if PageControl1.ActivePage = tsDashboards then
     SetSbEnabledDashboardFilter;

  if PageControl1.ActivePage = tsUsers then
     SetDashboardUserFilter;
end;


procedure TEDIT_CO_DASHBOARDS.btnAddDashboardsClick(Sender: TObject);
var
 i, dCount : integer;

begin
  inherited;

  DM_CLIENT.CO_DASHBOARDS.DisableControls;
  try
    if grdSbDashboards.SelectedList.Count > 0 then
    begin
        if wwdsMaster.DataSet.State in [dsEdit] then
          DM_CLIENT.CO.Cancel;

      dCount:= DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('DASHBOARD_DEFAULT_COUNT').AsInteger;

      if DM_CLIENT.CO.FieldByName('DASHBOARD_COUNT').AsInteger > dCount then
          dCount:= DM_CLIENT.CO.FieldByName('DASHBOARD_COUNT').AsInteger;

      for i := grdSbDashboards.SelectedList.Count-1 downto 0 do
      begin
        grdSbDashboards.DataSource.DataSet.GotoBookmark(grdSbDashboards.SelectedList[i]);

        if not DM_CLIENT.CO_DASHBOARDS.Locate('DASHBOARD_LEVEL;DASHBOARD_NBR',
                  VarArrayOf([ cdSbEnabledDashboards.fieldByName('DASHBOARD_LEVEL').AsString, cdSbEnabledDashboards.FieldByName('DASHBOARD_NBR').AsInteger]),[]) then
        begin

         if DM_CLIENT.CO_DASHBOARDS.RecordCount < dCount then
         begin
            DM_CLIENT.CO_DASHBOARDS.Insert;
            DM_CLIENT.CO_DASHBOARDS.FieldByName('DASHBOARD_LEVEL').AsString := cdSbEnabledDashboards.fieldByName('DASHBOARD_LEVEL').AsString;
            DM_CLIENT.CO_DASHBOARDS.FieldByName('DASHBOARD_NBR').AsInteger := cdSbEnabledDashboards.fieldByName('DASHBOARD_NBR').AsInteger;
            DM_CLIENT.CO_DASHBOARDS.Post;

            cdSbEnabledDashboards.Delete;
         end
         else
          raise EUpdateError.CreateHelp('Maximum number of Dashboards have been selected.', IDH_ConsistencyViolation);
        end;
      end;
    end;
   finally
    DM_CLIENT.CO_DASHBOARDS.EnableControls;
    grdSbDashboards.UnselectAll;
   end;

end;

procedure TEDIT_CO_DASHBOARDS.btnDelDashbordsClick(Sender: TObject);
var
 i : integer;
begin
  inherited;

   DM_CLIENT.CO_DASHBOARDS.DisableControls;
   try
     if grdCoDashboards.SelectedList.Count > 0 then
     begin
        DM_CLIENT.CO_DASHBOARDS_USER.UserFiltered := false;

        for i := grdCoDashboards.SelectedList.Count-1 downto 0 do
        begin
          grdCoDashboards.DataSource.DataSet.GotoBookmark(grdCoDashboards.SelectedList[i]);

          if DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Locate('DASHBOARD_LEVEL;DASHBOARD_NBR',
                          VarArrayOf([ DM_CLIENT.CO_DASHBOARDS.FieldByName('DASHBOARD_LEVEL').AsString, DM_CLIENT.CO_DASHBOARDS.FieldByName('DASHBOARD_NBR').AsInteger]),[]) then
          begin
            if DM_CLIENT.CO_DASHBOARDS_USER.Locate('CO_DASHBOARDS_NBR', DM_CLIENT.CO_DASHBOARDS.FieldByName('CO_DASHBOARDS_NBR').AsInteger,[]) then
               raise EUpdateError.CreateHelp('Please remove all users before removing a dashboard.', IDH_ConsistencyViolation);

             cdSbEnabledDashboards.Insert;
             cdSbEnabledDashboards.FieldByName('DESCRIPTION').AsString := DM_CLIENT.CO_DASHBOARDS.FieldByName('DESCRIPTION').AsString;
             cdSbEnabledDashboards.FieldByName('DASHBOARD_LEVEL').AsString := DM_CLIENT.CO_DASHBOARDS.FieldByName('DASHBOARD_LEVEL').AsString;
             cdSbEnabledDashboards.FieldByName('DASHBOARD_NBR').AsInteger := DM_CLIENT.CO_DASHBOARDS.FieldByName('DASHBOARD_NBR').AsInteger;
             cdSbEnabledDashboards.Post;
          end;

          DM_CLIENT.CO_DASHBOARDS.Delete;
        end;
     end;
   finally
     DM_CLIENT.CO_DASHBOARDS.EnableControls;
     grdCoDashboards.UnselectAll;
   end;
end;

procedure TEDIT_CO_DASHBOARDS.btnAddUserClick(Sender: TObject);
var
 i, uCount: integer;
 bUSerExist: Boolean;

  function isUserExist:Boolean;
  var
    Q: IevQuery;
  begin
    inherited;
    Q := TevQuery.Create('  select count(DISTINCT SB_USER_NBR)  from CO_DASHBOARDS_USER where {AsOfNow<CO_DASHBOARDS_USER>} and CO_NBR =:coNbr and SB_USER_NBR=:sbUserNbr ');
    Q.Params.AddValue('coNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
    Q.Params.AddValue('sbUserNbr', grdAvailableUsers.DataSource.DataSet.FieldByName('SB_USER_NBR').AsInteger);
    Q.Execute;
    result := Q.Result.Fields[0].AsInteger > 0;
  end;

begin
  inherited;

  DM_CLIENT.CO_DASHBOARDS_USER.DisableControls;
  try
    if grdAvailableUsers.SelectedList.Count > 0 then
    begin
        if wwdsMaster.DataSet.State in [dsEdit] then
           DM_CLIENT.CO.Cancel;

      uCount:= DM_SYSTEM_MISC.SY_ANALYTICS_TIER.FieldByName('USERS_DEFAULT_COUNT').AsInteger;

      if DM_CLIENT.CO.FieldByName('USERS_COUNT').AsInteger > uCount then
          uCount:= DM_CLIENT.CO.FieldByName('USERS_COUNT').AsInteger;

      for i := grdAvailableUsers.SelectedList.Count-1 downto 0 do
      begin
        grdAvailableUsers.DataSource.DataSet.GotoBookmark(grdAvailableUsers.SelectedList[i]);

        if not DM_CLIENT.CO_DASHBOARDS_USER.Locate('SB_USER_NBR', grdAvailableUsers.DataSource.DataSet.FieldByName('SB_USER_NBR').AsInteger,[]) then
        begin

         bUSerExist:= isUserExist;
         if ((DashboardUserCount + tmpUCount) < uCount) or  bUSerExist then
         begin
          DM_CLIENT.CO_DASHBOARDS_USER.Insert;
          DM_CLIENT.CO_DASHBOARDS_USER.FieldByName('CO_DASHBOARDS_NBR').AsInteger := DM_CLIENT.CO_DASHBOARDS.fieldByName('CO_DASHBOARDS_NBR').AsInteger;
          DM_CLIENT.CO_DASHBOARDS_USER.FieldByName('SB_USER_NBR').AsInteger := grdAvailableUsers.DataSource.DataSet.fieldByName('SB_USER_NBR').AsInteger;
          DM_CLIENT.CO_DASHBOARDS_USER.Post;
          if not bUSerExist then
            inc(tmpUCount);
          cdRemoteUsers.Delete;
         end
         else
          raise EUpdateError.CreateHelp('Maximum number of Users have been selected.', IDH_ConsistencyViolation);
        end;
      end;
    end;
   finally
    DM_CLIENT.CO_DASHBOARDS_USER.EnableControls;
    grdAvailableUsers.UnselectAll;
   end;

end;

procedure TEDIT_CO_DASHBOARDS.btnDeleteUserClick(Sender: TObject);
var
 i: integer;
begin
  inherited;

   DM_CLIENT.CO_DASHBOARDS_USER.DisableControls;
   try
     if grdSelectedUser.SelectedList.Count > 0 then
     begin
        for i := grdSelectedUser.SelectedList.Count-1 downto 0 do
        begin
          grdSelectedUser.DataSource.DataSet.GotoBookmark(grdSelectedUser.SelectedList[i]);

          if DM_SERVICE_BUREAU.SB_USER.Locate('SB_USER_NBR',DM_CLIENT.CO_DASHBOARDS_USER.FieldByName('SB_USER_NBR').AsInteger,[]) then
          begin
             cdRemoteUsers.Insert;
             cdRemoteUsers.FieldByName('SB_USER_NBR').AsInteger := DM_CLIENT.CO_DASHBOARDS_USER.FieldByName('SB_USER_NBR').AsInteger;
             cdRemoteUsers.FieldByName('USER_ID').AsString := DM_SERVICE_BUREAU.SB_USER.FieldByName('USER_ID').AsString;
             cdRemoteUsers.FieldByName('LAST_NAME').AsString := DM_SERVICE_BUREAU.SB_USER.FieldByName('LAST_NAME').AsString;
             cdRemoteUsers.FieldByName('FIRST_NAME').AsString := DM_SERVICE_BUREAU.SB_USER.FieldByName('FIRST_NAME').AsString;
             cdRemoteUsers.Post;
          end;
          DM_CLIENT.CO_DASHBOARDS_USER.Delete;
        end;
     end;
   finally
     DM_CLIENT.CO_DASHBOARDS_USER.EnableControls;
     grdSelectedUser.UnselectAll;
   end;

end;

procedure TEDIT_CO_DASHBOARDS.cbAnalyticsTierChange(Sender: TObject);
begin
  inherited;
  SetDefaultValues;
end;

initialization
  RegisterClass(TEDIT_CO_DASHBOARDS);

end.
