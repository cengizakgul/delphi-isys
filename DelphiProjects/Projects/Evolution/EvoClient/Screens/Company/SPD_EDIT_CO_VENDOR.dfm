inherited EDIT_CO_VENDOR: TEDIT_CO_VENDOR
  Width = 637
  Height = 576
  inherited Panel1: TevPanel
    Width = 637
    inherited pnlFavoriteReport: TevPanel
      Left = 485
    end
    inherited pnlTopLeft: TevPanel
      Width = 485
    end
  end
  inherited PageControl1: TevPageControl
    Width = 637
    Height = 522
    HelpContext = 30502
    ActivePage = tsVendorDetail
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 629
        Height = 493
        inherited pnlBorder: TevPanel
          Width = 625
          Height = 489
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 625
          Height = 489
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 784
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 772
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 424
              Top = 80
              Width = 217
              Height = 312
            end
          end
          inherited Panel3: TevPanel
            Width = 577
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 577
            Height = 382
            inherited Splitter1: TevSplitter
              Height = 378
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 550
              Height = 378
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 500
                Height = 298
                IniAttributes.SectionName = 'TEDIT_CO_VENDOR\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 553
              Width = 20
              Height = 378
            end
          end
        end
      end
    end
    object tsVendors: TTabSheet
      Caption = 'Vendors'
      ImageIndex = 2
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 629
        Height = 493
        Align = alClient
        TabOrder = 0
        object fpCOVendors: TisUIFashionPanel
          Left = 8
          Top = 7
          Width = 449
          Height = 239
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdCoVendors: TevDBGrid
            Left = 12
            Top = 35
            Width = 420
            Height = 181
            DisableThemesInTitle = False
            Selected.Strings = (
              'VENDOR_NAME'#9'80'#9'Vendor Name'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCOVendorsDetail: TisUIFashionPanel
          Left = 8
          Top = 257
          Width = 449
          Height = 78
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Company Vendors'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object edtVendor: TevDBLookupCombo
            Left = 12
            Top = 34
            Width = 416
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'80'#9'NAME'#9'F'
              'LEVEL_DESC'#9'8'#9'LEVEL_DESC'#9'F')
            DataField = 'SB_VENDOR_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SB_VENDOR.SB_VENDOR
            LookupField = 'SB_VENDOR_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
      end
    end
    object tsVendorDetail: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbEnlistGroups: TScrollBox
        Left = 0
        Top = 0
        Width = 629
        Height = 493
        Align = alClient
        TabOrder = 0
        object fpSummary: TisUIFashionPanel
          Left = 8
          Top = 83
          Width = 544
          Height = 258
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgValues: TevDBGrid
            Left = 12
            Top = 35
            Width = 505
            Height = 200
            HelpContext = 17001
            DisableThemesInTitle = False
            Selected.Strings = (
              'SB_DETAIL'#9'20'#9'Vendor Detail'#9'F'
              'DETAIL_VALUES'#9'40'#9'Value'#9'F'
              'SB_DETAIL_VALUES'#9'40'#9'Pre-Defined Value'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_VENDOR\wwdgValues'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsVendorDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCompany: TisUIFashionPanel
          Left = 12
          Top = 350
          Width = 287
          Height = 135
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCompany'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Vendor Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object cbDetail: TevDBLookupCombo
            Left = 16
            Top = 48
            Width = 241
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DETAIL'#9'40'#9'Detail'#9'F'
              'DETAIL_LEVEL_DESC'#9'8'#9'DETAIL_LEVEL_DESC'#9'F')
            DataField = 'SB_VENDOR_DETAIL_NBR'
            DataSource = dsVendorDetail
            LookupTable = DM_SB_VENDOR_DETAIL.SB_VENDOR_DETAIL
            LookupField = 'SB_VENDOR_DETAIL_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnBeforeDropDown = cbDetailBeforeDropDown
            OnCloseUp = cbDetailCloseUp
          end
          inline MiniNavigationFrame: TMiniNavigationFrame
            Left = 20
            Top = 86
            Width = 235
            Height = 25
            TabOrder = 1
            inherited SpeedButton1: TevSpeedButton
              Width = 100
              Caption = 'Create'
              Glyph.Data = {00000000}
            end
            inherited SpeedButton2: TevSpeedButton
              Left = 131
              Width = 100
              Caption = 'Delete'
              Glyph.Data = {00000000}
            end
          end
        end
        object fpDetails: TisUIFashionPanel
          Left = 309
          Top = 350
          Width = 244
          Height = 135
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDetails'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Vendor Detail Value'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label2: TevLabel
            Left = 12
            Top = 35
            Width = 30
            Height = 13
            Caption = 'Value '
            FocusControl = edtValue
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 74
            Width = 125
            Height = 13
            Caption = 'Pre-Defined Value Options'
            FocusControl = cbPreDefine
          end
          object edtValue: TevDBEdit
            Left = 12
            Top = 50
            Width = 210
            Height = 21
            HelpContext = 17001
            DataField = 'DETAIL_VALUES'
            DataSource = dsVendorDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object cbPreDefine: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 210
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DETAIL_VALUES'#9'80'#9'Detail values'#9#9)
            DataField = 'SB_VENDOR_DETAIL_VALUES_NBR'
            DataSource = dsVendorDetail
            LookupTable = DM_SB_VENDOR_DETAIL_VALUES.SB_VENDOR_DETAIL_VALUES
            LookupField = 'SB_VENDOR_DETAIL_VALUES_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnBeforeDropDown = cbPreDefineBeforeDropDown
            OnCloseUp = cbPreDefineCloseUp
          end
        end
        object isUIFashionPanel1: TisUIFashionPanel
          Left = 12
          Top = 11
          Width = 537
          Height = 66
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'Vendor '
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Vendor'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblVendor: TevLabel
            Left = 60
            Top = 33
            Width = 49
            Height = 13
            Caption = 'Vendor  '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 33
            Width = 40
            Height = 13
            Caption = 'Vendor  '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_VENDOR.CO_VENDOR
    OnDataChange = wwdsDetailDataChange
  end
  inherited dsCL: TevDataSource
    Left = 580
    Top = 218
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 512
    Top = 32
  end
  object dsVendorDetail: TevDataSource
    DataSet = DM_CO_VENDOR_DETAIL_VALUES.CO_VENDOR_DETAIL_VALUES
    OnStateChange = dsVendorDetailStateChange
    Left = 632
    Top = 112
  end
end
