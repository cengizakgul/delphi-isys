inherited EDIT_CO_TEAM_PR_BATCH_DEFLT_ED: TEDIT_CO_TEAM_PR_BATCH_DEFLT_ED
  inherited Panel1: TevPanel
    inherited Label2: TevLabel
      Left = 578
      Width = 27
      Caption = 'Team'
    end
    inherited DBText2: TevDBText
      Left = 578
      Width = 233
      DataSource = wwdsSubMaster4
    end
    object evLabel1: TevLabel [2]
      Left = 544
      Top = 17
      Width = 27
      Height = 13
      Caption = 'Team'
    end
    inherited pnlTopLeft: TevPanel
      inherited pnlUser: TevPanel
        Left = 382
        Width = 19
        inherited dbtxUserLast: TevDBText
          Left = 24
        end
      end
    end
  end
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited pnlSubbrowse: TevPanel
              Left = 307
              Width = 473
              inherited Panel4: TevPanel
                Width = 473
                inherited Panel5: TevPanel
                  Width = 473
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 441
                IniAttributes.SectionName = 'TEDIT_CO_TEAM_PR_BATCH_DEFLT_ED\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Team'
              inherited Splitter2: TevSplitter
                Left = 405
                Height = 441
              end
              inherited Splitter3: TevSplitter
                Left = 98
                Height = 441
              end
              object Splitter4: TevSplitter [2]
                Left = 181
                Top = 48
                Height = 441
              end
              inherited wwDBGrid1: TevDBGrid
                Width = 80
                Height = 441
                IniAttributes.SectionName = 'TEDIT_CO_TEAM_PR_BATCH_DEFLT_ED\wwDBGrid1'
              end
              inherited wwDBGrid3: TevDBGrid
                Left = 101
                Width = 80
                Height = 441
                IniAttributes.SectionName = 'TEDIT_CO_TEAM_PR_BATCH_DEFLT_ED\wwDBGrid3'
              end
              inherited wwDBGrid4: TevDBGrid
                Left = 184
                Height = 441
                IniAttributes.SectionName = 'TEDIT_CO_TEAM_PR_BATCH_DEFLT_ED\wwDBGrid4'
                Align = alLeft
              end
              object wwDBGrid5: TevDBGrid
                Left = 408
                Top = 48
                Width = 173
                Height = 441
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'TEAM')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_TEAM_PR_BATCH_DEFLT_ED\wwDBGrid5'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster4
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 3
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited sbDivisionDetail: TScrollBox
        inherited fpSummary: TisUIFashionPanel
          inherited wwDBGrid2: TevDBGrid
            IniAttributes.SectionName = 'TEDIT_CO_TEAM_PR_BATCH_DEFLT_ED\wwDBGrid2'
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = wwdsSubMaster4
  end
  object wwdsSubMaster4: TevDataSource
    DataSet = DM_CO_TEAM.CO_TEAM
    MasterDataSource = wwdsSubMaster3
    Left = 144
  end
end
