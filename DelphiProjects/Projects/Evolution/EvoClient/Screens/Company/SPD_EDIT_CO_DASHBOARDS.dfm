inherited EDIT_CO_DASHBOARDS: TEDIT_CO_DASHBOARDS
  Width = 854
  Height = 613
  inherited Panel1: TevPanel
    Width = 854
    inherited pnlFavoriteReport: TevPanel
      Left = 702
    end
    inherited pnlTopLeft: TevPanel
      Width = 702
    end
  end
  inherited PageControl1: TevPageControl
    Width = 854
    Height = 559
    HelpContext = 30502
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 846
        Height = 530
        inherited pnlBorder: TevPanel
          Width = 842
          Height = 526
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 842
          Height = 526
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 784
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 772
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 424
              Top = 80
              Width = 217
              Height = 312
            end
          end
          inherited Panel3: TevPanel
            Width = 794
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 794
            Height = 419
            inherited Splitter1: TevSplitter
              Height = 415
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 767
              Height = 415
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 717
                Height = 335
                IniAttributes.SectionName = 'TEDIT_CO_DASHBOARDS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 770
              Width = 20
              Height = 415
            end
          end
        end
      end
    end
    object tsTier: TTabSheet
      Caption = 'Tier '
      ImageIndex = 2
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 846
        Height = 530
        Align = alClient
        TabOrder = 0
        object fpCOTier: TisUIFashionPanel
          Left = 8
          Top = 12
          Width = 400
          Height = 78
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Tier '
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object cbAnalyticsTier: TevDBLookupCombo
            Left = 12
            Top = 35
            Width = 365
            Height = 21
            HelpContext = 16501
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'TIER_ID'#9'40'#9'Tier id'#9'F')
            DataField = 'SY_ANALYTICS_TIER_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_SY_ANALYTICS_TIER.SY_ANALYTICS_TIER
            LookupField = 'SY_ANALYTICS_TIER_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = cbAnalyticsTierChange
          end
        end
        object fpCOVendorsDetail: TisUIFashionPanel
          Left = 8
          Top = 99
          Width = 400
          Height = 153
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Settings'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel7: TevLabel
            Left = 12
            Top = 51
            Width = 60
            Height = 13
            Caption = 'Dashboards '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel8: TevLabel
            Left = 12
            Top = 79
            Width = 30
            Height = 13
            Caption = 'Users '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel9: TevLabel
            Left = 12
            Top = 108
            Width = 81
            Height = 13
            Caption = 'Lookback Years '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel10: TevLabel
            Left = 124
            Top = 35
            Width = 37
            Height = 13
            Caption = 'Default '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel11: TevLabel
            Left = 228
            Top = 35
            Width = 48
            Height = 13
            Caption = 'Overrides '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edtDashboardsMax: TevDBEdit
            Left = 212
            Top = 51
            Width = 89
            Height = 21
            HelpContext = 17001
            DataField = 'DASHBOARD_COUNT'
            DataSource = wwdsMaster
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edtTierUserMax: TevDBEdit
            Left = 212
            Top = 79
            Width = 89
            Height = 21
            HelpContext = 17001
            DataField = 'USERS_COUNT'
            DataSource = wwdsMaster
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edtTierLookbackMax: TevDBEdit
            Left = 212
            Top = 107
            Width = 89
            Height = 21
            HelpContext = 17001
            DataField = 'LOOKBACK_YEARS'
            DataSource = wwdsMaster
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edtTierDefault: TevEdit
            Left = 108
            Top = 51
            Width = 89
            Height = 21
            Enabled = False
            TabOrder = 3
          end
          object edtTierUserDefault: TevEdit
            Left = 108
            Top = 79
            Width = 89
            Height = 21
            Enabled = False
            TabOrder = 4
          end
          object edtLookbackDefault: TevEdit
            Left = 108
            Top = 107
            Width = 89
            Height = 21
            Enabled = False
            TabOrder = 5
          end
        end
      end
    end
    object tsDashboards: TTabSheet
      Caption = 'Dashboards '
      ImageIndex = 2
      object sbEnlistGroups: TScrollBox
        Left = 0
        Top = 0
        Width = 846
        Height = 530
        Align = alClient
        TabOrder = 0
        object fpSummary: TisUIFashionPanel
          Left = 8
          Top = 12
          Width = 799
          Height = 439
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object btnAddDashboards: TevSpeedButton
            Left = 351
            Top = 170
            Width = 85
            Height = 22
            Caption = 'Add'
            HideHint = True
            AutoSize = False
            OnClick = btnAddDashboardsClick
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
              DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
              DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
              78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
              8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
              F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
              F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
              DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
              DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
            Layout = blGlyphRight
            ParentColor = False
            ShortCut = 0
          end
          object btnDelDashbords: TevSpeedButton
            Left = 351
            Top = 243
            Width = 85
            Height = 22
            Caption = 'Remove'
            HideHint = True
            AutoSize = False
            OnClick = btnDelDashbordsClick
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
              DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
              DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
              78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
              448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
              F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
              F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
              44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
              DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
            ParentColor = False
            ShortCut = 0
          end
          object evLabel1: TevLabel
            Left = 28
            Top = 43
            Width = 106
            Height = 13
            Caption = 'Available Dashboards '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 468
            Top = 43
            Width = 108
            Height = 13
            Caption = 'Selected Dashboards  '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object grdSbDashboards: TevDBGrid
            Left = 12
            Top = 67
            Width = 330
            Height = 342
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'80'#9'Description'#9'F'
              'LEVEL_DESC'#9'8'#9'Level Desc'#9'F'
              'DASHBOARD_LEVEL'#9'1'#9'Dashboard Level'#9'F'
              'DASHBOARD_NBR'#9'10'#9'Dashboard Nbr'#9'F'
              'SB_ENABLED_DASHBOARDS_NBR'#9'10'#9'Sb Enabled Dashboards Nbr'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_HR_POSITIONS\evGridSelected'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsEnabledDashboards
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object grdCoDashboards: TevDBGrid
            Left = 447
            Top = 64
            Width = 330
            Height = 345
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'80'#9'Description'#9'F'
              'LEVEL_DESC'#9'8'#9'Level Desc'#9'F'
              'CO_DASHBOARDS_NBR'#9'10'#9'Co dashboards nbr'#9'F'
              'CO_NBR'#9'10'#9'Co nbr'#9'F'
              'DASHBOARD_LEVEL'#9'1'#9'Dashboard level'#9'F'
              'DASHBOARD_NBR'#9'10'#9'Dashboard nbr'#9'F'
              'EFFECTIVE_DATE'#9'10'#9'Effective date'#9'F'
              'EFFECTIVE_UNTIL'#9'10'#9'Effective until'#9'F'
              'REC_VERSION'#9'10'#9'Rec version'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_DASHBOARDS\grdCoDashboards'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
    object tsUsers: TTabSheet
      Caption = 'Users '
      ImageIndex = 3
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 846
        Height = 530
        Align = alClient
        TabOrder = 0
        object isUIFashionPanel2: TisUIFashionPanel
          Left = 8
          Top = 12
          Width = 617
          Height = 528
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Selected Dashboards'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object btnAddUser: TevSpeedButton
            Left = 255
            Top = 307
            Width = 85
            Height = 22
            Caption = 'Add'
            HideHint = True
            AutoSize = False
            OnClick = btnAddUserClick
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
              DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
              DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
              78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
              8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
              F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
              F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
              DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
              DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
            Layout = blGlyphRight
            ParentColor = False
            ShortCut = 0
          end
          object btnDeleteUser: TevSpeedButton
            Left = 255
            Top = 355
            Width = 85
            Height = 22
            Caption = 'Remove'
            HideHint = True
            AutoSize = False
            OnClick = btnDeleteUserClick
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
              DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
              DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
              78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
              448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
              F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
              F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
              44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
              DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
            ParentColor = False
            ShortCut = 0
          end
          object evLabel3: TevLabel
            Left = 28
            Top = 195
            Width = 76
            Height = 13
            Caption = 'Available Users '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 372
            Top = 195
            Width = 72
            Height = 13
            Caption = 'Selected Users'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object grdAvailableUsers: TevDBGrid
            Left = 12
            Top = 219
            Width = 227
            Height = 282
            DisableThemesInTitle = False
            Selected.Strings = (
              'USER_ID'#9'128'#9'User id'#9'F'
              'FIRST_NAME'#9'20'#9'First name'#9'F'
              'LAST_NAME'#9'30'#9'Last name'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_HR_POSITIONS\evGridSelected'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsUsers
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object grdSelectedUser: TevDBGrid
            Left = 354
            Top = 216
            Width = 232
            Height = 285
            DisableThemesInTitle = False
            Selected.Strings = (
              'USER_ID'#9'128'#9'User Id'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_DASHBOARDS\grdSelectedUser'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCoDashboardUsers
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object grdSelectedDashboards: TevDBGrid
            Left = 12
            Top = 35
            Width = 457
            Height = 152
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'80'#9'Description'#9'F'
              'LEVEL_DESC'#9'8'#9'Level Desc'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_HR_POSITIONS\evGridSelected'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 2
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_DASHBOARDS.CO_DASHBOARDS
    OnDataChange = wwdsDetailDataChange
  end
  inherited dsCL: TevDataSource
    Left = 636
    Top = 146
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 512
    Top = 32
  end
  object dsSyAnalyticsTier: TevDataSource
    DataSet = DM_SY_ANALYTICS_TIER.SY_ANALYTICS_TIER
    Left = 800
    Top = 160
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 544
    Top = 24
  end
  object dsCoDashboardUsers: TevDataSource
    DataSet = DM_CO_DASHBOARDS_USER.CO_DASHBOARDS_USER
    Left = 784
    Top = 296
  end
  object dsEnabledDashboards: TevDataSource
    DataSet = cdSbEnabledDashboards
    Left = 708
    Top = 386
  end
  object dsUsers: TevDataSource
    DataSet = cdRemoteUsers
    Left = 784
    Top = 344
  end
  object cdRemoteUsers: TevClientDataSet
    Left = 560
    Top = 248
    object cdRemoteUsersSB_USER_NBR: TIntegerField
      FieldName = 'SB_USER_NBR'
    end
    object cdRemoteUsersUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 128
    end
    object cdRemoteUsersLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Size = 30
    end
    object cdRemoteUsersFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
    end
  end
  object cdSbEnabledDashboards: TevClientDataSet
    Left = 560
    Top = 288
    object cdSbEnabledDashboardsDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 80
    end
    object cdSbEnabledDashboardsDASHBOARD_LEVEL: TStringField
      FieldName = 'DASHBOARD_LEVEL'
      Size = 1
    end
    object cdSbEnabledDashboardsDASHBOARD_NBR: TIntegerField
      FieldName = 'DASHBOARD_NBR'
    end
  end
end
