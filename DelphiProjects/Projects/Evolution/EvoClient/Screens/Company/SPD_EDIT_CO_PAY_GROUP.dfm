inherited EDIT_CO_PAY_GROUP: TEDIT_CO_PAY_GROUP
  Width = 761
  Height = 588
  inherited Panel1: TevPanel
    Width = 761
    inherited pnlFavoriteReport: TevPanel
      Left = 609
    end
    inherited pnlTopLeft: TevPanel
      Width = 609
    end
  end
  inherited PageControl1: TevPageControl
    Width = 761
    Height = 534
    HelpContext = 40501
    ActivePage = tshtPay_Groups
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 753
        Height = 505
        inherited pnlBorder: TevPanel
          Width = 749
          Height = 501
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 749
          Height = 501
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 784
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 772
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 491
              Top = 155
              Width = 270
              Height = 310
            end
          end
          inherited Panel3: TevPanel
            Width = 701
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 701
            Height = 394
            inherited Splitter1: TevSplitter
              Height = 390
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 113
              Height = 390
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 63
                Height = 310
                IniAttributes.SectionName = 'TEDIT_CO_PAY_GROUP\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 116
              Width = 581
              Height = 390
            end
          end
        end
      end
    end
    object tshtPay_Groups: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbPayGroupsDetail: TScrollBox
        Left = 0
        Top = 0
        Width = 753
        Height = 505
        Align = alClient
        TabOrder = 0
        object fpCOPayGroupSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 318
          Height = 377
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgPay_Groups: TevDBGrid
            Left = 12
            Top = 35
            Width = 285
            Height = 321
            TabStop = False
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'41'#9'Pay Group Description'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_PAY_GROUP\wwdgPay_Groups'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCOPayGroupDetails: TisUIFashionPanel
          Left = 8
          Top = 393
          Width = 318
          Height = 92
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Pay Group Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablDescription: TevLabel
            Left = 8
            Top = 35
            Width = 65
            Height = 13
            Caption = '~Description'
            FocusControl = dedtDescription
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtDescription: TevDBEdit
            Left = 8
            Top = 50
            Width = 289
            Height = 21
            DataField = 'DESCRIPTION'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
        end
      end
    end
    object tsVMR: TTabSheet
      Caption = 'Mail Room'
      ImageIndex = 3
      object isUIFashionPanel2: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 378
        Height = 209
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'pnlSummary'
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Mail Box Overrides'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object evLabel20: TevLabel
          Left = 12
          Top = 35
          Width = 65
          Height = 13
          Caption = 'Payroll Check'
        end
        object evLabel1: TevLabel
          Left = 12
          Top = 74
          Width = 49
          Height = 13
          Caption = 'EE Report'
        end
        object evLabel2: TevLabel
          Left = 12
          Top = 113
          Width = 70
          Height = 13
          Caption = '2nd EE Report'
        end
        object evLabel25: TevLabel
          Left = 12
          Top = 152
          Width = 49
          Height = 13
          Caption = 'EE Return'
        end
        object evDBLookupCombo7: TevDBLookupCombo
          Left = 12
          Top = 167
          Width = 345
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DESCRIPTION'#9'20'#9'Description'#9'F'
            'luServiceName'#9'10'#9'Service Name'#9'F'
            'luMethodName'#9'10'#9'Method Name'#9'F'
            'luMediaName'#9'20'#9'Media Name'#9'F')
          DataField = 'TAX_EE_RETURN_MB_GROUP_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
          LookupField = 'CL_MAIL_BOX_GROUP_NBR'
          Options = [loColLines, loTitles]
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object evDBLookupCombo5: TevDBLookupCombo
          Left = 12
          Top = 128
          Width = 345
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DESCRIPTION'#9'20'#9'Description'#9'F'
            'luServiceName'#9'10'#9'Service Name'#9'F'
            'luMethodName'#9'10'#9'Method Name'#9'F'
            'luMediaName'#9'20'#9'Media Name'#9'F')
          DataField = 'PR_EE_REPORT_SEC_MB_GROUP_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
          LookupField = 'CL_MAIL_BOX_GROUP_NBR'
          Options = [loColLines, loTitles]
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object evDBLookupCombo1: TevDBLookupCombo
          Left = 12
          Top = 89
          Width = 345
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DESCRIPTION'#9'20'#9'Description'#9'F'
            'luServiceName'#9'10'#9'Service Name'#9'F'
            'luMethodName'#9'10'#9'Method Name'#9'F'
            'luMediaName'#9'20'#9'Media Name'#9'F')
          DataField = 'PR_EE_REPORT_MB_GROUP_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
          LookupField = 'CL_MAIL_BOX_GROUP_NBR'
          Options = [loColLines, loTitles]
          Style = csDropDownList
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object evDBLookupCombo2: TevDBLookupCombo
          Left = 12
          Top = 50
          Width = 345
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DESCRIPTION'#9'20'#9'Description'#9'F'
            'luServiceName'#9'10'#9'Service Name'#9'F'
            'luMethodName'#9'10'#9'Method Name'#9'F'
            'luMediaName'#9'20'#9'Media Name'#9'F')
          DataField = 'PR_CHECK_MB_GROUP_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
          LookupField = 'CL_MAIL_BOX_GROUP_NBR'
          Options = [loColLines, loTitles]
          Style = csDropDownList
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 472
    Top = 90
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_PAY_GROUP.CO_PAY_GROUP
  end
end
