inherited EDIT_CO: TEDIT_CO
  Width = 876
  Height = 684
  object evLabel3: TevLabel [0]
    Left = 464
    Top = 168
    Width = 67
    Height = 16
    Caption = '~Status Code'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  inherited Panel1: TevPanel
    Width = 876
    inherited pnlFavoriteReport: TevPanel
      Left = 724
    end
    inherited pnlTopLeft: TevPanel
      Width = 724
    end
  end
  inherited PageControl1: TevPageControl
    Width = 876
    Height = 630
    HelpContext = 10656
    MultiLine = True
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 868
        Height = 582
        inherited pnlBorder: TevPanel
          Width = 864
          Height = 578
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 864
          Height = 578
          inherited pnlFashionBody: TevPanel
            inherited pnlSubbrowse: TevPanel
              Width = 400
              Height = 523
            end
          end
          inherited Panel3: TevPanel
            Width = 816
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 816
            Height = 471
            inherited Splitter1: TevSplitter
              Height = 467
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 789
              Height = 467
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 739
                Height = 387
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
                  'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
                  'FEIN'#9'9'#9'EIN'#9
                  'DBA'#9'40'#9'DBA '#9'F'
                  'UserFirstName'#9'20'#9'CSR First Name'#9'F'
                  'UserLastName'#9'30'#9'CSR Last Name'#9'F'
                  'HomeState'#9'2'#9'Home State'#9'F'
                  'CO_NBR'#9'10'#9'Internal CO#'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 792
              Width = 20
              Height = 467
            end
          end
        end
      end
    end
    object tshtAddress: TTabSheet
      Caption = 'Address/Calendar Setup'
      ImageIndex = 40
      object sbCompanyAddress: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpAddress: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 302
          Height = 380
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Company'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablCompany_Code: TevLabel
            Left = 12
            Top = 35
            Width = 81
            Height = 16
            Caption = '~Company Code'
            FocusControl = dedtCompany_Code
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablName: TevLabel
            Left = 12
            Top = 74
            Width = 37
            Height = 16
            Caption = '~Name'
            FocusControl = dedtName
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDBA: TevLabel
            Left = 12
            Top = 113
            Width = 22
            Height = 13
            Caption = 'DBA'
            FocusControl = dedtDBA
          end
          object lablAddress1: TevLabel
            Left = 12
            Top = 152
            Width = 56
            Height = 16
            Caption = '~Address 1'
            FocusControl = dedtAddress_1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablAddress_2: TevLabel
            Left = 12
            Top = 191
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = dedtAddress_2
          end
          object lablCity: TevLabel
            Left = 12
            Top = 230
            Width = 26
            Height = 16
            Caption = '~City'
            FocusControl = dedtCity
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablState: TevLabel
            Left = 149
            Top = 230
            Width = 34
            Height = 16
            Caption = '~State'
            FocusControl = dedtState
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablZip_Code: TevLabel
            Left = 197
            Top = 230
            Width = 24
            Height = 16
            Caption = '~Zip'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablCounty: TevLabel
            Left = 12
            Top = 269
            Width = 33
            Height = 13
            Caption = 'County'
            FocusControl = dedtCounty
          end
          object lablE_Mail_Address: TevLabel
            Left = 12
            Top = 322
            Width = 28
            Height = 13
            Caption = 'E-mail'
            FocusControl = dedtE_Mail_Address
          end
          object EvBevel3: TEvBevel
            Left = 12
            Top = 315
            Width = 266
            Height = 3
            Shape = bsTopLine
          end
          object dedtCompany_Code: TevDBEdit
            Left = 12
            Top = 51
            Width = 106
            Height = 21
            HelpContext = 10502
            DataField = 'CUSTOM_COMPANY_NUMBER'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtName: TevDBEdit
            Left = 12
            Top = 89
            Width = 266
            Height = 21
            HelpContext = 10503
            DataField = 'NAME'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object dedtDBA: TevDBEdit
            Left = 12
            Top = 128
            Width = 266
            Height = 21
            HelpContext = 10504
            DataField = 'DBA'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object dedtAddress_1: TevDBEdit
            Left = 12
            Top = 167
            Width = 266
            Height = 21
            HelpContext = 10659
            DataField = 'ADDRESS1'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object dedtAddress_2: TevDBEdit
            Left = 12
            Top = 206
            Width = 266
            Height = 21
            HelpContext = 10659
            DataField = 'ADDRESS2'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object dedtCity: TevDBEdit
            Left = 12
            Top = 245
            Width = 129
            Height = 21
            HelpContext = 10659
            DataField = 'CITY'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object dedtState: TevDBEdit
            Left = 149
            Top = 245
            Width = 40
            Height = 21
            HelpContext = 10659
            CharCase = ecUpperCase
            DataField = 'STATE'
            DataSource = wwdsMaster
            Picture.PictureMask = '*{&,@}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object dedtCounty: TevDBEdit
            Left = 12
            Top = 284
            Width = 266
            Height = 21
            HelpContext = 10660
            DataField = 'COUNTY'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtE_Mail_Address: TevDBEdit
            Left = 12
            Top = 337
            Width = 266
            Height = 21
            HelpContext = 10661
            DataField = 'E_MAIL_ADDRESS'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 10
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeZip_Code: TevDBEdit
            Left = 197
            Top = 245
            Width = 81
            Height = 21
            HelpContext = 10659
            DataField = 'ZIP_CODE'
            DataSource = wwdsMaster
            Picture.PictureMask = '*5{#,?}[-*4{#,?}]'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object evBitBtn2: TevBitBtn
            Left = 122
            Top = 51
            Width = 159
            Height = 21
            Caption = 'Get Client Name and Address '
            TabOrder = 1
            OnClick = evBitBtn2Click
            Color = clBlack
            NumGlyphs = 2
            Margin = 0
          end
        end
        object fpLegal: TisUIFashionPanel
          Left = 322
          Top = 8
          Width = 302
          Height = 212
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Legal Address'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablLegal_Name: TevLabel
            Left = 12
            Top = 35
            Width = 28
            Height = 13
            Caption = 'Name'
            FocusControl = dedtLegal_Name
          end
          object lablLegal_Address_1: TevLabel
            Left = 12
            Top = 74
            Width = 47
            Height = 13
            Caption = 'Address 1'
            FocusControl = dedtLegal_Address_1
          end
          object lablLegal_Address_2: TevLabel
            Left = 12
            Top = 113
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = dedtLegal_Address_2
          end
          object lablLegal_City: TevLabel
            Left = 12
            Top = 152
            Width = 17
            Height = 13
            Caption = 'City'
            FocusControl = dedtLegal_City
          end
          object lablLegal_State: TevLabel
            Left = 149
            Top = 152
            Width = 25
            Height = 13
            Caption = 'State'
            FocusControl = dedtLegal_State
          end
          object lablLegal_Zip_Code: TevLabel
            Left = 197
            Top = 152
            Width = 15
            Height = 13
            Caption = 'Zip'
          end
          object dedtLegal_Name: TevDBEdit
            Left = 12
            Top = 50
            Width = 266
            Height = 21
            HelpContext = 10505
            DataField = 'LEGAL_NAME'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtLegal_Address_1: TevDBEdit
            Left = 12
            Top = 89
            Width = 266
            Height = 21
            HelpContext = 10662
            DataField = 'LEGAL_ADDRESS1'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtLegal_Address_2: TevDBEdit
            Left = 12
            Top = 128
            Width = 266
            Height = 21
            HelpContext = 10662
            DataField = 'LEGAL_ADDRESS2'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtLegal_City: TevDBEdit
            Left = 12
            Top = 167
            Width = 129
            Height = 21
            HelpContext = 10662
            DataField = 'LEGAL_CITY'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtLegal_State: TevDBEdit
            Left = 149
            Top = 167
            Width = 40
            Height = 21
            HelpContext = 10662
            CharCase = ecUpperCase
            DataField = 'LEGAL_STATE'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&,@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeLegal_Zip_Code: TevDBEdit
            Left = 197
            Top = 167
            Width = 81
            Height = 21
            HelpContext = 10662
            DataField = 'LEGAL_ZIP_CODE'
            DataSource = wwdsMaster
            Picture.PictureMask = '*5{#}[-*4#]'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpNavigation: TisUIFashionPanel
          Left = 8
          Top = 396
          Width = 302
          Height = 83
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpNavigation'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Navigation'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evBitBtn3: TevBitBtn
            Left = 12
            Top = 35
            Width = 129
            Height = 25
            Caption = 'Calendar Setup'
            TabOrder = 0
            OnClick = Button1Click
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
              5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
              A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
              5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
              FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
              B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
              6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
              FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
              B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
              64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
              CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
              B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
              5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
              00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
              BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
              57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
              00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
              BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
              82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
              60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
              BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
              83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
              00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
              C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
              4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
              FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
              C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
              4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
              FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
              C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
              49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
              FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
              CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
              45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
              FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
              DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
              3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
              FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
              EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
              8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
            NumGlyphs = 2
            Margin = 0
          end
          object evBitBtn1: TevBitBtn
            Left = 150
            Top = 35
            Width = 129
            Height = 25
            Caption = 'Contacts'
            TabOrder = 1
            OnClick = evBitBtn1Click
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
              5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
              A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
              5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
              FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
              B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
              6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
              FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
              B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
              64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
              CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
              B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
              5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
              00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
              BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
              57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
              00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
              BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
              82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
              60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
              BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
              83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
              00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
              C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
              4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
              FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
              C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
              4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
              FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
              C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
              49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
              FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
              CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
              45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
              FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
              DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
              3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
              FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
              EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
              8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object fpStatus: TisUIFashionPanel
          Left = 322
          Top = 228
          Width = 302
          Height = 251
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpStatus'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Status'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablStart_Date: TevLabel
            Left = 12
            Top = 35
            Width = 57
            Height = 16
            Caption = '~Start Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablTermination_Date: TevLabel
            Left = 12
            Top = 74
            Width = 53
            Height = 13
            Caption = 'Terminated'
          end
          object lablStatus_Code: TevLabel
            Left = 149
            Top = 35
            Width = 67
            Height = 16
            Caption = '~Status Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablTermination_Notes: TevLabel
            Left = 12
            Top = 113
            Width = 86
            Height = 13
            Caption = 'Termination Notes'
          end
          object evLabel13: TevLabel
            Left = 149
            Top = 74
            Width = 65
            Height = 13
            Caption = 'Other Service'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblSetupCompleted: TevLabel
            Left = 12
            Top = 192
            Width = 93
            Height = 16
            Caption = '~Setup Completed '
          end
          object dmemTermination_Notes: TEvDBMemo
            Left = 12
            Top = 128
            Width = 266
            Height = 57
            HelpContext = 10515
            DataField = 'TERMINATION_NOTES'
            DataSource = wwdsMaster
            ScrollBars = ssVertical
            TabOrder = 4
          end
          object wwcbStatus_Code: TevDBComboBox
            Left = 149
            Top = 50
            Width = 129
            Height = 21
            HelpContext = 10514
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TERMINATION_CODE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'ACTIVE'#9'A'
              'IN ACTIVE'#9'N'
              'OUT OF BUS'#9'D'
              'OTHER SERVICE'#9'L'
              'SEASONAL ACTIVE'#9'C'
              'SEASONAL INACTIVE'#9'S'
              'IN HOUSE'#9'I'
              'SOLD'#9'O')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
            OnChange = wwcbStatus_CodeChange
          end
          object wwdpStart_Date: TevDBDateTimePicker
            Left = 12
            Top = 50
            Width = 129
            Height = 21
            HelpContext = 10512
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'START_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 0
          end
          object wwdpTermination_Date: TevDBDateTimePicker
            Left = 12
            Top = 89
            Width = 129
            Height = 21
            HelpContext = 10513
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'TERMINATION_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
          object wwcbOtherService: TevDBLookupCombo
            Left = 149
            Top = 89
            Width = 129
            Height = 21
            HelpContext = 10514
            Picture.PictureMaskFromDataSet = False
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'60'#9'NAME'#9'F')
            DataField = 'SB_OTHER_SERVICE_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_SB_OTHER_SERVICE.SB_OTHER_SERVICE
            LookupField = 'SB_OTHER_SERVICE_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbSetup_Completed: TevDBComboBox
            Left = 12
            Top = 209
            Width = 266
            Height = 21
            HelpContext = 10514
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'SETUP_COMPLETED'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'ACTIVE'#9'A'
              'IN ACTIVE'#9'N'
              'OUT OF BUS'#9'D'
              'OTHER SERVICE'#9'L'
              'SEASONAL ACTIVE'#9'C'
              'SEASONAL INACTIVE'#9'S'
              'IN HOUSE'#9'I'
              'SOLD'#9'O')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 5
            UnboundDataType = wwDefault
            OnChange = wwcbStatus_CodeChange
          end
        end
      end
    end
    object tshtGeneral_Business: TTabSheet
      Caption = 'General Business'
      ImageIndex = 39
      object sbGeneralBusiness: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpGeneral: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 475
          Height = 289
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'General'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablBusiness_Start_Date: TevLabel
            Left = 12
            Top = 35
            Width = 104
            Height = 16
            Caption = '~Evolution Start Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel28: TevLabel
            Left = 124
            Top = 35
            Width = 95
            Height = 13
            Caption = 'Initial Effective Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablAccountant: TevLabel
            Left = 12
            Top = 74
            Width = 55
            Height = 13
            Caption = 'Accountant'
          end
          object lablContact: TevLabel
            Left = 12
            Top = 113
            Width = 37
            Height = 13
            Caption = 'Contact'
            FocusControl = dedtContact
          end
          object lablBusiness_Type: TevLabel
            Left = 237
            Top = 35
            Width = 69
            Height = 13
            Caption = 'Business Type'
          end
          object lablCorporation_Type: TevLabel
            Left = 237
            Top = 113
            Width = 90
            Height = 16
            Caption = '~Corporation Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablSIC_Code: TevLabel
            Left = 237
            Top = 152
            Width = 45
            Height = 13
            Caption = 'SIC Code'
            FocusControl = dedtSIC_Code
          end
          object lablFiscal_Year_End: TevLabel
            Left = 12
            Top = 152
            Width = 74
            Height = 13
            Caption = 'Fiscal Year End'
            FocusControl = dedtFiscal_Year_End
          end
          object lablDays_Open: TevLabel
            Left = 124
            Top = 152
            Width = 53
            Height = 13
            Caption = 'Days Open'
          end
          object lablTime_Closed: TevLabel
            Left = 124
            Top = 191
            Width = 58
            Height = 13
            Caption = 'Time Closed'
          end
          object lablTime_Open: TevLabel
            Left = 12
            Top = 191
            Width = 52
            Height = 13
            Caption = 'Time Open'
          end
          object lablProcessPriority: TevLabel
            Left = 12
            Top = 230
            Width = 81
            Height = 16
            Caption = '~Process Priority'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel50: TevLabel
            Left = 237
            Top = 74
            Width = 70
            Height = 13
            Caption = 'Employer Type'
          end
          object wwdpBusiness_Start_Date: TevDBDateTimePicker
            Left = 12
            Top = 50
            Width = 104
            Height = 21
            HelpContext = 10561
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'BUSINESS_START_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 0
          end
          object wwdpInitial_Effective_date: TevDBDateTimePicker
            Left = 124
            Top = 50
            Width = 104
            Height = 21
            HelpContext = 10561
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'INITIAL_EFFECTIVE_DATE'
            DataSource = wwdsMaster
            Date = 2.000000000000000000
            Epoch = 1950
            Enabled = False
            ShowButton = True
            TabOrder = 1
            DisplayFormat = 'm/d/yyyy'
          end
          object wwlcAccountant: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 216
            Height = 21
            HelpContext = 10563
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME'
              'ADDRESS1'#9'30'#9'ADDRESS1'
              'ADDRESS2'#9'30'#9'ADDRESS2'
              'CITY'#9'20'#9'CITY'
              'STATE'#9'3'#9'STATE'
              'ZIP_CODE'#9'10'#9'ZIP_CODE')
            DataField = 'SB_ACCOUNTANT_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_SB_ACCOUNTANT.SB_ACCOUNTANT
            LookupField = 'SB_ACCOUNTANT_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object dedtContact: TevDBEdit
            Left = 12
            Top = 128
            Width = 216
            Height = 21
            HelpContext = 10564
            DataField = 'ACCOUNTANT_CONTACT'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbBusiness_Type: TevDBComboBox
            Left = 237
            Top = 50
            Width = 216
            Height = 21
            HelpContext = 10567
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'BUSINESS_TYPE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Government'#9'G'
              'Manufacturing'#9'N'
              'Education'#9'E'
              'Medical'#9'M'
              'Municipal'#9'U'
              'Construction'#9'C'
              'Hospitality'#9'H'
              'Agriculture'#9'A'
              'Household'#9'S'
              'Military'#9'L'
              'RailRoad'#9'R'
              'Seasonally Active'#9'Y'
              'Other'#9'O')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 9
            UnboundDataType = wwDefault
          end
          object wwcbCorporation_Type: TevDBComboBox
            Left = 237
            Top = 128
            Width = 216
            Height = 21
            HelpContext = 10568
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CORPORATION_TYPE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 11
            UnboundDataType = wwDefault
          end
          object dedtSIC_Code: TevDBEdit
            Left = 237
            Top = 167
            Width = 216
            Height = 21
            HelpContext = 10569
            DataField = 'SIC_CODE'
            DataSource = wwdsMaster
            TabOrder = 12
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtFiscal_Year_End: TevDBEdit
            Left = 12
            Top = 167
            Width = 104
            Height = 21
            HelpContext = 10572
            DataField = 'FISCAL_YEAR_END'
            DataSource = wwdsMaster
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBSpinEdit1: TevDBSpinEdit
            Left = 124
            Top = 167
            Width = 104
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 7.000000000000000000
            DataField = 'DAYS_OPEN'
            DataSource = wwdsMaster
            TabOrder = 5
            UnboundDataType = wwDefault
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 124
            Top = 206
            Width = 104
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'TIME_CLOSE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = False
            TabOrder = 7
            DisplayFormat = 'HH:NN AM/PM'
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 12
            Top = 206
            Width = 104
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'TIME_OPEN'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = False
            TabOrder = 6
            DisplayFormat = 'HH:NN AM/PM'
          end
          object wwseProcessPriority: TevDBSpinEdit
            Left = 12
            Top = 245
            Width = 104
            Height = 21
            HelpContext = 10665
            Increment = 1.000000000000000000
            Value = 1.000000000000000000
            DataField = 'PROCESS_PRIORITY'
            DataSource = wwdsMaster
            TabOrder = 8
            UnboundDataType = wwDefault
          end
          object drgpRestaurant: TevDBRadioGroup
            Left = 237
            Top = 230
            Width = 104
            Height = 37
            HelpContext = 10571
            Caption = '~Restaurant'
            Columns = 2
            DataField = 'RESTAURANT'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 14
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpNon_Profit: TevDBRadioGroup
            Left = 349
            Top = 230
            Width = 104
            Height = 37
            HelpContext = 10570
            Caption = '~Non-Profit'
            Columns = 2
            DataField = 'NON_PROFIT'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 15
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpSuccessor_Company: TevDBRadioGroup
            Left = 237
            Top = 191
            Width = 216
            Height = 37
            HelpContext = 10566
            Caption = '~Successor Company'
            Columns = 2
            DataField = 'SUCCESSOR_COMPANY'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 13
            Values.Strings = (
              'Y'
              'N')
          end
          object edEmployerType: TevDBComboBox
            Left = 237
            Top = 89
            Width = 216
            Height = 21
            HelpContext = 10567
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'EMPLOYER_TYPE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 10
            UnboundDataType = wwDefault
          end
        end
        object fpBankAccounts: TisUIFashionPanel
          Left = 491
          Top = 8
          Width = 270
          Height = 367
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 5
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Bank Accounts'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablBank_Account_Level: TevLabel
            Left = 12
            Top = 35
            Width = 35
            Height = 16
            Caption = '~Level'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablPayroll_Bank_Account_Number: TevLabel
            Left = 12
            Top = 74
            Width = 40
            Height = 16
            Caption = '~Payroll'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablTax_Bank_Account_Number: TevLabel
            Left = 12
            Top = 113
            Width = 27
            Height = 16
            Caption = '~Tax'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDD_Bank_Account_Number: TevLabel
            Left = 12
            Top = 152
            Width = 76
            Height = 16
            Caption = '~Direct Deposit'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablBilling_Bank_Account_Number: TevLabel
            Left = 12
            Top = 191
            Width = 36
            Height = 16
            Caption = '~Billing'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablWorkers_Comp_Bank_Account_Number: TevLabel
            Left = 12
            Top = 230
            Width = 79
            Height = 16
            Caption = '~Workers Comp'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel29: TevLabel
            Left = 12
            Top = 269
            Width = 35
            Height = 13
            Caption = 'Manual'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwcbBank_Account_Level: TevDBComboBox
            Left = 12
            Top = 50
            Width = 121
            Height = 21
            HelpContext = 10579
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'BANK_ACCOUNT_LEVEL'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Company'#9'0'
              'Division'#9'1'
              'Branch'#9'2'
              'Department'#9'3'
              'Team'#9'4')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object wwlcPayroll_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 235
            Height = 21
            HelpContext = 10583
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name'
              'Description'#9'20'#9'Description')
            DataField = 'PAYROLL_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwlcTax_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 235
            Height = 21
            HelpContext = 10584
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name'
              'Description'#9'20'#9'Description')
            DataField = 'TAX_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwlcDD_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 235
            Height = 21
            HelpContext = 10587
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name'
              'Description'#9'20'#9'Description')
            DataField = 'DD_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwlcBilling_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 206
            Width = 235
            Height = 21
            HelpContext = 10586
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name'
              'Description'#9'20'#9'Description')
            DataField = 'BILLING_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwlcWorkers_Comp_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 245
            Width = 235
            Height = 21
            HelpContext = 10585
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name'
              'Description'#9'20'#9'Description')
            DataField = 'W_COMP_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object cbManualBankAccount: TevDBLookupCombo
            Left = 12
            Top = 284
            Width = 235
            Height = 21
            HelpContext = 10585
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name'
              'Description'#9'20'#9'Description')
            DataField = 'MANUAL_CL_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 6
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evDBRadioGroup9: TevDBRadioGroup
            Left = 12
            Top = 308
            Width = 235
            Height = 37
            HelpContext = 10644
            Caption = '~Use Non-Home DBDT Payroll Bank Account'
            Columns = 2
            DataField = 'BREAK_CHECKS_BY_DBDT'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 7
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object fpNotes: TisUIFashionPanel
          Left = 213
          Top = 451
          Width = 270
          Height = 212
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Notes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablNature_Business: TevLabel
            Left = 12
            Top = 35
            Width = 31
            Height = 13
            Caption = 'Payroll'
          end
          object lablCompany_Notes: TevLabel
            Left = 12
            Top = 113
            Width = 44
            Height = 13
            Caption = 'Company'
          end
          object dmemNature_Business: TEvDBMemo
            Left = 12
            Top = 50
            Width = 234
            Height = 57
            HelpContext = 10655
            DataField = 'NATURE_OF_BUSINESS'
            DataSource = wwdsMaster
            ScrollBars = ssVertical
            TabOrder = 0
          end
          object dmemCompany_Notes: TEvDBMemo
            Left = 12
            Top = 128
            Width = 234
            Height = 61
            HelpContext = 10577
            DataField = 'COMPANY_NOTES'
            DataSource = wwdsMaster
            ScrollBars = ssVertical
            TabOrder = 1
          end
        end
        object fpDelivery: TisUIFashionPanel
          Left = 213
          Top = 306
          Width = 270
          Height = 137
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Delivery'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablDelivery_Group_Number: TevLabel
            Left = 12
            Top = 35
            Width = 29
            Height = 13
            Caption = 'Group'
          end
          object lablAnnual_Delivery_Group_Number: TevLabel
            Left = 12
            Top = 74
            Width = 65
            Height = 13
            Caption = 'Annual Group'
          end
          object lablQrtly_Delivery_Group_Number: TevLabel
            Left = 133
            Top = 35
            Width = 74
            Height = 13
            Caption = 'Quarterly Group'
          end
          object lablPercentage: TevLabel
            Left = 133
            Top = 74
            Width = 58
            Height = 13
            Caption = '% Sales Tax'
          end
          object wwlcDelivery_Group_Number: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 113
            Height = 21
            HelpContext = 10599
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DELIVERY_GROUP_DESCRIPTION'#9'20'#9'DELIVERY_GROUP_DESCRIPTION')
            DataField = 'CL_DELIVERY_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_DELIVERY_GROUP.CL_DELIVERY_GROUP
            LookupField = 'CL_DELIVERY_GROUP_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcAnnual_Delivery_Group_Number: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 113
            Height = 21
            HelpContext = 10599
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DELIVERY_GROUP_DESCRIPTION'#9'20'#9'DELIVERY_GROUP_DESCRIPTION')
            DataField = 'ANNUAL_CL_DELIVERY_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_DELIVERY_GROUP.CL_DELIVERY_GROUP
            LookupField = 'CL_DELIVERY_GROUP_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcQrtly_Delivery_Group_Number: TevDBLookupCombo
            Left = 133
            Top = 50
            Width = 113
            Height = 21
            HelpContext = 10599
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DELIVERY_GROUP_DESCRIPTION'#9'20'#9'DELIVERY_GROUP_DESCRIPTION')
            DataField = 'QUARTER_CL_DELIVERY_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_DELIVERY_GROUP.CL_DELIVERY_GROUP
            LookupField = 'CL_DELIVERY_GROUP_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwdePercentage: TevDBEdit
            Left = 133
            Top = 89
            Width = 113
            Height = 21
            DataField = 'SALES_TAX_PERCENTAGE'
            DataSource = wwdsMaster
            PopupMenu = pmSalesTax
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpReporting: TisUIFashionPanel
          Left = 8
          Top = 451
          Width = 197
          Height = 212
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Reporting'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablCommon_Paymaster_Number: TevLabel
            Left = 12
            Top = 35
            Width = 93
            Height = 13
            Caption = 'Consolidated Taxes'
          end
          object lablCL_CO_Consolidation_Number: TevLabel
            Left = 12
            Top = 74
            Width = 110
            Height = 13
            Caption = 'Consolidated Reporting'
          end
          object lablCustomerService: TevLabel
            Left = 12
            Top = 113
            Width = 158
            Height = 13
            Caption = 'Customer Service Representative'
          end
          object evLabel46: TevLabel
            Left = 12
            Top = 152
            Width = 132
            Height = 13
            Caption = 'Dun and Bradstreet Number'
            FocusControl = evDBEdit14
          end
          object wwlcCommon_Paymaster_Number: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 162
            Height = 21
            HelpContext = 10663
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CPM_NAME'#9'40'#9'CPM_NAME')
            DataField = 'CL_COMMON_PAYMASTER_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_COMMON_PAYMASTER.CL_COMMON_PAYMASTER
            LookupField = 'CL_COMMON_PAYMASTER_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcCL_CO_Consolidation_Number: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 162
            Height = 21
            HelpContext = 10664
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'DESCRIPTION')
            DataField = 'CL_CO_CONSOLIDATION_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_CO_CONSOLIDATION.CL_CO_CONSOLIDATION
            LookupField = 'CL_CO_CONSOLIDATION_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object dblkupCustomerServiceUser: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 162
            Height = 21
            HelpContext = 10593
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'LAST_NAME'#9'30'#9'LAST_NAME'
              'FIRST_NAME'#9'20'#9'FIRST_NAME'
              'MIDDLE_INITIAL'#9'1'#9'MIDDLE_INITIAL')
            DataField = 'CUSTOMER_SERVICE_SB_USER_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_SB_USER.SB_USER
            LookupField = 'SB_USER_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object evDBEdit14: TevDBEdit
            Left = 12
            Top = 167
            Width = 162
            Height = 21
            HelpContext = 10564
            DataField = 'Duns_and_Bradstreet'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpCPA: TisUIFashionPanel
          Left = 8
          Top = 306
          Width = 197
          Height = 137
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCPA'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'CPA'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object drgpPrint_CPA: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 162
            Height = 37
            HelpContext = 10565
            Caption = '~Print on Reports'
            Columns = 2
            DataField = 'PRINT_CPA'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            PopupMenu = pmPrintCPA
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpNameOnInvoice: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 162
            Height = 37
            HelpContext = 10565
            Caption = '~Print on Invoices'
            Columns = 2
            DataField = 'NAME_ON_INVOICE'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'A'
              'B')
          end
        end
        object fpNetwork: TisUIFashionPanel
          Left = 491
          Top = 382
          Width = 270
          Height = 279
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 6
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Network'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablNetwork_Administrator: TevLabel
            Left = 12
            Top = 35
            Width = 103
            Height = 13
            Caption = 'Network Administrator'
            FocusControl = dedtNetwork_Administrator
          end
          object lablNetwork: TevLabel
            Left = 12
            Top = 119
            Width = 76
            Height = 16
            Caption = '~Network Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablNetwork_Admin_Phone_Type: TevLabel
            Left = 12
            Top = 163
            Width = 67
            Height = 16
            Caption = '~Phone Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablNetwork_Administrator_Phone: TevLabel
            Left = 12
            Top = 211
            Width = 31
            Height = 13
            Caption = 'Phone'
          end
          object dedtNetwork_Administrator: TevDBEdit
            Left = 12
            Top = 50
            Width = 233
            Height = 21
            HelpContext = 10607
            DataField = 'NETWORK_ADMINISTRATOR'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdcNetwork: TevDBComboBox
            Left = 12
            Top = 134
            Width = 233
            Height = 21
            HelpContext = 18020
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'NETWORK'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Windows NT 3.x'#9'3'
              'Windows NT 4.x'#9'4'
              'Windows NT 5.x'#9'5'
              'Novell 3.x'#9'V'
              'Novell 4.x'#9'E'
              'Novell 5.x'#9'J'
              'Lantastic'#9'L'
              'AS/400'#9'4'
              'Unix'#9'U'
              'Other'#9'O'
              'None'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object wwcbNetwork_Admin_Phone_Type: TevDBComboBox
            Left = 12
            Top = 178
            Width = 233
            Height = 21
            HelpContext = 10609
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'NETWORK_ADMIN_PHONE_TYPE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Primary Phone'#9'P'
              'Phone 2'#9'2'
              'Pager'#9'G'
              'Fax'#9'F'
              'Cell Phone'#9'C'
              'Emergency Phone'#9'E'
              'After Hours'#9'A'
              'None'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object wwdeNetwork_Administrator_Phone: TevDBEdit
            Left = 12
            Top = 226
            Width = 233
            Height = 21
            HelpContext = 10610
            DataField = 'NETWORK_ADMINISTRATOR_PHONE'
            DataSource = wwdsMaster
            Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBRadioGroup4: TevDBRadioGroup
            Left = 12
            Top = 78
            Width = 233
            Height = 37
            HelpContext = 10604
            Caption = '~External'
            Columns = 2
            DataField = 'EXTERNAL_NETWORK_ADMINISTRATOR'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
        end
      end
    end
    object tshtMisc: TTabSheet
      Caption = 'Payroll && Employees'
      ImageIndex = 41
      object sbPayroll: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpPayroll: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 390
          Height = 305
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpPayroll'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Payroll'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablPay_Frequencies: TevLabel
            Left = 12
            Top = 35
            Width = 88
            Height = 16
            Caption = '~Pay Frequencies'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablPay_Frequency_Hourly_Default: TevLabel
            Left = 12
            Top = 74
            Width = 166
            Height = 16
            Caption = '~Employee Pay Frequency Default'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablPrimary_Sort_Field: TevLabel
            Left = 12
            Top = 113
            Width = 69
            Height = 16
            Caption = '~Entry Sorting'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablSecondary_Sort_Field: TevLabel
            Left = 12
            Top = 152
            Width = 79
            Height = 16
            Caption = '~Entry Grouping'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablPayroll_Password: TevLabel
            Left = 12
            Top = 248
            Width = 80
            Height = 13
            Caption = 'Payroll Password'
            FocusControl = dedtPayroll_Password
          end
          object lablPayrate_Precision: TevLabel
            Left = 213
            Top = 191
            Width = 64
            Height = 13
            Caption = 'Supplemental'
          end
          object evLabel11: TevLabel
            Left = 214
            Top = 35
            Width = 153
            Height = 16
            Caption = '~Distributed Deductions Default'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object EvBevel2: TEvBevel
            Left = 12
            Top = 238
            Width = 353
            Height = 3
            Shape = bsTopLine
          end
          object evLabel70: TevLabel
            Left = 12
            Top = 191
            Width = 63
            Height = 16
            Caption = '~Entry Mode'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwcbPay_Frequencies: TevDBComboBox
            Left = 12
            Top = 50
            Width = 189
            Height = 21
            HelpContext = 10629
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'PAY_FREQUENCIES'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Weekly'#9'A'
              'Bi-Weekly'#9'B'
              'Semi-Monthly'#9'C'
              'Monthly'#9'D'
              'Weekly, Bi-Weekly'#9'E'
              'Weekly, Semi-Monthly'#9'F'
              'Weekly, Monthly'#9'G'
              'Bi-Weekly, Semi-Monthly'#9'H'
              'Bi-Weekly, Monthly'#9'I'
              'Semi-Monthly, Monthly'#9'J'
              'Weekly, Bi-Weekly, Semi-Monthly'#9'K'
              'Weekly, Bi-Weekly, Monthly'#9'L'
              'Weekly, Semi-Monthly, Monthly'#9'M'
              'Bi-Weekly, Semi-Monthly, Monthly'#9'N'
              'Weekly, Bi-Weekly, Semi-Monthly, Monthly'#9'O')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
            OnCloseUp = wwcbPay_FrequenciesCloseUp
          end
          object wwcbPay_Frequency_Hourly_Default: TevDBComboBox
            Left = 12
            Top = 89
            Width = 190
            Height = 21
            HelpContext = 10632
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'PAY_FREQUENCY_HOURLY_DEFAULT'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Weekly'#9'W'
              'Bi-Weekly'#9'B'
              'Semi-Monthly'#9'S'
              'Monthly'#9'M'
              'Quarterly'#9'Q'
              'Semi-Annual'#9'N'
              'Annual'#9'A')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object wwcbPrimary_Sort_Field: TevDBComboBox
            Left = 12
            Top = 128
            Width = 190
            Height = 21
            HelpContext = 10630
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'PRIMARY_SORT_FIELD'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'EE Code'#9'E'
              'SS #'#9'S'
              'Alpha'#9'A')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object wwcbSecondary_Sort_Field: TevDBComboBox
            Left = 12
            Top = 167
            Width = 190
            Height = 21
            HelpContext = 10631
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'SECONDARY_SORT_FIELD'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Company Number'#9'0'
              'Company Name'#9'6'
              'Division Number'#9'1'
              'Division Name'#9'7'
              'Branch Number'#9'2'
              'Branch Name'#9'8'
              'Department Number'#9'3'
              'Department Name'#9'9'
              'Team Number'#9'4'
              'Team Name'#9'A')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object dedtPayroll_Password: TevDBEdit
            Left = 12
            Top = 263
            Width = 190
            Height = 21
            HelpContext = 10639
            DataField = 'PAYROLL_PASSWORD'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBRadioGroup3: TevDBRadioGroup
            Left = 214
            Top = 74
            Width = 154
            Height = 60
            HelpContext = 10644
            Caption = '~Block Checks/Reports'
            Columns = 2
            DataField = 'EXCLUDE_R_C_B_0R_N'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Reports'
              'Checks'
              'Both'
              'None')
            ParentFont = False
            TabOrder = 7
            Values.Strings = (
              'R'
              'C'
              'B'
              'N')
          end
          object drgpAuto_Labor_Dist_Show_Deducts: TevDBRadioGroup
            Left = 214
            Top = 136
            Width = 154
            Height = 52
            HelpContext = 10644
            Caption = '~Auto Labor Distribution'
            Columns = 2
            DataField = 'AUTO_LABOR_DIST_SHOW_DEDUCTS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 8
            Values.Strings = (
              'Y'
              'N')
          end
          object wwdePayrate_Precision: TevDBEdit
            Left = 213
            Top = 206
            Width = 154
            Height = 21
            HelpContext = 10637
            DataField = 'PAYRATE_PRECISION'
            DataSource = wwdsMaster
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBComboBox1: TevDBComboBox
            Left = 214
            Top = 50
            Width = 154
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'DISTRIBUTE_DEDUCTIONS_DEFAULT'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Distribute Taxes'#9'T'
              'Distribute Deductions'#9'D'
              'Distribute Both'#9'Y'
              'Distribute None'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 6
            UnboundDataType = wwDefault
          end
          object cbEntryMode: TevDBComboBox
            Left = 12
            Top = 206
            Width = 190
            Height = 21
            HelpContext = 10631
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'PAYROLL_ENTRY_MODE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
        end
        object fpMisc: TisUIFashionPanel
          Left = 8
          Top = 321
          Width = 390
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpMisc'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Miscellaneous'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel15: TevLabel
            Left = 12
            Top = 35
            Width = 95
            Height = 16
            Caption = '~Autopay Company'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDBDT_Level: TevLabel
            Left = 12
            Top = 74
            Width = 68
            Height = 16
            Caption = '~DBDT Level'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evDBComboBox5: TevDBComboBox
            Left = 12
            Top = 50
            Width = 353
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'AUTOPAY_COMPANY'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object wwcbDBDT_Level: TevDBComboBox
            Left = 12
            Top = 89
            Width = 353
            Height = 21
            HelpContext = 10638
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'DBDT_LEVEL'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Company'#9'0'
              'Division'#9'1'
              'Branch'#9'2'
              'Department'#9'3'
              'Team'#9'4')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
            OnChange = wwcbDBDT_LevelChange
          end
        end
        object fpTimeOff: TisUIFashionPanel
          Left = 408
          Top = 8
          Width = 228
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpTimeOff'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Time Off'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel35: TevLabel
            Left = 8
            Top = 74
            Width = 82
            Height = 16
            Caption = '~Check Balance'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object drgpTime_Off_Accrual: TevDBRadioGroup
            Left = 8
            Top = 35
            Width = 196
            Height = 37
            HelpContext = 10598
            Caption = '~Accrual'
            Columns = 2
            DataField = 'TIME_OFF_ACCRUAL'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object DBCboCheckTOBalance: TevDBComboBox
            Left = 8
            Top = 89
            Width = 196
            Height = 21
            HelpContext = 10632
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CHECK_TIME_OFF_AVAIL'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
        end
        object fpAutoReduction: TisUIFashionPanel
          Left = 408
          Top = 149
          Width = 228
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpAutoReduction'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Auto Reduction'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object LabelAutoReductionEDGroup: TevLabel
            Left = 12
            Top = 74
            Width = 166
            Height = 13
            Caption = 'Auto Reduction E/D Group Default'
          end
          object DBlookAutoReductionED: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 196
            Height = 21
            HelpContext = 8150
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'AUTO_RD_DFLT_CL_E_D_GROUPS_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
            LookupField = 'CL_E_D_GROUPS_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object DBRadioGrpAutoReductionEEs: TevDBRadioGroup
            Left = 12
            Top = 33
            Width = 196
            Height = 37
            HelpContext = 10644
            Caption = 'Employees to Reduce Default'
            Columns = 3
            DataField = 'AUTO_REDUCTION_DEFAULT_EES'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Hourly'
              'Salaried'
              'Both')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'H'
              'S'
              'B')
          end
        end
        object fpEmployees: TisUIFashionPanel
          Left = 408
          Top = 289
          Width = 228
          Height = 255
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpEmployees'
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Employees'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablAuto_Increment: TevLabel
            Left = 12
            Top = 35
            Width = 89
            Height = 13
            Caption = 'EE Auto Increment'
            FocusControl = dedtAuto_Increment
          end
          object lablAverage_Hours: TevLabel
            Left = 12
            Top = 74
            Width = 71
            Height = 13
            Caption = 'Average Hours'
          end
          object lablW2_Type: TevLabel
            Left = 12
            Top = 113
            Width = 95
            Height = 16
            Caption = '~Annual Form Type'
            FocusControl = wwcbAnualFormType
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dedtAuto_Increment: TevDBEdit
            Left = 12
            Top = 50
            Width = 95
            Height = 21
            HelpContext = 10636
            DataField = 'AUTO_INCREMENT'
            DataSource = wwdsMaster
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object btnCopyEEs: TevBitBtn
            Left = 115
            Top = 49
            Width = 90
            Height = 21
            Caption = 'Copy Employees'
            TabOrder = 2
            OnClick = btnCopyEEsClick
            Color = clBlack
            NumGlyphs = 2
            Margin = 0
          end
          object wwdeAverage_Hours: TevDBEdit
            Left = 12
            Top = 89
            Width = 95
            Height = 21
            HelpContext = 10635
            DataField = 'AVERAGE_HOURS'
            DataSource = wwdsMaster
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object btnNewHireSetup: TevButton
            Left = 115
            Top = 89
            Width = 90
            Height = 21
            Caption = 'New Hire Setup'
            TabOrder = 3
            OnClick = btnNewHireSetupClick
            Color = clBlack
            Margin = 0
          end
          object wwcbAnualFormType: TevDBComboBox
            Left = 12
            Top = 128
            Width = 101
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ANNUAL_FORM_TYPE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmAnnualFormType
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
          object rgEEDefaultPrintVoucher: TevDBRadioGroup
            Left = 12
            Top = 155
            Width = 192
            Height = 37
            HelpContext = 10598
            Caption = '~Default for Employee Print Voucher'
            Columns = 2
            DataField = 'EE_PRINT_VOUCHER_DEFAULT'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            PopupMenu = pmEEPrintVoucher
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
          object rgEnforgerDOB: TevDBRadioGroup
            Left = 12
            Top = 196
            Width = 192
            Height = 37
            HelpContext = 10598
            Caption = '~Enforce DOB on W2 Employees'
            Columns = 2
            DataField = 'ENFORCE_EE_DOB'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            PopupMenu = pmEnforgeDOB
            TabOrder = 6
          end
        end
      end
    end
    object tshtChecks: TTabSheet
      Caption = 'Checks'
      ImageIndex = 42
      object sbChecks: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpOptions: TisUIFashionPanel
          Left = 8
          Top = 226
          Width = 640
          Height = 249
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Check Printing Options'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablCO_Check_Primary_Sort: TevLabel
            Left = 12
            Top = 35
            Width = 76
            Height = 16
            Caption = '~Check Sorting'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablCO_Check_Secondary_Sort: TevLabel
            Left = 12
            Top = 74
            Width = 86
            Height = 16
            Caption = '~Check Grouping'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablCheck_Form: TevLabel
            Left = 12
            Top = 113
            Width = 66
            Height = 16
            Caption = '~Check Form'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel17: TevLabel
            Left = 12
            Top = 152
            Width = 91
            Height = 16
            Caption = '~Misc Check Form'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label28: TevLabel
            Left = 253
            Top = 35
            Width = 95
            Height = 16
            Caption = '~Print Signature On'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablAuto_Labor_Dist_Level: TevLabel
            Left = 253
            Top = 74
            Width = 148
            Height = 16
            Caption = '~Check Options for Labor Dist.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablTargetsOnCheck: TevLabel
            Left = 439
            Top = 35
            Width = 96
            Height = 16
            Caption = '~Targets On Check'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablCheckMessage: TevLabel
            Left = 439
            Top = 113
            Width = 77
            Height = 13
            Caption = 'Check Message'
          end
          object LabelReprintTOBalances: TevLabel
            Left = 439
            Top = 74
            Width = 145
            Height = 16
            Caption = '~Reprint TO Balances Default'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblShowPaystub: TevLabel
            Left = 439
            Top = 152
            Width = 106
            Height = 13
            Caption = 'Show Paystub in ESS '
          end
          object evLabel54: TevLabel
            Left = 439
            Top = 172
            Width = 130
            Height = 13
            Caption = '# Days Prior to Check Date'
          end
          object wwcbCO_Check_Primary_Sort: TevDBComboBox
            Left = 12
            Top = 50
            Width = 233
            Height = 21
            HelpContext = 10549
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CO_CHECK_PRIMARY_SORT'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'EE Code'#9'E'
              'SS #'#9'S'
              'Alpha'#9'A')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object wwcbCO_Check_Secondary_Sort: TevDBComboBox
            Left = 12
            Top = 89
            Width = 233
            Height = 21
            HelpContext = 10550
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CO_CHECK_SECONDARY_SORT'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Company'#9'0'
              'Division'#9'1'
              'Branch'#9'2'
              'Department'#9'3'
              'Team'#9'4')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object evDBRadioGroup5: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 233
            Height = 37
            HelpContext = 10558
            Caption = '~Reverse Check Printing'
            Columns = 2
            DataField = 'REVERSE_CHECK_PRINTING'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
          end
          object cbCheckForm: TevComboBox
            Left = 12
            Top = 128
            Width = 233
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            PopupMenu = pmCheckForm
            TabOrder = 2
            OnCloseUp = cbCheckFormCloseUp
            OnKeyDown = cbCheckFormKeyDown
          end
          object cbMiscCheckForm: TevComboBox
            Left = 12
            Top = 167
            Width = 233
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            PopupMenu = pmMicsCheck
            TabOrder = 3
            OnCloseUp = cbCheckFormCloseUp
            OnKeyDown = cbCheckFormKeyDown
          end
          object wwDBComboBox2: TevDBComboBox
            Left = 253
            Top = 50
            Width = 178
            Height = 21
            HelpContext = 3534
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'REMOTE_OF_CLIENT'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'All Checks'#9'N'
              'Regular Checks'#9'Y'
              'Misc. Checks'#9'M')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 5
            UnboundDataType = wwDefault
          end
          object wwcbAuto_Labor_Dist_Level: TevDBComboBox
            Left = 253
            Top = 89
            Width = 178
            Height = 21
            HelpContext = 10634
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'AUTO_LABOR_DIST_LEVEL'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Suppress'#9'C'
              'Do not Suppress'#9'S')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 6
            UnboundDataType = wwDefault
          end
          object evDBRadioGroup8: TevDBRadioGroup
            Left = 253
            Top = 113
            Width = 178
            Height = 37
            HelpContext = 10558
            Caption = '~Summarize SUI On Check'
            Columns = 2
            DataField = 'SUMMARIZE_SUI'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 7
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpCollateChecks: TevDBRadioGroup
            Left = 253
            Top = 152
            Width = 178
            Height = 37
            HelpContext = 10627
            Caption = '~Collate Checks'
            Columns = 2
            DataField = 'COLLATE_CHECKS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 8
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpPrint_Manual_Check_Stubs: TevDBRadioGroup
            Left = 253
            Top = 191
            Width = 178
            Height = 37
            HelpContext = 10559
            Caption = '~Print Manual Check Stubs'
            Columns = 2
            DataField = 'PRINT_MANUAL_CHECK_STUBS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 9
            Values.Strings = (
              'Y'
              'N')
          end
          object wwcbTargetsOnCheck: TevDBComboBox
            Left = 439
            Top = 50
            Width = 178
            Height = 21
            HelpContext = 10551
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CHECK_TYPE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 10
            UnboundDataType = wwDefault
          end
          object wwdpCheckMessage: TevDBEdit
            Left = 439
            Top = 128
            Width = 178
            Height = 21
            HelpContext = 10670
            DataField = 'CHECK_MESSAGE'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 12
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbReprintToBalances: TevDBComboBox
            Left = 439
            Top = 89
            Width = 178
            Height = 21
            HelpContext = 10551
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'REPRINT_TO_BALANCE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 11
            UnboundDataType = wwDefault
          end
          object edDaysDue: TevDBEdit
            Left = 578
            Top = 167
            Width = 39
            Height = 21
            HelpContext = 5
            DataField = 'SHOW_PAYSTUBS_ESS_DAYS'
            DataSource = wwdsMaster
            TabOrder = 13
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpShow: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 640
          Height = 210
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpShow'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Show'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel76: TevLabel
            Left = 253
            Top = 172
            Width = 46
            Height = 13
            Caption = 'EIN State'
          end
          object drgpShow_SS_Number_Check: TevDBRadioGroup
            Left = 253
            Top = 35
            Width = 178
            Height = 37
            HelpContext = 10558
            Caption = '~SS# on Check'
            Columns = 3
            DataField = 'SHOW_SS_NUMBER_ON_CHECK'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No'
              'Last 4')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpShow_Rate_ByHours: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 233
            Height = 37
            HelpContext = 10557
            Caption = '~Rate / Hour on Check'
            Columns = 2
            DataField = 'SHOW_RATE_BY_HOURS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            PopupMenu = pmShowRateHours
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpShow_YDT_Check: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 233
            Height = 37
            HelpContext = 10556
            Caption = '~YTD on Check'
            Columns = 2
            DataField = 'SHOW_YTD_ON_CHECK'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpShow_Shifts_Check: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 233
            Height = 37
            HelpContext = 10555
            Caption = '~Shifts on Check'
            Columns = 2
            DataField = 'SHOW_SHIFTS_ON_CHECK'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 439
            Top = 35
            Width = 178
            Height = 37
            HelpContext = 10558
            Caption = '~Rates on Check'
            Columns = 2
            DataField = 'SHOW_RATES_ON_CHECKS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup2: TevDBRadioGroup
            Left = 439
            Top = 74
            Width = 178
            Height = 37
            HelpContext = 10558
            Caption = '~Direct Deposit Number on Check'
            Columns = 2
            DataField = 'SHOW_DIR_DEP_NBR_ON_CHECKS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpShowShortfall: TevDBRadioGroup
            Left = 439
            Top = 113
            Width = 178
            Height = 37
            HelpContext = 10559
            Caption = '~Shortfalls On Check'
            Columns = 2
            DataField = 'SHOW_SHORTFALL_CHECK'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 8
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpShow_Punch_Details: TevDBRadioGroup
            Left = 253
            Top = 74
            Width = 178
            Height = 37
            Caption = '~Punch Details on Check'
            Columns = 2
            DataField = 'SHOW_TIME_CLOCK_PUNCH'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup6: TevDBRadioGroup
            Left = 253
            Top = 113
            Width = 178
            Height = 37
            HelpContext = 10559
            Caption = '~Manual Checks in ESS'
            Columns = 2
            DataField = 'SHOW_MANUAL_CHECKS_IN_ESS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 7
            Values.Strings = (
              'Y'
              'N')
          end
          object edShowEINOnCheck: TevDBRadioGroup
            Left = 12
            Top = 153
            Width = 233
            Height = 37
            HelpContext = 10557
            Caption = '~Show EIN on Check'
            Columns = 3
            DataField = 'SHOW_EIN_NUMBER_ON_CHECK'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
            OnChange = edShowEINOnCheckChange
          end
          object edtEINState: TevDBLookupCombo
            Left = 310
            Top = 169
            Width = 121
            Height = 21
            HelpContext = 10525
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'SHOW_EIN_NUMBER_CO_STATES_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 10
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object rgShowTipCredit: TevDBRadioGroup
            Left = 439
            Top = 153
            Width = 178
            Height = 37
            HelpContext = 10557
            Caption = '~Show Tip Credit on Check'
            Columns = 3
            DataField = 'SHOW_TIP_CREDIT_ON_CHECK'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 11
            OnChange = edShowEINOnCheckChange
          end
        end
      end
    end
    object FederalTax: TTabSheet
      Caption = 'Tax Reporting'
      ImageIndex = 14
      object sbTaxReporting: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object lablFederal_Tax_Transfer_Method: TevLabel
          Left = 656
          Top = 112
          Width = 146
          Height = 16
          Caption = '~Federal Tax Transfer Method'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Visible = False
        end
        object wwcbFederal_Tax_Transfer_Method: TevDBComboBox
          Left = 656
          Top = 128
          Width = 137
          Height = 21
          HelpContext = 10532
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'FEDERAL_TAX_TRANSFER_METHOD'
          DataSource = wwdsMaster
          DropDownCount = 8
          ItemHeight = 0
          Items.Strings = (
            'Division'#9'1'
            'Branch'#9'2'
            'Department'#9'3'
            'Team'#9'4')
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 4
          UnboundDataType = wwDefault
          Visible = False
        end
        object fpFederal: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 194
          Height = 452
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpFederal'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Federal'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablEIN_Number: TevLabel
            Left = 12
            Top = 35
            Width = 67
            Height = 16
            Caption = '~EIN Number'
            FocusControl = dedtEIN_Number
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablFederal_Tax_Deposit_Frequency: TevLabel
            Left = 12
            Top = 113
            Width = 157
            Height = 16
            Caption = '~Federal Tax Deposit Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel14: TevLabel
            Left = 12
            Top = 152
            Width = 140
            Height = 16
            Caption = '~945 Tax Deposit Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablFUI_Tax_Deposit_Frequency: TevLabel
            Left = 12
            Top = 223
            Width = 139
            Height = 16
            Caption = '~FUI Tax Deposit Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablFUI_Rate_Override: TevLabel
            Left = 12
            Top = 262
            Width = 116
            Height = 13
            Caption = 'FUI Rate Credit Override'
          end
          object bvFederal: TEvBevel
            Left = 12
            Top = 205
            Width = 160
            Height = 3
            Shape = bsTopLine
          end
          object lablFederal_Tax_Payment_Method: TevLabel
            Left = 12
            Top = 74
            Width = 148
            Height = 16
            Caption = '~Federal Tax Payment Method'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object EvBevel4: TEvBevel
            Left = 12
            Top = 315
            Width = 160
            Height = 3
            Shape = bsTopLine
          end
          object dedtEIN_Number: TevDBEdit
            Left = 12
            Top = 50
            Width = 160
            Height = 21
            HelpContext = 10527
            DataField = 'FEIN'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*9{#}'
            Picture.AutoFill = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnKeyPress = dedtEIN_NumberKeyPress
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object wwcbFederal_Tax_Deposit_Frequency: TevDBComboBox
            Left = 12
            Top = 128
            Width = 160
            Height = 21
            HelpContext = 10529
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'FEDERAL_TAX_DEPOSIT_FREQUENCY'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Next Day'#9'D'
              'Semi-Weekly'#9'X'
              'Monthly'#9'M'
              'Quarterly'#9'Q')
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmFed_Tax_Deposit_Frequency
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object wwcbFUI_Tax_Deposit_Frequency: TevDBComboBox
            Left = 12
            Top = 238
            Width = 160
            Height = 21
            HelpContext = 10537
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'FUI_TAX_DEPOSIT_FREQUENCY'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Quarterly'#9'Q')
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmFUI_Tax_Deposit_Frequency
            Sorted = False
            TabOrder = 5
            UnboundDataType = wwDefault
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object wwdeFUI_Override_Rate: TevDBEdit
            Left = 12
            Top = 277
            Width = 160
            Height = 21
            HelpContext = 10538
            DataField = 'FUI_RATE_OVERRIDE'
            DataSource = wwdsMaster
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object wwdb945TaxFreq: TevDBComboBox
            Left = 12
            Top = 167
            Width = 160
            Height = 21
            HelpContext = 10529
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'FED_945_TAX_DEPOSIT_FREQUENCY'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Next Day'#9'D'
              'Semi-Weekly'#9'X'
              'Monthly'#9'M'
              'Quarterly'#9'Q')
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmFed945_Tax_Deposit_Frequency
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object wwcbFederal_Tax_Payment_Method: TevDBComboBox
            Left = 12
            Top = 89
            Width = 160
            Height = 21
            HelpContext = 10533
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'FEDERAL_TAX_PAYMENT_METHOD'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'EFT Credit'#9'C'
              'EFT Debit'#9'D'
              'Check'#9'H'
              'Notices'#9'N'
              'Notices & Checks'#9'X'
              'Notices & EFT Credit'#9'Z'
              'Notices & EFT Debit'#9'Y')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
            OnChange = wwcbFederal_Tax_Payment_MethodChange
          end
          object gbQuarterLock: TevGroupBox
            Left = 12
            Top = 332
            Width = 160
            Height = 98
            Caption = 'Quarter Lock'
            TabOrder = 7
            object lablLockDate: TevLabel
              Left = 12
              Top = 20
              Width = 88
              Height = 13
              Caption = 'Quarter Lock Date'
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object dtpcLockDate: TevDBDateTimePicker
              Left = 12
              Top = 35
              Width = 142
              Height = 21
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'LOCK_DATE'
              DataSource = wwdsMaster
              Epoch = 1950
              Enabled = False
              ShowButton = True
              TabOrder = 0
            end
            object evDBCheckBox1: TevDBCheckBox
              Left = 12
              Top = 65
              Width = 125
              Height = 18
              Caption = 'Lock Tax Pmnts'
              DataField = 'QTR_LOCK_FOR_TAX_PMTS'
              DataSource = wwdsMaster
              TabOrder = 1
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
          end
          object edSneakyEditTabStop0ToPreventAutoPopup: TEdit
            Left = 4000
            Top = 4000
            Width = 121
            Height = 21
            TabOrder = 0
            Text = 'edSneakyEditTabStop0ToPreventAutoPopup'
          end
        end
        object fpReportingAndPayment: TisUIFashionPanel
          Left = 210
          Top = 8
          Width = 415
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpReportingAndPayment'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Reporting / Payment'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label14: TevLabel
            Left = 12
            Top = 35
            Width = 132
            Height = 16
            Caption = '~Federal Reporting Agency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label15: TevLabel
            Left = 12
            Top = 74
            Width = 148
            Height = 16
            Caption = '~Federal Tax Payment Agency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablFUI_Tax_Payment_Angency: TevLabel
            Left = 206
            Top = 74
            Width = 86
            Height = 16
            Caption = '~FUI Tax Agency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablFUI_Tax_Report_Agency: TevLabel
            Left = 206
            Top = 35
            Width = 114
            Height = 16
            Caption = '~FUI Reporting Agency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwDBLookupCombo2: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 186
            Height = 21
            HelpContext = 10510
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'40'#9'AGENCY_NAME'#9'F')
            DataField = 'SY_FED_REPORTING_AGENCY_NBR'
            DataSource = wwdsMaster
            LookupField = 'SY_FED_REPORTING_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnChange = wwDBLookupCombo2Change
          end
          object wwDBLookupCombo3: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 186
            Height = 21
            HelpContext = 10536
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
            DataField = 'SY_FED_TAX_PAYMENT_AGENCY_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_SY_FED_TAX_PAYMENT_AGENCY.SY_FED_TAX_PAYMENT_AGENCY
            LookupField = 'SY_FED_TAX_PAYMENT_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = wwDBLookupCombo3CloseUp
          end
          object wwlcFUI_Tax_Payment_Angency: TevDBLookupCombo
            Left = 206
            Top = 89
            Width = 186
            Height = 21
            HelpContext = 10653
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
            DataField = 'FUI_SY_TAX_PAYMENT_AGENCY'
            DataSource = wwdsMaster
            LookupTable = DM_SY_FED_TAX_PAYMENT_AGENCY.SY_FED_TAX_PAYMENT_AGENCY
            LookupField = 'SY_FED_TAX_PAYMENT_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnCloseUp = wwlcFUI_Tax_Payment_AngencyCloseUp
          end
          object wwlcFUI_Tax_Report_Agency: TevDBLookupCombo
            Left = 206
            Top = 50
            Width = 186
            Height = 21
            HelpContext = 10654
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
            DataField = 'FUI_SY_TAX_REPORT_AGENCY'
            DataSource = wwdsMaster
            LookupField = 'SY_FED_REPORTING_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
        object fpTaxExemptions: TisUIFashionPanel
          Left = 210
          Top = 149
          Width = 415
          Height = 211
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Tax Exemptions - Use Effective Dates to Change Tax Statuses'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object drgpApply_Misc_Limit_To_1099: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 186
            Height = 37
            HelpContext = 10544
            Caption = '~Apply Misc Limit to 1099'
            Columns = 2
            DataField = 'APPLY_MISC_LIMIT_TO_1099'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpFederal_Tax_Exempt_Status: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 186
            Height = 37
            HelpContext = 10546
            Caption = '~Exempt Federal'
            Columns = 2
            DataField = 'FEDERAL_TAX_EXEMPT_STATUS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object drgpFed_Tax_Exempt_EE_OASDI: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 186
            Height = 37
            HelpContext = 10539
            Caption = '~Exempt EE OASDI'
            Columns = 2
            DataField = 'FED_TAX_EXEMPT_EE_OASDI'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object drgpWithholding_Default: TevDBRadioGroup
            Left = 206
            Top = 152
            Width = 186
            Height = 37
            HelpContext = 10545
            Caption = '~Withholding Default'
            Columns = 2
            DataField = 'WITHHOLDING_DEFAULT'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Single'
              'Married')
            ParentFont = False
            TabOrder = 7
            Values.Strings = (
              'S'
              'M')
          end
          object drgpFUI_Tax_Exempt: TevDBRadioGroup
            Left = 206
            Top = 35
            Width = 186
            Height = 37
            HelpContext = 10543
            Caption = '~Exempt FUI'
            Columns = 2
            DataField = 'FUI_TAX_EXEMPT'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object drgpFed_Tax_Exempt_ER_Medicare: TevDBRadioGroup
            Left = 206
            Top = 113
            Width = 186
            Height = 37
            HelpContext = 10542
            Caption = '~Exempt ER Medicare'
            Columns = 2
            DataField = 'FED_TAX_EXEMPT_ER_MEDICARE'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object drgpFed_Tax_Exempt_EE_Medicare: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 186
            Height = 37
            HelpContext = 10540
            Caption = '~Exempt EE Medicare'
            Columns = 2
            DataField = 'FED_TAX_EXEMPT_EE_MEDICARE'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object drgpFed_Tax_Exempt_ER_OASDI: TevDBRadioGroup
            Left = 206
            Top = 74
            Width = 186
            Height = 37
            HelpContext = 10541
            Caption = '~Exempt ER OASDI'
            Columns = 2
            DataField = 'FED_TAX_EXEMPT_ER_OASDI'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
        end
        object fpState: TisUIFashionPanel
          Left = 210
          Top = 368
          Width = 415
          Height = 92
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpState'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Home State'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablHome_State: TevLabel
            Left = 12
            Top = 35
            Width = 84
            Height = 13
            Caption = 'Withholding State'
          end
          object lablHome_SDI_State: TevLabel
            Left = 141
            Top = 35
            Width = 18
            Height = 13
            Caption = 'SDI'
          end
          object lablHome_SUI_State: TevLabel
            Left = 271
            Top = 35
            Width = 18
            Height = 13
            Caption = 'SUI'
          end
          object wwlcHome_State: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 121
            Height = 21
            HelpContext = 10525
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'HOME_CO_STATES_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcHome_SDI_State: TevDBLookupCombo
            Left = 141
            Top = 50
            Width = 121
            Height = 21
            HelpContext = 10525
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'HOME_SDI_CO_STATES_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcHome_SUI_State: TevDBLookupCombo
            Left = 271
            Top = 50
            Width = 121
            Height = 21
            HelpContext = 10525
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE'#9'2'#9'STATE')
            DataField = 'HOME_SUI_CO_STATES_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CO_STATES.CO_STATES
            LookupField = 'CO_STATES_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
      end
    end
    object tshtFederal_Reporting: TTabSheet
      Caption = 'Tax Defaults'
      ImageIndex = 37
      object sbTaxDefaults: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpEFTPS: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 254
          Height = 211
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpEFTPS'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'EFTPS Info'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablEnrollment_Status: TevLabel
            Left = 12
            Top = 35
            Width = 82
            Height = 13
            Caption = 'Enrollment Status'
          end
          object lablEnrollment_Date: TevLabel
            Left = 125
            Top = 35
            Width = 75
            Height = 13
            Caption = 'Enrollment Date'
          end
          object lablSequence_Number: TevLabel
            Left = 12
            Top = 74
            Width = 59
            Height = 13
            Caption = 'Sequence #'
            FocusControl = dedtSequence_Number
          end
          object lablName2: TevLabel
            Left = 12
            Top = 113
            Width = 28
            Height = 13
            Caption = 'Name'
            FocusControl = dedtName2
          end
          object lablPIN_Number: TevLabel
            Left = 12
            Top = 152
            Width = 58
            Height = 13
            Caption = 'PIN Number'
            FocusControl = dedtPIN_Number
          end
          object lablEnrollment_Number: TevLabel
            Left = 125
            Top = 74
            Width = 100
            Height = 13
            Caption = 'Enrollment # / Status'
            FocusControl = dedtEnrollment_Number
          end
          object dedtSequence_Number: TevDBEdit
            Left = 12
            Top = 89
            Width = 105
            Height = 21
            HelpContext = 10648
            DataField = 'EFTPS_SEQUENCE_NUMBER'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtName2: TevDBEdit
            Left = 12
            Top = 128
            Width = 218
            Height = 21
            HelpContext = 10650
            DataField = 'EFTPS_NAME'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtPIN_Number: TevDBEdit
            Left = 12
            Top = 167
            Width = 218
            Height = 21
            HelpContext = 10652
            DataField = 'PIN_NUMBER'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbEnrollment_Status: TevDBComboBox
            Left = 12
            Top = 50
            Width = 105
            Height = 21
            HelpContext = 10646
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'EFTPS_ENROLLMENT_STATUS'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Pending'#9'P'
              'Active'#9'A'
              'Term'#9'T')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object wwdpEFTPS_Enrollment_Date: TevDBDateTimePicker
            Left = 125
            Top = 50
            Width = 105
            Height = 21
            HelpContext = 10647
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'EFTPS_ENROLLMENT_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
          object dedtEnrollment_Number: TevDBEdit
            Left = 125
            Top = 89
            Width = 105
            Height = 21
            HelpContext = 10649
            DataField = 'EFTPS_ENROLLMENT_NUMBER'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpEnlistment: TisUIFashionPanel
          Left = 270
          Top = 8
          Width = 275
          Height = 469
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpEnlistment'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Enlistment'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel31: TevLabel
            Left = 12
            Top = 192
            Width = 134
            Height = 13
            Caption = 'eFile Business Name Control'
            FocusControl = edControlName
          end
          object evLabel1: TevLabel
            Left = 12
            Top = 230
            Width = 124
            Height = 13
            Caption = 'Auto Correct Taxes Up To'
          end
          object lablTaxCoverLetterNotes: TevLabel
            Left = 12
            Top = 269
            Width = 110
            Height = 13
            Caption = 'Tax Cover Letter Notes'
          end
          object drgpPutReturnsOnHold: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 240
            Height = 37
            Caption = '~Put All Tax Returns on Hold'
            Columns = 2
            DataField = 'HOLD_RETURN_QUEUE'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object edControlName: TevDBEdit
            Left = 12
            Top = 206
            Width = 240
            Height = 21
            HelpContext = 10652
            MaxLength = 4
            Picture.PictureMaskFromDataSet = False
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = edControlNameChange
            Glowing = False
          end
          object rgAutoEnlistQ: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 240
            Height = 37
            Caption = '~Auto Enlist Returns'
            Columns = 2
            DataField = 'AUTO_ENLIST'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpDBA_On_Tax_Return: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 240
            Height = 37
            HelpContext = 10528
            Caption = '~Name to Use on Tax Returns'
            Columns = 3
            DataField = 'USE_DBA_ON_TAX_RETURN'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'DBA'
              'Legal'
              'Primary')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'D'
              'L'
              'P')
          end
          object eMINIMUM_TAX_THRESHOLD: TevDBEdit
            Left = 12
            Top = 245
            Width = 240
            Height = 21
            HelpContext = 18021
            DataField = 'MINIMUM_TAX_THRESHOLD'
            DataSource = wwdsMaster
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object drgpExternal_Tax_Export: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 240
            Height = 37
            HelpContext = 10597
            Caption = '~External Tax Export'
            Columns = 2
            DataField = 'EXTERNAL_TAX_EXPORT'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object dmemTaxCoverLetterNotes: TEvDBMemo
            Left = 12
            Top = 284
            Width = 240
            Height = 163
            HelpContext = 10608
            DataField = 'TAX_COVER_LETTER_NOTES'
            DataSource = wwdsMaster
            ScrollBars = ssVertical
            TabOrder = 6
          end
        end
        object fpTaxDefaultOptions: TisUIFashionPanel
          Left = 8
          Top = 227
          Width = 254
          Height = 250
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpTaxDefaultOptions'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Options'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel53: TevLabel
            Left = 12
            Top = 35
            Width = 41
            Height = 13
            Caption = 'Location'
          end
          object evLabel12: TevLabel
            Left = 12
            Top = 191
            Width = 80
            Height = 16
            Caption = '~Worksite Level'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evDBLookupCombo8: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 218
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ACCOUNT_NUMBER'#9'30'#9'Account Number'#9'F'
              'ADDRESS1'#9'30'#9'Address1'#9'F'
              'CITY'#9'20'#9'City'#9'F'
              'ZIP_CODE'#9'10'#9'Zip Code'#9'F'
              'STATE'#9'2'#9'State'#9'F')
            DataField = 'CO_LOCATIONS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_LOCATIONS.CO_LOCATIONS
            LookupField = 'CO_LOCATIONS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object drgpMakeup_Tax_Deduct_Shortfalls: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 218
            Height = 37
            HelpContext = 10645
            Caption = '~FIT/SIT Shortfall Makeup'
            Columns = 2
            DataField = 'MAKE_UP_TAX_DEDUCT_SHORTFALLS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
          object rgCalculateLocalFirst: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 218
            Height = 37
            HelpContext = 10645
            Caption = '~Calculate Locals First'
            Columns = 2
            DataField = 'CALCULATE_LOCALS_FIRST'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBRadioGroup10: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 218
            Height = 37
            HelpContext = 10645
            Caption = '~Calculate States First'
            Columns = 3
            DataField = 'CALCULATE_STATES_FIRST'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'No'
              'State,SUI'
              'SUI,State')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'N'
              'Y'
              'S')
          end
          object evDBComboBox2: TevDBComboBox
            Left = 12
            Top = 206
            Width = 218
            Height = 21
            HelpContext = 10638
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'FEDERAL_TAX_TRANSFER_METHOD'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Division'#9'1'
              'Branch'#9'2'
              'Department'#9'3'
              'Team'#9'4')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
        end
      end
    end
    object tsCashManagement: TTabSheet
      HelpContext = 10669
      Caption = 'Cash Management '
      ImageIndex = 43
      OnEnter = tsCashManagementEnter
      object sbCashManagement: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpCMServices: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 614
          Height = 153
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCMServices'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Services'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablOBC_End_Date: TevLabel
            Left = 502
            Top = 94
            Width = 45
            Height = 13
            Caption = 'End Date'
          end
          object lablOBC_Start_Date: TevLabel
            Left = 404
            Top = 94
            Width = 48
            Height = 13
            Caption = 'Start Date'
          end
          object lablTrust_Service_End_Date: TevLabel
            Left = 306
            Top = 94
            Width = 45
            Height = 13
            Caption = 'End Date'
          end
          object lablTrust_Service_Start_Date: TevLabel
            Left = 208
            Top = 94
            Width = 48
            Height = 13
            Caption = 'Start Date'
          end
          object lablTax_Service_End_Date: TevLabel
            Left = 110
            Top = 94
            Width = 45
            Height = 13
            Caption = 'End Date'
          end
          object lablTax_Service_Start_Date: TevLabel
            Left = 12
            Top = 94
            Width = 48
            Height = 13
            Caption = 'Start Date'
          end
          object drgpTax_Service: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 188
            Height = 57
            HelpContext = 10658
            Caption = '~Tax'
            Columns = 3
            DataField = 'TAX_SERVICE'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
            OnChange = drgpTax_ServiceChange
          end
          object drgpTrust_Service: TevDBRadioGroup
            Left = 208
            Top = 35
            Width = 188
            Height = 57
            HelpContext = 10601
            Caption = '~Trust'
            Columns = 2
            DataField = 'TRUST_SERVICE'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
            OnChange = drgpTrust_ServiceChange
          end
          object drgpOBC_Service: TevDBRadioGroup
            Left = 404
            Top = 35
            Width = 188
            Height = 57
            HelpContext = 10600
            Caption = '~OBC'
            Columns = 3
            DataField = 'OBC'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
          end
          object wwdpOBC_End_Date: TevDBDateTimePicker
            Left = 502
            Top = 109
            Width = 90
            Height = 21
            HelpContext = 10602
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'OBC_END_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 8
          end
          object wwdpOBC_Start_Date: TevDBDateTimePicker
            Left = 404
            Top = 109
            Width = 90
            Height = 21
            HelpContext = 10602
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'OBC_START_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 7
          end
          object wwdpTrust_Service_End_Date: TevDBDateTimePicker
            Left = 306
            Top = 109
            Width = 90
            Height = 21
            HelpContext = 10602
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'TRUST_SERVICE_END_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 5
          end
          object wwdpTrust_Service_Start_Date: TevDBDateTimePicker
            Left = 208
            Top = 109
            Width = 90
            Height = 21
            HelpContext = 10602
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'TRUST_SERVICE_START_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object wwdpTax_Service_End_Date: TevDBDateTimePicker
            Left = 110
            Top = 109
            Width = 90
            Height = 21
            HelpContext = 10602
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'TAX_SERVICE_END_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 2
          end
          object wwDBDateTimePicker1: TevDBDateTimePicker
            Left = 12
            Top = 109
            Width = 90
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'TAX_SERVICE_START_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
        end
        object fpBilling: TisUIFashionPanel
          Left = 630
          Top = 8
          Width = 269
          Height = 153
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpBilling'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Billing'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablBilling_Information: TevLabel
            Left = 12
            Top = 35
            Width = 61
            Height = 16
            Caption = '~Information'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablBilling_Level: TevLabel
            Left = 12
            Top = 74
            Width = 35
            Height = 16
            Caption = '~Level'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwlcBilling_Information: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 233
            Height = 21
            HelpContext = 10594
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME'
              'ADDRESS1'#9'30'#9'ADDRESS1'
              'ADDRESS2'#9'30'#9'ADDRESS2'
              'CITY'#9'20'#9'CITY'
              'STATE'#9'2'#9'STATE'
              'ZIP_CODE'#9'10'#9'ZIP_CODE')
            DataField = 'CL_BILLING_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_BILLING.CL_BILLING
            LookupField = 'CL_BILLING_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwcbBilling_Level: TevDBComboBox
            Left = 12
            Top = 89
            Width = 233
            Height = 21
            HelpContext = 10595
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'BILLING_LEVEL'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Company'#9'0'
              'Division'#9'1'
              'Branch'#9'2'
              'Department'#9'3'
              'Team'#9'4')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object cbBillingCPA: TevDBCheckBox
            Left = 12
            Top = 117
            Width = 92
            Height = 18
            Caption = 'Check To CPA'
            DataField = 'BILLING_CHECK_CPA'
            DataSource = wwdsMaster
            TabOrder = 2
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
        end
        object fpSBBankAccounts: TisUIFashionPanel
          Left = 630
          Top = 169
          Width = 269
          Height = 289
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSBBankAccounts'
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Service Bureau Bank Accounts'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablSB_Trust_Account_Number: TevLabel
            Left = 12
            Top = 35
            Width = 24
            Height = 13
            Caption = 'Trust'
          end
          object lablSB_Tax_Bank_Account_Number: TevLabel
            Left = 12
            Top = 74
            Width = 18
            Height = 13
            Caption = 'Tax'
          end
          object lablSB_ACH_Bank_Account_Number: TevLabel
            Left = 12
            Top = 113
            Width = 213
            Height = 13
            AutoSize = False
            Caption = 'Cash'
          end
          object lablSB_Billing_Bank_Account_Number: TevLabel
            Left = 12
            Top = 152
            Width = 27
            Height = 13
            Caption = 'Billing'
          end
          object lablSB_OBC_Account_Nbr: TevLabel
            Left = 12
            Top = 230
            Width = 22
            Height = 13
            Caption = 'OBC'
          end
          object lablSB_Workers_Comp_Bank_Account_Number: TevLabel
            Left = 12
            Top = 191
            Width = 70
            Height = 13
            Caption = 'Workers Comp'
          end
          object wwlcSB_Trust_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 233
            Height = 21
            HelpContext = 10591
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
            DataField = 'TRUST_SB_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = cdsSbBankAccountsForTrust
            LookupField = 'SB_BANK_ACCOUNTS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcSB_Tax_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 233
            Height = 21
            HelpContext = 10588
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
            DataField = 'TAX_SB_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = cdsSbBankAccountsForTax
            LookupField = 'SB_BANK_ACCOUNTS_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcSB_ACH_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 233
            Height = 21
            HelpContext = 10582
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
            DataField = 'ACH_SB_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = cdsSbBankAccountsForAch
            LookupField = 'SB_BANK_ACCOUNTS_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcSB_Billing_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 233
            Height = 21
            HelpContext = 10590
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
            DataField = 'BILLING_SB_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = cdsSbBankAccountsForBilling
            LookupField = 'SB_BANK_ACCOUNTS_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcSB_OBC_Account_Nbr: TevDBLookupCombo
            Left = 12
            Top = 245
            Width = 233
            Height = 21
            HelpContext = 10581
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
            DataField = 'SB_OBC_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = cdsSbBankAccountsForOBC
            LookupField = 'SB_BANK_ACCOUNTS_NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcSB_Workers_Comp_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 206
            Width = 233
            Height = 21
            HelpContext = 10589
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
            DataField = 'W_COMP_SB_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            LookupTable = cdsSbBankAccountsForWC
            LookupField = 'SB_BANK_ACCOUNTS_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
        end
        object fpCashManagement: TisUIFashionPanel
          Left = 8
          Top = 169
          Width = 614
          Height = 366
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCashManagement'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Cash Management'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel16: TevLabel
            Left = 12
            Top = 35
            Width = 135
            Height = 13
            Caption = 'Name for Cash Management'
            FocusControl = evDBEdit15
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel42: TevLabel
            Left = 378
            Top = 35
            Width = 132
            Height = 13
            Caption = 'Maximum Amount for Payroll'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel43: TevLabel
            Left = 378
            Top = 74
            Width = 163
            Height = 13
            Caption = 'Maximum Amount for Tax Impound'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel44: TevLabel
            Left = 378
            Top = 113
            Width = 161
            Height = 13
            Caption = 'Maximum Amount for DD Impound'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel45: TevLabel
            Left = 378
            Top = 230
            Width = 133
            Height = 16
            Caption = '~Payroll Process Limitations'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel47: TevLabel
            Left = 378
            Top = 269
            Width = 124
            Height = 16
            Caption = '~ACH Process Limitations'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel48: TevLabel
            Left = 378
            Top = 308
            Width = 148
            Height = 16
            Caption = '~Tax Exception Impound Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablMaximum_Hours_Check: TevLabel
            Left = 378
            Top = 152
            Width = 124
            Height = 13
            Caption = 'Maximum Hours on Check'
          end
          object lablMaximum_Dollars_Check: TevLabel
            Left = 378
            Top = 191
            Width = 128
            Height = 13
            Caption = 'Maximum Dollars on Check'
          end
          object EvBevel5: TEvBevel
            Left = 12
            Top = 88
            Width = 348
            Height = 3
            Shape = bsTopLine
          end
          object evDBEdit15: TevDBEdit
            Left = 12
            Top = 50
            Width = 348
            Height = 21
            HelpContext = 10580
            DataField = 'BANK_ACCOUNT_REGISTER_NAME'
            DataSource = wwdsMaster
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit11: TevDBEdit
            Left = 378
            Top = 50
            Width = 212
            Height = 21
            HelpContext = 10580
            DataField = 'CO_MAX_AMOUNT_FOR_PAYROLL'
            DataSource = wwdsMaster
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit12: TevDBEdit
            Left = 378
            Top = 89
            Width = 212
            Height = 21
            HelpContext = 10580
            DataField = 'CO_MAX_AMOUNT_FOR_TAX_IMPOUND'
            DataSource = wwdsMaster
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit13: TevDBEdit
            Left = 378
            Top = 128
            Width = 212
            Height = 21
            HelpContext = 10580
            DataField = 'CO_MAX_AMOUNT_FOR_DD'
            DataSource = wwdsMaster
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBComboBox12: TevDBComboBox
            Left = 378
            Top = 245
            Width = 212
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CO_PAYROLL_PROCESS_LIMITATIONS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmProcessLimitations
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
          end
          object evDBComboBox13: TevDBComboBox
            Left = 378
            Top = 284
            Width = 212
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CO_ACH_PROCESS_LIMITATIONS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 8
            UnboundDataType = wwDefault
          end
          object evDBComboBox14: TevDBComboBox
            Left = 378
            Top = 323
            Width = 212
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CO_EXCEPTION_PAYMENT_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 9
            UnboundDataType = wwDefault
          end
          object wwdpMaximum_Hours_On_Check: TevDBEdit
            Left = 378
            Top = 167
            Width = 212
            Height = 21
            HelpContext = 10553
            DataField = 'MAXIMUM_HOURS_ON_CHECK'
            DataSource = wwdsMaster
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwDBEdit2: TevDBEdit
            Left = 378
            Top = 206
            Width = 212
            Height = 21
            HelpContext = 10554
            DataField = 'MAXIMUM_DOLLARS_ON_CHECK'
            DataSource = wwdsMaster
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object gbImpound: TevGroupBox
            Left = 12
            Top = 104
            Width = 348
            Height = 240
            Caption = 'Impound Type'
            TabOrder = 1
            object evLabel32: TevLabel
              Left = 12
              Top = 33
              Width = 33
              Height = 16
              Caption = '~Trust'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel41: TevLabel
              Left = 12
              Top = 72
              Width = 27
              Height = 16
              Caption = '~Tax'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel40: TevLabel
              Left = 12
              Top = 111
              Width = 76
              Height = 16
              Caption = '~Direct Deposit'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel34: TevLabel
              Left = 12
              Top = 150
              Width = 36
              Height = 16
              Caption = '~Billing'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel33: TevLabel
              Left = 12
              Top = 189
              Width = 79
              Height = 16
              Caption = '~Workers Comp'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object lablDebit_Number_Days_Prior_PR: TevLabel
              Left = 238
              Top = 18
              Width = 86
              Height = 13
              Caption = 'Debit # Days Prior'
              FocusControl = dedtDebit_Number_Days_Prior_PR
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel7: TevLabel
              Left = 241
              Top = 33
              Width = 81
              Height = 13
              AutoSize = False
              Caption = '~to Check Date'
            end
            object TrustComboBox: TevDBComboBox
              Left = 12
              Top = 48
              Width = 210
              Height = 21
              HelpContext = 11507
              ShowButton = True
              Style = csDropDownList
              MapList = False
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'TRUST_IMPOUND'
              DataSource = wwdsDetail
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              PopupMenu = pmTrustImpound
              Sorted = False
              TabOrder = 0
              UnboundDataType = wwDefault
            end
            object TaxComboBox: TevDBComboBox
              Left = 12
              Top = 87
              Width = 210
              Height = 21
              HelpContext = 11507
              ShowButton = True
              Style = csDropDownList
              MapList = False
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'TAX_IMPOUND'
              DataSource = wwdsDetail
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              PopupMenu = pmTaxImpound
              Sorted = False
              TabOrder = 2
              UnboundDataType = wwDefault
            end
            object WCComboBox: TevDBComboBox
              Left = 12
              Top = 204
              Width = 210
              Height = 21
              HelpContext = 11507
              ShowButton = True
              Style = csDropDownList
              MapList = False
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'WC_IMPOUND'
              DataSource = wwdsDetail
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              PopupMenu = pmWCImpound
              Sorted = False
              TabOrder = 8
              UnboundDataType = wwDefault
            end
            object BillingComboBox: TevDBComboBox
              Left = 12
              Top = 165
              Width = 210
              Height = 21
              HelpContext = 11507
              ShowButton = True
              Style = csDropDownList
              MapList = False
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'BILLING_IMPOUND'
              DataSource = wwdsDetail
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              PopupMenu = pmBillingImpound
              Sorted = False
              TabOrder = 6
              UnboundDataType = wwDefault
            end
            object DDComboBox: TevDBComboBox
              Left = 12
              Top = 126
              Width = 210
              Height = 21
              HelpContext = 11507
              ShowButton = True
              Style = csDropDownList
              MapList = False
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'DD_IMPOUND'
              DataSource = wwdsDetail
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              PopupMenu = pmDDImpound
              Sorted = False
              TabOrder = 4
              UnboundDataType = wwDefault
            end
            object dedtDebit_Number_Days_Prior_PR: TevDBEdit
              Left = 230
              Top = 48
              Width = 103
              Height = 21
              HelpContext = 10580
              DataField = 'DEBIT_NUMBER_DAYS_PRIOR_PR'
              DataSource = wwdsMaster
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object dedtDebit_Number_Days_Prior_Tax: TevDBEdit
              Left = 230
              Top = 87
              Width = 103
              Height = 21
              HelpContext = 10580
              DataField = 'DEBIT_NUMBER_OF_DAYS_PRIOR_TAX'
              DataSource = wwdsMaster
              TabOrder = 3
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object dedtDebit_Number_Days_Prior_DD: TevDBEdit
              Left = 230
              Top = 126
              Width = 103
              Height = 21
              HelpContext = 10580
              DataField = 'DEBIT_NUMBER_OF_DAYS_PRIOR_DD'
              DataSource = wwdsMaster
              TabOrder = 5
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object dedtDebit_Number_Days_Prior_Bill: TevDBEdit
              Left = 230
              Top = 165
              Width = 103
              Height = 21
              HelpContext = 10580
              DataField = 'DEBIT_NUMBER_DAYS_PRIOR_BILL'
              DataSource = wwdsMaster
              TabOrder = 7
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object dedtDebit_Number_Days_Prior_WC: TevDBEdit
              Left = 230
              Top = 204
              Width = 103
              Height = 21
              HelpContext = 10580
              DataField = 'DEBIT_NUMBER_DAYS_PRIOR_WC'
              DataSource = wwdsMaster
              TabOrder = 9
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
          end
        end
        object fpCMInvoicing: TisUIFashionPanel
          Left = 630
          Top = 466
          Width = 269
          Height = 212
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCMInvoicing'
          Color = 14737632
          TabOrder = 6
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Invoicing'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel26: TevLabel
            Left = 12
            Top = 35
            Width = 84
            Height = 13
            Caption = 'Number of Copies'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablInvoiceNotes: TevLabel
            Left = 12
            Top = 74
            Width = 66
            Height = 13
            Caption = 'Invoice Notes'
          end
          object evDBComboBox7: TevDBSpinEdit
            Left = 12
            Top = 50
            Width = 84
            Height = 21
            HelpContext = 10595
            Increment = 1.000000000000000000
            MaxValue = 255.000000000000000000
            MinValue = 1.000000000000000000
            Value = 1.000000000000000000
            DataField = 'NUMBER_OF_INVOICE_COPIES'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object cbProrateFlatFee: TevDBCheckBox
            Left = 105
            Top = 52
            Width = 143
            Height = 17
            Caption = 'Prorate Flat Fee for DBDT'
            DataField = 'PRORATE_FLAT_FEE_FOR_DBDT'
            DataSource = wwdsMaster
            TabOrder = 1
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object dmemInvoiceNotes: TEvDBMemo
            Left = 12
            Top = 89
            Width = 233
            Height = 100
            HelpContext = 10673
            DataField = 'INVOICE_NOTES'
            DataSource = wwdsMaster
            ScrollBars = ssVertical
            TabOrder = 2
          end
        end
        object fpACH: TisUIFashionPanel
          Left = 467
          Top = 545
          Width = 155
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpACH'
          Color = 14737632
          TabOrder = 5
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'ACH'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel30: TevLabel
            Left = 12
            Top = 35
            Width = 98
            Height = 13
            Caption = 'ACH Audit Character'
          end
          object evDBEdit6: TevDBEdit
            Left = 12
            Top = 50
            Width = 120
            Height = 21
            HelpContext = 10637
            DataField = 'WELLS_FARGO_ACH_FLAG'
            DataSource = wwdsMaster
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object btnChangePrenote: TevBitBtn
            Left = 12
            Top = 85
            Width = 120
            Height = 25
            Caption = 'Change Prenote'
            TabOrder = 1
            OnClick = btnChangePrenoteClick
            Color = clBlack
            NumGlyphs = 2
            Margin = 0
          end
        end
        object fpMgmtSettings: TisUIFashionPanel
          Left = 8
          Top = 545
          Width = 451
          Height = 133
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpMgmtSettings'
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Management Settings'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel49: TevLabel
            Left = 240
            Top = 74
            Width = 188
            Height = 13
            Caption = 'Days Prior To Check Date for Backdate'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablCredit_Hold: TevLabel
            Left = 240
            Top = 35
            Width = 90
            Height = 16
            Caption = '~Credit Hold Level'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evDBRadioGroup11: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 220
            Height = 76
            HelpContext = 10644
            Caption = '~Payroll Requires Manager Approval'
            Columns = 2
            DataField = 'PAYROLL_REQUIRES_MGR_APPROVAL'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object wwseDaysPriorToCheckDate: TevDBSpinEdit
            Left = 240
            Top = 89
            Width = 188
            Height = 21
            HelpContext = 11510
            Increment = 1.000000000000000000
            MaxValue = 365.000000000000000000
            DataField = 'DAYS_PRIOR_TO_CHECK_DATE'
            DataSource = wwdsMaster
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object wwcbCredit_Hold: TevDBComboBox
            Left = 240
            Top = 51
            Width = 188
            Height = 21
            HelpContext = 10596
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CREDIT_HOLD'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'None'#9'N'
              'Low'#9'L'
              'Medium'#9'M'
              'High'#9'H')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
            OnChange = wwcbCredit_HoldChange
          end
        end
      end
    end
    object tshtWComp_And_Benefits: TTabSheet
      Caption = 'WComp && Benefits'
      ImageIndex = 44
      object sbWCandBenies: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpWorkersComp: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 336
          Height = 250
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Workers Comp'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablClient_Workers_Comp_Agency: TevLabel
            Left = 12
            Top = 113
            Width = 138
            Height = 13
            Caption = 'Client Workers Comp Agency'
          end
          object lablWorkers_Comp_Policy_ID: TevLabel
            Left = 12
            Top = 35
            Width = 115
            Height = 13
            Caption = 'Workers Comp Policy ID'
            FocusControl = dedtWorkers_Comp_Policy_ID
          end
          object lablWorkers_Comp_Number: TevLabel
            Left = 93
            Top = 74
            Width = 107
            Height = 13
            Caption = 'Default Workers Comp'
          end
          object Label1: TevLabel
            Left = 12
            Top = 74
            Width = 51
            Height = 13
            Caption = 'W/C Code'
          end
          object Label27: TevLabel
            Left = 262
            Top = 74
            Width = 25
            Height = 13
            Caption = 'State'
          end
          object lablDiscountRate: TevLabel
            Left = 12
            Top = 152
            Width = 68
            Height = 13
            Caption = 'Discount Rate'
          end
          object lablModRate: TevLabel
            Left = 166
            Top = 152
            Width = 47
            Height = 13
            Caption = 'Mod Rate'
          end
          object evLabel51: TevLabel
            Left = 12
            Top = 191
            Width = 128
            Height = 13
            Caption = 'Period Begin Date (mm/dd)'
          end
          object dtFiscalBeginDate: TevDBDateTimePicker
            Left = 12
            Top = 206
            Width = 146
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'WC_FISCAL_YEAR_BEGIN'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 8
            OnCloseUp = dtFiscalBeginDateCloseUp
            OnExit = dtFiscalBeginDateCloseUp
          end
          object edFiscalBeginDate: TevEdit
            Left = 12
            Top = 206
            Width = 146
            Height = 21
            ReadOnly = True
            TabOrder = 7
            OnClick = edFiscalBeginDateClick
          end
          object dedtWorkers_Comp_Policy_ID: TevDBEdit
            Left = 12
            Top = 50
            Width = 300
            Height = 21
            HelpContext = 10623
            DataField = 'WORKERS_COMP_POLICY_ID'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcClient_Workers_Comp_Agency: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 300
            Height = 21
            HelpContext = 10625
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'40'#9'AGENCY_NAME')
            DataField = 'WORKERS_COMP_CL_AGENCY_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_AGENCY.CL_AGENCY
            LookupField = 'CL_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwlcWorkers_Comp_Number: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 73
            Height = 21
            HelpContext = 10624
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'WORKERS_COMP_CODE'#9'5'#9'W/C Code'#9'F'
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F'
              'Co_State_Lookup'#9'20'#9'Co_State_Lookup'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
          end
          object wwDBLookupCombo1: TevDBLookupCombo
            Left = 93
            Top = 89
            Width = 161
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 2
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwDBLookupCombo4: TevDBLookupCombo
            Left = 262
            Top = 89
            Width = 50
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'Co_State_Lookup'#9'20'#9'Co_State_Lookup'#9'F')
            DataField = 'CO_WORKERS_COMP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
            LookupField = 'CO_WORKERS_COMP_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 3
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwdeDiscountRate: TevDBEdit
            Left = 12
            Top = 167
            Width = 146
            Height = 21
            HelpContext = 10671
            DataField = 'DISCOUNT_RATE'
            DataSource = wwdsMaster
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeModRate: TevDBEdit
            Left = 166
            Top = 167
            Width = 146
            Height = 21
            HelpContext = 10672
            DataField = 'MOD_RATE'
            DataSource = wwdsMaster
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object fpBenefits: TisUIFashionPanel
          Left = 352
          Top = 8
          Width = 342
          Height = 250
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Benefits'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel5: TevLabel
            Left = 12
            Top = 35
            Width = 81
            Height = 13
            Caption = 'COBRA Fee Due'
          end
          object evLabel6: TevLabel
            Left = 169
            Top = 35
            Width = 80
            Height = 13
            Caption = 'Notification Days'
          end
          object evLabel8: TevLabel
            Left = 12
            Top = 74
            Width = 98
            Height = 13
            Caption = 'Eligible Confirm Days'
          end
          object evLabel9: TevLabel
            Left = 169
            Top = 74
            Width = 79
            Height = 13
            Caption = 'SS Disability Fee'
          end
          object lablRetirement_Age: TevLabel
            Left = 169
            Top = 191
            Width = 73
            Height = 13
            Caption = 'Retirement Age'
            FocusControl = dedtRetirement_Age
          end
          object lablMaximum_Group_Term_Life: TevLabel
            Left = 12
            Top = 191
            Width = 123
            Height = 13
            Caption = 'Maximum Group Term Life'
          end
          object evDBSpinEdit2: TevDBSpinEdit
            Left = 12
            Top = 50
            Width = 149
            Height = 21
            Increment = 1.000000000000000000
            DataField = 'COBRA_FEE_DAY_OF_MONTH_DUE'
            DataSource = wwdsMaster
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object evDBSpinEdit3: TevDBSpinEdit
            Left = 169
            Top = 50
            Width = 149
            Height = 21
            Increment = 1.000000000000000000
            DataField = 'COBRA_NOTIFICATION_DAYS'
            DataSource = wwdsMaster
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object evDBSpinEdit4: TevDBSpinEdit
            Left = 12
            Top = 89
            Width = 149
            Height = 21
            Increment = 1.000000000000000000
            DataField = 'COBRA_ELIGIBILITY_CONFIRM_DAYS'
            DataSource = wwdsMaster
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object evDBEdit4: TevDBEdit
            Left = 169
            Top = 89
            Width = 149
            Height = 21
            DataField = 'SS_DISABILITY_ADMIN_FEE'
            DataSource = wwdsMaster
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object cbChargeAdminFee: TevDBCheckBox
            Left = 12
            Top = 120
            Width = 209
            Height = 17
            Caption = 'Charge COBRA Admin Fee'
            DataField = 'CHARGE_COBRA_ADMIN_FEE'
            DataSource = wwdsMaster
            TabOrder = 4
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object dedtRetirement_Age: TevDBEdit
            Left = 169
            Top = 206
            Width = 149
            Height = 21
            HelpContext = 10621
            DataField = 'RETIREMENT_AGE'
            DataSource = wwdsMaster
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeMaximum_Group_Term_Life: TevDBEdit
            Left = 12
            Top = 206
            Width = 149
            Height = 21
            HelpContext = 10622
            DataField = 'MAXIMUM_GROUP_TERM_LIFE'
            DataSource = wwdsMaster
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBCheckBox2: TevDBCheckBox
            Left = 12
            Top = 144
            Width = 85
            Height = 17
            Caption = 'Medical Plan'
            DataField = 'MEDICAL_PLAN'
            DataSource = wwdsMaster
            TabOrder = 5
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object evDBCheckBox3: TevDBCheckBox
            Left = 12
            Top = 168
            Width = 101
            Height = 17
            Caption = 'Smoker Default'
            DataField = 'SMOKER_DEFAULT'
            DataSource = wwdsMaster
            TabOrder = 6
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object rgBenefitEligible: TevDBRadioGroup
            Left = 169
            Top = 120
            Width = 149
            Height = 37
            Caption = '~Benefits Eligible Default'
            Columns = 2
            DataField = 'BENEFITS_ELIGIBLE_DEFAULT'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            PopupMenu = pmBenefitsEligibleDefault
            TabOrder = 9
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object fpHardware: TisUIFashionPanel
          Left = 8
          Top = 264
          Width = 688
          Height = 94
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Hardware'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablHardware_O_S: TevLabel
            Left = 124
            Top = 35
            Width = 76
            Height = 16
            Caption = '~Hardware O S'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablModem_Speed: TevLabel
            Left = 248
            Top = 35
            Width = 78
            Height = 16
            Caption = '~Modem Speed'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablModem_Connection_Type: TevLabel
            Left = 368
            Top = 35
            Width = 128
            Height = 16
            Caption = '~Modem Connection Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablLast_Call_In_Date: TevLabel
            Left = 523
            Top = 35
            Width = 78
            Height = 13
            Caption = 'Last Call In Date'
          end
          object wwdcHardware_O_S: TevDBComboBox
            Left = 124
            Top = 50
            Width = 110
            Height = 21
            HelpContext = 10606
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'HARDWARE_O_S'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Windows 95'#9'5'
              'Windows 98'#9'8'
              'Windows 3.1'#9'3'
              'Windows NT'#9'N'
              'MS-DOS'#9'D'
              'OS/2 Warp'#9'W'
              'Mac OS'#9'M'
              'Unix'#9'U'
              'Other'#9'O'
              'N/A'#9'A')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object wwcbModem_Speed: TevDBComboBox
            Left = 248
            Top = 50
            Width = 110
            Height = 21
            HelpContext = 10612
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'MODEM_SPEED'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              '28.8K'#9'2'
              '33.6K'#9'3'
              '56K'#9'5'
              '64K'#9'6'
              '128K'#9'1'
              'Other'#9'O'
              'N/A'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object wwcbModem_Connection_Type: TevDBComboBox
            Left = 368
            Top = 50
            Width = 140
            Height = 21
            HelpContext = 10613
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'MODEM_CONNECTION_TYPE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Regular Direct'#9'D'
              'Regular Switch'#9'S'
              'ISDN'#9'I'
              'Leased'#9'L'
              'Other'#9'O')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object drgpRemote: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 101
            Height = 37
            HelpContext = 10604
            Caption = '~Remote'
            Columns = 2
            DataField = 'REMOTE'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object wwdpLast_Call_In_Date: TevDBDateTimePicker
            Left = 522
            Top = 50
            Width = 110
            Height = 21
            HelpContext = 10617
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'LAST_CALL_IN_DATE'
            DataSource = wwdsMaster
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
        end
      end
    end
    object GeneralLedger: TTabSheet
      Caption = 'General Ledger'
      ImageIndex = 45
      object sbGeneralLedger: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpGL: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 675
          Height = 395
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'General Ledger'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label13: TevLabel
            Left = 12
            Top = 35
            Width = 62
            Height = 13
            Caption = 'Format String'
            FocusControl = DBEdit12
          end
          object Label3: TevLabel
            Left = 336
            Top = 121
            Width = 27
            Height = 13
            Caption = 'Billing'
            FocusControl = DBEdit3
          end
          object Label35: TevLabel
            Left = 336
            Top = 237
            Width = 52
            Height = 13
            Caption = 'Tax Check'
            FocusControl = DBEdit20
          end
          object Label37: TevLabel
            Left = 12
            Top = 121
            Width = 35
            Height = 13
            Caption = 'Federal'
            FocusControl = DBEdit22
          end
          object Label38: TevLabel
            Left = 12
            Top = 150
            Width = 50
            Height = 13
            Caption = 'EE OASDI'
            FocusControl = DBEdit23
          end
          object Label39: TevLabel
            Left = 12
            Top = 179
            Width = 51
            Height = 13
            Caption = 'ER OASDI'
            FocusControl = DBEdit24
          end
          object Label40: TevLabel
            Left = 12
            Top = 208
            Width = 61
            Height = 13
            Caption = 'EE Medicare'
            FocusControl = DBEdit25
          end
          object Label41: TevLabel
            Left = 12
            Top = 237
            Width = 62
            Height = 13
            Caption = 'ER Medicare'
            FocusControl = DBEdit26
          end
          object Label42: TevLabel
            Left = 12
            Top = 266
            Width = 17
            Height = 13
            Caption = 'FUI'
            FocusControl = DBEdit27
          end
          object Label43: TevLabel
            Left = 12
            Top = 295
            Width = 17
            Height = 13
            Caption = 'EIC'
            FocusControl = DBEdit28
          end
          object Label44: TevLabel
            Left = 336
            Top = 150
            Width = 96
            Height = 13
            Caption = 'Backup Withholding'
            FocusControl = DBEdit29
          end
          object Label45: TevLabel
            Left = 336
            Top = 179
            Width = 68
            Height = 13
            Caption = 'Trust Impound'
            FocusControl = DBEdit30
          end
          object evLabel10: TevLabel
            Left = 336
            Top = 208
            Width = 62
            Height = 13
            Caption = 'Tax Impound'
            FocusControl = evDBEdit5
          end
          object evLabel37: TevLabel
            Left = 336
            Top = 266
            Width = 69
            Height = 13
            Caption = 'VT Healthcare'
            FocusControl = evDBEdit8
          end
          object evLabel39: TevLabel
            Left = 336
            Top = 295
            Width = 60
            Height = 13
            Caption = 'UI Rounding'
            FocusControl = evDBEdit10
          end
          object evLabel55: TevLabel
            Left = 161
            Top = 95
            Width = 18
            Height = 16
            Caption = 'DR'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object evLabel56: TevLabel
            Left = 269
            Top = 95
            Width = 18
            Height = 16
            Caption = 'CR'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 485
            Top = 95
            Width = 18
            Height = 16
            Caption = 'DR'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object evLabel27: TevLabel
            Left = 593
            Top = 95
            Width = 18
            Height = 16
            Caption = 'CR'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object evLabel36: TevLabel
            Left = 12
            Top = 95
            Width = 29
            Height = 16
            Caption = 'Tags'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EvBevel6: TEvBevel
            Left = 12
            Top = 324
            Width = 641
            Height = 3
            Shape = bsTopLine
          end
          object Label2: TevLabel
            Left = 12
            Top = 337
            Width = 44
            Height = 13
            Caption = 'Company'
            FocusControl = DBEdit2
          end
          object Label29: TevLabel
            Left = 120
            Top = 337
            Width = 69
            Height = 13
            Caption = 'Net Payroll CR'
            FocusControl = DBEdit1
          end
          object Label33: TevLabel
            Left = 228
            Top = 337
            Width = 95
            Height = 13
            Caption = '3rd Party Check DR'
            FocusControl = DBEdit18
          end
          object Label34: TevLabel
            Left = 336
            Top = 337
            Width = 82
            Height = 13
            Caption = '3rd Party Tax DR'
            FocusControl = DBEdit19
          end
          object evLabel4: TevLabel
            Left = 444
            Top = 337
            Width = 66
            Height = 13
            Caption = 'ER Match CR'
            FocusControl = evDBEdit3
          end
          object lblCobraDR: TevLabel
            Left = 552
            Top = 337
            Width = 77
            Height = 13
            Caption = 'Cobra Credit DR'
            FocusControl = edCobraCreditTagDR
          end
          object evLabel38: TevLabel
            Left = 336
            Top = 95
            Width = 29
            Height = 16
            Caption = 'Tags'
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EvBevel7: TEvBevel
            Left = 12
            Top = 83
            Width = 641
            Height = 3
            Shape = bsTopLine
          end
          object DBEdit12: TevDBEdit
            Left = 12
            Top = 50
            Width = 317
            Height = 21
            HelpContext = 10668
            DataField = 'GENERAL_LEDGER_FORMAT_STRING'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit3: TevDBEdit
            Left = 444
            Top = 117
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'BILLING_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 15
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit4: TevDBEdit
            Left = 228
            Top = 117
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'FEDERAL_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit5: TevDBEdit
            Left = 228
            Top = 146
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'EE_OASDI_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit6: TevDBEdit
            Left = 228
            Top = 175
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'ER_OASDI_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit7: TevDBEdit
            Left = 228
            Top = 204
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'EE_MEDICARE_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit8: TevDBEdit
            Left = 228
            Top = 233
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'ER_MEDICARE_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 10
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit9: TevDBEdit
            Left = 228
            Top = 262
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'FUI_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 12
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit10: TevDBEdit
            Left = 228
            Top = 291
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'EIC_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 14
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit11: TevDBEdit
            Left = 552
            Top = 146
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'BACKUP_W_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 18
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit13: TevDBEdit
            Left = 552
            Top = 204
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'TAX_IMP_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 22
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit14: TevDBEdit
            Left = 552
            Top = 175
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'TRUST_IMP_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 20
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit20: TevDBEdit
            Left = 444
            Top = 233
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'TRUST_CHK_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 23
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit22: TevDBEdit
            Left = 120
            Top = 117
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'FEDERAL_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit23: TevDBEdit
            Left = 120
            Top = 146
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'EE_OASDI_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit24: TevDBEdit
            Left = 120
            Top = 175
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'ER_OASDI_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit25: TevDBEdit
            Left = 120
            Top = 204
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'EE_MEDICARE_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit26: TevDBEdit
            Left = 120
            Top = 233
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'ER_MEDICARE_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit27: TevDBEdit
            Left = 120
            Top = 262
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'FUI_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 11
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit28: TevDBEdit
            Left = 120
            Top = 291
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'EIC_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 13
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit29: TevDBEdit
            Left = 444
            Top = 146
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'BACKUP_W_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 17
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit30: TevDBEdit
            Left = 444
            Top = 175
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'TRUST_IMP_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 19
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit1: TevDBEdit
            Left = 552
            Top = 117
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'BILLING_EXP_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 16
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit5: TevDBEdit
            Left = 444
            Top = 204
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'ER_MEDICARE_EXP_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 21
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit2: TevDBEdit
            Left = 552
            Top = 233
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'TRUST_CHK_OFFSET_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 24
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit7: TevDBEdit
            Left = 552
            Top = 262
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'VT_Healthcare_GL_Tag'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 26
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit8: TevDBEdit
            Left = 444
            Top = 262
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'VT_Healthcare_Offset_GL_Tag'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 25
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit9: TevDBEdit
            Left = 552
            Top = 291
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'UI_Rounding_GL_Tag'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 28
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit10: TevDBEdit
            Left = 444
            Top = 291
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'UI_Rounding_Offset_GL_Tag'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 27
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit2: TevDBEdit
            Left = 12
            Top = 352
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 29
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit1: TevDBEdit
            Left = 120
            Top = 352
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'NET_PAY_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 30
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit18: TevDBEdit
            Left = 228
            Top = 352
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'THRD_P_CHK_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 31
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit19: TevDBEdit
            Left = 336
            Top = 352
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'THRD_P_TAX_GENERAL_LEDGER_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 32
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit3: TevDBEdit
            Left = 444
            Top = 352
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'ER_OASDI_EXP_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 33
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edCobraCreditTagDR: TevDBEdit
            Left = 552
            Top = 352
            Width = 100
            Height = 21
            HelpContext = 10668
            DataField = 'COBRA_CREDIT_GL_TAG'
            DataSource = wwdsMaster
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 34
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
      end
    end
    object tshtRemote_Client: TTabSheet
      Caption = 'Payroll Products'
      ImageIndex = 46
      object sbRemoteClient: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object lablTransmission_Destination: TevLabel
          Left = 48
          Top = 520
          Width = 117
          Height = 13
          Caption = 'Transmission Destination'
          Visible = False
        end
        object wwcbTransmissionDestination: TevDBComboBox
          Left = 48
          Top = 536
          Width = 225
          Height = 21
          HelpContext = 10615
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = True
          AutoDropDown = True
          DataField = 'TRANSMISSION_DESTINATION'
          DataSource = wwdsMaster
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 5
          UnboundDataType = wwDefault
          Visible = False
        end
        object fpSwipeClock: TisUIFashionPanel
          Left = 286
          Top = 346
          Width = 176
          Height = 109
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Swipe Clock Login'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel52: TevLabel
            Left = 12
            Top = 35
            Width = 58
            Height = 13
            Caption = 'Site Number'
          end
          object evSwipeClockSiteNumber: TevDBEdit
            Left = 12
            Top = 50
            Width = 142
            Height = 21
            HelpContext = 10610
            DataField = 'WC_OFFS_BANK_ACCOUNT_NBR'
            DataSource = wwdsMaster
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object isUIFashionPanel1: TisUIFashionPanel
          Left = 286
          Top = 204
          Width = 176
          Height = 136
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Analytics'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel69: TevLabel
            Left = 12
            Top = 75
            Width = 80
            Height = 16
            Caption = '~Analytics Level'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object rgIncludeAnalytics: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 142
            Height = 37
            HelpContext = 10604
            Caption = '~Include in Analytics'
            Columns = 2
            DataField = 'ENABLE_ANALYTICS'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            PopupMenu = pmEnableAnalytics
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
            OnChange = rgIncludeAnalyticsChange
          end
          object cbAnalyticsLevel: TevDBComboBox
            Left = 12
            Top = 91
            Width = 142
            Height = 21
            HelpContext = 18020
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ANALYTICS_LICENSE'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Windows NT 3.x'#9'3'
              'Windows NT 4.x'#9'4'
              'Windows NT 5.x'#9'5'
              'Novell 3.x'#9'V'
              'Novell 4.x'#9'E'
              'Novell 5.x'#9'J'
              'Lantastic'#9'L'
              'AS/400'#9'4'
              'Unix'#9'U'
              'Other'#9'O'
              'None'#9'N')
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmAnalyticsLevel
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
            OnChange = cbAnalyticsLevelChange
          end
        end
        object fpSelfServe: TisUIFashionPanel
          Left = 11
          Top = 146
          Width = 270
          Height = 309
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Employee Portal'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel64: TevLabel
            Left = 12
            Top = 95
            Width = 44
            Height = 16
            Caption = '~EE Info'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel65: TevLabel
            Left = 12
            Top = 134
            Width = 52
            Height = 16
            Caption = '~Time Off '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel66: TevLabel
            Left = 12
            Top = 173
            Width = 50
            Height = 16
            Caption = '~Benefits '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel68: TevLabel
            Left = 12
            Top = 211
            Width = 76
            Height = 16
            Caption = '~Direct Deposit'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evddSelfServePortal: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 244
            Height = 51
            HelpContext = 10566
            Caption = '~Employee Access Option'
            Columns = 2
            DataField = 'ESS_OR_EP'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            PopupMenu = pmEnable_EEAccess
            TabOrder = 0
            Values.Strings = (
              'Y'
              'R'
              'N')
          end
          object evcbEEInfo: TevDBComboBox
            Left = 12
            Top = 110
            Width = 233
            Height = 21
            HelpContext = 10568
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ENABLE_ESS'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmEnable_ESS
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object evcbTimeOff: TevDBComboBox
            Left = 12
            Top = 149
            Width = 233
            Height = 21
            HelpContext = 10568
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ENABLE_TIME_OFF'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmEnable_Time_Off
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object evcbBenefit: TevDBComboBox
            Left = 12
            Top = 188
            Width = 233
            Height = 21
            HelpContext = 10568
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ENABLE_BENEFITS'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmEnable_Benefits
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object evcbDirectDeposit: TevDBComboBox
            Left = 12
            Top = 226
            Width = 233
            Height = 21
            HelpContext = 10568
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ENABLE_DD'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmEnable_DD
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
        end
        object fpPayrollProduct: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 270
          Height = 131
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Payroll'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel71: TevLabel
            Left = 12
            Top = 35
            Width = 71
            Height = 13
            Caption = 'Payroll Product'
          end
          object evLabel75: TevLabel
            Left = 12
            Top = 75
            Width = 40
            Height = 13
            Caption = 'HCM Url'
          end
          object cbPayrollProduct: TevDBComboBox
            Left = 12
            Top = 50
            Width = 233
            Height = 21
            HelpContext = 10567
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'PAYROLL_PRODUCT'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Government'#9'G'
              'Manufacturing'#9'N'
              'Education'#9'E'
              'Medical'#9'M'
              'Municipal'#9'U'
              'Construction'#9'C'
              'Hospitality'#9'H'
              'Agriculture'#9'A'
              'Household'#9'S'
              'Military'#9'L'
              'RailRoad'#9'R'
              'Seasonally Active'#9'Y'
              'Other'#9'O')
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmPayrollProduct
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object evDBEdit16: TevDBEdit
            Left = 12
            Top = 90
            Width = 233
            Height = 21
            HelpContext = 10610
            DataField = 'HCM_URL'
            DataSource = wwdsMaster
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object isUIFashionPanel2: TisUIFashionPanel
          Left = 286
          Top = 8
          Width = 176
          Height = 191
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'HR'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel67: TevLabel
            Left = 12
            Top = 35
            Width = 25
            Height = 16
            Caption = '~HR'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cbCoreHR: TevDBCheckBox
            Left = 12
            Top = 80
            Width = 85
            Height = 17
            Caption = 'Core HR'
            DataField = 'ADVANCED_HR_CORE'
            DataSource = wwdsMaster
            TabOrder = 0
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object cbApplicantTracking: TevDBCheckBox
            Left = 12
            Top = 104
            Width = 117
            Height = 17
            Caption = 'Applicant Tracking'
            DataField = 'ADVANCED_HR_ATS'
            DataSource = wwdsMaster
            TabOrder = 1
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object cbTalentManagement: TevDBCheckBox
            Left = 12
            Top = 128
            Width = 133
            Height = 17
            Caption = 'Talent Management'
            DataField = 'ADVANCED_HR_TALENT_MGMT'
            DataSource = wwdsMaster
            TabOrder = 2
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object cbOnlineBenefitEnrollment: TevDBCheckBox
            Left = 12
            Top = 152
            Width = 144
            Height = 17
            Caption = 'Online Benefit Enrollment'
            DataField = 'ADVANCED_HR_ONLINE_ENROLLMENT'
            DataSource = wwdsMaster
            TabOrder = 3
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object evcbHR: TevDBComboBox
            Left = 12
            Top = 50
            Width = 142
            Height = 21
            HelpContext = 10568
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ENABLE_HR'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            PopupMenu = pmEnable_HR
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
            OnChange = evcbHRChange
          end
        end
      end
    end
    object tshtAca: TTabSheet
      Caption = 'ACA'
      ImageIndex = 47
      object sbAca: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpAca: TisUIFashionPanel
          Left = 10
          Top = 10
          Width = 881
          Height = 627
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Affordable Care Act'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object btnAddCert: TevSpeedButton
            Left = 535
            Top = 272
            Width = 74
            Height = 22
            Caption = 'Add'
            HideHint = True
            AutoSize = False
            OnClick = btnAddCertClick
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
              DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
              DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
              78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
              8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
              F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
              F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
              DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
              DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
            Layout = blGlyphRight
            ParentColor = False
            ShortCut = 0
          end
          object btnRemoveCert: TevSpeedButton
            Left = 535
            Top = 320
            Width = 74
            Height = 22
            Caption = 'Remove'
            HideHint = True
            AutoSize = False
            OnClick = btnRemoveCertClick
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
              DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
              DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
              78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
              448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
              F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
              F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
              44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
              DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
            ParentColor = False
            ShortCut = 0
          end
          object evLabel73: TevLabel
            Left = 591
            Top = 35
            Width = 104
            Height = 13
            Caption = 'Administration Period  '
          end
          object lblALETransition: TevLabel
            Left = 284
            Top = 390
            Width = 102
            Height = 13
            Caption = 'ALE Transition Relief '
          end
          object lblACANotes: TevLabel
            Left = 542
            Top = 402
            Width = 52
            Height = 13
            Caption = 'ACA Notes'
          end
          object rgEducationOrg: TevDBRadioGroup
            Left = 284
            Top = 35
            Width = 127
            Height = 37
            HelpContext = 10597
            Caption = ' Education Organization '
            Columns = 2
            DataField = 'ACA_EDUCATION_ORG'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
          end
          object evGroupBox1: TevGroupBox
            Left = 12
            Top = 171
            Width = 260
            Height = 238
            Caption = ' Setup Defaults '
            TabOrder = 1
            object evLabel57: TevLabel
              Left = 12
              Top = 22
              Width = 98
              Height = 13
              Caption = 'Standard ACA Hours'
            end
            object evLabel63: TevLabel
              Left = 12
              Top = 49
              Width = 91
              Height = 13
              Caption = 'Default ACA Status'
            end
            object evLabel72: TevLabel
              Left = 12
              Top = 76
              Width = 120
              Height = 13
              Caption = 'Default ACA Annual Form'
            end
            object evLabel78: TevLabel
              Left = 12
              Top = 103
              Width = 133
              Height = 13
              Caption = 'Default ACA Coverage Offer'
            end
            object evLabel77: TevLabel
              Left = 12
              Top = 155
              Width = 121
              Height = 13
              Caption = 'Safe Harbor Type Default'
            end
            object evLabel79: TevLabel
              Left = 12
              Top = 182
              Width = 107
              Height = 13
              Caption = 'Health Care Start Date'
            end
            object evLabel80: TevLabel
              Left = 12
              Top = 209
              Width = 111
              Height = 13
              Caption = 'Health Care Start Days '
            end
            object evLabel83: TevLabel
              Left = 12
              Top = 129
              Width = 116
              Height = 13
              Caption = 'Default ACA Relief Code'
            end
            object cbACAStatus: TevDBComboBox
              Left = 149
              Top = 45
              Width = 98
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'ACA_DEFAULT_STATUS'
              DataSource = wwdsMaster
              DropDownCount = 8
              ItemHeight = 0
              Items.Strings = (
                'Full Time'#9'F'
                'Part Time'#9'P'
                'Variable Hour'#9'V'
                'Seasonal'#9'S'
                'Seasonal < 120 days'#9'E'
                'Full Time Ongoing'#9'L'
                'Part Time Ongoing'#9'R'
                'New Hire'#9'I'
                'Does Not Apply'#9'N')
              ItemIndex = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 1
              UnboundDataType = wwDefault
            end
            object edtACAHours: TevDBEdit
              Left = 149
              Top = 18
              Width = 98
              Height = 21
              DataField = 'ACA_STANDARD_HOURS'
              DataSource = wwdsMaster
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object cbACAAnnualForm: TevDBComboBox
              Left = 149
              Top = 72
              Width = 98
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'ACA_FORM_DEFAULT'
              DataSource = wwdsMaster
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 2
              UnboundDataType = wwDefault
            end
            object cbOfferCodes: TevDBLookupCombo
              Left = 149
              Top = 99
              Width = 98
              Height = 21
              HelpContext = 19572
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'DESCRIPTION'#9'44'#9'Description'#9'F'#9)
              DataField = 'SY_FED_ACA_OFFER_CODES_NBR'
              DataSource = wwdsMaster
              LookupField = 'SY_FED_ACA_OFFER_CODES_NBR'
              Style = csDropDownList
              TabOrder = 3
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = True
            end
            object cbACASafeHarbor: TevDBComboBox
              Left = 149
              Top = 151
              Width = 98
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'ACA_SAFE_HARBOR_TYPE_DEFAULT'
              DataSource = wwdsMaster
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 5
              UnboundDataType = wwDefault
            end
            object cbHealthCareStart: TevDBComboBox
              Left = 149
              Top = 178
              Width = 98
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = False
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'ACA_HEALTH_CARE_START'
              DataSource = wwdsMaster
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 6
              UnboundDataType = wwDefault
            end
            object edHealthCareStartDay: TevDBSpinEdit
              Left = 199
              Top = 205
              Width = 49
              Height = 21
              Increment = 1.000000000000000000
              MaxValue = 365.000000000000000000
              DataField = 'ACA_HEALTH_CARE_START_DAYS'
              DataSource = wwdsMaster
              MaxLength = 3
              TabOrder = 7
              UnboundDataType = wwDefault
            end
            object cbReliefCodes: TevDBLookupCombo
              Left = 149
              Top = 125
              Width = 98
              Height = 21
              HelpContext = 19572
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'DESCRIPTION'#9'44'#9'Description'#9'F'#9)
              DataField = 'SY_FED_ACA_RELIEF_CODES_NBR'
              DataSource = wwdsMaster
              LookupField = 'SY_FED_ACA_RELIEF_CODES_NBR'
              Style = csDropDownList
              TabOrder = 4
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = True
            end
          end
          object evGroupBox2: TevGroupBox
            Left = 284
            Top = 414
            Width = 247
            Height = 89
            Caption = ' Reporting '
            TabOrder = 10
            object evLabel58: TevLabel
              Left = 14
              Top = 23
              Width = 65
              Height = 13
              Caption = 'Control Group'
            end
            object evLabel74: TevLabel
              Left = 14
              Top = 55
              Width = 52
              Height = 13
              Caption = 'ALE Group'
            end
            object evDBLookupCombo9: TevDBLookupCombo
              Left = 87
              Top = 20
              Width = 146
              Height = 21
              HelpContext = 19572
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'DESCRIPTION'#9'20'#9'Description'#9'F')
              DataField = 'ACA_CL_CO_CONSOLIDATION_NBR'
              DataSource = wwdsMaster
              LookupTable = DM_CL_CO_CONSOLIDATION.CL_CO_CONSOLIDATION
              LookupField = 'CL_CO_CONSOLIDATION_NBR'
              Style = csDropDownList
              TabOrder = 0
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = True
            end
            object lcbACAGroup: TevDBLookupCombo
              Left = 87
              Top = 52
              Width = 146
              Height = 21
              HelpContext = 19572
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'ACA_GROUP_DESCRIPTION'#9'40'#9'Aca group description'#9'F')
              DataField = 'SB_ACA_GROUP_NBR'
              DataSource = wwdsMaster
              LookupTable = DM_SB_ACA_GROUP.SB_ACA_GROUP
              LookupField = 'SB_ACA_GROUP_NBR'
              Style = csDropDownList
              PopupMenu = pmAleGroup
              TabOrder = 1
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = True
            end
          end
          object evGroupBox3: TevGroupBox
            Left = 12
            Top = 414
            Width = 260
            Height = 123
            Caption = ' Average Hours '
            TabOrder = 2
            object evDBRadioGroup12: TevDBRadioGroup
              Left = 12
              Top = 64
              Width = 235
              Height = 44
              Caption = ' Period to Use for Average Hours Worked '
              Columns = 2
              DataField = 'ACA_AVG_HOURS_WORKED_PERIOD'
              DataSource = wwdsMaster
              Items.Strings = (
                'Weekly'
                'Monthly')
              TabOrder = 1
              Values.Strings = (
                'W'
                'M')
            end
            object evDBRadioGroup13: TevDBRadioGroup
              Left = 12
              Top = 19
              Width = 236
              Height = 37
              HelpContext = 10565
              Caption = 'Base Average on Processed Payrolls '
              Columns = 2
              DataField = 'ACA_USE_AVG_HOURS_WORKED'
              DataSource = wwdsMaster
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 0
              Values.Strings = (
                'Y'
                'N')
            end
          end
          object evGroupBox4: TevGroupBox
            Left = 284
            Top = 80
            Width = 469
            Height = 71
            Caption = ' Initial Measurement Period  '
            TabOrder = 7
            object evLabel59: TevLabel
              Left = 12
              Top = 26
              Width = 23
              Height = 13
              Caption = 'From'
            end
            object evLabel60: TevLabel
              Left = 236
              Top = 26
              Width = 13
              Height = 13
              Caption = 'To'
            end
            object AcaToMonth: TevComboBox
              Left = 336
              Top = 26
              Width = 110
              Height = 21
              AutoDropDown = True
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              ItemIndex = 0
              TabOrder = 3
              Text = 'January'
              OnChange = AcaToMonthChange
              OnCloseUp = AcaToMonthChange
              Items.Strings = (
                'January'
                'February'
                'March'
                'April'
                'May'
                'June'
                'July'
                'August'
                'September'
                'October'
                'November'
                'December')
            end
            object AcaFromMonth: TevComboBox
              Left = 112
              Top = 26
              Width = 110
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              ItemIndex = 0
              TabOrder = 1
              Text = 'January'
              OnChange = AcaFromMonthChange
              Items.Strings = (
                'January'
                'February'
                'March'
                'April'
                'May'
                'June'
                'July'
                'August'
                'September'
                'October'
                'November'
                'December')
            end
            object AcaFromYear: TevComboBox
              Left = 44
              Top = 26
              Width = 62
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 0
              OnChange = AcaFromYearChange
              Items.Strings = (
                '2014'
                '2015'
                '2016'
                '2017'
                '2018'
                '2019'
                '2020'
                '2021'
                '2022'
                '2023'
                '2024'
                '2025')
            end
            object AcaToYear: TevComboBox
              Left = 267
              Top = 26
              Width = 62
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 2
              OnChange = AcaToYearChange
              Items.Strings = (
                '2014'
                '2015'
                '2016'
                '2017'
                '2018'
                '2019'
                '2020'
                '2021'
                '2022'
                '2023'
                '2024'
                '2025')
            end
          end
          object evGroupBox5: TevGroupBox
            Left = 284
            Top = 157
            Width = 469
            Height = 71
            Caption = ' Stability Period  '
            TabOrder = 8
            object evLabel61: TevLabel
              Left = 12
              Top = 26
              Width = 23
              Height = 13
              Caption = 'From'
            end
            object evLabel62: TevLabel
              Left = 236
              Top = 26
              Width = 13
              Height = 13
              Caption = 'To'
            end
            object AcaStabFromMonth: TevComboBox
              Left = 112
              Top = 26
              Width = 110
              Height = 21
              AutoDropDown = True
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              ItemIndex = 0
              TabOrder = 1
              Text = 'January'
              OnChange = AcaStabFromMonthChange
              OnCloseUp = AcaStabFromMonthChange
              Items.Strings = (
                'January'
                'February'
                'March'
                'April'
                'May'
                'June'
                'July'
                'August'
                'September'
                'October'
                'November'
                'December')
            end
            object AcaStabToMonth: TevComboBox
              Left = 336
              Top = 26
              Width = 110
              Height = 21
              AutoDropDown = True
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              ItemIndex = 0
              TabOrder = 3
              Text = 'January'
              OnChange = AcaStabToMonthChange
              Items.Strings = (
                'January'
                'February'
                'March'
                'April'
                'May'
                'June'
                'July'
                'August'
                'September'
                'October'
                'November'
                'December')
            end
            object AcaStabFromYear: TevComboBox
              Left = 44
              Top = 26
              Width = 62
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 0
              OnChange = AcaStabFromYearChange
              Items.Strings = (
                '2014'
                '2015'
                '2016'
                '2017'
                '2018'
                '2019'
                '2020'
                '2021'
                '2022'
                '2023'
                '2024'
                '2025')
            end
            object AcaStabToYear: TevComboBox
              Left = 267
              Top = 26
              Width = 62
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 2
              OnChange = AcaStabToYearChange
              Items.Strings = (
                '2014'
                '2015'
                '2016'
                '2017'
                '2018'
                '2019'
                '2020'
                '2021'
                '2022'
                '2023'
                '2024'
                '2025')
            end
          end
          object evGroupBox6: TevGroupBox
            Left = 12
            Top = 542
            Width = 260
            Height = 55
            Caption = ' Additional Worked Earnings '
            TabOrder = 3
            object wwlcE_D_Group: TevDBLookupCombo
              Left = 12
              Top = 20
              Width = 210
              Height = 21
              HelpContext = 19572
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'NAME'#9'40'#9'NAME')
              DataField = 'ACA_ADD_EARN_CL_E_D_GROUPS_NBR'
              DataSource = wwdsMaster
              LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
              LookupField = 'CL_E_D_GROUPS_NBR'
              Style = csDropDownList
              TabOrder = 0
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = True
            end
          end
          object grdAvailableCertificationsEligibility: TevDBGrid
            Left = 284
            Top = 239
            Width = 240
            Height = 117
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'80'#9'Available Certifications of Eligibility'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO\grdAvailableCertificationsEligibility'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Color = 2
            DataSource = dsAvailableCertificationsEligibility
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 13
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object grdAssignedCertificationsEligibility: TevDBGrid
            Left = 620
            Top = 239
            Width = 240
            Height = 117
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'80'#9'Assigned Certifications of Eligibility'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO\grdAssignedCertificationsEligibility'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Color = 2
            DataSource = dsAssignedCertificationsEligibility
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 14
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object rgSelfInsured: TevDBRadioGroup
            Left = 417
            Top = 35
            Width = 166
            Height = 37
            HelpContext = 10597
            Caption = 'Self Insured '
            Columns = 3
            DataField = 'ACA_SELF_INSURED'
            DataSource = wwdsMaster
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No'
              'Both')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
          object edtAdminPeriod: TevDBSpinEdit
            Left = 619
            Top = 50
            Width = 49
            Height = 21
            Increment = 1.000000000000000000
            MaxValue = 365.000000000000000000
            DataField = 'ACA_ADMIN_PERIOD'
            DataSource = wwdsMaster
            MaxLength = 3
            TabOrder = 6
            UnboundDataType = wwDefault
          end
          object cbALETransition: TevDBComboBox
            Left = 398
            Top = 387
            Width = 123
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'ALE_TRANSITION_RELIEF'
            DataSource = wwdsMaster
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 9
            UnboundDataType = wwDefault
          end
          object btnACAAffordability: TevBitBtn
            Left = 284
            Top = 534
            Width = 125
            Height = 27
            Caption = 'Post ACA Affordability'
            TabOrder = 12
            OnClick = btnACAAffordabilityClick
            Color = clBlack
            NumGlyphs = 2
            Margin = 0
          end
          object edACANotes: TEvDBMemo
            Left = 542
            Top = 417
            Width = 311
            Height = 125
            HelpContext = 10655
            DataField = 'ACA_NOTES'
            DataSource = wwdsMaster
            ScrollBars = ssVertical
            TabOrder = 11
          end
          object gbService: TevGroupBox
            Left = 12
            Top = 35
            Width = 260
            Height = 127
            Caption = 'Service '
            TabOrder = 0
            object evLabel81: TevLabel
              Left = 12
              Top = 71
              Width = 48
              Height = 13
              Caption = 'Start Date'
            end
            object evLabel82: TevLabel
              Left = 130
              Top = 71
              Width = 45
              Height = 13
              Caption = 'End Date'
            end
            object rgACAService: TevDBRadioGroup
              Left = 10
              Top = 21
              Width = 237
              Height = 44
              HelpContext = 10658
              Caption = '~ACA Services'
              Columns = 3
              DataField = 'ACA_SERVICES'
              DataSource = wwdsMaster
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              PopupMenu = pmACAService
              TabOrder = 0
            end
            object edACAStartDate: TevDBDateTimePicker
              Left = 12
              Top = 89
              Width = 100
              Height = 21
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'ACA_SERVICES_START_DATE'
              DataSource = wwdsMaster
              Epoch = 1950
              ShowButton = True
              TabOrder = 1
            end
            object edtACAEndDate: TevDBDateTimePicker
              Left = 130
              Top = 89
              Width = 100
              Height = 21
              HelpContext = 10602
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'ACA_SERVICES_END_DATE'
              DataSource = wwdsMaster
              Epoch = 1950
              ShowButton = True
              TabOrder = 2
            end
          end
        end
      end
    end
    object tsVMR: TTabSheet
      Caption = 'Mail Room'
      ImageIndex = 3
      object sbMailRoom: TScrollBox
        Left = 0
        Top = 0
        Width = 868
        Height = 582
        Align = alClient
        TabOrder = 0
        object fpMailRoom: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 380
          Height = 366
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Mail Box Overrides'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel18: TevLabel
            Left = 12
            Top = 35
            Width = 70
            Height = 13
            Caption = 'Agency Check'
          end
          object evLabel19: TevLabel
            Left = 12
            Top = 74
            Width = 52
            Height = 13
            Caption = 'Tax Check'
          end
          object evLabel20: TevLabel
            Left = 12
            Top = 113
            Width = 65
            Height = 13
            Caption = 'Payroll Check'
          end
          object evLabel21: TevLabel
            Left = 12
            Top = 152
            Width = 66
            Height = 13
            Caption = 'Payroll Report'
          end
          object evLabel22: TevLabel
            Left = 12
            Top = 191
            Width = 87
            Height = 13
            Caption = '2nd Payroll Report'
          end
          object evLabel23: TevLabel
            Left = 12
            Top = 230
            Width = 53
            Height = 13
            Caption = 'Tax Return'
          end
          object evLabel24: TevLabel
            Left = 12
            Top = 269
            Width = 74
            Height = 13
            Caption = '2nd Tax Return'
          end
          object evLabel25: TevLabel
            Left = 12
            Top = 308
            Width = 99
            Height = 13
            Caption = 'EE Electronic Return'
          end
          object edVmrAgencyChecks: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'AGENCY_CHECK_MB_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'TAX_CHECK_MB_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo2: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_CHECK_MB_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo3: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_REPORT_MB_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo4: TevDBLookupCombo
            Left = 12
            Top = 206
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'PR_REPORT_SECOND_MB_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo5: TevDBLookupCombo
            Left = 12
            Top = 245
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'TAX_RETURN_MB_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo6: TevDBLookupCombo
            Left = 12
            Top = 284
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'TAX_RETURN_SECOND_MB_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 6
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBLookupCombo7: TevDBLookupCombo
            Left = 12
            Top = 323
            Width = 345
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'Description'#9'F'
              'luServiceName'#9'10'#9'Service Name'#9'F'
              'luMethodName'#9'10'#9'Method Name'#9'F'
              'luMediaName'#9'20'#9'Media Name'#9'F')
            DataField = 'TAX_EE_RETURN_MB_GROUP_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
            LookupField = 'CL_MAIL_BOX_GROUP_NBR'
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 7
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    OnStateChange = wwdsMasterStateChange
    OnDataChange = wwdsMasterDataChange
    OnUpdateData = wwdsMasterUpdateData
    Left = 1012
    Top = 126
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
    Left = 1058
    Top = 80
  end
  inherited wwdsList: TevDataSource
    Left = 954
    Top = 218
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 967
    Top = 90
  end
  inherited PageControlImages: TevImageList
    Left = 272
    Top = 16
    Bitmap = {
      494C010130003100040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000D000000001001000000000000068
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A6B39673967
      39673967396739673967396739673967396700000000000000005A6B39673967
      39673967396739677B6F00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003712F605F605
      F605F605F605F605F605F605F605F60537120000000000005A6BB556B556B556
      B556B556B556B556D65A5A6B0000000000000000000000000000000000000000
      3E53786300000000000000000000000000003967396739673967396739673967
      396739673967396739673967396739677B6F000000007B6F3967F60500000000
      00000000000000000000000000000000F605000000000000B55639677B6F7B6F
      7B6F7B6F7B6F7B6F3967B5560000000000000000000000000000000000000000
      18331843000000000000000000000000000010530F530F4F0F4F0F4F0F4F0F4F
      0F4F0F4F0F4F0F4F0F4F0F4F0F4F0F53335700000000782A170AF60100000000
      00001A5300001B530000FA5200000000F605000000000000B5565B6B5A675A6B
      5A6B5A6B5A6B5A675B6BB5560000000000000000000000000000000000000000
      9822902200000000000000000000000000000F53FD77FC77FC77FC77FC77FC77
      FC77FC77FC77FC77FC77FC77FC77FD770F535A6B3967170A0000D6010000FA4E
      FA4EFB52FB521B53FB52FB52FA4E0000F605000000000000B5565A6BB5569552
      8C2D8C2D9552B5565A6BB556000000000000000000000000000000000000FE7F
      98321833FE7F0000000000000000000000000F53FC778F2EFA73566356635663
      566356635663566356635663F973FC770F4F3712160AF6050000D6010000FF7B
      FF7FFB4EFF7FFB4EFF7FFB4EFF7F0000F605000000000000B5565A6B4A29EF39
      19631963EF394A295A6BB5560000000000000000000000000000000000001853
      9012902218530000000000000000000000000F53FD776F2EF973F873F86FF86F
      F86FF86FF86FF86FF86FF86FF86FFB770F4FF6090000D6010000D6010000DA4A
      FA4AFB4EFB4EFB4EFB4EFB4EFA4A0000F605000000000000B5563A67E81CAD35
      185F185FAD35E81C3A67B5560000000000000000000000000000000000001833
      1843184318430000000000000000000000000F53FD7B6E2AF873555F555F555F
      555F555F555F555F555F345FD76BFB770F4FF6050000D6010000D6010000FF73
      FF73FB4AFF77FB4AFF77FB4AFF730000F605000000000000B5563A6752461146
      A514A514114652463A67B5560000000000000000000000000000000000009012
      1843184390120000000000000000000000000F53FD7B4E26F76FD76BD76BD76B
      D76BD76BD76BD76BD76BD66BD66BDC770F4FF6050000D6010000D6010000DB42
      DB42DB46DB46FB46DB46DB46DB420000F605000000000000B5565A6B185F1863
      396339631863185F5A6BB5560000000000000000000000000000000000009012
      1843183390120000000000000000000000000F53FD7B4D26F66FD66BD66BD66B
      D66BD66BD66BD66BD66BD66BB567DC770F4FF6050000D6010000D6010000087F
      087FDC3E087FDC42087FDC3E087F0000F605000000000000B5567C6F08213963
      29252925396308217C6FB5560000000000000000000000000000000018539822
      1843185390221853000000000000000000000F53FE7B2C1ED56B6B2D2E3E0E3E
      0E3ED5674B2D2E3E0E3E0E3EB467DD7B0F4FF6050000D6010000D60100000000
      00000000000000000000000000000000F605000000000000B5569C7339673963
      3A673A67396339679C73B5560000000000000000000000000000000098221853
      7863185318539822000000000000000000000F53FE7BFE7BFE7BFE7BFE7BFE7B
      FE7BFD7BFE7BFE7BFE7BFD7BDD7BFD7B0F53F6050000D6010000312AD601D601
      D601D501D501D501D601D601F601F6053716000000000000B556BD7708215A67
      292529255A670821BD77B5560000000000007863982210021002100210021002
      10021002100210021002100210029822786375630F530F530F530F530F4F0F53
      0F530F530F530F4F0F530F530F4F0F53755FF6050000D6010000000000000000
      000000000000000000000000F60900000000000000000000B556BD775A6B5A6B
      5B6B5A6B5A675B67BE77B556000000000000FE7F786318439822100210021002
      18537863100210021002982218437863FE7F0000000000000000000000000000
      000000000000000000000000000000000000F60500005032D601D501D501D501
      D501D501D501D601F601F609BA3200000000000000000000B556DE7B08217B6B
      08215A6B5B670060FF7BB5560000000000000000000000000000FE7F90129022
      1833184390129822FE7F00000000000000000000000000000000000000000000
      000000000000000000000000000000000000F605000000000000000000000000
      0000000000000000F609000000000000000000000000000039677B6FFF7FDE7B
      FF7BDE7BDE7BFF7B7B6B39670000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003712F605F605F605F605F605F605
      F605F605F605F609371600000000000000000000000000000000D65AB556B556
      B556B556B556B556396700000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000396739673967396739673967
      39673967396739673967396739673967396700009E6F7E677E677E677E677E67
      7E677E677E677E677E677E677E677E67DF7B0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000782A170AF605F605F605F605F605
      F605F605F7055536945694529452945295520000FC4A9C32BC36BC36BC36BC36
      BC36BC36BC36BC369C369C369C369B32BE6F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000170EFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FD946FA4A7C6B5A6BDE7B955200001D53BC36DD3EDD3EDD3EDD3E
      DD3ADD3ADD3ADD3ABD3ABD36BD36BC36BE733967396739673967396739673967
      396739673967396739673967396739677B6F7B6F396739673967396739673967
      396739673967396739673967396739677B6F170EFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F3626DF777D677B6BB55600007E63DC3AFD42FD42FD42FD3E
      DD3EDD3ADD3ADD3ABC3ABC3ABC36DC42FF7BD066CF62AF62AF62AF62AF62AF62
      AF62AF62AF62AF62AF62AF62AF62CF62F366F152CD46CD46CD46CD46CD46CD46
      CD46CD46CD46CD46CD46CD4ACD46CD46F1523712FF7FFF7F1863FF7F1A63FF7F
      1A63FF7F1963FF7F5636BE73FF7B7D67D65A0000DF7BFC461D431D471D471D47
      FD46FD42FD3EDD3ADD3ABD3ABC365D5F0000CF62FC7FFC7FFB7FFB7FFB7FFB7F
      FB7FFB7FFB7FFB7FFB7FFB7FFB7FFC7FCF62AD46D96F115BF052325F325F325F
      325F325F325F325F325FEF521057D96FAD463816FF7FFF7B185F1A638036803A
      80361A6319631963773E162A572E1A53D65E000000007E67FC423D4B1D471C4F
      1D4F1D4F1C4BFC42DC3ABC36FC4AFF7B0000CF62FC7F8B3ADA7F366B366B366B
      366B366B366B366B366B366BD97FDB7FAF62AC42D96F8B3E4936B767B667535B
      10531053535BB667B76749366B3AD96FAC42581AFF7FFF7BF85EFF7F803A605B
      803AFF7F1A63FF7F1A67FF7FBE6BB842B74600000000FF7F7D631C4B7D63BE73
      BE6FBE6FBE73BE731D4FFC4ADF7700000000CF62FC7F8B3AB97FB97FB87FB87F
      B87FB87FB87FB87FB87FB87FB87FDB7FAF628C42D973CE4A966796679667AD46
      96639663AD46966796679667CE4AD9738C42581EFF7FFF77F75AF95E803A803A
      803A1B63803A803A803AF95EFF7BFF7B591E0000000000000000DF7B9E67DC42
      9C369C32BC3A3D53DF77FF7F000000000000CF62FC7F6A36B87F156B156B156B
      156B156B156B156B156B146B977BDB7FAF628B3EDA73AD4695674A369567AD46
      96639663AD4695674A369567AD46DA738B3E7922FF7FFF77D75AFF7BF95EFF7B
      F95EFF7B803A605B803AFF7BFF77FF7F792200000000000000009E67BC36BC36
      DD3ADD3ABC369C32FC4AFF7F000000000000CF62DC7F4932977F977B977B977B
      977B977B977B977B977B977B767BDB7FCF628B3EDB77AC429463746374638C3E
      966396638C3E746374639463AC42DB778B3E7926FF7FFF73D656D75AD75AD75A
      D75AD85A803A803A803AD85AFF73FF7F7926000000000000FF7BDC42BC36DD3A
      DD3ADD3ADD3ADD3A9C327D63000000000000CF62FD7F492E967F967B967B967B
      767B767B967B967B967B767B7577DC7FCF626B3ADB776A3A8B3E73637363F052
      EF4ACF46F052736373638B3E6A3ADB776B3A992AFF7FFF73B556FF73B656FF73
      D656FF77D856FF77D856FF73FF6FFF7F992A000000000000BE73BC3AFD3EFD3E
      DD3EDD3EDD3ADD3ABC361C4F000000000000CF62FD7F272A757B6B2D0E460E46
      EE41757B6B290E460E46EE415477DD7FCF626A3AFC7B282E28326B3A6B3A6B3A
      6B3A6B3A6B3A6B3A6B3A2832282EFC7B6A3A9A2EFF7FFF6FB552B552B652B652
      B652B652B652B652B6529552DF6FFF7F9A2E0000000000009E6FBC3AFD42FD42
      FD42FD3EDD3EDD3ABC36FC4A000000000000CF62FD7FFD7FFD7FFD7FFD7FFD7F
      FD7FFD7FFD7FFD7FFD7FFD7FDD7FFD7FCF624A36FC7BDC7BDC7BDC7BDC7BDC7B
      DC7BDC7BDC7BDC7BDC7BDC7BDC7BFC7B4A36BC2AFF7BFF67FF6BFF6BFF6BFF6B
      FF6BFF6BFF6BFF6BFF6BFF6BFF67FF7BBC2A000000000000DF77FC421D471D47
      1D47FD42FD3EDD3EBC363D57000000000000356FCF62CF62CF62CF62CF62CF62
      CF62CF62CF62CF62CF62CF62CF62CF62356FF14E115396679567956795679567
      95679567956795679567956796671153F14E637E0F7F657E657E657E657E657E
      657E657E657E657E657E657E657E0F7F637E000000000000FF7F3D571D473E4B
      1D4B1D47FD42DD3EDC3EBE730000000000000000000000000000000000000000
      0000000000000000000000000000000000000000D14E4A364936493649364936
      4936493649364936493649364A36D14E0000637E757F757F757F757F757F757F
      757F757F757F757F757F757F757F757F637E0000000000000000DF771C531D4B
      3D4F1D4BFD42FC427D6300000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008E66427E437E437E437E437E437E
      437E437E437E437E437E437E437E427E8E6600000000000000000000FF7B7E67
      3D573D535D5BBE73000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000396739673967DE7B0000000000000000000000007B6F
      396739677B6F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A6B396739673967396739673967
      39673967396739673967095609560956F6620000000000000000000000006D62
      085E075E4C5E7B6F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008C318C318C318C318C318C318C2D
      6C2D6C2D8C2D8C318C318C318C318C318C31FD3EDD3ADD3ADD3ADD3ADD3AFD3A
      FE3AFE3AFE3A1F3BE6596E5EA47E327748620000DE7BDE7BDE7BDE7BFF7F2962
      8A6A8B6A29624C5E7B6FDE7B0000000000003967396739673967396739673967
      3967396739673967396739673967396739676C2D31466B2D6C2D6C2D6B2DB556
      B556B556B5566B2D6C2D6C2D6B2D31466C2DDD3ABF67BF63BF63BF63DF67D852
      EF3DCE3DCE3DCE3D3142964E136BB57F48625A6B156716671667166717674A62
      967F6A6A6A6A29624C5E16675A6B000000009452734E524A524A524A524A524A
      524A524A524A524A524A524A524A734E9452CE39EF3DAD354A294B294A291042
      F03DF03D10424A294B294B29AE35EF3DCE39DD3ABF67DF73DF73BF5F3142524E
      5A6F9C739C735A6F734E1142BE7377774866696A686A486A4766476647662962
      6966967F6A6A6A6A0662676E896E5A6BFF7F734E000000000000000000000000
      00000000000000000000000000000000734E7B6F6B293146524A2925524A2925
      524A2925524A2925524A292531466B297B6FDD3ABF67DF6FDF73B84E734E9C73
      1D4B3E437E479E579C73734EB65647660000686A476AF57FD37FD27FD27FD27F
      AC6A4966957FB06AF75E113E113E53461863734EFF7FDE7BDE7FDE7FDE7FDE7F
      DE7FDE7FDE7FDE7FDE7FDE7FDE7BFF7F734EFF7F2A2532464A25524A0921524A
      0921524A0921524A09254A29524A2A25FF7FDD3ABF6B7F5B9F5B10429C73FC46
      3D475E479F4FBF539E579C73113E00000000486AAA6E717BB47F907F907F907F
      B17FAC6A8A6A7C6F3A675A6BBD779D733246734EDE7BF529F529F529F529F529
      F529F529F529F529F529F529F529DE7B734EFF7FF85EAD351042082108210821
      082108210821082108211042AD35F85EFF7FDD3ABF6B7F579F5BEF3DDE7FDC3A
      7E5B7E537E4F9F4F7E47DE7F114200000000486A0E77ED72D77F6E7F6E7F6F7F
      8F7F8F7F8F7FD8567B6F3967324652465346734EDE7BF52D5F5B5F5B5F5F5F5F
      5F5B5F5B3F573F575F575F5BF52DDE7B734EFF7FFF7F08210721E724E724E724
      E724E724E724E724E72407250821FF7FFF7FDD3ABF6BBF6FDF6FF03DFF7FDC3A
      BE6B7E5B7E535E473D3FFF7F114200000000486A727F886EFB7FFB7FFB7FFB7F
      6E7F6E7F6E7F54465B6BF85E534A2E7FF36A734EDD7BF52D5F5BF42DF52DF52D
      F42D3F57FF7FDD7B9B73596BF529DD7B734EFF7FFF7F08259F169F16BF16FF16
      1F1B1F1BFF1ADF1A9F169F160825FF7FFF7FDD3ABF6FBF6BDF6F1142DD7BDC42
      BF73BE6B7E5B3D4B3D4BDD7B3246000000004766D67FAA6E2666266626662666
      EB766D7F6C7F54467C6FB656744E744A954E734EDD7BF52D5F5B5F5B5F5B5F5B
      5F5B3F57FF7FDD7B9B73596BF529DD7B734EFF7FFF7F28297E169D1A9E1ABE1A
      DF1ADF1ABE1A9E1A9D1A7E162829FF7FFF7FDD3ADF735F535F53F85AB5560000
      DC42FC42FC421D4B0000B55A9842000000004766D77F6E7F8F7F8F7F8F7F8F7F
      AA6EEA76FB7F17673A67DE779C6F3A639356734EDE7BF52D5F5FF52D5F5FF52D
      5F5B5F5B3F573F575F575F5BF52DDE7B734EFF7FFF7F29297D167D1A9D1EBE1A
      BE16BE169E169D167C167D162829FF7FFF7FDD36DF735F4F5F4FDF6F744ED65A
      FF7F00000000FF7FD65A9452FE3A000000004766D87F6E7F6E7F6E7F6E7F6E7F
      6E7FA96E266627667156964EB652935AF26E734EFF7FF529F52DF52DF52DF52D
      F52DF529F529F529F529F529F529FF7F734EFF7FFF7F492D5D165C1A9C1EBD26
      DD26BD26BD26BD26BD2ABE2A4929FF7FFF7FDD36DF77BF67BF6B3F4F3F4FF95A
      524A31463146524AF956FF77DD3A000000004766D97F4D7F4D7F4D7F4D7F4D7F
      4D7F6D7F6D7F6D7F6C7FFA7F466A9B7B0000734E000000000000000000000000
      00000000000000000000000000000000734EFF7FFF7F4A313C165B1A7C1E9C22
      BD26BD2ADD32DD36DD3AFE3E4A2DFF7FFF7FDD36FF7B9F679F673F4B3F4BBF67
      BF673F4B3F4BBF67BF67FF7BDD36000000004766DA7F4C7F4C7FB67FDB7FFB7F
      FB7FFB7FFB7FFB7FFB7FFB7F476ABB7B0000F75E734E734E734E734E734E734E
      734E734E734E734E734E734E734E734EF75EFF7FFF7F6A313C163B165B1A7C1E
      9C269C2ABC2EBC32DD36DE3A4A2DFF7FFF7FDD3AFF7FFF7BFF7BFF7FFF7FFF7B
      FF7BFF7FFF7FFF7BFF7BFF7FDD3A00000000476AFB7FDB7FDB7FFB7F26664766
      47664766476647664766476A8A6A000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7F8B311C121B163C165C1A
      7D229D269D2ABD2EBD36DE3A6B31FF7FFF7FFD42DD3ADD36DD36DD36DD36DD36
      DD36DD36DD36DD36DD36DD3AFD4200000000696A476A47664766476A8A6ABC7F
      BB7BBB7BBB7BBB7BBB7BBA7B0000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7F524A8C358B358B358B35
      8B358B358B358B318B318C31524AFF7FFF7F0000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000005A6B000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000396739677B6F0000000000000000AB3DA941496E2A6A0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000BD77422A00000000000000000000000000000000DE7B396739673967
      39673967396739673967DE7B0000000000000000DE7B39673967396739673967
      39673967202A0043683A7B6F0000000000000B4E905A527FEB7EA74D0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00003967DE7B3967396739673967
      39670000D152402E396739673967FF7F0000000000000000B84A550954055405
      54055405540554055509B84A0000000000000000B84A55095505550556055705
      5705390120268053E042883A7B6F00000000A976937F737F647E627EA74D0000
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FBD7720265563603680368036A036
      602EBD77803EC04680368036602E5967000000000000000076095A2E9C369C36
      9C369C369C369C365B2E7609000000000000000076095A2E9C369D36682E2026
      20262026002260536053E042883A7B6F0000C976917FE87E857E637E637EA74D
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FD04EA0425663A046005320536053
      C03A335BE052004F204F40538057873A0000000000000000D819FD467B2E7B2E
      7B2A7B2A7B2E7C2EFD46B7110000000000000000D819FD467C2E7E2E0022A763
      60536053404F404F404F404FE042883A0000FF7FE661297FE77EA57E637E637E
      A74D0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F60320053F152ED4E803A8036A03A
      602E00006032C04680368036404BA036000000000000000019263D57BD36BD36
      FE7FDD7BDD36BD363D57D815000000000000000019263D57BD36DF3E001EAD67
      204F204F204F004B204F204F8C6720220000FF7FFF7FE7612A7FE77EA57E637E
      637EA74D0000FF7FFF7FFF7FFF7FFF7FFF7F60360053A03A5A6BDE7B39670000
      00000000DC77402E0000165FAF4A202600000000000000005A2E7E67FE3EDF6F
      FF7FDE7B7D671F3F9E67F91900000000000000005A2E7E671F3FFF77001EB273
      A96BA96B896BB26F004B8C63C042CA420000FF7FFF7FFF7FE7612A7FE77EA57E
      637E637EA74D0000FF7FFF7FFF7FFF7FFF7F6036E04E20536232F35600220000
      000000000000A83E9C7360320047402A00000000000000005A261D4FBF6B7677
      8B62E7516F5ADF6F1D535A2A00000000000000005A261D4FBF6B777B433A001E
      001E001E001E906B6B63A03ECB4200000000FF7FFF7FFF7FFF7FE7612A7FE77E
      A57E637E637EA74D0000FF7FFF7FFF7FFF7FEC4AE04A004F204B404B545F0000
      0000000000000000AD46E04E4053C03A00000000000000000000FD42BE32175F
      6C5EE84D5246BE36FD42000000000000000000000000FD42BE32185F6C5EE951
      544ADF3600228F6FA03ECB42000000000000FF7FFF7FFF7FFF7FFF7FE7612A7F
      E77EA57E637E627EA6510000FF7FFF7FFF7F3967402E20534053A03600000000
      0000000000000000402E0057404F85360000000000000000000000007B6FAF62
      EF6EAD664C569C73000000000000000000000000000000007B6FAF62EF6EAD6A
      4C569C732022A042CA420000000000000000FF7FFF7FFF7FFF7FFF7FFF7FE761
      2A7FE77EA57E627EAF6AF0390000FF7FFF7F202660328036A03A365F39679C73
      DE7B39673967396739676132A03A0000000000000000000000000000CB49937F
      1077CE6EAD6A0D5200000000000000000000000000000000CB49937F1077CE6E
      AD6A0E520000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      E761297FE67EF26E3242D756EF350000FF7F00000000BD7739678A3E603AAC46
      BA732022C046E042602E9C73B96F0000000000000000000000000000C030D57F
      727F30730F73A02C00000000000000000000000000000000C030D57F727F3073
      0F73812C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FE665977B123E5A673142D659B365FF7F0000000075636036004F40536036
      BD779C73402E204F404BAD4A00000000000000000000000000000000E1348449
      2A5ACF6A6C5EA12C00000000000000000000000000000000E13484492A5ACF6A
      6C5EA12C0000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F123EDE7730425966F759F565FF7F000000000000803A0053204F8053
      CF4AAD4AE046204F204BA03A5A6B00000000000000000000000000000239E655
      295EEA552539C230000000000000000000000000000000000239E655295EEA55
      2539C2300000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F103EFD769A6A376AFF7FFF7F000000000000986BA03EE0428436
      603AA046205300470F53873A422E00000000000000000000000000006741C455
      275EE75524396741000000000000000000000000000000006741C455275EE755
      243967410000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F386E376AFF7FFF7FFF7F0000000000000000BA6F0E4FA93E
      402A803A8036613200000000000000000000000000000000000000009B774441
      63454341443D9B77000000000000000000000000000000009B77444163454341
      443D9B7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FE7F186318631863FE7F00004539253925394539453945394539
      45394539453945392539253925394539FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000000000000000000000000000
      00000000FE7F88420022002200228842FE7F0D4E2E52CA458841884188418841
      8841884188418841AA452E522E520D4EFF7F3967396739673967396739673967
      3967396739673967396739673967396739670000000000000000000000000000
      000000009C73CD5DC454C454C454CD5D9C730000000000000000000000000000
      0000000088428032004390630043803288424F52663D4F56CA45884188458845
      8845A845A84588418841CA4545394F52FF7F5542343E343E343E343E343E343E
      343E343E343E343E343E343E343E343E55420000000000000000000000000000
      00000000AC5DE560877D877D877DE560CC5D7863186318631863186318631863
      1863186300220043004300000043004300224539883D8841A845A945EA4D8E5A
      D56FEB4DA949A949A945884188414539FF7F343E000000000000000000000000
      00000000000000000000000000000000343E7B6F396739673967396739673967
      396739678358887D877D677D877D887DC3541053104310431043104310431043
      1053105300229063000000000000906300222539A945A945A949C949B56F536B
      5267B56F3267C949C949A945CA452539FF7F343E0000734ED55A9352314A9452
      9452945293527352B4569452734E0000343EF152CD46CD46CD46CD46CD46CD46
      CD46EE466254F57E000000000000F57EA3549042786310631053106310631063
      10631863002200530053000000530053002225352D52A949CA4DCA4DEB4D6F5A
      F162B56F956FCA4DCA4DC9494E562535FF7F343E000000000000000000009C73
      00009C7300000000FF7F000000000000343EAD46D96F115BF052325F325F325F
      325F535F6254CB7DCB7DCA7DCB7DEB7DA3549042786388428832786378639063
      1053105388328032805390738053803288324539705A0C52EB4DEB51B66FB673
      3367B05E2D56EC51EB4D0C524E56A945FF7F343E0000774A3B67774E3B67774E
      3B67774E3B67FA5E56465646FA5E0000343EAC42D96F8B3E4936B767B667535B
      115331538851E5640D7E2E7E0D7E0665A84D9042786310537863786378639042
      786378631053084300220022002208439042BC77463DD4622D560C52546BB773
      756F966F336B0C522E56D466663DBC77FF7F343E0000564AFF7F574AFE7B574A
      FE7B574ADE77574AFF7FFF7F564A0000343E8C42D973CE4A966796679667AD46
      96639663EE46EB5D6254625461540D62CD3E8842FE7F90427863883278639042
      7863786390427863904278631053FE7F9042FF7F1667463DF5664F5A2E56B162
      766FD3662E5A4F5AF56A463D1667FF7FFF7F343E0000574AFE7F574ADE7B574A
      DE7B574ABD77574AFE7FFE7F5646FF7F343E8B3EDA73AD4695674A369567AD46
      96639663CE46B6678B36D663EE46FA738C3E8842FE7F90427863786378639042
      7863786390427863786378639042FE7F8842FF7FFF7F1667663DF56A4F5A505E
      AF660E525277166B673D1667FF7FFF7FFF7F343E0000574A1A63774A1A63774A
      1A63774A1A63F95A56465646D95AFF7F343E8B3EDB77AC429463746374638C3E
      966396638C3E946394639563AD42DB778B3E8842FE7F88428842906390631053
      1053104310539063906388428842FE7F8842FF7FFF7FFF7FD462EB4D386F176B
      B47FB062757BCB49D462FF7FFF7FFF7FFF7F343EFF7F9C735B6B9C735B6B9C73
      5B6B9C735B6BBD77BD77BD77BD77FF7F343E6B3ADB776A3A8B3E73637363F052
      EF4ACF46F052736373638B3E6A3ADB776B3A8842FE7F08320832884288428842
      8842884288428842884208320832FE7F8842FF7FFF7FFF7FFF7F673D88412739
      CF66737B0B520D4EFF7FFF7FFF7FFF7FFF7F343E0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000343E6A3AFC7B282E28326B3A6B3A6B3A
      6B3A6B3A6B3A6B3A6B3A2832282EFC7B6A3A8832FE7FFE7FFE7FFE7FFE7FFE7F
      FE7FFE7FFE7FFE7FFE7FFE7FFE7FFE7F8832FF7FFF7FFF7FFF7FFF7F0E4E7377
      947BB47F1567FF7FFF7FFF7FFF7FFF7FFF7FD856343E343E343E343E343E343E
      343E343E343E343E343E343E343E343ED8564A36FC7BDC7BDC7BDC7BDC7BDC7B
      DC7BDC7BDC7BDC7BDC7BDC7BDC7BFC7B4A361053105378637863786378637863
      786378637863786378637863786310531053FF7FFF7FFF7FFF7FFF7F463DEB4D
      505AEC51EC45FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000F14E115396679567956795679567
      95679567956795679567956796671153F14E0000105388328832883288328832
      883288328832883288328832883210530000FF7FFF7FFF7FFF7FFF7F596F925A
      0D4E67414639FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000D14E4A364936493649364936
      4936493649364936493649364A36D14E0000000000000000FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000396739673967396739673967
      3967396739673967396739673967396739670000000000003967396700000000
      0000000000000000000000000000000000000000000039673967000000000000
      000000009C733967396739679C7300000000075607560756FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FB84A55095405550536012576266A
      076A076A076A076A076A076A076A076A276A000000000000666E876E5A6B0000
      00000000000000000000000000000000000000000000666E876E5A6B00000000
      00009C73CA46602E602E602ECA469C730000E755CD6AE755FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F760D7B2EBC3A9C369D32266E487F
      697F697F697F687F697F697F697F497F276E000000000000CC72666E886E3967
      FF7F0000000000000000000000000000000000000000CC72666E886E3967FF7F
      0000CA468036E0426E63E0428036CA460000E755CD6AE655FF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF8191D4B9C327C2E7D2A563E4672
      6A7F6A7F6A7F0E1D6A7F6A7F6A7F67728F5A000000000000767B666E0C77666E
      1767FF7F000000000000000000000000000000000000767B666E0C77666E1767
      FF7F602A0047E0420000E0420047602E0000E755CC6AE659FF7FFF7FFF7FFF7F
      FF7FDE7B39670000000000007B6FDE7BFF7F39265E5FDE3AFE3EFF7FDF772B62
      097B8B7F8B7F0C6F8B7F8B7F0A7F276206320000000000000000666E507B917F
      666E1767BD77000000000000000000000000000000000000666E507B917F666E
      396760268E670000000000008E67602E0000E655CC6AE45D7B6FDE7BFF7FFF7F
      FF7FFA4E5C163C123C123C129B2EFA569C733A227E639F67FF77FF7FDF7B7C6B
      276E8C7FAC7F2D21AD7F8C7F066AED5206360000000000000000CD72EA76D57F
      6F7F666EF26ABD7700000000000000000000000000000000CD72EA76D57F6F7F
      68726026204B204700002047204F602E0000E655AC6A3D0EBB32FA5200000000
      00005C163C123C127F477F43FF2A7D1ABB36FF7B9B32FD4215678B62E74D1042
      9552C976CE7F2D21CE7FC9766652725F063A0000000039673967F46AA872D77F
      907F6E7B666EF36A9C730000000000000000000039673967F46AA872D77F907F
      6F7F624AA032404FAE6B404FA03A0D530000E655AB723E0A9F167D163C123C12
      3C123C125D163B0E7F435F3F1F2F3F3F3C12FF7FFF7FBF73B2568C622A56CC35
      354F6A6E8E7F2D21AE7F6A6A935F7363263E00000000676EA972A8728772D77F
      8F7F8F7F6E7B8872D06A7B6F0000000000000000676EA972A8728772D77F8F7F
      8F7F6E7F834A6026602A602E0D5300000000E655AA723D0A3F3BFF261F333F37
      3F3B5F3B5F471B0E7F435F3F1F2B5F473C12FF7FFF7F3867EF720F73AE6A4B5E
      4846894E6872706F68720A4AED528A4AFE7F00000000666ED37F907F8E7F6D7B
      6D7B6D7B8E7F6E7F8872AD6A7B6F000000000000666ED37F907F8E7F6D7B6D7B
      6D7F8E7F6E7F8876AF6E7B6F000000000000E655AA6E3D0A5F43DF221F2F3F3B
      7F475F3B7F4B1B0A7F435F3F1F2B7F533C0EFF7FFF7FE234B47F517BEF6ECE6A
      0539FE7B295E89722862E841BB77FF7FFF7F000000008B6E707B917F6D7B6D7B
      D77FD77FD67FD57FD57FA872AD6E0000000000008B6E707B917F6D7B6D7BD77F
      D77FD67FD57FD57FA872AD6E000000000000E655AA6E3D067F4FDF221F2F3F37
      7F433F377F531B0A7F435F3BFF2A9F5B3C0EFF7FFF7FE134AD66CF6A3177F072
      80285A6B1073CE6ECE6A6B5E3867FF7FFF7F0000000013770D77B37F4B7B4C7B
      C872466E466E466E676E676E686E00000000000013770D77B37F4B7B4C7BC872
      466E466E466E676E676E686E000000000000E655AA6E3D067F57DF1E1F2F3F37
      7F433F379F5B1B0A9F575F3BFF26BF633C0EFF7FFF7F0239C555295AEA51263D
      A12C453DD57F527BF06ECE6AE334FF7FFF7F00000000BB7FA972B67F6C7B4A7B
      B57FCC72D16AFF7F000000000000000000000000BB7FA972B67F6C7B4A7BB57F
      CC72D16AFF7F000000000000000000000000E655AA6E3D069F5FDF1EFF2A3F37
      7F433F37BF631B0ABF6FDF73DF73DF733C0EFF7FFF7F0239E555285EE8552439
      2539E134AD66CF6A31771073A12CFF7FFF7F000000000000676EB57F707B297B
      707B957F476E5A6B0000000000000000000000000000676EB57F707B297B707B
      957F476E5A6B000000000000000000000000E655AB6E3E06FF777F535F4B5F43
      7F4B5F43FF731C0A3C129C269D2A9F673C0EFF7FFF7F9B77223D834943410339
      FF7F0239E555295AEA51463DC230FF7FFF7F0000000000008B6E717BB57F277B
      287BD77FED728B6ADE7B0000000000000000000000008B6E717BB57F277B287B
      D77FED728B6ADE7B00000000000000000000E655CB6E3E06BE2A3F439F5FFF73
      FF77FF737F573C0EFF7FBF6B7E5BBD2E3C12FF7FFF7FFF7FFF7FFF7FDE7BFF7F
      FF7F0239E555285EE8552439C234FF7FFF7F00000000000013730D77FA7FD97F
      D87FFA7FD87F686EF46A00000000000000000000000013730D77FA7FD97FD87F
      FA7FD87F686EF46A00000000000000000000AE62E559D35A5E539D263C0E3C0E
      3C0E3C0E3C121E47FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7F9C77223D6349434102399B77FF7FFF7F000000000000BA7B686E476E476E
      476E476E686E686E696E000000000000000000000000BA7B686E476E476E476E
      476E686E686E696E000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000000000000000000000000000
      0000000000009C733967396739679C7300000000000039673967396739673967
      3967396739673967396739673967000000006072607240724072407260766076
      607640724072407240724072407260726072000039673967BD779C7339673967
      3967396739670E6A2665276527650E669C730000000000000000000000000000
      000000007B6FCA46602E602E602ECA469C7300000000F231D129B129B129B129
      B129B129B129B129B129D129F231000000006172677B677B667B867BC762C935
      A67F867B667B667B667B667B677B877B617200008876887A136F184A152DF52C
      F52CF6280C51466D667D667D677D47690E667B6F396739673967396739673967
      39673967A9428036E0426E63E0428036CA4600000000D1290000000000000000
      000000000000000000000000D12900000000307BC376687B467B467BA7628A08
      A835667F467B467B467B467B687BC376307B0000A87AB07F3435162D572D772D
      772D7829246D877D877D677D877D887D26655726F605F605F605F605F605F605
      F705F901602E0047E0420000E0420047602E00000000B1290000FF7FFF7F0000
      D515D5150000FF7FFF7F0000B12900000000FF7F4072497B477B467B667F667F
      C910C666467F467B267B467B497B4072FF7F00007677343537297831DA39FC3D
      FC3D1D3A0469F47E000000000000F57E2665F605FE7FDE7FDE7FDE7FDE7FDE7F
      DE7FFF7F402A8E670000000000008E67602E00000000B1290000FF7FFF7FD515
      FE3AFE3AD515FF7FFF7F0000B12900000000FF7F307BA3764A7B06778662C66A
      8835C9100673257F257B4A7BA476307BFF7F0000184A372DB835FB3D3C67FD7F
      B356FE7FC364CB7DCA7DCA7DCA7DEB7D2665F605FE7FBD77BD77BD77BD77BD77
      000000004026204B204700002047204F602E00000000B1290000FE7FD515FD36
      FD3AFD3AFD36D515FE7F0000B12900000000FF7FFF7F61722A7B2877C814A90C
      A90C8908E818E576287B2A7B6172FF7FFF7F0000362DB83DFB3D3B67FB7FFC7F
      FC7FFD7F4F72476D0D7E2E7E2D7E686D5072F605FE7F9C73734E734EBD73F85E
      000000002D53A036404FAE6B404FA03A0D5300000000B1290000D5111E431E43
      1D43FD363E471E43D5110000B12900000000FF7FFF7F757B83766E7F27568808
      A73D457F257F067B4E7F8376757BFF7FFF7F0000162D3A4A1C42FA7FFB7F2A21
      366BFB7FFC7F4E72E4640469256971720000F605FE7F9C739C739C739C739452
      945294529656673A40264026602A4A1E000000000000B1290000983AD619F619
      3D4FBC2EF61DF619983A0000B12900000000FF7FFF7FFF7F61722B7B6C7FE71C
      88082756267F2B7B2C7B6172FF7FFF7FFF7F000016297B521D42704EFA7F566B
      4B251567FA7F714E1F3E7D4E182100000000F605FE7F7B6F7B6F7B6F7B6F7B6F
      7B6F7B6F7C6F7D737D739E730000F905000000000000B1290000BD7BDD7FD615
      7D5B3D53F619DD7FBD7B0000B12900000000FF7FFF7FFF7FB97F6176937F8966
      6704870CA96E937F8276B97FFF7FFF7FFF7F00001629BC5A1D42F87F356B4B29
      3567D97FD87FF87F1D42BD5A172500000000F605FE7F5B6F7B6F7B6F5B6BBD77
      0000000000000000BE775B6FFF7FF705000000000000B12900009C739C77983A
      D615D615983A9C779C730000B12900000000FF7FFF7FFF7FFF7FA5762D7B927F
      E7202600C9140C77A576FF7FFF7FFF7FFF7F7B6F36299C565D4A3A674B293467
      D87FD77FD67F19635D4A9C5636297B6F0000F605FE7F5A6B3246524A5A6B185F
      0000000000000000185F5A6BFE7FF605000000000000B1290000000000000000
      000000000000000000000000B12900000000FF7FFF7FFF7FFF7FB97F8172967F
      4E7F4E7FD87F8276B97FFF7FFF7FFF7FFF7FCE76EF51B939FE663E461967D67F
      0C46D57FF9621D42FE66B939EF51CE760000F605FE7F39673A673A673A679452
      935273527352935293523967FE7FF6050000000000003726381E171E171E171E
      171E171E171E171E171E381E162600000000FF7FFF7FFF7FFF7FFF7FEB760B7B
      977F967F0D7BEB76FF7FFF7FFF7FFF7FFF7FA97A0A7F75313B4A3F6F9E521E42
      1E421E427E523F6F3B4A75310A7FA97A0000F605FE7FDE7FDE7FFE7FFE7FFE7F
      FE7FFE7FFE7FFE7FFE7FFE7FFE7FF60500000000000058225E4FFD36FD3AFD3A
      FD3AFD3AFD3AFD3AFD365E4F582200000000FF7FFF7FFF7FFF7FFF7FFE7F6072
      B97FB97F6072FE7FFF7FFF7FFF7FFF7FFF7FCA7A4C7F4D7F9531DA3D3F6B9F7F
      9F7F9F7F3F6BDA3D95314D7F4C7FCA7A0000F6099E63BB26BB26BB26BB26BB26
      BB26BB26BB26BB26BB26BB269E63F60900000000000058261C4B9A2A9A2A9A2A
      9A2A9A2A9A2A9A2A9A2A1C4B582600000000FF7FFF7FFF7FFF7FFF7FFF7FEA76
      0E7B0E7BEA76FF7FFF7FFF7FFF7FFF7FFF7F987F0B7BCF7FEF7F3052562D5629
      56295629562D3052EF7FCF7F0B7B987F0000170A5D533D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F5D53170A0000000000005826FB46FB46FB46FB46
      FB46FB46FB46FB46FB46FB46582600000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      60726072FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000987FEA7A097F707F00000000
      000000000000707F097FEA7A987F00000000B932170A160A160A160A160A160A
      160A160A160A160A160A160A170AB932000000000000792A5826582658265826
      582658265826582658265826792A000000000000000000000000000000003967
      3967396700000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      00000000000000000000000000000000000000007B6F39673967396739673967
      39673967396739673967396739675A6B0000000000000000000000000000AB3D
      89414A6E3967000000000000000000000000453A453A453A4536433614361436
      14361436143643364536453A453A453AFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000D65A734E734E734E734E734E
      734E734E734E734E734E734E734EB55600000000FF7FBD773967396739670B52
      905A527FA64D39673967BD77FF7F000000000532C542C542A43EE229FF77E461
      2766E461FF77E229A43EC542A53E0532FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F734E00000000DE7BD65A314610421142C87A
      917F507F627EA64D5342D65ADE7B00000000DD77EA2D222E4C3E5B63DF73A059
      2866A059DF735B634C3A222EEA2DDD77FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EDE7BDE7B3146AD35DE7B
      3146AD35DE7F7C2A7C2ADD7BDE7B734E000000005A6B734EDE77BE777B6B7D6B
      C465087FA57E627EA64D954E5A6B00000000FF7F353ADF77BF6FBF6FDF6F4055
      27664055DF6FBF6FBF6FDF77353AFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000734EDE7BBD77DE7BDE7BDE77
      DE7BDE7BDD7BBD329C2EBD7BDE7B734E00000000F75EB556BD7711425B6B3142
      7D67C561287FA57E627EA551D65A00000000FF7F343ADF779E6BBE6F12323436
      343634361232BE6F9E6BDF77343AFF7FFF7F0000000000000000000000000000
      39673967DE7B0000000000000000000000000000734EDE7BBD733146AD35BD77
      3146AD35BD7BBE32BE32BC77DE7B734E00000000B556F75A5B6B3A6731465A6B
      32465C67E565087FA47ED06AEF3939670000FF7F333ADF77D852F952F9561A57
      1A571A57F956F952D852DF77333AFF7FFF7F0000000000000000000000003967
      8C318C31F75E0000000000000000000000000000734EBE779C739D73BD779D73
      9D77BD779C739C779C779C73BE77734E00000000524A18633967524639675246
      3A6752463B63C465757B3242F85EEE353967FF7F343ADF779E6BF9529E6FF952
      9E6FF9529E6FF9529E6BDF77343AFF7FFF7F0000000000000000000039678C31
      B556B5568C313967000000000000000000000000734EBD777C6F3246AE359C73
      3246AE359C733246AE357C6FBD77734E0000000031463963524A1963524A1963
      524A1963534A3963103EBD733142F759B365FF7F343EDF77D852F952F952F952
      F952F952F952F952D852DF77343EFF7FFF7F000000000000000039678C31F85E
      D65AD65AF85E8C31396700000000000000000000734EBD777B6B7C6B7C6B7C6F
      7C6F7C6F7C6F7C6B7C6B7B6BBD77734E00005A6B104295529452945294529452
      945294529452955295520F3EDC725966F669FF7F543EDF777D67D9529E6BF952
      9E6BF9529E6BD9527D67DF77543EFF7FFF7F00000000000000008C317B6F9D73
      9C739C739D737B6F8C3100000000000000000000734EBD775B67E37D847D7B6B
      3246AE357B6B047E847D5B67BD77734E00003146BD777B6F7B737B6F7B6F7B6B
      7B6B7B6B7B6F7B6F7B739B6F176E186E0000FF7F543EFF7BD84ED952D952D952
      D952D952D952D952D84EFF7B543EFF7FFF7F000000000000000031468C318C31
      8C318C318C318C313146000000000000000000009452DE7BBE77DE77DE77DE77
      DE7BDE7BDE77DE77DE77DE77DE7B945200001042BC77BA32B801186B1863185F
      185F185F1863186BB801BA32BC7730420000FF7F553EFF7B7D67D9527D67D952
      7D67D9527D67D9527D67FF7B553EFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000009452EF3DEF41EF41EF41EF41
      EF41EF41EF41EF41EF41EF41EF3D94520000B556BC77BF577B1AD8019B7B9C73
      9C739C739B7BD8017B1ABF57BC77524A0000FF7F5542FF7BD84ED852D852D852
      D852D852D852D852D84EFF7B5542FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000094523F471F3F1F3F1F431F43
      1F431F431F431F431F3F1F3F3F47945200000000B5560F46BF5B7B1AD8010F4A
      10460F4AD8017B1ABF5B0F46B55A00000000FF7F7542FF7F5C63D84E5D63D84E
      5D63D84E5D63D84E5C63FF7F7542FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000093525F539D329C32BC32BC32
      BC32BC32BC32BC329C329D325F53935200000000000000000000BF5B7B1AD901
      0000D9017B1ABF5B00000000000000000000FF7F7642FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F7642FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000093527F5B7F5F7F5F7F5F7F5F
      7F5F7F5F7F5F7F5F7F5F7F5F7F5B9352000000000000000000000000BF5B7B1E
      00007B1EBF5B000000000000000000000000FF7F195B76467646764276427642
      764276427642764276467646195BFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000B55693529352935293529352
      9352935293529352935293529352B55600000000396739673967396739673967
      396739670000FF7FFF7FFF7FFF7FFF7FFF7F00000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F000000000000FF7F000000007B6F3967396739673967
      39673967396739673967396739677B6F00000000000000000000000000000000
      000000000000000000000000000000000000B84A550554055405540554055405
      54055505B84AFF7FFF7FFF7FFF7FFF7FFF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000F75ED656D656D656D656
      B556B556B556B556B556B556B556F75E00000000000000000000000000000000
      000000000000000000000000000000000000760D7B2EBC3ABC3A9C3A9C3ABC3A
      BC3A7B2E960DFF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D65600000000307A0000
      0000000000000000000000000000D65600000000000000000000000000000000
      000000000000000000000000000000000000F8191D4B9C327C2E7C2E7C2E7C2E
      9C321D4FB711FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000B55600000000E6700000
      0000000000000000000000000000B556000000000000000039677B6F00000000
      00000000000000000000000000000000000039265E5FDE3AFE3EFE7FBD7BFE3E
      FE3E7E63D815FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C5FDA46845CDA46
      B846B846B846B846B846B8465C63D55600000000000039672026653600000000
      0000000000000000000000000000000000003A227E639F67FF77FF7FDE7B9E6B
      3D535D5F1A22FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000000007710000
      0000FF7FFF7FFF7FFF7FFF7F0000D55600000000396700226A5B202239673967
      396739673967396739673967396739677B6FFF7B9C32FD4215676B62E7512E46
      F71D1726D952BD77DE7BFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63DA46845CFA46
      B84AB84AB84AB84AB846B8465C63D5560000396700224C5B6053E03E00222022
      20222022202220222022202220222026873AFF7FFF7FBF73B2568C622A560E42
      BA42FF73B93EB84AF95A39673967BD77FF7FF2310000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D5560000FF7F0671FF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000D556000020266E5F204F204F204F204F4053
      405340534053405340534053405340532026FF7FFF7F3867EF720F73AD6A4962
      954ADA42FF7FBA42993A16221726D84EBD77F231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000D5565C63DA46845CFA4A
      B84AB84AB84AB84AB84AB8465C63D556000020267067004F004F6C638C678C67
      8C678C678C678C678C678C678C678C6B2026FF7FFF7FE234B47F517BEF72714E
      792E7E5FFF73FF73FF77FF779E637936D8520000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5560000FF7BE670FF7F
      FE7FDE7FDE7FDE7FDE7FDE7B0000D556000000000022936B004FA03E00220022
      20222022202220222022202220222022C942FF7FFF7FE134AD66CF6E307B3826
      9F67DF6FDF6FDF6FDF6BDF6BDF6FBF67372AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C63DA46845CFA4A
      B94AB84AB84AB84AB84AB8465C63D5560000000000000022946F202200000000
      000000000000000000000000000000000000FF7FFF7F0239C555295EC8553922
      FF73BF67BF6BBF6BBF6BBF67BF67FF733826F231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000FF77E66CFF7B
      DE7BDD7BDD7BDD7BBD7BBD7B0000D55600000000000000002026643200000000
      000000000000000000000000000000000000FF7FFF7F0239E555275EC5555A26
      DF739F63BF67BF67BF67BF679F63DF73382A0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63D946845CDA46
      B84AB84AB84AB84AB84AB8465C63D55600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7F9C77223D63492141392A
      BF67BF679F639F639F639F63BF639F67392AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000B5560000BD73C568BD73
      9C739C739C739C739C739C730000B55600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7F7E67
      9B3A9E67DF73DF73DF73DF739E679A3A7D6300000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D65600000000317A0000
      0000000000000000000000000000D65600000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      7E637A32592E592E592E592E7A327D63FF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231FF7FFF7F00000000F75AD656D656D656D656
      B556B556B556B556B556B556D656F75A00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000396739673967396739670000
      FF7FFF7F00003967396739673967396700000000000000000000000000000000
      0000000000000000000000000000000000000000396739673967396739673967
      39673967396739673967396739675A6BFF7F0000DE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7B7B6FCA46602E602E602ECA469C73524ACE39AE35AD358D318C311042
      FF7FFF7F524ACE39AE35AD358D318C3110423967396739673967396739673967
      3967396739673967396739673967396739678C66476627664766476A466AAB39
      89412A6A466A4666266626662666696AFF7F5A6B195F195F195F195F195F195F
      195F1A5FC9468036E0426E63E0428036CA46EF39F03D734A1142AE354B296C2D
      FF7FFF7FEF39F03D734A1142AE354B296C2D216C006C006C006C006C006C006C
      006C006C006C006C006C006C006C006C216C47666E7F4B7F297BA772CA760C4E
      905A517F8649B07F8F7F8F7F907F2666FF7F993A993279327932793279327932
      79329C32602EE046E0420000E0420047602ECE3939675B6B3A67396318636B2D
      FF7FFF7FCE393A675B6B3A67396318636B2D7B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF7B7B5B27664B7F097BA772E77A2666C976
      917F507F627E86498F7F6E7F907F2666FF7F9932FF7BFF77FF77FF77FF77FF77
      FF77FF7B402A8E670000000000008E67602ECE39D6563963B6565246CE396B2D
      39673967CE39D6563963B6565246CE396B2D0070237DC07CC07CC07CC07CC07C
      C07CC07CC07CC07CC07CC07CC07C237D00704766297BA772E776487F26668E7F
      C661087FA57E637E86458E7F907F2666FF7F7932FF773C539E63DF6FDF6FDF6F
      DF6FFF734026204B004700002047204F602ECE39D6563963B656524ACE396B2D
      EF398C31CE39D6563963B656524ACE396C2D7B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF775B5B4766A772E77A487F6C7F06666D7F
      6D7FC65D097FA57E627E8649B17F466AFF7F7932FF77FF77DB421C4BDF6FDF6F
      DF6FFF730D4BA036404FAE6B404FA0368B32CE39D6563963B6565246CE396B29
      CE396C2DCE39D6563963B6565246CE396B2D0078877D807C807C807C807C807C
      807C807C807C807C807C807C807C877D00704766CB72266626660666886E6C7F
      4C7F6C7FC65D087FA47EB06AD031456EFF7F7932FF77BF6BFF7BDB42DF6FFB46
      582A592EFE4A0D4B402A4026402A0D4F9C36CE397B6FBD777B6F3967F75E4A29
      5A6B3146CE397B6FBD777B6F3967F75E6B2D7401FF73BC01DF63BC01DF63BC01
      DF63BD01FF7BFF77FF77FF77FF77FF775B5B4666AB726C7F6C7F6C7F4C7F4B7F
      4B7F4C7F6C7FC5617677323EF85AEF3539677932FF77BF67BF67FF7F1C4BFB46
      00000000FC461E4F0000DF6BDF6BFF779A32EF39AD358C316C2D6B2D6B2D3246
      5A6B3142744EAD358C316C2D6C2D6C2D524A72019D2A9F677901BF6B7901BF6B
      7901BF63407C407C407C407C407CCC7D00702666B27F4A7F4B7F4B7F4B7F4B7F
      4B7F4B7F4B7F6A7FF2399D733142F759B3657932FF77BF63BF633D4FDB420000
      BF67BF670000DB423D4FBF67BF63FF777932FF7FCE39F75E39679452EF3D4B29
      5A6B3146CE39F75A39679452EF3D6B2DFF7F5201FF7F57019E6B57019E6B5701
      9E6B5801FF7FFF77FF77FF77FF77FF775A5B2666B37F497F4A7F4A7F4A7F4A7F
      4A7F4A7F4A7F4A7F497FF139DC6E5962F6657932FF77BF637E57FB42FF7FBF63
      BF63BF63BF63FF7FFB427E57BF63FF777932FF7FCE39D75A1963944EEF3D6B2D
      CE396C2DCE39F75A1963944EEF3D6C2DFF7F5201DC429E6736019E6B36019E6B
      36019F63007C207C207C207C007C307E00702666B37F287F297F297F297F297F
      297F297F297F297F497F477F1966196AFF7F7932FF7B9F5BDB42FF779F5F9F5F
      9F5F9F5F9F5F9F5FFF77DB429F5BFF7B7932FF7FCF39F75E39679452EF3D6B2D
      EF398C31CF39F75E39679452F03D6C2DFF7F5201000014017E6714017E671401
      7E671501FF7FFF77FF77FF77FF77FF777B5B2666B47F917F917F927F927F927F
      927F927F927F927F917F917FD37F2566FF7F7932FF7BFC46DF739F5B9F5B9F5B
      9F5B9F5B9F5B9F5B9F5BDF73FC46FF7B7932FF7F9452AE358D318C2D6C2D3146
      FF7FFF7F734EAD358D318C318C31524AFF7F72011C5700001C5300001C530000
      1C53FF7FB37EB47EB47EB47EB57ED57E006C2666B57FC676C776C776C776C776
      C776C776C776C776C776C676D57F2666FF7F9932FF7FFF7FFF7BFF7BFF7BFF7B
      FF7BFF7BFF7BFF7BFF7BFF7BFF7FFF7F9932FF7FFF7F3967CF393963734E6C2D
      FF7FFF7FCF393963534A6C2D3967FF7FFF7F5726720152015201520152015201
      720174010074006C006C006C006C006CE7702666D67F267F277F277F277F277F
      277F277F277F277F277F267FD67F2666FF7F1B4F993279327932793279327932
      79327932793279327932793279329932DA42FF7FFF7FEF39324632468C316C2D
      FF7FFF7FCF39314632468D318C2DFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000002666B07F907F907F907F907F907F
      907F907F907F907F907F907FB07F2666FF7F0000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FEF39CE35AD358D316C2D
      FF7FFF7FEF39CE35AD358D318C2DFF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000686A266626662666266626662666
      2666266626662666266626662666686AFF7F0000000000000000000000005A6B
      F75EF75E5A6B00000000000000000000000000000000000000000000F75EF75E
      F75EDE7B00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B6F
      396739673967396739673967396739677B6F00005A6BF75EF75EF75EF75E734E
      10423146734EF75EF75EF75EF75E5A6B00000000BD771863F75EF75E6B2D4A29
      3146D65A1863BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B6F39673967396739673967CB51
      24412541254125412541254125412541CB4D00001042AD35AD35AD35AD35524A
      B5563967F75E524AAD35AD35AD35104200000000524A0821E71CE71CEF3D3146
      5A6B8C310821524A000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000010428C318C318C318C318D312445
      4B5EE455E459C451C451E459E4554B5E2441F75EAD353967CE39AD35DE7BBD77
      1042F75E7B6FF75E104210423146EF3D7B6F00002925EF3D31463146D65A5A6B
      5A6B94528C312925000000000000000000000000000000000000000000000000
      00000000000000009C73F75E0000000000008C31AE35CF39CF398C31AE312441
      0656824D8249413D413D8249824D06562441EF3DF75E2925EF3D8C31BD777B6F
      9C731042F75E5A6BF75E18631863734E945200006B2D945210421042EF3DCE39
      1863B55694528C31F75E00000000000000000000000000000000000000000000
      0000000000000000524A8C31F75E00000000CE39744EEF398C314A294B292445
      275AA24DA24D087F087FA24DA24D275A24410000AD356B2D10428C31BD777B6F
      7B6F7B6FEF3D396739675A6B5A6B5A6B524A0000AD35F75E524A524AFF7FBD77
      CE391863D65A94528C31F75E0000000000007B6FF75EF75EF75EF75EF75EF75E
      F75EF75EF75EF75E8C3118638C31F75E00008C3118638C31CE39C61809210441
      8C62814D814D81498149814D814D8C622441F75EAD35396731468C31BD775A6B
      5A6B7B6FEF3D7B6F7B6FAD35AD359C73524A0000EF3D5A6B9452BD770000DE7B
      5A6BCE391863B556B5568C31F75E0000000010428C318C318C318C318C318C31
      8C318C318C318C31524AF75EF75E8C31F75E1863AD35103E1863324653460341
      8D5E6C5A6C5E6C5A6C5A6C5E6C5E8D5E2441EF3DF75E2925524A8C31BD773967
      DE7B000010425A6B7B6FCE39CE397B6F734E0000CE39D65A9C735A6B524ACE39
      3146BD77EF3D5A6BCE39B5568C31F75E00008C31F75ED65AD65AD65AD65AD65A
      D65AD65AD65AD65AB556B556B55618638C310000FF7FAE35BD7BC614C614CA51
      E23CE23C0341033DE33C033DE24003412D5A0000AD356B2D734E8C31BD773967
      0000B556734E94525A6B5A6B5A6BB5561863000000009452524AD65A3146AD35
      EF3D524AB556AD359C73CE39EF3DCE3900008C315A6B39673967396739673967
      396739673967396739679452945239678C3100000000FF7F366FAD6A4A5A156B
      DE7BBE77E140DF77DF77E13C000019020000F75EAD35396794528C31BD771863
      0000AD35D65A734E524A524A734E104200000000000000005A6B734ED65A734E
      EF3D9C7300000000CE39D65A524A10420000524A8C318C318C318C318C318C31
      8C318C318C318C31314694525A6B8C310000000000005A6F31771073AD6A8C66
      3E0F7F07CC2DC040C040AC250000F7010000EF3DF75E2925B5568C31DE7B1863
      DE7B000000000000DE7B3967DE7BAD3500000000000000008C317B6FF75EB556
      9452CE39000000000000524A3146000000000000000000000000000000000000
      00000000000000008C315A6B8C3100000000000000006741B57F517BEF72CD6E
      48215F235E1F5F1F5F1F5E1B0000F60500000000AD356B2DB5568C31DE7B1863
      186318631863186318631863DE7B8C310000000000000000C618DE7B5A6B1863
      F75E841000000000000000000000000000000000000000000000000000000000
      0000000000000000734E8C3100000000000000000000E134AD6AAE6A1173CE6E
      4028000000000000000000000000F6050000F75EAD35F75EB5568C31FF7FF75E
      18636B2D314631461863F75EFF7F8C310000000000000000E71C4A29EF3DB556
      3146A51400000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000239E555295EEA510439
      802C19639C730000BF577F230000F6050000EF3DF75E0821B5568C31FF7FF75E
      F75EF75EF75EF75EF75EF75EFF7F8C310000000000000000E71CCE391042CE39
      0821A51400000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000705AE455275EE755E234
      AB411963F75ED75E9E43DC020000F60500000000AD35BD779C738C310000FF7F
      FF7F00000000FF7FFF7FFF7F0000AD3500000000000000004A29AD35EF3DAD35
      0821292500000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000004E5A624921456F5E
      0000000000000000000000000000F60500000000524AAD35AD35AD35AD358C31
      8C318C318C318C318C318C31AD35524A00000000000000009C73292529252925
      08219C7300000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000DC2E1806
      F705F605F605F605F605F605F605B93200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000039673967
      3967DE7B00000000000000000000000000000000396739673967396739673967
      3967396739673967396739673967396700000000000000000000000000003967
      3967396739670000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000DE7B396739673967CA458941
      496E166B3967DE7B000000000000000000005722F605F605F605F605F605F605
      F605F605F605F605F605F605F605F6055722000000000000000000000000EF3D
      9C73D65A10420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000B84A5509550536012A566E5A
      517FA5515701B84A00000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6057B6F396739673967396739671042
      BD77F75A1042396739673967396739677B6F000000000000F75E396700000000
      000000000000000000000000000000000000000076095A2E9C369D32C77E4F7F
      507F627EA555990100000000000000000000F605FF7F744E3146524A5A6BFF7F
      18631863FF7F1863186318631863FF7FF6053146AD358D318D31AD31AD31AE31
      8D318D318D31AD35AD35AD35AD35AD35314600000000F75E8C31EF3D00000000
      0000000000000000000000000000000000000000D819FD467C2E7C2A7E22C365
      077FA57E627EA65139670000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF605AE3539671863185F5A6B954A0060
      954E744E744E744E744E744E744EB552AE350000F75E8C3118638C31F75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E5A6B000019263D57BD36BD36FF7FDF73
      C461087FA57E627EA6513967000000000000F605FF7F534A314231463967FF7F
      F75EF75EFF7FF75EF75EF75EF75EFF7FF605CE391863D65AD65A3967D7520060
      D752B552B552B552B552B552B552F75ECE39F75E8C31F75EF75E524A8C318C31
      8C318C318C318C318C318C318C318C31EF3D00005A2E7E67FE3EDF6FFF7FDF77
      9F5FC465087FA47ED06EEF39396700000000F605FF7FFF7FFF7FFF7FFF7FDE7B
      FF7BFF7BDE7BFF7BFF7FFF7FFF7FFF7FF605CF39185FB556B5523963185B0060
      195BF75AD75AD75AD75AD75AD75A3A67CE398C311863B556B556B556D65AD65A
      D65AD65AD65AD65AD65AD65AD65AF75E8C3100005A261D4FBF6B76778B62E751
      7056DF67E369757B3242F85EEE3539670000F605FF7F524610421142185FDE7B
      D65AD65ADE7BD65AD65AD65AD65AFF7FF605EF3D3967944E744E7B6B5A630064
      5A631963186318631863186319639C6FCF398C31396794529452396739673967
      39673967396739673967396739675A6B8C3100000000FD42BE32175F6C5EE84D
      5246BF321E471042BD733142F759B3650000F605FF7FDE7BDE7BDE7BDE77BD77
      BD77BD77BD77BD77DE77DE77BD77FF7FF605F03D5A6B534A524A7B6B9C670064
      9C675A675A675A675A673A677B6BBD77EF3D00008C315A6B945231468C318C31
      8C318C318C318C318C318C318C318C31524A0000000000007B6FAF62EF6EAD66
      4C569C73000000003042DC725962F5650000F605FF7F3146F03D103EF75ABD73
      B556B556BD73B556B556B556B556FF7FF605103E9C73524A3246DE77DE6F0064
      DE6F9C6F7C6F7C6F7C6F9C6FDE77BD77F03D000000008C315A6B8C3100000000
      000000000000000000000000000000000000000000000000CB49937F1077CE6E
      AD6A0D52000000000000386E376A00000000F605FF7F9C73BD77BD739C739C6F
      9C739C739C6F9C739C739C739C73FF7FF605B656D65A9C73DE7BFF7FFF7B0068
      FF7BDF7BDE7BDE7BDE7BDE7BDE7BB656B6560000000000008C31EF3D00000000
      000000000000000000000000000000000000000000000000C030D57F727F3073
      0F73A02C0000000000000000000000000000F605FF7FF03DCE39CF39B5567B6F
      944E944E7B6F9452945294527352FF7FF6050000534A3146314231425342006C
      7342314231423142314231423142524A00000000000000000000000000000000
      000000000000000000000000000000000000000000000000E13484492A5ACF6A
      6C5EA12C0000000000000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6050000000000000000000000000070
      3967396739677B6F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000239E655295EEA55
      2539C2300000000000000000000000000000F6097E5B7B1A7B1E7B1E7B1E7B1E
      7B1E7B1E7B1E7B1E7B1E7B1E7B1A7E5BF6090000000000000000000000000070
      0070007400740871000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000006741C455275EE755
      243967410000000000000000000000000000170A5D4F3D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F3D4F5D4F170A0000000000000000000000000070
      00700068005C0070000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000009B77444163454341
      443D9B770000000000000000000000000000992A170A160A160A160A160A160A
      160A160A160A160A160A160A160A170A992A0000000000000000000000000070
      00700070006C297500000000000000000000424D3E000000000000003E000000
      2800000040000000D00000000100010000000000800600000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFF000F00FFFFFFFFFF000E007FE7F
      0000C7FEE007FE7F0000C756E007FE7F00001402E007FC3F00001402E007FC3F
      00005402E007FC3F00005402E007FC3F00005402E007FC3F00005402E007F81F
      000057FEE007F81F00005000E007000000005FFBE0070000FFFF4003E007F00F
      FFFF7FEFE007FFFFFFFF000FF00FFFFF80008000FFFFFFFF00008000FFFFFFFF
      0000800000000000000080000000000000008001000000000000C00100000000
      0000C003000000000000F007000000000000F007000000000000E00700000000
      0000E007000000000000E007000000000000E007000000000000E007FFFF8001
      0000F00FFFFFFFFF0000F81FFFFFFFFFFFF0FC3FFFFFFFFF0000FC1FFFFF0000
      00008007000000000000000300000000000000007FFE00000001000000000000
      0003000000000000000300000000000000030000000000000003000000000000
      021300000000000000C3000000000000000300017FFE00000003000100000000
      00030003FFFF000000030007FFFF0000F000FFDFFFFFFF8F0800FF9FE0078007
      04008081E007800302000001E007800101000001E007800100800081E0078001
      00400391E0078001002003C1E0078003001003E1F00FC007000807E1F81FE00F
      00040003F81FE07F0002C003F81FE07F0000C007F81FE07F0000E003F81FE07F
      0000E003F81FE07F0000F01FF81FE07FFFFFFFFEFFFFFFFFFFC10000FFFFFFC1
      FF8000000000FF80FF8000000000FF80000800007FFE0000001C00004002001C
      000800007D6E0000000000004002000000000000400200000000000040000000
      0000000040000000000000000000000000000000400200000000000000000000
      00000000FFFF000080010000FFFF8001E0008000E7FFCF8300000000E3FFC701
      00000000E0FFC10100000000E07FC01100380000F03FE03900000000F01FE011
      07000000C00F800100000000C007800300000000C003800700000000C0038007
      00000000C003800700000000C01F803F00000000E01FC03F00000000E00FC01F
      00000000E00FC01F00000000E00FC01FFFFFFFC1FFC1C00300008000FF80C003
      000080000000DFFB000080000008D24B0000801C001CD00B000080000188D00B
      000080000180D00B000080010001D00B000080030005D00B0000800301E1D00B
      0000000101E1DFFB000000010001C003000000010001C003000000010001C003
      000000010001C003000087C30001C003FC7FFFFEFFFF8001FC3F0000FFFF8001
      80030000FFFF800180030000FFFF800180030000FFFF800180030000FE3F8001
      80010000FC3F800180000000F81F800180000000F00F800100000000F00F8001
      00010000F00F800100010000FFFF800100010000FFFF800180030000FFFF8001
      F11F0000FFFF8001F93F0000FFFF80018040DB6EC001FFFF00000000C001FFFF
      00000000DBFDFFFF00008002DBFDE7FF00000000C001C7FF00000000DB058000
      00008002C001000000005B6CD005000000000000C001000000008002D0058000
      00000000C001C7FF00000000D005E7FF00008002C001FFFF00000000D005FFFF
      0000DB6CDBFDFFFF00000000C001FFFFFFC18241FFFF80008000000000000000
      00000000000000000008000000000000001C0000000000000008000000000000
      0000000000000000000000000000000001900000000000000240000000000000
      00000000000000000000000040000000000000002A0000000000000000000000
      00000000FFFF0000FFFF0000FFFF0000FC3FF87FFFFFFC008001801FFFFF0000
      8001801FFFFF00000000801FFFE700000000800FFFE300008000800700010000
      000084030000000000808001000080008100C0010000C0050101E0610001C005
      00E1E073FFE3C0058001E07FFFE7C0FD0001E07FFFFFC0250001E07FFFFFC005
      84C5E07FFFFFE1FD8001E07FFFFFF801FFFFF87F8001FC3FFFFF801F0000FC3F
      FFFF801F00000000E7FF801F00000000C7FF800F000000008000800700000000
      000080030000000000008001000000000000C001000000008000E06100000000
      C7FFE07300000000E7FFE07F00008001FFFFE07F0000FC1FFFFFE07F0000FC1F
      FFFFE07F0000FC1FFFFFE07F0000FC1F00000000000000000000000000000000
      000000000000}
  end
  inherited dsCL: TevDataSource
    Left = 1028
    Top = 90
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 890
    Top = 90
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 956
    Top = 173
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 956
    Top = 136
  end
  object DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL
    Left = 924
    Top = 92
  end
  object srcCoStates: TevDataSource
    DataSet = DM_CO_STATES.CO_STATES
    MasterDataSource = wwdsMaster
    Left = 1054
    Top = 122
  end
  object evSecElement1: TevSecElement
    ElementType = otFunction
    ElementTag = 'USER_CAN_EDIT_TAX_EXEMPTIONS'
    Left = 336
  end
  object pmFed_Tax_Deposit_Frequency: TevPopupMenu
    Left = 781
    Top = 541
    object FTDFCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = FTDFCopyToClick
    end
  end
  object cdsSbBankAccountsForAch: TevClientDataSet
    Filter = 'ACH_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 876
    Top = 527
  end
  object pmFed945_Tax_Deposit_Frequency: TevPopupMenu
    Left = 814
    Top = 545
    object Fed945TDFCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = Fed945TDFCopyToClick
    end
  end
  object pmFUI_Tax_Deposit_Frequency: TevPopupMenu
    Left = 843
    Top = 545
    object FUITDFCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = FUITDFCopyToClick
    end
  end
  object cdsSbBankAccountsForTrust: TevClientDataSet
    Filter = 'TRUST_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 908
    Top = 527
  end
  object cdsSbBankAccountsForTax: TevClientDataSet
    Filter = 'TAX_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 940
    Top = 527
  end
  object cdsSbBankAccountsForBilling: TevClientDataSet
    Filter = 'BILLING_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 972
    Top = 527
  end
  object cdsSbBankAccountsForWC: TevClientDataSet
    Filter = 'WORKERS_COMP_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 1004
    Top = 527
  end
  object cdsSbBankAccountsForOBC: TevClientDataSet
    Filter = 'OBC_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 1036
    Top = 527
  end
  object pmEnable_ESS: TevPopupMenu
    Left = 805
    Top = 317
    object Enable_ESSCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = Enable_ESSCopyToClick
    end
  end
  object pmTaxImpound: TevPopupMenu
    Left = 901
    Top = 437
    object MenuItem1: TMenuItem
      Caption = 'Copy To...'
      OnClick = MenuItem1Click
    end
  end
  object pmTrustImpound: TevPopupMenu
    Left = 965
    Top = 445
    object MenuItem2: TMenuItem
      Caption = 'Copy To...'
      OnClick = MenuItem2Click
    end
  end
  object pmDDImpound: TevPopupMenu
    Left = 1005
    Top = 445
    object MenuItem3: TMenuItem
      Caption = 'Copy To...'
      OnClick = MenuItem3Click
    end
  end
  object pmBillingImpound: TevPopupMenu
    Left = 877
    Top = 445
    object MenuItem4: TMenuItem
      Caption = 'Copy To...'
      OnClick = MenuItem4Click
    end
  end
  object pmWCImpound: TevPopupMenu
    Left = 941
    Top = 445
    object MenuItem5: TMenuItem
      Caption = 'Copy To...'
      OnClick = MenuItem5Click
    end
  end
  object pmProcessLimitations: TevPopupMenu
    Left = 829
    Top = 445
    object MenuItem6: TMenuItem
      Caption = 'Copy To...'
      OnClick = MenuItem6Click
    end
  end
  object pmMicsCheck: TevPopupMenu
    Left = 797
    Top = 437
    object miMiscCheckFormCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = miMiscCheckFormCopyToClick
    end
  end
  object pmCheckForm: TevPopupMenu
    Left = 765
    Top = 429
    object miCheckFormCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = miCheckFormCopyToClick
    end
  end
  object pmSalesTax: TevPopupMenu
    Left = 893
    Top = 269
    object MenuItem7: TMenuItem
      Caption = 'Copy To...'
      OnClick = MenuItem7Click
    end
  end
  object pmShowRateHours: TevPopupMenu
    Left = 781
    Top = 509
    object miCopyShowRateHours: TMenuItem
      Caption = 'Copy To...'
      OnClick = miCopyShowRateHoursClick
    end
  end
  object pmEnable_Time_Off: TevPopupMenu
    Left = 805
    Top = 357
    object Enable_TimeOffCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = Enable_TimeOffCopyToClick
    end
  end
  object pmEnable_Benefits: TevPopupMenu
    Left = 805
    Top = 397
    object Enable_BenefitsCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = Enable_BenefitsCopyToClick
    end
  end
  object pmEnable_HR: TevPopupMenu
    OnPopup = pmEnable_HRPopup
    Left = 805
    Top = 253
    object Enable_HRCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = Enable_HRCopyToClick
    end
  end
  object pmAnnualFormType: TevPopupMenu
    Left = 760
    Top = 392
    object CopyTo1: TMenuItem
      Caption = 'Copy To...'
      OnClick = CopyTo1Click
    end
    object mnAnnualTypeCopyToEE: TMenuItem
      Caption = 'Copy To EE...'
      OnClick = mnAnnualTypeCopyToEEClick
    end
  end
  object pmEnable_DD: TevPopupMenu
    Left = 845
    Top = 317
    object Enable_DDCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = Enable_DDCopyToClick
    end
  end
  object pmEnable_EEAccess: TevPopupMenu
    Left = 885
    Top = 317
    object Enable_EEAcces: TMenuItem
      Caption = 'Copy To...'
      OnClick = Enable_EEAccesClick
    end
  end
  object pmEEPrintVoucher: TevPopupMenu
    Left = 712
    Top = 600
    object mnCOPrintVoucher: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnCOPrintVoucherClick
    end
    object mnEEPrintVoucher: TMenuItem
      Caption = 'Copy To EE...'
      OnClick = mnEEPrintVoucherClick
    end
  end
  object pmPrintCPA: TevPopupMenu
    Left = 781
    Top = 589
    object mnPrintCPA: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnPrintCPAClick
    end
  end
  object cdAvailableCertificationsEligibility: TevClientDataSet
    Left = 680
    Top = 688
    object cdAvailableCertificationsEligibilityACA_CERT_ELIGIBILITY_TYPE: TStringField
      FieldName = 'ACA_CERT_ELIGIBILITY_TYPE'
      Size = 1
    end
    object cdAvailableCertificationsEligibilityDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 80
    end
  end
  object dsAvailableCertificationsEligibility: TevDataSource
    DataSet = cdAvailableCertificationsEligibility
    Left = 712
    Top = 688
  end
  object dsAssignedCertificationsEligibility: TevDataSource
    DataSet = DM_CO_ACA_CERT_ELIGIBILITY.CO_ACA_CERT_ELIGIBILITY
    Left = 712
    Top = 658
  end
  object pmEnableAnalytics: TevPopupMenu
    Left = 789
    Top = 141
    object mnEnableAnalytics: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnEnableAnalyticsClick
    end
  end
  object pmAnalyticsLevel: TevPopupMenu
    Left = 789
    Top = 173
    object mnAnalyticsLevel: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnAnalyticsLevelClick
    end
  end
  object pmBenefitsEligibleDefault: TevPopupMenu
    Left = 357
    Top = 469
    object mnBenefitsEligibleDefaultCO: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnBenefitsEligibleDefaultCOClick
    end
    object mnBenefitsEligibleDefaultEE: TMenuItem
      Caption = 'Copy To EE...'
      OnClick = mnBenefitsEligibleDefaultEEClick
    end
  end
  object pmPayrollProduct: TevPopupMenu
    Left = 821
    Top = 589
    object mnPayrollProduct: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnPayrollProductClick
    end
  end
  object pmAleGroup: TevPopupMenu
    Left = 853
    Top = 589
    object mnAleGroup: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnAleGroupClick
    end
  end
  object pmACAService: TevPopupMenu
    Left = 805
    Top = 669
    object mnACAService: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnACAServiceClick
    end
  end
  object pmEnforgeDOB: TevPopupMenu
    Left = 597
    Top = 341
    object mnEnforgeDOB: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnEnforgeDOBClick
    end
  end
end
