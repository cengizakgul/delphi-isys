// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_CO_GENERAL_LEDGER;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, wwdblook, Wwdotdot,
  Wwdbcomb, StdCtrls, Mask, wwdbedit, Db, Wwdatsrc, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Controls, Classes,
  EvConsts, wwFrame, SysUtils, Windows, Variants, SDDClasses, EvContext, isVCLBugFix,
  ISBasicClasses, SDataDictclient, SDataDicttemp, Menus, SPD_EDIT_COPY, EvUtils,
  EvUIComponents, EvClientDataSet, EvUIUtils, Dialogs, isUIwwDBLookupCombo,
  isUIwwDBComboBox, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_GENERAL_LEDGER = class(TEDIT_CO_BASE)
    tshtGeneral_Ledger_Numbers: TTabSheet;
    wwdgGeneral_Ledger_Number: TevDBCheckGrid;
    evPopupMenu1: TevPopupMenu;
    CopySelectedGLs: TMenuItem;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpCOGeneralLedgerSummary: TisUIFashionPanel;
    fpCOGeneralLedgerDetails: TisUIFashionPanel;
    qlblLevelType: TevLabel;
    lblDataType: TevLabel;
    lblFormat: TevLabel;
    edtFormat: TevDBEdit;
    wwcbDataType: TevDBComboBox;
    wwcbLevelType: TevDBComboBox;
    lblLevelValue: TevLabel;
    wwlcLevelValue: TevDBLookupCombo;
    wwlcDataValue: TevDBLookupCombo;
    lblDataValue: TevLabel;
    btnMultiInsert: TevBitBtn;
    sbGeneralLedger: TScrollBox;
    procedure wwlcLevelValueDropDown(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure wwcbLevelTypeChange(Sender: TObject);
    procedure wwcbDataTypeChange(Sender: TObject);
    procedure btnMultiInsertClick(Sender: TObject);
    procedure CopySelectedGLsClick(Sender: TObject);
    procedure pnlCOBrowseLeftResize(Sender: TObject);
  private
    procedure ChooseDBDT;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  public
    function GetInsertControl: TWinControl; override;
    function DeleteMessage: Word; override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    procedure Deactivate; override;
    procedure Activate; override;
  end;


implementation

uses
  SPD_DBDTSelectionListFiltr, SPD_GLGroup_Insert, SPackageEntry,
  SDM_CO_GENERAL_LEDGER, SFrameEntry, SPD_EDIT_Open_BASE;

{$R *.DFM}

procedure TEDIT_CO_GENERAL_LEDGER.Activate;
begin
  inherited;

end;

procedure TEDIT_CO_GENERAL_LEDGER.ChooseDBDT;
var
  DBDTSelectionList: TDBDTSelectionListFiltr;
  iDivNbr, iBranchNbr, iDeptNbr, iTeamNbr: Variant;
begin
  inherited;
  iDivNbr := null;
  iBranchNbr := null;
  iDeptNbr := null;
  iTeamNbr := null;
  if wwcbLevelType.Value = GL_LEVEL_DIVISION then
    iDivNbr := wwdsDetail.DataSet.FieldbyName('LEVEL_NBR').Value
  else if wwcbLevelType.Value = GL_LEVEL_BRANCH then
    iBranchNbr := wwdsDetail.DataSet.FieldbyName('LEVEL_NBR').Value
  else if wwcbLevelType.Value = GL_LEVEL_DEPT then
    iDeptNbr := wwdsDetail.DataSet.FieldbyName('LEVEL_NBR').Value
  else if wwcbLevelType.Value = GL_LEVEL_TEAM then
    iTeamNbr := wwdsDetail.DataSet.FieldbyName('LEVEL_NBR').Value;

  DBDTSelectionList := TDBDTSelectionListFiltr.Create(nil);
  try
    with DM_COMPANY, DBDTSelectionList do
    begin
      DBDTSelectionList.Setup(DM_COMPANY.CO_DIVISION, DM_COMPANY.CO_BRANCH,
        DM_COMPANY.CO_DEPARTMENT, DM_COMPANY.CO_TEAM,
        iDivNbr, iBranchNbr, iDeptNbr, iTeamNbr,
        DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionList.ShowModal = mrOK then
      begin
        if wwdsDetail.DataSet.State = dsBrowse then
          if wwdsDetail.DataSet.RecordCount = 0 then
            wwdsDetail.DataSet.Insert
          else
            wwdsDetail.DataSet.Edit;
        if wwcbLevelType.Value = GL_LEVEL_DIVISION then
          wwdsDetail.DataSet.FieldbyName('LEVEL_NBR').Value := wwcsTempDBDT.FieldByName('DIVISION_NBR').Value
        else if wwcbLevelType.Value = GL_LEVEL_BRANCH then
          wwdsDetail.DataSet.FieldbyName('LEVEL_NBR').Value := wwcsTempDBDT.FieldByName('BRANCH_NBR').Value
        else if wwcbLevelType.Value = GL_LEVEL_DEPT then
          wwdsDetail.DataSet.FieldbyName('LEVEL_NBR').Value := wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value
        else if wwcbLevelType.Value = GL_LEVEL_TEAM then
          wwdsDetail.DataSet.FieldbyName('LEVEL_NBR').Value := wwcsTempDBDT.FieldByName('TEAM_NBR').Value;
      end;
    end;
  finally
    DBDTSelectionList.Free;
  end;
end;

procedure TEDIT_CO_GENERAL_LEDGER.wwlcLevelValueDropDown(Sender: TObject);
begin
  inherited;
  if wwlcLevelValue.ButtonStyle = cbsEllipsis then
  begin
    ChooseDBDT;
    AbortEx;
  end;
end;

procedure TEDIT_CO_GENERAL_LEDGER.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_F4) and wwlcLevelValue.Enabled and
     (wwlcLevelValue.ButtonStyle = cbsEllipsis) then
  begin
    ChooseDBDT;
    Key := 0;
  end;
end;

procedure TEDIT_CO_GENERAL_LEDGER.wwdsDataChange(Sender: TObject; Field: TField);
begin
  if not Assigned(Field) or (UpperCase(Field.FieldName) = 'LEVEL_TYPE') then
  begin
    if (wwdsDetail.State in [dsEdit, dsInsert]) and Assigned(Field) then
      wwdsDetail.DataSet.FieldByName('LEVEL_NBR').Clear;
    wwlcLevelValue.Enabled := (wwcbLevelType.Value <> '') and
                              (wwcbLevelType.Value <> GL_LEVEL_NONE);
    wwlcLevelValue.DataField := '';
    if wwcbLevelType.Value = GL_LEVEL_DIVISION then
    begin
      wwlcLevelValue.ButtonStyle := cbsEllipsis;
      wwlcLevelValue.LookupField := '';
      wwlcLevelValue.LookupTable := DM_COMPANY.CO_DIVISION;
      wwlcLevelValue.LookupField := 'CO_DIVISION_NBR';
      wwlcLevelValue.Selected.Text := 'CUSTOM_DIVISION_NUMBER'#9'15';
    end
    else if wwcbLevelType.Value = GL_LEVEL_BRANCH then
    begin
      wwlcLevelValue.ButtonStyle := cbsEllipsis;
      wwlcLevelValue.LookupField := '';
      wwlcLevelValue.LookupTable := DM_COMPANY.CO_BRANCH;
      wwlcLevelValue.LookupField := 'CO_BRANCH_NBR';
      wwlcLevelValue.Selected.Text := 'CUSTOM_BRANCH_NUMBER'#9'15';
    end
    else if wwcbLevelType.Value = GL_LEVEL_DEPT then
    begin
      wwlcLevelValue.ButtonStyle := cbsEllipsis;
      wwlcLevelValue.LookupField := '';
      wwlcLevelValue.LookupTable := DM_COMPANY.CO_DEPARTMENT;
      wwlcLevelValue.LookupField := 'CO_DEPARTMENT_NBR';
      wwlcLevelValue.Selected.Text := 'CUSTOM_DEPARTMENT_NUMBER'#9'15';
    end
    else if wwcbLevelType.Value = GL_LEVEL_TEAM then
    begin
      wwlcLevelValue.ButtonStyle := cbsEllipsis;
      wwlcLevelValue.LookupField := '';
      wwlcLevelValue.LookupTable := DM_COMPANY.CO_TEAM;
      wwlcLevelValue.LookupField := 'CO_TEAM_NBR';
      wwlcLevelValue.Selected.Text := 'CUSTOM_TEAM_NUMBER'#9'15';
    end
    else if wwcbLevelType.Value = GL_LEVEL_EMPLOYEE then
    begin
      wwlcLevelValue.ButtonStyle := cbsDownArrow;
      wwlcLevelValue.LookupField := '';
      wwlcLevelValue.LookupTable := DM_EMPLOYEE.EE;
      wwlcLevelValue.LookupField := 'EE_NBR';
      wwlcLevelValue.Selected.Text :=
        'CUSTOM_EMPLOYEE_NUMBER'#9'13'#13#10 +
        'Employee_Name_Calculate'#9'35'#13#10 +
        'SOCIAL_SECURITY_NUMBER'#9'13';
    end
    else if wwcbLevelType.Value = GL_LEVEL_JOB then
    begin
      wwlcLevelValue.ButtonStyle := cbsDownArrow;
      wwlcLevelValue.LookupField := '';
      wwlcLevelValue.LookupTable := DM_COMPANY.CO_JOBS;
      wwlcLevelValue.LookupField := 'CO_JOBS_NBR';
      wwlcLevelValue.Selected.Text := 'Description'#9'40';
    end;
    wwlcLevelValue.DataField := 'LEVEL_NBR';
    wwlcLevelValue.LookupTable := wwlcLevelValue.LookupTable;
    wwlcLevelValue.LookupValue := wwlcLevelValue.DataSource.DataSet.FieldByName(wwlcLevelValue.DataField).AsString;
  end;

  if not Assigned(Field) or (UpperCase(Field.FieldName) = 'DATA_TYPE') then
  begin
    if (wwdsDetail.State in [dsEdit, dsInsert]) and Assigned(Field) then
      wwdsDetail.DataSet.FieldByName('DATA_NBR').Clear;
    wwlcDataValue.Enabled := (wwcbDataType.Value <> '') and
                             (wwcbDataType.Value <> GL_DATA_NONE) and
                             (wwcbDataType.Value <> GL_DATA_FEDERAL) and
                             (wwcbDataType.Value <> GL_DATA_ER_OASDI) and
                             (wwcbDataType.Value <> GL_DATA_EE_OASDI) and
                             (wwcbDataType.Value <> GL_DATA_EE_MEDICARE) and
                             (wwcbDataType.Value <> GL_DATA_ER_MEDICARE) and
                             (wwcbDataType.Value <> GL_DATA_FUI) and
                             (wwcbDataType.Value <> GL_DATA_EIC) and
                             (wwcbDataType.Value <> GL_DATA_BACKUP) and
                             (wwcbDataType.Value <> GL_DATA_FEDERAL_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_ER_OASDI_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_EE_OASDI_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_EE_MEDICARE_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_ER_MEDICARE_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_FUI_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_EIC_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_BACKUP_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_BILLING) and
                             (wwcbDataType.Value <> GL_DATA_BILLING_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_ER_MATCH_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_NET_PAY) and
                             (wwcbDataType.Value <> GL_DATA_TAX_IMPOUND) and
                             (wwcbDataType.Value <> GL_DATA_TAX_IMPOUND_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_TRUST_IMPOUND) and
                             (wwcbDataType.Value <> GL_DATA_TRUST_IMPOUND_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_3P_CHECK) and
                             (wwcbDataType.Value <> GL_DATA_3P_TAX) and
                             (wwcbDataType.Value <> GL_DATA_TAX_CHECK_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_TAX_CHECK) and
                             (wwcbDataType.Value <> GL_DATA_VT_HEALTHCARE) and
                             (wwcbDataType.Value <> GL_DATA_UI_ROUNDING) and
                             (wwcbDataType.Value <> GL_DATA_VT_HEALTHCARE_OFFSET) and
                             (wwcbDataType.Value <> GL_DATA_UI_ROUNDING_OFFSET);

    wwlcDataValue.DataField := '';
    if (wwcbDataType.Value = GL_DATA_AGENCY_CHECK) or
       (wwcbDataType.Value = GL_DATA_AGENCY_CHECK_OFFSET) then
    begin
      wwlcDataValue.LookupField := '';
      wwlcDataValue.LookupTable := DM_CLIENT.CL_AGENCY;
      wwlcDataValue.LookupField := 'CL_AGENCY_NBR';
      wwlcDataValue.Selected.Text := 'Agency_Name'#9'40';
    end
    else if (wwcbDataType.Value = GL_DATA_WC_IMPOUND) or
            (wwcbDataType.Value = GL_DATA_WC_IMPOUND_OFFSET) then
    begin
      wwlcDataValue.LookupField := '';
      wwlcDataValue.LookupTable := DM_COMPANY.CO_WORKERS_COMP;
      wwlcDataValue.LookupField := 'CO_WORKERS_COMP_NBR';
      wwlcDataValue.Selected.Text := 'WORKERS_COMP_CODE'#9'5'#13#10 +
                                      'Description'#9'40';
    end
    else if (wwcbDataType.Value = GL_DATA_ED) or
            (wwcbDataType.Value = GL_DATA_ED_OFFSET) then
    begin
      wwlcDataValue.LookupField := '';
      wwlcDataValue.LookupTable := DM_COMPANY.CO_E_D_CODES;
      wwlcDataValue.LookupField := 'CL_E_DS_NBR';
      wwlcDataValue.Selected.Text := 'ED_Lookup'#9'15'#13#10 +
                                      'CodeDescription'#9'40';
    end
    else if (wwcbDataType.Value = GL_DATA_STATE) or
            (wwcbDataType.Value = GL_DATA_EE_SDI) or
            (wwcbDataType.Value = GL_DATA_ER_SDI) or
            (wwcbDataType.Value = GL_DATA_EE_SDI_OFFSET) or
            (wwcbDataType.Value = GL_DATA_ER_SDI_OFFSET) or
            (wwcbDataType.Value = GL_DATA_STATE_OFFSET) then
    begin
      wwlcDataValue.LookupField := '';
      wwlcDataValue.LookupTable := DM_COMPANY.CO_STATES;
      wwlcDataValue.LookupField := 'CO_STATES_NBR';
      wwlcDataValue.Selected.Text := 'STATE'#9'2';
    end
    else if (wwcbDataType.Value = GL_DATA_EE_SUI) or
            (wwcbDataType.Value = GL_DATA_EE_SUI_OFFSET) then
    begin
      DM_COMPANY.CO_SUI.Filter := 'EE_Or_ER in (''' + GROUP_BOX_EE + ''', ''' + GROUP_BOX_EE_OTHER + ''')';
      DM_COMPANY.CO_SUI.Filtered := True;
      wwlcDataValue.LookupField := '';
      wwlcDataValue.LookupTable := DM_COMPANY.CO_SUI;
      wwlcDataValue.LookupField := 'CO_SUI_NBR';
      wwlcDataValue.Selected.Text := 'Sui_Name'#9'40';
    end
    else if (wwcbDataType.Value = GL_DATA_ER_SUI) or
            (wwcbDataType.Value = GL_DATA_ER_SUI_OFFSET) then
    begin
      DM_COMPANY.CO_SUI.Filter := 'EE_Or_ER in (''' + GROUP_BOX_ER + ''', ''' + GROUP_BOX_ER_OTHER + ''')';
      DM_COMPANY.CO_SUI.Filtered := True;
      wwlcDataValue.LookupField := '';
      wwlcDataValue.LookupTable := DM_COMPANY.CO_SUI;
      wwlcDataValue.LookupField := 'CO_SUI_NBR';
      wwlcDataValue.Selected.Text := 'Sui_Name'#9'40';
    end
    else if (wwcbDataType.Value = GL_DATA_LOCAL) or
            (wwcbDataType.Value = GL_DATA_LOCAL_OFFSET) then
    begin
      wwlcDataValue.LookupField := '';
      wwlcDataValue.LookupTable := DM_COMPANY.CO_LOCAL_TAX;
      wwlcDataValue.LookupField := 'CO_LOCAL_TAX_NBR';
      wwlcDataValue.Selected.Text := 'LocalName'#9'20'#13#10 +
                                     'LocalState'#9'2';
    end;
    wwlcDataValue.DataField := 'DATA_NBR';
    wwlcDataValue.LookupTable := wwlcDataValue.LookupTable;
    wwlcDataValue.LookupValue := wwlcDataValue.DataSource.DataSet.FieldByName(wwlcDataValue.DataField).AsString;
  end;
end;

procedure TEDIT_CO_GENERAL_LEDGER.wwcbLevelTypeChange(Sender: TObject);
begin
  inherited;
  wwdsDataChange(Sender, wwdsDetail.DataSet.FieldByName('LEVEL_TYPE'));
end;

procedure TEDIT_CO_GENERAL_LEDGER.wwcbDataTypeChange(Sender: TObject);
begin
  inherited;
  wwdsDataChange(Sender, wwdsDetail.DataSet.FieldByName('DATA_TYPE'));
end;

procedure TEDIT_CO_GENERAL_LEDGER.btnMultiInsertClick(Sender: TObject);
var
  i, j, k, l: Integer;
  LevelValue, DataValue: Variant;
begin
  inherited;
  with TGLGroup_Insert.Create(Self) do
  try
    TevClientDataSet(wwdsDetail.DataSet).DisableControls;
    if ShowModal = mrOK then
    begin
      ctx_StartWait;
      try
        for i := 0 to Pred(lbLevelType.Items.Count) do
          if lbLevelType.Selected[i] then
          begin
            if not lbLevelValue.Enabled or (lbLevelValue.SelectedList.Count = 0) then
              LevelValue := VarArrayOf([null])
            else
            begin
              LevelValue := VarArrayCreate([0, Pred(lbLevelValue.SelectedList.Count)], varInteger);
              for j := 0 to Pred(lbLevelValue.SelectedList.Count) do
              begin
                lbLevelValue.DataSource.DataSet.GotoBookmark(lbLevelValue.SelectedList.Items[j]);
                if lbLevelValue.DataSource.DataSet.Name = 'CO_EMPLOYEE' then
                  LevelValue[j] := lbLevelValue.DataSource.DataSet['EE_NBR']
                else
                  LevelValue[j] := lbLevelValue.DataSource.DataSet[lbLevelValue.DataSource.DataSet.Name + '_NBR'];
              end;
            end;

            for j := VarArrayLowBound(LevelValue, 1) to VarArrayHighBound(LevelValue, 1) do
              for k := 0 to Pred(lbDataType.Items.Count) do
                if lbDataType.Selected[k] then
                begin
                  if not lbDataValue.Enabled or (lbDataValue.SelectedList.Count = 0) then
                    DataValue := VarArrayOf([null])
                  else
                  begin
                    DataValue := VarArrayCreate([0, Pred(lbDataValue.SelectedList.Count)], varInteger);
                    for l := 0 to Pred(lbDataValue.SelectedList.Count) do
                    begin
                      lbDataValue.DataSource.DataSet.GotoBookmark(lbDataValue.SelectedList.Items[l]);
                      if lbDataValue.DataSource.DataSet.Name = 'CO_E_D_CODES' then
                        DataValue[l] := lbDataValue.DataSource.DataSet['CL_E_DS_NBR']
                      else
                        DataValue[l] := lbDataValue.DataSource.DataSet[lbDataValue.DataSource.DataSet.Name + '_NBR'];
                    end;
                  end;

                  for l := VarArrayLowBound(DataValue, 1) to VarArrayHighBound(DataValue, 1) do
                    with wwdsDetail do
                    begin
                      DataSet.Insert;
                      DataSet['LEVEL_TYPE'] := slLevelType.Values[lbLevelType.Items[i]];
                      DataSet['LEVEL_NBR'] := LevelValue[j];
                      DataSet['DATA_TYPE'] := slDataType.Values[lbDataType.Items[k]];
                      DataSet['DATA_NBR'] := DataValue[l];
                      DataSet['GENERAL_LEDGER_FORMAT'] := edtGLMask.Text;
                      DataSet.Post;
                    end;
                end;
          end;
      finally
        ctx_EndWait;
      end;
    end;
  finally
    TevClientDataSet(wwdsDetail.DataSet).EnableControls;
    Free;
  end;
end;

function TEDIT_CO_GENERAL_LEDGER.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_GENERAL_LEDGER;
end;

function TEDIT_CO_GENERAL_LEDGER.GetInsertControl: TWinControl;
begin
  Result := wwcbLevelType;
end;

procedure TEDIT_CO_GENERAL_LEDGER.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);
  AddDS(DM_EMPLOYEE.EE, aDS);
  AddDS(DM_COMPANY.CO_JOBS, aDS);
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_SUI, aDS);
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  AddDS(DM_COMPANY.CO_GENERAL_LEDGER, aDS);
  inherited;
end;

function TEDIT_CO_GENERAL_LEDGER.GetDataSetConditions(
  sName: string): string;
begin
  if (sName = 'CO_BRANCH') or (sName = 'CO_DEPARTMENT') or (sName = 'CO_TEAM') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_GENERAL_LEDGER.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
  if Kind = NavOK then
    btnMultiInsert.Enabled := not Value;
end;

procedure TEDIT_CO_GENERAL_LEDGER.Deactivate;
begin
  inherited;
  DM_COMPANY.CO_SUI.Filter := '';
  DM_COMPANY.CO_SUI.Filtered := False;
end;

procedure TEDIT_CO_GENERAL_LEDGER.CopySelectedGLsClick(Sender: TObject);
var CopyForm: TEDIT_COPY;
    tmp_Co_GL_Nbr, tmp_Co_Nbr, tmp_Cl_Nbr, tmp_Custco_Nbr : string;
    aDS: TArrayDS;
    Close: Boolean;
begin
  inherited;

  if (wwdgGeneral_Ledger_Number.SelectedList.Count > 0) then
  begin
    CopyForm := TEDIT_COPY.Create(Self);
    CopyForm.wwcsCopySecond.Data := DM_CLIENT.CO_GENERAL_LEDGER.Data;
    tmp_Co_GL_Nbr := DM_CLIENT.CO_GENERAL_LEDGER.FieldByName('CO_GENERAL_LEDGER_NBR').AsString;
    tmp_Cl_Nbr := DM_CLIENT.CL.FieldByName('CL_NBR').AsString;
    tmp_Co_Nbr := DM_CLIENT.CO.FieldByName('CO_NBR').AsString;
    tmp_Custco_Nbr := DM_CLIENT.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
    try
      DM_CLIENT.CO_GENERAL_LEDGER.DisableControls;
      with DM_CLIENT.CO_GENERAL_LEDGER do
      begin
        First;
        while not Eof do
        begin
          if not wwdgGeneral_Ledger_Number.IsSelectedRecord then
          begin
            if CopyForm.wwcsCopySecond.Locate('CO_GENERAL_LEDGER_NBR', FieldByName('CO_GENERAL_LEDGER_NBR').AsString, []) then
              CopyForm.wwcsCopySecond.Delete;
          end;
          Next;
        end;
      end;
      CopyForm.CopyTo := 'GeneralLedger';
      CopyForm.ShowModal;

      ctx_DataAccess.OpenClient(StrToInt(tmp_Cl_Nbr));
      GetDataSetsToReopen(aDS, Close);
      SetDataSetsProps(aDS, DM_TEMPORARY.TMP_CO);
      DM_COMPANY.CO.DataRequired('CO_NBR='+tmp_Co_Nbr);
      DM_EMPLOYEE.EE.DataRequired('CO_NBR='+tmp_Co_Nbr);
      DM_COMPANY.CO_JOBS.DataRequired('CO_NBR='+tmp_Co_Nbr);
      DM_CLIENT.CO_GENERAL_LEDGER.DataRequired('CO_NBR='+tmp_Co_Nbr);
      DM_COMPANY.CO_DIVISION.DataRequired('CO_NBR='+tmp_Co_Nbr);
      DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR='+tmp_Co_Nbr);
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR='+tmp_Co_Nbr);
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR='+tmp_Co_Nbr);
      DM_COMPANY.CO_WORKERS_COMP.DataRequired('CO_NBR='+tmp_Co_Nbr);
      DM_COMPANY.CO_SUI.DataRequired('CO_NBR='+tmp_Co_Nbr);

      ctx_DataAccess.OpenDataSets(aDS);
      DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR',VarArrayOf([tmp_Cl_Nbr,tmp_Co_Nbr]) , []);
      DM_CLIENT.CL.Locate('CL_NBR', tmp_Cl_Nbr, []);
      DM_COMPANY.CO.Locate('CO_NBR',tmp_Co_Nbr,[]);
      DM_CLIENT.CO.Locate('CO_NBR',tmp_Co_Nbr,[]);
      wwdgGeneral_Ledger_Number.UnselectAll;
      DM_CLIENT.CO_GENERAL_LEDGER.Locate('CO_GENERAL_LEDGER_NBR', tmp_Co_GL_Nbr, []);
    finally
      DM_CLIENT.CO_GENERAL_LEDGER.EnableControls;
      CopyForm.Destroy;
    end;
  end;
end;

procedure TEDIT_CO_GENERAL_LEDGER.pnlCOBrowseLeftResize(Sender: TObject);
begin
  inherited;

end;

// This will override the DeleteMessage function, to allow for multi-deletion.

function TEDIT_CO_GENERAL_LEDGER.DeleteMessage: Word;
  var i: Integer;
  var Delete_Dataset: TevClientDataSet;
begin
  Result := mrCancel;

  Delete_Dataset := wwdgGeneral_Ledger_Number.DataSource.DataSet as TevClientDataSet;

  if wwdgGeneral_Ledger_Number.SelectedList.Count > 0 then
  begin
    if EvMessage('Are you sure you want to delete the selected record(s)?', mtConfirmation, mbOKCancel) = mrOK then
    begin
      if wwdgGeneral_Ledger_Number.SelectedList.Count = 1 then
        Result := mrOK
      else
      begin
        Delete_Dataset.DisableControls;

        try
          //Delete_Dataset.Edit;

          for i := 0 to wwdgGeneral_Ledger_Number.SelectedList.Count-1 do
          begin
            Delete_Dataset.GotoBookmark(wwdgGeneral_Ledger_Number.SelectedList[i]);
            Delete_Dataset.Delete;
          end;

          //Delete_Dataset.Post;
        finally
          Delete_Dataset.EnableControls;
        end;
      end;
    end;
  end;
end;

initialization
  Classes.RegisterClass(TEDIT_CO_GENERAL_LEDGER);

end.
