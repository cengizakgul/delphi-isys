// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_TEAM;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, StdCtrls, ExtCtrls,
  DBCtrls, wwdblook, Wwdotdot, Wwdbcomb, Mask, wwdbedit, Db, Wwdatsrc,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Controls, Classes, EvConsts,
  EvSecElement, Forms, SPD_EDIT_CO_DBDT_LOCALS, SPackageEntry, SysUtils, EvTypes,
  sCopyHelper, sDBDTCopyHelper, Variants, SDDClasses,
  ISBasicClasses, Dialogs, SDataDictclient, SDataDicttemp,sCompanyShared,
  EvContext, EvExceptions, Menus, ActnList,SPD_DBDTChoiceQuery, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, EvDataAccessComponents;

type
  TEDIT_CO_TEAM = class(TEDIT_CO_BASE)
    tshtGeneral_Info: TTabSheet;
    tshtBank_Accounts: TTabSheet;
    tshtDefaults: TTabSheet;
    Panel4: TevPanel;
    Panel5: TevPanel;
    wwdsSubMaster2: TevDataSource;
    wwdsSubMaster3: TevDataSource;
    pdsDetail: TevProxyDataSet;
    tbshLocals: TTabSheet;
    TDBDTLocals1: TDBDTLocals;
    tsVMR: TTabSheet;
    LocalCopyService: TevActionList;
    CopyService: TAction;
    pmLocalCopy: TevPopupMenu;
    MenuItem3: TMenuItem;
    evLabel5: TevLabel;
    edtAccountNumber: TevDBEdit;
    rgPrimary: TevDBRadioGroup;
    fpTeamRight: TisUIFashionPanel;
    sbTeamLocals: TScrollBox;
    ScrollBox1: TScrollBox;
    fpTeam: TisUIFashionPanel;
    fpDivisionContact: TisUIFashionPanel;
    fpDivisionOptions: TisUIFashionPanel;
    lablName: TevLabel;
    lablAddress1: TevLabel;
    lablAddress2: TevLabel;
    lablTeam_Code: TevLabel;
    lablCity: TevLabel;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    dedtName: TevDBEdit;
    dedtAddress1: TevDBEdit;
    dedtAddress2: TevDBEdit;
    dedtTeam_Code: TevDBEdit;
    dedtCity: TevDBEdit;
    wwdeState: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    EvBevel2: TEvBevel;
    qlblShowAddressOnCheck: TevLabel;
    wwcbPrint_Branch_Address_Check: TevDBComboBox;
    Label1: TevLabel;
    evLabel4: TevLabel;
    DBEdit1: TevDBEdit;
    evDBEdit1: TevDBEdit;
    lablPrimary_Contact: TevLabel;
    lablPhone1: TevLabel;
    lablDescription1: TevLabel;
    dedtPrimary_Contact: TevDBEdit;
    wwdePhone1: TevDBEdit;
    dedtDescription1: TevDBEdit;
    EvBevel1: TEvBevel;
    lablFax: TevLabel;
    lablFax_Description: TevLabel;
    dedtFax_Description: TevDBEdit;
    wwdeFax: TevDBEdit;
    lablE_Mail: TevLabel;
    dedtE_Mail: TevDBEdit;
    lablSecondary_Contact: TevLabel;
    lablPhone2: TevLabel;
    lablDescription2: TevLabel;
    dedtSecondary_Contact: TevDBEdit;
    wwdePhone2: TevDBEdit;
    dedtDescription2: TevDBEdit;
    evLabel6: TevLabel;
    evDBLookupCombo8: TevDBLookupCombo;
    sbTeamBanks: TScrollBox;
    fpDivisionBanks: TisUIFashionPanel;
    lablPayroll_Bank_Account: TevLabel;
    lablBilling_Bank_Account: TevLabel;
    lablTax_Bank_Account: TevLabel;
    lablDD_Bank_Account: TevLabel;
    wwlcPayroll_Bank_Account: TevDBLookupCombo;
    wwlcBilling_Bank_Account: TevDBLookupCombo;
    wwlcTax_Bank_Account: TevDBLookupCombo;
    wwlcDD_Bank_Account: TevDBLookupCombo;
    sbTeamDefaults: TScrollBox;
    fpDivisionDefaults: TisUIFashionPanel;
    fpDivisionNotes: TisUIFashionPanel;
    dmemTeam_Notes: TEvDBMemo;
    drgpHomeStateType: TevDBRadioGroup;
    lablHome_State: TevLabel;
    lablOverride_Pay_Rate: TevLabel;
    lablOverride_EE_Rate: TevLabel;
    wwlcHome_State: TevDBLookupCombo;
    dedtOverride_EE_Rate: TevDBEdit;
    wwdeOverride_Pay_Rate: TevDBEdit;
    evLabel3: TevLabel;
    evDBLookupCombo6: TevDBLookupCombo;
    Label6: TevLabel;
    wwlcWorkers_Comp_Number: TevDBLookupCombo;
    wwDBLookupCombo1: TevDBLookupCombo;
    wwDBLookupCombo4: TevDBLookupCombo;
    sbTeamMailRoom: TScrollBox;
    fpMailRoom: TisUIFashionPanel;
    evLabel20: TevLabel;
    evLabel21: TevLabel;
    evLabel22: TevLabel;
    evLabel25: TevLabel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBLookupCombo4: TevDBLookupCombo;
    evDBLookupCombo7: TevDBLookupCombo;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo5: TevDBLookupCombo;
    lablTEAM_NAME: TevLabel;
    Label4: TevLabel;
    Label3: TevLabel;
    Label2: TevLabel;
    DBText4: TevDBText;
    DBText3: TevDBText;
    DBText2: TevDBText;
    DBText10: TevDBText;
    Splitter4: TevSplitter;
    wwDBGrid1: TevDBGrid;
    wwDBGrid2: TevDBGrid;
    Splitter5: TevSplitter;
    wwDBGrid4: TevDBGrid;
    wwDBGrid3: TevDBGrid;
    Splitter6: TevSplitter;
    CopyHelper: TDBDTCopyHelper;
    procedure pdsDetailNewRecord(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure ResultFIELDS_CL_BANK_Function;
    procedure CopyServiceExecute(Sender: TObject);
    procedure LocalTaxCopy;
    procedure CopyHelperCopyaddressto1Click(Sender: TObject);
    procedure edtAccountNumberChange(Sender: TObject);
    procedure fpTeamRightResize(Sender: TObject);
  private
    procedure HandleCopy(Sender: TObject; parent_nbr: integer;
      selectedDetails, details: TEvClientDataSet; params: TStringList);
    procedure HandleDelete(Sender: TObject; parent_nbr: integer;
      selectedDetails, details: TEvClientDataSet; params: TStringList);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    function GetDataSetConditions(sName: string): string; override;
  public
    GL_Setuped:boolean;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.DFM}
uses
  sCopier, EvUtils;

procedure TEDIT_CO_TEAM.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP, aDS);
  AddDS(DM_CLIENT.CL_TIMECLOCK_IMPORTS, aDS);
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_CLIENT.CL_BILLING, aDS);
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  AddDS(DM_COMPANY.CO_TEAM_LOCALS, aDS);
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);

  inherited;
end;

procedure TEDIT_CO_TEAM.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
  if ( wwdsList.DataSet.Active )and (wwdsList.DataSet.RecordCount <= 0) then exit;
  if Kind in [NavOk] then
  begin
    wwDBGrid1.Enabled := not Value;
    wwDBGrid2.Enabled := not Value;
    wwDBGrid3.Enabled := not Value;
    wwDBGrid4.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavNext.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavPrevious.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavLast.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavFirst.Enabled := not Value;
  end;
end;

function TEDIT_CO_TEAM.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_TEAM;
end;

function TEDIT_CO_TEAM.GetInsertControl: TWinControl;
begin
  Result := dedtTeam_Code;
end;

procedure TEDIT_CO_TEAM.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_DIVISION, SupportDataSets);
  AddDS(DM_COMPANY.CO_BRANCH, SupportDataSets);
  AddDS(DM_COMPANY.CO_DEPARTMENT, SupportDataSets);
  AddDS(DM_COMPANY.CO_TEAM_LOCALS, EditDataSets);

end;

function TEDIT_CO_TEAM.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_BRANCH') or (sName = 'CO_DEPARTMENT') or (sName = 'CO_TEAM') or (sName = 'CO_TEAM_LOCALS') then //gdy
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_TEAM.Activate;
var
  dummy: boolean;
begin
  inherited;
  GL_Setuped := false;
  wwdsSubMaster2.DataSet := DM_COMPANY.CO_BRANCH;
  wwdsSubMaster3.DataSet := DM_COMPANY.CO_DEPARTMENT;
  wwdsDetail.DataSet := DM_COMPANY.CO_TEAM;

  CopyHelper.Grid := wwDBGrid4;
  CopyHelper.OnCopyDetail := HandleCopy;
  CopyHelper.OnDeleteDetail := HandleDelete;
  CopyHelper.CopyAddressEnabled := True;
  CopyHelper.Copyaddressto1.OnClick := CopyHelperCopyaddressto1Click;
  GetDataSetsToReopen( CopyHelper.FDSToSave,  dummy );

  PageControl1.ActivePageIndex := 0;
  tsVMR.TabVisible := tsVMR.TabVisible and CheckVmrLicenseActive;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CO_TEAM.pdsDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  with DM_COMPANY.CO do
    if Active then
    begin
      DataSet.FindField('HOME_CO_STATES_NBR').AsInteger :=
        FindField('HOME_CO_STATES_NBR').AsInteger;
      DataSet.FindField('PAY_FREQUENCY_HOURLY').AsString :=
        FindField('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
      DataSet.FindField('PAY_FREQUENCY_SALARY').AsString :=
        FindField('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
    end;
end;

procedure TEDIT_CO_TEAM.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
 V: Variant;
 tmpDataSet: TevClientDataSet;
begin
  case Kind of
    NavOk:
    begin
      tmpDataSet := TevClientDataSet.Create(nil);
      try
        with TExecDSWrapper.Create('SelectDBDTWithName') do
        begin
          SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
          ctx_DataAccess.GetCustomData(tmpDataSet, AsVariant);
          ctx_DataAccess.OpenDataSets([tmpDataSet]);
          V := tmpDataSet.Lookup('TNAME',dedtName.Text,'CUSTOM_TEAM_NUMBER');
        end;
        if (not VarIsNull(V)) and (V <> DM_COMPANY.CO_TEAM['CUSTOM_TEAM_NUMBER']) then
           EvMessage('Team name has been used by other Team, it could cause inaccurate reporting.', mtWarning, [mbOK]);
      finally
        tmpDataSet.free;
      end;

      if DM_COMPANY.CO_TEAM.FieldByName('HOME_CO_STATES_NBR').AsInteger = 0 then
        raise EUpdateError.CreateHelp('You must select a home state!', IDH_ConsistencyViolation);
      if (DM_COMPANY.CO_TEAM.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_OVERRIDE) and
         ((not DM_COMPANY.CO_TEAM.FieldByName('OVERRIDE_PAY_RATE').IsNull) or (wwdeOverride_Pay_Rate.Text <> '')) then
        EvMessage('Auto Labor Distribution will NOT use this override!!!', mtWarning, [mbOK]);
      if GL_Setuped and not CheckGlSetuped(DM_CLIENT.CO_TEAM,['GENERAL_LEDGER_TAG']) then
           EvMessage(sDontForgetSetupGL);
      GL_Setuped := false;
    end;
    NavInsert:
    begin
      lablName.Caption := '~' + lablName.Caption;
      DM_CLIENT.CO_TEAM.FieldByName('NAME').Required := true;
      GL_Setuped := CheckGlSetupExist(DM_CLIENT.CO_TEAM,['GENERAL_LEDGER_TAG'],false);
    end
  end;

  if (Kind in [NavOK,NavCommit]) and
      (trim(edtAccountNumber.Text) <> '') then
         if (Trim(rgPrimary.Value) = '') then
           raise EUpdateError.CreateHelp('Please select Primary!', IDH_ConsistencyViolation);

  inherited;
end;

procedure TEDIT_CO_TEAM.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavOk, NavCancel:
    begin
      lablName.Caption := StringReplace( lablName.Caption, '~','', [rfReplaceAll] );
      DM_CLIENT.CO_TEAM.FieldByName('NAME').Required := false;
    end;
  end;
end;


procedure TEDIT_CO_TEAM.HandleCopy(Sender: TObject; parent_nbr: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList);
var
  k: integer;
  fn: string;
begin
  if details.Locate('CUSTOM_TEAM_NUMBER', VarArrayOf([selecteddetails.FieldByNAme('CUSTOM_TEAM_NUMBER').AsString]), []) then
    details.Edit
  else
    details.Insert;
  for k := 0 to SelectedDetails.Fields.Count - 1 do
  begin
    fn := Selecteddetails.Fields[k].FieldName;
    if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) then
      details[fn] := selectedDetails[fn];
  end;
  details['CO_DEPARTMENT_NBR'] := parent_nbr;
  details.Post;
end;

procedure TEDIT_CO_TEAM.HandleDelete(Sender: TObject; parent_nbr: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList);
begin
  if details.Locate('CUSTOM_TEAM_NUMBER', VarArrayOf([selecteddetails.FieldByNAme('CUSTOM_TEAM_NUMBER').AsString]), []) then
    details.Delete;
end;

procedure TEDIT_CO_TEAM.AfterDataSetsReopen;
begin
  inherited;
  tsVMR.TabVisible := CheckVmrLicenseActive and CheckVmrSetupActive;
  AssignVMRClientLookups(tsVMR);
end;

procedure TEDIT_CO_TEAM.PageControl1Change(Sender: TObject);
begin
  inherited;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CO_TEAM.ResultFIELDS_CL_BANK_Function;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_CL_BANK_') <> stEnabled then
   Begin
    wwlcPayroll_Bank_Account.Enabled := false;
    wwlcBilling_Bank_Account.Enabled := False;
    wwlcTax_Bank_Account.Enabled := false;
    wwlcDD_Bank_Account.Enabled := false;
   end;
End;

procedure TEDIT_CO_TEAM.CopyServiceExecute(Sender: TObject);
begin
  inherited;
   LocalTaxCopy;
end;

procedure TEDIT_CO_TEAM.LocalTaxCopy;
var
  CurrentLocalNbr, CurrentNbr, CurrentLocalTaxNbr: integer;

  procedure DoCopyLocals;
  var
    k: integer;
  begin
    with TDBDTChoiceQuery.Create( Self ) do
    try
      SetFilter(CLIENT_LEVEL_TEAM, 'TEAM_NBR <>  ' + DM_COMPANY.CO_TEAM_LOCALS.FieldByName('CO_TEAM_NBR').AsString );
      Caption := 'Select the Teams you want this Local Tax copied to';
      ConfirmAllMessage := 'This operation will copy the Local Tax to all Teams. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
            DM_COMPANY.CO_TEAM_LOCALS.DataRequired('ALL');
            if dgChoiceList.SelectedList.Count > 0 then
            begin
              DM_COMPANY.CO_TEAM_LOCALS.DisableControls;
              try
                for k := 0 to dgChoiceList.SelectedList.Count - 1 do
                begin
                  cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                  if not DM_COMPANY.CO_TEAM_LOCALS.Locate('CO_TEAM_NBR;CO_LOCAL_TAX_NBR',
                       VarArrayOf([cdsChoiceList.FieldByName('TEAM_NBR').AsInteger,CurrentLocalTaxNbr]), []) then
                  begin
                    DM_COMPANY.CO_TEAM_LOCALS.Insert;
                    DM_COMPANY.CO_TEAM_LOCALS['CO_TEAM_NBR'] := cdsChoiceList.FieldByName('TEAM_NBR').AsInteger;
                    DM_COMPANY.CO_TEAM_LOCALS['CO_LOCAL_TAX_NBR'] := CurrentLocalTaxNbr;
                    DM_COMPANY.CO_TEAM_LOCALS.Post;
                  end;
                end;
              finally
                ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TEAM_LOCALS]);
                DM_COMPANY.CO_TEAM_LOCALS.EnableControls;
              end;
             end;
      end;
    finally
      Free;
    end;
  end;
begin
  CurrentNbr := DM_COMPANY.CO_TEAM_LOCALS.FieldByName('CO_TEAM_NBR').AsInteger;
  CurrentLocalNbr := DM_COMPANY.CO_TEAM_LOCALS.FieldByName('CO_TEAM_LOCALS_NBR').AsInteger;
  CurrentLocalTaxNbr := DM_COMPANY.CO_TEAM_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
  if (CurrentLocalNbr > 0) and  (CurrentLocalTaxNbr > 0 )then
  begin
    try
      DoCopyLocals;
    finally
      DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR',CurrentNbr,[]);
      DM_COMPANY.CO_TEAM_LOCALS.Locate('CO_TEAM_LOCALS_NBR',CurrentLocalNbr,[]);
    end;
  end;
end;


procedure TEDIT_CO_TEAM.CopyHelperCopyaddressto1Click(Sender: TObject);
var
  CurrentNbr, CurrentParentNbr : integer;
  CurrentAddr1,Currentaddr2,CurrentCity,CurrentState,CurrentZip: string;
  procedure DoCopyAddress;
  var
    k: integer;
  begin
    with TDBDTChoiceQuery.Create( Self ) do
    try
      SetFilter(CLIENT_LEVEL_TEAM, 'Team_NBR <>  ' + DM_COMPANY.CO_Team.FieldByName('CO_Team_NBR').AsString, false );
      Caption := 'Select the Teams you want the Address copied to';
      ConfirmAllMessage := 'This operation will copy the Address to all Teams. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        for k := 0 to dgChoiceList.SelectedList.Count - 1 do
        begin
          cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
          DM_COMPANY.CO_Team.DataRequired('CO_Team_NBR = ' + cdsChoiceList.FieldByName('Team_NBR').AsString);
          if DM_COMPANY.CO_Team.RecordCount > 0 then
          begin
            DM_COMPANY.CO_Team.Edit;
            DM_COMPANY.CO_Team['ADDRESS1'] := CurrentAddr1;
            DM_COMPANY.CO_Team['ADDRESS2'] := Currentaddr2;
            DM_COMPANY.CO_Team['CITY']     := CurrentCity;
            DM_COMPANY.CO_Team['STATE']    := CurrentState;
            DM_COMPANY.CO_Team['ZIP_CODE'] := CurrentZip;
            DM_COMPANY.CO_Team.Post;
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_Team]);
          end;
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  CurrentNbr   := DM_COMPANY.CO_Team.FieldByName('CO_Team_NBR').AsInteger;
  CurrentParentNbr := DM_COMPANY.CO_Team.FieldByName('CO_Department_NBR').AsInteger;
  CurrentAddr1 := DM_COMPANY.CO_Team.FieldByName('ADDRESS1').AsString;
  Currentaddr2 := DM_COMPANY.CO_Team.FieldByName('ADDRESS2').AsString;
  CurrentCity  := DM_COMPANY.CO_Team.FieldByName('CITY').AsString;
  CurrentState := DM_COMPANY.CO_Team.FieldByName('STATE').AsString;
  CurrentZip   := DM_COMPANY.CO_Team.FieldByName('ZIP_CODE').AsString;
  try
    DM_COMPANY.CO_Team.DisableControls;
    wwDBGrid4.DataSource.DataSet.DisableControls;
    DoCopyAddress;
  finally
    DM_COMPANY.CO_Team.DataRequired('ALL');
    DM_COMPANY.CO_Department.Locate('CO_Department_NBR',CurrentParentNbr,[]);
    DM_COMPANY.CO_Team.Locate('CO_Team_NBR',CurrentNbr,[]);
    DM_COMPANY.CO_Team.EnableControls;
    wwDBGrid4.DataSource.DataSet.enableControls;
  end;
end;

procedure TEDIT_CO_TEAM.edtAccountNumberChange(Sender: TObject);
begin
  inherited;
  if Trim(edtAccountNumber.Text) <> '' then
      rgPrimary.Caption := '~Primary  '
  else
      rgPrimary.Caption := 'Primary ';
end;

constructor TEDIT_CO_TEAM.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENT
  pnlSubbrowse.Parent := fpTeamRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_TEAM.fpTeamRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpTeamRight.Width - 40;
  pnlSubbrowse.Height := fpTeamRight.Height - 65;
end;

initialization
  RegisterClass(TEDIT_CO_TEAM);

end.
