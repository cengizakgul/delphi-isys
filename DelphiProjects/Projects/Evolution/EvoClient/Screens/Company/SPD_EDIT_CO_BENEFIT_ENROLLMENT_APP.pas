// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BENEFIT_ENROLLMENT_APP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBLookupCombo,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, Mask, wwdbedit,
  isUIwwDBEdit, Wwdotdot, Wwdbcomb, isUIwwDBComboBox, EvContext, EvCommonInterfaces, isBaseClasses,
  SReportSettings, EvStreamUtils, EvSendMail, EvDataset, isBasicUtils, EvEEChangeRequest,
  Variants, EvClasses, EvExceptions, EvUIUtils, EvConsts, SPackageEntry;

type
  TEDIT_CO_BENEFIT_ENROLLMENT_APP= class(TEDIT_CO_BASE)
    tshtOpprovalDetail: TTabSheet;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    fpCOPensionSummary: TisUIFashionPanel;
    fpApprovalButtons: TisUIFashionPanel;
    sbPensions: TScrollBox;
    evdsComplete: TevDataSource;
    evpgApprove: TevPageControl;
    tsComplete: TTabSheet;
    tsProgress: TTabSheet;
    tsNoElections: TTabSheet;
    wwdgCompleted: TevDBCheckGrid;
    wwdgProgress: TevDBCheckGrid;
    wwdgNoElection: TevDBCheckGrid;
    btnApprove: TevButton;
    btnClose: TevButton;
    btnPending: TevButton;
    btnEmail: TevButton;
    evdsInProgress: TevDataSource;
    evdsNoElection: TevDataSource;
    btnPrint: TevButton;
    emailType: TevCheckBox;
    procedure evpgApproveChange(Sender: TObject);
    function GetSummaryDataset(const s:string): IevDataset;
    procedure PageControl1Change(Sender: TObject);
    procedure btnApproveClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnPendingClick(Sender: TObject);
    procedure btnEmailClick(Sender: TObject);
    procedure SetGridDataset(const grd:TevDBCheckGrid;var ds: IevDataset;const sType:string);
    procedure CloseSelected(const grd:TevDBCheckGrid;var ds: TevDataSource);
    procedure EmailSelected(const grd:TevDBCheckGrid;var ds: TevDataSource);
    procedure btnPrintClick(Sender: TObject);
  private
    FCompleted: IevDataset;
    FProgress: IevDataset;
    FNoElection: IevDataset;
    FEnableHr: boolean;
  protected
    function GetIfReadOnly: Boolean; override;
    procedure AfterDataSetsReopen; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
      var InsertDataSets, EditDataSets: TArrayDS;
      var DeleteDataSet: TevClientDataSet;
      var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure DeActivate; override;
  end;

implementation

{$R *.DFM}

function TEDIT_CO_BENEFIT_ENROLLMENT_APP.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if (not Result) then
     result:= FEnableHr;
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.Activate;
begin
  inherited;
  fpEDIT_OPEN_BASE_LEFT.Align := alClient;
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.Deactivate;
begin
  inherited;
  DM_EMPLOYEE.EE_BENEFITS.Close;
  DM_EMPLOYEE.EE_BENEFICIARY.Close;
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.Close;
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_COMPANY.EE, aDS);
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.AfterDataSetsReopen;
begin
  inherited;
  (Owner as TFramePackageTmpl).SetButton(NavInsert, false);
  (Owner as TFramePackageTmpl).SetButton(NavDelete, false);
  (Owner as TFramePackageTmpl).SetButton(NavHistory, false);

  FEnableHr := GetEnableHrBenefit(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  if GetIfReadOnly then
     SetReadOnly(True)
  else
    ClearReadOnly;
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_COMPANY.EE, SupportDataSets, 'ALL');
end;



function TEDIT_CO_BENEFIT_ENROLLMENT_APP.GetSummaryDataset(const s:string): IevDataset;
var
   EENbrList: IisStringList;
   sl : TStringList;
begin
  EENbrList := TisStringList.Create;
  if not DM_COMPANY.EE.Active then
     DM_COMPANY.EE.DataRequired('CO_NBR = ' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  sl := DM_COMPANY.EE.GetFieldValueList('EE_NBR',true);
  try
    if sl.Count > 0 then
       EENbrList.CommaText := sl.CommaText;
  finally
     FreeandNil(sl);
  end;

  Result:= Context.RemoteMiscRoutines.GetSummaryBenefitEnrollmentStatus(EENbrList, s);
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.SetGridDataset(const grd:TevDBCheckGrid;var ds: IevDataset;const sType:string);
begin
    ds:= GetSummaryDataset(sType);
    grd.DataSource.DataSet := ds.vclDataSet;
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.evpgApproveChange(
  Sender: TObject);
begin
  inherited;

  if not GetIfReadOnly then
  begin
    btnApprove.SecurityRO := (evpgApprove.ActivePage <> tsComplete) or
                             (ctx_AccountRights.Functions.GetState('USER_CAN_APPROVE_BENEFITS_ENROLLMENT') <> stEnabled);
    btnPending.SecurityRO := evpgApprove.ActivePage <> tsComplete;
    btnPrint.SecurityRO := evpgApprove.ActivePage <> tsComplete;
    btnClose.SecurityRO := evpgApprove.ActivePage = tsComplete;
  end;

  if evpgApprove.ActivePage = tsComplete then
     SetGridDataset(wwdgCompleted, FCompleted, 'C')
  else
  if evpgApprove.ActivePage = tsProgress then
     SetGridDataset(wwdgProgress, FProgress, 'P')
  else
  if evpgApprove.ActivePage = tsNoElections then
     SetGridDataset(wwdgNoElection, FNoElection, 'N');
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.PageControl1Change(
  Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = tshtOpprovalDetail then
     evpgApproveChange(Sender);
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.btnApproveClick(Sender: TObject);
var
  i: integer;
begin
  inherited;
  if wwdgCompleted.SelectedList.Count > 0 then
  begin
    for i := 0 to wwdgCompleted.SelectedList.Count - 1 do
    begin
      evdsComplete.DataSet.GotoBookmark(wwdgCompleted.SelectedList.Items[i]);
      if evdsComplete.DataSet.FieldByName('EE_CHANGE_REQUEST_NBR').asinteger > 0 then
         Context.RemoteMiscRoutines.ApproveBenefitRequest(evdsComplete.DataSet.FieldByName('EE_CHANGE_REQUEST_NBR').asinteger);
    end;
    SetGridDataset(wwdgCompleted, FCompleted, 'C');
  end;
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.btnPendingClick(Sender: TObject);
var
  i: integer;
begin
  inherited;
  if wwdgCompleted.SelectedList.Count > 0 then
  begin
    for i := 0 to wwdgCompleted.SelectedList.Count - 1 do
    begin
      evdsComplete.DataSet.GotoBookmark(wwdgCompleted.SelectedList.Items[i]);
      if evdsComplete.DataSet.FieldByName('EE_CHANGE_REQUEST_NBR').asinteger > 0 then
         Context.RemoteMiscRoutines.RevertBenefitRequest(evdsComplete.DataSet.FieldByName('EE_CHANGE_REQUEST_NBR').asinteger);
    end;
    SetGridDataset(wwdgCompleted, FCompleted, 'C');
  end;
end;


procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.CloseSelected(const grd:TevDBCheckGrid;var ds: TevDataSource);
var
  i: integer;
begin
  inherited;
  for i := 0 to grd.SelectedList.Count - 1 do
  begin
    ds.DataSet.GotoBookmark(grd.SelectedList.Items[i]);
    if ds.DataSet.FieldByName('EE_NBR').asinteger > 0 then
       Context.RemoteMiscRoutines.closeBenefitEnrollment(
               ds.DataSet.FieldByName('EE_NBR').AsInteger,
                ds.DataSet.FieldByName('CATEGORY_TYPE').AsString,
                 ds.DataSet.FieldByName('EE_CHANGE_REQUEST_NBR').AsInteger);
  end;
end;


procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.btnCloseClick(Sender: TObject);
begin
  inherited;
  if evpgApprove.ActivePage = tsProgress then
  begin
     CloseSelected(wwdgProgress, evdsInProgress);
     SetGridDataset(wwdgProgress, FProgress, 'P');
  end
  else
  if evpgApprove.ActivePage = tsNoElections then
  begin
     CloseSelected(wwdgNoElection, evdsNoElection);
     SetGridDataset(wwdgNoElection, FNoElection, 'N');
  end;
end;

procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.btnPrintClick(Sender: TObject);
var
  ReportResults: TrwReportResults;
  r: TrwReportResults;
  ReportResult: TrwReportResult;
  Report: IisStream;
  i: integer;
begin
  inherited;

  ReportResults := TrwReportResults.Create;
  try
    for i := 0 to wwdgCompleted.SelectedList.Count - 1 do
    begin
      evdsComplete.DataSet.GotoBookmark(wwdgCompleted.SelectedList.Items[i]);
      if evdsComplete.DataSet.FieldByName('EE_CHANGE_REQUEST_NBR').asinteger > 0 then
      begin
        Report := Context.RemoteMiscRoutines.GetBenefitRequestConfirmationReport(evdsComplete.DataSet.FieldByName('EE_CHANGE_REQUEST_NBR').asinteger, false);
        ReportResult := ReportResults.Add;
        r := TrwReportResults.Create;
        try
            r.SetFromStream(Report);
            Assert(r.Count > 0);
            ReportResult.Assign(r[0]);
            ReportResult.ReportName := evdsComplete.DataSet.FieldByName('FIRST_NAME').AsString + ' ' + evdsComplete.DataSet.FieldByName('LAST_NAME').AsString;
        finally
          r.Free;
        end;
      end;
    end;
  finally
    ctx_RWLocalEngine.Preview(ReportResults);
    ReportResults.Free;
  end;

end;


procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.EmailSelected(const grd:TevDBCheckGrid;var ds: TevDataSource);
var
  EENbrList: IisStringList;
  i: integer;
  EM: TStringList;
  body:String;
  Q: IEvQuery;
  tmpEENbr: integer;
begin
  tmpEENbr := -1;
  Q := TevQuery.Create('select CUSTOM_EMPLOYEE_NUMBER, FIRST_NAME, LAST_NAME, CUSTOM_COMPANY_NUMBER, A.CO_NBR, ' +
    ' a.e_mail_address as EE_MAIL, b.e_mail_address as CL_PERSON_MAIL, a.BENEFIT_E_MAIL_ADDRESS ' +
    ' from EE a, cl_person b, co c where a.co_nbr=c.co_nbr and a.cl_person_nbr=b.cl_person_nbr and #CONDITION' +
    ' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} ');


  if emailType.Checked then
  begin
      EENbrList := TisStringList.Create;
      for i := 0 to grd.SelectedList.Count - 1 do
      begin
        ds.DataSet.GotoBookmark(grd.SelectedList.Items[i]);
        EENbrList.Add(ds.DataSet.FieldByName('EE_NBR').AsString);
      end;

      if EENbrList.Count > 0 then
      begin
        Q.Macros.AddValue('CONDITION', BuildINsqlStatement('EE_NBR', EENbrList.CommaText));
        Q.Execute;

        if Assigned(Q.Result) then
        begin
          EM := TStringList.Create;
          try
            Q.result.First;
            while not Q.result.Eof do
            begin
              if Q.result.FieldByName('BENEFIT_E_MAIL_ADDRESS').AsString <> '' then
                EM.Add(Q.result.FieldByName('BENEFIT_E_MAIL_ADDRESS').AsString)
              else if Q.result.FieldByName('EE_MAIL').AsString <> '' then
                EM.Add(Q.result.FieldByName('EE_MAIL').AsString)
              else if Q.result.FieldByName('CL_PERSON_MAIL').AsString <> '' then
                EM.Add(Q.result.FieldByName('CL_PERSON_MAIL').AsString);

              Q.result.Next;
            end;
          finally
            if EM.Count > 0 then
               MailTo(EM, nil, nil, nil, '', '');

            EM.Free;
          end;
        end;
      end;
  end
  else
    for i := 0 to grd.SelectedList.Count - 1 do
    begin
      ds.DataSet.GotoBookmark(grd.SelectedList.Items[i]);
      if ds.DataSet.FieldByName('EE_NBR').asinteger <> tmpEENbr then
      begin
        tmpEENbr := ds.DataSet.FieldByName('EE_NBR').AsInteger;
        Q.Macros.AddValue('CONDITION', 'EE_NBR = ' + ds.DataSet.FieldByName('EE_NBR').AsString);
        Q.Execute;
      end;

      body := #13#10#13#10#13#10+
              '  Benefit Enrollment Information'+#13#10+
              '--------------------------------------------'+#13#10+
              'Category:'+ds.DataSet.FieldByName('CATEGORY_TYPE_DESC').AsString;

      if Assigned(Q.Result) then
      begin
          EM := TStringList.Create;
          try
            if Q.result.FieldByName('BENEFIT_E_MAIL_ADDRESS').AsString <> '' then
              EM.Add(Q.result.FieldByName('BENEFIT_E_MAIL_ADDRESS').AsString)
            else if Q.result.FieldByName('EE_MAIL').AsString <> '' then
              EM.Add(Q.result.FieldByName('EE_MAIL').AsString)
            else if Q.result.FieldByName('CL_PERSON_MAIL').AsString <> '' then
              EM.Add(Q.result.FieldByName('CL_PERSON_MAIL').AsString);

          finally
            if EM.Count > 0 then
               MailTo(EM, nil, nil, nil, '', body);

            EM.Free;
          end;
      end;
    end;
end;


procedure TEDIT_CO_BENEFIT_ENROLLMENT_APP.btnEmailClick(Sender: TObject);
begin
  inherited;

  if evpgApprove.ActivePage = tsComplete then
     EmailSelected(wwdgCompleted, evdsComplete)
  else
  if evpgApprove.ActivePage = tsProgress then
     EmailSelected(wwdgProgress, evdsInProgress)
  else
  if evpgApprove.ActivePage = tsNoElections then
     EmailSelected(wwdgNoElection, evdsNoElection);

end;


initialization
  RegisterClass(TEDIT_CO_BENEFIT_ENROLLMENT_APP);

end.

