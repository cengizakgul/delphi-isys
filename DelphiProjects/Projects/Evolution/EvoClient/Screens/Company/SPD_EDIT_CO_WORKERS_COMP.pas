// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_WORKERS_COMP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, DBCtrls, StdCtrls, Buttons, Grids, Variants,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, Mask, wwdblook, wwdbedit, 
  SDataStructure,  sCompanyCopyHelper, sCopyHelper, evConsts,
  SDDClasses, ISBasicClasses, SDataDictclient, SDataDicttemp,sCompanyShared,SPackageEntry,EvUtils, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_WORKERS_COMP = class(TEDIT_CO_BASE)
    Detail: TTabSheet;
    wwDBGrid1: TevDBGrid;
    wwDBGrid2: TevDBGrid;
    CopyHelper: TCompanyCopyHelper;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    pnlFPCORightBody: TevPanel;
    fpCOWorkerSummary: TisUIFashionPanel;
    fpCOWorkerDetails: TisUIFashionPanel;
    Label1: TevLabel;
    DBEdit1: TevDBEdit;
    DBEdit4: TevDBEdit;
    Label4: TevLabel;
    Label3: TevLabel;
    evLabel10: TevLabel;
    evDBEdit5: TevDBEdit;
    wwDBLookupCombo1: TevDBLookupCombo;
    Label6: TevLabel;
    wwdeRate: TevDBEdit;
    evDBEdit2: TevDBEdit;
    evLabel27: TevLabel;
    Label7: TevLabel;
    wwdeExperience_Rating: TevDBEdit;
    Label8: TevLabel;
    wwDBLookupCombo2: TevDBLookupCombo;
    DBRadioGroup3: TevDBRadioGroup;

  private
    procedure HandleCopyWC( Sender: TObject; client, company: integer; selectedDetails, details: TEvClientDataSet );
    procedure HandleDeleteWC( Sender: TObject; client, company: integer; selectedDetails, details: TEvClientDataSet );
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    GL_Setuped:boolean;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;

  end;

implementation

{$R *.DFM}
uses
  sCopier;


procedure TEDIT_CO_WORKERS_COMP.Activate;
var
  dummy: boolean;
begin
  inherited;
  CopyHelper.Grid := wwDBGrid2;
  CopyHelper.UserFriendlyName := 'Workers Compensation';
  CopyHelper.OnCopyDetail := HandleCopyWC;
  CopyHelper.OnDeleteDetail := HandleDeleteWC;
  GetDataSetsToReopen( CopyHelper.FDSToSave,  dummy );
end;

procedure TEDIT_CO_WORKERS_COMP.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  inherited;
  AddDS(DM_COMPANY.CO_STATES, aDS);
end;

function TEDIT_CO_WORKERS_COMP.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_WORKERS_COMP;
end;

function TEDIT_CO_WORKERS_COMP.GetInsertControl: TWinControl;
begin
  Result := DBEdit1;
end;


procedure TEDIT_CO_WORKERS_COMP.HandleCopyWC(Sender: TObject; client, company: integer;
  selectedDetails, details: TEvClientDataSet);
var
  msg: string;
  state: integer;
  v: Variant;
  k: integer;
  fn: string;
begin
  DM_COMPANY.CO_STATES.EnsureDSCondition(details.RetrieveCondition  );
  v := DM_COMPANY.CO_STATES.Lookup( 'SY_STATES_NBR', selectedDetails['SyState'], 'CO_STATES_NBR');
  if not VarIsNull( v ) then
  begin
    state := v;
    DM_CLIENT.CL_E_D_GROUPS.EnsureDSCondition('');
    v := DM_CLIENT.CL_E_D_GROUPS.Lookup( 'NAME', selectedDetails['EDGroupName'], 'CL_E_D_GROUPS_NBR');

    if details.Locate('WORKERS_COMP_CODE;CO_STATES_NBR', VarArrayOf([selecteddetails.FieldByNAme('WORKERS_COMP_CODE').AsString, state]), []) then
      details.Edit
    else
      details.Insert;
    for k := 0 to SelectedDetails.Fields.Count - 1 do
    begin
      fn := Selecteddetails.Fields[k].FieldName;
      if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) and ( fn <> 'CO_STATES_NBR' ) and ( fn <> 'CL_E_D_GROUPS_NBR' ) then
        details[fn] := selectedDetails[fn];
    end;
    details['CO_NBR'] := company;
    details['CO_STATES_NBR'] := state;
    if not VarIsNull( v ) then
      details['CL_E_D_GROUPS_NBR'] := v;
    details.Post;
  end
  else
  begin
    msg := Format( 'Cannot copy %s to company %s because state %s doesn''t exist',
           [ VarToStr(selectedDetails['WORKERS_COMP_CODE']),
             (Sender as TCompanyDetailCopier).TargetCompanyNumber,
             VarToStr( selectedDetails['Co_State_Lookup'] ) ] );
    EvErrMessage(msg);
  end;
end;

procedure TEDIT_CO_WORKERS_COMP.HandleDeleteWC(Sender: TObject; client, company: integer;
  selectedDetails, details: TEvClientDataSet);
begin
  if details.Locate('WORKERS_COMP_CODE', VarArrayOf([selecteddetails.FieldByNAme('WORKERS_COMP_CODE').AsString]), []) then
    details.Delete;
end;


procedure TEDIT_CO_WORKERS_COMP.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  case Kind of
    NavInsert:
    begin
      GL_Setuped := CheckGlSetupExist(DM_CLIENT.CO_WORKERS_COMP,['GL_TAG','GL_OFFSET_TAG'],false);
    end;
    NavOk:
    begin
      if GL_Setuped and not CheckGlSetuped(DM_CLIENT.CO_WORKERS_COMP,['GL_TAG','GL_OFFSET_TAG']) then
           EvMessage(sDontForgetSetupGL);
      GL_Setuped := false;
     end;
  end;
  inherited;
end;

initialization
  RegisterClass(TEDIT_CO_WORKERS_COMP);
end.
