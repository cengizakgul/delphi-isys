// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDP_IM_HRTrax;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFrameEntry, Db, Wwdatsrc,  StdCtrls, EEIFuncs,
  ISBasicClasses, EvUIComponents, LMDCustomButton, LMDButton, isUILMDButton;

type
  TIM_HRTrax = class(TFrameEntry)
    evButton1: TevButton;
    OpenDialog1: TOpenDialog;
    procedure evButton1Click(Sender: TObject);
  private
    { Private declarations }
  end;

implementation

{$R *.DFM}

{ TIM_HRTrax }

procedure TIM_HRTrax.evButton1Click(Sender: TObject);
begin
  inherited;
  if OpenDialog1.Execute then
    ImportFromFile(OpenDialog1.FileName);
end;

initialization
  RegisterClass(TIM_HRTrax);

end.

