inherited EDIT_CO_TAX_LIABILITIES_BASE: TEDIT_CO_TAX_LIABILITIES_BASE
  inherited Panel1: TevPanel
    object Label22: TevLabel [0]
      Left = 539
      Top = 17
      Width = 65
      Height = 13
      Caption = 'Company EIN'
    end
    object DBText3: TevDBText [1]
      Left = 611
      Top = 17
      Width = 145
      Height = 17
      DataField = 'FEIN'
      DataSource = wwdsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited pnlTopLeft: TevPanel
      inherited CompanyNameText: TevDBText
        Width = 289
      end
      inherited pnlUser: TevPanel
        Left = 410
      end
    end
  end
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited Panel3: TevPanel
            object cbGroup: TevCheckBox
              Left = 280
              Top = 8
              Width = 97
              Height = 17
              Caption = 'Group by payroll'
              Checked = True
              State = cbChecked
              TabOrder = 2
              OnClick = cbGroupClick
            end
            object cbShowCredit: TevCheckBox
              Left = 408
              Top = 8
              Width = 129
              Height = 17
              Caption = 'Show credit liabilities'
              TabOrder = 1
              OnClick = cbGroupClick
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 320
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 457
                IniAttributes.SectionName = 'TEDIT_CO_TAX_LIABILITIES_BASE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 48
              Align = alClient
              Visible = True
              Title = 'Tax Liabilities'
              object Splitter2: TevSplitter
                Left = 171
                Top = 48
                Height = 457
              end
              object wwDBGrid2x: TevDBGrid
                Left = 174
                Top = 48
                Width = 601
                Height = 457
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'ADJUSTMENT_TYPE;CustomEdit;lcbAdjType')
                Selected.Strings = (
                  'TAX_TYPE'#9'15'#9'Type'#9'F'
                  'AMOUNT'#9'12'#9'Amount'#9'F'
                  'DUE_DATE'#9'10'#9'Due date'#9'F'
                  'CHECK_DATE'#9'10'#9'Check date'#9'F'
                  'ADJUSTMENT_TYPE'#9'10'#9'Adj. type'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_TAX_LIABILITIES_BASE\wwDBGrid2x'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object wwDBGrid1x: TevDBGrid
                Left = 18
                Top = 48
                Width = 153
                Height = 457
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CHECK_DATE'#9'10'#9'Check Date'#9
                  'RUN_NUMBER'#9'5'#9'RUN #'#9'F'
                  'SCHEDULED_PROCESS_DATE'#9'10'#9'Scheduled Process Date'#9)
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_TAX_LIABILITIES_BASE\wwDBGrid1x'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alLeft
                DataSource = wwdsSubMaster
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object lcbAdjType: TevDBComboBox
                Left = 160
                Top = 232
                Width = 121
                Height = 21
                HelpContext = 43508
                ShowButton = True
                Style = csDropDownList
                MapList = False
                AllowClearKey = True
                AutoDropDown = True
                DataField = 'ADJUSTMENT_TYPE'
                DataSource = wwdsDetail
                DropDownCount = 8
                Enabled = False
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 2
                UnboundDataType = wwDefault
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbTaxLiabilitiesBase: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 183
        Align = alClient
        TabOrder = 0
        object fpLiabilityNotes: TisUIFashionPanel
          Left = 8
          Top = 440
          Width = 305
          Height = 112
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpLiabilityNotes'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Notes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object dmemNotes: TEvDBMemo
            Left = 12
            Top = 35
            Width = 269
            Height = 54
            DataField = 'NOTES'
            DataSource = wwdsDetail
            ScrollBars = ssVertical
            TabOrder = 0
          end
        end
        object fpBaseLiabilitiesDetails: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 305
          Height = 425
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpBaseLiabilitiesDetails'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Liability Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablCheck_Date: TevLabel
            Left = 24
            Top = 80
            Width = 66
            Height = 16
            Caption = '~Check Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablProcess_Date: TevLabel
            Left = 24
            Top = 128
            Width = 64
            Height = 13
            Caption = 'Process Date'
          end
          object lablImpound: TevLabel
            Left = 24
            Top = 176
            Width = 41
            Height = 13
            Caption = 'Impound'
            FocusControl = dedtImpound
          end
          object lablTax_Deposits: TevLabel
            Left = 24
            Top = 224
            Width = 62
            Height = 13
            Caption = 'Tax Deposits'
          end
          object lablPayroll_Run: TevLabel
            Left = 24
            Top = 32
            Width = 54
            Height = 13
            Caption = 'Payroll Run'
            FocusControl = dedtPayroll_Run
          end
          object wwdbtpCheckDate: TevDBDateTimePicker
            Left = 24
            Top = 96
            Width = 121
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'CHECK_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 0
          end
          object dedtImpound: TevDBEdit
            Left = 24
            Top = 192
            Width = 121
            Height = 21
            HelpContext = 43506
            DataField = 'IMPOUND_CO_BANK_ACCT_REG_NBR'
            DataSource = wwdsDetail
            Enabled = False
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtPayroll_Run: TevDBEdit
            Left = 24
            Top = 48
            Width = 121
            Height = 21
            HelpContext = 43502
            DataField = 'RUN_NUMBER'
            Enabled = False
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwDBDateTimePicker2: TevDBDateTimePicker
            Left = 24
            Top = 144
            Width = 121
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'PROCESS_DATE'
            DataSource = wwdsSubMaster
            Epoch = 1950
            Enabled = False
            ReadOnly = True
            ShowButton = True
            TabOrder = 3
          end
          object drgpThirdParty: TevDBRadioGroup
            Left = 24
            Top = 270
            Width = 121
            Height = 43
            HelpContext = 11516
            Caption = 'Third Party'
            Columns = 2
            DataField = 'THIRD_PARTY'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
          end
          object dedtTax_Deposits: TevEdit
            Left = 24
            Top = 240
            Width = 121
            Height = 21
            ReadOnly = True
            TabOrder = 5
            Text = 'dedtTax_Deposits'
          end
          object buttonAssociateDeposit: TevBitBtn
            Left = 144
            Top = 240
            Width = 21
            Height = 21
            Hint = 'Attach a tax deposit to this liability'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
            OnClick = buttonAssociateDepositClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCC
              CCCCCCCCE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFE2E2E2CDCCCCCDCCCCCDCCCCE2E2E2FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E153A882008C4C008B
              4A008C4C53A882E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFE2E2E29E9E9E7E7E7D7C7B7B7E7E7D9E9E9EE2E2E2FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF54A88100995B00BB8677E0
              C600BB8600995C53A882FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF9D9D9D8A8A8AACACACD7D6D6ACACAC8A8A8A9E9E9EDCDCDCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00894600C18D00BC83FFFF
              FF00BC8300C18D008C4CDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCC7A7979B2B2B2ACACACFFFFFFACACACB2B2B27E7E7D8CBAA36DB08F
              6EB18F6FB18F6EB18F6EB08E6EB18E70B1907AB49500864175E6CDFFFFFFFFFF
              FFFFFFFF77E7CE008A49B4B4B4A7A7A7A8A8A7A8A8A7A8A8A7A7A7A7A8A8A7A8
              A8A7ABABAB777676DDDDDDFFFFFFFFFFFFFFFFFFDEDEDE7B7B7B6AAD8BCBF4DB
              8CC7B583BEA793CBBB92CAB992CBBA95CCBCA0CFC200854000CB9700C990FFFF
              FF00C99000CC98008A49A3A3A3EEEEEEC1C1C1B8B7B7C6C6C6C4C4C4C5C5C5C7
              C7C7CBCBCB767675BCBCBBB9B9B9FFFFFFB9B9B9BDBCBC7B7B7B65A986CAF3DC
              5FA27C4E9368B8EBCFB7E9CD98D1B288C4A391C8A92C9B67009F6000D39D74EE
              D400D39C00A061309B68A09F9FEDEDED989898888888E4E3E3E2E2E2C9C8C8BB
              BBBBC0C0C08E8E8E909090C4C3C3E4E4E4C4C3C39191918E8E8E62A581CDF2E0
              73B593B3E7CDB0E4CAB0E4CA6BAC89B3E4C7B6E5C97AB2924AB28200843F0083
              3E00833E56B78A72AB8A9B9B9BEDEDEDABABABE0E0E0DDDDDDDDDDDDA2A2A2DC
              DCDCDEDEDEAAA9A9A5A5A5757474747373747373AAAAAAA2A2A25FA37ED0F2E4
              6CAD8BABE2C851946DABE2C86DAC88B0E2C6B1E2C671AE8AB5E6CF609B76BAE8
              D179B292DAF6E963A580999999EFEEEEA4A4A4DBDADA8A8A8ADBDADAA2A2A2DB
              DADADBDADAA4A4A4E0DFDF929292E2E2E2AAA9A9F3F3F39B9B9B5CA07AD8F5E8
              67AA85A7E0C6A5DEC4A5DEC461A17CB2E3C6B2E3C661A27CA7DFC6A7DFC6A9E1
              C869AB86D9F5E95DA07A969595F2F2F1A1A0A0D9D9D9D7D6D6D7D6D6979797DC
              DBDBDCDBDB989898D8D8D8D8D8D8DADADAA1A1A1F2F2F1969595599D75DFF7EF
              549A705EA27D9FDBC39FDBC280BFA07EB89478B38F80BFA09FDBC29FDBC35EA2
              7D559B73DFF7EF599D75939393F5F5F58F8F8F989898D4D4D4D4D4D4B7B6B6AF
              AFAFAAA9A9B7B6B6D4D4D4D4D4D4989898919191F5F5F5939393569B72E5F9F4
              448B5E468C615A9E765A9D755A9E765B9F775B9F775A9E765A9D755A9E76468C
              61448B5EE5F9F4569B72919191F8F8F880807F82818194939393939394939395
              959495959494939393939394939382818180807FF8F8F891919154976FE7FAF5
              E3F6F1E3F6F1E3F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E3F5F0E3F6
              F1E3F6F1E7FAF554976F8D8D8DFAF9F9F5F5F5F5F5F5F4F4F4F4F4F4F4F4F4F4
              F4F4F4F4F4F4F4F4F4F4F4F4F4F4F5F5F5F5F5F5FAF9F98D8D8D8CB99E89C6A6
              B0E7CCAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5
              CAB0E7CC89C6A68CB99EB2B2B2BEBDBDE0DFDFDEDEDEDEDEDEDEDEDEDEDEDEDE
              DEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEE0DFDFBEBDBDB2B2B2FFFFFF8AB79B
              5092694F91684F91684F91684F91684F91684F91684F91684F91684F91684F91
              685092698AB79BFFFFFFFFFFFFB0AFAF87878786868686868686868686868686
              8686868686868686868686868686868686878787B0AFAFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 24
            Top = 320
            Width = 129
            Height = 41
            Caption = 'Collected for Quarterlies'
            Columns = 2
            DataField = 'IMPOUNDED'
            DataSource = wwdsDetail
            Items.Strings = (
              'Yes'
              'No')
            TabOrder = 7
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBEditAchKey: TevDBEdit
            Left = 168
            Top = 336
            Width = 121
            Height = 21
            HelpContext = 43502
            DataField = 'ACH_KEY'
            DataSource = wwdsDetail
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evPanelssd1: TevPanel
            Left = 168
            Top = 320
            Width = 121
            Height = 17
            BevelOuter = bvNone
            TabOrder = 9
            object evLabelssd1: TevLabel
              Left = 0
              Top = 0
              Width = 117
              Height = 13
              Align = alLeft
              Caption = 'ACH Key (mm/dd/yyyy#)'
            end
          end
          object evDBDateTimePicker3: TevDBDateTimePicker
            Left = 24
            Top = 384
            Width = 121
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'PERIOD_BEGIN_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 10
          end
          object evDBDateTimePicker4: TevDBDateTimePicker
            Left = 168
            Top = 384
            Width = 121
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'PERIOD_END_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            ShowButton = True
            TabOrder = 11
          end
          object evPanel2: TevPanel
            Left = 24
            Top = 368
            Width = 121
            Height = 17
            BevelOuter = bvNone
            TabOrder = 12
            object evLabel6: TevLabel
              Left = 0
              Top = 0
              Width = 107
              Height = 13
              Align = alLeft
              Caption = 'Tax Period Begin Date'
            end
          end
          object evPanel3: TevPanel
            Left = 168
            Top = 368
            Width = 121
            Height = 17
            BevelOuter = bvNone
            TabOrder = 13
            object evLabel4: TevLabel
              Left = 0
              Top = 0
              Width = 99
              Height = 13
              Align = alLeft
              Caption = 'Tax Period End Date'
            end
          end
        end
        object fpTaxLiabilities: TisUIFashionPanel
          Left = 320
          Top = 8
          Width = 459
          Height = 327
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 3
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Tax Liabilities'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label27: TevLabel
            Left = 12
            Top = 291
            Width = 65
            Height = 13
            Caption = 'Total Taxes ='
            FocusControl = wwdgTaxLiabilities
          end
          object lablTotalTaxValue: TevLabel
            Left = 85
            Top = 291
            Width = 3
            Height = 13
          end
          object Label2x: TevLabel
            Left = 328
            Top = 16
            Width = 98
            Height = 16
            Caption = 'Tax Liabilities'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object wwdgTaxLiabilities: TevDBGrid
            Left = 12
            Top = 35
            Width = 423
            Height = 245
            DisableThemesInTitle = False
            ControlType.Strings = (
              'ADJUSTMENT_TYPE;CustomEdit;lcbAdjType')
            Selected.Strings = (
              'TAX_TYPE'#9'15'#9'Type'#9'F'
              'AMOUNT'#9'12'#9'Amount'#9'F'
              'THIRD_PARTY'#9'1'#9'3rd prty'#9'F'
              'IMPOUNDED'#9'1'#9'Collected'#9'F'
              'CHECK_DATE'#9'10'#9'Check date'#9'F'
              'ACH_KEY'#9'11'#9'ACH key'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_TAX_LIABILITIES_BASE\wwdgTaxLiabilities'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            OnMultiSelectRecord = wwdgTaxLiabilitiesMultiSelectRecord
            OnRowChanged = wwdgTaxLiabilitiesRowChanged
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpTools: TisUIFashionPanel
          Left = 8
          Top = 559
          Width = 305
          Height = 83
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpLiabilityNotes'
          Color = 14737632
          TabOrder = 4
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Controls'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object BitBtn3: TevBitBtn
            Left = 12
            Top = 35
            Width = 130
            Height = 25
            Caption = 'Re-calc Totals'
            TabOrder = 0
            OnClick = BitBtn3Click
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFDCDCDC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCCCCCCCD3D3D3FFFFFFFFFFFFDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
              CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCD4D4D4FFFFFFFFFFFFB0B0B0
              9F9F9F9E9E9E9E9E9E9E9F9F9E9E9E9E9E9E9E9F9F9E9E9E9D9E9F9D9E9F9D9E
              9E9F9F9FAAAAAAFFFFFFFFFFFFB1B0B0A09F9F9F9F9E9F9F9EA09F9F9F9F9E9F
              9F9EA09F9F9F9F9E9F9F9E9F9F9E9F9F9EA09F9FAAAAAAFFFFFFFFFFFF9F9F9F
              FFFEFEFBFAFAFEFDFDFEFDFDFDFCFCFEFDFDFEFDFEFBFCFDF9FCFFF9FCFFF9FA
              FCFFFEFE9F9F9FFFFFFFFFFFFFA09F9FFFFFFFFCFCFBFEFEFEFEFEFEFEFDFDFE
              FEFEFFFFFFFEFDFDFEFEFEFEFEFEFCFCFBFFFFFFA09F9FFFFFFFFFFFFF9E9E9E
              F7F6F6F2F1F08D8A886D6B68F7F6F58D8B896C6B69F2F6F9E49F55E49F55EEF2
              F5F7F6F79E9E9EFFFFFFFFFFFF9F9F9EF7F7F7F3F2F28A8A8A6A6A6AF7F7F78B
              8B8B6B6B6BF8F8F8999999999999F4F4F4F8F8F89F9F9EFFFFFFFFFFFF9D9E9E
              F4F3F2EDECEBF2F1F0F3F3F2F0F0EFF3F2F1F3F3F3EDF1F6EEAD66E4A25BE9ED
              F2F4F4F39D9E9EFFFFFFFFFFFF9F9F9EF4F4F4EDEDEDF3F2F2F4F4F4F1F1F1F3
              F3F3F4F4F4F3F3F3A7A7A79C9C9CF0EFEFF5F5F59F9F9EFFFFFFFFFFFF9E9E9E
              F3F2F0E9E8E78E8C8A6E6C6AEDECEB8F8D8A6E6C6BE9ECF0F0AC62F0AC62E5E8
              ECF2F2F19E9E9EFFFFFFFFFFFF9F9F9EF3F3F3E9E9E98C8C8C6C6C6CEDEDED8D
              8D8D6C6C6CEEEEEEA6A6A6A6A6A6EAEAEAF3F3F39F9F9EFFFFFFFFFFFF9E9E9E
              F0EEEEE4E2E2E9E7E7EAE8E9E8E6E6E9E7E8EAE8E9E6E5E7E5E6EBE4E6EBE2E2
              E4F0EEEE9E9E9EFFFFFFFFFFFF9F9F9EF0EFEFE4E3E3E8E8E8EAEAEAE8E7E7E8
              E8E8EAEAEAE7E7E6E8E8E8E8E8E8E4E3E3F0EFEF9F9F9EFFFFFFFFFFFF9E9F9F
              EEEDEDE0DEDD918E8A716F6BE5E2E2918F8C706E6CE4E2E2918F8C706E6AE1DF
              DEEEEDEE9E9F9FFFFFFFFFFFFFA09F9FEFEEEEE0DFDF8E8E8E6E6E6EE4E3E38F
              8F8F6D6D6DE4E3E38F8F8F6D6D6DE0E0E0EFEEEEA09F9FFFFFFFFFFFFF9F9F9F
              EEECECDDD9D5E4DED6E5DFD7E0DDD9E0DEDDE1DFDEE1DDD9E5DFD7E5DFD6DDD9
              D5EEEDEC9F9F9FFFFFFFFFFFFFA09F9FEDEDEDDADADADEDEDEDFDFDEDEDEDEE0
              DFDFE0E0E0DEDEDEDFDFDEDFDFDEDADADAEEEEEEA09F9FFFFFFFFFFFFF9F9F9F
              EDECEADAD5CE1F7FFF2266FFDDD8D1928F8C716F6CDED8D22080FF2265FFDAD5
              CEEEECEA9F9F9FFFFFFFFFFFFFA09F9FEDEDEDD5D5D5A6A6A69A9A9AD8D8D88F
              8F8F6E6E6ED8D8D8A6A6A6999999D5D5D5EDEDEDA09F9FFFFFFFFFFFFFA0A1A1
              F1F0F0F1EFEDF6F1ECF6F2ECF2F1EFF1F1F2F2F2F3F2F1EFF6F2ECF6F2ECF1F0
              EDF1F0F0A0A1A1FFFFFFFFFFFFA1A1A1F2F2F1F0F0F0F2F2F1F3F2F2F2F2F1F3
              F2F2F3F3F3F2F2F1F3F2F2F3F2F2F1F1F1F2F2F1A1A1A1FFFFFFFFFFFFA2A3A5
              7C7E7F7B7D807C7E807C7E817B7E817B7E827B7E827B7E817C7E817C7E807B7D
              807C7E7FA2A3A5FFFFFFFFFFFFA4A4A47F7E7E7E7E7D7F7E7E7F7E7E7F7E7E80
              807F80807F7F7E7E7F7E7E7F7E7E7E7E7D7F7E7EA4A4A4FFFFFFFFFFFFA0A3A7
              FFCA8BF9C27EF9C27FF9C280F9C280F9C280F9C280F9C280F9C280F9C27FF9C2
              7EFFCA8BA0A3A7FFFFFFFFFFFFA4A4A4C4C3C3BBBBBBBBBBBBBBBBBBBBBBBBBB
              BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC4C3C3A4A4A4FFFFFFFFFFFF9EA2A6
              F9D0A4E8A760E7A762E7A863E7A863E7A863E7A863E7A863E7A863E7A762E8A7
              60F9D0A49EA2A6FFFFFFFFFFFFA3A3A3CBCBCBA1A0A0A1A0A0A1A1A1A1A1A1A1
              A1A1A1A1A1A1A1A1A1A1A1A1A0A0A1A0A0CBCBCBA3A3A3FFFFFFFFFFFF9FA2A5
              FDDBB6F8DAB8F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DA
              B8FDDBB69FA2A5FFFFFFFFFFFFA3A3A3D7D7D7D7D6D6D7D6D6D7D6D6D7D6D6D7
              D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D7D7A3A3A3FFFFFFFFFFFFA9AAAB
              9FA2A59EA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59EA1
              A59FA2A5A9AAABFFFFFFFFFFFFABABABA3A3A3A2A2A2A2A2A2A2A2A2A2A2A2A2
              A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A3A3A3ABABABFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object bbtnAttach: TevBitBtn
            Left = 151
            Top = 35
            Width = 130
            Height = 25
            Caption = 'Attach to Payroll'
            TabOrder = 1
            OnClick = bbtnAttachClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFD5D5D5CCCCCCCCCCCCCCCCCCD9D9D9FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5CCCCCCCC
              CCCCCCCCCCD9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCCCCC998F83978A7F978A7F978A7F9E968CCCCCCCFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC8E8E8E89898989
              8989898989959594CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFD5D5D5978A7FAEA59DC8C1BBC6BFB9C6BEB8ADA39A978A7FD5D5D5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5898989A5A5A5C1C1C1BF
              BFBFBEBEBEA2A2A2898989D5D5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC0B8B2978A7FB6ACA4FFFFFFFFFFFFFFFFFFC4BCB5ADA39A9A8F84FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8B8B8898989ABABABFFFFFFFF
              FFFFFFFFFFBCBCBBA2A2A28E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC1B9B3978A7FFFFFFFCCCCCCCCCCCCCCCCCCFFFFFFC5C0B8978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B9B9898989FFFFFFCCCCCCCC
              CCCCCCCCCCFFFFFFBFBFBF898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BEB6978A7FCCCCCC978A7F978A7F978A7FCCCCCCC6C0B8978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD898989CCCCCC89898989
              8989898989CCCCCCBFBFBF898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC3BDB5978A7FA4998EC8C2BAC4BEB7BAB2AA978A7FC6C0B9978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989989898C2C2C1BE
              BDBDB2B1B1898989C0C0C0898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC3BDB5978A7FC0B8B0968A7DFFFFFFC2BBB3978A7FC7C1B9978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989B8B7B7888888FF
              FFFFBBBBBB898989C1C1C1898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BDB5978A7FC6BFB896887BFFFFFFC2BBB3978A7FC8C0B9978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989BFBFBF868686FF
              FFFFBBBBBB898989C0C0C0898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BDB5978A7FC6BFB896897CFFFFFFCFC9C4978A7FC5BEB7978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989BFBFBF878787FF
              FFFFCAC9C9898989BEBDBD898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BDB5978A7FC7C0B9998D80FFFFFFFFFFFFFFFFFFC2BAB2978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989C0C0C08B8B8BFF
              FFFFFFFFFFFFFFFFBABABA898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFCFC9C4978A7FC8C0B99D8F82FFFFFFFFFFFFFDFDFDC2B8B1978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAC9C9898989C0C0C08E8E8EFF
              FFFFFFFFFFFEFEFEB8B8B8898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFC7BFB99D9084E7E7E7FFFFFFE9E9E9BEB4AD978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF8F8F8FE8
              E7E7FFFFFFE9E9E9B4B4B4898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFC5BCB6A4968CB1AAA4CCCCCCBCB7B2A5978EB4ACA5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBB969595AA
              A9A9CCCCCCB8B7B7979696AAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFD2CBC6C1B8B2A3968C9E8F839D8E839F9085CCC4BEFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBCBCBB8B8B896
              95958E8E8E8D8D8D8F8F8FC4C3C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFDBD6D2C4BBB4C4BCB5C5BDB6DAD5D1FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D6D6BB
              BBBBBCBCBBBDBCBCD5D5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object Panel4: TevPanel
          Left = 808
          Top = 64
          Width = 332
          Height = 408
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object fpTaxDeposit: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 321
            Height = 393
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Tax Deposit'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object lablAdjustment_Type1: TevLabel
              Left = 12
              Top = 35
              Width = 79
              Height = 13
              Caption = 'Adjustment Type'
              FocusControl = wwcbAdjustment_Type1
            end
            object Label91: TevLabel
              Left = 140
              Top = 35
              Width = 30
              Height = 13
              Caption = 'Status'
              FocusControl = wwcbAdjustment_Type1
            end
            object Label101: TevLabel
              Left = 12
              Top = 74
              Width = 55
              Height = 13
              Caption = 'Tax deposit'
              FocusControl = wwcbAdjustment_Type1
            end
            object wwcbAdjustment_Type1: TevDBComboBox
              Left = 12
              Top = 50
              Width = 120
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = True
              AutoDropDown = True
              DataField = 'ADJUSTMENT_TYPE'
              DataSource = dsGroupUpdate
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 0
              UnboundDataType = wwDefault
            end
            object wwcbStatus1: TevDBComboBox
              Left = 140
              Top = 50
              Width = 120
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = True
              AutoDropDown = True
              DataField = 'STATUS'
              DataSource = dsGroupUpdate
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 1
              UnboundDataType = wwDefault
            end
            object DBRadioGroup11: TevDBRadioGroup
              Left = 12
              Top = 113
              Width = 120
              Height = 37
              HelpContext = 11516
              Caption = 'Third Party'
              Columns = 2
              DataField = 'THIRD_PARTY'
              DataSource = dsGroupUpdate
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 4
              Values.Strings = (
                'Y'
                'N')
            end
            object Button1: TevButton
              Left = 140
              Top = 206
              Width = 120
              Height = 21
              Caption = 'Apply'
              TabOrder = 9
              OnClick = Button1Click
              Color = clBlack
              Margin = 0
            end
            object wwDBLookupCombo11: TevDBLookupCombo
              Left = 12
              Top = 89
              Width = 120
              Height = 21
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'TAX_PAYMENT_REFERENCE_NUMBER'#9'20'#9'TAX_PAYMENT_REFERENCE_NUMBER'#9'F'
                'STATUS'#9'1'#9'STATUS'#9'F'
                'STATUS_DATE'#9'18'#9'STATUS_DATE'#9'F'
                'PayrollDescription'#9'10'#9'PayrollDescription'#9'F')
              DataField = 'CO_TAX_DEPOSITS_NBR'
              DataSource = dsGroupUpdate
              LookupTable = DM_CO_TAX_DEPOSITS.CO_TAX_DEPOSITS
              LookupField = 'CO_TAX_DEPOSITS_NBR'
              Style = csDropDownList
              TabOrder = 2
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = True
            end
            object cbClearTaxDepRecord: TevCheckBox
              Left = 140
              Top = 91
              Width = 129
              Height = 17
              Caption = 'Unlink deposit record'
              TabOrder = 3
              OnClick = cbClearTaxDepRecordClick
            end
            object evDBRadioGroup2: TevDBRadioGroup
              Left = 140
              Top = 113
              Width = 120
              Height = 37
              Caption = 'Collected for Quarterlies'
              Columns = 2
              DataField = 'IMPOUNDED'
              DataSource = dsGroupUpdate
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 5
              Values.Strings = (
                'Y'
                'N')
            end
            object evPanel1: TevPanel
              Left = 12
              Top = 191
              Width = 121
              Height = 13
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 10
              object evLabel1: TevLabel
                Left = 0
                Top = 0
                Width = 117
                Height = 13
                Align = alClient
                Caption = 'ACH Key (mm/dd/yyyy#)'
              end
            end
            object evDBEdit1: TevDBEdit
              Left = 12
              Top = 206
              Width = 120
              Height = 21
              HelpContext = 43502
              DataField = 'ACH_KEY'
              DataSource = dsGroupUpdate
              TabOrder = 8
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object evDBDateTimePicker1: TevDBDateTimePicker
              Left = 12
              Top = 167
              Width = 120
              Height = 21
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'PERIOD_BEGIN_DATE'
              DataSource = dsGroupUpdate
              Epoch = 1950
              ShowButton = True
              TabOrder = 6
            end
            object evDBDateTimePicker2: TevDBDateTimePicker
              Left = 140
              Top = 167
              Width = 120
              Height = 21
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'PERIOD_END_DATE'
              DataSource = dsGroupUpdate
              Epoch = 1950
              ShowButton = True
              TabOrder = 7
            end
            object evPanel4: TevPanel
              Left = 12
              Top = 152
              Width = 121
              Height = 13
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 11
              object evLabel2: TevLabel
                Left = 0
                Top = 0
                Width = 107
                Height = 13
                Align = alClient
                Caption = 'Tax Period Begin Date'
              end
            end
            object evPanel5: TevPanel
              Left = 140
              Top = 152
              Width = 121
              Height = 13
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 12
              object evLabel3: TevLabel
                Left = 0
                Top = 0
                Width = 99
                Height = 13
                Align = alClient
                Caption = 'Tax Period End Date'
              end
            end
          end
        end
      end
    end
    object tsCO_TAX_DEPOSITS: TTabSheet
      Caption = 'Tax Deposits'
      ImageIndex = 28
      TabVisible = False
      object sbTaxDeposits: TScrollBox
        Left = 0
        Top = 0
        Width = 427
        Height = 183
        Align = alClient
        TabOrder = 0
        object Label8: TevLabel
          Left = 584
          Top = 0
          Width = 110
          Height = 16
          Caption = 'TAX DEPOSITS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object fpTaxDeposits: TisUIFashionPanel
          Left = 296
          Top = 8
          Width = 376
          Height = 334
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Tax Deposits'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwDBGrid1: TevDBGrid
            Left = 12
            Top = 35
            Width = 341
            Height = 276
            DisableThemesInTitle = False
            Selected.Strings = (
              'TAX_PAYMENT_REFERENCE_NUMBER'#9'20'#9'Tax Payment Reference Number'#9
              'STATUS'#9'1'#9'Status'#9
              'STATUS_DATE'#9'18'#9'Status Date'#9
              'PayrollDescription'#9'40'#9'Payrolldescription'#9)
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_TAX_LIABILITIES_BASE\wwDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsCO_TAX_DEPOSITS
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpTaxDepositDetail: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 282
          Height = 334
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpTaxDepositDetail'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Tax Deposit Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label1: TevLabel
            Left = 12
            Top = 35
            Width = 255
            Height = 48
            Caption = 
              'With this screen you can select a tax deposit to apply to the cu' +
              'rrent liability.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            WordWrap = True
          end
          object lablStatus2: TevLabel
            Left = 140
            Top = 217
            Width = 39
            Height = 16
            Caption = '~Status'
            FocusControl = wwcbStatus2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablAdjustment_Type2: TevLabel
            Left = 12
            Top = 178
            Width = 79
            Height = 13
            Caption = 'Adjustment Type'
            FocusControl = wwcbAdjustment_Type2
          end
          object lablAmount2: TevLabel
            Left = 140
            Top = 178
            Width = 45
            Height = 16
            Caption = '~Amount'
            FocusControl = wwedAmount2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDue_Date2: TevLabel
            Left = 140
            Top = 139
            Width = 55
            Height = 16
            Caption = '~Due Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablTax_Type2: TevLabel
            Left = 12
            Top = 100
            Width = 54
            Height = 16
            Caption = '~Tax Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label2: TevLabel
            Left = 12
            Top = 139
            Width = 66
            Height = 16
            Caption = '~Check Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TevLabel
            Left = 140
            Top = 100
            Width = 64
            Height = 13
            Caption = 'Process Date'
          end
          object Label4: TevLabel
            Left = 400
            Top = 191
            Width = 41
            Height = 13
            Caption = 'Impound'
            FocusControl = DBEdit1
            Visible = False
          end
          object Label6: TevLabel
            Left = 400
            Top = 74
            Width = 54
            Height = 13
            Caption = 'Payroll Run'
            FocusControl = DBEdit2
            Visible = False
          end
          object Label7: TevLabel
            Left = 12
            Top = 217
            Width = 62
            Height = 13
            Caption = 'Tax Deposits'
          end
          object BitBtn5: TevBitBtn
            Left = 12
            Top = 287
            Width = 120
            Height = 25
            Caption = 'Apply Deposit'
            TabOrder = 8
            OnClick = BitBtn5Click
            Color = clBlack
            NumGlyphs = 2
            Margin = 0
          end
          object wwcbStatus2: TevDBComboBox
            Left = 140
            Top = 232
            Width = 120
            Height = 21
            HelpContext = 43509
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            Enabled = False
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
          end
          object wwcbAdjustment_Type2: TevDBComboBox
            Left = 12
            Top = 193
            Width = 120
            Height = 21
            HelpContext = 43508
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'ADJUSTMENT_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            Enabled = False
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
          object wwedAmount2: TevDBEdit
            Left = 140
            Top = 193
            Width = 120
            Height = 21
            HelpContext = 43512
            DataField = 'AMOUNT'
            DataSource = wwdsDetail
            Enabled = False
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdpDue_Date2: TevDBDateTimePicker
            Left = 140
            Top = 154
            Width = 120
            Height = 21
            HelpContext = 43505
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'DUE_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            Enabled = False
            ShowButton = True
            TabOrder = 3
          end
          object wwcbType2: TevDBComboBox
            Left = 12
            Top = 115
            Width = 120
            Height = 21
            HelpContext = 43513
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TAX_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            Enabled = False
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object DBEdit1: TevDBEdit
            Left = 400
            Top = 206
            Width = 120
            Height = 21
            HelpContext = 43506
            DataField = 'IMPOUND_CO_BANK_ACCT_REG_NBR'
            DataSource = wwdsDetail
            Enabled = False
            TabOrder = 11
            UnboundDataType = wwDefault
            Visible = False
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit2: TevDBEdit
            Left = 400
            Top = 89
            Width = 120
            Height = 21
            HelpContext = 43502
            DataField = 'RUN_NUMBER'
            Enabled = False
            TabOrder = 10
            UnboundDataType = wwDefault
            Visible = False
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwDBDateTimePicker1: TevDBDateTimePicker
            Left = 12
            Top = 154
            Width = 120
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'CHECK_DATE'
            DataSource = wwdsDetail
            Epoch = 1950
            Enabled = False
            ShowButton = True
            TabOrder = 2
          end
          object wwDBDateTimePicker3: TevDBDateTimePicker
            Left = 140
            Top = 115
            Width = 120
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'SCHEDULED_PROCESS_DATE'
            Epoch = 1950
            Enabled = False
            ReadOnly = True
            ShowButton = True
            TabOrder = 1
          end
          object dedtTaxDeposit2: TevEdit
            Left = 12
            Top = 232
            Width = 120
            Height = 21
            ReadOnly = True
            TabOrder = 6
            Text = 'dedtTax_Deposits'
          end
          object BitBtn4: TevBitBtn
            Left = 140
            Top = 287
            Width = 120
            Height = 25
            Caption = 'Clear Deposit'
            TabOrder = 9
            OnClick = BitBtn4Click
            Color = clBlack
            NumGlyphs = 2
            Margin = 0
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    OnDataChange = wwdsDataChange
    MasterDataSource = wwdsSubMaster
  end
  object wwdsCO_TAX_DEPOSITS: TevDataSource
    DataSet = DM_CO_TAX_DEPOSITS.CO_TAX_DEPOSITS
    Left = 532
    Top = 137
  end
  object dsGroupUpdate: TevDataSource
    DataSet = zLIABILITIES
    Left = 140
    Top = 33
  end
  object zLIABILITIES: TevClientDataSet
    Left = 60
    Top = 25
    object zLIABILITIESSADJUSTMENT_TYPE: TStringField
      FieldName = 'ADJUSTMENT_TYPE'
      Size = 1
    end
    object zLIABILITIESSSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object zLIABILITIESSCO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
    end
    object zLIABILITIESSTHIRD_PARTY: TStringField
      FieldName = 'THIRD_PARTY'
      Size = 1
    end
    object zLIABILITIESIMPOUNDED: TStringField
      FieldName = 'IMPOUNDED'
      Size = 1
    end
    object zLIABILITIESACH_KEY: TStringField
      FieldName = 'ACH_KEY'
      Size = 11
    end
    object zLIABILITIESPERIOD_BEGIN_DATE: TDateTimeField
      FieldName = 'PERIOD_BEGIN_DATE'
    end
    object zLIABILITIESPERIOD_END_DATE: TDateTimeField
      FieldName = 'PERIOD_END_DATE'
    end
    object zLIABILITIESCO_LOCATIONS_NBR: TIntegerField
      FieldName = 'CO_LOCATIONS_NBR'
    end
    object zLIABILITIESNONRES_CO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'NONRES_CO_LOCAL_TAX_NBR'
    end
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_PR.PR
    OnDataChange = wwdsSubMasterDataChange
    MasterDataSource = wwdsMaster
    Left = 180
    Top = 51
  end
  object DM_PAYROLL: TDM_PAYROLL
    Left = 455
    Top = 45
  end
end
