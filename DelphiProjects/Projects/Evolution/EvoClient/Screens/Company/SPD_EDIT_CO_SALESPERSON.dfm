inherited EDIT_CO_SALESPERSON: TEDIT_CO_SALESPERSON
  Width = 846
  Height = 662
  inherited Panel1: TevPanel
    Width = 846
    inherited pnlFavoriteReport: TevPanel
      Left = 694
    end
    inherited pnlTopLeft: TevPanel
      Width = 694
    end
  end
  inherited PageControl1: TevPageControl
    Width = 846
    Height = 608
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 838
        Height = 579
        inherited pnlBorder: TevPanel
          Width = 834
          Height = 575
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 834
          Height = 575
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited splEDIT_CO_BASE: TevSplitter
                    Left = 304
                  end
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 298
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 286
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 304
                    Width = 486
                    Visible = True
                    object pnlSalesRightBorder: TevPanel
                      Left = 6
                      Top = 6
                      Width = 474
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      object fpSalesRight: TisUIFashionPanel
                        Left = 0
                        Top = 0
                        Width = 474
                        Height = 537
                        Align = alClient
                        BevelOuter = bvNone
                        BorderWidth = 12
                        Color = 14737632
                        TabOrder = 0
                        OnResize = fpSalesRightResize
                        RoundRect = True
                        ShadowDepth = 8
                        ShadowSpace = 8
                        ShowShadow = True
                        ShadowColor = clSilver
                        TitleColor = clGrayText
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWhite
                        TitleFont.Height = -13
                        TitleFont.Name = 'Arial'
                        TitleFont.Style = [fsBold]
                        Title = 'Sales'
                        LineWidth = 0
                        LineColor = clWhite
                        Theme = ttCustom
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 786
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 786
            Height = 468
            inherited Splitter1: TevSplitter
              Height = 464
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 464
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 384
                IniAttributes.SectionName = 'TEDIT_CO_SALESPERSON\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 303
              Width = 479
              Height = 464
              Align = alClient
              Visible = True
              Title = 'Sales'
              object evDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 429
                Height = 384
                DisableThemesInTitle = False
                Selected.Strings = (
                  'Name'#9'20'#9'Name'#9)
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_SALESPERSON\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster3
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbCompanySales: TScrollBox
        Left = 0
        Top = 0
        Width = 838
        Height = 579
        Align = alClient
        TabOrder = 0
        object evPanel3: TevPanel
          Left = 232
          Top = 112
          Width = 243
          Height = 154
          BevelOuter = bvNone
          Caption = 'evPanel3'
          TabOrder = 3
          Visible = False
          object evPanel6: TevPanel
            Left = 0
            Top = 0
            Width = 243
            Height = 17
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
          end
        end
        object pnlFlatAmount: TevPanel
          Left = 610
          Top = 59
          Width = 143
          Height = 262
          BevelOuter = bvNone
          TabOrder = 4
          Visible = False
          object evPanel4: TevPanel
            Left = 0
            Top = 158
            Width = 143
            Height = 104
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
          end
        end
        object fpSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 193
          Height = 557
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Salesperson'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwDBGrid1: TevDBGrid
            Left = 12
            Top = 35
            Width = 158
            Height = 499
            DisableThemesInTitle = False
            Selected.Strings = (
              'Name'#9'20'#9'Name'#9)
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_SALESPERSON\wwDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsSubMaster3
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpDetail: TisUIFashionPanel
          Left = 209
          Top = 8
          Width = 580
          Height = 215
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Salesperson Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label16: TevLabel
            Left = 12
            Top = 35
            Width = 37
            Height = 16
            Caption = '~Name'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label23: TevLabel
            Left = 12
            Top = 74
            Width = 33
            Height = 16
            Caption = '~Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label22: TevLabel
            Left = 12
            Top = 113
            Width = 68
            Height = 13
            Caption = 'Last Visit Date'
          end
          object Label24: TevLabel
            Left = 292
            Top = 35
            Width = 62
            Height = 13
            Caption = 'Charge Back'
          end
          object Label25: TevLabel
            Left = 430
            Top = 35
            Width = 113
            Height = 13
            Caption = 'Projected Annual Comm'
            WordWrap = True
          end
          object Label21: TevLabel
            Left = 150
            Top = 113
            Width = 80
            Height = 13
            Caption = 'Next Comm Date'
          end
          object Label20: TevLabel
            Left = 150
            Top = 74
            Width = 65
            Height = 13
            Caption = 'Next Comm %'
          end
          object Label19: TevLabel
            Left = 150
            Top = 35
            Width = 75
            Height = 16
            Caption = '~Commission %'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablReferred_By: TevLabel
            Left = 430
            Top = 74
            Width = 56
            Height = 13
            Caption = 'Referred By'
            FocusControl = dedtReferred_By
          end
          object lablSB_Referrals_Nbr: TevLabel
            Left = 292
            Top = 74
            Width = 63
            Height = 13
            Caption = 'Referral From'
          end
          object Label26: TevLabel
            Left = 292
            Top = 113
            Width = 28
            Height = 13
            Caption = 'Notes'
          end
          object dblkupSALESPERSON_NAME: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 130
            Height = 21
            HelpContext = 10517
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'LAST_NAME'#9'30'#9'LAST_NAME'
              'FIRST_NAME'#9'20'#9'FIRST_NAME'
              'MIDDLE_INITIAL'#9'1'#9'MIDDLE_INITIAL'
              'ACTIVE_USER'#9'1'#9'ACTIVE_USER')
            DataField = 'SB_USER_NBR'
            DataSource = wwdsSubMaster3
            LookupTable = DM_SB_USER.SB_USER
            LookupField = 'SB_USER_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            OnBeforeDropDown = dblkupSALESPERSON_NAMEBeforeDropDown
            OnCloseUp = dblkupSALESPERSON_NAMECloseUp
          end
          object wwDBComboBox1: TevDBComboBox
            Left = 12
            Top = 89
            Width = 130
            Height = 21
            HelpContext = 10507
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'SALESPERSON_TYPE'
            DataSource = wwdsSubMaster3
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Full Time'#9'F'
              'Full Time Temp'#9'U'
              'Part Time'#9'P'
              'Part Time temp'#9'R'
              'Seasonal'#9'S'
              'Student'#9'T'
              '1099'#9'1')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object wwdpLast_Visit_Date: TevDBDateTimePicker
            Left = 12
            Top = 128
            Width = 130
            Height = 21
            HelpContext = 10522
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'LAST_VISIT_DATE'
            DataSource = wwdsSubMaster3
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object wwdeCharge_Back: TevDBEdit
            Left = 292
            Top = 50
            Width = 130
            Height = 21
            HelpContext = 10508
            DataField = 'CHARGEBACK'
            DataSource = wwdsSubMaster3
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeProjected_Annual_Commission: TevDBEdit
            Left = 430
            Top = 50
            Width = 130
            Height = 21
            HelpContext = 10509
            DataField = 'PROJECTED_ANNUAL_COMMISSION'
            DataSource = wwdsSubMaster3
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdpNext_Comm_Date: TevDBDateTimePicker
            Left = 150
            Top = 128
            Width = 130
            Height = 21
            HelpContext = 10521
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'NEXT_COMMISSION_PERCENT_DATE'
            DataSource = wwdsSubMaster3
            Epoch = 1950
            ShowButton = True
            TabOrder = 5
          end
          object wwdeNext_Commission_Percent: TevDBEdit
            Left = 150
            Top = 89
            Width = 130
            Height = 21
            HelpContext = 10520
            DataField = 'NEXT_COMMISSION_PERCENTAGE'
            DataSource = wwdsSubMaster3
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeCommision_Percent: TevDBEdit
            Left = 150
            Top = 50
            Width = 130
            Height = 21
            HelpContext = 10519
            DataField = 'COMMISSION_PERCENTAGE'
            DataSource = wwdsSubMaster3
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtReferred_By: TevDBEdit
            Left = 430
            Top = 89
            Width = 130
            Height = 21
            HelpContext = 10523
            DataField = 'REFERRED_BY'
            DataSource = wwdsMaster
            TabOrder = 9
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwlcSB_Referrals_Nbr: TevDBLookupCombo
            Left = 292
            Top = 89
            Width = 130
            Height = 21
            HelpContext = 10506
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'SB_REFERRALS_NBR'
            DataSource = wwdsMaster
            LookupTable = DM_SB_REFERRALS.SB_REFERRALS
            LookupField = 'SB_REFERRALS_NBR'
            Style = csDropDownList
            TabOrder = 8
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object DBMemo1: TEvDBMemo
            Left = 292
            Top = 128
            Width = 268
            Height = 72
            DataField = 'NOTES'
            DataSource = wwdsSubMaster3
            TabOrder = 10
          end
        end
        object fpFlatAmount: TisUIFashionPanel
          Left = 209
          Top = 228
          Width = 580
          Height = 199
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpFlatAmount'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Flat Amount'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgFlatAmount: TevDBGrid
            Left = 12
            Top = 35
            Width = 549
            Height = 141
            DisableThemesInTitle = False
            ControlType.Strings = (
              'FREQUENCY;CustomEdit;cbFrequency;F')
            Selected.Strings = (
              'FLAT_AMOUNT_DATE'#9'16'#9'Start Date'#9'F'
              'END_DATE'#9'16'#9'End Date'#9'F'
              'FLAT_AMOUNT'#9'17'#9'Amount'#9'F'
              'FREQUENCY'#9'20'#9'Frequency'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_SALESPERSON\wwdgFlatAmount'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsSubMaster4
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpFlatAmtDetails: TisUIFashionPanel
          Left = 209
          Top = 433
          Width = 580
          Height = 131
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 5
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Flat Amount Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          DesignSize = (
            580
            131)
          object lablAmount: TevLabel
            Left = 12
            Top = 35
            Width = 45
            Height = 16
            Caption = '~Amount'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablDate: TevLabel
            Left = 12
            Top = 75
            Width = 102
            Height = 16
            Caption = '~Effective Start Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel1: TevLabel
            Left = 128
            Top = 35
            Width = 59
            Height = 16
            Caption = '~Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 128
            Top = 75
            Width = 99
            Height = 16
            Caption = '~Effective End Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 268
            Top = 35
            Width = 69
            Height = 13
            Caption = 'Week Number'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel4: TevLabel
            Left = 268
            Top = 75
            Width = 70
            Height = 13
            Caption = 'Month Number'
          end
          object wwdeFlat_Amount: TevDBEdit
            Left = 12
            Top = 50
            Width = 105
            Height = 21
            HelpContext = 10666
            DataField = 'FLAT_AMOUNT'
            DataSource = wwdsSubMaster4
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdpFlat_Amount_Date: TevDBDateTimePicker
            Left = 12
            Top = 90
            Width = 105
            Height = 21
            HelpContext = 10667
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'FLAT_AMOUNT_DATE'
            DataSource = wwdsSubMaster4
            Epoch = 1950
            ShowButton = True
            TabOrder = 3
          end
          inline MiniNavigationFrame: TMiniNavigationFrame
            Left = 440
            Top = 46
            Width = 120
            Height = 65
            Anchors = []
            TabOrder = 6
            inherited SpeedButton1: TevSpeedButton
              Width = 116
              Caption = 'Create'
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
                CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
                E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
                7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
                82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
                9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
                55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
                5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
                CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
                0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
                76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
                FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
                16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
                8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
                FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
                38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
                9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
                FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
                51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
                9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
                66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
                9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
                76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
                9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
                FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
                59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
                9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
                FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
                0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
                87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
                FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
                55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
                556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
                B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
                88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
                E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
                78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            end
            inherited SpeedButton2: TevSpeedButton
              Left = 0
              Top = 39
              Width = 116
              Caption = 'Delete'
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
                CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
                F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
                6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
                A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
                CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
                A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
                2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
                C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
                9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
                5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
                F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
                9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
                576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
                FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
                97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
                4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
                F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
                8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
                3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
                F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
                334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
                F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
                2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
                EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
                3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
                EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
                84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
                8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
                F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
                8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
                9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
                F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
                8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
                2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
                C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
                807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
                C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
                EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
                CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
                6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            end
            inherited evActionList1: TevActionList
              Left = 136
            end
          end
          object cbFlatFreq: TevDBComboBox
            Left = 128
            Top = 50
            Width = 130
            Height = 21
            HelpContext = 10507
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'FREQUENCY'
            DataSource = wwdsSubMaster4
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object edEndDate: TevDBDateTimePicker
            Left = 128
            Top = 90
            Width = 130
            Height = 21
            HelpContext = 10667
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'END_DATE'
            DataSource = wwdsSubMaster4
            Epoch = 1950
            ShowButton = True
            TabOrder = 4
          end
          object cbWeekNumber: TevDBComboBox
            Left = 268
            Top = 50
            Width = 161
            Height = 21
            HelpContext = 10507
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'WEEK_NUMBER'
            DataSource = wwdsSubMaster4
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object edMonthNumber: TevDBSpinEdit
            Left = 268
            Top = 91
            Width = 161
            Height = 21
            HelpContext = 20
            Increment = 1.000000000000000000
            MaxValue = 12.000000000000000000
            MinValue = 1.000000000000000000
            Value = 1.000000000000000000
            DataField = 'MONTH_NUMBER'
            DataSource = wwdsSubMaster4
            TabOrder = 5
            UnboundDataType = wwDefault
          end
        end
        object cbFrequency: TevDBComboBox
          Left = 472
          Top = 312
          Width = 132
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = False
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'FREQUENCY'
          DataSource = wwdsSubMaster4
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 6
          UnboundDataType = wwDefault
        end
      end
    end
  end
  inherited PageControlImages: TevImageList
    Left = 464
    Top = 48
  end
  object wwdsSubMaster3: TevDataSource
    DataSet = DM_CO_SALESPERSON.CO_SALESPERSON
    MasterDataSource = wwdsMaster
    Left = 588
    Top = 96
  end
  object wwdsSubMaster4: TevDataSource
    DataSet = DM_CO_SALESPERSON_FLAT_AMT.CO_SALESPERSON_FLAT_AMT
    MasterDataSource = wwdsSubMaster3
    Left = 520
    Top = 96
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 600
    Top = 32
  end
  object evSimpleSecElement1: TevSimpleSecElement
    Left = 288
    Top = 8
  end
end
