object ChoseLocal: TChoseLocal
  Left = 433
  Top = 327
  Width = 468
  Height = 379
  Caption = 'Choose Local'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    452
    341)
  PixelsPerInch = 96
  TextHeight = 13
  object evBitBtn1: TevBitBtn
    Left = 8
    Top = 310
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&OK'
    TabOrder = 1
    OnClick = evBitBtn1Click
    Color = clBlack
    Margin = 0
    Kind = bkOK
  end
  object evBitBtn2: TevBitBtn
    Left = 92
    Top = 310
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Cancel'
    TabOrder = 2
    OnClick = evBitBtn2Click
    Color = clBlack
    Margin = 0
    Kind = bkCancel
  end
  object evDBGrid1: TevDBGrid
    Left = 8
    Top = 8
    Width = 444
    Height = 292
    DisableThemesInTitle = False
    ControlType.Strings = (
      'AGENCY_NUMBER;CustomEdit;wwlcSY_Local_Reporting_Agency_Nbr;F')
    Selected.Strings = (
      'name'#9'21'#9'Name'#9'F'
      'AgencyName'#9'19'#9'Agency'#9'F'
      'CountyName'#9'21'#9'County'#9'F'
      'LOCAL_TYPE'#9'4'#9'Type'#9'F'
      'PSDCode'#9'20'#9'PSD Code'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TChoseLocal\evDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = evDataSource1
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnDblClick = evDBGrid1DblClick
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object evDataSource1: TevDataSource
    Left = 200
    Top = 312
  end
end
