// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_CO_LOC;

interface

uses
  SDataStructure, EvComponents, SPD_EDIT_CO_BASE, wwdblook, StdCtrls,
  ExtCtrls, DBCtrls, Mask, wwdbedit, Db, Wwdatsrc, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, Controls, Classes, SysUtils, EvTypes,
  SPD_MiniNavigationFrame, Forms, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, EvExceptions, ActnList, Menus, SPD_JobChoiceQuery,
  Variants, SDataDictsystem;

type
  TEDIT_CO_LOC = class(TEDIT_CO_BASE)
    tshtCompany_Locs: TTabSheet;
    wwdgCompany_Locs: TevDBGrid;
    Label62: TevLabel;
    Label65: TevLabel;
    Label67: TevLabel;
    Label68: TevLabel;
    DBEdit27: TevDBEdit;
    DBEdit28: TevDBEdit;
    DBEdit29: TevDBEdit;
    DBEdit31: TevDBEdit;
    Label66: TevLabel;
    DBEdit30: TevDBEdit;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;

    procedure editChange(Sender: TObject);    
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;

  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

uses
  EvUtils;

{$R *.DFM}

procedure TEDIT_CO_LOC.editChange(Sender: TObject);
var
  SenderValue: string;
begin
  inherited;

  SenderValue := '';
  if Sender is TevEdit then
    SenderValue := TevEdit(Sender).Text
  else if Sender is TevDateTimePicker then
    if TevDateTimePicker(Sender).Checked then
      SenderValue := FormatDateTime('yyyymmdd', TevDateTimePicker(Sender).Date);

end;

procedure TEDIT_CO_LOC.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);
end;

function TEDIT_CO_LOC.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_LOCATIONS;
end;

procedure TEDIT_CO_LOC.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SYSTEM_LOCAL.SY_GLOBAL_AGENCY, SupportDataSets);
  AddDSWithCheck(DM_COMPANY.CO_LOCATIONS, EditDataSets, '');
end;

procedure TEDIT_CO_LOC.Activate;
begin
  inherited;
end;

initialization
  RegisterClass(TEDIT_CO_LOC);

end.
