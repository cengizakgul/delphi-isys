inherited EDIT_CO_SHIFTS: TEDIT_CO_SHIFTS
  inherited Panel1: TevPanel
    inherited pnlTopLeft: TevPanel
      inherited dbtxClientNbr: TevDBText
        Height = 13
      end
      inherited dbtxClientName: TevDBText
        Height = 13
      end
      inherited DBText1: TevDBText
        Height = 13
      end
      inherited CompanyNameText: TevDBText
        Height = 13
      end
    end
  end
  inherited PageControl1: TevPageControl
    HelpContext = 30001
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 300
                    Align = alLeft
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 288
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 306
                    Width = 484
                    Align = alClient
                    Visible = True
                    object fpShiftsRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 472
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      OnResize = fpShiftsRightResize
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Shifts'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Width = 343
              Height = 419
              Visible = True
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 283
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 283
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 233
                Height = 394
                IniAttributes.SectionName = 'TEDIT_CO_SHIFTS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 286
              Width = 85
              Align = alClient
              Visible = True
              Title = 'Shifts'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 35
                Height = 394
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'38'#9'Name'#9'F'
                  'DEFAULT_AMOUNT'#9'17'#9'Default Amount'#9'F'
                  'DEFAULT_PERCENTAGE'#9'20'#9'Default Percentage'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SaveToRegistry = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_SHIFTS\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              inline CopyHelper: TCompanyCopyHelper
                Left = 16
                Top = 112
                Width = 114
                Height = 54
                TabOrder = 1
                Visible = False
              end
            end
          end
        end
      end
    end
    object tshtShifts: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object fpSummary: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 314
        Height = 209
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'fpSummary'
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Summary'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object wwdgShifts: TevDBGrid
          Left = 12
          Top = 35
          Width = 278
          Height = 152
          TabStop = False
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'40'#9'Shift Name')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CO_SHIFTS\wwdgShifts'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsDetail
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
      object fpDetail: TisUIFashionPanel
        Left = 8
        Top = 225
        Width = 314
        Height = 172
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'fpSummary'
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Shift Detail'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object lablName: TevLabel
          Left = 12
          Top = 35
          Width = 35
          Height = 13
          Caption = '~Name'
          FocusControl = dedtName
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablE_D_Group: TevLabel
          Left = 12
          Top = 74
          Width = 52
          Height = 13
          Caption = 'E/D Group'
          FocusControl = wwlcE_D_Group
        end
        object lablDefault_Amount: TevLabel
          Left = 12
          Top = 113
          Width = 73
          Height = 13
          Caption = 'Default Amount'
        end
        object lablDefault_Percentage: TevLabel
          Left = 155
          Top = 113
          Width = 45
          Height = 13
          Caption = 'Default %'
        end
        object dedtName: TevDBEdit
          Left = 12
          Top = 50
          Width = 278
          Height = 21
          DataField = 'NAME'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwlcE_D_Group: TevDBLookupCombo
          Left = 12
          Top = 89
          Width = 135
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME')
          DataField = 'CL_E_D_GROUPS_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
          LookupField = 'CL_E_D_GROUPS_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
        end
        object wwdeDefault_Amount: TevDBEdit
          Left = 12
          Top = 128
          Width = 135
          Height = 21
          DataField = 'DEFAULT_AMOUNT'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '[-]*12[#][.*6[#]]'
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwdeDefault_Percentage: TevDBEdit
          Left = 155
          Top = 128
          Width = 135
          Height = 21
          DataField = 'DEFAULT_PERCENTAGE'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '[-]*12[#][.*6[#]]'
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object rgAutocreate: TevDBRadioGroup
          Left = 155
          Top = 74
          Width = 135
          Height = 37
          HelpContext = 14516
          Caption = '~Autocreate On New Hire'
          Columns = 2
          DataField = 'AUTO_CREATE_ON_NEW_HIRE'
          DataSource = wwdsDetail
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 2
          Values.Strings = (
            'Y'
            'N')
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_SHIFTS.CO_SHIFTS
  end
end
