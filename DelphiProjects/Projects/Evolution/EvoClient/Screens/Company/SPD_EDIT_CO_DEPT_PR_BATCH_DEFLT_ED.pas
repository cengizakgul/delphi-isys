// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_DEPT_PR_BATCH_DEFLT_ED;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BRCH_PR_BATCH_DEFLT_ED, Db, Wwdatsrc, DBCtrls, wwdblook,
  StdCtrls, ExtCtrls, Mask, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  SDataStructure,  wwdbedit, EvUIComponents, EvUtils, EvClientDataSet,
  SDDClasses, SDataDictclient, ImgList, ISBasicClasses, SDataDicttemp,
  isUIwwDBEdit, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED = class(TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED)
    Panel5: TevPanel;
    Splitter3: TevSplitter;
    wwDBGrid4: TevDBGrid;
    procedure fpDivisionRightResize(Sender: TObject);
    procedure pnlSubbrowseResize(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetDataSetConditions(sName: string): string; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses SPD_EDIT_CO_DIV_PR_BATCH_DEFLT_ED, SPD_EDIT_Open_BASE;

{$R *.DFM}

function TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED.GetDataSetConditions(
  sName: string): string;
begin
  if (sName = 'CO_DEPT_PR_BATCH_DEFLT_ED') or
     (sName = 'CO_BRANCH') or
     (sName = 'CO_DEPARTMENT') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

function TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_DEPT_PR_BATCH_DEFLT_ED;
end;

procedure TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_DIVISION, SupportDataSets);
  AddDS(DM_COMPANY.CO_BRANCH, SupportDataSets);
  AddDS(DM_COMPANY.CO_DEPARTMENT, SupportDataSets);
end;

procedure TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  inherited;
end;

procedure TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED.Activate;
begin
  wwdsDetail.DataSet := GetDefaultDataSet;
  inherited;
end;

constructor TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENT
  pnlSubbrowse.Parent := fpDivisionRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED.fpDivisionRightResize(
  Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpDivisionRight.Width - 40;
  pnlSubbrowse.Height := fpDivisionRight.Height - 65;
end;

procedure TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED.pnlSubbrowseResize(
  Sender: TObject);
begin
  inherited;
  wwDBGrid1.Width := pnlSubbrowse.Width div 4;
  wwDBGrid3.Width := pnlSubbrowse.Width div 4;
end;

initialization
  RegisterClass(TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED);

end.
