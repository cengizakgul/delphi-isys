// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_TIME_OFF_ACCRUAL_RATES;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, StdCtrls, Mask, wwdbedit,
  Wwdbspin, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, Controls, Classes, Forms, sCompanyCopyHelper,
  sCopyHelper, Variants, SDDClasses, ISBasicClasses, SDataDictclient,
  SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_TIME_OFF_ACCRUAL_RATES = class(TEDIT_CO_BASE)
    ctrlDetail: TTabSheet;
    wwdsSubMaster: TevDataSource;
    evDBText1: TevDBText;
    evLabel1: TevLabel;
    CopyHelper: TCompanyCopyHelper;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    sbAccrualRates: TScrollBox;
    fpCOTOARatesSummary: TisUIFashionPanel;
    DBGridDetail: TevDBGrid;
    fpCOTOARateDetails: TisUIFashionPanel;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    Label6: TevLabel;
    Label4: TevLabel;
    DBSpinMinimumMonthNumber: TevDBSpinEdit;
    DBSpinMaximumMonthNumber: TevDBSpinEdit;
    DBSpinHours: TevDBSpinEdit;
    DBSpinAnnualAccrualMaximum: TevDBSpinEdit;
    DBSpinMaximumCarryover: TevDBSpinEdit;
    evBitBtn1: TevBitBtn;
    wwDBGridTimeOffAccrual: TevDBGrid;
    procedure evBitBtn1Click(Sender: TObject);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
  private
    currentDesc: string;
    procedure HandleCopy(Sender: TObject; client, company: integer;
      selectedDetails, details: TEvClientDataSet);
    procedure HandleDelete(Sender: TObject; client, company: integer;
      selectedDetails, details: TEvClientDataSet);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure AfterDataSetsReopen; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;

  end;

implementation

uses
  spackageentry, sysutils, evutils, sCopier, dialogs;
{$R *.DFM}

//gdy
procedure TEDIT_CO_TIME_OFF_ACCRUAL_RATES.Activate;
var
  dummy: boolean;
begin
  inherited;
  CopyHelper.Grid := DBGridDetail; //gdy
//  CopyHelper.DeleteEnabled := false;
  CopyHelper.UserFriendlyName := 'Time off accrual rate'; //gdy
  CopyHelper.OnCopyDetail := HandleCopy; //gdy
  CopyHelper.OnDeleteDetail := HandleDelete; //gdy
  GetDataSetsToReopen( CopyHelper.FDSToSave,  dummy );
//  CopyHelper.SetDSToSave( [DM_COMPANY.CO_TIME_OFF_ACCRUAL ] ); //gdy
end;

function TEDIT_CO_TIME_OFF_ACCRUAL_RATES.GetDataSetConditions(
  sName: string): string;
begin
  if sName = 'CO_TIME_OFF_ACCRUAL_RATES' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL_RATES.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_TIME_OFF_ACCRUAL, aDS);
  inherited;
end;

function TEDIT_CO_TIME_OFF_ACCRUAL_RATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_TIME_OFF_ACCRUAL_RATES;
end;

function TEDIT_CO_TIME_OFF_ACCRUAL_RATES.GetInsertControl: TWinControl;
begin
  Result := DBSpinMinimumMonthNumber;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL_RATES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_TIME_OFF_ACCRUAL, SupportDataSets);
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL_RATES.evBitBtn1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO_TIME_OFF_ACCRUAL')

end;


//gdy
procedure TEDIT_CO_TIME_OFF_ACCRUAL_RATES.HandleCopy(Sender: TObject; client,
  company: integer; selectedDetails, details: TEvClientDataSet);
var
  msg: string;
  toa: integer;
  v: Variant;
  k: integer;
  fn: string;
begin

  DM_COMPANY.CO_TIME_OFF_ACCRUAL.EnsureDSCondition('CO_NBR='+IntTostr(company)  );
  v := DM_COMPANY.CO_TIME_OFF_ACCRUAL.Lookup( 'DESCRIPTION', currentDesc, 'CO_TIME_OFF_ACCRUAL_NBR');
  if not VarIsNull( v ) then
  begin
    toa := v;
    if details.Locate('CO_TIME_OFF_ACCRUAL_NBR;MINIMUM_MONTH_NUMBER;MAXIMUM_MONTH_NUMBER;HOURS',
               VarArrayOf([toa,
                           selecteddetails.FieldByNAme('MINIMUM_MONTH_NUMBER').AsString,
                           selecteddetails.FieldByNAme('MAXIMUM_MONTH_NUMBER').AsString,
                           selecteddetails.FieldByNAme('HOURS').AsString]), []) then
      details.Edit
    else
      details.Insert;
    for k := 0 to SelectedDetails.Fields.Count - 1 do
    begin
      fn := Selecteddetails.Fields[k].FieldName;
      if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) and ( fn <> 'CO_TIME_OFF_ACCRUAL_NBR' )  then
        details[fn] := selectedDetails[fn];
    end;
    details['CO_TIME_OFF_ACCRUAL_NBR'] := toa;
    details.Post;
  end
  else
  begin
    msg := Format( 'Cannot copy %s to company %s because Time Off Accrual %s doesn''t exist',
           [ 'the rate',
             (Sender as TCompanyDetailCopier).TargetCompanyNumber,
             currentDesc  ] );
    EvErrMessage(msg);
  end;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL_RATES.HandleDelete(Sender: TObject; client,
  company: integer; selectedDetails, details: TEvClientDataSet);
var
  toa: integer;
  v: Variant;
begin
  DM_COMPANY.CO_TIME_OFF_ACCRUAL.EnsureDSCondition('CO_NBR='+IntTostr(company)  );
//  ODS('looking for <%s>',[VarToStr(selectedDetails['CustomEDCodeNumber'])]);
  v := DM_COMPANY.CO_TIME_OFF_ACCRUAL.Lookup( 'DESCRIPTION', currentDesc, 'CO_TIME_OFF_ACCRUAL_NBR');
  if not VarIsNull( v ) then
  begin
    toa := v;
    if details.Locate('CO_TIME_OFF_ACCRUAL_NBR;MINIMUM_MONTH_NUMBER;MAXIMUM_MONTH_NUMBER;HOURS',
               VarArrayOf([toa,
                           selecteddetails.FieldByNAme('MINIMUM_MONTH_NUMBER').AsString,
                           selecteddetails.FieldByNAme('MAXIMUM_MONTH_NUMBER').AsString,
                           selecteddetails.FieldByNAme('HOURS').AsString]), []) then
      details.Delete;
  end;
end;
//end gdy

procedure TEDIT_CO_TIME_OFF_ACCRUAL_RATES.AfterDataSetsReopen;
begin
  inherited;
  wwdsSubMaster.DoDataChange( Self, nil );  //gdy14
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL_RATES.wwdsSubMasterDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  currentDesc := DM_COMPANY.CO_TIME_OFF_ACCRUAL.DESCRIPTION.AsString;
end;

initialization
  RegisterClass(TEDIT_CO_TIME_OFF_ACCRUAL_RATES);

end.
