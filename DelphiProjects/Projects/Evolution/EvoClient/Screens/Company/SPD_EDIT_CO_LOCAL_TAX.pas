// Copyright � 2000-2004 iSystems LLC. All rights reserved.
{ FILLER fields description CO_LOCAL_TAX
    1-10: Life to Date Amount
}

unit SPD_EDIT_CO_LOCAL_TAX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, wwdbedit, Wwdotdot, Wwdbcomb,
  StdCtrls, ExtCtrls, DBCtrls, wwdblook, Mask, Grids, Wwdbigrd, Wwdbgrid,
  Buttons,  wwdbdatetimepicker, SDataStructure, Variants, EvContext,
   SPackageEntry, EvUtils, EvSecElement, EvTypes, ActnList, menus,
  SDDClasses, ISBasicClasses, SDataDictsystem, SDataDictclient,
  SDataDicttemp,SfieldCodeValues,EvConsts,sCompanyShared, SDataDictbureau,
  EvExceptions,SPD_EmployeeChoiceQuery, EvDataAccessComponents, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_LOCAL_TAX = class(TEDIT_CO_BASE)
    tshtDetail: TTabSheet;
    Panel4: TevPanel;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    SyLocalsSrc: TevDataSource;
    SyLocalsFreqSrc: TevDataSource;
    evSecElement1: TevSecElement;
    evPopupMenu1: TevPopupMenu;
    CopyTDF: TMenuItem;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    pmLocalTaxCopyToEE: TPopupMenu;
    mnLocalTaxToCopyEE: TMenuItem;
    pmLocalTaxCopyTaxRate: TPopupMenu;
    mnCopyTaxRate: TMenuItem;
    fpLocalsRight: TisUIFashionPanel;
    sbLocalTax: TScrollBox;
    fpLocalTax: TisUIFashionPanel;
    lablLocal: TevLabel;
    lablEIN_Number: TevLabel;
    lablTax_Return_Code: TevLabel;
    lablPayment_Method: TevLabel;
    lablEFT_Name: TevLabel;
    lablEFT_PIN_Number: TevLabel;
    lablEFT_Status: TevLabel;
    Label1: TevLabel;
    Label2: TevLabel;
    dedtEIN_Number: TevDBEdit;
    dedtTax_Return_Code: TevDBEdit;
    dedtEFT_Name: TevDBEdit;
    dedtEFT_PIN_Number: TevDBEdit;
    wwcbPayment_Method: TevDBComboBox;
    wwcbEFT_Status: TevDBComboBox;
    wwlcLocal: TevDBLookupCombo;
    DBEdit1: TevDBEdit;
    DBEdit2: TevDBEdit;
    evSpeedButton1: TevSpeedButton;
    cbAppliedFor: TevDBCheckBox;
    fpRates: TisUIFashionPanel;
    lablDeposit_Freq: TevLabel;
    lablMiscellaneous_Amount: TevLabel;
    evLabel1: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    wwdeMiscellaneous_Amount: TevDBEdit;
    evDBEdit1: TevDBEdit;
    lablTax_Rate: TevLabel;
    wwdeTax_Rate: TevDBEdit;
    butnAttachLocal: TevBitBtn;
    fpOptions: TisUIFashionPanel;
    lablCTSCode: TevLabel;
    evLabel2: TevLabel;
    drgpFinal_Tax_Return: TevDBRadioGroup;
    drgpExempt: TevDBRadioGroup;
    rgActive: TevDBRadioGroup;
    rgSelfAdjustTax: TevDBRadioGroup;
    rgAutocreate: TevDBRadioGroup;
    editCTSCode: TevEdit;
    evDBLookupCombo1: TevDBLookupCombo;
    rgDeduct: TevDBRadioGroup;
    evLabel7: TevLabel;
    wwDBGrid3: TevDBGrid;
    popOPTClientED: TevPopupMenu;
    MenuItem1: TMenuItem;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwlcLocalChange(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure butnAttachLocalClick(Sender: TObject);
    procedure CopyTDFClick(Sender: TObject);
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure editCTSCodeChange(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure wwcbPayment_MethodDropDown(Sender: TObject);
    procedure wwcbPayment_MethodChange(Sender: TObject);
    procedure mnLocalTaxToCopyEEClick(Sender: TObject);
    procedure CopyLocalTaxToEE;
    procedure mnCopyTaxRateClick(Sender: TObject);
    procedure fpLocalsRightResize(Sender: TObject);
    procedure wwlcLocalCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
  private
    procedure FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    function CanChangeOrDelete: Boolean;
    procedure AttachLocalToPayrolls(COLocalNbr: Integer; BeginCheckDate,
      EndCheckDate: TDateTime);
    procedure CopyLocalTaxDeposit(const field: String);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure AfterDataSetsReopen; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;

  public
    GL_Setuped:boolean;
    constructor Create(AOwner: TComponent); override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;    
  end;

implementation

{$R *.DFM}

uses SSecurityInterface, SPD_CompanyChoiceQuery, ChoseLocalDlg;

procedure TEDIT_CO_LOCAL_TAX.Activate;
begin
  GL_Setuped := false;
  DM_SYSTEM_LOCAL.SY_LOCALS.OnFilterRecord := FilterRecord;
  DM_SYSTEM_LOCAL.SY_LOCALS.Filtered := True;
  DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'], []);
  DM_CLIENT.CO_LOCAL_TAX.Refresh;
  // to avoid evo freezing
  wwdbGrid3.DataSource := wwdsDetail;
  inherited;

end;

procedure TEDIT_CO_LOCAL_TAX.Deactivate;
begin
  DM_SYSTEM_LOCAL.SY_LOCALS.Filtered := False;
  DM_SYSTEM_LOCAL.SY_LOCALS.OnFilterRecord := nil;
  inherited;
end;

procedure TEDIT_CO_LOCAL_TAX.FilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := not VarIsNull(DM_COMPANY.CO_STATES.Lookup('STATE',
                     DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR',
                     DataSet['SY_STATES_NBR'], 'STATE'), 'STATE'));
end;

function TEDIT_CO_LOCAL_TAX.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_LOCAL_TAX;
end;

function TEDIT_CO_LOCAL_TAX.GetInsertControl: TWinControl;
begin
  Result := wwlcLocal;
end;

procedure TEDIT_CO_LOCAL_TAX.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_LOCAL.SY_LOCALS, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ, SupportDataSets, '');
  AddDS(DM_CLIENT.CL_E_DS, SupportDataSets);
  AddDS(DM_COMPANY.CO_STATES, SupportDataSets);
end;

procedure TEDIT_CO_LOCAL_TAX.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  wwDBLookupCombo1.Enabled := not DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_LOCALS_NBR').IsNull;
  if (wwdsDetail.DataSet <> nil) and (wwdsDetail.DataSet.State = dsBrowse) then
    editCTSCode.Text := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 10);

  if not Assigned(Field) and (wwdsDetail.DataSet <> nil) and (wwdsDetail.DataSet.State = dsBrowse) then
  begin
     wwcbPayment_MethodDropDown(wwcbPayment_Method);
  end;
end;

procedure TEDIT_CO_LOCAL_TAX.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);
  DM_SYSTEM_LOCAL.SY_LOCALS.Filtered := False;
  inherited;
end;

procedure TEDIT_CO_LOCAL_TAX.AfterDataSetsReopen;
begin
  inherited;
  DM_SYSTEM_LOCAL.SY_LOCALS.Filtered := True;
  DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'], []);
end;

procedure TEDIT_CO_LOCAL_TAX.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  case Kind of
    NavInsert:
      GL_Setuped := CheckGlSetupExist(DM_CLIENT.CO_LOCAL_TAX,['GENERAL_LEDGER_TAG','OFFSET_GL_TAG'],false);
    NavDelete:
    begin
      if not CanChangeOrDelete then
      begin
        DM_COMPANY.CO_LOCAL_TAX.Cancel;
        raise EUpdateError.CreateHelp('Cannot delete this local tax because it''s in use!', IDH_ConsistencyViolation);
      end;
    end;
    NavOk:
    begin
     if (DM_CLIENT.CO_LOCAL_TAX.State in [dsEdit,dsInsert]) then
     begin
       wwdsDetail.DataSet.UpdateRecord;
     end;
     if DM_CLIENT.CO_LOCAL_TAX.SY_STATES_NBR.Value <> DM_SYSTEM_STATE.SY_STATES.SY_STATES_NBR.Value then
         DM_SYSTEM_STATE.SY_STATES.locate('SY_STATES_NBR', DM_CLIENT.CO_LOCAL_TAX.SY_STATES_NBR.Value, []);
     if not CheckPaymentMethodsChoice(DM_CLIENT.CO_LOCAL_TAX.PAYMENT_METHOD,DM_SYSTEM_STATE.SY_STATES.STATE_EFT_TYPE)
       and not (DM_SERVICE_BUREAU.SB.FieldByName('PARENT_SB_MODEM_NUMBER').Value = 'W3LL5') then
     begin
        handled :=  false;
        raise EUpdateError.CreateHelp('Wrong payment method.', IDH_ConsistencyViolation);
     end;
     if    (DM_CLIENT.CO.CALCULATE_LOCALS_FIRST.Value = GROUP_BOX_YES) then
     begin
       DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR',DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'],[]);
       if (DM_SYSTEM_LOCAL.SY_LOCALS.CALCULATION_METHOD.Value = CALC_METHOD_PERC_MEDICARE_WAGES)   then
       begin
          raise EUpdateError.CreateHelp('Cannot post this local tax, because the company has Calculate Locals First = Yes!', IDH_ConsistencyViolation);
       end;
     end;
     if GL_Setuped  and not CheckGlSetuped(DM_CLIENT.CO_LOCAL_TAX,['GENERAL_LEDGER_TAG','OFFSET_GL_TAG']) then
           EvMessage(sDontForgetSetupGL);
      GL_Setuped := false;
    end;
  end;
end;

procedure TEDIT_CO_LOCAL_TAX.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind in [NavInsert, NavCancel, NavCommit, NavAbort] then
     ApplySecurity;
end;

function TEDIT_CO_LOCAL_TAX.CanChangeOrDelete: Boolean;
  function CheckLocal(TableName: string): Integer;
  begin
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('TableName', TableName);
      SetMacro('Columns', 'count(*)');
      SetMacro('NbrField', 'CO_LOCAL_TAX');
      SetParam('RecordNbr', DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.Open;
      Result := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger;
    end;
  end;
begin
  if (CheckLocal('CO_DEPARTMENT_LOCALS') > 0) or (CheckLocal('CO_BRANCH_LOCALS') > 0) or (CheckLocal('CO_BATCH_LOCAL_OR_TEMPS') > 0) or
     (CheckLocal('CO_DIVISION_LOCALS') > 0) or (CheckLocal('CO_LOCAL_TAX_LIABILITIES') > 0) or (CheckLocal('CO_TEAM_LOCALS') > 0) or
     (CheckLocal('EE_LOCALS') > 0) then
  begin
    result := False;
  end
  else
    result := True;
end;

procedure TEDIT_CO_LOCAL_TAX.wwlcLocalChange(Sender: TObject);
begin
  inherited;
  if wwlcLocal.Focused and (DM_COMPANY.CO_LOCAL_TAX.State <> dsBrowse) and (DM_COMPANY.CO_LOCAL_TAX.State <> dsInsert) then
  if not CanChangeOrDelete then
  begin
    DM_COMPANY.CO_LOCAL_TAX.Cancel;
    raise EUpdateError.CreateHelp('Cannot change this local tax because it''s in use!', IDH_ConsistencyViolation);
  end;

  if (DM_COMPANY.CO_LOCAL_TAX.State = dsInsert) and
      (not DM_COMPANY.CO_LOCAL_TAX.SY_LOCAL_DEPOSIT_FREQ_NBR.IsNull) then
    DM_COMPANY.CO_LOCAL_TAX.SY_LOCAL_DEPOSIT_FREQ_NBR.Clear;
end;

procedure TEDIT_CO_LOCAL_TAX.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if (wwdsDetail.DataSet <> nil) and (wwdsDetail.DataSet.State = dsInsert) then
    editCTSCode.Text := '';
  wwlcLocal.Enabled := wwdsDetail.DataSet.State = dsInsert;
  evSpeedButton1.Enabled := wwlcLocal.Enabled;
end;

procedure TEDIT_CO_LOCAL_TAX.butnAttachLocalClick(Sender: TObject);
var
  BeginDate, EndDate: String;
begin
  inherited;
  if EvMessage('This operation can violate the integrity of your data. Would you like to proceed?', mtWarning, [mbYes, mbNo]) = mrNo then
    Exit;
  if not EvDialog('Input required', 'Please, enter begin check date', BeginDate) then
    Exit;
  if not EvDialog('Input required', 'Please, enter end check date', EndDate) then
    Exit;
  ctx_DataAccess.CheckCompanyLock(DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_NBR').AsInteger, StrToDate(BeginDate));
  AttachLocalToPayrolls(DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger, StrToDate(BeginDate), StrToDate(EndDate));
end;

procedure TEDIT_CO_LOCAL_TAX.AttachLocalToPayrolls(COLocalNbr: Integer;
  BeginCheckDate, EndCheckDate: TDateTime);
var
  AllOK: Boolean;
begin
  ctx_StartWait;
  with DM_PAYROLL do
  try
    PR.DataRequired('ALL');
    PR_CHECK_LOCALS.DataRequired('ALL');
    DM_EMPLOYEE.EE_LOCALS.DataRequired('ALL');
    AllOK := False;
    try
      PR.First;
      while not PR.EOF do
      begin
        if (PR.FieldByName('CHECK_DATE').AsDateTime >= BeginCheckDate) and (PR.FieldByName('CHECK_DATE').AsDateTime <= EndCheckDate) then
        begin
          ctx_DataAccess.UnlockPR;
          PR_CHECK.DataRequired('PR_NBR=' + PR.FieldByName('PR_NBR').AsString);
          PR_CHECK.First;
          while not PR_CHECK.EOF do
          begin
            if DM_EMPLOYEE.EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([PR_CHECK.FieldByName('EE_NBR').Value, COLocalNbr]), []) then
            if not DM_PAYROLL.PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK.FieldByName('PR_CHECK_NBR').Value, DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').Value]), []) then
            begin
              DM_PAYROLL.PR_CHECK_LOCALS.Insert;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_NBR').Value := PR_CHECK.FieldByName('PR_NBR').Value;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value := DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').Value;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value := 0;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value := 0;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').Value := 0;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EXCLUDE_LOCAL').Value := 'N';
              DM_PAYROLL.PR_CHECK_LOCALS.Post;
            end;
            PR_CHECK.Next;
          end;
        end;
        PR.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK_LOCALS]);
      AllOK := True;
    finally
      PR.First;
      while not PR.EOF do
      begin
        if (PR.FieldByName('CHECK_DATE').AsDateTime >= BeginCheckDate) and (PR.FieldByName('CHECK_DATE').AsDateTime <= EndCheckDate) then
          ctx_DataAccess.LockPR(AllOK);
        PR.Next;
      end;
    end;
  finally
    ctx_EndWait;
  end;
end;

constructor TEDIT_CO_LOCAL_TAX.Create(AOwner: TComponent);
begin
  inherited;
  evSecElement1.AtomComponent := nil;
  //GUI 2.0 PARENTAL REASSIGNMENTS
  pnlSubbrowse.Parent := fpLocalsRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 18;
end;

procedure TEDIT_CO_LOCAL_TAX.CopyLocalTaxDeposit(const field: String);
var
  CurClient: integer;
  CurCompany: integer;
  CurLocal: integer;
  Local: integer;

  ClientRetrieveCondition: string;
  CompanyRetrieveCondition: string;
  LocalsRetrieveCondition: string;

  procedure DoCopyTaxDeposit;
  var
    copyField : Variant;
    k: integer;

    procedure DoCopyTaxDepositTo( client, company: integer );
    begin
      ctx_DataAccess.OpenClient( client );

      DM_COMPANY.CO.Close;
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + intToStr(company) );
      if DM_COMPANY.CO_LOCAL_TAX.Locate( 'SY_LOCALS_NBR', local,[] ) then
      begin
        DM_COMPANY.CO_LOCAL_TAX.Edit;
        DM_COMPANY.CO_LOCAL_TAX[field] := copyField;
        DM_COMPANY.CO_LOCAL_TAX.Post;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_LOCAL_TAX]);
    end;
  begin
    with TCompanyChoiceQuery.Create( Self ) do
    try
      SetFilter( 'CUSTOM_COMPANY_NUMBER<>'''+DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'''' );
      if field = 'SY_LOCAL_DEPOSIT_FREQ_NBR' then
         Caption := 'Select the companies you want this Tax Deposit Frequency copied to'
      else
         Caption := 'Select the companies you want this Override Tax Rate copied to';

      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      if field = 'SY_LOCAL_DEPOSIT_FREQ_NBR' then
         ConfirmAllMessage := 'This operation will copy the Tax Deposit Frequency to all clients and companies. Are you sure you want to do this?'
      else
         ConfirmAllMessage := 'This operation will copy the Override Tax Rate to all clients and companies. Are you sure you want to do this?';


      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        DM_COMPANY.CO_LOCAL_TAX.DisableControls;
        try
          copyField := DM_COMPANY.CO_LOCAL_TAX[field];
          Local := DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'];
          DM_TEMPORARY.TMP_CO.DisableControls;
          try
            for k := 0 to dgChoiceList.SelectedList.Count - 1 do
            begin
              cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
              DoCopyTaxDepositTo( cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger );
            end;
          finally
            DM_TEMPORARY.TMP_CO.EnableControls;
          end;
        finally
          DM_COMPANY.CO_LOCAL_TAX.EnableControls;
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  DM_COMPANY.CO_LOCAL_TAX.Cancel;

  CurClient := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  ClientRetrieveCondition := DM_CLIENT.CL.RetrieveCondition;

  CurCompany := DM_COMPANY.CO['CO_NBR'];
  CompanyRetrieveCondition := DM_COMPANY.CO.RetrieveCondition;

  CurLocal := DM_COMPANY.CO_LOCAL_TAX['CO_LOCAL_TAX_NBR'];
  LocalsRetrieveCondition := DM_COMPANY.CO_LOCAL_TAX.RetrieveCondition;
  try
    DoCopyTaxDeposit;
  finally
    ctx_DataAccess.OpenClient( CurClient );

    DM_CLIENT.CL.DataRequired(ClientRetrieveCondition );
    DM_COMPANY.CO.DataRequired(CompanyRetrieveCondition );
    DM_COMPANY.CO_LOCAL_TAX.DataRequired(LocalsRetrieveCondition);

    wwdsList.dataset.Locate('CO_NBR;CL_NBR', VarArrayOf([CurCompany, CurClient ]), []);
    DM_COMPANY.CO.Locate('CO_NBR',CurCompany,[]);
    DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR',CurLocal,[]);

  end;
end;

procedure TEDIT_CO_LOCAL_TAX.CopyTDFClick(Sender: TObject);
begin
  CopyLocalTaxDeposit('SY_LOCAL_DEPOSIT_FREQ_NBR');
end;

procedure TEDIT_CO_LOCAL_TAX.mnCopyTaxRateClick(Sender: TObject);
begin
  inherited;
  CopyLocalTaxDeposit('TAX_RATE');
end;


procedure TEDIT_CO_LOCAL_TAX.wwdsDetailUpdateData(Sender: TObject);
begin
  inherited;
  if ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 10) <> editCTSCode.Text then
    wwdsDetail.DataSet.FieldByName('FILLER').AsString :=
      PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, editCTSCode.Text, 1, 10);
end;

procedure TEDIT_CO_LOCAL_TAX.editCTSCodeChange(Sender: TObject);
begin
  inherited;
  with wwdsDetail.DataSet do
  if (ExtractFromFiller(FieldByName('FILLER').AsString, 1, 10) <> TevEdit(Sender).Text)
  or ((TevEdit(Sender).Text = '') and (ExtractFromFiller(FieldByName('FILLER').AsString, 1, 10) <> ''))
  and (State = dsBrowse) then
    Edit;
end;

procedure TEDIT_CO_LOCAL_TAX.evSpeedButton1Click(Sender: TObject);
begin
  inherited;
  with TChoseLocal.Create(Self) do
  try
    evDataSource1.DataSet := DM_SYSTEM_LOCAL.SY_LOCALS;
    DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR', DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'], []);
    if ShowModal = mrOk then
    begin
      if DM_COMPANY.CO_LOCAL_TAX.State = dsBrowse then
        DM_COMPANY.CO_LOCAL_TAX.Edit;
      DM_COMPANY.CO_LOCAL_TAX['SY_LOCALS_NBR'] := DM_SYSTEM_LOCAL.SY_LOCALS['SY_LOCALS_NBR'];

      if (DM_COMPANY.CO_LOCAL_TAX.State = dsInsert) and
         (not DM_COMPANY.CO_LOCAL_TAX.SY_LOCALS_NBR.IsNull) then
          wwcbPayment_MethodDropDown(wwcbPayment_Method);
    end
    else
    if wwlcLocal.LookupValue <> '' then
      DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR', wwlcLocal.LookupValue, []);
  finally
    Free;
  end;
end;

procedure TEDIT_CO_LOCAL_TAX.wwcbPayment_MethodDropDown(Sender: TObject);
begin
  inherited;

  if (DM_CLIENT.CO_LOCAL_TAX.SY_STATES_NBR.Value <> DM_SYSTEM_STATE.SY_STATES.SY_STATES_NBR.Value) and (DM_SYSTEM_STATE.SY_STATES.Active)  then
  begin
     DM_SYSTEM_LOCAL.SY_LOCALS.locate('SY_LOCALS_NBR', DM_CLIENT.CO_LOCAL_TAX.SY_LOCALS_NBR.Value, []);
     DM_SYSTEM_STATE.SY_STATES.locate('SY_STATES_NBR', DM_SYSTEM_LOCAL.SY_LOCALS.SY_STATES_NBR.Value, []);
  end;
  if DM_SERVICE_BUREAU.SB.FieldByName('PARENT_SB_MODEM_NUMBER').Value <> 'W3LL5' then
    FilterPaymentMethodsChoices( Sender as TevDBComboBox,DM_SYSTEM_STATE.SY_STATES.STATE_EFT_TYPE);
end;

procedure TEDIT_CO_LOCAL_TAX.wwcbPayment_MethodChange(Sender: TObject);
begin
  inherited;
  wwcbPayment_Method.UpdateRecord();
end;

procedure TEDIT_CO_LOCAL_TAX.mnLocalTaxToCopyEEClick(Sender: TObject);
begin
  inherited;
  if (wwdsDetail.DataSet.State = dsBrowse) then
     CopyLocalTaxToEE;
end;

procedure TEDIT_CO_LOCAL_TAX.CopyLocalTaxToEE;
var
  k: integer;
  CoLocalNbr : integer;

  procedure DoTransfer;
  begin
    if DM_CLIENT.CO_STATES.Locate('STATE', wwdsDetail.DataSet.fieldByName('LocalState').Value, []) then
     if DM_CLIENT.CO_STATES['STATE_NON_PROFIT'] = GROUP_BOX_YES then
        exit;

    DM_EMPLOYEE.EE_LOCALS.Insert;
    try
        DM_EMPLOYEE.EE_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').Value := wwdsDetail.DataSet.fieldByName('CO_LOCAL_TAX_NBR').Value;

        if DM_EMPLOYEE.EE['COMPANY_OR_INDIVIDUAL_NAME'] <> 'C' then
          DM_EMPLOYEE.EE_LOCALS.FieldByName('EXEMPT_EXCLUDE').Value := GROUP_BOX_INCLUDE
        else
          DM_EMPLOYEE.EE_LOCALS.FieldByName('EXEMPT_EXCLUDE').Value := GROUP_BOX_EXEMPT;

        DM_EMPLOYEE.EE_LOCALS.FieldByName('MISCELLANEOUS_AMOUNT').Value := wwdsDetail.DataSet.fieldByName('MISCELLANEOUS_AMOUNT').Value;
        DM_EMPLOYEE.EE_LOCALS.FieldByName('FILLER').Value := wwdsDetail.DataSet.fieldByName('FILLER').Value;
        DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_TAX_CODE').Value := wwdsDetail.DataSet.fieldByName('TAX_RETURN_CODE').Value;
        DM_EMPLOYEE.EE_LOCALS.FieldByName('DEDUCT').AsString := GetCoLocalTaxDeduct(wwdsDetail.DataSet.fieldByName('DEDUCT').AsString, wwdsDetail.DataSet.fieldByName('LOCAL_TYPE').AsString );

        if DM_SYSTEM.SY_LOCALS.Locate('SY_LOCALS_NBR', wwdsDetail.DataSet.fieldByName('SY_LOCALS_NBR').Value, []) then
        begin
          DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_COUNTY').AsString :=
             DM_SYSTEM.SY_COUNTY.Lookup('SY_COUNTY_NBR', DM_SYSTEM.SY_LOCALS.FIELDBYNAME('SY_COUNTY_NBR').AsString, 'COUNTY_NAME');
        end;
    finally
      DM_EMPLOYEE.EE_LOCALS.Post;
    end;
  end;

begin

  DM_CLIENT.EE.DataRequired('CO_NBR = ' + wwdsDetail.DataSet.FieldByName('CO_NBR').AsString);
  DM_CLIENT.EE_LOCALS.DataRequired('ALL');
  DM_CLIENT.CO_STATES.DataRequired('CO_NBR = ' + wwdsDetail.DataSet.FieldByName('CO_NBR').AsString);
  DM_SYSTEM.SY_LOCALS.DataRequired('ALL');

  CoLocalNbr := DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
  with TEmployeeChoiceQuery.Create( Self ) do
  try
    SetFilter('');
    dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
    Caption := 'Employees';
    btnNo.Caption := '&Cancel';
    btnYes.Caption := 'C&opy';
    btnAll.Caption := 'Copy to &all';
    ConfirmAllMessage := 'This operation will copy local tax for all employees. Are you sure you want to do this?';
    if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
    begin
      ctx_StartWait('Processing...');
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            if DM_CLIENT.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []) then
              if (not DM_EMPLOYEE.EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([DM_CLIENT.EE.FieldByName('EE_NBR').Value, CoLocalNbr]), [])) then
                DoTransfer
              else
              begin
                DM_EMPLOYEE.EE_LOCALS.Edit;
                DM_EMPLOYEE.EE_LOCALS['DEDUCT'] := GetCoLocalTaxDeduct(wwdsDetail.DataSet.fieldByName('DEDUCT').AsString, wwdsDetail.DataSet.fieldByName('LOCAL_TYPE').AsString );
                DM_EMPLOYEE.EE_LOCALS.Post;
              end;
          end;
        finally
          ctx_EndWait;
          ctx_DataAccess.PostDataSets([DM_CLIENT.EE_LOCALS]);
        end;
    end;
  finally
    Free;
  end;
end;

procedure TEDIT_CO_LOCAL_TAX.fpLocalsRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpLocalsRight.Width - 40;
  pnlSubbrowse.Height := fpLocalsRight.Height - 65;
end;




procedure TEDIT_CO_LOCAL_TAX.wwlcLocalCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
 
  if (DM_COMPANY.CO_LOCAL_TAX.State = dsInsert) and
      (not DM_COMPANY.CO_LOCAL_TAX.SY_LOCALS_NBR.IsNull) then
         wwcbPayment_MethodDropDown(wwcbPayment_Method);
end;

initialization
  RegisterClass(TEDIT_CO_LOCAL_TAX);
end.
