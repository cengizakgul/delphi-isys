inherited EDIT_CO_DEPT_PR_BATCH_DEFLT_ED: TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED
  inherited Panel1: TevPanel
    inherited Label2: TevLabel
      Left = 520
      Width = 55
      Caption = 'Department'
    end
    inherited DBText2: TevDBText
      Width = 257
      DataSource = wwdsSubMaster3
    end
    inherited pnlTopLeft: TevPanel
      inherited pnlUser: TevPanel
        Width = 9
        inherited dbtxUserLast: TevDBText
          Left = 0
        end
      end
    end
  end
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    inherited fpDivisionRight: TisUIFashionPanel
                      Title = 'Department'
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 269
              Width = 491
              inherited Panel4: TevPanel
                Width = 491
                object Panel5: TevPanel
                  Left = 0
                  Top = 0
                  Width = 491
                  Height = 257
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                end
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 372
                IniAttributes.SectionName = 'TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Department'
              inherited Splitter2: TevSplitter
                Left = 128
                Height = 372
              end
              object Splitter3: TevSplitter [1]
                Left = 241
                Top = 48
                Height = 372
              end
              inherited wwDBGrid1: TevDBGrid
                Width = 110
                Height = 372
                IniAttributes.SectionName = 'TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED\wwDBGrid1'
              end
              inherited wwDBGrid3: TevDBGrid
                Left = 131
                Width = 110
                Height = 372
                IniAttributes.SectionName = 'TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED\wwDBGrid3'
                Align = alLeft
              end
              object wwDBGrid4: TevDBGrid
                Left = 244
                Top = 48
                Width = 221
                Height = 372
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'DEPARTMENT')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED\wwDBGrid4'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster3
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 2
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited sbDivisionDetail: TScrollBox
        inherited fpSummary: TisUIFashionPanel
          inherited wwDBGrid2: TevDBGrid
            IniAttributes.SectionName = 'TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED\wwDBGrid2'
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = wwdsSubMaster3
  end
  object wwdsSubMaster3: TevDataSource
    DataSet = DM_CO_DEPARTMENT.CO_DEPARTMENT
    MasterDataSource = wwdsSubMaster2
    Left = 96
  end
end
