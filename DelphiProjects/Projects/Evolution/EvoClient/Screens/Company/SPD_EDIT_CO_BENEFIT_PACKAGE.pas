// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BENEFIT_PACKAGE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBLookupCombo,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, Mask, wwdbedit,
  isUIwwDBEdit, Wwdotdot, Wwdbcomb, isUIwwDBComboBox;

type
  TEDIT_CO_BENEFIT_PACKAGE= class(TEDIT_CO_BASE)
    tshtPensions: TTabSheet;
    wwdgPension: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    fpCOPensionSummary: TisUIFashionPanel;
    fpCOPensionDetails: TisUIFashionPanel;
    lblName: TevLabel;
    sbPensions: TScrollBox;
    edName: TevDBEdit;
  private
    FEnableHr: boolean;
  protected
    function GetIfReadOnly: Boolean; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure AfterDataSetsReopen; override;
  public
    function GetInsertControl: TWinControl; override;

  end;

implementation

{$R *.DFM}

function TEDIT_CO_BENEFIT_PACKAGE.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if (not Result) then
     result:= FEnableHr;
end;

procedure TEDIT_CO_BENEFIT_PACKAGE.AfterDataSetsReopen;
begin
  inherited;
  FEnableHr := GetEnableHrBenefit(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  if GetIfReadOnly then
     SetReadOnly(True)
  else
    ClearReadOnly;
end;

function TEDIT_CO_BENEFIT_PACKAGE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BENEFIT_PACKAGE;
end;

function TEDIT_CO_BENEFIT_PACKAGE.GetInsertControl: TWinControl;
begin
  Result := edName;
end;


initialization
  RegisterClass(TEDIT_CO_BENEFIT_PACKAGE);

end.
