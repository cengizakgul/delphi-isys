// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_REDIT_CO;

interface

uses
   SPD_EDIT_CO_BASE, StdCtrls, Mask, wwdbedit, ExtCtrls,
  SDataStructure, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, DBCtrls, Controls, Classes, EvUtils, wwdblook, EvContext, 
   SFieldCodeValues, EvConsts, SysUtils, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  SDDClasses, ISDataAccessComponents, EvDataAccessComponents,
  SDataDictbureau, SDataDictclient, SDataDicttemp, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TREDIT_CO = class(TEDIT_CO_BASE)
    TabSheet11: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    wwgrCO_E_DS: TevDBGrid;
    TabSheet4: TTabSheet;
    wwgrCO_DBDT: TevDBGrid;
    wwdsCO_States: TevDataSource;
    wwdsCO_SUI: TevDataSource;
    wwdsCO_Local_Tax: TevDataSource;
    wwdsCO_E_DS: TevDataSource;
    wwdsCO_DBDT: TevDataSource;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    CO_DBDT: TevClientDataSet;
    CO_DBDTCUSTOM_NUMBER: TStringField;
    CO_DBDTNAME: TStringField;
    CO_DBDTHOME_STATE: TStringField;
    CO_DBDTPAY_FREQUENCY_HOURLY: TStringField;
    CO_DBDTPAY_FREQUENCY_SALARY: TStringField;
    CO_DBDTOVERRIDE_EE_RATE_NUMBER: TIntegerField;
    CO_DBDTOVERRIDE_PAY_RATE: TCurrencyField;
    CO_DBDTPAYROLL_BANK_ACCOUNT: TStringField;
    CO_DBDTTAX_BANK_ACCOUNT: TStringField;
    CO_DBDTBILLING_BANK_ACCOUNT: TStringField;
    CO_DBDTDD_BANK_ACCOUNT: TStringField;
    CO_DBDTHOME_CO_STATES_NBR: TIntegerField;
    CO_DBDTPAYROLL_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DBDTTAX_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DBDTBILLING_CL_BANK_ACCOUNT_NBR: TIntegerField;
    CO_DBDTDD_CL_BANK_ACCOUNT_NBR: TIntegerField;
    dsCO_E_DS: TevClientDataSet;
    sbAddressAccounts: TScrollBox;
    fpCompany: TisUIFashionPanel;
    lablCompany_Code: TevLabel;
    lablName: TevLabel;
    lablDBA: TevLabel;
    lablAddress1: TevLabel;
    lablAddress_2: TevLabel;
    lablCity: TevLabel;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    lablCounty: TevLabel;
    lablE_Mail_Address: TevLabel;
    lablCustomer_Service_Rep: TevLabel;
    lablCustomer_Service_Team: TevLabel;
    dedtE_Mail_Address: TevDBEdit;
    dedtCompany_Code: TevDBEdit;
    dedtName: TevDBEdit;
    dedtDBA: TevDBEdit;
    dedtAddress_1: TevDBEdit;
    dedtAddress_2: TevDBEdit;
    dedtCity: TevDBEdit;
    dedtState: TevDBEdit;
    dedtZip_Code: TevDBEdit;
    dedtCounty: TevDBEdit;
    dedtCustomer_Service_Team: TevDBLookupCombo;
    dedtCustomer_Service_Rep: TevDBLookupCombo;
    fpLegal: TisUIFashionPanel;
    lablLegal_Name: TevLabel;
    lablLegal_Address_1: TevLabel;
    lablLegal_Address_2: TevLabel;
    lablLegal_City: TevLabel;
    lablLegal_State: TevLabel;
    lablLegal_Zip_Code: TevLabel;
    dedtLegal_Name: TevDBEdit;
    dedtLegal_Address_1: TevDBEdit;
    dedtLegal_Address_2: TevDBEdit;
    dedtLegal_City: TevDBEdit;
    dedtLegal_State: TevDBEdit;
    dedtLegal_Zip_Code: TevDBEdit;
    fpBankAccounts: TisUIFashionPanel;
    lablPayroll_Bank_Account_Number: TevLabel;
    lablTax_Bank_Account_Number: TevLabel;
    lablBilling_Bank_Account_Number: TevLabel;
    lablDD_Bank_Account_Number: TevLabel;
    lablWorkers_Comp_Bank_Account_Number: TevLabel;
    dedtPayroll_Bank_Account: TevDBLookupCombo;
    dedtTax_Bank_Account: TevDBLookupCombo;
    dedtWorkers_Comp_Bank_Account: TevDBLookupCombo;
    dedtBilling_Bank_Account: TevDBLookupCombo;
    dedtDD_Bank_Account: TevDBLookupCombo;
    bvCompany: TEvBevel;
    sbEINandRates: TScrollBox;
    fpEINStates: TisUIFashionPanel;
    wwgrCO_States: TevDBGrid;
    fpSUI: TisUIFashionPanel;
    wwgrCO_SUI: TevDBGrid;
    fpLocals: TisUIFashionPanel;
    wwgrCO_Local_Tax: TevDBGrid;
    lablEIN_Number: TevLabel;
    dedtEIN_Number: TevDBEdit;
    sbEarnDeduct: TScrollBox;
    fpEarnDeduct: TisUIFashionPanel;
    sbDBDT: TScrollBox;
    fpDBDT: TisUIFashionPanel;
    pnlSBBorder: TevPanel;
    fpEDIT_CO_BASE_RightSide: TisUIFashionPanel;
    procedure fpDBDTResize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure fpEDIT_CO_BASE_CompanyResize(Sender: TObject);
  private
    procedure BuildDBDT;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    function GetDataSetConditions(sName: string): string; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

{ TREDIT_CO }

procedure TREDIT_CO.Activate;
begin
  inherited;
  wwdsCO_E_DS.DataSet := dsCO_E_DS;
end;

procedure TREDIT_CO.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_SUI, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);

  with TExecDSWrapper.Create('GenericSelect2CurrentNbr') do
  begin
    SetMacro('Table1', 'CL_E_DS');
    SetMacro('Table2', 'CO_E_D_CODES');
    SetMacro('JoinField', 'CL_E_DS');
    SetMacro('NbrField', 'CO');
    SetMacro('Columns', 'T1.CL_E_DS_NBR, CUSTOM_E_D_CODE_NUMBER, E_D_CODE_TYPE, DESCRIPTION, OVERRIDE_RATE, '
      + 'OVERRIDE_EE_RATE_NUMBER, REGULAR_OT_RATE, SD_FREQUENCY, SD_EFFECTIVE_START_DATE, SD_AMOUNT, SD_RATE, '
      + 'SD_CALCULATION_METHOD, SD_AUTO, SD_ROUND, SD_EXCLUDE_WEEK_1, SD_EXCLUDE_WEEK_2, SD_EXCLUDE_WEEK_3, '
      + 'SD_EXCLUDE_WEEK_4, SD_EXCLUDE_WEEK_5, CO_E_D_CODES_NBR');
    SetParam('RecordNbr', wwdsList.DataSet.FieldByName('CO_NBR').AsInteger);
    dsCO_E_DS.DataRequest(AsVariant);
  end;
  AddDS(dsCO_E_DS, aDS);

  inherited;
end;

procedure TREDIT_CO.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  NavigationDataSet := DM_COMPANY.CO;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_USER, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_TEAM, SupportDataSets, '');
end;

procedure TREDIT_CO.BuildDBDT;
var
  aDataSet: TevClientDataSet;
  UpperLevelNbr: Integer;
  CoNbr: Integer;
begin
  aDataSet := nil;
  ctx_StartWait;
  with CO_DBDT, DM_COMPANY do
  try
    IndexName := '';
    if Active then
      EmptyDataSet
    else
      CreateDataSet;

    if CO.FieldByName('DBDT_LEVEL').Value = CLIENT_LEVEL_COMPANY then
      Exit;
    case CO.FieldByName('DBDT_LEVEL').AsString[1] of
      CLIENT_LEVEL_DIVISION:
        begin
          aDataSet := CO_DIVISION;
        end;
      CLIENT_LEVEL_BRANCH:
        begin
          aDataSet := CO_BRANCH;
        end;
      CLIENT_LEVEL_DEPT:
        begin
          aDataSet := CO_DEPARTMENT;
        end;
      CLIENT_LEVEL_TEAM:
        begin
          aDataSet := CO_TEAM;
        end;
    end;
    aDataSet.First;
    while not aDataSet.EOF do
    begin
      CoNbr := 0;
      Insert;
      case CO.FieldByName('DBDT_LEVEL').AsString[1] of
        CLIENT_LEVEL_DIVISION:
          begin
            FieldByName('CUSTOM_NUMBER').AsString := Trim(aDataSet.FieldByName('CUSTOM_DIVISION_NUMBER').AsString);
            FieldByName('NAME').AsString := aDataSet.FieldByName('NAME').AsString;
            CoNbr := aDataSet.FieldByName('CO_NBR').AsInteger;
          end;
        CLIENT_LEVEL_BRANCH:
          begin
            FieldByName('CUSTOM_NUMBER').AsString := Trim(aDataSet.FieldByName('CUSTOM_BRANCH_NUMBER').AsString);
            FieldByName('NAME').AsString := aDataSet.FieldByName('NAME').AsString;
            UpperLevelNbr := aDataSet.FieldByName('CO_DIVISION_NBR').AsInteger;

            FieldByName('CUSTOM_NUMBER').AsString := Trim(ConvertNull(CO_DIVISION.Lookup('CO_DIVISION_NBR', UpperLevelNbr,
              'CUSTOM_DIVISION_NUMBER'), '')) + ' | ' + FieldByName('CUSTOM_NUMBER').AsString;
            FieldByName('NAME').AsString := ConvertNull(CO_DIVISION.Lookup('CO_DIVISION_NBR', UpperLevelNbr, 'NAME'), '')
              + ' | ' + FieldByName('NAME').AsString;
            CoNbr := ConvertNull(CO_DIVISION.Lookup('CO_DIVISION_NBR', UpperLevelNbr, 'CO_NBR'), '0');
          end;
        CLIENT_LEVEL_DEPT:
          begin
            FieldByName('CUSTOM_NUMBER').AsString := Trim(aDataSet.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString);
            FieldByName('NAME').AsString := aDataSet.FieldByName('NAME').AsString;
            UpperLevelNbr := aDataSet.FieldByName('CO_BRANCH_NBR').AsInteger;

            FieldByName('CUSTOM_NUMBER').AsString := Trim(ConvertNull(CO_BRANCH.Lookup('CO_BRANCH_NBR', UpperLevelNbr,
              'CUSTOM_BRANCH_NUMBER'), '')) + ' | ' + FieldByName('CUSTOM_NUMBER').AsString;
            FieldByName('NAME').AsString := ConvertNull(CO_BRANCH.Lookup('CO_BRANCH_NBR', UpperLevelNbr, 'NAME'), '')
              + ' | ' + FieldByName('NAME').AsString;
            UpperLevelNbr := CO_BRANCH.Lookup('CO_BRANCH_NBR', UpperLevelNbr, 'CO_DIVISION_NBR');

            FieldByName('CUSTOM_NUMBER').AsString := Trim(ConvertNull(CO_DIVISION.Lookup('CO_DIVISION_NBR', UpperLevelNbr,
              'CUSTOM_DIVISION_NUMBER'), '')) + ' | ' + FieldByName('CUSTOM_NUMBER').AsString;
            FieldByName('NAME').AsString := ConvertNull(CO_DIVISION.Lookup('CO_DIVISION_NBR', UpperLevelNbr, 'NAME'), '')
              + ' | ' + FieldByName('NAME').AsString;
            CoNbr := ConvertNull(CO_DIVISION.Lookup('CO_DIVISION_NBR', UpperLevelNbr, 'CO_NBR'), '0');
          end;
        CLIENT_LEVEL_TEAM:
          begin
            FieldByName('CUSTOM_NUMBER').AsString := Trim(aDataSet.FieldByName('CUSTOM_TEAM_NUMBER').AsString);
            FieldByName('NAME').AsString := aDataSet.FieldByName('NAME').AsString;
            UpperLevelNbr := aDataSet.FieldByName('CO_DEPARTMENT_NBR').AsInteger;

            FieldByName('CUSTOM_NUMBER').AsString := Trim(ConvertNull(CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', UpperLevelNbr,
              'CUSTOM_DEPARTMENT_NUMBER'), '')) + ' | ' + FieldByName('CUSTOM_NUMBER').AsString;
            FieldByName('NAME').AsString := ConvertNull(CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', UpperLevelNbr, 'NAME'), '')
              + ' | ' + FieldByName('NAME').AsString;
            UpperLevelNbr := CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', UpperLevelNbr, 'CO_BRANCH_NBR');

            FieldByName('CUSTOM_NUMBER').AsString := Trim(ConvertNull(CO_BRANCH.Lookup('CO_BRANCH_NBR', UpperLevelNbr,
              'CUSTOM_BRANCH_NUMBER'), '')) + ' | ' + FieldByName('CUSTOM_NUMBER').AsString;
            FieldByName('NAME').AsString := ConvertNull(CO_BRANCH.Lookup('CO_BRANCH_NBR', UpperLevelNbr, 'NAME'), '')
              + ' | ' + FieldByName('NAME').AsString;
            UpperLevelNbr := CO_BRANCH.Lookup('CO_BRANCH_NBR', UpperLevelNbr, 'CO_DIVISION_NBR');

            FieldByName('CUSTOM_NUMBER').AsString := Trim(ConvertNull(CO_DIVISION.Lookup('CO_DIVISION_NBR', UpperLevelNbr,
              'CUSTOM_DIVISION_NUMBER'), '')) + ' | ' + FieldByName('CUSTOM_NUMBER').AsString;
            FieldByName('NAME').AsString := ConvertNull(CO_DIVISION.Lookup('CO_DIVISION_NBR', UpperLevelNbr, 'NAME'), '')
              + ' | ' + FieldByName('NAME').AsString;
            CoNbr := ConvertNull(CO_DIVISION.Lookup('CO_DIVISION_NBR', UpperLevelNbr, 'CO_NBR'), '0');
          end;
      end;
      FieldByName('HOME_CO_STATES_NBR').Value := aDataSet.FieldByName('HOME_CO_STATES_NBR').Value;
      FieldByName('PAY_FREQUENCY_HOURLY').Value := ReturnDescription(FrequencyType_ComboChoices,
        aDataSet.FieldByName('PAY_FREQUENCY_HOURLY').Value);
      FieldByName('PAY_FREQUENCY_SALARY').Value := ReturnDescription(FrequencyType_ComboChoices,
        aDataSet.FieldByName('PAY_FREQUENCY_SALARY').Value);
      FieldByName('OVERRIDE_EE_RATE_NUMBER').Value := aDataSet.FieldByName('OVERRIDE_EE_RATE_NUMBER').Value;
      FieldByName('OVERRIDE_PAY_RATE').Value := aDataSet.FieldByName('OVERRIDE_PAY_RATE').Value;
      FieldByName('PAYROLL_CL_BANK_ACCOUNT_NBR').Value := aDataSet.FieldByName('PAYROLL_CL_BANK_ACCOUNT_NBR').Value;
      FieldByName('TAX_CL_BANK_ACCOUNT_NBR').Value := aDataSet.FieldByName('TAX_CL_BANK_ACCOUNT_NBR').Value;
      FieldByName('BILLING_CL_BANK_ACCOUNT_NBR').Value := aDataSet.FieldByName('BILLING_CL_BANK_ACCOUNT_NBR').Value;
      FieldByName('DD_CL_BANK_ACCOUNT_NBR').Value := aDataSet.FieldByName('DD_CL_BANK_ACCOUNT_NBR').Value;
      if CoNbr = CO.FieldByName('CO_NBR').AsInteger then
        Post
      else
        Cancel;
      aDataSet.Next;
    end;
    First;
  finally
    ctx_EndWait;
  end;
end;

procedure TREDIT_CO.AfterDataSetsReopen;
begin
  inherited;
  if wwdsList.DataSet.RecordCount > 0 then
    BuildDBDT;
end;

function TREDIT_CO.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_BRANCH') or (sName = 'CO_DEPARTMENT') or (sName = 'CO_TEAM') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TREDIT_CO.fpDBDTResize(Sender: TObject);
begin
  inherited;
  wwgrCO_DBDT.Width  := fpDBDT.Width - 40;
  wwgrCO_DBDT.Height := fpDBDT.Height - 65;
end;

procedure TREDIT_CO.PageControl1Change(Sender: TObject);
begin
  inherited;
  //wwgrCO_DBDT columns are NOT holding the default sizes and INI is off.
  wwgrCO_DBDT.Columns[1].DisplayWidth := 48;

end;

procedure TREDIT_CO.TabSheet1Show(Sender: TObject);
begin
  inherited;
  //No way to set column widths. This change is to force the default
  //presentation to look "filled" in the default width.
  wwdbgridSelectClient.Columns[0].DisplayWidth := 12;
  wwdbgridSelectClient.Columns[1].DisplayWidth := 37;
end;

procedure TREDIT_CO.fpEDIT_CO_BASE_CompanyResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  wwdbgridSelectClient.Height := fpEDIT_CO_BASE_Company.Height - 65;
  wwdbgridSelectClient.Width  := fpEDIT_CO_BASE_Company.Width - 40;
end;

initialization
  RegisterClass(TREDIT_CO);

end.
