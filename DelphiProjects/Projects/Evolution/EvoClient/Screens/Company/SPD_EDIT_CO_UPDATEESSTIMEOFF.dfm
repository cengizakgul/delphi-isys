object EDIT_CO_UPDATEESSTIMEOFF: TEDIT_CO_UPDATEESSTIMEOFF
  Left = 588
  Top = 261
  BorderStyle = bsSingle
  Caption = 'Time Off Request'
  ClientHeight = 329
  ClientWidth = 279
  Color = clBtnFace
  Constraints.MinHeight = 339
  Constraints.MinWidth = 251
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 0
    Width = 279
    Height = 329
    Align = alClient
    Caption = 'evPanel1'
    TabOrder = 0
    object evlblCodeandName: TevLabel
      Left = 16
      Top = 19
      Width = 125
      Height = 16
      Caption = 'evlblCodeandName'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLblFromToDate: TevLabel
      Left = 16
      Top = 45
      Width = 112
      Height = 16
      Caption = 'evLblFromToDate'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evlblType: TevLabel
      Left = 16
      Top = 72
      Width = 61
      Height = 16
      Caption = 'evlblType'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EvDBMemo1: TEvDBMemo
      Left = 16
      Top = 99
      Width = 242
      Height = 35
      DataField = 'Notes'
      DataSource = dsTOADetails
      MaxLength = 40
      TabOrder = 0
    end
    object wwwdgTOADetails: TevDBGrid
      Left = 16
      Top = 144
      Width = 241
      Height = 129
      DisableThemesInTitle = False
      ControlInfoInDataSet = True
      Selected.Strings = (
        'AccrualDate'#9'10'#9'Date'#9'T'
        'Time'#9'10'#9'Hours'#9'T')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TEDIT_CO_UPDATEESSTIMEOFF\wwwdgTOADetails'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
      DataSource = dsTOADetails
      ReadOnly = False
      TabOrder = 1
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
      DefaultSort = '-'
    end
    object evDBSpinEdit1: TevDBSpinEdit
      Left = 96
      Top = 209
      Width = 121
      Height = 21
      EditorEnabled = False
      Increment = 0.250000000000000000
      MaxValue = 24.000000000000000000
      AutoSelect = False
      DataField = 'Time'
      DataSource = dsTOADetails
      TabOrder = 2
      UnboundDataType = wwDefault
      OnChange = evDBSpinEdit1Change
    end
    object OKBtn: TButton
      Left = 44
      Top = 289
      Width = 75
      Height = 25
      Caption = 'Update'
      Default = True
      ModalResult = 1
      TabOrder = 3
      OnClick = OKBtnClick
    end
    object CancelBtn: TButton
      Left = 149
      Top = 289
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 4
    end
    object BtnUsed: TevBitBtn
      Left = 227
      Top = 201
      Width = 75
      Height = 25
      Caption = 'Used'
      TabOrder = 5
      Visible = False
      NumGlyphs = 2
    end
  end
  object wwdsDetail: TevDataSource
    Left = 214
    Top = 18
  end
  object dsTOADetails: TevDataSource
    Left = 215
    Top = 57
  end
end
