// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_PHONE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc, StdCtrls, Mask, DBCtrls,
  Wwdotdot, Wwdbcomb, wwdbedit, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, SPD_EDIT_CO_BASE,
  Buttons, ExtCtrls, SDataStructure,  ISBasicClasses,
  SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, EvCommonInterfaces, EvDataset, EvExceptions;

type
  TEDIT_CO_PHONE = class(TEDIT_CO_BASE)
    shtDetails: TTabSheet;
    lablContact: TevLabel;
    lablPhone_Number: TevLabel;
    lablPhone_Type: TevLabel;
    lablDescription: TevLabel;
    dedtContact: TevDBEdit;
    dedtDescription: TevDBEdit;
    wwdePhone_Number: TevDBEdit;
    wwcbPhone_Type: TevDBComboBox;
    grdContact: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    evBitBtn1: TevBitBtn;
    evDBEdit1: TevDBEdit;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    lablPhone_Ext: TevLabel;
    wwdePhone_Ext: TevDBEdit;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    pnlFPCORightBody: TevPanel;
    fpCOContactsSummary: TisUIFashionPanel;
    fpCOContactsDetails: TisUIFashionPanel;
    procedure evBitBtn1Click(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;


implementation

uses
  sPackageEntry;

{$R *.DFM}

function TEDIT_CO_PHONE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_PHONE;
end;

function TEDIT_CO_PHONE.GetInsertControl: TWinControl;
begin
  Result := dedtCONTACT;
end;

procedure TEDIT_CO_PHONE.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
      s : String;
      Q : IevQuery;
begin
  inherited;
  if Kind in [NavOk, NavCommit] then
  begin
   if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
   begin
      wwdsDetail.DataSet.UpdateRecord;

      s := 'select count(*) countTaxReturn from CO_PHONE a where a.PHONE_TYPE=''T'' and ' +
                           'a.CO_NBR= :CoNbr and {AsOfNow<a>}';
      Q := TevQuery.Create(s);
      Q.Params.AddValue('CoNbr',  DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
      Q.Execute;
      if Q.Result.Fields[0].AsInteger > 0 then
         if ((wwdsDetail.DataSet.State = dsInsert) and
                 (wwdsDetail.DataSet.FieldByName('PHONE_TYPE').Value = 'T')) or
                ((wwdsDetail.DataSet.State = dsEdit) and
                 (wwdsDetail.DataSet.FieldByName('PHONE_TYPE').OldValue <> 'T') and
                 (wwdsDetail.DataSet.FieldByName('PHONE_TYPE').NewValue = 'T')) then
         raise EUpdateError.CreateHelp('Tax Return Phone Type already added.', IDH_ConsistencyViolation);
   end;
  end;
end;

procedure TEDIT_CO_PHONE.evBitBtn1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO')
end;

procedure TEDIT_CO_PHONE.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  grdContact.Enabled := not (wwdsDetail.DataSet.State in [dsInsert,dsEdit]);
end;

procedure TEDIT_CO_PHONE.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  AllowChange := not (wwdsDetail.DataSet.State in [dsInsert,dsEdit]);
end;

initialization
  RegisterClass(TEDIT_CO_PHONE);

end.
