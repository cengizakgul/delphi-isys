// Copyright � 2000-2004 iSystems LLC. All rights reserved.
{ FILLER fields description CO_JOBS

    1-20: Life to Date Amount
    21-40: Life to Date Hours
    41-60: Life to Date E/D Code
    61-100: User ID
    101-140: Title
    141-148: Job Start Date
}

unit SPD_EDIT_CO_JOBS;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, wwdblook, StdCtrls,
  ExtCtrls, DBCtrls, Mask, wwdbedit, Db, Wwdatsrc, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, Controls, Classes, SysUtils, EvTypes,
  SPD_MiniNavigationFrame, Forms, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, EvExceptions, ActnList, Menus, SPD_JobChoiceQuery,
  Variants, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIEdit, isUIwwDBEdit, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, SPD_EDIT_COPY, Dialogs;

const
  FillerPositions: array[1..6, 1..2] of integer = ((1, 20), (21, 20), (41, 20), (61, 40), (101, 40), (141, 8));

type
  TEDIT_CO_JOBS = class(TEDIT_CO_BASE)
    tshtCompany_Jobs: TTabSheet;
    wwdgCompany_Jobs: TevDBGrid;
    tshtLocals: TTabSheet;
    dsJobsLocals: TevDataSource;
    evDBGrid1: TevDBGrid;
    pmJobCopy: TevPopupMenu;
    MenuItem3: TMenuItem;
    JobCopyService: TevActionList;
    CopyService: TAction;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpCOJobSummary: TisUIFashionPanel;
    fpDetails: TisUIFashionPanel;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    lablDescription: TevLabel;
    dedtDescription: TevDBEdit;
    lablWorkers_Comp_Number: TevLabel;
    wwlcWorkersComp: TevDBLookupCombo;
    DBEdit20: TevDBEdit;
    Label35: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel2: TevLabel;
    wwdeRate_Per_Hour: TevDBEdit;
    lablRate_Per_Hour: TevLabel;
    fpCOJobsLocalSummary: TisUIFashionPanel;
    fpCOJobsLocalDetails: TisUIFashionPanel;
    evLabel3: TevLabel;
    evDBLookupCombo1: TevDBLookupCombo;
    MiniNavigationFrame: TMiniNavigationFrame;
    fpLifeToDate: TisUIFashionPanel;
    lablAmount: TevLabel;
    lablHours: TevLabel;
    lablEDCode: TevLabel;
    editHours: TevEdit;
    editEDCode: TevEdit;
    editAmount: TevEdit;
    fpSignature: TisUIFashionPanel;
    lblUserID: TevLabel;
    lblTitle: TevLabel;
    editTitle: TevEdit;
    editUserID: TevEdit;
    cbJobActive: TevDBCheckBox;
    cbFederalCertified: TevDBCheckBox;
    cbStateCertified: TevDBCheckBox;
    lblJobStartDate: TevLabel;
    editJobStartDate: TevDateTimePicker;
    fpAddress: TisUIFashionPanel;
    Label62: TevLabel;
    Label68: TevLabel;
    Label67: TevLabel;
    Label66: TevLabel;
    Label65: TevLabel;
    evLabel4: TevLabel;
    DBEdit28: TevDBEdit;
    DBEdit27: TevDBEdit;
    DBEdit29: TevDBEdit;
    DBEdit30: TevDBEdit;
    evDBLookupCombo2: TevDBLookupCombo;
    DBEdit31: TevDBEdit;
    EvBevel1: TEvBevel;
    sbJobsDetail: TScrollBox;
    sbJobsLocals: TScrollBox;
    mnJobFieldsCopy: TevPopupMenu;
    CopytoCompanyJobs: TMenuItem;
    lablHome_State: TevLabel;
    wwlcHome_State: TevDBLookupCombo;
    pdsDetail: TevProxyDataSet;
    Button1: TButton;

    procedure editChange(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure CopyServiceExecute(Sender: TObject);
    procedure JobLocalTaxCopy;
    procedure CopytoCompanyJobsClick(Sender: TObject);
    procedure pdsDetailNewRecord(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;

  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

uses
  EvUtils;

{$R *.DFM}

procedure TEDIT_CO_JOBS.editChange(Sender: TObject);
var
  SenderValue: string;
begin
  inherited;

  SenderValue := '';
  if Sender is TevEdit then
    SenderValue := TevEdit(Sender).Text
  else if Sender is TevDateTimePicker then
    if TevDateTimePicker(Sender).Checked then
      SenderValue := FormatDateTime('yyyymmdd', TevDateTimePicker(Sender).Date);

  with wwdsDetail.DataSet do
    if (ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, FillerPositions[TComponent(Sender).Tag, 1], FillerPositions[TComponent(Sender).Tag, 2]) <> SenderValue) or
       (SenderValue = '') and
        (ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, FillerPositions[TComponent(Sender).Tag, 1], FillerPositions[TComponent(Sender).Tag, 2]) <> '') and
       (State = dsBrowse) then
      Edit;
end;

procedure TEDIT_CO_JOBS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if (wwdsDetail.DataSet <> nil) and (wwdsDetail.DataSet.State = dsInsert) then
  begin
    editAmount.Text := '';
    editHours.Text := '';
    editEDCode.Text := '';
    editTitle.Text := '';
    editUserID.Text := '';
    editJobStartDate.Checked := False;
  end;
end;

procedure TEDIT_CO_JOBS.wwdsDataChange(Sender: TObject; Field: TField);

  procedure SetEditText(aEvEdit: TevEdit);
  begin
    aEvEdit.Text := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, FillerPositions[aEvEdit.Tag, 1], FillerPositions[aEvEdit.Tag, 2]);
  end;

  procedure SetDateTimePickerDate(aEvDateTimePicker: TevDateTimePicker);
  var
    sDate: string;
  begin
    aEvDateTimePicker.Date := Date;
    sDate := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, FillerPositions[aEvDateTimePicker.Tag, 1], FillerPositions[aEvDateTimePicker.Tag, 2]);
    if sDate = '' then
      aEvDateTimePicker.Checked := False
    else
    try
      aEvDateTimePicker.Date := EncodeDate(StrToInt(Copy(sDate, 1, 4)), StrToInt(Copy(sDate, 5, 2)), StrToInt(Copy(sDate, 7, 2)));
      aEvDateTimePicker.Checked := True;
    except
      aEvDateTimePicker.Checked := False;
    end;
  end;

begin
  inherited;
  if (wwdsDetail.DataSet <> nil) and (wwdsDetail.DataSet.State = dsBrowse) then
  begin
    SetEditText(editAmount);
    SetEditText(editHours);
    SetEditText(editEDCode);
    SetEditText(editTitle);
    SetEditText(editUserID);
    SetDateTimePickerDate(editJobStartDate);
  end;
end;

procedure TEDIT_CO_JOBS.wwdsDetailUpdateData(Sender: TObject);

  procedure CheckIfNumeric(e: TevEdit);
  begin
    try
      if e.Text <> '' then
        StrToFloat(e.Text);
    except
      e.SetFocus;
      raise EUpdateError.CreateHelp('The value must be numeric!', IDH_ConsistencyViolation);
    end;
  end;

  procedure SetNumericValue(aEvEdit: TevEdit);
  begin
    if ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, FillerPositions[aEvEdit.Tag, 1], FillerPositions[aEvEdit.Tag, 2]) <> aEvEdit.Text then
    begin
      CheckIfNumeric(aEvEdit);
      wwdsDetail.DataSet.FieldByName('FILLER').AsString :=
        PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, aEvEdit.Text, FillerPositions[aEvEdit.Tag, 1], FillerPositions[aEvEdit.Tag, 2]);
    end;
  end;

  procedure SetTextValue(aEvEdit: TevEdit);
  begin
    if ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, FillerPositions[aEvEdit.Tag, 1], FillerPositions[aEvEdit.Tag, 2]) <> aEvEdit.Text then
    begin
      wwdsDetail.DataSet.FieldByName('FILLER').AsString :=
        PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, Copy(aEvEdit.Text, 1, FillerPositions[aEvEdit.Tag, 2]), FillerPositions[aEvEdit.Tag, 1], FillerPositions[aEvEdit.Tag, 2]);
    end;
  end;

  procedure SetDateTimeValue(aEvDateTimePicker: TevDateTimePicker);
  var
    sDate: string;
  begin
    if aEvDateTimePicker.Checked then
      sDate := FormatDateTime('yyyymmdd', aEvDateTimePicker.Date)
    else
      sDate := '';

    if ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, FillerPositions[aEvDateTimePicker.Tag, 1], FillerPositions[aEvDateTimePicker.Tag, 2]) <> sDate then
    begin
      wwdsDetail.DataSet.FieldByName('FILLER').AsString :=
        PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, sDate, FillerPositions[aEvDateTimePicker.Tag, 1], FillerPositions[aEvDateTimePicker.Tag, 2]);
    end;
  end;

begin
  inherited;

  SetNumericValue( editAmount );
  SetNumericValue( editHours );
  SetTextValue( editEDCode );
  SetTextValue( editTitle );
  SetTextValue( editUserID );
  SetDateTimeValue( editJobStartDate );
end;

procedure TEDIT_CO_JOBS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_JOBS_LOCALS, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);  
end;

function TEDIT_CO_JOBS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_JOBS;
end;

function TEDIT_CO_JOBS.GetInsertControl: TWinControl;
begin
  Result := evDBEdit1;//dedtDescription;
end;

procedure TEDIT_CO_JOBS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_LOCAL_TAX, SupportDataSets);
  AddDS(DM_COMPANY.CO_LOCATIONS, SupportDataSets);
  AddDSWithCheck(DM_COMPANY.CO_JOBS_LOCALS, EditDataSets, '');

end;

procedure TEDIT_CO_JOBS.Activate;
begin
  MiniNavigationFrame.DataSource := dsJobsLocals;
  MiniNavigationFrame.InsertFocusControl := evDBLookupCombo1;
//  MiniNavigationFrame.OnDeleteRecordConfirm := ;
  inherited;
end;

function TEDIT_CO_JOBS.GetDataSetConditions(sName: string): string;
begin
  if sName = 'CO_JOBS_LOCALS'  then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_JOBS.CopyServiceExecute(Sender: TObject);
begin
  inherited;
   JobLocalTaxCopy;
end;

procedure TEDIT_CO_JOBS.JobLocalTaxCopy;

var
  CurrentLocalNbr, CurrentNbr, CurrentLocalTaxNbr: integer;

  procedure DoCopyLocals;
  var
    k: integer;
  begin
    with TJobChoiceQuery.Create( Self ) do
    try
      SetFilter('CO_JOBS_NBR <>  ' + DM_COMPANY.CO_JOBS.FieldByName('CO_JOBS_NBR').AsString );
      Caption := 'Select the Jobs you want this Local Tax copied to';
      ConfirmAllMessage := 'This operation will copy the Local Tax to all Jobs. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
            DM_COMPANY.CO_JOBS_LOCALS.DataRequired('ALL');
            if dgChoiceList.SelectedList.Count > 0 then
            begin
              DM_COMPANY.CO_JOBS_LOCALS.DisableControls;
              try
                for k := 0 to dgChoiceList.SelectedList.Count - 1 do
                begin
                  cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                  if not DM_COMPANY.CO_JOBS_LOCALS.Locate('CO_JOBS_NBR;CO_LOCAL_TAX_NBR',
                       VarArrayOf([cdsChoiceList.FieldByName('CO_JOBS_NBR').AsInteger,CurrentLocalTaxNbr]), []) then
                  begin
                    DM_COMPANY.CO_JOBS_LOCALS.Insert;
                    DM_COMPANY.CO_JOBS_LOCALS['CO_JOBS_NBR'] := cdsChoiceList.FieldByName('CO_JOBS_NBR').AsInteger;
                    DM_COMPANY.CO_JOBS_LOCALS['CO_LOCAL_TAX_NBR'] := CurrentLocalTaxNbr;
                    DM_COMPANY.CO_JOBS_LOCALS.Post;
                  end;
                end;
              finally
                ctx_DataAccess.PostDataSets([DM_COMPANY.CO_JOBS_LOCALS]);
                DM_COMPANY.CO_JOBS_LOCALS.EnableControls;
              end;
             end;
      end;
    finally
      Free;
    end;
  end;

begin

  CurrentNbr := DM_COMPANY.CO_JOBS.FieldByName('CO_JOBS_NBR').AsInteger;
  CurrentLocalNbr := DM_COMPANY.CO_JOBS_LOCALS.FieldByName('CO_JOBS_LOCALS_NBR').AsInteger;
  CurrentLocalTaxNbr := DM_COMPANY.CO_JOBS_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
  if (CurrentLocalNbr > 0) and  (CurrentLocalTaxNbr > 0 )then
  begin
    try
      DoCopyLocals;
    finally
      DM_COMPANY.CO_JOBS.Locate('CO_JOBS_NBR',CurrentNbr,[]);
      DM_COMPANY.CO_JOBS_LOCALS.Locate('CO_JOBS_LOCALS_NBR',CurrentLocalNbr,[]);
    end;
  end;
end;

procedure TEDIT_CO_JOBS.CopytoCompanyJobsClick(Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  cClientNbr, cCoNbr, cCoJobsNbr: Integer;
  cCustomCoNbr: String;
begin
  inherited;

  cClientNbr    := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  cCoNbr        := DM_COMPANY.CO_JOBS.FieldByName('CO_NBR').AsInteger;
  cCustomCoNbr  := wwdsMaster.dataset.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
  cCoJobsNbr := DM_COMPANY.CO_JOBS.FieldByName('CO_JOBS_NBR').AsInteger;

  if (wwdgCompany_Jobs.SelectedList.Count > 0) then
  begin
    CopyForm := TEDIT_COPY.Create(Self);

    DM_COMPANY.CO_JOBS.DisableControls;
    try
      DM_COMPANY.CO_JOBS.UserFilter :='';
      DM_COMPANY.CO_JOBS.UserFiltered := false;
      CopyForm.wwcsCoReports.Data := DM_COMPANY.CO_JOBS.Data;

      DM_COMPANY.CO_JOBS.First;
      while not DM_COMPANY.CO_JOBS.Eof do
      begin
        if not wwdgCompany_Jobs.IsSelectedRecord then
        begin
          if CopyForm.wwcsCoReports.locate('CO_JOBS_NBR', DM_COMPANY.CO_JOBS['CO_JOBS_NBR'], []) then
            CopyForm.wwcsCoReports.Delete;
        end;
        DM_COMPANY.CO_JOBS.Next;
      end;

      CopyForm.CopyTo := 'CO_JOBS';
      CopyForm.CopyFrom := 'CO_JOBS';
      DM_COMPANY.CO_JOBS.Cancel;

      wwdsMaster.DataSet := nil;
      CopyForm.ShowModal;

      ctx_DataAccess.OpenClient(cClientNbr);
      DM_CLIENT.CL.DataRequired('ALL');
      DM_COMPANY.CO.DataRequired('CO_NBR='+IntToStr(cCoNbr));
      DM_COMPANY.CO_JOBS.DataRequired('CO_NBR='+IntToStr(cCoNbr));
      wwdsMaster.DataSet := DM_CLIENT.CO;
      wwdsList.dataset.Locate('CO_NBR;CL_NBR;CUSTOM_COMPANY_NUMBER', VarArrayOf([cCoNbr, cClientNbr, cCustomCoNbr]), []);
      DM_COMPANY.CO_JOBS.Locate('CO_JOBS_NBR', cCoJobsNbr, []);
    finally
      wwdgCompany_Jobs.UnselectAll;
      DM_COMPANY.CO_JOBS.EnableControls;
      CopyForm.Free;
    end;
  end;

end;

procedure TEDIT_CO_JOBS.pdsDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  with DM_COMPANY.CO do
    if Active then
      DataSet.FindField('HOME_CO_STATES_NBR').AsInteger :=
        FindField('HOME_CO_STATES_NBR').AsInteger;
end;

procedure TEDIT_CO_JOBS.Button1Click(Sender: TObject);
begin
  inherited;
 //  test
 // test 2
 
 
end;

initialization
  RegisterClass(TEDIT_CO_JOBS);

end.
