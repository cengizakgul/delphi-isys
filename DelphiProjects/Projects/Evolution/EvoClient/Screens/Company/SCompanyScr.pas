// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SCompanyScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, EvCommonInterfaces,
  EvSecElement, ActnList, SDataStructure, EvTypes, Graphics, ISBasicClasses,
  EvMainboard, EvExceptions, EvUIComponents, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TCompanyFrm = class(TFramePackageTmpl, IevCompanyScreens)
  protected
    function InitPackage: Boolean; override;
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function ActivatePackage(WorkPlace: TWinControl): Boolean; override;
    function  PackageBitmap: TBitmap; override;
  end;

implementation

uses EvUtils, EvContext, EvConsts;

var
  CompanyFrm: TCompanyFrm;

{$R *.DFM}


function GetCompanyScreens: IevCompanyScreens;
begin
  if not Assigned(CompanyFrm) then
    CompanyFrm := TCompanyFrm.Create(nil);
  Result := CompanyFrm;

end;

{ TSystemFrm }

function TCompanyFrm.ActivatePackage(WorkPlace: TWinControl): Boolean;
begin
  Result := inherited ActivatePackage(WorkPlace);
  if Result then
  begin
    DM_TEMPORARY.TMP_CL.Activate;
    if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
      raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);
  end;
end;

function TCompanyFrm.InitPackage: Boolean;
begin
  Result := inherited InitPackage;

  if not Context.License.SelfServe then
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_ESS_GROUP_ASSIGNMENT', stDisabled, False);
     
  if not Context.License.Benefits then
  begin
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_BENEFIT_CATEGORY', stDisabled, False);
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_BENEFIT_DISCOUNT', stDisabled, False);
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_BENEFIT_ENROLLMENT_APP', stDisabled, False);
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_BENEFIT_GROUP', stDisabled, False);
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_BENEFIT_PACKAGE', stDisabled, False);
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_BENEFIT_PKG_ASMNT', stDisabled, False);
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_BENEFIT_PKG_DETAIL', stDisabled, False);
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_BENEFIT_PROVIDER', stDisabled, False);
     ctx_AccountRights.SetElementState(etScreen, 'TEDIT_CO_BENEFIT_SETUP', stDisabled, False);
  end;

end;

function TCompanyFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TCompanyFrm.PackageCaption: string;
begin
  Result := '&Company';
end;

function TCompanyFrm.PackageSortPosition: string;
begin
  Result := '040';
end;

procedure TCompanyFrm.UserPackageStructure;
begin
  AddStructure('General|010');
  AddStructure('General\Company Info|110', 'TEDIT_CO');
  AddStructure('General\Company Summary|112', 'TREDIT_CO');
  AddStructure('General\Calendar|115', 'TEDIT_CO_SCHEDULER');
  AddStructure('General\Contacts|120', 'TEDIT_CO_PHONE');
  AddStructure('General\Services|130', 'TEDIT_CO_SERVICES');
  AddStructure('General\Billing History|135', 'TEDIT_CO_BILLING_HISTORY');
  AddStructure('General\Workers Compensation|150', 'TEDIT_CO_WORKERS_COMP');
  AddStructure('General\General Ledger|200', 'TEDIT_CO_GENERAL_LEDGER');
  AddStructure('General\Sales|210', 'TEDIT_CO_SALESPERSON');
  AddStructure('General\E/Ds|220', 'TEDIT_CO_ED_CODES');
  AddStructure('General\Additional Info|230', 'TEDIT_CO_ADDITIONAL_INFO_NAMES');
  AddStructure('General\Auto Enlist Returns|240', 'TEDIT_CO_AUTO_ENLIST_RETURNS');
  AddStructure('General\Enlist Groups|250', 'TEDIT_CO_ENLIST_GROUPS');
  AddStructure('General\Analytics|300', 'TEDIT_CO_DASHBOARDS');
  AddStructure('General\Vendors|400', 'TEDIT_CO_VENDOR');

  AddStructure('Taxes|020');
  AddStructure('Taxes\States|010', 'TEDIT_CO_STATES');
  AddStructure('Taxes\Local Tax|030', 'TEDIT_CO_LOCAL_TAX');
  AddStructure('Taxes\Federal Tax Liabilities|040', 'TEDIT_CO_FED_TAX_LIABILITIES');
  AddStructure('Taxes\State Tax Liabilities|050', 'TEDIT_CO_STATE_TAX_LIABILITIES');
  AddStructure('Taxes\SUI Liabilities|060', 'TEDIT_CO_SUI_LIABILITIES');
  AddStructure('Taxes\Local Tax Liabilities|070', 'TEDIT_CO_LOCAL_TAX_LIABILITIES');
  AddStructure('Taxes\Tax Deposit|080', 'TEDIT_CO_TAX_DEPOSIT');
  AddStructure('Div/Branch/Dept/Team|030');
  AddStructure('Div/Branch/Dept/Team\Locations|109', 'TEDIT_CO_LOC');
  AddStructure('Div/Branch/Dept/Team\Divisions|110', 'TEDIT_CO_DIVISION');
  AddStructure('Div/Branch/Dept/Team\Branches|120', 'TEDIT_CO_BRANCH');
  AddStructure('Div/Branch/Dept/Team\Departments|130', 'TEDIT_CO_DEPARTMENT');
  AddStructure('Div/Branch/Dept/Team\Teams|140', 'TEDIT_CO_TEAM');
  AddStructure('Div/Branch/Dept/Team\Jobs|150', 'TEDIT_CO_JOBS');
  AddStructure('Div/Branch/Dept/Team\Shifts|160', 'TEDIT_CO_SHIFTS');
  AddStructure('Benefits|040');
  AddStructure('Benefits\Benefits|070', 'TEDIT_CO_BENEFITS');
  AddStructure('Benefits\Benefits Administration|080');
  AddStructure('Benefits\Benefits Administration\Benefit Categories|100', 'TEDIT_CO_BENEFIT_CATEGORY');
  AddStructure('Benefits\Benefits Administration\Benefit Discounts|110', 'TEDIT_CO_BENEFIT_DISCOUNT');
  AddStructure('Benefits\Benefits Administration\Benefit Packages|120', 'TEDIT_CO_BENEFIT_PACKAGE');
  AddStructure('Benefits\Benefits Administration\Benefit Package Details|130', 'TEDIT_CO_BENEFIT_PKG_DETAIL');
  AddStructure('Benefits\Benefits Administration\Benefit Groups|140', 'TEDIT_CO_BENEFIT_GROUP');
  AddStructure('Benefits\Benefits Administration\Benefit Package Assignment|150', 'TEDIT_CO_BENEFIT_PKG_ASMNT');
  AddStructure('Benefits\Benefits Administration\Benefit Providers|160', 'TEDIT_CO_BENEFIT_PROVIDER');
  AddStructure('Benefits\Benefits Administration\Enrollment Setup|170', 'TEDIT_CO_BENEFIT_SETUP');
  AddStructure('Benefits\Benefits Administration\Enrollment Approval|180', 'TEDIT_CO_BENEFIT_ENROLLMENT_APP');

  AddStructure('Benefits\ESS Group Assignment|090', 'TEDIT_CO_ESS_GROUP_ASSIGNMENT');
  AddStructure('Benefits\ESS Time Off Management|100', 'TEDIT_CO_ESSTIMEOFFMANAGEMENT');
  AddStructure('Benefits\Pensions|110', 'TEDIT_CO_PENSIONS');
  AddStructure('Benefits\Time off accrual|120', 'TEDIT_CO_TIME_OFF_ACCRUAL');
  AddStructure('Benefits\Time off accrual rates|130', 'TEDIT_CO_TIME_OFF_ACCRUAL_RATES');
  AddStructure('Benefits\Time off accrual tiers|140', 'TEDIT_CO_TIME_OFF_ACCRUAL_TIERS');
  AddStructure('Benefits\HR Positions|150', 'TEDIT_CO_HR_POSITIONS');
  AddStructure('Benefits\Unions|160', 'TEDIT_CO_UNIONS');
  AddStructure('Payroll Defaults|050');
  AddStructure('Payroll Defaults\Default E/D''s -- Company|110', 'TEDIT_CO_PR_BATCH_DEFLT_ED');
  AddStructure('Payroll Defaults\Default E/D''s -- Division|120', 'TEDIT_CO_DIV_PR_BATCH_DEFLT_ED');
  AddStructure('Payroll Defaults\Default E/D''s -- Branch|130', 'TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED');
  AddStructure('Payroll Defaults\Default E/D''s -- Department|140', 'TEDIT_CO_DEPT_PR_BATCH_DEFLT_ED');
  AddStructure('Payroll Defaults\Default E/D''s -- Team|150', 'TEDIT_CO_TEAM_PR_BATCH_DEFLT_ED');
  AddStructure('Payroll Defaults\Payroll Check Templates|160', 'TEDIT_CO_PR_CHECK_TEMPLATES');
  AddStructure('Payroll Defaults\Pay Group|190', 'TEDIT_CO_PAY_GROUP');
  AddStructure('Payroll Defaults\Payroll Filters|200', 'TEDIT_CO_PR_FILTERS');
  AddStructure('Imports\HR Import|145', 'TIM_HRTrax');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetCompanyScreens, IevCompanyScreens, 'Screens Company');

finalization
  CompanyFrm.Free;


end.
