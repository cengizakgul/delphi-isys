inherited EDIT_CO_ED_CODES: TEDIT_CO_ED_CODES
  Width = 677
  Height = 574
  inherited Panel1: TevPanel
    Width = 677
    inherited pnlFavoriteReport: TevPanel
      Left = 525
    end
    inherited pnlTopLeft: TevPanel
      Width = 525
    end
  end
  inherited PageControl1: TevPageControl
    Width = 677
    Height = 520
    HelpContext = 13502
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 669
        Height = 491
        inherited pnlBorder: TevPanel
          Width = 665
          Height = 487
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 665
          Height = 487
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 784
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 772
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 456
              Top = 128
              Width = 257
              Height = 360
            end
          end
          inherited Panel3: TevPanel
            Width = 617
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 617
            Height = 380
            inherited Splitter1: TevSplitter
              Height = 376
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 74
              Height = 376
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 24
                Height = 296
                IniAttributes.SectionName = 'TEDIT_CO_ED_CODES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 77
              Height = 376
            end
          end
        end
      end
    end
    object tshtCO_ED_Codes_Detail: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object drgpPrint_Branch_Address_Check: TevDBRadioGroup
        Left = 544
        Top = 128
        Width = 185
        Height = 50
        Caption = '~Distribute'
        Columns = 2
        DataField = 'DISTRIBUTE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 0
        Values.Strings = (
          'Y'
          'N')
        Visible = False
      end
      object sbED: TScrollBox
        Left = 0
        Top = 0
        Width = 669
        Height = 491
        Align = alClient
        TabOrder = 1
        object fpCOEdCodesSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 424
          Height = 305
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwDBGrid2x: TevDBGrid
            Left = 12
            Top = 35
            Width = 391
            Height = 249
            DisableThemesInTitle = False
            Selected.Strings = (
              'ED_Lookup'#9'29'#9'E/D Code'#9'F'
              'CodeDescription'#9'29'#9'Description'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_ED_CODES\wwDBGrid2x'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpCOEdCodesDetails: TisUIFashionPanel
          Left = 8
          Top = 321
          Width = 424
          Height = 246
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'E/D Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablE_D_Code: TevLabel
            Left = 12
            Top = 35
            Width = 57
            Height = 16
            Caption = '~E/D Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label1x: TevLabel
            Left = 12
            Top = 74
            Width = 41
            Height = 13
            Caption = 'G/L Tag'
            FocusControl = DBEdit1x
          end
          object evLabel1: TevLabel
            Left = 205
            Top = 74
            Width = 72
            Height = 13
            Caption = 'G/L Offset Tag'
            FocusControl = evDBEdit1
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 113
            Width = 42
            Height = 13
            Caption = 'Benefit   '
          end
          object evLabel4: TevLabel
            Left = 205
            Top = 113
            Width = 89
            Height = 16
            Caption = '~EE or ER Benefit'
            FocusControl = evDBComboBox1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel3: TevLabel
            Left = 12
            Top = 152
            Width = 99
            Height = 13
            Caption = 'Benefit Amount Type'
          end
          object evLabel5: TevLabel
            Left = 205
            Top = 152
            Width = 87
            Height = 16
            Caption = '~Used as Benefit '
            FocusControl = edUsedAsBenefit
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwlcE_D_Code: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 185
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'CUSTOM_E_D_CODE_NUMBER'
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'No')
            DataField = 'CL_E_DS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_DS.CL_E_DS
            LookupField = 'CL_E_DS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = wwlcE_D_CodeChange
          end
          object wwDBLookupCombo2: TevDBLookupCombo
            Left = 205
            Top = 50
            Width = 198
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'DESCRIPTION')
            DataField = 'CL_E_DS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_DS.CL_E_DS
            LookupField = 'CL_E_DS_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 1
            AutoDropDown = True
            ShowButton = False
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object DBEdit1x: TevDBEdit
            Left = 12
            Top = 89
            Width = 185
            Height = 21
            DataField = 'GENERAL_LEDGER_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit1: TevDBEdit
            Left = 205
            Top = 89
            Width = 198
            Height = 21
            DataField = 'GL_OFFSET'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evBenefitReference: TevDBLookupCombo
            Left = 12
            Top = 128
            Width = 185
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'BENEFIT_NAME'#9'40'#9'Benefit Name'#9'F')
            DataField = 'CO_BENEFITS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_BENEFITS.CO_BENEFITS
            LookupField = 'CO_BENEFITS_NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = evBenefitReferenceChange
            OnCloseUp = evBenefitReferenceCloseUp
          end
          object evDBComboBox1: TevDBComboBox
            Left = 205
            Top = 128
            Width = 198
            Height = 21
            HelpContext = 8140
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'EE_OR_ER_BENEFIT'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 5
            UnboundDataType = wwDefault
          end
          object edUsedAsBenefit: TevDBComboBox
            Left = 205
            Top = 167
            Width = 198
            Height = 21
            HelpContext = 8140
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'USED_AS_BENEFIT'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
          end
          object evBenefitSubType: TevDBLookupCombo
            Left = 12
            Top = 167
            Width = 185
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'Description'#9'F')
            DataField = 'CO_BENEFIT_SUBTYPE_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE
            LookupField = 'CO_BENEFIT_SUBTYPE_NBR'
            Style = csDropDownList
            TabOrder = 6
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object rgShowInEss: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 185
            Height = 37
            Caption = '~Show in EE Portal '
            Columns = 2
            DataField = 'SHOW_IN_ESS'
            DataSource = wwdsDetail
            TabOrder = 8
          end
          object cbACA: TevDBRadioGroup
            Left = 204
            Top = 191
            Width = 198
            Height = 37
            Caption = '~ACA'
            Columns = 2
            DataField = 'ACA'
            DataSource = wwdsDetail
            TabOrder = 9
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_E_D_CODES.CO_E_D_CODES
    OnStateChange = wwdsDetailStateChange
  end
  object dsCOBenefits: TevDataSource
    AutoEdit = False
    DataSet = DM_CO_BENEFITS.CO_BENEFITS
    Left = 456
    Top = 288
  end
  object dsCOBenefitsSubType: TevDataSource
    AutoEdit = False
    MasterDataSource = dsCOBenefits
    Left = 456
    Top = 328
  end
end
