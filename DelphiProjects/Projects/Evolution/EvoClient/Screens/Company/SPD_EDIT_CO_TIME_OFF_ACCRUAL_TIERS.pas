unit SPD_EDIT_CO_TIME_OFF_ACCRUAL_TIERS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  SDataStructure, SDDClasses, SPackageEntry,
  DB, Wwdatsrc, ISBasicClasses, 
  StdCtrls, ExtCtrls, DBCtrls, wwdblook, Wwdotdot, Wwdbcomb, Mask,
  wwdbedit, Buttons, sCopyHelper, sDBDTCopyHelper, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, SPD_EDIT_CO_BASE, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  ImgList, LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_TIME_OFF_ACCRUAL_TIERS = class(TEDIT_CO_BASE)
    wwdsSubMaster: TevDataSource;
    tshtDetails: TTabSheet;
    gTiers: TevDBGrid;
    evSplitter1: TevSplitter;
    pErrorMessage: TevPanel;
    evLabel1: TevLabel;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    pnlFPCORightBody: TevPanel;
    fpCOTOATiersSummary: TisUIFashionPanel;
    evDBGrid1: TevDBGrid;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
   procedure Activate; override;
  end;

implementation

{$R *.dfm}

uses EvTypes, SPD_EDIT_Open_BASE, SFrameEntry;

{ TEDIT_CO_TIME_OFF_ACCRUAL_TIERS }

procedure TEDIT_CO_TIME_OFF_ACCRUAL_TIERS.Activate;
begin

  if wwdsList.DataSet.RecordCount <= 0 then
    inherited;
  ApplySecurity;
end;

function TEDIT_CO_TIME_OFF_ACCRUAL_TIERS.GetDataSetConditions(
  sName: string): string;
begin
  if sName = 'CO_TIME_OFF_ACCRUAL_TIERS' then 
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL_TIERS.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS([DM_COMPANY.CO_TIME_OFF_ACCRUAL_TIERS, DM_COMPANY.CO_TIME_OFF_ACCRUAL], aDS);
  inherited;
end;

function TEDIT_CO_TIME_OFF_ACCRUAL_TIERS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_TIME_OFF_ACCRUAL_TIERS;
end;

function TEDIT_CO_TIME_OFF_ACCRUAL_TIERS.GetInsertControl: TWinControl;
begin
  Result := gTiers;
end;

procedure TEDIT_CO_TIME_OFF_ACCRUAL_TIERS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS([DM_COMPANY.CO_TIME_OFF_ACCRUAL], SupportDataSets);
end;

initialization
  RegisterClass(TEDIT_CO_TIME_OFF_ACCRUAL_TIERS);

end.
