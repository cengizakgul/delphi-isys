// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_AUTO_ENLIST_RETURNS;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, StdCtrls, ExtCtrls,
  DBCtrls, wwdbdatetimepicker, wwdblook, Wwdotdot, Wwdbcomb, Mask,
  wwdbedit, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Controls, Classes,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents, Variants,
  EvDataAccessComponents, SDataDictsystem, SDataDictclient, SDataDicttemp,
  Forms, SPD_MiniNavigationFrame,SFieldCodeValues, SPackageEntry, isBaseClasses,
  Menus, ActnList, EvContext, EvUIComponents, EvClientDataSet, EvCommonInterfaces,
  EvDataSet, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_AUTO_ENLIST_RETURNS = class(TEDIT_CO_BASE)
    tsDoAuto: TTabSheet;
    wwdgSY_Auto_Enlist_Returns: TevDBGrid;
    cdSyGl_Report: TevClientDataSet;
    dsSyGl_Report: TevDataSource;
    btnAdd: TevBitBtn;
    btnDel: TevBitBtn;
    wwdg_CO_Auto_Enlist_Returns: TevDBGrid;
    tsDontAutoSyReport: TTabSheet;
    wwdgSY_DontAuto_Enlist_SYReports: TevDBGrid;
    wwdg_CO_DontAuto_Enlist_SYReports: TevDBGrid;
    cdSyGl_ReportNAME: TStringField;
    cdSyGl_ReportSY_GL_AGENCY_REPORT_NBR: TIntegerField;
    cdSyGl_ReportAGENCY_NAME: TStringField;
    cdSyGl_ReportSTATE: TStringField;
    cdSyGl_ReportDEPOSIT_FREQUENCY: TStringField;
    cdSyGl_ReportRETURN_FREQUENCY: TStringField;
    cdSyGl_ReportSYSTEM_TAX_TYPE: TStringField;
    cdSyGl_ReportNUMBER_OF_DAYS_FOR_WHEN_DUE: TIntegerField;
    cdSyGl_ReportWHEN_DUE_TYPE: TStringField;
    CoAutoEnlistMiniNavigationFrame: TMiniNavigationFrame;
    CoDontAutoSyReportMiniNavigationFrame: TMiniNavigationFrame;
    cdSyGl_ReportsFilter: TStringField;
    cdSyGl_ReportsDEPOSIT_FREQUENCY: TStringField;
    cdSyGl_ReportsRETURN_FREQUENCY: TStringField;
    cdSyGl_ReportsSYSTEM_TAX_TYPE: TStringField;
    cdSyGl_ReportsWHEN_DUE_TYPE: TStringField;
    tsInactiveEnlist: TTabSheet;
    wwdgSY_DontAutoInactive_Enlist_Returns: TevDBGrid;
    CoInactiveMiniNavigationFrame: TMiniNavigationFrame;
    wwdg_CO_InactiveEnlist: TevDBGrid;
    pmAutoReports: TevPopupMenu;
    MenuItem3: TMenuItem;
    pmDontAutoReports: TevPopupMenu;
    MenuItem1: TMenuItem;
    EnlistCopyService: TevActionList;
    CopyService: TAction;
    DontEnlistCopyService: TevActionList;
    CopyServiceDontEnlist: TAction;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpAutoEnlistReturnsSystem: TisUIFashionPanel;
    fpAutoEnlistReturnsCompany: TisUIFashionPanel;
    fpDontAutoEnlistSystemCompany: TisUIFashionPanel;
    fpDontAutoEnlistSystemSystem: TisUIFashionPanel;
    fpAutoEnlistIInactiveCompany: TisUIFashionPanel;
    fpAutoEnlistIInactiveSystem: TisUIFashionPanel;
    tsPAAutoEnlist: TTabSheet;
    sbPA: TScrollBox;
    wwdgSY_PAEITLocalReturns: TevDBGrid;
    wwdg_CO_PAEITLocalReturns: TevDBGrid;
    CoPAEITMiniNavigationFrame: TMiniNavigationFrame;
    cdSyGl_ReportMEDIA_TYPE: TStringField;
    cdSyGl_ReportReturnFormat: TStringField;
    fpPASystem: TisUIFashionPanel;
    fpPACompany: TisUIFashionPanel;
    sbAutoEnlistInactive: TScrollBox;
    sbDontAuto: TScrollBox;
    sbAutoEnlist: TScrollBox;
    procedure CoAutoEnlistMiniNavigationFrameSpeedButton1Click(
      Sender: TObject);
    procedure CoAutoEnlistMiniNavigationFrameSpeedButton2Click(
      Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure CoDontAutoSyReportMiniNavigationFrameSpeedButton1Click(
      Sender: TObject);
    procedure CoDontAutoSyReportMiniNavigationFrameSpeedButton2Click(
      Sender: TObject);
    procedure cdSyGl_ReportCalcFields(DataSet: TDataSet);
    function  PrepareSyList(cd: TevClientDataset; field : String):String;
    procedure PrepareSyLevel;
    procedure CopyServiceUpdate(Sender: TObject);
    procedure CopyServiceExecute(Sender: TObject);
    procedure CopyServiceDontEnlistUpdate(Sender: TObject);
    procedure CopyServiceDontEnlistExecute(Sender: TObject);
    procedure CoInactiveMiniNavigationFrameSpeedButton1Click(
      Sender: TObject);
    procedure CoInactiveMiniNavigationFrameSpeedButton2Click(
      Sender: TObject);
    procedure CoPAEITMiniNavigationFrameSpeedButton1Click(Sender: TObject);
    procedure CoPAEITMiniNavigationFrameSpeedButton2Click(Sender: TObject);
    function  CheckPAStateSetup: integer;
  private
      { Private declarations }
    procedure CopyReports( grdName: TevDBGrid);
  protected
    procedure AfterDataSetsReopen; override;
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure Activate; override;
    procedure AfterClick( Kind: Integer ); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
      var InsertDataSets, EditDataSets: TArrayDS;
      var DeleteDataSet: TevClientDataSet;
      var SupportDataSets: TArrayDS); override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  end;

implementation

{$R *.DFM}

uses EvUtils, SysUtils, SPD_CompanyChoiceQuery, EvConsts, isDataSet;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_COMPANY.CO_SUI, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_GLOBAL_AGENCY, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_COMPANY.CO_SUI, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_COMPANY.CO_STATES, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_COMPANY.CO_LOCAL_TAX, SupportDataSets, 'ALL');

end;

function TEDIT_CO_AUTO_ENLIST_RETURNS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_AUTO_ENLIST_RETURNS;
end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.AfterDataSetsReopen;
begin
  inherited;
  (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
  PrepareSyLevel;

  if Assigned(tsPAAutoEnlist) then
    if CheckPAStateSetup = 0 then
       tsPAAutoEnlist.TabVisible := false
    else
       tsPAAutoEnlist.TabVisible := True;
end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavOk, NavCommit:
      (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
  end;
end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.PrepareSyLevel;
var
 s, sPALocalFilter : string;
begin
                                                     //   Hard code for PA state Nbr = 41
  if DM_COMPANY.CO_STATES.Locate('CO_NBR;SY_STATES_NBR', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').asInteger, 41]), []) and
     (not DM_COMPANY.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').IsNull ) then
  begin
      Assert(DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ.Locate('SY_AGENCY_DEPOSIT_FREQ_NBR', DM_COMPANY.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsVariant, []),'Missing TCD Agency deposit frequency. Please contact with Isystems LLC.' );
      Assert(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Locate('SY_GLOBAL_AGENCY_NBR', DM_SYSTEM_MISC.SY_AGENCY_DEPOSIT_FREQ['SY_GLOBAL_AGENCY_NBR'], []), 'Missing TCD Agency set up. Please contact with Isystems LLC.');
      Assert((DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.FieldByName('AGENCY_TYPE').AsString = 'Y'), 'TCD Agency is inactive.  Select an active TCD Agency.');

      sPALocalFilter :='a.COMBINE_FOR_TAX_PAYMENTS = ''Y'' and b.SY_GLOBAL_AGENCY_NBR = ' + DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.fieldByname('SY_GLOBAL_AGENCY_NBR').AsString;
  end
  else
  begin
      DM_COMPANY.CO_LOCAL_TAX.Filter:= 'LOCAL_ACTIVE = ''Y''';
      DM_COMPANY.CO_LOCAL_TAX.Filtered := True;
      sPALocalFilter := PrepareSyList(DM_COMPANY.CO_LOCAL_TAX, 'SY_LOCALS_NBR');
  end;

  DM_COMPANY.CO_SUI.Filter := 'FINAL_TAX_RETURN = ''Y''';
  DM_COMPANY.CO_SUI.Filtered := True;
  DM_COMPANY.CO_STATES.Filter := 'STATE_NON_PROFIT = ''Y''';
  DM_COMPANY.CO_STATES.Filtered := True;
  DM_COMPANY.CO_LOCAL_TAX.Filter:= 'LOCAL_ACTIVE = ''N''';
  DM_COMPANY.CO_LOCAL_TAX.Filtered := True;

  s :=
  'select d.AGENCY_NAME, d.STATE, ' +
        'b.NAME, ' +
        'a.DEPOSIT_FREQUENCY, a.RETURN_FREQUENCY, a.SYSTEM_TAX_TYPE, ' +
        'a.NUMBER_OF_DAYS_FOR_WHEN_DUE, a.WHEN_DUE_TYPE, a.SY_GL_AGENCY_REPORT_NBR, b.MEDIA_TYPE, ' +
        '''1'' sFilter ' +
        'from SY_GL_AGENCY_REPORT a, SY_REPORTS_GROUP b, ' +
              'SY_GL_AGENCY_FIELD_OFFICE c, SY_GLOBAL_AGENCY d ' +
        'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
               'a.SY_GL_AGENCY_FIELD_OFFICE_NBR = c.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
               'c.SY_GLOBAL_AGENCY_NBR = d.SY_GLOBAL_AGENCY_NBR and ' +
               'a.SY_REPORTS_GROUP_NBR = b.SY_REPORTS_GROUP_NBR and ' +
               'ENLIST_AUTOMATICALLY = ''N''' +
        'union ' +
        'select d.AGENCY_NAME, d.STATE, ' +
        'b.NAME, ' +
        'a.DEPOSIT_FREQUENCY, a.RETURN_FREQUENCY, a.SYSTEM_TAX_TYPE, ' +
        'a.NUMBER_OF_DAYS_FOR_WHEN_DUE, a.WHEN_DUE_TYPE, a.SY_GL_AGENCY_REPORT_NBR, b.MEDIA_TYPE, ' +
        '''2'' sFilter ' +
        'from SY_GL_AGENCY_REPORT a, SY_REPORTS_GROUP b, ' +
              'SY_GL_AGENCY_FIELD_OFFICE c, SY_GLOBAL_AGENCY d ' +
        'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and ' +
               'a.SY_GL_AGENCY_FIELD_OFFICE_NBR = c.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
               'c.SY_GLOBAL_AGENCY_NBR = d.SY_GLOBAL_AGENCY_NBR and ' +
               'a.SY_REPORTS_GROUP_NBR = b.SY_REPORTS_GROUP_NBR and ' +
               'ENLIST_AUTOMATICALLY = ''Y'' and ' +
               'd.SY_GLOBAL_AGENCY_NBR = 441 ' +
        'union ' +
         'select b.AGENCY_NAME, b.STATE, e.NAME, d.DEPOSIT_FREQUENCY, ' +
           'd.RETURN_FREQUENCY, d.SYSTEM_TAX_TYPE, d.NUMBER_OF_DAYS_FOR_WHEN_DUE, ' +
           ' d.WHEN_DUE_TYPE, d.SY_GL_AGENCY_REPORT_NBR, e.MEDIA_TYPE, ''3'' sFilter ' +
           'from SY_STATES a, SY_GLOBAL_AGENCY b, SY_GL_AGENCY_FIELD_OFFICE c, ' +
             'SY_GL_AGENCY_REPORT d, SY_REPORTS_GROUP e ' +
             'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and {AsOfNow<e>} and ' +
              PrepareSyList(DM_COMPANY.CO_STATES, 'SY_STATES_NBR') + ' and ' +
              'a.SY_STATE_REPORTING_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
              'c.SY_GLOBAL_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
              'c.SY_GL_AGENCY_FIELD_OFFICE_NBR = d.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
              'd.SY_REPORTS_GROUP_NBR = e.SY_REPORTS_GROUP_NBR and ' +
              'd.SYSTEM_TAX_TYPE = ''C'' and d.ENLIST_AUTOMATICALLY = ''Y'' and ' +
              ' b.SY_GLOBAL_AGENCY_NBR <> 441 ' +
        'union ' +
         'select b.AGENCY_NAME, b.STATE, e.NAME, d.DEPOSIT_FREQUENCY, d.RETURN_FREQUENCY, ' +
           'd.SYSTEM_TAX_TYPE, d.NUMBER_OF_DAYS_FOR_WHEN_DUE, d.WHEN_DUE_TYPE, ' +
           'd.SY_GL_AGENCY_REPORT_NBR, e.MEDIA_TYPE, ''3'' sFilter ' +
          'from SY_SUI a, SY_GLOBAL_AGENCY b, SY_GL_AGENCY_FIELD_OFFICE c, ' +
            'SY_GL_AGENCY_REPORT d, SY_REPORTS_GROUP e ' +
            'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and {AsOfNow<e>} and ' +
             PrepareSyList(DM_COMPANY.CO_SUI, 'SY_SUI_NBR') + ' and ' +
             'a.SY_SUI_REPORTING_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
             'c.SY_GLOBAL_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
             'c.SY_GL_AGENCY_FIELD_OFFICE_NBR = d.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
             'd.SY_REPORTS_GROUP_NBR = e.SY_REPORTS_GROUP_NBR and ' +
             'd.SYSTEM_TAX_TYPE = ''D'' and d.ENLIST_AUTOMATICALLY = ''Y''   and ' +
             'b.SY_GLOBAL_AGENCY_NBR <> 441 ' +
        'union ' +
         'select b.AGENCY_NAME, b.STATE, e.NAME, d.DEPOSIT_FREQUENCY, d.RETURN_FREQUENCY, ' +
           'd.SYSTEM_TAX_TYPE, d.NUMBER_OF_DAYS_FOR_WHEN_DUE, d.WHEN_DUE_TYPE, ' +
           'd.SY_GL_AGENCY_REPORT_NBR, e.MEDIA_TYPE, ''3'' sFilter ' +
          'from SY_LOCALS a, SY_GLOBAL_AGENCY b, SY_GL_AGENCY_FIELD_OFFICE c, ' +
            'SY_GL_AGENCY_REPORT d, SY_REPORTS_GROUP e ' +
            'where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and {AsOfNow<d>} and {AsOfNow<e>} and ' +
             PrepareSyList(DM_COMPANY.CO_LOCAL_TAX, 'SY_LOCALS_NBR') + ' and ' +
             'a.SY_LOCAL_REPORTING_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
             'c.SY_GLOBAL_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
             'c.SY_GL_AGENCY_FIELD_OFFICE_NBR = d.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
             'd.SY_REPORTS_GROUP_NBR = e.SY_REPORTS_GROUP_NBR and ' +
             'd.SYSTEM_TAX_TYPE = ''G'' and d.ENLIST_AUTOMATICALLY = ''Y''  and ' +
             'b.SY_GLOBAL_AGENCY_NBR <> 441 ' +
        'union ' +
         'select b.AGENCY_NAME, b.STATE, e.NAME, d.DEPOSIT_FREQUENCY, d.RETURN_FREQUENCY, ' +
           'd.SYSTEM_TAX_TYPE, d.NUMBER_OF_DAYS_FOR_WHEN_DUE, d.WHEN_DUE_TYPE, ' +
           'd.SY_GL_AGENCY_REPORT_NBR, e.MEDIA_TYPE, ''4'' sFilter ' +
          'from SY_LOCALS a, SY_GLOBAL_AGENCY b, SY_GL_AGENCY_FIELD_OFFICE c, ' +
           'SY_GL_AGENCY_REPORT d, SY_REPORTS_GROUP e ' +
            'where {AsOfNow<a>} and {AsOfNow<b>} and ' +
             '{AsOfNow<c>} and {AsOfNow<d>} and ' +
             '{AsOfNow<e>} and b.STATE = ''PA'' and ' +
             sPALocalFilter + ' and ' +
            ' a.LOCAL_TYPE IN ('''+LOCAL_TYPE_SCHOOL+''',''' + LOCAL_TYPE_INC_RESIDENTIAL + ''','''+LOCAL_TYPE_INC_NON_RESIDENTIAL+''') and ' +
             'a.SY_LOCAL_REPORTING_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
             'c.SY_GLOBAL_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
             'c.SY_GL_AGENCY_FIELD_OFFICE_NBR = d.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
             'd.SY_REPORTS_GROUP_NBR = e.SY_REPORTS_GROUP_NBR and ' +
             'a.COMBINE_FOR_TAX_PAYMENTS = ''Y'' and ' +
             'd.SYSTEM_TAX_TYPE = ''G'' and d.ENLIST_AUTOMATICALLY = ''Y''  and ' +
             'b.SY_GLOBAL_AGENCY_NBR <> 441 ';

    with TExecDSWrapper.Create(s) do
    begin
       cdSyGl_Report.AlwaysCallCalcFields := True;
      cdSyGl_Report.Close;
      ctx_DataAccess.GetSyCustomData(cdSyGl_Report, AsVariant);
    end;

  DM_COMPANY.CO_SUI.Filter := '';
  DM_COMPANY.CO_SUI.Filtered := False;
  DM_COMPANY.CO_STATES.Filter := '';
  DM_COMPANY.CO_STATES.Filtered := False;
  DM_COMPANY.CO_LOCAL_TAX.Filter:= '';
  DM_COMPANY.CO_LOCAL_TAX.Filtered := false;
end;


function TEDIT_CO_AUTO_ENLIST_RETURNS.PrepareSyList(cd: TevClientDataset; field : String):String;
var
 SyList: IisStringList;
begin
  Result := '';
  SyList := TisStringList.Create;
  SyList.Clear;
  SyList.Capacity := cd.RecordCount;
  cd.First;
  while not cd.Eof do
  begin
   if cd.FieldByName(field).AsInteger > 0 then
     SyList.Add(cd.FieldByName(field).AsString);
   cd.Next;
  end;
  if SyList.Count > 0 then
     Result := BuildINsqlStatement('a.'+Field, SyList)
  else
     Result := 'a.'+Field  + ' IN (0)';
end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.Activate;
begin
    PrepareSyLevel;

    cdSyGl_Report.Filter := 'sFilter = ''1''';
    cdSyGl_Report.Filtered := True;
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filter := 'PROCESS_TYPE in ( ''A'', ''B'') ';
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filtered := True;
    CoAutoEnlistMiniNavigationFrame.DataSource := wwdsDetail;
    CoDontAutoSyReportMiniNavigationFrame.DataSource := wwdsDetail;
    CoInactiveMiniNavigationFrame.DataSource := wwdsDetail;
    CoPAEITMiniNavigationFrame.DataSource := wwdsDetail;

    CoAutoEnlistMiniNavigationFrame.SpeedButton1.OnClick := CoAutoEnlistMiniNavigationFrameSpeedButton1Click;
    CoAutoEnlistMiniNavigationFrame.SpeedButton2.OnClick := CoAutoEnlistMiniNavigationFrameSpeedButton2Click;
    CoDontAutoSyReportMiniNavigationFrame.SpeedButton1.OnClick := CoDontAutoSyReportMiniNavigationFrameSpeedButton1Click;
    CoDontAutoSyReportMiniNavigationFrame.SpeedButton2.OnClick := CoDontAutoSyReportMiniNavigationFrameSpeedButton2Click;
    CoInactiveMiniNavigationFrame.SpeedButton1.OnClick := CoInactiveMiniNavigationFrameSpeedButton1Click;
    CoInactiveMiniNavigationFrame.SpeedButton2.OnClick := CoInactiveMiniNavigationFrameSpeedButton2Click;
    CoPAEITMiniNavigationFrame.SpeedButton1.OnClick := CoPAEITMiniNavigationFrameSpeedButton1Click;
    CoPAEITMiniNavigationFrame.SpeedButton2.OnClick := CoPAEITMiniNavigationFrameSpeedButton2Click;
  inherited;
end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = tsDoAuto then
  begin
    cdSyGl_Report.Filter := 'sFilter = ''1''';
    cdSyGl_Report.Filtered := True;
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filter := 'PROCESS_TYPE in ( ''A'', ''B'') ';
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filtered := True;
  end
  else
  if PageControl1.ActivePage = tsDontAutoSyReport then
  begin
    cdSyGl_Report.Filter := 'sFilter = ''2''';
    cdSyGl_Report.Filtered := True;
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filter := 'PROCESS_TYPE = ''D'' and SY_GLOBAL_AGENCY_NBR = 441';
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filtered := True;
  end
  else
  if PageControl1.ActivePage = tsInactiveEnlist then
  begin
    cdSyGl_Report.Filter := 'sFilter = ''3''';
    cdSyGl_Report.Filtered := True;
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filter := 'PROCESS_TYPE in (''A'', ''B'') ';
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filtered := True;
  end
  else
  if PageControl1.ActivePage = tsPAAutoEnlist then
  begin
    cdSyGl_Report.Filter := 'sFilter = ''4''';
    cdSyGl_Report.Filtered := True;
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filter := 'PROCESS_TYPE in ( ''A'', ''B'') ';
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filtered := True;
  end;

end;


function TEDIT_CO_AUTO_ENLIST_RETURNS.CheckPAStateSetup: integer;
var
  Q: IevQuery;
  s : String;
begin
   s := 'select * from CO_STATES where state = ''PA'' and {AsOfNow<CO_STATES>} and CO_NBR = :CoNbr';
   Q := TevQuery.Create(s);
   Q.Params.AddValue('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
   Q.Execute;
   Result := Q.Result.RecordCount;
end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CoAutoEnlistMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  if wwdgSY_Auto_Enlist_Returns.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'] > 0 then
  begin
    if not wwdsDetail.DataSet.Locate('SY_GL_AGENCY_REPORT_NBR',
        wwdgSY_Auto_Enlist_Returns.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'], []) then
    begin
      inherited;
      CoAutoEnlistMiniNavigationFrame.InsertRecordExecute(Sender);
      if wwdsDetail.DataSet.State in [dsinsert] then
      begin
       wwdsDetail.DataSet.FieldByName('SY_GL_AGENCY_REPORT_NBR').AsInteger :=
                wwdgSY_Auto_Enlist_Returns.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'];
       wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString := PROCESS_TYPE_ENLIST;
       wwdsDetail.DataSet.FieldByName('CO_NBR').AsInteger :=
             DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger;
      end;
    end;
  end;
end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CoAutoEnlistMiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
var
  k : integer;
  NbrList: IisStringList;
begin
  inherited;
  NbrList := TisStringList.Create;
  NbrList.Capacity := wwdg_CO_Auto_Enlist_Returns.SelectedList.Count;
  for k := 0 to wwdg_CO_Auto_Enlist_Returns.SelectedList.Count - 1 do
  begin
    wwdsDetail.DataSet.GotoBookmark(wwdg_CO_Auto_Enlist_Returns.SelectedList[k]);
    NbrList.Add(wwdsDetail.DataSet.FieldByName('CO_AUTO_ENLIST_RETURNS_NBR').AsVariant);
  end;

  for k := 0 to NbrList.Count - 1 do
   if wwdsDetail.DataSet.Locate('CO_AUTO_ENLIST_RETURNS_NBR', NbrList[k], []) then
      CoAutoEnlistMiniNavigationFrame.DeleteRecordExecute(Sender);
end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CoDontAutoSyReportMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin

  if wwdgSY_DontAuto_Enlist_SYReports.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'] > 0 then
  begin
    if not wwdsDetail.DataSet.Locate('SY_GL_AGENCY_REPORT_NBR',
        wwdgSY_DontAuto_Enlist_SYReports.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'], []) then
    begin
      inherited;
      CoDontAutoSyReportMiniNavigationFrame.InsertRecordExecute(Sender);
      if wwdsDetail.DataSet.State in [dsinsert] then
      begin
       wwdsDetail.DataSet.FieldByName('SY_GL_AGENCY_REPORT_NBR').AsInteger :=
                wwdgSY_DontAuto_Enlist_SYReports.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'];
       wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString := 'D';
       wwdsDetail.DataSet.FieldByName('CO_NBR').AsInteger :=
             DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger;
      end;
    end;
  end;

end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CoDontAutoSyReportMiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
var
  k : integer;
  NbrList: IisStringList;
begin
  inherited;
  NbrList := TisStringList.Create;
  NbrList.Capacity := wwdg_CO_DontAuto_Enlist_SYReports.SelectedList.Count;
  for k := 0 to wwdg_CO_DontAuto_Enlist_SYReports.SelectedList.Count - 1 do
  begin
    wwdsDetail.DataSet.GotoBookmark(wwdg_CO_DontAuto_Enlist_SYReports.SelectedList[k]);
    NbrList.Add(wwdsDetail.DataSet.FieldByName('CO_AUTO_ENLIST_RETURNS_NBR').AsVariant);
  end;

  for k := 0 to NbrList.Count - 1 do
   if wwdsDetail.DataSet.Locate('CO_AUTO_ENLIST_RETURNS_NBR', NbrList[k], []) then
     CoDontAutoSyReportMiniNavigationFrame.DeleteRecordExecute(Sender);
end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CoInactiveMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  if wwdgSY_DontAutoInactive_Enlist_Returns.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'] > 0 then
  begin
    if not wwdsDetail.DataSet.Locate('SY_GL_AGENCY_REPORT_NBR',
        wwdgSY_DontAutoInactive_Enlist_Returns.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'], []) then
    begin
      inherited;
      CoAutoEnlistMiniNavigationFrame.InsertRecordExecute(Sender);
      if wwdsDetail.DataSet.State in [dsinsert] then
      begin
       wwdsDetail.DataSet.FieldByName('SY_GL_AGENCY_REPORT_NBR').AsInteger :=
                wwdgSY_DontAutoInactive_Enlist_Returns.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'];
       wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString := PROCESS_TYPE_ENLIST;
       wwdsDetail.DataSet.FieldByName('CO_NBR').AsInteger :=
             DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger;
      end;
    end;
  end;
end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CoInactiveMiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  CoInactiveMiniNavigationFrame.DeleteRecordExecute(Sender);

end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.cdSyGl_ReportCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  cdSyGl_ReportsDEPOSIT_FREQUENCY.AsString :=
        ReturnDescription(ReturnLocalDepFrequencyType_ComboChoices, cdSyGl_ReportDEPOSIT_FREQUENCY.AsString);
  cdSyGl_ReportsRETURN_FREQUENCY.AsString :=
        ReturnDescription(ReturnFrequencyType_ComboChoices, cdSyGl_ReportRETURN_FREQUENCY.AsString);
  cdSyGl_ReportsSYSTEM_TAX_TYPE.AsString :=
        ReturnDescription(TaxReturnType_ComboChoices, cdSyGl_ReportSYSTEM_TAX_TYPE.AsString);
  cdSyGl_ReportsWHEN_DUE_TYPE.AsString :=
        ReturnDescription(ReturnWhenDueType_ComboChoices, cdSyGl_ReportWHEN_DUE_TYPE.AsString);
  cdSyGl_ReportReturnFormat.AsString :=
        ReturnDescription(ReportResultType_ComboChoices, cdSyGl_ReportMEDIA_TYPE.AsString);
end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CopyReports(grdName: TevDBGrid);
var
  CurClient: integer;
  CurCompany: integer;
  ClientRetrieveCondition, CompanyRetrieveCondition,
  AutoEnlistRetrieveCondition, sdesc: string;

  procedure DoCopyReports;
  var
    SyReportNbr : TevClientDataset;
    k: integer;

    procedure DoCopyReportsTo( client, company: integer );
    begin
      ctx_DataAccess.OpenClient( client );

      DM_COMPANY.CO_AUTO_ENLIST_RETURNS.DataRequired('CO_NBR=' + intToStr(company) );

      SyReportNbr.first;
      while not SyReportNbr.Eof do
      begin
        if not DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Locate('CO_NBR;SY_GL_AGENCY_REPORT_NBR;PROCESS_TYPE',
             VarArrayOf([company, SyReportNbr['Nbr'], SyReportNbr['Process']]), []) then
        begin
          DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Insert;
          DM_COMPANY.CO_AUTO_ENLIST_RETURNS['CO_NBR'] := company;
          DM_COMPANY.CO_AUTO_ENLIST_RETURNS['SY_GL_AGENCY_REPORT_NBR'] := SyReportNbr['Nbr'];
          DM_COMPANY.CO_AUTO_ENLIST_RETURNS['PROCESS_TYPE'] := SyReportNbr['Process'];
          DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Post;
        end;
        SyReportNbr.Next;
      end;

      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_AUTO_ENLIST_RETURNS]);
    end;
  begin
    with TCompanyChoiceQuery.Create( Self ) do
    try
      SetFilter( 'CUSTOM_COMPANY_NUMBER<>'''+DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'''' );
      sdesc := 'report ';
      Caption :=format('Select the companies you want this %s copied to',[sdesc]);
      ConfirmAllMessage := format('This operation will copy the %s to all clients and companies. Are you sure you want to do this?',[sdesc]);

      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
         SyReportNbr := TevClientDataSet.Create(nil);
         try
            SyReportNbr.FieldDefs.Add('Nbr', ftInteger, 0, True);
            SyReportNbr.FieldDefs.Add('Process', ftString, 1, True);
            SyReportNbr.CreateDataSet;
            SyReportNbr.LogChanges := False;

            for k := 0 to grdName.SelectedList.Count - 1 do
            begin
              DM_COMPANY.CO_AUTO_ENLIST_RETURNS.GotoBookmark(grdName.SelectedList[k]);
              SyReportNbr.AppendRecord([DM_COMPANY.CO_AUTO_ENLIST_RETURNS['SY_GL_AGENCY_REPORT_NBR'],
                                        DM_COMPANY.CO_AUTO_ENLIST_RETURNS['PROCESS_TYPE']])
            end;

            DM_TEMPORARY.TMP_CO.DisableControls;
            try
              for k := 0 to dgChoiceList.SelectedList.Count - 1 do
              begin
                cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                DoCopyReportsTo( cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger );
              end;
            finally
              DM_TEMPORARY.TMP_CO.EnableControls;
            end;

         finally
          SyReportNbr.Free;
         end;
      end;
    finally
      Free;
    end;
  end;
begin

  CurClient := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  ClientRetrieveCondition := DM_CLIENT.CL.RetrieveCondition;

  CurCompany := DM_COMPANY.CO['CO_NBR'];
  CompanyRetrieveCondition := DM_COMPANY.CO.RetrieveCondition;

  AutoEnlistRetrieveCondition := DM_COMPANY.CO_AUTO_ENLIST_RETURNS.RetrieveCondition;

  try
   if grdName.SelectedList.Count > 0 then
    DoCopyReports;
  finally
    ctx_DataAccess.OpenClient( CurClient );

    DM_CLIENT.CL.DataRequired(ClientRetrieveCondition );
    DM_COMPANY.CO.DataRequired(CompanyRetrieveCondition );
    DM_COMPANY.CO_AUTO_ENLIST_RETURNS.DataRequired(AutoEnlistRetrieveCondition);

    wwdsList.dataset.Locate('CO_NBR;CL_NBR', VarArrayOf([CurCompany, CurClient ]), []);
    DM_COMPANY.CO.Locate('CO_NBR',CurCompany,[]);
  end;

end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CopyServiceUpdate(Sender: TObject);
begin
  inherited;
  (Sender as Taction).Enabled := wwdg_CO_Auto_Enlist_Returns.SelectedList.Count > 0;

end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CopyServiceExecute(Sender: TObject);
begin
  inherited;
   CopyReports(wwdg_CO_Auto_Enlist_Returns);
   DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filter := 'PROCESS_TYPE = ''A''';
   DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filtered := True;

end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CopyServiceDontEnlistUpdate(
  Sender: TObject);
begin
  inherited;
  (Sender as Taction).Enabled := wwdg_CO_DontAuto_Enlist_SYReports.SelectedList.Count > 0;
end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CopyServiceDontEnlistExecute(
  Sender: TObject);
begin
  inherited;
   CopyReports(wwdg_CO_DontAuto_Enlist_SYReports);
   DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filter := 'PROCESS_TYPE = ''D'' and SY_GLOBAL_AGENCY_NBR = 441';
   DM_COMPANY.CO_AUTO_ENLIST_RETURNS.Filtered := True;
end;


procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CoPAEITMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;

  if wwdgSY_Auto_Enlist_Returns.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'] > 0 then
  begin
    if not wwdsDetail.DataSet.Locate('SY_GL_AGENCY_REPORT_NBR',
        wwdgSY_Auto_Enlist_Returns.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'], []) then
    begin
      inherited;
      CoPAEITMiniNavigationFrame.InsertRecordExecute(Sender);
      if wwdsDetail.DataSet.State in [dsinsert] then
      begin
       wwdsDetail.DataSet.FieldByName('SY_GL_AGENCY_REPORT_NBR').AsInteger :=
                wwdgSY_Auto_Enlist_Returns.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'];
       wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString := PROCESS_TYPE_PA_ENLIST;
       wwdsDetail.DataSet.FieldByName('CO_NBR').AsInteger :=
             DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger;
      end;
    end;
  end;

end;

procedure TEDIT_CO_AUTO_ENLIST_RETURNS.CoPAEITMiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
var
  k : integer;
  NbrList: IisStringList;
begin
  inherited;

  NbrList := TisStringList.Create;
  NbrList.Capacity := wwdg_CO_PAEITLocalReturns.SelectedList.Count;
  for k := 0 to wwdg_CO_PAEITLocalReturns.SelectedList.Count - 1 do
  begin
    wwdsDetail.DataSet.GotoBookmark(wwdg_CO_PAEITLocalReturns.SelectedList[k]);
    NbrList.Add(wwdsDetail.DataSet.FieldByName('CO_AUTO_ENLIST_RETURNS_NBR').AsVariant);
  end;

  for k := 0 to NbrList.Count - 1 do
   if wwdsDetail.DataSet.Locate('CO_AUTO_ENLIST_RETURNS_NBR', NbrList[k], []) then
      CoPAEITMiniNavigationFrame.DeleteRecordExecute(Sender);

end;

initialization
  RegisterClass(TEDIT_CO_AUTO_ENLIST_RETURNS);


end.
