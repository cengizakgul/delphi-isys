// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_UNIONS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, StdCtrls, wwdblook, Grids,
  Wwdbigrd, Wwdbgrid, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBLookupCombo,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_UNIONS = class(TEDIT_CO_BASE)
    tshtUnions: TTabSheet;
    wwDBGrid1: TevDBGrid;
    sbUnionDetails: TScrollBox;
    pnlSummary: TisUIFashionPanel;
    wwdgUnions: TevDBGrid;
    pnlUnionDetails: TisUIFashionPanel;
    lablUnion: TevLabel;
    wwlcUnion: TevDBLookupCombo;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CO_UNIONS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_UNION, aDS);
  inherited;
end;

function TEDIT_CO_UNIONS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_UNIONS;
end;

function TEDIT_CO_UNIONS.GetInsertControl: TWinControl;
begin
  Result := wwlcUnion;
end;

initialization
  RegisterClass(TEDIT_CO_UNIONS);

end.
