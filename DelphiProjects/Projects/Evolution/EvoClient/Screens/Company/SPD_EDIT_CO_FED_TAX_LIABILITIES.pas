// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_FED_TAX_LIABILITIES;

interface

uses
  SDataStructure,  SPD_EDIT_CO_TAX_LIABILITIES_BASE, StdCtrls,
  DBCtrls, Db,  Wwdatsrc, wwdbdatetimepicker, wwdbedit,
  wwdblook, ExtCtrls, Buttons, Mask, Wwdotdot, Wwdbcomb, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, Controls, Classes, SysUtils, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDDClasses, SDataDictclient, SDataDicttemp, ISBasicClasses , variants, EvUIComponents, EvClientDataSet,
  ImgList, isUIwwDBLookupCombo, isUIEdit, isUIwwDBEdit,
  isUIwwDBDateTimePicker, isUIDBMemo, isUIwwDBComboBox, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_FED_TAX_LIABILITIES = class(TEDIT_CO_TAX_LIABILITIES_BASE)
    lablTax_Type: TevLabel;
    lablDue_Date: TevLabel;
    lablAdjustment_Type: TevLabel;
    lablStatus: TevLabel;
    lablAmount: TevLabel;
    wwcbAdjustment_Type: TevDBComboBox;
    wwcbStatus: TevDBComboBox;
    wwcbType: TevDBComboBox;
    wwedAmount: TevDBEdit;
    wwdpDue_Date: TevDBDateTimePicker;
    lcbType: TevDBComboBox;
    fpFederalTax: TisUIFashionPanel;
    Label1x: TevLabel;
    lablTotalTaxLessFUI: TevLabel;
    procedure fpFederalTaxResize(Sender: TObject);
    procedure Panel3Resize(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure ReCalcTotals; override;
    procedure Activate; override;
    constructor Create(AOwner: TComponent); override;
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TEDIT_CO_FED_TAX_LIABILITIES.Activate;
begin
  inherited;
  wwDBGrid2x.ControlType.Clear;
  wwDBGrid2x.ControlType.Add('ADJUSTMENT_TYPE;CustomEdit;lcbAdjType');
  wwDBGrid2x.ControlType.Add('TAX_TYPE;CustomEdit;lcbType;F');
  wwdgTaxLiabilities.ControlType.Clear;
  wwdgTaxLiabilities.ControlType.Add('ADJUSTMENT_TYPE;CustomEdit;lcbAdjType');
  wwdgTaxLiabilities.ControlType.Add('TAX_TYPE;CustomEdit;lcbType;F');

  wwcbStatus.OnDropDown := wwcbStatusDropDown;
  wwcbStatus.OnCloseUp := wwcbStatusCloseUp;
end;

constructor TEDIT_CO_FED_TAX_LIABILITIES.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENTS
  pnlSubbrowse.Parent := fpFederalTax;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_FED_TAX_LIABILITIES.fpFederalTaxResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpFederalTax.Width  - 40;
  pnlSubbrowse.Height := fpFederalTax.Height - 65;
end;

function TEDIT_CO_FED_TAX_LIABILITIES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_FED_TAX_LIABILITIES;
end;

function TEDIT_CO_FED_TAX_LIABILITIES.GetInsertControl: TWinControl;
begin
  Result := wwcbType;
end;

procedure TEDIT_CO_FED_TAX_LIABILITIES.Panel3Resize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RELOCATING
  cbShowCredit.Left := Panel3.Width - 140;
  cbGroup.Left      := cbShowCredit.Left - 115;
end;

procedure TEDIT_CO_FED_TAX_LIABILITIES.ReCalcTotals;
var
  TotalTax: Currency;
  TotalTaxLessFUI: Currency;
begin
  TotalTax := 0;
  TotalTaxLessFUI := 0;

  with wwdsDetail.DataSet do
  begin
    First;
    DisableControls;
    while not EOF do
    begin

                //  Added By CA...  If any record of AMOUNT has null,  skip it
      if not VarIsNull(wwdsDetail.DataSet.FieldByName('AMOUNT').Value) then
       begin

        TotalTax := TotalTax + wwdsDetail.DataSet.FieldByName('AMOUNT').Value;
        if wwdsDetail.DataSet.FieldByName('TAX_TYPE').AsString <> 'U' then
          TotalTaxLessFUI := TotalTaxLessFUI + wwdsDetail.DataSet.FieldByName('AMOUNT').Value;

       end;

      Next;
    end;
    First;
    EnableControls;
  end;
  lablTotalTaxValue.Caption := CurrToStrF(TotalTax, ffCurrency, 2);
  lablTotalTaxLessFUI.Caption := CurrToStrF(TotalTaxLessFUI, ffCurrency, 2);
end;





initialization
  RegisterClass(TEDIT_CO_FED_TAX_LIABILITIES);

end.
