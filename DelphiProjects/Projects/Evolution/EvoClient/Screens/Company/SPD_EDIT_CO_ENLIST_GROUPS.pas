// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_ENLIST_GROUPS;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, StdCtrls, ExtCtrls,
  DBCtrls, wwdbdatetimepicker, wwdblook, Wwdotdot, Wwdbcomb, Mask,
  wwdbedit, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Controls, Classes,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents, Variants,
  EvDataAccessComponents, SDataDictsystem, SDataDictclient, SDataDicttemp,
  Forms, SPD_MiniNavigationFrame, SPackageEntry, EvConsts, SFieldCodeValues,
  EvExceptions, Menus, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBLookupCombo, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_ENLIST_GROUPS = class(TEDIT_CO_BASE)
    tshtTax_Deposit: TTabSheet;
    wwdgSY_Enlist_Groups: TevDBGrid;
    wwdg_CO_Enlist_Groups: TevDBGrid;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    dsSyEnlist: TevDataSource;
    cdSyEnlist: TevClientDataSet;
    CoEnlistReturnMiniNavigationFrame: TMiniNavigationFrame;
    pmAutoReports: TevPopupMenu;
    MenuItem3: TMenuItem;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    fpCOEnlistGroupSystem: TisUIFashionPanel;
    fpCOEnlistGroupCompany: TisUIFashionPanel;
    fpCOEnlistGroupDetail: TisUIFashionPanel;
    lablName: TevLabel;
    edReportGroup: TevDBLookupCombo;
    evLabel1: TevLabel;
    cbProcessType: TevDBComboBox;
    evLblMediaType: TevLabel;
    cbMediaType: TevDBComboBox;
    sbEnlistGroups: TScrollBox;
    Procedure GetCoEnlistFilter;
    Procedure SetMediaTypeComboBox(Group: Integer);
    procedure SetMediaTypeVisible(Group: Integer);    
    procedure CoEnlistReturnMiniNavigationFrameSpeedButton2Click(
      Sender: TObject);
    procedure CoEnlistReturnMiniNavigationFrameSpeedButton1Click(
      Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure cbProcessTypeChange(Sender: TObject);
    procedure cbProcessTypeExit(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
  private
      { Private declarations }
    procedure CopyReports;      
  protected
    procedure AfterDataSetsReopen; override;
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure AfterClick( Kind: Integer ); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;


implementation

{$R *.DFM}

uses EvUtils, SysUtils, SPD_CompanyChoiceQuery, SFrameEntry,ISBasicUtils;


procedure TEDIT_CO_ENLIST_GROUPS.GetCoEnlistFilter;
var
 s : String;
begin
   s := '-1';
   with wwdsDetail.DataSet do
   begin
     First;
     while not Eof do
     begin
        s := s + ','+ FieldByName('SY_REPORT_GROUPS_NBR').AsString;
        Next;
     end;
   end;
  cdSyEnlist.Filter := ' Not SY_REPORT_GROUPS_NBR in ( ' + s + ')';
  cdSyEnlist.Filtered := True;
end;

procedure TEDIT_CO_ENLIST_GROUPS.SetMediaTypeComboBox(Group: Integer);
var
 i : integer;
 s, sTilde : String;

begin
    sTilde := '~';
    if Group = TAX_RETURN_GROUP_W2 then
     s := W2MediaType_ComboChoices
    else
    if Group = TAX_RETURN_GROUP_1099M then
     s := MediaType1099M_ComboChoices
    else
    if Group = TAX_RETURN_GROUP_1095B then
     s := MediaType1095B_ComboChoices
    else
    if Group = TAX_RETURN_GROUP_1095C then
     s := MediaType1095C_ComboChoices
    else
    begin
     s := MediaType_ComboChoices;
     sTilde := '';
    end;
    if cbProcessType.ItemIndex = 1 then sTilde := '';
    evLblMediaType.Caption := sTilde + 'Media Type      ';
    cbMediaType.Items.Text := s;
    cbMediaType.ApplyList;
    cbMediaType.ItemIndex := -1;
    if (assigned(cbMediaType.Field)) and (not cbMediaType.Field.IsNull) then
    begin
      for i:=0 to cbMediaType.Items.Count - 1 do
      begin
         s := cbMediaType.Items[i];
         s := GetPrevStrValue(s, #9);
         if ansicomparestr(s, Trim(cbMediaType.Field.AsString))=0 then
         begin
            cbMediaType.ItemIndex := i;
            break;
         end;
      end;
    end;

end;

procedure TEDIT_CO_ENLIST_GROUPS.SetMediaTypeVisible(Group: Integer);
begin
    if (Group = TAX_RETURN_GROUP_W2) or (Group = TAX_RETURN_GROUP_1099M) or
       (Group = TAX_RETURN_GROUP_1095B) or (Group = TAX_RETURN_GROUP_1095C) then
    begin
     evLblMediaType.Visible := cbProcessType.ItemIndex = 0;
     cbMediaType.Visible := cbProcessType.ItemIndex = 0;
    end
    else
    begin
     evLblMediaType.Visible := False;
     cbMediaType.Visible := False;
    end;
end;

procedure TEDIT_CO_ENLIST_GROUPS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind in [NavOk, NavCommit] then
  begin
   if wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger  in [TAX_RETURN_GROUP_W2,TAX_RETURN_GROUP_1099M, TAX_RETURN_GROUP_1095B, TAX_RETURN_GROUP_1095C] then
    if cbProcessType.ItemIndex = 0 then
     if cbMediaType.ItemIndex = -1 then
      raise EUpdateError.CreateHelp('You need to select Media Type.', IDH_ConsistencyViolation);
  end;
end;

procedure TEDIT_CO_ENLIST_GROUPS.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavOk, NavCommit:
    begin
      GetCoEnlistFilter;
      (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
    end;
  end;
end;

procedure TEDIT_CO_ENLIST_GROUPS.AfterDataSetsReopen;
begin
  inherited;
  GetCoEnlistFilter;
  (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
end;


function TEDIT_CO_ENLIST_GROUPS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_ENLIST_GROUPS;
end;

function TEDIT_CO_ENLIST_GROUPS.GetInsertControl: TWinControl;
begin
  Result := cbProcessType;
end;

procedure TEDIT_CO_ENLIST_GROUPS.Activate;
var
 s : String;
begin
  s := 'Select a.NAME, ''Enlist'' Process, ''A'' Process_type, a.SY_REPORT_GROUPS_NBR ' +
     'from SY_REPORT_GROUPS a ' +
     'where {AsOfNow<a>} and ' +
     'a.NAME like ''Tax Return-%'' and not a.NAME like ''%Corrections%''';

   with TExecDSWrapper.Create(s) do
   begin
     cdSyEnlist.Close;
     ctx_DataAccess.GetSyCustomData(cdSyEnlist, AsVariant);
   end;
   cdSyEnlist.LogChanges := False;

  GetCoEnlistFilter;
  CoEnlistReturnMiniNavigationFrame.DataSource := wwdsDetail;
  CoEnlistReturnMiniNavigationFrame.InsertFocusControl := cbProcessType;
  CoEnlistReturnMiniNavigationFrame.SpeedButton1.OnClick := CoEnlistReturnMiniNavigationFrameSpeedButton1Click;
  CoEnlistReturnMiniNavigationFrame.SpeedButton2.OnClick := CoEnlistReturnMiniNavigationFrameSpeedButton2Click;
  inherited;

end;

procedure TEDIT_CO_ENLIST_GROUPS.CoEnlistReturnMiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;

  CoEnlistReturnMiniNavigationFrame.DeleteRecordExecute(Sender);
  GetCoEnlistFilter;
end;

procedure TEDIT_CO_ENLIST_GROUPS.CoEnlistReturnMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  if wwdgSY_Enlist_Groups.DataSource.DataSet['SY_REPORT_GROUPS_NBR'] > 0 then
  begin
    CoEnlistReturnMiniNavigationFrame.InsertRecordExecute(Sender);
    if wwdsDetail.DataSet.State in [dsinsert] then
    begin
     wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger :=
              wwdgSY_Enlist_Groups.DataSource.DataSet['SY_REPORT_GROUPS_NBR'];
     wwdsDetail.DataSet.FieldByName('CO_NBR').AsInteger :=  DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger;
     wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString := PROCESS_TYPE_DONT_ENLIST;

     SetMediaTypeComboBox(wwdgSY_Enlist_Groups.DataSource.DataSet['SY_REPORT_GROUPS_NBR']);

     evLblMediaType.Visible := False;
     cbMediaType.Visible := False;
    end;
  end;
end;

procedure TEDIT_CO_ENLIST_GROUPS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (wwdsDetail.DataSet.State = dsBrowse) and (wwdsDetail.DataSet.RecordCount > 0) then
  begin
    SetMediaTypeComboBox(wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger);
    if wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString = 'A' then
      cbProcessType.ItemIndex := 0
    else
    if wwdsDetail.DataSet.FieldByName('PROCESS_TYPE').AsString = 'D' then
      cbProcessType.ItemIndex := 1;
  end;

  if assigned(evLblMediaType) then
    SetMediaTypeVisible(wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger);
end;

procedure TEDIT_CO_ENLIST_GROUPS.cbProcessTypeExit(Sender: TObject);
begin
  inherited;
  if (cbProcessType.ItemIndex = 0) and (wwdsDetail.DataSet.State in [dsinsert, dsEdit]) then
    wwdsDetail.DataSet.FieldByName('MEDIA_TYPE').Value := Null;
end;

procedure TEDIT_CO_ENLIST_GROUPS.cbProcessTypeChange(Sender: TObject);
begin
  inherited;
  SetMediaTypeVisible(wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger);
  if wwdsDetail.DataSet.State in [dsinsert, dsEdit] then
  begin
    wwdsDetail.DataSet.FieldByName('MEDIA_TYPE').Value := Null;
    SetMediaTypeComboBox(wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger);
  end;

end;


procedure TEDIT_CO_ENLIST_GROUPS.CopyReports;
var
  CurClient: integer;
  CurCompany: integer;
  ClientRetrieveCondition, CompanyRetrieveCondition,
  AutoEnlistRetrieveCondition, sdesc: string;
  pSY_REPORT_GROUPS_NBR, pMEDIA_TYPE, pPROCESS_TYPE : variant;

  procedure DoCopyReports;
  var
    k: integer;

    procedure DoCopyReportsTo( client, company: integer );
    begin
      ctx_DataAccess.OpenClient( client );
      DM_COMPANY.CO_ENLIST_GROUPS.DataRequired('CO_NBR=' + intToStr(company) );

      if not DM_COMPANY.CO_ENLIST_GROUPS.Locate('CO_NBR;SY_REPORT_GROUPS_NBR',
              VarArrayOf([company,pSY_REPORT_GROUPS_NBR]), []) then
      begin
        DM_COMPANY.CO_ENLIST_GROUPS.Insert;
        DM_COMPANY.CO_ENLIST_GROUPS['CO_NBR'] := company;
        DM_COMPANY.CO_ENLIST_GROUPS['SY_REPORT_GROUPS_NBR'] := pSY_REPORT_GROUPS_NBR;
        DM_COMPANY.CO_ENLIST_GROUPS['MEDIA_TYPE'] := pMEDIA_TYPE;
        DM_COMPANY.CO_ENLIST_GROUPS['PROCESS_TYPE'] := pPROCESS_TYPE;
        DM_COMPANY.CO_ENLIST_GROUPS.Post;
      end
      else
      if (DM_COMPANY.CO_ENLIST_GROUPS['MEDIA_TYPE'] <> pMEDIA_TYPE)
      or (DM_COMPANY.CO_ENLIST_GROUPS['PROCESS_TYPE'] <> pPROCESS_TYPE) then
      begin
        DM_COMPANY.CO_ENLIST_GROUPS.Edit;
        DM_COMPANY.CO_ENLIST_GROUPS['MEDIA_TYPE'] := pMEDIA_TYPE;
        DM_COMPANY.CO_ENLIST_GROUPS['PROCESS_TYPE'] := pPROCESS_TYPE;
        DM_COMPANY.CO_ENLIST_GROUPS.Post;
      end;

      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_ENLIST_GROUPS]);
    end;
  begin
    with TCompanyChoiceQuery.Create( Self ) do
    try
      SetFilter( 'CUSTOM_COMPANY_NUMBER<>'''+DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'''' );
      sdesc := 'report ';
      Caption :=format('Select the companies you want this %s copied to',[sdesc]);
      ConfirmAllMessage := format('This operation will copy the %s to all clients and companies. Are you sure you want to do this?',[sdesc]);

      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
         DM_TEMPORARY.TMP_CO.DisableControls;
         try
           for k := 0 to dgChoiceList.SelectedList.Count - 1 do
           begin
             cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
             DoCopyReportsTo( cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger );
           end;
         finally
           DM_TEMPORARY.TMP_CO.EnableControls;
         end;
      end;
    finally
      Free;
    end;
  end;
begin

  CurClient := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  ClientRetrieveCondition := DM_CLIENT.CL.RetrieveCondition;

  CurCompany := DM_COMPANY.CO['CO_NBR'];
  CompanyRetrieveCondition := DM_COMPANY.CO.RetrieveCondition;

  AutoEnlistRetrieveCondition := DM_COMPANY.CO_ENLIST_GROUPS.RetrieveCondition;

  wwdsDetail.DataSet.DisableControls;
  pSY_REPORT_GROUPS_NBR := wwdsDetail.DataSet['SY_REPORT_GROUPS_NBR'];
  pMEDIA_TYPE := wwdsDetail.DataSet['MEDIA_TYPE'];
  pPROCESS_TYPE := wwdsDetail.DataSet['PROCESS_TYPE'];
  try
    DoCopyReports;
  finally
    ctx_DataAccess.OpenClient( CurClient );

    DM_CLIENT.CL.DataRequired(ClientRetrieveCondition );
    DM_COMPANY.CO.DataRequired(CompanyRetrieveCondition );
    DM_COMPANY.CO_ENLIST_GROUPS.DataRequired(AutoEnlistRetrieveCondition);

    wwdsList.dataset.Locate('CO_NBR;CL_NBR', VarArrayOf([CurCompany, CurClient ]), []);
    DM_COMPANY.CO.Locate('CO_NBR',CurCompany,[]);
    wwdsDetail.dataset.Locate('SY_REPORT_GROUPS_NBR;MEDIA_TYPE;PROCESS_TYPE', VarArrayOf([pSY_REPORT_GROUPS_NBR, pMEDIA_TYPE, pPROCESS_TYPE]), []);
    wwdsDetail.DataSet.EnableControls;
  end;

end;



procedure TEDIT_CO_ENLIST_GROUPS.MenuItem3Click(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger > 0 then
     CopyReports;
end;

initialization
  RegisterClass(TEDIT_CO_ENLIST_GROUPS);

end.
