// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BENEFIT_GROUP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBLookupCombo,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, Mask, wwdbedit,
  isUIwwDBEdit, Wwdotdot, Wwdbcomb, isUIwwDBComboBox,
  SPackageEntry, EvConsts, EvExceptions, EvCommonInterfaces, EvDataset,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents;

type
  TEDIT_CO_BENEFIT_GROUP= class(TEDIT_CO_BASE)
    tshtGroups: TTabSheet;
    wwdgPension: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    fpCOPensionSummary: TisUIFashionPanel;
    fpCOPensionDetails: TisUIFashionPanel;
    lblName: TevLabel;
    sbPensions: TScrollBox;
    edName: TevDBEdit;
    tsEmployeeAssign: TTabSheet;
    ScrollBox1: TScrollBox;
    isUIFashionPanel1: TisUIFashionPanel;
    grdGroups: TevDBGrid;
    fpAvailable: TisUIFashionPanel;
    grdEE: TevDBGrid;
    fpAssigned: TisUIFashionPanel;
    grdEESelected: TevDBGrid;
    evBtnDelete: TevBitBtn;
    evBtnUpdate: TevBitBtn;
    cdEE: TevClientDataSet;
    cdEEEE_NBR: TIntegerField;
    cdEECUSTOM_EMPLOYEE_NUMBER: TStringField;
    cdEEEmployee_LastName_Calculate: TStringField;
    cdEEEmployee_FirstName_Calculate: TStringField;
    cdEECO_GROUP_NBR: TIntegerField;
    dsEE: TevDataSource;
    dsEEAssigned: TevDataSource;
    cdEEAssigned: TevClientDataSet;
    cdEEAssignedEE_NBR: TIntegerField;
    cdEEAssignedCUSTOM_EMPLOYEE_NUMBER: TStringField;
    cdEEAssignedEmployee_LastName_Calculate: TStringField;
    cdEEAssignedEmployee_FirstName_Calculate: TStringField;
    cdEEAssignedCO_GROUP_NBR: TIntegerField;
    cdEEAssignedCO_GROUP_MEMBER_NBR: TIntegerField;
    procedure evBtnDeleteClick(Sender: TObject);
    procedure evBtnUpdateClick(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure PageControl1Change(Sender: TObject);
  private
    FEnableHr: boolean;
    procedure SetAssignedFilter(const coGroup:string);
    procedure SetcdEEFilter(const sFilter:string; const bCond:boolean);
    procedure isGroupExist;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetIfReadOnly: Boolean; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure AfterDataSetsReopen; override;
    function GetDataSetConditions(sName: string): string; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure DeActivate; override;
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick( Kind: Integer ); override;
  end;

implementation

uses SPD_EDIT_Open_BASE, isDataSet;

{$R *.DFM}

function TEDIT_CO_BENEFIT_GROUP.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if (not Result) then
     result:= FEnableHr;
end;

procedure TEDIT_CO_BENEFIT_GROUP.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_GROUP_MEMBER, EditDataSets);
end;

function TEDIT_CO_BENEFIT_GROUP.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_GROUP;
end;

function TEDIT_CO_BENEFIT_GROUP.GetInsertControl: TWinControl;
begin
  Result := edName;
end;
                                 
procedure TEDIT_CO_BENEFIT_GROUP.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS( DM_COMPANY.CO_GROUP_MEMBER, aDS);
  inherited;
end;

function TEDIT_CO_BENEFIT_GROUP.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_GROUP_MEMBER') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_BENEFIT_GROUP.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind = NavOK then
  begin
    if (Trim(edName.Text) = '') then
        raise EUpdateError.CreateHelp('Name can''t be empty', IDH_ConsistencyViolation);

    if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
       wwdsDetail.DataSet.FieldByName('GROUP_TYPE').AsString := ESS_GROUPTYPE_BENEFIT;
  end;
end;

procedure TEDIT_CO_BENEFIT_GROUP.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavCommit:
    begin
      AfterDataSetsReopen;
      isGroupExist;
    end;
  end;
end;

procedure TEDIT_CO_BENEFIT_GROUP.DeActivate;
begin
  inherited;
  DM_COMPANY.CO_GROUP.Filter := '';
  DM_COMPANY.CO_GROUP.Filtered := false;
end;

procedure TEDIT_CO_BENEFIT_GROUP.SetAssignedFilter(const coGroup:string);
begin
  if assigned(cdEEAssigned) then
  begin
    cdEEAssigned.Filter := 'CO_GROUP_NBR = ' + coGroup;
    cdEEAssigned.Filtered := True;
  end;
end;

procedure TEDIT_CO_BENEFIT_GROUP.SetcdEEFilter(const sFilter:string; const bCond:boolean);
begin
  if assigned(cdEE) then
  begin
    cdEE.Filter := sFilter;
    cdEE.Filtered := bCond;
  end;
end;

procedure TEDIT_CO_BENEFIT_GROUP.AfterDataSetsReopen;
var
   Q: IevQuery;
   coGroup:integer;
begin
  inherited;
  DM_COMPANY.CO_GROUP.Filter := 'GROUP_TYPE = ''' + ESS_GROUPTYPE_BENEFIT + '''';
  DM_COMPANY.CO_GROUP.Filtered := True;

  DM_COMPANY.EE.close;
  DM_COMPANY.EE.RetrieveCondition := 'CO_NBR = ' + DM_COMPANY.CO.fieldByName('CO_NBR').asString;
  DM_COMPANY.EE.open;

  Q := TevQuery.Create('select a.CO_GROUP_NBR, a.EE_NBR, b.GROUP_NAME, b.CO_NBR from CO_GROUP_MEMBER a, CO_GROUP b where {AsOfNow<a>} AND {AsOfNow<b>} AND ' +
                                'a.CO_GROUP_NBR = b.CO_GROUP_NBR and b.GROUP_TYPE = ''' + ESS_GROUPTYPE_BENEFIT + ''' and b.CO_NBR = :coNbr ');
  Q.Params.AddValue('coNbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
  Q.Execute;

  cdEE.EmptyDataSet;
  if cdEE.FieldDefs.Count = 0 then
     cdEE.CreateDataSet;

  cdEEAssigned.EmptyDataSet;
  if cdEEAssigned.FieldDefs.Count = 0 then
     cdEEAssigned.CreateDataSet;

  DM_COMPANY.EE.First;
  while not DM_COMPANY.EE.Eof do
  begin
     if Q.Result.Locate('EE_NBR',DM_COMPANY.EE['EE_NBR'], [] ) then
        coGroup :=  Q.Result.Fields[1].AsInteger
      else
        coGroup :=  0;
       cdEE.AppendRecord([DM_COMPANY.EE['EE_NBR'], DM_COMPANY.EE['CUSTOM_EMPLOYEE_NUMBER'],
             DM_COMPANY.EE.FieldByName('Employee_LastName_Calculate').asString,
              DM_COMPANY.EE.FieldByName('Employee_FirstName_Calculate').asString, coGroup]);
    DM_COMPANY.EE.Next;
  end;

  DM_COMPANY.CO_GROUP_MEMBER.First;
  while not DM_COMPANY.CO_GROUP_MEMBER.Eof do
  begin
      if DM_COMPANY.EE.Locate('EE_NBR',DM_COMPANY.CO_GROUP_MEMBER['EE_NBR'],[]) then
         cdEEAssigned.AppendRecord([DM_COMPANY.EE['EE_NBR'], DM_COMPANY.EE['CUSTOM_EMPLOYEE_NUMBER'],
              DM_COMPANY.EE.FieldByName('Employee_LastName_Calculate').asString,
               DM_COMPANY.EE.FieldByName('Employee_FirstName_Calculate').asString,
               DM_COMPANY.CO_GROUP_MEMBER['CO_GROUP_NBR'],
               DM_COMPANY.CO_GROUP_MEMBER['CO_GROUP_MEMBER_NBR']]);
    DM_COMPANY.CO_GROUP_MEMBER.Next;
  end;

  if DM_COMPANY.CO_GROUP.fieldByName('CO_GROUP_NBR').asString <> '' then
     SetAssignedFilter(DM_COMPANY.CO_GROUP.fieldByName('CO_GROUP_NBR').asString);
  SetcdEEFilter('CO_GROUP_NBR = 0', True);
  cdEE.First;
  cdEEAssigned.First;


  FEnableHr := GetEnableHrBenefit(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  if GetIfReadOnly then
     SetReadOnly(True)
  else
    ClearReadOnly;
end;

procedure TEDIT_CO_BENEFIT_GROUP.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if PageControl1.ActivePage = tsEmployeeAssign then
   if DM_COMPANY.CO_GROUP.fieldByName('CO_GROUP_NBR').AsString <> '' then
     SetAssignedFilter(DM_COMPANY.CO_GROUP.fieldByName('CO_GROUP_NBR').asString);
end;

procedure TEDIT_CO_BENEFIT_GROUP.isGroupExist;
var
 Q: IevQuery;
Begin
  Q := TevQuery.Create('select CO_GROUP_NBR from CO_GROUP where {AsOfNow<CO_GROUP>} and ' +
                                'GROUP_TYPE = ''' + ESS_GROUPTYPE_BENEFIT + ''' and CO_NBR = :coNbr ');
  Q.Params.AddValue('coNbr', DM_COMPANY.CO.fieldByName('CO_NBR').AsInteger);
  Q.Execute;
  evBtnDelete.Enabled  := Q.Result.RecordCount > 0;
  evBtnUpdate.Enabled  := Q.Result.RecordCount > 0;
End;

procedure TEDIT_CO_BENEFIT_GROUP.PageControl1Change(Sender: TObject);
begin
  inherited;
  if (Sender as TevPageControl).ActivePage = tsEmployeeAssign  then
     isGroupExist;
end;

procedure TEDIT_CO_BENEFIT_GROUP.evBtnDeleteClick(Sender: TObject);
var
 i : integer;
begin
  inherited;

  cdEEAssigned.DisableControls;
  cdEE.DisableControls;
  SetcdEEFilter('', false);
  try
      if grdEESelected.SelectedList.Count > 0 then
      begin
        for i := grdEESelected.SelectedList.Count-1 downto 0 do
        begin
          grdEESelected.DataSource.DataSet.GotoBookmark(grdEESelected.SelectedList[i]);

          if DM_COMPANY.CO_GROUP_MEMBER.Locate('CO_GROUP_MEMBER_NBR',
                                  grdEESelected.DataSource.DataSet['CO_GROUP_MEMBER_NBR'],[]) then
          begin
             DM_COMPANY.CO_GROUP_MEMBER.Delete;

             if cdEE.Locate('EE_NBR', cdEEAssigned.FieldByName('EE_NBR').asinteger,[]) then
             begin
               cdEE.edit;
               cdEE.FieldByName('CO_GROUP_NBR').asInteger := 0;
               cdEE.post;
             end;

             cdEEAssigned.Delete;
          end;
        end;
      end;
  finally
     SetcdEEFilter('CO_GROUP_NBR = 0', True);

     cdEEAssigned.EnableControls;
     cdEE.EnableControls;
     grdEESelected.UnselectAll;
     grdEE.UnselectAll;
  end;
end;

procedure TEDIT_CO_BENEFIT_GROUP.evBtnUpdateClick(Sender: TObject);
var
 i : integer;
begin
  inherited;

  cdEEAssigned.DisableControls;
  cdEE.DisableControls;
  try
    if grdEE.SelectedList.Count > 0 then
    begin
      for i := grdEE.SelectedList.Count-1 downto 0 do
      begin
        grdEE.DataSource.DataSet.GotoBookmark(grdEE.SelectedList[i]);

        if not cdEEAssigned.Locate('EE_NBR', grdEE.DataSource.DataSet.FieldByName('EE_NBR').asinteger,[]) then
        begin
          DM_COMPANY.CO_GROUP_MEMBER.Insert;
          DM_COMPANY.CO_GROUP_MEMBER.FieldByName('CO_GROUP_NBR').AsInteger :=
                     wwdsDetail.DataSet.FieldByName('CO_GROUP_NBR').asInteger;
          DM_COMPANY.CO_GROUP_MEMBER.FieldByName('EE_NBR').AsInteger :=
                     grdEE.DataSource.DataSet.FieldByName('EE_NBR').asinteger;
          DM_COMPANY.CO_GROUP_MEMBER.Post;

          cdEEAssigned.AppendRecord([cdEE['EE_NBR'], cdEE['CUSTOM_EMPLOYEE_NUMBER'],
              cdEE['Employee_LastName_Calculate'],cdEE['Employee_FirstName_Calculate'],
               DM_COMPANY.CO_GROUP_MEMBER['CO_GROUP_NBR'],
               DM_COMPANY.CO_GROUP_MEMBER['CO_GROUP_MEMBER_NBR']]);

          if cdEE.Locate('EE_NBR', cdEEAssigned.FieldByName('EE_NBR').asinteger,[]) then
          begin
             cdEE.edit;
             cdEE.FieldByName('CO_GROUP_NBR').asInteger := wwdsDetail.DataSet.FieldByName('CO_GROUP_NBR').asInteger;
             cdEE.post;
          end;

        end;
      end;
    end;
   finally
     cdEEAssigned.EnableControls;
     cdEE.EnableControls;
     grdEESelected.UnselectAll;
     grdEE.UnselectAll;
   end;

end;


initialization
  RegisterClass(TEDIT_CO_BENEFIT_GROUP);

end.
