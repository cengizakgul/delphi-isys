unit NewHireReportsSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ISBasicClasses,  Buttons, Grids,
  Wwdbigrd, Wwdbgrid, SDDClasses, SDataStructure, DB, EvContext, EvUIComponents,
  Wwdatsrc, EvUtils, EvConsts, SReportSettings, SDataDictbureau,
  LMDCustomButton, LMDButton, isUILMDButton, ExtCtrls, isUIFashionPanel;

type
  TNewHireReportSetup = class(TForm)
    btnOK: TevBitBtn;
    btnCancel: TevBitBtn;
    evLabel1: TevLabel;
    evDataSource1: TevDataSource;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    fpNewHireReport: TisUIFashionPanel;
    grReports: TevDBCheckGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    FSetups: TList;
    procedure ReadSetupInfo;
    procedure WriteSetupInfo;
  public
  end;

implementation

uses kbmMemTable;

{$R *.dfm}

procedure TNewHireReportSetup.FormCreate(Sender: TObject);
begin
  FSetups := TList.Create;

  DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.DataRequired('REPORT_LEVEL = ''' + CH_DATABASE_SYSTEM + ''' and REPORT_NBR = 1072');

  ReadSetupInfo;
end;

procedure TNewHireReportSetup.FormDestroy(Sender: TObject);
begin
  DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.Close;
  FreeAndNil(FSetups);
end;

procedure TNewHireReportSetup.ReadSetupInfo;
var
  RepParams: TrwReportParams;
  aClients, aCompanies: Variant;
  i, CL_NBR, CO_NBR: Integer;
begin
  CL_NBR := ctx_DataAccess.ClientID;
  CO_NBR := DM_COMPANY.CO.CO_NBR.AsInteger;

  with DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS do
  begin
    RepParams := TrwReportParams.Create;
    DisableControls;
    try
      First;
      while not Eof do
      begin
        RepParams.ReadFromBlobField(INPUT_PARAMS);
        aClients := Unassigned;
        aCompanies := Unassigned;
        i := RepParams.IndexOf('Clients');
        if i <> -1 then
        begin
          aClients := RepParams[i].Value;
          i := RepParams.IndexOf('Companies');
          if i <> -1 then
            aCompanies := RepParams[i].Value
        end;

        if not VarIsClear(aClients) and not VarIsClear(aCompanies) then
          for i := VarArrayLowBound(aClients, 1) to VarArrayHighBound(aClients, 1) do
            if (aClients[i] = CL_NBR) and (aCompanies[i] = CO_NBR) then
            begin
              grReports.SelectedList.Add(GetBookmark);
              FSetups.Add(Pointer(SB_MULTICLIENT_REPORTS_NBR.AsInteger));
              break;
            end;

        Next;
      end;
    finally
      First;
      EnableControls;
      FreeAndNil(RepParams);
    end;
  end;
end;

procedure TNewHireReportSetup.WriteSetupInfo;
var
  i, j, CL_NBR, CO_NBR: Integer;
  BM: TBookmark;
  fl: Boolean;

  procedure ExcludeCompany;
  var
    RepParams: TrwReportParams;
    aClients, aCompanies: Variant;
    i, j: Integer;
  begin
    with DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS do
    begin
      RepParams := TrwReportParams.Create;
      try
        RepParams.ReadFromBlobField(INPUT_PARAMS);
        aClients := RepParams[RepParams.IndexOf('Clients')].Value;
        aCompanies := RepParams[RepParams.IndexOf('Companies')].Value;

        i := VarArrayLowBound(aClients, 1);
        while not ((aClients[i] = CL_NBR) and (aCompanies[i] = CO_NBR)) and (i <= VarArrayHighBound(aClients, 1)) do
          inc(i);

        if (aClients[i] = CL_NBR) and (aCompanies[i] = CO_NBR) then
          for j := i + 1 to VarArrayHighBound(aClients, 1) do
          begin
            aClients[j - 1] := aClients[j];
            aCompanies[j - 1] := aCompanies[j];
          end;

        VarArrayRedim(aClients, VarArrayHighBound(aClients, 1) - 1);
        VarArrayRedim(aCompanies, VarArrayHighBound(aCompanies, 1) - 1);

        RepParams[RepParams.IndexOf('Clients')].Value := aClients;
        RepParams[RepParams.IndexOf('Companies')].Value := aCompanies;

        Edit;
        RepParams.SaveToBlobField(INPUT_PARAMS);
        Post;

      finally
        FreeAndNil(RepParams);
      end;
    end;
  end;

  procedure IncludeCompany;
  var
    RepParams: TrwReportParams;
    aClients, aCompanies: Variant;
  begin
    with DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS do
    begin
      RepParams := TrwReportParams.Create;
      try
        RepParams.ReadFromBlobField(INPUT_PARAMS);
        if RepParams.IndexOf('Clients') = -1 then
          RepParams.Add('Clients', VarArrayOf([]), True);
        if RepParams.IndexOf('Companies') = -1 then
          RepParams.Add('Companies', VarArrayOf([]), True);

        aClients := RepParams[RepParams.IndexOf('Clients')].Value;
        aCompanies := RepParams[RepParams.IndexOf('Companies')].Value;
        VarArrayRedim(aClients, VarArrayHighBound(aClients, 1) + 1);
        VarArrayRedim(aCompanies, VarArrayHighBound(aCompanies, 1) + 1);
        aClients[VarArrayHighBound(aClients, 1)] := CL_NBR;
        aCompanies[VarArrayHighBound(aCompanies, 1)] := CO_NBR;

        RepParams[RepParams.IndexOf('Clients')].Value := aClients;
        RepParams[RepParams.IndexOf('Companies')].Value := aCompanies;

        Edit;
        RepParams.SaveToBlobField(INPUT_PARAMS);
        Post;

      finally
        FreeAndNil(RepParams);
      end;
    end;
  end;

begin
  CL_NBR := ctx_DataAccess.ClientID;
  CO_NBR := DM_COMPANY.CO.CO_NBR.AsInteger;

  with DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS do
  begin
    DisableControls;
    try
      for i := 0 to FSetups.Count - 1 do
        if Locate('SB_MULTICLIENT_REPORTS_NBR', Integer(FSetups[i]), []) then
        begin
          BM := GetBookmark;
          fl := False;
          for j := 0 to grReports.SelectedList.Count - 1 do
            if CompareBookmarks(BM, grReports.SelectedList[j]) = 0 then
            begin
              fl := True;
              Break;
            end;

          if not fl then
            ExcludeCompany;

          FreeBookmark(BM);
        end;

      for i := 0 to grReports.SelectedList.Count - 1 do
        if BookmarkValid(grReports.SelectedList[i]) then
        begin
          GotoBookmark(grReports.SelectedList[i]);
          if FSetups.IndexOf(Pointer(SB_MULTICLIENT_REPORTS_NBR.AsInteger)) = -1 then
            IncludeCompany;
        end;

    finally
      EnableControls;
    end;
  end;

  ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS]);
end;

procedure TNewHireReportSetup.btnOKClick(Sender: TObject);
begin

  WriteSetupInfo;
  ModalResult := mrOK;
end;

procedure TNewHireReportSetup.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
