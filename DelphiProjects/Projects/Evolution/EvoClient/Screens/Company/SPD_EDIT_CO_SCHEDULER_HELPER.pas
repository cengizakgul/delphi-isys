// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_SCHEDULER_HELPER;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,  DB,  Wwdatsrc,
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, EvUIComponents, EvClientDataSet,
  ISDataAccessComponents, EvDataAccessComponents, LMDCustomButton,
  LMDButton, isUILMDButton;

type
  TSchedulerHelper = class(TForm)
    evPanel1: TevPanel;
    evDBCheckGrid1: TevDBCheckGrid;
    evBitBtn1: TevBitBtn;
    evDataSource1: TevDataSource;
    evClientDataSet1: TevClientDataSet;
    evClientDataSet1Date: TDateTimeField;
    evClientDataSet1Description: TStringField;
    evMemo1: TevMemo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SchedulerHelper: TSchedulerHelper;

implementation

{$R *.dfm}

procedure TSchedulerHelper.FormCreate(Sender: TObject);
begin
  evClientDataSet1.CreateDataSet;
end;

end.
