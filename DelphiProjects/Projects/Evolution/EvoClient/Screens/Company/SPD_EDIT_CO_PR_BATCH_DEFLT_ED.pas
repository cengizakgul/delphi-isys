// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_PR_BATCH_DEFLT_ED;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, DBCtrls, StdCtrls, Buttons, Wwdbigrd,
  Grids, Wwdbgrid, ExtCtrls, ComCtrls, wwdblook, Mask, 
  SDataStructure,  wwdbedit, SPackageEntry, ISBasicClasses,
  SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_PR_BATCH_DEFLT_ED = class(TEDIT_CO_BASE)
    TabSheet2: TTabSheet;
    wwDBGrid2: TevDBGrid;
    fpSummary: TisUIFashionPanel;
    fpDetail: TisUIFashionPanel;
    Label1: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    DBRadioGroup1: TevDBRadioGroup;
    UpBtn: TevSpeedButton;
    DownBtn: TevSpeedButton;
    fpOverrides: TisUIFashionPanel;
    Label3: TevLabel;
    Label4: TevLabel;
    Label6: TevLabel;
    Label7: TevLabel;
    Label8: TevLabel;
    DBEdit3: TevDBEdit;
    DBEdit4: TevDBEdit;
    DBEdit5: TevDBEdit;
    DBEdit6: TevDBEdit;
    DBEdit7: TevDBEdit;
    lblJob: TevLabel;
    lcJob: TevDBLookupCombo;
    sbDefaultsDetail: TScrollBox;
    procedure UpBtnClick(Sender: TObject);
    procedure DownBtnClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
  private
    procedure CheckFiller;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;


implementation

uses ISKbmMemDataSet;

{$R *.DFM}

procedure TEDIT_CO_PR_BATCH_DEFLT_ED.UpBtnClick(Sender: TObject);
var
  iPrior, iCurrent, iNbr: integer;
begin
  inherited;
  iNbr := wwdsDetail.DataSet.FieldByName(wwdsDetail.DataSet.Name + '_NBR').AsInteger;
  if Trim(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3)) <> '' then
    iCurrent := StrToInt(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3))
  else
    iCurrent := wwdsDetail.DataSet.RecNo;
  wwdsDetail.DataSet.Prior;
  if Trim(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3)) <> '' then
    iPrior := StrToInt(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3))
  else
    iPrior := wwdsDetail.DataSet.RecNo;
  wwdsDetail.DataSet.Edit;
  wwdsDetail.DataSet.FieldByName('FILLER').AsString := Format('%3.3d', [iCurrent]) + Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 4,
    Length(wwdsDetail.DataSet.FieldByName('FILLER').AsString));
  wwdsDetail.DataSet.FieldByName('FILLTRIM').AsString := Format('%3.3d', [iCurrent]);
  wwdsDetail.DataSet.Post;
  sleep(1000);
  wwdsDetail.DataSet.Locate(wwdsDetail.DataSet.Name + '_NBR', iNbr, []);
  wwdsDetail.DataSet.Edit;
  wwdsDetail.DataSet.FieldByName('FILLER').AsString := Format('%3.3d', [iPrior]) + Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 4,
    Length(wwdsDetail.DataSet.FieldByName('FILLER').AsString));
  wwdsDetail.DataSet.FieldByName('FILLTRIM').AsString := Format('%3.3d', [iPrior]);
  wwdsDetail.DataSet.Post;
end;

procedure TEDIT_CO_PR_BATCH_DEFLT_ED.DownBtnClick(Sender: TObject);
var
  iNext, iCurrent, iNbr: integer;
begin
  inherited;
  iNbr := wwdsDetail.DataSet.FieldByName(wwdsDetail.DataSet.Name + '_NBR').AsInteger;
  if Trim(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3)) <> '' then
    iCurrent := StrToInt(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3))
  else
    iCurrent := wwdsDetail.DataSet.RecNo;
  wwdsDetail.DataSet.Next;
  if Trim(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3)) <> '' then
    iNext := StrToInt(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3))
  else
    iNext := wwdsDetail.DataSet.RecNo;
  wwdsDetail.DataSet.Edit;
  wwdsDetail.DataSet.FieldByName('FILLER').AsString := Format('%3.3d', [iCurrent]) + Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 4,
    Length(wwdsDetail.DataSet.FieldByName('FILLER').AsString));
  wwdsDetail.DataSet.FieldByName('FILLTRIM').AsString := Format('%3.3d', [iCurrent]);
  wwdsDetail.DataSet.Post;
  sleep(1000);
  wwdsDetail.DataSet.Locate(wwdsDetail.DataSet.Name + '_NBR', iNbr, []);
  wwdsDetail.DataSet.Edit;
  wwdsDetail.DataSet.FieldByName('FILLER').AsString := Format('%3.3d', [iNext]) + Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 4,
    Length(wwdsDetail.DataSet.FieldByName('FILLER').AsString));
  wwdsDetail.DataSet.FieldByName('FILLTRIM').AsString := Format('%3.3d', [iNext]);
  wwdsDetail.DataSet.Post;
end;

procedure TEDIT_CO_PR_BATCH_DEFLT_ED.BitBtn1Click(Sender: TObject);
begin
  inherited;
  CheckFiller;
end;

procedure TEDIT_CO_PR_BATCH_DEFLT_ED.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_COMPANY.CO_JOBS, aDS);
  inherited;
end;

function TEDIT_CO_PR_BATCH_DEFLT_ED.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_PR_BATCH_DEFLT_ED;
end;

function TEDIT_CO_PR_BATCH_DEFLT_ED.GetInsertControl: TWinControl;
begin
  Result := wwDBLookupCombo1;
end;

procedure TEDIT_CO_PR_BATCH_DEFLT_ED.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Assigned(UpBtn) then
    UpBtn.Enabled := wwdsDetail.DataSet.Recno > 1;
  if Assigned(DownBtn) then
    DownBtn.Enabled := (wwdsDetail.DataSet.Recno < wwdsDetail.DataSet.RecordCount) and (wwdsDetail.DataSet.Recno >= 1);
end;

procedure TEDIT_CO_PR_BATCH_DEFLT_ED.Activate;
begin
  inherited;
  CheckFiller;
end;

procedure TEDIT_CO_PR_BATCH_DEFLT_ED.CheckFiller;
var
  bChanged: boolean;
begin
  bChanged := False;
  if wwdsDetail.DataSet.Active then
    with TevClientDataSet(wwdsDetail.DataSet) do
    try
      DisableControls;
      Last;
      while not Bof do
      begin
        Edit;
        if Trim(Copy(FieldByName('FILLER').AsString, 1, 3)) = '' then
        begin
          FieldByName('FILLER').AsString := Format('%3.3d', [RecNo]) + Copy(FieldByName('FILLER').AsString, 4,
            Length(FieldByName('FILLER').AsString));
          bChanged := True;
        end;
        FieldByName('FILLTRIM').AsString := Copy(FieldByName('FILLER').AsString, 1, 3);
        Post;
        Prior;
      end;
      IndexFieldNames := 'FILLTRIM';
      if not bChanged then
        MergeChangeLog;
      TFramePackageTmpl(Self.Owner).ClickButton(NavCommit);
    finally
      EnableControls;
    end;
end;

initialization
  RegisterClass(TEDIT_CO_PR_BATCH_DEFLT_ED);

end.
