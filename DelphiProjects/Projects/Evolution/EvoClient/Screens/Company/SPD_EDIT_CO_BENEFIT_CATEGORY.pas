// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BENEFIT_CATEGORY;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBLookupCombo,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, isUIwwDBComboBox, isUIwwDBEdit, Wwdbspin,
  wwdbdatetimepicker, isUIwwDBDateTimePicker, isUIDBMemo,
  SPackageEntry, EvCommonInterfaces, EvDataset, EvExceptions,
  SPD_MiniNavigationFrame;

type
  TEDIT_CO_BENEFIT_CATEGORY = class(TEDIT_CO_BASE)
    tshtCategory: TTabSheet;
    wwdgCategory: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    pnlCOBrowseRight: TevPanel;
    fpCORight: TisUIFashionPanel;
    fpCOCategorySummary: TisUIFashionPanel;
    fpIntroText: TisUIFashionPanel;
    sbPensions: TScrollBox;
    evNote: TEvDBMemo;
    cbCategoryType: TevDBComboBox;
    cbFrequency: TevDBComboBox;    
    cbbCategoryType: TevDBComboBox;
    cbbFrequency: TevDBComboBox;
    fpGeneral: TisUIFashionPanel;
    lablCategory: TevLabel;
    evcbCategoryType: TevDBComboBox;
    evLabel4: TevLabel;
    evcbFrequency: TevDBComboBox;
    evrgReadonly: TevDBRadioGroup;
    fpDaysPrior: TisUIFashionPanel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evDayPriorHR: TevDBSpinEdit;
    evDayPriorEmployee: TevDBSpinEdit;
    fpDaysBeforeEndReminder: TisUIFashionPanel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evDayBeforeHR: TevDBSpinEdit;
    evDayBeforeEmployee: TevDBSpinEdit;
    fpReoccurring: TisUIFashionPanel;
    evLabel8: TevLabel;
    evReoccurHR: TevDBSpinEdit;
    evLabel9: TevLabel;
    evReoccurEmployee: TevDBSpinEdit;
    fpBenefitRates: TisUIFashionPanel;
    fpRateDetails: TisUIFashionPanel;
    evPanel1: TevPanel;
    pnlRateFields: TevPanel;
    EnrollmentdatesMiniNavigationFrame: TMiniNavigationFrame;
    evEnrolmentStart: TevDBDateTimePicker;
    evLabel11: TevLabel;
    evEnromentEnd: TevDBDateTimePicker;
    lblEndDate: TevLabel;
    grEnrollmentDates: TevDBGrid;
    dsBenefitEnrollment: TevDataSource;
    evLabel1: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evLabel7: TevLabel;
    evLabel10: TevLabel;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    Procedure  SetCategoryTypeSecurity;
    procedure evcbCategoryTypeDropDown(Sender: TObject);
    procedure evcbCategoryTypeCloseUp(Sender: TwwDBComboBox;
      Select: Boolean);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure EnrollmentdatesMiniNavigationFrameSpeedButton1Click(
      Sender: TObject);
    procedure EnrollmentdatesMiniNavigationFrameSpeedButton2Click(
      Sender: TObject);
    procedure evcbCategoryTypeChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  Private
      FCategoryTypeList: string;
      FEnableHr: boolean;
      procedure SetBenefitEnrollmentFilter;      
  protected
    function GetIfReadOnly: Boolean; override;
    function GetDataSetConditions(sName: string): string; override;    
    procedure SetReadOnly(Value: Boolean); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure AfterDataSetsReopen; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
    procedure Activate; override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
  end;

implementation

uses SFrameEntry;

{$R *.DFM}

function TEDIT_CO_BENEFIT_CATEGORY.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if (not Result) then
     result:= FEnableHr;
end;

function TEDIT_CO_BENEFIT_CATEGORY.GetDataSetConditions(sName: string): string;
begin
  Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.AfterDataSetsReopen;
begin
  inherited;
  FEnableHr := GetEnableHrBenefit(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  if GetIfReadOnly then
     SetReadOnly(True)
  else
    ClearReadOnly;

  SetCategoryTypeSecurity;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.SetReadOnly(Value: Boolean);
begin
  inherited;
  SetCategoryTypeSecurity;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;

  if (DM_COMPANY.CO_BENEFIT_ENROLLMENT.state in  [dsEdit, dsInsert]) then
    if Kind = NavOK then
      if Value then
      begin
        (Owner as TFramePackageTmpl).NavigationDataSet := nil;
         TabSheet1.Enabled := False;
         wwdgCategory.Enabled := False;
         grEnrollmentDates.Enabled := False;
      end
      else
      begin
        (Owner as TFramePackageTmpl).NavigationDataSet := DM_COMPANY.CO_BENEFIT_CATEGORY;
        TabSheet1.Enabled := True;
        wwdgCategory.Enabled := True;
        grEnrollmentDates.Enabled := True;
      end;

end;
function TEDIT_CO_BENEFIT_CATEGORY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BENEFIT_CATEGORY;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_BENEFIT_ENROLLMENT, EditDataSets);
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_BENEFIT_ENROLLMENT, aDS);
  inherited;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.Activate;
begin
  inherited;
  //assign the navigation datasources

  EnrollmentdatesMiniNavigationFrame.DataSource := dsBenefitEnrollment;
  EnrollmentdatesMiniNavigationFrame.InsertFocusControl := evEnrolmentStart;
  EnrollmentdatesMiniNavigationFrame.SpeedButton1.OnClick := EnrollmentdatesMiniNavigationFrameSpeedButton1Click;
  EnrollmentdatesMiniNavigationFrame.SpeedButton2.OnClick := EnrollmentdatesMiniNavigationFrameSpeedButton2Click;
end;

function TEDIT_CO_BENEFIT_CATEGORY.GetInsertControl: TWinControl;
begin
  Result := evcbFrequency;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.ButtonClicked(Kind: Integer;
  var Handled: Boolean);

  function CheckEnrollmentDatesLimit: Boolean;
  var
    s, sTmp: String;
    Q: IevQuery;
  begin
    Result := false;
    if dsBenefitEnrollment.DataSet.State in [dsEdit] then
       sTmp := ' and CO_BENEFIT_ENROLLMENT_NBR <> ' + dsBenefitEnrollment.DataSet.fieldByName('CO_BENEFIT_ENROLLMENT_NBR').AsString;

    s:= 'select CO_BENEFIT_ENROLLMENT_NBR, ENROLLMENT_START_DATE, ENROLLMENT_END_DATE ' +
           ' from CO_BENEFIT_ENROLLMENT where {AsOfNow<CO_BENEFIT_ENROLLMENT>} and CO_BENEFIT_CATEGORY_NBR = :pCategory ' + sTmp +
            ' order by ENROLLMENT_START_DATE desc ';
    Q := TevQuery.Create(s);
    Q.Params.AddValue('pCategory', wwdsDetail.DataSet.FieldByName('CO_BENEFIT_CATEGORY_NBR').AsInteger);
    Q.Execute;
    if Q.Result.RecordCount > 0 then
    begin
      if Q.Result.Locate('ENROLLMENT_START_DATE', evEnrolmentStart.Date, []) then
         result := True
      else
      begin
        Q.Result.First;
        while not Q.Result.Eof do
        begin
           if (( evEnrolmentStart.Date>=Q.Result.FieldByName('ENROLLMENT_START_DATE').AsDateTime) and
               ( evEnrolmentStart.Date<=Q.Result.FieldByName('ENROLLMENT_END_DATE').AsDateTime))  then // EXCEPTION WRONG_PERIOD_BEGIN;
              result := True;

           if ((evEnromentEnd.Date>=Q.Result.FieldByName('ENROLLMENT_START_DATE').AsDateTime) and
               (evEnromentEnd.Date<=Q.Result.FieldByName('ENROLLMENT_END_DATE').AsDateTime)) then  // EXCEPTION WRONG_PERIOD_END;
              result := True;

           if (evEnrolmentStart.Date<Q.Result.FieldByName('ENROLLMENT_START_DATE').AsDateTime) and
              (evEnromentEnd.Date>Q.Result.FieldByName('ENROLLMENT_END_DATE').AsDateTime) then  // EXCEPTION WRONG_PERIOD_END;
              result := True;

           if result then
              break;

           Q.Result.Next;
        end;
      end;
    end;

  end;

begin
  inherited;

  if Kind = NavInsert then
  begin
     evcbCategoryType.SetFocus;
  end;

  if Kind = NavOK then
  begin
   if (Trim(evcbCategoryType.Text) = '') then
       raise EUpdateError.CreateHelp('Please select a Category!', IDH_ConsistencyViolation);
   if (Trim(evcbFrequency.Text) = '') then
       raise EUpdateError.CreateHelp('Please select a Frequency!', IDH_ConsistencyViolation);

   if dsBenefitEnrollment.State in [dsEdit, dsInsert] then
     dsBenefitEnrollment.DataSet.FieldByName('CO_BENEFIT_CATEGORY_NBR').AsInteger :=
        wwdsDetail.DataSet.FieldByName('CO_BENEFIT_CATEGORY_NBR').AsInteger;

   if (Trim(evEnrolmentStart.Text) = '') then
       raise EUpdateError.CreateHelp('Please select an Enrollment Start Date!', IDH_ConsistencyViolation);
   if (Trim(evEnromentEnd.Text) = '') then
       raise EUpdateError.CreateHelp('Please select an Enrollment End Date!', IDH_ConsistencyViolation);
   if CheckEnrollmentDatesLimit then
       raise EUpdateError.CreateHelp('Please check Enrolment Start Date and Enrolment End Date limits.', IDH_ConsistencyViolation);

  end;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.AfterClick(Kind: Integer);
begin
  inherited;

  if Kind in [NavOK, NavCommit, NavCancel, NavAbort] then
  begin
    (Owner as TFramePackageTmpl).NavigationDataSet := DM_COMPANY.CO_BENEFIT_CATEGORY;
    TabSheet1.Enabled := True;
    wwdgCategory.Enabled := True;
    grEnrollmentDates.Enabled := True;
  end;
end;



Procedure  TEDIT_CO_BENEFIT_CATEGORY.SetCategoryTypeSecurity;
begin
  if Assigned(wwdsDetail.DataSet) and  wwdsDetail.DataSet.Active then
  begin
   if (wwdsDetail.DataSet.State <> dsInsert) then
     evcbCategoryType.SecurityRO := GetIfReadOnly or (wwdsDetail.DataSet.RecordCount > 0);
  end;
End;

procedure TEDIT_CO_BENEFIT_CATEGORY.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then
    Exit;

  if  Assigned(wwdsDetail.DataSet) and  wwdsDetail.DataSet.Active and
     (wwdsDetail.DataSet.State = dsInsert) then
  begin
       evcbCategoryType.SecurityRO := GetIfReadOnly;
       evcbCategoryType.SetFocus;
  end;

end;

procedure TEDIT_CO_BENEFIT_CATEGORY.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;

  if csLoading in ComponentState then
    Exit;

  SetCategoryTypeSecurity;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.evcbCategoryTypeDropDown(
  Sender: TObject);
var
  i: Integer;
  Q: IevQuery;
begin
  inherited;
  FCategoryTypeList := TwwDBComboBox(Sender).Items.CommaText;

  Q := TevQuery.Create('SELECT CATEGORY_TYPE FROM CO_BENEFIT_CATEGORY t WHERE {AsOfNow<t>} and CO_NBR = :CO_NBR ');
  Q.Params.AddValue('CO_NBR', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  Q.Execute;
  if Q.Result.RecordCount > 0 then
   with TwwDBComboBox(Sender).Items do
   for i := Count - 1 downto 0 do
    if Q.Result.Locate('CATEGORY_TYPE', Strings[i][Length(Strings[i])],[]) then
       Delete(i);
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.evcbCategoryTypeCloseUp(
  Sender: TwwDBComboBox; Select: Boolean);
begin
  inherited;
    Sender.Items.CommaText := FCategoryTypeList;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.EnrollmentdatesMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;

  EnrollmentdatesMiniNavigationFrame.InsertRecordExecute(Sender);
  if dsBenefitEnrollment.DataSet.State in [dsInsert, dsEdit] then
    dsBenefitEnrollment.DataSet.FieldByName('CO_BENEFIT_CATEGORY_NBR').AsInteger :=
        wwdsDetail.DataSet.FieldByName('CO_BENEFIT_CATEGORY_NBR').AsInteger;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.EnrollmentdatesMiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  EnrollmentdatesMiniNavigationFrame.DeleteRecordExecute(Sender);

end;

procedure TEDIT_CO_BENEFIT_CATEGORY.evcbCategoryTypeChange(
  Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then
     Exit; 

  if PageControl1.ActivePage = tshtCategory then
     SetBenefitEnrollmentFilter;
end;

procedure TEDIT_CO_BENEFIT_CATEGORY.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = tshtCategory then
     SetBenefitEnrollmentFilter;
end;


procedure TEDIT_CO_BENEFIT_CATEGORY.SetBenefitEnrollmentFilter;
Begin
   if DM_COMPANY.CO_BENEFIT_ENROLLMENT.Active and (DM_COMPANY.CO_BENEFIT_ENROLLMENT.state = dsBrowse) then
   begin
    DM_COMPANY.CO_BENEFIT_ENROLLMENT.DisableControls;
    try
       DM_COMPANY.CO_BENEFIT_ENROLLMENT.IndexFieldNames := 'CO_BENEFIT_CATEGORY_NBR';
       DM_COMPANY.CO_BENEFIT_ENROLLMENT.SetRange([DM_COMPANY.CO_BENEFIT_CATEGORY['CO_BENEFIT_CATEGORY_NBR']], [DM_COMPANY.CO_BENEFIT_CATEGORY['CO_BENEFIT_CATEGORY_NBR']]);

    finally
       DM_COMPANY.CO_BENEFIT_ENROLLMENT.EnableControls;
    end;
   end;
end;

initialization
  RegisterClass(TEDIT_CO_BENEFIT_CATEGORY);

end.
