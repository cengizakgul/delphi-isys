// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_VENDOR;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, StdCtrls, ExtCtrls,
  DBCtrls, wwdbdatetimepicker, wwdblook, Wwdotdot, Wwdbcomb, Mask,
  wwdbedit, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Controls, Classes,  kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents, Variants,
  EvDataAccessComponents, SDataDictsystem, SDataDictclient, SDataDicttemp,
  Forms, SPD_MiniNavigationFrame, SPackageEntry, EvConsts, SFieldCodeValues,
  EvExceptions, Menus, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBLookupCombo, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, isUIwwDBDateTimePicker, isUIwwDBEdit,
  SDataDictbureau;

type
  TEDIT_CO_VENDOR = class(TEDIT_CO_BASE)
    tsVendorDetail: TTabSheet;
    sbCOBrowse: TScrollBox;
    pnlCOBrowseBorder: TevPanel;
    pnlCOBrowseLeft: TevPanel;
    fpCOLeft: TisUIFashionPanel;
    pnlFPCOLeftBody: TevPanel;
    sbEnlistGroups: TScrollBox;
    tsVendors: TTabSheet;
    ScrollBox1: TScrollBox;
    fpSummary: TisUIFashionPanel;
    wwdgValues: TevDBGrid;
    fpCompany: TisUIFashionPanel;
    fpDetails: TisUIFashionPanel;
    Label2: TevLabel;
    evLabel2: TevLabel;
    edtValue: TevDBEdit;
    cbPreDefine: TevDBLookupCombo;
    fpCOVendors: TisUIFashionPanel;
    grdCoVendors: TevDBGrid;
    fpCOVendorsDetail: TisUIFashionPanel;
    edtVendor: TevDBLookupCombo;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    isUIFashionPanel1: TisUIFashionPanel;
    lblVendor: TevLabel;
    dsVendorDetail: TevDataSource;
    cbDetail: TevDBLookupCombo;
    MiniNavigationFrame: TMiniNavigationFrame;
    evLabel1: TevLabel;
    procedure SetCOVendorDetail;
    procedure MiniNavigationFrameSpeedButton1Click(Sender: TObject);
    procedure MiniNavigationFrameSpeedButton2Click(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure cbDetailChange(Sender: TObject);
    procedure cbPreDefineBeforeDropDown(Sender: TObject);
    procedure cbPreDefineCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure cbDetailBeforeDropDown(Sender: TObject);
    procedure cbDetailCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure dsVendorDetailStateChange(Sender: TObject);
  private
      { Private declarations }

  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;


implementation

{$R *.DFM}

uses EvUtils, SysUtils;


procedure TEDIT_CO_VENDOR.Activate;
begin
  inherited;
  MiniNavigationFrame.DataSource := dsVendorDetail;
  MiniNavigationFrame.SpeedButton1.OnClick := MiniNavigationFrameSpeedButton1Click;
  MiniNavigationFrame.SpeedButton2.OnClick := MiniNavigationFrameSpeedButton2Click;
end;

procedure TEDIT_CO_VENDOR.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;

  if Kind = NavDelete then
    if (DM_CLIENT.CO_VENDOR_DETAIL_VALUES.RecordCount > 0) then
          raise EUpdateError.CreateHelp('This vendor has details attached.  You must delete the details to remove this vendor.', IDH_ConsistencyViolation);

  if Kind in [NavOk] then
    if (wwdsDetail.DataSet.State = dsEdit) and
        (DM_CLIENT.CO_VENDOR_DETAIL_VALUES.RecordCount > 0) and
        (wwdsDetail.DataSet.FieldByName('SB_VENDOR_NBR').OldValue <>
            wwdsDetail.DataSet.FieldByName('SB_VENDOR_NBR').NewValue) then
          raise EUpdateError.CreateHelp('This vendor has details attached.  You must delete the details before updating the vendor.  Otherwise, please add a new vendor.', IDH_ConsistencyViolation);
end;


procedure TEDIT_CO_VENDOR.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_CLIENT.CO_VENDOR_DETAIL_VALUES, EditDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_VENDOR, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_VENDOR_DETAIL, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES, SupportDataSets, '');

end;

procedure TEDIT_CO_VENDOR.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_CLIENT.CO_VENDOR_DETAIL_VALUES, aDS);
end;

function TEDIT_CO_VENDOR.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_VENDOR;
end;

function TEDIT_CO_VENDOR.GetInsertControl: TWinControl;
begin
  Result := edtVendor;
end;


procedure TEDIT_CO_VENDOR.SetCOVendorDetail;
begin
   lblVendor.Caption := DM_CLIENT.CO_VENDOR.FieldByName('VENDOR_NAME').AsString;

   if DM_CLIENT.CO_VENDOR_DETAIL_VALUES.Active then
   begin
    DM_CLIENT.CO_VENDOR_DETAIL_VALUES.UserFilter := 'CO_VENDOR_NBR = ' + IntToStr(DM_CLIENT.CO_VENDOR.CO_VENDOR_NBR.AsInteger);
    DM_CLIENT.CO_VENDOR_DETAIL_VALUES.UserFiltered := True;
   end;
end;


procedure TEDIT_CO_VENDOR.MiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;

  if DM_CLIENT.CO_VENDOR.CO_VENDOR_NBR.AsInteger > 0 then
  begin
    MiniNavigationFrame.InsertRecordExecute(Sender);

     dsVendorDetail.DataSet.FieldByName('CO_VENDOR_NBR').Value :=
           DM_CLIENT.CO_VENDOR.CO_VENDOR_NBR.AsVariant;
  end;

End;

procedure TEDIT_CO_VENDOR.MiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  MiniNavigationFrame.DeleteRecordExecute(Sender);
end;

procedure TEDIT_CO_VENDOR.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then
    Exit;

  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and DM_CLIENT.CO_VENDOR_DETAIL_VALUES.Active then
    SetCOVendorDetail;
end;

procedure TEDIT_CO_VENDOR.cbDetailChange(Sender: TObject);
begin
  inherited;
  if dsVendorDetail.DataSet.State in [dsEdit,dsInsert] then
     DM_CLIENT.CO_VENDOR_DETAIL_VALUES['SB_VENDOR_DETAIL_VALUES_NBR'] := null;
end;

procedure TEDIT_CO_VENDOR.cbPreDefineBeforeDropDown(Sender: TObject);
begin
  inherited;
  if DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.Active then
  begin
    DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.UserFilter := 'SB_VENDOR_DETAIL_NBR = ' + IntToStr(DM_CLIENT.CO_VENDOR_DETAIL_VALUES.SB_VENDOR_DETAIL_NBR.AsInteger);
    DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.UserFiltered := True;
  end;
end;

procedure TEDIT_CO_VENDOR.cbPreDefineCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.UserFilter := '';
  DM_SERVICE_BUREAU.SB_VENDOR_DETAIL_VALUES.UserFiltered := false;
end;

procedure TEDIT_CO_VENDOR.cbDetailBeforeDropDown(Sender: TObject);
begin
  inherited;
  if DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.Active then
  begin
    DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.UserFilter := 'DETAIL_LEVEL = ''C'' and SB_VENDOR_NBR = ' + IntToStr(DM_CLIENT.CO_VENDOR.SB_VENDOR_NBR.AsInteger);
    DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.UserFiltered := True;
  end;
end;

procedure TEDIT_CO_VENDOR.cbDetailCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
    DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.UserFilter := '';
    DM_SERVICE_BUREAU.SB_VENDOR_DETAIL.UserFiltered := false;
end;

procedure TEDIT_CO_VENDOR.PageControl1Change(Sender: TObject);
begin
  inherited;
  SetCOVendorDetail;
end;

procedure TEDIT_CO_VENDOR.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;

  AllowChange:= (wwdsDetail.DataSet.State = dsBrowse) and
                 (dsVendorDetail.State = dsBrowse);


end;

procedure TEDIT_CO_VENDOR.dsVendorDetailStateChange(Sender: TObject);
begin
  inherited;
  MiniNavigationFrame.Enabled := dsVendorDetail.State = dsBrowse;
end;

initialization
  RegisterClass(TEDIT_CO_VENDOR);

end.
