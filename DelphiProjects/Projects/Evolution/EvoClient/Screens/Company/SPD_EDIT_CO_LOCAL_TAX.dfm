inherited EDIT_CO_LOCAL_TAX: TEDIT_CO_LOCAL_TAX
  inherited PageControl1: TevPageControl
    HelpContext = 14501
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                inherited pnlsbEDIT_CO_BASE_Inner_Border: TevPanel
                  inherited splEDIT_CO_BASE: TevSplitter
                    Left = 245
                  end
                  inherited pnlEDIT_CO_BASE_LEFT: TevPanel
                    Width = 239
                    inherited fpEDIT_CO_BASE_Company: TisUIFashionPanel
                      Width = 227
                    end
                  end
                  inherited pnlEDIT_CO_BASE_RIGHT: TevPanel
                    Left = 245
                    Width = 545
                    Visible = True
                    object fpLocalsRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 533
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      OnResize = fpLocalsRightResize
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Locals'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 439
              Visible = True
              object Panel4: TevPanel
                Left = 0
                Top = 0
                Width = 477
                Height = 565
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 409
              Height = 63
              Visible = True
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 409
              Height = 63
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 359
                Height = 294
                IniAttributes.SectionName = 'TEDIT_CO_LOCAL_TAX\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 412
              Width = 0
              Height = 63
              Align = alClient
              Caption = ''
              Visible = True
              Title = 'Locals'
              object wwDBGrid3: TevDBGrid
                Left = 18
                Top = 48
                Width = 178
                Height = 294
                DisableThemesInTitle = False
                Selected.Strings = (
                  'LocalName'#9'40'#9'Local'#9'F'
                  'localState'#9'2'#9'State'#9'F'
                  'LocalTypeDesc'#9'30'#9'Local Type'#9'F'
                  'LOCAL_ACTIVE'#9'1'#9'Active'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_LOCAL_TAX\wwDBGrid3'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                PopupMenu = pmLocalTaxCopyToEE
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtDetail: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbLocalTax: TScrollBox
        Left = 0
        Top = 0
        Width = 435
        Height = 194
        Align = alClient
        TabOrder = 0
        object fpLocalTax: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 333
          Height = 259
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Local Tax'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablLocal: TevLabel
            Left = 12
            Top = 45
            Width = 35
            Height = 16
            Caption = '~Local'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablEIN_Number: TevLabel
            Left = 12
            Top = 123
            Width = 67
            Height = 16
            Caption = '~EIN Number'
            FocusControl = dedtEIN_Number
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablTax_Return_Code: TevLabel
            Left = 12
            Top = 162
            Width = 81
            Height = 13
            Caption = 'Tax Return Code'
            FocusControl = dedtTax_Return_Code
          end
          object lablPayment_Method: TevLabel
            Left = 165
            Top = 162
            Width = 89
            Height = 16
            Caption = '~Payment Method'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablEFT_Name: TevLabel
            Left = 165
            Top = 45
            Width = 141
            Height = 13
            Caption = 'Override Company EFT Name'
            FocusControl = dedtEFT_Name
          end
          object lablEFT_PIN_Number: TevLabel
            Left = 165
            Top = 84
            Width = 81
            Height = 13
            Caption = 'EFT PIN Number'
            FocusControl = dedtEFT_PIN_Number
          end
          object lablEFT_Status: TevLabel
            Left = 165
            Top = 123
            Width = 53
            Height = 13
            Caption = 'EFT Status'
          end
          object Label1: TevLabel
            Left = 12
            Top = 84
            Width = 41
            Height = 13
            Caption = 'G/L Tag'
            FocusControl = DBEdit1
          end
          object Label2: TevLabel
            Left = 12
            Top = 201
            Width = 72
            Height = 13
            Caption = 'Offset G/L Tag'
            FocusControl = DBEdit2
          end
          object evSpeedButton1: TevSpeedButton
            Left = 136
            Top = 60
            Width = 21
            Height = 21
            Caption = '...'
            HideHint = True
            AutoSize = False
            OnClick = evSpeedButton1Click
            ParentColor = False
            ShortCut = 0
          end
          object evLabel7: TevLabel
            Left = 33
            Top = 27
            Width = 255
            Height = 13
            Caption = 'Use Effective Dates to Change Tax Statuses'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object dedtEIN_Number: TevDBEdit
            Left = 12
            Top = 138
            Width = 145
            Height = 21
            HelpContext = 14504
            DataField = 'LOCAL_EIN_NUMBER'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtTax_Return_Code: TevDBEdit
            Left = 12
            Top = 177
            Width = 145
            Height = 21
            HelpContext = 14505
            DataField = 'TAX_RETURN_CODE'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtEFT_Name: TevDBEdit
            Left = 165
            Top = 60
            Width = 145
            Height = 21
            HelpContext = 14506
            DataField = 'EFT_NAME'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object dedtEFT_PIN_Number: TevDBEdit
            Left = 165
            Top = 99
            Width = 145
            Height = 21
            HelpContext = 14507
            DataField = 'EFT_PIN_NUMBER'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwcbPayment_Method: TevDBComboBox
            Left = 165
            Top = 177
            Width = 145
            Height = 21
            HelpContext = 14509
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'PAYMENT_METHOD'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
            OnChange = wwcbPayment_MethodChange
            OnDropDown = wwcbPayment_MethodDropDown
          end
          object wwcbEFT_Status: TevDBComboBox
            Left = 165
            Top = 138
            Width = 145
            Height = 21
            HelpContext = 14508
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = True
            AutoDropDown = True
            DataField = 'EFT_STATUS'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 5
            UnboundDataType = wwDefault
          end
          object wwlcLocal: TevDBLookupCombo
            Left = 12
            Top = 60
            Width = 122
            Height = 21
            HelpContext = 14502
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'LOCALITY_NAME'#9'F'
              'LocalState'#9'4'#9'LocalState'#9'F'
              'LocalTypeDescription'#9'40'#9'LocalTypeDescription'#9'F'
              'CountyName'#9'30'#9'CountyName'#9'F'
              'LOCAL_TYPE'#9'4'#9'LOCAL_TYPE'#9'F'
              'PSDCode'#9'20'#9'PSD Code'#9'F')
            DataField = 'SY_LOCALS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SY_LOCALS.SY_LOCALS
            LookupField = 'SY_LOCALS_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = wwlcLocalChange
            OnCloseUp = wwlcLocalCloseUp
          end
          object DBEdit1: TevDBEdit
            Left = 12
            Top = 99
            Width = 145
            Height = 21
            HelpContext = 14522
            DataField = 'GENERAL_LEDGER_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object DBEdit2: TevDBEdit
            Left = 12
            Top = 216
            Width = 145
            Height = 21
            HelpContext = 14522
            DataField = 'OFFSET_GL_TAG'
            DataSource = wwdsDetail
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object cbAppliedFor: TevDBCheckBox
            Left = 165
            Top = 218
            Width = 97
            Height = 17
            Caption = 'Applied For'
            DataField = 'APPLIED_FOR'
            DataSource = wwdsDetail
            TabOrder = 9
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
        end
        object fpRates: TisUIFashionPanel
          Left = 8
          Top = 275
          Width = 333
          Height = 171
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpRates'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Current Rates & Amounts'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablDeposit_Freq: TevLabel
            Left = 12
            Top = 74
            Width = 98
            Height = 16
            Caption = '~Deposit Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lablMiscellaneous_Amount: TevLabel
            Left = 165
            Top = 74
            Width = 106
            Height = 13
            Caption = 'Miscellaneous Amount'
          end
          object evLabel1: TevLabel
            Left = 165
            Top = 35
            Width = 90
            Height = 13
            Caption = 'Standard Tax Rate'
          end
          object lablTax_Rate: TevLabel
            Left = 12
            Top = 35
            Width = 87
            Height = 13
            Caption = 'Override Tax Rate'
          end
          object wwDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 145
            Height = 21
            HelpContext = 14519
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'20'#9'DESCRIPTION')
            DataField = 'SY_LOCAL_DEPOSIT_FREQ_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_SY_LOCAL_DEPOSIT_FREQ.SY_LOCAL_DEPOSIT_FREQ
            LookupField = 'SY_LOCAL_DEPOSIT_FREQ_NBR'
            Style = csDropDownList
            PopupMenu = evPopupMenu1
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object wwdeMiscellaneous_Amount: TevDBEdit
            Left = 165
            Top = 89
            Width = 145
            Height = 21
            HelpContext = 14520
            DataField = 'MISCELLANEOUS_AMOUNT'
            DataSource = wwdsDetail
            Picture.PictureMask = 
              '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
              '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
              '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBEdit1: TevDBEdit
            Left = 165
            Top = 50
            Width = 145
            Height = 21
            Color = clBtnFace
            DataField = 'systemRate'
            DataSource = wwdsDetail
            Enabled = False
            ReadOnly = True
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object wwdeTax_Rate: TevDBEdit
            Left = 12
            Top = 50
            Width = 145
            Height = 21
            HelpContext = 14517
            DataField = 'TAX_RATE'
            DataSource = wwdsDetail
            PopupMenu = pmLocalTaxCopyTaxRate
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object butnAttachLocal: TevBitBtn
            Left = 12
            Top = 124
            Width = 298
            Height = 25
            Caption = 'Attach this Local to Processed Payrolls'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            OnClick = butnAttachLocalClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFD5D5D5CCCCCCCCCCCCCCCCCCD9D9D9FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5CCCCCCCC
              CCCCCCCCCCD9D9D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCCCCC998F83978A7F978A7F978A7F9E968CCCCCCCFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC8E8E8E89898989
              8989898989959594CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFD5D5D5978A7FAEA59DC8C1BBC6BFB9C6BEB8ADA39A978A7FD5D5D5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D5D5898989A5A5A5C1C1C1BF
              BFBFBEBEBEA2A2A2898989D5D5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC0B8B2978A7FB6ACA4FFFFFFFFFFFFFFFFFFC4BCB5ADA39A9A8F84FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8B8B8898989ABABABFFFFFFFF
              FFFFFFFFFFBCBCBBA2A2A28E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC1B9B3978A7FFFFFFFCCCCCCCCCCCCCCCCCCFFFFFFC5C0B8978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B9B9898989FFFFFFCCCCCCCC
              CCCCCCCCCCFFFFFFBFBFBF898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BEB6978A7FCCCCCC978A7F978A7F978A7FCCCCCCC6C0B8978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD898989CCCCCC89898989
              8989898989CCCCCCBFBFBF898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC3BDB5978A7FA4998EC8C2BAC4BEB7BAB2AA978A7FC6C0B9978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989989898C2C2C1BE
              BDBDB2B1B1898989C0C0C0898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC3BDB5978A7FC0B8B0968A7DFFFFFFC2BBB3978A7FC7C1B9978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989B8B7B7888888FF
              FFFFBBBBBB898989C1C1C1898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BDB5978A7FC6BFB896887BFFFFFFC2BBB3978A7FC8C0B9978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989BFBFBF868686FF
              FFFFBBBBBB898989C0C0C0898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BDB5978A7FC6BFB896897CFFFFFFCFC9C4978A7FC5BEB7978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989BFBFBF878787FF
              FFFFCAC9C9898989BEBDBD898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFC4BDB5978A7FC7C0B9998D80FFFFFFFFFFFFFFFFFFC2BAB2978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDBCBC898989C0C0C08B8B8BFF
              FFFFFFFFFFFFFFFFBABABA898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFCFC9C4978A7FC8C0B99D8F82FFFFFFFFFFFFFDFDFDC2B8B1978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCAC9C9898989C0C0C08E8E8EFF
              FFFFFFFFFFFEFEFEB8B8B8898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFC7BFB99D9084E7E7E7FFFFFFE9E9E9BEB4AD978A7FFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF8F8F8FE8
              E7E7FFFFFFE9E9E9B4B4B4898989FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFC5BCB6A4968CB1AAA4CCCCCCBCB7B2A5978EB4ACA5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBCBCBB969595AA
              A9A9CCCCCCB8B7B7979696AAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFD2CBC6C1B8B2A3968C9E8F839D8E839F9085CCC4BEFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBCBCBB8B8B896
              95958E8E8E8D8D8D8F8F8FC4C3C3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFDBD6D2C4BBB4C4BCB5C5BDB6DAD5D1FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7D6D6BB
              BBBBBCBCBBBDBCBCD5D5D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object fpOptions: TisUIFashionPanel
          Left = 349
          Top = 8
          Width = 221
          Height = 438
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpOptions'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Options'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablCTSCode: TevLabel
            Left = 12
            Top = 341
            Width = 49
            Height = 13
            Caption = 'CTS Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 380
            Width = 74
            Height = 13
            Caption = 'OPT Client E/D'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object drgpFinal_Tax_Return: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 185
            Height = 37
            HelpContext = 14516
            Caption = '~Final Tax Return'
            Columns = 2
            DataField = 'FINAL_TAX_RETURN'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpExempt: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 185
            Height = 37
            HelpContext = 14515
            Caption = '~Exempt'
            Columns = 2
            DataField = 'EXEMPT'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
            AutoPopupEffectiveDateDialog = True
            AutoPopupForceQuartersOnly = True
          end
          object rgActive: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 185
            Height = 37
            HelpContext = 14515
            Caption = '~Active'
            Columns = 2
            DataField = 'LOCAL_ACTIVE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
          object rgSelfAdjustTax: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 185
            Height = 37
            HelpContext = 14513
            Caption = '~SelfAdjustTax'
            Columns = 2
            DataField = 'SELF_ADJUST_TAX'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object rgAutocreate: TevDBRadioGroup
            Left = 12
            Top = 191
            Width = 185
            Height = 71
            HelpContext = 14516
            Caption = '~Autocreate On New Hire'
            Columns = 2
            DataField = 'AUTOCREATE_ON_NEW_HIRE'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
          end
          object editCTSCode: TevEdit
            Tag = 1
            Left = 12
            Top = 356
            Width = 185
            Height = 21
            HelpContext = 14001
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 10
            ParentFont = False
            TabOrder = 6
            OnChange = editCTSCodeChange
          end
          object evDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 395
            Width = 185
            Height = 21
            HelpContext = 14519
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'CUSTOM_E_D_CODE_NUMBER'
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'No')
            DataField = 'CL_E_DS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_E_DS.CL_E_DS
            LookupField = 'CL_E_DS_NBR'
            Style = csDropDownList
            PopupMenu = popOPTClientED
            TabOrder = 7
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object rgDeduct: TevDBRadioGroup
            Left = 12
            Top = 267
            Width = 185
            Height = 71
            HelpContext = 14516
            Caption = '~Deduct '
            Columns = 2
            DataField = 'DEDUCT'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Always'
              'No Overrides'
              'Never')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'D'
              'N')
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_LOCAL_TAX.CO_LOCAL_TAX
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    OnUpdateData = wwdsDetailUpdateData
  end
  inherited PageControlImages: TevImageList
    Left = 304
    Top = 48
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 328
    Top = 13
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 450
    Top = 18
  end
  object SyLocalsSrc: TevDataSource
    DataSet = DM_SY_LOCALS.SY_LOCALS
    Left = 376
    Top = 57
  end
  object SyLocalsFreqSrc: TevDataSource
    DataSet = DM_SY_LOCAL_DEPOSIT_FREQ.SY_LOCAL_DEPOSIT_FREQ
    MasterDataSource = SyLocalsSrc
    Left = 391
    Top = 17
  end
  object evSecElement1: TevSecElement
    ElementType = otFunction
    ElementTag = 'USER_CAN_EDIT_TAX_EXEMPTIONS'
    Left = 352
    Top = 8
  end
  object evPopupMenu1: TevPopupMenu
    Left = 596
    Top = 41
    object CopyTDF: TMenuItem
      Caption = 'Copy To...'
      OnClick = CopyTDFClick
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 528
    Top = 16
  end
  object pmLocalTaxCopyToEE: TPopupMenu
    Left = 640
    Top = 27
    object mnLocalTaxToCopyEE: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnLocalTaxToCopyEEClick
    end
  end
  object pmLocalTaxCopyTaxRate: TPopupMenu
    Left = 80
    Top = 307
    object mnCopyTaxRate: TMenuItem
      Caption = 'Copy To...'
      OnClick = mnCopyTaxRateClick
    end
  end
  object popOPTClientED: TevPopupMenu
    Left = 596
    Top = 73
    object MenuItem1: TMenuItem
      Caption = 'Copy To...'
      OnClick = CopyTDFClick
    end
  end
end
