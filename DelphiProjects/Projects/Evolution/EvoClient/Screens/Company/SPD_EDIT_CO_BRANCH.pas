// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BRANCH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc, StdCtrls, DBCtrls, Mask, ComCtrls, Variants,
  wwdbedit, ExtCtrls, Wwdotdot, Wwdbcomb, Grids, Wwdbigrd, Wwdbgrid,
  wwdblook, SPD_EDIT_CO_BASE, Buttons, SDataStructure,  EvConsts,
  EvSecElement, SPD_EDIT_CO_DBDT_LOCALS, SPackageEntry, EvTypes,
  sCopyHelper, sDBDTCopyHelper, SDDClasses, ISBasicClasses,
  SDataDictclient, SDataDicttemp,sCompanyShared, EvContext, EvExceptions,
  ActnList, Menus, SPD_DBDTChoiceQuery, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, EvDataAccessComponents;

type
  TEDIT_CO_BRANCH = class(TEDIT_CO_BASE)
    tshtGeneral_Info: TTabSheet;
    tshtMisc_Info: TTabSheet;
    tshtDefaults: TTabSheet;
    Panel4: TevPanel;
    lablBRANCH_NAME: TevLabel;
    DBText10: TevDBText;
    Label1: TevLabel;
    DBText2: TevDBText;
    pdsDetail: TevProxyDataSet;
    tbshLocals: TTabSheet;
    TDBDTLocals1: TDBDTLocals;
    tsVmr: TTabSheet;
    pmLocalCopy: TevPopupMenu;
    MenuItem3: TMenuItem;
    LocalCopyService: TevActionList;
    CopyService: TAction;
    fpBranchRight: TisUIFashionPanel;
    sbCompanyBranch: TScrollBox;
    fpCompanyBranch: TisUIFashionPanel;
    fpBranchContact: TisUIFashionPanel;
    fpBranchOptions: TisUIFashionPanel;
    lablBranch_Code: TevLabel;
    dedtBranch_Code: TevDBEdit;
    lablName: TevLabel;
    lablAddress1: TevLabel;
    lablAddress2: TevLabel;
    lablCity: TevLabel;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    dedtName: TevDBEdit;
    dedtAddress1: TevDBEdit;
    dedtAddress2: TevDBEdit;
    dedtCity: TevDBEdit;
    wwdeState: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    Label2: TevLabel;
    evLabel4: TevLabel;
    DBEdit1: TevDBEdit;
    evDBEdit1: TevDBEdit;
    lablPrimary_Contact: TevLabel;
    lablPhone: TevLabel;
    lablDescription1: TevLabel;
    lablSecondary_Contact: TevLabel;
    lablPhone2: TevLabel;
    lablDescription2: TevLabel;
    dedtPrimary_Contact: TevDBEdit;
    wwdePhone1: TevDBEdit;
    dedtDescription1: TevDBEdit;
    dedtSecondary_Contact: TevDBEdit;
    wwdePhone2: TevDBEdit;
    dedtDescription2: TevDBEdit;
    EvBevel1: TEvBevel;
    sbBranchBanks: TScrollBox;
    fpDivisionBanks: TisUIFashionPanel;
    lablPayroll_Bank_Account: TevLabel;
    lablBilling_Bank_Account: TevLabel;
    lablTax_Bank_Account: TevLabel;
    lablDD_Bank_Account: TevLabel;
    wwlcPayroll_Bank_Account: TevDBLookupCombo;
    wwlcBilling_Bank_Account: TevDBLookupCombo;
    wwlcTax_Bank_Account: TevDBLookupCombo;
    wwlcDD_Bank_Account: TevDBLookupCombo;
    sbBranchDefaults: TScrollBox;
    fpDivisionDefaults: TisUIFashionPanel;
    lablHome_State: TevLabel;
    lablOverride_Pay_Rate: TevLabel;
    lablOverride_EE_Rate: TevLabel;
    Label6: TevLabel;
    evLabel3: TevLabel;
    wwlcHome_State: TevDBLookupCombo;
    dedtOverride_EE_Rate: TevDBEdit;
    wwdeOverride_Pay_Rate: TevDBEdit;
    wwlcWorkers_Comp_Number: TevDBLookupCombo;
    wwDBLookupCombo4: TevDBLookupCombo;
    wwDBLookupCombo1: TevDBLookupCombo;
    drgpHomeStateType: TevDBRadioGroup;
    evDBLookupCombo6: TevDBLookupCombo;
    fpDivisionNotes: TisUIFashionPanel;
    dmemBranch_Notes: TEvDBMemo;
    sbBranchLocals: TScrollBox;
    lablFax: TevLabel;
    wwdeFax: TevDBEdit;
    lablDescription: TevLabel;
    dedtDescription: TevDBEdit;
    lablE_Mail: TevLabel;
    dedtE_Mail: TevDBEdit;
    EvBevel2: TEvBevel;
    qlblShowAddressOnCheck: TevLabel;
    wwcbPrint_Branch_Address_Check: TevDBComboBox;
    evLabel6: TevLabel;
    evDBLookupCombo8: TevDBLookupCombo;
    DBText3: TevDBText;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evDBText1: TevDBText;
    rgPrimary: TevDBRadioGroup;
    edtAccountNumber: TevDBEdit;
    evLabel5: TevLabel;
    sbDivisionMailRoom: TScrollBox;
    fpMailRoom: TisUIFashionPanel;
    evLabel20: TevLabel;
    evLabel21: TevLabel;
    evLabel22: TevLabel;
    evLabel25: TevLabel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBLookupCombo4: TevDBLookupCombo;
    evDBLookupCombo7: TevDBLookupCombo;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo5: TevDBLookupCombo;
    wwDBGrid2: TevDBGrid;
    Splitter2: TevSplitter;
    wwDBGrid3: TevDBGrid;
    CopyHelper: TDBDTCopyHelper;
    procedure pdsDetailNewRecord(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure ResultFIELDS_CL_BANK_Function;
    procedure CopyServiceExecute(Sender: TObject);
    procedure LocalTaxCopy;
    procedure CopyHelperCopyaddressto1Click(Sender: TObject);
    procedure edtAccountNumberChange(Sender: TObject);
    procedure fpBranchRightResize(Sender: TObject);
  private
    procedure HandleCopy( Sender: TObject; parent_nbr: integer; selectedDetails, details: TEvClientDataSet; params: TStringList );
    procedure HandleDelete( Sender: TObject; parent_nbr: integer; selectedDetails, details: TEvClientDataSet; params: TStringList );
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    function GetDataSetConditions(sName: string): string; override;
  public
    GL_Setuped:boolean;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick( Kind: Integer ); override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    constructor Create(AOwner: TComponent); override;  
  end;

implementation

uses
  SCopier, EvUtils, SFrameEntry;
{$R *.DFM}

procedure TEDIT_CO_BRANCH.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin

  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP, aDS);
  AddDS(DM_CLIENT.CL_TIMECLOCK_IMPORTS, aDS);
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_CLIENT.CL_BILLING, aDS);
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_LOCAL_TAX, aDS);
  AddDS(DM_COMPANY.CO_BRANCH_LOCALS, aDS);
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);

  inherited;

end;

procedure TEDIT_CO_BRANCH.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
  if ( wwdsList.DataSet.Active )and (wwdsList.DataSet.RecordCount <= 0) then exit;
  if Kind in [NavOk] then
  begin
    wwDBGrid2.Enabled := not Value;
    wwDBGrid3.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavNext.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavPrevious.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavLast.Enabled := not Value;
    (Owner as TFramePackageTmpl).btnNavFirst.Enabled := not Value;
  end;
end;


function TEDIT_CO_BRANCH.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BRANCH;
end;

function TEDIT_CO_BRANCH.GetInsertControl: TWinControl;
begin
  Result := dedtBranch_Code;
end;

procedure TEDIT_CO_BRANCH.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_DIVISION, SupportDataSets);
  AddDS(DM_COMPANY.CO_BRANCH_LOCALS, EditDataSets);

end;

function TEDIT_CO_BRANCH.GetDataSetConditions(sName: string): string;
begin
  if ( sName = 'CO_BRANCH' ) or ( sName = 'CO_BRANCH_LOCALS' ) then //gdy
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_BRANCH.Activate;
var
  dummy: boolean;
begin
  inherited;
  wwdsDetail.DataSet := DM_COMPANY.CO_BRANCH;
  GL_Setuped := false;
  CopyHelper.Grid := wwDBGrid3;
  CopyHelper.OnCopyDetail := HandleCopy;
  CopyHelper.OnDeleteDetail := HandleDelete;
  CopyHelper.CopyAddressEnabled := True;
  CopyHelper.Copyaddressto1.OnClick := CopyHelperCopyaddressto1Click;
  GetDataSetsToReopen( CopyHelper.FDSToSave,  dummy );

  PageControl1.ActivePageIndex := 0;
  tsVMR.TabVisible := tsVMR.TabVisible and CheckVmrLicenseActive;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CO_BRANCH.pdsDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  with DM_COMPANY.CO do
    if Active then
    begin
      DataSet.FindField('HOME_CO_STATES_NBR').AsInteger :=
        FindField('HOME_CO_STATES_NBR').AsInteger;
      DataSet.FindField('PAY_FREQUENCY_HOURLY').AsString :=
        FindField('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
      DataSet.FindField('PAY_FREQUENCY_SALARY').AsString :=
        FindField('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
    end;
end;

procedure TEDIT_CO_BRANCH.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
 V: Variant;
 tmpDataSet: TevClientDataSet;
begin
  case Kind of
    NavOk:
    begin
      tmpDataSet := TevClientDataSet.Create(nil);
      try
        with TExecDSWrapper.Create('SelectDBDTWithName') do
        begin
          SetParam('CoNbr', DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
          ctx_DataAccess.GetCustomData(tmpDataSet, AsVariant);
          ctx_DataAccess.OpenDataSets([tmpDataSet]);
          V := tmpDataSet.Lookup('BNAME',dedtName.Text,'CUSTOM_BRANCH_NUMBER');
        end;
        if (not VarIsNull(V)) and (V <> DM_COMPANY.CO_BRANCH['CUSTOM_BRANCH_NUMBER']) then
           EvMessage('Branch name has been used by other branch, it could cause inaccurate reporting.', mtWarning, [mbOK]);
      finally
        tmpDataSet.free;
      end;

      if DM_COMPANY.CO_BRANCH.FieldByName('HOME_CO_STATES_NBR').AsInteger = 0 then
        raise EUpdateError.CreateHelp('You must select a home state!', IDH_ConsistencyViolation);
      if (DM_COMPANY.CO_BRANCH.FieldByName('HOME_STATE_TYPE').AsString = HOME_STATE_TYPE_OVERRIDE) and
         ((not DM_COMPANY.CO_BRANCH.FieldByName('OVERRIDE_PAY_RATE').IsNull) or (wwdeOverride_Pay_Rate.Text <> '')) then
        EvMessage('Auto Labor Distribution will NOT use this override!!!', mtWarning, [mbOK]);
      if GL_Setuped and not CheckGlSetuped(DM_CLIENT.CO_BRANCH,['GENERAL_LEDGER_TAG'])then
           EvMessage(sDontForgetSetupGL);
      GL_Setuped := false;

    end;
    NavInsert:
    begin
      lablName.Caption := '~' + lablName.Caption;
      DM_CLIENT.CO_BRANCH.FieldByName('NAME').Required := true;
      GL_Setuped := CheckGlSetupExist(DM_CLIENT.CO_BRANCH,['GENERAL_LEDGER_TAG'],false);
    end
  end;

  if (Kind in [NavOK,NavCommit]) and
      (trim(edtAccountNumber.Text) <> '') then
       if (Trim(rgPrimary.Value) = '') then
         raise EUpdateError.CreateHelp('Please select Primary!', IDH_ConsistencyViolation);

  inherited;
end;

procedure TEDIT_CO_BRANCH.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavOk, NavCancel:
    begin
      lablName.Caption := StringReplace( lablName.Caption, '~','', [rfReplaceAll] );
      DM_CLIENT.CO_BRANCH.FieldByName('NAME').Required := false;
    end;

  end;
end;


procedure TEDIT_CO_BRANCH.HandleCopy(Sender: TObject; parent_nbr: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList);
var
  k: integer;
  fn: string;
begin
  if details.Locate('CUSTOM_BRANCH_NUMBER', VarArrayOf([selecteddetails.FieldByNAme('CUSTOM_BRANCH_NUMBER').AsString]), []) then
    details.Edit
  else
    details.Insert;
  for k := 0 to SelectedDetails.Fields.Count - 1 do
  begin
    fn := Selecteddetails.Fields[k].FieldName;
    if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) then
      details[fn] := selectedDetails[fn];
  end;
  details['CO_DIVISION_NBR'] := parent_nbr;
  details.Post;
end;

procedure TEDIT_CO_BRANCH.HandleDelete(Sender: TObject; parent_nbr: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList);
begin
  if details.Locate('CUSTOM_BRANCH_NUMBER', VarArrayOf([selecteddetails.FieldByNAme('CUSTOM_BRANCH_NUMBER').AsString]), []) then
    details.Delete;
end;

procedure TEDIT_CO_BRANCH.AfterDataSetsReopen;
begin
  inherited;
  tsVMR.TabVisible := CheckVmrLicenseActive and CheckVmrSetupActive;
  AssignVMRClientLookups(tsVMR);
end;

procedure TEDIT_CO_BRANCH.PageControl1Change(Sender: TObject);
begin
  inherited;
  ResultFIELDS_CL_BANK_Function;
end;
procedure TEDIT_CO_BRANCH.ResultFIELDS_CL_BANK_Function;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_CL_BANK_') <> stEnabled then
   Begin
    wwlcPayroll_Bank_Account.Enabled := false;
    wwlcBilling_Bank_Account.Enabled := false;
    wwlcTax_Bank_Account.Enabled := false;
    wwlcDD_Bank_Account.Enabled := false;
   end;
End;

procedure TEDIT_CO_BRANCH.CopyServiceExecute(Sender: TObject);
begin
  inherited;
   LocalTaxCopy;
end;

procedure TEDIT_CO_BRANCH.LocalTaxCopy;
var
  CurrentLocalNbr, CurrentNbr, CurrentLocalTaxNbr: integer;

  procedure DoCopyLocals;
  var
    k: integer;
  begin
    with TDBDTChoiceQuery.Create( Self ) do
    try
      SetFilter(CLIENT_LEVEL_BRANCH, 'BRANCH_NBR <>  ' + DM_COMPANY.CO_BRANCH_LOCALS.FieldByName('CO_BRANCH_NBR').AsString );
      Caption := 'Select the Branches that you want this Local Tax copied to';
      ConfirmAllMessage := 'This operation will copy the Local Tax to all Branches. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
            DM_COMPANY.CO_BRANCH_LOCALS.DataRequired('ALL');
            if dgChoiceList.SelectedList.Count > 0 then
            begin
              DM_COMPANY.CO_BRANCH_LOCALS.DisableControls;
              try
                for k := 0 to dgChoiceList.SelectedList.Count - 1 do
                begin
                  cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                  if not DM_COMPANY.CO_BRANCH_LOCALS.Locate('CO_BRANCH_NBR;CO_LOCAL_TAX_NBR',
                       VarArrayOf([cdsChoiceList.FieldByName('BRANCH_NBR').AsInteger,CurrentLocalTaxNbr]), []) then
                  begin
                    DM_COMPANY.CO_BRANCH_LOCALS.Insert;
                    DM_COMPANY.CO_BRANCH_LOCALS['CO_BRANCH_NBR'] := cdsChoiceList.FieldByName('BRANCH_NBR').AsInteger;
                    DM_COMPANY.CO_BRANCH_LOCALS['CO_LOCAL_TAX_NBR'] := CurrentLocalTaxNbr;
                    DM_COMPANY.CO_BRANCH_LOCALS.Post;
                  end;
                end;
              finally
                ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BRANCH_LOCALS]);
                DM_COMPANY.CO_BRANCH_LOCALS.EnableControls;
              end;
             end;
      end;
    finally
      Free;
    end;
  end;
begin
  CurrentNbr := DM_COMPANY.CO_BRANCH_LOCALS.FieldByName('CO_BRANCH_NBR').AsInteger;
  CurrentLocalNbr := DM_COMPANY.CO_BRANCH_LOCALS.FieldByName('CO_BRANCH_LOCALS_NBR').AsInteger;
  CurrentLocalTaxNbr := DM_COMPANY.CO_BRANCH_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
  if (CurrentLocalNbr > 0) and  (CurrentLocalTaxNbr > 0 )then
  begin
    try
      DoCopyLocals;
    finally
      DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR',CurrentNbr,[]);
      DM_COMPANY.CO_BRANCH_LOCALS.Locate('CO_BRANCH_LOCALS_NBR',CurrentLocalNbr,[]);
    end;
  end;
end;

procedure TEDIT_CO_BRANCH.CopyHelperCopyaddressto1Click(Sender: TObject);
var
  CurrentNbr, CurrentParentNbr : integer;
  CurrentAddr1,Currentaddr2,CurrentCity,CurrentState,CurrentZip: string;
  procedure DoCopyAddress;
  var
    k: integer;
  begin
    with TDBDTChoiceQuery.Create( Self ) do
    try
      SetFilter(CLIENT_LEVEL_Branch, 'Branch_NBR <>  ' + DM_COMPANY.CO_Branch.FieldByName('CO_Branch_NBR').AsString, False);
      Caption := 'Select the Branches you want the Address copied to';
      ConfirmAllMessage := 'This operation will copy the Address to all Branches. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        for k := 0 to dgChoiceList.SelectedList.Count - 1 do
        begin
          cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
          DM_COMPANY.CO_BRANCH.DataRequired('CO_BRANCH_NBR = ' + cdsChoiceList.FieldByName('Branch_NBR').AsString);
          if DM_COMPANY.CO_BRANCH.RecordCount > 0 then
          begin
            DM_COMPANY.CO_BRANCH.Edit;
            DM_COMPANY.CO_BRANCH['ADDRESS1'] := CurrentAddr1;
            DM_COMPANY.CO_BRANCH['ADDRESS2'] := Currentaddr2;
            DM_COMPANY.CO_BRANCH['CITY']     := CurrentCity;
            DM_COMPANY.CO_BRANCH['STATE']    := CurrentState;
            DM_COMPANY.CO_BRANCH['ZIP_CODE'] := CurrentZip;
            DM_COMPANY.CO_BRANCH.Post;
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BRANCH]);
          end;
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  CurrentNbr   := DM_COMPANY.CO_Branch.FieldByName('CO_Branch_NBR').AsInteger;
  CurrentParentNbr := DM_COMPANY.CO_Branch.FieldByName('CO_Division_NBR').AsInteger;
  CurrentAddr1 := DM_COMPANY.CO_Branch.FieldByName('ADDRESS1').AsString;
  Currentaddr2 := DM_COMPANY.CO_Branch.FieldByName('ADDRESS2').AsString;
  CurrentCity  := DM_COMPANY.CO_Branch.FieldByName('CITY').AsString;
  CurrentState := DM_COMPANY.CO_Branch.FieldByName('STATE').AsString;
  CurrentZip   := DM_COMPANY.CO_Branch.FieldByName('ZIP_CODE').AsString;
  try
    DM_COMPANY.CO_Branch.DisableControls;
    wwDBGrid3.DataSource.DataSet.DisableControls;
    DoCopyAddress;
  finally
    DM_COMPANY.CO_BRANCH.DataRequired('ALL');
    DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR',CurrentParentNbr,[]);
    DM_COMPANY.CO_Branch.Locate('CO_Branch_NBR',CurrentNbr,[]);
    DM_COMPANY.CO_Branch.enableControls;
    wwDBGrid3.DataSource.DataSet.enableControls;
  end;
end;

procedure TEDIT_CO_BRANCH.edtAccountNumberChange(Sender: TObject);
begin
  inherited;

  if Trim(edtAccountNumber.Text) <> '' then
      rgPrimary.Caption := '~Primary  '
  else
      rgPrimary.Caption := 'Primary ';

end;
constructor TEDIT_CO_BRANCH.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENTS
  pnlSubbrowse.Parent := fpBranchRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_BRANCH.fpBranchRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpBranchRight.Width - 40;
  pnlSubbrowse.Height := fpBranchRight.Height - 65;
end;

initialization
  RegisterClass(TEDIT_CO_BRANCH);

end.
