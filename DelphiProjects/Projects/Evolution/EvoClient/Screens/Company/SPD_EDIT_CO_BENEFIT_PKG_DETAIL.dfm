inherited EDIT_CO_BENEFIT_PKG_DETAIL: TEDIT_CO_BENEFIT_PKG_DETAIL
  Width = 1033
  Height = 667
  inherited Panel1: TevPanel
    Width = 1033
    inherited pnlFavoriteReport: TevPanel
      Left = 881
    end
    inherited pnlTopLeft: TevPanel
      Width = 881
    end
  end
  inherited PageControl1: TevPageControl
    Width = 1033
    Height = 613
    HelpContext = 29501
    ActivePage = tshtPensions
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 1025
        Height = 584
        inherited pnlBorder: TevPanel
          Width = 1021
          Height = 580
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 1021
          Height = 580
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              inherited sbEDIT_CO_BASE_Inner: TScrollBox
                TabOrder = 1
              end
              object sbCOBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 800
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCOBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 796
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCOBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCOLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Company'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCOLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCOBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 481
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCORight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 469
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Pensions'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 371
              Top = 59
              Width = 379
              Height = 520
              Visible = True
            end
          end
          inherited Panel3: TevPanel
            Width = 973
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 973
            Height = 473
            inherited Splitter1: TevSplitter
              Left = 320
              Height = 469
              Visible = True
            end
            object evSplitter2: TevSplitter [1]
              Left = 513
              Top = 0
              Height = 469
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 0
              Width = 320
              Height = 469
              Align = alLeft
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 389
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'13'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F'
                  'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'#9'F'
                  'FEIN'#9'9'#9'EIN'#9
                  'DBA'#9'40'#9'DBA '#9'F'
                  'UserFirstName'#9'20'#9'CSR First Name'#9'F'
                  'UserLastName'#9'30'#9'CSR Last Name'#9'F'
                  'HomeState'#9'2'#9'Home State'#9'F')
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_PKG_DETAIL\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 190
              Height = 469
              Align = alLeft
              Visible = True
              Title = 'Package Name'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 140
                Height = 389
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'17'#9'Name'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_PKG_DETAIL\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                IniAttributes.CheckNewFields = False
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
            object fpPackageDetails: TisUIFashionPanel
              Left = 516
              Top = 0
              Width = 453
              Height = 469
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Caption = 'fpPackageDetails'
              Color = 14737632
              Constraints.MinWidth = 159
              TabOrder = 2
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Package Benefits'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              Margins.Top = 12
              Margins.Left = 6
              Margins.Right = 12
              Margins.Bottom = 12
              object wwdgEDGROUPCODES: TevDBGrid
                Left = 18
                Top = 48
                Width = 403
                Height = 389
                DisableThemesInTitle = False
                Selected.Strings = (
                  'BenefitName'#9'40'#9'Benefit Name'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_PKG_DETAIL\wwdgEDGROUPCODES'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtPensions: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbPensions: TScrollBox
        Left = 0
        Top = 0
        Width = 1025
        Height = 584
        Align = alClient
        TabOrder = 0
        object fpCOBenefitPkgDetails: TisUIFashionPanel
          Left = 8
          Top = 468
          Width = 312
          Height = 94
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Benefit Package Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 47
            Height = 16
            Caption = '~Benefits'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evBenefit: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 278
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'BENEFIT_NAME'#9'40'#9'Benefit name'#9'F'
              'CategoryName'#9'40'#9'Category'#9'F')
            DataField = 'CO_BENEFITS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_BENEFITS.CO_BENEFITS
            LookupField = 'CO_BENEFITS_NBR'
            Options = [loTitles]
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
        object fpCOPkgDetailSummary: TisUIFashionPanel
          Left = 8
          Top = 110
          Width = 312
          Height = 350
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlSummary'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgPension: TevDBGrid
            Left = 12
            Top = 36
            Width = 280
            Height = 294
            TabStop = False
            DisableThemesInTitle = False
            Selected.Strings = (
              'BenefitName'#9'20'#9'Benefit name'#9'F'
              'CategoryName'#9'19'#9'Category'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_BENEFIT_PKG_DETAIL\wwdgPension'
            IniAttributes.Delimiter = ';;'
            IniAttributes.CheckNewFields = False
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpPackage: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 312
          Height = 94
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpPackage'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Package'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lblDescription: TevLabel
            Left = 12
            Top = 35
            Width = 52
            Height = 16
            Caption = '~Package'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evPackage: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 278
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F')
            LookupTable = DM_CO_BENEFIT_PACKAGE.CO_BENEFIT_PACKAGE
            LookupField = 'CO_BENEFIT_PACKAGE_NBR'
            Style = csDropDownList
            Navigator = True
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_BENEFIT_PKG_DETAIL.CO_BENEFIT_PKG_DETAIL
    MasterDataSource = wwdsSubMaster
  end
  inherited PageControlImages: TevImageList
    Top = 88
  end
  inherited dsCL: TevDataSource
    Left = 264
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_CO_BENEFIT_PACKAGE.CO_BENEFIT_PACKAGE
    MasterDataSource = wwdsMaster
    Left = 186
    Top = 51
  end
end
