inherited EDIT_CO_BANK_ACCOUNT_REGISTER: TEDIT_CO_BANK_ACCOUNT_REGISTER
  inherited PageControl1: TevPageControl
    HelpContext = 44001
    ActivePage = TabSheet2
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited pnlSubbrowse: TevPanel
              Width = 248
              Height = 404
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 404
                IniAttributes.SectionName = 'TEDIT_CO_BANK_ACCOUNT_REGISTER\wwdbgridSelectClient'
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object Label1: TevLabel
        Left = 16
        Top = 128
        Width = 69
        Height = 13
        Caption = 'PR Check Nbr'
      end
      object Label2: TevLabel
        Left = 16
        Top = 168
        Width = 80
        Height = 13
        Caption = 'Check Serial Nbr'
      end
      object Label3: TevLabel
        Left = 16
        Top = 208
        Width = 57
        Height = 13
        Caption = 'Check Date'
      end
      object Label4: TevLabel
        Left = 16
        Top = 248
        Width = 30
        Height = 13
        Caption = 'Status'
      end
      object Label6: TevLabel
        Left = 16
        Top = 288
        Width = 56
        Height = 13
        Caption = 'Status Date'
      end
      object Label7: TevLabel
        Left = 16
        Top = 328
        Width = 127
        Height = 13
        Caption = 'Transaction Effective Date'
      end
      object Label8: TevLabel
        Left = 176
        Top = 128
        Width = 36
        Height = 13
        Caption = 'Amount'
      end
      object Label9: TevLabel
        Left = 176
        Top = 208
        Width = 75
        Height = 13
        Caption = 'Cleared Amount'
      end
      object Label10: TevLabel
        Left = 176
        Top = 248
        Width = 24
        Height = 13
        Caption = 'Type'
      end
      object Label11: TevLabel
        Left = 232
        Top = 248
        Width = 62
        Height = 13
        Caption = 'Manual Type'
      end
      object Label12: TevLabel
        Left = 336
        Top = 168
        Width = 96
        Height = 13
        Caption = 'Co Manual ACH Nbr'
      end
      object Label13: TevLabel
        Left = 336
        Top = 208
        Width = 76
        Height = 13
        Caption = 'Co PR ACH Nbr'
      end
      object Label14: TevLabel
        Left = 336
        Top = 248
        Width = 123
        Height = 13
        Caption = 'Co Tax Payment ACH Nbr'
      end
      object Label15: TevLabel
        Left = 336
        Top = 128
        Width = 64
        Height = 13
        Caption = 'Process Date'
      end
      object Label16: TevLabel
        Left = 176
        Top = 296
        Width = 28
        Height = 13
        Caption = 'Notes'
      end
      object Label17: TevLabel
        Left = 176
        Top = 168
        Width = 110
        Height = 13
        Caption = 'SB Bank Accounts Nbr'
      end
      object lablCloseDate: TevLabel
        Left = 480
        Top = 128
        Width = 52
        Height = 13
        Caption = 'Close Date'
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 625
        Height = 120
        DisableThemesInTitle = False
        Selected.Strings = (
          'CO_BANK_ACCOUNT_REGISTER_NBR'#9'10'#9'Co Bank Account Register Nbr'#9'F'
          'CO_NBR'#9'10'#9'Co Nbr'#9'F'
          'SB_BANK_ACCOUNTS_NBR'#9'10'#9'Sb Bank Accounts Nbr'#9'F'
          'PR_CHECK_NBR'#9'10'#9'Pr Check Nbr'#9'F'
          'CHECK_SERIAL_NUMBER'#9'10'#9'Check Serial Number'#9'F'
          'CHECK_DATE'#9'18'#9'Check Date'#9'F'
          'STATUS'#9'1'#9'Status'#9'F'
          'STATUS_DATE'#9'18'#9'Status Date'#9'F'
          'EFFECTIVE_DATE'#9'18'#9'Effective Date'#9'F'
          'AMOUNT'#9'10'#9'Amount'#9'F'
          'CLEARED_AMOUNT'#9'10'#9'Cleared Amount'#9'F'
          'REGISTER_TYPE'#9'1'#9'Type'#9'F'
          'MANUAL_TYPE'#9'1'#9'Manual Type'#9'F'
          'CO_MANUAL_ACH_NBR'#9'10'#9'Co Manual Ach Nbr'#9'F'
          'CO_PR_ACH_NBR'#9'10'#9'Co Pr Ach Nbr'#9'F'
          'CO_TAX_PAYMENT_ACH_NBR'#9'10'#9'Co Tax Payment Ach Nbr'#9'F'
          'PROCESS_DATE'#9'18'#9'Process Date'#9'F'
          'PR_MISCELLANEOUS_CHECKS_NBR'#9'10'#9'Pr Miscellaneous Checks Nbr'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_CO_BANK_ACCOUNT_REGISTER\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwDBEdit1: TevDBEdit
        Left = 16
        Top = 144
        Width = 81
        Height = 21
        DataField = 'PR_CHECK_NBR'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit2: TevDBEdit
        Left = 16
        Top = 184
        Width = 81
        Height = 21
        DataField = 'CHECK_SERIAL_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBDateTimePicker1: TevDBDateTimePicker
        Left = 16
        Top = 224
        Width = 97
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'CHECK_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 3
      end
      object wwDBEdit3: TevDBEdit
        Left = 16
        Top = 264
        Width = 81
        Height = 21
        DataField = 'STATUS'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBDateTimePicker2: TevDBDateTimePicker
        Left = 16
        Top = 304
        Width = 97
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'STATUS_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 5
      end
      object wwDBDateTimePicker3: TevDBDateTimePicker
        Left = 16
        Top = 344
        Width = 97
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'TRANSACTION_EFFECTIVE_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 6
      end
      object wwDBEdit4: TevDBEdit
        Left = 176
        Top = 144
        Width = 81
        Height = 21
        DataField = 'AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit5: TevDBEdit
        Left = 176
        Top = 184
        Width = 81
        Height = 21
        DataField = 'SB_BANK_ACCOUNTS_NBR'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit6: TevDBEdit
        Left = 176
        Top = 224
        Width = 81
        Height = 21
        DataField = 'CLEARED_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit7: TevDBEdit
        Left = 176
        Top = 264
        Width = 33
        Height = 21
        DataField = 'REGISTER_TYPE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 10
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit8: TevDBEdit
        Left = 232
        Top = 264
        Width = 33
        Height = 21
        DataField = 'MANUAL_TYPE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 11
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBDateTimePicker4: TevDBDateTimePicker
        Left = 336
        Top = 144
        Width = 97
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'PROCESS_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 12
      end
      object wwDBEdit9: TevDBEdit
        Left = 336
        Top = 184
        Width = 81
        Height = 21
        DataField = 'CO_MANUAL_ACH_NBR'
        DataSource = wwdsDetail
        TabOrder = 13
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit10: TevDBEdit
        Left = 336
        Top = 224
        Width = 81
        Height = 21
        DataField = 'CO_PR_ACH_NBR'
        DataSource = wwdsDetail
        TabOrder = 14
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit11: TevDBEdit
        Left = 336
        Top = 264
        Width = 81
        Height = 21
        DataField = 'CO_TAX_PAYMENT_ACH_NBR'
        DataSource = wwdsDetail
        TabOrder = 15
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBRichEdit1: TevDBRichEdit
        Left = 176
        Top = 312
        Width = 449
        Height = 81
        AutoURLDetect = False
        DataField = 'NOTES'
        DataSource = wwdsDetail
        PrintJobName = 'Delphi 4'
        TabOrder = 16
        EditorCaption = 'Edit Rich Text'
        EditorPosition.Left = 0
        EditorPosition.Top = 0
        EditorPosition.Width = 0
        EditorPosition.Height = 0
        MeasurementUnits = muInches
        PrintMargins.Top = 1.000000000000000000
        PrintMargins.Bottom = 1.000000000000000000
        PrintMargins.Left = 1.000000000000000000
        PrintMargins.Right = 1.000000000000000000
        PrintHeader.VertMargin = 0.500000000000000000
        PrintHeader.Font.Charset = DEFAULT_CHARSET
        PrintHeader.Font.Color = clWindowText
        PrintHeader.Font.Height = -11
        PrintHeader.Font.Name = 'MS Sans Serif'
        PrintHeader.Font.Style = []
        PrintFooter.VertMargin = 0.500000000000000000
        PrintFooter.Font.Charset = DEFAULT_CHARSET
        PrintFooter.Font.Color = clWindowText
        PrintFooter.Font.Height = -11
        PrintFooter.Font.Name = 'MS Sans Serif'
        PrintFooter.Font.Style = []
        RichEditVersion = 2
        Data = {
          810000007B5C727466315C616E73695C616E7369637067313235315C64656666
          305C6465666C616E67313034397B5C666F6E7474626C7B5C66305C666E696C20
          4D532053616E732053657269663B7D7D0D0A5C766965776B696E64345C756331
          5C706172645C66305C6673313620777744425269636845646974315C7061720D
          0A7D0D0A00}
      end
      object wwDBDateTimePicker5: TevDBDateTimePicker
        Left = 480
        Top = 144
        Width = 97
        Height = 21
        HelpContext = 44019
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'CLOSE_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 17
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_BANK_ACCOUNT_REGISTER.CO_BANK_ACCOUNT_REGISTER
  end
end
