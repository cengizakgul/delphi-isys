// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_STATE_TAX_LIABILITIES;

interface

uses
  SDataStructure,  SPD_EDIT_CO_TAX_LIABILITIES_BASE, StdCtrls,
  DBCtrls, Db,  Wwdatsrc, wwdbdatetimepicker, wwdbedit,
  wwdblook, ExtCtrls, Buttons, Mask, Wwdotdot, Wwdbcomb, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, Controls, Classes, SysUtils, Dialogs, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SDDClasses, SDataDictclient, SDataDicttemp, ISBasicClasses  , variants, EvUIUtils, EvUIComponents, EvClientDataSet,
  ImgList, isUIwwDBLookupCombo, isUIEdit, isUIwwDBEdit,
  isUIwwDBDateTimePicker, isUIDBMemo, isUIwwDBComboBox, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_STATE_TAX_LIABILITIES = class(TEDIT_CO_TAX_LIABILITIES_BASE)
    lablDue_Date: TevLabel;
    lablAdjustment_Type: TevLabel;
    lablStatus: TevLabel;
    lablTax_Type: TevLabel;
    lablAmount: TevLabel;
    lablState: TevLabel;
    wwcbAdjustment_Type: TevDBComboBox;
    wwcbStatus: TevDBComboBox;
    wwcbType: TevDBComboBox;
    wwdeAmount: TevDBEdit;
    wwlcState: TevDBLookupCombo;
    wwdpDue_Date: TevDBDateTimePicker;
    lcbType: TevDBComboBox;
    fpStatesRight: TisUIFashionPanel;
    procedure wwdbtpCheckDateExit(Sender: TObject);
    procedure fpStatesRightResize(Sender: TObject);
    procedure Panel3Resize(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
    procedure ReCalcTotals; override;
    procedure Activate; override;
    constructor Create(AOwner: TComponent); override;
    { Public declarations }
  end;

implementation

{$R *.DFM}

uses EvUtils, SFrameEntry;

procedure TEDIT_CO_STATE_TAX_LIABILITIES.Activate;
begin
  inherited;
  wwDBGrid2x.ControlType.Clear;
  wwDBGrid2x.ControlType.Add('ADJUSTMENT_TYPE;CustomEdit;lcbAdjType');
  wwDBGrid2x.ControlType.Add('TAX_TYPE;CustomEdit;lcbType;F');
  wwdgTaxLiabilities.ControlType.Clear;
  wwdgTaxLiabilities.ControlType.Add('ADJUSTMENT_TYPE;CustomEdit;lcbAdjType');
  wwdgTaxLiabilities.ControlType.Add('TAX_TYPE;CustomEdit;lcbType;F');

  wwcbStatus.OnDropDown := wwcbStatusDropDown;
  wwcbStatus.OnCloseUp := wwcbStatusCloseUp;
end;

constructor TEDIT_CO_STATE_TAX_LIABILITIES.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENT
  pnlSubbrowse.Parent := fpStatesRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_STATE_TAX_LIABILITIES.fpStatesRightResize(
  Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpStatesRight.Width - 40;
  pnlSubbrowse.Height := fpStatesRight.Height - 65;
end;

procedure TEDIT_CO_STATE_TAX_LIABILITIES.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_STATES, aDS);
  inherited;
end;

function TEDIT_CO_STATE_TAX_LIABILITIES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_STATE_TAX_LIABILITIES;
end;

function TEDIT_CO_STATE_TAX_LIABILITIES.GetInsertControl: TWinControl;
begin
  Result := wwlcState;
end;

procedure TEDIT_CO_STATE_TAX_LIABILITIES.Panel3Resize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 MOVING STUFF AROUND
  cbShowCredit.Left := Panel3.Width - 140;
  cbGroup.Left      := cbShowCredit.Left - 115;
end;

procedure TEDIT_CO_STATE_TAX_LIABILITIES.ReCalcTotals;
var
  TotalTax: Currency;
begin
  TotalTax := 0;
  with wwdsDetail.DataSet do
  begin
    First;
    DisableControls;
    while not EOF do
    begin
             //  Added By CA...  If any record of AMOUNT has null,  skip it
      if not VarIsNull(wwdsDetail.DataSet.FieldByName('AMOUNT').Value) then
        TotalTax := TotalTax + wwdsDetail.DataSet.FieldByName('AMOUNT').Value;
      Next;
    end;
    First;
    EnableControls;
  end;
  lablTotalTaxValue.Caption := CurrToStrF(TotalTax, ffCurrency, 2);
end;

procedure TEDIT_CO_STATE_TAX_LIABILITIES.wwdbtpCheckDateExit(
  Sender: TObject);
var
  NYThreshholdDate: TDateTime;
begin
  inherited;
  if (wwdsDetail.State in [dsInsert{, dsEdit}])
  and (DM_COMPANY.CO_STATES['STATE'] = 'NY') then
  begin
    try
      NYThreshholdDate := StrToDate(Copy(DM_COMPANY.CO_STATES.FieldByName('FILLER').AsString, 1, 10));
    except
      NYThreshholdDate := 0;
    end;
    wwdbtpCheckDate.Update;
    if ConvertNull(wwdsDetail.DataSet['CHECK_DATE'], StrToDate('1/1/2200')) < NYThreshholdDate then
      EvMessage('You backdate NY liabilities'#13'If you want to pay them automatically,'#13+
        'change NY threshhold date to any date before the check date'); 
  end;
end;

initialization
  RegisterClass(TEDIT_CO_STATE_TAX_LIABILITIES);

end.
