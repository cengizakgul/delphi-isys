// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_SERVICES;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, Wwdbspin, Wwdotdot,
  Wwdbcomb, StdCtrls, wwdbdatetimepicker, ExtCtrls, DBCtrls, wwdblook,
  Mask, wwdbedit, Buttons, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, Controls, Classes, SysUtils, EvSecElement, ActnList, Menus,
   Variants, EvConsts, Dialogs, EvTypes, 
  SDDClasses, ISBasicClasses, SDataDictbureau, SDataDictclient,
  SDataDicttemp, EvExceptions, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBDateTimePicker, isUIwwDBLookupCombo,
  isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIDBMemo;

type
  TEDIT_CO_SERVICES = class(TEDIT_CO_BASE)
    tshtServicesDetails: TTabSheet;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evPopupMenu1: TevPopupMenu;
    evActionList1: TevActionList;
    CopyService: TAction;
    DeleteService: TAction;
    CopyTo1: TMenuItem;
    Delete1: TMenuItem;
    fpEDIT_CO_SERVICES_RIGHT: TisUIFashionPanel;
    sbCompanyServices: TScrollBox;
    fpServices: TisUIFashionPanel;
    fpDiscounts: TisUIFashionPanel;
    fpSalesTax: TisUIFashionPanel;
    fpSummary: TisUIFashionPanel;
    wwDBGrid2: TevDBGrid;
    evLabel6: TevLabel;
    drgpSales_Tax: TevDBRadioGroup;
    evOverrideTax: TevDBEdit;
    lablService_Offered: TevLabel;
    lablName: TevLabel;
    lablEffective_Start_Date: TevLabel;
    lablEffective_End_Date: TevLabel;
    lablOverride_Flat_Fee: TevLabel;
    lablFrequency: TevLabel;
    lablMonth_Number: TevLabel;
    Label2: TevLabel;
    UpBtn: TevSpeedButton;
    DownBtn: TevSpeedButton;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    dedtName: TevDBEdit;
    wwlcService_Offered: TevDBLookupCombo;
    wwdpEffective_Start_Date: TevDBDateTimePicker;
    wwdpEffective_End_Date: TevDBDateTimePicker;
    wwdeOverride_Flat_Fee: TevDBEdit;
    CheckBox1: TevCheckBox;
    wwcbFrequency: TevDBComboBox;
    wwseMonth_Number: TevDBSpinEdit;
    wwcbWeek: TevDBComboBox;
    evBitBtn1: TevBitBtn;
    cbEDCode: TevDBLookupCombo;
    evDBComboBox1: TevDBComboBox;
    cbEDGroups: TevDBLookupCombo;
    cbWorkersComp: TevDBLookupCombo;
    EvBevel1: TEvBevel;
    fpServiceDiscounts: TisUIFashionPanel;
    evLabel32: TevLabel;
    evLabel33: TevLabel;
    evLabel34: TevLabel;
    dedtInvoiceDiscount: TevDBEdit;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    lablDiscount: TevLabel;
    lablDiscount_Start_Date: TevLabel;
    evLabel1: TevLabel;
    wwdpDiscount_Start_Date: TevDBDateTimePicker;
    wwdeDiscount: TevDBEdit;
    wwdpDiscount_End_Date: TevDBDateTimePicker;
    wwDBGrid1: TevDBGrid;
    fpNotes: TisUIFashionPanel;
    dmemNotes: TEvDBMemo;
    lablNature_Business: TevLabel;
    procedure CheckBox1Click(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure UpBtnClick(Sender: TObject);
    procedure DownBtnClick(Sender: TObject);
    procedure wwlcService_OfferedCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evBitBtn1Click(Sender: TObject);
    procedure CopyServiceExecute(Sender: TObject);
    procedure CopyServiceUpdate(Sender: TObject);
    procedure DeleteServiceUpdate(Sender: TObject);
    procedure DeleteServiceExecute(Sender: TObject);
    procedure evDBComboBox1Change(Sender: TObject);
    procedure fpEDIT_CO_SERVICES_RIGHTResize(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    constructor Create(AOwner: TComponent); override;
  private
//begin gdy
    FCurClient: integer;
    FCurCompany: integer;
    FCurService: integer;

    FClientRetrieveCondition: string;
    FCompanyRetrieveCondition: string;
    FServicesRetrieveCondition: string;

    procedure SaveClientState;
    procedure RestoreClientState;
//    function CreateDataSetWithSelectedRows( Source: TEvClientDataSet; bookmarks: TList ): TEvClientDataSet;
//end gdy
  end;

implementation

uses
  SPackageEntry, SPD_CompanyChoiceQuery,evUtils, windows;
{$R *.DFM}

procedure TEDIT_CO_SERVICES.CheckBox1Click(Sender: TObject);
begin
  inherited;
  if CheckBox1.Focused then
    if CheckBox1.Checked and (Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 1, 1) <> 'Y') then
    begin
      if wwdsDetail.DataSet.State = dsBrowse then
        if wwdsDetail.DataSet.RecordCount = 0 then
          wwdsDetail.DataSet.Insert
        else
          wwdsDetail.DataSet.Edit;
      wwdsDetail.DataSet.FieldByName('Filler').AsString := 'Y' + Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 2, Pred(wwdsDetail.DataSet.FieldByName('Filler').Size));
    end
    else if not CheckBox1.Checked and (Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 1, 1) = 'Y') then
    begin
      if wwdsDetail.DataSet.State = dsBrowse then
        if wwdsDetail.DataSet.RecordCount = 0 then
          wwdsDetail.DataSet.Insert
        else
          wwdsDetail.DataSet.Edit;
      wwdsDetail.DataSet.FieldByName('Filler').AsString := 'N' + Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 2, Pred(wwdsDetail.DataSet.FieldByName('Filler').Size));
    end;
end;

procedure TEDIT_CO_SERVICES.wwdsDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active then
    CheckBox1.Checked := Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 1, 1) = 'Y';
  UpBtn.Enabled := wwdsDetail.DataSet.Active and (wwdsDetail.DataSet.State = dsBrowse) and
                   (wwdsDetail.DataSet.RecNo > 1);
  DownBtn.Enabled := wwdsDetail.DataSet.Active and (wwdsDetail.DataSet.State = dsBrowse) and
                     (wwdsDetail.DataSet.RecNo < wwdsDetail.DataSet.RecordCount);
end;

procedure TEDIT_CO_SERVICES.UpBtnClick(Sender: TObject);
var
  Nbr: Integer;
  Sort1, Sort2: string;
begin
  inherited;
  DM_COMPANY.CO_SERVICES.DisableControls;
  try
    Nbr := wwdsDetail.DataSet.FieldByName('CO_SERVICES_NBR').AsInteger;
    Sort1 := wwdsDetail.DataSet.FieldByName('Sort').AsString;
    wwdsDetail.DataSet.Prior;
    Sort2 := wwdsDetail.DataSet.FieldByName('Sort').AsString;
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.FieldByName('Filler').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('Filler').AsString, Sort1, 2, 3);
    wwdsDetail.DataSet.Post;
    wwdsDetail.DataSet.Locate('CO_SERVICES_NBR', Nbr, []);
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.FieldByName('Filler').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('Filler').AsString, Sort2, 2, 3);
    wwdsDetail.DataSet.Post;
  finally
    DM_COMPANY.CO_SERVICES.EnableControls;
  end;
end;

procedure TEDIT_CO_SERVICES.DownBtnClick(Sender: TObject);
var
  Nbr: Integer;
  Sort1, Sort2: string;
begin
  inherited;
  DM_COMPANY.CO_SERVICES.DisableControls;
  try
    Nbr := wwdsDetail.DataSet.FieldByName('CO_SERVICES_NBR').AsInteger;
    Sort1 := wwdsDetail.DataSet.FieldByName('Sort').AsString;
    wwdsDetail.DataSet.Next;
    Sort2 := wwdsDetail.DataSet.FieldByName('Sort').AsString;
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.FieldByName('Filler').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('Filler').AsString, Sort1, 2, 3);
    wwdsDetail.DataSet.Post;
    wwdsDetail.DataSet.Locate('CO_SERVICES_NBR', Nbr, []);
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.FieldByName('Filler').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('Filler').AsString, Sort2, 2, 3);
    wwdsDetail.DataSet.Post;
  finally
    DM_COMPANY.CO_SERVICES.EnableControls;
  end;
end;

function TEDIT_CO_SERVICES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_SERVICES;
end;

function TEDIT_CO_SERVICES.GetInsertControl: TWinControl;
begin
  Result := wwlcService_Offered;
end;

procedure TEDIT_CO_SERVICES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_SERVICES, SupportDataSets, '');
  AddDS(DM_COMPANY.CO, EditDataSets);
end;

procedure TEDIT_CO_SERVICES.wwlcService_OfferedCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active and
     wwlcService_Offered.Focused and
     (wwlcService_Offered.Text <> '') and
     (wwdsDetail.DataSet.State <> dsBrowse) then
  begin
    wwdsDetail.DataSet.FieldByName('Filler').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('Filler').AsString, Copy(wwlcService_Offered.LookupTable.FieldByName('Filler').AsString, 1, 1), 1, 1);
    wwdsDetail.DataSet.FieldByName('Frequency').Assign(wwlcService_Offered.LookupTable.FieldByName('Frequency'));
    wwdsDetail.DataSet.FieldByName('Sales_Tax').Assign(wwlcService_Offered.LookupTable.FieldByName('Sales_Taxable'));
    wwdsDetail.DataSet.FieldByName('Month_Number').Assign(wwlcService_Offered.LookupTable.FieldByName('Month_Number'));
    wwdsDetail.DataSet.FieldByName('Week_Number').Assign(wwlcService_Offered.LookupTable.FieldByName('Week_Number'));
    wwdsDetail.DataSet.FieldByName('SB_SERVICES_NBR').Assign(wwlcService_Offered.LookupTable.FieldByName('SB_SERVICES_NBR'));
    wwdsDetail.DataSet.FieldByName('NAME').AsString := wwlcService_Offered.Text;
  end;
end;

procedure TEDIT_CO_SERVICES.evBitBtn1Click(Sender: TObject);
begin
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO_BILLING_HISTORY')
end;


procedure TEDIT_CO_SERVICES.CopyServiceExecute(Sender: TObject);
  procedure DoCopyServices;
    var
      SelectedServices: TevClientDataSet;
      k: integer;

    procedure DoCopyServicesTo( client, company: integer );
    var
      fn: string;
      k: integer;
      F: TField;
    begin
      ctx_DataAccess.OpenClient( client );

      DM_COMPANY.CO_SERVICES.DataRequired('CO_NBR=' + intToStr(company) );
      DM_COMPANY.CO.DataRequired('CO_NBR=' + intToStr(company) );

      SelectedServices.First;
      while not SelectedServices.Eof do
      begin
        if DM_COMPANY.CO_SERVICES.Locate('SB_SERVICES_NBR;NAME', VarArrayOf([SelectedServices['SB_SERVICES_NBR'],SelectedServices['NAME']]), []) then
          DM_COMPANY.CO_SERVICES.Edit
        else
          DM_COMPANY.CO_SERVICES.Insert;
        for k := 0 to SelectedServices.Fields.Count - 1 do
        begin
          fn := SelectedServices.Fields[k].FieldName;
          F := DM_COMPANY.CO_SERVICES.FindField(fn);
          if Assigned(F) then
          begin
            if (fn <> 'EFFECTIVE_DATE') and (fn <> 'EFFECTIVE_UNTIL') and (fn <> 'REC_VERSION')
               and ( fn <> 'CO_NBR' ) and ( fn <> 'CO_SERVICES_NBR' )
               and (F.FieldKind = fkData ) then
            begin
                F.Value := SelectedServices[fn];
                //DM_COMPANY.CO_SERVICES[fn] := SelectedServices[fn];
            end;
          end;
        end;
        DM_COMPANY.CO_SERVICES['CO_NBR'] := company;
        DM_COMPANY.CO_SERVICES.Post;
        SelectedServices.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_SERVICES]);

    end;

  begin
    with TCompanyChoiceQuery.Create( Self ) do
    try
      SetFilter( 'CUSTOM_COMPANY_NUMBER<>'''+DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'''' );
      Caption := 'Select the companies you want this service added to';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'A&dd';
      btnAll.Caption := '&Add to all';
      btnAll.Visible := False;
      ConfirmAllMessage := 'This operation will copy the services you have selected to all clients and companies. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        DM_COMPANY.CO_SERVICES.DisableControls;
        try
          SelectedServices := CreateDataSetWithSelectedRows( DM_COMPANY.CO_SERVICES, wwDBGrid1.SelectedList );
          try
            DM_TEMPORARY.TMP_CO.DisableControls;
            try
              for k := 0 to dgChoiceList.SelectedList.Count - 1 do
              begin
                cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                //ODS('copying to %d %d',[cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger]);

                  DM_COMPANY.CO.Close; //!!!!

                DoCopyServicesTo( cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger );
              end;
            finally
              DM_TEMPORARY.TMP_CO.EnableControls;
            end;
          finally
            SelectedServices.Free;
          end;
        finally
          DM_COMPANY.CO_SERVICES.EnableControls;
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  DM_COMPANY.CO_SERVICES.Cancel;
  SaveClientState;
  try
    DoCopyServices;
  finally
    RestoreClientState;
  end;
end;

procedure TEDIT_CO_SERVICES.CopyServiceUpdate(Sender: TObject);
begin
  (Sender as Taction).Enabled := wwDBGrid1.SelectedList.Count > 0;
end;

procedure TEDIT_CO_SERVICES.SaveClientState;
begin
  FCurClient := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  FClientRetrieveCondition := DM_CLIENT.CL.RetrieveCondition;

  FCurCompany := DM_COMPANY.CO['CO_NBR'];
  FCompanyRetrieveCondition := DM_COMPANY.CO.RetrieveCondition;

  FCurService := DM_COMPANY.CO_SERVICES['CO_SERVICES_NBR'];
  FServicesRetrieveCondition := DM_COMPANY.CO_SERVICES.RetrieveCondition;
end;

procedure TEDIT_CO_SERVICES.RestoreClientState;
begin
  ctx_DataAccess.OpenClient( FCurClient );

  DM_CLIENT.CL.DataRequired(FClientRetrieveCondition );
  DM_COMPANY.CO.DataRequired(FCompanyRetrieveCondition );
  DM_COMPANY.CO_SERVICES.DataRequired(FServicesRetrieveCondition);

  wwdsList.dataset.Locate('CO_NBR;CL_NBR', VarArrayOf([FCurCompany, FCurClient ]), []);
  DM_COMPANY.CO.Locate('CO_NBR',FCurCompany,[]);
  DM_COMPANY.CO_SERVICES.Locate('CO_SERVICES_NBR',FCurService,[]);
end;


procedure TEDIT_CO_SERVICES.DeleteServiceUpdate(Sender: TObject);
begin
  (Sender as Taction).Enabled := wwDBGrid1.SelectedList.Count > 0;
end;

procedure TEDIT_CO_SERVICES.DeleteServiceExecute(Sender: TObject);
  procedure DoDeleteServices;
    var
      SelectedServices: TevClientDataSet;
      k: integer;
    procedure DoDeleteServicesFrom( client, company: integer );
    begin
      ctx_DataAccess.OpenClient( client );

      DM_COMPANY.CO_SERVICES.DataRequired('CO_NBR=' + intToStr(company) );
      SelectedServices.First;
      while not SelectedServices.Eof do
      begin
        if DM_COMPANY.CO_SERVICES.Locate('SB_SERVICES_NBR;NAME', VarArrayOf([SelectedServices['SB_SERVICES_NBR'],SelectedServices['NAME']]), []) then
          DM_COMPANY.CO_SERVICES.Delete;
        SelectedServices.Next;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_SERVICES]);
    end;

  begin
    with TCompanyChoiceQuery.Create( Self ) do
    try
      SetFilter( '' );
      Caption := 'Select the companies you want this service deleted from';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := '&Delete';
      btnAll.Caption := 'Delete from &all';
      btnAll.Visible := False;
      ConfirmAllMessage := 'This operation will delete the services you have selected from all clients and companies. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        DM_COMPANY.CO_SERVICES.DisableControls;
        try
          SelectedServices := CreateDataSetWithSelectedRows( DM_COMPANY.CO_SERVICES, wwDBGrid1.SelectedList );
          try
            DM_TEMPORARY.TMP_CO.DisableControls;
            try
              for k := 0 to dgChoiceList.SelectedList.Count - 1 do
              begin
                cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                  DM_COMPANY.CO.Close; //!!!!
                DoDeleteServicesFrom( cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger );
              end;
            finally
              DM_TEMPORARY.TMP_CO.EnableControls;
            end;
          finally
            SelectedServices.Free;
          end;
        finally
          DM_COMPANY.CO_SERVICES.EnableControls;
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  DM_COMPANY.CO_SERVICES.Cancel;
  SaveClientState;
  try
    DoDeleteServices;
  finally
    RestoreClientState;
  end;
end;

procedure TEDIT_CO_SERVICES.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind = NavOK then
  begin
    if (DM_COMPANY.CO_SERVICES.FieldByName('FREQUENCY').AsString = SCHED_FREQ_ONE_TIME) and
       (Copy(DM_COMPANY.CO_SERVICES.FieldByName('FILLER').AsString, 5, 1) = 'Y') and
       (EvMessage('Do you want to use this service again?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
      DM_COMPANY.CO_SERVICES.FieldByName('Filler').AsString := PutIntoFiller(DM_COMPANY.CO_SERVICES.FieldByName('Filler').AsString, ' ', 5, 1);
    if  TDataSet(DM_CLIENT.CO).State in [dsEdit,dsInsert] then
        DM_CLIENT.CO.UpdateRecord;
    if( DM_CLIENT.CO.INVOICE_DISCOUNT.Value <-100) or (DM_CLIENT.CO.INVOICE_DISCOUNT.Value > 100) then
       raise EUpdateError.CreateHelp('Invoice discount value must be between -100 and 100!', IDH_ConsistencyViolation);
  end;
end;

procedure TEDIT_CO_SERVICES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_COMPANY.CL_E_D_GROUPS, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  inherited;
end;

procedure TEDIT_CO_SERVICES.evDBComboBox1Change(Sender: TObject);
begin
  inherited;
  fpServiceDiscounts.Visible := (evDBComboBox1.Value = 'A') or (evDBComboBox1.Value = 'P');
  if fpServiceDiscounts.Visible then
    fpNotes.Width := fpServiceDiscounts.Left + fpServiceDiscounts.Width - fpSalesTax.Left
  else
    fpNotes.Width := fpDiscounts.Left + fpDiscounts.Width - fpSalesTax.Left;
end;

constructor TEDIT_CO_SERVICES.Create(AOwner: TComponent);
begin
  inherited;
  //assign the wwDBGrid1 to the fashion panel
  pnlSubbrowse.Parent := fpEDIT_CO_SERVICES_RIGHT;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_SERVICES.fpEDIT_CO_SERVICES_RIGHTResize(
  Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZE THE wwDBGrid1
  pnlSubbrowse.Width :=  fpEDIT_CO_SERVICES_RIGHT.Width - 40;
  pnlSubbrowse.Height := fpEDIT_CO_SERVICES_RIGHT.Height - 65;
end;

initialization
  classes.RegisterClass(TEDIT_CO_SERVICES);

end.
