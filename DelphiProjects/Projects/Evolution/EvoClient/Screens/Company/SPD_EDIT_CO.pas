// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Db, Wwdatsrc, StdCtrls, Mask, DBCtrls, SPackageEntry,
  SPD_EDIT_CO_BASE, SDataStructure, ExtCtrls, Wwdbspin, EvUtils,
  wwdblook, Buttons, wwdbdatetimepicker, Wwdotdot, Wwdbcomb, wwdbedit,
  Grids, Wwdbigrd, Wwdbgrid, ImgList, ActnList,
  EvConsts, EvSecElement, menus, EvTypes,  Variants, DateUtils,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvContext,
  EvDataAccessComponents, ISDataAccessComponents, SFrameEntry, SSecurityInterface,
  SDataDictsystem, SDataDictbureau, SDataDictclient, SDataDicttemp, isVCLBugFix,
  EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBDateTimePicker, isUIwwDBComboBox,
  isUIDBMemo, isUIwwDBEdit, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIEdit, Spin, SPD_EmployeeChoiceQuery, EvCommonInterfaces;

type
  TEDIT_CO = class(TEDIT_CO_BASE)
    tshtAddress: TTabSheet;
    tshtFederal_Reporting: TTabSheet;
    tshtChecks: TTabSheet;
    tshtGeneral_Business: TTabSheet;
    tshtRemote_Client: TTabSheet;
    tshtWComp_And_Benefits: TTabSheet;
    tshtMisc: TTabSheet;
    FederalTax: TTabSheet;
    GeneralLedger: TTabSheet;
    tsCashManagement: TTabSheet;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL;
    srcCoStates: TevDataSource;
    evSecElement1: TevSecElement;
    pmFed_Tax_Deposit_Frequency: TevPopupMenu;
    cdsSbBankAccountsForAch: TevClientDataSet;
    FTDFCopyTo: TMenuItem;
    pmFed945_Tax_Deposit_Frequency: TevPopupMenu;
    Fed945TDFCopyTo: TMenuItem;
    pmFUI_Tax_Deposit_Frequency: TevPopupMenu;
    FUITDFCopyTo: TMenuItem;
    tshtAca: TTabSheet;
    sbAca: TScrollBox;
    fpAca: TisUIFashionPanel;
    tsVMR: TTabSheet;
    cdsSbBankAccountsForTrust: TevClientDataSet;
    cdsSbBankAccountsForTax: TevClientDataSet;
    cdsSbBankAccountsForBilling: TevClientDataSet;
    cdsSbBankAccountsForWC: TevClientDataSet;
    cdsSbBankAccountsForOBC: TevClientDataSet;
    evLabel3: TevLabel;
    pmEnable_ESS: TevPopupMenu;
    Enable_ESSCopyTo: TMenuItem;
    pmTaxImpound: TevPopupMenu;
    MenuItem1: TMenuItem;
    pmTrustImpound: TevPopupMenu;
    MenuItem2: TMenuItem;
    pmDDImpound: TevPopupMenu;
    MenuItem3: TMenuItem;
    pmBillingImpound: TevPopupMenu;
    MenuItem4: TMenuItem;
    pmWCImpound: TevPopupMenu;
    MenuItem5: TMenuItem;
    pmProcessLimitations: TevPopupMenu;
    MenuItem6: TMenuItem;
    pmMicsCheck: TevPopupMenu;
    miMiscCheckFormCopyTo: TMenuItem;
    pmCheckForm: TevPopupMenu;
    miCheckFormCopyTo: TMenuItem;
    pmSalesTax: TevPopupMenu;
    MenuItem7: TMenuItem;
    pmShowRateHours: TevPopupMenu;
    miCopyShowRateHours: TMenuItem;
    pmEnable_Time_Off: TevPopupMenu;
    Enable_TimeOffCopyTo: TMenuItem;
    pmEnable_Benefits: TevPopupMenu;
    Enable_BenefitsCopyTo: TMenuItem;
    pmEnable_HR: TevPopupMenu;
    Enable_HRCopyTo: TMenuItem;
    sbCompanyAddress: TScrollBox;
    fpAddress: TisUIFashionPanel;
    lablCompany_Code: TevLabel;
    lablName: TevLabel;
    lablDBA: TevLabel;
    lablAddress1: TevLabel;
    lablAddress_2: TevLabel;
    lablCity: TevLabel;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    lablCounty: TevLabel;
    lablE_Mail_Address: TevLabel;
    dedtCompany_Code: TevDBEdit;
    dedtName: TevDBEdit;
    dedtDBA: TevDBEdit;
    dedtAddress_1: TevDBEdit;
    dedtAddress_2: TevDBEdit;
    dedtCity: TevDBEdit;
    dedtState: TevDBEdit;
    dedtCounty: TevDBEdit;
    dedtE_Mail_Address: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    evBitBtn2: TevBitBtn;
    fpLegal: TisUIFashionPanel;
    lablLegal_Name: TevLabel;
    lablLegal_Address_1: TevLabel;
    lablLegal_Address_2: TevLabel;
    lablLegal_City: TevLabel;
    lablLegal_State: TevLabel;
    lablLegal_Zip_Code: TevLabel;
    dedtLegal_Name: TevDBEdit;
    dedtLegal_Address_1: TevDBEdit;
    dedtLegal_Address_2: TevDBEdit;
    dedtLegal_City: TevDBEdit;
    dedtLegal_State: TevDBEdit;
    wwdeLegal_Zip_Code: TevDBEdit;
    sbGeneralBusiness: TScrollBox;
    fpGeneral: TisUIFashionPanel;
    fpBankAccounts: TisUIFashionPanel;
    fpNotes: TisUIFashionPanel;
    fpDelivery: TisUIFashionPanel;
    lablDelivery_Group_Number: TevLabel;
    lablAnnual_Delivery_Group_Number: TevLabel;
    lablQrtly_Delivery_Group_Number: TevLabel;
    lablPercentage: TevLabel;
    wwlcDelivery_Group_Number: TevDBLookupCombo;
    wwlcAnnual_Delivery_Group_Number: TevDBLookupCombo;
    wwlcQrtly_Delivery_Group_Number: TevDBLookupCombo;
    wwdePercentage: TevDBEdit;
    lablNature_Business: TevLabel;
    lablCompany_Notes: TevLabel;
    dmemNature_Business: TEvDBMemo;
    dmemCompany_Notes: TEvDBMemo;
    fpReporting: TisUIFashionPanel;
    lablCommon_Paymaster_Number: TevLabel;
    lablCL_CO_Consolidation_Number: TevLabel;
    lablCustomerService: TevLabel;
    evLabel46: TevLabel;
    wwlcCommon_Paymaster_Number: TevDBLookupCombo;
    wwlcCL_CO_Consolidation_Number: TevDBLookupCombo;
    dblkupCustomerServiceUser: TevDBLookupCombo;
    evDBEdit14: TevDBEdit;
    lablBusiness_Start_Date: TevLabel;
    evLabel28: TevLabel;
    lablAccountant: TevLabel;
    lablContact: TevLabel;
    lablBusiness_Type: TevLabel;
    lablCorporation_Type: TevLabel;
    lablSIC_Code: TevLabel;
    lablFiscal_Year_End: TevLabel;
    lablDays_Open: TevLabel;
    lablTime_Closed: TevLabel;
    lablTime_Open: TevLabel;
    lablProcessPriority: TevLabel;
    evLabel50: TevLabel;
    wwdpBusiness_Start_Date: TevDBDateTimePicker;
    wwdpInitial_Effective_date: TevDBDateTimePicker;
    wwlcAccountant: TevDBLookupCombo;
    dedtContact: TevDBEdit;
    wwcbBusiness_Type: TevDBComboBox;
    wwcbCorporation_Type: TevDBComboBox;
    dedtSIC_Code: TevDBEdit;
    dedtFiscal_Year_End: TevDBEdit;
    evDBSpinEdit1: TevDBSpinEdit;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evDBDateTimePicker1: TevDBDateTimePicker;
    wwseProcessPriority: TevDBSpinEdit;
    drgpRestaurant: TevDBRadioGroup;
    drgpNon_Profit: TevDBRadioGroup;
    drgpSuccessor_Company: TevDBRadioGroup;
    edEmployerType: TevDBComboBox;
    lablBank_Account_Level: TevLabel;
    lablPayroll_Bank_Account_Number: TevLabel;
    lablTax_Bank_Account_Number: TevLabel;
    lablDD_Bank_Account_Number: TevLabel;
    lablBilling_Bank_Account_Number: TevLabel;
    lablWorkers_Comp_Bank_Account_Number: TevLabel;
    evLabel29: TevLabel;
    wwcbBank_Account_Level: TevDBComboBox;
    wwlcPayroll_Bank_Account_Number: TevDBLookupCombo;
    wwlcTax_Bank_Account_Number: TevDBLookupCombo;
    wwlcDD_Bank_Account_Number: TevDBLookupCombo;
    wwlcBilling_Bank_Account_Number: TevDBLookupCombo;
    wwlcWorkers_Comp_Bank_Account_Number: TevDBLookupCombo;
    cbManualBankAccount: TevDBLookupCombo;
    evDBRadioGroup9: TevDBRadioGroup;
    fpCPA: TisUIFashionPanel;
    drgpPrint_CPA: TevDBRadioGroup;
    drgpNameOnInvoice: TevDBRadioGroup;
    sbPayroll: TScrollBox;
    fpPayroll: TisUIFashionPanel;
    lablPay_Frequencies: TevLabel;
    lablPay_Frequency_Hourly_Default: TevLabel;
    lablPrimary_Sort_Field: TevLabel;
    lablSecondary_Sort_Field: TevLabel;
    lablPayroll_Password: TevLabel;
    lablPayrate_Precision: TevLabel;
    evLabel11: TevLabel;
    wwcbPay_Frequencies: TevDBComboBox;
    wwcbPay_Frequency_Hourly_Default: TevDBComboBox;
    wwcbPrimary_Sort_Field: TevDBComboBox;
    wwcbSecondary_Sort_Field: TevDBComboBox;
    dedtPayroll_Password: TevDBEdit;
    evDBRadioGroup3: TevDBRadioGroup;
    drgpAuto_Labor_Dist_Show_Deducts: TevDBRadioGroup;
    wwdePayrate_Precision: TevDBEdit;
    evDBComboBox1: TevDBComboBox;
    fpMisc: TisUIFashionPanel;
    evLabel15: TevLabel;
    lablDBDT_Level: TevLabel;
    evDBComboBox5: TevDBComboBox;
    wwcbDBDT_Level: TevDBComboBox;
    fpTimeOff: TisUIFashionPanel;
    fpAutoReduction: TisUIFashionPanel;
    fpEmployees: TisUIFashionPanel;
    evLabel35: TevLabel;
    drgpTime_Off_Accrual: TevDBRadioGroup;
    DBCboCheckTOBalance: TevDBComboBox;
    LabelAutoReductionEDGroup: TevLabel;
    DBlookAutoReductionED: TevDBLookupCombo;
    DBRadioGrpAutoReductionEEs: TevDBRadioGroup;
    lablAuto_Increment: TevLabel;
    lablAverage_Hours: TevLabel;
    dedtAuto_Increment: TevDBEdit;
    btnCopyEEs: TevBitBtn;
    wwdeAverage_Hours: TevDBEdit;
    btnNewHireSetup: TevButton;
    EvBevel2: TEvBevel;
    sbChecks: TScrollBox;
    fpOptions: TisUIFashionPanel;
    fpShow: TisUIFashionPanel;
    lablCO_Check_Primary_Sort: TevLabel;
    lablCO_Check_Secondary_Sort: TevLabel;
    lablCheck_Form: TevLabel;
    evLabel17: TevLabel;
    wwcbCO_Check_Primary_Sort: TevDBComboBox;
    wwcbCO_Check_Secondary_Sort: TevDBComboBox;
    evDBRadioGroup5: TevDBRadioGroup;
    cbCheckForm: TevComboBox;
    cbMiscCheckForm: TevComboBox;
    drgpShow_SS_Number_Check: TevDBRadioGroup;
    drgpShow_Rate_ByHours: TevDBRadioGroup;
    drgpShow_YDT_Check: TevDBRadioGroup;
    drgpShow_Shifts_Check: TevDBRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBRadioGroup2: TevDBRadioGroup;
    drgpShowShortfall: TevDBRadioGroup;
    drgpShow_Punch_Details: TevDBRadioGroup;
    evDBRadioGroup6: TevDBRadioGroup;
    fpNavigation: TisUIFashionPanel;
    evBitBtn3: TevBitBtn;
    evBitBtn1: TevBitBtn;
    fpStatus: TisUIFashionPanel;
    lablStart_Date: TevLabel;
    lablTermination_Date: TevLabel;
    lablStatus_Code: TevLabel;
    lablTermination_Notes: TevLabel;
    evLabel13: TevLabel;
    dmemTermination_Notes: TEvDBMemo;
    wwcbStatus_Code: TevDBComboBox;
    wwdpStart_Date: TevDBDateTimePicker;
    wwdpTermination_Date: TevDBDateTimePicker;
    wwcbOtherService: TevDBLookupCombo;
    EvBevel3: TEvBevel;
    Label28: TevLabel;
    lablAuto_Labor_Dist_Level: TevLabel;
    wwDBComboBox2: TevDBComboBox;
    wwcbAuto_Labor_Dist_Level: TevDBComboBox;
    evDBRadioGroup8: TevDBRadioGroup;
    drgpCollateChecks: TevDBRadioGroup;
    drgpPrint_Manual_Check_Stubs: TevDBRadioGroup;
    lablTargetsOnCheck: TevLabel;
    lablCheckMessage: TevLabel;
    LabelReprintTOBalances: TevLabel;
    wwcbTargetsOnCheck: TevDBComboBox;
    wwdpCheckMessage: TevDBEdit;
    wwcbReprintToBalances: TevDBComboBox;
    lblShowPaystub: TevLabel;
    edDaysDue: TevDBEdit;
    evLabel54: TevLabel;
    sbTaxReporting: TScrollBox;
    lablFederal_Tax_Transfer_Method: TevLabel;
    wwcbFederal_Tax_Transfer_Method: TevDBComboBox;
    fpFederal: TisUIFashionPanel;
    lablEIN_Number: TevLabel;
    lablFederal_Tax_Deposit_Frequency: TevLabel;
    evLabel14: TevLabel;
    lablFUI_Tax_Deposit_Frequency: TevLabel;
    lablFUI_Rate_Override: TevLabel;
    dedtEIN_Number: TevDBEdit;
    wwcbFederal_Tax_Deposit_Frequency: TevDBComboBox;
    wwcbFUI_Tax_Deposit_Frequency: TevDBComboBox;
    wwdeFUI_Override_Rate: TevDBEdit;
    wwdb945TaxFreq: TevDBComboBox;
    fpReportingAndPayment: TisUIFashionPanel;
    Label14: TevLabel;
    Label15: TevLabel;
    lablFUI_Tax_Payment_Angency: TevLabel;
    lablFUI_Tax_Report_Agency: TevLabel;
    wwDBLookupCombo2: TevDBLookupCombo;
    wwDBLookupCombo3: TevDBLookupCombo;
    wwlcFUI_Tax_Payment_Angency: TevDBLookupCombo;
    wwlcFUI_Tax_Report_Agency: TevDBLookupCombo;
    fpTaxExemptions: TisUIFashionPanel;
    drgpApply_Misc_Limit_To_1099: TevDBRadioGroup;
    drgpFederal_Tax_Exempt_Status: TevDBRadioGroup;
    drgpFed_Tax_Exempt_EE_OASDI: TevDBRadioGroup;
    drgpWithholding_Default: TevDBRadioGroup;
    drgpFUI_Tax_Exempt: TevDBRadioGroup;
    drgpFed_Tax_Exempt_ER_Medicare: TevDBRadioGroup;
    drgpFed_Tax_Exempt_EE_Medicare: TevDBRadioGroup;
    drgpFed_Tax_Exempt_ER_OASDI: TevDBRadioGroup;
    fpState: TisUIFashionPanel;
    lablHome_State: TevLabel;
    lablHome_SDI_State: TevLabel;
    lablHome_SUI_State: TevLabel;
    wwlcHome_State: TevDBLookupCombo;
    wwlcHome_SDI_State: TevDBLookupCombo;
    wwlcHome_SUI_State: TevDBLookupCombo;
    bvFederal: TEvBevel;
    lablFederal_Tax_Payment_Method: TevLabel;
    wwcbFederal_Tax_Payment_Method: TevDBComboBox;
    EvBevel4: TEvBevel;
    gbQuarterLock: TevGroupBox;
    lablLockDate: TevLabel;
    dtpcLockDate: TevDBDateTimePicker;
    evDBCheckBox1: TevDBCheckBox;
    sbTaxDefaults: TScrollBox;
    fpEFTPS: TisUIFashionPanel;
    fpEnlistment: TisUIFashionPanel;
    fpTaxDefaultOptions: TisUIFashionPanel;
    lablEnrollment_Status: TevLabel;
    lablEnrollment_Date: TevLabel;
    lablSequence_Number: TevLabel;
    lablName2: TevLabel;
    lablPIN_Number: TevLabel;
    dedtSequence_Number: TevDBEdit;
    dedtName2: TevDBEdit;
    dedtPIN_Number: TevDBEdit;
    wwcbEnrollment_Status: TevDBComboBox;
    wwdpEFTPS_Enrollment_Date: TevDBDateTimePicker;
    lablEnrollment_Number: TevLabel;
    dedtEnrollment_Number: TevDBEdit;
    evLabel31: TevLabel;
    drgpPutReturnsOnHold: TevDBRadioGroup;
    edControlName: TevDBEdit;
    rgAutoEnlistQ: TevDBRadioGroup;
    drgpDBA_On_Tax_Return: TevDBRadioGroup;
    evLabel1: TevLabel;
    eMINIMUM_TAX_THRESHOLD: TevDBEdit;
    drgpExternal_Tax_Export: TevDBRadioGroup;
    lablTaxCoverLetterNotes: TevLabel;
    dmemTaxCoverLetterNotes: TEvDBMemo;
    evLabel53: TevLabel;
    evDBLookupCombo8: TevDBLookupCombo;
    evLabel12: TevLabel;
    drgpMakeup_Tax_Deduct_Shortfalls: TevDBRadioGroup;
    rgCalculateLocalFirst: TevDBRadioGroup;
    evDBRadioGroup10: TevDBRadioGroup;
    evDBComboBox2: TevDBComboBox;
    sbCashManagement: TScrollBox;
    fpCMServices: TisUIFashionPanel;
    lablOBC_End_Date: TevLabel;
    lablOBC_Start_Date: TevLabel;
    lablTrust_Service_End_Date: TevLabel;
    lablTrust_Service_Start_Date: TevLabel;
    lablTax_Service_End_Date: TevLabel;
    lablTax_Service_Start_Date: TevLabel;
    drgpTax_Service: TevDBRadioGroup;
    drgpTrust_Service: TevDBRadioGroup;
    drgpOBC_Service: TevDBRadioGroup;
    wwdpOBC_End_Date: TevDBDateTimePicker;
    wwdpOBC_Start_Date: TevDBDateTimePicker;
    wwdpTrust_Service_End_Date: TevDBDateTimePicker;
    wwdpTrust_Service_Start_Date: TevDBDateTimePicker;
    wwdpTax_Service_End_Date: TevDBDateTimePicker;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    fpBilling: TisUIFashionPanel;
    lablBilling_Information: TevLabel;
    lablBilling_Level: TevLabel;
    wwlcBilling_Information: TevDBLookupCombo;
    wwcbBilling_Level: TevDBComboBox;
    cbBillingCPA: TevDBCheckBox;
    fpSBBankAccounts: TisUIFashionPanel;
    lablSB_Trust_Account_Number: TevLabel;
    lablSB_Tax_Bank_Account_Number: TevLabel;
    lablSB_ACH_Bank_Account_Number: TevLabel;
    lablSB_Billing_Bank_Account_Number: TevLabel;
    lablSB_OBC_Account_Nbr: TevLabel;
    lablSB_Workers_Comp_Bank_Account_Number: TevLabel;
    wwlcSB_Trust_Account_Number: TevDBLookupCombo;
    wwlcSB_Tax_Bank_Account_Number: TevDBLookupCombo;
    wwlcSB_ACH_Bank_Account_Number: TevDBLookupCombo;
    wwlcSB_Billing_Bank_Account_Number: TevDBLookupCombo;
    wwlcSB_OBC_Account_Nbr: TevDBLookupCombo;
    wwlcSB_Workers_Comp_Bank_Account_Number: TevDBLookupCombo;
    fpCashManagement: TisUIFashionPanel;
    fpCMInvoicing: TisUIFashionPanel;
    evLabel26: TevLabel;
    lablInvoiceNotes: TevLabel;
    evDBComboBox7: TevDBSpinEdit;
    cbProrateFlatFee: TevDBCheckBox;
    dmemInvoiceNotes: TEvDBMemo;
    fpACH: TisUIFashionPanel;
    evLabel30: TevLabel;
    evDBEdit6: TevDBEdit;
    btnChangePrenote: TevBitBtn;
    fpMgmtSettings: TisUIFashionPanel;
    evLabel49: TevLabel;
    evDBRadioGroup11: TevDBRadioGroup;
    wwseDaysPriorToCheckDate: TevDBSpinEdit;
    lablCredit_Hold: TevLabel;
    wwcbCredit_Hold: TevDBComboBox;
    evLabel16: TevLabel;
    evDBEdit15: TevDBEdit;
    evLabel42: TevLabel;
    evLabel43: TevLabel;
    evLabel44: TevLabel;
    evLabel45: TevLabel;
    evLabel47: TevLabel;
    evLabel48: TevLabel;
    lablMaximum_Hours_Check: TevLabel;
    lablMaximum_Dollars_Check: TevLabel;
    evDBEdit11: TevDBEdit;
    evDBEdit12: TevDBEdit;
    evDBEdit13: TevDBEdit;
    evDBComboBox12: TevDBComboBox;
    evDBComboBox13: TevDBComboBox;
    evDBComboBox14: TevDBComboBox;
    wwdpMaximum_Hours_On_Check: TevDBEdit;
    wwDBEdit2: TevDBEdit;
    gbImpound: TevGroupBox;
    evLabel32: TevLabel;
    evLabel41: TevLabel;
    evLabel40: TevLabel;
    evLabel34: TevLabel;
    evLabel33: TevLabel;
    lablDebit_Number_Days_Prior_PR: TevLabel;
    TrustComboBox: TevDBComboBox;
    TaxComboBox: TevDBComboBox;
    WCComboBox: TevDBComboBox;
    BillingComboBox: TevDBComboBox;
    DDComboBox: TevDBComboBox;
    dedtDebit_Number_Days_Prior_PR: TevDBEdit;
    dedtDebit_Number_Days_Prior_Tax: TevDBEdit;
    dedtDebit_Number_Days_Prior_DD: TevDBEdit;
    dedtDebit_Number_Days_Prior_Bill: TevDBEdit;
    dedtDebit_Number_Days_Prior_WC: TevDBEdit;
    evLabel7: TevLabel;
    EvBevel5: TEvBevel;
    sbWCandBenies: TScrollBox;
    fpWorkersComp: TisUIFashionPanel;
    fpBenefits: TisUIFashionPanel;
    lablClient_Workers_Comp_Agency: TevLabel;
    lablWorkers_Comp_Policy_ID: TevLabel;
    lablWorkers_Comp_Number: TevLabel;
    Label1: TevLabel;
    Label27: TevLabel;
    lablDiscountRate: TevLabel;
    lablModRate: TevLabel;
    evLabel51: TevLabel;
    dtFiscalBeginDate: TevDBDateTimePicker;
    dedtWorkers_Comp_Policy_ID: TevDBEdit;
    wwlcClient_Workers_Comp_Agency: TevDBLookupCombo;
    wwlcWorkers_Comp_Number: TevDBLookupCombo;
    wwDBLookupCombo1: TevDBLookupCombo;
    wwDBLookupCombo4: TevDBLookupCombo;
    wwdeDiscountRate: TevDBEdit;
    wwdeModRate: TevDBEdit;
    edFiscalBeginDate: TevEdit;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel8: TevLabel;
    evLabel9: TevLabel;
    evDBSpinEdit2: TevDBSpinEdit;
    evDBSpinEdit3: TevDBSpinEdit;
    evDBSpinEdit4: TevDBSpinEdit;
    evDBEdit4: TevDBEdit;
    cbChargeAdminFee: TevDBCheckBox;
    lablRetirement_Age: TevLabel;
    lablMaximum_Group_Term_Life: TevLabel;
    dedtRetirement_Age: TevDBEdit;
    wwdeMaximum_Group_Term_Life: TevDBEdit;
    evDBCheckBox2: TevDBCheckBox;
    evDBCheckBox3: TevDBCheckBox;
    sbGeneralLedger: TScrollBox;
    fpGL: TisUIFashionPanel;
    Label13: TevLabel;
    DBEdit12: TevDBEdit;
    sbMailRoom: TScrollBox;
    fpMailRoom: TisUIFashionPanel;
    evLabel18: TevLabel;
    evLabel19: TevLabel;
    evLabel20: TevLabel;
    evLabel21: TevLabel;
    evLabel22: TevLabel;
    evLabel23: TevLabel;
    evLabel24: TevLabel;
    evLabel25: TevLabel;
    edVmrAgencyChecks: TevDBLookupCombo;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBLookupCombo4: TevDBLookupCombo;
    evDBLookupCombo5: TevDBLookupCombo;
    evDBLookupCombo6: TevDBLookupCombo;
    evDBLookupCombo7: TevDBLookupCombo;
    sbRemoteClient: TScrollBox;
    lablTransmission_Destination: TevLabel;
    wwcbTransmissionDestination: TevDBComboBox;
    fpSwipeClock: TisUIFashionPanel;
    evLabel52: TevLabel;
    evSwipeClockSiteNumber: TevDBEdit;
    Label3: TevLabel;
    Label35: TevLabel;
    Label37: TevLabel;
    Label38: TevLabel;
    Label39: TevLabel;
    Label40: TevLabel;
    Label41: TevLabel;
    Label42: TevLabel;
    Label43: TevLabel;
    Label44: TevLabel;
    Label45: TevLabel;
    evLabel10: TevLabel;
    evLabel37: TevLabel;
    evLabel39: TevLabel;
    evLabel55: TevLabel;
    evLabel56: TevLabel;
    evLabel2: TevLabel;
    evLabel27: TevLabel;
    DBEdit3: TevDBEdit;
    DBEdit4: TevDBEdit;
    DBEdit5: TevDBEdit;
    DBEdit6: TevDBEdit;
    DBEdit7: TevDBEdit;
    DBEdit8: TevDBEdit;
    DBEdit9: TevDBEdit;
    DBEdit10: TevDBEdit;
    DBEdit11: TevDBEdit;
    DBEdit13: TevDBEdit;
    DBEdit14: TevDBEdit;
    DBEdit20: TevDBEdit;
    DBEdit22: TevDBEdit;
    DBEdit23: TevDBEdit;
    DBEdit24: TevDBEdit;
    DBEdit25: TevDBEdit;
    DBEdit26: TevDBEdit;
    DBEdit27: TevDBEdit;
    DBEdit28: TevDBEdit;
    DBEdit29: TevDBEdit;
    DBEdit30: TevDBEdit;
    evDBEdit1: TevDBEdit;
    evDBEdit5: TevDBEdit;
    evDBEdit2: TevDBEdit;
    evDBEdit7: TevDBEdit;
    evDBEdit8: TevDBEdit;
    evDBEdit9: TevDBEdit;
    evDBEdit10: TevDBEdit;
    evLabel36: TevLabel;
    EvBevel6: TEvBevel;
    Label2: TevLabel;
    Label29: TevLabel;
    Label33: TevLabel;
    Label34: TevLabel;
    evLabel4: TevLabel;
    lblCobraDR: TevLabel;
    DBEdit2: TevDBEdit;
    DBEdit1: TevDBEdit;
    DBEdit18: TevDBEdit;
    DBEdit19: TevDBEdit;
    evDBEdit3: TevDBEdit;
    edCobraCreditTagDR: TevDBEdit;
    evLabel38: TevLabel;
    EvBevel7: TEvBevel;
    edSneakyEditTabStop0ToPreventAutoPopup: TEdit;
    lablW2_Type: TevLabel;
    wwcbAnualFormType: TevDBComboBox;
    rgEducationOrg: TevDBRadioGroup;
    evGroupBox1: TevGroupBox;
    evLabel57: TevLabel;
    evGroupBox2: TevGroupBox;
    evLabel58: TevLabel;
    evGroupBox3: TevGroupBox;
    evGroupBox4: TevGroupBox;
    evLabel59: TevLabel;
    evLabel60: TevLabel;
    evGroupBox5: TevGroupBox;
    evLabel61: TevLabel;
    evLabel62: TevLabel;
    evGroupBox6: TevGroupBox;
    wwlcE_D_Group: TevDBLookupCombo;
    evLabel63: TevLabel;
    cbACAStatus: TevDBComboBox;
    evDBRadioGroup12: TevDBRadioGroup;
    AcaToMonth: TevComboBox;
    AcaStabFromMonth: TevComboBox;
    AcaStabToMonth: TevComboBox;
    AcaFromMonth: TevComboBox;
    evDBRadioGroup13: TevDBRadioGroup;
    edtACAHours: TevDBEdit;
    evDBLookupCombo9: TevDBLookupCombo;
    AcaFromYear: TevComboBox;
    AcaToYear: TevComboBox;
    AcaStabFromYear: TevComboBox;
    AcaStabToYear: TevComboBox;
    pmAnnualFormType: TevPopupMenu;
    CopyTo1: TMenuItem;
    pmEnable_DD: TevPopupMenu;
    Enable_DDCopyTo: TMenuItem;
    pmEnable_EEAccess: TevPopupMenu;
    Enable_EEAcces: TMenuItem;
    mnAnnualTypeCopyToEE: TMenuItem;
    rgEEDefaultPrintVoucher: TevDBRadioGroup;
    pmEEPrintVoucher: TevPopupMenu;
    mnCOPrintVoucher: TMenuItem;
    mnEEPrintVoucher: TMenuItem;

    cdAvailableCertificationsEligibility: TevClientDataSet;
    dsAvailableCertificationsEligibility: TevDataSource;
    dsAssignedCertificationsEligibility: TevDataSource;
    cdAvailableCertificationsEligibilityACA_CERT_ELIGIBILITY_TYPE: TStringField;
    cdAvailableCertificationsEligibilityDESCRIPTION: TStringField;
    grdAvailableCertificationsEligibility: TevDBGrid;
    grdAssignedCertificationsEligibility: TevDBGrid;
    btnAddCert: TevSpeedButton;
    btnRemoveCert: TevSpeedButton;

    pmPrintCPA: TevPopupMenu;
    mnPrintCPA: TMenuItem;
    isUIFashionPanel1: TisUIFashionPanel;
    rgIncludeAnalytics: TevDBRadioGroup;
    evLabel69: TevLabel;
    cbAnalyticsLevel: TevDBComboBox;
    pmEnableAnalytics: TevPopupMenu;
    mnEnableAnalytics: TMenuItem;
    pmAnalyticsLevel: TevPopupMenu;
    mnAnalyticsLevel: TMenuItem;
    rgBenefitEligible: TevDBRadioGroup;
    pmBenefitsEligibleDefault: TevPopupMenu;
    mnBenefitsEligibleDefaultCO: TMenuItem;
    mnBenefitsEligibleDefaultEE: TMenuItem;
    evLabel70: TevLabel;
    cbEntryMode: TevDBComboBox;
    pmPayrollProduct: TevPopupMenu;
    mnPayrollProduct: TMenuItem;
    evLabel72: TevLabel;
    cbACAAnnualForm: TevDBComboBox;
    rgSelfInsured: TevDBRadioGroup;
    evLabel73: TevLabel;
    edtAdminPeriod: TevDBSpinEdit;
    evLabel74: TevLabel;
    lcbACAGroup: TevDBLookupCombo;
    pmAleGroup: TevPopupMenu;
    mnAleGroup: TMenuItem;
    fpSelfServe: TisUIFashionPanel;
    evLabel64: TevLabel;
    evLabel65: TevLabel;
    evLabel66: TevLabel;
    evLabel68: TevLabel;
    evddSelfServePortal: TevDBRadioGroup;
    evcbEEInfo: TevDBComboBox;
    evcbTimeOff: TevDBComboBox;
    evcbBenefit: TevDBComboBox;
    evcbDirectDeposit: TevDBComboBox;
    fpNetwork: TisUIFashionPanel;
    lablNetwork_Administrator: TevLabel;
    lablNetwork: TevLabel;
    lablNetwork_Admin_Phone_Type: TevLabel;
    lablNetwork_Administrator_Phone: TevLabel;
    dedtNetwork_Administrator: TevDBEdit;
    wwdcNetwork: TevDBComboBox;
    wwcbNetwork_Admin_Phone_Type: TevDBComboBox;
    wwdeNetwork_Administrator_Phone: TevDBEdit;
    evDBRadioGroup4: TevDBRadioGroup;
    fpHardware: TisUIFashionPanel;
    lablHardware_O_S: TevLabel;
    lablModem_Speed: TevLabel;
    lablModem_Connection_Type: TevLabel;
    lablLast_Call_In_Date: TevLabel;
    wwdcHardware_O_S: TevDBComboBox;
    wwcbModem_Speed: TevDBComboBox;
    wwcbModem_Connection_Type: TevDBComboBox;
    drgpRemote: TevDBRadioGroup;
    wwdpLast_Call_In_Date: TevDBDateTimePicker;
    fpPayrollProduct: TisUIFashionPanel;
    evLabel71: TevLabel;
    cbPayrollProduct: TevDBComboBox;
    evLabel75: TevLabel;
    evDBEdit16: TevDBEdit;
    isUIFashionPanel2: TisUIFashionPanel;
    cbCoreHR: TevDBCheckBox;
    cbApplicantTracking: TevDBCheckBox;
    cbTalentManagement: TevDBCheckBox;
    cbOnlineBenefitEnrollment: TevDBCheckBox;
    evcbHR: TevDBComboBox;
    evLabel67: TevLabel;
    lblALETransition: TevLabel;
    cbALETransition: TevDBComboBox;
    cbSetup_Completed: TevDBComboBox;
    lblSetupCompleted: TevLabel;
    btnACAAffordability: TevBitBtn;
    lblACANotes: TevLabel;
    edACANotes: TEvDBMemo;
    edShowEINOnCheck: TevDBRadioGroup;
    edtEINState: TevDBLookupCombo;
    evLabel76: TevLabel;
    pmACAService: TevPopupMenu;
    mnACAService: TMenuItem;
    rgEnforgerDOB: TevDBRadioGroup;
    pmEnforgeDOB: TevPopupMenu;
    mnEnforgeDOB: TMenuItem;
    rgShowTipCredit: TevDBRadioGroup;
    evLabel78: TevLabel;
    cbOfferCodes: TevDBLookupCombo;
    evLabel77: TevLabel;
    cbACASafeHarbor: TevDBComboBox;
    evLabel79: TevLabel;
    cbHealthCareStart: TevDBComboBox;
    evLabel80: TevLabel;
    edHealthCareStartDay: TevDBSpinEdit;
    gbService: TevGroupBox;
    rgACAService: TevDBRadioGroup;
    evLabel81: TevLabel;
    edACAStartDate: TevDBDateTimePicker;
    edtACAEndDate: TevDBDateTimePicker;
    evLabel82: TevLabel;
    cbReliefCodes: TevDBLookupCombo;
    evLabel83: TevLabel;

    procedure Button1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure drgpTax_ServiceChange(Sender: TObject);
    procedure drgpTrust_ServiceChange(Sender: TObject);
    procedure wwcbCredit_HoldChange(Sender: TObject);
    procedure btnChangePrenoteClick(Sender: TObject);
    procedure wwDBLookupCombo2Change(Sender: TObject);
    procedure wwdsMasterStateChange(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure evBitBtn2Click(Sender: TObject);
    procedure dedtEIN_NumberKeyPress(Sender: TObject; var Key: Char);
    procedure wwcbFederal_Tax_Payment_MethodChange(Sender: TObject);
    procedure wwDBLookupCombo3CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcFUI_Tax_Payment_AngencyCloseUp(Sender: TObject;
      LookupTable, FillTable: TDataSet; modified: Boolean);
    procedure btnCopyEEsClick(Sender: TObject);
    procedure FTDFCopyToClick(Sender: TObject);
    procedure Fed945TDFCopyToClick(Sender: TObject);
    procedure FUITDFCopyToClick(Sender: TObject);
    procedure wwcbStatus_CodeChange(Sender: TObject);
    procedure btnNewHireSetupClick(Sender: TObject);
    procedure edControlNameChange(Sender: TObject);
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
    procedure wwcbPay_FrequenciesCloseUp(Sender: TwwDBComboBox;
      Select: Boolean);
    procedure ResultFIELDS_CL_BANK_Function;
    procedure CopySbEnlistGroupsToCoLevel;
    procedure Enable_ESSCopyToClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure miMiscCheckFormCopyToClick(Sender: TObject);
    procedure miCheckFormCopyToClick(Sender: TObject);
    procedure cbCheckFormCloseUp(Sender: TObject);
    procedure cbCheckFormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MenuItem7Click(Sender: TObject);
    procedure tsCashManagementEnter(Sender: TObject);
    procedure miCopyShowRateHoursClick(Sender: TObject);
    procedure edFiscalBeginDateClick(Sender: TObject);
    procedure dtFiscalBeginDateCloseUp(Sender: TObject);
    procedure Enable_TimeOffCopyToClick(Sender: TObject);
    procedure Enable_BenefitsCopyToClick(Sender: TObject);
    procedure Enable_HRCopyToClick(Sender: TObject);
    procedure wwcbDBDT_LevelChange(Sender: TObject);
    procedure AcaFromYearChange(Sender: TObject);
    procedure AcaFromMonthChange(Sender: TObject);
    procedure AcaToYearChange(Sender: TObject);
    procedure AcaToMonthChange(Sender: TObject);
    procedure AcaStabFromYearChange(Sender: TObject);
    procedure AcaStabFromMonthChange(Sender: TObject);
    procedure AcaStabToYearChange(Sender: TObject);
    procedure AcaStabToMonthChange(Sender: TObject);
    procedure wwdsMasterUpdateData(Sender: TObject);
    procedure CopyTo1Click(Sender: TObject);
    procedure Enable_DDCopyToClick(Sender: TObject);
    procedure Enable_EEAccesClick(Sender: TObject);
    procedure mnCOPrintVoucherClick(Sender: TObject);
    procedure mnAnnualTypeCopyToEEClick(Sender: TObject);
    procedure mnEEPrintVoucherClick(Sender: TObject);
    procedure btnAddCertClick(Sender: TObject);
    procedure btnRemoveCertClick(Sender: TObject);
    procedure mnPrintCPAClick(Sender: TObject);
    procedure mnEnableAnalyticsClick(Sender: TObject);
    procedure mnAnalyticsLevelClick(Sender: TObject);
    procedure mnBenefitsEligibleDefaultCOClick(Sender: TObject);
    procedure mnBenefitsEligibleDefaultEEClick(Sender: TObject);
    procedure rgIncludeAnalyticsChange(Sender: TObject);
    procedure cbAnalyticsLevelChange(Sender: TObject);
    procedure mnPayrollProductClick(Sender: TObject);
    procedure mnAleGroupClick(Sender: TObject);
    procedure evcbHRChange(Sender: TObject);
    procedure pmEnable_HRPopup(Sender: TObject);
    procedure btnACAAffordabilityClick(Sender: TObject);
    procedure edShowEINOnCheckChange(Sender: TObject);
    procedure mnACAServiceClick(Sender: TObject);
    procedure mnEnforgeDOBClick(Sender: TObject);
  private
    delCONbr: integer;
    AchUpd: Boolean;
    NewCoSetup: Boolean;
    SettingAca: Boolean;
    FrequecyHasChanged: Boolean;
    CheckFormCodeValues: TStrings;
    MiscCheckFormCodeValues: TStrings;

    cdACAOffer, cdACARelief: IEvDataSet;

    procedure CopyTaxDeposit( aTDFfieldName: string);
    procedure UpdateControls;
    procedure CopyToEE(const CoField, EEField:String);
    procedure UpdateRequired;
    procedure SetevcbHR;
  protected
    procedure CheckDefaultDataSet(var CheckCondition: string; var bCheck: Boolean); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    function GetDataSetConditions(sName: string): string; override;
    procedure OnGetSecurityStates(const States: TSecurityStateList); override;
    procedure SetReadOnly(Value: Boolean); override;
    function CashManagementBankIsMandatory: Boolean;
    procedure FillCheckForms;
    procedure ShowCheckForm(aDefault: boolean = False);
    procedure UpdateAca;
    procedure SetAca;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure LocateCo(ClNbr:integer;CoNbr:integer);
    procedure AfterClick(Kind: Integer); override;
    procedure DeActivate; override;
    procedure Activate; override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
  end;

implementation

uses
  SPD_EDIT_CO_SCHEDULER, wwIntl, SPD_CompanyChoiceQuery, SFieldCodeValues,
  SCopyWizard, SCompanyCopyWizard, SPD_EDIT_Open_BASE, NewHireReportsSetup,
  EvInitApp, SPD_SafeChoiceFromListQuery, AcaAffordabilityDlg;

{$R *.DFM}

const
  ctEnabledSRo = 'S';

procedure TEDIT_CO.Button1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO_SCHEDULER');
end;

procedure TEDIT_CO.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP, aDS);
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_CLIENT.CL_BILLING, aDS);
  AddDS(DM_CLIENT.CL_DELIVERY_GROUP, aDS);
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_CLIENT.CL_COMMON_PAYMASTER, aDS);
  AddDS(DM_CLIENT.CL_CO_CONSOLIDATION, aDS);
  AddDS(DM_CLIENT.CL_TIMECLOCK_IMPORTS, aDS);

  inherited;
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);
  AddDS(DM_COMPANY.CO_STATES, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_COMPANY.CO_SALESPERSON, aDS);
  AddDS(DM_COMPANY.CO_SALESPERSON_FLAT_AMT, aDS);
  AddDS(DM_COMPANY.CO_ACA_CERT_ELIGIBILITY, aDS);

end;

function TEDIT_CO.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO;
end;

function TEDIT_CO.GetInsertControl: TWinControl;
begin
  Result := dedtCompany_Code;
end;

//*****************************************************************************
procedure TEDIT_CO.RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                                var InsertDataSets, EditDataSets: TArrayDS;
                                var DeleteDataSet: TevClientDataSet;
                                var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_FEDERAL.SY_FED_REPORTING_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_FEDERAL.SY_FED_ACA_OFFER_CODES, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_BANKS, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_ACCOUNTANT, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_REFERRALS, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_USER, SupportDataSets, ' STRCOPY(CAST(ConvertNullString(USER_FUNCTIONS, '''') AS VARCHAR(512)), 0, 2) IN (''-1'', '''') AND ACTIVE_USER = ''Y'' ');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_OTHER_SERVICE, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_ACA_GROUP, SupportDataSets, '');
  AddDS(DM_COMPANY.CO_SALESPERSON, EditDataSets);
  AddDSWithCheck(DM_COMPANY.CO_SALESPERSON_FLAT_AMT, EditDataSets, '');
  AddDS(DM_COMPANY.CO_ACA_CERT_ELIGIBILITY, EditDataSets);
end;

procedure TEDIT_CO.AfterDataSetsReopen;
begin
  inherited;
  tsVMR.TabVisible := CheckVmrLicenseActive and CheckVmrSetupActive;
  AssignVMRClientLookups(tsVMR);
  wwcbTransmissionDestination.Items.Clear;

  if wwdsList.DataSet.Active then
    with TEvClientDataSet(wwdsList.DataSet).ShadowDataSet do
    begin
      SetRange([DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger], [DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger]);
      try
        First;
        while not EOF do
        begin
          wwcbTransmissionDestination.Items.Add(FieldByName('NAME').AsString + #9 + FieldByName('CO_NBR').AsString);
          Next;
        end;
      finally
        CancelRange;
      end;
    end;


  DM_COMPANY.CO.BUSINESS_START_DATE.Required := True;

  cdAvailableCertificationsEligibility.EmptyDataSet;
  if cdAvailableCertificationsEligibility.FieldDefs.Count = 0 then
     cdAvailableCertificationsEligibility.CreateDataSet;

  cdAvailableCertificationsEligibility.AppendRecord(['A','A.Qualifying Offer Method']);
  cdAvailableCertificationsEligibility.AppendRecord(['B','B.Qualifying Offer Method Transition Relief']);
  cdAvailableCertificationsEligibility.AppendRecord(['C','C.Section 4980H Transition Relief']);
  cdAvailableCertificationsEligibility.AppendRecord(['D','D.98% Offer Method']);

  cbALETransition.Visible := false;
  lblALETransition.Visible:= cbALETransition.Visible;

  DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.First;
  while not DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.Eof do
  begin
    if cdAvailableCertificationsEligibility.Locate('ACA_CERT_ELIGIBILITY_TYPE', DM_COMPANY.CO_ACA_CERT_ELIGIBILITY['ACA_CERT_ELIGIBILITY_TYPE'],[]) then
       cdAvailableCertificationsEligibility.delete;

    if DM_COMPANY.CO_ACA_CERT_ELIGIBILITY['ACA_CERT_ELIGIBILITY_TYPE'] = ACA_CERT_ELIGIBILITY_TYPE_C then
    begin
     cbALETransition.Visible := True;
     lblALETransition.Visible:= cbALETransition.Visible;
    end;

    DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.Next;
  end;
  cdAvailableCertificationsEligibility.First;
  DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.First;

  UpdateRequired;

  cbEntryMode.AllowClearKey:= false;

  if DM_COMPANY.CO.FieldByName('ENABLE_HR').AsString = GROUP_BOX_ADVANCED_HR then
     dedtCompany_Code.SecurityRO := True;

  delCONbr:=0;

  SetevcbHR;


  cdACAOffer:= ctx_RemoteMiscRoutines.GetACAOfferCodes(DM_COMPANY.CO.CO_NBR.AsInteger, Now);
  cdACARelief:= ctx_RemoteMiscRoutines.GetACAReliefCodes(DM_COMPANY.CO.CO_NBR.AsInteger, Now);

  cbOfferCodes.LookupTable := cdACAOffer.vclDataSet;
  cbReliefCodes.LookupTable := cdACARelief.vclDataSet;

end;

procedure TEDIT_CO.LocateCo(ClNbr, CoNbr: integer);
var
  Close:boolean;
  aDS: TArrayDS;
  I: Integer;
begin
  ctx_DataAccess.OpenClient(ClNbr);
  DM_TEMPORARY.TMP_CO.Close;
  DM_TEMPORARY.TMP_CO.Open;

  Close := false;
  GetDataSetsToReopen(aDS, Close);
  for i := Low(aDS) to High(aDS) do
  begin
     if Close or (aDS[i].ClientID = 0) or (aDS[i].ClientID >= 0)  and  (aDS[i].ClientID <> ClNbr)
       or (aDS[i].OpenCondition <> GetDataSetConditions(aDS[i].TableName)) then
     begin
       aDS[i].Close;
     end;
  end;
  ctx_DataAccess.OpenDataSets(aDS);
  DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR',VarArrayOf([ClNbr,CoNbr]) , []);
  DM_COMPANY.CO.Locate('CO_NBR',CONBR,[]);
end;


procedure TEDIT_CO.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  ClNumber: String;
  ClNbr: Integer;
  aDS: TArrayDS;
  CoNbr:integer;
  Close: Boolean;
  I: Integer;
  CompanyWizard: TCompanyCopyWizard;
  result:TModalResult;
begin
  edFiscalBeginDate.BringToFront;
  edFiscalBeginDate.TabStop := True;
  dtFiscalBeginDate.TabStop := False;
  if Kind = NavInsert then
  begin
    ClNumber := '';
    repeat
      if ClNumber <> '' then
      begin
        ctx_DataAccess.OpenDataSets([DM_TEMPORARY.TMP_CL]);
        ClNbr := ConvertNull(DM_TEMPORARY.TMP_CL.Lookup('CUSTOM_CLIENT_NUMBER', ClNumber, 'CL_NBR'), 0);
        if ClNbr = 0 then
          Continue;
        CheckStatus;
        with Owner as TFramePackageTmpl do
           if BitBtn1.Enabled and ButtonEnabled(NavCommit) then
              ClickButton(NavCommit);

        Close := False;
        GetDataSetsToReopen(aDS, Close);
        for i := Low(aDS) to High(aDS) do
        begin
           if Close or (aDS[i].ClientID = 0) or (aDS[i].ClientID >= 0)  and  (aDS[i].ClientID <> ClNbr)
             or (aDS[i].OpenCondition <> GetDataSetConditions(aDS[i].TableName)) then
           begin
             aDS[i].Close;
           end;
        end;
        DM_TEMPORARY.TMP_CL.Locate('CUSTOM_CLIENT_NUMBER', ClNumber, []);
        GetInsertControl.Show;
        SetDataSetsProps(aDS, DM_TEMPORARY.TMP_CL);
        ctx_DataAccess.OpenClient(ClNbr);
        ctx_DataAccess.OpenDataSets(aDS);
        wwdsList.DoDataChange(Self, nil);
        AfterDataSetsReopen;
      end;

      if dsCL.DataSet.FieldByName('NAME').AsString <> '' then
        if (EvMessage('Create new company for ' + dsCL.DataSet.FieldByName('NAME').AsString + '?',
          mtConfirmation, [mbYes, mbNo]) = mrYes) then
        begin
          if (EvMessage('Create new company based on an existing company?',
            mtConfirmation, [mbYes, mbNo]) = mrYes) then
          begin
            CompanyWizard := TCompanyCopyWizard.CreateTCopyWizard(Nil, evCompany);
            srcCoStates.MasterDataSource := nil;
            wwdsMaster.DataSet := nil;
            try
              CoNbr := DM_COMPANY.CO.CO_NBR.AsInteger;
              ClNbr := dsCL.DataSet['CL_NBR'];
              CompanyWizard.Caption := CompanyWizard.Caption + 'Company';
              CompanyWizard.CLList.CLBase := dsCL.DataSet['CL_NBR'];
              CompanyWizard.CLList.CLNew := dsCL.DataSet['CL_NBR'];
              CompanyWizard.CLNumber := dsCL.DataSet['CUSTOM_CLIENT_NUMBER'];
              result := CompanyWizard.ShowModal;
              if result = mrOk then
              begin
                ClNbr := CompanyWizard.CLList.CLNew;
                CoNbr := DM_COMPANY.CO.CO_NBR.Value;
              end;
            finally
              srcCoStates.MasterDataSource := wwdsMaster;
              wwdsMaster.DataSet := DM_COMPANY.CO;
              CompanyWizard.Free;
            end;
            Handled := True;
            LocateCo(CLNBr,CoNbr);
            if result = mrCancel then
               PageControl1.ActivatePage(TabSheet1)
            else
            begin
              GetInsertControl.Show;
              SetDataSetsProps(aDS, DM_TEMPORARY.TMP_CL);
              wwdsList.DoDataChange(Self, nil);
              AfterDataSetsReopen;
            end;
            Exit;
          end

          else
          begin
            inherited;
            cbOfferCodes.DisplayValue:= '';
            cbReliefCodes.DisplayValue:= '';            
            (Owner as TFramePackageTmpl).SetButton(NavInsert, True);
            (Owner as TFramePackageTmpl).SetButton(NavDelete, True);
            ShowCheckForm(True);
          end;

          Exit;
        end;
    until not EvDialog('Input Required', 'Enter a Client Number to create a new company for:', ClNumber);
    PageControl1.ActivatePage(TabSheet1);
    AbortEx;
  end
  else if Kind = NavOK then
  begin
    if  TDataSet(DM_CLIENT.CO).State in [dsInsert,dsEdit] then
    begin
      UpdateAca;
      DM_CLIENT.CO.UpdateRecord;
    end;

    if (evcbHR.Value = GROUP_BOX_ADVANCED_HR) and
       (Length(Trim(DM_CLIENT.CO.FieldByName('HCM_URL').AsString)) = 0) then
      raise EUpdateError.CreateHelp('The HCM URL is required when Advanced HR is selected.', IDH_ConsistencyViolation);

    if DM_CLIENT.CO.FieldByName('TAX_SERVICE').OldValue <>  DM_CLIENT.CO.FieldByName('TAX_SERVICE').NewValue then
      if (drgpTax_Service.Value = TAX_SERVICE_FULL) and
          DM_CLIENT.CO.FieldByName('TAX_SERVICE_START_DATE').IsNull then
        raise EUpdateError.CreateHelp('Tax Start Date cannot be empty.', IDH_ConsistencyViolation);

    if (edShowEINOnCheck.ItemIndex = 1) and (Trim(edtEINState.Value) = '')  then
        raise EUpdateError.CreateHelp('EIN State cannot be empty.', IDH_ConsistencyViolation);

    with wwdsMaster.DataSet do
    begin
      if DM_CLIENT.CO.ACA_ADMIN_PERIOD.AsInteger > 365 then
        raise EUpdateError.CreateHelp('ACA Admin Period can not be bigger than 365.', IDH_ConsistencyViolation);

      if (FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').IsNull) or  (FieldByName('SY_FED_REPORTING_AGENCY_NBR').IsNull) or
         (FieldByName('FUI_TAX_DEPOSIT_FREQUENCY').IsNull) or (FieldByName('SY_FED_TAX_PAYMENT_AGENCY_NBR').IsNull) or
         (FieldByName('FUI_SY_TAX_PAYMENT_AGENCY').IsNull) or (FieldByName('FUI_SY_TAX_REPORT_AGENCY').IsNull) then
      begin
        raise EUpdateError.CreateHelp('One or more of required fields on the Tax Reporting tab is empty. Data can not be committed!', IDH_ConsistencyViolation);
      end;

      FieldByName('FILLER').AsString := PutIntoFiller(FieldByName('FILLER').AsString, edControlName.Text, 61, 4);

      if cbCheckForm.ItemIndex >= 0 then
      begin
        if Length(CheckFormCodeValues[ cbCheckForm.ItemIndex ]) > 1 then
        begin
          // Put SB Check Number into Filler since Check_Form field has 1 char only
          FieldByName('CHECK_FORM').AsString := CheckFormCodeValues[ cbCheckForm.ItemIndex ][1];
          FieldByName('FILLER').AsString := PutIntoFiller(FieldByName('FILLER').AsString, CheckFormCodeValues[ cbCheckForm.ItemIndex ], 65, 6);
        end
        else
          FieldByName('CHECK_FORM').AsString := CheckFormCodeValues[ cbCheckForm.ItemIndex ];
      end
      else
      begin
        PageControl1.ActivePage := tshtChecks;
        cbCheckForm.SetFocus;
        raise EUpdateError.CreateHelp('"Check Form" field is empty. Data can not be committed!', IDH_ConsistencyViolation);
      end;

      if Length(MiscCheckFormCodeValues[ cbMiscCheckForm.ItemIndex ]) > 1 then
      begin
        // Put SB Misc Check Number into Filler since Check_Form field has 1 char only
        FieldByName('MISC_CHECK_FORM').AsString := MiscCheckFormCodeValues[ cbMiscCheckForm.ItemIndex ][1];
        FieldByName('FILLER').AsString := PutIntoFiller(FieldByName('FILLER').AsString, MiscCheckFormCodeValues[ cbMiscCheckForm.ItemIndex ], 71, 6);
      end
      else
        FieldByName('MISC_CHECK_FORM').AsString := MiscCheckFormCodeValues[ cbMiscCheckForm.ItemIndex ];
    end;

    if DM_CLIENT.CO.TAX_SB_BANK_ACCOUNT_NBR.IsNull and ((wwcbFederal_Tax_Payment_Method.Value = TAX_PAYMENT_METHOD_DEBIT) or (wwcbFederal_Tax_Payment_Method.Value = TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT )) then
        raise EUpdateError.CreateHelp('You need a SB Tax Bank Account for EFT Debit Tax Payments.', IDH_ConsistencyViolation);

    if not ((wwcbFederal_Tax_Payment_Method.Value = TAX_PAYMENT_METHOD_DEBIT) or (wwcbFederal_Tax_Payment_Method.Value = TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT)) then
    begin
      if not DM_CLIENT.CO.TAX_SB_BANK_ACCOUNT_NBR.IsNull and
        ((drgpTax_Service.Value = '') or (drgpTax_Service.Value = TAX_SERVICE_NONE)) then
      begin
        raise EUpdateError.CreateHelp('Tax Service must be Full or Direct when SB Tax Bank Account is not empty.', IDH_ConsistencyViolation);
      end;
    end;
    if(DM_CLIENT.CO.CALCULATE_LOCALS_FIRST.Value='Y') then
    begin
       DM_CLIENT.CO_LOCAL_TAX.Active := True;
       DM_CLIENT.CO_LOCAL_TAX.First;
       while not DM_CLIENT.CO_LOCAL_TAX.Eof do
       begin
         if(DM_SYSTEM_STATE.SY_LOCALS.Lookup(
                     'SY_LOCALS_NBR',
                     DM_CLIENT.CO_LOCAL_TAX.SY_LOCALS_NBR.Value,
                     'CALCULATION_METHOD') = CALC_METHOD_PERC_MEDICARE_WAGES ) then
         begin
            raise EUpdateError.CreateHelp('Calculating local taxes first is not possible since this company has local taxes that are calculated based on medicare wages.', IDH_ConsistencyViolation);
         end;
         DM_CLIENT.CO_LOCAL_TAX.Next;
       end;
    end;
    if( DM_CLIENT.CO.INVOICE_DISCOUNT.Value <-100) or (DM_CLIENT.CO.INVOICE_DISCOUNT.Value > 100) then
       raise EUpdateError.CreateHelp('Invoice discount value must be between -100 and 100!', IDH_ConsistencyViolation);
    if drgpDBA_On_Tax_Return.Value = 'L' then
      if (dedtLegal_Name.Text = '' ) or
         ((dedtLegal_Address_1.Text = '') and (dedtLegal_Address_2.Text = '')) then
      begin
        PageControl1.ActivatePage(tshtAddress);
        if dedtLegal_Address_1.CanFocus then
          dedtLegal_Address_1.SetFocus;
        raise EUpdateError.CreateHelp('You have chosen Legal for Name to Use on Tax Returns. Please enter Legal Address.', IDH_ConsistencyViolation);
      end;

    if drgpDBA_On_Tax_Return.Value = 'D' then
      if (dedtDBA.Text = '') then
      begin
        PageControl1.ActivatePage(tshtAddress);
        if dedtDBA.CanFocus then
          dedtDBA.SetFocus;
        raise EUpdateError.CreateHelp('You have chosen DBA for Name to Use on Tax Returns. Please fill DBA field.', IDH_ConsistencyViolation);
      end;

    Inherited;
  end
  else
    inherited;

  if Kind = NavDelete then
    delCONbr := DM_CLIENT.CO.FieldByName('CO_NBR').AsInteger;

end;

function TEDIT_CO.GetDataSetConditions(sName: string): string;
begin
  if sName = 'CO_SALESPERSON_FLAT_AMT' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO.PageControl1Change(Sender: TObject);
var
  dateNow : TDate;
begin
  inherited;
  dateNow := trunc(now);
  if PageControl1.ActivePage = TabSheet1 then
    TabSheet1.Enabled := TevClientDataSet(DM_COMPANY.CO).State <> dsInsert;

  if (PageControl1.ActivePage = tsCashManagement) or
     (PageControl1.ActivePage = tshtGeneral_Business) then
  begin
      with HorzScrollBar do
      begin
        Range := 935;
        Position := 0;
        Visible := True;
      end;
      with VertScrollBar do
      begin
        Range := 654;
        Position := 0;
        Visible := True;
      end;

      DM_CLIENT.CL_CO_CONSOLIDATION.Filter:= 'CONSOLIDATION_TYPE <> ''C''';
      DM_CLIENT.CL_CO_CONSOLIDATION.Filtered := true;
  end
  else
  begin
      with HorzScrollBar do
      begin
        Range := 0;
        Position := 0;
        Visible := True;
      end;
      with VertScrollBar do
      begin
        Range := 0;
        Position := 0;
        Visible := True;
      end;
  end;

  if PageControl1.ActivePage = tshtWComp_And_Benefits then
  begin
     edFiscalBeginDate.BringToFront;
     edFiscalBeginDate.TabStop := True;
     dtFiscalBeginDate.TabStop := False;
  end;
  //this is a non-databound control that does not follow standard protocol
  //to become disabled when SecurityRO is turned on.
  if PageControl1.ActivePage = tshtWComp_And_Benefits then
  begin
    edFiscalBeginDate.Enabled := ctx_DataAccess.AsOfDate = dateNow;
    if edFiscalBeginDate.Enabled then
      edFiscalBeginDate.Color := clWindow
    else
      edFiscalBeginDate.Color := clBtnFace;
  end;


  if PageControl1.ActivePage = tshtAca then
  begin
    DM_CLIENT.CL_CO_CONSOLIDATION.Filter:= 'CONSOLIDATION_TYPE = ''C''';
    DM_CLIENT.CL_CO_CONSOLIDATION.Filtered := true;
  end;

  ResultFIELDS_CL_BANK_Function;
end;

//*****************************************************************************
procedure TEDIT_CO.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = navInsert then
  begin
    DM_SERVICE_BUREAU.SB.DataRequired('');
    DM_COMPANY.CO.TAX_IMPOUND.Value := DM_SERVICE_BUREAU.SB.TAX_IMPOUND.Value;
    DM_COMPANY.CO.TRUST_IMPOUND.Value := DM_SERVICE_BUREAU.SB.TRUST_IMPOUND.Value;
    DM_COMPANY.CO.BILLING_IMPOUND.Value := DM_SERVICE_BUREAU.SB.BILLING_IMPOUND.Value;
    DM_COMPANY.CO.DD_IMPOUND.Value := DM_SERVICE_BUREAU.SB.DD_IMPOUND.Value;
    DM_COMPANY.CO.WC_IMPOUND.Value := DM_SERVICE_BUREAU.SB.WC_IMPOUND.Value;

    NewCoSetup := True;

  end;
  if Kind = NavCommit then
  try
    if NewCoSetup then
    begin
      CopySbEnlistGroupsToCoLevel;
      NewCoSetup := False;
    end;

    DM_TEMPORARY.TMP_CO.DisableControls;
    DM_TEMPORARY.TMP_CO.Close;
    ctx_DataAccess.OpenDataSets([DM_TEMPORARY.TMP_CO]);
    DM_TEMPORARY.TMP_CO.Locate('CL_NBR; CO_NBR', VarArrayOf([DM_COMPANY.CO.ClientID, DM_COMPANY.CO['CO_NBR']]), []);
    UpdateControls;
  finally
    DM_TEMPORARY.TMP_CO.EnableControls;
    if FrequecyHasChanged then
    begin
      FrequecyHasChanged := False;
      EvMessage('You have changed your company pay frequency, you MUST UPDATE/FIX your company CALENDAR!  You will be redirected to the company calendar screen!');
      DM_COMPANY.CO.DataRequired('CO_NBR = ' + DM_COMPANY.CO.fieldByName('CO_NBR').AsString);
      PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Company - General - Calendar' + #0)), 0);
    end;

    updateSbENABLE_ANALYTICS(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger, DM_COMPANY.CO.FieldByName('ENABLE_ANALYTICS').AsString);
  end;
  if (Kind = NavDelete)  and  (delCONbr > 0) then
  begin
    updateSbENABLE_ANALYTICS(delCONbr, 'N');
    delCONbr:=0;
  end;

  ResultFIELDS_CL_BANK_Function;

  if Kind in [NavInsert, NavCancel, NavCommit, NavAbort] then
     ApplySecurity;
end;

procedure TEDIT_CO.UpdateControls;
begin
  if drgpTax_Service.Value = TAX_SERVICE_FULL then
  begin
    TaxCombobox.Enabled := True;
    evDBComboBox14.Enabled := True;
  end
  else
  begin
    if TaxCombobox.Value <> PAYMENT_TYPE_NONE then
      TaxCombobox.Value := PAYMENT_TYPE_NONE;
      
    TaxCombobox.Enabled := False;

    if evDBComboBox14.Value <> PAYMENT_TYPE_NONE then
      evDBComboBox14.Value := PAYMENT_TYPE_NONE;

    evDBComboBox14.Enabled := False;

  end;
end;

procedure TEDIT_CO.UpdateRequired;
Begin
    DM_CLIENT.CO.TAX_CL_BANK_ACCOUNT_NBR.Required := drgpTax_Service.Value = TAX_SERVICE_FULL;
    DM_CLIENT.CO.TAX_SB_BANK_ACCOUNT_NBR.Required := drgpTax_Service.Value = TAX_SERVICE_FULL;
    CashManagementBankIsMandatory;
    UpdateControls;
End;

procedure TEDIT_CO.drgpTax_ServiceChange(Sender: TObject);
begin
  inherited;
  if Assigned(wwdsMaster.DataSet) and ( wwdsMaster.DataSet.State in [dsInsert, dsEdit] ) then
     UpdateRequired;
end;

procedure TEDIT_CO.drgpTrust_ServiceChange(Sender: TObject);
begin
  inherited;
  //Issue#75274
  if Assigned(wwdsMaster.DataSet) and
     ( wwdsMaster.DataSet.State in [dsInsert, dsEdit] ) then
  begin
    DM_CLIENT.CO.PAYROLL_CL_BANK_ACCOUNT_NBR.Required := drgpTrust_Service.Value = TRUST_SERVICE_ALL;
    DM_CLIENT.CO.TRUST_SB_ACCOUNT_NBR.Required := drgpTrust_Service.Value = TRUST_SERVICE_ALL;
    CashManagementBankIsMandatory;

    if drgpTrust_Service.Value = TRUST_SERVICE_NO then
    begin
      TrustCombobox.Value := PAYMENT_TYPE_NONE;
      TrustCombobox.Enabled := False;
    end
    else
    begin
      TrustCombobox.Enabled := True;
    end;
  end;
end;

procedure TEDIT_CO.wwcbCredit_HoldChange(Sender: TObject);
begin
  inherited;
  if (TevClientDataSet(DM_COMPANY.CO).State in [dsInsert, dsEdit]) and (wwcbCredit_Hold.Value <> '') then
    case wwcbCredit_Hold.Value[1] of
    CREDIT_HOLD_LEVEL_LOW:
      if EvMessage('This change will result in getting a warning when accessing this company. Would you like to make it?', mtWarning, [mbYes, mbNo]) = mrNo then
        wwcbCredit_Hold.Value := DM_COMPANY.CO.FieldByName('CREDIT_HOLD').Value;
    CREDIT_HOLD_LEVEL_MEDIUM:
      if EvMessage('This change will result in getting a warning when accessing this company and preventing this company''s payrolls from processing. Would you like to make it?', mtWarning, [mbYes, mbNo]) = mrNo then
        wwcbCredit_Hold.Value := DM_COMPANY.CO.FieldByName('CREDIT_HOLD').Value;
    CREDIT_HOLD_LEVEL_HIGH:
      if EvMessage('This change will result in complete denial of access to this company. You will need to have rights to change it back. Would you like to make this change?', mtWarning, [mbYes, mbNo]) = mrNo then
        wwcbCredit_Hold.Value := DM_COMPANY.CO.FieldByName('CREDIT_HOLD').Value;
    CREDIT_HOLD_LEVEL_MAINT:
      if EvMessage('This change will result in complete denial of access to this company. You will need to have rights to change it back. Would you like to make this change?', mtWarning, [mbYes, mbNo]) = mrNo then
        wwcbCredit_Hold.Value := DM_COMPANY.CO.FieldByName('CREDIT_HOLD').Value;
    end;
end;

procedure TEDIT_CO.btnChangePrenoteClick(Sender: TObject);
var
  s: string;
begin
  inherited;
  case EvMessage('Change prenote to', mtConfirmation, mbYesNoCancel) of
  mrYes: s := GROUP_BOX_YES;
  mrNo: s := GROUP_BOX_NO;
  else
    Exit;
  end;
  ctx_StartWait;
  try
    DM_EMPLOYEE.EE_DIRECT_DEPOSIT.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
    DM_EMPLOYEE.EE.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
    ctx_DataAccess.OpenDataSets([DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_DIRECT_DEPOSIT]);
    with DM_EMPLOYEE.EE_DIRECT_DEPOSIT do
    begin
      First;
      while not Eof do
      begin
        if DM_EMPLOYEE.EE.Lookup('EE_NBR', FieldByName('EE_NBR').Value, 'CO_NBR') = DM_COMPANY.CO['CO_NBR'] then
        begin
          Edit;
          FieldByName('IN_PRENOTE').AsString := s;
          Post;
        end;
        Next;
      end;
    end;
    ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_DIRECT_DEPOSIT]);
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_CO.wwDBLookupCombo2Change(Sender: TObject);
begin
  inherited;
  ;
end;

procedure TEDIT_CO.Deactivate;
begin
  inherited;
  DM_CLIENT.CL_CO_CONSOLIDATION.Filter :=  '';
  DM_CLIENT.CL_CO_CONSOLIDATION.Filtered := False;
end;

procedure TEDIT_CO.SetevcbHR;
begin
  evcbHR.Items.Text := EnableCompanyFeatures_ComboChoicesAll;
  if Context.License.Benefits then
      evcbHR.Items.Text :=  evcbHR.Items.Text + EnableCompanyFeatures_ComboChoicesBenefit;
  if Context.License.HR then
      evcbHR.Items.Text :=  evcbHR.Items.Text + EnableCompanyFeatures_ComboChoicesHR;
  evcbHR.ApplyList;
end;

//*****************************************************************************
procedure TEDIT_CO.Activate;
var
  enable_Analytics: Boolean;
begin
  inherited;

  AchUpd := False;
  wwlcFUI_Tax_Report_Agency.LookupTable := CreateLookupDataset(DM_SYSTEM_FEDERAL.SY_FED_REPORTING_AGENCY, Self);
  wwDBLookupCombo2.LookupTable := CreateLookupDataset(DM_SYSTEM_FEDERAL.SY_FED_REPORTING_AGENCY, Self);
  cdsSbBankAccountsForAch.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForTrust.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForTax.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForBilling.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForOBC.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForWC.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  lablLockDate.Enabled := ctx_AccountRights.Functions.GetState('USER_CAN_CHANGE_QUARTER_LOCK_DATE') = stEnabled;
  dtpcLockDate.Enabled := lablLockDate.Enabled;
  PageControl1.ActivePageIndex := 0;
  tsVMR.TabVisible := tsVMR.TabVisible and CheckVmrLicenseActive;

  tsCashManagement.TabVisible := (ctx_AccountRights.Functions.GetState('USER_CAN_EDIT_CO_BANK_ACCOUNTS') = stEnabled);

  wwdeZip_Code.Picture.PictureMask := '{&{#& #&#},{*5{#}[-*4#]}}';
  wwdeLegal_Zip_Code.Picture.PictureMask := '{&{#& #&#},{*5{#}[-*4#]}}';

  SetevcbHR;

  evcbEEInfo.enabled := Context.License.SelfServe;
  evcbBenefit.Enabled := Context.License.SelfServe;
  evcbTimeOff.Enabled := Context.License.SelfServe;
  evcbDirectDeposit.Enabled := Context.License.SelfServe;

  // DC-386 - changed to check for 'Y'
  enable_Analytics := DM_SERVICE_BUREAU.SB.ANALYTICS_LICENSE.Value = 'Y';
  rgIncludeAnalytics.Enabled := enable_Analytics;
  cbAnalyticsLevel.Enabled := enable_Analytics;
  evLabel69.Enabled := enable_Analytics;

  // Fill check form and misc check form dropdown list with checks from SB level
  FillCheckForms;

  edtEINState.Enabled := false;
  if edShowEINOnCheck.ItemIndex = 1 then
     edtEINState.Enabled := True;
end;

procedure TEDIT_CO.SetReadOnly(Value: Boolean);
begin
  inherited;
  if SecurityState = ctEnabledSRo then
    dedtCompany_Code.SecurityRO := True;

  if DM_COMPANY.CO.FieldByName('ENABLE_HR').AsString = GROUP_BOX_ADVANCED_HR then
     dedtCompany_Code.SecurityRO := True;
end;

procedure TEDIT_CO.wwdsMasterStateChange(Sender: TObject);
begin
  inherited;
  CashManagementBankIsMandatory;
end;

procedure TEDIT_CO.evBitBtn1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO_PHONE')
end;

procedure TEDIT_CO.evBitBtn2Click(Sender: TObject);
var
  fieldsToTransfer: string;
begin
  if dedtName.SecurityRO then
  begin
    wwdsMasterStateChange(nil);
    fieldsToTransfer := 'ADDRESS1;ADDRESS2;CITY;STATE;ZIP_CODE';
  end
  else
    fieldsToTransfer := 'NAME;ADDRESS1;ADDRESS2;CITY;STATE;ZIP_CODE';
  if not (TevClientDataSet(DM_COMPANY.CO).State in [dsEdit,dsInsert]) then
    DM_COMPANY.CO.Edit;
  DM_COMPANY.CO[fieldsToTransfer] := DM_CLIENT.CL[fieldsToTransfer];
end;

procedure TEDIT_CO.dedtEIN_NumberKeyPress(Sender: TObject; var Key: Char);
begin
// Ugly code below fixes following problem: bounded edit allows
// enter characters that are not allowed by mask even if UsePictureMask=true
// if you know how to solve this problem please remove this code
  if not (integer(Key) in [integer('0')..integer('9'), VK_ESCAPE, VK_UP, VK_DOWN, VK_NEXT, VK_PRIOR,
   VK_LEFT, VK_RIGHT, VK_HOME, VK_END, VK_INSERT, VK_DELETE, VK_F2, VK_BACK ] ) then
  begin
    Key := #0;
    MessageBeep(0);
  end;
end;

procedure TEDIT_CO.CopyTaxDeposit( aTDFfieldName: string);
const arry_desc :array[0..22, 0..1] of string =
(('FEDERAL_TAX_DEPOSIT_FREQUENCY',   'Federal Tax Deposit Frequency'),
 ('FED_945_TAX_DEPOSIT_FREQUENCY',   'Federal 945 Tax Deposit Frequency'),
 ('FUI_TAX_DEPOSIT_FREQUENCY',       'FUI Tax Deposit Frequency'),
 ('ENABLE_ESS',                      'Enable ESS'),
 ('TAX_IMPOUND',                     'Tax Impound Type'),
 ('TRUST_IMPOUND',                   'Trust Impound Type'),
 ('DD_IMPOUND',                      'Direct Deposit Impound Type'),
 ('BILLING_IMPOUND',                 'Billing Impound Type'),
 ('WC_IMPOUND',                      'Workers Comp Impound Type'),
 ('CO_PAYROLL_PROCESS_LIMITATIONS',  'Payroll Process Limitation'),
 ('MISC_CHECK_FORM',                 'MISC Check Form'),
 ('CHECK_FORM',                      'Check Form'),
 ('SALES_TAX_PERCENTAGE',            '% Sales Tax'),
 ('SHOW_EIN_NUMBER_ON_CHECK',        'Show Rate / Hour on Check'),
 ('ANNUAL_FORM_TYPE',                'Annual Form Type'),
 ('PRINT_CPA',                       'Print on Reports'),
 ('ENABLE_ANALYTICS',                'Include in Analytics'),
 ('ANALYTICS_LICENSE',               'Analytics Level'),
 ('PAYROLL_PRODUCT',                 'Payroll Product'),
 ('SB_ACA_GROUP_NBR',                'ALE Group'),
 ('ENFORCE_EE_DOB',                  'Enforce DOB on W2 Employees'),
 ('BENEFITS_ELIGIBLE_DEFAULT',       'Benefits Eligible Default'),
 ('ACA_SERVICES',                    'ACA Services'));

var
  CurClient: integer;
  CurCompany: integer;

  ClientRetrieveCondition: string;
  CompanyRetrieveCondition, sdesc: string;

  function GetDesc : string;
  var i : Integer;
  begin
    result := aTDFfieldName;
    for i := low(arry_desc) to high(arry_desc) do
      if aTDFfieldName = arry_desc[i,0] then
      begin
        result := arry_desc[i,1];
        break;
      end;
  end;

  procedure DoCopyTaxDeposit;
  var
    TDFValue: Variant;
    k: integer;
    sSBCheckNbr: string;
    extValue: variant;
    extField: String;

    procedure DoCopyTaxDepositTo( client, company: integer; const effDate: TDateTime; const bEffectiveDate: Boolean);
    begin
      ctx_DataAccess.OpenClient( client );
      DM_COMPANY.CO.DataRequired('CO_NBR=' + intToStr(company) );

      if bEffectiveDate then
      begin
        ctx_DBAccess.StartTransaction([dbtClient]);
        try
          ctx_DBAccess.UpdateFieldValue('CO', aTDFfieldName, DM_COMPANY.CO.CO_NBR.Value, effDate, DayBeforeEndOfTime, TDFValue);
          ctx_DBAccess.CommitTransaction;
          DM_COMPANY.CO.RefreshFieldValue(aTDFfieldName);
        except
          on E: EInvalidRecord do
          begin
            E.Message := E.Message +  #13'Company: ' + DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER'] + ''#13#13;
            raise
          end
          else
          begin
            ctx_DBAccess.RollbackTransaction;
            raise;
          end;
        end;
      end
      else
      begin
        try
          DM_COMPANY.CO.Edit;
          DM_COMPANY.CO[aTDFfieldName] := TDFValue;
          if aTDFfieldName = 'CHECK_FORM' then
            DM_COMPANY.CO.FieldByName('FILLER').AsString := PutIntoFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, sSBCheckNbr, 65, 6);
          if aTDFfieldName = 'MISC_CHECK_FORM' then
            DM_COMPANY.CO.FieldByName('FILLER').AsString := PutIntoFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, sSBCheckNbr, 71, 6);

          if extField <> '' then
            DM_COMPANY.CO.FieldByName(extField).AsDateTime := extValue;

         DM_COMPANY.CO.Post;
        except
          on E: EInvalidRecord do
          begin
            E.Message := E.Message +  #13'Company: ' + DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER'] + ''#13#13;
            raise
          end
          else
            raise;
        end;

        ctx_DataAccess.PostDataSets([DM_COMPANY.CO]);

      end;
    end;
  begin

    if aTDFfieldName = 'SALES_TAX_PERCENTAGE' then
      if EvMessage('The selected % Sales Tax will be copied. If any % Sales Tax already exists on the destination company(s), they will be overridden. Do you want to continue with copying the selected % Sales Tax?', mtConfirmation, [mbYes, mbNo]) = mrNo then
         Exit;

    with TCompanyChoiceQuery.Create( Self ) do
    try
      SetFilter( 'CUSTOM_COMPANY_NUMBER<>'''+DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'''' );
      sdesc := Getdesc;
      Caption :=format('Select the companies you want this %s copied to',[sdesc]);
      ConfirmAllMessage := format('This operation will copy the %s to all clients and companies. Are you sure you want to do this?',[sdesc]);

      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      if (aTDFfieldName = 'PRINT_CPA') or (aTDFfieldName = 'PAYROLL_PRODUCT') then
        pnlEffectiveDate.Visible := true;

      extField:= '';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        DM_COMPANY.CO_SERVICES.DisableControls;
        try
          TDFValue := DM_COMPANY.CO[aTDFfieldName];
          if aTDFfieldName = 'CHECK_FORM' then
            sSBCheckNbr := ExtractFromFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, 65, 6);
          if aTDFfieldName = 'MISC_CHECK_FORM' then
            sSBCheckNbr := ExtractFromFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, 71, 6);

          if aTDFfieldName = 'ACA_SERVICES' then
          begin
            extField:= 'ACA_SERVICES_END_DATE';
            if DM_COMPANY.CO.FieldByName('ACA_SERVICES').AsString = GROUP_BOX_YES then
               extField:= 'ACA_SERVICES_START_DATE';
            extValue:= DM_COMPANY.CO.FieldByName(extField).AsVariant
          end;

          DM_TEMPORARY.TMP_CO.DisableControls;
          try
            for k := 0 to dgChoiceList.SelectedList.Count - 1 do
            begin
              cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
              DoCopyTaxDepositTo(cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger, dtBeginEffective.Date, pnlEffectiveDate.Visible);
            end;
          finally
            DM_TEMPORARY.TMP_CO.EnableControls;
          end;
        finally
          DM_COMPANY.CO_SERVICES.EnableControls;
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  DM_COMPANY.CO.Cancel;

  CurClient := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  ClientRetrieveCondition := DM_CLIENT.CL.RetrieveCondition;

  CurCompany := DM_COMPANY.CO['CO_NBR'];
  CompanyRetrieveCondition := DM_COMPANY.CO.RetrieveCondition;
  DM_COMPANY.CO.DisableControls;

  try
    DoCopyTaxDeposit;
  finally
    ctx_DataAccess.OpenClient( CurClient );

    DM_CLIENT.CL.DataRequired(ClientRetrieveCondition );
    DM_COMPANY.CO.DataRequired(CompanyRetrieveCondition );

    wwdsList.dataset.Locate('CO_NBR;CL_NBR', VarArrayOf([CurCompany, CurClient ]), []);
    DM_COMPANY.CO.Locate('CO_NBR',CurCompany,[]);
    DM_COMPANY.CO.EnableControls;    
  end;

end;

procedure TEDIT_CO.CheckDefaultDataSet(var CheckCondition: string;
  var bCheck: Boolean);
begin
  inherited;
  bCheck := False;
end;

constructor TEDIT_CO.Create(AOwner: TComponent);
begin
  inherited;
  evSecElement1.AtomComponent := nil;
  CheckFormCodeValues := TStringList.Create;
  MiscCheckFormCodeValues := TStringList.Create;
end;

procedure TEDIT_CO.OnSetButton(Kind: Integer; var Value: Boolean);
begin
  inherited;
  if (Kind = NavOK) and not Value then
    TabSheet1.Enabled := True;
end;

procedure TEDIT_CO.wwcbFederal_Tax_Payment_MethodChange(Sender: TObject);
var
  V: Variant;
begin
  inherited;
  if Assigned(wwdsMaster.DataSet) and
     (wwdsMaster.DataSet.State in [dsEdit, dsInsert]) then
  begin
    if (wwcbFederal_Tax_Payment_Method.Text = 'EFT Debit') or (wwcbFederal_Tax_Payment_Method.Text = 'Notices & EFT Debit') then
    begin
      if (DM_SERVICE_BUREAU.SB['EFTPS_BANK_FORMAT'] = GROUP_BOX_CHICAGO) then
      begin
        V := DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('AGENCY_NAME', 'Anexsys', 'SY_FED_TAX_PAYMENT_AGENCY_NBR');
        if not VARISNULL(V) then
        begin
          DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'] := V;
          DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'] := V;
        end;
      end
      else if ((DM_SERVICE_BUREAU.SB['EFTPS_BANK_FORMAT'] = GROUP_BOX_NATIONS) or (DM_SERVICE_BUREAU.SB['EFTPS_BANK_FORMAT'] = GROUP_BOX_BATCH_PROVIDER)) then
      begin
        V := DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('AGENCY_NAME', 'Bank Of America', 'SY_FED_TAX_PAYMENT_AGENCY_NBR');
        if not VARISNULL(V)then
        begin
          DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'] := V;
          DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'] := V;
        end;
      end;
    end;
  end;
end;

procedure TEDIT_CO.wwDBLookupCombo3CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
var
  V: Variant;
begin
  inherited;
  if (Modified) and (wwdsMaster.DataSet.State in [dsEdit, dsInsert]) and ((wwcbFederal_Tax_Payment_Method.Text = 'EFT Debit') or (wwcbFederal_Tax_Payment_Method.Text = 'Notices & EFT Debit')) then
  begin
    if EvMessage('If Federal Tax Payment method is EFT Debit, then Federal Tax Payment Agency must be Anexsys or Bank of America!',
    mtInformation, [mbOk]) = mrOk then
    begin
      if (DM_SERVICE_BUREAU.SB['EFTPS_BANK_FORMAT'] = GROUP_BOX_CHICAGO) then
      begin
        V := DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('AGENCY_NAME', 'Anexsys', 'SY_FED_TAX_PAYMENT_AGENCY_NBR');
        if not VARISNULL(V) then
        begin
          DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'] := V;
          DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'] := V;
        end;
      end
      else if ((DM_SERVICE_BUREAU.SB['EFTPS_BANK_FORMAT'] = GROUP_BOX_NATIONS) or (DM_SERVICE_BUREAU.SB['EFTPS_BANK_FORMAT'] = GROUP_BOX_BATCH_PROVIDER)) then
      begin
        V := DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('AGENCY_NAME', 'Bank Of America', 'SY_FED_TAX_PAYMENT_AGENCY_NBR');
        if not VARISNULL(V)then
        begin
          DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'] := V;
          DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'] := V;
        end;
      end;
    end;
  end;
end;

procedure TEDIT_CO.wwlcFUI_Tax_Payment_AngencyCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
var
  V: Variant;
begin
  inherited;
  if (Modified) and (wwdsMaster.DataSet.State in [dsEdit, dsInsert]) and ((wwcbFederal_Tax_Payment_Method.Text = 'EFT Debit') or (wwcbFederal_Tax_Payment_Method.Text = 'Notices & EFT Debit')) then
  begin
    if EvMessage('If Federal Tax Payment method is EFT Debit, then Federal Tax Payment Agency must be Anexsys or Bank of America!',
    mtInformation, [mbOk]) = mrOk then
    begin
      if (DM_SERVICE_BUREAU.SB['EFTPS_BANK_FORMAT'] = GROUP_BOX_CHICAGO) then
      begin
        V := DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('AGENCY_NAME', 'Anexsys', 'SY_FED_TAX_PAYMENT_AGENCY_NBR');
        if not VARISNULL(V) then
        begin
          DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'] := V;
          DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'] := V;
        end;
      end
      else if ((DM_SERVICE_BUREAU.SB['EFTPS_BANK_FORMAT'] = GROUP_BOX_NATIONS) or (DM_SERVICE_BUREAU.SB['EFTPS_BANK_FORMAT'] = GROUP_BOX_BATCH_PROVIDER)) then
      begin
        V := DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('AGENCY_NAME', 'Bank Of America', 'SY_FED_TAX_PAYMENT_AGENCY_NBR');
        if not VARISNULL(V)then
        begin
          DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'] := V;
          DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'] := V;
        end;
      end;
    end;
  end;
end;


procedure TEDIT_CO.btnCopyEEsClick(Sender: TObject);
var
  CompanyWizard: TCompanyCopyWizard;
  ClNbr: Integer;
  CoNbr:integer;
  sTmp_ClUserFilter : String;
begin
  inherited;
  if EvMessage('Are you sure you want to copy employees from a different company?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;

  CompanyWizard := TCompanyCopyWizard.CreateTCopyWizard(Nil, evEmployee);
  srcCoStates.MasterDataSource := nil;
  SaveDSStates([dsCL.DataSet as TevClientDataSet, wwdsMaster.DataSet as TevClientDataSet]);
  wwdsMaster.DataSet := nil;
  CoNbr := DM_COMPANY.CO.CO_NBR.AsInteger;
  ClNbr := dsCL.DataSet['CL_NBR'];
  sTmp_ClUserFilter:= '';
  if DM_TEMPORARY.TMP_CL.UserFiltered then
  begin
    sTmp_ClUserFilter:= DM_TEMPORARY.TMP_CL.userfilter;
    DM_TEMPORARY.TMP_CL.UserFiltered := false;
  end;
  try
    CompanyWizard.Caption := CompanyWizard.Caption + 'Employee';
    CompanyWizard.CLList.CLBase := dsCL.DataSet['CL_NBR'];
    CompanyWizard.CLList.CLNew := dsCL.DataSet['CL_NBR'];
    CompanyWizard.CLList.CONew := DM_COMPANY.CO.CO_NBR.Value;
    CompanyWizard.evPanel2.Visible := False;
    CompanyWizard.ShowModal;
  finally
    if sTmp_ClUserFilter <> '' then
    begin
      DM_TEMPORARY.TMP_CL.userfilter := sTmp_ClUserFilter;
      DM_TEMPORARY.TMP_CL.UserFiltered := True;
    end;
    wwdsMaster.DataSet := DM_COMPANY.CO;
    srcCoStates.MasterDataSource := wwdsMaster;
    LoadDSStates([dsCL.DataSet as TevClientDataSet, wwdsMaster.DataSet as TevClientDataSet]);
    CompanyWizard.Free;
    LocateCo(ClNBr,CoNbr);
  end;
end;
procedure TEDIT_CO.FTDFCopyToClick(Sender: TObject);
begin
  CopyTaxDeposit( 'FEDERAL_TAX_DEPOSIT_FREQUENCY');
end;

procedure TEDIT_CO.Fed945TDFCopyToClick(Sender: TObject);
begin
  CopyTaxDeposit( 'FED_945_TAX_DEPOSIT_FREQUENCY');
end;

procedure TEDIT_CO.FUITDFCopyToClick(Sender: TObject);
begin
  CopyTaxDeposit( 'FUI_TAX_DEPOSIT_FREQUENCY');
end;

procedure TEDIT_CO.wwcbStatus_CodeChange(Sender: TObject);
begin
  inherited;
  wwcbOtherService.Enabled := wwcbStatus_Code.Value = TERMINATION_OTHER_SERVICE;
end;

procedure TEDIT_CO.btnNewHireSetupClick(Sender: TObject);
begin
  with TNewHireReportSetup.Create(Self) do
  begin
    ShowModal;
    Free;
  end;
end;

procedure TEDIT_CO.OnGetSecurityStates(const States: TSecurityStateList);
const
  secScreenCOIDRo: TAtomContextRecord = (Tag: ctEnabledSRo; Caption: 'Enabled Except Company ID'; IconName: ''; Priority: 200);
begin
  with secScreenCOIDRo do
    States.InsertAfter(ctEnabled, Tag[1], Caption, IconName);
end;

procedure TEDIT_CO.edControlNameChange(Sender: TObject);
var
  h: String;
begin
  if (edControlName.Tag = 0) and edControlName.Focused and (TevClientDataSet(DM_COMPANY.CO).State = dsBrowse) then
  begin
    h := edControlName.Text;
    DM_COMPANY.CO.Edit;
    edControlName.Text := h;
  end;
end;

procedure TEDIT_CO.wwdsMasterDataChange(Sender: TObject; Field: TField);
begin
  inherited;

  if TevClientDataSet(DM_COMPANY.CO).State = dsBrowse then
  begin
    SetAca;

    edControlName.Tag := 1;
    try
      edControlName.Text := ExtractFromFiller(DM_COMPANY.CO.FILLER.AsString, 61, 4);
    finally
      edControlName.Tag := 0;
    end;

    edFiscalBeginDate.Tag := 1;
    try
     if dtFiscalBeginDate.date > 0 then
       edFiscalBeginDate.Text := FormatDatetime('mm/dd', dtFiscalBeginDate.date)
     else
       edFiscalBeginDate.Text := '';
    finally
      edFiscalBeginDate.Tag := 0;
    end;

    ShowCheckForm;
  end;
end;

procedure TEDIT_CO.wwcbPay_FrequenciesCloseUp(Sender: TwwDBComboBox;
  Select: Boolean);
begin
  inherited;
  FrequecyHasChanged := (wwcbPay_Frequencies.Modified);
    //EvMessage('Changing company pay frequency can effect your calendar setup!');
end;

procedure TEDIT_CO.ResultFIELDS_CL_BANK_Function;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_CL_BANK_') <> stEnabled then
   Begin
//    (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
    wwcbBank_Account_Level.Enabled := false;
    wwlcPayroll_Bank_Account_Number.Enabled := false;
    wwlcTax_Bank_Account_Number.Enabled := false;
    wwlcDD_Bank_Account_Number.Enabled := false;
    wwlcBilling_Bank_Account_Number.Enabled := false;
    wwlcWorkers_Comp_Bank_Account_Number.Enabled := false;
    cbManualBankAccount.Enabled := false;
   end;
  if ctx_AccountRights.Functions.GetState('FIELDS_SB_BANK_') <> stEnabled then
   Begin
//    (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;

    wwlcSB_Trust_Account_Number.Enabled := false;
    wwlcSB_Tax_Bank_Account_Number.Enabled := false;
    wwlcSB_ACH_Bank_Account_Number.Enabled := false;
    wwlcSB_Billing_Bank_Account_Number.Enabled := false;
    wwlcSB_Workers_Comp_Bank_Account_Number.Enabled := false;
    wwlcSB_OBC_Account_Nbr.Enabled := false;
   end;

End;

procedure TEDIT_CO.Enable_ESSCopyToClick(Sender: TObject);
begin
   CopyTaxDeposit( 'ENABLE_ESS');
end;

procedure TEDIT_CO.Enable_TimeOffCopyToClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('ENABLE_TIME_OFF');
end;

procedure TEDIT_CO.Enable_BenefitsCopyToClick(Sender: TObject);
begin
  inherited;
   CopyTaxDeposit('ENABLE_BENEFITS');
end;

procedure TEDIT_CO.Enable_DDCopyToClick(Sender: TObject);
begin
  inherited;
   CopyTaxDeposit( 'ENABLE_DD');
end;

procedure TEDIT_CO.Enable_EEAccesClick(Sender: TObject);
begin
  inherited;
   CopyTaxDeposit( 'ESS_OR_EP');
end;

function TEDIT_CO.CashManagementBankIsMandatory: Boolean;
begin
  if not DM_CLIENT.CO.IsEmpty then
  begin
    DM_CLIENT.CO.ACH_SB_BANK_ACCOUNT_NBR.Required :=
      (drgpTax_Service.Value = TAX_SERVICE_FULL)
      or (drgpTrust_Service.Value = TRUST_SERVICE_ALL)
      or (drgpTrust_Service.Value = TRUST_SERVICE_AGENCY);

    if DM_CLIENT.CO.ACH_SB_BANK_ACCOUNT_NBR.Required then
      lablSB_ACH_Bank_Account_Number.Caption := '~Cash Management'
    else
      lablSB_ACH_Bank_Account_Number.Caption := 'Cash Management';

    Result := DM_CLIENT.CO.ACH_SB_BANK_ACCOUNT_NBR.Required;
  end
  else
    Result := False;
end;

procedure TEDIT_CO.MenuItem1Click(Sender: TObject);
begin
  CopyTaxDeposit('TAX_IMPOUND');
end;

procedure TEDIT_CO.MenuItem2Click(Sender: TObject);
begin
  CopyTaxDeposit('TRUST_IMPOUND');
end;

procedure TEDIT_CO.MenuItem3Click(Sender: TObject);
begin
  CopyTaxDeposit('DD_IMPOUND');
end;

procedure TEDIT_CO.MenuItem4Click(Sender: TObject);
begin
  CopyTaxDeposit('BILLING_IMPOUND');
end;

procedure TEDIT_CO.MenuItem5Click(Sender: TObject);
begin
  CopyTaxDeposit('WC_IMPOUND');
end;

procedure TEDIT_CO.CopySbEnlistGroupsToCoLevel;
begin
    DM_SERVICE_BUREAU.SB_ENLIST_GROUPS.DataRequired('ALL');
    DM_COMPANY.CO_ENLIST_GROUPS.DataRequired('ALL');
    DM_SERVICE_BUREAU.SB_ENLIST_GROUPS.First;
    while not DM_SERVICE_BUREAU.SB_ENLIST_GROUPS.Eof do
    begin
      if (DM_SERVICE_BUREAU.SB_ENLIST_GROUPS.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger > 0) and
         (not DM_COMPANY.CO_ENLIST_GROUPS.Locate('CO_NBR;SY_REPORT_GROUPS_NBR',
                          VarArrayOf([DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger,DM_SERVICE_BUREAU.SB_ENLIST_GROUPS.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger]) ,[])) then
      begin
       DM_COMPANY.CO_ENLIST_GROUPS.Insert;
       DM_COMPANY.CO_ENLIST_GROUPS.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger :=
              DM_SERVICE_BUREAU.SB_ENLIST_GROUPS.FieldByName('SY_REPORT_GROUPS_NBR').AsInteger;
       DM_COMPANY.CO_ENLIST_GROUPS.FieldByName('CO_NBR').AsInteger :=
              DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger;
       DM_COMPANY.CO_ENLIST_GROUPS.FieldByName('PROCESS_TYPE').AsString :=
              DM_SERVICE_BUREAU.SB_ENLIST_GROUPS.FieldByName('PROCESS_TYPE').AsString;
       DM_COMPANY.CO_ENLIST_GROUPS.FieldByName('MEDIA_TYPE').AsString :=
              DM_SERVICE_BUREAU.SB_ENLIST_GROUPS.FieldByName('MEDIA_TYPE').AsString;
       DM_COMPANY.CO_ENLIST_GROUPS.Post;
      end;
      DM_SERVICE_BUREAU.SB_ENLIST_GROUPS.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_ENLIST_GROUPS]);
End;

procedure TEDIT_CO.MenuItem6Click(Sender: TObject);
begin
  CopyTaxDeposit('CO_PAYROLL_PROCESS_LIMITATIONS');
end;

procedure TEDIT_CO.miMiscCheckFormCopyToClick(Sender: TObject);
begin
  CopyTaxDeposit('MISC_CHECK_FORM');
end;

procedure TEDIT_CO.miCheckFormCopyToClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('CHECK_FORM');
end;

procedure TEDIT_CO.FillCheckForms;
var
  RegularSBChecks, MiscSBChecks: TStrings;
  i: integer;
  S1, S2, ComboChoices: String;
begin
  ComboChoices := CheckForm_ComboChoices;
  I := 1;
  while not GetToken(ComboChoices, I, S1) do
  begin
    GetToken(ComboChoices, I, S2);
    cbCheckForm.Items.Append(S1);
    CheckFormCodeValues.Append(S2);
    cbMiscCheckForm.Items.Append(S1);
    MiscCheckFormCodeValues.Append(S2);
  end;

  RegularSBChecks := TStringList.Create;
  MiscSBChecks := TStringList.Create;
  try
    ctx_DataAccess.FillSBCheckList(RegularSBChecks, MiscSBChecks);
    for i := 0 to RegularSBChecks.Count - 1 do
    begin
      cbCheckForm.Items.Append( RegularSBChecks.Names[i] );
      CheckFormCodeValues.Append( RegularSBChecks.ValueFromIndex[i] );
    end;
    for i := 0 to MiscSBChecks.Count - 1 do
    begin
      cbMiscCheckForm.Items.Append( MiscSBChecks.Names[i] );
      MiscCheckFormCodeValues.Append( MiscSBChecks.ValueFromIndex[i] );
    end;
  finally
    RegularSBChecks.Free;
    MiscSBChecks.Free;
  end;

  ShowCheckForm;
end;

procedure TEDIT_CO.ShowCheckForm(aDefault: boolean = False);
var
  s: string;
begin
  if aDefault then
    s := DM_SERVICE_BUREAU.SB.DEFAULT_CHECK_FORMAT.Value
  else if (DM_COMPANY.CO.FieldByName('CHECK_FORM').AsString = CHECK_FORM_BUREAU_DESIGNER_CHECK) or
    (DM_COMPANY.CO.FieldByName('CHECK_FORM').AsString = CHECK_FORM_BUREAU_NEW_CHECK) then
    s := ExtractFromFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, 65, 6)
  else
    s := DM_COMPANY.CO.FieldByName('CHECK_FORM').AsString;

  if Assigned(CheckFormCodeValues) then
    cbCheckForm.ItemIndex := CheckFormCodeValues.IndexOf(s);

  if aDefault then
    s := DM_SERVICE_BUREAU.SB.MISC_CHECK_FORM.Value
  else if (DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString = CHECK_FORM_BUREAU_DESIGNER_CHECK) or
    (DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString = CHECK_FORM_BUREAU_NEW_CHECK) then
    s := ExtractFromFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, 71, 6)
  else
    s := DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString;

  if Assigned(MiscCheckFormCodeValues) then
    cbMiscCheckForm.ItemIndex := MiscCheckFormCodeValues.IndexOf(s);
end;

destructor TEDIT_CO.Destroy;
begin
  CheckFormCodeValues.Free;
  MiscCheckFormCodeValues.Free;
  inherited;
end;

procedure TEDIT_CO.cbCheckFormCloseUp(Sender: TObject);
begin
  inherited;
  if not (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) then
    wwdsDetail.DataSet.Edit;
end;

procedure TEDIT_CO.cbCheckFormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  cbCheckFormCloseUp(Sender);
end;

procedure TEDIT_CO.MenuItem7Click(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('SALES_TAX_PERCENTAGE');
end;

procedure TEDIT_CO.tsCashManagementEnter(Sender: TObject);
begin
  inherited;
  if not DM_SERVICE_BUREAU.SB.Active then
    DM_SERVICE_BUREAU.SB.DataRequired('');
  DDComboBox.Enabled := DM_SERVICE_BUREAU.SB.MICR_FONT.AsString = 'N';
  UpdateControls;
end;

procedure TEDIT_CO.miCopyShowRateHoursClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('SHOW_EIN_NUMBER_ON_CHECK');
end;

procedure TEDIT_CO.mnEnableAnalyticsClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('ENABLE_ANALYTICS');
end;

procedure TEDIT_CO.mnAnalyticsLevelClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('ANALYTICS_LICENSE');
end;

procedure TEDIT_CO.edFiscalBeginDateClick(Sender: TObject);
begin
  inherited;
  dtFiscalBeginDate.BringToFront;
  if dtFiscalBeginDate.CanFocus then dtFiscalBeginDate.SetFocus;
end;

procedure TEDIT_CO.dtFiscalBeginDateCloseUp(Sender: TObject);
begin
  inherited;
  dtFiscalBeginDate.SendToBack;
  if dtFiscalBeginDate.date > 0 then
    edFiscalBeginDate.Text := FormatDatetime('mm/dd', dtFiscalBeginDate.date)
  else
    edFiscalBeginDate.Text := '';
  edFiscalBeginDate.BringToFront;
end;

procedure TEDIT_CO.Enable_HRCopyToClick(Sender: TObject);
begin
  inherited;
  if wwdsMaster.State = dsBrowse then
    CopyTaxDeposit('ENABLE_HR');
end;

procedure TEDIT_CO.wwcbDBDT_LevelChange(Sender: TObject);
begin
  inherited;
  if (PageControl1.ActivePage = tshtMisc) and (wwdsMaster.State in [ dsInsert, dsEdit] ) then
  begin
     if (not VarisNull(wwdsMaster.DataSet.FieldByName('DBDT_LEVEL').OldValue)) and
        (wwdsMaster.DataSet.FieldByName('DBDT_LEVEL').OldValue < wwcbDBDT_Level.ItemIndex) then
        EvMessage('After changing the DBDT Level to a lower level, verify that all of the previous level''s DBDTs now have the lower levels populated.  You will also need to update all employees individually with the current DBDT level.');
  end;
end;

procedure TEDIT_CO.SetAca;

  function YearOf(ADate : TDateTime) : Word;
  var mm, dd : Word;
  begin
    DecodeDate(ADate,Result,mm,dd);
  end;

  function MonthOf(ADate : TDateTime) : Word;
  var yy, dd : Word;
  begin
    DecodeDate(ADate,yy,Result,dd);
  end;


begin
  SettingAca := True;
  try
    if wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').IsNull then
    begin
      AcaFromYear.ItemIndex := -1;
      AcaFromMonth.ItemIndex := -1;
    end
    else
    begin
      AcaFromYear.ItemIndex := YearOf(wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime)-2014;
      AcaFromMonth.ItemIndex := MonthOf(wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime)-1;
    end;
    if wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').IsNull then
    begin
      AcaToYear.ItemIndex := -1;
      AcaToMonth.ItemIndex := -1;
    end
    else
    begin
      AcaToYear.ItemIndex := YearOf(wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime)-2014;
      AcaToMonth.ItemIndex := MonthOf(wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime)-1;
    end;
    if wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').IsNull then
    begin
      AcaStabFromYear.ItemIndex := -1;
      AcaStabFromMonth.ItemIndex := -1;
    end
    else
    begin
      AcaStabFromYear.ItemIndex := YearOf(wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').AsDateTime)-2014;
      AcaStabFromMonth.ItemIndex := MonthOf(wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').AsDateTime)-1;
    end;
    if wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').IsNull then
    begin
      AcaStabToYear.ItemIndex := -1;
      AcaStabToMonth.ItemIndex := -1;
    end
    else
    begin
      AcaStabToYear.ItemIndex := YearOf(wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').AsDateTime)-2014;
      AcaStabToMonth.ItemIndex := MonthOf(wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').AsDateTime)-1;
    end;
  finally
    SettingAca := False;
  end;
end;

function DefMonth(const Value: Integer): Integer;
begin
  if Value < 0 then
    Result := MonthOf(Now)
  else
    Result := Value + 1;
end;

function DefYear(const Value: String): Integer;
begin
  if StrToIntDef(Value,0) < 2000 then
    Result := YearOf(Now)
  else
    Result := StrToIntDef(Value,0);
end;

procedure TEDIT_CO.UpdateAca;

  function YearOf(ADate : TDateTime) : Word;
  var mm, dd : Word;
  begin
    DecodeDate(ADate,Result,mm,dd);
  end;

  function MonthOf(ADate : TDateTime) : Word;
  var yy, dd : Word;
  begin
    DecodeDate(ADate,yy,Result,dd);
  end;

begin
  inherited;

  if (AcaFromYear.Text <> '') or (AcaFromMonth.ItemIndex <> -1) then
    wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime := EncodeDate(DefYear(AcaFromYear.Text), DefMonth(AcaFromMonth.ItemIndex),1)
  else
    wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').Clear;

  if (AcaToYear.Text <> '') or (AcaToMonth.ItemIndex <> -1) then
    wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime := EncodeDate(DefYear(AcaToYear.Text), DefMonth(AcaToMonth.ItemIndex),1)
  else
    wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').Clear;

  if (AcaStabFromYear.Text <> '') or (AcaStabFromMonth.ItemIndex <> -1) then
    wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').AsDateTime := EncodeDate(DefYear(AcaStabFromYear.Text), DefMonth(AcaStabFromMonth.ItemIndex),1)
  else
    wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').Clear;

  if (AcaStabToYear.Text <> '') or (AcaStabToMonth.ItemIndex <> -1) then
    wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').AsDateTime := EncodeDate(DefYear(AcaStabToYear.Text), DefMonth(AcaStabToMonth.ItemIndex),1)
  else
    wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').Clear;

end;

function YearOf(ADate : TDateTime) : Word;
var mm, dd : Word;
begin
  DecodeDate(ADate,Result,mm,dd);
end;

function MonthOf(ADate : TDateTime) : Word;
var yy, dd : Word;
begin
  DecodeDate(ADate,yy,Result,dd);
end;

procedure TEDIT_CO.AcaFromYearChange(Sender: TObject);
begin
  if (wwdsMaster.DataSet.State in [dsBrowse]) and not SettingAca then
    wwdsMaster.DataSet.Edit;
  if wwdsMaster.DataSet.State in [dsInsert, dsEdit] then
  begin
    if (AcaFromYear.Text <> '') or (AcaFromMonth.ItemIndex <> -1) then
    begin
      if wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime <> EncodeDate(DefYear(AcaFromYear.Text), DefMonth(AcaFromMonth.ItemIndex),1) then
        wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime := EncodeDate(DefYear(AcaFromYear.Text), DefMonth(AcaFromMonth.ItemIndex),1)
    end
    else
      wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').Clear;
  end;
end;

procedure TEDIT_CO.AcaFromMonthChange(Sender: TObject);
begin
  if (wwdsMaster.DataSet.State in [dsBrowse]) and not SettingAca then
    wwdsMaster.DataSet.Edit;
  if wwdsMaster.DataSet.State in [dsInsert, dsEdit] then
  begin
    if (AcaFromYear.Text <> '') or (AcaFromMonth.ItemIndex <> -1) then
    begin
      if wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime <> EncodeDate(DefYear(AcaFromYear.Text), DefMonth(AcaFromMonth.ItemIndex),1) then
        wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').AsDateTime := EncodeDate(DefYear(AcaFromYear.Text), DefMonth(AcaFromMonth.ItemIndex),1)
    end
    else
      wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_FROM').Clear;
  end;
end;

procedure TEDIT_CO.AcaToYearChange(Sender: TObject);
begin
  if (wwdsMaster.DataSet.State in [dsBrowse]) and not SettingAca then
    wwdsMaster.DataSet.Edit;
  if wwdsMaster.DataSet.State in [dsInsert, dsEdit] then
  begin
    if (ConvertNull(AcaToYear.Text,0) <> 0) or (AcaToMonth.ItemIndex <> -1) then
    begin
      if wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime <> EncodeDate(DefYear(AcaToYear.Text), DefMonth(AcaToMonth.ItemIndex),1) then
        wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime := EncodeDate(DefYear(AcaToYear.Text), DefMonth(AcaToMonth.ItemIndex),1);
    end
    else
      wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').Clear;
  end;
end;

procedure TEDIT_CO.AcaToMonthChange(Sender: TObject);
begin
  if (wwdsMaster.DataSet.State in [dsBrowse]) and not SettingAca then
    wwdsMaster.DataSet.Edit;
  if wwdsMaster.DataSet.State in [dsInsert, dsEdit] then
  begin
    if (AcaToYear.Text <> '') or (AcaToMonth.ItemIndex <> -1) then
    begin
      if wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime <> EncodeDate(DefYear(AcaToYear.Text), DefMonth(AcaToMonth.ItemIndex),1) then
        wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').AsDateTime := EncodeDate(DefYear(AcaToYear.Text), DefMonth(AcaToMonth.ItemIndex),1);
    end
    else
      wwdsMaster.Dataset.FieldByName('ACA_INITIAL_PERIOD_TO').Clear;
  end;
end;

procedure TEDIT_CO.AcaStabFromYearChange(Sender: TObject);
begin
  if (wwdsMaster.DataSet.State in [dsBrowse]) and not SettingAca then
    wwdsMaster.DataSet.Edit;
  if wwdsMaster.DataSet.State in [dsInsert, dsEdit] then
  begin
    if (AcaStabFromYear.Text <> '') or (AcaStabFromMonth.ItemIndex <> -1) then
    begin
      if wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').AsDateTime <> EncodeDate(DefYear(AcaStabFromYear.Text), DefMonth(AcaStabFromMonth.ItemIndex),1) then
        wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').AsDateTime := EncodeDate(DefYear(AcaStabFromYear.Text), DefMonth(AcaStabFromMonth.ItemIndex),1)
    end
    else
      wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').Clear;
  end;
end;

procedure TEDIT_CO.AcaStabFromMonthChange(Sender: TObject);
begin
  if (wwdsMaster.DataSet.State in [dsBrowse]) and not SettingAca then
    wwdsMaster.DataSet.Edit;
  if wwdsMaster.DataSet.State in [dsInsert, dsEdit] then
  begin
    if (AcaStabFromYear.Text <> '') or (AcaStabFromMonth.ItemIndex <> -1) then
    begin
      if wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').AsDateTime <> EncodeDate(DefYear(AcaStabFromYear.Text), DefMonth(AcaStabFromMonth.ItemIndex),1) then
        wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').AsDateTime := EncodeDate(DefYear(AcaStabFromYear.Text), DefMonth(AcaStabFromMonth.ItemIndex),1)
    end
    else
      wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_FROM').Clear;
  end;
end;

procedure TEDIT_CO.AcaStabToYearChange(Sender: TObject);
begin
  if (wwdsMaster.DataSet.State in [dsBrowse]) and not SettingAca then
    wwdsMaster.DataSet.Edit;
  if wwdsMaster.DataSet.State in [dsInsert, dsEdit] then
  begin
    if (AcaStabToYear.Text <> '') or (AcaStabToMonth.ItemIndex <> -1) then
    begin
      if wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').AsDateTime <> EncodeDate(DefYear(AcaStabToYear.Text), DefMonth(AcaStabToMonth.ItemIndex),1) then
        wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').AsDateTime := EncodeDate(DefYear(AcaStabToYear.Text), DefMonth(AcaStabToMonth.ItemIndex),1)
    end
    else
      wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').Clear;
  end;
end;

procedure TEDIT_CO.AcaStabToMonthChange(Sender: TObject);
begin
  if (wwdsMaster.DataSet.State in [dsBrowse]) and not SettingAca then
    wwdsMaster.DataSet.Edit;
  if wwdsMaster.DataSet.State in [dsInsert, dsEdit] then
  begin
    if (AcaStabToYear.Text <> '') or (AcaStabToMonth.ItemIndex <> -1) then
    begin
      if wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').AsDateTime <> EncodeDate(DefYear(AcaStabToYear.Text), DefMonth(AcaStabToMonth.ItemIndex),1) then
        wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').AsDateTime := EncodeDate(DefYear(AcaStabToYear.Text), DefMonth(AcaStabToMonth.ItemIndex),1)
    end
    else
      wwdsMaster.Dataset.FieldByName('ACA_STABILITY_PERIOD_TO').Clear;
  end;
end;

procedure TEDIT_CO.wwdsMasterUpdateData(Sender: TObject);
begin
  SetAca;
end;

procedure TEDIT_CO.CopyTo1Click(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('ANNUAL_FORM_TYPE');
end;

procedure TEDIT_CO.CopyToEE(const CoField, EEField:String);
var
  k: integer;

  procedure DoCopyEETo(const effDate: TDateTime; const bEffectiveDate: Boolean);
  begin
    if bEffectiveDate then
    begin
      ctx_DBAccess.StartTransaction([dbtClient]);
      try
        ctx_DBAccess.UpdateFieldValue('EE', EEField, DM_EMPLOYEE.EE.EE_NBR.Value, effDate, DayBeforeEndOfTime, wwdsMaster.DataSet[CoField]);
        ctx_DBAccess.CommitTransaction;
      except
        ctx_DBAccess.RollbackTransaction;
        raise;
      end;
    end
    else
    begin
      DM_EMPLOYEE.EE.Edit;
      DM_EMPLOYEE.EE[EEField] := wwdsMaster.DataSet[CoField];
      DM_EMPLOYEE.EE.Post;
    end;
  end;

begin

  DM_EMPLOYEE.EE.DataRequired('CO_NBR = ' + wwdsMaster.DataSet.FieldByName('CO_NBR').AsString);
  with TEmployeeChoiceQuery.Create( Self ) do
  try
    SetFilter('');
    dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
    Caption := 'Employees';
    btnNo.Caption := '&Cancel';
    btnYes.Caption := 'C&opy';
    btnAll.Caption := 'Copy to &all';
    if CoField = 'ANNUAL_FORM_TYPE' then
         ConfirmAllMessage := 'This operation will copy Annual Form Type for all employees. Are you sure you want to do this?'
    else
    if CoField = 'BENEFITS_ELIGIBLE_DEFAULT' then
    begin
         ConfirmAllMessage := 'This operation will copy Benefits Eligible Default Type for all employees. Are you sure you want to do this?';
         pnlEffectiveDate.Visible:= true;
    end
    else
         ConfirmAllMessage := 'This operation will copy Print Voucher for all employees. Are you sure you want to do this?';

    if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
    begin
      ctx_StartWait('Processing...');
      try
        for k := 0 to dgChoiceList.SelectedList.Count - 1 do
        begin
          cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
          if DM_EMPLOYEE.EE.Locate('EE_NBR', cdsChoiceList.FieldByName('EE_NBR').AsString, []) then
            DoCopyEETo(dtBeginEffective.Date, pnlEffectiveDate.Visible);
        end;
      finally
        if not pnlEffectiveDate.Visible then
           ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE]);
        ctx_EndWait;
      end;
    end;
  finally
    Free;
  end;
end;

procedure TEDIT_CO.mnAnnualTypeCopyToEEClick(Sender: TObject);
begin
  inherited;
  CopyToEE('ANNUAL_FORM_TYPE', 'W2_TYPE');
end;


procedure TEDIT_CO.mnCOPrintVoucherClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('EE_PRINT_VOUCHER_DEFAULT');
end;

procedure TEDIT_CO.mnEEPrintVoucherClick(Sender: TObject);
begin
  inherited;
  CopyToEE('EE_PRINT_VOUCHER_DEFAULT', 'PRINT_VOUCHER');
end;

procedure TEDIT_CO.btnAddCertClick(Sender: TObject);
var
  i: integer;
begin
  inherited;

  cdAvailableCertificationsEligibility.DisableControls;
  DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.DisableControls;
  try
    for i := 0 to grdAvailableCertificationsEligibility.SelectedList.Count - 1 do
    begin
      cdAvailableCertificationsEligibility.GotoBookmark(grdAvailableCertificationsEligibility.SelectedList[i]);

      if not DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.Locate('ACA_CERT_ELIGIBILITY_TYPE',
                                          VarArrayOf([cdAvailableCertificationsEligibility['ACA_CERT_ELIGIBILITY_TYPE']]),[]) then
      begin
          DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.Insert;
          DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.FieldbyName('ACA_CERT_ELIGIBILITY_TYPE').AsString :=
                             cdAvailableCertificationsEligibility.fieldByName('ACA_CERT_ELIGIBILITY_TYPE').AsString;
          DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.FieldbyName('DESCRIPTION').AsString :=
                             cdAvailableCertificationsEligibility.fieldByName('DESCRIPTION').AsString;
          DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.Post;

          if DM_COMPANY.CO_ACA_CERT_ELIGIBILITY['ACA_CERT_ELIGIBILITY_TYPE'] = ACA_CERT_ELIGIBILITY_TYPE_C then
          begin
           cbALETransition.Visible := True;
           lblALETransition.Visible:= cbALETransition.Visible;
          end;


          cdAvailableCertificationsEligibility.Delete;
      end;
    end;
    grdAvailableCertificationsEligibility.UnselectAll;
  finally
    cdAvailableCertificationsEligibility.EnableControls;
    DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.EnableControls;
  end;

end;

procedure TEDIT_CO.btnRemoveCertClick(Sender: TObject);
var
  i: integer;
begin
  inherited;

  cdAvailableCertificationsEligibility.DisableControls;
  DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.DisableControls;
  try
    for i := 0 to grdAssignedCertificationsEligibility.SelectedList.Count - 1 do
    begin
      DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.GotoBookmark(grdAssignedCertificationsEligibility.SelectedList[i]);

      cdAvailableCertificationsEligibility.AppendRecord([DM_COMPANY.CO_ACA_CERT_ELIGIBILITY['ACA_CERT_ELIGIBILITY_TYPE'],
                                    DM_COMPANY.CO_ACA_CERT_ELIGIBILITY['DESCRIPTION']]);

      if DM_COMPANY.CO_ACA_CERT_ELIGIBILITY['ACA_CERT_ELIGIBILITY_TYPE'] = ACA_CERT_ELIGIBILITY_TYPE_C then
      begin
       if  not (wwdsMaster.DataSet.State in [dsInsert, dsEdit]) then
       begin
         DM_COMPANY.CO.Edit;
         DM_COMPANY.CO['ALE_TRANSITION_RELIEF']:= 'Y';
       end;
       cbALETransition.Visible := false;
       lblALETransition.Visible:= cbALETransition.Visible;
      end;

      DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.Delete;
    end;
    grdAssignedCertificationsEligibility.UnselectAll;
  finally
    cdAvailableCertificationsEligibility.EnableControls;
    DM_COMPANY.CO_ACA_CERT_ELIGIBILITY.EnableControls;
  end;

end;

procedure TEDIT_CO.mnACAServiceClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('ACA_SERVICES');
end;


procedure TEDIT_CO.mnPrintCPAClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('PRINT_CPA');
end;

procedure TEDIT_CO.mnBenefitsEligibleDefaultCOClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('BENEFITS_ELIGIBLE_DEFAULT');
end;

procedure TEDIT_CO.mnBenefitsEligibleDefaultEEClick(Sender: TObject);
begin
  inherited;
  CopyToEE('BENEFITS_ELIGIBLE_DEFAULT', 'BENEFITS_ELIGIBLE');
end;

procedure TEDIT_CO.rgIncludeAnalyticsChange(Sender: TObject);
begin
  inherited;

  if Assigned(wwdsMaster.DataSet) and ( wwdsMaster.DataSet.State in [dsInsert, dsEdit] ) then
     if rgIncludeAnalytics.ItemIndex = 1 then
        wwdsMaster.DataSet.FieldByName('ANALYTICS_LICENSE').AsString :=ANALYTICS_LICENSE_NA
     else
        wwdsMaster.DataSet.FieldByName('ANALYTICS_LICENSE').AsString :=ANALYTICS_LICENSE_BASIC;
end;

procedure TEDIT_CO.cbAnalyticsLevelChange(Sender: TObject);
begin
  inherited;
  if Assigned(wwdsMaster.DataSet) and ( wwdsMaster.DataSet.State in [dsInsert, dsEdit] ) then
     if  cbAnalyticsLevel.ItemIndex = 0  then
          rgIncludeAnalytics.ItemIndex := 1
     else
          rgIncludeAnalytics.ItemIndex := 0;
end;

procedure TEDIT_CO.mnPayrollProductClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('PAYROLL_PRODUCT');
end;


procedure TEDIT_CO.mnAleGroupClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('SB_ACA_GROUP_NBR');
end;

procedure TEDIT_CO.evcbHRChange(Sender: TObject);
begin
  inherited;
   cbOnlineBenefitEnrollment.Enabled := Context.License.HR and (evcbHR.Value = GROUP_BOX_ADVANCED_HR);
   cbCoreHR.Enabled := cbOnlineBenefitEnrollment.Enabled;
   cbTalentManagement.Enabled := cbOnlineBenefitEnrollment.Enabled;
   cbApplicantTracking.Enabled := cbOnlineBenefitEnrollment.Enabled;
end;

procedure TEDIT_CO.pmEnable_HRPopup(Sender: TObject);
begin
  inherited;
  Enable_HRCopyTo.Enabled := True;
  if DM_COMPANY.CO.FieldByName('ENABLE_HR').AsString = GROUP_BOX_ADVANCED_HR then
     Enable_HRCopyTo.Enabled := False;

end;

procedure TEDIT_CO.btnACAAffordabilityClick(Sender: TObject);
begin
  inherited;
  ShowPostAcaAffordabilityDlg;
end;

procedure TEDIT_CO.edShowEINOnCheckChange(Sender: TObject);
begin
  inherited;

  edtEINState.Enabled := false;
  if edShowEINOnCheck.ItemIndex = 1 then
     edtEINState.Enabled := True;

  if (edShowEINOnCheck.ItemIndex <> 1) and
     (TevClientDataSet(DM_COMPANY.CO).State in [dsEdit,dsInsert]) then
      DM_COMPANY.CO.FieldByName('SHOW_EIN_NUMBER_CO_STATES_NBR').Clear;
end;

procedure TEDIT_CO.mnEnforgeDOBClick(Sender: TObject);
begin
  inherited;
  CopyTaxDeposit('ENFORCE_EE_DOB');
end;

initialization
  RegisterClass(TEDIT_CO);

end.


