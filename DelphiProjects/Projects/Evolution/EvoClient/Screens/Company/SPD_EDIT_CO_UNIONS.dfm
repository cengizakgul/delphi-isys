inherited EDIT_CO_UNIONS: TEDIT_CO_UNIONS
  Width = 948
  Height = 566
  inherited Panel1: TevPanel
    Width = 948
    inherited pnlFavoriteReport: TevPanel
      Left = 796
    end
    inherited pnlTopLeft: TevPanel
      Width = 796
    end
  end
  inherited PageControl1: TevPageControl
    Width = 948
    Height = 512
    HelpContext = 31002
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 940
        Height = 483
        inherited pnlBorder: TevPanel
          Width = 936
          Height = 479
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 936
          Height = 479
          inherited pnlFashionBody: TevPanel
            inherited pnlSubbrowse: TevPanel
              object wwDBGrid1: TevDBGrid
                Left = 0
                Top = 0
                Width = 477
                Height = 565
                DisableThemesInTitle = False
                Selected.Strings = (
                  'UNION_NAME'#9'40'#9'Union Name'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CO_UNIONS\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 888
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 888
            Height = 372
            inherited Splitter1: TevSplitter
              Height = 368
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 345
              Height = 368
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 295
                Height = 288
                IniAttributes.SectionName = 'TEDIT_CO_UNIONS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 348
              Height = 368
            end
          end
        end
      end
    end
    object tshtUnions: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbUnionDetails: TScrollBox
        Left = 0
        Top = 0
        Width = 940
        Height = 483
        Align = alClient
        TabOrder = 0
        object pnlSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 314
          Height = 369
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwdgUnions: TevDBGrid
            Left = 12
            Top = 35
            Width = 281
            Height = 313
            TabStop = False
            DisableThemesInTitle = False
            Selected.Strings = (
              'UNION_NAME'#9'40'#9'Union Name'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_UNIONS\wwdgUnions'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object pnlUnionDetails: TisUIFashionPanel
          Left = 8
          Top = 385
          Width = 314
          Height = 92
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Union Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object lablUnion: TevLabel
            Left = 12
            Top = 34
            Width = 37
            Height = 16
            Caption = '~Name'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object wwlcUnion: TevDBLookupCombo
            Left = 13
            Top = 50
            Width = 281
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'NAME'#9'40'#9'NAME')
            DataField = 'CL_UNION_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CL_UNION.CL_UNION
            LookupField = 'CL_UNION_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO_UNIONS.CO_UNIONS
  end
end
