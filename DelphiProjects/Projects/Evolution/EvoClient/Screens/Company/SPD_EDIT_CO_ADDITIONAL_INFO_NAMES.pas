// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_ADDITIONAL_INFO_NAMES;

interface

uses
  SPD_EDIT_CO_BASE, StdCtrls, Mask, DBCtrls,  Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, Controls, Classes, SPackageEntry,
  SDataStructure, Db, Wwdatsrc, wwdbedit, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, Forms, SPD_MiniNavigationFrame, ActnList,
  Wwdotdot, Wwdbcomb, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_ADDITIONAL_INFO_NAMES = class(TEDIT_CO_BASE)
    wwdsValues: TevDataSource;
    sbCustomFields: TScrollBox;
    fpCustomFields: TisUIFashionPanel;
    wwdgCompany_Jobs: TevDBGrid;
    fpFieldDetail: TisUIFashionPanel;
    Label1: TevLabel;
    dedtName: TevDBEdit;
    edCategory: TevDBRadioGroup;
    qlblShowAddressOnCheck: TevLabel;
    edtDataType: TevDBComboBox;
    evLabel2: TevLabel;
    edSeqNumber: TevDBEdit;
    pnlValues: TevPanel;
    fpFieldValues: TisUIFashionPanel;
    evDBGrid1: TevDBGrid;
    fpValueDetail: TisUIFashionPanel;
    evLabel1: TevLabel;
    dedtValue: TevDBEdit;
    MiniNavigationFrame: TMiniNavigationFrame;
    evLabel3: TevLabel;
    cbShowinPayroll: TevDBComboBox;
    procedure MiniNavigationFrameSpeedButton1Click(Sender: TObject);
    procedure MiniNavigationFrameSpeedButton2Click(Sender: TObject);
    procedure dedtValueChange(Sender: TObject);
    procedure edCategoryChange(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure AfterClick( Kind: Integer ); override;
  end;

implementation

{$R *.DFM}

{ TEDIT_CO_ADDITIONAL_INFO_NAMES }

function TEDIT_CO_ADDITIONAL_INFO_NAMES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_ADDITIONAL_INFO_NAMES;
end;

procedure TEDIT_CO_ADDITIONAL_INFO_NAMES.Activate;
begin
  MiniNavigationFrame.DataSource := wwdsValues;
  MiniNavigationFrame.InsertFocusControl := dedtValue;
  inherited;
end;

procedure TEDIT_CO_ADDITIONAL_INFO_NAMES.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavInsert:
      DM_COMPANY.CO_ADDITIONAL_INFO_NAMES.FieldByName('SEQ_NUM').Value := 0;
  end;
end;

procedure TEDIT_CO_ADDITIONAL_INFO_NAMES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_CLIENT.CO_ADDITIONAL_INFO_VALUES, aDS);
end;
procedure TEDIT_CO_ADDITIONAL_INFO_NAMES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_CLIENT.CO_ADDITIONAL_INFO_VALUES, EditDataSets, '');
end;

function TEDIT_CO_ADDITIONAL_INFO_NAMES.GetInsertControl: TWinControl;
begin
  Result := dedtName;
end;

procedure TEDIT_CO_ADDITIONAL_INFO_NAMES.MiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  MiniNavigationFrame.InsertRecordExecute(Sender);
  DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.CO_ADDITIONAL_INFO_NAMES_NBR.Value :=
          DM_CLIENT.CO_ADDITIONAL_INFO_NAMES.CO_ADDITIONAL_INFO_NAMES_NBR.Value;
  DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.CO_NBR.Value :=
          DM_CLIENT.CO.CO_NBR.Value

end;

procedure TEDIT_CO_ADDITIONAL_INFO_NAMES.MiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  MiniNavigationFrame.DeleteRecordExecute(Sender);
end;

procedure TEDIT_CO_ADDITIONAL_INFO_NAMES.dedtValueChange(Sender: TObject);
begin
  inherited;
  if (DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.State in [dsEdit, dsInsert]) and (DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.CO_ADDITIONAL_INFO_NAMES_NBR.IsNull) then
  begin
    DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.CO_ADDITIONAL_INFO_NAMES_NBR.Value :=
          DM_CLIENT.CO_ADDITIONAL_INFO_NAMES.CO_ADDITIONAL_INFO_NAMES_NBR.Value;
    DM_CLIENT.CO_ADDITIONAL_INFO_VALUES.CO_NBR.Value :=
          DM_CLIENT.CO.CO_NBR.Value
  end;
end;

procedure TEDIT_CO_ADDITIONAL_INFO_NAMES.edCategoryChange(
  Sender: TObject);
begin
  inherited;
   edtDataType.Enabled := edCategory.ItemIndex = 0;
end;

procedure TEDIT_CO_ADDITIONAL_INFO_NAMES.wwdsDetailDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  //Disabled per Jake #97212
  //pnlValues.Visible := edtDataType.Text = 'Value';
end;

initialization
  RegisterClass(TEDIT_CO_ADDITIONAL_INFO_NAMES);

end.
