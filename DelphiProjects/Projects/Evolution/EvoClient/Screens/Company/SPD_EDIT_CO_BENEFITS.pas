unit SPD_EDIT_CO_BENEFITS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, ComCtrls, Grids, Wwdbigrd, Wwdbgrid, SPackageEntry,
  StdCtrls, wwdblook, DBCtrls, ExtCtrls, Buttons, SDataStructure,
   SDDClasses, SDataDictclient, SDataDicttemp, ISBasicClasses,
  wwdbdatetimepicker, Wwdotdot, Wwdbcomb, Mask, wwdbedit, EvConsts, EvExceptions,
  SPD_MiniNavigationFrame, SDataDictsystem, Wwdbspin, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvUtils,
  EvCommonInterfaces, EvDataset, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBComboBox, isUIwwDBLookupCombo,
  isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton, Variants, isBaseClasses, 
  isUISpeedButton, Menus, SPD_BenefitChoice;


type
  TEDIT_CO_BENEFITS = class(TEDIT_CO_BASE)
    tshtDetail: TTabSheet;
    tsSubTypeRates: TTabSheet;
    evPanel1: TevPanel;
    dsRates: TevDataSource;
    dsEDs: TevDataSource;
    Panel4: TPanel;
    dsSubtype: TevDataSource;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    edSubTypeDescription: TevDBEdit;
    lblBenefitAsOfDate: TevLabel;
    dtBenefitAsOfDate: TevDBDateTimePicker;
    cbRateType: TevDBComboBox;
    pnlRateGrid: TevPanel;
    grRates: TevDBGrid;
    pnlSubType: TevPanel;
    grdSubtype: TevDBGrid;
    pnlRateFields: TevPanel;
    fpBenefitsRight: TisUIFashionPanel;
    sbBenefitDetails: TScrollBox;
    fpBenefitDetails: TisUIFashionPanel;
    fpBenefitBenefit: TisUIFashionPanel;
    drgpRound_Benefit: TevDBRadioGroup;
    drgpTaxable: TevDBRadioGroup;
    drgpBenefit_Type: TevDBRadioGroup;
    rgUsedAsBenefit: TevDBRadioGroup;
    fpBenefitDeductionCode: TisUIFashionPanel;
    evLabel12: TevLabel;
    evLabel16: TevLabel;
    evLabel17: TevLabel;
    evLabel24: TevLabel;
    edEEdeductionNbr: TevDBLookupCombo;
    edERDeductionNbr: TevDBLookupCombo;
    cbEECobra: TevDBLookupCombo;
    cbERCobra: TevDBLookupCombo;
    lablBenefit_Name: TevLabel;
    lablCertificate_Number: TevLabel;
    lablGroup_Number: TevLabel;
    lablPolicy_Number: TevLabel;
    lablClient_Agency: TevLabel;
    lablFrequency: TevLabel;
    lablEmployee_Type: TevLabel;
    evLabel5: TevLabel;
    dedtBenefit_Name: TevDBEdit;
    dedtCertificate_Number: TevDBEdit;
    dedtGroup_Number: TevDBEdit;
    dedtPolicy_Number: TevDBEdit;
    wwlcClient_Agency: TevDBLookupCombo;
    wwcbFrequency: TevDBComboBox;
    wwcbEmployee_Type: TevDBComboBox;
    cbBenefitCategory: TevDBLookupCombo;
    fpAdditional: TisUIFashionPanel;
    lblLine1: TevLabel;
    edAdditional1: TevDBEdit;
    edAdditional2: TevDBEdit;
    edAdditional3: TevDBEdit;
    EvBevel1: TEvBevel;
    lablMinimum_Amount: TevLabel;
    lablMaximum_Amount: TevLabel;
    lablEligibility_Waiting_Period: TevLabel;
    lablMinimum_Age_Requirement: TevLabel;
    lablExpiration_Date: TevLabel;
    wwdeMinimum_Amount: TevDBEdit;
    wwdeMaximum_Amount: TevDBEdit;
    dedtEligibility_Waiting_Period: TevDBEdit;
    dedtMinimum_Age_Requirement: TevDBEdit;
    wwdpExpiration_Date: TevDBDateTimePicker;
    lblLine2: TevLabel;
    lblLine3: TevLabel;
    sbBenefitRates: TScrollBox;
    fpDescriptions: TisUIFashionPanel;
    fpBenefitRates: TisUIFashionPanel;
    fpRateDetails: TisUIFashionPanel;
    navDescription: TMiniNavigationFrame;
    fpDescriptionDetail: TisUIFashionPanel;
    evLabel2: TevLabel;
    SubTypeMiniNavigationFrame: TMiniNavigationFrame;
    pcRateType: TevPageControl;
    tsAmount: TTabSheet;
    evLabel20: TevLabel;
    evLabel14: TevLabel;
    evPERRatio: TevDBEdit;
    evPERPart: TevDBEdit;
    tsPercent: TTabSheet;
    lbEDGroup: TevLabel;
    evLabel21: TevLabel;
    evLabel22: TevLabel;
    evLabel23: TevLabel;
    edClientEDG: TevDBLookupCombo;
    evAEEPart: TevDBEdit;
    evAERPart: TevDBEdit;
    evACobraAmount: TevDBEdit;
    pnlRateHeader: TevPanel;
    evLabel1: TevLabel;
    evLabel4: TevLabel;
    evLabel3: TevLabel;
    lblEndDate: TevLabel;
    cbSubTypeDescription: TevDBLookupCombo;
    edMaxDependent: TevDBEdit;
    edPeriodBegin: TevDBDateTimePicker;
    edPeriodEnd: TevDBDateTimePicker;
    evLabel13: TevLabel;
    evLabel19: TevLabel;
    evPEEPart: TevDBEdit;
    evPEERatio: TevDBEdit;
    evLabel15: TevLabel;
    evLabel18: TevLabel;
    evPCobraAmount: TevDBEdit;
    evPAmount: TevDBEdit;
    wwDBGrid1: TevDBGrid;
    tsEnrollment: TTabSheet;
    ScrollBox1: TScrollBox;
    fpSettings: TisUIFashionPanel;
    evRequiresPOP: TevDBRadioGroup;
    evrgReguiresBeneficiaries: TevDBRadioGroup;
    evAllowHSA: TevDBRadioGroup;
    evShowRates: TevDBRadioGroup;
    evRequiresDOB: TevDBComboBox;
    evLabel6: TevLabel;
    evAllowEE: TevDBRadioGroup;
    evLabel7: TevLabel;
    evRequiresSSN: TevDBComboBox;
    fpQualifyingEvent: TisUIFashionPanel;
    evLabel8: TevLabel;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBSpinEdit1: TevDBSpinEdit;
    fpStateAvailability: TisUIFashionPanel;
    bvSettings: TEvBevel;
    dsFilteredSubTypes: TevDataSource;
    dsFilteredRates: TevDataSource;
    dsStates: TevDataSource;
    grdBenefitStates: TevDBGrid;
    evLabel9: TevLabel;
    cbBenefitState: TevDBLookupCombo;
    StateMiniNavigator: TMiniNavigationFrame;
    lblTaxable: TevLabel;
    edTaxableFringe: TevDBLookupCombo;
    evLabel11: TevLabel;
    evDBComboBox1: TevDBComboBox;
    pmCopyBenefits: TevPopupMenu;
    MenuItem3: TMenuItem;
    rgAcaLowestCost: TevDBRadioGroup;
    rgW2Flag: TevDBRadioGroup;
    rgAcaBenefit: TevDBRadioGroup;
    rgSelfInduredPlan: TevDBRadioGroup;
    pmCopySelfInsured: TevPopupMenu;
    mnSelfInsured: TMenuItem;
    cbBenefitStartDate: TevDBComboBox;
    evLabel10: TevLabel;
    evLabel25: TevLabel;
    cbPensionCatchUp: TevDBLookupCombo;
    Label1: TevLabel;
    DBText3: TevDBText;
    pmCopyRate: TevPopupMenu;
    mnCopyRate: TMenuItem;
    procedure dsSubtypeStateChange(Sender: TObject);
    procedure evAEEPartExit(Sender: TObject);
    procedure evAERPartExit(Sender: TObject);
    procedure evPAmountExit(Sender: TObject);
    procedure evPEERatioExit(Sender: TObject);
    procedure evPERRatioExit(Sender: TObject);
    procedure SubTypeMiniNavigationFrameSpeedButton1Click(Sender: TObject);
    procedure SubTypeMiniNavigationFrameSpeedButton2Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure dtBenefitAsOfDateChange(Sender: TObject);
    procedure dsRatesStateChange(Sender: TObject);
    procedure dedtBenefit_NameChange(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure dsRatesDataChange(Sender: TObject; Field: TField);
    procedure cbSubTypeDescriptionBeforeDropDown(Sender: TObject);
    procedure fpBenefitsRightResize(Sender: TObject);
    procedure navDescriptionSpeedButton1Click(Sender: TObject);
    procedure navDescriptionSpeedButton2Click(Sender: TObject);
    procedure edPeriodEndExit(Sender: TObject);
    procedure StateMiniNavigatorSpeedButton1Click(Sender: TObject);
    procedure StateMiniNavigatorSpeedButton2Click(Sender: TObject);
    procedure pcRateTypeChange(Sender: TObject);
    procedure pcRateTypeChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure dsFilteredSubTypesDataChange(Sender: TObject; Field: TField);
    procedure dsSubtypeUpdateData(Sender: TObject);
    procedure dsFilteredRatesDataChange(Sender: TObject; Field: TField);
    procedure dsRatesUpdateData(Sender: TObject);
    procedure cbBenefitCategoryChange(Sender: TObject);
    procedure cbBenefitCategoryCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure MenuItem3Click(Sender: TObject);
    procedure fpRateDetailsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure mnSelfInsuredClick(Sender: TObject);
    procedure mnCopyRateClick(Sender: TObject);
  private
    bAlwaysCallCalcFields, bLookupsEnabled, bUpdateRateEndDate, bRateCopy : boolean;
    FFilteredSubTypeDS, FFilteredRatesDS : IevDataSet;
    QCompanyCategory: IevQuery;
    function CalcParts(const FlatPart, Ratio: Double): Double;
    procedure SetBenefitRateFilter(const AllTable:Boolean=True);
    procedure ClearRateTabFields;
    procedure CreateQCompanyCategory;
    procedure CopyBenefits;
    procedure SetEnabledForRate;
    procedure CopySelfInsured;
    procedure CopyRate;        
  protected
    function GetDataSetConditions(sName: string): string; override;
    procedure OnActivateParams(const AContext: Integer; const AParams: array of Variant); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick( Kind: Integer ); override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure DoAfterPost; override;
    procedure DoOnCancel; override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses DateUtils, SFrameEntry, isDataSet, SPD_EDIT_Open_BASE, SPD_CompanyChoiceQuery, ISBasicUtils;

{$R *.DFM}

procedure TEDIT_CO_BENEFITS.OnActivateParams(const AContext: Integer; const AParams: array of Variant);
begin
  inherited;
  if AContext = 1 then
  begin
    if AParams[0] > 0 then
    begin
     PageControl1.ActivePage := tshtDetail;
     DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR', AParams[0], []);
    end
    else
     PageControl1.ActivePage := TabSheet1;
  end;
end;

procedure TEDIT_CO_BENEFITS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);

  function CheckRateDatesLimit: Boolean;
  var
    s, sTmp: String;
    Q: IevQuery;
  begin
    Result := false;
    if dsRates.DataSet.State in [dsEdit] then
       sTmp := ' and CO_BENEFIT_RATES_NBR <> ' + dsRates.DataSet.fieldByName('CO_BENEFIT_RATES_NBR').AsString;

    s:= 'select CO_BENEFIT_RATES_NBR, CO_BENEFIT_SUBTYPE_NBR, PERIOD_BEGIN, PERIOD_END ' +
           ' from CO_BENEFIT_RATES where {AsOfNow<CO_BENEFIT_RATES>} and CO_BENEFIT_SUBTYPE_NBR = :pSubType ' + sTmp +
            ' order by PERIOD_BEGIN desc ';
    Q := TevQuery.Create(s);
    Q.Params.AddValue('pSubType', DM_COMPANY.CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE_NBR.AsInteger);
    Q.Execute;
    if Q.Result.RecordCount > 0 then
    begin
      if Q.Result.Locate('PERIOD_BEGIN', edPeriodBegin.Date, []) then
         result := True
      else
      begin
        Q.Result.First;
        while not Q.Result.Eof do
        begin
           if ((( edPeriodBegin.Date>=Q.Result.FieldByName('PERIOD_BEGIN').AsDateTime) and
                ( edPeriodBegin.Date<=Q.Result.FieldByName('PERIOD_END').AsDateTime))
               or
               ((edPeriodBegin.Date>=Q.Result.FieldByName('PERIOD_BEGIN').AsDateTime) and
                (Q.Result.FieldByName('PERIOD_END').AsDateTime = 73051)))  then // EXCEPTION WRONG_PERIOD_BEGIN;
           begin
              result := True;
              break;
           end;
           if (((edPeriodEnd.Date>=Q.Result.FieldByName('PERIOD_BEGIN').AsDateTime) and
                (edPeriodEnd.Date<=Q.Result.FieldByName('PERIOD_END').AsDateTime))
               or
               ((edPeriodEnd.Date>=Q.Result.FieldByName('PERIOD_BEGIN').AsDateTime) and
                (Q.Result.FieldByName('PERIOD_END').AsDateTime = 73051))) then  // EXCEPTION WRONG_PERIOD_END;
           begin
              result := True;
              break;
           end;
           if ((edPeriodBegin.Date<Q.Result.FieldByName('PERIOD_BEGIN').AsDateTime) and
               ((edPeriodEnd.Date>Q.Result.FieldByName('PERIOD_END').AsDateTime) or
                (edPeriodEnd.Date = 0))) then  // EXCEPTION WRONG_PERIOD_END;
           begin
              result := True;
              break;
           end;

           Q.Result.Next;
        end;
      end;
    end;

  end;

  function CheckBenefitInUSe:boolean;
   var
     s: string;
     Q: IEvQuery;
  begin
    s:= 'select count(CO_BENEFIT_SUBTYPE_NBR) from CO_BENEFIT_SUBTYPE ' +
           ' where {AsOfNow<CO_BENEFIT_SUBTYPE>} and CO_BENEFITS_NBR = ' + IntToStr(DM_CLIENT.CO_BENEFITS.FieldByName('CO_BENEFITS_NBR').AsInteger);

    Q := TevQuery.Create(s);
    Q.Execute;
    result := Q.Result.Fields[0].AsInteger > 0;
  end;


begin

  if Kind = NavOK then
  begin
    if dsRates.DataSet.State in [dsEdit,dsInsert] then
    begin
     if (trim(edPeriodBegin.Text) <>'') and (trim(edPeriodEnd.Text) <>'') then
       if edPeriodBegin.Date > edPeriodEnd.Date then
         raise EUpdateError.CreateHelp('You can not have an Period end Date less than the Period begin Date.', IDH_ConsistencyViolation);

     if  (LowerCase(Trim(cbSubTypeDescription.Text)) = 'employee defined') and (trim(edPeriodEnd.Text) = '') then
         raise EUpdateError.CreateHelp('Period End Date must be entered for Employee Defined rates.', IDH_ConsistencyViolation);

     if pcRateType.ActivePage  = tsPercent then
       if (Length(trim(edClientEDG.Text)) = 0) then
         raise EUpdateError.CreateHelp('You have to select E/D Group.', IDH_ConsistencyViolation);

     if CheckRateDatesLimit then
         raise EUpdateError.CreateHelp('Please check Period Begin Date and Period End Date limits.', IDH_ConsistencyViolation);

     bAlwaysCallCalcFields := DM_COMPANY.CO_BENEFIT_RATES.AlwaysCallCalcFields;
     bLookupsEnabled := DM_COMPANY.CO_BENEFIT_RATES.LookupsEnabled;
     DM_COMPANY.CO_BENEFIT_RATES.AlwaysCallCalcFields := false;
     DM_COMPANY.CO_BENEFIT_RATES.LookupsEnabled := false;
     bUpdateRateEndDate := True;
    end;

    if dsSubtype.DataSet.State in [dsEdit,dsInsert] then
    begin
      bAlwaysCallCalcFields := DM_COMPANY.CO_BENEFIT_RATES.AlwaysCallCalcFields;
      bLookupsEnabled := DM_COMPANY.CO_BENEFIT_RATES.LookupsEnabled;
      DM_COMPANY.CO_BENEFIT_RATES.AlwaysCallCalcFields := false;
      DM_COMPANY.CO_BENEFIT_RATES.LookupsEnabled := false;
    end;
  end;
  inherited;

  if Kind = NavDelete then
    if CheckBenefitInUSe then
          raise EUpdateError.CreateHelp('This Benefit has attached rates, Please delete them first.', IDH_ConsistencyViolation);

end;

procedure TEDIT_CO_BENEFITS.AfterClick(Kind: Integer);
var
 beginDate : TDateTime;
begin
  inherited;

  if Kind in [NavCommit, NavAbort] then
  begin
     if Kind = NavAbort then
        DoOnCancel;

     SetEnabledForRate;

     if (Kind = NavCommit) and bUpdateRateEndDate then
     begin

          beginDate := edPeriodBegin.Date - 1;
          if (DM_COMPANY.CO_BENEFIT_RATES.State = dsBrowse) then
          begin
           if DM_COMPANY.CO_BENEFIT_RATES.RecordCount > 0 then
           begin
              DM_COMPANY.CO_BENEFIT_RATES.Filter :=
                  'CO_BENEFIT_RATES_NBR <> ' +
                   DM_COMPANY.CO_BENEFIT_RATES.FieldByName('CO_BENEFIT_RATES_NBR').AsString +
                   ' and CO_BENEFIT_SUBTYPE_NBR = ' +  DM_COMPANY.CO_BENEFIT_RATES.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsString +
                     ' and cPERIOD_END is null';
              DM_COMPANY.CO_BENEFIT_RATES.Filtered  := True;
           end;
           try
             if DM_COMPANY.CO_BENEFIT_RATES.RecordCount > 0 then
             begin
               if DM_COMPANY.CO_BENEFIT_RATES.RecordCount = 1 then
               begin
                if beginDate > DM_COMPANY.CO_BENEFIT_RATES.FieldByName('PERIOD_BEGIN').AsDateTime then
                begin
                   DM_COMPANY.CO_BENEFIT_RATES.Edit;
                   DM_COMPANY.CO_BENEFIT_RATES.FieldByName('PERIOD_END').AsDateTime := beginDate;
                   DM_COMPANY.CO_BENEFIT_RATES.FieldByName('cPERIOD_END').AsDateTime := beginDate;
                   DM_COMPANY.CO_BENEFIT_RATES.Post;
                   ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BENEFIT_RATES]);
                end;
               end
               else
                 EvMessage('There are more then one record, please fix manually!!!', mtWarning, [mbOK]);
             end;
           finally
             DM_COMPANY.CO_BENEFIT_RATES.LookupsEnabled := bLookupsEnabled;
             DM_COMPANY.CO_BENEFIT_RATES.AlwaysCallCalcFields := bAlwaysCallCalcFields;
             SetBenefitRateFilter;
           end;

          end;
     end;
     DM_COMPANY.CO_BENEFIT_RATES.LookupsEnabled := bLookupsEnabled;
     DM_COMPANY.CO_BENEFIT_RATES.AlwaysCallCalcFields := bAlwaysCallCalcFields;
     bUpdateRateEndDate := false;
  end;

end;



function TEDIT_CO_BENEFITS.GetDataSetConditions(sName: string): string;
begin
  Result := inherited GetDataSetConditions(sName);
end;

function TEDIT_CO_BENEFITS.GetInsertControl: TWinControl;
begin
  Result := dedtBENEFIT_NAME;
end;

function TEDIT_CO_BENEFITS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BENEFITS;
end;

procedure TEDIT_CO_BENEFITS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SYSTEM_STATE.SY_STATES, SupportDataSets);
  AddDS(DM_CLIENT.CL_E_D_GROUPS, SupportDataSets);
  AddDS(DM_COMPANY.CO_BENEFIT_CATEGORY, SupportDataSets);
  AddDS(DM_COMPANY.CO_E_D_CODES, SupportDataSets);
  AddDS(DM_COMPANY.CO_BENEFIT_SUBTYPE, EditDataSets);
  AddDS(DM_COMPANY.CO_BENEFIT_RATES, EditDataSets);
  AddDS(DM_CLIENT.CL_AGENCY, EditDataSets);
  AddDS(DM_COMPANY.CO_BENEFIT_STATES, EditDataSets);
end;

procedure TEDIT_CO_BENEFITS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  AddDS(DM_COMPANY.CO_BENEFIT_SUBTYPE, aDS);
  AddDS(DM_COMPANY.CO_BENEFIT_RATES, aDS);
  AddDS(DM_COMPANY.CO_BENEFIT_CATEGORY, aDS);
  AddDS(DM_COMPANY.CO_BENEFIT_STATES, aDS);
  inherited;

end;

procedure TEDIT_CO_BENEFITS.CreateQCompanyCategory;
var
 s: string;
begin
  s:='select CO_BENEFIT_CATEGORY_NBR,CATEGORY_TYPE from CO_BENEFIT_CATEGORY where {AsOfNow<CO_BENEFIT_CATEGORY>} and CO_NBR=:CoNbr';
  QCompanyCategory := TevQuery.Create(s);
  QCompanyCategory.Params.AddValue('CoNbr', DM_COMPANY.CO.CO_NBR.AsInteger);
  QCompanyCategory.Execute;
end;

procedure TEDIT_CO_BENEFITS.AfterDataSetsReopen;
begin
  inherited;
  CreateQCompanyCategory;
end;

procedure TEDIT_CO_BENEFITS.Activate;
begin
  inherited;
  bLookupsEnabled := DM_COMPANY.CO_BENEFIT_RATES.LookupsEnabled;
  bAlwaysCallCalcFields := DM_COMPANY.CO_BENEFIT_RATES.AlwaysCallCalcFields;
  bUpdateRateEndDate := False;
  dtBenefitAsOfDate.Date := EncodeDate(YearOf(Date),1,1);
  pcRateType.ActivePage := tsAmount;

  //assign the navigation datasources

  SubTypeMiniNavigationFrame.DataSource := dsRates;
  SubTypeMiniNavigationFrame.InsertFocusControl := edMaxDependent;
  SubTypeMiniNavigationFrame.SpeedButton1.OnClick := SubTypeMiniNavigationFrameSpeedButton1Click;
  SubTypeMiniNavigationFrame.SpeedButton2.OnClick := SubTypeMiniNavigationFrameSpeedButton2Click;

  navDescription.DataSource := dsSubtype;
  navDescription.InsertFocusControl := edSubTypeDescription;
  navDescription.SpeedButton1.OnClick := navDescriptionSpeedButton1Click;
  navDescription.SpeedButton2.OnClick := navDescriptionSpeedButton2Click;

  StateMiniNavigator.DataSource := dsStates;
  StateMiniNavigator.InsertFocusControl := cbBenefitState;
  StateMiniNavigator.SpeedButton1.OnClick := StateMiniNavigatorSpeedButton1Click;
  StateMiniNavigator.SpeedButton2.OnClick := StateMiniNavigatorSpeedButton2Click;


  if not DM_COMPANY.CO.CO_NBR.IsNull then
    CreateQCompanyCategory;

end;

procedure TEDIT_CO_BENEFITS.Deactivate;
begin
  inherited;

  FFilteredSubTypeDS := nil;
  FFilteredRatesDS := nil;

  DM_COMPANY.CO_BENEFIT_RATES.IndexFieldNames := '';
  if DM_COMPANY.CO_BENEFIT_RATES.Active then
    DM_COMPANY.CO_BENEFIT_RATES.CancelRange;
  DM_COMPANY.CO_BENEFIT_SUBTYPE.IndexFieldNames := '';
  if DM_COMPANY.CO_BENEFIT_SUBTYPE.Active then
    DM_COMPANY.CO_BENEFIT_SUBTYPE.CancelRange;
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.Close;
end;

function TEDIT_CO_BENEFITS.CalcParts(const FlatPart,
  Ratio: Double): Double;
begin
  Result := (Ratio/100)*FlatPart;
end;

procedure TEDIT_CO_BENEFITS.evAEEPartExit(Sender: TObject);
begin
  inherited;
  if (DM_COMPANY.CO_BENEFIT_RATES.State = dsBrowse) or (DM_COMPANY.CO_BENEFIT_RATES.RATE_TYPE.AsString <> BENEFIT_RATES_RATE_TYPE_PERCENT) then
  begin
    DM_COMPANY.CO_BENEFIT_RATES.OnCalcFields(DM_COMPANY.CO_BENEFIT_RATES);
    Exit;
  end;
  DM_COMPANY.CO_BENEFIT_RATES.ER_RATE.AsFloat := 100 - DM_COMPANY.CO_BENEFIT_RATES.EE_RATE.AsFloat;
end;

procedure TEDIT_CO_BENEFITS.evAERPartExit(Sender: TObject);
begin
  inherited;
  if (DM_COMPANY.CO_BENEFIT_RATES.State = dsBrowse) or (DM_COMPANY.CO_BENEFIT_RATES.RATE_TYPE.AsString <> BENEFIT_RATES_RATE_TYPE_PERCENT) then
  begin
    DM_COMPANY.CO_BENEFIT_RATES.OnCalcFields(DM_COMPANY.CO_BENEFIT_RATES);
    Exit;
  end;
  DM_COMPANY.CO_BENEFIT_RATES.EE_RATE.AsFloat := 100 - DM_COMPANY.CO_BENEFIT_RATES.ER_RATE.AsFloat;
end;

procedure TEDIT_CO_BENEFITS.evPAmountExit(Sender: TObject);
var
  F1, F2: Double;
begin
  if DM_COMPANY.CO_BENEFIT_RATES.State <> dsBrowse then
  begin
    if (DM_COMPANY.CO_BENEFIT_RATES.FieldByName('cTotalAmount').AsFloat > 0) and
       (DM_COMPANY.CO_BENEFIT_RATES.FieldByName('cEEPortion').AsFloat > 0) and
       (DM_COMPANY.CO_BENEFIT_RATES.FieldByName('cERPortion').AsFloat > 0) then
    begin
       F1 := CalcParts(DM_COMPANY.CO_BENEFIT_RATES['cTotalAmount'], DM_COMPANY.CO_BENEFIT_RATES['cEEPortion']);
       F2 := CalcParts(DM_COMPANY.CO_BENEFIT_RATES['cTotalAmount'], DM_COMPANY.CO_BENEFIT_RATES['cERPortion']);
       DM_COMPANY.CO_BENEFIT_RATES.EE_RATE.AsFloat := F1;
       DM_COMPANY.CO_BENEFIT_RATES.ER_RATE.AsFloat := F2;
    end;
  end;
end;

procedure TEDIT_CO_BENEFITS.evPEERatioExit(Sender: TObject);
var
  F1, F2: Double;
begin
  if DM_COMPANY.CO_BENEFIT_RATES.State <> dsBrowse then
  begin
    if (DM_COMPANY.CO_BENEFIT_RATES.FieldByName('cTotalAmount').AsFloat > 0) then
    begin
      F1 := CalcParts(DM_COMPANY.CO_BENEFIT_RATES['cTotalAmount'], DM_COMPANY.CO_BENEFIT_RATES['cEEPortion']);
      F2 := CalcParts(DM_COMPANY.CO_BENEFIT_RATES['cTotalAmount'], 100 - DM_COMPANY.CO_BENEFIT_RATES['cEEPortion']);
      DM_COMPANY.CO_BENEFIT_RATES.EE_RATE.AsFloat :=  F1;
      DM_COMPANY.CO_BENEFIT_RATES.ER_RATE.AsFloat := F2;
    end
    else
    begin
      DM_COMPANY.CO_BENEFIT_RATES['cEEPortion'] := 0;
      DM_COMPANY.CO_BENEFIT_RATES['cERPortion'] := 0;
      DM_COMPANY.CO_BENEFIT_RATES['cTotalAmount'] := 0;
    end;
  end;
end;

procedure TEDIT_CO_BENEFITS.evPERRatioExit(Sender: TObject);
var
  F1, F2: Double;
begin
  if DM_COMPANY.CO_BENEFIT_RATES.State <> dsBrowse then
  begin
    if (DM_COMPANY.CO_BENEFIT_RATES.FieldByName('cTotalAmount').AsFloat > 0) then
    begin
      F1 := CalcParts(DM_COMPANY.CO_BENEFIT_RATES['cTotalAmount'], 100 - DM_COMPANY.CO_BENEFIT_RATES['cERPortion']);
      F2 := CalcParts(DM_COMPANY.CO_BENEFIT_RATES['cTotalAmount'], DM_COMPANY.CO_BENEFIT_RATES['cERPortion']);
      DM_COMPANY.CO_BENEFIT_RATES.EE_RATE.AsFloat :=  F1;
      DM_COMPANY.CO_BENEFIT_RATES.ER_RATE.AsFloat := F2;
    end
    else
    begin
      DM_COMPANY.CO_BENEFIT_RATES['cEEPortion'] := 0;
      DM_COMPANY.CO_BENEFIT_RATES['cERPortion'] := 0;
      DM_COMPANY.CO_BENEFIT_RATES['cTotalAmount'] := 0;
    end;
  end;
end;

procedure TEDIT_CO_BENEFITS.SubTypeMiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  if (DM_COMPANY.CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE_NBR.AsInteger > 0) then
  begin
    if DM_COMPANY.CO_BENEFIT_RATES.State = dsBrowse then
      edMaxDependent.SetFocus;

    SubTypeMiniNavigationFrame.InsertRecordExecute(Sender);

    if (dsRates.DataSet.State = dsInsert) and dsRates.DataSet.FieldByName('EE_RATE').isnull then
       dsRates.DataSet.FieldByName('EE_RATE').AsCurrency :=0;
    if (dsRates.DataSet.State = dsInsert) and dsRates.DataSet.FieldByName('ER_RATE').isnull then
       dsRates.DataSet.FieldByName('ER_RATE').AsCurrency :=0;

    if (dsRates.DataSet.State = dsInsert) then
    begin
       if pcRateType.ActivePage  = tsAmount then
          dsRates.DataSet.FieldByName('RATE_TYPE').AsString   := BENEFIT_RATES_RATE_TYPE_AMOUNT
       else
          dsRates.DataSet.FieldByName('RATE_TYPE').AsString   := BENEFIT_RATES_RATE_TYPE_PERCENT;
    end;
  end
  else
    EvMessage('Please enter a description.', mtWarning, [mbOK]);
end;

procedure TEDIT_CO_BENEFITS.SubTypeMiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  SubTypeMiniNavigationFrame.DeleteRecordExecute(Sender);
end;

procedure TEDIT_CO_BENEFITS.ClearRateTabFields;
begin
   cbSubTypeDescription.Text := '';
   edMaxDependent.Text := '';
   edPeriodBegin.Text := '';
   edPeriodEnd.Text := '';

   evPEEPart.Text := '';
   evPEERatio.Text := '';
   evPERPart.Text := '';
   evPERRatio.Text := '';
   evPAmount.Text := '';
   evPCobraAmount.Text := '';

   edClientEDG.Text := '';
   evAEEPart.Text := '';
   evAERPart.Text := '';
   evACobraAmount.Text := '';
end;


procedure TEDIT_CO_BENEFITS.SetBenefitRateFilter(const AllTable:Boolean=True);
var
  key_val: Integer;
Begin
  if DM_COMPANY.CO_BENEFIT_RATES.Active and (not bRateCopy) then
  begin
      DM_COMPANY.CO_BENEFIT_SUBTYPE.DisableControls;
      try
         key_val := DM_COMPANY.CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE_NBR.AsInteger;
         DM_COMPANY.CO_BENEFIT_SUBTYPE.IndexFieldNames := 'CO_BENEFITS_NBR;CO_BENEFIT_SUBTYPE_NBR';
         DM_COMPANY.CO_BENEFIT_SUBTYPE.SetRange([DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']], [DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']]);

         dsFilteredSubTypes.DataSet := nil;
         FFilteredSubTypeDS := TevDataSet.Create(DM_COMPANY.CO_BENEFIT_SUBTYPE);
         dsFilteredSubTypes.DataSet := FFilteredSubTypeDS.vclDataSet;

         if not FFilteredSubTypeDS.Locate('CO_BENEFIT_SUBTYPE_NBR', key_val, []) then
           FFilteredSubTypeDS.First;

         cbSubTypeDescription.LookupTable := FFilteredSubTypeDS.vclDataSet;
         cbSubTypeDescription.LookupField := 'CO_BENEFIT_SUBTYPE_NBR';

      finally
         DM_COMPANY.CO_BENEFIT_SUBTYPE.EnableControls;
      end;

     if AllTable then
     Begin
      DM_COMPANY.CO_BENEFIT_RATES.DisableControls;
      try
       key_val := DM_COMPANY.CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE_NBR.AsInteger;

       DM_COMPANY.CO_BENEFIT_RATES.IndexFieldNames := 'CO_BENEFITS_NBR;CO_BENEFIT_SUBTYPE_NBR;PERIOD_BEGIN';
       DM_COMPANY.CO_BENEFIT_RATES.SetRange([DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']], [DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR']]);

       DM_COMPANY.CO_BENEFIT_RATES.Filter := 'PERIOD_END >= ''' + DateToStr(dtBenefitAsOfDate.Date) + '''';
       DM_COMPANY.CO_BENEFIT_RATES.Filtered := True;
       DM_COMPANY.CO_BENEFIT_RATES.first;

       dsFilteredRates.DataSet := nil;
       FFilteredRatesDS := TevDataSet.Create(DM_COMPANY.CO_BENEFIT_RATES);
       dsFilteredRates.DataSet := FFilteredRatesDS.vclDataSet;

       if not FFilteredRatesDS.Locate('CO_BENEFIT_RATES_NBR', key_val, []) then
         FFilteredRatesDS.First;
      finally
       DM_COMPANY.CO_BENEFIT_RATES.EnableControls;
       if (FFilteredRatesDS.RecordCount = 0) then
          ClearRateTabFields;
       if FFilteredSubTypeDS.RecordCount = 0 then
          edSubTypeDescription.Text := '';

      end;
     end;

     cbSubTypeDescription.RefreshDisplay;
     edClientEDG.RefreshDisplay;
  end;
end;

procedure TEDIT_CO_BENEFITS.dedtBenefit_NameChange(Sender: TObject);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and
   (not wwdsDetail.DataSet.fieldByName('BENEFIT_NAME').isnull) then
     lblBenefitAsOfDate.Caption := '~Display "' +  TRim(wwdsDetail.DataSet.fieldByName('BENEFIT_NAME').AsString) + '" Benefit Rates As Of       '
   else
     lblBenefitAsOfDate.Caption := '~Display Benefit Rates As Of: ';
  lblBenefitAsOfDate.Width := Length( lblBenefitAsOfDate.Caption) * 7;
  dtBenefitAsOfDate.Left := Length( lblBenefitAsOfDate.Caption) * 6;
  if PageControl1.ActivePage = tsSubTypeRates then
     SetBenefitRateFilter;

end;

procedure TEDIT_CO_BENEFITS.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePage = tsSubTypeRates then
     SetBenefitRateFilter;

  inherited;

  if PageControl1.ActivePage = tshtDetail then
     cbBenefitCategoryChange(Sender);
end;

procedure TEDIT_CO_BENEFITS.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if PageControl1.ActivePage = tshtDetail then
    AllowChange := not (wwdsDetail.DataSet.State in [dsInsert,dsEdit]);

  if PageControl1.ActivePage = tsSubTypeRates then
  begin
    AllowChange := (not (dsRates.DataSet.State in [dsInsert,dsEdit])) and
                    (not (dsSubtype.DataSet.State in [dsInsert,dsEdit]));
  end;
  if not AllowChange then
     EvErrMessage('You must post or cancel changes.');
end;

procedure TEDIT_CO_BENEFITS.dtBenefitAsOfDateChange(Sender: TObject);
begin
  inherited;
  if dtBenefitAsOfDate.Focused then
     SetBenefitRateFilter;
end;

procedure TEDIT_CO_BENEFITS.SetEnabledForRate;
begin
    pcRateType.Enabled := not (dsSubtype.DataSet.State in [dsInsert,dsEdit]);
    if pcRateType.Enabled then
       pcRateType.Enabled := (not DM_COMPANY.CO_BENEFIT_SUBTYPE.IsDataModified);
                                                    // (not (Owner as TFramePackageTmpl).btnNavAbort.Enabled);

    SubTypeMiniNavigationFrame.Enabled := pcRateType.Enabled;
    grRates.Enabled := pcRateType.Enabled;
    dtBenefitAsOfDate.Enabled:= pcRateType.Enabled;
end;


procedure TEDIT_CO_BENEFITS.dsSubtypeStateChange(Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then exit;
  if assigned(FFilteredSubTypeDS) then
  begin
    if DM_COMPANY.CO_BENEFIT_SUBTYPE.State = dsInsert then
    begin
       DM_COMPANY.CO_BENEFIT_SUBTYPE.CO_BENEFITS_NBR.AsInteger := DM_COMPANY.CO_BENEFITS.CO_BENEFITS_NBR.AsInteger;
       FFilteredSubTypeDS.Insert;
    end
    else if DM_COMPANY.CO_BENEFIT_SUBTYPE.State = dsEdit then
    begin
      FFilteredSubTypeDS.Edit;
    end;

    SetEnabledForRate;

  end;
end;

procedure TEDIT_CO_BENEFITS.dsRatesStateChange(Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then exit;
  if assigned(FFilteredRatesDS) then
  begin
    if DM_COMPANY.CO_BENEFIT_RATES.State = dsInsert then
    begin
       DM_COMPANY.CO_BENEFIT_RATES.CO_BENEFIT_SUBTYPE_NBR.AsInteger := DM_COMPANY.CO_BENEFIT_SUBTYPE.CO_BENEFIT_SUBTYPE_NBR.AsInteger;

       FFilteredRatesDS.Insert;
    end
    else if DM_COMPANY.CO_BENEFIT_RATES.State = dsEdit then
    begin
      FFilteredRatesDS.Edit;
    end;
  end;
  grRates.Enabled := not (dsRates.DataSet.State in [dsInsert,dsEdit]);
  grdSubtype.Enabled := not (dsRates.DataSet.State in [dsInsert,dsEdit]);
  fpDescriptionDetail.Enabled := grdSubtype.Enabled;
  dtBenefitAsOfDate.Enabled := not (dsRates.DataSet.State in [dsInsert,dsEdit]);
end;

procedure TEDIT_CO_BENEFITS.dsRatesDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;

  lblEndDate.Caption := 'End Date        ';
  if (LowerCase(Trim(cbSubTypeDescription.Text)) = 'employee defined') then
      lblEndDate.Caption := '~End Date        ';

  if Assigned(dsRates.DataSet) and dsRates.DataSet.Active and (dsRates.DataSet.State = dsBrowse) then
  begin
    if (not dsRates.DataSet.FieldByName('RATE_TYPE').IsNull) and
       (dsRates.DataSet.FieldByName('RATE_TYPE').AsString  = BENEFIT_RATES_RATE_TYPE_PERCENT) then
    begin
     if pcRateType.ActivePage <> tsPercent then
        pcRateType.ActivePage := tsPercent;
    end
    else
     if pcRateType.ActivePage <> tsAmount then
        pcRateType.ActivePage := tsAmount;

    pnlRateHeader.Parent := pcRateType.ActivePage;
  end;
end;

procedure TEDIT_CO_BENEFITS.pcRateTypeChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  AllowChange := dsRates.DataSet.State = dsBrowse;
end;


procedure TEDIT_CO_BENEFITS.pcRateTypeChange(Sender: TObject);
begin
  inherited;
  //move the header panel around
  pnlRateHeader.Parent := pcRateType.ActivePage;
end;

procedure TEDIT_CO_BENEFITS.cbSubTypeDescriptionBeforeDropDown(
  Sender: TObject);
begin
  inherited;
  SetBenefitRateFilter(false);
end;

constructor TEDIT_CO_BENEFITS.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNMENTS
  pnlSubbrowse.Parent := fpBenefitsRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_BENEFITS.fpBenefitsRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpBenefitsRight.Width - 40;
  pnlSubbrowse.Height := fpBenefitsRight.Height - 65;
end;

procedure TEDIT_CO_BENEFITS.navDescriptionSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  if DM_COMPANY.CO_BENEFIT_SUBTYPE.State = dsBrowse then
     edSubTypeDescription.SetFocus;
  navDescription.InsertRecordExecute(Sender);
end;

procedure TEDIT_CO_BENEFITS.navDescriptionSpeedButton2Click(
  Sender: TObject);

  function CheckSubtypeInUSe:boolean;
   var
     s: string;
     Q: IEvQuery;
  begin
    s:= 'select count(CO_BENEFIT_RATES_NBR) from CO_BENEFIT_RATES ' +
           ' where {AsOfNow<CO_BENEFIT_RATES>} and CO_BENEFIT_SUBTYPE_NBR = ' + IntToStr(DM_CLIENT.CO_BENEFIT_SUBTYPE.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger);

    Q := TevQuery.Create(s);
    Q.Execute;
    result := Q.Result.Fields[0].AsInteger > 0;
  end;
begin

  if CheckSubtypeInUSe then
     raise EUpdateError.CreateHelp('This Rate Description has attached records. Please delete them first.', IDH_ConsistencyViolation);

  inherited;
  navDescription.DeleteRecordExecute(Sender);
end;

procedure TEDIT_CO_BENEFITS.edPeriodEndExit(Sender: TObject);
begin
  inherited;
  //fix the tab order on the percent tab.
  if pcRateType.ActivePageIndex = 1 then edClientEDG.SetFocus;
end;

procedure TEDIT_CO_BENEFITS.StateMiniNavigatorSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  if DM_COMPANY.CO_BENEFIT_STATES.State = dsBrowse then
     cbBenefitState.SetFocus;

  StateMiniNavigator.InsertRecordExecute(Sender);

  if DM_COMPANY.CO_BENEFIT_STATES.State = dsInsert then
     DM_COMPANY.CO_BENEFIT_STATES.CO_BENEFITS_NBR.AsInteger := DM_COMPANY.CO_BENEFITS.CO_BENEFITS_NBR.AsInteger;
end;

procedure TEDIT_CO_BENEFITS.StateMiniNavigatorSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  StateMiniNavigator.DeleteRecordExecute(Sender);
end;

procedure TEDIT_CO_BENEFITS.dsFilteredSubTypesDataChange(Sender: TObject; Field: TField);
begin
  if DM_COMPANY.CO_BENEFIT_SUBTYPE.State = dsBrowse then
    DM_COMPANY.CO_BENEFIT_SUBTYPE.FindKey([DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'],FFilteredSubTypeDS.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger]);
end;

procedure TEDIT_CO_BENEFITS.dsFilteredRatesDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_COMPANY.CO_BENEFIT_RATES.State = dsBrowse then
    DM_COMPANY.CO_BENEFIT_RATES.Locate('CO_BENEFIT_RATES_NBR',FFilteredRatesDS.FieldByName('CO_BENEFIT_RATES_NBR').AsInteger, []);
end;

procedure TEDIT_CO_BENEFITS.dsSubtypeUpdateData(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  if assigned(FFilteredSubTypeDS) and (not bRateCopy) then
    if DM_COMPANY.CO_BENEFIT_SUBTYPE.State in [dsInsert, dsEdit] then
    begin
       for i := 0 to DM_COMPANY.CO_BENEFIT_SUBTYPE.FieldCount - 1 do
         FFilteredSubTypeDS.Fields[i].Value := DM_COMPANY.CO_BENEFIT_SUBTYPE.Fields[i].Value;
    end;
end;

procedure TEDIT_CO_BENEFITS.dsRatesUpdateData(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  if assigned(FFilteredRatesDS) and (not bRateCopy) then
    if DM_COMPANY.CO_BENEFIT_RATES.State in [dsInsert, dsEdit] then
    begin
       for i := 0 to DM_COMPANY.CO_BENEFIT_RATES.FieldCount - 1 do
         FFilteredRatesDS.Fields[i].Value := DM_COMPANY.CO_BENEFIT_RATES.Fields[i].Value;
    end;
end;

procedure TEDIT_CO_BENEFITS.DoAfterPost;
begin
  inherited;
  SetBenefitRateFilter;
end;

procedure TEDIT_CO_BENEFITS.DoOnCancel;
begin
  inherited;
  SetBenefitRateFilter;
end;

procedure TEDIT_CO_BENEFITS.cbBenefitCategoryChange(Sender: TObject);
begin
  inherited;

  if Assigned(QCompanyCategory) then
  begin
    if (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
        wwdsDetail.DataSet.UpdateRecord;

    if (not wwdsDetail.DataSet.FieldByName('CO_BENEFIT_CATEGORY_NBR').IsNull) and
        QCompanyCategory.Result.Locate('CO_BENEFIT_CATEGORY_NBR',wwdsDetail.DataSet.FieldByName('CO_BENEFIT_CATEGORY_NBR').Value, []) and
        (QCompanyCategory.Result.FieldByName('CATEGORY_TYPE').AsString <> BENEFIT_CATEGORY_TYPE_HEALTH) then
    begin
       if (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
          evAllowHSA.ItemIndex := 1;
       evAllowHSA.SecurityRO := true
    end
    else
       evAllowHSA.SecurityRO := GetIfReadOnly;

    if (cbBenefitCategory.Value <> '') and
       (not wwdsDetail.DataSet.FieldByName('CO_BENEFIT_CATEGORY_NBR').IsNull) and
        QCompanyCategory.Result.Locate('CO_BENEFIT_CATEGORY_NBR',wwdsDetail.DataSet.FieldByName('CO_BENEFIT_CATEGORY_NBR').Value, []) then
    begin
        edTaxableFringe.Enabled := True;
        if (QCompanyCategory.Result.FieldByName('CATEGORY_TYPE').AsString <> '') and
           (QCompanyCategory.Result.FieldByName('CATEGORY_TYPE').AsString[1] in [
                 BENEFIT_CATEGORY_TYPE_DENTAL,  BENEFIT_CATEGORY_TYPE_ADDITIONAL_DENTAL_1,
                 BENEFIT_CATEGORY_TYPE_ADDITIONAL_DENTAL_2,  BENEFIT_CATEGORY_TYPE_ADDITIONAL_DENTAL_3,
                 BENEFIT_CATEGORY_TYPE_HEALTH,  BENEFIT_CATEGORY_TYPE_ADDITIONAL_HEALTH_1,
                 BENEFIT_CATEGORY_TYPE_ADDITIONAL_HEALTH_2,  BENEFIT_CATEGORY_TYPE_ADDITIONAL_HEALTH_3,
                 BENEFIT_CATEGORY_TYPE_VISION,  BENEFIT_CATEGORY_TYPE_ADDITIONAL_VISION_1,
                 BENEFIT_CATEGORY_TYPE_ADDITIONAL_VISION_2, BENEFIT_CATEGORY_TYPE_ADDITIONAL_VISION_3,
                 BENEFIT_CATEGORY_TYPE_PENSION ]) then
              lblTaxable.Caption := 'Taxable Fringe '
        else
        if QCompanyCategory.Result.FieldByName('CATEGORY_TYPE').AsString =  BENEFIT_CATEGORY_TYPE_HSA then
              lblTaxable.Caption := 'Optional'
        else
        if QCompanyCategory.Result.FieldByName('CATEGORY_TYPE').AsString =  BENEFIT_CATEGORY_TYPE_POST_TAX_PENSION then
              lblTaxable.Caption := 'Catchup'
        else
            edTaxableFringe.Enabled := false;
    end
    else
      edTaxableFringe.Enabled := false;
  end;

end;

procedure TEDIT_CO_BENEFITS.cbBenefitCategoryCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
    if (not edTaxableFringe.Enabled) and
       (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
         wwdsDetail.DataSet.FieldByName('TAXABLE_FRINGE').Value := null;
end;

procedure TEDIT_CO_BENEFITS.CopyBenefits;
var
  CurClient, CurCompany: integer;
  s, sClientCond, sCompanyCond,
  sRateIndex, sBenefitCond,sSubTypeCond,sRatesCond, sStatesCond, sCategoryCond, sdesc: string;
  qBenefits, qSubTypes, qRates, qStates, qEDCodes, qEDGroups: IevQuery;
  pCATEGORY_TYPE, pEE_DEDUCTION_NBR, pER_DEDUCTION_NBR,
  pEE_COBRA_NBR, pER_COBRA_NBR, pTAXABLE_FRINGE, pPENSION_CATCH_UP_NBR : Variant;


  procedure DoCopyBenefits;
  var
    k: integer;

    procedure DoCopyBenefitsTo(client, company: integer );
    var
      fn,fnr: string;
      F, FR: TField;

      Procedure DoCopyBenefitStates;
      begin
        if Assigned(qStates) then
        begin
          qStates.Result.vclDataSet.First;
          while not qStates.Result.vclDataSet.Eof do
          begin
            if DM_COMPANY.CO_STATES.Locate('SY_STATES_NBR',qStates.Result.vclDataSet['SY_STATES_NBR'],[]) then
            begin
              DM_COMPANY.CO_BENEFIT_STATES.Insert;
              DM_COMPANY.CO_BENEFIT_STATES['SY_STATES_NBR'] := qStates.Result.vclDataSet['SY_STATES_NBR'];
              DM_COMPANY.CO_BENEFIT_STATES['CO_BENEFITS_NBR'] := DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'];
              DM_COMPANY.CO_BENEFIT_STATES.Post;
            end;
            qStates.Result.vclDataSet.Next;
           end;
        end;
      end;

      Procedure AddBenefitRates;
      var
       n : integer;
      Begin
        DM_COMPANY.CO_BENEFIT_RATES.Insert;

        for n := 0 to qRates.Result.vclDataSet.Fields.Count - 1 do
        begin
          fnr := qRates.Result.vclDataSet.Fields[n].FieldName;
          FR := DM_COMPANY.CO_BENEFIT_RATES.FindField(fnr);

          if Assigned(FR) and (fnr <> 'EFFECTIVE_DATE') and (fnr <> 'EFFECTIVE_UNTIL') and (fnr <> 'REC_VERSION')
               and (fnr <> 'CO_BENEFIT_SUBTYPE_NBR') and (fnr <> 'CO_BENEFIT_RATES_NBR') and (fnr <> 'CO_NBR')
               and (fnr <> 'CO_BENEFITS_NBR') and (fnr <> 'CL_E_D_GROUPS_NBR') and (FR.FieldKind = fkData ) then
                FR.Value := qRates.Result.vclDataSet[fnr];
        end;

        if not qRates.Result.FieldByName('NAME').IsNull then
        begin
         if qEDGroups.Result.Locate('NAME', qRates.Result.FieldByName('NAME').AsString, []) then
            DM_COMPANY.CO_BENEFIT_RATES['CL_E_D_GROUPS_NBR'] := qEDGroups.Result['CL_E_D_GROUPS_NBR']
         else
             CheckCondition(false, 'ED Group (' + qRates.Result.FieldByName('NAME').AsString + ') isn''t setup on CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString);
        end;

        DM_COMPANY.CO_BENEFIT_RATES['CO_BENEFIT_SUBTYPE_NBR'] := DM_COMPANY.CO_BENEFIT_SUBTYPE['CO_BENEFIT_SUBTYPE_NBR'];
        DM_COMPANY.CO_BENEFIT_RATES.Post;
      End;

      Procedure DoCopyBenefitRates;
      Begin
        if DM_COMPANY.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFITS_NBR;DESCRIPTION',
                                        VarArrayOf([DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'],qSubTypes.Result.vclDataSet['DESCRIPTION']]),[]) and
           Assigned(qRates) then
        begin
          qRates.Result.vclDataSet.SetRange([qSubTypes.Result.vclDataSet['CO_BENEFIT_SUBTYPE_NBR']], [qSubTypes.Result.vclDataSet['CO_BENEFIT_SUBTYPE_NBR']]);
          qRates.Result.vclDataSet.First;
          while not qRates.Result.vclDataSet.Eof do
          begin
            AddBenefitRates;

            qRates.Result.vclDataSet.Next;
          end;
        end;
      End;

      Procedure AddBenefitSubType;
      var
       m: integer;
      begin
        DM_COMPANY.CO_BENEFIT_SUBTYPE.Append;

        for m := 0 to qSubTypes.Result.vclDataSet.Fields.Count - 1 do
        begin
          fn := qSubTypes.Result.vclDataSet.Fields[m].FieldName;
          F := DM_COMPANY.CO_BENEFIT_SUBTYPE.FindField(fn);

          if Assigned(F) and  (fn <> 'EFFECTIVE_DATE') and (fn <> 'EFFECTIVE_UNTIL') and (fn <> 'REC_VERSION')
               and ( fn <> 'CO_BENEFIT_SUBTYPE_NBR' ) and ( fn <> 'CO_BENEFITS_NBR' ) and (fn <> 'CO_NBR') and (fn <> 'CO_BENEFIT_AGE_BAND_DESC_NBR' )
               and (F.FieldKind = fkData ) then
                F.Value := qSubTypes.Result.vclDataSet[fn];
        end;

        DM_COMPANY.CO_BENEFIT_SUBTYPE['CO_BENEFITS_NBR'] := DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'];
        DM_COMPANY.CO_BENEFIT_SUBTYPE.Post;

        DoCopyBenefitRates;
      end;

      Procedure DoCopyBenefitSubType;
      Begin
        if Assigned(qSubTypes) then
        begin
          qSubTypes.Result.vclDataSet.First;
          while not qSubTypes.Result.vclDataSet.Eof do
          begin
            AddBenefitSubType;

            qSubTypes.Result.vclDataSet.Next;
          end;
        end;
      End;

      Procedure doCopyCoBenefit;
      var
        l: integer;
      Begin
        for l := 0 to qBenefits.Result.vclDataSet.Fields.Count - 1 do
        begin
          fn := qBenefits.Result.vclDataSet.Fields[l].FieldName;
          F := DM_COMPANY.CO_BENEFITS.FindField(fn);

          if Assigned(F) and  (fn <> 'EFFECTIVE_DATE') and (fn <> 'EFFECTIVE_UNTIL') and (fn <> 'REC_VERSION')
               and (fn <> 'CO_BENEFIT_CATEGORY_NBR')
               and (fn <> 'CL_AGENCY_NBR') and (fn <> 'EE_DEDUCTION_NBR') and (fn <> 'ER_DEDUCTION_NBR')
               and (fn <> 'EE_COBRA_NBR') and (fn <> 'ER_COBRA_NBR') and (fn <> 'TAXABLE_FRINGE')
               and (fn <> 'CO_NBR') and ( fn <> 'CO_BENEFITS_NBR') and (F.FieldKind = fkData ) then
                F.Value := qBenefits.Result.vclDataSet[fn];
        end;
      End;

    Begin
      DM_COMPANY.CO.Close;
      DM_COMPANY.CO_BENEFIT_SUBTYPE.Close;
      DM_COMPANY.CO_BENEFIT_RATES.Close;
      DM_COMPANY.CO_BENEFIT_STATES.Close;

      ctx_DataAccess.OpenClient( client );
      DM_COMPANY.CO.DataRequired('CO_NBR=' + intToStr(company) );
      DM_COMPANY.CO_BENEFITS.DataRequired('CO_NBR=' + intToStr(company) );
      DM_COMPANY.CO_BENEFIT_SUBTYPE.DataRequired('CO_NBR=' + intToStr(company) );
      DM_COMPANY.CO_BENEFIT_RATES.DataRequired('CO_NBR=' + intToStr(company) );
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + intToStr(company) );
      DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired('CO_NBR=' + intToStr(company));

      s:=  'select a.CO_NBR, a.CO_E_D_CODES_NBR, b.CUSTOM_E_D_CODE_NUMBER ' +
              'from CO_E_D_CODES a, CL_E_DS b where {AsOfNow<a>} and {AsOfNow<b>} and ' +
              'a.CL_E_DS_NBR = b.CL_E_DS_NBR and a.CO_NBR = :coNbr ';
      qEDCodes := TevQuery.Create(s);
      qEDCodes.Params.AddValue('coNbr', company);
      qEDCodes.Execute;

      s:=  'select a.CL_E_D_GROUPS_NBR, a.NAME  from CL_E_D_GROUPS a order by a.Name ';
      qEDGroups := TevQuery.Create(s);
      qEDGroups.Execute;
      qEDGroups.Result.IndexFieldNames := 'NAME';

      if not DM_COMPANY.CO_BENEFIT_CATEGORY.Locate('CATEGORY_TYPE',pCATEGORY_TYPE,[]) then
         raise EUpdateError.CreateHelp('Category does not exist!', IDH_ConsistencyViolation);

      DM_COMPANY.CO.Open;
      DM_COMPANY.CO_BENEFIT_SUBTYPE.Open;
      DM_COMPANY.CO_BENEFIT_RATES.Open;
      DM_COMPANY.CO_BENEFIT_STATES.Open;

      qBenefits.Result.vclDataSet.First;
      if not DM_COMPANY.CO_BENEFITS.Locate('BENEFIT_NAME', qBenefits.Result.FieldByName('BENEFIT_NAME').AsString, []) then
      begin
        DM_COMPANY.CO_BENEFITS.Insert;

        doCopyCoBenefit;

        DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'] := DM_COMPANY.CO_BENEFIT_CATEGORY['CO_BENEFIT_CATEGORY_NBR'];

        if CurClient = client then
           DM_COMPANY.CO_BENEFITS['CL_AGENCY_NBR'] := qBenefits.Result.vclDataSet['CL_AGENCY_NBR'];

        if not VarIsNull(pEE_DEDUCTION_NBR) then
        begin
          if qEDCodes.Result.vclDataSet.Locate('CUSTOM_E_D_CODE_NUMBER', pEE_DEDUCTION_NBR, []) then
             DM_COMPANY.CO_BENEFITS['EE_DEDUCTION_NBR'] := qEDCodes.Result.vclDataSet['CO_E_D_CODES_NBR']
          else
             EvMessage('ED ' + VarToStr(pEE_DEDUCTION_NBR) + ' didn''t copy to CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' because it isn''t attached to the company.', mtWarning, [mbOK]);
        end;
        if not VarIsNull(pER_DEDUCTION_NBR) then
        begin
          if qEDCodes.Result.vclDataSet.Locate('CUSTOM_E_D_CODE_NUMBER', pER_DEDUCTION_NBR, []) then
             DM_COMPANY.CO_BENEFITS['ER_DEDUCTION_NBR'] := qEDCodes.Result.vclDataSet['CO_E_D_CODES_NBR']
          else
             EvMessage('ED ' + VarToStr(pER_DEDUCTION_NBR) + ' didn''t copy to CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' because it isn''t attached to the company.', mtWarning, [mbOK]);
        end;
        if not VarIsNull(pEE_COBRA_NBR) then
        begin
         if qEDCodes.Result.vclDataSet.Locate('CUSTOM_E_D_CODE_NUMBER', pEE_COBRA_NBR, []) then
             DM_COMPANY.CO_BENEFITS['EE_COBRA_NBR'] := qEDCodes.Result.vclDataSet['CO_E_D_CODES_NBR']
          else
             EvMessage('ED ' + VarToStr(pEE_COBRA_NBR) + ' didn''t copy to CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' because it isn''t attached to the company.', mtWarning, [mbOK]);
        end;
        if not VarIsNull(pER_COBRA_NBR) then
        begin
          if qEDCodes.Result.vclDataSet.Locate('CUSTOM_E_D_CODE_NUMBER', pER_COBRA_NBR, []) then
             DM_COMPANY.CO_BENEFITS['ER_COBRA_NBR'] := qEDCodes.Result.vclDataSet['CO_E_D_CODES_NBR']
          else
             EvMessage('ED ' + VarToStr(pER_COBRA_NBR) + ' didn''t copy to CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' because it isn''t attached to the company.', mtWarning, [mbOK]);
        end;
        if not VarIsNull(pTAXABLE_FRINGE) then
        begin
         if qEDCodes.Result.vclDataSet.Locate('CUSTOM_E_D_CODE_NUMBER', pTAXABLE_FRINGE, []) then
            DM_COMPANY.CO_BENEFITS['TAXABLE_FRINGE'] := qEDCodes.Result.vclDataSet['CO_E_D_CODES_NBR']
          else
             EvMessage('ED ' + VarToStr(pTAXABLE_FRINGE) + ' didn''t copy to CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' because it isn''t attached to the company.', mtWarning, [mbOK]);
        end;
        if not VarIsNull(pPENSION_CATCH_UP_NBR) then
        begin
         if qEDCodes.Result.vclDataSet.Locate('CUSTOM_E_D_CODE_NUMBER', pPENSION_CATCH_UP_NBR, []) then
            DM_COMPANY.CO_BENEFITS['PENSION_CATCH_UP_NBR'] := qEDCodes.Result.vclDataSet['CO_E_D_CODES_NBR']
          else
             EvMessage('ED ' + VarToStr(pPENSION_CATCH_UP_NBR) + ' didn''t copy to CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' because it isn''t attached to the company.', mtWarning, [mbOK]);
        end;

        DM_COMPANY.CO_BENEFITS['CO_NBR'] := company;
        DM_COMPANY.CO_BENEFITS.Post;

        DoCopyBenefitStates;
        DoCopyBenefitSubType;

      end
      else
      begin

        DM_COMPANY.CO_BENEFITS.Edit;
        doCopyCoBenefit;
        DM_COMPANY.CO_BENEFITS.Post;

        if Assigned(qSubTypes) then
        begin
          DM_COMPANY.CO_BENEFIT_RATES.IndexFieldNames := 'CO_BENEFIT_SUBTYPE_NBR;PERIOD_BEGIN';
          qSubTypes.Result.vclDataSet.First;
          while not qSubTypes.Result.vclDataSet.Eof do
          begin
            if not DM_COMPANY.CO_BENEFIT_SUBTYPE.Locate('CO_BENEFITS_NBR;DESCRIPTION',
               VarArrayOf([DM_COMPANY.CO_BENEFITS['CO_BENEFITS_NBR'], qSubTypes.Result.FieldByName('DESCRIPTION').AsString]), []) then
              AddBenefitSubType
            else
            begin
              if Assigned(qRates) then
              begin
                DM_COMPANY.CO_BENEFIT_RATES.SetRange([DM_COMPANY.CO_BENEFIT_SUBTYPE['CO_BENEFIT_SUBTYPE_NBR']], [DM_COMPANY.CO_BENEFIT_SUBTYPE['CO_BENEFIT_SUBTYPE_NBR']]);
                try
                    DM_COMPANY.CO_BENEFIT_RATES.last;

                    qRates.Result.vclDataSet.SetRange([qSubTypes.Result.vclDataSet['CO_BENEFIT_SUBTYPE_NBR']], [qSubTypes.Result.vclDataSet['CO_BENEFIT_SUBTYPE_NBR']]);
                    qRates.Result.vclDataSet.First;
                    while not qRates.Result.vclDataSet.Eof do
                    begin
                      if qRates.Result.vclDataSet.FieldByName('PERIOD_BEGIN').AsDateTime >
                           DM_COMPANY.CO_BENEFIT_RATES.FieldByName('PERIOD_END').AsDateTime then
                      begin
                        AddBenefitRates;
                        DM_COMPANY.CO_BENEFIT_RATES.last;
                      end;
                      qRates.Result.vclDataSet.Next;
                    end;

                finally
                  DM_COMPANY.CO_BENEFIT_RATES.CancelRange;
                end;
              end;
            end;

            qSubTypes.Result.vclDataSet.Next;
          end;
        end;
      end;

      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BENEFITS, DM_COMPANY.CO_BENEFIT_STATES, DM_COMPANY.CO_BENEFIT_SUBTYPE, DM_COMPANY.CO_BENEFIT_RATES]);
    end;
  begin
    with TCompanyChoiceQuery.Create( Self ) do
    try
      SetFilter( 'CUSTOM_COMPANY_NUMBER<>'''+DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'''' );
      sdesc := 'benefit';
      Caption :=format('Select the companies you want this %s copied to',[sdesc]);
      ConfirmAllMessage := format('This operation will copy the %s to all clients and companies. Are you sure you want to do this?',[sdesc]);

      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
         DM_TEMPORARY.TMP_CO.DisableControls;
         try
           for k := 0 to dgChoiceList.SelectedList.Count - 1 do
           begin
                cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                DoCopyBenefitsTo(cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger );
           end;
          finally
           DM_TEMPORARY.TMP_CO.EnableControls;
          end;
      end;
    finally
      Free;
    end;
  end;
begin

  CurClient := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  CurCompany := DM_COMPANY.CO['CO_NBR'];
  sClientCond := DM_CLIENT.CL.RetrieveCondition;
  sCompanyCond := DM_COMPANY.CO.RetrieveCondition;
  sBenefitCond := DM_COMPANY.CO_BENEFITS.RetrieveCondition;
  sSubTypeCond := DM_COMPANY.CO_BENEFIT_SUBTYPE.RetrieveCondition;
  sRatesCond := DM_COMPANY.CO_BENEFIT_RATES.RetrieveCondition;
  sStatesCond := DM_COMPANY.CO_BENEFIT_STATES.RetrieveCondition;
  sCategoryCond := DM_COMPANY.CO_BENEFIT_CATEGORY.RetrieveCondition;
  DM_CLIENT.CL_E_DS.DataRequired('ALL');
  DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR=' + intToStr(CurCompany));
  sRateIndex:= DM_COMPANY.CO_BENEFIT_RATES.IndexName;
  try
    s:= 'select * from CO_BENEFITS where {AsOfNow<CO_BENEFITS>} and CO_BENEFITS_NBR = :pNbr ';
    qBenefits := TevQuery.Create(s);
    qBenefits.Params.AddValue('pNbr', wwdsDetail.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger);
    qBenefits.Execute;

    pCATEGORY_TYPE := DM_COMPANY.CO_BENEFIT_CATEGORY.Lookup('CO_BENEFIT_CATEGORY_NBR', DM_COMPANY.CO_BENEFITS['CO_BENEFIT_CATEGORY_NBR'], 'CATEGORY_TYPE');

    pEE_DEDUCTION_NBR := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DM_COMPANY.CO_E_D_CODES.Lookup('CO_E_D_CODES_NBR', DM_COMPANY.CO_BENEFITS['EE_DEDUCTION_NBR'], 'CL_E_DS_NBR'), 'CUSTOM_E_D_CODE_NUMBER');
    pER_DEDUCTION_NBR := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DM_COMPANY.CO_E_D_CODES.Lookup('CO_E_D_CODES_NBR', DM_COMPANY.CO_BENEFITS['ER_DEDUCTION_NBR'], 'CL_E_DS_NBR'), 'CUSTOM_E_D_CODE_NUMBER');
    pEE_COBRA_NBR     := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DM_COMPANY.CO_E_D_CODES.Lookup('CO_E_D_CODES_NBR', DM_COMPANY.CO_BENEFITS['EE_COBRA_NBR'], 'CL_E_DS_NBR'), 'CUSTOM_E_D_CODE_NUMBER');
    pER_COBRA_NBR     := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DM_COMPANY.CO_E_D_CODES.Lookup('CO_E_D_CODES_NBR', DM_COMPANY.CO_BENEFITS['ER_COBRA_NBR'], 'CL_E_DS_NBR'), 'CUSTOM_E_D_CODE_NUMBER');
    pTAXABLE_FRINGE   := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DM_COMPANY.CO_E_D_CODES.Lookup('CO_E_D_CODES_NBR', DM_COMPANY.CO_BENEFITS['TAXABLE_FRINGE'], 'CL_E_DS_NBR'), 'CUSTOM_E_D_CODE_NUMBER');
    pPENSION_CATCH_UP_NBR  := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DM_COMPANY.CO_E_D_CODES.Lookup('CO_E_D_CODES_NBR', DM_COMPANY.CO_BENEFITS['PENSION_CATCH_UP_NBR'], 'CL_E_DS_NBR'), 'CUSTOM_E_D_CODE_NUMBER');

    s:= 'select * from CO_BENEFIT_STATES where {AsOfNow<CO_BENEFIT_STATES>} and CO_BENEFITS_NBR = :pNbr ';
    qStates := TevQuery.Create(s);
    qStates.Params.AddValue('pNbr', wwdsDetail.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger);
    qStates.Execute;

    s:= 'select * from CO_BENEFIT_SUBTYPE where {AsOfNow<CO_BENEFIT_SUBTYPE>} and CO_BENEFITS_NBR = :pNbr ';
    qSubTypes := TevQuery.Create(s);
    qSubTypes.Params.AddValue('pNbr', wwdsDetail.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger);
    qSubTypes.Execute;

    if qSubTypes.Result.RecordCount > 0 then
    begin
      s:= 'select a.*, b.NAME  from CO_BENEFIT_RATES a ' +
             ' LEFT OUTER JOIN CL_E_D_GROUPS b on a.CL_E_D_GROUPS_NBR = b.CL_E_D_GROUPS_NBR ' +
             ' where {AsOfNow<a>} and a.CO_BENEFITS_NBR = :pNbr ';
      qRates := TevQuery.Create(s);
      qRates.Params.AddValue('pNbr', wwdsDetail.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger);
      qRates.Execute;
      qRates.Result.IndexFieldNames := 'CO_BENEFIT_SUBTYPE_NBR;PERIOD_BEGIN';
    end;

    DoCopyBenefits;

  finally
    ctx_DataAccess.OpenClient( CurClient );

    DM_CLIENT.CL.DataRequired(sClientCond );
    DM_COMPANY.CO.DataRequired(sCompanyCond );
    DM_CLIENT.CL_E_DS.DataRequired('ALL');
    DM_CLIENT.CL_AGENCY.DataRequired('ALL');
    DM_CLIENT.CL_E_D_GROUPS.DataRequired('ALL');
    DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR=' + intToStr(CurCompany));
    DM_COMPANY.CO_BENEFITS.DataRequired(sBenefitCond);
    DM_COMPANY.CO_BENEFIT_SUBTYPE.DataRequired(sSubTypeCond);
    DM_COMPANY.CO_BENEFIT_RATES.DataRequired(sRatesCond);
    DM_COMPANY.CO_BENEFIT_STATES.DataRequired(sStatesCond);
    DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired(sCategoryCond);
    DM_COMPANY.CO_BENEFIT_RATES.IndexName := sRateIndex;

    wwdsList.dataset.Locate('CO_NBR;CL_NBR', VarArrayOf([CurCompany, CurClient ]), []);
    DM_COMPANY.CO.Locate('CO_NBR',CurCompany,[]);
  end;
end;

procedure TEDIT_CO_BENEFITS.MenuItem3Click(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger > 0 then
     CopyBenefits;
end;

procedure TEDIT_CO_BENEFITS.fpRateDetailsMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;

  if (dsSubtype.DataSet.State in [dsInsert,dsEdit]) or DM_COMPANY.CO_BENEFIT_SUBTYPE.IsDataModified  then
     EvErrMessage('You must post or cancel changes.');
end;

procedure TEDIT_CO_BENEFITS.CopySelfInsured;
var
 CurrentNbr: integer;
 CurrentValue: Variant;

  procedure DoCopySelfInsured;
  var
    k: integer;
  begin
    with TBenefitChoice.Create( Self ) do
    try
      SetFilter('CO_BENEFITS_NBR <>  ' + DM_COMPANY.CO_BENEFITS.FieldByName('CO_BENEFITS_NBR').AsString );
      Caption := 'Select the Benefits that you want this Self Insured Plan copied to';
      ConfirmAllMessage := 'This operation will copy the Self Insured Plan to all Benefits. Are you sure you want to do this?';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        DM_COMPANY.CO_BENEFITS.DisableControls;
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            if DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR', cdsChoiceList.FieldByName('CO_BENEFITS_NBR').AsString, []) then
            begin
              DM_COMPANY.CO_BENEFITS.Edit;
              DM_COMPANY.CO_BENEFITS['CAFETERIA_PLAN'] := CurrentValue;
              DM_COMPANY.CO_BENEFITS.Post;
            end;
          end;
        finally
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BENEFITS]);
          DM_COMPANY.CO_BENEFITS.EnableControls;
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  CurrentNbr := DM_COMPANY.CO_BENEFITS.FieldByName('CO_BENEFITS_NBR').AsInteger;
  if (CurrentNbr > 0) then
  begin
    try
      CurrentValue:= DM_COMPANY.CO_BENEFITS['CAFETERIA_PLAN'];
      DoCopySelfInsured;
    finally
      DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR',CurrentNbr,[]);
    end;
  end;
end;


procedure TEDIT_CO_BENEFITS.mnSelfInsuredClick(Sender: TObject);
begin
  inherited;

  if (wwdsDetail.DataSet.State = dsBrowse) then
     CopySelfInsured;
end;

procedure TEDIT_CO_BENEFITS.CopyRate;
var
  CurClient, CurCompany, CurBenefit : integer;
  s, sClientCond, sCompanyCond,
  sRateIndex, sBenefitCond,sSubTypeCond,sRatesCond, sStatesCond, sCategoryCond, sdesc: string;
  qBenefits, qSubTypes, qRates, qEDGroups: IevQuery;

  procedure DoCopyRate;
  var
    k: integer;

    procedure DoCopyRateTo(client, company: integer );
    var
      fnr: string;
      FR: TField;

      Procedure AddBenefitRates;
      var
       n : integer;
      Begin
        DM_COMPANY.CO_BENEFIT_RATES.Insert;

        for n := 0 to qRates.Result.vclDataSet.Fields.Count - 1 do
        begin
          fnr := qRates.Result.vclDataSet.Fields[n].FieldName;
          FR := DM_COMPANY.CO_BENEFIT_RATES.FindField(fnr);

          if Assigned(FR) and (fnr <> 'EFFECTIVE_DATE') and (fnr <> 'EFFECTIVE_UNTIL') and (fnr <> 'REC_VERSION')
               and (fnr <> 'CO_BENEFIT_SUBTYPE_NBR') and (fnr <> 'CO_BENEFIT_RATES_NBR')
               and (fnr <> 'CO_BENEFITS_NBR') and (fnr <> 'CO_NBR') and (fnr <> 'CL_E_D_GROUPS_NBR') and (FR.FieldKind = fkData ) then
                FR.Value := qRates.Result.vclDataSet[fnr];
        end;

        if not qRates.Result.FieldByName('NAME').IsNull then
        begin
         if qEDGroups.Result.Locate('NAME', qRates.Result.FieldByName('NAME').AsString, []) then
            DM_COMPANY.CO_BENEFIT_RATES['CL_E_D_GROUPS_NBR'] := qEDGroups.Result['CL_E_D_GROUPS_NBR']
         else
             CheckCondition(false, 'ED Group (' + qRates.Result.FieldByName('NAME').AsString + ') isn''t setup on CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString);
        end;

        DM_COMPANY.CO_BENEFIT_RATES['CO_BENEFIT_SUBTYPE_NBR'] := DM_COMPANY.CO_BENEFIT_SUBTYPE['CO_BENEFIT_SUBTYPE_NBR'];
        DM_COMPANY.CO_BENEFIT_RATES.Post;
      End;

    Begin
      DM_COMPANY.CO_BENEFITS.Close;
      DM_COMPANY.CO_BENEFIT_SUBTYPE.Close;
      DM_COMPANY.CO_BENEFIT_RATES.Close;
      ctx_DataAccess.OpenClient( client );
      DM_COMPANY.CO.DataRequired('CO_NBR=' + intToStr(company));
      CheckCondition(DM_COMPANY.CO.RecordCount > 0, 'Company not found');

      qBenefits.Result.vclDataSet.First;
      DM_COMPANY.CO_BENEFITS.DataRequired('CO_NBR=' + intToStr(company) + ' and BENEFIT_NAME = ''' + qBenefits.Result.FieldByName('BENEFIT_NAME').AsString + '''');
      if DM_COMPANY.CO_BENEFITS.RecordCount = 0 then
         raise EUpdateError.CreateHelp('Benefit on CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' not found', IDH_ConsistencyViolation);

      if DM_COMPANY.CO_BENEFITS.RecordCount > 0 then
      begin
        DM_COMPANY.CO_BENEFIT_SUBTYPE.DataRequired('CO_BENEFITS_NBR =' + DM_COMPANY.CO_BENEFITS.fieldByName('CO_BENEFITS_NBR').AsString + ' and DESCRIPTION = ''' +  qSubTypes.Result.FieldByName('DESCRIPTION').AsString+ '''');
        if DM_COMPANY.CO_BENEFIT_SUBTYPE.RecordCount = 0 then
         raise EUpdateError.CreateHelp('Benefit Rate Description on CO# ' + DM_COMPANY.CO.fieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' not found.', IDH_ConsistencyViolation);

        if (DM_COMPANY.CO_BENEFIT_SUBTYPE.RecordCount > 0) then
        begin

          s:=  'select a.CL_E_D_GROUPS_NBR, a.NAME  from CL_E_D_GROUPS a order by a.Name ';
          qEDGroups := TevQuery.Create(s);
          qEDGroups.Execute;
          qEDGroups.Result.IndexFieldNames := 'NAME';

          DM_COMPANY.CO_BENEFIT_RATES.DataRequired('CO_BENEFIT_SUBTYPE_NBR=' + DM_COMPANY.CO_BENEFIT_SUBTYPE.fieldByName('CO_BENEFIT_SUBTYPE_NBR').AsString  );

          DM_COMPANY.CO_BENEFIT_RATES.IndexFieldNames := 'PERIOD_BEGIN';
          DM_COMPANY.CO_BENEFIT_RATES.last;

          if (qRates.Result.vclDataSet.FieldByName('PERIOD_BEGIN').AsDateTime >
              DM_COMPANY.CO_BENEFIT_RATES.FieldByName('PERIOD_END').AsDateTime) then
             AddBenefitRates;
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BENEFIT_RATES]);
        end;
      end;
    end;

  begin
    with TCompanyChoiceQuery.Create( Self ) do
    try
      SetFilter( 'CUSTOM_COMPANY_NUMBER<>'''+DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'''' );
      sdesc:= 'benefit rate';
      Caption := format('Select the companies you want this %s copied to',[sdesc]);
      ConfirmAllMessage := format('This operation will copy the %s to all clients and companies. Are you sure you want to do this?',[sdesc]);

      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
         DM_TEMPORARY.TMP_CO.DisableControls;
         try
           for k := 0 to dgChoiceList.SelectedList.Count - 1 do
           begin
                cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                DoCopyRateTo(cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger );
           end;
          finally
           DM_TEMPORARY.TMP_CO.EnableControls;
          end;
      end;
    finally
      Free;
    end;
  end;
begin

  CurClient := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  CurCompany := DM_COMPANY.CO['CO_NBR'];
  CurBenefit := DM_COMPANY.CO_BENEFITS.fieldByName('CO_BENEFITS_NBR').AsInteger;
  sClientCond := DM_CLIENT.CL.RetrieveCondition;
  sCompanyCond := DM_COMPANY.CO.RetrieveCondition;
  sBenefitCond := DM_COMPANY.CO_BENEFITS.RetrieveCondition;
  sSubTypeCond := DM_COMPANY.CO_BENEFIT_SUBTYPE.RetrieveCondition;
  sRatesCond := DM_COMPANY.CO_BENEFIT_RATES.RetrieveCondition;
  sStatesCond := DM_COMPANY.CO_BENEFIT_STATES.RetrieveCondition;
  sCategoryCond := DM_COMPANY.CO_BENEFIT_CATEGORY.RetrieveCondition;
  DM_CLIENT.CL_E_DS.DataRequired('ALL');
  DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR=' + intToStr(CurCompany));
  sRateIndex:= DM_COMPANY.CO_BENEFIT_RATES.IndexName;
  bRateCopy:= True;
  try
    s:= 'select * from CO_BENEFITS where {AsOfNow<CO_BENEFITS>} and CO_BENEFITS_NBR = :pNbr ';
    qBenefits := TevQuery.Create(s);
    qBenefits.Params.AddValue('pNbr', dsFilteredRates.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger);
    qBenefits.Execute;

    s:= 'select * from CO_BENEFIT_SUBTYPE where {AsOfNow<CO_BENEFIT_SUBTYPE>} and CO_BENEFIT_SUBTYPE_NBR = :pNbr ';
    qSubTypes := TevQuery.Create(s);
    qSubTypes.Params.AddValue('pNbr', dsFilteredRates.DataSet.FieldByName('CO_BENEFIT_SUBTYPE_NBR').AsInteger);
    qSubTypes.Execute;

    s:= 'select a.*, b.NAME  from CO_BENEFIT_RATES a ' +
             ' LEFT OUTER JOIN CL_E_D_GROUPS b on a.CL_E_D_GROUPS_NBR = b.CL_E_D_GROUPS_NBR ' +
             ' where {AsOfNow<a>} and CO_BENEFIT_RATES_NBR = :pNbr ';
    qRates := TevQuery.Create(s);
    qRates.Params.AddValue('pNbr', dsFilteredRates.DataSet.FieldByName('CO_BENEFIT_RATES_NBR').AsInteger);
    qRates.Execute;
    qRates.Result.IndexFieldNames := 'PERIOD_BEGIN';

    DoCopyRate;

  finally
    DM_COMPANY.CO_BENEFITS.Close;
    DM_COMPANY.CO_BENEFIT_SUBTYPE.Close;
    DM_COMPANY.CO_BENEFIT_RATES.Close;
    ctx_DataAccess.OpenClient( CurClient );

    DM_CLIENT.CL.DataRequired(sClientCond );
    DM_COMPANY.CO.DataRequired(sCompanyCond );
    DM_CLIENT.CL_E_DS.DataRequired('ALL');
    DM_CLIENT.CL_AGENCY.DataRequired('ALL');
    DM_CLIENT.CL_E_D_GROUPS.DataRequired('ALL');
    DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR=' + intToStr(CurCompany));
    DM_COMPANY.CO_BENEFITS.DataRequired(sBenefitCond);
    DM_COMPANY.CO_BENEFIT_SUBTYPE.DataRequired(sSubTypeCond);
    DM_COMPANY.CO_BENEFIT_RATES.DataRequired(sRatesCond);
    DM_COMPANY.CO_BENEFIT_CATEGORY.DataRequired(sCategoryCond);
    DM_COMPANY.CO_BENEFIT_RATES.IndexName := sRateIndex;

    wwdsList.dataset.Locate('CO_NBR;CL_NBR', VarArrayOf([CurCompany, CurClient ]), []);
    DM_COMPANY.CO.Locate('CO_NBR',CurCompany,[]);
    DM_COMPANY.CO_BENEFITS.Locate('CO_BENEFITS_NBR', CurBenefit, []);
    bRateCopy:= false;    
    SetBenefitRateFilter;

  end;
end;


procedure TEDIT_CO_BENEFITS.mnCopyRateClick(Sender: TObject);
begin
  inherited;
  if (wwdsDetail.DataSet.FieldByName('CO_BENEFITS_NBR').AsInteger > 0) then
     CopyRate;
end;

initialization
  RegisterClass(TEDIT_CO_BENEFITS);

end.

