// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_DIV_PR_BATCH_DEFLT_ED;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, Db, Wwdatsrc, DBCtrls, StdCtrls, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, Mask, wwdblook, 
  SDataStructure,  wwdbedit, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, EvUtils, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_DIV_PR_BATCH_DEFLT_ED = class(TEDIT_CO_BASE)
    TabSheet2: TTabSheet;
    wwDBGrid2: TevDBGrid;
    Label2: TevLabel;
    DBText2: TevDBText;
    fpDivisionRight: TisUIFashionPanel;
    sbDivisionDetail: TScrollBox;
    fpSummary: TisUIFashionPanel;
    fpDetail: TisUIFashionPanel;
    UpBtn: TevSpeedButton;
    DownBtn: TevSpeedButton;
    Label1: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    DBRadioGroup1: TevDBRadioGroup;
    fpOverrides: TisUIFashionPanel;
    lblJob: TevLabel;
    lcJob: TevDBLookupCombo;
    Label3: TevLabel;
    Label4: TevLabel;
    DBEdit3: TevDBEdit;
    DBEdit4: TevDBEdit;
    Label6: TevLabel;
    Label7: TevLabel;
    Label8: TevLabel;
    DBEdit5: TevDBEdit;
    DBEdit6: TevDBEdit;
    DBEdit7: TevDBEdit;
    wwDBGrid1: TevDBGrid;
    procedure DownBtnClick(Sender: TObject);
    procedure UpBtnClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
  private
    bFillerChecked: Boolean;
    procedure CheckFiller;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses
  SPackageEntry;

{$R *.DFM}

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.DownBtnClick(Sender: TObject);
var
  iNext, iCurrent, iNbr: integer;
begin
  inherited;
  try
    wwdsDetail.DataSet.DisableControls;
    iNbr := wwdsDetail.DataSet.FieldByName(wwdsDetail.DataSet.Name + '_NBR').AsInteger;
    if Trim(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3)) <> '' then
      iCurrent := StrToInt(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3))
    else
      iCurrent := wwdsDetail.DataSet.RecNo;
    wwdsDetail.DataSet.Next;
    if Trim(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3)) <> '' then
      iNext := StrToInt(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3))
    else
      iNext := wwdsDetail.DataSet.RecNo;
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.FieldByName('FILLER').AsString := Format('%3.3d', [iCurrent]) + Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 4,
      Length(wwdsDetail.DataSet.FieldByName('FILLER').AsString));
    wwdsDetail.DataSet.FieldByName('FILLTRIM').AsString := Format('%3.3d', [iCurrent]);
    wwdsDetail.DataSet.Post;
    sleep(1000);
    wwdsDetail.DataSet.Locate(wwdsDetail.DataSet.Name + '_NBR', iNbr, []);
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.FieldByName('FILLER').AsString := Format('%3.3d', [iNext]) + Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 4,
      Length(wwdsDetail.DataSet.FieldByName('FILLER').AsString));
    wwdsDetail.DataSet.FieldByName('FILLTRIM').AsString := Format('%3.3d', [iNext]);
    wwdsDetail.DataSet.Post;
    ctx_DataAccess.PostDataSets([TevClientDataSet(wwdsDetail.DataSet)]);
    sleep(1000);
  finally
     wwdsDetail.DataSet.EnableControls;
  end;
end;

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.UpBtnClick(Sender: TObject);
var
  iPrior, iCurrent, iNbr: integer;
begin
  inherited;
  try
    wwdsDetail.DataSet.DisableControls;
    iNbr := wwdsDetail.DataSet.FieldByName(wwdsDetail.DataSet.Name + '_NBR').AsInteger;
    if Trim(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3)) <> '' then
      iCurrent := StrToInt(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3))
    else
      iCurrent := wwdsDetail.DataSet.RecNo;
    wwdsDetail.DataSet.Prior;
    if Trim(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3)) <> '' then
      iPrior := StrToInt(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 3))
    else
      iPrior := wwdsDetail.DataSet.RecNo;
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.FieldByName('FILLER').AsString := Format('%3.3d', [iCurrent]) + Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 4,
      Length(wwdsDetail.DataSet.FieldByName('FILLER').AsString));
    wwdsDetail.DataSet.FieldByName('FILLTRIM').AsString := Format('%3.3d', [iCurrent]);
    wwdsDetail.DataSet.Post;
    sleep(1000);
    wwdsDetail.DataSet.Locate(wwdsDetail.DataSet.Name + '_NBR', iNbr, []);
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.FieldByName('FILLER').AsString := Format('%3.3d', [iPrior]) + Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 4,
      Length(wwdsDetail.DataSet.FieldByName('FILLER').AsString));
    wwdsDetail.DataSet.FieldByName('FILLTRIM').AsString := Format('%3.3d', [iPrior]);
    wwdsDetail.DataSet.Post;
    ctx_DataAccess.PostDataSets([TevClientDataSet(wwdsDetail.DataSet)]);
    sleep(1000);
  finally
     wwdsDetail.DataSet.EnableControls;
  end;
end;

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.BitBtn1Click(Sender: TObject);
begin
  inherited;
  bFillerChecked := False;
  CheckFiller;
end;

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.PageControl1Changing(
  Sender: TObject; var AllowChange: Boolean);
begin
  inherited;
  bFillerChecked := false;
  CheckFiller;
end;

function TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.GetDataSetConditions(
  sName: string): string;
begin
  if sName = 'CO_DIV_PR_BATCH_DEFLT_ED' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_JOBS, aDS);
  inherited;
end;

function TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_DIV_PR_BATCH_DEFLT_ED;
end;

function TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.GetInsertControl: TWinControl;
begin
  Result := wwDBLookupCombo1;
end;

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_DIVISION, SupportDataSets);
end;

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.Activate;
begin
  CheckFiller;
  inherited;
end;

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.wwdsDetailDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  if not assigned(Field) then
     bFillerChecked := false;
  if Assigned(UpBtn) then
    UpBtn.Enabled := wwdsDetail.DataSet.Recno > 1;
  if Assigned(DownBtn) then
    DownBtn.Enabled := (wwdsDetail.DataSet.Recno < wwdsDetail.DataSet.RecordCount) and (wwdsDetail.DataSet.Recno >= 1);
end;

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.CheckFiller;
var
  bChanged,bWrongFiller: boolean;
  prev,Curr,RN:integer;

begin
  //TevClientDataSet(wwdsDetail.DataSet).IndexFieldNames := 'FILLTRIM';
  //exit;
  bChanged := False;
  if wwdsDetail.DataSet.Active and not bFillerChecked then
    with TevClientDataSet(wwdsDetail.DataSet) do
    try
      DisableControls;
      IndexFieldNames := '';
      last;
      while not Bof do
      begin
         Edit;
         FieldByName('FILLTRIM').AsString := Copy(FieldByName('FILLER').AsString, 1, 3);
         Post;
         prior;
      end;
      IndexFieldNames := 'FILLTRIM';
      // test for errors in filler
      prev :=-1;
      bWrongFiller := false;
      first;
      RN := 1;
      while not Eof do
      begin
         curr := StrToInt(Copy(FieldByName('FILLER').AsString, 1, 3));
         if (curr <= prev) or ( curr <> RN) then
         begin
             bWrongFiller := true;
             break;
         end;
         prev := curr;
         Inc(RN);
         next;
      end;
      if(bWrongFiller) then
      begin
        IndexFieldNames := 'FILLER';
        last;
        RN := RecordCount;
        while not Bof do
        begin
          Edit;
          FieldByName('FILLTRIM').AsString := Format('%3.3d', [RN]);
          Post;
          Dec(RN);
          prior;
        end;
        IndexFieldNames := '';;
        while not Eof do
        begin
          if (Trim(Copy(FieldByName('FILLER').AsString, 1, 3)) <> FieldByName('FILLTRIM').AsString) then
          begin
            Edit;
            FieldByName('FILLER').AsString := FieldByName('FILLTRIM').AsString + Copy(FieldByName('FILLER').AsString, 4,
                Length(FieldByName('FILLER').AsString));
            Post;
            bChanged := True;
          end;
          Next;
        end;
      end;
      IndexFieldNames := 'FILLTRIM';

      if not bChanged then
        MergeChangeLog;
      bFillerChecked := True;
      TFramePackageTmpl(Self.Owner).ClickButton(NavCommit);
      ctx_DataAccess.PostDataSets([TevClientDataSet(wwdsDetail.DataSet)]);
    finally
      EnableControls;
    end;
end;

procedure TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.DeActivate;
begin
  inherited;
  ctx_DataAccess.PostDataSets([TevClientDataSet(wwdsDetail.DataSet)]);
end;

constructor TEDIT_CO_DIV_PR_BATCH_DEFLT_ED.Create(AOwner: TComponent);
begin
  inherited;

end;

initialization
  RegisterClass(TEDIT_CO_DIV_PR_BATCH_DEFLT_ED);

end.
