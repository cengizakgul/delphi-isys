// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BRCH_PR_BATCH_DEFLT_ED;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_DIV_PR_BATCH_DEFLT_ED, Db, Wwdatsrc, DBCtrls, wwdblook,
  StdCtrls, ExtCtrls, Mask, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  SDataStructure,  wwdbedit, EvUIComponents, EvUtils, EvClientDataSet,
  SDDClasses, SDataDictclient, ImgList, ISBasicClasses, SDataDicttemp,
  isUIwwDBEdit, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED = class(TEDIT_CO_DIV_PR_BATCH_DEFLT_ED)
    Panel4: TevPanel;
    wwDBGrid3: TevDBGrid;
    Splitter2: TevSplitter;
    procedure pnlSubbrowseResize(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetDataSetConditions(sName: string): string; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED.pnlSubbrowseResize(
  Sender: TObject);
begin
  inherited;
  wwDBGrid1.Width := pnlSubbrowse.Width div 2;
end;

function TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED.GetDataSetConditions(
  sName: string): string;
begin
  if (sName = 'CO_BRCH_PR_BATCH_DEFLT_ED') or
     (sName = 'CO_BRANCH') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

function TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_BRCH_PR_BATCH_DEFLT_ED;
end;

procedure TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO_DIVISION, SupportDataSets);
  AddDS(DM_COMPANY.CO_BRANCH, SupportDataSets);
end;

procedure TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  inherited;
end;

procedure TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED.Activate;
begin
  wwdsDetail.DataSet := GetDefaultDataSet;
  inherited;
end;

constructor TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED.Create(AOwner: TComponent);
begin
  inherited;

end;

initialization
  RegisterClass(TEDIT_CO_BRCH_PR_BATCH_DEFLT_ED);

end.

