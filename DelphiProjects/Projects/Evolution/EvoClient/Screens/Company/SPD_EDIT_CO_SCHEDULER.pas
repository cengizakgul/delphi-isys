// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_SCHEDULER;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CO_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Spin,
  CheckLst,  EvUtils, EvConsts, Mask, wwdbedit, EvDataAccessComponents,
  Wwdotdot, Wwdbcomb, wwdbdatetimepicker, wwdblook, SPackageEntry, Variants,
  EvSecElement, wwDialog, wwrcdvw, EvTypes, DateUtils,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, SDDClasses, EvContext,
  SCalendarCreation, ISDataAccessComponents, SDataDictclient, SDataDicttemp,
  EvMainboard, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBComboBox, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_SCHEDULER = class(TEDIT_CO_BASE)
    tbshCreateCalendar: TTabSheet;
    evcsHoliday: TevClientDataSet;
    DM_PAYROLL: TDM_PAYROLL;
    evDataSource1: TevDataSource;
    TabSheet3: TTabSheet;
    evPanel2: TevPanel;
    evPanel3: TevPanel;
    evDBGrid1: TevDBGrid;
    evDBGrid2: TevDBGrid;
    evDataSource2: TevDataSource;
    evDBComboBox2: TevDBComboBox;
    RV: TwwRecordViewDialog;
    Calendar: TEvBasicClientDataSet;
    CalendarCHECK_DATE: TDateField;
    CalendarPERIOD_BEGIN_DATE: TDateField;
    CalendarPERIOD_END_DATE: TDateField;
    CalendarCALL_IN_DATE: TDateTimeField;
    CalendarDELIVERY_DATE: TDateTimeField;
    CalendarLAST_REAL_CHECK_DATE: TDateField;
    CalendarLAST_REAL_CALL_IN_DATE: TDateTimeField;
    CalendarLAST_REAL_DELIVERY_DATE: TDateTimeField;
    CalendarPAY_FREQUENCY: TStringField;
    CalendarDAYS_PRIOR: TIntegerField;
    CalendarDAYS_POST: TIntegerField;
    sbCreateCalendar: TScrollBox;
    fpCalendar: TisUIFashionPanel;
    fpOptions: TisUIFashionPanel;
    evpcFrequency: TevPageControl;
    tshtWeekly: TTabSheet;
    evlbWeekDay1: TevLabel;
    evlbWeeklyCheckDate: TevLabel;
    evlbWeeklyPeriodBegin: TevLabel;
    evlbWeeklyPeriodEnd: TevLabel;
    evdpWeeklyCheckDate: TevDateTimePicker;
    evdpWeeklyPeriodBegin: TevDateTimePicker;
    evdpWeeklyPeriodEnd: TevDateTimePicker;
    evGroupBox3: TevGroupBox;
    evlbWeekDaysPrior: TevLabel;
    evlbWeekCallInTime: TevLabel;
    evseWeekDaysPrior: TevSpinEdit;
    evdpWeekCallInTime: TevDateTimePicker;
    evGroupBox4: TevGroupBox;
    evlbWeeklyDaysAfter: TevLabel;
    evlbWeeklyDeliveryTime: TevLabel;
    evLabel34: TevLabel;
    evseWeeklyDaysAfter: TevSpinEdit;
    evdpWeeklyDeliveryTime: TevDateTimePicker;
    tshtBIWeekly: TTabSheet;
    evLabel9: TevLabel;
    evLabel14: TevLabel;
    evLabel15: TevLabel;
    evLabel16: TevLabel;
    evdpBWeeklyCheckDate: TevDateTimePicker;
    evdpBWeeklyPeriodBegin: TevDateTimePicker;
    evdpBWeeklyPeriodEnd: TevDateTimePicker;
    evGroupBox5: TevGroupBox;
    evseBWeekDaysPrior: TevSpinEdit;
    evdpBWeekCallInTime: TevDateTimePicker;
    evGroupBox6: TevGroupBox;
    evseBWeeklyDaysAfter: TevSpinEdit;
    evdpBWeeklyDeliveryTime: TevDateTimePicker;
    tshtSemiMonthly: TTabSheet;
    evLabel17: TevLabel;
    evLabel22: TevLabel;
    evLabel23: TevLabel;
    evLabel24: TevLabel;
    evLabel25: TevLabel;
    evdpSMonthlyCheckDate: TevDateTimePicker;
    evdpSMonthlyPeriodBegin: TevDateTimePicker;
    evdpSMonthlyPeriodEnd: TevDateTimePicker;
    evdpSMonthlyCheckDate2: TevDateTimePicker;
    evdpSMonthlyPeriodBegin2: TevDateTimePicker;
    evdpSMonthlyPeriodEnd2: TevDateTimePicker;
    evGroupBox7: TevGroupBox;
    evseSMonthlyDaysPrior: TevSpinEdit;
    evdpSMonthlyCallInTime: TevDateTimePicker;
    evseSMonthlyDaysPrior2: TevSpinEdit;
    evdpSMonthlyCallInTime2: TevDateTimePicker;
    evGroupBox8: TevGroupBox;
    evseSMonthlyDaysAfter: TevSpinEdit;
    evdpBMonthlyDeliveryTime: TevDateTimePicker;
    evdpBMonthlyDeliveryTime2: TevDateTimePicker;
    evseSMonthlyDaysAfter2: TevSpinEdit;
    tshtMonthly: TTabSheet;
    evLabel26: TevLabel;
    evLabel31: TevLabel;
    evLabel32: TevLabel;
    evLabel33: TevLabel;
    evdpMonthlyCheckDate: TevDateTimePicker;
    evdpMonthlyPeriodBegin: TevDateTimePicker;
    evdpMonthlyPeriodEnd: TevDateTimePicker;
    evGroupBox9: TevGroupBox;
    evseMonthlyDaysPrior: TevSpinEdit;
    evdpMonthlyCallInTime: TevDateTimePicker;
    evGroupBox10: TevGroupBox;
    evseMonthlyDaysAfter: TevSpinEdit;
    evdpMonthlyDeliveryTime: TevDateTimePicker;
    tshtQuarterly: TTabSheet;
    evLabel44: TevLabel;
    evLabel48: TevLabel;
    evLabel49: TevLabel;
    evLabel50: TevLabel;
    evGroupBox11: TevGroupBox;
    evseQuarterlyDaysPrior: TevSpinEdit;
    evdpQuarterlyCallInTime: TevDateTimePicker;
    evGroupBox12: TevGroupBox;
    evseQuarterlyDaysAfter: TevSpinEdit;
    evdpQuarterlyDeliveryTime: TevDateTimePicker;
    evdpQuarterlyCheckDate: TevDateTimePicker;
    evdpQuarterlyPeriodBegin: TevDateTimePicker;
    evdpQuarterlyPeriodEnd: TevDateTimePicker;
    evCreateCalendar: TevBitBtn;
    evPanel4: TevPanel;
    evlbNbrMonths: TevLabel;
    evseNbrMonths: TevSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    evcbMoveCheck: TevDBComboBox;
    evcbMoveCallIn: TevDBComboBox;
    evcbMoveDelivery: TevDBComboBox;
    evCalculateDay: TevRadioGroup;
    evlbHolidayList: TevListBox;
    Label4: TLabel;
    Label6: TLabel;
    evLabel8: TevLabel;
    evLabel10: TevLabel;
    Label7: TLabel;
    evLabel11: TevLabel;
    evLabel12: TevLabel;
    evLabel13: TevLabel;
    evLabel18: TevLabel;
    evLabel19: TevLabel;
    Label8: TLabel;
    evLabel20: TevLabel;
    evLabel21: TevLabel;
    evLabel35: TevLabel;
    evLabel27: TevLabel;
    evLabel28: TevLabel;
    Label9: TLabel;
    evLabel29: TevLabel;
    evLabel30: TevLabel;
    evLabel36: TevLabel;
    evLabel37: TevLabel;
    evLabel38: TevLabel;
    Label10: TLabel;
    evLabel39: TevLabel;
    evLabel40: TevLabel;
    evLabel41: TevLabel;
    sbEditCalendar: TScrollBox;
    fpEditCalendar: TisUIFashionPanel;
    evLabel1: TevLabel;
    evLabel3: TevLabel;
    evLabel2: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker3: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    evButton1: TevBitBtn;
    fpBatch: TisUIFashionPanel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    SpeedButton2: TevSpeedButton;
    SpeedButton1: TevSpeedButton;
    evDBDateTimePicker4: TevDBDateTimePicker;
    evDBDateTimePicker5: TevDBDateTimePicker;
    evDBDateTimePicker6: TevDBDateTimePicker;
    evDBComboBox1: TevDBComboBox;
    fpBrowseCalendar: TisUIFashionPanel;
    sbBrowseCalendar: TScrollBox;
    pnlSBBorder: TevPanel;
    pnlBrowseCalendarLeft: TevPanel;
    evSplitter1: TevSplitter;
    pnlBrowseCalendarRight: TevPanel;
    fpCalendarRight: TisUIFashionPanel;
    fpCalendarLeft: TisUIFashionPanel;
    procedure evCreateClendarClick(Sender: TObject);
    procedure evDBGrid1RowChanged(Sender: TObject);
    procedure evButton1Click(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure evdpWeeklyPeriodBeginChange(Sender: TObject);
    procedure evdpWeeklyPeriodEndExit(Sender: TObject);
    procedure evdpBWeeklyPeriodBeginChange(Sender: TObject);
    procedure evdpBWeeklyPeriodEndExit(Sender: TObject);
    procedure evCalculateDayClick(Sender: TObject);
    procedure fpCalendarLeftResize(Sender: TObject);
    procedure fpCalendarRightResize(Sender: TObject);
  private
    { Private declarations }
    FPayFrequency: String;
    function FillWeeklyStructure: boolean;
    procedure LoadHolidayList;
    function FillBiWeeklyStructure: boolean;
    function FillSMonthlyStructure: boolean;
    function FillMonthlyStructure: boolean;
    function FillQuarterlyStructure: boolean;
    procedure DeleteNonUsedRows(aDate: TDate);
    procedure ConvertUsedRowsToNewFormat;
    procedure GetDefaultsIfAnyExist;
    procedure DetermineTabsheetsToShow;
    function ReturnCorrectFrequency(S: String): String;
  public
    { Public declarations }
    PrRecNo: Integer;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterDataSetsReopen; Override;
    procedure Activate; Override;
    procedure Deactivate; Override;
  end;

implementation

uses SPD_EDIT_CO_SCHEDULER_HELPER;

{$R *.DFM}

{ TEDIT_CO_SCHEDULER }


procedure TEDIT_CO_SCHEDULER.evCreateClendarClick(Sender: TObject);
var
  WCrossed, BCrossed, SCrossed, MCrossed, QCrossed: Boolean;
const
  M = ' calendar has check dates and batch dates in different years.  If this is not correct, you will need to recreate the calendar.';
begin

  WCrossed := False;
  BCrossed := False;
  SCrossed := False;
  MCrossed := False;
  QCrossed := False;
  Calendar.EmptyDataSet;

  DM_COMPANY.CO_CALENDAR_DEFAULTS.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);

  if tshtWeekly.TabVisible and (not FillWeeklyStructure) then   exit;
  if tshtBIWeekly.TabVisible and (not FillBiWeeklyStructure) then exit;
  if tshtSemiMonthly.TabVisible and (not FillSMonthlyStructure) then exit;
  if tshtMonthly.TabVisible and (not FillMonthlyStructure) then  exit;
  if tshtQuarterly.TabVisible and (not FillQuarterlyStructure) then exit;

  ctx_StartWait;
  try
    DM_PAYROLL.PR_SCHEDULED_EVENT.DisableControls;
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.DisableControls;
    DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.DataRequired('ALL');

    AddToCalendar(Calendar, evcsHoliday, WCrossed, BCrossed, SCrossed, MCrossed, QCrossed);

    if tshtWeekly.TabVisible and WCrossed then       ShowMessage('Weekly' + M);
    if tshtBIWeekly.TabVisible and BCrossed then     ShowMessage('Bi-Weekly' + M);
    if tshtSemiMonthly.TabVisible and SCrossed then  ShowMessage('Semi-Monthly' + M);
    if tshtMonthly.TabVisible and MCrossed then      ShowMessage('Monthly' + M);
    if tshtQuarterly.TabVisible and QCrossed then    ShowMessage('Quarterly' + M);

    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_CALENDAR_DEFAULTS, DM_PAYROLL.PR_SCHEDULED_EVENT, DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH]);

    EvMessage('Calendar creation is complete!');

  finally
    ctx_EndWait;
    DM_PAYROLL.PR_SCHEDULED_EVENT.Close;
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Close;
    DM_PAYROLL.PR_SCHEDULED_EVENT.EnableControls;
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.EnableControls;
    if AutoBackToPreviosScreen then
    begin
      AutoBackToPreviosScreen := False;
      CameFrom := '';                                                             // Payroll|010', 'TREDIT_PAYROLL
      if EvMessage('Would you like to go back to payroll now?',
                     mtConfirmation, [mbYes, mbNo]) = mrYes then
        PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Payroll - Payroll' + #0)), 0)
      else
        PageControl1.ActivatePage(TabSheet3);
    end;
  end;
end;

procedure TEDIT_CO_SCHEDULER.LoadHolidayList;
var
  y: Integer;
begin
  evlbHolidayList.Clear;

  GetHolidayList(evcsHoliday);

  y := YearOf(Today);
  evcsHoliday.First;
  while not evcsHoliday.Eof do
  begin
    if YearOf(evcsHoliday.FieldByName('holiday_date').AsDateTime) >= y  then
      evlbHolidayList.Items.Add(evcsHoliday.FieldByName('HOLIDAY_DATE').AsString+'-'+evcsHoliday.FieldByName('HOLIDAY_NAME').AsString);
    evcsHoliday.Next;
  end;
end;


function TEDIT_CO_SCHEDULER.FillWeeklyStructure: boolean;
var
  CheckDate, PeriodBegin,  PeriodEnd: TDate;
  Adjustment: array[1..3] of Integer;
  CalendarDefaults: TCalendarDefaults;
begin

  Result := True;

  if evseWeekDaysPrior.Value = 0 then
    if EvMessage('Your Call In Date is set up to be the same as your Check Date.  Continue?' , mtWarning, [mbNo, mbYes], mbNo, 0) = mrNo then
    begin
      tshtWeekly.Visible := True;
      result := False;
    end;

  case evcbMoveCheck.ItemIndex of
    0: Adjustment[1] := 1;
    1: Adjustment[1] := -1;
  else
    Adjustment[1] := 0;
  end;

  case evcbMoveCallIn.ItemIndex of
    0: Adjustment[2] := 1;
    1: Adjustment[2] := -1;
  else
    Adjustment[2] := 0;
  end;

  case evcbMoveDelivery.ItemIndex of
    0: Adjustment[3] := 1;
    1: Adjustment[3] := -1;
  else
    Adjustment[3] := 0;
  end;

  CheckDate := evdpWeeklyCheckDate.Date;
  PeriodBegin := evdpWeeklyPeriodBegin.Date;
  PeriodEnd := evdpWeeklyPeriodEnd.Date;

    //   if business day   set up first caracter of filler   'B'  -  Business
    //  otherwise 'R'     this  is reguler day

  if evCalculateDay.ItemIndex = 1 then
        CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'B', 1, 1)
   else CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'R', 1, 1);

  CalendarDefaults.MOVE_CHECK_DATE := Adjustment[1];
  CalendarDefaults.MOVE_CALLIN_DATE := Adjustment[2];
  CalendarDefaults.MOVE_DELIVERY_DATE := Adjustment[3];
  CalendarDefaults.NUMBER_OF_MONTHS := evseNbrMonths.Value;
  CalendarDefaults.CO_NBR := DM_COMPANY.CO['co_nbr'];
  CalendarDefaults.FREQUENCY := CO_FREQ_WEEKLY;
  CalendarDefaults.CALL_IN_TIME := evdpWeekCallInTime.Time;
  CalendarDefaults.DELIVERY_TIME := evdpWeeklyDeliveryTime.Time;
  CalendarDefaults.NUMBER_OF_DAYS_PRIOR := evseWeekDaysPrior.Value;
  CalendarDefaults.NUMBER_OF_DAYS_AFTER := evseWeeklyDaysAfter.Value;

  CreateCalendar(Calendar, evcsHoliday, CalendarDefaults, CheckDate,PeriodBegin, PeriodEnd,false,false,True);
  UpdateDefaultsByFrequency(CalendarDefaults);

end;

function TEDIT_CO_SCHEDULER.FillBiWeeklyStructure: boolean;
var
  CheckDate, PeriodBegin, PeriodEnd: TDate;
  Adjustment: array[1..3] of Integer;
  CalendarDefaults: TCalendarDefaults;
begin
  Result := True;

  if evseBWeekDaysPrior.Value = 0 then
    if EvMessage('Your Call In Date is set up to be the same as your Check Date.  Continue?' , mtWarning, [mbNo, mbYes], mbNo, 0) = mrNo then
    begin
      tshtBIWeekly.Visible := True;
      Result := False;
      Exit;
    end;

  case evcbMoveCheck.ItemIndex of
    0: Adjustment[1] := 1;
    1: Adjustment[1] := -1;
  else
    Adjustment[1] := 0;
  end;

  case evcbMoveCallIn.ItemIndex of
    0: Adjustment[2] := 1;
    1: Adjustment[2] := -1;
  else
    Adjustment[2] := 0;
  end;

  case evcbMoveDelivery.ItemIndex of
    0: Adjustment[3] := 1;
    1: Adjustment[3] := -1;
  else
    Adjustment[3] := 0;
  end;

  CheckDate := evdpBWeeklyCheckDate.Date;
  PeriodBegin := evdpBWeeklyPeriodBegin.Date;
  PeriodEnd := evdpBWeeklyPeriodEnd.Date;

    //   if business day   set up first caracter of filler   'B'  -  Business
    //  otherwise 'R'     this  is reguler day
  if evCalculateDay.ItemIndex = 1 then
        CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'B', 1, 1)
   else CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'R', 1, 1);
  CalendarDefaults.MOVE_CHECK_DATE := Adjustment[1];
  CalendarDefaults.MOVE_CALLIN_DATE := Adjustment[2];
  CalendarDefaults.MOVE_DELIVERY_DATE := Adjustment[3];
  CalendarDefaults.NUMBER_OF_MONTHS := evseNbrMonths.Value;
  CalendarDefaults.CO_NBR := DM_COMPANY.CO['co_nbr'];
  CalendarDefaults.FREQUENCY := CO_FREQ_BWEEKLY;
  CalendarDefaults.CALL_IN_TIME := evdpBWeekCallInTime.Time;
  CalendarDefaults.DELIVERY_TIME := evdpBWeeklyDeliveryTime.Time;
  CalendarDefaults.NUMBER_OF_DAYS_PRIOR := evseBWeekDaysPrior.Value;
  CalendarDefaults.NUMBER_OF_DAYS_AFTER := evseBWeeklyDaysAfter.Value;

  CreateCalendar(Calendar, evcsHoliday, CalendarDefaults, CheckDate,PeriodBegin, PeriodEnd,False,False,True);
  UpdateDefaultsByFrequency(CalendarDefaults);

end;

function TEDIT_CO_SCHEDULER.FillSMonthlyStructure: boolean;
  procedure AddDates(DS: TevClientDataSet; Desc: String; sDate: TDateTime);
  begin
    DS.Append;
    DS.FieldByName('Description').AsString := Desc;
    DS.FieldByName('Date').AsDateTime := sDate;
    DS.Post;
  end;
var
  SH: TSchedulerHelper;
  I: Integer;
  Adjustment: array[1..3] of Integer;
  BM: TBookmark;
  CheckDate, PeriodBegin, PeriodEnd: TDate;
  CheckAlwaysLast, CheckAlwaysLast2, PeriodAlwaysLast, PeriodAlwaysLast2: Boolean;
  CalendarDefaults: TCalendarDefaults;
begin
  Result := True;
  if (evseSMonthlyDaysPrior.Value = 0) or (evseSMonthlyDaysPrior2.Value = 0) then
  begin
    if EvMessage('Your Call In Date is set up to be the same as your Check Date.  Continue?' , mtWarning, [mbNo, mbYes], mbNo, 0) = mrNo then
    begin
      tshtSemiMonthly.Visible := True;
      Result := False;
      Exit;
    end;
  end;

  CheckAlwaysLast := False;
  CheckAlwaysLast2 := False;
  PeriodAlwaysLast := False;
  PeriodAlwaysLast2 := False;

  SH := TSchedulerHelper.Create(Self);
  try
    with SH do
    begin
      if CompareDate(EndOfTheMonth(evdpSMonthlyCheckDate.Date), evdpSMonthlyCheckDate.Date) = 0 then
        AddDates(evClientDataSet1, 'Check Date 1', evdpSMonthlyCheckDate.Date);
      if CompareDate(EndOfTheMonth(evdpSMonthlyPeriodEnd.Date), evdpSMonthlyPeriodEnd.Date) = 0 then
        AddDates(evClientDataSet1, 'Period End Date 1', evdpSMonthlyPeriodEnd.Date);
      if CompareDate(EndOfTheMonth(evdpSMonthlyCheckDate2.Date), evdpSMonthlyCheckDate2.Date) = 0 then
        AddDates(evClientDataSet1, 'Check Date 2', evdpSMonthlyCheckDate2.Date);
      if CompareDate(EndOfTheMonth(evdpSMonthlyPeriodEnd2.Date), evdpSMonthlyPeriodEnd2.Date) = 0 then
        AddDates(evClientDataSet1, 'Period End Date 2', evdpSMonthlyPeriodEnd2.Date);
      if SH.evClientDataSet1.RecordCount <> 0 then
      begin
        evDBCheckGrid1.SelectAll;
        ShowModal;
        for I := 0 to evDBCheckGrid1.SelectedList.Count - 1 do
        begin
          BM := evDBCheckGrid1.SelectedList.Items[I];
          evClientDataSet1.GotoBookmark(BM);
          if evClientDataSet1.FieldByName('Description').AsString = 'Check Date 1' then
             CheckAlwaysLast := True;
          if evClientDataSet1.FieldByName('Description').AsString = 'Period End Date 1' then
             PeriodAlwaysLast := True;
          if evClientDataSet1.FieldByName('Description').AsString = 'Check Date 2' then
             CheckAlwaysLast2 := True;
          if evClientDataSet1.FieldByName('Description').AsString = 'Period End Date 2' then
             PeriodAlwaysLast2 := True;
        end;
      end;
    end;
  finally
    SH.Free;
  end;

  case evcbMoveCheck.ItemIndex of
    0: Adjustment[1] := 1;
    1: Adjustment[1] := -1;
  else
    Adjustment[1] := 0;
  end;

  case evcbMoveCallIn.ItemIndex of
    0: Adjustment[2] := 1;
    1: Adjustment[2] := -1;
  else
    Adjustment[2] := 0;
  end;

  case evcbMoveDelivery.ItemIndex of
    0: Adjustment[3] := 1;
    1: Adjustment[3] := -1;
  else
    Adjustment[3] := 0;
  end;

  //Check Date 1
  CheckDate := evdpSMonthlyCheckDate.Date;
  PeriodBegin := evdpSMonthlyPeriodBegin.Date;
  PeriodEnd := evdpSMonthlyPeriodEnd.Date;

    //   if business day   set up first caracter of filler   'B'  -  Business
    //  otherwise 'R'     this  is reguler day

  if evCalculateDay.ItemIndex = 1 then
        CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'B', 1, 1)
   else CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'R', 1, 1);

  CalendarDefaults.MOVE_CHECK_DATE := Adjustment[1];
  CalendarDefaults.MOVE_CALLIN_DATE := Adjustment[2];
  CalendarDefaults.MOVE_DELIVERY_DATE := Adjustment[3];
  CalendarDefaults.NUMBER_OF_MONTHS := evseNbrMonths.Value;
  CalendarDefaults.CO_NBR := DM_COMPANY.CO['co_nbr'];
  CalendarDefaults.FREQUENCY := CO_FREQ_SMONTHLY;
  CalendarDefaults.CALL_IN_TIME := evdpSMonthlyCallInTime.Time;
  CalendarDefaults.DELIVERY_TIME := evdpBMonthlyDeliveryTime.Time;
  CalendarDefaults.NUMBER_OF_DAYS_PRIOR := evseSMonthlyDaysPrior.Value;
  CalendarDefaults.NUMBER_OF_DAYS_AFTER := evseSMonthlyDaysAfter.Value;
  CalendarDefaults.CALL_IN_TIME2 := evdpSMonthlyCallInTime2.Time;
  CalendarDefaults.DELIVERY_TIME2 := evdpBMonthlyDeliveryTime2.Time;
  CalendarDefaults.NUMBER_OF_DAYS_PRIOR2 := evseSMonthlyDaysPrior2.Value;
  CalendarDefaults.NUMBER_OF_DAYS_AFTER2 := evseSMonthlyDaysAfter2.Value;

  CreateCalendar(Calendar, evcsHoliday, CalendarDefaults,
                     CheckDate,PeriodBegin, PeriodEnd,CheckAlwaysLast, PeriodAlwaysLast,True);

  //Check Date 2
  CheckDate := evdpSMonthlyCheckDate2.Date;
  PeriodBegin := evdpSMonthlyPeriodBegin2.Date;
  PeriodEnd := evdpSMonthlyPeriodEnd2.Date;

  CreateCalendar(Calendar, evcsHoliday, CalendarDefaults,
                     CheckDate,PeriodBegin, PeriodEnd,CheckAlwaysLast2, PeriodAlwaysLast2,False);

  UpdateDefaultsByFrequency(CalendarDefaults);

end;

function TEDIT_CO_SCHEDULER.FillMonthlyStructure: boolean;
  procedure AddDates(DS: TevClientDataSet; Desc: String; sDate: TDateTime);
  begin
    DS.Append;
    DS.FieldByName('Description').AsString := Desc;
    DS.FieldByName('Date').AsDateTime := sDate;
    DS.Post;
  end;
var
  SH: TSchedulerHelper;
  I: Integer;
  Adjustment: array[1..3] of Integer;
  BM: TBookmark;
  CheckDate, PeriodBegin, PeriodEnd: TDate;
  CheckAlwaysLast, PeriodAlwaysLast: Boolean;
  CalendarDefaults: TCalendarDefaults;
begin
  Result := True;
  CheckAlwaysLast := False;
  PeriodAlwaysLast := False;

  if evseMonthlyDaysPrior.Value = 0 then
  begin
    if EvMessage('Your Call In Date is set up to be the same as your Check Date.  Continue?' , mtWarning, [mbNo, mbYes], mbNo, 0) = mrNo then
    begin
      tshtMonthly.Visible := True;
      result := False;
      Exit;
    end;
  end;

  SH := TSchedulerHelper.Create(Self);
  try
    with SH do
    begin
      if CompareDate(EndOfTheMonth(evdpMonthlyCheckDate.Date), evdpMonthlyCheckDate.Date) = 0 then
        AddDates(evClientDataSet1, 'Check Date 1', evdpMonthlyCheckDate.Date);
      if CompareDate(EndOfTheMonth(evdpMonthlyPeriodEnd.Date), evdpMonthlyPeriodEnd.Date) = 0 then
        AddDates(evClientDataSet1, 'Period End Date 1', evdpMonthlyPeriodEnd.Date);
      if SH.evClientDataSet1.RecordCount <> 0 then
      begin
        evDBCheckGrid1.SelectAll;
        ShowModal;
        for I := 0 to evDBCheckGrid1.SelectedList.Count - 1 do
        begin
          BM := evDBCheckGrid1.SelectedList.Items[I];
          evClientDataSet1.GotoBookmark(BM);
          if evClientDataSet1.FieldByName('Description').AsString = 'Check Date 1' then
             CheckAlwaysLast := True;
          if evClientDataSet1.FieldByName('Description').AsString = 'Period End Date 1' then
             PeriodAlwaysLast := True;
        end;
      end;
    end;
  finally
    SH.Free;
  end;

  case evcbMoveCheck.ItemIndex of
    0: Adjustment[1] := 1;
    1: Adjustment[1] := -1;
  else
    Adjustment[1] := 0;
  end;

  case evcbMoveCallIn.ItemIndex of
    0: Adjustment[2] := 1;
    1: Adjustment[2] := -1;
  else
    Adjustment[2] := 0;
  end;

  case evcbMoveDelivery.ItemIndex of
    0: Adjustment[3] := 1;
    1: Adjustment[3] := -1;
  else
    Adjustment[3] := 0;
  end;

  CheckDate := evdpMonthlyCheckDate.Date;
  PeriodBegin := evdpMonthlyPeriodBegin.Date;
  PeriodEnd := evdpMonthlyPeriodEnd.Date;

    //   if business day   set up first caracter of filler   'B'  -  Business
    //  otherwise 'R'     this  is reguler day

  if evCalculateDay.ItemIndex = 1 then
        CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'B', 1, 1)
   else CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'R', 1, 1);
  CalendarDefaults.MOVE_CHECK_DATE := Adjustment[1];
  CalendarDefaults.MOVE_CALLIN_DATE := Adjustment[2];
  CalendarDefaults.MOVE_DELIVERY_DATE := Adjustment[3];
  CalendarDefaults.NUMBER_OF_MONTHS := evseNbrMonths.Value;
  CalendarDefaults.CO_NBR := DM_COMPANY.CO['co_nbr'];
  CalendarDefaults.FREQUENCY := CO_FREQ_MONTHLY;
  CalendarDefaults.CALL_IN_TIME := evdpMonthlyCallInTime.Time;
  CalendarDefaults.DELIVERY_TIME := evdpMonthlyDeliveryTime.Time;
  CalendarDefaults.NUMBER_OF_DAYS_PRIOR := evseMonthlyDaysPrior.Value;
  CalendarDefaults.NUMBER_OF_DAYS_AFTER := evseMonthlyDaysAfter.Value;

  CreateCalendar(Calendar, evcsHoliday, CalendarDefaults,
                     CheckDate,PeriodBegin, PeriodEnd,CheckAlwaysLast, PeriodAlwaysLast,True);

  UpdateDefaultsByFrequency(CalendarDefaults);

end;

function TEDIT_CO_SCHEDULER.FillQuarterlyStructure: boolean;
  procedure AddDates(DS: TevClientDataSet; Desc: String; sDate: TDateTime);
  begin
    DS.Append;
    DS.FieldByName('Description').AsString := Desc;
    DS.FieldByName('Date').AsDateTime := sDate;
    DS.Post;
  end;
var
  SH: TSchedulerHelper;
  I : Integer;
  Adjustment: array[1..3] of Integer;
  BM: TBookmark;
  CheckDate, PeriodBegin, PeriodEnd: TDate;
  CheckAlwaysLast, PeriodAlwaysLast: Boolean;
  CalendarDefaults: TCalendarDefaults;
begin
  Result := True;
  CheckAlwaysLast := False;
  PeriodAlwaysLast := False;

  if evseQuarterlyDaysPrior.Value = 0 then
  begin
    if EvMessage('Your Call In Date is set up to be the same as your Check Date.  Continue?' , mtWarning, [mbNo, mbYes], mbNo, 0) = mrNo then
    begin
      tshtMonthly.Visible := True;
      result := False;
      Exit;
    end;
  end;

  SH := TSchedulerHelper.Create(Self);
  try
    with SH do
    begin
      if CompareDate(EndOfTheMonth(evdpQuarterlyCheckDate.Date), evdpQuarterlyCheckDate.Date) = 0 then
        AddDates(evClientDataSet1, 'Check Date 1', evdpQuarterlyCheckDate.Date);
      if CompareDate(EndOfTheMonth(evdpQuarterlyPeriodEnd.Date), evdpQuarterlyPeriodEnd.Date) = 0 then
        AddDates(evClientDataSet1, 'Period End Date 1', evdpQuarterlyPeriodEnd.Date);
      if SH.evClientDataSet1.RecordCount <> 0 then
      begin
        evDBCheckGrid1.SelectAll;
        ShowModal;
        for I := 0 to evDBCheckGrid1.SelectedList.Count - 1 do
        begin
          BM := evDBCheckGrid1.SelectedList.Items[I];
          evClientDataSet1.GotoBookmark(BM);
          if evClientDataSet1.FieldByName('Description').AsString = 'Check Date 1' then
             CheckAlwaysLast := True;
          if evClientDataSet1.FieldByName('Description').AsString = 'Period End Date 1' then
             PeriodAlwaysLast := True;
        end;
      end;
    end;

    case evcbMoveCheck.ItemIndex of
      0: Adjustment[1] := 1;
      1: Adjustment[1] := -1;
    else
      Adjustment[1] := 0;
    end;

    case evcbMoveCallIn.ItemIndex of
      0: Adjustment[2] := 1;
      1: Adjustment[2] := -1;
    else
      Adjustment[2] := 0;
    end;

    case evcbMoveDelivery.ItemIndex of
      0: Adjustment[3] := 1;
      1: Adjustment[3] := -1;
    else
      Adjustment[3] := 0;
    end;

    CheckDate := evdpQuarterlyCheckDate.Date;
    PeriodBegin := evdpQuarterlyPeriodBegin.Date;
    PeriodEnd := evdpQuarterlyPeriodEnd.Date;

    //   if business day   set up first caracter of filler   'B'  -  Business
    //  otherwise 'R'     this  is reguler day

    if evCalculateDay.ItemIndex = 1 then
           CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'B', 1, 1)
     else  CalendarDefaults.FILLER := PutIntoFiller(CalendarDefaults.FILLER, 'R', 1, 1);

    CalendarDefaults.MOVE_CHECK_DATE := Adjustment[1];
    CalendarDefaults.MOVE_CALLIN_DATE := Adjustment[2];
    CalendarDefaults.MOVE_DELIVERY_DATE := Adjustment[3];
    CalendarDefaults.NUMBER_OF_MONTHS := evseNbrMonths.Value;
    CalendarDefaults.CO_NBR := DM_COMPANY.CO['co_nbr'];
    CalendarDefaults.FREQUENCY := CO_FREQ_QUARTERLY;
    CalendarDefaults.CALL_IN_TIME := evdpQuarterlyCallInTime.Time;
    CalendarDefaults.DELIVERY_TIME := evdpQuarterlyDeliveryTime.Time;
    CalendarDefaults.NUMBER_OF_DAYS_PRIOR := evseQuarterlyDaysPrior.Value;
    CalendarDefaults.NUMBER_OF_DAYS_AFTER := evseQuarterlyDaysAfter.Value;

    CreateCalendar(Calendar, evcsHoliday, CalendarDefaults,
                     CheckDate,PeriodBegin, PeriodEnd,CheckAlwaysLast, PeriodAlwaysLast,True);

    UpdateDefaultsByFrequency(CalendarDefaults);
  finally
    SH.Free;
  end;
end;

procedure TEDIT_CO_SCHEDULER.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  AddDS(DM_PAYROLL.PR_SCHEDULED_EVENT, EditDataSets);
  AddDS(DM_PAYROLL.PR_SCHEDULED_EVENT, InsertDataSets);
  DeleteDataSet := DM_PAYROLL.PR_SCHEDULED_EVENT;
  inherited;
  AddDS(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH, EditDataSets);
  AddDS(DM_COMPANY.CO_CALENDAR_DEFAULTS, EditDataSets);
end;

procedure TEDIT_CO_SCHEDULER.DeleteNonUsedRows(aDate: TDate);
var
  ParentNbr: Integer;
  ChildNbr: Integer;
begin
  inherited;

  TevClientDataSet(wwdsDetail.DataSet).DisableControls;
  TevClientDataSet(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH).DisableControls;
  wwdsDetail.DataSet.First;
  while not wwdsDetail.DataSet.Eof do
  begin
    if (wwdsDetail.DataSet.FieldByName('PR_NBR').AsString = '') and (DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_CHECK_DATE').Value >= aDate) then
    begin
      DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RetrieveCondition := 'PR_SCHEDULED_EVENT_NBR='+DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('PR_SCHEDULED_EVENT_NBR').AsString;
      DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.First;
      while not DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Eof do  //DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RecordCount <> 0 do
      begin
        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Delete;
        ChildNbr := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RecNo;
        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Resync([]);
        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RecNo := ChildNbr;
      end;
      DM_PAYROLL.PR_SCHEDULED_EVENT.Delete;
      ParentNbr := DM_PAYROLL.PR_SCHEDULED_EVENT.RecNo;
      DM_PAYROLL.PR_SCHEDULED_EVENT.Resync([]);
      DM_PAYROLL.PR_SCHEDULED_EVENT.RecNo := ParentNbr;
    end
    else
      DM_PAYROLL.PR_SCHEDULED_EVENT.Next
  end;

  try
   ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH, TevClientDataSet(wwdsDetail.DataSet)]);
  finally
    TevClientDataSet(wwdsDetail.DataSet).EnableControls;
    TevClientDataSet(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH).EnableControls;
  end;

end;

function TEDIT_CO_SCHEDULER.ReturnCorrectFrequency(S: String): String;
begin
  if S = 'W' then
    Result := 'A'
  else if S = 'B' then
    Result := 'B'
  else if S = 'S' then
     Result := 'C'
  else if S = 'Q' then
     Result := 'Q'
  else
    Result := 'D';
end;

procedure TEDIT_CO_SCHEDULER.ConvertUsedRowsToNewFormat;
var
  MyTransaction: TTransactionManager;
begin
  inherited;

  MyTransaction := TTransactionManager.Create(Self);
  MyTransAction.Initialize([DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH, TevClientDataSet(wwdsDetail.DataSet)]);

  TevClientDataSet(wwdsDetail.DataSet).DisableControls;
  TevClientDataSet(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH).DisableControls;


  wwdsDetail.DataSet.First;
  while not wwdsDetail.DataSet.Eof do
  begin
    if wwdsDetail.DataSet.FieldByName('PR_NBR').AsString <> '' then
    begin
      DM_PAYROLL.PR_BATCH.RetrieveCondition := 'PR_NBR='+DM_PAYROLL.PR_SCHEDULED_EVENT.PR_NBR.AsString;
      DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RetrieveCondition := 'PR_SCHEDULED_EVENT_NBR='+DM_PAYROLL.PR_SCHEDULED_EVENT.PR_SCHEDULED_EVENT_NBR.AsString;
      if not DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Locate('PR_SCHEDULED_EVENT_NBR;FREQUENCY', VarArrayOf([DM_PAYROLL.PR_SCHEDULED_EVENT['PR_SCHEDULED_EVENT_NBR'], DM_PAYROLL.PR_BATCH['FREQUENCY']]), []) then
      begin
        if (evDataSource1.DataSet.RecordCount <> 0)
        and not DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Locate('PR_SCHEDULED_EVENT_NBR;FREQUENCY',
          VarArrayOf([DM_PAYROLL.PR_SCHEDULED_EVENT.PR_SCHEDULED_EVENT_NBR.Value, ReturnCorrectFrequency(evDataSource1.DataSet['FREQUENCY'])]), []) then
        begin
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Insert;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.PR_SCHEDULED_EVENT_NBR.AsInteger := DM_PAYROLL.PR_SCHEDULED_EVENT.PR_SCHEDULED_EVENT_NBR.AsInteger;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.PERIOD_BEGIN_DATE.AsDateTime := evDataSource1.DataSet.FieldByName('PERIOD_BEGIN_DATE').AsDateTime;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.PERIOD_END_DATE.AsDateTime := evDataSource1.DataSet.FieldByName('PERIOD_END_DATE').AsDateTime;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FREQUENCY.AsString := ReturnCorrectFrequency(evDataSource1.DataSet.FieldByName('FREQUENCY').AsString);
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.SCHEDULED_CALL_IN_DATE.AsDateTime := DM_PAYROLL.PR_SCHEDULED_EVENT.SCHEDULED_CHECK_DATE.AsDateTime-2;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Post;
        end;
      end
      else
      begin
        if (evDataSource1.DataSet.RecordCount <> 0)
        and ((DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.PERIOD_BEGIN_DATE.AsDateTime <> DM_PAYROLL.PR_BATCH.PERIOD_BEGIN_DATE.AsDateTime)
              or (DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.PERIOD_END_DATE.AsDateTime <> DM_PAYROLL.PR_BATCH.PERIOD_END_DATE.AsDateTime)) then
        begin
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Edit;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.PERIOD_BEGIN_DATE.AsDateTime := DM_PAYROLL.PR_BATCH.PERIOD_BEGIN_DATE.AsDateTime;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FREQUENCY.AsString := ReturnCorrectFrequency(evDataSource1.DataSet.FieldByName('FREQUENCY').AsString);
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.PERIOD_END_DATE.AsDateTime := DM_PAYROLL.PR_BATCH.PERIOD_END_DATE.AsDateTime;
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Post;
        end;
      end;
    end;
    wwdsDetail.DataSet.Next;
  end;

  try
    MyTransaction.ApplyUpdates;
  finally
    TevClientDataSet(wwdsDetail.DataSet).EnableControls;
    TevClientDataSet(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH).EnableControls;
    MyTransaction.Free;
  end;

end;

procedure TEDIT_CO_SCHEDULER.AfterDataSetsReopen;
begin
  inherited;
  if (wwdsList.DataSet.RecordCount <= 0) then
    exit;

  FPayFrequency := DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString;

  if FPayFrequency = '' then
    raise EInconsistentData.CreateHelp('Pay frequency field has been left blank, this field is required to create a calendar!',
       IDH_InconsistentData);

  DetermineTabsheetsToShow;

  if mb_AppSettings['Settings\AutoJump'] <> 'Y' then
    PageControl1.ActivePageIndex := 1;
end;

procedure TEDIT_CO_SCHEDULER.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if PageControl1.ActivePage = TabSheet3 then
    case Kind of
      NavDelete:
      begin
        if DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('PR_NBR').AsString <> ''  then
        begin
          DM_PAYROLL.PR_SCHEDULED_EVENT.Cancel;
          raise EUpdateError.CreateHelp('Cannot delete any entries that have processed payrolls attatched!', IDH_ConsistencyViolation);
        end;
      end;
    end
    else
      case Kind of
        NavOK: evCreateCalendar.Click;
      end;
end;

procedure TEDIT_CO_SCHEDULER.Activate;
begin
  inherited;
  Calendar.CreateDataSet;
  LoadHolidayList;
end;

procedure TEDIT_CO_SCHEDULER.GetDefaultsIfAnyExist;
var
  CoNbr, DAYS_PRIOR, DAYS_POST, DAYS_PRIOR2, DAYS_POST2: Integer;
  CheckDate, PeriodBegin, PeriodEnd, CheckDate2, PeriodBegin2, PeriodEnd2: TDate;
  Callin, Delivery, CallIn2, Delivery2: TDateTime;
  CalendarDefaults: TCalendarDefaults;
begin
  evcbMoveCheck.ItemIndex := 2;
  evcbMoveCallIn.ItemIndex := 2;
  evcbMoveDelivery.ItemIndex := 2;

  evCalculateDay.ItemIndex := 0;


  CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
  ctx_DataAccess.CUSTOM_VIEW.Close;


 //Weekly
  if tshtWeekly.TabVisible then
  begin
    if GetDefaultsFromTable(CoNbr, CO_FREQ_WEEKLY, CalendarDefaults) then
    begin
      CheckCalendarDefaults(CalendarDefaults, CO_FREQ_WEEKLY);
      evseNbrMonths.Value            := CalendarDefaults.NUMBER_OF_MONTHS;
      evdpWeeklyCheckDate.DateTime   := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
      evdpWeeklyPeriodBegin.DateTime := CalendarDefaults.PERIOD_BEGIN_DATE;
      evdpWeeklyPeriodEnd.DateTime   := CalendarDefaults.PERIOD_END_DATE;
      evdpWeekCallInTime.Time        := CalendarDefaults.CALL_IN_TIME;
      evdpWeeklyDeliveryTime.Time    := CalendarDefaults.DELIVERY_TIME;
      evseWeekDaysPrior.Value        := CalendarDefaults.NUMBER_OF_DAYS_PRIOR;
      evseWeeklyDaysAfter.Value      := CalendarDefaults.NUMBER_OF_DAYS_AFTER;

      evcbMoveCheck.ItemIndex        := ReturnCBIndex(CalendarDefaults.MOVE_CHECK_DATE);
      evcbMoveCallIn.ItemIndex       := ReturnCBIndex(CalendarDefaults.MOVE_CALLIN_DATE);
      evcbMoveDelivery.ItemIndex     := ReturnCBIndex(CalendarDefaults.MOVE_DELIVERY_DATE);

      if ExtractFromFiller(CalendarDefaults.FILLER, 1, 1) = 'B' then
        evCalculateDay.ItemIndex := 1;

    end
    else
    begin
      GetWeekly_BiWeeklyDefaults(CoNbr, 7, CO_FREQ_WEEKLY, CheckDate, PeriodBegin, PeriodEnd, Callin, Delivery, DAYS_PRIOR, DAYS_POST);
      evdpWeeklyCheckDate.DateTime   := CheckDate;
      evdpWeeklyPeriodBegin.DateTime := PeriodBegin;
      evdpWeeklyPeriodEnd.DateTime   := PeriodEnd;
      evdpWeekCallInTime.Time        := CallIn;
      evdpWeeklyDeliveryTime.Time    := Delivery;
      evseWeekDaysPrior.Value        := DAYS_PRIOR;
      evseWeeklyDaysAfter.Value      := DAYS_POST;
    end;
  end;

  //Bi-Weekly
  if tshtBIWeekly.TabVisible then
  begin
    if GetDefaultsFromTable(CoNbr, CO_FREQ_BWEEKLY, CalendarDefaults) then
    begin
      CheckCalendarDefaults(CalendarDefaults, CO_FREQ_BWEEKLY);
      evseNbrMonths.Value             := CalendarDefaults.NUMBER_OF_MONTHS;
      evdpBWeeklyCheckDate.DateTime   := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
      evdpBWeeklyPeriodBegin.DateTime := CalendarDefaults.PERIOD_BEGIN_DATE;
      evdpBWeeklyPeriodEnd.DateTime   := CalendarDefaults.PERIOD_END_DATE;
      evdpBWeekCallInTime.Time        := CalendarDefaults.CALL_IN_TIME;
      evdpMonthlyDeliveryTime.Time    := CalendarDefaults.DELIVERY_TIME;
      evseBWeekDaysPrior.Value        := CalendarDefaults.NUMBER_OF_DAYS_PRIOR;
      evseBWeeklyDaysAfter.Value      := CalendarDefaults.NUMBER_OF_DAYS_AFTER;

      evcbMoveCheck.ItemIndex         := ReturnCBIndex(CalendarDefaults.MOVE_CHECK_DATE);
      evcbMoveCallIn.ItemIndex        := ReturnCBIndex(CalendarDefaults.MOVE_CALLIN_DATE);
      evcbMoveDelivery.ItemIndex      := ReturnCBIndex(CalendarDefaults.MOVE_DELIVERY_DATE);

      if ExtractFromFiller(CalendarDefaults.FILLER, 1, 1) = 'B' then
        evCalculateDay.ItemIndex := 1;

    end
    else
    begin
      GetWeekly_BiWeeklyDefaults(CoNbr, 14, CO_FREQ_BWEEKLY, CheckDate, PeriodBegin, PeriodEnd, Callin, Delivery, DAYS_PRIOR, DAYS_POST);
      evdpBWeeklyCheckDate.DateTime   :=  CheckDate;
      evdpBWeeklyPeriodBegin.DateTime :=  PeriodBegin;
      evdpBWeeklyPeriodEnd.DateTime   :=  PeriodEnd;
      evdpBWeekCallInTime.Time        :=  CallIn;
      evdpMonthlyDeliveryTime.Time    :=  Delivery;
      evseBWeekDaysPrior.Value        :=  DAYS_PRIOR;
      evseBWeeklyDaysAfter.Value      :=  DAYS_POST;
    end;
  end;

  //Semi-Montly
  if tshtSemiMonthly.TabVisible then
  begin
    if GetDefaultsFromTable(CoNbr, CO_FREQ_SMONTHLY, CalendarDefaults) then
    begin
      CheckCalendarDefaults(CalendarDefaults, CO_FREQ_SMONTHLY);
      evseNbrMonths.Value               := CalendarDefaults.NUMBER_OF_MONTHS;
      evdpSMonthlyCheckDate.DateTime    := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
      evdpSMonthlyPeriodBegin.DateTime  := CalendarDefaults.PERIOD_BEGIN_DATE;
      evdpSMonthlyPeriodEnd.DateTime    := CalendarDefaults.PERIOD_END_DATE;
      evdpSMonthlyCallInTime.Time       := CalendarDefaults.CALL_IN_TIME;
      evdpBMonthlyDeliveryTime.Time     := CalendarDefaults.DELIVERY_TIME;
      evseSMonthlyDaysPrior.Value       := CalendarDefaults.NUMBER_OF_DAYS_PRIOR;
      evseSMonthlyDaysAfter.Value       := CalendarDefaults.NUMBER_OF_DAYS_AFTER;

      evdpSMonthlyCheckDate2.DateTime   := CalendarDefaults.LAST_REAL_SCHED_CHECK_DATE2;
      evdpSMonthlyPeriodBegin2.DateTime := CalendarDefaults.PERIOD_BEGIN_DATE2;
      evdpSMonthlyPeriodEnd2.DateTime   := CalendarDefaults.PERIOD_END_DATE2;
      evdpSMonthlyCallInTime2.Time      := CalendarDefaults.CALL_IN_TIME2;
      evdpBMonthlyDeliveryTime2.Time    := CalendarDefaults.DELIVERY_TIME2;
      evseSMonthlyDaysPrior2.Value      := CalendarDefaults.NUMBER_OF_DAYS_PRIOR2;
      evseSMonthlyDaysAfter2.Value      := CalendarDefaults.NUMBER_OF_DAYS_AFTER2;

      evcbMoveCheck.ItemIndex           := ReturnCBIndex(CalendarDefaults.MOVE_CHECK_DATE);
      evcbMoveCallIn.ItemIndex          := ReturnCBIndex(CalendarDefaults.MOVE_CALLIN_DATE);
      evcbMoveDelivery.ItemIndex        := ReturnCBIndex(CalendarDefaults.MOVE_DELIVERY_DATE);

      if ExtractFromFiller(CalendarDefaults.FILLER, 1, 1) = 'B' then
        evCalculateDay.ItemIndex := 1;

    end
    else
    begin
      GetSemiMonthlyDefaults(CoNbr, CO_FREQ_SMONTHLY, CheckDate, PeriodBegin, PeriodEnd, CallIn, Delivery, CheckDate2, PeriodBegin2, PeriodEnd2, CallIn2, Delivery2, DAYS_PRIOR, DAYS_POST, DAYS_PRIOR2, DAYS_POST2);
      evdpSMonthlyCheckDate.DateTime    := CheckDate;
      evdpSMonthlyPeriodBegin.DateTime  := PeriodBegin;
      evdpSMonthlyPeriodEnd.DateTime    := PeriodEnd;
      evdpSMonthlyCallInTime.Time       := Callin;
      evdpBMonthlyDeliveryTime.Time     := Delivery;
      evseSMonthlyDaysPrior.Value       := DAYS_PRIOR;
      evseSMonthlyDaysAfter.Value       := DAYS_POST;

      evdpSMonthlyCheckDate2.DateTime   := CheckDate2;
      evdpSMonthlyPeriodBegin2.DateTime := PeriodBegin2;
      evdpSMonthlyPeriodEnd2.DateTime   := PeriodEnd2;
      evdpSMonthlyCallInTime2.Time      := Callin2;
      evdpBMonthlyDeliveryTime2.Time    := Delivery2;
      evseSMonthlyDaysPrior2.Value      := DAYS_PRIOR2;
      evseSMonthlyDaysAfter2.Value      := DAYS_POST2;
    end;
  end;

  //Monthly
  if tshtMonthly.TabVisible then
  begin
    if GetDefaultsFromTable(CoNbr, CO_FREQ_MONTHLY, CalendarDefaults) then
    begin
      CheckCalendarDefaults(CalendarDefaults, CO_FREQ_MONTHLY);
      evseNbrMonths.Value             := CalendarDefaults.NUMBER_OF_MONTHS;
      evdpMonthlyCheckDate.DateTime   := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
      evdpMonthlyPeriodBegin.DateTime := CalendarDefaults.PERIOD_BEGIN_DATE;
      evdpMonthlyPeriodEnd.DateTime   := CalendarDefaults.PERIOD_END_DATE;
      evdpMonthlyCallInTime.Time      := CalendarDefaults.CALL_IN_TIME;
      evdpMonthlyDeliveryTime.Time    := CalendarDefaults.DELIVERY_TIME;
      evseMonthlyDaysPrior.Value      := CalendarDefaults.NUMBER_OF_DAYS_PRIOR;
      evseMonthlyDaysAfter.Value      := CalendarDefaults.NUMBER_OF_DAYS_AFTER;

      evcbMoveCheck.ItemIndex         := ReturnCBIndex(CalendarDefaults.MOVE_CHECK_DATE);
      evcbMoveCallIn.ItemIndex        := ReturnCBIndex(CalendarDefaults.MOVE_CALLIN_DATE);
      evcbMoveDelivery.ItemIndex      := ReturnCBIndex(CalendarDefaults.MOVE_DELIVERY_DATE);


      if ExtractFromFiller(CalendarDefaults.FILLER, 1, 1) = 'B' then
        evCalculateDay.ItemIndex := 1;

    end
    else
    begin
      GetMonthlyDefaults(CoNbr, CO_FREQ_MONTHLY, CheckDate, PeriodBegin, PeriodEnd, CallIn, Delivery, DAYS_PRIOR, DAYS_POST);
      evdpMonthlyCheckDate.DateTime   := CheckDate;
      evdpMonthlyPeriodBegin.DateTime := PeriodBegin;
      evdpMonthlyPeriodEnd.DateTime   := PeriodEnd;
      evdpMonthlyCallInTime.Time      := CallIn;
      evdpMonthlyDeliveryTime.Time    := Delivery;
      evseMonthlyDaysPrior.Value      := DAYS_PRIOR;
      evseMonthlyDaysAfter.Value      := DAYS_POST;
    end;
  end;

  //Quarterly
  if tshtQuarterly.TabVisible then
  begin
    if GetDefaultsFromTable(CoNbr, CO_FREQ_QUARTERLY, CalendarDefaults) then
    begin
      CheckCalendarDefaults(CalendarDefaults, CO_FREQ_QUARTERLY);
      evseNbrMonths.Value               := CalendarDefaults.NUMBER_OF_MONTHS;
      evdpQuarterlyCheckDate.DateTime   := CalendarDefaults.LAST_REAL_SCHEDULED_CHECK_DATE;
      evdpQuarterlyPeriodBegin.DateTime := CalendarDefaults.PERIOD_BEGIN_DATE;
      evdpQuarterlyPeriodEnd.DateTime   := CalendarDefaults.PERIOD_END_DATE;
      evdpQuarterlyCallInTime.Time      := CalendarDefaults.CALL_IN_TIME;
      evdpQuarterlyDeliveryTime.Time    := CalendarDefaults.DELIVERY_TIME;
      evseQuarterlyDaysPrior.Value      := CalendarDefaults.NUMBER_OF_DAYS_PRIOR;
      evseQuarterlyDaysAfter.Value      := CalendarDefaults.NUMBER_OF_DAYS_AFTER;

      evcbMoveCheck.ItemIndex           := ReturnCBIndex(CalendarDefaults.MOVE_CHECK_DATE);
      evcbMoveCallIn.ItemIndex          := ReturnCBIndex(CalendarDefaults.MOVE_CALLIN_DATE);
      evcbMoveDelivery.ItemIndex        := ReturnCBIndex(CalendarDefaults.MOVE_DELIVERY_DATE);

      if ExtractFromFiller(CalendarDefaults.FILLER, 1, 1) = 'B' then
        evCalculateDay.ItemIndex := 1;

    end
    else
    begin
      GetQuarterlyDefaults(CoNbr, CO_FREQ_QUARTERLY, CheckDate, PeriodBegin, PeriodEnd, CallIn, Delivery, DAYS_PRIOR, DAYS_POST);
      evdpQuarterlyCheckDate.DateTime   := CheckDate;
      evdpQuarterlyPeriodBegin.DateTime := PeriodBegin;
      evdpQuarterlyPeriodEnd.DateTime   := PeriodEnd;
      evdpQuarterlyCallInTime.Time      := CallIn;
      evdpQuarterlyDeliveryTime.Time    := Delivery;
      evseQuarterlyDaysPrior.Value      := DAYS_PRIOR;
      evseQuarterlyDaysAfter.Value      := DAYS_POST;
    end;
  end;

  evCalculateDayClick(Self);   //   To control Calculateind Day options...

end;

procedure TEDIT_CO_SCHEDULER.DetermineTabsheetsToShow;
var
  TS: TTabSheet;
begin
  evpcFrequency.ActivePage := nil;
  tshtWeekly.TabVisible := False;
  tshtBIWeekly.TabVisible := False;
  tshtSemiMonthly.TabVisible := False;
  tshtMonthly.TabVisible := False;
  tshtQuarterly.TabVisible := False;

  TS := nil;

  if HasWeekly(FPayFrequency[1]) then
  begin
    tshtWeekly.TabVisible := True;
    TS := tshtWeekly;
  end;

  if HasBiWeekly(FPayFrequency[1]) then
  begin
    tshtBIWeekly.TabVisible := True;
    if not Assigned(TS) then
      TS := tshtBIWeekly;
  end;

  if HasSemiMonthly(FPayFrequency[1]) then
  begin
    tshtSemiMonthly.TabVisible := True;
    if not Assigned(TS) then
      TS := tshtSemiMonthly;
  end;

  if HasMonthly(FPayFrequency[1]) then
  begin
    tshtMonthly.TabVisible := True;
    if not Assigned(TS) then
      TS := tshtMonthly;
  end;

  if HasQuarterly(FPayFrequency[1]) then
  begin
    tshtQuarterly.TabVisible := True;
    if not Assigned(TS) then
      TS := tshtQuarterly;
  end;

  evpcFrequency.ActivePage := TS;

  GetDefaultsIfAnyExist;
end;

procedure TEDIT_CO_SCHEDULER.Deactivate;
begin

  inherited;
end;


procedure TEDIT_CO_SCHEDULER.evDBGrid1RowChanged(Sender: TObject);
begin
  inherited;
  if DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('PR_NBR').AsString<> '' then
  begin
    evDataSource1.DataSet.Filter := 'PR_NBR='+DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('PR_NBR').AsString;
    evDataSource1.DataSet.Filtered := True;
    evDataSource1.DataSet.Active := True;
  end
  else
    evDataSource1.DataSet.Active := False;
end;

procedure TEDIT_CO_SCHEDULER.evButton1Click(Sender: TObject);
var
  ClickedOK : Boolean;
  NewString: String;
  ValidDate: Boolean;
  aDate: TDate;
  BM: TBookMark;
begin
  inherited;

  BM := evDBGrid1.DataSource.DataSet.GetBookmark;

  ConvertUsedRowsToNewFormat;

  ValidDate := False;
  ClickedOK := True;
  aDate := StrToDate('01/01/2200');
  try
    evDBGrid1.DataSource.DataSet.GotoBookmark(BM);
    while (not ValidDate) and (ClickedOK) do
    begin
      NewString := DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('SCHEDULED_CHECK_DATE').AsString;
      ClickedOK := EvDialog('Beginning Date for Cleanup.', 'Date', NewString);
      Update;
      if ClickedOK then
      begin
        try
          aDate := StrToDateTime(NewString);
          ValidDate := True;
        except
          ValidDate := False;
          EvMessage('You have entered an invalid date. Please enter a valid date or choose cancel to exit cleanup!');
        end;
      end;
      DeleteNonUsedRows(aDate);
    end;
  finally
    evDBGrid1.DataSource.DataSet.FreeBookmark(BM);
  end;
end;

procedure TEDIT_CO_SCHEDULER.TabSheet3Show(Sender: TObject);
begin
  inherited;

  Update;

  with DM_COMPANY, DM_PAYROLL do
  begin
    PR_SCHEDULED_EVENT.DataRequired('CO_NBR='+CO.FieldByName('CO_NBR').AsString);
    PR_SCHEDULED_EVENT_BATCH.DataRequired('ALL');
    PR_BATCH.DataRequired('ALL');
    PR_SCHEDULED_EVENT.First;
  end;
  evDBGrid2.SetControlType('FREQUENCY',  fctCustom, 'evDBComboBox2');
end;

procedure TEDIT_CO_SCHEDULER.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  evDBDateTimePicker4.SetFocus;
  if evDataSource2.DataSet.State in [dsInsert, dsEdit] then
  begin
    evDataSource2.DataSet.Post;
  end;
  if evDataSource2.DataSet.State in [dsBrowse] then
  begin
    evDataSource2.DataSet.Insert;
  end;
end;

procedure TEDIT_CO_SCHEDULER.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  if (evDataSource2.DataSet.State in [dsBrowse]) and (evDataSource2.DataSet.RecordCount > 0) then
  begin
    if EvMessage('Delete record.  Are you sure?',
      mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      evDataSource2.DataSet.Delete;
    end;
  end;
end;

procedure TEDIT_CO_SCHEDULER.evdpWeeklyPeriodBeginChange(Sender: TObject);
begin
  inherited;
  evdpWeeklyPeriodEnd.DateTime := evdpWeeklyPeriodBegin.DateTime+6;
end;

procedure TEDIT_CO_SCHEDULER.evdpWeeklyPeriodEndExit(Sender: TObject);
begin
  inherited;
  if evdpWeeklyPeriodEnd.DateTime <> evdpWeeklyPeriodBegin.DateTime+6 then
    if EvMessage('By changing your pay period, you may not be including all days of the month.  Be sure your calendar includes the 12th of the month for quarterly reporting.  If you need more information please contact your tax department. Fix it?',
                 mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      evdpWeeklyPeriodEnd.DateTime := evdpWeeklyPeriodBegin.DateTime+6;
    end;
end;

procedure TEDIT_CO_SCHEDULER.evdpBWeeklyPeriodBeginChange(Sender: TObject);
begin
  inherited;
  evdpBWeeklyPeriodEnd.DateTime := evdpBWeeklyPeriodBegin.DateTime+13;
end;

procedure TEDIT_CO_SCHEDULER.evdpBWeeklyPeriodEndExit(Sender: TObject);
begin
  inherited;
  if evdpBWeeklyPeriodEnd.DateTime <> evdpBWeeklyPeriodBegin.DateTime+13 then
    if EvMessage('By changing your pay period, you may not be including all days of the month.  Be sure your calendar includes the 12th of the month for quarterly reporting.  If you need more information please contact your tax department. Fix it?',
                 mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      evdpBWeeklyPeriodEnd.DateTime := evdpBWeeklyPeriodBegin.DateTime+13;
    end;
end;


procedure TEDIT_CO_SCHEDULER.evCalculateDayClick(Sender: TObject);
begin
  inherited;

   evcbMoveCallIn.Visible := True;
   evcbMoveDelivery.Visible := True;
   Label2.Visible := True;
   Label3.Visible := True;
   if evCalculateDay.ItemIndex = 1 then
    Begin
     evcbMoveCallIn.Visible := False;
     evcbMoveDelivery.Visible := False;
     Label2.Visible := False;
     Label3.Visible := False;
    end;

end;

procedure TEDIT_CO_SCHEDULER.fpCalendarLeftResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  evPanel2.Width  := fpCalendarLeft.Width - 40;
  evPanel2.Height := fpCalendarLeft.Height - 65;
end;

procedure TEDIT_CO_SCHEDULER.fpCalendarRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  evPanel3.Width  := fpCalendarRight.Width - 40;
  evPanel3.Height := fpCalendarRight.Height - 65;
end;

initialization
  RegisterClass(TEDIT_CO_SCHEDULER);

end.
