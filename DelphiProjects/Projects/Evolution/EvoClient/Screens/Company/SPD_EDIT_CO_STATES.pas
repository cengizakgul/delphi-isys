// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_STATES;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, StdCtrls, ExtCtrls,
  DBCtrls, wwdbdatetimepicker, Wwdotdot, Wwdbcomb, wwdblook, Mask, EvBasicUtils,
  wwdbedit, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Controls, Classes, SPackageEntry, EvUtils, sysutils, EvSecElement, Forms,
  SPD_MiniNavigationFrame, ActnList, EvTypes, menus, Variants, EvContext,
  SDDClasses, Types, ISBasicClasses, SDataDictsystem, SDataDictclient,
  SDataDicttemp,SfieldCodeValues,sCompanyShared, EvExceptions, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  EvCommonInterfaces, EvDataset, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBEdit, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton,
  isUIwwDBDateTimePicker;

type
  TEDIT_CO_STATES = class(TEDIT_CO_BASE)
    tshtGeneral_Info: TTabSheet;
    tshtFlag_And_Settings: TTabSheet;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    SyStatesSrc: TevDataSource;
    SyStatesFreqSrc: TevDataSource;
    TabSheet2: TTabSheet;
    dsSUI: TevDataSource;
    SySUISrc: TevDataSource;
    evSecElement1: TevSecElement;
    pmStateDepositFrequency: TevPopupMenu;
    CopyTo1: TMenuItem;
    pmSUITaxDepositFrequency: TevPopupMenu;
    MenuItem1: TMenuItem;
    cdTCD_CollectionAgency: TevClientDataSet;
    dsTaxCollection: TevDataSource;
    pmTCDDepositFrequency: TevPopupMenu;
    MenuItem3: TMenuItem;
    TCDCopyService: TevActionList;
    CopyService: TAction;
    TabSheet3: TTabSheet;
    dsLocations: TevDataSource;
    evSpeedButton1: TevSpeedButton;
    fpStatesRight: TisUIFashionPanel;
    sbDetails: TScrollBox;
    fpState: TisUIFashionPanel;
    lablState: TevLabel;
    lablState_EIN: TevLabel;
    lablState_SDI_EIN: TevLabel;
    lablState_Deposit_Frequency: TevLabel;
    lablState_Tax_Deposit_Method: TevLabel;
    lablState_EFT_Name: TevLabel;
    lablState_EFT_Enrollment_Status: TevLabel;
    lablState_EFT_PIN_Number: TevLabel;
    lablSTATE_EFT_EIN: TevLabel;
    lablSTATE_SDI_EFT_EIN: TevLabel;
    lNYThresholdLabel: TevLabel;
    evLabel2: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    dedtState_EIN: TevDBEdit;
    dedtState_SDI_EIN: TevDBEdit;
    wwlcState: TevDBLookupCombo;
    wwcbState_Tax_Deposit_Method: TevDBComboBox;
    wwDBLookupCombo1: TevDBLookupCombo;
    dedtState_EFT_Name: TevDBEdit;
    wwcbState_EFT_Enrollment_Status: TevDBComboBox;
    dedtState_EFT_PIN_Number: TevDBEdit;
    dedtSTATE_EFT_EIN: TevDBEdit;
    dedtSTATE_SDI_EFT_EIN: TevDBEdit;
    lNYThresholdPicker: TevDateTimePicker;
    butnAttachState: TevBitBtn;
    cbAppliedFor: TevDBCheckBox;
    cbTCDPaymentMethod: TevDBComboBox;
    cbTaxCollection: TevDBLookupCombo;
    cbTCDDEpositFrequency: TevDBLookupCombo;
    fpNotes: TisUIFashionPanel;
    lablNotes: TevLabel;
    dmemNotes: TEvDBMemo;
    sbFlags: TScrollBox;
    isUIFashionPanel1: TisUIFashionPanel;
    lablTax_Return_Code: TevLabel;
    lablUse_DBA_Tax_Return: TevLabel;
    drgpIgnore_State_Tax_Dep_Threshold: TevDBRadioGroup;
    drgpState_Non_Profit: TevDBRadioGroup;
    drgpState_Exempt: TevDBRadioGroup;
    drgpSUI_Exempt: TevDBRadioGroup;
    drgpEE_SDI_Exempt: TevDBRadioGroup;
    drgpUse_State_For_SUI: TevDBRadioGroup;
    dedtTax_Return_Code: TevDBEdit;
    wwcbUse_DBA_Tax_Return: TevDBComboBox;
    evDBRadioGroup2: TevDBRadioGroup;
    drgpER_SDI_Exempt: TevDBRadioGroup;
    evDBRadioGroup3: TevDBRadioGroup;
    drgpFinal_Tax_Return: TevDBRadioGroup;
    fpGLTags: TisUIFashionPanel;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    DBEdit1: TevDBEdit;
    DBEdit2: TevDBEdit;
    DBEdit3: TevDBEdit;
    DBEdit6: TevDBEdit;
    DBEdit7: TevDBEdit;
    DBEdit8: TevDBEdit;
    ScrollBox1: TScrollBox;
    evPanel1: TevPanel;
    fpSummary: TisUIFashionPanel;
    wwDBGrid3: TevDBGrid;
    fpSUI: TisUIFashionPanel;
    lablSUI_Tax_Deposit_Method: TevLabel;
    lablSUI_EIN: TevLabel;
    lablSUI_Tax_Deposit_Frequency: TevLabel;
    lablSUI_EFT_Name: TevLabel;
    lablSUI_EFT_Enrollment_Status: TevLabel;
    lablSUI_EFT_PIN_Number: TevLabel;
    lablSUI_EFT_EIN: TevLabel;
    wwcbSUI_Tax_Deposit_Method: TevDBComboBox;
    dedtSUI_EIN: TevDBEdit;
    wwcbSUI_Tax_Deposit_Frequency: TevDBComboBox;
    dedtSUI_EFT_Name: TevDBEdit;
    wwcbSUI_EFT_Enrollment_Status: TevDBComboBox;
    dedtSUI_EFT_PIN_Number: TevDBEdit;
    dedtSUI_EFT_EIN: TevDBEdit;
    isUIFashionPanel2: TisUIFashionPanel;
    lablSY_SUI_Number: TevLabel;
    evLabel3: TevLabel;
    lablDescription: TevLabel;
    lablRate: TevLabel;
    evLabel4: TevLabel;
    lablPayment_Method: TevLabel;
    SpeedButton1: TevSpeedButton;
    evLabel10: TevLabel;
    evLabel27: TevLabel;
    evLabel1: TevLabel;
    wwlcSY_SUI_Number: TevDBLookupCombo;
    evDBEdit3: TevDBEdit;
    dedtDescription: TevDBEdit;
    wwdeRate: TevDBEdit;
    wwcbPayment_Method: TevDBComboBox;
    MiniNavigationFrame: TMiniNavigationFrame;
    butnAttachSUI: TevBitBtn;
    evDBEdit5: TevDBEdit;
    evDBEdit2: TevDBEdit;
    dblkupGLOBAL_RATE: TevDBLookupCombo;
    evDBRadioGroup4: TevDBRadioGroup;
    rgAlternateWage: TevDBRadioGroup;
    evDBCheckBox1: TevDBCheckBox;
    evDBRadioGroup5: TevDBRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    ScrollBox2: TScrollBox;
    fpPASummary: TisUIFashionPanel;
    wwdgCompany_Locs: TevDBGrid;
    fpDetails: TisUIFashionPanel;
    evLabel8: TevLabel;
    Label62: TevLabel;
    Label68: TevLabel;
    Label67: TevLabel;
    Label66: TevLabel;
    Label65: TevLabel;
    AddLocationBtn: TevSpeedButton;
    DelLocationBtn: TevSpeedButton;
    evDBEdit1: TevDBEdit;
    DBEdit27: TevDBEdit;
    DBEdit28: TevDBEdit;
    DBEdit29: TevDBEdit;
    DBEdit30: TevDBEdit;
    DBEdit31: TevDBEdit;
    evLabel36: TevLabel;
    evLabel55: TevLabel;
    evLabel56: TevLabel;
    evLabel7: TevLabel;
    wwDBGrid1: TevDBGrid;
    pmTCDDistrict: TevPopupMenu;
    MenuItem2: TMenuItem;
    pmTCDPaymentMethod: TevPopupMenu;
    MenuItem4: TMenuItem;
    lblLastReviewedDate: TevLabel;
    edtLastReviewedDate: TevDBDateTimePicker;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwlcStateChange(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure wwdsDetailDataChangeBeforeChildren(Sender: TObject;
      Field: TField);
    procedure butnAttachSUIClick(Sender: TObject);
    procedure wwlcSY_SUI_NumberChange(Sender: TObject);
    procedure dsSUIStateChange(Sender: TObject);
    procedure lNYThresholdPickerChange(Sender: TObject);
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure MiniNavigationFrameInsertRecordExecute(Sender: TObject);
    procedure CopyStateTaxDeposit(Sender: TObject);
    procedure CopySUIStateTaxDeposit(Sender: TObject);
    procedure dsSUIDataChange(Sender: TObject; Field: TField);
    procedure drgpIgnore_State_Tax_Dep_ThresholdChange(Sender: TObject);
    procedure MiniNavigationFrameSpeedButton1Click(Sender: TObject);
    procedure tshtGeneral_InfoShow(Sender: TObject);
    procedure wwcbState_Tax_Deposit_MethodDropDown(Sender: TObject);
    procedure wwcbSUI_Tax_Deposit_MethodDropDown(Sender: TObject);
    procedure wwcbPayment_MethodDropDown(Sender: TObject);
    procedure wwcbState_Tax_Deposit_MethodChange(Sender: TObject);
    procedure wwcbSUI_Tax_Deposit_MethodChange(Sender: TObject);
    procedure wwcbPayment_MethodChange(Sender: TObject);
    procedure CopyServiceExecute(Sender: TObject);
    procedure CopyTCD;
    procedure dsTaxCollectionDataChange(Sender: TObject; Field: TField);
    procedure cbTCDDEpositFrequencyEnter(Sender: TObject);
    procedure MiniNavigationFrameSpeedButton2Click(Sender: TObject);
    procedure AddLocationBtnClick(Sender: TObject);
    procedure DelLocationBtnClick(Sender: TObject);
    procedure fpStatesRightResize(Sender: TObject);
    procedure cbTaxCollectionMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PageControl1Change(Sender: TObject);
  private
    procedure AttachStateToPayrolls(COStateNbr: Integer; BeginCheckDate, EndCheckDate: TDateTime);
    function CanChangeOrDeleteSUI: Boolean;
    procedure OnDeleteSUIRecordConfirm(Sender: TObject; var Allow: boolean);
    procedure CopyTaxDepositFrequency(stateOrSUI: boolean);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    function CanChangeOrDelete: Boolean;
    procedure AfterDataSetsReopen; override;
    function GetDataSetConditions(sName: string): string; override;

  public
    control_Sy_State_Deposit : Boolean;  
    GL_Setuped : boolean;
    GL_Setuped_st : boolean;
    constructor Create(AOwner: TComponent); override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
      var InsertDataSets, EditDataSets: TArrayDS;
      var DeleteDataSet: TevClientDataSet;
      var SupportDataSets: TArrayDS); override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;

    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick( Kind: Integer ); override;
    procedure Activate; override;

  end;


implementation

uses
  Dialogs, SSecurityInterface, EvConsts, SPD_CompanyChoiceQuery, SDueDateCalculation;

{$R *.DFM}

procedure TEDIT_CO_STATES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.Close;
  inherited;
  AddDS(DM_COMPANY.CO_SUI, aDS);
  AddDS(DM_COMPANY.CO_STATE_TAX_LIABILITIES, aDS);
  AddDS(DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ, aDS);
  AddDS(DM_COMPANY.CO_LOCATIONS, aDS);
end;

procedure TEDIT_CO_STATES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  DM_SYSTEM.SY_AGENCY_DEPOSIT_FREQ.Close;
  cdTCD_CollectionAgency.Close;
  cdTCD_CollectionAgency.ProviderName := DM_SYSTEM_STATE.SY_GLOBAL_AGENCY.ProviderName;
  AddDSWithCheck(cdTCD_CollectionAgency, SupportDataSets, 'AGENCY_TYPE = ''Y''');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_GLOBAL_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATES, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_SUI, SupportDataSets, '');
  AddDSWithCheck(DM_COMPANY.CO_LOCATIONS, EditDataSets, '');

  AddDSWithCheck(DM_COMPANY.CO_SUI, EditDataSets, GetDataSetConditions('CO_SUI'));
  AddDSWithCheck(DM_COMPANY.CO_STATE_TAX_LIABILITIES, EditDataSets, GetDataSetConditions('CO_STATE_TAX_LIABILITIES'));
end;


function TEDIT_CO_STATES.GetDataSetConditions(sName: string): string;
begin
  Result := inherited GetDataSetConditions(sName);
  if (sName = 'CO_STATE_TAX_LIABILITIES') and (Result <> '') then
    Result := Result + ' and CO_TAX_DEPOSITS_NBR is NULL'
  else
  if (sName = 'SY_AGENCY_DEPOSIT_FREQ') then
    Result := '';
end;

function TEDIT_CO_STATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_STATES;
end;

function TEDIT_CO_STATES.GetInsertControl: TWinControl;
begin
  Result := wwlcSTATE;
end;

procedure TEDIT_CO_STATES.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;

  case Kind of
     NavInsert:
     begin
       GL_Setuped_St := CheckGlSetupExist(DM_CLIENT.CO_STATES,
                      ['STATE_GENERAL_LEDGER_TAG',
                       'STATE_OFFSET_GL_TAG',
                       'EE_SDI_GENERAL_LEDGER_TAG',
                       'EE_SDI_OFFSET_GL_TAG',
                       'ER_SDI_GENERAL_LEDGER_TAG',
                       'ER_SDI_OFFSET_GL_TAG'],false);
     end;
    NavDelete:
      begin
        if not CanChangeOrDelete then
        begin
          DM_COMPANY.CO_STATES.Cancel;
          raise EUpdateError.CreateHelp('Cannot delete this state because it''s in use!', IDH_ConsistencyViolation);
        end;
      end;
    NavOk:
      begin
        if TDataSet(DM_CLIENT.CO_STATES).State = dsInsert then
          if GL_Setuped_St and not CheckGlSetuped(DM_CLIENT.CO_STATES,
                        ['STATE_GENERAL_LEDGER_TAG',
                         'STATE_OFFSET_GL_TAG',
                         'EE_SDI_GENERAL_LEDGER_TAG',
                         'EE_SDI_OFFSET_GL_TAG',
                         'ER_SDI_GENERAL_LEDGER_TAG',
                         'ER_SDI_OFFSET_GL_TAG']) then
             EvMessage(sDontForgetSetupGL);
        if DM_CLIENT.CO_SUI.State = dsInsert then
          if GL_Setuped and not CheckGlSetuped(DM_CLIENT.CO_SUI,['GL_TAG','GL_OFFSET_TAG']) then
             EvMessage(sDontForgetSetupGL);
        GL_Setuped_St := false;
        GL_Setuped := false;


       if DM_CLIENT.CO_STATES.STATE.Value <> DM_SYSTEM_STATE.SY_STATES.STATE.Value then
          DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_CLIENT.CO_STATES['STATE'], []);
       if (not CheckPaymentMethodsChoice(DM_CLIENT.CO_STATES.STATE_TAX_DEPOSIT_METHOD,DM_SYSTEM_STATE.SY_STATES.STATE_EFT_TYPE))
         and not (DM_SERVICE_BUREAU.SB.FieldByName('PARENT_SB_MODEM_NUMBER').Value = 'W3LL5') then
       begin
          handled := false;
          raise EUpdateError.CreateHelp('Wrong payment method of State Tax Payment Method.', IDH_ConsistencyViolation);
       end;
       if(not CheckPaymentMethodsChoice(DM_CLIENT.CO_STATES.SUI_TAX_DEPOSIT_METHOD,DM_SYSTEM_STATE.SY_STATES.SUI_EFT_TYPE))
         and not (DM_SERVICE_BUREAU.SB.FieldByName('PARENT_SB_MODEM_NUMBER').Value = 'W3LL5') then
       begin
          handled := false;
          raise EUpdateError.CreateHelp('Wrong payment method of SUI Tax Payment Method.', IDH_ConsistencyViolation);
       end;

       if (DM_CLIENT.CO_SUI.state in [dsInsert, dsEdit]) and
          (wwdeRate.Text = '') then
           raise EUpdateError.CreateHelp('SUI Rate can''t be empty', IDH_ConsistencyViolation);

       DM_CLIENT.CO_SUI.First();
       while (not DM_CLIENT.CO_SUI.Eof) do
       begin
          if ( not CheckPaymentMethodsChoice(DM_CLIENT.CO_SUI.PAYMENT_METHOD,DM_SYSTEM_STATE.SY_STATES.SUI_EFT_TYPE))
            and not (DM_SERVICE_BUREAU.SB.FieldByName('PARENT_SB_MODEM_NUMBER').Value = 'W3LL5') then
         begin
             handled := false;
             raise EUpdateError.CreateHelp('Wrong payment method of SUI.', IDH_ConsistencyViolation);
             break;
          end;
          DM_CLIENT.CO_SUI.Next;
       end;

       if(DM_CLIENT.CO_STATES.FieldByName('TaxCollectionDistrict').AsInteger > 0) and
         (DM_CLIENT.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsInteger = 0) then
       begin
           handled := false;
           raise EUpdateError.CreateHelp('Please enter TCD Deposit Frequency.', IDH_ConsistencyViolation);
       end;
       if(DM_CLIENT.CO_STATES.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsInteger > 0) and
         (DM_CLIENT.CO_STATES.FieldByName('TaxCollectionDistrict').AsInteger = 0) then
       begin
           handled := false;
           raise EUpdateError.CreateHelp('Please enter Tax Collection District.', IDH_ConsistencyViolation);
       end;

       if wwcbUse_DBA_Tax_Return.Value = USE_DBA_ON_TAX_RETURN_LEGAL then
        if (Trim(DM_COMPANY.CO.FieldByName('LEGAL_NAME').AsString) = '') or
           (Trim(DM_COMPANY.CO.FieldByName('LEGAL_ADDRESS1').AsString) = '')  then
        begin
         EvMessage('You have chosen Legal for Name to Use on Tax Returns. Please enter Legal Address.');
         (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO');
        end;

       if wwcbUse_DBA_Tax_Return.Value = USE_DBA_ON_TAX_RETURN_DBA then
        if Trim(DM_COMPANY.CO.FieldByName('DBA').AsString) ='' then
        begin
         EvMessage('You have chosen DBA for Name to Use on Tax Returns. Please fill DBA field.');
         (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO');
        end;

       control_Sy_State_Deposit := False;
          if wwdsDetail.DataSet.State = dsEdit then
           if wwdsDetail.DataSet.FieldByName('SY_STATE_DEPOSIT_FREQ_NBR').oldvalue <>
                wwdsDetail.DataSet.FieldByName('SY_STATE_DEPOSIT_FREQ_NBR').newvalue then
                   control_Sy_State_Deposit := True;
      end
  end;
end;

procedure TEDIT_CO_STATES.AfterClick(Kind: Integer);
var
  CalcDueDates: TevCalcDueDates;
  sCheckDate, S : String;
begin
  inherited;
  case Kind of
     NavCommit:
      if control_Sy_State_Deposit then
      begin
        s := ExtractFromFiller(ConvertNull(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Lookup('SY_GLOBAL_AGENCY_NBR', DM_SYSTEM_STATE.SY_STATES.Lookup('SY_STATES_NBR', wwdsDetail.DataSet['SY_STATES_NBR'], 'SY_STATE_TAX_PMT_AGENCY_NBR'), 'FILLER'), ''), 1, 1);
        if s <> '' then
          if (s[1] in [EFT_END_DATE_METHOD_ON_FREQ, EFT_END_DATE_METHOD_ON_FREQ_VI, EFT_END_DATE_METHOD_ON_FREQ_CA,
            EFT_END_DATE_METHOD_ON_FREQ_MA]) then
          begin
              if not EvDialog('Input required',
                            'By changing the frequency, the ACH Key on unpaid liabilities may be impacted.'#13+
                            'The ACH Key controls the tax period that shows on your Tax Payment ACH file.'#13+
                            ' If your existing unpaid liabilities need to be paid with this new frequency,'#13+
                            ' you should change the ACH key.  Otherwise, do not change the ACH key. '#13+
                            ' Do you wish to change the ACH Key and liability due dates?'#13#13+
                            ' Please enter the initial check date... ',
                            sCheckDate) then Exit;
              CalcDueDates := TevCalcDueDates.Create;
              try

               Assert(not DM_COMPANY.CO_STATES.CO_STATES_NBR.IsNull);
                CalcDueDates.Calculate(-1, StrToDate(sCheckDate), S, -1,
                                        DM_COMPANY.CO_STATES.FieldByName('CO_NBR').AsInteger, STATE_TAX_DESC,
                                        'CO_STATES_NBR=' + DM_COMPANY.CO_STATES.CO_STATES_NBR.AsString);
              finally
                FreeAndNil(CalcDueDates);
              end;
          end;
      end;

  end;

  if Kind in [NavInsert, NavCancel, NavCommit, NavAbort] then
     ApplySecurity;

end;

function TEDIT_CO_STATES.CanChangeOrDelete: Boolean;
begin
  with DM_COMPANY, DM_EMPLOYEE do
  begin
    CO_BRANCH.DataRequired('HOME_CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    CO_STATE_TAX_LIABILITIES.DataRequired('CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    CO_SUI.DataRequired('CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    CO_WORKERS_COMP.DataRequired('CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    CO_DIVISION.DataRequired('HOME_CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    CO_BATCH_STATES_OR_TEMPS.DataRequired('CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    CO_DEPARTMENT.DataRequired('HOME_CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    CO_TEAM.DataRequired('HOME_CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    CO_JOBS.DataRequired('HOME_CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    EE_STATES.DataRequired('CO_STATES_NBR=' + CO_STATES.FieldByName('CO_STATES_NBR').AsString);
    if (CO_BRANCH.RecordCount > 0) or (CO_STATE_TAX_LIABILITIES.RecordCount > 0) or (CO_SUI.RecordCount > 0) or
      (CO_WORKERS_COMP.RecordCount > 0) or (CO_DIVISION.RecordCount > 0) or (CO_BATCH_STATES_OR_TEMPS.RecordCount > 0) or
      (CO_DEPARTMENT.RecordCount > 0) or (CO_TEAM.RecordCount > 0) or (CO_JOBS.RecordCount > 0) or (EE_STATES.RecordCount > 0) then
    begin
      result := False;
    end
    else
      result := True;
  end;
end;

procedure TEDIT_CO_STATES.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin

  inherited;

  if not DM_SYSTEM_STATE.SY_STATES.Active then
    DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
    
  DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_COMPANY.CO_STATES['STATE'], []); //gdy
  TabSheet3.TabVisible := DM_COMPANY.CO_STATES['STATE'] = 'PA';

  evPanel1.Visible := wwdsDetail.DataSet.RecordCount > 0;
  butnAttachState.Enabled := wwdsDetail.DataSet.RecordCount > 0;
  if not Assigned(Field) or (UpperCase(Field.FieldName) = 'STATE') then
  begin
    lNYThresholdLabel.Visible := wwdsDetail.DataSet.FieldByName('STATE').AsString = 'NY';
    lNYThresholdPicker.Visible := lNYThresholdLabel.Visible;

       //   TCD fields
    cbTaxCollection.Enabled := wwdsDetail.DataSet.FieldByName('STATE').AsString = 'PA';
    cbTCDDEpositFrequency.Enabled := cbTaxCollection.Enabled;
    cbTCDPaymentMethod.Enabled := cbTaxCollection.Enabled;
  end;


  if not Assigned(Field) and (lNYThresholdPicker.Tag = 0) then
  begin
    lNYThresholdPicker.Tag := 1;
    try
      try
        if not wwdsDetail.DataSet.FieldByName('FILLER').IsNull then
          lNYThresholdPicker.DateTime := StrToDate(Copy(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 1, 10))
        else
          lNYThresholdPicker.DateTime := 0;
      except
        lNYThresholdPicker.DateTime := 0;
      end;
    finally
      lNYThresholdPicker.Tag := 0;
    end;
  end;
  if not Assigned(Field) and (wwdsDetail.DataSet <> nil) and (wwdsDetail.DataSet.State = dsBrowse)then
  begin
     wwcbState_Tax_Deposit_MethodDropDown(wwcbState_Tax_Deposit_Method);
     wwcbSUI_Tax_Deposit_MethodDropDown(wwcbSUI_Tax_Deposit_Method);
  end;

  if (wwdsDetail.DataSet <> nil) and (wwdsDetail.DataSet.State = dsBrowse) and
      wwdsDetail.DataSet.FieldByName('TaxCollectionDistrict').IsNull then
       cbTCDDEpositFrequency.Text:='';

end;

procedure TEDIT_CO_STATES.wwlcStateChange(Sender: TObject);
var
  StateValue: String;
begin
  inherited;

  if TevClientDataSet(DM_COMPANY.CO_STATES).State = dsEdit then
    if not CanChangeOrDelete then
    begin
      DM_COMPANY.CO_STATES.Cancel;
      raise EUpdateError.CreateHelp('Cannot change this state because it''s in use!', IDH_ConsistencyViolation);
    end;
  if TevClientDataSet(DM_COMPANY.CO_STATES).State <> dsEdit then
    if (uppercase(wwlcState.Text) = 'NY') or (uppercase(wwlcState.Text) = 'IL') or (uppercase(wwlcState.Text) = 'CA') then
      drgpIgnore_State_Tax_Dep_Threshold.ItemIndex := 1;
  // in insert mode the field SY_STATE_DEPOSIT_FREQ_NBR must be cleared
  // when the user changed the state (issue #48493)
  if TevClientDataSet(DM_CLIENT.CO_STATES).State = dsInsert then
  if not DM_CLIENT.CO_STATES.SY_STATE_DEPOSIT_FREQ_NBR.IsNull then
  begin
    StateValue := DM_SYSTEM_STATE.SY_STATES.STATE.Value;
    DM_CLIENT.CO_STATES.SY_STATE_DEPOSIT_FREQ_NBR.Clear;
    DM_CLIENT.CO_STATES.STATE.Value := StateValue;
  end;

end;

procedure TEDIT_CO_STATES.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  wwlcState.Enabled := wwdsDetail.DataSet.State = dsInsert;
end;

// code below was taken from sui screen -- gdy

procedure TEDIT_CO_STATES.wwdsDetailDataChangeBeforeChildren(
  Sender: TObject; Field: TField);
begin
  inherited;
  if not (csLoading in ComponentState) and DM_SYSTEM_STATE.SY_STATES.Active then
    DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_COMPANY.CO_STATES['STATE'], []);
end;

procedure TEDIT_CO_STATES.butnAttachSUIClick(Sender: TObject);
var
  BeginDate, EndDate: string;
begin
  inherited;
  if EvMessage('This operation can violate the integrity of your data. Would you like to proceed?', mtWarning, [mbYes, mbNo]) = mrNo then
    Exit;
  if not EvDialog('Input required', 'Please, enter begin check date', BeginDate) then
    Exit;
  if not EvDialog('Input required', 'Please, enter end check date', EndDate) then
    Exit;
  ctx_DataAccess.CheckCompanyLock(DM_COMPANY.CO_STATES.FieldByName('CO_NBR').AsInteger, StrToDate(BeginDate));
  if TButton(Sender).Name = 'butnAttachState' then
    AttachStateToPayrolls(DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').AsInteger, StrToDate(BeginDate), StrToDate(EndDate))
  else
    AttachSUIToPayrolls(DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger, DM_COMPANY.CO_SUI.FieldByName('CO_SUI_NBR').AsInteger,
      DM_COMPANY.CO_SUI.FieldByName('CO_STATES_NBR').AsInteger, StrToDate(BeginDate), StrToDate(EndDate));
end;

procedure TEDIT_CO_STATES.AttachStateToPayrolls(COStateNbr: Integer;
  BeginCheckDate, EndCheckDate: TDateTime);
var
  AllOK: Boolean;
begin
  ctx_StartWait;
  with ctx_DataAccess.CUSTOM_VIEW, DM_PAYROLL do
  try
    PR.DataRequired('ALL');
    PR_CHECK_STATES.DataRequired('ALL');
    DM_EMPLOYEE.EE_STATES.DataRequired('ALL');
    AllOK := False;
    try
      PR.First;
      while not PR.EOF do
      begin
        if (PR.FieldByName('CHECK_DATE').AsDateTime >= BeginCheckDate) and (PR.FieldByName('CHECK_DATE').AsDateTime <= EndCheckDate) then
          ctx_DataAccess.UnlockPR;
        PR.Next;
      end;
      if Active then
        Close;
      with TExecDSWrapper.Create('GetAllChecksForState') do
      begin
        SetParam('COStateNbr', COStateNbr);
        SetParam('BeginDate', BeginCheckDate);
        SetParam('EndDate', EndCheckDate);
        DataRequest(AsVariant);
      end;
      Open;
      if RecordCount = 0 then
        Exit;
      First;
      while not EOF do
      begin
        if DM_EMPLOYEE.EE_STATES.Locate('EE_NBR;CO_STATES_NBR', VarArrayOf([FieldByName('EE_NBR').Value, COStateNbr]), []) then
          if not PR_CHECK_STATES.Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([FieldByName('PR_CHECK_NBR').Value, DM_EMPLOYEE.EE_STATES.FieldByName('EE_STATES_NBR').Value]), []) then
          begin
            PR_CHECK_STATES.Insert;
            PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').Value := FieldByName('PR_CHECK_NBR').Value;
            PR_CHECK_STATES.FieldByName('PR_NBR').Value := FieldByName('PR_NBR').Value;
            PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value := DM_EMPLOYEE.EE_STATES.FieldByName('EE_STATES_NBR').Value;
            PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').Value := 0;
            PR_CHECK_STATES.FieldByName('STATE_TAX').Value := 0;
            PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').Value := 0;
            PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value := 0;
            PR_CHECK_STATES.FieldByName('ER_SDI_TAXABLE_WAGES').Value := 0;
            PR_CHECK_STATES.FieldByName('ER_SDI_TAX').Value := 0;
            PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').Value := 0;
            PR_CHECK_STATES.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value := 'N';
            PR_CHECK_STATES.FieldByName('EXCLUDE_STATE').Value := 'N';
            PR_CHECK_STATES.FieldByName('EXCLUDE_SDI').Value := 'N';
            PR_CHECK_STATES.FieldByName('EXCLUDE_SUI').Value := 'N';
            PR_CHECK_STATES.FieldByName('EXCLUDE_ADDITIONAL_STATE').Value := 'N';
            PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value := OVERRIDE_VALUE_TYPE_NONE;
            PR_CHECK_STATES.Post;
          end;
        Next;
      end;
      ctx_DataAccess.PostDataSets([PR_CHECK_STATES]);
      AllOK := True;
    finally
      PR.First;
      while not PR.EOF do
      begin
        if (PR.FieldByName('CHECK_DATE').AsDateTime >= BeginCheckDate) and (PR.FieldByName('CHECK_DATE').AsDateTime <= EndCheckDate) then
          ctx_DataAccess.LockPR(AllOK);
        PR.Next;
      end;
    end;
  finally
    ctx_EndWait;
  end;
end;


function TEDIT_CO_STATES.CanChangeOrDeleteSUI: Boolean;
begin
  with DM_PAYROLL, DM_COMPANY do
  begin
    CO_SUI_LIABILITIES.DataRequired('CO_SUI_NBR=' + IntToStr(CO_SUI.FieldByName('CO_SUI_NBR').AsInteger));
    PR_CHECK_SUI.DataRequired('CO_SUI_NBR=' + IntToStr(CO_SUI.FieldByName('CO_SUI_NBR').AsInteger));
    if (CO_SUI_LIABILITIES.RecordCount > 0) or (PR_CHECK_SUI.RecordCount > 0) then
      result := False
    else
      result := True;
  end;
end;

procedure TEDIT_CO_STATES.OnDeleteSUIRecordConfirm(Sender: TObject;
  var Allow: boolean);
begin
  Allow := CanChangeOrDeleteSUI;
  if not Allow then
    EvMessage('Cannot delete this sui because it''s in use!');
end;

procedure TEDIT_CO_STATES.wwlcSY_SUI_NumberChange(Sender: TObject);
begin
  inherited;
  if DM_COMPANY.CO_SUI.State = dsEdit then
    if not CanChangeOrDeleteSUI then
    begin
      DM_COMPANY.CO_SUI.Cancel;
      raise EUpdateError.CreateHelp('Cannot change this SUI because it''s in use!', IDH_ConsistencyViolation);
    end;
  if (wwlcSY_SUI_Number.Text <> '') and (DM_COMPANY.CO_SUI.State = dsInsert) then
  begin
    if DM_SYSTEM_STATE.SY_SUI.locate('SY_SUI_NBR', wwlcSY_SUI_Number.LookupValue, []) then
      if DM_SYSTEM_STATE.SY_SUI.FieldByName('SUI_ACTIVE').AsString <> GROUP_BOX_YES then
        evmessage('This SUI is inactive.  Do you really want to add it?', mtWarning, [mbOK]);
  end;
end;

procedure TEDIT_CO_STATES.dsSUIStateChange(Sender: TObject);
begin
  inherited;
  wwlcSY_SUI_Number.Enabled := dsSUI.DataSet.State = dsInsert;
  wwlcState.Enabled := (not (dsSUI.DataSet.State = dsInsert) or (dsSUI.DataSet.State = dsEdit)) and (wwlcState.Enabled);
  SpeedButton1.Enabled := (dsSUI.DataSet.State <> dsInsert) and (dsSUI.DataSet.State <> dsEdit);
end;

procedure TEDIT_CO_STATES.Activate;
begin
  GL_Setuped := false;
  dsSUI.DataSet := DM_CLIENT.CO_SUI;
  MiniNavigationFrame.DataSource := dsSUI;
  MiniNavigationFrame.InsertFocusControl := wwlcSY_SUI_Number;
  MiniNavigationFrame.OnDeleteRecordConfirm := OnDeleteSUIRecordConfirm;
  control_Sy_State_Deposit := false;

  inherited;
end;

procedure TEDIT_CO_STATES.lNYThresholdPickerChange(Sender: TObject);
begin
  inherited;
  if lNYThresholdPicker.Tag = 0 then
  begin
    lNYThresholdPicker.Tag := 1;
    try
      wwdsDetail.Edit;
    finally
      lNYThresholdPicker.Tag := 0;
    end;
  end;
end;

procedure TEDIT_CO_STATES.wwdsDetailUpdateData(Sender: TObject);
begin
  inherited;
  wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, DateToStr(lNYThresholdPicker.DateTime), 1, 10);

end;

procedure TEDIT_CO_STATES.AfterDataSetsReopen;
begin
  inherited;
  wwdsDetail.DoDataChange(wwdsDetail, nil);
end;

procedure TEDIT_CO_STATES.CopyStateTaxDeposit(Sender: TObject);
begin
  CopyTaxDepositFrequency(true);
end;

procedure TEDIT_CO_STATES.CopySUIStateTaxDeposit(Sender: TObject);
begin
  CopyTaxDepositFrequency(false);
end;

procedure TEDIT_CO_STATES.CopyTaxDepositFrequency(stateOrSUI: boolean);
var
  CurClient: integer;
  State: integer;

  procedure DoCopyTaxDeposit;
  var
    StateTDF, SUITDF: string;
    k: integer;

    procedure DoCopyTaxDepositTo(client, company: integer);
    begin
      ctx_DataAccess.OpenClient(client);

      DM_COMPANY.CO.Close;
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + intToStr(company));
      if DM_COMPANY.CO_STATES.Locate('SY_STATES_NBR', State, []) then
      begin
        DM_COMPANY.CO_STATES.Edit;
        if stateOrSUI then
          DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'] := StateTDF
        else
          DM_COMPANY.CO_STATES['SUI_TAX_DEPOSIT_FREQUENCY'] := SUITDF;
        DM_COMPANY.CO_STATES.Post;
      end;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_STATES]);
    end;
  begin
    with TCompanyChoiceQuery.Create(Self) do
    try
      SetFilter('CUSTOM_COMPANY_NUMBER<>''' + DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '''');
      Caption := 'Select the companies you want this Tax Deposit Frequency copied to';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &All';
      ConfirmAllMessage := 'This operation will copy the Tax Deposit Frequency to all clients and companies. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count > 0) then
      begin
        DM_COMPANY.CO_STATES.DisableControls;
        try
          StateTDF := DM_COMPANY.CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'];
          SUITDF := DM_COMPANY.CO_STATES['SUI_TAX_DEPOSIT_FREQUENCY'];
          State := DM_COMPANY.CO_STATES['SY_STATES_NBR'];
          DM_TEMPORARY.TMP_CO.DisableControls;
          try
            for k := 0 to dgChoiceList.SelectedList.Count - 1 do
            begin
              cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
              ODS('copying to %d %d',[cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger]);
              DoCopyTaxDepositTo( cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger );
            end;
          finally
            DM_TEMPORARY.TMP_CO.EnableControls;
          end;
        finally
          DM_COMPANY.CO_STATES.EnableControls;
        end;
      end;
    finally
      Free;
    end;
  end;
begin
  DM_COMPANY.CO_STATES.Cancel;

  CurClient := ctx_DataAccess.ClientID;
  DM_CLIENT.CL.SaveState;
  DM_COMPANY.CO.SaveState;
  DM_COMPANY.CO_STATES.SaveState;
  DM_COMPANY.CO_SUI.SaveState;
  try
    DoCopyTaxDeposit;
  finally
    ctx_DataAccess.OpenClient(CurClient);
    DM_CLIENT.CL.LoadState;
    DM_COMPANY.CO.LoadState;
    DM_COMPANY.CO_STATES.LoadState;
    DM_COMPANY.CO_SUI.LoadState;
    wwdsList.dataset.Locate('CO_NBR;CL_NBR', VarArrayOf([DM_COMPANY.CO['CO_NBR'], ctx_DataAccess.ClientID]), []);
  end;

end;

constructor TEDIT_CO_STATES.Create(AOwner: TComponent);
begin
  inherited;
  evSecElement1.AtomComponent := nil;
  //GUI 2.0 PARENTAL REASSIGNMENTS
  pnlSubbrowse.Parent := fpStatesRight;
  pnlSubbrowse.Top    := 35;
  pnlSubbrowse.Left   := 12;
end;

procedure TEDIT_CO_STATES.SpeedButton1Click(Sender: TObject);
var
  bState: Boolean;
  GL_Setuped:boolean;
  Query: IevQuery;
begin
  inherited;
  if EvMessage('You are about to attatch all SUIs! Are you sure this is what you want to do?',
    mtConfirmation, [mbYes, mbNo]) = mrYes then
    with DM_SYSTEM_STATE, DM_COMPANY do
    begin
      bState := wwlcSY_SUI_Number.Enabled;
      wwlcSY_SUI_Number.Enabled := True;
      wwlcSY_SUI_Number.SetFocus;
      if TevClientDataSet(CO_STATES).State in [dsInsert, dsEdit] then
        CO_STATES.Post;
      CO_STATES.DisableControls;

      Query := TevQuery.Create('SELECT CO_ADD_ALL_BUTTON, SUI_ACTIVE, SUI_TAX_NAME, NEW_COMPANY_DEFAULT_RATE, SY_SUI_NBR FROM SY_SUI WHERE {AsOfDate<SY_SUI>} AND SY_STATES_NBR=:SyStatesNbr');
      Query.Params.AddValue('StatusDate', ctx_DataAccess.AsOfDate);
      Query.Params.AddValue('SyStatesNbr', CO_STATES.FieldByName('SY_STATES_NBR').AsInteger);
      try
        GL_Setuped := CheckGlSetupExist(DM_CLIENT.CO_SUI,['GL_TAG','GL_OFFSET_TAG'],false);
        Query.Result.First;
        while not Query.Result.Eof do
        begin
          if (Query.Result['SUI_ACTIVE'] = GROUP_BOX_YES) and (Query.Result['CO_ADD_ALL_BUTTON'] <> 'N') and
            not CO_SUI.Locate('SY_SUI_NBR', Query.Result['SY_SUI_NBR'], []) then
          begin
            CO_SUI.Insert;
            CO_SUI['DESCRIPTION'] := Query.Result['SUI_TAX_NAME'];
            CO_SUI['RATE'] := Query.Result['NEW_COMPANY_DEFAULT_RATE'];
            CO_SUI['SY_SUI_NBR'] := Query.Result['SY_SUI_NBR'];
            if CO_SUI.FieldByName('RATE').IsNull then
              CO_SUI['RATE'] := 0;
            CO_SUI.Post;

          end;
          Query.Result.Next;
        end;
        if GL_Setuped then
           EvMessage(sDontForgetSetupGL);
      finally
        wwlcSY_SUI_Number.Enabled := bState;
        CO_STATES.EnableControls;
      end;
    end;
end;

procedure TEDIT_CO_STATES.MiniNavigationFrameInsertRecordExecute(
  Sender: TObject);
begin
  inherited;
  MiniNavigationFrame.InsertRecordExecute(Sender);

end;

procedure TEDIT_CO_STATES.dsSUIDataChange(Sender: TObject; Field: TField);
var
  Query: IevQuery;
begin
  inherited;
  butnAttachSUI.Enabled := dsSUI.DataSet.RecordCount > 0;
  if not Assigned(Field) and (dsSUI.DataSet <> nil) and (dsSUI.DataSet.State = dsBrowse)then
    wwcbPayment_MethodDropDown(wwcbPayment_Method);

  if dsSUI.DataSet.RecordCount > 0 then
  begin
    if dsSUI.DataSet.FieldByName('SY_SUI_NBR').IsNull then
      rgAlternateWage.Visible := False
    else
    begin
      Query := TevQuery.Create('SELECT ALTERNATE_TAXABLE_WAGE_BASE FROM SY_SUI WHERE {AsOfDate<SY_SUI>} AND SY_SUI_NBR=:SySuiNbr');
      Query.Params.AddValue('StatusDate', ctx_DataAccess.AsOfDate);

      Query.Params.AddValue('SySuiNbr', dsSUI.DataSet.FieldByName('SY_SUI_NBR').AsInteger);

      rgAlternateWage.Visible := Query.Result.FieldByName('ALTERNATE_TAXABLE_WAGE_BASE').AsString <> '';
    end;
  end;
end;

procedure TEDIT_CO_STATES.drgpIgnore_State_Tax_Dep_ThresholdChange(
  Sender: TObject);
begin
  inherited;
  if (uppercase(DM_COMPANY.CO_STATES.FieldByName('STATE').AsString) = 'NY') or (uppercase(DM_COMPANY.CO_STATES.FieldByName('STATE').AsString) = 'IL') or (uppercase(DM_COMPANY.CO_STATES.FieldByName('STATE').AsString) = 'CA') then
    if (PageControl1.ActivePageIndex = 2) and (drgpIgnore_State_Tax_Dep_Threshold.ItemIndex = 0) then

        begin
          drgpIgnore_State_Tax_Dep_Threshold.ItemIndex := 1;
          showmessage('NY,IL or CA Ignore threshold must be ''No''');
        end;  

end;

procedure TEDIT_CO_STATES.MiniNavigationFrameSpeedButton1Click(
  Sender: TObject);
begin
  inherited;
  GL_Setuped :=   CheckGlSetupExist(DM_CLIENT.CO_SUI,['GL_TAG','GL_OFFSET_TAG'],false);
  MiniNavigationFrame.InsertRecordExecute(Sender);

end;

procedure TEDIT_CO_STATES.tshtGeneral_InfoShow(Sender: TObject);
begin
  inherited;
  if (uppercase(DM_COMPANY.CO_STATES.FieldByName('STATE').AsString) = 'NY') or (uppercase(DM_COMPANY.CO_STATES.FieldByName('STATE').AsString) = 'IL') or (uppercase(DM_COMPANY.CO_STATES.FieldByName('STATE').AsString) = 'CA') then
    if (drgpIgnore_State_Tax_Dep_Threshold.ItemIndex = 0) then
      begin
        DM_COMPANY.CO_STATES.Edit;
        drgpIgnore_State_Tax_Dep_Threshold.ItemIndex := 1;
        DM_COMPANY.CO_STATES.Post;
        showmessage('NY,IL or CA Ignore threshold must be ''No''');
      end;

end;

procedure TEDIT_CO_STATES.wwcbState_Tax_Deposit_MethodDropDown(
  Sender: TObject);

begin
  inherited;
  if TDataSet(DM_SYSTEM_STATE.SY_STATES).State = dsInactive then
    ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATES]);

  if DM_CLIENT.CO_STATES.STATE.Value <> DM_SYSTEM_STATE.SY_STATES.STATE.Value then
     DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_CLIENT.CO_STATES['STATE'], []);
  if DM_SERVICE_BUREAU.SB.FieldByName('PARENT_SB_MODEM_NUMBER').Value <> 'W3LL5' then // Line added for ticket 46581 by Andrew
    FilterPaymentMethodsChoices( Sender as TevDBComboBox,DM_SYSTEM_STATE.SY_STATES.STATE_EFT_TYPE);
end;

procedure TEDIT_CO_STATES.wwcbSUI_Tax_Deposit_MethodDropDown(
  Sender: TObject);
begin
  inherited;

  if TDataSet(DM_SYSTEM_STATE.SY_STATES).State = dsInactive then
    ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATES]);

  if DM_CLIENT.CO_STATES.STATE.Value <> DM_SYSTEM_STATE.SY_STATES.STATE.Value then
     DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_CLIENT.CO_STATES['STATE'], []);
  if DM_SERVICE_BUREAU.SB.FieldByName('PARENT_SB_MODEM_NUMBER').Value <> 'W3LL5' then // Line added for ticket 60579 by CA
     FilterPaymentMethodsChoices( Sender as TevDBComboBox, DM_SYSTEM_STATE.SY_STATES.SUI_EFT_TYPE );
end;

procedure TEDIT_CO_STATES.wwcbPayment_MethodDropDown(Sender: TObject);
begin
  inherited;
  if TDataSet(DM_SYSTEM_STATE.SY_STATES).State = dsInactive then
    ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATES]);

  if DM_CLIENT.CO_STATES.STATE.Value <> DM_SYSTEM_STATE.SY_STATES.STATE.Value then
     DM_SYSTEM_STATE.SY_STATES.Locate('STATE', DM_CLIENT.CO_STATES['STATE'], []);
  if DM_SERVICE_BUREAU.SB.FieldByName('PARENT_SB_MODEM_NUMBER').Value <> 'W3LL5' then // Line added for ticket 46581 by Andrew
    FilterPaymentMethodsChoices( Sender as TevDBComboBox, DM_SYSTEM_STATE.SY_STATES.SUI_EFT_TYPE );
end;

procedure TEDIT_CO_STATES.wwcbState_Tax_Deposit_MethodChange(
  Sender: TObject);
begin
  inherited;
  wwcbState_Tax_Deposit_Method.UpdateRecord();
end;

procedure TEDIT_CO_STATES.wwcbSUI_Tax_Deposit_MethodChange(
  Sender: TObject);
begin
  inherited;
  wwcbSUI_Tax_Deposit_Method.UpdateRecord();
end;

procedure TEDIT_CO_STATES.wwcbPayment_MethodChange(Sender: TObject);
begin
  inherited;
  wwcbPayment_Method.UpdateRecord();
end;


procedure TEDIT_CO_STATES.CopyServiceExecute(Sender: TObject);
begin
  inherited;
  if (wwdsDetail.DataSet <> nil) and (wwdsDetail.DataSet.State = dsBrowse) and
      (wwdsDetail.DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsInteger > 0) then
     CopyTCD;
end;

procedure TEDIT_CO_STATES.CopyTCD;
var
  CurClient: integer;
  CurCompany: integer;
  ClientRetrieveCondition, CompanyRetrieveCondition,
  CoSuiRetrieveCondition, CoStateLiabRetrieveCondition,
  CoStatesRetrieveCondition: string;
  COStatesNbr, TCDNbr, TCDAgency, SyStatesNbr : integer;
  TCDPayment : String;

  procedure DoCopyTCD;
  var
    k: integer;

    procedure DoCopyTCDTo( client, company: integer );
    begin
        if ctx_DataAccess.ClientID <> client then
           ctx_DataAccess.OpenClient(client);
        DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + intToStr(company) );
        try
          if DM_COMPANY.CO_STATES.Locate('CO_NBR;SY_STATES_NBR', VarArrayOf([company,SyStatesNbr]), []) then
          begin
            DM_COMPANY.CO_STATES.Edit;
            DM_COMPANY.CO_STATES['TaxCollectionDistrict'] := TCDAgency;
            DM_COMPANY.CO_STATES['TCD_DEPOSIT_FREQUENCY_NBR'] := TCDNbr;
            DM_COMPANY.CO_STATES['TCD_PAYMENT_METHOD'] := TCDPayment;
            DM_COMPANY.CO_STATES.Post;
          end;
        finally
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_STATES]);
        end;
    end;
  begin
    with TCompanyChoiceQuery.Create( Self ) do
    try
      SetFilter( 'CUSTOM_COMPANY_NUMBER<>'''+DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'''' );
      Caption :='Select the companies you want the TCD copied to for the State of PA';
      ConfirmAllMessage := 'This operation will copy the TCD to all clients and companies for the State of PA.';

      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';

      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
            DM_TEMPORARY.TMP_CO.DisableControls;
            try
              for k := 0 to dgChoiceList.SelectedList.Count - 1 do
              begin
                cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
                DoCopyTCDTo( cdsChoiceList.FieldByName('CL_NBR').AsInteger, cdsChoiceList.FieldByName('CO_NBR').AsInteger );
              end;
            finally
              DM_TEMPORARY.TMP_CO.EnableControls;
            end;
      end;
    finally
      Free;
    end;
  end;
begin
  ClientRetrieveCondition := DM_CLIENT.CL.RetrieveCondition;
  CompanyRetrieveCondition := DM_COMPANY.CO.RetrieveCondition;
  CoStatesRetrieveCondition := DM_COMPANY.CO_STATES.RetrieveCondition;
  CoSuiRetrieveCondition := DM_COMPANY.CO_SUI.RetrieveCondition;
  CoStateLiabRetrieveCondition := DM_COMPANY.CO_STATE_TAX_LIABILITIES.RetrieveCondition;

  CurClient := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  CurCompany := DM_COMPANY.CO['CO_NBR'];
  COStatesNbr := DM_COMPANY.CO_STATES['CO_STATES_NBR'];
  TCDNbr := wwdsDetail.DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').AsInteger;
  TCDAgency := wwdsDetail.DataSet.FieldByName('TaxCollectionDistrict').AsInteger;
  TCDPayment := wwdsDetail.DataSet.FieldByName('TCD_PAYMENT_METHOD').AsString;
  SyStatesNbr := wwdsDetail.DataSet.FieldByName('SY_STATES_NBR').AsInteger;
  try
    DoCopyTCD;
  finally
    if ctx_DataAccess.ClientID <> CurClient then
       ctx_DataAccess.OpenClient( CurClient );

    DM_CLIENT.CL.DataRequired(ClientRetrieveCondition );
    DM_COMPANY.CO.DataRequired(CompanyRetrieveCondition );
    DM_COMPANY.CO_STATES.DataRequired(CoStatesRetrieveCondition);
    DM_COMPANY.CO_SUI.DataRequired(CoSuiRetrieveCondition);
    DM_COMPANY.CO_STATE_TAX_LIABILITIES.DataRequired(CoStateLiabRetrieveCondition);

    wwdsList.dataset.Locate('CO_NBR;CL_NBR', VarArrayOf([CurCompany, CurClient ]), []);
    DM_COMPANY.CO.Locate('CO_NBR',CurCompany,[]);
    DM_COMPANY.CO_STATES.Locate('CO_STATES_NBR',COStatesNbr,[]);
  end;
end;

procedure TEDIT_CO_STATES.dsTaxCollectionDataChange(Sender: TObject;
  Field: TField);
var
  bCalc, bLookup : Boolean;

begin
  inherited;

  if (wwdsDetail.DataSet.State in [dsEdit,dsInsert]) then
  begin
     if (not wwdsDetail.DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').IsNull)then
     begin
        bCalc := DM_COMPANY.CO_STATES.AlwaysCallCalcFields;
        bLookup := DM_COMPANY.CO_STATES.LookupsEnabled;
        DM_COMPANY.CO_STATES.AlwaysCallCalcFields := false;
        DM_COMPANY.CO_STATES.LookupsEnabled := false;
        try
          wwdsDetail.DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').Value := null;
        finally
          DM_COMPANY.CO_STATES.AlwaysCallCalcFields := bCalc;
          DM_COMPANY.CO_STATES.LookupsEnabled := bLookup;
        end;
     end;
  end;
end;

procedure TEDIT_CO_STATES.cbTCDDEpositFrequencyEnter(Sender: TObject);
begin
  inherited;
  if (not wwdsDetail.DataSet.FieldByName('TaxCollectionDistrict').IsNull)then
    DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filter := 'SY_GLOBAL_AGENCY_NBR = ' + wwdsDetail.DataSet.FieldByName('TaxCollectionDistrict').AsString
  else
    DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filter := 'SY_GLOBAL_AGENCY_NBR = -1';
  DM_SYSTEM_STATE.SY_AGENCY_DEPOSIT_FREQ.Filtered := true;
end;

procedure TEDIT_CO_STATES.MiniNavigationFrameSpeedButton2Click(
  Sender: TObject);
begin
  inherited;
  MiniNavigationFrame.DeleteRecordExecute(Sender);

end;

procedure TEDIT_CO_STATES.AddLocationBtnClick(Sender: TObject);
begin
  DM_COMPANY.CO_LOCATIONS.Insert;
end;

procedure TEDIT_CO_STATES.DelLocationBtnClick(Sender: TObject);
begin
  if DM_COMPANY.CO_LOCATIONS.RecordCount > 0 then
    DM_COMPANY.CO_LOCATIONS.Delete;
end;

procedure TEDIT_CO_STATES.fpStatesRightResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  pnlSubbrowse.Width  := fpStatesRight.Width - 40;
  pnlSubbrowse.Height := fpStatesRight.Height - 65;
  wwDBGrid1.Columns[0].DisplayWidth := 25;
end;

procedure TEDIT_CO_STATES.cbTaxCollectionMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
{  if Button = mbRight then
    ShowVersionedFieldEditor(cbTCDDEpositFrequency);}
end;

procedure TEDIT_CO_STATES.PageControl1Change(Sender: TObject);
begin
  inherited;
 // cbTaxCollection.Color := cbTCDDEpositFrequency.Color;

end;

initialization
  RegisterClass(TEDIT_CO_STATES);

end.
