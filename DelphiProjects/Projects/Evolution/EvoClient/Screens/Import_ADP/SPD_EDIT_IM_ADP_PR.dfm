inherited EDIT_IM_ADP_PR: TEDIT_IM_ADP_PR
  inherited PageControl1: TevPageControl
    ActivePage = TabSheet1
    inherited TabSheet1: TTabSheet
      inherited evPanel1: TevPanel
        Width = 427
        Height = 238
        inherited evPanel5: TevPanel
          Width = 427
          Height = 133
          inherited EvBevel2: TEvBevel
            Height = 129
          end
          inherited pnlSetup: TevPanel
            Width = 296
            Height = 129
            inherited EvBevel1: TEvBevel
              Width = 296
            end
            inherited pnlSetupControls: TevPanel
              Width = 296
              Height = 90
            end
          end
          inherited evBitBtn2: TevBitBtn
            TabOrder = 5
          end
          inline ParamInput: TADPPrImportParamFrame
            Left = 216
            Top = 48
            Width = 297
            Height = 89
            TabOrder = 3
          end
        end
        inherited pnlConnectionInputHolder: TevPanel
          Width = 427
          inherited ConnectDatabasePathFrame: TConnectDatabasePathFrame
            Width = 423
          end
        end
      end
    end
    inherited TabSheet5: TTabSheet
      inherited viewerFrame: TTableSpaceViewFrame
        inherited evPanel2: TevPanel
          inherited dgView: TevDBGrid
            Width = 561
            Height = 357
          end
        end
      end
    end
    object TabSheet3: TTabSheet [2]
      Caption = 'Locals Mapping'
      ImageIndex = 4
      inline BinderFrame1: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 134
          end
          inherited pnlTopLeft: TevPanel
            Height = 134
            inherited dgLeft: TevDBGrid
              Height = 109
            end
          end
          inherited pnlTopRight: TevPanel
            Height = 134
            inherited dgRight: TevDBGrid
              Height = 109
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
        end
      end
    end
    object TabSheet4: TTabSheet [3]
      Caption = 'Hours Mapping'
      ImageIndex = 5
      inline BinderFrame2: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 142
          end
          inherited pnlTopLeft: TevPanel
            Height = 142
            inherited dgLeft: TevDBGrid
              Height = 117
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 323
            Height = 142
            inherited evPanel4: TevPanel
              Width = 323
            end
            inherited dgRight: TevDBGrid
              Width = 323
              Height = 117
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 593
            inherited evPanel5: TevPanel
              Width = 593
            end
            inherited dgBottom: TevDBGrid
              Width = 593
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 593
          end
        end
      end
    end
    object TabSheet7: TTabSheet [4]
      Caption = 'Amount Mapping'
      ImageIndex = 7
      inline BinderFrame4: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 142
          end
          inherited pnlTopLeft: TevPanel
            Height = 142
            inherited dgLeft: TevDBGrid
              Height = 117
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 323
            Height = 142
            inherited evPanel4: TevPanel
              Width = 323
            end
            inherited dgRight: TevDBGrid
              Width = 323
              Height = 117
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 593
            inherited evPanel5: TevPanel
              Width = 593
            end
            inherited dgBottom: TevDBGrid
              Width = 593
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 593
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited LogFrame: TImportLoggerViewFrame
        inherited pnlLog: TevPanel
          inherited pgLogView: TevPageControl
            Width = 435
            Height = 357
          end
          inherited LoggerRichView: TImportLoggerRichViewFrame
            inherited pnlUserView: TevPanel
              inherited pnlBottom: TevPanel
                Height = 53
                inherited evSplitter2: TevSplitter
                  Height = 53
                end
                inherited pnlLeft: TevPanel
                  Height = 53
                  inherited mmDetails: TevMemo
                    Height = 28
                  end
                end
                inherited pnlRight: TevPanel
                  Height = 53
                  inherited mmContext: TevMemo
                    Height = 28
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
