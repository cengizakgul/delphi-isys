object SelectCoDlg: TSelectCoDlg
  Left = 366
  Top = 405
  BorderStyle = bsDialog
  Caption = 'Select Company'
  ClientHeight = 179
  ClientWidth = 219
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  DesignSize = (
    219
    179)
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 136
    Top = 8
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 136
    Top = 38
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object evDBGrid1: TevDBGrid
    Left = 8
    Top = 8
    Width = 121
    Height = 161
    DisableThemesInTitle = False
    Selected.Strings = (
      'CompanyCode'#9'3'#9'Code')
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = evDataSource1
    TabOrder = 2
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnDblClick = evDBGrid1DblClick
  end
  object evDataSource1: TevDataSource
    Left = 168
    Top = 88
  end
end
