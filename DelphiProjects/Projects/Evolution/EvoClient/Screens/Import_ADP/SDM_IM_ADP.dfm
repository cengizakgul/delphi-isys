object DM_IM_ADP: TDM_IM_ADP
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 358
  Top = 204
  Height = 545
  Width = 556
  object SDDatabase: TSDDatabase
    LoginPrompt = False
    DatabaseName = 'dbADP'
    IdleTimeOut = 0
    Params.Strings = (
      'USER NAME=sys'
      'password=admin')
    RemoteDatabase = 'PAY4WIN'
    SessionName = 'Default'
    Left = 48
    Top = 32
  end
  object CO: TSDQuery
    DatabaseName = 'dbadp'
    SQL.Strings = (
      'select distinct CompanyCode from reports.v_employee')
    Left = 80
    Top = 104
  end
  object EDS: TSDQuery
    DatabaseName = 'dbadp'
    SQL.Strings = (
      
        'select '#39'E'#39' as eord, hrs_ern_c as code,  type_c as type, descript' +
        'ion_tx as name '
      '   from pcpaysys.t_co_hrs_earn'
      '   where co_c=:Co'
      'union'
      'select '#39'D'#39', ded_c,  category_c, description_tx'
      '   from pcpaysys.t_co_ded'
      '   where co_c=:Co'
      'union'
      'select '#39'M'#39', memo_c,  '#39#39', description_tx'
      '   from pcpaysys.t_co_memo'
      '   where co_c=:Co')
    Left = 168
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object T_CO_DEPT: TSDQuery
    DatabaseName = 'dbadp'
    SQL.Strings = (
      'select * from pcpaysys.t_co_dept'
      'where co_c=:Co')
    Left = 144
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object V_EMP_ALL: TSDQuery
    DatabaseName = 'dbadp'
    AfterOpen = V_EMP_ALLAfterOpen
    OnFilterRecord = V_EMP_ALLFilterRecord
    SQL.Strings = (
      'select * from reports.v_emp_all'
      'where CompanyCode=:Co')
    Left = 216
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object V_LOCAL_TAX_CODE: TSDQuery
    DatabaseName = 'dbadp'
    SQL.Strings = (
      'select * from reports.v_local_tax_code'
      'where CompanyCode=:Co')
    Left = 312
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object V_BANKING_INFO: TSDQuery
    DatabaseName = 'dbadp'
    OnFilterRecord = V_EMP_ALLFilterRecord
    SQL.Strings = (
      'select * from reports.V_BANKING_INFO'
      'where CompanyCode=:Co')
    Left = 80
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object V_DEDUCTIONS: TSDQuery
    DatabaseName = 'dbadp'
    OnFilterRecord = V_EMP_ALLFilterRecord
    SQL.Strings = (
      
        'select V_DEDUCTIONS.* from reports.V_DEDUCTIONS, pcpaysys.T_CO_D' +
        'ED'
      'where V_DEDUCTIONS.CompanyCode=:Co and '
      '           V_DEDUCTIONS.CompanyCode = T_CO_DED.CO_C and'
      '           V_DEDUCTIONS.DeductionCode = T_CO_DED.DED_C and'
      '           T_CO_DED.CATEGORY_C not in ('#39'D'#39') ')
    Left = 200
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object cdsBankType: TevClientDataSet
    Left = 488
    Top = 24
    object cdsBankTypeCode: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object cdsBankTypeName: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object EDS_D: TSDQuery
    DatabaseName = 'dbadp'
    SQL.Strings = (
      
        'select '#39'D'#39' as eord, ded_c as code,  category_c as type, descript' +
        'ion_tx as name'
      '   from pcpaysys.t_co_ded'
      '   where co_c=:Co and category_c='#39'D'#39)
    Left = 240
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object V_TD_GOAL_ACCUM: TSDQuery
    DatabaseName = 'dbadp'
    OnFilterRecord = V_EMP_ALLFilterRecord
    SQL.Strings = (
      'select CompanyCode, File#, Goal1 as Goal,'
      '           Goal1DedCode as GoalDedCode,'
      '           Goal1Limit as GoalLimit,'
      '           Goal1ToDate as GoalToDate'
      'from reports.V_TD_GOAL_ACCUM'
      'where V_TD_GOAL_ACCUM.CompanyCode=:Co and Goal1<>0'
      'union'
      'select CompanyCode, File#, Goal2,'
      '           Goal2DedCode,'
      '           Goal2Limit,'
      '           Goal2ToDate'
      'from reports.V_TD_GOAL_ACCUM'
      'where V_TD_GOAL_ACCUM.CompanyCode=:Co and Goal2<>0'
      'union'
      'select CompanyCode, File#, Goal3,'
      '           Goal3DedCode,'
      '           Goal3Limit,'
      '           Goal3ToDate'
      'from reports.V_TD_GOAL_ACCUM'
      'where V_TD_GOAL_ACCUM.CompanyCode=:Co and Goal3<>0'
      'union'
      'select CompanyCode, File#, Goal4,'
      '           Goal4DedCode,'
      '           Goal4Limit,'
      '           Goal4ToDate'
      'from reports.V_TD_GOAL_ACCUM'
      'where V_TD_GOAL_ACCUM.CompanyCode=:Co and Goal4<>0'
      'union'
      'select CompanyCode, File#, Goal5,'
      '           Goal5DedCode,'
      '           Goal5Limit,'
      '           Goal5ToDate'
      'from reports.V_TD_GOAL_ACCUM'
      'where V_TD_GOAL_ACCUM.CompanyCode=:Co and Goal5<>0'
      'union'
      'select CompanyCode, File#, Goal6,'
      '           Goal6DedCode,'
      '           Goal6Limit,'
      '           Goal6ToDate'
      'from reports.V_TD_GOAL_ACCUM'
      'where V_TD_GOAL_ACCUM.CompanyCode=:Co and Goal6<>0'
      'union'
      'select CompanyCode, File#, Goal7,'
      '           Goal7DedCode,'
      '           Goal7Limit,'
      '           Goal7ToDate'
      'from reports.V_TD_GOAL_ACCUM'
      'where V_TD_GOAL_ACCUM.CompanyCode=:Co and Goal7<>0'
      'union'
      'select CompanyCode, File#, Goal8,'
      '           Goal8DedCode,'
      '           Goal8Limit,'
      '           Goal8ToDate'
      'from reports.V_TD_GOAL_ACCUM'
      'where V_TD_GOAL_ACCUM.CompanyCode=:Co and Goal8<>0'
      'union'
      'select CompanyCode, File#, Goal9,'
      '           Goal9DedCode,'
      '           Goal9Limit,'
      '           Goal9ToDate'
      'from reports.V_TD_GOAL_ACCUM'
      'where V_TD_GOAL_ACCUM.CompanyCode=:Co and Goal9<>0')
    Left = 304
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object V_LIENS: TSDQuery
    DatabaseName = 'dbadp'
    OnFilterRecord = V_EMP_ALLFilterRecord
    SQL.Strings = (
      'select * from reports.V_LIENS'
      'where CompanyCode=:Co')
    Left = 400
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object AGENCIES: TSDQuery
    DatabaseName = 'dbadp'
    SQL.Strings = (
      'select distinct payeecode, payeenameline1, payeenameline2 '
      'from reports.V_LIENS'
      'where CompanyCode=:Co')
    Left = 304
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object V_YTD_INFO: TSDQuery
    DatabaseName = 'dbadp'
    OnFilterRecord = V_EMP_ALLFilterRecord
    SQL.Strings = (
      'select V_YTD_INFO.*, V_EMPLOYEE.CURRWORKDSTATESEQ#,'
      '          V_EMPLOYEE.SUISDITAXJURISCD,'
      '          V_EMPLOYEE.CURRWORKDLOCALSEQ#'
      'from reports.V_YTD_INFO, reports.V_EMPLOYEE'
      'where V_YTD_INFO.CompanyCode=:Co and '
      '           V_YTD_INFO.CompanyCode=V_EMPLOYEE.CompanyCode and'
      '           V_YTD_INFO.File#=V_EMPLOYEE.File#')
    Left = 80
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object SUI_SDI: TSDQuery
    DatabaseName = 'dbadp'
    SQL.Strings = (
      'select * from reports.V_SUI_SDI_CODE'
      'where CompanyCode=:Co')
    Left = 376
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object CO_ACC: TSDQuery
    DatabaseName = 'dbadp'
    SQL.Strings = (
      'select * from pcpaysys.T_CO_ACC'
      'where Co_C=:Co')
    Left = 432
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object V_YTD_ACCUMULATOR: TSDQuery
    DatabaseName = 'dbadp'
    OnFilterRecord = V_EMP_ALLFilterRecord
    SQL.Strings = (
      'select * from reports.V_YTD_ACCUMULATOR'
      'where CompanyCode=:Co')
    Left = 232
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Co'
        ParamType = ptInput
      end>
  end
  object TestADP: TSDQuery
    DatabaseName = 'dbadp'
    SQL.Strings = (
      
        'select NAME from sysadm.SYSCOLUMNS where tbname='#39'V_EMPLOYEE'#39' and' +
        ' tbcreator='#39'REPORTS'#39' and name='#39'CURRWORKDSTATESEQ#'#39)
    Left = 88
    Top = 320
  end
end
