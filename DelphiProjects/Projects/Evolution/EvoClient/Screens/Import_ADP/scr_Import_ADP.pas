// Screens of "ADP Import"
unit scr_Import_ADP;

interface

uses
  sImportADPScr,
  SPD_EDIT_IM_ADP_BASE,
  SDM_IM_ADP,
  SPD_EDIT_IM_ADP_CO_AND_EE,
  SPD_EDIT_IM_ADP_PR,
  SPD_SelectCo,
  sADPPrImportParam,
  SADPImportDef,
  sConnectDatabaseByName;

implementation

end.
