// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SADPImportDef;

interface

uses
  genericImport, Classes, genericImportDefinitionImpl, IEMap, gdyBinder, EvConsts,
  Variants, SysUtils,  DateUtils, EvUtils, sImportVisualController,
  EvContext, EvCOmmonInterfaces, EvDataset, EvClientDataSet;

const
  sParamCheckDate = 'PR.CHECK_DATE';
  sParamBatchFrequency = 'PR_BATCH.FREQUENCY';

const
  sEDBindingName = 'EDs';
  sEDHoursBindingName = 'Hours';
  sEDAmountsBindingName = 'Amounts';
  sLocalsBindingName = 'Locals';
  sBankTypeBindingName = 'Bank Account Types';
  sAgencyBindingName = 'Agencies';

const
  SDIMap: string =
  '01=NJ,' +                   '35=SC,' +                       '88=HI,' +
  '02=MA,' +                   '36=OK,' +                       '89=IN,' +
  '03=AL,' +                   '37=AZ,' +                       '90=MO,' +
  '05=MD,' +                   '39=HI,' +                       '92=ND,' +
  '07=DC,' +                   '40=PA,' +                       '93=OR,' +
  '08=DE,' +                   '41=MT,' +                       '94=ID,' +
  '09=NY,' +                   '42=FL,' +                       '95=WY,' +
  '10=AK,' +                   '43=IL,' +
  '11=NJ,' +                   '44=KS,' +
  '12=NC,' +                   '45=ME,' +
  '13=VT,' +                   '46=MS,' +
  '14=AL,' +                   '47=NE,' +
  '15=CO,' +                   '48=NV,' +
  '17=VA,' +                   '50=RI,' +
  '18=AK,' +                   '51=SD,' +
  '19=NY,' +                   '52=TN,' +
  '20=MN,' +                   '53=TX,' +
  '21=NJ,' +                   '54=WA,' +
  '23=GA,' +                   '56=NJ,' +
  '24=LA,' +                   '58=NH,' +
  '25=CA,' +                   '59=PA,' +
  '26=WV,' +                   '60=MI,' +
  '27=CT,' +                   '61=NJ,' +
  '28=UT,' +                   '62=MT,' +
  '29=PR,' +                   '63=AK,' +
  '30=OH,' +                   '75=CA,' +
  '31=KY,' +                   '78=NY,' +
  '32=RI,' +                   '85=NJ,' +
  '33=NM,' +                   '86=IA,' +
  '34=PR,' +                   '87=WI';

const
  SUIMap: string = '';
  
function CreateADPCO_AND_EEImport( aLogger: IImportLogger ): IImport;
function CreateADPPRImport( aLogger: IImportLogger ): IImport;

implementation

uses SDM_IM_ADP, genericImportBaseImpl, sImportHelpers;

function ConvertABA( const aLogger: IImportLogger; const Value: Variant ): Variant;
var
  s: string;
  i, a: Integer;
  Koef: Byte;
begin
  if VarIsNull(Value) or (Length(VarToStr(Value)) > 8) then
    Result := Value
  else
  begin
    s := VarToStr(Value);
    a := 0;
    for i := 1 to Length(s) do
    begin
      if i in [1,4,7] then
        Koef := 3
      else if i in [2,5,8] then
        Koef := 7
      else if i in [3,6] then
        Koef := 1
      else
        Koef := 0;
      a := a + StrToInt(s[i]) * Koef;
    end;
    a := a mod 10;
    if a <> 0 then
      a := 10 - a;
    Result := s + IntToStr(a);
  end;
end;

const
  sHiddenDivisionCode='     Hidden Division';
  sHiddenBranchCode  ='       Hidden Branch';


const
  ADPConversion: array [0..10] of TConvertDesc =
  (
    (Table: 'T_CO_DEPT'; Field:'DEPT_C'; Func: ConvertDBDTCustomNumber),
    (Table: 'V_EMP_ALL'; Field:'HOMEDEPARTMENT'; Func: ConvertDBDTCustomNumber),
    (Table: 'V_EMP_ALL'; Field:'SOCIALSECURITY#'; Func: ConvertSSN),
    (Table: 'V_EMP_ALL'; Field:'FILE#'; Func: ConvertEECustomNumber),
    (Table: 'V_BANKING_INFO'; Field:'FILE#'; Func: ConvertEECustomNumber),
    (Table: 'V_BANKING_INFO'; Field:'BANKDEPTRANSITABA'; Func: ConvertABA),
    (Table: 'V_DEDUCTIONS'; Field:'FILE#'; Func: ConvertEECustomNumber),
    (Table: 'V_TD_GOAL_ACCUM'; Field:'FILE#'; Func: ConvertEECustomNumber),
    (Table: 'V_LIENS'; Field:'FILE#'; Func: ConvertEECustomNumber),
    (Table: 'V_YTD_INFO'; Field:'FILE#'; Func: ConvertEECustomNumber),
    (Table: 'V_YTD_ACCUMULATOR'; Field:'FILE#'; Func: ConvertEECustomNumber)
  );

  ADPKeys: array [0..34] of TKeySourceDesc =
  (
    (ExternalTable: 'T_CO_DEPT'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'T_CO_DEPT'; EvoKeyTable: 'CO_DIVISION'; EvoKeyField: 'CUSTOM_DIVISION_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'T_CO_DEPT'; EvoKeyTable: 'CO_BRANCH'; EvoKeyField: 'CUSTOM_BRANCH_NUMBER'; HowToGet: ksFixedValue),
    (ExternalTable: 'T_CO_DEPT'; EvoKeyTable: 'CO_DEPARTMENT'; EvoKeyField: 'CUSTOM_DEPARTMENT_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'DEPT_C' ),

    (ExternalTable: 'V_EMP_ALL'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_EMP_ALL'; EvoKeyTable: 'CL_PERSON'; EvoKeyField: 'SOCIAL_SECURITY_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'SOCIALSECURITY#' ),
    (ExternalTable: 'V_EMP_ALL'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'FILE#' ),
    (ExternalTable: 'V_EMP_ALL'; EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksFromThisTable; ValueSourceField: 'CURRWORKDSTATESEQ#' ),

    (ExternalTable: 'V_BANKING_INFO'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_BANKING_INFO'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'FILE#' ),
    (ExternalTable: 'V_BANKING_INFO'; EvoKeyTable: 'EE_DIRECT_DEPOSIT'; EvoKeyField: 'EE_ABA_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'BANKDEPTRANSITABA' ),
    (ExternalTable: 'V_BANKING_INFO'; EvoKeyTable: 'EE_DIRECT_DEPOSIT'; EvoKeyField: 'EE_BANK_ACCOUNT_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'BANKDEPACCOUNTNUM' ),

    (ExternalTable: 'V_DEDUCTIONS'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_DEDUCTIONS'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'FILE#' ),

    (ExternalTable: 'V_TD_GOAL_ACCUM'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_TD_GOAL_ACCUM'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'FILE#' ),

    (ExternalTable: 'V_LIENS'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_LIENS'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'FILE#' ),

    (ExternalTable: 'V_YTD_INFO'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_INFO'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'FILE#' ),
    (ExternalTable: 'V_YTD_INFO'; EvoKeyTable: 'PR'; EvoKeyField: 'CHECK_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_INFO'; EvoKeyTable: 'PR'; EvoKeyField: 'RUN_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_INFO'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_BEGIN_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_INFO'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_END_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_INFO'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'FREQUENCY'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_INFO'; EvoKeyTable: 'PR_CHECK'; EvoKeyField: 'CHECK_TYPE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_INFO'; EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksFromThisTable; ValueSourceField: 'CURRWORKDSTATESEQ#' ),

    (ExternalTable: 'V_YTD_ACCUMULATOR'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_ACCUMULATOR'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'FILE#' ),
    (ExternalTable: 'V_YTD_ACCUMULATOR'; EvoKeyTable: 'PR'; EvoKeyField: 'CHECK_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_ACCUMULATOR'; EvoKeyTable: 'PR'; EvoKeyField: 'RUN_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_ACCUMULATOR'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_BEGIN_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_ACCUMULATOR'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_END_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_ACCUMULATOR'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'FREQUENCY'; HowToGet: ksFixedValue ),
    (ExternalTable: 'V_YTD_ACCUMULATOR'; EvoKeyTable: 'PR_CHECK'; EvoKeyField: 'CHECK_TYPE'; HowToGet: ksFixedValue )
  );

  ADPRowImport: array [0..7] of TRowImportDesc =
  (
    ( ExternalTable: 'V_EMP_ALL'; CustomFunc: 'REHIRE_DATE'),
    ( ExternalTable: 'V_BANKING_INFO'; CustomFunc: 'DD_ALL'),
    ( ExternalTable: 'V_DEDUCTIONS'; CustomFunc: 'ED_ALL'),
    ( ExternalTable: 'V_TD_GOAL_ACCUM'; CustomFunc: 'TARGET_ALL'),
    ( ExternalTable: 'V_LIENS'; CustomFunc: 'LIENS_ALL'),
    ( ExternalTable: 'V_YTD_INFO'; CustomFunc: 'YTD_INFO_ALL'),
    ( ExternalTable: 'V_YTD_ACCUMULATOR'; CustomFunc: 'YTD_ED_HOURS'),
    ( ExternalTable: 'V_YTD_ACCUMULATOR'; CustomFunc: 'YTD_ED_AMOUNT')
  );

  ADPMap: array [0..47] of TMapRecord =
  (
    (ITable: 'T_CO_DEPT'; IField: 'DEPT_C'; ETable: 'CO_DEPARTMENT'; EField: 'CUSTOM_DEPARTMENT_NUMBER';  MapType: mtDirect ),
    (ITable: 'T_CO_DEPT'; IField: 'DESCRIPTION_TX'; ETable: 'CO_DEPARTMENT'; EField: 'NAME';  MapType: mtDirect ),

    //CL_PERSON
    (ITable: 'V_EMP_ALL'; IField: 'SOCIALSECURITY#'; ETable: 'CL_PERSON'; EField: 'SOCIAL_SECURITY_NUMBER'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'LASTNAME'; ETable: 'CL_PERSON'; EField: 'LAST_NAME'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'FIRSTNAME'; MapType: mtCustom; FuncName: 'IEFirstNameMI'),
    (ITable: 'V_EMP_ALL'; IField: 'GENDER'; ETable: 'CL_PERSON'; EField: 'GENDER'; MapType: mtMap;
                                  MapValues: 'M=' + GROUP_BOX_MALE + ',' +
                                             'F=' + GROUP_BOX_FEMALE; MapDefault: GROUP_BOX_UNKOWN),
    (ITable: 'V_EMP_ALL'; IField: 'BIRTHDATE'; ETable: 'CL_PERSON'; EField: 'BIRTH_DATE'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'STREETLINE1'; ETable: 'CL_PERSON'; EField: 'ADDRESS1'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'STREETLINE2'; ETable: 'CL_PERSON'; EField: 'ADDRESS2'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'CITY'; ETable: 'CL_PERSON'; EField: 'CITY'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'STATE'; MapType: mtCustom; FuncName: 'E1_STATE'),
    (ITable: 'V_EMP_ALL'; IField: 'ZIPCODE'; ETable: 'CL_PERSON'; EField: 'ZIP_CODE'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'AREACODEPHONENUM'; ETable: 'CL_PERSON'; EField: 'PHONE1'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'ETHNICCODE'; ETable: 'CL_PERSON'; EField: 'ETHNICITY'; MapType: mtMap;
                                  MapValues: '1=' + ETHNIC_CAUCASIAN
                                              + ',2=' + ETHNIC_AFRICAN
                                              + ',3=' + ETHNIC_HISPANIC
                                              + ',4=' + ETHNIC_ASIAN
                                              + ',5=' +  ETHNIC_INDIAN
                                             ; MapDefault: ETHNIC_NOT_APPLICATIVE),
    //  EE
    (ITable: 'V_EMP_ALL'; IField: 'EEOCOCCCD'; ETable: 'EE'; EField: 'SY_HR_EEO_NBR'; MapType: mtMapLookup; MLookupTable: 'SY_HR_EEO'; MLookupField: 'DESCRIPTION';
                                   MMapValues: '"1=Officials And Managers",' +
                                               '"2=Professional",' +
                                               '"3=Technicians",' +
                                               '"4=Sales Workers",' +
                                               '"5=Office Clerical",' +
                                               '"6=Craft Workers - Skilled",' +
                                               '"7=Operatives - Semi-Skilled",' +
                                               '"8=Laborers - Unskilled",' +
                                               '"9=Service Workers"'
                                             ; MMapDefault: 'No EEO Class Assigned'),
    (ITable: 'V_EMP_ALL'; IField: 'FILE#'; ETable: 'EE'; EField: 'CUSTOM_EMPLOYEE_NUMBER'; MapType: mtDirect ),

    (ITable: 'V_EMP_ALL'; IField: 'FEDMARITALSTATUS'; ETable: 'EE'; EField: 'FEDERAL_MARITAL_STATUS'; MapType: mtDirect ),  //!!
    (ITable: 'V_EMP_ALL'; IField: 'FEDEXEMPTIONS'; ETable: 'EE'; EField: 'NUMBER_OF_DEPENDENTS'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'CLOCK#'; ETable: 'EE'; EField: 'TIME_CLOCK_NUMBER'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'HIREDATE'; ETable: 'EE'; EField: 'ORIGINAL_HIRE_DATE'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'STATUS'; ETable: 'EE'; EField: 'CURRENT_TERMINATION_CODE'; MapType: mtMap;
                                  MapValues: 'A=' + EE_TERM_ACTIVE
                                              + ',L=' + EE_TERM_LEAVE
                                              + ',T=' + EE_TERM_TERMINATED
                                              + ',D=' + EE_TERM_DEATH),
    (ITable: 'V_EMP_ALL'; IField: 'TERMINATIONDATE'; ETable: 'EE'; EField: 'CURRENT_TERMINATION_DATE'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'REVIEWDATE'; ETable: 'EE'; EField: 'REVIEW_DATE'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'NEXTREVIEWDATE'; ETable: 'EE'; EField: 'NEXT_REVIEW_DATE'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'PAYFRQ'; ETable: 'EE'; EField: 'PAY_FREQUENCY'; MapType: mtMap;
                                  MapValues: 'W=' + FREQUENCY_TYPE_WEEKLY
                                              + ',B=' + FREQUENCY_TYPE_BIWEEKLY
                                              + ',S=' + FREQUENCY_TYPE_SEMI_MONTHLY
                                              + ',M=' + FREQUENCY_TYPE_MONTHLY),
    (ITable: 'V_EMP_ALL'; IField: 'ANNUALSALARY'; ETable: 'EE'; EField: 'CALCULATED_SALARY'; MapType: mtDirect),

    (ITable: 'V_EMP_ALL'; IField: 'FEDTAXCALCSTATUSCD'; ETable: 'EE'; EField: 'EXEMPT_EXCLUDE_EE_FED'; MapType: mtMap;
                                  MapValues: 'E=' + GROUP_BOX_EXEMPT
                                              + ',X=' + GROUP_BOX_EXCLUDE;
                                  MapDefault: GROUP_BOX_INCLUDE),
    (ITable: 'V_EMP_ALL'; IField: 'FEDTAXMOD$'; MapType: mtCustom; FuncName: 'AdditionalTax'),
    (ITable: 'V_EMP_ALL'; IField: 'FEDTAXMODPCT'; MapType: mtCustom; FuncName: 'AdditionalTax'),
    (ITable: 'V_EMP_ALL'; IField: 'FUTATXBLCALCSTATUS'; ETable: 'EE'; EField: 'EXEMPT_EMPLOYER_FUI'; MapType: mtMap;
                                  MapValues: 'X=' + GROUP_BOX_YES;
                                  MapDefault: GROUP_BOX_NO),
    (ITable: 'V_EMP_ALL'; IField: 'SOCSECTAXCALCSTA'; MapType: mtCustom; FuncName: 'OASDITax'),
    (ITable: 'V_EMP_ALL'; IField: 'MEDTAXCALCSTATUS'; MapType: mtCustom; FuncName: 'MedicareTax'),
    (ITable: 'V_EMP_ALL'; IField: 'STANDARDHOURS'; ETable: 'EE'; EField: 'STANDARD_HOURS'; MapType: mtDirect),

    //EE_RATES
//    (ITable: 'V_EMP_ALL'; IField: 'RATE1AMT'; MapType: mtCustom; FuncName: 'Rate'),
    (ITable: 'V_EMP_ALL'; IField: 'RATE2AMT'; MapType: mtCustom; FuncName: 'Rate'),
    (ITable: 'V_EMP_ALL'; IField: 'RATE3AMT'; MapType: mtCustom; FuncName: 'Rate'),
    // EE_STATES
    (ITable: 'V_EMP_ALL'; IField: 'CURRWORKDSTATESEQ#'; MapType: mtCustom; FuncName: 'HomeTax'),
    (ITable: 'V_EMP_ALL'; IField: 'SUISDITAXJURISCD'; MapType: mtCustom; FuncName: 'HomeSui'),

    (ITable: 'V_EMP_ALL'; IField: 'TAXSTAT'; ETable: 'EE_STATES'; EField: 'STATE_EXEMPT_EXCLUDE'; MapType: mtMap;
                                  MapValues: 'E=' + GROUP_BOX_EXEMPT
                                              + ',X=' + GROUP_BOX_EXCLUDE;
                                  MapDefault: GROUP_BOX_INCLUDE),
    (ITable: 'V_EMP_ALL'; IField: 'EXTRATAXTYPE'; ETable: 'EE_STATES'; EField: 'OVERRIDE_STATE_TAX_TYPE'; MapType: mtMap;
                                  MapValues: 'D=' + OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT
                                              + ',P=' + OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT;
                                  MapDefault: OVERRIDE_VALUE_TYPE_NONE),
    (ITable: 'V_EMP_ALL'; IField: 'STATEXTRATAX'; ETable: 'EE_STATES'; EField: 'OVERRIDE_STATE_TAX_VALUE'; MapType: mtDirect),
    (ITable: 'V_EMP_ALL'; IField: 'SUISDITAXCALCSTAT'; MapType: mtCustom; FuncName: 'SUISDITax'),

    (ITable: 'V_BANKING_INFO'; IField: 'BANKDEPACCOUNTNUM'; ETable: 'EE_DIRECT_DEPOSIT'; EField: 'EE_BANK_ACCOUNT_NUMBER'; MapType: mtDirect),
    (ITable: 'V_BANKING_INFO'; IField: 'BANKDEPTRANSITABA'; ETable: 'EE_DIRECT_DEPOSIT'; EField: 'EE_ABA_NUMBER'; MapType: mtDirect),
    (ITable: 'V_BANKING_INFO'; IField: 'BANKDEPCODE'; MapType: mtCustom; FuncName: 'BankAccountType'),

    (ITable: 'V_YTD_INFO'; IField: 'YTDFEDTAX$'; ETable: 'PR_CHECK'; EField: 'FEDERAL_TAX'; MapType: mtDirect),
    (ITable: 'V_YTD_INFO'; IField: 'YTDSOCSECTAX$'; ETable: 'PR_CHECK'; EField: 'EE_OASDI_TAX'; MapType: mtDirect),
    (ITable: 'V_YTD_INFO'; IField: 'YTDMEDICARE$'; ETable: 'PR_CHECK'; EField: 'EE_MEDICARE_TAX'; MapType: mtDirect),
    (ITable: 'V_YTD_INFO'; IField: 'YTDGROSS$'; ETable: 'PR_CHECK'; EField: 'GROSS_WAGES'; MapType: mtDirect)
  );

type
  TADPImportDef = class( TImportDefinition )
  protected
    CheckLineNbr: Integer;
    function MatchState(Input: Variant): Variant;
    function MatchSUI(Input: Variant): Variant;
  public
    constructor Create(const AIEMap: IIEMap); override;
    destructor Destroy; override;
  published
    procedure IEFirstNameMI(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure Rate(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure HomeTax(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure HomeSui(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure AdditionalTax(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure OASDITax(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure MedicareTax(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure SUISDITax(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure BankAccountType(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure E1_STATE(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);
    procedure REHIRE_DATE(const Input, Keys, Output: TFieldValues);
    procedure DD_ALL(const Input, Keys, Output: TFieldValues);
    procedure ED_ALL(const Input, Keys, Output: TFieldValues);
    procedure TARGET_ALL(const Input, Keys, Output: TFieldValues);
    procedure LIENS_ALL(const Input, Keys, Output: TFieldValues);
    procedure YTD_INFO_ALL(const Input, Keys, Output: TFieldValues);
    procedure YTD_ED_HOURS(const Input, Keys, Output: TFieldValues);
    procedure YTD_ED_AMOUNT(const Input, Keys, Output: TFieldValues);
    procedure AI_EE(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_RATES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_DIRECT_DEPOSIT(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_SCHEDULED_E_DS(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_CHILD_SUPPORT_CASES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_DIVISION(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_BRANCH(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_DEPARTMENT(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_BATCH(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_LINES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_LOCALS(const ds: TevClientDataSet; const Keys: TFieldValues );
  end;

const
  ADPCO_AND_EEimport: TImportDesc = (
    ImportName: 'ADP Employee Import';
    RequiredMatchersNames: sBankTypeBindingName
                             + #13#10 + sLocalsBindingName
                            + #13#10 + sEDBindingName
                            + #13#10 + sAgencyBindingName;
    ExternalDataModuleImplClass: TDM_IM_ADP;
    ImportDefinitionImplClass: TADPImportDef
  );
  ADPPRimport: TImportDesc = (
    ImportName: 'ADP Payroll Import';
    RequiredMatchersNames: sEDHoursBindingName
                             + #13#10 + sEDAmountsBindingName
                             + #13#10 + sLocalsBindingName;
    ExternalDataModuleImplClass: TDM_IM_ADP;
    ImportDefinitionImplClass: TADPImportDef
  );

type
  TADPCO_AND_EEImport = class (TImportBase)
  protected
    procedure DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings ); override;
  end;

  TADPPRImport = class (TImportBase)
  protected
    procedure DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings ); override;
  end;

const
  ADPBindingDesc: array [0..3] of TBindingDesc = (
    ( Name: sLocalsBindingName;
      Caption: 'Mapped Locals';
      LeftDesc: ( Caption: 'ADP Locals'; Unique: true; KeyFields: 'TAXCODE'; ListFields: {State;}'Description');
      RightDesc: ( Caption: 'Evolution Locals'; Unique: true; KeyFields: 'CO_LOCAL_TAX_NBR'; ListFields: 'LocalState;LocalName')
    ),
    ( Name: sEDBindingName;
      Caption: 'Mapped E/Ds';
      LeftDesc: ( Caption: 'ADP E/Ds'; Unique: true; KeyFields: 'eord;code'; ListFields: 'eord;CODE;TYPE;NAME');
      RightDesc: ( Caption: 'Evolution Client E/Ds'; Unique: false; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION')
    ),
    ( Name: sBankTypeBindingName;
      Caption: 'Mapped Bank Types';
      LeftDesc: ( Caption: 'ADP Deposit Deductions'; Unique: true; KeyFields: 'code'; ListFields: 'eord;CODE;TYPE;NAME');
      RightDesc: ( Caption: 'Evolution Bank Types'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
    ),
    ( Name: sAgencyBindingName;
      Caption: 'Mapped Agencies';
      LeftDesc: ( Caption: 'ADP Payees'; Unique: true; KeyFields: 'payeecode'; ListFields: 'payeenameline1;payeenameline2');
      RightDesc: ( Caption: 'Evolution Agencies'; Unique: true; KeyFields: 'CL_AGENCY_NBR'; ListFields: 'Agency_Name')
    )
  );

  ADPPRBindingDesc: array [0..2] of TBindingDesc = (
    ( Name: sLocalsBindingName;
      Caption: 'Mapped Locals';
      LeftDesc: ( Caption: 'ADP Locals'; Unique: true; KeyFields: 'TAXCODE'; ListFields: {State;}'Description');
      RightDesc: ( Caption: 'Evolution Locals'; Unique: true; KeyFields: 'CO_LOCAL_TAX_NBR'; ListFields: 'LocalState;LocalName')
    ),
    ( Name: sEDHoursBindingName;
      Caption: 'Mapped E/Ds';
      LeftDesc: ( Caption: 'ADP Hours Accumulators'; Unique: true; KeyFields: 'ACC_C'; ListFields: 'ACC_C;description_tx');
      RightDesc: ( Caption: 'Evolution Client E/Ds'; Unique: false; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION')
    ),
    ( Name: sEDAmountsBindingName;
      Caption: 'Mapped E/Ds';
      LeftDesc: ( Caption: 'ADP Amount Accumulators'; Unique: true; KeyFields: 'ACC_C'; ListFields: 'ACC_C;description_tx');
      RightDesc: ( Caption: 'Evolution Client E/Ds'; Unique: false; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION')
    )
  );

function CreateADPCO_AND_EEImport( aLogger: IImportLogger ): IImport;
begin
  Result := TADPCO_AND_EEImport.Create( aLogger, ADPCO_AND_EEimport, ADPBindingDesc );
end;

function CreateADPPRImport( aLogger: IImportLogger ): IImport;
begin
  Result := TADPPRImport.Create( aLogger, ADPPRimport, ADPPRBindingDesc );
end;

{ TADPCO_AND_EEImport }

procedure TADPCO_AND_EEImport.DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings);
var
  EETerm: Boolean;
begin
  EETerm := aParams.Values[sEETermedParam] = 'Y';
  DataModule.GetTable('V_BANKING_INFO').Filtered := EETerm;
  DataModule.GetTable('V_EMP_ALL').Filtered := EETerm;
  DataModule.GetTable('V_DEDUCTIONS').Filtered := EETerm;
  DataModule.GetTable('V_TD_GOAL_ACCUM').Filtered := EETerm;
  DataModule.GetTable('V_LIENS').Filtered := EETerm;
  inherited;
  aImportDefinition.SetFixedKeys( 'CO.CUSTOM_COMPANY_NUMBER=' + DataModule.CustomCompanyNumber+#13#10 +
                                  'CO_DIVISION.CUSTOM_DIVISION_NUMBER='+sHiddenDivisionCode+#13#10 +
                                  'CO_BRANCH.CUSTOM_BRANCH_NUMBER='+sHiddenBranchCode);
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'T_CO_DEPT', 'Importing departments' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'V_EMP_ALL', 'Importing employees' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'V_BANKING_INFO', 'Importing Direct Deposits' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'V_DEDUCTIONS', 'Importing Scheduled E/Ds' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'V_TD_GOAL_ACCUM', 'Importing Targets' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'V_LIENS', 'Importing Liens' );
end;

{ TADPImportDef }

procedure TADPImportDef.AdditionalTax(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
begin
  if VarToStr(Value) <> '' then
  begin
    if Field = 'FEDTAXMOD$' then
      Output.Add('EE', 'OVERRIDE_FED_TAX_TYPE', OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT)
    else
      Output.Add('EE', 'OVERRIDE_FED_TAX_TYPE', OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT);
    Output.Add('EE', 'OVERRIDE_FED_TAX_VALUE', Value)
  end;
end;

procedure TADPImportDef.AI_EE_CHILD_SUPPORT_CASES(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds['PRIORITY_NUMBER'] := 1;
end;

procedure TADPImportDef.AI_CO_BRANCH(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  if FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').IsNull then
  begin
    ds.Cancel;
    RaiseMissingValue( 'Please define company Home State for %s', [VarToStr(FImpEngine.IDS('CO')['CUSTOM_COMPANY_NUMBER'])] );
  end;
  ds.FieldByName('HOME_CO_STATES_NBR').AsInteger := FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').AsInteger;
  ds.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  ds.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').AsString := EXCLUDE_REPT_AND_ADDRESS_ON_CHECK;
  ds.FieldByName('PAY_FREQUENCY_HOURLY').AsString :=  FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
  ds.FieldByName('PAY_FREQUENCY_SALARY').AsString := FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
end;

procedure TADPImportDef.AI_CO_DEPARTMENT(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  if FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').IsNull then
  begin
    ds.Cancel;
    RaiseMissingValue( 'Please define company Home State for %s', [ VarToStr(FImpEngine.IDS('CO')['CUSTOM_COMPANY_NUMBER']) ] );
  end;
  ds.FieldByName('HOME_CO_STATES_NBR').AsInteger := FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').AsInteger;
  ds.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  ds.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').AsString := INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK;
  ds.FieldByName('PAY_FREQUENCY_HOURLY').AsString :=  FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
  ds.FieldByName('PAY_FREQUENCY_SALARY').AsString := FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
end;

procedure TADPImportDef.AI_CO_DIVISION(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  if FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').IsNull then
  begin
    ds.Cancel;
    RaiseMissingValue( 'Please define company Home State for %s', [ VarToStr(FImpEngine.IDS('CO')['CUSTOM_COMPANY_NUMBER']) ] );
  end;
  ds.FieldByName('HOME_CO_STATES_NBR').AsInteger := FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').AsInteger;
  ds.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  ds.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').AsString := EXCLUDE_REPT_AND_ADDRESS_ON_CHECK;
  ds.FieldByName('PAY_FREQUENCY_HOURLY').AsString :=  FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
  ds.FieldByName('PAY_FREQUENCY_SALARY').AsString := FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
end;

procedure TADPImportDef.AI_EE_DIRECT_DEPOSIT(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds['IN_PRENOTE'] := GROUP_BOX_NO;
end;

procedure TADPImportDef.AI_EE_RATES(const ds: TevClientDataSet;
  const Keys: TFieldValues);
var
  i: Integer;
begin
  for i := 0 to High(Keys.Fields) do
    if Keys.Fields[i] = 'PRIMARY_RATE' then
    begin
      ds['RATE_NUMBER'] := 1;
      Break;
    end
    else if Keys.Fields[i] = 'RATE_NUMBER' then
    begin
      if Keys.Values[i] = '1' then
        ds['PRIMARY_RATE'] := GROUP_BOX_YES;
      Break;
    end;
end;

procedure TADPImportDef.AI_EE_SCHEDULED_E_DS(const ds: TevClientDataSet;
  const Keys: TFieldValues);
  procedure SetDefault( fname: string; srcfname: string = '' );
  begin
    if srcfname = '' then
      srcfname := 'SD_'+fname;
    if not VarIsNull( FImpEngine.IDS('CL_E_DS')[srcfname] ) then
      ds[fname] := FImpEngine.IDS('CL_E_DS')[srcfname];
  end;
var
  Q: IevQuery;
begin
  ds['EFFECTIVE_START_DATE'] := Date;
  ds['FREQUENCY'] := SCHED_FREQ_DAILY;
  ds['EXCLUDE_WEEK_1'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_2'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_3'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_4'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_5'] := GROUP_BOX_NO;
  if FImpEngine.IDS('CL_E_DS')['SCHEDULED_DEFAULTS'] = GROUP_BOX_YES then
  begin
    SetDefault( 'FREQUENCY' );
    SetDefault( 'EFFECTIVE_START_DATE' );
    SetDefault( 'EXCLUDE_WEEK_1' );
    SetDefault( 'EXCLUDE_WEEK_2' );
    SetDefault( 'EXCLUDE_WEEK_3' );
    SetDefault( 'EXCLUDE_WEEK_4' );
    SetDefault( 'EXCLUDE_WEEK_5' );
    SetDefault( 'CALCULATION_TYPE', 'SD_CALCULATION_METHOD' );
    SetDefault( 'EXPRESSION' );
    SetDefault( 'CL_E_D_GROUPS_NBR', 'CL_E_D_GROUPS_NBR' );
    SetDefault( 'CL_AGENCY_NBR', 'DEFAULT_CL_AGENCY_NBR' );
    SetDefault( 'AMOUNT' );
    SetDefault( 'PERCENTAGE', 'SD_RATE' );
    /////////////////////////////////////
    SetDefault( 'WHICH_CHECKS');
    SetDefault( 'PRIORITY_NUMBER');
    SetDefault( 'ALWAYS_PAY');
    SetDefault( 'PLAN_TYPE');
    SetDefault( 'MAX_AVG_AMT_CL_E_D_GROUPS_NBR','SD_MAX_AVG_AMT_GRP_NBR');
    SetDefault( 'MAX_AVG_HRS_CL_E_D_GROUPS_NBR','SD_MAX_AVG_HRS_GRP_NBR' );
    SetDefault( 'MAX_AVERAGE_HOURLY_WAGE_RATE', 'SD_MAX_AVERAGE_HOURLY_WAGE_RATE');
    SetDefault( 'THRESHOLD_E_D_GROUPS_NBR','SD_THRESHOLD_E_D_GROUPS_NBR');
    SetDefault( 'THRESHOLD_AMOUNT', 'SD_THRESHOLD_AMOUNT' );
    SetDefault( 'DEDUCTIONS_TO_ZERO', 'SD_DEDUCTIONS_TO_ZERO' );
    Q := TevQuery.Create('SELECT CL_E_DS_NBR, CO_BENEFIT_SUBTYPE_NBR FROM CO_E_D_CODES WHERE {AsOfNow<CO_E_D_CODES>} and CL_E_DS_NBR=' + IntToStr(FImpEngine.IDS('CL_E_DS')['CL_E_DS_NBR']));
    Q.Execute;
    if Q.Result.RecordCount > 0 then
      ds['CO_BENEFIT_SUBTYPE_NBR'] := Q.Result.FieldByName('CO_BENEFIT_SUBTYPE_NBR').Value;
    /////////////////////////////////////


  end
end;

procedure TADPImportDef.AI_EE_STATES(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds.FieldByName('IMPORTED_MARITAL_STATUS').AsString := FImpEngine.IDS('EE').FieldByName('FEDERAL_MARITAL_STATUS').AsString;
end;

procedure TADPImportDef.BankAccountType(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
begin
  Output.Add('EE_DIRECT_DEPOSIT', 'EE_BANK_ACCOUNT_TYPE', Matchers.Get(sBankTypeBindingName).RightMatch( Value ));
end;

constructor TADPImportDef.Create(const AIEMap: IIEMap);
begin
  inherited Create(AIEMap);

  RegisterConvertDescs( ADPConversion );
  RegisterKeySourceDescs( ADPKeys );
  RegisterRowImport(ADPRowImport);
  RegisterMap( ADPMap );
end;

procedure TADPImportDef.DD_ALL(const Input, Keys, Output: TFieldValues);
var
  ed_custom_number: Variant;
begin
  ed_custom_number := Matchers.Get(sEDBindingName).RightMatch( VarArrayOf( ['D', Input['V_BANKING_INFO.DEDUCTIONCODE']] ) );
  if not VarIsNull( ed_custom_number ) then
  begin
    Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', ed_custom_number);
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_E_DS'));
    Output.Add( 'EE_SCHEDULED_E_DS', 'CL_E_DS_NBR', FImpEngine.IDS('CL_E_DS').Lookup('CUSTOM_E_D_CODE_NUMBER', ed_custom_number, 'CL_E_DS_NBR') );
    Output.Add( 'EE_SCHEDULED_E_DS', 'DEDUCT_WHOLE_CHECK', Input['V_BANKING_INFO.BANKFULLDEPIND'] );
    if VarToStr(Input['V_BANKING_INFO.BANKFULLDEPIND']) = GROUP_BOX_YES then
      Output.Add( 'EE_SCHEDULED_E_DS', 'CALCULATION_TYPE', CALC_METHOD_NONE );
    Output.Add( 'EE_SCHEDULED_E_DS', 'AMOUNT', Input['V_BANKING_INFO.DEDUCTIONAMOUNT'] );
  end;
end;

procedure TADPImportDef.ED_ALL(const Input, Keys, Output: TFieldValues);
var
  ed_custom_number: Variant;
begin
  ed_custom_number := Matchers.Get(sEDBindingName).RightMatch( VarArrayOf( ['D', Input['V_DEDUCTIONS.DEDUCTIONCODE']] ) );
  if not VarIsNull( ed_custom_number ) then
  begin
    Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', ed_custom_number);
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_E_DS'));
    Output.Add( 'EE_SCHEDULED_E_DS', 'CL_E_DS_NBR', FImpEngine.IDS('CL_E_DS').Lookup('CUSTOM_E_D_CODE_NUMBER', ed_custom_number, 'CL_E_DS_NBR') );
    Output.Add( 'EE_SCHEDULED_E_DS', 'PERCENTAGE', Input['V_DEDUCTIONS.DEDFACTOR'] );
    Output.Add( 'EE_SCHEDULED_E_DS', 'AMOUNT', Input['V_DEDUCTIONS.DEDUCTIONAMOUNT'] );
  end;
end;

procedure TADPImportDef.HomeSui(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
var
  v: Variant;
begin
  v := MatchState(Value);
  Output.Add('EE_STATES', 'SUI_APPLY_CO_STATES_NBR', v);
  Output.Add('EE_STATES', 'SDI_APPLY_CO_STATES_NBR', v);
end;

procedure TADPImportDef.HomeTax(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
var
  v: Variant;
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'));
  v := FImpEngine.IDS('CO_STATES').Lookup('CO_NBR;STATE', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], Value]), 'CO_STATES_NBR');
  Output.Add('EE_STATES', 'CO_STATES_NBR', v);
  Output.Add('EE', 'HOME_TAX_EE_STATES_NBR', VarArrayOf(['EE_STATES', 'CO_STATES_NBR', v, 'EE_STATES_NBR']));
end;

procedure TADPImportDef.IEFirstNameMI(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
var
  fn, mi: string;
begin
  fn := VarToStr(Value);
  mi := '';
  if (Length(fn) > 0) and
     (fn[Length(fn)] = '.') then
   Delete(fn, Length(fn), 1);
  if (Length(fn) > 2) and
     (fn[Length(fn) - 1] = ' ') then
  begin
    mi := fn[Length(fn)];
    fn := Copy(fn, 1, Length(fn) - 2);
  end;
  if Trim(fn) <> '' then
    Output.Add('CL_PERSON', 'FIRST_NAME', fn);
  if Trim(mi) <> '' then
    Output.Add('CL_PERSON', 'MIDDLE_INITIAL', mi);
end;

procedure TADPImportDef.LIENS_ALL(const Input, Keys, Output: TFieldValues);
var
  ed_custom_number, agency: Variant;
begin
  ed_custom_number := Matchers.Get(sEDBindingName).RightMatch( VarArrayOf( ['D', Input['V_LIENS.LIENDEDUCTION']] ) );
  agency := Matchers.Get(sAgencyBindingName).RightMatch( Input['V_LIENS.PAYEECODE'] );
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_AGENCY'), 'CL_AGENCY_NBR=' + VarToStr(agency));
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SB_AGENCY'), 'SB_AGENCY_NBR=' + IntToStr(FImpEngine.IDS('CL_AGENCY').FieldByName('SB_AGENCY_NBR').AsInteger));
  if not VarIsNull( ed_custom_number ) then
  begin
    Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', ed_custom_number);
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_E_DS'));
    if FImpEngine.IDS('SB_AGENCY')['AGENCY_TYPE'] <> AGENCY_TYPE_CHILD_SUPPORT then
    begin
      Output.Add( 'EE_SCHEDULED_E_DS', 'GARNISNMENT_ID', Input['V_LIENS.LIENCASE#'] );
      Output.Add( 'EE_SCHEDULED_E_DS', 'CL_AGENCY_NBR', agency );
    end
    else
    begin
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE'), 'CUSTOM_EMPLOYEE_NUMBER=''' + VarToStr(Input['V_LIENS.FILE#']) + '''');
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE_CHILD_SUPPORT_CASES'), 'EE_NBR=' + IntToStr(FImpEngine.IDS('EE').FieldByName('EE_NBR').AsInteger) + ' and ' +
                                                         'CUSTOM_CASE_NUMBER=''' + Input['V_LIENS.LIENCASE#'] + '''');
      if FImpEngine.IDS('EE_CHILD_SUPPORT_CASES').RecordCount = 0 then
      begin
        Output.Add( 'EE_CHILD_SUPPORT_CASES', 'CUSTOM_CASE_NUMBER', Input['V_LIENS.LIENCASE#'] );
        Output.Add( 'EE_CHILD_SUPPORT_CASES', 'ORIGINATION_STATE', Input['V_LIENS.COURTORDERSTATE'] );
        Output.Add( 'EE_CHILD_SUPPORT_CASES', 'CL_AGENCY_NBR', agency );
        Output.Add( 'EE_CHILD_SUPPORT_CASES', 'FIPS_CODE', Input['V_LIENS.FIPSCODE'] );
        Output.Add( 'EE_SCHEDULED_E_DS', 'EE_CHILD_SUPPORT_CASES_NBR', VarArrayOf(['EE_CHILD_SUPPORT_CASES', 'CUSTOM_CASE_NUMBER', Input['V_LIENS.LIENCASE#'], 'EE_CHILD_SUPPORT_CASES_NBR']));
      end
      else
        Output.Add( 'EE_SCHEDULED_E_DS', 'EE_CHILD_SUPPORT_CASES_NBR', FImpEngine.IDS('EE_CHILD_SUPPORT_CASES')['EE_CHILD_SUPPORT_CASES_NBR']);
    end;
  end;
end;

procedure TADPImportDef.MedicareTax(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
begin
  if Value = 'X' then
  begin
    Output.Add( 'EE', 'EXEMPT_EMPLOYEE_MEDICARE', GROUP_BOX_YES);
    Output.Add( 'EE', 'EXEMPT_EMPLOYER_MEDICARE', GROUP_BOX_YES);
  end
  else
  begin
    Output.Add( 'EE', 'EXEMPT_EMPLOYEE_MEDICARE', GROUP_BOX_NO);
    Output.Add( 'EE', 'EXEMPT_EMPLOYER_MEDICARE', GROUP_BOX_NO);
  end;
end;

procedure TADPImportDef.OASDITax(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
begin
  if Value = 'X' then
  begin
    Output.Add( 'EE', 'EXEMPT_EMPLOYEE_OASDI', GROUP_BOX_YES);
    Output.Add( 'EE', 'EXEMPT_EMPLOYER_OASDI', GROUP_BOX_YES);
  end
  else
  begin
    Output.Add( 'EE', 'EXEMPT_EMPLOYEE_OASDI', GROUP_BOX_NO);
    Output.Add( 'EE', 'EXEMPT_EMPLOYER_OASDI', GROUP_BOX_NO);
  end;
end;

procedure TADPImportDef.REHIRE_DATE(const Input, Keys,
  Output: TFieldValues);
var
  State: string;
  StMarStat: string;
  esms: string;
  Tmp: Integer;
  local, colocalsnbr: Variant;
  LocalExempt: string;
  division: Variant;
  branch: Variant;
  department: Variant;
begin
  if VarToStr(Input['V_EMP_ALL.STREETLINE1']) = '' then
  begin
    Output.Add('CL_PERSON', 'ADDRESS1', '<ADDRESS>');
    Logger.LogWarningFmt('Employee %s does not have a correct address. Please change it.', [Input['V_EMP_ALL.FILE#']]);
  end;
  if VarToStr(Input['V_EMP_ALL.CITY']) = '' then
  begin
    Output.Add('CL_PERSON', 'CITY', '<CITY>');
    Logger.LogWarningFmt('Employee %s does not have a correct city. Please change it.', [Input['V_EMP_ALL.FILE#']]);
  end;
  if (VarToStr(Input['V_EMP_ALL.ZIPCODE']) = '') or (VarToStr(Input['V_EMP_ALL.ZIPCODE']) = '0') then
  begin
    Output.Add('CL_PERSON', 'ZIP_CODE', '<ZIP>');
    Logger.LogWarningFmt('Employee %s does not have a correct zip code. Please change it.', [Input['V_EMP_ALL.FILE#']]);
  end;

  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DIVISION'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
  division := FImpEngine.IDS('CO_DIVISION').Lookup( 'CUSTOM_DIVISION_NUMBER', sHiddenDivisionCode, 'CO_DIVISION_NBR' );

  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_BRANCH'), 'CO_DIVISION_NBR='+VarToStr(division) );
  branch := FImpEngine.IDS('CO_BRANCH').Lookup( 'CUSTOM_BRANCH_NUMBER', sHiddenBranchCode, 'CO_BRANCH_NBR' );

  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DEPARTMENT'), 'CO_BRANCH_NBR='+VarToStr(branch) );
  department := FImpEngine.IDS('CO_DEPARTMENT').Lookup( 'CUSTOM_DEPARTMENT_NUMBER', Input['V_EMP_ALL.HOMEDEPARTMENT'], 'CO_DEPARTMENT_NBR' );

  Output.Add( 'EE', 'CO_DIVISION_NBR', division );
  Output.Add( 'EE', 'CO_BRANCH_NBR', branch );
  Output.Add( 'EE', 'CO_DEPARTMENT_NBR', department );

  if VarToStr(Input['V_EMP_ALL.REHIREDATE']) = '' then
    Output.Add( 'EE', 'CURRENT_HIRE_DATE', Input['V_EMP_ALL.HIREDATE'] )
  else
    Output.Add( 'EE', 'CURRENT_HIRE_DATE', Input['V_EMP_ALL.REHIREDATE'] );

  State := VarToStr(Input['V_EMP_ALL.CURRWORKDSTATESEQ#']);
  StMarStat := VarToStr(Input['V_EMP_ALL.STMARSTAT']);
  if State = 'AR' then
  begin
    if (StMarStat = 'M') or   //Married
       (StMarStat = 'N') or   //Married HOH
       (StMarStat = 'X') or   //Married 2 incomes
       (StMarStat = 'Y') then //Married HOH/2 incomes
      esms := 'C'
    else if (StMarStat = 'S') or   //Single
            (StMarStat = 'R') or   //Single HOH/Qual.dependent
            (StMarStat = 'T') then //Single HOH
      esms := 'A'
  end
  else if State = 'AZ' then
    esms := '10'
  else if State = 'CA' then
  begin
    if (StMarStat = 'M') or   //Married
       (StMarStat = 'N') or   //Married HOH
       (StMarStat = 'X') or   //Married 2 incomes
       (StMarStat = 'Y') then //Married HOH/2 incomes
      esms := 'M'
    else if (StMarStat = 'S') or   //Single
            (StMarStat = 'R') or   //Single HOH/Qual.dependent
            (StMarStat = 'T') then //Single HOH
      esms := 'SM'
  end
  else if State = 'CT' then
    esms := 'A'
  else if State = 'GA' then
  begin
    if (StMarStat = 'M') or   //Married
       (StMarStat = 'N') or   //Married HOH
       (StMarStat = 'X') or   //Married 2 incomes
       (StMarStat = 'Y') then //Married HOH/2 incomes
      esms := 'B1'
    else if (StMarStat = 'S') or   //Single
            (StMarStat = 'R') or   //Single HOH/Qual.dependent
            (StMarStat = 'T') then //Single HOH
      esms := 'A';
  end
  else if State = 'IA' then
    if (VarToStr(Input['V_EMP_ALL.STATEEXEMPTS']) = '') or (Input['V_EMP_ALL.STATEEXEMPTS'] < 2) then
      esms := 'S'
    else
      esms := 'S2'
  else if State = 'LA' then
    esms := '0'
  else if State = 'MA' then
    esms := '0'
  else if State = 'MD' then
    esms := 'RA'
  else if (State = 'MI') or
          (State = 'OH') or
          (State = 'VA') or
          (State = 'PA') then
    esms := 'SM'
  else if State = 'MO' then
  begin
    if (StMarStat = 'M') or   //Married
       (StMarStat = 'N') or   //Married HOH
       (StMarStat = 'X') or   //Married 2 incomes
       (StMarStat = 'Y') then //Married HOH/2 incomes
      esms := 'M0'
    else if (StMarStat = 'S') or   //Single
            (StMarStat = 'R') or   //Single HOH/Qual.dependent
            (StMarStat = 'T') then //Single HOH
      esms := 'S'
  end
  else if State = 'NC' then
  begin
    if (StMarStat = 'M') or   //Married
       (StMarStat = 'N') or   //Married HOH
       (StMarStat = 'X') or   //Married 2 incomes
       (StMarStat = 'Y') then //Married HOH/2 incomes
      esms := 'MW'
    else if (StMarStat = 'S') or   //Single
            (StMarStat = 'R') or   //Single HOH/Qual.dependent
            (StMarStat = 'T') then //Single HOH
      esms := 'S'
  end
  else if State = 'SC' then
    esms := '0'
  else if State = 'NJ' then
    if StMarStat = 'M' then
      esms := 'B'
    else
      esms := 'A'
  else
    if (StMarStat = 'M') or   //Married
       (StMarStat = 'N') or   //Married HOH
       (StMarStat = 'X') or   //Married 2 incomes
       (StMarStat = 'Y') then //Married HOH/2 incomes
      esms := 'M'
    else if (StMarStat = 'S') or   //Single
            (StMarStat = 'R') or   //Single HOH/Qual.dependent
            (StMarStat = 'T') then //Single HOH
      esms := 'S';

  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'), 'STATE=''' + State + '''' );
  if FImpEngine.IDS('SY_STATES').RecordCount <> 1 then
    RaiseUnknownCode( 'Cannot identify state by <%s>', [state] );

  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR='+vartostr(FImpEngine.IDS('SY_STATES')['SY_STATES_NBR']) );
  FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Locate('STATUS_TYPE', esms, []);
  Tmp := FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['SY_STATE_MARITAL_STATUS_NBR'];
  Output.Add('EE_STATES', 'SY_STATE_MARITAL_STATUS_NBR', Tmp);

  Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', esms );

  if VarToStr(Input['V_EMP_ALL.STATEEXEMPTS']) = '' then
    Output.Add( 'EE_STATES', 'STATE_NUMBER_WITHHOLDING_ALLOW', Input['V_EMP_ALL.FEDEXEMPTIONS'] )
  else
    Output.Add( 'EE_STATES', 'STATE_NUMBER_WITHHOLDING_ALLOW', Input['V_EMP_ALL.STATEEXEMPTS'] );

  local := Input['V_EMP_ALL.LCLTYWRKEDINCODE'];
  if not VarIsEmpty(local) and not VarIsNull(local) then
  begin
    colocalsnbr := Matchers.Get(sLocalsBindingName).RightMatch( local );
    if not VarIsNull(colocalsnbr) then
    begin
      Output.Add( 'EE_LOCALS', 'CO_LOCAL_TAX_NBR', colocalsnbr );

      LocalExempt := VarToStr(Input['V_EMP_ALL.LCLTAXSTAT']);
      if LocalExempt = 'X' then
        Output.Add( 'EE_LOCALS', 'EXEMPT_EXCLUDE', GROUP_BOX_EXCLUDE)
      else if LocalExempt = 'E' then
        Output.Add( 'EE_LOCALS', 'EXEMPT_EXCLUDE', GROUP_BOX_EXEMPT)
      else
        Output.Add( 'EE_LOCALS', 'EXEMPT_EXCLUDE', GROUP_BOX_INCLUDE);
    end;
  end;

  if Input['V_EMP_ALL.RATETYPE'] = 'S' then
  begin
    Keys.Add('EE_RATES', 'RATE_NUMBER', 1);
    Output.Add('EE', 'SALARY_AMOUNT', Input['V_EMP_ALL.RATE1AMT']);
    Output.Add('EE_RATES', 'RATE_AMOUNT', null);
  end
  else if Input['V_EMP_ALL.RATETYPE'] = 'H' then
  begin
    Keys.Add('EE_RATES', 'RATE_NUMBER', 1);
    Output.Add('EE', 'SALARY_AMOUNT', null);
    Output.Add('EE_RATES', 'RATE_AMOUNT', Input['V_EMP_ALL.RATE1AMT']);
  end
  else if Input['V_EMP_ALL.RATETYPE'] = 'D' then
  begin
    Keys.Add('EE_RATES', 'RATE_NUMBER', 1);
    Output.Add('EE', 'SALARY_AMOUNT', null);
    Output.Add('EE_RATES', 'RATE_AMOUNT', RoundAll(Input['V_EMP_ALL.RATE1AMT']/8, 2));
  end;
end;

procedure TADPImportDef.SUISDITax(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
begin
  if Value = 'X' then
  begin
    Output.Add( 'EE_STATES', 'EE_SDI_EXEMPT_EXCLUDE', GROUP_BOX_EXCLUDE);
    Output.Add( 'EE_STATES', 'ER_SDI_EXEMPT_EXCLUDE', GROUP_BOX_EXCLUDE);
    Output.Add( 'EE_STATES', 'EE_SUI_EXEMPT_EXCLUDE', GROUP_BOX_EXCLUDE);
    Output.Add( 'EE_STATES', 'ER_SUI_EXEMPT_EXCLUDE', GROUP_BOX_EXCLUDE);
  end
  else if Value = 'E' then
  begin
    Output.Add( 'EE_STATES', 'EE_SDI_EXEMPT_EXCLUDE', GROUP_BOX_EXEMPT);
    Output.Add( 'EE_STATES', 'ER_SDI_EXEMPT_EXCLUDE', GROUP_BOX_EXEMPT);
    Output.Add( 'EE_STATES', 'EE_SUI_EXEMPT_EXCLUDE', GROUP_BOX_EXEMPT);
    Output.Add( 'EE_STATES', 'ER_SUI_EXEMPT_EXCLUDE', GROUP_BOX_EXEMPT);
  end
  else
  begin
    Output.Add( 'EE_STATES', 'EE_SDI_EXEMPT_EXCLUDE', GROUP_BOX_INCLUDE);
    Output.Add( 'EE_STATES', 'ER_SDI_EXEMPT_EXCLUDE', GROUP_BOX_INCLUDE);
    Output.Add( 'EE_STATES', 'EE_SUI_EXEMPT_EXCLUDE', GROUP_BOX_INCLUDE);
    Output.Add( 'EE_STATES', 'ER_SUI_EXEMPT_EXCLUDE', GROUP_BOX_INCLUDE);
  end;
end;

procedure TADPImportDef.TARGET_ALL(const Input, Keys,
  Output: TFieldValues);
var
  ed_custom_number: Variant;
begin
  ed_custom_number := Matchers.Get(sEDBindingName).RightMatch( VarArrayOf( ['D', Input['V_TD_GOAL_ACCUM.GOALDEDCODE']] ) );
  if not VarIsNull( ed_custom_number ) then
  begin
    Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', ed_custom_number);
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_E_DS'));
    Output.Add( 'EE_SCHEDULED_E_DS', 'TARGET_ACTION', SCHED_ED_TARGET_REMOVE );
    Output.Add( 'EE_SCHEDULED_E_DS', 'TARGET_AMOUNT', Input['V_TD_GOAL_ACCUM.GOALLIMIT'] );
    Output.Add( 'EE_SCHEDULED_E_DS', 'BALANCE', Input['V_TD_GOAL_ACCUM.GOALTODATE'] );
  end;
end;

procedure TADPImportDef.YTD_INFO_ALL(const Input, Keys,
  Output: TFieldValues);
var
  cosuinbr, local, colocalsnbr: Variant;
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'), 'CO_NBR=' + VarToStr(FImpEngine.IDS('CO')['CO_NBR']) + ' and STATE=''' + VarToStr(Input['V_YTD_INFO.CURRWORKDSTATESEQ#']) + '''');
  if FImpEngine.IDS('CO_STATES').RecordCount > 0 then
  begin
    Keys.Add( 'EE_STATES', 'CO_STATES_NBR', FImpEngine.IDS('CO_STATES')['CO_STATES_NBR'] );
    Output.Add( 'PR_CHECK_STATES', 'STATE_TAX', Input['V_YTD_INFO.YTDWORKEDSTATETAX$'] );
  end
  else
    Logger.LogWarningFmt('State %s is not setup on a company level', [VarToStr(Input['V_YTD_INFO.CURRWORKDSTATESEQ#'])]);

  cosuinbr := MatchSUI( Input['V_YTD_INFO.SUISDITAXJURISCD'] );
  if not VarIsNull(cosuinbr) then
  begin
    Keys.Add( 'PR_CHECK_SUI', 'CO_SUI_NBR', cosuinbr );
    Output.Add( 'PR_CHECK_SUI', 'SUI_TAX', Input['V_YTD_INFO.YTDSUISDITAX$'] );
  end;

  local := Input['V_YTD_INFO.CURRWORKDLOCALSEQ#'];
  colocalsnbr := Matchers.Get(sLocalsBindingName).RightMatch( local );
  if not VarIsNull(colocalsnbr) then
  begin
    Keys.Add( 'EE_LOCALS', 'CO_LOCAL_TAX_NBR', colocalsnbr );
    Output.Add( 'PR_CHECK_LOCALS', 'LOCAL_TAX', Input['V_YTD_INFO.YTDWORKEDLOCALTAX$'] );
  end;
end;

procedure TADPImportDef.AI_PR(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds.FieldByName('CHECK_DATE_STATUS').AsString := DATE_STATUS_IGNORE; //as in rapid import
  ds.FieldByName('SCHEDULED').AsString := GROUP_BOX_NO; //as in rapid import
  ds.FieldByName('PAYROLL_TYPE').AsString := PAYROLL_TYPE_IMPORT;
  ds.FieldByName('STATUS').AsString := PAYROLL_STATUS_PENDING; // as default
  ds.FieldByName('STATUS_DATE').AsDateTime := SysTime; //Date or DateTime?
  ds.FieldByName('EXCLUDE_ACH').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_TAX_DEPOSITS').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_AGENCY').AsString := GROUP_BOX_YES;
  ds.FieldByName('MARK_LIABS_PAID_DEFAULT').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_BILLING').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_R_C_B_0R_N').AsString := GROUP_BOX_BOTH2;
  ds.FieldByName('INVOICE_PRINTED').AsString := INVOICE_PRINT_STATUS_calculated_do_not_print;

  ds.FieldByName('SCHEDULED_CALL_IN_DATE').Value := Date;
  ds.FieldByName('SCHEDULED_PROCESS_DATE').AsDateTime := Date;
  ds.FieldByName('SCHEDULED_DELIVERY_DATE').AsDateTime := Date;
  ds.FieldByName('INVOICE_PRINTED').AsString := INVOICE_PRINT_STATUS_calculated_do_not_print;
  ds.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
end;

procedure TADPImportDef.AI_PR_BATCH(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds.FieldByName('PERIOD_BEGIN_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
  ds.FieldByName('PERIOD_END_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
  ds.FieldByName('PAY_SALARY').AsString := GROUP_BOX_NO;
  ds.FieldByName('PAY_STANDARD_HOURS').AsString := GROUP_BOX_NO;
  ds.FieldByName('LOAD_DBDT_DEFAULTS').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
end;

procedure TADPImportDef.AI_PR_CHECK(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds.FieldByName('PAYMENT_SERIAL_NUMBER').AsInteger := ctx_PayrollCalculation.GetNextPaymentSerialNbr;
  ds.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString := GetCustomBankAccountNumber(FImpEngine);
  ds.FieldByName('EXCLUDE_DD').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_DD_EXCEPT_NET').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_TIME_OFF_ACCURAL').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
  ds.FieldByName('EXCLUDE_AUTO_DISTRIBUTION').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_SCH_E_D_FROM_AGCY_CHK').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_SCH_E_D_EXCEPT_PENSION').AsString := GROUP_BOX_NO;
  ds.FieldByName('PRORATE_SCHEDULED_E_DS').AsString := GROUP_BOX_NO;
  ds.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
  ds.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_CLEARED;
  ds.FieldByName('STATUS_CHANGE_DATE').AsDateTime := Now;
  ds.FieldByName('EXCLUDE_FROM_AGENCY').AsString := GROUP_BOX_YES;
  ds.FieldByName('ER_OASDI_TAXABLE_TIPS').Value := 0.0;
  ds.FieldByName('FEDERAL_SHORTFALL').Value := 0.0;
  ds.FieldByName('CHECK_TYPE_945').AsString := GROUP_BOX_NO;
  ds.FieldByName('CALCULATE_OVERRIDE_TAXES').AsString := GROUP_BOX_NO;

  ds.FieldByName('TAX_FREQUENCY').AsString := FImpEngine.IDS('EE').FieldByName('PAY_FREQUENCY').AsString; //as Roman said
end;

procedure TADPImportDef.AI_PR_CHECK_LINES(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds['LINE_TYPE'] := CHECK_LINE_TYPE_USER; 
  ds['CO_DIVISION_NBR'] := FImpEngine.IDS('EE')['CO_DIVISION_NBR'];
  ds['CO_BRANCH_NBR'] := FImpEngine.IDS('EE')['CO_BRANCH_NBR'];
  ds['CO_DEPARTMENT_NBR'] := FImpEngine.IDS('EE')['CO_DEPARTMENT_NBR'];
  ds['EE_STATES_NBR'] := FImpEngine.IDS('EE')['HOME_TAX_EE_STATES_NBR'];
  ds['EE_SUI_STATES_NBR'] := FImpEngine.IDS('EE')['HOME_TAX_EE_STATES_NBR'];
end;

procedure TADPImportDef.AI_PR_CHECK_LOCALS(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds['EXCLUDE_LOCAL'] := GROUP_BOX_NO;
end;

procedure TADPImportDef.AI_PR_CHECK_STATES(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds['TAX_AT_SUPPLEMENTAL_RATE'] := GROUP_BOX_NO;
  ds['EXCLUDE_STATE'] := GROUP_BOX_NO;
  ds['EXCLUDE_SDI'] := GROUP_BOX_NO;
  ds['EXCLUDE_SUI'] := GROUP_BOX_NO;
  ds['EXCLUDE_ADDITIONAL_STATE'] := GROUP_BOX_YES;
  ds['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_NONE;
end;

destructor TADPImportDef.Destroy;
begin
  inherited;
end;

procedure TADPImportDef.YTD_ED_HOURS(const Input, Keys,
  Output: TFieldValues);
var
  ed_custom_number: Variant;
begin
  ed_custom_number := Matchers.Get(sEDHoursBindingName).RightMatch( Input['V_YTD_ACCUMULATOR.SPECIALACCUMCODE'] );
  if not VarIsNull(ed_custom_number) then
  begin
    Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', ed_custom_number);
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_E_DS'));
    Output.Add( 'PR_CHECK_LINES', 'CL_E_DS_NBR', FImpEngine.IDS('CL_E_DS').Lookup('CUSTOM_E_D_CODE_NUMBER', ed_custom_number, 'CL_E_DS_NBR') );
    Output.Add( 'PR_CHECK_LINES', 'HOURS_OR_PIECES', Input['V_YTD_ACCUMULATOR.SPECIALACCUMAMT'] );
    Inc(CheckLineNbr);
    Keys.Add( 'PR_CHECK_LINES', 'E_D_DIFFERENTIAL_AMOUNT', CheckLineNbr );
  end;
end;

procedure TADPImportDef.Rate(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
begin
  if VarToStr(Value) <> '' then
  begin
    Keys.Add('EE_RATES', 'RATE_NUMBER', StrToInt(Copy(Field, 5, 1)));
    Output.Add('EE_RATES', 'RATE_AMOUNT', Value);
  end;
end;

procedure TADPImportDef.YTD_ED_AMOUNT(const Input, Keys,
  Output: TFieldValues);
var
  ed_custom_number: Variant;
begin
  ed_custom_number := Matchers.Get(sEDAmountsBindingName).RightMatch( Input['V_YTD_ACCUMULATOR.SPECIALACCUMCODE'] );
  if not VarIsNull(ed_custom_number) then
  begin
    Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', ed_custom_number);
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_E_DS'));
    Output.Add( 'PR_CHECK_LINES', 'CL_E_DS_NBR', FImpEngine.IDS('CL_E_DS').Lookup('CUSTOM_E_D_CODE_NUMBER', ed_custom_number, 'CL_E_DS_NBR') );
    Output.Add( 'PR_CHECK_LINES', 'AMOUNT', Input['V_YTD_ACCUMULATOR.SPECIALACCUMAMT'] );
    Inc(CheckLineNbr);
    Keys.Add( 'PR_CHECK_LINES', 'E_D_DIFFERENTIAL_AMOUNT', CheckLineNbr );
  end;
end;

function TADPImportDef.MatchSUI(Input: Variant): Variant;
var
  t: TStringList;
  v: Variant;
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_SUI'));
  t := TStringList.Create;
  try
    t.CommaText := SUIMap;
    if t.Values[Input] <> '' then
      v := StrToInt(t.Values[Input])
    else
    begin
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_SUI'));
      t.CommaText := SDIMap;
      v := t.Values[Input];
      v := FImpEngine.IDS('SY_SUI').Lookup('SUI_TAX_NAME', v + '-SUI', 'SY_SUI_NBR');
    end;
    Result := FImpEngine.IDS('CO_SUI').Lookup('CO_NBR;SY_SUI_NBR', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], v]), 'CO_SUI_NBR');
  finally
    t.Free;
  end;
end;

function TADPImportDef.MatchState(Input: Variant): Variant;
var
  t: TStringList;
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'));
  t := TStringList.Create;
  try
    t.CommaText := SDIMap;
    Result := FImpEngine.IDS('CO_STATES').Lookup('CO_NBR;STATE', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], t.Values[Input]]), 'CO_STATES_NBR');
  finally
    t.Free;
  end;
end;

procedure TADPImportDef.E1_STATE(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'));
  Output.Add( 'CL_PERSON', 'STATE', Value );
  Output.Add( 'CL_PERSON', 'RESIDENTIAL_STATE_NBR', FImpEngine.IDS('SY_STATES').Lookup('STATE', Value, 'SY_STATES_NBR') );
end;

procedure TADPImportDef.AI_EE(const ds: TevClientDataSet; const Keys: TFieldValues);
begin
  ds['NEW_HIRE_REPORT_SENT'] := NEW_HIRE_REPORT_COMP_BY_PREDECESSOR;
end;

{ TADPPRImport }

procedure TADPPRImport.DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings);
var
  EETerm: Boolean;
begin
  EETerm := aParams.Values[sEETermedParam] = 'Y';
  DataModule.GetTable('V_YTD_INFO').Filtered := EETerm;
  DataModule.GetTable('V_YTD_ACCUMULATOR').Filtered := EETerm;
  inherited;
  aImportDefinition.SetFixedKeys( 'CO.CUSTOM_COMPANY_NUMBER=' + DataModule.CustomCompanyNumber
              +#13#10+'PR.CHECK_DATE=' + aParams.Values[sParamCheckDate]
              +#13#10+'PR.RUN_NUMBER=1'
              +#13#10+'PR_BATCH.PERIOD_BEGIN_DATE=' + DateToStr(StartOfTheYear(StrToDate(aParams.Values[sParamCheckDate])))
              +#13#10+'PR_BATCH.PERIOD_END_DATE=' + aParams.Values[sParamCheckDate]
              +#13#10+'PR_BATCH.FREQUENCY=' + aParams.Values[sParamBatchFrequency]
              +#13#10+'PR_CHECK.CHECK_TYPE=' + CHECK_TYPE2_MANUAL);
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'V_YTD_INFO', 'Importing YTD taxes and gross wages' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'V_YTD_ACCUMULATOR', 'Importing YTD E/Ds' );
end;

end.
