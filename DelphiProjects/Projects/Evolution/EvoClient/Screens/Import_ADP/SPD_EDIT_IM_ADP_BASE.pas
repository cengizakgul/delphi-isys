// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_ADP_BASE;

interface

uses
  Windows, SPD_EDIT_IM_BASE, sConnectDatabaseByName, Dialogs,
  ImgList, Controls,  Classes, ActnList, DB, Wwdatsrc,
  sImportQueue, Forms, sTablespaceView, StdCtrls, Buttons,
  ExtCtrls, ComCtrls, sImportLoggerView, ISBasicClasses;

type
  TEDIT_IM_ADP_BASE = class(TEDIT_IM_BASE )
    ConnectDatabasePathFrame: TConnectDatabasePathFrame;
    procedure ConnectDatabasePathFrameBitBtn1Click(Sender: TObject);
  end;

implementation

{$R *.DFM}

{ TEDIT_IM_ADP_BASE }


procedure TEDIT_IM_ADP_BASE.ConnectDatabasePathFrameBitBtn1Click(
  Sender: TObject);
begin
  inherited;
  ConnectDatabasePathFrame.BitBtn1Click(Sender);
end;

end.
