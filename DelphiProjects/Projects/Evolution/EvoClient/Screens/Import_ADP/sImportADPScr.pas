// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportADPScr;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPackageEntry, ExtCtrls, Menus,  ImgList, EvMainboard, EvContext,
  ComCtrls, ToolWin, StdCtrls, Buttons, ActnList, ISBasicClasses, evCommonInterfaces;

type
  TADPFrm = class(TFramePackageTmpl, IevImportADPScreens)
  protected
    function  InitPackage: Boolean; override;
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
  public
  end;

implementation

{$R *.DFM}

var
  ADPFrm: TADPFrm;

function GetImportADPScreens: IevImportADPScreens;
begin
  if not Assigned(ADPFrm) then
    ADPFrm := TADPFrm.Create(nil);
  Result := ADPFrm;
end;


{ TPaychoiceFrm }

function TADPFrm.InitPackage: Boolean;
begin
  if Context.License.ADPImport then
    Result := inherited InitPackage
  else
    Result := False;
end;

function TADPFrm.PackageCaption: string;
begin
  Result := 'ADP';
end;

function TADPFrm.PackageSortPosition: string;
begin
  Result := '';
end;

procedure TADPFrm.UserPackageStructure;
begin
  AddStructure('\&Company\Imports\ADP Employees|381', 'TEDIT_IM_ADP_CO_AND_EE');
  AddStructure('\&Company\Imports\ADP Payrolls|382', 'TEDIT_IM_ADP_PR');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetImportADPScreens, IevImportADPScreens, 'Screens Import ADP');

finalization
  FreeAndNil(ADPFrm );

end.
