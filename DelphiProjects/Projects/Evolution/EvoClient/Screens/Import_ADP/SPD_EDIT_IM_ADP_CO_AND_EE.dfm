inherited EDIT_IM_ADP_CO_AND_EE: TEDIT_IM_ADP_CO_AND_EE
  inherited PageControl1: TevPageControl
    ActivePage = TabSheet1
    inherited TabSheet1: TTabSheet
      inherited evPanel1: TevPanel
        Width = 427
        Height = 238
        inherited evPanel5: TevPanel
          Width = 427
          Height = 133
          inherited EvBevel2: TEvBevel
            Height = 129
          end
          inherited pnlSetup: TevPanel
            Width = 296
            Height = 129
            inherited EvBevel1: TEvBevel
              Width = 296
            end
            inherited pnlSetupControls: TevPanel
              Width = 296
              Height = 90
              inline ParamInput: TImportParamFrame
                Left = 56
                Top = 32
                Width = 174
                Height = 35
                TabOrder = 0
              end
            end
          end
        end
        inherited pnlConnectionInputHolder: TevPanel
          Width = 427
          inherited ConnectDatabasePathFrame: TConnectDatabasePathFrame
            Width = 423
          end
        end
      end
    end
    inherited TabSheet5: TTabSheet
      inherited viewerFrame: TTableSpaceViewFrame
        inherited evPanel2: TevPanel
          inherited dgView: TevDBGrid
            Width = 640
            Height = 410
          end
        end
      end
    end
    object TabSheet3: TTabSheet [2]
      Caption = 'Locals Mapping'
      ImageIndex = 5
      inline BinderFrame1: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 187
          end
          inherited pnlTopLeft: TevPanel
            Height = 187
            inherited dgLeft: TevDBGrid
              Height = 162
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 370
            Height = 187
            inherited evPanel4: TevPanel
              Width = 370
            end
            inherited dgRight: TevDBGrid
              Width = 370
              Height = 162
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 640
            inherited evPanel5: TevPanel
              Width = 640
            end
            inherited dgBottom: TevDBGrid
              Width = 640
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 640
          end
        end
      end
    end
    object TabSheet6: TTabSheet [3]
      Caption = 'Agencies Mapping'
      ImageIndex = 7
      inline BinderFrame4: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 187
          end
          inherited pnlTopLeft: TevPanel
            Height = 187
            inherited dgLeft: TevDBGrid
              Height = 162
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 370
            Height = 187
            inherited evPanel4: TevPanel
              Width = 370
            end
            inherited dgRight: TevDBGrid
              Width = 370
              Height = 162
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 640
            inherited evPanel5: TevPanel
              Width = 640
            end
            inherited dgBottom: TevDBGrid
              Width = 640
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 640
          end
        end
      end
    end
    object TabSheet4: TTabSheet [4]
      Caption = 'Bank Types Mapping'
      ImageIndex = 6
      inline BinderFrame3: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 254
          end
          inherited pnlTopLeft: TevPanel
            Height = 254
            inherited dgLeft: TevDBGrid
              Height = 229
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 424
            Height = 254
            inherited evPanel4: TevPanel
              Width = 424
            end
            inherited dgRight: TevDBGrid
              Width = 424
              Height = 229
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 694
            inherited evPanel5: TevPanel
              Width = 694
            end
            inherited dgBottom: TevDBGrid
              Width = 694
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 694
          end
        end
      end
    end
    object tbshSchedEdsMapping: TTabSheet [5]
      Caption = 'E/Ds Mapping'
      ImageIndex = 4
      inline BinderFrame2: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 187
          end
          inherited pnlTopLeft: TevPanel
            Height = 187
            inherited dgLeft: TevDBGrid
              Height = 162
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 370
            Height = 187
            inherited evPanel4: TevPanel
              Width = 370
            end
            inherited dgRight: TevDBGrid
              Width = 370
              Height = 162
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 640
            inherited evPanel5: TevPanel
              Width = 640
            end
            inherited dgBottom: TevDBGrid
              Width = 640
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 640
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited LogFrame: TImportLoggerViewFrame
        inherited pnlLog: TevPanel
          inherited pgLogView: TevPageControl
            Width = 435
            Height = 136
          end
        end
      end
    end
  end
end
