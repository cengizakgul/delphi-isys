// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_ADP_CO_AND_EE;

interface

uses
  Windows, Messages, GenericImport, SPD_EDIT_IM_ADP_BASE, Dialogs, ImgList,
  Controls,  Classes, ActnList, DB, Wwdatsrc, sImportQueue,
  sTablespaceView, Forms, sConnectDatabaseByName, StdCtrls,
  Buttons, ExtCtrls, ComCtrls, gdyBinder, sImportVisualController, EvUtils,
  SDataStructure, gdyBinderVisual, sImportLoggerView, sImportParam,
  ISBasicClasses;

type
  TEDIT_IM_ADP_CO_AND_EE = class(TEDIT_IM_ADP_BASE)
    tbshSchedEdsMapping: TTabSheet;
    BinderFrame2: TBinderFrame;
    TabSheet3: TTabSheet;
    BinderFrame1: TBinderFrame;
    TabSheet4: TTabSheet;
    BinderFrame3: TBinderFrame;
    TabSheet6: TTabSheet;
    BinderFrame4: TBinderFrame;
    ParamInput: TImportParamFrame;
    procedure ConnectDatabasePathFrameBitBtn1Click(Sender: TObject);
  end;

implementation

uses SADPImportDef;

{$R *.dfm}

{ TEDIT_IM_ADP_CO_AND_EE }

procedure TEDIT_IM_ADP_CO_AND_EE.ConnectDatabasePathFrameBitBtn1Click(
  Sender: TObject);
var
  ivc: IImportVisualController;
  aImport: IImport;
begin
  aImport := CreateADPCO_AND_EEImport( LogFrame.LoggerKeeper.Logger );
  ivc := TImportVisualController.Create( LogFrame.LoggerKeeper, aImport, ParamInput, ViewerFrame );
  ivc.SetConnectionStringInputFrame( ConnectDatabasePathFrame );

  inherited;

  DM_COMPANY.CO_LOCAL_TAX.EnsureHasMetadata;
  ivc.BindingKeeper(sLocalsBindingName).SetTables( aImport.DataModule.GetTable('V_LOCAL_TAX_CODE'), DM_COMPANY.CO_LOCAL_TAX );
  ivc.BindingKeeper(sLocalsBindingName).SetVisualBinder( BinderFrame1 );

  DM_CLIENT.CL_E_DS.EnsureHasMetadata;
  ivc.BindingKeeper(sEDBindingName).SetTables( aImport.DataModule.GetTable('EDS'), DM_CLIENT.CL_E_DS );
  ivc.BindingKeeper(sEDBindingName).SetVisualBinder( BinderFrame2 );

  ivc.BindingKeeper(sBankTypeBindingName).SetTables( aImport.DataModule.GetTable('EDS_D'), aImport.DataModule.GetTable('cdsBankType') );
  ivc.BindingKeeper(sBankTypeBindingName).SetVisualBinder( BinderFrame3 );

  DM_CLIENT.CL_AGENCY.EnsureHasMetadata;
  ivc.BindingKeeper(sAgencyBindingName).SetTables( aImport.DataModule.GetTable('AGENCIES'), DM_CLIENT.CL_AGENCY );
  ivc.BindingKeeper(sAgencyBindingName).SetVisualBinder( BinderFrame4 );
  SetController( ivc );
end;

initialization
  RegisterClass(TEDIT_IM_ADP_CO_AND_EE);

end.
 