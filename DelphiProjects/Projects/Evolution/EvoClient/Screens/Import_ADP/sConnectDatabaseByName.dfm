object ConnectDatabasePathFrame: TConnectDatabasePathFrame
  Left = 0
  Top = 0
  Width = 519
  Height = 104
  TabOrder = 0
  DesignSize = (
    519
    104)
  object lblSource: TevLabel
    Left = 229
    Top = 46
    Width = 420
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    Caption = 
      '                                                                ' +
      '                                                                ' +
      '            '
  end
  object evLabel1: TevLabel
    Left = 8
    Top = 13
    Width = 46
    Height = 13
    Caption = 'Database'
    FocusControl = evEdit1
  end
  object evLabel2: TevLabel
    Left = 8
    Top = 45
    Width = 48
    Height = 13
    Caption = 'Username'
    FocusControl = evEdit2
  end
  object evLabel3: TevLabel
    Left = 8
    Top = 77
    Width = 46
    Height = 13
    Caption = 'Password'
    FocusControl = evEdit3
  end
  object BitBtn1: TevBitBtn
    Left = 228
    Top = 8
    Width = 174
    Height = 25
    Caption = 'Connect to database'
    Default = True
    TabOrder = 3
    OnClick = BitBtn1Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD8000000
      0008DDDDDDFFFFFFFFFDDDD8848888888880DDDDDD888888888FDD8CC4891919
      1980DDD88D8D8D8D8D8FD8CC648888888880DD88DD888888888FDC66C4777777
      7770D8DD8D8DDDDDDD8F8CCC647777777770D888DD8DDDDDDD8FC6C66C677777
      77708D8DD8D8DDDDDD8FCC6E6C677777777088DDD8D8DDDDDD8FC6C6ECC47777
      77788D8DD88D8888888DCECCCCEE664464488D8888DDDDDDDDDDCECEEECEECE6
      CEC88D8DDD8DD8DD8D8D8CECFECFECEECC8DD8D8DD8DD8DD88DDDCEECCCCCCFC
      6C8DD8DD888888D8D8DDD8CFCFEFEFCEC8DDDD8D8DDDDD8D8DDDDD8CCEFFFECC
      8DDDDDD88DDDDD88DDDDDDDD8CCCCC8DDDDDDDDDD88888DDDDDD}
    NumGlyphs = 2
  end
  object evEdit1: TevEdit
    Left = 88
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object evEdit2: TevEdit
    Left = 88
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object evEdit3: TevEdit
    Left = 88
    Top = 72
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 2
  end
end
