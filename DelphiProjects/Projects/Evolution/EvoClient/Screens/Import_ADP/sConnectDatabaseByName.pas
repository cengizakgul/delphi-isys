// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sConnectDatabaseByName;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  Buttons, sImportVisualController, ISBasicClasses, EvUIComponents;

type
  TConnectDatabasePathFrame= class(TFrame, IConnectionStringInputFrame )
    BitBtn1: TevBitBtn;
    lblSource: TevLabel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evEdit1: TevEdit;
    evEdit2: TevEdit;
    evEdit3: TevEdit;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FController: IImportVisualController;
    {IConnectionStringInputFrame}
    procedure ConnectionStringChanged;
    procedure SetController( aController: IImportVisualController );
    procedure SetDefaultConnectionString( aDefConnStr: string );
    
  public
    { Public declarations }
  end;

implementation

uses
  evutils;
{$R *.DFM}

procedure TConnectDatabasePathFrame.BitBtn1Click(Sender: TObject);
begin
  FController.SetConnectionString('Database=' + evEdit1.Text + #13#10 + 'User name=' + evEdit2.Text + #13#10 +
                                  'Password=' + evEdit3.Text);
  BitBtn1.Enabled := False;
  evEdit2.Enabled := False;
  evEdit3.Enabled := False;
end;

procedure TConnectDatabasePathFrame.ConnectionStringChanged;
var
  t: TStringList;
begin
  if assigned(FController) and (trim(FController.ConnectionString) <> '') then
  begin
    t := TStringList.Create;
    try
      t.Text := FController.ConnectionString;
      lblSource.Caption := 'Importing from ' + t.Values['Database'];
    finally
      t.Free;
    end;
  end
  else
    lblSource.Caption := 'No database specified';
end;

procedure TConnectDatabasePathFrame.SetController(
  aController: IImportVisualController);
begin
  FController := AController;
  ConnectionStringChanged;
end;

procedure TConnectDatabasePathFrame.SetDefaultConnectionString(
  aDefConnStr: string);
begin
//do nothing
end;

end.
