object ADPPrImportParamFrame: TADPPrImportParamFrame
  Left = 0
  Top = 0
  Width = 257
  Height = 72
  TabOrder = 0
  object evLabel4: TevLabel
    Left = 6
    Top = 4
    Width = 57
    Height = 13
    Caption = 'Check Date'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object evLabel3: TevLabel
    Left = 120
    Top = 3
    Width = 81
    Height = 13
    Caption = 'Batch Frequency'
  end
  object evdtCheckDate: TevDateTimePicker
    Left = 6
    Top = 20
    Width = 105
    Height = 21
    TabOrder = 0
  end
  object evcbFrequency: TevDBComboBox
    Left = 120
    Top = 20
    Width = 121
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DropDownCount = 8
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 1
    UnboundDataType = wwDefault
  end
  object cbTerm: TevCheckBox
    Left = 6
    Top = 48
    Width = 201
    Height = 17
    Caption = 'Import only active employees'
    TabOrder = 2
  end
end
