// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sADPPrImportParam;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb,  ComCtrls,
  sImportVisualcontroller, EvBasicUtils, ISBasicClasses, EvUIComponents;

type
  TADPPrImportParamFrame = class(TFrame, IParamInputFrame )
    evLabel4: TevLabel;
    evdtCheckDate: TevDateTimePicker;
    evcbFrequency: TevDBComboBox;
    evLabel3: TevLabel;
    cbTerm: TevCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Activate;
    procedure ParamsToControls( aParams: TStrings );
    procedure ControlsToParams( aParams: TStrings );
  end;

implementation

uses
  SFieldCodeValues, EvConsts, sADPImportDef;
{$R *.DFM}
procedure PopulateComboBox( cb: TevDBCombobox; C: string );
begin
  cb.MapList := True;
  cb.AllowClearKey := false;
  cb.Items.Text := C;
end;

{ TPrImportParamFrame }

procedure TADPPrImportParamFrame.Activate;
begin
  PopulateComboBox( evcbFrequency, PayFrequencyType_ComboChoices );
end;

procedure TADPPrImportParamFrame.ControlsToParams(aParams: TStrings);
begin
  aParams.Values[sParamBatchFrequency] := evcbFrequency.Value;
  aParams.Values[sParamCheckDate] := DateToStr(evdtCheckDate.Date);
  aParams.Values[sEETermedParam] := BoolToYN(cbTerm.Checked);
end;

procedure TADPPrImportParamFrame.ParamsToControls(aParams: TStrings);
begin
  cbTerm.Checked := aParams.Values[sEETermedParam] = 'Y';
  if trim(aParams.Values[sParamCheckDate]) = '' then
    evdtCheckDate.Date := Date
  else
    evdtCheckDate.Date := StrToDate(aParams.Values[sParamCheckDate]);
  if trim(aParams.Values[sParamBatchFrequency]) = '' then
    evcbFrequency.Value := FREQUENCY_TYPE_MONTHLY
  else
    evcbFrequency.Value := aParams.Values[sParamBatchFrequency];

end;

end.
