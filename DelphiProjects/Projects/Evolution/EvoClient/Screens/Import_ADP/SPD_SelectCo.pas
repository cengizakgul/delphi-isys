// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_SelectCo;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid,  DB, Wwdatsrc, EvUIComponents;

type
  TSelectCoDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    evDBGrid1: TevDBGrid;
    evDataSource1: TevDataSource;
    procedure evDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TSelectCoDlg.evDBGrid1DblClick(Sender: TObject);
begin
  OKBtn.Click;
end;

end.
