// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_IM_ADP;

interface

{
ADP5:
    Params.Strings = (
      'USER NAME=sysadmin'
      'password=master')
ADP3:
    Params.Strings = (
      'USER NAME=sys'
      'password=admin')

}

uses
  SysUtils, Classes, genericImport, DB, SDEngine,  Forms, EvUtils, SDataStructure,
  Variants, Controls,  SFieldCodeValues, DateUtils, kbmMemTable, isBasicUtils, EvContext, isVCLBugFix,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, evExceptions, EvUIComponents, EvClientDataSet;

type
  TADPVersion = (ADP3, ADP5);

  IADPDataModule = interface
['{81E1CFBB-A9EF-4150-BF6B-069FAB887785}']
    function ADPVersion: TADPVersion;
  end;

  TDM_IM_ADP = class(TDataModule, ITablespace, IExternalDataModule, IADPDataModule)
    SDDatabase: TSDDatabase;
    CO: TSDQuery;
    EDS: TSDQuery;
    T_CO_DEPT: TSDQuery;
    V_EMP_ALL: TSDQuery;
    V_LOCAL_TAX_CODE: TSDQuery;
    V_BANKING_INFO: TSDQuery;
    V_DEDUCTIONS: TSDQuery;
    cdsBankType: TevClientDataSet;
    cdsBankTypeCode: TStringField;
    cdsBankTypeName: TStringField;
    EDS_D: TSDQuery;
    V_TD_GOAL_ACCUM: TSDQuery;
    V_LIENS: TSDQuery;
    AGENCIES: TSDQuery;
    V_YTD_INFO: TSDQuery;
    SUI_SDI: TSDQuery;
    CO_ACC: TSDQuery;
    V_YTD_ACCUMULATOR: TSDQuery;
    TestADP: TSDQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure V_EMP_ALLAfterOpen(DataSet: TDataSet);
    procedure V_EMP_ALLFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure DataModuleCreate(Sender: TObject);
  private
    IDList: string;
    FConnectionString: string;
    FCustomCompanyNumber: string;
    FLogger: IImportLogger;
    FV_YTD_INFO_OrigSQL: string;
    FADPVersion: TADPVersion;
  protected
    {IExternalDataModule}
    procedure OpenConnection( aConnectionString: string );
    procedure CloseConnection;
    function Connected: boolean;
    function ConnectionString: string;
    function CustomCompanyNumber: string;
    procedure SetLogger( aLogger: IImportLogger );

    {IADPDataModule}
    function ADPVersion: TADPVersion;
  public
    {ITableSpace}
    function GetTable( const name: string ): TDataSet;
    procedure GetAvailableTables( const list: TStrings );
  end;

implementation

uses SPD_SelectCo;

{$R *.dfm}

{ TDM_IM_ADP }

procedure TDM_IM_ADP.CloseConnection;
var
  i: integer;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TSDQuery then
    try
      if TSDQuery(Components[i]).Active then
      begin
        TSDQuery(Components[i]).Close;
      end
    except
      on e: Exception do FLogger.LogDebug( 'Error occured while closing connection. Ignored.', e.Message );
      //dont raise
    end;
  SDDatabase.Close;
  FConnectionString := '';
end;

function TDM_IM_ADP.Connected: boolean;
begin
  Result := SDDatabase.Connected;
end;

function TDM_IM_ADP.ConnectionString: string;
begin
  Result := FConnectionString;
end;

function TDM_IM_ADP.CustomCompanyNumber: string;
begin
  Result := FCustomCompanyNumber;
end;

procedure TDM_IM_ADP.GetAvailableTables(const list: TStrings);
var
  i: integer;
begin
  for i := 0 to ComponentCount-1 do
    if Components[i] is TSDQuery then
      list.Add(Components[i].Name);
end;

function TDM_IM_ADP.GetTable(const name: string): TDataSet;
var
  c: TComponent;
  tn: string;
begin
  tn := name;
  c := FindComponent( tn );
  if not assigned(c) {or not (c is TSDQuery)} then
    raise EevException.CreateFmt( 'Unknown table requested: <%s>',[name]);
  Result := c as TDataSet;
end;

procedure OpenClientForCompany( custom_company_number: string );
begin
  DM_TEMPORARY.TMP_CO.DataRequired('' );
  if DM_TEMPORARY.TMP_CO.Locate('CUSTOM_COMPANY_NUMBER', custom_company_number, []) then
    ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO['CL_NBR'])
  else
    raise EevException.CreateFmt( 'Company <%s> doesn''t exist in Evolution database', [custom_company_number] );
  DM_COMPANY.CO.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_CLIENT.CL_E_DS.DataRequired('' );
  DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_CLIENT.CL_AGENCY.DataRequired('' );
  DM_COMPANY.CO_SUI.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
end;

procedure TDM_IM_ADP.OpenConnection(aConnectionString: string);
const
  sText = 'Opening ADP database';
var
  t: TStringList;
  s: string;
  i: integer;
  

  function GetCustomCompanyNumber: string;
  begin
    CO.Open;
    CO.Last;
    CO.First;
    if CO.RecordCount > 0 then
      if CO.RecordCount = 1 then
        Result := VarToStr(CO['CompanyCode'])
      else
        with TSelectCoDlg.Create(nil) do
        try
          evDataSource1.DataSet := CO;
          if ShowModal = mrOK then
            Result := VarToStr(CO['CompanyCode'])
          else
            AbortEx;
        finally
          Free;
        end
    else
      raise EevException.Create('There are no employees in a database. Cannot determinate custom company number.');
  end;
begin
  t := TStringList.Create;
  try
    t.Text := aConnectionString;
    t.Delete(0);
    s := T.Text;
  finally
    t.Free;
  end;
  if not Connected or (SDDatabase.Params.Text <> s) then
  begin
    FLogger.LogEntry( sText );
    try
      try
        ctx_StartWait( sText, ComponentCount-1 );
        try
          CloseConnection;
          try
            cdsBankType.Close;
//            FLogger.LogContextItem( sciExtDatabase, 'PAY4WIN' );
            SDDatabase.Params.Text := s;
            SDDatabase.Open;
            FConnectionString := aConnectionString;
            FCustomCompanyNumber := GetCustomCompanyNumber;
            FADPVersion := ADP3;
            try
              TestADP.Open;
              if TestADP.RecordCount = 0 then
                FADPVersion := ADP5;
              TestADP.Close;
            except
              on e: Exception do
                FLogger.LogWarningFmt( 'Got error while detecting ADP version (%s)',[e.Message] );
              //do nothing;
            end;

            case FADPVersion of
              ADP5:
              begin
{                V_YTD_INFO.SQL.Clear;
                V_YTD_INFO.SQL.Append( 'select V_YTD_INFO.*, V_EMP_ALL.CURRWORKDSTATESEQ#,' );
                V_YTD_INFO.SQL.Append( 'V_EMP_ALL.SUISDITAXJURISCD,' );
                V_YTD_INFO.SQL.Append( 'V_EMP_ALL.CURRWORKDLOCALSEQ#' );
                V_YTD_INFO.SQL.Append( 'from reports.V_YTD_INFO, reports.V_EMP_ALL' );
                V_YTD_INFO.SQL.Append( 'where V_YTD_INFO.CompanyCode=:Co and' );
                V_YTD_INFO.SQL.Append( 'V_YTD_INFO.CompanyCode=V_EMP_ALL.CompanyCode and' );
                V_YTD_INFO.SQL.Append( 'V_YTD_INFO.File#=V_EMP_ALL.File#' );
}
                FLogger.LogEvent('ADP 5 database structure detected.');
                V_YTD_INFO.SQL.Text := StringReplace( FV_YTD_INFO_OrigSQL, 'V_EMPLOYEE', 'V_EMP_ALL', [rfReplaceAll,rfIgnoreCase]);
              end;
              ADP3:
              begin
                FLogger.LogEvent('ADP 3 database structure detected.');
                V_YTD_INFO.SQL.Text := FV_YTD_INFO_OrigSQL;
              end
              else
                Assert(false);
            end;

            for i := 0 to ComponentCount - 1 do
              if Components[i] is TSDQuery then
              try
                with TSDQuery(Components[i]) do
                begin
                  if Assigned(Params.FindParam('Co')) then
                    ParamByName('Co').AsString := FCustomCompanyNumber;
                  Open;
                  Last;
                  First;
                end;
                ctx_UpdateWait( sText, i );
              except
                FLogger.StopExceptionAndWarnFmt( 'Failed to read data', [] );
              end;
            OpenClientForCompany( FCustomCompanyNumber );
            cdsBankType.CreateDataSet;
            t := TStringList.Create;
            try
              t.Text := ReceivingAcctType_ComboChoices;
              for i := 0 to Pred(t.Count) do
              begin
                s := t[i];
                cdsBankType.Append;
                cdsBankType['Name'] := GetNextStrValue(s, #9);
                cdsBankType['Code'] := s;
                cdsBankType.Post;
              end;
            finally
              t.Free;
            end;
          except
            CloseConnection;
            raise;
          end;
        finally
          ctx_EndWait;
        end;
      except
        FLogger.PassthroughExceptionAndWarnFmt( sImportFailedToOpenConnection, [] );
      end;
    finally
      FLogger.LogExit;
    end;
  end;
end;

procedure TDM_IM_ADP.DataModuleDestroy(Sender: TObject);
begin
  CloseConnection;
end;

procedure TDM_IM_ADP.SetLogger(aLogger: IImportLogger);
begin
  FLogger := aLogger;
end;

procedure TDM_IM_ADP.V_EMP_ALLAfterOpen(DataSet: TDataSet);
var
  dt: TDateTime;
  by: TDateTime;
begin
  by := StartOfTheYear(Date);
  IDList := ',';
  while not DataSet.Eof do
  begin
    dt := DataSet.FieldByName('TERMINATIONDATE').AsDateTime;
    if (DataSet.FieldByName('STATUS').AsString <> 'A') and (dt <> 0) and (dt < by) then
      IDList := IDList + DataSet.FieldByName('FILE#').AsString + ',';
    DataSet.Next;
  end;
  DataSet.First;
end;

procedure TDM_IM_ADP.V_EMP_ALLFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := Pos(',' + DataSet.FieldByName('FILE#').AsString + ',', IDList) = 0;
end;

procedure TDM_IM_ADP.DataModuleCreate(Sender: TObject);
begin
  FV_YTD_INFO_OrigSQL := V_YTD_INFO.SQL.Text;
end;

function TDM_IM_ADP.ADPVersion: TADPVersion;
begin
  Result := FADPVersion;
end;

end.
