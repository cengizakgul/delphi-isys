// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_ADP_PR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_IM_ADP_BASE, ImgList,  ActnList, DB, SDataStructure,
  Wwdatsrc, sImportQueue, sTablespaceView, sImportVisualController,
  sImportDatabaseFileNameInput, StdCtrls, Buttons, ExtCtrls, ComCtrls, EvUtils,
  sConnectDatabaseByName, sADPPrImportParam, genericImport, gdyBinder, SADPImportDef,
  gdyBinderVisual, sImportLoggerView, ISBasicClasses;

type
  TEDIT_IM_ADP_PR = class(TEDIT_IM_ADP_BASE)
    ParamInput: TADPPrImportParamFrame;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    BinderFrame1: TBinderFrame;
    BinderFrame2: TBinderFrame;
    TabSheet7: TTabSheet;
    BinderFrame4: TBinderFrame;
    procedure ConnectDatabasePathFrameBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

{ TEDIT_IM_ADP_PR }

procedure TEDIT_IM_ADP_PR.ConnectDatabasePathFrameBitBtn1Click(
  Sender: TObject);
var
  aImport: IImport;
  ivc: IImportVisualController;
begin
  aImport := CreateADPPRImport( LogFrame.LoggerKeeper.Logger );
  ParamInput.Activate;
  ivc := TImportVisualController.Create( LogFrame.LoggerKeeper, aImport, ParamInput, ViewerFrame );
  ivc.SetConnectionStringInputFrame( ConnectDatabasePathFrame );

  inherited;

  DM_COMPANY.CO_LOCAL_TAX.EnsureHasMetadata;
  ivc.BindingKeeper(sLocalsBindingName).SetTables( aImport.DataModule.GetTable('V_LOCAL_TAX_CODE'), DM_COMPANY.CO_LOCAL_TAX );
  ivc.BindingKeeper(sLocalsBindingName).SetVisualBinder( BinderFrame1 );

  DM_CLIENT.CL_E_DS.EnsureHasMetadata;
  ivc.BindingKeeper(sEDHoursBindingName).SetTables( aImport.DataModule.GetTable('CO_ACC'), DM_CLIENT.CL_E_DS );
  ivc.BindingKeeper(sEDHoursBindingName).SetVisualBinder( BinderFrame2 );
  ivc.BindingKeeper(sEDAmountsBindingName).SetTables( aImport.DataModule.GetTable('CO_ACC'), DM_CLIENT.CL_E_DS );
  ivc.BindingKeeper(sEDAmountsBindingName).SetVisualBinder( BinderFrame4 );

  SetController( ivc );
end;

initialization
  RegisterClass(TEDIT_IM_ADP_PR);
end.
