// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_PR_CHECK_REPRINT;

interface

uses
  SDataStructure,  SPD_EDIT_PR_BASE, Db, Wwdatsrc, StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Controls, Classes, SPackageEntry, SPD_Payroll_Expert, EvUtils,
  Dialogs, SLogCheckReprint, EvTypes, EvConsts, SFieldCodeValues, DBActns, ActnList,
  SysUtils,  kbmMemTable, ISKbmMemDataSet, EvContext, EvCommonInterfaces, EvMainboard,
  ISBasicClasses, SDDClasses, EvDataAccessComponents,
  ISDataAccessComponents, SDataDictclient, SDataDicttemp, EvExceptions, EvDataset,
  SPD_DM_HISTORICAL_TAXES, EvUIUtils, EvUIComponents, EvClientDataSet,
  ImgList, LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms;

type
  TEDIT_PR_CHECK_REPRINT = class(TEDIT_PR_BASE)
    tsChecks: TTabSheet;
    tsMiscChecks: TTabSheet;
    wwdbgPR_CHECKS: TevDBGrid;
    wwdbgPR_MISC_CHECKS: TevDBGrid;
    tsReprintHistory: TTabSheet;
    wwdbgPR_REPRINT_HISTORY: TevDBGrid;
    wwdsDetail2: TevDataSource;
    wwdbgHistoryDetail: TevDBGrid;
    Splitter2: TevSplitter;
    wwdsSubMaster1: TevDataSource;
    wwdsSubMaster2: TevDataSource;
    bbtnPrintEverything: TevBitBtn;
    cbCheckForm: TevComboBox;
    evLabel2: TevLabel;
    tempChecks: TevClientDataSet;
    evLabel3: TevLabel;
    cbMiscCheckForm: TevComboBox;
    cbDestination: TevComboBox;
    evLabel4: TevLabel;
    LabelReprintTOBalances: TevLabel;
    cbReprintTOBalances: TevComboBox;
    cdPR_REPRINT_HISTORY_DETAIL: TevClientDataSet;
    cdPR_REPRINT_HISTORY_DETAILBeginPayment_Serial_Number: TIntegerField;
    cdPR_REPRINT_HISTORY_DETAILEndPayment_Serial_Number: TIntegerField;
    cdPR_REPRINT_HISTORY_DETAILMiscellaneous_Check: TStringField;
    cdPR_REPRINT_HISTORY_DETAILBegin_Check_Number: TIntegerField;
    cdPR_REPRINT_HISTORY_DETAILEnd_Check_Number: TIntegerField;
    cbDontUseVMR: TCheckBox;
    chbDontPrintBankInfo: TCheckBox;
    chbHideBackground: TCheckBox;
    sbPayrollChecks: TScrollBox;
    pnlFPPayrollChecks: TevPanel;
    fpPayrollChecks: TisUIFashionPanel;
    sbMiscChecks: TScrollBox;
    pnlFPMiscChecks: TevPanel;
    fpMiscChecks: TisUIFashionPanel;
    sbCheckReprintBrows: TScrollBox;
    evSplitter1: TevSplitter;
    pnlCheckReprintLeft: TevPanel;
    pnlCheckReprintRight: TevPanel;
    fpCheckReprintLeft: TisUIFashionPanel;
    pnlFPLeft: TevPanel;
    fpCheckReprintRight: TisUIFashionPanel;
    pnlFPRight: TevPanel;
    sbReprintDetail: TScrollBox;
    pnlReprintDetailsBorder: TevPanel;
    pnlReprintDetailsLeft: TevPanel;
    fpReprints: TisUIFashionPanel;
    pnlReprintDetailsRight: TevPanel;
    fpReprintHistoryChecks: TisUIFashionPanel;
    edHiddenReprintField: TEdit;
    procedure PageControl1Change(Sender: TObject);
    procedure PRNextExecute(Sender: TObject);
    procedure bbtnPrintEverythingClick(Sender: TObject);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure cbDestinationChange(Sender: TObject);
    procedure wwdbgPR_REPRINT_HISTORYRowChanged(Sender: TObject);
  private
    CheckFormCodeValues: TStrings;
    MiscCheckFormCodeValues: TStrings;
    FReprintBalCodeValues: String;
    FDestination: TrwReportDestination;
    FPrReprintHistoryNBR: Integer;
    function CheckPreparedChecks(CheckIfManual: Boolean; slNormalChecks, slAgencyChecks, slTaxChecks,
      slBillingChecks, slMiscChecks: TStringList; var aReason: String): Boolean;
    procedure UpdateDestChoices;
    procedure List_Reprint_History_Detail;
    procedure AppendCheckForms;
    function GetCheckForm(aRegular: boolean = True): string;
    procedure AssignValues(var aReprintReason : string; var aPrReprintHistoryNBR:Integer);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure ReopenPRChecks; override;
    procedure AfterDataSetsReopen; override;
    procedure RecalculateCheck(const PrCheckNbr: Integer);
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure RePrintChecks;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

uses SReportSettings, EvStreamUtils, SReprintParameters, SDM_PR_CHECK, isBaseClasses,
     SPD_EDIT_Open_BASE, ISBasicUtils;

{$R *.DFM}

procedure TEDIT_PR_CHECK_REPRINT.UpdateDestChoices;
var
  i: Integer;
begin
  i := cbDestination.ItemIndex;
  if CheckVmrLicenseActive and CheckVmrSetupActive then
    cbDestination.Items.Text := 'Print'#13'Preview'#13'Send To Mailbox'
  else
    cbDestination.Items.Text := 'Print'#13'Preview';
  cbDestination.ItemIndex := i;
  cbDestinationChange(nil);
end;

procedure TEDIT_PR_CHECK_REPRINT.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  case Kind of
    NavInsert:
      begin
        case PageControl1.ActivePage.Tag of
          2, 3:
          begin
            try
              RePrintChecks;
            except
              ctx_DataAccess.CancelDataSets([DM_PAYROLL.PR_CHECK,
                              DM_PAYROLL.PR_CHECK_STATES,
                              DM_PAYROLL.PR_CHECK_SUI,
                              DM_PAYROLL.PR_CHECK_LOCALS]);
              raise;
            end;
          end;
        end;
        Handled := True;
      end;
  end;
end;

procedure TEDIT_PR_CHECK_REPRINT.RecalculateCheck(const PrCheckNbr: Integer);
var
  b: Boolean;
  SQLQuery: IevQuery;
  Warnings: String;
begin
  DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR='+IntToStr(PrCheckNbr));

  SQLQuery := TevQuery.Create('GenericSelect2CurrentWithCondition');
  SQLQuery.Macros.AddValue('COLUMNS', 'count(*)');
  SQLQuery.Macros.AddValue('TABLE1', 'PR_REPRINT_HISTORY');
  SQLQuery.Macros.AddValue('TABLE2', 'PR_REPRINT_HISTORY_DETAIL');
  SQLQuery.Macros.AddValue('JOINFIELD', 'PR_REPRINT_HISTORY');
  SQLQuery.Macros.AddValue('CONDITION', 't2.MISCELLANEOUS_CHECK=''N'' and t1.PR_NBR=' +
        DM_PAYROLL.PR_CHECK.PR_NBR.AsString + ' and t2.BEGIN_CHECK_NUMBER<=' +
        IntToStr(PrCheckNbr) + ' and t2.END_CHECK_NUMBER>=' +IntToStr(PrCheckNbr));
  SQLQuery.Execute;
  if SQLQuery.Result.Fields[0].Value > 0 then
    Exit;

  DM_PAYROLL.PR.DataRequired('PR_NBR='+DM_PAYROLL.PR_CHECK.PR_NBR.AsString);
  DM_PAYROLL.PR_BATCH.DataRequired('PR_BATCH_NBR='+DM_PAYROLL.PR_CHECK.PR_BATCH_NBR.AsString);
  DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_NBR='+IntToStr(PrCheckNbr));

  if DM_PAYROLL.PR_CHECK_LINES.RecordCount = 0 then
    Exit;

  DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR='+DM_PAYROLL.PR.CO_NBR.AsString);  
  DM_PAYROLL.PR_CHECK_LINE_LOCALS.DataRequired('');
  DM_PAYROLL.PR_CHECK_SUI.DataRequired('PR_CHECK_NBR='+IntToStr(PrCheckNbr));
  DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_CHECK_NBR='+IntToStr(PrCheckNbr));
  DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR='+IntToStr(PrCheckNbr));
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired('EE_NBR='+DM_PAYROLL.PR_CHECK.EE_NBR.AsString);
  DM_EMPLOYEE.EE.DataRequired('EE_NBR='+DM_PAYROLL.PR_CHECK.EE_NBR.AsString);
  DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR='+DM_PAYROLL.PR_CHECK.EE_NBR.AsString);
  DM_EMPLOYEE.EE_LOCALS.DataRequired('EE_NBR='+DM_PAYROLL.PR_CHECK.EE_NBR.AsString);
  DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.DataRequired('EE_NBR='+DM_PAYROLL.PR_CHECK.EE_NBR.AsString);
  DM_EMPLOYEE.EE_PENSION_FUND_SPLITS.DataRequired('EE_NBR='+DM_PAYROLL.PR_CHECK.EE_NBR.AsString);

  DM_HISTORICAL_TAXES.LoadDataAsOfDate(DM_PAYROLL.PR.CHECK_DATE.AsDateTime + 1 - 1 / 1440);

  b := ctx_DataAccess.RecalcLineBeforePost;
  try
    try
      ctx_DataAccess.CheckMaxAmountAndHours := False;
      ctx_DataAccess.RecalcLineBeforePost := True;
      ctx_DataAccess.PayrollIsProcessing := True;
      try
        ctx_PayrollCalculation.GrossToNet(Warnings, PrCheckNbr, False, False, False, False);
        ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK_LINE_LOCALS]);
      finally
        ctx_DataAccess.PayrollIsProcessing := False;
      end;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK,
                    DM_PAYROLL.PR_CHECK_LINES,
                    DM_PAYROLL.PR_CHECK_STATES,
                    DM_PAYROLL.PR_CHECK_SUI,
                    DM_PAYROLL.PR_CHECK_LOCALS,
                    DM_PAYROLL.PR_CHECK_LINE_LOCALS]);
    except
      on E: Exception do
      begin
        ctx_DataAccess.CancelDataSets([DM_PAYROLL.PR_CHECK,
                        DM_PAYROLL.PR_CHECK_LINES,
                        DM_PAYROLL.PR_CHECK_STATES,
                        DM_PAYROLL.PR_CHECK_SUI,
                        DM_PAYROLL.PR_CHECK_LOCALS,
                        DM_PAYROLL.PR_CHECK_LINE_LOCALS]);
        raise;
      end;
    end;
  finally
    ctx_DataAccess.RecalcLineBeforePost := b;
  end;
end;

procedure TEDIT_PR_CHECK_REPRINT.RePrintChecks;
var
  slNormalChecks, slAgencyChecks, slTaxChecks, slBillingChecks, slMiscChecks: TStringList;
  I: integer;
  CanPrint, CheckIfManual, CurrentToa: Boolean;
  qTask: IevReprintPayrollTask;
  d: TDateTime;
  Reason: String;
begin
   // Exit if nothing selected
  if (wwdbgPR_CHECKS.SelectedList.Count < 1) and (wwdbgPR_MISC_CHECKS.SelectedList.Count < 1) then
    Exit;
  CanPrint := True;
  CheckIfManual := False;
  if not (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED]) then
    CanPrint := False;
  if not CanPrint and (ctx_AccountRights.Functions.GetState('USER_CAN_PRINT_UNPROCESSED_CHECKS') = stEnabled) then
  begin
    CanPrint := True;
    CheckIfManual := True;
  end;
  if not CanPrint then
    raise ECheckPrintingException.CreateHelp('You can not print checks for unprocessed payrolls!', IDH_CheckPrintingException);
  slNormalChecks := TStringList.Create;
  slAgencyChecks := TStringList.Create;
  slTaxChecks := TStringList.Create;
  slBillingChecks := TStringList.Create;
  slMiscChecks := TStringList.Create;
  try
    // Normal Checks
    TevClientDataSet(wwdbgPR_CHECKS.DataSource.DataSet).DisableControls;
    TevClientDataSet(wwdbgPR_CHECKS.DataSource.DataSet).LookupsEnabled := False;
    for i := 0 to wwdbgPR_CHECKS.SelectedList.Count - 1 do
    begin
      TevClientDataSet(wwdbgPR_CHECKS.DataSource.DataSet).GotoBookMark(wwdbgPR_CHECKS.SelectedList.Items[i]);
      if not CheckIfManual or (CheckIfManual and
        (TevClientDataSet(wwdbgPR_CHECKS.DataSource.DataSet).FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL)) then
      begin
        slNormalChecks.Add(TevClientDataSet(wwdbgPR_CHECKS.DataSource.DataSet).FieldByName('PR_CHECK_NBR').AsString);
        if (DM_PAYROLL.PR.STATUS.AsString <> PAYROLL_STATUS_PROCESSED)
        and (DM_PAYROLL.PR.STATUS.AsString <> PAYROLL_STATUS_VOIDED) then
          RecalculateCheck(TevClientDataSet(wwdbgPR_CHECKS.DataSource.DataSet).FieldByName('PR_CHECK_NBR').AsInteger);
      end;
    end;
    TevClientDataSet(wwdbgPR_CHECKS.DataSource.DataSet).LookupsEnabled := True;
    TevClientDataSet(wwdbgPR_CHECKS.DataSource.DataSet).EnableControls;

    DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR=' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);

    d := 0;
    // Misc Checks
    if not CheckIfManual then
      with wwdbgPR_MISC_CHECKS, TevClientDataSet(wwdbgPR_MISC_CHECKS.DataSource.DataSet) do
      begin
//        DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR=' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
        DisableControls;
        LookupsEnabled := False;
        for i := 0 to SelectedList.Count - 1 do
        begin
          GotoBookMark(SelectedList.Items[i]);
          if FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_AGENCY, MISC_CHECK_TYPE_AGENCY_VOID] then
            slAgencyChecks.Add(FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
          if FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_TAX, MISC_CHECK_TYPE_TAX_VOID] then
            slTaxChecks.Add(FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
          if FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_BILLING, MISC_CHECK_TYPE_BILLING_VOID] then
            slBillingChecks.Add(FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
          slMiscChecks.Add(FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
        end;
        LookupsEnabled := True;
        EnableControls;
      end
    else if EvMessage('Would you like to print checks with today''s date?', mtConfirmation, [mbYes,mbNo], mbNo) = mrYes then
      d := Date;
    if CheckPreparedChecks(CheckIfManual, slNormalChecks, slAgencyChecks, slTaxChecks, slBillingChecks, slMiscChecks, Reason) then
    begin
      CurrentTOA := FReprintBalCodeValues[cbReprintTOBalances.ItemIndex + 1] = GROUP_BOX_CURRENT;
      qTask := mb_TaskQueue.CreateTask(QUEUE_TASK_REPRINT_PAYROLL, True) as IevReprintPayrollTask;
      try
        qTask.Caption := 'Reprinting ' + Trim(DM_TEMPORARY.TMP_CO.FieldByName('Name').AsString) + ' ' +
          DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString + '-' + DM_PAYROLL.PR.FieldByName('RUN_NUMBER').AsString;
        qTask.Reason := Reason;  
        qTask.ClientID := ctx_DataAccess.ClientID;
        qTask.PrNbr := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
        qTask.PrintReports := False;
        qTask.PrintInvoices := False;
        qTask.CurrentTOA := CurrentToa;
        qTask.PayrollChecks := slNormalChecks.CommaText;
        qTask.AgencyChecks := slAgencyChecks.CommaText;
        qTask.TaxChecks := slTaxChecks.CommaText;
        qTask.BillingChecks := slBillingChecks.CommaText;
        qTask.CheckForm := CheckFormCodeValues[cbCheckForm.ItemIndex];
        qTask.MiscCheckForm := MiscCheckFormCodeValues[cbMiscCheckForm.ItemIndex];
        qTask.Destination := FDestination;
        qTask.CheckDate := d;
        qTask.SkipCheckStubCheck := True;
        qTask.DontPrintBankInfo := chbDontPrintBankInfo.Checked;
        qTask.HideBackground := chbHideBackground.Checked;
        qTask.DontUseVMRSettings := cbDontUseVMR.Visible and cbDontUseVMR.Checked;
        if FDestination = rdtVMR then
          qTask.PrReprintHistoryNBR := FPrReprintHistoryNBR;
        ctx_EvolutionGUI.AddTaskQueue(qTask);
      finally
        qTask := nil;
      end;
    end;
  finally
    slNormalChecks.Free;
    slAgencyChecks.Free;
    slTaxChecks.Free;
    slBillingChecks.Free;
    slMiscChecks.Free;
  end;
end;

function TEDIT_PR_CHECK_REPRINT.GetDataSetConditions(
  sName: string): string;
begin
  if (sName = 'PR_MISCELLANEOUS_CHECKS') or
     (sName = 'PR_REPRINT_HISTORY') or (sName = 'PR_REPRINT_HISTORY_DETAIL') then
    Result := ''
  else if (sName = 'CL_PERSON') or (sName = 'CO_BRANCH') or (sName = 'CO_DEPARTMENT') or (sName = 'CO_TEAM') then //RE #27566
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_PR_CHECK_REPRINT.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_EMPLOYEE.EE, aDS);
  AddDS(DM_PAYROLL.PR, aDS);
  AddDS(DM_PAYROLL.PR_MISCELLANEOUS_CHECKS, aDS);
  AddDS(DM_PAYROLL.PR_REPRINT_HISTORY, aDS);
  //RE #27566
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);
  AddDS(DM_CLIENT.CL_PERSON, aDS);
  //end of RE #27566
  inherited;
end;

function TEDIT_PR_CHECK_REPRINT.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR_REPRINT_HISTORY_DETAIL;
end;

procedure TEDIT_PR_CHECK_REPRINT.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_PAYROLL.PR_REPRINT_HISTORY, EditDataSets);

  //RE #27566
  AddDS(DM_COMPANY.CO_DIVISION, SupportDataSets);
  AddDS(DM_COMPANY.CO_BRANCH, SupportDataSets);
  AddDS(DM_COMPANY.CO_DEPARTMENT, SupportDataSets);
  AddDS(DM_COMPANY.CO_TEAM, SupportDataSets);
  AddDS(DM_CLIENT.CL_PERSON, SupportDataSets);
  AddDS(DM_CLIENT.EE, SupportDataSets);

  //end of RE #27566
end;

procedure TEDIT_PR_CHECK_REPRINT.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
  Value := Value and (Kind <> NavDelete);
end;

procedure TEDIT_PR_CHECK_REPRINT.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage.Tag = 2 then
    ReopenPRChecks;

  if PageControl1.ActivePage = tsReprintHistory then
      List_Reprint_History_Detail;
end;

procedure TEDIT_PR_CHECK_REPRINT.List_Reprint_History_Detail;
begin
  cdPR_REPRINT_HISTORY_DETAIL.DisableControls;
  try
   if cdPR_REPRINT_HISTORY_DETAIL.Active then cdPR_REPRINT_HISTORY_DETAIL.Close;
   if DM_CLIENT.PR_REPRINT_HISTORY.FieldByName('PR_REPRINT_HISTORY_NBR').AsInteger > 0 then
     with TExecDSWrapper.Create('List_Reprint_History_Detail') do
     begin
      SetParam('Pr_History_Nbr', DM_CLIENT.PR_REPRINT_HISTORY.FieldByName('PR_REPRINT_HISTORY_NBR').AsString);
      ctx_DataAccess.GetCustomData(cdPR_REPRINT_HISTORY_DETAIL, AsVariant);
     end;
  Finally
   cdPR_REPRINT_HISTORY_DETAIL.EnableControls;
  end;
end;

procedure TEDIT_PR_CHECK_REPRINT.wwdbgPR_REPRINT_HISTORYRowChanged(
  Sender: TObject);
begin
//  inherited;
   List_Reprint_History_Detail;
end;

procedure TEDIT_PR_CHECK_REPRINT.PRNextExecute(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRChecks;
end;

procedure TEDIT_PR_CHECK_REPRINT.ReopenPRChecks;
begin
  if ctx_DataAccess.CUSTOM_VIEW.Active then
    ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('SelectCheckWithEE') do
  begin
    SetMacro('Columns', 'P.SOCIAL_SECURITY_NUMBER, P.LAST_NAME, P.FIRST_NAME, E.CUSTOM_EMPLOYEE_NUMBER, C.NET_WAGES, '
      + 'P.LAST_NAME || '', '' || P.FIRST_NAME Employee_Name_Calculate, Trim(E.CUSTOM_EMPLOYEE_NUMBER) EE_Custom_Number, '
      + 'E.CO_DIVISION_NBR, E.CO_BRANCH_NBR, E.CO_DEPARTMENT_NBR, E.CO_TEAM_NBR, C.PR_CHECK_NBR, C.EE_NBR, C.CHECK_TYPE, C.PAYMENT_SERIAL_NUMBER, '
      + 'V.CUSTOM_DIVISION_NUMBER, A.CUSTOM_BRANCH_NUMBER, D.CUSTOM_DEPARTMENT_NUMBER, T.CUSTOM_TEAM_NUMBER');
    SetMacro('NbrField', 'PR');
    SetParam('RecordNbr', DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  tempChecks.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
  ctx_DataAccess.CUSTOM_VIEW.Close;

  tempChecks.IndexFieldNames := ctx_PayrollCalculation.ConstructCheckSortIndex;
end;

procedure TEDIT_PR_CHECK_REPRINT.Deactivate;
begin
  DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
  CheckFormCodeValues.Free;
  MiscCheckFormCodeValues.Free;
  DM_EMPLOYEE.EE.Close;
  inherited;
end;

procedure TEDIT_PR_CHECK_REPRINT.bbtnPrintEverythingClick(Sender: TObject);
var
  slNormalChecks, slAgencyChecks, slTaxChecks, slBillingChecks, slMiscChecks: TStringList;
  qTask: IevReprintPayrollTask;
  aCurrentToa: Boolean;
  Reason: String;
begin
  inherited;
  ReprintParameters := TReprintParameters.Create(Nil);
  try
    aCurrentToa := False;
    if ReprintParameters.ShowModal <> mrOK then
      Exit;
    ReopenPRChecks;
    slNormalChecks := TStringList.Create;
    slAgencyChecks := TStringList.Create;
    slTaxChecks := TStringList.Create;
    slBillingChecks := TStringList.Create;
    slMiscChecks := TStringList.Create;
    try
      if ReprintParameters.cbPayrollChecks.Checked or ReprintParameters.cbMiscChecks.Checked then
      begin
        if ReprintParameters.cbPayrollChecks.Checked then
        with TevClientDataSet(wwdbgPR_CHECKS.DataSource.DataSet) do
        begin
          DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR=' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
          DisableControls;
          LookupsEnabled := False;
          First;
          while not EOF do
          begin
            slNormalChecks.Add(FieldByName('PR_CHECK_NBR').AsString);
            Next;
          end;
          LookupsEnabled := True;
          EnableControls;

          aCurrentTOA := FReprintBalCodeValues[cbReprintTOBalances.ItemIndex + 1] = GROUP_BOX_CURRENT;
        end;
        if ReprintParameters.cbMiscChecks.Checked then
        with TevClientDataSet(wwdbgPR_MISC_CHECKS.DataSource.DataSet) do
        begin
          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_NBR=' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
          DisableControls;
          LookupsEnabled := False;
          First;
          while not EOF do
          begin
            if FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_AGENCY, MISC_CHECK_TYPE_AGENCY_VOID] then
              slAgencyChecks.Add(FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
            if FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_TAX, MISC_CHECK_TYPE_TAX_VOID] then
              slTaxChecks.Add(FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
            if FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] in [MISC_CHECK_TYPE_BILLING, MISC_CHECK_TYPE_BILLING_VOID] then
              slBillingChecks.Add(FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
            slMiscChecks.Add(FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
            Next;
          end;
          LookupsEnabled := True;
          EnableControls;
        end;
      end;
      if CheckPreparedChecks(False, slNormalChecks, slAgencyChecks, slTaxChecks, slBillingChecks, slMiscChecks, Reason)
      or ReprintParameters.cbReports.Checked or ReprintParameters.cbInvoices.Checked then
      begin
        qTask := mb_TaskQueue.CreateTask(QUEUE_TASK_REPRINT_PAYROLL, True) as IevReprintPayrollTask;
        try
          qTask.Caption := 'Reprinting ' + Trim(DM_TEMPORARY.TMP_CO.FieldByName('Name').AsString) + ' ' +
            DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString + '-' + DM_PAYROLL.PR.FieldByName('RUN_NUMBER').AsString;
          qTask.Reason := Reason;  
          qTask.ClientID := ctx_DataAccess.ClientID;
          qTask.PrNbr := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
          qTask.PrintReports := ReprintParameters.cbReports.Checked;
          qTask.PrintInvoices := ReprintParameters.cbInvoices.Checked;
          qTask.CurrentTOA := aCurrentToa;
          qTask.PayrollChecks := slNormalChecks.CommaText;
          qTask.AgencyChecks := slAgencyChecks.CommaText;
          qTask.TaxChecks := slTaxChecks.CommaText;
          qTask.BillingChecks := slBillingChecks.CommaText;
          qTask.CheckForm := CheckFormCodeValues[cbCheckForm.ItemIndex];
          qTask.MiscCheckForm := MiscCheckFormCodeValues[cbMiscCheckForm.ItemIndex];
          qTask.Destination := FDestination;
          qTask.SkipCheckStubCheck := False;
          qTask.DontPrintBankInfo := chbDontPrintBankInfo.Checked;
          qTask.HideBackground := chbHideBackground.Checked;
          qTask.DontUseVMRSettings := cbDontUseVMR.Visible and cbDontUseVMR.Checked;
        if FDestination = rdtVMR then
          qTask.PrReprintHistoryNBR := FPrReprintHistoryNBR;
          ctx_EvolutionGUI.AddTaskQueue(qTask);
        finally
          qTask := nil;
        end;
      end;
    finally
      slNormalChecks.Free;
      slAgencyChecks.Free;
      slTaxChecks.Free;
      slBillingChecks.Free;
      slMiscChecks.Free;
    end;
  finally
    ReprintParameters.Free;
  end;
end;

procedure TEDIT_PR_CHECK_REPRINT.Activate;
var
  I: Integer;
  S1, S2, ComboChoices: String;
begin
  inherited;
  //to hide FavoriteReport icon
  pnlFavoriteReport.visible := False;

  if (wwdsList.DataSet.recordcount <= 0) then exit;

  CheckFormCodeValues := TStringList.Create;
  MiscCheckFormCodeValues := TStringList.Create;
  ComboChoices := CheckForm_ComboChoices;
  I := 1;
  while not GetToken(ComboChoices, I, S1) do
  begin
    GetToken(ComboChoices, I, S2);
    cbCheckForm.Items.Append(S1);
    cbMiscCheckForm.Items.Append(S1);
    CheckFormCodeValues.Append(S2);
    MiscCheckFormCodeValues.Append(S2);
  end;
  AppendCheckForms;
  if cbCheckForm.Visible then
    cbCheckForm.ItemIndex := CheckFormCodeValues.IndexOf(GetCheckForm);
  if cbMiscCheckForm.Visible then
    cbMiscCheckForm.ItemIndex := MiscCheckFormCodeValues.IndexOf(GetCheckForm(False));

  ComboChoices := ReprintTOBalance_ComboChoices;
  I := 1;
  FReprintBalCodeValues := '';
  while not GetToken(ComboChoices, I, S1) do
  begin
    GetToken(ComboChoices, I, S2);
    cbReprintTOBalances.Items.Append(S1);
    FReprintBalCodeValues := FReprintBalCodeValues + S2;
  end;
  if cbReprintTOBalances.Visible then
    cbReprintTOBalances.ItemIndex := Pos(DM_COMPANY.CO.FieldByName('REPRINT_TO_BALANCE').AsString, FReprintBalCodeValues) - 1;

  UpdateDestChoices;
end;

procedure TEDIT_PR_CHECK_REPRINT.wwdsSubMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_PAYROLL.PR.Active and (DM_PAYROLL.PR.RecordCount > 0) then
    bbtnPrintEverything.Enabled := DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED];
end;

function TEDIT_PR_CHECK_REPRINT.CheckPreparedChecks(CheckIfManual: Boolean; slNormalChecks,
  slAgencyChecks, slTaxChecks, slBillingChecks, slMiscChecks: TStringList; var aReason: String): Boolean;
var
  S, ReprintReason: String;
  I: Integer;

  procedure CheckCheckBeforePost(DataSet: TDataSet);
  begin
    if (DM_PAYROLL.PR.Lookup('PR_NBR', DataSet.FieldByName('PR_NBR').Value, 'STATUS') = PAYROLL_STATUS_PROCESSED)
    or (DM_PAYROLL.PR.Lookup('PR_NBR', DataSet.FieldByName('PR_NBR').Value, 'STATUS') = PAYROLL_STATUS_VOIDED) then
      Exit;

    if (DataSet.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString <> OVERRIDE_VALUE_TYPE_NONE) then
      if DataSet.FieldByName('OR_CHECK_FEDERAL_VALUE').IsNull then
        raise EUpdateError.CreateHelp('You must enter federal override value', IDH_ConsistencyViolation);

    if DataSet.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_YTD, CHECK_TYPE2_QTD] then
    begin
      if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value <> PAYROLL_TYPE_SETUP then
        raise EUpdateError.CreateHelp('You can not have Setup YTD or QTD check types on this payroll.', IDH_ConsistencyViolation);
    end
    else
    begin
      if (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP) and (DataSet.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_3RD_PARTY) then
        raise EUpdateError.CreateHelp('You can not have this check type on a Setup payroll.', IDH_ConsistencyViolation);
    end;
  end;

begin
  Result := False;
  ReprintReason := '';
  FPrReprintHistoryNBR := 0;
  if (slNormalChecks.Count > 0) or (slMiscChecks.Count > 0) then
  begin
    if (slNormalChecks.Count > 0) and (slMiscChecks.Count > 0) then
      S := 'Reprint SELECTED Payroll and Miscellaneous Checks?'
    else
    if slNormalChecks.Count > 0 then
      S := 'Reprint SELECTED Payroll Checks?'
    else
      S := 'Reprint SELECTED Miscellaneous Checks?';

    if EvMessage(S, mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      if not CheckIfManual and (FDestination <> rdtPreview) and (slNormalChecks.Count > 0)
      and (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED])
      and (ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_PROCESSED_CHECKS') <> stEnabled) then
      begin
        EvErrMessage('You can not reprint Payroll checks for processed payrolls!');
        slNormalChecks.Clear;
      end;
      if (FDestination <> rdtPreview) and (slMiscChecks.Count > 0)
      and (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED])
      and (ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_MISC_CHECKS') <> stEnabled) then
      begin
        EvErrMessage('You can not reprint Misc checks!');
        slMiscChecks.Clear;
        slAgencyChecks.Clear;
        slTaxChecks.Clear;
        slBillingChecks.Clear;
      end;
      if slNormalChecks.Count > 0 then
      begin
        slNormalChecks.Sort;

        if (DM_PAYROLL.PR.STATUS.AsString <> PAYROLL_STATUS_PROCESSED)
        and (DM_PAYROLL.PR.STATUS.AsString <> PAYROLL_STATUS_VOIDED) then
        begin
          for I := 0 to slNormalChecks.Count - 1 do
          begin
            DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR=' + slNormalChecks[I]);
            CheckCheckBeforePost(DM_PAYROLL.PR_CHECK);
          end;
        end;

        if (FDestination <> rdtPreview) then
        begin
          ReprintReason := AskForCheckReprintReason; //LogCheckReprint(slNormalChecks, False, DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger);
          AssignValues(ReprintReason,FPrReprintHistoryNBR);
          aReason := ReprintReason;
          if CheckIfManual then
          try
            ctx_DataAccess.UnlockPRSimple;
            with DM_PAYROLL do
            for I := 0 to slNormalChecks.Count - 1 do
            begin
              DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR=' + slNormalChecks[I]);
              DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + slNormalChecks[I]);
              DM_PAYROLL.PR_CHECK_SUI.DataRequired('PR_CHECK_NBR=' + slNormalChecks[I]);
              DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + slNormalChecks[I]);

              PR_CHECK.Edit;
              PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value := PR_CHECK.FieldByName('FEDERAL_TAX').Value;
              if PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').IsNull then
                PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').Value := OVERRIDE_VALUE_TYPE_NONE
              else
                PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').Value := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
              PR_CHECK.FieldByName('OR_CHECK_OASDI').Value := PR_CHECK.FieldByName('EE_OASDI_TAX').Value;
              PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value := PR_CHECK.FieldByName('EE_MEDICARE_TAX').Value;
              PR_CHECK.Post;

              PR_CHECK_STATES.First;
              while not PR_CHECK_STATES.EOF do
              begin
                PR_CHECK_STATES.Edit;
                PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value := PR_CHECK_STATES.FieldByName('STATE_TAX').Value;
                if PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').IsNull then
                  PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value := OVERRIDE_VALUE_TYPE_NONE
                else
                  PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').Value := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
                PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').Value := PR_CHECK_STATES.FieldByName('EE_SDI_TAX').Value;
                PR_CHECK_STATES.Post;
                PR_CHECK_STATES.Next;
              end;

              PR_CHECK_SUI.First;
              while not PR_CHECK_SUI.EOF do
              begin
                PR_CHECK_SUI.Edit;
                PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value := PR_CHECK_SUI.FieldByName('SUI_TAX').Value;
                PR_CHECK_SUI.Post;
                PR_CHECK_SUI.Next;
              end;

              PR_CHECK_LOCALS.First;
              while not PR_CHECK_LOCALS.EOF do
              begin
                PR_CHECK_LOCALS.Edit;
                PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Value := PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value;
                PR_CHECK_LOCALS.Post;
                PR_CHECK_LOCALS.Next;
              end;
              ctx_DataAccess.PostDataSets([PR_CHECK, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS]);
            end;
          finally
            ctx_DataAccess.LockPRSimple;
          end;
        end;
      end;
      if slMiscChecks.Count > 0 then
      begin
        slMiscChecks.Sort;
//        if (FDestination <> rdtPreview) then
//          LogCheckReprint(slMiscChecks, True, DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger, ReprintReason);
      end;
    end
    else
      Exit;
  end
  else
    Exit;
  if (slNormalChecks.Count > 0) or (slMiscChecks.Count > 0) then
    Result := True;
end;

procedure TEDIT_PR_CHECK_REPRINT.wwdsListDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if Assigned(cbCheckForm) then
  begin
    cbCheckForm.Visible := not BitBtn1.Enabled;
    cbMiscCheckForm.Visible := not BitBtn1.Enabled;
    cbReprintTOBalances.Visible := not BitBtn1.Enabled;
    cbDestination.Visible := not BitBtn1.Enabled;
    evLabel2.Visible := not BitBtn1.Enabled;
    evLabel3.Visible := not BitBtn1.Enabled;
    LabelReprintTOBalances.Visible := not BitBtn1.Enabled;
    bbtnPrintEverything.Visible := not BitBtn1.Enabled;
    evLabel4.Visible := not BitBtn1.Enabled;
    chbDontPrintBankInfo.Visible := not BitBtn1.Enabled;
    chbHideBackground.Visible := not BitBtn1.Enabled;
    cbDontUseVMR.Visible := not BitBtn1.Enabled;
    if cbCheckForm.Visible and Assigned(CheckFormCodeValues) then
      cbCheckForm.ItemIndex := CheckFormCodeValues.IndexOf(GetCheckForm);
    if cbMiscCheckForm.Visible and Assigned(MiscCheckFormCodeValues) then
      cbMiscCheckForm.ItemIndex := MiscCheckFormCodeValues.IndexOf(GetCheckForm(False));
    if cbReprintTOBalances.Visible then
      cbReprintTOBalances.ItemIndex := Pos(DM_COMPANY.CO.FieldByName('REPRINT_TO_BALANCE').AsString, FReprintBalCodeValues) - 1;
  end;
end;

procedure TEDIT_PR_CHECK_REPRINT.AfterDataSetsReopen;
begin
  inherited;
  UpdateDestChoices;
end;

procedure TEDIT_PR_CHECK_REPRINT.cbDestinationChange(Sender: TObject);
begin
  inherited;
  if (cbDestination.ItemIndex = 1)
  or ((cbDestination.ItemIndex = 0) and mb_AppSettings.AsBoolean['Settings\AlwaysDoPreview']) then
    FDestination := rdtPreview
  else if cbDestination.ItemIndex = 0 then
    FDestination := rdtPrinter
  else
    FDestination := rdtVMR;

  cbDontUseVMR.Visible := not BitBtn1.Enabled and (FDestination <> rdtVMR);
end;

procedure TEDIT_PR_CHECK_REPRINT.AppendCheckForms;
var
  RegularSBChecks, MiscSBChecks: TStrings;
  i: integer;
begin
  RegularSBChecks := TStringList.Create;
  MiscSBChecks := TStringList.Create;
  try
    ctx_DataAccess.FillSBCheckList(RegularSBChecks, MiscSBChecks);
    for i := 0 to RegularSBChecks.Count - 1 do
    begin
      cbCheckForm.Items.Append( RegularSBChecks.Names[i] );
      CheckFormCodeValues.Append( RegularSBChecks.ValueFromIndex[i] );
    end;
    for i := 0 to MiscSBChecks.Count - 1 do
    begin
      cbMiscCheckForm.Items.Append( MiscSBChecks.Names[i] );
      MiscCheckFormCodeValues.Append( MiscSBChecks.ValueFromIndex[i] );
    end;
  finally
    RegularSBChecks.Free;
    MiscSBChecks.Free;
  end;
end;

function TEDIT_PR_CHECK_REPRINT.GetCheckForm(aRegular: boolean): string;
begin
  if aRegular then
    Result := DM_COMPANY.CO.FieldByName('CHECK_FORM').AsString
  else
    Result := DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString;

  if (Result = CHECK_FORM_BUREAU_DESIGNER_CHECK) or (Result = CHECK_FORM_BUREAU_NEW_CHECK) then
  begin
    if aRegular then
      Result := ExtractFromFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, 65, 6)
    else
      Result := ExtractFromFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, 71, 6);
  end;
end;

procedure TEDIT_PR_CHECK_REPRINT.AssignValues(var aReprintReason: string; var aPrReprintHistoryNBR: Integer);
var s: String;
begin
  s := aReprintReason;
  GetPrevStrValue(aReprintReason, '#PR_REPRINT_HISTORY_NBR#');
  s := Copy(s, pos('#PR_REPRINT_HISTORY_NBR#',s)+24, Length(s)-(pos('#PR_REPRINT_HISTORY_NBR#',s)+23));
  aPrReprintHistoryNBR := StrToIntdef(s,0);
end;

initialization
  RegisterClass(TEDIT_PR_CHECK_REPRINT);

end.
