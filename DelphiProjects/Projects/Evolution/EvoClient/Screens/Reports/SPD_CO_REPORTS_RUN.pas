// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_CO_REPORTS_RUN;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,
  SDataStructure, EvSecElement, Wwdatsrc, Buttons, wwdblook, StdCtrls,
  Wwdotdot, Wwdbcomb, Wwdbspin, Mask, wwdbedit, DBCtrls, Grids, Wwdbigrd,
  Wwdbgrid, ExtCtrls, ComCtrls,  SPD_EDIT_CO_BASE, EvUtils, evTypes,
  SReportSettings, SPD_EDIT_Open_BASE, SPackageEntry, Variants, EvContext,
  ISBasicClasses, SDDClasses, rwInputFormContainerFrm, SDataDictclient,
  SDataDicttemp, EvCommonInterfaces, EvUIUtils, EvUIComponents, EvClientDataSet,
  ImgList, LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, EvConsts,
  SPayrollReport;

type
  TCO_REPORTS_RUN = class(TEDIT_CO_BASE)
    grReports: TevDBCheckGrid;
    tsInForm: TTabSheet;
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    evPanel3: TevPanel;
    evPanel4: TevPanel;
    Panel4: TPanel;
    btnRun: TevBitBtn;
    cbOverride: TPanel;
    Label1: TLabel;
    cbOverrideLevel: TevComboBox;
    Bevel1: TBevel;
    Panel5: TPanel;
    btnSetUpRep: TevBitBtn;
    chbQueue: TevCheckBox;
    btnRestore: TevBitBtn;
    lNoParams: TLabel;
    cbDestination: TevComboBox;
    sbBrowseReportsRun: TScrollBox;
    pnlsbBrowseReportsBody: TevPanel;
    pnlRunReportsLeft: TevPanel;
    evSplitter1: TevSplitter;
    fpReportsCompany: TisUIFashionPanel;
    evPanel5: TevPanel;
    fpReportsReport: TisUIFashionPanel;
    pnlFPReportsCompany: TevPanel;
    pnlFPReportsReport: TevPanel;
    sbReportSetup: TScrollBox;
    procedure tsInFormShow(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure cbOverrideLevelChange(Sender: TObject);
    procedure tsInFormHide(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnSetUpRepClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnRestoreClick(Sender: TObject);
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);

  private
    FParams: TrwReportParams;
    FStoredParams: TrwReportParams;
    FCurrRepKey: String;
    FRepGroups: TStringList;
    pnlInputForm: TrwInputFormContainer;

    procedure InitReportRes;

  protected
    function  GetDefaultDataSet: TevClientDataSet; override;
    function  IgnoreAutoJump: Boolean; override;
    procedure AfterDataSetsReopen; override;
    procedure UpdateDestChoices;
  public
    procedure Activate; override;
    procedure DeActivate; override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
  end;

implementation

uses isBaseClasses, EvDataset;
{$R *.DFM}


{ TCO_REPORTS_RUN }

procedure TCO_REPORTS_RUN.Activate;
begin
  inherited;
  //to hide FavoriteReport icon
  pnlFavoriteReport.visible := False;

  wwdsDetail.DataSet := DM_COMPANY.CO_REPORTS;

  pnlInputForm := TrwInputFormContainer.Create(Self);
  pnlInputForm.Parent := sbReportSetup; //GUI 2.0. Old Parent = tsInForm;
  pnlInputForm.Align := alClient;

  FParams := TrwReportParams.Create;
  FStoredParams := TrwReportParams.Create;
  FCurrRepKey := '';
  FRepGroups := TStringList.Create;
  ctx_RWLocalEngine.BeginWork;
  UpdateDestChoices;
end;

procedure TCO_REPORTS_RUN.DeActivate;
begin
  if tsInForm.Visible then
    tsInFormHide(nil);

  FStoredParams.Free;
  FStoredParams := nil;
  FParams.Free;
  FParams := nil;
  FRepGroups.Free;
  FRepGroups := nil;

  try
    ctx_RWLocalEngine.EndWork;
  except
  end;  

  inherited;
end;

function TCO_REPORTS_RUN.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_REPORTS;
end;


procedure TCO_REPORTS_RUN.tsInFormShow(Sender: TObject);
var
  i: Integer;
begin
  lNoParams.Visible := False;
  Update;

  InitReportRes;

  for i := 0 to FStoredParams.Count - 1 do
    if not FStoredParams[i].Store then
      FParams.Add(FStoredParams[i].Name, FStoredParams[i].Value, FStoredParams[i].Store);

  cbOverrideLevel.ItemIndex := 0;
  cbOverrideLevel.OnChange(nil);
end;

procedure TCO_REPORTS_RUN.btnRunClick(Sender: TObject);
const sQry : string = 'SELECT CHECK_DATE, PR_NBR FROM PR WHERE {AsOfNow<PR>} and PR_NBR IN (%s) ORDER BY CHECK_DATE DESC';
var
  OldBM, BM: TBookmark;
  i, j, k: Integer;
  lParams: TrwReportParams;
  Dest: TrwReportDestination;
  iVmrPrNbr: Integer;
  dVmrEventDate: TDateTime;
  sCaption, sReportDesc : String;
  Qry : IevQuery;
  PrNbrList : IisStringList;
  RunParams: TevPrRepPrnSettings;
begin
  dVmrEventDate := 0 ;
  iVmrPrNbr := 0;
  sReportDesc := 'Reprinted reports';
  ctx_StartWait;
  try
    ctx_RWLocalEngine.CloseInputForm(mrOK, FParams);
    pnlInputForm.Visible := False;
    for i := 0 to FParams.Count - 1 do
    begin
      FStoredParams.Add(FParams[i].Name, FParams[i].Value, FParams[i].Store);
      if FParams[i].Name = 'Payrolls' then
        if VarIsArray(FParams[i].Value) then
        begin
          PrNbrList := TisStringList.Create;
          for j := VarArrayLowBound(FParams[i].Value, 1) to VarArrayHighBound(FParams[i].Value, 1) do
              PrNbrList.Add(FParams[i].Value[j]);
        end;
    end;

    if Assigned(PrNbrList) and (PrNbrList.count > 0) then
    begin
      Qry := TevQuery.Create(format(sQry, [PrNbrList.CommaText]));
      Qry.Execute;
      if Qry.Result.RecordCount > 0 then
      begin
        dVmrEventDate := Qry.Result.FieldByName('CHECK_DATE').asDateTime;
        iVmrPrNbr := Qry.Result.FieldByName('PR_NBR').asInteger;
      end;
      sReportDesc   := sReportDesc + ' ' + DateToStr(dVmrEventDate)+'#1';
    end;

    FCurrRepKey := '';
    PageControl1.ActivePage := TabSheet1;

    if grReports.SelectedList.Count > 1 then
      lParams := TrwReportParams.Create
    else
      lParams := FParams;

    Dest := rdtCancel;
    ctx_RWLocalEngine.StartGroup;

    try
      with wwdsDetail.DataSet do
      begin
        DisableControls;
        OldBM := GetBookmark;
        try
          First;
          while not Eof do
          begin
            BM := GetBookmark;
            try
              for i := 0 to grReports.SelectedList.Count - 1 do
                if CompareBookmarks(grReports.SelectedList[i], BM) = 0 then
                begin
                  if lParams <> FParams then
                  begin
                    lParams.ReadFromBlobField(TBlobField(wwdsDetail.DataSet.FieldByName('INPUT_PARAMS')));
                    for j := 0 to FParams.Count - 1 do
                    begin
                      k := lParams.IndexOf(FParams[j].Name);
                      if (k = -1) {and not FParams[j].Store} then
                        lParams.Add(FParams[j].Name, FParams[j].Value, FParams[j].Store)
                      else if (k <> -1) and not VarIsNull(FParams[j].Value) then
                        lParams[k].Value := FParams[j].Value;
                    end;
                  end;

                  lParams.Add('ReportName', wwdsDetail.DataSet.FieldByName('CUSTOM_NAME').AsString +
                    ' (' + wwdsDetail.DataSet.FieldByName('REPORT_LEVEL').AsString +
                    wwdsDetail.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').AsString + ')');
                  lParams.Add('Clients', varArrayOf([ctx_DataAccess.ClientID]) );
                  lParams.Add('Companies', varArrayOf([wwdsList.DataSet.FieldByName('CO_NBR').AsInteger]));
                  lParams.Add('ClientCompany', varArrayOf([IntToStr(ctx_DataAccess.ClientID) + ':' + wwdsList.DataSet.FieldByName('CO_NBR').AsString]));
                  lParams.Add('HideCompanyList', True);
                  lParams.Add('SetupMode', False);
                  lParams.Add('Co_Reports_Nbr', DM_COMPANY.CO_REPORTS.CO_REPORTS_NBR.AsInteger);

                  if not DM_COMPANY.CO_REPORTS.RUN_PARAMS.IsNull then
                  begin
                    RunParams := TevPrRepPrnSettings.Create(nil);
                    try
                      try
                        RunParams.ReadFromBlobField(TBlobField(DM_COMPANY.CO_REPORTS.RUN_PARAMS));
                        lParams.Add(__DASHBOARD, RunParams.DisplayOnDashboard);
                      except
                      end
                    finally
                      RunParams.Free;
                    end;
                  end;

                  lParams.Copies := 1;

                  sCaption := wwdsDetail.DataSet.FieldByName('CUSTOM_NAME').AsString;
                  if i = 0 then
                    sCaption := 'CO# ' + wwdsList.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '. ' + sCaption;
                  ctx_RWLocalEngine.CalcPrintReport(
                    wwdsDetail.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').AsInteger,
                    wwdsDetail.DataSet.FieldByName('REPORT_LEVEL').AsString,
                    lParams, sCaption).OverrideVmrMbGroupNbr :=
                    wwdsDetail.DataSet.FieldByName('OVERRIDE_MB_GROUP_NBR').AsInteger;
                end;

             finally
               FreeBookmark(BM);
             end;

             Next;
          end;

          GotoBookmark(OldBM);
        finally
          FreeBookmark(OldBM);
          EnableControls;
        end;
      end;

      case cbDestination.ItemIndex of
      0: Dest := rdtPrinter;
      1: Dest := rdtPreview;
      2: Dest := rdtVMR;
      end;
    finally
      try
        if chbQueue.Checked then
          ctx_RWLocalEngine.EndGroupForTask(Dest,
            sReportDesc, wwdsList.DataSet.FieldByName('CO_NBR').AsInteger, iVmrPrNbr, dVmrEventDate)
        else
          ctx_RWLocalEngine.EndGroup(Dest,
            sReportDesc, wwdsList.DataSet.FieldByName('CO_NBR').AsInteger, iVmrPrNbr, dVmrEventDate);
      finally
        if lParams <> FParams then
          lParams.Free;
      end;
    end;

  finally
    ctx_EndWait;
  end;
end;


procedure TCO_REPORTS_RUN.InitReportRes;
var
  OldBM, BM: TBookmark;
  i, j, k, l: Integer;
  h: String;
  lPar: TrwReportParams;
begin
  pnlInputForm.Visible := False;
  ctx_RWLocalEngine.CloseInputForm(mrCancel, nil);
  FCurrRepKey := '';

  cbOverrideLevel.Clear;
  FRepGroups.Clear;

  with wwdsDetail.DataSet do
  begin
    if grReports.SelectedList.Count = 0 then
    begin
      Exit;
    end

    else if grReports.SelectedList.Count = 1 then
    begin
      DisableControls;
      OldBM := GetBookmark;
      try
        GotoBookmark(grReports.SelectedList[0]);
        cbOverrideLevel.Items.Add(wwdsDetail.DataSet.FieldByName('NDescription').AsString);
        FRepGroups.Add(wwdsDetail.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').AsString +
          wwdsDetail.DataSet.FieldByName('REPORT_LEVEL').AsString);
        FParams.ReadFromBlobField(TBlobField(wwdsDetail.DataSet.FieldByName('INPUT_PARAMS')));

        GotoBookmark(OldBM);
      finally
        FreeBookmark(OldBM);
        EnableControls;
      end;

      cbOverrideLevel.Enabled := False;
    end

    else
    begin
      DisableControls;
      OldBM := GetBookmark;
      lPar := TrwReportParams.Create;
      FParams.Clear;
      try
        First;
        while not Eof do
        begin
          BM := GetBookmark;
          try

            for i := 0 to grReports.SelectedList.Count - 1 do
              if CompareBookmarks(grReports.SelectedList[i], BM) = 0 then
              begin
                lPar.ReadFromBlobField(TBlobField(wwdsDetail.DataSet.FieldByName('INPUT_PARAMS')));
                for j := 0 to lPar.Count - 1 do
                begin
                  k := FParams.IndexOf(lPar[j].Name);
                  if k = -1 then
                    FParams.Add(lPar[j].Name, lPar[j].Value, lPar[j].Store)
                  else
                  begin
                    if VarIsArray(FParams[k].Value) and VarIsArray(lPar[j].Value) then
                      if (VarArrayLowBound(FParams[k].Value, 1) <> VarArrayLowBound(lPar[j].Value, 1)) or
                         (VarArrayHighBound(FParams[k].Value, 1) <> VarArrayHighBound(lPar[j].Value, 1)) then
                        FParams[k].Value := Null
                      else
                        for l := VarArrayLowBound(FParams[k].Value, 1) to VarArrayHighBound(FParams[k].Value, 1) do
                        begin
                          if (VarType(FParams[k].Value[l]) = VarType(lPar[j].Value[l])) and (FParams[k].Value[l] = lPar[j].Value[l]) then
                            Continue
                          else
                          begin
                            FParams[k].Value := Null;
                            break;
                          end;
                        end

                    else if (VarType(FParams[k].Value) <> VarType(lPar[j].Value)) or (lPar[j].Value <> FParams[k].Value) then
                      FParams[k].Value := Null;
                  end;
                end;

                h := ctx_RWLocalEngine.GetRepAncestor(
                  wwdsDetail.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').AsInteger,
                  wwdsDetail.DataSet.FieldByName('REPORT_LEVEL').AsString);

                if (h <> '') and (FRepGroups.IndexOf(h) = -1) then
                begin
                  cbOverrideLevel.Items.Add(ctx_RWLocalEngine.ClassNameToDescr(h));
                  FRepGroups.Add(h);
                end;
              end;

           finally
             FreeBookmark(BM);
           end;

           Next;
        end;

        GotoBookmark(OldBM);
      finally
        lPar.Free;
        FreeBookmark(OldBM);
        EnableControls;
      end;

      cbOverrideLevel.Enabled := True;
    end;
  end;
end;

procedure TCO_REPORTS_RUN.cbOverrideLevelChange(Sender: TObject);
var
  sKey: String;
  lPar: TrwReportParams;
  i: Integer;
begin
  Update;

  try
    if (FCurrRepKey <> '') then
    begin
      lPar := TrwReportParams.Create;
      try
        try
          pnlInputForm.Visible := False;        
          ctx_RWLocalEngine.CloseInputForm(mrOk, lPar);
          for i := 0 to lPar.Count - 1 do
            FParams.Add(lPar[i].Name, lPar[i].Value);
        except
        end;
      finally
        lPar.Free;
      end;
    end;

    if cbOverrideLevel.ItemIndex = -1 then
    begin
      btnRun.Enabled := False;
      Exit;
    end;

    FParams.Add('Clients', varArrayOf([ctx_DataAccess.ClientID]) );
    FParams.Add('Companies', varArrayOf([wwdsList.DataSet.FieldByName('CO_NBR').AsInteger]));
    FParams.Add('ClientCompany', varArrayOf([IntToStr(ctx_DataAccess.ClientID) + ':' + wwdsList.DataSet.FieldByName('CO_NBR').AsString]));
    FParams.Add('HideCompanyList', True);
    FParams.Add('SetupMode', False);


    ctx_StartWait('Opening Report...');
    try
      pnlInputForm.Align := alNone;
      pnlInputForm.Visible := False;
      Update;

      sKey := FRepGroups[cbOverrideLevel.ItemIndex];
      if (sKey[1] >= '0') and (sKey[1] <= '9') then
        lNoParams.Visible := not ctx_RWLocalEngine.ShowInputForm(StrToInt(Copy(sKey, 1, Length(sKey) - 1)), sKey[Length(sKey)], pnlInputForm, FParams)
      else
        lNoParams.Visible := not ctx_RWLocalEngine.ShowAncestorInputForm(sKey, pnlInputForm, FParams);
    finally
      pnlInputForm.Align := alClient;
      pnlInputForm.Visible := True;
      ctx_EndWait;
      Update;
    end;

    FCurrRepKey := sKey;
    btnRun.Enabled := True;

  finally
    if not DM_COMPANY.CO.Active then
      DM_COMPANY.CO.Active := True;
  end;
end;

procedure TCO_REPORTS_RUN.tsInFormHide(Sender: TObject);
begin
  pnlInputForm.Visible := False;
  ctx_RWLocalEngine.CloseInputForm(mrCancel, nil);
  FCurrRepKey := '';
end;

procedure TCO_REPORTS_RUN.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;

  if grReports.SelectedList.Count = 0 then
  begin
    AllowChange := False;
    EvErrMessage('Reports are not selected');
  end;
end;

procedure TCO_REPORTS_RUN.btnSetUpRepClick(Sender: TObject);
begin
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CO_REPORTS');
end;


procedure TCO_REPORTS_RUN.BitBtn1Click(Sender: TObject);
begin
  grReports.UnSelectAll;
  inherited;
end;

function TCO_REPORTS_RUN.IgnoreAutoJump: Boolean;
begin
  Result := True;
end;

procedure TCO_REPORTS_RUN.btnRestoreClick(Sender: TObject);
var
  i: Integer;
begin
  i := cbOverrideLevel.ItemIndex;

  if i = -1 then
    Exit;

  InitReportRes;

  cbOverrideLevel.ItemIndex := i;
  cbOverrideLevel.OnChange(nil);
end;

procedure TCO_REPORTS_RUN.wwdsMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Assigned(FStoredParams) then
    FStoredParams.Clear;
end;

procedure TCO_REPORTS_RUN.AfterDataSetsReopen;
begin
  inherited;

  if Context.UserAccount.AccountType = uatRemote then
  begin
    // Filter the reports dataset by HideForRemotes false
    dm_company.co_reports.Filter := 'HideForRemotes = ''N''';
    dm_company.co_reports.Filtered := True;
  end;

  UpdateDestChoices;
end;

procedure TCO_REPORTS_RUN.UpdateDestChoices;
var
  i: Integer;
begin
  i := cbDestination.ItemIndex;
  if CheckVmrLicenseActive and CheckVmrSetupActive then
    cbDestination.Items.Text := 'Print'#13'Preview'#13'Send To Mailbox'
  else
    cbDestination.Items.Text := 'Print'#13'Preview';
  cbDestination.ItemIndex := i;
end;


procedure TCO_REPORTS_RUN.OnSetButton(Kind: Integer; var Value: Boolean);
begin
  inherited;
  if Kind in [NavInsert, NavDelete] then
    Value := False;
end;


initialization
  RegisterClass(TCO_REPORTS_RUN);

end.
