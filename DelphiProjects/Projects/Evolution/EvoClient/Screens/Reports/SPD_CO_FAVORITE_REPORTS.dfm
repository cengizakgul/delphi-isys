object CO_FavoriteReports: TCO_FavoriteReports
  Left = 476
  Top = 219
  Width = 910
  Height = 724
  Caption = 'Favorite Reports'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 894
    Height = 686
    ActivePage = tsFavoriteReports
    Align = alClient
    Constraints.MinHeight = 394
    Constraints.MinWidth = 286
    TabOrder = 0
    OnChange = PageControl1Change
    object tsFavoriteReports: TTabSheet
      Caption = 'Favorite Reports'
      ImageIndex = 2
      object sbFavorite: TScrollBox
        Left = 0
        Top = 0
        Width = 886
        Height = 658
        Align = alClient
        TabOrder = 0
        object fpFavorite: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 262
          Height = 352
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpFavorite'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Favorite'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grFavoriteReports: TevDBCheckGrid
            Left = 12
            Top = 35
            Width = 226
            Height = 293
            DisableThemesInTitle = False
            Selected.Strings = (
              'NDescription'#9'255'#9'Report Name'#9'F')
            IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
            IniAttributes.SectionName = 'TCO_FavoriteReports\grFavoriteReports'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsFavoriteReports
            MultiSelectOptions = [msoShiftSelect]
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            ReadOnly = True
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
    object tsReports: TTabSheet
      Caption = 'Company Reports'
      object sbCompany: TScrollBox
        Left = 0
        Top = 0
        Width = 886
        Height = 657
        Align = alClient
        TabOrder = 0
        object fpCompany: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 262
          Height = 352
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpCompany'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Company'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grReports: TevDBCheckGrid
            Left = 12
            Top = 35
            Width = 226
            Height = 294
            DisableThemesInTitle = False
            Selected.Strings = (
              'ndescription'#9'255'#9'Report Name'#9'F')
            IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
            IniAttributes.SectionName = 'TCO_FavoriteReports\grReports'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsReports
            MultiSelectOptions = [msoShiftSelect]
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            ReadOnly = True
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
    object tsDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 1
      object pnlDetail: TevPanel
        Left = 0
        Top = 0
        Width = 886
        Height = 658
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object sbDetails: TScrollBox
          Left = 0
          Top = 0
          Width = 886
          Height = 658
          Align = alClient
          TabOrder = 0
          object fpDetails: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 262
            Height = 352
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'fpDetails'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Details'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object lblDateRange: TLabel
              Left = 80
              Top = 91
              Width = 65
              Height = 13
              Caption = 'lblDateRange'
            end
            object Label1: TLabel
              Left = 12
              Top = 90
              Width = 64
              Height = 13
              Caption = 'Date Range: '
            end
            object rgTimePeriods: TevRadioGroup
              Left = 12
              Top = 118
              Width = 229
              Height = 212
              Caption = 'Time Periods'
              Items.Strings = (
                'Last Waiting Payroll'
                'Last Processed Payroll'
                'Current Month '
                'Current Quarter '
                'Current Year'
                'Prior Year to Date')
              TabOrder = 0
              OnClick = rgTimePeriodsClick
            end
            object evPeriodYear: TevComboBox
              Left = 129
              Top = 272
              Width = 88
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              Enabled = False
              ItemHeight = 13
              ItemIndex = 1
              TabOrder = 1
              Text = 'To Runtime'
              OnClick = evPeriodYearClick
              Items.Strings = (
                'To The end'
                'To Runtime')
            end
            object evPeriodQuarter: TevComboBox
              Left = 129
              Top = 239
              Width = 88
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              Enabled = False
              ItemHeight = 13
              ItemIndex = 1
              TabOrder = 2
              Text = 'To Runtime'
              OnClick = evPeriodQuarterClick
              Items.Strings = (
                'To The end'
                'To Runtime')
            end
            object evPeriodMonth: TevComboBox
              Left = 129
              Top = 206
              Width = 88
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              Enabled = False
              ItemHeight = 13
              ItemIndex = 1
              TabOrder = 3
              Text = 'To Runtime'
              OnClick = evPeriodMonthClick
              Items.Strings = (
                'To The end'
                'To Runtime')
            end
            object chbQueue: TevCheckBox
              Left = 12
              Top = 64
              Width = 135
              Height = 17
              Caption = 'Run through the Queue'
              Checked = True
              State = cbChecked
              TabOrder = 4
            end
            object cbDestination: TevComboBox
              Left = 12
              Top = 35
              Width = 109
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 5
              Items.Strings = (
                'Print'
                'Preview')
            end
            object btnRun: TevBitBtn
              Left = 140
              Top = 35
              Width = 101
              Height = 21
              Caption = 'Run Report(s)'
              TabOrder = 6
              OnClick = btnRunClick
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
                CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
                CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
                5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
                A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
                5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
                FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
                B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
                6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
                FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
                B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
                64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
                CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
                B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
                5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
                00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
                BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
                57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
                00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
                BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
                82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
                60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
                BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
                83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
                00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
                C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
                4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
                FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
                C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
                4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
                FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
                C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
                49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
                FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
                CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
                45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
                FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
                DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
                3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
                FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
                EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
                8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
              NumGlyphs = 2
              Margin = 0
            end
          end
        end
      end
    end
  end
  object dsReports: TevDataSource
    Left = 68
    Top = 345
  end
  object dsFavoriteReports: TevDataSource
    Left = 112
    Top = 345
  end
end
