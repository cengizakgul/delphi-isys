unit SPD_EDIT_CL_ADHOC_REPORTS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SFrameEntry, DB, Wwdatsrc, ISBasicClasses, 
  ComCtrls, SDDClasses, SDataStructure, Grids, Wwdbigrd, EvContext,
  Wwdbgrid, StdCtrls, Mask, wwdbedit, kbmMemTable, ISKbmMemDataSet,
  EvUtils, EvConsts, Buttons, ExtCtrls, wwdblook, SReportSettings, DBCtrls,
  SSecurityInterface, EvTypes, ISDataAccessComponents, SPayrollReport,
  EvDataAccessComponents, SPD_EDIT_CO_BASE, SDataDictclient, SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_ADHOC_REPORTS = class(TEDIT_CO_BASE)
    csReports: TevClientDataSet;
    evPanel1: TevPanel;
    chbPreview: TevCheckBox;
    chbQueue: TevCheckBox;
    Button3: TevBitBtn;
    grReports: TevDBGrid;
    TabSheet2: TTabSheet;
    evDataSource1: TevDataSource;
    sbAdHocReportsBrowse: TScrollBox;
    pnlBrowseAdHocBorder: TevPanel;
    pnlBrowseAdHocLeft: TevPanel;
    evSplitter1: TevSplitter;
    pnlBrowseAdHocRight: TevPanel;
    fpAdHocCompany: TisUIFashionPanel;
    pnlFPAdHocCompanyBody: TevPanel;
    fpAdHocReport: TisUIFashionPanel;
    pnlfpAdHocReportBody: TevPanel;
    sbAdHocDetail: TScrollBox;
    fpAdHocDetail: TisUIFashionPanel;
    Label1: TevLabel;
    evLabel1: TevLabel;
    wwDBEdit1: TevDBEdit;
    cbReport: TevDBLookupCombo;
    btnInpForm: TevBitBtn;
    memNote: TEvDBMemo;
    gbASCII: TevGroupBox;
    sbBrowse: TevSpeedButton;
    dbeFile: TevDBEdit;
    chbAddFile: TevCheckBox;
    chbDuplexing: TevCheckBox;
    procedure cbReportChange(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure btnInpFormClick(Sender: TObject);
    procedure sbBrowseClick(Sender: TObject);
    procedure dbeFileChange(Sender: TObject);
    procedure chbAddFileClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure chbDuplexingClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    SY_DS: TevClientDataSet;
    FInParams: TrwReportParams;
    FRunParams: TevPrRepPrnSettings;
    procedure CreateReportSourceDataSet;
    procedure EditMode(AEnabled: Boolean);
    procedure SyncRunParams;
    procedure OnFilterCoReports(ADataSet: TDataSet; var Accept: Boolean);
  protected
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure SetReadOnly(Value: Boolean); override;
    procedure AfterDataSetsReopen; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   Activate; override;
    procedure   DeActivate; override;
    procedure   ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses SPD_EDIT_Open_BASE;

{$R *.dfm}

{ TEDIT_CL_ADHOC_REPORTS }

procedure TEDIT_CL_ADHOC_REPORTS.Activate;
begin
  ctx_RWLocalEngine.BeginWork;
  SY_DS := nil;
  CreateReportSourceDataSet;
  DM_CLIENT.CO_REPORTS.OnFilterRecord := OnFilterCoReports;
  wwdsDetailDataChange(nil, nil);
  inherited;
  //to hide FavoriteReport icon
  pnlFavoriteReport.visible := False;
end;


function TEDIT_CL_ADHOC_REPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CO_REPORTS;
end;

procedure TEDIT_CL_ADHOC_REPORTS.cbReportChange(Sender: TObject);
begin
  EditMode(True);
  btnInpForm.Enabled := (cbReport.LookupValue <> '');

  if not gbASCII.Enabled and (dbeFile.Text <> '') then
    dbeFile.Text := '';

  if wwdsDetail.DataSet.State <> dsBrowse then
    DM_CLIENT.CO_REPORTS.CUSTOM_NAME.AsString := csReports.FieldByName('cName').AsString;
end;

procedure TEDIT_CL_ADHOC_REPORTS.CreateReportSourceDataSet;

  procedure AddDBReports;
  var
    KeyFld, h, h1: String;
    DS: TevClientDataSet;
  begin
    KeyFld := '_REPORT_WRITER_REPORTS_NBR';
    KeyFld := 'SY' + KeyFld;
    DS := SY_DS;

    DS.First;
    while not DS.Eof do
    begin
      if DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Locate('SY_REPORT_WRITER_REPORTS_NBR', DS.FieldByName(KeyFld).AsInteger, []) then
      begin
        h1 := DS.FieldByName('report_description').AsString;
        h := h1 + ' (S' + DS.FieldByName(KeyFld).AsString + ')';
        csReports.Append;
        csReports.FieldByName('nbr').AsInteger := DS.FieldByName(KeyFld).AsInteger;
        csReports.FieldByName('level').AsString := 'S';
        csReports.FieldByName('Key').AsString := DS.FieldByName(KeyFld).AsString + 'S';
        csReports.FieldByName('name').AsString := h;
        csReports.FieldByName('cName').AsString := h1;
        csReports.FieldByName('notes').Value := DS.FieldByName('notes').Value;
        csReports.FieldByName('Type').AsString := DS.FieldByName('REPORT_TYPE').AsString[1];
        csReports.Post;
      end;

      DS.Next;
    end;
  end;

begin
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Open;
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filter := 'SY_REPORT_GROUPS_NBR = 15';
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filtered := False;
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filtered := True;
  csReports.DisableControls;

  try
    if not Assigned(SY_DS) then
    begin
      SY_DS := TevClientDataSet.Create(nil);

      if ctx_DataAccess.SY_CUSTOM_VIEW.Active then
        ctx_DataAccess.SY_CUSTOM_VIEW.Close;

      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', 'SY_REPORT_WRITER_REPORTS_NBR, REPORT_DESCRIPTION, REPORT_TYPE, NOTES, Ancestor_Class_Name');
        SetMacro('TableName', 'SY_REPORT_WRITER_REPORTS');
        SetMacro('Condition', 'Ancestor_Class_Name <> ''''');
        ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.SY_CUSTOM_VIEW.Open;
        SY_DS.Data := ctx_DataAccess.SY_CUSTOM_VIEW.Data;
        ctx_DataAccess.SY_CUSTOM_VIEW.Close;
      end;
    end;

    csReports.Close;
    csReports.CreateDataSet;

    AddDBReports;

  finally
    csReports.EnableControls;
    DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filter := '';
    DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filtered := False;
  end;
end;

procedure TEDIT_CL_ADHOC_REPORTS.wwdsDetailDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;

  if (wwdsDetail.DataSet.State = dsBrowse) and csReports.Active then
  begin
    wwdsDetail.Tag := 1;
    try
      if csReports.Locate('key',
        DM_CLIENT.CO_REPORTS.REPORT_WRITER_REPORTS_NBR.AsString +
        DM_CLIENT.CO_REPORTS.REPORT_LEVEL.AsString, []) then
      begin
        cbReport.LookupField := 'key';   // bugfix, don't touch
        cbReport.LookupValue := csReports.FieldByName('key').AsString;
        btnInpForm.Enabled := True;
      end

      else
      begin
        cbReport.LookupValue :='';
        memNote.Clear;
        btnInpForm.Enabled := False;
      end;

      if not DM_CLIENT.CO_REPORTS.INPUT_PARAMS.IsNull then
        try
          FInParams.ReadFromBlobField(DM_CLIENT.CO_REPORTS.INPUT_PARAMS);
        except
          FInParams.Clear;
        end
      else
        FInParams.Clear;

      SyncRunParams;  
    finally
      wwdsDetail.Tag := 0;
    end;
  end;
end;

procedure TEDIT_CL_ADHOC_REPORTS.EditMode(AEnabled: Boolean);
begin
  if wwdsDetail.Tag = 0 then
    if (wwdsDetail.DataSet.State = dsBrowse) and AEnabled then
      wwdsDetail.DataSet.Edit
    else if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) and not AEnabled then
      wwdsDetail.DataSet.Cancel;
end;

procedure TEDIT_CL_ADHOC_REPORTS.btnInpFormClick(Sender: TObject);
var
  Res: TModalResult;
begin
  try
    FInParams.Add('SetupMode', True);
    FInParams.Add('ReportName', DM_CLIENT.CO_REPORTS.CUSTOM_NAME.AsString);
    FInParams.Add('Clients', VarArrayOf([ctx_DataAccess.ClientID]), False);
    FInParams.Add('Companies', VarArrayOf([DM_COMPANY.CO.CO_NBR.AsInteger]), False);
    FInParams.Add('ClientCompany', VarArrayOf([IntToStr(ctx_DataAccess.ClientID) + ':' + DM_COMPANY.CO.CO_NBR.AsString]), False);
    FInParams.Add('ReportName', DM_CLIENT.CO_REPORTS.CUSTOM_NAME.AsString);
    Res := ctx_RWLocalEngine.ShowModalInputForm(csReports.FieldByName('NBR').AsInteger,
      csReports.FieldByName('LEVEL').AsString, FInParams, False);
  finally
    if not DM_COMPANY.CO.Active then
      DM_COMPANY.CO.Active := True;
  end;

  if not GetIfReadOnly then
  begin
    if (Res = mrOk) and (wwdsDetail.DataSet.State = dsBrowse) then
      wwdsDetail.DataSet.Edit;
  end
  else
    if not DM_CLIENT.CO_REPORTS.INPUT_PARAMS.IsNull then
      try
        FInParams.ReadFromBlobField(DM_CLIENT.CO_REPORTS.INPUT_PARAMS);
      except
        FInParams.Clear;
      end
    else
      FInParams.Clear;
end;

procedure TEDIT_CL_ADHOC_REPORTS.SetReadOnly(Value: Boolean);
begin
  inherited;
  cbReport.ReadOnly := cbReport.ReadOnly or (SecurityState <> stEnabled);
end;

constructor TEDIT_CL_ADHOC_REPORTS.Create(AOwner: TComponent);
begin
  inherited;
  FInParams := TrwReportParams.Create;

  FRunParams := TevPrRepPrnSettings.Create(nil);
  FRunParams.Copies := 0;
  FRunParams.Frequency := SCHED_FREQ_DAILY;  
end;

destructor TEDIT_CL_ADHOC_REPORTS.Destroy;
begin
  FreeAndNil(FRunParams);
  FreeAndNil(FInParams);
  inherited;
end;

procedure TEDIT_CL_ADHOC_REPORTS.DeActivate;
begin
  DM_CLIENT.CO_REPORTS.Filtered := False;
  DM_CLIENT.CO_REPORTS.OnFilterRecord := nil;  
  ctx_RWLocalEngine.EndWork;
  FreeAndNil(SY_DS);
  inherited;
end;

procedure TEDIT_CL_ADHOC_REPORTS.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  if Kind = 10 then
  begin
    FInParams.Duplexing := chbDuplexing.Checked;
    FInParams.FileName := dbeFile.Text;

    if cbReport.LookupValue <> '' then
    begin
      DM_CLIENT.CO_REPORTS.REPORT_WRITER_REPORTS_NBR.AsInteger := csReports.FieldByName('NBR').AsInteger;
      DM_CLIENT.CO_REPORTS.REPORT_LEVEL.AsString := csReports.FieldByName('LEVEL').AsString;
    end
    else
    begin
      DM_CLIENT.CO_REPORTS.REPORT_WRITER_REPORTS_NBR.Clear;
      DM_CLIENT.CO_REPORTS.REPORT_LEVEL.Clear;
    end;

    FInParams.SaveToBlobField(DM_CLIENT.CO_REPORTS.INPUT_PARAMS);
  end;
end;

procedure TEDIT_CL_ADHOC_REPORTS.sbBrowseClick(Sender: TObject);
var
  TempStr: String;
begin
  inherited;
  TempStr := RunFolderDialog('Please, select an output path:', dbeFile.Text);
  dbeFile.Text := TempStr;
end;

procedure TEDIT_CL_ADHOC_REPORTS.dbeFileChange(Sender: TObject);
var
   h: String;
begin
  EditMode(True);

  h := Trim(dbeFile.Text);

  if Length(h) = 0 then
    chbAddFile.Checked := False
  else
    chbAddFile.Checked := (h[Length(h)] = '+');
end;

procedure TEDIT_CL_ADHOC_REPORTS.chbAddFileClick(Sender: TObject);
var
  h: string;
begin
  EditMode(True);
  
  h := Trim(dbeFile.Text);
  if h = '' then
  begin
    chbAddFile.Checked := False;
    Exit;
  end;

  if Length(h) = 0 then
    Exit;

  if h[Length(h)] = '+' then
    Delete(h, Length(h), 1);

  if chbAddFile.Checked then
    h := h + '+';

  dbeFile.Text := h;
end;

procedure TEDIT_CL_ADHOC_REPORTS.SyncRunParams;
begin
  chbDuplexing.Checked := FInParams.Duplexing;
  dbeFile.Text := FInParams.FileName;
  chbAddFile.Checked := FInParams.AddToExistFile;
end;

procedure TEDIT_CL_ADHOC_REPORTS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;

  if wwdsDetail.DataSet.State = dsInsert then
  begin
    SyncRunParams;
    FRunParams.SaveToBlobField(DM_CLIENT.CO_REPORTS.RUN_PARAMS);
    FInParams.Clear;
    FInParams.SaveToBlobField(DM_CLIENT.CO_REPORTS.INPUT_PARAMS);
    cbReport.LookupValue :='';
    memNote.Clear;
    btnInpForm.Enabled := False;
  end;
end;

procedure TEDIT_CL_ADHOC_REPORTS.chbDuplexingClick(Sender: TObject);
begin
  EditMode(True);
end;

procedure TEDIT_CL_ADHOC_REPORTS.Button3Click(Sender: TObject);
var
  Params: TrwReportParams;
  prv: TrwReportDestination;
  mr: TModalResult;
  rn: String;
begin
  if chbPreview.Checked then
    prv := rdtPreview
  else
    prv := rdtPrinter;

  Params := TrwReportParams.Create;
  try
    rn := DM_CLIENT.CO_REPORTS.CUSTOM_NAME.AsString +
      ' (' + DM_CLIENT.CO_REPORTS.REPORT_LEVEL.AsString +
      DM_CLIENT.CO_REPORTS.REPORT_WRITER_REPORTS_NBR.AsString + ')';


    Params.Assign(FInParams);
    Params.Add('SetupMode', False);
    Params.Add('ReportName', rn);
    Params.Add('Clients', VarArrayOf([ctx_DataAccess.ClientID]), False);
    Params.Add('Companies', VarArrayOf([DM_COMPANY.CO.CO_NBR.AsInteger]), False);
    Params.Add('ClientCompany', VarArrayOf([IntToStr(ctx_DataAccess.ClientID) + ':' + DM_COMPANY.CO.CO_NBR.AsString]), False);

    mr := ctx_RWLocalEngine.ShowModalInputForm(
      DM_CLIENT.CO_REPORTS.REPORT_WRITER_REPORTS_NBR.AsInteger,
      DM_CLIENT.CO_REPORTS.REPORT_LEVEL.AsString,
      Params, True, False);

    if mr = mrOk then
    begin
      ctx_RWLocalEngine.StartGroup;
      try
        ctx_RWLocalEngine.CalcPrintReport(
          DM_CLIENT.CO_REPORTS.REPORT_WRITER_REPORTS_NBR.AsInteger,
          DM_CLIENT.CO_REPORTS.REPORT_LEVEL.AsString,
          Params, rn);
      finally
        if chbQueue.Checked then
          ctx_RWLocalEngine.EndGroupForTask(prv)        
        else
          ctx_RWLocalEngine.EndGroup(prv);
      end;
    end;

  finally
    FreeAndNil(Params);
  end;
end;

procedure TEDIT_CL_ADHOC_REPORTS.AfterDataSetsReopen;
begin
  inherited;
  DM_CLIENT.CO_REPORTS.Filtered := True;
end;

procedure TEDIT_CL_ADHOC_REPORTS.OnFilterCoReports(ADataSet: TDataSet; var Accept: Boolean);
begin
  Accept := csReports.Locate('nbr;level', VarArrayOf([DM_CLIENT.CO_REPORTS.REPORT_WRITER_REPORTS_NBR.AsInteger, DM_CLIENT.CO_REPORTS.REPORT_LEVEL.AsString]), []);
end;

initialization
  RegisterClass(TEDIT_CL_ADHOC_REPORTS);

end.
