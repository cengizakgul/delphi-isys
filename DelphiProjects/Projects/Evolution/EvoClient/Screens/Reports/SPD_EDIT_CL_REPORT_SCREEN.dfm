inherited EDIT_CL_REPORT_SCREEN: TEDIT_CL_REPORT_SCREEN
  Width = 660
  Height = 446
  object gbTop: TevGroupBox [0]
    Left = 0
    Top = 55
    Width = 660
    Height = 391
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object pnlDateRange: TevPanel
      Left = 2
      Top = 15
      Width = 656
      Height = 116
      Align = alTop
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnResize = pnlDateRangeResize
      object evLabel1: TevLabel
        Left = 914
        Top = 0
        Width = 39
        Height = 13
        Caption = 'Grids     '
        Visible = False
      end
      object pcDateOptions: TevPageControl
        Left = 0
        Top = 0
        Width = 656
        Height = 116
        ActivePage = tsDateRange
        Align = alClient
        TabOrder = 0
        OnChange = pcDateOptionsChange
        object tsDateRange: TTabSheet
          Caption = 'Date Range'
          DesignSize = (
            648
            88)
          object evBegLabel: TevLabel
            Left = 8
            Top = 0
            Width = 62
            Height = 13
            Caption = 'Begin Date   '
          end
          object evEndLabel: TevLabel
            Left = 100
            Top = 0
            Width = 51
            Height = 13
            Caption = 'End Date  '
          end
          object cbDateFrom: TevDateTimePicker
            Left = 8
            Top = 18
            Width = 85
            Height = 21
            Date = 36601.619315972200000000
            Time = 36601.619315972200000000
            TabOrder = 0
          end
          object cbDateTill: TevDateTimePicker
            Left = 100
            Top = 18
            Width = 85
            Height = 21
            Date = 36891.619596412000000000
            Time = 36891.619596412000000000
            TabOrder = 1
          end
          object rgCheckDate: TevRadioGroup
            Left = 8
            Top = 40
            Width = 178
            Height = 38
            Color = clBtnFace
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Check Date'
              'Batch Period')
            ParentColor = False
            TabOrder = 2
            OnClick = rgDateTypeClick
          end
          object btnDateList: TevBitBtn
            Left = 1287
            Top = 56
            Width = 100
            Height = 25
            Anchors = [akRight, akBottom]
            Caption = 'Refresh '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            OnClick = btnDateListClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5DADADACC
              CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFDDDDDDC8BEB1BD9768B88444B98545FFFFFFFFFFFFCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCDCDCDFFFFFFFFFFFFFFFFFFFFFFFFDDDDDDBCBCBB9292927E
              7E7D7F7E7EFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCFFFFFFFFFFFF
              E1E1E1BF9C72BE8A4ADEAC66F4C57BB88343FFFFFFFFFFFFB98545B78343B681
              42B78242B98444BB8849FFFFFFFFFFFFFFFFFFE1E1E1979797838383A4A4A4BD
              BCBC7E7E7DFFFFFFFFFFFF7F7E7E7D7C7C7C7B7B7D7C7C7F7E7EFFFFFFF9F9F9
              C2A37EC18C4CECBC72F1C581F8D39DB78242FFFFFFFFFFFFB78343F9C97EF6C6
              7BF5CC8CD2A771FFFFFFFFFFFFFFFFFFFAF9F99F9F9E868686B3B3B3BDBCBCCD
              CCCC7D7C7CFFFFFFFFFFFF7D7C7CC0C0C0BEBDBDC4C4C4A1A1A1FFFFFFD9D3CB
              BA8646E9B86DEDC07DEFCE9CD5AC71B88343FFFFFFFFFFFFB68141F2C47EF0C4
              82EAB86FBC8947E5E5E5FFFFFFFFFFFFD2D2D180807FB0AFAFB8B8B8C8C8C7A5
              A5A57E7E7DFFFFFFFFFFFF7C7B7BBCBCBBBCBCBBB0AFAF828282FFFFFFC09E74
              D2A059E6B368EDCC9AC08F51D8B995F6EFE6FFFFFFFFFFFFB78241F5D9ACF0CF
              9FEAB86CD4A25CC3A988FFFFFFFFFFFF999999989898AAAAAAC6C6C6888888B5
              B5B5EEEEEEFFFFFFFFFFFF7C7B7BD3D3D3CAC9C9B0AFAF9A9A9AFFFFFFB88444
              DDAB61E2B167D4AB77D7BA95FFFFFFFFFFFFFFFFFFFFFFFFB98443C6995FD7B2
              82E6B266E0AE65B98547FFFFFFFFFFFF7E7E7DA3A3A3A9A8A8A5A5A5B5B5B5FF
              FFFFFFFFFFFFFFFFFFFFFF7E7E7D939393ACACACAAA9A9A6A6A6FFFFFFB88344
              DFAB5FE2B774C6995FFEFDFCFEFEFEFFFFFFFFFFFFFFFFFFBD8B4DFDFBF9C697
              5FE6BB79E5B36AB78344FFFFFFFFFFFF7E7E7DA3A3A3AFAFAF939393FEFDFDFE
              FEFEFFFFFFFFFFFFFFFFFF868585FBFBFB919191B3B3B3ABABABFFFFFFB88343
              DFB373DBA85EB78243FDFDFDCECECDFFFFFFFFFFFFFFFFFFFFFEFDFEFEFEB682
              42DFAC63E4B877B78243FFFFFFFFFFFF7E7E7DABABABA1A0A07D7C7CFEFDFDCE
              CDCDFFFFFFFFFFFFFFFFFFFEFEFEFEFEFE7C7B7BA4A4A4B1B0B0FFFFFFB88342
              E2BB83D59F52BF8B48D5D3D0BC894BFFFFFFFFFFFFFFFFFFFFFFFFE3E2E1C08C
              49D9A557E5C087B78242FFFFFFFFFFFF7D7C7CB4B4B4979797858484D3D2D283
              8383FFFFFFFFFFFFFFFFFFFFFFFFE2E2E28685859D9D9DB9B9B9FFFFFFB98547
              E1C192CF9A4AC18C48C0955EBA8545FFFFFFFFFFFFF8F8F8E2E2E2C1A481C792
              4BD39E4FE3C395B88241FFFFFFFFFFFF80807FBBBBBB9292928685858F8F8F80
              807FFFFFFFFFFFFFF9F9F8E2E2E2A09F9F8B8B8B969595BEBDBDFFFFFFDCC1A0
              D6B386D4A661C99243E4C79DB98443FFFFFFFFFFFFCAC3BAC2A581BC8746CD98
              4AD8AC6DD9B98ECFAB80FFFFFFFFFFFFBDBCBCAEAEAE9F9F9E8A8A8AC2C2C17E
              7E7DFFFFFFFFFFFFC2C2C1A1A0A0818080909090A5A5A5B4B4B4FFFFFFFFFFFF
              BD8D4FD1A668C48B39D9B580B88241FFFFFFFFFFFFBA8646C28D45C99344D09E
              56E7CEAABD8B4FF7F1E9FFFFFFFFFFFFFFFFFF8686869F9F9E838383AFAFAF7D
              7C7CFFFFFFFFFFFF80807F8686868B8B8B979696CAC9C9868585FFFFFFCDCDCD
              CCA472E6CFACE9D3B2EAD5B4B88341FFFFFFFFFFFFB98444CD9C54DAB57FEAD5
              B5C69A64D6B791FFFFFFFFFFFFFFFFFFCDCCCC9F9F9ECACACACFCFCFD1D1D17D
              7C7CFFFFFFFFFFFF7F7E7E959594AFAFAFD1D1D1959594B2B2B2FFFFFFBC8849
              B98444B88241B8813FB88241BA8545FFFFFFFFFFFFB98342EAD5B6DBBD95C090
              54D1AD81FFFFFFFFFFFFFFFFFFFFFFFF8282827F7E7E7D7C7C7C7B7B7D7C7C80
              807FFFFFFFFFFFFF7E7E7DD1D1D1B8B8B88A8A8AA7A7A7FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBA8544BB8849CBA374F1E7
              DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF7F7E7E8281819E9E9EE5E5E5FFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object btnApplyPeriod: TevButton
            Left = 196
            Top = 18
            Width = 85
            Height = 21
            Caption = 'Apply Period'
            TabOrder = 4
            OnClick = btnApplyPeriodClick
            Color = clBlack
            Margin = 0
          end
        end
        object tsQuarters: TTabSheet
          Caption = 'Quarters'
          ImageIndex = 1
          object lblYear: TevLabel
            Left = 12
            Top = 0
            Width = 22
            Height = 13
            Caption = 'Year'
          end
          object cbQYears: TevComboBox
            Left = 9
            Top = 18
            Width = 86
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 1
            OnChange = cbQYearsChange
          end
          object cblQuarter: TevCheckListBox
            Left = 192
            Top = 18
            Width = 304
            Height = 59
            Columns = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemHeight = 13
            Items.Strings = (
              '1st quarter 2011'
              '2nd quarter 2011'
              '3rd quarter 2011'
              '4th quarter 2011'
              '1st quarter 2012'
              '2nd quarter 2012'
              '3rd quarter 2012'
              '4th quarter 2012'
              '1st quarter 2013'
              '2nd quarter 2013'
              '3rd quarter 2013'
              '4th quarter 2013')
            ParentFont = False
            TabOrder = 2
          end
          object btnAllOrNone: TevButton
            Left = 100
            Top = 18
            Width = 85
            Height = 21
            Caption = 'Select All'
            TabOrder = 3
            OnClick = btnAllOrNoneClick
            Color = clBlack
            Margin = 0
          end
          object cbPriorYear: TevCheckBox
            Left = 192
            Top = 0
            Width = 97
            Height = 17
            Caption = 'Prior Year'
            Checked = True
            State = cbChecked
            TabOrder = 4
            OnClick = cbPriorYearClick
          end
          object cbNextYear: TevCheckBox
            Left = 430
            Top = 0
            Width = 70
            Height = 17
            Caption = 'Next Year'
            TabOrder = 5
            OnClick = cbPriorYearClick
          end
          object edHiddenField: TEdit
            Left = 3000
            Top = 32
            Width = 121
            Height = 21
            TabOrder = 0
            Text = 'edHiddenField'
          end
        end
      end
      object rgDateType: TevRadioGroup
        Left = 856
        Top = 56
        Width = 230
        Height = 36
        Color = clBtnFace
        Columns = 2
        Enabled = False
        ItemIndex = 0
        Items.Strings = (
          'Date Range'
          'Quarters')
        ParentColor = False
        TabOrder = 1
        Visible = False
        OnClick = rgDateTypeClick
      end
      object cbGrids: TevComboBox
        Left = 853
        Top = 15
        Width = 100
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 1
        TabOrder = 2
        Text = 'Check lines'
        Visible = False
        OnChange = cbGridsChange
        Items.Strings = (
          'Checks'
          'Check lines')
      end
    end
    object pgList: TevPageControl
      Left = 2
      Top = 131
      Width = 656
      Height = 258
      ActivePage = tsClient
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Images = ilTabImages
      ParentFont = False
      TabOrder = 1
      OnChange = pgListChange
      object tsClient: TTabSheet
        Caption = 'Client '
        object sbClient: TScrollBox
          Left = 0
          Top = 0
          Width = 648
          Height = 229
          Align = alClient
          TabOrder = 0
          object pnlClientBorder: TevPanel
            Left = 0
            Top = 0
            Width = 644
            Height = 225
            Align = alClient
            BorderWidth = 8
            TabOrder = 0
            object fpClient: TisUIFashionPanel
              Left = 9
              Top = 9
              Width = 626
              Height = 207
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 0
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Client'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object grdClientFilter: TevDBGrid
                Left = 12
                Top = 36
                Width = 594
                Height = 151
                DisableThemesInTitle = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_REPORT_SCREEN\grdClientFilter'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsClientFilter
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                UseTFields = True
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
      object tsCompany: TTabSheet
        Caption = 'Company '
        ImageIndex = 1
        object sbCompany: TScrollBox
          Left = 0
          Top = 0
          Width = 648
          Height = 229
          Align = alClient
          TabOrder = 0
          object pnlCompanyBorder: TevPanel
            Left = 0
            Top = 0
            Width = 644
            Height = 225
            Align = alClient
            BorderWidth = 8
            TabOrder = 0
            object fpCompany: TisUIFashionPanel
              Left = 9
              Top = 9
              Width = 626
              Height = 207
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 0
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Company'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object grdCompFilter: TevDBCheckGrid
                Left = 12
                Top = 36
                Width = 594
                Height = 151
                DisableThemesInTitle = False
                IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
                IniAttributes.SectionName = 'TEDIT_CL_REPORT_SCREEN\grdCompFilter'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsCompanyFilter
                MultiSelectOptions = [msoShiftSelect]
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                ReadOnly = True
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
      object tsDBDT: TTabSheet
        Caption = 'Home DBDT'
        ImageIndex = 2
        object sbDBDT: TScrollBox
          Left = 0
          Top = 0
          Width = 648
          Height = 229
          Align = alClient
          TabOrder = 0
          object pnlDBDTBorder: TevPanel
            Left = 0
            Top = 0
            Width = 644
            Height = 225
            Align = alClient
            BorderWidth = 8
            TabOrder = 0
            object fpDBDT: TisUIFashionPanel
              Left = 9
              Top = 9
              Width = 626
              Height = 207
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 0
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Home DBDT'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object grdDBDT: TevDBCheckGrid
                Left = 12
                Top = 36
                Width = 594
                Height = 151
                DisableThemesInTitle = False
                IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
                IniAttributes.SectionName = 'TEDIT_CL_REPORT_SCREEN\grdDBDT'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsDBDT
                MultiSelectOptions = [msoShiftSelect]
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                ReadOnly = True
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
      object tsEE: TTabSheet
        Caption = 'Employee '
        ImageIndex = 3
        object sbEmployee: TScrollBox
          Left = 0
          Top = 0
          Width = 783
          Height = 379
          Align = alClient
          TabOrder = 0
          object pnlEmployeeBorder: TevPanel
            Left = 0
            Top = 0
            Width = 779
            Height = 375
            Align = alClient
            BorderWidth = 8
            TabOrder = 0
            object fpEmployee: TisUIFashionPanel
              Left = 9
              Top = 9
              Width = 761
              Height = 357
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 0
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Employee'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object grEEList: TevDBCheckGrid
                Left = 12
                Top = 36
                Width = 729
                Height = 301
                DisableThemesInTitle = False
                IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
                IniAttributes.SectionName = 'TEDIT_CL_REPORT_SCREEN\grEEList'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsEEList
                MultiSelectOptions = [msoShiftSelect]
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                ReadOnly = True
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
      object tsPayroll: TTabSheet
        Caption = 'Payroll '
        ImageIndex = 4
        object sbPayroll: TScrollBox
          Left = 0
          Top = 0
          Width = 648
          Height = 229
          Align = alClient
          TabOrder = 0
          object pnlPayrollBorder: TevPanel
            Left = 0
            Top = 0
            Width = 644
            Height = 225
            Align = alClient
            BorderWidth = 8
            TabOrder = 0
            object fpPayroll: TisUIFashionPanel
              Left = 9
              Top = 9
              Width = 626
              Height = 207
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 0
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Payroll'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object grCheckPeriod: TevDBCheckGrid
                Left = 12
                Top = 36
                Width = 594
                Height = 151
                DisableThemesInTitle = False
                IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
                IniAttributes.SectionName = 'TEDIT_CL_REPORT_SCREEN\grCheckPeriod'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsCheckPeriod
                MultiSelectOptions = [msoShiftSelect]
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                ReadOnly = True
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
      object tsGrids: TTabSheet
        Caption = 'Grid '
        ImageIndex = 5
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 0
          Width = 648
          Height = 229
          Align = alClient
          TabOrder = 0
          object evPanel1: TevPanel
            Left = 0
            Top = 0
            Width = 644
            Height = 225
            Align = alClient
            BorderWidth = 8
            TabOrder = 0
            object fpGrid: TisUIFashionPanel
              Left = 9
              Top = 9
              Width = 626
              Height = 207
              Align = alClient
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 0
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Grid'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object pnlFPGridBody: TevPanel
                Left = 12
                Top = 36
                Width = 594
                Height = 151
                Align = alClient
                BevelOuter = bvNone
                ParentColor = True
                TabOrder = 0
                inline QEEGrid: TQEEGrid
                  Left = 0
                  Top = 0
                  Width = 594
                  Height = 151
                  Align = alClient
                  TabOrder = 0
                  inherited pnlTop: TevPanel
                    Width = 594
                    ParentColor = True
                  end
                  inherited pnlGrid: TevPanel
                    Width = 594
                    Height = 126
                    inherited dgQEE: TevDBGrid
                      Width = 729
                      Height = 276
                      PaintOptions.AlternatingRowColor = 14544093
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object pnlDarkHeader: TevPanel [1]
    Left = 0
    Top = 0
    Width = 660
    Height = 55
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvLowered
    Color = 4276545
    TabOrder = 1
    object lablClient: TevLabel
      Left = 8
      Top = 8
      Width = 38
      Height = 13
      Caption = 'CLIENT'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -9
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblClientName: TevLabel
      Left = 58
      Top = 8
      Width = 5
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 24
    Top = 240
  end
  inherited wwdsDetail: TevDataSource
    Left = 24
    Top = 312
  end
  inherited wwdsList: TevDataSource
    Left = 26
    Top = 274
  end
  object cdCompanyFilter: TevClientDataSet
    Left = 640
    Top = 240
  end
  object dsCompanyFilter: TevDataSource
    DataSet = cdCompanyFilter
    Left = 680
    Top = 240
  end
  object cdClientFilter: TevClientDataSet
    Left = 640
    Top = 208
  end
  object dsClientFilter: TevDataSource
    DataSet = cdClientFilter
    OnDataChange = dsClientFilterDataChange
    Left = 680
    Top = 208
  end
  object cdPayrollFilter: TevClientDataSet
    Left = 536
    Top = 208
  end
  object dsPayrollFilter: TevDataSource
    DataSet = cdPayrollFilter
    Left = 568
    Top = 208
  end
  object cdCheckPeriod: TevClientDataSet
    Left = 104
    Top = 232
  end
  object dsCheckPeriod: TevDataSource
    DataSet = cdCheckPeriod
    Left = 136
    Top = 232
  end
  object dsEEList: TevDataSource
    DataSet = cdEEList
    Left = 136
    Top = 264
  end
  object cdEEList: TevClientDataSet
    Left = 104
    Top = 264
  end
  object cdDBDT: TevClientDataSet
    Left = 104
    Top = 296
  end
  object dsDBDT: TevDataSource
    DataSet = cdDBDT
    Left = 136
    Top = 296
  end
  object ilTabImages: TevImageList
    Left = 448
    Top = 16
    Bitmap = {
      494C010106000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DCDCDC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00DCDCDC00BF9A5000B67E0E00B47B0900B47A
      0700B47A0800B47B0800B47A0800B47A0800B47B0800B47A0800B47A0800B47B
      0800B47A0800B47B0900B67E0E00BF9A50000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008CBAA3006EB18F006FB28F0070B2
      8F006FB28D006FB284006FB0FF0079C1FB0079C1FB006FB0FF006FB284006FB2
      8D0070B390006FB28F006FB18F008CBAA300B67E0E00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B67E0E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006AAD8B00CAF4DB008CC7B50082BE
      A70093CBB90094CBB10068A8FE0092F4FB0092F4FB0068A8FE0094CBB10093CB
      BA007CBBA00085C0AC00CBF4DC006AAE8B00B47B0900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00D5BCA100FFFFFF00FFFFFF00D5BCA100FFFFFF00FFFFFF00D5BC
      A100FFFFFF00FFFFFF00FFFFFF00B47B09000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000066AA8600CAF2DC005FA27C004E93
      6800B9EBCD00BAEBC50062A3F9008DEAF9008DEAF90062A3F900BAEBC500B9EB
      CD004F926900599F7700CAF3DD0066AA8600B47A0700FFFFFF00FFFEFB00FFFE
      F900FFFFFE00D6BC9D00FFFFFF00FFFFFF00D6BD9F00FFFFFF00FFFFFF00D6BD
      9F00FFFFFF00FFFFFC00FFFFFF00B47A07000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000063A78200CDF2E00073B59300B3E7
      CD00B1E5C900B3E5C1005E9EF70089E5F90089E5F9005E9EF700B3E5C100B1E5
      C900B3E7CD0074B59300CDF2E00063A78200B47A0800FFFFFF00FFFFF900FFFF
      F900FFFFFD00D6BC9E00FFFFFF00FFFFFF00D9C3A800FFFFFF00FFFFFF00D9C3
      A800FFFFFF00FFFFFA00FFFFFF00B47A08000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000060A47F00D0F2E4006CAD8B00ABE2
      C80052956B00ADE3BF005C9BF50086DFF80086DFF8005C9BF500ADE3BF005295
      6B00ABE2C8006CAD8B00D0F2E40060A47F00B47B0800FFFFFF00D4B89600D5B9
      9700D7BC9B00D8BE9E00D7BD9D00D9C2A500B15A0200B25C0600B25C0600B15A
      0200D9C1A300D5B99800FFFFFF00B47B08000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005DA17B00D8F5E80067AA8500A7E0
      C600A5DFC300A7E0BB005998F30083DAF60083DAF6005998F300A7E0BB00A5DF
      C300A7E0C60067AA8500D8F5E8005DA17B00B47B0800FFFFFF00FFFCED00FFFD
      EF00FFFFF300D7BB9900FFFFF500FFFFFF00B25C0700EAD0AF00EAD0AF00B25C
      0700FFFFFE00FFFEF000FFFFFF00B47B08000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005B9F7800DFF7EF00539A70005EA2
      7D00A0DCC100A1DDB9005496F0007DD5F5007DD5F5005496F000A1DDB900A0DC
      C1005EA27D00559B7300DFF7EF005B9F7800B47B0800FFFFFF00FFFAE800FFFB
      E900FFFEED00D7B99500FFFFF000FFFFFA00B25D0800EAD0AF00EAD0AF00B25D
      0800FFFFF800FFFCEA00FFFFFF00B47B08000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000579D7400E5F9F400448C5E00478C
      61005B9F75005B9F6D004C90EE0074CBF30074CBF3004C90EE005B9F6D005B9F
      7500478C6100448C5E00E5F9F400579D7400B47B0900FFFFFF00D4B38A00D5B5
      8D00D7B89000D8BA9300D7B99200D9BE9A00B15B0500B25D0900B25D0900B15B
      0500D9BD9800D5B58C00FFFFFF00B47B09000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000569A7200E8F9F500E3F6F100E3F6
      F000E3F5EF00E4F7EA00C9D9FB00DDF4FD00DDF4FD00C9D9FB00E4F7EA00E3F5
      EF00E3F6F000E3F6F100E8F9F500569A7200B47B0900FFFFFF00FFF5DD00FFF6
      DF00FFF9E300D6B68E00FFFAE400FFFCE700D8BC9700FFFFEF00FFFFEF00D8BC
      9700FFFBE500FFF6DE00FFFFFF00B47B09000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008EBB9F007FC0A2009AD9BF0096D6
      BB0096D5B90098D7B50064A8F90086EAFB0086EAFB0064A8F90098D7B50096D5
      B90096D6BB009AD9BF007FC0A2008EBB9F00B47B0900FFFFFF00FFF5D600FFF6
      D800FFF9DD00D7B68A00FFFADE00FFFADE00D8B78B00FFFBE000FFFBE000D8B7
      8B00FFF9DC00FFF5D700FFFFFF00B47B09000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008EBA9F0093CBAC00BDEE
      D400BBECD100BEEECE0081BBE7007DD4F9007DD4F90081BBE700BEEECE00BBEC
      D100BDEED40093CBAC008EBA9F0000000000B57B0900FFFFFF00DDB17C00E0B4
      7E00E0B68200DEB78600DFB68300DFB68300DEB78600DFB68300DFB68300DEB7
      8600DFB58100DDB27C00FFFFFF00B57B09000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008AB79B005092
      69004F9268005092630052928A0070C0F70070C0F70052928A00509263004F92
      6800509269008AB79B000000000000000000B57C0900FFFFFF0044C3FF0049C5
      FF0047C6FF00E3B57E0046C7FF0046C7FF00E3B57E0046C7FF0046C7FF00E3B5
      7E0046C6FF0044C4FF00FFFFFF00B57C09000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B67E0E00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B67E0E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BE8C2700B67E0E00B67C0900B67B
      0700B67B0800B57B0800B67B0800B67B0800B57B0800B67B0800B67B0800B57B
      0800B67B0800B67C0900B67E0E00BE8C27000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DCDCDC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CFCFCF0000000000DCDCDC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00DCDCDC0000000000000000000000
      0000000000000000000000000000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      80008080800080808000808080008080800000000000F5E7DA00F0DDC800F0DD
      CA00F0DDCA00F0DDCA00F0DDCA00F0DECA00F0DECA00F0DECA00F0DECA00F0DE
      CA00F0DECA00F0DECA00F0DDC900FCF7F3000000000000000000C0C0C000B8B9
      BB00B9BCC000B8BCC000B6BBC200C6A57B00CC964E00CB954E00CB954E00CA95
      4E00CB954E00CB954F00CC975200CE9E60000000000094A9B5007C9AAA00809B
      AA00829BA900829BA900809BAA007B9AAA0092ABBA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000080808000C0C0
      C000C0C0C000C0C0C000C0C0C0008080800000000000E4BB9100E0A56400E2A9
      6C00E2A96C00E2A96B00E2A96B00E2A96A00E1A86900E1A86900E1A86900E1A7
      6800E1A76800E1A76800DCA06100F5E8DB000000000000000000B8B9BB000000
      0000B27A3D00B17B3F0000000000CB9247000000000000000000000000000000
      0000000000000000000000000000CC975200000000007D9BAB00BFD4E2002167
      94003FAAE2003FAAE20021679400BFD4E300789CB600CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00DCDCDC0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000080808000C0C0
      C000C0C0C000C0C0C000C0C0C0008080800000000000E9C7A300E6AE6E00EAB6
      7900EAB67900EAB67900E9B47800E9B37600E9B27400E8B17200E8B07100E8AF
      7000E8AF6F00E8AE6E00E2A96D00F7EEE5000000000000000000B7BCC100AF78
      3D00FFFBEB00FFF9E600AC794300C991470000000000B4B2AC00D2CEC800FEFB
      F200B4B2AB00D2CFC80000000000CA954E00000000007F9CAD00C7D9E400236A
      9F0044ABDF0044ABDF00236A9F00C5D9E600779BB500BC7B0000B67B0700B57B
      0900B57B0900B57C0900B67E0E00BF9A500000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000808080008080
      80008080800080808000808080008080800000000000F0DBC500E5B07100EBBB
      8000EBBB8000EBBA8000EAB97D00EAB67A00E9B47700E8B27400E8B17300E7AF
      7100E7AF7000E6AA6A00E3B58200FCF9F60000000000CCCCCC00B17B3F00FFFF
      FB00FAC37900FAC07500FFF9EA00C790470000000000FAF5E400FBF4E400F9F4
      E300FAF4E300F9F4E20000000000CA954E0000000000849FAD00CBDEE800206C
      A90043ABDE0043ABDE00206CA900C9DDE8007898AC0000000000000000000000
      0000000000000000000000000000B67E0E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C0008080800000000000FBF6F100E5BC8B00EDC0
      8600EDC18A00EEC28A00EDC28B00ECBF8800EBBD8300EAB87E00E9B47700E8B1
      7200E8AF7000E3AB6900EED6BB0000000000CCCCCC00B27D430000000000F8C0
      7200F5AD4C00F8AE4A00FFD59A00C690490000000000ADA79600CCC3B000CBC2
      B000F6EBD600ACA5940000000000CA954E00CCCCCC008DA3B000D0E3EC005F98
      C7001E6EB4001E6EB4005F98C700CEE1EB007F9BAB0000000000F0D9B700EDD9
      BA00ECD8B900EAD6B70000000000B57B090000000000000000008080800000FF
      0000008000008080800080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000F1DECB00E5BB
      8700EFC89300E9C18E00E7C39800E8C69E00E8C69E00E7C29500E5B98300E7B2
      7500E5AE6E00E5BE9200FCF9F50000000000B5844D0000000000F6B76000F19E
      2E00F7BA6800FBBF6E00FFC67600C6904B0000000000F0E3C400F0E4C600F0E4
      C700EFE3C600EFE2C40000000000CA954E002D75A8004E809F00D8EAF100D2E6
      F000D2EAF500D2EAF500D2E6F000D5E8F00040789B000860A100F2D6AB00D0AB
      7F00CEAA7F00E8D0A90000000000B47B0900000000008080800000FF000000FF
      000000FF00000080000080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000FFFEFE00EED8
      C000E6C19600EFDCC500F7EFE600F6EBDF00F4E8DB00F7EEE400F6ECE100E8C7
      9E00E5BF9400F9F2EB000000000000000000B5844D0000000000F2A13100F3A5
      3D00F7BE7100FBC57D00FEB95D00C6914D0000000000E8D9B200E8D9B400E8DA
      B400E8D9B400E8D9B20000000000CA954E00000000002A73AC00689BBD00DAEF
      FC00C3620E00C3620E00D9EEFB006096BB001165A500FFFAF800EBCF9D00E8D1
      A400E7D0A400E4CB9C0000000000B47B09000000000000FF000000FF000000FF
      000000FF000000FF00000080000080808000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000000000000000
      0000FBF7F300F2E0CE00E3B48300E0A76900E0A56600E0AA7000E8C8A500F9F2
      EB00FDFBF80000000000000000000000000000000000B27D440000000000F7B6
      5D00F7BB6900FACC8D00F9B04D00C69350000000000000000000000000000000
      0000000000000000000000000000CC96500000000000000000002573B200679F
      C800E1F7FF00E1F7FF00629BC5001165A900FAF1EB00EDECEE00E4C68E00C9A2
      6D00C8A26D00E1C48E0000000000B47B090000000000000000000000000000FF
      0000008000000000000080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000000000000000
      0000F2E0CD00E0A96C00E7AC6C00E8B07100E8B07200E7AE6E00E2A66400E4BC
      9000FDFAF8000000000000000000000000000000000000000000B17B3F000000
      0000F8BF7100F7C47E00F9C37800DAA05300C9924900C88F4300C88D4000C78C
      4000C78C3F00C88D4000CC924600D4A362000000000000000000000000001D73
      C0006BA3D20069A1CD001267AF00F2E9E100E5E2E000E0E1E500DFBF7D00DFC2
      8500DFC28500DDBD7D0000000000B57B090000000000000000000000000000FF
      0000008000000000000000800000008000000080000000800000008000000080
      000000800000008000000080000000800000000000000000000000000000FCF8
      F500E3B48200E7AF6F00E9B47700E9B37600E8B27400E8B17300E8B07100E1A4
      6300EFD9C1000000000000000000000000000000000000000000B6BBC100AE77
      3B0000000000F8BF7300FCDAAD0000000000AE783E00EFF5FC00EDF2F700ECF1
      F600ECF0F40000000000B3B8BE0000000000000000000000000000000000CB84
      0100126FC2000F68B600E8DFD800DCD8D500D6D5D500D4D6DB00D9B36600D9B5
      6C00D8B56B00D7B3660000000000B57B090000000000000000000000000000FF
      000000800000008000000080000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000800000000000000000000000000000F6EB
      E000E2AC7000EAB87C00EAB87C00EAB77B00E9B57900E9B37600E8B27400E5A9
      6800E6C098000000000000000000000000000000000000000000B6B8B9000000
      0000AB7336000000000000000000AC753800EDF1F600EDEEEF00ECECEC00EAEA
      EA00E8E8E80000000000B4B5B60000000000000000000000000000000000BC7F
      0A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B57C0B0000000000000000000000000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000800000000000000000000000000000F4E7
      D900E3AF7200EDBE8500ECBE8500EBBC8200EAB97E00E9B67A00E9B37600E6AC
      6B00E4BB90000000000000000000000000000000000000000000B5B5B5000000
      0000E7EBEF00AD753700AD753700E8EDF100E8EAEB009D9E9D00BDBDBC00BBBB
      BA00E5E5E40000000000B4B4B40000000000000000000000000000000000B67F
      0F00F9E2BF00E9B97000E9BE7900E8BE7900E8BD7900E8BD7900E7BD7900E7BD
      7900E7BC7700E6B66E00F7E0BE00B57E0F000000000000000000000000000000
      0000000000000000000000800000008000000080000000800000008000000080
      000000800000008000000080000000800000000000000000000000000000F9F1
      E900E5B88200EEC48C00EEC48D00EDC28A00ECBE8500EAB97E00EAB57900E5AB
      6A00EACBAA000000000000000000000000000000000000000000B5B5B5000000
      0000E1E1E200E3E5E700E3E5E700E2E3E300E3E2E100E4E3E200E4E3E200E3E2
      E100E1DFDE0000000000B5B5B50000000000000000000000000000000000B680
      1300F2D8AE00CD840800D08A1300D38F1D00D4932400D6962B00D99A3400DB9E
      3B00DCA24200DFA64900F1D7AC00B68013000000000000000000000000000000
      0000000000000000000080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C00080808000000000000000000000000000FEFE
      FD00EACCAB00EBC48E00F0CC9700EEC79100EDC18900EBBC8200EAB77A00E3B2
      7A00F6ECE0000000000000000000000000000000000000000000B6B6B6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B6B6B60000000000000000000000000000000000B882
      1700EED09C00ECCE9900ECCF9A00ECCE9A00ECCE9900ECCE9900EBCD9800EBCD
      9800EBCD9700EBCD9600EECF9A00B88217000000000000000000000000000000
      0000000000000000000080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000808080000000000000000000000000000000
      0000F9F3ED00E7C6A000EBC79600EECA9800EEC69200EABE8600E5B98300EFDB
      C400000000000000000000000000000000000000000000000000BEBEBE00B6B6
      B600B5B5B500B4B4B400B4B4B400B4B4B400B4B4B400B4B4B400B4B4B400B4B4
      B400B5B5B500B6B6B600BEBEBE0000000000000000000000000000000000D0AB
      6200B8821700B7811600B7811600B7811600B7811600B7811500B7811500B681
      1500B6811500B7811500B8821700D0AB62000000000000000000000000000000
      0000000000000000000080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000000000000000
      000000000000FCF9F600F0DDC800EACCAB00E8C8A400ECD2B400F6ECE1000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF8001000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000008001000000000000C003000000000000
      FFFF000000000000FFFF000000000000C000807F80008000C000807F00008000
      D2FE800000008000C0828000000080008082807E0000800120820042C000C001
      408200028000C003408280028000F007A0FEC002E400F007D000E002E400E007
      C905E002E000E007D605EFFEE000E007D005E000FC00E007D005E000FC00E007
      DFFDE000FC00F00FC001E000FC00F81F00000000000000000000000000000000
      000000000000}
  end
end
