// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_REPORTS;

interface

uses
  Windows, SDataStructure,  SPD_EDIT_CO_BASE, Menus, StdCtrls,
  ExtCtrls, Spin, CheckLst, wwdblook, DBCtrls, Wwdotdot, Wwdbcomb,
  Wwdbspin, Mask, wwdbedit, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, Controls, Classes, SReportSettings, Dialogs, EvUtils,
  SysUtils, EvConsts, SPD_EDIT_COPY, EvSecElement,
  SSecurityInterface, EvTypes, DBTables, Wwquery, EvCommonInterfaces,
  Wwtable, Graphics, ActnList, Printers, Forms, SPayrollReport,
  SPackageEntry, Variants, SFrameEntry, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, EvDataAccessComponents, EvContext,
  ISDataAccessComponents, SDataDictsystem, SDataDictbureau,
  SDataDictclient, SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit, isUIDBMemo, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton
  ,isBaseClasses
  ,EvStreamUtils
  ;

type
  TEDIT_CO_REPORTS = class(TEDIT_CO_BASE)
    tsCoReportsDetail: TTabSheet;
    wwDBGCoReports: TevDBGrid;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    csReports: TevClientDataSet;
    dsReports: TevDataSource;
    PopupMenu2: TPopupMenu;
    CopyReport1: TMenuItem;
    UpdatePrintwithAdjustmentPayrolls1: TMenuItem;
    sbSunkenBrowse: TScrollBox;
    pnlSBSunkenBorder: TevPanel;
    pnlBrowseLeft: TevPanel;
    evSplitter1: TevSplitter;
    pnlBrowseRight: TevPanel;
    fpBrowseLeft: TisUIFashionPanel;
    pnlFPBrowseLeftBody: TevPanel;
    fpBrowseRight: TisUIFashionPanel;
    pnlFPBrowseRightBody: TevPanel;
    ScrollBox1: TScrollBox;
    fpReport: TisUIFashionPanel;
    fpOutput: TisUIFashionPanel;
    fpPayrollProcessing: TisUIFashionPanel;
    Label1: TevLabel;
    Label2: TevLabel;
    memNote: TEvDBMemo;
    edRepName: TevDBEdit;
    cbReport: TevDBLookupCombo;
    evPanel1: TevPanel;
    evSpeedButton1: TevSpeedButton;
    Label9: TevLabel;
    Label10: TevLabel;
    lWeek: TevLabel;
    lMonth: TevLabel;
    Label8: TevLabel;
    edCopies: TevDBSpinEdit;
    edPriority: TevDBSpinEdit;
    cbFrequency: TevDBComboBox;
    cbWeek: TevDBComboBox;
    chbDuplexing: TevCheckBox;
    cbMonth: TevDBComboBox;
    chbSummarize: TevCheckBox;
    evLabel21: TevLabel;
    gbASCII: TevGroupBox;
    sbBrowse: TevSpeedButton;
    dbeFile: TevDBEdit;
    chbAddFile: TevCheckBox;
    btnRunRep: TevBitBtn;
    btnInpForm: TevBitBtn;
    evDBLookupCombo3: TevDBLookupCombo;
    chbHideForRemotes: TevCheckBox;
    chbPrnWithAdjPayrolls: TevCheckBox;
    chkFavorite: TevDBCheckBox;
    edHiddenSelectReport: TEdit;
    chbDashboard: TevCheckBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure sbBrowseClick(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure chbAddFileClick(Sender: TObject);
    procedure dbeFileChange(Sender: TObject);
    procedure cbFrequencyChange(Sender: TObject);
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
    procedure btnInpFormClick(Sender: TObject);
    procedure btnRunRepClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure CopyReport1Click(Sender: TObject);
    procedure cbReportChange(Sender: TObject);
    procedure edPriorityChange(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure UpdatePrintwithAdjustmentPayrolls1Click(Sender: TObject);
    procedure wwDBGCoReportsDblClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure chbHideForRemotesClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    FRunParams: TevPrRepPrnSettings;
    FInParams: TrwReportParams;
    FCurRepKey: String;
    FCurReport: TObject;
    SY_DS: TevClientDataSet;
    SB_DS: TevClientDataSet;
    procedure CreateReportSourceDataSet;
    procedure SyncRunParams;
    procedure EditMode(AEnabled: Boolean);
    procedure FBeforePost;
    procedure InitMonthCombo;
    function IsDashboardReport(aNotes: string): boolean;
  protected
    procedure Loaded; override;
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure OnGetSecurityStates(const States: TSecurityStateList); override;
    procedure SetReadOnly(Value: Boolean); override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses ChoseReportDlg, SPD_EDIT_Open_BASE;


{$R *.DFM}

const
  ctEnabledWoSb = 'S';
  ctEnabledWoSbWoCc = 'T';

procedure TEDIT_CO_REPORTS.Activate;
begin
  inherited;
  //to hide FavoriteReport icon
  pnlFavoriteReport.visible := False;

  ctx_RWLocalEngine.BeginWork;
  SY_DS := nil;
  SB_DS := nil;

  wwdsMaster.Tag := 1;
  wwdsMaster.OnDataChange(nil, nil);

  // Display the checkbox only for internal S/B users
  if Context.UserAccount.AccountType in [uatServiceBureau, uatSBAdmin] then
  begin
    chbHideForRemotes.Visible := true;
    // MR 10/24/06 added requirement in reso# 41438 (display all fields)
    wwDBGCoReports.Selected.Text :=
            'NDescription'#9'40'#9'Report print Name'#9'F'#13#10 +
            'RWDescription'#9'40'#9'Report Description'#9'F'#13#10 +
            'Priority'#9'10'#9'Report Print Priority'#9'F'#13#10 +
            'HideForRemotes'#9'1'#9'Hide For Remotes'#9'F';
    edCopies.Enabled := True;
    chbPrnWithAdjPayrolls.Visible:=True;
  end
  else
  begin
    chbHideForRemotes.Visible := false;
    // MR added requirement in reso# 41438 to hide new grid field for remote users
    wwDBGCoReports.Selected.Text :=
            'NDescription'#9'40'#9'Report print Name'#9'F'#13#10 +
            'RWDescription'#9'40'#9'Report Description'#9'F'#13#10 +
            'Priority'#9'10'#9'Report Print Priority'#9'F';
    edCopies.Enabled := False;
    chbPrnWithAdjPayrolls.Visible:=False;
  end;
end;


function TEDIT_CO_REPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_REPORTS;
end;

procedure TEDIT_CO_REPORTS.sbBrowseClick(Sender: TObject);
var
  TempStr: String;
begin
  inherited;
  TempStr := RunFolderDialog('Please, select an output path:', dbeFile.Text);
  dbeFile.Text := TempStr;
end;

procedure TEDIT_CO_REPORTS.SetReadOnly(Value: Boolean);
begin
  inherited;
  cbReport.ReadOnly := cbReport.ReadOnly;
  cbReport.ReadOnly := not ((SecurityState = stEnabled) or (SecurityState = ctEnabledWoSb) or (SecurityState = ctEnabledWoSbWoCc));
  evSpeedButton1.Enabled := not cbReport.ReadOnly;
  edCopies.ReadOnly := (SecurityState = ctEnabledWoSbWoCc);
  //edCopies.Enabled := not edCopies.ReadOnly;

end;

procedure TEDIT_CO_REPORTS.DeActivate;
begin
  ctx_RWLocalEngine.EndWork;

  SY_DS.Free;
  SB_DS.Free;

  wwdsMaster.Tag := 0; // avoid security issue

  inherited;
end;

procedure TEDIT_CO_REPORTS.CreateReportSourceDataSet;

  procedure AddDBReports(ALevel: String);
  var
    KeyFld, CoDefFld, h, h1: String;
    DS: TevClientDataSet;
  begin
    KeyFld := '_REPORT_WRITER_REPORTS_NBR';
    case ALevel[1] of
      CH_DATABASE_SYSTEM:  begin
              KeyFld := 'SY' + KeyFld;
              CoDefFld := 'REPORT_WRITER_REPORTS_NBR';
              DS := SY_DS;
            end;

      CH_DATABASE_SERVICE_BUREAU:
            begin
              KeyFld := 'SB' + KeyFld;
              CoDefFld := 'REPORT_WRITER_REPORTS_NBR';
              DS := SB_DS;
            end;

      CH_DATABASE_CLIENT:
            begin
              KeyFld := 'CL' + KeyFld;
              CoDefFld := '';
              DS := DM_CLIENT.CL_REPORT_WRITER_REPORTS;
              ctx_DataAccess.OpenDataSets([DS]);
            end;
    else
      DS := nil;
    end;

    DM_SERVICE_BUREAU.SB_REPORTS.IndexFieldNames := 'REPORT_LEVEL;REPORT_WRITER_REPORTS_NBR';

    DS.First;
    while not DS.Eof do
    begin
      h := '';
      if Length(CoDefFld) > 0 then
      begin
        if DM_SERVICE_BUREAU.SB_REPORTS.FindKey([ALevel, DS.FieldByName(KeyFld).AsInteger]) then
        begin
          h := DM_SERVICE_BUREAU.SB_REPORTS.FieldByName('ndescription').AsString;
          h1 := DM_SERVICE_BUREAU.SB_REPORTS.FieldByName('description').AsString;
        end
      end
      else
      begin
        h1 := DS.FieldByName('report_description').AsString;
        h := h1 + ' (C' + DS.FieldByName('CL_REPORT_WRITER_REPORTS_NBR').AsString + ')';
      end;

      if Length(h) > 0 then
      begin
        csReports.Append;
        csReports.FieldByName('nbr').AsInteger := DS.FieldByName(KeyFld).AsInteger;
        csReports.FieldByName('level').AsString := ALevel;
        csReports.FieldByName('Key').AsString := DS.FieldByName(KeyFld).AsString + ALevel;
        csReports.FieldByName('name').AsString := h;
        csReports.FieldByName('cName').AsString := h1;
        csReports.FieldByName('notes').Value := DS.FieldByName('notes').Value;
        csReports.FieldByName('Type').AsString := DS.FieldByName('REPORT_TYPE').AsString[1];
        csReports.Post;
      end;

      DS.Next;
    end;
  end;

begin
  if csReports.Tag = ctx_DataAccess.ClientID then
    Exit;

  if not Assigned(SY_DS) then
  begin
    SY_DS := TevClientDataSet.Create(nil);

    if ctx_DataAccess.SY_CUSTOM_VIEW.Active then
      ctx_DataAccess.SY_CUSTOM_VIEW.Close;

    with TExecDSWrapper.Create('GenericSelectCurrent') do
    begin
      SetMacro('Columns', 'SY_REPORT_WRITER_REPORTS_NBR, REPORT_DESCRIPTION, REPORT_TYPE, NOTES');
      SetMacro('TableName', 'SY_REPORT_WRITER_REPORTS');
      ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.SY_CUSTOM_VIEW.Open;
      SY_DS.Data := ctx_DataAccess.SY_CUSTOM_VIEW.Data;
      ctx_DataAccess.SY_CUSTOM_VIEW.Close;
    end;
  end;

  if not Assigned(SB_DS) then
  begin
    SB_DS := TevClientDataSet.Create(nil);

    if ctx_DataAccess.SB_CUSTOM_VIEW.Active then
      ctx_DataAccess.SB_CUSTOM_VIEW.Close;

    with TExecDSWrapper.Create('GenericSelectCurrent') do
    begin
      SetMacro('Columns', 'SB_REPORT_WRITER_REPORTS_NBR, REPORT_DESCRIPTION, REPORT_TYPE, NOTES');
      SetMacro('TableName', 'SB_REPORT_WRITER_REPORTS');
      ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.SB_CUSTOM_VIEW.Open;
      SB_DS.Data := ctx_DataAccess.SB_CUSTOM_VIEW.Data;
      ctx_DataAccess.SB_CUSTOM_VIEW.Close;
    end;
  end;

  DM_SERVICE_BUREAU.SB_REPORTS.CheckDSCondition('');
  ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_REPORTS]);

  csReports.DisableControls;
  csReports.Close;
  csReports.CreateDataSet;

  if not ((SecurityState = ctEnabledWoSb) or (SecurityState = ctEnabledWoSbWoCc)) then
  begin
    AddDBReports(CH_DATABASE_SYSTEM);
    AddDBReports(CH_DATABASE_SERVICE_BUREAU);
  end;

  AddDBReports(CH_DATABASE_CLIENT);

  csReports.EnableControls;
  csReports.Tag := ctx_DataAccess.ClientID;
end;

procedure TEDIT_CO_REPORTS.wwdsDetailDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if (wwdsDetail.DataSet.State = dsBrowse) and Assigned(FRunParams) and csReports.Active then
  begin
    wwdsDetail.Tag := 1;
    try
      if csReports.Locate('key',
        wwdsDetail.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').AsString +
        wwdsDetail.DataSet.FieldByName('REPORT_LEVEL').AsString, []) then
      begin
        cbReport.LookupField := 'key';   // bugfix, don't touch
        cbReport.LookupValue := csReports.FieldByName('key').AsString;
        btnInpForm.Enabled := True;
      end

      else
      begin
        cbReport.LookupValue :='';
        memNote.Clear;
        btnInpForm.Enabled := False;
      end;

      if not wwdsDetail.DataSet.FieldByName('RUN_PARAMS').IsNull then
        try
          FRunParams.ReadFromBlobField(TBlobField(wwdsDetail.DataSet.FieldByName('RUN_PARAMS')))
        except
          FRunParams.Clear;
        end
      else
        FRunParams.Clear;

      if not wwdsDetail.DataSet.FieldByName('INPUT_PARAMS').IsNull then
        try
          FInParams.ReadFromBlobField(TBlobField(wwdsDetail.DataSet.FieldByName('INPUT_PARAMS')))
        except
          FInParams.Clear;
        end
      else
        FInParams.Clear;

      tsCoReportsDetail.Enabled := not ((wwdsDetail.DataSet.FieldByName('REPORT_LEVEL').AsString = REPORT_LEVEL_BUREAU) and ((SecurityState = ctEnabledWoSb) or (SecurityState = ctEnabledWoSbWoCc)));

      SyncRunParams;
    finally
      wwdsDetail.Tag := 0;
    end;
  end
  else
  begin
    if (wwdsDetail.DataSet.State = dsInsert) then
    begin
      tsCoReportsDetail.Enabled := True;
      chkFavorite.Enabled := True;
    end;
  end;
end;

procedure TEDIT_CO_REPORTS.chbAddFileClick(Sender: TObject);
var
  h: string;
begin
  EditMode(True);

  h := Trim(dbeFile.Text);
  if h = '' then
  begin
    chbAddFile.Checked := False;
    Exit;
  end;

  if Length(h) = 0 then
    Exit;

  if h[Length(h)] = '+' then
    Delete(h, Length(h), 1);

  if chbAddFile.Checked then
    h := h + '+';

  dbeFile.Text := h;
end;

procedure TEDIT_CO_REPORTS.dbeFileChange(Sender: TObject);
var
   h: String;
begin
  EditMode(True);

  h := Trim(dbeFile.Text);

  if Length(h) = 0 then
    chbAddFile.Checked := False
  else
    chbAddFile.Checked := (h[Length(h)] = '+');
end;

procedure TEDIT_CO_REPORTS.SyncRunParams;
begin
  cbFrequency.Value := FRunParams.Frequency;
  InitMonthCombo;  
  edPriority.Value := FRunParams.Priority;
  cbWeek.Value := FRunParams.WeekOfMonth;
  cbMonth.Value := IntToStr(FRunParams.MonthNumber);
  if Context.UserAccount.AccountType in [uatServiceBureau, uatSBAdmin] then
    edCopies.Value := FRunParams.Copies
  else
    edCopies.Value := 0;


  chbSummarize.Checked := FRunParams.Summarized;
  chbDuplexing.Checked := FInParams.Duplexing;
  dbeFile.Text := FInParams.FileName;
  chbAddFile.Checked := FInParams.AddToExistFile;
  //MR add new parameter
  chbHideForRemotes.Checked := FRunParams.HideForRemotes;
  // 'Print with Adjustment Payrolls' param for reso# 45004
  chbPrnWithAdjPayrolls.Checked := FRunParams.PrnWithAdjPayrolls;

  chbDashboard.Visible := IsDashboardReport(VarToStr(csReports.FieldByName('notes').Value));
  chbDashboard.Checked := FRunParams.DisplayOnDashboard;
 { DM_COMPANY.CO_REPORTS.Edit;
  DM_COMPANY.CO_REPORTS.fieldbyname('Priority').value := FRunParams.Priority;
  DM_COMPANY.CO_REPORTS.Post;  }
//  gbASCII.Enabled := (cbReport.LookupValue <> '') and (csReports.FieldByName('type').AsString = 'A');

  //To check PR driven reports
  chkFavorite.enabled := (cbReport.LookupValue <> '')
                         and ( Trim(csReports.FieldByName('Type').AsString) <>'' )
                         and ( ( csReports.FieldByName('Type').AsString[1] = SYSTEM_REPORT_TYPE_NORMAL )
                               or ( csReports.FieldByName('Type').AsString[1] = SYSTEM_REPORT_TYPE_HR) );
end;


procedure TEDIT_CO_REPORTS.cbFrequencyChange(Sender: TObject);
begin
  InitMonthCombo;
  EditMode(True);
end;

procedure TEDIT_CO_REPORTS.EditMode(AEnabled: Boolean);
begin
  if (wwdsDetail.Tag = 0) and Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active then
    if (wwdsDetail.DataSet.State = dsBrowse) and AEnabled then
      wwdsDetail.DataSet.Edit
    else if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) and not AEnabled then
      wwdsDetail.DataSet.Cancel;
end;

procedure TEDIT_CO_REPORTS.wwdsMasterDataChange(Sender: TObject;
  Field: TField);
var
  sOldIndex: string;
begin
  inherited;


  if (wwdsMaster.Tag = 1) and (wwdsMaster.DataSet.Active) and (wwdsDetail.DataSet.Active) then
  begin
    TevClientDataSet(wwdsDetail.DataSet).DisableControls;
    csReports.DisableControls;
    CreateReportSourceDataSet;

    sOldIndex := TevClientDataSet(wwdsDetail.DataSet).IndexName;
    TevClientDataSet(wwdsDetail.DataSet).IndexName := '';
    try
      wwdsDetail.DataSet.First;
      while not wwdsDetail.DataSet.Eof do
      begin
        wwdsDetail.DataSet.Edit;
        if csReports.Locate('KEY',
          wwdsDetail.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').AsString + wwdsDetail.DataSet.FieldByName('REPORT_LEVEL').AsString, []) then
          wwdsDetail.DataSet['RWDescription'] := csReports['notes']
        else
          wwdsDetail.DataSet['RWDescription'] := null;
        wwdsDetail.DataSet.Post;
        wwdsDetail.DataSet.Next;
      end;
    finally
      TevClientDataSet(wwdsDetail.DataSet).MergeChangeLog;
      TevClientDataSet(wwdsDetail.DataSet).IndexName := sOldIndex;
      wwdsDetail.DataSet.First;
      csReports.EnableControls;
      TevClientDataSet(wwdsDetail.DataSet).EnableControls;
    end;
  end;
  
end;

procedure TEDIT_CO_REPORTS.btnInpFormClick(Sender: TObject);
var
  Res: TModalResult;
begin
  FInParams.Add('Clients', varArrayOf([ctx_DataAccess.ClientID]) );
  FInParams.Add('Companies', varArrayOf([wwdsList.DataSet.FieldByName('CO_NBR').AsInteger]));
  FInParams.Add('ClientCompany', varArrayOf([IntToStr(ctx_DataAccess.ClientID) + ':' + wwdsList.DataSet.FieldByName('CO_NBR').AsString]));
  FInParams.Add('HideCompanyList', True);
  FInParams.Add('SetupMode', True);
  FInParams.Add('ReportName', wwdsDetail.DataSet.FieldByName('CUSTOM_NAME').AsString);

  try
    Res := ctx_RWLocalEngine.ShowModalInputForm(csReports.FieldByName('NBR').AsInteger,
      csReports.FieldByName('LEVEL').AsString, FInParams, False);
  finally
    if not DM_COMPANY.CO.Active then
      DM_COMPANY.CO.Active := True;
  end;

  if not GetIfReadOnly then
  begin
    if (Res = mrOk) and (wwdsDetail.DataSet.State = dsBrowse) then
      wwdsDetail.DataSet.Edit;
  end
  else
    if not wwdsDetail.DataSet.FieldByName('INPUT_PARAMS').IsNull then
      try
        FInParams.ReadFromBlobField(TBlobField(wwdsDetail.DataSet.FieldByName('INPUT_PARAMS')))
      except
        FInParams.Clear;
      end
    else
      FInParams.Clear;
end;

procedure TEDIT_CO_REPORTS.btnRunRepClick(Sender: TObject);
begin
  (Owner as TFramePackageTmpl).OpenFrame('TCO_REPORTS_RUN');
end;

procedure TEDIT_CO_REPORTS.FBeforePost;
begin
  FRunParams.Frequency := cbFrequency.Value;
  FRunParams.Priority := Trunc(edPriority.Value);
  FRunParams.WeekOfMonth := cbWeek.Value;
  if cbMonth.Value <> '' then
    FRunParams.MonthNumber := StrToInt(cbMonth.Value)
  else
    FRunParams.MonthNumber := 0;
  FRunParams.Copies := Trunc(edCopies.Value);
  FRunParams.Summarized := chbSummarize.Checked;
  FInParams.Duplexing := chbDuplexing.Checked;
  FInParams.FileName := dbeFile.Text;
  FRunParams.HideForRemotes := chbHideForRemotes.Checked;
  FRunParams.PrnWithAdjPayrolls := chbPrnWithAdjPayrolls.Checked;
  FRunParams.DisplayOnDashboard := chbDashboard.Checked;

  if cbReport.LookupValue <> '' then
  begin
    wwdsDetail.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').AsInteger := csReports.FieldByName('NBR').AsInteger;
    wwdsDetail.DataSet.FieldByName('REPORT_LEVEL').AsString := csReports.FieldByName('LEVEL').AsString;
  end
  else
  begin
    wwdsDetail.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').Clear;
    wwdsDetail.DataSet.FieldByName('REPORT_LEVEL').Clear;
  end;

  FRunParams.SaveToBlobField(TBlobField(wwdsDetail.DataSet.FieldByName('RUN_PARAMS')));
  FInParams.SaveToBlobField(TBlobField(wwdsDetail.DataSet.FieldByName('INPUT_PARAMS')));
end;

procedure TEDIT_CO_REPORTS.ButtonClicked(Kind: Integer; var Handled: Boolean);
     function CheckMaxFavoriteReports : string;
     begin
        Result := '';
        if ctx_DataAccess.CUSTOM_VIEW.Active then
          ctx_DataAccess.CUSTOM_VIEW.Close;
        with TExecDSWrapper.Create(' Select count(1) from CO_REPORTS Where CO_NBR = :CONBR '+
                                   ' and FAVORITE = ''Y'' and {AsOfNow<CO_REPORTS>}') do
        begin
          SetParam('CONBR', wwdsDetail.DataSet.FieldByName('CO_NBR').Value);
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        end;
        ctx_DataAccess.CUSTOM_VIEW.Open;

        if ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value > 14 then
        begin
          Result := 'Only 15 total reports may be selected as Favorite Reports.'#13#10 +
                 'You must adjust the reports selected before you will be able to add this report to your list.';
        end;
        ctx_DataAccess.CUSTOM_VIEW.Close;
      end;

var
  err: String;
  i:integer;
  EnabledFavorite : Boolean;
begin
  inherited;
  if Kind = 10 then //NavOK
  begin
    if cbReport.LookupValue <> '' then
    begin
      EnabledFavorite := ( Trim(csReports.FieldByName('Type').AsString) <>'' )
                            and ( ( csReports.FieldByName('Type').AsString[1] = SYSTEM_REPORT_TYPE_NORMAL )
                                  or ( csReports.FieldByName('Type').AsString[1] = SYSTEM_REPORT_TYPE_HR) );
      if (not EnabledFavorite)
      then begin
        wwdsDetail.DataSet.FieldByName('Favorite').AsString := 'N';
        chkFavorite.Checked := False;
      end;
    end;

    Err := '';
    if cbFrequency.Value = ''  then
      Err := 'Print frequency must be not empty'

    else
      if (cbFrequency.Value[1] in [SCHED_FREQ_SCHEDULED_MONTHLY, SCHED_FREQ_QUARTERLY,
         SCHED_FREQ_ANNUALLY, SCHED_FREQ_SEMI_ANNUALLY]) and (cbWeek.Value = '') then
        Err := 'Week number must be not empty'
      else if (cbFrequency.Value[1] in [SCHED_FREQ_QUARTERLY, SCHED_FREQ_ANNUALLY,
              SCHED_FREQ_SEMI_ANNUALLY]) and (cbMonth.Value = '') then
        Err := 'Month must be not empty';

    if (( Err = '')  and ( chkFavorite.Checked ) )then
       Err := CheckMaxFavoriteReports;

    if Err = '' then
      FBeforePost
    else
    begin
      Handled := True;
      EvErrMessage(Err);
    end;
  end;
  if Kind = navDelete then
  begin
   if PageControl1.ActivePage <> tsCoReportsDetail then
   begin
      if EvMessage('Are you sure?', mtConfirmation, mbOKCancel) <> mrOK then
         Handled := true
      else
      begin
        DM_COMPANY.CO_REPORTS.DisableControls;
        try
          if wwDBGCoReports.SelectedList.Count > 0 then
          begin
            for i := 0 to wwDBGCoReports.SelectedList.Count-1 do
            begin
              DM_COMPANY.CO_REPORTS.GotoBookmark(wwDBGCoReports.SelectedList.items[i]);
              DM_COMPANY.CO_REPORTS.Delete;
            end;
          end
          else
            DM_COMPANY.CO_REPORTS.Delete;
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_REPORTS]);
          Handled := True;
        finally
          DM_COMPANY.CO_REPORTS.EnableControls;
        end;
      end;
   end;
  end;

end;

procedure TEDIT_CO_REPORTS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;

  if (wwdsDetail.DataSet.State = dsInsert) and Assigned(FRunParams) then
  begin
    FRunParams.Clear;
    FInParams.Clear;
    if (SecurityState = ctEnabledWoSbWoCc) then
        FRunParams.Copies := 0
    else
      FRunParams.Copies := 1;
    FRunParams.Priority := 5;
    SyncRunParams;
    cbReport.LookupValue :='';
    memNote.Clear;
    btnInpForm.Enabled := False;
  end;
end;

procedure TEDIT_CO_REPORTS.CopyReport1Click(Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  cClientNbr, cCoNbr, cCoReportsNbr: Integer;
  cCustomCoNbr: String;
  fl: Boolean;
begin
  inherited;

  cClientNbr    := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  cCoNbr        := DM_COMPANY.CO_REPORTS.FieldByName('CO_NBR').AsInteger;
  cCustomCoNbr  := wwdsMaster.dataset.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
  cCoReportsNbr := DM_COMPANY.CO_REPORTS.FieldByName('CO_REPORTS_NBR').AsInteger;

  if (wwDBGCoReports.SelectedList.Count > 0) then
  begin
    CopyForm := TEDIT_COPY.Create(Self);

    DM_COMPANY.CO_REPORTS.DisableControls;
    try
      DM_COMPANY.CO_REPORTS.UserFilter :='';
      DM_COMPANY.CO_REPORTS.UserFiltered := false;
      CopyForm.wwcsCoReports.Data := DM_COMPANY.CO_REPORTS.Data;
      fl := False;
      with DM_COMPANY.CO_REPORTS do
      begin
        First;
        while not Eof do
        begin
          if not wwDBGCoReports.IsSelectedRecord then
          begin
            if CopyForm.wwcsCoReports.locate('CO_REPORTS_NBR', DM_COMPANY.CO_REPORTS['CO_REPORTS_NBR'], []) then
              CopyForm.wwcsCoReports.Delete;
          end
{          else if DM_COMPANY.CO_REPORTS['PRINT_BARCODE'] =  CH_DATABASE_CLIENT then
          begin
            if CopyForm.wwcsCoReports.locate('CO_REPORTS_NBR', DM_COMPANY.CO_REPORTS['CO_REPORTS_NBR'], []) then
            begin
              fl := True;
              CopyForm.wwcsCoReports.Delete;
            end;
          end};
          Next;
        end;
      end;
      CopyForm.CopyTo := 'CoReports';
      CopyForm.CopyFrom := 'CoReports';
      DM_COMPANY.CO_REPORTS.Cancel;

      if fl then
        MessageDlg('This will not copy any client reports!', mtInformation, [mbOK], 0);

      wwdsMaster.DataSet := nil;
      CopyForm.ShowModal;

      ctx_DataAccess.OpenClient(cClientNbr);
      DM_CLIENT.CL.DataRequired('ALL');
      DM_COMPANY.CO.DataRequired('CO_NBR='+IntToStr(cCoNbr));
      DM_COMPANY.CO_REPORTS.DataRequired('CO_NBR='+IntToStr(cCoNbr));
      wwdsMaster.DataSet := DM_CLIENT.CO;
      wwdsList.dataset.Locate('CO_NBR;CL_NBR;CUSTOM_COMPANY_NUMBER', VarArrayOf([cCoNbr, cClientNbr, cCustomCoNbr]), []);
      DM_COMPANY.CO_REPORTS.Locate('CO_REPORTS_NBR', cCoReportsNbr, []);
    finally
      wwDBGCoReports.UnselectAll;
      DM_COMPANY.CO_REPORTS.EnableControls;
      CopyForm.Free;
    end;
  end;
end;

procedure TEDIT_CO_REPORTS.cbReportChange(Sender: TObject);
begin
  if SecurityState = 'R' then
    Exit;
  EditMode(True);
  btnInpForm.Enabled := (cbReport.LookupValue <> '');
//  gbASCII.Enabled := (cbReport.LookupValue <> '') and (csReports.FieldByName('type').AsString = 'A');
  if not gbASCII.Enabled and (dbeFile.Text <> '') then
    dbeFile.Text := '';

  if wwdsDetail.DataSet.State <> dsBrowse then
    wwdsDetail.DataSet.FieldByName('CUSTOM_NAME').AsString := csReports.FieldByName('cName').AsString;
end;

constructor TEDIT_CO_REPORTS.Create(AOwner: TComponent);
begin
  inherited;
  FRunParams := TevPrRepPrnSettings.Create(nil);
  FInParams := TrwReportParams.Create;
  FCurReport := nil;
  FCurRepKey := '';
end;

destructor TEDIT_CO_REPORTS.Destroy;
begin
  FInParams.Free;
  FRunParams.Free;
  FRunParams := nil;

  inherited;
end;

procedure TEDIT_CO_REPORTS.InitMonthCombo;
begin
  if cbFrequency.Value = SCHED_FREQ_QUARTERLY then
    cbMonth.Items.Text := '1st' + #9 + '1' + #13 +
                          '2nd' + #9 + '2' + #13 +
                          '3rd' + #9 + '3'
  else if cbFrequency.Value = SCHED_FREQ_SEMI_ANNUALLY then
    cbMonth.Items.Text := '1st' + #9 + '1' + #13 +
                          '2nd' + #9 + '2' + #13 +
                          '3rd' + #9 + '3' + #13 +
                          '4th' + #9 + '4' + #13 +
                          '5th' + #9 + '5' + #13 +
                          '6th' + #9 + '6'

  else if cbFrequency.Value = SCHED_FREQ_ANNUALLY then
    cbMonth.Items.Text := 'January' + #9 + '1' + #13 +
                          'February' + #9 + '2' + #13 +
                          'March' + #9 + '3' + #13 +
                          'April' + #9 + '4' + #13 +
                          'May' + #9 + '5' + #13 +
                          'June' + #9 + '6' + #13 +
                          'July' + #9 + '7' + #13 +
                          'August' + #9 + '8' + #13 +
                          'September' + #9 + '9' + #13 +
                          'October' + #9 + '10' + #13 +
                          'November' + #9 + '11' + #13 +
                          'December' + #9 + '12'
  else
    cbMonth.Items.Text := '';

  cbMonth.Value := '';
  lMonth.Enabled := (cbMonth.Items.Text <> '');
  cbMonth.Enabled := lMonth.Enabled and not cbMonth.ReadOnly;

  cbWeek.Value := '';
  lWeek.Enabled := (cbFrequency.Value <> '') and
    (cbFrequency.Value[1] in [SCHED_FREQ_SCHEDULED_MONTHLY, SCHED_FREQ_QUARTERLY, SCHED_FREQ_SEMI_ANNUALLY, SCHED_FREQ_ANNUALLY]);
  cbWeek.Enabled := lWeek.Enabled and not cbWeek.ReadOnly;
end;

procedure TEDIT_CO_REPORTS.edPriorityChange(Sender: TObject);
begin
  EditMode(True);
end;

procedure TEDIT_CO_REPORTS.AfterDataSetsReopen;
begin
  inherited;
  evDBLookupCombo3.Visible := CheckVmrLicenseActive and CheckVmrSetupActive;
  evLabel21.Visible := evDBLookupCombo3.Visible;

  if Context.UserAccount.AccountType = uatRemote then
  begin
    // Filter the reports dataset by HideForRemotes false
    dm_company.co_reports.Filter := 'HideForRemotes = ''N''';
    dm_company.co_reports.Filtered := True;
  end;
end;

procedure TEDIT_CO_REPORTS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP, aDS);
end;

procedure TEDIT_CO_REPORTS.evSpeedButton1Click(Sender: TObject);
begin
  inherited;
  with TChoseReport.Create(Self) do
  try
    evDataSource1.DataSet := csReports;
    csReports.Locate('key', cbReport.LookupValue, []);
    if ShowModal = mrOk then
      cbReport.LookupValue := csReports['key']
    else
      csReports.Locate('key', cbReport.LookupValue, []);
  finally
    Free;
  end;
end;

procedure TEDIT_CO_REPORTS.OnGetSecurityStates(
  const States: TSecurityStateList);
const
  secScreenSBRO: TAtomContextRecord = (Tag: ctEnabledWoSb; Caption: 'Enabled except SB report lookup'; IconName: 'TREESCREENSENABLED'; Priority: 200);
  secScreenSBCCRO: TAtomContextRecord = (Tag: ctEnabledWoSbWoCC; Caption: 'Enabled except SB report lookup and copy counter'; IconName: 'TREESCREENSENABLED'; Priority: 190);
begin
  with secScreenSBCCRO do
    States.InsertAfter(ctEnabled, Tag[1], Caption, IconName);
  with secScreenSBRO do
    States.InsertAfter(ctEnabled, Tag[1], Caption, IconName);
end;

// Update 'Print with Adjustment Payrolls' field (reso# 46084)
procedure TEDIT_CO_REPORTS.UpdatePrintwithAdjustmentPayrolls1Click(
  Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  cClientNbr, cCoNbr, cCoReportsNbr: Integer;
  cCustomCoNbr: String;
  opChecked: Boolean;
begin
  inherited;

  opChecked := False;

  if not(EvChboxDialog('Options', 'Options for reports:', 'Print with Adjustment Payrolls', opChecked)) then Exit;

  cClientNbr    := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  cCoNbr        := DM_COMPANY.CO_REPORTS.FieldByName('CO_NBR').AsInteger;
  cCustomCoNbr  := wwdsMaster.dataset.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
  cCoReportsNbr := DM_COMPANY.CO_REPORTS.FieldByName('CO_REPORTS_NBR').AsInteger;

  with wwDBGCoReports, wwDBGCoReports.DataSource.DataSet do
    if (SelectedList.IndexOf( GetBookmark ) < 0) then
      SelectedList.Add( GetBookmark );

  if (wwDBGCoReports.SelectedList.Count > 0) then
  begin
    CopyForm := TEDIT_COPY.Create(Self);

    try
      DM_COMPANY.CO_REPORTS.DisableControls;
      CopyForm.wwcsCoReports.Data := DM_COMPANY.CO_REPORTS.Data;
      with DM_COMPANY.CO_REPORTS do
      begin
        First;
        while not Eof do
        begin
          if not wwDBGCoReports.IsSelectedRecord then
          begin
            if CopyForm.wwcsCoReports.locate('CO_REPORTS_NBR', DM_COMPANY.CO_REPORTS['CO_REPORTS_NBR'], []) then
              CopyForm.wwcsCoReports.Delete;
          end;
          Next;
        end;
      end;
      CopyForm.CopyTo := 'PwAP';
      if opChecked then CopyForm.CopyFrom := 'Set'
        else CopyForm.CopyFrom := 'Unset';

      CopyForm.ShowModal;

      ctx_DataAccess.OpenClient(cClientNbr);
      DM_CLIENT.CL.DataRequired('ALL');
      DM_COMPANY.CO.DataRequired('CO_NBR='+IntToStr(cCoNbr));
      DM_COMPANY.CO_REPORTS.DataRequired('CO_NBR='+IntToStr(cCoNbr));
      wwdsList.dataset.Locate('CO_NBR;CL_NBR;CUSTOM_COMPANY_NUMBER', VarArrayOf([cCoNbr, cClientNbr, cCustomCoNbr]), []);
      DM_COMPANY.CO_REPORTS.Locate('CO_REPORTS_NBR', cCoReportsNbr, []);
      wwDBGCoReports.UnselectAll;
      DM_COMPANY.CO_REPORTS.EnableControls;
    finally
      DM_COMPANY.CO_REPORTS.EnableControls;
      CopyForm.Free;
    end;
  end;
end;

procedure TEDIT_CO_REPORTS.wwDBGCoReportsDblClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 1;
end;

procedure TEDIT_CO_REPORTS.PageControl1Change(Sender: TObject);
begin
  inherited;
  memNote.Refresh;
end;

function TEDIT_CO_REPORTS.IsDashboardReport(aNotes: string): boolean;
begin
  Result := Pos('DASHBOARD', UpperCase(aNotes)) > 0;
end;

procedure TEDIT_CO_REPORTS.chbHideForRemotesClick(Sender: TObject);
begin
  EditMode(True);
end;

procedure TEDIT_CO_REPORTS.Button1Click(Sender: TObject);
var
   //iClientNbr,
   iCoNbr: integer;
   iCoReportsNbr: integer;
   aDia: TOpenDialog;
   iResult: integer;
   iList: iIsStringList;
begin
  inherited;
//  iClientNbr    := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  iCoNbr        := DM_COMPANY.CO_REPORTS.FieldByName('CO_NBR').AsInteger;
  iCoReportsNbr := wwdsDetail.DataSet.FieldByName('CO_REPORTS_NBR').Asinteger;


   aDia := TOpenDialog.Create(Self);
   try
     aDia.Filter := 'XML|*.xml';
     if aDia.Execute then
     begin
        iList := tisStringList.Create;
        iList.LoadFromFile(aDia.FileName);
        iResult :=Context.RemoteMiscRoutines.StoreReportParametersFromXML(iCoNbr, iCoReportsNbr, iList.Text);
        if iResult > 0 then
          ShowMessage('Sucess! stored at CO_REPORTS_NBR=' + IntToStr(iResult))
        else
          ShowMessage('Failed to store at CO_REPORTS table');
     end;
   finally
     aDia.Free;
   end;
end;

procedure TEDIT_CO_REPORTS.Button2Click(Sender: TObject);
var
   //iClientNbr,
   iCoNbr: integer;
   iCoReportsNbr: integer;
   aDia: TSaveDialog;
   sXML:string;
   iList: iIsStringList;
begin
  inherited;
//  iClientNbr    := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
  iCoNbr        := DM_COMPANY.CO_REPORTS.FieldByName('CO_NBR').AsInteger;
  iCoReportsNbr := wwdsDetail.DataSet.FieldByName('CO_REPORTS_NBR').Asinteger;


   aDia := TSaveDialog.Create(Self);
   try
     aDia.Filter := 'XML|*.xml';
     if aDia.Execute then
     begin
        sXML :=Context.RemoteMiscRoutines.GetCoReportsParametersAsXML(iCoNbr, iCoReportsNbr);
        iList := TisStringList.Create;
        iList.Text := sXML;
        iList.SaveToFile(aDia.FileName);
        ShowMessage('File saved as' + #13#10 + aDia.FileName);
     end;
   finally
     aDia.Free;
   end;
end;

procedure TEDIT_CO_REPORTS.Button3Click(Sender: TObject);
var
   aDia: TOpenDialog;
   iList: iIsStringList;
   oRwp: TrwReportParams;
   sXMLAnsi: AnsiString;
   iiS: IisStream;
begin
  inherited;

   aDia := TOpenDialog.Create(Self);
   try
     aDia.Filter := 'XML|*.xml';
     if aDia.Execute then
     begin
        iList := tisStringList.Create;
        iList.LoadFromFile(aDia.FileName);
        sXMLAnsi := iList.Text;
        iiS :=Context.RemoteMiscRoutines.ConvertXMLtoRWParameters(sXMLAnsi);
        oRwp := TrwReportParams.Create;
        try
          iiS.Position := 0;
          oRwp.LoadFromStream(iiS.RealStream);
          oRwp.SaveToFile(aDia.FileName + '.aaa.rwp');
        finally
          oRwp.Free;
        end;

        ShowMessage('File saved as' +#13#10 + aDia.FileName + #13#10 + 'Size ' + IntToStr(iiS.Size));
     end;
   finally
     aDia.Free;
   end;end;

procedure TEDIT_CO_REPORTS.Loaded;
var i: integer;
begin
  inherited;
  if Assigned(Application) then
     for i := 1 to System.ParamCount do
       if AnsiCompareText(System.Paramstr(i), 'xml') = 0 then
       begin
         Button1.Visible := True;
         Button2.Visible := True;
         Button3.Visible := True;
         Exit;
       end;
end;

initialization
  RegisterClass(TEDIT_CO_REPORTS);

end.


