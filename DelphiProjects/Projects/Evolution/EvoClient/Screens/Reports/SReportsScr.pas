// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SReportsScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls,
  Controls, Buttons, Classes, ExtCtrls, EvTypes, EvUtils, SDataStructure,
  ActnList, Menus, ImgList, ToolWin, Graphics, ISBasicClasses, EvMainboard,
  EvCommonInterfaces, EvExceptions, EvUIComponents, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TReportsFrm = class(TFramePackageTmpl, IevReportsScreens)
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  ActivatePackage(WorkPlace: TWinControl): Boolean; override;
    function  PackageBitmap: TBitmap; override;
  end;


implementation

var
  ReportsFrm: TReportsFrm;

function GetReportsScreens: IevReportsScreens;
begin
  if not Assigned(ReportsFrm) then
    ReportsFrm := TReportsFrm.Create(nil);
  Result := ReportsFrm;
end;


{$R *.DFM}

{ TSystemFrm }

function TReportsFrm.ActivatePackage(WorkPlace: TWinControl): Boolean;
begin
  Result := inherited ActivatePackage(WorkPlace);
  if Result then
  begin
    DM_TEMPORARY.TMP_CL.Activate;
    if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
      raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);
  end;
end;

function TReportsFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TReportsFrm.PackageCaption: string;
begin
  Result := '&Reports';
end;

function TReportsFrm.PackageSortPosition: string;
begin
  Result := '112';
end;

procedure TReportsFrm.UserPackageStructure;
begin
  AddStructure('Run Reports|030', 'TCO_REPORTS_RUN');
  AddStructure('Set up Reports|040', 'TEDIT_CO_REPORTS');
  AddStructure('Favorite Reports|050', 'TEDIT_CO_USER_REPORTS');
  AddStructure('Reprint Checks/Reports|060', 'TEDIT_PR_CHECK_REPRINT');
  AddStructure('Print Blank Checks|070', 'TPRINT_BLANK_CHECKS');
  AddStructure('Ad Hoc Reports|080', 'TEDIT_CL_ADHOC_REPORTS');
  AddStructure('Report Search|090', 'TREPORT_SEARCH');
  AddStructure('Grid Reporting|100', 'TEDIT_CL_REPORT_SCREEN');


end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetReportsScreens, IevReportsScreens, 'Screens Reports');

finalization
  ReportsFrm.Free;

end.
