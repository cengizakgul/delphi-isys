// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_REPORT_SCREEN;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  StdCtrls, Variants, EvBasicUtils, ISUtils, ISZippingRoutines, Printers,
  ComCtrls, Grids, Wwdbigrd, Wwdbgrid, Buttons, wwdblook, DBCtrls, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, ExtCtrls,  SFrameEntry, EvContext, EvMainboard,
  wwdbdatetimepicker, Spin, EvTypes, Menus,
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, Wwdatsrc, isBaseClasses, EvCommonInterfaces,
  EvDataAccessComponents, ISDataAccessComponents, isBasicUtils, EvExceptions,ISErrorUtils,
  EvDataSet, SDDClasses, SDataDictsystem, SDataStructure,
  Wwdbdlg, CheckLst, SQEEGrid, sRemotePayrollUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, ImgList, isUIFashionPanel;

type
 TEDIT_CL_REPORT_SCREEN = class(TFrameEntry)
    cdCompanyFilter: TevClientDataSet;
    dsCompanyFilter: TevDataSource;
    cdClientFilter: TevClientDataSet;
    dsClientFilter: TevDataSource;
    cdPayrollFilter: TevClientDataSet;
    dsPayrollFilter: TevDataSource;
    cdCheckPeriod: TevClientDataSet;
    dsCheckPeriod: TevDataSource;
    dsEEList: TevDataSource;
    cdEEList: TevClientDataSet;
    cdDBDT: TevClientDataSet;
    dsDBDT: TevDataSource;
    ilTabImages: TevImageList;
    gbTop: TevGroupBox;
    pnlDateRange: TevPanel;
    btnDateList: TevBitBtn;
    pgList: TevPageControl;
    tsClient: TTabSheet;
    tsCompany: TTabSheet;
    grdCompFilter: TevDBCheckGrid;
    tsDBDT: TTabSheet;
    grdDBDT: TevDBCheckGrid;
    tsEE: TTabSheet;
    grEEList: TevDBCheckGrid;
    tsPayroll: TTabSheet;
    grCheckPeriod: TevDBCheckGrid;
    tsGrids: TTabSheet;
    QEEGrid: TQEEGrid;
    sbClient: TScrollBox;
    pnlClientBorder: TevPanel;
    fpClient: TisUIFashionPanel;
    grdClientFilter: TevDBGrid;
    sbCompany: TScrollBox;
    pnlCompanyBorder: TevPanel;
    fpCompany: TisUIFashionPanel;
    sbDBDT: TScrollBox;
    pnlDBDTBorder: TevPanel;
    fpDBDT: TisUIFashionPanel;
    sbEmployee: TScrollBox;
    pnlEmployeeBorder: TevPanel;
    fpEmployee: TisUIFashionPanel;
    sbPayroll: TScrollBox;
    pnlPayrollBorder: TevPanel;
    fpPayroll: TisUIFashionPanel;
    ScrollBox1: TScrollBox;
    evPanel1: TevPanel;
    fpGrid: TisUIFashionPanel;
    pnlFPGridBody: TevPanel;
    pnlDarkHeader: TevPanel;
    lablClient: TevLabel;
    lblClientName: TevLabel;
    pcDateOptions: TevPageControl;
    tsDateRange: TTabSheet;
    tsQuarters: TTabSheet;
    evBegLabel: TevLabel;
    evEndLabel: TevLabel;
    cbDateFrom: TevDateTimePicker;
    cbDateTill: TevDateTimePicker;
    cbQYears: TevComboBox;
    lblYear: TevLabel;
    cblQuarter: TevCheckListBox;
    rgDateType: TevRadioGroup;
    rgCheckDate: TevRadioGroup;
    evLabel1: TevLabel;
    cbGrids: TevComboBox;
    btnAllOrNone: TevButton;
    cbPriorYear: TevCheckBox;
    cbNextYear: TevCheckBox;
    edHiddenField: TEdit;
    btnApplyPeriod: TevButton;


    procedure rgDateTypeClick(Sender: TObject);
    procedure pgListChange(Sender: TObject);
    procedure dsClientFilterDataChange(Sender: TObject; Field: TField);
    procedure cbGridsChange(Sender: TObject);
    procedure cbQYearsChange(Sender: TObject);
    procedure cbPriorYearClick(Sender: TObject);
    procedure pnlDateRangeResize(Sender: TObject);
    procedure btnAllOrNoneClick(Sender: TObject);
    procedure pcDateOptionsChange(Sender: TObject);
    procedure btnDateListClick(Sender: TObject);
    procedure btnApplyPeriodClick(Sender: TObject);

  private
    { Private declarations }

    FClNbr  : integer;
    FCoList, FDBDTList, FPayrollFilter, FBatchFilter, FCheckFilter, FEEFilter  : String;

    procedure ShowGrids;
    procedure GetClientList;
    procedure GetCompanyList;
    Function GetFieldValueListFromDataset(const cd: TevClientDataset; const Field : String;
                                               const MergeFilter : String = ''):String;

    function GetRealFilter(const Filter, Field : String; const sAndOr: String=' and '): string;
    procedure DatasetsClose;
    function Get_Filter_With_Filter(const Table, Field, FilterField, Filter : String;
                                                   const ExtraFilter : String = ''; const bCoNbr : Boolean = false) : String;

    procedure Get_cdCheckPeriodList;
    procedure Get_cdEEList;
    procedure Get_cdDBDTList;

    function FillOutCompanyFilter:String;
    function FillOutDBDTFilter:String;

    function GetEENbr(const Filter : String = ''):string;

    procedure CreateActivateIndex(const d: TevClientDataset; const IName, Fields: string);
    Procedure UpdateQuarterRange(const bSetDefault : boolean=True);
    Procedure GridRefresh(const pRefresh:boolean= false);
  protected

  public
    { Public declarations }

    procedure Activate; override;
    procedure DeActivate; override;
  end;

var
  EDIT_CL_REPORT_SCREEN: TEDIT_CL_REPORT_SCREEN;

implementation

uses
  EvConsts,  EvUtils,  EvStreamUtils, DateUtils;

{$R *.DFM}

{ TEDIT_CL_REPORT_SCREEN }

procedure TEDIT_CL_REPORT_SCREEN.CreateActivateIndex(const d: TevClientDataset; const IName, Fields: string);
begin
    d.AddIndex(IName, Fields, []);
    d.IndexDefs.Update;
    d.IndexName := IName;
end;

procedure TEDIT_CL_REPORT_SCREEN.DatasetsClose;
begin
     cdCheckPeriod.Close;
     cdEEList.Close;
     cdCompanyFilter.Close;
     cdDBDT.Close;

     FEEFilter := '-1';
     FPayrollFilter := '-1';
     FBatchFilter := '-1';
     FCheckFilter := '-1';
end;

procedure TEDIT_CL_REPORT_SCREEN.DeActivate;
begin
   QEEGrid.Deactivate;
end;

Procedure  TEDIT_CL_REPORT_SCREEN.UpdateQuarterRange(const bSetDefault : boolean=True);
var
  pYear  : integer;
begin
    pYear := StrToInt(cbQYears.Items[cbQYears.ItemIndex]);
    cblQuarter.Clear;
    with cblQuarter do
    begin
      //Added check box for prior year. Only add if desired.
      if cbPriorYear.Checked then
      begin
        AddItem('1st quarter '+ IntToStr(pYear-1), Pointer(Trunc(EncodeDate(pYear-1,   1, 1))));
        AddItem('2nd quarter '+ IntToStr(pYear-1), Pointer(Trunc(EncodeDate(pYear-1,   4, 1))));
        AddItem('3rd quarter '+ IntToStr(pYear-1), Pointer(Trunc(EncodeDate(pYear-1,   7, 1))));
        AddItem('4th quarter '+ IntToStr(pYear-1), Pointer(Trunc(EncodeDate(pYear-1,  10, 1))));
      end;
      //Add the current year regardless
      AddItem('1st quarter '+ IntToStr(pYear)  , Pointer(Trunc(EncodeDate(pYear  ,   1, 1))));
      AddItem('2nd quarter '+ IntToStr(pYear)  , Pointer(Trunc(EncodeDate(pYear  ,   4, 1))));
      AddItem('3rd quarter '+ IntToStr(pYear)  , Pointer(Trunc(EncodeDate(pYear  ,   7, 1))));
      AddItem('4th quarter '+ IntToStr(pYear)  , Pointer(Trunc(EncodeDate(pYear  ,  10, 1))));

      //add next year quarters only if desired.
      if cbNextYear.Checked then
      begin
        AddItem('1st quarter '+ IntToStr(pYear+1), Pointer(Trunc(EncodeDate(pYear+1, 1, 1))));
        AddItem('2nd quarter '+ IntToStr(pYear+1), Pointer(Trunc(EncodeDate(pYear+1, 4, 1))));
        AddItem('3rd quarter '+ IntToStr(pYear+1), Pointer(Trunc(EncodeDate(pYear+1, 7, 1))));
        AddItem('4th quarter '+ IntToStr(pYear+1), Pointer(Trunc(EncodeDate(pYear+1,10, 1))));
      end;
    end;
    if bSetDefault then
    begin
       //this needed to be modified to support the check boxes as well.
       if cbPriorYear.Checked then
         //only add 3 if the prior year is in the list.
         cblQuarter.Checked[GetQuarterNumber(Date)+ 3] := True
       else
         //the check state of the Next year is irrelevant to this exercise
         cblQuarter.Checked[GetQuarterNumber(Date)] := True;
    end;
end;

procedure TEDIT_CL_REPORT_SCREEN.cbQYearsChange(Sender: TObject);
begin
  inherited;
  btnAllOrNone.Caption := 'Select All';
  UpdateQuarterRange(false);
end;

procedure TEDIT_CL_REPORT_SCREEN.Activate;
var
      pYear, pYearEnd : integer;
begin
    inherited;

    cbDateFrom.DateTime := GetBeginQuarter(GetBeginMonth(Date)-1);
    cbDateTill.DateTime := GetEndQuarter(cbDateFrom.DateTime);

    pYear    := GetYear(Now)-5;
    pYearEnd := GetYear(Now) + 2;
    cbQYears.Clear;
    with cbQYears do
    begin
      repeat
        Items.Add(IntToStr(pYear));
        inc(pYear);
      until pYear > pYearEnd;
    end;
    cbQYears.ItemIndex := 5;

    UpdateQuarterRange;

    FClNbr := -1;
    FCoList :='-1';

    GetClientList;
    pgList.ActivatePage(tsClient);
    cbPriorYear.Checked := false;
    rgDateType.OnClick(nil);
end;

function TEDIT_CL_REPORT_SCREEN.GetRealFilter(const Filter, Field : String; const sAndOr: String=' and '): string;
 Function TestFilter(const Filter: String):String;
 var
   tmpList : TStringList;
 begin
    tmpList := TStringList.Create;
    tmpList.Sorted := True;
    tmpList.Duplicates := Classes.dupIgnore;
    try
     tmpList.CommaText := Filter;
     Result := tmpList.CommaText;
     if Result[1] = ',' then
        Delete(Result,1,1);
    finally
     tmpList.Free;
    end;
 end;
begin
  Result := '';
  if Length(Field) = 0 then exit;
  if Length(Filter) > 0 then
      Result := sAndOr + BuildINsqlStatement(Field, TestFilter(Filter));
end;

Function TEDIT_CL_REPORT_SCREEN.GetFieldValueListFromDataset(const cd: TevClientDataset; const Field : String; const MergeFilter : String = ''):String;
var
    pList: TStringList;
Begin
    Result := '';
    pList := cd.GetFieldValueList(Field,True);
    try
      Result := pList.CommaText;
    finally
      pList.Free;
      if Length(MergeFilter) = 0 then
      begin
       if Length(Result) = 0 then
          Result := '-1';
      end
      else
      begin
      if Length(Result) = 0 then
          Result := MergeFilter
        else
          Result := Result + ',' + MergeFilter;
      end;
    end;
end;

function TEDIT_CL_REPORT_SCREEN.Get_Filter_With_Filter(const Table, Field, FilterField, Filter : String;
                                                  const ExtraFilter : String = ''; const bCoNbr : Boolean = false) : String;
var
 cdFilter: TevClientDataset;
 s, sCoFilter : String;
begin
    sCoFilter := '';
    cdFilter := TevClientDataset.Create(nil);
    try
       if bCoNbr then
          sCoFilter := ' and CO_NBR in ( ' + FCoList + ' ) ';

        s := 'select ' + Field  + ' from ' + Table + ' where 1=1 and {AsOfNow<'+ Table +'>} '+
                   GetRealFilter(Filter, FilterField ) +
                   ExtraFilter +
                   sCoFilter;
        with TExecDSWrapper.Create(s) do
        begin
          ctx_DataAccess.GetCustomData(cdFilter, AsVariant);
        end;
         result := GetFieldValueListFromDataset(cdFilter,Field);
    Finally
     cdFilter.Free;
    End;
end;


procedure TEDIT_CL_REPORT_SCREEN.ShowGrids;

  Procedure GetPayrollBatchFilter;
  var
     i : integer;
     sComma : string;
     pBookMark: TBookmark;
  begin
    FPayrollFilter := '-1';
    FBatchFilter := '-1';
    sComma := '';

    if (grCheckPeriod.SelectedList.Count > 0) then
    begin
      cdCheckPeriod.DisableControls;
      pBookMark := cdCheckPeriod.GetBookmark;
      try

        for i := 0 to Pred(grCheckPeriod.SelectedList.Count) do
        begin
          cdCheckPeriod.GotoBookmark(grCheckPeriod.SelectedList[i]);

          if Length(Trim(FPayrollFilter)) > 0 then sComma := ',';
          FPayrollFilter := FPayrollFilter + sComma + IntToStr(cdCheckPeriod['PR_NBR']);
          if rgCheckDate.ItemIndex = 1 then
            FBatchFilter := FBatchFilter + sComma + IntToStr(cdCheckPeriod['PR_BATCH_NBR'])
        end;
      finally

       if rgCheckDate.ItemIndex = 0 then
           FBatchFilter  := Get_Filter_With_Filter('PR_BATCH','PR_BATCH_NBR', 'PR_NBR', FPayrollFilter);

        cdCheckPeriod.GotoBookmark(pBookMark);
        cdCheckPeriod.FreeBookmark(pBookMark);
        cdCheckPeriod.EnableControls;
      end;
    end;
  end;

  Procedure GetEEFilter;
  var
     i : integer;
     sComma : string;
     pBookMark: TBookmark;
  begin
    FEEFilter := '-1';
    sComma := '';

    if not cdEEList.Active then
        FEEFilter := GetEENbr
    else
      if (grEEList.SelectedList.Count > 0) then
      begin
        cdEEList.DisableControls;
        pBookMark := cdEEList.GetBookmark;
        try
          for i := 0 to Pred(grEEList.SelectedList.Count) do
          begin
            cdEEList.GotoBookmark(grEEList.SelectedList[i]);
            if Length(Trim(FEEFilter)) > 0 then sComma := ',';
            FEEFilter := FEEFilter + sComma + IntToStr(cdEEList['EE_NBR']);
          end;
        finally
          cdEEList.GotoBookmark(pBookMark);
          cdEEList.FreeBookmark(pBookMark);
          cdEEList.EnableControls;
        end;
      end;
  end;

begin
  GetPayrollBatchFilter;
  GetEEFilter;
  FCheckFilter  := Get_Filter_With_Filter('PR_CHECK','PR_CHECK_NBR', 'PR_BATCH_NBR', FBatchFilter,' and ' + BuildINsqlStatement('EE_NBR', FEEFilter));

  QEEGrid.sFCoList := FCoList;
  QEEGrid.sFCheckFilter := FCheckFilter;
  QEEGrid.sFEEFilter := FEEFilter;
  QEEGrid.sFBatchFilter := FBatchFilter;
  QEEGrid.sFPayrollFilter := FPayrollFilter;

  QEEGrid.gridIndex := gCheckLines;
  if cbGrids.ItemIndex = Ord(gChecks) then
     QEEGrid.gridIndex := gChecks;

  QEEGrid.Activate;
end;

procedure TEDIT_CL_REPORT_SCREEN.rgDateTypeClick(Sender: TObject);
begin
  inherited;

{  cbDateFrom.Visible := rgDateType.ItemIndex = 0;
  //changed following from visible to enabled to keep the "box of controls" constant
  cbDateTill.Enabled := rgDateType.ItemIndex = 0;
  evEndLabel.Enabled := rgDateType.ItemIndex = 0;
  cblQuarter.Enabled := rgDateType.ItemIndex = 1;
  cbQYears.Visible   := rgDateType.ItemIndex = 1;
  //added to support new check boxes introduced October 10, 2012 by Craig
  cbPriorYear.Enabled := rgDateType.ItemIndex = 1;
  cbNextYear.Enabled  := rgDateType.ItemIndex = 1;

  evBegLabel.Caption := 'Begin Date';
  if rgDateType.ItemIndex = 1 then
     evBegLabel.Caption := ' Year ';     }

{ //    87453
  if rgDateType.ItemIndex = 0 then
  begin
     for i := 0 to Pred(cblQuarter.Items.Count) do
       if cblQuarter.Checked[i] then
       begin
         cbDateFrom.DateTime := integer(cblQuarter.Items.Objects[i]);
         cbDateTill.DateTime := GetEndQuarter(cbDateFrom.DateTime);
         break;
       end;
  end;
}


 // Get_cdCheckPeriodList;
end;

procedure TEDIT_CL_REPORT_SCREEN.dsClientFilterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  //changed from gbTop.Caption to:
  lblClientName.Caption := Trim(cdClientFilter.FieldByName('Custom_Client_Number').AsString) + '          ' +
                        cdClientFilter.FieldByName('Name').AsString;
end;

Procedure TEDIT_CL_REPORT_SCREEN.GridRefresh(const pRefresh:boolean= false);
var
 sCotmp, sDBDTtmp : String;
 bEErefresh : Boolean;
 bcdCheckPeriodList: boolean;
begin

  if (pgList.ActivePage <>  tsClient) and
     (pgList.ActivePage <>  tsGrids)  then
  begin
    if cdClientFilter.Active then
    begin
      sCotmp := FillOutCompanyFilter;
      sDBDTtmp := FillOutDBDTFilter;
      bEErefresh := false;
      bcdCheckPeriodList:=pRefresh;

      if (FClNbr <>  cdClientFilter.FieldByName('cl_nbr').AsInteger) then
      begin
          DatasetsClose;
          FClNbr := cdClientFilter.FieldByName('cl_nbr').AsInteger;
          ctx_DataAccess.OpenClient(FClNbr);

          GetCompanyList;
          FCoList := FillOutCompanyFilter;
          Get_cdDBDTList;

          bcdCheckPeriodList:= true;
          bEErefresh := True;
      end
      else
      if (FCoList <> sCotmp) then
      begin
           FCoList := sCotmp;
           bEErefresh := True;
           Get_cdDBDTList;
           bcdCheckPeriodList:= true;
      end
      else
      if (sDBDTtmp <> '') and
          (FDBDTList <> sDBDTtmp) then
      begin
        FDBDTList := sDBDTtmp;
        bEErefresh := True;
      end;

      if bcdCheckPeriodList then
         Get_cdCheckPeriodList;

      if bEErefresh then
         Get_cdEEList;
    end;
  end;

  if pgList.ActivePage = tsGrids then
     ShowGrids;
end;


procedure TEDIT_CL_REPORT_SCREEN.pgListChange(Sender: TObject);
begin
  inherited;
  GridRefresh;
end;

procedure TEDIT_CL_REPORT_SCREEN.cbGridsChange(Sender: TObject);
begin
  inherited;
  if pgList.ActivePage = tsGrids then
     ShowGrids;
end;


procedure TEDIT_CL_REPORT_SCREEN.GetClientList;
var
   s : String;
begin
   grdClientFilter.Selected.Clear;
    AddSelected( grdClientFilter.Selected,'Custom_Client_Number', 22, 'Client Number', True);
    AddSelected( grdClientFilter.Selected,'Name', 40, 'Name', True);
    AddSelected( grdClientFilter.Selected,'cl_nbr', 10, 'CL#', false);

  cdClientFilter.DisableControls;
  try
    s :='select Custom_Client_Number,Name,cl_nbr from TMP_CL order by Custom_Client_Number ';
    cdClientFilter.Close;
    with TExecDSWrapper.Create(s) do
    begin
      ctx_DataAccess.GetTmpCustomData(cdClientFilter, AsVariant);
    end;
  finally
    cdClientFilter.First;
    cdClientFilter.EnableControls;
  end;
end;


procedure TEDIT_CL_REPORT_SCREEN.GetCompanyList;
var
  s : String;
begin
  grdCompFilter.Selected.Clear;
  AddSelected( grdCompFilter.Selected,'custom_company_number', 22, 'Company Number', True);
  AddSelected( grdCompFilter.Selected,'Name', 40, 'Name', True);
  AddSelected( grdCompFilter.Selected,'Custom_Client_Number', 22, 'Client Number', True);
  AddSelected( grdCompFilter.Selected,'ClientName', 40, 'Client Name', True);
  AddSelected( grdCompFilter.Selected,'cl_nbr', 10, 'CL#', false);
  AddSelected( grdCompFilter.Selected,'co_nbr', 10, 'CO#', false);

  s :='select b.custom_company_number, b.name , a.Custom_Client_Number Client_Number, a.Name ClientName, a.cl_nbr, b.co_nbr ' +
       ' from TMP_CL a, TMP_CO b ' +
       ' where a.CL_NBR = b.CL_NBR and ' +
           ' b.CL_NBR = ' + cdClientFilter.FieldByName('cl_nbr').AsString +
       ' order by b.custom_company_number ';
   cdCompanyFilter.Close;
   with TExecDSWrapper.Create(s) do
   begin
    ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
    ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
    cdCompanyFilter.Data := ctx_DataAccess.TEMP_CUSTOM_VIEW.Data;
   end;
   grdCompFilter.SelectAll;
end;


function TEDIT_CL_REPORT_SCREEN.FillOutCompanyFilter:String;
var
  i : Integer;
  sComma : string;
  pBookMark: TBookmark;
  ds: TDataSet;
begin
    Result := '-1';
    ds := grdCompFilter.DataSource.DataSet;
    ds.DisableControls;
    pBookMark := ds.GetBookmark;
    try
      for i := 0 to grdCompFilter.SelectedList.Count - 1 do
      begin
        ds.GotoBookmark(grdCompFilter.SelectedList.Items[i]);
        if Length(Trim(Result)) > 0 then sComma := ',';
        Result := Result + sComma + IntToStr(ds['co_nbr'])
      end;
    finally
      ds.GotoBookmark(pBookMark);
      ds.FreeBookmark(pBookMark);
      ds.EnableControls;
    end;
end;

procedure TEDIT_CL_REPORT_SCREEN.Get_cdDBDTList;
var
   s : String;
Begin
    grdDBDT.Selected.Clear;
    AddSelected( grdDBDT.Selected,'CUSTOM_COMPANY_NUMBER', 22, 'Company Number', True);
    AddSelected( grdDBDT.Selected,'NAME', 40, 'Company Name', True);
    AddSelected( grdDBDT.Selected,'HOME_DBDT', 30, 'Home DBDT', True);
    AddSelected( grdDBDT.Selected,'DivName', 30, 'Division Name', false);
    AddSelected( grdDBDT.Selected,'BranchName', 30, 'Branch Name', false);
    AddSelected( grdDBDT.Selected,'DeptName', 30, 'Department Name', false);
    AddSelected( grdDBDT.Selected,'TeamName', 30, 'Team Name', false);

     s :=
      'select distinct ' +
         'b.CO_DIVISION_NBR, c.CO_BRANCH_NBR, d.CO_DEPARTMENT_NBR, e.CO_TEAM_NBR, f.CO_NBR,  ' +
          ' f.CUSTOM_COMPANY_NUMBER, f.NAME,' +
          'coalesce(trim(b.CUSTOM_DIVISION_NUMBER), '''') || ' +
          'coalesce('' | ''' + ' || trim(c.CUSTOM_BRANCH_NUMBER)  , '''') || ' +
          'coalesce('' | ''' + ' || trim(d.CUSTOM_DEPARTMENT_NUMBER) , '''') || ' +
          'coalesce('' | ''' + ' || trim(e.CUSTOM_TEAM_NUMBER) , '''') HOME_DBDT, ' +

          'b.CUSTOM_DIVISION_NUMBER, b.NAME DivName, ' +
          'c.CUSTOM_BRANCH_NUMBER, c.NAME BranchName, ' +
          'd.CUSTOM_DEPARTMENT_NUMBER, d.NAME DeptName, ' +
          'e.CUSTOM_TEAM_NUMBER, e.NAME TeamName ' +
      'from ' +
        'EE a ' +
        'left join CO_DIVISION b on a.CO_DIVISION_NBR = b.CO_DIVISION_NBR and {AsOfNow<b>} ' +
        'left join CO_BRANCH c on a.CO_BRANCH_NBR = c.CO_BRANCH_NBR and {AsOfNow<c>} ' +
        'left join CO_DEPARTMENT d on a.CO_DEPARTMENT_NBR = d.CO_DEPARTMENT_NBR and {AsOfNow<d>} '+
        'left join CO_TEAM e on a.CO_TEAM_NBR = e.CO_TEAM_NBR and {AsOfNow<e>} ' +
        'join CO f on a.CO_NBR = f.CO_NBR and {AsOfNow<f>} ' +

        'where {AsOfNow<a>} ' +
           ' and a.CO_NBR in ( ' + FCoList + ' ) ' +
        'order by   CUSTOM_COMPANY_NUMBER, HOME_DBDT ';
   cdDBDT.Close;
   with TExecDSWrapper.Create(s) do
   begin
     ctx_DataAccess.CUSTOM_VIEW.Close;
     ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
     ctx_DataAccess.CUSTOM_VIEW.Open;
     cdDBDT.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
   end;
   if cdDBDT.Active then
   begin
     cdDBDT.First;
     grdDBDT.SelectAll;
     FDBDTList := FillOutDBDTFilter;
   end;
end;


function TEDIT_CL_REPORT_SCREEN.FillOutDBDTFilter:String;
var
  i : Integer;
  sTmp, sOr : string;
  pBookMark: TBookmark;
  ds: TDataSet;
begin
    Result := '';
    ds := grdDBDT.DataSource.DataSet;
    ds.DisableControls;
    pBookMark := ds.GetBookmark;
    try
      for i := 0 to grdDBDT.SelectedList.Count - 1 do
      begin
        ds.GotoBookmark(grdDBDT.SelectedList.Items[i]);
        if Length(Trim(Result)) > 0 then sOr := ' or ';

        sTmp := '';

        if not ds.FieldByName('CO_NBR').IsNull then
           sTmp := '##CO_NBR = ' + ds.FieldByName('CO_NBR').AsString;
        if not ds.FieldByName('CO_DIVISION_NBR').IsNull then
           sTmp := sTmp + ' and ##CO_DIVISION_NBR = ' + ds.FieldByName('CO_DIVISION_NBR').AsString;
        if not ds.FieldByName('CO_BRANCH_NBR').IsNull then
           sTmp := sTmp + ' and ##CO_BRANCH_NBR = ' + ds.FieldByName('CO_BRANCH_NBR').AsString;
        if not ds.FieldByName('CO_DEPARTMENT_NBR').IsNull then
           sTmp := sTmp + ' and ##CO_DEPARTMENT_NBR = ' + ds.FieldByName('CO_DEPARTMENT_NBR').AsString;
        if not ds.FieldByName('CO_TEAM_NBR').IsNull then
           sTmp := sTmp + ' and ##CO_TEAM_NBR = ' + ds.FieldByName('CO_TEAM_NBR').AsString;
        if sTmp <> '' then
          Result := Result  + sOr + '(' + sTmp  + ')';
      end;
    finally
      ds.GotoBookmark(pBookMark);
      ds.FreeBookmark(pBookMark);
      ds.EnableControls;
      if Result <> '' then
         Result := '(' + Result  + ')';
    end;
end;

procedure TEDIT_CL_REPORT_SCREEN.Get_cdCheckPeriodList;
var
   s, sFilterDate : String;

   function GetPeriodDateFilter(const bd,ed : TDateTime): string;
   begin
      Result := ' (b.PERIOD_BEGIN_DATE between ' + QuotedStr(DateToStr(bd)) + ' and ' + QuotedStr(DateToStr(ed)) + ') ' +
                    ' or (b.PERIOD_END_DATE between ' + QuotedStr(DateToStr(bd)) + ' and ' + QuotedStr(DateToStr(ed)) + ') '
   end;

   function GetCheckDateFilter(const bd,ed : TDateTime): string;
   begin
      Result := '(a.CHECK_DATE between ' + QuotedStr(DateToStr(bd)) + ' and ' + QuotedStr(DateToStr(ed)) + ') ';
   end;

   function GetPeriodFilter : string;
   var
    i : integer;
    bd, ed : TDateTime;
   begin
     Result := '';
     for i := 0 to Pred(cblQuarter.Items.Count) do
       if cblQuarter.Checked[i] then
       begin
           bd  :=  integer(cblQuarter.Items.Objects[i]);
           ed  := GetEndQuarter(bd);
           if Result <> '' then
              Result := Result + ' or ';

          if rgCheckDate.ItemIndex = 0 then
             Result := Result + GetCheckDateFilter(bd,ed)
          else
             Result := Result + GetPeriodDateFilter(bd,ed);
       end;
     if Result <> '' then
       Result := ' and (' + Result + ') '
     else
       Result := ' and ( ' + AlwaysFalseCond + ' ) ';

   end;
Begin
   grCheckPeriod.Selected.Clear;

   AddSelected( grCheckPeriod.Selected, 'CHECK_DATE', 12, 'Check Date', true );
   AddSelected( grCheckPeriod.Selected, 'RUN_NUMBER', 5, 'Run #', true );
   AddSelected( grCheckPeriod.Selected, 'PROCESS_DATE', 12, 'Processed', true );

   if  pcDateOptions.ActivePageIndex = 1 then                          //rgCheckDate.ItemIndex old code
   begin
      AddSelected( grCheckPeriod.Selected, 'PERIOD_BEGIN_DATE', 12, 'Begin Date', true );
      AddSelected( grCheckPeriod.Selected, 'PERIOD_END_DATE', 12, 'End Date', True );

      if rgDateType.ItemIndex = 0 then
         sFilterDate := ' and (' + GetPeriodDateFilter(cbDateFrom.DateTime,cbDateTill.DateTime) + ')'
      else
         sFilterDate := GetPeriodFilter;

      s := 'select  a.CHECK_DATE, a.RUN_NUMBER, a.PROCESS_DATE, b.PERIOD_BEGIN_DATE, b.PERIOD_END_DATE, a.PR_NBR, b.PR_BATCH_NBR, ' +
                      'c.CO_NBR, c.CUSTOM_COMPANY_NUMBER, c.NAME,' +
                      'PAYROLL_TYPE, ' +
                      'CASE a.PAYROLL_TYPE ' +
                       'WHEN ''R'' THEN ''Regular'' ' +
                       'WHEN ''S'' THEN ''Supplemental'' ' +
                       'WHEN ''V'' THEN ''Reversal'' ' +
                       'WHEN ''C'' THEN ''Client Correction'' ' +
                       'WHEN ''B'' THEN ''SB Correction'' ' +
                       'WHEN ''E'' THEN ''Setup Run'' ' +
                       'WHEN ''M'' THEN ''Misc Check Adjustment'' ' +
                       'WHEN ''T'' THEN ''Tax Adjustment'' ' +
                       'WHEN ''Q'' THEN ''Wage Adj with Qtr'' ' +
                       'WHEN ''N'' THEN ''Wage Adj no Qtr'' ' +
                       'WHEN ''A'' THEN ''Qtr End Tax Adjustment'' ' +
                       'WHEN ''D'' THEN ''Tax Deposit'' ' +
                       'WHEN ''I'' THEN ''Import'' ' +
                       'WHEN ''K'' THEN ''Backdated Setup'' ' +
                       'ELSE ''Unknown'' ' +
                     'END Payroll_Type_Desc, ' +
                     'STATUS, ' +
                      'CASE a.STATUS ' +
                       'WHEN ''W'' THEN ''Pending'' ' +
                       'WHEN ''P'' THEN ''Processed'' ' +
                       'WHEN ''C'' THEN ''Completed'' ' +
                       'WHEN ''V'' THEN ''Voided'' ' +
                       'WHEN ''D'' THEN ''Deleted'' ' +
                       'WHEN ''H'' THEN ''Hold'' ' +
                       'WHEN ''R'' THEN ''SB Review'' ' +
                       'WHEN ''I'' THEN ''Processing'' ' +
                       'WHEN ''Y'' THEN ''Pre-Processing'' ' +
                       'WHEN ''B'' THEN ''Backdated'' ' +
                       'WHEN ''A'' THEN ''Required Mgr.Approval'' ' +
                      'ELSE ''Unknown'' ' +
                     'END Status_Desc ' +
              'from  PR a, PR_BATCH b, CO c where a.PR_NBR = b.PR_NBR and a.CO_NBR = c.CO_NBR and ' +
                       ' a.PAYROLL_TYPE not in (''D'',''T'', ''M'' ) ' +
                        sFilterDate +
                        ' and {AsOfNow<a>} ' +
                        ' and {AsOfNow<b>} ' +
                        ' and {AsOfNow<c>} ' +
                        ' and c.CO_NBR in ( ' + FCoList + ' ) ';
   end
   else
   begin

    if pcDateOptions.ActivePageIndex = 0 then     //old code rgDateType.ItemIndex
       sFilterDate := ' and ' + GetCheckDateFilter(cbDateFrom.DateTime,cbDateTill.DateTime)
    else
       sFilterDate := GetPeriodFilter;

    s := 'select a.CHECK_DATE, a.RUN_NUMBER, a.PROCESS_DATE, a.PR_NBR, b.CO_NBR, b.CUSTOM_COMPANY_NUMBER, b.NAME,' +
                     'PAYROLL_TYPE, ' +
                     'CASE PAYROLL_TYPE ' +
                       'WHEN ''R'' THEN ''Regular'' ' +
                       'WHEN ''S'' THEN ''Supplemental'' ' +
                       'WHEN ''V'' THEN ''Reversal'' ' +
                       'WHEN ''C'' THEN ''Client Correction'' ' +
                       'WHEN ''B'' THEN ''SB Correction'' ' +
                       'WHEN ''E'' THEN ''Setup Run'' ' +
                       'WHEN ''M'' THEN ''Misc Check Adjustment'' ' +
                       'WHEN ''T'' THEN ''Tax Adjustment'' ' +
                       'WHEN ''Q'' THEN ''Wage Adj with Qtr'' ' +
                       'WHEN ''N'' THEN ''Wage Adj no Qtr'' ' +
                       'WHEN ''A'' THEN ''Qtr End Tax Adjustment'' ' +
                       'WHEN ''D'' THEN ''Tax Deposit'' ' +
                       'WHEN ''I'' THEN ''Import'' ' +
                       'WHEN ''K'' THEN ''Backdated Setup'' ' +
                       'ELSE ''Unknown'' ' +
                    'END Payroll_Type_Desc, ' +
                     'STATUS, ' +
                      'CASE STATUS ' +
                       'WHEN ''W'' THEN ''Pending'' ' +
                       'WHEN ''P'' THEN ''Processed'' ' +
                       'WHEN ''C'' THEN ''Completed'' ' +
                       'WHEN ''V'' THEN ''Voided'' ' +
                       'WHEN ''D'' THEN ''Deleted'' ' +
                       'WHEN ''H'' THEN ''Hold'' ' +
                       'WHEN ''R'' THEN ''SB Review'' ' +
                       'WHEN ''I'' THEN ''Processing'' ' +
                       'WHEN ''Y'' THEN ''Pre-Processing'' ' +
                       'WHEN ''B'' THEN ''Backdated'' ' +
                       'WHEN ''A'' THEN ''Required Mgr.Approval'' ' +
                      'ELSE ''Unknown'' ' +
                     'END Status_Desc ' +
            'from  PR a, CO b where a.CO_NBR = b.CO_NBR and ' +
              ' a.PAYROLL_TYPE not in (''D'',''T'', ''M'' ) ' +
               sFilterDate +
               ' and {AsOfNow<a>} ' +
               ' and {AsOfNow<b>} ' +
               ' and b.CO_NBR in ( ' + FCoList + ' ) ';
   end;

   AddSelected( grCheckPeriod.Selected, 'Payroll_Type_Desc', 22, 'Payroll type', true );
   AddSelected( grCheckPeriod.Selected, 'Status_Desc', 22, 'Status', true );
   AddSelected( grCheckPeriod.Selected, 'CUSTOM_COMPANY_NUMBER', 22, 'Company Number', true );
   AddSelected( grCheckPeriod.Selected, 'NAME', 40, 'Company Name', true );

   cdCheckPeriod.Close;
   with TExecDSWrapper.Create(s) do
   begin
     ctx_DataAccess.CUSTOM_VIEW.Close;
     ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
     ctx_DataAccess.CUSTOM_VIEW.Open;
     cdCheckPeriod.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
   end;
   CreateActivateIndex(cdCheckPeriod,'NBR','CHECK_DATE;RUN_NUMBER');
   cdCheckPeriod.First;
end;

procedure TEDIT_CL_REPORT_SCREEN.Get_cdEEList;
var
   s: String;
Begin
  grEEList.Selected.Clear;
  AddSelected( grEEList.Selected, 'CUSTOM_EMPLOYEE_NUMBER', 9, 'EE Code', True);
  AddSelected( grEEList.Selected, 'FIRST_NAME', 20, 'First name', true );
  AddSelected( grEEList.Selected, 'LAST_NAME', 20, 'Last name', true );
  AddSelected( grEEList.Selected, 'SOCIAL_SECURITY_NUMBER', 11, 'SSN', true );
  AddSelected( grEEList.Selected, 'Status_Desc', 32, 'Status', True);
  AddSelected( grEEList.Selected, 'HOME_DBDT', 40, 'Home DBDT', True);
  AddSelected( grEEList.Selected, 'CUSTOM_COMPANY_NUMBER', 22, 'Company Number', True);

     s := 'select ' +
            ' a.CUSTOM_EMPLOYEE_NUMBER, b.FIRST_NAME, b.LAST_NAME, a.EE_NBR, b.CL_PERSON_NBR, ' +
            ' a.CURRENT_TERMINATION_CODE, b.SOCIAL_SECURITY_NUMBER, ' +
              'CASE CURRENT_TERMINATION_CODE ' +
                'WHEN ''A'' THEN ''Active'' ' +
                'WHEN ''I'' THEN ''Involuntary Layoff'' ' +
                'WHEN ''C'' THEN ''Involuntary Termination Due to Misconduct'' ' +
                'WHEN ''L'' THEN ''Termination Due to Layoff'' ' +
                'WHEN ''F'' THEN ''Release Without Prejudice'' ' +
                'WHEN ''R'' THEN ''Voluntary Resignation'' ' +
                'WHEN ''E'' THEN ''Termination Due to Retirement'' ' +
                'WHEN ''T'' THEN ''Termination Due to Transfer'' ' +
                'WHEN ''Y'' THEN ''Military Leave'' ' +
                'WHEN ''V'' THEN ''Leave of Absence'' ' +
                'WHEN ''S'' THEN ''Seasonal'' ' +
                'WHEN ''D'' THEN ''Termination Due to Death'' ' +
                'WHEN ''M'' THEN ''Terminated'' ' +
                'WHEN ''P'' THEN ''Suspended'' ' +
                'WHEN ''Z'' THEN ''FMLA'' ' +
                'WHEN ''N'' THEN ''Other/See Notes'' ' +
                'ELSE ''Unknown'' ' +
              'END Status_Desc, ' +

          'coalesce(trim(d1.CUSTOM_DIVISION_NUMBER), '''') || ' +
          'coalesce('' | ''' + ' || trim(d2.CUSTOM_BRANCH_NUMBER)  , '''') || ' +
          'coalesce('' | ''' + ' || trim(d3.CUSTOM_DEPARTMENT_NUMBER) , '''') || ' +
          'coalesce('' | ''' + ' || trim(d4.CUSTOM_TEAM_NUMBER) , '''') HOME_DBDT, ' +

            ' c.CO_NBR, c.CUSTOM_COMPANY_NUMBER ' +
         ' from EE a ' +
        'join CL_PERSON b on a.CL_PERSON_NBR = b.CL_PERSON_NBR and {AsOfNow<b>} ' +
        'join CO c on a.CO_NBR = c.CO_NBR and {AsOfNow<c>} and c.CO_NBR in ( ' + FCoList + ' ) ' +

        'left join CO_DIVISION d1 on a.CO_DIVISION_NBR = d1.CO_DIVISION_NBR and {AsOfNow<d1>} ' +
        'left join CO_BRANCH d2 on a.CO_BRANCH_NBR = d2.CO_BRANCH_NBR and {AsOfNow<d2>} ' +
        'left join CO_DEPARTMENT d3 on a.CO_DEPARTMENT_NBR = d3.CO_DEPARTMENT_NBR and {AsOfNow<d3>} '+
        'left join CO_TEAM d4 on a.CO_TEAM_NBR = d4.CO_TEAM_NBR and {AsOfNow<d4>} ' +
         ' where {AsOfNow<a>} ';

     if FDBDTList <> '' then
        s := s + ' and ' + StringReplace(FDBDTList, '##', 'a.', [rfReplaceAll]);

   cdEEList.Close;
   with TExecDSWrapper.Create(s) do
   begin
     ctx_DataAccess.CUSTOM_VIEW.Close;
     ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
     ctx_DataAccess.CUSTOM_VIEW.Open;
     cdEEList.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
   end;
   CreateActivateIndex(cdEEList,'NBR','CUSTOM_EMPLOYEE_NUMBER');
   if cdEEList.Active then
   begin
     grEEList.SelectAll;
     cdEeList.First;
   end;
end;

function TEDIT_CL_REPORT_SCREEN.GetEENbr(const Filter : String = ''):string;
var
cd : TevClientDataSet;
s : String;
begin
      result := '';
      cd := TevClientDataset.Create(nil);
      try
         s :=
          'select a.EE_NBR from EE a, CL_PERSON b ' +
            'where a.CL_PERSON_NBR = b.CL_PERSON_NBR and ' +
              ' {AsOfNow<a>} and {AsOfNow<b>} ' +
              ' and a.CO_NBR in ( ' + FCoList + ' ) ' +
            Filter;
         with TExecDSWrapper.Create(s) do
         begin
            ctx_DataAccess.GetCustomData(cd, AsVariant);
         end;
         result := GetFieldValueListFromDataset(cd,'EE_NBR');
      Finally
       cd.Free;
      End;
end;

procedure TEDIT_CL_REPORT_SCREEN.cbPriorYearClick(Sender: TObject);
begin
  inherited;
  cbQYears.OnChange(self);
end;

procedure TEDIT_CL_REPORT_SCREEN.pnlDateRangeResize(Sender: TObject);
begin
  inherited;
  //move the run reports around as necessary

end;

procedure TEDIT_CL_REPORT_SCREEN.btnAllOrNoneClick(Sender: TObject);
var
  i : integer;
begin
  inherited;
  //toggle the check boxes all/none
  if btnAllOrNone.Caption = 'Select All' then
  begin
    for i := 0 to cblQuarter.Items.Count - 1 do
      cblQuarter.Checked[i] := true;
    btnAllOrNone.Caption := 'Select None';
  end
  else begin
    for i := 0 to cblQuarter.Items.Count - 1 do
      cblQuarter.Checked[i] := false;
    btnAllOrNone.Caption := 'Select All'
  end;
end;

procedure TEDIT_CL_REPORT_SCREEN.pcDateOptionsChange(Sender: TObject);
begin
  inherited;
  //shuffle this radio group between tabs.
  Case pcDateOptions.ActivePageIndex of
    1: begin
         rgCheckDate.Parent := tsQuarters;
         btnDateList.Parent := tsQuarters;
       end;
  else
    begin
      rgCheckDate.Parent := tsDateRange;
      btnDateList.Parent := tsDateRange;
    end;
  end;
end;

procedure TEDIT_CL_REPORT_SCREEN.btnDateListClick(Sender: TObject);
begin
  inherited;
  Get_cdCheckPeriodList;
end;

procedure TEDIT_CL_REPORT_SCREEN.btnApplyPeriodClick(Sender: TObject);
begin
  inherited;
  pgList.ActivePage := tsPayroll;
  GridRefresh(true);
end;

initialization
  RegisterClass(TEDIT_CL_REPORT_SCREEN);

end.

