// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_USER_REPORTS;

interface

uses
  SPD_EDIT_CO_BASE, StdCtrls, Mask, DBCtrls,  Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, Controls, Classes, SPackageEntry,
  SDataStructure, Db, Wwdatsrc, wwdbedit, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, Forms,ActnList, EvContext, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,Variants, EvUtils, EvUIUtils, EvUIComponents, EvClientDataSet,
  ImgList, LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_USER_REPORTS = class(TEDIT_CO_BASE)
    TabSheet2: TTabSheet;
    dsGirdAssigned: TevDataSource;
    dsGridAvailable: TevDataSource;
    CoUserReportsDS: TevClientDataSet;
    CoUserReportsDSNDescription: TStringField;
    CoUserReportsDSCO_REPORTS_NBR: TIntegerField;
    CoReportsDS: TevClientDataSet;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    sbUserReports: TScrollBox;
    AddFavoriteReports: TevSpeedButton;
    RemoveFavoriteReports: TevSpeedButton;
    fpAvailable: TisUIFashionPanel;
    grdAvailableReports: TevDBGrid;
    fpFavorites: TisUIFashionPanel;
    grdAssignedReports: TevDBGrid;
    edUserReportsHiddenField: TEdit;
    procedure AddFavoriteReportsClick(Sender: TObject);
    procedure RemoveFavoriteReportsClick(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
  private
    rwPRReports : TevClientDataset;
    procedure OpenCoUserReportsDS;
    procedure OpenCoReportsDS;
    procedure OpenPRReports;
    procedure LoadData;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.DFM}

uses SysUtils,EvDataSet, EvCommonInterfaces, EvConsts;

{ TEDIT_CO_USER_REPORTS }

function TEDIT_CO_USER_REPORTS.GetDefaultDataSet: TevClientDataSet;
begin
   Result := DM_COMPANY.CO_USER_REPORTS;
end;

procedure TEDIT_CO_USER_REPORTS.Activate;
begin
  inherited;
  (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
  (Owner as TFramePackageTmpl).SetButton(NavDelete, False);
  (Owner as TFramePackageTmpl).SetButton(NavRefresh, False);
  (Owner as TFramePackageTmpl).SetButton(NavHistory, False);

  //to hide FavoriteReport icon
  pnlFavoriteReport.visible := False;
  OpenPRReports;
  OpenCoReportsDS;
  OpenCoUserReportsDS;
end;

procedure TEDIT_CO_USER_REPORTS.AddFavoriteReportsClick(Sender: TObject);
var i : integer;
begin
  inherited;

  if ( grdAvailableReports.SelectedList.Count + dsGirdAssigned.DataSet.RecordCount > 15 ) then
  begin
    EvErrMessage('Only 15 total reports may be selected as Favorite Reports.'#13#10 +
                 'You must adjust the reports selected before you will be able to add this report to your list.');
    exit;
  end;

  dsGridAvailable.DataSet.DisableControls;
  dsGirdAssigned.DataSet.DisableControls;
  try
    for i := 0 to grdAvailableReports.SelectedList.Count - 1 do
    begin
       dsGridAvailable.DataSet.GotoBookmark(grdAvailableReports.SelectedList[i]);
       DM_COMPANY.CO_USER_REPORTS.Append;
       DM_COMPANY.CO_USER_REPORTS.FieldbyName('SB_USER_NBR').asInteger   :=  Context.UserAccount.InternalNbr;
       DM_COMPANY.CO_USER_REPORTS.FieldbyName('CO_NBR').asInteger        :=  wwdsMaster.DataSet.FieldByName('CO_NBR').AsInteger;
       DM_COMPANY.CO_USER_REPORTS.FieldbyName('CO_REPORTS_NBR').asInteger := dsGridAvailable.DataSet.FieldByName('CO_REPORTS_NBR').asInteger;
       DM_COMPANY.CO_USER_REPORTS.Post;
    end;
    grdAvailableReports.UnselectAll;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_USER_REPORTS]);
  finally
    dsGridAvailable.DataSet.EnableControls;
    dsGirdAssigned.DataSet.EnableControls;
    LoadData;
  end;
end;

procedure TEDIT_CO_USER_REPORTS.RemoveFavoriteReportsClick(
  Sender: TObject);
var i : integer;
begin
  inherited;
  dsGridAvailable.DataSet.DisableControls;
  dsGirdAssigned.DataSet.DisableControls;
  try
    for i := 0 to grdAssignedReports.SelectedList.Count - 1 do
    begin
       dsGirdAssigned.DataSet.GotoBookmark(grdAssignedReports.SelectedList[i]);
      if DM_COMPANY.CO_USER_REPORTS.Locate('CO_NBR;CO_REPORTS_NBR;SB_USER_NBR',
                                            VarArrayOf([ wwdsMaster.DataSet.FieldByName('CO_NBR').AsInteger,
                                                         dsGirdAssigned.DataSet.FieldByName('CO_REPORTS_NBR').asInteger,
                                                         Context.UserAccount.InternalNbr]),[])
      then DM_COMPANY.CO_USER_REPORTS.Delete;
    end;
    grdAssignedReports.UnselectAll;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_USER_REPORTS]);
  finally
    dsGridAvailable.DataSet.EnableControls;
    dsGirdAssigned.DataSet.EnableControls;
    LoadData;
  end;
end;

procedure TEDIT_CO_USER_REPORTS.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if (wwdsList.DataSet.RecordCount > 0) and (PageControl1.ActivePage = TabSheet1) then
    LoadData;
end;

procedure TEDIT_CO_USER_REPORTS.LoadData;
var
  qryCoReports, qryCoUserReports : IevQuery;
begin
  qryCoUserReports := TevQuery.Create('Select UR.CO_USER_REPORTS_NBR, UR.CO_REPORTS_NBR,'+
                          ' CR.CUSTOM_NAME || ''('' || CR.REPORT_LEVEL || cast(CR.REPORT_WRITER_REPORTS_NBR as varchar(20))  || '')'' NDescription ' +
                          ' From CO_USER_REPORTS UR, CO_REPORTS CR '+
                          ' where CR.CO_REPORTS_NBR = UR.CO_REPORTS_NBR '+
                          ' and {AsOfNow<CR>} and {AsOfNow<UR>} '+
                          ' and CR.CO_NBR = UR.CO_NBR '+
                          ' and CR.CO_NBR = :CONBR '+
                          ' and UR.SB_USER_NBR = :SBUSERNBR' );
  qryCoUserReports.Params.AddValue('CONBR', wwdsMaster.DataSet.FieldByName('CO_NBR').AsInteger);
  qryCoUserReports.Params.AddValue('SBUSERNBR', Context.UserAccount.InternalNbr);
  qryCoUserReports.Execute;

  OpenCoUserReportsDS;
  OpenCoReportsDS;
  OpenPRReports;  

  CoUserReportsDS.EmptyDataSet;
  while not qryCoUserReports.Result.Eof do
  begin
    CoUserReportsDS.AppendRecord([qryCoUserReports.Result['NDescription'],
                                  qryCoUserReports.Result['CO_REPORTS_NBR']]);
    qryCoUserReports.Result.Next;
  end;
  CoUserReportsDS.First;
  dsGirdAssigned.DataSet := CoUserReportsDS;
  RemoveFavoriteReports.Enabled := not dsGirdAssigned.DataSet.IsEmpty;

  qryCoReports := TevQuery.Create(' SELECT CR.CO_REPORTS_NBR, '+
                     ' CR.REPORT_LEVEL, CR.REPORT_WRITER_REPORTS_NBR, '+
                     ' CR.CUSTOM_NAME || ''('' || CR.REPORT_LEVEL || cast(CR.REPORT_WRITER_REPORTS_NBR as varchar(20))  || '')'' NDescription ' +
                     ' FROM CO_REPORTS CR '+
                     ' WHERE {AsOfNow<CR>} AND CR.CO_NBR = :CONBR '+
                     ' AND NOT EXISTS(SELECT 1 FROM CO_USER_REPORTS UR WHERE {AsOfNow<UR>} AND UR.CO_NBR = CR.CO_NBR'+
                                      ' AND UR.SB_USER_NBR = :SBUSERNBR AND UR.CO_REPORTS_NBR = CR.CO_REPORTS_NBR) ');
  qryCoReports.Params.AddValue('CONBR', wwdsMaster.DataSet.FieldByName('CO_NBR').AsInteger);
  qryCoReports.Params.AddValue('SBUSERNBR', Context.UserAccount.InternalNbr);
  qryCoReports.Execute;

  DM_COMPANY.CO_REPORTS.DataRequired('CO_NBR = ' + wwdsMaster.DataSet.FieldByName('CO_NBR').asString);
  if Context.UserAccount.AccountType = uatRemote
  then DM_COMPANY.CO_REPORTS.RetrieveCondition := DM_COMPANY.CO_REPORTS.RetrieveCondition + ' AND HideForRemotes = ''N'' ';
  try
    CoReportsDS.EmptyDataSet;
    while not qryCoReports.Result.Eof do
    begin
      if ( ( rwPRReports.Locate('LEVEL;NBR',varArrayof( [qryCoReports.Result['REPORT_LEVEL'],
                                                       qryCoReports.Result['REPORT_WRITER_REPORTS_NBR']] ), []) )
           and DM_COMPANY.CO_REPORTS.Locate('CO_REPORTS_NBR', qryCoReports.Result['CO_REPORTS_NBR'], []) )

      then CoReportsDS.AppendRecord([qryCoReports.Result['NDescription'],qryCoReports.Result['CO_REPORTS_NBR'] ]);
      qryCoReports.Result.Next;
    end;

    CoReportsDS.First;
    dsGridAvailable.DataSet  := CoReportsDS;
    AddFavoriteReports.Enabled := not dsGridAvailable.DataSet.IsEmpty;
    (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
    (Owner as TFramePackageTmpl).SetButton(NavDelete, False);
    (Owner as TFramePackageTmpl).SetButton(NavRefresh, False);
    (Owner as TFramePackageTmpl).SetButton(NavHistory, False);
  finally
    DM_COMPANY.CO_REPORTS.RetrieveCondition :='';
  end;
end;

procedure TEDIT_CO_USER_REPORTS.Deactivate;
begin
  inherited;
  FreeAndNil(rwPRReports);
end;

procedure TEDIT_CO_USER_REPORTS.OpenCoUserReportsDS;
begin
  if not CoUserReportsDS.Active then
  begin
    CoUserReportsDS.CreateDataSet;
    CoUserReportsDS.AddIndex('SORT', 'NDescription', []);
    CoUserReportsDS.IndexName := 'SORT';
    CoUserReportsDS.IndexDefs.Update;
  end;
end;

procedure TEDIT_CO_USER_REPORTS.OpenCoReportsDS;
begin
  if not CoReportsDS.Active then
  begin
    CoReportsDS.CreateDataSet;
    CoReportsDS.AddIndex('SORT', 'NDescription', []);
    CoReportsDS.IndexName := 'SORT';
    CoReportsDS.IndexDefs.Update;
  end;  
end;

procedure TEDIT_CO_USER_REPORTS.OpenPRReports;
const
  ReportTypes = '''' + SYSTEM_REPORT_TYPE_NORMAL + ''',''' + SYSTEM_REPORT_TYPE_HR + '''';
begin
  if not Assigned(rwPRReports) then
  begin
    rwPRReports := TevClientDataset(nil);
    rwPRReports := GetReportsByType(ReportTypes, 'SBC', Self);
  end;
end;

initialization
  RegisterClass(TEDIT_CO_USER_REPORTS);

end.
