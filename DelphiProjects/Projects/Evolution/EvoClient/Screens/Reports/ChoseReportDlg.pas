// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit ChoseReportDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,  Grids, Wwdbigrd, Wwdbgrid, Buttons, DB,
  Wwdatsrc, EvUIComponents, ISBasicClasses, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TChoseReport = class(TForm)
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    evDBGrid1: TevDBGrid;
    evDataSource1: TevDataSource;
    procedure evDBGrid1DblClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure evBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ChoseReport: TChoseReport;

implementation

{$R *.dfm}

procedure TChoseReport.evDBGrid1DblClick(Sender: TObject);
begin
  evBitBtn1.Click;
end;

procedure TChoseReport.evBitBtn1Click(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TChoseReport.evBitBtn2Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
