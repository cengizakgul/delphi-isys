// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SReprintParameters;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  Buttons, ExtCtrls, EvUtils, EvContext, EvTypes,
  ISBasicClasses, EvConsts, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel;

type
  TReprintParameters = class(TForm)
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    cbPayrollChecks: TevCheckBox;
    cbMiscChecks: TevCheckBox;
    cbInvoices: TevCheckBox;
    cbReports: TevCheckBox;
    fpReprint: TisUIFashionPanel;
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var
  ReprintParameters: TReprintParameters;

implementation


{$R *.DFM}

procedure TReprintParameters.FormShow(Sender: TObject);
begin
  cbPayrollChecks.Enabled := ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_PROCESSED_CHECKS') = stEnabled;
  cbPayrollChecks.Checked := cbPayrollChecks.Enabled;
  cbMiscChecks.Enabled := ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_MISC_CHECKS') = stEnabled;
  cbMiscChecks.Checked := cbMiscChecks.Enabled;
  cbInvoices.Enabled := ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_PAYROLL_REPORTS') = stEnabled;
  cbInvoices.Checked := cbInvoices.Enabled;
  cbReports.Enabled := ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_PAYROLL_REPORTS') = stEnabled;
  cbReports.Checked := cbReports.Enabled;
end;

end.
