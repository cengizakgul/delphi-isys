unit SPD_CO_FAVORITE_REPORTS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Wwdatsrc, ISBasicClasses,  Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls,SDataStructure, DBGrids, StdCtrls, Buttons, ExtCtrls,EvDataset,EvCommonInterfaces,
  EvDataAccessComponents, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel;

type
  TCO_FavoriteReports = class(TForm)
    PageControl1: TPageControl;
    tsReports: TTabSheet;
    tsDetails: TTabSheet;
    dsReports: TevDataSource;
    grReports: TevDBCheckGrid;
    pnlDetail: TevPanel;
    cbDestination: TevComboBox;
    chbQueue: TevCheckBox;
    btnRun: TevBitBtn;
    rgTimePeriods: TevRadioGroup;
    Label1: TLabel;
    lblDateRange: TLabel;
    tsFavoriteReports: TTabSheet;
    dsFavoriteReports: TevDataSource;
    grFavoriteReports: TevDBCheckGrid;
    evPeriodMonth: TevComboBox;
    evPeriodQuarter: TevComboBox;
    evPeriodYear: TevComboBox;
    sbFavorite: TScrollBox;
    fpFavorite: TisUIFashionPanel;
    sbCompany: TScrollBox;
    fpCompany: TisUIFashionPanel;
    sbDetails: TScrollBox;
    fpDetails: TisUIFashionPanel;
    procedure btnRunClick(Sender: TObject);
    procedure rgTimePeriodsClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure evPeriodMonthClick(Sender: TObject);
    procedure evPeriodQuarterClick(Sender: TObject);
    procedure evPeriodYearClick(Sender: TObject);
  private
    { Private declarations }
    FConbr, FCustomCompanyNbr : string;
    dFrom_Date, dTo_Date : TDatetime;
    qryFavoriteReports : IevQuery;
  public
    { Public declarations }
    class procedure execute (const sCoNbr, sCustomCompanynbr : string; CoUserReportsNbr: integer = -1);
  end;

implementation

{$R *.dfm}

uses EvUtils, EvContext, SReportSettings, EvTypes, EvConsts, EvBasicUtils, isDataSet;

procedure TCO_FavoriteReports.btnRunClick(Sender: TObject);
var
  i: integer;
  vPrList: Variant;
  lParams: TrwReportParams;
  Dest: TrwReportDestination;
  ReportsReady: boolean;
  sCaption : string;

  function GetLastPR (aStatus : Char): string;
  var
    Qry : IevQuery;
  begin
    Result := '';
    Qry := TevQuery.Create(' select PR_NBR from PR where CO_NBR = ' + FCoNbr +
                           ' and PAYROLL_TYPE in (''R'',''S'',''V'',''B'',''C'',''E'',''K'',''I'') ' +
                           ' and status = ''' + aStatus + ''' and {AsOfNow<PR>} order by Status_Date desc ' );

    if not Qry.Result.eof then
     Result := Trim(Qry.Result.FieldByName('PR_NBR').AsString);
  end;

  function FillPrList: Variant;
  var
    sPrList: TStrings;
    i: integer;
    sPRNBR : string;
  begin
    Result := Null;
    case rgTimePeriods.ItemIndex of
    0: begin //last Waiting Payroll
         sPRNBR := GetLastPR(PAYROLL_STATUS_PENDING);
         if sPRNBR <> '' then
         begin
           sPrList := TStringList.Create;
           sPrList.Add(sPRNBR);
         end;
       end;
    1: begin //last Processed Payroll
         sPRNBR := GetLastPR(PAYROLL_STATUS_PROCESSED);
         if sPRNBR <> '' then
         begin
           sPrList := TStringList.Create;
           sPrList.Add(sPRNBR);
         end;
       end;
    2,3,4,5:
       begin
         ctx_DataAccess.CUSTOM_VIEW.Close;
         with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
         begin
           SetMacro('Columns', 'PR_NBR');
           SetMacro('TABLENAME', 'PR');
           SetMacro('CONDITION', 'CO_NBR='+ FCoNbr + ' AND CHECK_DATE between :fdate and :tdate AND status = ''P'' ');
           SetParam('fdate', dFrom_date);
           SetParam('tdate', dTo_date);
           ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
         end;
         ctx_DataAccess.CUSTOM_VIEW.Open;
         sPrList := ctx_DataAccess.CUSTOM_VIEW.GetFieldValueList('PR_NBR', True);
         ctx_DataAccess.CUSTOM_VIEW.Close;
       end;//2,3,4,5:
    end;

    if Assigned(sPrList)
       and (sPrList.Count > 0) then
    begin
      Result := VarArrayCreate([0, sPrList.Count - 1], varVariant);
      for i := 0 to sPrList.Count - 1 do
          Result[i] := StrToInt(sPrList[i]);
    end;

    if assigned(sPrList) then
       FreeAndNil(sPrList);
  end;
begin

  if rgTimePeriods.ItemIndex = -1 then
  begin
    EvMessage('Please select Time Period');
    exit;
  end;

  vPrList := FillPrList;

  if VarIsNull(vPrList)  then
  begin
    EvMessage('There are no payrolls within selected periods');
    exit;
  end;

  lParams := TrwReportParams.Create;
  Dest := rdtCancel;
  ReportsReady := False;
  ctx_RWLocalEngine.StartGroup;
  try
    for i := 0 to grFavoriteReports.SelectedList.Count - 1 do
    begin
      dsFavoriteReports.DataSet.GotoBookmark(grFavoriteReports.SelectedList.Items[i]);
      if qryFavoriteReports.Result.Locate('CO_USER_REPORTS_NBR',dsFavoriteReports.DataSet['CO_USER_REPORTS_NBR'],[]) then
      begin
        lParams.Clear;
        lParams.ReadFromBlobField(TBlobField(qryFavoriteReports.Result.FieldByName('INPUT_PARAMS')));
        lParams.Add('Clients', varArrayOf([ctx_DataAccess.ClientID]));
        lParams.Add('Companies', varArrayOf([StrToInt(FCONbr)]));
        lParams.Add('ClientCompany', varArrayOf([IntTostr(ctx_DataAccess.ClientID) + ':' + FCONbr]));
        lParams.Add('Payrolls', vPrList);
        lParams.Add('ReportName', qryFavoriteReports.Result.FieldByName('CUSTOM_NAME').AsString +
           ' (' + qryFavoriteReports.Result.FieldByName('REPORT_LEVEL').AsString +
          qryFavoriteReports.Result.FieldByName('REPORT_WRITER_REPORTS_NBR').AsString + ')');

        lParams.Copies := 1;

        sCaption := 'CO# ' + FCustomCompanyNbr + '. ' +
                    qryFavoriteReports.Result.FieldByName('CUSTOM_NAME').AsString;

        ctx_RWLocalEngine.CalcPrintReport(
              qryFavoriteReports.Result.FieldByName('REPORT_WRITER_REPORTS_NBR').AsInteger,
              qryFavoriteReports.Result.FieldByName('REPORT_LEVEL').AsString,
              lParams, sCaption).OverrideVmrMbGroupNbr :=
              qryFavoriteReports.Result.FieldByName('OVERRIDE_MB_GROUP_NBR').AsInteger;
        ReportsReady := True;
      end;
    end;

    for i := 0 to grReports.SelectedList.Count - 1 do
    begin
      dsReports.DataSet.GotoBookmark(grReports.SelectedList.Items[i]);

      lParams.Clear;
      lParams.ReadFromBlobField(TBlobField(dsReports.DataSet.FieldByName('INPUT_PARAMS')));
      lParams.Add('Clients', varArrayOf([ctx_DataAccess.ClientID]));
      lParams.Add('Companies', varArrayOf([StrToInt(FCONbr)]));
      lParams.Add('ClientCompany', varArrayOf([IntTostr(ctx_DataAccess.ClientID) + ':' + FCONbr]));
      lParams.Add('Payrolls', vPrList);
      lParams.Add('ReportName', dsReports.DataSet.FieldByName('CUSTOM_NAME').AsString +
        ' (' + dsReports.DataSet.FieldByName('REPORT_LEVEL').AsString +
        dsReports.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').AsString + ')');
      lParams.Copies := 1;

      sCaption := 'CO# ' + FCustomCompanyNbr + '. ' +
                  dsReports.DataSet.FieldByName('CUSTOM_NAME').AsString;

      ctx_RWLocalEngine.CalcPrintReport(
        dsReports.DataSet.FieldByName('REPORT_WRITER_REPORTS_NBR').AsInteger,
        dsReports.DataSet.FieldByName('REPORT_LEVEL').AsString,
        lParams,sCaption).OverrideVmrMbGroupNbr :=
        dsReports.DataSet.FieldByName('OVERRIDE_MB_GROUP_NBR').AsInteger;
      ReportsReady := True;
    end;

    case cbDestination.ItemIndex of
      0: Dest := rdtPrinter;
      1: Dest := rdtPreview;
    end;

    if ReportsReady then
    begin
      if chbQueue.Checked then
         ctx_RWLocalEngine.EndGroupForTask(Dest, 'Print Favorite Reports')
      else
         ctx_RWLocalEngine.EndGroup(Dest, 'Print Favorite Reports');
    end;
  finally
    lParams.Free;
    ctx_EndWait;
  end;
end;

class procedure TCO_FavoriteReports.execute (const sCoNbr, sCustomCompanynbr : string; CoUserReportsNbr: integer = -1);
var
  FrmReport :TCO_FavoriteReports;
  CoReportsDS, CoUserReportsDS : TevClientDataset;
begin
  FrmReport := TCO_FavoriteReports.Create(Nil);
  try
    FrmReport.PageControl1.ActivePage := FrmReport.tsFavoriteReports;
    FrmReport.rgTimePeriods.ItemIndex := -1;
    FrmReport.cbDestination.ItemIndex := 0; // default print
    FrmReport.lblDateRange.Caption := '';
    FrmReport.FConbr := sCoNbr;
    FrmReport.FCustomCompanyNbr := sCustomCompanynbr;
    CoReportsDS := TevClientDataset.Create( nil );
    if not DM_COMPANY.CO_REPORTS.Active then
        DM_COMPANY.CO_REPORTS.DataRequired('ALL');

    CoReportsDS.CloneCursor(DM_COMPANY.CO_REPORTS, True, False);
    if Context.UserAccount.AccountType = uatRemote
    then CoReportsDS.RetrieveCondition := 'CO_NBR = ' + FrmReport.FConbr + ' and Favorite = ''Y'' and HideForRemotes = ''N'' '
    else CoReportsDS.RetrieveCondition := 'CO_NBR = ' + FrmReport.FConbr + ' and Favorite = ''Y'' ';
    FrmReport.dsReports.Dataset := CoReportsDS;


    FrmReport.qryFavoriteReports := TevQuery.Create('Select CR.CUSTOM_NAME, CR.REPORT_LEVEL, CR.REPORT_WRITER_REPORTS_NBR, '+
                          ' CR.OVERRIDE_MB_GROUP_NBR, CR.INPUT_PARAMS, UR.CO_USER_REPORTS_NBR, '+
                          ' UR.SB_USER_NBR, UR.CO_NBR, UR.CO_REPORTS_NBR, '+
                          ' CR.CUSTOM_NAME || ''('' || CR.REPORT_LEVEL || cast(CR.REPORT_WRITER_REPORTS_NBR as varchar(20))  || '')'' NDescription ' +
                          ' From CO_REPORTS CR, CO_USER_REPORTS UR '+
                          ' where CR.CO_REPORTS_NBR = UR.CO_REPORTS_NBR '+
                          ' and {AsOfNow<CR>} and {AsOfNow<UR>} '+
                          ' and CR.CO_NBR = UR.CO_NBR '+
                          ' and CR.CO_NBR = :CONBR '+
                          ' and UR.SB_USER_NBR = :SBUSERNBR' );
    FrmReport.qryFavoriteReports.Params.AddValue('CONBR', FrmReport.FConbr);
    FrmReport.qryFavoriteReports.Params.AddValue('SBUSERNBR', Context.UserAccount.InternalNbr);
    FrmReport.qryFavoriteReports.Execute;
    //This Is for Grid (dsFavoriteReports) So I don't need to have a DataModule

    CoUserReportsDS := TevClientDataset.Create( nil );
    CoUserReportsDS.FieldDefs.Add('NDescription', ftString, 255, False);
    CoUserReportsDS.FieldDefs.Add('CO_USER_REPORTS_NBR', ftInteger, 0, False);
    CoUserReportsDS.CreateDataSet;
    CoUserReportsDS.AddIndex('SORT', 'NDescription', []);
    CoUserReportsDS.IndexName := 'SORT';
    CoUserReportsDS.IndexDefs.Update;

    while not FrmReport.qryFavoriteReports.Result.EOF do
    begin
      CoUserReportsDS.AppendRecord([FrmReport.qryFavoriteReports.Result['NDescription'],
                                    FrmReport.qryFavoriteReports.Result['CO_USER_REPORTS_NBR']]);
      FrmReport.qryFavoriteReports.Result.Next;
    end;
    CoUserReportsDS.First;
    FrmReport.dsFavoriteReports.Dataset := CoUserReportsDS;

    if (CoUserReportsNbr > -1) and CoUserReportsDS.Locate('CO_USER_REPORTS_NBR', CoUserReportsNbr, []) then
    begin
      FrmReport.grFavoriteReports.SelectRecord;
      FrmReport.PageControl1.ActivePage := FrmReport.tsDetails;
    end;
    
    FrmReport.ShowModal;
  finally
    FreeAndNil(CoReportsDS);
    FreeAndNil(CoUserReportsDS);
    FreeAndNil(FrmReport);
  end;
end;

procedure TCO_FavoriteReports.rgTimePeriodsClick(Sender: TObject);
begin
  evPeriodMonth.Enabled := False;
  evPeriodQuarter.Enabled := False;
  evPeriodYear.Enabled := False;
  evPeriodMonth.ItemIndex := 1;  //default
  evPeriodQuarter.ItemIndex := 1;
  evPeriodYear.ItemIndex := 1;


  case rgTimePeriods.ItemIndex of
    2 : begin
          evPeriodMonth.Enabled := True;
          dFrom_date := GetBeginMonth(date);
        end;
    3 : begin
          evPeriodQuarter.Enabled := True;
          dFrom_date := GetBeginQuarter(date);
        end;
    4 : begin
          evPeriodYear.Enabled := True;
          dFrom_date := GetBeginYear(date);
        end;
    5 : dFrom_date := GetYearAgoDate(date);
  end;

  case rgTimePeriods.ItemIndex of
    0,1 : lblDateRange.Caption := '';
    2,3,4,5 : begin
        if ( rgTimePeriods.ItemIndex = 2 )
           and (evPeriodMonth.ItemIndex = 0) //ToTheEnd
        then dTo_date := GetEndMonth(date)

        else if ( rgTimePeriods.ItemIndex = 3 )
                and (evPeriodQuarter.ItemIndex = 0) //ToTheEnd
        then dTo_date := GetEndQuarter(date)

        else if ( rgTimePeriods.ItemIndex = 4 )
                and (evPeriodYear.ItemIndex = 0) //ToTheEnd
        then dTo_date := GetEndYear(date)
        else dTo_date := date;

        lblDateRange.Caption := FormatDateTime('mm/dd/yyyy',dFrom_date) + ' ~ ' + FormatDateTime('mm/dd/yyyy',dTo_date);
    end;
  end;
end;

procedure TCO_FavoriteReports.PageControl1Change(Sender: TObject);
begin
  pnlDetail.Enabled := True;
  if ( PageControl1.ActivePage = tsDetails ) then
  begin
    if dsFavoriteReports.DataSet.IsEmpty
       and dsReports.DataSet.IsEmpty
    then begin
       EvMessage('No reports to run');
       pnlDetail.Enabled := False;
    end
    else if (grFavoriteReports.SelectedList.Count = 0)
            and (grReports.SelectedList.Count = 0)
    then begin
      EvErrMessage('Reports are not selected');
      pnlDetail.Enabled := False;
    end;
  end;
end;

procedure TCO_FavoriteReports.evPeriodMonthClick(Sender: TObject);
begin
  if ( rgTimePeriods.ItemIndex = 2 ) then
  begin
    if (evPeriodMonth.ItemIndex = 0) //ToTheEnd
    then dTo_date := GetEndMonth(date)
    else dTo_date := date;

    lblDateRange.Caption := FormatDateTime('mm/dd/yyyy',dFrom_date) + ' ~ ' + FormatDateTime('mm/dd/yyyy',dTo_date);
  end;
end;

procedure TCO_FavoriteReports.evPeriodQuarterClick(Sender: TObject);
begin
  if ( rgTimePeriods.ItemIndex = 3 ) then
  begin
    if (evPeriodQuarter.ItemIndex = 0) //ToTheEnd
    then dTo_date := GetEndQuarter(date)
    else dTo_date := date;

    lblDateRange.Caption := FormatDateTime('mm/dd/yyyy',dFrom_date) + ' ~ ' + FormatDateTime('mm/dd/yyyy',dTo_date);
  end;
end;

procedure TCO_FavoriteReports.evPeriodYearClick(Sender: TObject);
begin
  if ( rgTimePeriods.ItemIndex = 4 ) then
  begin
    if (evPeriodYear.ItemIndex = 0) //ToTheEnd
    then dTo_date := GetEndYear(date)
    else dTo_date := date;

    lblDateRange.Caption := FormatDateTime('mm/dd/yyyy',dFrom_date) + ' ~ ' + FormatDateTime('mm/dd/yyyy',dTo_date);
  end;

end;

end.
