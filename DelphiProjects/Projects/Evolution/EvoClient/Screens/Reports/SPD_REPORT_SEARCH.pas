unit SPD_REPORT_SEARCH;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SFrameEntry, DB, Wwdatsrc, ISBasicClasses, 
  StdCtrls, Buttons, ISTestUtils, EvUtils, ComCtrls, wwdblook, Wwdbdlg,
  ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, kbmMemTable, ISKbmMemDataSet,
  SPD_MiniNavigationFrame, DBCtrls, Mask, wwdbedit, SPackageEntry, EvContext,
  EvDataAccessComponents, ISDataAccessComponents, SDDClasses, strUtils,
  SDataDictsystem, SDataStructure, ActnList, EvStreamUtils, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIEdit, isUIFashionPanel;

type
  TREPORT_SEARCH = class(TFrameEntry)
    evPanel2: TevPanel;
    evPanel1: TevPanel;
    lblResult: TevLabel;
    evDBGrid1: TevDBGrid;
    lblSearchFor: TevLabel;
    dsReports: TevClientDataSet;
    dsReportsName: TStringField;
    dsReportsNbr: TIntegerField;
    dsReportsLevel: TStringField;
    gbSearchPlace: TevGroupBox;
    edPattern: TevEdit;
    chbSystem: TevCheckBox;
    chbSB: TevCheckBox;
    chbClient: TevCheckBox;
    btnStart: TevButton;
    Actions: TevActionList;
    Start: TAction;
    Stop: TAction;
    chbMatchCase: TevCheckBox;
    chbMatchWords: TevCheckBox;
    fpParameters: TisUIFashionPanel;
    fpResults: TisUIFashionPanel;
    sbReportSearch: TScrollBox;
    dsReportsFieldFound: TStringField;
    procedure StopUpdate(Sender: TObject);
    procedure StopExecute(Sender: TObject);
    procedure StartUpdate(Sender: TObject);
    procedure StartExecute(Sender: TObject);
    procedure edPatternKeyPress(Sender: TObject; var Key: Char);
  private
    FStop: boolean;
    FPattern: string;
    FSSOptions: TStringSearchOptions;
    procedure AddToResult(aNbr: integer; aLvl, aName, aStr: string);
    procedure Search(ds: TDataset; lvl: string);
    procedure ClearResult;
    procedure SearchReports;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation
uses ISBasicUtils;
{$R *.dfm}

procedure TREPORT_SEARCH.Activate;
begin
  inherited;
  chbSystem.Checked := True;
  chbSB.Checked := True;
  chbClient.Checked := True;
end;

constructor TREPORT_SEARCH.Create(AOwner: TComponent);
begin
  inherited;
  dsReports.CreateDataSet;
  dsReports.Open;
  FStop := True;
end;

procedure TREPORT_SEARCH.SearchReports;

  procedure SearchInClient;
  begin
    ctx_StartWait('Search in "' + DM_TEMPORARY.TMP_CL.CUSTOM_CLIENT_NUMBER.Value + '" Client Reports...');
    try
      ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CL['CL_NBR']);
//      DM_CLIENT.CL_REPORT_WRITER_REPORTS.DataRequired();
      Search( DM_CLIENT.CL_REPORT_WRITER_REPORTS, 'C' );
    finally
      ctx_EndWait;
    end;
  end;

begin
  ClearResult;

  FSSOptions := [soDown];
  if chbMatchCase.Checked then
    FSSOptions := FSSOptions + [soMatchCase];
  if chbMatchWords.Checked then
    FSSOptions := FSSOptions + [soWholeWord];

  if chbSystem.Checked then
  begin
    ctx_StartWait('Search in System Reports...');
    try
      Search( DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS, 'S' );
    finally
      ctx_EndWait;
    end;
  end;

  if chbSB.Checked then
  begin
    ctx_StartWait('Search in SB Reports...');
    try
      Search( DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS, 'B' );
    finally
      ctx_EndWait;
    end;
  end;

  if chbClient.Checked then
  begin
    DM_TEMPORARY.TMP_CL.First;
    while not DM_TEMPORARY.TMP_CL.Eof do
    begin
      SearchInClient;
      DM_TEMPORARY.TMP_CL.Next;
    end;
  end;

  dsReports.First;
end;

procedure TREPORT_SEARCH.StopUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := not FStop;
end;

procedure TREPORT_SEARCH.StopExecute(Sender: TObject);
begin
  FStop := True;
end;

procedure TREPORT_SEARCH.StartUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := FStop and (Trim( edPattern.Text ) <> '')
    and (chbSystem.Checked or chbSB.Checked or chbClient.Checked);
end;

procedure TREPORT_SEARCH.StartExecute(Sender: TObject);
begin
  FStop := False;
  FPattern := edPattern.Text;
  SearchReports;
  FStop := True;
end;

procedure TREPORT_SEARCH.Search(ds: TDataset; lvl: string);
var
  Nbr: variant;
  KeyName: string;
  MS: IEvDualStream;
  ActiveDs: boolean;
  strSearch, str:string;
begin
  ActiveDs := ds.Active;
  KeyName := ds.Name + '_NBR';
  if not ActiveDs then
    ds.Open
  else
    Nbr := ds.FieldByName(KeyName).Value;

  MS := TEvDualStreamHolder.CreateInMemory;

  ds.First;
  while not (FStop or ds.Eof) do
  begin
    if ds.State in [dsBrowse, dsEdit] then
      try
        strSearch := FPattern;
        while strSearch <> '' do
        begin
           str := Trim(GetNextStrValue(strSearch, '|'));
           ctx_RWLocalEngine.GetReportResources(ds.FieldByName(KeyName).AsInteger, lvl, MS);

           if Assigned( SearchBuf( MS.Memory, MS.Size, 0, 0, str, FSSOptions ) ){ <> nil} then
              AddToResult( ds.FieldByName(KeyName).AsInteger, lvl, ds.FieldByName('REPORT_DESCRIPTION').AsString, str);
        end;
      except
        on E: Exception do
        begin
          FStop := True;
          MS.Clear;
          EvErrMessage(E.Message);
        end;
      end;

    ds.Next;
  end;

  if not ActiveDs then
    ds.Close
  else
    ds.Locate(ds.Name + '_NBR', Nbr, []);
end;

procedure TREPORT_SEARCH.AddToResult(aNbr: integer; aLvl, aName, aStr: string);
begin
  dsReports.Append;
  dsReportsNbr.Value := aNbr;
  dsReportsLevel.Value := aLvl;
  dsReportsName.Value := aName;
  dsReportsFieldFound.Value := aStr;
  dsReports.Post;
end;

procedure TREPORT_SEARCH.Deactivate;
begin
  inherited;
  dsReports.Close;
end;

procedure TREPORT_SEARCH.ClearResult;
var
  i: integer;
begin
  if dsReports.Active and (dsReports.RecordCount > 0) then
  begin
    dsReports.DisableControls;
    try
      for i := 1 to dsReports.RecordCount do
        dsReports.Delete;
    finally
      dsReports.EnableControls;
    end;
  end;
end;

procedure TREPORT_SEARCH.edPatternKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  //click the button if they press ENTER
  if ((Key = #13) and (btnStart.Enabled)) then btnStart.Click;
end;

initialization
  RegisterClass(TREPORT_SEARCH);

end.
