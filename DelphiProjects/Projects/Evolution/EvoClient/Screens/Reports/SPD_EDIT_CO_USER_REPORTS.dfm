inherited EDIT_CO_USER_REPORTS: TEDIT_CO_USER_REPORTS
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited Panel3: TevPanel
            inherited BitBtn1: TevBitBtn
              Top = 1
              TabOrder = 1
            end
            object edUserReportsHiddenField: TEdit
              Left = 2400
              Top = 8
              Width = 121
              Height = 21
              TabOrder = 0
              Text = 'edUserReportsHiddenField'
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 296
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 246
                Height = 279
                IniAttributes.SectionName = 'TEDIT_CO_USER_REPORTS\wwdbgridSelectClient'
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object sbUserReports: TScrollBox
        Left = 0
        Top = 0
        Width = 891
        Height = 474
        Align = alClient
        TabOrder = 0
        object AddFavoriteReports: TevSpeedButton
          Left = 294
          Top = 190
          Width = 74
          Height = 22
          Caption = 'Add'
          HideHint = True
          AutoSize = False
          OnClick = AddFavoriteReportsClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
            DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
            DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
            78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
            8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
            F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
            F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
            DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
            DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
          Layout = blGlyphRight
          ShortCut = 0
        end
        object RemoveFavoriteReports: TevSpeedButton
          Left = 294
          Top = 238
          Width = 74
          Height = 22
          Caption = 'Remove'
          HideHint = True
          AutoSize = False
          OnClick = RemoveFavoriteReportsClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
            DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
            DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
            78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
            448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
            F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
            F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
            44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
            DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
          ShortCut = 0
        end
        object fpAvailable: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 277
          Height = 411
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpAvailable'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Available Reports'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdAvailableReports: TevDBGrid
            Left = 12
            Top = 35
            Width = 241
            Height = 353
            DisableThemesInTitle = False
            Selected.Strings = (
              'NDescription'#9'500'#9'Report Name'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_USER_REPORTS\grdAvailableReports'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsGridAvailable
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgShowCellHint, dgFixedResizable, dgDblClickColSizing]
            ReadOnly = False
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object fpFavorites: TisUIFashionPanel
          Left = 377
          Top = 8
          Width = 277
          Height = 411
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpFavorites'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Favorite Reports'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object grdAssignedReports: TevDBGrid
            Left = 12
            Top = 35
            Width = 241
            Height = 353
            DisableThemesInTitle = False
            Selected.Strings = (
              'NDescription'#9'500'#9'Report Name'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_USER_REPORTS\grdAssignedReports'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsGirdAssigned
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgShowCellHint, dgFixedResizable, dgDblClickColSizing]
            ReadOnly = False
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 128
    Top = 34
  end
  inherited wwdsDetail: TevDataSource
    Left = 166
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Top = 18
  end
  inherited dsCL: TevDataSource
    Left = 284
    Top = 18
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 318
    Top = 18
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 352
    Top = 21
  end
  object dsGirdAssigned: TevDataSource
    Left = 400
    Top = 128
  end
  object dsGridAvailable: TevDataSource
    Left = 56
    Top = 136
  end
  object CoUserReportsDS: TevClientDataSet
    Left = 400
    Top = 160
    object CoUserReportsDSNDescription: TStringField
      DisplayWidth = 500
      FieldName = 'NDescription'
      Size = 500
    end
    object CoUserReportsDSCO_REPORTS_NBR: TIntegerField
      FieldName = 'CO_REPORTS_NBR'
    end
  end
  object CoReportsDS: TevClientDataSet
    Left = 88
    Top = 136
    object StringField1: TStringField
      DisplayWidth = 500
      FieldName = 'NDescription'
      Size = 500
    end
    object IntegerField1: TIntegerField
      FieldName = 'CO_REPORTS_NBR'
    end
  end
end
