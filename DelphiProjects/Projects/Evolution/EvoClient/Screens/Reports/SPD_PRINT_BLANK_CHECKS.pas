// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PRINT_BLANK_CHECKS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_Open_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, Spin,
  SReportSettings, EvUtils, EvTypes, EvConsts, DBCtrls, SFieldCodeValues,
  wwdblook, Variants, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  SDDClasses, ISDataAccessComponents, EvContext,
  EvDataAccessComponents, SDataDicttemp, EvExceptions, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TPRINT_BLANK_CHECKS = class(TEDIT_OPEN_BASE)
    CHECK_DATA: TevClientDataSet;
    CHECK_DATACheckNbr: TIntegerField;
    CHECK_DATAEntityName: TStringField;
    CHECK_DATAEntityAddress1: TStringField;
    CHECK_DATAEntityAddress2: TStringField;
    CHECK_DATAEntityAddress3: TStringField;
    CHECK_DATABankName: TStringField;
    CHECK_DATABankAddress: TStringField;
    CHECK_DATABankTopABANumber: TStringField;
    CHECK_DATABankBottomABANumber: TStringField;
    CHECK_DATAMICRLine: TStringField;
    CHECK_DATACheckAmount: TStringField;
    CHECK_DATACheckAmountSpelled: TStringField;
    CHECK_DATACheckAmountSecurity: TStringField;
    CHECK_DATAPaymentSerialNumber: TStringField;
    CHECK_DATACheckNumberOrVoucher: TStringField;
    CHECK_DATADivisionNumber: TStringField;
    CHECK_DATABranchNumber: TStringField;
    CHECK_DATADepartmentNumber: TStringField;
    CHECK_DATATeamNumber: TStringField;
    CHECK_DATACheckMessage: TStringField;
    CHECK_DATAEmployeeName: TStringField;
    CHECK_DATAEmployeeAddress1: TStringField;
    CHECK_DATAEmployeeAddress2: TStringField;
    CHECK_DATAEmployeeAddress3: TStringField;
    CHECK_DATAEmployeeSSN: TStringField;
    CHECK_DATAEmployeeNumber: TStringField;
    CHECK_DATAEmployeeHireDate: TStringField;
    CHECK_DATATotalHours: TStringField;
    CHECK_DATATotalEarnings: TStringField;
    CHECK_DATATotalYTDEarnings: TStringField;
    CHECK_DATATotalDeductions: TStringField;
    CHECK_DATATotalYTDDeductions: TStringField;
    CHECK_DATANet: TStringField;
    CHECK_DATAYTDNet: TStringField;
    CHECK_DATATotalDirDep: TStringField;
    CHECK_DATANetAndDirDep: TStringField;
    CHECK_DATAPeriodBeginDate: TStringField;
    CHECK_DATAPeriodEndDate: TStringField;
    CHECK_DATACheckDate: TStringField;
    CHECK_DATACompanyName: TStringField;
    CHECK_DATACompanyNumber: TStringField;
    CHECK_DATACheckComments: TMemoField;
    CHECK_DATASignature: TBlobField;
    CHECK_DATAMICRHorizontalAdjustment: TIntegerField;
    CHECK_STUB_DATA: TevClientDataSet;
    CHECK_STUB_DATAStubLineNbr: TIntegerField;
    CHECK_STUB_DATACheckNbr: TIntegerField;
    CHECK_STUB_DATAEarningDesc: TStringField;
    CHECK_STUB_DATALocation: TStringField;
    CHECK_STUB_DATARate: TStringField;
    CHECK_STUB_DATAHours: TStringField;
    CHECK_STUB_DATAEarningCurrent: TStringField;
    CHECK_STUB_DATAEarningYTD: TStringField;
    CHECK_STUB_DATADeductionDesc: TStringField;
    CHECK_STUB_DATADeductionCurrent: TStringField;
    CHECK_STUB_DATADeductionYTD: TStringField;
    TIME_OFF_DATA: TevClientDataSet;
    TIME_OFF_DATACheckNbr: TIntegerField;
    TIME_OFF_DATATimeOffNbr: TIntegerField;
    TIME_OFF_DATATimeOffLine: TStringField;
    dsCL: TevDataSource;
    evLabel4: TevLabel;
    cbCheckForm: TevComboBox;
    CHECK_DATALogo_Nbr: TIntegerField;
    CHECK_DATASignature_Nbr: TIntegerField;
    CHECK_DATASignature_Db: TStringField;
    CHECK_DATAOBCCheckMessage: TStringField;
    cdsCheckDataLite: TevClientDataSet;
    IntegerField1: TIntegerField;
    cdsCheckDataLiteYTDNet: TFloatField;
    cdsCheckDataLiteBankLevel: TStringField;
    cdsCheckDataLiteAccountNbr: TIntegerField;
    cdsCheckDataLiteDirDep: TFloatField;
    cdsCheckDataLiteEE_NBR: TIntegerField;
    cdsCheckDataLiteVoucher: TIntegerField;
    cdsTOALite: TevClientDataSet;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    cdsDeductionsLite: TevClientDataSet;
    IntegerField3: TIntegerField;
    StringField2: TStringField;
    cdsDeductionsLiteAmount: TFloatField;
    cdsDeductionsLiteYTD: TFloatField;
    cdsEarningsLite: TevClientDataSet;
    IntegerField4: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    cdsEarningsLiteHours: TFloatField;
    cdsEarningsLiteRate: TStringField;
    cdsEarningsLiteClEDsNbr: TIntegerField;
    cdsEarningsLiteaType: TIntegerField;
    cdsEarningsLiteLocation: TStringField;
    evLabel47: TevLabel;
    evPanel1: TevPanel;
    lablClient: TevLabel;
    dbtxClientNbr: TevDBText;
    dbtxClientName: TevDBText;
    lablCompany: TevLabel;
    dbtxCompanyNumber: TevDBText;
    CompanyNameText: TevDBText;
    fpSettings: TisUIFashionPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    spinNumber: TevSpinEdit;
    bbtnPrintChecks: TevBitBtn;
    editStartCheck: TevEdit;
    wwlcCl_Bank_Account_Number: TevDBLookupCombo;
    edHiddenBlankCheck: TEdit;
    evEdit1: TevEdit;
    procedure bbtnPrintChecksClick(Sender: TObject);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure wwdbgridSelectClientEnter(Sender: TObject);
    procedure edHiddenBlankCheckEnter(Sender: TObject);
    procedure evEdit1Enter(Sender: TObject);
  private
    CheckFormCodeValues: TStrings;
    procedure AppendCheckForms;
    function GetCheckForm(aRegular: boolean = True): string;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;

  end;

var
  PRINT_BLANK_CHECKS: TPRINT_BLANK_CHECKS;

implementation

{$R *.DFM}

procedure TPRINT_BLANK_CHECKS.bbtnPrintChecksClick(Sender: TObject);
var
  I, SBBankNbr, MICRCheckNbrStart, MICRBankAcctStart: Integer;
  ReportParams: TrwReportParams;
  TempStr: String;
  v: Variant;

  procedure CreateDataSetForCheckPrint(aDS: TevClientDataSet);
  begin
    if aDS.Active then
      aDS.Close;
    aDS.CreateDataSet;
  end;

  function IsOldCheck: boolean;
  begin
    Result := (Pos(CheckFormCodeValues[cbCheckForm.ItemIndex][1], CHECK_FORM_REGULAR_NEW + CHECK_FORM_REGULAR_TOP_NEW + CHECK_FORM_LETTER_BTM_NEW +
          CHECK_FORM_LETTER_BTM_SPANISH_NEW + CHECK_FORM_LETTER_BTM_RATE_NEW + CHECK_FORM_LETTER_BTM_FF_NEW + CHECK_FORM_LETTER_TOP_NEW + 
          CHECK_FORM_LETTER_BTM_WITH_ADDR_NEW + CHECK_FORM_PRESSURE_SEAL_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_NEW + CHECK_FORM_LETTER_BTM_2_STUB_NEW +
          CHECK_FORM_PRESSURE_SEAL_LEGAL_NODBDT_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_MOORE_NEW + CHECK_FORM_PRESSURE_SEAL_GREATLAND_NEW +
          CHECK_FORM_PRESSURE_SEAL_LEGAL_GREATLAND_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_VERSASEAL_CD_NEW +
          CHECK_FORM_PRESSURE_SEAL_LEGAL_FF_NEW + CHECK_FORM_LETTER_BTM_STUB_ONLY_NEW + CHECK_FORM_PRESSURE_SEAL_LEGAL_OVFLW_NEW +
          CHECK_FORM_LETTER_BTM_STUB_PUNCH + CHECK_FORM_BUREAU_NEW_CHECK + CHECK_FORM_BUREAU_DESIGNER_CHECK+CHECK_FORM_PRESSURE_SEAL_LEGAL_ANSI_NEW) = 0);
  end;
begin
  inherited;

  if wwlcCl_Bank_Account_Number.Value = '' then
    raise EInvalidParameters.CreateHelp('Client Bank Account must be selected!', IDH_InvalidParameters);
  try
    StrToInt(editStartCheck.Text);
  except
    raise EInvalidParameters.CreateHelp('Starting Check # is too large! It can not be greater than 2 billion.', IDH_InvalidParameters);
  end;

  if IsOldCheck then
  begin
    CreateDataSetForCheckPrint(CHECK_DATA);
    CreateDataSetForCheckPrint(CHECK_STUB_DATA);
    CreateDataSetForCheckPrint(TIME_OFF_DATA);

    with CHECK_DATA do
    for I := StrToInt(editStartCheck.Text) to spinNumber.Value + StrToInt(editStartCheck.Text) - 1 do
    begin
      Insert;
      FieldByName('CheckNbr').Value := I;
      FieldByName('MICRHorizontalAdjustment').Value := DM_SERVICE_BUREAU.SB.FieldByName('MICR_HORIZONTAL_ADJUSTMENT').Value;
      FieldByName('MICRLine').Value := '';
      FieldByName('CheckAmountSecurity').Value := '';
      FieldByName('CheckNumberOrVoucher').Value := IntToStr(I);
      if DM_CLIENT.CL.PRINT_CLIENT_NAME.Value = GROUP_BOX_YES then
      begin
        FieldByName('EntityName').Value := DM_CLIENT.CL.FieldByName('NAME').AsString;
        FieldByName('EntityAddress1').Value := DM_CLIENT.CL.FieldByName('ADDRESS1').AsString;
        FieldByName('EntityAddress2').Value := DM_CLIENT.CL.FieldByName('ADDRESS2').AsString;
        FieldByName('EntityAddress3').Value := DM_CLIENT.CL.FieldByName('CITY').AsString + ', ' +
          DM_CLIENT.CL.FieldByName('STATE').AsString + ' ' + DM_CLIENT.CL.FieldByName('ZIP_CODE').AsString;
      end
      else
      begin
        FieldByName('EntityName').Value := DM_COMPANY.CO.FieldByName('NAME').AsString;
        FieldByName('EntityAddress1').Value := DM_COMPANY.CO.FieldByName('ADDRESS1').AsString;
        FieldByName('EntityAddress2').Value := DM_COMPANY.CO.FieldByName('ADDRESS2').AsString;
        FieldByName('EntityAddress3').Value := DM_COMPANY.CO.FieldByName('CITY').AsString + ', ' +
          DM_COMPANY.CO.FieldByName('STATE').AsString + ' ' + DM_COMPANY.CO.FieldByName('ZIP_CODE').AsString;
      end;
      if FieldByName('EntityAddress2').AsString = '' then
      begin
        FieldByName('EntityAddress2').Value := FieldByName('EntityAddress3').Value;
        FieldByName('EntityAddress3').Value := '';
      end;
      FieldByName('Logo_Nbr').Value := DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', wwlcCl_Bank_Account_Number.LookupValue, 'LOGO_CL_BLOB_NBR');
      FieldByName('Signature_Db').Value := 'C';

      SBBankNbr := DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', wwlcCl_Bank_Account_Number.LookupValue, 'SB_BANKS_NBR');
      FieldByName('BankName').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'PRINT_NAME'));
      FieldByName('BankAddress').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'CITY')) + ', '
        + VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'STATE')) + ' '
        + VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'ZIP_CODE'));

      MICRCheckNbrStart := ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'MICR_CHECK_NUMBER_START_POSITN'), 0);
      MICRBankAcctStart := ConvertNull(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'MICR_ACCOUNT_START_POSITION'), 0);

      TempStr := StringOfChar(' ', MICRCheckNbrStart) + '>' + PadStringLeft(IntToStr(I), '0', 6) + '>';
      TempStr := TempStr + ' |' + VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'ABA_NUMBER'))
        + '|' + StringOfChar(' ', MICRBankAcctStart + 1) + DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CL_BANK_ACCOUNT_NBR', wwlcCl_Bank_Account_Number.LookupValue, 'CUSTOM_BANK_ACCOUNT_NUMBER') + '>';
      if TempStr[Length(TempStr) - 1] = '~' then
        System.Delete(TempStr, Length(TempStr) - 1, 2);
      FieldByName('MICRLine').Value := TempStr;
      FieldByName('BankTopABANumber').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'TOP_ABA_NUMBER'));
      FieldByName('BankBottomABANumber').Value := VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', SBBankNbr, 'BOTTOM_ABA_NUMBER'));
      Post;
    end;
    v := DataSetsToVarArray([CHECK_DATA, CHECK_STUB_DATA, TIME_OFF_DATA]);
  end
  else begin
    CreateDataSetForCheckPrint(cdsCheckDataLite);
    CreateDataSetForCheckPrint(cdsTOALite);
    CreateDataSetForCheckPrint(cdsDeductionsLite);
    CreateDataSetForCheckPrint(cdsEarningsLite);

    // the only record has the following values: EE_NBR = -1,
    // CHECKNBR field = Number of Checks,
    // Starting Check Number is in VOUCHER field, and YTDNet field is Co_Nbr
    with cdsCheckDataLite do
    begin
      Insert;
      FieldByName('EE_NBR').Value := -1;
      FieldByName('CHECKNBR').Value := spinNumber.Value;
      FieldByName('VOUCHER').Value := StrToInt(editStartCheck.Text);
      FieldByName('YTDNet').Value := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
      FieldByName('ACCOUNTNBR').Value := wwlcCl_Bank_Account_Number.LookupValue;
      FieldByName('BANKLEVEL').Value := 'C';
      Post;
    end;

    v := DataSetsToVarArray([cdsCheckDataLite, cdsTOALite, cdsEarningsLite, cdsDeductionsLite]);
  end;

  ReportParams := TrwReportParams.Create;
  try
    TempStr := CheckFormCodeValues[cbCheckForm.ItemIndex];
    if (TempStr[1] = CHECK_FORM_BUREAU_NEW_CHECK) or (TempStr[1] = CHECK_FORM_BUREAU_DESIGNER_CHECK) then
    begin
      TempStr := Copy(TempStr, 2, Length(TempStr));

      if TryStrToInt(TempStr, i) then
        DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.DataRequired('SB_REPORT_WRITER_REPORTS_NBR=''' + TempStr + '''')
      else
        i := 0;

      if (Trim(TempStr) = '') or (i = 0) or (DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.RecordCount = 0) then
        raise EMissingReport.CreateHelp('SB Check Report (B' + TempStr + ') does not exist in the database!', IDH_MissingReport);
    end
    else
      ctx_DataAccess.LocateCheckReport(CheckFormCodeValues[cbCheckForm.ItemIndex], True);

    ReportParams.Add('DataSets', v);
    ReportParams.Add('Clients', VarArrayOf([ctx_DataAccess.ClientID]));

    ctx_RWLocalEngine.StartGroup;
    try
      if (CheckFormCodeValues[cbCheckForm.ItemIndex][1] = CHECK_FORM_BUREAU_NEW_CHECK) or
        (CheckFormCodeValues[cbCheckForm.ItemIndex][1] = CHECK_FORM_BUREAU_DESIGNER_CHECK) then
        ctx_RWLocalEngine.CalcPrintReport(DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').Value,
          CH_DATABASE_SERVICE_BUREAU, ReportParams)
      else
        ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').Value,
          CH_DATABASE_SYSTEM, ReportParams);
    finally
      ctx_RWLocalEngine.EndGroup( rdtPrinter );
    end;

  finally
    ReportParams.Free;
  end;
end;

procedure TPRINT_BLANK_CHECKS.Activate;
var
  I: Integer;
  S1, S2, ComboChoices: String;
begin
  inherited;
  //to hide FavoriteReport icon
  pnlFavoriteReport.visible := False;

  wwdsList.DataSet.Locate('CL_NBR;CO_NBR', VarArrayOf([TevClientDataSet(wwdsMaster.DataSet).ClientID, wwdsMaster.DataSet['CO_NBR']]), []);
  if not BitBtn1.Enabled then
    BitBtn1Click(Self);
  DM_SERVICE_BUREAU.SB.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANKS.DataRequired('ALL');

  ComboChoices := CheckForm_ComboChoices;
  I := 1;
  CheckFormCodeValues := TStringList.Create;
  while not GetToken(ComboChoices, I, S1) do
  begin
    GetToken(ComboChoices, I, S2);
    cbCheckForm.Items.Append(S1);
    CheckFormCodeValues.Append(S2);
  end;
  AppendCheckForms;
  if cbCheckForm.Visible then
    cbCheckForm.ItemIndex := CheckFormCodeValues.IndexOf(GetCheckForm);

  wwlcCl_Bank_Account_Number.LookupTable := DM_CLIENT.CL_BANK_ACCOUNT;
end;

procedure TPRINT_BLANK_CHECKS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_CLIENT.CL, aDS);
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_COMPANY.CO, aDS);
end;

procedure TPRINT_BLANK_CHECKS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_TEMPORARY.TMP_CO, SupportDataSets);
  AddDS(DM_COMPANY.CO, SupportDataSets);
  AddDS(DM_CLIENT.CL, SupportDataSets);
  if not Assigned(wwdsList.DataSet) then
    wwdsList.DataSet := DM_TEMPORARY.TMP_CO;
  if not Assigned(wwdsMaster.DataSet) then
    wwdsMaster.DataSet := DM_COMPANY.CO;
  if not Assigned(dsCL.DataSet) then
    dsCL.DataSet := DM_CLIENT.CL;
end;

procedure TPRINT_BLANK_CHECKS.wwdsListDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Assigned(cbCheckForm) then
  begin
    cbCheckForm.Visible := not BitBtn1.Enabled;
    bbtnPrintChecks.Visible := not BitBtn1.Enabled;
    evLabel4.Visible := not BitBtn1.Enabled;
    if cbCheckForm.Visible and Assigned(CheckFormCodeValues) then
      cbCheckForm.ItemIndex := CheckFormCodeValues.IndexOf(GetCheckForm);

    wwlcCl_Bank_Account_Number.enabled := not BitBtn1.Enabled;
    if not wwlcCl_Bank_Account_Number.enabled then
        wwlcCl_Bank_Account_Number.Text := '';
  end;
end;

function TPRINT_BLANK_CHECKS.GetDataSetConditions(sName: string): string;
begin
  Result := inherited GetDataSetConditions(sName);
  if (sName <> 'CL') and
     (Copy(sName, 1, 3) <> 'CL_') and
     wwdsList.DataSet.Active then
    Result := 'CO_NBR = ' + IntToStr(wwdsList.DataSet.FieldByName('CO_NBR').AsInteger);
end;

procedure TPRINT_BLANK_CHECKS.AppendCheckForms;
var
  RegularSBChecks: TStrings;
  i: integer;
begin
  RegularSBChecks := TStringList.Create;
  try
    ctx_DataAccess.FillSBCheckList(RegularSBChecks, nil);
    for i := 0 to RegularSBChecks.Count - 1 do
    begin
      cbCheckForm.Items.Append( RegularSBChecks.Names[i] );
      CheckFormCodeValues.Append( RegularSBChecks.ValueFromIndex[i] );
    end;
  finally
    RegularSBChecks.Free;
  end;
end;

function TPRINT_BLANK_CHECKS.GetCheckForm(aRegular: boolean): string;
begin
  Result := DM_COMPANY.CO.FieldByName('CHECK_FORM').AsString;

  if (Result = CHECK_FORM_BUREAU_NEW_CHECK) or (Result = CHECK_FORM_BUREAU_DESIGNER_CHECK) then
  begin
    Result := ExtractFromFiller(DM_COMPANY.CO.FieldByName('FILLER').AsString, 65, 6)
  end;
end;

procedure TPRINT_BLANK_CHECKS.wwdbgridSelectClientEnter(Sender: TObject);
begin
  inherited;
  wwdbgridSelectClient.Columns[2].DisplayLabel := 'FEIN'; //WON'T LET ME DO IT IN ALT-F12
  
end;

procedure TPRINT_BLANK_CHECKS.edHiddenBlankCheckEnter(Sender: TObject);
begin
  inherited;
  //sneaky way to force FEIN title change
  wwdbgridSelectClient.SetFocus;
end;

procedure TPRINT_BLANK_CHECKS.evEdit1Enter(Sender: TObject);
begin
  inherited;
  editStartCheck.Height := 22;
  //don't ask me why I can't change the height of this field anywhere else.
  //some crazy stuff!!!
end;

procedure TPRINT_BLANK_CHECKS.Deactivate;
begin
  CheckFormCodeValues.Free;
  inherited;
end;

initialization
  RegisterClass(TPRINT_BLANK_CHECKS);

end.
