inherited PRINT_BLANK_CHECKS: TPRINT_BLANK_CHECKS
  inherited Panel1: TevPanel
    Height = 51
    Color = 4276545
    inherited pnlFavoriteReport: TevPanel
      Height = 51
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      inherited spbFavoriteReports: TevSpeedButton
        Top = 16
      end
      inherited btnSwipeClock: TevSpeedButton
        Top = 16
      end
      inherited BtnMRCLogIn: TevSpeedButton
        Top = 16
      end
      object evLabel47: TevLabel
        Left = 2
        Top = 2
        Width = 111
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Additional Tools'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object evPanel1: TevPanel
      Left = 0
      Top = 0
      Width = 320
      Height = 51
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      TabOrder = 1
      object lablClient: TevLabel
        Left = 8
        Top = 8
        Width = 38
        Height = 13
        Caption = 'CLIENT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtxClientNbr: TevDBText
        Left = 69
        Top = 8
        Width = 65
        Height = 15
        DataField = 'CUSTOM_CLIENT_NUMBER'
        DataSource = dsCL
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtxClientName: TevDBText
        Left = 141
        Top = 7
        Width = 241
        Height = 15
        DataField = 'NAME'
        DataSource = dsCL
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lablCompany: TevLabel
        Left = 8
        Top = 24
        Width = 53
        Height = 13
        Caption = 'COMPANY'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtxCompanyNumber: TevDBText
        Left = 69
        Top = 22
        Width = 65
        Height = 17
        DataField = 'CUSTOM_COMPANY_NUMBER'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object CompanyNameText: TevDBText
        Left = 141
        Top = 21
        Width = 241
        Height = 17
        DataField = 'NAME'
        DataSource = wwdsMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  inherited PageControl1: TevPageControl
    Top = 51
    Height = 215
    OnChanging = nil
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Height = 186
        inherited pnlBorder: TevPanel
          Left = 364
          Width = 305
          Height = 217
          object evEdit1: TevEdit
            Left = 3000
            Top = 300
            Width = 128
            Height = 21
            TabOrder = 0
            Text = '1'
            OnEnter = evEdit1Enter
          end
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 364
          Height = 217
          Align = alLeft
          Margins.Right = 4
          inherited pnlFashionBody: TevPanel
            Width = 328
            TabOrder = 1
            inherited Panel2: TevPanel
              Left = 331
              Top = 37
              Width = 0
              Height = 563
            end
            inherited pnlSubbrowse: TevPanel
              Left = 331
              Top = 37
              Width = 0
              Height = 563
            end
          end
          inherited Panel3: TevPanel
            Width = 320
            Height = 37
            TabOrder = 0
            inherited BitBtn1: TevBitBtn
              Width = 121
              Caption = 'Open Company'
              TabOrder = 1
            end
            object edHiddenBlankCheck: TEdit
              Left = 2600
              Top = 8
              Width = 121
              Height = 21
              TabOrder = 0
              Text = 'edHiddenBlankCheck'
              OnEnter = edHiddenBlankCheckEnter
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Top = 81
            Width = 320
            Height = 108
            inherited Splitter1: TevSplitter
              Left = 0
              Height = 104
              Visible = False
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 3
              Width = 303
              Height = 104
              Align = alClient
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 253
                Height = 24
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
                  'NAME'#9'25'#9'Name'#9'F'
                  'FEIN'#9'10'#9'Fein'#9)
                IniAttributes.SectionName = 'TPRINT_BLANK_CHECKS\wwdbgridSelectClient'
                OnEnter = wwdbgridSelectClientEnter
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 306
              Width = 10
              Height = 104
              Align = alRight
              Visible = False
            end
          end
        end
        object fpSettings: TisUIFashionPanel
          Left = 368
          Top = 8
          Width = 301
          Height = 209
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpSettings'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Check Settings'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 148
            Top = 113
            Width = 88
            Height = 13
            Caption = 'Number of Checks'
          end
          object evLabel2: TevLabel
            Left = 12
            Top = 113
            Width = 80
            Height = 13
            Caption = 'Starting Check #'
          end
          object evLabel3: TevLabel
            Left = 12
            Top = 74
            Width = 97
            Height = 13
            Caption = 'Client Bank Account'
          end
          object evLabel4: TevLabel
            Left = 12
            Top = 35
            Width = 81
            Height = 13
            Caption = '~Check Form'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object spinNumber: TevSpinEdit
            Left = 148
            Top = 128
            Width = 128
            Height = 22
            MaxValue = 9999
            MinValue = 1
            TabOrder = 3
            Value = 1
          end
          object bbtnPrintChecks: TevBitBtn
            Left = 72
            Top = 161
            Width = 145
            Height = 25
            Caption = 'Print Checks'
            TabOrder = 4
            OnClick = bbtnPrintChecksClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
              08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
              8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
              8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
              8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
              78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
              7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
              DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
              DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
            NumGlyphs = 2
            Margin = 0
          end
          object editStartCheck: TevEdit
            Left = 12
            Top = 128
            Width = 128
            Height = 21
            TabOrder = 2
            Text = '1'
          end
          object wwlcCl_Bank_Account_Number: TevDBLookupCombo
            Left = 12
            Top = 89
            Width = 264
            Height = 21
            HelpContext = 10583
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
              'Bank_Name'#9'40'#9'Bank_Name')
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object cbCheckForm: TevComboBox
            Left = 12
            Top = 50
            Width = 264
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
          end
        end
      end
    end
  end
  object CHECK_DATA: TevClientDataSet
    Left = 648
    Top = 24
    object CHECK_DATACheckNbr: TIntegerField
      FieldName = 'CheckNbr'
    end
    object CHECK_DATAEntityName: TStringField
      FieldName = 'EntityName'
      Size = 40
    end
    object CHECK_DATAEntityAddress1: TStringField
      FieldName = 'EntityAddress1'
      Size = 40
    end
    object CHECK_DATAEntityAddress2: TStringField
      FieldName = 'EntityAddress2'
      Size = 40
    end
    object CHECK_DATAEntityAddress3: TStringField
      FieldName = 'EntityAddress3'
      Size = 40
    end
    object CHECK_DATABankName: TStringField
      FieldName = 'BankName'
      Size = 30
    end
    object CHECK_DATABankAddress: TStringField
      FieldName = 'BankAddress'
      Size = 30
    end
    object CHECK_DATABankTopABANumber: TStringField
      FieldName = 'BankTopABANumber'
    end
    object CHECK_DATABankBottomABANumber: TStringField
      FieldName = 'BankBottomABANumber'
    end
    object CHECK_DATAMICRLine: TStringField
      FieldName = 'MICRLine'
      Size = 50
    end
    object CHECK_DATACheckAmount: TStringField
      FieldName = 'CheckAmount'
    end
    object CHECK_DATACheckAmountSpelled: TStringField
      DisplayWidth = 80
      FieldName = 'CheckAmountSpelled'
      Size = 80
    end
    object CHECK_DATACheckAmountSecurity: TStringField
      FieldName = 'CheckAmountSecurity'
    end
    object CHECK_DATAPaymentSerialNumber: TStringField
      FieldName = 'PaymentSerialNumber'
    end
    object CHECK_DATACheckNumberOrVoucher: TStringField
      FieldName = 'CheckNumberOrVoucher'
    end
    object CHECK_DATADivisionNumber: TStringField
      FieldName = 'DivisionNumber'
    end
    object CHECK_DATABranchNumber: TStringField
      FieldName = 'BranchNumber'
    end
    object CHECK_DATADepartmentNumber: TStringField
      FieldName = 'DepartmentNumber'
    end
    object CHECK_DATATeamNumber: TStringField
      FieldName = 'TeamNumber'
    end
    object CHECK_DATACheckMessage: TStringField
      FieldName = 'CheckMessage'
      Size = 40
    end
    object CHECK_DATAEmployeeName: TStringField
      FieldName = 'EmployeeName'
      Size = 40
    end
    object CHECK_DATAEmployeeAddress1: TStringField
      FieldName = 'EmployeeAddress1'
      Size = 40
    end
    object CHECK_DATAEmployeeAddress2: TStringField
      FieldName = 'EmployeeAddress2'
      Size = 40
    end
    object CHECK_DATAEmployeeAddress3: TStringField
      FieldName = 'EmployeeAddress3'
      Size = 40
    end
    object CHECK_DATAEmployeeSSN: TStringField
      FieldName = 'EmployeeSSN'
    end
    object CHECK_DATAEmployeeNumber: TStringField
      FieldName = 'EmployeeNumber'
      Size = 10
    end
    object CHECK_DATAEmployeeHireDate: TStringField
      FieldName = 'EmployeeHireDate'
    end
    object CHECK_DATATotalHours: TStringField
      FieldName = 'TotalHours'
    end
    object CHECK_DATATotalEarnings: TStringField
      FieldName = 'TotalEarnings'
    end
    object CHECK_DATATotalYTDEarnings: TStringField
      FieldName = 'TotalYTDEarnings'
    end
    object CHECK_DATATotalDeductions: TStringField
      FieldName = 'TotalDeductions'
    end
    object CHECK_DATATotalYTDDeductions: TStringField
      FieldName = 'TotalYTDDeductions'
    end
    object CHECK_DATANet: TStringField
      FieldName = 'Net'
    end
    object CHECK_DATAYTDNet: TStringField
      FieldName = 'YTDNet'
    end
    object CHECK_DATATotalDirDep: TStringField
      FieldName = 'TotalDirDep'
    end
    object CHECK_DATANetAndDirDep: TStringField
      FieldName = 'NetAndDirDep'
    end
    object CHECK_DATAPeriodBeginDate: TStringField
      FieldName = 'PeriodBeginDate'
    end
    object CHECK_DATAPeriodEndDate: TStringField
      FieldName = 'PeriodEndDate'
    end
    object CHECK_DATACheckDate: TStringField
      FieldName = 'CheckDate'
    end
    object CHECK_DATACompanyName: TStringField
      FieldName = 'CompanyName'
      Size = 40
    end
    object CHECK_DATACompanyNumber: TStringField
      FieldName = 'CompanyNumber'
    end
    object CHECK_DATACheckComments: TMemoField
      FieldName = 'CheckComments'
      BlobType = ftMemo
    end
    object CHECK_DATASignature: TBlobField
      FieldName = 'Signature'
    end
    object CHECK_DATAMICRHorizontalAdjustment: TIntegerField
      FieldName = 'MICRHorizontalAdjustment'
    end
    object CHECK_DATALogo_Nbr: TIntegerField
      FieldName = 'Logo_Nbr'
    end
    object CHECK_DATASignature_Nbr: TIntegerField
      FieldName = 'Signature_Nbr'
    end
    object CHECK_DATASignature_Db: TStringField
      FieldName = 'Signature_Db'
      Size = 1
    end
    object CHECK_DATAOBCCheckMessage: TStringField
      FieldName = 'OBCCheckMessage'
      Size = 255
    end
  end
  object CHECK_STUB_DATA: TevClientDataSet
    Left = 680
    Top = 24
    object CHECK_STUB_DATAStubLineNbr: TIntegerField
      FieldName = 'StubLineNbr'
    end
    object CHECK_STUB_DATACheckNbr: TIntegerField
      FieldName = 'CheckNbr'
    end
    object CHECK_STUB_DATAEarningDesc: TStringField
      FieldName = 'EarningDesc'
      Size = 18
    end
    object CHECK_STUB_DATALocation: TStringField
      FieldName = 'Location'
      Size = 40
    end
    object CHECK_STUB_DATARate: TStringField
      FieldName = 'Rate'
      Size = 10
    end
    object CHECK_STUB_DATAHours: TStringField
      FieldName = 'Hours'
      Size = 10
    end
    object CHECK_STUB_DATAEarningCurrent: TStringField
      FieldName = 'EarningCurrent'
    end
    object CHECK_STUB_DATAEarningYTD: TStringField
      FieldName = 'EarningYTD'
    end
    object CHECK_STUB_DATADeductionDesc: TStringField
      FieldName = 'DeductionDesc'
      Size = 30
    end
    object CHECK_STUB_DATADeductionCurrent: TStringField
      FieldName = 'DeductionCurrent'
    end
    object CHECK_STUB_DATADeductionYTD: TStringField
      FieldName = 'DeductionYTD'
    end
  end
  object TIME_OFF_DATA: TevClientDataSet
    Left = 712
    Top = 24
    object TIME_OFF_DATACheckNbr: TIntegerField
      FieldName = 'CheckNbr'
    end
    object TIME_OFF_DATATimeOffNbr: TIntegerField
      FieldName = 'TimeOffNbr'
    end
    object TIME_OFF_DATATimeOffLine: TStringField
      FieldName = 'TimeOffLine'
      Size = 60
    end
  end
  object dsCL: TevDataSource
    Left = 255
    Top = 39
  end
  object cdsCheckDataLite: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CheckNbr'
        DataType = ftInteger
      end
      item
        Name = 'YTDNet'
        DataType = ftFloat
      end
      item
        Name = 'BankLevel'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AccountNbr'
        DataType = ftInteger
      end
      item
        Name = 'DirDep'
        DataType = ftFloat
      end
      item
        Name = 'EE_NBR'
        DataType = ftInteger
      end>
    Left = 652
    Top = 64
    object IntegerField1: TIntegerField
      FieldName = 'CheckNbr'
    end
    object cdsCheckDataLiteYTDNet: TFloatField
      FieldName = 'YTDNet'
    end
    object cdsCheckDataLiteBankLevel: TStringField
      FieldName = 'BankLevel'
      Size = 1
    end
    object cdsCheckDataLiteAccountNbr: TIntegerField
      FieldName = 'AccountNbr'
    end
    object cdsCheckDataLiteDirDep: TFloatField
      FieldName = 'DirDep'
    end
    object cdsCheckDataLiteEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsCheckDataLiteVoucher: TIntegerField
      FieldName = 'Voucher'
    end
  end
  object cdsTOALite: TevClientDataSet
    Left = 684
    Top = 64
    object IntegerField2: TIntegerField
      FieldName = 'CheckNbr'
    end
    object StringField1: TStringField
      FieldName = 'TimeOffLine'
      Size = 60
    end
  end
  object cdsDeductionsLite: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CheckNbr'
        DataType = ftInteger
      end
      item
        Name = 'Description'
        DataType = ftString
        Size = 30
      end>
    Left = 716
    Top = 64
    object IntegerField3: TIntegerField
      FieldName = 'CheckNbr'
    end
    object StringField2: TStringField
      FieldName = 'Description'
      Size = 30
    end
    object cdsDeductionsLiteAmount: TFloatField
      FieldName = 'Amount'
    end
    object cdsDeductionsLiteYTD: TFloatField
      FieldName = 'YTD'
    end
  end
  object cdsEarningsLite: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CheckNbr'
        DataType = ftInteger
      end
      item
        Name = 'Amount'
        DataType = ftFloat
      end
      item
        Name = 'YTD'
        DataType = ftFloat
      end
      item
        Name = 'Hours'
        DataType = ftFloat
      end
      item
        Name = 'Rate'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'ClEDsNbr'
        DataType = ftInteger
      end>
    Left = 748
    Top = 64
    object IntegerField4: TIntegerField
      FieldName = 'CheckNbr'
    end
    object FloatField1: TFloatField
      FieldName = 'Amount'
    end
    object FloatField2: TFloatField
      FieldName = 'YTD'
    end
    object cdsEarningsLiteHours: TFloatField
      FieldName = 'Hours'
    end
    object cdsEarningsLiteRate: TStringField
      FieldName = 'Rate'
      Size = 10
    end
    object cdsEarningsLiteClEDsNbr: TIntegerField
      FieldName = 'ClEDsNbr'
    end
    object cdsEarningsLiteaType: TIntegerField
      FieldName = 'EType'
    end
    object cdsEarningsLiteLocation: TStringField
      FieldName = 'Location'
      Size = 30
    end
  end
end
