object QEEGrid: TQEEGrid
  Left = 0
  Top = 0
  Width = 534
  Height = 517
  TabOrder = 0
  object pnlTop: TevPanel
    Left = 0
    Top = 0
    Width = 534
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
  end
  object pnlGrid: TevPanel
    Left = 0
    Top = 25
    Width = 534
    Height = 492
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object dgQEE: TevDBGrid
      Tag = 9
      Left = 0
      Top = 0
      Width = 534
      Height = 492
      HelpContext = 1504
      DisableThemesInTitle = False
      IniAttributes.Enabled = False
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TQuickEDGrid\dgQED'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = dsQEE
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      KeyOptions = [dgEnterToTab, dgAllowInsert]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgShowFooter, dgFooter3DCells, dgTrailingEllipsis, dgFixedResizable, dgFixedEditable, dgDblClickColSizing]
      ParentFont = False
      PopupMenu = pmFields
      ReadOnly = False
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = RUSSIAN_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -9
      TitleFont.Name = 'Small Fonts'
      TitleFont.Style = []
      TitleLines = 2
      OnCalcCellColors = dgQEECalcCellColors
      PaintOptions.FastRecordScrolling = False
      PaintOptions.AlternatingRowColor = clCream
      PaintOptions.ActiveRecordColor = clBlack
      OnBeforeDrawCell = dgQEEBeforeDrawCell
      DefaultSort = '-'
      NoFire = False
    end
  end
  object dsQEE: TevDataSource
    Left = 192
    Top = 32
  end
  object pmFields: TevPopupMenu
    AutoHotkeys = maManual
    OnPopup = pmFieldsPopup
    Left = 240
    Top = 72
    object miAddField: TMenuItem
      Caption = 'Add Extra Field'
    end
    object miAddED: TMenuItem
      Caption = 'Add E/D'
    end
    object miAddEDDlg: TMenuItem
      Caption = 'Add E/D...'
      OnClick = miAddEDDlgClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object miRemoveField: TMenuItem
      Caption = 'Remove Extra Field'
    end
    object miRemoveED: TMenuItem
      Caption = 'Remove E/D'
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object RestoreDefaults: TMenuItem
      Caption = 'Restore Defaults'
      OnClick = RestoreDefaultsClick
    end
  end
end
