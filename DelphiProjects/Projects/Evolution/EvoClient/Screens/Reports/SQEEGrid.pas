// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SQEEGrid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sdatastructure, StdCtrls, DBCtrls, EvContext,
  ISBasicClasses,  ExtCtrls, DB, Wwdatsrc, wwdblook, Grids,
  Wwdbigrd, Wwdbgrid, SPayrollScr, Menus,
  SDataDictclient, SDDClasses, sRemotePayrollUtils, gdystrset,
  EvMainboard, EvExceptions, isBaseClasses, EvDataAccessComponents, EvUIComponents, EvClientDataSet;

type
  TGridList = (gChecks, gCheckLines );

  TQEEGridState = (qeegInactive, qeegActive );
  TExtraFieldType = ( qftAmount, qftHours );

  TExtraFieldDesc = record
    FieldName: string;
    Caption: string;
    FieldType: TExtraFieldType;

    CL_E_DS_NBR: integer;
    CUSTOM_E_D_CODE_NUMBER: string;
    E_D_CODE_TYPE: string;


  end;

  PExtraFieldDesc = ^TExtraFieldDesc;

  TExtraFieldDescs = array of TExtraFieldDesc;

  TUserFields = record
    DefaultFields: TStringSet;
    ExtraFieldDescs: TExtraFieldDescs;
  end;

  TDefaulFields = record
    User: TStringSet;
    ToLoad: string;
    ToLoadCount: integer;
  end;

  TQEEGrid = class(TFrame)
    pnlTop: TevPanel;
    pnlGrid: TevPanel;
    dsQEE: TevDataSource;
    dgQEE: TevDBGrid;
    pmFields: TevPopupMenu;
    miAddField: TMenuItem;
    miRemoveField: TMenuItem;
    miAddED: TMenuItem;
    miRemoveED: TMenuItem;
    miAddEDDlg: TMenuItem;
    RestoreDefaults: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;

    procedure dgQEEBeforeDrawCell(Sender: TwwCustomDBGrid;
      DrawCellInfo: TwwCustomDrawGridCellInfo);
    procedure dgQEECalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure pmFieldsPopup(Sender: TObject);
    procedure miAddEDDlgClick(Sender: TObject);
    procedure RestoreDefaultsClick(Sender: TObject);

  private

    FExtraList: TStringList;

    FDefaultFields: TDefaulFields;
    FExtraFieldDescs: TExtraFieldDescs;

    FQEE: TevClientDataSet;

    FIndexDefs: TIndexDefs;

    FState: TQEEGridState;

    FCurrentColumn: string;

    DefaultSelectedFields: IisStringList;
    DefaultDatasetFirstFields: IisStringList;
    DefaultDatasetSecondFields: IisStringList;


    procedure AddDefaultField(idx: integer);
    procedure AddExtraField(idx: integer);

    procedure SaveSettings;

    function GetUserFields: TUserFields;
    procedure EnsureValid(const userFields: TUserFields );
    procedure InvalidateAll;

    procedure BuildFieldsMenu(const SetDefault : Boolean=false);

    procedure HandleAddField(Sender: TObject);
    procedure HandleAddExtraField(Sender: TObject);
    procedure HandleRemoveField(Sender: TObject);
    procedure HandleRemoveEDField(Sender: TObject);

    property QEE: TevClientDataSet read FQEE;

  public
    sFCoList, sFCheckFilter, sFEEFilter, sFBatchFilter, sFPayrollFilter : String;
    gridIndex : TGridList;

    procedure Activate;
    procedure Deactivate;

  end;

type
  TGridInfo = record
    DatasetFirst: string;
    DatasetSecond: string;
    Selected: string;
    Index: string;
    ToLoad: string;
  end;

  TDefaultFieldDesc = record
    Caption: string;
    FieldName: string;
    FieldType: TFieldType;
    Width: integer;
    DisplayWidth: integer;
    Hidden: boolean;
  end;
  PDefaultFieldDescs = ^TDefaultFieldDesc;

const
  DefaultFieldDescs: array [0..35] of TDefaultFieldDesc =
  (
    (Caption: 'EE_NBR'; FieldName: 'EE_NBR';                         FieldType: ftInteger; Width: 0; DisplayWidth: 6; Hidden: true ),
    (Caption: 'Pr_CHECK_NBR'; FieldName: 'PR_CHECK_NBR';             FieldType: ftInteger; Width: 0; DisplayWidth: 6; Hidden: true ),

    (Caption: 'Check type'; FieldName: 'CHECK_TYPE';                 FieldType: ftString;  Width: 2; DisplayWidth: 2;  ),
    (Caption: 'EE Code'; FieldName: 'CUSTOM_EMPLOYEE_NUMBER';        FieldType: ftString;  Width: 10; DisplayWidth: 10;   ),
    (Caption: 'EE Name'; FieldName: 'Employee_Name_Calculate';       FieldType: ftString; Width: 40; DisplayWidth: 40;   ),
    (Caption: 'Check date '; FieldName: 'CHECK_DATE';                FieldType: ftDateTime; Width: 0; DisplayWidth:12;   ),
    (Caption: 'Run #'; FieldName: 'RUN_NUMBER';                      FieldType: ftInteger; Width: 0; DisplayWidth: 6;   ),

    (Caption: 'Check line DBDT'; FieldName: 'LINES_DBDT';            FieldType: ftString;  Width: 80; DisplayWidth: 30;   ),
    (Caption: 'Home DBDT'; FieldName: 'HOME_DBDT';                   FieldType: ftString;  Width: 80; DisplayWidth: 30;   ),

    (Caption: 'SSN'; FieldName: 'SOCIAL_SECURITY_NUMBER';            FieldType: ftString;  Width: 11; DisplayWidth: 11;   ),
    (Caption: 'First name'; FieldName: 'FIRST_NAME';                 FieldType: ftString;  Width: 20; DisplayWidth: 20;   ),
    (Caption: 'Last name'; FieldName: 'LAST_NAME';                   FieldType: ftString;  Width: 20; DisplayWidth: 20;   ),
    (Caption: 'Address 1'; FieldName: 'ADDRESS1';                     FieldType: ftString;  Width: 30; DisplayWidth: 30;   ),
    (Caption: 'Address 2'; FieldName: 'ADDRESS2';                     FieldType: ftString;  Width: 30; DisplayWidth: 30;   ),
    (Caption: 'City'; FieldName: 'CITY';                             FieldType: ftString;  Width: 20; DisplayWidth: 20;   ),
    (Caption: 'State'; FieldName: 'STATE';                           FieldType: ftString;  Width: 2; DisplayWidth: 2;  ),
    (Caption: 'Zip code'; FieldName: 'ZIP_CODE';                     FieldType: ftString;  Width:10; DisplayWidth: 10;  ),
    (Caption: 'Birth date'; FieldName: 'BIRTH_DATE';                 FieldType: ftDateTime; Width: 0; DisplayWidth:10;   ),
    (Caption: 'Hire date'; FieldName: 'CURRENT_HIRE_DATE';           FieldType: ftDateTime; Width: 0; DisplayWidth:10;   ),
    (Caption: 'Salary'; FieldName: 'SALARY_AMOUNT';                  FieldType: ftCurrency; Width: 0; DisplayWidth: 10; ),
    (Caption: 'Position'; FieldName: 'POSITION_DESCRIPTION';         FieldType: ftString;  Width: 30; DisplayWidth: 30;   ),
    (Caption: 'Position Status'; FieldName: 'POSITION_STATUS_desc';  FieldType: ftString;  Width: 30; DisplayWidth: 30;   ),
    (Caption: 'Default Job'; FieldName: 'Default_Job';               FieldType: ftString;  Width: 40; DisplayWidth: 30;   ),
    (Caption: 'Primary Rate'; FieldName: 'Primary_Rate';             FieldType: ftCurrency; Width: 0; DisplayWidth: 10;  ),


    (Caption: 'Company Number'; FieldName: 'CUSTOM_COMPANY_NUMBER';  FieldType: ftString;  Width: 20; DisplayWidth: 20;   ),

    (Caption: 'Agency'; FieldName: 'CL_AGENCY_NBR';                  FieldType: ftInteger; Width: 0; DisplayWidth: 40;   ),
    (Caption: 'Check Line Job'; FieldName: 'JOB_DESCRIPTION';        FieldType: ftString;  Width: 40; DisplayWidth: 30;  ),
    (Caption: 'Line Item Date'; FieldName: 'LINE_ITEM_DATE';         FieldType: ftDate;    Width: 0; DisplayWidth: 20;  ),
    (Caption: 'Line Item End Date'; FieldName: 'LINE_ITEM_END_DATE'; FieldType: ftDate;    Width: 0; DisplayWidth: 20;  ),
    (Caption: 'Piece'; FieldName: 'CL_PIECES_NBR';                   FieldType: ftInteger; Width: 0; DisplayWidth: 20;  ),
    (Caption: 'Rate Nbr'; FieldName: 'RATE_NUMBER';                  FieldType: ftInteger; Width: 0; DisplayWidth: 10;  ),
    (Caption: 'Check Line Rate of Pay'; FieldName: 'RATE_OF_PAY';    FieldType: ftCurrency; Width: 0; DisplayWidth: 10;  ),
    (Caption: 'Shift'; FieldName: 'CO_SHIFTS_NBR';                   FieldType: ftInteger; Width: 0; DisplayWidth: 20;  ),
    (Caption: 'State'; FieldName: 'EE_STATES_NBR';                   FieldType: ftInteger; Width: 0; DisplayWidth: 2;  ),
    (Caption: 'SUI'; FieldName: 'EE_SUI_STATES_NBR';                 FieldType: ftInteger; Width: 0; DisplayWidth: 2;  ),
    (Caption: 'Workers Comp'; FieldName: 'CO_WORKERS_COMP_NBR';      FieldType: ftInteger; Width: 0; DisplayWidth: 5;  )
  );


  DefaultGridInfo: array [0..1] of TGridInfo =
  (
    (DatasetFirst:  'EE_NBR;PR_CHECK_NBR;' +
               'SOCIAL_SECURITY_NUMBER;CUSTOM_COMPANY_NUMBER;HOME_DBDT;' +
               'Employee_Name_Calculate;CHECK_DATE;RUN_NUMBER;CUSTOM_EMPLOYEE_NUMBER;CHECK_TYPE;';
     DatasetSecond:  'EE_NBR;PR_CHECK_NBR;' +
               'SOCIAL_SECURITY_NUMBER;CUSTOM_COMPANY_NUMBER;HOME_DBDT;' +
               'Employee_Name_Calculate;CHECK_DATE;RUN_NUMBER;CUSTOM_EMPLOYEE_NUMBER;CHECK_TYPE;';
     Selected: 'CHECK_TYPE;CUSTOM_EMPLOYEE_NUMBER;Employee_Name_Calculate;CHECK_DATE;RUN_NUMBER;';
     Index:    'PR_CHECK_NBR';
     ToLoad:   'PR_CHECK_NBR'  ),

    (
     DatasetFirst:  'PR_CHECK_NBR;' +
                    'CHECK_TYPE;CHECK_DATE;RUN_NUMBER;' +
                    'LINES_DBDT;' +
                    'RATE_NUMBER;RATE_OF_PAY;JOB_DESCRIPTION;';
     DatasetSecond:  'EE_NBR;' +
               'CUSTOM_EMPLOYEE_NUMBER;Employee_Name_Calculate;FIRST_NAME;LAST_NAME;' +
               'SOCIAL_SECURITY_NUMBER;CUSTOM_COMPANY_NUMBER;HOME_DBDT;ADDRESS1;ADDRESS2;CITY;STATE;ZIP_CODE;' +
               'BIRTH_DATE;CURRENT_HIRE_DATE;SALARY_AMOUNT;POSITION_DESCRIPTION;POSITION_STATUS_desc;Default_Job;Primary_Rate;';

     Selected: 'CHECK_TYPE;CUSTOM_EMPLOYEE_NUMBER;Employee_Name_Calculate;CHECK_DATE;RUN_NUMBER;';
     Index:    'PR_CHECK_NBR';
     ToLoad:   'PR_CHECK_NBR'  )
  );


function FieldNameForGrid( const f: TDefaultFieldDesc ): string;
function GetStringListFromconst(const fields : String):IisStringList;

implementation

uses
  evtypes, evutils, sPackageEntry,
  SSecurityInterface, SPD_EDIT_SELECT_ITEM, evconsts, strutils,
  evDeferredCall;

{$R *.dfm}

function FieldNameForGrid( const f: TDefaultFieldDesc ): string;
begin
  Result := f.FieldName;
end;

function GetStringListFromconst(const fields : String):IisStringList;
begin
    result := TisStringList.Create;
    result.CommaText := StringReplace(fields , ';', ',', [rfReplaceAll]);
end;

{TQEEGrid}

procedure TQEEGrid.Activate;
begin
  FState := qeegActive;

  if not assigned(FExtraList)  then
  begin
   FExtraList := TStringList.Create;
   FExtraList.Sorted := true;
   FExtraList.Duplicates := dupIgnore;
  end;

  DefaultDatasetFirstFields  :=   GetStringListFromconst(DefaultGridInfo[ord(gridIndex)].DatasetFirst);
  DefaultDatasetSecondFields  :=   GetStringListFromconst(DefaultGridInfo[ord(gridIndex)].DatasetSecond);


  DefaultSelectedFields :=   GetStringListFromconst(DefaultGridInfo[ord(gridIndex)].Selected);

  BuildFieldsMenu;
  EnsureValid(GetUserFields);
end;

procedure TQEEGrid.Deactivate;
begin
  if FState = qeegInactive then Exit;

  FExtraFieldDescs := nil;

  InvalidateAll;
  FState := qeegInactive;
  inherited;
  FreeAndNil( FExtraList );
end;

procedure TQEEGrid.InvalidateAll;
begin
  if assigned(QEE) then
  begin
    FreeAndNil( FIndexDefs );
    if QEE.IndexName = 'SORT' then     //save index generated by grid
    begin
      FIndexDefs := TIndexDefs.Create(nil);
      QEE.IndexDefs.Update;
      FIndexDefs.Assign( QEE.IndexDefs );
    end;
    SaveSettings;
  end;
  FExtraList.Clear;
  FreeAndNil( FQEE );
end;


procedure TQEEGrid.BuildFieldsMenu(const SetDefault : Boolean=false);

    function LoadSettings : boolean;
        procedure AddDefaultSelectedFields;
            procedure AddDefaultExtraFields;
            var
             i : integer;
            begin
              for i := low(FExtraFieldDescs) to high(FExtraFieldDescs) do
                if (FExtraFieldDescs[i].E_D_CODE_TYPE = ED_OEARN_SALARY) or
                   (FExtraFieldDescs[i].E_D_CODE_TYPE = ED_OEARN_REGULAR) then
                  AddExtraField( i );
            end;
        var
         i : integer;
        begin
          for i := low(DefaultFieldDescs) to high(DefaultFieldDescs) do
            if (DefaultSelectedFields.IndexOf(DefaultFieldDescs[i].FieldName) > -1) and
                ( not DefaultFieldDescs[i].Hidden) then
               AddSelected( dgQEE.Selected, DefaultFieldDescs[i].FieldName, DefaultFieldDescs[i].Width, DefaultFieldDescs[i].Caption, false );

          if gridIndex = gCheckLines then
              AddDefaultExtraFields;

        end;

        function TrimGridFieldName( fieldname: string ): string;
        begin
          if RightStr(fieldname, Length('_DESC')) = '_DESC' then
            Result := Copy(fieldname, 1, Length(fieldname) - Length('_DESC') )
          else
            Result := fieldname;
        end;

        function IndexOfDefaultFieldDesc( fieldname: string ): integer;
        var
          i: integer;
        begin
          Result := -1;
          for i := low(DefaultFieldDescs) to high(DefaultFieldDescs) do
            if fieldname = DefaultFieldDescs[i].FieldName then
            begin
              Result := i;
              break;
            end;
        end;

        function IndexOfExtraFieldDesc(edd: TExtraFieldDescs; fieldname: string): integer;
        var
          i: integer;
        begin
          Result := -1;
          for i := low(edd) to high(edd) do
            if fieldname = edd[i].FieldName then
            begin
              Result := i;
              break;
            end;
        end;

    var
      gridkey: string;
      clcokey: string;
      i: integer;
      sel: TSelectedRecArray;
      idx: integer;
    begin

      gridkey := 'Misc\' + Self.ClassName + '\dgQEE';
      clcokey := gridkey + '\CL ' + IntToStr(ctx_DataAccess.ClientID) + ' Grid ' + inttostr(ord(gridIndex));
      sel := ParseSelected(mb_AppSettings[clcokey + '\Selected']);

      dgQEE.Selected.Clear;

      if not SetDefault then
      begin
        //filter out unknown fields
        for i := low(sel) to high(sel) do
        begin
            idx := IndexOfDefaultFieldDesc( TrimGridFieldName(sel[i].FieldName) );
            if idx <> -1 then
            begin
              if not DefaultFieldDescs[idx].Hidden then
                AddDefaultField(idx);
            end
            else
            begin
              idx := IndexOfExtraFieldDesc( FExtraFieldDescs, sel[i].FieldName );
              if idx <> -1 then
                AddExtraField( idx );
            end
        end;
      end;

      //look at what we have after filtering
      sel := ParseSelected(dgQEE.Selected);

      Result := Length(sel) > 0;
      if not Result then   //make sure that all default fields are shown in the grid
        AddDefaultSelectedFields;

      FCurrentColumn := mb_AppSettings[clcokey + '\CurrentColumn'];
    end;

var
  i : integer;
  s : String;
  cd : TevClientDataSet;
  miAdd, miRemove: TMenuItem;
begin
     FExtraFieldDescs := nil;

     miAddField.Clear;
     miRemoveField.Clear;
     miAddED.Clear;
     miRemoveED.Clear;

     for i := low(DefaultFieldDescs) to high(DefaultFieldDescs) do
       if ((DefaultDatasetFirstFields.IndexOf(DefaultFieldDescs[i].FieldName) >  -1) or
           (DefaultDatasetSecondFields.IndexOf(DefaultFieldDescs[i].FieldName) >  -1))
           and (not DefaultFieldDescs[i].Hidden) then
       begin
         AddMenuItem( miAddField, DefaultFieldDescs[i].Caption, HandleAddField, true, i );
         AddMenuItem( miRemoveField, DefaultFieldDescs[i].Caption, HandleRemoveField, false, i );
       end;

     if gridIndex = gCheckLines then
     begin
        cd := TevClientDataset.Create(nil);
        try
            s :='select b.CL_E_DS_NBR, b.DESCRIPTION CodeDescription, b.CUSTOM_E_D_CODE_NUMBER ED_Lookup,b.E_D_CODE_TYPE ' +
                  ' from CL_E_DS b ' +
                     ' where b.CL_E_DS_NBR in ( ' +
                           ' select  distinct  a.CL_E_DS_NBR  from  CO_E_D_CODES  a ' +
                                              ' where {AsOfNow<a>} and a.CO_NBR in ( ' + sFCoList + ' ) ) ' +
                           ' and {AsOfNow<b>} ';

            with TExecDSWrapper.Create(s) do
              ctx_DataAccess.GetCustomData(cd, AsVariant);

            SetLength( FExtraFieldDescs, cd.RecordCount*2 );

            i := 0;
            cd.IndexFieldNames := 'ED_Lookup';
            cd.First;
            while not cd.Eof do
            begin
              miAdd := AddMenuItem( miAddED, cd.FieldByName('ED_Lookup').AsString + ' ' + cd.FieldByName('CodeDescription').AsString, nil, true, -1 );
              miRemove := AddMenuItem( miRemoveED, cd.FieldByName('ED_Lookup').AsString + ' ' + cd.FieldByName('CodeDescription').AsString, nil, true, -1 );

              FExtraFieldDescs[i].FieldName := 'Amt_' + cd.FieldByName('ED_Lookup').AsString;
              FExtraFieldDescs[i].Caption := 'Amt '+ cd.FieldByName('ED_Lookup').AsString + '~' + cd.FieldByName('CodeDescription').AsString;
              FExtraFieldDescs[i].FieldType := qftAmount;

              FExtraFieldDescs[i].CL_E_DS_NBR := cd.FieldByName('CL_E_DS_NBR').AsInteger;
              FExtraFieldDescs[i].CUSTOM_E_D_CODE_NUMBER := cd.FieldByName('ED_Lookup').AsString;
              FExtraFieldDescs[i].E_D_CODE_TYPE := cd.FieldByName('E_D_CODE_TYPE').AsString;

              AddMenuItem( miAdd, 'Amt', HandleAddExtraField, true, i );
              AddMenuItem( miRemove, 'Amt', HandleRemoveEDField, true, i );
              inc(i);

              FExtraFieldDescs[i].FieldName := 'Hrs_' + cd.FieldByName('ED_Lookup').AsString;
              FExtraFieldDescs[i].Caption := 'Hrs '+ cd.FieldByName('ED_Lookup').AsString + '~' + cd.FieldByName('CodeDescription').AsString;
              FExtraFieldDescs[i].FieldType := qftHours;

              FExtraFieldDescs[i].CL_E_DS_NBR := cd.FieldByName('CL_E_DS_NBR').AsInteger;
              FExtraFieldDescs[i].CUSTOM_E_D_CODE_NUMBER := cd.FieldByName('ED_Lookup').AsString;
              FExtraFieldDescs[i].E_D_CODE_TYPE := cd.FieldByName('E_D_CODE_TYPE').AsString;

              AddMenuItem( miAdd, 'Hrs', HandleAddExtraField, true, i );
              AddMenuItem( miRemove, 'Hrs', HandleRemoveEDField, true, i );
              inc(i);

              cd.Next;
            end;
        Finally
           cd.Free;
        end;
     end;

     LoadSettings;

end;


function TQEEGrid.GetUserFields: TUserFields;
var
  i: integer;
  sel: TSelectedRecArray;
  edcount: integer;
  prcount: integer;
begin
  sel := ParseSelected( dgQEE.Selected );

  SetLength( Result.ExtraFieldDescs, dgQEE.Selected.Count );     //max possible    ????

  edcount := 0;
  for i := low(FExtraFieldDescs) to high(FExtraFieldDescs) do
  begin
    if IndexOfSelectedField( sel, FExtraFieldDescs[i].FieldName ) <> -1 then
    begin
      Result.ExtraFieldDescs[edcount] := FExtraFieldDescs[i];
      inc(edcount);
    end;
  end;
  SetLength( Result.ExtraFieldDescs, edcount );


  SetLength( Result.DefaultFields, dgQEE.Selected.Count );      //max possible   ???
  prcount := 0;
  for i := low(DefaultFieldDescs) to high(DefaultFieldDescs) do
    if not DefaultFieldDescs[i].Hidden then
    begin
      if IndexOfSelectedField( sel, FieldNameForGrid( DefaultFieldDescs[i] ) ) <> -1 then
       if (DefaultDatasetFirstFields.IndexOf(DefaultFieldDescs[i].FieldName) >  -1) then
       begin
        Result.DefaultFields[prcount] := DefaultFieldDescs[i].FieldName;
        inc(prcount);
       end;
    end;
  SetLength( Result.DefaultFields, prcount );
end;


procedure TQEEGrid.EnsureValid(const userFields: TUserFields );
var
  idx: integer;

    procedure BuildDataSet(uf: TUserFields);
        procedure AddFloatFieldDef( fname: string );
        begin
          with QEE.FieldDefs.AddFieldDef do
          begin
            DataType := ftFloat;
            Name := fname;
          end;
          with QEE.Aggregates.Add do
          begin
            AggregateName := fname;
            Expression := 'Sum(' + fname + ')';
            Active := True;
          end;
        end;

        procedure FillDataSet;
        var
          cdFirst,  cdSecond : TevClientDataSet;
          sDefault, sExtra, EDCode : String;
          QEDDefaultFieldValues: Variant;
          DefaultFieldValues: Variant;
        begin
           if gridIndex = gCheckLines then
           begin
             sDefault :=
              'select ' +
                 'pr.PR_NBR, pr.CHECK_DATE, pr.RUN_NUMBER, pc.CHECK_TYPE, pc.EE_NBR, ' +
                 'a.PR_CHECK_NBR, a.PR_CHECK_LINES_NBR, ' +
                 'ds.CL_E_DS_NBR, ds.CUSTOM_E_D_CODE_NUMBER, ds.E_D_CODE_TYPE, ' +
                 'a.CL_AGENCY_NBR, a.AMOUNT, a.AGENCY_STATUS, ' +
                 'a.CL_PIECES_NBR, a.CO_BRANCH_NBR, a.CO_JOBS_NBR, ' +
                 'a.CO_SHIFTS_NBR, a.CO_WORKERS_COMP_NBR, a.EE_STATES_NBR, ' +
                 'a.EE_SUI_STATES_NBR, a.RATE_NUMBER, a.RATE_OF_PAY, ' +
                 'a.HOURS_OR_PIECES, t1.DESCRIPTION JOB_DESCRIPTION, ' +
                  'b.CUSTOM_DIVISION_NUMBER, c.CUSTOM_BRANCH_NUMBER, d.CUSTOM_DEPARTMENT_NUMBER, e.CUSTOM_TEAM_NUMBER , ' +
                  'coalesce(trim(b.CUSTOM_DIVISION_NUMBER), '''') || ' +
                  'coalesce('' | ''' + ' || trim(c.CUSTOM_BRANCH_NUMBER)  , '''') || ' +
                  'coalesce('' | ''' + ' || trim(d.CUSTOM_DEPARTMENT_NUMBER) , '''') || ' +
                  'coalesce('' | ''' + ' || trim(e.CUSTOM_TEAM_NUMBER) , '''') LINES_DBDT ' +
              'from ' +
                'PR_CHECK_LINES a ' +
                'left join CO_JOBS t1 on a.CO_JOBS_NBR = t1.CO_JOBS_NBR and {AsOfNow<t1>} ' +
                'left join CO_WORKERS_COMP t2 on a.CO_WORKERS_COMP_NBR = t2.CO_WORKERS_COMP_NBR and {AsOfNow<t2>} ' +
                'left join CL_AGENCY t3 on a.CL_AGENCY_NBR = t3.CL_AGENCY_NBR and {AsOfNow<t3>} ' +
                'left join CO_SHIFTS t4 on a.CO_SHIFTS_NBR = t4.CO_SHIFTS_NBR and {AsOfNow<t4>} ' +
                'left join CL_PIECES t5 on a.CL_PIECES_NBR = t5.CL_PIECES_NBR and {AsOfNow<t5>} ' +
                'left join CO_DIVISION b on a.CO_DIVISION_NBR = b.CO_DIVISION_NBR and {AsOfNow<b>} ' +
                'left join CO_BRANCH c on a.CO_BRANCH_NBR = c.CO_BRANCH_NBR and {AsOfNow<c>} ' +
                'left join CO_DEPARTMENT d on a.CO_DEPARTMENT_NBR = d.CO_DEPARTMENT_NBR and {AsOfNow<d>} ' +
                'left join CO_TEAM e on a.CO_TEAM_NBR = e.CO_TEAM_NBR and {AsOfNow<e>} ' +
                'join CL_E_DS ds on a.CL_E_DS_NBR = ds.CL_E_DS_NBR and {AsOfNow<ds>} ' +
                'join PR pr on a.PR_NBR = pr.PR_NBR and {AsOfNow<pr>} ' +
                'join PR_CHECK pc on a.PR_CHECK_NBR = pc.PR_CHECK_NBR and {AsOfNow<pc>} ' +
                'where {AsOfNow<a>} and ' + BuildINsqlStatement('a.PR_CHECK_NBR', sFCheckFilter);

             sExtra :=
             'select ' +
               'pte.EE_NBR, pte.CL_PERSON_NBR, pte.CO_NBR, ' +
               'pte.CUSTOM_EMPLOYEE_NUMBER, pte.CURRENT_HIRE_DATE, ' +
               'pte.SALARY_AMOUNT, pl.SOCIAL_SECURITY_NUMBER, ' +
               'pl.FIRST_NAME, pl.LAST_NAME, ' +
               'pl.FIRST_NAME || '' '' || pl.LAST_NAME Employee_Name_Calculate, ' +
               'pl.ADDRESS1, pl.ADDRESS2, pl.CITY, pl.STATE, pl.ZIP_CODE, ' +
               'pl.BIRTH_DATE, ' +
               'po.CUSTOM_COMPANY_NUMBER, ' +
                  'coalesce(trim(d1.CUSTOM_DIVISION_NUMBER), '''') || ' +
                  'coalesce('' | ''' + ' || trim(d2.CUSTOM_BRANCH_NUMBER)  , '''') || ' +
                  'coalesce('' | ''' + ' || trim(d3.CUSTOM_DEPARTMENT_NUMBER) , '''') || ' +
                  'coalesce('' | ''' + ' || trim(d4.CUSTOM_TEAM_NUMBER) , '''') HOME_DBDT, ' +
               't1.DESCRIPTION POSITION_DESCRIPTION, ' +
               'CASE pte.POSITION_STATUS ' +
                'WHEN ''N'' THEN ''N/A'' ' +
                'WHEN ''F'' THEN ''Full Time'' ' +
                'WHEN ''U'' THEN ''Full Time Temp'' ' +
                'WHEN ''P'' THEN ''Part Time'' ' +
                'WHEN ''R'' THEN ''Part Time temp'' ' +
                'WHEN ''H'' THEN ''Half Time'' ' +
                'WHEN ''S'' THEN ''Seasonal'' ' +
                'WHEN ''T'' THEN ''Student'' ' +
                'WHEN ''1'' THEN ''1099'' ' +
                'ELSE ''Other'' ' +
                'END POSITION_STATUS_Desc, ' +
                't2.DESCRIPTION Default_Job, ' +
                't3.RATE_AMOUNT Primary_Rate ' +
               'from EE pte ' +
                   'join CL_PERSON pl on pte.CL_PERSON_NBR = pl.CL_PERSON_NBR and {AsOfNow<pl>} ' +
                   'join CO po on pte.CO_NBR = po.CO_NBR and {AsOfNow<po>} ' +
                   'left join CO_DIVISION d1 on pte.CO_DIVISION_NBR = d1.CO_DIVISION_NBR and {AsOfNow<d1>} ' +
                   'left join CO_BRANCH d2 on pte.CO_BRANCH_NBR = d2.CO_BRANCH_NBR and {AsOfNow<d2>} ' +
                   'left join CO_DEPARTMENT d3 on pte.CO_DEPARTMENT_NBR = d3.CO_DEPARTMENT_NBR and {AsOfNow<d3>} ' +
                   'left join CO_TEAM d4 on pte.CO_TEAM_NBR = d4.CO_TEAM_NBR and {AsOfNow<d4>} ' +
                   'left join CO_HR_POSITIONS t1 on pte.CO_HR_POSITIONS_NBR = t1.CO_HR_POSITIONS_NBR and {AsOfNow<t1>} ' +
                   'left join CO_JOBS t2 on pte.CO_JOBS_NBR = t2.CO_JOBS_NBR and {AsOfNow<t2>} ' +
                   'left join EE_RATES t3 on pte.EE_NBR = t3.EE_NBR and {AsOfNow<t3>} and t3.PRIMARY_RATE = ''Y'' ' +
               'where {AsOfNow<pte>} and ' + BuildINsqlStatement('pte.EE_NBR', sFEEFilter);
           end;

           if gridIndex = gChecks then    
           begin
             sDefault :=
              'select pr.PR_NBR, pe.PR_CHECK_NBR, ' +
                 'pe.EE_NBR,  pr.CHECK_DATE, pr.RUN_NUMBER, ' +
                 'pe.CHECK_TYPE, pe.CUSTOM_EMPLOYEE_NUMBER, ' +
                 'pe.SOCIAL_SECURITY_NUMBER, pe.FIRST_NAME, pe.LAST_NAME, ' +
                 'pe.Employee_Name_Calculate, ' +
                 'pe.ADDRESS1, pe.ADDRESS2, pe.CITY, pe.STATE, pe.ZIP_CODE, ' +
                 'pe.BIRTH_DATE, pe.CURRENT_HIRE_DATE, pe.SALARY_AMOUNT, ' +
                 'pe.HOME_DBDT, ' +
                 'pe.CUSTOM_COMPANY_NUMBER ' +

               'from ' +
                  'PR pr, ' +
                  '( ' +
                   'select ' +
                     'pc1.PR_NBR,  pc1.PR_CHECK_NBR, pc1.CHECK_TYPE, ' +

                     'pte.EE_NBR,   pte.CL_PERSON_NBR,  pte.CO_NBR,  pte.CUSTOM_EMPLOYEE_NUMBER, ' +
                     'pte.CURRENT_HIRE_DATE,  pte.SALARY_AMOUNT, ' +

                     'pl.SOCIAL_SECURITY_NUMBER, ' +
                     'pl.FIRST_NAME,   pl.LAST_NAME, ' +
                     'pl.FIRST_NAME || '' '' || pl.LAST_NAME Employee_Name_Calculate, ' +

                     'pl.ADDRESS1, pl.ADDRESS2,  pl.CITY, pl.STATE, pl.ZIP_CODE, pl.BIRTH_DATE, ' +

                     'po.CUSTOM_COMPANY_NUMBER, ' +
                     'coalesce(trim(d1.CUSTOM_DIVISION_NUMBER), '''') || ' +
                     'coalesce('' | ''' + ' || trim(d2.CUSTOM_BRANCH_NUMBER)  , '''') || ' +
                     'coalesce('' | ''' + ' || trim(d3.CUSTOM_DEPARTMENT_NUMBER) , '''') || ' +
                     'coalesce('' | ''' + ' || trim(d4.CUSTOM_TEAM_NUMBER) , '''') HOME_DBDT ' +
                     'from EE pte ' +
                         'join PR_CHECK pc1 on pte.EE_NBR = pc1.EE_NBR and {AsOfNow<pc1>} ' +
                                                       'and  pc1.PR_CHECK_NBR in ( ' + sFCheckFilter + ' ) ' +
                         'join CL_PERSON pl on pte.CL_PERSON_NBR = pl.CL_PERSON_NBR and {AsOfNow<pl>} ' +
                         'join CO po on pte.CO_NBR = po.CO_NBR and {AsOfNow<po>} ' +

                         'left join CO_DIVISION d1 on pte.CO_DIVISION_NBR = d1.CO_DIVISION_NBR and {AsOfNow<d1>} ' +
                         'left join CO_BRANCH d2 on pte.CO_BRANCH_NBR = d2.CO_BRANCH_NBR and {AsOfNow<d2>} ' +
                         'left join CO_DEPARTMENT d3 on pte.CO_DEPARTMENT_NBR = d3.CO_DEPARTMENT_NBR and {AsOfNow<d3>} ' +
                         'left join CO_TEAM d4 on pte.CO_TEAM_NBR = d4.CO_TEAM_NBR and {AsOfNow<d4>} ' +

                     'where {AsOfNow<pte>} ' +
                   ') pe ' +
                 'where ' +
                   'pr.PR_NBR = pe.PR_NBR and {AsOfNow<pr>} ';
           end;

          cdFirst := TevClientDataset.Create(nil);
          cdSecond   := TevClientDataset.Create(nil);
          try
              with TExecDSWrapper.Create(sDefault) do
                 ctx_DataAccess.GetCustomData(cdFirst, AsVariant);

              with TExecDSWrapper.Create(sExtra) do
                 ctx_DataAccess.GetCustomData(cdSecond, AsVariant);

              QEE.DisableControls;
              try
                 QEDDefaultFieldValues := Null;
                 if gridIndex = gCheckLines then
                 begin
                      cdFirst.IndexFieldNames := FDefaultFields.ToLoad;
                      cdSecond.IndexFieldNames := 'EE_NBR';
                      cdFirst.First;
                      while not cdFirst.EOF do
                      begin
                            EDCode := cdFirst.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
                            if (EDCode <> '') and
                               (FExtraList.IndexOf(EDCode) > -1) then
                            begin
                              Assert(cdSecond.Locate('EE_NBR', cdFirst['EE_NBR'], []));
                              DefaultFieldValues := cdFirst[FDefaultFields.ToLoad];

                              if VarIsNull(QEDDefaultFieldValues)
                                 or not ValuesAreEqual(QEDDefaultFieldValues, DefaultFieldValues, FDefaultFields.ToLoadCount)
                                 or not QEE.FieldByName('Nbr_'+EDCode).IsNull then
                              begin
                                  if QEE.State <> dsBrowse then
                                    QEE.Post;

                                  QEE.Append;
                                  QEE[DefaultGridInfo[ord(gridIndex)].DatasetFirst] := cdFirst[DefaultGridInfo[ord(gridIndex)].DatasetFirst];
                                  QEE[DefaultGridInfo[ord(gridIndex)].DatasetSecond] := cdSecond[DefaultGridInfo[ord(gridIndex)].DatasetSecond];

                                  QEDDefaultFieldValues := DefaultFieldValues;
                              end;

                              QEE['Hrs_'+EDCode] := cdFirst.FieldByName('HOURS_OR_PIECES').AsVariant;
                              QEE['Amt_'+EDCode] := cdFirst.FieldByName('AMOUNT').AsVariant;
                              QEE['Nbr_'+EDCode] := cdFirst.FieldByName('PR_CHECK_LINES_NBR').Value;
                            end;
                       cdFirst.Next;
                      end;
                 end;

{  //  when we add new grid
                   if gridIndex = gChecks then
                   begin
                        DefaultFieldValues := cdDefault[FDefaultFields.ToLoad];

                        if VarIsNull(QEDDefaultFieldValues)
                           or not ValuesAreEqual(QEDDefaultFieldValues, DefaultFieldValues, FDefaultFields.ToLoadCount) then
                        begin
                            if QEE.State <> dsBrowse then
                              QEE.Post;

                            QEE.Append;
                            QEE[DefaultGridInfo[ord(gridIndex)].Dataset] := cdDefault[DefaultGridInfo[ord(gridIndex)].Dataset];
                            QEDDefaultFieldValues := DefaultFieldValues;
                        end;
                   end;
}

                if QEE.State <> dsBrowse then
                   QEE.Post;
              finally
                QEE.EnableControls;
              end;
          Finally
           cdFirst.Free;
           cdSecond.Free;
          End;
        end;

    var
      i: integer;
      s: string;
    begin
        Assert( QEE = nil );
        Assert( FExtraList.Count = 0 );

        SetAssign( FDefaultFields.User, uf.DefaultFields );
        if Length(FDefaultFields.User ) > 0 then
        begin
          FDefaultFields.ToLoad := FDefaultFields.ToLoad + ';' + SetToStr(FDefaultFields.User ,';');
          FDefaultFields.ToLoadCount := FDefaultFields.ToLoadCount + Length(FDefaultFields.User );
        end;

        FQEE := TevClientDataSet.Create(nil);
        FQEE.EnableVersioning := false;
        FQEE.AggregatesActive := True;

        for i := low(DefaultFieldDescs) to high(DefaultFieldDescs) do
         if (DefaultDatasetFirstFields.IndexOf(DefaultFieldDescs[i].FieldName) > -1) or
            (DefaultDatasetSecondFields.IndexOf(DefaultFieldDescs[i].FieldName) > -1) then
           QEE.FieldDefs.Add( DefaultFieldDescs[i].FieldName, DefaultFieldDescs[i].FieldType, DefaultFieldDescs[i].Width, false);

        if gridIndex = gCheckLines then
        begin
          for i := low(uf.ExtraFieldDescs) to high(uf.ExtraFieldDescs) do
            FExtraList.AddObject( uf.ExtraFieldDescs[i].CUSTOM_E_D_CODE_NUMBER, TObject(uf.ExtraFieldDescs[i].CL_E_DS_NBR) );

          for i := 0 to FExtraList.Count-1 do
          begin
            AddFloatFieldDef( 'Amt_'+FExtraList[i] );
            AddFloatFieldDef( 'Hrs_'+FExtraList[i] );

            with QEE.FieldDefs.AddFieldDef do
            begin
              DataType := ftInteger;
              Name := 'Nbr_'+FExtraList[i];
            end;
          end;
        end;

        QEE.CreateFields;
        QEE.FieldDefs.Clear;

        if gridIndex = gCheckLines then
        begin

          for i := 0 to FExtraList.Count-1 do
            (QEE.FieldByName( 'Amt_'+FExtraList[i] ) as TNumericField).DisplayFormat := '#,##0.00';

        end;

        if FIndexDefs <> nil then
        begin
          s := ValidateFields( QEE, FIndexDefs.Find('SORT').Fields );
          if s <> '' then
          begin
            QEE.AddIndex('SORT', s, [ixCaseInsensitive], ValidateFields( QEE, FIndexDefs.Find('SORT').DescFields ) );
            QEE.IndexName := 'SORT';
          end;
          FreeAndNil( FIndexDefs );
        end
        else
        begin
          s := DefaultGridInfo[ord(gridIndex)].Index;
          QEE.IndexFieldNames := ValidateFields( QEE, s );
        end;

        QEE.CreateDataSet;
        FillDataSet;
    end;

    function AreUserFieldsUptodate( const userFields: TUserFields): boolean;
    var
      ueds: TStringSet;
      i: integer;
    begin
      for i := low(userFields.extraFieldDescs) to high(userFields.extraFieldDescs) do
        SetInclude(ueds, userFields.extraFieldDescs[i].CUSTOM_E_D_CODE_NUMBER);
      Result := Length(ueds) = FExtraList.Count;
      if Result then
        for i := low(ueds) to high(ueds) do
          if FExtraList.IndexOf(ueds[i]) = -1 then
          begin
            Result := false;
            break;
          end;
      Result := Result and SetEqual( FDefaultFields.User, userFields.DefaultFields );
    end;

begin
  dsQEE.DataSet := nil;
  dgQEE.ApplySelected;

  if (not AreUserFieldsUptodate(userFields)) or
      not (QEE = nil) then InvalidateAll;

  FDefaultFields.ToLoad := DefaultGridInfo[ord(gridIndex)].ToLoad;
  FDefaultFields.ToLoadCount := 1;

  BuildDataset(userFields);

  dsQEE.DataSet := QEE;

  idx := IndexOfSelectedField( ParseSelected(dgQEE.Selected), FCurrentColumn );
  if idx <> -1 then
  begin
    dgQEE.SelectedIndex := idx;
    FCurrentColumn := '';
  end;
end;

procedure TQEEGrid.SaveSettings;
var
  gridkey: string;
  clcokey: string;
  sel: TSelectedRecArray;
  activeColumn: string;
begin
  SetLength(sel, 0);     //avoiding compiler hint
  if FState = qeegInactive then
    Exit;

  gridkey := 'Misc\' + Self.ClassName + '\dgQEE';
  clcokey := gridkey + '\CL ' + IntToStr(ctx_DataAccess.ClientID) + ' Grid ' + inttostr(ord(gridIndex));

  if ctx_DataAccess.ClientID > 0 then
  begin
    mb_AppSettings.DeleteNode( clcokey );
    mb_AppSettings[clcokey + '\Selected'] := dgQEE.Selected.Text;
    activeColumn := '';
    if dgQEE.SelectedIndex <> -1 then
    begin
      sel := ParseSelected( dgQEE.Selected );
      activeColumn := sel[dgQEE.SelectedIndex].FieldName;
    end;
    mb_AppSettings[clcokey + '\CurrentColumn'] := activeColumn;
  end;
end;


procedure TQEEGrid.RestoreDefaultsClick(Sender: TObject);
begin
  BuildFieldsMenu(true);
  EnsureValid(GetUserFields);
  QEE.First;
end;


procedure TQEEGrid.dgQEEBeforeDrawCell(Sender: TwwCustomDBGrid;
  DrawCellInfo: TwwCustomDrawGridCellInfo);
begin
  if (gdFixed in DrawCellInfo.State) and
     (DrawCellInfo.DataRow >= 0) then
    Sender.Canvas.Font := TevDBGrid(Sender).Font;
end;

procedure TQEEGrid.dgQEECalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  if (gdFixed in State) and
     (ABrush.Color = TevDBGrid(Sender).TitleColor) then
    if not TevDBGrid(Sender).IsAlternatingRow(TevDBGrid(Sender).CalcCellRow) then
      ABrush.Color := TevDBGrid(Sender).Color
    else
      ABrush.Color := TevDBGrid(Sender).PaintOptions.AlternatingRowColor;
{
  if assigned(Field) and assigned(Field.DataSet) and Field.DataSet.Active and ((State = []) or (State = [gdFixed])) then
  begin
    if (Field.DataSet.FieldByName('NOTES').AsString = 'Y') then
    begin
      AFont.Color := clBlack; //!!temp fix
      ABrush.Color := clYellow;
    end;

    if (Field.DataSet.FieldByName('LineTypeSort').AsString = CHECK_LINE_TYPE_SCHEDULED) then
      AFont.Color := clRed;
  end;
}
end;


procedure TQEEGrid.AddDefaultField(idx: integer);
var
  f: PDefaultFieldDescs;
begin
  f := @DefaultFieldDescs[idx];
  AddSelected( dgQEE.Selected, FieldNameForGrid(f^), f.DisplayWidth, f.Caption, false);
end;

procedure TQEEGrid.AddExtraField( idx: integer );
var
  f: PExtraFieldDesc;
begin
  f := @FExtraFieldDescs[idx];
  AddSelected( dgQEE.Selected, f.FieldName, 10, f.Caption, ctx_AccountRights.Functions.GetState('FIELDS_WAGES') <> stEnabled );
end;

procedure TQEEGrid.HandleAddField(Sender: TObject);
begin
  AddDefaultField( (Sender as TMenuItem).Tag );
  EnsureValid(GetUserFields);
end;

procedure TQEEGrid.HandleRemoveField(Sender: TObject);
begin
  dgQEE.Selected.Delete( IndexOfSelectedField( ParseSelected(dgQEE.Selected), FieldNameForGrid( DefaultFieldDescs[(Sender as TMenuItem).Tag] ) ) );
  dgQEE.SetControlType( FieldNameForGrid( DefaultFieldDescs[(Sender as TMenuItem).Tag] ) , fctField, '');
  EnsureValid(GetUserFields);
end;

procedure TQEEGrid.HandleAddExtraField(Sender: TObject);
begin
  AddExtraField( (Sender as TMenuItem).Tag );
  EnsureValid(GetUserFields);
end;

procedure TQEEGrid.HandleRemoveEDField(Sender: TObject);
var
  f: PExtraFieldDesc;
begin
  f := @FExtraFieldDescs[(Sender as TMenuItem).Tag];
  dgQEE.Selected.Delete( IndexOfSelectedField( ParseSelected(dgQEE.Selected), f.FieldName ) );
  dgQEE.SetControlType( f.FieldName, fctField, '');
  EnsureValid(GetUserFields);
end;

procedure TQEEGrid.pmFieldsPopup(Sender: TObject);
var
  i : integer;
  vis: boolean;
  visCount: integer;
  totalCount: integer;
  sel: TSelectedRecArray;

  procedure UpdateEDMenu(miED: TMenuItem; needvis: boolean );
  var
    i: integer;
    j: integer;
    localvisCount: integer;
  begin
    viscount := 0;
    for i := 0 to miED.Count-1 do
    begin
      localvisCount := 0;
      for j := 0 to miED.Items[i].Count-1 do
      begin
        vis := IndexOfSelectedField( sel, FExtraFieldDescs[miED.Items[i].Items[j].Tag].FieldName ) <> -1;
        miED.Items[i].Items[j].Visible := vis = needvis;
        if vis = needvis then
          inc(localvisCount);
      end;
      miED.Items[i].Visible := localvisCount <> 0;
      if miED.Items[i].Visible then
        inc(visCount);
    end;
    miED.Enabled := visCount <> 0;
  end;
begin
  visCount := 0;
  totalCount := 0;
  sel := ParseSelected( dgQEE.Selected );


  for i := low(DefaultFieldDescs) to high(DefaultFieldDescs) do
       if ((DefaultDatasetFirstFields.IndexOf(DefaultFieldDescs[i].FieldName) >  -1) or
           (DefaultDatasetSecondFields.IndexOf(DefaultFieldDescs[i].FieldName) >  -1))
            and (not DefaultFieldDescs[i].Hidden) then
    begin
      inc(totalCount);
      vis := IndexOfSelectedField( sel, FieldNameForGrid( DefaultFieldDescs[i] ) ) <> -1;
      if vis then
        inc(visCount);

      FindByTag( miAddField, i ).Visible := not vis;
      FindByTag( miRemoveField, i ).Visible := vis;
    end;

  miAddField.Enabled := visCount < totalCount;
  miRemoveField.Enabled  := visCount > 0;

  UpdateEDMenu(miAddED, false);
  UpdateEDMenu(miRemoveED, true);
  miAddEDDlg.Enabled := miAddED.Enabled;
end;

procedure TQEEGrid.miAddEDDlgClick(Sender: TObject);
begin
  with TEDIT_SELECT_ITEM.Create( Self ) do
  try
    RenderQPMenu( miAddED );
    if ShowModal = mrOk then
      Selected.Click;
  finally
    Free;
  end;
end;


end.


