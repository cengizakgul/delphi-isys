inherited REPORT_SEARCH: TREPORT_SEARCH
  Width = 736
  Height = 551
  AutoScroll = False
  AutoSize = False
  object evPanel2: TevPanel [0]
    Left = 400
    Top = 136
    Width = 627
    Height = 145
    BevelOuter = bvNone
    TabOrder = 0
  end
  object evPanel1: TevPanel [1]
    Left = 96
    Top = 233
    Width = 627
    Height = 427
    BevelOuter = bvNone
    TabOrder = 1
    object lblResult: TevLabel
      Left = 208
      Top = 80
      Width = 30
      Height = 13
      Caption = 'Result'
    end
  end
  object sbReportSearch: TScrollBox [2]
    Left = 0
    Top = 0
    Width = 736
    Height = 551
    Align = alClient
    TabOrder = 2
    object fpParameters: TisUIFashionPanel
      Left = 8
      Top = 8
      Width = 636
      Height = 183
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'fpParameters'
      Color = 14737632
      TabOrder = 0
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Parameters'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      object lblSearchFor: TevLabel
        Left = 12
        Top = 35
        Width = 52
        Height = 13
        Caption = 'Search For'
      end
      object gbSearchPlace: TevGroupBox
        Left = 12
        Top = 74
        Width = 185
        Height = 87
        Caption = 'Search in'
        TabOrder = 1
        object chbSystem: TevCheckBox
          Left = 12
          Top = 18
          Width = 97
          Height = 17
          Caption = 'System Reports'
          TabOrder = 0
        end
        object chbClient: TevCheckBox
          Left = 12
          Top = 60
          Width = 97
          Height = 17
          Caption = 'Client Reports'
          TabOrder = 2
        end
        object chbSB: TevCheckBox
          Left = 12
          Top = 39
          Width = 97
          Height = 17
          Caption = 'SB Reports'
          TabOrder = 1
        end
      end
      object edPattern: TevEdit
        Left = 12
        Top = 50
        Width = 328
        Height = 21
        TabOrder = 0
        OnKeyPress = edPatternKeyPress
      end
      object chbMatchWords: TevCheckBox
        Left = 261
        Top = 92
        Width = 135
        Height = 17
        Caption = 'Match whole word only'
        TabOrder = 2
      end
      object chbMatchCase: TevCheckBox
        Left = 261
        Top = 113
        Width = 135
        Height = 17
        Caption = 'Match case'
        TabOrder = 3
      end
      object btnStart: TevButton
        Left = 342
        Top = 50
        Width = 54
        Height = 21
        Action = Start
        TabOrder = 4
        Color = clBlack
        ParentColor = False
        Margin = 0
      end
    end
    object fpResults: TisUIFashionPanel
      Left = 12
      Top = 199
      Width = 636
      Height = 342
      BevelOuter = bvNone
      BorderWidth = 12
      Caption = 'fpParameters'
      Color = 14737632
      TabOrder = 1
      RoundRect = True
      ShadowDepth = 8
      ShadowSpace = 8
      ShowShadow = True
      ShadowColor = clSilver
      TitleColor = clGrayText
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWhite
      TitleFont.Height = -13
      TitleFont.Name = 'Arial'
      TitleFont.Style = [fsBold]
      Title = 'Results'
      LineWidth = 0
      LineColor = clWhite
      Theme = ttCustom
      object evDBGrid1: TevDBGrid
        Left = 12
        Top = 35
        Width = 604
        Height = 285
        DisableThemesInTitle = False
        Selected.Strings = (
          'Level'#9'1'#9'Level'#9#9
          'Nbr'#9'10'#9'Number'#9#9
          'Name'#9'50'#9'Report Description'#9'F'#9
          'FieldFound'#9'50'#9'Field found'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TREPORT_SEARCH\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsList
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 633
    Top = 108
  end
  inherited wwdsDetail: TevDataSource
    Left = 665
    Top = 108
  end
  inherited wwdsList: TevDataSource
    DataSet = dsReports
    Left = 602
    Top = 108
  end
  object dsReports: TevClientDataSet
    FieldDefs = <
      item
        Name = 'Level'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Nbr'
        DataType = ftInteger
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 80
      end>
    OpenWithLookups = False
    Left = 566
    Top = 108
    object dsReportsLevel: TStringField
      DisplayWidth = 1
      FieldName = 'Level'
      Size = 1
    end
    object dsReportsNbr: TIntegerField
      DisplayLabel = 'Number'
      DisplayWidth = 10
      FieldName = 'Nbr'
    end
    object dsReportsName: TStringField
      DisplayLabel = 'Report Description'
      DisplayWidth = 50
      FieldName = 'Name'
      Size = 80
    end
    object dsReportsFieldFound: TStringField
      DisplayLabel = 'Field found'
      DisplayWidth = 50
      FieldName = 'FieldFound'
      Size = 50
    end
  end
  object Actions: TevActionList
    Left = 405
    Top = 95
    object Start: TAction
      Caption = 'Start'
      OnExecute = StartExecute
      OnUpdate = StartUpdate
    end
    object Stop: TAction
      Caption = 'Stop'
      OnExecute = StopExecute
      OnUpdate = StopUpdate
    end
  end
end
