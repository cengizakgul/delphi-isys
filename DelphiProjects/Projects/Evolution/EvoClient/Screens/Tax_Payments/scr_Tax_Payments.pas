// Screens of "Tax Pmnts"
unit scr_Tax_Payments;

interface

uses
  STaxPaymentsScr,
  SPD_PROCESS_CLEAR_CREDIT_LIAB,
  SPD_ProcessTaxPayments,
  SPD_EFTPS_Enrollment,
  SPD_MarkEftdLiabilities,
  SPD_Prenote_EFT_ACH,
  SPD_PROCESS_REPORT_CREDIT_LIAB,
  SPD_ReCreatePaymentFile;

implementation

end.
