// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit STaxPaymentsScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList,
  SDataStructure, EvTypes, EvUtils, Graphics, ISBasicClasses, EvCommonInterfaces,
  EvMainboard, EvExceptions, EvUIComponents, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TTaxPaymentsFrm = class(TFramePackageTmpl, IevTaxPaymentsScreens)
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  ActivatePackage(WorkPlace: TWinControl): Boolean; override;
    function  PackageBitmap: TBitmap; override;
  end;


implementation

{$R *.DFM}

var
  TaxPaymentsFrm: TTaxPaymentsFrm;

function GetTaxPaymentsScreens: IevTaxPaymentsScreens;
begin
  if not Assigned(TaxPaymentsFrm) then
    TaxPaymentsFrm := TTaxPaymentsFrm.Create(nil);
  Result := TaxPaymentsFrm;
end;



{ TSystemFrm }

function TTaxPaymentsFrm.ActivatePackage(WorkPlace: TWinControl): Boolean;
begin
  Result := inherited ActivatePackage(WorkPlace);
  if Result then
  begin
    DM_TEMPORARY.TMP_CL.Activate;
    if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
      raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);
  end;
end;

function TTaxPaymentsFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TTaxPaymentsFrm.PackageCaption: string;
begin
  Result := '&Tax pmnts';
end;

function TTaxPaymentsFrm.PackageSortPosition: string;
begin
  Result := '150';
end;

procedure TTaxPaymentsFrm.UserPackageStructure;
begin
  AddStructure('Tax payments|020', 'TProcessTaxPayments');
  AddStructure('EFTPS Enrollment|030', 'TEFTPS_Enrollment');
  AddStructure('Statusing EFT file as paid|040', 'TMarkEftdLiabilities');
  AddStructure('Report credit liabilities (overpaid)|050', 'TPROCESS_REPORT_CREDIT_LIAB');
  AddStructure('Clear credit liabilities as paid|060', 'TPROCESS_CLEAR_CREDIT_LIAB');
  AddStructure('Prenote EFT payment|070', 'TPrenote_EFT_ACH');
  //AddStructure('Re-create Tax Payment File|080', 'TReCreatePaymentFile');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTaxPaymentsScreens, IevTaxPaymentsScreens, 'Screens Tax Payments');

finalization
  TaxPaymentsFrm.Free;

end.
