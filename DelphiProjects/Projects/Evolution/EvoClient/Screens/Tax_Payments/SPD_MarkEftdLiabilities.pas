// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_MarkEftdLiabilities;

interface

uses
  SFrameEntry,  Db,  StdCtrls, Controls,
  Grids, Wwdbigrd, Wwdbgrid, Classes, Wwdatsrc, Buttons, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents, EvContext,
  EvDataAccessComponents, SDDClasses, SDataStructure, SDataDictbureau, EvUIComponents, EvClientDataSet;

type
  TMarkEftdLiabilities = class(TFrameEntry)
    cd: TevClientDataSet;
    ds: TevDataSource;
    cdReference: TStringField;
    cdDate_: TDateTimeField;
    cdCL_NBR: TIntegerField;
    cdCO_NBR: TIntegerField;
    wwDBGrid1: TevDBGrid;
    Label1: TevLabel;
    cdTemp: TevClientDataSet;
    Button1: TevBitBtn;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
      IsBatchProvider: Boolean;
  public
    { Public declarations }
    procedure Activate; override;
  end;

var
  MarkEftdLiabilities: TMarkEftdLiabilities;

implementation

{$R *.DFM}

uses
  EvConsts, SPD_EFTPS_Enrollment, EvUtils, SysUtils, Forms;

procedure TMarkEftdLiabilities.Button1Click(Sender: TObject);
var
  s: string;
  s2: string;
  ResponseList, ConfNbrList, TraceList: TStringList;
begin
  inherited;
  ResponseList := TStringList.Create;
  ConfNbrList := TStringList.Create;
  TraceList := TStringList.Create;

  ctx_StartWait;
  try
    cdTemp.First;
    while not cdTemp.Eof do
    begin
      s2 := Trim(cdTemp.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString);
      if Copy(s2, 1, 5) = 'EFTD#' then
        Delete(s2, 1, 5);

      if length(s2) = 15 then
        s := Copy(s2, 1, 11)
      else
        s := s2;

      if s = cd['Reference'] then
      begin
        s2 := s2 + '=0000';
        if ResponseList.IndexOf(s2) = -1 then
          ResponseList.Append(s2);
      end;
      cdTemp.Next;
    end;
    {s := cd['Reference'];
    if Copy(s, 1, 5) = 'EFTD#' then
      s := Copy(s, 6, 15);
    ResponseList.Append(s + '=0000');}
    ProcessPaymentResponse(ResponseList, ConfNbrList, TraceList,IsBatchProvider);
    cd.Delete;
    cd.MergeChangeLog;
  finally
    ResponseList.Free;
    ConfNbrList.Free;
    TraceList.Free;
    ctx_EndWait;
  end;
end;

procedure TMarkEftdLiabilities.Activate;
var
  s: string;
  s2: string;
begin
  inherited;

  ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB]);
  IsBatchprovider :=  (DM_SERVICE_BUREAU.SB.FieldByName('EFTPS_BANK_FORMAT').AsString[1] = GROUP_BOX_BATCH_PROVIDER);

  s := GetReadOnlyClintCompanyFilter(True,'b.','c.');

  ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('GetUnpaidEFTPDebits') do
  begin
    SetParam('STATUS', TAX_DEPOSIT_STATUS_PENDING);
    SetParam('DEPOSIT_TYPE', TAX_DEPOSIT_TYPE_DEBIT);
    SetMacro('cond', s);
    ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
  cdTemp.Close;
  cdTemp.Data := ctx_DataAccess.TEMP_CUSTOM_VIEW.Data;
  cdTemp.First;
  cd.CreateDataSet;
  while not cdTemp.Eof do
  begin
    s2 := Trim(cdTemp.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString);
    if Copy(s2, 1, 5) = 'EFTD#' then
      Delete(S2, 1, 5);
    if Length(s2) = 15 then
    begin
      s := Copy(s2, 1, 11);
    end
    else
    begin
      s := s2;
    end;
    if (not cd.Locate('REFERENCE', s, [])) or (Length(s) = 20) then
    begin
      cd.Insert;
      cd['REFERENCE'] := s;
      if s <> '' then
      try
        if (Length(s) = 11) then
          cd['DATE_'] := EncodeDate(StrToInt(Copy(s, 1, 4)), StrToInt(Copy(s, 5, 2)), StrToInt(Copy(s, 7, 2)))
        else
          cd['DATE_'] := EncodeDate(StrToInt(Copy(s, 1, 2)), StrToInt(Copy(s, 3, 2)), StrToInt(Copy(s, 5, 2)));
      except
        Exception.Create('It appears you have payments left over from a different bank format!');
      end;
      cd['CO_NBR'] := cdTemp['CO_NBR'];
      cd['CL_NBR'] := cdTemp['CL_NBR'];
      cd.Post;
    end;
    cdTemp.Next;
  end;
end;

initialization
  RegisterClass(TMARKEFTDLIABILITIES);

end.
