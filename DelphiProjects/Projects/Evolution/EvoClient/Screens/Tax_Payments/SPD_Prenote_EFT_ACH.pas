// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Prenote_EFT_ACH;

interface

uses
  SPD_EDIT_GLOBAL_CO_BASE, StdCtrls, wwdblook,  ExtCtrls, Db, EvBasicUtils,
   Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, Variants, EvContext,
  Controls, ComCtrls, Classes, SDataStructure, evAchEFT,
  SDDClasses, SDataDictsystem, ISBasicClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SACHTypes, evAchBase, isBaseClasses, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TPrenote_EFT_ACH = class(TEDIT_GLOBAL_CO_BASE)
    rgrpTaxType: TevRadioGroup;
    wwlcState: TevDBLookupCombo;
    Label1: TevLabel;
    Label2: TevLabel;
    bbtnAdd: TevBitBtn;
    bbtnCreate: TevBitBtn;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    procedure bbtnAddClick(Sender: TObject);
    procedure bbtnCreateClick(Sender: TObject);
  private
    { Private declarations }
    FSequenceNbr: Integer;
    ACH: TevAchEFT;
    AllClients: TStringList;
  public
    { Public declarations }
    procedure Activate; override;
    procedure Deactivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  Prenote_EFT_ACH: TPrenote_EFT_ACH;

implementation

uses EvUtils, Dialogs, SysUtils, EvTypes, EvConsts;

{$R *.DFM}

procedure TPrenote_EFT_ACH.bbtnAddClick(Sender: TObject);
begin
  inherited;
  if wwlcState.Text = '' then
    Exit;
  if EvMessage('Are you sure you want to add selected companies for ' + wwlcState.Text + '?', mtConfirmation, [mbYes, mbNo]) = mrYes then
  begin
    ctx_StartWait;
    try
      ACH.InPrenote := True;
      SelectedCompanies.First;
      while not SelectedCompanies.EOF do
      begin
        ctx_DataAccess.OpenClient(SelectedCompanies.FieldByName('CL_NBR').AsInteger);
        if AllClients.IndexOf(SelectedCompanies.FieldByName('CL_NBR').AsString) = -1 then
          AllClients.Add(SelectedCompanies.FieldByName('CL_NBR').AsString);
        DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
        DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + SelectedCompanies.FieldByName('CO_NBR').AsString);
        DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + SelectedCompanies.FieldByName('CO_NBR').AsString);
        Inc(FSequenceNbr);
        if DM_COMPANY.CO_STATES.Locate('STATE', wwlcState.Text, []) then
          case rgrpTaxType.ItemIndex of
          0: // state
            ACH.ProcessEFTPayment(0, Null, Date, GetEndQuarter(Date), FSequenceNbr, DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').AsInteger, 0, ttState);
          1: // SUI
            ACH.ProcessEFTPayment(0, Null, Date, GetEndQuarter(Date), FSequenceNbr, DM_COMPANY.CO_SUI.Lookup('CO_STATES_NBR', DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').AsInteger, 'CO_SUI_NBR'), 0, ttSUI);
          else
            Assert(False)
          end;
        SelectedCompanies.Next;
      end;

      bbtnCreate.Enabled := (ACH.AchRecordCount > 0);
    finally
      ctx_EndWait;
    end;
  end;
end;

procedure TPrenote_EFT_ACH.bbtnCreateClick(Sender: TObject);
var
  D: TSaveDialog;
  s: String;
begin
  inherited;
  ctx_StartWait;
  try
    ACH.SaveAch;
    D := TSaveDialog.Create(nil);
    if ACH.ACHFiles.Count > 0 then
    try
      D.Title := 'Prenote ACH file';
      D.FileName := ACH.FileName;
      if D.Execute then
      begin
        ACH.FileName := ExtractFileName(D.FileName);
        s := ExtractFileDir(D.FileName);
        if Length(s) > 0 then
          if s[Length(s)] = '\' then
            s := copy(s,1,Length(s)-1);
        (IInterface(ACH.ACHFiles[0].Value) as IisStringList).SaveToFile(D.FileName);
      end;
    finally
      D.Free;
    end;
  finally
    ctx_EndWait;
  end;
  AllClients.Clear;
  bbtnCreate.Enabled := False;
  GenerateAchReport(True, True, ACH.FileSaved, ACH.FileName, s, ACH.ACHFile, ACH.ACHDescription);
  ACH.Free;
  ACH := TevAchEFT.Create(nil);
  LoadACHOptionsFromRegistry(ACH.Options);
  ACH.bPrenote := True;
end;

procedure TPrenote_EFT_ACH.Activate;
var
 sfilter : String;
begin
  inherited;
  ACH := TevAchEFT.Create(nil);
  LoadACHOptionsFromRegistry(ACH.Options);
  ACH.bPrenote := True;
  FSequenceNbr := 0;
  DM_SYSTEM_STATE.SY_STATES.IndexFieldNames := 'STATE';
  AllClients := TStringList.Create;


  sFilter := GetReadOnlyClintCompanyListFilter(false);
  if sFilter <> '' then
  begin
     if DM_TEMPORARY.TMP_CO.filter <> '' then
        DM_TEMPORARY.TMP_CO.filter := DM_TEMPORARY.TMP_CO.filter + ' and ' + sFilter
     else
        DM_TEMPORARY.TMP_CO.filter := sFilter;
     DM_TEMPORARY.TMP_CO.filtered := True;
  end;

end;

procedure TPrenote_EFT_ACH.Deactivate;
begin
  ACH.Free;
  AllClients.Free;
  inherited;
  DM_TEMPORARY.TMP_CO.Filter := '';
  DM_TEMPORARY.TMP_CO.Filtered := false;

end;

procedure TPrenote_EFT_ACH.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATES, SupportDataSets, '');
end;

initialization
  RegisterClass(TPRENOTE_EFT_ACH);

end.
