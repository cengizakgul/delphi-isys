// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ProcessTaxPayments;

interface

uses
  Windows, SFrameEntry,  Dialogs, Db,  StdCtrls,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, EvBasicUtils,
  Buttons, ComCtrls, Controls, Classes, Wwdatsrc, Graphics, Menus, Variants,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents, EvContext,
  EvDataAccessComponents, SDataDictClient, SDDClasses, SDataDictbureau,
  SDataStructure, EvMainboard, isVCLBugFix, EvExceptions, EvDataset, EvCommonInterfaces,
  isUIwwDBComboBox, isUIEdit, LMDCustomButton, LMDButton, isUILMDButton,
  EvUIComponents, EvClientDataSet, EvUIUtils;

type
  TChangeSelectedOption = (csoAllImpounded, csoAllExceptNSF, csoAll, csoNone);
  TProcessTaxPayments = class(TFrameEntry)
    PageControl1: TevPageControl;
    SelectTabSheet: TTabSheet;
    DetailTabSheet: TTabSheet;
    btnGetTaxesDue: TevBitBtn;
    Panel1: TevPanel;
    Label7: TevLabel;
    PrinTevButton: TevBitBtn;
    PayButton: TevBitBtn;
    Splitter1: TevSplitter;
    wwDBGrid1: TevDBGrid;
    DateLabel: TevLabel;
    SortByRadioGroup: TevRadioGroup;
    gbDateGroup: TevGroupBox;
    Label2: TevLabel;
    Label4: TevLabel;
    wwdsGrid1: TevDataSource;
    wwdsGrid2: TevDataSource;
    wwdsGrid3: TevDataSource;
    BitBtn7: TevBitBtn;
    RecalcButton: TevBitBtn;
    SaveDialog1: TSaveDialog;
    FromDate: TevDateTimePicker;
    ToDate: TevDateTimePicker;
    ExcelButton: TevBitBtn;
    TEMP_CO: TevClientDataSet;
    TEMP_PR_LIABILITY: TevClientDataSet;
    TEMP_DETAIL: TevClientDataSet;
    TEMP_TAX_TYPE: TevClientDataSet;
    wwdsTEMP_DETAIL: TevDataSource;
    wwDBGrid2: TevDBGrid;
    Splitter2: TevSplitter;
    wwDBGrid3: TevDBGrid;
    cbFromDateRange: TevCheckBox;
    rgClientTaxType: TevRadioGroup;
    cbPayMethod: TevComboBox;
    Label3: TevLabel;
    GroupBox1: TevGroupBox;
    bbAdd: TevBitBtn;
    bbRemove: TevBitBtn;
    btnAddAll: TevBitBtn;
    Label8: TevLabel;
    cbTaxTable: TevComboBox;
    wwDBComboBox1: TevDBComboBox;
    wwDBComboBox2: TevDBComboBox;
    pcAvail: TevPageControl;
    tsByAgency: TTabSheet;
    tsByTaxes: TTabSheet;
    evSplitter1: TevSplitter;
    evSplitter2: TevSplitter;
    btnChangeSelected: TevBitBtn;
    evPopupMenu1: TevPopupMenu;
    SelectAllImpounded1: TMenuItem;
    SelectAllExceptNSF1: TMenuItem;
    SelectAllRegardlessStatus1: TMenuItem;
    DeselectAll1: TMenuItem;
    N1: TMenuItem;
    AutoRecalcTotals1: TMenuItem;
    cbForceChecks: TevCheckBox;
    evLabel17: TevLabel;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    cbSelectFilteredRecords: TevCheckBox;
    tsMaintenance: TTabSheet;
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    evDBGrid1: TevDBGrid;
    cdMaintenanceCo: TevClientDataSet;
    dsMaintenanceCo: TevDataSource;
    cdMaintenanceCoCL_NBR: TIntegerField;
    cdMaintenanceCoCO_NBR: TIntegerField;
    cdMaintenanceCoCUSTOM_COMPANY_NUMBER: TStringField;
    cdMaintenanceCoNAME: TStringField;
    evLabel1: TevLabel;
    cbEMail: TevCheckBox;
    eEMail: TevEdit;
    cbEmailSendRule: TevComboBox;
    evLabel9: TevLabel;
    cbPostProcessReport: TevComboBox;
    edtTaxDesc: TevEdit;
    evLabel2: TevLabel;
    DetailRollBack: TTabSheet;
    dsTaxPayment: TevDataSource;
    evTaxPaymentList: TevBitBtn;
    cdSBTaxPayment: TevClientDataSet;
    cdSBTaxPaymentDESCRIPTION: TStringField;
    cdSBTaxPaymentPaymentDate: TDateTimeField;
    cdSBTaxPaymentStatus_desc: TStringField;
    cdSBTaxPaymentSTATUS_DATE: TDateTimeField;
    cdSBTaxPaymentSB_TAX_PAYMENT_NBR: TIntegerField;
    cdSBTaxPaymentSTATUS: TStringField;
    TaxListPageControl: TevPageControl;
    TaxListDetail: TTabSheet;
    TaxListLiabs: TTabSheet;
    evPanel3: TevPanel;
    btnRollBack: TevBitBtn;
    evcbTenDays: TevCheckBox;
    evstatusFilter: TevRadioGroup;
    evGrdRollBack: TevDBGrid;
    evGrdTaxListLiab: TevDBGrid;
    cdSBTaxPaymentLiabs: TevClientDataSet;
    dsTaxPaymentLiabs: TevDataSource;
    evPanel4: TevPanel;
    btnTaxListLiab: TevBitBtn;
    cdSBTaxPaymentLiabsCUSTOM_COMPANY_NUMBER: TStringField;
    cdSBTaxPaymentLiabsName: TStringField;
    cdSBTaxPaymentLiabsLiabType: TStringField;
    cdSBTaxPaymentLiabsDEPOSIT_TYPE_desc: TStringField;
    cdSBTaxPaymentLiabsAmount: TCurrencyField;
    cdSBTaxPaymentLiabsDeposit_Type: TStringField;
    cdSBTaxPaymentLiabsCo_Tax_Deposits_Nbr: TIntegerField;
    cdSBTaxPaymentLiabsCl_Nbr: TIntegerField;
    cdSBTaxPaymentLiabsCo_Nbr: TIntegerField;
    cdSBTaxPaymentLiabsSb_Tax_Payment_Nbr: TIntegerField;
    cdSBTaxPaymentLiabsSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField;
    cdSBTaxPaymentLiabsSY_GLOBAL_AGENCY_NBR: TIntegerField;
    cdSBTaxPaymentLiabsAGENCY_NAME: TStringField;
    evCbCoNbr: TevComboBox;
    evCbAgencyName: TevComboBox;
    evCbLiabType: TevComboBox;
    evCbDepType: TevComboBox;
    cdSBTaxPaymentLiabsCUSTOM_CLIENT_NUMBER: TStringField;
    evCbClNbr: TevComboBox;
    btnRollBack2: TevBitBtn;
    evPanel5: TevPanel;
    evBtnClose: TevBitBtn;
    evPanel6: TevPanel;
    evBtnClose2: TevBitBtn;
    cdSBTaxPaymentLiabsCHECK_DATE: TDateTimeField;
    cdSBTaxPaymentLiabsDUE_DATE: TDateTimeField;
    cbMiscCheckForm: TevComboBox;
    cdAgencyFrom: TevClientDataSet;
    cdAgencyTo: TevClientDataSet;
    cdTaxFrom: TevClientDataSet;
    cdTaxTo: TevClientDataSet;
    grdTaxFrom: TevDBGrid;
    dsAgencyFrom: TevDataSource;
    dsAgencyTo: TevDataSource;
    dsTaxFrom: TevDataSource;
    dsTaxTo: TevDataSource;
    grdTaxTo: TevDBGrid;
    grdAgencyFrom: TevDBGrid;
    grdAgencyTo: TevDBGrid;
    cdAgencyTaxLink: TevClientDataSet;
    cdTCDAgencyList: TevClientDataSet;

    procedure bbAddClick(Sender: TObject);
    procedure bbRemoveClick(Sender: TObject);
    procedure SortByRadioGroupClick(Sender: TObject);
    procedure wwdsGrid1DataChange(Sender: TObject; Field: TField);
    procedure wwdsGrid2DataChange(Sender: TObject; Field: TField);
    procedure BitBtn7Click(Sender: TObject);
    procedure btnGetTaxesDueClick(Sender: TObject);
    procedure RecalcButtonClick(Sender: TObject);
    procedure PrintButtonClick(Sender: TObject);
    procedure PayButtonClick(Sender: TObject);
    procedure ExcelButtonClick(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure wwDBGrid3CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnAddAllClick(Sender: TObject);
    procedure cbTaxTableChange(Sender: TObject);
    procedure wwDBComboBox1CloseUp(Sender: TwwDBComboBox; Select: Boolean);
    procedure btnChangeSelectedClick(Sender: TObject);
    procedure AutoRecalcTotals1Click(Sender: TObject);
    procedure SelectAllImpounded1Click(Sender: TObject);
    procedure SelectAllRegardlessStatus1Click(Sender: TObject);
    procedure DeselectAll1Click(Sender: TObject);
    procedure SelectAllExceptNSF1Click(Sender: TObject);
    procedure cbEMailClick(Sender: TObject);
    procedure DetailTabSheetShow(Sender: TObject);
    procedure cbPostProcessReportClick(Sender: TObject);
    procedure evTaxPaymentListClick(Sender: TObject);
    procedure btnRollBackClick(Sender: TObject);
    procedure evcbTenDaysClick(Sender: TObject);
    procedure evstatusFilterClick(Sender: TObject);
    procedure TaxListPageControlChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure evBtnClose2Click(Sender: TObject);
    procedure btnTaxListLiabClick(Sender: TObject);
    procedure cdSBTaxPaymentLiabsCalcFields(DataSet: TDataSet);
    procedure evCbCoNbrChange(Sender: TObject);
    procedure evGrdTaxListLiabDrawFooterCell(Sender: TObject;
      Canvas: TCanvas; FooterCellRect: TRect; Field: TField;
      FooterText: String; var DefaultDrawing: Boolean);
    procedure pcAvailChange(Sender: TObject);

  private
    { Private declarations }
    MiscCheckFormCodeValues: TStrings;
    FPaymentWoFundsOn: Boolean;
    cdtmpTCDAgencyList: IevDataSet;
    cdsyTCDAgencyFreqList: IevDataSet;
    cdsyLocalsState: IevDataSet;
    bgrdAgencyControl : Boolean;
    tmpClientID, tmpCoNbr: integer;
    function TaxServiceSelected(const Service: string): Boolean;
    function MethodSelected(const Method: string): Boolean;
    procedure DoChangeSelected(const Option: TChangeSelectedOption);
    procedure DoChangeSelectedCompany(const Option: TChangeSelectedOption);
    procedure AutoCreditMo(const PeriodEndDate: TDateTime);
    procedure LoadAsciiReportNames;
    procedure GetSbTaxPayment;
    procedure GetSbTaxPaymentLiabs(const SbTaxPaymentNbr : integer);
    procedure SetTaxListComboBoxs(const ds : TevClientDataSet; const Field : String; const cb : tevComboBox);

    procedure cdLiabLocalsCalculate(DataSet: TDataSet; const NBR : integer);
    procedure cdLiabLocalsRegularCalculate(DataSet: TDataSet);
    procedure cdLiabLocalsFillerCalculate(DataSet: TDataSet);

    function tmp_TCDAgencyList: IevDataSet;
    function sy_TCDAgencyFreqList: IevDataSet;
    function sy_TCDLocalsState: IevDataSet;
    procedure Get_cdAgencyTaxData(const bLink :Boolean = false);
    procedure Get_cdTaxData;
    procedure cdAddProcess(var cdFrom, cdTo : TevClientDataSet; const bAgency : Boolean= false);
    procedure addGridClick(var cdFrom,cdTo :TevClientDataSet; var grdFrom, grdTo: TevDBGrid; const bMoveAll : Boolean=false);
    procedure deleteGridClick(var cdFrom ,cdTo : TevClientDataSet; grdFrom, grdTo: TevDBGrid);

    procedure Update_cdTaxTo;

  public
    { Public declarations }
    InRecalculateFlags: boolean;
    function FromDateText: string;
    function ToDateText: string;
    procedure MakeAllButPayReadOnly(aDataSet: TevClientDataSet; IsReadOnly: boolean);
    procedure UpdateGridViewsColumns;
    procedure AfterLiabilityPost(DataSet: TDataSet);
    procedure PopulateDetailData;
    procedure ReadData;
    procedure ForceRecalc;
    procedure RecalculateFlags;
    procedure AfterDetailPost(DataSet: TDataSet);
    procedure AfterMasterPost(DataSet: TDataSet);
    procedure ChangePayStatusForDetail(DETAIL_NBR: integer; NewPayStatus: string);
    procedure ValidatePaidBox(Sender: TField);
    procedure Activate; override;
    procedure Deactivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  ProcessTaxPayments: TProcessTaxPayments;

implementation

uses SDM_MISC_LIAB_CORRECT, EvConsts, SFieldCodeValues, EvUtils,  EvTypes, SysUtils, SSecurityInterface,
     DateUtils, SDataDictsystem;

{$R *.DFM}
var
  InReport: boolean = False;

function TProcessTaxPayments.FromDateText: string;
begin
  result := FormatDateTime('mm/dd/yyyy', FromDate.Date);
end;

function TProcessTaxPayments.ToDateText: string;
begin
  result := FormatDateTime('mm/dd/yyyy', ToDate.Date);
end;

procedure TProcessTaxPayments.MakeAllButPayReadOnly(aDataSet: TevClientDataSet; IsReadOnly: boolean);
var
  I: integer;
begin
  with aDataSet do
  begin
    for I := 0 to FieldCount - 1 do
    begin
      with Fields[I] do
      begin
        if not (FieldName = 'PAY') then
        begin
          ReadOnly := IsReadOnly;
        end;
      end;
    end;
  end;
end;

procedure TProcessTaxPayments.UpdateGridViewsColumns;
begin
  if SortByRadioGroup.ItemIndex = 0 then
  begin
    wwdsGrid1.DataSet := TEMP_TAX_TYPE;
    wwDBGrid2.Selected.Text :=
      'Name' + #9 + '20' + #9 + 'Name' + #9 + 'F' + #13 + #10 +
      'Number' + #9 + '10' + #9 + 'Co#' + #9 + 'F' + #13 + #10 +
      'Client_Number' + #9 + '10' + #9 + 'Cl#' + #9 + 'F' + #13 + #10 +
      'EIN' + #9 + '30' + #9 + 'EIN' + #9 + 'F' + #13 + #10 +
      'Method' + #9 + '4' + #9 + 'Method' + #9 + 'F' + #13 + #10 +
      'HasNSF' + #9 + '2' + #9 + 'HasNSF' + #9 + 'F' + #13 + #10 +
      'Frequency' + #9 + '8' + #9 + 'Freq' + #9 + 'F' + #13 + #10 +
      'PAY' + #9 + '6' + #9 + 'Pay' + #9 + 'F' + #13 + #10 +
      'AMOUNT' + #9 + '12' + #9 + 'Amt to be paid' + #9 + 'F' + #13 + #10 +
      'AMOUNT_I' + #9 + '12' + #9 + 'Amount imp.' + #9 + 'F' + #13 + #10 +
      'AMOUNT_P' + #9 + '12' + #9 + 'Amount pend.' + #9 + 'F';
  end
  else
  begin
    wwdsGrid1.DataSet := TEMP_CO;
    wwDBGrid2.Selected.Text :=
      'EIN' + #9 + '30' + #9 + 'EIN' + #9 + 'F' + #13 + #10 +
      'Tax_Agency' + #9 + '10' + #9 + 'Tax_Agency' + #9 + 'F' + #13 + #10 +
      'Method' + #9 + '4' + #9 + 'Method' + #9 + 'F' + #13 + #10 +
      'HasNSF' + #9 + '2' + #9 + 'HasNSF' + #9 + 'F' + #13 + #10 +
      'Frequency' + #9 + '8' + #9 + 'Freq' + #9 + 'F' + #13 + #10 +
      'PAY' + #9 + '6' + #9 + 'Pay' + #9 + 'F' + #13 + #10 +
      'AMOUNT' + #9 + '12' + #9 + 'Amt to be paid' + #9 + 'F' + #13 + #10 +
      'AMOUNT_I' + #9 + '12' + #9 + 'Amount imp.' + #9 + 'F' + #13 + #10 +
      'AMOUNT_P' + #9 + '12' + #9 + 'Amount pend.' + #9 + 'F';
  end;
  wwDBGrid1.SetControlType('PAY', fctCustom, 'wwDBComboBox2');
  wwDBGrid1.Selected.Text :=
    'Tax_Agency' + #9 + '20' + #9 + 'Tax Agency' + #9 + 'F' + #13 + #10 +
    'Name' + #9 + '20' + #9 + 'Name' + #9 + 'F' + #13 + #10 +
    'Number' + #9 + '10' + #9 + 'Co#' + #9 + 'F' + #13 + #10 +
    'Client_Number' + #9 + '10' + #9 + 'Cl#' + #9 + 'F' + #13 + #10 +
    'TAX_SERVICE' + #9 + '2' + #9 + 'TAX' + #9 + 'F' + #13 + #10 +
    'Report_Agency' + #9 + '13' + #9 + 'Type' + #9 + 'F' + #13 + #10 +
    'HasNSF' + #9 + '2' + #9 + 'HasNSF' + #9 + 'F' + #13 + #10 +
    'PAY' + #9 + '7' + #9 + 'Pay' + #9 + 'F' + #13 + #10 +
    'AMOUNT' + #9 + '12' + #9 + 'Amt to be paid' + #9 + 'F' + #13 + #10 +
    'AMOUNT_I' + #9 + '12' + #9 + 'Amount imp.' + #9 + 'F' + #13 + #10 +
    'AMOUNT_P' + #9 + '12' + #9 + 'Amount pend.' + #9 + 'F';
  wwDBGrid3.Selected.Text :=
    'RUN_NUMBER' + #9 + '10' + #9 + 'Run #' + #9 + 'F' + #13 + #10 +
    'CHECK_DATE' + #9 + '10' + #9 + 'Check date' + #9 + 'F' + #13 + #10 +
    'DUE_DATE' + #9 + '10' + #9 + 'Due date' + #9 + 'F' + #13 + #10 +
    'Status' + #9 + '10' + #9 + 'Status' + #9 + 'F' + #13 + #10 +
    'Name' + #9 + '40' + #9 + 'Name' + #9 + 'F' + #13 + #10 +
    'Tax_Type' + #9 + '15' + #9 + 'Type' + #9 + 'F' + #13 + #10 +
    'HasNSF' + #9 + '2' + #9 + 'HasNSF' + #9 + 'F' + #13 + #10 +
    'PAY' + #9 + '2' + #9 + 'Pay' + #9 + 'F' + #13 + #10 +
    'AMOUNT' + #9 + '15' + #9 + 'Amount' + #9 + 'F';
  wwDBGrid3.SetControlType('PAY', fctCheckBox, 'Y;N');
  wwDBGrid2.SetControlType('PAY', fctCustom, 'wwDBComboBox1');
  wwDBGrid1.ApplySelected;
  wwDBGrid2.ApplySelected;
  wwDBGrid3.ApplySelected;
end;


procedure TProcessTaxPayments.SortByRadioGroupClick(Sender: TObject);
begin
  inherited;
  UpdateGridViewsColumns;

  if SortByRadioGroup.ItemIndex = 0 then
  begin
     cbSelectFilteredRecords.State := cbUnchecked;
     cbSelectFilteredRecords.Enabled := False;
  end
  else
     cbSelectFilteredRecords.Enabled := True;
end;

procedure TProcessTaxPayments.wwdsGrid1DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Field = nil then
  begin
    if not (wwdsGrid1.DataSet.State in [dsBrowse]) then
      exit;

    if wwdsGrid1.DataSet = TEMP_CO then
    begin
      if TEMP_CO.EOF and TEMP_CO.BOF then
        TEMP_DETAIL.Filter := 'TEMP_CO_NBR = -1'
      else
        TEMP_DETAIL.Filter := 'TEMP_CO_NBR = ' + TEMP_CO.FieldByName('TEMP_CO_NBR').AsString;
    end
    else
      if wwdsGrid1.DataSet = TEMP_TAX_TYPE then
    begin
      if TEMP_TAX_TYPE.EOF and TEMP_TAX_TYPE.BOF then
        TEMP_DETAIL.Filter := 'TAX_TYPE_NBR = -1'
      else
        TEMP_DETAIL.Filter := 'TAX_TYPE_NBR = ' + TEMP_TAX_TYPE.FieldByName('TAX_TYPE_NBR').AsString;
    end;
    TEMP_DETAIL.Filtered := True;
  end;
end;

function TProcessTaxPayments.MethodSelected(const Method: string): Boolean;
begin
  Assert(Length(Method) > 0);
  Result := (cbPayMethod.ItemIndex = 0)
  or ((cbPayMethod.ItemIndex = 1) and (Method[1] in [TAX_PAYMENT_METHOD_CREDIT, TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT]))
  or ((cbPayMethod.ItemIndex = 2) and (Method[1] in [TAX_PAYMENT_METHOD_DEBIT, TAX_PAYMENT_METHOD_NOTICES_EFT_DEBIT]))
  or ((cbPayMethod.ItemIndex = 3) and (Method[1] in [TAX_PAYMENT_METHOD_CHECK, TAX_PAYMENT_METHOD_NOTICES_CHECKS]))
  or ((cbPayMethod.ItemIndex = 4) and (Method[1] in [TAX_PAYMENT_METHOD_NOTICES]))
  or ((cbPayMethod.ItemIndex = 5) and (Method[1] in [TAX_PAYMENT_METHOD_CREDIT]))
  or ((cbPayMethod.ItemIndex = 6) and (Method[1] in [TAX_PAYMENT_METHOD_NOTICES_EFT_CREDIT]));
end;

function TProcessTaxPayments.TaxServiceSelected(
  const Service: string): Boolean;
begin
  Result := ((rgClientTaxType.ItemIndex = 0)
    or ((rgClientTaxType.ItemIndex = 1) and (Service = TAX_SERVICE_FULL))
    or ((rgClientTaxType.ItemIndex = 2) and (Service = TAX_SERVICE_DIRECT))
    or ((rgClientTaxType.ItemIndex = 3) and (Service = TAX_SERVICE_NONE)))
end;

procedure TProcessTaxPayments.ReadData;
var
  s: string;
  StateList, tmpCondition, ConditionFiller: string;
  j: integer;
  TEMP_CO_NBR_GEN: integer;
  TEMP_DETAIL_NBR_GEN: integer;
  TAX_TYPE_NBR_GEN: integer;
  TEMP_PR_LIABILITY_GEN: integer;
  DetailNbr: integer;
  CL_NBR: integer;
  Nbr: Integer;
  Tax_Service: string;
  DepMethod: Char;

  cdLiabLocalsRegular: TevClientDataset;
  cdLiabLocalsFiller: TevClientDataset;

  function GetTMP_CO_NBR(CL_NBR, CO_NBR: integer): integer;
  begin
    if not TEMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([CL_NBR, CO_NBR]), []) then
      with TEMP_CO do
      begin
        Insert;
        FieldByName('CL_NBR').AsInteger := CL_NBR;
        FieldByName('CO_NBR').AsInteger := CO_NBR;
        DM_TEMPORARY.TMP_CL.Locate('CL_NBR', CL_NBR, []);
        DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([CL_NBR, CO_NBR]), []);
        FieldByName('HasNSF').AsString := '-';
        FieldByName('TAX_SERVICE').AsString := DM_TEMPORARY.TMP_CO.FieldByName('TAX_SERVICE').AsString;
        FieldByName('Name').AsString := DM_TEMPORARY.TMP_CO.FieldByName('Name').AsString;
        FieldByName('Number').AsString := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
        FieldByName('Client_Number').AsString := DM_TEMPORARY.TMP_CL.FieldByName('CUSTOM_CLIENT_NUMBER').AsString;
        FieldByName('TEMP_CO_NBR').AsInteger := TEMP_CO_NBR_GEN;
        TEMP_CO_NBR_GEN := TEMP_CO_NBR_GEN + 1;
        Post;
      end;
    Result := TEMP_CO.FieldByName('TEMP_CO_NBR').AsInteger;
  end;

  function GetTMP_TAX_TYPE(const pTaxTable: string; const pNbr: Integer; const pTax_TYPE: string;const TCDAgencyNbr, paymentSY_LOCALS_NBR : integer): Integer;
  var
    GlobalAgencyNbr, tmpSyNbr: integer;
    aTaxType, aDescription, sReportAgency: string;
  begin
    Result := -1;
    GlobalAgencyNbr := -1;

    sReportAgency := '';
    case pTaxTable[1] of
       'F' : sReportAgency := 'Federal';
       'S' : sReportAgency := 'State';
       'U' : sReportAgency := 'SUI';
       'L' : sReportAgency := 'Local';
    end;

    tmpSyNbr := pNbr;
    if  paymentSY_LOCALS_NBR <> -1 then
        tmpSyNbr := paymentSY_LOCALS_NBR;

    if cdTaxTo.Locate('TaxType;Nbr',VarArrayOf([pTaxTable,tmpSyNbr]),[]) then
    begin
      if (pTaxTable[1] = 'F') then
      begin
         while (cdTaxTo.FieldByName('Nbr').AsInteger = tmpSyNbr) and
                (cdTaxTo.FieldByName('TaxType').AsString = pTaxTable) and
               (not cdTaxTo.Eof) do
         begin
            if (Pos(pTax_TYPE, cdTaxTo.FieldByName('PaymentTaxType').AsString) > 0) then
            begin
             GlobalAgencyNbr := cdTaxTo.FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger;
             aDescription := cdTaxTo.FieldByName('NAME').AsString;
             aTaxType := pTaxTable + cdTaxTo.FieldByName('PaymentTaxType').AsString;
             break;
            end;
            cdTaxTo.Next;
         end;
      end
      else
      begin
          if TCDAgencyNbr <> 0 then
          begin
             GlobalAgencyNbr := TCDAgencyNbr;
             Assert(DM_SYSTEM.SY_GLOBAL_AGENCY.Locate('SY_GLOBAL_AGENCY_NBR',TCDAgencyNbr,[]));
             aDescription := DM_SYSTEM.SY_GLOBAL_AGENCY.FieldByName('AGENCY_NAME').AsString;
             aTaxType := pTaxTable + IntToStr(TCDAgencyNbr);
             sReportAgency := 'TCD';
          end
          else
          begin
             GlobalAgencyNbr := cdTaxTo.FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger;
             aDescription := cdTaxTo.FieldByName('NAME').AsString;
             aTaxType := pTaxTable + cdTaxTo.FieldByName('Nbr').AsString;
          end;
      end;
    end;

    if GlobalAgencyNbr = -1 then
      Exit;
    if not TEMP_TAX_TYPE.Locate('SY_GLOBAL_AGENCY_NBR;TAX_TYPE', VarArrayof([GlobalAgencyNbr, aTaxType]), []) then
      with TEMP_TAX_TYPE do
      begin
        Insert;
        FieldByName('TAX_TYPE_NBR').AsInteger := TAX_TYPE_NBR_GEN;
        TAX_TYPE_NBR_GEN := TAX_TYPE_NBR_GEN + 1;
        FieldByName('TAX_TYPE').AsString := aTaxType;
        FieldByName('Tax_Agency').AsString := aDescription;
        FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger := GlobalAgencyNbr;
        FieldByName('Report_Agency').AsString := sReportAgency;
        Post;
      end;
    Result := TEMP_TAX_TYPE.FieldByName('TAX_TYPE_NBR').AsInteger;
  end;

  function CreateDetail_NBR(const CL_NBR, CO_NBR: Integer; const TaxTable: String; const Nbr: Integer; const Tax_TYPE: string; var
    Tax_Service: string; const TCDAgencyNbr, paymentSY_LOCALS_NBR : integer): Integer;
  var
    TEMP_CO_NBR: Integer;
    TEMP_TAX_TYPE_NBR: Integer;
  begin
    Result := -1;
    TEMP_TAX_TYPE_NBR := GetTMP_TAX_TYPE(TaxTable, Nbr, Tax_TYPE, TCDAgencyNbr, paymentSY_LOCALS_NBR);
    if TEMP_TAX_TYPE_NBR = -1 then
      Exit;
    TEMP_CO_NBR := GetTMP_CO_NBR(CL_NBR, CO_NBR);
    Tax_Service := Temp_CO.FieldByName('TAX_SERVICE').AsString;
    if not TEMP_DETAIL.Locate('TEMP_CO_NBR;TAX_TYPE_NBR', VarArrayOf([TEMP_CO_NBR, TEMP_TAX_TYPE_NBR]), []) then
      with TEMP_DETAIL do
      begin
        Insert;
        FieldByName('DETAIL_NBR').AsInteger := TEMP_DETAIL_NBR_GEN;
        TEMP_DETAIL_NBR_GEN := TEMP_DETAIL_NBR_GEN + 1;
        FieldByName('TEMP_CO_NBR').AsInteger := TEMP_CO_NBR;
        FieldByName('TAX_TYPE_NBR').AsInteger := TEMP_TAX_TYPE_NBR;
        if TaxTable = 'F' then
        begin
          Assert(DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([CL_NBR, CO_NBR]), []));
          FieldByName('EIN').AsString := DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
          if Tax_TYPE = TAX_LIABILITY_TYPE_ER_FUI then
          begin
            ctx_DataAccess.OpenClient(CL_NBR);
            DM_COMPANY.CO.DataRequired('CO_NBR='+ IntToStr(CO_NBR));
            Assert(DM_COMPANY.CO.RecordCount=1);

            FieldByName('Frequency').AsString := ConvertCode(TCO, 'FUI_TAX_DEPOSIT_FREQUENCY', DM_COMPANY.CO['FUI_TAX_DEPOSIT_FREQUENCY'])
          end
          else
            FieldByName('Frequency').AsString := ConvertCode(TCO, 'FEDERAL_TAX_DEPOSIT_FREQUENCY', DM_TEMPORARY.TMP_CO['FEDERAL_TAX_DEPOSIT_FREQUENCY']);
        end
        else
        if TaxTable = 'S' then
        begin
          Assert(DM_SYSTEM_STATE.SY_STATES.FindKey([Nbr]));
          Assert(DM_TEMPORARY.TMP_CO_STATES.Locate('CL_NBR;CO_NBR;STATE', VarArrayOf([CL_NBR, CO_NBR, DM_SYSTEM_STATE.SY_STATES['STATE']]),
            []));
          FieldByName('EIN').AsString := DM_TEMPORARY.TMP_CO_STATES.FieldByName('STATE_EIN').AsString;
          Assert(DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.FindKey([DM_TEMPORARY.TMP_CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR']]));
          FieldByName('Frequency').AsString := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ['DESCRIPTION'];
        end
        else
        if TaxTable = 'U' then
        begin
          Assert(DM_SYSTEM_STATE.SY_SUI.FindKey([Nbr]));
          Assert(DM_SYSTEM_STATE.SY_STATES.FindKey([DM_SYSTEM_STATE.SY_SUI['SY_STATES_NBR']]));
          if not DM_TEMPORARY.TMP_CO_states.Locate('CL_NBR;CO_NBR;STATE', VarArrayOf([CL_NBR, CO_NBR, DM_SYSTEM_STATE.SY_STATES['STATE']]), []) then
            raise EInconsistentData.CreateHelp('Company ' + DM_TEMPORARY.TMP_CL.Lookup('CL_NBR', CL_NBR, 'NAME') + ' is out of sync with temp tables', IDH_InconsistentData);
          FieldByName('EIN').AsString := DM_TEMPORARY.TMP_CO_STATES.FieldByName('SUI_EIN').AsString;
          FieldByName('Frequency').AsString := ConvertCode(TCO_STATES, 'SUI_TAX_DEPOSIT_FREQUENCY', DM_TEMPORARY.TMP_CO_STATES['SUI_TAX_DEPOSIT_FREQUENCY']);
        end
        else
        if TaxTable = 'L' then
        begin
          Assert(DM_SYSTEM_LOCAL.SY_LOCALS.FindKey([Nbr]));
          if not DM_TEMPORARY.TMP_CO_LOCAL_TAX.Locate('CL_NBR;CO_NBR;SY_LOCALS_NBR', VarArrayOf([CL_NBR, CO_NBR, Nbr]), []) then
            raise EInconsistentData.CreateHelp('Company ' + DM_TEMPORARY.TMP_CL.Lookup('CL_NBR', CL_NBR, 'NAME') + ' is out of sync with temp tables', IDH_InconsistentData);
          FieldByName('EIN').AsString := DM_TEMPORARY.TMP_CO_LOCAL_TAX['LOCAL_EIN_NUMBER'];
          Assert(DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.FindKey([DM_TEMPORARY.TMP_CO_LOCAL_TAX['SY_LOCAL_DEPOSIT_FREQ_NBR']]));
          FieldByName('Frequency').AsString := DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ['DESCRIPTION'];
        end
        else
          Assert(False);

        FieldByName('Method').AsString := ReturnDescription(TaxPaymentMethod_ComboChoices, DepMethod);
        //FieldByName('Frequency').AsString := '-NA-';
        Post;
      end;
    Result := TEMP_DETAIL.FieldByName('DETAIL_NBR').AsInteger;
  end;

begin
  TEMP_PR_LIABILITY.DisableControls;
  try
    TEMP_CO_NBR_GEN := 1;
    TEMP_DETAIL_NBR_GEN := 1;
    TAX_TYPE_NBR_GEN := 1;
    TEMP_PR_LIABILITY_GEN := 1;

    s := GetReadOnlyClintCompanyFilter(true,'cl.','c.');

    cdTaxTo.IndexName := 'NBR';

    // State liab
    StateList :='';
    cdTaxTo.Filter := 'TaxType = ''S''';
    cdTaxTo.Filtered :=  True;
    cdTaxTo.First;
    while not cdTaxTo.EOF do
    begin
      if StateList <> '' then
          StateList := StateList + ', ';
      StateList := StateList + '''' + cdTaxTo.FieldByName('STATE').AsString + '''';
      cdTaxTo.Next;
    end;

    if not (StateList = '') then
    begin
      with ctx_DataAccess.TEMP_CUSTOM_VIEW do
      begin
        Close;
        with TExecDSWrapper.Create('TmpLiabilitiesWithStates') do
        begin
          if cbFromDateRange.Checked then
          begin
            SetMacro('StartDate', 'DUE_DATE >= :StartDate and');
            SetParam('StartDate', StrToDateTime(FromDateText));
          end
          else
            SetMacro('StartDate', '');
          SetParam('EndDate', StrToDateTime(ToDateText));
          SetMacro('StateList', StateList);
          SetMacro('cond', s);
          SetParam('Status1', TAX_DEPOSIT_STATUS_PENDING);
          SetParam('Status2', TAX_DEPOSIT_STATUS_IMPOUNDED);
          SetParam('Status3', TAX_DEPOSIT_STATUS_NSF);
          SetParam('Status4', TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        while not EOF do
        begin
          if TaxServiceSelected(FieldByName('TAX_SERVICE').AsString)
            and MethodSelected(FieldByName('DEP_METHOD').AsString) then
          begin
            DepMethod := FieldByName('DEP_METHOD').AsString[1];
            CL_NBR := FieldByName('CL_NBR').AsInteger;
            Nbr := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', FieldValues['STATE'], 'SY_STATES_NBR');
            DetailNbr := CreateDetail_NBR(CL_NBR, FieldByName('CO_NBR').AsInteger, 'S', Nbr, FieldByName('Tax_Type').AsString, Tax_Service, 0, -1);
            if not (DetailNbr = -1) then
            begin
              TEMP_PR_LIABILITY.Insert;
              TEMP_PR_LIABILITY.FieldByName('TEMP_PR_LIABILITY_NBR').AsInteger := TEMP_PR_LIABILITY_GEN;
              TEMP_PR_LIABILITY_GEN := TEMP_PR_LIABILITY_GEN + 1;
              TEMP_PR_LIABILITY.FieldByName('DETAIL_NBR').AsInteger := DetailNbr;
              TEMP_PR_LIABILITY.FieldByName('CL_NBR').AsInteger := CL_NBR;
              TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'N';
              if FieldByName('CO_TAX_DEPOSITS_NBR').IsNull and not FieldByName('CHECK_DATE').IsNull then
              begin
                if CompanyRequiresTaxesBeImpounded(Tax_Service) then
                begin
                  if (FieldByName('Status').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED)
                    or (FieldByName('Status').AsString = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED) then
                    TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'Y';
                end
                else
                    TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'Y';
              end;
              TEMP_PR_LIABILITY.FieldByName('CO_TAX_DEPOSITS_NBR').Value := FieldByName('CO_TAX_DEPOSITS_NBR').Value;
              TEMP_PR_LIABILITY.FieldByName('CO_NBR').AsInteger := FieldByName('CO_NBR').AsInteger;
              TEMP_PR_LIABILITY.FieldByName('CO_STATE_TAX_LIABILITIES_NBR').AsInteger :=
                FieldByName('CO_STATE_TAX_LIABILITIES_NBR').AsInteger;
              TEMP_PR_LIABILITY.FieldByName('CHECK_DATE').AsString := FieldByName('CHECK_DATE').AsString;
              TEMP_PR_LIABILITY.FieldByName('DUE_DATE').AsString := FieldByName('DUE_DATE').AsString;
              TEMP_PR_LIABILITY.FieldByName('Tax_Type').AsString := ConvertCode(TCO_STATE_TAX_LIABILITIES, 'TAX_TYPE',
                FieldByName('TAX_TYPE').AsString);
              TEMP_PR_LIABILITY.FieldByName('Status').AsString := ConvertCode(TCO_STATE_TAX_LIABILITIES, 'STATUS',
                FieldByName('Status').AsString);
              TEMP_PR_LIABILITY.FieldByName('NAME').AsString := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', FieldValues['STATE'], 'NAME');
              TEMP_PR_LIABILITY.FieldByName('InternalStatus').AsString := FieldByName('Status').AsString;
              TEMP_PR_LIABILITY.FieldByName('Pay').AsString := TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString;
              TEMP_PR_LIABILITY.FieldByName('Paid').AsString := 'N';
              TEMP_PR_LIABILITY.FieldByName('RUN_NUMBER').AsString := FieldByName('RUN_NUMBER').AsString;
              TEMP_PR_LIABILITY.FieldByName('Amount').AsCurrency := FieldByName('AMOUNT').AsCurrency;
              TEMP_PR_LIABILITY.FieldByName('STATE').AsString := FieldValues['STATE'];
              TEMP_PR_LIABILITY.FieldByName('LIABILITY_TYPE').AsString := FieldByName('TAX_TYPE').AsString;
              TEMP_PR_LIABILITY.Post;
            end;
          end;
          Next;
        end;
      end;
    end;
    // SUI

    StateList :='';
    cdTaxTo.Filter := 'TaxType = ''U''';
    cdTaxTo.Filtered :=  True;
    cdTaxTo.First;
    while not cdTaxTo.EOF do
    begin
      if StateList <> '' then
          StateList := StateList + ', ';
      StateList := StateList + cdTaxTo.FieldByName('Nbr').AsString;
      cdTaxTo.Next;
    end;

    if StateList <> '' then
    begin
      with ctx_DataAccess.TEMP_CUSTOM_VIEW do
      begin
        Close;
        with TExecDSWrapper.Create('TmpLiabilitiesWithSUI') do
        begin
          if cbFromDateRange.Checked then
          begin
            SetMacro('StartDate', 'DUE_DATE >= :StartDate and');
            SetParam('StartDate', StrToDateTime(FromDateText));
          end
          else
            SetMacro('StartDate', '');
          SetParam('EndDate', StrToDateTime(ToDateText));
          SetMacro('SUIList', StateList);
          SetMacro('cond', s);
          SetParam('Status1', TAX_DEPOSIT_STATUS_PENDING);
          SetParam('Status2', TAX_DEPOSIT_STATUS_IMPOUNDED);
          SetParam('Status3', TAX_DEPOSIT_STATUS_NSF);
          SetParam('Status4', TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED);
          DataRequest(AsVariant);
        end;
        Open;
        First;
        while not EOF do
        begin
          CL_NBR := FieldByName('CL_NBR').AsInteger;
          Assert(DM_SYSTEM_STATE.SY_SUI.FindKey([FieldByName('NBR').AsInteger]));
          Assert(DM_SYSTEM_STATE.SY_STATES.FindKey([DM_SYSTEM_STATE.SY_SUI['SY_STATES_NBR']]));
          Assert(DM_TEMPORARY.TMP_CO_STATES.Locate('CL_NBR;CO_NBR;STATE', VarArrayOf([CL_NBR, FieldByName('CO_NBR').AsInteger,
            DM_SYSTEM_STATE.SY_STATES['STATE']]), []), 'Rebuild temp tables for client '+ IntToStr(CL_NBR));
          DepMethod := string(DM_TEMPORARY.TMP_CO_STATES['SUI_TAX_DEPOSIT_METHOD'])[1];
          if TaxServiceSelected(FieldByName('TAX_SERVICE').AsString)
            and MethodSelected(DepMethod) then
          begin
            DetailNbr := CreateDetail_NBR(CL_NBR, FieldByName('CO_NBR').AsInteger, 'U', FieldByName('NBR').AsInteger, '', Tax_Service ,0, -1);
            if not (DetailNbr = -1) then
            begin
              TEMP_PR_LIABILITY.Insert;
              TEMP_PR_LIABILITY.FieldByName('TEMP_PR_LIABILITY_NBR').AsInteger := TEMP_PR_LIABILITY_GEN;
              TEMP_PR_LIABILITY_GEN := TEMP_PR_LIABILITY_GEN + 1;
              TEMP_PR_LIABILITY.FieldByName('DETAIL_NBR').AsInteger := DetailNbr;
              TEMP_PR_LIABILITY.FieldByName('CL_NBR').AsInteger := CL_NBR;
              TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'N';
              if FieldByName('CO_TAX_DEPOSITS_NBR').IsNull and not FieldByName('CHECK_DATE').IsNull then
              begin
                if CompanyRequiresTaxesBeImpounded(Tax_Service) then
                begin
                  if (FieldByName('Status').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED)
                    or (FieldByName('Status').AsString = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED) then
                    TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'Y';
                end
                else
                    TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'Y';
              end;
              TEMP_PR_LIABILITY.FieldByName('CO_TAX_DEPOSITS_NBR').Value := FieldByName('CO_TAX_DEPOSITS_NBR').Value;
              TEMP_PR_LIABILITY.FieldByName('CO_NBR').AsInteger := FieldByName('CO_NBR').AsInteger;
              TEMP_PR_LIABILITY.FieldByName('CO_SUI_LIABILITIES_NBR').AsInteger := FieldByName('CO_SUI_LIABILITIES_NBR').AsInteger;
              TEMP_PR_LIABILITY.FieldByName('CHECK_DATE').AsString := FieldByName('CHECK_DATE').AsString;
              TEMP_PR_LIABILITY.FieldByName('DUE_DATE').AsString := FieldByName('DUE_DATE').AsString;
              TEMP_PR_LIABILITY.FieldByName('Tax_Type').AsString := 'SUI';
              TEMP_PR_LIABILITY.FieldByName('Status').AsString := ConvertCode(TCO_STATE_TAX_LIABILITIES, 'STATUS',
                FieldByName('Status').AsString);

              TEMP_PR_LIABILITY.FieldByName('NAME').AsString := DM_SYSTEM_STATE.SY_SUI.fieldByName('SUI_TAX_NAME').AsString;
              TEMP_PR_LIABILITY.FieldByName('InternalStatus').AsString := FieldByName('Status').AsString;
              TEMP_PR_LIABILITY.FieldByName('Pay').AsString := TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString;
              TEMP_PR_LIABILITY.FieldByName('Paid').AsString := 'N';
              TEMP_PR_LIABILITY.FieldByName('RUN_NUMBER').AsString := FieldByName('RUN_NUMBER').AsString;
              TEMP_PR_LIABILITY.FieldByName('Amount').AsCurrency := FieldByName('AMOUNT').AsCurrency;
              TEMP_PR_LIABILITY.Post;
            end;
          end;
          Next;
        end;
      end;
    end;

    // local
    cdLiabLocalsRegular := TevClientDataSet.Create(nil);
    cdLiabLocalsFiller := TevClientDataSet.Create(nil);
    try
        cdLiabLocalsRegular.FieldDefs.Add('TAX_SERVICE', ftString, 1, True);
        cdLiabLocalsRegular.FieldDefs.Add('DEP_METHOD', ftString, 1, True);
        cdLiabLocalsRegular.FieldDefs.Add('CO_LOCAL_TAX_LIABILITIES_NBR', ftInteger, 0, True);
        cdLiabLocalsRegular.FieldDefs.Add('co_tax_deposits_nbr', ftInteger, 0, false);
        cdLiabLocalsRegular.FieldDefs.Add('NBR', ftInteger, 0, True);
        cdLiabLocalsRegular.FieldDefs.Add('CL_NBR', ftInteger, 0, True);
        cdLiabLocalsRegular.FieldDefs.Add('CO_NBR', ftInteger, 0, True);
        cdLiabLocalsRegular.FieldDefs.Add('AMOUNT', ftCurrency, 0, True);
        cdLiabLocalsRegular.FieldDefs.Add('CHECK_DATE', ftDateTime, 0, True);
        cdLiabLocalsRegular.FieldDefs.Add('RUN_NUMBER', ftInteger, 0, false);
        cdLiabLocalsRegular.FieldDefs.Add('DUE_DATE', ftDateTime, 0, True);
        cdLiabLocalsRegular.FieldDefs.Add('Status', ftString, 1, True);

        cdLiabLocalsRegular.FieldDefs.Add('TCD_DEPOSIT_FREQUENCY_NBR', ftInteger, 0, false);
        cdLiabLocalsRegular.FieldDefs.Add('TCD_Agency_Nbr', ftInteger, 0, false);
        cdLiabLocalsRegular.FieldDefs.Add('TCD_PAYMENT_METHOD', ftString, 1, false);
        cdLiabLocalsRegular.FieldDefs.Add('State', ftString, 2, True);
        cdLiabLocalsRegular.FieldDefs.Add('paymentSY_LOCALS_NBR', ftInteger, 0, false);

        cdLiabLocalsRegular.CreateFields;

        cdLiabLocalsRegular.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').FieldKind := fkInternalCalc;
        cdLiabLocalsRegular.FieldByName('TCD_Agency_Nbr').FieldKind := fkInternalCalc;
        cdLiabLocalsRegular.FieldByName('TCD_PAYMENT_METHOD').FieldKind := fkInternalCalc;
        cdLiabLocalsRegular.FieldByName('State').FieldKind := fkInternalCalc;

        cdLiabLocalsRegular.OnCalcFields := cdLiabLocalsRegularCalculate;
        cdLiabLocalsRegular.CreateDataSet;
        cdLiabLocalsRegular.LogChanges := False;

        cdLiabLocalsFiller.FieldDefs.Assign(cdLiabLocalsRegular.FieldDefs);
        cdLiabLocalsFiller.OnCalcFields := cdLiabLocalsFillerCalculate;        
        cdLiabLocalsFiller.CreateDataSet;
        cdLiabLocalsFiller.LogChanges := False;

        StateList := 't.SY_LOCALS_NBR in (';
        tmpCondition := 'IIF(Trim(STRCOPY(CAST(t.Filler AS VARCHAR(512)), 21, 10)) = '''', -1,CAST(Trim(STRCOPY(CAST(t.Filler AS VARCHAR(512)), 21, 10)) as Integer)) in ( ';
        ConditionFiller := tmpCondition;

        j := 1;
        cdTaxTo.Filter := 'TaxType = ''L''';
        cdTaxTo.Filtered :=  True;
        cdTaxTo.First;
        while not cdTaxTo.EOF do
        begin
          if (j > 1) and (j mod 1490 = 0) then
          begin
             StateList := StateList+ '0) or t.SY_LOCALS_NBR in (';
             ConditionFiller := ConditionFiller + '0) or ' + tmpCondition;
          end;
          Inc(j);
          StateList := StateList + cdTaxTo.FieldByName('Nbr').AsString + ',';
          ConditionFiller := ConditionFiller + cdTaxTo.FieldByName('Nbr').AsString + ',';
          cdTaxTo.Next;
        end;
        StateList := '(' + StateList+ '0)) ';
        ConditionFiller := '(' + ConditionFiller + '0) )';
        if j > 1 then
        begin
                cdtmpTCDAgencyList := tmp_TCDAgencyList;
                cdsyTCDAgencyFreqList := sy_TCDAgencyFreqList;
                cdsyLocalsState := sy_TCDLocalsState;

                cdLiabLocalsRegular.close;
                with TExecDSWrapper.Create('TmpLiabilitiesWithLocals') do
                begin
                  if cbFromDateRange.Checked then
                  begin
                    SetMacro('StartDate', 'DUE_DATE >= :StartDate and');
                    SetParam('StartDate', StrToDateTime(FromDateText));
                  end
                  else
                    SetMacro('StartDate', '');
                  SetParam('EndDate', StrToDateTime(ToDateText));
                  SetMacro('Condition', StateList);
                  SetMacro('cond', s);
                  SetParam('Status1', TAX_DEPOSIT_STATUS_PENDING);
                  SetParam('Status2', TAX_DEPOSIT_STATUS_IMPOUNDED);
                  SetParam('Status3', TAX_DEPOSIT_STATUS_NSF);
                  SetParam('Status4', TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED);

                  cdLiabLocalsRegular.ProviderName := ctx_DataAccess.TEMP_CUSTOM_VIEW.ProviderName;
                  cdLiabLocalsRegular.DataRequest(AsVariant);
                end;
                cdLiabLocalsRegular.Open;

                tmpCondition :=
                   'select c.TAX_SERVICE, s.PAYMENT_METHOD DEP_METHOD, ' +
                    't.CO_LOCAL_TAX_LIABILITIES_NBR, t.co_tax_deposits_nbr, t.SY_LOCALS_NBR NBR, t.CL_NBR, ' +
                    't.CO_NBR, t.AMOUNT, t.CHECK_DATE, p.RUN_NUMBER, t.DUE_DATE, t.Status, ' +
                    'CAST(Trim(STRCOPY(CAST(t.Filler AS VARCHAR(512)), 21, 10)) as Integer) paymentSY_LOCALS_NBR ' +
                    'from TMP_CO_LOCAL_TAX_LIABILITIES t ' +
                    'join TMP_CO c on ' +
                      'c.CL_NBR = t.CL_NBR and ' +
                      'c.CO_NBR = t.CO_NBR ' +
                      'and c.TERMINATION_CODE <> '''+ TERMINATION_SAMPLE+ ''' '+
                      'and c.CREDIT_HOLD <> '''+ CREDIT_HOLD_LEVEL_MAINT + ''' ' +
                      'join TMP_CL cl on ' +
                      'c.CL_NBR = cl.CL_NBR ' +
                      ' #cond ' +
                      'join TMP_CO_LOCAL_TAX s on ' +
                      's.SY_LOCALS_NBR = t.SY_LOCALS_NBR and ' +
                      's.CL_NBR = t.CL_NBR and ' +
                      's.CO_NBR = t.CO_NBR ' +
                    'left outer join TMP_PR p on '+
                      'p.PR_NBR = t.PR_NBR and ' +
                      'p.CL_NBR = t.CL_NBR ' +
                    'where DUE_DATE<=:EndDate AND AMOUNT <> 0 AND ' +
                    '#StartDate ' +
                    '#ConditionFiller and ' +
                    't.CO_TAX_DEPOSITS_NBR is null and t.THIRD_PARTY = ''N'' and ' +
                    '(t.Status = :Status1 or ' +
                    't.status = :Status2 or ' +
                    't.status = :Status4 or ' +
                    't.status = :Status3) ' +
                    'and Trim(STRCOPY(CAST(t.Filler AS VARCHAR(512)), 21, 10)) <> '''' ';

                cdLiabLocalsFiller.close;
                with TExecDSWrapper.Create(tmpCondition) do
                begin
                  if cbFromDateRange.Checked then
                  begin
                    SetMacro('StartDate', 'DUE_DATE >= :StartDate and');
                    SetParam('StartDate', StrToDateTime(FromDateText));
                  end
                  else
                    SetMacro('StartDate', '');
                  SetParam('EndDate', StrToDateTime(ToDateText));
                  SetMacro('ConditionFiller', ConditionFiller);
                  SetMacro('cond', s);
                  SetParam('Status1', TAX_DEPOSIT_STATUS_PENDING);
                  SetParam('Status2', TAX_DEPOSIT_STATUS_IMPOUNDED);
                  SetParam('Status3', TAX_DEPOSIT_STATUS_NSF);
                  SetParam('Status4', TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED);

                  cdLiabLocalsFiller.ProviderName := ctx_DataAccess.TEMP_CUSTOM_VIEW.ProviderName;
                  cdLiabLocalsFiller.DataRequest(AsVariant);
                end;
                cdLiabLocalsFiller.Open;

                cdLiabLocalsFiller.First;
                while not cdLiabLocalsFiller.Eof do
                begin
                  cdLiabLocalsRegular.Append;
                  for j:=0 to cdLiabLocalsRegular.fieldcount-1 do
                      cdLiabLocalsRegular.Fields[j].Assign(cdLiabLocalsFiller.FindField(cdLiabLocalsRegular.Fields[j].FieldName));
                  cdLiabLocalsRegular.Post;
                  cdLiabLocalsFiller.next
                end;

                cdLiabLocalsRegular.First;
                while not cdLiabLocalsRegular.EOF do
                begin
                  if TaxServiceSelected(cdLiabLocalsRegular.FieldByName('TAX_SERVICE').AsString)
                    and MethodSelected(cdLiabLocalsRegular.FieldByName('DEP_METHOD').AsString) then
                  begin
                    DepMethod := cdLiabLocalsRegular.FieldByName('DEP_METHOD').AsString[1];
                    CL_NBR := cdLiabLocalsRegular.FieldByName('CL_NBR').AsInteger;
                    DetailNbr := CreateDetail_NBR(CL_NBR, cdLiabLocalsRegular.FieldByName('CO_NBR').AsInteger, 'L',
                                                   cdLiabLocalsRegular.FieldByName('NBR').AsInteger, '', Tax_Service,
                                                   cdLiabLocalsRegular.FieldByName('TCD_Agency_Nbr').AsInteger,
                                                   cdLiabLocalsRegular.FieldByName('paymentSY_LOCALS_NBR').AsInteger);
                    if not (DetailNbr = -1) then
                    begin
                      Assert(DM_SYSTEM_STATE.SY_LOCALS.FindKey([cdLiabLocalsRegular.FieldByName('NBR').AsInteger]));
                      TEMP_PR_LIABILITY.Insert;
                      TEMP_PR_LIABILITY.FieldByName('TEMP_PR_LIABILITY_NBR').AsInteger := TEMP_PR_LIABILITY_GEN;
                      TEMP_PR_LIABILITY_GEN := TEMP_PR_LIABILITY_GEN + 1;
                      TEMP_PR_LIABILITY.FieldByName('DETAIL_NBR').AsInteger := DetailNbr;
                      TEMP_PR_LIABILITY.FieldByName('CL_NBR').AsInteger := CL_NBR;
                      TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'N';
                      if cdLiabLocalsRegular.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull and not cdLiabLocalsRegular.FieldByName('CHECK_DATE').IsNull then
                      begin
                        if CompanyRequiresTaxesBeImpounded(Tax_Service) then
                        begin
                          if (cdLiabLocalsRegular.FieldByName('Status').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED)
                            or (cdLiabLocalsRegular.FieldByName('Status').AsString = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED) then
                            TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'Y';
                        end
                        else
                          TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'Y';
                      end;
                      TEMP_PR_LIABILITY.FieldByName('CO_TAX_DEPOSITS_NBR').Value := cdLiabLocalsRegular.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
                      TEMP_PR_LIABILITY.FieldByName('CO_NBR').AsInteger := cdLiabLocalsRegular.FieldByName('CO_NBR').AsInteger;
                      TEMP_PR_LIABILITY.FieldByName('CO_LOCAL_TAX_LIABILITIES_NBR').AsInteger :=
                        cdLiabLocalsRegular.FieldByName('CO_LOCAL_TAX_LIABILITIES_NBR').AsInteger;
                      TEMP_PR_LIABILITY.FieldByName('CHECK_DATE').AsString := cdLiabLocalsRegular.FieldByName('CHECK_DATE').AsString;
                      TEMP_PR_LIABILITY.FieldByName('DUE_DATE').AsString := cdLiabLocalsRegular.FieldByName('DUE_DATE').AsString;
                      TEMP_PR_LIABILITY.FieldByName('Tax_Type').AsString := 'Local';
                      TEMP_PR_LIABILITY.FieldByName('Status').AsString := ConvertCode(TCO_STATE_TAX_LIABILITIES, 'STATUS',
                        cdLiabLocalsRegular.FieldByName('Status').AsString);

                      TEMP_PR_LIABILITY.FieldByName('NAME').AsString := DM_SYSTEM_STATE.SY_LOCALS.fieldByName('NAME').AsString;
                      TEMP_PR_LIABILITY.FieldByName('InternalStatus').AsString := cdLiabLocalsRegular.FieldByName('Status').AsString;
                      TEMP_PR_LIABILITY.FieldByName('Pay').AsString := TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString;
                      TEMP_PR_LIABILITY.FieldByName('Paid').AsString := 'N';
                      TEMP_PR_LIABILITY.FieldByName('RUN_NUMBER').AsString := cdLiabLocalsRegular.FieldByName('RUN_NUMBER').AsString;
                      TEMP_PR_LIABILITY.FieldByName('Amount').AsCurrency := cdLiabLocalsRegular.FieldByName('AMOUNT').AsCurrency;
                      TEMP_PR_LIABILITY.Post;
                    end;
                  end;
                  cdLiabLocalsRegular.Next;
                end;
        end;
    finally
      cdLiabLocalsRegular.Free;
      cdLiabLocalsFiller.Free;
    end;

    // federal
    cdTaxTo.Filter := 'TaxType = ''F''';
    cdTaxTo.Filtered :=  True;
    with ctx_DataAccess.TEMP_CUSTOM_VIEW do
    begin
      Close;
      with TExecDSWrapper.Create('TmpLiabilities') do
      begin
        if cbFromDateRange.Checked then
        begin
          SetMacro('StartDate', 'DUE_DATE >= :StartDate and');
          SetParam('StartDate', StrToDateTime(FromDateText));
        end
        else
          SetMacro('StartDate', '');
        SetParam('EndDate', StrToDateTime(ToDateText));
        SetMacro('StateList', StateList);
        SetMacro('cond', s);
        SetParam('Status1', TAX_DEPOSIT_STATUS_PENDING);
        SetParam('Status2', TAX_DEPOSIT_STATUS_IMPOUNDED);
        SetParam('Status3', TAX_DEPOSIT_STATUS_NSF);
        SetParam('Status4', TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED);
        DataRequest(AsVariant);
      end;
      Open;
      First;
      while not EOF do
      begin
        if TaxServiceSelected(FieldByName('TAX_SERVICE').AsString)
          and MethodSelected(FieldByName('DEP_METHOD').AsString) then
        begin
          DepMethod := FieldByName('DEP_METHOD').AsString[1];
          CL_NBR := FieldByName('CL_NBR').AsInteger;
          if FieldByName('SY_FED_TAX_PAYMENT_AGENCY_NBR').IsNull then
            raise EInconsistentData.CreateHelp('Company ' + DM_TEMPORARY.TMP_CO.Lookup('CL_NBR;CO_NBR', VarArrayOf([CL_NBR, FieldValues['CO_NBR']]),
              'NAME') + ' doesn''t have federal agency attached', IDH_InconsistentData);
          DetailNbr := CreateDetail_NBR(CL_NBR, FieldByName('CO_NBR').AsInteger, 'F',
            FieldByName('SY_FED_TAX_PAYMENT_AGENCY_NBR').AsInteger, FieldByName('TAX_TYPE').AsString, Tax_Service, 0, -1);
          if not (DetailNbr = -1) then
          begin
            TEMP_PR_LIABILITY.Insert;
            TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'N';
            if FieldByName('CO_TAX_DEPOSITS_NBR').IsNull and not FieldByName('CHECK_DATE').IsNull then
            begin
              if CompanyRequiresTaxesBeImpounded(Tax_Service) then
              begin
                if (FieldByName('Status').AsString = TAX_DEPOSIT_STATUS_IMPOUNDED)
                  or (FieldByName('Status').AsString = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED) then
                  TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'Y';
              end
              else
                  TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString := 'Y';
            end;
            TEMP_PR_LIABILITY.FieldByName('CO_TAX_DEPOSITS_NBR').Value := FieldByName('CO_TAX_DEPOSITS_NBR').Value;
            TEMP_PR_LIABILITY.FieldByName('TEMP_PR_LIABILITY_NBR').AsInteger := TEMP_PR_LIABILITY_GEN;
            TEMP_PR_LIABILITY_GEN := TEMP_PR_LIABILITY_GEN + 1;
            TEMP_PR_LIABILITY.FieldByName('DETAIL_NBR').AsInteger := DetailNbr;
            TEMP_PR_LIABILITY.FieldByName('CL_NBR').AsInteger := CL_NBR;
            TEMP_PR_LIABILITY.FieldByName('CO_NBR').AsInteger := FieldByName('CO_NBR').AsInteger;
            TEMP_PR_LIABILITY.FieldByName('CO_FED_TAX_LIABILITIES_NBR').AsInteger := FieldByName('CO_FED_TAX_LIABILITIES_NBR').AsInteger;
            TEMP_PR_LIABILITY.FieldByName('CHECK_DATE').AsString := FieldByName('CHECK_DATE').AsString;
            TEMP_PR_LIABILITY.FieldByName('DUE_DATE').AsString := FieldByName('DUE_DATE').AsString;
            TEMP_PR_LIABILITY.FieldByName('Tax_Type').AsString := ConvertCode(TCO_FED_TAX_LIABILITIES, 'TAX_TYPE',
              FieldByName('TAX_TYPE').AsString);
            TEMP_PR_LIABILITY.FieldByName('Status').AsString := ConvertCode(TCO_FED_TAX_LIABILITIES, 'STATUS',
              FieldByName('Status').AsString);
            TEMP_PR_LIABILITY.FieldByName('NAME').AsString := 'Federal';              
            TEMP_PR_LIABILITY.FieldByName('InternalStatus').AsString := FieldByName('Status').AsString;
            TEMP_PR_LIABILITY.FieldByName('Pay').AsString := TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString;
            TEMP_PR_LIABILITY.FieldByName('Paid').AsString := 'N';
            TEMP_PR_LIABILITY.FieldByName('RUN_NUMBER').AsString := FieldByName('RUN_NUMBER').AsString;
            TEMP_PR_LIABILITY.FieldByName('Amount').AsCurrency := FieldByName('AMOUNT').AsCurrency;
            TEMP_PR_LIABILITY.Post;
          end;
        end;
        Next;
      end;
    end;
  finally
    TEMP_PR_LIABILITY.EnableControls;
    cdTaxTo.IndexName := 'NAME';
    cdTaxTo.Filter := '';
    cdTaxTo.Filtered := false;
  end;
end;


procedure TProcessTaxPayments.cdLiabLocalsCalculate(DataSet: TDataSet; const NBR : integer);
begin
    Assert(DM_SYSTEM_STATE.SY_LOCALS.FindKey([NBR]));
    if (DM_SYSTEM_STATE.SY_LOCALS.FieldByName('LOCAL_TYPE').AsString[1] in [LOCAL_TYPE_SCHOOL, LOCAL_TYPE_INC_RESIDENTIAL,LOCAL_TYPE_INC_NON_RESIDENTIAL]) and
       (DM_SYSTEM_STATE.SY_LOCALS.FieldByName('COMBINE_FOR_TAX_PAYMENTS').AsString = GROUP_BOX_YES) then
    begin
      if cdsyLocalsState.Locate('SY_LOCALS_NBR', NBR , []) then
         Dataset.FieldByName('State').AsString := cdsyLocalsState.fieldByName('STATE').AsString;

      if not DataSet.FieldByName('state').IsNull then
        if cdtmpTCDAgencyList.Locate('CL_NBR;CO_NBR;STATE',
           VarArrayOf([DataSet.FieldByName('CL_NBR').Value, DataSet.FieldByName('CO_NBR').Value,DataSet.FieldByName('state').Value]), []) then
        begin
           Dataset.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').Value := cdtmpTCDAgencyList.fieldByName('TCD_DEPOSIT_FREQUENCY_NBR').Value;
           Dataset.FieldByName('TCD_PAYMENT_METHOD').Value := cdtmpTCDAgencyList.fieldByName('TCD_PAYMENT_METHOD').Value;
        end;

      if not DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').IsNull then
        if cdsyTCDAgencyFreqList.Locate('SY_AGENCY_DEPOSIT_FREQ_NBR', DataSet.FieldByName('TCD_DEPOSIT_FREQUENCY_NBR').Value , []) then
           Dataset.FieldByName('TCD_Agency_Nbr').Value := cdsyTCDAgencyFreqList.fieldByName('SY_GLOBAL_AGENCY_NBR').Value;
    end;
end;

procedure TProcessTaxPayments.cdLiabLocalsRegularCalculate(DataSet: TDataSet);
begin
  if (not DataSet.FieldByName('NBR').IsNull) and
      (not DataSet.FieldByName('CL_NBR').IsNull) and
       (not DataSet.FieldByName('CO_NBR').IsNull) then
    cdLiabLocalsCalculate(DataSet, DataSet.FieldByName('NBR').AsInteger);
end;

procedure TProcessTaxPayments.cdLiabLocalsFillerCalculate(DataSet: TDataSet);
begin
  if (not DataSet.FieldByName('paymentSY_LOCALS_NBR').IsNull) and
      (not DataSet.FieldByName('CL_NBR').IsNull) and
       (not DataSet.FieldByName('CO_NBR').IsNull) then
    cdLiabLocalsCalculate(DataSet, DataSet.FieldByName('paymentSY_LOCALS_NBR').AsInteger);
end;


function TProcessTaxPayments.tmp_TCDAgencyList: IevDataSet;
var
  Q: IevQuery;
  s : String;
begin
   s :='select a.CL_NBR, a.CO_NBR, a.STATE, a.TCD_DEPOSIT_FREQUENCY_NBR, a.TCD_PAYMENT_METHOD ' +
          ' from TMP_CO_STATES a where a.TCD_DEPOSIT_FREQUENCY_NBR is not null ' +
             ' order by a.CL_NBR, a.CO_NBR, a.STATE ';
   Q := TevQuery.Create(s);
   Q.Execute;
   Result := Q.Result;
end;

function TProcessTaxPayments.sy_TCDAgencyFreqList: IevDataSet;
var
  Q: IevQuery;
  s : String;
begin
   s :=' select a.SY_GLOBAL_AGENCY_NBR, b.SY_AGENCY_DEPOSIT_FREQ_NBR, b.FREQUENCY_TYPE ' +
             ' from SY_GLOBAL_AGENCY  a, SY_AGENCY_DEPOSIT_FREQ b ' +
               'where {AsOfNow<a>} and {AsOfNow<b>}  and ' +
                 'a.SY_GLOBAL_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and a.AGENCY_TYPE = ''Y'' ';
   Q := TevQuery.Create(s);
   Q.Execute;
   Result := Q.Result;
end;

function TProcessTaxPayments.sy_TCDLocalsState: IevDataSet;
var
  Q: IevQuery;
  s : String;
begin
   s :='select a.SY_LOCALS_NBR, a.SY_STATES_NBR, b.STATE from SY_LOCALS a, SY_STATES b ' +
        'where {AsOfNow<a>} and {AsOfNow<b>} and a.SY_STATES_NBR = b.SY_STATES_NBR ' ;
   Q := TevQuery.Create(s);
   Q.Execute;
   Result := Q.Result;
end;


procedure TProcessTaxPayments.PopulateDetailData;
begin
  with TEMP_DETAIL do
  begin
    IndexName := 'DETAIL_NBRIndex';
    First;
    while not EOF do
    begin
      if not TEMP_CO.Locate('TEMP_CO_NBR', FieldByName('TEMP_CO_NBR').AsInteger, []) then
        raise EInconsistentData.CreateHelp('TEMP_CO_NBR not found', IDH_InconsistentData);
      TEMP_DETAIL.Edit;
      FieldByName('Name').AsString := TEMP_CO.FieldByName('Name').AsString;
      FieldByName('Number').AsString := TEMP_CO.FieldByName('Number').AsString;
      FieldByName('Client_Number').AsString := TEMP_CO.FieldByName('Client_Number').AsString;
      if not TEMP_TAX_TYPE.Locate('TAX_TYPE_NBR', FieldByName('TAX_TYPE_NBR').AsInteger, []) then
        raise EInconsistentData.CreateHelp('TAX_TYPE_NBR not found', IDH_InconsistentData);
      FieldByName('Tax_Agency').AsString := TEMP_TAX_TYPE.FieldByName('Tax_Agency').AsString;
      Post;
      Next;
    end;
    First;
  end;
end;

procedure TProcessTaxPayments.RecalculateFlags;
var
  NewAmount: Currency;
  NewAmountP, NewAmountI: Currency;
  TotalCount, TotalPaid: integer;
  PR_OldFiltered: boolean;
  PRIndexName: string;
  DetailFiltered: boolean;
  DetailIndexName: string;
  HasNSF: string;

  procedure UpdateMainTable(aDataSet: TevClientDataSet; IdFieldName: string);
  var
    Pay_Status, Temp_Pay_Status: string;
    OldFiltered: boolean;
  begin
    with aDataSet do
    begin
      DisableControls;
      IndexName := '';
      OldFiltered := TEMP_DETAIL.Filtered;
      TEMP_DETAIL.Filtered := False;
      try
        First;

        FieldByName('AMOUNT').ReadOnly := False;
        FieldByName('AMOUNT_I').ReadOnly := False;
        FieldByName('AMOUNT_P').ReadOnly := False;
        FieldByName('HasNSF').ReadOnly := False;
        while not EOF do
        begin
          NewAmount := 0;
          NewAmountP := 0;
          NewAmountI := 0;
          TEMP_DETAIL.First;
          Pay_Status := '';
          HasNSF := 'N';
          while not TEMP_DETAIL.EOF do
          begin
            if TEMP_DETAIL.FieldByName(IdFieldName).AsInteger = FieldByName(IdFieldName).AsInteger then
            begin
              NewAmount := NewAmount + TEMP_DETAIL.FieldByName('Amount').AsCurrency;
              NewAmountP := NewAmountP + TEMP_DETAIL.FieldByName('Amount_P').AsCurrency;
              NewAmountI := NewAmountI + TEMP_DETAIL.FieldByName('Amount_I').AsCurrency;
              Temp_Pay_Status := TEMP_DETAIL.FieldByName('Pay').AsString;
              if Pay_Status = '' then
                Pay_Status := Temp_Pay_Status
              else
                if not (Pay_Status = Temp_Pay_Status) then
                Pay_Status := 'S';
              if TEMP_DETAIL.FieldByName('HasNSF').AsString = 'Y' then
                HasNSF := 'Y';
            end;
            TEMP_DETAIL.Next;
          end;

          Edit;
          FieldByName('AMOUNT').AsCurrency := NewAmount;
          FieldByName('AMOUNT_P').AsCurrency := NewAmountP;
          FieldByName('AMOUNT_I').AsCurrency := NewAmountI;
          FieldByName('Pay').AsString := Pay_Status;
          FieldByName('HasNSF').AsString := HasNSF;
          Post;
          Next;
        end;
        FieldByName('AMOUNT_I').ReadOnly := True;
        FieldByName('AMOUNT_P').ReadOnly := True;
        FieldByName('AMOUNT').ReadOnly := True;
        FieldByName('HasNSF').ReadOnly := True;
      finally
        TEMP_DETAIL.Filtered := OldFiltered;
        EnableControls;
      end;
    end;
  end;
var
  fAmount, fCanBePaid, fPay, fInternalStatus: TField;
begin
  try
    InRecalculateFlags := True;
    TEMP_DETAIL.DisableControls;
    TEMP_CO.DisableControls;
    TEMP_TAX_TYPE.DisableControls;
    TEMP_PR_LIABILITY.DisableControls;

    with TEMP_DETAIL do
    begin
      DetailFiltered := Filtered;
      Filtered := False;
      DetailIndexName := IndexName;
      IndexName := 'DETAIL_NBRIndex';

      TEMP_PR_LIABILITY.DisableControls;
      PR_OldFiltered := TEMP_PR_LIABILITY.Filtered;
      TEMP_PR_LIABILITY.Filtered := False;
      PRIndexName := TEMP_PR_LIABILITY.IndexName;
      TEMP_PR_LIABILITY.IndexName := 'PR_DETAIL_NBRIndex';
      FieldByName('AMOUNT').ReadOnly := False;
      FieldByName('AMOUNT_I').ReadOnly := False;
      FieldByName('AMOUNT_P').ReadOnly := False;
      FieldByName('HasNSF').ReadOnly := False;
      fPay := TEMP_PR_LIABILITY.FieldByName('Pay');
      fAmount := TEMP_PR_LIABILITY.FieldByName('Amount');
      fCanBePaid := TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID');
      fInternalStatus := TEMP_PR_LIABILITY.FieldByName('InternalStatus');
      First;
      while not EOF do
      begin
        NewAmount := 0;
        NewAmountP := 0;
        NewAmountI := 0;
        TEMP_PR_LIABILITY.SetRange([FieldByName('DETAIL_NBR').AsInteger], [FieldByName('DETAIL_NBR').AsInteger]);
        TEMP_PR_LIABILITY.First;
        TotalCount := 0;
        TotalPaid := 0;
        HasNSF := 'N';
        while not TEMP_PR_LIABILITY.EOF do
        begin
          if fPay.AsString = 'Y' then
          begin
            NewAmount := NewAmount + fAmount.AsCurrency;
            TotalPaid := TotalPaid + 1;
            TotalCount := TotalCount + 1;
          end
          else
          if fCanBePaid.AsString = 'Y' then
            TotalCount := TotalCount + 1;
          case fInternalStatus.AsString[1] of
          TAX_DEPOSIT_STATUS_NSF:
            HasNSF := 'Y';
          TAX_DEPOSIT_STATUS_IMPOUNDED, TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED:
            NewAmountI := NewAmountI + fAmount.AsCurrency;
          TAX_DEPOSIT_STATUS_PENDING:
            NewAmountP := NewAmountP + fAmount.AsCurrency;
          end;
          TEMP_PR_LIABILITY.Next;
        end;

        Edit;
        FieldByName('AMOUNT').AsCurrency := NewAmount;
        FieldByName('AMOUNT_P').AsCurrency := NewAmountP;
        FieldByName('AMOUNT_I').AsCurrency := NewAmountI;
        if TotalPaid = TotalCount then
          FieldByName('Pay').AsString := 'A'
        else
          if TotalPaid = 0 then
          FieldByName('Pay').AsString := 'N'
        else
          FieldByName('Pay').AsString := 'S';
        FieldByName('HasNSF').AsString := HasNSF;
        Post;
        Next;
      end;
      FieldByName('AMOUNT_I').ReadOnly := True;
      FieldByName('AMOUNT_P').ReadOnly := True;
      FieldByName('AMOUNT').ReadOnly := True;
      FieldByName('HasNSF').ReadOnly := True;
      UpdateMainTable(TEMP_CO, 'TEMP_CO_NBR');
      UpdateMainTable(TEMP_TAX_TYPE, 'TAX_TYPE_NBR');

      TEMP_PR_LIABILITY.IndexName := PRIndexName;
      TEMP_PR_LIABILITY.Filtered := PR_OldFiltered;
      TEMP_PR_LIABILITY.EnableControls;

      IndexName := DetailIndexName;
      Filtered := DetailFiltered;
      EnableControls;

      RecalcButton.Enabled := False;
      PrinTevButton.Enabled := True;
      PayButton.Enabled := True;
      ExcelButton.Enabled := True;
    end;
  finally
    InRecalculateFlags := False;
    TEMP_PR_LIABILITY.CancelRange;
    TEMP_PR_LIABILITY.EnableControls;
    TEMP_DETAIL.EnableControls;
    TEMP_CO.EnableControls;
    TEMP_TAX_TYPE.EnableControls;
  end;
  TEMP_PR_LIABILITY.First;
  TEMP_DETAIL.First;
  TEMP_CO.First;
  TEMP_TAX_TYPE.First;
end;

procedure TProcessTaxPayments.ForceRecalc;
begin
  RecalcButton.Enabled := True;
  PrinTevButton.Enabled := False;
  PayButton.Enabled := False;
  ExcelButton.Enabled := False;
end;

procedure TProcessTaxPayments.AfterLiabilityPost(DataSet: TDataSet);
begin
  ForceRecalc;
end;

procedure TProcessTaxPayments.AfterDetailPost(DataSet: TDataSet);
var
  NewPayStatus: string;
begin
  if InRecalculateFlags then
    exit;

  NewPayStatus := 'N';
  with TEMP_DETAIL do
  begin
    if FieldByName('Pay').AsString = 'A' then
    begin
      NewPayStatus := 'Y'
    end
    else
      if not (FieldByName('Pay').AsString = 'N') then
    begin
      exit;
    end;

    ChangePayStatusForDetail(FieldByName('DETAIL_NBR').AsInteger, NewPayStatus);
  end;
  ForceRecalc;
end;

procedure TProcessTaxPayments.ChangePayStatusForDetail(DETAIL_NBR: integer; NewPayStatus: string);
var
  PRBookMark: TBookmark;
  PR_OldFiltered: boolean;
  PR_OldIndexName: string;
begin
  with TEMP_PR_LIABILITY do
  begin
    AfterPost := nil;

    PR_OldFiltered := Filtered;
    Filtered := False;
    PR_OldIndexName := IndexName;
    IndexName := '';
    PRBookMark := GetBookmark;
    DisableControls;

    try
      First;
      while not EOF do
      begin
        if FieldByName('DETAIL_NBR').AsInteger = DETAIL_NBR then
        begin
            if not (FieldByName('Pay').AsString = NewPayStatus) then
            begin
              Edit;
              FieldByName('Pay').AsString := NewPayStatus;
              Post;
            end;
        end;
        Next;
      end;
    finally
      GotoBookmark(PRBookMark);
      FreeBookmark(PRBookMark);
      Filtered := PR_OldFiltered;
      IndexName := PR_OldIndexName;
      EnableControls;
      First;
      AfterPost := AfterLiabilityPost;
    end;
  end;
end;

procedure TProcessTaxPayments.AfterMasterPost(DataSet: TDataSet);
var
  NewPayStatus: string;
  DetailFiltered: boolean;
  DetailIndexName: string;
  DetailBookMark: TBookmark;
  IdFieldName: string;
begin
  if InRecalculateFlags then
    exit;

  NewPayStatus := 'N';
  with DataSet do
  begin
    if FieldByName('Pay').AsString = 'A' then
    begin
      NewPayStatus := 'Y'
    end
    else
      if not (FieldByName('Pay').AsString = 'N') then
    begin
      exit;
    end;
  end;

  IdFieldName := 'TAX_TYPE_NBR';
  if DataSet = TEMP_CO then
    IdFieldName := 'TEMP_CO_NBR';

  with TEMP_DETAIL do
  begin
    DisableControls;
    DetailFiltered := Filtered;
    DetailIndexName := IndexName;
    IndexName := '';
    Filtered := False;
    DetailBookMark := GetBookmark;
    try
      First;
      while not EOF do
      begin
        if FieldByName(IdFieldName).AsString = DataSet.FieldByName(IdFieldName).AsString then
        begin
          ChangePayStatusForDetail(TEMP_DETAIL.FieldByName('DETAIL_NBR').AsInteger, NewPayStatus);
        end;
        Next;
      end;
    finally
      GotoBookmark(DetailBookMark);
      FreeBookmark(DetailBookMark);
      IndexName := DetailIndexName;
      Filtered := DetailFiltered;
      EnableControls;
    end;
  end;
  ForceRecalc;
end;

procedure TProcessTaxPayments.wwdsGrid2DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if InReport then
    exit;
  if Field <> nil then
  begin
    {if (Field.FieldName = 'PAY') and (Field.DataSet.State = dsEdit) then
      Field.DataSet.Post;}
  end
  else
  begin
    if not (wwdsGrid2.DataSet = TEMP_DETAIL) then
      exit;
    if not assigned(TEMP_PR_LIABILITY) then
      exit;
    if not TEMP_PR_LIABILITY.Active then
      exit;
    if not (TEMP_DETAIL.State in [dsBrowse]) then
      exit;
    if TEMP_DETAIL.BOF and TEMP_DETAIL.EOF then
      TEMP_PR_LIABILITY.Filter := 'DETAIL_NBR= -1'
    else
      TEMP_PR_LIABILITY.Filter := 'DETAIL_NBR=' + TEMP_DETAIL.FieldByName('DETAIL_NBR').AsString;
    TEMP_PR_LIABILITY.Filtered := True;
  end;
end;

procedure TProcessTaxPayments.BitBtn7Click(Sender: TObject);
begin
  inherited;
  SelectTabSheet.Enabled := True;
  PageControl1.ActivePage := SelectTabSheet;
  DetailTabSheet.Enabled := False;
  DetailTabSheet.TabVisible := False;
  DetailRollBack.Enabled := False;
  DetailRollBack.TabVisible := False;
end;

procedure TProcessTaxPayments.btnGetTaxesDueClick(Sender: TObject);
var
  s: string;
begin
  inherited;

  if bgrdAgencyControl then
     Update_cdTaxTo;

  if cdTaxTo.RecordCount = 0 then
  begin
    EvMessage('You must select a taxing agency');
    exit;
  end;

  if ToDateText = '' then
  begin
    EvMessage('You must enter an end date');
    exit;
  end;

  Assert(DM_SYSTEM_STATE.SY_STATES.Locate('STATE', 'MO', []));
  if cdTaxTo.Locate('SY_GLOBAL_AGENCY_NBR',DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATE_TAX_PMT_AGENCY_NBR').AsVariant,[]) then
  begin
     if EvMessage('You have selected MO State tax. Would you like to check for pending credits?',
        mtWarning, [mbYes, mbNo]) = mrYes then
     begin
          s := DateToStr(GetBeginMonth(ToDate.DateTime)-1);
          if not EvDialog('Confirmation', 'MO State Credit Check Date: ', s) then
            AbortEx;
          AutoCreditMo(StrToDate(s));
     end;
  end;

  if not cbFromDateRange.Checked then
    DateLabel.Caption := 'All up to ' + ToDateText
  else
    DateLabel.Caption := FromDateText + ' - ' + ToDateText;

  wwdsGrid1.OnDataChange := nil;
  wwdsGrid2.OnDataChange := nil;

  TEMP_PR_LIABILITY.AfterPost := nil;
  TEMP_DETAIL.AfterPost := nil;
  TEMP_CO.AfterPost := nil;
  TEMP_TAX_TYPE.AfterPost := nil;

  TEMP_PR_LIABILITY.Filtered := False;
  TEMP_DETAIL.Filtered := False;
  TEMP_CO.Filtered := False;
  TEMP_TAX_TYPE.Filtered := False;

  MakeAllButPayReadOnly(TEMP_PR_LIABILITY, False);
  MakeAllButPayReadOnly(TEMP_CO, False);
  MakeAllButPayReadOnly(TEMP_TAX_TYPE, False);
  MakeAllButPayReadOnly(TEMP_DETAIL, False);

  TEMP_PR_LIABILITY.EmptyDataSet;
  TEMP_DETAIL.EmptyDataSet;
  TEMP_CO.UserFilter :='';
  TEMP_CO.UserFiltered := false;
  TEMP_TAX_TYPE.UserFilter :='';
  TEMP_TAX_TYPE.UserFiltered := false;
  TEMP_CO.EmptyDataSet;
  TEMP_TAX_TYPE.EmptyDataSet;
  TEMP_PR_LIABILITY.DisableControls;
  TEMP_DETAIL.DisableControls;
  TEMP_CO.DisableControls;
  TEMP_TAX_TYPE.DisableControls;
  cdTaxTo.DisableControls;
  try
    ctx_StartWait('Accessing data...');
    ReadData;
    ctx_UpdateWait('Calculating subtotals...');
    PopulateDetailData;
    RecalculateFlags;
  finally
    cdTaxTo.EnableControls;
    ctx_EndWait;
  end;
  MakeAllButPayReadOnly(TEMP_PR_LIABILITY, True);
  MakeAllButPayReadOnly(TEMP_CO, True);
  MakeAllButPayReadOnly(TEMP_TAX_TYPE, True);
  MakeAllButPayReadOnly(TEMP_DETAIL, True);

  TEMP_PR_LIABILITY.AfterPost := AfterLiabilityPost;
  TEMP_DETAIL.AfterPost := AfterDetailPost;
  TEMP_CO.AfterPost := AfterMasterPost;
  TEMP_TAX_TYPE.AfterPost := AfterMasterPost;

  TEMP_PR_LIABILITY.EnableControls;
  TEMP_DETAIL.EnableControls;
  TEMP_CO.EnableControls;
  TEMP_TAX_TYPE.EnableControls;

  wwdsGrid1.OnDataChange := wwdsGrid1DataChange;
  wwdsGrid2.OnDataChange := wwdsGrid2DataChange;
  wwdbGrid3.DataSource := nil;
  wwdbGrid3.DataSource := wwdsGrid3;

  SelectTabSheet.Enabled := False;
  DetailTabSheet.TabVisible := True;
  DetailTabSheet.Enabled := True;

  wwdbGrid1.Visible := True;
  wwdbGrid2.Visible := True;
  wwdbGrid3.Visible := True;

  wwDBGrid3.SetControlType('PAY', fctCheckBox, 'Y;N');
  wwDBGrid2.SetControlType('PAY', fctCustom, 'wwDBComboBox1');
  UpdateGridViewsColumns;

  PageControl1.ActivePage := DetailTabSheet;
end;

procedure TProcessTaxPayments.RecalcButtonClick(Sender: TObject);
begin
  inherited;
  ctx_StartWait;
  try
    RecalculateFlags;
  finally
    ctx_EndWait;
  end;
end;

procedure TProcessTaxPayments.PrintButtonClick(Sender: TObject);
begin
  inherited;
  EvMessage('Button is disabled');
end;

procedure TProcessTaxPayments.PayButtonClick(Sender: TObject);

  function Get_Liability_List(CL_NBR: integer; LiabilityField: string): string;
  var
    f: TField;
    Tmp: String;
    ACount,aGCount: Integer;
    sep,sep2:string;

  begin
    result := '';
    aGCount := 0;
    tmp := '';
    with TEMP_PR_LIABILITY do
    begin
      SetRange([CL_NBR, 'Y', 'N'], [CL_NBR, 'Y', 'N']);
      Result := '';
      sep := '';
      sep2 := '';
      ACount := 0;
      First;
      f := FieldByName(LiabilityField);
      while not EOF do
      begin
        if (not f.IsNull) then
        begin
          Inc(ACount);
          Inc(aGCount);
          Result := Result + sep + f.AsString;
          Sep := ',';
          if ACount > 1490 then
          begin
            Tmp := Tmp + sep2 + LiabilityField+ ' in ('+ Result+ ')';
            Result := '';
            ACount := 0;
            sep := '';
            sep2 := ' or ';
          end
        end;
        Next;
      end;
    end;

    if aGCount >0 then
    begin
         tmp :='(' + Tmp + sep2;
         if ACount > 0 then
            tmp := tmp + LiabilityField+ ' in ('+ Result+ ')';
         Result := tmp + ')';
    end
    else
      Result := '';
  end;

  function CreateNewSbTaxPayment : Integer;
  begin
     DM_SERVICE_BUREAU.SB_TAX_PAYMENT.Activate;
     DM_SERVICE_BUREAU.SB_TAX_PAYMENT.Append;
     if Trim(edtTaxDesc.Text) <> '' then
        DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('DESCRIPTION').AsString := edtTaxDesc.Text
     else
        DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('DESCRIPTION').AsString :=
                     'Tax Payment by ' + Context.UserAccount.User + ' - '  + DateToStr(Now);
     DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('STATUS').AsString := SB_TAX_PAYMENT_WAITING;
     DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('STATUS_DATE').AsDateTime := Now;
     DM_SERVICE_BUREAU.SB_TAX_PAYMENT.Post;
     ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_TAX_PAYMENT]);
     Result := DM_SERVICE_BUREAU.SB_TAX_PAYMENT.FieldByName('SB_TAX_PAYMENT_NBR').AsInteger;
  end;

var
  t: IevProcessTaxPaymentsTask;
  FedNbrs: string;
  StateNbrs: string;
  SuiNbrs: string;
  LocalNbrs: string;
  WasLiabIndexName: string;
  WasFiltered, WasLiabFiltered: Boolean;
  CL_NBR: Integer;
  StateTax, OtherTax: Double;
begin
  if not (EvMessage('Are you sure you want to pay the selected liabilities ?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
    Exit;

  t := mb_TaskQueue.CreateTask(QUEUE_TASK_PROCESS_TAX_PAYMENTS, True) as IevProcessTaxPaymentsTask;
  try
    DM_TEMPORARY.TMP_CL.IndexFieldNames := 'CUSTOM_CLIENT_NUMBER';
    DM_TEMPORARY.TMP_CL.First;
    WasLiabIndexName := TEMP_PR_LIABILITY.IndexName;
    WasLiabFiltered := TEMP_PR_LIABILITY.Filtered;
    TEMP_PR_LIABILITY.DisableControls;
    TEMP_PR_LIABILITY.Filtered := False;
    TEMP_PR_LIABILITY.IndexFieldNames := 'CL_NBR;PAY;PAID';
    try
      while not DM_TEMPORARY.TMP_CL.Eof do
      begin
        CL_NBR := DM_TEMPORARY.TMP_CL['CL_NBR'];
        FedNbrs := Get_Liability_List(CL_NBR, 'CO_FED_TAX_LIABILITIES_NBR');
        StateNbrs := Get_Liability_List(CL_NBR, 'CO_STATE_TAX_LIABILITIES_NBR');
        SuiNbrs := Get_Liability_List(CL_NBR, 'CO_SUI_LIABILITIES_NBR');
        LocalNbrs := Get_Liability_List(CL_NBR, 'CO_LOCAL_TAX_LIABILITIES_NBR');
        
        StateTax := 0;
        OtherTax := 0;
        TEMP_PR_LIABILITY.SetRange([CL_NBR, 'Y', 'N'], [CL_NBR, 'Y', 'N']);
        TEMP_PR_LIABILITY.First;
        while not TEMP_PR_LIABILITY.Eof do
        begin
          if TEMP_PR_LIABILITY.FieldByName('STATE').AsString = 'CA' then
          begin
            if TEMP_PR_LIABILITY.FieldByName('LIABILITY_TYPE').AsString = TAX_LIABILITY_TYPE_STATE then
              StateTax := StateTax + TEMP_PR_LIABILITY.FieldByName('AMOUNT').AsFloat
            else
              OtherTax := OtherTax + TEMP_PR_LIABILITY.FieldByName('AMOUNT').AsFloat;
          end;
          TEMP_PR_LIABILITY.Next;
        end;
        if ((StateTax < -0.005) and (OtherTax > 0.005) or (StateTax > 0.005) and (OtherTax < -0.005)) then
          raise ETaxDepositException.Create('Can''t offset one tax bucket with another through EFT credit. This should be fixed through tax return by requesting a credit.');

        if (FedNbrs <> '')
        or (StateNbrs <> '')
        or (SuiNbrs <> '')
        or (LocalNbrs <> '') then
          with t.Params.Add do
          begin
            ClientID := CL_NBR;
            CustomClientNumber := DM_TEMPORARY.TMP_CL['CUSTOM_CLIENT_NUMBER'];
            FedTaxLiabilityList := FedNbrs;
            StateTaxLiabilityList := StateNbrs;
            SUITaxLiabilityList := SuiNbrs;
            LocalTaxLiabilityList := LocalNbrs;
          end;
        DM_TEMPORARY.TMP_CL.Next;
      end;
    finally
      TEMP_PR_LIABILITY.CancelRange;
      TEMP_PR_LIABILITY.IndexName := WasLiabIndexName;
      TEMP_PR_LIABILITY.Filtered := WasLiabFiltered;
      TEMP_PR_LIABILITY.EnableControls;
    end;

    t.DefaultMiscCheckForm := MiscCheckFormCodeValues[cbMiscCheckForm.ItemIndex];
    t.SbTaxPaymentNbr := CreateNewSbTaxPayment;
    t.ForceChecks := cbForceChecks.Checked;
    t.EftpReference := FormatDateTime('yyyymmddhhmm', Now);
    t.EftpReference := 'EFTD#'+ Copy(t.EftpReference, 1, 8)+ Copy(t.EftpReference, 10, 255);
    t.SendEmailNotification := cbEMail.Checked;
    t.NotificationEmail :=  eEMail.Text;
    t.EmailSendRule := TevEmailSendRule(cbEmailSendRule.ItemIndex);
    t.PostProcessReportName := cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];

    ctx_EvolutionGUI.AddTaskQueue(t);
    with TEMP_PR_LIABILITY do
    begin
      WasFiltered := Filtered;
      try
        DisableControls;
        FieldByName('Paid').ReadOnly := False;
        FieldByName('Pay').OnValidate := nil;
        Filtered := False;
        First;
        while not EOF do
        begin
          if {(FieldByName('CL_NBR').AsInteger = CL_NBR)
            and} (FieldByName('Pay').AsString = 'Y')
            {and (Pos(' ' + FieldByName('CO_NBR').AsString + ',', BadCompanies) = 0)} then
          begin
            Edit;
            FieldByName('Paid').AsString := 'Y';
            FieldByName('Pay').AsString := 'N';
            Post;
          end;
          Next;
        end;
      finally
        FieldByName('Paid').ReadOnly := True;
        FieldByName('Pay').OnValidate := ValidatePaidBox;
        Filtered := WasFiltered;
        EnableControls;
        First;
      end;
    end;
  finally
    t := nil;
  end;
end;

procedure TProcessTaxPayments.ExcelButtonClick(Sender: TObject);
var
  F1: TextFile;
  PRBookMark: TBookmark;
  PR_OldFiltered: boolean;
  DetailFiltered: boolean;
  DetailBookMark: TBookmark;
  procedure WriteElement(const s: string; const LastInTheLine: Boolean = False);
  begin
    if LastInTheLine then
      Writeln(F1, AnsiQuotedStr(s, '"'))
    else
      Write(F1, AnsiQuotedStr(s, '"')+ ',');
  end;
begin
  inherited;
  SaveDialog1.Title := 'Save to Excel file';
  InReport := True;
  try
    if SaveDialog1.Execute then
    begin
      ctx_StartWait;
      try
        AssignFile(F1, SaveDialog1.Filename);
        Rewrite(F1);

        WriteElement('EIN');
        WriteElement('Method');
        WriteElement('Tax_agency');
        WriteElement('Client_Number');
        WriteElement('Number');
        WriteElement('Name');
        //WriteElement('Tax_Service');
        WriteElement('CHECK_DATE');
        WriteElement('DUE_DATE');
        WriteElement('Tax_type');
        WriteElement('Status');
        WriteElement('Run_number');
        WriteElement('Amount');
        WriteElement('Pay');
        WriteElement('Paid', True);

        PRBookMark := TEMP_PR_LIABILITY.GetBookmark;
        PR_OldFiltered := TEMP_PR_LIABILITY.Filtered;
        TEMP_PR_LIABILITY.Filtered := False;
        TEMP_PR_LIABILITY.DisableControls;

        DetailBookMark := TEMP_DETAIL.GetBookmark;
        DetailFiltered := TEMP_DETAIL.Filtered;
        TEMP_DETAIL.Filtered := False;
        TEMP_DETAIL.DisableControls;

        TEMP_DETAIL.First;
        while not TEMP_DETAIL.EOF do
        begin
          TEMP_PR_LIABILITY.First;
          while not TEMP_PR_LIABILITY.EOF do
          begin
            if TEMP_PR_LIABILITY.FieldByName('DETAIL_NBR').AsInteger =
              TEMP_DETAIL.FieldByName('DETAIL_NBR').AsInteger then
            begin

              WriteElement(TEMP_DETAIL.FieldByName('EIN').AsString);
              WriteElement(TEMP_DETAIL.FieldByName('Method').AsString);
              WriteElement(TEMP_DETAIL.FieldByName('Tax_agency').AsString);
              WriteElement(TEMP_DETAIL.FieldByName('Client_Number').AsString);
              WriteElement(TEMP_DETAIL.FieldByName('Number').AsString);
              WriteElement(TEMP_DETAIL.FieldByName('Name').AsString);
              //WriteElement(TEMP_DETAIL.FieldByName('TAX_SERVICE').AsString);
              WriteElement(TEMP_PR_LIABILITY.FieldByName('CHECK_DATE').AsString);
              WriteElement(TEMP_PR_LIABILITY.FieldByName('DUE_DATE').AsString);
              WriteElement(TEMP_PR_LIABILITY.FieldByName('Tax_type').AsString);
              WriteElement(TEMP_PR_LIABILITY.FieldByName('STATUS').AsString);
              WriteElement(TEMP_PR_LIABILITY.FieldByName('Run_number').AsString);
              WriteElement(TEMP_PR_LIABILITY.FieldByName('Amount').AsString);
              WriteElement(TEMP_PR_LIABILITY.FieldByName('Pay').AsString);
              WriteElement(TEMP_PR_LIABILITY.FieldByName('Paid').AsString, True);
            end;
            TEMP_PR_LIABILITY.Next;
          end;
          TEMP_DETAIL.Next;
        end;

        TEMP_DETAIL.GotoBookmark(DetailBookMark);
        TEMP_DETAIL.FreeBookmark(DetailBookMark);
        TEMP_DETAIL.Filtered := DetailFiltered;
        TEMP_DETAIL.EnableControls;

        TEMP_PR_LIABILITY.GotoBookmark(PRBookMark);
        TEMP_PR_LIABILITY.FreeBookmark(PRBookMark);
        TEMP_PR_LIABILITY.Filtered := PR_OldFiltered;
        TEMP_PR_LIABILITY.EnableControls;

        CloseFile(F1);
      finally
        ctx_EndWait;
      end;
    end;
  finally
    InReport := False;
  end;
end;

procedure TProcessTaxPayments.BitBtn6Click(Sender: TObject);

procedure SaveIt(aDataSet: TevClientDataSet);
  begin
    aDataSet.SaveToFile('f:\evolution\client\' + aDataSet.Name + '.cds');
  end;
begin
  inherited;
  SaveIt(TEMP_CO);
  SaveIt(TEMP_PR_LIABILITY);
  SaveIt(TEMP_TAX_TYPE);
  SaveIt(TEMP_DETAIL);
end;

procedure TProcessTaxPayments.Button5Click(Sender: TObject);
begin
  inherited;
  TEMP_PR_LIABILITY.MasterSource := wwdsTEMP_DETAIL;
  TEMP_PR_LIABILITY.MasterFields := 'DETAIL_NBR';
  TEMP_PR_LIABILITY.IndexName := 'PR_DETAIL_NBRIndex';
end;

procedure TProcessTaxPayments.ValidatePaidBox(Sender: TField);
var
  b: Boolean;
begin
  with TEMP_PR_LIABILITY do
    if not ControlsDisabled then
    begin
      if (FieldByName('PAY').AsString = 'Y') and (FieldByName('CAN_BE_PAID').AsString = 'N') then
      begin
        if FieldByName('CHECK_DATE').IsNull then
          raise ECanNotPerformOperation.CreateHelp('Payment of this liability is restricted.'+
            #13'It doesn''t have a valid check date', IDH_CanNotPerformOperation)
        else
        if not FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
          raise ECanNotPerformOperation.CreateHelp('Payment of this liability is restricted.'+
            #13'It has a deposit record attached', IDH_CanNotPerformOperation)
        else
        if not FPaymentWoFundsOn then
        begin
          ctx_StartWait;
          try
            b := ctx_AccountRights.Functions.GetState('ABILITY_PAY_WO_FUNDS') <> stEnabled;
          finally
            ctx_EndWait;
          end;
          if b then
            raise ECanNotPerformOperation.CreateHelp('Payment of this liability is restricted.'+
              #13'Companies on Tax Service must have the liability status as Impounded.'+
              #13'Non-Tax Service companies must have the liability status of Pending.', IDH_CanNotPerformOperation)
          else
            case EvMessage('Do you really want to pay liabilities without funds?'#13'This warning will not be shown again while you in the screen', mtConfirmation, [mbYes, mbNo]) of
              mrYes: FPaymentWoFundsOn := True;
              mrNo: AbortEx;
            end;
        end;
      end;
      if TEMP_PR_LIABILITY.FieldByName('Paid').AsString = 'Y' then
        raise ECanNotPerformOperation.CreateHelp('Liability has been paid.', IDH_CanNotPerformOperation);
    end;
end;

procedure TProcessTaxPayments.wwDBGrid3CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;

  if not (Field.FieldName = 'PAY') then
  begin
    if TEMP_PR_LIABILITY.FieldByName('Paid').AsString = 'Y' then
      ABrush.Color := clAqua
    else
      if not TEMP_PR_LIABILITY.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
        ABrush.Color := clFuchsia
      else
        if TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString = 'N' then
          ABrush.Color := clYellow;
  end;
end;

procedure TProcessTaxPayments.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  AllowChange := False;
end;

procedure TProcessTaxPayments.wwDBComboBox1CloseUp(Sender: TwwDBComboBox;
  Select: Boolean);
begin
  inherited;
  if Select and (Sender.DataSource.DataSet.State = dsEdit) then
  begin
    Sender.DataSource.DataSet[Sender.DataField] := Sender.Value;
    Sender.DataSource.DataSet.Post;
  end;
end;

procedure TProcessTaxPayments.SetTaxListComboBoxs(const ds : TevClientDataSet; const Field : String; const cb : tevComboBox);
var
 pList: TStringList;
begin
    pList := ds.GetFieldValueList(Field);
    try
      cb.Items.SetText(pList.GetText);
      if pList.Count > 1 then
         cb.Items.Add('   ALL');
    finally
      pList.Free;
    end;
end;


procedure TProcessTaxPayments.evCbCoNbrChange(Sender: TObject);
 function SetTaxListFilter(const sFilter: String; const Field : String; const cb : tevComboBox):String;
 var
  sAnd: String;
 begin
  Result :='';    sAnd := '';
  if sFilter <> '' then
  begin
   Result := sFilter;  sAnd := ' and ';
  end;
  if (Trim(cb.Text) <> 'ALL') and (Trim(cb.Text) <> '') then
     Result := Result + sand + Field + ' like ''%' + Trim(cb.Text) + '%'' ';
 end;

var
 Filter : String;

begin
  inherited;
  Filter := SetTaxListFilter(Filter, 'CUSTOM_CLIENT_NUMBER',evCbClNbr);
  Filter := SetTaxListFilter(Filter, 'CUSTOM_COMPANY_NUMBER',evCbCoNbr);
  Filter := SetTaxListFilter(Filter, 'AGENCY_NAME',evCbAgencyName);
  Filter := SetTaxListFilter(Filter, 'LiabType',evCbLiabType);
  Filter := SetTaxListFilter(Filter, 'DEPOSIT_TYPE_desc',evCbDepType);
  if Filter <> '' then
  begin
   cdSBTaxPaymentLiabs.Filter := Filter;
   cdSBTaxPaymentLiabs.Filtered := True;
  end
  else
   cdSBTaxPaymentLiabs.Filtered := false;

end;

procedure TProcessTaxPayments.evGrdTaxListLiabDrawFooterCell(
  Sender: TObject; Canvas: TCanvas; FooterCellRect: TRect; Field: TField;
  FooterText: String; var DefaultDrawing: Boolean);
begin
  inherited;

  if cdSBTaxPaymentLiabs.AggregatesActive then
  begin
     evGrdTaxListLiab.ColumnByName('Amount').FooterValue :=
         FormatFloat('#,##0.##', ConvertNull(cdSBTaxPaymentLiabs.Aggregates.Find('amount').Value, 0));
  end;
end;



procedure TProcessTaxPayments.GetSbTaxPaymentLiabs(const SbTaxPaymentNbr : integer);
var
 s : String;
begin
    s :=  ' SELECT t9.CUSTOM_CLIENT_NUMBER, t2.CUSTOM_COMPANY_NUMBER, t2.Name, ''STATE'' LiabType, ' +
             ' CASE t1.DEPOSIT_TYPE ' +
               ' WHEN ''C'' THEN ''Credit'' ' +
               ' WHEN ''D'' THEN ''Debit'' ' +
               ' WHEN ''N'' THEN ''None'' ' +
               ' ELSE ''Check'' ' +
              ' END DEPOSIT_TYPE_desc, ' +
                   ' t6.Amount, t6.DUE_DATE, t6.CHECK_DATE, t1.SY_GL_AGENCY_FIELD_OFFICE_NBR, t1.Deposit_Type, t1.Co_Tax_Deposits_Nbr, t1.Cl_Nbr, t1.Co_Nbr, t1.Sb_Tax_Payment_Nbr ' +
             ' FROM Tmp_CO_STATE_TAX_LIABILITIES t6, Tmp_Co_Tax_Deposits  t1, Tmp_Co  t2, TMP_CL t9 ' +
             ' where t1.Cl_Nbr = t6.Cl_Nbr  AND  t1.Co_Tax_Deposits_Nbr = t6.Co_Tax_Deposits_Nbr and ' +
                    ' t1.Sb_Tax_Payment_Nbr = :SbTaxPaymentNbr and t2.Cl_Nbr = t1.Cl_Nbr  AND  t2.Co_Nbr = t1.Co_Nbr and t9.Cl_Nbr = t1.Cl_Nbr ' +
          ' UNION ALL ' +
           ' SELECT t10.CUSTOM_CLIENT_NUMBER, t2.CUSTOM_COMPANY_NUMBER, t2.Name, ''FED'' LiabType,  ' +
             ' CASE t1.DEPOSIT_TYPE ' +
               ' WHEN ''C'' THEN ''Credit'' ' +
               ' WHEN ''D'' THEN ''Debit'' ' +
               ' WHEN ''N'' THEN ''None'' ' +
               ' ELSE ''Check'' ' +
              ' END DEPOSIT_TYPE_desc, ' +
                   ' t3.Amount, t3.DUE_DATE, t3.CHECK_DATE, t1.SY_GL_AGENCY_FIELD_OFFICE_NBR, t1.Deposit_Type, t1.Co_Tax_Deposits_Nbr, t1.Cl_Nbr, t1.Co_Nbr, t1.Sb_Tax_Payment_Nbr ' +
             ' FROM Tmp_CO_FED_TAX_LIABILITIES  t3, Tmp_Co_Tax_Deposits t1, Tmp_Co  t2, TMP_CL t10  ' +
             ' where t1.Cl_Nbr = t3.Cl_Nbr  AND t1.Co_Tax_Deposits_Nbr = t3.Co_Tax_Deposits_Nbr and ' +
                    ' t1.Sb_Tax_Payment_Nbr = :SbTaxPaymentNbr and t2.Cl_Nbr = t1.Cl_Nbr  AND  t2.Co_Nbr = t1.Co_Nbr and t10.Cl_Nbr = t1.Cl_Nbr  ' +
          ' UNION ALL ' +
           ' SELECT t11.CUSTOM_CLIENT_NUMBER, t2.CUSTOM_COMPANY_NUMBER, t2.Name, ''SUI'' LiabType, ' +
             ' CASE t1.DEPOSIT_TYPE ' +
               ' WHEN ''C'' THEN ''Credit'' ' +
               ' WHEN ''D'' THEN ''Debit'' ' +
               ' WHEN ''N'' THEN ''None'' ' +
               ' ELSE ''Check'' ' +
              ' END DEPOSIT_TYPE_desc, ' +
                   ' t7.Amount, t7.DUE_DATE, t7.CHECK_DATE, t1.SY_GL_AGENCY_FIELD_OFFICE_NBR, t1.Deposit_Type, t1.Co_Tax_Deposits_Nbr, t1.Cl_Nbr, t1.Co_Nbr, t1.Sb_Tax_Payment_Nbr ' +
              ' FROM Tmp_CO_SUI_LIABILITIES t7, Tmp_Co_Tax_Deposits t1, Tmp_Co  t2, TMP_CL t11  ' +
              ' where t1.Cl_Nbr = t7.Cl_Nbr AND t1.Co_Tax_Deposits_Nbr = t7.Co_Tax_Deposits_Nbr and ' +
                     ' t1.Sb_Tax_Payment_Nbr = :SbTaxPaymentNbr and t2.Cl_Nbr = t1.Cl_Nbr  AND  t2.Co_Nbr = t1.Co_Nbr and t11.Cl_Nbr = t1.Cl_Nbr ' +
          ' UNION ALL ' +
           ' SELECT t12.CUSTOM_CLIENT_NUMBER, t2.CUSTOM_COMPANY_NUMBER, t2.Name, ''LOCAL'' LiabType, ' +
           ' CASE t1.DEPOSIT_TYPE ' +
               ' WHEN ''C'' THEN ''Credit'' ' +
               ' WHEN ''D'' THEN ''Debit'' ' +
               ' WHEN ''N'' THEN ''None'' ' +
               ' ELSE ''Check'' ' +
              ' END DEPOSIT_TYPE_desc, ' +
                  ' t8.Amount, t8.DUE_DATE, t8.CHECK_DATE, t1.SY_GL_AGENCY_FIELD_OFFICE_NBR, t1.Deposit_Type, t1.Co_Tax_Deposits_Nbr, t1.Cl_Nbr, t1.Co_Nbr, t1.Sb_Tax_Payment_Nbr ' +
             ' FROM Tmp_CO_LOCAL_TAX_LIABILITIES t8, Tmp_Co_Tax_Deposits t1, Tmp_Co  t2, TMP_CL t12  ' +
             ' where t1.Cl_Nbr = t8.Cl_Nbr  AND  t1.Co_Tax_Deposits_Nbr = t8.Co_Tax_Deposits_Nbr and ' +
                  ' t1.Sb_Tax_Payment_Nbr = :SbTaxPaymentNbr and t2.Cl_Nbr = t1.Cl_Nbr  AND  t2.Co_Nbr = t1.Co_Nbr and t12.Cl_Nbr = t1.Cl_Nbr  ';
  cdSBTaxPaymentLiabs.DisableControls;
  with TExecDSWrapper.Create(s) do
  begin
      cdSBTaxPaymentLiabs.Close;
      SetParam('SbTaxPaymentNbr', SbTaxPaymentNbr);
      ctx_DataAccess.GetTmpCustomData(cdSBTaxPaymentLiabs, AsVariant);

      with cdSBTaxPaymentLiabs.Aggregates.Add do
      begin
           AggregateName := 'Amount';
           Expression := 'Sum(Amount)';
           Active := True;
      end;
      cdSBTaxPaymentLiabs.AggregatesActive := True;

      SetTaxListComboBoxs(cdSBTaxPaymentLiabs,'CUSTOM_CLIENT_NUMBER',evCbClNbr);
      SetTaxListComboBoxs(cdSBTaxPaymentLiabs,'CUSTOM_COMPANY_NUMBER',evCbCoNbr);
      SetTaxListComboBoxs(cdSBTaxPaymentLiabs,'AGENCY_NAME',evCbAgencyName);
      SetTaxListComboBoxs(cdSBTaxPaymentLiabs,'LiabType',evCbLiabType);
      SetTaxListComboBoxs(cdSBTaxPaymentLiabs,'DEPOSIT_TYPE_desc',evCbDepType);

  end;
  cdSBTaxPaymentLiabs.EnableControls;
end;

procedure TProcessTaxPayments.cdSBTaxPaymentLiabsCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  if not DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Active then
      DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.DataRequired('ALL');
  if not DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Active then
      DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.DataRequired('ALL');
  if cdSBTaxPaymentLiabsSY_GL_AGENCY_FIELD_OFFICE_NBR.AsInteger > 0  then
  begin
   cdSBTaxPaymentLiabsAGENCY_NAME.AsString :=
    DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(StrToInt(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.ShadowDataSet.CachedLookup(cdSBTaxPaymentLiabsSY_GL_AGENCY_FIELD_OFFICE_NBR.AsInteger ,'SY_GLOBAL_AGENCY_NBR')) ,'AGENCY_NAME');

   cdSBTaxPaymentLiabsSY_GLOBAL_AGENCY_NBR.AsInteger :=
      StrToInt(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.ShadowDataSet.CachedLookup(cdSBTaxPaymentLiabsSY_GL_AGENCY_FIELD_OFFICE_NBR.AsInteger,'SY_GLOBAL_AGENCY_NBR'));
  end;
end;


procedure TProcessTaxPayments.GetSbTaxPayment;
var
 s, sDays, sStatus : String;
begin
  sDays := '';
  if evcbTenDays.Checked then
     sDays := ' and PaymentDate > :Date1 ';

  sStatus := 'and a.STATUS in (''P'',''W'') ';
  if evstatusFilter.ItemIndex = 1 then
     sStatus := ' and a.STATUS = ''R'' '
  else
  if evstatusFilter.ItemIndex = 2 then
     sStatus := '';

  s := 'select DESCRIPTION, PaymentDate, ' +
          ' CASE a.STATUS ' +
              ' WHEN ''W'' THEN ''Waiting''' +
              ' WHEN ''R'' THEN ''Rolled back''' +
              ' ELSE ''Processed''' +
              ' END Status_desc, ' +
          ' STATUS_DATE, a.SB_TAX_PAYMENT_NBR, STATUS ' +
        ' from SB_TAX_PAYMENT a, ' +
          ' (Select SB_TAX_PAYMENT_NBR, Min(STATUS_DATE) PaymentDate ' +
            ' from SB_TAX_PAYMENT group by SB_TAX_PAYMENT_NBR) b ' +
            ' where {AsOfNow<a>} and ' +
                 ' a.SB_TAX_PAYMENT_NBR = b.SB_TAX_PAYMENT_NBR ' +
                 sDays + sStatus;

  cdSBTaxPayment.DisableControls;
  with TExecDSWrapper.Create(s) do
  begin
      cdSBTaxPayment.Close;
      if evcbTenDays.Checked then
         SetParam('Date1', Now-10);
      ctx_DataAccess.GetSbCustomData(cdSBTaxPayment, AsVariant);
  end;
  cdSBTaxPayment.EnableControls;
  evGrdRollBack.UnselectAll;
end;


procedure TProcessTaxPayments.Activate;
var
  IntFld: TIntegerField;
  StrFld: TStringField;
  DateFld: TDateTimeField;
  CurrencyFld: TCurrencyField;

var
  TaxTable: TTaxTable;
  I: Integer;
  s, sFilter : String;
  S1, S2, ComboChoices: String;
begin
  inherited;

  tmpClientID := ctx_DataAccess.ClientID;
  tmpCoNbr:= DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger;

  cbMiscCheckForm.Clear;
  MiscCheckFormCodeValues := TStringList.Create;
  ComboChoices := CheckForm_ComboChoices;
  I := 1;
  while not GetToken(ComboChoices, I, S1) do
  begin
    GetToken(ComboChoices, I, S2);
    cbMiscCheckForm.Items.Append(S1);
    MiscCheckFormCodeValues.Append(S2);
  end;
  if not DM_SERVICE_BUREAU.SB.FieldByName('MISC_CHECK_FORM').IsNull then
     cbMiscCheckForm.ItemIndex := MiscCheckFormCodeValues.IndexOf(DM_SERVICE_BUREAU.SB.FieldByName('MISC_CHECK_FORM').AsString);


  s := 'select a.CL_NBR, a.CO_NBR, a.CUSTOM_COMPANY_NUMBER, a.NAME ' +
              ' from TMP_CO a where a.CREDIT_HOLD = ''C'' ';

  sFilter := GetReadOnlyClintCompanyFilter(True,'b.','a.',True);
  if sFilter <> '' then
   s := s +
          ' union ' +
          ' select a.CL_NBR, a.CO_NBR, a.CUSTOM_COMPANY_NUMBER, a.NAME ' +
                  ' from TMP_CO a, TMP_CL b ' +
                  ' where a.CL_NBR = b.CL_NBR ' + sFilter;

  with TExecDSWrapper.Create(s) do
  begin
      cdMaintenanceCo.Close;
      ctx_DataAccess.GetTmpCustomData(cdMaintenanceCo, AsVariant);
  end;

  tsMaintenance.TabVisible := False;
  if cdMaintenanceCo.RecordCount > 0 then
  begin
    tsMaintenance.TabVisible := True;
    pcAvail.ActivePage := tsMaintenance;
  end
  else
    pcAvail.ActivePage := tsByAgency;


  Get_cdAgencyTaxData(True);


  btnRollBack.Enabled := ctx_AccountRights.Functions.GetState('ROLLBACK_TAX_PAYMENTS') = stEnabled;

  DM_SYSTEM_STATE.SY_STATES.IndexFieldNames := 'SY_STATES_NBR';
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.IndexFieldNames := 'SY_STATE_DEPOSIT_FREQ_NBR';
  DM_SYSTEM_STATE.SY_SUI.IndexFieldNames := 'SY_SUI_NBR';
  DM_SYSTEM_LOCAL.SY_LOCALS.IndexFieldNames := 'SY_LOCALS_NBR';
  DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ.IndexFieldNames := 'SY_LOCAL_DEPOSIT_FREQ_NBR';
  FPaymentWoFundsOn := False;
  SelectAllRegardlessStatus1.Enabled := ctx_AccountRights.Functions.GetState('ABILITY_PAY_WO_FUNDS') = stEnabled;
  PageControl1.ActivePage := SelectTabSheet;
  InRecalculateFlags := False;

  FromDate.DateTime := GetBeginQuarter(GetBeginMonth(Date)-1);
  ToDate.DateTime := GetEndMonth(Date);

  eEMail.Text := Context.UserAccount.EMail;;
  cbEmailSendRule.ItemIndex :=0;

  cbPayMethod.Items.Clear;
  cbPayMethod.Items.Append('-All-');
  cbPayMethod.ItemIndex := 0;
  cbPayMethod.Items.Append('EFT Credit');
  cbPayMethod.Items.Append('EFT Debit');
  cbPayMethod.Items.Append('Check');
  cbPayMethod.Items.Append('Mark As Paid');
  cbPayMethod.Items.Append('EFT Credit 1');
  cbPayMethod.Items.Append('EFT Credit 2');

  cbTaxTable.Items.Clear;
  cbTaxTable.Items.Append('-All-');
  cbTaxTable.ItemIndex := 0;
  for TaxTable := ttFed to ttLoc do
    cbTaxTable.Items.Append(TaxTableName(TaxTable));
  cbTaxTable.OnChange(nil); // forces first-time init

  {----------------------------------}

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'TAX_TYPE_NBR';
    FieldKind := fkData;
    DisplayLabel := 'TAX_TYPE_NBR';
    DataSet := TEMP_TAX_TYPE;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'SY_GLOBAL_AGENCY_NBR';
    FieldKind := fkData;
    DisplayLabel := 'SY_GLOBAL_AGENCY_NBR';
    DataSet := TEMP_TAX_TYPE;
    Visible := False;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Tax_Agency';
    FieldKind := fkData;
    Size := 30;
    DisplayLabel := 'Tax Agency';
    DataSet := TEMP_TAX_TYPE;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Tax_Type';
    FieldKind := fkData;
    Size := 30;
    DisplayLabel := 'Tax Type';
    Visible := False;
    DataSet := TEMP_TAX_TYPE;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'HasNSF';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'HasNSF';
    DataSet := TEMP_TAX_TYPE;
  end;

  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT';
    FieldKind := fkData;
    DisplayLabel := 'Amount to pay';
    DataSet := TEMP_TAX_TYPE;
  end;

  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT_I';
    FieldKind := fkData;
    DisplayLabel := 'Amount imp.';
    DataSet := TEMP_TAX_TYPE;
  end;

  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT_P';
    FieldKind := fkData;
    DisplayLabel := 'Amount pend.';
    DataSet := TEMP_TAX_TYPE;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'PAY';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'Pay';
    DataSet := TEMP_TAX_TYPE;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Report_Agency';
    FieldKind := fkData;
    Size := 10;
    DisplayLabel := 'Reporting Agency';
    DataSet := TEMP_TAX_TYPE;
  end;

  TEMP_TAX_TYPE.CreateDataSet;
  TEMP_TAX_TYPE.AddIndex('TAX_TYPE_NBRIndex', 'TAX_TYPE_NBR', [ixCaseInsensitive], '', '', 0);
  TEMP_TAX_TYPE.IndexName := 'TAX_TYPE_NBRIndex';

  {----------------------------------}

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'DETAIL_NBR';
    FieldKind := fkData;
    DisplayLabel := 'DETAIL_NBR';
    DataSet := TEMP_DETAIL;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'TAX_TYPE_NBR';
    FieldKind := fkData;
    DisplayLabel := 'TAX_TYPE_NBR';
    DataSet := TEMP_DETAIL;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'TEMP_CO_NBR';
    FieldKind := fkData;
    DisplayLabel := 'TEMP_CO_NBR';
    DataSet := TEMP_DETAIL;
    Visible := False;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Name';
    FieldKind := fkData;
    Size := 30;
    DisplayLabel := 'Name';
    DataSet := TEMP_DETAIL;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Number';
    FieldKind := fkData;
    Size := 30;
    DisplayLabel := 'Number';
    DataSet := TEMP_DETAIL;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'HasNSF';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'HasNSF';
    DataSet := TEMP_DETAIL;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Client_Number';
    FieldKind := fkData;
    Size := 10;
    DisplayLabel := 'Client Number';
    DataSet := TEMP_DETAIL;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Tax_Agency';
    FieldKind := fkData;
    Size := 30;
    DisplayLabel := 'Tax Agency';
    DataSet := TEMP_DETAIL;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'EIN';
    FieldKind := fkData;
    Size := 30;
    DisplayLabel := 'EIN';
    DataSet := TEMP_DETAIL;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Method';
    FieldKind := fkData;
    Size := 10;
    DisplayLabel := 'Method';
    DataSet := TEMP_DETAIL;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Frequency';
    FieldKind := fkData;
    Size := 10;
    DisplayLabel := 'Frequency';
    DataSet := TEMP_DETAIL;
  end;

  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT';
    FieldKind := fkData;
    DisplayLabel := 'Amount';
    DataSet := TEMP_DETAIL;
  end;

  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT_I';
    FieldKind := fkData;
    DisplayLabel := 'Amount imp.';
    DataSet := TEMP_DETAIL;
  end;

  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT_P';
    FieldKind := fkData;
    DisplayLabel := 'Amount pend.';
    DataSet := TEMP_DETAIL;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'PAY';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'Pay';
    DataSet := TEMP_DETAIL;
  end;

  TEMP_DETAIL.CreateDataSet;
  TEMP_DETAIL.LogChanges := False;
  TEMP_DETAIL.AddIndex('DETAIL_NBRIndex', 'DETAIL_NBR', [ixCaseInsensitive], '', '', 0);
  TEMP_DETAIL.IndexName := 'DETAIL_NBRIndex';
  wwdsGrid2.DataSet := TEMP_DETAIL;

  {----------------------------------}

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'CO_NBR';
    FieldKind := fkData;
    DisplayLabel := 'CO_NBR';
    DataSet := TEMP_CO;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'CL_NBR';
    FieldKind := fkData;
    DisplayLabel := 'CL_NBR';
    DataSet := TEMP_CO;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'TEMP_CO_NBR';
    FieldKind := fkData;
    DisplayLabel := 'TEMP_CO_NBR';
    DataSet := TEMP_CO;
    Visible := False;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Name';
    FieldKind := fkData;
    Size := 30;
    DisplayLabel := 'Name';
    DataSet := TEMP_CO;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Number';
    FieldKind := fkData;
    Size := 30;
    DisplayLabel := 'Number';
    DataSet := TEMP_CO;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Client_Number';
    FieldKind := fkData;
    Size := 10;
    DisplayLabel := 'Client Number';
    DataSet := TEMP_CO;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'HasNSF';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'HasNSF';
    DataSet := TEMP_CO;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'TAX_SERVICE';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'TAX_SERVICE';
    DataSet := TEMP_CO;
  end;

  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT';
    FieldKind := fkData;
    DisplayLabel := 'Amount';
    DataSet := TEMP_CO;
  end;

  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT_I';
    FieldKind := fkData;
    DisplayLabel := 'Amount imp.';
    DataSet := TEMP_CO;
  end;

  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT_P';
    FieldKind := fkData;
    DisplayLabel := 'Amount pend.';
    DataSet := TEMP_CO;
  end;

  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'PAY';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'Pay';
    DataSet := TEMP_CO;
  end;
  TEMP_CO.CreateDataSet;
  TEMP_CO.AddIndex('TEMP_CO_NBRIndex', 'TEMP_CO_NBR', [ixCaseInsensitive], '', '', 0);
  TEMP_CO.IndexName := 'TEMP_CO_NBRIndex';

  {----------------------------------}

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'TEMP_PR_LIABILITY_NBR';
    FieldKind := fkData;
    DisplayLabel := 'TEMP_PR_LIABILITY_NBR';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'CO_TAX_DEPOSITS_NBR';
    FieldKind := fkData;
    DisplayLabel := 'CO_TAX_DEPOSITS_NBR';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'DETAIL_NBR';
    FieldKind := fkData;
    DisplayLabel := 'DETAIL_NBR';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'CL_NBR';
    FieldKind := fkData;
    DisplayLabel := 'CL_NBR';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'CO_NBR';
    FieldKind := fkData;
    DisplayLabel := 'CO_NBR';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;

  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'CO_FED_TAX_LIABILITIES_NBR';
    FieldKind := fkData;
    DisplayLabel := 'CO_FED_TAX_LIABILITIES_NBR';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;
  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'CO_LOCAL_TAX_LIABILITIES_NBR';
    FieldKind := fkData;
    DisplayLabel := 'CO_LOCAL_TAX_LIABILITIES_NBR';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;
  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'CO_SUI_LIABILITIES_NBR';
    FieldKind := fkData;
    DisplayLabel := 'CO_SUI_LIABILITIES_NBR';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;
  IntFld := TIntegerField.Create(Self);
  with IntFld do
  begin
    FieldName := 'CO_STATE_TAX_LIABILITIES_NBR';
    FieldKind := fkData;
    DisplayLabel := 'CO_STATE_TAX_LIABILITIES_NBR';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;
  DateFld := TDateTimeField.Create(Self);
  with DateFld do
  begin
    FieldName := 'CHECK_DATE';
    FieldKind := fkData;
    DisplayLabel := 'Check Date';
    DataSet := TEMP_PR_LIABILITY;
  end;
  DateFld := TDateTimeField.Create(Self);
  with DateFld do
  begin
    FieldName := 'DUE_DATE';
    FieldKind := fkData;
    DisplayLabel := 'Due Date';
    DataSet := TEMP_PR_LIABILITY;
  end;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Tax_Type';
    FieldKind := fkData;
    Size := 15;
    DisplayLabel := 'Tax type';
    DataSet := TEMP_PR_LIABILITY;
  end;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'CAN_BE_PAID';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'Tax';
    DataSet := TEMP_PR_LIABILITY;
  end;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Status';
    FieldKind := fkData;
    Size := 10;
    DisplayLabel := 'Status';
    DataSet := TEMP_PR_LIABILITY;
  end;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Name';
    FieldKind := fkData;
    Size := 40;
    DisplayLabel := 'Name';
    DataSet := TEMP_PR_LIABILITY;
  end;
  CurrencyFld := TCurrencyField.Create(Self);
  with CurrencyFld do
  begin
    FieldName := 'AMOUNT';
    FieldKind := fkData;
    DisplayLabel := 'Amount';
    DataSet := TEMP_PR_LIABILITY;
  end;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'PAY';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'Pay';
    DataSet := TEMP_PR_LIABILITY;
  end;
  StrFld.OnValidate := ValidatePaidBox;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'InternalStatus';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'InternalStatus';
    DataSet := TEMP_PR_LIABILITY;
  end;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'Paid';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := 'Paid';
    DataSet := TEMP_PR_LIABILITY;
    Visible := False;
  end;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'RUN_NUMBER';
    FieldKind := fkData;
    Size := 10;
    DisplayLabel := 'RUN_NUMBER';
    DataSet := TEMP_PR_LIABILITY;
  end;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'STATE';
    FieldKind := fkData;
    Size := 2;
    DisplayLabel := '';
    DataSet := TEMP_PR_LIABILITY;
  end;
  StrFld := TStringField.Create(Self);
  with StrFld do
  begin
    FieldName := 'LIABILITY_TYPE';
    FieldKind := fkData;
    Size := 1;
    DisplayLabel := '';
    DataSet := TEMP_PR_LIABILITY;
  end;
  TEMP_PR_LIABILITY.CreateDataSet;
  TEMP_PR_LIABILITY.LogChanges := False;
  TEMP_PR_LIABILITY.AddIndex('TEMP_PR_LIABILITY_NBRIndex', 'TEMP_PR_LIABILITY_NBR', [ixCaseInsensitive], '', '', 0);
  TEMP_PR_LIABILITY.AddIndex('PR_DETAIL_NBRIndex', 'DETAIL_NBR;TEMP_PR_LIABILITY_NBR', [ixCaseInsensitive], '', '', 0);
  TEMP_PR_LIABILITY.IndexName := 'PR_DETAIL_NBRIndex';
  wwdsGrid3.DataSet := TEMP_PR_LIABILITY;
  UpdateGridViewsColumns;
  inherited;
end;

procedure TProcessTaxPayments.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck([DM_SYSTEM_STATE.SY_STATES, DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ, DM_SYSTEM_STATE.SY_SUI,
    DM_SYSTEM_LOCAL.SY_LOCALS, DM_SYSTEM_LOCAL.SY_LOCAL_DEPOSIT_FREQ,
    DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY, DM_SYSTEM_MISC.SY_GLOBAL_AGENCY,
    DM_TEMPORARY.TMP_CO, DM_TEMPORARY.TMP_CL, DM_TEMPORARY.TMP_CO_LOCAL_TAX,
    DM_TEMPORARY.TMP_CO_STATES,DM_SERVICE_BUREAU.SB], SupportDataSets, '');
end;

procedure TProcessTaxPayments.Deactivate;
begin
  inherited;

  cdSBTaxPaymentLiabs.AggregatesActive := False;
  cdSBTaxPaymentLiabs.Aggregates.Clear;
  cdSBTaxPaymentLiabs.Filtered := False;
  MiscCheckFormCodeValues.Free;

  if (tmpClientID <> ctx_DataAccess.ClientID) or
     (tmpCoNbr <> DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger) then
  begin
     ctx_DataAccess.OpenClient(tmpClientID);
     if not DM_TEMPORARY.TMP_CO.Active then
        DM_TEMPORARY.TMP_CO.DataRequired('ALL');
     DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([tmpClientID,  tmpCoNbr]),[]);
  end;
end;

procedure TProcessTaxPayments.btnChangeSelectedClick(Sender: TObject);
begin
  inherited;
  with Mouse.CursorPos do
    btnChangeSelected.PopupMenu.Popup(X, Y);
end;

procedure TProcessTaxPayments.DoChangeSelectedCompany(
  const Option: TChangeSelectedOption);
var
  CoBookMark: TBookmark;
  DetailFiltered: boolean;
  DetailBookMark: TBookmark;

  PRBookMark: TBookmark;
  PR_OldFiltered: boolean;
begin

    if wwDBGrid1.DataSource.DataSet <> TEMP_CO then exit;

    TEMP_CO.DisableControls;
    CoBookMark := TEMP_CO.GetBookmark;
    try
      TEMP_CO.First;
      while not TEMP_CO.EOF do
      begin

         TEMP_DETAIL.DisableControls;
         DetailFiltered := TEMP_DETAIL.Filtered;
         DetailBookMark := TEMP_DETAIL.GetBookmark;

         if TEMP_CO.EOF and TEMP_CO.BOF then
           TEMP_DETAIL.Filter := 'TEMP_CO_NBR = -1'
         else
           TEMP_DETAIL.Filter := 'TEMP_CO_NBR = ' + TEMP_CO.FieldByName('TEMP_CO_NBR').AsString;
         TEMP_DETAIL.Filtered := True;

         try
           TEMP_DETAIL.First;
           while not TEMP_DETAIL.EOF do
           begin
            if TEMP_DETAIL.FieldByName('TEMP_CO_NBR').AsString = TEMP_CO.FieldByName('TEMP_CO_NBR').AsString then
            begin

                TEMP_PR_LIABILITY.AfterPost := nil;
                PR_OldFiltered := TEMP_PR_LIABILITY.Filtered;

                if TEMP_DETAIL.BOF and TEMP_DETAIL.EOF then
                  TEMP_PR_LIABILITY.Filter := 'DETAIL_NBR= -1'
                else
                  TEMP_PR_LIABILITY.Filter := 'DETAIL_NBR=' + TEMP_DETAIL.FieldByName('DETAIL_NBR').AsString;
                TEMP_PR_LIABILITY.Filtered := True;

                PRBookMark := TEMP_PR_LIABILITY.GetBookmark;
                TEMP_PR_LIABILITY.DisableControls;
                try
                 TEMP_PR_LIABILITY.First;
                 while not TEMP_PR_LIABILITY.EOF do
                 begin
                   if TEMP_PR_LIABILITY.FieldByName('DETAIL_NBR').AsInteger = TEMP_DETAIL.FieldByName('DETAIL_NBR').AsInteger then
                   begin

                    if TEMP_PR_LIABILITY.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
                    begin
                     TEMP_PR_LIABILITY.Edit;
                     case Option of
                      csoNone:
                        TEMP_PR_LIABILITY.FieldByName('PAY').AsString := GROUP_BOX_NO;
                      csoAllImpounded:
                        if (TEMP_PR_LIABILITY.FieldByName('PAID').AsString = GROUP_BOX_NO) and
                           (TEMP_PR_LIABILITY.FieldByName('CAN_BE_PAID').AsString = GROUP_BOX_YES) then
                            TEMP_PR_LIABILITY.FieldByName('PAY').AsString := GROUP_BOX_YES
                        else
                           TEMP_PR_LIABILITY.FieldByName('PAY').AsString := GROUP_BOX_NO;
                      csoAll:
                        if (TEMP_PR_LIABILITY.FieldByName('PAID').AsString = GROUP_BOX_NO) then
                           TEMP_PR_LIABILITY.FieldByName('PAY').AsString := GROUP_BOX_YES
                        else
                           TEMP_PR_LIABILITY.FieldByName('PAY').AsString := GROUP_BOX_NO;
                      csoAllExceptNSF:
                        if (TEMP_PR_LIABILITY.FieldByName('PAID').AsString = GROUP_BOX_NO) and
                           (TEMP_PR_LIABILITY.FieldByName('InternalStatus').AsString <> TAX_DEPOSIT_STATUS_NSF) then
                          TEMP_PR_LIABILITY.FieldByName('PAY').AsString := GROUP_BOX_YES
                        else
                          TEMP_PR_LIABILITY.FieldByName('PAY').AsString := GROUP_BOX_NO;
                     end;
                     if TEMP_PR_LIABILITY.Modified then
                         TEMP_PR_LIABILITY.Post
                     else
                         TEMP_PR_LIABILITY.Cancel;
                    end;

                   end;
                   TEMP_PR_LIABILITY.Next;
                 end;
                finally
                 TEMP_PR_LIABILITY.GotoBookmark(PRBookMark);
                 TEMP_PR_LIABILITY.FreeBookmark(PRBookMark);
                 TEMP_PR_LIABILITY.Filtered := PR_OldFiltered;
                 TEMP_PR_LIABILITY.EnableControls;
                 TEMP_PR_LIABILITY.First;
                 TEMP_PR_LIABILITY.AfterPost := AfterLiabilityPost;
                end;

            end;
            TEMP_DETAIL.Next;
          end;
         finally
           TEMP_DETAIL.GotoBookmark(DetailBookMark);
           TEMP_DETAIL.FreeBookmark(DetailBookMark);
           TEMP_DETAIL.Filtered := DetailFiltered;
           TEMP_DETAIL.EnableControls;
         end;


       TEMP_CO.Next;
      end;
    finally
      TEMP_CO.GotoBookmark(CoBookMark);
      TEMP_CO.FreeBookmark(CoBookMark);
      TEMP_CO.EnableControls;
    end;

  ForceRecalc;

end;


procedure TProcessTaxPayments.DoChangeSelected(
  const Option: TChangeSelectedOption);
var
  SavePlace: TBookmark;
  WasFiltered: boolean;
  WasIndexName: string;
  fDeposit, fSelected, fStatus, fPaid, fInternalStatus: TField;
begin

  if cbSelectFilteredRecords.Checked then
  begin
   DoChangeSelectedCompany(Option);
   exit;
  end;

  with TEMP_PR_LIABILITY do
  begin
    DisableControls;
    SavePlace := GetBookmark;
    WasFiltered := Filtered;
    WasIndexName := IndexName;
    IndexName := 'TEMP_PR_LIABILITY_NBRIndex';
    Filtered := False;
    ctx_StartWait;
    try
      First;
      fDeposit := FieldByName('CO_TAX_DEPOSITS_NBR');
      fSelected := FieldByName('PAY');
      fStatus := FieldByName('CAN_BE_PAID');
      fPaid := FieldByName('PAID');
      fInternalStatus := FieldByName('InternalStatus');
      while not EOF do
      begin
        if fDeposit.IsNull then
        begin
          Edit;
          case Option of
          csoNone:
            fSelected.AsString := GROUP_BOX_NO;
          csoAllImpounded:
            if (fPaid.AsString = GROUP_BOX_NO) and (fStatus.AsString = GROUP_BOX_YES) then
              fSelected.AsString := GROUP_BOX_YES
            else
              fSelected.AsString := GROUP_BOX_NO;
          csoAll:
            if (fPaid.AsString = GROUP_BOX_NO) then
              fSelected.AsString := GROUP_BOX_YES
            else
              fSelected.AsString := GROUP_BOX_NO;
          csoAllExceptNSF:
            if (fPaid.AsString = GROUP_BOX_NO) and (fInternalStatus.AsString <> TAX_DEPOSIT_STATUS_NSF) then
              fSelected.AsString := GROUP_BOX_YES
            else
              fSelected.AsString := GROUP_BOX_NO;
          end;
          if Modified then
            Post
          else
            Cancel;
        end;
        Next;
      end;
    finally
      Filtered := WasFiltered;
      IndexName := WasIndexName;
      GotoBookmark(SavePlace);
      FreeBookmark(SavePlace);
      EnableControls;
      ctx_EndWait;
    end;
  end;
  if AutoRecalcTotals1.Checked then
    RecalcButton.Click;
end;

procedure TProcessTaxPayments.AutoRecalcTotals1Click(Sender: TObject);
begin
  inherited;
  AutoRecalcTotals1.Checked := not AutoRecalcTotals1.Checked;
end;

procedure TProcessTaxPayments.SelectAllImpounded1Click(Sender: TObject);
begin
  inherited;
  DoChangeSelected(csoAllImpounded);
end;

procedure TProcessTaxPayments.SelectAllRegardlessStatus1Click(
  Sender: TObject);
begin
  inherited;
  DoChangeSelected(csoAll);
end;

procedure TProcessTaxPayments.DeselectAll1Click(Sender: TObject);
begin
  inherited;
  DoChangeSelected(csoNone);
end;

procedure TProcessTaxPayments.SelectAllExceptNSF1Click(Sender: TObject);
begin
  inherited;
  DoChangeSelected(csoAllExceptNSF);
end;

procedure TProcessTaxPayments.AutoCreditMo(const PeriodEndDate: TDateTime);
var
  MoQuaterlyFreqNbr: Variant;
begin
  ctx_StartWait('Checking MO tax credit...');
  DM_SYSTEM_STATE.SY_STATES.Activate;
  DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Activate;
  MoQuaterlyFreqNbr := DM_SYSTEM_STATE.SY_STATE_DEPOSIT_FREQ.Lookup('SY_STATES_NBR;FREQUENCY_TYPE',
    VarArrayOf([DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', 'MO', 'SY_STATES_NBR'), FREQUENCY_TYPE_QUARTERLY]),
    'SY_STATE_DEPOSIT_FREQ_NBR');
  DM_TEMPORARY.TMP_CO_STATES.Filter := 'STATE=''MO''';
  DM_TEMPORARY.TMP_CO_STATES.Filtered := True;
  ctx_StartWait('', DM_TEMPORARY.TMP_CO_STATES.RecordCount);
  try
    DM_TEMPORARY.TMP_CO_STATES.First;
    while not DM_TEMPORARY.TMP_CO_STATES.Eof do
    begin
      if DM_TEMPORARY.TMP_CO_STATES['SY_STATE_DEPOSIT_FREQ_NBR'] <> MoQuaterlyFreqNbr then
      begin
        Assert(DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', DM_TEMPORARY.TMP_CO_STATES['CL_NBR;CO_NBR'], []));
        if DM_TEMPORARY.TMP_CO['TERMINATION_CODE'] = TERMINATION_ACTIVE then
        begin
          ctx_UpdateWait('Processing '+ QuotedStr(DM_TEMPORARY.TMP_CO['NAME']), DM_TEMPORARY.TMP_CO_STATES.RecNo);
          ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO_STATES['Cl_NBR']);
          DM_COMPANY.CO_STATES.DataRequired('CO_STATES_NBR='+ IntToStr(DM_TEMPORARY.TMP_CO_STATES['CO_STATES_NBR']));
          Assert( DM_COMPANY.CO_STATES.RecordCount=1);
          DM_COMPANY.CO.DataRequired('CO_NBR='+ IntToStr(DM_COMPANY.CO_STATES['CO_NBR']));
          Assert(DM_COMPANY.CO.RecordCount=1);
          if DM_COMPANY.CO_STATES['MO_TAX_CREDIT_ACTIVE'] = GROUP_BOX_YES then
          begin
            with TDM_MISC_LIAB_CORRECT.Create(nil, DM_TEMPORARY.TMP_CO_STATES['Cl_NBR'],
              DM_TEMPORARY.TMP_CO_STATES['CO_NBR'],
              SbSendsTaxPaymentsForCompany, PeriodEndDate, False, True, False, False) do
            try
              cdTaxes.First;
              while not cdTaxes.Eof do
              begin
                cdTaxes.Edit;
                cdTaxes['ToDo'] := cdTaxes['AdjType'] = TAX_LIABILITY_ADJUSTMENT_TYPE_LINE_STATE_CREDIT;
                cdTaxes.Post;
                cdTaxes.Next;
              end;
              ApplyChanges;
            finally
              Free;
            end;
          end;
        end;
      end;
      DM_TEMPORARY.TMP_CO_STATES.Next;
    end;
  finally
    DM_TEMPORARY.TMP_CO_STATES.Filtered := False;
    DM_TEMPORARY.TMP_CO_STATES.Filter := '';
    ctx_EndWait;
    ctx_EndWait;
  end;
end;

procedure TProcessTaxPayments.cbEMailClick(Sender: TObject);
begin
  inherited;
  eEMail.Enabled := cbEMail.Enabled and cbEMail.Checked;
  cbEmailSendRule.Enabled := cbEMail.Enabled and cbEMail.Checked;
end;

procedure TProcessTaxPayments.LoadAsciiReportNames;
var
  ReportName: String;
  i: Integer;
begin
  ReportName := mb_AppSettings.AsString['PostProcessACHReport'];

  cbPostProcessReport.Items.Clear;
  cbPostProcessReport.Items.Add('N/A');
  DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.DataRequired('ANCESTOR_CLASS_NAME='''+REPORT_ASCII_TYPE+'''');
  DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.First;
  while not DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Eof do
  begin
    cbPostProcessReport.Items.Add(DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.REPORT_DESCRIPTION.AsString+
      ' (B'+DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.SB_REPORT_WRITER_REPORTS_NBR.AsString+')');
    DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Next;
  end;
  DM_SYSTEM.SY_REPORT_WRITER_REPORTS.DataRequired('ANCESTOR_CLASS_NAME='''+REPORT_ASCII_TYPE+'''');
  DM_SYSTEM.SY_REPORT_WRITER_REPORTS.First;
  while not DM_SYSTEM.SY_REPORT_WRITER_REPORTS.Eof do
  begin
    cbPostProcessReport.Items.Add(DM_SYSTEM.SY_REPORT_WRITER_REPORTS.REPORT_DESCRIPTION.AsString+
      ' (B'+DM_SYSTEM.SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR.AsString+')');
    DM_SYSTEM.SY_REPORT_WRITER_REPORTS.Next;
  end;
  cbPostProcessReport.ItemIndex := 0;

  for i := 0 to cbPostProcessReport.Items.Count - 1 do
  if cbPostProcessReport.Items[i] = ReportName then
  begin
    cbPostProcessReport.ItemIndex := i;
    break;
  end;

  if cbPostProcessReport.ItemIndex = 0 then
    mb_AppSettings.AsString['PostProcessACHReport'] :=
      cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];

end;

procedure TProcessTaxPayments.DetailTabSheetShow(Sender: TObject);
begin
  LoadAsciiReportNames;
end;

procedure TProcessTaxPayments.cbPostProcessReportClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsString['PostProcessACHReport'] :=
    cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];
end;

procedure TProcessTaxPayments.evTaxPaymentListClick(Sender: TObject);
begin
  inherited;

  SelectTabSheet.Enabled := False;
  DetailRollBack.TabVisible := True;
  DetailRollBack.Enabled := True;

  GetSbTaxPayment;

  TaxListPageControl.ActivePage :=  TaxListDetail;
  PageControl1.ActivePage := DetailRollBack;
end;



procedure TProcessTaxPayments.btnRollBackClick(Sender: TObject);
const
  woMessage = 'Rolling back. Please wait...';
var
  i : Integer;
  ds: TDataSet;

begin
  Assert(evGrdRollBack.SelectedList.Count = 1 , 'Please select only one tax payment.');

  if EvMessage('Are you sure you want to roll back the selected payments?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;

  EvMessage('Confirm due dates for threshold based deposits. The liability''s due date and period end dates may need to be adjusted.');

  ctx_StartWait(woMessage, evGrdRollBack.SelectedList.Count);
  ctx_DataAccess.UnlockPRSimple;
  try
    try
     if evGrdRollBack.SelectedList.Count > 0 then
     begin
       ds := evGrdRollBack.DataSource.DataSet;
       ds.DisableControls;
       try
           for i := 0 to evGrdRollBack.SelectedList.Count - 1 do
           begin
            ds.GotoBookmark(evGrdRollBack.SelectedList.Items[i]);

            ctx_UpdateWait(woMessage, i);
            if ds.FieldByName('SB_TAX_PAYMENT_NBR').AsInteger > 0 then
               ctx_DataAccess.TaxPaymentRollback(ds.FieldByName('SB_TAX_PAYMENT_NBR').AsInteger, i);

           end;
       finally
         if (Sender as TevBitBtn).Name = 'btnRollBack2' then
             (Sender as TevBitBtn).Enabled := ctx_DataAccess.CheckSbTaxPaymentstatus(ds.FieldByName('SB_TAX_PAYMENT_NBR').AsInteger);
         GetSbTaxPayment;
         ds.EnableControls;
       end;
     end;
    except
      EvMessage('This payment may be partially rolled back');
      raise;
    end
  finally
    ctx_EndWait;
    ctx_DataAccess.LockPRSimple;
  end;
end;

procedure TProcessTaxPayments.btnTaxListLiabClick(Sender: TObject);
begin
  inherited;

  Assert(evGrdRollBack.SelectedList.Count = 1 , 'Please select only one tax payment.');

  if evGrdRollBack.SelectedList.Count > 0 then
  begin
    GetSbTaxPaymentLiabs(evGrdRollBack.DataSource.DataSet.FieldByName('SB_TAX_PAYMENT_NBR').asinteger);
    TaxListLiabs.TabVisible := True;
    TaxListLiabs.Enabled := True;
    btnRollBack2.Enabled := True;
    TaxListPageControl.ActivePage := TaxListLiabs;
  end
  else
    EvMessage('Please select Tax Payment.');
end;

procedure TProcessTaxPayments.evcbTenDaysClick(Sender: TObject);
begin
  inherited;
  GetSbTaxPayment;
end;

procedure TProcessTaxPayments.evstatusFilterClick(Sender: TObject);
begin
  inherited;
  GetSbTaxPayment;
  btnRollBack.Enabled := (ctx_AccountRights.Functions.GetState('ROLLBACK_TAX_PAYMENTS') = stEnabled) and (evstatusFilter.ItemIndex = 0);
  btnTaxListLiab.Enabled := (evstatusFilter.ItemIndex = 0);
end;

procedure TProcessTaxPayments.TaxListPageControlChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  AllowChange := False;
end;

procedure TProcessTaxPayments.evBtnClose2Click(Sender: TObject);
begin
  inherited;
  TaxListDetail.Enabled := True;
  TaxListPageControl.ActivePage := TaxListDetail;
  TaxListLiabs.Enabled := False;
  TaxListLiabs.TabVisible := False;
end;




procedure TProcessTaxPayments.cbTaxTableChange(Sender: TObject);
begin
  inherited;
  Get_cdAgencyTaxData;
  grdAgencyFrom.UnselectAll;
  grdTaxFrom.UnselectAll;
end;


procedure TProcessTaxPayments.Get_cdTaxData;
    procedure CreateActivateIndex(const d: TevClientDataset; const IName, Fields: string);
    begin
        d.AddIndex(IName, Fields, []);
        d.IndexDefs.Update;
        d.IndexName := IName;
    end;

    function create_Taxsql:string;
    begin
       result := '';
       if (cbTaxTable.ItemIndex = 0) or (cbTaxTable.ItemIndex = 1) then
       begin
         result :=
              ' select ''F'' TaxType, b.SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, a.SY_FED_TAX_PAYMENT_AGENCY_NBR Nbr , ''-- 941'' || '' '' || b.AGENCY_NAME NAME,  ''-'' State, ''FOAMCEB5D'' PaymentTaxType, ''N'' HIDDEN  ' +
                ' from SY_FED_TAX_PAYMENT_AGENCY a, SY_GLOBAL_AGENCY b ' +
                   ' where {AsOfNow<a>} and {AsOfNow<b>} and ' +
                   ' a.SY_GLOBAL_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR ' +
               ' union ' +
               ' select ''F'' TaxType, b.SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, a.SY_FED_TAX_PAYMENT_AGENCY_NBR Nbr , ''-- 940'' || '' '' || b.AGENCY_NAME NAME,  ''-'' State, ''U'' PaymentTaxType, ''N'' HIDDEN  ' +
                 ' from  SY_FED_TAX_PAYMENT_AGENCY a, SY_GLOBAL_AGENCY b ' +
                   ' where {AsOfNow<a>} and {AsOfNow<b>} and ' +
                     ' a.SY_GLOBAL_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR ';
       end;
       if (cbTaxTable.ItemIndex = 0) or (cbTaxTable.ItemIndex = 2) then
       begin
         if result <> '' then
            result := result + ' union ';
         result := result +
              ' select distinct ''S'' TaxType, b.SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, a.SY_STATES_NBR Nbr, b.AGENCY_NAME NAME,  a.STATE, ''-'' PaymentTaxType, ''N'' HIDDEN  ' +
                 ' from SY_STATES a, SY_GLOBAL_AGENCY b ' +
                 ' where {AsOfNow<a>} and {AsOfNow<b>} and ' +
                  ' a.SY_STATE_TAX_PMT_AGENCY_NBR = b.SY_GLOBAL_AGENCY_NBR and ' +
                    ' a.SY_STATE_TAX_PMT_AGENCY_NBR is not null ';
       end;
       if (cbTaxTable.ItemIndex = 0) or (cbTaxTable.ItemIndex = 3) then
       begin
         if result <> '' then
            result := result + ' union ';

         result := result +
              ' select Distinct  ''U'' TaxType, c.SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, a.SY_SUI_NBR Nbr, c.AGENCY_NAME || ''-'' || a.SUI_TAX_NAME NAME, b.STATE, ''-'' PaymentTaxType, ''N'' HIDDEN  ' +
                         ' from SY_SUI a, SY_STATES b, SY_GLOBAL_AGENCY c ' +
                          ' where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                              ' b.SY_STATES_NBR = a.SY_STATES_NBR and ' +
                                ' a.SY_SUI_TAX_PMT_AGENCY_NBR = c.SY_GLOBAL_AGENCY_NBR and ' +
                                ' a.SY_SUI_TAX_PMT_AGENCY_NBR is not null ';
       end;
       if (cbTaxTable.ItemIndex = 0) or (cbTaxTable.ItemIndex = 4) then
       begin
         if result <> '' then
            result := result + ' union ';
         result := result +
          ' select distinct  ''L'' TaxType, a.SY_GLOBAL_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, b.SY_LOCALS_NBR Nbr, a.AGENCY_NAME || ''-'' || b.NAME NAME, c.STATE, ''-'' PaymentTaxType, ''N'' HIDDEN  ' +
            ' from SY_LOCALS b, SY_STATES c, SY_GLOBAL_AGENCY a ' +
                  ' where {AsOfNow<a>} and ' +
                     ' {AsOfNow<b>} and {AsOfNow<c>} and ' +
                      ' b.SY_STATES_NBR = c.SY_STATES_NBR and a.SY_GLOBAL_AGENCY_NBR = b.SY_LOCAL_TAX_PMT_AGENCY_NBR ';
       end;
    end;
begin
         cdTaxFrom.Close;
         cdTaxTo.Close;
         with TExecDSWrapper.Create(create_Taxsql) do
         begin
           ctx_DataAccess.CUSTOM_VIEW.Close;
           ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
           ctx_DataAccess.CUSTOM_VIEW.Open;
           cdTaxFrom.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
           cdTaxTo.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
         end;
         cdTaxTo.EmptyDataSet;
         cdTaxFrom.LogChanges := false;
         cdTaxTo.LogChanges := false;
         CreateActivateIndex(cdTaxFrom,'NAME','NAME');
         CreateActivateIndex(cdTaxTo,'NBR','TaxType;Nbr');
         CreateActivateIndex(cdTaxTo,'NAME','NAME');
         cdTaxFrom.Filter :='HIDDEN = '''+ GROUP_BOX_NO+ '''';
         cdTaxFrom.Filtered := True;
         cdTaxFrom.First;
end;


procedure TProcessTaxPayments.Get_cdAgencyTaxData(const bLink :Boolean = false);
var
  s, sTaxSql, sFilter : string;
  bFilter : Boolean;
  pList: TStringList;

    procedure CreateActivateIndex(const d: TevClientDataset; const IName, Fields: string);
    begin
        d.AddIndex(IName, Fields, []);
        d.IndexDefs.Update;
        d.IndexName := IName;
    end;

    procedure CreateTaxLinkDataset;
    begin
      s :=
            'select distinct a.SY_STATE_TAX_PMT_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, ''S'' TaxType, a.SY_STATES_NBR Nbr, a.STATE, '''' Prefix, ''-'' PaymentTaxType ' +
             ' from SY_STATES a where {AsOfNow<a>} and a.SY_STATE_TAX_PMT_AGENCY_NBR is not null ' +
            'union ' +
            'select distinct  a.SY_SUI_TAX_PMT_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, ''U'' TaxType, a.SY_SUI_NBR, b.STATE, '''' Prefix, ''-'' PaymentTaxType  ' +
                 ' from SY_SUI a, SY_STATES b ' +
                   ' where {AsOfNow<a>} and {AsOfNow<b>}  and ' +
                           '  b.SY_STATES_NBR = a.SY_STATES_NBR and  a.SY_SUI_TAX_PMT_AGENCY_NBR is not null ' +
            'union ' +
            'select a.SY_GLOBAL_AGENCY_NBR, ''F'' TaxType, a.SY_FED_TAX_PAYMENT_AGENCY_NBR Nbr , ''-'' State, ''-- 941'' Prefix, ''FOAMCEB5D'' PaymentTaxType  ' +
                ' from SY_FED_TAX_PAYMENT_AGENCY a where {AsOfNow<a>} ' +
            'union '+
            'select a.SY_GLOBAL_AGENCY_NBR, ''F'' TaxType, a.SY_FED_TAX_PAYMENT_AGENCY_NBR Nbr ,''-'' State, ''-- 940'' Prefix, ''U'' PaymentTaxType ' +
                ' from SY_FED_TAX_PAYMENT_AGENCY a where {AsOfNow<a>} ' +
            'union ' +
            'select distinct b.SY_LOCAL_TAX_PMT_AGENCY_NBR SY_GLOBAL_AGENCY_NBR, ''L'' TaxType,  b.SY_LOCALS_NBR Nbr, c.STATE, '''' Prefix, ''-'' PaymentTaxType ' +
                 ' from SY_LOCALS b, SY_STATES c, SY_GLOBAL_AGENCY a ' +
                       ' where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and b.SY_STATES_NBR = c.SY_STATES_NBR and ' +
                          ' b.SY_LOCAL_TAX_PMT_AGENCY_NBR = a.SY_GLOBAL_AGENCY_NBR and ' +
                             ' ( a.AGENCY_TYPE = ''N'' or (a.AGENCY_TYPE = ''Y'' and b.LOCAL_TYPE in (''C'', ''N'', ''R'') ) ) ' +
            'union ' +
            'select distinct 0 SY_GLOBAL_AGENCY_NBR, ''L'' TaxType,  b.SY_LOCALS_NBR Nbr, c.STATE, '''' Prefix, ''-'' PaymentTaxType ' +
               ' from SY_GLOBAL_AGENCY a, SY_LOCALS b, SY_STATES c ' +
                  ' where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                        ' b.SY_STATES_NBR = c.SY_STATES_NBR and a.AGENCY_TYPE = ''Y'' and b.LOCAL_TYPE in (''S'', ''R'',''N'') and a.STATE = c.STATE ';

       cdAgencyTaxLink.Close;
       with TExecDSWrapper.Create(s) do
       begin
         ctx_DataAccess.CUSTOM_VIEW.Close;
         ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
         ctx_DataAccess.CUSTOM_VIEW.Open;
         cdAgencyTaxLink.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
       end;
       CreateActivateIndex(cdAgencyTaxLink,'NBR','SY_GLOBAL_AGENCY_NBR;TaxType;Nbr');

       s :=' select distinct a.SY_GLOBAL_AGENCY_NBR, c.STATE from SY_GLOBAL_AGENCY a, SY_LOCALS b, SY_STATES c ' +
                 ' where {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                      ' b.SY_STATES_NBR = c.SY_STATES_NBR and a.AGENCY_TYPE = ''Y'' and  a.STATE = c.STATE ';
       cdTCDAgencyList.Close;
       with TExecDSWrapper.Create(s) do
       begin
         ctx_DataAccess.CUSTOM_VIEW.Close;
         ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
         ctx_DataAccess.CUSTOM_VIEW.Open;
         cdTCDAgencyList.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
       end;
       CreateActivateIndex(cdTCDAgencyList,'NBR','SY_GLOBAL_AGENCY_NBR');

    end;

begin

  ctx_StartWait;
  cdAgencyFrom.DisableControls;
  cdAgencyTo.DisableControls;
  cdTaxFrom.DisableControls;
  cdTaxTo.DisableControls;
  try
      if not cdAgencyTaxLink.Active then
         CreateTaxLinkDataset;

      if not bLink then
      begin
         bFilter := True;
         sFilter := '';
         sTaxSql := '';
         case cbTaxTable.ItemIndex of
            1 : sFilter := 'TaxType = ''F''';
            2 : sFilter := 'TaxType = ''S''';
            3 : sFilter := 'TaxType = ''U''';
            4 : sFilter := 'TaxType = ''L''';
          else
                bFilter := false;
         end;
         cdAgencyTaxLink.Filter := sFilter;
         cdAgencyTaxLink.Filtered := bFilter;
         if sFilter <> '' then
         begin
           pList := cdAgencyTaxLink.GetFieldValueList('SY_GLOBAL_AGENCY_NBR',True);
           try
              sFilter := ' and ' + BuildINsqlStatement('SY_GLOBAL_AGENCY_NBR', pList.CommaText)
           finally
              pList.Free;
           end;
         end;
         s := 'select a.AGENCY_NAME NAME,  a.SY_GLOBAL_AGENCY_NBR,  a.AGENCY_TYPE, a.STATE, ''N'' HIDDEN ' +
                 ' from SY_GLOBAL_AGENCY a ' +
                       ' where {AsOfNow<a>} ' + sFilter +
                       ' order by a.AGENCY_NAME ';

         cdAgencyFrom.Close;
         cdAgencyTo.Close;
         with TExecDSWrapper.Create(s) do
         begin
           ctx_DataAccess.CUSTOM_VIEW.Close;
           ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
           ctx_DataAccess.CUSTOM_VIEW.Open;
           cdAgencyFrom.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
           cdAgencyTo.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
         end;
         cdAgencyTo.EmptyDataSet;
         cdAgencyFrom.LogChanges := false;
         cdAgencyTo.LogChanges := false;

         CreateActivateIndex(cdAgencyFrom,'NAME','NAME');
         CreateActivateIndex(cdAgencyTo,'NAME','NAME');
         cdAgencyFrom.Filter :='HIDDEN = '''+ GROUP_BOX_NO+ '''';
         cdAgencyFrom.Filtered := true;
         cdAgencyFrom.First;

         Get_cdTaxData;   //  Grid Tax Data

         if grdAgencyFrom.Selected.Count = 0 then
         begin
           grdAgencyFrom.Selected.Add('NAME'+#9+IntToStr(45)+#9+'Agency Name'+#9+'T');
           grdAgencyTo.Selected.Add('NAME'+#9+IntToStr(45)+#9+'Agency Name'+#9+'T');
           grdTaxFrom.Selected.Add('NAME'+#9+IntToStr(45)+#9+'Tax Name'+#9+'T');
           grdTaxTo.Selected.Add('NAME'+#9+IntToStr(45)+#9+'Tax Name'+#9+'T');
         end;
      end;
  finally
    bgrdAgencyControl := True;
    cdAgencyFrom.EnableControls;
    cdAgencyTo.EnableControls;
    cdTaxFrom.EnableControls;
    cdTaxTo.EnableControls;
    ctx_EndWait;
  end;

end;


procedure TProcessTaxPayments.pcAvailChange(Sender: TObject);
begin
  inherited;
  if pcAvail.ActivePage = tsByTaxes then
     Update_cdTaxTo;
end;

procedure TProcessTaxPayments.bbAddClick(Sender: TObject);
begin
  inherited;
  if pcAvail.ActivePage = tsByAgency then
    addGridClick(cdAgencyFrom,cdAgencyTo, grdAgencyFrom, grdAgencyto)
  else
    addGridClick(cdTaxFrom,cdTaxTo, grdTaxFrom, grdTaxTo);
end;

procedure TProcessTaxPayments.bbRemoveClick(Sender: TObject);
begin
  inherited;
  if pcAvail.ActivePage = tsByAgency then
    deleteGridClick(cdAgencyFrom,cdAgencyTo, grdAgencyFrom, grdAgencyto)
  else
    deleteGridClick(cdTaxFrom,cdTaxTo, grdTaxFrom, grdTaxTo);
end;

procedure TProcessTaxPayments.btnAddAllClick(Sender: TObject);
begin
  inherited;
  if pcAvail.ActivePage = tsByAgency then
    addGridClick(cdAgencyFrom,cdAgencyTo, grdAgencyFrom, grdAgencyto, True)
  else
    addGridClick(cdTaxFrom,cdTaxTo, grdTaxFrom, grdTaxTo, True);
end;

procedure TProcessTaxPayments.cdAddProcess(var cdFrom, cdTo : TevClientDataSet; const bAgency : Boolean= false);
var
  i: Integer;
  bAppend : Boolean;
begin
    bAppend := True;
    if bAgency and (cdFrom.FieldByName('TaxType').AsString = 'L') then
    begin
        if cdTo.Locate('TaxType;Nbr', cdFrom['TaxType;Nbr'], []) then
           bAppend := false;
    end;
    if bAppend then
    begin
      cdTo.Append;
      for i := 0 to cdTo.FieldCount-1 do
        if cdTo.Fields[i].FieldKind in [fkData, fkInternalCalc] then
            cdTo.Fields[i].Value := cdFrom[cdTo.Fields[i].FieldName];
      cdTo.Post;

      cdFrom.Edit;
      cdFrom['HIDDEN'] := GROUP_BOX_YES;
      cdFrom.Post;
    end;
end;

procedure TProcessTaxPayments.Update_cdTaxTo;
var
  bTCDAgencyProcessed :Boolean;

  procedure moveTaxsprocess(const sFilter : String);
  begin
       cdAgencyTaxLink.Filter := sFilter;
       cdAgencyTaxLink.Filtered := True;
       cdAgencyTaxLink.First;
       while not cdAgencyTaxLink.Eof do
       begin
          if cdTaxFrom.Locate('TaxType;Nbr', cdAgencyTaxLink['TaxType;Nbr'], []) then
             cdAddProcess(cdTaxFrom, cdTaxTo, True);
          cdAgencyTaxLink.Next;
       end;
  end;

begin
    if not bgrdAgencyControl then
       exit;

     ctx_StartWait;
     cdTaxFrom.DisableControls;
     cdTaxTo.DisableControls;
     cdAgencyFrom.DisableControls;
     cdAgencyTo.DisableControls;
     try
       Get_cdTaxData;

       bTCDAgencyProcessed := false;
       cdAgencyTo.First;
       while not cdAgencyTo.Eof do
       begin
           if cdTCDAgencyList.Locate('SY_GLOBAL_AGENCY_NBR', cdAgencyTo.fieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger,[]) then
           begin
             if not bTCDAgencyProcessed then
             begin
              moveTaxsprocess('SY_GLOBAL_AGENCY_NBR = 0');
              bTCDAgencyProcessed := True;
             end;
           end;
           moveTaxsprocess('SY_GLOBAL_AGENCY_NBR= ' +  cdAgencyTo.fieldByName('SY_GLOBAL_AGENCY_NBR').AsString);
           cdAgencyTo.Next;
       end;
     finally
       bgrdAgencyControl := false;
       grdTaxFrom.UnselectAll;
       grdTaxTo.UnselectAll;

       cdTaxFrom.EnableControls;
       cdTaxTo.EnableControls;
       cdAgencyFrom.EnableControls;
       cdAgencyTo.EnableControls;
       cdAgencyTaxLink.Filtered := false;
       ctx_EndWait;
     end;
end;

procedure TProcessTaxPayments.addGridClick(var cdFrom,cdTo :TevClientDataSet; var grdFrom, grdTo: TevDBGrid; const bMoveAll : Boolean=false);
var
  i: Integer;
  iRecNo: Integer;
begin
  ctx_StartWait;
  try
      if bMoveAll then
        grdFrom.SelectAll;

      iRecNo := cdFrom.RecNo;
      if cdFrom.RecordCount > 0 then
      begin
        cdFrom.DisableControls;
        try
          if grdFrom.SelectedList.Count > 1 then
          begin
            for i := grdFrom.SelectedList.Count-1 downto 0 do
            begin
              grdFrom.DataSource.DataSet.GotoBookmark(grdFrom.SelectedList[i]);
              cdAddProcess(cdFrom, cdTo);
            end;
          end
          else
            cdAddProcess(cdFrom, cdTo);
        finally
          cdFrom.EnableControls;
        end;
        grdFrom.UnselectAll;
        cdFrom.RecNo := iRecNo;
        grdFrom.SelectRecord;
      end;
  finally
    if pcAvail.ActivePage = tsByAgency then
       bgrdAgencyControl := True;
    ctx_EndWait;
  end;
end;

procedure TProcessTaxPayments.deleteGridClick(var cdFrom ,cdTo : TevClientDataSet; grdFrom, grdTo: TevDBGrid);
var
  i: Integer;
  cd: TevClientDataSet;

  procedure cdDeleteProcess;
  begin
      if cd.Locate('NAME', cdTo['NAME'], []) then
      begin
        cd.Edit;
        cd['HIDDEN'] := GROUP_BOX_NO;
        cd.Post;
      end;
      cdTo.Delete;
  end;


begin
  ctx_StartWait;
  try
      if cdTo.RecordCount > 0 then
      begin
        cd := TevClientDataSet.Create(nil);
        cd.CloneCursor(cdFrom, True);
        cdFrom.DisableControls;
        try
          if grdTo.SelectedList.Count > 1 then
          begin
            for i := grdTo.SelectedList.Count-1 downto 0 do
            begin
              grdTo.DataSource.DataSet.GotoBookmark(grdTo.SelectedList[i]);
              cdDeleteProcess;
            end;
          end
          else
              cdDeleteProcess;
        finally
          cdFrom.EnableControls;
          grdFrom.UnselectAll;
          cdFrom.Locate('NAME', cd['NAME'], []);
          grdFrom.Refresh;
          cd.Free;
          grdTo.UnselectAll;
        end;
      end;
  finally
    if pcAvail.ActivePage = tsByAgency then
       bgrdAgencyControl := True;
    ctx_EndWait;
  end;
end;

initialization
  RegisterClass(TPROCESSTAXPAYMENTS);

end.
