inherited MarkEftdLiabilities: TMarkEftdLiabilities
  object Label1: TevLabel [0]
    Left = 312
    Top = 16
    Width = 265
    Height = 13
    Caption = 'Select EFTP file you want to mark as paid and hit button'
  end
  object wwDBGrid1: TevDBGrid [1]
    Left = 16
    Top = 16
    Width = 281
    Height = 433
    DisableThemesInTitle = False
    Selected.Strings = (
      'Reference'#9'20'#9'Reference'#9'F'
      'Date_'#9'10'#9'Date'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TMarkEftdLiabilities\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = ds
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object Button1: TevBitBtn [2]
    Left = 312
    Top = 40
    Width = 105
    Height = 25
    Caption = 'Mark as paid'
    TabOrder = 1
    OnClick = Button1Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDD88888888888888DDFFFFFFFFFFFFFFD222222222222
      228D88888888888888FD27FFF7A7FFFFF28D8FDDDDFDDDDDD8FD27FF7A2AFFFF
      F28D8FDDDF8FDDDDD8FD27F7A22A7FFFF28D8FDDF88FDDDDD8FD277A2222AFFF
      F28D8FDF8888FDDDD8FD27A22772A7FFF28D8FF88DD8FDDDD8FD2A27FFF22AFF
      F28D8F8DDDD88FDDD8FD277FFFF72A7FF28D8FDDDDDD8FDDD8FD27FFFFFF22AF
      F28D8FDDDDDD88FDD8FD27FFFFFF72A7F28D8FDDDDDDD8FDD8FD27FFFFFFF22A
      F28D8FDDDDDDD88FD8FD27FFFFFFF72A728D8FDDDDDDDD8FD8FD277777777722
      A28D8FFFFFFFFF88F8FD22222222222222DD88888888888888DD}
    NumGlyphs = 2
  end
  object cd: TevClientDataSet
    FieldDefs = <
      item
        Name = 'Reference'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Date_'
        DataType = ftDateTime
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end>
    Left = 8
    Top = 16
    object cdReference: TStringField
      DisplayWidth = 20
      FieldName = 'Reference'
    end
    object cdDate_: TDateTimeField
      DisplayWidth = 18
      FieldName = 'Date_'
    end
    object cdCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object cdCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
  end
  object ds: TevDataSource
    DataSet = cd
    Left = 48
    Top = 16
  end
  object cdTemp: TevClientDataSet
    Left = 8
    Top = 48
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 392
    Top = 80
  end
end
