// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PROCESS_CLEAR_CREDIT_LIAB;

interface

uses
  SFrameEntry,  Db,  Controls, Grids,
  Wwdbigrd, Wwdbgrid, StdCtrls, ComCtrls, Classes, Wwdatsrc, Graphics,
  Buttons, Variants, EvBasicUtils, kbmMemTable, ISKbmMemDataSet, EvContext,
  ISBasicClasses, ISDataAccessComponents, EvDataAccessComponents, EvUIComponents, EvClientDataSet;

type
  TPROCESS_CLEAR_CREDIT_LIAB = class(TFrameEntry)
    cdClCo: TevClientDataSet;
    cdClCoCL_NBR: TIntegerField;
    cdClCoCUSTOM_CLIENT_NUMBER: TStringField;
    cdClCoCLIENT_NAME: TStringField;
    cdClCoCO_NBR: TIntegerField;
    cdClCoCUSTOM_COMPANY_NUMBER: TStringField;
    cdClCoCOMPANY_NAME: TStringField;
    cdClCoTAX_SERVICE: TStringField;
    dsClCo: TevDataSource;
    Pc: TevPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    cbQuarter: TevComboBox;
    cbYear: TevEdit;
    UpDown1: TUpDown;
    wwDBGrid1: TevDBGrid;
    cdTaxes: TevClientDataSet;
    dsTaxes: TevDataSource;
    cdTaxesNUMBER: TStringField;
    cdTaxesNAME: TStringField;
    cdTaxesTaxType: TStringField;
    cdTaxesTaxDesc: TStringField;
    cdTaxesTotalCalc: TCurrencyField;
    cdTaxesDo: TBooleanField;
    wwDBGrid2: TevDBGrid;
    cdTaxescl_nbr: TIntegerField;
    cdTaxesco_nbr: TIntegerField;
    cdTaxesNbr: TIntegerField;
    cdTaxesDone: TBooleanField;
    cdClCoLOAD: TBooleanField;
    cdClCoDESCRI: TStringField;
    Button4: TevBitBtn;
    Button5: TevBitBtn;
    Button1: TevBitBtn;
    Button3: TevBitBtn;
    Button2: TevBitBtn;
    procedure wwDBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure UpDown1Click(Sender: TObject; Button: TUDBtnType);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    FBegDate, FEndDate: TDateTime;
    OpenFilter: string;
    procedure UpdateAll(const Value: Boolean);
  public
    { Public declarations }
    procedure Activate; override;
  end;

var
  PROCESS_CLEAR_CREDIT_LIAB: TPROCESS_CLEAR_CREDIT_LIAB;

implementation

{$R *.DFM}

uses
  EvUtils, SFieldCodeValues, EvConsts, SDataStructure, SysUtils, isBaseClasses;

procedure TPROCESS_CLEAR_CREDIT_LIAB.wwDBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  {if ConvertNull(cdClCo['LOAD'], False) then
  begin
    ABrush.Color := clYellow;
    if (ConvertNull(cdClCo['FUI_DO'], False) and (ConvertNull(cdClCo['FUI_DIFF'], 0) <> 0))
    or (ConvertNull(cdClCo['SUI_DO'], False) and (ConvertNull(cdClCo['SUI_DIFF'], 0) <> 0)) then
      ABrush.Color := clRed;
  end;}
end;

procedure TPROCESS_CLEAR_CREDIT_LIAB.UpDown1Click(Sender: TObject;
  Button: TUDBtnType);
begin
  inherited;
  cbYear.Text := IntToStr(UpDown1.Position);
end;

procedure TPROCESS_CLEAR_CREDIT_LIAB.Button1Click(Sender: TObject);
  procedure Process(const ClNbr, CoNbr: Integer);
    procedure AddLine(const TaxType: string;
      const Nbr: Variant; const TaxDesc: string;
      const Amount: Currency);
    begin
      if cdTaxes.Locate('cl_nbr;co_nbr;TaxType;Nbr', VarArrayOf([ClNbr, CoNbr, TaxType, Nbr]), []) then
      begin
        cdTaxes.Edit;
        cdTaxes['Amount'] := cdTaxes['Amount']+ Amount;
      end
      else
      begin
        cdTaxes.Append;
        cdTaxes['number'] := DM_COMPANY.CO['CUSTOM_COMPANY_NUMBER'];
        cdTaxes['Name'] := DM_COMPANY.CO['NAME'];
        cdTaxes['TaxType'] := TaxType;
        cdTaxes['Nbr'] := Nbr;
        cdTaxes['TaxDesc'] := TaxDesc;
        cdTaxes['Do'] := False;
        cdTaxes['Done'] := False;
        cdTaxes['cl_nbr'] := ClNbr;
        cdTaxes['co_nbr'] := CoNbr;
        cdTaxes['Amount'] := Amount;
      end;
      cdTaxes.Post;
    end;
  begin
    if DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []) then
    begin
      DM_COMPANY.CO_FED_TAX_LIABILITIES.DataRequired('CO_NBR = '+ IntToStr(CoNbr)+ ' and '+
        OpenFilter);
      with DM_COMPANY.CO_FED_TAX_LIABILITIES do
      begin
        First;
        while not Eof do
        begin
          if FieldValues['TAX_TYPE'] <> TAX_LIABILITY_TYPE_ER_FUI then
            AddLine('F941' , Null, '941', FieldValues['AMOUNT'])
          else
            AddLine('F940' , Null, '940', FieldValues['AMOUNT']);
          Next;
        end;
      end;
      DM_COMPANY.CO_STATES.DataRequired('ALL');
      DM_COMPANY.CO_STATE_TAX_LIABILITIES.DataRequired('CO_NBR = '+ IntToStr(CoNbr)+ ' and '+
        OpenFilter);
      with DM_COMPANY, DM_COMPANY.CO_STATE_TAX_LIABILITIES do
      begin
        First;
        while not Eof do
        begin
          if CO_STATES.Locate('CO_STATES_NBR', FieldValues['CO_STATES_NBR'], []) then
            AddLine('S'+ FieldValues['TAX_TYPE'] , FieldValues['CO_STATES_NBR'], CO_STATES['State']+ '- '+ ReturnDescription(StateTaxLiabilityType_ComboChoices, FieldValues['TAX_TYPE']), FieldValues['AMOUNT']);
          Next;
        end;
      end;
      DM_COMPANY.CO_SUI.DataRequired('ALL');
      DM_COMPANY.CO_SUI_LIABILITIES.DataRequired('CO_NBR = '+ IntToStr(CoNbr)+ ' and '+
        OpenFilter);
      with DM_COMPANY, DM_COMPANY.CO_SUI_LIABILITIES do
      begin
        First;
        while not Eof do
        begin
          if CO_SUI.Locate('CO_SUI_NBR', FieldValues['CO_SUI_NBR'], []) then
            AddLine('U', FieldValues['CO_SUI_NBR'], CO_SUI['Description'], FieldValues['AMOUNT']);
          Next;
        end;
      end;
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('ALL');
      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.DataRequired('CO_NBR = '+ IntToStr(CoNbr)+ ' and '+
        OpenFilter);
      with DM_COMPANY, DM_SYSTEM_LOCAL, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES do
      begin
        First;
        while not Eof do
        begin
          if CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', FieldValues['CO_LOCAL_TAX_NBR'], [])
          and SY_LOCALS.Locate('SY_LOCALS_NBR', CO_LOCAL_TAX['SY_LOCALS_NBR'], []) then
            AddLine('L', FieldValues['CO_LOCAL_TAX_NBR'], SY_LOCALS['NAME'], FieldValues['AMOUNT']);
          Next;
        end;
      end;
    end;
  end;
var
  CurrClNbr: Integer;
begin
  cdClCo.CheckBrowseMode;
  FBegDate := EncodeDate(StrToInt(cbYear.Text), cbQuarter.ItemIndex*3+ 1, 1);
  if cbQuarter.ItemIndex <> 3 then
    FEndDate := EncodeDate(StrToInt(cbYear.Text), cbQuarter.ItemIndex*3+ 4, 1)-1
  else
    FEndDate := EncodeDate(StrToInt(cbYear.Text), 12, 31);
  OpenFilter := 'CHECK_DATE between '''+ DateToStr(FBegDate)+ ''' and '''+ DateToStr(FEndDate)+ ''' and '+
    '(STATUS = '''+ TAX_DEPOSIT_STATUS_PENDING+ ''' or STATUS = '''+ TAX_DEPOSIT_STATUS_IMPOUNDED+ ''') and '+
    'ADJUSTMENT_TYPE = '''+ TAX_LIABILITY_ADJUSTMENT_TYPE_CREDIT+ '''';
  cdTaxes.Close;
  cdTaxes.CreateDataSet;
  inherited;
  CurrClNbr := -1;
  with TevClientDataSet.Create(nil) do
  try
    CloneCursor(cdClCo, False);
    IndexFieldNames := 'CL_NBR';
    First;
    while not Eof do
    begin
      if ConvertNull(FieldValues['LOAD'], False) then
      begin
        ctx_StartWait('Loading client '+ FieldValues['CLIENT_NAME']);
        try
          if CurrClNbr <> FieldValues['CL_NBR'] then
          begin
            ctx_DataAccess.OpenClient(FieldValues['CL_NBR']);
            DM_COMPANY.CO.DataRequired('ALL');
            CurrClNbr := FieldValues['CL_NBR'];
          end;
          Process(FieldValues['CL_NBR'], FieldValues['CO_NBR']);
        finally
          ctx_EndWait;
        end;
      end;
      Next;
    end;
  finally
    Free;
  end;
  cdTaxes.MergeChangeLog;
  Pc.ActivePage := ts2;
  ts2.TabVisible := True;
  ts1.TabVisible := False;
end;

procedure TPROCESS_CLEAR_CREDIT_LIAB.Button3Click(Sender: TObject);
begin
  inherited;
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
end;

procedure TPROCESS_CLEAR_CREDIT_LIAB.Button2Click(Sender: TObject);
var
  CurrClNbr: Integer;
  procedure Mark(const cd: TevClientDataSet; const Filter: string);
  begin
    cd.IndexName := '';
    cd.Filter := Filter;
    cd.Filtered := True;
    try
      cd.First;
      while not cd.Eof do
      begin
        cd.Edit;
        cd['status'] := TAX_DEPOSIT_STATUS_PAID;
        cd.Post;
        cd.Next;
      end;
    finally
      cd.Filtered := False;
    end;
  end;
  procedure Apply;
  begin
    if CurrClNbr <> -1 then
    try
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_FED_TAX_LIABILITIES,
        DM_COMPANY.CO_STATE_TAX_LIABILITIES,
        DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,
        DM_COMPANY.CO_SUI_LIABILITIES]);
      cdTaxes.MergeChangeLog;
    except
      cdTaxes.CancelUpdates;
      raise;
    end;
  end;
begin
  inherited;
  cdTaxes.CheckBrowseMode;
  CurrClNbr := -1;
  ctx_StartWait('Marking...');
  with TevClientDataSet.Create(nil) do
  try
    CloneCursor(cdTaxes, False);
    IndexFieldNames := 'CL_NBR';
    Resync([]);
    First;
    while not Eof do
    begin
      if FieldValues['DO'] then
      begin
        if CurrClNbr <> FieldValues['CL_NBR'] then
        begin
          Apply;
          ctx_DataAccess.OpenClient(FieldValues['CL_NBR']);
          DM_COMPANY.CO_FED_TAX_LIABILITIES.DataRequired(OpenFilter);
          DM_COMPANY.CO_STATE_TAX_LIABILITIES.DataRequired(OpenFilter);
          DM_COMPANY.CO_SUI_LIABILITIES.DataRequired(OpenFilter);
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.DataRequired(OpenFilter);
          CurrClNbr := FieldValues['CL_NBR'];
        end;
        if string(FieldValues['TaxType'])[1] = 'F' then
        begin
          if Copy(string(FieldValues['TaxType']), 2, 3) = '940' then
            Mark(DM_COMPANY.CO_FED_TAX_LIABILITIES, 'TAX_TYPE = '''+ TAX_LIABILITY_TYPE_ER_FUI+ '''')
          else
            Mark(DM_COMPANY.CO_FED_TAX_LIABILITIES, 'TAX_TYPE <> '''+ TAX_LIABILITY_TYPE_ER_FUI+ '''');
        end
        else
        if string(FieldValues['TaxType'])[1] = 'S' then
          Mark(DM_COMPANY.CO_STATE_TAX_LIABILITIES, 'CO_STATES_NBR = '+ IntToStr(FieldValues['nbr'])+
            ' and TAX_TYPE = '''+ string(FieldValues['TaxType'])[2]+ '''')
        else
        if string(FieldValues['TaxType'])[1] = 'U' then
          Mark(DM_COMPANY.CO_SUI_LIABILITIES, 'CO_SUI_NBR = '+ IntToStr(FieldValues['nbr']))
        else
        if string(FieldValues['TaxType'])[1] = 'L' then
          Mark(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES, 'CO_LOCAL_TAX_NBR = '+ IntToStr(FieldValues['nbr']))
        else
          Assert(False);
        Edit;
        FieldValues['Done'] := True;
        Post;
      end;
      Next;
    end;
    Apply;
  finally
    Free;
    ctx_EndWait;
  end;
  UpdateAll(False);
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
end;

procedure TPROCESS_CLEAR_CREDIT_LIAB.Button4Click(Sender: TObject);
begin
  inherited;
  UpdateAll(True);
end;

procedure TPROCESS_CLEAR_CREDIT_LIAB.Button5Click(Sender: TObject);
begin
  inherited;
  UpdateAll(False);
end;

procedure TPROCESS_CLEAR_CREDIT_LIAB.UpdateAll(const Value: Boolean);
begin
  with TevClientDataSet.Create(nil) do
  try
    CloneCursor(cdClCo, False);
    First;
    while not Eof do
    begin
      Edit;
      FieldValues['LOAD'] := Value;
      Post;
      Next;
    end;
  finally
    Free
  end;
end;

procedure TPROCESS_CLEAR_CREDIT_LIAB.wwDBGrid1DblClick(Sender: TObject);
var
  d: TDateTime;
begin
  inherited;
  Exit;
  ctx_DataAccess.OpenClient(cdClCo['cl_nbr']);
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    Close;
    with TExecDSWrapper.Create('ConsolidationQuarterSuiWages') do
    begin
      d := StrToDateTime('1/1/2000');
      SetParam('BegDate', d);
      d := StrToDateTime('3/1/2000');
      SetParam('EndDate', d);
      SetParam('CoNbr', cdClCo['co_nbr']);
      DataRequest(AsVariant);
    end;
    Open;
  end;
end;

procedure TPROCESS_CLEAR_CREDIT_LIAB.Activate;
var
  i: Integer;
  f: TField;
begin
  inherited;
  cdClCo.CreateDataSet;
  with ctx_DataAccess.TEMP_CUSTOM_VIEW do
  begin
    Close;
    with TExecDSWrapper.Create('ConsolidationsFull') do
    begin
      SetMacro('CLCOFILTER', GetReadOnlyClintCompanyFilter(true,'c.','co.'));
      DataRequest(AsVariant);
    end;
    Open;
    First;
    while not Eof do
    begin
      cdClCo.Append;
      for i := 0 to cdClCo.FieldCount-1 do
      begin
        f := FindField(cdClCo.Fields[i].FieldName);
        if Assigned(f) then
          cdClCo.Fields[i].Assign(f);
      end;
      if VarIsNull(FieldValues['CL_CO_CONSOLIDATION_NBR']) then
        cdClCo['DESCR'] := 'Company'
      else
        cdClCo['DESCR'] := 'Consolidation';
      cdClCo.Post;
      Next;
    end;
    Close;
  end;
  wwDBGrid1.SelectRecord;
  cbQuarter.ItemIndex := GetQuarterNumber(Date)- 1;
  UpDown1.Position := GetYear(Date);
  cbYear.Text := IntToStr(UpDown1.Position);
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
end;

initialization
  RegisterClass(TPROCESS_CLEAR_CREDIT_LIAB);

end.
