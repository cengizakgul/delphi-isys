inherited EFTPS_Enrollment: TEFTPS_Enrollment
  inherited PageControl1: TevPageControl
    OnChange = PageControl1Change
    OnEnter = PageControl1Enter
    inherited TabSheet1: TTabSheet
      Caption = 'Enrollment'
      inherited Panel2: TevPanel
        Top = 73
        Height = 165
        inherited Splitter1: TevSplitter
          Height = 163
        end
        inherited wwdbgridSelectClient: TevDBGrid
          Height = 163
          IniAttributes.SectionName = 'TEFTPS_Enrollment\wwdbgridSelectClient'
          OnCalcCellColors = wwdbgridSelectClientCalcCellColors
          OnFilterChange = wwdbgridSelectClientFilterChange
        end
        inherited pnlSubbrowse: TevPanel
          Height = 163
          inherited Panel4: TevPanel
            Height = 163
            inherited wwDBGrid1: TevDBGrid
              Height = 161
              IniAttributes.SectionName = 'TEFTPS_Enrollment\wwDBGrid1'
              OnCalcCellColors = wwdbgridSelectClientCalcCellColors
            end
          end
        end
      end
      inherited Panel3: TevPanel
        Height = 73
        inherited bbtnCopyAll: TevBitBtn
          Left = 320
          Top = 40
          Height = 27
          TabOrder = 1
        end
        inherited bbtnRemoveAll: TevBitBtn
          Left = 416
          Top = 40
          Height = 27
          TabOrder = 2
        end
        object bbtnEnroll: TevBitBtn
          Left = 512
          Top = 40
          Width = 169
          Height = 27
          Caption = 'Process Selected Companies'
          TabOrder = 3
          OnClick = bbtnEnrollClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD0DD0DDDDD
            DDDDDDDFDDFDDDDDDDDDDD0E00E0DDDDDDDDDDF8FF8FDDDDDDDDDDEEEEEE8888
            8888DD888888FFFFFFFFD00EEEE007FFFFF8DFF8888FFD88888FDEEE07EEE7FF
            FFF8D888FD888D88888FDEEE00EEE74444F8D888FF888DDDDD8FDD0EEEE07FFF
            FFF8DDF8888FD888888FDDEEEEEE744444F8DD888888DDDDDD8FD03E77E7FFFF
            FFF8DFD8DD8D8888888FDBB33F74444444F8D88DD8DDDDDDDD8F00BB3FFFFFFF
            FFF8FF88D8888888888FBBB0DFF4444444F8888FD88DDDDDDD8FBBB00FFFFFFF
            FFF8888FF8888888888FD0BB3FF4444444F8DF88D88DDDDDDD8FDBBB3FFFFFFF
            FFF8D888D8888888888FDDBD3FFFFFFFFFFDDD8DD8888888888D}
          NumGlyphs = 2
        end
        object rgrpSelected: TevRadioGroup
          Left = 0
          Top = 34
          Width = 249
          Height = 33
          Caption = 'Select'
          Columns = 3
          ItemIndex = 2
          Items.Strings = (
            'All'
            'Enrolled'
            'Not Enrolled')
          TabOrder = 0
          OnClick = rgrpSelectedClick
        end
        object cbTaxOnly: TevCheckBox
          Left = 0
          Top = 9
          Width = 105
          Height = 17
          Caption = 'Show Tax Only'
          TabOrder = 4
          OnClick = cbTaxOnlyClick
        end
        object rgEnroll: TevRadioGroup
          Left = 320
          Top = 0
          Width = 361
          Height = 33
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Enroll'
            'CR Enroll'
            'Terminate'
            'CR Terminate')
          TabOrder = 5
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Payment Response'
      ImageIndex = 1
      object Label1: TevLabel
        Left = 16
        Top = 48
        Width = 73
        Height = 16
        Caption = 'Succeeded:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TevLabel
        Left = 328
        Top = 48
        Width = 58
        Height = 16
        Caption = 'Rejected:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object bbtnPaymentResponse: TevBitBtn
        Left = 16
        Top = 16
        Width = 137
        Height = 25
        Caption = 'Payment Response'
        TabOrder = 1
        OnClick = bbtnPaymentResponseClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
          8888DDDDDDDDDDDDDDDDD444444444444444D888888888888888D4FFFFFFFFFF
          FFF4D888888888888888D4FF7777777777F4D888DDDDDDDDDDD8D4F744444444
          44F4D88FFFF888888888D470000FFFF777F4D8F888888888DDD8D40BB0777774
          44F4DF8DD8FFFFFD888880BB0000000FFFF4F8DD8888888D88880BBBBBBBBB04
          44448DDDDDDDDD8DDDDDD0BB0000000DDDDDD8DD8888888DDDDDD80BB00BB08D
          DDDDDF8DD88DD8FDDDDDD0000000BB08DDDDD8888888DD8FDDDDD0BBBBBBBBB0
          DDDDD8DDDDDDDDD8DDDDD0000000BB0DDDDDD8888888DD8DDDDDDDDDD80BB0DD
          DDDDDDDDDF8DD8DDDDDDDDDDD0000DDDDDDDDDDDD8888DDDDDDD}
        NumGlyphs = 2
      end
      object bbtnEnrollmentResponse: TevBitBtn
        Left = 168
        Top = 16
        Width = 137
        Height = 25
        Caption = 'Enrollment Response'
        TabOrder = 0
        OnClick = bbtnEnrollmentResponseClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D66666666666
          6666DDDDDDDDDDDDD888FFFFFFFFFFFF6E66888888888888DD88F877787778FF
          66668D888D888D88D888F878788788FF6E668DDDDDDDDD88DD88F878888788FF
          66668DDFFFFDDD88D888F880000788FF6E6688F888888888DD88F70BB077777F
          66668F8DD8FFFFF8D88880BB0000000EEE66F8DD8888888DDD880BBBBBBBBB06
          666D8DDDDDDDDD8D888DD0BB0000000DD6DDD8DD8888888DD8DDD80BB00BB08D
          D6DDDF8DD88DD8FDD8DDD0000000BB08D6DDD8888888DD8FD8DDD0BBBBBBBBB0
          D6DDD8DDDDDDDDD8D8DDD0000000BB0EE6DDD8888888DD8DD8DDDDDDD80BB0EE
          E6DDDDDDDF8DD8DDD8DDDDDDD0000DDDE6DDDDDDD8888DDD88DD}
        NumGlyphs = 2
      end
      object lboxSucceeded: TListBox
        Left = 16
        Top = 72
        Width = 289
        Height = 313
        ItemHeight = 13
        TabOrder = 2
      end
      object lboxRejected: TListBox
        Left = 328
        Top = 72
        Width = 289
        Height = 313
        ItemHeight = 13
        TabOrder = 3
      end
      object bbtnPrintRejections: TevBitBtn
        Left = 504
        Top = 16
        Width = 115
        Height = 25
        Caption = 'Print Rejections'
        TabOrder = 4
        OnClick = bbtnPrintRejectionsClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
          08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
          8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
          8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
          8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
          78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
          7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
          DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
          DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
        NumGlyphs = 2
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Enrollment Response'
      ImageIndex = 2
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 569
        Height = 201
        Align = alTop
        Caption = 'Panel7'
        TabOrder = 0
        object Splitter3: TSplitter
          Left = 346
          Top = 1
          Height = 199
        end
        object Panel1: TPanel
          Left = 1
          Top = 1
          Width = 345
          Height = 199
          Align = alLeft
          BevelOuter = bvLowered
          Caption = 'Panel1'
          TabOrder = 0
          object wwgdFile: TevDBGrid
            Left = 1
            Top = 21
            Width = 343
            Height = 177
            DisableThemesInTitle = False
            Selected.Strings = (
              'TaxPayerTIN'#9'8'#9'Taxpayertin'
              'TaxPayerName'#9'25'#9'Taxpayername'
              'FileDate'#9'8'#9'Filedate'
              'TaxPayerPin'#9'10'#9'Taxpayerpin')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEFTPS_Enrollment\wwgdFile'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = evDsFile
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
          end
          object StaticText2: TStaticText
            Left = 1
            Top = 1
            Width = 35
            Height = 20
            Align = alTop
            Alignment = taCenter
            Caption = 'File...'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 1
          end
        end
        object Panel5: TPanel
          Left = 349
          Top = 1
          Width = 219
          Height = 199
          Align = alClient
          BevelOuter = bvLowered
          Caption = 'Panel5'
          TabOrder = 1
          object StaticText3: TStaticText
            Left = 1
            Top = 1
            Width = 59
            Height = 20
            Align = alTop
            Alignment = taCenter
            Caption = 'Evolution'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 0
          end
          object wwgdEvolution: TevDBGrid
            Left = 1
            Top = 21
            Width = 149
            Height = 177
            DisableThemesInTitle = False
            Selected.Strings = (
              'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom Company Number'
              'NAME'#9'36'#9'Name'
              'FEIN'#9'9'#9'Fein'
              'EFTPS_ENROLLMENT_STATUS'#9'18'#9'Eftps Enrollment Status')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEFTPS_Enrollment\wwgdEvolution'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = evDsEvo
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
          end
          object Panel15: TPanel
            Left = 150
            Top = 21
            Width = 68
            Height = 177
            Align = alRight
            BevelOuter = bvLowered
            TabOrder = 2
            DesignSize = (
              68
              177)
            object btnAdd: TBitBtn
              Left = 2
              Top = 99
              Width = 62
              Height = 74
              Anchors = [akLeft, akTop, akRight, akBottom]
              Caption = '&Add'
              Default = True
              ModalResult = 6
              TabOrder = 0
              OnClick = btnAddClick
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
                DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
                888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
                A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
                A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
              Layout = blGlyphBottom
              NumGlyphs = 2
            end
            object btnEnrolmentFile: TevBitBtn
              Left = 3
              Top = 4
              Width = 60
              Height = 77
              Caption = '&File'
              TabOrder = 1
              OnClick = btnEnrolmentFileClick
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                8888DDDDDDDDDDDDDDDDD444444444444444D888888888888888D4FFFFFFFFFF
                FFF4D888888888888888D4FF7777777777F4D888DDDDDDDDDDD8D4F744444444
                44F4D88FFFF888888888D470000FFFF777F4D8F888888888DDD8D40BB0777774
                44F4DF8DD8FFFFFD888880BB0000000FFFF4F8DD8888888D88880BBBBBBBBB04
                44448DDDDDDDDD8DDDDDD0BB0000000DDDDDD8DD8888888DDDDDD80BB00BB08D
                DDDDDF8DD88DD8FDDDDDD0000000BB08DDDDD8888888DD8FDDDDD0BBBBBBBBB0
                DDDDD8DDDDDDDDD8DDDDD0000000BB0DDDDDD8888888DD8DDDDDDDDDD80BB0DD
                DDDDDDDDDF8DD8DDDDDDDDDDD0000DDDDDDDDDDDD8888DDDDDDD}
              NumGlyphs = 2
            end
          end
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 201
        Width = 427
        Height = 150
        Align = alTop
        Caption = 'Panel8'
        TabOrder = 1
        object Panel6: TPanel
          Left = 1
          Top = 1
          Width = 495
          Height = 148
          Align = alClient
          BevelOuter = bvLowered
          Caption = 'Panel6'
          TabOrder = 0
          object StaticText1: TStaticText
            Left = 1
            Top = 1
            Width = 56
            Height = 20
            Align = alTop
            Alignment = taCenter
            Caption = 'Matched'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 0
          end
          object wwgdMatched: TevDBGrid
            Left = 1
            Top = 21
            Width = 493
            Height = 126
            DisableThemesInTitle = False
            Selected.Strings = (
              'TaxPayerTIN'#9'9'#9'Taxpayertin'#9
              'TaxPayerPIN'#9'4'#9'Taxpayerpin'#9
              'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom Company Number'#9
              'NAME'#9'35'#9'Name'#9
              'FEIN'#9'9'#9'Fein'#9)
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEFTPS_Enrollment\wwgdMatched'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = evDsMatched
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
          end
        end
        object Panel9: TPanel
          Left = 496
          Top = 1
          Width = 72
          Height = 148
          Align = alRight
          BevelOuter = bvLowered
          TabOrder = 1
          DesignSize = (
            72
            148)
          object btnRemove: TBitBtn
            Left = 5
            Top = 5
            Width = 65
            Height = 67
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = 'Re&move'
            Default = True
            ModalResult = 6
            TabOrder = 0
            OnClick = btnRemoveClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
              888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
              C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
              C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
            Layout = blGlyphBottom
            NumGlyphs = 2
          end
          object btnApply: TBitBtn
            Left = 5
            Top = 77
            Width = 65
            Height = 68
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = 'A&pply'
            Default = True
            ModalResult = 6
            TabOrder = 1
            OnClick = btnApplyClick
            Layout = blGlyphBottom
            NumGlyphs = 2
          end
        end
      end
      object evPanel1: TevPanel
        Left = 0
        Top = 351
        Width = 427
        Height = 154
        Align = alTop
        BevelInner = bvLowered
        Caption = 'evPanel1'
        TabOrder = 2
        object evLabel1: TevLabel
          Left = 11
          Top = 3
          Width = 73
          Height = 16
          Caption = 'Succeeded:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel2: TevLabel
          Left = 384
          Top = 3
          Width = 58
          Height = 16
          Caption = 'Rejected:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbSucceeded: TListBox
          Left = 9
          Top = 23
          Width = 357
          Height = 116
          ItemHeight = 13
          TabOrder = 0
        end
        object lbRejected: TListBox
          Left = 381
          Top = 23
          Width = 384
          Height = 116
          ItemHeight = 13
          TabOrder = 1
        end
        object btnEnrollmentRejectPrint: TevBitBtn
          Left = 768
          Top = 26
          Width = 89
          Height = 25
          Caption = 'Print Rejections'
          TabOrder = 2
          OnClick = btnEnrollmentRejectPrintClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
            08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
            8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
            8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
            8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
            78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
            7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
            DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
            DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
          NumGlyphs = 2
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 389
  end
  inherited wwdsDetail: TevDataSource
    Left = 270
    Top = 8
  end
  inherited wwdsList: TevDataSource
    Left = 290
    Top = 18
  end
  inherited SelectedCompanies: TevClientDataSet
    Left = 236
    object SelectedCompaniesEFTPS_ENROLLMENT_STATUS: TStringField
      FieldName = 'EFTPS_ENROLLMENT_STATUS'
      Visible = False
      Size = 1
    end
  end
  inherited wwdsSelectedCompanies: TevDataSource
    Left = 216
  end
  inherited wwdsTempTable: TevDataSource
    Left = 320
    Top = 16
  end
  object SaveDialog1: TSaveDialog
    FileName = 'enroll.txt'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Please, select location for enrollment file'
    Left = 512
    Top = 96
  end
  object OpenDialog1: TOpenDialog
    Title = 'Please, select response file'
    Left = 548
    Top = 97
  end
  object PrintDialog1: TPrintDialog
    Options = [poDisablePrintToFile]
    Left = 588
    Top = 97
  end
  object DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL
    Left = 184
  end
  object evCsFile: TevClientDataSet
    FieldDefs = <
      item
        Name = 'TaxPayerTIN'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'TaxPayerName'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'FileDate'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'TaxPayerPin'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'HasBeenMatched'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'BatchFilerID'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'MasterInquiryPIN'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'FileSequenceNumber'
        DataType = ftString
        Size = 3
      end
      item
        Name = 'EnrollmentNumber'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'ActionCode'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TaxPayerType'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SecondaryTIN'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'CountryCode'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'State'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'ZipCode'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'SignatureEquivalent'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'RemittanceMethod'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PrimaryInputMethod'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MasterAccountFlag'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'RoutingTransitNumber'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'AccountNumber'
        DataType = ftString
        Size = 17
      end
      item
        Name = 'AccountType'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PreNoteAccount'
        DataType = ftString
        Size = 1
      end>
    Filter = 'HasBeenMatched='#39'F'#39' or  HasBeenMatched='#39'P'#39
    Left = 400
    Top = 328
    object evCsFileTaxPayerTIN: TStringField
      DisplayLabel = 'Taxpayertin'
      DisplayWidth = 8
      FieldName = 'TaxPayerTIN'
      Size = 9
    end
    object evCsFileTaxPayerName: TStringField
      DisplayLabel = 'Taxpayername'
      DisplayWidth = 25
      FieldName = 'TaxPayerName'
      Size = 35
    end
    object evCsFileFileDate: TStringField
      DisplayLabel = 'Filedate'
      DisplayWidth = 8
      FieldName = 'FileDate'
      Size = 8
    end
    object evCsFileTaxPayerPin: TStringField
      DisplayLabel = 'Taxpayerpin'
      DisplayWidth = 10
      FieldName = 'TaxPayerPin'
      Size = 4
    end
    object evCsFileHasBeenMatched: TStringField
      DisplayLabel = 'Hasbeenmatched'
      DisplayWidth = 1
      FieldKind = fkCalculated
      FieldName = 'HasBeenMatched'
      Visible = False
      Size = 1
      Calculated = True
    end
    object evCsFileBatchFilerID: TStringField
      FieldName = 'BatchFilerID'
      Visible = False
      Size = 9
    end
    object evCsFileMasterInquiryPIN: TStringField
      FieldName = 'MasterInquiryPIN'
      Visible = False
      Size = 4
    end
    object evCsFileFileSequenceNumber: TStringField
      FieldName = 'FileSequenceNumber'
      Visible = False
      Size = 3
    end
    object evCsFileEnrollmentNumber: TStringField
      FieldName = 'EnrollmentNumber'
      Visible = False
      Size = 4
    end
    object evCsFileActionCode: TStringField
      FieldName = 'ActionCode'
      Visible = False
      Size = 1
    end
    object evCsFileTaxPayerType: TStringField
      FieldName = 'TaxPayerType'
      Visible = False
      Size = 1
    end
    object evCsFileSecondaryTIN: TStringField
      FieldName = 'SecondaryTIN'
      Visible = False
      Size = 9
    end
    object evCsFileCountryCode: TStringField
      FieldName = 'CountryCode'
      Visible = False
      Size = 2
    end
    object evCsFileState: TStringField
      FieldName = 'State'
      Visible = False
      Size = 2
    end
    object evCsFileZipCode: TStringField
      FieldName = 'ZipCode'
      Visible = False
      Size = 9
    end
    object evCsFileSignatureEquivalent: TStringField
      FieldName = 'SignatureEquivalent'
      Visible = False
      Size = 4
    end
    object evCsFileRemittanceMethod: TStringField
      FieldName = 'RemittanceMethod'
      Visible = False
      Size = 1
    end
    object evCsFilePrimaryInputMethod: TStringField
      FieldName = 'PrimaryInputMethod'
      Visible = False
      Size = 1
    end
    object evCsFileMasterAccountFlag: TStringField
      FieldName = 'MasterAccountFlag'
      Visible = False
      Size = 1
    end
    object evCsFileRoutingTransitNumber: TStringField
      FieldName = 'RoutingTransitNumber'
      Visible = False
      Size = 9
    end
    object evCsFileAccountNumber: TStringField
      FieldName = 'AccountNumber'
      Visible = False
      Size = 17
    end
    object evCsFileAccountType: TStringField
      FieldName = 'AccountType'
      Visible = False
      Size = 1
    end
    object evCsFilePreNoteAccount: TStringField
      FieldName = 'PreNoteAccount'
      Visible = False
      Size = 1
    end
  end
  object evDsFile: TevDataSource
    DataSet = evCsFile
    Left = 432
    Top = 328
  end
  object evDsMatched: TevDataSource
    DataSet = evCsMatched
    Left = 608
    Top = 328
  end
  object evCsMatched: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CL_NBR'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CO_NBR'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'TaxPayerTIN'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'TaxPayerPIN'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 35
      end
      item
        Name = 'FEIN'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'EnrollmentNumber'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'EFTPS_ENROLLMENT_STATUS'
        DataType = ftString
        Size = 1
      end>
    Left = 568
    Top = 328
    object evCsMatchedTaxPayerTIN: TStringField
      DisplayWidth = 9
      FieldName = 'TaxPayerTIN'
      Size = 9
    end
    object evCsMatchedTaxPayerPIN: TStringField
      DisplayWidth = 4
      FieldName = 'TaxPayerPIN'
      Size = 4
    end
    object evCsMatchedCUSTOM_COMPANY_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_COMPANY_NUMBER'
      Size = 10
    end
    object evCsMatchedNAME: TStringField
      DisplayWidth = 35
      FieldName = 'NAME'
      Size = 35
    end
    object evCsMatchedFEIN: TStringField
      DisplayWidth = 9
      FieldName = 'FEIN'
      Size = 9
    end
    object evCsMatchedCL_NBR: TStringField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
      Visible = False
      Size = 10
    end
    object evCsMatchedCO_NBR: TStringField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Visible = False
      Size = 10
    end
    object evCsMatchedEnrollmentNumber: TStringField
      DisplayWidth = 4
      FieldName = 'EnrollmentNumber'
      Visible = False
      Size = 4
    end
    object evCsMatchedEFTPS_ENROLLMENT_STATUS: TStringField
      DisplayWidth = 1
      FieldName = 'EFTPS_ENROLLMENT_STATUS'
      Visible = False
      Size = 1
    end
  end
  object evDsEvo: TevDataSource
    DataSet = evCsEvo
    Left = 512
    Top = 224
  end
  object evCsEvo: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'FEIN'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'EFTPS_ENROLLMENT_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'EFTPS_ENROLLMENT_DATE'
        DataType = ftDateTime
      end>
    Filter = 'EFTPS_ENROLLMENT_STATUS='#39'P'#39
    Filtered = True
    Left = 512
    Top = 184
    object evCsEvoCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Custom Company Number'
      DisplayWidth = 20
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object evCsEvoNAME: TStringField
      DisplayLabel = 'Name'
      DisplayWidth = 36
      FieldName = 'NAME'
      Size = 40
    end
    object evCsEvoFEIN: TStringField
      DisplayLabel = 'Fein'
      DisplayWidth = 9
      FieldName = 'FEIN'
      Size = 9
    end
    object evCsEvoEFTPS_ENROLLMENT_STATUS: TStringField
      DisplayLabel = 'Eftps Enrollment Status'
      DisplayWidth = 18
      FieldName = 'EFTPS_ENROLLMENT_STATUS'
      Size = 1
    end
    object evCsEvoCL_NBR: TIntegerField
      DisplayLabel = 'Cl Nbr'
      DisplayWidth = 10
      FieldName = 'CL_NBR'
      Visible = False
    end
    object evCsEvoCO_NBR: TIntegerField
      DisplayLabel = 'Co Nbr'
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Visible = False
    end
    object evCsEvoEFTPS_ENROLLMENT_DATE: TDateTimeField
      FieldName = 'EFTPS_ENROLLMENT_DATE'
      Visible = False
    end
  end
end
