inherited PROCESS_CLEAR_CREDIT_LIAB: TPROCESS_CLEAR_CREDIT_LIAB
  object Pc: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 695
    Height = 465
    ActivePage = ts1
    TabOrder = 0
    object ts1: TTabSheet
      Caption = 'Select companies'
      object cbQuarter: TevComboBox
        Left = 8
        Top = 408
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          '1'#39'st quarter'
          '2'#39'nd quarter'
          '3'#39'rd quarter'
          '4'#39'th quarter')
      end
      object cbYear: TevEdit
        Left = 168
        Top = 408
        Width = 49
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 2
        Text = '2000'
      end
      object UpDown1: TUpDown
        Left = 217
        Top = 409
        Width = 13
        Height = 19
        Min = 1990
        Max = 2100
        Position = 2000
        TabOrder = 3
        OnClick = UpDown1Click
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 685
        Height = 401
        DisableThemesInTitle = False
        ControlType.Strings = (
          'LOAD;CheckBox;True;False'
          'TAX_SERVICE;CheckBox;Y;N')
        Selected.Strings = (
          'LOAD'#9'5'#9'Load'#9'F'
          'DESCR'#9'15'#9'Description'#9'T'
          'CUSTOM_CLIENT_NUMBER'#9'12'#9'Client #'#9'T'
          'CLIENT_NAME'#9'25'#9'Client name'#9'T'
          'CUSTOM_COMPANY_NUMBER'#9'12'#9'Comp. #'#9'T'
          'COMPANY_NAME'#9'25'#9'Company name'#9'T'
          'TAX_SERVICE'#9'1'#9'Tax'#9'T')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TPROCESS_CLEAR_CREDIT_LIAB\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsClCo
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = wwDBGrid1DblClick
        PaintOptions.AlternatingRowColor = clCream
      end
      object Button4: TevBitBtn
        Left = 384
        Top = 408
        Width = 97
        Height = 25
        Caption = 'Select all'
        TabOrder = 4
        OnClick = Button4Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD0000
          008DDDDDDDDD888888FD0000DDDD0000008D8888DDDD888888FD00D0DDD80000
          008D88D8DDDF888888FD0D80DD80FFFF77088D88DDF8DDDDDD8F0000DD0FFFFF
          F7088888DD8DDDDDDD8FDDDDDD07FFFFF708DDDDDD8DDDDDDD8F0000DD80FFFF
          77088888DDD8DDDDDD8F00D0DDD0FF77008D88D8DDD8DDDD88FD0D80DDD0F000
          8DDD8D88DDD8D888FDDD0000DDD0F08DDDDD8888DDD8D8FDDDDDDDDDDDD0F08D
          DDDDDDDDDDD8D8FDDDDD0000DDD0F08DDDDD8888DDD8D8FDDDDD00D0DDD0F08D
          DDDD88D8DDD8D8FDDDDD0D80DDD808DDDDDD8D88DDDD8FDDDDDD0000DDDDDDDD
          DDDD8888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object Button5: TevBitBtn
        Left = 488
        Top = 408
        Width = 97
        Height = 25
        Caption = 'Unselect all'
        TabOrder = 5
        OnClick = Button5Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDD0000DDDDDDDDDDDD8888DDDDDDDDDDDD00D0DD88DDDD
          D88D88D8DDDDDDDDDDDD0D80DD008DDD80088D88DDFFDDDDDFFD0000D8FF08D8
          0FF88888DD88FDDDF88DDDDDD8FFF080FFF8DDDDDD888FDF888D0000DD8FFF0F
          FF8D8888DDD888F888DD00D0DDD8FFFFF8DD88D8DDDD88888DDD0D80DDD80FFF
          08DD8D88DDDDF888FDDD0000DD80FFFFF08D8888DDDF88888FDDDDDDD80FFF8F
          FF08DDDDDDF888D888FD0000D8FFF8D8FFF88888DD888DDD888D00D0D8FF8DDD
          8FF888D8DD88DDDDD88D0D80DD88DDDDD88D8D88DDDDDDDDDDDD0000DDDDDDDD
          DDDD8888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object Button1: TevBitBtn
        Left = 600
        Top = 408
        Width = 83
        Height = 25
        Caption = 'Load'
        TabOrder = 6
        OnClick = Button1Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D44444444444
          4DDDD888888888888DDDD4FFFF7FFFFF4DDDD8DDDDDDDDDD8DDDD4FFF708FFFF
          4DDDD8DDDDFDDDDD8DDDD4FF70A08FFF4DDDD8DDDF8FDDDD8DDDD4F70AAA08FF
          4888D8DDF888FDDD8D88D470AAAAA08F4778D8DF88888FDD8DD8D47AAAAAAA08
          4778D8D8888888FD8DD8D4777AAA07774778D8DDD888FDDD8DD8D4FF7AAA0888
          4778D8DDD888FDDD8DD8D4FF7AAA00000078D8DDD888FFFFFFD8D4444AAAAAAA
          A078D888D88888888FD8D4444AAAAAAAA078D888D88888888FD8DDDD8AAAAAAA
          A078DDDDD88888888FD8DDDD877777777778DDDD8DDDDDDDDDD8DDDD88888888
          8888DDDD888888888888DDDD888888888888DDDD888888888888}
        NumGlyphs = 2
      end
    end
    object ts2: TTabSheet
      Caption = 'Selected credit liabs for marking as refunded'
      ImageIndex = 1
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 0
        Width = 685
        Height = 401
        DisableThemesInTitle = False
        ControlType.Strings = (
          'Do;CheckBox;True;False')
        Selected.Strings = (
          'NUMBER'#9'15'#9'Cust #'#9'T'
          'NAME'#9'35'#9'Name'#9'T'
          'TaxDesc'#9'17'#9'Taxdesc'#9'T'
          'Amount'#9'10'#9'Amount'#9'T'
          'Do'#9'5'#9'Correct'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TPROCESS_CLEAR_CREDIT_LIAB\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsTaxes
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object Button3: TevBitBtn
        Left = 504
        Top = 408
        Width = 83
        Height = 25
        Caption = 'Back'
        TabOrder = 1
        OnClick = Button3Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDA2DDDDDDDDDDDDDDFFDDDDDDDDDDDDAAA
          2DDDDDDDDDDDDFFD8DDDDDDDDDDAAAAA2DDDDDDDDDDFFDDD8DDDDDDDDAAAAAAA
          2DDDDDDDDFFDDDDD8DDDDDDAAAAAAAAA2DDDDDDFFDDDDDDD8DDDD222AAAAAAAA
          2DDDDFF8DDDDDDDD8DDDDD2222AAAAAA2DDDDD8888DDDDDD8DDDDDDD2222AAAA
          2DDDDDDD8888DDDD8DDDDDDDDD2222AA2DDDDDDDDD8888DD8DDDDDDDDDDD2222
          2DDDDDDDDDDD88888DDDDDDDDDDDDD222DDDDDDDDDDDDD888DDDDDDDDDDDDDDD
          2DDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object Button2: TevBitBtn
        Left = 600
        Top = 408
        Width = 83
        Height = 25
        Caption = 'Mark'
        TabOrder = 2
        OnClick = Button2Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDD88888888888888DDFFFFFFFFFFFFFFD222222222222
          228D88888888888888FD27FFF7A7FFFFF28D8FDDDDFDDDDDD8FD27FF7A2AFFFF
          F28D8FDDDF8FDDDDD8FD27F7A22A7FFFF28D8FDDF88FDDDDD8FD277A2222AFFF
          F28D8FDF8888FDDDD8FD27A22772A7FFF28D8FF88DD8FDDDD8FD2A27FFF22AFF
          F28D8F8DDDD88FDDD8FD277FFFF72A7FF28D8FDDDDDD8FDDD8FD27FFFFFF22AF
          F28D8FDDDDDD88FDD8FD27FFFFFF72A7F28D8FDDDDDDD8FDD8FD27FFFFFFF22A
          F28D8FDDDDDDD88FD8FD27FFFFFFF72A728D8FDDDDDDDD8FD8FD277777777722
          A28D8FFFFFFFFF88F8FD22222222222222DD88888888888888DD}
        NumGlyphs = 2
      end
    end
  end
  object cdClCo: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N'
      'FUI_DO;CheckBox;True;False'
      'LOAD;CheckBox;True;False'
      'SUI_DO;CheckBox;True;False')
    Filtered = True
    Left = 24
    object cdClCoLOAD: TBooleanField
      DisplayLabel = 'Load'
      DisplayWidth = 5
      FieldKind = fkInternalCalc
      FieldName = 'LOAD'
    end
    object cdClCoDESCRI: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DESCR'
      Size = 15
    end
    object cdClCoCUSTOM_CLIENT_NUMBER: TStringField
      DisplayLabel = 'Client #'
      DisplayWidth = 7
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdClCoCLIENT_NAME: TStringField
      DisplayLabel = 'Client name'
      DisplayWidth = 15
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdClCoCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Comp. #'
      DisplayWidth = 7
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdClCoCOMPANY_NAME: TStringField
      DisplayLabel = 'Company name'
      DisplayWidth = 15
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdClCoTAX_SERVICE: TStringField
      DisplayLabel = 'Tax'
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      FixedChar = True
      Size = 1
    end
    object cdClCoCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object cdClCoCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
  end
  object dsClCo: TevDataSource
    DataSet = cdClCo
    Left = 56
  end
  object cdTaxes: TevClientDataSet
    Left = 408
    object cdTaxesNUMBER: TStringField
      FieldName = 'NUMBER'
    end
    object cdTaxesNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object cdTaxesTaxType: TStringField
      FieldName = 'TaxType'
      Size = 3
    end
    object cdTaxesNbr: TIntegerField
      FieldName = 'Nbr'
    end
    object cdTaxesTaxDesc: TStringField
      DisplayWidth = 15
      FieldName = 'TaxDesc'
    end
    object cdTaxesTotalCalc: TCurrencyField
      FieldName = 'Amount'
    end
    object cdTaxesDo: TBooleanField
      FieldName = 'Do'
    end
    object cdTaxescl_nbr: TIntegerField
      FieldName = 'cl_nbr'
    end
    object cdTaxesco_nbr: TIntegerField
      FieldName = 'co_nbr'
    end
    object cdTaxesDone: TBooleanField
      FieldName = 'Done'
    end
  end
  object dsTaxes: TevDataSource
    DataSet = cdTaxes
    Left = 440
  end
end
