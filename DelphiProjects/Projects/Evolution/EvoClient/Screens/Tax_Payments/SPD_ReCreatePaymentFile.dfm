inherited ReCreatePaymentFile: TReCreatePaymentFile
  object Label1: TevLabel [0]
    Left = 352
    Top = 16
    Width = 265
    Height = 13
    Caption = 'Select EFTP file you want to mark as paid and hit button'
  end
  object wwDBGrid1: TevDBGrid [1]
    Left = 8
    Top = 8
    Width = 681
    Height = 433
    DisableThemesInTitle = False
    ControlType.Strings = (
      'STATUS;CustomEdit;cbDbStatus'
      'MARK;CheckBox;True;False')
    Selected.Strings = (
      'MARK'#9'9'#9'Selected'#9'F'
      'DAY_DATE'#9'12'#9'Status date'#9'T'
      'STATUS'#9'12'#9'Status'#9'T'
      'CUSTOM_COMPANY_NUMBER'#9'15'#9'Comp #'#9'T'
      'NAME'#9'25'#9'Name'#9'T'
      'TAX_PAYMENT_REFERENCE_NUMBER'#9'20'#9'Reference#'#9'T')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TReCreatePaymentFile\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = ds
    KeyOptions = [dgEnterToTab]
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object cbDbStatus: TevDBComboBox [2]
    Left = 488
    Top = 56
    Width = 121
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = True
    AutoDropDown = True
    DataField = 'STATUS'
    DataSource = ds
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Pending'#9'X'
      'Paid'#9'P'
      'Paid-Send Notice'#9'S'
      'NSF'#9'N'
      'Voided'#9'V'
      'Send-Notice'#9'C'
      'Notice-Sent'#9'T'
      'Deleted'#9'D'
      'Manually impounded'#9'M'
      'Paid w/o funds'#9'W'
      'Impounded'#9'I')
    Picture.PictureMaskFromDataSet = False
    ReadOnly = True
    Sorted = False
    TabOrder = 1
    UnboundDataType = wwDefault
  end
  object Button1: TevBitBtn [3]
    Left = 584
    Top = 456
    Width = 107
    Height = 25
    Caption = 'Undo Deposit'
    TabOrder = 2
    OnClick = Button1Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD8888
      8DDDDDDDDDDDFFFFFDDDDDDDDDDD000008DDDDDDDDDD88888FDDDDDDDDDD0FFF
      F08DDDDDDDDD8DDDD8FDDDDD808D0FFFF70DDDDDDFFD8DDDDD8DDDD8008D0777
      780DDDDDF8FD8DDDDF8DDD8060880888800DDDDF88FFFFFFF88DD806E0000000
      0E0DDDF888888888888D806EEEEEEEEEEE0DDF8888888888888D06EEEEEEEEEE
      EE0DF88888888888888D0EEEEEEEEEEEEE0D888888888888888DD0EEEEEEEEEE
      EE0DD88888888888888DDD0EEEEEEEEEE0DDDD888888888888DDDDD0E0000000
      0DDDDDD8888888888DDDDDDD008DDDDDDDDDDDDD88DDDDDDDDDDDDDDD0DDDDDD
      DDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
    NumGlyphs = 2
  end
  object ds: TevDataSource
    DataSet = cd
    Left = 424
    Top = 8
  end
  object cd: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CL_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'CO_TAX_DEPOSITS_NBR'
        Attributes = [faRequired]
        DataType = ftInteger
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'TAX_PAYMENT_REFERENCE_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'MARK'
        DataType = ftBoolean
      end>
    Left = 456
    Top = 8
    object cdCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Required = True
    end
    object cdCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Required = True
    end
    object cdCO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
      Required = True
    end
    object cdSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object cdSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
    end
    object cdTAX_PAYMENT_REFERENCE_NUMBER: TStringField
      FieldName = 'TAX_PAYMENT_REFERENCE_NUMBER'
    end
    object cdCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object cdMARK: TBooleanField
      FieldKind = fkInternalCalc
      FieldName = 'MARK'
    end
    object cdDAY_DATE: TDateField
      FieldName = 'DAY_DATE'
    end
  end
end
