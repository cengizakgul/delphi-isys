inherited Prenote_EFT_ACH: TPrenote_EFT_ACH
  object Label1: TevLabel [0]
    Left = 312
    Top = 32
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited Panel2: TevPanel
        inherited wwdbgridSelectClient: TevDBGrid
          IniAttributes.SectionName = 'TPrenote_EFT_ACH\wwdbgridSelectClient'
        end
        inherited pnlSubbrowse: TevPanel
          inherited Panel4: TevPanel
            inherited wwDBGrid1: TevDBGrid
              IniAttributes.SectionName = 'TPrenote_EFT_ACH\wwDBGrid1'
            end
          end
        end
      end
      inherited Panel3: TevPanel
        object Label2: TevLabel [0]
          Left = 344
          Top = 0
          Width = 25
          Height = 13
          Caption = 'State'
        end
        inherited bbtnCopyAll: TevBitBtn
          Width = 105
          Caption = 'Copy All CO'
        end
        inherited bbtnRemoveAll: TevBitBtn
          Left = 112
          Width = 105
          Caption = 'Remove All CO'
        end
        object rgrpTaxType: TevRadioGroup
          Left = 224
          Top = 0
          Width = 113
          Height = 33
          Caption = 'Tax Type'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'State'
            'SUI')
          TabOrder = 2
        end
        object bbtnAdd: TevBitBtn
          Left = 396
          Top = 8
          Width = 93
          Height = 25
          Caption = 'Add to File'
          TabOrder = 3
          OnClick = bbtnAddClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD88888888
            8888DDDDDDDDDDDDDDDDDDD8487487774448DDDD88D88DDD888DDDD448748777
            4448DDD888D88DDD888DDDD4487487774448DDD888D88DDD888DDDD448777777
            4448DDD888DDDDDD888DDDD4444444444448DDD888888888888DDDD442444444
            4448DDD88F888888888D88847A2777777748DDD8D8FDDDDDDD8D22222AA277FF
            FF48FFFFF88FDDDDDD8DAAAAAAAA277FFF4888888888FDDDDD8DAAAAAAAAA277
            FF48888888888FDDDD8DAAAAAAAAAA7FFF488888888888DDDD8DAAAAAAAAA7FF
            FF48888888888DDDDD8DAAAAAAAA8888884D88888888D888888DDDDDDAADDDDD
            DDDDDDDDD88DDDDDDDDDDDDDDADDDDDDDDDDDDDDD8DDDDDDDDDD}
          NumGlyphs = 2
        end
      end
    end
  end
  object wwlcState: TevDBLookupCombo [2]
    Left = 347
    Top = 40
    Width = 46
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'STATE'#9'2'#9'STATE')
    LookupTable = DM_SY_STATES.SY_STATES
    LookupField = 'STATE'
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  object bbtnCreate: TevBitBtn [3]
    Left = 504
    Top = 32
    Width = 129
    Height = 25
    Caption = 'Create Prenote ACH'
    Enabled = False
    TabOrder = 1
    OnClick = bbtnCreateClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD888888888
      8888DDDDDDDDDDDDDDDDDD84487487774448DDD88D88DDDD888D884448888877
      4448DFFFFFFFFFDD888DBBBBBBBBB8774448888888888FDD888DBBBBBBBBB877
      4448888888888FDD888DBBB333BBB4444448888DDD888F88888DBB3BBB3BB444
      444888D888D88F88888DBBBBBB3BB7777748888888D88FDDDD8DBBB333BBB7FF
      FF48888DDD888FDDDD8DBB3BBB3BB7FFFF4888D888D88FDDDD8DBB3BBB3BB7FF
      FF4888D888D88FDDDD8DBBB333BBB7FFFF48888DDD888FDDDD8DBB3BBBBBB7FF
      FF4888D888888FDDDD8DBB3BBB3BB888884D88D888D88F88888DBBB333BBB8DD
      DDDD888DDD888FDDDDDDBBBBBBBBBDDDDDDD888888888DDDDDDD}
    NumGlyphs = 2
  end
  inherited SelectedCompanies: TevClientDataSet
    IndexFieldNames = 'CL_NBR'
    Left = 284
    Top = 65521
  end
  inherited wwdsSelectedCompanies: TevDataSource
    Left = 408
    Top = 65520
  end
  inherited wwdsTempTable: TevDataSource
    Left = 156
    Top = 65521
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 248
    Top = 16
  end
end
