// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EFTPS_Enrollment;

interface

uses
  Windows, SPD_EDIT_GLOBAL_CO_BASE, Dialogs, StdCtrls, wwdblook, 
  ExtCtrls, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Variants,
  Wwdbgrid, Controls, ComCtrls, Classes, Graphics, SysUtils, SDataStructure, EvTypes,
  SDDClasses, ISBasicClasses, kbmMemTable, ISKbmMemDataSet, EvContext,
  EvDataAccessComponents, ISDataAccessComponents, SDataDictsystem, EvStreamUtils,
  SDataDicttemp,  EvMainboard, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

const
  EnrollmentsConfig = 'BatchFilerID=1,9' + #13 +
                      'MasterInquiryPIN=10,4' + #13 +
                      'FileDate=14,8' + #13 +
                      'FileSequenceNumber=22,3' + #13 +
                      'EnrollmentNumber=25,4' + #13 +
                      'ActionCode=29,1' + #13 +
                      'TaxPayerName=30,35' + #13 +
                      'TaxPayerType=65,1' + #13 +
                      'TaxPayerTIN=66,9' + #13 +
                      'TaxPayerPIN=77,4' + #13 +
                      'SecondaryTIN=81,9' + #13 +
                      'CountryCode=90,2' + #13 +
                      'State=92,2' + #13 +
                      'ZipCode=94,9' + #13 +
                      'SignatureEquivalent=103,4' + #13 +
                      'RemittanceMethod=107,1' + #13 +
                      'PrimaryInputMethod=108,1' + #13 +
                      'MasterAccountFlag=109,1' + #13 +
                      'RoutingTransitNumber=110,9' + #13 +
                      'AccountNumber=119,17' + #13 +
                      'AccountType=136,1' + #13 +
                      'PreNoteAccount=137,1';

type
  TEFTPS_Enrollment = class(TEDIT_GLOBAL_CO_BASE)
    bbtnEnroll: TevBitBtn;
    rgrpSelected: TevRadioGroup;
    SelectedCompaniesEFTPS_ENROLLMENT_STATUS: TStringField;
    TabSheet2: TTabSheet;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    bbtnPaymentResponse: TevBitBtn;
    bbtnEnrollmentResponse: TevBitBtn;
    lboxSucceeded: TListBox;
    lboxRejected: TListBox;
    Label1: TevLabel;
    Label2: TevLabel;
    bbtnPrintRejections: TevBitBtn;
    PrintDialog1: TPrintDialog;
    DM_SYSTEM_FEDERAL: TDM_SYSTEM_FEDERAL;
    cbTaxOnly: TevCheckBox;
    rgEnroll: TevRadioGroup;
    TabSheet3: TTabSheet;
    evCsFile: TevClientDataSet;
    evDsFile: TevDataSource;
    evDsMatched: TevDataSource;
    evCsMatched: TevClientDataSet;
    evDsEvo: TevDataSource;
    evCsFileBatchFilerID: TStringField;
    evCsFileMasterInquiryPIN: TStringField;
    evCsFileFileDate: TStringField;
    evCsFileFileSequenceNumber: TStringField;
    evCsFileEnrollmentNumber: TStringField;
    evCsFileActionCode: TStringField;
    evCsFileTaxPayerName: TStringField;
    evCsFileTaxPayerType: TStringField;
    evCsFileTaxPayerTIN: TStringField;
    evCsFileSecondaryTIN: TStringField;
    evCsFileCountryCode: TStringField;
    evCsFileState: TStringField;
    evCsFileZipCode: TStringField;
    evCsFileSignatureEquivalent: TStringField;
    evCsFileRemittanceMethod: TStringField;
    evCsFilePrimaryInputMethod: TStringField;
    evCsFileMasterAccountFlag: TStringField;
    evCsFileRoutingTransitNumber: TStringField;
    evCsFileAccountNumber: TStringField;
    evCsFileAccountType: TStringField;
    evCsFilePreNoteAccount: TStringField;
    evCsFileTaxPayerPin: TStringField;
    evCsFileHasBeenMatched: TStringField;
    evCsMatchedCL_NBR: TStringField;
    evCsMatchedCO_NBR: TStringField;
    evCsMatchedTaxPayerTIN: TStringField;
    evCsMatchedTaxPayerPIN: TStringField;
    evCsMatchedCUSTOM_COMPANY_NUMBER: TStringField;
    evCsMatchedNAME: TStringField;
    evCsMatchedFEIN: TStringField;
    Panel7: TPanel;
    Panel1: TPanel;
    wwgdFile: TevDBGrid;
    StaticText2: TStaticText;
    Splitter3: TSplitter;
    Panel5: TPanel;
    StaticText3: TStaticText;
    wwgdEvolution: TevDBGrid;
    Panel15: TPanel;
    btnAdd: TBitBtn;
    Panel8: TPanel;
    Panel6: TPanel;
    StaticText1: TStaticText;
    wwgdMatched: TevDBGrid;
    Panel9: TPanel;
    btnRemove: TBitBtn;
    btnEnrolmentFile: TevBitBtn;
    btnApply: TBitBtn;
    evCsMatchedEnrollmentNumber: TStringField;
    evCsMatchedEFTPS_ENROLLMENT_STATUS: TStringField;
    evCsEvo: TevClientDataSet;
    evCsEvoCO_NBR: TIntegerField;
    evCsEvoCL_NBR: TIntegerField;
    evCsEvoCUSTOM_COMPANY_NUMBER: TStringField;
    evCsEvoNAME: TStringField;
    evCsEvoFEIN: TStringField;
    evCsEvoEFTPS_ENROLLMENT_STATUS: TStringField;
    evCsEvoEFTPS_ENROLLMENT_DATE: TDateTimeField;
    evPanel1: TevPanel;
    lbSucceeded: TListBox;
    evLabel1: TevLabel;
    lbRejected: TListBox;
    evLabel2: TevLabel;
    btnEnrollmentRejectPrint: TevBitBtn;
    procedure bbtnEnrollClick(Sender: TObject);
    procedure wwdbgridSelectClientCalcCellColors(Sender: TObject;
      Field: TField; State: TGridDrawState; Highlight: Boolean;
      AFont: TFont; ABrush: TBrush);
    procedure rgrpSelectedClick(Sender: TObject);
    procedure bbtnEnrollmentResponseClick(Sender: TObject);
    procedure bbtnPrintRejectionsClick(Sender: TObject);
    procedure bbtnPaymentResponseClick(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure cbTaxOnlyClick(Sender: TObject);
    procedure btnEnrolmentFileClick(Sender: TObject);

    procedure CreateImportDataSets(Fname: String; Cfg: TStringList; aDataSet: TevClientDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure btnEnrollmentRejectPrintClick(Sender: TObject);
    procedure PageControl1Enter(Sender: TObject);
    procedure wwdbgridSelectClientFilterChange(Sender: TObject;
      DataSet: TDataSet; NewFilter: String);
  private
    EnrollmentsCfg   : TStringList;

    procedure DoEnrollment;
    procedure DoEnrollmentResponse(FileName: TFileName);
    //  by Ca
    //  After Added new Page we dont need this procedure...
   //  procedure DoBatchEnrollmentResponse(FileName: TFileName);
    procedure DoPaymentResponse(FileName: TFileName);
    procedure DoBatchPaymentResponse(FileName: TFileName);

    procedure ApplyFilter;
    procedure EnrollmentResponseApplyFilter;
    procedure EnrollmentResponseMatch(var TaxPayerTIN : String; TaxPayerPIN : String; EnrNbr : String; Status : String);
    function  EnrollmentResponseMatchControl(var Pin : String;EnrNbr : String; TaxPayerTin : String): String;
    procedure SafeFileSave(const aList: TStringList; const aSaveDialog: TSaveDialog; const SkipDialog: Boolean = False);
  public
    { Public declarations }
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

var
  EFTPS_Enrollment: TEFTPS_Enrollment;

procedure ProcessPaymentResponse(ResponseList, ConfNbrList, TraceList : TStringList; BatchProvider: Boolean = False);

implementation

uses
  EvConsts, EvUtils, DateUtils,
  Forms, WinSpool, PCLPrinter, Printers, isBaseClasses;

{$R *.DFM}

procedure ProcessPaymentResponse(ResponseList, ConfNbrList, TraceList : TStringList; BatchProvider: Boolean = False);
var
  I: Integer;
  PaymentAmt: Double;
  DueDate: TDateTime;

  procedure GetLiabilityTableData(LiabilityTable: TevClientDataSet; TaxDepositNbr: Integer; var Amount: Double; var DueDate: TDateTime);
  begin
    LiabilityTable.Filter := 'CO_TAX_DEPOSITS_NBR=' + IntToStr(TaxDepositNbr);
    LiabilityTable.Filtered := True;
    LiabilityTable.First;
    while not LiabilityTable.EOF do
    begin
      Amount := Amount + LiabilityTable.FieldByName('AMOUNT').Value;
      DueDate := LiabilityTable.FieldByName('DUE_DATE').Value;
      LiabilityTable.Next;
    end;
    LiabilityTable.Filtered := False;
  end;

var
  s: string;
begin
  with DM_COMPANY, DM_TEMPORARY do
    for I := 0 to ResponseList.Count - 1 do
      if ResponseList.Names[I] <> '' then
      begin
        s := 'EFTD#' + ResponseList.Names[I];
        DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.Close;
        DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.DataRequired('TAX_PAYMENT_REFERENCE_NUMBER='
          + QuotedStr(ResponseList.Names[I]) + ' or TAX_PAYMENT_REFERENCE_NUMBER='+ QuotedStr(s));
        DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.First;
        while not DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.Eof do
        begin
          ctx_DataAccess.OpenClient(TMP_CO_TAX_DEPOSITS.FieldByName('CL_NBR').AsInteger);
          CO.DataRequired('ALL');
          CO_BANK_ACCOUNT_REGISTER.DataRequired('CO_NBR=0');

          CO_TAX_DEPOSITS.DataRequired('CO_TAX_DEPOSITS_NBR=' + TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsString);
          CO_FED_TAX_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR=' +
            TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsString);
          CO_STATE_TAX_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR=' +
            TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsString);
          CO_SUI_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR=' +
            TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsString);
          CO_LOCAL_TAX_LIABILITIES.DataRequired('CO_TAX_DEPOSITS_NBR=' +
            TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsString);

          if (ResponseList.Values[ResponseList.Names[I]] = '0000') or (ResponseList.Values[ResponseList.Names[I]] = '5122') or (ResponseList.Values[ResponseList.Names[I]] = '5125') then
          begin
            if CO_TAX_DEPOSITS.FieldByName('STATUS').Value <> TAX_DEPOSIT_STATUS_PAID then
            begin
              CO_TAX_DEPOSITS.Edit;
              CO_TAX_DEPOSITS.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_PAID;
              if I < ConfNbrList.Count then
                CO_TAX_DEPOSITS.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').Value := 'CONF#' + ConfNbrList[I];
              if I < TraceList.Count then
                CO_TAX_DEPOSITS.FieldByName('TRACE_NUMBER').Value :=  TraceList[I];
              CO_TAX_DEPOSITS.Post;

              ctx_PayrollCalculation.UpdateAllLiabilityTablesStatus(TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger);
              PaymentAmt := 0;
              DueDate := 0;
              GetLiabilityTableData(CO_FED_TAX_LIABILITIES, TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger, PaymentAmt,
                DueDate);
              GetLiabilityTableData(CO_STATE_TAX_LIABILITIES, TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger, PaymentAmt,
                DueDate);
              GetLiabilityTableData(CO_SUI_LIABILITIES, TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger, PaymentAmt, DueDate);
              GetLiabilityTableData(CO_LOCAL_TAX_LIABILITIES, TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger, PaymentAmt,
                DueDate);
              Assert(CO.Locate('CO_NBR', TMP_CO_TAX_DEPOSITS['CO_NBR'], []));

              if CO.TAX_SERVICE.Value = TAX_SERVICE_FULL then
              begin
                CO_BANK_ACCOUNT_REGISTER.Insert;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('SB_BANK_ACCOUNTS_NBR').Value := CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').Value;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').Value := BANK_REGISTER_STATUS_Outstanding;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_DATE').Value := DueDate;
                if CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_DATE').Value < Date then
                  CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').Value := Date
                else
                  CO_BANK_ACCOUNT_REGISTER.FieldByName('TRANSACTION_EFFECTIVE_DATE').Value := CO_BANK_ACCOUNT_REGISTER.FieldByName('CHECK_DATE').Value;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('AMOUNT').Value := PaymentAmt * (-1);
                CO_BANK_ACCOUNT_REGISTER.FieldByName('REGISTER_TYPE').Value := BANK_REGISTER_TYPE_EFTPayment;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('MANUAL_TYPE').Value := BANK_REGISTER_MANUALTYPE_None;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('PROCESS_DATE').Value := Date;
                CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_DEPOSITS_NBR').Value := TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').Value;
                CO_BANK_ACCOUNT_REGISTER.Post;
              end;
            end
            else
            begin
              DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.Next;
              continue;
            end;
          end
          else
          begin
            CO_TAX_DEPOSITS.Edit;
            CO_TAX_DEPOSITS.FieldByName('STATUS').Value := TAX_DEPOSIT_STATUS_DELETED;
            if I < ConfNbrList.Count then
              CO_TAX_DEPOSITS.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').Value := 'CONF#' + ConfNbrList[I];
            if I < TraceList.Count then
                CO_TAX_DEPOSITS.FieldByName('TRACE_NUMBER').Value :=  TraceList[I];
            CO_TAX_DEPOSITS.Post;
            if not CO_FED_TAX_LIABILITIES.eof then
              ctx_DataAccess.UnlinkLiabilitiesFromDeposit(CO_FED_TAX_LIABILITIES, '(CO_TAX_DEPOSITS_NBR IN ( ' + TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsString + ' ))');
            if not CO_STATE_TAX_LIABILITIES.eof then
              ctx_DataAccess.UnlinkLiabilitiesFromDeposit(CO_STATE_TAX_LIABILITIES, '(CO_TAX_DEPOSITS_NBR IN ( ' + TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsString + ' ))');
            if not CO_SUI_LIABILITIES.eof then
              ctx_DataAccess.UnlinkLiabilitiesFromDeposit(CO_SUI_LIABILITIES, '(CO_TAX_DEPOSITS_NBR IN ( ' + TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsString + ' ))');
            if not CO_LOCAL_TAX_LIABILITIES.eof then
              ctx_DataAccess.UnlinkLiabilitiesFromDeposit(CO_LOCAL_TAX_LIABILITIES, '(CO_TAX_DEPOSITS_NBR IN ( ' + TMP_CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsString + ' ))');
          end;

          ctx_DataAccess.PostDataSets([CO_FED_TAX_LIABILITIES, CO_STATE_TAX_LIABILITIES, CO_SUI_LIABILITIES, CO_LOCAL_TAX_LIABILITIES,
            CO_TAX_DEPOSITS, CO_BANK_ACCOUNT_REGISTER]);

          if BatchProvider then
            Break;

          DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.Next;
        end;
      end;
end;

procedure TEFTPS_Enrollment.bbtnEnrollClick(Sender: TObject);
begin
  inherited;
  if EvMessage('Are you sure you want to enroll/terminate selected companies?', mtConfirmation, [mbYes, mbNo]) = mrYes then
  begin
    DoEnrollment;
    DM_TEMPORARY.TMP_CO.DataRequired('CL_NBR>0');
    SelectedCompanies.Close;
    SelectedCompanies.CreateDataSet;
  end;
end;

procedure TEFTPS_Enrollment.DoEnrollment;

  function StripChars(S: String): String;
  var
    I: Integer;
  begin
    Result := '';
    for I := 1 to Length(S) do
    if (S[I] >= '0') and (S[I] <= '9') then
      Result := Result + S[I];
  end;

  function EnsureStringLength(const MyString: Variant; const DesiredLength: Integer): string;
  begin
     Result := Copy(ConvertNull(MyString, '')+ StringOfChar(' ', DesiredLength), 1, DesiredLength);
  end;

var
  EnrollmentList: TStringList;
  BatchFilerID, FileSequenceNbr, CompanyName, Temp, ZipCode: string;
  FileDate: TDateTime;
begin
  if SelectedCompanies.RecordCount = 0 then
    raise EInvalidParameters.CreateHelp('No companies are selected!', IDH_InvalidParameters);

  EnrollmentList := TStringList.Create;
  ctx_StartWait;
  with SelectedCompanies, DM_COMPANY do
  try
    Temp := FormatDateTime('yyyymmddhhmm', Now);
    FileSequenceNbr := Copy(Temp, 10, 3);
    FileDate := Date;

    IndexFieldNames := 'CL_NBR';
    First;
    CO.DataRequired('CO_NBR=0');
      // To avoid a strange problem where first time you open a CO table it would be empty ???
    while not EOF do
    begin
      ctx_DataAccess.OpenClient(FieldByName('CL_NBR').AsInteger);
      CO.DataRequired('CO_NBR=' + FieldByName('CO_NBR').AsString);

      if ConvertNull(CO.FieldByName('EFTPS_NAME').Value, '') <> '' then
        CompanyName := CO.FieldByName('EFTPS_NAME').AsString
      else
      case CO.FieldByName('USE_DBA_ON_TAX_RETURN').AsString[1] of
      USE_DBA_ON_TAX_RETURN_DBA:
        CompanyName := CO.FieldByName('DBA').AsString;
      USE_DBA_ON_TAX_RETURN_LEGAL:
        CompanyName := CO.FieldByName('LEGAL_NAME').AsString;
      USE_DBA_ON_TAX_RETURN_PRIMARY:
        CompanyName := CO.FieldByName('NAME').AsString;
      end;

      ZipCode := Trim(CO.FieldByName('ZIP_CODE').AsString);
      if Pos('-', ZipCode) <> 0 then
        System.Delete(ZipCode, Pos('-', ZipCode), 1);
      if Pos(' ', ZipCode) <> 0 then
        System.Delete(ZipCode, Pos(' ', ZipCode), 1);

      if CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').IsNull then
        raise EInvalidParameters.CreateHelp('SB Tax Bank Account needs to be selected for company # '
          + CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '!', IDH_InvalidParameters);

      BatchFilerID := ConvertNull(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Lookup('SB_BANK_ACCOUNTS_NBR', CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').Value, 'BATCH_FILER_ID'), '');
      if BatchFilerID = '' then
        BatchFilerID := PadStringLeft(DM_SERVICE_BUREAU.SB.FieldByName('EFTPS_TIN_NUMBER').AsString, ' ', 9)
      else
        BatchFilerID := PadStringLeft(BatchFilerID, ' ', 9);

      Temp := BatchFilerID + GetMasterInquiryPIN(CO.FieldByName('TAX_SB_BANK_ACCOUNT_NBR').Value)
        + FormatDateTime('yyyymmdd', FileDate) + FileSequenceNbr
        + PadStringLeft(IntToStr(EnrollmentList.Count + 1), '0', 4);
      if rgEnroll.ItemIndex in [0, 1] then
        Temp := Temp + 'A'
      else
        Temp := Temp + 'T';
                     // UpperCase(EnsureStringLength(CompanyName, 35))
      Temp := Temp + UpperCase(EnsureStringLength(CompanyName, 35)) + 'B' {Business}
        + EnsureStringLength(CO.FieldByName('FEIN').AsString, 9)
        + EnsureStringLength(CO.FieldByName('PIN_NUMBER').AsString, 4) + '         ' + 'US' + CO.FieldByName('STATE').AsString
        + EnsureStringLength(ZipCode, 9) + '8655';
      if rgEnroll.ItemIndex in [1, 3] then
        Temp := Temp + 'C                              '
      else
      if CO.FieldByName('TAX_SERVICE').AsString <> TAX_SERVICE_FULL then
      begin
        Temp := Temp + 'DBN';
        DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('ALL');
        DM_SERVICE_BUREAU.SB_BANKS.DataRequired('ALL');
        DM_CLIENT.CL_BANK_ACCOUNT.Locate('CL_BANK_ACCOUNT_NBR', CO.FieldByName('TAX_CL_BANK_ACCOUNT_NBR').Value, []);
        Temp := Temp
          + EnsureStringLength(VarToStr(DM_SERVICE_BUREAU.SB_BANKS.Lookup('SB_BANKS_NBR', DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('SB_BANKS_NBR').Value, 'ABA_NUMBER')), 9)
          + EnsureStringLength(StripChars(DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('CUSTOM_BANK_ACCOUNT_NUMBER').AsString), 17)
          + DM_CLIENT.CL_BANK_ACCOUNT.FieldByName('BANK_ACCOUNT_TYPE').AsString + 'N';
      end
      else
        Temp := Temp + 'DBY                            ';
      EnrollmentList.Add(Temp);

      CO.Edit;
      if rgEnroll.ItemIndex in [0, 1] then
        CO.FieldByName('EFTPS_ENROLLMENT_STATUS').Value := EFT_ENROLLMENT_STATUS_PENDING
      else
        CO.FieldByName('EFTPS_ENROLLMENT_STATUS').Value := EFT_ENROLLMENT_STATUS_TERM;
      CO.FieldByName('EFTPS_ENROLLMENT_DATE').Value := FileDate;
      CO.FieldByName('EFTPS_SEQUENCE_NUMBER').Value := FileSequenceNbr;
      CO.FieldByName('EFTPS_ENROLLMENT_NUMBER').Value := PadStringLeft(IntToStr(EnrollmentList.Count), '0', 4);
      CO.Post;
      ctx_DataAccess.PostDataSets([CO]);

      Next;
    end;
    if EnrollmentList.Count > 0 then
    begin
      SaveDialog1.FileName := mb_AppSettings['EFTPSEnrollmentFile'];
      SafeFileSave(EnrollmentList, SaveDialog1);
      mb_AppSettings['EFTPSEnrollmentFile'] := SaveDialog1.FileName;
    end;
  finally
    EnrollmentList.Free;
    ctx_EndWait;
  end;
end;

procedure TEFTPS_Enrollment.wwdbgridSelectClientCalcCellColors(
  Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  inherited;
  if TevDBGrid(Sender).DataSource.DataSet.FieldByName('EFTPS_ENROLLMENT_STATUS').IsNull
  or (TevDBGrid(Sender).DataSource.DataSet.FieldByName('EFTPS_ENROLLMENT_STATUS').AsString[1] in [EFT_ENROLLMENT_STATUS_NONE,
  EFT_ENROLLMENT_STATUS_TERM]) then
    ABrush.Color := clRed;
end;


{
procedure TEFTPS_Enrollment.CopyCompany;
begin
  with wwdsTempTable.DataSet do
  if not SelectedCompanies.Locate('CL_NBR;CO_NBR', VarArrayOf([FieldByName('CL_NBR').Value, FieldByName('CO_NBR').Value]), []) then
  begin
    SelectedCompanies.Append;
    SelectedCompanies.FieldByName('CL_NBR').Value:=FieldByName('CL_NBR').Value;
    SelectedCompanies.FieldByName('CO_NBR').Value:=FieldByName('CO_NBR').Value;
    SelectedCompanies.FieldByName('CUSTOM_COMPANY_NUMBER').Value:=FieldByName('CUSTOM_COMPANY_NUMBER').Value;
    SelectedCompanies.FieldByName('NAME').Value:=FieldByName('NAME').Value;
    SelectedCompanies.FieldByName('EFTPS_ENROLLMENT_STATUS').Value:=FieldByName('EFTPS_ENROLLMENT_STATUS').Value;
    SelectedCompanies.Post;
  end;
end;
}

procedure TEFTPS_Enrollment.bbtnEnrollmentResponseClick(Sender: TObject);
begin
  inherited;
  OpenDialog1.FileName := mb_AppSettings['EFTPSEnrollmentResponseFile'];
  if OpenDialog1.Execute then
  begin
    rgrpSelected.ItemIndex := 0;
    rgrpSelectedClick(Self);
    if DM_SERVICE_BUREAU.SB.FieldByName('EFTPS_BANK_FORMAT').AsString <> GROUP_BOX_BATCH_PROVIDER then
      DoEnrollmentResponse(OpenDialog1.FileName);


      //  By Ca
      //   After Added new page we dont need this part
//     else
//       DoBatchEnrollmentResponse(OpenDialog1.FileName);

    mb_AppSettings['EFTPSEnrollmentResponseFile'] := OpenDialog1.FileName;
    DM_TEMPORARY.TMP_CO.DataRequired('CL_NBR>0');
  end;
end;

procedure TEFTPS_Enrollment.DoEnrollmentResponse(FileName: TFileName);
var
  F, T: TextFile;
  C: Char;
  S, EnrStatus, SeqNbr, EnrNbr: string;
  EnrDate: TDateTime;
begin
  lboxSucceeded.Clear;
  lboxRejected.Clear;
  AssignFile(F, ExtractFilePath(FileName) + 'enroll.sav');
  AssignFile(T, FileName);
  ReWrite(F);
  Reset(T);
  while not EOF(T) do
  begin
    Read(T, C);
    if C = Chr(10) then
      Writeln(F)
    else
      Write(F, C);
  end;
  CloseFile(T);
  Reset(F);

  ctx_StartWait;
  try
    DM_COMPANY.CO.DataRequired('CO_NBR=0');
    // To avoid a strange problem where first time you open a CO table it would be empty ???
    with DM_COMPANY do
      while not EOF(F) do
      begin
        Readln(F, S);
        if S <> '' then
        begin
          EnrDate := StrToDate(Copy(S, 14, 2) + '/' + Copy(S, 16, 2) + '/' + Copy(S, 10, 4));
          SeqNbr := Trim(Copy(S, 18, 3));
          EnrNbr := Trim(Copy(S, 21, 4));
          EnrStatus := Copy(S, 29, 4);
          if wwdsTempTable.DataSet.Locate('EFTPS_ENROLLMENT_DATE;EFTPS_SEQUENCE_NUMBER;EFTPS_ENROLLMENT_NUMBER', VarArrayOf([EnrDate, SeqNbr, EnrNbr]), []) then
          begin
            ctx_DataAccess.OpenClient(wwdsTempTable.DataSet.FieldByName('CL_NBR').AsInteger);
            CO.DataRequired('CO_NBR=' + wwdsTempTable.DataSet.FieldByName('CO_NBR').AsString);
            CO.Edit;
            if (EnrStatus = '0000') or (EnrStatus = '4001') or (EnrStatus = '4002') or (EnrStatus = '4004') then
            begin
              if (EnrStatus = '0000') and (CO.FieldByName('EFTPS_ENROLLMENT_STATUS').Value = EFT_ENROLLMENT_STATUS_TERM) then
              begin
                CO.FieldByName('PIN_NUMBER').Value := 'TERM';
                lboxSucceeded.Items.Add(CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + CO.FieldByName('NAME').AsString +
                  ' - Terminated');
              end
              else
              begin
                CO.FieldByName('EFTPS_ENROLLMENT_STATUS').Value := EFT_ENROLLMENT_STATUS_ACTIVE;
                case DM_SERVICE_BUREAU.SB.FieldByName('EFTPS_BANK_FORMAT').AsString[1] of
                GROUP_BOX_CHICAGO:
                  CO.FieldByName('SY_FED_TAX_PAYMENT_AGENCY_NBR').Value := 3 {First Chicago Bank};
                GROUP_BOX_NATIONS:
                  CO.FieldByName('SY_FED_TAX_PAYMENT_AGENCY_NBR').Value := 4 {Nations Bank};
                end;
                CO.FieldByName('FEDERAL_TAX_PAYMENT_METHOD').Value := TAX_PAYMENT_METHOD_DEBIT;
                CO.FieldByName('PIN_NUMBER').Value := Copy(S, 25, 4);
                lboxSucceeded.Items.Add(CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + CO.FieldByName('NAME').AsString +
                  ' - Enrolled');
              end;
              CO.FieldByName('EFTPS_ENROLLMENT_NUMBER').Value := EnrStatus;
            end
            else if (EnrStatus <> '9999') then
            begin
              CO.FieldByName('EFTPS_ENROLLMENT_STATUS').Value := EFT_ENROLLMENT_STATUS_NONE;
              CO.FieldByName('EFTPS_ENROLLMENT_DATE').Value := Null;
              CO.FieldByName('EFTPS_SEQUENCE_NUMBER').Value := Null;
              CO.FieldByName('EFTPS_ENROLLMENT_NUMBER').Value := Null;

              lboxRejected.Items.Add(wwdsTempTable.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' +
                wwdsTempTable.DataSet.FieldByName('NAME').AsString + ' - ' + EnrStatus);
            end;
            CO.Post;
            ctx_DataAccess.PostDataSets([CO]);
          end;
        end;
      end;
  finally
    ctx_EndWait;
    CloseFile(F);
    Erase(F);
  end;
end;

procedure TEFTPS_Enrollment.bbtnPrintRejectionsClick(Sender: TObject);
var
  I: Integer;
  rtDest: TRect;
begin
  inherited;
  if lboxRejected.Items.Count = 0 then
    Exit;
  if PrintDialog1.Execute then
  begin
    Printer.BeginDoc;
    Printer.Canvas.Font := lboxRejected.Font;
    for I := 0 to lboxRejected.Items.Count - 1 do
    begin
      rtDest := Rect(100, 100 * (I + 2), 200, 100 * (I + 3));
      DrawText(Printer.Canvas.Handle, PChar(lboxRejected.Items[I]), Length(lboxRejected.Items[I]), rtDest, DT_NOCLIP or DT_NOPREFIX or DT_LEFT);
    end;
    Printer.EndDoc;
  end;
end;


procedure TEFTPS_Enrollment.btnEnrollmentRejectPrintClick(Sender: TObject);
var
  I: Integer;
  rtDest: TRect;
begin
  inherited;
  if lbRejected.Items.Count = 0 then
    Exit;
  if PrintDialog1.Execute then
  begin
    Printer.BeginDoc;
    Printer.Canvas.Font := lbRejected.Font;
    for I := 0 to lbRejected.Items.Count - 1 do
    begin
      rtDest := Rect(100, 100 * (I + 2), 200, 100 * (I + 3));
      DrawText(Printer.Canvas.Handle, PChar(lbRejected.Items[I]), Length(lbRejected.Items[I]), rtDest, DT_NOCLIP or DT_NOPREFIX or DT_LEFT);
    end;
    Printer.EndDoc;
  end;

end;


procedure TEFTPS_Enrollment.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  AllowChange := True;
end;

procedure TEFTPS_Enrollment.Activate;
begin
  inherited;
  DM_SERVICE_BUREAU.SB.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');

  DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.DataRequired('ALL');
  DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.DataRequired('ALL');
  ApplyFilter;


  EnrollmentsCfg := TStringList.Create;
  EnrollmentsCfg.Text := EnrollmentsConfig;

end;

procedure TEFTPS_Enrollment.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck([DM_SERVICE_BUREAU.SB,
    DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS,
    DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY], SupportDataSets, '');
end;

procedure TEFTPS_Enrollment.ApplyFilter;
var
 F:string;
begin
  with wwdsTempTable.DataSet do
  begin
    //Filtered := False;
    F := 'TERMINATION_CODE <>'''+TERMINATION_SAMPLE+''' ';
    case rgrpSelected.ItemIndex of
      //0:
      //  F := '';
      1:
        F := F + ' and  (EFTPS_ENROLLMENT_STATUS=''' + EFT_ENROLLMENT_STATUS_PENDING + ''' or EFTPS_ENROLLMENT_STATUS=''' +
          EFT_ENROLLMENT_STATUS_ACTIVE + ''')';
      2:
        F := F +'  and  EFTPS_ENROLLMENT_STATUS<>''' + EFT_ENROLLMENT_STATUS_PENDING + ''' and EFTPS_ENROLLMENT_STATUS<>''' +
          EFT_ENROLLMENT_STATUS_ACTIVE + ''' and EFTPS_ENROLLMENT_STATUS<>'''+EFT_ENROLLMENT_STATUS_TERM+'''';
    end;
    if cbTaxOnly.Checked then
    begin
        F := F + ' and TAX_SERVICE=''Y'''
    end;

    if DM_TEMPORARY.TMP_CO.UserFiltered then
         F := DM_TEMPORARY.TMP_CO.UserFilter + ' and ' + F;
    Filter := F;
    Filtered := True;
  end;
end;

procedure TEFTPS_Enrollment.cbTaxOnlyClick(Sender: TObject);
begin
  inherited;
  ApplyFilter;
end;

procedure TEFTPS_Enrollment.Deactivate;
begin
  inherited;
  wwdsTempTable.DataSet.Filtered := False;
  wwdsTempTable.DataSet.Filter := '';

  EnrollmentsCfg.Free;
end;



procedure TEFTPS_Enrollment.DoBatchPaymentResponse(FileName: TFileName);
  function GetCorrectTaxType(TaxType: String): String;
  begin
    if Pos('940', TaxType) <> 0 then
      Result := '0'//'09405'
    else
      if Pos('941', TaxType) <> 0 then
        Result := '1'//'94105'
    else
      if Pos('943', TaxType) <> 0 then
        Result := '3'//'09435'
   else
      if Pos('944', TaxType) <> 0 then
        Result := '4'//'94405'
    else
      if Pos('945', TaxType) <> 0 then
        Result := '5';//'09455'
  end;
  function CompressAmount(aString: String): String;
  var
    AmountAsInt: Int64;
  begin
    AmountAsInt := StrToInt64(aString);
    Result := IntToHex(AmountAsInt, 9);
  end;
var
  F: TextFile;
  S, sFileDate, sPaymentAmount, Tin, PmtStatus, RefNbr: string;
  FileStringList, MyStringList, MyStringList2, MyTraceList : TStringList;
  PassAttemptNbr: Integer;
begin
  lboxSucceeded.Clear;
  lboxRejected.Clear;
  AssignFile(F, FileName);
  Reset(F);

  FileStringList := TStringList.Create;
  MyStringList := TStringList.Create;
  MyStringList2 := TStringList.Create;
  MyTraceList := TStringList.Create;

  ctx_StartWait;
  try
    DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.DataRequired('CL_NBR>0');

    while not EOF(F) do
    begin
      Readln(F, S);
      FileStringList.CommaText := StringReplace(S, ' ', ';', [rfReplaceAll]);
      Tin := FileStringList[0];
      try
        sFileDate := Copy(FileStringList[17], 3, 6);
        sPaymentAmount := Copy(FileStringList[12],5, 11);
        RefNbr := sFileDate + Copy(FileStringList[11], 3, 4) + GetCorrectTaxType(FileStringList[10]) + CompressAmount(sPaymentAmount);
      except
        raise EevException.Create('Wrong response file format detected for Batch Provider.');
      end;
      //Get list of companies for the correspsonding TIN
      DM_TEMPORARY.TMP_CO.DataRequired('FEIN='+QuotedStr(Tin) + GetReadOnlyClintCompanyListFilter);
      PassAttemptNbr := 0;
      if DM_TEMPORARY.TMP_CO.RecordCount = 0 then
          lboxRejected.Items.Add(Tin + ' ' + sFileDate + ' ' + sPaymentAmount + ' - ' + 'Could not find the EIN');

      DM_TEMPORARY.TMP_CO.First;
      while not DM_TEMPORARY.TMP_CO.Eof do
      begin
        if DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.Locate('CL_NBR;CO_NBR;TAX_PAYMENT_REFERENCE_NUMBER', VarArrayOf([DM_TEMPORARY.TMP_CO['CL_NBR'], DM_TEMPORARY.TMP_CO['CO_NBR'], RefNbr]), []) then
        begin
          PmtStatus := FileStringList[15];

          if FileStringList[2] <> '' then
          begin
            MyStringList.Add(RefNbr + '=' + '0000');
            MyStringList2.Add(FileStringList[2]);  //Confirmation Number
            MyTraceList.Add(FileStringList[14]);  //Trace Number
          end
          else
          begin
            MyStringList.Add(RefNbr + '=' + '9999');
            MyStringList2.Add(FileStringList[2]);  //Confirmation Number
            MyTraceList.Add(FileStringList[14]);  //Trace Number
          end;

          wwdsTempTable.DataSet.Locate('CL_NBR;CO_NBR', VarArrayOf([DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('CL_NBR').Value,
            DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('CO_NBR').Value]), []);
          if FileStringList[2] <> '' then
            lboxSucceeded.Items.Add(wwdsTempTable.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + RefNbr + ' - Paid')
          else
            lboxRejected.Items.Add(wwdsTempTable.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + RefNbr + ' - Rejected');
          Break;
        end
        else
        begin
          PassAttemptNbr := PassAttemptNbr + 1;       //  added this line because of ticket number 51290
            //   this one take off because of Ticket number 47724   //   rollback because of ticekt number 51290
          if PassAttemptNbr = DM_TEMPORARY.TMP_CO.RecordCount then
            lboxRejected.Items.Add(Tin + ' ' + sFileDate + ' ' + sPaymentAmount + ' - ' + 'Match not Found');
        end;
        DM_TEMPORARY.TMP_CO.Next;
      end;
    end;
    ProcessPaymentResponse(MyStringList, MyStringList2, MyTraceList, True);
  finally
    DM_TEMPORARY.TMP_CO.DataRequired('');
    MyStringList.Free;
    MyStringList2.Free;
    MyTraceList.Free;
    FileStringList.Free;
    CloseFile(F);
    ctx_EndWait;
  end;
end;




procedure TEFTPS_Enrollment.DoPaymentResponse(FileName: TFileName);
var
  F, T: TextFile;
  C: Char;
  S, PmtStatus, RefNbr: string;
  MyStringList, MyStringList2, MyTraceList: TStringList;
begin
  lboxSucceeded.Clear;
  lboxRejected.Clear;
  AssignFile(F, ExtractFilePath(FileName) + 'payment.sav');
  AssignFile(T, FileName);
  ReWrite(F);
  Reset(T);
  while not EOF(T) do
  begin
    Read(T, C);
    if C = Chr(10) then
      Writeln(F)
    else
      Write(F, C);
  end;
  CloseFile(T);

  Reset(F);
  MyStringList := TStringList.Create;
  MyStringList2 := TStringList.Create;
  MyTraceList := TStringList.Create;
  ctx_StartWait;
  try
    DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.DataRequired('CL_NBR>0');
    while not EOF(F) do
    begin
      Readln(F, S);

      if Pos(',', S) = -1 then
        raise EevException.Create('Wrong response file format detected for Bank of America/Anexsys.');

      RefNbr := Copy(S, 10, 15);
      if DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.Locate('TAX_PAYMENT_REFERENCE_NUMBER', 'EFTD#' + RefNbr, [])
      or DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.Locate('TAX_PAYMENT_REFERENCE_NUMBER', RefNbr, []) then
      begin
        PmtStatus := Copy(S, 40, 4);
        if PmtStatus <> '9999' then
        begin
          MyStringList.Add(RefNbr + '=' + PmtStatus);
          MyStringList2.Add(Copy(S, 25, 15));
        end;
        wwdsTempTable.DataSet.Locate('CL_NBR;CO_NBR', VarArrayOf([DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('CL_NBR').Value,
          DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('CO_NBR').Value]), []);
        if PmtStatus = '0000' then
          lboxSucceeded.Items.Add(wwdsTempTable.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + RefNbr + ' - Paid')
        else
          if PmtStatus = '5122' then
            lboxSucceeded.Items.Add(wwdsTempTable.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + RefNbr + ' - Duplicate Payment Accepted')
        else
          if PmtStatus = '5125' then
            lboxSucceeded.Items.Add(wwdsTempTable.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + RefNbr + ' - Paid After 8 PM EST')
        else
          if PmtStatus <> '9999' then
          lboxRejected.Items.Add(wwdsTempTable.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + RefNbr + ' - ' + PmtStatus);
      end;
    end;
    ProcessPaymentResponse(MyStringList, MyStringList2, MyTraceList);
  finally
    MyStringList.Free;
    MyStringList2.Free;
    MyTraceList.Free;
    CloseFile(F);
    Erase(F);
    ctx_EndWait;
  end;

end;


procedure TEFTPS_Enrollment.rgrpSelectedClick(Sender: TObject);
begin
  inherited;
  ApplyFilter;
end;


procedure TEFTPS_Enrollment.bbtnPaymentResponseClick(Sender: TObject);
begin
  inherited;
  OpenDialog1.FileName := mb_AppSettings['EFTPSPaymentResponseFile'];
  if OpenDialog1.Execute then
  begin
    rgrpSelected.ItemIndex := 0;
    rgrpSelectedClick(Self);
    if DM_SERVICE_BUREAU.SB.FieldByName('EFTPS_BANK_FORMAT').AsString <> GROUP_BOX_BATCH_PROVIDER then
      DoPaymentResponse(OpenDialog1.FileName)
    else
      DoBatchPaymentResponse(OpenDialog1.FileName);
    mb_AppSettings['EFTPSPaymentResponseFile'] := OpenDialog1.FileName;
  end;
end;



 //   Batch Enrollment Response

procedure TEFTPS_Enrollment.CreateImportDataSets(Fname: String; Cfg: TStringList; aDataSet: TevClientDataSet);
type
  TRecord = record
    Field: TField;
    StartPos, StopPos: Integer;
  end;
  TRecordArray = array of TRecord;
var
  RecordArray: TRecordArray;

  procedure CreateConfigDataSets(var CfgList: TStringList);
  var
    I: Integer;
    InputString: String;
  begin
    SetLength(RecordArray, cfglist.Count);
    for I := 0 to cfglist.Count - 1 do
      with RecordArray[i] do
      begin
        InputString := cfglist.values[cfglist.Names[I]];
        Field := aDataSet.FindField(cfglist.Names[I]);
        StartPos := StrToInt(Copy(InputString, 1, Pos(',', InputString)-1));
        StopPos :=  StrToInt(Copy(InputString, Pos(',', InputString)+1, Length(InputString)));
      end;
  end;
  function GetRecord(var sLineText: String; aDelimeter: String; aQualifier: String): String;
  begin

    if Pos(aQualifier+aDelimeter+aQualifier, sLineText) > 0 then
    begin
      Result := Copy(sLineText, 1, Pos(aDelimeter, sLineText)-1);
      if Trim(Result) = aQualifier+'/  /'+aQualifier then
        Result := '';
      Delete(sLineText, 1, Pos(aDelimeter, sLineText));
    end
    else
      Result := sLineText;

    Result := StringReplace(Result, aQualifier, '', [rfReplaceAll, rfIgnoreCase]);

  end;


var
  F: TextFile;
  S, S1, FileType, Delimeter, Qualifier, WaitString: String;
  I: Integer;

  Pin, EnrStatus, EnrNbr, TaxpayerName, TaxpayerTin: string;
  EnrDate: TDateTime;

begin
  WaitString := 'Please wait while '+Fname+' file is being read... This could take as long as 2 minutes depending on the size!';
  if not FileExists(FName) then
    raise EInvalidParameters.CreateHelp('One or more of the Key files need for the import does not exist in the specified path.', IDH_BadPath);

  if not aDataSet.Active then
    aDataSet.CreateDataSet;


  if aDataSet.RecordCount = 0 then
  begin
    ctx_StartWait(WaitString);
    CreateConfigDataSets(Cfg);
    AssignFile(F, FName);
    Reset(F);

    aDataSet.DisableControls;

    try


      //FileType F = fixed length, D = delimeted
      {
      if evrbFixed.Checked then
        FileType := 'F'
      else
      begin
       }
        FileType := 'D';
        {
        if evrbComma.Checked then
        }
          Delimeter := ','   ;
{
        else if evrbSemiColon.Checked then
          Delimeter := ';'
        else if evrbTab.Checked then
          Delimeter := #9
        else
          Delimeter := evedOther.Text;
      end;

      case evcbQualifier.ItemIndex of
        1: Qualifier := '''';
        2: Qualifier := '"';
      else
      }
        Qualifier := '';
        {
      end;
         }


      if not evCsEvo.Active then
       Begin
         evCsEvo.CreateDataSet;
         evDsEvo.Active := True;
       end;

      DM_TEMPORARY.TMP_CO.Filtered :=False;
      DM_TEMPORARY.TMP_CO.Filter :=' EFTPS_ENROLLMENT_STATUS='+'''' + EFT_ENROLLMENT_STATUS_PENDING + '''';
      DM_TEMPORARY.TMP_CO.Filtered := True;

 //     DM_TEMPORARY.TMP_CO.DataRequired('EFTPS_ENROLLMENT_STATUS=''' + EFT_ENROLLMENT_STATUS_PENDING + '''');

      DM_TEMPORARY.TMP_CO.DataRequired('ALL');

      DM_TEMPORARY.TMP_CO.First;
      while not DM_TEMPORARY.TMP_CO.Eof do
      begin

       if DM_TEMPORARY.TMP_CO.FieldByName('EFTPS_ENROLLMENT_STATUS').AsString = 'P' then
        Begin
          evCsEvo.Insert;
          evCsEvo.FieldbyName('CO_NBR').AsInteger := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger;
          evCsEvo.FieldbyName('CL_NBR').AsInteger := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsInteger;
          evCsEvo.FieldbyName('CUSTOM_COMPANY_NUMBER').AsString := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
          evCsEvo.FieldbyName('NAME').AsString := DM_TEMPORARY.TMP_CO.FieldByName('NAME').AsString;
          evCsEvo.FieldbyName('FEIN').AsString := DM_TEMPORARY.TMP_CO.FieldByName('FEIN').AsString;
          evCsEvo.FieldbyName('EFTPS_ENROLLMENT_STATUS').AsString := DM_TEMPORARY.TMP_CO.FieldByName('EFTPS_ENROLLMENT_STATUS').AsString;
          evCsEvo.FieldbyName('EFTPS_ENROLLMENT_DATE').AsDateTime  := DM_TEMPORARY.TMP_CO.FieldByName('EFTPS_ENROLLMENT_DATE').AsDateTime;

          evCsEvo.post;
        End;

        DM_TEMPORARY.TMP_CO.Next;
      end;

      while not System.Eof(F) do
      begin
        Readln(F, S);

        aDataSet.Append;

        for i := 0 to High(RecordArray) do
        begin
        {
          if FileType = 'F' then
          begin
            S1 := Trim(Copy(S, RecordArray[i].StartPos, RecordArray[i].StopPos));
            if S1 = '/  /' then
              S1 := '';
            RecordArray[i].Field.AsString := S1;
          end
          else
          begin
          }
            S1 := Trim(GetRecord(S, Delimeter, Qualifier));
            if S1 = '/  /' then
              S1 := '';
            RecordArray[i].Field.AsString := S1;
           {
          end;
          }
        end;

   //    Enrolment Response Match Process

        lbSucceeded.Clear;
        lbRejected.Clear;

        ctx_StartWait;
        try

          DM_COMPANY.CO.DataRequired('CO_NBR=0'); //  ???
    // To avoid a strange problem where first time you open a CO table it would be empty ???

            EnrDate := StrToDate(Copy(RecordArray[2].Field.Text, 5, 2) + '/' + Copy(RecordArray[2].Field.Text, 7, 2) + '/' + Copy(RecordArray[2].Field.Text, 1, 4));

            EnrNbr := RecordArray[4].Field.Text;
            Pin := RecordArray[9].Field.Text;
            TaxpayerName := StringReplace(RecordArray[6].Field.Text, ';', ' ', [rfReplaceAll]);
            TaxpayerTin := RecordArray[8].Field.Text;
            EnrStatus := '';

            evDsEvo.DataSet.FilterOptions := [foCaseInsensitive];
            evDsEvo.DataSet.Filter  :=
            'EFTPS_ENROLLMENT_DATE ='+ ''''+ DateToStr(EnrDate) + '''' +' and FEIN = ' + '''' + TaxpayerTin + '''' + ' and ' +  'NAME = ' + '''' + TaxpayerName + '''';
            evDsEvo.DataSet.Filtered := true;

            aDataSet.FieldByName('HasBeenMatched').AsString := 'F';  //  There is more than one record

            if evDsEvo.DataSet.RecordCount = 1 then    //  control if there is one record match control
            begin
             aDataSet.FieldByName('HasBeenMatched').AsString :=
                   EnrollmentResponseMatchControl(Pin, EnrNbr, TaxPayerTin);

               if aDataSet.FieldByName('HasBeenMatched').AsString = 'T' then
               Begin
                 evDsEvo.DataSet.Edit;
                 evDsEvo.DataSet.FieldByName('EFTPS_ENROLLMENT_STATUS').Value := 'A';
                 evDsEvo.DataSet.Post;
               end;


            end;

        finally
          ctx_EndWait;
        end;

        aDataSet.Post;
        ctx_UpdateWait(WaitString);

      end;

    finally
      CloseFile(F);
      ctx_EndWait;
      aDataSet.EnableControls;
      EnrollmentResponseApplyFilter;
      evCsFile.Filtered := True;
      wwgdFile.RefreshDisplay;
    end;

  end;

end;




function TEFTPS_Enrollment.EnrollmentResponseMatchControl(var Pin : String; EnrNbr : String; TaxPayerTin : String): String;
var
  EnrStatus : String;
  Status : String;
Begin
          EnrStatus :='';
          Status := '';
          with DM_COMPANY do
          Begin

           ctx_DataAccess.OpenClient(evDsEvo.DataSet.FieldByName('CL_NBR').AsInteger);
           CO.DataRequired('CO_NBR=' + evDsEvo.DataSet.FieldByName('CO_NBR').AsString);

           if (CO.FieldByName('PIN_NUMBER').AsString=Pin) and
              (CO.FieldByName('EFTPS_ENROLLMENT_STATUS').Value =EFT_ENROLLMENT_STATUS_ACTIVE) then
            Begin
             Result := 'T';
             lbSucceeded.Items.Add(CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + CO.FieldByName('NAME').AsString +
                  ' - Before Recorded');
            End
            else
            Begin

              //0000T is a made code signifying sucessful Termination
              //0000E is a made code signifying sucessful Enrollment

             if ((Pin = '0000') and (CO.FieldByName('PIN_NUMBER').AsString <> '')) then
               EnrStatus := '0000T'
             else if (Pin <> '0000') and (Pin  <> '9999') then
               EnrStatus := '0000E'
             else
                Begin
                   EnrStatus := '';
                   if ((Pin = '0000') and (CO.FieldByName('PIN_NUMBER').AsString = '')) then
                    begin
                      Result:='T';
                      EnrollmentResponseMatch(TaxpayerTin, Pin, EnrNbr, Status);
                      exit;
                     end;

                   if ((Pin = '9999') and (CO.FieldByName('PIN_NUMBER').AsString = '')) then
                    begin
                      Result:='T';
                      EnrollmentResponseMatch(TaxpayerTin, Pin, EnrNbr, Status);
                      exit;
                     end;

                  if ((Pin = '') and (CO.FieldByName('PIN_NUMBER').AsString = '')) then
                    begin
                      Result:='T';
                      EnrollmentResponseMatch(TaxpayerTin, Pin, EnrNbr, Status);
                      exit;
                     end;
                end;


             if (EnrStatus = '0000E') or (EnrStatus = '0000T') then
             begin
               if (EnrStatus = '0000T') then
               begin
                 Status := EFT_ENROLLMENT_STATUS_TERM;
               end
               else
               begin
                 Status :=EFT_ENROLLMENT_STATUS_ACTIVE;
               end;

               Result := 'T';

               EnrollmentResponseMatch(TaxpayerTin, Pin, EnrNbr, Status);

             end
             else
             begin
               Result := 'F';
             end;
           end;
          end;
End;


procedure TEFTPS_Enrollment.EnrollmentResponseMatch(var TaxPayerTIN : String; TaxPayerPIN : String; EnrNbr : String; Status : String);
 begin

   if not evCsMatched.Active then
      evCsMatched.CreateDataSet;

   with evCsMatched do
   begin
       Insert;

       FieldbyName('CO_NBR').AsInteger := evDsEvo.DataSet.FieldByName('CO_NBR').AsInteger;
       FieldbyName('CL_NBR').AsInteger := evDsEvo.DataSet.FieldByName('CL_NBR').AsInteger;
       FieldbyName('CUSTOM_COMPANY_NUMBER').AsString := evDsEvo.DataSet.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
       FieldbyName('NAME').AsString := evDsEvo.DataSet.FieldByName('NAME').AsString;
       FieldbyName('FEIN').AsString := evDsEvo.DataSet.FieldByName('FEIN').AsString;

       FieldbyName('TaxPayerTIN').AsString := TaxPayerTIN;
       FieldbyName('TaxPayerPIN').AsString := TaxPayerPIN;
       FieldbyName('EnrollmentNumber').AsString := EnrNbr;
       FieldbyName('EFTPS_ENROLLMENT_STATUS').AsString := Status;

       Post;
   end;
end;






procedure TEFTPS_Enrollment.btnEnrolmentFileClick(Sender: TObject);
begin
 inherited;
  OpenDialog1.FileName := mb_AppSettings['EFTPSPaymentResponseFile'];
  if OpenDialog1.Execute then
   Begin
    CreateImportDataSets(OpenDialog1.FileName, EnrollmentsCfg, evCsFile);

    mb_AppSettings['EFTPSEnrollmentResponseFile'] := OpenDialog1.FileName;
   end;
end;

procedure TEFTPS_Enrollment.EnrollmentResponseApplyFilter;
begin
  with evDsEvo.DataSet do
  begin
    Filtered := False;
    Filter  := ' EFTPS_ENROLLMENT_STATUS='+'''' + EFT_ENROLLMENT_STATUS_PENDING + '''';
    Filtered := True;
  end;
end;


procedure TEFTPS_Enrollment.PageControl1Change(Sender: TObject);
begin
  inherited;

   if PageControl1.ActivePageIndex = 0 then
       ApplyFilter;

end;

procedure TEFTPS_Enrollment.btnAddClick(Sender: TObject);
var
  Pin : String;
  EnrNbr : String;
  TaxPayerTin : String;

begin

//   Inherited;

  try
    if wwgdFile.SelectedList.Count = 0 then
    begin
      EvMessage('You have not yet selected any items in the file list to match! Please do so at this time!');
      Exit;
    end;
    if wwgdEvolution.SelectedList.Count = 0 then
    begin
      EvMessage('You have not yet selected any items in the file list to match! Please do so at this time!');
      Exit;
    end;

    if (wwgdEvolution.SelectedList.Count = 1) and (wwgdFile.SelectedList.Count = 1) then
    Begin

          evDsFile.Dataset.GotoBookmark(wwgdFile.SelectedList.Items[0]);
          evDsEvo.Dataset.GotoBookmark(wwgdEvolution.SelectedList.items[0]);

          Pin :=evDsFile.Dataset.FieldbyName('TaxPayerPin').AsString;
          EnrNbr :=evDsFile.Dataset.FieldbyName('EnrollmentNumber').AsString;
          TaxPayerTin := evDsFile.Dataset.FieldbyName('TaxPayerTIN').AsString;

          evDsFile.Dataset.Edit;
          evDsFile.Dataset.FieldByName('HasBeenMatched').AsString :=
             EnrollmentResponseMatchControl(Pin,EnrNbr,TaxPayerTin);
          if evDsFile.Dataset.FieldByName('HasBeenMatched').AsString = 'T' then
           Begin
             evCsEvo.Edit;
             evCsEvo.FieldByName('EFTPS_ENROLLMENT_STATUS').AsString := 'A';
             evCsEvo.post;
           End;

          evDsFile.Dataset.Post ;

          wwgdFile.UnselectAll;
          wwgdEvolution.UnselectAll;

    end
    else
    begin
     EvMessage('Please only one record!');
     Exit;
    end;

  finally
      EnrollmentResponseApplyFilter;
      evCsFile.Filtered := True;
      wwgdFile.RefreshDisplay;
      wwgdEvolution.RefreshDisplay;
  end;

end;

procedure TEFTPS_Enrollment.btnRemoveClick(Sender: TObject);
var
  Pin : String;
  TaxPayerTin : String;

  I: Integer;
begin
//   inherited;

  try
    if wwgdMatched.SelectedList.Count = 0 then
    begin
      EvMessage('You have not yet selected any items in the file list to match! Please do so at this time!');
      Exit;
    end;

    with wwgdMatched do
    begin
       evCsFile.Filtered := False;
      for I := 0 to SelectedList.Count - 1 do
      begin

          evCsMatched.GotoBookmark(SelectedList.items[I]);

          Pin :=evCsMatched.FieldbyName('TaxPayerPIN').AsString;
          TaxPayerTin := evCsMatched.FieldbyName('TaxPayerTIN').AsString;

          if evCsFile.Locate('TaxPayerPin', evCsMatched.FieldbyName('TaxPayerPIN').Value, []) then
           Begin
            evCsFile.Edit;
            evCsFile.FieldByName('HasBeenMatched').AsString := 'F';
            evCsFile.Post;
           End;


          evDsEvo.DataSet.Filtered := False;
          evDsEvo.DataSet.Filter  := '' ;
          evDsEvo.DataSet.Filtered := true;
          if evCsEvo.Locate('CL_NBR;CO_NBR', VarArrayOf([evCsMatched.FieldbyName('CL_NBR').Value,evCsMatched.FieldbyName('CO_NBR').Value]), []) then
           Begin
             evCsEvo.Edit;
             evCsEvo.FieldByName('EFTPS_ENROLLMENT_STATUS').AsString := 'P';
             evCsEvo.post;
           End;


          evCsMatched.Delete;

    end;
   end;


  finally
      EnrollmentResponseApplyFilter;
      evCsFile.Filtered := True;
      wwgdFile.RefreshDisplay;
      wwgdEvolution.RefreshDisplay;
  end;

end;

procedure TEFTPS_Enrollment.btnApplyClick(Sender: TObject);
begin
 //  inherited;

    if not evCsMatched.Active then exit;

    evCsMatched.First;
    while not evCsMatched.EOF do
    begin

      if (evCsMatched.FieldbyName('TaxPayerPIN').AsString <>'')     and
         (evCsMatched.FieldbyName('TaxPayerPIN').AsString <>'0000') and
         (evCsMatched.FieldbyName('TaxPayerPIN').AsString <>'9999') then
        Begin

          with DM_COMPANY do
          Begin

             ctx_DataAccess.OpenClient(evCsMatched.FieldbyName('CL_NBR').AsInteger);
             CO.DataRequired('CO_NBR=' + evCsMatched.FieldbyName('CO_NBR').AsString);

              //0000T is a made code signifying sucessful Termination
              //0000E is a made code signifying sucessful Enrollment

             CO.Edit;

             if evCsMatched.FieldbyName('EFTPS_ENROLLMENT_STATUS').AsString = EFT_ENROLLMENT_STATUS_TERM then
              Begin
                 CO.FieldByName('EFTPS_ENROLLMENT_STATUS').Value := EFT_ENROLLMENT_STATUS_TERM;
                 CO.FieldByName('PIN_NUMBER').Value := 'TERM';

                lbSucceeded.Items.Add(CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + CO.FieldByName('NAME').AsString +
                  ' - Terminated');
              End;

             if evCsMatched.FieldbyName('EFTPS_ENROLLMENT_STATUS').AsString = EFT_ENROLLMENT_STATUS_ACTIVE then
              Begin
                 CO.FieldByName('EFTPS_ENROLLMENT_STATUS').Value := EFT_ENROLLMENT_STATUS_ACTIVE;
                 CO.FieldByName('SY_FED_TAX_PAYMENT_AGENCY_NBR').Value := 4 {Nations Bank};
                 CO.FieldByName('FEDERAL_TAX_PAYMENT_METHOD').Value := TAX_PAYMENT_METHOD_DEBIT;
                 CO.FieldByName('PIN_NUMBER').Value := evCsMatched.FieldbyName('TaxPayerPIN').AsString;

                 lbSucceeded.Items.Add(CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' + CO.FieldByName('NAME').AsString +
                   ' - Enrolled');
              End;

             CO.FieldByName('EFTPS_ENROLLMENT_NUMBER').Value := evCsMatched.FieldbyName('EnrollmentNumber').AsString;

             CO.Post;
             ctx_DataAccess.PostDataSets([CO]);

          end;

        end;

      if evCsMatched.FieldbyName('TaxPayerPIN').AsString = '9999' then
        Begin
         lbRejected.Items.Add(DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' ' +
                       DM_COMPANY.CO.FieldByName('NAME').AsString );
        end;


      evCsMatched.Next;
    end;

end;


procedure TEFTPS_Enrollment.PageControl1Enter(Sender: TObject);
begin
  inherited;

   if DM_SERVICE_BUREAU.SB.FieldByName('EFTPS_BANK_FORMAT').AsString <> GROUP_BOX_BATCH_PROVIDER then
     Begin
      PageControl1.Pages[1].Caption  := 'Response';
      TabSheet3.TabVisible := False;
      bbtnEnrollmentResponse.Visible := True;
     End
    else
     Begin
      TabSheet3.TabVisible :=True;
      bbtnEnrollmentResponse.Visible := False;
      PageControl1.Pages[1].Caption  := 'Payment Response';
     End;

   PageControl1.ActivatePage(TabSheet1);

end;

procedure TEFTPS_Enrollment.wwdbgridSelectClientFilterChange(
  Sender: TObject; DataSet: TDataSet; NewFilter: String);
begin
  inherited;
  ApplyFilter;
end;

procedure TEFTPS_Enrollment.SafeFileSave(const aList: TStringList;
  const aSaveDialog: TSaveDialog; const SkipDialog: Boolean);
var
  SuccessfulSave: Boolean;
begin
  repeat
    SuccessfulSave := True;
    try
      if not SkipDialog then
        repeat  until aSaveDialog.Execute;
      aList.SaveToFile(aSaveDialog.FileName);
    except
      on E: Exception do
      begin
        SuccessfulSave := False;
        EvMessage(E.Message, mtWarning, [mbOK]);
      end;
    end;
  until SuccessfulSave;
end;

initialization
  RegisterClass(TEFTPS_ENROLLMENT);

end.
