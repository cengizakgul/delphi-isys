// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PROCESS_REPORT_CREDIT_LIAB;

interface

uses
  SFrameEntry,  Db,  ComCtrls, StdCtrls, EvBasicUtils,
  Controls, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, Classes, Wwdatsrc, Buttons,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISUtils, EvDataAccessComponents,
  ISDataAccessComponents, EvContext, EvUIComponents, EvClientDataSet;

type
  TPROCESS_REPORT_CREDIT_LIAB = class(TFrameEntry)
    cdMain: TevClientDataSet;
    cdMainCUSTOM_CLIENT_NUMBER: TStringField;
    cdMainCLIENT_NAME: TStringField;
    cdMainCUSTOM_COMPANY_NUMBER: TStringField;
    cdMainCOMPANY_NAME: TStringField;
    cdMainTAX_SERVICE: TStringField;
    cdMainCL_NBR: TIntegerField;
    cdMainCO_NBR: TIntegerField;
    dsClCo: TevDataSource;
    wwDBGrid1: TevDBGrid;
    Label1: TevLabel;
    cbMonth: TevComboBox;
    Bevel1: TEvBevel;
    cdMainAMOUNT: TCurrencyField;
    cdMainTAX_PAYMENT_REFERENCE_NUMBER: TStringField;
    Label2: TevLabel;
    edYear: TevEdit;
    udYear: TUpDown;
    cdMainCO_TAX_DEPOSITS_NBR: TIntegerField;
    cdMainSTATUS: TStringField;
    bLoad: TevBitBtn;
    btnGo: TevBitBtn;
    procedure bLoadClick(Sender: TObject);
    procedure udYearChangingEx(Sender: TObject; var AllowChange: Boolean;
      NewValue: Smallint; Direction: TUpDownDirection);
    procedure cbMonthChange(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Activate; override;
  end;

var
  PROCESS_REPORT_CREDIT_LIAB: TPROCESS_REPORT_CREDIT_LIAB;

implementation

uses EvConsts, EvUtils, SDataStructure, SysUtils, isBaseClasses;

{$R *.DFM}

procedure TPROCESS_REPORT_CREDIT_LIAB.bLoadClick(Sender: TObject);
  procedure FillAmounts(const TableName: string);
  begin
    with TExecDSWrapper.Create('TmpDepositSummary') do
    begin
      SetMacro('TableName', TableName);
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
      ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
      ctx_DataAccess.TEMP_CUSTOM_VIEW.First;
      while not ctx_DataAccess.TEMP_CUSTOM_VIEW.Eof do
      begin
        if cdMain.Locate('cl_nbr;CO_TAX_DEPOSITS_NBR', ctx_DataAccess.TEMP_CUSTOM_VIEW['cl_nbr;CO_TAX_DEPOSITS_NBR'], []) then
        begin
          cdMain.Edit;
          cdMain['Amount'] := cdMain.FieldByName('Amount').AsCurrency+ ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('Amount').AsCurrency;
          cdMain.Post;
        end;
        ctx_DataAccess.TEMP_CUSTOM_VIEW.Next;
      end;
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
    end;
  end;
var
  d: TDateTime;
  d1, d2: string[10];
  cdClCo: TEvBasicClientDataSet;
begin
  d := EncodeDate(GetYear(Date), cbMonth.ItemIndex+ 1, 1);
  d1 := DateToStr(d);
  d2 := DateToStr(GetEndMonth(d)+1);
  wwDbGrid1.Visible := False;
  wwDbGrid1.UnselectAll;

  DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.DataRequired('STATUS_DATE between '''+ d1+ ''' and '''+ d2+
    ''' and STATUS in ('''+ TAX_DEPOSIT_STATUS_PAID+ ''','''+ TAX_DEPOSIT_STATUS_PAID_WO_FUNDS+ ''')'+ GetReadOnlyClintCompanyListFilter);
    
  cdMain.Close;
  cdMain.IndexFieldNames := '';
  ctx_StartWait;
  try
    cdMain.Data := CreateLookupData(DM_TEMPORARY.TMP_CO_TAX_DEPOSITS);
    cdClCo := TEvBasicClientDataSet.Create(nil);
    try
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
      ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(TExecDSWrapper.Create('SelectClientsCompanies').AsVariant);
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
      cdClCo.Data := ctx_DataAccess.TEMP_CUSTOM_VIEW.Data;
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;

      cdMain.IndexFieldNames := 'CL_NBR;CO_NBR';
      cdMain.First;
      while not cdMain.Eof do
      begin
        Assert(cdClCo.Locate('CL_NBR;CO_NBR', cdMain['CL_NBR;CO_NBR'], []));
        cdMain.Edit;
        cdMain['CUSTOM_CLIENT_NUMBER;CLIENT_NAME;CUSTOM_COMPANY_NUMBER;COMPANY_NAME;TAX_SERVICE'] :=
          cdClCo['CUSTOM_CLIENT_NUMBER;CLIENT_NAME;CUSTOM_COMPANY_NUMBER;COMPANY_NAME;TAX_SERVICE'];
        cdMain.Post;
        cdMain.Next;
      end;
      FillAmounts('TMP_CO_FED_TAX_LIABILITIES');
      FillAmounts('TMP_CO_LOCAL_TAX_LIABILITIES');
      FillAmounts('TMP_CO_STATE_TAX_LIABILITIES');
      FillAmounts('TMP_CO_SUI_LIABILITIES');
    finally
      cdClCo.Free;
    end;
    cdMain.First;
  finally
    ctx_EndWait;
    wwDBGrid1.Visible := True;
    wwDbGrid1.SelectRecord;
    btnGo.Visible := True;
  end;
end;

procedure TPROCESS_REPORT_CREDIT_LIAB.udYearChangingEx(Sender: TObject;
  var AllowChange: Boolean; NewValue: Smallint;
  Direction: TUpDownDirection);
begin
  inherited;
  edYear.Text := IntToStr(NewValue);
  wwDBGrid1.Visible := False;
  btnGo.Visible := False;
end;

procedure TPROCESS_REPORT_CREDIT_LIAB.cbMonthChange(Sender: TObject);
begin
  inherited;
  wwDBGrid1.Visible := False;
  btnGo.Visible := False;
end;

procedure TPROCESS_REPORT_CREDIT_LIAB.btnGoClick(Sender: TObject);
var
  UnpaidStatus: string[1];
  procedure Process(const ds: TevClientDataSet);
  var
    i: Integer;
    d: TISBasicClientDataSet;
  begin
    ds.DataRequired('CO_TAX_DEPOSITS_NBR = '+ IntToStr(cdMain['CO_TAX_DEPOSITS_NBR']));
    d := TISBasicClientDataSet.Create(nil);
    with d do
    try
      CloneCursor(ds, True);
      Last;
      while not Bof do
      begin
        ds.Append;
        for i := 0 to FieldCount-1 do
          if (Fields[i].FieldName = 'CO_NBR')
          or (Fields[i].FieldName = 'DUE_DATE')
          or (Fields[i].FieldName = 'TAX_TYPE')
          or (Fields[i].FieldName = 'AMOUNT')
          or (Fields[i].FieldName = 'THIRD_PARTY')
          or (Fields[i].FieldName = 'CHECK_DATE')
          or (Fields[i].FieldName = 'CO_STATES_NBR')
          or (Fields[i].FieldName = 'CO_SUI_NBR')
          or (Fields[i].FieldName = 'CO_LOCAL_TAX_NBR')
          or (Fields[i].FieldName = 'FILLER') then
            ds.FindField(Fields[i].FieldName).Assign(Fields[i]);
        ds['STATUS'] := TAX_DEPOSIT_STATUS_PAID;
        ds['ADJUSTMENT_TYPE'] := TAX_LIABILITY_ADJUSTMENT_TYPE_CREDIT;
        ds['CO_TAX_DEPOSITS_NBR'] := FieldValues['CO_TAX_DEPOSITS_NBR'];
        ds.Post;
        ds.Append;
        for i := 0 to FieldCount-1 do
          if (Fields[i].FieldName = 'CO_NBR')
          or (Fields[i].FieldName = 'DUE_DATE')
          or (Fields[i].FieldName = 'TAX_TYPE')
          or (Fields[i].FieldName = 'AMOUNT')
          or (Fields[i].FieldName = 'THIRD_PARTY')
          or (Fields[i].FieldName = 'CHECK_DATE')
          or (Fields[i].FieldName = 'CO_STATES_NBR')
          or (Fields[i].FieldName = 'CO_SUI_NBR')
          or (Fields[i].FieldName = 'CO_LOCAL_TAX_NBR')
          or (Fields[i].FieldName = 'FILLER') then
            ds.FindField(Fields[i].FieldName).Assign(Fields[i]);
        ds['AMOUNT'] := -ds['AMOUNT'];
        ds['STATUS'] := UnpaidStatus;
        ds['ADJUSTMENT_TYPE'] := TAX_LIABILITY_ADJUSTMENT_TYPE_CREDIT;
        ds.Post;
        Prior;
      end;
    finally
      Free;
    end;
  end;
begin
  inherited;
  ctx_StartWait('Processing...');
  try
    ctx_DataAccess.OpenClient(cdMain['cl_nbr']);
    if cdMain['TAX_SERVICE'] = GROUP_BOX_YES then
      UnpaidStatus := TAX_DEPOSIT_STATUS_IMPOUNDED
    else
      UnpaidStatus := TAX_DEPOSIT_STATUS_PENDING;
    try
      Process(DM_COMPANY.CO_FED_TAX_LIABILITIES);
      Process(DM_COMPANY.CO_STATE_TAX_LIABILITIES);
      Process(DM_COMPANY.CO_SUI_LIABILITIES);
      Process(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES);
    except
      ctx_DataAccess.CancelDataSets([DM_COMPANY.CO_FED_TAX_LIABILITIES,
      DM_COMPANY.CO_STATE_TAX_LIABILITIES, DM_COMPANY.CO_SUI_LIABILITIES,
      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES]);
      raise;
    end;
    ctx_UpdateWait('Saving...');
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_FED_TAX_LIABILITIES,
      DM_COMPANY.CO_STATE_TAX_LIABILITIES, DM_COMPANY.CO_SUI_LIABILITIES,
      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES]);
    cdMain.Delete;
  finally
    ctx_EndWait;
  end;
end;

procedure TPROCESS_REPORT_CREDIT_LIAB.Activate;
begin
  inherited;
  cbMonth.ItemIndex := GetMonth(Date)-1;
  udYear.Position := GetYear(Date);
  edYear.Text := IntToStr(udYear.Position);
end;

initialization
  RegisterClass(TPROCESS_REPORT_CREDIT_LIAB);

end.
