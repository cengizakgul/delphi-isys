// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ReCreatePaymentFile;

interface

uses
  SFrameEntry,  Db,  StdCtrls, Controls,
  Grids, Wwdbigrd, Wwdbgrid, Classes, Wwdatsrc, Mask, wwdbedit, Wwdotdot,
  Wwdbcomb, Dialogs, Buttons, Variants, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, ISDataAccessComponents, EvDataAccessComponents, EvTypes,
  EvContext, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TReCreatePaymentFile = class(TFrameEntry)
    ds: TevDataSource;
    wwDBGrid1: TevDBGrid;
    Label1: TevLabel;
    cd: TevClientDataSet;
    cdCL_NBR: TIntegerField;
    cdCO_NBR: TIntegerField;
    cdCO_TAX_DEPOSITS_NBR: TIntegerField;
    cdSTATUS: TStringField;
    cdSTATUS_DATE: TDateTimeField;
    cdTAX_PAYMENT_REFERENCE_NUMBER: TStringField;
    cbDbStatus: TevDBComboBox;
    cdCUSTOM_COMPANY_NUMBER: TStringField;
    cdNAME: TStringField;
    cdMARK: TBooleanField;
    cdDAY_DATE: TDateField;
    Button1: TevBitBtn;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Activate; override;
  end;

var
  ReCreatePaymentFile: TReCreatePaymentFile;

implementation

{$R *.DFM}

uses SFieldCodeValues, EvConsts, SDataStructure, EvUtils, SysUtils;

procedure TReCreatePaymentFile.Button1Click(Sender: TObject);
  procedure Process(const d: TEvClientDataSet; const NewStatus: Char);
  begin
    d.First;
    while not d.Eof do
    begin
      d.Edit;
      d['CO_TAX_DEPOSITS_NBR'] := Null;
      d['STATUS'] := NewStatus;
      d.Post;
      d.Next;
    end;
  end;
var
  cdRecNo: Integer;
  NewStatus: Char;
begin
  inherited;
  {if EvMessage('This operation is unreversible. Once tax deposit is undone, '+
    'you will need to pay the taxes again!'#13'Do you want to continue?', mtWarning,
    [mbYes, mbNo], mbNo) <> mrYes then
      Exit;}
  cd.CheckBrowseMode;
  with DM_COMPANY do
  begin
    CO_FED_TAX_LIABILITIES.IndexName := '';
    CO_LOCAL_TAX_LIABILITIES.IndexName := '';
    CO_SUI_LIABILITIES.IndexName := '';
    CO_STATE_TAX_LIABILITIES.IndexName := '';
  end;
  ctx_DataAccess.OpenClient(0);
  cdRecNo := cd.RecNo;
  cd.DisableControls;

  try
    cd.First;
    while not cd.Eof do
    begin
      if not VarIsNull(cd['MARK']) then
        if cd['MARK'] then
        begin
          ctx_DataAccess.OpenClient(cd['CL_NBR']);
          ctx_DataAccess.StartNestedTransaction([dbtClient]);
          try
            with DM_COMPANY, DM_PAYROLL do
            begin
              PopulateDataSets([CO_FED_TAX_LIABILITIES, CO_LOCAL_TAX_LIABILITIES, CO_SUI_LIABILITIES,
                CO_STATE_TAX_LIABILITIES, CO_TAX_DEPOSITS, PR_MISCELLANEOUS_CHECKS],
                'CO_TAX_DEPOSITS_NBR='+ IntToStr(cd['CO_TAX_DEPOSITS_NBR']),False);
              CO.RetrieveCondition := 'CO_NBR='+ IntToStr(cd['CO_NBR']);
              ctx_DataAccess.OpenDataSets([CO, CO_FED_TAX_LIABILITIES, CO_LOCAL_TAX_LIABILITIES, CO_SUI_LIABILITIES, CO_STATE_TAX_LIABILITIES, CO_TAX_DEPOSITS, PR_MISCELLANEOUS_CHECKS]);
              Assert(CO_TAX_DEPOSITS.RecordCount = 1);
              if CO['TAX_SERVICE'] = GROUP_BOX_YES then
                NewStatus := TAX_DEPOSIT_STATUS_IMPOUNDED
              else
                NewStatus := TAX_DEPOSIT_STATUS_PENDING;
              Process(CO_FED_TAX_LIABILITIES, NewStatus);
              Process(CO_LOCAL_TAX_LIABILITIES, NewStatus);
              Process(CO_SUI_LIABILITIES, NewStatus);
              Process(CO_STATE_TAX_LIABILITIES, NewStatus);
              CO_TAX_DEPOSITS.Delete;
              while PR_MISCELLANEOUS_CHECKS.RecordCount > 0 do
                PR_MISCELLANEOUS_CHECKS.Delete;
              ctx_DataAccess.PostDataSets([CO_FED_TAX_LIABILITIES, CO_LOCAL_TAX_LIABILITIES, CO_SUI_LIABILITIES, CO_STATE_TAX_LIABILITIES, PR_MISCELLANEOUS_CHECKS, CO_TAX_DEPOSITS]);
            end;
            ctx_DataAccess.CommitNestedTransaction;
          except
            ctx_DataAccess.RollbackNestedTransaction;
            raise;
          end;
        end;
      cd.Next;
    end;
  finally
    cd.RecNo := cdRecNo;
    cd.EnableControls;
  end;
end;

procedure TReCreatePaymentFile.Activate;
begin
  cbDbStatus.Items.Text := TaxDepositStatus_ComboChoices;
  cd.ProviderName := ctx_DataAccess.TEMP_CUSTOM_VIEW.ProviderName;
  inherited;
  with TExecDSWrapper.Create('GenericSelect2WithCondition') do
  begin
    SetMacro('COLUMNS', 't2.CL_NBR, t2.CO_NBR, t2.CO_TAX_DEPOSITS_NBR, t2.STATUS, '+
      'STATUS_DATE, cast(t2.status_date as date) DAY_DATE, t2.TAX_PAYMENT_REFERENCE_NUMBER, t1.CUSTOM_COMPANY_NUMBER, t1.NAME');
    SetMacro('TABLE1', 'TMP_CO');
    SetMacro('TABLE2', 'TMP_CO_TAX_DEPOSITS');
    SetMacro('JOINFIELD', 'CO');
    SetMacro('CONDITION', 't2.CL_NBR = t1.CL_NBR');
    cd.DataRequest(AsVariant);
  end;
  cd.Open;
  SortDSBy( cd, 'DAY_DATE', 'DAY_DATE' ); //gdy23
  cd.Last;
end;

initialization
  RegisterClass(TReCreatePaymentFile);

end.
