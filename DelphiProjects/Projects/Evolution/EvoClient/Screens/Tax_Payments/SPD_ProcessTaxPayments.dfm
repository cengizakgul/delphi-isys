inherited ProcessTaxPayments: TProcessTaxPayments
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    ActivePage = SelectTabSheet
    Align = alClient
    TabOrder = 0
    OnChanging = PageControl1Changing
    object SelectTabSheet: TTabSheet
      Caption = 'Select taxes'
      object Label3: TevLabel
        Left = 296
        Top = 40
        Width = 101
        Height = 13
        Caption = 'Payment method filter'
      end
      object gbDateGroup: TevGroupBox
        Left = 8
        Top = 0
        Width = 265
        Height = 78
        Caption = 'Tax due dates'
        TabOrder = 0
        object Label2: TevLabel
          Left = 8
          Top = 16
          Width = 23
          Height = 13
          Caption = 'From'
        end
        object Label4: TevLabel
          Left = 136
          Top = 16
          Width = 13
          Height = 13
          Caption = 'To'
        end
        object FromDate: TevDateTimePicker
          Left = 8
          Top = 32
          Width = 121
          Height = 21
          Date = 36407.906538101900000000
          Time = 36407.906538101900000000
          TabOrder = 0
        end
        object ToDate: TevDateTimePicker
          Left = 136
          Top = 32
          Width = 121
          Height = 21
          Date = 36407.906538101900000000
          Time = 36407.906538101900000000
          TabOrder = 1
        end
        object cbFromDateRange: TevCheckBox
          Left = 8
          Top = 56
          Width = 249
          Height = 17
          Caption = 'Don'#39't show unpaid taxes before date range'
          TabOrder = 2
        end
      end
      object btnGetTaxesDue: TevBitBtn
        Left = 520
        Top = 5
        Width = 112
        Height = 31
        Caption = 'Get taxes due'
        TabOrder = 4
        OnClick = btnGetTaxesDueClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88DDDD888DD
          DDDDDDDDDDDDDDDDDDDDAA08DD8AA08DDDDD88FDDDD88FDDDDDDDAA08DA0DA0D
          DDDDD88FDD8FD8FDDDDDDDAA08A00A0DDDDDDD88FD8FD8FDDDDDDDDAA08AA0DD
          DDDDDDD88FD88FDDDDDDD888AA0444444444DDDD88FDD88888888AA08AA04EE4
          EE4FD88FD88F8DD8DD8DA0DA04AA06E4EE4F8FD8FD88FDD8DD8DA00A046AA064
          EE4F8FD8F8D88FD8DD8DDAA074E6AA04EE4FD88FD8DD88F8DD8DDDDDF4EE4664
          EE4FDDDDD8DDDDD8DD8DDDDDF4444EE4EE4FDDDDD8888DD8DD8DDDDDFFFF4EE4
          EE4FDDDDDDDD8DD8DD8DDDDDDDDF4444EE4FDDDDDDDD8888DD8DDDDDDDDFFFF4
          EE4FDDDDDDDDDDD8DD8DDDDDDDDDDDF4444FDDDDDDDDDDD8888D}
        NumGlyphs = 2
      end
      object rgClientTaxType: TevRadioGroup
        Left = 296
        Top = 0
        Width = 209
        Height = 40
        Caption = 'Tax Clients'
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Any'
          'Full'
          'Direct'
          'No')
        TabOrder = 1
      end
      object cbPayMethod: TevComboBox
        Left = 296
        Top = 56
        Width = 209
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
      end
      object GroupBox1: TevGroupBox
        Left = 8
        Top = 80
        Width = 625
        Height = 367
        Caption = 'Taxes'
        TabOrder = 3
        object Label8: TevLabel
          Left = 8
          Top = 16
          Width = 63
          Height = 13
          Caption = 'Tax type filter'
        end
        object bbAdd: TevBitBtn
          Left = 328
          Top = 24
          Width = 89
          Height = 26
          Caption = 'Add'
          TabOrder = 1
          TabStop = False
          OnClick = bbAddClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
            DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
            DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
            888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
            A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
            A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
            DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
            DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
        end
        object bbRemove: TevBitBtn
          Left = 424
          Top = 24
          Width = 89
          Height = 26
          Caption = 'Remove '
          TabOrder = 2
          TabStop = False
          OnClick = bbRemoveClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
            888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
            C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
            C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
        end
        object btnAddAll: TevBitBtn
          Left = 528
          Top = 24
          Width = 89
          Height = 26
          Caption = 'Add all'
          TabOrder = 3
          TabStop = False
          OnClick = btnAddAllClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD2222DDDD
            DDDDDDDDFFFFDDDDDDDDDDDDAAA2D2DDDDDDDDDD888FD8DDDDDDDDDDAAA2D2D2
            DDDDDDDD888FD8D8DDDDDDDDAAA2DDDDDDDDDDDD888FDDDDDDDD2222AAA22222
            DDDDFFFF888FFFFFDDDDAAAAAAAAAAA2D2DD88888888888FD8DDAAAAAAAAAAA2
            D2D288888888888FD8D8AAAAAAAAAAA2D2D288888888888FD8D8DDDDAAA2DDDD
            D2D2DDDD888FDDDDD8D8DD88AAA2D2DDDDD2DD8D888FD8DDDDD8DDDDAAA2D2D2
            DDDDDDDD888FD8D8DDDDDDDDAAA2D2D2DDDDDDDD888FD8D8DDDDDDDDDDDDD2D2
            DDDDDDDDDDDDD8D8DDDDDDDDDDDDDDD2DDDDDDDDDDDDDDD8DDDD}
          NumGlyphs = 2
        end
        object cbTaxTable: TevComboBox
          Left = 8
          Top = 32
          Width = 209
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbTaxTableChange
        end
        object pcAvail: TevPageControl
          Left = 6
          Top = 56
          Width = 612
          Height = 305
          ActivePage = tsByTaxes
          TabOrder = 4
          OnChange = pcAvailChange
          object tsMaintenance: TTabSheet
            Caption = 'Maintenance Hold'
            ImageIndex = 2
            object evPanel1: TevPanel
              Left = 0
              Top = 0
              Width = 604
              Height = 36
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object evLabel1: TevLabel
                Left = 38
                Top = 11
                Width = 508
                Height = 13
                Caption = 
                  'The payments due for these companies will not be included when t' +
                  'he user clicks the Get Taxes Due button.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
            end
            object evPanel2: TevPanel
              Left = 0
              Top = 36
              Width = 604
              Height = 241
              Align = alClient
              BevelOuter = bvNone
              Caption = 'evPanel2'
              TabOrder = 1
              object evDBGrid1: TevDBGrid
                Left = 0
                Top = 0
                Width = 604
                Height = 241
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom Company Number'#9#9
                  'NAME'#9'40'#9'Name'#9#9)
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TProcessTaxPayments\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsMaintenanceCo
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
              end
            end
          end
          object tsByAgency: TTabSheet
            Caption = 'Agencies'
            object evSplitter2: TevSplitter
              Left = 296
              Top = 0
              Height = 277
            end
            object grdAgencyFrom: TevDBGrid
              Left = 0
              Top = 0
              Width = 296
              Height = 277
              DisableThemesInTitle = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TProcessTaxPayments\grdAgencyFrom'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alLeft
              DataSource = dsAgencyFrom
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = bbAddClick
              PaintOptions.AlternatingRowColor = clCream
            end
            object grdAgencyTo: TevDBGrid
              Left = 299
              Top = 0
              Width = 305
              Height = 277
              DisableThemesInTitle = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TProcessTaxPayments\grdAgencyTo'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = dsAgencyTo
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 1
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = bbRemoveClick
              PaintOptions.AlternatingRowColor = clCream
            end
          end
          object tsByTaxes: TTabSheet
            Caption = 'Taxes'
            ImageIndex = 1
            object evSplitter1: TevSplitter
              Left = 296
              Top = 0
              Height = 277
            end
            object grdTaxFrom: TevDBGrid
              Left = 0
              Top = 0
              Width = 296
              Height = 277
              DisableThemesInTitle = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TProcessTaxPayments\grdTaxFrom'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alLeft
              DataSource = dsTaxFrom
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = bbAddClick
              PaintOptions.AlternatingRowColor = clCream
            end
            object grdTaxTo: TevDBGrid
              Left = 299
              Top = 0
              Width = 305
              Height = 277
              DisableThemesInTitle = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TProcessTaxPayments\grdTaxTo'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = dsTaxTo
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 1
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = bbRemoveClick
              PaintOptions.AlternatingRowColor = clCream
            end
          end
        end
      end
      object evTaxPaymentList: TevBitBtn
        Left = 520
        Top = 44
        Width = 112
        Height = 31
        Caption = 'Tax Payments List'
        TabOrder = 5
        OnClick = evTaxPaymentListClick
        NumGlyphs = 2
      end
    end
    object DetailTabSheet: TTabSheet
      Caption = 'Detail'
      ImageIndex = 1
      TabVisible = False
      OnShow = DetailTabSheetShow
      object Splitter1: TevSplitter
        Left = 0
        Top = 231
        Width = 427
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object Splitter2: TevSplitter
        Left = 0
        Top = 379
        Width = 427
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 115
        Align = alTop
        TabOrder = 0
        object Label7: TevLabel
          Left = 9
          Top = 8
          Width = 64
          Height = 13
          Caption = 'Taxes due '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DateLabel: TevLabel
          Left = 6
          Top = 34
          Width = 75
          Height = 13
          Caption = 'date range here'
        end
        object evLabel17: TevLabel
          Left = 237
          Top = 22
          Width = 78
          Height = 13
          Caption = 'Tax Check Form'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel2: TevLabel
          Left = 109
          Top = 96
          Width = 118
          Height = 13
          Caption = 'Tax payment description '
        end
        object evLabel9: TevLabel
          Left = 109
          Top = 70
          Width = 116
          Height = 13
          Caption = 'Post process ACH report'
          FocusControl = cbPostProcessReport
        end
        object PrinTevButton: TevBitBtn
          Left = 637
          Top = 34
          Width = 25
          Height = 25
          Hint = 'Print'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Visible = False
          OnClick = PrintButtonClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
            08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
            8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
            8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
            8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
            78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
            7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
            DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
            DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
          NumGlyphs = 2
        end
        object PayButton: TevBitBtn
          Left = 665
          Top = 4
          Width = 25
          Height = 25
          Hint = 'Pay'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = PayButtonClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D888DDDD888D
            DDDDDDDDDDDDDDDDDDDDDAA28DD8AA28DDDDD88FDDDD88FDDDDDDDAA28DA2DA2
            DDDDDD88FDD8FD8FDDDDDDDAA28A28A2DDDDDDD88FD8FD8FDDDDDDDDAA28AA2D
            DDDDDDDD88FD88FDDDDDDD888AA28DDDDDDDDDDDD88FDDDDDDDDD8AA28AA28DD
            DDDDDD88FD88FDDDDDDDDA28A28AA2888888D8FD8FD88FDDDDDD0A20A200AA20
            0008D8FD8F8D88F8888D07AA27FF7AA27F088D88F888D88F888D0F7777777777
            7F0888DDD8888DDD888D0F88888888888F0888DDDDDDDDDDD88D0F88FFFFFF77
            7F0888DD88888888888D0FFFFFFFFF888F088888888888DDD88D0FFFFFFFFFFF
            FF08888888888888888D000000000000000D888888888888888D}
          NumGlyphs = 2
        end
        object SortByRadioGroup: TevRadioGroup
          Left = 88
          Top = 1
          Width = 140
          Height = 32
          Caption = 'Sort by'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Tax type'
            'Company')
          TabOrder = 0
          OnClick = SortByRadioGroupClick
        end
        object BitBtn7: TevBitBtn
          Left = 608
          Top = 76
          Width = 81
          Height = 32
          Caption = '&Close'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = BitBtn7Click
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
            88DDDFFFFFFFFFFFFFDD8CCCCCCCCCCCCC8DF8888888888888FDC77777777777
            77CD8DDDDDDDDDDDDD8DC7788777778877CD8DDFFDDDDDFFDD8DC77CC87778CC
            77CD8DD88FDDDF88DD8DC77CCC878CCC77CD8DD888FDF888DD8DC777CCC8CCC7
            77CD8DDD888F888DDD8DC7777CCCCC7777CD8DDDD88888DDDD8DC77778CCC877
            77CD8DDDDF888FDDDD8DC7778CCCCC8777CD8DDDF88888FDDD8DC778CCC7CCC8
            77CD8DDF888D888FDD8DC77CCC777CCC77CD8DD888DDD888DD8DC77CC77777CC
            77CD8DD88DDDDD88DD8DC7777777777777CD8DDDDDDDDDDDDD8DC88888888888
            88CD8FFFFFFFFFFFFF8DDCCCCCCCCCCCCCDDD8888888888888DD}
          NumGlyphs = 2
        end
        object RecalcButton: TevBitBtn
          Left = 608
          Top = 34
          Width = 25
          Height = 25
          Hint = 'Recalc'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = RecalcButtonClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD84666664
            448DDDDDD888888888DD8DD84EEEEE466648DDDD8DDDDD8DDD8D4884EEEEE486
            66488DD8DDDDD8DDDD8D444E888888886664888DDDDDDDDDDDD84EE6FFFFFFF0
            44448DDD8888888F88884EE6FF0888F0DDDD8DDD88FDDD8FDDDD4EEE6FF08888
            DDDD8DDDD88FDDDDDDDD4EEEE4FF044444448DDDDD88F88888884444444FF04E
            EEE4888888D88F8DDDD8DDDDD8FF084EEEE4DDDDDD88FD8DDDD8DDDD8FF08488
            EEE4DDDDD88FD8DDDDD84444FF0888F0EEE4888D88FDDD8FDDD84666FFFFFFF0
            E4448DDD8888888FD88884666846666E4884D8DDDDDDDDDD8DD8846664EEEEE4
            8DD8D8DDD8DDDDD8DDDDD84446666648DDDDDD888888888DDDDD}
          NumGlyphs = 2
        end
        object ExcelButton: TevBitBtn
          Left = 637
          Top = 4
          Width = 25
          Height = 25
          Hint = 'Save to Excel File'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = ExcelButtonClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0DDDDDDDDD
            DDDDDDFDDDDDDDDDDDDDDD00DDDDDDDDDDDDDD8FDDDDDDDDDDDDDD0B0DDDDDDD
            DDDDDD88FDDDDDDDDDDD000BB0888888888DFF888FDDDDDDDDDD0BBBBB022222
            2222888888FD888888880BBBBBB07FFFFFF28888888FDDDDDDD80BBBBB08FF72
            22F28888888DDDD888D8000BB022222722F2888888D8888D88D8880B02222272
            27F2DD888D8888D88DD8DD008F22272222F2DD88DD888D8888D8DD02FFF27227
            FFF2DD8DDDD8D88DDDD8DD82FF2722227FF2DDD8DD8D8888DDD8DD82F2722722
            27F2DDD8D8D88D888DD8DD82F2227F7222F2DDD8D888DDD888D8DD82FFFFFFFF
            FFF2DDD8DDDDDDDDDDD8DDD2222222222222DDD8888888888888}
          NumGlyphs = 2
        end
        object btnChangeSelected: TevBitBtn
          Left = 608
          Top = 4
          Width = 25
          Height = 25
          Hint = 'Select'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          PopupMenu = evPopupMenu1
          ShowHint = True
          TabOrder = 2
          OnClick = btnChangeSelectedClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD0000
            008DDDDDDDDD888888FD0000DDDD0000008D8888DDDD888888FD00D0DDD80000
            008D88D8DDDF888888FD0D80DD80FFFF77088D88DDF8DDDDDD8F0000DD0FFFFF
            F7088888DD8DDDDDDD8FDDDDDD07FFFFF708DDDDDD8DDDDDDD8F0000DD80FFFF
            77088888DDD8DDDDDD8F00D0DDD0FF77008D88D8DDD8DDDD88FD0D80DDD0F000
            8DDD8D88DDD8D888FDDD0000DDD0F08DDDDD8888DDD8D8FDDDDDDDDDDDD0F08D
            DDDDDDDDDDD8D8FDDDDD0000DDD0F08DDDDD8888DDD8D8FDDDDD00D0DDD0F08D
            DDDD88D8DDD8D8FDDDDD0D80DDD808DDDDDD8D88DDDD8FDDDDDD0000DDDDDDDD
            DDDD8888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
        end
        object cbForceChecks: TevCheckBox
          Left = 239
          Top = 6
          Width = 90
          Height = 17
          Caption = 'Force Checks'
          TabOrder = 1
        end
        object cbEMail: TevCheckBox
          Left = 342
          Top = 7
          Width = 104
          Height = 17
          Caption = 'E-Mail Notification'
          TabOrder = 8
          OnClick = cbEMailClick
        end
        object eEMail: TevEdit
          Left = 452
          Top = 5
          Width = 150
          Height = 21
          Enabled = False
          TabOrder = 9
        end
        object cbEmailSendRule: TevComboBox
          Left = 452
          Top = 37
          Width = 151
          Height = 21
          Style = csDropDownList
          Enabled = False
          ItemHeight = 13
          TabOrder = 10
          Items.Strings = (
            'Always'
            'Successful'
            'Exceptions'
            'Exceptions/Warnings')
        end
        object cbSelectFilteredRecords: TevCheckBox
          Left = 11
          Top = 49
          Width = 191
          Height = 17
          Caption = 'Select for Filtered Records Only'
          Enabled = False
          TabOrder = 11
        end
        object edtTaxDesc: TevEdit
          Left = 240
          Top = 88
          Width = 363
          Height = 21
          TabOrder = 13
        end
        object cbPostProcessReport: TevComboBox
          Left = 240
          Top = 63
          Width = 363
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 12
          OnClick = cbPostProcessReportClick
          Items.Strings = (
            'Do not Combine'
            'Company Trans Only'
            'Company and SB Trans')
        end
        object cbMiscCheckForm: TevComboBox
          Left = 240
          Top = 37
          Width = 206
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 14
        end
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 115
        Width = 427
        Height = 116
        DisableThemesInTitle = False
        ControlInfoInDataSet = True
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TProcessTaxPayments\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alTop
        DataSource = wwdsGrid1
        ReadOnly = False
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        Visible = False
        PaintOptions.AlternatingRowColor = clCream
      end
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 234
        Width = 427
        Height = 145
        DisableThemesInTitle = False
        ControlInfoInDataSet = True
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TProcessTaxPayments\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alTop
        DataSource = wwdsGrid2
        ReadOnly = False
        TabOrder = 2
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        Visible = False
        PaintOptions.AlternatingRowColor = clCream
      end
      object wwDBGrid3: TevDBGrid
        Left = 0
        Top = 382
        Width = 427
        Height = 25
        DisableThemesInTitle = False
        ControlInfoInDataSet = True
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TProcessTaxPayments\wwDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsGrid3
        ReadOnly = False
        TabOrder = 3
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        Visible = False
        OnCalcCellColors = wwDBGrid3CalcCellColors
        PaintOptions.AlternatingRowColor = clCream
      end
      object wwDBComboBox1: TevDBComboBox
        Left = 328
        Top = 144
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'PAY'
        DataSource = wwdsGrid2
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'All'#9'A'
          'Some'#9'S'
          'None'#9'N')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 4
        UnboundDataType = wwDefault
        OnCloseUp = wwDBComboBox1CloseUp
      end
      object wwDBComboBox2: TevDBComboBox
        Left = 328
        Top = 168
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'PAY'
        DataSource = wwdsGrid1
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'All'#9'A'
          'Some'#9'S'
          'None'#9'N')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
        OnCloseUp = wwDBComboBox1CloseUp
      end
    end
    object DetailRollBack: TTabSheet
      Caption = 'Tax Payments List'
      ImageIndex = 2
      TabVisible = False
      object TaxListPageControl: TevPageControl
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        ActivePage = TaxListDetail
        Align = alClient
        TabOrder = 0
        OnChanging = TaxListPageControlChanging
        object TaxListDetail: TTabSheet
          Caption = ' List  '
          object evPanel3: TevPanel
            Left = 0
            Top = 0
            Width = 419
            Height = 37
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object btnRollBack: TevBitBtn
              Left = 552
              Top = 5
              Width = 120
              Height = 25
              Caption = 'Rollback '
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = btnRollBackClick
              NumGlyphs = 2
            end
            object evcbTenDays: TevCheckBox
              Left = 6
              Top = 14
              Width = 62
              Height = 16
              Caption = '10 Days'
              Checked = True
              State = cbChecked
              TabOrder = 1
              OnClick = evcbTenDaysClick
            end
            object evstatusFilter: TevRadioGroup
              Left = 76
              Top = -2
              Width = 249
              Height = 32
              Caption = 'Status Filter'
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'Processed'
                'Rolled back'
                'All              ')
              TabOrder = 2
              OnClick = evstatusFilterClick
            end
            object btnTaxListLiab: TevBitBtn
              Left = 370
              Top = 5
              Width = 120
              Height = 25
              Caption = ' Detail'
              TabOrder = 3
              OnClick = btnTaxListLiabClick
            end
            object evPanel5: TevPanel
              Left = 261
              Top = 0
              Width = 158
              Height = 37
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 4
              object evBtnClose: TevBitBtn
                Left = 21
                Top = 5
                Width = 120
                Height = 25
                Caption = '&Close'
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BitBtn7Click
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                  88DDDFFFFFFFFFFFFFDD8CCCCCCCCCCCCC8DF8888888888888FDC77777777777
                  77CD8DDDDDDDDDDDDD8DC7788777778877CD8DDFFDDDDDFFDD8DC77CC87778CC
                  77CD8DD88FDDDF88DD8DC77CCC878CCC77CD8DD888FDF888DD8DC777CCC8CCC7
                  77CD8DDD888F888DDD8DC7777CCCCC7777CD8DDDD88888DDDD8DC77778CCC877
                  77CD8DDDDF888FDDDD8DC7778CCCCC8777CD8DDDF88888FDDD8DC778CCC7CCC8
                  77CD8DDF888D888FDD8DC77CCC777CCC77CD8DD888DDD888DD8DC77CC77777CC
                  77CD8DD88DDDDD88DD8DC7777777777777CD8DDDDDDDDDDDDD8DC88888888888
                  88CD8FFFFFFFFFFFFF8DDCCCCCCCCCCCCCDDD8888888888888DD}
                NumGlyphs = 2
              end
            end
          end
          object evGrdRollBack: TevDBGrid
            Left = 0
            Top = 37
            Width = 419
            Height = 173
            DisableThemesInTitle = False
            Selected.Strings = (
              'DESCRIPTION'#9'70'#9'Description'#9'F'
              'Status_desc'#9'11'#9'Status'#9'F'
              'STATUS_DATE'#9'18'#9'Status Date'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TProcessTaxPayments\evGrdRollBack'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsTaxPayment
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
          end
        end
        object TaxListLiabs: TTabSheet
          Caption = ' Detail  '
          ImageIndex = 1
          object evGrdTaxListLiab: TevDBGrid
            Left = 0
            Top = 37
            Width = 419
            Height = 173
            DisableThemesInTitle = False
            Selected.Strings = (
              'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client  Number'#9'F'
              'CUSTOM_COMPANY_NUMBER'#9'20'#9'Company Number'#9'F'
              'Name'#9'40'#9'Name'#9#9
              'AGENCY_NAME'#9'40'#9'Agency Name'#9'F'
              'LiabType'#9'6'#9'Type'#9'F'
              'DEPOSIT_TYPE_desc'#9'7'#9'Deposit Type'#9'F'
              'Amount'#9'10'#9'Amount'#9#9
              'CHECK_DATE'#9'18'#9'Check Date'#9'F'
              'DUE_DATE'#9'18'#9'Due Date'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TProcessTaxPayments\evGrdTaxListLiab'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsTaxPaymentLiabs
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgShowFooter, dgFooter3DCells, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnDrawFooterCell = evGrdTaxListLiabDrawFooterCell
            PaintOptions.AlternatingRowColor = clCream
          end
          object evPanel4: TevPanel
            Left = 0
            Top = 0
            Width = 419
            Height = 37
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object evCbCoNbr: TevComboBox
              Left = 139
              Top = 8
              Width = 131
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              Sorted = True
              TabOrder = 0
              OnChange = evCbCoNbrChange
            end
            object evCbAgencyName: TevComboBox
              Left = 510
              Top = 8
              Width = 241
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              Sorted = True
              TabOrder = 1
              OnChange = evCbCoNbrChange
            end
            object evCbLiabType: TevComboBox
              Left = 751
              Top = 8
              Width = 51
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              Sorted = True
              TabOrder = 2
              OnChange = evCbCoNbrChange
            end
            object evCbDepType: TevComboBox
              Left = 802
              Top = 8
              Width = 64
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              Sorted = True
              TabOrder = 3
              OnChange = evCbCoNbrChange
            end
            object evCbClNbr: TevComboBox
              Left = 8
              Top = 8
              Width = 131
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              Sorted = True
              TabOrder = 4
              OnChange = evCbCoNbrChange
            end
            object btnRollBack2: TevBitBtn
              Left = 885
              Top = 4
              Width = 120
              Height = 25
              Caption = 'Rollback '
              ParentShowHint = False
              ShowHint = True
              TabOrder = 5
              OnClick = btnRollBackClick
              NumGlyphs = 2
            end
            object evPanel6: TevPanel
              Left = 262
              Top = 0
              Width = 157
              Height = 37
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 6
              object evBtnClose2: TevBitBtn
                Left = 23
                Top = 5
                Width = 120
                Height = 25
                Caption = '&Close'
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = evBtnClose2Click
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                  88DDDFFFFFFFFFFFFFDD8CCCCCCCCCCCCC8DF8888888888888FDC77777777777
                  77CD8DDDDDDDDDDDDD8DC7788777778877CD8DDFFDDDDDFFDD8DC77CC87778CC
                  77CD8DD88FDDDF88DD8DC77CCC878CCC77CD8DD888FDF888DD8DC777CCC8CCC7
                  77CD8DDD888F888DDD8DC7777CCCCC7777CD8DDDD88888DDDD8DC77778CCC877
                  77CD8DDDDF888FDDDD8DC7778CCCCC8777CD8DDDF88888FDDD8DC778CCC7CCC8
                  77CD8DDF888D888FDD8DC77CCC777CCC77CD8DD888DDD888DD8DC77CC77777CC
                  77CD8DD88DDDDD88DD8DC7777777777777CD8DDDDDDDDDDDDD8DC88888888888
                  88CD8FFFFFFFFFFFFF8DDCCCCCCCCCCCCCDDD8888888888888DD}
                NumGlyphs = 2
              end
            end
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 125
    Top = 218
  end
  inherited wwdsDetail: TevDataSource
    Left = 190
    Top = 65530
  end
  inherited wwdsList: TevDataSource
    Top = 210
  end
  object wwdsGrid1: TevDataSource
    OnDataChange = wwdsGrid1DataChange
    Left = 308
    Top = 65528
  end
  object wwdsGrid2: TevDataSource
    DataSet = TEMP_DETAIL
    OnDataChange = wwdsGrid2DataChange
    Left = 216
    Top = 65529
  end
  object wwdsGrid3: TevDataSource
    DataSet = TEMP_PR_LIABILITY
    Left = 248
    Top = 65529
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'Excel CSV files|*.csv'
    Title = 'Enter export file name'
    Left = 164
    Top = 208
  end
  object TEMP_CO: TevClientDataSet
    Left = 76
    Top = 272
  end
  object TEMP_PR_LIABILITY: TevClientDataSet
    ControlType.Strings = (
      'PAY;CheckBox;Y;N')
    Left = 348
  end
  object TEMP_DETAIL: TevClientDataSet
    Left = 380
    Top = 65528
  end
  object TEMP_TAX_TYPE: TevClientDataSet
    Left = 36
    Top = 272
  end
  object wwdsTEMP_DETAIL: TevDataSource
    DataSet = TEMP_DETAIL
    Left = 108
    Top = 320
  end
  object evPopupMenu1: TevPopupMenu
    MenuAnimation = [maTopToBottom]
    Left = 260
    Top = 312
    object SelectAllImpounded1: TMenuItem
      Caption = 'Select All Impounded'
      OnClick = SelectAllImpounded1Click
    end
    object SelectAllExceptNSF1: TMenuItem
      Caption = 'Select All Except NSF'
      OnClick = SelectAllExceptNSF1Click
    end
    object SelectAllRegardlessStatus1: TMenuItem
      Caption = 'Select All Regardless Status'
      OnClick = SelectAllRegardlessStatus1Click
    end
    object DeselectAll1: TMenuItem
      Caption = 'Deselect All'
      OnClick = DeselectAll1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AutoRecalcTotals1: TMenuItem
      Caption = 'Auto Recalc Totals'
      Checked = True
      OnClick = AutoRecalcTotals1Click
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 592
    Top = 160
  end
  object cdMaintenanceCo: TevClientDataSet
    Left = 504
    Top = 208
    object cdMaintenanceCoCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdMaintenanceCoCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdMaintenanceCoCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdMaintenanceCoNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
  end
  object dsMaintenanceCo: TevDataSource
    DataSet = cdMaintenanceCo
    Left = 512
    Top = 240
  end
  object dsTaxPayment: TevDataSource
    DataSet = cdSBTaxPayment
    Left = 336
    Top = 272
  end
  object cdSBTaxPayment: TevClientDataSet
    FieldDefs = <
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'PatymentDate'
        DataType = ftDateTime
      end
      item
        Name = 'Status_desc'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'SB_TAX_PAYMENT_NBR'
        DataType = ftInteger
      end>
    Left = 336
    Top = 232
    object cdSBTaxPaymentDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 80
    end
    object cdSBTaxPaymentPaymentDate: TDateTimeField
      FieldName = 'PaymentDate'
    end
    object cdSBTaxPaymentStatus_desc: TStringField
      DisplayWidth = 11
      FieldName = 'Status_desc'
      Size = 11
    end
    object cdSBTaxPaymentSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
    end
    object cdSBTaxPaymentSB_TAX_PAYMENT_NBR: TIntegerField
      FieldName = 'SB_TAX_PAYMENT_NBR'
    end
    object cdSBTaxPaymentSTATUS: TStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS'
      Size = 1
      Calculated = True
    end
  end
  object cdSBTaxPaymentLiabs: TevClientDataSet
    FieldDefs = <
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'Status_desc'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'SB_TAX_PAYMENT_NBR'
        DataType = ftInteger
      end>
    OnCalcFields = cdSBTaxPaymentLiabsCalcFields
    Left = 464
    Top = 304
    object cdSBTaxPaymentLiabsCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdSBTaxPaymentLiabsCUSTOM_COMPANY_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdSBTaxPaymentLiabsName: TStringField
      DisplayWidth = 40
      FieldName = 'Name'
      Size = 40
    end
    object cdSBTaxPaymentLiabsAGENCY_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkCalculated
      FieldName = 'AGENCY_NAME'
      Size = 40
      Calculated = True
    end
    object cdSBTaxPaymentLiabsLiabType: TStringField
      DisplayWidth = 6
      FieldName = 'LiabType'
      Size = 6
    end
    object cdSBTaxPaymentLiabsDEPOSIT_TYPE_desc: TStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'DEPOSIT_TYPE_desc'
      Size = 7
      Calculated = True
    end
    object cdSBTaxPaymentLiabsAmount: TCurrencyField
      DisplayWidth = 10
      FieldName = 'Amount'
    end
    object cdSBTaxPaymentLiabsDUE_DATE: TDateTimeField
      FieldName = 'DUE_DATE'
    end
    object cdSBTaxPaymentLiabsCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
    object cdSBTaxPaymentLiabsSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
      Visible = False
    end
    object cdSBTaxPaymentLiabsDeposit_Type: TStringField
      DisplayWidth = 1
      FieldName = 'Deposit_Type'
      Visible = False
      Size = 1
    end
    object cdSBTaxPaymentLiabsCo_Tax_Deposits_Nbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'Co_Tax_Deposits_Nbr'
      Visible = False
    end
    object cdSBTaxPaymentLiabsCl_Nbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'Cl_Nbr'
      Visible = False
    end
    object cdSBTaxPaymentLiabsCo_Nbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'Co_Nbr'
      Visible = False
    end
    object cdSBTaxPaymentLiabsSb_Tax_Payment_Nbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'Sb_Tax_Payment_Nbr'
      Visible = False
    end
    object cdSBTaxPaymentLiabsSY_GLOBAL_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
      Visible = False
      Calculated = True
    end
  end
  object dsTaxPaymentLiabs: TevDataSource
    DataSet = cdSBTaxPaymentLiabs
    Left = 464
    Top = 344
  end
  object cdAgencyFrom: TevClientDataSet
    Left = 272
    Top = 376
  end
  object cdAgencyTo: TevClientDataSet
    Left = 360
    Top = 384
  end
  object cdTaxFrom: TevClientDataSet
    Left = 272
    Top = 416
  end
  object cdTaxTo: TevClientDataSet
    Left = 360
    Top = 416
  end
  object dsAgencyFrom: TevDataSource
    DataSet = cdAgencyFrom
    Left = 312
    Top = 376
  end
  object dsAgencyTo: TevDataSource
    DataSet = cdAgencyTo
    Left = 400
    Top = 384
  end
  object dsTaxFrom: TevDataSource
    DataSet = cdTaxFrom
    Left = 312
    Top = 416
  end
  object dsTaxTo: TevDataSource
    DataSet = cdTaxTo
    Left = 400
    Top = 416
  end
  object cdAgencyTaxLink: TevClientDataSet
    Left = 184
    Top = 424
  end
  object cdTCDAgencyList: TevClientDataSet
    Left = 176
    Top = 384
  end
end
