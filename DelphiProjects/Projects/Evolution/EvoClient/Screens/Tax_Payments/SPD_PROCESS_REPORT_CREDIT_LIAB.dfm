inherited PROCESS_REPORT_CREDIT_LIAB: TPROCESS_REPORT_CREDIT_LIAB
  object Label1: TevLabel [0]
    Left = 128
    Top = 8
    Width = 30
    Height = 13
    Caption = 'Month'
  end
  object Bevel1: TEvBevel [1]
    Left = 8
    Top = 40
    Width = 641
    Height = 9
    Shape = bsTopLine
  end
  object Label2: TevLabel [2]
    Left = 8
    Top = 8
    Width = 22
    Height = 13
    Caption = 'Year'
  end
  object wwDBGrid1: TevDBGrid [3]
    Left = 8
    Top = 56
    Width = 643
    Height = 377
    DisableThemesInTitle = False
    ControlType.Strings = (
      'LOAD;CheckBox;True;False'
      'TAX_SERVICE;CheckBox;Y;N')
    Selected.Strings = (
      'CUSTOM_CLIENT_NUMBER'#9'9'#9'Client #'#9'T'
      'CLIENT_NAME'#9'20'#9'Client name'#9'T'
      'CUSTOM_COMPANY_NUMBER'#9'9'#9'Comp. #'#9'T'
      'COMPANY_NAME'#9'20'#9'Company name'#9'T'
      'TAX_SERVICE'#9'1'#9'Tax'#9'T'
      'TAX_PAYMENT_REFERENCE_NUMBER'#9'20'#9'Reference'#9'T'
      'AMOUNT'#9'10'#9'Amount'#9'T')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TPROCESS_REPORT_CREDIT_LIAB\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsClCo
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 3
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    Visible = False
    PaintOptions.AlternatingRowColor = clCream
  end
  object cbMonth: TevComboBox [4]
    Left = 176
    Top = 8
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnChange = cbMonthChange
    Items.Strings = (
      'January'
      'February'
      'March'
      'April'
      'May'
      'June'
      'July'
      'August'
      'September'
      'October'
      'November'
      'December')
  end
  object edYear: TevEdit [5]
    Left = 40
    Top = 8
    Width = 41
    Height = 21
    ReadOnly = True
    TabOrder = 0
    Text = '2000'
  end
  object udYear: TUpDown [6]
    Left = 80
    Top = 8
    Width = 13
    Height = 19
    Min = 1990
    Max = 2100
    Position = 1990
    TabOrder = 2
    OnChangingEx = udYearChangingEx
  end
  object bLoad: TevBitBtn [7]
    Left = 336
    Top = 8
    Width = 81
    Height = 25
    Caption = 'Load'
    TabOrder = 4
    OnClick = bLoadClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D44444444444
      4DDDD888888888888DDDD4FFFF7FFFFF4DDDD8DDDDDDDDDD8DDDD4FFF708FFFF
      4DDDD8DDDDFDDDDD8DDDD4FF70A08FFF4DDDD8DDDF8FDDDD8DDDD4F70AAA08FF
      4888D8DDF888FDDD8D88D470AAAAA08F4778D8DF88888FDD8DD8D47AAAAAAA08
      4778D8D8888888FD8DD8D4777AAA07774778D8DDD888FDDD8DD8D4FF7AAA0888
      4778D8DDD888FDDD8DD8D4FF7AAA00000078D8DDD888FFFFFFD8D4444AAAAAAA
      A078D888D88888888FD8D4444AAAAAAAA078D888D88888888FD8DDDD8AAAAAAA
      A078DDDDD88888888FD8DDDD877777777778DDDD8DDDDDDDDDD8DDDD88888888
      8888DDDD888888888888DDDD888888888888DDDD888888888888}
    NumGlyphs = 2
  end
  object btnGo: TevBitBtn [8]
    Left = 520
    Top = 440
    Width = 131
    Height = 25
    Caption = 'Create credit liabs'
    TabOrder = 5
    OnClick = btnGoClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDFCDDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDDFCDDDDDD888DDDDD8FDDDDDDDDDDDFCDFCDFCDD86
      668D8FD8FD8FDDD888DDDFCDDFCDD86E6668D8FDD8FDDD8D888DDDDDDDDDD6EE
      6666DDDDDDDDD8DD8888DFCDDFCDD8DDDDD8D8FDD8FDD8DDDDD8FCDFCDFCD8DD
      DDD88FD8FD8FD8DDDDD8DDDFCDDDDD8DDD8DDDD8FDDDDD8DDD8DDDDFCDDDDD8D
      DD8DDDD8FDDDDD8DDD8DDDDDDD8DDDD8D8DDDDDDDDDDDDD8D8DDDDDDDD68DDD8
      68DDDDDDDD8DDDD888DDDDDDD6668DDD6DDDDDDDD888DDDD8DDD66666E666666
      6666888888888888888888888E6688888886DDDDD888DDDDDDD8DDDDDD68DDDD
      DD68DDDDDD8DDDDDDD8DDDDDDD68DDDDDDD6DDDDDD8DDDDDDDD8}
    NumGlyphs = 2
  end
  object cdMain: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N'
      'FUI_DO;CheckBox;True;False'
      'LOAD;CheckBox;True;False'
      'SUI_DO;CheckBox;True;False')
    Left = 584
    Top = 8
    object cdMainCUSTOM_CLIENT_NUMBER: TStringField
      DisplayLabel = 'Client #'
      DisplayWidth = 7
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdMainCLIENT_NAME: TStringField
      DisplayLabel = 'Client name'
      DisplayWidth = 15
      FieldKind = fkInternalCalc
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdMainCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Comp. #'
      DisplayWidth = 7
      FieldKind = fkInternalCalc
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdMainCOMPANY_NAME: TStringField
      DisplayLabel = 'Company name'
      DisplayWidth = 15
      FieldKind = fkInternalCalc
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdMainTAX_SERVICE: TStringField
      DisplayLabel = 'Tax'
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'TAX_SERVICE'
      FixedChar = True
      Size = 1
    end
    object cdMainCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object cdMainCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object cdMainAMOUNT: TCurrencyField
      FieldKind = fkInternalCalc
      FieldName = 'AMOUNT'
    end
    object cdMainTAX_PAYMENT_REFERENCE_NUMBER: TStringField
      FieldName = 'TAX_PAYMENT_REFERENCE_NUMBER'
    end
    object cdMainCO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
    end
    object cdMainSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
  end
  object dsClCo: TevDataSource
    DataSet = cdMain
    Left = 616
    Top = 8
  end
end
