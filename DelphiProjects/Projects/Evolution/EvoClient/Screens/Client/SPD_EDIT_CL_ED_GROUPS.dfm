inherited EDIT_CL_ED_GROUPS: TEDIT_CL_ED_GROUPS
  Width = 788
  Height = 575
  inherited Panel1: TevPanel
    Width = 788
    inherited pnlFavoriteReport: TevPanel
      Left = 673
    end
    inherited pnlTopRight: TevPanel
      Width = 673
    end
  end
  inherited PageControl1: TevPageControl
    Width = 788
    Height = 521
    HelpContext = 9002
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 780
        Height = 492
        inherited pnlBorder: TevPanel
          Width = 776
          Height = 488
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 776
          Height = 488
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 3
              Width = 797
              object sbCLBrowse: TScrollBox
                Left = 1
                Top = 1
                Width = 795
                Height = 563
                Align = alClient
                TabOrder = 0
                object pnlCLBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 791
                  Height = 559
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 547
                  end
                  object pnlCLBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 547
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCLLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Client'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCLBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 476
                    Height = 547
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCLRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 464
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'E/D Groups'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLRightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 355
              Top = 75
              Width = 365
              Height = 493
            end
          end
          inherited Panel3: TevPanel
            Width = 728
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 728
            Height = 381
            inherited Splitter1: TevSplitter
              Height = 377
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 377
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 297
                IniAttributes.SectionName = 'TEDIT_CL_ED_GROUPS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 372
              Height = 377
              Title = 'E/D Groups'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 322
                Height = 297
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_ED_GROUPS\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtED_Groups_Detail: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object fpCLEDGroupSummary: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 333
        Height = 378
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Summary'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object wwDBGrid1x: TevDBGrid
          Left = 12
          Top = 35
          Width = 300
          Height = 318
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'43'#9'Name'#9'F')
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CL_ED_GROUPS\wwDBGrid1x'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsDetail
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
      object fpCLEDGroupDetail: TisUIFashionPanel
        Left = 12
        Top = 394
        Width = 333
        Height = 131
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'E/D Group Details'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object lablName: TevLabel
          Left = 12
          Top = 35
          Width = 37
          Height = 16
          Caption = '~Name'
          FocusControl = dedtName
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBRadioGroup2: TevDBRadioGroup
          Left = 12
          Top = 74
          Width = 145
          Height = 36
          HelpContext = 8134
          Caption = '~Subtract Deductions'
          Columns = 2
          DataField = 'SUBTRACT_DEDUCTIONS'
          DataSource = wwdsDetail
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 1
          Values.Strings = (
            'Y'
            'N')
        end
        object evBitBtn1: TevBitBtn
          Left = 165
          Top = 85
          Width = 147
          Height = 25
          Caption = 'E/D Group Codes'
          TabOrder = 2
          OnClick = evBitBtn1Click
          Color = clBlack
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFEDEDEDCDCDCDCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEEEECECECECDCCCCCCCCCCCCCCCC
            CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
            CCA0B2C14B7DA368A4D9CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
            CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCB6B6B5868585B0AFAF5C5C5C5C5C5C
            5E5B5A5E5A595D5A5A5B5A5B5A5B5B5A5B5B5A5B5B5B5A5A5C59565768764E7E
            A44C80AC5082AB65A2D55C5C5C5C5C5C5B5B5B5A5A5A5A5A5A5A5A5A5B5B5B5B
            5B5B5B5B5B5A5A5A5959596A6A6A8686868989898B8B8BADADADFFFFFFFFFFFF
            FFFFFF3F69A57566677068696D69696C6A696C6A696C6A686E67624C89BA4E85
            B24D83AE5D8CB2629ED1FFFFFFFFFFFFFFFFFF7979796767676969696969696A
            6A6A6A6A6A6A6A6A6666669493938F8F8F8C8C8C949393AAA9A9FFFFFFFFFFFF
            FFFFFF13826B009346715C626A626367646366646367646268615B4F8ABB5086
            B44F84B16895B95F9BCDFFFFFFFFFFFFFFFFFF7A79798282825E5E5E63636364
            64646464646464646161609595949090908E8E8E9D9D9DA6A6A6CFCFCFCCCCCC
            CCCCCC008C464FDDB0008D436B585E655E6063616062605F645D57518DBE528A
            B75187B4739FC25D97C9D0D0D0CDCCCCCDCCCC7D7C7CCFCFCF7D7C7C5A5A5A5E
            5E5E6161606060605C5C5C989898949393919191A6A6A6A2A2A20D9154008A47
            00884500844100DAA260D9B3008D4268545A625B5C605C5A6058525490C2558C
            BA4E81AD7EA6C85A94C48282827B7B7B797979767675CACACACECDCD7D7C7C57
            57575B5B5B5C5C5C5757579B9B9B9796968B8B8BADADAD9F9F9E008A4763EDD0
            00D4A000D29E00CC9C00CD9C6FDCBD0093466154575C57565B534D5794C5588E
            BC47749B88AFCF5790C07B7B7BE3E3E2C4C4C4C3C2C2BEBDBDBEBEBED2D2D182
            82825656565757575352529F9F9E9898987D7C7CB6B6B59B9B9B008A4761E1D0
            60DDCA63DCC800C49B00C69C82E1C80094475C5054585353574F4A5A96CA5B8F
            BE22B9F795B5D3548DBC7B7B7BD9D9D9D5D5D5D4D4D4B7B6B6B9B9B9D8D8D883
            83835151515454534F4F4FA1A1A1999999C6C6C6BCBCBB979797109457008A47
            00884400853F00C1A097E3D1008F435A484E56505153514F524B455B9ACD5C91
            C120B7F59EBCD75189B88685857B7B7B797979767675B6B6B5DCDCDC7F7E7E4A
            4A4A5050505050504A4A4AA5A5A59C9C9CC4C3C3C2C2C1949393FFFFFFFFFFFF
            FFFFFF008B44A0E8DA00914455434A524B4D4F4D4E4F4D4C4D46415E9CD25C95
            C55990C1A6C4DF4E86B5FFFFFFFFFFFFFFFFFF7C7B7BE3E3E28180804545454B
            4B4B4E4E4E4D4C4C454545A9A8A8A09F9F9B9B9BCACACA919191FFFFFFFFFFFF
            FFFFFF17866D009647523F454F47494D494A4C4A4A4C48484A423D60A0D55D98
            C95894C6AFCCE64B83B0FFFFFFFFFFFFFFFFFF7E7E7D86858542424148484849
            49494A4A4A484848414141ACACACA3A3A39F9F9ED2D2D18D8D8DFFFFFFFFFFFF
            FFFFFF4D7BB04C3D3B4A4343484544484644484644474542433C365FA1D85C9A
            CC5896C9B8D3EB4980ACFFFFFFFFFFFFFFFFFF8787873D3D3D43434344444445
            45454545454444443A3A3AADADADA5A5A5A1A1A1D8D8D8898989FFFFFFFFFFFF
            FFFFFF4A7FAC443831433B37433D38433D38433D38423B363C332CB9DAF57FB0
            DA5495CCC0DAEF467CA8FFFFFFFFFFFFFFFFFF8989893737373A3A3A3C3C3C3C
            3C3C3C3C3C3A3A3A313131E0DFDFB9B9B9A1A1A1DFDFDE868585FFFFFFFFFFFF
            FFFFFF82A6C34A82AE4A83B04A83B04A83B04A83B04A82AF447DA9709CBFB9D5
            EBB3D1EAC1DBF24279A5FFFFFFFFFFFFFFFFFFACACAC8B8B8B8D8D8D8D8D8D8D
            8D8D8D8D8D8C8C8C868686A3A3A3DADADAD7D6D6E0E0E0828282FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFEBF3FACEE4F63F75A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F5E8E8E87F7E7E}
          NumGlyphs = 2
          Margin = 0
        end
        object dedtName: TevDBEdit
          Left = 12
          Top = 50
          Width = 300
          Height = 21
          HelpContext = 9002
          DataField = 'NAME'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
  end
end
