inherited EDIT_CL_BANK_ACCOUNT: TEDIT_CL_BANK_ACCOUNT
  HelpContext = 7001
  inherited PageControl1: TevPageControl
    HelpContext = 7008
    OnChange = PageControl1Change
    OnChanging = nil
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            Width = 861
            inherited Panel2: TevPanel
              Left = 3
              Width = 858
              object sbCLBrowse: TScrollBox
                Left = 1
                Top = 1
                Width = 856
                Height = 563
                Align = alClient
                TabOrder = 0
                object pnlCLBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 852
                  Height = 559
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 547
                  end
                  object pnlCLBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 547
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCLLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Client'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCLBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 537
                    Height = 547
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCLRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 525
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Bank Accounts'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLRightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 355
              Top = 75
              Width = 349
              Height = 517
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 386
                IniAttributes.SectionName = 'TEDIT_CL_BANK_ACCOUNT\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Bank Accounts'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 585
                Height = 386
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CUSTOM_BANK_ACCOUNT_NUMBER'#9'32'#9'Bank Account Number'#9'F'
                  'Print_Name_Lookup'#9'20'#9'Print Name'#9'F'
                  'aba_nbr_lookup'#9'20'#9'ABA Nbr'#9'F'
                  'Description'#9'20'#9'Description'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_BANK_ACCOUNT\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                OnAfterDrawCell = wwDBGrid1AfterDrawCell
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtBank_Account_Details: TTabSheet
      HelpContext = 7001
      Caption = 'Details'
      ImageIndex = 2
      object bevCL_BANK1: TEvBevel
        Left = 0
        Top = 0
        Width = 435
        Height = 194
        Align = alClient
      end
      object lablBeginning_Balance: TevLabel
        Left = 520
        Top = 520
        Width = 89
        Height = 13
        Caption = 'Beginning Balance'
        Visible = False
      end
      object wwdeBeginning_Balance: TevDBEdit
        Left = 520
        Top = 544
        Width = 129
        Height = 21
        HelpContext = 7002
        DataField = 'BEGINNING_BALANCE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 0
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object pnlListOfCLBankAccount: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 628
        Height = 233
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Summary'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object wwdgTeamMembers: TevDBGrid
          Left = 12
          Top = 35
          Width = 595
          Height = 177
          DisableThemesInTitle = False
          Selected.Strings = (
            'CUSTOM_BANK_ACCOUNT_NUMBER'#9'32'#9'Bank Account Number'#9'F'
            'Bank_Name'#9'39'#9'Bank Name'#9'F'
            'Description'#9'20'#9'Description'#9'F')
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CL_BANK_ACCOUNT\wwdgTeamMembers'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsDetail
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          OnAfterDrawCell = wwdgTeamMembersAfterDrawCell
          NoFire = False
        end
      end
      object pnlCLBankAccountInfo: TisUIFashionPanel
        Left = 8
        Top = 249
        Width = 628
        Height = 259
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 2
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Bank Account Details'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object Label1x: TevLabel
          Left = 12
          Top = 35
          Width = 34
          Height = 16
          Caption = '~Bank'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablCustom_Bank_Account_Number: TevLabel
          Left = 12
          Top = 74
          Width = 97
          Height = 16
          Caption = '~Bank Account Nbr'
          FocusControl = dedtCustom_Bank_Account_Number
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablNext_Check_Number: TevLabel
          Left = 12
          Top = 113
          Width = 131
          Height = 16
          Caption = '~Next Available Check Nbr'
          FocusControl = dedtNext_Check_Number
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablDescription: TevLabel
          Left = 12
          Top = 152
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = dedtNext_Check_Number
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablSignature: TevLabel
          Left = 240
          Top = 113
          Width = 45
          Height = 13
          Caption = 'Signature'
          FocusControl = dimgSignature
        end
        object evLabel1: TevLabel
          Left = 481
          Top = 35
          Width = 114
          Height = 13
          Caption = 'Recurring Wire Number '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablLogo: TevLabel
          Left = 481
          Top = 74
          Width = 24
          Height = 13
          Caption = 'Logo'
          FocusControl = dimgLogo
        end
        object wwDBLookupCombo1: TevDBLookupCombo
          Left = 12
          Top = 50
          Width = 220
          Height = 21
          HelpContext = 7010
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'Name'
            'ABA_NUMBER'#9'9'#9'ABA Number'
            'CITY'#9'20'#9'City'
            'STATE'#9'2'#9'State')
          DataField = 'SB_BANKS_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_SB_BANKS.SB_BANKS
          LookupField = 'SB_BANKS_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object dedtCustom_Bank_Account_Number: TevDBEdit
          Left = 12
          Top = 89
          Width = 220
          Height = 21
          HelpContext = 7003
          DataField = 'CUSTOM_BANK_ACCOUNT_NUMBER'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtNext_Check_Number: TevDBEdit
          Left = 12
          Top = 128
          Width = 220
          Height = 21
          HelpContext = 7004
          DataField = 'NEXT_CHECK_NUMBER'
          DataSource = wwdsDetail
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dmemDescript: TEvDBMemo
          Left = 12
          Top = 167
          Width = 220
          Height = 67
          HelpContext = 27503
          DataField = 'Description'
          DataSource = wwdsDetail
          TabOrder = 3
        end
        object drgpBank_Account_Type: TevDBRadioGroup
          Left = 240
          Top = 35
          Width = 233
          Height = 76
          HelpContext = 7007
          Caption = '~Bank Account Type'
          DataField = 'BANK_ACCOUNT_TYPE'
          DataSource = wwdsDetail
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Checking'
            'Savings'
            'Money Market')
          ParentFont = False
          TabOrder = 4
          Values.Strings = (
            'C'
            'S'
            'M')
        end
        object dimgSignature: TevDBImage
          Left = 240
          Top = 128
          Width = 233
          Height = 72
          HelpContext = 7005
          DataField = 'SIGNATURE_CL_BLOB_NBR'
          DataSource = wwdsDetail
          Stretch = True
          TabOrder = 5
        end
        object bbtnLoad_Signature: TevBitBtn
          Left = 240
          Top = 208
          Width = 233
          Height = 25
          Caption = 'Load Signature'
          TabOrder = 6
          OnClick = bbtnLoad_SignatureClick
          Color = clBlack
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCC
            CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFF5A6C7A4B67845291D9CCCCCCFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E6E6E6D
            6D6DA3A3A3CCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F4F4
            F5F5F5F5F5F5F5F5F5F8F8F862859D82A7B690D6FF366A9ACCCCCCF7F7F7F4F4
            F4FFFFFFFFFFFFFFFFFFFFFFFFF4F4F4F6F6F5F6F6F5F6F6F5F9F9F8898989AA
            A9A9DEDEDE767675CCCCCCF7F7F7F4F4F4FFFFFFFFFFFFFFFFFFD0D0D0AFC1CE
            B1C2CEB1C2CDB3C3CEBAC5CD4FB2F18FE7FF81D4FF159BFF3C6A97B7C5CEB0C2
            CED0D0D0FFFFFFFFFFFFD0D0D0C4C3C3C4C3C3C4C3C3C5C5C5C7C7C7C0C0C0EA
            EAEADCDCDCB5B5B5757474C6C6C6C4C4C4D0D0D0FFFFFFFFFFFF4F9ED34398D2
            4094D03E92CF3E92CE3E91CE3D91CD2E73C243C4FF2BABFF1A9DFF3C6895479C
            D5509FD5EDEDEDFFFFFFAAA9A9A5A5A5A1A1A1A09F9FA09F9F9F9F9E9F9F9E88
            8888D0D0D0BFBFBFB7B6B6737373A9A8A8AAAAAAEDEDEDFFFFFF4499D23F94D0
            ABFBFF9BF3FF92F1FF93F1FF93F1FF97F5FF3072BF47C6FF2DABFF179BFF3466
            986AC3EEA4C3D9FFFFFFA6A6A6A1A1A1FAF9F9F3F3F3F1F1F1F1F1F1F1F1F1F4
            F4F4868686D1D1D1BFBFBFB5B5B5727171CBCBCBC8C8C7FFFFFF4397D156ACDD
            8EDAF5A2EDFF82E5FE84E5FE85E6FE87E8FF8EEFFF3273BF44C6FF22AAFF85AF
            D485746764AAD8FFFFFFA4A4A4B6B6B5DEDEDEEFEEEEE8E7E7E8E8E8E8E8E8EB
            EBEAF0EFEF878787D1D1D1BEBEBEB7B6B6737373B3B3B3FFFFFF4296D171C4EA
            6CBCE6BBF2FF75DEFD77DEFC78DFFC7CE0FD80E3FE85E9FF2C72C3B2DAEE9388
            7EC4BDB5807868CCCCCCA3A3A3CBCBCBC4C4C4F3F3F3E2E2E2E2E2E2E3E3E2E4
            E3E3E7E7E6EBEBEA888888DEDEDE878787BDBCBC767675CCCCCC4095D090DDF8
            44A0D8DDFCFFDAFAFFDBF9FFDEFAFF74DCFC76DBFB77DDFC76E2FF8F7B71EEE7
            E1898B81BD78B5A065C9A2A2A2E1E1E1ACACACFDFDFCFBFBFBFAFAFAFBFBFBE0
            E0E0E0DFDFE1E1E1E6E6E67B7B7BE8E7E78989898E8E8E8888883E93CFB2F6FF
            51ACDE358ACA358ACA358ACA368ACA5BBDE96ED9FB6AD6FB67D8FD62DCFF867D
            73E4B0DECF95C5B77ACDA1A0A0F6F6F5B6B6B5999999999999999999999999C5
            C5C5DEDEDEDCDBDBDEDEDEE1E1E17C7B7BC0C0C0A6A6A69796963D92CFB8F3FF
            77DFFE7BE0FE7CE1FE7CE1FF7DE2FF52ABDD56B9E8DAF8FFD6F7FFD4F9FFD1FF
            FFCD84D1CF86D2359DD4A09F9FF4F4F4E4E3E3E4E4E4E5E5E5E5E5E5E6E6E6B5
            B5B5C2C2C1FAF9F9F9F9F8FAF9F9FEFEFEA09F9FA1A0A0A9A8A83C92CFC0F3FF
            70D9FB73DAFB74DAFB74DAFB74DBFB76DEFD4FA9DC368BCA358BCB338CCB328F
            CE3697D13C9AD37EBBE2A09F9FF4F4F4DEDEDEDFDFDEDFDFDEDFDFDEE0DFDFE2
            E2E2B3B3B39A9A9A9A9A9A9B9B9B9E9E9EA4A4A4A6A6A6C3C2C23B92CFCAF6FF
            69D5F96CD5F96AD4F969D4F969D5F96AD6FA6BD8FB6BD9FC6BDAFD69DAFDDAFD
            FF3C94D0DAE7F0FFFFFFA09F9FF7F7F7DBDADADBDADADADADADADADADBDADADC
            DBDBDDDDDDDEDEDEE0DFDFE0DFDFFDFDFCA1A1A1E8E8E8FFFFFF3B92CFD5F7FF
            60D1F961D0F8B4EBFDD9F6FFDAF8FFDAF8FFDAF9FFDBF9FFDAF9FFDAFAFFDFFE
            FF3D94D0D9EAF6FFFFFFA09F9FF8F8F8D7D7D7D7D6D6EEEEEEF8F8F8FAF9F9FA
            F9F9FAFAFAFAFAFAFAFAFAFBFBFBFEFEFEA1A1A1ECECECFFFFFF3D94D0DCFCFF
            D8F7FFD8F7FFDBFAFF358ECD3991CE3A92CF3A92CF3A92CF3A92CF3A92CF3D94
            D051A1D6FFFFFFFFFFFFA1A1A1FDFDFCF9F9F8F9F9F8FBFBFB9D9D9D9F9F9EA0
            9F9FA09F9FA09F9FA09F9FA09F9FA1A1A1ACACACFFFFFFFFFFFF4F9FD53D94D0
            3A92CF3A92CF3D94D055A2D6E0EEF8DBEBF7DAEBF6DAEBF6DAEBF6DAEBF6D7E9
            F6FFFFFFFFFFFFFFFFFFAAAAAAA1A1A1A09F9FA09F9FA1A1A1ADADADF0EFEFED
            EDEDEDEDEDEDEDEDEDEDEDEDEDEDECECECFFFFFFFFFFFFFFFFFF}
          NumGlyphs = 2
          ParentColor = False
          Margin = 0
        end
        object edRecurringWire: TevDBEdit
          Left = 481
          Top = 50
          Width = 127
          Height = 21
          HelpContext = 7004
          DataField = 'RECURRING_WIRE_NUMBER'
          DataSource = wwdsDetail
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dimgLogo: TevDBImage
          Left = 481
          Top = 89
          Width = 127
          Height = 113
          HelpContext = 7006
          DataField = 'LOGO_CL_BLOB_NBR'
          DataSource = wwdsDetail
          Stretch = True
          TabOrder = 8
        end
        object bbtnLoad_Logo: TevBitBtn
          Left = 481
          Top = 208
          Width = 127
          Height = 25
          Caption = 'Load Logo'
          TabOrder = 9
          OnClick = bbtnLoad_LogoClick
          Color = clBlack
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
            CCCCCCCCCCCCCCCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDCDCCC
            CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCFCFCFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFC29D76B6844CB58349B68248B68248B68248B682
            48B58349B6844DBA8D5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9898987F
            7E7E7E7E7D7D7C7C7D7C7C7D7C7C7D7C7C7E7E7D7F7E7E888888FFFFFFF4F4F4
            F5F5F5F5F5F5F5F5F5F8F8F8BF8343FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFB6844DFFFFFFF4F4F4F6F6F5F6F6F5F6F6F5F9F9F87E7E7DFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7E7ED0D0D0AFC1CE
            B1C2CEB1C2CDB3C3CEB9C6CFC1813DFFFFFF0E3FF21266F3259AF629A6F81875
            F4003BF2FFFFFFB58349D0D0D0C4C3C3C4C3C3C4C3C3C5C5C5C8C8C77C7B7BFF
            FFFF868585959594B2B1B1B9B9B99C9C9C858484FFFFFF7E7E7D4F9ED34398D2
            4094D03E92CF3D92D13394DAC07F3BFFFFFC2B52F23579F22E93F4329AF42577
            F1073EF0FFFFFFB68349AAA9A9A5A5A5A1A1A1A09F9FA1A0A0A4A4A47B7B7BFF
            FFFF8C8C8C9E9E9EADADADB1B0B09C9C9C838383FFFFFF7E7E7D4499D23F94D0
            ABFBFF9BF3FF91F2FF8BF6FFBD7C39FFFFFA1F3FF0517EF25293F43783F21B5D
            F00833F0FFFFFCB68349A6A6A6A1A1A1FAF9F9F3F3F3F2F2F1F5F5F5787877FF
            FFFF858484A1A0A0ADADADA3A3A38F8F8F818080FFFFFF7E7E7D4397D156ACDD
            8EDAF5A2EDFF81E6FF7DECFFBC7B38FFFFF91024EE4062F05D85F37295F4839A
            F6808DF9FFFFFAB58349A4A4A4B6B6B5DEDEDEEFEEEEE9E9E9EDEDED777676FF
            FFFF7D7C7C929292A5A5A5B0AFAFB4B4B4ADADADFFFFFF7E7E7D4296D171C4EA
            6CBCE6BBF2FF74DFFF70E5FFBB7B38FFFFFC0002EC2D40ED4960F05C73F26C7B
            F47275F6FFFFFDB58349A3A3A3CBCBCBC4C4C4F3F3F3E4E3E3E8E7E7777676FF
            FFFF7979798383839191919B9B9BA1A0A09F9F9EFFFFFF7E7E7D4095D090DDF8
            44A0D8DDFCFFD9FBFFD5FFFFBA7C3AFFFFFF0000EC0C0DEE272EF03D45F25052
            F55E5DF7FFFFFFB5834AA2A2A2E1E1E1ACACACFDFDFCFBFBFBFEFEFE777676FF
            FFFF7878777A79798180808787878E8E8E949393FFFFFF7E7E7D3E93CFB2F6FF
            51ACDE358ACA338BCD2D8ED6BF7D3BFFFFF0FFFFEBFFFFEAFFFFEAFFFFE9FFFF
            E9FFFFEBFFFFF4B9844BA1A0A0F6F6F5B6B6B59999999B9B9BA09F9F797979FE
            FDFDFCFCFBFCFCFBFCFCFBFCFCFBFCFCFBFCFCFBFEFEFE7F7E7E3D92CFB8F3FF
            77DFFE7BE0FE7BE1FF76E4FFA5A487C07C3AC07F3CC1813DC1823DC1813DC081
            3DC1823EC28341A98C65A09F9FF4F4F4E4E3E3E4E4E4E5E5E5E7E7E69F9F9E78
            78777B7B7B7C7B7B7D7C7C7C7B7B7C7B7B7D7C7C7E7E7D8787873C92CFC0F3FF
            70D9FB73DAFB74DAFB72DBFE6FDEFF6DE1FF47AEEA3292D93293D93193DA3194
            DB3598DC399ADE7ABBE9A09F9FF4F4F4DEDEDEDFDFDEDFDFDEE0E0E0E3E3E2E4
            E4E4BBBBBBA3A3A3A3A3A3A4A4A4A5A5A5A8A8A7AAA9A9C4C4C43B92CFCAF6FF
            69D5F96CD5F96AD4F969D5F968D5FB68D7FC6AD9FE6BDBFF6BDBFF69DBFFDAFE
            FF3C94D2DAE7F0FFFFFFA09F9FF7F7F7DBDADADBDADADADADADBDADADBDADADD
            DDDDDFDFDEE0E0E0E0E0E0E0E0E0FEFDFDA2A2A2E8E8E8FFFFFF3B92CFD5F7FF
            60D1F961D0F8B4EBFDD9F6FFDAF8FFDAF8FFDAF9FFDBF9FFDAF9FFDAFAFFDFFE
            FF3D94D0D9EAF6FFFFFFA09F9FF8F8F8D7D7D7D7D6D6EEEEEEF8F8F8FAF9F9FA
            F9F9FAFAFAFAFAFAFAFAFAFBFBFBFEFEFEA1A1A1ECECECFFFFFF3D94D0DCFCFF
            D8F7FFD8F7FFDBFAFF358ECD3991CE3A92CF3A92CF3A92CF3A92CF3A92CF3D94
            D051A1D6FFFFFFFFFFFFA1A1A1FDFDFCF9F9F8F9F9F8FBFBFB9D9D9D9F9F9EA0
            9F9FA09F9FA09F9FA09F9FA09F9FA1A1A1ACACACFFFFFFFFFFFF4F9FD53D94D0
            3A92CF3A92CF3D94D055A2D6E0EEF8DBEBF7DAEBF6DAEBF6DAEBF6DAEBF6D7E9
            F6FFFFFFFFFFFFFFFFFFAAAAAAA1A1A1A09F9FA09F9FA1A1A1ADADADF0EFEFED
            EDEDEDEDEDEDEDEDEDEDEDEDEDEDECECECFFFFFFFFFFFFFFFFFF}
          NumGlyphs = 2
          ParentColor = False
          Margin = 0
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 408
    Top = 21
  end
end
