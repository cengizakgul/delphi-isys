// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_ED_GROUP_CODES;
{**************************************************************************}
{* unit: SPD_EDIT_CL_ED_GROUP_CODES.pas                                   *}
{* Description: Form used to maintain Client ED Group Codes information   *}
{*              from the Client -> ED Group Codes comboboxes.             *}
{*                                                                        *}
{* Copyright(c) PayData Payroll Services, Inc.                            *}
{*                                                                        *}
{* Author:  Tim Stevenson                                                 *}
{* Written: 04/25/1998                                                    *}
{* Updated: 04/29/1998                                                    *}
{*                                                                        *}
{**************************************************************************}

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, StdCtrls, wwdblook, Db, Wwdatsrc,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls,
  Controls, Classes, SPD_EDIT_COPY, SPackageEntry, EvSecElement, Forms,
  sCopyHelper, sClientCopyHelper, Variants, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CL_ED_GROUP_CODES = class(TEDIT_CL_BASE)
    tshtCL_ED_Group_Codes: TTabSheet;
    dtxtNAME: TevDBText;
    lablTeam: TevLabel;
    wwdgEDGROUPCODES: TevDBGrid;
    wwdsSubMaster: TevDataSource;
    evLabel1: TevLabel;
    evDBText1: TevDBText;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    pnlCLBrowseRight: TevPanel;
    fpCLRight: TisUIFashionPanel;
    pnlFPCLRightBody: TevPanel;
    evSplitter1: TevSplitter;
    pnlCLBrowseRight2: TevPanel;
    fpCLRight2: TisUIFashionPanel;
    pnlFPCLRight2Body: TevPanel;
    pnlSubbrowse2: TevPanel;
    fpCLEDCodesSummary: TisUIFashionPanel;
    fpCLEDCodesDetail: TisUIFashionPanel;
    wwDBGrid1x: TevDBGrid;
    lablCL_E_DS_Nbr: TevLabel;
    wwlcCL_E_DS_Nbr: TevDBLookupCombo;
    wwlcDescription: TevDBLookupCombo;
    lablDescription: TevLabel;
    evBitBtn1: TevBitBtn;
    evSplitter2: TevSplitter;
    fpGroupCodes: TisUIFashionPanel;
    wwDBGrid1: TevDBGrid;
    wwdgEDGroups: TevDBGrid;
    CopyHelper: TClientCopyHelper;
    procedure evBitBtn1Click(Sender: TObject);
    procedure fpCLRightResize(Sender: TObject);
  private
    procedure HandleBeforeCopy(Sender: TObject;
      selectedDetails: TEvClientDataSet; params: TStringList);
    function HandleCopy(Sender: TObject; client: integer; selectedDetails,
      details: TEvClientDataSet; params: TStringList): integer;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

uses
  evutils, sCopier, sysutils, dialogs;
{$R *.DFM}

procedure TEDIT_CL_ED_GROUP_CODES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  inherited;
end;

function TEDIT_CL_ED_GROUP_CODES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_E_D_GROUP_CODES;
end;

function TEDIT_CL_ED_GROUP_CODES.GetInsertControl: TWinControl;
begin
  Result := wwlcCL_E_DS_NBR;
end;

procedure TEDIT_CL_ED_GROUP_CODES.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  CopyForm: TEDIT_COPY;
begin
  inherited;
  case Kind of
    NavInsert:
      begin
        CopyForm := TEDIT_COPY.Create(Self);
        try
          CopyForm.CopyTo := 'CLEDGroupCodes';
          CopyForm.CopyFrom := 'CL_E_DS';
          DM_CLIENT.CL_E_D_GROUP_CODES.Cancel;
          DM_CLIENT.CL_E_D_GROUP_CODES.DisableControls;
          CopyForm.ShowModal;
          DM_CLIENT.CL_E_D_GROUP_CODES.EnableControls;
        finally
          CopyForm.Free;
        end;
        Handled := True;
      end;
  end;
end;

procedure TEDIT_CL_ED_GROUP_CODES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_CLIENT.CL_E_D_GROUPS, SupportDataSets, '');
end;

procedure TEDIT_CL_ED_GROUP_CODES.evBitBtn1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CL_ED_GROUPS')
end;

procedure TEDIT_CL_ED_GROUP_CODES.Activate;
var
  dummy: boolean;
begin
  inherited;
  CopyHelper.Grid := wwdgEDGroups; //gdy
  CopyHelper.UserFriendlyName := 'E/D Group'; //gdy
  CopyHelper.DeleteEnabled := false;
  CopyHelper.OnBeforeCopy := HandleBeforeCopy; //gdy
  CopyHelper.OnCopyDetail := HandleCopy; //gdy
//  CopyHelper.OnDeleteDetail := HandleDelete; //gdy
  GetDataSetsToReopen( CopyHelper.FDSToSave, dummy );//gdy
end;

procedure TEDIT_CL_ED_GROUP_CODES.HandleBeforeCopy(Sender: TObject; selectedDetails: TEvClientDataSet;
  params: TStringList);
var
  edCount: integer;
begin
  edCount := 0;
  DM_CLIENT.CL_E_D_GROUP_CODES.DisableControls;
  try
    DM_CLIENT.CL_E_D_GROUP_CODES.EnsureDSCondition('CL_E_D_GROUPS_NBR='+selectedDetails.fieldByName('CL_E_D_GROUPS_NBR').AsString);
    DM_CLIENT.CL_E_D_GROUP_CODES.First;
    while not DM_CLIENT.CL_E_D_GROUP_CODES.Eof do
    begin
      params.Values['CUSTOM_E_D_CODE_NUMBER#'+IntToStr(edCount)] := DM_CLIENT.CL_E_D_GROUP_CODES.FieldByName('Custom_Code_lookup').AsString;
      inc( edCount );
      DM_CLIENT.CL_E_D_GROUP_CODES.Next;
    end;
    params.Values['CUSTOM_E_D_CODE_NUMBER#Count'] := IntToStr( edCount );
   finally
     DM_CLIENT.CL_E_D_GROUP_CODES.EnableControls;
   end;
end;

function TEDIT_CL_ED_GROUP_CODES.HandleCopy(Sender: TObject; client: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList): integer;
var
  i: integer;
  k: integer;
  fn: string;
  edCount: integer;
  msg: string;
  ed: string;
  ednbr: integer;
  absentCodes: string;
  absentCount: integer;
  v: Variant;
begin
  edCount := strtoint(params.Values['CUSTOM_E_D_CODE_NUMBER#Count']);
  absentCount := 0;
  DM_CLIENT.CL_E_DS.EnsureDsCondition('');
  for i := 0 to edCount - 1 do
  begin
    ed := params.Values['CUSTOM_E_D_CODE_NUMBER#'+IntToStr(i)];
    v := DM_CLIENT.CL_E_DS.Lookup( 'CUSTOM_E_D_CODE_NUMBER', ed, 'CL_E_DS_NBR' );
    if not VarIsNull( v ) then
      params.Objects[ params.IndexOfName('CUSTOM_E_D_CODE_NUMBER#'+IntToStr(i)) ] := TObject( integer(v) )
    else
    begin
      inc( absentCount );
      if absentCodes <> '' then
        absentCodes := absentCodes + ', ' + ed
      else
        absentCodes := ed;
    end
  end;

  if absentCount = 0 then
  begin
    if details.Locate('NAME', VarArrayOf([selecteddetails.FieldByNAme('NAME').AsString]), []) then
      details.Edit
    else
      details.Insert;
    for k := 0 to SelectedDetails.Fields.Count - 1 do
    begin
      fn := Selecteddetails.Fields[k].FieldName;
      if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) then
        details[fn] := selectedDetails[fn];
    end;
    details.Post;
    ctx_DataAccess.PostDataSets([details]);
    DM_CLIENT.CL_E_D_GROUP_CODES.EnsureDSCondition('CL_E_D_GROUPS_NBR='+details.fieldByName('CL_E_D_GROUPS_NBR').AsString);
    for i := 0 to edCount - 1 do
    begin
      ednbr := integer( params.Objects[params.IndexOfName('CUSTOM_E_D_CODE_NUMBER#'+IntToStr(i))] );
      if VarIsNull( DM_CLIENT.CL_E_D_GROUP_CODES.Lookup('CL_E_DS_NBR', ednbr,'CL_E_D_GROUP_CODES_NBR'{anything}) ) then
      begin
        DM_CLIENT.CL_E_D_GROUP_CODES.Insert;
        DM_CLIENT.CL_E_D_GROUP_CODES['CL_E_DS_NBR'] := ednbr;
        DM_CLIENT.CL_E_D_GROUP_CODES['CL_E_D_GROUPS_NBR'] := details['CL_E_D_GROUPS_NBR'];
        DM_CLIENT.CL_E_D_GROUP_CODES.Post;
      end;
    end;
    ctx_DataAccess.PostDataSets([DM_CLIENT.CL_E_D_GROUP_CODES]);
  end
  else
  begin
    if absentCount = 1 then
      msg := Format( 'Cannot copy %s to company %s because E/D %s doesn''t exist',
             [ selectedDetails.FieldByNAme('NAME').AsString,
               (Sender as TClientDetailCopier).TargetClientNumber,
               absentCodes  ] )
    else
      msg := Format( 'Cannot copy %s to company %s because E/Ds %s don''t exist',
             [ selectedDetails.FieldByNAme('NAME').AsString,
               (Sender as TClientDetailCopier).TargetClientNumber,
               absentCodes  ] );
    EvErrMessage(msg);
  end;
  ctx_DataAccess.OpenDataSets([DM_CLIENT.CL_E_D_GROUP_CODES]);
  Result := Details.FieldByName(Details.TableName + '_NBR').AsInteger;
end;

procedure TEDIT_CL_ED_GROUP_CODES.fpCLRightResize(Sender: TObject);
begin
  inherited; pnlFPCLRightBody.Width  := fpCLRight.Width - 40;
  pnlFPCLRightBody.Height := fpCLRight.Height - 65;
end;

initialization
  RegisterClass(TEDIT_CL_ED_GROUP_CODES);

end.
