// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_AGENCY;
{**************************************************************************}
{* unit: SPD_EDIT_CL_AGENCY.pas                                           *}
{* Description: Form used to maintain Client Agencies information         *}
{*              from the Client -> Agencies comboboxes.                   *}
{*                                                                        *}
{* Copyright(c) PayData Payroll Services, Inc.                            *}
{*                                                                        *}
{* Author:  Tim Stevenson                                                 *}
{* Written: 04/25/1998                                                    *}
{* Updated: 04/28/1998                                                    *}
{*                                                                        *}
{**************************************************************************}

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, wwdblook, Wwdotdot,
  Wwdbcomb, StdCtrls, DBCtrls, Wwdbspin, Mask, wwdbedit, ExtCtrls, Db,
  Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Controls, Classes,
  Forms, sCopyHelper, sClientCopyHelper, Variants, 
  SDDClasses, ISBasicClasses, SDataDictbureau, SDataDictclient,
  SDataDicttemp,SPackageEntry, EvContext, EvConsts, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBComboBox, isUIDBMemo, isUIwwDBEdit, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_AGENCY = class(TEDIT_CL_BASE)
    tshtClient_Agency_Details: TTabSheet;
    lablReporting_Method: TevLabel;
    lablFrequency: TevLabel;
    lablReporting_Month: TevLabel;
    lablCL_Bank_Account_Number: TevLabel;
    lablDelivery_Group_Number: TevLabel;
    lablCustom_Field: TevLabel;
    lablNotes: TevLabel;
    dedtCustom_Field: TevDBEdit;
    wwseReporting_Month: TevDBSpinEdit;
    dmemNotes: TEvDBMemo;
    wwcbReporting_Method: TevDBComboBox;
    wwcbFrequency: TevDBComboBox;
    lablAgency_Name: TevLabel;
    wwlcBankAccount: TevDBLookupCombo;
    wwlcDeliveryGroup: TevDBLookupCombo;
    wwlcAgencyName: TevDBLookupCombo;
    wwPayment_Type: TevDBComboBox;
    Label1: TevLabel;
    bev_CL_agency1: TEvBevel;
    AgencySrc: TevDataSource;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    AgencyReportSrc: TevDataSource;
    evLabel10: TevLabel;
    evDBEdit5: TevDBEdit;
    evLabel27: TevLabel;
    evDBEdit2: TevDBEdit;
    edMemo1: TevDBEdit;
    lblMemo1: TevLabel;
    edMemo2: TevDBEdit;
    lblMemo2: TevLabel;
    wwDBGrid1: TevDBGrid;
    CopyHelper: TClientCopyHelper;
    procedure PageControl1Change(Sender: TObject);
    procedure ResultFIELDS_CL_BANK_Function;
  private
    function HandleCopy(Sender: TObject; client: integer; selectedDetails, details: TEvClientDataSet; params: TStringList): integer;
    function HandleDelete(Sender: TObject; client: integer; selectedDetails, details: TEvClientDataSet; params: TStringList): integer;
    procedure HandleBeforeCopy(Sender: TObject; selectedDetails: TEvClientDataSet; params: TStringList);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    GL_Setuped : boolean;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure Activate; override;
    procedure AfterClick(Kind: Integer); override;

  end;

implementation

uses
  sCopier, evutils, sysutils, dialogs;
{$R *.DFM}

procedure TEDIT_CL_AGENCY.Activate;
var
  dummy: boolean;
begin
  inherited;
  GL_Setuped := false;
  CopyHelper.Grid := wwDBGrid1; //gdy
  CopyHelper.UserFriendlyName := 'Agency'; //gdy
  CopyHelper.OnCopyDetail := HandleCopy; //gdy
  CopyHelper.OnDeleteDetail := HandleDelete; //gdy
  CopyHelper.OnBeforeCopy := HandleBeforeCopy; //gdy
  GetDataSetsToReopen( CopyHelper.FDSToSave, dummy ); //gdy
end;

procedure TEDIT_CL_AGENCY.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  case Kind of
    NavInsert:
    begin
     GL_Setuped := CheckGlSetupExist(DM_CLIENT.CL_AGENCY,['GL_TAG','GL_OFFSET_TAG'],false);
    end;
    NavOk:
    begin
      if GL_Setuped and not CheckGlSetuped(DM_CLIENT.CL_AGENCY,['GL_TAG','GL_OFFSET_TAG']) then
           EvMessage(sDontForgetSetupGL);
      GL_Setuped := false;
     end;
  end;
  inherited;

end;

procedure TEDIT_CL_AGENCY.AfterClick(Kind: Integer);
begin
  inherited;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CL_AGENCY.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_CLIENT.CL_DELIVERY_GROUP, aDS);
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  inherited;
end;

function TEDIT_CL_AGENCY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_AGENCY;
end;

function TEDIT_CL_AGENCY.GetInsertControl: TWinControl;
begin
  Result := wwlcAgencyName;
end;

//begin gdy
procedure TEDIT_CL_AGENCY.HandleBeforeCopy(Sender: TObject; selectedDetails: TEvClientDataSet;
  params: TStringList);
var
  v: Variant;
begin
  v := DM_CLIENT.CL_BANK_ACCOUNT.Lookup( 'CL_BANK_ACCOUNT_NBR', selectedDetails['CL_BANK_ACCOUNT_NBR'], 'SB_BANKS_NBR;CUSTOM_BANK_ACCOUNT_NUMBER;BANK_ACCOUNT_TYPE' );
  if not VarIsNull( v ) then
  begin
    params.Values['SB_BANKS_NBR'] := v[0];
    params.Values['CUSTOM_BANK_ACCOUNT_NUMBER'] := v[1];
    params.Values['BANK_ACCOUNT_TYPE'] := v[2];
  end;
  v := DM_CLIENT.CL_DELIVERY_GROUP.Lookup( 'CL_DELIVERY_GROUP_NBR', selectedDetails['CL_DELIVERY_GROUP_NBR'], 'DELIVERY_GROUP_DESCRIPTION' );
  if not VarIsNull( v ) then
    params.Values['DELIVERY_GROUP_DESCRIPTION'] := v;
end;

function TEDIT_CL_AGENCY.HandleCopy(Sender: TObject; client: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList): integer;
var
  k: integer;
  fn: string;
  v: Variant;
  msg: string;
  clBankAccountNbr: integer;
  randomAccount: boolean;
begin
  randomAccount := true;
  DM_CLIENT.CL_BANK_ACCOUNT.EnsureDSCondition('');
  v := DM_CLIENT.CL_BANK_ACCOUNT.Lookup( 'SB_BANKS_NBR;CUSTOM_BANK_ACCOUNT_NUMBER;BANK_ACCOUNT_TYPE',
        VarArrayOf([params.Values['SB_BANKS_NBR'], params.Values['CUSTOM_BANK_ACCOUNT_NUMBER'], params.Values['BANK_ACCOUNT_TYPE']]), 'CL_BANK_ACCOUNT_NBR');
  if VarIsNull( v ) and (DM_CLIENT.CL_BANK_ACCOUNT.RecordCount > 0) then
  begin
    randomAccount := true;
    v := DM_CLIENT.CL_BANK_ACCOUNT['CL_BANK_ACCOUNT_NBR'];
  end;
  if not VarIsNull( v ) then
  begin
    clBankAccountNbr := v;
    DM_CLIENT.CL_DELIVERY_GROUP.EnsureDSCondition('');
    v := DM_CLIENT.CL_DELIVERY_GROUP.Lookup( 'DELIVERY_GROUP_DESCRIPTION', params.Values['DELIVERY_GROUP_DESCRIPTION'], 'CL_DELIVERY_GROUP_NBR' );
    if details.Locate('SB_AGENCY_NBR', VarArrayOf([selecteddetails.FieldByNAme('SB_AGENCY_NBR').AsString]), []) then
      details.Edit
    else
    begin
      if randomAccount then
      begin
        msg := Format( 'The Client Bank Account for the Agency ''%s'' has been automatically assigned'+#13+#10+
        ' because account %s doesn''t exist in client %s.'+ #13+#10+
                       'Please verify that it is correct.',
               [ VarToStr(selectedDetails['Agency_Name']),
                 params.Values['CUSTOM_BANK_ACCOUNT_NUMBER'],
                 (Sender as TClientDetailCopier).TargetClientNumber ] );
        EvMessage( msg, mtWarning, [mbOk]);
      end;
      details.Insert;
    end;
    for k := 0 to SelectedDetails.Fields.Count - 1 do
    begin
      fn := Selecteddetails.Fields[k].FieldName;
      if (Sender as TBasicCopier).IsFieldToCopy( fn, details )
         and ( (copy(fn,Length(fn)-3,4) <> '_NBR') or (copy(fn,1,3) <> 'CL_') ) then
        details[fn] := selectedDetails[fn];
    end;
    if details.fieldbyName('CL_BANK_ACCOUNT_NBR').IsNull then //dont assign random account when editing existing agency
      details['CL_BANK_ACCOUNT_NBR'] := clBankAccountNbr;
    if not VarIsNull(v)  then
      details['CL_DELIVERY_GROUP_NBR'] := v;
    details.Post;
  end
  else
  begin
    msg := Format( 'Cannot copy %s to client %s because no CL Bank Account exist',
           [ VarToStr(selectedDetails['Agency_Name']),
             (Sender as TClientDetailCopier).TargetClientNumber ] );
    EvErrMessage(msg);
  end;
  Result := Details.FieldByName(Details.TableName + '_NBR').AsInteger;
end;
{
+        CL_BANK_ACCOUNT_NBR INTEGER NOT NULL,   --(SB_BANKS_NBR, CUSTOM_BANK_ACCOUNT_NUMBER, BANK_ACCOUNT_TYPE)
        CL_DELIVERY_GROUP_NBR INTEGER,
}


function TEDIT_CL_AGENCY.HandleDelete(Sender: TObject; client: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList): integer;
begin
  if details.Locate('SB_AGENCY_NBR', VarArrayOf([selecteddetails.FieldByNAme('SB_AGENCY_NBR').AsString]), []) then
  begin
    Result := Details.FieldByName(Details.TableName + '_NBR').AsInteger;
    details.Delete;
  end
  else
    Result := -1;
end;

//end gdy

procedure TEDIT_CL_AGENCY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_AGENCY, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_AGENCY_REPORTS, SupportDataSets, '');
end;

procedure TEDIT_CL_AGENCY.PageControl1Change(Sender: TObject);
begin
  inherited;
  ResultFIELDS_CL_BANK_Function;
end;
procedure TEDIT_CL_AGENCY.ResultFIELDS_CL_BANK_Function;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_CL_BANK_') <> stEnabled then
   Begin
    (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
    (Owner as TFramePackageTmpl).btnNavDelete.Enabled := false;
    wwlcBankAccount.Enabled := False;
   end;
End;

initialization
  RegisterClass(TEDIT_CL_AGENCY);


end.
