// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_CO_CONSOLIDATION;
interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, wwdblook, Wwdotdot,
  Wwdbcomb, StdCtrls, Mask, wwdbedit, ExtCtrls, Db, Wwdatsrc, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, ComCtrls, DBCtrls, Controls, Classes,
  ISBasicClasses, SDDClasses, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  SysUtils, EvUtils, EvTypes, EvConsts, SDataDictclient, SDataDicttemp,
  EvContext, EvUIComponents, EvClientDataSet, isUIwwDBLookupCombo,
  isUIwwDBComboBox, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, SFieldCodeValues, SPackageEntry;

type
  TEDIT_CL_CO_CONSOLIDATION = class(TEDIT_CL_BASE)
    tshtCompany_Consolidation_Detail: TTabSheet;
    bev_CL_CO1: TEvBevel;
    wwDBGrid1: TevDBGrid;
    DM_COMPANY: TDM_COMPANY;
    CoSrc: TevDataSource;
    evConsolidationList: TevDataSource;
    evDBGrid1: TevDBGrid;
    cdConsolidationList: TevClientDataSet;
    cdConsolidationListCL_CO_CONSOLIDATION_NBR: TIntegerField;
    cdConsolidationListCUSTOM_COMPANY_NUMBER: TStringField;
    cdConsolidationListNAME: TStringField;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    pnlCLBrowseRight: TevPanel;
    fpCLRight: TisUIFashionPanel;
    pnlFPCLRightBody: TevPanel;
    fpCLConsolidationList: TisUIFashionPanel;
    fpCLConsolidationCompanies: TisUIFashionPanel;
    fpCLConsolidationDetails: TisUIFashionPanel;
    lablConsolidation_Type: TevLabel;
    wwcbConsolidation_Type: TevDBComboBox;
    evLabel1: TevLabel;
    wwcbConsolidation_Scope: TevDBComboBox;
    lablDescription: TevLabel;
    dedtDescription: TevDBEdit;
    Label1: TevLabel;
    lcPrimaryCompany: TevDBLookupCombo;
    wwdgBrowse: TevDBGrid;
    cdConsolidationListACA_CL_CO_CONSOLIDATION_NBR: TIntegerField;
    procedure SetReportingOptions(const Consolidation_Scope, sDetailField : string);
    procedure SetType(const Consolidation_Scope, sDetailField : string);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwcbConsolidation_ScopeEnter(Sender: TObject);
    procedure wwcbConsolidation_TypeCloseUp(Sender: TwwDBComboBox;
      Select: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure wwcbConsolidation_TypeChange(Sender: TObject);
    function getDetailFieldName: string;
    procedure wwcbConsolidation_TypeDropDown(Sender: TObject);
  private
    FTypeList: string;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function GetInsertControl: TWinControl; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_CO_CONSOLIDATION.AfterDataSetsReopen;
begin
  inherited;
  cdConsolidationList.Close;
  with TExecDSWrapper.Create('select ACA_CL_CO_CONSOLIDATION_NBR, CL_CO_CONSOLIDATION_NBR, CUSTOM_COMPANY_NUMBER, NAME from CO WHERE {AsOfNow<CO>}') do
    ctx_DataAccess.GetCustomData(cdConsolidationList, AsVariant);
  cdConsolidationList.Open;
end;

procedure TEDIT_CL_CO_CONSOLIDATION.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO, aDS);
  inherited;
end;

function TEDIT_CL_CO_CONSOLIDATION.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_CO_CONSOLIDATION;
end;

function TEDIT_CL_CO_CONSOLIDATION.GetInsertControl: TWinControl;
begin
  Result := wwcbCONSOLIDATION_TYPE;
end;

procedure TEDIT_CL_CO_CONSOLIDATION.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if (Kind = NavCancel)then
      SetReportingOptions(Co_Consolidation_Scope_ComboChoices, getDetailFieldName);
end;

function TEDIT_CL_CO_CONSOLIDATION.getDetailFieldName: string;
begin
  if wwdsDetail.DataSet.FieldByName('CONSOLIDATION_TYPE').AsString = CONSOLIDATION_TYPE_ACA then
     Result:= 'ACA_CL_CO_CONSOLIDATION_NBR'
  else
     Result:= 'CL_CO_CONSOLIDATION_NBR';
end;

procedure TEDIT_CL_CO_CONSOLIDATION.SetType(const Consolidation_Scope, sDetailField : string);
var
 i : integer;
 s : String;
begin
    s := Consolidation_Scope;
    wwcbConsolidation_Scope.Items.Text := s;
    wwcbConsolidation_Scope.ApplyList;
    wwcbConsolidation_Scope.ItemIndex := -1;

    if assigned(wwcbConsolidation_Scope.Field) and (not wwcbConsolidation_Scope.Field.IsNull) then
    begin
      for i:=0 to wwcbConsolidation_Scope.Items.Count - 1 do
      begin
         s := wwcbConsolidation_Scope.Items[i];
         if s[Length(s)] = wwcbConsolidation_Scope.Field.AsString[1] then
         begin
            wwcbConsolidation_Scope.ItemIndex := i;
            break;
         end;
      end;
      if (wwcbConsolidation_Scope.ItemIndex = -1) and
         (wwcbConsolidation_Scope.datasource.State in [dsEdit,dsInsert]) then
         begin
            s := wwcbConsolidation_Scope.Items[0];
           wwcbConsolidation_Scope.Field.Value:= s[Length(s)];
           wwcbConsolidation_Scope.ItemIndex := 0;
         end;
    end;
    if cdConsolidationList.Active then
    begin
      if DM_CLIENT.CL_CO_CONSOLIDATION.CL_CO_CONSOLIDATION_NBR.AsInteger > 0 then
        cdConsolidationList.Filter:= sDetailField + '=' + DM_CLIENT.CL_CO_CONSOLIDATION.CL_CO_CONSOLIDATION_NBR.AsString
      else
        cdConsolidationList.Filter:= sDetailField + '= -1';
      cdConsolidationList.Filtered:= True;
    end;

end;

procedure TEDIT_CL_CO_CONSOLIDATION.SetReportingOptions(const Consolidation_Scope, sDetailField : string);
var
 i : integer;
 s : String;
begin
    s := Consolidation_Scope;
    wwcbConsolidation_Scope.Items.Text := s;
    wwcbConsolidation_Scope.ApplyList;
    wwcbConsolidation_Scope.ItemIndex := -1;

    if assigned(wwcbConsolidation_Scope.Field) and (not wwcbConsolidation_Scope.Field.IsNull) then
    begin
      for i:=0 to wwcbConsolidation_Scope.Items.Count - 1 do
      begin
         s := wwcbConsolidation_Scope.Items[i];
         if s[Length(s)] = wwcbConsolidation_Scope.Field.AsString[1] then
         begin
            wwcbConsolidation_Scope.ItemIndex := i;
            break;
         end;
      end;
      if (wwcbConsolidation_Scope.ItemIndex = -1) and
         (wwcbConsolidation_Scope.datasource.State in [dsEdit,dsInsert]) then
         begin
            s := wwcbConsolidation_Scope.Items[0];
           wwcbConsolidation_Scope.Field.Value:= s[Length(s)];
           wwcbConsolidation_Scope.ItemIndex := 0;
         end;
    end;
    if cdConsolidationList.Active then
    begin
      if DM_CLIENT.CL_CO_CONSOLIDATION.CL_CO_CONSOLIDATION_NBR.AsInteger > 0 then
        cdConsolidationList.Filter:= sDetailField + '=' + DM_CLIENT.CL_CO_CONSOLIDATION.CL_CO_CONSOLIDATION_NBR.AsString
      else
        cdConsolidationList.Filter:= sDetailField + '= -1';
      cdConsolidationList.Filtered:= True;
    end;

end;

procedure TEDIT_CL_CO_CONSOLIDATION.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if wwdsDetail.DataSet.State = dsBrowse then
     SetReportingOptions(Co_Consolidation_Scope_ComboChoices, getDetailFieldName);
end;

procedure TEDIT_CL_CO_CONSOLIDATION.wwcbConsolidation_ScopeEnter(
  Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.FieldByName('CONSOLIDATION_TYPE').AsString[1] = CONSOLIDATION_TYPE_ACA then
    SetReportingOptions(Co_Consolidation_Scope_ComboChoices2, 'ACA_CL_CO_CONSOLIDATION_NBR')
  else
       SetReportingOptions(Co_Consolidation_Scope_ComboChoices1, 'CL_CO_CONSOLIDATION_NBR');
end;

procedure TEDIT_CL_CO_CONSOLIDATION.wwcbConsolidation_TypeCloseUp(
  Sender: TwwDBComboBox; Select: Boolean);
begin
  inherited;
  Sender.Items.CommaText := FTypeList;
  Sender.ApplyList;

  if wwcbConsolidation_Type.Value = CONSOLIDATION_TYPE_ACA then
       SetReportingOptions(Co_Consolidation_Scope_ComboChoices2, 'ACA_CL_CO_CONSOLIDATION_NBR')
  else
       SetReportingOptions(Co_Consolidation_Scope_ComboChoices1, 'CL_CO_CONSOLIDATION_NBR');
end;

procedure TEDIT_CL_CO_CONSOLIDATION.PageControl1Change(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.FieldByName('CONSOLIDATION_TYPE').AsString = CONSOLIDATION_TYPE_ACA then
       SetReportingOptions(Co_Consolidation_Scope_ComboChoices2, 'ACA_CL_CO_CONSOLIDATION_NBR')
  else
       SetReportingOptions(Co_Consolidation_Scope_ComboChoices1, 'CL_CO_CONSOLIDATION_NBR');

end;

procedure TEDIT_CL_CO_CONSOLIDATION.wwcbConsolidation_TypeChange(
  Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then exit;
  if wwdsDetail.DataSet.Active and (wwdsDetail.DataSet.State <> dsInsert) then
  begin
   if wwdsDetail.DataSet.FieldByName('CONSOLIDATION_TYPE').AsString[1] = CONSOLIDATION_TYPE_ACA then
      SetReportingOptions(Co_Consolidation_Scope_ComboChoices2, 'ACA_CL_CO_CONSOLIDATION_NBR')
   else
      SetReportingOptions(Co_Consolidation_Scope_ComboChoices1, 'CL_CO_CONSOLIDATION_NBR');
  end;
end;

procedure TEDIT_CL_CO_CONSOLIDATION.wwcbConsolidation_TypeDropDown(
  Sender: TObject);
var
 i: integer;
begin
  inherited;
  FTypeList := TwwDBComboBox(Sender).Items.CommaText;

  if wwdsDetail.DataSet.State <> dsInsert then
  begin
    if wwdsDetail.DataSet.FieldByName('CONSOLIDATION_TYPE').AsString[1] = CONSOLIDATION_TYPE_ACA then
      with TwwDBComboBox(Sender).Items do
      for i := 2 downto 0 do
       Delete(i)
    else
      with TwwDBComboBox(Sender).Items do
        Delete(3);
  end;
end;

initialization
  RegisterClass(TEDIT_CL_CO_CONSOLIDATION);

end.
