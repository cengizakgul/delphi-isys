inherited EDIT_CL_UNION_DUES: TEDIT_CL_UNION_DUES
  Width = 654
  inherited Panel1: TevPanel
    Width = 654
    inherited DBText1: TevDBText
      Left = 128
      Width = 225
    end
    object lablTEAM_DESCRIPTION: TevLabel
      Left = 368
      Top = 8
      Width = 28
      Height = 13
      Caption = 'Union'
    end
    object dtxtCL_UNION_NBR: TevDBText
      Left = 408
      Top = 8
      Width = 225
      Height = 13
      DataField = 'NAME'
      DataSource = wwdsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited PageControl1: TevPageControl
    Width = 654
    HelpContext = 28002
    inherited TabSheet1: TTabSheet
      inherited Panel2: TevPanel
        Width = 646
        inherited pnlSubbrowse: TevPanel
          Width = 321
          object wwdgTeam: TevDBGrid
            Left = 0
            Top = 0
            Width = 321
            Height = 173
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F')
            IniAttributes.Delimiter = ';;'
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsSubMaster
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            IndicatorColor = icBlack
          end
        end
      end
      inherited Panel3: TevPanel
        Width = 646
      end
    end
    object tshtUnionDues: TTabSheet
      Caption = 'Details'
      object bev_cl_ed_udues: TEvBevel
        Left = 0
        Top = 0
        Width = 646
        Height = 216
        Align = alClient
      end
      object lablCL_Union_Dues_Nbr: TevLabel
        Left = 264
        Top = 272
        Width = 95
        Height = 13
        Caption = 'Client Union Dues #'
        FocusControl = dedtCL_Union_Dues_Nbr
        Visible = False
      end
      object lablMinimum_Rate: TevLabel
        Left = 16
        Top = 224
        Width = 67
        Height = 13
        Caption = 'Minimum Rate'
        FocusControl = dedtMinimum_Rate
      end
      object lablMaximum_Rate: TevLabel
        Left = 16
        Top = 272
        Width = 70
        Height = 13
        Caption = 'Maximum Rate'
        FocusControl = dedtMaximum_Rate
      end
      object lablDue_Amount: TevLabel
        Left = 16
        Top = 320
        Width = 59
        Height = 13
        Caption = 'Due Amount'
        FocusControl = dedtDue_Amount
      end
      object lablUnion: TevLabel
        Left = 264
        Top = 224
        Width = 34
        Height = 13
        Caption = '~Union'
        FocusControl = dedtCL_Union_Dues_Nbr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lablFuture_Due_Amount: TevLabel
        Left = 136
        Top = 320
        Width = 92
        Height = 13
        Caption = 'Future Due Amount'
        FocusControl = dedtFuture_Due_Amount
      end
      object lablFuture_Begin_Date: TevLabel
        Left = 264
        Top = 320
        Width = 86
        Height = 13
        Caption = 'Future Begin Date'
      end
      object lablFuture_Minimum_Rate: TevLabel
        Left = 136
        Top = 224
        Width = 100
        Height = 13
        Caption = 'Future Minimum Rate'
        FocusControl = dedtFuture_Minimum_Rate
      end
      object lablFuture_Maximum_Rate: TevLabel
        Left = 136
        Top = 272
        Width = 103
        Height = 13
        Caption = 'Future Maximum Rate'
        FocusControl = dedtFuture_Maximum_Rate
      end
      object dedtCL_Union_Dues_Nbr: TevDBEdit
        Left = 264
        Top = 288
        Width = 64
        Height = 21
        DataField = 'CL_UNION_DUES_NBR'
        TabOrder = 8
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object dedtUnion: TevDBEdit
        Left = 264
        Top = 240
        Width = 64
        Height = 21
        DataField = 'CL_UNION_NBR'
        TabOrder = 7
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object dedtMinimum_Rate: TevDBEdit
        Left = 16
        Top = 240
        Width = 64
        Height = 21
        DataField = 'MINIMUM_RATE'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtMaximum_Rate: TevDBEdit
        Left = 16
        Top = 288
        Width = 64
        Height = 21
        DataField = 'MAXIMUM_RATE'
        DataSource = wwdsDetail
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtDue_Amount: TevDBEdit
        Left = 16
        Top = 336
        Width = 64
        Height = 21
        DataField = 'DUE_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtFuture_Due_Amount: TevDBEdit
        Left = 136
        Top = 336
        Width = 64
        Height = 21
        DataField = 'NEXT_DUE_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtFuture_Minimum_Rate: TevDBEdit
        Left = 136
        Top = 240
        Width = 64
        Height = 21
        DataField = 'NEXT_MINIMUM_RATE'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtFuture_Maximum_Rate: TevDBEdit
        Left = 136
        Top = 288
        Width = 64
        Height = 21
        DataField = 'NEXT_MAXIMUM_RATE'
        DataSource = wwdsDetail
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdgTeamMembers: TevDBGrid
        Left = 0
        Top = 0
        Width = 329
        Height = 217
        Selected.Strings = (
          'MINIMUM_RATE'#9'10'#9'Minimum Rate'
          'MAXIMUM_RATE'#9'10'#9'Maximum Rate'
          'DUE_AMOUNT'#9'10'#9'Due Amount'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
      object wwdpFuture_Begin_Date: TevDBDateTimePicker
        Left = 264
        Top = 336
        Width = 81
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'NEXT_BEGIN_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 9
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_UNION_DUES.CL_UNION_DUES
    MasterDataSource = wwdsSubMaster
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Top = 26
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 374
    Top = 26
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_CL_UNION.CL_UNION
    Left = 174
    Top = 51
  end
end
