// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_3RDPARTY_SICK_ADMIN;
{**************************************************************************}
{* unit: SPD_EDIT_CL_3RDPARTY_SICK_ADMIN.pas                              *}
{* Description: Form used to maintain Client 3rd Party Sick Pay Admin.    *}
{*              information from the Client -> 3rd Party Sick Pay Admin.  *}
{*              comboboxes.                                               *}
{*                                                                        *}
{* Copyright(c) PayData Payroll Services, Inc.                            *}
{*                                                                        *}
{* Author:  Tim Stevenson                                                 *}
{* Written: 04/25/1998                                                    *}
{* Updated: 04/29/1998                                                    *}
{*                                                                        *}
{**************************************************************************}

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, wwdblook, StdCtrls,
  ExtCtrls, DBCtrls, Mask, wwdbedit, Db, Wwdatsrc, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, Controls, Classes, EvUIComponents, EvUtils, EvClientDataSet,
  SDataDictbureau, isUIwwDBLookupCombo, ISBasicClasses, isUIwwDBEdit,
  SDDClasses, SDataDictclient, ImgList, SDataDicttemp, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_3RDPARTY_SICK_ADMIN = class(TEDIT_CL_BASE)
    tshtThirdpary_Sick_Pay_Admin_Details: TTabSheet;
    wwdgBrowse: TevDBGrid;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    pnlCLBrowseRight: TevPanel;
    fpCLRight: TisUIFashionPanel;
    pnlFPCLRightBody: TevPanel;
    fp3PartyDetail: TisUIFashionPanel;
    lablAgency_Name: TevLabel;
    wwlcAgency_Name: TevDBLookupCombo;
    lablDescription: TevLabel;
    dedtDescription: TevDBEdit;
    lablContact: TevLabel;
    dedtContact: TevDBEdit;
    lablPhone: TevLabel;
    wwdePhone: TevDBEdit;
    dedtPhone_Description: TevDBEdit;
    lablPhone_Description: TevLabel;
    lablFax_Description: TevLabel;
    dedtFax_Description: TevDBEdit;
    wwdeFax: TevDBEdit;
    lablFax: TevLabel;
    lablE_Mail_Address: TevLabel;
    dedtE_Mail_Address: TevDBEdit;
    fp3PartyDetail2: TisUIFashionPanel;
    drgpShort_Term: TevDBRadioGroup;
    drgpLong_Term: TevDBRadioGroup;
    drgpNon_Tax_In_Payroll: TevDBRadioGroup;
    drgpState_Tax_In_Payroll: TevDBRadioGroup;
    drgpLocal_Tax_In_Payroll: TevDBRadioGroup;
    drgpW2: TevDBRadioGroup;
    lablNon_Taxable_Percentage: TevLabel;
    wwdeNon_Taxable_Percentage: TevDBEdit;
    bvlTaxDetails1: TEvBevel;
    evPanel1: TevPanel;
    ScrollBox1: TScrollBox;
    evPanel3: TevPanel;
    evPanel2: TevPanel;
    wwDBGrid1x: TevDBGrid;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

function TEDIT_CL_3RDPARTY_SICK_ADMIN.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_3_PARTY_SICK_PAY_ADMIN;
end;

function TEDIT_CL_3RDPARTY_SICK_ADMIN.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

procedure TEDIT_CL_3RDPARTY_SICK_ADMIN.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_AGENCY, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_CL_3RDPARTY_SICK_ADMIN);

end.
