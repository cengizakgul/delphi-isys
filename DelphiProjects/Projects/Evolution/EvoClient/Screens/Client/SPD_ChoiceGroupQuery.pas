// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ChoiceGroupQuery;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,SPD_ChoiceFromListQuery,
  Db, Wwdatsrc,  Grids, Wwdbigrd, Wwdbgrid, StdCtrls, Buttons,
  ExtCtrls, SDataStructure;

type
  TChoiceGroupQuery = class(TChoiceFromListQuery)
    DM_CLIENT: TDM_CLIENT;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

end.
