// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_DELIVERY_METHOD;

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, wwdblook, StdCtrls, Mask,
  wwdbedit, ExtCtrls, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, DBCtrls, Controls, Classes, EvUIComponents, SDataDictbureau,
  ISBasicClasses, SDDClasses, SDataDictclient, SDataDicttemp, EvUtils, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_DELIVERY_METHOD = class(TEDIT_CL_BASE)
    tshtDelivery_Method_Details: TTabSheet;
    bev_cl_del1: TEvBevel;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    pnlCLBrowseRight: TevPanel;
    fpCLRight: TisUIFashionPanel;
    pnlFPCLRightBody: TevPanel;
    fpCLDeliveryMethodDetail: TisUIFashionPanel;
    lablDelivery_Company: TevLabel;
    wwlcDelivery_Company: TevDBLookupCombo;
    evBitBtn2: TevBitBtn;
    lablName: TevLabel;
    dedtName: TevDBEdit;
    lablAddress1: TevLabel;
    dedtAddress1: TevDBEdit;
    lablAddress2: TevLabel;
    dedtAddress2: TevDBEdit;
    lablCity: TevLabel;
    dedtCity: TevDBEdit;
    dedtSTATE: TevDBEdit;
    lablState: TevLabel;
    wwdeZip_Code: TevDBEdit;
    lablZip_Code: TevLabel;
    wwlcCover_Sheet_SB_Report_Nbr: TevDBLookupCombo;
    lablCover_Sheet_Report: TevLabel;
    wwdePhone_Number: TevDBEdit;
    lablPhone_Number: TevLabel;
    dedtAttention: TevDBEdit;
    lablAttention: TevLabel;
    wwDBGrid1: TevDBGrid;
    procedure evBitBtn2Click(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

{***************************************}
{* TEDIT_CL_DELIVERY_METHOD.FormCreate *}
{***************************************}

function TEDIT_CL_DELIVERY_METHOD.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_DELIVERY_METHOD;
end;

function TEDIT_CL_DELIVERY_METHOD.GetInsertControl: TWinControl;
begin
  Result := wwlcDelivery_Company;
end;

procedure TEDIT_CL_DELIVERY_METHOD.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_DELIVERY_COMPANY_SVCS, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_REPORTS, SupportDataSets, '');
end;

procedure TEDIT_CL_DELIVERY_METHOD.evBitBtn2Click(Sender: TObject);
const
  fieldsToTransfer: array [0..5] of string = (
   'NAME',
   'ADDRESS1',
   'ADDRESS2',
   'CITY',
   'STATE',
   'ZIP_CODE'
  );
var
  i: integer;
begin
  if not (TevClientDataSet(DM_CLIENT.CL_DELIVERY_METHOD).State in [dsEdit,dsInsert]) then
    DM_CLIENT.CL_DELIVERY_METHOD.Edit;
  for i := low(fieldsToTransfer) to high(fieldsToTransfer) do
    DM_CLIENT.CL_DELIVERY_METHOD[fieldsToTransfer[i]] := DM_CLIENT.CL[fieldsToTransfer[i]];
end;

initialization
  RegisterClass(TEDIT_CL_DELIVERY_METHOD);

end.
