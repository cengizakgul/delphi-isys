// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ChoiceCompanyQuery;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_ChoiceFromListQuery, Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Buttons, ExtCtrls, SDataStructure;

type
  TChoiceCompanyQuery = class(TChoiceFromListQuery)
    DM_COMPANY: TDM_COMPANY;
    DM_TEMPORARY: TDM_TEMPORARY;
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

{$R *.DFM}

end.
