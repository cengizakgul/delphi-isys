inherited EDIT_CL_CO_CONSOLIDATION: TEDIT_CL_CO_CONSOLIDATION
  Width = 625
  Height = 497
  inherited Panel1: TevPanel
    Width = 625
    inherited pnlFavoriteReport: TevPanel
      Left = 510
    end
    inherited pnlTopRight: TevPanel
      Width = 510
      inherited DBText1: TevDBText
        Left = 128
      end
    end
  end
  inherited PageControl1: TevPageControl
    Width = 625
    Height = 443
    HelpContext = 24501
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 617
        Height = 414
        inherited pnlBorder: TevPanel
          Width = 613
          Height = 410
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 613
          Height = 410
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 3
              Width = 797
              object sbCLBrowse: TScrollBox
                Left = 1
                Top = 1
                Width = 795
                Height = 563
                Align = alClient
                TabOrder = 0
                object pnlCLBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 791
                  Height = 559
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 547
                  end
                  object pnlCLBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 547
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCLLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Client'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCLBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 476
                    Height = 547
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCLRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 464
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Consolidated Reports'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLRightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 371
              Top = 59
              Width = 330
              Height = 514
            end
          end
          inherited Panel3: TevPanel
            Width = 565
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 565
            Height = 303
            inherited Splitter1: TevSplitter
              Height = 299
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 299
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 219
                IniAttributes.SectionName = 'TEDIT_CL_CO_CONSOLIDATION\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 209
              Height = 299
              Title = 'Consolidated Reports'
              object wwdgBrowse: TevDBGrid
                Left = 18
                Top = 48
                Width = 159
                Height = 219
                DisableThemesInTitle = False
                Selected.Strings = (
                  'DESCRIPTION'#9'34'#9'Description'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_CO_CONSOLIDATION\wwdgBrowse'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtCompany_Consolidation_Detail: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object bev_CL_CO1: TEvBevel
        Left = 0
        Top = 0
        Width = 617
        Height = 414
        Align = alClient
      end
      object fpCLConsolidationList: TisUIFashionPanel
        Left = 227
        Top = 8
        Width = 244
        Height = 209
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Consolidation List'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object wwDBGrid1: TevDBGrid
          Left = 12
          Top = 35
          Width = 211
          Height = 153
          DisableThemesInTitle = False
          Selected.Strings = (
            'CONSOLIDATION_TYPE'#9'1'#9'Type'#9'F'
            'DESCRIPTION'#9'24'#9'Description'#9'F')
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CL_CO_CONSOLIDATION\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsDetail
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
      object fpCLConsolidationCompanies: TisUIFashionPanel
        Left = 8
        Top = 225
        Width = 462
        Height = 255
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Companies that belong to this Consolidation'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object evDBGrid1: TevDBGrid
          Left = 12
          Top = 35
          Width = 429
          Height = 199
          DisableThemesInTitle = False
          Selected.Strings = (
            'CUSTOM_COMPANY_NUMBER'#9'22'#9'Company Number'#9'F'
            'NAME'#9'42'#9'Company Name'#9'F'#9)
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CL_CO_CONSOLIDATION\evDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evConsolidationList
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
      object fpCLConsolidationDetails: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 210
        Height = 209
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 2
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Consolidation Details'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object lablConsolidation_Type: TevLabel
          Left = 12
          Top = 35
          Width = 33
          Height = 16
          Caption = '~Type'
          FocusControl = wwcbConsolidation_Type
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel1: TevLabel
          Left = 12
          Top = 74
          Width = 94
          Height = 16
          Caption = '~Reporting Options'
          FocusControl = wwcbConsolidation_Scope
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablDescription: TevLabel
          Left = 12
          Top = 113
          Width = 62
          Height = 16
          Caption = '~Description'
          FocusControl = dedtDescription
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TevLabel
          Left = 12
          Top = 152
          Width = 78
          Height = 16
          Caption = '~Main company'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object wwcbConsolidation_Type: TevDBComboBox
          Left = 12
          Top = 50
          Width = 177
          Height = 21
          HelpContext = 24501
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          AutoSize = False
          DataField = 'CONSOLIDATION_TYPE'
          DataSource = wwdsDetail
          DropDownCount = 8
          ItemHeight = 0
          Items.Strings = (
            'All Reports'#9'A'
            'State Only'#9'S'
            'Fed Only'#9'F')
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 0
          UnboundDataType = wwDefault
          OnChange = wwcbConsolidation_TypeChange
          OnDropDown = wwcbConsolidation_TypeDropDown
          OnCloseUp = wwcbConsolidation_TypeCloseUp
        end
        object wwcbConsolidation_Scope: TevDBComboBox
          Left = 12
          Top = 89
          Width = 177
          Height = 21
          HelpContext = 24501
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          AutoSize = False
          DataField = 'SCOPE'
          DataSource = wwdsDetail
          DropDownCount = 8
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 1
          UnboundDataType = wwDefault
          OnEnter = wwcbConsolidation_ScopeEnter
        end
        object dedtDescription: TevDBEdit
          Left = 12
          Top = 128
          Width = 177
          Height = 21
          HelpContext = 24501
          DataField = 'DESCRIPTION'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object lcPrimaryCompany: TevDBLookupCombo
          Left = 12
          Top = 167
          Width = 177
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'20'#9'NAME'#9'F')
          DataField = 'PRIMARY_CO_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CO.CO
          LookupField = 'CO_NBR'
          Style = csDropDownList
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_CO_CONSOLIDATION.CL_CO_CONSOLIDATION
    OnDataChange = wwdsDetailDataChange
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 414
    Top = 18
  end
  object CoSrc: TevDataSource
    DataSet = DM_CO.CO
    Left = 243
    Top = 45
  end
  object evConsolidationList: TevDataSource
    DataSet = cdConsolidationList
    Left = 232
    Top = 96
  end
  object cdConsolidationList: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CL_CO_CONSOLIDATION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end>
    DetailFields = 'CL_CO_CONSOLIDATION_NBR'
    Left = 216
    Top = 152
    object cdConsolidationListCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Company Number'
      DisplayWidth = 22
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdConsolidationListNAME: TStringField
      DisplayLabel = 'Company Name'
      DisplayWidth = 42
      FieldName = 'NAME'
      Size = 40
    end
    object cdConsolidationListCL_CO_CONSOLIDATION_NBR: TIntegerField
      FieldName = 'CL_CO_CONSOLIDATION_NBR'
      Visible = False
    end
    object cdConsolidationListACA_CL_CO_CONSOLIDATION_NBR: TIntegerField
      FieldName = 'ACA_CL_CO_CONSOLIDATION_NBR'
    end
  end
end
