inherited EDIT_CL_PENSION: TEDIT_CL_PENSION
  inherited Panel1: TevPanel
    inherited pnlTopRight: TevPanel
      inherited DBText1: TevDBText
        Left = 112
      end
    end
  end
  inherited PageControl1: TevPageControl
    HelpContext = 9512
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 241
                IniAttributes.SectionName = 'TEDIT_CL_PENSION\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Pension'
              inline CopyHelper: TClientCopyHelper
                Left = 24
                Top = 112
                Width = 115
                Height = 54
                TabOrder = 0
                Visible = False
              end
              object wwDBGrid1x: TevDBGrid
                Left = 18
                Top = 48
                Width = 366
                Height = 241
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_PENSION\wwDBGrid1x'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtPension_Details: TTabSheet
      HelpContext = 9502
      Caption = 'Details'
      ImageIndex = 2
      object bev_cl_ed_pen1: TEvBevel
        Left = 0
        Top = 0
        Width = 427
        Height = 183
        Align = alClient
      end
      object lablPlan_ID: TevLabel
        Left = 16
        Top = 112
        Width = 35
        Height = 13
        Caption = 'Plan ID'
        FocusControl = dedtPlan_ID
      end
      object lablPolicy_ID: TevLabel
        Left = 16
        Top = 160
        Width = 42
        Height = 13
        Caption = 'Policy ID'
        FocusControl = dedtPolicy_ID
      end
      object lablName: TevLabel
        Left = 16
        Top = 16
        Width = 33
        Height = 13
        Caption = '~Name'
        FocusControl = dedtName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablType: TevLabel
        Left = 16
        Top = 64
        Width = 29
        Height = 13
        Caption = '~Type'
        FocusControl = wwcbType
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablClient_Agency: TevLabel
        Left = 16
        Top = 208
        Width = 65
        Height = 13
        Caption = 'Client Agency'
        FocusControl = wwlcClient_Agency
      end
      object lablMax_Allowed_Percentage: TevLabel
        Left = 16
        Top = 256
        Width = 112
        Height = 13
        Caption = 'Maximum EE % Allowed'
        FocusControl = wwdeMax_Allowed_Percentage
      end
      object lablProbation_Period_Days: TevLabel
        Left = 16
        Top = 304
        Width = 116
        Height = 13
        Caption = 'Probation Period in Days'
        FocusControl = wwseProbation_Period_Days
      end
      object lablMinimum_Age: TevLabel
        Left = 160
        Top = 304
        Width = 63
        Height = 13
        Caption = 'Minimum Age'
        FocusControl = wwseMinimum_Age
      end
      object lablE_D_Group: TevLabel
        Left = 296
        Top = 368
        Width = 52
        Height = 13
        Caption = 'E/D Group'
        FocusControl = wwlcE_D_Group
        Visible = False
      end
      object dedtPlan_ID: TevDBEdit
        Left = 16
        Top = 128
        Width = 244
        Height = 21
        DataField = 'PLAN_ID'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtPolicy_ID: TevDBEdit
        Left = 16
        Top = 176
        Width = 244
        Height = 21
        HelpContext = 9503
        DataField = 'POLICY_ID'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtName: TevDBEdit
        Left = 16
        Top = 32
        Width = 244
        Height = 21
        HelpContext = 9504
        DataField = 'NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeMax_Allowed_Percentage: TevDBEdit
        Left = 16
        Top = 272
        Width = 65
        Height = 21
        HelpContext = 9506
        DataField = 'MAX_ALLOWED_PERCENTAGE'
        DataSource = wwdsDetail
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwseProbation_Period_Days: TevDBEdit
        Left = 16
        Top = 320
        Width = 65
        Height = 21
        HelpContext = 9507
        DataField = 'PROBATION_PERIOD_DAYS'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwseMinimum_Age: TevDBEdit
        Left = 160
        Top = 320
        Width = 65
        Height = 21
        HelpContext = 9508
        DataField = 'MINIMUM_AGE'
        DataSource = wwdsDetail
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwcbType: TevDBComboBox
        Left = 16
        Top = 80
        Width = 129
        Height = 21
        HelpContext = 9505
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'PENSION_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          '401K'#9'1'
          '403B'#9'3'
          '457'#9'7'
          'Simple'#9'I'
          'SEP'#9'S'
          'Pension Plan'#9'P'
          'Defferred Comp.'#9'D'
          'State Pension'#9'T'
          'MA Retirement'#9'M')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object drgpDiscontinue_Match_EE_Reaches_Limit: TevDBRadioGroup
        Left = 304
        Top = 16
        Width = 297
        Height = 57
        HelpContext = 9509
        Caption = '~Discontinue Match if there is no EE Contribution'
        Columns = 3
        DataField = 'STOP_WHEN_EE_REACHES_MAX'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 9
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpMark_Pension_W2: TevDBRadioGroup
        Left = 304
        Top = 96
        Width = 297
        Height = 57
        HelpContext = 9510
        Caption = '~Mark Pension Box on W2'
        Columns = 3
        DataField = 'MARK_PENSION_ON_W2'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No'
          'System Rules')
        ParentFont = False
        TabOrder = 10
        Values.Strings = (
          'Y'
          'N'
          'S')
      end
      object wwlcClient_Agency: TevDBLookupCombo
        Left = 16
        Top = 224
        Width = 241
        Height = 21
        HelpContext = 9513
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'Agency Name'#9'F'
          'AgencyAddress'#9'62'#9'AgencyAddress'#9'F')
        DataField = 'CL_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_AGENCY.CL_AGENCY
        LookupField = 'CL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 4
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwlcE_D_Group: TevDBLookupCombo
        Left = 296
        Top = 384
        Width = 297
        Height = 21
        HelpContext = 9511
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME'#9'No')
        DataField = 'CL_E_D_GROUPS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
        LookupField = 'CL_E_D_GROUPS_NBR'
        Style = csDropDownList
        TabOrder = 8
        Visible = False
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_PENSION.CL_PENSION
  end
end
