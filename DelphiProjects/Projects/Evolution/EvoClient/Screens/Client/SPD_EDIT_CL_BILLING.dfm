inherited EDIT_CL_BILLING: TEDIT_CL_BILLING
  Width = 793
  Height = 610
  inherited Panel1: TevPanel
    Width = 793
    inherited pnlFavoriteReport: TevPanel
      Left = 678
    end
    inherited pnlTopRight: TevPanel
      Width = 678
    end
  end
  inherited PageControl1: TevPageControl
    Width = 793
    Height = 556
    HelpContext = 7502
    ActivePage = tshtBilling_Details
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 785
        Height = 527
        inherited pnlBorder: TevPanel
          Width = 781
          Height = 523
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 781
          Height = 523
          inherited Panel3: TevPanel
            Width = 733
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 733
            Height = 416
            inherited Splitter1: TevSplitter
              Left = 348
              Height = 412
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 348
              Height = 412
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 298
                Height = 332
                IniAttributes.SectionName = 'TEDIT_CL_BILLING\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 351
              Width = 378
              Height = 412
              Title = 'Billing'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 328
                Height = 332
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_BILLING\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtBilling_Details: TTabSheet
      HelpContext = 7501
      Caption = 'Details'
      ImageIndex = 2
      object bev_CL_bill1: TEvBevel
        Left = 0
        Top = 0
        Width = 785
        Height = 527
        Align = alClient
      end
      object lablName: TevLabel
        Left = 16
        Top = 50
        Width = 37
        Height = 16
        Caption = '~Name'
        FocusControl = dedtName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAddress1: TevLabel
        Left = 16
        Top = 91
        Width = 47
        Height = 13
        Caption = 'Address 1'
        FocusControl = dedtAddress1
      end
      object lablAddress2: TevLabel
        Left = 16
        Top = 131
        Width = 47
        Height = 13
        Caption = 'Address 2'
        FocusControl = dedtAddress2
      end
      object lablCity: TevLabel
        Left = 16
        Top = 170
        Width = 17
        Height = 13
        Caption = 'City'
        FocusControl = dedtCity
      end
      object lablState: TevLabel
        Left = 152
        Top = 170
        Width = 25
        Height = 13
        Caption = 'State'
        FocusControl = dedtState
      end
      object lablZip_Code: TevLabel
        Left = 192
        Top = 170
        Width = 43
        Height = 13
        Caption = 'Zip Code'
        FocusControl = wwdeZip_Code
      end
      object dedtName: TevDBEdit
        Left = 16
        Top = 66
        Width = 249
        Height = 21
        HelpContext = 7502
        DataField = 'NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtAddress1: TevDBEdit
        Left = 16
        Top = 107
        Width = 249
        Height = 21
        HelpContext = 7502
        DataField = 'ADDRESS1'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtAddress2: TevDBEdit
        Left = 16
        Top = 147
        Width = 249
        Height = 21
        HelpContext = 7502
        DataField = 'ADDRESS2'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtCity: TevDBEdit
        Left = 16
        Top = 186
        Width = 129
        Height = 21
        HelpContext = 7502
        DataField = 'CITY'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeZip_Code: TevDBEdit
        Left = 192
        Top = 186
        Width = 73
        Height = 21
        HelpContext = 7502
        DataField = 'ZIP_CODE'
        DataSource = wwdsDetail
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwcbInvoice_Separately: TevDBRadioGroup
        Left = 350
        Top = 218
        Width = 249
        Height = 74
        HelpContext = 7502
        Caption = '~Invoice'
        DataField = 'INVOICE_SEPARATELY'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Separately'
          'No Invoice'
          'Invoice with Payroll')
        ParentFont = False
        TabOrder = 8
        Values.Strings = (
          'Y'
          'N'
          'P')
      end
      object grbxNotes: TevGroupBox
        Left = 14
        Top = 218
        Width = 329
        Height = 97
        Caption = 'Notes'
        TabOrder = 7
        DesignSize = (
          329
          97)
        object dmemBILLING_NOTES: TEvDBMemo
          Left = 8
          Top = 16
          Width = 313
          Height = 73
          HelpContext = 7502
          Anchors = [akLeft, akTop, akRight]
          DataField = 'BILLING_NOTES'
          DataSource = wwdsDetail
          TabOrder = 0
        end
      end
      object dedtState: TevDBEdit
        Left = 152
        Top = 186
        Width = 33
        Height = 21
        HelpContext = 7502
        CharCase = ecUpperCase
        DataField = 'STATE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&,@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object grbxContact_Info: TevGroupBox
        Left = 288
        Top = 2
        Width = 313
        Height = 209
        Caption = 'Contact Info'
        TabOrder = 6
        object lablContact: TevLabel
          Left = 16
          Top = 16
          Width = 37
          Height = 16
          Caption = '~Name'
          FocusControl = dedtContact
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablPhone: TevLabel
          Left = 16
          Top = 64
          Width = 31
          Height = 13
          Caption = 'Phone'
          FocusControl = wwdePhone
        end
        object lablPhone_Description: TevLabel
          Left = 152
          Top = 64
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = dedtPhone_Description
        end
        object lablFax: TevLabel
          Left = 16
          Top = 112
          Width = 17
          Height = 13
          Caption = 'Fax'
          FocusControl = wwdeFax
        end
        object lablFax_Description: TevLabel
          Left = 152
          Top = 112
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = dedtFax_Description
        end
        object lablE_Mail_Address: TevLabel
          Left = 16
          Top = 160
          Width = 28
          Height = 13
          Caption = 'E-mail'
          FocusControl = dedtE_Mail_Address
        end
        object dedtContact: TevDBEdit
          Left = 16
          Top = 32
          Width = 281
          Height = 21
          HelpContext = 7502
          DataField = 'CONTACT'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePhone: TevDBEdit
          Left = 16
          Top = 80
          Width = 121
          Height = 21
          HelpContext = 7502
          DataField = 'PHONE'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtPhone_Description: TevDBEdit
          Left = 152
          Top = 80
          Width = 145
          Height = 21
          HelpContext = 7202
          DataField = 'DESCRIPTION'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeFax: TevDBEdit
          Left = 16
          Top = 128
          Width = 121
          Height = 21
          HelpContext = 7502
          DataField = 'FAX'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtFax_Description: TevDBEdit
          Left = 152
          Top = 128
          Width = 145
          Height = 21
          HelpContext = 7502
          DataField = 'FAX_DESCRIPTION'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtE_Mail_Address: TevDBEdit
          Left = 16
          Top = 176
          Width = 281
          Height = 21
          HelpContext = 7502
          DataField = 'E_MAIL_ADDRESS'
          DataSource = wwdsDetail
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object evBitBtn2: TevBitBtn
        Left = 16
        Top = 11
        Width = 249
        Height = 33
        Caption = 'Get Client Name and Address '
        TabOrder = 9
        OnClick = evBitBtn2Click
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
          8888DFFFFFFFFFFFFFFF0444444444400000888888888888888804444474447F
          FFF0888888D888DDDDD80F44477447FFFFF08D888DD88DDDDDD80FF474447888
          88F08DD8D888D88888D80FFF77787FFFFFF08DDD8DD8DDDDDDD80FF477478788
          88F08DD8DD8D8D8888D80F47777778FFFFF08D8DDDDDD8DDDDD80447777778FF
          88F0888DDDDDD8DD88D804447474747FFFF08888D8D8D8DDDDD804447777747F
          FFF08888DDDDD8DDDDD804444777447FFF0888888DDD88DDDD8DD44444444000
          008DD8888888888888DDD44444448DDDDDDDD8888888DDDDDDDDDD444448DDDD
          DDDDDD88888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_BILLING.CL_BILLING
  end
end
