// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_FUNDS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CL_BASE, Db, Wwdatsrc, ComCtrls, StdCtrls, Mask, DBCtrls, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, Buttons, SDataStructure, 
  wwdbedit, EvUIComponents, EvUtils, EvClientDataSet, isUIwwDBEdit,
  ISBasicClasses, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_FUNDS = class(TEDIT_CL_BASE)
    tshtClient_Pension_Funds: TTabSheet;
    wwDBGrid1: TevDBGrid;
    wwDBGrid2: TevDBGrid;
    wwdsSubMaster: TevDataSource;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    fpCLPensionFundSummary: TisUIFashionPanel;
    fpCLPensionFundDetail: TisUIFashionPanel;
    lablName: TevLabel;
    dedtName: TevDBEdit;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_FUNDS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_PENSION, aDS);
  inherited;
end;

function TEDIT_CL_FUNDS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_FUNDS;
end;

function TEDIT_CL_FUNDS.GetInsertControl: TWinControl;
begin
  Result := dedtName;
end;

initialization
  RegisterClass(TEDIT_CL_FUNDS);

end.
