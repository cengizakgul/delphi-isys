// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_ED_GROUPS;

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, StdCtrls, Mask, wwdbedit,
  Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls,
  DBCtrls, Controls, Classes, EvExceptions, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, EvDataAccessComponents, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CL_ED_GROUPS = class(TEDIT_CL_BASE)
    tshtED_Groups_Detail: TTabSheet;
    wwDBGrid1x: TevDBGrid;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    pnlCLBrowseRight: TevPanel;
    fpCLRight: TisUIFashionPanel;
    pnlFPCLRightBody: TevPanel;
    fpCLEDGroupSummary: TisUIFashionPanel;
    fpCLEDGroupDetail: TisUIFashionPanel;
    evDBRadioGroup2: TevDBRadioGroup;
    evBitBtn1: TevBitBtn;
    dedtName: TevDBEdit;
    lablName: TevLabel;
    wwDBGrid1: TevDBGrid;
    procedure evBitBtn1Click(Sender: TObject);
  private
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses
  sPackageEntry, evUtils, evTypes;
{$R *.DFM}

//ALD_CL_E_D_GROUPS_NBR
{*********************************}
{* TEDIT_CL_ED_GROUPS.FormCreate *}
{*********************************}

function TEDIT_CL_ED_GROUPS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_E_D_GROUPS;
end;

function TEDIT_CL_ED_GROUPS.GetInsertControl: TWinControl;
begin
  Result := dedtNAME;
end;

procedure TEDIT_CL_ED_GROUPS.evBitBtn1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CL_ED_GROUP_CODES')
end;

function GetEdGroupUseCount( edGroupNbr: integer ): integer;
begin
  Result := 0;
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    if Active then Close;
    with TExecDSWrapper.Create('GenericSelectCurrentNBR') do
    begin
      SetMacro('Columns', 'count(ALD_CL_E_D_GROUPS_NBR) EdGroupCount');
      SetMacro('TableName', 'EE');
      SetMacro('NbrField', 'ALD_CL_E_D_GROUPS');
      SetParam('RecordNbr', edGroupNbr);
      DataRequest(AsVariant);
    end;
    Open;
    if RecordCount > 0 then
      result := FieldByName('EdGroupCount').AsInteger;
  end;
end;


procedure TEDIT_CL_ED_GROUPS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  if Kind = NavDelete then
  begin
    if GetEdGroupUseCount( DM_CLIENT.CL_E_D_GROUPS.FieldByName('CL_E_D_GROUPS_NBR').AsInteger ) > 0 then
    begin
      DM_CLIENT.CL_E_D_GROUPS.Cancel;
      raise EUpdateError.CreateHelp('Cannot delete an E/D Group that is used in auto labor distribution!', IDH_ConsistencyViolation);
    end;
  end;
  inherited;
end;

initialization
  RegisterClass(TEDIT_CL_ED_GROUPS);

end.
