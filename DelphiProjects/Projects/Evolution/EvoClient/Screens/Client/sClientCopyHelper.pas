// Copyright � 2000-2004 iSystems LLC. All rights reserved.
//gdy
unit sClientCopyHelper;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  sCopyHelper, Menus,  ActnList, ImgList, sCopier,
  ISBasicClasses, sDDClasses, EvUIComponents;

type
  TClientCopyHelper = class(TCopyHelper)
  private
    FRelationsList: TRelationsList;
    FOnDeleteDetail: TProcessClientDetailEvent;
    FOnCopyDetail: TProcessClientDetailEvent;
    FOnBeforeCopy: TBeforeCopyEvent;
  protected
    function CreateCopier: TCopier; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property OnCopyDetail: TProcessClientDetailEvent read FOnCopyDetail write FOnCopyDetail;
    property OnDeleteDetail: TProcessClientDetailEvent read FOnDeleteDetail write FOnDeleteDetail;
    property OnBeforeCopy: TBeforeCopyEvent read FOnBeforeCopy write FOnBeforeCopy;
    function AddRelation(Table: TddTableClass; OnCopy: TProcessRelationEvent): integer;
  end;

implementation
{$R *.DFM}

constructor TClientCopyHelper.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FRelationsList := TRelationsList.Create;
end;

destructor TClientCopyHelper.Destroy;
begin
  FreeAndNil(FRelationsList);
  inherited Destroy;
end;

function TClientCopyHelper.CreateCopier: TCopier;
begin
  Result := inherited CreateCopier;
  if not assigned( Result ) then
  begin
    Result := TClientDetailCopier.Create( DataSet, Grid.SelectedList, UserFriendlyName );
    TClientDetailCopier(Result).RelationsList.OwnsObjects := False;
    TClientDetailCopier(Result).OnDeleteDetail :=  FOnDeleteDetail;
    TClientDetailCopier(Result).OnCopyDetail := FOnCopyDetail;
    TClientDetailCopier(Result).OnBeforeCopy := FOnBeforeCopy;
    TClientDetailCopier(Result).AddTablesToSave( FDSToSave );
    TClientDetailCopier(Result).RelationsList.Assign(FRelationsList);
  end;
end;

function TClientCopyHelper.AddRelation(Table: TddTableClass;
  OnCopy: TProcessRelationEvent): integer;
begin
  Result := FRelationsList.Add(Table, OnCopy);
end;

end.
