inherited EDIT_CL_3RDPARTY_SICK_ADMIN: TEDIT_CL_3RDPARTY_SICK_ADMIN
  Width = 648
  Height = 560
  inherited Panel1: TevPanel
    Width = 648
    inherited pnlFavoriteReport: TevPanel
      Left = 533
    end
    inherited pnlTopRight: TevPanel
      Width = 533
      inherited DBText1: TevDBText
        Left = 112
      end
    end
  end
  inherited PageControl1: TevPageControl
    Width = 648
    Height = 506
    HelpContext = 27005
    ActivePage = tshtThirdpary_Sick_Pay_Admin_Details
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 640
        Height = 477
        inherited pnlBorder: TevPanel
          Width = 636
          Height = 473
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 636
          Height = 473
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 3
              Width = 797
              BevelOuter = bvNone
              object sbCLBrowse: TScrollBox
                Left = 0
                Top = 0
                Width = 797
                Height = 565
                Align = alClient
                TabOrder = 0
                object pnlCLBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 793
                  Height = 561
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 549
                  end
                  object pnlCLBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 549
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCLLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Client'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCLBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 478
                    Height = 549
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCLRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 466
                      Height = 537
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Third Party Sick Administration'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLRightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 371
              Top = 99
              Width = 321
              Height = 457
              object wwdgBrowse: TevDBGrid
                Left = 0
                Top = 0
                Width = 321
                Height = 457
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_3RDPARTY_SICK_ADMIN\wwdgBrowse'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
          inherited Panel3: TevPanel
            Width = 588
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 588
            Height = 366
            inherited Splitter1: TevSplitter
              Left = 0
              Height = 362
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 3
              Height = 362
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 282
                IniAttributes.SectionName = 'TEDIT_CL_3RDPARTY_SICK_ADMIN\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 232
              Height = 362
              Title = 'Third Party Sick Administration'
              object wwDBGrid1x: TevDBGrid
                Left = 18
                Top = 48
                Width = 182
                Height = 282
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_3RDPARTY_SICK_ADMIN\wwDBGrid1x'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtThirdpary_Sick_Pay_Admin_Details: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object evPanel1: TevPanel
        Left = 0
        Top = 0
        Width = 640
        Height = 477
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 0
          Width = 640
          Height = 477
          Align = alClient
          TabOrder = 0
          object evPanel3: TevPanel
            Left = 0
            Top = 0
            Width = 636
            Height = 473
            Align = alClient
            BevelOuter = bvNone
            BevelWidth = 2
            BorderWidth = 8
            TabOrder = 0
            object fp3PartyDetail: TisUIFashionPanel
              Left = 8
              Top = 8
              Width = 283
              Height = 326
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 0
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Third Party Sick Administration Details'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object lablAgency_Name: TevLabel
                Left = 12
                Top = 35
                Width = 76
                Height = 16
                Caption = '~Agency Name'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object lablDescription: TevLabel
                Left = 12
                Top = 74
                Width = 62
                Height = 16
                Caption = '~Description'
                FocusControl = dedtDescription
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object lablContact: TevLabel
                Left = 12
                Top = 152
                Width = 37
                Height = 13
                Caption = 'Contact'
                FocusControl = dedtContact
              end
              object lablPhone: TevLabel
                Left = 12
                Top = 191
                Width = 31
                Height = 13
                Caption = 'Phone'
              end
              object lablPhone_Description: TevLabel
                Left = 141
                Top = 191
                Width = 53
                Height = 13
                Caption = 'Description'
              end
              object lablFax_Description: TevLabel
                Left = 141
                Top = 230
                Width = 53
                Height = 13
                Caption = 'Description'
              end
              object lablFax: TevLabel
                Left = 12
                Top = 230
                Width = 17
                Height = 13
                Caption = 'Fax'
              end
              object lablE_Mail_Address: TevLabel
                Left = 12
                Top = 269
                Width = 29
                Height = 13
                Caption = 'E-Mail'
                FocusControl = dedtE_Mail_Address
              end
              object bvlTaxDetails1: TEvBevel
                Left = 12
                Top = 130
                Width = 250
                Height = 3
                Shape = bsTopLine
              end
              object wwlcAgency_Name: TevDBLookupCombo
                Left = 12
                Top = 50
                Width = 250
                Height = 21
                DropDownAlignment = taLeftJustify
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F'
                  'ADDRESS1'#9'30'#9'Address1'#9'F'
                  'CITY'#9'20'#9'City'#9'F'
                  'STATE'#9'2'#9'State'#9'F'
                  'ZIP_CODE'#9'10'#9'Zip'#9'F')
                DataField = 'SB_AGENCY_NBR'
                DataSource = wwdsDetail
                LookupTable = DM_SB_AGENCY.SB_AGENCY
                LookupField = 'SB_AGENCY_NBR'
                Style = csDropDownList
                TabOrder = 0
                AutoDropDown = True
                ShowButton = True
                PreciseEditRegion = False
                AllowClearKey = False
              end
              object dedtDescription: TevDBEdit
                Left = 12
                Top = 89
                Width = 250
                Height = 21
                DataField = 'NAME'
                DataSource = wwdsDetail
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 1
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object dedtContact: TevDBEdit
                Left = 12
                Top = 167
                Width = 250
                Height = 21
                DataField = 'CONTACT'
                DataSource = wwdsDetail
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 2
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object wwdePhone: TevDBEdit
                Left = 12
                Top = 206
                Width = 121
                Height = 21
                DataField = 'PHONE'
                DataSource = wwdsDetail
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
                TabOrder = 3
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object dedtPhone_Description: TevDBEdit
                Left = 141
                Top = 206
                Width = 121
                Height = 21
                DataField = 'DESCRIPTION'
                DataSource = wwdsDetail
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 4
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object dedtFax_Description: TevDBEdit
                Left = 141
                Top = 245
                Width = 121
                Height = 21
                DataField = 'FAX_DESCRIPTION'
                DataSource = wwdsDetail
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
                TabOrder = 6
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object wwdeFax: TevDBEdit
                Left = 12
                Top = 245
                Width = 121
                Height = 21
                DataField = 'FAX'
                DataSource = wwdsDetail
                Picture.PictureMaskFromDataSet = False
                Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
                TabOrder = 5
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
              object dedtE_Mail_Address: TevDBEdit
                Left = 12
                Top = 284
                Width = 250
                Height = 21
                DataField = 'E_MAIL_ADDRESS'
                DataSource = wwdsDetail
                TabOrder = 7
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
            end
            object fp3PartyDetail2: TisUIFashionPanel
              Left = 299
              Top = 8
              Width = 242
              Height = 326
              BevelOuter = bvNone
              BorderWidth = 12
              Color = 14737632
              TabOrder = 1
              RoundRect = True
              ShadowDepth = 8
              ShadowSpace = 8
              ShowShadow = True
              ShadowColor = clSilver
              TitleColor = clGrayText
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWhite
              TitleFont.Height = -13
              TitleFont.Name = 'Arial'
              TitleFont.Style = [fsBold]
              Title = 'Settings'
              LineWidth = 0
              LineColor = clWhite
              Theme = ttCustom
              object lablNon_Taxable_Percentage: TevLabel
                Left = 12
                Top = 152
                Width = 72
                Height = 13
                Caption = 'Non-Taxable %'
              end
              object drgpShort_Term: TevDBRadioGroup
                Left = 12
                Top = 35
                Width = 209
                Height = 36
                Caption = '~Short Term'
                Columns = 2
                DataField = 'SHORT_TERM'
                DataSource = wwdsDetail
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'Yes'
                  'No')
                ParentFont = False
                TabOrder = 0
                Values.Strings = (
                  'Y'
                  'N')
              end
              object drgpLong_Term: TevDBRadioGroup
                Left = 12
                Top = 74
                Width = 209
                Height = 36
                Caption = '~Long Term'
                Columns = 2
                DataField = 'LONG_TERM'
                DataSource = wwdsDetail
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'Yes'
                  'No')
                ParentFont = False
                TabOrder = 1
                Values.Strings = (
                  'Y'
                  'N')
              end
              object drgpNon_Tax_In_Payroll: TevDBRadioGroup
                Left = 12
                Top = 113
                Width = 209
                Height = 36
                Caption = '~Non-Taxable'
                Columns = 2
                DataField = 'NON_TAX_IN_PAYROLL'
                DataSource = wwdsDetail
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'Yes'
                  'No')
                ParentFont = False
                TabOrder = 2
                Values.Strings = (
                  'Y'
                  'N')
              end
              object drgpState_Tax_In_Payroll: TevDBRadioGroup
                Left = 12
                Top = 191
                Width = 209
                Height = 36
                Caption = '~Deduct State Tax'
                Columns = 2
                DataField = 'STATE_TAX_IN_PAYROLL'
                DataSource = wwdsDetail
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'Yes'
                  'No')
                ParentFont = False
                TabOrder = 4
                Values.Strings = (
                  'Y'
                  'N')
              end
              object drgpLocal_Tax_In_Payroll: TevDBRadioGroup
                Left = 12
                Top = 230
                Width = 209
                Height = 36
                Caption = '~Deduct Local Tax'
                Columns = 2
                DataField = 'LOCAL_TAX_IN_PAYROLL'
                DataSource = wwdsDetail
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'Yes'
                  'No')
                ParentFont = False
                TabOrder = 5
                Values.Strings = (
                  'Y'
                  'N')
              end
              object drgpW2: TevDBRadioGroup
                Left = 12
                Top = 269
                Width = 209
                Height = 36
                Caption = '~W2'
                Columns = 2
                DataField = 'W2'
                DataSource = wwdsDetail
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  'Yes'
                  'No')
                ParentFont = False
                TabOrder = 6
                Values.Strings = (
                  'Y'
                  'N')
              end
              object wwdeNon_Taxable_Percentage: TevDBEdit
                Left = 12
                Top = 167
                Width = 209
                Height = 21
                DataField = 'NON_TAXABLE_PERCENTAGE'
                DataSource = wwdsDetail
                TabOrder = 3
                UnboundDataType = wwDefault
                WantReturns = False
                WordWrap = False
                Glowing = False
              end
            end
          end
          object evPanel2: TevPanel
            Left = 0
            Top = 473
            Width = 636
            Height = 0
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_3_PARTY_SICK_PAY_ADMIN.CL_3_PARTY_SICK_PAY_ADMIN
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 411
    Top = 18
  end
end
