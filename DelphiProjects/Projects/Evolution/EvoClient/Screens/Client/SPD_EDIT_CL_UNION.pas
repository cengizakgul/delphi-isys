// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_UNION;
{***************************************************************************}
{* unit: SPD_EDIT_CL_UNION.pas                                             *}
{* Description: Form used to maintain Client Union information             *}
{*              from the Client -> Unions comboboxes.                      *}
{*                                                                         *}
{* Copyright(c) PayData Payroll Services, Inc.                             *}
{*                                                                         *}
{* Author:  Tim Stevenson                                                  *}
{* Written: 05/08/1998                                                     *}
{* Updated: 05/08/1998                                                     *}
{*                                                                         *}
{***************************************************************************}

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, Wwdotdot, Wwdbcomb,
  StdCtrls, ExtCtrls, DBCtrls, wwdblook, Mask, wwdbedit, Db, Wwdatsrc,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Controls, Classes, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBComboBox, ISBasicClasses, isUIwwDBLookupCombo, isUIwwDBEdit,
  SDDClasses, SDataDictclient, ImgList, SDataDicttemp, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_UNION = class(TEDIT_CL_BASE)
    tshtUnion: TTabSheet;
    lablClient_Union: TevLabel;
    wwdgBrowse: TevDBGrid;
    pnlUnion: TisUIFashionPanel;
    lablName: TevLabel;
    dedtName: TevDBEdit;
    lablAddress1: TevLabel;
    dedtAddress1: TevDBEdit;
    lablAddress2: TevLabel;
    dedtAddress2: TevDBEdit;
    lablCity: TevLabel;
    dedtCity: TevDBEdit;
    lablState: TevLabel;
    wwdeState: TevDBEdit;
    lablZip_Code: TevLabel;
    wwdeZip_Code: TevDBEdit;
    lablPhone: TevLabel;
    wwdePhone: TevDBEdit;
    lablContact: TevLabel;
    dedtContact: TevDBEdit;
    bvlTop: TBevel;
    pnlListOfCLBankAccount: TisUIFashionPanel;
    wwdgTeamMembers: TevDBGrid;
    bevCL_UNION: TEvBevel;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_UNION.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  inherited;
end;

function TEDIT_CL_UNION.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_UNION;
end;

function TEDIT_CL_UNION.GetInsertControl: TWinControl;
begin
  Result := dedtName;
end;

initialization
  RegisterClass(TEDIT_CL_UNION);

end.
