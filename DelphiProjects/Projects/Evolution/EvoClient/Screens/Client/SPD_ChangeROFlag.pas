// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ChangeROFlag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc,  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid,
  ExtCtrls, ISBasicClasses, DBCtrls, EvUIComponents;

type
  TChangeROFlag = class(TForm)
    evPanel1: TevPanel;
    btnYes: TevBitBtn;
    btnNo: TevBitBtn;
    evRgReadOnly: TevRadioGroup;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

{ TChangeROFlag }


end.
