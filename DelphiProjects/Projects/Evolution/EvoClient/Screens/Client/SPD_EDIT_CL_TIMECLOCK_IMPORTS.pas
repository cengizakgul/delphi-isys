// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_TIMECLOCK_IMPORTS;
{***************************************************************************}
{* unit: SPD_EDIT_CL_TIMECLOCK_IMPORTS.pas                                 *}
{* Description: Form used to maintain Client Timeclock Imports information *}
{*              from the Client -> Timeclock Imports comboboxes.           *}
{*                                                                         *}
{* Copyright(c) PayData Payroll Services, Inc.                             *}
{*                                                                         *}
{* Author:  Tim Stevenson                                                  *}
{* Written: 04/25/1998                                                     *}
{* Updated: 04/28/1998                                                     *}
{*                                                                         *}
{***************************************************************************}

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, Dialogs, StdCtrls,
  DBCtrls, ExtCtrls, Wwdotdot, Wwdbcomb, wwdbdatetimepicker, Mask,
  wwdbedit, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Controls, Classes, SysUtils, EvUIComponents, EvClientDataSet,
  ISBasicClasses, isUIDBMemo, isUIwwDBComboBox, isUIwwDBDateTimePicker,
  isUIwwDBEdit, SDDClasses, SDataDictclient, ImgList, SDataDicttemp,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel, Forms,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_TIMECLOCK_IMPORTS = class(TEDIT_CL_BASE)
    tshtTimeclock_Import_Details: TTabSheet;
    lablDescription: TevLabel;
    lablTimeclock_Type: TevLabel;
    lablHardware_Type: TevLabel;
    lablDate_Lease_Ends: TevLabel;
    lablPayroll_Import_Format: TevLabel;
    lablPayroll_Export_Format: TevLabel;
    dedtDescription: TevDBEdit;
    grbxInstallation_Information: TevGroupBox;
    lablScheduled_Install_Date: TevLabel;
    lablActual_Install_Date: TevLabel;
    lablInstaller: TevLabel;
    dedtInstaller: TevDBEdit;
    wwcbTimeclock_Type: TevDBComboBox;
    wwcbHardware_Type: TevDBComboBox;
    drgpConnection_Type: TevDBRadioGroup;
    drgpSB_Supported: TevDBRadioGroup;
    wwcbPayroll_Import_Format: TevDBComboBox;
    wwcbPayroll_Export_Format: TevDBComboBox;
    grbxNotes: TevGroupBox;
    dmemNotes: TEvDBMemo;
    wwDBGrid1: TevDBGrid;
    bev_cl_ed_tc1a: TEvBevel;
    bev_cl_ed_tc1b: TEvBevel;
    wwdpDate_Lease_Ends: TevDBDateTimePicker;
    wwdpScheduled_Install_Date: TevDBDateTimePicker;
    wwdpActual_Install_Date: TevDBDateTimePicker;
    Label4: TevLabel;
    wwdeExport_Directory: TevDBEdit;
    butnOpenARImportDirectory: TevButton;
    dlgOpen: TOpenDialog;
    wwdgBrowse: TevDBGrid;
    procedure butnOpenARImportDirectoryClick(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_TIMECLOCK_IMPORTS.butnOpenARImportDirectoryClick(
  Sender: TObject);
begin
  inherited;
  dlgOpen.Title := 'Select Timeclock Export Directory';
  dlgOpen.FileName := wwdeExport_Directory.Text;
  if dlgOpen.Execute then
    with wwdeExport_Directory.DataSource.DataSet do
    begin
      if not (State in [dsEdit, dsInsert]) then
        edit;
      FieldByName(wwdeExport_Directory.DataField).AsString := ExtractFilePath(DlgOpen.FileName);
    end;

end;

function TEDIT_CL_TIMECLOCK_IMPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_TIMECLOCK_IMPORTS;
end;

function TEDIT_CL_TIMECLOCK_IMPORTS.GetInsertControl: TWinControl;
begin
  Result := dedtDESCRIPTION;
end;

initialization
  RegisterClass(TEDIT_CL_TIMECLOCK_IMPORTS);

end.
