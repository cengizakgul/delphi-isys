// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_UNION_DUES;

interface

uses
  SDataStructure,  wwdbedit, SPD_EDIT_CL_BASE,
  wwdbdatetimepicker, StdCtrls, Mask, ExtCtrls, Db, Wwdatsrc, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, ComCtrls, DBCtrls, Controls, Classes, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_CL_UNION_DUES = class(TEDIT_CL_BASE)
    tshtUnionDues: TTabSheet;
    lablCL_Union_Dues_Nbr: TevLabel;
    dedtCL_Union_Dues_Nbr: TevDBEdit;
    dedtUnion: TevDBEdit;
    lablMinimum_Rate: TevLabel;
    dedtMinimum_Rate: TevDBEdit;
    lablMaximum_Rate: TevLabel;
    dedtMaximum_Rate: TevDBEdit;
    lablDue_Amount: TevLabel;
    dedtDue_Amount: TevDBEdit;
    lablUnion: TevLabel;
    lablFuture_Due_Amount: TevLabel;
    dedtFuture_Due_Amount: TevDBEdit;
    lablFuture_Begin_Date: TevLabel;
    lablFuture_Minimum_Rate: TevLabel;
    dedtFuture_Minimum_Rate: TevDBEdit;
    lablFuture_Maximum_Rate: TevLabel;
    dedtFuture_Maximum_Rate: TevDBEdit;
    lablTEAM_DESCRIPTION: TevLabel;
    dtxtCL_UNION_NBR: TevDBText;
    wwdgTeam: TevDBGrid;
    wwdgTeamMembers: TevDBGrid;
    bev_cl_ed_udues: TEvBevel;
    wwdpFuture_Begin_Date: TevDBDateTimePicker;
    wwdsSubMaster: TevDataSource;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_UNION_DUES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_UNION, aDS);
  inherited;
end;

function TEDIT_CL_UNION_DUES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_UNION_DUES;
end;

function TEDIT_CL_UNION_DUES.GetInsertControl: TWinControl;
begin
  Result := dedtMinimum_Rate;
end;

procedure TEDIT_CL_UNION_DUES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_CLIENT.CL_UNION, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_CL_UNION_DUES);

end.
