inherited EDIT_CL_COMMON_PAYMASTER: TEDIT_CL_COMMON_PAYMASTER
  inherited PageControl1: TevPageControl
    HelpContext = 24001
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 3
              Width = 797
              object sbCLBrowse: TScrollBox
                Left = 1
                Top = 1
                Width = 795
                Height = 563
                Align = alClient
                TabOrder = 0
                object pnlCLBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 791
                  Height = 559
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 547
                  end
                  object pnlCLBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 547
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCLLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Client'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCLBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 476
                    Height = 547
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCLRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 464
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Consolidated Taxes'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLRightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 347
              Width = 407
              Height = 458
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 0
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 3
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 249
                IniAttributes.SectionName = 'TEDIT_CL_COMMON_PAYMASTER\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Consolidated Taxes'
              object wwdgBrowse: TevDBGrid
                Left = 18
                Top = 48
                Width = 302
                Height = 249
                DisableThemesInTitle = False
                Selected.Strings = (
                  'DESCRIPTION'#9'34'#9'Description'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_CO_CONSOLIDATION\wwdgBrowse'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                Visible = False
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object wwdgCommon_Paymaster: TevDBGrid
                Left = 18
                Top = 48
                Width = 302
                Height = 249
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CPM_NAME'#9'40'#9'Name')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_COMMON_PAYMASTER\wwdgCommon_Paymaster'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtCommon_Paymaster_Details: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object bev_CL_CO2: TEvBevel
        Left = 0
        Top = 0
        Width = 427
        Height = 183
        Align = alClient
      end
      object fpCLConsolidatedGrid: TisUIFashionPanel
        Left = 293
        Top = 8
        Width = 436
        Height = 248
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Companies that belong to this Common Paymaster'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object wwDBGrid1: TevDBGrid
          Left = 12
          Top = 35
          Width = 403
          Height = 194
          DisableThemesInTitle = False
          Selected.Strings = (
            'CUSTOM_COMPANY_NUMBER'#9'20'#9'Company Number'#9'F'
            'NAME'#9'40'#9'Company Name'#9'F')
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CL_COMMON_PAYMASTER\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsDetail
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
      object fpCLConsolidatedDetails: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 277
        Height = 248
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Consolidated Tax Details'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object lablCPM_Name: TevLabel
          Left = 12
          Top = 35
          Width = 65
          Height = 13
          Caption = '~Description'
          FocusControl = dedtCPM_Name
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dedtCPM_Name: TevDBEdit
          Left = 12
          Top = 50
          Width = 244
          Height = 21
          HelpContext = 24001
          DataField = 'CPM_NAME'
          DataSource = wwdsSubMaster
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object drgpCombine_OASDI: TevDBRadioGroup
          Left = 12
          Top = 74
          Width = 244
          Height = 36
          HelpContext = 24001
          Caption = '~Combine OASDI'
          Columns = 2
          DataField = 'COMBINE_OASDI'
          DataSource = wwdsSubMaster
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 1
          Values.Strings = (
            'Y'
            'N')
        end
        object drgpCombine_Medicare: TevDBRadioGroup
          Left = 12
          Top = 113
          Width = 244
          Height = 36
          HelpContext = 24001
          Caption = '~Combine Medicare'
          Columns = 2
          DataField = 'COMBINE_MEDICARE'
          DataSource = wwdsSubMaster
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 2
          Values.Strings = (
            'Y'
            'N')
        end
        object drgpCombine_FUI: TevDBRadioGroup
          Left = 12
          Top = 152
          Width = 244
          Height = 36
          HelpContext = 24001
          Caption = '~Combine FUI'
          Columns = 2
          DataField = 'COMBINE_FUI'
          DataSource = wwdsSubMaster
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 3
          Values.Strings = (
            'Y'
            'N')
        end
        object drgpCombine_State_SUI: TevDBRadioGroup
          Left = 12
          Top = 191
          Width = 244
          Height = 36
          HelpContext = 24001
          Caption = '~Combine State SUI'
          Columns = 2
          DataField = 'COMBINE_STATE_SUI'
          DataSource = wwdsSubMaster
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 4
          Values.Strings = (
            'Y'
            'N')
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CO.CO
    MasterDataSource = wwdsSubMaster
  end
  object wwdsSubMaster: TevDataSource
    DataSet = DM_CL_COMMON_PAYMASTER.CL_COMMON_PAYMASTER
    Left = 177
    Top = 51
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 408
    Top = 18
  end
end
