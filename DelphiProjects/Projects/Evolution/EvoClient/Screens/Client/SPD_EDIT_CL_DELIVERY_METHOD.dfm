inherited EDIT_CL_DELIVERY_METHOD: TEDIT_CL_DELIVERY_METHOD
  Width = 690
  Height = 539
  inherited Panel1: TevPanel
    Width = 690
    inherited pnlFavoriteReport: TevPanel
      Left = 575
    end
    inherited pnlTopRight: TevPanel
      Width = 575
    end
  end
  inherited PageControl1: TevPageControl
    Width = 690
    Height = 485
    HelpContext = 25503
    ActivePage = tshtDelivery_Method_Details
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 682
        Height = 456
        inherited pnlBorder: TevPanel
          Width = 678
          Height = 452
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 678
          Height = 452
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 3
              Width = 797
              object sbCLBrowse: TScrollBox
                Left = 1
                Top = 1
                Width = 795
                Height = 563
                Align = alClient
                TabOrder = 0
                object pnlCLBrowseBorder: TevPanel
                  Left = 0
                  Top = 0
                  Width = 791
                  Height = 559
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 6
                  TabOrder = 0
                  object splitSkills: TevSplitter
                    Left = 306
                    Top = 6
                    Height = 547
                  end
                  object pnlCLBrowseLeft: TevPanel
                    Left = 6
                    Top = 6
                    Width = 300
                    Height = 547
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 0
                    object fpCLLeft: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 288
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Client'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLLeftBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                  object pnlCLBrowseRight: TevPanel
                    Left = 309
                    Top = 6
                    Width = 476
                    Height = 547
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 6
                    TabOrder = 1
                    object fpCLRight: TisUIFashionPanel
                      Left = 6
                      Top = 6
                      Width = 464
                      Height = 535
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Delivery Methods'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      object pnlFPCLRightBody: TevPanel
                        Left = 12
                        Top = 35
                        Width = 248
                        Height = 400
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                      end
                    end
                  end
                end
              end
            end
            inherited pnlSubbrowse: TevPanel
              Left = 363
              Width = 391
              Height = 500
            end
          end
          inherited Panel3: TevPanel
            Width = 630
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 630
            Height = 345
            inherited Splitter1: TevSplitter
              Left = 0
              Height = 341
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Left = 3
              Height = 341
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 502
                IniAttributes.SectionName = 'TEDIT_CL_DELIVERY_METHOD\wwdbgridSelectClient'
                Align = alNone
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 274
              Height = 341
              Title = 'Delivery Method'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 224
                Height = 261
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Destination Name'#9'No')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_DELIVERY_METHOD\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtDelivery_Method_Details: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object bev_cl_del1: TEvBevel
        Left = 0
        Top = 0
        Width = 682
        Height = 456
        Align = alClient
      end
      object fpCLDeliveryMethodDetail: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 582
        Height = 287
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Delivery Method Details'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object lablDelivery_Company: TevLabel
          Left = 12
          Top = 35
          Width = 86
          Height = 16
          Caption = '~Delivery Service'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablName: TevLabel
          Left = 12
          Top = 113
          Width = 93
          Height = 16
          Caption = '~Destination Name'
          FocusControl = dedtName
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablAddress1: TevLabel
          Left = 12
          Top = 152
          Width = 47
          Height = 13
          Caption = 'Address 1'
          FocusControl = dedtAddress1
        end
        object lablAddress2: TevLabel
          Left = 12
          Top = 191
          Width = 47
          Height = 13
          Caption = 'Address 2'
          FocusControl = dedtAddress2
        end
        object lablCity: TevLabel
          Left = 12
          Top = 230
          Width = 17
          Height = 13
          Caption = 'City'
          FocusControl = dedtCity
        end
        object lablState: TevLabel
          Left = 168
          Top = 230
          Width = 25
          Height = 13
          Caption = 'State'
        end
        object lablZip_Code: TevLabel
          Left = 208
          Top = 230
          Width = 43
          Height = 13
          Caption = 'Zip Code'
        end
        object lablCover_Sheet_Report: TevLabel
          Left = 296
          Top = 152
          Width = 94
          Height = 13
          Caption = 'Cover Sheet Report'
        end
        object lablPhone_Number: TevLabel
          Left = 296
          Top = 113
          Width = 71
          Height = 13
          Caption = 'Phone Number'
        end
        object lablAttention: TevLabel
          Left = 296
          Top = 74
          Width = 51
          Height = 16
          Caption = '~Attention'
          FocusControl = dedtAttention
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object wwlcDelivery_Company: TevDBLookupCombo
          Left = 12
          Top = 50
          Width = 265
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NameAndService'#9'40'#9'NameAndService'#9'F')
          DataField = 'SB_DELIVERY_COMPANY_SVCS_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_SB_DELIVERY_COMPANY_SVCS.SB_DELIVERY_COMPANY_SVCS
          LookupField = 'SB_DELIVERY_COMPANY_SVCS_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object evBitBtn2: TevBitBtn
          Left = 12
          Top = 81
          Width = 265
          Height = 25
          Caption = 'Get Client Name and Address '
          TabOrder = 1
          OnClick = evBitBtn2Click
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
            8888DFFFFFFFFFFFFFFF0444444444400000888888888888888804444474447F
            FFF0888888D888DDDDD80F44477447FFFFF08D888DD88DDDDDD80FF474447888
            88F08DD8D888D88888D80FFF77787FFFFFF08DDD8DD8DDDDDDD80FF477478788
            88F08DD8DD8D8D8888D80F47777778FFFFF08D8DDDDDD8DDDDD80447777778FF
            88F0888DDDDDD8DD88D804447474747FFFF08888D8D8D8DDDDD804447777747F
            FFF08888DDDDD8DDDDD804444777447FFF0888888DDD88DDDD8DD44444444000
            008DD8888888888888DDD44444448DDDDDDDD8888888DDDDDDDDDD444448DDDD
            DDDDDD88888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object dedtName: TevDBEdit
          Left = 12
          Top = 128
          Width = 265
          Height = 21
          DataField = 'NAME'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtAddress1: TevDBEdit
          Left = 12
          Top = 167
          Width = 265
          Height = 21
          DataField = 'ADDRESS1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtAddress2: TevDBEdit
          Left = 12
          Top = 206
          Width = 265
          Height = 21
          DataField = 'ADDRESS2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtCity: TevDBEdit
          Left = 12
          Top = 245
          Width = 145
          Height = 21
          DataField = 'CITY'
          DataSource = wwdsDetail
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtSTATE: TevDBEdit
          Left = 168
          Top = 245
          Width = 33
          Height = 21
          CharCase = ecUpperCase
          DataField = 'STATE'
          DataSource = wwdsDetail
          Picture.PictureMask = '*{&,@}'
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeZip_Code: TevDBEdit
          Left = 208
          Top = 245
          Width = 73
          Height = 21
          DataField = 'ZIP_CODE'
          DataSource = wwdsDetail
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwlcCover_Sheet_SB_Report_Nbr: TevDBLookupCombo
          Left = 296
          Top = 167
          Width = 121
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DESCRIPTION'#9'40'#9'DESCRIPTION')
          DataField = 'COVER_SHEET_SB_REPORT_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_SB_REPORTS.SB_REPORTS
          LookupField = 'SB_REPORTS_NBR'
          Style = csDropDownList
          TabOrder = 8
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
        end
        object wwdePhone_Number: TevDBEdit
          Left = 296
          Top = 128
          Width = 121
          Height = 21
          DataField = 'PHONE_NUMBER'
          DataSource = wwdsDetail
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 9
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtAttention: TevDBEdit
          Left = 296
          Top = 89
          Width = 265
          Height = 21
          DataField = 'ATTENTION'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 10
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_DELIVERY_METHOD.CL_DELIVERY_METHOD
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 411
    Top = 21
  end
end
