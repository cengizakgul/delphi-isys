// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_BILLING;

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, StdCtrls, DBCtrls,
  ExtCtrls, Wwdotdot, Wwdbcomb, Mask, wwdbedit, Db, Wwdatsrc, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Controls, Classes, ISBasicClasses,
  SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvClientDataSet,
  isUIDBMemo, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_BILLING = class(TEDIT_CL_BASE)
    tshtBilling_Details: TTabSheet;
    lablName: TevLabel;
    lablAddress1: TevLabel;
    lablAddress2: TevLabel;
    lablCity: TevLabel;
    dedtName: TevDBEdit;
    dedtAddress1: TevDBEdit;
    dedtAddress2: TevDBEdit;
    dedtCity: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    wwcbInvoice_Separately: TevDBRadioGroup;
    grbxNotes: TevGroupBox;
    dmemBILLING_NOTES: TEvDBMemo;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    dedtState: TevDBEdit;
    grbxContact_Info: TevGroupBox;
    lablContact: TevLabel;
    dedtContact: TevDBEdit;
    lablPhone: TevLabel;
    wwdePhone: TevDBEdit;
    lablPhone_Description: TevLabel;
    dedtPhone_Description: TevDBEdit;
    lablFax: TevLabel;
    wwdeFax: TevDBEdit;
    lablFax_Description: TevLabel;
    dedtFax_Description: TevDBEdit;
    lablE_Mail_Address: TevLabel;
    dedtE_Mail_Address: TevDBEdit;
    wwDBGrid1: TevDBGrid;
    bev_CL_bill1: TEvBevel;
    evBitBtn2: TevBitBtn;
    procedure evBitBtn2Click(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

function TEDIT_CL_BILLING.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_BILLING;
end;

function TEDIT_CL_BILLING.GetInsertControl: TWinControl;
begin
  Result := dedtNAME;
end;

procedure TEDIT_CL_BILLING.evBitBtn2Click(Sender: TObject);
const
  fieldsToTransfer: array [0..5] of string = (
   'NAME',
   'ADDRESS1',
   'ADDRESS2',
   'CITY',
   'STATE',
   'ZIP_CODE'
  );
var
  i: integer;
begin
  if not (TevClientDataSet(DM_CLIENT.CL_BILLING).State in [dsEdit,dsInsert]) then
    DM_CLIENT.CL_BILLING.Edit;
  for i := low(fieldsToTransfer) to high(fieldsToTransfer) do
    DM_CLIENT.CL_BILLING[fieldsToTransfer[i]] := DM_CLIENT.CL[fieldsToTransfer[i]];
end;

initialization
  RegisterClass(TEDIT_CL_BILLING);

end.
