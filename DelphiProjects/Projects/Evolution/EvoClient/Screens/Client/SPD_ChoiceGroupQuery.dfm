inherited ChoiceGroupQuery: TChoiceGroupQuery
  Width = 383
  Height = 339
  Caption = 'Select the E/D Groups you want this E/D added to'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Top = 271
    Width = 375
    inherited btnYes: TevBitBtn
      Left = 202
      Caption = '&Add'
    end
    inherited btnNo: TevBitBtn
      Left = 290
      Kind = bkCancel
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 375
    Height = 271
    Selected.Strings = (
      'NAME'#9'40'#9'Name'#9'F')
  end
  inherited dsChoiceList: TevDataSource
    DataSet = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 72
    Top = 72
  end
end
