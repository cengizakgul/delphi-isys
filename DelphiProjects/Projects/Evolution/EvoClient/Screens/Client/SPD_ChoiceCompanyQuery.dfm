inherited ChoiceCompanyQuery: TChoiceCompanyQuery
  Top = 268
  Width = 452
  Caption = 'Select the companies you want this E/D added to'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited evPanel1: TevPanel
    Width = 444
    inherited btnYes: TevBitBtn
      Left = 275
      Caption = '&Add'
    end
    inherited btnNo: TevBitBtn
      Left = 363
      Kind = bkCancel
    end
  end
  inherited dgChoiceList: TevDBGrid
    Width = 444
    Selected.Strings = (
      'CUSTOM_COMPANY_NUMBER'#9'20'#9'Number'#9'F'
      'NAME'#9'40'#9'Name'#9'F'
      'CO_NBR'#9'10'#9'CO_NBR'#9'F')
  end
  inherited dsChoiceList: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    Left = 176
    Top = 88
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 248
    Top = 56
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 264
    Top = 136
  end
end
