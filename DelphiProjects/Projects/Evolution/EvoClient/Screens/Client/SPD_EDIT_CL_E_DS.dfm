inherited EDIT_CL_E_DS: TEDIT_CL_E_DS
  Width = 820
  Height = 682
  inherited Panel1: TevPanel
    Width = 820
    object Label3: TevLabel [0]
      Left = 408
      Top = 0
      Width = 54
      Height = 13
      Caption = 'E/D Code: '
    end
    object Label4: TevLabel [1]
      Left = 408
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Desc: '
    end
    object DBText3: TevDBText [2]
      Left = 464
      Top = 0
      Width = 145
      Height = 17
      DataField = 'CUSTOM_E_D_CODE_NUMBER'
      DataSource = wwdsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText4: TevDBText [3]
      Left = 464
      Top = 16
      Width = 145
      Height = 17
      DataField = 'DESCRIPTION'
      DataSource = wwdsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited pnlFavoriteReport: TevPanel
      Left = 705
      TabOrder = 2
    end
    inherited pnlTopRight: TevPanel
      Width = 705
      inherited DBText1: TevDBText
        Left = 112
      end
    end
    object evBitBtn1: TevBitBtn
      Left = 581
      Top = 4
      Width = 140
      Height = 25
      Caption = 'Update Scheduled E/D'#39's'
      TabOrder = 0
      OnClick = evBitBtn1Click
      Color = clBlack
      Margin = 0
    end
  end
  inherited PageControl1: TevPageControl
    Width = 820
    Height = 628
    HelpContext = 8054
    ActivePage = tshtMain
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 812
        Height = 599
        inherited pnlBorder: TevPanel
          Width = 808
          Height = 595
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 808
          Height = 595
          inherited Panel3: TevPanel
            Width = 760
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 760
            Height = 488
            inherited Splitter1: TevSplitter
              Height = 484
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 484
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 404
                IniAttributes.SectionName = 'TEDIT_CL_E_DS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 404
              Height = 484
              Title = 'E/Ds'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 354
                Height = 404
                DisableThemesInTitle = False
                Selected.Strings = (
                  'CUSTOM_E_D_CODE_NUMBER'#9'13'#9'E/D Code'#9'F'
                  'DESCRIPTION'#9'40'#9'Description')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_E_DS\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                PopupMenu = evPopupMenu1
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              inline CopyHelper: TClientCopyHelper
                Left = 160
                Top = 88
                Width = 114
                Height = 54
                TabOrder = 1
                Visible = False
              end
            end
          end
        end
      end
    end
    object tshtMain: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object bev_cl_eds1a: TEvBevel
        Left = 0
        Top = 0
        Width = 812
        Height = 599
        Align = alClient
      end
      object lablCustom_E_D_Code_Number: TevLabel
        Left = 16
        Top = 16
        Width = 57
        Height = 16
        Caption = '~E/D Code'
        FocusControl = dedtCustom_E_D_Code_Number
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablE_D_Code_Type: TevLabel
        Left = 16
        Top = 64
        Width = 61
        Height = 16
        Caption = '~Code Type'
        FocusControl = wwcdE_D_Code_Type
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablDescription: TevLabel
        Left = 104
        Top = 64
        Width = 62
        Height = 16
        Caption = '~Description'
        FocusControl = dedtDecription
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablE_D_Group: TevLabel
        Left = 362
        Top = 9
        Width = 52
        Height = 13
        Caption = 'E/D Group'
        FocusControl = wwlcE_D_Group
      end
      object lablOverride_Rate: TevLabel
        Left = 16
        Top = 216
        Width = 66
        Height = 13
        Caption = 'Override Rate'
      end
      object lablOverride_EE_Rate_Number: TevLabel
        Left = 16
        Top = 264
        Width = 106
        Height = 13
        Caption = 'Override Rate Number'
        FocusControl = dedtOverride_Rate_Number
      end
      object lablW2_Box: TevLabel
        Left = 16
        Top = 312
        Width = 81
        Height = 13
        Caption = 'Override W2 Box'
        FocusControl = dedtW2_Box
      end
      object lablPriority_To_Exclude: TevLabel
        Left = 16
        Top = 360
        Width = 87
        Height = 13
        Caption = 'Priority to Exclude '
        FocusControl = dedtPriority_Code_Exlude_From_Check
      end
      object lablRegular_OT_Rate: TevLabel
        Left = 16
        Top = 120
        Width = 119
        Height = 13
        Caption = 'Over Time Rate Multiplier'
      end
      object lablE_D_Group_OT_Calculation: TevLabel
        Left = 362
        Top = 57
        Width = 140
        Height = 13
        Caption = 'E/D Group for OT Calculation'
        FocusControl = wwlcE_D_Group_OT_Calculation
      end
      object lablTip_Min_Wage_Make_Up_Type: TevLabel
        Left = 362
        Top = 105
        Width = 150
        Height = 16
        Caption = '~Tip Min Wage Make Up Type'
        FocusControl = wwlcTip_Min_Wage_Make_Up_Type
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablOffsetting_Deduction_Code: TevLabel
        Left = 144
        Top = 443
        Width = 125
        Height = 13
        Caption = 'Offsetting Deduction Code'
        FocusControl = wwlcOffsetting_Deduction_Code
        Visible = False
      end
      object bev_cl_eds1b: TEvBevel
        Left = 342
        Top = 16
        Width = 6
        Height = 385
        Style = bsRaised
      end
      object Label1: TevLabel
        Left = 16
        Top = 168
        Width = 102
        Height = 16
        Caption = '~Override Rate Type'
        FocusControl = wwlcOverrideRateType
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TevLabel
        Left = 362
        Top = 153
        Width = 127
        Height = 16
        Caption = '~Worksheet printing mode'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblSyState: TevLabel
        Left = 362
        Top = 204
        Width = 76
        Height = 13
        Caption = 'Tax Credit State'
        FocusControl = lcSyState
      end
      object lblUpdate_Hours: TevLabel
        Left = 144
        Top = 395
        Width = 163
        Height = 16
        Caption = '~Show Hours on Checks\Reports'
        FocusControl = evcbUpdate_Hours
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dedtCustom_E_D_Code_Number: TevDBEdit
        Left = 16
        Top = 32
        Width = 124
        Height = 21
        HelpContext = 8123
        DataField = 'CUSTOM_E_D_CODE_NUMBER'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '{&,#,_}*{?,#,_}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtDecription: TevDBEdit
        Left = 104
        Top = 80
        Width = 232
        Height = 21
        HelpContext = 8125
        DataField = 'DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtOverride_Rate_Number: TevDBEdit
        Left = 16
        Top = 280
        Width = 73
        Height = 21
        HelpContext = 8128
        DataField = 'OVERRIDE_EE_RATE_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtW2_Box: TevDBEdit
        Left = 16
        Top = 328
        Width = 73
        Height = 21
        HelpContext = 8129
        DataField = 'W2_BOX'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtPriority_Code_Exlude_From_Check: TevDBEdit
        Left = 16
        Top = 376
        Width = 73
        Height = 21
        HelpContext = 8130
        DataField = 'PRIORITY_TO_EXCLUDE'
        DataSource = wwdsDetail
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwlcE_D_Group: TevDBLookupCombo
        Left = 362
        Top = 25
        Width = 205
        Height = 21
        HelpContext = 8150
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'CL_E_D_GROUPS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
        LookupField = 'CL_E_D_GROUPS_NBR'
        Style = csDropDownList
        TabOrder = 17
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object drgpShow_YDT_On_Checks: TevDBRadioGroup
        Left = 144
        Top = 233
        Width = 193
        Height = 37
        HelpContext = 8134
        Caption = '~Show YTD on Check'
        Columns = 2
        DataField = 'SHOW_YTD_ON_CHECKS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 13
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpPrevent_Direct_Deposit: TevDBRadioGroup
        Left = 144
        Top = 152
        Width = 193
        Height = 37
        HelpContext = 8131
        Caption = '~Prevent Direct Deposit'
        Columns = 2
        DataField = 'SKIP_HOURS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 11
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpMake_Up_Deduction_Shortfall: TevDBRadioGroup
        Left = 144
        Top = 193
        Width = 193
        Height = 37
        HelpContext = 8133
        Caption = '~Make Up Deducts Shortfall'
        Columns = 2
        DataField = 'MAKE_UP_DEDUCTION_SHORTFALL'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 12
        Values.Strings = (
          'Y'
          'N')
      end
      object wwlcE_D_Group_OT_Calculation: TevDBLookupCombo
        Left = 362
        Top = 73
        Width = 205
        Height = 21
        HelpContext = 8135
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'OT_ALL_CL_E_D_GROUPS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
        LookupField = 'CL_E_D_GROUPS_NBR'
        Style = csDropDownList
        TabOrder = 18
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwlcOffsetting_Deduction_Code: TevDBLookupCombo
        Left = 144
        Top = 459
        Width = 189
        Height = 21
        HelpContext = 8137
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'CUSTOM_E_D_CODE_NUMBER'
          'DESCRIPTION'#9'40'#9'DESCRIPTION')
        DataField = 'OFFSET_CL_E_DS_NBR'
        DataSource = wwdsDetail
        LookupTable = OffsetDeductionCL_ED
        LookupField = 'CL_E_DS_NBR'
        Style = csDropDownList
        TabOrder = 16
        Visible = False
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwlcTip_Min_Wage_Make_Up_Type: TevDBComboBox
        Left = 362
        Top = 121
        Width = 205
        Height = 21
        HelpContext = 8140
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TIP_MINIMUM_WAGE_MAKE_UP_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'Pay Minimum Wage Makeup'#9'P'
          'Force Tip Make Up'#9'F')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 19
        UnboundDataType = wwDefault
        OnChange = wwlcTip_Min_Wage_Make_Up_TypeChange
      end
      object wwdeRegular_OT_Rate: TevDBEdit
        Left = 16
        Top = 136
        Width = 73
        Height = 21
        HelpContext = 8126
        DataField = 'REGULAR_OT_RATE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeOverride_Rate: TevDBEdit
        Left = 16
        Top = 232
        Width = 73
        Height = 21
        HelpContext = 8127
        DataField = 'OVERRIDE_RATE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwlcOverrideRateType: TevDBComboBox
        Left = 16
        Top = 184
        Width = 119
        Height = 21
        HelpContext = 8176
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'OVERRIDE_RATE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'Regular Amount'#9'A'
          'Regular Percent'#9'P'
          'Additional Amount'#9'M'
          'Additional Percent'#9'E')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object DbCboPrintOnWorksheet: TevDBComboBox
        Left = 362
        Top = 171
        Width = 205
        Height = 21
        HelpContext = 8178
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SHOW_ON_INPUT_WORKSHEET'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Don'#39't print at all'#9'N'
          'Only on bottom'#9'Y'
          'On bottom and on top #1'#9'1'
          'On bottom and on top #2'#9'2'
          'On bottom and on top #3'#9'3'
          'On bottom and on top #4'#9'4'
          'On bottom and on top #5'#9'5'
          'On bottom and on top #6'#9'6'
          'On bottom and on top #7'#9'7'
          'On bottom and on top #8'#9'8'
          'On bottom and on top #9'#9'9')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 20
        UnboundDataType = wwDefault
      end
      object lcSyState: TevDBLookupCombo
        Left = 362
        Top = 220
        Width = 72
        Height = 21
        HelpContext = 8136
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'STATE'#9'2'#9'STATE'#9'F')
        DataField = 'SY_STATE_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_STATES.SY_STATES
        LookupField = 'SY_STATES_NBR'
        Style = csDropDownList
        TabOrder = 21
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 144
        Top = 274
        Width = 193
        Height = 37
        HelpContext = 8134
        Caption = '~Show E/D on Check'
        Columns = 2
        DataField = 'SHOW_ED_ON_CHECKS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 14
        Values.Strings = (
          'Y'
          'N')
      end
      object evGroupBox2: TevGroupBox
        Left = 354
        Top = 248
        Width = 241
        Height = 161
        Caption = 'Piecework Info'
        TabOrder = 22
        object lablE_D_Group_Piecework_Calculation: TevLabel
          Left = 8
          Top = 17
          Width = 175
          Height = 13
          Caption = 'E/D Group for Piecework Calculation'
          FocusControl = wwlcE_D_Group_Piecework_Calculation
        end
        object lablPiecework_Minimum_Wage: TevLabel
          Left = 8
          Top = 65
          Width = 126
          Height = 13
          Caption = 'Piecework Minimum Wage'
        end
        object lablPiecework_Min_Wage_Make_Up_Method: TevLabel
          Left = 8
          Top = 113
          Width = 199
          Height = 16
          Caption = '~Piecework Min Wage Recovery Method'
          FocusControl = wwlcPiecework_Min_Wage_Make_Up_Method
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object wwlcE_D_Group_Piecework_Calculation: TevDBLookupCombo
          Left = 8
          Top = 33
          Width = 205
          Height = 21
          HelpContext = 8136
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME')
          DataField = 'PIECE_CL_E_D_GROUPS_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
          LookupField = 'CL_E_D_GROUPS_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
        end
        object wwdePiecework_Minimum_Wage: TevDBEdit
          Left = 8
          Top = 81
          Width = 73
          Height = 21
          HelpContext = 8138
          DataField = 'PIECEWORK_MINIMUM_WAGE'
          DataSource = wwdsDetail
          Picture.PictureMask = 
            '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
            '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
            '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwlcPiecework_Min_Wage_Make_Up_Method: TevDBComboBox
          Left = 8
          Top = 129
          Width = 205
          Height = 21
          HelpContext = 8139
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DataField = 'PW_MIN_WAGE_MAKE_UP_METHOD'
          DataSource = wwdsDetail
          DropDownCount = 8
          ItemHeight = 0
          Items.Strings = (
            'None'#9'N'
            'Use EE'#39's First Rate of Pay'#9'F'
            'Use Piecework Minimum Wage'#9'P')
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 2
          UnboundDataType = wwDefault
        end
      end
      object evDBRadioGroup3: TevDBRadioGroup
        Left = 144
        Top = 112
        Width = 193
        Height = 37
        HelpContext = 8131
        Caption = '~Reduce Hrs for Avg OT'
        Columns = 2
        DataField = 'OVERSTATE_HOURS_FOR_OVERTIME'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 10
        Values.Strings = (
          'Y'
          'N')
      end
      object evcbUpdate_Hours: TevDBComboBox
        Left = 144
        Top = 411
        Width = 193
        Height = 21
        HelpContext = 8176
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'UPDATE_HOURS'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'Regular Amount'#9'A'
          'Regular Percent'#9'P'
          'Additional Amount'#9'M'
          'Additional Percent'#9'E')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 15
        UnboundDataType = wwDefault
      end
      object evDBRadioGroup5: TevDBRadioGroup
        Left = 144
        Top = 315
        Width = 193
        Height = 37
        HelpContext = 8133
        Caption = '~Show Four Decimals On Checks'
        Columns = 2
        DataField = 'FILLER'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 23
        Values.Strings = (
          'Y'
          'N')
      end
      object rgCobraEligible: TevDBRadioGroup
        Left = 459
        Top = 201
        Width = 108
        Height = 39
        HelpContext = 8131
        Caption = '~Cobra Eligible'
        Columns = 2
        DataField = 'COBRA_ELIGIBLE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 24
        Values.Strings = (
          'N'
          'E'
          'R')
      end
      object evDBRadioGroup6: TevDBRadioGroup
        Left = 144
        Top = 354
        Width = 193
        Height = 37
        HelpContext = 8134
        Caption = '~Show Memo on Reports'
        Columns = 2
        DataField = 'SHOW_ON_REPORT'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 25
        Values.Strings = (
          'Y'
          'N')
      end
      object edCodeType: TevDBEdit
        Left = 16
        Top = 80
        Width = 71
        Height = 21
        HelpContext = 8126
        DataField = 'E_D_CODE_TYPE'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
        Glowing = False
        AutoPopupEffectiveDateDialog = True
        AutoPopupForceQuartersOnly = True
      end
      object wwcdE_D_Code_Type: TevDBComboDlg
        Left = 16
        Top = 80
        Width = 73
        Height = 21
        HelpContext = 8124
        OnCustomDlg = wwcdE_D_Code_TypeCustomDlg
        ShowButton = True
        Style = csDropDown
        DataField = 'E_D_CODE_TYPE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = 
          '[DI,DH,DG,DA,DB,DC,DD,DE,DF,DL,DM,DW,DK,D7,DV,ET,EU,EV,EW,EX,EO,' +
          'EP,EQ,M1,M2,M3,M4,M5,M6,ER,ES,E4,E3,E5,E6,E1,E8,E7,D1,DN,DQ,DP,D' +
          'R,DS,DO,DT,DU,DZ,EF,EG,EH,EI,EJ,EK,EL,EM,EB,EN,EA,EC,ED,EE]'
        Picture.AutoFill = False
        TabOrder = 1
        WordWrap = False
        UnboundDataType = wwDefault
        OnChange = wwcdE_D_Code_TypeChange
      end
      object evDBRadioGroup7: TevDBRadioGroup
        Left = 354
        Top = 414
        Width = 241
        Height = 37
        HelpContext = 8134
        Caption = '~Round OT Calculation from 3 to 2 Decimals'
        Columns = 2
        DataField = 'ROUND_OT_CALCULATION'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 26
        Values.Strings = (
          'Y'
          'N')
        OnChange = evDBRadioGroup7Change
      end
    end
    object tshtTaxation: TTabSheet
      Caption = 'Taxation'
      ImageIndex = 29
      object bev_eds2a: TEvBevel
        Left = 0
        Top = 0
        Width = 812
        Height = 599
        Align = alClient
      end
      object bev_cl_eds2b: TEvBevel
        Left = 312
        Top = 8
        Width = 6
        Height = 393
        Style = bsRaised
      end
      object Label6: TevLabel
        Left = 344
        Top = 8
        Width = 137
        Height = 39
        Caption = 'Note:  On Pretax Deductions, taxes are reflected as backwards.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object drgpEE_OASDI: TevDBRadioGroup
        Left = 16
        Top = 208
        Width = 273
        Height = 70
        HelpContext = 8146
        Caption = '~EE OASDI'
        Columns = 3
        DataField = 'EE_EXEMPT_EXCLUDE_OASDI'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object drgpEE_Medicare: TevDBRadioGroup
        Left = 16
        Top = 288
        Width = 273
        Height = 70
        HelpContext = 8147
        Caption = '~EE Medicare'
        Columns = 3
        DataField = 'EE_EXEMPT_EXCLUDE_MEDICARE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object drgpEE_Federal: TevDBRadioGroup
        Left = 16
        Top = 128
        Width = 273
        Height = 70
        HelpContext = 8141
        Caption = '~EE Federal'
        Columns = 3
        DataField = 'EE_EXEMPT_EXCLUDE_FEDERAL'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object drgpEE_EIC: TevDBRadioGroup
        Left = 344
        Top = 48
        Width = 273
        Height = 70
        HelpContext = 8142
        Caption = '~EE EIC'
        Columns = 3
        DataField = 'EE_EXEMPT_EXCLUDE_EIC'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 4
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object drgpER_OASDI: TevDBRadioGroup
        Left = 344
        Top = 128
        Width = 273
        Height = 70
        HelpContext = 8148
        Caption = '~ER OASDI'
        Columns = 3
        DataField = 'ER_EXEMPT_EXCLUDE_OASDI'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 5
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object drgpER_Medicare: TevDBRadioGroup
        Left = 344
        Top = 208
        Width = 273
        Height = 70
        HelpContext = 8149
        Caption = '~ER Medicare'
        Columns = 3
        DataField = 'ER_EXEMPT_EXCLUDE_MEDICARE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 6
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object drgpER_FUI: TevDBRadioGroup
        Left = 344
        Top = 288
        Width = 273
        Height = 70
        HelpContext = 8143
        Caption = '~ER FUI'
        Columns = 3
        DataField = 'ER_EXEMPT_EXCLUDE_FUI'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 7
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object grbxOverride: TevGroupBox
        Left = 16
        Top = 16
        Width = 273
        Height = 97
        Caption = 'Override'
        TabOrder = 0
        TabStop = True
        object lablOverride_Federal_Tax_Value: TevLabel
          Left = 152
          Top = 32
          Width = 86
          Height = 13
          Caption = 'Federal Tax Value'
        end
        object drgpOverride_Federal_Tax_Type: TevDBRadioGroup
          Left = 16
          Top = 24
          Width = 129
          Height = 57
          HelpContext = 8144
          Caption = 'Federal Tax Type'
          DataField = 'OVERRIDE_FED_TAX_TYPE'
          DataSource = wwdsDetail
          Items.Strings = (
            'Amount'
            '%'
            'None')
          TabOrder = 0
          Values.Strings = (
            'A'
            'P'
            'N')
        end
        object wwdeOverride_Federal_Tax_Value: TevDBEdit
          Left = 152
          Top = 48
          Width = 73
          Height = 21
          HelpContext = 8145
          DataField = 'OVERRIDE_FED_TAX_VALUE'
          DataSource = wwdsDetail
          Picture.PictureMask = 
            '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
            '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
            '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object evDBRadioGroup2: TevDBRadioGroup
        Left = 16
        Top = 368
        Width = 273
        Height = 45
        HelpContext = 8134
        Caption = '~Apply Deduction Before Taxes on Tipped EEs'
        Columns = 2
        DataField = 'APPLY_BEFORE_TAXES'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 8
        Values.Strings = (
          'Y'
          'N')
      end
    end
    object tshtLimits: TTabSheet
      Caption = 'Limits'
      ImageIndex = 39
      object bev_cl_eds3: TEvBevel
        Left = 0
        Top = 0
        Width = 812
        Height = 599
        Align = alClient
      end
      object lablPay_Period_Minimum_Amount: TevLabel
        Left = 16
        Top = 16
        Width = 134
        Height = 13
        Caption = 'Pay Period Minimum Amount'
      end
      object lablPay_Period_Minimum_Percent: TevLabel
        Left = 16
        Top = 64
        Width = 118
        Height = 13
        Caption = 'Pay Period Minimum % of'
      end
      object lablMinimum_Percent_E_D_Group: TevLabel
        Left = 16
        Top = 112
        Width = 107
        Height = 13
        Caption = 'Minimum % E/D Group'
        FocusControl = wwlcMinimum_Percent_E_D_Group
      end
      object lablPer_Pay_Maximum_Amount: TevLabel
        Left = 16
        Top = 208
        Width = 137
        Height = 13
        Caption = 'Pay Period Maximum Amount'
      end
      object lablPer_Pay_Maximum_Percent: TevLabel
        Left = 16
        Top = 256
        Width = 121
        Height = 13
        Caption = 'Pay Period Maximum % of'
      end
      object lablMaximum_E_D_Group: TevLabel
        Left = 16
        Top = 304
        Width = 110
        Height = 13
        Caption = 'Maximum % E/D Group'
        FocusControl = wwlcMaximum_E_D_Group
      end
      object lablAnnual_Maximum_Amount: TevLabel
        Left = 16
        Top = 400
        Width = 119
        Height = 13
        Caption = 'Annual Maximum Amount'
      end
      object LabelTargetDescription: TevLabel
        Left = 16
        Top = 448
        Width = 87
        Height = 13
        Caption = 'Target Description'
      end
      object lblMinHours: TevLabel
        Left = 16
        Top = 160
        Width = 75
        Height = 13
        Caption = 'Minimum Hours '
      end
      object lblMaxHours: TevLabel
        Left = 16
        Top = 352
        Width = 78
        Height = 13
        Caption = 'Maximum Hours '
      end
      object wwlcMinimum_Percent_E_D_Group: TevDBLookupCombo
        Left = 16
        Top = 128
        Width = 213
        Height = 21
        HelpContext = 8153
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'MIN_CL_E_D_GROUPS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
        LookupField = 'CL_E_D_GROUPS_NBR'
        Style = csDropDownList
        TabOrder = 2
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwlcMaximum_E_D_Group: TevDBLookupCombo
        Left = 16
        Top = 320
        Width = 213
        Height = 21
        HelpContext = 8156
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'MAX_CL_E_D_GROUPS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
        LookupField = 'CL_E_D_GROUPS_NBR'
        Style = csDropDownList
        TabOrder = 5
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwdePay_Period_Minimum_Amount: TevDBEdit
        Left = 16
        Top = 32
        Width = 97
        Height = 21
        HelpContext = 8151
        DataField = 'PAY_PERIOD_MINIMUM_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePay_Period_Minimum_Percent_Of: TevDBEdit
        Left = 16
        Top = 80
        Width = 97
        Height = 21
        HelpContext = 8152
        DataField = 'PAY_PERIOD_MINIMUM_PERCENT_OF'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePay_Period_Maximum_Amount: TevDBEdit
        Left = 16
        Top = 224
        Width = 97
        Height = 21
        HelpContext = 8154
        DataField = 'PAY_PERIOD_MAXIMUM_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePay_Period_Maximum_Percent_Of: TevDBEdit
        Left = 16
        Top = 272
        Width = 97
        Height = 21
        HelpContext = 8155
        DataField = 'PAY_PERIOD_MAXIMUM_PERCENT_OF'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeAnnual_Maximum_Amount: TevDBEdit
        Left = 16
        Top = 416
        Width = 97
        Height = 21
        HelpContext = 8157
        DataField = 'ANNUAL_MAXIMUM'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object cbRemainingBalance: TevDBCheckBox
        Left = 16
        Top = 493
        Width = 233
        Height = 17
        Caption = 'Show Remaining Balance On Checks'
        DataField = 'SHOW_WHICH_BALANCE'
        DataSource = wwdsDetail
        TabOrder = 8
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dedtTargetDescription: TevDBEdit
        Left = 16
        Top = 464
        Width = 73
        Height = 21
        HelpContext = 8126
        DataField = 'TARGET_DESCRIPTION'
        DataSource = wwdsDetail
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object edtMinHours: TevDBEdit
        Left = 16
        Top = 176
        Width = 97
        Height = 21
        HelpContext = 8155
        DataField = 'MINIMUM_HW_HOURS'
        DataSource = wwdsDetail
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object edtMaxHours: TevDBEdit
        Left = 16
        Top = 368
        Width = 97
        Height = 21
        HelpContext = 8157
        DataField = 'MAXIMUM_HW_HOURS'
        DataSource = wwdsDetail
        TabOrder = 10
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
    object tshtPension_And_Benefits: TTabSheet
      Caption = 'Pension && Benefits'
      ImageIndex = 41
      object bev_cl_eds4: TEvBevel
        Left = 0
        Top = 0
        Width = 812
        Height = 599
        Align = alClient
      end
      object lablThird_Party_Sick_Administrator: TevLabel
        Left = 312
        Top = 20
        Width = 138
        Height = 13
        Caption = 'Third Party Sick Administrator'
        FocusControl = wwlcThird_Party_Sick_Administrator
      end
      object lablUnion_Link: TevLabel
        Left = 336
        Top = 272
        Width = 51
        Height = 13
        Caption = 'Union Link'
        FocusControl = wwlcUnion_Link
        Visible = False
      end
      object gbPandB1: TevGroupBox
        Left = 16
        Top = 16
        Width = 278
        Height = 281
        Caption = 'Pension && Benefits'
        TabOrder = 0
        TabStop = True
        object lablPension_Match_First_Percent: TevLabel
          Left = 16
          Top = 72
          Width = 63
          Height = 13
          Caption = 'Match First %'
        end
        object lablPension_Match_First_Ammount: TevLabel
          Left = 160
          Top = 72
          Width = 44
          Height = 13
          Caption = 'Of First %'
        end
        object lablPension_Match_Second_Percent: TevLabel
          Left = 16
          Top = 120
          Width = 81
          Height = 13
          Caption = 'Match Second %'
        end
        object lablPension_Match_Second_Amount: TevLabel
          Left = 160
          Top = 120
          Width = 62
          Height = 13
          Caption = 'Of Second %'
        end
        object lablPension_Flat_Match_Amount: TevLabel
          Left = 16
          Top = 168
          Width = 89
          Height = 13
          Caption = 'Match Flat Amount'
        end
        object lablPension_Link: TevLabel
          Left = 16
          Top = 24
          Width = 61
          Height = 13
          Caption = 'Pension Link'
          FocusControl = wwlcPension_Link
        end
        object wwlcPension_Link: TevDBLookupCombo
          Left = 16
          Top = 40
          Width = 257
          Height = 21
          HelpContext = 8158
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'PENSION_NAME')
          DataField = 'CL_PENSION_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_PENSION.CL_PENSION
          LookupField = 'CL_PENSION_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
        end
        object wwdeMatch_First_Percent: TevDBEdit
          Left = 16
          Top = 88
          Width = 97
          Height = 21
          HelpContext = 8159
          DataField = 'MATCH_FIRST_PERCENTAGE'
          DataSource = wwdsDetail
          Picture.PictureMask = 
            '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
            '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
            '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeMatch_First_Amount: TevDBEdit
          Left = 160
          Top = 88
          Width = 97
          Height = 21
          HelpContext = 8160
          DataField = 'MATCH_FIRST_AMOUNT'
          DataSource = wwdsDetail
          Picture.PictureMask = 
            '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
            '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
            '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeMatch_Second_Percent: TevDBEdit
          Left = 16
          Top = 136
          Width = 97
          Height = 21
          HelpContext = 8161
          DataField = 'MATCH_SECOND_PERCENTAGE'
          DataSource = wwdsDetail
          Picture.PictureMask = 
            '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
            '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
            '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeMatch_Second_Amount: TevDBEdit
          Left = 160
          Top = 136
          Width = 97
          Height = 21
          HelpContext = 8162
          DataField = 'MATCH_SECOND_AMOUNT'
          DataSource = wwdsDetail
          Picture.PictureMask = 
            '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
            '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
            '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePension_Flat_Match_Amount: TevDBEdit
          Left = 16
          Top = 184
          Width = 97
          Height = 21
          HelpContext = 8163
          DataField = 'FLAT_MATCH_AMOUNT'
          DataSource = wwdsDetail
          Picture.PictureMask = 
            '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
            '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
            '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object drgpRound: TevDBRadioGroup
          Left = 16
          Top = 216
          Width = 241
          Height = 45
          HelpContext = 8167
          Caption = '~Primary Pension for Limits'
          Columns = 2
          DataField = 'SD_ROUND'
          DataSource = wwdsDetail
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 6
          Values.Strings = (
            'Y'
            'N')
        end
      end
      object wwlcThird_Party_Sick_Administrator: TevDBLookupCombo
        Left = 312
        Top = 36
        Width = 257
        Height = 21
        HelpContext = 8166
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'CL_3_PARTY_SICK_PAY_ADMIN_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_3_PARTY_SICK_PAY_ADMIN.CL_3_PARTY_SICK_PAY_ADMIN
        LookupField = 'CL_3_PARTY_SICK_PAY_ADMIN_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwlcUnion_Link: TevDBLookupCombo
        Left = 336
        Top = 288
        Width = 257
        Height = 21
        HelpContext = 8164
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'CL_UNION_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_UNION.CL_UNION
        LookupField = 'CL_UNION_NBR'
        Style = csDropDownList
        TabOrder = 2
        Visible = False
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
    end
    object tshtScheduled_Defaults: TTabSheet
      Caption = 'Scheduled Defaults'
      ImageIndex = 42
      object EvBevel1: TEvBevel
        Left = 486
        Top = 17
        Width = 208
        Height = 94
      end
      object bev_cl_eds5b: TEvBevel
        Left = 469
        Top = 16
        Width = 6
        Height = 420
        Style = bsRaised
      end
      object lablFrequency: TevLabel
        Left = 16
        Top = 216
        Width = 59
        Height = 16
        Caption = '~Frequency'
        FocusControl = wwcbFrequency
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablEffective_Start_Date: TevLabel
        Left = 16
        Top = 348
        Width = 145
        Height = 13
        AutoSize = False
        Caption = 'Effective Start Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAmount: TevLabel
        Left = 16
        Top = 172
        Width = 36
        Height = 13
        Caption = 'Amount'
      end
      object lablRate: TevLabel
        Left = 88
        Top = 172
        Width = 8
        Height = 13
        Caption = '%'
      end
      object lablCalculation_Method: TevLabel
        Left = 16
        Top = 128
        Width = 100
        Height = 16
        Caption = '~Calculation Method'
        FocusControl = wwcbCalculation_Method
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablCustom_Expression: TevLabel
        Left = 486
        Top = 358
        Width = 89
        Height = 13
        Caption = 'Custom Expression'
        FocusControl = dmemCustom_Expression
        Visible = False
      end
      object lablDefault_Agency: TevLabel
        Left = 16
        Top = 304
        Width = 73
        Height = 13
        Caption = 'Default Agency'
        FocusControl = wwlcDefault_Agency
      end
      object lbMonthNumber: TevLabel
        Left = 16
        Top = 260
        Width = 40
        Height = 13
        Caption = 'Month #'
      end
      object lbMaxAvgHourlyWageRate: TevLabel
        Left = 488
        Top = 117
        Width = 133
        Height = 13
        Caption = 'Max Avg Hourly Wage Rate'
        FocusControl = evDBEdit6
      end
      object evLabel9: TevLabel
        Left = 120
        Top = 260
        Width = 70
        Height = 13
        Caption = 'Which Payrolls'
        FocusControl = wwdpWhich_payrolls
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel5: TevLabel
        Left = 288
        Top = 392
        Width = 94
        Height = 13
        Caption = 'Always Pay/Deduct'
        FocusControl = wwdpAlways_Pay_Deduct
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel3: TevLabel
        Left = 489
        Top = 64
        Width = 195
        Height = 13
        AutoSize = False
        Caption = 'Maximum Average Hours E/D Group'
        FocusControl = evDBLookupCombo3
      end
      object evLabel2: TevLabel
        Left = 489
        Top = 24
        Width = 195
        Height = 13
        AutoSize = False
        Caption = 'Maximum Average Amount E/D Group'
        FocusControl = evDBLookupCombo2
      end
      object lablPriority: TevLabel
        Left = 16
        Top = 392
        Width = 49
        Height = 13
        AutoSize = False
        Caption = 'Priority'
        FocusControl = dbsePriority
      end
      object lblFundingCode: TevLabel
        Left = 486
        Top = 276
        Width = 83
        Height = 13
        AutoSize = False
        Caption = 'Funding code '
      end
      object lblExcessCode: TevLabel
        Left = 486
        Top = 316
        Width = 110
        Height = 13
        AutoSize = False
        Caption = 'Excess Code '
      end
      object drgpExclude_Fifth_Week: TevDBRadioGroup
        Left = 288
        Top = 240
        Width = 169
        Height = 50
        HelpContext = 8179
        Caption = '~Block 5th Week of Month'
        Columns = 2
        DataField = 'SD_EXCLUDE_WEEK_5'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 15
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpExclude_Fourth_Week: TevDBRadioGroup
        Left = 288
        Top = 184
        Width = 169
        Height = 50
        HelpContext = 8179
        Caption = '~Block 4th Week of Month'
        Columns = 2
        DataField = 'SD_EXCLUDE_WEEK_4'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 14
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpExclude_Third_Week: TevDBRadioGroup
        Left = 288
        Top = 128
        Width = 169
        Height = 50
        HelpContext = 8179
        Caption = '~Block 3rd Week of Month'
        Columns = 2
        DataField = 'SD_EXCLUDE_WEEK_3'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 13
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpExclude_Second_Week: TevDBRadioGroup
        Left = 288
        Top = 72
        Width = 169
        Height = 50
        HelpContext = 8179
        Caption = '~Block 2nd Week of Month'
        Columns = 2
        DataField = 'SD_EXCLUDE_WEEK_2'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 12
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpExclude_First_Week: TevDBRadioGroup
        Left = 288
        Top = 16
        Width = 169
        Height = 50
        HelpContext = 8179
        Caption = '~Block 1st Week of Month'
        Columns = 2
        DataField = 'SD_EXCLUDE_WEEK_1'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 11
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpScheduled_Defaults: TevDBRadioGroup
        Left = 16
        Top = 16
        Width = 169
        Height = 50
        HelpContext = 8171
        Caption = '~Scheduled Defaults'
        Columns = 2
        DataField = 'SCHEDULED_DEFAULTS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 0
        Values.Strings = (
          'Y'
          'N')
      end
      object drgpAuto: TevDBRadioGroup
        Left = 16
        Top = 72
        Width = 169
        Height = 50
        HelpContext = 8172
        Caption = '~Auto Enlist on New Hire'
        Columns = 2
        DataField = 'SD_AUTO'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'Y'
          'N')
      end
      object wwcbFrequency: TevDBComboBox
        Left = 16
        Top = 232
        Width = 249
        Height = 21
        HelpContext = 8174
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SD_FREQUENCY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Every Pay'#9'D'
          'Every Scheduled Pay'#9'P'
          'Every Other Scheduled Pay'#9'O'
          'Monthly'#9'Y'
          'First Scheduled of Month'#9'F'
          'Last Scheduled of Month'#9'L'
          'Closest Scheduled to 15th of Month'#9'M'
          'Quarterly'#9'Q'
          'Annual'#9'A'
          'Semi-Annual'#9'S')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object wwcbCalculation_Method: TevDBComboBox
        Left = 16
        Top = 144
        Width = 249
        Height = 21
        HelpContext = 8168
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SD_CALCULATION_METHOD'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Fixed'#9'F'
          '% of Gross'#9'G'
          '% of Net'#9'N'
          '% of Disposable Earnings'#9'D'
          'Garnishment'#9'R'
          '% of Earn Code Group'#9'P'
          'Levy'#9'L'
          'Pension Match'#9'S'
          'Rate * Hours'#9'H'
          'New York SDI'#9'Y'
          'None'#9'Z')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object dmemCustom_Expression: TEvDBMemo
        Left = 486
        Top = 374
        Width = 177
        Height = 97
        HelpContext = 8170
        DataField = 'SD_EXPRESSION'
        DataSource = wwdsDetail
        TabOrder = 22
        Visible = False
      end
      object wwlcDefault_Agency: TevDBLookupCombo
        Left = 16
        Top = 320
        Width = 249
        Height = 21
        HelpContext = 8175
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Agency_Name'#9'40'#9'Agency_Name'#9'F'
          'AgencyAddress'#9'62'#9'AgencyAddress'#9'F')
        DataField = 'DEFAULT_CL_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_AGENCY.CL_AGENCY
        LookupField = 'CL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 8
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwdpEffective_Start_Date: TevDBDateTimePicker
        Left = 16
        Top = 364
        Width = 153
        Height = 21
        HelpContext = 8169
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'SD_EFFECTIVE_START_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 9
      end
      object wwdeSD_Amount: TevDBEdit
        Left = 16
        Top = 188
        Width = 65
        Height = 21
        HelpContext = 8190
        DataField = 'SD_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeSD_Rate: TevDBEdit
        Left = 88
        Top = 188
        Width = 48
        Height = 21
        HelpContext = 8191
        DataField = 'SD_RATE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object evDBEdit6: TevDBEdit
        Left = 488
        Top = 132
        Width = 105
        Height = 21
        HelpContext = 8190
        DataField = 'SD_MAX_AVERAGE_HOURLY_WAGE_RATE'
        DataSource = wwdsDetail
        TabOrder = 20
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdpMonth: TevDBComboBox
        Left = 16
        Top = 276
        Width = 81
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SD_PLAN_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object wwdpWhich_payrolls: TevDBComboBox
        Left = 120
        Top = 276
        Width = 105
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SD_WHICH_CHECKS'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object wwdpAlways_Pay_Deduct: TevDBComboBox
        Left = 288
        Top = 408
        Width = 133
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'SD_ALWAYS_PAY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 16
        UnboundDataType = wwDefault
      end
      object evDBRadioGroup4: TevDBRadioGroup
        Left = 288
        Top = 293
        Width = 169
        Height = 47
        HelpContext = 19597
        Caption = 'Deductions to zero'
        Columns = 2
        DataField = 'SD_DEDUCTIONS_TO_ZERO'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 17
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBLookupCombo3: TevDBLookupCombo
        Left = 489
        Top = 80
        Width = 203
        Height = 21
        HelpContext = 19591
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'SD_MAX_AVG_HRS_GRP_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
        LookupField = 'CL_E_D_GROUPS_NBR'
        Style = csDropDownList
        TabOrder = 19
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object evDBLookupCombo2: TevDBLookupCombo
        Left = 489
        Top = 40
        Width = 203
        Height = 21
        HelpContext = 19591
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'SD_MAX_AVG_AMT_GRP_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
        LookupField = 'CL_E_D_GROUPS_NBR'
        Style = csDropDownList
        TabOrder = 18
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object dbsePriority: TevDBSpinEdit
        Left = 16
        Top = 408
        Width = 49
        Height = 21
        Increment = 1.000000000000000000
        MaxValue = 999.000000000000000000
        MinValue = 1.000000000000000000
        Value = 1.000000000000000000
        DataField = 'SD_PRIORITY_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 10
        UnboundDataType = wwDefault
      end
      object Panel4: TPanel
        Left = 486
        Top = 158
        Width = 177
        Height = 105
        BevelOuter = bvLowered
        TabOrder = 21
        object labl_Threshold_Amount: TevLabel
          Left = 8
          Top = 44
          Width = 110
          Height = 13
          AutoSize = False
          Caption = 'Threshold Amount'
        end
        object labl_Threshold_E_D_Group: TevLabel
          Left = 8
          Top = 4
          Width = 153
          Height = 13
          AutoSize = False
          Caption = 'Threshold E/D Group'
          FocusControl = wwlcThreshold_E_D_Group
        end
        object wwlcThreshold_E_D_Group: TevDBLookupCombo
          Left = 8
          Top = 20
          Width = 161
          Height = 19
          HelpContext = 19591
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME')
          DataField = 'SD_THRESHOLD_E_D_GROUPS_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
          LookupField = 'CL_E_D_GROUPS_NBR'
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Frame.FocusStyle = efsFrameSunken
          Frame.NonFocusStyle = efsFrameSunken
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
        end
        object dedtThreshold_Amount: TevDBEdit
          Left = 8
          Top = 60
          Width = 64
          Height = 19
          HelpContext = 8190
          BorderStyle = bsNone
          DataField = 'SD_THRESHOLD_AMOUNT'
          DataSource = wwdsDetail
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Frame.FocusStyle = efsFrameSunken
          Frame.NonFocusStyle = efsFrameSunken
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object cbUsePensionLimit: TevDBCheckBox
          Left = 8
          Top = 84
          Width = 161
          Height = 17
          Caption = 'Use Pension Limit'
          DataField = 'SD_USE_PENSION_LIMIT'
          DataSource = wwdsDetail
          TabOrder = 2
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnClick = cbUsePensionLimitClick
        end
      end
      object drgpDeduct_whole_check: TevDBRadioGroup
        Left = 288
        Top = 344
        Width = 167
        Height = 41
        HelpContext = 19597
        Caption = '~Deduct Whole Check'
        Columns = 2
        DataField = 'SD_DEDUCT_WHOLE_CHECK'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 23
        Values.Strings = (
          'Y'
          'N')
      end
      object cbFundingCode: TevDBLookupCombo
        Left = 486
        Top = 292
        Width = 178
        Height = 19
        HelpContext = 19591
        BorderStyle = bsNone
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'HW_FUNDING_CL_E_D_GROUPS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
        LookupField = 'CL_E_D_GROUPS_NBR'
        Style = csDropDownList
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        Frame.FocusStyle = efsFrameSunken
        Frame.NonFocusStyle = efsFrameSunken
        TabOrder = 24
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object cbExcessCode: TevDBLookupCombo
        Left = 486
        Top = 332
        Width = 178
        Height = 19
        HelpContext = 19591
        BorderStyle = bsNone
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'HW_EXCESS_CL_E_D_GROUPS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
        LookupField = 'CL_E_D_GROUPS_NBR'
        Style = csDropDownList
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        Frame.FocusStyle = efsFrameSunken
        Frame.NonFocusStyle = efsFrameSunken
        TabOrder = 25
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
    end
    object tbshStateTaxation: TTabSheet
      Caption = 'State Taxation'
      ImageIndex = 40
      object lablState: TevLabel
        Left = 16
        Top = 208
        Width = 34
        Height = 16
        Caption = '~State'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablWarning1: TevLabel
        Left = 320
        Top = 336
        Width = 233
        Height = 49
        Alignment = taCenter
        AutoSize = False
        Caption = 
          'Do  not use this screen unless overriding taxation that is setup' +
          ' on the system level'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object evDBGrid1: TevDBGrid
        Left = 8
        Top = 8
        Width = 297
        Height = 193
        DisableThemesInTitle = False
        Selected.Strings = (
          'State_Lookup'#9'2'#9'State'#9'F'
          'EMPLOYEE_STATE_OR_TYPE'#9'1'#9'Override Type'#9'F'
          'EMPLOYEE_STATE_OR_VALUE'#9'10'#9'Override Value'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_CL_E_DS\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsStateTax
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwlcState: TevDBLookupCombo
        Left = 16
        Top = 224
        Width = 65
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'STATE'#9'2'#9'STATE')
        DataField = 'SY_STATES_NBR'
        DataSource = wwdsStateTax
        LookupTable = DM_SY_STATES.SY_STATES
        LookupField = 'SY_STATES_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnChange = wwlcStateChange
      end
      object evGroupBox1: TevGroupBox
        Left = 16
        Top = 264
        Width = 241
        Height = 145
        Caption = 'Override'
        TabOrder = 2
        TabStop = True
        object lablValue: TevLabel
          Left = 16
          Top = 88
          Width = 27
          Height = 13
          Caption = 'Value'
          FocusControl = dedtValue
        end
        object dedtValue: TevDBEdit
          Left = 16
          Top = 104
          Width = 105
          Height = 21
          DataField = 'EMPLOYEE_STATE_OR_VALUE'
          DataSource = wwdsStateTax
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object drgpType: TevDBRadioGroup
          Left = 16
          Top = 24
          Width = 209
          Height = 51
          Caption = '~Type'
          Columns = 2
          DataField = 'EMPLOYEE_STATE_OR_TYPE'
          DataSource = wwdsStateTax
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Amount'
            '%'
            'None')
          ParentFont = False
          TabOrder = 0
          Values.Strings = (
            'A'
            'P'
            'N')
        end
      end
      object drgpEmployee_Exempt_Exclude_State: TevDBRadioGroup
        Left = 320
        Top = 8
        Width = 233
        Height = 57
        Caption = '~EE State'
        Columns = 3
        DataField = 'EMPLOYEE_EXEMPT_EXCLUDE_STATE'
        DataSource = wwdsStateTax
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object drgpEmployee_Exempt_Exclude_SDI: TevDBRadioGroup
        Left = 320
        Top = 72
        Width = 233
        Height = 57
        Caption = '~EE SDI'
        Columns = 3
        DataField = 'EMPLOYEE_EXEMPT_EXCLUDE_SDI'
        DataSource = wwdsStateTax
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 4
        Values.Strings = (
          'X'
          'E'
          'I')
      end
      object drgpEmployee_Exempt_Exclude_SUI: TevDBRadioGroup
        Left = 320
        Top = 136
        Width = 233
        Height = 57
        Caption = '~EE SUI'
        Columns = 3
        DataField = 'EMPLOYEE_EXEMPT_EXCLUDE_SUI'
        DataSource = wwdsStateTax
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 5
        Values.Strings = (
          'X'
          'E'
          'I')
      end
      object drgpEmployer_Exempt_Exclude_SDI: TevDBRadioGroup
        Left = 320
        Top = 200
        Width = 233
        Height = 57
        Caption = '~ER SDI'
        Columns = 3
        DataField = 'EMPLOYER_EXEMPT_EXCLUDE_SDI'
        DataSource = wwdsStateTax
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 6
        Values.Strings = (
          'X'
          'E'
          'I')
      end
      object drgpEmployer_Exempt_Exclude_SUI: TevDBRadioGroup
        Left = 320
        Top = 264
        Width = 233
        Height = 57
        Caption = '~ER SUI'
        Columns = 3
        DataField = 'EMPLOYER_EXEMPT_EXCLUDE_SUI'
        DataSource = wwdsStateTax
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 7
        Values.Strings = (
          'X'
          'E'
          'I')
      end
      inline StateTaxMiniNavigationFrame: TMiniNavigationFrame
        Left = 238
        Top = 208
        Width = 67
        Height = 25
        TabOrder = 8
        inherited evActionList1: TevActionList
          Left = 0
        end
      end
    end
    object tbshLocalTaxation: TTabSheet
      Caption = 'Local Taxation'
      ImageIndex = 43
      object lablLocal: TevLabel
        Left = 352
        Top = 8
        Width = 35
        Height = 16
        Caption = '~Local'
        FocusControl = wwlcSY_LOCALS_NBR
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablWarning2: TevLabel
        Left = 352
        Top = 152
        Width = 233
        Height = 49
        Alignment = taCenter
        AutoSize = False
        Caption = 
          'Do  not use this screen unless overriding taxation that is setup' +
          ' on the system level'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object wwlcSY_LOCALS_NBR: TevDBLookupCombo
        Left = 352
        Top = 24
        Width = 265
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'LOCALITY_NAME')
        DataField = 'SY_LOCALS_NBR'
        DataSource = wwdsLocalTax
        LookupTable = DM_SY_LOCALS.SY_LOCALS
        LookupField = 'SY_LOCALS_NBR'
        Style = csDropDownList
        TabOrder = 0
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnChange = wwlcSY_LOCALS_NBRChange
      end
      object drgpExempt_Exlude: TevDBRadioGroup
        Left = 352
        Top = 64
        Width = 265
        Height = 75
        Caption = '~Local Tax'
        Columns = 3
        DataField = 'EXEMPT_EXCLUDE'
        DataSource = wwdsLocalTax
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'E'
          'X'
          'I')
      end
      object evDBGrid2: TevDBGrid
        Left = 8
        Top = 8
        Width = 321
        Height = 361
        DisableThemesInTitle = False
        Selected.Strings = (
          'Local_Name_Lookup'#9'10'#9'State'#9'F'
          'EXEMPT_EXCLUDE'#9'1'#9'Local Tax'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_CL_E_DS\evDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsLocalTax
        TabOrder = 2
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      inline LocalTaxMiniNavigationFrame: TMiniNavigationFrame
        Left = 264
        Top = 376
        Width = 65
        Height = 25
        TabOrder = 3
        inherited evActionList1: TevActionList
          Left = 8
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 133
    Top = 74
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_E_DS.CL_E_DS
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    Left = 174
    Top = 74
  end
  inherited wwdsList: TevDataSource
    Left = 122
    Top = 66
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 155
    Top = 114
  end
  inherited PageControlImages: TevImageList
    Bitmap = {
      494C01012C003100040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000D0000000010020000000000000D0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EDEDED00DBDBDB00CCCCCC00CCCCCC00CCCCCC00E5E5
      E500000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DDDDDD00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CFCFCF000000000000000000000000000000
      000000000000DCDCDC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CFCFCF0000000000D1D1D100CCCCCC00CCCC
      CC00CCCCCC00D1D1D10000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CCCCCC00DEDEDE00F5F5F500EDED
      ED00DBDBDB00CCCCCC0095B8CD005EA4CD003294CD003294CD003294CD007FB1
      CE00CCCCCC00E5E5E500000000000000000000000000DCDCDC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00BFA17E00B5855100B5834F00B5834E00B6834F00B684
      4F00B5834E00B5834F00B5855100BA8D5E000000000000000000000000000000
      000000000000B09F8A00A08766009E8563009E8462009E8462009E8462009E84
      62009E8462009E856300A0876600A7907300B2C6D3005BB1E7004DABE9004EAB
      E90051ACE9005FB2E700B4C6D300FFFFFF00FFFFFF00F6F6F600D1D1D100CCCC
      CC00CCCCCC00CCCCCC00D1D1D1000000000057555400BFB8B500ABC1CD0096B9
      CD005EA4CD003193CD0038A1D7003EAEE30042BBEE0042BBEE0040B8EC003BAA
      E1003399D2007EB0CD00CCCCCC00E5E5E50000000000C09F7A00B8895600B88A
      5700B8895600B6875200B4834E00FFF9DD00FFF3D400FFF4D400FFF5D500FFF6
      D500FFF5D400FFF3D400FFFADE00B58551000000000000000000000000000000
      000000000000A087660000000000000000000000000000000000000000000000
      0000000000000000000000000000A08766004FADEA009EE3F700BCFBFF0095F0
      FF006AE8FF0051CBF80054AEEA00CCCCCC00CCCCCC00B5C6D1005CB0E7004DAB
      E9004EABE90051ACE9005FB2E700B2C6D3005B595800E4DDD900339DD7003DA2
      D80049B1E30052C0EF0056C4F2004BB6E80039A2D9003499D200389BD20088D2
      F2007CD2F5005BBCEA003EA6DC007AAECC00CCCCCC00BC884E00FFDFAA00BC8D
      5800B5855000FFFEF400B17D4600FFF5DB002CB78C0026B58B002FB98C001A71
      85002DB98C002FB88D00FFF6DC00B4834E000000000000000000000000000000
      0000000000009E85630000000000C0C1C200C1C1C200C1C2C200C1C2C200C1C2
      C200C1C1C200C0C1C200000000009E8563004EACEA008FD1F200A5D2F40083C1
      EF0066B3EC0052B6EE0052ADEA0051ADEA0051ACEA004EABEA009EE2F700BCFB
      FF0095F0FF006AE8FF0051CBF80054AEEA00615F5E00EAE2E0005AC5F30061C7
      F20061C6F20063C7F2003398D00040A5D9006EC1EA009FDFFA0065B5DF003497
      D00075BFE5009AD4F000BDEBFD006EBBE2008D8D9000C5CAD10057616E00C697
      5E00B4834D00FFF9EE00AE7B4300FFF5DF00FFE5AF00FFE8B200C1CEA0001886
      8000C0CEA000FFE7B000FFF6E100B4834D00CFCFCF00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC009E85620000000000BEBDBD00FFFEFE00BFBEBE00FFFEFE00BFBE
      BE00FFFEFE00BEBDBD00000000009E8563004EACEA0095DBF600AFF0FD0088E6
      FD005FDEFF0047C4F60057B0EA009BF4FF006AE9FF004CADEA008FD0F200A5D2
      F40083C1EF0066B3EC0052B6EE0054AEEA0067656400F0EBE900ABE3FB00A5E0
      F9009ADCF80095DAF800A6E1FA00C4ECFD0093CFED0062B2DE002E8CCB003E82
      C6003E8DC90071B6DE002D92CD0057A8D80000000000BF894D00AB7A3E00CB9F
      6500B4824C00FFF8ED00AE7A4200FFF7E400FFDDA200FFE1A600149E820022A3
      8400139E8200FFE1A500FFF9E600B3824D00A7907300A08766009E8563009E85
      63009F8664009D825F0000000000BCBDBA00BEBEBA00C4C3C000BEBEBA00C4C3
      C000BEBEBA00BCBDBA00000000009F8663004EACEA008DCEF200A5D2F40083C1
      EF0066B4EC004EB3EE004FABE90087C3EF0066B4EC004BA9E90095DBF600AFF0
      FD0088E6FD005FDEFF0047C5F70054AEEA006E6B6A00FDF7F50080C0E50062B0
      DC0079BFE5007BC1E60067B6DF0048A6D9005DB5E4006FC2EB0075C9EF0067C4
      E9004084C70000000000FBFDFF00F9FCFE00CCCCCC00BD874C00FFDFA400D0A5
      6800B3814C00FFF8ED00AE7A4100FFFAEA00FFD79000FFDB95004CB18D0022AA
      8D0048B08E00FFDA9800FFFBF000B3824C00A087660000000000000000000000
      0000000000009B805A0000000000C4C2B500FFFFF100C9C7B600FFFFF200C9C7
      B600FFFFF100C4C2B50000000000A08663004EACEA0095DBF600AFF0FD0088E6
      FD005FDEFF0047C5F60056AFEA008FEBFE005FDFFF004AACEA008DCDF200A5D2
      F30084C1EF0066B3EC004EB3EE0054AEEA0076737200D9D4D000000000000000
      000000000000488AC7003982C5003A80C500397FC500397FC5003A80C5003C82
      C6004B8CC8000000000000000000000000008E8D8F00C5C9D100565F6D00D9AE
      6F00B3814C00FFF8EE00AF7A4000FFFEF00015B9FF007EC6DF00CFCB8D0018B0
      9900C4C99100FFD38B00FFFFF900B3824C009E85630000000000C0C1C200C1C2
      C400C5C8CB009D815900000000000000CE000000CE000000D0000000CF000000
      D0000000CE000000CE0000000000A18863004EACEA008ECDF200A5D2F30084C1
      EF0066B3EC004EB3EE0050ABE90088C4EF0067B5EC004DA9E90098DAF500B4EF
      FC008BE4FC0060DBFE0047C3F60054ADEA000000000000000000000000000000
      0000000000003D84C7008DD7F9009FE1FF00A3E5FF008FD9F9007ACEF10067C4
      E9003F85C80000000000000000000000000000000000BF884D00A67B3A00DEB5
      7600B2804B00FFF9EE00AF7A4000FFFFFB007AC9D5001DCAFF00FFCE7B00FFCE
      8100FFCD8100FFCC800000000000B3824C009E85630000000000BEBDBD000000
      0000C3C4C7009E825A00FFFFFE000000CD00FFFFE400D3D0AB00FFFFE500D3D0
      AB00FFFFE4000000CD0000000000A28863004FACEA0099DAF600B4EFFC008BE4
      FC0060DBFE0047C2F50050AAE9008AE6FD0060DFFF004FA8E80061E1FC0058DE
      FE005ADDFD005DDFFE0058E0FF0053ACE9000000000000000000000000000000
      00000000000081AFDB003C83C7003B82C6003B82C6003C83C6003D83C7003E84
      C70081AFDC00000000000000000000000000CCCCCC00BD874C00FFE09E00E3BB
      7A00B17F4A00FFFAF000AF7B4100FFF7E100FFF5DC00FFF4DC00FFF5E100FFF5
      E300FFF4E300FFF5E200FFFAE700B58550009F86630000000000BCBDBA00BEBF
      BB00C7C8C8009F845A00000000000000CD000000CE000000CF000000CF000000
      CF000000CE000000CD0000000000A188630051ABEA0062E1FC0058DEFE005ADD
      FD005DDFFE0057DFFE004FA9E80088C3EF0067B4EC0050A9E90073E5F70078EB
      F80078EAF80079EBF80069E4FA0053ADEA000000000000000000000000000000
      000000000000F7F7F700DADADA00CCCCCC00CCCCCC00CCCCCC00CCCCCC00DADA
      DA00F7F7F7000000000000000000000000008E8D9000C5C9D100555D6C00EBC4
      7F00B17F4900FFFAF000C9A07100AF7B4300AF7C4400AF7C4400AE7C4400AD7B
      4400AD7B4300AF7C4500B3824C00BF946500A086630000000000C4C2B500FFFF
      F300CACABE00A5895B0000000000FFFFF400FFFFF400FFFFF500FFFFF500FFFF
      F600FFFFF600FFFFF60000000000A189670052ADEA0073E6F80078EBF80078EA
      F80078EBF80066E2FA004BA8E80089E5FC0060DEFF0047C4F70057B2EA0051AB
      E90051ABE90052ABE90060BAF000DBEEFB000000000000000000000000000000
      000000000000B9C4CF006699C9003B82C6003B82C6003B82C6003B82C6006699
      C900B9C4CF0000000000000000000000000000000000BF884D00A57C3500ECC5
      8100B07E4900FFF9F000F3D7B700F5DBBD00F7DEC100F8DEC100F6DDBF00F5DB
      BD00F5DABA00FFFEF600B5844E0000000000A1886300000000000000CE000000
      CF000000D500594B8B00A5895A00A5895A00A88C5900A3885B00A2876000A389
      6300A3896400A2896400A1896700AA937500DAEEFB0067BBEF0051ABEA0051AA
      E9004FA9E90056B3EE0092C9F10088C3EF0067B3EC004EB3EE0054AEEA00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000004487C80073B8E8009FE3FF009EE2FF009EE2FF009FE3FF0073B8
      E7004487C800000000000000000000000000CCCCCC00BD874C00F9D08B00EBC4
      7F00B07D4800FFFBF300F0D1AE00F2D5B300AC794100D0A87C00CEA67A00F1D4
      B200F0D2AE00FFFCF500B4834C0000000000A2886300000000000000CD00FFFF
      E400D0CEAC00FFFFEB00C6C6B200FFFFEC000000D50000000000A28965000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0050ACEA009BDBF600B7F0FD008CE5FC0060DBFE0047C3F60054ADEA00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000003C83C600A0E4FF0066BCE6005FB8E3005FB8E30066BCE600A0E4
      FF004188CA000000000000000000000000008D8D9000C3C8CF00525A6700EBC3
      7B00B07D4600FFFDF800EECCA600EFCEA900F0D1AC00F0D1AC00F0CFAB00EECD
      A800EECCA600FFFEF900B4834C0000000000A1886300000000000000CD000000
      CE000000CF000000D0000000D0000000CF000000CF0000000000A18864000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0051ACEA0062E1FC0059DEFE005ADDFD005DDFFE0058E0FF0053ACE900FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000478BCA0074B9E8007DCAF2007AC8F0007AC8F0007DCAF20074B9
      E800478BCA0000000000000000000000000000000000BC864B00FFF2D400FEF1
      D400B27F490000000000FFFEFA00FFFEFA00FFFFFB00FFFFFB00FFFEFA00FFFE
      FA00FFFEFA0000000000B5844F0000000000A189670000000000FFFFF600FFFF
      F600FFFFF600FFFFF600FFFFF600FFFFF600FFFFF60000000000A18967000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0052ADEA0073E6F80078EBF80078EAF80079EBF80069E4FA0053ADEA00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000E1ECF70074A7D7003C83C7003C83C7003C83C7003C83C70074A7
      D700E1ECF70000000000000000000000000000000000D0AF8A00B7875200B686
      5200B7875200B5844E00B4824C00B4824B00B4824B00B4824B00B4824B00B482
      4B00B4824C00B5844F00CEAE8A0000000000AA937500A1896700A2896400A389
      6400A3896400A3896400A3896400A3896400A2896400A1896700AA9375000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00DAEEFB0067BBEF0051ABEA0051ABE90052ABE90060BAF000DBEEFB00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CECECE00CECECE00CECECE00F7F7F7000000000000000000000000000000
      00000000000000000000DCDCDC00CCCCCC00CCCCCC00DDDDDD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D5D5
      D500CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00EBEBEB00D6D6D600CCCC
      CC00CCCCCC00CCCCCC00D5D5D50000000000D6D6D600CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE004A84AD004A84AD004A84AD00B5BDC6000000000000000000000000000000
      000000000000000000006E99C2004182BD003D80BB006290BB00DEDEDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D5D5D5007473
      72005B5957005755520055514E003A4E58004CB7E30098BCCD005A5350002F2D
      2B006A6867005B59580066656400D5D5D500EFBD7B00EFB57300EFB57300EFB5
      7300EFB57300EFB57300EFBD7300F7BD7300F7BD7300F7BD7300FFC67300317B
      B500739CBD0021ADFF0094CEEF004294C60000000000F4F4F400F5F5F500F5F5
      F500F5F5F500F8F8F8004D8DC30057A4D60059A0D3004C8FC5006490BB00DFDF
      DF00F5F5F500000000000000000000000000CFCFCF00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CFCFCF000000000000000000807C7900A7A4
      A200E5E3E200DFDCDB009F9C9900504A4700435557004F6B6F00665E5C009997
      9400AEACAA00AFADAB00908E8D006D6D6C00EFB57300FFEFCE00FFEFC600FFEF
      C600FFEFC600FFF7CE00C6B5A5007B7B7B0073737B0073737B0073737B008C8C
      8400B5A59C009CC6D600ADEFFF004294C600D0D0D000AFC1CE00B1C2CE00B1C2
      CD00B3C3CE00BAC6CD005291C700B7E6FA00529FD300579FD2004B8EC5006690
      BA00B3C4CF00D1D1D1000000000000000000A0A0A0009A9A9A00979797009797
      9700979797009797970097979700979797009797970097979700979797009797
      970097979700979797009A9A9A00A0A0A00000000000F2F2F200776D6900E8E5
      E300837F7D006B65630055514F0053504E00544F4E0055504E00565351005654
      53006D6B69005B595800A9A7A500706E6D00EFB57300FFEFCE00FFF7E700FFF7
      E700FFEFBD008C8C840094949C00D6D6DE00E7E7E700E7E7E700D6D6DE009C9C
      9C008C848400F7EFE700BDDEEF004294CE004F9ED3004398D2004094D0003E92
      CF003E92CE003E92CF00498DC5004D99CE00B4E3F900509ED300539DD3003080
      C2003D9BDA004BA0DA00D6D6D600F8F8F8009A9A9A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009A9A9A0000000000A7C3CF007F716C00ECE7
      E5007E79750052BDE3006B65620000000000EFEDEC00E6E4E300C3C1BF007D7B
      7A00B3B3B20053515000B1AFAE00817F7D00EFB57300FFEFCE00FFF7DE00FFF7
      E700C6AD9C009C9C9C00E7E7E700EFC69400F7CE8400F7DE8C00F7E7AD00E7E7
      E7009C9C9C00B5ADAD003994CE00000000004499D2003F94D000ABFBFF009BF3
      FF0092F1FF0093F1FF0093F3FF0066AED7004B96CD00AFE1F90081AFD000BCBF
      B9008E8178008E827A009B958F00C5C4C30098989900FBFBFC00F1F5F700F1F5
      F900F1F5F900F1F5F900F1F5F900F1F5F900F1F5F900F1F5F900F1F5F900F1F5
      F900F1F5F900F1F5F700FBFBFC0098989900E5E5E5003FA7D60087797100FFFF
      FE00807C7A0065605E007C7876007D7A77007F7A77007F7A77007E7B78007F7C
      7A006866640057555300D1CFCE007F7D7C00EFB57300FFEFD600FFDEB500FFE7
      B50084848400E7E7E700E7BD8C00EFCE8C00F7D68C00FFE79C00FFEFA500F7E7
      AD00E7E7E7008C847B0000000000000000004397D10056ACDD008EDAF500A2ED
      FF0082E5FE0084E5FE0085E7FF0088EBFF0061A9D60057A7D700E0DCD800D4CD
      CC00D5D2D100EEEBE900EBE7E600928F8B00999A9A00F4F6F700AA7A5300AA7C
      5700A97C5700A97C5700A97C5700A97C5700A87B5600A87B5500A87B5500A97B
      5600AA7C5600AA7A5300F4F6F700999A9A0089B8CF004AB9E60081969A00CFC6
      C200FEF9F800E9E4E200E7E2E000B9B1AE0066767A00738C9300CDC6C3000000
      0000F1EFEE00E8E5E400BEBCBB0092908F00EFB57300FFEFD600FFDEAD00FFE7
      B5007B7B7B00F7F7FF00E7B57300F7DEB500F7DEA500F7DE9C00FFE79C00F7DE
      8C00F7F7FF008C84840000000000000000004296D10071C4EA006CBCE600BBF2
      FF0075DEFD0077DEFC0078DFFD007DE1FE007EE4FF0078E6FF00C1B4AF00DCD8
      D800CFCDCC00918C890097908B009E948D009A9A9B00F0F3F500AC7E5900FFD6
      B600FFD6B700FFD7B800FFD7B800FFD5B600FFD2B100FDCEAD00FECEAC00FFD0
      AE00FFD4B200AC7E5800F0F3F5009A9A9B0046A4CF0054CFFA003FA1D2007D8C
      91008B7E750084776F007D6E6700737D7E0069CEE90067DAFB007B8A8E00887A
      7300877C770086807C009B989600EEEEED00EFB57300FFEFD600FFEFDE00FFF7
      DE00847B7B00FFFFFF00E7B57300F7EFD600F7DEB500F7DEA500F7D68C00EFCE
      7B00FFFFFF008C84840000000000000000004095D00090DDF80044A0D800DDFC
      FF00DAFAFF00DBF9FF00DEFAFF0074DCFC0075DCFC0070DFFF00A1938E00D9D5
      D400C3BFBE009B95910070C8F9009EBDD2009A9A9B00EEF1F300AB7E5A00FFD5
      B600A77E5D00A87F5E00A87F5E00A57C5A00FBCDAC00FDFFFF00ECF0F300DEE1
      E300CFD3D700AD7D5600EEF1F3009A9A9B0046A3CE0054D3FE00297CB1003390
      C6002F92CA002A92C9004CC7F20061DDFF0066DEFF006EE1FF0055C3E9004BB9
      E30078B7D400FEFEFE000000000000000000EFB57300FFEFDE00FFEFD600FFF7
      DE008C848400EFF7F700E7B58400FFEFE700F7EFD600F7DEB500EFCE9400EFCE
      9400EFF7F700948C8C0000000000000000003E93CF00B2F6FF0051ACDE00358A
      CA00358ACA00358ACA00368ACA005BBDE9006ED9FC0066DAFF00A3958F00E3DF
      DE00B1AFAC00A29D9900A69D9700ACA098009A9A9B00EEF1F400AB7E5A00FFD5
      B600FFD5B600FFD5B600FFD5B600FED2B300FACCAB00FDFFFF00ECF0F300DEE1
      E300CFD3D700AD7D5600EEF1F4009A9A9B0047A4CE0052CFFC002976AB00287A
      AF00369DCD004FC8F4007DDFFC0073DAFB0062D7FA006CDDFC0067D4F40062CC
      EE0049ADD900D2D4D5000000000000000000EFB57300FFF7E700FFD6A500FFD6
      A500C6BDB500ADADAD0000000000E7B58400E7BD8400E7BD8400EFC694000000
      0000ADADB500C6A5840000000000000000003D92CF00B8F3FF0077DFFE007BE0
      FE007CE1FE007CE1FF007DE2FF0052ABDD0055BAE900D9FAFF00BCC7C900D4CC
      C800F3F0EE00E3E0DD00D1CAC4009FA4A8009A9A9B00F1F4F600AC7E5900FFD7
      B800A97F5E00FFD7B800A87F5D00FFD4B500FED1B000FDCEAD00FECEAC00FFD0
      AE00FFD4B200AC7E5800F1F4F6009A9A9B009FD0E6004CBDE90050CEFA0071D9
      FD0083DEFC00AFEAFD00A2E4FA0087DBF8005ED2F80061D7F90069DBFB0061CC
      EE005DC4E8005DACCF00FDFDFD0000000000EFB56B00FFF7E700FFD69C00FFD6
      9C00FFF7DE00A59C9C00B5B5B500FFFFFF000000000000000000FFFFFF00B5B5
      B500A5A5A500F7BD730000000000000000003C92CF00C0F3FF0070D9FB0073DA
      FB0074DAFB0074DAFB0074DBFB0076DEFD004FA9DD00358CCC00388ECC00899D
      AD00B1A69F00B5AAA20098A6B00091BDDB00999A9A00F8F9FC00AC7B5400AC7E
      5900AC7F5A00AC7F5A00AC7F5A00AA7D5800A97C5700A97B5600A97B5600AA7C
      5700AB7C5700AB7B5400F8F9FC00999A9A0000000000BFDFEE0049BCE80055CC
      F900D0F4FF00D2F4FF009EE1FA0081D9F800A1E4FB007EDBF90060D7FA0066D7
      F80060CCEE0043A1CD00D8DCDD0000000000EFB56B00FFF7EF00FFEFCE00FFEF
      D600FFCE9C00FFCE9C00CEBDB500949494008C8C8C008C8C8C0094949400CEBD
      AD00FFFFEF00EFB5730000000000000000003B92CF00CAF6FF0069D5F9006CD5
      F9006AD4F90069D4F90069D5F9006AD6FA006BD8FB006BD9FD0069DAFE0063D9
      FF00D2FBFF003392D300D9E7F000000000009A9A9B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009A9A9B000000000000000000C9E4F100A0D0
      E60062B2D6005DB3D80052B6DF00ADE1F500BEEDFE00ADE7FC007EDCFA0062D9
      FB0064D6F7003F9BC70080BAD500FCFCFC00EFB56B00FFFFF700FFE7CE00FFE7
      CE00FFCE9400FFCE9400FFEFCE00FFEFCE00FFCE9400FFCE9400FFEFCE00FFEF
      CE00FFFFF700EFB56B0000000000000000003B92CF00D5F7FF0060D1F90061D0
      F800B4EBFD00D9F6FF00DAF8FF00DAF8FF00DAF9FF00DBF9FF00DAF9FF00D9FA
      FF00DDFDFF003C94D100D8EAF60000000000BCBCBC009A9A9A0098999A009899
      9A0098999A0098999A0098999A0098999A0098999A0098999A0098999A009899
      9A0098999A0098999A009A9A9A00BCBCBC000000000000000000000000000000
      00000000000000000000DAEDF60082C2DF003FAAD70077D3F500A8E9FD0088E3
      FE005CC1E60046A7D30062BCE400F1F7FA00EFB57300FFFFFF00FFFFF700FFFF
      F700FFFFFF00FFFFFF00FFFFF700FFFFF700FFFFFF00FFFFFF00FFFFF700FFFF
      F700FFFFFF00EFB5730000000000000000003D94D000DCFCFF00D8F7FF00D8F7
      FF00DBFAFF00358ECD003991CE003A92CF003A92CF003A92CF003A92CF003A92
      CF003D94D00051A1D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F8FBFD0073BADA004AAED80056AD
      D5004AA9D3006FDEFB0065C0E500F4FAFD00EFBD8400EFB57300EFB56B00EFB5
      6B00EFB56B00EFB56B00EFB56B00EFB56B00EFB56B00EFB56B00EFB56B00EFB5
      6B00EFB57300EFBD840000000000000000004F9FD5003D94D0003A92CF003A92
      CF003D94D00055A2D600E0EEF800DBEBF700DAEBF600DAEBF600DAEBF600DAEB
      F600D7E9F6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000AFDA
      EE005FB4DE005EB3DD0091CCE800000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6D6D6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECECE00CECECE00DEDE
      DE00000000000000000000000000000000005A6B7B004A6B84004A94DE00528C
      D60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000EFEFEF00109452000000
      000000000000000000000000000000000000000000000000000000000000F7F7
      F700CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00F7F7F70000000000000000000000000000000000F7F7F700CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00008C520000C68400429C
      7300DEDEDE000000000000000000000000005A849C0084A5B50094D6FF005ABD
      FF00396B9C0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000CECECE00F7F7F700CECE
      CE00CECECE00CECECE00CECECE00CECECE00000000008CB5A50000945A00CECE
      CE00CECECE00CECECE00FFFFFF0000000000000000000000000000000000C6AD
      9400AD521000A5520800A5520800A5520800A5520800A5520800A5520800AD52
      1000C6AD940000000000000000000000000000000000C6AD9400AD521000AD52
      0800AD520800B5520800BD520800BD520800CE4A0000008C4A0000E7A50000BD
      840042A57300DEDEDE0000000000000000004AADEF009CE7FF009CDEFF00219C
      FF00109CFF00396B9C0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEFEF00008C4A00ADD6C600009C
      6B0000A56B0000A56B0000AD6B00009C5A00EFEFEF0000A57B0000B58C0000A5
      6B0000A56B00009C5A00CED6CE0000000000000000000000000000000000B55A
      1000D6945A00E7A56B00E7A56B00E7A56B00E7A56B00E7A56B00E7A56B00DE94
      5A00B55A100000000000000000000000000000000000B55A1000D6945A00E7A5
      6B00EFA56B00429C5A00008C4A00008C4A00008C4A000084420000DEA50000DE
      A50000BD840042A57300DEDEDE00000000004AB5EF008CE7FF0042BDFF0029A5
      FF00189CFF00189CFF00396B9C0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084B59C0000AD8400B5D6C60000AD
      8C0000C6A50000CEA50000DEA50000B573009CCEB50000BDA50000C69C0000CE
      9C0000D6A50000E7AD0039A5730000000000000000000000000000000000C673
      3100EFBD8C00DE9C5A00DE9C5A00DE9C5200DE9C5200DE9C5A00E79C5A00EFBD
      8C00BD6B210000000000000000000000000000000000C6733100EFBD8C00E79C
      5A00F79C5A000084420039EFC60000DEA50000DEA50000D69C0000D69C0000D6
      9C0000D69C0000BD840042A5730000000000FFFFFF00317BC6004ACEFF0039BD
      FF0029ADFF00189CFF00189CFF00396B9C0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00009C630000C6A5008CBDA5006BBD
      9C0000A5730000A56B0000AD7300009C5A0000000000009C630000B58C0000A5
      6B0000A56B0000D6940000AD6B0000000000000000000000000000000000CE84
      4A00EFCEAD00EFAD6B00EFAD6B00F7FFFF00EFF7F700EFB56B00EFAD6B00EFCE
      AD00C673290000000000000000000000000000000000CE844A00EFCEAD00EFAD
      6B00FFB57B00008439006BEFCE0000CE9C0000CE9C0000CE9C0000C6940000CE
      9C0000CE9C0063E7CE00008C420000000000FFFFFF00FFFFFF00397BC60052CE
      FF0039BDFF0029ADFF00189CFF00189CFF00396B9C0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00009C6B0000C6A50000AD7300D6D6
      D600F7F7F700CECECE00000000000000000000000000E7F7EF0000945A000000
      0000B5C6BD007BAD9400008C4A0000000000000000000000000000000000D694
      5A00F7DECE00F7BD7B00FFF7DE00FFFFFF00F7F7F700EFDECE00FFC67B00F7E7
      CE00CE7B310000000000000000000000000000000000D6945A00F7DECE00FFC6
      7B00FFFFEF000084390094EFE7004AEFD6004AEFD6004AE7D60094EFDE0000C6
      940063E7C60000B5840052B5840000000000FFFFFF00FFFFFF00FFFFFF00397B
      C60052CEFF0039BDFF0029ADFF00189CFF00189CFF00396B9C0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00009C6B0000BD9C0000CEA500109C
      63009CBDAD00008442000000000000000000000000000000000042AD7B00E7E7
      E700009C630000C68C000094520000000000000000000000000000000000D694
      4A00EFC69C00FFEFD600B5DEEF005AA5C600397BA5007B9CB500FFF7DE00EFC6
      A500D694520000000000000000000000000000000000D6944A00EFC69C00FFEF
      D600BDDEF700189473000084390000843900008439000084390084E7D6005ADE
      C60000AD7B005AB584000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00397BC60052CEFF0039BDFF0029ADFF00189CFF00189CFF00396B9C000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0063BD940000BD940000C69C0000CE
      940000D69400A5D6BD0000000000000000000000000000000000000000006BAD
      8C0000BD9C0000D6A50000B57300000000000000000000000000000000000000
      0000EFBD8400F7AD6300BDC6BD00639CBD00427B9C0094948C00F7AD6B00EFBD
      8400000000000000000000000000000000000000000000000000EFBD8400F7AD
      6300C6C6BD00639CBD004A7BA500A5949400FFB56B00008442007BE7DE0000AD
      7B005AB58400000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00397BC60052CEFF0039BDFF0029ADFF00189CFF00109CFF00316B
      A50000000000FFFFFF00FFFFFF00FFFFFF00CECECE0000945A0000CEA50000D6
      A50000AD6B000000000000000000000000000000000000000000000000000094
      5A0000C6AD0000D69C0029A56B00000000000000000000000000000000000000
      000000000000DEDEDE007BADC6007BBDDE006BADCE006394AD00E7E7E7000000
      000000000000000000000000000000000000000000000000000000000000DEDE
      DE007BADC6007BBDDE006BADD6006394AD00E7E7E700008C420000AD840052B5
      840000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00397BC60052CEFF0039BDFF0029ADFF00109CFF007BAD
      D600847B730000000000FFFFFF00FFFFFF00008C4A00009C630000A56B0000AD
      7300B5CEBD00CECECE00E7E7E700F7F7F700CECECE00CECECE00CECECE00CECE
      CE00089C630000AD730000000000000000000000000000000000000000000000
      0000000000005A7394009CE7FF0084C6EF0073B5DE006BADD6006B84A5000000
      0000000000000000000000000000000000000000000000000000000000005A73
      94009CE7FF0084C6EF0073B5DE006BADD6007384A50000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00397BC6004ACEFF0031BDFF0094BDDE00948C
      8400BDB5AD007B7B6B0000000000FFFFFF000000000000000000EFEFEF00CECE
      CE0052A57B00009C730063AD8C00D6EFE700008C420000B58C0000BD8400009C
      5A00E7E7E700CEEFDE0000000000000000000000000000000000000000000000
      00000000000000316300ADF7FF0094DEFF0084CEE7007BC6E70000295A000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300ADF7FF0094DEFF0084CEE7007BC6E70008215A0000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00317BCE00BDE7F70094847B00D6D6
      CE008C8C8400B573B5009C6BCE00FFFFFF000000000000000000ADDEC600009C
      6B0000C69C0000D6A500009C6B00EFEFEF00E7E7E70000945A0000CE9C0000D6
      94006BAD94000000000000000000000000000000000000000000000000000000
      00000000000008396B0021639400528CB5007BB5D600639CBD0008295A000000
      0000000000000000000000000000000000000000000000000000000000000839
      6B0021639400528CB5007BB5D600639CBD0008295A0000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0094847B00F7F7EF00848C
      8400CE94CE00BD7BB500AD7BCE00FFFFFF0000000000000000000000000000A5
      730000C6A50000CE9C0000E7A5007BB594006BAD940000BD8C0000CE9C0000CE
      940000AD7300D6D6D60000000000000000000000000000000000000000000000
      00000000000010427300317BAD004A8CBD00527BAD00294A7300103163000000
      0000000000000000000000000000000000000000000000000000000000001042
      7300317BAD004A8CBD00527BAD00294A73001031630000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084847B00EFBD
      EF00D6A5D600BD8CD600FFFFFF00FFFFFF00000000000000000000000000C6E7
      D60000AD7B0000BD840021A56B00009C730000AD8C0000CEA50000C68C007BC6
      A50039A5730010945A0000000000000000000000000000000000000000000000
      000000000000395A84002173AD00398CBD00397BAD00214A7300395A84000000
      000000000000000000000000000000000000000000000000000000000000395A
      84002173AD00398CBD00397BAD00214A7300395A840000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C68C
      DE00BD8CD600FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000D6EFDE0073C69C004AAD7B000094520000A5730000A56B00089C63000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DEE7EF0021528400185A8C001852840021527B00DEE7EF000000
      000000000000000000000000000000000000000000000000000000000000DEE7
      EF0021528400185A8C001852840021527B00DEE7EF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F7FFFF00C6C6
      C600C6C6C600C6C6C600F7FFFF000000000029527300294A7300294A73002952
      730029527300295273002952730029527300295273002952730029527300294A
      7300294A7300294A730029527300FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700CECE
      CE00CECECE00CECECE00E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7FFFF0042A584000084
      4200008442000084420042A58400F7FFFF006B849C00738CA50052738C004263
      840042638400426384004263840042638400426384004263840042638400526B
      8C00738CA500738CA5006B849C00FFFFFF00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E7006B73BD002131
      AD002131AD002131AD006B73BD00E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000042A5840000A5630000C6
      840084E7C60000C6840000A5630042A584007B94A500315A7B007B94AD005273
      8C004263840042638C0042638C0042638C00426B8C00426B8C00426384004263
      840052738C00295273007B94A500FFFFFF00AD948400A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00AD9484000000000000000000000000000000
      00000000000000000000000000000000000000000000636BBD002939C6003963
      FF003963FF003963FF002939C6006373BD00C6DEC600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000084420000C6840000C6
      84000000000000C6840000C68400008442002952730042637B0042638400426B
      8C004A6B8C00527B9C0073A5B500ADF7DE005A7B9C004A6B94004A6B94004A6B
      8C00426384004263840029527300FFFFFF00A58C7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A58C7B00DEDEDE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE001821B5004263FF003963
      FF00395AFF003963FF004263FF001831AD0084C6A50084C6840084C6840084C6
      840084C6840084C6840084C6840084C6A50084C6A5000084420084E7C6000000
      0000000000000000000084E7C60000844200294A73004A6B8C004A6B8C004A6B
      94004A739400ADEFDE009CD6D60094D6CE00ADEFDE0094CECE004A7394004A73
      94004A6B8C0052738C00294A7300FFFFFF00A58C7B00000000009C9C9C00ADB5
      B5009CA5A5008C8C9400A5A5A500A5A5A500A5A5A5009CA5A5009C9CA500A5AD
      AD00A5A5A5009C9C9C0000000000A58C7B008CBDA5006BB58C006BB58C006BB5
      8C006BB58C006BB58C006BB58C006BB58C0073BD8C001018AD00ADBDFF000000
      00000000000000000000ADBDFF001829AD0084A58400C6DEC60084C6C60084C6
      A50084C6C60084C6C60084C6C60084C6C600C6C6C6000084420000C6A50000C6
      A5000000000000C6A50000C6A50000844200294A6B006B8CA5004A6B94005273
      9C0052739C005A7B9C007B9CB5008CBDC600ADEFDE00ADE7DE0052739C005273
      9C004A7394007394AD00294A6B00FFFFFF00A58C7B0000000000000000000000
      00000000000000000000E7E7E70000000000E7E7E7000000000000000000FFFF
      FF00000000000000000000000000A58C7B006BAD8C00CEF7DE008CC6B50084BD
      A50094CEBD0094CEBD0094CEBD0094CEBD009CD6BD001018AD005A73FF005A73
      FF005273FF005A73FF005A7BFF001829AD0084A58400C6DEC60042A5840042A5
      6300C6DEC600C6DEC60084E7C60084C6A50084C6A50042A5630000A5630000E7
      A50084E7E70000E7A50000A5630042A5630029527300849CB5006384A5005A7B
      9C005A7BA500B5EFDE00B5EFE7009CCECE0084ADBD006B8CAD00637BA5005A7B
      9C006384A5007394AD004A6B8C00FFFFFF00A58C7B0000000000BD9C9400DECE
      CE00BD9C9C00DECECE00BD9C9C00DECECE00BD9C9C00DECECE00D6BDBD00B594
      8C00B5948C00D6BDBD0000000000A58C7B0063AD8400CEF7DE005AA57B004A94
      6B00BDEFCE00B5EFCE009CD6B5008CC6A5008CCEA5004263A5002939CE006B84
      FF00738CFF006B84FF003142CE00426B9C0084A58400C6DEC60084C6A500C6DE
      C600C6DEC600C6DEC60084A58400C6DEC600C6DEC60084C6A50042C684000084
      4200008442000084420042C6840084A58400E7EFEF0031527B00A5B5C6006B8C
      AD006384A500A5D6D600BDEFE700ADDEDE00B5E7DE009CCED6006384A500738C
      AD00A5B5CE00315A7B00E7EFEF00FFFFFF00A58C7B0000000000B5949400FFFF
      FF00BD949400F7FFF700BD949400F7FFF700BD949400F7F7EF00BD949400FFFF
      FF00FFFFFF00B594940000000000A58C7B0063A58400CEF7E70073B59400B5E7
      CE00B5E7CE00B5E7CE006BAD8C00B5E7C600B5E7C60073BD8C005A7BBD001018
      AD001018AD000818AD006B84C6006BB57B0042A58400F7FFFF0084A58400C6DE
      C60042A56300C6DEC60084A58400C6DEC600C6DEC60084A58400C6DEC60084A5
      8400C6DEC60084C6A500F7FFFF0084A58400FFFFFF00B5C6CE0031527B00ADBD
      CE007B94B500738CAD008CADC600B5DEDE009CB5CE00738CB5007B94B500ADBD
      D60031527B00B5C6CE00FFFFFF00FFFFFF00A58C7B0000000000BD949400F7FF
      FF00BD949400F7F7F700BD949400F7F7F700BD949400EFEFEF00BD949400F7FF
      FF00F7FFFF00B5948C00FFFFFF00A58C7B005AA57B00D6F7E7006BAD8C00ADE7
      CE0052946B00ADE7CE006BAD8C00B5E7C600B5E7C60073B58C00B5EFCE005AA5
      6B00B5F7C60073BD8C00D6FFE70063A57B0042A58400F7FFFF0084A58400C6DE
      C600C6DEC600C6DEC60084A58400C6DEC600C6DEC60084A58400C6DEC600C6DE
      C600C6DEC60084A58400F7FFFF0042A58400FFFFFF00FFFFFF00B5C6CE00315A
      7B00ADBDD6007B94B5008494BD007BADCE007384A50094D6EF00B5C6D600395A
      7B00B5C6CE00FFFFFF00FFFFFF00FFFFFF00A58C7B0000000000BD949400D6C6
      C600BD9C9400D6C6C600BD9C9400D6C6C600BD9C9400D6C6C600CEBDB500B594
      8C00B5948C00CEB5B500FFFFFF00A58C7B005AA57B00DEF7EF0063AD8400A5E7
      C600A5DEC600A5DEC60063A57B00B5E7C600B5E7C60063A57B00A5E7C600A5E7
      C600ADE7C6006BAD8400DEF7EF005AA57B0042A58400F7FFFF0042A5840042A5
      840084E7C60084E7C60084C6A50084C6A50084C6840084C6A50084E7C60084E7
      C60042A5840042A58400F7FFFF0042A58400FFFFFF00FFFFFF00FFFFFF00A5B5
      C6005A7B9C00C6CEDE00BDC6D600A5EFFF0084ADC600ADDEF7005A739400A5B5
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00A58C7B00FFFFFF00E7E7E700DED6
      D600E7E7E700DED6D600E7E7E700DED6D600E7E7E700DED6D600EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00FFFFFF00A58C7B005A9C7300DEF7EF00529C73005AA5
      7B009CDEC6009CDEC60084BDA5007BBD94007BB58C0084BDA5009CDEC6009CDE
      C6005AA57B00529C7300DEF7EF005A9C730042A58400F7FFFF00428463004284
      630042A5840042A5840042A5840042A5840042A5840042A5840042A5840042A5
      84004284630042846300F7FFFF0042A58400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00395A7B0042638400394A73007BB5CE009CDEF7005A84A5006B849C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A58C7B0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000A58C7B00529C7300E7FFF700428C5A00428C
      63005A9C73005A9C73005A9C73005A9C73005A9C73005A9C73005A9C73005A9C
      7300428C6300428C5A00E7FFF700529C730042A56300F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00F7FFFF0042A56300FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0073849C009CDEEF00A5E7F700A5EFFF00ADC6CE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6B5AD00A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C7B00A58C
      7B00A58C7B00A58C7B00A58C7B00C6B5AD0052946B00E7FFF700E7F7F700E7F7
      F700E7F7F700E7F7F700E7F7F700E7F7F700E7F7F700E7F7F700E7F7F700E7F7
      F700E7F7F700E7F7F700E7FFF70052946B0084C6A50084C6A500C6DEC600C6DE
      C600C6DEC600C6DEC600C6DEC600C6DEC600C6DEC600C6DEC600C6DEC600C6DE
      C600C6DEC600C6DEC60084C6A50084C6A500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0031527B005A7B9C008494B500637BA500637B8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008CBD9C008CC6A500B5E7CE00ADE7
      CE00ADE7CE00ADE7CE00ADE7CE00ADE7CE00ADE7CE00ADE7CE00ADE7CE00ADE7
      CE00ADE7CE00B5E7CE008CC6A5008CBD9C000000000084C6A50042A5630042A5
      630042A5630042A5630042A5630042A5630042A5630042A5630042A5630042A5
      630042A5630042A5630084C6A50000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CED6DE0094A5B5006B849C00395A840031527300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008CB59C0052946B004A94
      6B004A946B004A946B004A946B004A946B004A946B004A946B004A946B004A94
      6B004A946B0052946B008CB59C0000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00000000000000000000000000CECE
      CE00CECECE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECECE00CECE
      CE000000000000000000000000000000000000000000E7E7E700CECECE00CECE
      CE00CECECE00E7E7E70000000000000000003984AD003984AD003984AD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6AD9400AD521000A5520800AD52
      0800B54A0000298CEF00318CD6003984D6003984D6003984D6003984D6003984
      D6003984D6003984D6003984D600398CD600000000000000000000000000319C
      DE0039A5DE00D6D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000319CDE0039A5
      DE00D6D6D600000000000000000000000000E7E7E70052B58C00009C5A00009C
      5A00009C5A0052B58C00E7E7E70000000000397BAD006BB5D600397BAD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B55A1800DE9C5A00E7AD7300E7A5
      6B00EFA56300318CDE0042D6FF004ADEFF004ADEFF004ADEFF0042DEFF004ADE
      FF004ADEFF004ADEFF004AD6FF00398CDE0000000000000000000000000063B5
      E700319CDE0042A5DE00CECECE00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000063B5E700319C
      DE0042A5DE00CECECE00FFFFFF000000000052B58C0000A56B0000BD840073DE
      C60000BD840000A56B0052B58C0000000000397BAD006BB5D600317BAD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C67B3100EFC69400E7A56300E79C
      5A00EF9C5200B5947B003194E70052DEFF0052DEFF0052DEFF007342390052DE
      FF0052DEFF0052DEFF00399CE7007BA5B500000000000000000000000000B5DE
      F700319CDE0063C6EF00319CDE00BDC6CE00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000B5DEF700319C
      DE0063C6EF00319CDE00BDC6CE00FFFFFF00009C520000C68C0000BD84000000
      000000BD840000C68C00009C5A0000000000397BAD0063B5D600317BB500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7F700CECECE00000000000000
      000000000000DEDEDE00F7F7F700FFFFFF00CE8C4A00F7D6BD00F7B57300F7BD
      7B00FFFFFF00FFF7EF005A8CC6004AC6F7005AE7FF005AE7FF0063C6DE005AE7
      FF005AE7FF0052C6FF00398CC600318463000000000000000000000000000000
      0000319CDE0084D6F7008CE7FF00319CDE00BDC6CE00EFEFEF00000000000000
      000000000000000000000000000000000000000000000000000000000000319C
      DE0084D6F7008CE7FF00319CDE00CECECE00009C4A0073E7CE00000000000000
      00000000000073E7CE00009C5A0000000000317BAD0063B5D600217BBD00DEDE
      DE00F7F7F700FFFFFF00FFFFFF00FFFFFF00D6BD9C00E7942900E78C2100E78C
      2100E78C2100DEA55A00D6BDAD00E7E7E700D68C4200F7DEC600FFE7CE00FFFF
      EF00FFFFFF00FFF7F700E7DED600398CDE0063E7FF0063EFFF006B4A42006BEF
      FF0063E7FF003184D6006BBDA50031846B000000000000000000000000000000
      00006BB5E70052BDEF00ADF7FF007BDEFF00319CDE0094BDD600EFEFEF000000
      0000000000000000000000000000000000000000000000000000000000006BB5
      E70052BDEF00ADF7FF007BDEFF00429CE700009C4A0000CE940000CE8C000000
      000000CE8C0000CE9C00009C5A0000000000317BAD0063ADD600EF8C1800DEAD
      6300D6BDA500000000000000000000000000E7942900E78C2100E78C2100FFDE
      8C00FFDE8400FFBD5200EF9C3100DEAD6B00FFFFF700DEA56300EFBD8400ADC6
      CE005AA5C600397B9C0084848400ADA5A5004AB5EF0073F7FF006B4A420073F7
      FF004AB5EF00319CA50094DEBD00318473000000000000000000CECECE00CECE
      CE00A5BDD60042ADE700BDF7FF0084E7FF0073DEF700319CDE009CBDD600E7E7
      E7000000000000000000000000000000000000000000CECECE00CECECE00A5BD
      D60042ADE700BDF7FF0084E7FF007BDEFF00109C940000AD630000D69C0073EF
      D60000D69C0000AD73006BC6A50000000000317BAD005AADE700F78C1000FFA5
      2900EF9C2900E78C2100E78C2100E78C2100E78C2100EF942900DE8C1800FFDE
      8400FFD67B00FFC65A00FFCE7B00E78C2100FFFFFF00FFFFFF00FFEFE70094AD
      AD0063A5C600528CAD0063736B00ADCE9C00529CDE0073E7FF006B4A420073EF
      FF00529CD6009CE7BD009CDEC600318C7B000000000000000000399CDE004AAD
      E70042ADE70039A5E700BDF7FF007BE7FF007BE7FF0073DEF70042A5E70084B5
      D600DEDEDE0000000000000000000000000000000000399CDE004AADE70042AD
      E70039A5E700BDF7FF007BE7FF007BE7FF0073DEFF0018A59400009C4A00009C
      5200009C5A006BC6A5000000000000000000317BAD0052ADE700EF8C1000FFCE
      7300FFBD4A00FFC66300FFCE6B00FFCE7300FFD67300FFD68C00DE841800FFDE
      8400FFD67B00FFC65200FFD68C00E78C2100FFFFFF00FFFFFF00C6CECE007BBD
      E7007BC6E70073ADD6005A94BD0042948C004AA59C00429CE70084DEDE00429C
      E700528494006BBDA50052A59400F7FFFF000000000000000000319CDE009CF7
      FF0084E7FF0073E7FF006BDEF7006BDEF7006BDEF70073E7FF0073DEFF0042A5
      E7006BADD600DEDEDE00000000000000000000000000319CDE009CF7FF0084E7
      FF0073E7FF006BDEF7006BDEF7006BDEFF0073E7FF0073DEFF0042A5EF007BAD
      DE00DEDEDE00000000000000000000000000317BAD0052ADDE00EF8C1000FFD6
      8400FFB54200FFC65A00FFCE7300FFDE8C00FFD67300FFDE9400DE841000FFDE
      8400FFD67B00FFC65200FFDEA500E78C1800FFFFFF00FFFFFF0010396B00A5EF
      FF008CD6F7007BBDDE0073B5D60029427300F7FFF7004A8CBD004AA5E700428C
      C600427B8400DEEFEF00FFFFFF00FFFFFF0000000000000000005AA5DE0084DE
      F7008CE7FF006BDEF7006BDEF700BDF7FF00BDF7FF00B5F7FF00ADF7FF00ADF7
      FF0042ADE7006BADDE000000000000000000000000005AA5DE0084DEF7008CE7
      FF006BDEF7006BDEF700BDF7FF00BDF7FF00B5F7FF00ADF7FF00ADF7FF0042AD
      E7006BADDE00000000000000000000000000317BAD0052ADDE00EF8C0800FFDE
      9C00FFB54200FFC65A00FFCE6B00FFDE8400FFCE6B00FFDEA500DE841000FFDE
      8400FFD67300FFBD5200FFE7B500E78C1800FFFFFF00FFFFFF0008396B006BAD
      CE007BB5D6008CCEEF0084BDE70000215200D6D6D60084C6E70073B5DE0073B5
      D6005A9CBD00C6CECE00FFFFFF00FFFFFF0000000000000000009CC6EF006BC6
      EF009CEFFF005AD6F70063D6F70042B5E7003194DE003194DE003194DE00399C
      DE00399CDE00429CDE000000000000000000000000009CC6EF006BC6EF009CEF
      FF005AD6F70063D6F70042B5E7003194DE003194DE003194DE00399CDE00399C
      DE00429CDE00000000000000000000000000317BAD0052ADDE00EF8C0800FFDE
      AD00FFB53900FFC65A00FFCE6B00FFDE8400FFCE6B00FFE7B500DE841000FFE7
      AD00FFD67300FFBD4A00FFEFC600E78C1800FFFFFF00FFFFFF00104273002973
      AD004A8CB500527BA500314A7B0008295A0029527B00ADF7FF0094D6F70084BD
      DE0073B5D60018396B00FFFFFF00FFFFFF000000000000000000DEEFFF004AAD
      E700B5EFFF0063DEF70052D6F700ADEFFF0063B5E7008CB5D600FFFFFF000000
      00000000000000000000000000000000000000000000DEEFFF004AADE700B5EF
      FF0063DEF70052D6F700ADEFFF0063B5E7008CB5D600FFFFFF00000000000000
      000000000000000000000000000000000000317BAD0052ADDE00EF8C0800FFE7
      BD00FFB53900FFBD5200FFCE6B00FFDE8400FFCE6B00FFEFC600DE841000FFEF
      DE00FFF7E700FFF7E700FFF7E700E78C1800FFFFFF00FFFFFF0010427300297B
      AD00428CBD00427BAD00214A7300294A730008396B006BADCE007BB5D6008CCE
      EF0084C6E70008295A00FFFFFF00FFFFFF00000000000000000000000000399C
      DE00ADEFFF0084DEF7004ACEF70084DEF700ADE7FF003994DE00D6D6D6000000
      0000000000000000000000000000000000000000000000000000399CDE00ADEF
      FF0084DEF7004ACEF70084DEF700ADE7FF003994DE00D6D6D600000000000000
      000000000000000000000000000000000000317BAD005AADDE00F78C0800FFFF
      EF00FFDEA500FFD69400FFD68400FFDE9400FFD68400FFFFE700E7841000E78C
      2100E7A54A00EFA55200FFE7CE00E78C1800FFFFFF00FFFFFF00DEE7EF00104A
      7B00186394001852840018427300FFFFFF0010427300297BAD004A8CB500527B
      A50031527B0010316300FFFFFF00FFFFFF000000000000000000000000005AA5
      DE008CDEF700ADEFFF0039CEF70042CEF700BDF7FF006BBDE7005AA5D600F7F7
      F7000000000000000000000000000000000000000000000000005AA5DE008CDE
      F700ADEFFF0039CEF70042CEF700BDF7FF006BBDE7005AA5D600F7F7F7000000
      000000000000000000000000000000000000317BAD005AB5DE00F78C0800F7AD
      5200FFCE8400FFE7BD00FFFFE700FFFFEF00FFFFE700FFDEAD00E78C1800FFFF
      FF00FFEFD600F7DEB500EFAD5A00E78C2100FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F7F7F700FFFFFF00FFFFFF0010427300297BAD00428CBD00427B
      AD00214A730010316B00FFFFFF00FFFFFF000000000000000000000000009CC6
      E7006BC6EF00D6FFFF00CEF7FF00C6F7FF00D6FFFF00C6F7FF00429CDE00A5BD
      D6000000000000000000000000000000000000000000000000009CC6E7006BC6
      EF00D6FFFF00CEF7FF00C6F7FF00D6FFFF00C6F7FF00429CDE00A5BDD6000000
      00000000000000000000000000000000000073ADC600297BB5009CB5B500F7D6
      A500EFA54A00E78C1800E78C1800E78C1800E78C1800E78C2100F7C68C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7EF00104A7B00185A94001852
      840010427300DEE7EF00FFFFFF00FFFFFF00000000000000000000000000D6EF
      F700429CDE003994DE003994DE003994DE003994DE00429CDE00429CDE004A9C
      DE00000000000000000000000000000000000000000000000000D6EFF700429C
      DE003994DE003994DE003994DE003994DE00429CDE00429CDE004A9CDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700CECE
      CE00CECECE00CECECE00E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700CECE
      CE00CECECE00CECECE00E7E7E700000000000000000000000000CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE000000000000000000009CE700009CE7000094E7000094
      E7000094E700009CEF00009CEF00009CEF000094E7000094E7000094E7000094
      E7000094E7000094E700009CE700009CE70000000000CECECE00CECECE00EFEF
      EF00E7E7E700CECECE00CECECE00CECECE00CECECE00CECECE007384D600314A
      CE00394ACE00394ACE007384CE00E7E7E7000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDEDE0052B58C00009C
      5A00009C5A00009C5A0052B58C00E7E7E7000000000000000000947B63008C73
      52008C6B52008C6B52008C6B52008C6B52008C6B52008C6B52008C6B52008C6B
      52008C735200947B63000000000000000000089CE70039DEF70039DEF70031DE
      F70031E7F70039B5C6004A736B0031EFFF0031E7F70031DEF70031DEF70031DE
      F70031DEF70039DEF70039E7F700089CE7000000000042A5EF0042A5F7009CC6
      DE00C6849400AD425A00AD395A00AD395A00B53952006342A5003152DE00315A
      FF00315AFF00395AFF003952D6007384CE00DEDEDE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE004AAD840000A56B0000BD
      840073DEC60000BD840000A56B0052B58C0000000000000000008C7352000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C735200000000000000000084CEF70018B5EF0042DEF70031D6
      F70031D6F70039ADC60052211000426B6B0031DEFF0031D6F70031D6F70031D6
      F70031D6F70042DEF70018B5EF0084CEF7000000000042ADF70084EFFF00A54A
      6B00B5425A00BD525A00BD5A5A00BD5A5A00C65A5200214ADE003963FF003963
      FF00395AFF003963FF004263FF00314ACE00BD944A00B57B0800B57B0800B57B
      0800B57B0800B57B0800B57B0800BD7B0800CE7B0000009C5A0000C68C0000BD
      84000000000000BD840000C68C00009C5A0000000000000000008C6B52000000
      0000FFFFFF00FFFFFF0000000000AD732900AD73290000000000FFFFFF00FFFF
      FF00000000008C6B52000000000000000000FFFFFF000094E7004AD6F70039D6
      F70031D6F70031DEFF0031DEFF004A31210031B5CE0031D6FF0031D6F70031CE
      F70031D6F7004AD6F7000094E700FFFFFF0000000000B5DEEF00A54A6B00BD4A
      5200C65A6300D6737300E77B7B00E77B7B00EF8473002142D600A5BDFF000000
      00000000000000000000ADBDFF00314ACE00B57B0800F7FFFF00F7F7FF00F7F7
      FF00F7F7FF00F7F7FF00F7F7FF00F7F7FF00FFFFFF000094520073E7CE000000
      0000000000000000000073E7CE00009C5A0000000000000000008C6B52000000
      0000FFFFFF00FFFFFF00AD732900F7BD7300F7BD7300AD732900FFFFFF00FFFF
      FF00000000008C6B52000000000000000000FFFFFF0084CEF70018ADEF0052D6
      F70031C6EF0031A5C60031B5D60042636B004A31210031C6E70029CEFF0029CE
      F70052D6F70021ADEF0084CEF700FFFFFF0000000000C6849400BD4A5A00C66B
      6B00DE7B7B00E7CECE00EFFFFF009CADAD00F7FFFF001831CE005A73FF005273
      FF005273FF005273FF005A7BFF00314ACE00B57B0800F7FFFF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00000000000000000000944A0000CE940000CE
      8C000000000000CE8C0000CE9C00009C5A0000000000000000008C6B52000000
      0000F7FFFF00AD732900EFBD6B00EFBD7300EFBD7300EFBD6B00AD732900F7FF
      FF00000000008C6B52000000000000000000FFFFFF00FFFFFF00089CE70052CE
      F70042CEEF00423129004A2918004A2918004A2110004239310029BDEF0042CE
      F70052CEF700089CE700FFFFFF00FFFFFF0000000000B54A5A00C66B7B00DE7B
      7B00DECECE00DEFFFF00E7FFFF00E7FFFF00EFFFFF007B94E7003952DE006B84
      FF00738CFF006B8CFF00425ADE008494E700B57B0800F7FFFF00E7E7E7009C9C
      9C009C9C9C00EFEFE700C6BDBD0000000000000000006BCEA50000AD6B0000D6
      9C0073EFD60000D69C0000AD73006BC6A50000000000000000008C6B52000000
      0000AD732100F7C68400F7C68400EFC68400EFBD6B00F7CE8C00F7C68400AD73
      2100000000008C6B52000000000000000000FFFFFF00FFFFFF00ADDEF70018A5
      EF0073DEFF00398CAD0042211000396B7B0029D6FF0029CEFF0031C6F70073D6
      FF0018A5EF00ADDEF700FFFFFF00FFFFFF0000000000B5425A00D68C9400E784
      8400D6FFFF00DEFFFF00524A4200B5CED600DEFFFF00E7FFFF007394E7002139
      CE002142D600294AD6008C9CE70000000000B57B0800F7FFFF00E7E7E700E7E7
      E700E7E7E700E7E7E700A5A5A500A5A5A500A5A5A500B5A5AD00399C73000094
      4A0000944A00009C5200529439000000000000000000000000008C6B52000000
      0000C6A57300B5733100B57B3100EFCE9C00E7AD5A00B57B3900B57B3100C6A5
      7300000000008C6B52000000000000000000FFFFFF00FFFFFF00FFFFFF00089C
      E7005ACEF70063DEFF003939390042211000398CAD0031CEFF005ACEF70063CE
      F700089CE700FFFFFF00FFFFFF00FFFFFF0000000000B5425200DE9CA500EF84
      8400849C9C00D6FFFF00B5D6D6005A524A00ADC6CE00D6FFFF008C9C9C00FF84
      7B00EF9C9C00C64242000000000000000000B57B0800F7FFFF00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00E7DEDE00EFDEE700EFDE
      E700F7E7E70000000000CE7B08000000000000000000000000008C6B52000000
      0000EFEFF700EFF7FF00B5732900EFDEB500EFCEA500B57B3100EFF7FF00EFEF
      F700000000008C6B52000000000000000000FFFFFF00FFFFFF00FFFFFF00CEEF
      FF00089CEF009CE7FF004AA5CE0039180800392118004AADDE009CE7FF0010A5
      EF00CEEFFF00FFFFFF00FFFFFF00FFFFFF0000000000B5425200E7ADB500EF84
      8400C6FFFF00ADCED6005A525200ADCECE00CEF7FF00C6F7FF00C6FFFF00EF84
      8400EFADB500BD424A000000000000000000B57B0800F7FFFF00DED6DE00DEDE
      DE00DEDEDE00DED6D600EFEFEF0000000000000000000000000000000000F7EF
      EF00DED6DE00FFFFFF00BD7B08000000000000000000000000008C6B52000000
      0000E7E7E700E7E7EF00C6A57300B5732900B5732900C6A57300E7E7EF00E7E7
      E700000000008C6B52000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0029ADEF006BCEF70094E7FF0039394200310800004A31290063C6EF0029AD
      EF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DEDEDE00B54A5200E7A5AD00EF94
      9400D6CECE005A525200A5CECE00C6F7FF00BDF7FF00B5F7FF00CEC6C600EF94
      9400E7A5AD00B54A5200DEDEDE0000000000B57B0800F7FFFF00D6D6D600948C
      8C0094949400D6D6D600C6C6BD0000000000000000000000000000000000C6C6
      BD00D6D6D600F7FFFF00B57B08000000000000000000000000008C6B52000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C6B52000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CEEFFF0008A5E700B5E7FF0073D6FF0073D6FF00C6F7FF0010A5EF00CEEF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0073B5EF007B7BA500CE6B7300F7BD
      CE00F78C8C00CEC6CE00B5F7FF0063848C00ADF7FF00CEBDC600EF848400F7BD
      CE00CE6B73007B7BA50073B5EF0000000000B57B0800F7FFFF00CECECE00D6CE
      CE00D6CECE00D6CECE00A5A5A5009CA5A5009C9CA5009C9CA5009CA5A5009CA5
      A500CECECE00F7FFFF00B57B0800000000000000000000000000BD8C4A00C68C
      3900BD843900BD843900BD843900BD843900BD843900BD843900BD843900BD84
      3900C68C3900B5844A000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF005ABDEF005AC6F700BDE7FF00B5E7FF006BC6F7005ABDEF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004AADF70052C6FF00AD5A6300DE8C
      9400FFCEDE00F7A5A500F7848400F7848400F7848400F79CA500FFCEDE00DE8C
      9400AD5A630052C6FF004AADF70000000000B57B0800F7FFFF00F7F7FF00F7F7
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00B57B0800000000000000000000000000C6944200F7D6
      9C00EFBD6B00EFBD7300EFBD7300EFBD7300EFBD7300EFBD7300EFBD7300EFBD
      6B00F7D69C00C69442000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F7FFFF00009CE700CEEFFF00CEEFFF00009CE700F7FFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0052B5F70063D6FF006BD6FF00AD63
      6300D6737B00FFCED600FFE7FF00FFE7FF00FFE7FF00FFCED600D6737B00AD63
      63006BD6FF0063D6FF0052B5F70000000000B57B1000F7E7C600DEAD4A00DEAD
      4A00DEAD4A00DEAD4A00DEAD4A00DEAD4A00DEAD4A00DEAD4A00DEAD4A00DEAD
      4A00DEAD4A00F7E7C600B57B1000000000000000000000000000C6944A00E7C6
      9400D6A55200D6A55200D6A55200D6A55200D6A55200D6A55200D6A55200D6A5
      5200E7C69400C6944A000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0052BDEF0073C6F70073C6F70052BDEF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6E7FF005AC6F7007BF7FF007BFF
      FF00848CA500B5525A00B5525200B5525200B5525200B5525A00848CA5007BFF
      FF007BF7FF005AC6F700C6E7FF0000000000BD841000EFD6A500EFCE9C00EFCE
      9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE
      9C00EFCE9C00EFD6A500BD841000000000000000000000000000C6944A00DEBD
      8C00DEBD8C00DEBD8C00DEBD8C00DEBD8C00DEBD8C00DEBD8C00DEBD8C00DEBD
      8C00DEBD8C00C6944A000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00009CE700009CE700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6E7FF0052BDF7004AC6
      FF0084DEFF00000000000000000000000000000000000000000084DEFF004AC6
      FF0052BDF700C6E7FF000000000000000000CEAD6300BD841000B5841000B584
      1000B5841000B5841000B5841000B5841000B5841000B5841000B5841000B584
      1000B5841000BD841000CEAD6300000000000000000000000000CE9C5200C694
      4A00C6944A00C6944A00C6944A00C6944A00C6944A00C6944A00C6944A00C694
      4A00C6944A00CE9C520000000000000000000000000000000000000000000000
      00000000000000000000CECECE00CECECE00CECECE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEDEDE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00D6D6D600000000000000000000000000000000000000
      000000000000000000005A6B7B004A6384005294DE00CECECE00000000000000
      0000000000000000000000000000000000002994730029947300299473002994
      6B0018946B00A5846B00A5846B00A5846B00A5846B00A5846B0018946B002994
      6B00299473002994730029947300FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B5B5B5009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C00ADADAD000000000000000000FFFFFF00EFEFEF00CECE
      CE00CECECE00CECECE005A84A50084A5B50094D6FF00316B9C00CECECE00CECE
      CE00EFEFEF00FFFFFF0000000000000000002984630029B5840029B5840021AD
      7B00107B5200FFFFEF00217BC600398CCE00217BC600FFFFEF00107B520021AD
      7B0029B5840029AD7B0029846300FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF009C9C9C000000000000000000F7F7F700B5B5B5008C8C
      8C00848484008C84840042B5F7008CE7FF0084D6FF00109CFF00316B9C009C94
      8400B5B5B500F7F7F7000000000000000000EFF7EF00527B5A00108C5A006394
      7B00DED6C600FFF7E700006BB500428CCE00006BB500FFF7E700DED6C6006394
      7300108C5A00527B5A00EFF7EF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C9C9C00F7F7F700F7F7
      F7008C8C8C006B6B6B00F7F7F7008C8C8C006B6B6B00F7F7FF00E79C5200E79C
      5200EFF7F700F7F7F7009C9C9C000000000000000000D6D6D6009C9C9C00F7F7
      EF00F7EFEF00DEDED600EFDED6002173CE0042C6FF0029ADFF00109CFF00316B
      9C00ADA59C00D6D6D6000000000000000000FFFFFF00AD8C7300FFF7EF00FFEF
      DE00FFEFDE00FFF7DE000052AD00398CCE000052AD00FFF7DE00FFEFDE00FFEF
      DE00FFF7EF00AD8C7300FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C9C9C00F7F7F700EFEF
      EF00F7F7F700F7F7F700F7F7EF00F7F7F700F7F7F700EFF7F700EFAD6300E7A5
      5A00EFEFF700F7F7F7009C9C9C000000000000000000BDBDBD00ADADAD00EFEF
      EF008C848400DED6D6008C8C8400EFDECE002973C60042CEFF0029ADFF00109C
      FF00296BA500B5B5B5000000000000000000FFFFFF00A58C7300FFF7EF00F7E7
      D600F7EFDE0094846300A58C6B00A58C6B00A58C6B0094846300F7EFDE00F7E7
      D600FFF7EF00A58C7300FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000CECECE00CECECE00F7F7F700000000000000
      000000000000000000000000000000000000000000009C9C9C00F7F7F700EFEF
      E7008C8C8C006B6B6B00EFEFEF008C8C8C006B6B6B00EFEFF700F7AD6300F7AD
      6300E7EFEF00F7F7F7009C9C9C000000000000000000ADADAD00BDBDB500DED6
      D600D6CECE008C8C8C00D6D6D600948C8C00E7D6CE00297BCE0042C6FF0021AD
      FF0084B5D6007B7B7300CECECE0000000000FFFFFF009C8C7300FFF7EF00C6B5
      A500CEBDA500CEBDAD00D6C6AD00D6C6AD00D6C6AD00CEBDAD00CEBDA500C6B5
      A500FFF7EF009C8C7300FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000CECECE006363630063636300BDBDBD00000000000000
      000000000000000000000000000000000000000000009C9C9C00F7EFEF00E7E7
      E700EFE7E700EFEFEF00EFE7E700EFE7EF00EFEFEF00E7E7E700E7E7EF00E7E7
      EF00E7E7E700F7EFEF009C9C9C00000000000000000094949400C6C6C600CECE
      CE0094948C00CECECE0094948C00D6CECE0094948C00DECEC6002173CE00ADDE
      F700948C8400C6BDBD00737B6B00CECECE00FFFFFF00A58C7300FFF7EF00F7E7
      D600CEBDA500F7E7DE00CEBDA500F7E7DE00CEBDA500F7E7DE00CEBDA500F7E7
      D600FFF7EF00A58C7300FFFFFF00FFFFFF000000000000000000000000000000
      000000000000CECECE0063636300ADADAD00ADADAD0063636300CECECE000000
      000000000000000000000000000000000000000000009C9C9C00EFEFEF00E7DE
      DE00948C8C00736B6B00E7E7E700948C8C00736B6B00E7E7E700948C8C00736B
      6B00E7DEDE00EFEFEF009C9C9C0000000000000000008C8C8C00CECEC6009494
      9400CEC6C60094949400CEC6C60094949400CEC6C6009C949400CECEC6008484
      7B00EFEFE7008C8C8400BD7BB5009C6BCE00FFFFFF00A58C7B00FFF7EF00C6B5
      A500CEBDA500CEBDA500CEBDA500CEBDA500CEBDA500CEBDA500CEBDA500C6B5
      A500FFF7EF00A58C7B00FFFFFF00FFFFFF000000000000000000000000000000
      0000CECECE0063636300C6BDBD00B5B5B500B5B5B500C6BDBD0063636300CECE
      CE0000000000000000000000000000000000000000009C9C9C00EFEFEF00DEDE
      D600E7DED600E7DED600E7DEDE00E7DEDE00E7DEDE00E7DEDE00E7DED600E7DE
      D600DEDED600EFEFEF009C9C9C0000000000D6D6D60084848400ADA5A500A5A5
      A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500ADA5A500ADA5
      A5007B847B00E7B5E700CE94CE00B57BD600FFFFFF00A5947B00FFF7EF00EFDE
      CE00CEB5A500F7E7D600CEBDA500F7E7D600CEBDA500F7E7D600CEB5A500EFDE
      CE00FFF7EF00A5947B00FFFFFF00FFFFFF000000000000000000000000000000
      000063636300DEDEDE00EFE7E700E7E7E700E7E7E700EFE7E700DEDEDE006363
      630000000000000000000000000000000000000000009C9C9C00EFEFEF00DED6
      CE00187BFF002163FF00DEDED600948C8C00736B6B00DEDED6002184FF002163
      FF00DED6CE00EFEFEF009C9C9C00000000008C8C8C00EFEFEF00DEDEDE00DEDE
      E700DEDEDE00DEDEDE00DEDED600DEDED600DEDED600DEDEDE00DEDEDE00DEDE
      E700DEE7DE00BD84DE00C684DE0000000000FFFFFF00A5947B00FFFFF700C6B5
      9C00CEB5A500CEB5A500CEB5A500CEB5A500CEB5A500CEB5A500CEB5A500C6B5
      9C00FFFFF700A5947B00FFFFFF00FFFFFF000000000000000000000000000000
      00008C8C8C006363630063636300636363006363630063636300636363008C8C
      8C000000000000000000000000000000000000000000A5A5A500F7F7F700F7EF
      EF00F7F7EF00F7F7EF00F7F7EF00F7F7F700F7F7F700F7F7EF00F7F7EF00F7F7
      EF00F7F7EF00F7F7F700A5A5A5000000000084848400E7EFEF00D6AD6300C66B
      0000C6C6D600C6C6C600C6C6BD00C6C6BD00C6C6BD00C6C6C600C6C6D600C66B
      0000D6AD6300E7EFEF00848C840000000000FFFFFF00AD947B00FFFFF700EFDE
      CE00CEB5A500EFDECE00CEB5A500EFDECE00CEB5A500EFDECE00CEB5A500EFDE
      CE00FFFFF700AD947B00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5A5A5007B7B7B007B7B
      84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B84007B7B
      84007B7B84007B7B7B00A5A5A50000000000ADADAD00E7EFEF00FFEFAD00DE9C
      3100C6730000DEE7F700E7E7E700E7E7E700E7E7E700DEE7F700C6730000DE9C
      3100FFEFAD00E7EFEF009494940000000000FFFFFF00AD948400FFFFF700C6B5
      9C00C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5A500C6B5
      9C00FFFFF700AD948400FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5A5A500FFCE8C00FFC6
      7B00FFC67B00FFC68400FFC68400FFC68400FFC68400FFC68400FFC68400FFC6
      7B00FFC67B00FFCE8C00A5A5A5000000000000000000ADADAD007B848C00FFEF
      B500DE9C3100C67300007B84940084848C007B849400C6730000DE9C3100FFEF
      B5007B848C00ADADB5000000000000000000FFFFFF00AD9C8400FFFFFF00E7D6
      C600C6B59C00EFD6C600C6B59C00EFD6C600C6B59C00EFD6C600C6B59C00E7D6
      C600FFFFFF00AD9C8400FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009CA5A500FFD6A500EFA5
      6300E7A56300E7AD6300E7AD6300E7AD6300E7AD6300E7AD6300E7AD6300E7A5
      6300EFA56300FFD6A5009CA5A500000000000000000000000000000000000000
      0000FFEFB500DE9C3100CE73000000000000CE730000DE9C3100FFEFB5000000
      000000000000000000000000000000000000FFFFFF00B59C8400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00B59C8400FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009CA5A500FFDEB500FFDE
      BD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDEBD00FFDE
      BD00FFDEBD00FFDEB5009CA5A500000000000000000000000000000000000000
      000000000000FFEFB500DE9C390000000000DE9C3900FFEFB500000000000000
      000000000000000000000000000000000000FFFFFF00CEC6B500B59C8C00B59C
      8C00B59C8400B59C8400B59C8400B59C8400B59C8400B59C8400B59C8400B59C
      8C00B59C8C00CEC6B500FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADADAD009CA5A5009CA5
      A5009CA5A5009CA5A5009CA5A5009CA5A5009CA5A5009CA5A5009CA5A5009CA5
      A5009CA5A5009CA5A500ADADAD000000000000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF00000000000000000000000000FFFFFF000000000000000000DEDEDE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00DEDEDE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6AD9400AD520800A5520800A552
      0800A5520800A5520800A5520800A5520800AD520800C6AD9400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00947B6300947B6300FFFFFF00947B
      6300947B6300FFFFFF00947B6300947B6300FFFFFF00947B6300947B6300FFFF
      FF00947B6300947B6300947B6300FFFFFF000000000000000000BDBDBD00B5B5
      AD00B5B5AD00B5B5AD00B5B5AD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00ADADAD00ADADAD00BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B55A1800DE9C5A00E7AD7300E7AD
      7300E7A57300E7A57300E7AD7300E7AD7300DE9C5A00B5631800FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000B5B5AD000000
      000000000000848CF70000000000000000000000000000000000000000000000
      00000000000000000000B5B5AD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C67B3100EFC69400E7A56300E79C
      5A00E79C5A00E79C5A00E79C5A00E7A56300EFC69C00BD6B2100FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000ADADAD000000
      0000000000003139E70000000000000000000000000000000000000000000000
      00000000000000000000ADADAD0000000000000000000000000000000000CECE
      CE00DEDEDE000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE8C4A00F7D6BD00F7B57300F7BD
      7B00F7FFFF00EFEFF700F7BD7B00F7BD7B00F7DEC600C6732900FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADB5AD00E7D6
      BD00D6B58C002121BD00D6B58C00C6AD8C00C6AD8C00C6AD8C00C6AD8C00C6AD
      8C00C6AD8C00E7D6C600ADB5AD00000000000000000000000000CECECE00008C
      4A00299C6B000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D68C4200F7DEC600FFE7CE00FFFF
      EF00FFFFFF00F7F7F700F7E7D600EFCEA500EFD6BD00D6844200FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADB5AD000000
      0000000000003942E7000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000ADB5AD000000000000000000CECECE000084420052DE
      B500008C4200CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00DEDEDE00FFFFF700E7A56300EFBD8400ADC6
      CE005A9CC600397BA500738C8C00BD7B3900BD844A00CEB5A500EFEFEF00F7F7
      F700FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000ADB5AD00E7D6
      C600D6B58C002121BD00D6BD8C00C6AD9400C6AD9400C6AD9400C6AD9400C6AD
      8C00C6AD8C00E7D6C600ADB5AD0000000000CECECE000084420063D6B50000DE
      A50000BD7B0000844200008C4200008C4200008C4200008C4200008C4200008C
      4200008C4200008C4200008C4A0039A57300FFFFFF00FFFFFF00FFEFE70094AD
      AD0063A5C600528CAD0073848400D6AD8400FFFFE700CEAD7B00C6AD9400CEBD
      B500CECECE00CECECE00EFEFEF00FFFFFF00947B630000000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF000000000000000000947B6300FFFFFF000000000000000000ADB5AD000000
      0000FFFFFF003142E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000ADB5AD0000000000008C4A0073DEBD0000CE9C0000CE
      9C0000CE9C0000CE9C0000D6A50000D6A50000D6A50000D6A50000D6A50000D6
      A50000D6A50000D6A50000D6A500008C4A00FFFFFF00FFFFFF00C6CECE007BBD
      E7007BC6E7006BADD6004A94C600ADA59400D6B58400FFFFFF00D6AD8400CEA5
      7300B5844200BD844A00C6B59C00EFEFEF00947B6300947B6300FFFFFF00947B
      6300947B6300FFFFFF00947B6300947B6300FFFFFF00947B6300947B6300FFFF
      FF00947B6300947B6300947B6300FFFFFF000000000000000000ADB5AD00E7D6
      C600D6B58C002121BD00D6BD9400C6AD9400C6AD9400C6AD9400C6AD9400C6AD
      9400C6AD8C00E7D6C600ADB5AD0000000000008C4A0084DECE0000C69C0000C6
      9C0063DEC60063E7CE0063E7CE0063E7CE0063E7CE0063E7CE0063E7CE0063E7
      CE0063E7CE0063E7CE0063E7D600008C4A00FFFFFF00FFFFFF0010396B00A5EF
      FF008CD6F7007BBDE7008C9C9C00CE9C5A00F7DEBD00FFFFE700FFFFE700FFFF
      EF00FFFFEF00F7E7C600CE9C6B00C6B5A50000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000ADB5AD000000
      0000FFFFF7003139E700FFFFFF00F7FFFF00F7F7FF00F7F7FF00F7F7FF00F7F7
      FF00F7F7F70000000000ADB5AD000000000000000000008442009CE7D60000C6
      9C0000AD7B000084420000844200008C4200008C4200008C4200008C4200008C
      4200008C4200008C4200008C42004AB58400FFFFFF00FFFFFF0008396B006BAD
      CE007BB5DE0084CEF700C68C4A00FFE7CE00FFF7DE00FFF7DE00FFF7DE00FFF7
      D600FFF7D600FFF7DE00FFEFCE00BD8C5200947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADB5AD00E7D6
      C600D6B58C002121BD00D6BD9400CEAD9400C6AD9400C6AD9400C6AD9400C6AD
      9400C6AD8C00E7D6C600ADB5AD0000000000000000000000000000844200A5E7
      DE00008C42000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00104273002973
      AD004A8CBD004273AD00CE8C4200FFFFE700FFEFCE00FFEFD600FFEFD600FFEF
      D600FFEFCE00FFEFCE00FFFFE700C68C4A00947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADB5AD000000
      0000FFFFEF003139DE00FFFFF700F7F7F700EFF7F700EFF7F700EFF7F700EFEF
      F700EFEFF70000000000ADB5AD0000000000000000000000000000000000008C
      4A00219C63000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0010427300297B
      AD00398CBD002973AD00D6944A00FFF7E700FFE7C600FFEFCE00FFEFCE00FFEF
      CE00FFEFCE00FFE7C600FFF7E700C68C520000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000ADB5AD00E7D6
      C600CEB58C002121BD00D6B58C00C6AD9400C6AD9400C6AD9400C6AD9400C6AD
      9400C6AD8C00E7D6C600ADB5AD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00E7E7EF00104A
      7B00185A9400084A8400CE8C5200FFEFCE00FFEFCE00FFE7C600FFE7C600FFE7
      C600FFE7C600FFEFC600FFE7CE00CE8C5200947B6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00947B6300FFFFFF000000000000000000ADADAD000000
      0000EFEFE7002931D600EFEFE700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000ADADAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F7DECE00DEA57300F7E7CE00FFF7E700FFF7E700FFF7
      E700FFF7E700F7E7CE00D6A57300EFDEC6000000000000000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF000000000000000000947B6300FFFFFF000000000000000000B5B5AD000000
      0000000000008C8CF70000000000000000000000000000000000000000000000
      00000000000000000000B5B5AD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7DEC600D69C6300CE945A00CE945A00CE94
      5A00CE945A00D69C6300EFDEC600FFFFFF00947B6300947B6300FFFFFF00947B
      6300947B6300FFFFFF00947B6300947B6300FFFFFF00947B6300947B6300FFFF
      FF00947B6300947B6300FFFFFF00FFFFFF000000000000000000BDBDB500B5B5
      AD00B5B5AD00B5B5AD00B5B5AD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00ADADAD00B5B5AD00BDBDB500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700CECE
      CE00CECECE00CECECE00E7E7E7000000000000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE0000000000FFFFFF00FFFFFF0000000000CECECE00CECE
      CE00CECECE00CECECE00CECECE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00D6D6D600FFFFFF0000000000F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700DEDEDE0052B58C00009C
      5A00009C5A00009C5A0052B58C00E7E7E7009494940073737300736B6B006B6B
      6B006B6363006363630084848400FFFFFF00FFFFFF009494940073737300736B
      6B006B6B6B006B6363006363630084848400CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE0063A5CE003994CE00398CCE003994
      CE003994D6003194D6005A6B73004A638400528CD6003194D6003194CE00318C
      CE00318CCE00318CCE004A9CD600FFFFFF00D6D6D600CEC6BD00CEC6BD00CEC6
      BD00CEC6BD00CEC6BD00CEC6BD00CEC6BD00D6C6BD004AB58C0000A56B0000BD
      840073DEC60000BD840000A56B0052B58C007B7B7300847B7B009C9C94008C84
      8400736B6B005A525200635A5A00FFFFFF00FFFFFF007B7B7300847B7B009C9C
      94008C848400736B6B005A525200635A5A000808DE000000DE000000DE000000
      DE000000DE000000DE000000DE000000DE000000DE000000DE000000DE000000
      DE000000DE000000DE000000DE000808DE003994CE0073DEFF005AD6FF004ACE
      F70039ADE70052B5EF0063849C0084A5B5008CD6FF003163940084EFFF007BE7
      FF007BE7FF0084E7FF00318CCE00FFFFFF00CEA57300CEA56300CE9C6300CE9C
      6300CE9C6300CE9C6300CE9C6300CE9C6300E7A56300009C5A0000BD8C0000BD
      84000000000000BD840000C68C00009C5A0073737300CECECE00DED6D600D6CE
      CE00CECEC600C6C6C6005A5A5A00FFFFFF00FFFFFF0073737300D6CECE00DED6
      D600D6CECE00CECEC600C6C6C6005A5A5A00DEDEB500FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFF700DEDEB500398CCE005AD6FF004AC6F70039AD
      E70039BDF700318CCE004AB5EF008CE7FF0084D6FF00109CFF00316394007BE7
      FF0073DEFF0084E7FF00318CCE00FFFFFF00CEA56300FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFF7000094520073E7CE000000
      0000000000000000000073E7CE00009C5A0073737300B5B5AD00CECEC600B5AD
      AD0094948C00737373005A5A5A00CECECE00CECECE0073737300B5B5AD00CECE
      C600B5ADAD0094948C00737373005A5A5A000000E700184AFF000031FF000031
      FF000031FF000031FF000031FF000031FF000031FF000031FF000031FF000031
      FF000031FF000031FF00184AFF000000E7003994CE004ACEF70039ADE70039BD
      EF0042D6FF00318CCE0073E7FF003173C60042C6FF0029ADFF00189CFF003163
      8C0073E7FF0084E7FF00318CCE00FFFFFF00CE9C6300FFFFEF00E7CEA500F7E7
      C600FFF7DE00FFF7DE00FFF7DE00FFF7DE00FFFFE70000944A0000CE940000C6
      8C000000000000CE8C0000CE9C00009C5A0073737300B5B5AD00CECEC600B5AD
      AD0094949400737373005A5A5A007B7B73006363630073737300B5B5AD00CECE
      C600B5ADAD009494940073737300635A5A00DEDEB500FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00DED6B5003994CE0039ADE70039BDF70042D6
      FF0063DEFF003184CE006BDEFF006BDEFF003173BD004AC6FF0029ADFF00109C
      FF00316394008CEFFF003194D600FFFFFF00CE9C6300FFFFEF00FFFFEF00DEB5
      8400E7C69400FFF7DE00FFF7DE00FFF7DE00FFFFE7006BC6940000AD6B0000D6
      9C0073EFD60000D69C0000AD6B005AA5630073737300B5B5AD00CECEC600B5AD
      AD0094948C00737373005A5A520073737300635A5A0073737300B5B5AD00CECE
      C600B5ADAD0094948C00737373005A5A5A000000F7003963FF000021FF000021
      FF000021FF000021FF000021FF000021FF000021FF000021FF000021FF000021
      FF000021FF000021FF003963FF000000E7003994CE005AB5E700318CCE00318C
      CE003184CE0042A5DE0063DEFF0063D6FF0063DEFF003173BD0042C6FF0021AD
      FF0084ADD600847363002994DE00FFFFFF00CE9C6300FFFFEF00FFEFD600FFFF
      F700DEB58400FFF7DE00DEBD8C00C6945200CE945A00F7BD94006BC694000094
      520000944A00009452006BC69C00E7A56B0073737300DEDEDE00EFEFEF00DEDE
      DE00CECECE00BDBDBD0052525200D6D6D6008C8C8C0073737300DEDEDE00EFEF
      EF00DEDEDE00CECECE00BDBDBD005A5A5A00A55A0000FFFFE700E76B0000FFF7
      C600E76B0000FFF7C600E76B0000FFF7C600EF6B0000FFFFF700FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00DED6B5003194CE005AADE70063DEFF0063DE
      FF0063DEFF0063D6FF005AD6FF005AD6FF0063D6FF0063DEFF002973C600B5DE
      EF00948C7B00C6BDB5007B7B6B00CECECE00CE9C6300FFFFEF00FFEFCE00FFEF
      CE00FFFFFF00E7C69400DEBD8C000000000000000000E7BD8C00F7C69C000000
      0000FFF7D600FFF7D600FFFFEF00D6A563007B7B73006B6B6B0063636300635A
      5A005A5A5A005A5A5A00948C8C00D6D6D6008C8C8400A59C9C006B6B6B006363
      6300635A5A00635A5A00635A5A0094949400945A0000EFA55200FFE7CE00CE5A
      0000FFEFD600CE5A0000FFEFD600CE5A0000FFEFC6000010FF000010FF000010
      FF000010FF000010FF006373FF000000E700318CCE0094EFFF0052D6FF005AD6
      FF005AD6FF005AD6FF005AD6FF005AD6FF005AD6FF005AD6FF0052DEFF00947B
      7300EFE7E7008C8C8400BD7BB5009C6BCE00CE9C6300FFFFEF00FFEFC600FFEF
      C600EFCE9C00DEB5840000000000FFEFCE00FFEFCE0000000000DEB58400EFCE
      9C00FFEFCE00FFEFC600FFFFEF00CE9C6300FFFFFF0073737300BDBDBD00CECE
      CE00A5A5A5007B7B7B005A525200D6D6D6008C8C8C0073737300BDBDB500CECE
      CE00A5A5A5007B7B7B005A5A5A00FFFFFF0094520000FFFFFF00BD520000F7E7
      D600BD520000F7E7D600BD520000F7E7D600C6520000FFFFFF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00D6D6B500318CCE009CEFFF004AD6FF0052D6
      FF0052D6FF0052D6FF0052D6FF0052D6FF0052D6FF0052D6FF0052D6FF004AD6
      FF008C7B7300E7B5DE00CE94C600B57BCE00CE9C6300FFFFEF00FFEFC600F7DE
      AD00DEBD8400FFFFFF00FFEFC600FFEFC600FFEFC600FFEFC600FFFFFF00DEBD
      8400F7DEAD00FFEFC600FFFFEF00CE9C6300FFFFFF0073737300BDB5B500CEC6
      C600A5A59C007B7B7B005A5A5A0073737300635A5A0073737300BDBDB500CEC6
      C600A5A59C007B7B7B00635A5A00FFFFFF0094520000E7B58400F7E7CE00B54A
      0000F7E7D600B54A0000F7E7D600B54A0000FFE7C6000000FF000008FF000008
      FF000008FF000000FF00848CFF000000E700318CCE009CEFFF0042CEFF004ACE
      FF004ACEFF004ACEFF004ACEFF004ACEFF004ACEFF004ACEFF004ACEFF004AD6
      FF0039D6FF00CE84CE00CE84D600FFFFFF00CE9C6300FFFFF700FFE7B500DEB5
      8400FFFFEF00FFE7BD00FFE7BD00FFE7BD00FFE7BD00FFE7BD00FFE7BD00FFFF
      EF00DEB58400FFE7B500FFFFF700CE9C6300FFFFFF007B737300BDBDBD00CECE
      CE00A5A5A5007B7B7B005A5A5A007B7B7300636363007B737300BDBDBD00CECE
      CE00A5A5A500847B7B00635A5A00FFFFFF009452000000000000A5420000F7DE
      CE00A5420000F7DECE00A5420000F7DECE00AD420000FFFFFF00FFFFEF00FFFF
      EF00FFFFEF00FFFFEF00FFFFEF00DEDEB500318CCE00A5EFFF008CE7FF008CE7
      FF0094E7FF0094E7FF0094E7FF0094E7FF0094E7FF0094E7FF0094E7FF008CE7
      FF008CE7FF009CF7FF00298CCE00FFFFFF00CE9C6300FFFFF700E7BD8C00FFF7
      E700FFE7B500FFE7B500FFE7B500FFE7B500FFE7B500FFE7B500FFE7B500FFE7
      B500FFF7E700E7BD8C00FFFFF700CE9C6300FFFFFF00A5A5A500736B6B006B63
      630063635A00635A5A008C8C8C00FFFFFF00FFFFFF009C9C9C006B6B6B006B63
      6300636363006363630094949400FFFFFF00945A0000E7C6AD0000000000E7C6
      A50000000000E7C6A50000000000E7C6A500FFFFFF009CADFF00A5ADFF00A5AD
      FF00A5ADFF00ADADFF00ADB5FF000000DE00318CCE00ADEFFF0031B5EF0039B5
      EF0039B5EF0039B5EF0039B5EF0039B5EF0039B5EF0039B5EF0039B5EF0039B5
      EF0031B5EF00ADF7FF00318CCE00FFFFFF00CEA56300FFFFFF00FFFFFF00FFFF
      F700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFF
      F700FFFFF700FFFFFF00FFFFFF00CEA56300FFFFFF00FFFFFF00CECECE007B73
      7300CECEC6009C9C9C00635A5A00FFFFFF00FFFFFF007B737300CECEC6009C94
      9400635A5A00CECECE00FFFFFF00FFFFFF00BD944A00945A0000945200009452
      0000945200009452000094520000945A0000A55A00000000EF000000DE000000
      DE000000DE000000DE000000DE003939E700318CCE00B5F7FF0031CEFF0039CE
      FF0039CEFF0039CEFF0039CEFF0039CEFF0039CEFF0039CEFF0039CEFF0039CE
      FF0031CEFF00B5F7FF00318CCE00FFFFFF00DEC69C00CEA56300CE9C6300CE9C
      6300CE9C6300CE9C6300CE9C6300CE9C6300CE9C6300CE9C6300CE9C6300CE9C
      6300CE9C6300CE9C6300CEA56300D6B58400FFFFFF00FFFFFF007B7B7300948C
      8C00948C8C0063636300635A5A00FFFFFF00FFFFFF007B7373008C8C8C00948C
      8C006B63630063635A00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000318CCE0084EFFF0084E7FF0084E7
      FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7
      FF0084E7FF0084EFFF00318CCE00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF007B7B73007373
      6B006B6B6B006B636300635A5A00FFFFFF00FFFFFF007B7B730073736B006B6B
      6B006B63630063635A00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000429CD600318CCE00318CCE00318C
      CE00318CCE00318CCE00318CCE00318CCE00318CCE00318CCE00318CCE00318C
      CE00318CCE00318CCE00429CD600FFFFFF000000000000000000000000000000
      00000000000000000000D6D6D600BDBDBD00BDBDBD00D6D6D600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBDBD00BDBDBD00BDBDBD00F7F7F70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DEDEDE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00DEDEDE0000000000D6D6D600BDBDBD00BDBD
      BD00BDBDBD00BDBDBD009C9C9C00848484008C8C8C009C9C9C00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00D6D6D6000000000000000000EFEFEF00C6C6C600BDBD
      BD00BDBDBD005A5A5A00525252008C8C8C00B5B5B500C6C6C600EFEFEF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDEDE00CECECE00CECECE00CECE
      CE00CECECE00CECECE005A73A500214A8400294A8400294A8400294A8400294A
      8400294A8400294A8400294A84005A739C0000000000848484006B6B6B006B6B
      6B006B6B6B006B6B6B0094949400ADADAD00CECECE00BDBDBD00949494006B6B
      6B006B6B6B006B6B6B0084848400000000000000000094949400424242003939
      3900393939007B7B7B008C8C8C00D6D6D6006363630042424200949494000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840063636300636363006363
      6300636363006B636300214A8C005A94BD00217BAD00217BB5002173A5002173
      A500217BB500217BAD005A94BD00214A8400BDBDBD006B6B6B00CECECE007373
      73006B6B6B00F7F7F700EFEFEF0084848400BDBDBD00DEDEDE00BDBDBD008484
      8400848484008C8C8C007B7B7B00DEDEDE00000000004A4A4A007B7B7B008C8C
      8C008C8C8C00B5B5B500D6D6D600D6D6D600A5A5A500636363004A4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700BDBDBD0000000000000000000000000063636300736B6B007B7373007B73
      730063636300736B6300214A84003184AD0010639C001063940008527B000852
      7B001063940010639C003184AD00214A84007B7B7B00BDBDBD004A4A4A007B7B
      7B0063636300EFEFEF00DEDEDE00E7E7E70084848400BDBDBD00D6D6D600BDBD
      BD00C6C6C600C6C6C6009C9C9C00A5A5A500000000005A5A5A00A5A5A5008484
      8400848484007B7B7B0073737300C6C6C600ADADAD00A5A5A50063636300BDBD
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009494
      940063636300BDBDBD00000000000000000073737300A59C9C007B7B73006363
      6300525252005A525200214A8C00398CB500106B9C00106B9C0042C6FF0042C6
      FF00106B9C00106B9C00398CB500214A8400000000006B6B6B005A5A5A008484
      840063636300EFEFEF00DEDEDE00DEDEDE00DEDEDE007B7B7B00CECECE00CECE
      CE00D6D6D600D6D6D600D6D6D60094949400000000006B6B6B00BDBDBD009494
      940094949400FFFFFF00EFEFEF0073737300C6C6C600B5B5B500A5A5A5006363
      6300BDBDBD00000000000000000000000000DEDEDE00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD006363
      6300C6C6C60063636300BDBDBD000000000063636300C6C6C600636363007373
      7300313131004A4242002142840063A5C60008639C0008639C00086394000863
      940008639C0008639C0063A5C600214A8400BDBDBD006B6B6B00CECECE008C8C
      8C0063636300EFEFEF00D6D6D600D6D6D600DEDEDE007B7B7B00DEDEDE00DEDE
      DE006B6B6B006B6B6B00E7E7E70094949400000000007B7B7B00D6D6D600A5A5
      A500EFEFEF0000000000F7F7F700D6D6D60073737300C6C6C600ADADAD00ADAD
      AD0063636300BDBDBD0000000000000000008484840063636300636363006363
      6300636363006363630063636300636363006363630063636300636363009494
      9400BDBDBD00BDBDBD0063636300BDBDBD00C6C6C6006B6B6B0084847B00C6C6
      C600948C8C009C948C00184284006BA5BD00639CB500639CBD00639CB500639C
      B500639CBD00639CBD006BA5BD00214A84007B7B7B00BDBDBD004A4A4A009494
      940063636300EFEFEF00CECECE00F7F7F7000000000084848400D6D6D600DEDE
      DE007373730073737300DEDEDE009C9C9C000000000073737300B5B5B500E7E7
      E700D6D6D60094949400737373008C8C8C00EFEFEF007B7B7B00D6D6D6007373
      7300ADADAD0063636300BDBDBD000000000063636300BDBDBD00B5B5B500B5B5
      B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500ADAD
      AD00ADADAD00ADADAD00C6C6C6006363630000000000FFFFFF00736B6B00EFEF
      F70031312900313129005273A50010397B0010397B001842840018427B001839
      7B0018427B0010398400184284006B8CB500000000006B6B6B005A5A5A009C9C
      9C0063636300EFEFEF00CECECE0000000000ADADAD009C9C9C00A5A5A500D6D6
      D600D6D6D600D6D6D600ADADAD00C6C6C6000000000000000000A5A5A5009494
      9400B5B5B5008C8C8C006B6B6B007B7B7B0094949400ADADAD006B6B6B00E7E7
      E700737373007B7B7B00737373000000000063636300D6D6D600CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00A5A5A500A5A5A500CECECE00636363000000000000000000FFFFFF00B5CE
      DE006BADD6005294B500ADC6D600F7F7F700F7EFEF0008398400FFF7EF00FFF7
      EF0008397B0000000000CE84000000000000BDBDBD006B6B6B00CECECE00A5A5
      A50063636300EFEFEF00C6C6C600000000006B6B6B00B5B5B5009C9C9C009494
      9400949494009C9C9C008484840000000000000000000000000000000000D6D6
      D6009C9C9C00B5B5B5009C9C9C007B7B7B00E7E7E70000000000000000007373
      7300B5B5B5009494940084848400000000009494940063636300636363006363
      6300636363006363630063636300636363006363630063636300636363008C8C
      8C00A5A5A500D6D6D60063636300000000000000000000000000D6D6DE008CCE
      EF0084C6E7006BADD60063A5CE00F7CE1800FFDE080063735A00003184000031
      8400636B4A0000000000BD7B0000000000007B7B7B00BDBDBD004A4A4A00ADAD
      AD0063636300F7F7F700C6C6C600F7F7F700000000000000000000000000F7F7
      F700CECECE00F7F7F7006B6B6B00000000000000000000000000000000006363
      6300DEDEDE00BDBDBD00ADADAD00A5A5A5007373730000000000000000000000
      0000949494008C8C8C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006363
      6300D6D6D6006363630000000000000000000000000000000000395A8400ADEF
      FF008CD6F7007BBDE7006BB5DE0042524200FFD64200F7D63900FFD63900FFD6
      3900F7D6310000000000B57B080000000000000000006B6B6B005A5A5A00ADAD
      AD0063636300F7F7F700C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600F7F7F70063636300000000000000000000000000000000003131
      3100F7F7F700D6D6D600C6C6C600BDBDBD002121210000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C9C
      9C0063636300000000000000000000000000000000000000000008396B006BAD
      D60073ADD6008CC6E70073B5DE00001052000000000000000000000000000000
      00000000000000000000B57B080000000000BDBDBD006B6B6B00BDBDBD00ADAD
      AD0063636300FFFFFF00BDBDBD00C6C6C6005A5A5A008C8C8C008C8C8C00C6C6
      C600BDBDBD00FFFFFF0063636300000000000000000000000000000000003939
      3900525252007B7B7B00ADADAD008C8C8C002929290000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000010427300297B
      AD004A8CBD00527BA5002142730000215A00CEC6C600E7E7E70000000000FFEF
      AD00FFDE420000000000B57B0800000000007B7B7B00BDBDBD0042424200ADAD
      AD0063636300FFFFFF00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00FFFFFF0063636300000000000000000000000000000000003939
      3900737373008484840073737300424242002929290000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000849CB500217B
      AD00398CBD00397BAD0010396B005A6B8400CEC6C600BDBDBD00BDB5BD00F7E7
      8400E7B5000000000000B57B080000000000000000006B6B6B00EFEFEF00E7E7
      E7006363630000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00000000006B6B6B00000000000000000000000000000000005252
      52006B6B6B007B7B7B006B6B6B00424242004A4A4A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007394
      B500105A9400084A8C007B9CBD00000000000000000000000000000000000000
      00000000000000000000B57B08000000000000000000949494006B6B6B006B6B
      6B006B6B6B006B6B6B0063636300636363006363630063636300636363006363
      6300636363006B6B6B009494940000000000000000000000000000000000E7E7
      E7004A4A4A004A4A4A004A4A4A0042424200E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7B55A00C6840800BD7B0800B57B0800B57B0800B57B0800B57B
      0800B57B0800B57B0800CEAD6300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CECECE00CECECE00CECECE00F7F7F70000000000000000000000
      00000000000000000000000000000000000000000000CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00000000000000000000000000000000000000
      00000000000000000000CECECE00CECECE00CECECE00CECECE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7F7F700CECECE00CECE
      CE00CECECE0052738C004A6384004A94DE00B5C6D600CECECE00F7F7F7000000
      000000000000000000000000000000000000BD944200B57B0800B57B0800B57B
      0800B57B0800B57B0800B57B0800B57B0800B57B0800B57B0800B57B0800B57B
      0800B57B0800B57B0800B57B0800BD9442000000000000000000000000000000
      000000000000000000007B7B7B00E7E7E700B5B5B50084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AD9400AD521000AD52
      0800B54A0000528CAD00739CB5008CD6FF00296BA500BD520000C6AD94000000
      000000000000000000000000000000000000B57B0800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B57B0800DEDEDE00CECECE00CECECE00CECE
      CE00CECECE00CECECE0084848400EFEFEF00BDBDB50084848400CECECE00CECE
      CE00CECECE00CECECE00CECECE00DEDEDE00000000000000000000000000BDBD
      BD00CECECE000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B55A1000D6945A00E7A5
      6B00EFA5630039B5FF007BD6FF0084D6FF00109CFF00296BAD00CE6300000000
      000000000000000000000000000000000000B57B0800FFFFFF00A59C9C008C8C
      8C0094949400D6D6D600FFFFFF00C6C6C600C6C6C600FFFFFF00C6C6C600C6C6
      C600C6C6C600C6C6C600FFFFFF00B57B08008C8C8C006B6B6B006B6363006B63
      63006B6B63006B6B6300736B63006B6363006B6363006B6363006B6B6B006B6B
      6B006B6B6B006B6B6B006B6B6B008C8C8C000000000000000000BDBDBD006363
      63007B7B7B000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6733100EFBD8C00E79C
      5A00E79C5200F79C42001873CE0039C6FF0029ADFF00109CFF00316BA500CECE
      CE0000000000000000000000000000000000B57B0800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B57B0800736B6B00CECECE00C6C6C600C6C6
      BD00D6D6D600ADA594000000C600ADA59C00A59C9C00A59C9C00A59C9C00A59C
      9C00A59C9C00A59C9C00ADADA500736B6B0000000000BDBDBD0063636300C6C6
      C60063636300BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00D6D6D60000000000CE844A00EFCEAD00EFAD
      6B00EFAD6B00FFFFFF00FFF7E7002173C60042C6FF0029ADFF00109CFF00316B
      A500CECECE00000000000000000000000000B57B0800FFFFFF009C9494008C8C
      84008C8C8C00CECECE00FFFFFF00BDBDBD00BDBDBD00FFFFFF00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00FFFFFF00B57B080073737300C6C6C600B5B5B500B5B5
      B500CECECE00BDB5A5000000C600BDB5A500ADADA500ADADA500ADADA500ADAD
      A500ADADA500ADADA500BDBDBD0073737300BDBDBD0063636300BDBDBD00BDBD
      BD00949494006363630063636300636363006363630063636300636363006363
      63006363630063636300636363007B7B7B0000000000D6945A00F7DECE00F7BD
      7B00FFF7DE00FFFFFF00FFF7EF00FFE7BD002173CE0042C6FF0021ADFF0084B5
      DE007B7B7300CECECE000000000000000000B57B0800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F7F7F700FFFFF700FFFFF700F7F7F700FFFFF700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B57B08007B737300C6C6BD00ADADAD00ADAD
      A500CECEC600C6C6B5000000C600CEC6B500BDBDB500BDB5B500BDB5B500BDB5
      B500BDB5B500BDB5B500D6CECE007373730063636300C6C6C600ADADAD00ADAD
      AD00ADADAD00B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5
      B500B5B5B500B5B5B500BDBDBD006363630000000000D6944A00EFC69C00FFEF
      D600B5DEEF005AA5C600397BA500849CAD00FFF7CE00187BD600ADDEF700948C
      8400C6BDBD00737B6B00CECECE0000000000B57B0800FFFFFF0094948C008484
      84008C848400C6C6BD00F7F7F700B5B5B500B5B5B500F7F7F700B5B5B500B5B5
      B500B5B5B500B5B5B500FFFFFF00B57B08007B7B7B00CECECE00A5A59C00A59C
      9C00DEDED600D6D6C6000000CE00D6D6C600CEC6C600C6C6C600C6C6C600C6C6
      C600C6C6C600CEC6C600E7E7DE007B73730063636300CECECE00A5A5A500A5A5
      A500CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00D6D6D600636363000000000000000000EFBD8400F7AD
      6300BDC6BD00639CBD00427B9C0094948C00FFAD6300F7C68C0084848400EFEF
      E7008C8C8400BD7BB5009C6BCE0000000000B57B0800FFFFFF00F7F7F700F7F7
      F700F7F7F700F7F7EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00F7F7
      EF00F7F7EF00EFEFEF00FFFFFF00B57B0800847B7B00D6D6D6009C9494009494
      9400DEDED600E7E7CE000000CE00E7E7CE00D6D6CE00D6D6CE00D6D6CE00D6D6
      CE00D6CECE00DEDED600EFEFEF007B7B7B000000000063636300D6D6D600A5A5
      A5008C8C8C006363630063636300636363006363630063636300636363006363
      630063636300636363006363630094949400000000000000000000000000DEDE
      DE007BADC6007BBDDE006BADCE006394AD00E7E7E7000000000000000000848C
      8400E7B5E700CE94C600AD7BCE0000000000B57B0800FFFFFF008C8C8C00847B
      7B0084847B00BDBDB500EFEFE700ADADAD00ADADAD00EFEFE700ADADAD00ADAD
      AD00ADADAD00ADADAD00FFFFFF00B57B080084847B00E7E7E70094949400948C
      8C00F7F7EF00F7F7DE000000CE00F7F7DE00E7E7DE00E7DEDE00E7DEDE00E7DE
      DE00E7E7DE00F7F7EF00EFEFEF00847B7B00000000000000000063636300D6D6
      D600636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A73
      94009CE7FF0084C6EF0073B5DE006BADD6006B84A50000000000000000000000
      0000C68CDE00BD8CD6000000000000000000B57B0800FFFFFF00E7E7E700EFEF
      EF00EFEFE700E7E7E700E7E7DE00E7E7E700E7E7E700E7E7DE00E7E7E700E7E7
      E700E7E7E700E7E7E700FFFFFF00B57B0800B5ADAD00B5B5B500E7E7E700F7F7
      F700FFFFFF00FFFFF7000000D600FFFFF700FFF7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700B5ADAD00B5ADAD000000000000000000000000006363
      63007B7B7B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300ADF7FF0094DEFF0084CEE7007BC6E70000295A0000000000000000000000
      000000000000000000000000000000000000B57B0800FFFFFF00847B7B007373
      73007B737300ADADAD00DEDEDE00A5A59C00A5A59C00DEDEDE00A5A5A500A5A5
      A500A5A5A5009C9CA500FFFFFF00B57B0800000000009C9494008C8C8C008C8C
      84008C8C84009C9484000000DE009C9C84008C8C84008C8C84008C8C84008C8C
      84008C8C84008C8C840094949400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000839
      6B0021639400528CB5007BB5D600639CBD0008295A0000000000000000000000
      000000000000000000000000000000000000B57B0800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B57B08000000000000000000000000000000
      000000000000000000000000E700CECECE00CECECE00CECECE00DEDEDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001042
      7300317BAD004A8CBD00527BAD00294A73001031630000000000000000000000
      000000000000000000000000000000000000B57B1000F7DEB500DE9C3100DE9C
      3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C
      3900DE9C3900DE9C3100F7DEB500B57B10000000000000000000000000000000
      000000000000000000000000E7000000E7000000EF000000EF004242E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000395A
      84002173AD00398CBD00397BAD00214A7300395A840000000000000000000000
      000000000000000000000000000000000000BD841000EFD69C00EFCE9C00EFCE
      9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE9C00EFCE
      9C00EFCE9C00EFCE9C00EFD69C00BD8410000000000000000000000000000000
      000000000000000000000000E7000000E7000000D6000000BD000000E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEE7
      EF0021528400185A8C001852840021527B00DEE7EF0000000000000000000000
      000000000000000000000000000000000000CEA55200BD841000B5841000B584
      1000B5841000B5841000B5841000B5841000B5841000B5841000B5841000B584
      1000B5841000B5841000BD841000CEA552000000000000000000000000000000
      000000000000000000000000E7000000E7000000E7000000DE004A4AEF000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000D00000000100010000000000800600000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FC0FFC00F800820000038000F8000001
      00008000FBFE000000000000FA02000000000000020200000000800002020000
      000400007A0200003807000042020000F807800250020000F807000042020000
      F807000042020000F807800140000000F8070001405F0000F8070001405F0000
      F8078405405F0000F8078001001F0000FFF0FC3FFFFFE0010000FC1FFFFFC000
      000080070000C0000000000300008000000000007FFE81000001000000000000
      0003000000000010000300000000000000030000000000030003000000000003
      021300000000000100C3000000008001000300017FFEC000000300010000FC00
      00030003FFFFFF0000030007FFFFFFE1F000FFDFFFFFFF8F0800FF9FE0078007
      04008081E007800302000001E007800101000001E007800100800081E0078001
      00400391E0078001002003C1E0078003001003E1F00FC007000807E1F81FE00F
      00040003F81FE07F0002C003F81FE07F0000C007F81FE07F0000E003F81FE07F
      0000E003F81FE07F0000F01FF81FE07FFFFFFFFEFFFFFFFFFFC10000FFFFFFC1
      FF8000000000FF80FF8000000000FF80000800007FFE0000001C00004002001C
      000800007D6E0000000000004002000000000000400200000000000040000000
      0000000040000000000000000000000000000000400200000000000000000000
      00000000FFFF000080010000FFFF8001E0008000E7FFCF8300000000E3FFC701
      00000000E0FFC10100000000E07FC01100380000F03FE03900000000F01FE011
      07000000C00F800100000000C007800300000000C003800700000000C0038007
      00000000C003800700000000C01F803F00000000E01FC03F00000000E00FC01F
      00000000E00FC01F00000000E00FC01FFFFFFFC1FFC1C00300008000FF80C003
      000080000000DFFB000080000008D24B0000801C001CD00B000080000188D00B
      000080000180D00B000080010001D00B000080030005D00B0000800301E1D00B
      0000000101E1DFFB000000010001C003000000010001C003000000010001C003
      000000010001C003000087C30001C003FC7FFFFEFFFF8001FC3F0000FFFF8001
      80030000FFFF800180030000FFFF800180030000FFFF800180030000FE3F8001
      80010000FC3F800180000000F81F800180000000F00F800100000000F00F8001
      00010000F00F800100010000FFFF800100010000FFFF800180030000FFFF8001
      F11F0000FFFF8001F93F0000FFFF80018040DB6EC001FFFF00000000C001FFFF
      00000000DBFDFFFF00008002DBFDE7FF00000000C001C7FF00000000DB058000
      00008002C001000000005B6CD005000000000000C001000000008002D0058000
      00000000C001C7FF00000000D005E7FF00008002C001FFFF00000000D005FFFF
      0000DB6CDBFDFFFF00000000C001FFFFFFC18241FFFF80008000000000000000
      00000000000000000008000000000000001C0000000000000008000000000000
      0000000000000000000000000000000001900000000000000240000000000000
      00000000000000000000000040000000000000002A0000000000000000000000
      00000000FFFF0000FFFF0000FFFF0000FC3FF87FFFFFFC008001801FFFFF0000
      8001801FFFFF00000000801FFFE700000000800FFFE300008000800700010000
      000084030000000000808001000080008100C0010000C0050101E0610001C005
      00E1E073FFE3C0058001E07FFFE7C0FD0001E07FFFFFC0250001E07FFFFFC005
      84C5E07FFFFFE1FD8001E07FFFFFF801FFFFF87F8001FC3FFFFF801F0000FC3F
      FFFF801F00000000E7FF801F00000000C7FF800F000000008000800700000000
      000080030000000000008001000000000000C001000000008000E06100000000
      C7FFE07300000000E7FFE07F00008001FFFFE07F0000FC1FFFFFE07F0000FC1F
      FFFFE07F0000FC1FFFFFE07F0000FC1F00000000000000000000000000000000
      000000000000}
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 190
    Top = 106
  end
  object OffsetDeductionCL_ED: TevClientDataSet
    Left = 344
    Top = 16
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 221
    Top = 79
  end
  object evPopupMenu1: TevPopupMenu
    Left = 392
    Top = 19
    object CopytoCompanyED1: TMenuItem
      Caption = 'Copy to Company E/D'
      OnClick = CopytoCompanyED1Click
    end
    object Copyto1: TMenuItem
      Caption = 'Copy to...'
      ImageIndex = 2
    end
  end
  object wwdsStateTax: TevDataSource
    DataSet = DM_CL_E_D_STATE_EXMPT_EXCLD.CL_E_D_STATE_EXMPT_EXCLD
    MasterDataSource = wwdsDetail
    Left = 256
    Top = 8
  end
  object wwdsLocalTax: TevDataSource
    DataSet = DM_CL_E_D_LOCAL_EXMPT_EXCLD.CL_E_D_LOCAL_EXMPT_EXCLD
    MasterDataSource = wwdsDetail
    Left = 296
    Top = 8
  end
end
