inherited EDIT_CL_UNION: TEDIT_CL_UNION
  Width = 916
  Height = 598
  object lablClient_Union: TevLabel [0]
    Left = 16
    Top = 8
    Width = 57
    Height = 13
    Caption = 'Client Union'
  end
  inherited Panel1: TevPanel
    Width = 916
    inherited pnlFavoriteReport: TevPanel
      Left = 801
    end
    inherited pnlTopRight: TevPanel
      Width = 801
    end
  end
  inherited PageControl1: TevPageControl
    Width = 916
    Height = 544
    HelpContext = 28501
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 908
        Height = 515
        inherited pnlBorder: TevPanel
          Width = 904
          Height = 511
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 904
          Height = 511
          inherited Panel3: TevPanel
            Width = 856
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 856
            Height = 404
            inherited Splitter1: TevSplitter
              Height = 400
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 400
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 320
                IniAttributes.SectionName = 'TEDIT_CL_UNION\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 500
              Height = 400
              Title = 'Unions'
              object wwdgBrowse: TevDBGrid
                Left = 18
                Top = 48
                Width = 450
                Height = 320
                DisableThemesInTitle = False
                Selected.Strings = (
                  'NAME'#9'40'#9'Name'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_UNION\wwdgBrowse'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtUnion: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object bevCL_UNION: TEvBevel
        Left = 0
        Top = 0
        Width = 908
        Height = 515
        Align = alClient
      end
      object pnlUnion: TisUIFashionPanel
        Left = 8
        Top = 250
        Width = 487
        Height = 218
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'pnlUnion'
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Union Details'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object lablName: TevLabel
          Left = 11
          Top = 34
          Width = 68
          Height = 16
          Caption = '~Union Name'
          FocusControl = dedtName
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablAddress1: TevLabel
          Left = 11
          Top = 74
          Width = 47
          Height = 13
          Caption = 'Address 1'
          FocusControl = dedtAddress1
        end
        object lablAddress2: TevLabel
          Left = 11
          Top = 114
          Width = 47
          Height = 13
          Caption = 'Address 2'
          FocusControl = dedtAddress2
        end
        object lablCity: TevLabel
          Left = 11
          Top = 154
          Width = 17
          Height = 13
          Caption = 'City'
          FocusControl = dedtCity
        end
        object lablState: TevLabel
          Left = 139
          Top = 154
          Width = 25
          Height = 13
          Caption = 'State'
        end
        object lablZip_Code: TevLabel
          Left = 179
          Top = 154
          Width = 43
          Height = 13
          Caption = 'Zip Code'
        end
        object lablPhone: TevLabel
          Left = 284
          Top = 74
          Width = 31
          Height = 13
          Caption = 'Phone'
        end
        object lablContact: TevLabel
          Left = 284
          Top = 34
          Width = 37
          Height = 13
          Caption = 'Contact'
          FocusControl = dedtContact
        end
        object bvlTop: TBevel
          Left = 265
          Top = 50
          Width = 8
          Height = 141
          Shape = bsLeftLine
        end
        object dedtName: TevDBEdit
          Left = 11
          Top = 50
          Width = 241
          Height = 21
          DataField = 'NAME'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtAddress1: TevDBEdit
          Left = 11
          Top = 90
          Width = 241
          Height = 21
          DataField = 'ADDRESS1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtAddress2: TevDBEdit
          Left = 11
          Top = 130
          Width = 241
          Height = 21
          DataField = 'ADDRESS2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtCity: TevDBEdit
          Left = 11
          Top = 170
          Width = 121
          Height = 21
          DataField = 'CITY'
          DataSource = wwdsDetail
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeState: TevDBEdit
          Left = 139
          Top = 170
          Width = 33
          Height = 21
          CharCase = ecUpperCase
          DataField = 'STATE'
          DataSource = wwdsDetail
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeZip_Code: TevDBEdit
          Left = 179
          Top = 170
          Width = 73
          Height = 21
          DataField = 'ZIP_CODE'
          DataSource = wwdsDetail
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePhone: TevDBEdit
          Left = 284
          Top = 90
          Width = 184
          Height = 21
          DataField = 'PHONE'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtContact: TevDBEdit
          Left = 284
          Top = 50
          Width = 184
          Height = 21
          DataField = 'CONTACT'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object pnlListOfCLBankAccount: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 487
        Height = 233
        BevelOuter = bvNone
        BorderWidth = 12
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Summary'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object wwdgTeamMembers: TevDBGrid
          Left = 12
          Top = 35
          Width = 455
          Height = 177
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'40'#9'Name'#9'F')
          IniAttributes.Enabled = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CL_BANK_ACCOUNT\wwdgTeamMembers'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsDetail
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_UNION.CL_UNION
  end
end
