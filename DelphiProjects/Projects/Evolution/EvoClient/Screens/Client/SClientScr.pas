// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SClientScr;

interface

uses
  SPackageEntry, Classes, Db, Wwdatsrc,  
  SFrameEntry, ComCtrls, StdCtrls, Controls, Buttons, ExtCtrls, Menus,
  ImgList, ToolWin, ActnList, Graphics, ISBasicClasses, EvCommonInterfaces,
  EvMainboard, EvUIComponents, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TClientFrm = class(TFramePackageTmpl, IevClientScreens)
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;
  end;


implementation

{$R *.DFM}

var
  ClientFrm: TClientFrm;

function GetClientScreens: IevClientScreens;
begin
  if not Assigned(ClientFrm) then
    ClientFrm := TClientFrm.Create(nil);
  Result := ClientFrm;
end;


{ TSystemFrm }

function TClientFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TClientFrm.PackageCaption: string;
begin
  Result := 'C&lient';
end;

function TClientFrm.PackageSortPosition: string;
begin
  Result := '030';
end;

procedure TClientFrm.UserPackageStructure;
begin
  AddStructure('Client|010', 'TEDIT_CL');
  AddStructure('Bank Accounts|020', 'TEDIT_CL_BANK_ACCOUNT');
  AddStructure('Agency|030', 'TEDIT_CL_AGENCY');
  AddStructure('Billing|040', 'TEDIT_CL_BILLING');
  AddStructure('Delivery Method|050', 'TEDIT_CL_DELIVERY_METHOD');
  AddStructure('Delivery Groups|060', 'TEDIT_CL_DELIVERY_GROUPS');
  AddStructure('E/Ds|070', 'TEDIT_CL_E_DS');
  AddStructure('E/D Groups|080', 'TEDIT_CL_ED_GROUPS');
  AddStructure('E/D Group Codes|090', 'TEDIT_CL_ED_GROUP_CODES');
  AddStructure('Pension|100', 'TEDIT_CL_PENSION');
  AddStructure('Client Pension Funds|110', 'TEDIT_CL_FUNDS');
  AddStructure('Union|120', 'TEDIT_CL_UNION');
  AddStructure('Third Party Sick Administration|140', 'TEDIT_CL_3RDPARTY_SICK_ADMIN');
  AddStructure('Consolidated Reporting|170', 'TEDIT_CL_CO_CONSOLIDATION');
  AddStructure('Consolidated Taxes|180', 'TEDIT_CL_COMMON_PAYMASTER');
  AddStructure('Timeclock Imports|200', 'TEDIT_CL_TIMECLOCK_IMPORTS');
  AddStructure('Piecework|210', 'TEDIT_CL_PIECES');
  AddStructure('RW Reports|215', 'TEDIT_CL_RWREPORTS');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetClientScreens, IevClientScreens, 'Screens Client');

finalization
  ClientFrm.Free;


end.
