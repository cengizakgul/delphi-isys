// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL;

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, wwdbdatetimepicker,
  Wwdbspin, Wwdotdot, Wwdbcomb, ExtCtrls, DBCtrls, StdCtrls, wwdblook,
  wwdbedit, Mask, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, Controls, Classes, SPackageEntry, Dialogs, EvContext,
  ISBasicClasses, SDDClasses, SFrameEntry, SSecurityInterface, EvSecElement,
  SDataDictbureau, SDataDictclient, SDataDicttemp, EvConsts, EvExceptions,
  isBaseClasses, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBComboBox, isUIDBMemo,
  isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, EvDataset;

type
  TEDIT_CL = class(TEDIT_CL_BASE)
    tshtClient_Address: TTabSheet;
    tshtClient_Flags: TTabSheet;
    lablClient_Code: TevLabel;
    dedtClient_Code: TevDBEdit;
    lablClient_Name: TevLabel;
    dedtClient_Name: TevDBEdit;
    lablClient_Address1: TevLabel;
    dedtClient_Address1: TevDBEdit;
    lablClient_Address2: TevLabel;
    dedtClient_Address2: TevDBEdit;
    dedtClient_City: TevDBEdit;
    lablClient_State: TevLabel;
    dedtClient_State: TevDBEdit;
    lablClient_Zip_Code: TevLabel;
    lablAccountant_Number: TevLabel;
    lablAccountant_Contact: TevLabel;
    dedtAccountant_Contact: TevDBEdit;
    lablMinutes_Autosave: TevLabel;
    lablCustomer_Service_Rep: TevLabel;
    lablCustomer_Service_Team: TevLabel;
    lablStart_Date: TevLabel;
    lablTermination_Date: TevLabel;
    lablStatus_Code: TevLabel;
    lablTermination_Notes: TevLabel;
    wwlcAccountant_Number: TevDBLookupCombo;
    wwlcCustomer_Service_Team: TevDBLookupCombo;
    wwlcCustomer_Service_Rep: TevDBLookupCombo;
    dmemTermination_Notes: TEvDBMemo;
    drgpSecurity_Font: TevDBRadioGroup;
    drgpReciprocate_SUI: TevDBRadioGroup;
    drgpLeasing_Client: TevDBRadioGroup;
    drgpCPA_Reports: TevDBRadioGroup;
    wwlcStatus_Code: TevDBComboBox;
    lablClient_City: TevLabel;
    grbxClient_PrimaryContact: TevGroupBox;
    lablClient_PrimaryContact_Name: TevLabel;
    dedtClient_Contact1_Name: TevDBEdit;
    lablClient_PrimaryContact_Phone: TevLabel;
    lablClient_PrimaryContact_Description: TevLabel;
    dedtClient_PrimaryContact_Description: TevDBEdit;
    grbxClient_SecondaryContact: TevGroupBox;
    lablClient_SecondaryContact_Name: TevLabel;
    dedtClient_SecondaryContact_Name: TevDBEdit;
    lablClient_SecondaryContact_Phone: TevLabel;
    lablClient_SecondaryContact_Description: TevLabel;
    dedtClient_SecondaryContact_Description: TevDBEdit;
    lablClient_SecondaryContact_Email: TevLabel;
    dedtClient_SecondaryContact_Email: TevDBEdit;
    lablClient_PrimaryContact_E_Mail: TevLabel;
    dedtClient_PrimaryContact_Email: TevDBEdit;
    grbxClient_PrimaryFax: TevGroupBox;
    lablClient_PrimaryFax_Number: TevLabel;
    lablClient_PrimaryFax_Description: TevLabel;
    dedtClient_PrimaryFax_Description: TevDBEdit;
    grbxClient_SecondaryFax: TevGroupBox;
    lablClient_SecondaryFax_Number: TevLabel;
    lablClient_SecondaryFax_Description: TevLabel;
    dedtClient_Fax2_Description: TevDBEdit;
    wwdeClient_SecondaryContact_Phone: TevDBEdit;
    wwdeClient_PrimaryContact_Phone: TevDBEdit;
    wwseMinutes_Autosave: TevDBSpinEdit;
    wwdeClient_Fax1_Number: TevDBEdit;
    wwdeClient_Fax2_Number: TevDBEdit;
    wwlcService_Rep_LastName: TevDBLookupCombo;
    lblAutoSaveMins: TevLabel;
    bevCL_1: TEvBevel;
    bevCL_2: TEvBevel;
    wwdpStart_Date: TevDBDateTimePicker;
    wwdpTermination_Date: TevDBDateTimePicker;
    wwdeZip_Code: TevDBEdit;
    tsVMR: TTabSheet;
    evDBLookupCombo4: TevDBLookupCombo;
    evLabel5: TevLabel;
    evDBLookupCombo6: TevDBLookupCombo;
    evLabel7: TevLabel;
    evGroupBox1: TevGroupBox;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel6: TevLabel;
    evLabel8: TevLabel;
    edVmrAgencyChecks: TevDBLookupCombo;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBLookupCombo5: TevDBLookupCombo;
    evDBLookupCombo7: TevDBLookupCombo;
    lVmrStatus: TevLabel;
    lVmrPrinterLocation: TevLabel;
    cbVmrPrinterLocation: TevComboBox;
    rgPrintClientName: TevDBRadioGroup;
    lablClient_PrimaryContact_Ext: TevLabel;
    wwdeClient_PrimaryContact_Ext: TevDBEdit;
    lablClient_SecondaryContact_Ext: TevLabel;
    wwdeClient_SecondaryContact_Ext: TevDBEdit;
    bUnblock: TevButton;
    bPopulate: TevBitBtn;
    evRadioGroup1: TevRadioGroup;
    dcbBlockInvalidSSN: TevDBCheckBox;
    evRgClReadOnly: TevDBRadioGroup;
    btnClientRO: TevButton;
    procedure wwseMinutes_AutosaveChange(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
    procedure bPopulate_Click(Sender: TObject);
    procedure cbVmrPrinterLocationChange(Sender: TObject);
    procedure cbVmrPrinterLocationKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bUnblockClick(Sender: TObject);
    procedure evRadioGroup1Click(Sender: TObject);
    procedure btnClientROClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    procedure CheckAdvancedHR;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterDataSetsReopen; override;
    procedure OnGetSecurityStates(const States: TSecurityStateList); override;
    procedure SetReadOnly(Value: Boolean); override;
    procedure EnableEditLookupTables;
    procedure DisableEditLookupTables;
  public
    JustCreated: boolean;
    NewId: integer;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function  GetInsertControl: TWinControl; override;
    procedure AfterClick(Kind: Integer); override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;    
  end;

implementation

uses
  EvUtils, SysUtils, SCopyWizard, SClientCopyWizard, SCompanyCopyWizard,
  Graphics, SPD_EDIT_Open_BASE, EvTypes, EvInitApp, SPD_ChangeROFlag,
  EvCommonInterfaces;

{$R *.DFM}

const
  ctEnabledSRo = 'S';

procedure TEDIT_CL.Activate;
var
 aList : IisStringList;
begin
  inherited;
  JustCreated := False;
  EnableEditLookupTables;
  NewId := 0;
  PageControl1.ActivePageIndex := 0;
  tsVMR.TabVisible := CheckVmrLicenseActive;

  if DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', VmrSbLocation, []) then
    cbVmrPrinterLocation.Items.CommaText := DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString
  else
    cbVmrPrinterLocation.Items.CommaText := '';

  if CheckRemoteVMRActive then
  begin
    aList := GetRemotePrinterList;
    if (cbVmrPrinterLocation.Items.Count > 0) and (aList.count > 0) then
      cbVmrPrinterLocation.Items.CommaText := cbVmrPrinterLocation.Items.CommaText +',' + aList.CommaText
    else if (aList.count > 0) then
      cbVmrPrinterLocation.Items.CommaText := aList.CommaText;
  end;

  wwdeZip_Code.Picture.PictureMask := '{&{#& #&#},{*5{#}[-*4#]}}';
  wwdsMasterDataChange(nil, nil);

  CheckAdvancedHR;
end;

procedure TEDIT_CL.CheckAdvancedHR;
var
  Q :IEvQuery;
begin
    if (ctx_DataAccess.ClientID > 0) then
    begin
      Q := TEvQuery.Create('SELECT Count(*) cnt FROM CO WHERE {AsOfNow<CO>} and ENABLE_HR = ''' + GROUP_BOX_ADVANCED_HR + '''');
      Q.Execute;
      if Q.Result.FieldByName('cnt').AsInteger > 0 then
           dedtClient_Code.SecurityRO:= True
      else dedtClient_Code.SecurityRO:= GetIfReadOnly;

    end;
end;

procedure TEDIT_CL.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind in [NavCancel, NavAbort] then
  begin
    if JustCreated then
    begin
      ctx_StartWait;
      try
        ctx_DBAccess.DeleteClientDB(NewId);
        ctx_DataAccess.OpenClient(0);
      finally
        ctx_EndWait;
      end;
      PageControl1.ActivatePage(TabSheet1);
      BitBtn1.Enabled := True;
    end;
  end;

  if Kind = NavCommit then
  begin
    if NewID = 0 then
      NewID := DM_TEMPORARY.TMP_CL.FieldByName('CL_NBR').AsInteger;
    DM_TEMPORARY.TMP_CL.DisableControls;
    try
      DM_TEMPORARY.TMP_CL.Close;
      ctx_DataAccess.OpenDataSets([DM_TEMPORARY.TMP_CL]);
      DM_TEMPORARY.TMP_CL.Locate('CL_NBR', NewID, []);
    finally
      DM_TEMPORARY.TMP_CL.EnableControls;
    end;
    EnableEditLookupTables;
  end;

  if Kind in [NavCommit, NavCancel, NavAbort] then
  begin
    NewId := 0;
    JustCreated := False;
  end;
end;

procedure TEDIT_CL.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  CLCopy: TClientCopyWizard;
  CoCopy: TCompanyCopyWizard;
  aDS :  TArrayDS;
  tmpCLNbr:integer;

  procedure SetClientDb(const CLNbr: integer);
  var
    b:boolean;
  begin
    DM_TEMPORARY.TMP_CL.Close;
    DM_TEMPORARY.TMP_CL.Open;
    DM_TEMPORARY.TMP_CL.Locate('CL_NBR', CLNbr, []);
    ctx_DataAccess.OpenClient(CLNbr);
    DM_CLIENT.CL.Close;
    DM_CLIENT.CL.ClientID := CLNbr;
    ctx_DataAccess.OpenDataSets([DM_CLIENT.CL]);
    b := false;
    GetDataSetsToReopen(aDS, b);
    ctx_DataAccess.OpenDataSets(aDS);
  end;

begin
  if Kind = NavDelete then
  begin
    Handled :=  True;

    if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_CLIENT') = stDisabled then
      raise ENoRights.CreateHelp('You have no rights to perform this operation.', IDH_SecurityViolation);

    if EvMessage('Are you sure you want to delete   CL# ' + DM_CLIENT.CL.CL_NBR.AsString + ' - ' +
                                                            DM_CLIENT.CL.CUSTOM_CLIENT_NUMBER.AsString + ' - ' +
                                                            DM_CLIENT.CL.NAME.AsString + ' ?'#13'Select Ok to delete ENTIRE client and company information.', mtConfirmation, mbOKCancel, mbCancel) <> mrOK then
    Exit;

    tmpCLNbr := DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger;
    ctx_DBAccess.DeleteClientDB(DM_CLIENT.CL.CL_NBR.AsInteger);
    ctx_DataAccess.OpenClient(0);
    updateSbENABLE_ANALYTICSforClient(tmpCLNbr);

    DM_TEMPORARY.TMP_CL.Close;
    ctx_DataAccess.OpenDataSets([DM_TEMPORARY.TMP_CL]);

    PageControl1.ActivatePage(TabSheet1);
    BitBtn1.Enabled := True;

  end

  else if Kind = NavInsert then
  begin
    Handled := True;

    if EvMessage('Create new client. Are you sure?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
      Exit;

    if (DM_TEMPORARY.TMP_CL.RecordCount > 0) and
       (EvMessage('Would you like to create the new client based on another client?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
    begin
      CLCopy := TClientCopyWizard.CreateTCopyWizard(Nil, evClient);
      tmpCLNbr:= DM_CLIENT.CL.ClientID;
      try
        CLCopy.Caption := CLCopy.Caption + 'Client';
        if CLCopy.ShowModal <> mrCancel then
        begin
          if EvMessage('Would you like to create a company at this time?' , mtConfirmation, [mbYes, mbNo]) <> mrNo then
          begin
            DM_TEMPORARY.TMP_CO.DataRequired('CL_NBR>0');
            CoCopy := TCompanyCopyWizard.CreateTCopyWizard(Nil, evCompany);
            try
              COCopy.Caption := COCopy.Caption + 'Company';
              CoCopy.CLList.CLBase := CLCopy.MyTableList.CLBase;
              CoCopy.CLList.CLNew := CLCopy.MyTableList.CLNew;
              CoCopy.CLNumber := ClCopy.CLNumber;
              DM_TEMPORARY.TMP_CL.Filter := 'CL_NBR=' + IntToStr(CoCopy.CLList.CLBase);
              DM_TEMPORARY.TMP_CL.Filtered := True;
              {result := }CoCopy.ShowModal;
              DM_TEMPORARY.TMP_CO.Close;
              DM_TEMPORARY.TMP_CO.Open;
            finally
              CoCopy.Free;
              DM_TEMPORARY.TMP_CL.Filtered := False;
              DM_TEMPORARY.TMP_CL.Filter := '';
            end;
          end
          else
            DM_TEMPORARY.TMP_CO.DataRequired('CL_NBR>0');

          SetClientDb(CLCopy.MyTableList.CLNew);
        end
        else
          PageControl1.ActivatePage(TabSheet1);
      finally
        if DM_CLIENT.CL.ClientID = 0 then
           SetClientDb(tmpCLNbr);
        CLCopy.Free;
      end;
    end
    else
    begin
      ctx_StartWait;
      try
        NewId := ctx_DBAccess.CreateClientDB(nil);
        JustCreated := True;
        DisableEditLookupTables;
        ctx_DataAccess.OpenClient(NewId);
        DM_CLIENT.CL.Close;
        DM_CLIENT.CL.ClientID := NewId;
        ctx_DataAccess.OpenDataSets([DM_CLIENT.CL, DM_CLIENT.CL_MAIL_BOX_GROUP]);

        // CL table already has initial record
        DM_CLIENT.CL.Edit;
        DM_CLIENT.CL.CUSTOM_CLIENT_NUMBER.Clear;
        DM_CLIENT.CL.NAME.Clear;
        DM_CLIENT.CL.ADDRESS1.Clear;
        DM_CLIENT.CL.CITY.Clear;
        DM_CLIENT.CL.STATE.Clear;
        DM_CLIENT.CL.ZIP_CODE.Clear;
        DM_CLIENT.CL.START_DATE.Clear;
        BitBtn1.Enabled := False;
        PageControl1.ActivatePage(tshtClient_Address);
      finally
        ctx_EndWait;
      end;
    end;
  end;
end;

function TEDIT_CL.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL;
end;

function TEDIT_CL.GetInsertControl: TWinControl;
begin
  Result := dedtClient_Code;
end;

procedure TEDIT_CL.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  //AddDSWithCheck(DM_CLIENT.CL_MAIL_BOX_GROUP, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_ACCOUNTANT, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_TEAM, SupportDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_USER, SupportDataSets, ' STRCOPY(CAST(ConvertNullString(USER_FUNCTIONS, '''') AS VARCHAR(512)), 0, 2) IN (''-1'', '''')  AND ACTIVE_USER = ''Y'' ');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_OPTION, SupportDataSets, '');
end;

procedure TEDIT_CL.wwseMinutes_AutosaveChange(Sender: TObject);
begin
  inherited;
  if DM_CLIENT.CL.Active then
    (Owner as TFramePackageTmpl).AutoSave(DM_CLIENT.CL.FieldByName('AUTO_SAVE_MINUTES').AsInteger);
end;

procedure TEDIT_CL.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if PageControl1.ActivePage = TabSheet1 then
    AllowChange := AllowChange and DM_CLIENT.CL.Active and ((wwdsMaster.DataSet.RecordCount > 0) or JustCreated);

end;

procedure TEDIT_CL.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS([DM_CLIENT.CL_MAIL_BOX_GROUP], aDS);
end;

procedure TEDIT_CL.AfterDataSetsReopen;
begin
  inherited;
  AssignVMRClientLookups(tsVMR);
end;

procedure TEDIT_CL.wwdsMasterDataChange(Sender: TObject; Field: TField);
var
  sV: String;
begin
  inherited;
  if CheckVmrSetupActive then
  begin
    lVmrStatus.Caption := 'Required minimal setup is done. Mail room is active';
    lVmrStatus.Font.Color := clGreen;
  end
  else
  begin
    lVmrStatus.Caption := 'Required minimal setup is NOT done. Mail room is NOT active';
    lVmrStatus.Font.Color := clRed;
  end;
  if cbVmrPrinterLocation.Tag = 0 then
  begin
    cbVmrPrinterLocation.Tag := 1;
    try
      cbVmrPrinterLocation.ItemIndex := cbVmrPrinterLocation.Items.IndexOf(Trim(Copy(DM_CLIENT.CL.FILLER.AsString, 1, 40)));
      if (DM_CLIENT.CL.FILLER.AsString <> '') and (cbVmrPrinterLocation.ItemIndex = -1) then
        lVmrPrinterLocation.Font.Color := clRed
      else
        lVmrPrinterLocation.Font.Color := clBlack;
    finally
      cbVmrPrinterLocation.Tag := 0;
    end;
  end;

  if evRadioGroup1.Tag = 0 then
  begin
    evRadioGroup1.Tag := 1;
    try
      sV := Copy(DM_CLIENT.CL.FILLER.AsString, 41, 1);
      if (sV = 'N') or (sV = '') then
        evRadioGroup1.ItemIndex := 1
      else
        evRadioGroup1.ItemIndex := 0;
    finally
      evRadioGroup1.Tag := 0;
    end;
  end;
end;

procedure TEDIT_CL.bPopulate_Click(Sender: TObject);
begin
  inherited;
  wwdsMaster.Edit;
  with wwdsMaster.DataSet do
  begin
    FieldByName('PR_CHECK_MB_GROUP_NBR').Value := FieldByName('AGENCY_CHECK_MB_GROUP_NBR').Value;
    FieldByName('PR_REPORT_MB_GROUP_NBR').Value := FieldByName('AGENCY_CHECK_MB_GROUP_NBR').Value;
    FieldByName('TAX_CHECK_MB_GROUP_NBR').Value := FieldByName('AGENCY_CHECK_MB_GROUP_NBR').Value;
    FieldByName('TAX_EE_RETURN_MB_GROUP_NBR').Value := FieldByName('AGENCY_CHECK_MB_GROUP_NBR').Value;
    FieldByName('TAX_RETURN_MB_GROUP_NBR').Value := FieldByName('AGENCY_CHECK_MB_GROUP_NBR').Value;
  end;
end;

procedure TEDIT_CL.cbVmrPrinterLocationChange(Sender: TObject);
begin
  inherited;
  if cbVmrPrinterLocation.Tag = 0 then
  begin
    cbVmrPrinterLocation.Tag := 1;
    try
      wwdsMaster.Edit;
      if cbVmrPrinterLocation.ItemIndex = -1 then
        wwdsMaster.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsMaster.DataSet.FieldByName('FILLER').AsString, '', 1, 40)
      else
        wwdsMaster.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsMaster.DataSet.FieldByName('FILLER').AsString, cbVmrPrinterLocation.Items[cbVmrPrinterLocation.ItemIndex], 1, 40);

    finally
      cbVmrPrinterLocation.Tag := 0;
    end;
  end;
end;

procedure TEDIT_CL.cbVmrPrinterLocationKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key in [46,8] then
  begin
    cbVmrPrinterLocation.ItemIndex := -1;
    cbVmrPrinterLocationChange(cbVmrPrinterLocation);
  end;
end;

procedure TEDIT_CL.bUnblockClick(Sender: TObject);
begin
// This code and the button should be removed in Orange
{  CheckDSCondition(DM_SERVICE_BUREAU.SB_USER, 'trim(strcopy(user_functions, 0, 8))=''' + IntToStr(ctx_DataAccess.ClientID) + '''');
  ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_USER]);
  DM_SERVICE_BUREAU.SB_USER.First;
  while not DM_SERVICE_BUREAU.SB_USER.Eof do
  begin
    if DM_SERVICE_BUREAU.SB_USER.WRONG_PSWD_ATTEMPTS.Value >= 3 then
    begin
      DM_SERVICE_BUREAU.SB_USER.Edit;
      DM_SERVICE_BUREAU.SB_USER.WRONG_PSWD_ATTEMPTS.Value := 0;
      DM_SERVICE_BUREAU.SB_USER.Post;
    end;
    DM_SERVICE_BUREAU.SB_USER.Next;
  end;
  if DM_SERVICE_BUREAU.SB_USER.ChangeCount > 0 then
  begin
    ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_USER]);
    EvMessage('Users are unblocked');
  end
  else
    EvMessage('There were no blocked users');}
end;

procedure TEDIT_CL.OnGetSecurityStates(const States: TSecurityStateList);
const
  secScreenCLIDRo: TAtomContextRecord = (Tag: ctEnabledSRo; Caption: 'Enabled Except Client ID'; IconName: ''; Priority: 200);
begin
  with secScreenCLIDRo do
    States.InsertAfter(ctEnabled, Tag[1], Caption, IconName);
end;

procedure TEDIT_CL.SetReadOnly(Value: Boolean);
begin
  inherited;
  if SecurityState = ctEnabledSRo then
    dedtClient_Code.SecurityRO := True;

  evRgClReadOnly.SecurityRO := True;
  evRgClReadOnly.Enabled := false;
  btnClientRO.SecurityRO := CheckReadOnlyClientFunction;
end;

procedure TEDIT_CL.evRadioGroup1Click(Sender: TObject);
var
  sV: String;
begin
  inherited;
  if evRadioGroup1.Tag = 0 then
  begin
    evRadioGroup1.Tag := 1;
    try
      if ((Sender As TevRadioGroup).ItemIndex = 0) then
        sV := 'Y'
      else
        sV := 'N';
      wwdsMaster.DataSet.Edit;
      wwdsMaster.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsMaster.DataSet.FieldByName('FILLER').AsString, sV, 41, 1);
    finally
      evRadioGroup1.Tag := 0;
    end;
  end;

end;

procedure TEDIT_CL.DisableEditLookupTables;
var
   i : integer;
begin
   for i := 0 to self.ComponentCount - 1 do
     if self.Components[i] is TevDBLookupCombo then
       TevDBLookupCombo(self.Components[i]).LookupTableEditorEnabled := false;
end;

procedure TEDIT_CL.EnableEditLookupTables;
var
   i : integer;
begin
   for i := 0 to self.ComponentCount - 1 do
     if self.Components[i] is TevDBLookupCombo then
       TevDBLookupCombo(self.Components[i]).LookupTableEditorEnabled := true;
end;

procedure TEDIT_CL.btnClientROClick(Sender: TObject);
const
  ROValues : array [0..2] of char = ('Y', 'R', 'N');
begin
  
  if not (TevClientDataSet(wwdsMaster.DataSet).State in [dsInsert,dsEdit]) then
  begin
    with TChangeROFlag.Create( Self ) do
    try
       evRgReadOnly.ItemIndex := 2;
       if ClientReadOnlyFlag = READ_ONLY_MAINTENANCE then
          evRgReadOnly.ItemIndex := 0
       else
       if ClientReadOnlyFlag = READ_ONLY_READ_ONLY then
           evRgReadOnly.ItemIndex := 1;
       if ShowModal = mrYes then
          if ClientReadOnlyFlag <> ROValues[evRgReadOnly.ItemIndex] then
          begin
             ctx_DBAccess.SetClientROFlag(ROValues[evRgReadOnly.ItemIndex]);
             SetClientReadOnlyFlag(true);
             DM_CLIENT.CL.Close;
             DM_CLIENT.CL.DataRequired('ALL');
             BitBtn1Click(Self);
          end;
    finally
      Free;
    end;
   end
   else
     EvMessage('You have to be in browse mode.');
end;

procedure TEDIT_CL.PageControl1Change(Sender: TObject);
var
  dateNow : TDate;
begin
  inherited;
  dateNow := Trunc(now);
  if PageControl1.ActivePage = tshtClient_Flags then
    btnClientRO.Enabled := ctx_DataAccess.AsOfDate = dateNow;

  if PageControl1.ActivePage = TabSheet1 then
     (Owner as TFramePackageTmpl).btnNavDelete.SecurityRO:= True
  else
     (Owner as TFramePackageTmpl).btnNavDelete.SecurityRO := GetIfReadOnly;

end;

procedure TEDIT_CL.OnSetButton(Kind: Integer; var Value: Boolean);
begin
  inherited;
  if PageControl1.ActivePage = TabSheet1 then
     (Owner as TFramePackageTmpl).btnNavDelete.SecurityRO:= True
end;

initialization
  RegisterClass(TEDIT_CL);

end.
