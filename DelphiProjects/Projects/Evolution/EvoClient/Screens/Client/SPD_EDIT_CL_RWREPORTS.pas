// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_RWREPORTS;

interface

uses
   SFrameEntry, StdCtrls, Mask, wwdbedit, Controls, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, Classes, Db, Wwdatsrc, SDataStructure,
  EvConsts, EvUtils, Wwdotdot, Wwdbcomb, ISZippingRoutines, EvContext,
  DBCtrls, Menus, sysutils, Forms, Dialogs,
  EvSecElement, SSecurityInterface, SPD_EDIT_CL_BASE, Buttons, ExtCtrls,
  SPD_EDIT_Open_BASE, SPackageEntry, SDDClasses,
  ISBasicClasses, EvStreamUtils, SDataDictclient, SDataDicttemp, EvDataAccessComponents, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIDBMemo, isUIwwDBEdit, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton
  ,SSBReportCollection
  ,EvCommonInterfaces, EvDataSet
  ,StrUtils
  ,DBClient
  ;

type
  TEDIT_CL_RWREPORTS = class(TEDIT_CL_BASE)
    PopupMenu1: TPopupMenu;
    miCompile: TMenuItem;
    miConversion: TMenuItem;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    sdExport: TSaveDialog;
    odImport: TOpenDialog;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    isUIFashionPanel1: TisUIFashionPanel;
    isUIFashionPanel2: TisUIFashionPanel;
    Label1: TevLabel;
    wwDBEdit1: TevDBEdit;
    wwcbReport_Type: TevDBComboBox;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evDBComboBox2: TevDBComboBox;
    btnRWDesigner: TevBitBtn;
    EvDBMemo1: TEvDBMemo;
    btnExportReport: TevBitBtn;
    btnImportReport: TevBitBtn;
    grReports: TevDBGrid;
    evPanel1: TevPanel;
    ScrollBox1: TScrollBox;
    evPanel3: TevPanel;
    evPanel2: TevPanel;
    evPanel4: TevPanel;
    ScrollBox2: TScrollBox;
    evPanel5: TevPanel;
    evPanel6: TevPanel;
    ExportSelectedReportsToFile1: TMenuItem;
    ImportReportsFromCollectionFile1: TMenuItem;
    procedure btnRWDesignerClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure grReportsDblClick(Sender: TObject);
    procedure miCompileClick(Sender: TObject);
    procedure miConversionClick(Sender: TObject);
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure btnExportReportClick(Sender: TObject);
    procedure btnImportReportClick(Sender: TObject);
    procedure miSaveReportCollection(Sender: TObject);
    procedure miLoadReportCollection(Sender: TObject);
  private
    FCustomView: Boolean;
    fl_report_changed: Boolean;
    FPrevBeforePost:  TDataSetNotifyEvent;
    fImportCollectionDeltaNames:TStringList;

    procedure BeforePost(ADataSet: TDataSet);
    procedure CompressReport;
    procedure LoadNewReportItem(Sender: TObject;
                                var ReportType: string;
                                jClienNumber,
                                ImportedNbr: integer;
                                var ReportName: string;
                                var AssignedNewNbr: integer
                               );
    function ReportNameOrderHead(ProposedName: string): string;
  protected
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure SetReadOnly(Value: Boolean); override;
    function GetIfReadOnly: Boolean; override;
    procedure OnGetSecurityStates(const States: TSecurityStateList); override;
    procedure AfterDataSetsReopen; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

{$R *.DFM}

const
  ctReadOnlyWithExport = 'E';

procedure TEDIT_CL_RWREPORTS.btnRWDesignerClick(Sender: TObject);
var
  MS1, MS2: IEvDualStream;
  Notes, ClName, AncClName: String;

  procedure ShDsgn;
  begin
    MS2.Position := 0;
    ctx_RWDesigner.ShowDesigner(MS2, wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString, Notes, ClName, AncClName);
  end;

begin
  MS1 := TEvDualStreamHolder.CreateInMemory;
  MS2 := TEvDualStreamHolder.CreateInMemory;

  if fl_report_changed then
  begin
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).SaveToStream(MS1.RealStream);
    MS1.Position := 0;
  end
  else
  begin
    ctx_StartWait('Opening report...');
    try
      if wwdsDetail.DataSet.State in [dsBrowse, dsEdit] then
        try
          ctx_RWLocalEngine.GetReportResources(
            wwdsDetail.DataSet.FieldByName('CL_REPORT_WRITER_REPORTS_NBR').AsInteger, CH_DATABASE_CLIENT, MS1);
        except
          on E: Exception do
          begin
            MS1.Clear;
            EvErrMessage(E.Message);
          end;
        end;
    finally
      ctx_EndWait;
    end;
  end;

  MS2.RealStream.CopyFrom(MS1.RealStream, 0);

  ShDsgn;

  if not wwDBEdit1.ReadOnly then
  begin
    MS1.Position := 0;
    MS2.Position := 0;

    if (MS1.Size <> MS2.Size) or not CompareMem(MS1.Memory, MS2.Memory, MS1.Size) then
    begin
      grReports.Enabled := False;

      if not (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) then
        wwdsDetail.DataSet.Edit;

      TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromStream(MS2.RealStream);
      if Length(Notes) > 0 then
        Notes := Notes + #13#13;
      Notes := Notes + 'Last update: ' + DateTimeToStr(Now);
      TBlobField(wwdsDetail.DataSet.FieldByName('NOTES')).AsString := Notes;
      wwdsDetail.DataSet.FieldByName('Class_Name').AsString := ClName;
      wwdsDetail.DataSet.FieldByName('Ancestor_Class_Name').AsString := AncClName;
      fl_report_changed := true;
    end;
  end;
end;

function TEDIT_CL_RWREPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  if not FCustomView then
    if not DM_CLIENT.CL_REPORT_WRITER_REPORTS.Active then
    begin
      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;

      with TExecDSWrapper.Create('SelectHist') do
      begin
        SetMacro('Columns', 'CL_REPORT_WRITER_REPORTS_NBR, REPORT_DESCRIPTION, REPORT_TYPE, NOTES, NOTES REPORT_FILE, MEDIA_TYPE, ANCESTOR_CLASS_NAME, CLASS_NAME');
        SetMacro('TableName', 'CL_REPORT_WRITER_REPORTS');
        SetParam('StatusDate', ctx_DataAccess.AsOfDate);

        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Open;
        DM_CLIENT.CL_REPORT_WRITER_REPORTS.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
        ctx_DataAccess.CUSTOM_VIEW.Close;
      end;
      FCustomView := True;
    end
    else
      FCustomView := False;

  Result := DM_CLIENT.CL_REPORT_WRITER_REPORTS;
end;

procedure TEDIT_CL_RWREPORTS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and (wwdsDetail.DataSet.State = dsInsert) then
    wwdsDetail.DataSet.FieldByName('REPORT_TYPE').AsString := SYSTEM_REPORT_TYPE_NORMAL;

  if wwdsDetail.DataSet.State = dsBrowse then
    fl_report_changed := False;
end;

procedure TEDIT_CL_RWREPORTS.grReportsDblClick(Sender: TObject);
begin
  inherited;
  btnRWDesigner.Click;
end;

procedure TEDIT_CL_RWREPORTS.miCompileClick(Sender: TObject);
var
  MS: IEvDualStream;
  err: String;

  procedure CompileRep;
  var
    ClName, AncClName: String;
  begin
    MS.Clear;

    ctx_RWLocalEngine.GetReportResources(
      wwdsDetail.DataSet.FieldByName('CL_REPORT_WRITER_REPORTS_NBR').AsInteger,
      CH_DATABASE_CLIENT, MS);
    MS.Position := 0;

    try
      ctx_RWLocalEngine.CompileReport(MS, ClName, AncClName);
    except
      on E: Exception do
        err := E.Message;
    end;

    if Length(err) > 0 then
      Exit;

    wwdsDetail.DataSet.Edit;
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromStream(MS.RealStream);
    CompressReport;
    wwdsDetail.DataSet.FieldByName('Class_Name').AsString := ClName;
    wwdsDetail.DataSet.FieldByName('Ancestor_Class_Name').AsString := AncClName;
    wwdsDetail.DataSet.Post;
    ctx_DataAccess.PostDataSets([TevClientDataSet(wwdsDetail.DataSet)]);
  end;

begin
  if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) or wwDBEdit1.ReadOnly then
    Exit;

  MS := TEvDualStreamHolder.Create;
  TevClientDataSet(wwdsDetail.DataSet).DisableControls;
  Screen.Cursor := crAppStart;

  try
    err := '';
    ctx_StartWait('Compiling reports...');
    while grReports.SelectedList.Count > 0 do
    begin
      wwdsDetail.DataSet.GotoBookmark(grReports.SelectedList[0]);
      ctx_UpdateWait('Reports left: ' + IntToStr(grReports.SelectedList.Count) + #13 +
        wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString);
      Application.ProcessMessages;
      CompileRep;
      if Length(err) > 0 then
        Break;
      grReports.SelectedList.Delete(0);
    end
  finally
    ctx_EndWait;
    TevClientDataSet(wwdsDetail.DataSet).EnableControls;
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.Cancel;
    Screen.Cursor := crArrow;
  end;

  if Length(err) > 0 then
  begin
    EvErrMessage(Err);
    btnRWDesigner.Click;
  end;
end;


procedure TEDIT_CL_RWREPORTS.SetReadOnly(Value: Boolean);
var
  c: ISecurityElement;
begin
  inherited;
  c := FindSecurityElement(Self, ClassName);
  if not(Assigned(c) and (c.SecurityState = stReadOnly)) then
    btnRWDesigner.SecurityRO := False;

  miCompile.Enabled := not Value;
  miConversion.Enabled := not Value;
  EvDBMemo1.SecurityRO := True;

  btnExportReport.SecurityRO := not (SecurityState[1] in [ctReadOnlyWithExport, ctEnabled]);
end;



procedure TEDIT_CL_RWREPORTS.miConversionClick(Sender: TObject);
var
  MS1, MS2: IEvDualStream;
  Conv, NotConv: Integer;
  F: TextFile;

  function ConvertRep: Boolean;
  var
    ClName, AncClName: String;
  begin
    Result := False;
    MS2.Clear;
    ctx_RWLocalEngine.GetReportResources(
      wwdsDetail.DataSet.FieldByName('CL_REPORT_WRITER_REPORTS_NBR').AsInteger, CH_DATABASE_CLIENT, MS2);
    MS2.Position := 0;

    try
      Result := ctx_RWDesigner.ReCreateWizardReport(MS2);
      if Result then
      begin
        Result := False;
        MS2.Position := 0;
        ctx_RWLocalEngine.CompileReport(MS2, ClName, AncClName);
        Result := True;
      end;
    except
    end;

    if Result then
    begin
      MS2.Position := 0;
      MS1.Clear;
      DeflateStream(MS2.RealStream, MS1.RealStream);
      wwdsDetail.DataSet.Edit;
      TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromStream(MS1.RealStream);
      wwdsDetail.DataSet.FieldByName('Class_Name').AsString := ClName;
      wwdsDetail.DataSet.FieldByName('Ancestor_Class_Name').AsString := AncClName;
      wwdsDetail.DataSet.Post;
      ctx_DataAccess.PostDataSets([TevClientDataSet(wwdsDetail.DataSet)]);
    end;
  end;

begin
  if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) or wwDBEdit1.ReadOnly then
    Exit;

  Conv := 0;
  NotConv := 0;
  MS1 := TEvDualStreamHolder.Create;
  MS2 := TEvDualStreamHolder.Create;
  TevClientDataSet(wwdsDetail.DataSet).DisableControls;
  Screen.Cursor := crAppStart;
  AssignFile(F, 'C:\NotCorrectedReports.txt');
  Rewrite(F);
  try
    ctx_StartWait('Converting reports...');

    while grReports.SelectedList.Count > 0 do
    begin
      wwdsDetail.DataSet.GotoBookmark(grReports.SelectedList[0]);
      ctx_UpdateWait('Reports left: ' + IntToStr(wwdsDetail.DataSet.RecordCount - wwdsDetail.DataSet.RecNo) +
       '               Corrected: ' + IntToStr(Conv) + '               Not Corrected: ' + IntToStr(NotConv) + #13#13 +
       'Current Report: ' + wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString);
      Application.ProcessMessages;
      if not ConvertRep then
      begin
        Inc(NotConv);
        Writeln(F, wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString);
      end
      else
        Inc(Conv);
      grReports.SelectedList.Delete(0);
    end;

  finally
    ctx_EndWait;
    CloseFile(F);
    TevClientDataSet(wwdsDetail.DataSet).EnableControls;
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.Cancel;
    Screen.Cursor := crArrow;
  end;
end;



procedure TEDIT_CL_RWREPORTS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind = NavOK then
  begin
    grReports.Enabled := True;
  end

  else if Kind = NavCancel then
    grReports.Enabled := True;
end;

procedure TEDIT_CL_RWREPORTS.AfterDataSetsReopen;
begin
  inherited;
end;

constructor TEDIT_CL_RWREPORTS.Create(AOwner: TComponent);
begin
  inherited;
  FCustomView := False;
end;

procedure TEDIT_CL_RWREPORTS.Deactivate;
begin
  if FCustomView then
  begin
    DM_CLIENT.CL_REPORT_WRITER_REPORTS.Close;
    FCustomView := False;
  end;

  DM_CLIENT.CL_REPORT_WRITER_REPORTS.BeforePost := FPrevBeforePost;

  inherited;
end;

procedure TEDIT_CL_RWREPORTS.Activate;
begin
  inherited;
  FPrevBeforePost := DM_CLIENT.CL_REPORT_WRITER_REPORTS.BeforePost;
  DM_CLIENT.CL_REPORT_WRITER_REPORTS.BeforePost := BeforePost;

  fl_report_changed := False;  
end;



procedure TEDIT_CL_RWREPORTS.wwdsDetailUpdateData(Sender: TObject);
begin
  inherited;
  wwdsDetail.DataSet['MEDIA_TYPE'] := MEDIA_TYPE_PLAIN_LETTER;
end;

procedure TEDIT_CL_RWREPORTS.btnExportReportClick(Sender: TObject);
var
  FS: IEvDualStream;
begin
  sdExport.FileName := NormalizeFileName(Trim(wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString) + '.rwr');

  if not sdExport.Execute then
    Exit;

  sdExport.FileName := ChangeFileExt(sdExport.FileName, '.rwr');

  FS := TEvDualStreamHolder.CreateFromNewFile(sdExport.FileName);

  if fl_report_changed then
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).SaveToStream(FS.RealStream)

  else
  begin
    ctx_StartWait('Opening report...');
    try
      if wwdsDetail.DataSet.State in [dsBrowse, dsEdit] then
        ctx_RWLocalEngine.GetReportResources(
          wwdsDetail.DataSet.FieldByName('CL_REPORT_WRITER_REPORTS_NBR').AsInteger,
          CH_DATABASE_CLIENT, FS);
    finally
      ctx_EndWait;
    end;
  end;

  EvMessage('Report has been successfully exported to file');
end;

procedure TEDIT_CL_RWREPORTS.btnImportReportClick(Sender: TObject);
begin
  if not odImport.Execute then
    Exit;

  if not (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) then
    wwdsDetail.DataSet.Edit;
  TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromFile(odImport.FileName);
  fl_report_changed := True;

  EvMessage('Report has been successfully imported from file');
end;

procedure TEDIT_CL_RWREPORTS.BeforePost(ADataSet: TDataSet);
begin
  if Assigned(FPrevBeforePost) then
    FPrevBeforePost(ADataSet);

  if fl_report_changed then
    CompressReport;
end;

procedure TEDIT_CL_RWREPORTS.CompressReport;
var
  MS1, MS2: TMemoryStream;
begin
  MS1 := TMemoryStream.Create;
  MS2 := TMemoryStream.Create;
  try
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).SaveToStream(MS1);
    MS1.Position := 0;
    DeflateStream(MS1, MS2);
    MS2.Position := 0;
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromStream(MS2);
  finally
    MS1.Free;
    MS2.Free;
  end;
end;

function TEDIT_CL_RWREPORTS.GetIfReadOnly: Boolean;
begin
  Result := (SecurityState = ctReadOnlyWithExport) or inherited GetIfReadOnly;
end;

procedure TEDIT_CL_RWREPORTS.OnGetSecurityStates(
  const States: TSecurityStateList);
begin
  States.InsertAfter(ctEnabled, ctReadOnlyWithExport, 'Read only with export');
end;

procedure TEDIT_CL_RWREPORTS.miSaveReportCollection(Sender: TObject);
var
  iRepCollection : IEvReportCollectionHelper;
  jClientNumber: integer;
begin
  jClientNumber := wwdsMaster.DataSet.FieldByName('CL_NBR').asInteger;
  iRepCollection := TEvRepCollectionHelper.Create();
  iRepCollection.SaveReportCollection(grReports, CH_DATABASE_CLIENT, jClientNumber, True);
end;

function TEDIT_CL_RWREPORTS.ReportNameOrderHead(ProposedName:string):string;
var
  s: string;
  Q: iEvQuery;
  sFixedProposedName:string;
  jCopy: integer;
  aList:TstringList;
  sCheck:string;
begin
    sFixedProposedName := AnsiReplaceStr(ProposedName,'''','''''');
    if TEvRepCollectionHelper.LineHasCopyHeader(sFixedProposedName) then
      sFixedProposedName := System.Copy(sFixedProposedName, Length('Copy ZZ - ')+1, Length(sFixedProposedName));

    s := 'SELECT t1.REPORT_DESCRIPTION, t1.CL_REPORT_WRITER_REPORTS_NBR '
+ ' FROM Cl_Report_Writer_Reports t1 '
+ ' where t1.REPORT_DESCRIPTION ='''+ sFixedProposedName
+ ''' or t1.REPORT_DESCRIPTION like '''
+TEvRepCollectionHelper.DoubleApostropheSQLStrLeft('Copy __ - '+sFixedProposedName, 40)
+ ''''
+ ' order by t1.CL_REPORT_WRITER_REPORTS_NBR';
// it is supposed to be absolute search, not relying on current situation
//        + 'where {AsOfNow<t1>}'

         ;

   aList := TStringList.Create;
   try
      // accept all names that were altered for imported collection already
      // they will be invisible for direct SQL query yet - transaction not confirmed
      aList.Text := TEvRepCollectionHelper.ListOfRelatedNamesfromDelta(
                                                  FImportCollectionDeltaNames
                                                 ,sFixedProposedName
                                                 ,40
                                                 );

      Q := TevQuery.Create(s);
      Q.Execute;
      Q.Result.First;
      while NOT Q.Result.EOF do
      begin
        aList.Add(Q.Result.Fields[0].AsString);
        Q.Result.Next;
      end;
      jCopy := alist.Count;

// final check in case numbers are not in order or missing
      sCheck := TEvRepCollectionHelper.CopyNameVariationEx(ProposedName,jCopy, 40);
      while
            (aList.IndexOf( sCheck ) >=0)
            or
            ( FImportCollectionDeltaNames.IndexOf(sCheck) >= 0)
      do
      begin
           jCopy := jCopy + 1;
           sCheck := TEvRepCollectionHelper.CopyNameVariationEx(ProposedName,jCopy, 40);
      end;
      Result := sCheck;

   finally
     aList.Free;
   end;
end;


procedure TEDIT_CL_RWREPORTS.LoadNewReportItem(Sender: TObject;
                                    var ReportType: string;
                                        jClienNumber:integer;
                                        ImportedNbr:integer;
                                    var ReportName:string;
                                    var AssignedNewNbr:integer
                                              );
var
  bOldRequired: boolean;
  bOldTypeRequired: boolean;
  g: TGuid;
begin
// this function perform ajustments to foreign import reports
  CreateGuid(g);
  bOldRequired := wwdsDetail.Dataset.FieldByName('REPORT_DESCRIPTION').Required;
  bOldTypeRequired := wwdsDetail.Dataset.FieldByName('REPORT_TYPE').Required;
  try
    wwdsDetail.Dataset.FieldByName('REPORT_DESCRIPTION').Required := False;
    wwdsDetail.Dataset.FieldByName('REPORT_TYPE').Required := False;
    wwdsDetail.Dataset.Insert;
    wwdsDetail.Dataset.FieldByName('REPORT_DESCRIPTION').AsString := GuidToString(g);
    wwdsDetail.Dataset.FieldByName('REPORT_TYPE').AsString := 'N';
// generates pseudo-index , changes do not yet go to server    
    wwdsDetail.Dataset.Post;
    wwdsDetail.Dataset.Edit;
    AssignedNewNbr := wwdsDetail.Dataset.Fields[0].AsInteger;
// we need to detect presence of the same name - checked within call
    ReportName :=  ReportNameOrderHead( ReportName );

    ReportType := Trim(ReportType);
    if Length(ReportType) = 0 then
      ReportType := 'N';

    if (ReportType[1] <> 'N') and (ReportType[1] <> 'A') and (ReportType[1] <>'H') then
      ReportType := 'N';

// populate list of delta-altered names
    fImportCollectionDeltaNames.Add(ReportName);
  finally
    wwdsDetail.Dataset.FieldByName('REPORT_DESCRIPTION').Required := bOldRequired;
    wwdsDetail.Dataset.FieldByName('REPORT_TYPE').Required := bOldTypeRequired;
  end;
end;

procedure TEDIT_CL_RWREPORTS.miLoadReportCollection(Sender: TObject);
var
  iRepCollection : IEvReportCollectionHelper;
  jClientNumber: integer;
begin
    //  this list is to keep names of updated imorted reports,
    //  invisible for transaction yet
    fImportCollectionDeltaNames := TStringList.Create;
    try
      jClientNumber := wwdsList.Dataset.FieldByName('CL_NBR').asInteger;
      iRepCollection := SSBReportCollection.TEvRepCollectionHelper.Create();
      iRepCollection.LoadCollection(grReports, CH_DATABASE_CLIENT,jClientNumber,wwdsDetail.DataSet,LoadNewReportItem);
    finally
      fImportCollectionDeltaNames.Free;
    end;
end;

initialization
  RegisterClass(TEDIT_CL_RWREPORTS);

end.
