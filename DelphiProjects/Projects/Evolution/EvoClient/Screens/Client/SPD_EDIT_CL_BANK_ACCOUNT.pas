// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_BANK_ACCOUNT;
{**************************************************************************}
{* unit: SPD_EDIT_CL_BANK_ACCOUNT.pas                                     *}
{* Description: Form used to maintain Client Bank Accounts information    *}
{*              from the Client -> Bank Accounts comboboxes.              *}
{*                                                                        *}
{* Copyright(c) PayData Payroll Services, Inc.                            *}
{*                                                                        *}
{* Author:  Tim Stevenson                                                 *}
{* Written: 04/25/1998                                                    *}
{* Updated: 04/26/1998                                                    *}
{* Updated: 05/22/1998  By: Tim Stevenson                                 *}
{*          Linked SB Banks to CL Bank Accounts                           *}
{*                                                                        *}
{**************************************************************************}

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, Dialogs, wwdblook,
  StdCtrls, ExtCtrls, DBCtrls, Mask, wwdbedit, Db, Wwdatsrc, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Controls, Classes, Graphics, EvUtils,
  SPackageEntry, SDataDictbureau, ISBasicClasses, SDDClasses,
  SDataDictclient, SDataDicttemp, EvContext, EvConsts, EvCommonInterfaces,
  EvDataSet, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, isUIDBImage;

type
  TEDIT_CL_BANK_ACCOUNT = class(TEDIT_CL_BASE)
    tshtBank_Account_Details: TTabSheet;
    lablBeginning_Balance: TevLabel;
    wwdgTeamMembers: TevDBGrid;
    bevCL_BANK1: TEvBevel;
    wwdeBeginning_Balance: TevDBEdit;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    pnlCLBrowseRight: TevPanel;
    fpCLRight: TisUIFashionPanel;
    pnlFPCLRightBody: TevPanel;
    pnlListOfCLBankAccount: TisUIFashionPanel;
    pnlCLBankAccountInfo: TisUIFashionPanel;
    Label1x: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    lablCustom_Bank_Account_Number: TevLabel;
    dedtCustom_Bank_Account_Number: TevDBEdit;
    lablNext_Check_Number: TevLabel;
    dedtNext_Check_Number: TevDBEdit;
    lablDescription: TevLabel;
    dmemDescript: TEvDBMemo;
    drgpBank_Account_Type: TevDBRadioGroup;
    lablSignature: TevLabel;
    dimgSignature: TevDBImage;
    bbtnLoad_Signature: TevBitBtn;
    evLabel1: TevLabel;
    edRecurringWire: TevDBEdit;
    lablLogo: TevLabel;
    dimgLogo: TevDBImage;
    bbtnLoad_Logo: TevBitBtn;
    wwDBGrid1: TevDBGrid;
    procedure bbtnLoad_SignatureClick(Sender: TObject);
    procedure bbtnLoad_LogoClick(Sender: TObject);
    procedure ResultFIELDS_CL_BANK_Function;
    procedure PageControl1Change(Sender: TObject);
    procedure wwDBGrid1AfterDrawCell(Sender: TwwCustomDBGrid;
      DrawCellInfo: TwwCustomDrawGridCellInfo);
    procedure wwdgTeamMembersAfterDrawCell(Sender: TwwCustomDBGrid;
      DrawCellInfo: TwwCustomDrawGridCellInfo);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure SetReadOnly(Value: Boolean); override;    
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure AfterClick(Kind: Integer); override;
    procedure DoBeforePost; override;
    function DeleteMessage: Word; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_BANK_ACCOUNT.SetReadOnly(Value: Boolean);
begin
  inherited;
  dimgSignature.SecurityRO := false;
  dimgSignature.ReadOnly := Value;
end;

procedure TEDIT_CL_BANK_ACCOUNT.Deactivate;
begin
  inherited;
  DM_CLIENT.CL_BANK_ACCOUNT.UserFilter :='';
end;

procedure TEDIT_CL_BANK_ACCOUNT.bbtnLoad_SignatureClick(Sender: TObject);
begin
  dimgSignature.ContexMenu_LoadFromFile;
end;

procedure TEDIT_CL_BANK_ACCOUNT.bbtnLoad_LogoClick(Sender: TObject);
begin
  dimgLogo.ContexMenu_LoadFromFile;
end;

function TEDIT_CL_BANK_ACCOUNT.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_BANK_ACCOUNT;
end;

function TEDIT_CL_BANK_ACCOUNT.GetInsertControl: TWinControl;
begin
  Result := dedtCUSTOM_BANK_ACCOUNT_NUMBER;
end;

procedure TEDIT_CL_BANK_ACCOUNT.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_BANKS, SupportDataSets, '');
end;

function TEDIT_CL_BANK_ACCOUNT.DeleteMessage: Word;
begin
  Result := mrNone;
  if EvMessage('Are you sure you want to delete this bank account?', mtConfirmation, mbOKCancel) = mrOK then
    Result := mrOK;
end;

procedure TEDIT_CL_BANK_ACCOUNT.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
  NavCommit:
    if (EvMessage('Do you want to update bank accounts for client agencies?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
      (Owner as TFramePackageTmpl).OpenFrame('TEDIT_CL_AGENCY');
  end;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CL_BANK_ACCOUNT.ResultFIELDS_CL_BANK_Function;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_CL_BANK_') <> stEnabled then
   Begin
    (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
    (Owner as TFramePackageTmpl).btnNavDelete.Enabled := false;
    dedtCustom_Bank_Account_Number.Enabled := False;
    wwDBLookupCombo1.Enabled := False;
   end;
End;


procedure TEDIT_CL_BANK_ACCOUNT.PageControl1Change(Sender: TObject);
begin
  inherited;
  ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CL_BANK_ACCOUNT.wwDBGrid1AfterDrawCell(
  Sender: TwwCustomDBGrid; DrawCellInfo: TwwCustomDrawGridCellInfo);
begin
  inherited;
     ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CL_BANK_ACCOUNT.wwdgTeamMembersAfterDrawCell(
  Sender: TwwCustomDBGrid; DrawCellInfo: TwwCustomDrawGridCellInfo);
begin
  inherited;
    ResultFIELDS_CL_BANK_Function;
end;

procedure TEDIT_CL_BANK_ACCOUNT.DoBeforePost;
var
  Q: IevQuery;
begin
  if (DM_CLIENT.CL_BANK_ACCOUNT.RecordCount > 0)
  and (DM_CLIENT.CL_BANK_ACCOUNT.NEXT_CHECK_NUMBER.OldValue <> DM_CLIENT.CL_BANK_ACCOUNT.NEXT_CHECK_NUMBER.Value) then
  begin
    Q := TevQuery.Create(
      'SELECT NEXT_CHECK_NUMBER FROM CL_BANK_ACCOUNT WHERE {AsOfNow<CL_BANK_ACCOUNT>} AND CL_BANK_ACCOUNT_NBR='+
        DM_CLIENT.CL_BANK_ACCOUNT.CL_BANK_ACCOUNT_NBR.AsString);
    Q.Execute;
    if Q.Result.RecordCount > 0 then
    begin
      if Q.Result.FieldByName('NEXT_CHECK_NUMBER').AsInteger <> DM_CLIENT.CL_BANK_ACCOUNT.NEXT_CHECK_NUMBER.OldValue then
        raise EevException.Create('This record has been updated by another user. Please refresh the screen and make your changes again.');
    end;
  end;
  inherited;
end;

initialization
  RegisterClass(TEDIT_CL_BANK_ACCOUNT);

end.
