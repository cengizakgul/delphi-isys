// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_DELIVERY_GROUPS;

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, StdCtrls, DBCtrls,
  ExtCtrls, wwdblook, Mask, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, Controls, Classes, wwdbedit, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents,
  ISBasicClasses, SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_DELIVERY_GROUPS = class(TEDIT_CL_BASE)
    tshtDelivery_Groups_Details: TTabSheet;
    lablDescription: TevLabel;
    lablPrimary_Method: TevLabel;
    dedtDescription: TevDBEdit;
    wwlcPrimary_Method: TevDBLookupCombo;
    drgpStuff: TevDBRadioGroup;
    dmemNotes: TEvDBMemo;
    LablNotes: TevLabel;
    TabSheet1x: TTabSheet;
    lablSecondary_Method: TevLabel;
    wwlcSecondary_Method: TevDBLookupCombo;
    wwlcPrimary_Address1: TevDBLookupCombo;
    wwPrimary_Address2: TevDBLookupCombo;
    wwlcPrimary_City: TevDBLookupCombo;
    wwlcPrimary_State: TevDBLookupCombo;
    lablPrimary_State: TevLabel;
    lablPrimary_City: TevLabel;
    lablPrimary_Address2: TevLabel;
    lablPrimary_Address1: TevLabel;
    lablSecondary_Address1: TevLabel;
    wwlcSecondary_Address1: TevDBLookupCombo;
    lablSecondary_Address2: TevLabel;
    wwlcSecondary_Address2: TevDBLookupCombo;
    lablSecondary_City: TevLabel;
    wwlcSecondary_City: TevDBLookupCombo;
    lablSecondary_State: TevLabel;
    wwlcSecondary_State: TevDBLookupCombo;
    lablPrimary_Method_Service_Description: TevLabel;
    wwlcPrimary_Method_Service_Description: TevDBLookupCombo;
    wwlcSecondary_Method_Service_Description: TevDBLookupCombo;
    lablSecondary_Method_Service_Description: TevLabel;
    bev_CL_del1: TEvBevel;
    bev_CL_del2: TEvBevel;
    dsDeliveryMethod: TevClientDataSet;
    wwlcPrimary_Zip_Code: TevDBLookupCombo;
    lablPrimary_ZIP: TevLabel;
    lablSecondary_ZIP: TevLabel;
    wwlcSecondary_ZIP: TevDBLookupCombo;
    wwDBGrid2: TevDBGrid;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure AfterDataSetsReopen; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_DELIVERY_GROUPS.AfterDataSetsReopen;
begin
  inherited;
  dsDeliveryMethod.CloneCursor(DM_CLIENT.CL_DELIVERY_METHOD, False);
end;

procedure TEDIT_CL_DELIVERY_GROUPS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_DELIVERY_METHOD, aDS);
  inherited;
end;

function TEDIT_CL_DELIVERY_GROUPS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_DELIVERY_GROUP;
end;

function TEDIT_CL_DELIVERY_GROUPS.GetInsertControl: TWinControl;
begin
  Result := dedtDescription;
end;

initialization
  RegisterClass(TEDIT_CL_DELIVERY_GROUPS);

end.
