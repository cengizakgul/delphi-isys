// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_PENSION;

interface

uses
  SDataStructure,  SPD_EDIT_CL_BASE, wwdblook, StdCtrls,
  ExtCtrls, DBCtrls, Wwdotdot, Wwdbcomb, Wwdbspin, Mask, wwdbedit, Db,
  Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Controls, Classes,
  SDDClasses, ISBasicClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBComboBox, isUIwwDBEdit, ImgList,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel, Forms,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, sCopyHelper,
  sClientCopyHelper;

type
  TEDIT_CL_PENSION = class(TEDIT_CL_BASE)
    tshtPension_Details: TTabSheet;
    lablPlan_ID: TevLabel;
    lablPolicy_ID: TevLabel;
    lablName: TevLabel;
    lablType: TevLabel;
    lablClient_Agency: TevLabel;
    lablMax_Allowed_Percentage: TevLabel;
    lablProbation_Period_Days: TevLabel;
    lablMinimum_Age: TevLabel;
    lablE_D_Group: TevLabel;
    dedtPlan_ID: TevDBEdit;
    dedtPolicy_ID: TevDBEdit;
    dedtName: TevDBEdit;
    wwdeMax_Allowed_Percentage: TevDBEdit;
    wwseProbation_Period_Days: TevDBEdit;
    wwseMinimum_Age: TevDBEdit;
    wwcbType: TevDBComboBox;
    drgpDiscontinue_Match_EE_Reaches_Limit: TevDBRadioGroup;
    drgpMark_Pension_W2: TevDBRadioGroup;
    wwlcClient_Agency: TevDBLookupCombo;
    wwlcE_D_Group: TevDBLookupCombo;
    bev_cl_ed_pen1: TEvBevel;
    CopyHelper: TClientCopyHelper;
    wwDBGrid1x: TevDBGrid;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_PENSION.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  inherited;
end;

function TEDIT_CL_PENSION.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_PENSION;
end;

function TEDIT_CL_PENSION.GetInsertControl: TWinControl;
begin
  Result := dedtName;//dedtPLAN_ID;
end;

initialization
  RegisterClass(TEDIT_CL_PENSION);

end.
