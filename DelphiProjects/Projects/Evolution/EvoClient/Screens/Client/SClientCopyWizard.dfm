inherited ClientCopyWizard: TClientCopyWizard
  Left = 425
  Top = 192
  Caption = 'Copy Wizard - '
  ClientHeight = 534
  ClientWidth = 794
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TevPanel
    Width = 794
    Height = 534
    inherited Panel2: TevPanel
      Top = 484
      Width = 794
      inherited btBack: TevBitBtn
        Left = 623
        ModalResult = 0
      end
      inherited btNext: TevBitBtn
        Left = 703
        ModalResult = 0
      end
      inherited btCancel: TevBitBtn
        Left = 542
      end
    end
    inherited Panel4: TevPanel
      Width = 794
      Height = 484
      inherited pctCopyWizard: TevPageControl
        Width = 792
        Height = 482
        ActivePage = tshCL
        inherited tshSelectBase: TTabSheet
          inherited evSplitter1: TevSplitter
            Height = 451
          end
          inherited evPanel1: TevPanel
            Height = 451
            inherited gdClient: TevDBGrid
              Height = 451
              IniAttributes.SectionName = 'TClientCopyWizard\gdClient'
            end
          end
          inherited gdCompany: TevDBGrid
            Width = 524
            Height = 451
            IniAttributes.SectionName = 'TClientCopyWizard\gdCompany'
            Visible = False
          end
        end
        object tshCL: TTabSheet
          Caption = 'tshCL'
          ImageIndex = 1
          object lblCLxxCUSTOM_CLIENT_NUMBER: TevLabel
            Tag = 1
            Left = 8
            Top = 88
            Width = 63
            Height = 16
            Caption = '~Client Code'
            FocusControl = CLxxCUSTOM_CLIENT_NUMBER
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCLxxNAME: TevLabel
            Tag = 3
            Left = 8
            Top = 128
            Width = 37
            Height = 16
            Caption = '~Name'
            FocusControl = CLxxNAME
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCLxxADDRESS1: TevLabel
            Tag = 4
            Left = 8
            Top = 168
            Width = 53
            Height = 16
            Caption = '~Address1'
            FocusControl = CLxxADDRESS1
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCLxxADDRESS2: TevLabel
            Tag = 5
            Left = 8
            Top = 208
            Width = 44
            Height = 13
            Caption = 'Address2'
            FocusControl = CLxxADDRESS2
          end
          object lblCLxxCITY: TevLabel
            Tag = 6
            Left = 8
            Top = 248
            Width = 26
            Height = 16
            Caption = '~City'
            FocusControl = CLxxCITY
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCLxxSTATE: TevLabel
            Tag = 7
            Left = 152
            Top = 248
            Width = 34
            Height = 16
            Caption = '~State'
            FocusControl = CLxxSTATE
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCLxxZIP_CODE: TevLabel
            Tag = 8
            Left = 200
            Top = 248
            Width = 52
            Height = 16
            Caption = '~Zip Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCLxxSTART_DATE: TevLabel
            Tag = 2
            Left = 152
            Top = 88
            Width = 57
            Height = 16
            Caption = '~Start Date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CLxxCUSTOM_CLIENT_NUMBER: TevDBEdit
            Tag = 1
            Left = 8
            Top = 104
            Width = 124
            Height = 21
            DataField = 'CUSTOM_CLIENT_NUMBER'
            DataSource = dsCL
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CLxxNAME: TevDBEdit
            Tag = 2
            Left = 8
            Top = 144
            Width = 281
            Height = 21
            DataField = 'NAME'
            DataSource = dsCL
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CLxxADDRESS1: TevDBEdit
            Tag = 3
            Left = 8
            Top = 184
            Width = 281
            Height = 21
            DataField = 'ADDRESS1'
            DataSource = dsCL
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CLxxADDRESS2: TevDBEdit
            Tag = 4
            Left = 8
            Top = 224
            Width = 281
            Height = 21
            DataField = 'ADDRESS2'
            DataSource = dsCL
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CLxxCITY: TevDBEdit
            Tag = 5
            Left = 8
            Top = 264
            Width = 129
            Height = 21
            DataField = 'CITY'
            DataSource = dsCL
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CLxxSTATE: TevDBEdit
            Tag = 6
            Left = 152
            Top = 264
            Width = 33
            Height = 21
            DataField = 'STATE'
            DataSource = dsCL
            Picture.PictureMask = '*{&,@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CLxxZIP_CODE: TevDBEdit
            Tag = 7
            Left = 200
            Top = 264
            Width = 89
            Height = 21
            DataField = 'ZIP_CODE'
            DataSource = dsCL
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object grbxClient_PrimaryFax: TevGroupBox
            Left = 8
            Top = 296
            Width = 281
            Height = 73
            Caption = 'Primary Fax'
            TabOrder = 8
            object lblCLxxFAX1: TevLabel
              Tag = 9
              Left = 16
              Top = 24
              Width = 37
              Height = 13
              Caption = 'Number'
              FocusControl = CLxxFAX1
            end
            object lblCLxxFAX1_DESCRIPTION: TevLabel
              Tag = 10
              Left = 144
              Top = 24
              Width = 53
              Height = 13
              Caption = 'Description'
              FocusControl = CLxxFAX1_DESCRIPTION
            end
            object CLxxFAX1_DESCRIPTION: TevDBEdit
              Tag = 9
              Left = 144
              Top = 40
              Width = 121
              Height = 21
              DataField = 'FAX1_DESCRIPTION'
              DataSource = dsCL
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CLxxFAX1: TevDBEdit
              Tag = 8
              Left = 16
              Top = 40
              Width = 121
              Height = 21
              DataField = 'FAX1'
              DataSource = dsCL
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
          end
          object grbxClient_SecondaryFax: TevGroupBox
            Left = 8
            Top = 376
            Width = 281
            Height = 73
            Caption = 'Secondary Fax'
            TabOrder = 9
            object lblCLxxFAX2: TevLabel
              Tag = 11
              Left = 16
              Top = 24
              Width = 37
              Height = 13
              Caption = 'Number'
              FocusControl = CLxxFAX2
            end
            object lblCLxxFAX2_DESCRIPTION: TevLabel
              Tag = 12
              Left = 144
              Top = 24
              Width = 53
              Height = 13
              Caption = 'Description'
              FocusControl = CLxxFAX2_DESCRIPTION
            end
            object CLxxFAX2_DESCRIPTION: TevDBEdit
              Tag = 11
              Left = 144
              Top = 40
              Width = 121
              Height = 21
              DataField = 'FAX2_DESCRIPTION'
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CLxxFAX2: TevDBEdit
              Tag = 10
              Left = 16
              Top = 40
              Width = 121
              Height = 21
              DataField = 'FAX2'
              DataSource = dsCL
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
          end
          object grbxClient_SecondaryContact: TevGroupBox
            Left = 312
            Top = 295
            Width = 289
            Height = 154
            Caption = 'Secondary Contact'
            TabOrder = 11
            object lblCLxxCONTACT2: TevLabel
              Tag = 17
              Left = 16
              Top = 24
              Width = 28
              Height = 13
              Caption = 'Name'
              FocusControl = CLxxCONTACT2
            end
            object lblCLxxPHONE2: TevLabel
              Tag = 18
              Left = 16
              Top = 64
              Width = 31
              Height = 13
              Caption = 'Phone'
              FocusControl = CLxxPHONE2
            end
            object lblCLxxDESCRIPTION2: TevLabel
              Tag = 19
              Left = 144
              Top = 64
              Width = 53
              Height = 13
              Caption = 'Description'
              FocusControl = CLxxDESCRIPTION2
            end
            object lblCLxxE_MAIL_ADDRESS2: TevLabel
              Tag = 20
              Left = 16
              Top = 104
              Width = 28
              Height = 13
              Caption = 'E-mail'
              FocusControl = CLxxE_MAIL_ADDRESS2
            end
            object CLxxCONTACT2: TevDBEdit
              Tag = 16
              Left = 16
              Top = 40
              Width = 249
              Height = 21
              DataField = 'CONTACT2'
              DataSource = dsCL
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CLxxDESCRIPTION2: TevDBEdit
              Tag = 18
              Left = 144
              Top = 80
              Width = 121
              Height = 21
              DataField = 'DESCRIPTION2'
              DataSource = dsCL
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 2
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CLxxE_MAIL_ADDRESS2: TevDBEdit
              Tag = 19
              Left = 16
              Top = 120
              Width = 249
              Height = 21
              DataField = 'E_MAIL_ADDRESS2'
              DataSource = dsCL
              TabOrder = 3
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CLxxPHONE2: TevDBEdit
              Tag = 17
              Left = 16
              Top = 80
              Width = 121
              Height = 21
              DataField = 'PHONE2'
              DataSource = dsCL
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
          end
          object grbxClient_PrimaryContact: TevGroupBox
            Left = 312
            Top = 130
            Width = 289
            Height = 153
            Caption = 'Primary Contact'
            TabOrder = 10
            object lblCLxxCONTACT1: TevLabel
              Tag = 13
              Left = 16
              Top = 24
              Width = 37
              Height = 16
              Caption = '~Name'
              FocusControl = CLxxCONTACT1
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object lblCLxxPHONE1: TevLabel
              Tag = 14
              Left = 16
              Top = 64
              Width = 31
              Height = 13
              Caption = 'Phone'
              FocusControl = CLxxPHONE1
            end
            object lblCLxxDESCRIPTION1: TevLabel
              Tag = 15
              Left = 144
              Top = 64
              Width = 53
              Height = 13
              Caption = 'Description'
              FocusControl = CLxxDESCRIPTION1
            end
            object lblCLxxE_MAIL_ADDRESS1: TevLabel
              Tag = 16
              Left = 16
              Top = 104
              Width = 28
              Height = 13
              Caption = 'E-mail'
              FocusControl = CLxxE_MAIL_ADDRESS1
            end
            object CLxxCONTACT1: TevDBEdit
              Tag = 12
              Left = 16
              Top = 40
              Width = 257
              Height = 21
              DataField = 'CONTACT1'
              DataSource = dsCL
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CLxxDESCRIPTION1: TevDBEdit
              Tag = 14
              Left = 144
              Top = 80
              Width = 129
              Height = 21
              DataField = 'DESCRIPTION1'
              DataSource = dsCL
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 2
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CLxxE_MAIL_ADDRESS1: TevDBEdit
              Tag = 15
              Left = 16
              Top = 120
              Width = 257
              Height = 21
              DataField = 'E_MAIL_ADDRESS1'
              DataSource = dsCL
              TabOrder = 3
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CLxxPHONE1: TevDBEdit
              Tag = 13
              Left = 16
              Top = 80
              Width = 121
              Height = 21
              DataField = 'PHONE1'
              DataSource = dsCL
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
          end
          object grCL: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 73
            DisableThemesInTitle = False
            Selected.Strings = (
              'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'
              'NAME'#9'40'#9'Name')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TClientCopyWizard\grCL'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCL
            KeyOptions = [dgEnterToTab]
            ReadOnly = False
            TabOrder = 12
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            Sorting = False
            NoFire = False
          end
          object CLxxSTART_DATE: TevDBDateTimePicker
            Left = 152
            Top = 104
            Width = 97
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'START_DATE'
            DataSource = dsCL
            Epoch = 1950
            ShowButton = True
            TabOrder = 1
          end
        end
        object tshCL_BANK_ACCOUNT: TTabSheet
          Caption = 'tshCL_BANK_ACCOUNT'
          ImageIndex = 3
          object lblCL_BANK_ACCOUNTxxCUSTOM_BANK_ACCOUNT_NUMBER: TevLabel
            Tag = 2
            Left = 8
            Top = 152
            Width = 155
            Height = 16
            Caption = '~Custom Bank Account Number'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCL_BANK_ACCOUNTxxNEXT_CHECK_NUMBER: TevLabel
            Tag = 3
            Left = 8
            Top = 200
            Width = 105
            Height = 16
            Caption = '~Next Check Number'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCL_BANK_ACCOUNTxxBank_Name: TevLabel
            Tag = 1
            Left = 336
            Top = 152
            Width = 56
            Height = 13
            Caption = 'Bank Name'
            Visible = False
            WordWrap = True
          end
          object CL_BANK_ACCOUNTxxCUSTOM_BANK_ACCOUNT_NUMBER: TevDBEdit
            Left = 8
            Top = 168
            Width = 281
            Height = 21
            DataField = 'CUSTOM_BANK_ACCOUNT_NUMBER'
            DataSource = dsCL_BANK_ACCOUNT
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_BANK_ACCOUNTxxNEXT_CHECK_NUMBER: TevDBEdit
            Left = 8
            Top = 216
            Width = 281
            Height = 21
            DataField = 'NEXT_CHECK_NUMBER'
            DataSource = dsCL_BANK_ACCOUNT
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object grBankAccount: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'32'#9'Bank Account Number'#9'F'#9
              'aba_nbr_lookup'#9'20'#9'ABA Nbr'#9'F'#9
              'Print_Name_Lookup'#9'20'#9'Print Name'#9'F'#9
              'SIGNATURE_CL_BLOB_NBR'#9'10'#9'Signature Cl Blob Nbr'#9'F'#9
              'LOGO_CL_BLOB_NBR'#9'10'#9'Logo Cl Blob Nbr'#9'F'#9)
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TClientCopyWizard\grBankAccount'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCL_BANK_ACCOUNT
            KeyOptions = [dgEnterToTab]
            ReadOnly = False
            TabOrder = 2
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            Sorting = False
            NoFire = False
          end
        end
        object tshCL_BILLING: TTabSheet
          Caption = 'tshCL_BILLING'
          ImageIndex = 4
          object lblCL_BILLINGxxNAME: TevLabel
            Tag = 1
            Left = 8
            Top = 160
            Width = 37
            Height = 16
            Caption = '~Name'
            FocusControl = CL_BILLINGxxNAME
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCL_BILLINGxxADDRESS1: TevLabel
            Tag = 2
            Left = 8
            Top = 200
            Width = 47
            Height = 13
            Caption = 'Address 1'
            FocusControl = CL_BILLINGxxADDRESS1
          end
          object lblCL_BILLINGxxADDRESS2: TevLabel
            Tag = 3
            Left = 8
            Top = 240
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = CL_BILLINGxxADDRESS2
          end
          object lblCL_BILLINGxxCITY: TevLabel
            Tag = 4
            Left = 8
            Top = 280
            Width = 17
            Height = 13
            Caption = 'City'
            FocusControl = CL_BILLINGxxCITY
          end
          object lblCL_BILLINGxxSTATE: TevLabel
            Tag = 5
            Left = 144
            Top = 280
            Width = 25
            Height = 13
            Caption = 'State'
            FocusControl = CL_BILLINGxxSTATE
          end
          object lblCL_BILLINGxxZIP_CODE: TevLabel
            Tag = 6
            Left = 184
            Top = 280
            Width = 43
            Height = 13
            Caption = 'Zip Code'
            FocusControl = CL_BILLINGxxZIP_CODE
          end
          object grBilling: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'NAME'#9'40'#9'Name')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TClientCopyWizard\grBilling'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCL_BILLING
            KeyOptions = [dgEnterToTab]
            ReadOnly = False
            TabOrder = 7
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            Sorting = False
            NoFire = False
          end
          object CL_BILLINGxxNAME: TevDBEdit
            Left = 10
            Top = 176
            Width = 249
            Height = 21
            HelpContext = 7502
            DataField = 'NAME'
            DataSource = dsCL_BILLING
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_BILLINGxxADDRESS1: TevDBEdit
            Left = 8
            Top = 216
            Width = 249
            Height = 21
            HelpContext = 7502
            DataField = 'ADDRESS1'
            DataSource = dsCL_BILLING
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_BILLINGxxADDRESS2: TevDBEdit
            Left = 8
            Top = 256
            Width = 249
            Height = 21
            HelpContext = 7502
            DataField = 'ADDRESS2'
            DataSource = dsCL_BILLING
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_BILLINGxxCITY: TevDBEdit
            Left = 8
            Top = 296
            Width = 129
            Height = 21
            HelpContext = 7502
            DataField = 'CITY'
            DataSource = dsCL_BILLING
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_BILLINGxxSTATE: TevDBEdit
            Left = 144
            Top = 296
            Width = 33
            Height = 21
            HelpContext = 7502
            DataField = 'STATE'
            DataSource = dsCL_BILLING
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_BILLINGxxZIP_CODE: TevDBEdit
            Left = 184
            Top = 296
            Width = 73
            Height = 21
            HelpContext = 7502
            DataField = 'ZIP_CODE'
            DataSource = dsCL_BILLING
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object grbxContact_Info: TevGroupBox
            Left = 280
            Top = 168
            Width = 313
            Height = 209
            Caption = 'Contact Info'
            TabOrder = 6
            object lblCL_BILLINGxxCONTACT: TevLabel
              Tag = 7
              Left = 16
              Top = 16
              Width = 37
              Height = 16
              Caption = '~Name'
              FocusControl = CL_BILLINGxxCONTACT
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object lblCL_BILLINGxxPHONE: TevLabel
              Tag = 8
              Left = 16
              Top = 64
              Width = 31
              Height = 13
              Caption = 'Phone'
              FocusControl = CL_BILLINGxxPHONE
            end
            object lblCL_BILLINGxxDESCRIPTION: TevLabel
              Tag = 9
              Left = 152
              Top = 64
              Width = 53
              Height = 13
              Caption = 'Description'
              FocusControl = CL_BILLINGxxDESCRIPTION
            end
            object lblCL_BILLINGxxFAX: TevLabel
              Tag = 10
              Left = 16
              Top = 112
              Width = 17
              Height = 13
              Caption = 'Fax'
              FocusControl = CL_BILLINGxxFAX
            end
            object lblCL_BILLINGxxFAX_DESCRIPTION: TevLabel
              Tag = 11
              Left = 152
              Top = 112
              Width = 53
              Height = 13
              Caption = 'Description'
              FocusControl = CL_BILLINGxxFAX_DESCRIPTION
            end
            object lblCL_BILLINGxxE_MAIL_ADDRESS: TevLabel
              Tag = 12
              Left = 16
              Top = 160
              Width = 28
              Height = 13
              Caption = 'E-mail'
              FocusControl = CL_BILLINGxxE_MAIL_ADDRESS
            end
            object CL_BILLINGxxCONTACT: TevDBEdit
              Left = 16
              Top = 32
              Width = 281
              Height = 21
              HelpContext = 7502
              DataField = 'CONTACT'
              DataSource = dsCL_BILLING
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 0
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CL_BILLINGxxPHONE: TevDBEdit
              Left = 16
              Top = 80
              Width = 121
              Height = 21
              HelpContext = 7502
              DataField = 'PHONE'
              DataSource = dsCL_BILLING
              TabOrder = 1
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CL_BILLINGxxDESCRIPTION: TevDBEdit
              Left = 152
              Top = 80
              Width = 145
              Height = 21
              HelpContext = 7202
              DataField = 'DESCRIPTION'
              DataSource = dsCL_BILLING
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 2
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CL_BILLINGxxFAX: TevDBEdit
              Left = 16
              Top = 128
              Width = 121
              Height = 21
              HelpContext = 7502
              DataField = 'FAX'
              DataSource = dsCL_BILLING
              TabOrder = 3
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CL_BILLINGxxFAX_DESCRIPTION: TevDBEdit
              Left = 152
              Top = 128
              Width = 145
              Height = 21
              HelpContext = 7502
              DataField = 'FAX_DESCRIPTION'
              DataSource = dsCL_BILLING
              Picture.PictureMaskFromDataSet = False
              Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
              TabOrder = 4
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
            object CL_BILLINGxxE_MAIL_ADDRESS: TevDBEdit
              Left = 16
              Top = 176
              Width = 281
              Height = 21
              HelpContext = 7502
              DataField = 'E_MAIL_ADDRESS'
              DataSource = dsCL_BILLING
              TabOrder = 5
              UnboundDataType = wwDefault
              WantReturns = False
              WordWrap = False
              Glowing = False
            end
          end
          object evButton1: TevBitBtn
            Left = 8
            Top = 328
            Width = 249
            Height = 33
            Caption = 'Get Information from Client Table'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
            OnClick = evButton1Click
            Color = clBlack
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
              8888DFFFFFFFFFFFFFFF0444444444400000888888888888888804444474447F
              FFF0888888D888DDDDD80F44477447FFFFF08D888DD88DDDDDD80FF474447888
              88F08DD8D888D88888D80FFF77787FFFFFF08DDD8DD8DDDDDDD80FF477478788
              88F08DD8DD8D8D8888D80F47777778FFFFF08D8DDDDDD8DDDDD80447777778FF
              88F0888DDDDDD8DD88D804447474747FFFF08888D8D8D8DDDDD804447777747F
              FFF08888DDDDD8DDDDD804444777447FFF0888888DDD88DDDD8DD44444444000
              008DD8888888888888DDD44444448DDDDDDDD8888888DDDDDDDDDD444448DDDD
              DDDDDD88888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object tshCL_DELIVERY_METHOD: TTabSheet
          Caption = 'tshCL_DELIVERY_METHOD'
          ImageIndex = 5
          object lblCL_DELIVERY_METHODxxNAME: TevLabel
            Tag = 1
            Left = 8
            Top = 160
            Width = 93
            Height = 16
            Caption = '~Destination Name'
            FocusControl = CL_DELIVERY_METHODxxNAME
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCL_DELIVERY_METHODxxADDRESS1: TevLabel
            Tag = 2
            Left = 8
            Top = 208
            Width = 47
            Height = 13
            Caption = 'Address 1'
            FocusControl = CL_DELIVERY_METHODxxADDRESS1
          end
          object lblCL_DELIVERY_METHODxxADDRESS2: TevLabel
            Tag = 3
            Left = 8
            Top = 256
            Width = 47
            Height = 13
            Caption = 'Address 2'
            FocusControl = CL_DELIVERY_METHODxxADDRESS2
          end
          object lblCL_DELIVERY_METHODxxCITY: TevLabel
            Tag = 4
            Left = 8
            Top = 304
            Width = 17
            Height = 13
            Caption = 'City'
            FocusControl = CL_DELIVERY_METHODxxCITY
          end
          object lblCL_DELIVERY_METHODxxSTATE: TevLabel
            Tag = 5
            Left = 160
            Top = 304
            Width = 25
            Height = 13
            Caption = 'State'
          end
          object lblCL_DELIVERY_METHODxxZIP_CODE: TevLabel
            Tag = 6
            Left = 200
            Top = 304
            Width = 43
            Height = 13
            Caption = 'Zip Code'
          end
          object lblCL_DELIVERY_METHODxxATTENTION: TevLabel
            Tag = 7
            Left = 288
            Top = 160
            Width = 51
            Height = 16
            Caption = '~Attention'
            FocusControl = CL_DELIVERY_METHODxxATTENTION
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCL_DELIVERY_METHODxxPHONE_NUMBER: TevLabel
            Tag = 8
            Left = 288
            Top = 208
            Width = 71
            Height = 13
            Caption = 'Phone Number'
          end
          object CL_DELIVERY_METHODxxNAME: TevDBEdit
            Left = 8
            Top = 176
            Width = 265
            Height = 21
            DataField = 'NAME'
            DataSource = dsCL_DELIVERY_METHOD
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_DELIVERY_METHODxxADDRESS1: TevDBEdit
            Left = 8
            Top = 224
            Width = 265
            Height = 21
            DataField = 'ADDRESS1'
            DataSource = dsCL_DELIVERY_METHOD
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_DELIVERY_METHODxxADDRESS2: TevDBEdit
            Left = 8
            Top = 272
            Width = 265
            Height = 21
            DataField = 'ADDRESS2'
            DataSource = dsCL_DELIVERY_METHOD
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_DELIVERY_METHODxxCITY: TevDBEdit
            Left = 8
            Top = 320
            Width = 145
            Height = 21
            DataField = 'CITY'
            DataSource = dsCL_DELIVERY_METHOD
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_DELIVERY_METHODxxSTATE: TevDBEdit
            Left = 160
            Top = 320
            Width = 33
            Height = 21
            DataField = 'STATE'
            DataSource = dsCL_DELIVERY_METHOD
            TabOrder = 4
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_DELIVERY_METHODxxZIP_CODE: TevDBEdit
            Left = 200
            Top = 320
            Width = 73
            Height = 21
            DataField = 'ZIP_CODE'
            DataSource = dsCL_DELIVERY_METHOD
            TabOrder = 5
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_DELIVERY_METHODxxATTENTION: TevDBEdit
            Left = 288
            Top = 176
            Width = 265
            Height = 21
            DataField = 'ATTENTION'
            DataSource = dsCL_DELIVERY_METHOD
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 6
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_DELIVERY_METHODxxPHONE_NUMBER: TevDBEdit
            Left = 288
            Top = 224
            Width = 121
            Height = 21
            DataField = 'PHONE_NUMBER'
            DataSource = dsCL_DELIVERY_METHOD
            TabOrder = 7
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBGrid1: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'ServiceDescription_Lookup'#9'38'#9'Delivery Service'#9#9
              'NAME'#9'40'#9'Method Name'#9#9
              'STATE'#9'3'#9'State'#9#9)
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TClientCopyWizard\evDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCL_DELIVERY_METHOD
            TabOrder = 8
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            Sorting = False
            NoFire = False
          end
          object evButton2: TevBitBtn
            Left = 288
            Top = 309
            Width = 249
            Height = 33
            Caption = 'Get Information from Client Table'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
            OnClick = evButton2Click
            Color = clBlack
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
              8888DFFFFFFFFFFFFFFF0444444444400000888888888888888804444474447F
              FFF0888888D888DDDDD80F44477447FFFFF08D888DD88DDDDDD80FF474447888
              88F08DD8D888D88888D80FFF77787FFFFFF08DDD8DD8DDDDDDD80FF477478788
              88F08DD8DD8D8D8888D80F47777778FFFFF08D8DDDDDD8DDDDD80447777778FF
              88F0888DDDDDD8DD88D804447474747FFFF08888D8D8D8DDDDD804447777747F
              FFF08888DDDDD8DDDDD804444777447FFF0888888DDD88DDDD8DD44444444000
              008DD8888888888888DDD44444448DDDDDDDD8888888DDDDDDDDDD444448DDDD
              DDDDDD88888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object tshCL_AGENCY: TTabSheet
          Caption = 'tshCL_AGENCY'
          ImageIndex = 2
          object lblCL_AGENCYxxCL_BANK_ACCOUNT_NBR: TevLabel
            Tag = 2
            Left = 8
            Top = 160
            Width = 106
            Height = 16
            Caption = '~Client Bank Account'
            FocusControl = CL_AGENCYxxCL_BANK_ACCOUNT_NBR
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblCL_AGENCYxxAgency_Name: TevLabel
            Tag = 1
            Left = 168
            Top = 160
            Width = 67
            Height = 13
            Caption = 'Agency Name'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = False
          end
          object grAgency: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            ControlType.Strings = (
              'CL_BANK_ACCOUNT_NBR;CustomEdit;evDBLookupCombo1;F'
              'ClBankAccount;CustomEdit;evDBLookupCombo1;F')
            Selected.Strings = (
              'Agency_Name'#9'40'#9'Name'#9'F'#9)
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TClientCopyWizard\grAgency'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCL_AGENCY
            KeyOptions = [dgEnterToTab]
            ReadOnly = False
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            Sorting = False
            NoFire = False
          end
          object CL_AGENCYxxCL_BANK_ACCOUNT_NBR: TevDBLookupCombo
            Left = 8
            Top = 176
            Width = 145
            Height = 21
            HelpContext = 12015
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'ACCOUNT NUMBER'
              'BANK_ACCOUNT_TYPE'#9'1'#9'TYPE')
            DataField = 'CL_BANK_ACCOUNT_NBR'
            DataSource = dsCL_AGENCY
            LookupTable = TmpDS
            LookupField = 'CL_BANK_ACCOUNT_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
        object tshCL_PENSION: TTabSheet
          Caption = 'tshCL_PENSION'
          ImageIndex = 8
          object lblCL_PENSIONxxPLAN_ID: TevLabel
            Tag = 2
            Left = 8
            Top = 160
            Width = 35
            Height = 13
            Caption = 'Plan ID'
            FocusControl = CL_PENSIONxxPLAN_ID
          end
          object lblCL_PENSIONxxPOLICY_ID: TevLabel
            Tag = 3
            Left = 8
            Top = 208
            Width = 42
            Height = 13
            Caption = 'Policy ID'
            FocusControl = CL_PENSIONxxPOLICY_ID
          end
          object lblCL_PENSIONxxNAME: TevLabel
            Tag = 1
            Left = 280
            Top = 160
            Width = 28
            Height = 13
            Caption = 'Name'
            Visible = False
          end
          object grPension: TevDBGrid
            Left = 8
            Top = 8
            Width = 593
            Height = 137
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'NAME'#9'20'#9'Name'
              'PLAN_ID'#9'20'#9'Plan ID'
              'POLICY_ID'#9'20'#9'Policy ID')
            IniAttributes.Enabled = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TClientCopyWizard\grPension'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsCL_PENSION
            TabOrder = 2
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            Sorting = False
            NoFire = False
          end
          object CL_PENSIONxxPLAN_ID: TevDBEdit
            Left = 8
            Top = 176
            Width = 244
            Height = 21
            DataField = 'PLAN_ID'
            DataSource = dsCL_PENSION
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object CL_PENSIONxxPOLICY_ID: TevDBEdit
            Left = 8
            Top = 224
            Width = 244
            Height = 21
            HelpContext = 9503
            DataField = 'POLICY_ID'
            DataSource = dsCL_PENSION
            Picture.PictureMaskFromDataSet = False
            Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
            TabOrder = 1
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
        end
        object tshEndPage: TTabSheet
          Caption = 'tshEndPage'
          ImageIndex = 16
        end
      end
    end
  end
  inherited dsClient: TevDataSource
    Left = 26
    Top = 37
  end
  inherited TmpDS: TevClientDataSet
    Left = 533
    Top = 68
  end
  inherited dsCompany: TevDataSource
    Left = 189
    Top = 44
  end
  object dsCL: TevDataSource [4]
    Left = 520
    Top = 64
  end
  object dsCL_BANK_ACCOUNT: TevDataSource [5]
    Left = 424
    Top = 40
  end
  object dsCL_BILLING: TevDataSource [6]
    Left = 328
    Top = 56
  end
  object dsCL_DELIVERY_METHOD: TevDataSource [7]
    Left = 229
    Top = 68
  end
  object dsCL_AGENCY: TevDataSource [8]
    DataSet = DM_CL_AGENCY.CL_AGENCY
    Left = 461
    Top = 44
  end
  object dsCL_PENSION: TevDataSource [9]
    Left = 285
    Top = 68
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 397
    Top = 140
  end
end
