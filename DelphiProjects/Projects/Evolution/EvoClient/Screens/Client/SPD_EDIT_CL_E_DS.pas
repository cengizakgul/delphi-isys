// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_E_DS;

interface

uses
sshowdataset,
  SDataStructure,  SPD_EDIT_CL_BASE, Db,
  wwdbdatetimepicker, StdCtrls, DBCtrls, wwdbedit, Wwdbcomb, ExtCtrls,
  wwdblook, Wwdotdot, Mask, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, Controls, Classes, EvUtils, EvConsts, SFieldCodeValues, Graphics,
  ActnList, Dialogs, SPackageEntry, sysutils, Menus, SPD_EDIT_COPY, TypInfo,
  EvSecElement, Forms, SPD_MiniNavigationFrame, EvTypes, sClientCopyHelper, sCopier,
  sCopyHelper, Variants, SDDClasses, ISBasicClasses, SPD_EDFields,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, isVCLBugFix,
  EvDataAccessComponents, SDataDictsystem, SDataDictclient, SDataDicttemp,
  Wwdbspin, EvExceptions, gdystrset, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIDBMemo, isUIwwDBComboBox,
  isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CL_E_DS = class(TEDIT_CL_BASE)
    tshtMain: TTabSheet;
    tshtTaxation: TTabSheet;
    tshtLimits: TTabSheet;
    tshtPension_And_Benefits: TTabSheet;
    tshtScheduled_Defaults: TTabSheet;
    lablFrequency: TevLabel;
    lablEffective_Start_Date: TevLabel;
    lablAmount: TevLabel;
    lablRate: TevLabel;
    lablCalculation_Method: TevLabel;
    lablCustom_Expression: TevLabel;
    lablPay_Period_Minimum_Amount: TevLabel;
    lablPay_Period_Minimum_Percent: TevLabel;
    lablMinimum_Percent_E_D_Group: TevLabel;
    lablPer_Pay_Maximum_Amount: TevLabel;
    lablPer_Pay_Maximum_Percent: TevLabel;
    lablMaximum_E_D_Group: TevLabel;
    lablAnnual_Maximum_Amount: TevLabel;
    dedtCustom_E_D_Code_Number: TevDBEdit;
    dedtDecription: TevDBEdit;
    dedtOverride_Rate_Number: TevDBEdit;
    lablCustom_E_D_Code_Number: TevLabel;
    lablE_D_Code_Type: TevLabel;
    lablDescription: TevLabel;
    lablE_D_Group: TevLabel;
    lablOverride_Rate: TevLabel;
    lablOverride_EE_Rate_Number: TevLabel;
    lablW2_Box: TevLabel;
    dedtW2_Box: TevDBEdit;
    lablPriority_To_Exclude: TevLabel;
    dedtPriority_Code_Exlude_From_Check: TevDBEdit;
    lablRegular_OT_Rate: TevLabel;
    lablE_D_Group_OT_Calculation: TevLabel;
    lablTip_Min_Wage_Make_Up_Type: TevLabel;
    lablOffsetting_Deduction_Code: TevLabel;
    drgpEE_OASDI: TevDBRadioGroup;
    drgpEE_Medicare: TevDBRadioGroup;
    drgpEE_Federal: TevDBRadioGroup;
    drgpEE_EIC: TevDBRadioGroup;
    drgpER_OASDI: TevDBRadioGroup;
    drgpER_Medicare: TevDBRadioGroup;
    drgpER_FUI: TevDBRadioGroup;
    drgpExclude_Fifth_Week: TevDBRadioGroup;
    drgpExclude_Fourth_Week: TevDBRadioGroup;
    drgpExclude_Third_Week: TevDBRadioGroup;
    drgpExclude_Second_Week: TevDBRadioGroup;
    drgpExclude_First_Week: TevDBRadioGroup;
    drgpScheduled_Defaults: TevDBRadioGroup;
    drgpAuto: TevDBRadioGroup;
    wwcbFrequency: TevDBComboBox;
    wwlcMinimum_Percent_E_D_Group: TevDBLookupCombo;
    wwlcMaximum_E_D_Group: TevDBLookupCombo;
    wwcbCalculation_Method: TevDBComboBox;
    dmemCustom_Expression: TEvDBMemo;
    wwcdE_D_Code_Type: TevDBComboDlg;
    wwlcE_D_Group: TevDBLookupCombo;
    drgpShow_YDT_On_Checks: TevDBRadioGroup;
    drgpPrevent_Direct_Deposit: TevDBRadioGroup;
    drgpMake_Up_Deduction_Shortfall: TevDBRadioGroup;
    wwlcE_D_Group_OT_Calculation: TevDBLookupCombo;
    wwlcOffsetting_Deduction_Code: TevDBLookupCombo;
    wwlcTip_Min_Wage_Make_Up_Type: TevDBComboBox;
    OffsetDeductionCL_ED: TevClientDataSet;
    grbxOverride: TevGroupBox;
    drgpOverride_Federal_Tax_Type: TevDBRadioGroup;
    lablOverride_Federal_Tax_Value: TevLabel;
    lablDefault_Agency: TevLabel;
    wwlcDefault_Agency: TevDBLookupCombo;
    bev_cl_eds1b: TEvBevel;
    bev_cl_eds1a: TEvBevel;
    bev_cl_eds2b: TEvBevel;
    bev_eds2a: TEvBevel;
    bev_cl_eds3: TEvBevel;
    bev_cl_eds4: TEvBevel;
    bev_cl_eds5b: TEvBevel;
    gbPandB1: TevGroupBox;
    lablPension_Match_First_Percent: TevLabel;
    lablPension_Match_First_Ammount: TevLabel;
    lablPension_Match_Second_Percent: TevLabel;
    lablPension_Match_Second_Amount: TevLabel;
    lablPension_Flat_Match_Amount: TevLabel;
    lablPension_Link: TevLabel;
    wwlcPension_Link: TevDBLookupCombo;
    wwdeRegular_OT_Rate: TevDBEdit;
    wwdeOverride_Rate: TevDBEdit;
    wwdeOverride_Federal_Tax_Value: TevDBEdit;
    wwdePay_Period_Minimum_Amount: TevDBEdit;
    wwdePay_Period_Minimum_Percent_Of: TevDBEdit;
    wwdePay_Period_Maximum_Amount: TevDBEdit;
    wwdePay_Period_Maximum_Percent_Of: TevDBEdit;
    wwdeAnnual_Maximum_Amount: TevDBEdit;
    wwdeMatch_First_Percent: TevDBEdit;
    wwdeMatch_First_Amount: TevDBEdit;
    wwdeMatch_Second_Percent: TevDBEdit;
    wwdeMatch_Second_Amount: TevDBEdit;
    wwdePension_Flat_Match_Amount: TevDBEdit;
    wwdpEffective_Start_Date: TevDBDateTimePicker;
    wwdeSD_Amount: TevDBEdit;
    wwdeSD_Rate: TevDBEdit;
    Label1: TevLabel;
    wwlcOverrideRateType: TevDBComboBox;
    Label2: TevLabel;
    DbCboPrintOnWorksheet: TevDBComboBox;
    Label3: TevLabel;
    Label4: TevLabel;
    DBText3: TevDBText;
    DBText4: TevDBText;
    Label6: TevLabel;
    lcSyState: TevDBLookupCombo;
    lblSyState: TevLabel;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    evPopupMenu1: TevPopupMenu;
    CopytoCompanyED1: TMenuItem;
    tbshStateTaxation: TTabSheet;
    tbshLocalTaxation: TTabSheet;
    evDBGrid1: TevDBGrid;
    lablState: TevLabel;
    wwlcState: TevDBLookupCombo;
    evGroupBox1: TevGroupBox;
    lablValue: TevLabel;
    dedtValue: TevDBEdit;
    drgpType: TevDBRadioGroup;
    drgpEmployee_Exempt_Exclude_State: TevDBRadioGroup;
    drgpEmployee_Exempt_Exclude_SDI: TevDBRadioGroup;
    drgpEmployee_Exempt_Exclude_SUI: TevDBRadioGroup;
    drgpEmployer_Exempt_Exclude_SDI: TevDBRadioGroup;
    drgpEmployer_Exempt_Exclude_SUI: TevDBRadioGroup;
    lablLocal: TevLabel;
    wwlcSY_LOCALS_NBR: TevDBLookupCombo;
    drgpExempt_Exlude: TevDBRadioGroup;
    evDBGrid2: TevDBGrid;
    wwdsStateTax: TevDataSource;
    wwdsLocalTax: TevDataSource;
    StateTaxMiniNavigationFrame: TMiniNavigationFrame;
    LocalTaxMiniNavigationFrame: TMiniNavigationFrame;
    lablWarning1: TevLabel;
    lablWarning2: TevLabel;
    evDBRadioGroup1: TevDBRadioGroup;
    Copyto1: TMenuItem;
    evDBRadioGroup2: TevDBRadioGroup;
    evGroupBox2: TevGroupBox;
    lablE_D_Group_Piecework_Calculation: TevLabel;
    wwlcE_D_Group_Piecework_Calculation: TevDBLookupCombo;
    lablPiecework_Minimum_Wage: TevLabel;
    wwdePiecework_Minimum_Wage: TevDBEdit;
    lablPiecework_Min_Wage_Make_Up_Method: TevLabel;
    wwlcPiecework_Min_Wage_Make_Up_Method: TevDBComboBox;
    cbRemainingBalance: TevDBCheckBox;
    evDBRadioGroup3: TevDBRadioGroup;
    drgpRound: TevDBRadioGroup;
    lblUpdate_Hours: TevLabel;
    evcbUpdate_Hours: TevDBComboBox;
    evDBEdit6: TevDBEdit;
    lbMaxAvgHourlyWageRate: TevLabel;
    lbMonthNumber: TevLabel;
    wwdpMonth: TevDBComboBox;
    wwdpWhich_payrolls: TevDBComboBox;
    evLabel9: TevLabel;
    wwdpAlways_Pay_Deduct: TevDBComboBox;
    evLabel5: TevLabel;
    evDBRadioGroup4: TevDBRadioGroup;
    evDBLookupCombo3: TevDBLookupCombo;
    evLabel3: TevLabel;
    evDBLookupCombo2: TevDBLookupCombo;
    evLabel2: TevLabel;
    lablPriority: TevLabel;
    dbsePriority: TevDBSpinEdit;
    EvBevel1: TEvBevel;
    Panel4: TPanel;
    labl_Threshold_E_D_Group: TevLabel;
    wwlcThreshold_E_D_Group: TevDBLookupCombo;
    labl_Threshold_Amount: TevLabel;
    dedtThreshold_Amount: TevDBEdit;
    cbUsePensionLimit: TevDBCheckBox;
    evDBRadioGroup5: TevDBRadioGroup;
    evBitBtn1: TevBitBtn;
    rgCobraEligible: TevDBRadioGroup;
    drgpDeduct_whole_check: TevDBRadioGroup;
    lablThird_Party_Sick_Administrator: TevLabel;
    wwlcThird_Party_Sick_Administrator: TevDBLookupCombo;
    wwlcUnion_Link: TevDBLookupCombo;
    lablUnion_Link: TevLabel;
    evDBRadioGroup6: TevDBRadioGroup;
    wwDBGrid1: TevDBGrid;
    CopyHelper: TClientCopyHelper;
    edCodeType: TevDBEdit;
    evDBRadioGroup7: TevDBRadioGroup;
    edtMinHours: TevDBEdit;
    lblMinHours: TevLabel;
    lblMaxHours: TevLabel;
    edtMaxHours: TevDBEdit;
    lblFundingCode: TevLabel;
    lblExcessCode: TevLabel;
    cbFundingCode: TevDBLookupCombo;
    cbExcessCode: TevDBLookupCombo;    procedure wwcdE_D_Code_TypeChange(Sender: TObject);
    procedure wwcdE_D_Code_TypeCustomDlg(Sender: TObject);
    procedure CopytoCompanyED1Click(Sender: TObject);
    procedure wwlcStateChange(Sender: TObject);
    procedure wwlcSY_LOCALS_NBRChange(Sender: TObject);
    procedure Copyto1Click(Sender: TObject);
    procedure wwlcTip_Min_Wage_Make_Up_TypeChange(Sender: TObject);
    procedure CopyHelperCopyto1Click(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure cbUsePensionLimitClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure evDBRadioGroup7Change(Sender: TObject);
  private
    FWasInsert: boolean;
    FValueToUpdateTo: Variant;
    FEEEDFieldsToUpdate, FCLEDFieldsToUpdate: string;
    procedure HandleNewED;
    function HandleCopy(Sender: TObject; client: integer; selectedDetails, details: TEvClientDataSet; params: TStringList): integer;
    function HandleCopyStates(Sender: TObject; ForeignKeyValue: string; SelectedDetails, Details: TEvClientDataSet): integer;
    function HandleCopyLocals(Sender: TObject; ForeignKeyValue: string; SelectedDetails, Details: TEvClientDataSet): integer;
    function HandleDelete(Sender: TObject; client: integer; selectedDetails, details: TEvClientDataSet; params: TStringList): integer;

    function FromState(const s: string): string;
    function ToState(const s: string): string;
    procedure HandleUpdateED;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure AfterDataSetsReopen; override;
    procedure DoOnBroadcastTableChange(const ATable: String; const ANbr: Integer; const AChangeType: TevTableChangeType); override;    
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick( Kind: Integer ); override;
    procedure Activate; override;
    procedure Deactivate; override;

  end;

implementation

uses
  SPD_ChoiceGroupQuery, SPD_ChoiceCompanyQuery, SFrameEntry;

{$R *.DFM}

procedure DBRadioGroup_SetButtons(const Comp: TDBRadioGroup; const SetString: string; DefaultValue: string = '-'; const SetToDefault: Boolean = False);
var
  i: Integer;
begin
  Assert(Length(DefaultValue) <= 1, 'SetRadioGroupButtons:DefaultValue supposed to be a string[1]');

  if Length(SetString) = 1 then
    DefaultValue := SetString;

  if SetToDefault then
    Comp.ItemIndex := -1;

  for i := 0 to Comp.ControlCount - 1 do
  begin
    if Comp.Controls[i] is TRadioButton then
    begin
      Assert(Length(Comp.Values[i]) <= 1, 'DBRadioButtonValues are supposed to be a single character');
      Comp.Controls[i].Enabled := Pos(Comp.Values[i], SetString) > 0;
      if (Comp.Values[i] = DefaultValue) and Comp.Controls[i].Enabled then
      begin
        TRadioButton(Comp.Controls[i]).Font.Style := TRadioButton(Comp.Controls[i]).Font.Style + [fsBold];
        if SetToDefault then TRadioButton(Comp.Controls[i]).Checked := True;
      end
      else
        TRadioButton(Comp.Controls[i]).Font.Style := TRadioButton(Comp.Controls[i]).Font.Style - [fsBold];
    end;
  end;
end;

function DBRadioGroup_ED_GetAvailFromDefault(const EdType: string; const IsInSystem: Boolean; const DefaultValue: string; const AState: Integer = 0): string;
begin
  Assert(Length(DefaultValue) <= 1, 'SetRadioGroupButtons:DefaultValue supposed to be a string[1]');
  if (EdType = ED_EWOD_SPC_TAXED_EARNINGS) or
     (EdType = ED_ST_SP_TAXED_EARNING) or (EdType = ED_ST_SP_TAXED_DEDUCTION) or
     (EdType = ED_ST_SP_TAXED_1099_EARNINGS) or
     (EdType = ED_ST_1099_R_SP_TAXED) or
     ((AState = 38{'OH'}) and (EdType = ED_3RD_SHORT))then

    Result := GROUP_BOX_EXCLUDE + GROUP_BOX_INCLUDE + GROUP_BOX_EXEMPT

  else
  begin
    if (EdType = ED_ST_EXEMPT_EARNINGS) or (EdType = ED_ST_TIAA_CREFF) then
      Result := GROUP_BOX_EXEMPT

    else if EdType = '' then
      Result := ''

    else if EdType[1] = 'E' then
    begin
      if IsInSystem then
      begin
        if (DefaultValue = GROUP_BOX_EXEMPT) or (DefaultValue = GROUP_BOX_YES) then
          Result := GROUP_BOX_EXEMPT
        else
          Result := GROUP_BOX_EXCLUDE + GROUP_BOX_INCLUDE;
      end
      else
        Result := GROUP_BOX_EXCLUDE + GROUP_BOX_INCLUDE;
    end

    else if (EdType[1] = 'D') or (EdType[1] = 'M') then
    begin
      if IsInSystem then
        Result := DefaultValue
      else
        Result := GROUP_BOX_EXEMPT;
    end;
  end;
end;

function DBRadioGroup_ED_GetRightDefault(const EdType: string; const IsInSystem: Boolean; const DefaultValue: string): string;
begin
  if (EdType = ED_EWOD_SPC_TAXED_EARNINGS) or (EdType = ED_ST_SP_TAXED_EARNING) or
     (EdType = ED_ST_SP_TAXED_DEDUCTION) or (EdType = '') or (EdType = ED_ST_SP_TAXED_1099_EARNINGS) then

    Result := '-'

  else if IsInSystem then
  begin
    if DefaultValue = 'Y' then
      Result := GROUP_BOX_EXEMPT
    else if DefaultValue = 'N' then
      Result := GROUP_BOX_INCLUDE
    else
      Result := DefaultValue;
  end

  else if EdType[1] = 'E' then
    Result := GROUP_BOX_INCLUDE

  else
    Result := GROUP_BOX_EXEMPT;
end;


procedure TEDIT_CL_E_DS.wwcdE_D_Code_TypeChange(Sender: TObject);
var
  Found: Boolean;
  Code: string;
  s: string;
begin
  inherited;
  if csLoading in ComponentState then
    Exit;

  with DM_SYSTEM_FEDERAL do
  begin
    Code := wwcdE_D_Code_Type.Text;

    if (Code <> '') and
       (DM_CLIENT.CL_E_DS.State <> dsBrowse) then
      DM_CLIENT.CL_E_DS['DESCRIPTION'] := ReturnDescription(ED_ComboBoxChoices, Code);

    if not SY_FED_EXEMPTIONS.Active then
      Exit;
    Found := SY_FED_EXEMPTIONS.Locate('E_D_CODE_TYPE', Code, []);
    s := DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found,
      SY_FED_EXEMPTIONS['EXEMPT_FEDERAL']));
    drgpOverride_Federal_Tax_Type.Enabled := not (s = GROUP_BOX_EXEMPT);
    wwdeOverride_Federal_Tax_Value.Enabled := drgpOverride_Federal_Tax_Type.Enabled;
    if wwdeOverride_Federal_Tax_Value.Enabled then
      wwdeOverride_Federal_Tax_Value.Color := clWindow
    else
      wwdeOverride_Federal_Tax_Value.Color := clBtnFace;
    DBRadioGroup_SetButtons(drgpEE_Federal, s, DBRadioGroup_ED_GetRightDefault(Code, Found, SY_FED_EXEMPTIONS['EXEMPT_FEDERAL']),
      IsModified);
    DBRadioGroup_SetButtons(drgpEE_OASDI, DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found,
      SY_FED_EXEMPTIONS['EXEMPT_EMPLOYEE_OASDI'])), DBRadioGroup_ED_GetRightDefault(Code, Found,
      SY_FED_EXEMPTIONS['EXEMPT_EMPLOYEE_OASDI']), IsModified);
    DBRadioGroup_SetButtons(drgpEE_Medicare, DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code,
      Found, SY_FED_EXEMPTIONS['EXEMPT_EMPLOYEE_MEDICARE'])), DBRadioGroup_ED_GetRightDefault(Code, Found,
      SY_FED_EXEMPTIONS['EXEMPT_EMPLOYEE_MEDICARE']), IsModified);
    DBRadioGroup_SetButtons(drgpEE_EIC, DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found,
      SY_FED_EXEMPTIONS['EXEMPT_EMPLOYEE_EIC'])), DBRadioGroup_ED_GetRightDefault(Code, Found, SY_FED_EXEMPTIONS['EXEMPT_EMPLOYEE_EIC']),
      IsModified);
    DBRadioGroup_SetButtons(drgpER_OASDI, DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found,
      SY_FED_EXEMPTIONS['EXEMPT_EMPLOYER_OASDI'])), DBRadioGroup_ED_GetRightDefault(Code, Found,
      SY_FED_EXEMPTIONS['EXEMPT_EMPLOYER_OASDI']), IsModified);
    DBRadioGroup_SetButtons(drgpER_Medicare, DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code,
      Found, SY_FED_EXEMPTIONS['EXEMPT_EMPLOYER_MEDICARE'])), DBRadioGroup_ED_GetRightDefault(Code, Found,
      SY_FED_EXEMPTIONS['EXEMPT_EMPLOYER_MEDICARE']), IsModified);
    DBRadioGroup_SetButtons(drgpER_FUI, DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found,
      SY_FED_EXEMPTIONS['EXEMPT_FUI'])), DBRadioGroup_ED_GetRightDefault(Code, Found, SY_FED_EXEMPTIONS['EXEMPT_FUI']),
      IsModified);

    wwlcStateChange(nil);
    wwlcSY_LOCALS_NBRChange(nil);

    with (Sender as TevDBComboDlg) do
      if Assigned(DataSource) and Assigned(DataSource.DataSet) then
        with (DataSource.DataSet as TevClientDataSet) do
        begin
          if State in [dsInsert] then
            if Code = ED_DIRECT_DEPOSIT then
              FindField('SKIP_HOURS').AsString := GROUP_BOX_NO;
        end;

  end;

  if Context.License.HR then
  begin
    if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) and
        (length(Code) > 0)   then
    begin
      if InSet( Code, ['DL', 'DM', 'MC'] ) then
        wwdsDetail.DataSet['COBRA_ELIGIBLE'] := GROUP_BOX_YES
       else
        wwdsDetail.DataSet['COBRA_ELIGIBLE'] := GROUP_BOX_NO;
    end;
  end;
end;

procedure TEDIT_CL_E_DS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_PENSION, aDS);
  AddDS(DM_CLIENT.CL_UNION, aDS);
  AddDS(DM_CLIENT.CL_3_PARTY_SICK_PAY_ADMIN, aDS);
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  AddDS(DM_CLIENT.CL_E_D_GROUPS, aDS);
  AddDS(DM_CLIENT.CL_E_D_STATE_EXMPT_EXCLD, aDS);
  AddDS(DM_CLIENT.CL_E_D_LOCAL_EXMPT_EXCLD, aDS);
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_CLIENT.CL_E_D_GROUP_CODES, aDS);
  AddDS(DM_COMPANY.CO, aDS);
  AddDS(DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS, aDS);
  inherited;
end;

function TEDIT_CL_E_DS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_E_DS;
end;

function TEDIT_CL_E_DS.GetInsertControl: TWinControl;
begin
  Result := dedtCustom_E_D_Code_Number;
end;

procedure TEDIT_CL_E_DS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  AddDSWithCheck(DM_SYSTEM_FEDERAL.SY_FED_EXEMPTIONS, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATES, SupportDataSets, '');
  AddDSWithCheck(DM_SYSTEM_STATE.SY_STATE_EXEMPTIONS, SupportDataSets, '');
  AddDS(DM_CLIENT.CL_E_D_STATE_EXMPT_EXCLD, EditDataSets);
  AddDS(DM_CLIENT.CL_E_D_LOCAL_EXMPT_EXCLD, EditDataSets);
  AddDS(DM_COMPANY.CO_E_D_CODES, EditDataSets );
  AddDS(DM_CLIENT.CL_E_D_GROUP_CODES, EditDataSets );
  AddDS(DM_COMPANY.CO, EditDataSets );
  AddDSWithCheck(DM_SYSTEM_LOCAL.SY_LOCAL_EXEMPTIONS, EditDataSets, '');
  inherited;
end;

procedure TEDIT_CL_E_DS.AfterDataSetsReopen;
var
  s: string;
begin
  inherited;
  s := DM_CLIENT.CL_E_DS.IndexName;
  try
    DM_CLIENT.CL_E_DS.IndexName := '';
    OffsetDeductionCL_ED.CloneCursor(DM_CLIENT.CL_E_DS, True);
  finally
    if s <> '' then
      DM_CLIENT.CL_E_DS.IndexName := s;
  end;

end;

procedure TEDIT_CL_E_DS.DoOnBroadcastTableChange(const ATable: String;
  const ANbr: Integer; const AChangeType: TevTableChangeType);
var
 s: string;
begin
  inherited;
  if AnsiSameText(ATable, 'CL_E_DS') then
  begin
    DM_CLIENT.CL_E_DS.RefreshRecord(ANbr);
    s:= DM_CLIENT.CL_E_D_STATE_EXMPT_EXCLD.RetrieveCondition;
    DM_CLIENT.CL_E_D_STATE_EXMPT_EXCLD.DataRequired(s);
    s:= DM_CLIENT.CL_E_D_LOCAL_EXMPT_EXCLD.RetrieveCondition;
    DM_CLIENT.CL_E_D_LOCAL_EXMPT_EXCLD.DataRequired(s);
  end;
end;

procedure TEDIT_CL_E_DS.wwcdE_D_Code_TypeCustomDlg(Sender: TObject);
var
  EdCount: Integer;
begin
  EdCount := GetEDCheckLineCount(DM_CLIENT.CL_E_DS['CL_E_DS_NBR']);
  if EdCount > 0 then
  begin
    if EvMessage('This E/D Code has YTDs Attached! Changing this may cause wages and tips to be out of balance.  Are you sure you want to change it?',
           mtConfirmation, [mbYes, mbNo]) <> mrYes then
    begin
      DM_CLIENT.CL_E_DS.Cancel;
      AbortEx;
    end;
  end;
  TevDBComboDlg(Sender).ShowCategorizedComboDialog;
end;

procedure TEDIT_CL_E_DS.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  EdCount: Integer;
begin
  inherited;
  case Kind of
    NavDelete:
    begin
      EdCount := GetEDCheckLineCount(DM_CLIENT.CL_E_DS['CL_E_DS_NBR']);
      if EdCount > 0 then
      begin
        DM_CLIENT.CL_E_DS.Cancel;
        raise EUpdateError.CreateHelp('Cannot delete an E/D Code that is attached to check lines.', IDH_ConsistencyViolation);
      end;
    end;
    NavOk,NavCommit:
    begin
      FWasInsert := FWasInsert or (DM_CLIENT.CL_E_DS.State = dsInsert);
    end;
  end;
end;

procedure TEDIT_CL_E_DS.CopytoCompanyED1Click(Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  I: Integer;
begin
  inherited;

   CopyForm := TEDIT_COPY.Create(Self);
   try
     CopyForm.CopyTo := 'CO_ED_CODES';
     for I := 0  to wwDBGrid1.SelectedList.Count - 1 do
     begin
       wwDBGrid1.DataSource.DataSet.GotoBookmark(wwDBGrid1.SelectedList.items[I]);
       CopyForm.EDList.Add(DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').AsString);
     end;
     DM_TEMPORARY.TMP_CO.SaveState;
     try
       DM_TEMPORARY.TMP_CO.DisableControls;
       try
         DM_TEMPORARY.TMP_CO.DataRequired('CL_NBR='+DM_CLIENT.CL.FieldByName('CL_NBR').AsString);
         CopyForm.wwcsCopy.Data := DM_TEMPORARY.TMP_CO.Data;
         CopyForm.wwgdCopy.Selected.Text :=
             'CUSTOM_COMPANY_NUMBER' + #9 + '10' + #9 + 'Number' + #9 + 'F' + #13 + #10 +
             'NAME' + #9 + '40' + #9 + 'Name' + #9 + 'F' + #13 + #10;
         CopyForm.wwgdCopy.ApplySelected;
         CopyForm.ShowModal;
       finally
         DM_TEMPORARY.TMP_CO.EnableControls;
       end;
     finally
       DM_TEMPORARY.TMP_CO.LoadState;
     end;
   finally
     CopyForm.Free;
   end;
end;

function TEDIT_CL_E_DS.FromState(const s: string): string;
var
  i: Integer;
begin
  Result := s;
  for i := 1 to Length(Result) do
    if Result[i] = GROUP_BOX_EXE then
      Result[i] := GROUP_BOX_EXEMPT
    else
  if Result[i] = GROUP_BOX_EXC then
    Result[i] := GROUP_BOX_EXCLUDE;
end;


function TEDIT_CL_E_DS.ToState(const s: string): string;
var
  i: Integer;
begin
  Result := s;
  for i := 1 to Length(Result) do
    if Result[i] = GROUP_BOX_EXEMPT then
      Result[i] := GROUP_BOX_EXE
    else
      if Result[i] = GROUP_BOX_EXCLUDE then
        Result[i] := GROUP_BOX_EXC;
end;

procedure TEDIT_CL_E_DS.wwlcStateChange(
  Sender: TObject);
  // state exemption screen uses it's own codes for states. I decided to write two-way filter.
  // That allows me to use my other standart procedures.
var
  Found: Boolean;
  Code: string;
  s, SetStringSDI, SetStringSUI: string;
begin
  inherited;
  with DM_SYSTEM_STATE, DM_CLIENT do
    if SY_STATE_EXEMPTIONS.Active and (wwlcState.LookupValue <> '') then
    begin
      Found := SY_STATE_EXEMPTIONS.Locate('SY_STATES_NBR;E_D_CODE_TYPE', VarArrayOf([wwlcState.LookupValue, CL_E_DS['E_D_CODE_TYPE']]), []);
      Code := ConvertNull(CL_E_DS['E_D_CODE_TYPE'], '');
      s := DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found, FromState(SY_STATE_EXEMPTIONS['EXEMPT_STATE'])), SY_STATE_EXEMPTIONS['SY_STATES_NBR']);
      drgpType.Enabled := not (s = GROUP_BOX_EXEMPT);
      dedtValue.Enabled := drgpType.Enabled;
      if dedtValue.Enabled then
        dedtValue.Color := clWindow
      else
        dedtValue.Color := clBtnFace;
      DBRadioGroup_SetButtons(drgpEmployee_Exempt_Exclude_State, ToState(s), DBRadioGroup_ED_GetRightDefault(Code, Found, FromState(SY_STATE_EXEMPTIONS['EXEMPT_STATE'])), IsModified);

      SetStringSDI := ToState(DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found, SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYEE_SDI']), SY_STATE_EXEMPTIONS['SY_STATES_NBR']));
      DBRadioGroup_SetButtons(drgpEmployee_Exempt_Exclude_SDI, SetStringSDI, ToState(DBRadioGroup_ED_GetRightDefault(Code, Found, SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYEE_SDI'])), IsModified);

      SetStringSUI := ToState(DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found, SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYEE_SUI']), SY_STATE_EXEMPTIONS['SY_STATES_NBR']));
      DBRadioGroup_SetButtons(drgpEmployee_Exempt_Exclude_SUI, SetStringSUI , ToState(DBRadioGroup_ED_GetRightDefault(Code, Found, SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYEE_SUI'])), IsModified);
      
      DBRadioGroup_SetButtons(drgpEmployer_Exempt_Exclude_SDI,   ToState(DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found, SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYER_SDI']), SY_STATE_EXEMPTIONS['SY_STATES_NBR'])), ToState(DBRadioGroup_ED_GetRightDefault(Code, Found, SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYER_SDI'])), IsModified);
      DBRadioGroup_SetButtons(drgpEmployer_Exempt_Exclude_SUI,   ToState(DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found, SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYER_SUI']), SY_STATE_EXEMPTIONS['SY_STATES_NBR'])), ToState(DBRadioGroup_ED_GetRightDefault(Code, Found, SY_STATE_EXEMPTIONS['EXEMPT_EMPLOYER_SUI'])), IsModified);
    end;
end;

procedure TEDIT_CL_E_DS.wwlcSY_LOCALS_NBRChange(Sender: TObject);
var
  Found: Boolean;
  Code: string;
  s: string;
begin
  inherited;
  with DM_SYSTEM_LOCAL, DM_CLIENT do
    if (SY_LOCAL_EXEMPTIONS.Active) then
    begin
      Found := SY_LOCAL_EXEMPTIONS.Locate('SY_LOCALS_NBR;E_D_CODE_TYPE', VarArrayOf([SY_LOCALS['SY_LOCALS_NBR'], CL_E_DS['E_D_CODE_TYPE']]), []);
      Code := ConvertNull(CL_E_DS['E_D_CODE_TYPE'], '');
      s := DBRadioGroup_ED_GetAvailFromDefault(Code, Found, DBRadioGroup_ED_GetRightDefault(Code, Found, SY_LOCAL_EXEMPTIONS['EXEMPT']), SY_LOCALS['SY_STATES_NBR']);
      DBRadioGroup_SetButtons(drgpExempt_Exlude, s, DBRadioGroup_ED_GetRightDefault(Code, Found, SY_LOCAL_EXEMPTIONS['EXEMPT']),
        IsModified);
    end;
end;

procedure TEDIT_CL_E_DS.Activate;
var
  dummy: boolean;
begin
  inherited;
  StateTaxMiniNavigationFrame.DataSource := wwdsStateTax;
  StateTaxMiniNavigationFrame.InsertFocusControl := wwlcState;
  LocalTaxMiniNavigationFrame.DataSource := wwdsLocalTax;
  LocalTaxMiniNavigationFrame.InsertFocusControl := wwlcSY_LOCALS_NBR;

  CopyHelper.Grid := wwDBGrid1;
  CopyHelper.UserFriendlyName := 'E/D';
  CopyHelper.OnCopyDetail := HandleCopy;
  CopyHelper.OnDeleteDetail := HandleDelete;
  CopyHelper.AddRelation(TCL_E_D_STATE_EXMPT_EXCLD, HandleCopyStates);
  CopyHelper.AddRelation(TCL_E_D_LOCAL_EXMPT_EXCLD, HandleCopyLocals);
  GetDataSetsToReopen( CopyHelper.FDSToSave, dummy );
  Copyto1.OnClick := Copyto1Click;
  wwcdE_D_Code_TypeChange(wwcdE_D_Code_Type);

  DM_CLIENT.CL_E_DS.SD_THRESHOLD_E_D_GROUPS_NBR.DisplayLabel := labl_Threshold_E_D_Group.Caption;

   rgCobraEligible.Enabled := Context.License.HR;

end;

procedure TEDIT_CL_E_DS.Deactivate;
begin
  inherited;
  DM_CLIENT.CL_E_DS.UserFilter :='';
end;

procedure TEDIT_CL_E_DS.HandleNewED;
var
  Pos: TBookmark;
  i: integer;
begin
  DM_CLIENT.CL_E_D_GROUPS.DataRequired('');
  if DM_CLIENT.CL_E_D_GROUPS.RecordCount > 0 then
  begin
    with TChoiceGroupQuery.Create( Self ) do
    try
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        DM_CLIENT.CL_E_D_GROUP_CODES.DataRequired(''); //how can I avoid this ?
        DM_CLIENT.CL_E_D_GROUPS.DisableControls;
        try
          Pos := DM_CLIENT.CL_E_D_GROUPS.GetBookmark;
          try
            for i := 0 to dgChoiceList.SelectedList.Count - 1 do
            begin
              DM_CLIENT.CL_E_D_GROUPS.GotoBookmark( dgChoiceList.SelectedList[i] );
              DM_CLIENT.CL_E_D_GROUP_CODES.Insert;
              DM_CLIENT.CL_E_D_GROUP_CODES['CL_E_DS_NBR'] := DM_CLIENT.CL_E_DS['CL_E_DS_NBR'];
              DM_CLIENT.CL_E_D_GROUP_CODES['CL_E_D_GROUPS_NBR'] := DM_CLIENT.CL_E_D_GROUPS['CL_E_D_GROUPS_NBR'];
              DM_CLIENT.CL_E_D_GROUP_CODES.Post;
            end;
            DM_CLIENT.CL_E_D_GROUPS.GotoBookmark(pos);
          finally
            DM_CLIENT.CL_E_D_GROUPS.FreeBookmark(pos);
          end;
        finally
          DM_CLIENT.CL_E_D_GROUPS.EnableControls;
        end;
      end;
    finally
      Free;
    end;
  end;

  DM_TEMPORARY.TMP_CO.DataRequired('CL_NBR>0');
  DM_TEMPORARY.TMP_CO.Filter := 'CL_NBR='+ IntToStr(ctx_DataAccess.ClientId);
  DM_TEMPORARY.TMP_CO.Filtered := True;
  try
    if DM_TEMPORARY.TMP_CO.RecordCount > 0 then
    begin
      with TChoiceCompanyQuery.Create( Self ) do
      try
        dgChoiceList.SelectRecord;
        if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
        begin                                                     
          DM_COMPANY.CO_E_D_CODES.DataRequired('');
          DM_TEMPORARY.TMP_CO.DisableControls;
          try
            Pos := DM_TEMPORARY.TMP_CO.GetBookmark;
            try
              for i := 0 to dgChoiceList.SelectedList.Count - 1 do
              begin
                DM_COMPANY.CO.DataRequired('CO_NBR='+(DM_TEMPORARY.TMP_CO.CO_NBR.AsString));
                DM_TEMPORARY.TMP_CO.GotoBookmark( dgChoiceList.SelectedList[i] );
                DM_COMPANY.CO_E_D_CODES.Insert;
                DM_COMPANY.CO_E_D_CODES['CL_E_DS_NBR'] := DM_CLIENT.CL_E_DS['CL_E_DS_NBR'];
                DM_COMPANY.CO_E_D_CODES['DISTRIBUTE'] := GROUP_BOX_NO;
                DM_COMPANY.CO_E_D_CODES['CO_NBR'] := DM_TEMPORARY.TMP_CO['CO_NBR'];

                if Context.License.HR then
                   SetUpCO_E_D_CODESDefault(DM_CLIENT.CL_E_DS.FieldByName('E_D_CODE_TYPE').AsString,
                                             LowerCase(DM_CLIENT.CL_E_DS.FieldByName('DESCRIPTION').AsString));
                DM_COMPANY.CO_E_D_CODES.Post;
              end;
              DM_TEMPORARY.TMP_CO.GotoBookmark(pos);
            finally
              DM_TEMPORARY.TMP_CO.FreeBookmark(pos);
            end;
          finally
            DM_TEMPORARY.TMP_CO.EnableControls;
          end;
        end;
      finally
        Free;
      end;
    end;
  finally
    DM_TEMPORARY.TMP_CO.Filtered := False;
    DM_TEMPORARY.TMP_CO.Filter := '';
  end;
  DM_CLIENT.CL_E_DS.Insert;
  DM_CLIENT.CL_E_DS.Cancel;
end;

procedure TEDIT_CL_E_DS.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavOk,NavCommit:
      if FWasInsert then
      begin
        FWasInsert := false;
        HandleNewED;
      end;
    NavAbort:
      if FWasInsert then
        FWasInsert := false;
  end;

  if Kind in [NavInsert, NavCancel, NavCommit, NavAbort] then
     ApplySecurity;
end;

function TEDIT_CL_E_DS.HandleCopy(Sender: TObject; client: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList): integer;
var
  k: integer;
  fn: string;
begin
// Details = the target dataset that is copied to
// SelectedDetails = the records that are to be copied
  if details.Locate('CUSTOM_E_D_CODE_NUMBER', VarArrayOf([selecteddetails.FieldByNAme('CUSTOM_E_D_CODE_NUMBER').AsString]), []) then
    details.Edit
  else
    details.Insert;
  for k := 0 to SelectedDetails.Fields.Count - 1 do
  begin
    fn := Selecteddetails.Fields[k].FieldName;
    if (Sender as TBasicCopier).IsFieldToCopy( fn, details ) and
      (copy(fn,Length(fn)-3,4) <> '_NBR' ) then
      details[fn] := selectedDetails[fn];
  end;
  details.Post;
  Result := Details.FieldByName(Details.TableName + '_NBR').AsInteger;
end;

function TEDIT_CL_E_DS.HandleCopyLocals(Sender: TObject;
  ForeignKeyValue: string; SelectedDetails, Details: TEvClientDataSet): integer;
// Details = the target dataset that is copied to
// SelectedDetails = the records that are to be copied
var
  I: integer;
  FieldName: string;
begin
  if Details.Locate(
    'CL_E_DS_NBR;'+
    'SY_LOCALS_NBR;'+
    'EXEMPT_EXCLUDE',
     VarArrayOf([StrToIntDef(ForeignKeyValue, -1),
       selecteddetails.FieldByName('SY_LOCALS_NBR').AsInteger,
       selecteddetails.FieldByName('EXEMPT_EXCLUDE').AsString
       ]), []) then
    Details.Edit
  else
    Details.Insert;
  try
    for I := 0 to SelectedDetails.Fields.Count - 1 do
    begin
      FieldName := SelectedDetails.Fields[I].FieldName;
      if (Sender as TBasicCopier).IsFieldToCopy(FieldName, Details) then
      begin
        if FieldName = 'CL_E_DS_NBR' then
          Details[FieldName] := StrToInt(ForeignKeyValue)
        else
          Details[FieldName] := SelectedDetails[FieldName];
      end;
    end;
    Details.Post;
    Result := Details.FieldByName(Details.TableName + '_NBR').AsInteger;
  except
    Details.Rollback;
    raise;
  end;
end;

function TEDIT_CL_E_DS.HandleCopyStates(Sender: TObject;
  ForeignKeyValue: string; SelectedDetails, Details: TEvClientDataSet): integer;
// SelectedDetails = the records that are to be copied
// Details = the target dataset that is copied to
var
  I: integer;
  FieldName: string;
begin
  if Details.Locate(
    'CL_E_DS_NBR;'+
    'SY_STATES_NBR',
     VarArrayOf([StrToIntDef(ForeignKeyValue, -1),
       selecteddetails.FieldByName('SY_STATES_NBR').AsInteger,
       selecteddetails.FieldByName('EMPLOYEE_EXEMPT_EXCLUDE_STATE').AsString
       ]), []) then
    Details.Edit
  else
    Details.Insert;
  try
    for I := 0 to SelectedDetails.Fields.Count - 1 do
    begin
      FieldName := SelectedDetails.Fields[I].FieldName;
      if (Sender as TBasicCopier).IsFieldToCopy(FieldName, Details) then
      begin
        if FieldName = 'CL_E_DS_NBR' then
          Details[FieldName] := StrToInt(ForeignKeyValue)
        else
          Details[FieldName] := SelectedDetails[FieldName];
      end;
    end;
    Details.Post;
    Result := Details.FieldByName(Details.TableName + '_NBR').AsInteger;
  except
    Details.Rollback;
    raise;
  end;
end;

function TEDIT_CL_E_DS.HandleDelete(Sender: TObject; client: integer;
  selectedDetails, details: TEvClientDataSet; params: TStringList): integer;
begin
  if details.Locate('CUSTOM_E_D_CODE_NUMBER', VarArrayOf([selecteddetails.FieldByNAme('CUSTOM_E_D_CODE_NUMBER').AsString]), []) then
  begin
    Result := Details.FieldByName(Details.TableName + '_NBR').AsInteger;
    details.Delete;
  end
  else
    Result := -1;
end;

procedure TEDIT_CL_E_DS.Copyto1Click(Sender: TObject);
begin
  if EvMessage('The selected E/Ds will be copied. If any E/Ds already exist '+
    'on the destination client(s) they will be overriden. State and Local tax '+
    'overrides will also be copied, and overriden in case they already exist. '+
    'If you are copying a Specially Taxed E/D, please verify that taxation is '+
    'correct.'+#13+#10+#13+#10+
    'Do you want to continue with copying the selected E/Ds?',
    mtWarning, [mbYes, mbNo]) = mrYes then
  begin
    CopyHelper.CopyActionExecute(Sender);
  end;
end;

procedure TEDIT_CL_E_DS.wwlcTip_Min_Wage_Make_Up_TypeChange(
  Sender: TObject);
var
  d: TEvClientDataSet;
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) then
    if (wwlcTip_Min_Wage_Make_Up_Type.Value = TIP_MINWAGE_PAY_FORCE_TIP_MAKEUP) then
    begin
      d := TevClientDataSet.Create(nil);
      try
        d.CloneCursor(TEvClientDataset(wwdsDetail.DataSet), False);
        if not d.Locate('E_D_CODE_TYPE', ED_MU_TIPPED_CREDIT_MAKEUP, []) then
          EvMessage('Do not forget to set Tipped Credit Makeup ED Code');
      finally
        d.Free;
      end;
    end;
end;

procedure TEDIT_CL_E_DS.CopyHelperCopyto1Click(Sender: TObject);
begin
  inherited;
  CopyHelper.CopyActionExecute(Sender);

end;

procedure TEDIT_CL_E_DS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  if (Field = nil) or (Field.FieldName = 'E_D_CODE_TYPE') then
  begin
    if DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] = ED_DIRECT_DEPOSIT then
      if DM_CLIENT.CL_E_DS.State in [dsEdit, dsInsert] then //don't be too smart to start editing while user is just browsing
        DM_CLIENT.CL_E_DS['SD_AUTO'] := GROUP_BOX_NO;

    drgpAuto.Enabled := DM_CLIENT.CL_E_DS['E_D_CODE_TYPE'] <> ED_DIRECT_DEPOSIT;
  end;
  if (Field = nil) or (Field.FieldName = 'SD_USE_PENSION_LIMIT') then
  begin
     cbUsePensionLimitClick(Sender);
  end;
end;

procedure TEDIT_CL_E_DS.cbUsePensionLimitClick(Sender: TObject);
var Gray:boolean;
begin
  inherited;
  Gray := cbUsePensionLimit.Checked;
  SetFieldRequired(Gray,DM_CLIENT.CL_E_DS.SD_THRESHOLD_E_D_GROUPS_NBR,labl_Threshold_E_D_Group);
  GrayOutControl(dedtThreshold_Amount,Gray);
end;

procedure TEDIT_CL_E_DS.evBitBtn1Click(Sender: TObject);
var
  CopyForm: TEDIT_COPY;
  t1, t2: TStringList;
  i : Integer;
begin
  inherited;

  if wwdsDetail.DataSet.RecordCount = 0 then Exit;
  if wwDBGrid1.SelectedList.Count > 1 then
  begin
    EvMessage('You can update only one Client E/D at a time.', mtWarning, [mbOk]);
    Exit;
  end;
  t1 := TStringList.Create;
  t2 := TStringList.Create;
  with TEDFields.Create( Self ) do
  try
    with clbFields.Items do begin
      Add('Calculation Method');
      Add('Amount');
      Add('Percent');
      Add('Frequency');
      Add('Month #');
      Add('Which Payrolls');
      Add('E/D Group');
      Add('Agency');
      Add('Effective Start Date');
      Add('Priority');
      Add('Block Week 1');
      Add('Block Week 2');
      Add('Block Week 3');
      Add('Block Week 4');
      Add('Block Week 5');
      Add('Always Pay/Deduct');
      Add('Deductions to Zero');
      Add('Deduct Whole Check');
      Add('Maximum Average Amount E/D Group');
      Add('Maximum Average Hours E/D Group');
      Add('Maximum Average Hourly Wage Rate');
      Add('Threshold E/D Group');
      Add('Threshold Amount');
      Add('Use Pension Limit');
    end;
    with t1 do
    begin
      Add('CALCULATION_TYPE');
      Add('AMOUNT');
      Add('PERCENTAGE');
      Add('FREQUENCY');
      Add('PLAN_TYPE');
      Add('WHICH_CHECKS');
      Add('CL_E_D_GROUPS_NBR');
      Add('CL_AGENCY_NBR');
      Add('EFFECTIVE_START_DATE');
      Add('PRIORITY_NUMBER');
      Add('EXCLUDE_WEEK_1');
      Add('EXCLUDE_WEEK_2');
      Add('EXCLUDE_WEEK_3');
      Add('EXCLUDE_WEEK_4');
      Add('EXCLUDE_WEEK_5');
      Add('ALWAYS_PAY');
      Add('DEDUCTIONS_TO_ZERO');
      Add('DEDUCT_WHOLE_CHECK');
      Add('MAX_AVG_AMT_CL_E_D_GROUPS_NBR');
      Add('MAX_AVG_HRS_CL_E_D_GROUPS_NBR');
      Add('MAX_AVERAGE_HOURLY_WAGE_RATE');
      Add('THRESHOLD_E_D_GROUPS_NBR');
      Add('THRESHOLD_AMOUNT');
      Add('USE_PENSION_LIMIT');
    end;
    with t2 do
    begin
      Add('SD_CALCULATION_METHOD');
      Add('SD_AMOUNT');
      Add('SD_RATE');
      Add('SD_FREQUENCY');
      Add('SD_PLAN_TYPE');
      Add('SD_WHICH_CHECKS');
      Add('CL_E_D_GROUPS_NBR');
      Add('DEFAULT_CL_AGENCY_NBR');
      Add('SD_EFFECTIVE_START_DATE');
      Add('SD_PRIORITY_NUMBER');
      Add('SD_EXCLUDE_WEEK_1');
      Add('SD_EXCLUDE_WEEK_2');
      Add('SD_EXCLUDE_WEEK_3');
      Add('SD_EXCLUDE_WEEK_4');
      Add('SD_EXCLUDE_WEEK_5');
      Add('SD_ALWAYS_PAY');
      Add('SD_DEDUCTIONS_TO_ZERO');
      Add('SD_DEDUCT_WHOLE_CHECK');
      Add('SD_MAX_AVG_AMT_GRP_NBR');
      Add('SD_MAX_AVG_HRS_GRP_NBR');
      Add('SD_MAX_AVERAGE_HOURLY_WAGE_RATE');
      Add('SD_THRESHOLD_E_D_GROUPS_NBR');
      Add('SD_THRESHOLD_AMOUNT');
      Add('SD_USE_PENSION_LIMIT');
    end;

    if ShowModal = mrOk then
    begin
      FCLEDFieldsToUpdate := '';
      FEEEDFieldsToUpdate := '';
      for i := 0 to Pred(clbFields.Items.Count) do
        if clbFields.Checked[i] then
        begin
          FEEEDFieldsToUpdate := FEEEDFieldsToUpdate + t1[i] + ';';
          FCLEDFieldsToUpdate := FCLEDFieldsToUpdate + t2[i] + ';';
        end;
      Delete(FEEEDFieldsToUpdate, Length(FEEEDFieldsToUpdate), 1);
      Delete(FCLEDFieldsToUpdate, Length(FCLEDFieldsToUpdate), 1);

      FValueToUpdateTo := DM_CLIENT.CL_E_DS[FCLEDFieldsToUpdate];

      CopyForm := TEDIT_COPY.Create(Self);
      try
        CopyForm.CopyTo := 'EEEDCodes';
        CopyForm.CopyFrom := 'Client';
        CopyForm.EdNbr := DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
        CopyForm.UpdateEDs := True;
        CopyForm.CustomEDFunction := HandleUpdateED;
        DM_CLIENT.CL_E_DS.Cancel;
        DM_CLIENT.CL_E_DS.DisableControls;
        CopyForm.ShowModal;
        DM_CLIENT.CL_E_DS.EnableControls;
      finally
        CopyForm.Free;
      end;
    end;
  finally
    t1.Free;
    t2.Free;
    Free;
  end;
end;

procedure TEDIT_CL_E_DS.HandleUpdateED;
begin
  DM_EMPLOYEE.EE_SCHEDULED_E_DS[FEEEDFieldsToUpdate] := FValueToUpdateTo;
end;


procedure TEDIT_CL_E_DS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.State = dsInsert then
  begin
    wwcdE_D_Code_Type.Visible := True;
    edCodeType.Visible := False;
  end
  else
  begin
    wwcdE_D_Code_Type.Visible := False;
    edCodeType.Visible := True;
  end;
end;

procedure TEDIT_CL_E_DS.evDBRadioGroup7Change(Sender: TObject);
var
  EDType: String;
  RoundingWorksForEDType: Boolean;
begin
  if csLoading in ComponentState then
    Exit;

  if (wwdsDetail.Dataset.FieldByName('ROUND_OT_CALCULATION').AsString = 'Y') or (evDBRadioGroup7.ItemIndex = 1)
  or (wwdsDetail.Dataset.FieldByName('ROUND_OT_CALCULATION').AsString = '') then
    Exit;

  EDType := wwcdE_D_Code_Type.Text;

  RoundingWorksForEDType := ((EDType = ED_OEARN_OVERTIME) or (EDType = ED_OEARN_WAITSTAFF_OVERTIME) or (EDType = ED_OEARN_WEIGHTED_HALF_TIME_OT)
    or (EDType = ED_OEARN_WEIGHTED_3_HALF_TIME_OT) or (EDType = ED_OEARN_AVG_OVERTIME));

  if not RoundingWorksForEDType then
  begin
    evDBRadioGroup7.ItemIndex := 1;
    EvMessage('This Rounding OT Calculation feature is not applicable for the ED Code Type '''+EDType+'''.', mtError, [mbOk]);
  end;
end;

initialization
  RegisterClass(TEDIT_CL_E_DS);
end.


