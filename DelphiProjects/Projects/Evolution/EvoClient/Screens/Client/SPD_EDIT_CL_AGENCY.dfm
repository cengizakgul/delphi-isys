inherited EDIT_CL_AGENCY: TEDIT_CL_AGENCY
  inherited Panel1: TevPanel
    inherited pnlTopRight: TevPanel
      inherited Label5: TevLabel
        Left = 16
      end
    end
  end
  inherited PageControl1: TevPageControl
    HelpContext = 12026
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited Splitter1: TevSplitter
              Left = 320
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Width = 320
              inherited wwdbgridSelectClient: TevDBGrid
                Width = 270
                Height = 334
                IniAttributes.SectionName = 'TEDIT_CL_AGENCY\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Left = 323
              Width = 48
              Title = 'Agency'
              object wwDBGrid1: TevDBGrid
                Left = 18
                Top = 48
                Width = 392
                Height = 334
                DisableThemesInTitle = False
                Selected.Strings = (
                  'Agency_Name'#9'40'#9'Agency Name'#9'No')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_AGENCY\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              inline CopyHelper: TClientCopyHelper
                Left = 96
                Top = 128
                Width = 115
                Height = 54
                TabOrder = 1
                Visible = False
              end
            end
          end
        end
      end
    end
    object tshtClient_Agency_Details: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object bev_CL_agency1: TEvBevel
        Left = 0
        Top = 0
        Width = 427
        Height = 183
        Align = alClient
      end
      object lablReporting_Method: TevLabel
        Left = 16
        Top = 112
        Width = 102
        Height = 13
        Caption = '~Reporting Method'
        FocusControl = wwcbReporting_Method
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablFrequency: TevLabel
        Left = 184
        Top = 112
        Width = 119
        Height = 13
        Caption = '~Reporting Frequency'
        FocusControl = wwcbFrequency
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablReporting_Month: TevLabel
        Left = 408
        Top = 112
        Width = 79
        Height = 13
        Caption = 'Reporting Month'
        FocusControl = wwseReporting_Month
      end
      object lablCL_Bank_Account_Number: TevLabel
        Left = 16
        Top = 64
        Width = 117
        Height = 13
        Caption = '~Client Bank Account'
        FocusControl = wwlcBankAccount
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablDelivery_Group_Number: TevLabel
        Left = 344
        Top = 64
        Width = 70
        Height = 13
        Caption = 'Delivery Group'
        FocusControl = wwlcDeliveryGroup
      end
      object lablCustom_Field: TevLabel
        Left = 344
        Top = 16
        Width = 60
        Height = 13
        Caption = 'Custom Field'
        FocusControl = dedtCustom_Field
      end
      object lablNotes: TevLabel
        Left = 16
        Top = 248
        Width = 28
        Height = 13
        Caption = 'Notes'
        FocusControl = dmemNotes
      end
      object lablAgency_Name: TevLabel
        Left = 16
        Top = 16
        Width = 79
        Height = 13
        Caption = '~Agency Name'
        FocusControl = wwlcAgencyName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TevLabel
        Left = 184
        Top = 64
        Width = 81
        Height = 13
        Caption = '~Payment Type'
        FocusControl = wwPayment_Type
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel10: TevLabel
        Left = 16
        Top = 156
        Width = 36
        Height = 13
        BiDiMode = bdLeftToRight
        Caption = 'GL Tag'
        FocusControl = evDBEdit5
        ParentBiDiMode = False
      end
      object evLabel27: TevLabel
        Left = 16
        Top = 201
        Width = 67
        Height = 13
        Caption = 'GL Offset Tag'
        FocusControl = evDBEdit2
      end
      object lblMemo1: TevLabel
        Left = 184
        Top = 156
        Width = 38
        Height = 13
        BiDiMode = bdLeftToRight
        Caption = 'Memo 1'
        FocusControl = evDBEdit5
        ParentBiDiMode = False
      end
      object lblMemo2: TevLabel
        Left = 184
        Top = 201
        Width = 38
        Height = 13
        BiDiMode = bdLeftToRight
        Caption = 'Memo 2'
        FocusControl = evDBEdit5
        ParentBiDiMode = False
      end
      object dedtCustom_Field: TevDBEdit
        Left = 344
        Top = 32
        Width = 64
        Height = 21
        HelpContext = 12016
        DataField = 'CLIENT_AGENCY_CUSTOM_FIELD'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwseReporting_Month: TevDBSpinEdit
        Left = 448
        Top = 128
        Width = 41
        Height = 21
        HelpContext = 12019
        Increment = 1.000000000000000000
        MaxValue = 12.000000000000000000
        MinValue = 1.000000000000000000
        Value = 1.000000000000000000
        DataField = 'MONTH_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object dmemNotes: TEvDBMemo
        Left = 16
        Top = 269
        Width = 473
        Height = 97
        DataField = 'NOTES'
        DataSource = wwdsDetail
        ScrollBars = ssVertical
        TabOrder = 12
      end
      object wwcbReporting_Method: TevDBComboBox
        Left = 16
        Top = 128
        Width = 145
        Height = 21
        HelpContext = 12027
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'REPORT_METHOD'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'E-mail'#9'M'
          'FTP'#9'F'
          'Electronic'#9'E'
          'Disk'#9'D'
          'Paper'#9'P'
          'None'#9'N')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object wwcbFrequency: TevDBComboBox
        Left = 184
        Top = 128
        Width = 233
        Height = 21
        HelpContext = 12018
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'FREQUENCY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Every Pay'#9'D'
          'Monthly'#9'M'
          'Quarterly'#9'Q'
          'Semi-Annual'#9'N'
          'Annual'#9'A')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object wwlcBankAccount: TevDBLookupCombo
        Left = 16
        Top = 80
        Width = 145
        Height = 21
        HelpContext = 12015
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'ACCOUNT NUMBER'
          'BANK_ACCOUNT_TYPE'#9'1'#9'TYPE')
        DataField = 'CL_BANK_ACCOUNT_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
        LookupField = 'CL_BANK_ACCOUNT_NBR'
        Style = csDropDownList
        TabOrder = 2
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwlcDeliveryGroup: TevDBLookupCombo
        Left = 344
        Top = 80
        Width = 145
        Height = 21
        HelpContext = 12024
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DELIVERY_GROUP_DESCRIPTION'#9'20'#9'DELIVERY_GROUP_DESCRIPTION')
        DataField = 'CL_DELIVERY_GROUP_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_DELIVERY_GROUP.CL_DELIVERY_GROUP
        LookupField = 'CL_DELIVERY_GROUP_NBR'
        Style = csDropDownList
        TabOrder = 4
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object wwlcAgencyName: TevDBLookupCombo
        Left = 16
        Top = 32
        Width = 289
        Height = 21
        HelpContext = 12014
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'Name'#9'F'
          'ADDRESS1'#9'30'#9'Address1'#9'F'
          'CITY'#9'20'#9'City'#9'F'
          'STATE'#9'2'#9'State'#9'F'
          'ZIP_CODE'#9'10'#9'Zip Code'#9'F')
        DataField = 'SB_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SB_AGENCY.SB_AGENCY
        LookupField = 'SB_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 0
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwPayment_Type: TevDBComboBox
        Left = 184
        Top = 80
        Width = 121
        Height = 21
        HelpContext = 12023
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'PAYMENT_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Check'#9'C'
          'Direct Deposit'#9'D'
          'None'#9'N')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object evDBEdit5: TevDBEdit
        Left = 16
        Top = 172
        Width = 145
        Height = 21
        HelpContext = 10668
        DataField = 'GL_TAG'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBEdit2: TevDBEdit
        Left = 16
        Top = 217
        Width = 145
        Height = 21
        HelpContext = 10668
        DataField = 'GL_OFFSET_TAG'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object edMemo1: TevDBEdit
        Left = 184
        Top = 172
        Width = 302
        Height = 21
        HelpContext = 10668
        DataField = 'CHECK_MEMO_LINE_1'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        TabOrder = 10
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object edMemo2: TevDBEdit
        Left = 184
        Top = 217
        Width = 302
        Height = 21
        HelpContext = 10668
        DataField = 'CHECK_MEMO_LINE_2'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        TabOrder = 11
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_AGENCY.CL_AGENCY
  end
  object AgencySrc: TevDataSource
    DataSet = DM_SB_AGENCY.SB_AGENCY
    Left = 265
    Top = 57
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 408
    Top = 21
  end
  object AgencyReportSrc: TevDataSource
    DataSet = DM_SB_AGENCY_REPORTS.SB_AGENCY_REPORTS
    MasterDataSource = AgencySrc
    Left = 304
    Top = 57
  end
end
