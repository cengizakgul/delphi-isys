// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_PIECES;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_CL_BASE, SDataStructure, Db, Wwdatsrc,  StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Mask,
  wwdbedit, EvUIComponents, EvClientDataSet, isUIwwDBEdit, ISBasicClasses,
  SDDClasses, SDataDictclient, ImgList, SDataDicttemp, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_PIECES = class(TEDIT_CL_BASE)
    Details: TTabSheet;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    pnlCLBrowseRight: TevPanel;
    fpCLRight: TisUIFashionPanel;
    pnlFPCLRightBody: TevPanel;
    fpPieceWorkDetail: TisUIFashionPanel;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBEdit2: TevDBEdit;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evDBEdit3: TevDBEdit;
    fpCLPensionFundSummary: TisUIFashionPanel;
    wwDBGrid1: TevDBGrid;
    evPanel1: TevPanel;
    ScrollBox1: TScrollBox;
    evPanel3: TevPanel;
    evPanel2: TevPanel;
    wwDBGrid1x: TevDBGrid;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

{ TEDIT_CL_PIECES }

function TEDIT_CL_PIECES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_PIECES;
end;

function TEDIT_CL_PIECES.GetInsertControl: TWinControl;
begin
  Result := evDBEdit1;
end;

initialization
  RegisterClass(TEDIT_CL_PIECES);

end.
 