// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_COMMON_PAYMASTER;
{**************************************************************************}
{* unit: SPD_EDIT_CL_COMMON_PAYMASTER.pas                                 *}
{* Description: Form used to maintain Client Common Paymaster information *}
{*              from the Client -> Common Paymasters comboboxes.          *}
{*                                                                        *}
{* Copyright(c) PayData Payroll Services, Inc.                            *}
{*                                                                        *}
{* Author:  Tim Stevenson                                                 *}
{* Written: 04/25/1998                                                    *}
{* Updated: 04/28/1998                                                    *}
{*                                                                        *}
{**************************************************************************}

interface

uses
  SDataStructure,  wwdbedit, SPD_EDIT_CL_BASE, StdCtrls,
  ExtCtrls, DBCtrls, Mask, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, Controls, Classes, EvUIComponents, EvUtils, EvClientDataSet,
  ISBasicClasses, isUIwwDBEdit, SDDClasses, SDataDictclient, ImgList,
  SDataDicttemp, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CL_COMMON_PAYMASTER = class(TEDIT_CL_BASE)
    tshtCommon_Paymaster_Details: TTabSheet;
    wwdgCommon_Paymaster: TevDBGrid;
    bev_CL_CO2: TEvBevel;
    wwDBGrid1: TevDBGrid;
    wwdsSubMaster: TevDataSource;
    DM_COMPANY: TDM_COMPANY;
    sbCLBrowse: TScrollBox;
    pnlCLBrowseBorder: TevPanel;
    splitSkills: TevSplitter;
    pnlCLBrowseLeft: TevPanel;
    fpCLLeft: TisUIFashionPanel;
    pnlFPCLLeftBody: TevPanel;
    pnlCLBrowseRight: TevPanel;
    fpCLRight: TisUIFashionPanel;
    pnlFPCLRightBody: TevPanel;
    fpCLConsolidatedGrid: TisUIFashionPanel;
    fpCLConsolidatedDetails: TisUIFashionPanel;
    lablCPM_Name: TevLabel;
    dedtCPM_Name: TevDBEdit;
    drgpCombine_OASDI: TevDBRadioGroup;
    drgpCombine_Medicare: TevDBRadioGroup;
    drgpCombine_FUI: TevDBRadioGroup;
    drgpCombine_State_SUI: TevDBRadioGroup;
    wwdgBrowse: TevDBGrid;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CL_COMMON_PAYMASTER.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_COMPANY.CO, aDS);
end;

function TEDIT_CL_COMMON_PAYMASTER.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_COMMON_PAYMASTER;
end;

function TEDIT_CL_COMMON_PAYMASTER.GetInsertControl: TWinControl;
begin
  Result := dedtCPM_NAME;
end;

initialization
  RegisterClass(TEDIT_CL_COMMON_PAYMASTER);

end.
