// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SClientCopyWizard;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, SCopyWizard,
  SDataStructure, Db, Wwdatsrc,  ComCtrls, Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, Buttons, ExtCtrls, evUtils, Mask, wwdbedit, Wwdotdot,
  Wwdbcomb, wwdbdatetimepicker, wwdblook, SDDClasses, EvContext, isVCLBugFix, 
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictclient, SDataDicttemp, EvUIUtils, EvUIComponents;

type
  TClientCopyWizard = class(TCopyWizard)
    tshCL: TTabSheet;
    tshCL_AGENCY: TTabSheet;
    tshCL_BANK_ACCOUNT: TTabSheet;
    tshCL_BILLING: TTabSheet;
    tshCL_DELIVERY_METHOD: TTabSheet;
    tshCL_PENSION: TTabSheet;
    dsCL: TevDataSource;
    dsCL_BANK_ACCOUNT: TevDataSource;
    CL_BANK_ACCOUNTxxCUSTOM_BANK_ACCOUNT_NUMBER: TevDBEdit;
    CL_BANK_ACCOUNTxxNEXT_CHECK_NUMBER: TevDBEdit;
    lblCL_BANK_ACCOUNTxxCUSTOM_BANK_ACCOUNT_NUMBER: TevLabel;
    lblCL_BANK_ACCOUNTxxNEXT_CHECK_NUMBER: TevLabel;
    dsCL_BILLING: TevDataSource;
    lblCLxxCUSTOM_CLIENT_NUMBER: TevLabel;
    CLxxCUSTOM_CLIENT_NUMBER: TevDBEdit;
    lblCLxxNAME: TevLabel;
    CLxxNAME: TevDBEdit;
    lblCLxxADDRESS1: TevLabel;
    CLxxADDRESS1: TevDBEdit;
    lblCLxxADDRESS2: TevLabel;
    CLxxADDRESS2: TevDBEdit;
    lblCLxxCITY: TevLabel;
    CLxxCITY: TevDBEdit;
    lblCLxxSTATE: TevLabel;
    CLxxSTATE: TevDBEdit;
    lblCLxxZIP_CODE: TevLabel;
    CLxxZIP_CODE: TevDBEdit;
    grbxClient_PrimaryFax: TevGroupBox;
    lblCLxxFAX1: TevLabel;
    lblCLxxFAX1_DESCRIPTION: TevLabel;
    CLxxFAX1_DESCRIPTION: TevDBEdit;
    CLxxFAX1: TevDBEdit;
    grbxClient_SecondaryFax: TevGroupBox;
    lblCLxxFAX2: TevLabel;
    lblCLxxFAX2_DESCRIPTION: TevLabel;
    CLxxFAX2_DESCRIPTION: TevDBEdit;
    CLxxFAX2: TevDBEdit;
    grbxClient_SecondaryContact: TevGroupBox;
    lblCLxxCONTACT2: TevLabel;
    lblCLxxPHONE2: TevLabel;
    lblCLxxDESCRIPTION2: TevLabel;
    lblCLxxE_MAIL_ADDRESS2: TevLabel;
    CLxxCONTACT2: TevDBEdit;
    CLxxDESCRIPTION2: TevDBEdit;
    CLxxE_MAIL_ADDRESS2: TevDBEdit;
    CLxxPHONE2: TevDBEdit;
    grbxClient_PrimaryContact: TevGroupBox;
    lblCLxxCONTACT1: TevLabel;
    lblCLxxPHONE1: TevLabel;
    lblCLxxDESCRIPTION1: TevLabel;
    lblCLxxE_MAIL_ADDRESS1: TevLabel;
    CLxxCONTACT1: TevDBEdit;
    CLxxDESCRIPTION1: TevDBEdit;
    CLxxE_MAIL_ADDRESS1: TevDBEdit;
    CLxxPHONE1: TevDBEdit;
    grCL: TevDBGrid;
    grBankAccount: TevDBGrid;
    grBilling: TevDBGrid;
    lblCL_BILLINGxxNAME: TevLabel;
    CL_BILLINGxxNAME: TevDBEdit;
    lblCL_BILLINGxxADDRESS1: TevLabel;
    CL_BILLINGxxADDRESS1: TevDBEdit;
    lblCL_BILLINGxxADDRESS2: TevLabel;
    CL_BILLINGxxADDRESS2: TevDBEdit;
    lblCL_BILLINGxxCITY: TevLabel;
    CL_BILLINGxxCITY: TevDBEdit;
    lblCL_BILLINGxxSTATE: TevLabel;
    CL_BILLINGxxSTATE: TevDBEdit;
    lblCL_BILLINGxxZIP_CODE: TevLabel;
    CL_BILLINGxxZIP_CODE: TevDBEdit;
    grbxContact_Info: TevGroupBox;
    lblCL_BILLINGxxCONTACT: TevLabel;
    lblCL_BILLINGxxPHONE: TevLabel;
    lblCL_BILLINGxxDESCRIPTION: TevLabel;
    lblCL_BILLINGxxFAX: TevLabel;
    lblCL_BILLINGxxFAX_DESCRIPTION: TevLabel;
    lblCL_BILLINGxxE_MAIL_ADDRESS: TevLabel;
    CL_BILLINGxxCONTACT: TevDBEdit;
    CL_BILLINGxxPHONE: TevDBEdit;
    CL_BILLINGxxDESCRIPTION: TevDBEdit;
    CL_BILLINGxxFAX: TevDBEdit;
    CL_BILLINGxxFAX_DESCRIPTION: TevDBEdit;
    CL_BILLINGxxE_MAIL_ADDRESS: TevDBEdit;
    CLxxSTART_DATE: TevDBDateTimePicker;
    lblCLxxSTART_DATE: TevLabel;
    lblCL_DELIVERY_METHODxxNAME: TevLabel;
    CL_DELIVERY_METHODxxNAME: TevDBEdit;
    lblCL_DELIVERY_METHODxxADDRESS1: TevLabel;
    CL_DELIVERY_METHODxxADDRESS1: TevDBEdit;
    lblCL_DELIVERY_METHODxxADDRESS2: TevLabel;
    CL_DELIVERY_METHODxxADDRESS2: TevDBEdit;
    lblCL_DELIVERY_METHODxxCITY: TevLabel;
    CL_DELIVERY_METHODxxCITY: TevDBEdit;
    lblCL_DELIVERY_METHODxxSTATE: TevLabel;
    CL_DELIVERY_METHODxxSTATE: TevDBEdit;
    lblCL_DELIVERY_METHODxxZIP_CODE: TevLabel;
    CL_DELIVERY_METHODxxZIP_CODE: TevDBEdit;
    lblCL_DELIVERY_METHODxxATTENTION: TevLabel;
    CL_DELIVERY_METHODxxATTENTION: TevDBEdit;
    lblCL_DELIVERY_METHODxxPHONE_NUMBER: TevLabel;
    CL_DELIVERY_METHODxxPHONE_NUMBER: TevDBEdit;
    dsCL_DELIVERY_METHOD: TevDataSource;
    grAgency: TevDBGrid;
    dsCL_AGENCY: TevDataSource;
    lblCL_AGENCYxxCL_BANK_ACCOUNT_NBR: TevLabel;
    CL_AGENCYxxCL_BANK_ACCOUNT_NBR: TevDBLookupCombo;
    grPension: TevDBGrid;
    dsCL_PENSION: TevDataSource;
    lblCL_PENSIONxxPLAN_ID: TevLabel;
    CL_PENSIONxxPLAN_ID: TevDBEdit;
    lblCL_PENSIONxxPOLICY_ID: TevLabel;
    CL_PENSIONxxPOLICY_ID: TevDBEdit;
    tshEndPage: TTabSheet;
    lblCL_BANK_ACCOUNTxxBank_Name: TevLabel;
    evDBGrid1: TevDBGrid;
    evButton1: TevBitBtn;
    evButton2: TevBitBtn;
    lblCL_AGENCYxxAgency_Name: TevLabel;
    lblCL_PENSIONxxNAME: TevLabel;
    DM_CLIENT: TDM_CLIENT;
    procedure btNextClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure evButton1Click(Sender: TObject);
    procedure evButton2Click(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure btBackClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure HandleInitialPage;
  protected
    function ProcessValue(TableName: String; aField: TField): Variant; override;
  public
    { Public declarations }
    MyTableList: TtList;//TTableList;
    constructor CreateTCopyWizard(AOwner: TComponent; L: TDBLevel);
  end;

var
  ClientCopyWizard: TClientCopyWizard;

implementation


{$R *.DFM}

{ TClientCopyWizard }

constructor TClientCopyWizard.CreateTCopyWizard(AOwner: TComponent; L: TDBLevel);
begin
  Inherited Create(AOwner);
end;

procedure TClientCopyWizard.btNextClick(Sender: TObject);
var
  CTable: String;
  Dummy: TtList;
begin
  inherited;
  case pctCopyWizard.ActivePageIndex of
    0: HandleInitialPage
    else
    begin
      CTable := HandleClientPages(MyTableList.TableList, True);
      if CTable = 'CL_AGENCY' then
        TmpDS.Data := MyTableList.TableList[GetTableIndexByName('CL_BANK_ACCOUNT', MyTableList.TableList)].DSBase.Data
      else
      if CTable = 'EndPage' then
      begin
        btNext.Enabled := False;
        ctx_StartWait('Saving new client...');
        try
          MyTableList.CLNew := CreateNewDBStructure(MyTableList, Dummy, evClient);
        finally
          ctx_EndWait;
        end;
        ModalResult := mrOK;
      end;
    end;
  end;
end;

procedure TClientCopyWizard.HandleInitialPage;
var
  aTableInfo: TvsTableInf;
  aTableList: TTableList;
  S: String;
begin
  aTableList := Nil;
  if not Assigned(MyTableList.TableList) then
  begin
    ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CL['CL_NBR']);
    aTableList := CreateTableList(evClient, TablesToCopyCLList);
    MyTableList.TableList := aTableList;
    MyTableList.CLBase := DM_TEMPORARY.TMP_CL['CL_NBR'];
  end;
  if cbUseImport.Checked then
  if not EvDialog('Input Required', 'Please, enter a client number you want to create from import file:', S) then
  begin
    evPanel2.Visible := True;
    AbortEx;
  end;
  CLNumber := S;  
  pctCopyWizard.ActivePageIndex := pctCopyWizard.ActivePageIndex + 1;
  S := Copy(pctCopyWizard.Pages[pctCopyWizard.ActivePageIndex].Name, 4, Length(pctCopyWizard.Pages[pctCopyWizard.ActivePageIndex].Name));
  aTableInfo := MyTableList.TableList[GetTableIndexByName(S, MyTableList.TableList)];
  Self.Caption := 'Client Copy Wizard - ' + aTableInfo.Name;
  LoadExcludeList(aTableInfo);
  CleanOutFields(aTableInfo);
  dsCL.DataSet := aTableInfo.DSBase;
  SetFocusOnActivePage;
end;

procedure TClientCopyWizard.FormShow(Sender: TObject);
begin
  inherited;
  ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_AGENCY]);
end;

procedure TClientCopyWizard.evButton1Click(Sender: TObject);
var
  TableInfo, CLTableInfo: TvsTableInf;
begin
  inherited;
  TableInfo := MyTableList.TableList[GetTableIndexByName('CL_BILLING', MyTableList.TableList)];
  CLTableInfo := MyTableList.TableList[GetTableIndexByName('CL', MyTableList.TableList)];
  with TableInfo.DSBase do
  begin
    Edit;
    FieldByName('NAME').AsString := CLTableInfo.DSBase.FieldByName('NAME').AsString;
    FieldByName('ADDRESS1').AsString := CLTableInfo.DSBase.FieldByName('ADDRESS1').AsString;
    FieldByName('ADDRESS2').AsString := CLTableInfo.DSBase.FieldByName('ADDRESS2').AsString;
    FieldByName('CITY').AsString := CLTableInfo.DSBase.FieldByName('CITY').AsString;
    FieldByName('STATE').AsString := CLTableInfo.DSBase.FieldByName('STATE').AsString;
    FieldByName('ZIP_CODE').AsString := CLTableInfo.DSBase.FieldByName('ZIP_CODE').AsString;
    FieldByName('CONTACT').AsString := CLTableInfo.DSBase.FieldByName('CONTACT1').AsString;
    FieldByName('PHONE').AsString := CLTableInfo.DSBase.FieldByName('PHONE1').AsString;
    FieldByName('DESCRIPTION').AsString := CLTableInfo.DSBase.FieldByName('DESCRIPTION1').AsString;
    FieldByName('E_MAIL_ADDRESS').AsString := CLTableInfo.DSBase.FieldByName('E_MAIL_ADDRESS1').AsString;
    Post;
  end;
end;

procedure TClientCopyWizard.evButton2Click(Sender: TObject);
var
  TableInfo, CLTableInfo: TvsTableInf;
begin
  inherited;
  TableInfo := MyTableList.TableList[GetTableIndexByName('CL_DELIVERY_METHOD', MyTableList.TableList)];
  CLTableInfo := MyTableList.TableList[GetTableIndexByName('CL', MyTableList.TableList)];
  with TableInfo.DSBase do
  begin
    Edit;
    FieldByName('NAME').AsString := CLTableInfo.DSBase.FieldByName('NAME').AsString;
    FieldByName('ADDRESS1').AsString := CLTableInfo.DSBase.FieldByName('ADDRESS1').AsString;
    FieldByName('ADDRESS2').AsString := CLTableInfo.DSBase.FieldByName('ADDRESS2').AsString;
    FieldByName('CITY').AsString := CLTableInfo.DSBase.FieldByName('CITY').AsString;
    FieldByName('STATE').AsString := CLTableInfo.DSBase.FieldByName('STATE').AsString;
    FieldByName('ZIP_CODE').AsString := CLTableInfo.DSBase.FieldByName('ZIP_CODE').AsString;
    FieldByName('ATTENTION').AsString := CLTableInfo.DSBase.FieldByName('CONTACT1').AsString;
    FieldByName('PHONE_NUMBER').AsString := CLTableInfo.DSBase.FieldByName('PHONE1').AsString;
    Post;
  end;
end;

procedure TClientCopyWizard.btCancelClick(Sender: TObject);
begin
  inherited;
  with DM_CLIENT do
    ctx_DataAccess.CancelDataSets([CL, CL_BANK_ACCOUNT, CL_BILLING, CL_DELIVERY_METHOD,
                    CL_DELIVERY_GROUP, CL_AGENCY, CL_E_D_GROUPS, CL_PENSION,
                    CL_PIECES, CL_TIMECLOCK_IMPORTS, CL_FUNDS, CL_E_DS,
                    CL_E_D_GROUP_CODES, CL_E_D_LOCAL_EXMPT_EXCLD,
                    CL_E_D_STATE_EXMPT_EXCLD, CL_3_PARTY_SICK_PAY_ADMIN,
                    CL_REPORT_WRITER_REPORTS]);
end;

procedure TClientCopyWizard.btBackClick(Sender: TObject);
begin
  HandleClientPages(MyTableList.TableList, False);
  inherited;
end;

function TClientCopyWizard.ProcessValue(TableName: String; aField: TField): Variant;
begin
  if (TableName = 'CL_AGENCY') and (aField.FieldName = 'CL_BANK_ACCOUNT_NBR') then
    Result := DM_CLIENT.CL_BANK_ACCOUNT.Lookup('CUSTOM_BANK_ACCOUNT_NUMBER', aField.Value, 'CL_BANK_ACCOUNT_NBR')
  else
    Result := inherited ProcessValue(TableName, aField);
end;

procedure TClientCopyWizard.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  DM_TEMPORARY.TMP_CO.RetrieveCondition := '';
  DM_TEMPORARY.TMP_CO.Close;
end;

end.
