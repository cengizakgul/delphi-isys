inherited EDIT_CL_TIMECLOCK_IMPORTS: TEDIT_CL_TIMECLOCK_IMPORTS
  inherited Panel1: TevPanel
    inherited pnlTopRight: TevPanel
      inherited DBText1: TevDBText
        Left = 112
      end
    end
  end
  inherited PageControl1: TevPageControl
    HelpContext = 27503
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited pnlFashionBody: TevPanel
            inherited pnlSubbrowse: TevPanel
              object wwDBGrid1: TevDBGrid
                Left = 0
                Top = 0
                Width = 477
                Height = 565
                DisableThemesInTitle = False
                Selected.Strings = (
                  'DESCRIPTION'#9'20'#9'Description'
                  'TIMECLOCK_TYPE'#9'1'#9'Type'#9'F'
                  'SB_SUPPORTED'#9'1'#9'SB Supported')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_TIMECLOCK_IMPORTS\wwDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 207
                IniAttributes.SectionName = 'TEDIT_CL_TIMECLOCK_IMPORTS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Title = 'Timeclock Imports'
              object wwdgBrowse: TevDBGrid
                Left = 18
                Top = 48
                Width = 271
                Height = 207
                DisableThemesInTitle = False
                Selected.Strings = (
                  'DESCRIPTION'#9'34'#9'Description'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_CL_CO_CONSOLIDATION\wwdgBrowse'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsDetail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object tshtTimeclock_Import_Details: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object bev_cl_ed_tc1a: TEvBevel
        Left = 0
        Top = 0
        Width = 427
        Height = 183
        Align = alClient
      end
      object lablDescription: TevLabel
        Left = 16
        Top = 16
        Width = 65
        Height = 13
        Caption = '~Description'
        FocusControl = dedtDescription
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablTimeclock_Type: TevLabel
        Left = 16
        Top = 56
        Width = 91
        Height = 13
        Caption = '~Timeclock Type'
        FocusControl = wwcbTimeclock_Type
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablHardware_Type: TevLabel
        Left = 16
        Top = 96
        Width = 87
        Height = 13
        Caption = '~Hardware Type'
        FocusControl = wwcbHardware_Type
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablDate_Lease_Ends: TevLabel
        Left = 16
        Top = 264
        Width = 82
        Height = 13
        Caption = 'Date Lease Ends'
      end
      object lablPayroll_Import_Format: TevLabel
        Left = 16
        Top = 304
        Width = 124
        Height = 13
        Caption = '~Payroll Import Format '
        FocusControl = wwcbPayroll_Import_Format
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablPayroll_Export_Format: TevLabel
        Left = 16
        Top = 344
        Width = 121
        Height = 13
        Caption = '~Payroll Export Format'
        FocusControl = wwcbPayroll_Export_Format
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object bev_cl_ed_tc1b: TEvBevel
        Left = 256
        Top = 16
        Width = 6
        Height = 369
        Style = bsRaised
      end
      object Label4: TevLabel
        Left = 312
        Top = 158
        Width = 75
        Height = 13
        Caption = 'Export Directory'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dedtDescription: TevDBEdit
        Left = 16
        Top = 32
        Width = 185
        Height = 21
        HelpContext = 27503
        DataField = 'DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object grbxInstallation_Information: TevGroupBox
        Left = 312
        Top = 16
        Width = 289
        Height = 129
        HelpContext = 27503
        Caption = 'Installation Information'
        TabOrder = 8
        TabStop = True
        object lablScheduled_Install_Date: TevLabel
          Left = 24
          Top = 24
          Width = 107
          Height = 13
          Caption = 'Scheduled Install Date'
        end
        object lablActual_Install_Date: TevLabel
          Left = 152
          Top = 24
          Width = 86
          Height = 13
          Caption = 'Actual Install Date'
        end
        object lablInstaller: TevLabel
          Left = 24
          Top = 72
          Width = 36
          Height = 13
          Caption = 'Installer'
          FocusControl = dedtInstaller
        end
        object dedtInstaller: TevDBEdit
          Left = 24
          Top = 88
          Width = 225
          Height = 21
          DataField = 'INSTALLER'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwdpScheduled_Install_Date: TevDBDateTimePicker
          Left = 24
          Top = 40
          Width = 81
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          DataField = 'SCHEDULED_INSTALL_DATE'
          DataSource = wwdsDetail
          Epoch = 1950
          ShowButton = True
          TabOrder = 0
        end
        object wwdpActual_Install_Date: TevDBDateTimePicker
          Left = 152
          Top = 40
          Width = 81
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          DataField = 'ACTUAL_INSTALL_DATE'
          DataSource = wwdsDetail
          Epoch = 1950
          ShowButton = True
          TabOrder = 1
        end
      end
      object wwcbTimeclock_Type: TevDBComboBox
        Left = 16
        Top = 72
        Width = 185
        Height = 21
        HelpContext = 27503
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'TIMECLOCK_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 10
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object wwcbHardware_Type: TevDBComboBox
        Left = 16
        Top = 112
        Width = 185
        Height = 21
        HelpContext = 27503
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'HARDWARE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Verifone'#9'V'
          'PC'#9'P'
          'None'#9'N')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object drgpConnection_Type: TevDBRadioGroup
        Left = 16
        Top = 144
        Width = 185
        Height = 57
        HelpContext = 27503
        Caption = '~Connection Type'
        DataField = 'CONNECTION_TYPE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Direct Connection'
          'Modem')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'D'
          'M')
      end
      object drgpSB_Supported: TevDBRadioGroup
        Left = 16
        Top = 208
        Width = 185
        Height = 50
        HelpContext = 27503
        Caption = '~Is This SB Supported'
        Columns = 2
        DataField = 'SB_SUPPORTED'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 4
        Values.Strings = (
          'Y'
          'N')
      end
      object wwcbPayroll_Import_Format: TevDBComboBox
        Left = 16
        Top = 320
        Width = 185
        Height = 21
        HelpContext = 27503
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'PAYROLL_IMPORT_FORMAT'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object wwcbPayroll_Export_Format: TevDBComboBox
        Left = 16
        Top = 360
        Width = 185
        Height = 21
        HelpContext = 27503
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'PAYROLL_EXPORT_FORMAT'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object grbxNotes: TevGroupBox
        Left = 312
        Top = 208
        Width = 289
        Height = 161
        Caption = 'Notes'
        TabOrder = 11
        TabStop = True
        object dmemNotes: TEvDBMemo
          Left = 16
          Top = 24
          Width = 257
          Height = 121
          HelpContext = 27503
          DataField = 'NOTES'
          DataSource = wwdsDetail
          TabOrder = 0
        end
      end
      object wwdpDate_Lease_Ends: TevDBDateTimePicker
        Left = 16
        Top = 280
        Width = 81
        Height = 21
        HelpContext = 27503
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'DATE_LEASE_ENDS'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 5
      end
      object wwdeExport_Directory: TevDBEdit
        Left = 312
        Top = 174
        Width = 257
        Height = 21
        HelpContext = 27503
        DataField = 'EXPORT_FILE_PATH'
        DataSource = wwdsDetail
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object butnOpenARImportDirectory: TevButton
        Left = 576
        Top = 170
        Width = 25
        Height = 25
        Hint = 'List directories'
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
        OnClick = butnOpenARImportDirectoryClick
        Margin = 0
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_TIMECLOCK_IMPORTS.CL_TIMECLOCK_IMPORTS
  end
  object dlgOpen: TOpenDialog
    Left = 596
    Top = 192
  end
end
