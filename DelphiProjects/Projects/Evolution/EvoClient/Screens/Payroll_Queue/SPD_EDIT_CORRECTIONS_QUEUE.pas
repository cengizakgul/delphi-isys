// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CORRECTIONS_QUEUE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFrameEntry, Db,   Grids, Wwdbigrd, EvContext,
  Wwdbgrid, Wwdatsrc, SDataStructure, StdCtrls, Buttons, ExtCtrls, Variants,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, SDataDicttemp,
  ISDataAccessComponents, EvDataAccessComponents, EvCommonInterfaces,
  ActnList, EvUIComponents, EvClientDataSet, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TEDIT_CORRECTIONS_QUEUE = class(TFrameEntry)
    dbgrPayrolls: TevDBGrid;
    TMP_PR_LOCAL: TevClientDataSet;
    TMP_PR_LOCALCL_NBR: TIntegerField;
    TMP_PR_LOCALCO_NBR: TIntegerField;
    TMP_PR_LOCALPR_NBR: TIntegerField;
    TMP_PR_LOCALCLIENT_NUMBER: TStringField;
    TMP_PR_LOCALCOMPANY_NUMBER: TStringField;
    TMP_PR_LOCALCOMPANY_NAME: TStringField;
    TMP_PR_LOCALCHECK_DATE: TDateTimeField;
    TMP_PR_LOCALRUN_NUMBER: TIntegerField;
    TMP_PR_LOCALTYPE: TStringField;
    TMP_PR_LOCALSTATUS: TStringField;
    dsTMP_PR_LOCAL: TevDataSource;
    DM_TEMPORARY: TDM_TEMPORARY;
    TMP_PR_LOCALAPPROVED_BY_TAX: TStringField;
    TMP_PR_LOCALAPPROVED_BY_FINANCE: TStringField;
    TMP_PR_LOCALAPPROVED_BY_MANAGEMENT: TStringField;
    evPanel1: TevPanel;
    rgrpShow: TevRadioGroup;
    lablApprovalRights: TevLabel;
    butnRefresh: TevBitBtn;
    butnSBPayroll: TevBitBtn;
    butnPayrollBatch: TevBitBtn;
    butnSubmit: TevBitBtn;
    ActionList1: TActionList;
    procedure butnRefreshClick(Sender: TObject);
    procedure butnSubmitClick(Sender: TObject);
    procedure dsTMP_PR_LOCALDataChange(Sender: TObject; Field: TField);
    procedure butnPayrollBatchClick(Sender: TObject);
    procedure butnSBPayrollClick(Sender: TObject);
    procedure dbgrPayrollsCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure rgrpShowClick(Sender: TObject);
    procedure dbgrPayrollsDblClick(Sender: TObject);
    procedure TMP_PR_LOCALAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FRefreshing: Boolean;
    FTax, FFinance, FManagement: Boolean;
    procedure PopulateTMP_PR_LOCAL;
    procedure FilterTMP_PR_LOCAL;
    procedure PostChanges(ChangeStatus: Boolean);
  public
    { Public declarations }
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

uses
  EvUtils, EvTypes, EvConsts, SPD_CorrectionRun, isBaseClasses;

{$R *.DFM}

{ TEDIT_CORRECTIONS_QUEUE }

procedure TEDIT_CORRECTIONS_QUEUE.Activate;
begin
  inherited;
  FRefreshing := False;
  DM_TEMPORARY.TMP_CL.DataRequired('CL_NBR>0');
  DM_TEMPORARY.TMP_CO.DataRequired('CL_NBR>0');
  PopulateTMP_PR_LOCAL;
end;

procedure TEDIT_CORRECTIONS_QUEUE.PopulateTMP_PR_LOCAL;
var
  Category: String;
begin
  TMP_PR_LOCAL.DisableControls;
  FRefreshing := True;
  try
    if DM_TEMPORARY.TMP_PR.Active then
      DM_TEMPORARY.TMP_PR.Close;
    DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
    DM_TEMPORARY.TMP_PR.DataRequired('(PAYROLL_TYPE in (' + ListCorrectionRunTypes + ') or '+
    ' STATUS in (''' + PAYROLL_STATUS_HOLD
     + ''', ''' + PAYROLL_STATUS_BACKDATED + ''', ''' + PAYROLL_STATUS_REQUIRED_MGR_APPROVAL + '''))'
     + GetReadOnlyClintCompanyListFilter);

    DM_TEMPORARY.TMP_PR.IndexFieldNames := 'CL_NBR;CO_NBR';
    if TMP_PR_LOCAL.Active then
      TMP_PR_LOCAL.Close;
    TMP_PR_LOCAL.CreateDataSet;
    TMP_PR_LOCAL.FieldByName('APPROVED_BY_TAX').ReadOnly := False;
    TMP_PR_LOCAL.FieldByName('APPROVED_BY_FINANCE').ReadOnly := False;
    TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').ReadOnly := False;

    DM_TEMPORARY.TMP_PR.First;
    while not DM_TEMPORARY.TMP_PR.EOF do
    begin
        TMP_PR_LOCAL.Append;
        TMP_PR_LOCAL.FieldByName('CL_NBR').Value := DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').Value;
        TMP_PR_LOCAL.FieldByName('CO_NBR').Value := DM_TEMPORARY.TMP_PR.FieldByName('CO_NBR').Value;
        TMP_PR_LOCAL.FieldByName('PR_NBR').Value := DM_TEMPORARY.TMP_PR.FieldByName('PR_NBR').Value;
        TMP_PR_LOCAL.FieldByName('CLIENT_NUMBER').Value := DM_TEMPORARY.TMP_CL.NewLookup('CL_NBR', DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').Value, 'CUSTOM_CLIENT_NUMBER');
        TMP_PR_LOCAL.FieldByName('COMPANY_NUMBER').Value := DM_TEMPORARY.TMP_CO.NewLookup('CL_NBR;CO_NBR', VarArrayOf([DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').Value, DM_TEMPORARY.TMP_PR.FieldByName('CO_NBR').Value]), 'CUSTOM_COMPANY_NUMBER');
        TMP_PR_LOCAL.FieldByName('COMPANY_NAME').Value := DM_TEMPORARY.TMP_CO.NewLookup('CL_NBR;CO_NBR', VarArrayOf([DM_TEMPORARY.TMP_PR.FieldByName('CL_NBR').Value, DM_TEMPORARY.TMP_PR.FieldByName('CO_NBR').Value]), 'NAME');
        TMP_PR_LOCAL.FieldByName('CHECK_DATE').Value := DM_TEMPORARY.TMP_PR.FieldByName('CHECK_DATE').Value;
        TMP_PR_LOCAL.FieldByName('RUN_NUMBER').Value := DM_TEMPORARY.TMP_PR.FieldByName('RUN_NUMBER').Value;
        TMP_PR_LOCAL.FieldByName('PAYROLL_TYPE').Value := DM_TEMPORARY.TMP_PR.FieldByName('PAYROLL_TYPE').Value;
        TMP_PR_LOCAL.FieldByName('STATUS').Value := DM_TEMPORARY.TMP_PR.FieldByName('STATUS').Value;
        //Issue#67515- initialize value
        if ( TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_REQUIRED_MGR_APPROVAL ) then
        begin
          TMP_PR_LOCAL.FieldByName('APPROVED_BY_TAX').Value := PR_APPROVED_IGNORE;
          TMP_PR_LOCAL.FieldByName('APPROVED_BY_FINANCE').Value := PR_APPROVED_IGNORE;
          if DM_TEMPORARY.TMP_PR.FieldByName('APPROVED_BY_MANAGEMENT').Value <> PR_APPROVED_YES
          then TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').Value := PR_APPROVED_NO
          else TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').Value := PR_APPROVED_YES;
        end
        else
        begin
          if DM_TEMPORARY.TMP_PR.FieldByName('APPROVED_BY_TAX').Value <> PR_APPROVED_YES
          then TMP_PR_LOCAL.FieldByName('APPROVED_BY_TAX').Value := PR_APPROVED_NO
          else TMP_PR_LOCAL.FieldByName('APPROVED_BY_TAX').Value := PR_APPROVED_YES;

          if DM_TEMPORARY.TMP_PR.FieldByName('APPROVED_BY_FINANCE').Value <> PR_APPROVED_YES
          then TMP_PR_LOCAL.FieldByName('APPROVED_BY_FINANCE').Value := PR_APPROVED_NO
          else TMP_PR_LOCAL.FieldByName('APPROVED_BY_FINANCE').Value := PR_APPROVED_YES;

          if DM_TEMPORARY.TMP_PR.FieldByName('APPROVED_BY_MANAGEMENT').Value <> PR_APPROVED_YES
          then TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').Value := PR_APPROVED_NO
          else TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').Value := PR_APPROVED_YES;
        end;
        TMP_PR_LOCAL.Post;
        DM_TEMPORARY.TMP_PR.Next;
    end;

    SortDSby( TMP_PR_LOCAL, 'CHECK_DATE;RUN_NUMBER', 'CHECK_DATE' );
    DM_TEMPORARY.TMP_PR.IndexFieldNames := '';
    DM_TEMPORARY.TMP_PR.LookupsEnabled := True;
    TMP_PR_LOCAL.First;

    FTax := False;
    FFinance := False;
    FManagement := False;
    DM_SERVICE_BUREAU.SB_TEAM.Activate;
    DM_SERVICE_BUREAU.SB_TEAM_MEMBERS.Activate;
    DM_SERVICE_BUREAU.SB_TEAM_MEMBERS.Filter := 'SB_USER_NBR=' + IntToStr(Context.UserAccount.InternalNBR);
    DM_SERVICE_BUREAU.SB_TEAM_MEMBERS.Filtered := True;
    DM_SERVICE_BUREAU.SB_TEAM_MEMBERS.First;
    while not DM_SERVICE_BUREAU.SB_TEAM_MEMBERS.EOF do
    begin
      Category := DM_SERVICE_BUREAU.SB_TEAM.Lookup('SB_TEAM_NBR', DM_SERVICE_BUREAU.SB_TEAM_MEMBERS.FieldByName('SB_TEAM_NBR').Value, 'CR_CATEGORY');
      case Category[1] of
        CR_CATEGORY_TAX:
          FTax := True;
        CR_CATEGORY_FINANCE:
          FFinance := True;
        CR_CATEGORY_MANAGEMENT:
          FManagement  := True;
      end;
      DM_SERVICE_BUREAU.SB_TEAM_MEMBERS.Next;
    end;
    DM_SERVICE_BUREAU.SB_TEAM_MEMBERS.Filtered := False;

    TMP_PR_LOCAL.FieldByName('APPROVED_BY_TAX').ReadOnly := not FTax;
    TMP_PR_LOCAL.FieldByName('APPROVED_BY_FINANCE').ReadOnly := not FFinance;
    TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').ReadOnly := not FManagement;

    lablApprovalRights.Caption := 'You have';
    if FTax then
      lablApprovalRights.Caption := lablApprovalRights.Caption + ' TAX,';
    if FFinance then
      lablApprovalRights.Caption := lablApprovalRights.Caption + ' FINANCE,';
    if FManagement then
      lablApprovalRights.Caption := lablApprovalRights.Caption + ' MANAGEMENT,';
    if lablApprovalRights.Caption[Length(lablApprovalRights.Caption)] = ',' then
      lablApprovalRights.Caption := Copy(lablApprovalRights.Caption, 1, Length(lablApprovalRights.Caption) - 1)
    else
      lablApprovalRights.Caption := lablApprovalRights.Caption + ' NO';
    lablApprovalRights.Caption := lablApprovalRights.Caption + ' approval rights';
  finally
    FRefreshing := False;
    TMP_PR_LOCAL.EnableControls;
  end;
  FilterTMP_PR_LOCAL;
end;

procedure TEDIT_CORRECTIONS_QUEUE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck([DM_TEMPORARY.TMP_CL, DM_TEMPORARY.TMP_CO], SupportDataSets, '');
end;

procedure TEDIT_CORRECTIONS_QUEUE.butnRefreshClick(Sender: TObject);
begin
  inherited;
  PopulateTMP_PR_LOCAL;
end;

procedure TEDIT_CORRECTIONS_QUEUE.PostChanges(ChangeStatus: Boolean);
begin
  if TMP_PR_LOCAL.RecordCount = 0 then
    Exit;

  ctx_StartWait;
  try
    ctx_DataAccess.OpenClient(TMP_PR_LOCAL.FieldByName('CL_NBR').Value);
    DM_COMPANY.CO.ClientID := ctx_DataAccess.ClientID;
    DM_PAYROLL.PR.DataRequired('PR_NBR=' + TMP_PR_LOCAL.FieldByName('PR_NBR').AsString);
    if (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_COMPLETED ) then
      Exit;
    DM_PAYROLL.PR.Edit;
    if ChangeStatus then
      DM_PAYROLL.PR.FieldByName('STATUS').AsString := PAYROLL_STATUS_COMPLETED;

    if DM_PAYROLL.PR['APPROVED_BY_TAX'] <> TMP_PR_LOCAL['APPROVED_BY_TAX'] then
    begin
      if TMP_PR_LOCAL['APPROVED_BY_TAX'] = 'Y' then
        DM_PAYROLL.PR['APPROVED_BY_TAX_NBR'] := Context.UserAccount.InternalNbr
      else
        DM_PAYROLL.PR.FieldByName('APPROVED_BY_TAX_NBR').Clear;
    end;

    if DM_PAYROLL.PR['APPROVED_BY_FINANCE'] <> TMP_PR_LOCAL['APPROVED_BY_FINANCE'] then
    begin
      if TMP_PR_LOCAL['APPROVED_BY_FINANCE'] = 'Y' then
        DM_PAYROLL.PR['APPROVED_BY_FINANCE_NBR'] := Context.UserAccount.InternalNbr
      else
        DM_PAYROLL.PR.FieldByName('APPROVED_BY_FINANCE_NBR').Clear;
    end;

    if DM_PAYROLL.PR['APPROVED_BY_MANAGEMENT'] <> TMP_PR_LOCAL['APPROVED_BY_MANAGEMENT'] then
    begin
      if TMP_PR_LOCAL['APPROVED_BY_MANAGEMENT'] = 'Y' then
        DM_PAYROLL.PR['APPROVED_BY_MANAGEMENT_NBR'] := Context.UserAccount.InternalNbr
      else
        DM_PAYROLL.PR.FieldByName('APPROVED_BY_MANAGEMENT_NBR').Clear;
    end;

    DM_PAYROLL.PR.FieldByName('APPROVED_BY_TAX').Value := TMP_PR_LOCAL.FieldByName('APPROVED_BY_TAX').Value;
    DM_PAYROLL.PR.FieldByName('APPROVED_BY_FINANCE').Value := TMP_PR_LOCAL.FieldByName('APPROVED_BY_FINANCE').Value;
    DM_PAYROLL.PR.FieldByName('APPROVED_BY_MANAGEMENT').Value := TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').Value;
    DM_PAYROLL.PR.Post;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
//Retry logic if the same record changes faster than 1 sec in TevIBSQL.DoExecQuery
//Sleep(1000);
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_CORRECTIONS_QUEUE.butnSubmitClick(Sender: TObject);
begin
  inherited;
  PostChanges(True);
  TMP_PR_LOCAL.Edit;
  TMP_PR_LOCAL.FieldByName('STATUS').Value := PAYROLL_STATUS_COMPLETED;
  TMP_PR_LOCAL.Post;
end;

procedure TEDIT_CORRECTIONS_QUEUE.dsTMP_PR_LOCALDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if TMP_PR_LOCAL.RecordCount = 0 then
    Exit;

  if (TMP_PR_LOCAL.State = dsEdit) and not FRefreshing then
  begin

    PostChanges(False);
    if ((TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_HOLD)
         or (TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_BACKDATED))
        and (TMP_PR_LOCAL.FieldByName('APPROVED_BY_TAX').Value = PR_APPROVED_YES)
        and (TMP_PR_LOCAL.FieldByName('APPROVED_BY_FINANCE').Value = PR_APPROVED_YES)
        and (TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').Value = PR_APPROVED_YES) then
    begin
      butnSubmit.Enabled := True;
    end
    else if (TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_REQUIRED_MGR_APPROVAL)
            and (TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').Value = PR_APPROVED_YES) then
    begin
      butnSubmit.Enabled := True;
    end
    else
      butnSubmit.Enabled := False;
  end;
end;

procedure TEDIT_CORRECTIONS_QUEUE.butnPayrollBatchClick(Sender: TObject);
begin
  inherited;
  PostChanges(False);
  DM_PAYROLL.PR.DataRequired('CO_NBR = ' + TMP_PR_LOCAL.FieldByName('CO_NBR').AsString);
  DM_PAYROLL.PR.SetOpenCondition('CO_NBR = ' + TMP_PR_LOCAL.FieldByName('CO_NBR').AsString);
  DM_PAYROLL.PR.Locate('PR_NBR', TMP_PR_LOCAL.FieldByName('PR_NBR').Value, []);
  (Context.GetScreenPackage(IevPayrollScreens) as IevPayrollScreens).OpenPayroll;
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Payroll - Batch' + #0)), 0);
end;

procedure TEDIT_CORRECTIONS_QUEUE.butnSBPayrollClick(Sender: TObject);
begin
  inherited;
  PostChanges(False);
  DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([ctx_DataAccess.ClientID, TMP_PR_LOCAL.FieldByName('CO_NBR').Value]), []);
  DM_COMPANY.CO.DataRequired('CO_NBR = ' + TMP_PR_LOCAL.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO.SetOpenCondition('CO_NBR = ' + TMP_PR_LOCAL.FieldByName('CO_NBR').AsString);
  DM_PAYROLL.PR.DataRequired('CO_NBR = ' + TMP_PR_LOCAL.FieldByName('CO_NBR').AsString);
  DM_PAYROLL.PR.SetOpenCondition('CO_NBR = ' + TMP_PR_LOCAL.FieldByName('CO_NBR').AsString);
  DM_PAYROLL.PR.Locate('PR_NBR', TMP_PR_LOCAL.FieldByName('PR_NBR').Value, []);
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Aux Payroll - Payroll' + #0)), 0);
end;

procedure TEDIT_CORRECTIONS_QUEUE.FilterTMP_PR_LOCAL;
begin
  case rgrpShow.ItemIndex of
  0:
  begin
    TMP_PR_LOCAL.Filtered := False;
    TMP_PR_LOCAL.Filter := '';
    TMP_PR_LOCAL.Filter := '( STATUS='''+ PAYROLL_STATUS_REQUIRED_MGR_APPROVAL +''' AND APPROVED_BY_MANAGEMENT <> '''+PR_APPROVED_YES+''')'
    + 'OR ( STATUS in ('''+ PAYROLL_STATUS_BACKDATED +''',''' + PAYROLL_STATUS_HOLD + ''') AND ( ( APPROVED_BY_TAX <> '''+PR_APPROVED_YES+''' ) '
    + ' OR ( APPROVED_BY_FINANCE <> '''+PR_APPROVED_YES+''' ) OR ( APPROVED_BY_MANAGEMENT <> '''+PR_APPROVED_YES+''' )))';

    TMP_PR_LOCAL.Filtered := True;
    dbgrPayrolls.ReadOnly := False;
  end;
  1:
  begin
    TMP_PR_LOCAL.Filtered := False;
    TMP_PR_LOCAL.Filter := '';
    TMP_PR_LOCAL.Filter := 'STATUS=''' + PAYROLL_STATUS_HOLD + ''' or STATUS=''' + PAYROLL_STATUS_BACKDATED
                            + ''' or STATUS=''' + PAYROLL_STATUS_REQUIRED_MGR_APPROVAL + '''';
    TMP_PR_LOCAL.Filtered := True;
    dbgrPayrolls.ReadOnly := False;
  end;
  2:
  begin
    TMP_PR_LOCAL.Filtered := False;
    TMP_PR_LOCAL.Filter := '';
    dbgrPayrolls.ReadOnly := True;    
  end;
  end;
  butnSBPayroll.Enabled := TMP_PR_LOCAL.RecordCount <> 0;
  butnPayrollBatch.Enabled := TMP_PR_LOCAL.RecordCount <> 0;

end;

procedure TEDIT_CORRECTIONS_QUEUE.dbgrPayrollsCalcCellColors(
  Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  inherited;

  if not Highlight then
  begin
    if TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_COMPLETED then
    begin
        AFont.Color := clGreen;
    end
    else
      AFont.Color := clBlack;
  end;

  if not TMP_PR_LOCAL.FieldByName('STATUS').IsNull then
  begin
    if TMP_PR_LOCAL.FieldByName('STATUS').AsString[1] = PAYROLL_STATUS_REQUIRED_MGR_APPROVAL then
      ABrush.Color := clYellow;
  end;
end;

procedure TEDIT_CORRECTIONS_QUEUE.rgrpShowClick(Sender: TObject);
begin
  inherited;
  FilterTMP_PR_LOCAL;
end;

procedure TEDIT_CORRECTIONS_QUEUE.dbgrPayrollsDblClick(Sender: TObject);
var
  CorrectionRun: TCorrectionRun;
begin
  inherited;
  if TMP_PR_LOCAL.RecordCount = 0 then
    Exit;
  PostChanges(False);
  DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([ctx_DataAccess.ClientID, TMP_PR_LOCAL.FieldByName('CO_NBR').Value]), []);
  CorrectionRun := TCorrectionRun.Create(Nil);
  try
    CorrectionRun.ShowModal;
  finally
    CorrectionRun.Free;
  end;
end;

procedure TEDIT_CORRECTIONS_QUEUE.TMP_PR_LOCALAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  //Issue#67515
  if not FRefreshing then
  begin
    if FTax then TMP_PR_LOCAL.FieldByName('APPROVED_BY_TAX').readonly :=  TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_REQUIRED_MGR_APPROVAL;
    if FFinance then TMP_PR_LOCAL.FieldByName('APPROVED_BY_FINANCE').readonly  :=  TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_REQUIRED_MGR_APPROVAL;

    if ((TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_HOLD)
         or (TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_BACKDATED))
        and (TMP_PR_LOCAL.FieldByName('APPROVED_BY_TAX').Value = PR_APPROVED_YES)
        and (TMP_PR_LOCAL.FieldByName('APPROVED_BY_FINANCE').Value = PR_APPROVED_YES)
        and (TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').Value = PR_APPROVED_YES) then
    begin
      butnSubmit.Enabled := True;
    end
    else if (TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_REQUIRED_MGR_APPROVAL)
            and (TMP_PR_LOCAL.FieldByName('APPROVED_BY_MANAGEMENT').Value = PR_APPROVED_YES) then
    begin
      butnSubmit.Enabled := True;
    end
    else
      butnSubmit.Enabled := False;
  end;
end;

initialization
  RegisterClass(TEDIT_CORRECTIONS_QUEUE);

end.
