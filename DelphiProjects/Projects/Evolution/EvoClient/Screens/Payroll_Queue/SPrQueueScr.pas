// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPrQueueScr;

interface

uses
  SysUtils, SPackageEntry, ComCtrls,  StdCtrls,
  Controls, Buttons, Classes, ExtCtrls, EvTypes, EvUtils, SDataStructure,
  ActnList, Menus, ImgList, ToolWin, Graphics, ISBasicClasses, EvCommonInterfaces,
  EvMainboard, EvExceptions, EvUIComponents, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TPrQueueFrm = class(TFramePackageTmpl, IevPayrollQueueScreens)
  protected
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function ActivatePackage(WorkPlace: TWinControl): Boolean; override;
    function  PackageBitmap: TBitmap; override;
  end;

implementation

{$R *.DFM}

var
  PrQueueFrm: TPrQueueFrm;


function GetPayrollQueueScreens: IevPayrollQueueScreens;
begin
  if not Assigned(PrQueueFrm) then
    PrQueueFrm := TPrQueueFrm.Create(nil);
  Result := PrQueueFrm;
end;


{ TSystemFrm }

function TPrQueueFrm.ActivatePackage(WorkPlace: TWinControl): Boolean;
begin
  Result := inherited ActivatePackage(WorkPlace);
  if Result then
  begin
    DM_TEMPORARY.TMP_CL.Activate;
    if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
      raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);
  end;
end;

function TPrQueueFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TPrQueueFrm.PackageCaption: string;
begin
  Result := 'Operatio&ns';
end;

function TPrQueueFrm.PackageSortPosition: string;
begin
  Result := '120';
end;

procedure TPrQueueFrm.UserPackageStructure;
begin
  AddStructure('Process Payrolls|010', 'TEDIT_PAYROLL_QUEUE');
  AddStructure('Approve Payrolls|020', 'TEDIT_CORRECTIONS_QUEUE');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetPayrollQueueScreens, IevPayrollQueueScreens, 'Screens Payroll Queue');

finalization
  FreeAndNil(PrQueueFrm);

end.
