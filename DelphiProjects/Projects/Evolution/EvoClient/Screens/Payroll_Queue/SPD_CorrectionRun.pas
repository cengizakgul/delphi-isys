// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_CorrectionRun;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DBCtrls,  SDataStructure, Db, Wwdatsrc,
  Buttons, Mask, wwdbedit, Wwdbspin, wwdblook, SPD_MiniNavigationFrame,
  Grids, Wwdbigrd, Wwdbgrid, Variants, SDDClasses, EvContext,
  ISBasicClasses, SDataDicttemp, SDataDictclient, EvExceptions, ComCtrls, EvUIUtils, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton, isUIwwDBLookupCombo,
  isUIDBMemo, EvDataset, EvCommonInterfaces;

type
  TCorrectionRun = class(TForm)
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    bbtnApply: TevBitBtn;
    bbtnCancel: TevBitBtn;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBText2: TevDBText;
    evDBText3: TevDBText;
    dsCompany: TevDataSource;
    dsPayroll: TevDataSource;
    dsBatch: TevDataSource;
    DM_PAYROLL: TDM_PAYROLL;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evDBText1: TevDBText;
    evDBText4: TevDBText;
    evLabel5: TevLabel;
    DM_TEMPORARY: TDM_TEMPORARY;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evDBText7: TevDBText;
    evDBText8: TevDBText;
    DBRadioGroup2: TevDBRadioGroup;
    DBRadioGroup4: TevDBRadioGroup;
    DBRadioGroup6: TevDBRadioGroup;
    DBRadioGroup3: TevDBRadioGroup;
    DBRadioGroup7: TevDBRadioGroup;
    evDBRadioGroup2: TevDBRadioGroup;
    evLabel9: TevLabel;
    evLabel10: TevLabel;
    lablCRNotes: TevLabel;
    memoCRNotes: TEvDBMemo;
    evLabel11: TevLabel;
    EvDBMemo2: TEvDBMemo;
    evDBRadioGroup3: TevDBRadioGroup;
    evDBRadioGroup4: TevDBRadioGroup;
    grbxCombineRuns: TevGroupBox;
    evLabel12: TevLabel;
    evLabel13: TevLabel;
    evDBSpinEdit2: TevDBSpinEdit;
    evDBSpinEdit3: TevDBSpinEdit;
    evDBRadioGroup5: TevDBRadioGroup;
    dsPRReports: TevDataSource;
    evLabel14: TevLabel;
    evLabel15: TevLabel;
    evLabel16: TevLabel;
    evLabel17: TevLabel;
    evLabel18: TevLabel;
    evLabel19: TevLabel;
    bbtnPrint: TevBitBtn;
    evLabel20: TevLabel;
    evLabel6: TevLabel;
    evLabel21: TevLabel;
    dsServices: TevDataSource;
    evdsPrServices: TevDataSource;
    PageControl1: TPageControl;
    tsBilling: TTabSheet;
    tsReports: TTabSheet;
    gbServices: TGroupBox;
    evDBGridServices: TevDBCheckGrid;
    grbxReports: TevGroupBox;
    lablE_D_Code: TevLabel;
    grReports: TevDBGrid;
    MiniNavigationFrame: TMiniNavigationFrame;
    wwlcReport: TevDBLookupCombo;
    procedure FormCreate(Sender: TObject);
    procedure evDBRadioGroup4Change(Sender: TObject);
    procedure DBRadioGroup2Change(Sender: TObject);
    procedure evDBRadioGroup5Change(Sender: TObject);
    procedure bbtnCancelClick(Sender: TObject);
    procedure bbtnApplyClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bbtnPrintClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBRadioGroup6Change(Sender: TObject);
    procedure evDBGridServicesMultiSelectAllRecords(Grid: TwwDBGrid;
      Selecting: Boolean; var Accept: Boolean);
    procedure evDBGridServicesMultiSelectReverseRecords(Grid: TwwDBGrid;
      var Accept: Boolean);
  private
    { Private declarations }
     procedure LoadBillingServices;
     procedure SaveBillingServices;
     procedure MarkChanged;
  public
    { Public declarations }
  end;

implementation

uses
  EvUtils, EvTypes, EvConsts, SReportSettings;

{$R *.DFM}

procedure TCorrectionRun.FormCreate(Sender: TObject);
var
  I: Integer;
  Q: IevQuery;
begin
  DM_COMPANY.CO.DataRequired('CO_NBR = ' + DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_SERVICES.DataRequired('CO_NBR = ' + DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_REPORTS.DataRequired('CO_NBR = ' + DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString);
  DM_PAYROLL.PR_BATCH.DataRequired('PR_NBR = ' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
  DM_PAYROLL.PR_BATCH.First;
  DM_PAYROLL.PR_REPORTS.DataRequired('PR_NBR = ' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
  DM_PAYROLL.PR_SERVICES.DataRequired('PR_NBR = ' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
  MiniNavigationFrame.DataSource := dsPRReports;
  MiniNavigationFrame.InsertFocusControl := wwlcReport;

  if ctx_DataAccess.HISTORY_VIEW.Active then
    ctx_DataAccess.HISTORY_VIEW.Close;

  DM_SERVICE_BUREAU.SB_USER.DataRequired('ALL');

  Q := TevQuery.Create(
  '  select h.USER_ID'+
  '  from PR x'+
  '  join EV_TABLE t on t.NAME=''PR'''+
  '  join EV_TABLE_CHANGE c on c.EV_TABLE_NBR=t.NBR and c.RECORD_NBR=x.PR_NBR and CHANGE_TYPE=''I'''+
  '  join EV_TRANSACTION h on h.NBR=c.EV_TRANSACTION_NBR'+
  '  where x.PR_NBR='+DM_PAYROLL.PR.FieldByName('PR_NBR').AsString
  );

  evLabel3.Caption := ConvertNull(DM_SERVICE_BUREAU.SB_USER.Lookup('SB_USER_NBR', Q.Result['USER_ID'], 'FIRST_NAME'), '')
              + ' ' + ConvertNull(DM_SERVICE_BUREAU.SB_USER.Lookup('SB_USER_NBR', Q.Result['USER_ID'], 'LAST_NAME'), '');

  if (DM_PAYROLL.PR['APPROVED_BY_TAX'] <> 'Y') or DM_PAYROLL.PR.FieldByName('APPROVED_BY_TAX_NBR').IsNull then
    evLabel17.Caption := ''
  else
  begin
    Q := TevQuery.Create('select FIRST_NAME, LAST_NAME from SB_USER where SB_USER_NBR='+DM_PAYROLL.PR.FieldByName('APPROVED_BY_TAX_NBR').AsString+' and {AsOfNow<SB_USER>}');
    evLabel17.Caption := Q.Result.FieldByName('FIRST_NAME').AsString + ' ' + Q.Result.FieldByName('LAST_NAME').AsString;
  end;

  if (DM_PAYROLL.PR['APPROVED_BY_FINANCE'] <> 'Y') or DM_PAYROLL.PR.FieldByName('APPROVED_BY_FINANCE_NBR').IsNull then
    evLabel18.Caption := ''
  else
  begin
    Q := TevQuery.Create('select FIRST_NAME, LAST_NAME from SB_USER where SB_USER_NBR='+DM_PAYROLL.PR.FieldByName('APPROVED_BY_FINANCE_NBR').AsString+' and {AsOfNow<SB_USER>}');
    evLabel18.Caption := Q.Result.FieldByName('FIRST_NAME').AsString + ' ' + Q.Result.FieldByName('LAST_NAME').AsString;
  end;

  if (DM_PAYROLL.PR['APPROVED_BY_MANAGEMENT'] <> 'Y') or DM_PAYROLL.PR.FieldByName('APPROVED_BY_MANAGEMENT_NBR').IsNull then
    evLabel19.Caption := ''
  else
  begin
    Q := TevQuery.Create('select FIRST_NAME, LAST_NAME from SB_USER where SB_USER_NBR='+DM_PAYROLL.PR.FieldByName('APPROVED_BY_MANAGEMENT_NBR').AsString+' and {AsOfNow<SB_USER>}');
    evLabel19.Caption := Q.Result.FieldByName('FIRST_NAME').AsString + ' ' + Q.Result.FieldByName('LAST_NAME').AsString;
  end;

  evLabel20.Caption := ConvertNull(DM_SERVICE_BUREAU.SB_USER.Lookup('SB_USER_NBR', DM_COMPANY.CO.FieldByName('CUSTOMER_SERVICE_SB_USER_NBR').Value, 'FIRST_NAME'), '')
    + ' ' + ConvertNull(DM_SERVICE_BUREAU.SB_USER.Lookup('SB_USER_NBR', DM_COMPANY.CO.FieldByName('CUSTOMER_SERVICE_SB_USER_NBR').Value, 'LAST_NAME'), '');

  if (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_HOLD)
  and (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_REQUIRED_MGR_APPROVAL) then
  begin
    for I := 0 to ComponentCount - 1 do
    if (Components[I] is TControl) and (Components[I].Name <> 'lablCRNotes') and (Components[I].Name <> 'memoCRNotes')
    and (Components[I].Name <> 'bbtnPrint') and (Components[I].Name <> 'bbtnApply') and (Components[I].Name <> 'bbtnCancel')
    and (Components[I].Name <> 'evPanel1') and (Components[I].Name <> 'evPanel2') then
      TControl(Components[I]).Enabled := False;
    ctx_DataAccess.UnlockPR;
  end;
  LoadBillingServices;
end;

procedure TCorrectionRun.evDBRadioGroup4Change(Sender: TObject);
begin
  grbxCombineRuns.Visible := evDBRadioGroup4.ItemIndex = 0;
end;

procedure TCorrectionRun.DBRadioGroup2Change(Sender: TObject);
begin
  if csLoading in TComponent(Sender).ComponentState then
    Exit; 
  if DBRadioGroup2.ItemIndex = 0 then
  if EvMessage('You are blocking all ACH transactions from going through. Are you sure you want to do this?', mtWarning, [mbYes, mbNo], mbNo) = mrNo then
    DBRadioGroup2.ItemIndex := 1;

  Assert(DM_COMPANY.CO.CO_NBR.AsInteger=DM_PAYROLL.PR.CO_NBR.AsInteger);

  if DBRadioGroup2.Value = 'Y' then
  begin
    DM_PAYROLL.PR.TRUST_IMPOUND.AsString := PAYMENT_TYPE_NONE;
    DM_PAYROLL.PR.TAX_IMPOUND.AsString := PAYMENT_TYPE_NONE;
    DM_PAYROLL.PR.DD_IMPOUND.AsString := PAYMENT_TYPE_NONE;
    DM_PAYROLL.PR.BILLING_IMPOUND.AsString := PAYMENT_TYPE_NONE;
    DM_PAYROLL.PR.WC_IMPOUND.AsString := PAYMENT_TYPE_NONE;
  end else
  begin
    DM_PAYROLL.PR.TRUST_IMPOUND.AsString :=  DM_COMPANY.CO.TRUST_IMPOUND.Value;
    DM_PAYROLL.PR.TAX_IMPOUND.AsString := DM_COMPANY.CO.TAX_IMPOUND.Value;
    DM_PAYROLL.PR.DD_IMPOUND.AsString := DM_COMPANY.CO.DD_IMPOUND.Value;
    DM_PAYROLL.PR.BILLING_IMPOUND.AsString := DM_COMPANY.CO.BILLING_IMPOUND.Value;
    DM_PAYROLL.PR.WC_IMPOUND.AsString := DM_COMPANY.CO.WC_IMPOUND.Value;
  end;

end;

procedure TCorrectionRun.evDBRadioGroup5Change(Sender: TObject);
begin
  grbxReports.Visible := evDBRadioGroup5.ItemIndex = 1;
  tsReports.TabVisible := evDBRadioGroup5.ItemIndex = 1;
  PageControl1.Visible :=  tsReports.TabVisible or tsBilling.TabVisible; 
end;

procedure TCorrectionRun.bbtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TCorrectionRun.bbtnApplyClick(Sender: TObject);
var
  AllOK: Boolean;
begin
  SaveBillingServices;

  if DM_PAYROLL.PR.State = dsEdit then
    DM_PAYROLL.PR.Post;
  if DM_PAYROLL.PR_BATCH.State = dsEdit then
    DM_PAYROLL.PR_BATCH.Post;
  if DM_PAYROLL.PR_REPORTS.State in [dsInsert, dsEdit] then
    DM_PAYROLL.PR_REPORTS.Post;
  if (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_HOLD)
  or (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_REQUIRED_MGR_APPROVAL) then
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, DM_PAYROLL.PR_REPORTS,DM_PAYROLL.PR_SERVICES])
  else
  begin
    AllOK := False;
    try
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
      AllOK := True;
    finally
      ctx_DataAccess.LockPR(AllOK);
    end;
  end;
end;

procedure TCorrectionRun.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if ModalResult <> mrOK then
  begin
    DM_PAYROLL.PR.Cancel;
    DM_PAYROLL.PR_BATCH.Cancel;
    DM_PAYROLL.PR_REPORTS.Cancel;
    DM_PAYROLL.PR_REPORTS.CancelUpdates;
    if (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_HOLD)
    and (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_REQUIRED_MGR_APPROVAL) then
      ctx_DataAccess.LockPR(False);
  end;
end;

procedure TCorrectionRun.bbtnPrintClick(Sender: TObject);
var
  ReportParams: TrwReportParams;
begin
  if DM_PAYROLL.PR.State = dsEdit then
    DM_PAYROLL.PR.Post;
  if DM_PAYROLL.PR_BATCH.State = dsEdit then
    DM_PAYROLL.PR_BATCH.Post;
  if DM_PAYROLL.PR_REPORTS.State in [dsInsert, dsEdit] then
    DM_PAYROLL.PR_REPORTS.Post;
  if (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_HOLD)
  or (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_REQUIRED_MGR_APPROVAL) then
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, DM_PAYROLL.PR_REPORTS])
  else
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
    
  ReportParams := TrwReportParams.Create;
  try
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('ALL');
    if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'Correction Run Form', [loCaseInsensitive]) then
      raise EMissingReport.CreateHelp('"Correction Run Form" report does not exist in the database!', IDH_MissingReport);

    ReportParams.Add('Clients', varArrayOf([ctx_DataAccess.ClientID]));
    ReportParams.Add('Payrolls',  varArrayOf([DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger]));

    ctx_RWLocalEngine.StartGroup;
    try
      ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS['SY_REPORT_WRITER_REPORTS_NBR'],
        CH_DATABASE_SYSTEM, ReportParams);
    finally
      ctx_RWLocalEngine.EndGroup(rdtPrinter);
    end;
  finally
    ReportParams.Free;
  end;
end;

procedure TCorrectionRun.FormShow(Sender: TObject);
begin
  if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_CL_CORRECTION then
    Caption := 'Client Correction Run Form'
  else
    Caption := 'Service Bureau Correction Run Form';
end;

procedure TCorrectionRun.DBRadioGroup6Change(Sender: TObject);
begin
  gbServices.Visible := DBRadioGroup6.Value = GROUP_BOX_CONDITIONAL; //DM_CLIENT.PR.EXCLUDE_BILLING.AsString
  tsBilling.TabVisible := DBRadioGroup6.Value = GROUP_BOX_CONDITIONAL;
  PageControl1.Visible :=  tsReports.TabVisible or tsBilling.TabVisible;
end;

procedure TCorrectionRun.evDBGridServicesMultiSelectAllRecords(
  Grid: TwwDBGrid; Selecting: Boolean; var Accept: Boolean);
begin
  inherited;
  with DM_PAYROLL do
  if (PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED) or
     (PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED) then
  //if (PR.FieldByName('STATUS').AsString <> 'W') then
     Accept := False
  else
    MarkChanged;
end;

procedure TCorrectionRun.evDBGridServicesMultiSelectReverseRecords(
  Grid: TwwDBGrid; var Accept: Boolean);
begin
  inherited;
    with DM_PAYROLL do
  if (PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED) or
     (PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED) then
  //if (PR.FieldByName('STATUS').AsString <> 'W') then
     Accept := False
  else
    MarkChanged;
end;

procedure TCorrectionRun.MarkChanged;
begin
  DM_PAYROLL.PR_SERVICES.Edit;
end;

procedure TCorrectionRun.LoadBillingServices;
var
  bm:TBookMark;
  cd:string;
begin
   if  (DM_PAYROLL.PR.FieldByName('PR_NBR').IsNull) then exit;
   cd := DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString;
   DM_COMPANY.CO_SERVICES.Filter := '(EFFECTIVE_START_DATE is null or EFFECTIVE_START_DATE <= '''+cd+''' ) and (EFFECTIVE_END_DATE is null or EFFECTIVE_END_DATE>='''+cd +''')';
   DM_COMPANY.CO_SERVICES.Filtered := true;
   if DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').AsString <> 'C' then exit;
   DM_COMPANY.CO_SERVICES.DisableControls;
   try
     with DM_COMPANY.CO_SERVICES.ShadowDataSet do
     begin
       bm := GetBookmark;
       evDBGridServices.SelectedList.Clear;
       First;
       while not Eof do
       begin
         if DM_PAYROLL.PR_SERVICES.ShadowDataSet.Locate(
                     'PR_NBR;CO_SERVICES_NBR',
                     VarArrayOf([DM_PAYROLL.PR.FieldByName('PR_NBR').Value,FieldByName('CO_SERVICES_NBR').Value]),[]) then
             if DM_PAYROLL.PR_SERVICES['EXCLUDE'] = 'Y' then
                evDBGridServices.SelectedList.Add(GetBookMark);
         Next;
       end;
     GotoBookmark(bm);
     end;
   finally
     DM_COMPANY.CO_SERVICES.FreeBookmark(bm);
     DM_COMPANY.CO_SERVICES.EnableControls;
   end;
end;

procedure TCorrectionRun.SaveBillingServices;
var
  bm:TBookMark;
begin
DM_COMPANY.CO_SERVICES.DisableControls;
   try
     with DM_COMPANY.CO_SERVICES do
     begin
       bm := GetBookmark;
       First;
       while not Eof do
       begin

         if evDBGridServices.IsSelectedRecord then
         begin
             if not  DM_PAYROLL.PR_SERVICES.Locate('PR_NBR;CO_SERVICES_NBR',VarArrayOf([DM_PAYROLL.PR.FieldByName('PR_NBR').Value,FieldByName('CO_SERVICES_NBR').Value]),[]) then
             begin
                 DM_PAYROLL.PR_SERVICES.Append;
                 DM_PAYROLL.PR_SERVICES['CO_SERVICES_NBR'] := FieldByName('CO_SERVICES_NBR').Value;
                 DM_PAYROLL.PR_SERVICES['EXCLUDE'] := 'Y';
                 DM_PAYROLL.PR_SERVICES.Post;
             end else if DM_PAYROLL.PR_SERVICES['EXCLUDE'] <> 'Y' then
             begin
                 DM_PAYROLL.PR_SERVICES.Edit;
                 DM_PAYROLL.PR_SERVICES['EXCLUDE'] := 'Y';
                 DM_PAYROLL.PR_SERVICES.Post;
             end;
         end else
         begin
            if DM_PAYROLL.PR_SERVICES.Locate('PR_NBR;CO_SERVICES_NBR',VarArrayOf([DM_PAYROLL.PR.FieldByName('PR_NBR').Value,FieldByName('CO_SERVICES_NBR').Value]),[]) then
            begin
                DM_PAYROLL.PR_SERVICES.Delete;
            end;
         end;
         Next;
       end;
     GotoBookmark(bm);
     end;
   finally
     DM_COMPANY.CO_SERVICES.FreeBookmark(bm);
     DM_COMPANY.CO_SERVICES.EnableControls;
   end;
end;


end.
