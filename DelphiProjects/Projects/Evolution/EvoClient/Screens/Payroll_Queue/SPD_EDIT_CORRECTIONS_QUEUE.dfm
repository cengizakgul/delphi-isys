inherited EDIT_CORRECTIONS_QUEUE: TEDIT_CORRECTIONS_QUEUE
  object dbgrPayrolls: TevDBGrid [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 180
    DisableThemesInTitle = False
    ControlInfoInDataSet = True
    Selected.Strings = (
      'CLIENT_NUMBER'#9'15'#9'Client Number'
      'COMPANY_NUMBER'#9'15'#9'Company Number'
      'COMPANY_NAME'#9'40'#9'Company Name'
      'CHECK_DATE'#9'10'#9'Check Date'
      'RUN_NUMBER'#9'6'#9'Run #'
      'PAYROLL_TYPE'#9'4'#9'Type'
      'APPROVED_BY_TAX'#9'4'#9'Tax'#9'F'
      'APPROVED_BY_FINANCE'#9'4'#9'Fin'#9'F'
      'APPROVED_BY_MANAGEMENT'#9'4'#9'Mgmt'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_CORRECTIONS_QUEUE\dbgrPayrolls'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alClient
    DataSource = dsTMP_PR_LOCAL
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnCalcCellColors = dbgrPayrollsCalcCellColors
    OnDblClick = dbgrPayrollsDblClick
    PaintOptions.AlternatingRowColor = 14544093
    PaintOptions.ActiveRecordColor = clBlack
    DefaultSort = '-'
    NoFire = False
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 180
    Width = 435
    Height = 86
    Align = alBottom
    TabOrder = 1
    object lablApprovalRights: TevLabel
      Left = 176
      Top = 10
      Width = 55
      Height = 13
      Caption = 'You have'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object rgrpShow: TevRadioGroup
      Left = 8
      Top = 6
      Width = 161
      Height = 72
      Caption = 'Show Correction Runs'
      ItemIndex = 0
      Items.Strings = (
        'Pending for Approval'
        'Unprocessed'
        'All')
      TabOrder = 0
      OnClick = rgrpShowClick
    end
    object butnRefresh: TevBitBtn
      Left = 176
      Top = 30
      Width = 113
      Height = 33
      Caption = 'Refresh Queue'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = butnRefreshClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD84666664
        448DDDDDDFFFFFFFFDDD8DD84EEEEE487748DDDDF88888888FDD4884EEEEE487
        7748FDDF8888888DD8FD444EEEEE484777748FF8888888DDD8FD4EEEEEE48D44
        4444888888888DDDDD8F4EEEEE48DDDDDDDD88888888D888888D4EEEE48DDDDD
        DDDD8888888DFFFFFFFF4EEEE48D84444444888888FD8888888F44444448D84E
        EEE4888888FDD888888FDDDDDDDDD84EEEE48888888FD888888FDDDDDDDD84EE
        EEE4FFFFFFDD8888888F444444D84EEEEEE4888888D88888888F47777484EEEE
        E4448DDDDD888888888F8477784EEEEE4884D8DDD8888888FD8F847784EEEEE4
        8DD8D8DD8888888FDDDDD84446666648DDDDDD88888888FDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnSBPayroll: TevBitBtn
      Left = 296
      Top = 30
      Width = 121
      Height = 33
      Caption = 'Aux Payroll'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = butnSBPayrollClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDD8F744CCC447F8DDDD8D8888888D8FDDD8F74CCCCC47F
        8DDDD8D8888888D8FDDD8F74CCCCC47F8DDDD8D8888888D8FDDD8F7CC777C47F
        8DDDD8D88DDD88D8FDDD8FFCCFF7CCFF8888D8D88DDD88D8FDDD08FCCFF7CCF8
        00008D88888D888F88880F7773B3777FFFF08DDD8DD8FDDDDDD80F888B333888
        88F08DDD8D888F8888D80F777BB3377777F08D8D8DD88FDDDDD80F888BBB3888
        88F08DFF8DDD8F8888D80F744B3B37F777F08D888D8D8FDDDDD80FF7CBBBB7F8
        88F08DD88DDDDFD888D80FFF7CCC47FFFFF08DDD88888FDDDDD800000CCC4000
        0000888D88888F888888DDDDD8888DDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnPayrollBatch: TevBitBtn
      Left = 424
      Top = 30
      Width = 121
      Height = 33
      Caption = 'PR Batch Screen'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = butnPayrollBatchClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDD0000000000000DDDFFFFFFFFFFFFFDD04444444444
        4444DF88888888888888044FFFFFFFFFFFF4F888888888888888474FF8888888
        88F48D888DDDDDDDDD88474FF777777777F48D88888888888888474FF8888888
        88F48D888DDDDDDDDD88474FF88FFFF777F48D888DD888888888474FFFFFFFF8
        88F48D888888888DDD88474FFFFFFFFFFFF48D88888888888888474444444444
        44448D88888888888888474AAA477777774D8D8DDD8DDDDDDD8D474444444444
        44DD8D888888888888DD477774DDDDDDDDDD8DDDD8DDDDDDDDDD44444DDDDDDD
        DDDD88888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnSubmit: TevBitBtn
      Left = 552
      Top = 30
      Width = 137
      Height = 33
      Caption = 'Submit for Processing'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = butnSubmitClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD20DD
        DDDDDDDDDDDDFDDDDDDD8F744442A2078DDD8DD8888F8FDD8DDD8F74442AAA20
        8DDD8DD888F888FD8DDD8F7442AAAA208DDD8DD88F8888FD8DDD8F742AAAAAA2
        0DDD8DD8F888888F8DDD8F82AA888AA20DDD8DDF88DDD88F8DDDD82A88778AAA
        2088D8F8DDDDD888F888877773B378AA20788DDD88D8DD88FDD887888B3338AA
        A2088D888D888D888FD887777BB3378AA2088DDD8DD88DD88FD887888BBB388A
        AA208DDDDDDD8DD888F887744B3B3778AA208DD88D8D8DDD88F88777CBBBB778
        A2088DDD8DDDDDDD8FD887777CCC477777788DDDD8888DDDDDD888888CCC4888
        88888888D8888D888888DDDDD8888DDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
  end
  object TMP_PR_LOCAL: TevClientDataSet
    ControlType.Strings = (
      'APPROVED_BY_TAX;CheckBox;Y;N'
      'APPROVED_BY_FINANCE;CheckBox;Y;N'
      'APPROVED_BY_MANAGEMENT;CheckBox;Y;N')
    AfterScroll = TMP_PR_LOCALAfterScroll
    Left = 384
    Top = 64
    object TMP_PR_LOCALCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object TMP_PR_LOCALCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object TMP_PR_LOCALPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object TMP_PR_LOCALCLIENT_NUMBER: TStringField
      FieldName = 'CLIENT_NUMBER'
    end
    object TMP_PR_LOCALCOMPANY_NUMBER: TStringField
      FieldName = 'COMPANY_NUMBER'
    end
    object TMP_PR_LOCALCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object TMP_PR_LOCALCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
    object TMP_PR_LOCALRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
    end
    object TMP_PR_LOCALTYPE: TStringField
      FieldName = 'PAYROLL_TYPE'
      Size = 1
    end
    object TMP_PR_LOCALSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object TMP_PR_LOCALAPPROVED_BY_TAX: TStringField
      FieldName = 'APPROVED_BY_TAX'
      Size = 1
    end
    object TMP_PR_LOCALAPPROVED_BY_FINANCE: TStringField
      FieldName = 'APPROVED_BY_FINANCE'
      Size = 1
    end
    object TMP_PR_LOCALAPPROVED_BY_MANAGEMENT: TStringField
      FieldName = 'APPROVED_BY_MANAGEMENT'
      Size = 1
    end
  end
  object dsTMP_PR_LOCAL: TevDataSource
    DataSet = TMP_PR_LOCAL
    OnDataChange = dsTMP_PR_LOCALDataChange
    Left = 292
    Top = 64
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 472
    Top = 72
  end
  object ActionList1: TActionList
    Left = 80
    Top = 40
  end
end
