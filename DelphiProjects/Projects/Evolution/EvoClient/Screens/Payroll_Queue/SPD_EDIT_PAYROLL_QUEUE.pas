// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_PAYROLL_QUEUE;

interface

uses
  SFrameEntry,  Windows, Dialogs, Menus, Db, 
  StdCtrls, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Wwdatsrc, Graphics, Forms, EvTypes, SPD_ProcessException, EvSendMail,
  Buttons, Variants, EvBasicUtils, ISBasicClasses, kbmMemTable, EvContext, EvMainboard, isBasicUtils,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvCommonInterfaces,
  EvExceptions, isBaseClasses, SFieldCodeValues, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, EvDataset;

type
  TEDIT_PAYROLL_QUEUE = class(TFrameEntry)
    TMP_PR_LOCAL: TevClientDataSet;
    TMP_PR_LOCALCLIENT_NUMBER: TStringField;
    TMP_PR_LOCALCOMPANY_NUMBER: TStringField;
    TMP_PR_LOCALCOMPANY_NAME: TStringField;
    TMP_PR_LOCALCHECK_DATE: TDateTimeField;
    TMP_PR_LOCALRUN_NUMBER: TIntegerField;
    TMP_PR_LOCALTYPE: TStringField;
    TMP_PR_LOCALSTATUS: TStringField;
    TMP_PR_LOCALSELECTED: TStringField;
    TMP_PR_LOCALCL_NBR: TIntegerField;
    TMP_PR_LOCALCO_NBR: TIntegerField;
    TMP_PR_LOCALPR_NBR: TIntegerField;
    PopupMenu1: TevPopupMenu;
    SelectAll1: TMenuItem;
    UnselectAll1: TMenuItem;
    PrintDialog1: TPrintDialog;
    TMP_PR_LOCALPRIORITY: TIntegerField;
    MoveUp1: TMenuItem;
    MoveDown1: TMenuItem;
    MoveFirst1: TMenuItem;
    MoveLast1: TMenuItem;
    N1: TMenuItem;
    TMP_PR_LOCALPROCESS_PRIORITY: TIntegerField;
    dsTMP_PR_LOCAL: TevDataSource;
    N2: TMenuItem;
    UnlockSelected1: TMenuItem;
    UnlockTrustAcct1: TMenuItem;
    wwDBGrid1: TevDBGrid;
    evPanel1: TevPanel;
    rgrpStatus: TevRadioGroup;
    butnRefresh: TevBitBtn;
    butnHold: TevBitBtn;
    butnDeleteFromQueue: TevBitBtn;
    butnProcess: TevBitBtn;
    chbxQueue: TevCheckBox;
    chbxBrokerPrint: TevCheckBox;
    TMP_PR_LOCALAUTOPAY_COMPANY: TStringField;
    TMP_PR_LOCALCSR: TStringField;
    TMP_PR_LOCALSTATUS_DATE: TDateTimeField;
    procedure butnProcessClick(Sender: TObject);
    procedure butnDeleteFromQueueClick(Sender: TObject);
    procedure rgrpStatusClick(Sender: TObject);
    procedure butnRefreshClick(Sender: TObject);
    procedure wwDBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure SelectAll1Click(Sender: TObject);
    procedure UnselectAll1Click(Sender: TObject);
    procedure butnHoldClick(Sender: TObject);
    procedure MoveUp1Click(Sender: TObject);
    procedure MoveDown1Click(Sender: TObject);
    procedure MoveFirst1Click(Sender: TObject);
    procedure MoveLast1Click(Sender: TObject);
    procedure UnlockSelected1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  private
    { Private declarations }
    procedure PopulateTMP_PR_LOCAL;
    procedure ChangeSelectedTo(NewFlag: Char);
    procedure TestPayrollLocked;
    procedure FilterPayrolls;
  public
    { Public declarations }
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

var
  EDIT_PAYROLL_QUEUE: TEDIT_PAYROLL_QUEUE;

implementation

uses
  EvConsts, Printers, EvUtils, SDataStructure, SysUtils;

{$R *.DFM}

procedure TEDIT_PAYROLL_QUEUE.butnProcessClick(Sender: TObject);
var
  DoRefresh: Boolean;
  qTask: IevProcessPayrollTask;
  TaskList: IisList;
  Log: string;
  Q: IevQuery;
begin
  inherited;
  if chbxQueue.Checked then
  begin
    TaskList := TisList.Create;

    TMP_PR_LOCAL.DisableControls;
    ctx_StartWait('Preparing payrolls to be sent to the queue...');
    qTask := nil;

    if chbxBrokerPrint.Checked then
    begin
      qTask := mb_TaskQueue.CreateTask(QUEUE_TASK_PROCESS_PAYROLL, True) as IevProcessPayrollTask;
      qTask.PrList := '';
      qTask.Caption := '';
      qTask.Destination := rdtRemotePrinter;
    end;

    try
      TMP_PR_LOCAL.First;
      while not TMP_PR_LOCAL.EOF do
      begin
        ctx_DataAccess.OpenClient(TMP_PR_LOCAL.FieldByName('CL_NBR').AsInteger);
        Q := TevQuery.Create('select STATUS from PR where PR_NBR='+TMP_PR_LOCAL.FieldByName('PR_NBR').AsString+' and {AsOfNow<PR>}');
        
        if (TMP_PR_LOCAL.FieldByName('SELECTED').Value = 'Y') and (Q.Result.FieldByName('STATUS').Value = PAYROLL_STATUS_COMPLETED) then
        begin
          if TMP_PR_LOCAL.FieldByName('CHECK_DATE').AsDateTime < GetBeginYear(Now) then
          if EvMessage('Your check date for CO #' + TMP_PR_LOCAL.FieldByName('COMPANY_NUMBER').AsString + ' is for a prior year. Would you still like to process it?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
          begin
            TMP_PR_LOCAL.Edit;
            TMP_PR_LOCAL.FieldByName('SELECTED').Value := 'N';
            TMP_PR_LOCAL.Post;
            TMP_PR_LOCAL.Next;
            Continue;
          end;

          if not chbxBrokerPrint.Checked then
          begin
            qTask := mb_TaskQueue.CreateTask(QUEUE_TASK_PROCESS_PAYROLL, True) as IevProcessPayrollTask;
            qTask.PrList := '';
            qTask.Caption := '';
            qTask.Destination := rdtPrinter;
          end;

          try
            qTask.PrList := qTask.PrList + TMP_PR_LOCAL.FieldByName('CL_NBR').AsString + ';' + TMP_PR_LOCAL.FieldByName('PR_NBR').AsString + ' ';
            qTask.Caption := qTask.Caption + 'Co:' + Trim(TMP_PR_LOCAL.FieldbyName('COMPANY_NUMBER').AsString)
              + ' Pr:' + TMP_PR_LOCAL.FieldByName('CHECK_DATE').AsString + '-' + TMP_PR_LOCAL.FieldByName('RUN_NUMBER').AsString + '  ';

            TMP_PR_LOCAL.Edit;
            TMP_PR_LOCAL.FieldByName('STATUS').AsString := PAYROLL_STATUS_PROCESSING;
            TMP_PR_LOCAL.FieldByName('SELECTED').Value := 'N';
            TMP_PR_LOCAL.Post;
          finally
            if not chbxBrokerPrint.Checked then
              TaskList.Add(qTask);
          end;
        end
        else
          TMP_PR_LOCAL.Next;
      end;

      if chbxBrokerPrint.Checked and (qTask.PrList <> '') then
        TaskList.Add(qTask);

      ctx_DataAccess.OpenClient(0);
    finally
      ctx_EndWait;
      TMP_PR_LOCAL.EnableControls;
      if chbxBrokerPrint.Checked then
        qTask := nil;
    end;

    ctx_EvolutionGUI.AddTaskQueue(TaskList);
  end

  else
  begin
    TMP_PR_LOCAL.DisableControls;
    ctx_StartWait;
    try
      DoRefresh := False;
      TMP_PR_LOCAL.First;
      while not TMP_PR_LOCAL.EOF do
      begin
        if (TMP_PR_LOCAL.FieldByName('SELECTED').Value = 'Y') and (TMP_PR_LOCAL.FieldByName('STATUS').Value = PAYROLL_STATUS_COMPLETED) then
        begin
          TMP_PR_LOCAL.EnableControls;
          try
            Application.ProcessMessages;
            ctx_DataAccess.OpenClient(TMP_PR_LOCAL.FieldByName('CL_NBR').Value);
            DM_CLIENT.CL.DataRequired('ALL');
            DM_COMPANY.CO.DataRequired('ALL');
            DM_COMPANY.CO.Locate('CO_NBR', TMP_PR_LOCAL.FieldByName('CO_NBR').Value, []);
            with TMP_PR_LOCAL do
              ctx_UpdateWait(FieldByName('COMPANY_NAME').AsString + '(' + FieldByName('COMPANY_NUMBER').AsString + ') ' +
              FieldByName('CHECK_DATE').AsString + '-' + FieldByName('RUN_NUMBER').AsString);
            ctx_PayrollProcessing.ProcessPayroll(TMP_PR_LOCAL.FieldByName('PR_NBR').Value, False, Log, False);
            TMP_PR_LOCAL.Delete;
          finally
            TMP_PR_LOCAL.DisableControls;
          end;
          DM_PAYROLL.PR.Close;
          DM_COMPANY.CO_BILLING_HISTORY.Close;
        end
        else
          TMP_PR_LOCAL.Next;
      end;
      if DoRefresh then
        PopulateTMP_PR_LOCAL;
    finally
      ctx_EndWait;
      TMP_PR_LOCAL.EnableControls;
    end;

    DM_PAYROLL.PR_BATCH.Close;
    DM_PAYROLL.PR_CHECK.Close;
    DM_PAYROLL.PR_CHECK_LINES.Close;
    DM_PAYROLL.PR_CHECK_LINE_LOCALS.Close;
    DM_PAYROLL.PR_CHECK_LOCALS.Close;
    DM_PAYROLL.PR_CHECK_STATES.Close;
    DM_PAYROLL.PR_CHECK_SUI.Close;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.butnDeleteFromQueueClick(Sender: TObject);
var
  MyTransactionManager: TTransactionManager;
begin
  inherited;
  ctx_StartWait;
  MyTransactionManager := TTransactionManager.Create(nil);
  try
    TMP_PR_LOCAL.First;
    while not TMP_PR_LOCAL.EOF do
    begin
      if (TMP_PR_LOCAL.FieldByName('SELECTED').Value = 'Y')
      and (TMP_PR_LOCAL.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PROCESSING)
      and (TMP_PR_LOCAL.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PREPROCESSING) then
      begin
        ctx_DataAccess.OpenClient(TMP_PR_LOCAL.FieldByName('CL_NBR').Value);
        DM_COMPANY.CO.ClientID := ctx_DataAccess.ClientID;
        TestPayrollLocked;
        DM_PAYROLL.PR.DataRequired('CO_NBR=' + TMP_PR_LOCAL.FieldByName('CO_NBR').AsString);
        MyTransactionManager.Initialize([DM_PAYROLL.PR]);
        if DM_PAYROLL.PR.Locate('PR_NBR', TMP_PR_LOCAL.FieldByName('PR_NBR').Value, []) then
        begin
          DM_PAYROLL.PR.Edit;
          DM_PAYROLL.PR.FieldByName('STATUS').AsString := PAYROLL_STATUS_PENDING;
          DM_PAYROLL.PR.Post;
        end;
        MyTransactionManager.ApplyUpdates;
        TMP_PR_LOCAL.Delete;
      end
      else
        TMP_PR_LOCAL.Next;
    end;
  finally
    MyTransactionManager.Free;
    ctx_EndWait;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.PopulateTMP_PR_LOCAL;
var
  I: Integer;
  sUserName: String;
begin
  TMP_PR_LOCAL.DisableControls;
  try
    DM_SERVICE_BUREAU.SB_USER.DataRequired('ALL');

    if DM_TEMPORARY.TMP_PR.Active then
      DM_TEMPORARY.TMP_PR.Close;
    DM_TEMPORARY.TMP_PR.DataRequired('(STATUS=''' + PAYROLL_STATUS_COMPLETED
      + ''' or STATUS=''' + PAYROLL_STATUS_HOLD + ''' or STATUS=''' + PAYROLL_STATUS_PREPROCESSING
      + ''' or STATUS=''' + PAYROLL_STATUS_PROCESSING + ''') ' + GetReadOnlyClintCompanyListFilter);
    if TMP_PR_LOCAL.Active then
      TMP_PR_LOCAL.Close;
    TMP_PR_LOCAL.CreateDataSet;
    SortDSBy( TMP_PR_LOCAL, 'PROCESS_PRIORITY;CHECK_DATE;RUN_NUMBER', 'CHECK_DATE'); //gdy23
   (TMP_PR_LOCAL.FieldByName('STATUS_DATE') as TDateTimeField).DisplayFormat := 'mm/dd/yyyy hh:nn:ss AM/PM';

    with DM_TEMPORARY.TMP_PR do
    begin
      First;
      while not EOF do
      begin
        TMP_PR_LOCAL.Insert;
        TMP_PR_LOCAL.FieldByName('CL_NBR').Value := FieldByName('CL_NBR').Value;
        TMP_PR_LOCAL.FieldByName('CO_NBR').Value := FieldByName('CO_NBR').Value;
        TMP_PR_LOCAL.FieldByName('PR_NBR').Value := FieldByName('PR_NBR').Value;
        TMP_PR_LOCAL.FieldByName('CLIENT_NUMBER').Value := FieldByName('Client_Number_Lookup').Value;
        TMP_PR_LOCAL.FieldByName('COMPANY_NUMBER').Value := FieldByName('Company_Number_Lookup').Value;
        TMP_PR_LOCAL.FieldByName('COMPANY_NAME').Value := FieldByName('Company_Name_Lookup').Value;
        TMP_PR_LOCAL.FieldByName('CHECK_DATE').Value := FieldByName('CHECK_DATE').Value;
        TMP_PR_LOCAL.FieldByName('RUN_NUMBER').Value := FieldByName('RUN_NUMBER').Value;
        TMP_PR_LOCAL.FieldByName('PAYROLL_TYPE').Value := FieldByName('PAYROLL_TYPE').Value;
        TMP_PR_LOCAL.FieldByName('STATUS').Value := FieldByName('STATUS').Value;
        TMP_PR_LOCAL.FieldByName('STATUS_DATE').Value := FieldByName('STATUS_DATE').Value;
        TMP_PR_LOCAL.FieldByName('SELECTED').Value := 'N';
        TMP_PR_LOCAL.FieldByName('PROCESS_PRIORITY').Value := DM_TEMPORARY.TMP_CO.Lookup('CL_NBR;CO_NBR',
          VarArrayOf([FieldByName('CL_NBR').Value, FieldByName('CO_NBR').Value]), 'PROCESS_PRIORITY');
        TMP_PR_LOCAL.FieldByName('AUTOPAY_COMPANY').AsString :=
            ReturnDescription(AutopayCompany_ComboChoices,
             DM_TEMPORARY.TMP_CO.Lookup('CL_NBR;CO_NBR',
               VarArrayOf([FieldByName('CL_NBR').Value, FieldByName('CO_NBR').Value]), 'AUTOPAY_COMPANY'));

        sUserName:='';
        if DM_SERVICE_BUREAU.SB_USER.Locate('SB_USER_NBR',DM_TEMPORARY.TMP_CO.Lookup('CL_NBR;CO_NBR', VarArrayOf([FieldByName('CL_NBR').Value, FieldByName('CO_NBR').Value]), 'CUSTOMER_SERVICE_SB_USER_NBR'),[]) then
           sUserName:= Trim(DM_SERVICE_BUREAU.SB_USER.FieldByName('FIRST_NAME').AsString) + ' ' + Trim(DM_SERVICE_BUREAU.SB_USER.FieldByName('LAST_NAME').AsString);
        TMP_PR_LOCAL.FieldByName('CSR').AsString:= sUserName;

        TMP_PR_LOCAL.Post;

        Next;
      end;
    end;
    TMP_PR_LOCAL.First;
    I := 0;
    while not TMP_PR_LOCAL.EOF do
    begin
      Inc(I);
      TMP_PR_LOCAL.Edit;
      TMP_PR_LOCAL.FieldByName('PRIORITY').Value := I;
      TMP_PR_LOCAL.Post;
      TMP_PR_LOCAL.Next;
    end;
    TMP_PR_LOCAL.IndexFieldNames := 'PRIORITY';
    TMP_PR_LOCAL.First;
  finally
    TMP_PR_LOCAL.EnableControls;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.rgrpStatusClick(Sender: TObject);
begin
  inherited;
  ChangeSelectedTo('N');
  FilterPayrolls;
end;

procedure TEDIT_PAYROLL_QUEUE.butnRefreshClick(Sender: TObject);
begin
  inherited;
  PopulateTMP_PR_LOCAL;
  FilterPayrolls;
end;

procedure TEDIT_PAYROLL_QUEUE.wwDBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  if not TevDBGrid(Sender).DataSource.DataSet.FieldByName('STATUS').IsNull then
  begin
    AFont.Color := clBlack;
    if TevDBGrid(Sender).DataSource.DataSet.FieldByName('STATUS').AsString[1] = PAYROLL_STATUS_HOLD then      ABrush.Color := clYellow;
    if TevDBGrid(Sender).DataSource.DataSet.FieldByName('STATUS').AsString[1] = PAYROLL_STATUS_PROCESSING then      ABrush.Color := clLime;
    if TevDBGrid(Sender).DataSource.DataSet.FieldByName('STATUS').AsString[1] = PAYROLL_STATUS_PREPROCESSING then    begin
      AFont.Color := clWhite;
      ABrush.Color := clPurple;
    end;
  end;

  if Highlight then
  begin
    if ((ABrush.Color = clYellow) or (ABrush.Color = clLime)) then
      AFont.Color := clBlack
    else
      AFont.Color := clWhite;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.ChangeSelectedTo(NewFlag: Char);
begin
  with TMP_PR_LOCAL do
  begin
    DisableControls;
    First;
    while not EOF do
    begin
      if FieldByName('SELECTED').Value <> NewFlag then
      begin
        Edit;
        FieldByName('SELECTED').Value := NewFlag;
        Post;
      end;
      Next;
    end;
    First;
    EnableControls;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.SelectAll1Click(Sender: TObject);
begin
  inherited;
  ChangeSelectedTo('Y');
end;

procedure TEDIT_PAYROLL_QUEUE.UnselectAll1Click(Sender: TObject);
begin
  inherited;
  ChangeSelectedTo('N');
end;

procedure TEDIT_PAYROLL_QUEUE.butnHoldClick(Sender: TObject);
var
  MyTransactionManager: TTransactionManager;
begin
  inherited;
  ctx_StartWait;
  MyTransactionManager := TTransactionManager.Create(nil);
  try
    TMP_PR_LOCAL.First;
    while not TMP_PR_LOCAL.EOF do
    begin
      if (TMP_PR_LOCAL.FieldByName('SELECTED').Value = 'Y')
      and (TMP_PR_LOCAL.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PROCESSING)
      and (TMP_PR_LOCAL.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PREPROCESSING)
      and ((TMP_PR_LOCAL.FieldByName('STATUS').AsString = PAYROLL_STATUS_COMPLETED)
      or (TMP_PR_LOCAL.FieldByName('STATUS').AsString = PAYROLL_STATUS_HOLD)
      and not TypeIsCorrectionRun(TMP_PR_LOCAL.FieldByName('PAYROLL_TYPE').AsString)) then
      begin
        ctx_DataAccess.OpenClient(TMP_PR_LOCAL.FieldByName('CL_NBR').Value);
        DM_COMPANY.CO.ClientID := ctx_DataAccess.ClientID;
        TestPayrollLocked;
        DM_PAYROLL.PR.DataRequired('PR_NBR=' + TMP_PR_LOCAL.FieldByName('PR_NBR').AsString);
        MyTransactionManager.Initialize([DM_PAYROLL.PR]);
        DM_PAYROLL.PR.Edit;
        if DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_COMPLETED then
          DM_PAYROLL.PR.FieldByName('STATUS').AsString := PAYROLL_STATUS_HOLD
        else
        if DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_HOLD then
          DM_PAYROLL.PR.FieldByName('STATUS').AsString := PAYROLL_STATUS_COMPLETED;
        DM_PAYROLL.PR.Post;
        MyTransactionManager.ApplyUpdates;
        TMP_PR_LOCAL.Edit;
        if TMP_PR_LOCAL.FieldByName('STATUS').AsString = PAYROLL_STATUS_COMPLETED then
          TMP_PR_LOCAL.FieldByName('STATUS').AsString := PAYROLL_STATUS_HOLD
        else
        if TMP_PR_LOCAL.FieldByName('STATUS').AsString = PAYROLL_STATUS_HOLD then
          TMP_PR_LOCAL.FieldByName('STATUS').AsString := PAYROLL_STATUS_COMPLETED;
        TMP_PR_LOCAL.FieldByName('SELECTED').AsString := 'N';
        TMP_PR_LOCAL.Post;
        if DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_HOLD, PAYROLL_STATUS_COMPLETED] then
          TMP_PR_LOCAL.Next
        else
          TMP_PR_LOCAL.Delete;
      end
      else
        TMP_PR_LOCAL.Next;
    end;
  finally
    MyTransactionManager.Free;
    ctx_EndWait;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.MoveUp1Click(Sender: TObject);
var
  P1, P2: Integer;
begin
  inherited;
  with TMP_PR_LOCAL do
  begin
    P1 := FieldByName('PRIORITY').AsInteger;
    Prior;
    P2 := FieldByName('PRIORITY').AsInteger;
    Next;
    Edit;
    FieldByName('PRIORITY').AsInteger := 0;
    Post;
    Locate('PRIORITY', P2, []);
    Edit;
    FieldByName('PRIORITY').AsInteger := P1;
    Post;
    Locate('PRIORITY', 0, []);
    Edit;
    FieldByName('PRIORITY').AsInteger := P2;
    Post;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.MoveDown1Click(Sender: TObject);
var
  P1, P2: Integer;
begin
  inherited;
  with TMP_PR_LOCAL do
  begin
    P1 := FieldByName('PRIORITY').AsInteger;
    Next;
    P2 := FieldByName('PRIORITY').AsInteger;
    Prior;
    Edit;
    FieldByName('PRIORITY').AsInteger := 0;
    Post;
    Locate('PRIORITY', P2, []);
    Edit;
    FieldByName('PRIORITY').AsInteger := P1;
    Post;
    Locate('PRIORITY', 0, []);
    Edit;
    FieldByName('PRIORITY').AsInteger := P2;
    Post;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.MoveFirst1Click(Sender: TObject);
begin
  inherited;
  with TMP_PR_LOCAL do
  begin
    DisableControls;
    Edit;
    FieldByName('PRIORITY').AsInteger := 0;
    Post;
    Last;
    while not BOF do
    begin
      Edit;
      FieldByName('PRIORITY').AsInteger := FieldByName('PRIORITY').AsInteger + 1;
      Post;
      Prior;
    end;
    EnableControls;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.MoveLast1Click(Sender: TObject);
var
  P1: Integer;
begin
  inherited;
  with TMP_PR_LOCAL do
  begin
    Edit;
    FieldByName('PRIORITY').AsInteger := 0;
    Post;
    Last;
    P1 := FieldByName('PRIORITY').AsInteger;
    Locate('PRIORITY', 0, []);
    Edit;
    FieldByName('PRIORITY').AsInteger := P1 + 1;
    Post;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck([DM_TEMPORARY.TMP_CL, DM_TEMPORARY.TMP_CO], SupportDataSets, '');
end;

procedure TEDIT_PAYROLL_QUEUE.Activate;
begin
  inherited;
  DM_TEMPORARY.TMP_PR.LookupsEnabled := False;
  DM_TEMPORARY.TMP_PR.LookupsEnabled := True;
  PopulateTMP_PR_LOCAL;
  chbxBrokerPrint.Visible := ctx_DomainInfo.DomainName <> sDefaultDomain;
end;

procedure TEDIT_PAYROLL_QUEUE.TestPayrollLocked;
var
  FlagInfo: IevGlobalFlagInfo;
begin
  FlagInfo := mb_GlobalFlagsManager.GetLockedFlag(GF_CL_PAYROLL_OPERATION,
    TMP_PR_LOCAL.FieldByName('CL_NBR').AsString);
  if Assigned(FlagInfo) then
    raise ECanNotPerformOperation.CreateHelp('A payroll from a company under the same client is currently being processed, cannot process this payroll at this time.', IDH_CanNotPerformOperation);

  FlagInfo := mb_GlobalFlagsManager.GetLockedFlag(GF_CO_PAYROLL_OPERATION,
    TMP_PR_LOCAL.FieldByName('CL_NBR').AsString + '_' + TMP_PR_LOCAL.FieldByName('CO_NBR').AsString);
  if Assigned(FlagInfo) then
    raise ECanNotPerformOperation.CreateHelp('This payroll is locked by ' + FlagInfo.UserName, IDH_CanNotPerformOperation);
end;

procedure TEDIT_PAYROLL_QUEUE.UnlockSelected1Click(Sender: TObject);
begin
  inherited;
  ctx_StartWait;
  DM_PAYROLL.PR.Tag := 1;
  try
    mb_GlobalFlagsManager.ForcedUnlock(GF_CO_PAYROLL_OPERATION,
      TMP_PR_LOCAL.FieldByName('CL_NBR').AsString + '_' + TMP_PR_LOCAL.FieldByName('CO_NBR').AsString);
    mb_GlobalFlagsManager.ForcedUnlock(GF_CL_PAYROLL_OPERATION, TMP_PR_LOCAL.FieldByName('CL_NBR').AsString);

    if (TMP_PR_LOCAL.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSING)
    or (TMP_PR_LOCAL.FieldByName('STATUS').AsString = PAYROLL_STATUS_PREPROCESSING) then
    begin
      ctx_DataAccess.OpenClient(TMP_PR_LOCAL.FieldByName('CL_NBR').Value);
      DM_COMPANY.CO.ClientID := ctx_DataAccess.ClientID;
      DM_PAYROLL.PR.DataRequired('PR_NBR=' + TMP_PR_LOCAL.FieldByName('PR_NBR').AsString);
      with DM_PAYROLL.PR do
      begin
        Edit;
        if FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSING then
          FieldByName('STATUS').AsString := PAYROLL_STATUS_COMPLETED
        else
        if FieldByName('STATUS').AsString = PAYROLL_STATUS_PREPROCESSING then
          FieldByName('STATUS').AsString := PAYROLL_STATUS_PENDING
        else
        if FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED then
        begin
          Cancel;
          TMP_PR_LOCAL.Delete;
          Exit;
        end;
        Post;
      end;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
    end;
    if DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PREPROCESSING then
      TMP_PR_LOCAL.Delete
    else
    begin
      TMP_PR_LOCAL.Edit;
      TMP_PR_LOCAL.FieldByName('SELECTED').AsString := 'N';
      if TMP_PR_LOCAL.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSING then
        TMP_PR_LOCAL.FieldByName('STATUS').AsString := PAYROLL_STATUS_COMPLETED;
      TMP_PR_LOCAL.Post;
    end;
  finally
    DM_PAYROLL.PR.Tag := 0;
    ctx_EndWait;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.PopupMenu1Popup(Sender: TObject);
begin
  inherited;
  if TMP_PR_LOCAL.IndexFieldNames = 'PRIORITY' then
  begin
    MoveUp1.Enabled := True;
    MoveDown1.Enabled := True;
    MoveFirst1.Enabled := True;
    MoveLast1.Enabled := True;
  end
  else
  begin
    MoveUp1.Enabled := False;
    MoveDown1.Enabled := False;
    MoveFirst1.Enabled := False;
    MoveLast1.Enabled := False;
  end;
end;

procedure TEDIT_PAYROLL_QUEUE.FilterPayrolls;
begin
  with TMP_PR_LOCAL do
  case rgrpStatus.ItemIndex of
    0:
    begin
      Filtered := False;
      Filter := '';
    end;
    1:
    begin
      Filter := 'STATUS=''' + PAYROLL_STATUS_COMPLETED + '''';
      Filtered := True;
    end;
    2:
    begin
      Filter := 'STATUS=''' + PAYROLL_STATUS_HOLD + '''';
      Filtered := True;
    end;
  end;
end;

initialization
  RegisterClass(TEDIT_PAYROLL_QUEUE);

end.

