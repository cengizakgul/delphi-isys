inherited EDIT_PAYROLL_QUEUE: TEDIT_PAYROLL_QUEUE
  object wwDBGrid1: TevDBGrid [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 205
    DisableThemesInTitle = False
    ControlInfoInDataSet = True
    Selected.Strings = (
      'SELECTED'#9'8'#9'Selected'#9'F'
      'CLIENT_NUMBER'#9'17'#9'Client Number'
      'COMPANY_NUMBER'#9'17'#9'Company Number'
      'COMPANY_NAME'#9'37'#9'Company Name'
      'CHECK_DATE'#9'10'#9'Check Date'
      'RUN_NUMBER'#9'5'#9'Run#'
      'PAYROLL_TYPE'#9'5'#9'Type'
      'PROCESS_PRIORITY'#9'5'#9'Prty'
      'AUTOPAY_COMPANY'#9'20'#9'Autopay Company'#9'F'
      'CSR'#9'40'#9'Csr'#9'F'
      'STATUS_DATE'#9'18'#9'Submitted Time Stamp'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_PAYROLL_QUEUE\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alClient
    DataSource = dsTMP_PR_LOCAL
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
    PopupMenu = PopupMenu1
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnCalcCellColors = wwDBGrid1CalcCellColors
    PaintOptions.AlternatingRowColor = 14544093
    PaintOptions.ActiveRecordColor = clBlack
    DefaultSort = '-'
    NoFire = False
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 205
    Width = 435
    Height = 61
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object rgrpStatus: TevRadioGroup
      Left = 11
      Top = 12
      Width = 177
      Height = 41
      Caption = 'Show Payrolls'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'All'
        'Ready'
        'Hold')
      TabOrder = 0
      OnClick = rgrpStatusClick
    end
    object butnRefresh: TevBitBtn
      Left = 192
      Top = 20
      Width = 115
      Height = 33
      Caption = 'Refresh Queue'
      TabOrder = 1
      OnClick = butnRefreshClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD84666664
        448DDDDDDFFFFFFFFDDD8DD84EEEEE487748DDDDF88888888FDD4884EEEEE487
        7748FDDF8888888DD8FD444EEEEE484777748FF8888888DDD8FD4EEEEEE48D44
        4444888888888DDDDD8F4EEEEE48DDDDDDDD88888888D888888D4EEEE48DDDDD
        DDDD8888888DFFFFFFFF4EEEE48D84444444888888FD8888888F44444448D84E
        EEE4888888FDD888888FDDDDDDDDD84EEEE48888888FD888888FDDDDDDDD84EE
        EEE4FFFFFFDD8888888F444444D84EEEEEE4888888D88888888F47777484EEEE
        E4448DDDDD888888888F8477784EEEEE4884D8DDD8888888FD8F847784EEEEE4
        8DD8D8DD8888888FDDDDD84446666648DDDDDD88888888FDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnHold: TevBitBtn
      Left = 312
      Top = 20
      Width = 115
      Height = 33
      Caption = 'Put On/Off Hold'
      TabOrder = 2
      OnClick = butnHoldClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD8DDDDDDD
        DDD8DDDDFDDDDDDDDDDDDDDD38DDDDDD8800DDDD8FDDDDDDDD88DDDD338DDDD0
        0004DDDD88FDDDD8888888883B38DD077704FFFF888FDD8DDD8833333BB3807F
        F70488888888F8DDDD883BBBBBBB30FFF8048888888888DDD8883BBBBBBBB0F0
        70008888888888D8D8883BBBBBBB30F0F08D8888888888D8D8DD33333BB3D0F0
        70DD88888888D8D8D8DDDDDD3B3DDD0770DDDDDD888DDD8DD8DDDDDD33DDDD0F
        08DDDDDD88DDDD8D8DDDDDDD3DDDDD0F0DDDDDDD8DDDDD8D8DDDDDDDDDDDDD0F
        0DDDDDDDDDDDDD8D8DDDDDDDDDDDDDD0DDDDDDDDDDDDDDD8DDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnDeleteFromQueue: TevBitBtn
      Left = 432
      Top = 20
      Width = 123
      Height = 33
      Caption = 'Remove Payrolls'
      TabOrder = 3
      OnClick = butnDeleteFromQueueClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD808DDD
        808DDDD888F8F8DDF8FD8F74480F08780F088DD88F888FDF888F8F74407FF080
        FF708DD8D88888F888888F74C407FF0FF70D8DD88D888888888D8F7CC7707FFF
        70DD8DD888D8888888DD8FFCC7780FFF08888DD88DDF88888F88D8FCC780FF7F
        F088D8D88DF8888888F88777780FF707FF088DDDDF888888888F8788807F7080
        7F708D88D88888D8888887777B07077707088DDD8D888DDD888D87888BB03888
        80788D888DD8D888D8D887744B3B377777788DDDDDDDDDDDDDD88777CBBBB778
        88788DD88D8D8DD888D887777CCC477777788DDD8DDDDDDDDDD888888CCC4888
        88888888D8888D888888DDDDD8888DDDDDDDDDDDD8888DDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnProcess: TevBitBtn
      Left = 560
      Top = 20
      Width = 123
      Height = 33
      Caption = 'Process Payrolls'
      Default = True
      TabOrder = 4
      OnClick = butnProcessClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD0DD0DDDDD
        DDDDDDDFDDFDDDDDDDDDDD0E00E0DDDDDDDDDDF8FF8FDDDDDDDDDDEEEEEE8888
        8888DD888888FFFFFFFFD00EEEE007FFFFF8DFF8888FFD88888FDEEE07EEE7FF
        FFF8D888FD888D88888FDEEE00EEE74444F8D888FF888DDDDD8FDD0EEEE07FFF
        FFF8DDF8888FD888888FDDEEEEEE744444F8DD888888DDDDDD8FD03E77E7FFFF
        FFF8DFD8DD8D8888888FDBB33F74444444F8D88DD8DDDDDDDD8F00BB3FFFFFFF
        FFF8FF88D8888888888FBBB0DFF4444444F8888FD88DDDDDDD8FBBB00FFFFFFF
        FFF8888FF8888888888FD0BB3FF4444444F8DF88D88DDDDDDD8FDBBB3FFFFFFF
        FFF8D888D8888888888FDDBD3FFFFFFFFFFDDD8DD8888888888D}
      NumGlyphs = 2
      Margin = 0
    end
    object chbxQueue: TevCheckBox
      Left = 312
      Top = 3
      Width = 97
      Height = 15
      Caption = 'Send to queue'
      Checked = True
      State = cbChecked
      TabOrder = 5
      Visible = False
    end
    object chbxBrokerPrint: TevCheckBox
      Left = 488
      Top = 3
      Width = 201
      Height = 15
      Caption = 'Print Payrolls on Request Broker'
      TabOrder = 6
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 133
  end
  inherited wwdsDetail: TevDataSource
    Left = 166
  end
  object TMP_PR_LOCAL: TevClientDataSet
    IndexFieldNames = 'PRIORITY'
    ControlType.Strings = (
      'SELECTED;CheckBox;Y;N')
    FieldDefs = <
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CLIENT_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'COMPANY_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CHECK_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'RUN_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'PAYROLL_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SELECTED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PROCESS_PRIORITY'
        DataType = ftInteger
      end
      item
        Name = 'PRIORITY'
        DataType = ftInteger
      end
      item
        Name = 'AUTOPAY_COMPANY'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CSR'
        DataType = ftString
        Size = 40
      end>
    Left = 384
    Top = 64
    object TMP_PR_LOCALCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object TMP_PR_LOCALCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object TMP_PR_LOCALPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object TMP_PR_LOCALCLIENT_NUMBER: TStringField
      FieldName = 'CLIENT_NUMBER'
    end
    object TMP_PR_LOCALCOMPANY_NUMBER: TStringField
      FieldName = 'COMPANY_NUMBER'
    end
    object TMP_PR_LOCALCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object TMP_PR_LOCALCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
    object TMP_PR_LOCALRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
    end
    object TMP_PR_LOCALTYPE: TStringField
      FieldName = 'PAYROLL_TYPE'
      Size = 1
    end
    object TMP_PR_LOCALSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object TMP_PR_LOCALSTATUS_DATE: TDateTimeField
      FieldName = 'STATUS_DATE'
    end
    object TMP_PR_LOCALSELECTED: TStringField
      FieldName = 'SELECTED'
      Size = 1
    end
    object TMP_PR_LOCALPROCESS_PRIORITY: TIntegerField
      FieldName = 'PROCESS_PRIORITY'
    end
    object TMP_PR_LOCALPRIORITY: TIntegerField
      FieldName = 'PRIORITY'
    end
    object TMP_PR_LOCALAUTOPAY_COMPANY: TStringField
      FieldName = 'AUTOPAY_COMPANY'
    end
    object TMP_PR_LOCALCSR: TStringField
      FieldName = 'CSR'
      Size = 40
    end
  end
  object PopupMenu1: TevPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 556
    Top = 136
    object MoveUp1: TMenuItem
      Caption = 'Move Up'
      OnClick = MoveUp1Click
    end
    object MoveDown1: TMenuItem
      Caption = 'Move Down'
      OnClick = MoveDown1Click
    end
    object MoveFirst1: TMenuItem
      Caption = 'Move First'
      OnClick = MoveFirst1Click
    end
    object MoveLast1: TMenuItem
      Caption = 'Move Last'
      OnClick = MoveLast1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object SelectAll1: TMenuItem
      Caption = 'Select All'
      OnClick = SelectAll1Click
    end
    object UnselectAll1: TMenuItem
      Caption = 'Unselect All'
      OnClick = UnselectAll1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object UnlockSelected1: TMenuItem
      Caption = 'Unlock Selected PR'
      OnClick = UnlockSelected1Click
    end
    object UnlockTrustAcct1: TMenuItem
      Caption = 'Unlock Trust Acct'
      Visible = False
    end
  end
  object PrintDialog1: TPrintDialog
    Left = 612
    Top = 168
  end
  object dsTMP_PR_LOCAL: TevDataSource
    DataSet = TMP_PR_LOCAL
    Left = 292
    Top = 64
  end
end
