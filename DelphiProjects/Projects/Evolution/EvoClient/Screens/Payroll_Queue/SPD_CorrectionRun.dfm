object CorrectionRun: TCorrectionRun
  Left = 434
  Top = 161
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Special Handling Instructions'
  ClientHeight = 571
  ClientWidth = 756
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 0
    Width = 756
    Height = 530
    Align = alClient
    TabOrder = 0
    object evLabel1: TevLabel
      Left = 8
      Top = 8
      Width = 177
      Height = 13
      Caption = 'Correction Taken and done by:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel2: TevLabel
      Left = 328
      Top = 8
      Width = 72
      Height = 13
      Caption = 'Check Date:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evDBText2: TevDBText
      Left = 400
      Top = 8
      Width = 73
      Height = 17
      DataField = 'CHECK_DATE'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evDBText3: TevDBText
      Left = 72
      Top = 24
      Width = 64
      Height = 13
      AutoSize = True
      DataField = 'NAME'
      DataSource = dsCompany
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel3: TevLabel
      Left = 186
      Top = 8
      Width = 53
      Height = 13
      Caption = 'evLabel3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel4: TevLabel
      Left = 480
      Top = 8
      Width = 36
      Height = 13
      Caption = 'Run #'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evDBText1: TevDBText
      Left = 520
      Top = 8
      Width = 65
      Height = 17
      DataField = 'RUN_NUMBER'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evDBText4: TevDBText
      Left = 8
      Top = 24
      Width = 57
      Height = 13
      DataField = 'CUSTOM_COMPANY_NUMBER'
      DataSource = dsCompany
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel5: TevLabel
      Left = 480
      Top = 24
      Width = 30
      Height = 13
      Caption = 'CSR:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel7: TevLabel
      Left = 8
      Top = 40
      Width = 34
      Height = 13
      Caption = 'Trust:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel8: TevLabel
      Left = 72
      Top = 40
      Width = 26
      Height = 13
      Caption = 'Tax:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evDBText7: TevDBText
      Left = 42
      Top = 40
      Width = 17
      Height = 17
      DataField = 'TRUST_SERVICE'
      DataSource = dsCompany
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evDBText8: TevDBText
      Left = 98
      Top = 40
      Width = 17
      Height = 17
      DataField = 'TAX_SERVICE'
      DataSource = dsCompany
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel9: TevLabel
      Left = 128
      Top = 40
      Width = 98
      Height = 13
      Caption = 'Delivery Service:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object evLabel10: TevLabel
      Left = 230
      Top = 40
      Width = 60
      Height = 13
      Caption = 'evLabel10'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lablCRNotes: TevLabel
      Left = 8
      Top = 64
      Width = 111
      Height = 13
      Caption = 'Special Handling Notes'
    end
    object evLabel11: TevLabel
      Left = 352
      Top = 64
      Width = 124
      Height = 13
      Caption = 'Miscellaneous Instructions'
    end
    object evLabel14: TevLabel
      Left = 16
      Top = 416
      Width = 66
      Height = 13
      Caption = 'Tax Approval:'
    end
    object evLabel15: TevLabel
      Left = 16
      Top = 432
      Width = 86
      Height = 13
      Caption = 'Finance Approval:'
    end
    object evLabel16: TevLabel
      Left = 16
      Top = 448
      Width = 110
      Height = 13
      Caption = 'Management Approval:'
    end
    object evLabel17: TevLabel
      Left = 128
      Top = 416
      Width = 50
      Height = 13
      Caption = 'evLabel17'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object evLabel18: TevLabel
      Left = 128
      Top = 432
      Width = 50
      Height = 13
      Caption = 'evLabel18'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object evLabel19: TevLabel
      Left = 128
      Top = 448
      Width = 50
      Height = 13
      Caption = 'evLabel19'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
    end
    object evLabel20: TevLabel
      Left = 512
      Top = 24
      Width = 60
      Height = 13
      Caption = 'evLabel20'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evLabel21: TevLabel
      Left = 8
      Top = 352
      Width = 249
      Height = 33
      AutoSize = False
      Caption = 
        'Please, note that "Same Day Pull and Replace" and "Combine Runs"' +
        ' are for informational purposes only'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object DBRadioGroup2: TevDBRadioGroup
      Left = 8
      Top = 144
      Width = 113
      Height = 33
      Caption = '~Exclude ACH'
      Columns = 2
      DataField = 'EXCLUDE_ACH'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Items.Strings = (
        'Yes'
        'No')
      ParentFont = False
      TabOrder = 0
      Values.Strings = (
        'Y'
        'N')
      OnChange = DBRadioGroup2Change
    end
    object DBRadioGroup4: TevDBRadioGroup
      Left = 136
      Top = 144
      Width = 121
      Height = 33
      Caption = '~Exclude Agency'
      Columns = 2
      DataField = 'EXCLUDE_AGENCY'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Items.Strings = (
        'Yes'
        'No')
      ParentFont = False
      TabOrder = 1
      Values.Strings = (
        'Y'
        'N')
    end
    object DBRadioGroup6: TevDBRadioGroup
      Left = 8
      Top = 190
      Width = 113
      Height = 47
      Caption = '~Exclude Billing'
      Columns = 2
      DataField = 'EXCLUDE_BILLING'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Items.Strings = (
        'Yes'
        'Select'
        'None')
      ParentFont = False
      TabOrder = 2
      Values.Strings = (
        'Y'
        'N')
      OnChange = DBRadioGroup6Change
      OnClick = DBRadioGroup6Change
    end
    object DBRadioGroup3: TevDBRadioGroup
      Left = 136
      Top = 189
      Width = 137
      Height = 49
      Caption = '~Exclude Tax'
      Columns = 2
      DataField = 'EXCLUDE_TAX_DEPOSITS'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Items.Strings = (
        'Deposits'
        'Liabilities'
        'Both'
        'None')
      ParentFont = False
      TabOrder = 3
      Values.Strings = (
        'D'
        'L'
        'B'
        'N')
    end
    object DBRadioGroup7: TevDBRadioGroup
      Left = 264
      Top = 144
      Width = 121
      Height = 49
      Caption = '~Exclude'
      Columns = 2
      DataField = 'EXCLUDE_R_C_B_0R_N'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Items.Strings = (
        'Reports'
        'Checks'
        'Both'
        'None')
      ParentFont = False
      TabOrder = 4
      Values.Strings = (
        'R'
        'C'
        'B'
        'N')
    end
    object evDBRadioGroup2: TevDBRadioGroup
      Left = 8
      Top = 241
      Width = 121
      Height = 49
      Cursor = crDrag
      Caption = '~Exclude TOA'
      Columns = 2
      DataField = 'EXCLUDE_TIME_OFF'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Items.Strings = (
        'All'
        'Accrual'
        'No')
      ParentFont = False
      TabOrder = 5
      Values.Strings = (
        'F'
        'Y'
        'N')
    end
    object memoCRNotes: TEvDBMemo
      Left = 8
      Top = 80
      Width = 337
      Height = 57
      DataField = 'CR_NOTES'
      DataSource = dsPayroll
      TabOrder = 6
    end
    object EvDBMemo2: TEvDBMemo
      Left = 352
      Top = 80
      Width = 337
      Height = 57
      DataField = 'MISCELLANEOUS_INSTRUCTIONS'
      DataSource = dsPayroll
      TabOrder = 7
    end
    object evDBRadioGroup3: TevDBRadioGroup
      Left = 136
      Top = 241
      Width = 177
      Height = 33
      Caption = '~Same Day Pull and Replace'
      Columns = 2
      DataField = 'SAME_DAY_PULL_AND_REPLACE'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Items.Strings = (
        'Yes'
        'No')
      ParentFont = False
      TabOrder = 8
      Values.Strings = (
        'Y'
        'N')
    end
    object evDBRadioGroup4: TevDBRadioGroup
      Left = 8
      Top = 296
      Width = 113
      Height = 33
      Caption = '~Combine Runs'
      Columns = 2
      DataField = 'COMBINE_RUNS'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Items.Strings = (
        'Yes'
        'No')
      ParentFont = False
      TabOrder = 9
      Values.Strings = (
        'Y'
        'N')
      OnChange = evDBRadioGroup4Change
    end
    object grbxCombineRuns: TevGroupBox
      Left = 136
      Top = 280
      Width = 113
      Height = 65
      Caption = 'Runs To Combine'
      TabOrder = 10
      object evLabel12: TevLabel
        Left = 16
        Top = 16
        Width = 23
        Height = 13
        Caption = 'From'
      end
      object evLabel13: TevLabel
        Left = 72
        Top = 16
        Width = 13
        Height = 13
        Caption = 'To'
      end
      object evDBSpinEdit2: TevDBSpinEdit
        Left = 16
        Top = 32
        Width = 33
        Height = 21
        Increment = 1.000000000000000000
        MaxValue = 99.000000000000000000
        MinValue = 1.000000000000000000
        Value = 1.000000000000000000
        DataField = 'COMBINE_FROM_RUN'
        DataSource = dsPayroll
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object evDBSpinEdit3: TevDBSpinEdit
        Left = 64
        Top = 32
        Width = 33
        Height = 21
        Increment = 1.000000000000000000
        MaxValue = 99.000000000000000000
        MinValue = 1.000000000000000000
        Value = 1.000000000000000000
        DataField = 'COMBINE_TO_RUN'
        DataSource = dsPayroll
        TabOrder = 1
        UnboundDataType = wwDefault
      end
    end
    object evDBRadioGroup5: TevDBRadioGroup
      Left = 392
      Top = 144
      Width = 129
      Height = 33
      Caption = '~Print All Reports'
      Columns = 2
      DataField = 'PRINT_ALL_REPORTS'
      DataSource = dsPayroll
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Items.Strings = (
        'Yes'
        'No')
      ParentFont = False
      TabOrder = 11
      Values.Strings = (
        'Y'
        'N')
      OnChange = evDBRadioGroup5Change
    end
    object PageControl1: TPageControl
      Left = 392
      Top = 192
      Width = 305
      Height = 305
      ActivePage = tsBilling
      TabOrder = 12
      object tsBilling: TTabSheet
        Caption = 'Billing services'
        object gbServices: TGroupBox
          Left = 0
          Top = 0
          Width = 297
          Height = 273
          Caption = 'Select services to block'
          TabOrder = 0
          object evDBGridServices: TevDBCheckGrid
            Left = 8
            Top = 16
            Width = 281
            Height = 249
            DisableThemesInTitle = False
            Selected.Strings = (
              'NAME'#9'40'#9'Name'#9'F'#9
              'EFFECTIVE_START_DATE'#9'10'#9'Effective Start Date'#9'F'#9
              'EFFECTIVE_END_DATE'#9'10'#9'Effective End Date'#9'F'#9)
            IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
            IniAttributes.SectionName = 'TCorrectionRun\evDBGridServices'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            OnMultiSelectAllRecords = evDBGridServicesMultiSelectAllRecords
            OnMultiSelectRecord = evDBGridServicesMultiSelectAllRecords
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsServices
            MultiSelectOptions = [msoShiftSelect]
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            ReadOnly = True
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
            OnMultiSelectReverseRecords = evDBGridServicesMultiSelectReverseRecords
          end
        end
      end
      object tsReports: TTabSheet
        Caption = 'Reports to print'
        ImageIndex = 1
        object grbxReports: TevGroupBox
          Left = 0
          Top = 4
          Width = 297
          Height = 273
          Caption = 'Reports To Print'
          TabOrder = 0
          object lablE_D_Code: TevLabel
            Left = 8
            Top = 228
            Width = 89
            Height = 13
            Caption = '~Report Name'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object grReports: TevDBGrid
            Left = 8
            Top = 16
            Width = 281
            Height = 209
            DisableThemesInTitle = False
            Selected.Strings = (
              'ReportName'#9'40'#9'Report Name'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TCorrectionRun\grReports'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsPRReports
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          inline MiniNavigationFrame: TMiniNavigationFrame
            Left = 223
            Top = 232
            Width = 65
            Height = 25
            TabOrder = 1
            inherited evActionList1: TevActionList
              Left = 16
            end
          end
          object wwlcReport: TevDBLookupCombo
            Left = 8
            Top = 244
            Width = 177
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CUSTOM_NAME'#9'40'#9'CUSTOM_E_D_CODE_NUMBER')
            DataField = 'CO_REPORTS_NBR'
            DataSource = dsPRReports
            LookupTable = DM_CO_REPORTS.CO_REPORTS
            LookupField = 'CO_REPORTS_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
        end
      end
    end
  end
  object evPanel2: TevPanel
    Left = 0
    Top = 530
    Width = 756
    Height = 41
    Align = alBottom
    TabOrder = 1
    object evLabel6: TevLabel
      Left = 96
      Top = 16
      Width = 274
      Height = 13
      Caption = 'When you print, all your changes are applied automatically'
    end
    object bbtnApply: TevBitBtn
      Left = 520
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Apply'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = bbtnApplyClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
        DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
        DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
        DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
        DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
        DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
        2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
        A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
        AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
      NumGlyphs = 2
      Margin = 0
    end
    object bbtnCancel: TevBitBtn
      Left = 608
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
      OnClick = bbtnCancelClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
        DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
        9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
        DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
        DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
        DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
        91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
        999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
      NumGlyphs = 2
      Margin = 0
    end
    object bbtnPrint: TevBitBtn
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Print'
      TabOrder = 2
      OnClick = bbtnPrintClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
        08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
        8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
        8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
        8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
        78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
        7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
        DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
        DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
  end
  object dsCompany: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    Left = 600
    Top = 64
  end
  object dsPayroll: TevDataSource
    DataSet = DM_PR.PR
    Left = 648
    Top = 64
  end
  object dsBatch: TevDataSource
    DataSet = DM_PR_BATCH.PR_BATCH
    Left = 648
    Top = 112
  end
  object DM_PAYROLL: TDM_PAYROLL
    Left = 648
    Top = 16
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 600
    Top = 16
  end
  object dsPRReports: TevDataSource
    DataSet = DM_PR_REPORTS.PR_REPORTS
    MasterDataSource = dsPayroll
    Left = 456
    Top = 233
  end
  object dsServices: TevDataSource
    DataSet = DM_CO_SERVICES.CO_SERVICES
    MasterDataSource = dsPayroll
    Left = 452
    Top = 281
  end
  object evdsPrServices: TevDataSource
    DataSet = DM_PR_SERVICES.PR_SERVICES
    Left = 452
    Top = 321
  end
end
