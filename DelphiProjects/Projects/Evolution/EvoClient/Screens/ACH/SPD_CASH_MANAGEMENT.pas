// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_CASH_MANAGEMENT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, Db,  ComCtrls, ExtCtrls, SPackageEntry,
  Mask, Gauges, SFrameEntry,  Wwdbigrd, Wwdbgrid, Wwdatsrc,
  wwdblook, SDataStructure, SDDClasses, EvCommonInterfaces,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, wwdbedit, Wwdotdot, Wwdbcomb, SDataDictbureau,
  EvMainboard, EvContext, isBasicUtils, EvAchUtils, evAchPayrollList,
  Variants, EvUIUtils, EvUIComponents, EvClientDataSet, isUIEdit,
  isUIwwDBComboBox, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TPROCESS_SB_ACH_REMOTE = class(TFrameEntry)
    wwdsTMP_CO_PR_ACH: TevDataSource;
    pcACHFile: TevPageControl;
    tsAchAccounts: TTabSheet;
    wwdsTMP_CO_PR_ACH2: TevDataSource;
    tsFlags: TTabSheet;
    tsPayrolls: TTabSheet;
    wwDBGrid1: TevDBGrid;
    evPanel1: TevPanel;
    wwdbgSB_BANKS: TevDBLookupCombo;
    btnGetDataForAch: TevBitBtn;
    lblStatusFrom: TevLabel;
    Label1: TevLabel;
    evLabel2: TevLabel;
    cbStatusFrom: TevComboBox;
    dtDateFrom: TevDateTimePicker;
    cbPayrollType: TevComboBox;
    dtDateTO: TevDateTimePicker;
    evLabel1: TevLabel;
    evPanel2: TevPanel;
    butnSelectAll: TevBitBtn;
    evSpeedButton1: TevBitBtn;
    evPanel3: TevPanel;
    evPanel4: TevPanel;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    SB_BANK_ACCOUNTS: TSB_BANK_ACCOUNTS;
    cbCheckDateFilter: TevCheckBox;
    lACHProcessMsg: TLabel;
    evGroupBox1: TevGroupBox;
    rgBlockNegChecks: TevCheckBox;
    gbCombine: TevGroupBox;
    evLabel4: TevLabel;
    eThreshold: TevEdit;
    cbCombine: TevComboBox;
    gbEffectiveDate: TevGroupBox;
    cbEffectiveDate: TevCheckBox;
    lblStatusTo: TevLabel;
    cbStatusTo: TevComboBox;
    cbUseOffsets: TevComboBox;
    evLabel3: TevLabel;
    gbOther: TevGroupBox;
    cbUseSBEIN: TevCheckBox;
    cbBalanceBatches: TevCheckBox;
    cbCTS: TevCheckBox;
    cbSunTrust: TevCheckBox;
    cbFedReserve: TevCheckBox;
    cbIntercept: TevCheckBox;
    cbBlockSBCredit: TevCheckBox;
    cbPayrollHeader: TevCheckBox;
    cbBlockDebits: TevCheckBox;
    cbDescReversal: TevCheckBox;
    cbBankOne: TevCheckBox;
    gbHandShake: TGroupBox;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    eHeader: TevEdit;
    eFooter: TevEdit;
    gbCoId: TevGroupBox;
    evLabel7: TevLabel;
    eCoId: TevEdit;
    gbEINOverride: TevGroupBox;
    evLabel8: TevLabel;
    eSBEINOverride: TevDBComboBox;
    cbDisplayCoNbrAndName: TCheckBox;
    TrustComboBox: TevDBComboBox;
    BillingComboBox: TevDBComboBox;
    TaxComboBox: TevDBComboBox;
    DDComboBox: TevDBComboBox;
    WcComboBox: TevDBComboBox;
    rgBlockCompanyDD: TevCheckBox;
    cbBlockNegativeLiabilities: TevCheckBox;
    evLabel9: TevLabel;
    cbPostProcessReport: TevComboBox;
    PreprocessAchBtn: TevBitBtn;
    ProcessAchBtn: TevBitBtn;
    cbHSA: TevCheckBox;
    procedure btnGetDataForAchClick(Sender: TObject);
    procedure butnSelectAllClick(Sender: TObject);
    procedure cbCombineClick(Sender: TObject);
    procedure cbUseSBEINClick(Sender: TObject);
    procedure cbBalanceBatchesClick(Sender: TObject);
    procedure cbCTSClick(Sender: TObject);
    procedure cbSunTrustClick(Sender: TObject);
    procedure cbFedReserveClick(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure wwdbgSB_BANKSChange(Sender: TObject);
    procedure cbStatusFromChange(Sender: TObject);
    procedure cbStatusToChange(Sender: TObject);
    procedure eThresholdChange(Sender: TObject);
    procedure cbInterceptClick(Sender: TObject);
    procedure eHeaderChange(Sender: TObject);
    procedure eFooterChange(Sender: TObject);
    procedure cbBlockSBCreditClick(Sender: TObject);
    procedure cbPayrollHeaderClick(Sender: TObject);
    procedure cbBlockDebitsClick(Sender: TObject);
    procedure eCoIdChange(Sender: TObject);
    procedure cbDescReversalClick(Sender: TObject);
    procedure cbBankOneClick(Sender: TObject);
    procedure cbEffectiveDateClick(Sender: TObject);
    procedure eSBEINOverrideChange(Sender: TObject);
    procedure cbDisplayCoNbrAndNameClick(Sender: TObject);
    procedure tsFlagsShow(Sender: TObject);
    procedure rgBlockCompanyDDClick(Sender: TObject);
    procedure cbBlockNegativeLiabilitiesClick(Sender: TObject);
    procedure cbPostProcessReportClick(Sender: TObject);
    procedure tsPayrollsEnter(Sender: TObject);
    procedure PreprocessAchBtnClick(Sender: TObject);
    procedure ProcessAchBtnClick(Sender: TObject);
    procedure TrustComboBoxDropDown(Sender: TObject);
    procedure TrustComboBoxCloseUp(Sender: TwwDBComboBox; Select: Boolean);
    procedure pcACHFileChanging(Sender: TObject; var AllowChange: Boolean);
    procedure cbHSAClick(Sender: TObject);

  private
    FCurrentActNbr: integer;
    FScreenIsReadOnly: Boolean;
    PayrollList: TevAchPayrollList;
    FPreprocess: Boolean;
    procedure SetCurrentActNbr(const Value: Integer);
    procedure GetData;
    procedure CreateNewAchTask(const APreprocess: Boolean);
    function CheckThreshold: boolean;
    procedure SelectAll(const AValue: Boolean);
    procedure PayrollListAfterScroll(DataSet: TDataSet);
  protected
    function GetIfReadOnly: Boolean; override;
  public
    procedure OnGlobalFlagChange(const AResourceInfo: IevGlobalFlagInfo; const AOperation: Char); override;
    property  CurrentActNbr: Integer read FCurrentActNbr write SetCurrentActNbr;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
    function CanClose: Boolean; override;
  end;

var
  PROCESS_SB_ACH_REMOTE: TPROCESS_SB_ACH_REMOTE;

implementation

uses
  EvConsts, EvUtils, EvTypes, SFieldCodeValues, SACHTypes, EvStreamUtils,
  isBaseClasses;

{$R *.DFM}

procedure EnableChildren_ForControl(aControl: TControl);
var
  I: integer;
begin
  with aControl do
    for I := 0 to ComponentCount - 1 do
      if Components[I] is TControl then
        TControl(Components[I]).Enabled := Enabled;
end;

procedure GetACHDateRange(BankAcctNbr: Integer; Status: string; var dtFrom, dtTo: TDateTime);
begin
  dtFrom := Date;
  dtTo := dtFrom;
  with ctx_DataAccess.TEMP_CUSTOM_VIEW do
  begin
    if Active then Close;

    with TExecDSWrapper.Create('GenericSelect2WithCondition') do
    try
      SetMacro('Columns', 'min(T1.ACH_DATE) MinACHDATE, Max(T1.ACH_DATE) MaxACHDATE');
      SetMacro('TABLE1', 'TMP_CO_PR_ACH');
      SetMacro('TABLE2', 'TMP_CO');
      SetMacro('JOINFIELD', 'CO');
      SetMacro('Condition', 't1.cl_nbr = t2.cl_nbr and T1.STATUS in (' + Status + ') and T2.ACH_SB_BANK_ACCOUNT_NBR=' + IntToStr(BankAcctNbr));
      DataRequest(AsVariant);
    finally
      Open;
    end;

    First;
    if FieldByName('MinACHDATE').IsNull then
      dtFrom := Date
    else
      dtFrom := FieldByName('MinACHDATE').Value;
    if FieldByName('MaxACHDATE').IsNull then
      dtTo := Date
    else
      dtTo := FieldByName('MaxACHDATE').Value;

    Close;
  end;
end;

procedure TPROCESS_SB_ACH_REMOTE.Activate;
var
  ReportName: String;
  i: Integer;
begin
  inherited;
  FScreenIsReadOnly := False;
  (Owner as TFramePackageTmpl).SetButton(NavRefresh, False);
  (Owner as TFramePackageTmpl).SetButton(NavHistory, False);
  cbBalanceBatches.Checked := mb_AppSettings.AsBoolean['BalanceACHBatches'];
  cbBlockDebits.Checked := mb_AppSettings.AsBoolean['ACHBlockDebits'];
  cbCombine.ItemIndex := mb_AppSettings.AsInteger['CombineACHTrans'];

  ReportName := mb_AppSettings.AsString['PostProcessACHReport'];

  cbPostProcessReport.Items.Clear;
  cbPostProcessReport.Items.Add('N/A');
  DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.DataRequired('ANCESTOR_CLASS_NAME='''+REPORT_ASCII_TYPE+'''');
  DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.First;
  while not DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Eof do
  begin
    cbPostProcessReport.Items.Add(DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.REPORT_DESCRIPTION.AsString+
      ' (B'+DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.SB_REPORT_WRITER_REPORTS_NBR.AsString+')');
    DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Next;
  end;
  DM_SYSTEM.SY_REPORT_WRITER_REPORTS.DataRequired('ANCESTOR_CLASS_NAME='''+REPORT_ASCII_TYPE+'''');
  DM_SYSTEM.SY_REPORT_WRITER_REPORTS.First;
  while not DM_SYSTEM.SY_REPORT_WRITER_REPORTS.Eof do
  begin
    cbPostProcessReport.Items.Add(DM_SYSTEM.SY_REPORT_WRITER_REPORTS.REPORT_DESCRIPTION.AsString+
      ' (S'+DM_SYSTEM.SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR.AsString+')');
    DM_SYSTEM.SY_REPORT_WRITER_REPORTS.Next;
  end;
  cbPostProcessReport.ItemIndex := 0;

  for i := 0 to cbPostProcessReport.Items.Count - 1 do
  if cbPostProcessReport.Items[i] = ReportName then
  begin
    cbPostProcessReport.ItemIndex := i;
    break;
  end;

  if cbPostProcessReport.ItemIndex = 0 then
    mb_AppSettings.AsString['PostProcessACHReport'] :=
      cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];

  eThreshold.Text := mb_AppSettings.GetValue('CombineACHTreshold', '0.00');
  eThreshold.Enabled := cbCombine.Enabled and (cbCombine.ItemIndex <> 0);
  if not eThreshold.Enabled then
    eThreshold.Text := '0';
  eCoId.Text := mb_AppSettings['ACHCoId'];
  eSBEINOverride.Text := mb_AppSettings['ACHSBEINOverride'];
  cbUseSBEIN.Checked := mb_AppSettings.AsBoolean['USESBEINForACH'];
  cbCTS.Checked := mb_AppSettings.AsBoolean['ACHCTS'];
  cbSunTrust.Checked := mb_AppSettings.AsBoolean['ACHSunTrust'];
  cbBlockSBCredit.Checked := mb_AppSettings.AsBoolean['ACHBlockSBCredit'];
  cbFedReserve.Checked := mb_AppSettings.AsBoolean['ACHFedReserve'];
  cbIntercept.Checked := mb_AppSettings.AsBoolean['ACHIntercept'];
  cbPayrollHeader.Checked := mb_AppSettings.AsBoolean['ACHPayrollHeader'];
  cbDescReversal.Checked := mb_AppSettings.AsBoolean['ACHDescReversal'];
  cbBankOne.Checked := mb_AppSettings.AsBoolean['ACHBankOne'];
  cbBlockNegativeLiabilities.Checked := mb_AppSettings.AsBoolean['ACHBlockNegativeLiabilities'];
  cbEffectiveDate.Checked := mb_AppSettings.AsBoolean['ACHEffectiveDate'];
  cbDisplayCoNbrAndName.Checked := mb_AppSettings.AsBoolean['DisplayCoNbrAndName'];
  eHeader.Text := mb_AppSettings['ACHHeader'];
  eFooter.Text := mb_AppSettings['ACHFooter'];

  cbHSA.Checked := mb_AppSettings.AsBoolean['ACHHSA'];

  cbStatusFrom.ItemIndex := 0;
  cbStatusTo.ItemIndex := 0;
  cbPayrollType.ItemIndex := 1;

  if DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Active then
    CurrentActNbr := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger;
  with DM_SERVICE_BUREAU do
  begin
    SB.CheckDSCondition('');
    SB_AGENCY.CheckDSCondition('');
    SB_BANKS.CheckDSCondition('');
    SB_BANK_ACCOUNTS.CheckDSCondition('');
    ctx_DataAccess.OpenDataSets([SB, SB_AGENCY, SB_BANKS, SB_BANK_ACCOUNTS]);
  end;
  tsFlags.TabVisible := False;
  tsPayrolls.TabVisible := False;
  if not SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', CurrentActNbr, []) then
    CurrentActNbr := SB_BANK_ACCOUNTS.FieldByName('SB_BANK_ACCOUNTS_NBR').AsInteger;
  wwdbgSB_BANKS.LookupValue := IntToStr(CurrentActNbr);
  DM_TEMPORARY.TMP_CO.DataRequired('ALL');

  PayrollList := TevAchPayrollList.Create(nil);
  PayrollList.CreateMyFields;
  wwdsTMP_CO_PR_ACH2.DataSet := PayrollList;

  wwDBGrid1.SetControlType('TRUST_IMPOUND', fctCustom, 'TrustComboBox');
  wwDBGrid1.SetControlType('TAX_IMPOUND', fctCustom, 'TaxComboBox');
  wwDBGrid1.SetControlType('DD_IMPOUND', fctCustom, 'DDComboBox');
  wwDBGrid1.SetControlType('BILLING_IMPOUND', fctCustom, 'BillingComboBox');
  wwDBGrid1.SetControlType('WC_IMPOUND', fctCustom, 'WcComboBox');
  wwDBGrid1.ApplySelected;

  inherited;
end;

procedure TPROCESS_SB_ACH_REMOTE.PreprocessAchBtnClick(Sender: TObject);
begin
  CreateNewAchTask(True);
end;

procedure TPROCESS_SB_ACH_REMOTE.ProcessAchBtnClick(Sender: TObject);
begin
  CreateNewAchTask(False);
end;

procedure TPROCESS_SB_ACH_REMOTE.CreateNewAchTask(const APreprocess: Boolean);
var
  sMsg: String;
  I: Integer;
  t: IevCashManagementTask;
  Filter: String;
  Filtered: Boolean;
begin

  FPreprocess := APreprocess;

  if eThreshold.Enabled and not CheckThreshold then
    Exit;

  if (StrToFloatDef(eThreshold.Text, 0) > 0)
  and (cbCombine.ItemIndex <> 0) then
    if not (EvMessage('If you have companies in this ACH file with bank accounts broken out by DBDT,' + #13 +
    'the Combine function used with the Threshold may cause your file to be out of balance.' + #13 +
    'If you choose to use the Combine function, verify the file is correct before submitting it.', mtWarning, [mbYes, mbNo]) = mrYes) then
      Exit;

  DM_SERVICE_BUREAU.SB.Activate;

  if cbStatusTo.ItemIndex > 0 then
    sMsg := 'Ready to update status from "' + cbStatusFrom.Text + '" to "' + cbStatusTo.Text + '"?'
  else if not APreprocess then
    sMsg := 'Ready to process?'
  else
    sMsg := 'Ready to preprocess?';

  if not (EvMessage(sMsg, mtConfirmation, [mbYes, mbNo]) = mrYes) then
    Exit;

  t := mb_TaskQueue.CreateTask(QUEUE_TASK_PAYROLL_ACH, True) as IevCashManagementTask;
  try
    LoadACHOptionsFromRegistryI(t.ACHOptions);
    t.ACHOptions.PostProcessReportName := cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];

    t.Caption := 'Processing Cash Management';

    t.ReversalACH := False;
    if cbStatusFrom.ItemIndex = 3 then
    with CreateMessageDialog('Would you like to create a Reversal ACH?', mtConfirmation, [mbYes, mbNo, mbCancel]) do
    try
      for I := 0 to ComponentCount - 1 do
      begin
        if Components[I].Name = 'Cancel' then
          ActiveControl := TevButton(Components[I]);
      end;
      case ShowModal of
        mrYes:
          t.ReversalACH := True;
        mrCancel:
          Exit;
      end;
    finally
      Free;
    end;

    t.PeriodFrom := dtDateFrom.DateTime;
    t.PeriodTo := dtDateTo.DateTime;
    t.StatusFromIndex := cbStatusFrom.ItemIndex;
    t.StatusToIndex := cbStatusTo.ItemIndex;
    t.BlockNegativeChecks := rgBlockNegChecks.Checked;
    t.BlockNegativeLiabilities := cbBlockNegativeLiabilities.Checked;
    t.BlockCompanyDD := rgBlockCompanyDD.Checked;
    t.IncludeOffsets := cbUseOffsets.ItemIndex;
    t.SB_BANK_ACCOUNTS_NBR := CurrentActNbr;
    t.Combine := cbCombine.ItemIndex;
    t.Threshold := StrToFloat(eThreshold.Text);
    t.Preprocess := APreprocess;
    t.HSA := cbHSA.Checked;

    if APreprocess then
      t.Caption := 'Preprocessing Cash Management'
    else
      t.Caption := 'Processing Cash Management';

    if PayrollList.State = dsEdit then
      PayrollList.Post;

    Filtered := PayrollList.Filtered;
    Filter := PayrollList.Filter;

    PayrollList.Filtered := False;
    PayrollList.Filter := 'SELECTED';
    PayrollList.Filtered := True;
    try
      t.ACHDataStream := PayrollList.GetDataStream(True, False);
    finally
      PayrollList.Filter := Filter;
      PayrollList.Filtered := Filtered;
    end;
    SetReadOnly(True);
    FScreenIsReadOnly := True;
    ctx_EvolutionGUI.AddTaskQueue(t);
  finally
    t := nil;
  end;

  Mainboard.GlobalCallbacks.Subscribe('', cetGlobalFlagEvent, GF_SB_ACH_PROCESS);

  tsFlags.TabVisible := True;

  tsFlags.TabVisible := False;
  PayrollList.SortRecords;
end;

procedure TPROCESS_SB_ACH_REMOTE.btnGetDataForAchClick(Sender: TObject);
begin
  inherited;
  CurrentActNbr := StrToInt(wwdbgSB_BANKS.LookupValue);
  tsFlags.TabVisible := False;

  GetData;

  if cbStatusFrom.ItemIndex = 3 then
    butnSelectAll.Caption := 'Select All'
  else
    butnSelectAll.Caption := 'Select None';

  PayrollList.SortRecords;

  tsPayrolls.TabVisible := True;
  pcACHFile.ActivatePage(tsPayrolls);

end;

procedure TPROCESS_SB_ACH_REMOTE.Deactivate;
begin
  try // should be closed
    Mainboard.GlobalCallbacks.Unsubscribe('', cetGlobalFlagEvent, GF_SB_ACH_PROCESS);
  except
  end;
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Filtered := False;
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Filter := '';
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', CurrentActNbr, []);
  PayrollList.Free;
  inherited;
end;

procedure TPROCESS_SB_ACH_REMOTE.SelectAll(const AValue: Boolean);
var
  SaveBookmark: Integer;
  OldFiltered: boolean;
  sIndex: string;
begin
  SaveBookmark := PayrollList.FieldByName('CO_PR_ACH_NBR').AsInteger;
  OldFiltered := PayrollList.Filtered;
  PayrollList.Filtered := False;
  sIndex := PayrollList.IndexName;
  PayrollList.IndexName := '';
  PayrollList.DisableControls;
  try
    PayrollList.First;
    while not PayrollList.Eof do
    begin
      PayrollList.Edit;
      PayrollList.SELECTED.AsBoolean := AValue;
      PayrollList.Post;
      PayrollList.Next;
    end;
    PayrollList.IndexName := sIndex;
    PayrollList.Filtered := OldFiltered;
    PayrollList.Locate('CO_PR_ACH_NBR', SaveBookmark, []);
  finally
    PayrollList.EnableControls;
  end;  
end;

procedure TPROCESS_SB_ACH_REMOTE.butnSelectAllClick(Sender: TObject);
begin
  inherited;
  if butnSelectAll.Caption = 'Select All' then
  begin
    butnSelectAll.Caption := 'Select None';
    SelectAll(True);
  end
  else
  begin
    butnSelectAll.Caption := 'Select All';
    SelectAll(False);
  end;
end;

procedure TPROCESS_SB_ACH_REMOTE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.CheckDSCondition('');
  AddDS([DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS, SB_BANK_ACCOUNTS], SupportDataSets);
end;

procedure TPROCESS_SB_ACH_REMOTE.cbCombineClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsInteger['CombineACHTrans'] := cbCombine.ItemIndex;
  eThreshold.Enabled := cbCombine.Enabled and (cbCombine.ItemIndex <> 0);
  if not eThreshold.Enabled then
    eThreshold.Text := '0';
end;

procedure TPROCESS_SB_ACH_REMOTE.cbUseSBEINClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['USESBEINForACH'] := cbUseSBEIN.Checked;
  eSBEINOverride.Enabled := cbUseSBEIN.Checked;
  evLabel8.Enabled := cbUseSBEIN.Checked;
  gbEINOverride.Enabled := cbUseSBEIN.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbBalanceBatchesClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['BalanceACHBatches'] := cbBalanceBatches.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbCTSClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHCTS'] := cbCTS.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbSunTrustClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHSunTrust'] := cbSunTrust.Checked;
  cbBlockSBCredit.Enabled := cbSunTrust.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbFedReserveClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHFedReserve'] := cbFedReserve.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.evSpeedButton1Click(Sender: TObject);
begin
  inherited;

  tsFlags.TabVisible := True;
  pcACHFile.ActivatePage(tsFlags);
end;

procedure TPROCESS_SB_ACH_REMOTE.wwdbgSB_BANKSChange(Sender: TObject);
var
  dtFrom, dtTo: TDateTime;
  Status: string;
begin
  inherited;
  btnGetDataForAch.Enabled := wwdbgSB_BANKS.LookupValue <> '';
  if btnGetDataForAch.Enabled then
  begin
    CurrentActNbr := StrToInt(wwdbgSB_BANKS.LookupValue);
    Status := AchReportStatusStr(cbStatusFrom.ItemIndex);
    GetACHDateRange(CurrentActNbr, Status, dtFrom, dtTo);
    dtDateFrom.DateTime := dtFrom;
    dtDateTo.DateTime := dtTo;
  end;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbStatusFromChange(Sender: TObject);
begin
  inherited;
  if cbStatusFrom.ItemIndex = 3 then
  begin
    cbStatusTo.ItemIndex := -1;
    cbStatusTo.Enabled := False;
  end
  else
  begin
    if cbStatusTo.ItemIndex = -1 then
      cbStatusTo.ItemIndex := 0;
    cbStatusTo.Enabled := True;
  end;
  wwdbgSB_BANKSChange(Sender);
end;

procedure TPROCESS_SB_ACH_REMOTE.cbStatusToChange(Sender: TObject);
begin
  inherited;
  if cbStatusTo.ItemIndex > 0 then
  begin
    PreprocessAchBtn.Visible := False;
    ProcessAchBtn.Caption := 'Update';
  end
  else
  begin
    PreprocessAchBtn.Visible := True;
    ProcessAchBtn.Caption := 'Process';
  end;
end;

procedure TPROCESS_SB_ACH_REMOTE.SetCurrentActNbr(const Value: Integer);
begin
  Assert(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', Value, []));
  if DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('SUPPRESS_OFFSET_ACCOUNT').Value = SB_BANK_SUPPRESS_OFFSET_NO then
    cbUseOffsets.ItemIndex := OFF_USE_OFFSETS
  else if DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('SUPPRESS_OFFSET_ACCOUNT').Value = SB_BANK_SUPPRESS_OFFSET_EXCEPT_DD then
    cbUseOffsets.ItemIndex := OFF_ONLY_EE_DD
  else
    cbUseOffsets.ItemIndex := OFF_DONT_USE;
  rgBlockNegChecks.Checked := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('BLOCK_NEGATIVE_CHECKS').Value = 'Y';
  FCurrentActNbr := Value;
end;

procedure TPROCESS_SB_ACH_REMOTE.GetData;
var
  SentInfo: string;
  aDate: string;
  i: Integer;
  ReadOnlyClientList : IisStringList;
begin
  if ctx_DataAccess.TEMP_CUSTOM_VIEW.Active then
    ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;

  with TExecDSWrapper.Create('GetACHData') do
  begin
    SetParam('SB_BANK_ACCOUNTS_NBR', CurrentActNbr);
    SetParam('dtFrom', dtDateFrom.DateTime);
    SetParam('dtTo', dtDateTo.DateTime);
    SetMacro('CustomFilter', '');
    if cbCheckDateFilter.Checked then
      SetMacro('DatePeriod', 'CHECK_DATE >= :dtFrom and CHECK_DATE <= :dtTo')
    else
      SetMacro('DatePeriod', 'ACH_DATE >= :dtFrom and ACH_DATE <= :dtTo');
    case cbStatusFrom.ItemIndex of
      0:
        begin
          SetMacro('CustomFilter', 'and A.STATUS = :Status1');
          SetParam('Status1', COMMON_REPORT_STATUS__PENDING);
        end;
      1:
        begin
          SetMacro('CustomFilter', 'and A.STATUS = :Status1');
          SetParam('Status1', COMMON_REPORT_STATUS__HOLD);
        end;
      2:
        begin
          SetMacro('CustomFilter', 'and (A.STATUS = :Status1 or A.STATUS = :Status2)');
          SetParam('Status1', COMMON_REPORT_STATUS__PENDING);
          SetParam('Status2', COMMON_REPORT_STATUS__HOLD);
        end;
      3:
        begin
          SetMacro('CustomFilter', 'and A.STATUS = :Status1');
          SetParam('Status1', COMMON_REPORT_STATUS__COMPLETED);
        end;
      4:
        begin
          SetMacro('CustomFilter', 'and A.STATUS = :Status1');
          SetParam('Status1', COMMON_REPORT_STATUS__DELETED);
        end;
    end;
    ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;

  ctx_DataAccess.TEMP_CUSTOM_VIEW.First;
  PayrollList.DisableControls;
  PayrollList.EmptyDataSet;

  for i := 0 to PayrollList.Fields.Count - 1 do
    PayrollList.Fields[i].ReadOnly := False;

  PayrollList.Filtered := False;
  PayrollList.Filter := '';
  try
    ReadOnlyClientList := GetReadOnlyClientCompanyList;
    while not ctx_DataAccess.TEMP_CUSTOM_VIEW.EOF do
    begin
      if (ReadOnlyClientList.IndexOf(ctx_DataAccess.TEMP_CUSTOM_VIEW.fieldByName('CL_NBR').AsString+';'+ctx_DataAccess.TEMP_CUSTOM_VIEW.fieldByName('CO_NBR').AsString) = -1) then
      begin
        if (cbPayrollType.ItemIndex = 0) or
           (cbPayrollType.ItemIndex = 1) and
             not (ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT]) or
           (cbPayrollType.ItemIndex = 2) and
             (ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_ADJUSTMENT]) then
        begin
          PayrollList.Append;
          PayrollList.SELECTED.AsBoolean := (cbStatusFrom.ItemIndex <> 3);
          PayrollList.CO_PR_ACH_NBR.AsInteger := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CO_PR_ACH_NBR').AsInteger;
          PayrollList.CL_NBR.AsInteger := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CL_NBR').AsInteger;
          PayrollList.PROCESS_DATE.AsDateTime := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('PROCESS_DATE').AsDateTime;
          PayrollList.CO_NBR.AsInteger := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CO_NBR').AsInteger;
          PayrollList.CUSTOM_COMPANY_NUMBER.AsString := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
          PayrollList.CUSTOM_CLIENT_NUMBER.AsString := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CUSTOM_CLIENT_NUMBER').AsString;
          PayrollList.NAME.AsString := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('NAME').AsString;
          PayrollList.CL_NBR.AsInteger := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CL_NBR').AsInteger;
          PayrollList.CO_NBR.AsInteger := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CO_NBR').AsInteger;
          PayrollList.CHECK_DATE.Value := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CHECK_DATE').Value;
          PayrollList.PR_NBR.AsInteger := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('PR_NBR').AsInteger;
          PayrollList.RUN_NUMBER.AsInteger := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('RUN_NUMBER').AsInteger;
          PayrollList.ACH_DATE.AsDateTime := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('ACH_DATE').AsDateTime;
          PayrollList.WC_AMOUNT.Clear;
          PayrollList.STATUS.AsString := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('STATUS').AsString;
          PayrollList.BANK_ACCOUNT_REGISTER_NAME.Value := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('BANK_ACCOUNT_REGISTER_NAME').Value;
          PayrollList.CLIENT_NAME.AsString := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('CLIENT_NAME').AsString;
          PayrollList.COMPANY_FEIN.AsString := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('COMPANY_FEIN').AsString;

          Assert(DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([PayrollList.CL_NBR.Value, PayrollList.CO_NBR.Value]), []));

          if DM_TEMPORARY.TMP_CO.TRUST_SERVICE.AsString = TRUST_SERVICE_NO then
            PayrollList.TRUST_IMPOUND.Value := PAYMENT_TYPE_NONE
          else
            PayrollList.TRUST_IMPOUND.Value := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('TRUST_IMPOUND').Value;

          PayrollList.TAX_IMPOUND.Value := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('TAX_IMPOUND').Value;

          PayrollList.DD_IMPOUND.Value := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('DD_IMPOUND').Value;

          PayrollList.BILLING_IMPOUND.Value := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('BILLING_IMPOUND').Value;
          PayrollList.WC_IMPOUND.Value := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('WC_IMPOUND').Value;

          case cbStatusTo.ItemIndex of
            0: PayrollList.NEW_STATUS.AsString := COMMON_REPORT_STATUS__COMPLETED;
            1: PayrollList.NEW_STATUS.AsString := COMMON_REPORT_STATUS__HOLD;
            2: PayrollList.NEW_STATUS.AsString := COMMON_REPORT_STATUS__DELETED;
            3: PayrollList.NEW_STATUS.AsString := COMMON_REPORT_STATUS__PENDING;
          else
            PayrollList.NEW_STATUS.AsString := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('STATUS').AsString;
          end;

          PayrollList.PR_TYPE.AsString := ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('PAYROLL_TYPE').AsString;
          PayrollList.PR_TYPE_DESCRIPTION.AsString := ReturnDescription(PayrollType_ComboChoices, ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('PAYROLL_TYPE').AsString);

          SentInfo := GetFromFiller(ctx_DataAccess.TEMP_CUSTOM_VIEW.FieldByName('FILLER'), 'SENT');
          if SentInfo <> '' then
          begin
            aDate := Copy(SentInfo, 1, Pos(':', SentInfo) - 1);
            if Length(aDate) = 12 then
              aDate := aDate + '00'; // Old format compatibility
            PayrollList.SENT_DATE.AsDateTime := EncodeDate(StrToInt(Copy(aDate, 1, 4)), StrToInt(Copy(aDate, 5, 2)),
              StrToInt(Copy(aDate, 7, 2))) + EncodeTime(StrToInt(Copy(aDate, 9, 2)), StrToInt(Copy(aDate, 11, 2)), StrToInt(Copy(aDate, 13,
              2)), 0);
            System.Delete(SentInfo, 1, Pos(':', SentInfo));
            PayrollList.SENT_USER.AsString := Copy(SentInfo, 1, Pos(':', SentInfo) - 1);
            System.Delete(SentInfo, 1, Pos(':', SentInfo));
            PayrollList.SENT_FILE.AsString := SentInfo;
          end;

          PayrollList.Post;
        end;
      end;
      ctx_DataAccess.TEMP_CUSTOM_VIEW.Next;
    end;
    PayrollList.First;
    PayrollList.AfterScroll := PayrollListAfterScroll;
  finally
    PayrollList.EnableControls;
  end;
end;

procedure TPROCESS_SB_ACH_REMOTE.eThresholdChange(Sender: TObject);
begin
  inherited;
  mb_AppSettings['CombineACHTreshold'] := eThreshold.Text;
end;

function TPROCESS_SB_ACH_REMOTE.CanClose: Boolean;
begin
  Result := CheckThreshold;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbInterceptClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHIntercept'] := cbIntercept.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.eHeaderChange(Sender: TObject);
begin
  inherited;
  mb_AppSettings['ACHHeader'] := eHeader.Text;
end;

procedure TPROCESS_SB_ACH_REMOTE.eFooterChange(Sender: TObject);
begin
  inherited;
  mb_AppSettings['ACHFooter'] := eFooter.Text;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbBlockSBCreditClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHBlockSBCredit'] := cbBlockSBCredit.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbPayrollHeaderClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHPayrollHeader'] := cbPayrollHeader.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbBlockDebitsClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHBlockDebits'] := cbBlockDebits.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.eCoIdChange(Sender: TObject);
begin
  inherited;
  mb_AppSettings['ACHCoId'] := eCoId.Text;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbDescReversalClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHDescReversal'] := cbDescReversal.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbBankOneClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHBankOne'] := cbBankOne.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbEffectiveDateClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHEffectiveDate'] := cbEffectiveDate.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.eSBEINOverrideChange(Sender: TObject);
begin
  inherited;
  mb_AppSettings['ACHSBEINOverride'] := eSBEINOverride.Text;
end;

function TPROCESS_SB_ACH_REMOTE.GetIfReadOnly: Boolean;
var
  FlagInf: IevGlobalFlagInfo;
begin
  Result := inherited GetIfReadOnly;
  if not Result then
  begin
    FlagInf := mb_GlobalFlagsManager.GetLockedFlag(GF_SB_ACH_PROCESS);
    if Assigned(FlagInf) then
      lACHProcessMsg.Caption := 'ACH is being processed by ' + FlagInf.UserName + '. Please wait.'
    else
      lACHProcessMsg.Caption := '';

    Result := Assigned(FlagInf);
  end;
end;

procedure TPROCESS_SB_ACH_REMOTE.OnGlobalFlagChange(const AResourceInfo: IevGlobalFlagInfo; const AOperation: Char);
var
  RO: Boolean;
begin
  inherited;
  if AResourceInfo.FlagName = GF_SB_ACH_PROCESS then
  begin
    ClearReadOnly;  
    RO := GetIfReadOnly;
    SetReadOnly(RO);
    if FScreenIsReadOnly and not RO then
    begin
      if not FPreprocess then
        GetData;
      FScreenIsReadOnly := RO;
    end;  
  end;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbDisplayCoNbrAndNameClick(
  Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['DisplayCoNbrAndName'] := cbDisplayCoNbrAndName.Checked;
end;

function TPROCESS_SB_ACH_REMOTE.CheckThreshold: boolean;
begin
  Result := True;
  try
    StrToFloat(eThreshold.Text);
  except
    Result := False;
    EvErrMessage('Please enter a valid threshold value when combining ACH transactions');
    eThreshold.SetFocus;
  end;
end;

procedure TPROCESS_SB_ACH_REMOTE.PayrollListAfterScroll(DataSet: TDataSet);
begin
  if PayrollList.IsEmpty then
    Exit;

  if PayrollList.CO_NBR.IsNull then
    Exit;

  if not DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([PayrollList.CL_NBR.Value, PayrollList.CO_NBR.Value]), []) then
    raise Exception.Create('CO_NBR='+PayrollList.CO_NBR.AsString+' not found in TMP_CO');
  PayrollList.TRUST_IMPOUND.ReadOnly := DM_TEMPORARY.TMP_CO.TRUST_SERVICE.AsString = TRUST_SERVICE_NO;
  TrustCombobox.Enabled := not PayrollList.TRUST_IMPOUND.ReadOnly;
  PayrollList.TAX_IMPOUND.ReadOnly := DM_TEMPORARY.TMP_CO.TAX_SERVICE.AsString <> TAX_SERVICE_FULL;
  TaxCombobox.Enabled := not PayrollList.TAX_IMPOUND.ReadOnly;
end;

procedure TPROCESS_SB_ACH_REMOTE.tsFlagsShow(Sender: TObject);
begin
  inherited;
  if PayrollList.UserFilter <> '' then
    EvMessage('The filter should be cleared to ensure that the correct ACH files are selected to be processed', mtWarning, [mbOK]);
end;

procedure TPROCESS_SB_ACH_REMOTE.rgBlockCompanyDDClick(Sender: TObject);
begin
  inherited;
  if rgBlockCompanyDD.Checked and rgBlockCompanyDD.Focused and
     (EvMessage('This is very dangerous and will create an out of balance file blocking the debit transactions for all companies.  Do you wish to change this back to no?',
                 mtWarning, [mbYes, mbIgnore]) <> mrIgnore) then
    rgBlockCompanyDD.Checked := False;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbBlockNegativeLiabilitiesClick(
  Sender: TObject);
begin
  mb_AppSettings.AsBoolean['ACHBlockNegativeLiabilities'] := cbBlockNegativeLiabilities.Checked;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbPostProcessReportClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsString['PostProcessACHReport'] :=
    cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];
end;

procedure TPROCESS_SB_ACH_REMOTE.tsPayrollsEnter(Sender: TObject);
begin
  inherited;
  if not DM_SERVICE_BUREAU.SB.Active then
    DM_SERVICE_BUREAU.SB.DataRequired('');
  DDComboBox.Enabled := DM_SERVICE_BUREAU.SB.MICR_FONT.AsString = 'N';
end;

procedure TPROCESS_SB_ACH_REMOTE.TrustComboBoxDropDown(Sender: TObject);
var
 i : integer;
begin
  inherited;
  if (sender as tevDBComboBox).Text <> 'Internal Check' then
  begin
   for i := (sender as tevDBComboBox).Items.Count - 1  downto 0 do
    if (sender as tevDBComboBox).Items[i] = 'Internal Check' + #9 + PAYMENT_TYPE_INTERNAL_CHECK then
      (sender as tevDBComboBox).Items.Delete(i);
  end;
end;

procedure TPROCESS_SB_ACH_REMOTE.TrustComboBoxCloseUp(
  Sender: TwwDBComboBox; Select: Boolean);
begin
  inherited;
   (sender as tevDBComboBox).Items.Text := Cash_Management_ComboChoices;
   (sender as tevDBComboBox).Refresh;
end;

procedure TPROCESS_SB_ACH_REMOTE.pcACHFileChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  
  if wwdsTMP_CO_PR_ACH2.DataSet.State = dsEdit then
     wwdsTMP_CO_PR_ACH2.DataSet.Post;
end;

procedure TPROCESS_SB_ACH_REMOTE.cbHSAClick(Sender: TObject);
begin
  mb_AppSettings.AsBoolean['ACHHSA'] := cbHSA.Checked;
end;

initialization
  RegisterClass(TPROCESS_SB_ACH_REMOTE);

end.

