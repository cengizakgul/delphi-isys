// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_GLOBAL_CO_BASE_2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc, DBCtrls, StdCtrls, Buttons, Wwdbigrd, Variants,
  Grids, Wwdbgrid, ExtCtrls, ComCtrls,  
  SDataStructure, SPD_EDIT_CO_BASE_2, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictclient, SDataDicttemp, EvUIComponents, EvClientDataSet;

type
  TEDIT_GLOBAL_CO_BASE_2 = class(TEDIT_CO_BASE_2)
    bbtnCopyAll: TevBitBtn;
    bbtnRemoveAll: TevBitBtn;
    SelectedCompanies: TevClientDataSet;
    SelectedCompaniesCL_NBR: TIntegerField;
    SelectedCompaniesCO_NBR: TIntegerField;
    SelectedCompaniesCUSTOM_COMPANY_NUMBER: TStringField;
    SelectedCompaniesNAME: TStringField;
    wwdsSelectedCompanies: TevDataSource;
    Panel4: TevPanel;
    wwDBGrid1: TevDBGrid;
    procedure bbtnCopyAllClick(Sender: TObject);
    procedure bbtnRemoveAllClick(Sender: TObject);
    procedure wwdbgridSelectClientDblClick(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure CopyAllCompanies;
    procedure CopyCompany; virtual;
  public
    { Public declarations }
    procedure Activate; override;
  end;

var
  EDIT_GLOBAL_CO_BASE_2: TEDIT_GLOBAL_CO_BASE_2;

implementation

{$R *.DFM}

procedure TEDIT_GLOBAL_CO_BASE_2.bbtnCopyAllClick(Sender: TObject);
begin
  inherited;
  CopyAllCompanies;
end;

procedure TEDIT_GLOBAL_CO_BASE_2.bbtnRemoveAllClick(Sender: TObject);
begin
  inherited;
  SelectedCompanies.Close;
  SelectedCompanies.CreateDataSet;
end;

procedure TEDIT_GLOBAL_CO_BASE_2.CopyAllCompanies;
begin
  with wwdsList.DataSet do
  begin
    SelectedCompanies.DisableControls;
    DisableControls;
    First;
    while not EOF do
    begin
      CopyCompany;
      Next;
    end;
    EnableControls;
    SelectedCompanies.EnableControls;
  end;
end;

procedure TEDIT_GLOBAL_CO_BASE_2.CopyCompany;
var
  I: Integer;
begin
  with wwdsList.DataSet do
    if not SelectedCompanies.Locate('CL_NBR;CO_NBR', VarArrayOf([FieldByName('CL_NBR').Value, FieldByName('CO_NBR').Value]), []) then
    begin
      SelectedCompanies.Append;
      for I := 0 to SelectedCompanies.FieldCount - 1 do
        SelectedCompanies.Fields[I].Value := FieldByName(SelectedCompanies.Fields[I].FieldName).Value;
      SelectedCompanies.Post;
    end;
end;

procedure TEDIT_GLOBAL_CO_BASE_2.wwdbgridSelectClientDblClick(
  Sender: TObject);
begin
  CopyCompany;
end;

procedure TEDIT_GLOBAL_CO_BASE_2.wwDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if SelectedCompanies.RecordCount > 0 then
    SelectedCompanies.Delete;
end;

procedure TEDIT_GLOBAL_CO_BASE_2.Activate;
begin
  inherited;
  SelectedCompanies.CreateDataSet;
end;

end.
