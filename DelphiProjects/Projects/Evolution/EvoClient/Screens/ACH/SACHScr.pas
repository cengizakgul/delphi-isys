// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SACHScr;

interface

uses
  SPackageEntry, Classes, Db, Wwdatsrc,  
  SFrameEntry, ComCtrls, StdCtrls, Controls, Buttons, ExtCtrls, Menus, SysUtils,
  ImgList, ToolWin, ActnList, EvUtils, EvTypes, SDataStructure, Graphics,
  ISBasicClasses, EvStreamUtils, EvCommonInterfaces, EvMainboard,
  EvUIComponents, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TACHFrm = class(TFramePackageTmpl, IevACHScreens)
  protected
    procedure UserPackageStructure; override;
    function UninitPackage: Boolean; override;
  public
  end;

implementation

var
  ACHFrm: TACHFrm;


{$R *.DFM}

function GetACHScreens: IevACHScreens;
begin
  if not Assigned(ACHFrm) then
    ACHFrm := TACHFrm.Create(nil);
  Result := ACHFrm;
end;


{ TSystemFrm }

function TACHFrm.UninitPackage: Boolean;
begin
  Result := inherited UninitPackage;
end;

procedure TACHFrm.UserPackageStructure;
begin
  AddStructure('\Operatio&ns\Cash Management|200', 'TPROCESS_SB_ACH_REMOTE');
  AddStructure('\Operatio&ns\Prenote ACH|210', 'TPrenote_ACH');
  AddStructure('\Operatio&ns\Manual ACH|220', 'TEDIT_CO_MANUAL_ACH');
  AddStructure('\Operatio&ns\New Client Liab ACH|230', 'TEDIT_NEW_CLIENT_ACH');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetACHScreens, IevACHScreens, 'Screens ACH');

finalization
  ACHFrm.Free;

end.
