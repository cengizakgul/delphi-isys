inherited EDIT_CO_MANUAL_ACH: TEDIT_CO_MANUAL_ACH
  Width = 796
  Height = 565
  inherited Panel1: TevPanel
    Width = 796
    inherited CompanyNameText: TevDBText
      Top = 16
      Width = 401
      Height = 15
    end
    inherited dbtxClientName: TevDBText
      Width = 409
    end
  end
  inherited PageControl1: TevPageControl
    Width = 796
    Height = 532
    ActivePage = tbManualACH
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited Panel2: TevPanel
        Width = 788
        Height = 463
        inherited Splitter1: TevSplitter
          Height = 461
        end
        inherited wwdbgridSelectClient: TevDBGrid
          Height = 461
          IniAttributes.SectionName = 'TEDIT_CO_MANUAL_ACH\wwdbgridSelectClient'
        end
        inherited pnlSubbrowse: TevPanel
          Width = 463
          Height = 461
          object wwDBGrid1: TevDBGrid
            Left = 0
            Top = 0
            Width = 463
            Height = 461
            DisableThemesInTitle = False
            Selected.Strings = (
              'DEBIT_DATE'#9'12'#9'Debit Date'
              'CREDIT_DATE'#9'12'#9'Credit Date'
              'AMOUNT'#9'20'#9'Amount'
              'STATUS'#9'6'#9'Status')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_CO_MANUAL_ACH\wwDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
      inherited Panel3: TevPanel
        Width = 788
        inherited BitBtn1: TevBitBtn
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
            8888D8888888888888888EEEEEEEE668888888888888888888888EE66666666F
            FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
            FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
            FFF88DDDDDDDD88DDDD88EEEEEEEE6F777F88888888888DDDDD8877777767F7F
            C7F88DDDDDD8DDD8FD8D8FFFFFF6777FC78D8DDDDDD8DDD8F8DD88888886FC8F
            C8FC888DD88D8FD8FD8FDDDEE8D6DFCDDFCDDDD88DD8D8FDD8FDDDDEEEE6DDDD
            DDDDDDD88888DDDDDDDDDDDDEEE6DFCFCFCDDDDD8888D8F8F8FDDDDDDDE6FCDF
            CDFCDDDDDD8D8FD8FD8FDDDDDDD8DDDFCDDDDDDDDDD8DDD8FDDD}
        end
        object wwDBLookupCombo1: TevDBLookupCombo
          Left = 280
          Top = 8
          Width = 265
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME_DESCRIPTION'#9'40'#9'Name'#9'F'
            'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'Bank Account'#9'F')
          LookupTable = ACHSBBankAccounts
          LookupField = 'SB_BANK_ACCOUNTS_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = wwDBLookupCombo1Change
        end
      end
    end
    object tbManualACH: TTabSheet
      Caption = 'Manual ACH'
      ImageIndex = 1
      object Label2: TevLabel
        Left = 16
        Top = 144
        Width = 60
        Height = 16
        Caption = '~Debit Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TevLabel
        Left = 16
        Top = 96
        Width = 62
        Height = 16
        Caption = '~Credit Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TevLabel
        Left = 16
        Top = 192
        Width = 45
        Height = 16
        Caption = '~Amount'
        FocusControl = DBEdit5
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TevLabel
        Left = 16
        Top = 282
        Width = 37
        Height = 16
        Caption = '~Notes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object SpeedButton1: TevSpeedButton
        Left = 112
        Top = 32
        Width = 23
        Height = 22
        HideHint = True
        AutoSize = False
        OnClick = SpeedButton1Click
        NumGlyphs = 2
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
          8888D8888888888888888EEEEEEEE668888888888888888888888EE66666666F
          FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
          FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
          FFF88DDDDDDDD88DDDD88EEEEEEEE6F777F88888888888DDDDD8877777767F7F
          C7F88DDDDDD8DDD8FD8D8FFFFFF6777FC78D8DDDDDD8DDD8F8DD88888886FC8F
          C8FC888DD88D8FD8FD8FDDDEE8D6DFCDDFCDDDD88DD8D8FDD8FDDDDEEEE6DDDD
          DDDDDDD88888DDDDDDDDDDDDEEE6DFCFCFCDDDDD8888D8F8F8FDDDDDDDE6FCDF
          CDFCDDDDDD8D8FD8FD8FDDDDDDD8DDDFCDDDDDDDDDD8DDD8FDDD}
        ParentColor = False
        ShortCut = 0
      end
      object Label1: TevLabel
        Left = 16
        Top = 16
        Width = 54
        Height = 13
        Caption = 'Company #'
      end
      object Label4: TevLabel
        Left = 16
        Top = 410
        Width = 609
        Height = 17
        AutoSize = False
        Caption = 
          'Use <Insert> to insert a new record; <F10> to save; <Del> to can' +
          'cel; <F3> to switch to a new company'
        WordWrap = True
      end
      object DBEdit5: TevDBEdit
        Left = 16
        Top = 208
        Width = 121
        Height = 21
        DataField = 'AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object GroupBox1: TevGroupBox
        Left = 184
        Top = 16
        Width = 193
        Height = 225
        Caption = 'From:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        object Label8: TevLabel
          Left = 16
          Top = 24
          Width = 44
          Height = 13
          Caption = 'Company'
          Visible = False
        end
        object Label9: TevLabel
          Left = 16
          Top = 72
          Width = 46
          Height = 13
          Caption = 'Employee'
        end
        object Label10: TevLabel
          Left = 16
          Top = 168
          Width = 85
          Height = 13
          Caption = 'SB Bank Account'
        end
        object Label11: TevLabel
          Left = 16
          Top = 24
          Width = 97
          Height = 13
          Caption = 'Client Bank Account'
        end
        object Label12: TevLabel
          Left = 16
          Top = 120
          Width = 91
          Height = 13
          Caption = 'Employee Dir. Dep.'
        end
        object wwlcEmployeeFrom: TevDBLookupCombo
          Left = 16
          Top = 88
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_EMPLOYEE_NUMBER'#9'10'#9'CUSTOM_EMPLOYEE_NUMBER'
            'Employee_Name_Calculate'#9'50'#9'Employee_Name_Calculate')
          DataField = 'FROM_EE_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_EE.EE
          LookupField = 'EE_NBR'
          Style = csDropDownList
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnCloseUp = wwlcEmployeeFromCloseUp
        end
        object wwlcCompanyFrom: TevDBLookupCombo
          Left = 16
          Top = 40
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_COMPANY_NUMBER'#9'10'#9'CUSTOM_COMPANY_NUMBER'
            'NAME'#9'40'#9'NAME')
          DataField = 'FROM_CO_NBR'
          DataSource = wwdsDetail
          LookupField = 'CO_NBR'
          Style = csDropDownList
          TabOrder = 0
          Visible = False
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object wwlcSBAcctFrom: TevDBLookupCombo
          Left = 16
          Top = 184
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
            'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
            'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
          DataField = 'FROM_SB_BANK_ACCOUNTS_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_SB_BANK_ACCOUNTS.SB_BANK_ACCOUNTS
          LookupField = 'SB_BANK_ACCOUNTS_NBR'
          Style = csDropDownList
          TabOrder = 4
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object wwlcCLAcctFrom: TevDBLookupCombo
          Left = 16
          Top = 40
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
            'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
            'Bank_Name'#9'40'#9'Bank_Name'
            'Description'#9'20'#9'Description')
          DataField = 'FROM_CL_BANK_ACCOUNT_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
          LookupField = 'CL_BANK_ACCOUNT_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object wwlcEEDirDepFrom: TevDBLookupCombo
          Left = 16
          Top = 136
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'EE_BANK_ACCOUNT_NUMBER'#9'20'#9'EE_BANK_ACCOUNT_NUMBER'
            'EE_ABA_NUMBER'#9'10'#9'EE_ABA_NUMBER'
            'EE_BANK_ACCOUNT_TYPE'#9'1'#9'EE_BANK_ACCOUNT_TYPE')
          DataField = 'FROM_EE_DIRECT_DEPOSIT_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_EE_DIRECT_DEPOSIT.EE_DIRECT_DEPOSIT
          LookupField = 'EE_DIRECT_DEPOSIT_NBR'
          Style = csDropDownList
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnDropDown = wwlcEEDirDepFromDropDown
          OnCloseUp = wwlcEEDirDepCloseUp
        end
      end
      object GroupBox2: TevGroupBox
        Left = 424
        Top = 16
        Width = 193
        Height = 269
        Caption = 'To:'
        TabOrder = 7
        object Label13: TevLabel
          Left = 16
          Top = 24
          Width = 44
          Height = 13
          Caption = 'Company'
          Visible = False
        end
        object Label14: TevLabel
          Left = 16
          Top = 72
          Width = 46
          Height = 13
          Caption = 'Employee'
        end
        object Label15: TevLabel
          Left = 16
          Top = 217
          Width = 85
          Height = 13
          Caption = 'SB Bank Account'
        end
        object Label16: TevLabel
          Left = 16
          Top = 24
          Width = 97
          Height = 13
          Caption = 'Client Bank Account'
        end
        object Label17: TevLabel
          Left = 16
          Top = 120
          Width = 91
          Height = 13
          Caption = 'Employee Dir. Dep.'
        end
        object evLabel3: TevLabel
          Left = 16
          Top = 168
          Width = 90
          Height = 13
          Caption = 'Child Support Case'
        end
        object wwlcEmployeeTo: TevDBLookupCombo
          Left = 16
          Top = 88
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_EMPLOYEE_NUMBER'#9'10'#9'CUSTOM_EMPLOYEE_NUMBER'
            'Employee_Name_Calculate'#9'50'#9'Employee_Name_Calculate')
          DataField = 'TO_EE_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_EE.EE
          LookupField = 'EE_NBR'
          Style = csDropDownList
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnCloseUp = wwlcEmployeeToCloseUp
        end
        object wwlcCompanyTo: TevDBLookupCombo
          Left = 16
          Top = 40
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_COMPANY_NUMBER'#9'10'#9'CUSTOM_COMPANY_NUMBER'
            'NAME'#9'40'#9'NAME')
          DataField = 'TO_CO_NBR'
          DataSource = wwdsDetail
          LookupField = 'CO_NBR'
          Style = csDropDownList
          TabOrder = 0
          Visible = False
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object wwlcSBAcctTo: TevDBLookupCombo
          Left = 16
          Top = 233
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
            'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
            'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
          DataField = 'TO_SB_BANK_ACCOUNTS_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_SB_BANK_ACCOUNTS.SB_BANK_ACCOUNTS
          LookupField = 'SB_BANK_ACCOUNTS_NBR'
          Style = csDropDownList
          TabOrder = 5
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object wwlcCLAcctTo: TevDBLookupCombo
          Left = 16
          Top = 40
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
            'BANK_ACCOUNT_TYPE'#9'1'#9'BANK_ACCOUNT_TYPE'
            'Bank_Name'#9'40'#9'Bank_Name'
            'Description'#9'20'#9'Description')
          DataField = 'TO_CL_BANK_ACCOUNT_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
          LookupField = 'CL_BANK_ACCOUNT_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object wwlcEEDirDepTo: TevDBLookupCombo
          Left = 16
          Top = 136
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'EE_BANK_ACCOUNT_NUMBER'#9'20'#9'EE_BANK_ACCOUNT_NUMBER'
            'EE_ABA_NUMBER'#9'10'#9'EE_ABA_NUMBER'
            'EE_BANK_ACCOUNT_TYPE'#9'1'#9'EE_BANK_ACCOUNT_TYPE')
          DataField = 'TO_EE_DIRECT_DEPOSIT_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_EE_DIRECT_DEPOSIT.EE_DIRECT_DEPOSIT
          LookupField = 'EE_DIRECT_DEPOSIT_NBR'
          Style = csDropDownList
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnDropDown = wwlcEEDirDepToDropDown
          OnCloseUp = wwlcEEDirDepCloseUp
        end
        object wwlcChildSupportCases: TevDBLookupCombo
          Left = 16
          Top = 184
          Width = 161
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_CASE_NUMBER'#9'32'#9'Custom case number'#9'F')
          DataField = 'TO_EE_CHILD_SUPPORT_CASES_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_EE_CHILD_SUPPORT_CASES.EE_CHILD_SUPPORT_CASES
          LookupField = 'EE_CHILD_SUPPORT_CASES_NBR'
          Style = csDropDownList
          TabOrder = 4
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = wwlcChildSupportCasesChange
          OnDropDown = wwlcChildSupportCasesDropDown
          OnCloseUp = wwlcChildSupportCasesCloseUp
        end
      end
      object DBMemo1: TEvDBMemo
        Left = 16
        Top = 298
        Width = 601
        Height = 89
        DataField = 'PURPOSE_NOTES'
        DataSource = wwdsDetail
        TabOrder = 5
      end
      object wwDBDateTimePicker1: TevDBDateTimePicker
        Left = 16
        Top = 160
        Width = 121
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'DEBIT_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 3
      end
      object wwDBDateTimePicker2: TevDBDateTimePicker
        Left = 16
        Top = 112
        Width = 121
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'CREDIT_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 2
      end
      object DBCheckBox1: TDBCheckBox
        Left = 16
        Top = 72
        Width = 81
        Height = 17
        Caption = 'Put on Hold'
        DataField = 'STATUS'
        DataSource = wwdsDetail
        TabOrder = 1
        ValueChecked = 'H'
        ValueUnchecked = 'P'
      end
      object Edit1: TevEdit
        Left = 16
        Top = 32
        Width = 97
        Height = 21
        TabOrder = 0
        OnKeyDown = Edit1KeyDown
      end
    end
    object tbCreateFile: TTabSheet
      Caption = 'Create File'
      ImageIndex = 2
      object Gauge1: TGauge
        Left = 0
        Top = 487
        Width = 788
        Height = 17
        Align = alBottom
        ForeColor = clBlue
        ParentShowHint = False
        Progress = 0
        ShowHint = True
        Visible = False
      end
      object Panel4: TevPanel
        Left = 0
        Top = 0
        Width = 788
        Height = 92
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Bevel1: TEvBevel
          Left = 0
          Top = 0
          Width = 161
          Height = 92
          Align = alLeft
        end
        object Bevel2: TEvBevel
          Left = 161
          Top = 0
          Width = 627
          Height = 92
          Align = alClient
        end
        object evLabel1: TevLabel
          Left = 365
          Top = 72
          Width = 93
          Height = 13
          Caption = 'Check Recon Type'
        end
        object evLabel2: TevLabel
          Left = 178
          Top = 71
          Width = 41
          Height = 13
          Caption = 'Combine'
        end
        object evLabel9: TevLabel
          Left = 348
          Top = 40
          Width = 116
          Height = 13
          Caption = 'Post process ACH report'
          FocusControl = cbPostProcessReport
        end
        object RadioGroup1: TevRadioGroup
          Left = 8
          Top = 1
          Width = 137
          Height = 33
          Caption = 'Show'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Pending'
            'On-Hold')
          TabOrder = 0
          OnClick = RadioGroup1Click
        end
        object btnCreateFile: TevBitBtn
          Left = 327
          Top = 8
          Width = 137
          Height = 25
          Caption = 'Process'
          TabOrder = 7
          OnClick = btnCreateFileClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD0888
            8000DDDDDDDDFFFFFFFFDDDDDDD027277220DDDDDDDF8D8DD88FD88888822727
            7220DFFFFFD88D8DD88FFFFFFFF227777220888888D88DDDD88FFFFFFFF22222
            2220888888D88888888FFF444482FFFFFF2088DDDDD8DDDDDD8FFFFFFFF2FFF7
            FF20888888D8DDDDDD8FFF444482FF707F2088DDDDD8DDDFDD8FFFFFFFF2220C
            022D888888D888F8F88DFF444488F0CCC0DD88DDDDDDDF888FDDFFFFFFFF0CCC
            CC0D8888888DF88888FDFF444488F8CCCDDD88DDDDDDD8888DDDFFFFFFF000CC
            CDDD888888DFFF888DDDFF44448CCCCCCDDD88DDDDD888888DDDFFFFFFFCCCCC
            DDDD888888D88888DDDDFFFFFFFFFDDDDDDD888888DDDDDDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object btnPreprocess: TevBitBtn
          Left = 176
          Top = 8
          Width = 137
          Height = 25
          Caption = 'Preprocess'
          TabOrder = 2
          OnClick = btnPreprocessClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD0DD0DDDDD
            DDDDDDDFDDFDDDDDDDDDDD0E00E0DDDDDDDDDDF8FF8FDDDDDDDDDDEEEEEE8888
            8888DD888888FFFFFFFFD00EEEE007FFFFF8DFF8888FFD88888FDEEE07EEE7FF
            FFF8D888FD888D88888FDEEE00EEE74444F8D888FF888DDDDD8FDD0EEEE07FFF
            FFF8DDF8888FD888888FDDEEEEEE744444F8DD888888DDDDDD8FD03E77E7FFFF
            FFF8DFD8DD8D8888888FDBB33F74444444F8D88DD8DDDDDDDD8F00BB3FFFFFFF
            FFF8FF88D8888888888FBBB0DFF4444444F8888FD88DDDDDDD8FBBB00FFFFFFF
            FFF8888FF8888888888FD0BB3FF4444444F8DF88D88DDDDDDD8FDBBB3FFFFFFF
            FFF8D888D8888888888FDDBD3FFFFFFFFFFDDD8DD8888888888D}
          NumGlyphs = 2
          Margin = 0
        end
        object BitBtn7: TevBitBtn
          Left = 8
          Top = 40
          Width = 137
          Height = 25
          Caption = 'Put On-Hold'
          TabOrder = 1
          OnClick = BitBtn7Click
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD8DDDDDDD
            DDD8DDDDFDDDDDDDDDDDDDDD38DDDDDD8800DDDD8FDDDDDDDD88DDDD338DDDD0
            0004DDDD88FDDDD8888888883B38DD077704FFFF888FDD8DDD8833333BB3807F
            F70488888888F8DDDD883BBBBBBB30FFF8048888888888DDD8883BBBBBBBB0F0
            70008888888888D8D8883BBBBBBB30F0F08D8888888888D8D8DD33333BB3D0F0
            70DD88888888D8D8D8DDDDDD3B3DDD0770DDDDDD888DDD8DD8DDDDDD33DDDD0F
            08DDDDDD88DDDD8D8DDDDDDD3DDDDD0F0DDDDDDD8DDDDD8D8DDDDDDDDDDDDD0F
            0DDDDDDDDDDDDD8D8DDDDDDDDDDDDDD0DDDDDDDDDDDDDDD8DDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object cbShowAll: TevCheckBox
          Left = 179
          Top = 35
          Width = 62
          Height = 17
          Caption = 'Show All'
          TabOrder = 4
        end
        object cbUSESBEIN: TevCheckBox
          Left = 245
          Top = 36
          Width = 80
          Height = 17
          Caption = 'Use SB EIN'
          TabOrder = 3
          OnClick = cbUSESBEINClick
        end
        object cbBalanceBatches: TevCheckBox
          Left = 245
          Top = 51
          Width = 110
          Height = 17
          Caption = 'Balance batches'
          TabOrder = 5
          OnClick = cbBalanceBatchesClick
        end
        object cbCTS: TevCheckBox
          Left = 179
          Top = 50
          Width = 62
          Height = 17
          Caption = 'CTS'
          TabOrder = 6
          OnClick = cbCTSClick
        end
        object cbCheckReconType: TevDBComboBox
          Left = 465
          Top = 68
          Width = 145
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DropDownCount = 8
          ItemHeight = 13
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 8
          UnboundDataType = wwDefault
        end
        object cbCombine: TevComboBox
          Left = 224
          Top = 67
          Width = 121
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 9
          OnClick = cbCombineClick
          Items.Strings = (
            'Do not Combine'
            'Company Trans Only'
            'Company and SB Trans')
        end
        object cbPostProcessReport: TevComboBox
          Left = 472
          Top = 38
          Width = 209
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 10
          OnClick = cbPostProcessReportClick
          Items.Strings = (
            'Do not Combine'
            'Company Trans Only'
            'Company and SB Trans')
        end
      end
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 92
        Width = 788
        Height = 395
        DisableThemesInTitle = False
        Selected.Strings = (
          'CompanyNumber'#9'20'#9'Company Number'
          'CompanyName'#9'40'#9'Company Name'
          'DEBIT_DATE'#9'12'#9'Debit Date'
          'CREDIT_DATE'#9'12'#9'Credit Date'
          'AMOUNT'#9'15'#9'Amount')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_CO_MANUAL_ACH\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsTMP_MANUAL_ACH
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 424
    Top = 32
  end
  inherited wwdsDetail: TevDataSource
    Left = 584
    Top = 32
  end
  inherited wwdsList: TevDataSource
    Left = 196
    Top = 1
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 291
    Top = 2
  end
  inherited dsCL: TevDataSource
    Left = 264
    Top = 0
  end
  object wwdsSubMaster: TevDataSource
    Left = 456
    Top = 32
  end
  object wwdsSubMaster2: TevDataSource
    Left = 488
    Top = 32
  end
  object wwdsData: TevDataSource
    Left = 612
    Top = 32
  end
  object wwdsSubMaster3: TevDataSource
    Left = 520
    Top = 32
  end
  object wwdsSubMaster4: TevDataSource
    Left = 552
    Top = 32
  end
  object wwdsTMP_MANUAL_ACH: TevDataSource
    DataSet = DM_TMP_CO_MANUAL_ACH.TMP_CO_MANUAL_ACH
    Left = 116
    Top = 265
  end
  object ACHSBBankAccounts: TevClientDataSet
    Left = 652
    Top = 9
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 328
    Top = 8
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 476
    Top = 9
  end
  object ActionList1: TActionList
    Left = 60
    Top = 257
    object actInsert: TAction
      Caption = 'actInsert'
      ShortCut = 45
      OnExecute = actInsertExecute
    end
    object actUpdate: TAction
      Caption = 'actUpdate'
      ShortCut = 121
      OnExecute = actUpdateExecute
    end
    object actCancel: TAction
      Caption = 'actCancel'
      ShortCut = 46
      OnExecute = actCancelExecute
    end
    object actSwitch: TAction
      Caption = 'actSwitch'
      ShortCut = 114
      OnExecute = actSwitchExecute
    end
    object actDelete: TAction
      Caption = 'actDelete'
      ShortCut = 16430
      OnExecute = actDeleteExecute
    end
  end
end
