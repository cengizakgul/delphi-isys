inherited Prenote_ACH: TPrenote_ACH
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited Panel2: TevPanel
        Top = 65
        Height = 183
        inherited Splitter1: TevSplitter
          Height = 181
        end
        inherited wwdbgridSelectClient: TevDBGrid
          Height = 181
          IniAttributes.SectionName = 'TPrenote_ACH\wwdbgridSelectClient'
        end
        inherited pnlSubbrowse: TevPanel
          Height = 181
          inherited Panel4: TevPanel
            Height = 181
            inherited wwDBGrid1: TevDBGrid
              Height = 179
              IniAttributes.SectionName = 'TPrenote_ACH\wwDBGrid1'
            end
          end
        end
      end
      inherited Panel3: TevPanel
        Height = 65
        inherited BitBtn1: TevBitBtn
          Left = -3
        end
        inherited bbtnCopyAll: TevBitBtn
          Left = 280
          TabOrder = 1
        end
        inherited bbtnRemoveAll: TevBitBtn
          Left = 376
          TabOrder = 2
        end
        object bbtnCreate: TevBitBtn
          Left = 480
          Top = 8
          Width = 155
          Height = 25
          Caption = 'Create Prenote ACH'
          TabOrder = 3
          OnClick = bbtnCreateClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD0888
            8000DDDDDDDDFFFFFFFFDDDDDDD027277220DDDDDDDF8D8DD88FD88888822727
            7220DFFFFFD88D8DD88FFFFFFFF227777220888888D88DDDD88FFFFFFFF22222
            2220888888D88888888FFF444482FFFFFF2088DDDDD8DDDDDD8FFFFFFFF2FFF7
            FF20888888D8DDDDDD8FFF444482FF707F2088DDDDD8DDDFDD8FFFFFFFF2220C
            022D888888D888F8F88DFF444488F0CCC0DD88DDDDDDDF888FDDFFFFFFFF0CCC
            CC0D8888888DF88888FDFF444488F8CCCDDD88DDDDDDD8888DDDFFFFFFF000CC
            CDDD888888DFFF888DDDFF44448CCCCCCDDD88DDDDD888888DDDFFFFFFFCCCCC
            DDDD888888D88888DDDDFFFFFFFFFDDDDDDD888888DDDDDDDDDD}
          NumGlyphs = 2
        end
        object wwDBLookupCombo1: TevDBLookupCombo
          Left = 0
          Top = 8
          Width = 265
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME_DESCRIPTION'#9'40'#9'Name'#9'F'
            'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'Bank Account'#9'F')
          LookupTable = DM_SB_BANK_ACCOUNTS.SB_BANK_ACCOUNTS
          LookupField = 'SB_BANK_ACCOUNTS_NBR'
          Style = csDropDownList
          TabOrder = 4
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = wwDBLookupCombo1Change
        end
      end
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 376
    Top = 59
  end
  object Timer1: TTimer
    Interval = 3000
    OnTimer = Timer1Timer
    Left = 416
    Top = 64
  end
end
