inherited EDIT_CO_BASE_2: TEDIT_CO_BASE_2
  object Panel1: TevPanel [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label5: TevLabel
      Left = 8
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Company'
    end
    object DBText1: TevDBText
      Left = 64
      Top = 14
      Width = 65
      Height = 17
      DataField = 'CUSTOM_COMPANY_NUMBER'
      DataSource = wwdsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CompanyNameText: TevDBText
      Left = 136
      Top = 14
      Width = 241
      Height = 17
      DataField = 'NAME'
      DataSource = wwdsMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lablClient: TevLabel
      Left = 8
      Top = 0
      Width = 26
      Height = 13
      Caption = 'Client'
    end
    object dbtxClientNbr: TevDBText
      Left = 64
      Top = 0
      Width = 65
      Height = 15
      DataField = 'CUSTOM_CLIENT_NUMBER'
      DataSource = dsCL
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dbtxClientName: TevDBText
      Left = 136
      Top = 0
      Width = 241
      Height = 15
      DataField = 'NAME'
      DataSource = dsCL
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object PageControl1: TevPageControl [1]
    Left = 0
    Top = 33
    Width = 435
    Height = 233
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    OnChanging = PageControl1Changing
    object TabSheet1: TTabSheet
      Caption = 'Browse'
      object Panel2: TevPanel
        Left = 0
        Top = 41
        Width = 427
        Height = 164
        Align = alClient
        TabOrder = 0
        object Splitter1: TevSplitter
          Left = 321
          Top = 1
          Height = 162
        end
        object wwdbgridSelectClient: TevDBGrid
          Left = 1
          Top = 1
          Width = 320
          Height = 162
          DisableThemesInTitle = False
          Selected.Strings = (
            'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
            'NAME'#9'25'#9'Name'#9'F'
            'FEIN'#9'9'#9'Fein'#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_CO_BASE_2\wwdbgridSelectClient'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsList
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          OnDblClick = wwdbgridSelectClientDblClick
          OnKeyDown = wwdbgridSelectClientKeyDown
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object pnlSubbrowse: TevPanel
          Left = 324
          Top = 1
          Width = 102
          Height = 162
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
        end
      end
      object Panel3: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn1: TevBitBtn
          Left = 5
          Top = 8
          Width = 121
          Height = 25
          Caption = 'Open company'
          TabOrder = 0
          OnClick = BitBtn1Click
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
            B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
            B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
            0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
            55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
            55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
            55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
            5555575FFF755555555557000075555555555577775555555555}
          NumGlyphs = 2
          ParentColor = False
          Margin = 0
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_CO.CO
    Left = 144
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    OnDataChange = wwdsListDataChange
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 315
    Top = 18
  end
  object dsCL: TevDataSource
    DataSet = DM_CL.CL
    Left = 252
    Top = 18
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 366
    Top = 18
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 408
    Top = 21
  end
end
