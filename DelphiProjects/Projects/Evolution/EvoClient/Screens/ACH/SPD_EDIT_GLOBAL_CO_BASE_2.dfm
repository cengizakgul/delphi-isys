inherited EDIT_GLOBAL_CO_BASE_2: TEDIT_GLOBAL_CO_BASE_2
  inherited Panel1: TevPanel
    Height = 1
  end
  inherited PageControl1: TevPageControl
    Top = 1
    Height = 269
    inherited TabSheet1: TTabSheet
      inherited Panel2: TevPanel
        Height = 200
        inherited Splitter1: TevSplitter
          Height = 198
        end
        inherited wwdbgridSelectClient: TevDBGrid
          Height = 198
          IniAttributes.SectionName = 'TEDIT_GLOBAL_CO_BASE_2\wwdbgridSelectClient'
        end
        inherited pnlSubbrowse: TevPanel
          Height = 198
          object Panel4: TevPanel
            Left = 0
            Top = 0
            Width = 110
            Height = 198
            Align = alClient
            TabOrder = 0
            object wwDBGrid1: TevDBGrid
              Left = 1
              Top = 1
              Width = 108
              Height = 196
              Selected.Strings = (
                'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
                'NAME'#9'40'#9'Name'#9)
              IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
              IniAttributes.SectionName = 'TEDIT_GLOBAL_CO_BASE_2\wwDBGrid1'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = wwdsSelectedCompanies
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = wwDBGrid1DblClick
              PaintOptions.AlternatingRowColor = clCream
            end
          end
        end
      end
      inherited Panel3: TevPanel
        inherited BitBtn1: TevBitBtn
          Visible = False
        end
        object bbtnCopyAll: TevBitBtn
          Left = 0
          Top = 8
          Width = 89
          Height = 25
          Caption = 'Copy All'
          TabOrder = 2
          OnClick = bbtnCopyAllClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD4444444
            4444DDDDD88888888888DDDDD4FFFFFFFFF4DDDDD8DDDDDDDDD8D8888477777F
            FFF4DFFFFFFFFFFDDDD800000000007FFFF48888888888FDDDD80FFFF7FFF07F
            FFF48DDDDDDDD8FDDDD80F444444F007FFF48D888888D88FDDD80FFFF7FFF0F0
            7FF48DDDDDDDD8D8FDD80F444444FF0F07F48D888888DD8D8FD80FFFF7FFFFF0
            7FF48DDDDDDDDDD8DDD80F444444FFFF07F48D888888DDDD8DD80FFFF7FFFFF0
            7FF48DDDDDDDDDD8DDD80F444444FF07FFF48D888888DD8DDDD80FFFF7FFF07F
            FFF48DDDDDDDD8FDDDD80F444444704444448D888888D8F888880FFFFFFFF08D
            DDDD8DDDDDDDD8FDDDDD0000000000DDDDDD8888888888DDDDDD}
          NumGlyphs = 2
        end
        object bbtnRemoveAll: TevBitBtn
          Left = 96
          Top = 8
          Width = 89
          Height = 25
          Caption = 'Remove All'
          TabOrder = 1
          OnClick = bbtnRemoveAllClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD8888888
            8888DDDDDFFFFFFFFFFFDDDD000000000008DDDD88888888888FDDDD0FFFFFFF
            FF08DDDD8DDDDDDDDD8FDDDD0FFFFFFFFF08DDDD8DDDDDDDDD8F88DD0FF88FFF
            FF08FFDD8DDFFDDDDD8F998D0F899FFFFF0888FD8DF88DDDDD8F999808999FFF
            FF08888F8F888DDDDD8FD9998999FFFFFF08D888F888DDDDDD8FDD99999FFFFF
            FF08DD88888DDDDDDD8FDD89998FFFFFFF08DDF888FDDDDDDD8FD8999998FFFF
            FF08DF88888FDDDDDD8F899909998FFFFF08F888D888FDDDDD8F999D0F999FFF
            FF08888D8D888DDDDD8F99DD0FF99FFFFF0888DD8DD88DDDDD8FDDDD0FFFFFFF
            FF08DDDD8DDDDDDDDD8FDDDD00000000000DDDDD88888888888D}
          NumGlyphs = 2
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 65528
  end
  inherited wwdsDetail: TevDataSource
    Top = 65528
  end
  inherited wwdsList: TevDataSource
    Top = 65529
  end
  inherited dsCL: TevDataSource
    Top = 65528
  end
  object SelectedCompanies: TevClientDataSet
    Left = 308
    Top = 65529
    object SelectedCompaniesCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object SelectedCompaniesCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object SelectedCompaniesCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Company Nbr'
      DisplayWidth = 10
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object SelectedCompaniesNAME: TStringField
      DisplayLabel = 'Company Name'
      FieldName = 'NAME'
      Size = 40
    end
  end
  object wwdsSelectedCompanies: TevDataSource
    DataSet = SelectedCompanies
    Left = 344
    Top = 65528
  end
end
