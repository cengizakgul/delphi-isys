inherited EDIT_NEW_CLIENT_ACH: TEDIT_NEW_CLIENT_ACH
  OnResize = FrameResize
  object evPanel1: TevPanel [0]
    Left = 0
    Top = 225
    Width = 435
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object evLabel3: TevLabel
      Left = 9
      Top = 11
      Width = 166
      Height = 20
      Caption = 'Select liabilities and '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object evBitBtn1: TevBitBtn
      Left = 177
      Top = 9
      Width = 89
      Height = 25
      Caption = 'proceed'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = evBitBtn1Click
      Margin = 0
    end
  end
  object evPanel3: TevPanel [1]
    Left = 0
    Top = 0
    Width = 435
    Height = 232
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object evPanel2: TevPanel
      Left = 0
      Top = 0
      Width = 435
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object evLabel1: TevLabel
        Left = 6
        Top = 6
        Width = 35
        Height = 13
        Caption = 'Federal'
      end
      object evLabel2: TevLabel
        Left = 326
        Top = 6
        Width = 18
        Height = 13
        Caption = 'SUI'
      end
    end
    object evPanel5: TevPanel
      Left = 0
      Top = 25
      Width = 435
      Height = 207
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object evDBGrid1: TevDBCheckGrid
        Left = 0
        Top = 0
        Width = 320
        Height = 207
        DisableThemesInTitle = False
        Selected.Strings = (
          'CO_NUMBER'#9'6'#9'Co Number'#9'F'#9
          'CO_NAME'#9'20'#9'Co Name'#9'F'#9
          'TYPE'#9'8'#9'Type'#9'F'#9
          'AMOUNT'#9'10'#9'Amount'#9'F'#9
          'DUE_DATE'#9'10'#9'Due Date'#9'F'#9
          'CHECK_DATE'#9'10'#9'Check Date'#9'F'#9)
        IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
        IniAttributes.SectionName = 'TEDIT_NEW_CLIENT_ACH\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = dsFUI
        MultiSelectOptions = [msoShiftSelect]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = True
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object evDBGrid2: TevDBCheckGrid
        Left = 320
        Top = 0
        Width = 115
        Height = 207
        DisableThemesInTitle = False
        Selected.Strings = (
          'CO_NUMBER'#9'6'#9'Co Number'#9'F'#9
          'CO_NAME'#9'20'#9'Co Name'#9'F'#9
          'SUI_NAME'#9'20'#9'Sui Name'#9'F'#9
          'AMOUNT'#9'10'#9'Amount'#9'F'#9
          'DUE_DATE'#9'10'#9'Due Date'#9'F'#9
          'CHECK_DATE'#9'10'#9'Check Date'#9'F'#9)
        IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
        IniAttributes.SectionName = 'TEDIT_NEW_CLIENT_ACH\evDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsSUI
        MultiSelectOptions = [msoShiftSelect]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = True
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
  end
  object evPanel4: TevPanel [2]
    Left = 0
    Top = 232
    Width = 435
    Height = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object evPanel6: TevPanel
      Left = 0
      Top = 0
      Width = 435
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object evLabel4: TevLabel
        Left = 6
        Top = 6
        Width = 25
        Height = 13
        Caption = 'State'
      end
      object evLabel5: TevLabel
        Left = 326
        Top = 6
        Width = 26
        Height = 13
        Caption = 'Local'
      end
    end
    object evPanel7: TevPanel
      Left = 0
      Top = 25
      Width = 435
      Height = 367
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object evDBCheckGrid1: TevDBCheckGrid
        Left = 0
        Top = 0
        Width = 320
        Height = 367
        DisableThemesInTitle = False
        Selected.Strings = (
          'CO_NUMBER'#9'6'#9'Co Number'#9'F'#9
          'CO_NAME'#9'20'#9'Co Name'#9'F'#9
          'STATE'#9'3'#9'State'#9'F'#9
          'TYPE'#9'8'#9'Type'#9'F'#9
          'AMOUNT'#9'10'#9'Amount'#9'F'#9
          'DUE_DATE'#9'10'#9'Due Date'#9'F'#9
          'CHECK_DATE'#9'10'#9'Check Date'#9'F'#9)
        IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
        IniAttributes.SectionName = 'TEDIT_NEW_CLIENT_ACH\evDBCheckGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = dsState
        MultiSelectOptions = [msoShiftSelect]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = True
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object evDBCheckGrid2: TevDBCheckGrid
        Left = 320
        Top = 0
        Width = 115
        Height = 367
        DisableThemesInTitle = False
        Selected.Strings = (
          'CO_NUMBER'#9'6'#9'Co Number'#9'F'#9
          'CO_NAME'#9'20'#9'Co Name'#9'F'#9
          'LOCAL_NAME'#9'20'#9'Local'#9'F'#9
          'AMOUNT'#9'10'#9'Amount'#9'F'#9
          'DUE_DATE'#9'10'#9'Due Date'#9'F'#9
          'CHECK_DATE'#9'10'#9'Check Date'#9'F'#9)
        IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
        IniAttributes.SectionName = 'TEDIT_NEW_CLIENT_ACH\evDBCheckGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsLocal
        MultiSelectOptions = [msoShiftSelect]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = True
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
  end
  object dsFUI: TevDataSource
    DataSet = TMP_CO_FED_TAX_LIABILITIES
    Left = 128
    Top = 64
  end
  object dsSUI: TevDataSource
    DataSet = TMP_CO_SUI_LIABILITIES
    Left = 176
    Top = 64
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 224
    Top = 64
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 264
    Top = 64
  end
  object TMP_CO_FED_TAX_LIABILITIES: TTMP_CO_FED_TAX_LIABILITIES
    ProviderName = 'TMP_CO_FED_TAX_LIABILITIES_PROV'
    OnCalcFields = TMP_CO_FED_TAX_LIABILITIESCalcFields
    BlobsLoadMode = blmManualLoad
    Left = 272
    Top = 24
    object TMP_CO_FED_TAX_LIABILITIESCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object TMP_CO_FED_TAX_LIABILITIESCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
    object TMP_CO_FED_TAX_LIABILITIESAMOUNT: TCurrencyField
      DisplayWidth = 10
      FieldName = 'AMOUNT'
    end
    object TMP_CO_FED_TAX_LIABILITIESDUE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'DUE_DATE'
    end
    object TMP_CO_FED_TAX_LIABILITIESCHECK_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'CHECK_DATE'
    end
    object TMP_CO_FED_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'ADJUSTMENT_TYPE'
      Size = 1
    end
    object TMP_CO_FED_TAX_LIABILITIESCO_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'CO_NAME'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 40
      Lookup = True
    end
    object TMP_CO_FED_TAX_LIABILITIESCO_NUMBER: TStringField
      DisplayWidth = 10
      FieldKind = fkLookup
      FieldName = 'CO_NUMBER'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 10
      Lookup = True
    end
    object TMP_CO_FED_TAX_LIABILITIESSTATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object TMP_CO_FED_TAX_LIABILITIESCO_FED_TAX_LIABILITIES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_FED_TAX_LIABILITIES_NBR'
    end
    object TMP_CO_FED_TAX_LIABILITIESTYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'TYPE'
      Size = 40
      Calculated = True
    end
    object TMP_CO_FED_TAX_LIABILITIESTAX_TYPE: TStringField
      FieldName = 'TAX_TYPE'
      Size = 1
    end
  end
  object TMP_CO_SUI_LIABILITIES: TTMP_CO_SUI_LIABILITIES
    ProviderName = 'TMP_CO_SUI_LIABILITIES_PROV'
    FieldDefs = <
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SY_SUI_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_SUI_LIABILITIES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'AMOUNT'
        DataType = ftCurrency
      end
      item
        Name = 'DUE_DATE'
        DataType = ftDate
      end
      item
        Name = 'CHECK_DATE'
        DataType = ftDate
      end
      item
        Name = 'ADJUSTMENT_TYPE'
        DataType = ftString
        Size = 1
      end>
    BlobsLoadMode = blmManualLoad
    Left = 232
    Top = 24
    object TMP_CO_SUI_LIABILITIESSTATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object TMP_CO_SUI_LIABILITIESSUI_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'SUI_NAME'
      LookupDataSet = DM_SY_SUI.SY_SUI
      LookupKeyFields = 'SY_SUI_NBR'
      LookupResultField = 'SUI_TAX_NAME'
      KeyFields = 'SY_SUI_NBR'
      Size = 40
      Lookup = True
    end
    object TMP_CO_SUI_LIABILITIESSY_SUI_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_SUI_NBR'
    end
    object TMP_CO_SUI_LIABILITIESCO_SUI_LIABILITIES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_SUI_LIABILITIES_NBR'
    end
    object TMP_CO_SUI_LIABILITIESCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object TMP_CO_SUI_LIABILITIESCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
    object TMP_CO_SUI_LIABILITIESAMOUNT: TCurrencyField
      DisplayWidth = 10
      FieldName = 'AMOUNT'
    end
    object TMP_CO_SUI_LIABILITIESDUE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'DUE_DATE'
    end
    object TMP_CO_SUI_LIABILITIESCHECK_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'CHECK_DATE'
    end
    object TMP_CO_SUI_LIABILITIESADJUSTMENT_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'ADJUSTMENT_TYPE'
      Size = 1
    end
    object TMP_CO_SUI_LIABILITIESCO_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'CO_NAME'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 40
      Lookup = True
    end
    object TMP_CO_SUI_LIABILITIESCO_NUMBER: TStringField
      DisplayWidth = 10
      FieldKind = fkLookup
      FieldName = 'CO_NUMBER'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 10
      Lookup = True
    end
  end
  object dsState: TevDataSource
    DataSet = TMP_CO_STATE_TAX_LIABILITIES
    Left = 128
    Top = 104
  end
  object dsLocal: TevDataSource
    DataSet = TMP_CO_LOCAL_TAX_LIABILITIES
    Left = 176
    Top = 104
  end
  object TMP_CO_STATE_TAX_LIABILITIES: TTMP_CO_STATE_TAX_LIABILITIES
    ProviderName = 'TMP_CO_STATE_TAX_LIABILITIES_PROV'
    OnCalcFields = TMP_CO_STATE_TAX_LIABILITIESCalcFields
    BlobsLoadMode = blmManualLoad
    Left = 224
    Top = 96
    object TMP_CO_STATE_TAX_LIABILITIESSTATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object TMP_CO_STATE_TAX_LIABILITIESCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object TMP_CO_STATE_TAX_LIABILITIESCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
    object TMP_CO_STATE_TAX_LIABILITIESAMOUNT: TCurrencyField
      DisplayWidth = 10
      FieldName = 'AMOUNT'
    end
    object TMP_CO_STATE_TAX_LIABILITIESDUE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'DUE_DATE'
    end
    object TMP_CO_STATE_TAX_LIABILITIESCHECK_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'CHECK_DATE'
    end
    object TMP_CO_STATE_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'ADJUSTMENT_TYPE'
      Size = 1
    end
    object TMP_CO_STATE_TAX_LIABILITIESCO_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'CO_NAME'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 40
      Lookup = True
    end
    object TMP_CO_STATE_TAX_LIABILITIESCO_NUMBER: TStringField
      DisplayWidth = 10
      FieldKind = fkLookup
      FieldName = 'CO_NUMBER'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 10
      Lookup = True
    end
    object TMP_CO_STATE_TAX_LIABILITIESCO_STATE_TAX_LIABILITIES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_STATE_TAX_LIABILITIES_NBR'
    end
    object TMP_CO_STATE_TAX_LIABILITIESSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object TMP_CO_STATE_TAX_LIABILITIESTAX_TYPE: TStringField
      FieldName = 'TAX_TYPE'
      Size = 1
    end
    object TMP_CO_STATE_TAX_LIABILITIESTYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'TYPE'
      Size = 40
      Calculated = True
    end
  end
  object TMP_CO_LOCAL_TAX_LIABILITIES: TTMP_CO_LOCAL_TAX_LIABILITIES
    ProviderName = 'TMP_CO_LOCAL_TAX_LIABILITIES_PROV'
    BlobsLoadMode = blmManualLoad
    Left = 264
    Top = 96
    object TMP_CO_LOCAL_TAX_LIABILITIESSTATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESAMOUNT: TCurrencyField
      DisplayWidth = 10
      FieldName = 'AMOUNT'
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESDUE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'DUE_DATE'
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESCHECK_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'CHECK_DATE'
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'ADJUSTMENT_TYPE'
      Size = 1
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESCO_NAME: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'CO_NAME'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 40
      Lookup = True
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESCO_NUMBER: TStringField
      DisplayWidth = 10
      FieldKind = fkLookup
      FieldName = 'CO_NUMBER'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 10
      Lookup = True
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESCO_LOCAL_TAX_LIABILITIES_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_LOCAL_TAX_LIABILITIES_NBR'
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESLOCAL_NAME: TStringField
      FieldKind = fkLookup
      FieldName = 'LOCAL_NAME'
      LookupDataSet = DM_SY_LOCALS.SY_LOCALS
      LookupKeyFields = 'SY_LOCALS_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'SY_LOCALS_NBR'
      Size = 60
      Lookup = True
    end
    object TMP_CO_LOCAL_TAX_LIABILITIESSY_LOCALS_NBR: TIntegerField
      FieldName = 'SY_LOCALS_NBR'
    end
  end
end
