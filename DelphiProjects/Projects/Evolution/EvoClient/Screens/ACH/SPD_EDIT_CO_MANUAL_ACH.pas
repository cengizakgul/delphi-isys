// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_MANUAL_ACH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc, DBCtrls, StdCtrls, Buttons, Wwdbigrd,
  Grids, Wwdbgrid, ExtCtrls, ComCtrls, wwdbdatetimepicker, Mask, wwdblook,
  DBGrids, Gauges,  wwdbedit, SDataStructure, EvMainboard,
  SPD_EDIT_CO_BASE_2, ActnList, EvTypes, Variants,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, SDDClasses, EvContext,
  ISDataAccessComponents, EvDataAccessComponents, Wwdotdot, Wwdbcomb,
  SFieldCodeValues, SDataDictbureau, SDataDictclient, SDataDicttemp,
  EvCommonInterfaces, IsBasicUtils, SPackageEntry, EvExceptions,
  EvSecElement, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBComboBox, isUIEdit, isUIwwDBDateTimePicker, isUIDBMemo,
  isUIwwDBEdit, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TEDIT_CO_MANUAL_ACH = class(TEDIT_CO_BASE_2)
    tbManualACH: TTabSheet;
    Label2: TevLabel;
    Label3: TevLabel;
    Label6: TevLabel;
    DBEdit5: TevDBEdit;
    Label7: TevLabel;
    GroupBox1: TevGroupBox;
    Label8: TevLabel;
    Label9: TevLabel;
    Label10: TevLabel;
    Label11: TevLabel;
    Label12: TevLabel;
    GroupBox2: TevGroupBox;
    Label13: TevLabel;
    Label14: TevLabel;
    Label15: TevLabel;
    Label16: TevLabel;
    Label17: TevLabel;
    DBMemo1: TEvDBMemo;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    wwDBDateTimePicker2: TevDBDateTimePicker;
    wwlcEmployeeFrom: TevDBLookupCombo;
    wwlcEmployeeTo: TevDBLookupCombo;
    wwlcCompanyFrom: TevDBLookupCombo;
    wwlcCompanyTo: TevDBLookupCombo;
    wwlcSBAcctFrom: TevDBLookupCombo;
    wwlcSBAcctTo: TevDBLookupCombo;
    wwlcCLAcctFrom: TevDBLookupCombo;
    wwlcCLAcctTo: TevDBLookupCombo;
    wwlcEEDirDepFrom: TevDBLookupCombo;
    wwlcEEDirDepTo: TevDBLookupCombo;
    wwDBGrid1: TevDBGrid;
    DBCheckBox1: TDBCheckBox;
    tbCreateFile: TTabSheet;
    Panel4: TevPanel;
    RadioGroup1: TevRadioGroup;
    btnCreateFile: TevBitBtn;
    btnPreprocess: TevBitBtn;
    wwdsTMP_MANUAL_ACH: TevDataSource;
    wwDBGrid2: TevDBGrid;
    BitBtn7: TevBitBtn;
    Edit1: TevEdit;
    SpeedButton1: TevSpeedButton;
    Label1: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    cbShowAll: TevCheckBox;
    Bevel1: TEvBevel;
    Bevel2: TEvBevel;
    Gauge1: TGauge;
    Label4: TevLabel;
    ACHSBBankAccounts: TevClientDataSet;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    ActionList1: TActionList;
    actInsert: TAction;
    actUpdate: TAction;
    actCancel: TAction;
    actSwitch: TAction;
    actDelete: TAction;
    cbUSESBEIN: TevCheckBox;
    cbBalanceBatches: TevCheckBox;
    cbCTS: TevCheckBox;
    evLabel1: TevLabel;
    cbCheckReconType: TevDBComboBox;
    cbCombine: TevComboBox;
    evLabel2: TevLabel;
    evLabel9: TevLabel;
    cbPostProcessReport: TevComboBox;
    evLabel3: TevLabel;
    wwlcChildSupportCases: TevDBLookupCombo;
    procedure PageControl1Change(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure wwlcEEDirDepFromDropDown(Sender: TObject);
    procedure wwlcEEDirDepToDropDown(Sender: TObject);
    procedure wwDBLookupCombo1Change(Sender: TObject);
    procedure btnPreprocessClick(Sender: TObject);
    procedure btnCreateFileClick(Sender: TObject);
    procedure wwlcEmployeeFromCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcEmployeeToCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure btnAddClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure actInsertExecute(Sender: TObject);
    procedure actUpdateExecute(Sender: TObject);
    procedure actCancelExecute(Sender: TObject);
    procedure actSwitchExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure cbCombineClick(Sender: TObject);
    procedure cbUSESBEINClick(Sender: TObject);
    procedure cbBalanceBatchesClick(Sender: TObject);
    procedure cbCTSClick(Sender: TObject);
    procedure cbPostProcessReportClick(Sender: TObject);
    procedure wwlcEEDirDepCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcChildSupportCasesCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcChildSupportCasesDropDown(Sender: TObject);
    procedure wwlcChildSupportCasesChange(Sender: TObject);
  private
    FScreenIsReadOnly: Boolean;

    procedure GetManualAchData;
    procedure SetACHButtonsState(aState: Boolean);
    procedure MANUAL_ACHBeforeInsert(DataSet: TDataSet);
    procedure MANUAL_ACHBeforeEdit(DataSet: TDataSet);
    procedure MANUAL_ACHBeforePost(DataSet: TDataSet);
    procedure MANUAL_ACHBeforeDelete(DataSet: TDataSet);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure AfterOpenCompany; override;
    procedure OnActivateParams(const AContext: Integer; const AParams: array of Variant); override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
  end;

var
  EDIT_CO_MANUAL_ACH: TEDIT_CO_MANUAL_ACH;

implementation

uses
  EvConsts, EvUtils, SACHTypes, isBaseClasses;

{$R *.DFM}

procedure TEDIT_CO_MANUAL_ACH.PageControl1Change(Sender: TObject);
begin
  inherited;
  ctx_StartWait;
  try
    if PageControl1.ActivePage = tbCreateFile then
    begin
      GetManualAchData;
      SetACHButtonsState(False);
    end
    else
    begin
      if DM_TEMPORARY.TMP_CO.RecordCount > 0 then
        BitBtn1.Click;
    end;
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_CO_MANUAL_ACH.RadioGroup1Click(Sender: TObject);
begin
  inherited;
  GetManualAchData;
  wwDBGrid2.RefreshDisplay;
  if RadioGroup1.ItemIndex = 0 then
    BitBtn7.Caption := 'Put On-Hold'
  else
    BitBtn7.Caption := 'Make Pending';
end;

procedure TEDIT_CO_MANUAL_ACH.BitBtn7Click(Sender: TObject);
var
  CompanyNbr: Integer;
begin
  inherited;

  if DM_TEMPORARY.TMP_CO_MANUAL_ACH.RecordCount = 0 then
    Exit;

  ctx_StartWait;
  try
    CompanyNbr := DM_COMPANY.CO_MANUAL_ACH.FieldByName('CO_NBR').AsInteger;
    if DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CL_NBR').AsInteger <> ctx_DataAccess.ClientID then
    begin
      ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CL_NBR').AsInteger);
      DM_COMPANY.CO.Close;
      CompanyNbr := -1;
    end;

    if CompanyNbr <> DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CO_NBR').AsInteger then
    begin
      DM_COMPANY.CO_MANUAL_ACH.DataRequired('CO_NBR=' + DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CO_NBR').AsString);
      DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CL_NBR').Value, DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CO_NBR').Value]), []);
    end;

    if not DM_COMPANY.CO_MANUAL_ACH.Locate('CO_MANUAL_ACH_NBR', DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CO_MANUAL_ACH_NBR').Value, []) then
      raise EInconsistentData.CreateHelp('Could not locate this record in CO_MANUAL_ACH.', IDH_InconsistentData);
    DM_COMPANY.CO_MANUAL_ACH.Edit;

    if BitBtn7.Caption = 'Make Pending' then
      DM_COMPANY.CO_MANUAL_ACH.FieldByName('STATUS').AsString := COMMON_REPORT_STATUS__PENDING
    else
      DM_COMPANY.CO_MANUAL_ACH.FieldByName('STATUS').AsString := COMMON_REPORT_STATUS__HOLD;
      
    DM_COMPANY.CO_MANUAL_ACH.Post;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_MANUAL_ACH]);
    DM_TEMPORARY.TMP_CO_MANUAL_ACH.Delete;
  finally
    ctx_EndWait;
  end;

  SetACHButtonsState(False);
end;

procedure TEDIT_CO_MANUAL_ACH.GetManualAchData;
begin
  DM_TEMPORARY.TMP_CO_MANUAL_ACH.Close;
  case RadioGroup1.ItemIndex of
    0:
      DM_TEMPORARY.TMP_CO_MANUAL_ACH.DataRequired('STATUS=''' + COMMON_REPORT_STATUS__PENDING + '''');
    1:
      DM_TEMPORARY.TMP_CO_MANUAL_ACH.DataRequired('STATUS=''' + COMMON_REPORT_STATUS__HOLD + '''');
  end;
end;

procedure TEDIT_CO_MANUAL_ACH.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  if Edit1.Text = '' then
    Exit;
  if not DM_TEMPORARY.TMP_CO.Locate('CUSTOM_COMPANY_NUMBER', Edit1.Text, []) then
    raise EInvalidParameters.CreateHelp('Invalid Company Number!', IDH_InvalidParameters);

  ctx_StartWait;
  try
    BitBtn1.Click;
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_CO_MANUAL_ACH.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    SpeedButton1.Click;
end;

procedure TEDIT_CO_MANUAL_ACH.wwlcEEDirDepFromDropDown(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.FieldByName('FROM_EE_NBR').IsNull then
    raise EInvalidParameters.CreateHelp('You need to select an Employee first!', IDH_InvalidParameters);
  DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('EE_NBR=' + wwdsDetail.DataSet.FieldByName('FROM_EE_NBR').AsString);
end;

procedure TEDIT_CO_MANUAL_ACH.wwlcEEDirDepToDropDown(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.FieldByName('TO_EE_NBR').IsNull then
    raise EInvalidParameters.CreateHelp('You need to select an Employee first!', IDH_InvalidParameters);
  DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('EE_NBR=' + wwdsDetail.DataSet.FieldByName('TO_EE_NBR').AsString);
end;

procedure TEDIT_CO_MANUAL_ACH.wwDBLookupCombo1Change(Sender: TObject);
begin
  inherited;
  DM_TEMPORARY.TMP_CO.Filter := 'ACH_SB_BANK_ACCOUNT_NBR = ' +
    ACHSBBankAccounts.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString + GetReadOnlyClintCompanyListFilter;
  DM_TEMPORARY.TMP_CO.Filtered := True;
  if DM_TEMPORARY.TMP_CO.RecordCount = 0 then
  begin
    wwdsDetail.DataSet.Close;
    dsCL.DataSet.Close;
  end;
end;

procedure TEDIT_CO_MANUAL_ACH.btnPreprocessClick(Sender: TObject);
var
  t: IevProcessManualACHTask;
begin
  inherited;
  mb_AppSettings.AsString['PostProcessACHReport'] :=
    cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];

  if DM_TEMPORARY.TMP_CO_MANUAL_ACH.RecordCount = 0 then
    raise EInvalidParameters.CreateHelp('Nothing to process!', IDH_InvalidParameters);

  t := mb_TaskQueue.CreateTask(QUEUE_TASK_PROCESS_MANUAL_ACH, True) as IevProcessManualACHTask;
  try
    t.Caption := 'Preprocessing Manual ACH';
    t.ACHDataStream := DM_TEMPORARY.TMP_CO_MANUAL_ACH.GetDataStream(True, False);
    LoadACHOptionsFromRegistryI(t.ACHOptions);
    t.BatchBANK_REGISTER_TYPE := cbCheckReconType.Value;

    t.Preprocess := True;
    FScreenIsReadOnly := True;
    ctx_EvolutionGUI.AddTaskQueue(t);
  finally
    t := nil;
  end;
end;

procedure TEDIT_CO_MANUAL_ACH.btnCreateFileClick(Sender: TObject);
var
  t: IevProcessManualACHTask;
  BM: TBookmarkStr;
begin
  inherited;

  mb_AppSettings.AsString['PostProcessACHReport'] :=
    cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];


  DM_TEMPORARY.TMP_CO_MANUAL_ACH.DisableControls;
  try
    BM := DM_TEMPORARY.TMP_CO_MANUAL_ACH.Bookmark;
    try
      DM_TEMPORARY.TMP_CO_MANUAL_ACH.First;
      while not DM_TEMPORARY.TMP_CO_MANUAL_ACH.Eof do
      begin
        if (DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('STATUS').AsString <>
            COMMON_REPORT_STATUS__PENDING) and
           (DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('STATUS').AsString <>
            COMMON_REPORT_STATUS__PENDING) then
          raise EevException.Create('Can not change processed Manual ACH!');
        DM_TEMPORARY.TMP_CO_MANUAL_ACH.Edit;
        DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('STATUS').AsString :=
          COMMON_REPORT_STATUS__IN_PROCESS;
        DM_TEMPORARY.TMP_CO_MANUAL_ACH.Post;
        DM_TEMPORARY.TMP_CO_MANUAL_ACH.Next;
      end;
      DM_TEMPORARY.TMP_CO_MANUAL_ACH.MergeChangeLog;
    finally
      DM_TEMPORARY.TMP_CO_MANUAL_ACH.Bookmark := BM;
    end;
  finally
    DM_TEMPORARY.TMP_CO_MANUAL_ACH.EnableControls;
  end;

  t := mb_TaskQueue.CreateTask(QUEUE_TASK_PROCESS_MANUAL_ACH, True) as IevProcessManualACHTask;
  try
    t.Caption := 'Processing Manual ACH';
    t.ACHDataStream := DM_TEMPORARY.TMP_CO_MANUAL_ACH.GetDataStream(True, False);
    LoadACHOptionsFromRegistryI(t.ACHOptions);
    t.Preprocess := False;
    t.BatchBANK_REGISTER_TYPE := cbCheckReconType.Value;
    FScreenIsReadOnly := True;
    SetACHButtonsState(False);
    ctx_EvolutionGUI.AddTaskQueue(t);
  finally
    t := nil;
  end;

end;

procedure TEDIT_CO_MANUAL_ACH.SetACHButtonsState(aState: Boolean);
begin
  cbCombine.Enabled := {aState and} not mb_AppSettings.AsBoolean['BalanceACHBatches'];
end;

procedure TEDIT_CO_MANUAL_ACH.wwlcEmployeeFromCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if wwlcEmployeeFrom.Modified then
    wwdsDetail.DataSet.FieldByName('FROM_EE_DIRECT_DEPOSIT_NBR').Value := Null;
end;

procedure TEDIT_CO_MANUAL_ACH.wwlcEmployeeToCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if wwlcEmployeeTo.Modified then
    wwdsDetail.DataSet.FieldByName('TO_EE_DIRECT_DEPOSIT_NBR').Value := Null;
end;

procedure TEDIT_CO_MANUAL_ACH.btnAddClick(Sender: TObject);
begin
  inherited;
//  PageControl1.ActivePage := tbManualACH;
//  InsertWinControl.SetFocus;
  wwdsDetail.DataSet.Insert;
end;

procedure TEDIT_CO_MANUAL_ACH.btnDelClick(Sender: TObject);
begin
  inherited;
  if EvMessage('Are you sure?', mtConfirmation, [mbYes, mbNo]) = mrYes then
  begin
    wwdsDetail.DataSet.Delete;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_MANUAL_ACH]);
  end;
end;

procedure TEDIT_CO_MANUAL_ACH.btnOkClick(Sender: TObject);
begin
  inherited;
  ctx_DataAccess.PostDataSets([DM_COMPANY.CO_MANUAL_ACH]);
end;

procedure TEDIT_CO_MANUAL_ACH.btnCancelClick(Sender: TObject);
begin
  inherited;
  ctx_DataAccess.CancelDataSets([DM_COMPANY.CO_MANUAL_ACH]);
end;

procedure TEDIT_CO_MANUAL_ACH.BitBtn1Click(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = TabSheet1 then
    wwdbgridSelectClient.SetFocus;
end;

procedure TEDIT_CO_MANUAL_ACH.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_BANK_ACCOUNT, aDS);
  AddDS(DM_CLIENT.CL_PENSION, aDS);
  AddDS(DM_CLIENT.EE, aDS);
  inherited;
end;

function TEDIT_CO_MANUAL_ACH.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_COMPANY.CO_MANUAL_ACH;
end;

function TEDIT_CO_MANUAL_ACH.GetInsertControl: TWinControl;
begin
  Result := wwDBDateTimePicker2;
end;

procedure TEDIT_CO_MANUAL_ACH.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_BANKS, SupportDataSets, '');
end;

procedure TEDIT_CO_MANUAL_ACH.Activate;
var
  i: Integer;
  ReportName: String;
begin
  inherited;
  FScreenIsReadOnly := False;
  cbCheckReconType.Items.Text := GroupBox_Bank_Register_Type_ComboChoices;
  cbCheckReconType.Value := BANK_REGISTER_TYPE_Manual;
  cbBalanceBatches.Checked := mb_AppSettings.AsBoolean['BalanceACHBatches'];
  cbCombine.ItemIndex := mb_AppSettings.AsInteger['CombineACHTrans'];
  cbUseSBEIN.Checked := mb_AppSettings.AsBoolean['USESBEINForACH'];
  cbCTS.Checked := mb_AppSettings.AsBoolean['ACHCTS'];
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ACH_ACCOUNT = ''' + GROUP_BOX_YES + '''');
  ACHSBBankAccounts.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');

  wwDBLookupCombo1.Text := ACHSBBankAccounts.FieldByName('NAME_DESCRIPTION').AsString;
  DM_TEMPORARY.TMP_CO_MANUAL_ACH.Filtered := True;

  DM_COMPANY.CO_MANUAL_ACH.BeforeInsert := MANUAL_ACHBeforeInsert;
  DM_COMPANY.CO_MANUAL_ACH.BeforeEdit := MANUAL_ACHBeforeEdit;
  DM_COMPANY.CO_MANUAL_ACH.BeforePost := MANUAL_ACHBeforePost;
  DM_COMPANY.CO_MANUAL_ACH.BeforeDelete := MANUAL_ACHBeforeDelete;

  (Owner as TFramePackageTmpl).SetButton(navInsert, ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_MANUAL_ACH') = stEnabled);
  (Owner as TFramePackageTmpl).SetButton(navDelete, ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_MANUAL_ACH') = stEnabled);

  ReportName := mb_AppSettings.AsString['PostProcessACHReport'];

  cbPostProcessReport.Items.Clear;
  cbPostProcessReport.Items.Add('N/A');
  DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.DataRequired('ANCESTOR_CLASS_NAME='''+REPORT_ASCII_TYPE+'''');
  DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.First;
  while not DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Eof do
  begin
    cbPostProcessReport.Items.Add(DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.REPORT_DESCRIPTION.AsString+
      ' (B'+DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.SB_REPORT_WRITER_REPORTS_NBR.AsString+')');
    DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Next;
  end;
  DM_SYSTEM.SY_REPORT_WRITER_REPORTS.DataRequired('ANCESTOR_CLASS_NAME='''+REPORT_ASCII_TYPE+'''');
  DM_SYSTEM.SY_REPORT_WRITER_REPORTS.First;
  while not DM_SYSTEM.SY_REPORT_WRITER_REPORTS.Eof do
  begin
    cbPostProcessReport.Items.Add(DM_SYSTEM.SY_REPORT_WRITER_REPORTS.REPORT_DESCRIPTION.AsString+
      ' (S'+DM_SYSTEM.SY_REPORT_WRITER_REPORTS.SY_REPORT_WRITER_REPORTS_NBR.AsString+')');
    DM_SYSTEM.SY_REPORT_WRITER_REPORTS.Next;
  end;
  cbPostProcessReport.ItemIndex := 0;

  for i := 0 to cbPostProcessReport.Items.Count - 1 do
  if cbPostProcessReport.Items[i] = ReportName then
  begin
    cbPostProcessReport.ItemIndex := i;
    break;
  end;

  if cbPostProcessReport.ItemIndex = 0 then
    mb_AppSettings.AsString['PostProcessACHReport'] :=
      cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];
end;

procedure TEDIT_CO_MANUAL_ACH.Deactivate;
begin
  DM_COMPANY.CO_MANUAL_ACH.BeforeEdit := nil;
  DM_COMPANY.CO_MANUAL_ACH.BeforePost := nil;
  DM_COMPANY.CO_MANUAL_ACH.BeforeDelete := nil;

  DM_TEMPORARY.TMP_CO.Filtered := False;
  DM_TEMPORARY.TMP_CO_MANUAL_ACH.Filtered := False;
  inherited;
end;

procedure TEDIT_CO_MANUAL_ACH.actInsertExecute(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage <> tbManualACH then
    Exit;
  wwdsDetail.DataSet.Insert;
end;

procedure TEDIT_CO_MANUAL_ACH.actUpdateExecute(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage <> tbManualACH then
    Exit;
  if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
  begin
    wwdsDetail.DataSet.Post;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_MANUAL_ACH]);
  end;
end;

procedure TEDIT_CO_MANUAL_ACH.actCancelExecute(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage <> tbManualACH then
    Exit;
  wwdsDetail.DataSet.Cancel;
end;

procedure TEDIT_CO_MANUAL_ACH.actSwitchExecute(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage <> tbManualACH then
    Exit;
  Edit1.SetFocus;
end;

procedure TEDIT_CO_MANUAL_ACH.actDeleteExecute(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = tbCreateFile then
    Exit;
  if EvMessage('Are you sure?', mtConfirmation, [mbYes, mbNo]) = mrYes then
  begin
    wwdsDetail.DataSet.Delete;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_MANUAL_ACH]);
  end;
end;

procedure TEDIT_CO_MANUAL_ACH.AfterOpenCompany;
begin
  inherited;
  DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('ALL');
  DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.DataRequired('ALL');  
  wwlcEEDirDepFrom.RefreshDisplay();
  wwlcEEDirDepTo.RefreshDisplay();
end;

procedure TEDIT_CO_MANUAL_ACH.cbCombineClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsInteger['CombineACHTrans'] := cbCombine.ItemIndex;
end;

procedure TEDIT_CO_MANUAL_ACH.cbUSESBEINClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['USESBEINForACH'] := cbUseSBEIN.Checked;
end;

procedure TEDIT_CO_MANUAL_ACH.cbBalanceBatchesClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['BalanceACHBatches'] := cbBalanceBatches.Checked;
  cbCombine.Enabled := not mb_AppSettings.AsBoolean['BalanceACHBatches'];
  if not cbCombine.Enabled then
    cbCombine.ItemIndex := 0;
end;

procedure TEDIT_CO_MANUAL_ACH.cbCTSClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['ACHCTS'] := cbCTS.Checked;
end;

procedure TEDIT_CO_MANUAL_ACH.OnActivateParams(const AContext: Integer; const AParams: array of Variant);
begin
  inherited;
  if AContext = 1 then
    PageControl1.ActivatePage(tbCreateFile);
end;

procedure TEDIT_CO_MANUAL_ACH.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
  CompanyNbr: Integer;
begin
(*
  if (Kind in [NavInsert, NavDelete]) and
     (FindSecurityElement(Self, 'USER_CAN_CREATE_MANUAL_ACH').SecurityState <> stEnabled) then
    raise EevException.Create('You have no rights to perform this operation.');
*)
  if Kind = NavDelete then
  begin
    if not DM_TEMPORARY.TMP_CO_MANUAL_ACH.IsEmpty then
    begin
      CompanyNbr := DM_COMPANY.CO_MANUAL_ACH.FieldByName('CO_NBR').AsInteger;
      if DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CL_NBR').AsInteger <> ctx_DataAccess.ClientID then
      begin
        ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CL_NBR').AsInteger);
        DM_COMPANY.CO.Close;
        CompanyNbr := -1;
      end;

      if CompanyNbr <> DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CO_NBR').AsInteger then
      begin
        DM_COMPANY.CO_MANUAL_ACH.DataRequired('CO_NBR=' + DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CO_NBR').AsString);
        DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CL_NBR').Value, DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CO_NBR').Value]), []);
      end;

      if not DM_COMPANY.CO_MANUAL_ACH.Locate('CO_MANUAL_ACH_NBR', DM_TEMPORARY.TMP_CO_MANUAL_ACH.FieldByName('CO_MANUAL_ACH_NBR').Value, []) then
        raise EInconsistentData.CreateHelp('Could not locate this record in CO_MANUAL_ACH.', IDH_InconsistentData);
    end;
  end
  else inherited;
end;

procedure TEDIT_CO_MANUAL_ACH.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = NavDelete then
  begin
    GetManualAchData;
    wwDBGrid2.RefreshDisplay;
  end;
end;

procedure TEDIT_CO_MANUAL_ACH.MANUAL_ACHBeforeEdit(DataSet: TDataSet);
begin
  if ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_MANUAL_ACH') <> stEnabled then
    raise EUpdateError.CreateHelp('You have no rights to perform this operation.', IDH_ConsistencyViolation);
  if (DataSet.FieldByName('STATUS').AsString[1] in [COMMON_REPORT_STATUS__COMPLETED, COMMON_REPORT_STATUS__DELETED]) then
    raise EUpdateError.CreateHelp('Can not change processed Manual ACH!', IDH_ConsistencyViolation);
end;

procedure TEDIT_CO_MANUAL_ACH.MANUAL_ACHBeforePost(DataSet: TDataSet);
var
  TmpStr: String;
begin
  if (DataSet.FieldByName('FROM_SB_BANK_ACCOUNTS_NBR').IsNull) and (DataSet.FieldByName('FROM_CL_BANK_ACCOUNT_NBR').IsNull) and
    (DataSet.FieldByName('FROM_EE_DIRECT_DEPOSIT_NBR').IsNull) then
    raise EUpdateError.CreateHelp('"FROM" bank account must be defined!', IDH_ConsistencyViolation);
  if (DataSet.FieldByName('TO_SB_BANK_ACCOUNTS_NBR').IsNull) and (DataSet.FieldByName('TO_CL_BANK_ACCOUNT_NBR').IsNull) and
    (DataSet.FieldByName('TO_EE_DIRECT_DEPOSIT_NBR').IsNull) then
    raise EUpdateError.CreateHelp('"TO" bank account must be defined!', IDH_ConsistencyViolation);

  TevClientDataSet(DataSet).GetBlobData('PURPOSE_NOTES');
  if (DataSet.FieldByName('PURPOSE_NOTES').IsNull) then
    raise EUpdateError.CreateHelp('"Notes" must be defined!', IDH_ConsistencyViolation)
  else
    if (Length(Trim(DataSet.FieldByName('PURPOSE_NOTES').AsString))=0) then
      raise EUpdateError.CreateHelp('"Notes" must be defined!', IDH_ConsistencyViolation);

  if (DataSet.FieldByName('AMOUNT').IsNull) then
    raise EUpdateError.CreateHelp('"Amount" must be defined!', IDH_ConsistencyViolation);

  if DataSet.FieldByName('STATUS').IsNull then
    DataSet.FieldByName('STATUS').AsString := COMMON_REPORT_STATUS__PENDING;
  if DataSet.FieldByName('STATUS_DATE').IsNull then
    DataSet.FieldByName('STATUS_DATE').AsDateTime := Now;
  TmpStr := Trim(ExtractFromFiller(Dataset.FieldByName('FILLER').AsString,11,10));
  if Trim(wwlcChildSupportCases.Text) = '' then
    Dataset.FieldByName('FILLER').AsString := PutIntoFiller(Dataset.FieldByName('FILLER').AsString, '          ', 11,10)
  else  
  if TmpStr <> wwlcChildSupportCases.LookupTable.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').AsString then
    Dataset.FieldByName('FILLER').AsString :=
      PutIntoFiller(Dataset.FieldByName('FILLER').AsString, wwlcChildSupportCases.LookupTable.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').AsString, 11,10);
end;

procedure TEDIT_CO_MANUAL_ACH.MANUAL_ACHBeforeDelete(DataSet: TDataSet);
begin
  if ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_MANUAL_ACH') <> stEnabled then
    raise EUpdateError.CreateHelp('You have no rights to perform this operation.', IDH_ConsistencyViolation);
  if DataSet.FieldByName('STATUS').AsString[1] in [COMMON_REPORT_STATUS__COMPLETED, COMMON_REPORT_STATUS__DELETED] then
    raise EUpdateError.CreateHelp('Can not delete processed Manual ACH!', IDH_ConsistencyViolation);
end;

constructor TEDIT_CO_MANUAL_ACH.Create(AOwner: TComponent);
begin
  inherited;

end;

procedure TEDIT_CO_MANUAL_ACH.MANUAL_ACHBeforeInsert(DataSet: TDataSet);
begin
  if ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_MANUAL_ACH') <> stEnabled then
    raise EUpdateError.CreateHelp('You have no rights to perform this operation.', IDH_ConsistencyViolation);
end;


procedure TEDIT_CO_MANUAL_ACH.cbPostProcessReportClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsString['PostProcessACHReport'] :=
    cbPostProcessReport.Items[cbPostProcessReport.ItemIndex];
end;

procedure TEDIT_CO_MANUAL_ACH.wwlcEEDirDepCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  DM_EMPLOYEE.EE_DIRECT_DEPOSIT.DataRequired('ALL');
end;

procedure TEDIT_CO_MANUAL_ACH.wwlcChildSupportCasesCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.DataRequired('ALL');
end;

procedure TEDIT_CO_MANUAL_ACH.wwlcChildSupportCasesDropDown(
  Sender: TObject);
begin
  try
    inherited;
    if wwdsDetail.DataSet.FieldByName('TO_EE_NBR').IsNull then
      raise EInvalidParameters.CreateHelp('You need to select an Employee first!', IDH_InvalidParameters);
  except
    raise EInvalidParameters.CreateHelp('You need to select an Employee first!', IDH_InvalidParameters);  
  end;
  DM_EMPLOYEE.EE_CHILD_SUPPORT_CASES.DataRequired('EE_NBR=' + wwdsDetail.DataSet.FieldByName('TO_EE_NBR').AsString);
end;

procedure TEDIT_CO_MANUAL_ACH.wwlcChildSupportCasesChange(Sender: TObject);
var
  TmpStr: String;
begin
  inherited;
  if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
  begin
    TmpStr := Trim(ExtractFromFiller(wwdsDetail.Dataset.FieldByName('FILLER').AsString,11,10));
    if Trim(wwlcChildSupportCases.Text) = '' then
      wwdsDetail.Dataset.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.Dataset.FieldByName('FILLER').AsString, '          ', 11,10)
    else
    if TmpStr <> wwlcChildSupportCases.LookupTable.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').AsString then
      wwdsDetail.Dataset.FieldByName('FILLER').AsString :=
        PutIntoFiller(wwdsDetail.Dataset.FieldByName('FILLER').AsString, wwlcChildSupportCases.LookupTable.FieldByName('EE_CHILD_SUPPORT_CASES_NBR').AsString, 11,10);
  end;
end;

initialization
  RegisterClass(TEDIT_CO_MANUAL_ACH);

end.
