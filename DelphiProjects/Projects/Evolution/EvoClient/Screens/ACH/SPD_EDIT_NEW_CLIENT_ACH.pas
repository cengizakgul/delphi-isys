unit SPD_EDIT_NEW_CLIENT_ACH;

interface

uses
  SFrameEntry, Classes, DB, Wwdatsrc, ISBasicClasses, 
  StdCtrls, Buttons, Controls, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  SDataStructure, EvConsts, SDDClasses, Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, wwdblook, kbmMemTable, ISKbmMemDataSet, EvContext,
  ISDataAccessComponents, EvDataAccessComponents, EvUtils, Dialogs,
  SysUtils, evAchBase, SFieldCodeValues, SDataDicttemp,
  SDataDictsystem, evAchUtils, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_NEW_CLIENT_ACH = class(TFrameEntry)
    evPanel1: TevPanel;
    evBitBtn1: TevBitBtn;
    evLabel3: TevLabel;
    dsFUI: TevDataSource;
    dsSUI: TevDataSource;
    DM_TEMPORARY: TDM_TEMPORARY;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    TMP_CO_FED_TAX_LIABILITIES: TTMP_CO_FED_TAX_LIABILITIES;
    TMP_CO_SUI_LIABILITIES: TTMP_CO_SUI_LIABILITIES;
    TMP_CO_SUI_LIABILITIESSUI_NAME: TStringField;
    TMP_CO_SUI_LIABILITIESSY_SUI_NBR: TIntegerField;
    TMP_CO_SUI_LIABILITIESCO_SUI_LIABILITIES_NBR: TIntegerField;
    TMP_CO_SUI_LIABILITIESCL_NBR: TIntegerField;
    TMP_CO_SUI_LIABILITIESCO_NBR: TIntegerField;
    TMP_CO_SUI_LIABILITIESAMOUNT: TCurrencyField;
    TMP_CO_SUI_LIABILITIESDUE_DATE: TDateField;
    TMP_CO_SUI_LIABILITIESCHECK_DATE: TDateField;
    TMP_CO_SUI_LIABILITIESADJUSTMENT_TYPE: TStringField;
    TMP_CO_SUI_LIABILITIESCO_NAME: TStringField;
    TMP_CO_SUI_LIABILITIESCO_NUMBER: TStringField;
    TMP_CO_FED_TAX_LIABILITIESCL_NBR: TIntegerField;
    TMP_CO_FED_TAX_LIABILITIESCO_NBR: TIntegerField;
    TMP_CO_FED_TAX_LIABILITIESAMOUNT: TCurrencyField;
    TMP_CO_FED_TAX_LIABILITIESDUE_DATE: TDateField;
    TMP_CO_FED_TAX_LIABILITIESCHECK_DATE: TDateField;
    TMP_CO_FED_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField;
    TMP_CO_FED_TAX_LIABILITIESCO_NAME: TStringField;
    TMP_CO_FED_TAX_LIABILITIESCO_NUMBER: TStringField;
    TMP_CO_FED_TAX_LIABILITIESSTATUS: TStringField;
    TMP_CO_SUI_LIABILITIESSTATUS: TStringField;
    TMP_CO_FED_TAX_LIABILITIESCO_FED_TAX_LIABILITIES_NBR: TIntegerField;
    evPanel3: TevPanel;
    evPanel4: TevPanel;
    evPanel2: TevPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evPanel5: TevPanel;
    evDBGrid1: TevDBCheckGrid;
    evDBGrid2: TevDBCheckGrid;
    evPanel6: TevPanel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evPanel7: TevPanel;
    evDBCheckGrid1: TevDBCheckGrid;
    evDBCheckGrid2: TevDBCheckGrid;
    dsState: TevDataSource;
    dsLocal: TevDataSource;
    TMP_CO_STATE_TAX_LIABILITIES: TTMP_CO_STATE_TAX_LIABILITIES;
    TMP_CO_LOCAL_TAX_LIABILITIES: TTMP_CO_LOCAL_TAX_LIABILITIES;
    TMP_CO_STATE_TAX_LIABILITIESSTATUS: TStringField;
    TMP_CO_STATE_TAX_LIABILITIESCL_NBR: TIntegerField;
    TMP_CO_STATE_TAX_LIABILITIESCO_NBR: TIntegerField;
    TMP_CO_STATE_TAX_LIABILITIESAMOUNT: TCurrencyField;
    TMP_CO_STATE_TAX_LIABILITIESDUE_DATE: TDateField;
    TMP_CO_STATE_TAX_LIABILITIESCHECK_DATE: TDateField;
    TMP_CO_STATE_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField;
    TMP_CO_STATE_TAX_LIABILITIESCO_NAME: TStringField;
    TMP_CO_STATE_TAX_LIABILITIESCO_NUMBER: TStringField;
    TMP_CO_STATE_TAX_LIABILITIESCO_STATE_TAX_LIABILITIES_NBR: TIntegerField;
    TMP_CO_LOCAL_TAX_LIABILITIESSTATUS: TStringField;
    TMP_CO_LOCAL_TAX_LIABILITIESCL_NBR: TIntegerField;
    TMP_CO_LOCAL_TAX_LIABILITIESCO_NBR: TIntegerField;
    TMP_CO_LOCAL_TAX_LIABILITIESAMOUNT: TCurrencyField;
    TMP_CO_LOCAL_TAX_LIABILITIESDUE_DATE: TDateField;
    TMP_CO_LOCAL_TAX_LIABILITIESCHECK_DATE: TDateField;
    TMP_CO_LOCAL_TAX_LIABILITIESADJUSTMENT_TYPE: TStringField;
    TMP_CO_LOCAL_TAX_LIABILITIESCO_NAME: TStringField;
    TMP_CO_LOCAL_TAX_LIABILITIESCO_NUMBER: TStringField;
    TMP_CO_LOCAL_TAX_LIABILITIESCO_LOCAL_TAX_LIABILITIES_NBR: TIntegerField;
    TMP_CO_FED_TAX_LIABILITIESTYPE: TStringField;
    TMP_CO_FED_TAX_LIABILITIESTAX_TYPE: TStringField;
    TMP_CO_STATE_TAX_LIABILITIESSTATE: TStringField;
    TMP_CO_STATE_TAX_LIABILITIESTAX_TYPE: TStringField;
    TMP_CO_STATE_TAX_LIABILITIESTYPE: TStringField;
    TMP_CO_LOCAL_TAX_LIABILITIESLOCAL_NAME: TStringField;
    TMP_CO_LOCAL_TAX_LIABILITIESSY_LOCALS_NBR: TIntegerField;
    procedure FrameResize(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure TMP_CO_FED_TAX_LIABILITIESCalcFields(DataSet: TDataSet);
    procedure TMP_CO_STATE_TAX_LIABILITIESCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

{$R *.dfm}

procedure TEDIT_NEW_CLIENT_ACH.FrameResize(Sender: TObject);
begin
  inherited;
  evDBGrid1.Width := ClientWidth div 2;
  evLabel2.Left := evDBGrid2.Left + 6;
  evDBCheckGrid1.Width := evDBGrid1.Width;
  evLabel5.Left := evLabel2.Left;
  evPanel3.Height := (ClientHeight - evPanel1.Height) div 2;
end;

procedure TEDIT_NEW_CLIENT_ACH.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
var
 s : String;
begin
  inherited;
  s := GetReadOnlyClintCompanyListFilter;
  AddDSWithCheck(TMP_CO_FED_TAX_LIABILITIES, SupportDataSets, 'STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''' + s);
  AddDSWithCheck(TMP_CO_SUI_LIABILITIES, SupportDataSets, 'STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''' + s);
  AddDSWithCheck(TMP_CO_STATE_TAX_LIABILITIES, SupportDataSets, 'STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''' + s);
  AddDSWithCheck(TMP_CO_LOCAL_TAX_LIABILITIES, SupportDataSets, 'STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''' + s);
end;

procedure TEDIT_NEW_CLIENT_ACH.evBitBtn1Click(Sender: TObject);
var
  i: Integer;
  LiabilityAmount: Double;

  procedure PostManualACH;
  var
    d, EffDate: TDateTime;
  begin
    if LiabilityAmount <> 0 then
    with DM_COMPANY.CO_MANUAL_ACH do
    begin
      DM_COMPANY.CO_MANUAL_ACH.DataRequired(AlwaysFalseCond);
      d := Date;
      CalculateACHDates(d, EffDate, DM_COMPANY.CO.DEBIT_NUMBER_OF_DAYS_PRIOR_TAX.Value);
      Append;
      STATUS.Value := COMMON_REPORT_STATUS__PENDING;
      STATUS_DATE.Value := SysTime;
      DEBIT_DATE.Value := EffDate;
      CREDIT_DATE.Value := EffDate;
      PURPOSE_NOTES.Value := 'New client liabilities ACH';
      AMOUNT.Value := LiabilityAmount;
      FROM_CL_BANK_ACCOUNT_NBR.AsVariant := DM_COMPANY.CO.TAX_CL_BANK_ACCOUNT_NBR.AsVariant;
      TO_SB_BANK_ACCOUNTS_NBR.AsVariant := DM_COMPANY.CO.TAX_SB_BANK_ACCOUNT_NBR.AsVariant;
      Post;
      LiabilityAmount := 0;
    end;
  end;

  procedure AddManualACH(ds: TddTable);
  var
    clds: TddTable;
  begin
    if (DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsInteger <> ds.FieldByName('CL_NBR').AsInteger) or (DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger <> ds.FieldByName('CO_NBR').AsInteger) then
      Exit;

    if ds.FieldByName('CL_NBR').AsInteger <> ctx_DataAccess.ClientID then
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_MANUAL_ACH,
                    DM_COMPANY.CO_FED_TAX_LIABILITIES,
                    DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                    DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,
                    DM_COMPANY.CO_SUI_LIABILITIES]);
    ctx_DataAccess.OpenClient(ds.FieldByName('CL_NBR').AsInteger);
    DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(ds.FieldByName('CO_NBR').AsInteger));

    LiabilityAmount := LiabilityAmount + ds.FieldByName('AMOUNT').AsFloat;

    if ds = TMP_CO_FED_TAX_LIABILITIES then
      clds := DM_COMPANY.CO_FED_TAX_LIABILITIES
    else if ds = TMP_CO_SUI_LIABILITIES then
      clds := DM_COMPANY.CO_SUI_LIABILITIES
    else if ds = TMP_CO_STATE_TAX_LIABILITIES then
      clds := DM_COMPANY.CO_STATE_TAX_LIABILITIES
    else
      clds := DM_COMPANY.CO_LOCAL_TAX_LIABILITIES;
    clds.DataRequired('STATUS=''' + TAX_DEPOSIT_STATUS_PENDING + '''');
    Assert(clds.Locate(clds.TableName + '_NBR', ds[clds.TableName + '_NBR'], []));
    clds.Edit;
    clds.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_IMPOUNDED;
    clds.Post;
  end;

begin
  inherited;
  if (evDBGrid1.SelectedList.Count = 0) and
     (evDBGrid2.SelectedList.Count = 0) and
     (evDBCheckGrid1.SelectedList.Count = 0) and
     (evDBCheckGrid2.SelectedList.Count = 0) then
  begin
    EvErrMessage('Nothing selected, can not proceed');
    Exit;
  end;

  TMP_CO_SUI_LIABILITIES.DisableControls;
  TMP_CO_FED_TAX_LIABILITIES.DisableControls;
  TMP_CO_STATE_TAX_LIABILITIES.DisableControls;
  TMP_CO_LOCAL_TAX_LIABILITIES.DisableControls;
  ctx_StartWait;
  try
    LiabilityAmount := 0;

    DM_TEMPORARY.TMP_CO.DataRequired('CO_NBR>0' + GetReadOnlyClintCompanyListFilter);
    DM_TEMPORARY.TMP_CO.IndexFieldNames := 'CUSTOM_COMPANY_NUMBER';
    DM_TEMPORARY.TMP_CO.First;
    while not DM_TEMPORARY.TMP_CO.EOF do
    begin
      for i := 0 to Pred(evDBGrid1.SelectedList.Count) do
      begin
        TMP_CO_FED_TAX_LIABILITIES.GotoBookmark(evDBGrid1.SelectedList[i]);
        AddManualACH(TMP_CO_FED_TAX_LIABILITIES);
      end;
      for i := 0 to Pred(evDBGrid2.SelectedList.Count) do
      begin
        TMP_CO_SUI_LIABILITIES.GotoBookmark(evDBGrid2.SelectedList[i]);
        AddManualACH(TMP_CO_SUI_LIABILITIES);
      end;
      for i := 0 to Pred(evDBCheckGrid1.SelectedList.Count) do
      begin
        TMP_CO_STATE_TAX_LIABILITIES.GotoBookmark(evDBCheckGrid1.SelectedList[i]);
        AddManualACH(TMP_CO_STATE_TAX_LIABILITIES);
      end;
      for i := 0 to Pred(evDBCheckGrid2.SelectedList.Count) do
      begin
        TMP_CO_LOCAL_TAX_LIABILITIES.GotoBookmark(evDBCheckGrid2.SelectedList[i]);
        AddManualACH(TMP_CO_LOCAL_TAX_LIABILITIES);
      end;
      PostManualACH;
      DM_TEMPORARY.TMP_CO.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_MANUAL_ACH,
                  DM_COMPANY.CO_FED_TAX_LIABILITIES,
                  DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                  DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,
                  DM_COMPANY.CO_SUI_LIABILITIES]);
  finally
    ctx_EndWait;
    TMP_CO_SUI_LIABILITIES.EnableControls;
    TMP_CO_FED_TAX_LIABILITIES.EnableControls;
    TMP_CO_STATE_TAX_LIABILITIES.EnableControls;
    TMP_CO_LOCAL_TAX_LIABILITIES.EnableControls;
  end;

  if EvMessage('Manual ACH records are added. Would you like to go to Manual ACH screen to create a file?', mtConfirmation, [mbYes, mbNo]) = mrYes then
    ActivateFrameAs('Operations - Manual ACH', [], 1)
  else
  begin
    TMP_CO_SUI_LIABILITIES.Close;
    TMP_CO_FED_TAX_LIABILITIES.Close;
    TMP_CO_STATE_TAX_LIABILITIES.Close;
    TMP_CO_LOCAL_TAX_LIABILITIES.Close;
    TMP_CO_SUI_LIABILITIES.Open;
    TMP_CO_FED_TAX_LIABILITIES.Open;
    TMP_CO_STATE_TAX_LIABILITIES.Open;
    TMP_CO_LOCAL_TAX_LIABILITIES.Open;
  end;
end;

procedure TEDIT_NEW_CLIENT_ACH.Activate;
begin
  inherited;
  evDBGrid1.SelectAll;
  evDBGrid2.SelectAll;
  evDBCheckGrid1.SelectAll;
  evDBCheckGrid2.SelectAll;
end;

procedure TEDIT_NEW_CLIENT_ACH.TMP_CO_FED_TAX_LIABILITIESCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  DataSet['Type'] := ReturnDescription(FedTaxLiabilityType_ComboChoices, DataSet['Tax_Type']);
end;

procedure TEDIT_NEW_CLIENT_ACH.TMP_CO_STATE_TAX_LIABILITIESCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  DataSet['Type'] := ReturnDescription(StateTaxLiabilityType_ComboChoices, DataSet['Tax_Type']);
end;

initialization
  RegisterClass(TEDIT_NEW_CLIENT_ACH);

end.
