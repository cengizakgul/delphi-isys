// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PRENOTE_ACH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_GLOBAL_CO_BASE_2, Db,  Wwdatsrc, DBCtrls,
  StdCtrls, Buttons, Wwdbigrd, Grids, Wwdbgrid, ExtCtrls, ComCtrls,
  wwdblook,  SDataStructure, ISBasicClasses,
  kbmMemTable, ISKbmMemDataSet, SDDClasses, SDataDictbureau, EvContext,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictclient,
  SDataDicttemp, SACHTypes, EvUIUtils, EvUIComponents, isUIwwDBLookupCombo,
  EvClientDataSet, LMDCustomButton, LMDButton, isUILMDButton;

type    
  TPrenote_ACH = class(TEDIT_GLOBAL_CO_BASE_2)
    bbtnCreate: TevBitBtn;
    wwDBLookupCombo1: TevDBLookupCombo;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    Timer1: TTimer;
    procedure bbtnCreateClick(Sender: TObject);
    procedure wwDBLookupCombo1Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    function GetIfReadOnly: Boolean; override;
  private
  public
    procedure Activate; override;
    procedure Deactivate; override;
  end;

var
  Prenote_ACH: TPrenote_ACH;

implementation

uses
  EvConsts, EvUtils, evAchPrenote, EvCommonInterfaces, EvMainboard, isBaseClasses,
  SFrameEntry;

{$R *.DFM}

procedure TPrenote_ACH.Activate;
begin
  inherited;
  DM_SERVICE_BUREAU.SB_BANKS.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');

  // Show ONLY ACH Bank Accounts...
  with DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS do
  begin
    Filtered := True;
    Filter := 'ACH_ACCOUNT = ''' + GROUP_BOX_YES + '''';
  end;

  if wwDBLookupCombo1.Text = DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NAME_DESCRIPTION').AsString then
   wwDBLookupCombo1Change(nil)
  else
    wwDBLookupCombo1.Text := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('NAME_DESCRIPTION').AsString;

end;

procedure TPrenote_ACH.bbtnCreateClick(Sender: TObject);
var
  PrenoteCLAccts: Boolean;
  t: IevProcessPrenoteACHTask;
begin
  inherited;
  if EvMessage('Are you sure you want to create Prenote ACH for selected companies?', mtConfirmation, [mbYes, mbNo]) = mrYes then
  begin
    PrenoteCLAccts := (EvMessage('Would you like to prenote Client Bank Accounts?', mtConfirmation, [mbYes, mbNo]) = mrYes);
    begin
      t := mb_TaskQueue.CreateTask(QUEUE_TASK_PROCESS_PRENOTE_ACH, True) as IevProcessPrenoteACHTask;
      try

        LoadACHOptionsFromRegistryI(t.ACHOptions);
        t.Caption := 'Processing Prenote ACH';
        t.PrenoteCLAccounts := PrenoteCLAccts;

        t.ACHDataStream := SelectedCompanies.GetDataStream(True, False);

        ctx_EvolutionGUI.AddTaskQueue(t);
      finally
        t := nil;
      end;
      SetReadOnly(True);
    end;
  end;
end;

procedure TPrenote_ACH.Deactivate;
begin
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Filtered := False;
  wwdsList.DataSet.Filtered := False;
  inherited;
end;

procedure TPrenote_ACH.wwDBLookupCombo1Change(Sender: TObject);
var
 sFilter : String;
begin
  inherited;

  sFilter := GetReadOnlyClintCompanyListFilter(false);
  if sFilter <> '' then
    wwdsList.DataSet.Filter := 'ACH_SB_BANK_ACCOUNT_NBR = ' +
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString +  ' and ' + sFilter
  else
    wwdsList.DataSet.Filter := 'ACH_SB_BANK_ACCOUNT_NBR = ' +
      DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString;
  wwdsList.DataSet.Filtered := True;
  bbtnRemoveAll.Click;
end;

function TPrenote_ACH.GetIfReadOnly: Boolean;
var
  FlagInf: IevGlobalFlagInfo;
begin
  Result := inherited GetIfReadOnly;
  if not Result then
  begin
    FlagInf := mb_GlobalFlagsManager.GetLockedFlag(GF_SB_ACH_PROCESS);

    Result := Assigned(FlagInf);
  end;
end;

procedure TPrenote_ACH.Timer1Timer(Sender: TObject);
begin
  inherited;
  if GetIfReadOnly then
     SetReadOnly(true)
  else
    ClearReadOnly;
end;

initialization
  RegisterClass(TPRENOTE_ACH);

end.
