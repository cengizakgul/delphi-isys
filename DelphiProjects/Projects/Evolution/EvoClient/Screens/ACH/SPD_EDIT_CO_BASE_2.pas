// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_BASE_2;

interface

uses
   StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls,
  ComCtrls, DBCtrls, Controls, Classes, SFrameEntry, SDataStructure, Db,
  Wwdatsrc, SysUtils, Windows, SPackageEntry, EvUtils, Variants,
  SDDClasses, ISBasicClasses, SDataDictclient,
  SDataDicttemp, EvContext, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_CO_BASE_2 = class(TFrameEntry)
    Panel1: TEvPanel;
    Label5: TEvLabel;
    DBText1: TEvDBText;
    CompanyNameText: TEvDBText;
    lablClient: TEvLabel;
    dbtxClientNbr: TEvDBText;
    dbtxClientName: TEvDBText;
    PageControl1: TEvPageControl;
    TabSheet1: TTabSheet;
    Panel2: TEvPanel;
    Splitter1: TevSplitter;
    wwdbgridSelectClient: TEvDBGrid;
    pnlSubbrowse: TEvPanel;
    Panel3: TEvPanel;
    BitBtn1: TEvBitBtn;
    DM_TEMPORARY: TDM_TEMPORARY;
    dsCL: TevDataSource;
    DM_CLIENT: TDM_CLIENT;
    DM_COMPANY: TDM_COMPANY;
    procedure wwdbgridSelectClientDblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure wwdbgridSelectClientKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure CheckStatus;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); virtual;
    procedure SetDataSetsProps(aDS: TArrayDS; ListDataSet: TevClientDataSet); virtual;
    function GetDataSetConditions(sName: string): string; override;
    procedure AfterOpenCompany; virtual;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_CO_BASE_2.wwdbgridSelectClientDblClick(Sender: TObject);
begin
  inherited;
  BitBtn1.Click;
end;

procedure TEDIT_CO_BASE_2.BitBtn1Click(Sender: TObject);
var
  aDS: TArrayDS;
  Close: Boolean;
  i: Integer;
begin
  inherited;
  if PageControl1.ActivePage = TabSheet1 then
  begin
    CheckStatus;
    with Owner as TFramePackageTmpl do
      if BitBtn1.Enabled and ButtonEnabled(NavCommit) then
        ClickButton(NavCommit);

    Close := False;
    GetDataSetsToReopen(aDS, Close);
    for i := Low(aDS) to High(aDS) do
    begin
      if Close or
         (aDS[i].ClientID >= 0) and
          (aDS[i].ClientID <> wwdsList.DataSet.FieldByName('CL_NBR').AsInteger) or
         (aDS[i].OpenCondition <> GetDataSetConditions(aDS[i].TableName)) then
        aDS[i].Close;
    end;

    SetDataSetsProps(aDS, wwdsList.DataSet as TevClientDataSet);
    ctx_DataAccess.OpenClient(wwdsList.DataSet.FieldByName('CL_NBR').AsInteger);
    ctx_DataAccess.OpenDataSets(aDS);
    wwdsList.DoDataChange(Self, nil);
    AfterOpenCompany;
  end;
end;

procedure TEDIT_CO_BASE_2.wwdsListDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  BitBtn1.Enabled := (wwdsList.DataSet.RecordCount > 0) and
                     ((wwdsList.KeyValue <> wwdsMaster.KeyValue) or
                      (wwdsList.DataSet.FieldByName('CL_NBR').AsInteger <> TevClientDataSet(wwdsMaster.DataSet).ClientID));

  (Owner as TFramePackageTmpl).SetButton(NavInsert, not BitBtn1.Enabled);
  (Owner as TFramePackageTmpl).SetButton(NavDelete, not BitBtn1.Enabled);
end;

procedure TEDIT_CO_BASE_2.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_TEMPORARY.TMP_CO, SupportDataSets);
  AddDS(DM_COMPANY.CO, SupportDataSets);
  AddDS(DM_CLIENT.CL, SupportDataSets);
  if not Assigned(wwdsList.DataSet) then
    wwdsList.DataSet := DM_TEMPORARY.TMP_CO;
  if not Assigned(wwdsMaster.DataSet) then
    wwdsMaster.DataSet := DM_COMPANY.CO;
  if not Assigned(dsCL.DataSet) then
    dsCL.DataSet := DM_CLIENT.CL;
end;

procedure TEDIT_CO_BASE_2.Activate;
begin
  inherited;
  PageControl1.ActivePage := TabSheet1;
  wwdsList.DataSet.Locate('CL_NBR;CO_NBR', VarArrayOf([TevClientDataSet(wwdsMaster.DataSet).ClientID, wwdsMaster.DataSet['CO_NBR']]), []);
  if not BitBtn1.Enabled then
    BitBtn1Click(Self);
end;

procedure TEDIT_CO_BASE_2.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  BitBtn1.Click;
end;

procedure TEDIT_CO_BASE_2.wwdbgridSelectClientKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (Shift = [ssCtrl]) then
  begin
    BitBtn1.Click;
    Key := 0;
  end;
end;

procedure TEDIT_CO_BASE_2.GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL, aDS);
  AddDS(DM_COMPANY.CO, aDS);
  AddDS(GetDefaultDataSet, aDS);
end;

procedure TEDIT_CO_BASE_2.SetDataSetsProps(aDS: TArrayDS; ListDataSet: TevClientDataSet);
var
  i: Integer;
begin
  for i := Low(aDS) to High(aDS) do
    if not aDS[i].Active then
    begin
      aDS[i].ClientID := ListDataSet.FieldByName('CL_NBR').AsInteger;
      aDS[i].RetrieveCondition := GetDataSetConditions(aDS[i].TableName);
    end;
end;

procedure TEDIT_CO_BASE_2.AfterOpenCompany;
begin
end;

function TEDIT_CO_BASE_2.GetDataSetConditions(sName: string): string;
begin
  Result := inherited GetDataSetConditions(sName);
  if (sName <> 'CL') and
     (Copy(sName, 1, 3) <> 'CL_') and
     wwdsList.DataSet.Active then
    Result := 'CO_NBR = ' + IntToStr(wwdsList.DataSet.FieldByName('CO_NBR').AsInteger);
end;

procedure TEDIT_CO_BASE_2.CheckStatus;
begin
  with Owner as TFramePackageTmpl do
    if ButtonEnabled(NavOK) then
      ClickButton(NavOK);
end;

procedure TEDIT_CO_BASE_2.Deactivate;
begin
  CheckStatus;
  inherited;
end;

end.
