// Screens of "Operations"
unit scr_ACH;

interface

uses
  SACHScr,
  SPD_EDIT_GLOBAL_CO_BASE_2,
  SPD_EDIT_CO_BASE_2,  
  SPD_PRENOTE_ACH,
  SPD_EDIT_CO_MANUAL_ACH,
  SPD_CASH_MANAGEMENT,
  SPD_EDIT_NEW_CLIENT_ACH;

implementation

end.
