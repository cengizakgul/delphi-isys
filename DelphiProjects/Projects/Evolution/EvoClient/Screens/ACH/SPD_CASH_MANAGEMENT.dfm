inherited PROCESS_SB_ACH_REMOTE: TPROCESS_SB_ACH_REMOTE
  Width = 730
  Height = 618
  object pcACHFile: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 730
    Height = 618
    HelpContext = 38508
    ActivePage = tsFlags
    Align = alClient
    TabOrder = 0
    OnChanging = pcACHFileChanging
    object tsAchAccounts: TTabSheet
      Caption = 'Filter'
      object evPanel1: TevPanel
        Left = 0
        Top = 0
        Width = 722
        Height = 590
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 0
        object lblStatusFrom: TevLabel
          Left = 12
          Top = 41
          Width = 56
          Height = 13
          Caption = 'ACH status:'
        end
        object Label1: TevLabel
          Left = 12
          Top = 94
          Width = 66
          Height = 13
          Caption = 'Payroll Types:'
        end
        object evLabel2: TevLabel
          Left = 12
          Top = 67
          Width = 61
          Height = 13
          Caption = 'Date Range:'
        end
        object evLabel1: TevLabel
          Left = 12
          Top = 15
          Width = 120
          Height = 13
          Caption = 'Cash Management Bank:'
        end
        object btnGetDataForAch: TevBitBtn
          Left = 142
          Top = 118
          Width = 89
          Height = 25
          Hint = 'Find ACH Data for Selected Criteria'
          Caption = 'Find'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = btnGetDataForAchClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D4DDDDDDDDDD
            DDDDDFDDDDDDDDDDDDDD4C486666666666DDF8FD8888888888DDCCC48FFFFFFF
            F6DD888FDDDDDDDDD8DDDCCC48FFFFFFF6DDD888FDDDDDDDD8DDDDCCC48F777F
            F6DDDD888FDDDDDDD8DDDD6CCC48CCC476DDDDD888FF888FD8DDDD6FCC4C877C
            C4DDDD8D88D8DDD88FDDDD6FF48777777CDDDD8DDD8DDDDDD8FDDD6F8C766666
            7C4DDD8DD8D88888D8FDDD6FC877777777C8DD8D8DDDDDDDDD8FDD6FC8766666
            67C8DD8D8D8888888D8FDD6FC477777777C8DD8D8DDDDDDDDD8FDD6F7C876666
            7C4DDD8DD8D88888D8FDDD6FFC4777777C8DDD8DD8DDDDDDD8FDDD6666CC477C
            C8DDDD888D88DDD88FDDDDDDDDDDCCC4DDDDDDDDDDDD888FDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object wwdbgSB_BANKS: TevDBLookupCombo
          Left = 142
          Top = 11
          Width = 181
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME_DESCRIPTION'#9'40'#9'Name'#9'F'
            'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'Bank Account'#9'F'
            'ACH_ORIGIN_SB_BANKS_NBR'#9'14'#9'Ach Origin SB Nbr'#9'F')
          LookupTable = SB_BANK_ACCOUNTS
          LookupField = 'SB_BANK_ACCOUNTS_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = wwdbgSB_BANKSChange
        end
        object cbStatusFrom: TevComboBox
          Left = 142
          Top = 38
          Width = 181
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
          OnChange = cbStatusFromChange
          Items.Strings = (
            'Ach Ready'
            'Ach On Hold'
            'Ach Ready or Hold'
            'Ach Sent'
            'Ach Deleted'
            'Ach Processing')
        end
        object dtDateFrom: TevDateTimePicker
          Left = 142
          Top = 65
          Width = 89
          Height = 21
          Date = 36131.588787268500000000
          Time = 36131.588787268500000000
          TabOrder = 2
        end
        object cbPayrollType: TevComboBox
          Left = 142
          Top = 91
          Width = 181
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 5
          Items.Strings = (
            'All'
            'All but QEC, Tax dep. & corr.'
            'QEC and Tax correction')
        end
        object dtDateTO: TevDateTimePicker
          Left = 234
          Top = 65
          Width = 89
          Height = 21
          Date = 36131.588787268500000000
          Time = 36131.588787268500000000
          TabOrder = 3
        end
        object cbCheckDateFilter: TevCheckBox
          Left = 328
          Top = 67
          Width = 163
          Height = 17
          Caption = 'Filter By Check Date'
          TabOrder = 4
        end
      end
    end
    object tsPayrolls: TTabSheet
      Caption = 'Select'
      ImageIndex = 2
      OnEnter = tsPayrollsEnter
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 722
        Height = 549
        DisableThemesInTitle = False
        ControlType.Strings = (
          'SELECTED;CheckBox;True;False')
        Selected.Strings = (
          'SELECTED'#9'9'#9'Selected'#9'F'
          'CUSTOM_CLIENT_NUMBER'#9'10'#9'Cl Number'#9'T'
          'CUSTOM_COMPANY_NUMBER'#9'10'#9'Co Number'#9'T'
          'NAME'#9'24'#9'Name'#9'T'
          'ACH_DATE'#9'9'#9'Process Date'#9'T'
          'CHECK_DATE'#9'9'#9'Check Date'#9'T'
          'RUN_NUMBER'#9'10'#9'Run Number'#9'T'
          'PR_TYPE_DESCRIPTION'#9'10'#9'PR Type'#9'T'
          'STATUS'#9'7'#9'Status'#9'F'
          'SENT_DATE'#9'20'#9'Date Sent'#9'F'
          'SENT_USER'#9'10'#9'User Name'#9'F'
          'SENT_FILE'#9'20'#9'File Name'#9'F'
          'NEW_STATUS'#9'7'#9'Status (new)'#9'F'
          'TRUST_IMPOUND'#9'15'#9'Trust Impound'#9'F'
          'TAX_IMPOUND'#9'15'#9'Tax Impound'#9'F'
          'DD_IMPOUND'#9'15'#9'DD Impound'#9'F'
          'BILLING_IMPOUND'#9'15'#9'Billing Impound'#9'F'
          'WC_IMPOUND'#9'15'#9'WC Impound'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TPROCESS_SB_ACH_REMOTE\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
        Align = alClient
        DataSource = wwdsTMP_CO_PR_ACH2
        MultiSelectOptions = [msoShiftSelect]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        DefaultSort = 'CUSTOM_CLIENT_NUMBER;CUSTOM_COMPANY_NUMBER'
        NoFire = False
      end
      object evPanel2: TevPanel
        Left = 0
        Top = 549
        Width = 722
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object butnSelectAll: TevBitBtn
          Left = 8
          Top = 8
          Width = 89
          Height = 25
          Caption = 'Select None'
          TabOrder = 0
          OnClick = butnSelectAllClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDD0000DDDDDDDDDDDD8888DDDDDDDDDDDD00D0DD88DDDD
            D88D88D8DDDDDDDDDDDD0D80DD008DDD80088D88DDFFDDDDDFFD0000D8FF08D8
            0FF88888DD88FDDDF88DDDDDD8FFF080FFF8DDDDDD888FDF888D0000DD8FFF0F
            FF8D8888DDD888F888DD00D0DDD8FFFFF8DD88D8DDDD88888DDD0D80DDD80FFF
            08DD8D88DDDDF888FDDD0000DD80FFFFF08D8888DDDF88888FDDDDDDD80FFF8F
            FF08DDDDDDF888D888FD0000D8FFF8D8FFF88888DD888DDD888D00D0D8FF8DDD
            8FF888D8DD88DDDDD88D0D80DD88DDDDD88D8D88DDDDDDDDDDDD0000DDDDDDDD
            DDDD8888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object evSpeedButton1: TevBitBtn
          Left = 148
          Top = 8
          Width = 89
          Height = 25
          Caption = 'Options'
          TabOrder = 1
          OnClick = evSpeedButton1Click
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            2DDDDDDDDDDDDDDDFDDDDDDDD000DDD2A2DDDDDDD888DDDF8FDDDDDDD0F00D2A
            AA2DDDDDD8D88DF888FDDDDDD000DDAA8A2DDDDDD888DD88D8FDDDDDDD0DDDDD
            DAA2DDDDDD8DDDDDD88FDDDDDD0DDDDD2DA2DDDDDD8DDDDDFD8FDDDDD000DDD2
            A2DDDDDDD888DDDF8FDDDDDDD0F00DDAAA2DDDDDD8D88DF888FDDDDDD000DDAA
            8A2DDDDDD888DD88D8FDDDDDDD0DDDDDDAA2DDDDDD8DDDDDD88FDDDDDD0DDDDD
            2DA2DDDDDD8DDDDDFD8FDDDDD000DDD2A2DDDDDDD888DDDF8FDDDD0000F00D2A
            AA2DDD8888D88DF888FDDD0DD000DDAA8A2DDD8DD888DD88D8FDDD0DDDDDDDDD
            DAA2DD8DDDDDDDDDD88FDD0DDDDDDDDDDDA2DD8DDDDDDDDDDD8F}
          NumGlyphs = 2
          Margin = 0
        end
      end
      object TrustComboBox: TevDBComboBox
        Left = 376
        Top = 40
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TRUST_IMPOUND'
        DataSource = wwdsTMP_CO_PR_ACH2
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'ACH'#9'A'
          'External Check'#9'E'
          'Internal Check'#9'I'
          'Other'#9'O'
          'Wire Transfer'#9'W'
          'Cash'#9'M')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
        OnDropDown = TrustComboBoxDropDown
        OnCloseUp = TrustComboBoxCloseUp
      end
      object BillingComboBox: TevDBComboBox
        Left = 376
        Top = 112
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'BILLING_IMPOUND'
        DataSource = wwdsTMP_CO_PR_ACH2
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'ACH'#9'A'
          'External Check'#9'E'
          'Internal Check'#9'I'
          'Other'#9'O'
          'Wire Transfer'#9'W'
          'Cash'#9'M')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
        OnDropDown = TrustComboBoxDropDown
        OnCloseUp = TrustComboBoxCloseUp
      end
      object TaxComboBox: TevDBComboBox
        Left = 376
        Top = 64
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TAX_IMPOUND'
        DataSource = wwdsTMP_CO_PR_ACH2
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'ACH'#9'A'
          'External Check'#9'E'
          'Internal Check'#9'I'
          'Other'#9'O'
          'Wire Transfer'#9'W'
          'Cash'#9'M')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 4
        UnboundDataType = wwDefault
        OnDropDown = TrustComboBoxDropDown
        OnCloseUp = TrustComboBoxCloseUp
      end
      object DDComboBox: TevDBComboBox
        Left = 376
        Top = 88
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'DD_IMPOUND'
        DataSource = wwdsTMP_CO_PR_ACH2
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'ACH'#9'A'
          'External Check'#9'E'
          'Internal Check'#9'I'
          'Other'#9'O'
          'Wire Transfer'#9'W'
          'Cash'#9'M')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
        OnDropDown = TrustComboBoxDropDown
        OnCloseUp = TrustComboBoxCloseUp
      end
      object WcComboBox: TevDBComboBox
        Left = 376
        Top = 136
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'WC_IMPOUND'
        DataSource = wwdsTMP_CO_PR_ACH2
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'ACH'#9'A'
          'External Check'#9'E'
          'Internal Check'#9'I'
          'Other'#9'O'
          'Wire Transfer'#9'W'
          'Cash'#9'M')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
        OnDropDown = TrustComboBoxDropDown
        OnCloseUp = TrustComboBoxCloseUp
      end
    end
    object tsFlags: TTabSheet
      HelpContext = 38508
      Caption = 'Process Options'
      ImageIndex = 1
      OnShow = tsFlagsShow
      object evPanel3: TevPanel
        Left = 0
        Top = 0
        Width = 722
        Height = 590
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 0
        object evPanel4: TevPanel
          Left = 1
          Top = 535
          Width = 720
          Height = 54
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object lACHProcessMsg: TLabel
            Left = 10
            Top = 38
            Width = 82
            Height = 13
            Caption = 'lACHProcessMsg'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object PreprocessAchBtn: TevBitBtn
            Tag = 1
            Left = 9
            Top = 0
            Width = 140
            Height = 34
            Hint = 'Preprocess ACH'
            Caption = 'Pre-Process ACH'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = PreprocessAchBtnClick
            Color = clBlack
            Margin = 0
          end
          object ProcessAchBtn: TevBitBtn
            Tag = 1
            Left = 155
            Top = 0
            Width = 140
            Height = 34
            Hint = 'Preprocess ACH'
            Caption = 'Process ACH'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = ProcessAchBtnClick
            Color = clBlack
            Glyph.Data = {
              36030000424D3603000000000000360000002800000010000000100000000100
              1800000000000003000000000000000000000000000000000000C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C300FFFF00FFFF00FFFFC3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C300FFFF00FFFF00FFFFC3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C300FFFF00FFFF00FFFFC3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C300BF9400BF9400BF94C3
              C3C300914B00914B00914BC3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C300BF9400BF9400BF94C3C3C300914B00914B00914BC3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C300BF9400BF9400BF94C3
              C3C300914B00914B00914BC3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C300FBC400FBC400FBC4C3
              C3C300D17100D17100D171C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C300FBC400FBC400FBC4C3C3C300D17100D17100D171C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C300FBC400FBC400FBC4C3
              C3C300D17100D17100D171C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
              C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3}
            Margin = 0
          end
        end
        object evGroupBox1: TevGroupBox
          Left = 11
          Top = 21
          Width = 569
          Height = 405
          Caption = 'ACH Options'
          TabOrder = 1
          object lblStatusTo: TevLabel
            Left = 9
            Top = 19
            Width = 83
            Height = 13
            Caption = 'Change status to:'
            FocusControl = cbStatusTo
          end
          object evLabel3: TevLabel
            Left = 9
            Top = 49
            Width = 36
            Height = 13
            Caption = 'Offsets:'
          end
          object evLabel9: TevLabel
            Left = 228
            Top = 28
            Width = 116
            Height = 13
            Caption = 'Post process ACH report'
            FocusControl = cbPostProcessReport
          end
          object rgBlockNegChecks: TevCheckBox
            Left = 12
            Top = 88
            Width = 137
            Height = 16
            Caption = 'Block Negative DD'
            TabOrder = 0
          end
          object gbCombine: TevGroupBox
            Left = 12
            Top = 138
            Width = 157
            Height = 67
            Caption = 'Combine'
            TabOrder = 1
            object evLabel4: TevLabel
              Left = 12
              Top = 42
              Width = 47
              Height = 13
              Caption = 'Threshold'
            end
            object eThreshold: TevEdit
              Left = 67
              Top = 39
              Width = 78
              Height = 21
              Enabled = False
              TabOrder = 0
              Text = '0.00'
              OnChange = eThresholdChange
            end
            object cbCombine: TevComboBox
              Left = 12
              Top = 14
              Width = 133
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 1
              OnClick = cbCombineClick
              Items.Strings = (
                'Do not Combine'
                'Co Trans No DD'
                'Company and SB Trans'
                'Co Trans w DD'
                'Co Trans W +DD')
            end
          end
          object gbEffectiveDate: TevGroupBox
            Left = 12
            Top = 212
            Width = 157
            Height = 49
            Caption = 'Effective Date'
            TabOrder = 2
            object cbEffectiveDate: TevCheckBox
              Left = 12
              Top = 15
              Width = 133
              Height = 27
              Caption = 'Select ACH files by effective date'
              TabOrder = 0
              WordWrap = True
              OnClick = cbEffectiveDateClick
            end
          end
          object cbStatusTo: TevComboBox
            Left = 100
            Top = 16
            Width = 105
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 3
            OnChange = cbStatusToChange
            Items.Strings = (
              'Ach Sent'
              'Ach On Hold'
              'Ach Deleted'
              'Ach Ready')
          end
          object cbUseOffsets: TevComboBox
            Left = 100
            Top = 47
            Width = 105
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 4
            Items.Strings = (
              'No Offsets'
              'Use only EE DD'
              'Use Offsets')
          end
          object gbOther: TevGroupBox
            Left = 180
            Top = 75
            Width = 186
            Height = 316
            Caption = 'Other'
            TabOrder = 5
            object cbUseSBEIN: TevCheckBox
              Left = 12
              Top = 64
              Width = 80
              Height = 17
              Caption = 'Use SB EIN'
              TabOrder = 2
              OnClick = cbUseSBEINClick
            end
            object cbBalanceBatches: TevCheckBox
              Left = 12
              Top = 16
              Width = 101
              Height = 16
              Caption = 'Balance batches'
              TabOrder = 0
              OnClick = cbBalanceBatchesClick
            end
            object cbCTS: TevCheckBox
              Left = 12
              Top = 40
              Width = 62
              Height = 17
              Caption = 'CTS'
              TabOrder = 1
              OnClick = cbCTSClick
            end
            object cbSunTrust: TevCheckBox
              Left = 12
              Top = 88
              Width = 62
              Height = 17
              Caption = 'SunTrust'
              TabOrder = 3
              OnClick = cbSunTrustClick
            end
            object cbFedReserve: TevCheckBox
              Left = 12
              Top = 126
              Width = 84
              Height = 17
              Caption = 'Fed Reserve'
              TabOrder = 5
              OnClick = cbFedReserveClick
            end
            object cbIntercept: TevCheckBox
              Left = 12
              Top = 150
              Width = 84
              Height = 17
              Caption = 'Intercept'
              TabOrder = 6
              OnClick = cbInterceptClick
            end
            object cbBlockSBCredit: TevCheckBox
              Left = 28
              Top = 104
              Width = 93
              Height = 17
              Caption = 'Block SB Credit'
              Enabled = False
              TabOrder = 4
              OnClick = cbBlockSBCreditClick
            end
            object cbPayrollHeader: TevCheckBox
              Left = 12
              Top = 174
              Width = 101
              Height = 17
              Caption = 'Payroll Header'
              TabOrder = 7
              OnClick = cbPayrollHeaderClick
            end
            object cbBlockDebits: TevCheckBox
              Left = 12
              Top = 197
              Width = 101
              Height = 17
              Caption = 'Block Debits'
              TabOrder = 8
              OnClick = cbBlockDebitsClick
            end
            object cbDescReversal: TevCheckBox
              Left = 12
              Top = 220
              Width = 133
              Height = 17
              Caption = 'Show Reversal Detail'
              TabOrder = 9
              OnClick = cbDescReversalClick
            end
            object cbBankOne: TevCheckBox
              Left = 12
              Top = 243
              Width = 133
              Height = 17
              Caption = 'Bank One'
              TabOrder = 10
              OnClick = cbBankOneClick
            end
            object cbBlockNegativeLiabilities: TevCheckBox
              Left = 12
              Top = 267
              Width = 157
              Height = 17
              Caption = 'Block Negative Liabilities'
              TabOrder = 11
              OnClick = cbBlockNegativeLiabilitiesClick
            end
            object cbHSA: TevCheckBox
              Left = 12
              Top = 291
              Width = 157
              Height = 17
              Caption = 'HSA'
              TabOrder = 12
              OnClick = cbHSAClick
            end
          end
          object gbHandShake: TGroupBox
            Left = 383
            Top = 88
            Width = 161
            Height = 125
            Caption = 'Handshake'
            TabOrder = 6
            object evLabel5: TevLabel
              Left = 12
              Top = 18
              Width = 73
              Height = 13
              Caption = '$$ADD Header'
              FocusControl = eHeader
            end
            object evLabel6: TevLabel
              Left = 12
              Top = 66
              Width = 68
              Height = 13
              Caption = '$$END Footer'
              FocusControl = eFooter
            end
            object eHeader: TevEdit
              Left = 13
              Top = 39
              Width = 132
              Height = 21
              TabOrder = 0
              OnChange = eHeaderChange
            end
            object eFooter: TevEdit
              Left = 13
              Top = 86
              Width = 132
              Height = 21
              TabOrder = 1
              OnChange = eFooterChange
            end
          end
          object gbCoId: TevGroupBox
            Left = 383
            Top = 216
            Width = 161
            Height = 50
            Caption = 'Company ID'
            TabOrder = 7
            object evLabel7: TevLabel
              Left = 15
              Top = 21
              Width = 11
              Height = 13
              Caption = 'ID'
            end
            object eCoId: TevEdit
              Left = 33
              Top = 18
              Width = 24
              Height = 21
              MaxLength = 1
              TabOrder = 0
              Text = '1'
              OnChange = eCoIdChange
            end
          end
          object gbEINOverride: TevGroupBox
            Left = 383
            Top = 273
            Width = 161
            Height = 50
            Caption = 'SB EIN Override'
            Enabled = False
            TabOrder = 8
            object evLabel8: TevLabel
              Left = 11
              Top = 21
              Width = 18
              Height = 13
              Caption = 'EIN'
              Enabled = False
            end
            object eSBEINOverride: TevDBComboBox
              Left = 33
              Top = 18
              Width = 116
              Height = 21
              ShowButton = True
              Style = csDropDown
              MapList = False
              AllowClearKey = True
              AutoDropDown = True
              DropDownCount = 8
              Enabled = False
              HistoryList.Section = 'SBEINs'
              HistoryList.FileName = 'Software\Evolution\Evolution\Misc\ACH'
              HistoryList.MaxSize = 10
              HistoryList.StorageType = stRegistry
              HistoryList.Enabled = True
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 0
              UnboundDataType = wwDefault
              OnChange = eSBEINOverrideChange
            end
          end
          object cbDisplayCoNbrAndName: TCheckBox
            Left = 383
            Top = 336
            Width = 137
            Height = 17
            Caption = 'Display Co# and Name'
            TabOrder = 9
            OnClick = cbDisplayCoNbrAndNameClick
          end
          object rgBlockCompanyDD: TevCheckBox
            Left = 12
            Top = 112
            Width = 137
            Height = 16
            Caption = 'Block Company DD'
            TabOrder = 10
            OnClick = rgBlockCompanyDDClick
          end
          object cbPostProcessReport: TevComboBox
            Left = 223
            Top = 46
            Width = 322
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 11
            OnClick = cbPostProcessReportClick
            Items.Strings = (
              'Do not Combine'
              'Company Trans Only'
              'Company and SB Trans')
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 613
  end
  inherited wwdsDetail: TevDataSource
    Left = 654
    Top = 26
  end
  inherited wwdsList: TevDataSource
    Left = 570
  end
  object wwdsTMP_CO_PR_ACH: TevDataSource
    Left = 528
    Top = 112
  end
  object wwdsTMP_CO_PR_ACH2: TevDataSource
    Left = 584
    Top = 272
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 568
    Top = 160
  end
  object SB_BANK_ACCOUNTS: TSB_BANK_ACCOUNTS
    ProviderName = 'SB_BANK_ACCOUNTS_PROV'
    Filter = 'ACH_ACCOUNT='#39'Y'#39
    Filtered = True
    Left = 288
    Top = 144
  end
end
