object DM_IM_RAPID: TDM_IM_RAPID
  OldCreateOrder = False
  Left = 624
  Top = 191
  Height = 501
  Width = 843
  object LOCAL_MATCHING: TevClientDataSet
    MasterFields = 'CL_NBR;CO_NBR'
    DetailFields = 'CL_NBR;CO_NBR'
    MasterSource = evdsImportMaster
    Left = 42
    Top = 56
    object LOCAL_MATCHINGCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object LOCAL_MATCHINGCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object LOCAL_MATCHINGSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object LOCAL_MATCHINGRAPIDLOCAL: TStringField
      FieldName = 'RAPIDLOCAL'
    end
    object LOCAL_MATCHINGEVOLOCAL: TStringField
      FieldName = 'EVOLOCAL'
    end
    object LOCAL_MATCHINGLocalNbr: TIntegerField
      FieldName = 'LocalNbr'
    end
  end
  object evcsImportMaster: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DETYPEK'
        DataType = ftInteger
      end
      item
        Name = 'DETYPEQ'
        DataType = ftInteger
      end
      item
        Name = 'EFFECTIVE_START_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'WEEK_NBR'
        DataType = ftInteger
      end
      item
        Name = 'MONTH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'PERIOD_BEGIN_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'PERIOD_END_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'JAN'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'FEB'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MAR'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'APR'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MAY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'JUN'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'JUL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AUG'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SEP'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'OCT'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NOV'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DEC'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'LIAB_STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'ADJUST_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'DUE_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'USE_DATE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'COL_FOR_QUARTERLIES'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'StandardAverageHours'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'DETYPEX'
        DataType = ftInteger
      end>
    Left = 42
    Top = 8
    object evcsImportMasterCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object evcsImportMasterNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object evcsImportMasterCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object evcsImportMasterCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object evcsImportMasterDETYPEK: TIntegerField
      FieldName = 'DETYPEK'
    end
    object evcsImportMasterDETYPEQ: TIntegerField
      FieldName = 'DETYPEQ'
    end
    object evcsImportMasterDTYPEX: TIntegerField
      DisplayWidth = 10
      FieldName = 'DETYPEX'
    end
    object evcsImportMasterEFFECTIVE_START_DATE: TDateTimeField
      FieldName = 'EFFECTIVE_START_DATE'
    end
    object evcsImportMasterWEEK_NBR: TIntegerField
      FieldName = 'WEEK_NBR'
    end
    object evcsImportMasterMONTH_NBR: TIntegerField
      FieldName = 'MONTH_NBR'
    end
    object evcsImportMasterCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
    object evcsImportMasterPERIOD_BEGIN_DATE: TDateTimeField
      FieldName = 'PERIOD_BEGIN_DATE'
    end
    object evcsImportMasterPERIOD_END_DATE: TDateTimeField
      FieldName = 'PERIOD_END_DATE'
    end
    object evcsImportMasterJAN: TStringField
      FieldName = 'JAN'
      Size = 1
    end
    object evcsImportMasterFEB: TStringField
      FieldName = 'FEB'
      Size = 1
    end
    object evcsImportMasterMAR: TStringField
      FieldName = 'MAR'
      Size = 1
    end
    object evcsImportMasterAPR: TStringField
      FieldName = 'APR'
      Size = 1
    end
    object evcsImportMasterMAY: TStringField
      FieldName = 'MAY'
      Size = 1
    end
    object evcsImportMasterJUN: TStringField
      FieldName = 'JUN'
      Size = 1
    end
    object evcsImportMasterJUL: TStringField
      FieldName = 'JUL'
      Size = 1
    end
    object evcsImportMasterAUG: TStringField
      FieldName = 'AUG'
      Size = 1
    end
    object evcsImportMasterSEP: TStringField
      FieldName = 'SEP'
      Size = 1
    end
    object evcsImportMasterOCT: TStringField
      FieldName = 'OCT'
      Size = 1
    end
    object evcsImportMasterNOV: TStringField
      FieldName = 'NOV'
      Size = 1
    end
    object evcsImportMasterDEC: TStringField
      FieldName = 'DEC'
      Size = 1
    end
    object evcsImportMasterLIAB_STATUS: TStringField
      FieldName = 'LIAB_STATUS'
      Size = 1
    end
    object evcsImportMasterADJUST_TYPE: TStringField
      FieldName = 'ADJUST_TYPE'
      Size = 1
    end
    object evcsImportMasterDUE_DATE: TDateTimeField
      FieldName = 'DUE_DATE'
    end
    object evcsImportMasterUSE_DATE: TStringField
      FieldName = 'USE_DATE'
      Size = 1
    end
    object evcsImportMasterCOL_FOR_QUARTERLIES: TStringField
      FieldName = 'COL_FOR_QUARTERLIES'
      Size = 1
    end
    object evcsImportMasterStandardAverageHours: TStringField
      FieldName = 'StandardAverageHours'
      Size = 2
    end
  end
  object ED_MATCHING: TevClientDataSet
    MasterFields = 'CL_NBR;CO_NBR'
    DetailFields = 'CL_NBR;CO_NBR'
    MasterSource = evdsImportMaster
    Left = 46
    Top = 104
    object ED_MATCHINGCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object ED_MATCHINGCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object ED_MATCHINGRapidED: TStringField
      FieldName = 'RapidED'
      Size = 5
    end
    object ED_MATCHINGEvoED: TStringField
      FieldName = 'EvoED'
      Size = 5
    end
    object ED_MATCHINGCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
    object ED_MATCHINGDescription: TStringField
      FieldName = 'Description'
    end
  end
  object evcsRapidED: TevClientDataSet
    Filter = 'HasBeenMatched='#39'F'#39
    OnFilterRecord = evcsRapidEDFilterRecord
    Left = 302
    Top = 16
    object evcsRapidEDRapidED: TStringField
      DisplayWidth = 5
      FieldName = 'RapidED'
      Size = 5
    end
    object evcsRapidEDHasBeenMatched: TStringField
      FieldName = 'HasBeenMatched'
      Visible = False
      Size = 1
    end
  end
  object evcsRapidSpecial: TevClientDataSet
    Filter = 'HasBeenMatched='#39'F'#39
    Filtered = True
    Left = 302
    Top = 64
    object evcsRapidSpecialSpecialOrPercent: TStringField
      DisplayLabel = 'Special or Percent'
      DisplayWidth = 2
      FieldName = 'SpecialOrPercent'
      Size = 2
    end
    object evcsRapidSpecialHasBeenMatched: TStringField
      FieldName = 'HasBeenMatched'
      Visible = False
      Size = 1
    end
  end
  object evcsDirectDeposits: TevClientDataSet
    Left = 506
    Top = 64
    object evcsDirectDepositsSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object evcsDirectDepositsTIMESTOPRENOTE: TStringField
      FieldName = 'TIMESTOPRENOTE'
      Size = 1
    end
    object evcsDirectDepositsPRENOTE: TStringField
      FieldName = 'PRENOTE'
      Size = 1
    end
    object evcsDirectDepositsAMOUNT: TStringField
      FieldName = 'AMOUNT'
      Size = 12
    end
    object evcsDirectDepositsAMOUNTORPERCENT: TStringField
      FieldName = 'AMOUNTORPERCENT'
      Size = 1
    end
    object evcsDirectDepositsDECODE: TStringField
      FieldName = 'DECODE'
      Size = 2
    end
    object evcsDirectDepositsACCOUNT: TStringField
      FieldName = 'ACCOUNT'
    end
    object evcsDirectDepositsBANK: TStringField
      FieldName = 'BANK'
    end
    object evcsDirectDepositsSEQUENCE: TStringField
      FieldName = 'SEQUENCE'
      Size = 7
    end
    object evcsDirectDepositsEMPL: TStringField
      FieldName = 'EMPL'
      Size = 6
    end
  end
  object evcsEmployee: TevClientDataSet
    FieldDefs = <
      item
        Name = 'Status'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Extrareal'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Extraoptions10'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Extraoptions9'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Extraoptions8'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Extraoptions7'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Extraoptions6'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Extraoptions5'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Extraoptions4'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Extraoptions3'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Extraoptions2'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Extraoptions1'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Weeksworked4'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Weeksworked3'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Weeksworked2'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Weeksworked1'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'W2chars5'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2chars4'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2chars3'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2chars2'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'W2chars1'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Payincramount'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Payincrdate'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'GL'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Shiftdiffrate3'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Shiftdiffrate2'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Shiftdiffrate1'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Shiftdiffcode'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Workshift'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Localtaxpercent3'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Localtaxpercent2'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Localtaxpercent1'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Localtaxdesc3'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Localtaxdesc2'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Localtaxdesc1'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Localtaxcode3'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Localtaxcode2'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Localtaxcode1'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'Localtaxstate3'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Localtaxstate2'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Localtaxstate1'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Voluntary4'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Voluntary3'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Voluntary2'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Voluntary1'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Additional4'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Additional3'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Additional2'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Additional1'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Taxexempt23'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt22'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt21'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt20'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt19'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt18'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt17'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt16'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt15'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt14'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt13'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt12'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt11'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt10'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt9'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt8'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt7'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt6'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt5'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt4'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt3'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt2'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Taxexempt1'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Statetaxcode3'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Statetaxcode2'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Statetaxcode1'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Clocknumber'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Code1099'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Eic'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Union'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Workcomp'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Eeo'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Pension'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Ftpt'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'Exempt'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Averagehours'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Salary'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Ovdept4'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovdept3'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovdept2'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovdept1'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovbranch4'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovbranch3'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovbranch2'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovbranch1'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovdivision4'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovdivision3'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovdivision2'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Ovdivision1'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Rate5'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Rate4'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Rate3'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Rate2'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Rate1'
        DataType = ftString
        Size = 12
      end
      item
        Name = 'Payfreq'
        DataType = ftString
        Size = 7
      end
      item
        Name = 'Smaritalstatus'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Fmaritalstatus'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Sdependents'
        DataType = ftString
        Size = 7
      end
      item
        Name = 'Fdependents'
        DataType = ftString
        Size = 7
      end
      item
        Name = 'Position'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Sex'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TermCode'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'Nextincdate'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Changedate'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Termdate'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Raisedate'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Hiredate'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Birthdate'
        DataType = ftString
        Size = 8
      end
      item
        Name = 'Ssi'
        DataType = ftString
        Size = 11
      end
      item
        Name = 'Phone'
        DataType = ftString
        Size = 14
      end
      item
        Name = 'Zip'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'State'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'City'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Address2'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Address1'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'FName'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'LName'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Dept'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Branch'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Division'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'Emp'
        DataType = ftString
        Size = 6
      end>
    Left = 506
    Top = 8
    object evcsEmployeeStatus: TStringField
      FieldName = 'Status'
      Size = 1
    end
    object evcsEmployeeExtrareal: TStringField
      FieldName = 'Extrareal'
      Size = 12
    end
    object evcsEmployeeExtraoptions10: TStringField
      FieldName = 'Extraoptions10'
      Size = 1
    end
    object evcsEmployeeExtraoptions9: TStringField
      FieldName = 'Extraoptions9'
      Size = 1
    end
    object evcsEmployeeExtraoptions8: TStringField
      FieldName = 'Extraoptions8'
      Size = 4
    end
    object evcsEmployeeExtraoptions7: TStringField
      FieldName = 'Extraoptions7'
      Size = 1
    end
    object evcsEmployeeExtraoptions6: TStringField
      FieldName = 'Extraoptions6'
      Size = 1
    end
    object evcsEmployeeExtraoptions5: TStringField
      FieldName = 'Extraoptions5'
      Size = 1
    end
    object evcsEmployeeExtraoptions4: TStringField
      FieldName = 'Extraoptions4'
      Size = 1
    end
    object evcsEmployeeExtraoptions3: TStringField
      FieldName = 'Extraoptions3'
      Size = 1
    end
    object evcsEmployeeExtraoptions2: TStringField
      FieldName = 'Extraoptions2'
      Size = 1
    end
    object evcsEmployeeExtraoptions1: TStringField
      FieldName = 'Extraoptions1'
      Size = 1
    end
    object evcsEmployeeWeeksworked4: TStringField
      FieldName = 'Weeksworked4'
      Size = 12
    end
    object evcsEmployeeWeeksworked3: TStringField
      FieldName = 'Weeksworked3'
      Size = 12
    end
    object evcsEmployeeWeeksworked2: TStringField
      FieldName = 'Weeksworked2'
      Size = 12
    end
    object evcsEmployeeWeeksworked1: TStringField
      FieldName = 'Weeksworked1'
      Size = 12
    end
    object evcsEmployeeW2chars5: TStringField
      FieldName = 'W2chars5'
      Size = 1
    end
    object evcsEmployeeW2chars4: TStringField
      FieldName = 'W2chars4'
      Size = 1
    end
    object evcsEmployeeW2chars3: TStringField
      FieldName = 'W2chars3'
      Size = 1
    end
    object evcsEmployeeW2chars2: TStringField
      FieldName = 'W2chars2'
      Size = 1
    end
    object evcsEmployeeW2chars1: TStringField
      FieldName = 'W2chars1'
      Size = 1
    end
    object evcsEmployeePayincramount: TStringField
      FieldName = 'Payincramount'
      Size = 12
    end
    object evcsEmployeePayincrdate: TStringField
      FieldName = 'Payincrdate'
      Size = 8
    end
    object evcsEmployeeGL: TStringField
      FieldName = 'GL'
      Size = 12
    end
    object evcsEmployeeShiftdiffrate3: TStringField
      FieldName = 'Shiftdiffrate3'
      Size = 12
    end
    object evcsEmployeeShiftdiffrate2: TStringField
      FieldName = 'Shiftdiffrate2'
      Size = 12
    end
    object evcsEmployeeShiftdiffrate1: TStringField
      FieldName = 'Shiftdiffrate1'
      Size = 12
    end
    object evcsEmployeeShiftdiffcode: TStringField
      FieldName = 'Shiftdiffcode'
      Size = 1
    end
    object evcsEmployeeWorkshift: TStringField
      FieldName = 'Workshift'
      Size = 1
    end
    object evcsEmployeeLocaltaxpercent3: TStringField
      FieldName = 'Localtaxpercent3'
      Size = 12
    end
    object evcsEmployeeLocaltaxpercent2: TStringField
      FieldName = 'Localtaxpercent2'
      Size = 12
    end
    object evcsEmployeeLocaltaxpercent1: TStringField
      FieldName = 'Localtaxpercent1'
      Size = 12
    end
    object evcsEmployeeLocaltaxdesc3: TStringField
      FieldName = 'Localtaxdesc3'
      Size = 10
    end
    object evcsEmployeeLocaltaxdesc2: TStringField
      FieldName = 'Localtaxdesc2'
      Size = 10
    end
    object evcsEmployeeLocaltaxdesc1: TStringField
      FieldName = 'Localtaxdesc1'
      Size = 10
    end
    object evcsEmployeeLocaltaxcode3: TStringField
      FieldName = 'Localtaxcode3'
      Size = 10
    end
    object evcsEmployeeLocaltaxcode2: TStringField
      FieldName = 'Localtaxcode2'
      Size = 10
    end
    object evcsEmployeeLocaltaxcode1: TStringField
      FieldName = 'Localtaxcode1'
      Size = 10
    end
    object evcsEmployeeLocaltaxstate3: TStringField
      FieldName = 'Localtaxstate3'
      Size = 2
    end
    object evcsEmployeeLocaltaxstate2: TStringField
      FieldName = 'Localtaxstate2'
      Size = 2
    end
    object evcsEmployeeLocaltaxstate1: TStringField
      FieldName = 'Localtaxstate1'
      Size = 2
    end
    object evcsEmployeeVoluntary4: TStringField
      FieldName = 'Voluntary4'
      Size = 12
    end
    object evcsEmployeeVoluntary3: TStringField
      FieldName = 'Voluntary3'
      Size = 12
    end
    object evcsEmployeeVoluntary2: TStringField
      FieldName = 'Voluntary2'
      Size = 12
    end
    object evcsEmployeeVoluntary1: TStringField
      FieldName = 'Voluntary1'
      Size = 12
    end
    object evcsEmployeeAdditional4: TStringField
      FieldName = 'Additional4'
      Size = 12
    end
    object evcsEmployeeAdditional3: TStringField
      FieldName = 'Additional3'
      Size = 12
    end
    object evcsEmployeeAdditional2: TStringField
      FieldName = 'Additional2'
      Size = 12
    end
    object evcsEmployeeAdditional1: TStringField
      FieldName = 'Additional1'
      Size = 12
    end
    object evcsEmployeeTaxexempt23: TStringField
      FieldName = 'Taxexempt23'
      Size = 1
    end
    object evcsEmployeeTaxexempt22: TStringField
      FieldName = 'Taxexempt22'
      Size = 1
    end
    object evcsEmployeeTaxexempt21: TStringField
      FieldName = 'Taxexempt21'
      Size = 1
    end
    object evcsEmployeeTaxexempt20: TStringField
      FieldName = 'Taxexempt20'
      Size = 1
    end
    object evcsEmployeeTaxexempt19: TStringField
      FieldName = 'Taxexempt19'
      Size = 1
    end
    object evcsEmployeeTaxexempt18: TStringField
      FieldName = 'Taxexempt18'
      Size = 1
    end
    object evcsEmployeeTaxexempt17: TStringField
      FieldName = 'Taxexempt17'
      Size = 1
    end
    object evcsEmployeeTaxexempt16: TStringField
      FieldName = 'Taxexempt16'
      Size = 1
    end
    object evcsEmployeeTaxexempt15: TStringField
      FieldName = 'Taxexempt15'
      Size = 1
    end
    object evcsEmployeeTaxexempt14: TStringField
      FieldName = 'Taxexempt14'
      Size = 1
    end
    object evcsEmployeeTaxexempt13: TStringField
      FieldName = 'Taxexempt13'
      Size = 1
    end
    object evcsEmployeeTaxexempt12: TStringField
      FieldName = 'Taxexempt12'
      Size = 1
    end
    object evcsEmployeeTaxexempt11: TStringField
      FieldName = 'Taxexempt11'
      Size = 1
    end
    object evcsEmployeeTaxexempt10: TStringField
      FieldName = 'Taxexempt10'
      Size = 1
    end
    object evcsEmployeeTaxexempt9: TStringField
      FieldName = 'Taxexempt9'
      Size = 1
    end
    object evcsEmployeeTaxexempt8: TStringField
      FieldName = 'Taxexempt8'
      Size = 1
    end
    object evcsEmployeeTaxexempt7: TStringField
      FieldName = 'Taxexempt7'
      Size = 1
    end
    object evcsEmployeeTaxexempt6: TStringField
      FieldName = 'Taxexempt6'
      Size = 1
    end
    object evcsEmployeeTaxexempt5: TStringField
      FieldName = 'Taxexempt5'
      Size = 1
    end
    object evcsEmployeeTaxexempt4: TStringField
      FieldName = 'Taxexempt4'
      Size = 1
    end
    object evcsEmployeeTaxexempt3: TStringField
      FieldName = 'Taxexempt3'
      Size = 1
    end
    object evcsEmployeeTaxexempt2: TStringField
      FieldName = 'Taxexempt2'
      Size = 1
    end
    object evcsEmployeeTaxexempt1: TStringField
      FieldName = 'Taxexempt1'
      Size = 1
    end
    object evcsEmployeeStatetaxcode3: TStringField
      FieldName = 'Statetaxcode3'
      Size = 2
    end
    object evcsEmployeeStatetaxcode2: TStringField
      FieldName = 'Statetaxcode2'
      Size = 2
    end
    object evcsEmployeeStatetaxcode1: TStringField
      FieldName = 'Statetaxcode1'
      Size = 2
    end
    object evcsEmployeeClocknumber: TStringField
      FieldName = 'Clocknumber'
      Size = 6
    end
    object evcsEmployeeCode1099: TStringField
      FieldName = 'Code1099'
      Size = 1
    end
    object evcsEmployeeEic: TStringField
      FieldName = 'Eic'
      Size = 1
    end
    object evcsEmployeeUnion: TStringField
      FieldName = 'Union'
      Size = 6
    end
    object evcsEmployeeWorkcomp: TStringField
      FieldName = 'Workcomp'
      Size = 4
    end
    object evcsEmployeeEeo: TStringField
      FieldName = 'Eeo'
      Size = 1
    end
    object evcsEmployeePension: TStringField
      FieldName = 'Pension'
      Size = 1
    end
    object evcsEmployeeFtpt: TStringField
      FieldName = 'Ftpt'
      Size = 2
    end
    object evcsEmployeeExempt: TStringField
      FieldName = 'Exempt'
      Size = 1
    end
    object evcsEmployeeAveragehours: TStringField
      FieldName = 'Averagehours'
      Size = 12
    end
    object evcsEmployeeSalary: TStringField
      FieldName = 'Salary'
      Size = 12
    end
    object evcsEmployeeOvdept4: TStringField
      FieldName = 'Ovdept4'
      Size = 6
    end
    object evcsEmployeeOvdept3: TStringField
      FieldName = 'Ovdept3'
      Size = 6
    end
    object evcsEmployeeOvdept2: TStringField
      FieldName = 'Ovdept2'
      Size = 6
    end
    object evcsEmployeeOvdept1: TStringField
      FieldName = 'Ovdept1'
      Size = 6
    end
    object evcsEmployeeOvbranch4: TStringField
      FieldName = 'Ovbranch4'
      Size = 6
    end
    object evcsEmployeeOvbranch3: TStringField
      FieldName = 'Ovbranch3'
      Size = 6
    end
    object evcsEmployeeOvbranch2: TStringField
      FieldName = 'Ovbranch2'
      Size = 6
    end
    object evcsEmployeeOvbranch1: TStringField
      FieldName = 'Ovbranch1'
      Size = 6
    end
    object evcsEmployeeOvdivision4: TStringField
      FieldName = 'Ovdivision4'
      Size = 6
    end
    object evcsEmployeeOvdivision3: TStringField
      FieldName = 'Ovdivision3'
      Size = 6
    end
    object evcsEmployeeOvdivision2: TStringField
      FieldName = 'Ovdivision2'
      Size = 6
    end
    object evcsEmployeeOvdivision1: TStringField
      FieldName = 'Ovdivision1'
      Size = 6
    end
    object evcsEmployeeRate5: TStringField
      FieldName = 'Rate5'
      Size = 12
    end
    object evcsEmployeeRate4: TStringField
      FieldName = 'Rate4'
      Size = 12
    end
    object evcsEmployeeRate3: TStringField
      FieldName = 'Rate3'
      Size = 12
    end
    object evcsEmployeeRate2: TStringField
      FieldName = 'Rate2'
      Size = 12
    end
    object evcsEmployeeRate1: TStringField
      FieldName = 'Rate1'
      Size = 12
    end
    object evcsEmployeePayfreq: TStringField
      FieldName = 'Payfreq'
      Size = 7
    end
    object evcsEmployeeSmaritalstatus: TStringField
      FieldName = 'Smaritalstatus'
      Size = 1
    end
    object evcsEmployeeFmaritalstatus: TStringField
      FieldName = 'Fmaritalstatus'
      Size = 1
    end
    object evcsEmployeeSdependents: TStringField
      FieldName = 'Sdependents'
      Size = 7
    end
    object evcsEmployeeFdependents: TStringField
      FieldName = 'Fdependents'
      Size = 7
    end
    object evcsEmployeePosition: TStringField
      FieldName = 'Position'
    end
    object evcsEmployeeSex: TStringField
      FieldName = 'Sex'
      Size = 1
    end
    object evcsEmployeeTermCode: TStringField
      FieldName = 'TermCode'
      Size = 1
    end
    object evcsEmployeeNextincdate: TStringField
      FieldName = 'Nextincdate'
      Size = 8
    end
    object evcsEmployeeChangedate: TStringField
      FieldName = 'Changedate'
      Size = 8
    end
    object evcsEmployeeTermdate: TStringField
      FieldName = 'Termdate'
      Size = 8
    end
    object evcsEmployeeRaisedate: TStringField
      FieldName = 'Raisedate'
      Size = 8
    end
    object evcsEmployeeHiredate: TStringField
      FieldName = 'Hiredate'
      Size = 8
    end
    object evcsEmployeeBirthdate: TStringField
      FieldName = 'Birthdate'
      Size = 8
    end
    object evcsEmployeeSsi: TStringField
      FieldName = 'Ssi'
      Size = 11
    end
    object evcsEmployeePhone: TStringField
      FieldName = 'Phone'
      Size = 14
    end
    object evcsEmployeeZip: TStringField
      FieldName = 'Zip'
      Size = 10
    end
    object evcsEmployeeState: TStringField
      FieldName = 'State'
      Size = 2
    end
    object evcsEmployeeCity: TStringField
      FieldName = 'City'
    end
    object evcsEmployeeAddress2: TStringField
      FieldName = 'Address2'
      Size = 30
    end
    object evcsEmployeeAddress1: TStringField
      FieldName = 'Address1'
      Size = 30
    end
    object evcsEmployeeFName: TStringField
      FieldName = 'FName'
    end
    object evcsEmployeeLName: TStringField
      FieldName = 'LName'
      Size = 30
    end
    object evcsEmployeeDept: TStringField
      FieldName = 'Dept'
      Size = 6
    end
    object evcsEmployeeBranch: TStringField
      FieldName = 'Branch'
      Size = 6
    end
    object evcsEmployeeDivision: TStringField
      FieldName = 'Division'
      Size = 6
    end
    object evcsEmployeeEmp: TStringField
      FieldName = 'Emp'
      Size = 6
    end
  end
  object evcsRecurrings: TevClientDataSet
    Left = 506
    Top = 120
    object evcsRecurringsSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object evcsRecurringsRFIELDS5: TStringField
      FieldName = 'RFIELDS5'
      Size = 12
    end
    object evcsRecurringsRFIELDS4: TStringField
      FieldName = 'RFIELDS4'
      Size = 12
    end
    object evcsRecurringsRFIELDS3: TStringField
      FieldName = 'RFIELDS3'
      Size = 12
    end
    object evcsRecurringsRFIELDS2: TStringField
      FieldName = 'RFIELDS2'
      Size = 12
    end
    object evcsRecurringsRFIELDS1: TStringField
      FieldName = 'RFIELDS1'
      Size = 12
    end
    object evcsRecurringsFREQUENCY: TStringField
      FieldName = 'FREQUENCY'
      Size = 2
    end
    object evcsRecurringsFIRSTDUEDATE: TStringField
      FieldName = 'FIRSTDUEDATE'
      Size = 8
    end
    object evcsRecurringsANNUALMAX: TStringField
      FieldName = 'ANNUALMAX'
      Size = 12
    end
    object evcsRecurringsPAYPERIODMAX: TStringField
      FieldName = 'PAYPERIODMAX'
      Size = 12
    end
    object evcsRecurringsPAYPERIODMIN: TStringField
      FieldName = 'PAYPERIODMIN'
      Size = 12
    end
    object evcsRecurringsBALANCE: TStringField
      FieldName = 'BALANCE'
      Size = 12
    end
    object evcsRecurringsTARGETAMOUNT: TStringField
      FieldName = 'TARGETAMOUNT'
      Size = 12
    end
    object evcsRecurringsSPECIALCODE: TStringField
      FieldName = 'SPECIALCODE'
      Size = 1
    end
    object evcsRecurringsPERCENTCODE: TStringField
      FieldName = 'PERCENTCODE'
      Size = 1
    end
    object evcsRecurringsAMOUNT: TStringField
      FieldName = 'AMOUNT'
      Size = 12
    end
    object evcsRecurringsDETYPE: TStringField
      FieldName = 'DETYPE'
      Size = 1
    end
    object evcsRecurringsDECODE: TStringField
      FieldName = 'DECODE'
      Size = 2
    end
    object evcsRecurringsDET: TStringField
      FieldName = 'DET'
      Size = 1
    end
    object evcsRecurringsEMPL: TStringField
      FieldName = 'EMPL'
      Size = 6
    end
  end
  object evcsTimeOff: TevClientDataSet
    Left = 506
    Top = 176
    object evcsTimeOffSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object evcsTimeOffEETOHOURS5: TStringField
      FieldName = 'EETOHOURS5'
      Size = 12
    end
    object evcsTimeOffEETOHOURS4: TStringField
      FieldName = 'EETOHOURS4'
      Size = 12
    end
    object evcsTimeOffEETOHOURS3: TStringField
      FieldName = 'EETOHOURS3'
      Size = 12
    end
    object evcsTimeOffEETOHOURS2: TStringField
      FieldName = 'EETOHOURS2'
      Size = 12
    end
    object evcsTimeOffEETOHOURS1: TStringField
      FieldName = 'EETOHOURS1'
      Size = 12
    end
    object evcsTimeOffEETOOVHOURS5: TStringField
      FieldName = 'EETOOVHOURS5'
      Size = 12
    end
    object evcsTimeOffEETOOVHOURS4: TStringField
      FieldName = 'EETOOVHOURS4'
      Size = 12
    end
    object evcsTimeOffEETOOVHOURS3: TStringField
      FieldName = 'EETOOVHOURS3'
      Size = 12
    end
    object evcsTimeOffEETOOVHOURS2: TStringField
      FieldName = 'EETOOVHOURS2'
      Size = 12
    end
    object evcsTimeOffEETOOVHOURS1: TStringField
      FieldName = 'EETOOVHOURS1'
      Size = 12
    end
    object evcsTimeOffEETOOVRATE5: TStringField
      FieldName = 'EETOOVRATE5'
      Size = 12
    end
    object evcsTimeOffEETOOVRATE4: TStringField
      FieldName = 'EETOOVRATE4'
      Size = 12
    end
    object evcsTimeOffEETOOVRATE3: TStringField
      FieldName = 'EETOOVRATE3'
      Size = 12
    end
    object evcsTimeOffEETOOVRATE2: TStringField
      FieldName = 'EETOOVRATE2'
      Size = 12
    end
    object evcsTimeOffEETOOVRATE1: TStringField
      FieldName = 'EETOOVRATE1'
      Size = 12
    end
    object evcsTimeOffEETODATE5: TStringField
      FieldName = 'EETODATE5'
      Size = 8
    end
    object evcsTimeOffEETODATE4: TStringField
      FieldName = 'EETODATE4'
      Size = 8
    end
    object evcsTimeOffEETODATE3: TStringField
      FieldName = 'EETODATE3'
      Size = 8
    end
    object evcsTimeOffEETODATE2: TStringField
      FieldName = 'EETODATE2'
      Size = 8
    end
    object evcsTimeOffEETODATE1: TStringField
      FieldName = 'EETODATE1'
      Size = 8
    end
    object evcsTimeOffEEOTYPE5: TStringField
      FieldName = 'EEOTYPE5'
      Size = 4
    end
    object evcsTimeOffEEOTYPE4: TStringField
      FieldName = 'EEOTYPE4'
      Size = 4
    end
    object evcsTimeOffEEOTYPE3: TStringField
      FieldName = 'EEOTYPE3'
      Size = 4
    end
    object evcsTimeOffEEOTYPE2: TStringField
      FieldName = 'EEOTYPE2'
      Size = 4
    end
    object evcsTimeOffEEOTYPE1: TStringField
      FieldName = 'EEOTYPE1'
      Size = 4
    end
    object evcsTimeOffEMPL: TStringField
      FieldName = 'EMPL'
      Size = 6
    end
  end
  object evcsYearToDate: TevClientDataSet
    Left = 594
    Top = 8
    object evcsYearToDateSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object evcsYearToDateRFIELDS5: TStringField
      FieldName = 'RFIELDS5'
      Size = 12
    end
    object evcsYearToDateRFIELDS4: TStringField
      FieldName = 'RFIELDS4'
      Size = 12
    end
    object evcsYearToDateRFIELDS3: TStringField
      FieldName = 'RFIELDS3'
      Size = 12
    end
    object evcsYearToDateRFIELDS2: TStringField
      FieldName = 'RFIELDS2'
      Size = 12
    end
    object evcsYearToDateRFIELDS1: TStringField
      FieldName = 'RFIELDS1'
      Size = 12
    end
    object evcsYearToDateLAMOUNTS: TStringField
      FieldName = 'LAMOUNTS'
      Size = 12
    end
    object evcsYearToDateLHOURS: TStringField
      FieldName = 'LHOURS'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS12: TStringField
      FieldName = 'CAMOUNTS12'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS11: TStringField
      FieldName = 'CAMOUNTS11'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS10: TStringField
      FieldName = 'CAMOUNTS10'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS9: TStringField
      FieldName = 'CAMOUNTS9'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS8: TStringField
      FieldName = 'CAMOUNTS8'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS7: TStringField
      FieldName = 'CAMOUNTS7'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS6: TStringField
      FieldName = 'CAMOUNTS6'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS5: TStringField
      FieldName = 'CAMOUNTS5'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS4: TStringField
      FieldName = 'CAMOUNTS4'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS3: TStringField
      FieldName = 'CAMOUNTS3'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS2: TStringField
      FieldName = 'CAMOUNTS2'
      Size = 12
    end
    object evcsYearToDateCAMOUNTS1: TStringField
      FieldName = 'CAMOUNTS1'
      Size = 12
    end
    object evcsYearToDateCHOURS12: TStringField
      FieldName = 'CHOURS12'
      Size = 12
    end
    object evcsYearToDateCHOURS11: TStringField
      FieldName = 'CHOURS11'
      Size = 12
    end
    object evcsYearToDateCHOURS10: TStringField
      FieldName = 'CHOURS10'
      Size = 12
    end
    object evcsYearToDateCHOURS9: TStringField
      FieldName = 'CHOURS9'
      Size = 12
    end
    object evcsYearToDateCHOURS8: TStringField
      FieldName = 'CHOURS8'
      Size = 12
    end
    object evcsYearToDateCHOURS7: TStringField
      FieldName = 'CHOURS7'
      Size = 12
    end
    object evcsYearToDateCHOURS6: TStringField
      FieldName = 'CHOURS6'
      Size = 12
    end
    object evcsYearToDateCHOURS5: TStringField
      FieldName = 'CHOURS5'
      Size = 12
    end
    object evcsYearToDateCHOURS4: TStringField
      FieldName = 'CHOURS4'
      Size = 12
    end
    object evcsYearToDateCHOURS3: TStringField
      FieldName = 'CHOURS3'
      Size = 12
    end
    object evcsYearToDateCHOURS2: TStringField
      FieldName = 'CHOURS2'
      Size = 12
    end
    object evcsYearToDateCHOURS1: TStringField
      FieldName = 'CHOURS1'
      Size = 12
    end
    object evcsYearToDateDETYPE: TStringField
      FieldName = 'DETYPE'
      Size = 1
    end
    object evcsYearToDateLOCAL: TStringField
      FieldName = 'LOCAL'
      Size = 10
    end
    object evcsYearToDateSTATETAXCODE3: TStringField
      FieldName = 'STATETAXCODE3'
      Size = 2
    end
    object evcsYearToDateSTATETAXCODE2: TStringField
      FieldName = 'STATETAXCODE2'
      Size = 2
    end
    object evcsYearToDateSTATETAXCODE1: TStringField
      FieldName = 'STATETAXCODE1'
      Size = 2
    end
    object evcsYearToDateDECODE: TStringField
      FieldName = 'DECODE'
      Size = 2
    end
    object evcsYearToDateDET: TStringField
      FieldName = 'DET'
      Size = 1
    end
    object evcsYearToDateEMPL: TStringField
      FieldName = 'EMPL'
      Size = 6
    end
  end
  object evcsConfig: TevClientDataSet
    IndexFieldNames = 'Destination_Field'
    FieldDefs = <
      item
        Name = 'Destination_Field'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Field_Position'
        DataType = ftInteger
      end
      item
        Name = 'Field_Length'
        DataType = ftInteger
      end
      item
        Name = 'Destination_Table'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Insert_Order'
        DataType = ftInteger
      end
      item
        Name = 'Default_Value'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Procedure'
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
      end
      item
        Name = 'CHANGEINDEX'
      end>
    Left = 88
    Top = 304
    object evcsConfigDestination_Field: TStringField
      DisplayWidth = 50
      FieldName = 'Destination_Field'
      Size = 50
    end
    object evcsConfigField_Position: TIntegerField
      DisplayWidth = 10
      FieldName = 'Field_Position'
    end
    object evcsConfigField_Length: TIntegerField
      DisplayWidth = 10
      FieldName = 'Field_Length'
    end
  end
  object evcsHistory1: TevClientDataSet
    Left = 594
    Top = 120
    object evcsHistory1STATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object evcsHistory1EXTRAREAL: TStringField
      FieldName = 'EXTRAREAL'
      Size = 12
    end
    object evcsHistory1FLAGS10: TStringField
      FieldName = 'FLAGS10'
      Size = 1
    end
    object evcsHistory1FLAGS9: TStringField
      FieldName = 'FLAGS9'
      Size = 1
    end
    object evcsHistory1FLAGS8: TStringField
      FieldName = 'FLAGS8'
      Size = 1
    end
    object evcsHistory1FLAGS7: TStringField
      FieldName = 'FLAGS7'
      Size = 1
    end
    object evcsHistory1FLAGS6: TStringField
      FieldName = 'FLAGS6'
      Size = 1
    end
    object evcsHistory1FLAGS5: TStringField
      FieldName = 'FLAGS5'
      Size = 1
    end
    object evcsHistory1FLAGS4: TStringField
      FieldName = 'FLAGS4'
      Size = 1
    end
    object evcsHistory1FLAGS3: TStringField
      FieldName = 'FLAGS3'
      Size = 1
    end
    object evcsHistory1FLAGS2: TStringField
      FieldName = 'FLAGS2'
      Size = 1
    end
    object evcsHistory1FLAGS1: TStringField
      FieldName = 'FLAGS1'
      Size = 1
    end
    object evcsHistory1NET: TStringField
      FieldName = 'NET'
      Size = 12
    end
    object evcsHistory1DEDS: TStringField
      FieldName = 'DEDS'
      Size = 12
    end
    object evcsHistory1LOCALTAXAMOUNT3: TStringField
      FieldName = 'LOCALTAXAMOUNT3'
      Size = 12
    end
    object evcsHistory1LOCALTAXAMOUNT2: TStringField
      FieldName = 'LOCALTAXAMOUNT2'
      Size = 12
    end
    object evcsHistory1LOCALTAXAMOUNT1: TStringField
      FieldName = 'LOCALTAXAMOUNT1'
      Size = 12
    end
    object evcsHistory1LOCALTAXCODE3: TStringField
      FieldName = 'LOCALTAXCODE3'
      Size = 10
    end
    object evcsHistory1LOCALTAXCODE2: TStringField
      FieldName = 'LOCALTAXCODE2'
      Size = 10
    end
    object evcsHistory1LOCALTAXCODE1: TStringField
      FieldName = 'LOCALTAXCODE1'
      Size = 10
    end
    object evcsHistory1LOCALTAXSTATE3: TStringField
      FieldName = 'LOCALTAXSTATE3'
      Size = 2
    end
    object evcsHistory1LOCALTAXSTATE2: TStringField
      FieldName = 'LOCALTAXSTATE2'
      Size = 2
    end
    object evcsHistory1LOCALTAXSTATE1: TStringField
      FieldName = 'LOCALTAXSTATE1'
      Size = 2
    end
    object evcsHistory1ERTAXABLEWAGES10: TStringField
      FieldName = 'ERTAXABLEWAGES10'
      Size = 12
    end
    object evcsHistory1ERTAXABLEWAGES9: TStringField
      FieldName = 'ERTAXABLEWAGES9'
      Size = 12
    end
    object evcsHistory1ERTAXABLEWAGES8: TStringField
      FieldName = 'ERTAXABLEWAGES8'
      Size = 12
    end
    object evcsHistory1ERTAXABLEWAGES7: TStringField
      FieldName = 'ERTAXABLEWAGES7'
      Size = 12
    end
    object evcsHistory1ERTAXABLEWAGES6: TStringField
      FieldName = 'ERTAXABLEWAGES6'
      Size = 12
    end
    object evcsHistory1ERTAXABLEWAGES5: TStringField
      FieldName = 'ERTAXABLEWAGES5'
      Size = 12
    end
    object evcsHistory1ERTAXABLEWAGES4: TStringField
      FieldName = 'ERTAXABLEWAGES4'
      Size = 12
    end
    object evcsHistory1ERTAXABLEWAGES3: TStringField
      FieldName = 'ERTAXABLEWAGES3'
      Size = 12
    end
    object evcsHistory1ERTAXABLEWAGES2: TStringField
      FieldName = 'ERTAXABLEWAGES2'
      Size = 12
    end
    object evcsHistory1ERTAXABLEWAGES1: TStringField
      FieldName = 'ERTAXABLEWAGES1'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS10: TStringField
      FieldName = 'EETAXAMOUNTS10'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS9: TStringField
      FieldName = 'EETAXAMOUNTS9'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS8: TStringField
      FieldName = 'EETAXAMOUNTS8'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS7: TStringField
      FieldName = 'EETAXAMOUNTS7'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS6: TStringField
      FieldName = 'EETAXAMOUNTS6'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS5: TStringField
      FieldName = 'EETAXAMOUNTS5'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS4: TStringField
      FieldName = 'EETAXAMOUNTS4'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS3: TStringField
      FieldName = 'EETAXAMOUNTS3'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS2: TStringField
      FieldName = 'EETAXAMOUNTS2'
      Size = 12
    end
    object evcsHistory1EETAXAMOUNTS1: TStringField
      FieldName = 'EETAXAMOUNTS1'
      Size = 12
    end
    object evcsHistory1GROSS: TStringField
      FieldName = 'GROSS'
      Size = 12
    end
    object evcsHistory1STATETAXCODE3: TStringField
      FieldName = 'STATETAXCODE3'
      Size = 2
    end
    object evcsHistory1STATETAXCODE2: TStringField
      FieldName = 'STATETAXCODE2'
      Size = 2
    end
    object evcsHistory1STATETAXCODE1: TStringField
      FieldName = 'STATETAXCODE1'
      Size = 2
    end
    object evcsHistory1SALARY: TStringField
      FieldName = 'SALARY'
      Size = 12
    end
    object evcsHistory1HOURLYRATE: TStringField
      FieldName = 'HOURLYRATE'
      Size = 12
    end
    object evcsHistory1FNAME: TStringField
      FieldName = 'FNAME'
      Size = 7
    end
    object evcsHistory1LNAME: TStringField
      FieldName = 'LNAME'
      Size = 7
    end
    object evcsHistory1ACCOUNTNUMBER: TStringField
      FieldName = 'ACCOUNTNUMBER'
    end
    object evcsHistory1DIRECTDEPOSIT: TStringField
      FieldName = 'DIRECTDEPOSIT'
      Size = 1
    end
    object evcsHistory1MANUALCHECK: TStringField
      FieldName = 'MANUALCHECK'
      Size = 1
    end
    object evcsHistory1BATCH: TStringField
      FieldName = 'BATCH'
      Size = 1
    end
    object evcsHistory1PERIODENDDATE: TStringField
      FieldName = 'PERIODENDDATE'
      Size = 8
    end
    object evcsHistory1PERIODBEGINDATE: TStringField
      FieldName = 'PERIODBEGINDATE'
      Size = 8
    end
    object evcsHistory1DATEOFcHECK: TStringField
      FieldName = 'DATEOFcHECK'
      Size = 8
    end
    object evcsHistory1CHECK: TStringField
      FieldName = 'CHECK'
      Size = 10
    end
    object evcsHistory1DEPT: TStringField
      FieldName = 'DEPT'
      Size = 6
    end
    object evcsHistory1BRANCH: TStringField
      FieldName = 'BRANCH'
      Size = 6
    end
    object evcsHistory1DIVISION: TStringField
      FieldName = 'DIVISION'
      Size = 6
    end
    object evcsHistory1EMPL: TStringField
      FieldName = 'EMPL'
      Size = 6
    end
  end
  object evcsHistory2: TevClientDataSet
    Left = 594
    Top = 64
    object evcsHistory2STATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object evcsHistory2FLAGS10: TStringField
      FieldName = 'FLAGS10'
      Size = 1
    end
    object evcsHistory2FLAGS9: TStringField
      FieldName = 'FLAGS9'
      Size = 1
    end
    object evcsHistory2FLAGS8: TStringField
      FieldName = 'FLAGS8'
      Size = 1
    end
    object evcsHistory2FLAGS7: TStringField
      FieldName = 'FLAGS7'
      Size = 1
    end
    object evcsHistory2FLAGS6: TStringField
      FieldName = 'FLAGS6'
      Size = 1
    end
    object evcsHistory2FLAGS5: TStringField
      FieldName = 'FLAGS5'
      Size = 1
    end
    object evcsHistory2FLAGS4: TStringField
      FieldName = 'FLAGS4'
      Size = 1
    end
    object evcsHistory2FLAGS3: TStringField
      FieldName = 'FLAGS3'
      Size = 1
    end
    object evcsHistory2FLAGS2: TStringField
      FieldName = 'FLAGS2'
      Size = 1
    end
    object evcsHistory2FLAGS1: TStringField
      FieldName = 'FLAGS1'
      Size = 1
    end
    object evcsHistory2OVWORKCOMP: TStringField
      FieldName = 'OVWORKCOMP'
      Size = 8
    end
    object evcsHistory2OVSTATE2: TStringField
      FieldName = 'OVSTATE2'
      Size = 8
    end
    object evcsHistory2OVSTATE: TStringField
      FieldName = 'OVSTATE'
      Size = 8
    end
    object evcsHistory2DETYPE: TStringField
      FieldName = 'DETYPE'
      Size = 1
    end
    object evcsHistory2RATENUMBER: TStringField
      FieldName = 'RATENUMBER'
      Size = 1
    end
    object evcsHistory2DIRECTDEPOSIT: TStringField
      FieldName = 'DIRECTDEPOSIT'
      Size = 1
    end
    object evcsHistory2WORKSHIFT: TStringField
      FieldName = 'WORKSHIFT'
      Size = 1
    end
    object evcsHistory2DATE: TStringField
      FieldName = 'DATE'
      Size = 8
    end
    object evcsHistory2JOBNUMBER: TStringField
      FieldName = 'JOBNUMBER'
      Size = 12
    end
    object evcsHistory2SEQUENCE: TStringField
      FieldName = 'SEQUENCE'
      Size = 1
    end
    object evcsHistory2OVRTE: TStringField
      FieldName = 'OVRTE'
      Size = 12
    end
    object evcsHistory2OVDEPT: TStringField
      FieldName = 'OVDEPT'
      Size = 6
    end
    object evcsHistory2OVBRANCH: TStringField
      FieldName = 'OVBRANCH'
      Size = 6
    end
    object evcsHistory2OVDIVISION: TStringField
      FieldName = 'OVDIVISION'
      Size = 6
    end
    object evcsHistory2DEAMOUNT: TStringField
      FieldName = 'DEAMOUNT'
      Size = 12
    end
    object evcsHistory2DEHOURS: TStringField
      FieldName = 'DEHOURS'
      Size = 12
    end
    object evcsHistory2PRIORITY: TStringField
      FieldName = 'PRIORITY'
      Size = 7
    end
    object evcsHistory2DECODE: TStringField
      FieldName = 'DECODE'
      Size = 2
    end
    object evcsHistory2DET: TStringField
      FieldName = 'DET'
      Size = 1
    end
    object evcsHistory2BATCH: TStringField
      FieldName = 'BATCH'
      Size = 1
    end
    object evcsHistory2DATEOFCHECK: TStringField
      FieldName = 'DATEOFCHECK'
      Size = 8
    end
    object evcsHistory2CHECK: TStringField
      FieldName = 'CHECK'
      Size = 10
    end
    object evcsHistory2EMP: TStringField
      FieldName = 'EMP'
      Size = 6
    end
  end
  object evdsLOCAL_MATCHING: TevDataSource
    DataSet = LOCAL_MATCHING
    Left = 150
    Top = 56
  end
  object evdsRapidED: TevDataSource
    DataSet = evcsRapidED
    Left = 398
    Top = 13
  end
  object evdsRapidLocals: TevDataSource
    DataSet = evcsRapidLocals
    Left = 398
    Top = 112
  end
  object evdsLocals: TevDataSource
    DataSet = DM_SY_LOCALS.SY_LOCALS
    Left = 162
    Top = 304
  end
  object evdsEvoED: TevDataSource
    DataSet = DM_CL_E_DS.CL_E_DS
    Left = 26
    Top = 304
  end
  object evdsRapidSpecial: TevDataSource
    DataSet = evcsRapidSpecial
    Left = 398
    Top = 64
  end
  object evdsED_MATCHING: TevDataSource
    DataSet = ED_MATCHING
    Left = 150
    Top = 101
  end
  object evcsRapidLocals: TevClientDataSet
    FieldDefs = <
      item
        Name = 'STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'LOCAL'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'HasBeenMatched'
        DataType = ftString
        Size = 1
      end>
    Filter = 'HasBeenMatched='#39'F'#39
    Filtered = True
    Left = 302
    Top = 112
    object evcsRapidLocalsSTATE: TStringField
      DisplayLabel = 'State'
      DisplayWidth = 2
      FieldName = 'STATE'
      Size = 2
    end
    object evcsRapidLocalsLOCAL: TStringField
      DisplayLabel = 'Local'
      DisplayWidth = 20
      FieldName = 'LOCAL'
    end
    object evcsRapidLocalsHasBeenMatched: TStringField
      FieldName = 'HasBeenMatched'
      Visible = False
      Size = 1
    end
  end
  object evdsEDGroupCodes: TevDataSource
    Left = 256
    Top = 304
  end
  object ED_GROUP_MATCHING: TevClientDataSet
    MasterFields = 'CL_NBR;CO_NBR'
    DetailFields = 'CL_NBR;CO_NBR'
    MasterSource = evdsImportMaster
    Left = 48
    Top = 152
    object ED_GROUP_MATCHINGCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object ED_GROUP_MATCHINGCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object ED_GROUP_MATCHINGSpecialOrPercent: TStringField
      FieldName = 'SpecialOrPercent'
      Size = 2
    end
    object ED_GROUP_MATCHINGName: TStringField
      FieldName = 'Name'
      Size = 30
    end
    object ED_GROUP_MATCHINGCL_E_D_GROUPS_NBR: TIntegerField
      FieldName = 'CL_E_D_GROUPS_NBR'
    end
  end
  object evdsED_GROUP_MATCHING: TevDataSource
    DataSet = ED_GROUP_MATCHING
    Left = 152
    Top = 152
  end
  object evcsRapidDeposits: TevClientDataSet
    Filter = 'HasBeenMatched='#39'F'#39
    Filtered = True
    Left = 304
    Top = 160
    object evcsRapidDepositsDECODE: TStringField
      DisplayLabel = 'Deduction Code'
      DisplayWidth = 3
      FieldName = 'DECODE'
      Size = 3
    end
    object evcsRapidDepositsHasBeenMatched: TStringField
      FieldName = 'HasBeenMatched'
      Visible = False
      Size = 1
    end
  end
  object evdsRapidDeposits: TevDataSource
    DataSet = evcsRapidDeposits
    Left = 400
    Top = 160
  end
  object evDataSource1: TevDataSource
    Left = 504
    Top = 272
  end
  object DD_MATCHING: TevClientDataSet
    MasterFields = 'CL_NBR;CO_NBR'
    DetailFields = 'CL_NBR;CO_NBR'
    MasterSource = evdsImportMaster
    Left = 48
    Top = 200
    object DD_MATCHINGCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object DD_MATCHINGCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object DD_MATCHINGDECODE: TStringField
      FieldName = 'DECODE'
      Size = 5
    end
    object DD_MATCHINGCUSTOM_E_D_CODE_NUMBER: TStringField
      FieldName = 'CUSTOM_E_D_CODE_NUMBER'
      Size = 5
    end
    object DD_MATCHINGDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object DD_MATCHINGCL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
  end
  object evdsDD_MATCHING: TevDataSource
    DataSet = DD_MATCHING
    Left = 152
    Top = 200
  end
  object evcsLiabilities: TevClientDataSet
    Left = 588
    Top = 177
    object evcsLiabilitiesCHECK_DATE: TDateTimeField
      FieldName = 'CHECK_DATE'
    end
    object evcsLiabilitiesCOMPANY_ID: TStringField
      FieldName = 'COMPANY_ID'
    end
    object evcsLiabilitiesDUE_DATE: TDateTimeField
      FieldName = 'DUE_DATE'
    end
    object evcsLiabilitiesSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object evcsLiabilitiesLOCAL: TStringField
      FieldName = 'LOCAL'
      Size = 40
    end
    object evcsLiabilitiesDEP: TStringField
      FieldName = 'DEP'
      Size = 2
    end
    object evcsLiabilitiesAMOUNT1: TFloatField
      FieldName = 'AMOUNT1'
    end
    object evcsLiabilitiesAMOUNT2: TFloatField
      FieldName = 'AMOUNT2'
    end
    object evcsLiabilitiesAMOUNT3: TFloatField
      FieldName = 'AMOUNT3'
    end
    object evcsLiabilitiesAMOUNT4: TFloatField
      FieldName = 'AMOUNT4'
    end
    object evcsLiabilitiesAMOUNT5: TFloatField
      FieldName = 'AMOUNT5'
    end
    object evcsLiabilitiesAMOUNT6: TFloatField
      FieldName = 'AMOUNT6'
    end
    object evcsLiabilitiesAMOUNT7: TFloatField
      FieldName = 'AMOUNT7'
    end
    object evcsLiabilitiesAMOUNT8: TFloatField
      FieldName = 'AMOUNT8'
    end
    object evcsLiabilitiesAMOUNT9: TFloatField
      FieldName = 'AMOUNT9'
    end
    object evcsLiabilitiesAMOUNT10: TFloatField
      FieldName = 'AMOUNT10'
    end
  end
  object evdsImportMaster: TevDataSource
    DataSet = evcsImportMaster
    Left = 152
    Top = 8
  end
  object evcsErrorLog: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CUSTOM_COMPANY_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'CUSTOM_EMPLOYEE_NUMBER'
        DataType = ftString
        Size = 9
      end
      item
        Name = 'FNAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'LNAME'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'FIELD_NAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'REASON'
        DataType = ftString
        Size = 128
      end
      item
        Name = 'WHEN'
        DataType = ftDateTime
      end
      item
        Name = 'IMTYPE'
        DataType = ftString
        Size = 20
      end>
    Left = 344
    Top = 304
    object evcsErrorLogCUSTOM_COMPANY_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object evcsErrorLogNAME: TStringField
      DisplayWidth = 30
      FieldName = 'NAME'
      Size = 30
    end
    object evcsErrorLogCUSTOM_EMPLOYEE_NUMBER: TStringField
      DisplayWidth = 9
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Size = 9
    end
    object evcsErrorLogFNAME: TStringField
      DisplayWidth = 20
      FieldName = 'FNAME'
    end
    object evcsErrorLogLNAME: TStringField
      DisplayWidth = 30
      FieldName = 'LNAME'
      Size = 30
    end
    object evcsErrorLogFIELD_NAME: TStringField
      DisplayWidth = 20
      FieldName = 'FIELD_NAME'
    end
    object evcsErrorLogREASON: TStringField
      DisplayWidth = 128
      FieldName = 'REASON'
      Size = 128
    end
    object evcsErrorLogWHEN: TDateTimeField
      DisplayWidth = 18
      FieldName = 'WHEN'
    end
    object evcsErrorLogIMTYPE: TStringField
      FieldName = 'IMTYPE'
      Visible = False
    end
  end
  object evcsAutoLabor: TevClientDataSet
    Left = 512
    Top = 224
    object evcsAutoLaborEmp: TStringField
      FieldName = 'Emp'
      Size = 6
    end
    object evcsAutoLaborEarnSet: TStringField
      FieldName = 'EarnSet'
      Size = 1
    end
    object evcsAutoLaborTaxes: TStringField
      FieldName = 'Taxes'
      Size = 1
    end
    object evcsAutoLaborPercent1: TStringField
      FieldName = 'Percent1'
      Size = 12
    end
    object evcsAutoLaborDivision1: TStringField
      FieldName = 'Division1'
      Size = 6
    end
    object evcsAutoLaborBranch1: TStringField
      FieldName = 'Branch1'
      Size = 6
    end
    object evcsAutoLaborDepartment1: TStringField
      FieldName = 'Department1'
      Size = 6
    end
    object evcsAutoLaborPercent2: TStringField
      FieldName = 'Percent2'
      Size = 12
    end
    object evcsAutoLaborDivision2: TStringField
      FieldName = 'Division2'
      Size = 6
    end
    object evcsAutoLaborBranch2: TStringField
      FieldName = 'Branch2'
      Size = 6
    end
    object evcsAutoLaborDepartment2: TStringField
      FieldName = 'Department2'
      Size = 6
    end
    object evcsAutoLaborPercent3: TStringField
      FieldName = 'Percent3'
      Size = 12
    end
    object evcsAutoLaborDivision3: TStringField
      FieldName = 'Division3'
      Size = 6
    end
    object evcsAutoLaborBranch3: TStringField
      FieldName = 'Branch3'
      Size = 6
    end
    object evcsAutoLaborDepartment3: TStringField
      FieldName = 'Department3'
      Size = 6
    end
    object evcsAutoLaborPercent4: TStringField
      FieldName = 'Percent4'
      Size = 12
    end
    object evcsAutoLaborDivision4: TStringField
      FieldName = 'Division4'
      Size = 6
    end
    object evcsAutoLaborBranch4: TStringField
      FieldName = 'Branch4'
      Size = 6
    end
    object evcsAutoLaborDepartment4: TStringField
      FieldName = 'Department4'
      Size = 6
    end
    object evcsAutoLaborPercent5: TStringField
      FieldName = 'Percent5'
      Size = 12
    end
    object evcsAutoLaborDivision5: TStringField
      FieldName = 'Division5'
      Size = 6
    end
    object evcsAutoLaborBranch5: TStringField
      FieldName = 'Branch5'
      Size = 6
    end
    object evcsAutoLaborDepartment5: TStringField
      FieldName = 'Department5'
      Size = 6
    end
    object evcsAutoLaborPercent6: TStringField
      FieldName = 'Percent6'
      Size = 12
    end
    object evcsAutoLaborDivision6: TStringField
      FieldName = 'Division6'
      Size = 6
    end
    object evcsAutoLaborBranch6: TStringField
      FieldName = 'Branch6'
      Size = 6
    end
    object evcsAutoLaborDepartment6: TStringField
      FieldName = 'Department6'
      Size = 6
    end
    object evcsAutoLaborPercent7: TStringField
      FieldName = 'Percent7'
      Size = 12
    end
    object evcsAutoLaborDivision7: TStringField
      FieldName = 'Division7'
      Size = 6
    end
    object evcsAutoLaborBranch7: TStringField
      FieldName = 'Branch7'
      Size = 6
    end
    object evcsAutoLaborDepartment7: TStringField
      FieldName = 'Department7'
      Size = 6
    end
    object evcsAutoLaborPercent8: TStringField
      FieldName = 'Percent8'
      Size = 12
    end
    object evcsAutoLaborDivision8: TStringField
      FieldName = 'Division8'
      Size = 6
    end
    object evcsAutoLaborBranch8: TStringField
      FieldName = 'Branch8'
      Size = 6
    end
    object evcsAutoLaborDepartment8: TStringField
      FieldName = 'Department8'
      Size = 6
    end
    object evcsAutoLaborPercent9: TStringField
      FieldName = 'Percent9'
      Size = 12
    end
    object evcsAutoLaborDivision9: TStringField
      FieldName = 'Division9'
      Size = 6
    end
    object evcsAutoLaborBranch9: TStringField
      FieldName = 'Branch9'
      Size = 6
    end
    object evcsAutoLaborDepartment9: TStringField
      FieldName = 'Department9'
      Size = 6
    end
    object evcsAutoLaborPercent10: TStringField
      FieldName = 'Percent10'
      Size = 12
    end
    object evcsAutoLaborDivision10: TStringField
      FieldName = 'Division10'
      Size = 6
    end
    object evcsAutoLaborBranch10: TStringField
      FieldName = 'Branch10'
      Size = 6
    end
    object evcsAutoLaborDepartment10: TStringField
      FieldName = 'Department10'
      Size = 6
    end
    object evcsAutoLaborJobNumber1: TStringField
      FieldName = 'JobNumber1'
      Size = 12
    end
    object evcsAutoLaborJobNumber2: TStringField
      FieldName = 'JobNumber2'
      Size = 12
    end
    object evcsAutoLaborJobNumber3: TStringField
      FieldName = 'JobNumber3'
      Size = 12
    end
    object evcsAutoLaborJobNumber4: TStringField
      FieldName = 'JobNumber4'
      Size = 12
    end
    object evcsAutoLaborJobNumber5: TStringField
      FieldName = 'JobNumber5'
      Size = 12
    end
    object evcsAutoLaborJobNumber6: TStringField
      FieldName = 'JobNumber6'
      Size = 12
    end
    object evcsAutoLaborJobNumber7: TStringField
      FieldName = 'JobNumber7'
      Size = 12
    end
    object evcsAutoLaborJobNumber8: TStringField
      FieldName = 'JobNumber8'
      Size = 12
    end
    object evcsAutoLaborJobNumber9: TStringField
      FieldName = 'JobNumber9'
      Size = 12
    end
    object evcsAutoLaborJobNumber10: TStringField
      FieldName = 'JobNumber10'
      Size = 12
    end
    object evcsAutoLaborLineDate1: TStringField
      FieldName = 'LineDate1'
      Size = 8
    end
    object evcsAutoLaborLineDate2: TStringField
      FieldName = 'LineDate2'
      Size = 8
    end
    object evcsAutoLaborLineDate3: TStringField
      FieldName = 'LineDate3'
      Size = 8
    end
    object evcsAutoLaborLineDate4: TStringField
      FieldName = 'LineDate4'
      Size = 8
    end
    object evcsAutoLaborLineDate5: TStringField
      FieldName = 'LineDate5'
      Size = 8
    end
    object evcsAutoLaborLineDate6: TStringField
      FieldName = 'LineDate6'
      Size = 8
    end
    object evcsAutoLaborLineDate7: TStringField
      FieldName = 'LineDate7'
      Size = 8
    end
    object evcsAutoLaborLineDate8: TStringField
      FieldName = 'LineDate8'
      Size = 8
    end
    object evcsAutoLaborLineDate9: TStringField
      FieldName = 'LineDate9'
      Size = 8
    end
    object evcsAutoLaborLineDate10: TStringField
      FieldName = 'LineDate10'
      Size = 8
    end
    object evcsAutoLaborStatus: TStringField
      FieldName = 'Status'
      Size = 1
    end
  end
  object evds3rdParty: TevDataSource
    DataSet = evcs3rdParty
    Left = 400
    Top = 208
  end
  object evcs3rdParty: TevClientDataSet
    Filter = 'HasBeenMatched='#39'F'#39
    Filtered = True
    Left = 304
    Top = 208
    object evcs3rdPartyDeduction: TStringField
      DisplayLabel = '3rd Party Deduction'
      DisplayWidth = 2
      FieldName = 'Deduction'
      Size = 2
    end
    object evcs3rdPartyHasBeenMatched: TStringField
      FieldName = 'HasBeenMatched'
      Visible = False
      Size = 1
    end
  end
  object PARTY_MATCHING: TevClientDataSet
    MasterFields = 'CL_NBR;CO_NBR'
    DetailFields = 'CL_NBR;CO_NBR'
    MasterSource = evdsImportMaster
    Left = 48
    Top = 248
    object PARTY_MATCHINGCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object PARTY_MATCHINGCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object PARTY_MATCHINGDeduction: TStringField
      FieldName = 'Deduction'
      Size = 2
    end
    object PARTY_MATCHINGTAX: TStringField
      FieldName = 'TAX'
      Size = 15
    end
  end
  object evdsPARTY_MATCHING: TevDataSource
    DataSet = PARTY_MATCHING
    Left = 152
    Top = 248
  end
  object evcsEvoTaxes: TevClientDataSet
    Left = 296
    Top = 256
    object evcsEvoTaxesTax: TStringField
      DisplayWidth = 15
      FieldName = 'Tax'
      Size = 15
    end
  end
  object evdsEvoTaxes: TevDataSource
    DataSet = evcsEvoTaxes
    Left = 400
    Top = 256
  end
  object evcsNJTaxSui: TevClientDataSet
    FieldDefs = <
      item
        Name = 'TaxType'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'HasBeenMatched'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'STATE'
        DataType = ftString
        Size = 2
      end>
    Filter = 'HasBeenMatched='#39'F'#39
    Filtered = True
    Left = 232
    Top = 40
    object evcsNJTaxSuiTaxType: TStringField
      FieldName = 'TaxType'
      Size = 4
    end
    object evcsNJTaxSuiHasBeenMatched: TStringField
      FieldName = 'HasBeenMatched'
      Size = 1
    end
    object evcsNJTaxSuiSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
  end
  object evdsNJTaxSui: TevDataSource
    DataSet = evcsNJTaxSui
    Left = 232
    Top = 136
  end
  object evdsCOSUI: TevDataSource
    Left = 416
    Top = 312
  end
  object NJ_TAX: TevClientDataSet
    MasterFields = 'CL_NBR;CO_NBR'
    DetailFields = 'CL_NBR;CO_NBR'
    MasterSource = evdsImportMaster
    Left = 40
    Top = 368
    object NJ_TAXCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object NJ_TAXCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object NJ_TAXTAXTYPE: TStringField
      FieldName = 'TAXTYPE'
      Size = 4
    end
    object NJ_TAXCO_SUI_NBR: TIntegerField
      FieldName = 'CO_SUI_NBR'
    end
    object NJ_TAXDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object NJ_TAXSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
  end
  object evdsNJ_TAX: TevDataSource
    DataSet = NJ_TAX
    Left = 112
    Top = 360
  end
  object csLevelStructure: TevClientDataSet
    Left = 232
    Top = 392
  end
end
