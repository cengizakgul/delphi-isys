object EDIT_TeerSelector: TEDIT_TeerSelector
  Left = 538
  Top = 263
  Width = 259
  Height = 84
  Caption = 'Tier Selector'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TevLabel
    Left = 8
    Top = 8
    Width = 51
    Height = 13
    Caption = 'Select Tier'
  end
  object ComboBox1: TevComboBox
    Left = 8
    Top = 24
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = 'ComboBox1'
    Items.Strings = (
      'Company'
      'Division'
      'Branch'
      'Department'
      'Team')
  end
  object Button1: TevButton
    Left = 168
    Top = 24
    Width = 75
    Height = 25
    Caption = 'DONE'
    Enabled = True
    ModalResult = 1
    TabOrder = 1
  end
end
