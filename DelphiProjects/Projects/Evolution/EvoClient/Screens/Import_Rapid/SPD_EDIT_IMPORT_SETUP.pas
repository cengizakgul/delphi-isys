// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IMPORT_SETUP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls,  wwdbdatetimepicker, Spin, ExtCtrls,
  CheckLst, wwdblook, Grids, Wwdbigrd, Wwdbgrid, Db, Wwdatsrc, Variants,
  EvUtils, SDataStructure, Mask, wwdbedit, Wwdotdot, Wwdbcomb, EvConsts,
  SDDClasses, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  SFieldCodeValues, ISDataAccessComponents, EvDataAccessComponents, SDataDictsystem,
  SDataDictclient, EvUIComponents, EvClientDataSet;

type
  TEDIT_IMPORT_SETUP = class(TForm)
    PageControl1: TevPageControl;
    tbshMatching: TTabSheet;
    lsbxRapid: TListBox;
    bttnMatch: TevButton;
    lsbxEvolution: TListBox;
    lsbxMatched: TListBox;
    bttnDelete: TevButton;
    bttnNext: TevButton;
    bttnCancel: TevButton;
    TabSheet1: TTabSheet;
    evLabel1: TevLabel;
    evdtEffectiveStartDate: TevDBDateTimePicker;
    evButton1: TevButton;
    evseWeekNbr: TevSpinEdit;
    evseMonthNbr: TevSpinEdit;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    TabSheet2: TTabSheet;
    evdtCheckDate: TevDateTimePicker;
    evdtPeriodBegin: TevDateTimePicker;
    evdtPeriodEnd: TevDateTimePicker;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evButton2: TevButton;
    evRadioGroup1: TevRadioGroup;
    evRadioGroup2: TevRadioGroup;
    evCheckListBox1: TevCheckListBox;
    evComboBox1: TevComboBox;
    TabSheet3: TTabSheet;
    evButton3: TevButton;
    evClientDataSet1: TevClientDataSet;
    evDataSource1: TevDataSource;
    evClientDataSet1EE_NBR: TIntegerField;
    evClientDataSet1CO_STATES_NBR: TIntegerField;
    evClientDataSet1STATE: TStringField;
    evClientDataSet1HRSTATUS: TStringField;
    evClientDataSet1EVSTATUS: TStringField;
    TabSheet4: TTabSheet;
    evDBGrid1: TevDBGrid;
    evDBLookupCombo2: TevDBLookupCombo;
    evClientDataSet2: TevClientDataSet;
    evDataSource2: TevDataSource;
    evClientDataSet2HR_E_D: TStringField;
    evClientDataSet2EV_E_D: TStringField;
    evClientDataSet2CL_E_DS_NBR: TIntegerField;
    evButton4: TevButton;
    DM_CLIENT: TDM_CLIENT;
    evDBGrid5: TevDBGrid;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evClientDataSet2HR_DESCRIPTION: TStringField;
    TabSheet5: TTabSheet;
    evDataSource3: TevDataSource;
    evDBComboBox2: TevDBComboBox;
    evLabel8: TevLabel;
    evButton5: TevButton;
    TabSheet6: TTabSheet;
    evEESUI: TevClientDataSet;
    evsEESUI: TevDataSource;
    evEESUISTATE: TStringField;
    evDBGrid2: TevDBGrid;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    evDataSource4: TevDataSource;
    evLabel9: TevLabel;
    evDBGrid3: TevDBGrid;
    evLabel10: TevLabel;
    evButton6: TevButton;
    evERSUI: TevClientDataSet;
    evsERSUI: TevDataSource;
    evERSUISTATE: TStringField;
    evDBLookupCombo4: TevDBLookupCombo;
    evDBLookupCombo5: TevDBLookupCombo;
    evDBLookupCombo6: TevDBLookupCombo;
    evDBLookupCombo7: TevDBLookupCombo;
    evDBLookupCombo8: TevDBLookupCombo;
    evDBLookupCombo9: TevDBLookupCombo;
    evEESUIStorage: TevClientDataSet;
    evERSUIStorage: TevClientDataSet;
    evDBGrid4: TevDBGrid;
    evLabel11: TevLabel;
    evLocals: TevClientDataSet;
    evsLocals: TevDataSource;
    evLocalsRapidLocal: TStringField;
    evDBLookupCombo10: TevDBLookupCombo;
    evLocalsSTATE: TStringField;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    evEESUIAMOUNT3: TStringField;
    evEESUIAMOUNT4: TStringField;
    evEESUIAMOUNT5: TStringField;
    evEESUISUI4: TIntegerField;
    evEESUISUI5: TIntegerField;
    evEESUISUI3: TIntegerField;
    evERSUIAMOUNT8: TStringField;
    evERSUIAMOUNT9: TStringField;
    evERSUIAMOUNT10: TStringField;
    evERSUISUI8: TIntegerField;
    evERSUISUI9: TIntegerField;
    evERSUISUI10: TIntegerField;
    evLocalsEvoLocal: TStringField;
    evLocalsEvoLocalNbr: TIntegerField;
    evGroupBox1: TevGroupBox;
    evCheckBox1: TevCheckBox;
    evLabel12: TevLabel;
    evDateTimePicker1: TevDateTimePicker;
    evrgImpounded: TevRadioGroup;
    TabSheet7: TTabSheet;
    evDBGrid6: TevDBGrid;
    evButton7: TevButton;
    evYTDLocals: TevClientDataSet;
    evYTDLocalsSTATE: TStringField;
    evYTDLocalsRapidLocal: TStringField;
    evYTDLocalsEvoLocal: TStringField;
    evYTDLocalsEvoLocalNbr: TIntegerField;
    evdsYTDLocals: TevDataSource;
    evDBLookupCombo12: TevDBLookupCombo;
    evDataSource5: TevDataSource;
    evDataSource6: TevDataSource;
    evGroupBox2: TevGroupBox;
    evLabel7: TevLabel;
    evDBComboBox1: TevDBComboBox;
    evCheckBox2: TevCheckBox;
    procedure tbshMatchingShow(Sender: TObject);
    procedure lsbxRapidClick(Sender: TObject);
    procedure bttnDeleteClick(Sender: TObject);
    procedure bttnMatchClick(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure evComboBox1Change(Sender: TObject);
    procedure evDBGrid5RowChanged(Sender: TObject);
    procedure evButton3Click(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure evDBLookupCombo2Change(Sender: TObject);
    procedure evDBLookupCombo2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evButton4Click(Sender: TObject);
    procedure evDBLookupCombo3CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure TabSheet5Show(Sender: TObject);
    procedure evDBComboBox1Change(Sender: TObject);
    procedure evDBGrid2RowChanged(Sender: TObject);
    procedure evDBGrid3RowChanged(Sender: TObject);
    procedure evDBGrid4RowChanged(Sender: TObject);
    procedure evDBLookupCombo4CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evDBLookupCombo5CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evDBLookupCombo6CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evDBLookupCombo7CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evDBLookupCombo8CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evDBLookupCombo9CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evDBLookupCombo10CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evDBGrid6RowChanged(Sender: TObject);
    procedure evDBLookupCombo12CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evCheckBox1Click(Sender: TObject);
    procedure evCheckBox2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure FindAllMatches;
    procedure AddMatch;
    function Match(Tmp1, Tmp2: String): Boolean;
    procedure FindMatch;
    procedure DeleteMatch;
  public
    { Public declarations }
    EDsList: TStringList;
    It: String;
    NewFormat: Boolean;
  end;

var
  EDIT_IMPORT_SETUP: TEDIT_IMPORT_SETUP;

implementation

{$R *.DFM}

{ TEDIT_IMPORT_SETUP }

procedure TEDIT_IMPORT_SETUP.AddMatch;
begin
  if lsbxRapid.Items.Count > 0 then
  begin
    lsbxMatched.Items.Add(lsbxRapid.Items.Strings[lsbxRapid.ItemIndex]+'='+lsbxEvolution.Items.Strings[lsbxEvolution.ItemIndex]);
    lsbxRapid.Items.Delete(lsbxRapid.ItemIndex);
  end;
end;

procedure TEDIT_IMPORT_SETUP.FindAllMatches;
var
  X,Y: Integer;
  Tmp1, Tmp2: String;
  MatchFound: Boolean;
begin
  X := 0;
  MatchFound := False;
  while X <> lsbxRapid.Items.Count do
  begin
    Tmp1 := lsbxRapid.Items[X];
    for Y := 0 to lsbxEvolution.Items.Count - 1 do
    begin
      Tmp2 := lsbxEvolution.Items[Y];
      if Match(Tmp1, Tmp2) then//Tmp1 = Tmp2 then
      begin
        lsbxMatched.Items.Add(lsbxRapid.Items[X]+'='+lsbxEvolution.Items[Y]);
        lsbxRapid.Items.Delete(X);
        MatchFound := True;
        break;
      end
      else
        MatchFound := False;
    end;
    if not MatchFound then
      Inc(X)
  end;
end;

procedure TEDIT_IMPORT_SETUP.tbshMatchingShow(Sender: TObject);
begin
  if tbshMatching.Caption = 'Match E/Ds' then
    FindAllMatches;
end;

procedure TEDIT_IMPORT_SETUP.lsbxRapidClick(Sender: TObject);
begin
  if tbshMatching.Caption = 'Match E/Ds' then
    FindMatch;
end;

procedure TEDIT_IMPORT_SETUP.bttnDeleteClick(Sender: TObject);
begin
    DeleteMatch
end;

procedure TEDIT_IMPORT_SETUP.bttnMatchClick(Sender: TObject);
begin
    AddMatch
end;

procedure TEDIT_IMPORT_SETUP.DeleteMatch;
begin
  if lsbxMatched.Items.Count > 0 then
  begin
    lsbxRapid.Items.Add(lsbxMatched.Items.Names[lsbxMatched.ItemIndex]);
    lsbxMatched.Items.Delete(lsbxMatched.ItemIndex);
  end
end;

function TEDIT_IMPORT_SETUP.Match(Tmp1, Tmp2: String): Boolean;
begin
  if Pos('(', Tmp2) > 0 then
    Tmp2 := Copy(Tmp2, 1, Pos('(', Tmp2)-1);
  if Tmp2[2] = '0' then
    Delete(Tmp2, 2, 1);
  if Tmp1[2] = ' ' then
    Delete(Tmp1, 2, 1);
  if Tmp1 = Tmp2 then
    result := True
  else
    result := False;
end;

procedure TEDIT_IMPORT_SETUP.FindMatch;
var
  Tmp1, Tmp2: String;
  X: Integer;
begin
  Tmp1 := lsbxRapid.Items[lsbxRapid.ItemIndex];
  for X := 0  to lsbxEvolution.Items.Count - 1 do
  begin
    Tmp2 := lsbxEvolution.Items[X];
    if Match(Tmp1, Tmp2) then
    begin
      lsbxEvolution.ItemIndex := X;
      Exit;
    end;
  end;
end;

procedure TEDIT_IMPORT_SETUP.TabSheet1Show(Sender: TObject);
begin
  evdtEffectiveStartDate.Date := Now;
end;

procedure TEDIT_IMPORT_SETUP.TabSheet2Show(Sender: TObject);
var
  I: Integer;
begin
  evdtCheckDate.DateTime := now;
  evCheckListBox1.Enabled := False;
  evComboBox1.ItemIndex := 0;
  for I := 0  to evCheckListBox1.Items.Count - 1 do
  begin
    evCheckListBox1.Checked[I] := True;
  end;
end;

procedure TEDIT_IMPORT_SETUP.evComboBox1Change(Sender: TObject);
var
  I: Integer;
begin
  for I := 0  to evCheckListBox1.Items.Count - 1 do
  begin
    evCheckListBox1.Checked[I] := False;
  end;

  case evComboBox1.ItemIndex of
    0:
    begin
      for I := 0  to evCheckListBox1.Items.Count - 1 do
      begin
        evCheckListBox1.Checked[I] := True;
      end;
    end;
    1:
    begin
      for I := 0  to 2 do
      begin
        evCheckListBox1.Checked[I] := True;
      end;
    end;
    2:
    begin
      for I := 3  to 5 do
      begin
        evCheckListBox1.Checked[I] := True;
      end;
    end;
    3:
    begin
      for I := 6  to 8 do
      begin
        evCheckListBox1.Checked[I] := True;
      end;
    end;
    4:
    begin
      for I := 9  to 11 do
      begin
        evCheckListBox1.Checked[I] := True;
      end;
    end;
    5:
    begin
      for I := 0  to 5 do
      begin
        evCheckListBox1.Checked[I] := True;
      end;
    end;
    6:
    begin
      for I := 6  to 11 do
      begin
        evCheckListBox1.Checked[I] := True;
      end;
    end;
    7: evCheckListBox1.Enabled := True;
  end;
end;

procedure TEDIT_IMPORT_SETUP.evDBGrid5RowChanged(Sender: TObject);
var
  sState: String;
  V: Variant;
  SYStatesNbr: Integer;
begin
  inherited;
  SYStatesNbr := 0;
  if (evClientDataSet1.State <> dsInsert) and (evClientDataSet1.RecordCount > 0) then
  begin
    sState := evClientDataSet1.FieldByName('STATE').AsString;
    DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
    V := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', sState, 'SY_STATES_NBR');
    if not VARISNULL(V) then
      SYStatesNbr := V;
    with DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS do
    begin
      DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.DataRequired('SY_STATES_NBR='+IntToStr(SYStatesNbr));
    end;
  end;
end;

procedure TEDIT_IMPORT_SETUP.evButton3Click(Sender: TObject);
var
  CloseIt: Boolean;
begin
  CloseIt := True;
  evClientDataSet1.DisableControls;
  evClientDataSet1.First;
  while not evClientDataSet1.Eof do
  begin
    if evClientDataSet1EVSTATUS.AsString = '' then
    begin
      CloseIt := False;
      evClientDataSet1.EnableControls;
      Break;
    end;
    evClientDataSet1.Next;
  end;

  if CloseIt then
  begin
    evClientDataSet1.EnableControls;
    Self.Close;
  end;
end;

procedure TEDIT_IMPORT_SETUP.TabSheet4Show(Sender: TObject);
begin
  DM_CLIENT.CL_E_DS.DataRequired('ALL');
end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo2Change(Sender: TObject);
begin
//  evClientDataSet2.FieldByName('CL_E_DS_NBR').AsInteger := DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo2CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  evClientDataSet2.FieldByName('CL_E_DS_NBR').AsInteger := DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
end;

procedure TEDIT_IMPORT_SETUP.evButton4Click(Sender: TObject);
var
  CloseIt: Boolean;
begin
  CloseIt := True;
  evClientDataSet2.DisableControls;
  evClientDataSet2.First;
  while not evClientDataSet2.Eof do
  begin
    if evClientDataSet2CL_E_DS_NBR.AsString = '' then
    begin
      CloseIt := False;
      evClientDataSet2.EnableControls;
      Break;
    end;
    evClientDataSet2.Next;
  end;

  if CloseIt then
  begin
    evClientDataSet2.EnableControls;
    Self.Close;
  end;

end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo3CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
var
 I: Integer;
 aValue: String;
begin
  aValue := evDBLookupCombo3.Text;
  for I:=0 to evDBGrid5.SelectedList.Count - 1 do
  begin
    evDBGrid5.DataSource.DataSet.GotoBookmark(evDBGrid5.SelectedList.Items[I]);
    evDBGrid5.DataSource.DataSet.Edit;
    evDBGrid5.DataSource.DataSet.FieldByName('EVSTATUS').AsString := aValue;
    evDBGrid5.DataSource.DataSet.Post;
  end;
end;

procedure TEDIT_IMPORT_SETUP.TabSheet5Show(Sender: TObject);
begin
  evDBComboBox1.Items.Text := TaxDepositStatus_ComboChoices;
  evDBComboBox2.Items.Text := TaxLiabilityAdjustmentType_ComboChoices;
end;

procedure TEDIT_IMPORT_SETUP.evDBComboBox1Change(Sender: TObject);
begin
  evButton5.Enabled := (evDBComboBox1.ItemIndex <> -1);
end;

procedure TEDIT_IMPORT_SETUP.evDBGrid2RowChanged(Sender: TObject);
  var
  sState: String;
begin
  inherited;

  if (evEESUI.State <> dsInsert) and (evEESUI.RecordCount > 0) then
  begin
    sState := evEESUI.FieldByName('STATE').AsString;
    DM_SYSTEM_STATE.SY_STATES.DataRequired('STATE='''+sState+'''');
    with DM_SYSTEM_STATE.SY_SUI do
    begin
      DM_SYSTEM_STATE.SY_SUI.DataRequired('SY_STATES_NBR='+DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsString+' and EE_ER_OR_EE_OTHER_OR_ER_OTHER='''+GROUP_BOX_EE_OTHER+'''');
    end;
  end;
  evEESUIStorage.Data := DM_SYSTEM_STATE.SY_SUI.Data;

end;

procedure TEDIT_IMPORT_SETUP.evDBGrid3RowChanged(Sender: TObject);
 var
  sState: String;
begin
  inherited;

  if (evERSUI.State <> dsInsert) and (evERSUI.RecordCount > 0) then
  begin
    sState := evERSUI.FieldByName('STATE').AsString;
    DM_SYSTEM_STATE.SY_STATES.DataRequired('STATE='''+sState+'''');
    with DM_SYSTEM_STATE.SY_SUI do
    begin
      DM_SYSTEM_STATE.SY_SUI.DataRequired('SY_STATES_NBR='+DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsString+' and EE_ER_OR_EE_OTHER_OR_ER_OTHER='''+GROUP_BOX_ER_OTHER+'''');
    end;
  end;

  evERSUIStorage.Data := DM_SYSTEM_STATE.SY_SUI.Data;

end;

procedure TEDIT_IMPORT_SETUP.evDBGrid4RowChanged(Sender: TObject);
var
  sState: String;
begin
  inherited;

  if (evLocals.State <> dsInsert) and (evLocals.RecordCount > 0) then
  begin
    sState := evLocals.FieldByName('STATE').AsString;
    DM_SYSTEM_STATE.SY_STATES.DataRequired('STATE='''+sState+'''');
    with DM_SYSTEM_LOCAL.SY_LOCALS do
    begin
      DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('SY_STATES_NBR='+DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsString);
    end;
  end;

end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo4CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
   if evDBLookupCombo4.Text <> '' then
    if evEESUI.State = dsEdit then
    begin
      evEESUI.FieldByName('SUI3').AsInteger := evEESUIStorage.FieldBYName('SY_SUI_NBR').AsInteger;
      evEESUI.Post;
    end;
end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo5CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  if evDBLookupCombo5.Text <> '' then
    if evEESUI.State = dsEdit then
    begin
      evEESUI.FieldByName('SUI4').AsInteger := evEESUIStorage.FieldBYName('SY_SUI_NBR').AsInteger;
      evEESUI.Post;
    end;
end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo6CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  if evDBLookupCombo6.Text <> '' then
    if evEESUI.State = dsEdit then
    begin
      evEESUI.FieldByName('SUI5').AsInteger := evEESUIStorage.FieldBYName('SY_SUI_NBR').AsInteger;
      evEESUI.Post;
    end;
end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo7CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  if evDBLookupCombo7.Text <> '' then
    if evERSUI.State = dsEdit then
    begin
      evERSUI.FieldByName('SUI8').AsInteger := evERSUIStorage.FieldBYName('SY_SUI_NBR').AsInteger;
      evERSUI.Post;
    end;
end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo8CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  if evDBLookupCombo8.Text <> '' then
    if evERSUI.State = dsEdit then
    begin
      evERSUI.FieldByName('SUI9').AsInteger := evERSUIStorage.FieldBYName('SY_SUI_NBR').AsInteger;
      evERSUI.Post;
    end;

end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo9CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  if evDBLookupCombo9.Text <> '' then
    if evERSUI.State = dsEdit then
    begin
      evERSUI.FieldByName('SUI10').AsInteger := evERSUIStorage.FieldBYName('SY_SUI_NBR').AsInteger;
      evERSUI.Post;
    end;
end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo10CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  if evDBLookupCombo10.Text <> '' then
    if evLocals.State = dsEdit then
    begin
      evLocals.FieldByName('EvoLocalNbr').AsInteger := DM_SYSTEM_LOCAL.SY_LOCALS.FieldBYName('SY_LOCALS_NBR').AsInteger;
      evLocals.Post;
    end;
end;

procedure TEDIT_IMPORT_SETUP.evDBGrid6RowChanged(Sender: TObject);
var
  sState: String;
begin
  if (evYTDLocals.State <> dsInsert) and (evYTDLocals.RecordCount > 0) then
  begin
    sState := evYTDLocals.FieldByName('STATE').AsString;
    DM_SYSTEM_STATE.SY_STATES.DataRequired('STATE='''+sState+'''');
    with DM_SYSTEM_LOCAL.SY_LOCALS do
    begin
      DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('SY_STATES_NBR='+DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsString);
    end;
  end;
end;

procedure TEDIT_IMPORT_SETUP.evDBLookupCombo12CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  if evDBLookupCombo12.Text <> '' then
    if evYTDLocals.State = dsEdit then
    begin
      evYTDLocals.FieldByName('EvoLocalNbr').AsInteger := DM_SYSTEM_LOCAL.SY_LOCALS.FieldBYName('SY_LOCALS_NBR').AsInteger;
      evYTDLocals.Post;
    end;
end;


procedure TEDIT_IMPORT_SETUP.evCheckBox1Click(Sender: TObject);
begin

  evDateTimePicker1.Enabled := (evCheckBox1.Checked);

end;

procedure TEDIT_IMPORT_SETUP.evCheckBox2Click(Sender: TObject);
begin

  evDBComboBox1.Enabled := (evCheckBox2.Checked);

end;

procedure TEDIT_IMPORT_SETUP.FormShow(Sender: TObject);
begin

  evButton5.Enabled := NewFormat;

end;

end.
