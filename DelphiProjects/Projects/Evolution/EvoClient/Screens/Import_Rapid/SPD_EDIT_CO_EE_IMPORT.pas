// Copyright � 2000-2004 iSystems LLC. All rights reserved.
{******************************************************************************}
{*  Unit Name      : SPD_EDIT_CO_EE_IMPORT                                    *}
{*  Company        : Paydata Payroll Services                                 *}
{*  Author         : Travis Stafford                                          *}
{*  Creation Date  : 2/24/1999                                                *}
{*  Description    : This unit is created for importing company information   *}
{*                   from Rapid Payroll into Evolution                        *}
{*  Last Modification info                                                    *}
{*  --------------------------------------------------------------------------*}
{*              By : Travis Stafford             Date: 3/17/1999              *}
{******************************************************************************}
unit SPD_EDIT_CO_EE_IMPORT;

interface

uses
   SDataStructure, SPD_EDIT_CO_BASE, Dialogs, Db,
  StdCtrls, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid,
  ExtCtrls, ComCtrls, DBCtrls, Controls, Classes, SysUtils, EvUtils, EvConsts,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, CheckLst, DBGrids, wwdblook,
  EvSecElement, EvTypes, Variants, SDDClasses, EvContext,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, SDataDictsystem, SDataDictclient, SDataDicttemp,
  EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, ImgList, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TEDIT_CO_EE_IMPORT = class(TEDIT_CO_BASE)
    tbshImport: TTabSheet;
    wwdsImport: TevDataSource;
    wwcsEE_Import: TevClientDataSet;
    wwcsEE_ImportCUSTOM_EMPLOYEE_NBR: TStringField;
    wwcsEE_ImportCO_DIVISION_NBR: TStringField;
    wwcsEE_ImportCO_BRANCH_NBR: TStringField;
    wwcsEE_ImportCO_DEPT_NBR: TStringField;
    wwcsEE_ImportLAST_NAME: TStringField;
    wwcsEE_ImportFIRST_NAME: TStringField;
    wwcsEE_ImportADDRESS1: TStringField;
    wwcsEE_ImportCITY: TStringField;
    wwcsEE_ImportSTATE: TStringField;
    wwcsEE_ImportZIP_CODE: TStringField;
    wwcsEE_ImportSOCIAL_SECURITY_NBR: TStringField;
    wwcsEE_ImportHIRE_AND_ORGINAL_DATE: TStringField;
    wwcsEE_ImportHOURS: TStringField;
    wwcsEE_ImportPHONE: TStringField;
    wwcsEE_ImportPAY_FREQUENCY: TStringField;
    wwcsEE_ImportFLSA_EXEMPT: TStringField;
    wwcsEE_ImportGENDER: TStringField;
    wwcsEE_ImportTERMINATION_DATE: TStringField;
    wwcsEE_ImportNEXT_RAISE_DATE: TStringField;
    wwcsEE_ImportBIRTH_DATE: TStringField;
    wwcsEE_ImportTIME_CLOCK_IMPORT_NBR: TStringField;
    wwcsEE_ImportPOLICY_AMOUNT: TStringField;
    wwcsEE_ImportTERMINATION_CODE: TStringField;
    OpenDialog1: TOpenDialog;
    wwcsRate_Import: TevClientDataSet;
    wwcsRate_ImportCustom_EE_Nbr: TStringField;
    wwcsRate_ImportSalary: TStringField;
    wwcsRate_ImportRate1: TStringField;
    wwcsRate_ImportRate2: TStringField;
    wwcsRate_ImportOv_Div2: TStringField;
    wwcsRate_ImportOv_Branch2: TStringField;
    wwcsRate_ImportOv_Dept2: TStringField;
    wwcsRate_ImportRate3: TStringField;
    wwcsRate_ImportOv_Div3: TStringField;
    wwcsRate_ImportOv_Branch3: TStringField;
    wwcsRate_ImportOv_Dept3: TStringField;
    wwcsRate_ImportRate4: TStringField;
    wwcsRate_ImportOv_Div4: TStringField;
    wwcsRate_ImportOv_Branch4: TStringField;
    wwcsRate_ImportOv_Dept4: TStringField;
    wwcsRate_ImportRate5: TStringField;
    wwcsRate_ImportOv_Div5: TStringField;
    wwcsRate_ImportOv_Branch5: TStringField;
    wwcsRate_ImportOv_Dept5: TStringField;
    wwcsEE_ImportADDRESS2: TStringField;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    wwDBGrid1: TevDBGrid;
    lablImportType: TevLabel;
    cbImportType: TevComboBox;
    Splitter2: TSplitter;
    Panel6: TPanel;
    evDBGrid1: TevDBGrid;
    evdsTaxes: TevDataSource;
    evcsTaxes: TevClientDataSet;
    evcsTaxesCUSTOM_EMPLOYEE_NUMBER: TStringField;
    TabSheet2: TTabSheet;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    evDBGrid2: TevDBGrid;
    evcsMaritalStatus: TevClientDataSet;
    evdsMaritalStatus: TevDataSource;
    evcsMaritalStatusCO_STATES_NBR: TIntegerField;
    evcsMaritalStatusSTATE: TStringField;
    evcsMaritalStatusRSTATUS: TStringField;
    evcsMaritalStatusESTATUS: TStringField;
    evDBLookupCombo1: TevDBLookupCombo;
    evcsTaxesFSTAT: TStringField;
    evcsTaxesFDEP: TIntegerField;
    evcsTaxesEIC: TStringField;
    evcsTaxesFIT_EX: TStringField;
    evcsTaxesEE_OASDI: TStringField;
    evcsTaxesEE_MEDICARE: TStringField;
    evcsTaxesER_OASDI: TStringField;
    evcsTaxesER_MEDICARE: TStringField;
    evcsTaxesER_FUI: TStringField;
    evcsTaxesSSTAT: TStringField;
    evcsTaxesSDEP: TIntegerField;
    evcsTaxesSIT_ST: TStringField;
    evcsTaxesSUI_ST: TStringField;
    evcsTaxesSDI_ST: TStringField;
    evcsTaxesADDTL_FED_AMOUNT: TFloatField;
    evcsTaxesADDTL_FED_PERCENT: TFloatField;
    evcsTaxesFLAT_FED_AMOUNT: TFloatField;
    evcsTaxesFLAT_FED_PERCENT: TFloatField;
    evcsTaxesADDTL_STATE_AMOUNT: TFloatField;
    evcsTaxesADDTL_STATE_PERCENT: TFloatField;
    evcsTaxesFLAT_STATE_AMOUNT: TFloatField;
    evcsTaxesFLAT_STATE_PERCENT: TFloatField;
    evcsTaxesRECIP_METHOD: TStringField;
    evcsTaxesSECOND_STATE: TStringField;
    evcsTaxesSTATE_WITHHODLING_EX: TStringField;
    evcsTaxesEE_SDI: TStringField;
    evcsTaxesEE_SUI: TStringField;
    evcsTaxesER_SDI: TStringField;
    evcsTaxesER_SUI: TStringField;
    evcsTaxesSECOND_STATE2: TStringField;
    evcsLocals: TevClientDataSet;
    evcsLocalsCUSTOM_EMPLOYEE_NUMBER: TStringField;
    evcsLocalsSTATE_1: TStringField;
    evcsLocalsLOCAL_1: TStringField;
    evcsLocalsLOCAL_TAX_1: TFloatField;
    evcsLocalsSTATE_2: TStringField;
    evcsLocalsLOCAL_2: TStringField;
    evcsLocalsLOCAL_TAX_2: TFloatField;
    evcsLocalsSTATE_3: TStringField;
    evcsLocalsLOCAL_3: TStringField;
    evcsLocalsLOCAL_TAX_3: TFloatField;
    evYTDLocals: TevClientDataSet;
    evYTDLocalsSTATE: TStringField;
    evYTDLocalsRapidLocal: TStringField;
    evYTDLocalsEvoLocal: TStringField;
    evYTDLocalsEvoLocalNbr: TIntegerField;
    btFillGrid: TevBitBtn;
    btImport: TevBitBtn;
    procedure btImportClick(Sender: TObject);
    procedure wwcsEE_ImportAfterScroll(DataSet: TDataSet);
    procedure TabSheet2Show(Sender: TObject);
    procedure evDBGrid2RowChanged(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btFillGridClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure fpEDIT_CO_BASE_CompanyResize(Sender: TObject);
  private
    { Private declarations }
    sFName: string;
    FCoNbr: Integer;
    RapidTeer: Integer;
    LogCount: Integer;
    procedure CLEmployeeExists;
    procedure AddCLEmployee;
    procedure AddEE;
    procedure Log(EmplDesc, Reason: string);
    procedure PopulateTable(aDataSet: TEvBasicClientDataSet; aString1, aString2: string);
    procedure FillGrid(aString: string; aDataSet: TEvBasicClientDataSet);
    function FileIO: string;
    function TestFileName(aString: string): Boolean;
    procedure ReportOutCome;
    procedure AddRates;
    procedure EERateExists;
    function FilterRapidTeer(aDataSet, aDataSet2: TEvBasicClientDataSet; aString1, aString2, aString3, aString4: string): Boolean;
    procedure SetCoNbr(const Value: Integer);
    procedure GetAllLocalInfo;
    procedure AddLocals;
    function CreateEmployeeToLog(CuNbr, FName, LName: String): String;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure LoadTaxFile;
    procedure PopulateTaxTable(aStringList: TStringList);
    procedure FindAllStates;
    procedure AddEEStates;
  public
    { Public declarations }
    property CoNbr: Integer read FCoNbr write SetCoNbr;
    procedure Activate; override;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

uses SPD_EDIT_TEER_SELECTOR, SPD_EDIT_IMPORT_SETUP;

{$R *.DFM}

{******************************************************************************}
{*  Method Name  : AddEE                                                      *)
{*  Parameters   : None                                                       *}
{*  Purpose      : This method is here to write the imported data to the EE   *}
{*                 table                                                      *}
{******************************************************************************}

procedure TEDIT_CO_EE_IMPORT.AddEE;
var
  temp: string;
  EEDesc: String;
begin
  with DM_EMPLOYEE.EE do
  begin
    Tag := 1;
    insert;

    EEDesc := CreateEmployeeToLog(wwcsEE_Import.FieldByName('custom_employee_nbr').AsString, wwcsEE_Import.FieldByName('First_Name').AsString, wwcsEE_Import.FieldByName('Last_Name').AsString);

    if not FilterRapidTeer(DM_EMPLOYEE.EE, wwcsEE_IMPORT, 'CO_DIVISION_NBR', 'co_branch_nbr', 'co_dept_nbr', 'co_team_nbr') then
    begin
      Cancel;
      Log(EEDesc, 'DBDT information is incorrect or missing.');
      Exit;
    end;

    FieldByName('CO_NBR').AsString := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;

    if wwcsEE_Import.FieldByName('custom_employee_nbr').AsString <> '' then
      FieldByName('custom_employee_number').AsString := wwcsEE_Import.FieldByName('custom_employee_nbr').AsString
    else
    begin
      FieldByName('custom_employee_number').AsString := 'Need ID';
      Log(EEDesc, 'custom_employee_number is missing.');
    end;

    if not DM_CLIENT.CL_PERSON.Locate('social_security_number', wwcsEE_Import.FieldByName('social_security_nbr').Value, []) then
    begin
      Cancel;
      Exit;
    end;
   
    temp := wwcsEE_Import.FieldByName('hire_and_orginal_date').AsString;
    if (temp <> '') and (temp[1] <> ' ') then
      FieldByName('original_hire_date').AsDateTime := StrToDate(wwcsEE_Import.FieldByName('hire_and_orginal_date').AsString);

    temp := wwcsEE_Import.FieldByName('hire_and_orginal_date').AsString;
    if (temp <> '') and (temp[1] <> ' ') then
      FieldByName('current_hire_date').AsDateTime := StrToDate(wwcsEE_Import.FieldByName('hire_and_orginal_date').AsString)
    else
      FieldByName('current_hire_date').AsDateTime := now;
      
    FieldByName('standard_hours').AsString := wwcsEE_Import.FieldByName('hours').AsString;

    temp := wwcsEE_Import.FieldByName('pay_frequency').AsString;
    if temp = '' then
    begin
      Cancel;
      Log(EEDesc, 'pay_frequency is empty.');
      Exit;
    end;

    if temp = '52' then
      FieldByName('pay_frequency').AsString := 'W';
    if temp = '26' then
      FieldByName('pay_frequency').AsString := 'B';
    if temp = '12' then
      FieldByName('pay_frequency').AsString := 'M';
    if temp = '24' then
      FieldByName('pay_frequency').AsString := 'S';

    if wwcsEE_Import.FieldByName('flsa_exempt').AsString = '' then
      FieldByName('flsa_exempt').AsString := 'A'
    else
      FieldByName('flsa_exempt').AsString := wwcsEE_Import.FieldByName('flsa_exempt').AsString;

    temp := wwcsEE_Import.FieldByName('termination_date').AsString;
    if (temp <> '') and (temp[1] <> ' ') then
      FieldByName('current_termination_date').AsDateTime := StrToDate(wwcsEE_Import.FieldByName('termination_date').AsString);

    temp := wwcsEE_Import.FieldByName('next_raise_date').AsString;
    if (temp <> '') and (temp[1] <> ' ') then
      FieldByName('next_raise_date').AsDateTime := StrToDate(wwcsEE_Import.FieldByName('next_raise_date').AsString);

    FieldByName('time_clock_number').AsString := wwcsEE_Import.FieldByName('time_clock_import_nbr').AsString;
    FieldByName('group_term_policy_amount').AsString := wwcsEE_Import.FieldByName('policy_amount').AsString;

    temp := wwcsEE_Import.FieldByName('termination_code').AsString;
    if temp = '' then
      FieldByName('current_termination_code').AsString := 'A';
    if temp = 'T' then
      FieldByName('current_termination_code').AsString := 'M';
    if temp = 'L' then
      FieldByName('current_termination_code').AsString := 'V';

    FieldByName('Tipped_Directly').AsString := 'N';
    FieldByName('Distribute_Taxes').AsString := 'N';
    FieldByName('W2_Type').AsString := 'F';
    FieldByName('W2_Pension').AsString := 'N';
    FieldByName('w2_deferred_comp').AsString := 'N';
    FieldByName('w2_deceased').AsString := 'N';
    FieldByName('w2_statutory_employee').AsString := 'N';
    FieldByName('w2_legal_rep').AsString := 'N';

    if evcsTaxes.FieldByName('FIT_EX').AsString = 'Y' then FieldByName('exempt_exclude_ee_fed').AsString := GROUP_BOX_EXEMPT
    else if evcsTaxes.FieldByName('FIT_EX').AsString = 'B' then FieldByName('exempt_exclude_ee_fed').AsString := GROUP_BOX_EXCLUDE
    else if evcsTaxes.FieldByName('FIT_EX').AsString = 'N' then FieldByName('exempt_exclude_ee_fed').AsString := GROUP_BOX_INCLUDE;

    if (evcsTaxes.FieldByName('EE_OASDI').AsString = '') or (evcsTaxes.FieldByName('EE_OASDI').AsString = 'N') then FieldByName('exempt_employee_oasdi').AsString := GROUP_BOX_NO
    else if evcsTaxes.FieldByName('EE_OASDI').AsString = 'Y' then FieldByName('exempt_employee_oasdi').AsString := GROUP_BOX_YES;

    if (evcsTaxes.FieldByName('EE_MEDICARE').AsString = '') or (evcsTaxes.FieldByName('EE_MEDICARE').AsString = 'N') then FieldByName('exempt_employee_medicare').AsString := GROUP_BOX_NO
    else if evcsTaxes.FieldByName('EE_MEDICARE').AsString = 'Y' then FieldByName('exempt_employee_medicare').AsString := GROUP_BOX_YES;

    if (evcsTaxes.FieldByName('ER_OASDI').AsString = '') or (evcsTaxes.FieldByName('ER_OASDI').AsString = 'N') then FieldByName('exempt_employer_oasdi').AsString := GROUP_BOX_NO
    else if evcsTaxes.FieldByName('ER_OASDI').AsString = 'Y' then FieldByName('exempt_employer_oasdi').AsString := GROUP_BOX_YES;

    if (evcsTaxes.FieldByName('ER_MEDICARE').AsString = '') or (evcsTaxes.FieldByName('ER_MEDICARE').AsString = 'N') then FieldByName('exempt_employer_medicare').AsString := GROUP_BOX_NO
    else if evcsTaxes.FieldByName('ER_MEDICARE').AsString = 'Y' then FieldByName('exempt_employer_medicare').AsString := GROUP_BOX_YES;

    if (evcsTaxes.FieldByName('ER_FUI').AsString = '') or (evcsTaxes.FieldByName('ER_FUI').AsString = 'N') then FieldByName('exempt_employer_fui').AsString := GROUP_BOX_NO
    else if evcsTaxes.FieldByName('ER_FUI').AsString = 'Y' then FieldByName('exempt_employer_fui').AsString := GROUP_BOX_YES;

    if  evcsTaxes.FieldByName('ADDTL_FED_AMOUNT').AsFloat <> 0 then
    begin
      FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT;
      FieldByName('OVERRIDE_FED_TAX_VALUE').AsFloat :=  evcsTaxes.FieldByName('ADDTL_FED_AMOUNT').AsFloat;
    end
    else if  evcsTaxes.FieldByName('ADDTL_FED_PERCENT').AsFloat <> 0 then
    begin
      FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT;
      FieldByName('OVERRIDE_FED_TAX_VALUE').AsFloat := evcsTaxes.FieldByName('ADDTL_FED_PERCENT').AsFloat;
    end
    else if  evcsTaxes.FieldByName('FLAT_FED_AMOUNT').AsFloat <> 0 then
    begin
      FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
      FieldByName('OVERRIDE_FED_TAX_VALUE').AsFloat := evcsTaxes.FieldByName('FLAT_FED_AMOUNT').AsFloat;
    end
    else if  evcsTaxes.FieldByName('FLAT_FED_PERCENT').AsFloat <> 0 then
    begin
      FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_REGULAR_PERCENT;
      FieldByName('OVERRIDE_FED_TAX_VALUE').AsFloat := evcsTaxes.FieldByName('FLAT_FED_PERCENT').AsFloat;
    end
    else
      FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_NONE;

    FieldByName('base_returns_on_this_ee').AsString := 'Y';

    if evcsTaxes.FieldByName('EIC').AsString = 'S' then FieldByName('eic').AsString := EIC_SINGLE
    else if evcsTaxes.FieldByName('EIC').AsString = 'M' then FieldByName('eic').AsString := EIC_BOTH_SPOUSES;

    FieldByName('Gov_Garnish_Prior_Child_Suppt').AsString := 'N';
    FieldByName('new_hire_report_sent').AsString := 'C';
    FieldByName('company_or_individual_name').Value := GROUP_BOX_INDIVIDUAL;
    FieldByName('Federal_Marital_Status').Value := evcsTaxes.FieldByName('FSTAT').AsString;//wwcsEE_Import.FieldByName('FSTAT').AsString;

    if evcsTaxes.FieldByName('FDEP').AsString <> '' then
      FieldByName('Number_of_Dependents').Value := evcsTaxes.FieldByName('FDEP').AsString
    else
    begin            
      Cancel;
      Log(EEDesc, 'FDEP is empty.');
      Exit;
    end;

    //DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.DataRequired('ALL');
    AddEEStates;
    post;
    Tag := 0;
  end;
end;


{******************************************************************************}
{*  Method Name  : PopulateTable                                              *)
{*  Parameters   : TEvBasicClientDataSet (dataset to populate)                       *}
{*                 String (Name of field to Populate)                         *}
{*                 String (Data to populate with)                             *}
{*  Purpose      : This method is here to fill the import table with data     *}
{*                                                                            *}
{******************************************************************************}

procedure TEDIT_CO_EE_IMPORT.PopulateTable(aDataSet: TEvBasicClientDataSet; aString1, aString2: string);
begin
  aDataSet.FieldByName(aString1).Value := aString2;
end;

{******************************************************************************}
{*  Method Name  : Log                                                        *)
{*  Parameters   : String (the level the error happened)                      *}
{*  Purpose      : This method creates an error log file if one does not      *}
{*                 already exist and then writes the appropriate error        *}
{*                 message to the file.                                       *}
{******************************************************************************}

procedure TEDIT_CO_EE_IMPORT.Log(EmplDesc, Reason: string);
var
  F: TextFile;
  DoFlush: Boolean;
begin
  Doflush := False;
  INC(LogCount);
  if not FileExists(sFName + '.log') then
  begin
    AssignFile(F, sFName + '.log');
    Rewrite(F);
  end
  else
  begin
    AssignFile(F, sFName + '.log');
    DoFlush := True;
    Append(F);
  end;

  Write(F, 'Employee ('+EmplDesc+') failed to import becuase '+Reason+'   '+DateTimeToStr(now));
  if DoFlush then
    Flush(F);
  CloseFile(F);
{
  if not FileExists(sFName + '.log') then
  begin
    AssignFile(F, sFName + '.log');
    Rewrite(F);
    Writeln(F, 'Cannot insert employee   ' +
      wwcsEE_Import.FieldByName('custom_employee_nbr').AsString + '   ' +
      wwcsEE_Import.FieldByName('Last_Name').AsString + ', ' +
      wwcsEE_Import.FieldByName('First_Name').AsString + '  ' +
      'That Employee already exists on the ' + level + ' level!  ' + DateTimeToStr(Now));
    CloseFile(F);
  end
  else
  begin
    AssignFile(F, sFName + '.log');
    Append(F);
    Writeln(F, 'Cannot insert employee   ' +
      wwcsEE_Import.FieldByName('custom_employee_nbr').AsString + '   ' +
      wwcsEE_Import.FieldByName('Last_Name').AsString + ', ' +
      wwcsEE_Import.FieldByName('First_Name').AsString + '  ' +
      'That Employee already exists on the ' + level + ' level!  ' + DateTimeToStr(Now));
    Flush(F);
    CloseFile(F);
  end;
}
end;

{******************************************************************************}
{*  Method Name  : AddCLEmployee                                              *)
{*  Parameters   : None                                                       *}
{*  Purpose      : This method is here to write the imported data to the      *}
{*                 CL_PERSON table                                            *}
{******************************************************************************}

procedure TEDIT_CO_EE_IMPORT.AddCLEmployee;
var
  temp: string;
  Initial: string;
  EEDesc: String;
  V: Variant;
begin
  //Here will be the code that actually inserts the new record into the cl person table!
  with DM_CLIENT.CL_PERSON do
  begin
    insert;

    temp := CapitalizeWords(wwcsEE_Import.FieldByName('First_Name').AsString);
    if Pos(' ', temp) > 0 then
    begin
      Initial := Copy(temp, Pos(' ', temp) + 1, 1);
      System.delete(temp, Pos(' ', temp), Length(temp));
    end;

    EEDesc := CreateEmployeeToLog(wwcsEE_Import.FieldByName('custom_employee_nbr').AsString, wwcsEE_Import.FieldByName('First_Name').AsString, wwcsEE_Import.FieldByName('Last_Name').AsString);

    if wwcsEE_Import.FieldByName('Last_Name').AsString <> '' then
      FieldByName('Last_Name').Value := CapitalizeWords(wwcsEE_Import.FieldByName('Last_Name').AsString)
    else
    begin
      FieldByName('Last_Name').Value := 'Need LName';
      Log(EEDesc, 'Last_Name is missing.');
    end;


    if Temp <> '' then
      FieldByName('First_Name').Value := temp
    else
    begin
      FieldByName('First_Name').Value := 'Need FName';
      Log(EEDesc, 'First_Name is missing.');
    end;

    if Initial <> '' then
      FieldByName('Middle_Initial').Value := Initial
    else
      FieldByName('Middle_Initial').Clear;

    if wwcsEE_Import.FieldByName('address1').AsString <> '' then
      FieldByName('address1').Value := CapitalizeWords(wwcsEE_Import.FieldByName('address1').AsString)
    else
    begin
      FieldByName('address1').Value := 'Need Address1';
      Log(EEDesc, 'address1 is missing.');
    end;

    FieldByName('address2').Value := CapitalizeWords(wwcsEE_Import.FieldByName('address2').AsString);

    if wwcsEE_Import.FieldByName('city').AsString <> '' then
      FieldByName('city').Value := CapitalizeWords(wwcsEE_Import.FieldByName('city').AsString)
    else
    begin
      FieldByName('city').Value := DM_COMPANY.CO.FieldByName('city').AsString;
      Log(EEDesc, 'city is missing.');
    end;

    if wwcsEE_Import.FieldByName('state').AsString <> '' then
    begin
      FieldByName('state').Value := wwcsEE_Import.FieldByName('state').AsString;
      v := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', wwcsEE_Import.FieldByName('state').AsString, 'SY_STATES_NBR');
      if not VarIsNull(V) then
        FieldByName('RESIDENTIAL_STATE_NBR').Value := V;
    end
    else
    begin
      FieldByName('state').Value := DM_COMPANY.CO.FieldByName('state').AsString;
      v := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', DM_COMPANY.CO.FieldByName('state').AsString, 'SY_STATES_NBR');
      if not VarIsNull(V) then
        FieldByName('RESIDENTIAL_STATE_NBR').Value := V;
      Log(EEDesc, 'state is missing, defaulting Residential state to companies address state.');
    end;

    if wwcsEE_Import.FieldByName('zip_code').AsString <> '' then
      FieldByName('zip_code').Value := wwcsEE_Import.FieldByName('zip_code').AsString
    else
    begin
      FieldByName('zip_code').Value := DM_COMPANY.CO.FieldByName('zip_code').AsString;
      Log(EEDesc, 'zip_code is missing.');
    end;

    if wwcsEE_Import.FieldByName('social_security_nbr').AsString <> '' then
      FieldByName('social_security_number').Value := wwcsEE_Import.FieldByName('social_security_nbr').AsString
    else
    begin
      Cancel;
      Log(EEDesc, 'SOCIAL_SECURITY_NUMBER is missing.');
      Exit;
    end;

    if wwcsEE_Import.FieldByName('gender').AsString = '' then
    begin
      FieldByName('gender').Value := 'N';
    end
    else
      FieldByName('gender').Value := wwcsEE_Import.FieldByName('gender').AsString;

    temp := wwcsEE_Import.FieldByName('birth_date').AsString;
    if (temp <> '') and (temp[1] <> ' ') then
      FieldByName('birth_date').AsString := wwcsEE_Import.FieldByName('birth_date').AsString;
    FieldByName('ein_or_social_security_number').Value := 'S';
    FieldByName('smoker').Value := 'N';
    FieldByName('I9_on_file').Value := 'Y';
    FieldByName('w2_name_suffix').Value := 'n/a';
    FieldByName('ethnicity').Value := 'P';
    FieldByName('veteran').Value := 'A';
    FieldByName('visa_type').Value := 'N';    
    FieldByName('vietnam_veteran').Value := 'A';
    FieldByName('disabled_veteran').Value := 'A';
    FieldByName('military_reserve').Value := 'A';
    temp := wwcsEE_Import.FieldByName('phone').AsString;
    if (temp <> '') and (temp[2] <> '') then
    begin
      while Pos(' ', temp) > 0 do
        System.Delete(temp, Pos(' ', temp), 1);
      FieldByName('phone1').Value := temp;
      //wwcsEE_Import.FieldByName('phone').AsString;
    end;
    post;
  end;
end;

{******************************************************************************}
{*  Method Name  : CLEmployeeExists                                           *)
{*  Parameters   : None                                                       *}
{*  Purpose      : This method checks to make sure that the employee exists   *}
{*                 in the CL_PERSON table: If it does it logs the appropriate *}
{*                 info in the log file; else it calls AddCLEmployee          *}
{******************************************************************************}

procedure TEDIT_CO_EE_IMPORT.CLEmployeeExists;
var
  MyTransactionManager: TTransactionManager;
  MyCount: Integer;
begin
//This function is going to determine whether or not the employee exists already
//or not!

  EvMessage('Don''t forget to set-up Child Support case on employees that need them', mtConfirmation, [mbOK]);

  MyTransactionManager := TTransactionManager.Create(Self);
  MyCount := 1;
  DM_CLIENT.CL_PERSON.DataRequired('ALL');
  DM_EMPLOYEE.EE.DataRequired('ALL');
  DM_EMPLOYEE.EE_STATES.DataRequired('ALL');
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.DataRequired('ALL');
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
  MyTransactionManager.Initialize([DM_CLIENT.CL_PERSON, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL, DM_EMPLOYEE.EE_SCHEDULED_E_DS]);

  with wwcsEE_Import do
  begin
    try
      DisableControls;
      ctx_StartWait('Please wait while the employees are being Imported into Evolution....', RecordCount-1);
      ctx_DataAccess.Importing := True;
      First;
      while not Eof do
      begin
        if DM_CLIENT.CL_PERSON.Locate('SOCIAL_SECURITY_NUMBER', FieldByName('Social_Security_Nbr').Value, []) then
        begin
          if not DM_EMPLOYEE.EE.Locate('CUSTOM_EMPLOYEE_NUMBER;CO_NBR', VarArrayOf([FieldByName('custom_employee_nbr').Value, FCoNbr]), []) then
            AddEE;
        end
        else
        begin
          AddCLEmployee;
          AddEE;
        end;
        Next;
        Inc(MyCount);
        ctx_UpdateWait('Importing employee '+FieldByName('CUSTOM_EMPLOYEE_NBR').AsString+' - '+FieldByName('FIRST_NAME').AsString+' '+FieldByName('LAST_NAME').AsString, MyCount);
      end;
      try
        MyTransactionManager.ApplyUpdates;
      except
        MyTransactionManager.CancelUpdates;
      end;
    finally
      ctx_EndWait;
      EnableControls;
      ctx_DataAccess.Importing := False;
    end;
  end;
end;

{******************************************************************************}
{*  Method Name  : FileIO                                                     *)
{*  Parameters   : None                                                       *}
{*  Purpose      : This method is here to create a Conversion directory on    *}
{*                 the c drive and then copies the import file to that        *}
{*                 directory.  This method returns the import file name       *}
{******************************************************************************}

function TEDIT_CO_EE_IMPORT.FileIO: string;
var
  FName: string;
begin
  if OpenDialog1.Execute then
  begin
    FName := OpenDialog1.FileName;
  end;
  Result := FName;
end;

{******************************************************************************}
{*  Method Name  : FillGrid                                                   *)
{*  Parameters   : String (File Name)                                         *}
{*                 TEvBasicClientDataSet (DataSet to populate with the import info)  *}
{*  Purpose      : This method opens the import file and fills the DataSet    *}
{*                 with the file contents                                     *}
{******************************************************************************}

procedure TEDIT_CO_EE_IMPORT.FillGrid(aString: string; aDataSet: TEvBasicClientDataSet);
var
  slFieldData, slFieldNames: TStringList;
  x: Integer;
  F1: TextFile;
  S: string;
begin
  inherited;
  slFieldData := TStringList.Create;
  slFieldNames := TStringList.Create;
  try
    aDataSet.GetFieldNames(slFieldNames);
    AssignFile(F1, aString);
    Reset(F1);
    while not Eof(F1) do
    begin
      Readln(F1, S);
      slFieldData.CommaText := S;
      if slFieldData.Count = slFieldNames.Count then
      begin
        aDataSet.Insert;
        for x := 0 to slFieldData.Count - 1 do
        begin
          PopulateTable(aDataSet, slFieldNames.Strings[x], slFieldData.Strings[x]);
        end;
        aDataSet.Post;
      end
      else
      begin
        EvMessage('The File you are trying to import doesn''t contain the proper fields!');
        exit;
      end;
    end;
  finally
    CloseFile(F1);
    slFieldData.Free;
    slFieldNames.Free;
  end;
end;

{******************************************************************************}
{*  Method Name  : TestFileName                                               *)
{*  Parameters   : String (filename and path)                                 *}
{*  Purpose      : This method checks to see that the first 4 characters of   *}
{*                 the import file match the currently opened company and     *}
{*                 alse checks to make sure that the remaining characters     *}
{*                 match the correct import type: Returns Boolean             *}
{******************************************************************************}

function TEDIT_CO_EE_IMPORT.TestFileName(aString: string): Boolean;
var
  temp1, Temp2, FName: string;
  COMatch: Boolean;
  ImportMatch: Boolean;
begin
  COMatch := False;
  ImportMatch := False;
  FName := ExtractFileName(aString);
  if Pos('CONV', UpperCase(FName)) > 0 then
    Temp1 := copy(FName, 1, Pos('CONV', UpperCase(FName))-1)
  else if Pos('RATE', UpperCase(FName)) > 0 then
    Temp1 := copy(FName, 1, Pos('RATE', UpperCase(FName))-1)
  else if Pos('LOCL', UpperCase(FName)) > 0 then
    Temp1 := copy(FName, 1, Pos('LOCL', UpperCase(FName))-1)
  else
    Temp1 := copy(FName, 1, 4);
  Temp2 := copy(FName, Pos(Temp1, FName)+Length(Temp1), 4);
  sFName := Temp1 + Temp2;
  if UpperCase(Temp1) = UpperCase(DM_CLIENT.CL.FieldByName('CUSTOM_CLIENT_NUMBER').AsString) then
  begin
    COMatch := True;
  end;
  if (LowerCase(temp2) = 'conv') and (cbImportType.Text = 'Employee') then
  begin
    ImportMatch := True;
  end
  else if (LowerCase(temp2) = 'rate') and (cbImportType.Text = 'Rate') then
  begin
    ImportMatch := True;
  end
  else if (LowerCase(temp2) = 'locl') and (cbImportType.Text = 'Locals') then
  begin
    ImportMatch := True;
  end;
  if (COMatch) and (ImportMatch) then
    Result := True
  else
    Result := False;
end;

{******************************************************************************}
{*  Method Name  : btImportClick (on button click event)                      *)
{*  Parameters   : TObject                                                    *}
{*  Purpose      : This is the event that is fired when the user clicks the   *}
{*                 button to Import the data. Calls OpenClientDataSets        *}
{*                 CLEmployeeExists, EEExists                                 *}
{******************************************************************************}

procedure TEDIT_CO_EE_IMPORT.btImportClick(Sender: TObject);
var
  Rapid: TEDIT_TeerSelector;
begin
  inherited;
  CoNbr := wwdsMaster.DataSet['CO_NBR'];
//  OpenClientDataSets(cbImportType.Text);
  if cbImportType.Text <> 'Locals' then
  begin
    Rapid := TEDIT_TeerSelector.Create(nil);
    if Rapid.ShowModal = mrOk then
      RapidTeer := Rapid.ComboBox1.ItemIndex;
    Rapid.Free;
  end;

  LogCount := 0;

  if cbImportType.Text = 'Employee' then
  begin
    //ctx_StartWait;
    //try
      CLEmployeeExists;
    //finally
     //ctx_EndWait
//    end;
    ReportOutCome;
  end
  else if cbImportType.Text = 'Rate' then
  begin
    //ctx_StartWait;
    //try
      EERateExists;
    //finally
    //  ctx_EndWait;
    //end;
    ReportOutCome;
  end
  else if cbImportType.Text = 'Locals' then
  begin
    AddLocals;
  end;
end;

procedure TEDIT_CO_EE_IMPORT.ReportOutCome;
begin
  if LogCount > 0 then
    EvMessage('There are ' + IntToStr(LogCount) + ' new entries to the log file!')
  else
    EvMessage('All Employees were imported sucessfully!');
end;

procedure TEDIT_CO_EE_IMPORT.AddRates;
var
  MyTransManager: TTransactionManager;
begin
  MyTransManager := TTransactionManager.Create(Self);
  MyTransManager.Initialize([TevClientDataSet(DM_EMPLOYEE.EE), TevClientDataSet(DM_EMPLOYEE.EE_RATES)]);
  with DM_EMPLOYEE.EE_RATES do
  begin
    Insert;
    FieldByName('ee_nbr').Value := DM_EMPLOYEE.EE.FieldByName('ee_nbr').AsString;
    DM_EMPLOYEE.EE.Edit;
    if wwcsRate_ImportSalary.AsString <> '0.00' then
    begin
      DM_EMPLOYEE.EE.FieldByName('Salary_Amount').Value := wwcsRate_ImportSalary.AsString;
    end;
    FieldByName('primary_rate').Value := 'Y';
    FieldByName('rate_number').Value := '1';

    if wwcsRate_ImportRate1.AsString <> '' then
      FieldByName('rate_amount').Value := wwcsRate_ImportRate1.AsString
    else
    begin
      Cancel;
      Log(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'rate amount 1 is empty.');
      Exit;
    end;

    DM_EMPLOYEE.EE.Post;
    post;
    if wwcsRate_ImportRate2.AsString <> '0.00' then
    begin
      Insert;
      FieldByName('ee_nbr').Value := DM_EMPLOYEE.EE.FieldByName('ee_nbr').AsString;
      FieldByName('primary_rate').Value := 'N';
      FieldByName('rate_number').Value := '2';

      if wwcsRate_ImportRate2.AsString <> '' then
        FieldByName('rate_amount').Value := wwcsRate_ImportRate2.AsString
      else
      begin
        Cancel;
        Log(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'rate amount 2 is empty.');
        Exit;
      end;
      
      if (wwcsRate_ImportOv_Div2.AsString <> '') or (wwcsRate_ImportOv_Branch2.AsString <> '') or (wwcsRate_ImportOv_Dept2.AsString <> '')
        then
        if not FilterRapidTeer(DM_EMPLOYEE.EE_RATES, wwcsRate_Import, 'Ov_Div2', 'Ov_Branch2', 'Ov_Dept2', 'Ov_Team2') then
        begin
          Cancel;
          Log(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'DBDT information is incorrect or missing.');
          exit;
        end;
      post;
    end;
    if wwcsRate_ImportRate3.AsString <> '0.00' then
    begin
      Insert;
      FieldByName('ee_nbr').Value := DM_EMPLOYEE.EE.FieldByName('ee_nbr').AsString;
      FieldByName('primary_rate').Value := 'N';
      FieldByName('rate_number').Value := '3';
      
      if wwcsRate_ImportRate3.AsString <> '' then
        FieldByName('rate_amount').Value := wwcsRate_ImportRate3.AsString
      else
      begin
        Cancel;
        Log(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'rate amount 3 is empty.');
        Exit;
      end;

      if (wwcsRate_ImportOv_Div3.AsString <> '') or (wwcsRate_ImportOv_Branch3.AsString <> '') or (wwcsRate_ImportOv_Dept3.AsString <> '')
        then
        if not FilterRapidTeer(DM_EMPLOYEE.EE_RATES, wwcsRate_Import, 'Ov_Div3', 'Ov_Branch3', 'Ov_Dept3', 'Ov_team3') then
        begin
          cancel;
          Log(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'DBDT information is incorrect or missing.');
          exit;
        end;
      post;
    end;
    if wwcsRate_ImportRate4.AsString <> '0.00' then
    begin
      Insert;
      FieldByName('ee_nbr').Value := DM_EMPLOYEE.EE.FieldByName('ee_nbr').AsString;
      FieldByName('primary_rate').Value := 'N';
      FieldByName('rate_number').Value := '4';

      if wwcsRate_ImportRate4.AsString <> '' then
        FieldByName('rate_amount').Value := wwcsRate_ImportRate4.AsString
      else
      begin
        Cancel;
        Log(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'rate amount 4 is empty.');
        Exit;
      end;

      if (wwcsRate_ImportOv_Div4.AsString <> '') or (wwcsRate_ImportOv_Branch4.AsString <> '') or (wwcsRate_ImportOv_Dept4.AsString <> '')
        then
        if not FilterRapidTeer(DM_EMPLOYEE.EE_RATES, wwcsRate_Import, 'Ov_Div4', 'Ov_Branch4', 'Ov_Dept4', 'Ov_Team4') then
        begin
          Cancel;
          Log(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'DBDT information is incorrect or missing.');
          exit;
        end;
        
      post;
    end;
    if wwcsRate_ImportRate5.AsString <> '0.00' then
    begin
      Insert;
      FieldByName('ee_nbr').Value := DM_EMPLOYEE.EE.FieldByName('ee_nbr').AsString;
      FieldByName('primary_rate').Value := 'N';
      FieldByName('rate_number').Value := '5';

      if wwcsRate_ImportRate5.AsString <> '' then
        FieldByName('rate_amount').Value := wwcsRate_ImportRate5.AsString
      else
      begin
        Cancel;
        Log(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'rate amount 5 is empty.');
        Exit;
      end;

      FieldByName('rate_amount').Value := wwcsRate_ImportRate5.AsString;
      if (wwcsRate_ImportOv_Div5.AsString <> '') or (wwcsRate_ImportOv_Branch5.AsString <> '') or (wwcsRate_ImportOv_Dept5.AsString <> '')
        then
        if not FilterRapidTeer(DM_EMPLOYEE.EE_RATES, wwcsRate_Import, 'Ov_Div5', 'Ov_Branch5', 'Ov_Dept5', 'Ov_Team5') then
        begin
          cancel;
          Log(DM_EMPLOYEE.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'DBDT information is incorrect or missing.');
          exit;
        end;

      post;
    end;
  end;
  MyTransManager.ApplyUpdates;
  MyTransManager.Free;
end;

procedure TEDIT_CO_EE_IMPORT.EERateExists;
var
  MyCount: Integer;
begin
//This function is going to determine whether or not the employee exists already
//or not on the employee level!
  with wwcsRate_Import do
  begin
    DisableControls;
    MyCount := 1;
    ctx_StartWait('Please wait while the employee rates are being Imported into Evolution....', RecordCount);

    First;
    while not Eof do
    begin
      if not DM_EMPLOYEE.EE.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(FieldByName('custom_ee_nbr').Value, ' ', 9), []) then
        Log(FieldByName('custom_ee_nbr').AsString, 'Employee does not exist yet in evolution.')
      else
      begin
        if VARISNULL(DM_EMPLOYEE.EE_RATES.Lookup('ee_nbr;primary_rate', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('ee_nbr').AsInteger, 'Y']),
          'rate_number')) then
        begin
          AddRates;
        end;
      end;
      Next;
      Inc(MyCount);
      ctx_UpdateWait('Importing rates for employee '+FieldByName('CUSTOM_EE_NBR').AsString, MyCount);
    end;
    ctx_EndWait;
    EnableControls;
  end;
end;

{******************************************************************************}
{*  Method Name  : FilterRapidDBDT                                                 *)
{*  Parameters   : TEvBasicClientDataSet (DataSet that is being populated)           *}
{*  Purpose      : This method makes sure that the employee has the level that*}
{*                 is specified by the DBDT_LEVEL and then writes the DBDT    *}
{*                 numbers to the TEvBasicClientDataSet; Returns Boolean             *}
{******************************************************************************}

function TEDIT_CO_EE_IMPORT.FilterRapidTeer(aDataSet, aDataSet2: TEvBasicClientDataSet; aString1, aString2, aString3, aString4: string): Boolean;
var
  BranchNbr: Variant;
  DivisionNbr: Variant;
begin
  result := True;
  if RapidTeer = 0 then
  begin
    exit;
  end;
  with aDataSet do
  begin
    case RapidTeer of
      1:
        begin
          if aDataSet2.FieldByName(aString1).AsString = '' then
          begin
            result := False;
          end
          else
          begin
            if DM_COMPANY.CO_DIVISION.Locate('custom_division_number', PadStringLeft(aDataSet2.FieldByName(aString1).Value, ' ', 20),
              [loCaseInsensitive]) then
            begin
              DivisionNbr := DM_COMPANY.CO_DIVISION.FieldByName('co_division_nbr').AsString;
              FieldByName('co_division_nbr').Value := DivisionNbr;
              DM_COMPANY.CO_BRANCH.Locate('custom_branch_number;co_division_nbr',
                VarArrayOf([PadStringLeft(aDataSet2.FieldByName(aString2).Value, ' ', 20), DivisionNbr]), [loCaseInsensitive]);
              BranchNbr := DM_COMPANY.CO_BRANCH.FieldByName('co_branch_nbr').AsString;
              if BranchNbr <> '' then
              begin
                FieldByName('co_branch_nbr').Value := BranchNbr;
                if DM_COMPANY.CO_DEPARTMENT.Locate('custom_department_number;co_branch_nbr',
                      VarArrayOf([PadStringLeft(aDataSet2.FieldByName(aString3).Value, ' ', 20), BranchNbr]), [loCaseInsensitive]) then
                  FieldByName('co_department_nbr').Value := DM_COMPANY.CO_DEPARTMENT.FieldByName('co_department_nbr').AsString;
              end;
            end
            else
            begin
              result := False;
            end;
          end;
        end;
      2:
        begin
          if aDataSet2.FieldByName(aString2).AsString = '' then
          begin
            result := False;
          end
          else
          begin
            if DM_COMPANY.CO_BRANCH.Locate('custom_branch_number', PadStringLeft(aDataSet2.FieldByName(aString2).Value, ' ', 20),
              [loCaseInsensitive]) then
            begin
              BranchNbr := DM_COMPANY.CO_BRANCH.FieldByName('co_branch_nbr').AsString;
              FieldByName('co_branch_nbr').Value := BranchNbr;
              FieldByName('co_division_nbr').Value := DM_COMPANY.CO_BRANCH.FieldByName('co_division_nbr').AsString;
              if DM_COMPANY.CO_DEPARTMENT.Locate('custom_department_number;co_branch_nbr',
                     VarArrayOf([PadStringLeft(aDataSet2.FieldByName(aString3).Value, ' ', 20), BranchNbr]), [loCaseInsensitive]) then
                FieldByName('co_department_nbr').Value := DM_COMPANY.CO_DEPARTMENT.FieldByName('co_department_nbr').AsString;
            end
            else
            begin
              Result := False;
            end;
          end;
        end;
      3:
        begin
          if aDataSet2.FieldByName(aString3).AsString = '' then
          begin
            Result := False;
          end
          else
          begin
            if DM_COMPANY.CO_DEPARTMENT.Locate('custom_department_number', PadStringLeft(aDataSet2.FieldByName(aString3).Value, ' ', 20),
              [loCaseInsensitive]) then
            begin
              FieldByName('co_department_nbr').Value := DM_COMPANY.CO_DEPARTMENT.FieldByName('co_department_nbr').AsString;
              BranchNbr := DM_COMPANY.CO_DEPARTMENT.FieldByName('co_branch_nbr').Value;
              DM_COMPANY.CO_BRANCH.Locate('co_branch_nbr', BranchNbr, [loCaseInsensitive]);
              FieldByName('co_branch_nbr').Value := BranchNbr;
              FieldByName('co_division_nbr').Value := DM_COMPANY.CO_BRANCH.FieldByName('co_division_nbr').AsString;
            end
            else
            begin
              Result := False;
            end;
          end;
        end;
      { 4 : begin
             if aDataSet2.FieldByName(aString3).AsString = '' then
             begin
                Result := False;
             end
             else
             begin
                if DM_COMPANY.CO_DEPARTMENT.Locate('custom_department_number', PadStringLeft(aDataSet2.FieldByName(aString3).Value, ' ', 20), [loCaseInsensitive]) then
                begin
                   FieldByName('co_department_nbr').Value := DM_COMPANY.CO_DEPARTMENT.FieldByName('co_department_nbr').AsString;
                   BranchNbr := DM_COMPANY.CO_DEPARTMENT.FieldByName('co_branch_nbr').Value;
                   DM_COMPANY.CO_BRANCH.Locate('co_branch_nbr', BranchNbr, [loCaseInsensitive]);
                   FieldByName('co_branch_nbr').Value := BranchNbr;
                   FieldByName('co_division_nbr').Value := DM_COMPANY.CO_BRANCH.FieldByName('co_division_nbr').AsString;
                end
                else
                begin
                   Result := False;
                end;
             end;
          end;}
    end;
  end;
end;

procedure TEDIT_CO_EE_IMPORT.SetCoNbr(const Value: Integer);
begin
  FCoNbr := Value;
end;

procedure TEDIT_CO_EE_IMPORT.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_EMPLOYEE.EE, aDS);
  AddDS(DM_EMPLOYEE.EE_RATES, aDS);
  AddDS(DM_CLIENT.CL_PERSON, aDS);
  AddDS(DM_COMPANY.CO, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  inherited;
end;

function TEDIT_CO_EE_IMPORT.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_BRANCH') or (sName = 'CO_DEPARTMENT') or (sName = 'EE_RATES') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_CO_EE_IMPORT.Activate;
begin
  wwcsEE_Import.CreateDataSet;
end;

procedure TEDIT_CO_EE_IMPORT.LoadTaxFile;
var
  sPath, sName, sType, sFileName: String;
  TaxList, Temp: TStringList;
  I: Integer;
begin
  TaxList := TStringList.Create;
  Temp := TStringList.Create;
  sPath := ExtractFilePath(String(OpenDialog1.FileName));
  sName := ExtractFileName(String(OpenDialog1.FileName));
  sType := Copy(UpperCase(sName), 1, Pos('CONV', UpperCase(sName))-1);
  sFileName := sPath+sType+'TAXS.TXT';
  Temp.LoadFromFile(sFileName);
  evcsTaxes.CreateDataSet;
  for I := 0 to Temp.Count - 1 do
  begin
    TaxList.Clear;
    TaxList.CommaText := Temp[I];
    PopulateTaxTable(TaxList);
    evdsTaxes.Active := True;
  end;
  evcsTaxes.Filter := 'CUSTOM_EMPLOYEE_NUMBER='''+wwcsEE_Import.FieldByName('CUSTOM_EMPLOYEE_NBR').AsString+'''';
end;

procedure TEDIT_CO_EE_IMPORT.PopulateTaxTable(aStringList: TStringList);
var
 I: Integer;
begin

  evcsTaxes.Insert;
  for I := 0 to aStringList.Count - 1 do
  begin
    evcsTaxes.Fields.Fields[I].Value := aStringList[I];
  end;
  evcsTaxes.Post;

end;

procedure TEDIT_CO_EE_IMPORT.wwcsEE_ImportAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if evcsTaxes.Active then
    if evcsTaxes.RecordCount <> 0 then
      evcsTaxes.Filter := 'CUSTOM_EMPLOYEE_NUMBER='''+wwcsEE_Import.FieldByName('CUSTOM_EMPLOYEE_NBR').AsString+'''';
end;

procedure TEDIT_CO_EE_IMPORT.FindAllStates;
var
  COStateNbr: String;
begin
  DM_COMPANY.CO_STATES.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  wwcsEE_Import.First;
  while not wwcsEE_Import.Eof do
  begin
    COStateNbr := DM_COMPANY.CO_STATES.Lookup('STATE', evcsTaxes.FieldByName('SIT_ST').Value, 'CO_STATES_NBR');
    if not evcsMaritalStatus.Locate('STATE;RSTATUS', VarArrayOf([evcsTaxes.FieldByName('SIT_ST').Value, evcsTaxes.FieldByName('SSTAT').Value]), []) then
    begin
      evcsMaritalStatus.Insert;
      evcsMaritalStatus.FieldByName('CO_STATES_NBR').AsInteger := StrToInt(COStateNbr);
      evcsMaritalStatus.FieldByName('STATE').AsString := evcsTaxes.FieldByName('SIT_ST').Value;
      evcsMaritalStatus.FieldByName('RSTATUS').AsString := evcsTaxes.FieldByName('SSTAT').Value;
      evcsMaritalStatus.Post;
    end;
    wwcsEE_Import.Next;
  end;
end;

procedure TEDIT_CO_EE_IMPORT.TabSheet2Show(Sender: TObject);
begin
  inherited;
  if Not evcsMaritalStatus.Active then
  begin
    evcsMaritalStatus.CreateDataSet;
    evdsMaritalStatus.Active := True;
    evDBGrid2.SetControlType('ESTATUS',  fctCustom, 'evDBLookupCombo1');
    FindAllStates;
  end;
end;

procedure TEDIT_CO_EE_IMPORT.evDBGrid2RowChanged(Sender: TObject);
var
  sState: String;
  V: Variant;
  SYStatesNbr: Integer;
begin
  inherited;
  SYStatesNbr := 0;
  if (evcsMaritalStatus.State <> dsInsert) and (evcsMaritalStatus.RecordCount > 0) then
  begin
    sState := evcsMaritalStatus.FieldByName('STATE').AsString;
    DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
    V := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', sState, 'SY_STATES_NBR');
    if not VARISNULL(V) then
      SYStatesNbr := V;
    with DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS do
    begin
      DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.DataRequired('SY_STATES_NBR='+IntToStr(SYStatesNbr));
    end;
  end;
end;

procedure TEDIT_CO_EE_IMPORT.AddEEStates;
var
  V: Variant;
  SecState, EEDesc: String;
begin
  with DM_EMPLOYEE.EE_STATES do
  begin
    Insert;

    EEDesc := CreateEmployeeToLog(wwcsEE_Import.FieldByName('custom_employee_nbr').AsString, wwcsEE_Import.FieldByName('First_Name').AsString, wwcsEE_Import.FieldByName('Last_Name').AsString);

    FieldByName('EE_NBR').AsInteger := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;

    V := DM_COMPANY.CO_STATES.Lookup('STATE', evcsTaxes.FieldByName('SIT_ST').Value, 'CO_STATES_NBR');
    if NOT VARISNULL(V) then
      FieldByName('CO_STATES_NBR').AsInteger := V
    else
    begin
      Cancel;
      Log(EEDesc, 'the state '+evcsTaxes.FieldByName('SIT_ST').AsString+' is not setup for this company.');
      Exit;
    end;

    V := evcsMaritalStatus.Lookup('CO_STATES_NBR;RSTATUS', VarArrayOf([V, evcsTaxes.FieldByName('SSTAT').Value]), 'ESTATUS');

    if NOT VARISNULL(V) then
      FieldByName('STATE_MARITAL_STATUS').AsString := V
    else
    begin
      Cancel;
      Log(EEDesc, 'STATE_MARITAL_STATUS is empty.');
      Exit;
    end;

    FieldByName('STATE_NUMBER_WITHHOLDING_ALLOW').AsInteger := evcsTaxes.FieldByName('SDEP').asInteger;

    if evcsTaxes.FieldByName('STATE_WITHHODLING_EX').AsString = 'Y' then FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsTaxes.FieldByName('STATE_WITHHODLING_EX').AsString = 'B' then FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsTaxes.FieldByName('STATE_WITHHODLING_EX').AsString = 'N' then FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if  evcsTaxes.FieldByName('ADDTL_STATE_AMOUNT').AsFloat <> 0 then
    begin
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT;
      FieldByName('OVERRIDE_STATE_TAX_VALUE').AsFloat :=  evcsTaxes.FieldByName('ADDTL_STATE_AMOUNT').AsFloat;
    end
    else if  evcsTaxes.FieldByName('ADDTL_STATE_PERCENT').AsFloat <> 0 then
    begin
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT;
      FieldByName('OVERRIDE_STATE_TAX_VALUE').AsFloat := evcsTaxes.FieldByName('ADDTL_STATE_PERCENT').AsFloat;
    end
    else if  evcsTaxes.FieldByName('FLAT_STATE_AMOUNT').AsFloat <> 0 then
    begin
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
      FieldByName('OVERRIDE_STATE_TAX_VALUE').AsFloat := evcsTaxes.FieldByName('FLAT_STATE_AMOUNT').AsFloat;
    end
    else if  evcsTaxes.FieldByName('FLAT_STATE_PERCENT').AsFloat <> 0 then
    begin
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_PERCENT;
      FieldByName('OVERRIDE_STATE_TAX_VALUE').AsFloat := evcsTaxes.FieldByName('FLAT_STATE_PERCENT').AsFloat;
    end
    else
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;

    if evcsTaxes.FieldByName('SDI_ST').AsString <> '' then
    begin
      V := DM_COMPANY.CO_STATES.Lookup('STATE', evcsTaxes.FieldByName('SDI_ST').Value, 'CO_STATES_NBR');
      if NOT VARISNULL(V) then
        FieldByName('SDI_APPLY_CO_STATES_NBR').AsInteger := V
      else
      begin
        Cancel;
        Log(EEDesc, 'the state '+evcsTaxes.FieldByName('SDI_ST').AsString+' is not setup for this company.');
        Exit;
      end;
    end;

    if evcsTaxes.FieldByName('SUI_ST').AsString <> '' then
    begin
      V := DM_COMPANY.CO_STATES.Lookup('STATE', evcsTaxes.FieldByName('SUI_ST').Value, 'CO_STATES_NBR');
      if NOT VARISNULL(V) then
        FieldByName('SUI_APPLY_CO_STATES_NBR').AsInteger := V
      else
      begin
        Cancel;
        Log(EEDesc, 'the state '+evcsTaxes.FieldByName('SUI_ST').AsString+' is not setup for this company.');
        Exit;
      end;
    end;


    if evcsTaxes.FieldByName('EE_SDI').AsString = 'Y' then FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsTaxes.FieldByName('EE_SDI').AsString = 'B' then FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsTaxes.FieldByName('EE_SDI').AsString = 'N' then FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if evcsTaxes.FieldByName('ER_SDI').AsString = 'Y' then FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsTaxes.FieldByName('ER_SDI').AsString = 'B' then FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsTaxes.FieldByName('ER_SDI').AsString = 'N' then FieldByName('ER_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if evcsTaxes.FieldByName('EE_SUI').AsString = 'Y' then FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsTaxes.FieldByName('EE_SUI').AsString = 'B' then FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsTaxes.FieldByName('EE_SUI').AsString = 'N' then FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if evcsTaxes.FieldByName('ER_SUI').AsString = 'Y' then FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsTaxes.FieldByName('ER_SUI').AsString = 'B' then FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsTaxes.FieldByName('ER_SUI').AsString = 'N' then FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if evcsTaxes.FieldByName('RECIP_METHOD').AsString = '' then FieldByName('RECIPROCAL_METHOD').AsString := STATE_RECIPROCAL_TYPE_NONE
    else if evcsTaxes.FieldByName('RECIP_METHOD').AsString = '1' then FieldByName('RECIPROCAL_METHOD').AsString :=  STATE_RECIPROCAL_TYPE_DIFFERENCE
    else if evcsTaxes.FieldByName('RECIP_METHOD').AsString = '2' then FieldByName('RECIPROCAL_METHOD').AsString :=  STATE_RECIPROCAL_TYPE_FULL
    else if evcsTaxes.FieldByName('RECIP_METHOD').AsString = '3' then
    begin
      FieldByName('RECIPROCAL_METHOD').AsString :=  STATE_RECIPROCAL_TYPE_FLAT;//flat
      FieldByName('RECIPROCAL_AMOUNT_PERCENTAGE').AsFloat := evcsTaxes.FieldByName('FLAT_STATE_AMOUNT').AsFloat;
    end
    else if evcsTaxes.FieldByName('RECIP_METHOD').AsString = '4' then
    begin
      FieldByName('RECIPROCAL_METHOD').AsString :=  STATE_RECIPROCAL_TYPE_PERCENTAGE;//percent
      FieldByName('RECIPROCAL_AMOUNT_PERCENTAGE').AsFloat := evcsTaxes.FieldByName('FLAT_STATE_PERCENT').AsFloat;
    end;

    if (evcsTaxes.FieldByName('SECOND_STATE').AsString <> '') and (evcsTaxes.FieldByName('SECOND_STATE2').AsString <> '') then
    begin
      SecState := evcsTaxes.FieldByName('SECOND_STATE').AsString+evcsTaxes.FieldByName('SECOND_STATE2').AsString;
      V := DM_COMPANY.CO_STATES.Lookup('STATE', SecState, 'CO_STATES_NBR');
      if NOT VARISNULL(V) then
        FieldByName('RECIPROCAL_CO_STATES_NBR').AsInteger := V
      else
      begin
        Cancel;
        Log(EEDesc, 'the state '+evcsTaxes.FieldByName('SECOND_STATE').AsString+evcsTaxes.FieldByName('SECOND_STATE2').AsString+' is not setup for this company.');
        Exit;
      end;
    end;
    Post;
  end;
end;

procedure TEDIT_CO_EE_IMPORT.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if PageControl1.ActivePage = TabSheet2 then
  begin
    evcsMaritalStatus.First;
    while Not evcsMaritalStatus.Eof do
    begin
      if evcsMaritalStatus.FieldByName('ESTATUS').AsString = '' then
      begin
        btImport.Enabled := False;
        raise EInvalidParameters.CreateHelp('You have not yet matched all the necessary state marital status!', IDH_InvalidParameters);
      end;
      evcsMaritalStatus.Next;
    end;
    btImport.Enabled := true;
  end;
end;

procedure TEDIT_CO_EE_IMPORT.GetAllLocalInfo;
begin
  if not evYTDLocals.Active then
    evYTDLocals.CreateDataSet;
  with evcsLocals do
  begin
    First;
    while not Eof do
    begin
      if FieldByName('LOCAL_1').AsString <> '' then
        if not evYTDLocals.Locate('STATE;RapidLocal', VarArrayOf([FieldByName('STATE_1').Value, FieldByName('LOCAL_1').Value]), []) then
        begin
          evYTDLocals.Insert;
          evYTDLocals.FieldByName('STATE').AsString := FieldByName('STATE_1').AsString;
          evYTDLocals.FieldByName('RapidLocal').AsString := FieldByName('LOCAL_1').AsString;
          evYTDLocals.Post;
        end;
      if FieldByName('LOCAL_2').AsString <> '' then
        if not evYTDLocals.Locate('STATE;RapidLocal', VarArrayOf([FieldByName('STATE_2').Value, FieldByName('LOCAL_2').Value]), []) then
        begin
          evYTDLocals.Insert;
          evYTDLocals.FieldByName('STATE').AsString := FieldByName('STATE_2').AsString;
          evYTDLocals.FieldByName('RapidLocal').AsString := FieldByName('LOCAL_2').AsString;
          evYTDLocals.Post;
        end;
      if FieldByName('LOCAL_3').AsString <> '' then
        if not evYTDLocals.Locate('STATE;RapidLocal', VarArrayOf([FieldByName('STATE_3').Value, FieldByName('LOCAL_3').Value]), []) then
        begin
          evYTDLocals.Insert;
          evYTDLocals.FieldByName('STATE').AsString := FieldByName('STATE_3').AsString;
          evYTDLocals.FieldByName('RapidLocal').AsString := FieldByName('LOCAL_3').AsString;
          evYTDLocals.Post;
        end;
      Next;
    end;
  end;
end;

procedure TEDIT_CO_EE_IMPORT.AddLocals;
var
  MyTransactionManager: TTransactionManager;
  MyCount: Integer;
begin
  MyTransactionManager := TTransactionManager.Create(Self);
  MyCount := 0;
  MyTransactionManager.Initialize([DM_EMPLOYEE.EE_LOCALS]);
  with DM_EMPLOYEE.EE_LOCALS, DM_EMPLOYEE, DM_COMPANY do
  begin
    try
      ctx_StartWait('Please wait while the employee locals are being Imported into Evolution....', evcsLocals.RecordCount);
      EE.DataRequired('CO_NBR='''+IntToStr(FCoNbr)+'''');
      EE_LOCALS.DataRequired('ALL');
      evcsLocals.First;
      while not evcsLocals.Eof do
      begin
        if DM_EMPLOYEE.EE.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(evcsLocals.FieldByName('custom_employee_number').Value, ' ', 9), []) then
        begin
          MyCount := MyCount + 1;
          ctx_UpdateWait('Import locals  for employee '+evcsLocals.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, MyCount);
          if evcsLocals.FieldByName('LOCAL_1').AsString <> '' then
            if evYTDLocals.Locate('STATE;RapidLocal', VarArrayOf([evcsLocals.FieldByName('STATE_1').Value, evcsLocals.FieldByName('LOCAL_1').Value]), []) then
            begin
              DM_COMPANY.CO_LOCAL_TAX.DataRequired('SY_LOCALS_NBR='''+evYTDLocals.FieldByName('EvoLocalNbr').AsString+'''');
              if DM_COMPANY.CO_LOCAL_TAX.RecordCount <> 0 then
              begin
                Insert;
                FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;
                FieldByName('CO_LOCAL_TAX_NBR').AsInteger := CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
                if (evcsLocals.FieldByName('LOCAL_TAX_1').AsString <> '0') and (evcsLocals.FieldByName('LOCAL_TAX_1').AsString <> '') then
                  FieldByName('TAX_AMOUNT').AsFloat := evcsLocals.FieldByName('LOCAL_TAX_1').AsFloat;
                FieldByName('EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
                Post;
              end;
            end;

          if evcsLocals.FieldByName('LOCAL_2').AsString <> '' then
            if evYTDLocals.Locate('STATE;RapidLocal', VarArrayOf([evcsLocals.FieldByName('STATE_2').Value, evcsLocals.FieldByName('LOCAL_2').Value]), []) then
            begin
              DM_COMPANY.CO_LOCAL_TAX.DataRequired('SY_LOCALS_NBR='''+evYTDLocals.FieldByName('EvoLocalNbr').AsString+'''');
              if DM_COMPANY.CO_LOCAL_TAX.RecordCount <> 0 then
              begin
                Insert;
                FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;
                FieldByName('CO_LOCAL_TAX_NBR').AsInteger := CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
                if (evcsLocals.FieldByName('LOCAL_TAX_2').AsString <> '0') and (evcsLocals.FieldByName('LOCAL_TAX_2').AsString <> '') then
                  FieldByName('TAX_AMOUNT').AsFloat := evcsLocals.FieldByName('LOCAL_TAX_2').AsFloat;
                FieldByName('EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
                Post;
              end;
            end;
          if evcsLocals.FieldByName('LOCAL_3').AsString <> '' then
            if evYTDLocals.Locate('STATE;RapidLocal', VarArrayOf([evcsLocals.FieldByName('STATE_3').Value, evcsLocals.FieldByName('LOCAL_3').Value]), []) then
            begin
              DM_COMPANY.CO_LOCAL_TAX.DataRequired('SY_LOCALS_NBR='''+evYTDLocals.FieldByName('EvoLocalNbr').AsString+'''');
              if DM_COMPANY.CO_LOCAL_TAX.RecordCount <> 0 then
              begin
                Insert;
                FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;
                FieldByName('CO_LOCAL_TAX_NBR').AsInteger := CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
                if (evcsLocals.FieldByName('LOCAL_TAX_3').AsString <> '0') and (evcsLocals.FieldByName('LOCAL_TAX_3').AsString <> '') then
                  FieldByName('TAX_AMOUNT').AsFloat := evcsLocals.FieldByName('LOCAL_TAX_3').AsFloat;
                FieldByName('EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
                Post;
              end;
            end;
        end
        else
          Log(EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, 'Employee does not yet exist in evolution.');
        evcsLocals.Next;
      end;
      ctx_EndWait;
      MyTransactionManager.ApplyUpdates;
      DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('ALL');
    except
      ctx_EndWait;
      MyTransactionManager.CancelUpdates;
    end;
  end;
end;

function TEDIT_CO_EE_IMPORT.CreateEmployeeToLog(CuNbr, FName, LName: String): String;
begin
  Result := CuNbr+' - '+FName+' '+LName;
end;

{******************************************************************************}
{*  Method Name  : btFillGrid (on button click event)                         *)
{*  Parameters   : TObject                                                    *}
{*  Purpose      : This is the event that is fired when the user clicks the   *}
{*                 button to fill the grid. Calls FileIO, TestFileName,       *}
{*                 FillGrid                                                   *}
{******************************************************************************}
procedure TEDIT_CO_EE_IMPORT.btFillGridClick(Sender: TObject);
var
  FName: string;
  TImportSetup: TEDIT_IMPORT_SETUP;
begin
  if cbImportType.Text = '' then
  begin
    EvMessage('You must choose an import type!');
    exit
  end;
  FName := FileIO;
  if TestFileName(FName) then
  begin
    if cbImportType.Text = 'Employee' then
    begin
      FillGrid(FName, wwcsEE_Import);
      wwdsImport.DataSet := nil;
      wwdsImport.DataSet := wwcsEE_Import;
      wwDBGrid1.DataSource := wwdsImport;
      LoadTaxFile;
      PageControl1.ActivePage := TabSheet2;
    end
    else if cbImportType.Text = 'Rate' then
    begin
      wwcsRate_Import.CreateDataSet;
      FillGrid(FName, wwcsRate_Import);
      wwdsImport.DataSet := nil;
      wwdsImport.DataSet := wwcsRate_Import;
      wwDBGrid1.DataSource := wwdsImport;
    end
    else if cbImportType.Text = 'Locals' then
    begin
      evcsLocals.CreateDataSet;
      FillGrid(FName, evcsLocals);
      wwdsImport.DataSet := nil;
      wwdsImport.DataSet := evcsLocals;
      wwDBGrid1.DataSource := wwdsImport;

      TImportSetup := TEDIT_IMPORT_SETUP.Create(nil);
      TImportSetup.tbshMatching.TabVisible := False;
      TImportSetup.TabSheet1.TabVisible    := False;
      TImportSetup.TabSheet2.TabVisible    := False;
      TImportSetup.TabSheet3.TabVisible    := False;
      TImportSetup.TabSheet4.TabVisible    := False;
      TImportSetup.TabSheet5.TabVisible    := False;
      TImportSetup.TabSheet6.TabVisible    := False;
      TImportSetup.TabSheet7.TabVisible    := True;
      GetAllLocalInfo;
      TImportSetup.evYTDLocals.Data := evYTDLocals.Data;
      TImportSetup.ShowModal;
      evYTDLocals.Data := TImportSetup.evYTDLocals.Data;
      TImportSetup.Free;
    end;
  end
  else
  begin
    EvMessage('The File you have chosen does not match the Import type format' + #13 +
      ' or the company you have select does not match the company in the file!');
    exit;
  end;
end;

procedure TEDIT_CO_EE_IMPORT.evBitBtn1Click(Sender: TObject);
var
  Rapid: TEDIT_TeerSelector;
begin
  inherited;
  CoNbr := wwdsMaster.DataSet['CO_NBR'];
//  OpenClientDataSets(cbImportType.Text);
  if cbImportType.Text <> 'Locals' then
  begin
    Rapid := TEDIT_TeerSelector.Create(nil);
    if Rapid.ShowModal = mrOk then
      RapidTeer := Rapid.ComboBox1.ItemIndex;
    Rapid.Free;
  end;

  LogCount := 0;

  if cbImportType.Text = 'Employee' then
  begin
    //ctx_StartWait;
    //try
      CLEmployeeExists;
    //finally
     // ctx_EndWait
//    end;
    ReportOutCome;
  end
  else if cbImportType.Text = 'Rate' then
  begin
    //ctx_StartWait;
    //try
      EERateExists;
    //finally
    //  ctx_EndWait;
    //end;
    ReportOutCome;
  end
  else if cbImportType.Text = 'Locals' then
  begin
    AddLocals;
  end;
end;

constructor TEDIT_CO_EE_IMPORT.Create(AOwner: TComponent);
begin
  inherited;
  //GUI 2.0 PARENTAL REASSIGNEMENTS
  wwdbgridSelectClient.Parent := fpEDIT_CO_BASE_Company;
  wwdbgridSelectClient.Top    := 35;
  wwdbgridSelectClient.Left   := 12;
end;

procedure TEDIT_CO_EE_IMPORT.fpEDIT_CO_BASE_CompanyResize(Sender: TObject);
begin
  inherited;
  //GUI 2.0 RESIZING
  wwdbgridSelectClient.Width  := fpEDIT_CO_BASE_Company.Width - 40;
  wwdbgridSelectClient.Height := fpEDIT_CO_BASE_Company.Height - 65;
end;

initialization
  RegisterClass(TEDIT_CO_EE_IMPORT);

end.
