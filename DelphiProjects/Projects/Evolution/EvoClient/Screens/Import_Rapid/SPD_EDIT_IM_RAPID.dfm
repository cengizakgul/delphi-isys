inherited EDIT_IM_RAPID: TEDIT_IM_RAPID
  Width = 621
  Height = 397
  object evPanel1: TevPanel [0]
    Left = 0
    Top = 0
    Width = 621
    Height = 397
    Align = alClient
    BevelOuter = bvNone
    Caption = 'evPanel1'
    TabOrder = 0
    object evPanel2: TevPanel
      Left = 0
      Top = 0
      Width = 621
      Height = 30
      Align = alTop
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = 'Select companies and import type'
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object pnlFrameCntr: TPanel
      Left = 0
      Top = 30
      Width = 621
      Height = 367
      Align = alClient
      BevelOuter = bvNone
      BorderStyle = bsSingle
      TabOrder = 1
      object evPageControl1: TevPageControl
        Left = 0
        Top = 0
        Width = 617
        Height = 324
        ActivePage = TabSheet9
        Align = alClient
        TabOrder = 0
        OnChange = evPageControl1Change
        object TabSheet1: TTabSheet
          Caption = 'Main Tab'
          OnShow = TabSheet1Show
          object evrgNewOldSetup: TevRadioGroup
            Left = 392
            Top = 8
            Width = 273
            Height = 89
            Caption = 'What would you like to do?'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemIndex = 0
            Items.Strings = (
              'Configure companies for import'
              'Run a pre-configured import')
            ParentFont = False
            TabOrder = 0
            OnClick = evrgNewOldSetupClick
          end
          object evGroupBox1: TevGroupBox
            Left = 8
            Top = 104
            Width = 425
            Height = 273
            Caption = 'Choose Companies...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object evdgSelectCompany: TevDBGrid
              Left = 7
              Top = 24
              Width = 409
              Height = 241
              DisableThemesInTitle = False
              Selected.Strings = (
                'CUSTOM_COMPANY_NUMBER'#9'20'#9'Number'#9'F'
                'NAME'#9'40'#9'Name'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_IM_RAPID\evdgSelectCompany'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              DataSource = wwdsMaster
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              ParentFont = False
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -16
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
          object evGroupBox2: TevGroupBox
            Left = 440
            Top = 104
            Width = 229
            Height = 273
            Caption = 'Choose imports...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            object evclImportChoices: TevCheckListBox
              Left = 8
              Top = 24
              Width = 209
              Height = 241
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ItemHeight = 16
              Items.Strings = (
                'Employee'
                'Time Off Accrual'
                'Scheduled EDs'
                'Auto Labor'
                'Salary History'
                'Year to Dates'
                'Direct Deposits')
              ParentFont = False
              TabOrder = 0
            end
          end
          object evGroupBox5: TevGroupBox
            Left = 16
            Top = 8
            Width = 369
            Height = 89
            Caption = 'File Options'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            object evLabel5: TevLabel
              Left = 216
              Top = 48
              Width = 49
              Height = 16
              Caption = 'Qualifier'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evrbFixed: TevRadioButton
              Left = 24
              Top = 28
              Width = 65
              Height = 17
              Caption = 'Fixed'
              Checked = True
              TabOrder = 0
              TabStop = True
              OnClick = evrbFixedClick
            end
            object evrbComma: TevRadioButton
              Left = 24
              Top = 61
              Width = 81
              Height = 17
              Caption = 'Comma'
              TabOrder = 1
              OnClick = evrbCommaClick
            end
            object evrbSemiColon: TevRadioButton
              Left = 120
              Top = 28
              Width = 113
              Height = 17
              Caption = 'Semi-Colon'
              TabOrder = 2
              OnClick = evrbSemiColonClick
            end
            object evrbTab: TevRadioButton
              Left = 120
              Top = 61
              Width = 57
              Height = 17
              Caption = 'Tab'
              TabOrder = 3
              OnClick = evrbTabClick
            end
            object evrbOther: TevRadioButton
              Left = 240
              Top = 28
              Width = 65
              Height = 17
              Caption = 'Other'
              TabOrder = 4
              OnClick = evrbOtherClick
            end
            object evedOther: TevEdit
              Left = 311
              Top = 24
              Width = 19
              Height = 24
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxLength = 1
              ParentFont = False
              TabOrder = 5
            end
            object evcbQualifier: TevComboBox
              Left = 216
              Top = 64
              Width = 145
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ItemHeight = 13
              ItemIndex = 0
              ParentFont = False
              TabOrder = 6
              Text = 'None'
              Items.Strings = (
                'None'
                'Single Quote'
                'Double Quote')
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Local Matching'
          ImageIndex = 1
          OnShow = TabSheet2Show
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 878
            Height = 488
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel1'
            TabOrder = 0
            object Panel3: TPanel
              Left = 0
              Top = 0
              Width = 878
              Height = 488
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Panel3'
              TabOrder = 0
              object Splitter1: TSplitter
                Left = 0
                Top = 201
                Width = 878
                Height = 4
                Cursor = crVSplit
                Align = alTop
              end
              object Panel4: TPanel
                Left = 0
                Top = 0
                Width = 878
                Height = 201
                Align = alTop
                BevelOuter = bvNone
                Caption = 'Panel4'
                TabOrder = 0
                object Splitter2: TSplitter
                  Left = 345
                  Top = 0
                  Height = 201
                end
                object Panel8: TPanel
                  Left = 833
                  Top = 0
                  Width = 45
                  Height = 201
                  Align = alRight
                  BevelOuter = bvLowered
                  Caption = 'Panel8'
                  TabOrder = 0
                  DesignSize = (
                    45
                    201)
                  object btnAdd: TBitBtn
                    Left = 2
                    Top = 2
                    Width = 44
                    Height = 200
                    Anchors = [akLeft, akTop, akRight, akBottom]
                    Caption = '&Add'
                    Default = True
                    ModalResult = 6
                    TabOrder = 0
                    OnClick = btnAddClick
                    Glyph.Data = {
                      76010000424D7601000000000000760000002800000020000000100000000100
                      0400000000000001000000000000000000001000000000000000000000000000
                      80000080000000808000800000008000800080800000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
                      888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
                      A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
                      A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
                    Layout = blGlyphBottom
                    NumGlyphs = 2
                  end
                end
                object Panel9: TPanel
                  Left = 0
                  Top = 0
                  Width = 345
                  Height = 201
                  Align = alLeft
                  BevelOuter = bvLowered
                  Caption = 'Panel9'
                  TabOrder = 1
                  object StaticText2: TStaticText
                    Left = 1
                    Top = 1
                    Width = 35
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'File...'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object wwgdFile: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 343
                    Height = 179
                    DisableThemesInTitle = False
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwgdFile'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    OnRowChanged = wwgdFileRowChanged
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evdsRapidLocals
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
                object Panel10: TPanel
                  Left = 348
                  Top = 0
                  Width = 216
                  Height = 201
                  Align = alClient
                  BevelOuter = bvLowered
                  Caption = 'Panel10'
                  TabOrder = 2
                  object StaticText3: TStaticText
                    Left = 1
                    Top = 1
                    Width = 59
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Evolution'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object wwgdEvolution: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 483
                    Height = 179
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'NAME'#9'40'#9'Local'#9'F')
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwgdEvolution'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evdsLocals
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
              end
              object Panel5: TPanel
                Left = 0
                Top = 205
                Width = 609
                Height = 91
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel5'
                TabOrder = 1
                object Panel7: TPanel
                  Left = 0
                  Top = 0
                  Width = 878
                  Height = 283
                  Align = alClient
                  BevelOuter = bvNone
                  Caption = 'Panel7'
                  TabOrder = 0
                  object Panel6: TPanel
                    Left = 834
                    Top = 0
                    Width = 44
                    Height = 283
                    Align = alRight
                    BevelOuter = bvLowered
                    TabOrder = 0
                    DesignSize = (
                      44
                      91)
                    object BitBtn1: TBitBtn
                      Left = 1
                      Top = 0
                      Width = 44
                      Height = 210
                      Anchors = [akLeft, akTop, akRight, akBottom]
                      Caption = '&Remove'
                      Default = True
                      ModalResult = 6
                      TabOrder = 0
                      OnClick = BitBtn1Click
                      Glyph.Data = {
                        76010000424D7601000000000000760000002800000020000000100000000100
                        0400000000000001000000000000000000001000000000000000000000000000
                        80000080000000808000800000008000800080800000C0C0C000808080000000
                        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
                        888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
                        C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
                        C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
                      Layout = blGlyphBottom
                      NumGlyphs = 2
                    end
                  end
                  object Panel11: TPanel
                    Left = 0
                    Top = 0
                    Width = 565
                    Height = 91
                    Align = alClient
                    BevelOuter = bvLowered
                    Caption = 'Panel11'
                    TabOrder = 1
                    object StaticText1: TStaticText
                      Left = 1
                      Top = 1
                      Width = 56
                      Height = 20
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'Matched'
                      Color = clBtnFace
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentColor = False
                      ParentFont = False
                      TabOrder = 0
                    end
                    object wwgdMatched: TevDBGrid
                      Left = 1
                      Top = 21
                      Width = 832
                      Height = 261
                      DisableThemesInTitle = False
                      IniAttributes.Enabled = False
                      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                      IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwgdMatched'
                      IniAttributes.Delimiter = ';;'
                      ExportOptions.ExportType = wwgetSYLK
                      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                      TitleColor = clBtnFace
                      FixedCols = 0
                      ShowHorzScrollBar = True
                      Align = alClient
                      DataSource = DM_IM_RAPID.evdsLOCAL_MATCHING
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                      TabOrder = 1
                      TitleAlignment = taLeftJustify
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      TitleLines = 1
                      UseTFields = True
                      PaintOptions.AlternatingRowColor = 14544093
                      PaintOptions.ActiveRecordColor = clBlack
                      NoFire = False
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'E/D Matching'
          ImageIndex = 3
          OnShow = TabSheet4Show
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 878
            Height = 488
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel1'
            TabOrder = 0
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 878
              Height = 488
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Panel3'
              TabOrder = 0
              object Splitter3: TSplitter
                Left = 0
                Top = 193
                Width = 878
                Height = 4
                Cursor = crVSplit
                Align = alTop
              end
              object Panel14: TPanel
                Left = 0
                Top = 0
                Width = 878
                Height = 193
                Align = alTop
                BevelOuter = bvNone
                Caption = 'Panel4'
                TabOrder = 0
                object Splitter4: TSplitter
                  Left = 337
                  Top = 0
                  Height = 193
                end
                object Panel15: TPanel
                  Left = 834
                  Top = 0
                  Width = 44
                  Height = 193
                  Align = alRight
                  BevelOuter = bvLowered
                  Caption = 'Panel8'
                  TabOrder = 0
                  DesignSize = (
                    44
                    193)
                  object BitBtn2: TBitBtn
                    Left = 2
                    Top = 2
                    Width = 41
                    Height = 192
                    Anchors = [akLeft, akTop, akRight, akBottom]
                    Caption = '&Add'
                    Default = True
                    ModalResult = 6
                    TabOrder = 0
                    OnClick = BitBtn2Click
                    Glyph.Data = {
                      76010000424D7601000000000000760000002800000020000000100000000100
                      0400000000000001000000000000000000001000000000000000000000000000
                      80000080000000808000800000008000800080800000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
                      888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
                      A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
                      A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
                    Layout = blGlyphBottom
                    NumGlyphs = 2
                  end
                end
                object Panel16: TPanel
                  Left = 0
                  Top = 0
                  Width = 337
                  Height = 193
                  Align = alLeft
                  BevelOuter = bvLowered
                  Caption = 'Panel9'
                  TabOrder = 1
                  object StaticText4: TStaticText
                    Left = 1
                    Top = 1
                    Width = 35
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'File...'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object wwgdRapidED: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 335
                    Height = 171
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'RapidED'#9'5'#9'Rapided'#9)
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwgdRapidED'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evdsRapidED
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
                object Panel17: TPanel
                  Left = 340
                  Top = 0
                  Width = 225
                  Height = 193
                  Align = alClient
                  BevelOuter = bvLowered
                  Caption = 'Panel10'
                  TabOrder = 2
                  object StaticText5: TStaticText
                    Left = 1
                    Top = 1
                    Width = 59
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Evolution'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object wwgdEvolED: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 492
                    Height = 171
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'E\D Code'
                      'DESCRIPTION'#9'40'#9'Description')
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwgdEvolED'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evdsEvoED
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
              end
              object Panel18: TPanel
                Left = 0
                Top = 197
                Width = 609
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel5'
                TabOrder = 1
                object Panel19: TPanel
                  Left = 0
                  Top = 0
                  Width = 878
                  Height = 291
                  Align = alClient
                  BevelOuter = bvNone
                  Caption = 'Panel7'
                  TabOrder = 0
                  object Panel20: TPanel
                    Left = 834
                    Top = 0
                    Width = 44
                    Height = 291
                    Align = alRight
                    BevelOuter = bvLowered
                    TabOrder = 0
                    DesignSize = (
                      44
                      99)
                    object btnRemove: TBitBtn
                      Left = 2
                      Top = 2
                      Width = 41
                      Height = 202
                      Anchors = [akLeft, akTop, akRight, akBottom]
                      Caption = 'Re&move'
                      Default = True
                      ModalResult = 6
                      TabOrder = 0
                      OnClick = btnRemoveClick
                      Glyph.Data = {
                        76010000424D7601000000000000760000002800000020000000100000000100
                        0400000000000001000000000000000000001000000000000000000000000000
                        80000080000000808000800000008000800080800000C0C0C000808080000000
                        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
                        888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
                        C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
                        C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
                      Layout = blGlyphBottom
                      NumGlyphs = 2
                    end
                  end
                  object Panel21: TPanel
                    Left = 0
                    Top = 0
                    Width = 565
                    Height = 99
                    Align = alClient
                    BevelOuter = bvLowered
                    Caption = 'Panel11'
                    TabOrder = 1
                    object StaticText6: TStaticText
                      Left = 1
                      Top = 1
                      Width = 56
                      Height = 20
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'Matched'
                      Color = clBtnFace
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentColor = False
                      ParentFont = False
                      TabOrder = 0
                    end
                    object wwgdEDMatched: TevDBGrid
                      Left = 1
                      Top = 21
                      Width = 832
                      Height = 269
                      DisableThemesInTitle = False
                      Selected.Strings = (
                        'RapidED'#9'5'#9'Rapid E/D'#9'F'
                        'EvoED'#9'5'#9'E/D Code'#9'F'
                        'DESCRIPTION'#9'20'#9'Description'#9'F')
                      IniAttributes.Enabled = False
                      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                      IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwgdEDMatched'
                      IniAttributes.Delimiter = ';;'
                      ExportOptions.ExportType = wwgetSYLK
                      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                      TitleColor = clBtnFace
                      FixedCols = 0
                      ShowHorzScrollBar = True
                      Align = alClient
                      DataSource = DM_IM_RAPID.evdsED_MATCHING
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                      TabOrder = 1
                      TitleAlignment = taLeftJustify
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      TitleLines = 1
                      UseTFields = True
                      PaintOptions.AlternatingRowColor = 14544093
                      PaintOptions.ActiveRecordColor = clBlack
                      NoFire = False
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'ED Group Match'
          ImageIndex = 4
          OnShow = TabSheet5Show
          object Panel12: TPanel
            Left = 0
            Top = 0
            Width = 878
            Height = 488
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel1'
            TabOrder = 0
            object Panel23: TPanel
              Left = 0
              Top = 0
              Width = 878
              Height = 488
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Panel3'
              TabOrder = 0
              object Splitter5: TSplitter
                Left = 0
                Top = 193
                Width = 878
                Height = 4
                Cursor = crVSplit
                Align = alTop
              end
              object Panel24: TPanel
                Left = 0
                Top = 0
                Width = 878
                Height = 193
                Align = alTop
                BevelOuter = bvNone
                Caption = 'Panel4'
                TabOrder = 0
                object Splitter6: TSplitter
                  Left = 337
                  Top = 0
                  Height = 193
                end
                object Panel25: TPanel
                  Left = 834
                  Top = 0
                  Width = 44
                  Height = 193
                  Align = alRight
                  BevelOuter = bvLowered
                  Caption = 'Panel8'
                  TabOrder = 0
                  DesignSize = (
                    44
                    193)
                  object BitBtn3: TBitBtn
                    Left = 2
                    Top = 2
                    Width = 41
                    Height = 192
                    Anchors = [akLeft, akTop, akRight, akBottom]
                    Caption = '&Add'
                    Default = True
                    ModalResult = 6
                    TabOrder = 0
                    OnClick = BitBtn3Click
                    Glyph.Data = {
                      76010000424D7601000000000000760000002800000020000000100000000100
                      0400000000000001000000000000000000001000000000000000000000000000
                      80000080000000808000800000008000800080800000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
                      888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
                      A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
                      A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
                    Layout = blGlyphBottom
                    NumGlyphs = 2
                  end
                end
                object Panel26: TPanel
                  Left = 0
                  Top = 0
                  Width = 337
                  Height = 193
                  Align = alLeft
                  BevelOuter = bvLowered
                  Caption = 'Panel9'
                  TabOrder = 1
                  object StaticText7: TStaticText
                    Left = 1
                    Top = 1
                    Width = 35
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'File...'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object wwDBGrid1: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 335
                    Height = 171
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'SpecialOrPercent'#9'2'#9'Special or Percent')
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwDBGrid1'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evdsRapidSpecial
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
                object Panel27: TPanel
                  Left = 340
                  Top = 0
                  Width = 225
                  Height = 193
                  Align = alClient
                  BevelOuter = bvLowered
                  Caption = 'Panel10'
                  TabOrder = 2
                  object StaticText8: TStaticText
                    Left = 1
                    Top = 1
                    Width = 59
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Evolution'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object wwDBGrid2: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 492
                    Height = 171
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'NAME'#9'40'#9'Name')
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwDBGrid2'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = wwdsDetail
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
              end
              object Panel28: TPanel
                Left = 0
                Top = 197
                Width = 609
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel5'
                TabOrder = 1
                object Panel29: TPanel
                  Left = 0
                  Top = 0
                  Width = 878
                  Height = 291
                  Align = alClient
                  BevelOuter = bvNone
                  Caption = 'Panel7'
                  TabOrder = 0
                  object Panel30: TPanel
                    Left = 834
                    Top = 0
                    Width = 44
                    Height = 291
                    Align = alRight
                    BevelOuter = bvLowered
                    TabOrder = 0
                    DesignSize = (
                      44
                      99)
                    object BitBtn4: TBitBtn
                      Left = 2
                      Top = 2
                      Width = 41
                      Height = 216
                      Anchors = [akLeft, akTop, akRight, akBottom]
                      Caption = 'Re&move'
                      Default = True
                      ModalResult = 6
                      TabOrder = 0
                      OnClick = BitBtn4Click
                      Glyph.Data = {
                        76010000424D7601000000000000760000002800000020000000100000000100
                        0400000000000001000000000000000000001000000000000000000000000000
                        80000080000000808000800000008000800080800000C0C0C000808080000000
                        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
                        888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
                        C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
                        C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
                      Layout = blGlyphBottom
                      NumGlyphs = 2
                    end
                  end
                  object Panel31: TPanel
                    Left = 0
                    Top = 0
                    Width = 565
                    Height = 99
                    Align = alClient
                    BevelOuter = bvLowered
                    Caption = 'Panel11'
                    TabOrder = 1
                    object StaticText9: TStaticText
                      Left = 1
                      Top = 1
                      Width = 56
                      Height = 20
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'Matched'
                      Color = clBtnFace
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentColor = False
                      ParentFont = False
                      TabOrder = 0
                    end
                    object wwDBGrid3: TevDBGrid
                      Left = 1
                      Top = 21
                      Width = 832
                      Height = 269
                      DisableThemesInTitle = False
                      Selected.Strings = (
                        'SpecialOrPercent'#9'2'#9'Special Or Percent'
                        'Name'#9'20'#9'Name'#9)
                      IniAttributes.Enabled = False
                      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                      IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwDBGrid3'
                      IniAttributes.Delimiter = ';;'
                      ExportOptions.ExportType = wwgetSYLK
                      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                      TitleColor = clBtnFace
                      FixedCols = 0
                      ShowHorzScrollBar = True
                      Align = alClient
                      DataSource = DM_IM_RAPID.evdsED_GROUP_MATCHING
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                      TabOrder = 1
                      TitleAlignment = taLeftJustify
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      TitleLines = 1
                      UseTFields = True
                      PaintOptions.AlternatingRowColor = 14544093
                      PaintOptions.ActiveRecordColor = clBlack
                      NoFire = False
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Direct Deposits Matching'
          ImageIndex = 5
          OnShow = TabSheet6Show
          object Panel22: TPanel
            Left = 0
            Top = 0
            Width = 878
            Height = 488
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel1'
            TabOrder = 0
            object Panel33: TPanel
              Left = 0
              Top = 0
              Width = 878
              Height = 488
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Panel3'
              TabOrder = 0
              object Splitter7: TSplitter
                Left = 0
                Top = 193
                Width = 878
                Height = 4
                Cursor = crVSplit
                Align = alTop
              end
              object Panel34: TPanel
                Left = 0
                Top = 0
                Width = 878
                Height = 193
                Align = alTop
                BevelOuter = bvNone
                Caption = 'Panel4'
                TabOrder = 0
                object Splitter8: TSplitter
                  Left = 329
                  Top = 0
                  Height = 193
                end
                object Panel35: TPanel
                  Left = 834
                  Top = 0
                  Width = 44
                  Height = 193
                  Align = alRight
                  BevelOuter = bvLowered
                  Caption = 'Panel8'
                  TabOrder = 0
                  DesignSize = (
                    44
                    193)
                  object BitBtn5: TBitBtn
                    Left = 2
                    Top = 2
                    Width = 41
                    Height = 192
                    Anchors = [akLeft, akTop, akRight, akBottom]
                    Caption = '&Add'
                    Default = True
                    ModalResult = 6
                    TabOrder = 0
                    OnClick = BitBtn5Click
                    Glyph.Data = {
                      76010000424D7601000000000000760000002800000020000000100000000100
                      0400000000000001000000000000000000001000000000000000000000000000
                      80000080000000808000800000008000800080800000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
                      888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
                      A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
                      A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
                    Layout = blGlyphBottom
                    NumGlyphs = 2
                  end
                end
                object Panel36: TPanel
                  Left = 0
                  Top = 0
                  Width = 329
                  Height = 193
                  Align = alLeft
                  BevelOuter = bvLowered
                  Caption = 'Panel9'
                  TabOrder = 1
                  object StaticText10: TStaticText
                    Left = 1
                    Top = 1
                    Width = 35
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'File...'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object wwDBGrid4: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 327
                    Height = 171
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'DECODE'#9'3'#9'Deduction Code'#9#9)
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwDBGrid4'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evdsRapidDeposits
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
                object Panel37: TPanel
                  Left = 332
                  Top = 0
                  Width = 233
                  Height = 193
                  Align = alClient
                  BevelOuter = bvLowered
                  Caption = 'Panel10'
                  TabOrder = 2
                  object StaticText11: TStaticText
                    Left = 1
                    Top = 1
                    Width = 59
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Evolution'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object wwDBGrid5: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 500
                    Height = 171
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'Custom E D Code Number'#9#9
                      'DESCRIPTION'#9'40'#9'Description'#9#9
                      'OVERRIDE_RATE_TYPE'#9'1'#9'Override Rate Type'#9'F'#9
                      'SHOW_ON_INPUT_WORKSHEET'#9'1'#9'Show On Input Worksheet'#9'F'#9
                      'SY_STATE_NBR'#9'10'#9'Sy State Nbr'#9'F'#9
                      'GL_OFFSET_TAG'#9'20'#9'Gl Offset Tag'#9'F'#9
                      'SHOW_ED_ON_CHECKS'#9'1'#9'Show Ed On Checks'#9'F'#9
                      'NOTES'#9'10'#9'Notes'#9'F'#9
                      'APPLY_BEFORE_TAXES'#9'1'#9'Apply Before Taxes'#9'F'#9
                      'DEFAULT_HOURS'#9'10'#9'Default Hours'#9'F'#9)
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwDBGrid5'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evdsEvoED
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
              end
              object Panel38: TPanel
                Left = 0
                Top = 197
                Width = 609
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel5'
                TabOrder = 1
                object Panel39: TPanel
                  Left = 0
                  Top = 0
                  Width = 878
                  Height = 291
                  Align = alClient
                  BevelOuter = bvNone
                  Caption = 'Panel7'
                  TabOrder = 0
                  object Panel40: TPanel
                    Left = 834
                    Top = 0
                    Width = 44
                    Height = 291
                    Align = alRight
                    BevelOuter = bvLowered
                    TabOrder = 0
                    DesignSize = (
                      44
                      99)
                    object BitBtn6: TBitBtn
                      Left = 2
                      Top = 2
                      Width = 41
                      Height = 151
                      Anchors = [akLeft, akTop, akRight, akBottom]
                      Caption = 'Re&move'
                      Default = True
                      ModalResult = 6
                      TabOrder = 0
                      OnClick = BitBtn6Click
                      Glyph.Data = {
                        76010000424D7601000000000000760000002800000020000000100000000100
                        0400000000000001000000000000000000001000000000000000000000000000
                        80000080000000808000800000008000800080800000C0C0C000808080000000
                        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
                        888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
                        C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
                        C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
                      Layout = blGlyphBottom
                      NumGlyphs = 2
                    end
                  end
                  object Panel41: TPanel
                    Left = 0
                    Top = 0
                    Width = 565
                    Height = 99
                    Align = alClient
                    BevelOuter = bvLowered
                    Caption = 'Panel11'
                    TabOrder = 1
                    object StaticText12: TStaticText
                      Left = 1
                      Top = 1
                      Width = 56
                      Height = 20
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'Matched'
                      Color = clBtnFace
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentColor = False
                      ParentFont = False
                      TabOrder = 0
                    end
                    object wwDBGrid6: TevDBGrid
                      Left = 1
                      Top = 21
                      Width = 832
                      Height = 269
                      DisableThemesInTitle = False
                      Selected.Strings = (
                        'CL_NBR'#9'10'#9'Cl Nbr'#9'F'#9
                        'CO_NBR'#9'10'#9'Co Nbr'#9'F'#9
                        'DECODE'#9'5'#9'Decode'#9#9
                        'CUSTOM_E_D_CODE_NUMBER'#9'5'#9'Custom E D Code Number'#9#9
                        'DESCRIPTION'#9'30'#9'Description'#9#9
                        'CL_E_DS_NBR'#9'10'#9'Cl E Ds Nbr'#9'F'#9)
                      IniAttributes.Enabled = False
                      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                      IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwDBGrid6'
                      IniAttributes.Delimiter = ';;'
                      ExportOptions.ExportType = wwgetSYLK
                      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                      TitleColor = clBtnFace
                      FixedCols = 0
                      ShowHorzScrollBar = True
                      Align = alClient
                      DataSource = DM_IM_RAPID.evdsDD_MATCHING
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                      TabOrder = 1
                      TitleAlignment = taLeftJustify
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      TitleLines = 1
                      UseTFields = True
                      PaintOptions.AlternatingRowColor = 14544093
                      PaintOptions.ActiveRecordColor = clBlack
                      NoFire = False
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet8: TTabSheet
          Caption = '3rd Party'
          ImageIndex = 7
          OnShow = TabSheet8Show
          object Panel32: TPanel
            Left = 0
            Top = 0
            Width = 878
            Height = 488
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel1'
            TabOrder = 0
            object Panel42: TPanel
              Left = 0
              Top = 0
              Width = 878
              Height = 488
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Panel3'
              TabOrder = 0
              object Splitter9: TSplitter
                Left = 0
                Top = 193
                Width = 878
                Height = 4
                Cursor = crVSplit
                Align = alTop
              end
              object Panel43: TPanel
                Left = 0
                Top = 0
                Width = 878
                Height = 193
                Align = alTop
                BevelOuter = bvNone
                Caption = 'Panel4'
                TabOrder = 0
                object Splitter10: TSplitter
                  Left = 329
                  Top = 0
                  Height = 193
                end
                object Panel44: TPanel
                  Left = 834
                  Top = 0
                  Width = 44
                  Height = 193
                  Align = alRight
                  BevelOuter = bvLowered
                  Caption = 'Panel8'
                  TabOrder = 0
                  DesignSize = (
                    44
                    193)
                  object BitBtn7: TBitBtn
                    Left = 2
                    Top = 2
                    Width = 41
                    Height = 192
                    Anchors = [akLeft, akTop, akRight, akBottom]
                    Caption = '&Add'
                    Default = True
                    ModalResult = 6
                    TabOrder = 0
                    OnClick = BitBtn7Click
                    Glyph.Data = {
                      76010000424D7601000000000000760000002800000020000000100000000100
                      0400000000000001000000000000000000001000000000000000000000000000
                      80000080000000808000800000008000800080800000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
                      888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
                      A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
                      A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                      DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
                    Layout = blGlyphBottom
                    NumGlyphs = 2
                  end
                end
                object Panel45: TPanel
                  Left = 0
                  Top = 0
                  Width = 329
                  Height = 193
                  Align = alLeft
                  BevelOuter = bvLowered
                  Caption = 'Panel9'
                  TabOrder = 1
                  object StaticText13: TStaticText
                    Left = 1
                    Top = 1
                    Width = 35
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'File...'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object evDBGrid1: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 327
                    Height = 171
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'Deduction'#9'2'#9'3rd Party Deduction')
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\evDBGrid1'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evds3rdParty
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
                object Panel46: TPanel
                  Left = 332
                  Top = 0
                  Width = 233
                  Height = 193
                  Align = alClient
                  BevelOuter = bvLowered
                  Caption = 'Panel10'
                  TabOrder = 2
                  object StaticText14: TStaticText
                    Left = 1
                    Top = 1
                    Width = 59
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Evolution'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object evDBGrid2: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 500
                    Height = 171
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'Tax'#9'15'#9'Tax'#9'F')
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\evDBGrid2'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evdsEvoTaxes
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
              end
              object Panel47: TPanel
                Left = 0
                Top = 197
                Width = 609
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel5'
                TabOrder = 1
                object Panel48: TPanel
                  Left = 0
                  Top = 0
                  Width = 878
                  Height = 291
                  Align = alClient
                  BevelOuter = bvNone
                  Caption = 'Panel7'
                  TabOrder = 0
                  object Panel49: TPanel
                    Left = 834
                    Top = 0
                    Width = 44
                    Height = 291
                    Align = alRight
                    BevelOuter = bvLowered
                    TabOrder = 0
                    DesignSize = (
                      44
                      99)
                    object BitBtn8: TBitBtn
                      Left = 2
                      Top = 2
                      Width = 41
                      Height = 216
                      Anchors = [akLeft, akTop, akRight, akBottom]
                      Caption = 'Re&move'
                      Default = True
                      ModalResult = 6
                      TabOrder = 0
                      OnClick = BitBtn8Click
                      Glyph.Data = {
                        76010000424D7601000000000000760000002800000020000000100000000100
                        0400000000000001000000000000000000001000000000000000000000000000
                        80000080000000808000800000008000800080800000C0C0C000808080000000
                        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
                        888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
                        C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
                        C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
                      Layout = blGlyphBottom
                      NumGlyphs = 2
                    end
                  end
                  object Panel50: TPanel
                    Left = 0
                    Top = 0
                    Width = 565
                    Height = 99
                    Align = alClient
                    BevelOuter = bvLowered
                    Caption = 'Panel11'
                    TabOrder = 1
                    object StaticText15: TStaticText
                      Left = 1
                      Top = 1
                      Width = 56
                      Height = 20
                      Align = alTop
                      Alignment = taCenter
                      Caption = 'Matched'
                      Color = clBtnFace
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentColor = False
                      ParentFont = False
                      TabOrder = 0
                    end
                    object evDBGrid3: TevDBGrid
                      Left = 1
                      Top = 21
                      Width = 832
                      Height = 269
                      DisableThemesInTitle = False
                      Selected.Strings = (
                        'Deduction'#9'5'#9'3rd Part Deduction'
                        'Tax'#9'15'#9'Tax'#9)
                      IniAttributes.Enabled = False
                      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                      IniAttributes.SectionName = 'TEDIT_IM_RAPID\evDBGrid3'
                      IniAttributes.Delimiter = ';;'
                      ExportOptions.ExportType = wwgetSYLK
                      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                      TitleColor = clBtnFace
                      FixedCols = 0
                      ShowHorzScrollBar = True
                      Align = alClient
                      DataSource = DM_IM_RAPID.evdsPARTY_MATCHING
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                      TabOrder = 1
                      TitleAlignment = taLeftJustify
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      TitleLines = 1
                      UseTFields = True
                      PaintOptions.AlternatingRowColor = 14544093
                      PaintOptions.ActiveRecordColor = clBlack
                      NoFire = False
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'General Setup'
          ImageIndex = 2
          OnShow = TabSheet3Show
          object evgxSchedEDSetup: TevGroupBox
            Left = 8
            Top = 16
            Width = 593
            Height = 177
            Caption = 'Scheduled E/D Setup'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object evLabel1: TevLabel
              Left = 128
              Top = 120
              Width = 113
              Height = 16
              Caption = 'Effective Start Date'
            end
            object evLabel2: TevLabel
              Left = 320
              Top = 120
              Width = 61
              Height = 16
              Caption = 'Week Nbr'
            end
            object evLabel3: TevLabel
              Left = 416
              Top = 120
              Width = 61
              Height = 16
              Caption = 'Month Nbr'
            end
            object evgxTrreatK: TevRadioGroup
              Left = 16
              Top = 24
              Width = 169
              Height = 89
              Caption = 'Treat DETYPE K as?'
              ItemIndex = 2
              Items.Strings = (
                '% of earn code group'
                '% of gross'
                'None')
              TabOrder = 0
            end
            object evgxTrreatQ: TevRadioGroup
              Left = 208
              Top = 24
              Width = 169
              Height = 89
              Caption = 'Treat DETYPE Q as?'
              ItemIndex = 2
              Items.Strings = (
                '% of earn code group'
                '% of gross'
                'None')
              TabOrder = 1
            end
            object evdtEffectiveStartDate: TevDBDateTimePicker
              Left = 128
              Top = 144
              Width = 121
              Height = 24
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              Epoch = 1950
              ShowButton = True
              TabOrder = 2
            end
            object evseWeekNbr: TevSpinEdit
              Left = 320
              Top = 144
              Width = 49
              Height = 22
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxValue = 4
              MinValue = 1
              ParentFont = False
              TabOrder = 3
              Value = 1
            end
            object evseMonthNbr: TevSpinEdit
              Left = 416
              Top = 144
              Width = 49
              Height = 22
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              MaxValue = 12
              MinValue = 1
              ParentFont = False
              TabOrder = 4
              Value = 1
            end
            object evgxTrreatX: TevRadioGroup
              Left = 400
              Top = 24
              Width = 169
              Height = 89
              Caption = 'Treat DETYPE X as?'
              ItemIndex = 2
              Items.Strings = (
                '% of earn code group'
                '% of gross'
                'None')
              TabOrder = 5
            end
          end
          object evgxYearToDateSetup: TevGroupBox
            Left = 8
            Top = 208
            Width = 369
            Height = 177
            Caption = 'Year To Date Setup'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object evLabel4: TevLabel
              Left = 14
              Top = 24
              Width = 70
              Height = 16
              Caption = 'Check Date'
            end
            object evLabel6: TevLabel
              Left = 14
              Top = 72
              Width = 110
              Height = 16
              Caption = 'Period Begin Date'
            end
            object evLabel7: TevLabel
              Left = 14
              Top = 120
              Width = 99
              Height = 16
              Caption = 'Period End Date'
            end
            object evdtCheckDate: TevDateTimePicker
              Left = 14
              Top = 40
              Width = 105
              Height = 21
              Date = 36763.000000000000000000
              Time = 36763.000000000000000000
              TabOrder = 0
            end
            object evdtPeriodBegin: TevDateTimePicker
              Left = 14
              Top = 88
              Width = 105
              Height = 21
              Date = 36763.000000000000000000
              Time = 36763.000000000000000000
              TabOrder = 1
            end
            object evdtPeriodEnd: TevDateTimePicker
              Left = 14
              Top = 136
              Width = 105
              Height = 21
              Date = 36763.000000000000000000
              Time = 36763.000000000000000000
              TabOrder = 2
            end
            object evCheckListBox2: TevCheckListBox
              Left = 152
              Top = 64
              Width = 201
              Height = 97
              Columns = 3
              ItemHeight = 16
              Items.Strings = (
                'Jan.'
                'Feb.'
                'Mar.'
                'Apr.'
                'May'
                'Jun.'
                'Jul.'
                'Aug.'
                'Sep.'
                'Oct.'
                'Nov.'
                'Dec.')
              TabOrder = 3
            end
            object evComboBox1: TevComboBox
              Left = 152
              Top = 40
              Width = 199
              Height = 24
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 16
              TabOrder = 4
              OnChange = evComboBox1Change
              Items.Strings = (
                'All'
                'First Quarter'
                'Second Quarter'
                'Third Quarter'
                'Fourth Quarter'
                'First Half'
                'Second Half'
                'Custom')
            end
          end
          object evgpAverageStandardHrous: TevRadioGroup
            Left = 384
            Top = 210
            Width = 217
            Height = 56
            Caption = 'Import average hours into standard hours?'
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Yes'
              'No')
            TabOrder = 2
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'Final Tab'
          ImageIndex = 6
          OnShow = TabSheet7Show
          object evGroupBox3: TevGroupBox
            Left = 8
            Top = 80
            Width = 441
            Height = 305
            Caption = 'Select companies to import'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object wwDBGrid7: TevDBGrid
              Left = 16
              Top = 32
              Width = 409
              Height = 257
              DisableThemesInTitle = False
              Selected.Strings = (
                'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom Company Number'#9
                'NAME'#9'40'#9'Name'#9
                'CO_NBR'#9'10'#9'Co Nbr'#9
                'CL_NBR'#9'10'#9'Cl Nbr'#9
                'DETYPEK'#9'10'#9'Detypek'#9
                'DETYPEQ'#9'10'#9'Detypeq'#9
                'EFFECTIVE_START_DATE'#9'18'#9'Effective Start Date'#9
                'WEEK_NBR'#9'10'#9'Week Nbr'#9
                'MONTH_NBR'#9'10'#9'Month Nbr'#9
                'CHECK_DATE'#9'18'#9'Check Date'#9
                'PERIOD_BEGIN_DATE'#9'18'#9'Period Begin Date'#9
                'PERIOD_END_DATE'#9'18'#9'Period End Date'#9
                'JAN'#9'1'#9'Jan'#9
                'FEB'#9'1'#9'Feb'#9
                'MAR'#9'1'#9'Mar'#9
                'APR'#9'1'#9'Apr'#9
                'MAY'#9'1'#9'May'#9
                'JUN'#9'1'#9'Jun'#9
                'JUL'#9'1'#9'Jul'#9
                'AUG'#9'1'#9'Aug'#9
                'SEP'#9'1'#9'Sep'#9
                'OCT'#9'1'#9'Oct'#9
                'NOV'#9'1'#9'Nov'#9
                'DEC'#9'1'#9'Dec'#9
                'LIAB_STATUS'#9'1'#9'Liab Status'#9
                'ADJUST_TYPE'#9'1'#9'Adjust Type'#9
                'DUE_DATE'#9'18'#9'Due Date'#9
                'USE_DATE'#9'1'#9'Use Date'#9
                'COL_FOR_QUARTERLIES'#9'1'#9'Col For Quarterlies'#9
                'LOCAL_MATCHING'#9'10'#9'Local Matching'#9
                'ED_MATCHING'#9'10'#9'Ed Matching'#9
                'ED_GROUP_MATCHING'#9'10'#9'Ed Group Matching'#9
                'DD_MATCHING'#9'10'#9'Dd Matching'#9)
              IniAttributes.Enabled = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_IM_RAPID\wwDBGrid7'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              OnRowChanged = wwDBGrid7RowChanged
              FixedCols = 0
              ShowHorzScrollBar = True
              DataSource = DM_IM_RAPID.evdsImportMaster
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -16
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              UseTFields = True
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
          object evGroupBox4: TevGroupBox
            Left = 464
            Top = 80
            Width = 201
            Height = 305
            Caption = 'Select imports to run'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object evclImportsToRun: TevCheckListBox
              Left = 16
              Top = 32
              Width = 169
              Height = 257
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ItemHeight = 16
              Items.Strings = (
                'Employee'
                'Direct Deposits'
                'Scheduled EDs'
                'Time Off Accrual'
                'Auto Labor'
                'Salary History'
                'Year to Dates')
              ParentFont = False
              TabOrder = 0
            end
          end
          object evrgSetupImport: TevRadioGroup
            Left = 8
            Top = 8
            Width = 657
            Height = 65
            Caption = 'Setup or Import'
            Columns = 2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ItemIndex = 1
            Items.Strings = (
              'Setup another company'
              'Run imports now')
            ParentFont = False
            TabOrder = 2
          end
        end
        object TabSheet9: TTabSheet
          Caption = 'NJSUIMatch'
          ImageIndex = 8
          OnShow = TabSheet9Show
          object Panel51: TPanel
            Left = 0
            Top = 0
            Width = 609
            Height = 296
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel3'
            TabOrder = 0
            object Splitter11: TSplitter
              Left = 0
              Top = 193
              Width = 609
              Height = 4
              Cursor = crVSplit
              Align = alTop
            end
            object Panel52: TPanel
              Left = 0
              Top = 0
              Width = 609
              Height = 193
              Align = alTop
              BevelOuter = bvNone
              Caption = 'Panel4'
              TabOrder = 0
              object Splitter12: TSplitter
                Left = 337
                Top = 0
                Height = 193
              end
              object Panel53: TPanel
                Left = 565
                Top = 0
                Width = 44
                Height = 193
                Align = alRight
                BevelOuter = bvLowered
                Caption = 'Panel8'
                TabOrder = 0
                DesignSize = (
                  44
                  193)
                object BitBtn9: TBitBtn
                  Left = 2
                  Top = 2
                  Width = 41
                  Height = 192
                  Anchors = [akLeft, akTop, akRight, akBottom]
                  Caption = '&Add'
                  Default = True
                  ModalResult = 6
                  TabOrder = 0
                  OnClick = BitBtn9Click
                  Glyph.Data = {
                    76010000424D7601000000000000760000002800000020000000100000000100
                    0400000000000001000000000000000000001000000000000000000000000000
                    80000080000000808000800000008000800080800000C0C0C000808080000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                    DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
                    DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                    DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
                    888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
                    A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
                    A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                    DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
                    DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
                  Layout = blGlyphBottom
                  NumGlyphs = 2
                end
              end
              object Panel54: TPanel
                Left = 0
                Top = 0
                Width = 337
                Height = 193
                Align = alLeft
                BevelOuter = bvLowered
                Caption = 'Panel9'
                TabOrder = 1
                object StaticText16: TStaticText
                  Left = 1
                  Top = 1
                  Width = 335
                  Height = 20
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'File...'
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  TabOrder = 0
                end
                object evDBGrid4: TevDBGrid
                  Left = 1
                  Top = 21
                  Width = 335
                  Height = 171
                  DisableThemesInTitle = False
                  Selected.Strings = (
                    'TaxType'#9'2'#9'Tax Type')
                  IniAttributes.Enabled = False
                  IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                  IniAttributes.SectionName = 'TEDIT_IM_RAPID\evDBGrid4'
                  IniAttributes.Delimiter = ';;'
                  ExportOptions.ExportType = wwgetSYLK
                  ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                  TitleColor = clBtnFace
                  FixedCols = 0
                  ShowHorzScrollBar = True
                  Align = alClient
                  DataSource = DM_IM_RAPID.evdsNJTaxSui
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                  TabOrder = 1
                  TitleAlignment = taLeftJustify
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  TitleLines = 1
                  UseTFields = True
                  PaintOptions.AlternatingRowColor = 14544093
                  PaintOptions.ActiveRecordColor = clBlack
                  NoFire = False
                end
              end
              object Panel55: TPanel
                Left = 340
                Top = 0
                Width = 225
                Height = 193
                Align = alClient
                BevelOuter = bvLowered
                Caption = 'Panel10'
                TabOrder = 2
                object StaticText17: TStaticText
                  Left = 1
                  Top = 1
                  Width = 223
                  Height = 20
                  Align = alTop
                  Alignment = taCenter
                  Caption = 'Evolution'
                  Color = clBtnFace
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  TabOrder = 0
                end
                object evDBGrid5: TevDBGrid
                  Left = 1
                  Top = 21
                  Width = 223
                  Height = 171
                  DisableThemesInTitle = False
                  Selected.Strings = (
                    'DESCRIPTION'#9'40'#9'Description')
                  IniAttributes.Enabled = False
                  IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                  IniAttributes.SectionName = 'TEDIT_IM_RAPID\evDBGrid5'
                  IniAttributes.Delimiter = ';;'
                  ExportOptions.ExportType = wwgetSYLK
                  ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                  TitleColor = clBtnFace
                  FixedCols = 0
                  ShowHorzScrollBar = True
                  Align = alClient
                  Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                  TabOrder = 1
                  TitleAlignment = taLeftJustify
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  TitleLines = 1
                  UseTFields = True
                  PaintOptions.AlternatingRowColor = 14544093
                  PaintOptions.ActiveRecordColor = clBlack
                  NoFire = False
                end
              end
            end
            object Panel56: TPanel
              Left = 0
              Top = 197
              Width = 609
              Height = 99
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Panel5'
              TabOrder = 1
              object Panel57: TPanel
                Left = 0
                Top = 0
                Width = 609
                Height = 99
                Align = alClient
                BevelOuter = bvNone
                Caption = 'Panel7'
                TabOrder = 0
                object Panel58: TPanel
                  Left = 565
                  Top = 0
                  Width = 44
                  Height = 99
                  Align = alRight
                  BevelOuter = bvLowered
                  Caption = 'Panel6'
                  TabOrder = 0
                  DesignSize = (
                    44
                    99)
                  object BitBtn10: TBitBtn
                    Left = 2
                    Top = 2
                    Width = 41
                    Height = 216
                    Anchors = [akLeft, akTop, akRight, akBottom]
                    Caption = 'Re&move'
                    Default = True
                    ModalResult = 6
                    TabOrder = 0
                    Glyph.Data = {
                      76010000424D7601000000000000760000002800000020000000100000000100
                      0400000000000001000000000000000000001000000000000000000000000000
                      80000080000000808000800000008000800080800000C0C0C000808080000000
                      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
                      888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
                      C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
                      C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                      DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
                    Layout = blGlyphBottom
                    NumGlyphs = 2
                  end
                end
                object Panel59: TPanel
                  Left = 0
                  Top = 0
                  Width = 565
                  Height = 99
                  Align = alClient
                  BevelOuter = bvLowered
                  Caption = 'Panel11'
                  TabOrder = 1
                  object StaticText18: TStaticText
                    Left = 1
                    Top = 1
                    Width = 563
                    Height = 20
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Matched'
                    Color = clBtnFace
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                  end
                  object evDBGrid6: TevDBGrid
                    Left = 1
                    Top = 21
                    Width = 563
                    Height = 77
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'TAXTYPE'#9'2'#9'Tax Type'
                      'DESCRIPTION'#9'40'#9'Sui Description'#9)
                    IniAttributes.Enabled = False
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TEDIT_IM_RAPID\evDBGrid6'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = DM_IM_RAPID.evdsNJ_TAX
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 1
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    UseTFields = True
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
              end
            end
          end
        end
      end
      object evPanel3: TevPanel
        Left = 0
        Top = 324
        Width = 617
        Height = 39
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object evBitBtn1: TevBitBtn
          Left = 568
          Top = 8
          Width = 97
          Height = 25
          Caption = 'Next'
          Default = True
          ModalResult = 1
          TabOrder = 0
          OnClick = btbNextClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDD2ADDDDDDDDDDDDDDFFDDDDDDDDDDDDDD2AAADDDDD
            DDDDDDD8DFFDDDDDDDDDDDD2AAAAADDDDDDDDDD8DDDFFDDDDDDDDDD2AAAAAAAD
            DDDDDDD8DDDDDFFDDDDDDDD2AAAAAAAAADDDDDD8DDDDDDDFFDDDDDD2AAAAAAAA
            222DDDD8DDDDDDDD8FFDDDD2AAAAAA2222DDDDD8DDDDDD8888DDDDD2AAAA2222
            DDDDDDD8DDDD8888DDDDDDD2AA2222DDDDDDDDD8DD8888DDDDDDDDD22222DDDD
            DDDDDDD88888DDDDDDDDDDD222DDDDDDDDDDDDD888DDDDDDDDDDDDD2DDDDDDDD
            DDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
          Layout = blGlyphRight
          Margin = 0
        end
        object evBitBtn2: TevBitBtn
          Left = 23
          Top = 8
          Width = 97
          Height = 25
          Caption = 'Previous'
          Enabled = False
          ModalResult = 4
          TabOrder = 1
          OnClick = evBitBtn2Click
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDA2DDDDDDDDDDDDDDFFDDDDDDDDDDDDAAA
            2DDDDDDDDDDDDFFD8DDDDDDDDDDAAAAA2DDDDDDDDDDFFDDD8DDDDDDDDAAAAAAA
            2DDDDDDDDFFDDDDD8DDDDDDAAAAAAAAA2DDDDDDFFDDDDDDD8DDDD222AAAAAAAA
            2DDDDFF8DDDDDDDD8DDDDD2222AAAAAA2DDDDD8888DDDDDD8DDDDDDD2222AAAA
            2DDDDDDD8888DDDD8DDDDDDDDD2222AA2DDDDDDDDD8888DD8DDDDDDDDDDD2222
            2DDDDDDDDDDD88888DDDDDDDDDDDDD222DDDDDDDDDDDDD888DDDDDDDDDDDDDDD
            2DDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object evBitBtn3: TevBitBtn
          Left = 208
          Top = 8
          Width = 97
          Height = 25
          Caption = 'Save Setup'
          Enabled = False
          TabOrder = 2
          OnClick = evBitBtn3Click
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
            DDDDD8888888888DDDDD848747774448DDDD888D8DDD888DDDDD448747774448
            DDDD888D8DDD888DDDDD448777774448DDDD888DDDDD888DDDDD444444444448
            DDDD88888888888DDDDD444444444448DDDD88888888888DDDDD477778888848
            00008DDDDDDDDDDDDDDD47FF9991AAA2EEE68DDD888F888F888F47FF9991AAA2
            EEE68DDD888F888F888F47FF9991AAA2EEE68DDD888F888F888F47FF9991AAA2
            EEE68DDD888F888F888F88889991AAA26666888D888F888FFFFFDDDD1111AAA2
            8888DDDDFFFF888FDDDDDDDDD888AAA28DDDDDDDDDDD888FDDDDDDDDDDDDAAA2
            8DDDDDDDDDDD888FDDDDDDDDDDDD22228DDDDDDDDDDDFFFFDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object evbtViewLog: TevBitBtn
          Left = 384
          Top = 8
          Width = 97
          Height = 25
          Caption = 'View Log'
          Enabled = False
          TabOrder = 3
          OnClick = evbtViewLogClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDD8888888888888888FFFFFFFFFFFFFFFF000000000000
            000088888888888888887787778777877787DD8DDD8DDD8DDD8DFF8FFF8FFF8F
            FF8FDD8DDD8DDD8DDD8DFF7FFF7FFF7FFF7FDDDDDDDDDDDDDDDDFFFFFFFFFFFF
            666FDDDDDDDDDDDD888DFFFFFFF666FF6F6FDDDDDDD888DD8D8DFFF066F6F6F6
            6F6FDDD8D8D8D8D88D8DFF707766F6F6FF66DDD8DD88D8D8DD88F800077FF666
            FFFFDD888FDDD888DDDDF0777077FF7FFF7FD88888FDDDDDDDDD707778077787
            7787D888888FDD8DDD8D00FFF88000000000D8DDD888F8888888DD07778808DD
            DDDDDD8888888FDDDDDDDDD07778808DDDDDDDD8888888FDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    AutoEdit = False
    DataSet = DM_TMP_CO.TMP_CO
    Left = 594
    Top = 2
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_E_D_GROUPS.CL_E_D_GROUPS
    MasterDataSource = nil
    Left = 542
    Top = 2
  end
  inherited wwdsList: TevDataSource
    Left = 494
    Top = 65530
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 554
    Top = 195
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 174
    Top = 5
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 318
    Top = 213
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 482
    Top = 216
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 54
    Top = 65533
  end
  object evdsEvoDD: TevDataSource
    Left = 302
    Top = 309
  end
  object sdlgSaveSetup: TSaveDialog
    DefaultExt = 'cds'
    FileName = 'ImportSetup'
    Filter = 'DataSet|*.cds'
    Left = 390
    Top = 200
  end
  object opdgSetupFile: TOpenDialog
    Filter = 'DataSet|*.cds'
    Left = 46
    Top = 208
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 630
    Top = 200
  end
end
