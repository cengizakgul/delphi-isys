// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_TEER_SELECTOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  EvUIComponents;

type
  TEDIT_TeerSelector = class(TForm)
    ComboBox1: TevComboBox;
    Label1: TevLabel;
    Button1: TevButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EDIT_TeerSelector: TEDIT_TeerSelector;

implementation

{$R *.DFM}

procedure TEDIT_TeerSelector.FormShow(Sender: TObject);
begin
  ComboBox1.ItemIndex := 0;
end;

procedure TEDIT_TeerSelector.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caHide;
end;

end.
