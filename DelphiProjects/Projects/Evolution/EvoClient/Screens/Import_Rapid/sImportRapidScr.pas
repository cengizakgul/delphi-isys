// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportRapidScr;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPackageEntry, ExtCtrls, Menus,  ImgList, EvCommonInterfaces,
  ComCtrls, ToolWin, StdCtrls, Buttons, ActnList, EvMainboard,
  ISBasicClasses, EvUIComponents, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TImportfrm = class(TFramePackageTmpl, IevImportRapidScreens)
  private
    { Private declarations }
  protected
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

var
  ImportFrm: TImportFrm;

function GetImportRapidScreens: IevImportRapidScreens;
begin
  if not Assigned(ImportFrm) then
    ImportFrm := TImportfrm.Create(nil);
  Result := ImportFrm;
end;


{ TImportfrm }

function TImportfrm.PackageCaption: string;
begin
  Result := '';
end;

function TImportfrm.PackageSortPosition: string;
begin
  Result := '190';
end;

procedure TImportfrm.UserPackageStructure;
begin
  AddStructure('\&Company\Imports|060');
  AddStructure('\&Company\Imports\Custom Imports|070', 'TEDIT_CO_EE_IMPORT');
  AddStructure('\&Company\Imports\Liabilites and HR|080', 'TEDIT_CO_IMPORT');
  AddStructure('\&Company\Imports\Rapid Import|140', 'TEDIT_IM_RAPID');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetImportRapidScreens, IevImportRapidScreens, 'Screens Import Rapid');

finalization
  FreeAndNil(ImportFrm );

end.
