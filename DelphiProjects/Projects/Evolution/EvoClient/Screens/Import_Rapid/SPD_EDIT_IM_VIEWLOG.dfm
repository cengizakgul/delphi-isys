object EDIT_IM_VIEWLOG: TEDIT_IM_VIEWLOG
  Left = 234
  Top = 187
  Width = 870
  Height = 640
  Caption = 'EDIT_IM_VIEWLOG'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object evDBGrid1: TevDBGrid
    Left = 8
    Top = 8
    Width = 849
    Height = 561
    Selected.Strings = (
      'CUSTOM_COMPANY_NUMBER'#9'20'#9'CUSTOM_COMPANY_NUMBER'
      'NAME'#9'30'#9'NAME'
      'CUSTOM_EMPLOYEE_NUMBER'#9'9'#9'CUSTOM_EMPLOYEE_NUMBER'
      'FNAME'#9'20'#9'FNAME'
      'LNAME'#9'30'#9'LNAME'
      'FIELD_NAME'#9'20'#9'FIELD_NAME'
      'REASON'#9'128'#9'REASON'
      'WHEN'#9'18'#9'WHEN')
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = evDataSource1
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    UseTFields = True
    IndicatorColor = icBlack
  end
  object Done: TevBitBtn
    Left = 768
    Top = 584
    Width = 75
    Height = 25
    TabOrder = 1
    Kind = bkClose
  end
  object Save: TevBitBtn
    Left = 16
    Top = 584
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 2
    OnClick = SaveClick
  end
  object evDataSource1: TevDataSource
    DataSet = DM_IM_RAPID.evcsErrorLog
    Left = 216
    Top = 32
  end
  object SaveDialog1: TSaveDialog
    Left = 416
    Top = 80
  end
end
