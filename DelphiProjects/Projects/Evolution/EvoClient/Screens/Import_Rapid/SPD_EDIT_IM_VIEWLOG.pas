// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_VIEWLOG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,  Grids, Wwdbigrd, Wwdbgrid, Db, Wwdatsrc,
  SDM_IM_RAPID, EvUIComponents;

type
  TEDIT_IM_VIEWLOG = class(TForm)
    evDBGrid1: TevDBGrid;
    Done: TevBitBtn;
    Save: TevBitBtn;
    evDataSource1: TevDataSource;
    SaveDialog1: TSaveDialog;
    procedure SaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EDIT_IM_VIEWLOG: TEDIT_IM_VIEWLOG;

implementation

{$R *.DFM}

procedure TEDIT_IM_VIEWLOG.SaveClick(Sender: TObject);
var
  SL: TStringList;
  cName, CNo, ImType, ENo, FName, LName, FN, Reason, When: String;
begin
  SL := TStringList.Create;
  with evDataSource1.DataSet do
  begin
    First;
    while not Eof do
    begin
      cName := FieldByName('NAME').AsString;
      CNo   := FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
      ENo   := FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString;
      FName := FieldByName('FNAME').AsString;
      LName := FieldByName('LNAME').AsString;
      FN    := FieldByName('FIELD_NAME').AsString;
      Reason:= FieldByName('REASON').AsString;
      When  := FieldByName('WHEN').AsString;
      ImType:= FieldByName('IMTYPE').AsString;
      SL.Add(cName+' '+CNo+' '+ENo+' '+FName+' '+LName+' '+FN+' '+Reason+' '+When);
      Next;
    end;
  end;
  if SaveDialog1.Execute then
    SL.SaveToFile(SaveDialog1.FileName);
end;

end.
