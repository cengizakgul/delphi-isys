// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CO_IMPORT;

interface

uses
  SDataStructure,  SPD_EDIT_CO_BASE, wwDialog, wwrcdvw,
  Dialogs, Db,  StdCtrls, Wwdatsrc, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Controls, Classes, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, EvSecElement, EvUtils, wwdblook, EvTypes,
  CheckLst, Variants, SRapidMaritalStatusImport, EvContext,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictsystem,
  SDataDictclient, SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  ImgList, LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel,
  Forms, LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CO_IMPORT = class(TEDIT_CO_BASE)
    TabSheet2: TTabSheet;
    bttnSetup: TevButton;
    wwcsCfgFile: TevClientDataSet;
    wwcsCfgFileDestination_Field: TStringField;
    wwcsCfgFileField_Position: TIntegerField;
    wwcsCfgFileField_Length: TIntegerField;
    wwcsCfgFileDestination_Table: TStringField;
    wwcsCfgFileInsert_Order: TIntegerField;
    wwcsCfgFileDefault_Value: TStringField;
    wwcsCfgFileProcedure: TStringField;
    OpenDialog1: TOpenDialog;
    cmbxChooseImportType: TevComboBox;
    wwcsYearToDate: TevClientDataSet;
    wwDataSource1: TevDataSource;
    wwcsRecurring: TevClientDataSet;
    evDBGrid1: TevDBGrid;
    evDBGrid2: TevDBGrid;
    wwdsPreview: TevDataSource;
    wwdsPreview2: TevDataSource;
    wwcsHistory1: TevClientDataSet;
    wwcsHistory2: TevClientDataSet;
    evCheckBox1: TevCheckBox;
    TabSheet3: TTabSheet;
    evMemo1: TevMemo;
    SaveDialog1: TSaveDialog;
    evcsDirectDeposit: TevClientDataSet;
    evcsTimeOff: TevClientDataSet;
    TabSheet4: TTabSheet;
    evDataSource1: TevDataSource;
    evDBGrid3: TevDBGrid;
    evDBGrid4: TevDBGrid;
    evDataSource2: TevDataSource;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    evComboBox1: TevComboBox;
    evcsMstatus: TevClientDataSet;
    evcsMstatusEE_NBR: TIntegerField;
    evcsMstatusSTATE: TStringField;
    evcsMstatusHRSTATUS: TStringField;
    evcsMstatusEVSTATUS: TStringField;
    evcsMstatusCO_STATES_NBR: TIntegerField;
    evcsEDMatch: TevClientDataSet;
    evcsEDMatchHR_E_D: TStringField;
    evcsEDMatchEV_E_D: TStringField;
    evcsEDMatchCL_E_DS_NBR: TIntegerField;
    wwcsLiabilities: TevClientDataSet;
    evButton4: TevButton;
    wwcsLiabilitiesCOMPANY_ID: TStringField;
    wwcsLiabilitiesCHECK_DATE: TDateTimeField;
    wwcsLiabilitiesDUE_DATE: TDateTimeField;
    wwcsLiabilitiesSTATE: TStringField;
    wwcsLiabilitiesLOCAL: TStringField;
    wwcsLiabilitiesAMOUNT1: TFloatField;
    wwcsLiabilitiesAMOUNT2: TFloatField;
    wwcsLiabilitiesAMOUNT3: TFloatField;
    wwcsLiabilitiesAMOUNT4: TFloatField;
    wwcsLiabilitiesAMOUNT5: TFloatField;
    wwcsLiabilitiesAMOUNT6: TFloatField;
    wwcsLiabilitiesAMOUNT7: TFloatField;
    wwcsLiabilitiesAMOUNT8: TFloatField;
    wwcsLiabilitiesAMOUNT9: TFloatField;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    wwcsLiabilitiesAMOUNT10: TFloatField;
    wwcsLiabilitiesDEP: TStringField;
    evEESUI: TevClientDataSet;
    evEESUISTATE: TStringField;
    evEESUIAMOUNT3: TStringField;
    evEESUIAMOUNT4: TStringField;
    evEESUIAMOUNT5: TStringField;
    evEESUISUI4: TIntegerField;
    evEESUISUI5: TIntegerField;
    evEESUISUI3: TIntegerField;
    evERSUI: TevClientDataSet;
    evERSUISTATE: TStringField;
    evERSUIAMOUNT8: TStringField;
    evERSUIAMOUNT9: TStringField;
    evERSUIAMOUNT10: TStringField;
    evERSUISUI8: TIntegerField;
    evERSUISUI9: TIntegerField;
    evERSUISUI10: TIntegerField;
    evLocals: TevClientDataSet;
    evLocalsSTATE: TStringField;
    evLocalsRapidLocal: TStringField;
    evLocalsEvoLocal: TStringField;
    evLocalsEvoLocalNbr: TIntegerField;
    evYTDLocals: TevClientDataSet;
    Label1: TLabel;
    DataSource1: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    evYTDLocalsSTATE: TStringField;
    evYTDLocalsRapidLocal: TStringField;
    evYTDLocalsEvoLocal: TStringField;
    evYTDLocalsEvoLocalNbr: TIntegerField;
    evcsEmployee: TevClientDataSet;
    bttnLoadFile: TevBitBtn;
    evButton1: TevBitBtn;
    evButton3: TevBitBtn;
    evButton2: TevBitBtn;
    wwcsLiabilitiesSTATUS: TStringField;
    wwcsLiabilitiesNOTES: TStringField;
    procedure bttnSetupClick(Sender: TObject);
    procedure bttnLoadFileClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure cmbxChooseImportTypeChange(Sender: TObject);
    procedure evButton1Click(Sender: TObject);
    procedure evButton2Click(Sender: TObject);
    procedure evButton3Click(Sender: TObject);
    procedure evComboBox1Change(Sender: TObject);
    procedure evButton4Click(Sender: TObject);
  private
    { Private declarations }
    HREDList: TStringList;
    AdjustmentType: String;
    LiabStatus: String;
    Impounded: String;
    UseDueDate: Boolean;
    DueDate: TDateTime;
    MyTrans: TTransactionManager;
    procedure PopulateEETables(aFieldNameList, aDataList: TStringList);
    procedure PopulateEEStateTables(aFieldNameList, aDataList: TStringList);
    function PopulateEERateTables(aFieldNameList, aDataList: TStringList): TStringList;
    procedure PopulateEEEDTables(aFieldNameList, aDataList: TStringList);
    function GetProperEEFieldName(aFieldName: String): TField;
    function GetProperEEStateFieldName(aFieldName: String): TField;
    function GetProperEERateFieldName(aFieldName: String): TField;
    function GetProperEDFieldName(aFieldName: String): TField;
    function PrepareData(aFieldName, aFieldValue, ImportType, aState, aMCode, Local, Dep: String): Variant;
    procedure OpenHRFiles;
    function GetHRFileName(ImportType: String): String;
    procedure FillLiab(aFileName: String);
    procedure AddFedLiability(TargetDS: TevClientDataSet; Amount: Double; TaxType: String);
    procedure AddStateLiability(TargetDS: TevClientDataSet; Amount: Double; TaxType: String);
    procedure AddSui3_5Liability(TargetDS: TevClientDataSet; Amount: Double);
    procedure AddSui7Liability(TargetDS: TevClientDataSet; Amount: Double);
    procedure AddSui8_10Liability(TargetDS: TevClientDataSet; Amount: Double);
    procedure AddLocalLiability(TargetDS: TevClientDataSet; Amount: Double);
    procedure AttachDeposit(TargetDS: TevClientDataSet; ALiabStatus: String);
  public
    ShowDBDTError: Boolean;
    NewFormat: Boolean;
  end;

var
  EDIT_CO_IMPORT: TEDIT_CO_IMPORT;

implementation

uses SPD_EDIT_IMPORT_SETUP, EvConsts, SysUtils, SImportConfigs;

{$R *.DFM}

procedure TEDIT_CO_IMPORT.bttnSetupClick(Sender: TObject);
var
  TImportSetup: TEDIT_IMPORT_SETUP;
  EeSuiMissing, ErSuiMissing: Boolean;
  EeStateName, EeAmntName, ErStateName, ErAmntName: String;
begin
  inherited;
  if cmbxChooseImportType.Text = 'Liabilities(Rapid)' then
  begin
    AdjustmentType := '';
    LiabStatus := '';
    UseDueDate := False;
    TImportSetup := TEDIT_IMPORT_SETUP.Create(Application);
    TImportSetup.NewFormat := NewFormat;
    TImportSetup.TabSheet1.TabVisible := False;
    TImportSetup.TabSheet2.TabVisible := False;
    TImportSetup.TabSheet3.TabVisible := False;
    TImportSetup.TabSheet4.TabVisible := False;
    TImportSetup.TabSheet4.TabVisible := False;
    TImportSetup.tbshMatching.TabVisible := False;
    TImportSetup.TabSheet6.TabVisible := False;
    TImportSetup.TabSheet5.TabVisible := True;
    TImportSetup.TabSheet7.TabVisible := False;
    TImportSetup.evLocals.Data := evLocals.Data;
    TImportSetup.ShowModal;

    if TImportSetup.evDBComboBox2.ItemIndex <> -1 then
      AdjustmentType := Copy(TImportSetup.evDBComboBox2.Items.Strings[TImportSetup.evDBComboBox2.ItemIndex], Pos(#9, TImportSetup.evDBComboBox2.Items.Strings[TImportSetup.evDBComboBox2.ItemIndex])+1, 1);

    if TImportSetup.evDBComboBox1.ItemIndex <> -1 then
      LiabStatus := Copy(TImportSetup.evDBComboBox1.Items.Strings[TImportSetup.evDBComboBox1.ItemIndex], Pos(#9, TImportSetup.evDBComboBox1.Items.Strings[TImportSetup.evDBComboBox1.ItemIndex])+1, 1);

    UseDueDate := TImportSetup.evCheckBox1.Checked;
    DueDate := TImportSetup.evDateTimePicker1.Date;

    if TImportSetup.evrgImpounded.ItemIndex = 0 then
      Impounded := GROUP_BOX_YES
    else
      Impounded := GROUP_BOX_NO;

    evLocals.Data := TImportSetup.evLocals.Data;
    TImportSetup.Free;

    TImportSetup := TEDIT_IMPORT_SETUP.Create(Application);
    TImportSetup.TabSheet1.TabVisible := False;
    TImportSetup.TabSheet2.TabVisible := False;
    TImportSetup.TabSheet3.TabVisible := False;
    TImportSetup.TabSheet4.TabVisible := False;
    TImportSetup.TabSheet4.TabVisible := False;
    TImportSetup.tbshMatching.TabVisible := False;
    TImportSetup.TabSheet5.TabVisible := false;
    TImportSetup.TabSheet6.TabVisible := True;
    TImportSetup.TabSheet7.TabVisible := False;
    TImportSetup.evEESUI.Data := evEESUI.Data;
    TImportSetup.evERSUI.Data := evERSUI.Data;
    TImportSetup.ShowModal;
    evEESUI.Data := TImportSetup.evEESUI.Data;
    evERSUI.Data := TImportSetup.evERSUI.Data;
    TImportSetup.Free;

    EeSuiMissing := False;
    ErSuiMissing := False;
    EeStateName := '';
    EeAmntName := '';
    ErStateName := '';
    ErAmntName := '';
    evEESUI.First;
    while not evEESUI.Eof do
    begin
      if (evEESUI.FieldByName('Sui3').IsNull) or (evEESUI.FieldByName('Sui4').IsNull) or (evEESUI.FieldByName('Sui5').IsNull) then
      begin
        EeStateName := evEESUI.FieldByName('State').AsString;
        if (evEESUI.FieldByName('Sui3').IsNull) and (evEESUI.FieldByName('Amount3').AsString <> 'N/A') then
        begin
          EeAmntName := 'Amount3';
          EeSuiMissing := True;
          Break;
          //use Amount3 in message
        end
        else if (evEESUI.FieldByName('Sui4').IsNull) and (evEESUI.FieldByName('Amount4').AsString <> 'N/A') then
        begin
          EeAmntName := 'Amount4';
          EeSuiMissing := True;
          Break;
          //use Amount4 in message
        end
        else if (evEESUI.FieldByName('Sui5').IsNull) and (evEESUI.FieldByName('Amount5').AsString <> 'N/A')  then
        begin
          EeAmntName := 'Amount5';
          //use Amount5 in message                                                                                    s1
          EeSuiMissing := True;
          Break;
        end;
      end;
//      Sui3, Sui4, Sui5
      evEESUI.Next;
    end;

    evERSUI.First;
    while not evERSUI.Eof do
    begin
      ErStateName := evERSUI.FieldByName('State').AsString;
      if (evERSUI.FieldByName('Sui8').IsNull) or (evERSUI.FieldByName('Sui9').IsNull) or (evERSUI.FieldByName('Sui10').IsNull) then
      begin
        if (evERSUI.FieldByName('Sui8').IsNull) and (evERSUI.FieldByName('Amount8').AsString <> 'N/A') then
        begin
          ErAmntName := 'Amount8';
          ErSuiMissing := True;
          Break;
          //use Amount8 in message
        end
        else if (evERSUI.FieldByName('Sui9').IsNull) and (evERSUI.FieldByName('Amount9').AsString <> 'N/A') then
        begin
          ErAmntName := 'Amount9';
          ErSuiMissing := True;
          Break;
          //use Amount9 in message
        end
        else if (evERSUI.FieldByName('Sui10').IsNull) and (evERSUI.FieldByName('Amount10').AsString <> 'N/A') then
        begin
          ErAmntName := 'Amount10';
          //use Amount10 in message
          ErSuiMissing := True;
          Break;
        end;
      end;
//      Sui8, Sui9, Sui10
      evERSUI.Next;
    end;

    if (not EeSuiMissing) and (not ErSuiMissing) then
      evButton4.Enabled := True
    else
    begin
      if EeSuiMissing then
        ShowMessage(EeAmntName+' cannot be used for '+EeStateName);
      if ErSuiMissing then
        ShowMessage(ErAmntName+' cannot be used for '+ErStateName);
    end;
  end;
end;

procedure TEDIT_CO_IMPORT.bttnLoadFileClick(Sender: TObject);
var
  ImportFileName1: String;
  ClientNumber, CompanyNumber: String;
begin
  inherited;
  NewFormat := False;

  ClientNumber := DM_CLIENT.CL.FieldByName('CUSTOM_CLIENT_NUMBER').AsString;
  CompanyNumber := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;

  if cmbxChooseImportType.Text = 'Liabilities(Rapid)' then
  begin
    OpenDialog1.Execute;
    ImportFileName1 := OpenDialog1.FileName;
    FillLiab(ImportFileName1);
    bttnSetup.Enabled := True;
    wwdsPreview.DataSet := wwcsLiabilities;
  end;

  bttnLoadFile.Enabled := True;
end;

procedure TEDIT_CO_IMPORT.TabSheet2Show(Sender: TObject);
begin
  inherited;
  bttnLoadFile.Enabled := True;
  bttnSetup.Enabled := False;
  evButton4.Enabled := False;
  cmbxChooseImportType.ItemIndex := 0;
end;

procedure TEDIT_CO_IMPORT.cmbxChooseImportTypeChange(Sender: TObject);
begin
  inherited;
  bttnLoadFile.Enabled := True;
end;

procedure TEDIT_CO_IMPORT.evButton1Click(Sender: TObject);
begin

  OpenHRFiles;
{  if evComboBox1.ItemIndex = 1 then
    evButton3.Enabled := True
  else}
    evButton2.Enabled := True;

end;

procedure TEDIT_CO_IMPORT.PopulateEETables(aFieldNameList,
  aDataList: TStringList);
var
  I: Integer;
  P: TField;
  S: String;
begin
  DM_EMPLOYEE.EE.Tag := 1;
  MyTrans := TTransactionManager.Create(Self);
  MyTrans.Initialize([DM_CLIENT.CL_PERSON, DM_EMPLOYEE.EE]);
  DM_CLIENT.CL_PERSON.DataRequired('ALL');
  DM_EMPLOYEE.EE.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');

  if not DM_CLIENT.CL_PERSON.Locate('SOCIAL_SECURITY_NUMBER', aDataList[0], []) then
    DM_CLIENT.CL_PERSON.Insert
  else
    DM_CLIENT.CL_PERSON.Edit;
  if not DM_EMPLOYEE.EE.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(aDataList[1], ' ', 9), []) then
  begin
    DM_EMPLOYEE.EE.Insert;
    ShowDBDTError := True;
  end
  else
  begin
    DM_EMPLOYEE.EE.Edit;
    ShowDBDTError := False;
  end;
  for I := 0 to aDataList.Count - 1 do
  begin
    P := DM_CLIENT.CL_PERSON.FindField(aFieldNameList.Strings[I]);
    S := aDataList.Strings[I];
    if (Assigned(P)) then
    begin
      if Pos('"', S) > 0 then
      begin
        Delete(S,1,1);
        Delete(S, Length(S), 1);
      end;
      S := PrepareData(aFieldNameList.Strings[I], S, 'CL_PERSON', '', '', '', '');
      if (S <> '') then
        P.Value := S;
    end
    else if not Assigned(P) then
    begin
      P := GetProperEEFieldName(aFieldNameList.Strings[I]);
      if (Assigned(P)) then
      begin
        if Pos('"', S) > 0 then
        begin
          Delete(S,1,1);
          Delete(S, Length(S), 1);
        end;
        S := PrepareData(aFieldNameList.Strings[I], S, 'EE', '', '', '', '');
        if S <> '' then
          P.Value := S;
      end;
    end;
  end;
  DM_CLIENT.CL_PERSON.Post;
  DM_EMPLOYEE.EE.Post;
    DM_EMPLOYEE.EE.Tag := 0;
end;

function TEDIT_CO_IMPORT.PrepareData(aFieldName,
  aFieldValue, ImportType, aState, aMCode, Local, Dep: String): Variant;
var
  V: Variant;
  Initial, temp: String;
  StatesNbr, iDep: Integer;
begin
  {The first Group is for CL_PERSON defaults or what not}
  result := '';
  if (aFieldName = 'GENDER') and (aFieldValue = '') then
  begin
    Result := GROUP_BOX_UNKOWN;
  end
  else if (aFieldName = 'ETHNICITY') and (aFieldValue = '') then
  begin
    Result := ETHNIC_NOT_APPLICATIVE;
  end
  else if (aFieldName = 'SMOKER') and (aFieldValue = '') then
  begin
    Result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'VETERAN') and (aFieldValue = '') then
  begin
    Result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'VIETNAM_VETERAN') and (aFieldValue = '') then
  begin
    Result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'DISABLED_VETERAN') and (aFieldValue = '') then
  begin
    Result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'MILITARY_RESERVE') and (aFieldValue = '') then
  begin
    Result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'I9_ON_FILE') and (aFieldValue = '') then
  begin
    Result := GROUP_BOX_NO;
  end {The Next Bunch is for Employee Defaults or what not}
  else if (aFieldName = 'CUSTOM_EMPLOYEE_NUMBER') then
  begin
    result := PadStringLeft(aFieldValue, ' ', 9)
  end
  else if (aFieldName = 'FIRST_NAME') then
  begin
    temp := CapitalizeWords(aFieldValue);
    if Pos(' ', temp) > 0 then
    begin
      Initial := Copy(temp, Pos(' ', temp) + 1, Length(temp));
      if Length(Initial) = 1 then
        System.delete(temp, Pos(' ', temp), 2)
      else
        Initial := '';
    end;
    if TevClientDataSet(DM_CLIENT.CL_PERSON).State in [dsInsert, dsEdit] then
      if Initial <> '' then
        DM_CLIENT.CL_PERSON.FieldByName('MIDDLE_INITIAL').AsString := Initial
      else
        DM_CLIENT.CL_PERSON.FieldByName('MIDDLE_INITIAL').Clear;
    Result := temp;
  end
  else if (aFieldName = 'LAST_NAME') then
  begin
    Result := CapitalizeWords(aFieldValue);
  end
  else if (aFieldName = 'DIVISION') then
  begin
    if DM_COMPANY.CO_DIVISION.RecordCount = 0 then
      Exit;
    if (aFieldValue = '') and (DM_COMPANY.CO_DIVISION.RecordCount = 1) then
      Result := DM_COMPANY.CO_DIVISION.FieldByName('CO_DIVISION_NBR').Value
    else if not DM_COMPANY.CO_DIVISION.Locate('CO_NBR;CUSTOM_DIVISION_NUMBER', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').Value, PadStringLeft(aFieldValue, ' ', 20)]), [loCaseInsensitive]) then
    begin
      if ShowDBDTError then
        EvMessage('Division: '+aFieldValue+' does not exist in evolution.');
      result := '';
    end
    else
      Result := DM_COMPANY.CO_DIVISION.FieldByName('CO_DIVISION_NBR').Value;
  end
  else if (aFieldName = 'BRANCH') then
  begin
    if DM_COMPANY.CO_BRANCH.RecordCount = 0 then
      Exit;
    if (aFieldValue = '') and (DM_COMPANY.CO_BRANCH.RecordCount = 1) then
      Result := DM_COMPANY.CO_BRANCH.FieldByName('CO_BRANCH_NBR').Value
    else if not DM_COMPANY.CO_BRANCH.Locate('CO_DIVISION_NBR;CUSTOM_BRANCH_NUMBER', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').Value, PadStringLeft(aFieldValue, ' ', 20)]), [loCaseInsensitive]) then
    begin
      if ShowDBDTError then
        EvMessage('Branch: '+aFieldValue+' does not exist in evolution.');
      result := '';
    end
    else
      Result := DM_COMPANY.CO_BRANCH.FieldByName('CO_BRANCH_NBR').Value;
  end
  else if (aFieldName = 'DEPARTMENT') then
  begin
    if DM_COMPANY.CO_DEPARTMENT.RecordCount = 0 then
      Exit;
    if (aFieldValue = '') and (DM_COMPANY.CO_DEPARTMENT.RecordCount = 1) then
      Result := DM_COMPANY.CO_DEPARTMENT.FieldByName('CO_DEPARTMENT_NBR').Value
    else if not DM_COMPANY.CO_DEPARTMENT.Locate('CO_BRANCH_NBR;CUSTOM_DEPARTMENT_NUMBER', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').Value, PadStringLeft(aFieldValue, ' ', 20)]), [loCaseInsensitive]) then
    begin
      if ShowDBDTError then
        EvMessage('Department: '+aFieldValue+' does not exist in evolution.');
      result := '';
    end
    else
      Result := DM_COMPANY.CO_DEPARTMENT.FieldByName('CO_DEPARTMENT_NBR').Value;
  end
  else if (aFieldName = 'TEAM') then
  begin
    if DM_COMPANY.CO_TEAM.RecordCount = 0 then
      Exit;

    if (aFieldValue = '') and (DM_COMPANY.CO_TEAM.RecordCount = 1) then
      Result := DM_COMPANY.CO_TEAM.FieldByName('CO_TEAM_NBR').Value
    else if not DM_COMPANY.CO_TEAM.Locate('CO_DEPARTMENT_NBR;CUSTOM_TEAM_NUMBER', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').Value, PadStringLeft(aFieldValue, ' ', 20)]), [loCaseInsensitive]) then
    begin
      if ShowDBDTError then
        EvMessage('Team: '+aFieldValue+' does not exist in evolution.');
      result := '';
    end
    else
      Result := DM_COMPANY.CO_TEAM.FieldByName('CO_TEAM_NBR').Value;
  end
  else if (aFieldName = 'NEW_HIRE_REPORT_SENT') and (aFieldValue = '') then
  begin
    Result := NEW_HIRE_REPORT_PENDING;
  end
  else if (aFieldName = 'CURRENT_TERMINATION_CODE') and (aFieldValue = '') then
  begin
    Result := TERMINATION_ACTIVE;
  end
  else if (aFieldName = 'TIPPED_DIRECTLY') and (aFieldValue = '') then
  begin
    Result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'DISTRIBUTE_TAXES') and (aFieldValue = '') then
  begin
    if DM_COMPANY.CO.FieldByName('DISTRIBUTE_DEDUCTIONS_DEFAULT').AsString = 'Y' then
      Result := 'B'
    else
      Result := DM_COMPANY.CO.FieldByName('DISTRIBUTE_DEDUCTIONS_DEFAULT').AsString;
  end
  else if (aFieldName = 'EE_NBR') then
  begin
    Result := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
  end
  else if (aFieldName = 'STATE') and ((ImportType = 'EE') or (ImportType = 'EE_STATES')) then
  begin
    Result := DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').AsInteger;
    DM_EMPLOYEE.EE.Edit;
    DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsInteger := DM_EMPLOYEE.EE_STATES.FieldByName('EE_STATES_NBR').AsInteger;
    DM_EMPLOYEE.EE.Post;
  end
  else if (aFieldName = 'SDI_STATE') and (aFieldValue <> '') then
  begin
    V := DM_COMPANY.CO_STATES.Lookup('STATE', aFieldValue, 'CO_STATES_NBR');
    if NOT VARISNULL(V) then
      Result := V;
  end
  else if (aFieldName = 'SUI_STATE') and (aFieldValue <> '') then
  begin
    V := DM_COMPANY.CO_STATES.Lookup('STATE', aFieldValue, 'CO_STATES_NBR');
    if NOT VARISNULL(V) then
      Result := V;
  end
  else if (aFieldName = 'RECIPROCAL_STATE') and (aFieldValue <> '') then
  begin
    V := DM_COMPANY.CO_STATES.Lookup('STATE', aFieldValue, 'CO_STATES_NBR');
    if NOT VARISNULL(V) then
      Result := V;
  end
  else if (aFieldName = 'RECIPROCAL_METHOD') and (aFieldValue = '') then
  begin
    Result := STATE_RECIPROCAL_TYPE_NONE;
  end
  else if (aFieldName = 'JOB') and (aFieldValue <> '')  then
  begin
    V := DM_COMPANY.CO_JOBS.Lookup('CO_NBR;DESCRIPTION', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').Value, aFieldValue]), 'CO_JOBS_NBR');
    if NOT VARISNULL(V) then
      result := V;
  end
  else if (aFieldName = 'WORKERS_COMP_CODE') and (aFieldValue <> '')  then
  begin
    DM_EMPLOYEE.EE_STATES.DataRequired('ee_states_nbr='+DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsString);
    if DM_EMPLOYEE.EE_STATES.RecordCount > 0 then
    begin
      StatesNbr := DM_EMPLOYEE.EE_STATES.FieldByName('CO_STATES_NBR').AsInteger;
      DM_COMPANY.CO_STATES.DataRequired('co_states_nbr='+IntToStr(StatesNbr));
      temp := DM_COMPANY.CO_STATES.FieldByName('STATE').AsString;
      V := DM_COMPANY.CO_WORKERS_COMP.Lookup('CO_NBR;WORKERS_COMP_CODE;CO_STATES_NBR', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').Value, aFieldValue, StatesNbr]), 'CO_WORKERS_COMP_NBR');
      if not VarIsNull(V) then
        Result := V
      else
        Result := 'Error: The workers comp code '+aFieldValue+' was not found for the state '+Temp+'.';
    end;
  end
  else if (aFieldName = 'ED_CODE') and (aFieldValue <> '') then
  begin
    V := evcsEDMatch.LookUp('HR_E_D', aFieldValue, 'CL_E_DS_NBR');
    if not VarIsNull(V) then
      result  := V;
  end
  else if (aFieldName = 'FREQUENCY') and (aFieldValue = '') then
  begin
    result := SCHED_FREQ_DAILY;
  end
  else if (aFieldName = 'EXCLUDE_WEEK_1') and (aFieldValue = '') then
  begin
    result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'EXCLUDE_WEEK_2') and (aFieldValue = '') then
  begin
    result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'EXCLUDE_WEEK_3') and (aFieldValue = '') then
  begin
    result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'EXCLUDE_WEEK_4') and (aFieldValue = '') then
  begin
    result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'EXCLUDE_WEEK_5') and (aFieldValue = '') then
  begin
    result := GROUP_BOX_NO;
  end
  else if (aFieldName = 'STATE_MARITAL_STATUS') and (aFieldValue <> '') then
  begin
    if Dep = '' then
      iDep := 0
    else
      iDep := StrToInt(Dep);
    result := FindMaritalStatus(aState, aFieldValue, aMCode, Local, iDep);
  end
  else if (aFieldName = 'STATE') and (ImportType = 'CL_PERSON') then
  begin
    V := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', aFieldValue, 'SY_STATES_NBR');
    if not VarIsNull(V) then
      DM_CLIENT.CL_PERSON['RESIDENTIAL_STATE_NBR'] := V;
    result := aFieldValue;
  end
  else
    result := aFieldValue;

end;

function TEDIT_CO_IMPORT.GetProperEEFieldName(aFieldName: String): TField;
begin
  if aFieldName = 'DIVISION' then
  begin
    Result := DM_EMPLOYEE.EE.FindField('CO_DIVISION_NBR');
  end
  else if aFieldName = 'BRANCH' then
  begin
    Result := DM_EMPLOYEE.EE.FindField('CO_BRANCH_NBR');
  end
  else if aFieldName = 'DEPARTMENT' then
  begin
    Result := DM_EMPLOYEE.EE.FindField('CO_DEPARTMENT_NBR');
  end
  else if aFieldName = 'TEAM' then
  begin
    Result := DM_EMPLOYEE.EE.FindField('CO_TEAM_NBR');
  end
  else
    Result := DM_EMPLOYEE.EE.FindField(aFieldName);
end;

procedure TEDIT_CO_IMPORT.evButton2Click(Sender: TObject);
begin
  inherited;
  try
    ctx_StartWait;
    try
      MyTrans.ApplyUpdates;
      EvMessage('The data has been committed!');
    except
      MyTrans.CancelUpdates;
      Raise;
    end;
  finally
    MyTrans.Free;
    ctx_EndWait;
    evButton2.Enabled := False;
  end;
end;

procedure TEDIT_CO_IMPORT.OpenHRFiles;
var
  I: Integer;
  ImportFile, FieldNameList, DataList: TStringList;
  TImportSetup: TEDIT_IMPORT_SETUP;
  sDir: String;
  ErrorList: TStringList;
begin
  inherited;
  ctx_StartWait;
  try
    ctx_DataAccess.Importing := True;
    ImportFile := TStringList.Create;
    FieldNameList := TStringList.Create;
    ErrorList := TStringList.Create;
    DataList := TStringList.Create;
    if evComboBox1.ItemIndex = 0 then
    begin
      DM_CLIENT.CL_PERSON.DisableControls;
      DM_EMPLOYEE.EE.DisableControls;
      evDataSource1.DataSet := DM_CLIENT.CL_PERSON;
      evDataSource2.DataSet := DM_EMPLOYEE.EE;
      evDataSource2.MasterDataSource := evDataSource1;
      ImportFile.LoadFromFile(GetHRFileName('Empl.csv'));
      EvMessage('Please remember all employees must have DBDT setup.');
      FieldNameList.CommaText := ImportFile.Strings[0];

      for I := 1 to ImportFile.Count - 1 do
      begin
        DataList.Clear;
        DataList.CommaText := ImportFile.Strings[I];
        if FieldNameList.Count = DataList.Count then
          PopulateEETables(FieldNameList, DataList)
        else
          EvMessage('The number of data fields does not match the correct number of fields ');
      end;
      DM_CLIENT.CL_PERSON.EnableControls;
      DM_EMPLOYEE.EE.EnableControls;
    end
    else if evComboBox1.ItemIndex = 1 then
    begin
      DM_EMPLOYEE.EE_STATES.DisableControls;
//      evcsMstatus.CreateDataSet;
      evDataSource1.DataSet := DM_EMPLOYEE.EE_STATES;
      ImportFile.LoadFromFile(GetHRFileName('EE States.csv'));
      FieldNameList.CommaText := ImportFile[0];
      for I := 1 to ImportFile.Count - 1 do
      begin
        DataList.Clear;
        DataList.CommaText := ImportFile[I];
        if FieldNameList.Count = DataList.Count then
          PopulateEEStateTables(FieldNameList, DataList)
        else
          EvMessage('The number of data fields does not match the correct number of fields ');
      end;
      DM_EMPLOYEE.EE_STATES.EnableControls;
    end
    else if evComboBox1.ItemIndex = 2 then
    begin
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.Tag := 1;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.DisableControls;
      evDataSource1.DataSet := DM_EMPLOYEE.EE_SCHEDULED_E_DS;
      ImportFile.LoadFromFile(GetHRFileName('EE Scheduled E Ds.csv'));
      FieldNameList.CommaText := ImportFile[0];

      HREDList := TStringList.Create;
      HREDList.Sorted := True;
      HREDList.Duplicates := dupIgnore;

      TImportSetup := TEDIT_IMPORT_SETUP.Create(Self);
      TImportSetup.TabSheet1.TabVisible := False;
      TImportSetup.TabSheet2.TabVisible := False;
      TImportSetup.TabSheet3.TabVisible := False;
      TImportSetup.tbshMatching.TabVisible := False;
      TImportSetup.PageControl1.ActivePage := TabSheet4;
      TImportSetup.evClientDataSet2.CreateDataSet;
      TImportSetup.evClientDataSet1.CreateDataSet;

      for I := 1 to ImportFile.Count - 1 do
      begin
        DataList.Clear;
        DataList.CommaText := ImportFile[I];
        if FieldNameList.Count = DataList.Count then
        begin
          if not TImportSetup.evClientDataSet2.Locate('HR_E_D', DataList[2], []) then
          begin
            TImportSetup.evClientDataSet2.Insert;
            TImportSetup.evClientDataSet2.FieldByName('HR_E_D').AsString := DataList[2];
            TImportSetup.evClientDataSet2.FieldByName('HR_DESCRIPTION').AsString := DataList[3];
            TImportSetup.evClientDataSet2.Post;
          end;
        end
        else
          EvMessage('The number of data fields does not match the correct number of fields ');
      end;

      TImportSetup.evDBGrid1.SetControlType('EV_E_D',  fctCustom, 'evDBLookupCombo2');
      TImportSetup.ShowModal;
      evcsEDMatch.Data := TImportSetup.evClientDataSet2.Data;
      TImportSetup.Free;


      for I := 1 to ImportFile.Count - 1 do
      begin
        DataList.Clear;
        DataList.CommaText := ImportFile[I];
        if FieldNameList.Count = DataList.Count then
          PopulateEEEDTables(FieldNameList, DataList)
        else
          EvMessage('The number of data fields does not match the correct number of fields ');
      end;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.EnableControls;
    end
    else if evComboBox1.ItemIndex = 3 then
    begin
      DM_EMPLOYEE.EE_RATES.DisableControls;
      evDataSource1.DataSet := DM_EMPLOYEE.EE_RATES;
      sDir := GetHRFileName('EE Rates.csv');
      ImportFile.LoadFromFile(sDir);
      EvMessage('Please remember all employees must have DBDT setup.');
      FieldNameList.CommaText := ImportFile[0];
      for I := 1 to ImportFile.Count - 1 do
      begin
        DataList.Clear;
        DataList.CommaText := ImportFile[I];
        if FieldNameList.Count = DataList.Count then
          ErrorList.AddStrings(PopulateEERateTables(FieldNameList, DataList))
        else
          EvMessage('The number of data fields does not match the correct number of fields ');
      end;
      if ErrorList.Count > 0 then
      begin
        ErrorList.SaveToFile(ExtractFilePath(sDir)+'\eerates.log');
        EvMessage('A log file named eerates.log has be created in '+ExtractFilePath(sDir));
      end;
      DM_EMPLOYEE.EE_SCHEDULED_E_DS.Tag := 0;
      DM_EMPLOYEE.EE_RATES.EnableControls;
    end;
  finally
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.EnableControls;
    DM_EMPLOYEE.EE_STATES.EnableControls;
    DM_CLIENT.CL_PERSON.EnableControls;
    DM_EMPLOYEE.EE.EnableControls;
    ctx_EndWait;
    ctx_DataAccess.Importing := False;
  end;
end;

procedure TEDIT_CO_IMPORT.evButton3Click(Sender: TObject);
var
  TImportSetup: TEDIT_IMPORT_SETUP;
begin
  inherited;
  TImportSetup := TEDIT_IMPORT_SETUP.Create(Self);
  TImportSetup.TabSheet1.TabVisible := False;
  TImportSetup.TabSheet2.TabVisible := False;
  TImportSetup.TabSheet4.TabVisible := False;
  TImportSetup.tbshMatching.TabVisible := False;
  TImportSetup.PageControl1.ActivePage := TabSheet3;
  TImportSetup.evClientDataSet1.Data := evcsMstatus.Data;
  TImportSetup.evDBGrid5.SetControlType('EVSTATUS',  fctCustom, 'evDBLookupCombo1');
  TImportSetup.ShowModal;
  TImportSetup.evClientDataSet1.DisableControls;
  DM_EMPLOYEE.EE_STATES.DisableControls;
  TImportSetup.evClientDataSet1.First;
  while not TImportSetup.evClientDataSet1.Eof do
  begin
    if TImportSetup.evClientDataSet1.FieldByName('EVSTATUS').AsString <> '' then
    begin
      if DM_EMPLOYEE.EE_STATES.Locate('EE_NBR', TImportSetup.evClientDataSet1.FieldByName('EE_NBR').Value, []) then
      begin
        DM_EMPLOYEE.EE_STATES.Edit;
        DM_EMPLOYEE.EE_STATES.FieldByName('STATE_MARITAL_STATUS').AsString := TImportSetup.evClientDataSet1.FieldByName('EVSTATUS').AsString;
        DM_EMPLOYEE.EE_STATES.Post;
      end;
    end;
    TImportSetup.evClientDataSet1.Next;
  end;
  TImportSetup.evClientDataSet1.EnableControls;
  TImportSetup.Free;
  DM_EMPLOYEE.EE_STATES.EnableControls;
  evButton2.Enabled := True;
end;

procedure TEDIT_CO_IMPORT.evComboBox1Change(Sender: TObject);
begin
  inherited;
  evButton2.Enabled := False;
  evButton3.Enabled := False;
  evButton1.Enabled := (evComboBox1.ItemIndex in [0,1,2,3]);

end;

procedure TEDIT_CO_IMPORT.PopulateEEStateTables(aFieldNameList,
  aDataList: TStringList);
var
  I, EENbr, COStateNbr: Integer;
  P: TField;
  S, sLocal, sDep, sMaritalStatus, sExCode, sState: String;
begin
  MyTrans := TTransactionManager.Create(Self);
  MyTrans.Initialize([DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE]);
  DM_EMPLOYEE.EE.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_STATES.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_EMPLOYEE.EE_STATES.DataRequired('ALL');

  if (not DM_EMPLOYEE.EE.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(aDataList[0], ' ', 9), [])) then
    Exit
  else
    EENbr := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
  if not DM_COMPANY.CO_STATES.Locate('STATE', aDataList[1], []) then
    Exit
  else
  begin
    COStateNbr := DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').AsInteger;
{    if not evcsMstatus.Active then
      evcsMstatus.Active := True;

    evcsMstatus.Insert;
    evcsMstatusEE_NBR.AsInteger := EENbr;
    evcsMstatusCO_STATES_NBR.AsInteger := COStateNbr;
    evcsMstatusSTATE.AsString := aDataList[1];
    evcsMstatusHRSTATUS.AsString := aDataList[2];
    evcsMstatus.Post;
}
   end;
  if not DM_EMPLOYEE.EE_STATES.Locate('EE_NBR;CO_STATES_NBR', VarArrayOf([EENbr, COStateNbr]), []) then
    DM_EMPLOYEE.EE_STATES.Insert
  else
    DM_EMPLOYEE.EE_STATES.Edit;
  for I := 0 to aDataList.Count - 1 do
  begin
    P := GetProperEEStateFieldName(aFieldNameList.Strings[I]);
    S := aDataList.Strings[I];
    if Assigned(P) then
    begin
      if Pos('"', S) > 0 then
      begin
        Delete(S,1,1);
        Delete(S, Length(S), 1);
      end;
      sState := aDataList.Strings[1];
      sMaritalStatus := aDataList.Strings[2];
      sDep := aDataList.Strings[3];
      sExCode := aDataList.Strings[18];
      sLocal := aDataList.Strings[19];
      S := PrepareData(aFieldNameList.Strings[I], S, 'EE_STATES', sState, sExCode, sLocal, sDep);
      if (S <> '') then
        P.Value := S;
    end;
  end;

end;

function TEDIT_CO_IMPORT.GetProperEEStateFieldName(
  aFieldName: String): TField;
begin
  if aFieldName = 'STATE' then
    Result := DM_EMPLOYEE.EE_STATES.FindField('CO_STATES_NBR')
  else if aFieldName = 'SDI_STATE' then
    Result := DM_EMPLOYEE.EE_STATES.FindField('SDI_APPLY_CO_STATES_NBR')
  else if aFieldName = 'SUI_STATE' then
    Result := DM_EMPLOYEE.EE_STATES.FindField('SUI_APPLY_CO_STATES_NBR')
  else if aFieldName = 'RECIPROCAL_STATE' then
    Result := DM_EMPLOYEE.EE_STATES.FindField('RECIPROCAL_CO_STATES_NBR')
  else
    Result := DM_EMPLOYEE.EE_STATES.FindField(aFieldName);
end;

function TEDIT_CO_IMPORT.PopulateEERateTables(aFieldNameList,
  aDataList: TStringList): TStringList;
var
  I, EENbr: Integer;
  P: TField;
  S: String;
  aState: String;
//  ErrorList: TStringList;
begin
  Result := TStringList.Create;
  MyTrans := TTransactionManager.Create(Self);
  MyTrans.Initialize([DM_EMPLOYEE.EE_RATES]);
  DM_EMPLOYEE.EE.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_EMPLOYEE.EE_RATES.DataRequired('ALL');
  DM_COMPANY.CO_DIVISION.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_BRANCH.DataRequired('ALL');
  DM_COMPANY.CO_DEPARTMENT.DataRequired('ALL');
  DM_COMPANY.CO_TEAM.DataRequired('ALL');
  DM_COMPANY.CO_JOBS.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_COMPANY.CO_WORKERS_COMP.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  if (not DM_EMPLOYEE.EE.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(aDataList[0], ' ', 9), [])) then
  begin
    Exit;
  end
  else
  begin
    EENbr := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
  end;
  if not DM_EMPLOYEE.EE_RATES.Locate('EE_NBR;RATE_NUMBER', VarArrayOf([EENbr, aDataList[5]]), []) then
  begin
    DM_EMPLOYEE.EE_RATES.Insert;
    ShowDBDTError := True;
  end
  else
  begin
    DM_EMPLOYEE.EE_RATES.Edit;
    ShowDBDTError := False;
  end;
  for I := 0 to aDataList.Count - 1 do
  begin
    P := GetProperEERateFieldName(aFieldNameList.Strings[I]);
    S := aDataList.Strings[I];
    if Assigned(P) then
    begin
      if Pos('"', S) > 0 then
      begin
        Delete(S,1,1);
        Delete(S, Length(S), 1);
      end;
      if aDataList.Count = 11 then
        aState := aDataList.Strings[10];
      S := PrepareData(aFieldNameList.Strings[I], S, 'EE_RATES', aState, '', '', '');
      if Pos('Error:', S) > 0 then
      begin
        Result.Add(S);
      end
      else if (S <> '') then
        P.Value := S;
    end;
  end;
end;

function TEDIT_CO_IMPORT.GetProperEERateFieldName(
  aFieldName: String): TField;
begin
  if aFieldName = 'DIVISION' then
    Result := DM_EMPLOYEE.EE_RATES.FindField('CO_DIVISION_NBR')
  else if aFieldName = 'BRANCH' then
    Result := DM_EMPLOYEE.EE_RATES.FindField('CO_BRANCH_NBR')
  else if aFieldName = 'DEPARTMENT' then
    Result := DM_EMPLOYEE.EE_RATES.FindField('CO_DEPARTMENT_NBR')
  else if aFieldName = 'TEAM' then
    Result := DM_EMPLOYEE.EE_RATES.FindField('CO_TEAM_NBR')
  else if aFieldName = 'JOB' then
    Result := DM_EMPLOYEE.EE_RATES.FindField('CO_JOB_NBR')
  else  if aFieldName = 'WORKERS_COMP_CODE' then
    Result := DM_EMPLOYEE.EE_RATES.FindField('CO_WORKERS_COMP_NBR')
  else
    Result := DM_EMPLOYEE.EE_RATES.FindField(aFieldName);
end;

procedure TEDIT_CO_IMPORT.PopulateEEEDTables(aFieldNameList,
  aDataList: TStringList);
var
  I, EENbr, CLEDNbr: Integer;
  P: TField;
  S: String;
begin
  MyTrans := TTransactionManager.Create(Self);
  MyTrans.Initialize([DM_EMPLOYEE.EE_SCHEDULED_E_DS]);
  DM_EMPLOYEE.EE.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired('ALL');

  if (not DM_EMPLOYEE.EE.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(aDataList[0], ' ', 9), [])) then
  begin
    Exit;
  end
  else
  begin
    EENbr := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
  end;
  CLEDNbr := evcsEDMatch.Lookup('HR_E_D', aDataList[2], 'CL_E_DS_NBR');
  if not DM_EMPLOYEE.EE_SCHEDULED_E_DS.Locate('EE_NBR;CL_E_DS_NBR', VarArrayOf([EENbr, CLEDNbr]), []) then
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.Insert
  else
    DM_EMPLOYEE.EE_SCHEDULED_E_DS.Edit;
  for I := 0 to aDataList.Count - 1 do
  begin
    P := GetProperEDFieldName(aFieldNameList.Strings[I]);
    S := aDataList.Strings[I];
    if Assigned(P) then
    begin
      if Pos('"', S) > 0 then
      begin
        Delete(S,1,1);
        Delete(S, Length(S), 1);
      end;
      S := PrepareData(aFieldNameList.Strings[I], S, 'EE_SCHEDULED_E_DS', '', '', '', '');
      if (S <> '') then
        P.Value := S;
    end;
  end;
end;

function TEDIT_CO_IMPORT.GetHRFileName(ImportType: String): String;
var
  Dir: String;
  CoNumber: String;
begin

  CoNumber := DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
  Dir := RunFolderDialog('Please, select the path to import files:', Dir);
  result:= Dir+'\'+CoNumber+ImportType;
end;


function TEDIT_CO_IMPORT.GetProperEDFieldName(aFieldName: String): TField;
begin
  if aFieldName = 'AGENCY' then
    Result := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FindField('CL_AGENCY_NBR')
  else if aFieldName = 'ED_CODE' then
    Result := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FindField('CL_E_DS_NBR')
  else
    Result := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FindField(aFieldName);
end;

procedure TEDIT_CO_IMPORT.evButton4Click(Sender: TObject);
var
  MyTransactionManager: TTransactionManager;
  MyCount: Integer;
begin
  inherited;
  with DM_COMPANY, wwcsLiabilities do
  begin
    CO_FED_TAX_LIABILITIES.DataRequired('CO_NBR='+CO.FieldByName('CO_NBR').AsString);
    CO_TAX_DEPOSITS.DataRequired('CO_NBR='+CO.FieldByName('CO_NBR').AsString);
    CO_LOCAL_TAX_LIABILITIES.DataRequired('CO_NBR='+CO.FieldByName('CO_NBR').AsString);
    CO_STATES.DataRequired('CO_NBR='+CO.FieldByName('CO_NBR').AsString);
    CO_LOCAL_TAX.DataRequired('CO_NBR='+CO.FieldByName('CO_NBR').AsString);
    CO_STATE_TAX_LIABILITIES.DataRequired('CO_NBR='+CO.FieldByName('CO_NBR').AsString);
    CO_SUI_LIABILITIES.DataRequired('CO_NBR='+CO.FieldByName('CO_NBR').AsString);

    MyTransactionManager := TTransactionManager.Create(Application);

    MyTransactionManager.Initialize([CO_TAX_DEPOSITS, CO_FED_TAX_LIABILITIES, CO_LOCAL_TAX_LIABILITIES, CO_STATE_TAX_LIABILITIES, CO_SUI_LIABILITIES]);
    DisableControls;
    First;
    MyCount := 1;
    ctx_StartWait('Please wait while tax liabilities are being Imported into Evolution....', wwcsLiabilities.RecordCount);
    ctx_UpdateWait('', MyCount);
    while not Eof do
    begin
      if FieldByName('STATE').AsString = '' then
      begin
        AddFedLiability(CO_FED_TAX_LIABILITIES, FieldByName('AMOUNT1').AsFloat, TAX_LIABILITY_TYPE_FEDERAL);
        AddFedLiability(CO_FED_TAX_LIABILITIES, FieldByName('AMOUNT2').AsFloat, TAX_LIABILITY_TYPE_EE_OASDI);
        AddFedLiability(CO_FED_TAX_LIABILITIES, FieldByName('AMOUNT3').AsFloat, TAX_LIABILITY_TYPE_EE_MEDICARE);
        AddFedLiability(CO_FED_TAX_LIABILITIES, FieldByName('AMOUNT4').AsFloat, TAX_LIABILITY_TYPE_EE_EIC);
        AddFedLiability(CO_FED_TAX_LIABILITIES, FieldByName('AMOUNT5').AsFloat, TAX_LIABILITY_TYPE_EE_BACKUP_WITHHOLDING);
        AddFedLiability(CO_FED_TAX_LIABILITIES, FieldByName('AMOUNT6').AsFloat, TAX_LIABILITY_TYPE_ER_OASDI);
        AddFedLiability(CO_FED_TAX_LIABILITIES, FieldByName('AMOUNT7').AsFloat, TAX_LIABILITY_TYPE_ER_MEDICARE);
        AddFedLiability(CO_FED_TAX_LIABILITIES, FieldByName('AMOUNT8').AsFloat, TAX_LIABILITY_TYPE_ER_FUI);
      end;

      if (FieldByName('STATE').AsString <> '') and (FieldByName('LOCAL').AsString = '') then
      begin
        AddStateLiability(CO_STATE_TAX_LIABILITIES, FieldByName('AMOUNT1').AsFloat, TAX_LIABILITY_TYPE_STATE);
        AddStateLiability(CO_STATE_TAX_LIABILITIES, FieldByName('AMOUNT2').AsFloat, TAX_LIABILITY_TYPE_EE_SDI);
        AddStateLiability(CO_STATE_TAX_LIABILITIES, FieldByName('AMOUNT6').AsFloat, TAX_LIABILITY_TYPE_ER_SDI);
        AddSui7Liability(CO_SUI_LIABILITIES, FieldByName('AMOUNT7').AsFloat);
        if (FieldByName('AMOUNT3').AsFloat <> 0) or (FieldByName('AMOUNT4').AsFloat <> 0) or (FieldByName('AMOUNT5').AsFloat <> 0) then
        begin
          AddSui3_5Liability(CO_SUI_LIABILITIES, FieldByName('AMOUNT3').AsFloat);
          AddSui3_5Liability(CO_SUI_LIABILITIES, FieldByName('AMOUNT4').AsFloat);
          AddSui3_5Liability(CO_SUI_LIABILITIES, FieldByName('AMOUNT5').AsFloat);
        end;
        if (FieldByName('AMOUNT8').AsFloat <> 0) or (FieldByName('AMOUNT9').AsFloat <> 0) or (FieldByName('AMOUNT10').AsFloat <> 0) then
        begin
          AddSui8_10Liability(CO_SUI_LIABILITIES, FieldByName('AMOUNT8').AsFloat);
          AddSui8_10Liability(CO_SUI_LIABILITIES, FieldByName('AMOUNT9').AsFloat);
          AddSui8_10Liability(CO_SUI_LIABILITIES, FieldByName('AMOUNT10').AsFloat);
        end;
      end;

     if (FieldByName('STATE').AsString <> '') and (FieldByName('LOCAL').AsString <> '') and (FieldByName('AMOUNT1').AsFloat <> 0) then
     begin
       AddLocalLiability(CO_LOCAL_TAX_LIABILITIES, FieldByName('AMOUNT1').AsFloat);
     end;
     Inc(MyCount);
     ctx_UpdateWait('', MyCount);
     Next;
   end;
  end;

  try
    MyTransactionManager.ApplyUpdates;
  finally
    MyTransactionManager.Free;
    wwcsLiabilities.EnableControls;
    ctx_EndWait;
  end;

end;

procedure TEDIT_CO_IMPORT.FillLiab(aFileName: String);
var
  aStringList1, aStringList2: TStringList;
  I, X: Integer;
begin
  aStringList1 := TStringList.Create;
  aStringList2 := TStringList.Create;
  aStringList1.LoadFromFile(aFileName);
  wwcsLiabilities.CreateDataSet;
  evEESUI.CreateDataSet;
  evERSUI.CreateDataSet;
  evLocals.CreateDataSet;
  for I := 0 to aStringList1.Count - 1 do
  begin
    aStringList2.CommaText := aStringList1[I];
    NewFormat := (aStringList2.Count = 18);
    if aStringList2.Count > 0 then
    begin
      if (aStringList2[1] = Trim(DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString)) then
      begin
        wwcsLiabilities.Insert;
        for X := 0  to aStringList2.Count - 1 do
        begin
          if aStringList2[X] <> '' then
            if aStringList2[X] <> '  /  /  ' then
              wwcsLiabilities.Fields.Fields[X].Value := aStringList2[X];
        end;
        if (aStringList2[3] <> '') and (aStringList2[4] = '') then
            if (aStringList2[8] <> '0.00') or (aStringList2[9] <> '0.00') or (aStringList2[10] <> '0.00') then
              if not evEESUI.Locate('STATE', aStringList2[3], []) then
              begin
                evEESUI.Insert;
                if aStringList2[8] = '0.00' then
                  evEESUI.FieldByName('Amount3').Value := 'N/A';
                if aStringList2[9] = '0.00' then
                  evEESUI.FieldByName('Amount4').Value := 'N/A';
                if aStringList2[10] = '0.00' then
                  evEESUI.FieldByName('Amount5').Value := 'N/A';
                evEESUI.FieldByName('STATE').AsString := aStringList2[3];
                evEESUI.Post;
              end;
        if (aStringList2[3] <> '') and (aStringList2[4] = '') then
            if (aStringList2[13] <> '0.00') or (aStringList2[14] <> '0.00') or (aStringList2[15] <> '0.00') then
              if not evERSUI.Locate('STATE', aStringList2[3], []) then
              begin
                evERSUI.Insert;
                if aStringList2[13] = '0.00' then
                  evERSUI.FieldByName('Amount8').Value := 'N/A';
                if aStringList2[14] = '0.00' then
                  evERSUI.FieldByName('Amount9').Value := 'N/A';
                if aStringList2[15] = '0.00' then
                  evERSUI.FieldByName('Amount10').Value := 'N/A';
                evERSUI.FieldByName('STATE').AsString := aStringList2[3];
                evERSUI.Post;
              end;

        if (aStringList2[3] <> '') and (aStringList2[4] <> '') then
            if (aStringList2[6] <> '0.00') then
              if not evLocals.Locate('STATE;RapidLocal', VarArrayOf([aStringList2[3], aStringList2[4]]), []) then
              begin
                evLocals.Insert;
                evLocals.FieldByName('STATE').AsString := aStringList2[3];
                evLocals.FieldByName('RapidLocal').AsString := aStringList2[4];
                evLocals.Post;
              end;
        wwcsLiabilities.Post;
      end;
    end;
  end;
end;

procedure TEDIT_CO_IMPORT.AddFedLiability(TargetDS: TevClientDataSet; Amount: Double; TaxType: String);
begin
  if Amount <> 0 then
  begin
    TargetDS.Insert;
    TargetDS.FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;

    if not UseDueDate then
      TargetDS.FieldByName('DUE_DATE').AsDateTime := wwcsLiabilities.FieldByName('DUE_DATE').AsDateTime
    else
      TargetDS.FieldByName('DUE_DATE').AsDateTime :=  DueDate;

    TargetDS.FieldByName('AMOUNT').AsFloat := Amount;

    if AdjustmentType <> '' then
      TargetDS.FieldByName('ADJUSTMENT_TYPE').AsString := AdjustmentType;

    TargetDS.FieldByName('THIRD_PARTY').AsString := GROUP_BOX_NO;
    TargetDS.FieldByName('CHECK_DATE').AsDateTime := wwcsLiabilities.FieldByName('CHECK_DATE').AsDateTime;
    TargetDS.FieldByName('TAX_TYPE').AsString := TaxType;

    if NewFormat then
    begin
      LiabStatus := wwcsLiabilities.FieldByName('STATUS').AsString;
      TargetDS.FieldByName('STATUS').AsString := wwcsLiabilities.FieldByName('STATUS').AsString;
      if Trim(wwcsLiabilities.FieldByName('NOTES').AsString) <> '' then
        TargetDS.FieldByName('NOTES').Value := wwcsLiabilities.FieldByName('NOTES').Value;
    end
    else
      TargetDS.FieldByName('STATUS').AsString := LiabStatus;

    TargetDS.FieldByName('IMPOUNDED').AsString := Impounded;

    AttachDeposit(TargetDS, LiabStatus);

    TargetDS.Post;
  end;
end;

procedure TEDIT_CO_IMPORT.AddStateLiability(TargetDS: TevClientDataSet;
  Amount: Double; TaxType: String);
begin
  if (DM_COMPANY.CO_STATES.Locate('CO_NBR;STATE', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').Value, wwcsLiabilities.FieldByName('STATE').Value]), [])) and (Amount <> 0) then
  begin
    TargetDS.Insert;
    TargetDS.FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
    TargetDS.FieldByName('CO_STATES_NBR').AsInteger := DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').AsInteger;

    if not UseDueDate then
      TargetDS.FieldByName('DUE_DATE').AsDateTime := wwcsLiabilities.FieldByName('DUE_DATE').AsDateTime
    else
      TargetDS.FieldByName('DUE_DATE').AsDateTime := DueDate;

    if NewFormat then
    begin
      LiabStatus := wwcsLiabilities.FieldByName('STATUS').AsString;
      TargetDS.FieldByName('STATUS').AsString := wwcsLiabilities.FieldByName('STATUS').AsString;
      if Trim(wwcsLiabilities.FieldByName('NOTES').AsString) <> '' then
        TargetDS.FieldByName('NOTES').Value := wwcsLiabilities.FieldByName('NOTES').Value;
    end
    else
      TargetDS.FieldByName('STATUS').AsString := LiabStatus;

    if AdjustmentType <> '' then
      TargetDS.FieldByName('ADJUSTMENT_TYPE').AsString := AdjustmentType;
      TargetDS.FieldByName('AMOUNT').AsFloat := Amount;
      TargetDS.FieldByName('THIRD_PARTY').AsString := GROUP_BOX_NO;
      TargetDS.FieldByName('CHECK_DATE').AsDateTime := wwcsLiabilities.FieldByName('CHECK_DATE').AsDateTime;
      TargetDS.FieldByName('TAX_TYPE').AsString := TaxType;
      TargetDS.FieldByName('IMPOUNDED').AsString := Impounded;
      TargetDS.FieldByName('SY_STATE_DEPOSIT_FREQ_NBR').AsInteger := DM_COMPANY.CO_STATES.FieldByName('SY_STATE_DEPOSIT_FREQ_NBR').AsInteger;

      AttachDeposit(TargetDS, LiabStatus);

      TargetDS.Post;
    end;
end;

procedure TEDIT_CO_IMPORT.AddSui7Liability(TargetDS: TevClientDataSet;
  Amount: Double);
begin
  if Amount <> 0 then
  begin
    DM_SYSTEM_STATE.SY_STATES.DataRequired('STATE='''+wwcsLiabilities.FieldByName('STATE').AsString+'''');
    DM_SYSTEM_STATE.SY_SUI.DataRequired('SY_STATES_NBR='+DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsString+' and EE_ER_OR_EE_OTHER_OR_ER_OTHER='+''''+GROUP_BOX_ER+'''');
    DM_COMPANY.CO_SUI.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString+' and SY_SUI_NBR='+DM_SYSTEM_STATE.SY_SUI.FieldByName('SY_SUI_NBR').AsString);

    TargetDS.Insert;
    TargetDS.FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
    TargetDS.FieldByName('CO_SUI_NBR').AsInteger := DM_COMPANY.CO_SUI.FieldByName('CO_SUI_NBR').AsInteger;

    if not UseDueDate then
      TargetDS.FieldByName('DUE_DATE').AsDateTime := wwcsLiabilities.FieldByName('DUE_DATE').AsDateTime
    else
      TargetDS.FieldByName('DUE_DATE').AsDateTime := DueDate;

    if NewFormat then
    begin
      LiabStatus := wwcsLiabilities.FieldByName('STATUS').AsString;
      TargetDS.FieldByName('STATUS').AsString := wwcsLiabilities.FieldByName('STATUS').AsString;
      if Trim(wwcsLiabilities.FieldByName('NOTES').AsString) <> '' then
        TargetDS.FieldByName('NOTES').Value := wwcsLiabilities.FieldByName('NOTES').Value;
    end
    else
      TargetDS.FieldByName('STATUS').AsString := LiabStatus;

    TargetDS.FieldByName('AMOUNT').AsFloat := Amount;

    if AdjustmentType <> '' then
      TargetDS.FieldByName('ADJUSTMENT_TYPE').AsString := AdjustmentType;
    TargetDS.FieldByName('THIRD_PARTY').AsString := GROUP_BOX_NO;
    TargetDS.FieldByName('CHECK_DATE').AsDateTime :=  wwcsLiabilities.FieldByName('CHECK_DATE').AsDateTime;
    TargetDS.FieldByName('IMPOUNDED').AsString := Impounded;

    AttachDeposit(TargetDS, LiabStatus);

    TargetDS.Post;
  end;
end;

procedure TEDIT_CO_IMPORT.AddSui3_5Liability(TargetDS: TevClientDataSet;
  Amount: Double);
begin
  if Amount <> 0 then
  begin
    if evEESUI.Locate('STATE', wwcsLiabilities.FieldByName('STATE').Value, []) then
    begin
      DM_COMPANY.CO_STATES.Locate('STATE', wwcsLiabilities.FieldByName('STATE').Value, []);
      if DM_COMPANY.CO_SUI.Locate('SY_SUI_NBR;CO_STATES_NBR' ,VarArrayOf([evEESUI.FieldByName('SUI3').Value, DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').Value]) ,[]) then
      begin
        TargetDS.Insert;
        TargetDS.FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
        TargetDS.FieldByName('CO_SUI_NBR').AsInteger := DM_COMPANY.CO_SUI.FieldByName('CO_SUI_NBR').AsInteger;

        if not UseDueDate then
          TargetDS.FieldByName('DUE_DATE').AsDateTime := wwcsLiabilities.FieldByName('DUE_DATE').AsDateTime
        else
          TargetDS.FieldByName('DUE_DATE').AsDateTime := DueDate;

        if NewFormat then
        begin
          LiabStatus := wwcsLiabilities.FieldByName('STATUS').AsString;
          TargetDS.FieldByName('STATUS').AsString := wwcsLiabilities.FieldByName('STATUS').AsString;
          if Trim(wwcsLiabilities.FieldByName('NOTES').AsString) <> '' then
            TargetDS.FieldByName('NOTES').Value := wwcsLiabilities.FieldByName('NOTES').Value;
        end
        else
          TargetDS.FieldByName('STATUS').AsString := LiabStatus;


        TargetDS.FieldByName('AMOUNT').AsFloat := Amount;
        if AdjustmentType <> '' then
          TargetDS.FieldByName('ADJUSTMENT_TYPE').AsString := AdjustmentType;
        TargetDS.FieldByName('THIRD_PARTY').AsString := GROUP_BOX_NO;
        TargetDS.FieldByName('CHECK_DATE').AsDateTime :=  wwcsLiabilities.FieldByName('CHECK_DATE').AsDateTime;
        TargetDS.FieldByName('IMPOUNDED').AsString := Impounded;

        AttachDeposit(TargetDS, LiabStatus);

        TargetDS.Post;
      end;
    end;
  end;
end;

procedure TEDIT_CO_IMPORT.AddSui8_10Liability(TargetDS: TevClientDataSet;
  Amount: Double);
begin
  if Amount <> 0 then
  begin
    if evERSUI.Locate('STATE', wwcsLiabilities.FieldByName('STATE').Value, []) then
    begin
      DM_COMPANY.CO_STATES.Locate('STATE', wwcsLiabilities.FieldByName('STATE').Value, []);
      DM_COMPANY.CO_SUI.DataRequired('CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString+' and SY_SUI_NBR='+evERSUI.FieldByName('SUI8').AsString);//DM_SYSTEM_STATE.SY_SUI.FieldByName('SY_SUI_NBR').AsString);
      if DM_COMPANY.CO_SUI.Locate('SY_SUI_NBR;CO_STATES_NBR' ,VarArrayOf([evERSUI.FieldByName('SUI8').Value, DM_COMPANY.CO_STATES.FieldByName('CO_STATES_NBR').Value]) ,[]) then
      begin
        TargetDS.Insert;
        TargetDS.FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
        TargetDS.FieldByName('CO_SUI_NBR').AsInteger := DM_COMPANY.CO_SUI.FieldByName('CO_SUI_NBR').AsInteger;

        if not UseDueDate then
          TargetDS.FieldByName('DUE_DATE').AsDateTime := wwcsLiabilities.FieldByName('DUE_DATE').AsDateTime
        else
          TargetDS.FieldByName('DUE_DATE').AsDateTime := DueDate;

       if NewFormat then
       begin
         LiabStatus := wwcsLiabilities.FieldByName('STATUS').AsString;
         TargetDS.FieldByName('STATUS').AsString := wwcsLiabilities.FieldByName('STATUS').AsString;
         if Trim(wwcsLiabilities.FieldByName('NOTES').AsString) <> '' then
           TargetDS.FieldByName('NOTES').Value := wwcsLiabilities.FieldByName('NOTES').Value;
       end
       else
         TargetDS.FieldByName('STATUS').AsString := LiabStatus;

        TargetDS.FieldByName('AMOUNT').AsFloat := Amount;

        if AdjustmentType <> '' then
          TargetDS.FieldByName('ADJUSTMENT_TYPE').AsString := AdjustmentType;

        TargetDS.FieldByName('THIRD_PARTY').AsString := GROUP_BOX_NO;
        TargetDS.FieldByName('CHECK_DATE').AsDateTime :=  wwcsLiabilities.FieldByName('CHECK_DATE').AsDateTime;
        TargetDS.FieldByName('IMPOUNDED').AsString := Impounded;

        AttachDeposit(TargetDS, LiabStatus);

        TargetDS.Post;
      end;
    end;
  end;
end;

procedure TEDIT_CO_IMPORT.AddLocalLiability(TargetDS: TevClientDataSet;
  Amount: Double);
begin
  if evLocals.Locate('STATE;RapidLocal', VarArrayOf([wwcsLiabilities.FieldByName('STATE').Value, wwcsLiabilities.FieldByName('LOCAL').Value]), []) then
  begin
    if DM_COMPANY.CO_LOCAL_TAX.Locate('SY_LOCALS_NBR', evLocals.FieldByName('EvoLocalNbr').Value, []) then
    begin
      TargetDS.Insert;
      TargetDS.FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
      TargetDS.FieldByName('CO_LOCAL_TAX_NBR').AsInteger := DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;

      if not UseDueDate then
        TargetDS.FieldByName('DUE_DATE').AsDateTime := wwcsLiabilities.FieldByName('DUE_DATE').AsDateTime
      else
        TargetDS.FieldByName('DUE_DATE').AsDateTime := DueDate;

      if NewFormat then
      begin
        LiabStatus := wwcsLiabilities.FieldByName('STATUS').AsString;
        TargetDS.FieldByName('STATUS').AsString := wwcsLiabilities.FieldByName('STATUS').AsString;
        if Trim(wwcsLiabilities.FieldByName('NOTES').AsString) <> '' then
          TargetDS.FieldByName('NOTES').Value := wwcsLiabilities.FieldByName('NOTES').Value;
      end
      else
        TargetDS.FieldByName('STATUS').AsString := LiabStatus;

      TargetDS.FieldByName('AMOUNT').AsFloat := Amount;
      if AdjustmentType <> '' then
        TargetDS.FieldByName('ADJUSTMENT_TYPE').AsString :=  AdjustmentType;
      TargetDS.FieldByName('THIRD_PARTY').AsString := GROUP_BOX_NO;
      TargetDS.FieldByName('CHECK_DATE').AsDateTime := wwcsLiabilities.FieldByName('CHECK_DATE').AsDateTime;
      TargetDS.FieldByName('IMPOUNDED').AsString := Impounded;

      AttachDeposit(TargetDS, LiabStatus);

      TargetDS.Post;
    end;
  end;
end;

procedure TEDIT_CO_IMPORT.AttachDeposit(TargetDS: TevClientDataSet; ALiabStatus: String);
begin
  if ALiabStatus[1] in [TAX_DEPOSIT_STATUS_PAID, TAX_DEPOSIT_STATUS_PAID_WO_FUNDS, TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE] then
  begin
    if not DM_COMPANY.CO_TAX_DEPOSITS.Locate('CO_NBR;TAX_PAYMENT_REFERENCE_NUMBER', VarArrayOf([DM_COMPANY.CO.FieldByName('CO_NBR').Value, 'Import '+DateTimeToStr(Trunc(now))]), []) then
    begin
      DM_COMPANY.CO_TAX_DEPOSITS.Insert;
      DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
      DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PAID;
      DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString := 'Import '+DateTimeToStr(Trunc(now));
      DM_COMPANY.CO_TAX_DEPOSITS.Post;
      TargetDS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger := DM_COMPANY.CO_TAX_DEPOSITS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
    end;
  end;
end;

initialization
  RegisterClass(TEDIT_CO_IMPORT);

end.
