object EDIT_IMPORT_SETUP: TEDIT_IMPORT_SETUP
  Left = 388
  Top = 284
  Width = 554
  Height = 439
  Caption = 'Import Setup'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TevPageControl
    Left = 0
    Top = 0
    Width = 546
    Height = 405
    ActivePage = TabSheet5
    Align = alClient
    TabOrder = 0
    object tbshMatching: TTabSheet
      Caption = 'Match'
      OnShow = tbshMatchingShow
      object lsbxRapid: TListBox
        Left = 8
        Top = 8
        Width = 185
        Height = 217
        ItemHeight = 13
        TabOrder = 0
        OnClick = lsbxRapidClick
      end
      object bttnMatch: TevButton
        Left = 204
        Top = 109
        Width = 75
        Height = 25
        Caption = 'Match'
        TabOrder = 1
        OnClick = bttnMatchClick
      end
      object lsbxEvolution: TListBox
        Left = 288
        Top = 8
        Width = 185
        Height = 217
        ItemHeight = 13
        TabOrder = 2
      end
      object lsbxMatched: TListBox
        Left = 8
        Top = 240
        Width = 361
        Height = 137
        ItemHeight = 13
        TabOrder = 3
      end
      object bttnDelete: TevButton
        Left = 392
        Top = 254
        Width = 75
        Height = 25
        Caption = 'Delete Match'
        TabOrder = 4
        OnClick = bttnDeleteClick
      end
      object bttnNext: TevButton
        Left = 392
        Top = 350
        Width = 75
        Height = 25
        Caption = 'Done'
        ModalResult = 1
        TabOrder = 5
      end
      object bttnCancel: TevButton
        Left = 392
        Top = 307
        Width = 75
        Height = 25
        Caption = 'Cancel'
        ModalResult = 2
        TabOrder = 6
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Misc Setup'
      ImageIndex = 2
      OnShow = TabSheet1Show
      object evLabel1: TevLabel
        Left = 8
        Top = 104
        Width = 93
        Height = 13
        Caption = 'Effective Start Date'
      end
      object evLabel2: TevLabel
        Left = 8
        Top = 152
        Width = 49
        Height = 13
        Caption = 'Week Nbr'
      end
      object evLabel3: TevLabel
        Left = 72
        Top = 152
        Width = 50
        Height = 13
        Caption = 'Month Nbr'
      end
      object evdtEffectiveStartDate: TevDBDateTimePicker
        Left = 8
        Top = 120
        Width = 121
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        Epoch = 1950
        ShowButton = True
        TabOrder = 0
      end
      object evButton1: TevButton
        Left = 392
        Top = 344
        Width = 75
        Height = 25
        Caption = 'Done'
        ModalResult = 1
        TabOrder = 1
      end
      object evseWeekNbr: TevSpinEdit
        Left = 8
        Top = 168
        Width = 49
        Height = 30
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxValue = 4
        MinValue = 1
        ParentFont = False
        TabOrder = 2
        Value = 1
      end
      object evseMonthNbr: TevSpinEdit
        Left = 72
        Top = 168
        Width = 49
        Height = 30
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxValue = 12
        MinValue = 1
        ParentFont = False
        TabOrder = 3
        Value = 1
      end
      object evRadioGroup1: TevRadioGroup
        Left = 8
        Top = 16
        Width = 169
        Height = 73
        Caption = 'Treat DETYPE K as?'
        Enabled = False
        ItemIndex = 2
        Items.Strings = (
          '% of earn code group'
          '% of gross'
          'None')
        TabOrder = 4
      end
      object evRadioGroup2: TevRadioGroup
        Left = 192
        Top = 16
        Width = 169
        Height = 73
        Caption = 'evRadioGroup2'
        Enabled = False
        ItemIndex = 2
        Items.Strings = (
          '% of earn code group'
          '% of gross'
          'None')
        TabOrder = 5
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'YTD Setup'
      ImageIndex = 2
      OnShow = TabSheet2Show
      object evLabel4: TevLabel
        Left = 24
        Top = 24
        Width = 57
        Height = 13
        Caption = 'Check Date'
      end
      object evLabel5: TevLabel
        Left = 24
        Top = 72
        Width = 86
        Height = 13
        Caption = 'Period Begin Date'
      end
      object evLabel6: TevLabel
        Left = 24
        Top = 120
        Width = 78
        Height = 13
        Caption = 'Period End Date'
      end
      object evdtCheckDate: TevDateTimePicker
        Left = 24
        Top = 40
        Width = 105
        Height = 21
        Date = 36763.000000000000000000
        Time = 36763.000000000000000000
        TabOrder = 0
      end
      object evdtPeriodBegin: TevDateTimePicker
        Left = 24
        Top = 88
        Width = 105
        Height = 21
        Date = 36763.000000000000000000
        Time = 36763.000000000000000000
        TabOrder = 1
      end
      object evdtPeriodEnd: TevDateTimePicker
        Left = 24
        Top = 136
        Width = 105
        Height = 21
        Date = 36763.000000000000000000
        Time = 36763.000000000000000000
        TabOrder = 2
      end
      object evButton2: TevButton
        Left = 392
        Top = 344
        Width = 75
        Height = 25
        Caption = 'Done'
        ModalResult = 1
        TabOrder = 3
      end
      object evCheckListBox1: TevCheckListBox
        Left = 24
        Top = 200
        Width = 209
        Height = 113
        Columns = 3
        ItemHeight = 13
        Items.Strings = (
          'January'
          'Febuary'
          'March'
          'April'
          'May'
          'June'
          'July'
          'August'
          'September'
          'October'
          'November'
          'December')
        TabOrder = 4
      end
      object evComboBox1: TevComboBox
        Left = 24
        Top = 168
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 5
        OnChange = evComboBox1Change
        Items.Strings = (
          'All'
          'First Quarter'
          'Second Quarter'
          'Third Quarter'
          'Fourth Quarter'
          'First Half'
          'Second Half'
          'Custom')
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Marital Status Setup'
      ImageIndex = 3
      object evButton3: TevButton
        Left = 384
        Top = 344
        Width = 89
        Height = 33
        Caption = 'Done'
        TabOrder = 0
        OnClick = evButton3Click
      end
      object evDBGrid5: TevDBGrid
        Left = 16
        Top = 16
        Width = 321
        Height = 361
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_IMPORT_SETUP\evDBGrid5'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        OnRowChanged = evDBGrid5RowChanged
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = evDataSource1
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = False
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = clCream
      end
      object evDBLookupCombo1: TevDBLookupCombo
        Left = 208
        Top = 72
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'STATUS_TYPE'#9'2'#9'STATUS_TYPE'#9'F'
          'STATUS_DESCRIPTION'#9'40'#9'STATUS_DESCRIPTION'#9'F')
        DataField = 'EVSTATUS'
        DataSource = evDataSource1
        LookupTable = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
        LookupField = 'STATUS_TYPE'
        Style = csDropDownList
        TabOrder = 2
        AutoDropDown = True
        ShowButton = True
        UseTFields = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object evDBLookupCombo3: TevDBLookupCombo
        Left = 344
        Top = 16
        Width = 137
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'STATUS_TYPE'#9'2'#9'STATUS_TYPE'#9'F'
          'STATUS_DESCRIPTION'#9'40'#9'STATUS_DESCRIPTION'#9'F')
        DataField = 'EVSTATUS'
        DataSource = evDataSource1
        LookupTable = DM_SY_STATE_MARITAL_STATUS.SY_STATE_MARITAL_STATUS
        LookupField = 'STATUS_TYPE'
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        UseTFields = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = evDBLookupCombo3CloseUp
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'HR E/D Match'
      ImageIndex = 4
      OnShow = TabSheet4Show
      object evDBGrid1: TevDBGrid
        Left = 8
        Top = 8
        Width = 369
        Height = 369
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_IMPORT_SETUP\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = evDataSource2
        KeyOptions = [dgEnterToTab, dgAllowInsert]
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = clCream
      end
      object evDBLookupCombo2: TevDBLookupCombo
        Left = 224
        Top = 64
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'CUSTOM_E_D_CODE_NUMBER'#9'F'
          'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
        DataField = 'EV_E_D'
        DataSource = evDataSource2
        LookupTable = DM_CL_E_DS.CL_E_DS
        LookupField = 'CUSTOM_E_D_CODE_NUMBER'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnChange = evDBLookupCombo2Change
        OnCloseUp = evDBLookupCombo2CloseUp
      end
      object evButton4: TevButton
        Left = 392
        Top = 352
        Width = 75
        Height = 25
        Caption = 'Done'
        TabOrder = 2
        OnClick = evButton4Click
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Liabilities Setup'
      ImageIndex = 5
      OnShow = TabSheet5Show
      object evLabel8: TevLabel
        Left = 8
        Top = 128
        Width = 79
        Height = 13
        Caption = 'Adjustment Type'
      end
      object evLabel11: TevLabel
        Left = 8
        Top = 200
        Width = 64
        Height = 13
        Caption = 'Match Locals'
      end
      object evDBComboBox2: TevDBComboBox
        Left = 8
        Top = 144
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
      end
      object evButton5: TevButton
        Left = 448
        Top = 312
        Width = 75
        Height = 25
        Caption = 'Done'
        Enabled = False
        ModalResult = 1
        TabOrder = 1
      end
      object evDBGrid4: TevDBGrid
        Left = 8
        Top = 176
        Width = 425
        Height = 185
        DisableThemesInTitle = False
        ControlType.Strings = (
          'EvoLocal;CustomEdit;evDBLookupCombo10')
        Selected.Strings = (
          'STATE'#9'2'#9'State'#9'F'
          'RapidLocal'#9'30'#9'Rapidlocal'#9'F'
          'EvoLocal'#9'30'#9'Evolocal'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_IMPORT_SETUP\evDBGrid4'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        OnRowChanged = evDBGrid4RowChanged
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = evsLocals
        ReadOnly = False
        TabOrder = 2
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object evDBLookupCombo10: TevDBLookupCombo
        Left = 248
        Top = 176
        Width = 137
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'LOCALITY_NAME'#9'F')
        DataField = 'EvoLocal'
        DataSource = evsLocals
        LookupTable = DM_SY_LOCALS.SY_LOCALS
        LookupField = 'NAME'
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = evDBLookupCombo10CloseUp
      end
      object evGroupBox1: TevGroupBox
        Left = 168
        Top = 24
        Width = 153
        Height = 89
        Caption = 'Due Date'
        TabOrder = 4
        object evLabel12: TevLabel
          Left = 25
          Top = 40
          Width = 23
          Height = 13
          Caption = 'Date'
        end
        object evCheckBox1: TevCheckBox
          Left = 24
          Top = 24
          Width = 114
          Height = 17
          Caption = 'Use Override Date?'
          TabOrder = 0
          OnClick = evCheckBox1Click
        end
        object evDateTimePicker1: TevDateTimePicker
          Left = 24
          Top = 56
          Width = 97
          Height = 21
          Date = 37046.000000000000000000
          Time = 37046.000000000000000000
          Enabled = False
          TabOrder = 1
        end
      end
      object evrgImpounded: TevRadioGroup
        Left = 335
        Top = 24
        Width = 125
        Height = 89
        Caption = 'Collected for Quarterlies'
        ItemIndex = 0
        Items.Strings = (
          'Yes'
          'No')
        TabOrder = 5
      end
      object evGroupBox2: TevGroupBox
        Left = 8
        Top = 24
        Width = 148
        Height = 90
        Caption = 'Liability Status'
        TabOrder = 6
        object evLabel7: TevLabel
          Left = 12
          Top = 41
          Width = 67
          Height = 13
          Caption = 'Liability Status'
        end
        object evDBComboBox1: TevDBComboBox
          Left = 12
          Top = 57
          Width = 121
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          DropDownCount = 8
          Enabled = False
          ItemHeight = 0
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 0
          UnboundDataType = wwDefault
          OnChange = evDBComboBox1Change
        end
        object evCheckBox2: TevCheckBox
          Left = 14
          Top = 20
          Width = 123
          Height = 17
          Caption = 'Use Override Status?'
          TabOrder = 1
          OnClick = evCheckBox2Click
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'SUI Matching'
      ImageIndex = 6
      object evLabel9: TevLabel
        Left = 8
        Top = 0
        Width = 68
        Height = 13
        Caption = 'Match EE SUI'
      end
      object evLabel10: TevLabel
        Left = 8
        Top = 192
        Width = 69
        Height = 13
        Caption = 'Match ER SUI'
      end
      object evDBGrid2: TevDBGrid
        Left = 8
        Top = 16
        Width = 433
        Height = 169
        DisableThemesInTitle = False
        ControlType.Strings = (
          'AMOUNT3;CustomEdit;evDBLookupCombo4'
          'AMOUNT4;CustomEdit;evDBLookupCombo5'
          'AMOUNT5;CustomEdit;evDBLookupCombo6')
        Selected.Strings = (
          'STATE'#9'2'#9'State'#9'F'
          'AMOUNT3'#9'20'#9'Amount3'#9'F'
          'AMOUNT4'#9'20'#9'Amount4'#9'F'
          'AMOUNT5'#9'20'#9'Amount5'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_IMPORT_SETUP\evDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        OnRowChanged = evDBGrid2RowChanged
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = evsEESUI
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object evDBGrid3: TevDBGrid
        Left = 8
        Top = 208
        Width = 433
        Height = 177
        DisableThemesInTitle = False
        ControlType.Strings = (
          'AMOUNT8;CustomEdit;evDBLookupCombo7'
          'AMOUNT9;CustomEdit;evDBLookupCombo8'
          'AMOUNT10;CustomEdit;evDBLookupCombo9')
        Selected.Strings = (
          'STATE'#9'2'#9'State'#9'F'
          'AMOUNT8'#9'20'#9'Amount8'#9'F'
          'AMOUNT9'#9'20'#9'Amount9'#9'F'
          'AMOUNT10'#9'20'#9'Amount10'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_IMPORT_SETUP\evDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        OnRowChanged = evDBGrid3RowChanged
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = evsERSUI
        ReadOnly = False
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object evButton6: TevButton
        Left = 456
        Top = 352
        Width = 75
        Height = 25
        Caption = 'DONE'
        ModalResult = 1
        TabOrder = 2
      end
      object evDBLookupCombo4: TevDBLookupCombo
        Left = 64
        Top = 40
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'SUI_TAX_NAME'#9'40'#9'SUI_TAX_NAME'#9'F')
        DataField = 'AMOUNT3'
        DataSource = evsEESUI
        LookupTable = evEESUIStorage
        LookupField = 'SUI_TAX_NAME'
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = evDBLookupCombo4CloseUp
      end
      object evDBLookupCombo5: TevDBLookupCombo
        Left = 256
        Top = 40
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'SUI_TAX_NAME'#9'40'#9'SUI_TAX_NAME'#9'F')
        DataField = 'AMOUNT4'
        DataSource = evsEESUI
        LookupTable = evEESUIStorage
        LookupField = 'SUI_TAX_NAME'
        Style = csDropDownList
        TabOrder = 4
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = evDBLookupCombo5CloseUp
      end
      object evDBLookupCombo6: TevDBLookupCombo
        Left = 392
        Top = 40
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'SUI_TAX_NAME'#9'40'#9'SUI_TAX_NAME'#9'F')
        DataField = 'AMOUNT5'
        DataSource = evsEESUI
        LookupTable = evEESUIStorage
        LookupField = 'SUI_TAX_NAME'
        Style = csDropDownList
        TabOrder = 5
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = evDBLookupCombo6CloseUp
      end
      object evDBLookupCombo7: TevDBLookupCombo
        Left = 64
        Top = 232
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'SUI_TAX_NAME'#9'40'#9'SUI_TAX_NAME'#9'F')
        DataField = 'AMOUNT8'
        DataSource = evsERSUI
        LookupTable = evERSUIStorage
        LookupField = 'SUI_TAX_NAME'
        Style = csDropDownList
        TabOrder = 6
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = evDBLookupCombo7CloseUp
      end
      object evDBLookupCombo8: TevDBLookupCombo
        Left = 192
        Top = 232
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'SUI_TAX_NAME'#9'40'#9'SUI_TAX_NAME'#9'F')
        DataField = 'AMOUNT9'
        DataSource = evsERSUI
        LookupTable = evERSUIStorage
        LookupField = 'SUI_TAX_NAME'
        Style = csDropDownList
        TabOrder = 7
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = evDBLookupCombo8CloseUp
      end
      object evDBLookupCombo9: TevDBLookupCombo
        Left = 312
        Top = 232
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'SUI_TAX_NAME'#9'40'#9'SUI_TAX_NAME'#9'F')
        DataField = 'AMOUNT10'
        DataSource = evsERSUI
        LookupTable = evERSUIStorage
        LookupField = 'SUI_TAX_NAME'
        Style = csDropDownList
        TabOrder = 8
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = evDBLookupCombo9CloseUp
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'Local Matching'
      ImageIndex = 7
      object evDBGrid6: TevDBGrid
        Left = 8
        Top = 8
        Width = 505
        Height = 369
        DisableThemesInTitle = False
        ControlType.Strings = (
          'EvoLocal;CustomEdit;evDBLookupCombo12')
        Selected.Strings = (
          'STATE'#9'2'#9'State'#9'F'
          'RapidLocal'#9'12'#9'Rapidlocal'#9'F'
          'EvoLocal'#9'40'#9'Evolocal'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_IMPORT_SETUP\evDBGrid6'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        OnRowChanged = evDBGrid6RowChanged
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = evdsYTDLocals
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object evButton7: TevButton
        Left = 520
        Top = 352
        Width = 75
        Height = 25
        Caption = 'Done'
        ModalResult = 1
        TabOrder = 1
      end
      object evDBLookupCombo12: TevDBLookupCombo
        Left = 304
        Top = 32
        Width = 193
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'LOCALITY_NAME'#9'F')
        DataField = 'EvoLocal'
        DataSource = evdsYTDLocals
        LookupTable = DM_SY_LOCALS.SY_LOCALS
        LookupField = 'NAME'
        Style = csDropDownList
        TabOrder = 2
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnCloseUp = evDBLookupCombo12CloseUp
      end
    end
  end
  object evClientDataSet1: TevClientDataSet
    Left = 444
    Top = 48
    object evClientDataSet1EE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object evClientDataSet1CO_STATES_NBR: TIntegerField
      FieldName = 'CO_STATES_NBR'
    end
    object evClientDataSet1STATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object evClientDataSet1HRSTATUS: TStringField
      FieldName = 'HRSTATUS'
      Size = 2
    end
    object evClientDataSet1EVSTATUS: TStringField
      FieldName = 'EVSTATUS'
      Size = 2
    end
  end
  object evDataSource1: TevDataSource
    DataSet = evClientDataSet1
    Left = 396
    Top = 80
  end
  object evClientDataSet2: TevClientDataSet
    Left = 460
    Top = 160
    object evClientDataSet2HR_E_D: TStringField
      FieldName = 'HR_E_D'
      Size = 4
    end
    object evClientDataSet2HR_DESCRIPTION: TStringField
      FieldName = 'HR_DESCRIPTION'
    end
    object evClientDataSet2EV_E_D: TStringField
      FieldName = 'EV_E_D'
      Size = 4
    end
    object evClientDataSet2CL_E_DS_NBR: TIntegerField
      FieldName = 'CL_E_DS_NBR'
    end
  end
  object evDataSource2: TevDataSource
    DataSet = evClientDataSet2
    Left = 468
    Top = 104
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 204
    Top = 344
  end
  object evDataSource3: TevDataSource
    Left = 76
    Top = 344
  end
  object evEESUI: TevClientDataSet
    FieldDefs = <
      item
        Name = 'STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'AMOUNT3'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'AMOUNT4'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'AMOUNT5'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'SUI4'
        DataType = ftInteger
      end
      item
        Name = 'SUI5'
        DataType = ftInteger
      end
      item
        Name = 'SUI3'
        DataType = ftInteger
      end>
    Left = 396
    Top = 136
    object evEESUISTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object evEESUIAMOUNT3: TStringField
      FieldName = 'AMOUNT3'
      Size = 40
    end
    object evEESUIAMOUNT4: TStringField
      FieldName = 'AMOUNT4'
      Size = 40
    end
    object evEESUIAMOUNT5: TStringField
      FieldName = 'AMOUNT5'
      Size = 40
    end
    object evEESUISUI4: TIntegerField
      FieldName = 'SUI4'
    end
    object evEESUISUI5: TIntegerField
      FieldName = 'SUI5'
    end
    object evEESUISUI3: TIntegerField
      FieldName = 'SUI3'
    end
  end
  object evsEESUI: TevDataSource
    DataSet = evEESUI
    Left = 132
    Top = 344
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 236
    Top = 344
  end
  object evDataSource4: TevDataSource
    DataSet = DM_SY_SUI.SY_SUI
    Left = 52
    Top = 344
  end
  object evERSUI: TevClientDataSet
    FieldDefs = <
      item
        Name = 'STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'AMOUNT8'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'AMOUNT9'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'AMOUNT10'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'SUI8'
        DataType = ftInteger
      end
      item
        Name = 'SUI9'
        DataType = ftInteger
      end
      item
        Name = 'SUI10'
        DataType = ftInteger
      end>
    Left = 140
    Top = 288
    object evERSUISTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object evERSUIAMOUNT8: TStringField
      FieldName = 'AMOUNT8'
      Size = 40
    end
    object evERSUIAMOUNT9: TStringField
      FieldName = 'AMOUNT9'
      Size = 40
    end
    object evERSUIAMOUNT10: TStringField
      FieldName = 'AMOUNT10'
      Size = 40
    end
    object evERSUISUI8: TIntegerField
      FieldName = 'SUI8'
    end
    object evERSUISUI9: TIntegerField
      FieldName = 'SUI9'
    end
    object evERSUISUI10: TIntegerField
      FieldName = 'SUI10'
    end
  end
  object evsERSUI: TevDataSource
    DataSet = evERSUI
    Left = 292
    Top = 344
  end
  object evEESUIStorage: TevClientDataSet
    Left = 364
    Top = 24
  end
  object evERSUIStorage: TevClientDataSet
    Left = 260
    Top = 296
  end
  object evLocals: TevClientDataSet
    FieldDefs = <
      item
        Name = 'STATE'
        DataType = ftString
        Size = 2
      end
      item
        Name = 'RapidLocal'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'EvoLocal'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'EvoLocalNbr'
        DataType = ftInteger
      end>
    Left = 140
    Top = 200
    object evLocalsSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object evLocalsRapidLocal: TStringField
      FieldName = 'RapidLocal'
      Size = 40
    end
    object evLocalsEvoLocal: TStringField
      FieldName = 'EvoLocal'
      Size = 40
    end
    object evLocalsEvoLocalNbr: TIntegerField
      FieldName = 'EvoLocalNbr'
    end
  end
  object evsLocals: TevDataSource
    DataSet = evLocals
    Left = 212
    Top = 224
  end
  object DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL
    Left = 60
    Top = 96
  end
  object evYTDLocals: TevClientDataSet
    Left = 412
    Top = 289
    object evYTDLocalsSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object evYTDLocalsRapidLocal: TStringField
      FieldName = 'RapidLocal'
      Size = 40
    end
    object evYTDLocalsEvoLocal: TStringField
      FieldName = 'EvoLocal'
      Size = 40
    end
    object evYTDLocalsEvoLocalNbr: TIntegerField
      FieldName = 'EvoLocalNbr'
    end
  end
  object evdsYTDLocals: TevDataSource
    DataSet = evYTDLocals
    Left = 428
    Top = 352
  end
  object evDataSource5: TevDataSource
    DataSet = DM_SY_STATES.SY_STATES
    Left = 124
    Top = 128
  end
  object evDataSource6: TevDataSource
    DataSet = DM_SY_LOCALS.SY_LOCALS
    Left = 236
    Top = 128
  end
end
