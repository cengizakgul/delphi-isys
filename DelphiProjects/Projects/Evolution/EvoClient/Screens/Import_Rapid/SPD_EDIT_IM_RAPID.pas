// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_RAPID;

interface                                       

uses
  Windows,  SPackageEntry, SFrameEntry, SDataStructure, StdCtrls,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, ComCtrls, DBCtrls, Controls, EvBasicUtils,
  Classes, Db, Wwdatsrc, EvUtils, CheckLst, Dialogs, EvContext,
  Sysutils, Forms,  Messages, Graphics, Spin, Variants, EvDataAccessComponents,
  wwdbdatetimepicker, SDM_IM_RAPID, EvTypes, EvSecElement, EvConsts, SRapidMaritalStatusImport,
  SDDClasses, ISBasicClasses, IniFiles, EvStreamUtils, SDataDictbureau,
  SDataDictclient, SDataDictsystem, SDataDicttemp, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIwwDBDateTimePicker,
  isUIEdit;

type
  TEDIT_IM_RAPID = class(TFrameEntry)
    DM_TEMPORARY: TDM_TEMPORARY;
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    pnlFrameCntr: TPanel;
    evPageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    evrgNewOldSetup: TevRadioGroup;
    evGroupBox1: TevGroupBox;
    evdgSelectCompany: TevDBGrid;
    evGroupBox2: TevGroupBox;
    evclImportChoices: TevCheckListBox;
    Panel1: TPanel;
    Panel3: TPanel;
    Splitter1: TSplitter;
    Panel4: TPanel;
    Splitter2: TSplitter;
    Panel8: TPanel;
    btnAdd: TBitBtn;
    Panel9: TPanel;
    StaticText2: TStaticText;
    wwgdFile: TevDBGrid;
    Panel10: TPanel;
    StaticText3: TStaticText;
    wwgdEvolution: TevDBGrid;
    Panel5: TPanel;
    Panel7: TPanel;
    Panel6: TPanel;
    Panel11: TPanel;
    StaticText1: TStaticText;
    wwgdMatched: TevDBGrid;
    BitBtn1: TBitBtn;
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    Panel2: TPanel;
    Panel13: TPanel;
    Splitter3: TSplitter;
    Panel14: TPanel;
    Splitter4: TSplitter;
    Panel15: TPanel;
    BitBtn2: TBitBtn;
    Panel16: TPanel;
    StaticText4: TStaticText;
    wwgdRapidED: TevDBGrid;
    Panel17: TPanel;
    StaticText5: TStaticText;
    wwgdEvolED: TevDBGrid;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    btnRemove: TBitBtn;
    Panel21: TPanel;
    StaticText6: TStaticText;
    wwgdEDMatched: TevDBGrid;
    DM_CLIENT: TDM_CLIENT;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evPanel3: TevPanel;
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    TabSheet5: TTabSheet;
    Panel12: TPanel;
    Panel23: TPanel;
    Splitter5: TSplitter;
    Panel24: TPanel;
    Splitter6: TSplitter;
    Panel25: TPanel;
    BitBtn3: TBitBtn;
    Panel26: TPanel;
    StaticText7: TStaticText;
    wwDBGrid1: TevDBGrid;
    Panel27: TPanel;
    StaticText8: TStaticText;
    wwDBGrid2: TevDBGrid;
    Panel28: TPanel;
    Panel29: TPanel;
    Panel30: TPanel;
    BitBtn4: TBitBtn;
    Panel31: TPanel;
    StaticText9: TStaticText;
    wwDBGrid3: TevDBGrid;
    TabSheet6: TTabSheet;
    Panel22: TPanel;
    Panel33: TPanel;
    Splitter7: TSplitter;
    Panel34: TPanel;
    Splitter8: TSplitter;
    Panel35: TPanel;
    BitBtn5: TBitBtn;
    Panel36: TPanel;
    StaticText10: TStaticText;
    Panel37: TPanel;
    StaticText11: TStaticText;
    Panel38: TPanel;
    Panel39: TPanel;
    Panel40: TPanel;
    BitBtn6: TBitBtn;
    Panel41: TPanel;
    StaticText12: TStaticText;
    evgxSchedEDSetup: TevGroupBox;
    evgxTrreatK: TevRadioGroup;
    evgxTrreatQ: TevRadioGroup;
    evLabel1: TevLabel;
    evdtEffectiveStartDate: TevDBDateTimePicker;
    evLabel2: TevLabel;
    evseWeekNbr: TevSpinEdit;
    evLabel3: TevLabel;
    evseMonthNbr: TevSpinEdit;
    evgxYearToDateSetup: TevGroupBox;
    evLabel4: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    evdtCheckDate: TevDateTimePicker;
    evdtPeriodBegin: TevDateTimePicker;
    evdtPeriodEnd: TevDateTimePicker;
    evCheckListBox2: TevCheckListBox;
    evComboBox1: TevComboBox;
    TabSheet7: TTabSheet;
    evBitBtn3: TevBitBtn;
    evGroupBox3: TevGroupBox;
    wwDBGrid7: TevDBGrid;
    evGroupBox4: TevGroupBox;
    evclImportsToRun: TevCheckListBox;
    evrgSetupImport: TevRadioGroup;
    sdlgSaveSetup: TSaveDialog;
    opdgSetupFile: TOpenDialog;
    DM_COMPANY: TDM_COMPANY;
    evbtViewLog: TevBitBtn;
    TabSheet8: TTabSheet;
    Panel32: TPanel;
    Panel42: TPanel;
    Splitter9: TSplitter;
    Panel43: TPanel;
    Splitter10: TSplitter;
    Panel44: TPanel;
    BitBtn7: TBitBtn;
    Panel45: TPanel;
    StaticText13: TStaticText;
    evDBGrid1: TevDBGrid;
    Panel46: TPanel;
    StaticText14: TStaticText;
    evDBGrid2: TevDBGrid;
    Panel47: TPanel;
    Panel48: TPanel;
    Panel49: TPanel;
    BitBtn8: TBitBtn;
    Panel50: TPanel;
    StaticText15: TStaticText;
    evDBGrid3: TevDBGrid;
    evgpAverageStandardHrous: TevRadioGroup;
    TabSheet9: TTabSheet;
    Panel51: TPanel;
    Splitter11: TSplitter;
    Panel52: TPanel;
    Splitter12: TSplitter;
    Panel53: TPanel;
    BitBtn9: TBitBtn;
    Panel54: TPanel;
    StaticText16: TStaticText;
    evDBGrid4: TevDBGrid;
    Panel55: TPanel;
    StaticText17: TStaticText;
    evDBGrid5: TevDBGrid;
    Panel56: TPanel;
    Panel57: TPanel;
    Panel58: TPanel;
    BitBtn10: TBitBtn;
    Panel59: TPanel;
    StaticText18: TStaticText;
    evDBGrid6: TevDBGrid;
    evgxTrreatX: TevRadioGroup;
    evGroupBox5: TevGroupBox;
    evrbFixed: TevRadioButton;
    evrbComma: TevRadioButton;
    evrbSemiColon: TevRadioButton;
    evrbTab: TevRadioButton;
    evrbOther: TevRadioButton;
    evedOther: TevEdit;
    evcbQualifier: TevComboBox;
    evLabel5: TevLabel;
    wwDBGrid4: TevDBGrid;
    wwDBGrid5: TevDBGrid;
    wwDBGrid6: TevDBGrid;
    procedure TabSheet1Show(Sender: TObject);
    procedure btbNextClick(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure wwgdFileRowChanged(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure wwgdFileCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure wwgdRapidEDCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure btnRemoveClick(Sender: TObject);
    procedure TabSheet5Show(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure wwDBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure TabSheet6Show(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure wwDBGrid4CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure evComboBox1Change(Sender: TObject);
    procedure TabSheet3Show(Sender: TObject);
    procedure evBitBtn3Click(Sender: TObject);
    procedure TabSheet7Show(Sender: TObject);
    procedure evPageControl1Change(Sender: TObject);
    procedure evBitBtn2Click(Sender: TObject);
    procedure TabSheet4Show(Sender: TObject);
    procedure evbtViewLogClick(Sender: TObject);
    procedure TabSheet8Show(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure evDBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure wwDBGrid7RowChanged(Sender: TObject);
    procedure evrgNewOldSetupClick(Sender: TObject);
    procedure TabSheet9Show(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure evrbOtherClick(Sender: TObject);
    procedure evrbFixedClick(Sender: TObject);
    procedure evrbCommaClick(Sender: TObject);
    procedure evrbSemiColonClick(Sender: TObject);
    procedure evrbTabClick(Sender: TObject);
  private
    { Private declarations }
    Dir: String;
    AutoLaborConfig  : TStringList;
    TimeOffConfig    : TStringList;
    EmployeeConfig   : TStringList;
    ScheduledEDConfig: TStringList;
    SalaryHistConfig : TStringList;
    YearToDateConfig : TStringList;
    DirectDeposConfig: TStringList;
    procedure AddLocalMatch;
    procedure AddEDMatch;
    procedure AddEDGroupMatch;
    procedure AddDDMatch;
    procedure AddNJTaxMatch;
    procedure LoadConfigs;
    procedure CreateImportDataSets(Fname: String; Cfg: TStringList; aDataSet: TevClientDataSet; FType: Integer);
    procedure ScanForEmpLocals;
    procedure ScanForYTDLocals;
    procedure ScanForYTDT7_T16;
    procedure ScanForRecEDS;
    procedure AutoCreateMatches;
    procedure ScanForRecYTDs;
    procedure ScanForDDEDs;
    procedure ScanForEarnSets;
    procedure SaveSetupData;
    procedure Clearsetupdata;
    procedure AssignDataSources;
    procedure HasBeenMatched(sMatched, MatchType: String);
    procedure AddPartyMatch;
    procedure RunSelectImportsforSelectedCompanies;
    procedure ImportEmployees;
    function InsertClientPerson: Integer;
    function InsertEmployee(RapidTeer: Integer): Integer;
    function InsertEEStates: String;
    function InsertEERates: String;
    function InsertEELocals: String;
    procedure ImportScheduleds;
    procedure InsertScheduleds;
    procedure ImportDirectDeposits;
    procedure InsertDirectDeposits;
    procedure ImportTimeOffs;
    procedure InsertTimeOffs;
    procedure ImportYTDs;
    procedure InsertPR;
    procedure InsertPRBatch;
    procedure InsertYTDs;
    procedure CheckPrCheckStates;
    procedure CreateManualCheck(EENbr: Integer);
    procedure Create3rdPartyCheck(EENbr: Integer);
    procedure EditManualCheck(SumAmounts, SumHours: Double);
    procedure Edit3rdPartyCheck(SumAmounts, SumHours: Double);
    procedure CreateCheckLine(EDNumber: Integer; SumHours, SumAmounts : Double; EEState: Integer);
    function InsertYTDState: Integer; overload;
    function InsertYTDState(HomeNbr, SDINbr, SUINbr: Integer): Integer; overload;
    procedure HandleNYSDI(EDNumber: Integer; SumHours, SumAmounts : Double);
    procedure Handle_NJ_T7_T16(SumAmounts : Double);
    procedure ImportAutoLabor;
    procedure InsertAutoLabor;
    function FilterRapidTeer(RapidTeer: Integer; aDataSet, aDataSet2: TEvBasicClientDataSet; Division, Branch, Department: String): Boolean;
    function CompareData(aDataSet1, aDataSet2: TevClientDataSet; aField1, aField2: String): Integer;
    function GetEDNumber(aDataSet: TevClientDataSet): Integer;
    function DeterminePayFrequecy: String;
    function MappCalcMethod: String;
//    function GetCorrectEDGroupNumber: Integer;
//    function GetCorrectAgencyNumber: Integer;
    function GetEEStatesNumber(aDataSet: TevClientDataSet): Integer; Overload;
    function GetEEStatesNumber(aFieldName: String; aDataSet: TevClientDataSet): Integer; overload;
    function SumOfHours: Double;
    function SumOfAmounts: Double;
    procedure WriteToLog(ImType, CuEENo, FName, LName, Field_Name, Reason: String);
    procedure TurnTabsOff;
    procedure AddTaxes;
    function CheckForAndInsertEEStates(EENbr: Integer; HomeState, SDI, SUI: String): Integer;
    function LoadSetupFile: Boolean;
  public
    { Public declarations }
    LastClientSetup: Integer;
    procedure Activate; Override;
    procedure Deactivate; Override;
  end;

var
  DM: TDM_IM_RAPID;

implementation

uses SImportConfigs,
     SPD_EDIT_IM_VIEWLOG;


{$R *.DFM}

{ TEDIT_IM_RAPID }

procedure TEDIT_IM_RAPID.Activate;
begin
  inherited;
  Dir := '';
  evdtEffectiveStartDate.Date := Now;
  DM := TDM_IM_RAPID.Create(Nil);
  AssignDataSources;
  TurnTabsOff;
  evPageControl1.ActivePageIndex := 0;
  DM.evcsImportMaster.CreateDataSet;
  DM.LOCAL_MATCHING.CreateDataSet;
  DM.ED_MATCHING.CreateDataSet;
  DM.ED_GROUP_MATCHING.CreateDataSet;
  DM.DD_MATCHING.CreateDataSet;
  DM.PARTY_MATCHING.CreateDataSet;
  DM.NJ_TAX.CreateDataSet;
  DM.evcsEvoTaxes.CreateDataSet;
  AddTaxes;
  DM.evcsErrorLog.CreateDataSet;
  LoadConfigs;
end;

procedure TEDIT_IM_RAPID.LoadConfigs;
begin
  AutoLaborConfig := TStringList.Create;
  AutoLaborConfig.Text := ALaborCfg;

  TimeOffConfig := TStringList.Create;
  TimeOffConfig.Text := TimOffCfg;

  EmployeeConfig := TStringList.Create;
  EmployeeConfig.Text := EmpConfig;

  ScheduledEDConfig := TStringList.Create;
  ScheduledEDConfig.Text := RecEDSCfg;

  SalaryHistConfig := TStringList.Create;
  SalaryHistConfig.Text := SalHstCfg;

  YearToDateConfig := TStringList.Create;
  YearToDateConfig.Text := YearTDCfg;

  DirectDeposConfig := TStringList.Create;
  DirectDeposConfig.Text := DirDepCfg;
end;

procedure TEDIT_IM_RAPID.CreateImportDataSets(Fname: String; Cfg: TStringList; aDataSet: TevClientDataSet; FType: Integer);
type
  TRecord = record
    Field: TField;
    StartPos, StopPos: Integer;
  end;
  TRecordArray = array of TRecord;
var
  RecordArray: TRecordArray;
  procedure CreateConfigDataSets(CfgList: TStringList);
  var
    I: Integer;
    InputString: String;
  begin
    SetLength(RecordArray, cfglist.Count);
    for I := 0 to cfglist.Count - 1 do
      with RecordArray[i] do
      begin
        InputString := cfglist.values[cfglist.Names[I]];
        Field := aDataSet.FindField(cfglist.Names[I]);
        StartPos := StrToInt(Copy(InputString, 1, Pos(',', InputString)-1));
        StopPos :=  StrToInt(Copy(InputString, Pos(',', InputString)+1, Length(InputString)));
      end;
  end;
  function GetRecord(var sLineText: String; aDelimeter: String; aQualifier: String): String;
  begin

    if Pos(aQualifier+aDelimeter+aQualifier, sLineText) > 0 then
    begin
      Result := Copy(sLineText, 1, Pos(aDelimeter, sLineText)-1);
      if Trim(Result) = aQualifier+'/  /'+aQualifier then
        Result := '';
      Delete(sLineText, 1, Pos(aDelimeter, sLineText));
    end
    else
      Result := sLineText;

    Result := StringReplace(Result, aQualifier, '', [rfReplaceAll, rfIgnoreCase]);

  end;
var
  F: TextFile;
  S, S1, FileType, Delimeter, Qualifier, WaitString: String;
  I: Integer;
const
  EMPL = 836;
  YTD  = 399;
  EREC = 155;
  EDD  =  71;
  ADIS = 509;
  SHIS = 103;
  EETO = 247;

begin
  WaitString := 'Please wait while '+Fname+' file is being read... This could take as long as 2 minutes depending on the size!';
  if not FileExists(FName) then
    raise EInvalidParameters.CreateHelp('One or more of the Key files need for the import does not exist in the specified path.', IDH_BadPath);

  if not aDataSet.Active then
    aDataSet.CreateDataSet;

  if aDataSet.RecordCount = 0 then
  begin
    ctx_StartWait(WaitString);
    CreateConfigDataSets(Cfg);
    AssignFile(F, FName);
    Reset(F);

    aDataSet.DisableControls;

    try
      //FileType F = fixed length, D = delimeted
      if evrbFixed.Checked then
        FileType := 'F'
      else
      begin
        FileType := 'D';
        if evrbComma.Checked then
          Delimeter := ','
        else if evrbSemiColon.Checked then
          Delimeter := ';'
        else if evrbTab.Checked then
          Delimeter := #9
        else
          Delimeter := evedOther.Text;
      end;

      case evcbQualifier.ItemIndex of
        1: Qualifier := '''';
        2: Qualifier := '"';
      else
        Qualifier := '';
      end;

      while not System.Eof(F) do
      begin
        Readln(F, S);
        if FileType = 'F' then
        begin
          if (FType = 1) and (Length(S) <> EMPL) then
          begin
            EvMessage('The file '+Fname+' has an improper format. You must rerun the rapid dump and try again');
            Exit;
          end
          else if (FType = 2) and (Length(S) <> YTD) then
          begin
            EvMessage('The file '+Fname+' has an improper format. You must rerun the rapid dump and try again');
            Exit;
          end
          else if (FType = 3) and (Length(S) <> EREC) then
          begin
            EvMessage('The file '+Fname+' has an improper format. You must rerun the rapid dump and try again');
            Exit;
          end
          else if (FType = 5) and (Length(S) <> EDD) then
          begin
            EvMessage('The file '+Fname+' has an improper format. You must rerun the rapid dump and try again');
            Exit;
          end
          else if (FType = 4) and (Length(S) <> ADIS) then
          begin
            EvMessage('The file '+Fname+' has an improper format. You must rerun the rapid dump and try again');
            Exit;
          end
          else if (FType = 7) and (Length(S) <> SHIS) then
          begin
            EvMessage('The file '+Fname+' has an improper format. You must rerun the rapid dump and try again');
            Exit;
          end
          else if (FType = 6) and (Length(S) <> EETO) then
          begin
            EvMessage('The file '+Fname+' has an improper format. You must rerun the rapid dump and try again');
            Exit;
          end;
        end;

        aDataSet.Append;
        for i := 0 to High(RecordArray) do
        begin
          if FileType = 'F' then
          begin
            S1 := Trim(Copy(S, RecordArray[i].StartPos, RecordArray[i].StopPos));
            if S1 = '/  /' then
              S1 := '';
            RecordArray[i].Field.AsString := S1;
          end
          else
          begin
            S1 := Trim(GetRecord(S, Delimeter, Qualifier));
            if S1 = '/  /' then
              S1 := '';
            RecordArray[i].Field.AsString := S1;
          end;
        end;
        aDataSet.Post;
        ctx_UpdateWait(WaitString);
      end;
    finally
      CloseFile(F);
      ctx_EndWait;
      aDataSet.EnableControls;
    end;
  end;

end;

procedure TEDIT_IM_RAPID.TabSheet1Show(Sender: TObject);
begin
  inherited;
  evBitBtn2.Enabled := False;
  DM_TEMPORARY.TMP_CO.DataRequired('ALL');
  wwdsMaster.Active :=  True;
  evPanel2.Caption := 'Select a company/import type to setup or choose to run a previously setup import.';
  evclImportChoices.ItemEnabled[4] := False;
end;

procedure TEDIT_IM_RAPID.btbNextClick(Sender: TObject);
var
  FlagNotDoImportImmediatly : boolean;
begin
  inherited;
  FlagNotDoImportImmediatly := false;

  if (evrgNewOldSetup.ItemIndex = 1) and (evPageControl1.ActivePageIndex = 0) then
  begin
     if LoadSetupFile then
     begin
       FlagNotDoImportImmediatly := true;
       evPageControl1.ActivePageIndex := 7
     end
     else
       Exit;
  end;

  if (evrgNewOldSetup.ItemIndex = 0) and (Dir = '') then
  begin

    if (evrbComma.Checked) and (evcbQualifier.ItemIndex = 0) then
    begin
      if EvMessage('If you use only commas as delimiters, your file may not import correctly.  It is recommended that you choose another delimiter, or use quotes in addition to commas.  Do you want to continue without changing?', mtConfirmation, mbOKCancel, mbCancel) <> mrOK then
      begin
        Exit;
      end;
    end;

    Dir := RunFolderDialog('Please, select the path to import files:', Dir);

    if Length(Dir) > 0 then
    begin
      if Dir[Length(Dir)] <> '\' then
        Dir := Dir+'\';
    end
    else
      Exit;
  end;

  Application.ProcessMessages;

  with DM, DM_TEMPORARY, evclImportChoices do
  begin
    ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsInteger);
    case evPageControl1.ActivePageIndex of
      0:
      begin
        if not DM.evcsImportMaster.Locate('CUSTOM_COMPANY_NUMBER', DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').Value, []) then
        begin
          evcsImportMaster.Insert;
          evcsImportMaster.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
          evcsImportMaster.FieldByName('NAME').AsString := DM_TEMPORARY.TMP_CO.FieldByName('NAME').AsString;
          evcsImportMaster.FieldByName('CO_NBR').AsInteger := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger;
          evcsImportMaster.FieldByName('CL_NBR').AsInteger := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsInteger;
          evcsImportMaster.Post;
          LastClientSetup := evcsImportMaster.FieldByName('CL_NBR').AsInteger;
        end;
        if (Checked[0]) or (Checked[5]) then
        begin
          if Checked[0] then
          begin
            CreateImportDataSets(Dir+DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'EMPL.RDB', EmployeeConfig, evcsEmployee, 1);
            ScanForEmpLocals;
          end;
          if Checked[5] then
          begin
            CreateImportDataSets(Dir+DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'YTD.RDB', YearToDateConfig, evcsYearToDate, 2);
            ScanForYTDLocals;
            ScanForYTDT7_T16;
          end;
        end;
        evPageControl1.ActivePageIndex := 1;
      end;
      1:
      begin
        if Checked[2] then
        begin
          evcsImportMaster.Edit;
          evcsImportMaster.FieldByName('DETYPEK').AsString := '';
          evcsImportMaster.FieldByName('DETYPEQ').AsString := '';
          evcsImportMaster.Post;
          CreateImportDataSets(Dir+DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'EREC.RDB', ScheduledEDConfig, evcsRecurrings, 3);
          ScanForRecEDS;
//          AutoCreateMatches;
        end;
        if Checked[5] then
        begin
          CreateImportDataSets(Dir+DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'YTD.RDB', YearToDateConfig, evcsYearToDate, 2);
          ScanForRecYTDs;
        end;
        AutoCreateMatches;
        evPageControl1.ActivePageIndex := 2;
      end;
      2:
      begin
        if Checked[3] then
        begin
          CreateImportDataSets(Dir+DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'ADIS.RDB', AutoLaborConfig, evcsAutoLabor, 4);
          ScanForEarnSets;
        end;
        evPageControl1.ActivePageIndex := 3;
      end;
      3:
      begin
        if Checked[6] then
        begin
          CreateImportDataSets(Dir+DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'EDD.RDB', DirectDeposConfig, evcsDirectDeposits, 5);
          ScanForDDEDs;
        end;
        evPageControl1.ActivePageIndex := 4;
      end;
      4:
      begin
        evgxSchedEDSetup.Enabled := (Checked[2]);
        evgxYearToDateSetup.Enabled := (Checked[5]);
        evgxYearToDateSetup.Visible := evgxYearToDateSetup.Enabled;
        evPageControl1.ActivePageIndex := 5;
      end;
      5:
      begin
        //
        evPageControl1.ActivePageIndex := 8;
      end;
      6:
      begin
        SaveSetupData;
        evPageControl1.ActivePageIndex := 7;
      end;
      8: evPageControl1.ActivePageIndex := 6;
      7:
      begin
        case evrgSetupImport.ItemIndex of
          0:
          begin
            Clearsetupdata;
            DM.evcsRapidED.Filtered := False;
            evPageControl1.ActivePageIndex := 0;
          end;
          1:
          begin
            if not FlagNotDoImportImmediatly then
            begin
              RunSelectImportsforSelectedCompanies;
              if DM.evcsErrorLog.RecordCount > 0 then
              begin
                evbtViewLog.Enabled := True;
                if EvMessage('The exception log contains '+IntToStr(DM.evcsErrorLog.RecordCount)+' entries!  If you do not save the log before you exit this screen you will lose the log!  Would you like to view the log now?',
                                mtConfirmation, [mbYes, mbNo], mbYes) = mrYes then
                  evbtViewLog.Click;
              end
              else
                EvMessage('The imports have finished running!');
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TEDIT_IM_RAPID.TabSheet2Show(Sender: TObject);
begin
  inherited;
//  DM.evdsLocals.DataSet := DM_COMPANY.CO_LOCAL_TAX;
//  DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR='+DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString);
  evBitBtn2.Enabled := True;
  evPanel2.Caption := 'Match all rapid locals(left) to its corresponding evolution local(right).';
end;

procedure TEDIT_IM_RAPID.ScanForEmpLocals;
var
  TrueLevel, CurrentLevel: Integer;
begin

  with DM, DM.evcsEmployee do
  begin

    if not evcsRapidLocals.Active then
      evcsRapidLocals.CreateDataSet;

    evcsRapidLocals.Filtered := False;

    First;
    if FieldByName('DIVISION').AsString <> '' then
      TrueLevel := 1
    else if FieldByName('Branch').AsString <> '' then
      TrueLevel := 2
    else if FieldByName('Dept').AsString <> '' then
      TrueLevel := 3
    else
      TrueLevel := 0;
    while not Eof do
    begin

      if FieldByName('DIVISION').AsString <> '' then
        CurrentLevel := 1
      else if FieldByName('Branch').AsString <> '' then
        CurrentLevel := 2
      else if FieldByName('Dept').AsString <> '' then
        CurrentLevel := 3
      else
        CurrentLevel := 0;

      if CurrentLevel <> TrueLevel then
        raise EevException.Create('There is a mis-match in the DBDT information, import can not proceed!');

      if (FieldByName('LOCALTAXCODE1').AsString <> '') and (FieldByName('LOCALTAXSTATE1').AsString <> 'MD') then
        if not evcsRapidLocals.Locate('STATE;Local', VarArrayOf([FieldByName('LOCALTAXSTATE1').Value, FieldByName('LOCALTAXCODE1').Value]), []) then
        begin
          evcsRapidLocals.Insert;
          evcsRapidLocals.FieldByName('STATE').AsString := FieldByName('LOCALTAXSTATE1').AsString;
          evcsRapidLocals.FieldByName('Local').AsString := FieldByName('LOCALTAXCODE1').AsString;
          evcsRapidLocals.FieldByName('HASBEENMATCHED').AsString := 'F';
          evcsRapidLocals.Post;
        end;
      if (FieldByName('LOCALTAXCODE2').AsString <> '') and (FieldByName('LOCALTAXSTATE2').AsString <> 'MD') then
        if not evcsRapidLocals.Locate('STATE;Local', VarArrayOf([FieldByName('LOCALTAXSTATE2').Value, FieldByName('LOCALTAXCODE2').Value]), []) then
        begin
          evcsRapidLocals.Insert;
          evcsRapidLocals.FieldByName('STATE').AsString := FieldByName('LOCALTAXSTATE2').AsString;
          evcsRapidLocals.FieldByName('Local').AsString := FieldByName('LOCALTAXCODE2').AsString;
          evcsRapidLocals.FieldByName('HASBEENMATCHED').AsString := 'F';
          evcsRapidLocals.Post;
        end;
      if (FieldByName('LOCALTAXCODE3').AsString <> '') and (FieldByName('LOCALTAXSTATE3').AsString <> 'MD') then
        if not evcsRapidLocals.Locate('STATE;Local', VarArrayOf([FieldByName('LOCALTAXSTATE3').Value, FieldByName('LOCALTAXCODE3').Value]), []) then
        begin
          evcsRapidLocals.Insert;
          evcsRapidLocals.FieldByName('STATE').AsString := FieldByName('LOCALTAXSTATE3').AsString;
          evcsRapidLocals.FieldByName('Local').AsString := FieldByName('LOCALTAXCODE3').AsString;
          evcsRapidLocals.FieldByName('HASBEENMATCHED').AsString := 'F';
          evcsRapidLocals.Post;
        end;
      Next;
    end;
    evcsRapidLocals.Filtered := True;    
  end;
end;

procedure TEDIT_IM_RAPID.ScanForYTDLocals;
begin

  with DM, DM.evcsYearToDate do
  begin

    if not evcsRapidLocals.Active then
      evcsRapidLocals.CreateDataSet;

    if Not evcs3rdParty.Active then
       evcs3rdParty.CreateDataSet;

    evcs3rdParty.Filtered := False;

    First;
    while not Eof do
    begin
      if (FieldByName('LOCAL').AsString <> '') and (FieldByName('STATETAXCODE1').AsString <> 'MD') then
        if not evcsRapidLocals.Locate('STATE;Local', VarArrayOf([FieldByName('STATETAXCODE1').Value, FieldByName('LOCAL').Value]), []) then
        begin
          evcsRapidLocals.Insert;
          evcsRapidLocals.FieldByName('STATE').AsString := FieldByName('STATETAXCODE1').AsString;
          evcsRapidLocals.FieldByName('Local').AsString := FieldByName('LOCAL').AsString;
          evcsRapidLocals.FieldByName('HasBeenMatched').AsString := 'F';
          evcsRapidLocals.Post;
        end;

      if (FieldByName('DETYPE').AsString = '3') and (FieldByName('DET').AsString = 'D') and (not evcs3rdParty.Locate('Deduction', FieldByName('DECODE').Value, [])) then
      begin
        evcs3rdParty.Insert;
        evcs3rdParty.FieldByName('Deduction').AsString := FieldByName('DECODE').AsString;
        evcs3rdParty.FieldByName('HasBeenMatched').AsString := 'F';
        evcs3rdParty.Post;
      end;
      Next;
    end;
    evcs3rdParty.Filtered := True;
  end;

end;

procedure TEDIT_IM_RAPID.wwgdFileRowChanged(Sender: TObject);
var
  sState: String;
begin
  inherited;

  with DM do
  begin
//    if evPageControl1.ActivePageIndex = 1 then
      if (evcsRapidLocals.State <> dsInsert) and (evcsRapidLocals.RecordCount > 0) then
      begin
        sState := UpperCase(evcsRapidLocals.FieldByName('STATE').AsString);
        DM_SYSTEM_STATE.SY_STATES.DataRequired('STATE='''+sState+'''');
        with DM_SYSTEM_LOCAL.SY_LOCALS do
        begin
          DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('SY_STATES_NBR='+DM_SYSTEM_STATE.SY_STATES.FieldByName('SY_STATES_NBR').AsString);
        end;
      end;
  end;
end;

procedure TEDIT_IM_RAPID.btnAddClick(Sender: TObject);
var
  I: Integer;
  SL: TStringList;
begin
  Inherited;

  SL := TStringList.Create;
  try
    if wwgdFile.SelectedList.Count = 0 then
    begin
      EvMessage('You have not yet selected any items in the file list to match! Please do so at this time!');
      Exit;
    end;

    with DM, wwgdFile do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        evcsRapidLocals.GotoBookmark(SelectedList.items[I]);
        SL.Add(evcsRapidLocals.FieldByName('STATE').AsString+'='+evcsRapidLocals.FieldByName('LOCAL').AsString);
      end;
      wwgdFile.UnselectAll;

      for I := 0 to SL.Count - 1 do
      begin
        if (evcsRapidLocalsHasBeenMatched.AsString <> 'T') and (evcsRapidLocals.Locate('STATE;LOCAL', VarArrayOf([SL.Names[I],SL.Values[SL.Names[I]]]),[])) then
        begin
          AddLocalMatch;
          HasBeenMatched('T', 'L');
        end;
      end;

    end;
  finally
    SL.Free;
  end;
end;

procedure TEDIT_IM_RAPID.AddLocalMatch;
begin
  with DM, DM.LOCAL_MATCHING, DM_SYSTEM_LOCAL do
  begin
    Insert;
      FieldbyName('CO_NBR').AsInteger := evcsImportMaster.FieldByName('CO_NBR').AsInteger;
      FieldbyName('CL_NBR').AsInteger := evcsImportMaster.FieldByName('CL_NBR').AsInteger;
      FieldbyName('STATE').AsString := evcsRapidLocals.FieldbyName('STATE').AsString;
      FieldbyName('RAPIDLOCAL').AsString := evcsRapidLocals.FieldbyName('LOCAL').AsString;
      FieldbyName('EVOLOCAL').AsString := SY_LOCALS.FieldbyName('NAME').AsString;
      FieldbyName('LocalNbr').AsString := SY_LOCALS.FieldbyName('SY_LOCALS_NBR').AsString;
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.HasBeenMatched(sMatched, MatchType: String);
begin
  with DM do
  begin
    case MatchType[1] of
      'L':
      begin
        evcsRapidLocals.Edit;
        evcsRapidLocalsHasBeenMatched.AsString := sMatched;
        evcsRapidLocals.Post;
      end;
      'E':
      begin
        evcsRapidED.Edit;
        evcsRapidED.FieldByName('HASBEENMATCHED').AsString := sMatched;
        evcsRapidED.Post;
      end;
      'G':
      begin
        evcsRapidSpecial.Edit;
        evcsRapidSpecial.FieldByName('HASBEENMATCHED').AsString := sMatched;
        evcsRapidSpecial.Post;
      end;
      'D':
      begin
        evcsRapidDeposits.Edit;
        evcsRapidDeposits.FieldByName('HASBEENMATCHED').AsString := sMatched;
        evcsRapidDeposits.Post;
      end;
      'P':
      begin
        evcs3rdParty.Edit;
        evcs3rdParty.FieldByName('HASBEENMATCHED').AsString := sMatched;
        evcs3rdParty.Post;
      end;
      'N':
      begin
        evcsNJTaxSui.Edit;
        evcsNJTaxSui.FieldByName('HASBEENMATCHED').AsString := sMatched;
        evcsNJTaxSui.Post;
      end;
    end;
  end;
end;

procedure TEDIT_IM_RAPID.wwgdFileCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  with DM do
  begin
    if evcsRapidLocalsHasBeenMatched.AsString = 'T' then
    begin
      ABrush.Color := clAqua;
      if highlight then
        Afont.color := clblack;
    end
    else if ABrush.Color = clRed then
    begin
      ABrush.Color := clWhite;
      if highlight then
        Afont.color := clblack;
    end;
  end;
end;

procedure TEDIT_IM_RAPID.BitBtn1Click(Sender: TObject);
var
  I: Integer;
  SL: TStringList;  
begin
  inherited;

  SL := TStringList.Create;
  try
    DM.evcsRapidLocals.Filtered := False;
    with wwgdMatched, DM do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        LOCAL_MATCHING.GotoBookmark(SelectedList.items[I]);
        SL.Add(LOCAL_MATCHING.FieldByName('STATE').Value+'='+LOCAL_MATCHING.FieldByName('RapidLocal').Value);
        if evcsRapidLocals.Locate('STATE;Local', VarArrayOf([LOCAL_MATCHING.FieldByName('STATE').Value,LOCAL_MATCHING.FieldByName('RapidLocal').Value]), []) then
        begin
          HasBeenMatched('F', 'L');

        end;
      end;
      wwgdMatched.UnselectAll;

      for I := 0 to SL.Count - 1  do
      begin
        if LOCAL_MATCHING.Locate('STATE;Local', VarArrayOf([SL.Names[I], SL.Values[SL.Names[I]]]), []) then
          LOCAL_MATCHING.Delete;
      end;
    end;
  finally
    DM.evcsRapidLocals.Filtered := True;
    SL.Free;
  end;

{var
  I: Integer;
  SL: TStringList;
begin
  inherited;
  SL := TStringList.Create;
  try
    DM.evcsRapidED.Filtered := False;
    with DM, wwgdEDMatched do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        ED_MATCHING.GotoBookmark(SelectedList.items[I]);
        SL.Add(ED_MATCHING.FieldByName('RapidED').AsString);
        if evcsRapidED.Locate('RapidED', ED_MATCHING.FieldByName('RapidED').Value, []) then
        begin
          HasBeenMatched('F', 'E');
        end;
      end;
      wwgdEDMatched.UnselectAll;

      for I := 0 to SL.Count - 1  do
      begin
        if ED_MATCHING.Locate('RapidED', SL[I], []) then
          ED_MATCHING.Delete;
      end;
    end;
  finally
    DM.evcsRapidED.Filtered := True;
    SL.Free;
  end;}

end;

procedure TEDIT_IM_RAPID.ScanForRecEDS;
var
  RapED, Tmp1, Tmp2, Tmp3: String;
begin

  with DM, DM.evcsRecurrings do
  begin

    evcsRapidED.Filtered := False;
    evcsRapidSpecial.Filtered := False;

    if not evcsRapidED.Active then
      evcsRapidED.CreateDataSet;
    if not evcsRapidSpecial.Active then
      evcsRapidSpecial.CreateDataSet;

    First;
    while not EoF do
    begin
      if (FieldbyName('DET').AsString = 'E') or (FieldByName('DET').AsString = 'D') then
      begin
        RapED := Trim(FieldByName('DECODE').AsString);
        if Length(RapED) = 1 then
          RapED := '0'+RapED;
        RapED := FieldbyName('DET').AsString+RapED;
        if not evcsRapidED.Locate('RapidED', RapED, []) then
        begin
          evcsRapidED.Insert;
          evcsRapidED.FieldByName('RapidED').AsString := RapED;
          evcsRapidED.FieldByName('HasBeenMatched').AsString := 'F';
          evcsRapidED.Post;
        end;
      end;

      Tmp1 := FieldByName('SPECIALCODE').AsString;
      Tmp2 := FieldByName('PERCENTCODE').AsString;
      Tmp3 := FieldByName('DETYPE').AsString;
      if Tmp1 <> '' then
        if (Tmp1[1] in ['A'..'Z']) or (Tmp1[1] in ['a'..'z']) then
        begin
          if not evcsRapidSpecial.Locate('SPECIALORPERCENT', Tmp1, []) then
          begin
            evcsRapidSpecial.Insert;
            evcsRapidSpecial.FieldByName('SPECIALORPERCENT').AsString := Tmp1;
            evcsRapidSpecial.FieldByName('HasBeenMatched').AsString := 'F';
            evcsRapidSpecial.Post;
          end;
        end;
      if Tmp2 <> '' then
        if (Tmp1 = '') and (Tmp2[1] in ['A'..'Z']) or (Tmp2[1] in ['a'..'z']) then
        begin
          if not evcsRapidSpecial.Locate('SPECIALORPERCENT', Tmp2, []) then
          begin
            evcsRapidSpecial.Insert;
            evcsRapidSpecial.FieldByName('SPECIALORPERCENT').AsString := Tmp2;
            evcsRapidSpecial.FieldByName('HasBeenMatched').AsString := 'F';
            evcsRapidSpecial.Post;
          end;
        end;
      if Tmp3 <> '' then
        if (Tmp3 = 'K') or (Tmp3 = 'Q') or (Tmp3 = 'X') then
          if not evcsRapidSpecial.Locate('SPECIALORPERCENT', Tmp3, []) then
          begin
            evcsRapidSpecial.Insert;
            evcsRapidSpecial.FieldByName('SPECIALORPERCENT').AsString := Tmp3;
            evcsRapidSpecial.FieldByName('HasBeenMatched').AsString := 'F';
            evcsRapidSpecial.Post;
          end;
      Next;
    end;
    evcsRapidED.Filtered := True;
    evcsRapidSpecial.Filtered := True;
  end;

  DM_CLIENT.CL_E_DS.DataRequired('ALL');
  RapED := RapED;
{  KTypeFound := False;
  QTypeFound := False;
  if FieldByName('DETYPE').AsString  = 'K' then
    KTypeFound := True;
  if FieldByName('DETYPE').AsString  = 'Q' then
    QTypeFound := True;}
end;

procedure TEDIT_IM_RAPID.AutoCreateMatches;
begin
 dm.evcsRapidED.Filtered := False;
// DM_CLIENT.CL_E_DS.DataRequired('E_D_CODE_TYPE<>'+QuotedStr('EO')+' and E_D_CODE_TYPE<>'+QuotedStr('EP')+' and E_D_CODE_TYPE<>'+QuotedStr('EQ'));
 if DM.evcsRapidED.Active then
 begin
    with DM, DM.evcsRapidED, DM_CLIENT do
    begin
      first;
      while not Eof do
      begin
        if (FieldByName('HasBeenMatched').AsString = 'F') and (CL_E_DS.Locate('CUSTOM_E_D_CODE_NUMBER', FieldByName('RapidED').Value, [])) then
        begin
          ED_MATCHING.Insert;
          ED_MATCHING.FieldByName('CL_NBR').AsInteger := evcsImportMaster.FieldByName('CL_NBR').AsInteger;
          ED_MATCHING.FieldByName('CO_NBR').AsInteger := evcsImportMaster.FieldByName('CO_NBR').AsInteger;
          ED_MATCHING.FieldByName('RapidED').AsString := FieldByName('RapidED').AsString;
          ED_MATCHING.FieldByName('EvoED').AsString := CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
          ED_MATCHING.FieldByName('CL_E_DS_NBR').AsInteger := CL_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
          ED_MATCHING.FieldByName('DESCRIPTION').AsString := CL_E_DS.FieldByName('DESCRIPTION').AsString;
          ED_MATCHING.Post;
          HasBeenMatched('T', 'E');
        end;
        next;
      end;
    end;
  end;
  dm.evcsRapidED.Filtered := True;
end;

procedure TEDIT_IM_RAPID.BitBtn2Click(Sender: TObject);
var
  I: Integer;
  SL: TStringList;
begin
  inherited;

  SL := TStringList.Create;
  try
    with DM, wwgdRapidED do
    begin
      if SelectedList.Count = 0 then
      begin
        EvMessage('You have not yet selected any items in the file list to match! Please do so at this time!');
        Exit;
      end;

      for I := 0 to SelectedList.Count - 1 do
      begin
         evcsRapidED.GotoBookmark(SelectedList.items[I]);
         SL.Add(evcsRapidED.FieldByName('RapidED').AsString);
      end;
      UnselectAll;

      for I := 0 to SL.Count - 1 do
      begin
        if (evcsRapidEDHasBeenMatched.AsString <> 'T') and (evcsRapidED.Locate('RapidED', SL[I],[])) then
        begin
          AddEDMatch;
          HasBeenMatched('T', 'E');
        end;
      end;

    end;
  finally
    SL.Free;
  end;
end;

procedure TEDIT_IM_RAPID.AddEDMatch;
begin
  with DM, DM.ED_MATCHING, DM_CLIENT do
  begin
    Insert;
    ED_MATCHING.FieldByName('CL_NBR').AsInteger := evcsImportMaster.FieldByName('CL_NBR').AsInteger;
    ED_MATCHING.FieldByName('CO_NBR').AsInteger := evcsImportMaster.FieldByName('CO_NBR').AsInteger;
    FieldByName('RapidED').AsString := evcsRapidED.FieldByName('RapidED').AsString;
    FieldByName('EvoED').AsString := CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
    FieldByName('CL_E_DS_NBR').AsInteger := CL_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
    FieldByName('DESCRIPTION').AsString := CL_E_DS.FieldByName('DESCRIPTION').AsString;
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.wwgdRapidEDCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  if DM.evcsRapidEDHasBeenMatched.AsString = 'T' then
  begin
    ABrush.Color := clAqua;
    if highlight then
      Afont.color := clblack;
  end
  else if ABrush.Color = clRed then
  begin
    ABrush.Color := clWhite;
    if highlight then
      Afont.color := clblack;
  end;
end;

procedure TEDIT_IM_RAPID.btnRemoveClick(Sender: TObject);
var
  I: Integer;
  SL: TStringList;
begin
  inherited;
  SL := TStringList.Create;
  try
    DM.evcsRapidED.Filtered := False;
    with DM, wwgdEDMatched do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        ED_MATCHING.GotoBookmark(SelectedList.items[I]);
        SL.Add(ED_MATCHING.FieldByName('RapidED').AsString);
        if evcsRapidED.Locate('RapidED', ED_MATCHING.FieldByName('RapidED').Value, []) then
        begin
          HasBeenMatched('F', 'E');
        end;
      end;
      wwgdEDMatched.UnselectAll;

      for I := 0 to SL.Count - 1  do
      begin
        if ED_MATCHING.Locate('RapidED', SL[I], []) then
          ED_MATCHING.Delete;
      end;
    end;
  finally
    DM.evcsRapidED.Filtered := True;
    SL.Free;
  end;
end;

procedure TEDIT_IM_RAPID.ScanForRecYTDs;
var
  RapED: String;
begin

  with DM, DM.evcsYearToDate do
  begin
    evcsRapidED.Filtered := False;

    if not evcsRapidED.Active then
      evcsRapidED.CreateDataSet;

    First;
    while not EoF do
    begin
      if (FieldbyName('DET').AsString = 'E') or (FieldByName('DET').AsString = 'D') and (FieldByName('DETYPE').AsString <> '3') then
      begin
        RapED := Trim(FieldByName('DECODE').AsString);
        if Length(RapED) = 1 then
          RapED := '0'+RapED;
        RapED := FieldbyName('DET').AsString+RapED;
        if not evcsRapidED.Locate('RapidED', RapED, []) then
        begin
          evcsRapidED.Insert;
          evcsRapidED.FieldByName('RapidED').AsString := RapED;
          evcsRapidED.FieldByName('HasBeenMatched').AsString := 'F';
          evcsRapidED.Post;
        end;
      end;
      Next;
    end;
    evcsRapidED.Filtered := True;
  end;

  DM_CLIENT.CL_E_DS.DataRequired('ALL');
//  RapED := RapED;

end;

procedure TEDIT_IM_RAPID.TabSheet5Show(Sender: TObject);
begin
  inherited;
  DM_CLIENT.CL_E_D_GROUPS.DataRequired('ALL');
  evPanel2.Caption := 'Match all rapid Special/Percent Codes(left) to its corresponding evolution E/D Group(right).';
end;

procedure TEDIT_IM_RAPID.BitBtn3Click(Sender: TObject);
var
 I: Integer;
 SL: TStringList;
begin
  inherited;

  SL := TStringList.Create;
  try
    if wwDBGrid1.SelectedList.Count = 0 then
    begin
      EvMessage('You have not yet selected any items in the file list to match! Please do so at this time!');
      Exit;
    end;

    with DM, wwDBGrid1 do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        evcsRapidSpecial.GotoBookmark(SelectedList.items[I]);
        SL.Add(evcsRapidSpecial.FieldByName('SpecialOrPercent').AsString);
      end;
      UnselectAll;

      for I := 0 to SL.Count - 1 do
      begin
        if (evcsRapidSpecialHasBeenMatched.AsString <> 'T') and (evcsRapidSpecial.Locate('SpecialOrPercent', SL[I],[])) then
        begin
          AddEDGroupMatch;
          HasBeenMatched('T', 'G');
        end;
      end;

    end;
  finally
    SL.Free;
  end;
end;

procedure TEDIT_IM_RAPID.AddEDGroupMatch;
begin
  with DM, DM.ED_GROUP_MATCHING, DM_CLIENT do
  begin
    Insert;
    FieldByName('CL_NBR').AsInteger := evcsImportMaster.FieldByName('CL_NBR').AsInteger;
    FieldByName('CO_NBR').AsInteger := evcsImportMaster.FieldByName('CO_NBR').AsInteger;    
    FieldByName('SpecialOrPercent').AsString := evcsRapidSpecial.FieldByName('SpecialOrPercent').AsString;
    FieldByName('NAME').AsString := CL_E_D_GROUPS.FieldByName('NAME').AsString;
    FieldByName('CL_E_D_GROUPS_NBR').AsInteger := CL_E_D_GROUPS.FieldByName('CL_E_D_GROUPS_NBR').AsInteger;
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.BitBtn4Click(Sender: TObject);
var
  I: Integer;
  SL: TStringList;  
begin
  inherited;

  SL := TStringList.Create;
  try
    DM.evcsRapidSpecial.Filtered := False;

    with DM, wwDBGrid3 do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        ED_GROUP_MATCHING.GotoBookmark(SelectedList.items[I]);
        SL.Add(ED_GROUP_MATCHING.FieldByName('SpecialOrPercent').AsString);
        if evcsRapidSpecial.Locate('SpecialOrPercent', ED_GROUP_MATCHING.FieldByName('SpecialOrPercent').Value, []) then
        begin
          HasBeenMatched('F', 'G');
        end;
      end;
      UnselectAll;

      for I := 0 to SL.Count - 1  do
      begin
        if ED_GROUP_MATCHING.Locate('SpecialOrPercent', SL[I], []) then
          ED_GROUP_MATCHING.Delete;
      end;

    end;
  finally
    DM.evcsRapidSpecial.Filtered := True;
  end;
end;

procedure TEDIT_IM_RAPID.wwDBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  with DM do
  begin
    if evcsRapidSpecialHasBeenMatched.AsString = 'T' then
    begin
      ABrush.Color := clAqua;
      if highlight then
        Afont.color := clblack;
    end
    else if ABrush.Color = clRed then
    begin
      ABrush.Color := clWhite;
      if highlight then
        Afont.color := clblack;
    end;
  end;
end;

procedure TEDIT_IM_RAPID.TabSheet6Show(Sender: TObject);
begin
  inherited;
  DM_CLIENT.CL_E_DS.DataRequired('E_D_CODE_TYPE='+QuotedStr('D1'));
  evPanel2.Caption := 'Match all rapid dir. deps.(left) to its corresponding evolution E/D(right).';
end;

procedure TEDIT_IM_RAPID.ScanForDDEDs;
begin
  with DM, DM.evcsDirectDeposits do
  begin

    if not evcsRapidDeposits.Active then
      evcsRapidDeposits.CreateDataSet;

    evcsRapidDeposits.Filtered := False;

    First;
    while not EoF do
    begin
      if not evcsRapidDeposits.Locate('DECODE', FieldByName('DECODE').AsString, []) then
      begin
        evcsRapidDeposits.Insert;
        evcsRapidDeposits.FieldByName('DECODE').AsString := FieldByName('DECODE').AsString;
        evcsRapidDeposits.FieldByName('HasBeenMatched').AsString := 'F';
        evcsRapidDeposits.Post;
      end;
      Next;
    end;
    evcsRapidDeposits.Filtered := True;
  end;

end;

procedure TEDIT_IM_RAPID.BitBtn5Click(Sender: TObject);
var
  I: Integer;
  SL: TStringList;
begin
  Inherited;

  SL := TStringList.Create;
  try
    if wwDBGrid4.SelectedList.Count = 0 then
    begin
      EvMessage('You have not yet selected any items in the file list to match! Please do so at this time!');
      Exit;
    end;

    with DM, wwDBGrid4 do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        evcsRapidDeposits.GotoBookmark(SelectedList.items[I]);
        SL.Add(evcsRapidDeposits.FieldByName('DECODE').AsString);
      end;
      UnselectAll;
      for I := 0 to SL.Count - 1 do
      begin
        if (evcsRapidDepositsHasBeenMatched.AsString <> 'T') and (evcsRapidDeposits.Locate('DECODE', SL[I],[])) then
        begin
          AddDDMatch;
          HasBeenMatched('T', 'D');
        end;
      end;
    end;
  finally
    SL.Free;
  end;
end;

procedure TEDIT_IM_RAPID.AddDDMatch;
begin
  with DM, DM.DD_MATCHING, DM_CLIENT do
  begin
    Insert;
    FieldByName('CL_NBR').AsInteger := evcsImportMaster.FieldByName('CL_NBR').AsInteger;
    FieldByName('CO_NBR').AsInteger := evcsImportMaster.FieldByName('CO_NBR').AsInteger;    
    FieldByName('DECODE').AsString := evcsRapidDeposits.FieldByName('DECODE').AsString;
    FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString := CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
    FieldByName('CL_E_DS_NBR').AsInteger := CL_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
    FieldByName('DESCRIPTION').AsString := CL_E_DS.FieldByName('DESCRIPTION').AsString;
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.BitBtn6Click(Sender: TObject);
var
  I: Integer;
  SL: TStringList;
begin
  inherited;

  SL := TStringList.Create;
  try
    DM.evcsRapidDeposits.Filtered := False;
    with DM, wwDBGrid6 do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        DD_MATCHING.GotoBookmark(SelectedList.items[I]);
        SL.Add(DD_MATCHING.FieldByName('DECODE').AsString);
        if evcsRapidDeposits.Locate('DECODE', DD_MATCHING.FieldByName('DECODE').Value, []) then
        begin
          HasBeenMatched('F', 'D');
        end;
      end;
      UnselectAll;

      for I := 0 to SL.Count - 1  do
      begin
        if DD_MATCHING.Locate('DECODE', SL[I], []) then
          DD_MATCHING.Delete;
      end;
    end;
  finally
    DM.evcsRapidDeposits.Filtered := True;
    SL.Free;
  end;

end;

procedure TEDIT_IM_RAPID.wwDBGrid4CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  with DM do
  begin
    if evcsRapidDepositsHasBeenMatched.AsString = 'T' then
    begin
      ABrush.Color := clAqua;
      if highlight then
        Afont.color := clblack;
    end
    else if ABrush.Color = clRed then
    begin
      ABrush.Color := clWhite;
      if highlight then
        Afont.color := clblack;
    end;
  end;
end;

procedure TEDIT_IM_RAPID.SaveSetupData;
begin
  with DM.evcsImportMaster do
  begin
    Edit;
//    DM.evcsImportMaster.FieldByName('StandardAverageHours').AsString := evgpAverageStandardHrous.Items.Values[evgpAverageStandardHrous.Items.Names[evgpAverageStandardHrous.ItemIndex]];
    case evgpAverageStandardHrous.ItemIndex of
      0: DM.evcsImportMaster.FieldByName('StandardAverageHours').AsString := 'Y';
      1: DM.evcsImportMaster.FieldByName('StandardAverageHours').AsString := 'N';
    end;
    if evgxSchedEDSetup.Enabled then
    begin
      DM.evcsImportMaster.FieldByName('DETYPEK').AsInteger := evgxTrreatK.ItemIndex;
      DM.evcsImportMaster.FieldByName('DETYPEQ').AsInteger := evgxTrreatQ.ItemIndex;
      DM.evcsImportMaster.FieldByName('DETYPEX').AsInteger := evgxTrreatX.ItemIndex;
      DM.evcsImportMaster.FieldByName('EFFECTIVE_START_DATE').AsDateTime := evdtEffectiveStartDate.Date;
    end;
    if evgxYearToDateSetup.Enabled then
    begin
      DM.evcsImportMaster.FieldByName('CHECK_DATE').AsDateTime := evdtCheckDate.Date;
      DM.evcsImportMaster.FieldByName('PERIOD_BEGIN_DATE').AsDateTime := evdtPeriodBegin.Date;
      DM.evcsImportMaster.FieldByName('PERIOD_END_DATE').AsDateTime := evdtPeriodEnd.Date;
      with evCheckListBox2 do
      begin
        DM.evcsImportMaster.FieldByName('JAN').AsString := BoolToYN(Checked[0]);
        DM.evcsImportMaster.FieldByName('FEB').AsString := BoolToYN(Checked[1]);
        DM.evcsImportMaster.FieldByName('MAR').AsString := BoolToYN(Checked[2]);
        DM.evcsImportMaster.FieldByName('APR').AsString := BoolToYN(Checked[3]);
        DM.evcsImportMaster.FieldByName('MAY').AsString := BoolToYN(Checked[4]);
        DM.evcsImportMaster.FieldByName('JUN').AsString := BoolToYN(Checked[5]);
        DM.evcsImportMaster.FieldByName('JUL').AsString := BoolToYN(Checked[6]);
        DM.evcsImportMaster.FieldByName('AUG').AsString := BoolToYN(Checked[7]);
        DM.evcsImportMaster.FieldByName('SEP').AsString := BoolToYN(Checked[8]);
        DM.evcsImportMaster.FieldByName('OCT').AsString := BoolToYN(Checked[9]);
        DM.evcsImportMaster.FieldByName('NOV').AsString := BoolToYN(Checked[10]);
        DM.evcsImportMaster.FieldByName('DEC').AsString := BoolToYN(Checked[11]);
      end;
    end;
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.evComboBox1Change(Sender: TObject);
var
  I: Integer;
begin
  inherited;

  for I := 0  to evCheckListBox2.Items.Count - 1 do
  begin
    evCheckListBox2.Checked[I] := False;
  end;

  case evComboBox1.ItemIndex of
    0:
    begin
      for I := 0  to evCheckListBox2.Items.Count - 1 do
      begin
        evCheckListBox2.Checked[I] := True;
      end;
    end;
    1:
    begin
      for I := 0  to 2 do
      begin
        evCheckListBox2.Checked[I] := True;
      end;
    end;
    2:
    begin
      for I := 3  to 5 do
      begin
        evCheckListBox2.Checked[I] := True;
      end;
    end;
    3:
    begin
      for I := 6  to 8 do
      begin
        evCheckListBox2.Checked[I] := True;
      end;
    end;
    4:
    begin
      for I := 9  to 11 do
      begin
        evCheckListBox2.Checked[I] := True;
      end;
    end;
    5:
    begin
      for I := 0  to 5 do
      begin
        evCheckListBox2.Checked[I] := True;
      end;
    end;
    6:
    begin
      for I := 6  to 11 do
      begin
        evCheckListBox2.Checked[I] := True;
      end;
    end;
    7: evCheckListBox2.Enabled := True;
  end;
end;

procedure TEDIT_IM_RAPID.TabSheet3Show(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  if DM.evcsImportMaster.FieldByName('CHECK_DATE').IsNull then
  begin
    evdtCheckDate.DateTime := now;
    evdtPeriodBegin.DateTime := GetBeginMonth(now);
    evdtPeriodEnd.DateTime := now;
    evComboBox1.ItemIndex := 0;
  end;
  evCheckListBox2.Enabled := False;
  for I := 0  to evCheckListBox2.Items.Count - 1 do
  begin
    evCheckListBox2.Checked[I] := True;
  end;
  evPanel2.Caption := 'Select appropriate options for year to dates or scheduled E/Ds';
end;

procedure TEDIT_IM_RAPID.evBitBtn3Click(Sender: TObject);
var
  F: String;
  I: IevDualStream;
begin
  inherited;

  if sdlgSaveSetup.Execute then
    F := sdlgSaveSetup.FileName
  else
    Exit;


  I := TEvDualStreamHolder.CreateInMemory;
  I.WriteStream(DM.evcsImportMaster.GetDataStream(False, True));
  I.WriteStream(DM.LOCAL_MATCHING.GetDataStream(False, True));
  I.WriteStream(DM.ED_MATCHING.GetDataStream(False, True));
  I.WriteStream(DM.ED_GROUP_MATCHING.GetDataStream(False, True));
  I.WriteStream(DM.DD_MATCHING.GetDataStream(False, True));
  I.WriteStream(DM.PARTY_MATCHING.GetDataStream(False, True));
  I.WriteStream(DM.NJ_TAX.GetDataStream(False, True));

  I.SaveToFile(F);


//  if sdlgSaveSetup.Execute then
//    DM.evcsImportMaster.SaveToFile(sdlgSaveSetup.FileName);
end;

procedure TEDIT_IM_RAPID.Clearsetupdata;
begin
  with DM do
  begin
    if evcsRapidED.Active then
      evcsRapidED.EmptyDataSet;
    if evcsRapidSpecial.Active then
      evcsRapidSpecial.EmptyDataSet;
    if evcsRapidLocals.Active then
      evcsRapidLocals.EmptyDataSet;
    if evcsRapidDeposits.Active then
      evcsRapidDeposits.EmptyDataSet;
    if evcsEmployee.Active then
      evcsEmployee.EmptyDataSet;
    if evcsYearToDate.Active then
      evcsYearToDate.EmptyDataSet;
    if evcsHistory1.Active then
      evcsHistory1.EmptyDataSet;
    if evcsHistory2.Active then
      evcsHistory2.EmptyDataSet;
    if evcsRecurrings.Active then
      evcsRecurrings.EmptyDataSet;
    if evcsDirectDeposits.Active then
      evcsDirectDeposits.EmptyDataSet;
    if evcsTimeOff.Active then
      evcsTimeOff.EmptyDataSet;
    Dir := '';
  end;
end;

procedure TEDIT_IM_RAPID.TabSheet7Show(Sender: TObject);
begin
  inherited;
  evBitBtn3.Enabled := True;
  evPanel2.Caption := 'Setup another company for batch import, save setup or run imports now.';
  evclImportsToRun.ItemEnabled[5] := False;
  evrgSetupImport.ItemIndex := 1;
end;

procedure TEDIT_IM_RAPID.evPageControl1Change(Sender: TObject);
begin
  inherited;
  with evPageControl1 do
  begin
    evBitBtn3.Enabled := (ActivePageIndex = 6);
  end;
end;

procedure TEDIT_IM_RAPID.AssignDataSources;
begin
  //Attach grid data sources here
  wwgdFile.DataSource := DM.evdsRapidLocals;
end;

procedure TEDIT_IM_RAPID.evBitBtn2Click(Sender: TObject);
begin
  inherited;
  evPageControl1.ActivePageIndex := evPageControl1.ActivePageIndex - 1;
end;

procedure TEDIT_IM_RAPID.TabSheet4Show(Sender: TObject);
begin
  inherited;
//  DM_CLIENT.CL_E_DS.DataRequired('E_D_CODE_TYPE<>'+QuotedStr('EO')+' and E_D_CODE_TYPE<>'+QuotedStr('EP')+' and E_D_CODE_TYPE<>'+QuotedStr('EQ'));
  evPanel2.Caption := 'Match all rapid E/Ds(left) to its corresponding evolution E/Ds(right).';
end;

procedure TEDIT_IM_RAPID.RunSelectImportsforSelectedCompanies;
var
  I: Integer;
begin

  if Dir = '' then
    Dir := RunFolderDialog('Please, select the path to import files:', Dir);

  if Dir[Length(Dir)] <> '\' then
    Dir := Dir+'\';

  for I := 0 to wwDBGrid7.SelectedList.Count - 1 do
  begin
    DM.evcsImportMaster.GotoBookmark(wwDBGrid7.SelectedList.items[I]);
    ctx_DataAccess.OpenClient(DM.evcsImportMaster.FieldByName('CL_NBR').AsInteger);
    DM_COMPANY.CO.DataRequired('CO_NBR='+DM.evcsImportMaster.FieldByName('CO_NBR').AsString);

    if evclImportsToRun.Checked[0] then
    begin
      if (wwDBGrid7.SelectedList.Count > 1) and (dm.evcsEmployee.State <> dsInactive) then
        dm.evcsEmployee.EmptyDataSet;
      CreateImportDataSets(Dir+DM.evcsImportMaster.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'EMPL.RDB', EmployeeConfig, dm.evcsEmployee, 1);
      if dm.evcsEmployee.RecordCount <> 0 then
        ImportEmployees;
    end;
    if evclImportsToRun.Checked[1] then
    begin
      if (wwDBGrid7.SelectedList.Count > 1) and (dm.evcsDirectDeposits.State <> dsInactive) then
        dm.evcsDirectDeposits.EmptyDataSet;
      CreateImportDataSets(Dir+DM.evcsImportMaster.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'EDD.RDB', DirectDeposConfig, dm.evcsDirectDeposits, 5);
      if dm.evcsDirectDeposits.RecordCount <> 0 then
      ImportDirectDeposits;
    end;
    if evclImportsToRun.Checked[2] then
    begin
      if (wwDBGrid7.SelectedList.Count > 1)  and (dm.evcsRecurrings.State <> dsInactive) then
        dm.evcsRecurrings.EmptyDataSet;
      CreateImportDataSets(Dir+DM.evcsImportMaster.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'EREC.RDB', ScheduledEDConfig, dm.evcsRecurrings, 3);
      if DM.evcsRecurrings.RecordCount <> 0 then
        ImportScheduleds;
    end;
    if evclImportsToRun.Checked[3] then
    begin
      if (wwDBGrid7.SelectedList.Count > 1) and (dm.evcsTimeOff.State <> dsInactive) then
        dm.evcsTimeOff.EmptyDataSet;
      CreateImportDataSets(Dir+DM.evcsImportMaster.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'EETO.RDB', TimeOffConfig, dm.evcsTimeOff, 6);
      if dm.evcsTimeOff.RecordCount <> 0 then
        ImportTimeOffs;
    end;
    if evclImportsToRun.Checked[4] then
    begin
      if (wwDBGrid7.SelectedList.Count > 1) and (dm.evcsAutoLabor.State <> dsInactive) then
        dm.evcsAutoLabor.EmptyDataSet;
      CreateImportDataSets(Dir+DM.evcsImportMaster.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'ADIS.RDB', AutoLaborConfig, dm.evcsAutoLabor, 4);
      if dm.evcsAutoLabor.RecordCount <> 0 then
        ImportAutoLabor;
    end;
    if evclImportsToRun.Checked[6] then
    begin
      if (wwDBGrid7.SelectedList.Count > 1) and (dm.evcsYearToDate.State <> dsInactive) then
        dm.evcsYearToDate.EmptyDataSet;
      CreateImportDataSets(Dir+DM.evcsImportMaster.FieldByName('CUSTOM_COMPANY_NUMBER').AsString+'YTD.RDB', YearToDateConfig, dm.evcsYearToDate, 2);
      if dm.evcsYearToDate.RecordCount <> 0 then
        ImportYTDs;
    end;
    {
      ImportSalaryHistories;
    }
  end;
end;

procedure TEDIT_IM_RAPID.ImportScheduleds;
var
  MyTransManager: TTransactionManager;
begin
  MyTransManager := TTransactionManager.Create(Nil);
  MyTransManager.Initialize([DM_EMPLOYEE.EE_SCHEDULED_E_DS]);
  with DM, DM_CLIENT, DM_COMPANY, DM_EMPLOYEE do
  begin
    ctx_DataAccess.Importing := True;
    CL_E_DS.OpenWithLookups := False;
    CL_E_D_GROUPS.OpenWithLookups := False;
    CO.OpenWithLookups := False;
    CO_DIVISION.OpenWithLookups := False;
    CO_BRANCH.OpenWithLookups := False;
    CO_DEPARTMENT.OpenWithLookups := False;
    CO_TEAM.OpenWithLookups := False;
    EE.OpenWithLookups := False;
    EE_SCHEDULED_E_DS.OpenWithLookups := False;

    CL_E_DS.DisableControls;
    CL_E_D_GROUPS.DisableControls;
    CO.DisableControls;
    CO_DIVISION.DisableControls;
    CO_BRANCH.DisableControls;
    CO_DEPARTMENT.DisableControls;
    CO_TEAM.DisableControls;
    EE.DisableControls;
    EE_SCHEDULED_E_DS.DisableControls;

    CL_E_DS.DataRequired('ALL');
    CL_E_D_GROUPS.DataRequired('ALL');
    CO.DataRequired('CO_NBR='+dm.evcsImportMaster.FieldByName('CO_NBR').AsString);
    CO_DIVISION.DataRequired('');
    CO_BRANCH.DataRequired('');
    CO_DEPARTMENT.DataRequired('');
    CO_TEAM.DataRequired('');
    EE.DataRequired('CO_NBR='+dm.evcsImportMaster.FieldByName('CO_NBR').AsString);
    EE_SCHEDULED_E_DS.DataRequired('ALL');

    if evcsRecurrings.RecordCount <> 0 then
      try
        evcsRecurrings.First;
        ctx_StartWait('Please wait while scheduled e/d information is being imported.', DM.evcsRecurrings.RecordCount-1);
        ctx_UpdateWait('Please wait while demographic information is being imported for '+DM.evcsRecurrings.FieldByName('EMPL').AsString+'.', DM.evcsRecurrings.RecNo);

        while not evcsRecurrings.Eof do
        begin
          InsertScheduleds;
          evcsRecurrings.Next;
          ctx_UpdateWait('Please wait while demographic information is being imported for '+DM.evcsRecurrings.FieldByName('EMPL').AsString+'.', DM.evcsRecurrings.RecNo);
        end;
        ctx_UpdateWait('Please Wait while the imported data is being committed!');
        MyTransManager.ApplyUpdates;
      finally
        CO.OpenWithLookups := True;
        CO_DIVISION.OpenWithLookups := True;
        CO_BRANCH.OpenWithLookups := True;
        CO_DEPARTMENT.OpenWithLookups := True;
        CO_TEAM.OpenWithLookups := True;
        EE.OpenWithLookups := True;
        CL_E_DS.OpenWithLookups := True;;
        CL_E_D_GROUPS.OpenWithLookups := True;

        CL_E_DS.EnableControls;
        CL_E_D_GROUPS.EnableControls;
        CO.EnableControls;
        CO_DIVISION.EnableControls;
        CO_BRANCH.EnableControls;
        CO_DEPARTMENT.EnableControls;
        CO_TEAM.EnableControls;
        EE.EnableControls;
        EE_SCHEDULED_E_DS.EnableControls;

        MyTransManager.Free;
        ctx_DataAccess.Importing := False;
        ctx_EndWait;
      end;

  end;
end;

//Returns 0 if cl_person was created properly, 1 if employee already exists, 2 if there was an error
function TEDIT_IM_RAPID.InsertClientPerson: Integer;
var
 Initial, temp: String;
 Year, Month, Day: Word;
 V: Variant;
begin
  with DM, DM_CLIENT.CL_PERSON do
  begin

    Result := 0;
  //    EEDesc := CreateEmployeeToLog(evcsEmployee.FieldByName('custom_employee_nbr').AsString, evcsEmployee.FieldByName('First_Name').AsString, evcsEmployee.FieldByName('Last_Name').AsString);

    if (Locate('SOCIAL_SECURITY_NUMBER', evcsEmployee.FieldByName('ssi').AsString, [])) then
    begin
      Result := 1;
      Exit;
    end
    else
    begin

      if (evcsEmployee.FieldByName('ssi').AsString = '') then
      begin
       //'Social Security Number is empty...'
       WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                  evcsEmployee.FieldByName('LNAME').AsString, 'SOCIAL_SECURITY_NUMBER', 'Field is empty in import file.');
       Result := 2;
       Exit;
      end;

      insert;

      FieldByName('social_security_number').Value := evcsEmployee.FieldByName('ssi').AsString;
                               //LowerCase()
      temp := CapitalizeWords(evcsEmployee.FieldByName('FName').AsString);
      if Pos(' ', temp) > 0 then
      begin
        Initial := Copy(temp, Pos(' ', temp) + 1, 1);
        System.delete(temp, Pos(' ', temp), Length(temp));
      end;

      if evcsEmployee.FieldByName('LName').AsString <> '' then     //LowerCase()
        FieldByName('Last_Name').Value := CapitalizeWords(evcsEmployee.FieldByName('LName').AsString)
      else
      begin
        FieldByName('Last_Name').Value := 'Need LName';
  //      Log(EEDesc, 'Last_Name is missing.');
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'LAST_NAME', 'Field is empty in import file.');
      end;


      if Temp <> '' then
        FieldByName('First_Name').Value := temp
      else
      begin
        FieldByName('First_Name').Value := 'Need FName';
  //      Log(EEDesc, 'First_Name is missing.');
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'FIRST_NAME', 'Field is empty in import file.')
      end;

      if Initial <> '' then
        FieldByName('Middle_Initial').Value := Initial
      else
        FieldByName('Middle_Initial').Clear;

      if evcsEmployee.FieldByName('address1').AsString <> '' then  //LowerCase()
        FieldByName('address1').Value := CapitalizeWords(evcsEmployee.FieldByName('address1').AsString)
      else
      begin
        FieldByName('address1').Value := 'Need Address1';
  //      Log(EEDesc, 'address1 is missing.');
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'ADDRESS1', 'Field is empty in import file.')
      end;
                                                      //LowerCase()
      FieldByName('address2').Value := CapitalizeWords(evcsEmployee.FieldByName('address2').AsString);

      if evcsEmployee.FieldByName('city').AsString <> '' then  // LowerCase()
        FieldByName('city').Value := CapitalizeWords(evcsEmployee.FieldByName('city').AsString)
      else
      begin
        FieldByName('city').Value := DM_COMPANY.CO.FieldByName('city').AsString;
  //      Log(EEDesc, 'city is missing.');
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'CITY', 'Field is empty in import file.')
      end;

      if evcsEmployee.FieldByName('state').AsString <> '' then
      begin
        FieldByName('state').Value := evcsEmployee.FieldByName('state').AsString;
        v := DM_SYSTEM_STATE.SY_STATES.Lookup('STATE', evcsEmployee.FieldByName('state').AsString, 'SY_STATES_NBR');
        if not VarIsNull(V) then
          FieldByName('RESIDENTIAL_STATE_NBR').Value := V;
      end
      else
      begin
        FieldByName('state').Value := DM_COMPANY.CO.FieldByName('state').AsString;
  //      Log(EEDesc, 'state is missing.');
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'STATE', 'Field is empty in import file.')
      end;

      if evcsEmployee.FieldByName('zip').AsString <> '' then
        FieldByName('zip_code').Value := evcsEmployee.FieldByName('zip').AsString
      else
      begin
        FieldByName('zip_code').Value := DM_COMPANY.CO.FieldByName('zip_code').AsString;
  //      Log(EEDesc, 'zip_code is missing.');
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'ZIP_CODE', 'Field is empty in import file.')
      end;

      if evcsEmployee.FieldByName('sex').AsString = '' then
      begin
        FieldByName('gender').Value := GROUP_BOX_UNKOWN;
      end
      else
        FieldByName('gender').Value := evcsEmployee.FieldByName('sex').AsString;

      temp := evcsEmployee.FieldByName('birthdate').AsString;
      if (temp <> '') and (temp[1] <> ' ') then
      begin

        Temp := FormatDateTime('YY', evcsEmployee.FieldByName('birthdate').AsDateTime);

        if StrToInt(Temp) <= 10 then
          Temp := '20'+Temp
        else
          Temp := '19'+Temp;

        DecodeDate(evcsEmployee.FieldByName('birthdate').AsDateTime, Year, Month, Day);

        FieldByName('birth_date').AsDateTime := EncodeDate(StrToInt(Temp), Month, Day);

//        FieldByName('birth_date').AsString := evcsEmployee.FieldByName('birthdate').AsString;
      end;
      FieldByName('ein_or_social_security_number').Value := GROUP_BOX_SSN;
      FieldByName('smoker').Value := GROUP_BOX_NO;
      FieldByName('I9_on_file').Value := GROUP_BOX_YES;
      FieldByName('w2_name_suffix').Value := '';
      FieldByName('ethnicity').Value := ETHNIC_NOT_APPLICATIVE;
      FieldByName('veteran').Value := GROUP_BOX_NOT_APPLICATIVE;
      FieldByName('visa_type').Value := VISA_TYPE_NONE;
      FieldByName('vietnam_veteran').Value := GROUP_BOX_NOT_APPLICATIVE;
      FieldByName('disabled_veteran').Value := GROUP_BOX_NOT_APPLICATIVE;
      FieldByName('military_reserve').Value := GROUP_BOX_NOT_APPLICATIVE;
      temp := evcsEmployee.FieldByName('phone').AsString;
      if (temp <> '') and (temp <> '(   )    -') and (temp[2] <> '') then
      begin
        while Pos(' ', temp) > 0 do
          System.Delete(temp, Pos(' ', temp), 1);
        FieldByName('phone1').Value := temp;
      end;
      post;
    end;
  end;
end;


function TEDIT_IM_RAPID.InsertEELocals: String;
begin

  with DM, DM_EMPLOYEE.EE_LOCALS, DM_EMPLOYEE, DM_COMPANY, DM_SYSTEM_LOCAL do
  begin
    if evcsEmployee.FieldByName('LOCALTAXCODE1').AsString <> '' then
      if LOCAL_MATCHING.Locate('STATE;RapidLocal', VarArrayOf([evcsEmployee.FieldByName('Localtaxstate1').Value, evcsEmployee.FieldByName('LOCALTAXCODE1').Value]), []) then
      begin
        //Fitler by company also
        CO_LOCAL_TAX.DataRequired('SY_LOCALS_NBR='''+LOCAL_MATCHING.FieldByName('LocalNbr').AsString+'''');
        CO_LOCAL_TAX.Filter := 'CO_NBR='+DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString;
        CO_LOCAL_TAX.Filtered := True;
        if DM_COMPANY.CO_LOCAL_TAX.RecordCount <> 0 then
        begin
          if not EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EE.FieldByName('EE_NBR').AsInteger, CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger]), []) then
          begin
            Insert;
            FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;
            FieldByName('CO_LOCAL_TAX_NBR').AsInteger := CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
            if (evcsEmployee.FieldByName('LOCALTAXPERCENT1').AsString <> '0.00') and (evcsEmployee.FieldByName('LOCALTAXPERCENT1').AsString <> '') then
            begin
              FieldByName('OVERRIDE_LOCAL_TAX_VALUE').AsFloat := evcsEmployee.FieldByName('LOCALTAXPERCENT1').AsFloat;
              FieldByName('OVERRIDE_LOCAL_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_PERCENT;
            end
            else
              FieldByName('OVERRIDE_LOCAL_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
            FieldByName('EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
            FieldByName('DEDUCT').AsString := GROUP_BOX_YES;
            Post;
          end;
        end;
      end;

    if evcsEmployee.FieldByName('LOCALTAXCODE2').AsString <> '' then
      if LOCAL_MATCHING.Locate('STATE;RapidLocal', VarArrayOf([evcsEmployee.FieldByName('Localtaxstate2').Value, evcsEmployee.FieldByName('LOCALTAXCODE2').Value]), []) then
      begin
        //Fitler by company also
        CO_LOCAL_TAX.DataRequired('SY_LOCALS_NBR='''+LOCAL_MATCHING.FieldByName('LocalNbr').AsString+'''');
        CO_LOCAL_TAX.Filter := 'CO_NBR='+DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString;
        CO_LOCAL_TAX.Filtered := True;
        if DM_COMPANY.CO_LOCAL_TAX.RecordCount <> 0 then
        begin
          if not EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EE.FieldByName('EE_NBR').AsInteger, CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger]), []) then
          begin
            Insert;
            FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;
            FieldByName('CO_LOCAL_TAX_NBR').AsInteger := CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
            if (evcsEmployee.FieldByName('LOCALTAXPERCENT2').AsString <> '0.00') and (evcsEmployee.FieldByName('LOCALTAXPERCENT2').AsString <> '') then
            begin
              FieldByName('OVERRIDE_LOCAL_TAX_VALUE').AsFloat := evcsEmployee.FieldByName('LOCALTAXPERCENT2').AsFloat;
              FieldByName('OVERRIDE_LOCAL_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_PERCENT;
            end
            else
              FieldByName('OVERRIDE_LOCAL_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
            FieldByName('EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
            FieldByName('DEDUCT').AsString := GROUP_BOX_YES;
            Post;
          end;
        end;
      end;

    if evcsEmployee.FieldByName('LOCALTAXCODE3').AsString <> '' then
      //Fitler by company also
      if LOCAL_MATCHING.Locate('STATE;RapidLocal', VarArrayOf([evcsEmployee.FieldByName('Localtaxstate3').Value, evcsEmployee.FieldByName('LOCALTAXCODE3').Value]), []) then
      begin
        CO_LOCAL_TAX.DataRequired('SY_LOCALS_NBR='''+LOCAL_MATCHING.FieldByName('LocalNbr').AsString+'''');
        CO_LOCAL_TAX.Filter := 'CO_NBR='+DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString;
        CO_LOCAL_TAX.Filtered := True;
        if DM_COMPANY.CO_LOCAL_TAX.RecordCount <> 0 then
        begin
          if not EE_LOCALS.Locate('EE_NBR;CO_LOCAL_TAX_NBR', VarArrayOf([EE.FieldByName('EE_NBR').AsInteger, CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger]), []) then
          begin
            Insert;
            FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;
            FieldByName('CO_LOCAL_TAX_NBR').AsInteger := CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
            if (evcsEmployee.FieldByName('LOCALTAXPERCENT3').AsString <> '0.00') and (evcsEmployee.FieldByName('LOCALTAXPERCENT3').AsString <> '') then
            begin
              FieldByName('OVERRIDE_LOCAL_TAX_VALUE').AsFloat := evcsEmployee.FieldByName('LOCALTAXPERCENT3').AsFloat;
              FieldByName('OVERRIDE_LOCAL_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_PERCENT;
            end
            else
              FieldByName('OVERRIDE_LOCAL_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
            FieldByName('EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
            FieldByName('DEDUCT').AsString := GROUP_BOX_YES;
            Post;
          end;
        end;
      end;
     CO_LOCAL_TAX.Filtered := False;
  end;

end;

function TEDIT_IM_RAPID.InsertEERates: String;
var
  Tmp: String;
  RapidTeer: Integer;
  V: Variant;
begin

  Tmp := '0.00';
  RapidTeer := 0;

  with DM, DM_EMPLOYEE, DM_EMPLOYEE.EE_RATES, DM_COMPANY do
  begin
    if not Locate('EE_NBR;RATE_NUMBER', VarArrayOf([EE.FieldByName('ee_nbr').Value, 1]), []) then
    begin
      Insert;

      FieldByName('ee_nbr').Value := EE.FieldByName('ee_nbr').AsString;

      FieldByName('primary_rate').AsString := GROUP_BOX_YES;
      FieldByName('rate_number').AsInteger := 1;

      Tmp := FormatFloat('0.00', StrToFloat(Trim(evcsEmployee.FieldByName('Rate1').AsString)));
      if Tmp <> '0.00' then
        FieldByName('rate_amount').Value :=  evcsEmployee.FieldByName('Rate1').AsFloat
      else
        FieldByName('rate_amount').Value := 0;

      V := CO_STATES.Lookup('STATE', evcsEmployee.FieldByName('Statetaxcode1').Value, 'CO_STATES_NBR');
      if not VarIsNull(V) then
        if evcsEmployee.FieldByName('Workcomp').AsString <> '' then
          if CO_WORKERS_COMP.Locate('WORKERS_COMP_CODE;CO_STATES_NBR', VarArrayOf([evcsEmployee.FieldByName('Workcomp').Value, V]), []) then
             FieldByName('CO_WORKERS_COMP_NBR').AsInteger := CO_WORKERS_COMP.FieldByName('CO_WORKERS_COMP_NBR').AsInteger
          else
             WriteToLog('EE RATES', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'Workers Comp', 'Works comp code '+evcsEmployee.FieldByName('Workcomp').AsString+' is not set up on the company level for '+evcsEmployee.FieldByName('Statetaxcode1').AsString+', please verify company is setup properly.');
      post;
    end;

    Tmp := FormatFloat('0.00', StrToFloat(Trim(evcsEmployee.FieldByName('Rate2').AsString)));

    if Tmp <> '0.00' then
    begin
      if not Locate('EE_NBR;RATE_NUMBER', VarArrayOf([EE.FieldByName('ee_nbr').Value, 2]), []) then
      begin

        Insert;
        FieldByName('ee_nbr').Value := EE.FieldByName('ee_nbr').AsString;
        FieldByName('primary_rate').Value := GROUP_BOX_NO;
        FieldByName('rate_number').Value := 2;

        if evcsEmployee.FieldByName('Rate2').AsString <> '' then
          FieldByName('rate_amount').Value := evcsEmployee.FieldByName('Rate2').AsFloat
        else
        begin
          Cancel;
          Exit;
        end;

        if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString <> CLIENT_LEVEL_COMPANY then
        begin
          if (evcsEmployee.FieldByName('Ovdivision1').AsString <> '') or (evcsEmployee.FieldByName('Ovbranch1').AsString <> '') or (evcsEmployee.FieldByName('Ovdept1').AsString <> '') then
          begin
            if DM.evcsEmployee.FieldByName('Ovdivision1').AsString <> '' then
              RapidTeer := 1
            else if DM.evcsEmployee.FieldByName('Ovbranch1').AsString <> '' then
              RapidTeer := 2
            else if DM.evcsEmployee.FieldByName('Ovdept1').AsString <> '' then
              RapidTeer := 3;

            if not FilterRapidTeer(RapidTeer, DM_EMPLOYEE.EE_RATES, evcsEmployee, 'Ovdivision1', 'Ovbranch1', 'Ovdept1') then
            begin
              Cancel;
              WriteToLog('EE Rate 2', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                     evcsEmployee.FieldByName('LNAME').AsString, 'DBDT', 'Unable to resolve override DBDT information, please verify company is setup properly.');
              exit;
            end;
          end;
        end;
        post;
      end;
    end;

    Tmp := FormatFloat('0.00', StrToFloat(Trim(evcsEmployee.FieldByName('Rate3').AsString)));

    if Tmp <> '0.00' then
    begin
      if not Locate('EE_NBR;RATE_NUMBER', VarArrayOf([EE.FieldByName('ee_nbr').Value, 3]), []) then
      begin

        Insert;
        FieldByName('ee_nbr').Value := DM_EMPLOYEE.EE.FieldByName('ee_nbr').AsString;
        FieldByName('primary_rate').Value := GROUP_BOX_NO;
        FieldByName('rate_number').AsInteger := 3;

        if evcsEmployee.FieldByName('Rate3').AsString <> '' then
          FieldByName('rate_amount').Value := evcsEmployee.FieldByName('Rate3').AsFloat
        else
        begin
          Cancel;
          Exit;
        end;

        if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString <> CLIENT_LEVEL_COMPANY then
        begin

          if (evcsEmployee.FieldByName('Ovdivision2').AsString <> '') or (evcsEmployee.FieldByName('Ovbranch2').AsString <> '') or (evcsEmployee.FieldByName('Ovdept2').AsString <> '') then
          begin
            if DM.evcsEmployee.FieldByName('Ovdivision2').AsString <> '' then
              RapidTeer := 1
            else if DM.evcsEmployee.FieldByName('Ovbranch2').AsString <> '' then
              RapidTeer := 2
            else if DM.evcsEmployee.FieldByName('Ovdept2').AsString <> '' then
              RapidTeer := 3;

            if not FilterRapidTeer(RapidTeer, DM_EMPLOYEE.EE_RATES, evcsEmployee, 'Ovdivision2', 'Ovbranch2', 'Ovdept2') then
            begin
              cancel;
              WriteToLog('EE Rate 3', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                     evcsEmployee.FieldByName('LNAME').AsString, 'DBDT', 'Unable to resolve override DBDT information, please verify company is setup properly.');
              exit;
            end;
          end;
        end;
        post;
      end;
    end;

    Tmp := FormatFloat('0.00', StrToFloat(Trim(evcsEmployee.FieldByName('Rate4').AsString)));

    if Tmp <> '0.00' then
    begin
      if not Locate('EE_NBR;RATE_NUMBER', VarArrayOf([EE.FieldByName('ee_nbr').Value, 4]), []) then
      begin

        Insert;
        FieldByName('ee_nbr').Value := EE.FieldByName('ee_nbr').AsString;
        FieldByName('primary_rate').Value := GROUP_BOX_NO;
        FieldByName('rate_number').AsFloat := 4;

        if evcsEmployee.FieldByName('Rate4').AsString <> '' then
          FieldByName('rate_amount').Value := evcsEmployee.FieldByName('Rate4').AsFloat
        else
        begin
          Cancel;
          Exit;
        end;

        if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString <> CLIENT_LEVEL_COMPANY then
        begin

          if (evcsEmployee.FieldByName('Ovdivision3').AsString <> '') or (evcsEmployee.FieldByName('Ovbranch3').AsString <> '') or (evcsEmployee.FieldByName('Ovdept3').AsString <> '') then
          begin
            if DM.evcsEmployee.FieldByName('Ovdivision3').AsString <> '' then
              RapidTeer := 1
            else if DM.evcsEmployee.FieldByName('Ovbranch3').AsString <> '' then
              RapidTeer := 2
            else if DM.evcsEmployee.FieldByName('Ovdept3').AsString <> '' then
              RapidTeer := 3;

            if not FilterRapidTeer(RapidTeer, DM_EMPLOYEE.EE_RATES, evcsEmployee, 'Ovdivision3', 'Ovbranch3', 'Ovdept3') then
            begin
              cancel;
              WriteToLog('EE Rate 4', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                         evcsEmployee.FieldByName('LNAME').AsString, 'DBDT', 'Unable to resolve override DBDT information, please verify company is setup properly.');
              exit;
            end;
          end;
        end;
        post;
      end;
    end;

    Tmp := FormatFloat('0.00', StrToFloat(Trim(evcsEmployee.FieldByName('Rate5').AsString)));

    if Tmp <> '0.00' then
    begin
      if not Locate('EE_NBR;RATE_NUMBER', VarArrayOf([EE.FieldByName('ee_nbr').Value, 5]), []) then
      begin

        Insert;
        FieldByName('ee_nbr').Value := EE.FieldByName('ee_nbr').AsString;
        FieldByName('primary_rate').Value := 'N';
        FieldByName('rate_number').AsInteger := 5;

        if evcsEmployee.FieldByName('Rate5').AsString <> '' then
          FieldByName('rate_amount').Value := evcsEmployee.FieldByName('Rate5').AsFloat
        else
        begin
          Cancel;
          Exit;
        end;

        if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString <> CLIENT_LEVEL_COMPANY then
        begin

          if (evcsEmployee.FieldByName('Ovdivision4').AsString <> '') or (evcsEmployee.FieldByName('Ovbranch4').AsString <> '') or (evcsEmployee.FieldByName('Ovdept4').AsString <> '') then
          begin
            if DM.evcsEmployee.FieldByName('Ovdivision4').AsString <> '' then
              RapidTeer := 1
            else if DM.evcsEmployee.FieldByName('Ovbranch4').AsString <> '' then
              RapidTeer := 2
            else if DM.evcsEmployee.FieldByName('Ovdept4').AsString <> '' then
              RapidTeer := 3;

            if not FilterRapidTeer(RapidTeer, DM_EMPLOYEE.EE_RATES, evcsEmployee, 'Ovdivision4', 'Ovbranch4', 'Ovdept4') then
            begin
              cancel;
              WriteToLog('EE Rate 5', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                     evcsEmployee.FieldByName('LNAME').AsString, 'DBDT', 'Unable to resolve override DBDT information, please verify company is setup properly.');
              exit;
            end;
          end;
        end;
        post;
      end;
    end;
  end;
end;

function TEDIT_IM_RAPID.InsertEEStates: String;
var
  V: Variant;
  SecState: String;
  StateMarital: String;
begin
  with DM, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE, DM_COMPANY do
  begin
    Insert;

    FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;

    V := CO_STATES.Lookup('STATE', evcsEmployee.FieldByName('Statetaxcode1').Value, 'CO_STATES_NBR');
    if NOT VARISNULL(V) then
      FieldByName('CO_STATES_NBR').AsInteger := V
    else
    begin
      Cancel;
      WriteToLog('Employee States', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                 evcsEmployee.FieldByName('LNAME').AsString, 'CO_STATES_NBR', 'The state '+evcsEmployee.FieldByName('Statetaxcode1').AsString+' is not setup in evolution.');
      Exit;
    end;

    StateMarital := FindMaritalStatus(evcsEmployee.FieldByName('STATETAXCODE1').AsString, evcsEmployee.FieldByName('Smaritalstatus').AsString,
                                                                      evcsEmployee.FieldByName('ExtraOptions8').AsString, evcsEmployee.FieldByName('LOCALTAXDESC1').AsString,
                                                                      evcsEmployee.FieldByName('SDependents').AsInteger);

    FieldByName('STATE_MARITAL_STATUS').AsString := StateMarital;

    if FieldByName('STATE_MARITAL_STATUS').AsString = '' then
    begin
      Cancel;
      WriteToLog('Employee States', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                 evcsEmployee.FieldByName('LNAME').AsString, 'STATE_MARITAL_STATUS', 'There was a problem determining the state marital status for '+evcsEmployee.FieldByName('Statetaxcode1').AsString+'.');
      Exit;
    end
    else
    begin
      if Length(StateMarital) = 1 then
        StateMarital := StateMarital + ' ';
      V := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', FieldByName('CO_STATES_NBR').AsVariant, 'SY_STATES_NBR');
      if not VarIsNull(V) then
      begin
        V := DM_SYSTEM_STATE.SY_STATE_MARITAL_STATUS.Lookup('SY_STATES_NBR;STATUS_TYPE', VarArrayOf([V, StateMarital]), 'SY_STATE_MARITAL_STATUS_NBR');
        if not VarIsNull(V) then
          FieldByName('SY_STATE_MARITAL_STATUS_NBR').AsVariant := V
        else
        begin
          Cancel;
          WriteToLog('Employee States', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                 evcsEmployee.FieldByName('LNAME').AsString, 'STATE_MARITAL_STATUS', 'There was a problem determining the state marital status for '+evcsEmployee.FieldByName('Statetaxcode1').AsString+'.');
          Exit;
        end;
      end;
    end;

    FieldByName('IMPORTED_MARITAL_STATUS').Assign(DM_EMPLOYEE.EE.FieldByName('FEDERAL_MARITAL_STATUS'));
    FieldByName('STATE_NUMBER_WITHHOLDING_ALLOW').AsInteger := evcsEmployee.FieldByName('Sdependents').asInteger;
    FieldByName('CALCULATE_TAXABLE_WAGES_1099').AsString := GROUP_BOX_NO;

    if evcsEmployee.FieldByName('Taxexempt6').AsString = 'Y' then FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsEmployee.FieldByName('Taxexempt6').AsString = 'B' then FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsEmployee.FieldByName('Taxexempt6').AsString = 'N' then FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if  evcsEmployee.FieldByName('Additional2').AsFloat <> 0 then
    begin
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT;
      FieldByName('OVERRIDE_STATE_TAX_VALUE').AsFloat :=  evcsEmployee.FieldByName('Additional2').AsFloat;
    end
    else if  evcsEmployee.FieldByName('Additional4').AsFloat <> 0 then
    begin
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT;
      FieldByName('OVERRIDE_STATE_TAX_VALUE').AsFloat := evcsEmployee.FieldByName('Additional4').AsFloat;
    end
    else if  evcsEmployee.FieldByName('Voluntary2').AsFloat <> 0 then
    begin
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
      FieldByName('OVERRIDE_STATE_TAX_VALUE').AsFloat := evcsEmployee.FieldByName('Voluntary2').AsFloat;
    end
    else if  evcsEmployee.FieldByName('Voluntary4').AsFloat <> 0 then
    begin
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_PERCENT;
      FieldByName('OVERRIDE_STATE_TAX_VALUE').AsFloat := evcsEmployee.FieldByName('Voluntary4').AsFloat;
    end
    else
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;

    if evcsEmployee.FieldByName('Statetaxcode2').AsString <> '' then
    begin
      V := DM_COMPANY.CO_STATES.Lookup('STATE', evcsEmployee.FieldByName('Statetaxcode2').Value, 'CO_STATES_NBR');
      if NOT VARISNULL(V) then
        FieldByName('SDI_APPLY_CO_STATES_NBR').AsInteger := V
      else
      begin
        Cancel;
        WriteToLog('Employee States', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'CO_STATES_NBR', 'The state '+evcsEmployee.FieldByName('Statetaxcode2').AsString+' is not setup in evolution.');
        Exit;
      end;
    end;

    if evcsEmployee.FieldByName('Statetaxcode3').AsString <> '' then
    begin
      V := DM_COMPANY.CO_STATES.Lookup('STATE', evcsEmployee.FieldByName('Statetaxcode3').Value, 'CO_STATES_NBR');
      if NOT VARISNULL(V) then
        FieldByName('SUI_APPLY_CO_STATES_NBR').AsInteger := V
      else
      begin
        Cancel;
        WriteToLog('Employee States', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'CO_STATES_NBR', 'The state '+evcsEmployee.FieldByName('Statetaxcode3').AsString+' is not setup in evolution.');
        Exit;
      end;
    end;

    if evcsEmployee.FieldByName('Taxexempt7').AsString = 'Y' then FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsEmployee.FieldByName('Taxexempt7').AsString = 'B' then FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsEmployee.FieldByName('Taxexempt7').AsString = 'N' then FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if evcsEmployee.FieldByName('Taxexempt16').AsString = 'Y' then FieldByName('ER_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsEmployee.FieldByName('Taxexempt16').AsString = 'B' then FieldByName('ER_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsEmployee.FieldByName('Taxexempt16').AsString = 'N' then FieldByName('ER_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if evcsEmployee.FieldByName('Taxexempt8').AsString = 'Y' then FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsEmployee.FieldByName('Taxexempt8').AsString = 'B' then FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsEmployee.FieldByName('Taxexempt8').AsString = 'N' then FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if evcsEmployee.FieldByName('Taxexempt17').AsString = 'Y' then FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXEMPT
    else if evcsEmployee.FieldByName('Taxexempt17').AsString = 'B' then FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE
    else if evcsEmployee.FieldByName('Taxexempt17').AsString = 'N' then FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;

    if evcsEmployee.FieldByName('Extraoptions6').AsString = '' then FieldByName('RECIPROCAL_METHOD').AsString := STATE_RECIPROCAL_TYPE_NONE
    else if evcsEmployee.FieldByName('Extraoptions6').AsString = '1' then FieldByName('RECIPROCAL_METHOD').AsString :=  STATE_RECIPROCAL_TYPE_DIFFERENCE
    else if evcsEmployee.FieldByName('Extraoptions6').AsString = '2' then FieldByName('RECIPROCAL_METHOD').AsString :=  STATE_RECIPROCAL_TYPE_FULL
    else if evcsEmployee.FieldByName('Extraoptions6').AsString = '3' then
    begin
      FieldByName('RECIPROCAL_METHOD').AsString :=  STATE_RECIPROCAL_TYPE_FLAT;//flat
      FieldByName('RECIPROCAL_AMOUNT_PERCENTAGE').AsFloat := evcsEmployee.FieldByName('Voluntary2').AsFloat;
    end
    else if evcsEmployee.FieldByName('Extraoptions6').AsString = '4' then
    begin
      FieldByName('RECIPROCAL_METHOD').AsString :=  STATE_RECIPROCAL_TYPE_PERCENTAGE;//percent
      FieldByName('RECIPROCAL_AMOUNT_PERCENTAGE').AsFloat := evcsEmployee.FieldByName('Voluntary4').AsFloat;
    end;

    if (evcsEmployee.FieldByName('Extraoptions4').AsString <> '') and (evcsEmployee.FieldByName('Extraoptions5').AsString <> '') then
    begin
      SecState := evcsEmployee.FieldByName('Extraoptions4').AsString+evcsEmployee.FieldByName('Extraoptions5').AsString;
      V := DM_COMPANY.CO_STATES.Lookup('STATE', SecState, 'CO_STATES_NBR');
      if NOT VARISNULL(V) then
        FieldByName('RECIPROCAL_CO_STATES_NBR').AsInteger := V
      else
      begin
        Cancel;
        WriteToLog('Employee States', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'CO_STATES_NBR', 'The state '+evcsEmployee.FieldByName('Extraoptions4').AsString+evcsEmployee.FieldByName('Extraoptions5').AsString+' is not setup in evolution.');
        Exit;
      end;
    end;
    Post;
  end;
end;

function TEDIT_IM_RAPID.InsertEmployee(RapidTeer: Integer): Integer;
var
  temp: String;
  V: Variant;
begin
  with DM, DM_EMPLOYEE.EE, DM_CLIENT do
  begin
    Result := 0;
    if Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(evcsEmployee.FieldByName('Emp').Value, ' ', 9), []) then
    begin
      Result := 1;
      Exit
    end
    else
    begin
      DM_EMPLOYEE.EE.Tag := 1;
      if not CL_PERSON.Locate('social_security_number', evcsEmployee.FieldByName('Ssi').Value, []) then
      begin
        Result := 2;
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, '', 'Could not locate a person with this social security number'+evcsEmployee.FieldByName('Ssi').AsString+'.');
        Exit;
      end;

      insert;

      FieldByName('CL_PERSON_NBR').AsInteger := CL_PERSON.FieldByName('CL_PERSON_NBR').AsInteger;
      FieldByName('CO_NBR').AsString := DM_COMPANY.CO.FieldByName('CO_NBR').AsString;

      if evcsEmployee.FieldByName('Emp').AsString <> '' then
        FieldByName('custom_employee_number').AsString := PadStringLeft(evcsEmployee.FieldByName('Emp').AsString, ' ', 9)
      else
      begin
        FieldByName('custom_employee_number').AsString := 'Need ID';
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'CUSTOM_EMPLOYEE_NUMBER', 'Field is empty in import file.');
      end;

      if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString <> CLIENT_LEVEL_COMPANY then
      begin
        if DM.evcsEmployee.FieldByName('DIVISION').AsString <> '' then
          RapidTeer := 1
        else if DM.evcsEmployee.FieldByName('Branch').AsString <> '' then
          RapidTeer := 2
        else if DM.evcsEmployee.FieldByName('Dept').AsString <> '' then
          RapidTeer := 3
        else
          RapidTeer := 0;

        if not FilterRapidTeer(RapidTeer, DM_EMPLOYEE.EE, evcsEmployee, 'Division', 'Branch', 'Dept') then
        begin
          Cancel;
          Result := 2;
          WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, '', 'There was a problem with D/B/D/T information. Please verify that the company is setup correctly.');
          Exit;
        end;
      end;

      temp := evcsEmployee.FieldByName('Hiredate').AsString;
      if (temp <> '') and (temp[1] <> ' ') then
      begin
        FieldByName('current_hire_date').AsDateTime := StrToDate(evcsEmployee.FieldByName('Hiredate').AsString);
        FieldByName('original_hire_date').AsDateTime := StrToDate(evcsEmployee.FieldByName('Hiredate').AsString);
      end
      else
        FieldByName('current_hire_date').AsDateTime := now;

      if evcsImportMaster.FieldByName('StandardAverageHours').AsString = 'Y' then
        FieldByName('standard_hours').AsString := evcsEmployee.FieldByName('Averagehours').AsString;

      temp := Trim(evcsEmployee.FieldByName('Payfreq').AsString);
      if temp = '' then
      begin
        Cancel;
        Result := 2;
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'PAY_FREQUENCY', 'Field is empty in import file.');
        Exit;
      end;

      if temp = '52' then
        FieldByName('pay_frequency').AsString := FREQUENCY_TYPE_WEEKLY
      else if temp = '26' then
        FieldByName('pay_frequency').AsString := FREQUENCY_TYPE_BIWEEKLY
      else if temp = '12' then
        FieldByName('pay_frequency').AsString := FREQUENCY_TYPE_MONTHLY
      else if temp = '24' then
        FieldByName('pay_frequency').AsString := FREQUENCY_TYPE_SEMI_MONTHLY;

      if evcsEmployee.FieldByName('exempt').AsString = '' then
        FieldByName('flsa_exempt').AsString := GROUP_BOX_NOT_APPLICATIVE
      else
        FieldByName('flsa_exempt').AsString := evcsEmployee.FieldByName('exempt').AsString;

      temp := evcsEmployee.FieldByName('Termdate').AsString;
      if (temp <> '') and (temp[1] <> ' ') then
        FieldByName('current_termination_date').AsDateTime := StrToDate(evcsEmployee.FieldByName('Termdate').AsString);

      temp := evcsEmployee.FieldByName('Nextincdate').AsString;
      if (temp <> '') and (temp[1] <> ' ') then
        FieldByName('next_raise_date').AsDateTime := StrToDate(evcsEmployee.FieldByName('Nextincdate').AsString);

      FieldByName('time_clock_number').AsString := evcsEmployee.FieldByName('Clocknumber').AsString;
      FieldByName('group_term_policy_amount').AsFloat := evcsEmployee.FieldByName('extrareal').AsFloat;

      temp := evcsEmployee.FieldByName('TermCode').AsString;
      if temp = '' then
        FieldByName('current_termination_code').AsString := EE_TERM_ACTIVE;
      if temp = 'T' then
        FieldByName('current_termination_code').AsString := EE_TERM_TERMINATED;
      if temp = 'L' then
        FieldByName('current_termination_code').AsString := EE_TERM_LEAVE;


      temp := evcsEmployee.FieldByName('Ftpt').AsString;
      if temp = '' then
        FieldByName('POSITION_STATUS').AsString := POSITION_STATUS_NA;
      if temp = 'FT' then
        FieldByName('POSITION_STATUS').AsString := POSITION_STATUS_FULL;
      if temp = 'PT' then
        FieldByName('POSITION_STATUS').AsString := POSITION_STATUS_PART;

      V := CO_STATES.Lookup('STATE', evcsEmployee.FieldByName('Statetaxcode1').Value, 'CO_STATES_NBR');
      if not VarIsNull(V) then
        if evcsEmployee.FieldByName('Workcomp').AsString <> '' then
          if CO_WORKERS_COMP.Locate('WORKERS_COMP_CODE;CO_STATES_NBR', VarArrayOf([evcsEmployee.FieldByName('Workcomp').Value, V]), []) then
             FieldByName('CO_WORKERS_COMP_NBR').AsInteger := CO_WORKERS_COMP.FieldByName('CO_WORKERS_COMP_NBR').AsInteger;

      if evcsEmployee.FieldByName('Position').AsString <> '' then
       if CO_HR_POSITIONS.Locate('DESCRIPTION', VarArrayOf([evcsEmployee.FieldByName('Position').Value]), []) then
          FieldByName('CO_HR_POSITIONS_NBR').AsInteger := CO_HR_POSITIONS.FieldByName('CO_HR_POSITIONS_NBR').AsInteger;

      FieldByName('Tipped_Directly').AsString := GROUP_BOX_NO;
      FieldByName('Distribute_Taxes').AsString := DM_COMPANY.CO.FieldByName('DISTRIBUTE_DEDUCTIONS_DEFAULT').AsString;
      if FieldByName('Distribute_Taxes').AsString = CO_DISTRIBUTE_BOTH then
        FieldByName('Distribute_Taxes').AsString := DISTRIBUTE_BOTH;

      FieldByName('W2_Type').AsString := W2_TYPE_FEDERAL;
      FieldByName('W2_Pension').AsString := GROUP_BOX_NO;
      FieldByName('w2_deferred_comp').AsString := GROUP_BOX_NO;
      FieldByName('w2_deceased').AsString := GROUP_BOX_NO;
      FieldByName('w2_statutory_employee').AsString := GROUP_BOX_NO;
      FieldByName('w2_legal_rep').AsString := GROUP_BOX_NO;
      FieldByName('TAX_AMT_DETERMINED_1099R').AsString := GROUP_BOX_NO;
      FieldByName('TOTAL_DISTRIBUTION_1099R').AsString := GROUP_BOX_NO;
      FieldByName('PENSION_PLAN_1099R').AsString := PENSION_PLAN_1099R_NONE;
      FieldByName('MAKEUP_FICA_ON_CLEANUP_PR').AsString := GROUP_BOX_NO;

      if evcsEmployee.FieldByName('Taxexempt1').AsString = 'Y' then FieldByName('exempt_exclude_ee_fed').AsString := GROUP_BOX_EXEMPT
      else if evcsEmployee.FieldByName('Taxexempt1').AsString = 'B' then FieldByName('exempt_exclude_ee_fed').AsString := GROUP_BOX_EXCLUDE
      else if evcsEmployee.FieldByName('Taxexempt1').AsString = 'N' then FieldByName('exempt_exclude_ee_fed').AsString := GROUP_BOX_INCLUDE;

      if (evcsEmployee.FieldByName('Taxexempt2').AsString = '') or (evcsEmployee.FieldByName('Taxexempt2').AsString = 'N') then FieldByName('exempt_employee_oasdi').AsString := GROUP_BOX_NO
      else if evcsEmployee.FieldByName('Taxexempt2').AsString = 'Y' then FieldByName('exempt_employee_oasdi').AsString := GROUP_BOX_YES;

      if (evcsEmployee.FieldByName('Taxexempt3').AsString = '') or (evcsEmployee.FieldByName('Taxexempt3').AsString = 'N') then FieldByName('exempt_employee_medicare').AsString := GROUP_BOX_NO
      else if evcsEmployee.FieldByName('Taxexempt3').AsString = 'Y' then FieldByName('exempt_employee_medicare').AsString := GROUP_BOX_YES;

      if (evcsEmployee.FieldByName('Taxexempt11').AsString = '') or (evcsEmployee.FieldByName('Taxexempt11').AsString = 'N') then FieldByName('exempt_employer_oasdi').AsString := GROUP_BOX_NO
      else if evcsEmployee.FieldByName('Taxexempt11').AsString = 'Y' then FieldByName('exempt_employer_oasdi').AsString := GROUP_BOX_YES;

      if (evcsEmployee.FieldByName('Taxexempt12').AsString = '') or (evcsEmployee.FieldByName('Taxexempt12').AsString = 'N') then FieldByName('exempt_employer_medicare').AsString := GROUP_BOX_NO
      else if evcsEmployee.FieldByName('Taxexempt12').AsString = 'Y' then FieldByName('exempt_employer_medicare').AsString := GROUP_BOX_YES;

      if (evcsEmployee.FieldByName('Taxexempt13').AsString = '') or (evcsEmployee.FieldByName('Taxexempt13').AsString = 'N') then FieldByName('exempt_employer_fui').AsString := GROUP_BOX_NO
      else if evcsEmployee.FieldByName('Taxexempt13').AsString = 'Y' then FieldByName('exempt_employer_fui').AsString := GROUP_BOX_YES;

      if  evcsEmployee.FieldByName('Additional1').AsFloat <> 0 then
      begin
        FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT;
        FieldByName('OVERRIDE_FED_TAX_VALUE').AsFloat :=  evcsEmployee.FieldByName('Additional1').AsFloat;
      end
      else if evcsEmployee.FieldByName('Additional3').AsFloat <> 0 then
      begin
        FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT;
        FieldByName('OVERRIDE_FED_TAX_VALUE').AsFloat := evcsEmployee.FieldByName('Additional3').AsFloat;
      end
      else if  evcsEmployee.FieldByName('Voluntary1').AsFloat <> 0 then
      begin
        FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
        FieldByName('OVERRIDE_FED_TAX_VALUE').AsFloat := evcsEmployee.FieldByName('Voluntary1').AsFloat;
      end
      else if  evcsEmployee.FieldByName('Voluntary3').AsFloat <> 0 then
      begin
        FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_REGULAR_PERCENT;
        FieldByName('OVERRIDE_FED_TAX_VALUE').AsFloat := evcsEmployee.FieldByName('Voluntary3').AsFloat;
      end
      else
        FieldByName('override_fed_tax_type').AsString := OVERRIDE_VALUE_TYPE_NONE;

      FieldByName('base_returns_on_this_ee').AsString := GROUP_BOX_YES;

      if evcsEmployee.FieldByName('EIC').AsString = 'S' then
        FieldByName('eic').AsString := EIC_SINGLE
      else if evcsEmployee.FieldByName('EIC').AsString = 'M' then
        FieldByName('eic').AsString := EIC_MARRIED
      else if evcsEmployee.FieldByName('EIC').AsString = '1' then
        FieldByName('eic').AsString := EIC_BOTH_SPOUSES
      else
        FieldByName('eic').AsString := EIC_NONE;

      FieldByName('Gov_Garnish_Prior_Child_Suppt').AsString := GROUP_BOX_NO;

         //  changed because of ticket number 51006
//      FieldByName('new_hire_report_sent').AsString := NEW_HIRE_REPORT_COMPLETED;
      FieldByName('new_hire_report_sent').AsString := NEW_HIRE_REPORT_COMP_BY_PREDECESSOR;

      if evcsEmployee.FieldByName('Code1099').AsString = 'Y' then
        FieldByName('company_or_individual_name').Value := GROUP_BOX_COMPANY
      else
        FieldByName('company_or_individual_name').Value := GROUP_BOX_INDIVIDUAL;

      FieldByName('Federal_Marital_Status').Value := evcsEmployee.FieldByName('Fmaritalstatus').AsString;

      if evcsEmployee.FieldByName('Fdependents').AsString <> '' then
        FieldByName('Number_of_Dependents').Value := evcsEmployee.FieldByName('Fdependents').AsString
      else
      begin
        Cancel;
        Result := 2;
        WriteToLog('Employee', evcsEmployee.FieldByName('EMP').AsString, evcsEmployee.FieldByName('FNAME').AsString,
                   evcsEmployee.FieldByName('LNAME').AsString, 'FDEP', 'Field is empty in import file.');
        Exit;
      end;

      if evcsEmployee.FieldByName('Salary').AsString <> '0.00' then
      begin
        FieldByName('Salary_Amount').Value := evcsEmployee.FieldByName('Salary').AsFloat;//wwcsRate_ImportSalary.AsString;
      end;

      InsertEEStates;

      post;
      Tag := 0;
    end;
  end;
end;

procedure TEDIT_IM_RAPID.ImportEmployees;
var
  MyTransManager: TTransactionManager;
  RapidTeer: Integer;
begin

  MyTransManager := TTransactionManager.Create(Nil);
  MyTransManager.Initialize([DM_CLIENT.CL_PERSON, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_LOCALS, DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL, DM_EMPLOYEE.EE_SCHEDULED_E_DS]);
  with DM_CLIENT, DM_COMPANY, DM_EMPLOYEE, DM_SYSTEM_STATE, DM do
  begin
    CL_PERSON.OpenWithLookups := False;
    CO.OpenWithLookups := False;
    CO_DIVISION.OpenWithLookups := False;
    CO_BRANCH.OpenWithLookups := False;
    CO_DEPARTMENT.OpenWithLookups := False;
    CO_STATES.OpenWithLookups := False;
    CO_WORKERS_COMP.OpenWithLookups := False;
    CO_HR_POSITIONS.OpenWithLookups := False;
    EE.OpenWithLookups := False;
    EE_STATES.OpenWithLookups := False;
    EE_RATES.OpenWithLookups := False;
    EE_LOCALS.OpenWithLookups := False;

    CL_PERSON.DisableControls;
    CO.DisableControls;
    CO_DIVISION.DisableControls;
    CO_BRANCH.DisableControls;
    CO_DEPARTMENT.DisableControls;
    CO_STATES.DisableControls;
    CO_WORKERS_COMP.DisableControls;
    CO_HR_POSITIONS.DisableControls;
    EE.DisableControls;
    EE_STATES.DisableControls;
    EE_RATES.DisableControls;
    EE_LOCALS.DisableControls;

    CL_PERSON.DataRequired('All');
    CO.DataRequired('CO_NBR='+DM.evcsImportMaster.FieldByName('CO_NBR').AsString);
{
    with TExecDSWrapper.Create('GenericSelect3CurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 't1.CO_NBR, t1.CO_DIVISION_NBR, t1.CUSTOM_DIVISION_NUMBER, t2.CO_BRANCH_NBR, t2.CUSTOM_BRANCH_NUMBER, t3.CO_DEPARTMENT_NBR, t3.CUSTOM_DEPARTMENT_NUMBER');
      SetMacro('TABLE1', 'CO_DIVISION');
      SetMacro('TABLE2', 'CO_BRANCH');
      SetMacro('TABLE3', 'CO_DEPARTMENT');
      SetMacro('JOINFIELD2', 'CO_BRANCH');
      SetMacro('JOINFIELD', 'CO_DIVISION');
      SetMacro('CONDITION', 't1.CO_NBR = :CoNbr');
      SetParam('CoNbr', DM.evcsImportMaster.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.GetCustomData(DM.csLevelStructure, AsVariant);
    end;
}

    with TExecDSWrapper.Create('SelectDBDT') do
    begin
      SetParam('CoNbr', DM.evcsImportMaster.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.GetCustomData(DM.csLevelStructure, AsVariant);
    end;

    CO_DIVISION.DataRequired('CO_NBR='+DM.evcsImportMaster.FieldByName('CO_NBR').AsString);
    CO_BRANCH.DataRequired('ALL');
    CO_DEPARTMENT.DataRequired('ALL');
    CO_TEAM.DataRequired('ALL');
    CO_STATES.DataRequired('CO_NBR='+DM.evcsImportMaster.FieldByName('CO_NBR').AsString);
    CO_WORKERS_COMP.DataRequired('CO_NBR='+DM.evcsImportMaster.FieldByName('CO_NBR').AsString);
    CO_HR_POSITIONS.DataRequired('CO_NBR='+DM.evcsImportMaster.FieldByName('CO_NBR').AsString);
    EE.DataRequired('CO_NBR='+DM.evcsImportMaster.FieldByName('CO_NBR').AsString);
    EE_STATES.DataRequired('ALL');
    EE_RATES.DataRequired('ALL');
    EE_LOCALS.DataRequired('ALL');
    SY_STATES.DataRequired('ALL');
    SY_STATE_MARITAL_STATUS.DataRequired('ALL');

    if evcsEmployee.RecordCount <> 0 then
      try
        ctx_DataAccess.Importing := True;
        DM.evcsEmployee.First;
        ctx_StartWait('Please wait while demographic information is being imported.', DM.evcsEmployee.RecordCount-1);
        ctx_UpdateWait('Please wait while demographic information is being imported for '+DM.evcsEmployee.FieldByName('EMP').AsString+'.', DM.evcsEmployee.RecNo);
{
        if DM.evcsEmployee.FieldByName('DIVISION').AsString <> '' then
          RapidTeer := 1
        else if DM.evcsEmployee.FieldByName('Branch').AsString <> '' then
          RapidTeer := 2
        else if DM.evcsEmployee.FieldByName('Dept').AsString <> '' then
          RapidTeer := 3
        else
          RapidTeer := 0;
}

        RapidTeer := 0;

        while not DM.evcsEmployee.Eof do
        begin
          if InsertClientPerson <> 2 then
            if InsertEmployee(RapidTeer) <> 2 then
            begin
              InsertEERates;
              InsertEELocals;
            end;
          DM.evcsEmployee.Next;
          ctx_UpdateWait('Please wait while demographic information is being imported for '+DM.evcsEmployee.FieldByName('EMP').AsString+'.', DM.evcsEmployee.RecNo);
        end;

        ctx_UpdateWait('Please Wait while the imported data is being committed!');
        try
          MyTransManager.ApplyUpdates(True);
        except
          MyTransManager.CancelUpdates;
          raise;
        end;
      finally
        CO.OpenWithLookups := True;
        CO_DIVISION.OpenWithLookups := True;
        CO_BRANCH.OpenWithLookups := True;
        CO_DEPARTMENT.OpenWithLookups := True;
        CO_STATES.OpenWithLookups := True;
        CO_WORKERS_COMP.OpenWithLookups := True;
        CO_HR_POSITIONS.OpenWithLookups := True;
        EE.OpenWithLookups := True;
        EE_STATES.OpenWithLookups := True;
        EE_RATES.OpenWithLookups := True;
        EE_LOCALS.OpenWithLookups := True;

        CL_PERSON.EnableControls;
        CO.EnableControls;
        CO_DIVISION.EnableControls;
        CO_BRANCH.EnableControls;
        CO_DEPARTMENT.EnableControls;
        CO_STATES.EnableControls;
        CO_WORKERS_COMP.EnableControls;
        CO_HR_POSITIONS.EnableControls;
        EE.EnableControls;
        EE_STATES.EnableControls;
        EE_RATES.EnableControls;
        EE_LOCALS.EnableControls;
        ctx_DataAccess.Importing := False;
        ctx_EndWait
      end;
  end;
end;

function TEDIT_IM_RAPID.FilterRapidTeer(RapidTeer: Integer; aDataSet, aDataSet2: TEvBasicClientDataSet; Division, Branch, Department: String): Boolean;
var
  CustDiv: String;
  CustBra: String;
  CustDep: String;
begin
  result := True;

  if RapidTeer = 0 then
  begin
    exit;
  end;
  with aDataSet do
  begin
    CustDiv := PadStringLeft(aDataSet2.FieldByName(Division).Value, ' ', 20);
    CustBra := PadStringLeft(aDataSet2.FieldByName(Branch).Value, ' ', 20);
    CustDep := PadStringLeft(aDataSet2.FieldByName(Department).Value, ' ', 20);
    case RapidTeer of
      1:
        begin
          if aDataSet2.FieldByName(Division).AsString = '' then
          begin
            result := False;
          end
          else
          begin
            if DM.csLevelStructure.Locate('custom_division_number;custom_branch_number;custom_department_number',
                                          VarArrayOf([CustDiv, CustBra, CustDep]), [loCaseInsensitive]) then
            begin
              FieldByName('CO_DIVISION_NBR').Value   := DM.csLevelStructure.FieldByName('CO_DIVISION_NBR').AsInteger;
              FieldByName('CO_BRANCH_NBR').Value     := DM.csLevelStructure.FieldByName('CO_BRANCH_NBR').AsInteger;
              FieldByName('CO_DEPARTMENT_NBR').Value := DM.csLevelStructure.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
            end
            else if DM.csLevelStructure.Locate('custom_division_number;custom_branch_number',
                                          VarArrayOf([CustDiv, CustBra]), [loCaseInsensitive]) then
            begin
              FieldByName('CO_DIVISION_NBR').Value   := DM.csLevelStructure.FieldByName('CO_DIVISION_NBR').Value;
              FieldByName('CO_BRANCH_NBR').Value     := DM.csLevelStructure.FieldByName('CO_BRANCH_NBR').Value;
            end
            else if DM.csLevelStructure.Locate('custom_division_number',
                                          VarArrayOf([CustDiv]), [loCaseInsensitive]) then
            begin
              FieldByName('CO_DIVISION_NBR').Value   := DM.csLevelStructure.FieldByName('CO_DIVISION_NBR').Value;
            end
            else
            begin
              result := False;
            end;
          end;
        end;
      2:
        begin
          if aDataSet2.FieldByName(Branch).AsString = '' then
          begin
            result := False;
          end
          else
          begin
            if DM.csLevelStructure.Locate('custom_branch_number;custom_department_number', VarArrayOf([CustBra, CustDep]), [loCaseInsensitive]) then
            begin
              FieldByName('CO_DIVISION_NBR').Value   := DM.csLevelStructure.FieldByName('CO_DIVISION_NBR').AsInteger;
              FieldByName('CO_BRANCH_NBR').Value     := DM.csLevelStructure.FieldByName('CO_BRANCH_NBR').AsInteger;
              FieldByName('CO_DEPARTMENT_NBR').Value := DM.csLevelStructure.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
            end
            else
            begin
              Result := False;
            end;
          end;
        end;
      3:
        begin
          if aDataSet2.FieldByName(Department).AsString = '' then
          begin
            Result := False;
          end
          else
          begin
            if DM.csLevelStructure.Locate('custom_department_number', CustDep, [loCaseInsensitive]) then
            begin
              FieldByName('CO_DIVISION_NBR').Value   := DM.csLevelStructure.FieldByName('CO_DIVISION_NBR').AsInteger;
              FieldByName('CO_BRANCH_NBR').Value     := DM.csLevelStructure.FieldByName('CO_BRANCH_NBR').AsInteger;
              FieldByName('CO_DEPARTMENT_NBR').Value := DM.csLevelStructure.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
            end            else
            begin
              Result := False;
            end;
          end;
        end;
    end;
  end;
end;


procedure TEDIT_IM_RAPID.InsertScheduleds;
var
  Tmp, EDNumber, EENbr, I: Integer;
  Tmp2, BlockedWeeks: String;
begin
  inherited;

  with DM, DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE do
  begin
    EENbr := CompareData(dm.evcsRecurrings, EE, 'EMPL', 'CUSTOM_EMPLOYEE_NUMBER');
    EDNumber := GetEDNumber(DM.evcsRecurrings);
    if not Locate('ee_nbr;cl_e_ds_nbr', VarArrayof([EENbr, EDNumber]), []) then
    begin
      Insert;
      FieldByName('EE_NBR').AsInteger := EENbr;

      if (FieldByName('EE_NBR').AsInteger = 0) or (EDNumber = 0) then
      begin
        Cancel;
        WriteToLog('Recurrings', evcsRecurrings.FieldByName('EMPL').AsString, '',
                   '', 'EMPLOYEE', 'Cannot find the employee in evolution.');
        Exit;
      end
      else
      begin
        FieldByName('CL_E_DS_NBR').AsInteger := GetEDNumber(dm.evcsRecurrings);

        DM_CLIENT.CL_E_DS.Locate('CL_E_DS_NBR', EDNumber, []);

        Tmp := VarToInt(DM_CLIENT.CL_E_DS.LookUp('CL_E_DS_NBR', EDNumber, 'CL_E_D_GROUPS_NBR'));
//          GetCorrectEDGroupNumber;
        if Tmp <> 0 then
          FieldByName('CL_E_D_GROUPS_NBR').AsInteger := Tmp;

        if not DM_CLIENT.CL_E_DS.FieldByName('DEFAULT_CL_AGENCY_NBR').IsNull then
          FieldByName('CL_AGENCY_NBR').AsInteger := DM_CLIENT.CL_E_DS.FieldByName('DEFAULT_CL_AGENCY_NBR').AsInteger;

        if (dm.evcsRecurrings.FieldByName('DETYPE').AsString <> 'L') then
        begin
          Tmp2 := dm.evcsRecurrings.FieldByName('PERCENTCODE').AsString;
          if Tmp2 <> '' then
          begin
            if Tmp2[1] in ['0','4','5','9'] then
              FieldByName('AMOUNT').AsFloat := dm.evcsRecurrings.FieldByName('AMOUNT').AsFloat
            else if Tmp2[1] in ['1','3','A'..'Z'] then
              FieldByName('PERCENTAGE').AsFloat := dm.evcsRecurrings.FieldByName('AMOUNT').AsFloat;
          end;
          if dm.evcsRecurrings.FieldByName('TARGETAMOUNT').AsFloat <> 0 then
            FieldByName('TARGET_AMOUNT').AsFloat := dm.evcsRecurrings.FieldByName('TARGETAMOUNT').AsFloat;
        end;

        if dm.evcsRecurrings.FieldByName('TARGETAMOUNT').AsFloat <> 0 then
          FieldByName('TARGET_ACTION').AsString := SCHED_ED_TARGET_REMOVE;

        FieldByName('FREQUENCY').AsString := DeterminePayFrequecy;

        BlockedWeeks := PadStringLeft(IntToStr(Trunc(dm.evcsRecurrings.FieldByName('RFIELDS5').AsFloat)), '0', 5);
        for I := 1 to Length(BlockedWeeks) do
        begin
          if BlockedWeeks[I] = '1' then
            FieldByName('EXCLUDE_WEEK_'+IntToStr(I)).AsString := GROUP_BOX_YES
          else
           FieldByName('EXCLUDE_WEEK_'+IntToStr(I)).AsString := GROUP_BOX_NO;

//        FieldByName('EXCLUDE_WEEK_1').AsString := GROUP_BOX_NO;
//        FieldByName('EXCLUDE_WEEK_2').AsString := GROUP_BOX_NO;
//        FieldByName('EXCLUDE_WEEK_3').AsString := GROUP_BOX_NO;
//        FieldByName('EXCLUDE_WEEK_4').AsString := GROUP_BOX_NO;
//        FieldByName('EXCLUDE_WEEK_5').AsString := GROUP_BOX_NO;
        end;
        FieldByName('EFFECTIVE_START_DATE').AsDateTime := dm.evcsImportMaster.FieldByName('EFFECTIVE_START_DATE').AsDateTime;


        if dm.evcsRecurrings.FieldByName('PAYPERIODMAX').AsFloat <> 0 then
          FieldByName('MAXIMUM_PAY_PERIOD_AMOUNT').AsFloat := dm.evcsRecurrings.FieldByName('PAYPERIODMAX').AsFloat;
        if dm.evcsRecurrings.FieldByName('PAYPERIODMIN').AsFloat <> 0 then
          FieldByName('MINIMUM_PAY_PERIOD_AMOUNT').AsFloat := dm.evcsRecurrings.FieldByName('PAYPERIODMIN').AsFloat;
        if dm.evcsRecurrings.FieldByName('ANNUALMAX').AsFloat <> 0 then
          FieldByName('ANNUAL_MAXIMUM_AMOUNT').AsFloat := dm.evcsRecurrings.FieldByName('ANNUALMAX').AsFloat;
        FieldByName('WHICH_CHECKS').AsString := GROUP_BOX_ALL;
        if MappCalcMethod <> '' then
          FieldByName('CALCULATION_TYPE').AsString := MappCalcMethod;
        if dm.evcsRecurrings.FieldByName('percentcode').AsString = '9' then
          FieldByName('DEDUCT_WHOLE_CHECK').AsString := GROUP_BOX_YES
        else
          FieldByName('DEDUCT_WHOLE_CHECK').AsString := GROUP_BOX_NO;
        FieldByName('ALWAYS_PAY').AsString := GROUP_BOX_NO;
        if dm.evcsRecurrings.FieldByName('BALANCE').AsFloat <> 0 then
          FieldByName('BALANCE').AsFloat := dm.evcsRecurrings.FieldByName('BALANCE').AsFloat;
        Post;
      end;
    end;
  end;
end;

function TEDIT_IM_RAPID.CompareData(aDataSet1, aDataSet2: TevClientDataSet; aField1, aField2: String): Integer;
var
//  V: Variant;
  aString1: String;
begin
  result := 0;
  aString1 := aDataSet1.FieldByName(aField1).AsString;
  if aDataSet2.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(aString1, ' ', 9), []) then
  begin
    Result := aDataSet2.FieldByName('EE_NBR').AsInteger;
  end;
{
  V := aDataSet2.Lookup('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(aString1, ' ', 9), 'EE_NBR');
  if not VarIsNull(V) then
    Result := V
}
end;


function TEDIT_IM_RAPID.GetEDNumber(aDataSet: TevClientDataSet): Integer;
var
  RapEd: String;
begin
  result := 0;
  with dm, aDataSet do
  begin

    if (FieldByName('DET').AsString = 'E') or (FieldByName('DET').AsString = 'D') then
    begin
      RapED := Trim(FieldByName('DECODE').AsString);
      if Length(RapED) = 1 then
        RapED := '0'+RapED;
      RapED := FieldbyName('DET').AsString+RapED;
      if ED_MATCHING.Locate('RapidED', RapED, []) then
        Result := ED_MATCHING.FieldByName('CL_E_DS_NBR').AsInteger;
    end;
  end;

{  if (aDataSet.FieldByName('DET').AsString = 'E') or (aDataSet.FieldByName('DET').AsString = 'D') then
  begin
    Temp1 := aDataSet.FieldByName('DET').AsString + aDataSet.FieldByName('DECODE').AsString;
    Temp2 := EDsList.Values[Temp1];
    if Temp2 <> 'None' then
      result := StrToInt(copy(Temp2, Pos('*', Temp2)+1, Pos(')', Temp2)-Pos('*', Temp2)- 1));
  end;}
end;

function TEDIT_IM_RAPID.DeterminePayFrequecy: String;
var
  Tmp: String;
  Tmp2: TDateTime;
  ED: String;
begin
  ED := DM.evcsRecurrings.FieldByName('DET').AsString+DM.evcsRecurrings.FieldByName('DECODE').AsString;
  Tmp := Trim(DM.evcsRecurrings.FieldByName('FREQUENCY').AsString);
  case Tmp[1] of
    '1': result := SCHED_FREQ_DAILY;
    '2': result := SCHED_FREQ_EVRY_OTHER_PAY;
    'M':
    begin
      Tmp2 := DM.evcsRecurrings.FieldByName('FIRSTDUEDATE').AsDateTime;
      if GetBeginMonth(Tmp2) = Tmp2 then
        Result := SCHED_FREQ_FIRST_SCHED_PAY
      else if GetEndMonth(Tmp2) = Tmp2 then
        Result := SCHED_FREQ_LAST_SCHED_PAY
      else
        Result := SCHED_FREQ_MID_SCHED_PAY;
    end;
    'Q':
    begin
      WriteToLog('Scheduled EDs', Trim(DM.evcsRecurrings.FieldByName('EMPL').AsString), '', '', 'FREQUENCY', 'It has been determined that '+ED+' is setup as Quarterly which is not yet supported in Evolution, Please take appropriate action.');
      result := SCHED_FREQ_QUARTERLY;
    end;
    'S':
    begin
      WriteToLog('Scheduled EDs', Trim(DM.evcsRecurrings.FieldByName('EMPL').AsString), '', '', 'FREQUENCY', 'It has been determined that '+ED+' is setup as Semi-Annually which is not yet supported in Evolution, Please take appropriate action.');
      result := SCHED_FREQ_SEMI_ANNUALLY;
    end;
    'A':
    begin
      WriteToLog('Scheduled EDs', Trim(DM.evcsRecurrings.FieldByName('EMPL').AsString), '', '', 'FREQUENCY', 'It has been determined that '+ED+' is setup as Annually which is not yet supported in Evolution, Please take appropriate action.');
      result := SCHED_FREQ_ANNUALLY;
    end;
  end;

end;

function TEDIT_IM_RAPID.MappCalcMethod: String;
var
  Tmp: String;
  Tmp2: String;
begin
  Tmp := dm.evcsRecurrings.FieldByName('PERCENTCODE').AsString;
  Tmp2 := dm.evcsRecurrings.FieldByName('SPECIALCODE').AsString;
  if Tmp <> '' then
  begin
    case Tmp[1] of
      '0': result := CALC_METHOD_FIXED;
      '1':
      begin
        if (dm.evcsRecurrings.FieldByName('DETYPE').AsString = 'K') then
        begin
          if dm.evcsImportMaster.FieldByName('DETYPEK').AsInteger = 0 then
            result := CALC_METHOD_PERC
          else
            result := CALC_METHOD_GROSS
        end
        else if (dm.evcsRecurrings.FieldByName('DETYPE').AsString = 'Q') then
        begin
          if dm.evcsImportMaster.FieldByName('DETYPEQ').AsInteger = 0 then
            result := CALC_METHOD_PERC
          else
            result := CALC_METHOD_GROSS
        end
        else if (dm.evcsRecurrings.FieldByName('DETYPE').AsString = 'X') then
        begin
          if dm.evcsImportMaster.FieldByName('DETYPEX').AsInteger = 0 then
            result := CALC_METHOD_PERC
          else
            result := CALC_METHOD_GROSS
        end
  {      else if (wwcsRecurring.FieldByName('DETYPE').AsString = 'L') then
        begin
          result := CALC_METHOD_PENSION;
        end}
        else if Tmp2 = 'A' then
          result := CALC_METHOD_PERC
        else
          result := CALC_METHOD_GROSS;
      end;
      '3': result := CALC_METHOD_NET;
      '4': result := CALC_METHOD_GARNISHMENT;
  //    '5': don't have this not sure what to do, possible that this row will be skipped
      '6': result := CALC_METHOD_NONE;
      '9': result := CALC_METHOD_NONE;
    end;
  end;
  if (DM.evcsRecurrings.FieldByName('DETYPE').AsString = 'L') then
  begin
    result := CALC_METHOD_PENSION;
  end
end;

{
function TEDIT_IM_RAPID.GetCorrectAgencyNumber: Integer;
var
  ClientEDsNbr: Integer;
  I: Integer;
  AgencyName: String;
  AgencyNumber: Integer;
  CLEDValue: Integer;
  CLEDNAme: String;
begin
  Result := 0;
  ClientEDsNbr := DM_EMPLOYEE.EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
  result := 0;
  if AgencyList.Count > 0 then
  begin
    for I := 0 to AgencyList.Count - 1 do
    begin
      AgencyName := AgencyList.Names[I];
      AgencyNumber := StrToInt(Copy(AgencyName, Pos('(', AgencyName)+1, Pos(')', AgencyName)-Pos('(', AgencyName)-1));
      CLEDName := AgencyList.Values[AgencyList.Names[I]];
      CLEDValue := StrToInt(Copy(CLEDName, Pos('*', CLEDName)+1, Length(CLEDName)-Pos('*', CLEDName)-1));
      if CLEDValue = CLientEdsNbr then
        result := AgencyNumber;
    end;
  end;
end;
}
{
function TEDIT_IM_RAPID.GetCorrectEDGroupNumber: Integer;
var
  PercentCode: String;
  SpecialCode: String;
  tmp: String;
begin
  result := 0;
  PercentCode := DM.evcsRecurrings.FieldByName('PERCENTCODE').AsString;
  SpecialCode := DM.evcsRecurrings.FieldByName('SPECIALCODE').AsString;
  if (SpecialCode <> '') and (PercentCode <> '') then
  begin
    if (SpecialCode[1] in ['A'..'Z']) or (SpecialCode[1] in ['a'..'z']) then
    begin
      if DM.ED_GROUP_MATCHING.Locate('SpecialOrPercent', SpecialCode, []) then
        Result := DM.ED_GROUP_MATCHING.FieldByName('CL_E_D_GROUPS_NBR').AsInteger;
    end
    else if (PercentCode[1] in ['A'..'Z']) or (PercentCode[1] in ['a'..'z']) then
    begin
      if DM.ED_GROUP_MATCHING.Locate('SpecialOrPercent', PercentCode, []) then
        Result := DM.ED_GROUP_MATCHING.FieldByName('CL_E_D_GROUPS_NBR').AsInteger;
    end;
  end
  else if (DM.evcsRecurrings.FieldByName('DETYPE').AsString = 'K') or (DM.evcsRecurrings.FieldByName('DETYPE').AsString = 'Q') then
  begin
    Tmp := DM.evcsRecurrings.FieldByName('DETYPE').AsString;
    if (Tmp = 'K') and (DM.evcsImportMaster.FieldByName('DETYPEK').AsInteger = 0) then
    begin
      if DM.ED_GROUP_MATCHING.Locate('SpecialOrPercent', 'K', []) then
        Result := DM.ED_GROUP_MATCHING.FieldByName('CL_E_D_GROUPS_NBR').AsInteger;
    end;
    if (Tmp = 'Q') and (DM.evcsImportMaster.FieldByName('DETYPEQ').AsInteger = 0) then
    begin
      if DM.ED_GROUP_MATCHING.Locate('SpecialOrPercent', 'Q', []) then
        Result := DM.ED_GROUP_MATCHING.FieldByName('CL_E_D_GROUPS_NBR').AsInteger;
    end;
  end;
end;
}
procedure TEDIT_IM_RAPID.ImportDirectDeposits;
var
  MyTransManager: TTransactionManager;
begin
  MyTransManager := TTransactionManager.Create(Nil);
  MyTransManager.Initialize([DM_EMPLOYEE.EE_DIRECT_DEPOSIT, DM_EMPLOYEE.EE_SCHEDULED_E_DS]);
  with DM, DM_CLIENT, DM_COMPANY, DM_EMPLOYEE do
  begin
    ctx_DataAccess.Importing := True;

    CO_DIVISION.OpenWithLookups := False;
    CO_BRANCH.OpenWithLookups := False;
    CO_DEPARTMENT.OpenWithLookups := False;
    CO_TEAM.OpenWithLookups := False;
    EE.OpenWithLookups := False;
    EE_DIRECT_DEPOSIT.OpenWithLookups := False;
    EE_SCHEDULED_E_DS.OpenWithLookups := False;

    CO_DIVISION.DisableControls;
    CO_BRANCH.DisableControls;
    CO_DEPARTMENT.DisableControls;
    CO_TEAM.DisableControls;
    EE.DisableControls;
    EE_DIRECT_DEPOSIT.DisableControls;
    EE_SCHEDULED_E_DS.DisableControls;

    CO_DIVISION.DataRequired('');
    CO_BRANCH.DataRequired('');
    CO_DEPARTMENT.DataRequired('');
    CO_TEAM.DataRequired('');
    EE.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    EE_DIRECT_DEPOSIT.DataRequired('ALL');
    EE_SCHEDULED_E_DS.DataRequired('ALL');
    if evcsDirectDeposits.RecordCount <> 0 then
      try
        evcsDirectDeposits.First;
        ctx_StartWait('Please wait while direct deposit information is being imported.', DM.evcsDirectDeposits.RecordCount-1);
        ctx_UpdateWait('Please wait while direct deposit information is being imported for '+DM.evcsDirectDeposits.FieldByName('EMPL').AsString+'.', DM.evcsDirectDeposits.RecNo);

        while not evcsDirectDeposits.Eof do
        begin
          InsertDirectDeposits;
          evcsDirectDeposits.Next;
          ctx_UpdateWait('Please wait while direct deposit information is being imported for '+DM.evcsDirectDeposits.FieldByName('EMPL').AsString+'.', DM.evcsDirectDeposits.RecNo);
        end;
        ctx_UpdateWait('Please Wait while the imported data is being committed!');
        MyTransManager.ApplyUpdates;
      finally
        CO_DIVISION.OpenWithLookups := True;
        CO_BRANCH.OpenWithLookups := True;
        CO_DEPARTMENT.OpenWithLookups := True;
        CO_TEAM.OpenWithLookups := True;
        EE.OpenWithLookups := True;
        EE_DIRECT_DEPOSIT.OpenWithLookups := True;
        EE_SCHEDULED_E_DS.OpenWithLookups := True;

        CO_DIVISION.EnableControls;
        CO_BRANCH.EnableControls;
        CO_DEPARTMENT.EnableControls;
        CO_TEAM.EnableControls;
        EE.EnableControls;
        EE_DIRECT_DEPOSIT.EnableControls;
        EE_SCHEDULED_E_DS.EnableControls;

        MyTransManager.Free;
        ctx_DataAccess.Importing := False;
        ctx_EndWait;
      end;

  end;
end;

procedure TEDIT_IM_RAPID.InsertDirectDeposits;
var
  DeCode, sType, AccountType: String;
  Clednbr: Integer;
  aDate: TDate;
  aYear, aMonth, aDay: Word;
begin
  inherited;

  with DM, DM_EMPLOYEE do
  begin
    if DM_EMPLOYEE.EE.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(evcsDirectDeposits.FieldByName('EMPL').Value, ' ', 9), []) then
    begin

      DeCode := evcsDirectDeposits.FieldByName('DECODE').AsString;

      if DD_MATCHING.Locate('DECODE', DeCode, []) then
      begin
        sType := UpperCase(DD_MATCHING.FieldByName('DESCRIPTION').AsString);
        Clednbr := DD_MATCHING.FieldByName('CL_E_DS_NBR').AsInteger;
      end
      else
      begin
         WriteToLog('Direct Deposits', evcsDirectDeposits.FieldByName('EMPL').AsString, '',
                    '', 'ED_CODE', 'Cannot find a matching E/D in evolution for D'+evcsDirectDeposits.FieldByName('DECODE').AsString+'.');
        Exit;
      end;
//      sType := UpperCase(EDsList.Values[DeCode]);

      if Pos('CHECKING', sType) > 0 then
        AccountType := RECEIVING_ACCT_TYPE_CHECKING
      else
        AccountType := RECEIVING_ACCT_TYPE_SAVING;


      if EE_DIRECT_DEPOSIT.Locate('EE_NBR;EE_BANK_ACCOUNT_NUMBER;EE_ABA_NUMBER;EE_BANK_ACCOUNT_TYPE', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value, evcsDirectDeposits.FieldByName('ACCOUNT').Value, evcsDirectDeposits.FieldByName('BANK').Value, AccountType]), []) then
        Exit;

      EE_DIRECT_DEPOSIT.Insert;

      EE_DIRECT_DEPOSIT.FieldByName('EE_BANK_ACCOUNT_TYPE').AsString := AccountType;
      EE_DIRECT_DEPOSIT.FieldByName('EE_NBR').AsInteger := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
      EE_DIRECT_DEPOSIT.FieldByName('EE_BANK_ACCOUNT_NUMBER').AsString := evcsDirectDeposits.FieldByName('ACCOUNT').AsString;
      EE_DIRECT_DEPOSIT.FieldByName('EE_ABA_NUMBER').AsString := evcsDirectDeposits.FieldByName('BANK').AsString;
      if evcsDirectDeposits.FieldByName('PRENOTE').AsString <> '' then
        EE_DIRECT_DEPOSIT.FieldByName('IN_PRENOTE').AsString := GROUP_BOX_YES
      else
        EE_DIRECT_DEPOSIT.FieldByName('IN_PRENOTE').AsString := GROUP_BOX_NO;



      EE_DIRECT_DEPOSIT.Post;

      EE_SCHEDULED_E_DS.Insert;
      EE_SCHEDULED_E_DS.FieldByName('EE_NBR').AsInteger := DM_EMPLOYEE.EE_DIRECT_DEPOSIT.FieldByName('EE_NBR').AsInteger;


      EE_SCHEDULED_E_DS.FieldByName('CL_E_DS_NBR').AsInteger := Clednbr;

      EE_SCHEDULED_E_DS.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger := DM_EMPLOYEE.EE_DIRECT_DEPOSIT.FieldByName('EE_DIRECT_DEPOSIT_NBR').AsInteger;
      EE_SCHEDULED_E_DS.FieldByName('PLAN_TYPE').AsString := 'N';
      if evcsDirectDeposits.FieldByName('amountorpercent').AsString = 'A' then
      begin
        if evcsDirectDeposits.FieldByName('amount').AsFloat <> 0 then
          EE_SCHEDULED_E_DS.FieldByName('AMOUNT').AsFloat := evcsDirectDeposits.FieldByName('amount').AsFloat;
      end
      else
      begin
        if evcsDirectDeposits.FieldByName('amount').AsFloat <> 0 then
          EE_SCHEDULED_E_DS.FieldByName('PERCENTAGE').AsFloat := evcsDirectDeposits.FieldByName('amount').AsFloat;
      end;

      EE_SCHEDULED_E_DS.FieldByName('FREQUENCY').AsString := SCHED_FREQ_DAILY;
      EE_SCHEDULED_E_DS.FieldByName('EXCLUDE_WEEK_1').AsString := GROUP_BOX_NO;
      EE_SCHEDULED_E_DS.FieldByName('EXCLUDE_WEEK_2').AsString := GROUP_BOX_NO;
      EE_SCHEDULED_E_DS.FieldByName('EXCLUDE_WEEK_3').AsString := GROUP_BOX_NO;
      EE_SCHEDULED_E_DS.FieldByName('EXCLUDE_WEEK_4').AsString := GROUP_BOX_NO;
      EE_SCHEDULED_E_DS.FieldByName('EXCLUDE_WEEK_5').AsString := GROUP_BOX_NO;

      aDate := Now;
      DecodeDate(aDate, aYear, aMonth, aDay);
      EE_SCHEDULED_E_DS.FieldByName('EFFECTIVE_START_DATE').AsDateTime := StrToDate('01/01/'+IntToStr(aYear));
      EE_SCHEDULED_E_DS.FieldByName('WHICH_CHECKS').AsString := GROUP_BOX_ALL;

      if (evcsDirectDeposits.FieldByName('amountorpercent').AsString = 'A') and (evcsDirectDeposits.FieldByName('amount').AsFloat <> 0) then
      begin
        EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString := CALC_METHOD_FIXED;
        EE_SCHEDULED_E_DS.FieldByName('DEDUCT_WHOLE_CHECK').AsString := GROUP_BOX_NO;
      end
      else if (evcsDirectDeposits.FieldByName('amountorpercent').AsString = 'A') and (evcsDirectDeposits.FieldByName('amount').AsFloat = 0) then
      begin
        EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString := CALC_METHOD_NONE;
        EE_SCHEDULED_E_DS.FieldByName('DEDUCT_WHOLE_CHECK').AsString := GROUP_BOX_YES;
      end
      else
      begin
        EE_SCHEDULED_E_DS.FieldByName('CALCULATION_TYPE').AsString := CALC_METHOD_NET;
        EE_SCHEDULED_E_DS.FieldByName('DEDUCT_WHOLE_CHECK').AsString := GROUP_BOX_NO;
      end;

      EE_SCHEDULED_E_DS.FieldByName('ALWAYS_PAY').AsString := GROUP_BOX_NO;
      EE_SCHEDULED_E_DS.Post;
    end
    else
      WriteToLog('Direct Deposits', evcsDirectDeposits.FieldByName('EMPL').AsString, '',
                 '', 'EMPLOYEE', 'Cannot find the employee in evolution.');
  end;
end;

procedure TEDIT_IM_RAPID.ImportTimeOffs;
var
  MyTransManager: TTransactionManager;
begin
  MyTransManager := TTransactionManager.Create(Nil);
  MyTransManager.Initialize([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL]);
  with DM, DM_CLIENT, DM_COMPANY, DM_EMPLOYEE do
  begin
    ctx_DataAccess.Importing := True;

    EE.OpenWithLookups := False;
    CO_TIME_OFF_ACCRUAL.OpenWithLookups := False;
    EE_TIME_OFF_ACCRUAL.OpenWithLookups := False;

    EE.DisableControls;
    CO_TIME_OFF_ACCRUAL.DisableControls;
    EE_TIME_OFF_ACCRUAL.DisableControls;

    CO_TIME_OFF_ACCRUAL.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    EE.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    EE_TIME_OFF_ACCRUAL.DataRequired('ALL');
    if evcsTimeOff.RecordCount <> 0 then
      try
        evcsTimeOff.First;
        ctx_StartWait('Please wait while time off accrual information is being imported.', DM.evcsTimeOff.RecordCount-1);
        ctx_UpdateWait('Please wait while time off accrual information is being imported for '+DM.evcsTimeOff.FieldByName('EMPL').AsString+'.', DM.evcsTimeOff.RecNo);

        while not evcsTimeOff.Eof do
        begin
          InsertTimeOffs;
          evcsTimeOff.Next;
          ctx_UpdateWait('Please wait while time off accrual information is being imported for '+DM.evcsTimeOff.FieldByName('EMPL').AsString+'.', DM.evcsTimeOff.RecNo);
        end;
        ctx_UpdateWait('Please Wait while the imported time off accrual data is being committed!');
        MyTransManager.ApplyUpdates;
      finally
        EE.OpenWithLookups := True;
        CO_TIME_OFF_ACCRUAL.OpenWithLookups := True;
        EE_TIME_OFF_ACCRUAL.OpenWithLookups := True;

        EE.EnableControls;
        CO_TIME_OFF_ACCRUAL.EnableControls;
        EE_TIME_OFF_ACCRUAL.EnableControls;

        MyTransManager.Free;
        ctx_DataAccess.Importing := False;
        ctx_EndWait;
      end;
  end;
end;

procedure TEDIT_IM_RAPID.InsertTimeOffs;
var
  V: Variant;
  StrTest: String;
begin
  inherited;

  with DM, DM_EMPLOYEE do
  begin
    if DM_EMPLOYEE.EE.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(evcsTimeOff.FieldByName('EMPL').Value, ' ', 9), []) then
    begin
      if (evcsTimeOff.FieldByName('EEOTYPE1').AsString <> '') then
      begin
        V := DM_COMPANY.CO_TIME_OFF_ACCRUAL.LookUp('DESCRIPTION', evcsTimeOff.FieldByName('EEOTYPE1').Value, 'CO_TIME_OFF_ACCRUAL_NBR');
        if (NOT VARISNULL(V) and (not EE_TIME_OFF_ACCRUAL.Locate('EE_NBR;CO_TIME_OFF_ACCRUAL_NBR', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value, V]), []))) then
        begin
          EE_TIME_OFF_ACCRUAL.Insert;
          EE_TIME_OFF_ACCRUAL.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').AsInteger := V;
          EE_TIME_OFF_ACCRUAL.FieldByName('EE_NBR').AsInteger := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
          StrTest := evcsTimeOff.FieldByName('EETODATE1').AsString;
          if StrTest <> '' then
            if  StrTest[1] <> ' ' then
              EE_TIME_OFF_ACCRUAL.FieldByName('EFFECTIVE_ACCRUAL_DATE').AsDateTime := evcsTimeOff.FieldByName('EETODATE1').AsDateTime;
          if evcsTimeOff.FieldByName('EETOOVRATE1').AsFloat <> 0 then
            EE_TIME_OFF_ACCRUAL.FieldByName('OVERRIDE_RATE').AsFloat := evcsTimeOff.FieldByName('EETOOVRATE1').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_ACCRUED').AsFloat := evcsTimeOff.FieldByName('EETOHOURS1').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_USED').AsFloat := 0;
          EE_TIME_OFF_ACCRUAL.Post;
        end;
      end;
      if evcsTimeOff.FieldByName('EEOTYPE2').AsString <> '' then
      begin
        V := DM_COMPANY.CO_TIME_OFF_ACCRUAL.LookUp('DESCRIPTION', evcsTimeOff.FieldByName('EEOTYPE2').Value, 'CO_TIME_OFF_ACCRUAL_NBR');
        if (NOT VARISNULL(V) and (not EE_TIME_OFF_ACCRUAL.Locate('EE_NBR;CO_TIME_OFF_ACCRUAL_NBR', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value, V]), []))) then
        begin
          EE_TIME_OFF_ACCRUAL.Insert;
          EE_TIME_OFF_ACCRUAL.FieldByName('EE_NBR').AsInteger := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
          EE_TIME_OFF_ACCRUAL.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').AsInteger := V;
          StrTest := evcsTimeOff.FieldByName('EETODATE2').AsString;
          if StrTest <> '' then
            if StrTest[1] <> ' ' then
              EE_TIME_OFF_ACCRUAL.FieldByName('EFFECTIVE_ACCRUAL_DATE').AsDateTime := evcsTimeOff.FieldByName('EETODATE2').AsDateTime;
          if evcsTimeOff.FieldByName('EETOOVRATE2').AsFloat <> 0 then
            EE_TIME_OFF_ACCRUAL.FieldByName('OVERRIDE_RATE').AsFloat := evcsTimeOff.FieldByName('EETOOVRATE2').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_ACCRUED').AsFloat := evcsTimeOff.FieldByName('EETOHOURS2').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_USED').AsFloat := 0;
          EE_TIME_OFF_ACCRUAL.Post;
        end;
      end;
      if evcsTimeOff.FieldByName('EEOTYPE3').AsString <> '' then
      begin
        V := DM_COMPANY.CO_TIME_OFF_ACCRUAL.LookUp('DESCRIPTION', evcsTimeOff.FieldByName('EEOTYPE3').Value, 'CO_TIME_OFF_ACCRUAL_NBR');
        if (NOT VARISNULL(V) and (not EE_TIME_OFF_ACCRUAL.Locate('EE_NBR;CO_TIME_OFF_ACCRUAL_NBR', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value, V]), []))) then
        begin
          EE_TIME_OFF_ACCRUAL.Insert;
          EE_TIME_OFF_ACCRUAL.FieldByName('EE_NBR').AsInteger := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
          EE_TIME_OFF_ACCRUAL.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').AsInteger := V;
          StrTest := evcsTimeOff.FieldByName('EETODATE3').AsString;
          if StrTest <> '' then
            if StrTest[1] <> ' ' then
              EE_TIME_OFF_ACCRUAL.FieldByName('EFFECTIVE_ACCRUAL_DATE').AsDateTime := evcsTimeOff.FieldByName('EETODATE3').AsDateTime;
          if evcsTimeOff.FieldByName('EETOOVRATE3').AsFloat <> 0 then
            EE_TIME_OFF_ACCRUAL.FieldByName('OVERRIDE_RATE').AsFloat := evcsTimeOff.FieldByName('EETOOVRATE3').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_ACCRUED').AsFloat := evcsTimeOff.FieldByName('EETOHOURS3').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_USED').AsFloat := 0;
          EE_TIME_OFF_ACCRUAL.Post;
        end;
      end;
      if evcsTimeOff.FieldByName('EEOTYPE4').AsString <> '' then
      begin
        V := DM_COMPANY.CO_TIME_OFF_ACCRUAL.LookUp('DESCRIPTION', evcsTimeOff.FieldByName('EEOTYPE4').Value, 'CO_TIME_OFF_ACCRUAL_NBR');
        if (NOT VARISNULL(V) and (not EE_TIME_OFF_ACCRUAL.Locate('EE_NBR;CO_TIME_OFF_ACCRUAL_NBR', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value, V]), []))) then
        begin
          EE_TIME_OFF_ACCRUAL.Insert;
          EE_TIME_OFF_ACCRUAL.FieldByName('EE_NBR').AsInteger := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
          EE_TIME_OFF_ACCRUAL.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').AsInteger := V;
          StrTest := evcsTimeOff.FieldByName('EETODATE4').AsString;
          if StrTest <> '' then
            if StrTest[1] <> ' ' then
              EE_TIME_OFF_ACCRUAL.FieldByName('EFFECTIVE_ACCRUAL_DATE').AsDateTime := evcsTimeOff.FieldByName('EETODATE4').AsDateTime;
          if evcsTimeOff.FieldByName('EETOOVRATE4').AsFloat <> 0 then
            EE_TIME_OFF_ACCRUAL.FieldByName('OVERRIDE_RATE').AsFloat := evcsTimeOff.FieldByName('EETOOVRATE4').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_ACCRUED').AsFloat := evcsTimeOff.FieldByName('EETOHOURS4').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_USED').AsFloat := 0;
          EE_TIME_OFF_ACCRUAL.Post;
        end;
      end;
      if evcsTimeOff.FieldByName('EEOTYPE5').AsString <> '' then
      begin
        V := DM_COMPANY.CO_TIME_OFF_ACCRUAL.LookUp('DESCRIPTION', evcsTimeOff.FieldByName('EEOTYPE5').Value, 'CO_TIME_OFF_ACCRUAL_NBR');
        if (NOT VARISNULL(V) and (not EE_TIME_OFF_ACCRUAL.Locate('EE_NBR;CO_TIME_OFF_ACCRUAL_NBR', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value, V]), []))) then
        begin
          EE_TIME_OFF_ACCRUAL.Insert;
          EE_TIME_OFF_ACCRUAL.FieldByName('EE_NBR').AsInteger := DM_EMPLOYEE.EE.FieldByName('EE_NBR').AsInteger;
          V := DM_COMPANY.CO_TIME_OFF_ACCRUAL.LookUp('DESCRIPTION', evcsTimeOff.FieldByName('EEOTYPE5').Value, 'CO_TIME_OFF_ACCRUAL_NBR');
          EE_TIME_OFF_ACCRUAL.FieldByName('CO_TIME_OFF_ACCRUAL_NBR').AsInteger := V;
          StrTest := evcsTimeOff.FieldByName('EETODATE5').AsString;
          if StrTest <> '' then
            if StrTest[1] <> ' ' then
              EE_TIME_OFF_ACCRUAL.FieldByName('EFFECTIVE_ACCRUAL_DATE').AsDateTime := evcsTimeOff.FieldByName('EETODATE5').AsDateTime;
          if evcsTimeOff.FieldByName('EETOOVRATE5').AsFloat <> 0 then
            EE_TIME_OFF_ACCRUAL.FieldByName('OVERRIDE_RATE').AsFloat := evcsTimeOff.FieldByName('EETOOVRATE5').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_ACCRUED').AsFloat := evcsTimeOff.FieldByName('EETOHOURS5').AsFloat;
          EE_TIME_OFF_ACCRUAL.FieldByName('CURRENT_USED').AsFloat := 0;
          EE_TIME_OFF_ACCRUAL.Post;
        end;
      end;
    end
    else
      WriteToLog('Time Off Accrual', evcsTimeOff.FieldByName('EMPL').AsString, '',
                  '', 'EMPLOYEE', 'Cannot find the employee in evolution.');
  end;
end;

procedure TEDIT_IM_RAPID.ImportYTDs;
var
  MyTransManager: TTransactionManager;
begin
  MyTransManager := TTransactionManager.Create(Nil);
  MyTransManager.Initialize([DM_EMPLOYEE.EE_STATES, DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, DM_PAYROLL.PR_CHECK,
                             DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_STATES,
                             DM_PAYROLL.PR_CHECK_SUI, DM_PAYROLL.PR_CHECK_LOCALS]);

  with DM, DM_SYSTEM_STATE, DM_CLIENT, DM_COMPANY, DM_EMPLOYEE, DM_PAYROLL do
  begin
    ctx_DataAccess.Importing := True;

    CO_STATES.OpenWithLookups := False;
    CO_DIVISION.OpenWithLookups := False;
    CO_BRANCH.OpenWithLookups := False;
    CO_DEPARTMENT.OpenWithLookups := False;
    CO_TEAM.OpenWithLookups := False;
    CO_SUI.OpenWithLookups := False;
    CL_PERSON.OpenWithLookups := False;
    EE.OpenWithLookups := False;
    EE_STATES.OpenWithLookups := False;
    PR.OpenWithLookups := False;
    PR_CHECK.OpenWithLookups := False;
    PR_CHECK_LINES.OpenWithLookUps := False;
    PR_CHECK_STATES.OpenWithLookups := False;
    PR_CHECK_SUI.OpenWithLookups := False;
    SY_STATE_MARITAL_STATUS.OpenWithLookups := False;
    CL_E_DS.OpenWithLookups := False;

    CO_STATES.DisableControls;
    CO_DIVISION.DisableControls;
    CO_BRANCH.DisableControls;
    CO_DEPARTMENT.DisableControls;
    CO_TEAM.DisableControls;
    CO_SUI.DisableControls;
    CL_PERSON.DisableControls;
    EE.DisableControls;
    EE_STATES.DisableControls;
    PR.DisableControls;
    PR_CHECK.DisableControls;
    PR_CHECK_LINES.DisableControls;
    PR_CHECK_STATES.DisableControls;
    PR_CHECK_SUI.DisableControls;
    SY_STATE_MARITAL_STATUS.DisableControls;
    CL_E_DS.DisableControls;

    CL_E_DS.DataRequired('ALL');
    CO.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    CO_STATES.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);

    with TExecDSWrapper.Create('GenericSelect3CurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 't1.CO_NBR, t1.CO_DIVISION_NBR, t1.CUSTOM_DIVISION_NUMBER, t2.CO_BRANCH_NBR, t2.CUSTOM_BRANCH_NUMBER, t3.CO_DEPARTMENT_NBR, t3.CUSTOM_DEPARTMENT_NUMBER');
      SetMacro('TABLE1', 'CO_DIVISION');
      SetMacro('TABLE2', 'CO_BRANCH');
      SetMacro('TABLE3', 'CO_DEPARTMENT');
      SetMacro('JOINFIELD2', 'CO_BRANCH');
      SetMacro('JOINFIELD', 'CO_DIVISION');
      SetMacro('CONDITION', 't1.CO_NBR = :CoNbr');
      SetParam('CoNbr', DM.evcsImportMaster.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.GetCustomData(DM.csLevelStructure, AsVariant);
    end;

    CO_DIVISION.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    CO_BRANCH.DataRequired('ALL');
    CO_DEPARTMENT.DataRequired('ALL');
    CO_TEAM.DataRequired('ALL');
    CO_SUI.DataRequired('ALL');
    CL_PERSON.DataRequired('ALL');
    EE.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    EE_STATES.DataRequired('ALL');
    PR.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    PR_CHECK_STATES.DataRequired('ALL');
    PR_CHECK_SUI.DataRequired('ALL');
    SY_STATE_MARITAL_STATUS.DataRequired('ALL');

    if evcsYearToDate.RecordCount <> 0 then
      try
        evcsYearToDate.First;

        InsertPR;
        InsertPRBatch;

        ctx_StartWait('Please wait while year to date information is being imported.', DM.evcsYearToDate.RecordCount-1);
        ctx_UpdateWait('Please wait while year to date information is being imported for '+DM.evcsYearToDate.FieldByName('EMPL').AsString+'.', DM.evcsYearToDate.RecNo);

        while not evcsYearToDate.Eof do
        begin
          InsertYTDs;
          evcsYearToDate.Next;
          ctx_UpdateWait('Please wait while year to date information is being imported for '+DM.evcsYearToDate.FieldByName('EMPL').AsString+'.', DM.evcsYearToDate.RecNo);
        end;
        CheckPrCheckStates;
        ctx_UpdateWait('Please Wait while the imported year to dates data is being committed!');
        MyTransManager.ApplyUpdates;
      finally
        CO_STATES.OpenWithLookups := True;
        CO_DIVISION.OpenWithLookups := True;
        CO_BRANCH.OpenWithLookups := True;
        CO_DEPARTMENT.OpenWithLookups := True;
        CO_TEAM.OpenWithLookups := True;
        CO_SUI.OpenWithLookups := True;
        CL_PERSON.OpenWithLookups := True;
        EE.OpenWithLookups := True;
        EE_STATES.OpenWithLookups := True;
        PR.OpenWithLookups := True;
        PR_CHECK.OpenWithLookups := True;
        PR_CHECK_LINES.OpenWithLookUps := True;
        PR_CHECK_STATES.OpenWithLookups := True;
        PR_CHECK_SUI.OpenWithLookups := True;
        SY_STATE_MARITAL_STATUS.OpenWithLookups := True;
        CL_E_DS.OpenWithLookups := True;

        CO_STATES.EnableControls;
        CO_DIVISION.EnableControls;
        CO_BRANCH.EnableControls;
        CO_DEPARTMENT.EnableControls;
        CO_TEAM.EnableControls;
        CO_SUI.EnableControls;
        CL_PERSON.EnableControls;
        EE.EnableControls;
        EE_STATES.EnableControls;
        PR.EnableControls;
        PR_CHECK.EnableControls;
        PR_CHECK_LINES.EnableControls;
        PR_CHECK_STATES.EnableControls;
        PR_CHECK_SUI.EnableControls;
        SY_STATE_MARITAL_STATUS.EnableControls;
        CL_E_DS.EnableControls;

        MyTransManager.Free;
        ctx_DataAccess.Importing := False;
        ctx_EndWait;
      end;
  end;
end;

procedure TEDIT_IM_RAPID.InsertYTDs;
var
  Tmp, EDNumber, StateNbr: Integer;
  DeCode: String;
  EDCodeType: Variant;
  SumAmounts, SumHours: Double;
begin
  inherited;

    Tmp := CompareData(DM.evcsYearToDate, DM_EMPLOYEE.EE, 'EMPL', 'CUSTOM_EMPLOYEE_NUMBER');
    StateNbr := GetEEStatesNumber(dm.evcsYearToDate);
    SumAmounts := SumOfAmounts;
    SumHours := SumOfHours;
    DeCode := dm.evcsYearToDate.FieldByName('DECODE').AsString;
    if (Tmp <> 0) then
    begin
      with DM, DM_PAYROLL.PR_CHECK, DM_PAYROLL do
      begin
        DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_EMPLOYEE.EE.FieldByName('CL_PERSON_NBR').Value, []);
        EDNumber := GetEDNumber(evcsYearToDate);
        EDCodeType := DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', EDNumber, 'E_D_CODE_TYPE');

        DM_EMPLOYEE.EE_SCHEDULED_E_DS.DataRequired('EE_NBR='+QuotedStr(IntToStr(Tmp))+' and CL_E_DS_NBR='+QuotedStr(IntToStr(EDNumber)));

        if (evcsYearToDate.FieldByName('DETYPE').AsString = '3') or (EDCodeType = 'EO') or
           (EDCodeType = 'EP') or (EDCodeType = 'EQ') then
        begin
          if Not Locate('EE_NBR;PR_NBR;CHECK_TYPE', VarArrayOf([Tmp, DM_PAYROLL.PR.FieldByName('PR_NBR').Value, CHECK_TYPE2_3RD_PARTY]), []) then
            Create3rdPartyCheck(Tmp);
          Edit3rdPartyCheck(SumAmounts, SumHours);
        end
        else
        begin
          if Not Locate('EE_NBR;PR_NBR;CHECK_TYPE', VarArrayOf([Tmp, DM_PAYROLL.PR.FieldByName('PR_NBR').Value, CHECK_TYPE2_MANUAL]), []) then
            CreateManualCheck(Tmp);
          EditManualCheck(SumAmounts, SumHours);
        end;
      end;

      if (StateNbr = 0) and (dm.evcsYearToDate.FieldByName('STATETAXCODE1').AsString <> '') then
      begin
        StateNbr := InsertYTDState;
        if StateNbr <> 0 then
           WriteToLog('Year to Dates(EE States)', dm.evcsYearToDate.FieldByName('EMPL').AsString, '',
                      '', 'CO_STATES_NBR', 'The state '+dm.evcsYearToDate.FieldByName('Statetaxcode1').AsString+' has been setup dynamically with defaults. Please verifiy that it has been setup properly.');
      end;

      if DM_PAYROLL.PR_CHECK_LINES.State = dsInactive then
        DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_CHECK_NBR='+DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);

      with DM, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL do
      begin
        if (evcsYearToDate.FieldByName('DET').AsString <> 'T') and (EDNumber <> 0) then
        begin
          if (evcsYearToDate.FieldByName('DET').AsString = 'D') and
             (evcsYearToDate.FieldByName('DETYPE').AsString = '3') then
            Exit
          else
            CreateCheckLine(EDNumber, SumHours, SumAmounts, StateNbr);
        end
        else if (evcsYearToDate.FieldByName('DET').AsString = 'T') and (evcsYearToDate.FieldByName('STATETAXCODE1').AsString = 'NY') then
          HandleNYSDI(EDNumber, SumHours, SumAmounts)
        else if (evcsYearToDate.FieldByName('DET').AsString = 'T') and (evcsYearToDate.FieldByName('STATETAXCODE2').AsString = 'NJ') and ((Trim(evcsYearToDate.FieldByName('DECODE').AsString) = '7') or (Trim(evcsYearToDate.FieldByName('DECODE').AsString) = '9') or (Trim(evcsYearToDate.FieldByName('DECODE').AsString) = '10') or (evcsYearToDate.FieldByName('DECODE').AsString = '16')) then
          Handle_NJ_T7_T16(SumAmounts)
        else if (evcsYearToDate.FieldByName('DET').AsString = 'T') and (Trim(evcsYearToDate.FieldByName('DECODE').AsString) = '8') then
          Handle_NJ_T7_T16(SumAmounts)
        else if (EDNumber = 0) and (evcsYearToDate.FieldByName('DET').AsString <> 'T') then
        begin
          Cancel;
          WriteToLog('Year to Dates', dm.evcsYearToDate.FieldByName('EMPL').AsString, '',
                     '', 'E/D CODE', 'Cannot find a matching  E/D in evolution for'+evcsYearToDate.FieldByName('DET').AsString+evcsYearToDate.FieldByName('DECODE').AsString+' .');
        end;
      end;

      with DM_PAYROLL.PR_CHECK_STATES, DM_PAYROLL do
      begin
        if (dm.evcsYearToDate.FieldByName('DET').AsString = 'T') and ((Trim(dm.evcsYearToDate.FieldByName('DECODE').AsString) = '6') or (Trim(dm.evcsYearToDate.FieldByName('DECODE').AsString) = '7')) then
        begin

          if (Trim(dm.evcsYearToDate.FieldByName('DECODE').AsString) = '7') and (dm.evcsYearToDate.FieldByName('STATETAXCODE2').AsString = 'NJ') then
            Exit;

          if not Locate('PR_CHECK_NBR;EE_STATES_NBR', VarArrayOf([PR_CHECK.FieldByName('PR_CHECK_NBR').Value, StateNbr]), []) then
          begin
            Insert;
            FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;

            if StateNbr = 0 then
            begin
              WriteToLog('Year to Dates', dm.evcsYearToDate.FieldByName('EMPL').AsString, '',
                         '', 'CO_STATES_NBR', 'The state '+dm.evcsYearToDate.FieldByName('Statetaxcode1').AsString+' is not setup in evolution.');
              Cancel;
              Exit;
            end;

            FieldByName('EE_STATES_NBR').AsInteger := StateNbr;//GetEEStatesNumber(dm.evcsYearToDate);

            if (Trim(dm.evcsYearToDate.FieldByName('DECODE').AsString) = '7') and  (dm.evcsYearToDate.FieldByName('STATETAXCODE2').AsString = 'NY') then
            begin
              Cancel;
            end
            else
            begin
              FieldByName('TAX_AT_SUPPLEMENTAL_RATE').AsString := GROUP_BOX_NO;
              FieldByName('EXCLUDE_STATE').AsString := GROUP_BOX_NO;
              FieldByName('EXCLUDE_ADDITIONAL_STATE').AsString := GROUP_BOX_YES;
              FieldByName('EXCLUDE_SDI').AsString := GROUP_BOX_NO;
              FieldByName('EXCLUDE_SUI').AsString := GROUP_BOX_NO;
              FieldByName('STATE_OVERRIDE_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
              if (Trim(dm.evcsYearToDate.FieldByName('DECODE').AsString) = '6') then
                FieldByName('STATE_TAX').AsFloat := SumAmounts
              else if (Trim(dm.evcsYearToDate.FieldByName('DECODE').AsString) = '7') then
              begin
                FieldByName('EE_SDI_TAX').AsFloat := SumAmounts;
                if FieldByName('STATE_TAX').IsNull then
                  FieldByName('STATE_TAX').AsFloat := 0.00;
              end;
              Post;
            end;
          end
          else
          begin
            Edit;
            if (Trim(dm.evcsYearToDate.FieldByName('DECODE').AsString) = '6') then
              FieldByName('STATE_TAX').AsFloat := SumAmounts
            else if (Trim(dm.evcsYearToDate.FieldByName('DECODE').AsString) = '7') and (dm.evcsYearToDate.FieldByName('STATETAXCODE2').AsString <> 'NY') then
              FieldByName('EE_SDI_TAX').AsFloat := SumAmounts;
            post;
          end;
        end;
      end;

      with DM_PAYROLL.PR_CHECK_LOCALS, DM_PAYROLL do
      begin
        DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('ALL');
        if (dm.evcsYearToDate.FieldByName('STATETAXCODE1').AsString <> '') and (dm.evcsYearToDate.FieldByName('LOCAL').AsString <> '') then
        begin
          if dm.LOCAL_MATCHING.Locate('STATE;RapidLocal', VarArrayOf([dm.evcsYearToDate.FieldByName('STATETAXCODE1').Value, dm.evcsYearToDate.FieldByName('LOCAL').Value]) , []) then
          begin
            DM_COMPANY.CO_LOCAL_TAX.DataRequired('SY_LOCALS_NBR='+dm.LOCAL_MATCHING.FieldByName('LocalNbr').AsString+' and CO_NBR='+DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
            if DM_COMPANY.CO_LOCAL_TAX.RecordCount <> 0 then
            begin
              DM_EMPLOYEE.EE_LOCALS.DataRequired('EE_NBR='+IntToStr(Tmp)+' and CO_LOCAL_TAX_NBR='+DM_COMPANY.CO_LOCAL_TAX.FieldByName('CO_LOCAL_TAX_NBR').AsString);
              if DM_EMPLOYEE.EE_LOCALS.RecordCount <> 0 then
              begin
                if not Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([PR_CHECK.FieldByName('PR_CHECK_NBR').Value, DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').Value]), []) then
                begin
                  Insert;
                  FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
                  FieldByName('EE_LOCALS_NBR').AsInteger := DM_EMPLOYEE.EE_LOCALS.FieldByName('EE_LOCALS_NBR').AsInteger;
                  FieldByName('EXCLUDE_LOCAL').AsString := GROUP_BOX_NO;
                  FieldByName('LOCAL_TAX').AsFloat := SumAmounts;
                  Post;
                end;
              end;
            end;
          end;
        end;
      end;

{     with DM_PAYROLL.PR_CHECK_SUI, DM_PAYROLL do
      begin
        if NOT Locate('PR_CHECK_NBR;CO_SUI_NBR', VarArrayOf([PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').Value, GetCheckSuiNumber(dm.evcsYearToDate)]), []) then
        begin
          Insert;
          FieldByName('PR_CHECK_NBR').AsInteger := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
          FieldByName('CO_SUI_NBR').AsInteger := GetCheckSuiNumber(dm.evcsYearToDate);
          Post;
        end;
      end;}
    end
    else if (tmp = 0) then
    begin
      WriteToLog('Year to Dates', dm.evcsYearToDate.FieldByName('EMPL').AsString, '',
                 '', 'EMPLOYEE', 'Cannot find the employee in evolution.');
    end;
//  end;
{
  if evCheckBox1.Checked then
  begin
    DM_PAYROLL.PR.First;
    while not DM_PAYROLL.PR.Eof do
    begin
      if (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_COMPLETED) and (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_IMPORT) then
        PayrollProcessingInterface.ProcessPayroll(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger, False, False);
      DM_PAYROLL.PR.Next;
    end;
  end;
}

end;

function TEDIT_IM_RAPID.GetEEStatesNumber(
  aDataSet: TevClientDataSet): Integer;
var
  V: Variant;
  CoStatesNbr: Integer;
begin
  CoStatesNbr := 0;
  result := 0;
  V := DM_COMPANY.CO_STATES.Lookup('STATE', aDataSet.FieldByName('STATETAXCODE1').Value, 'CO_STATES_NBR');
  if NOT VARISNULL(V) then
    CoStatesNbr := V;                                                 //DM_PAYROLL.PR_CHECK
  V := DM_EMPLOYEE.EE_STATES.Lookup('EE_NBR;CO_STATES_NBR', VarArrayOf([DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value, CoStatesNbr]), 'EE_STATES_NBR');
  if NOT VARISNULL(V) then
    result := V;
end;

function TEDIT_IM_RAPID.GetEEStatesNumber(aFieldName: String; aDataSet: TevClientDataSet): Integer;
var
  V: Variant;
  CoStatesNbr: Integer;
begin
  CoStatesNbr := 0;
  result := 0;
  V := DM_COMPANY.CO_STATES.Lookup('STATE', aDataSet.FieldByName(aFieldName).Value, 'CO_STATES_NBR');
  if NOT VARISNULL(V) then
    CoStatesNbr := V;
  V := DM_EMPLOYEE.EE_STATES.Lookup('EE_NBR;SUI_APPLY_CO_STATES_NBR', VarArrayOf([DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value, CoStatesNbr]), 'EE_STATES_NBR');
  if NOT VARISNULL(V) then
    result := V;
end;


function TEDIT_IM_RAPID.SumOfAmounts: Double;
begin
  result := 0;
  with DM, dm.evcsYearToDate do
  begin
    if evcsImportMaster.FieldByName('JAN').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS1').AsFloat;
    if evcsImportMaster.FieldByName('FEB').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS2').AsFloat;
    if evcsImportMaster.FieldByName('MAR').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS3').AsFloat;
    if evcsImportMaster.FieldByName('APR').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS4').AsFloat;
    if evcsImportMaster.FieldByName('MAY').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS5').AsFloat;
    if evcsImportMaster.FieldByName('JUN').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS6').AsFloat;
    if evcsImportMaster.FieldByName('JUL').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS7').AsFloat;
    if evcsImportMaster.FieldByName('AUG').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS8').AsFloat;
    if evcsImportMaster.FieldByName('SEP').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS9').AsFloat;
    if evcsImportMaster.FieldByName('OCT').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS10').AsFloat;
    if evcsImportMaster.FieldByName('NOV').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS11').AsFloat;
    if evcsImportMaster.FieldByName('DEC').AsString = 'Y' then
      Result := Result + FieldByName('CAMOUNTS12').AsFloat;
  end;

end;

function TEDIT_IM_RAPID.SumOfHours: Double;
begin
  result := 0;
  with DM, dm.evcsYearToDate do
  begin
    if evcsImportMaster.FieldByName('JAN').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS1').AsFloat;
    if evcsImportMaster.FieldByName('FEB').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS2').AsFloat;
    if evcsImportMaster.FieldByName('MAR').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS3').AsFloat;
    if evcsImportMaster.FieldByName('APR').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS4').AsFloat;
    if evcsImportMaster.FieldByName('MAY').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS5').AsFloat;
    if evcsImportMaster.FieldByName('JUN').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS6').AsFloat;
    if evcsImportMaster.FieldByName('JUL').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS7').AsFloat;
    if evcsImportMaster.FieldByName('AUG').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS8').AsFloat;
    if evcsImportMaster.FieldByName('SEP').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS9').AsFloat;
    if evcsImportMaster.FieldByName('OCT').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS10').AsFloat;
    if evcsImportMaster.FieldByName('NOV').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS11').AsFloat;
    if evcsImportMaster.FieldByName('DEC').AsString = 'Y' then
      Result := Result + FieldByName('CHOURS12').AsFloat;
  end;
end;

procedure TEDIT_IM_RAPID.InsertPR;
begin
  with DM_PAYROLL.PR do
  begin
    Insert;
    FieldByName('CO_NBR').AsInteger := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
    FieldByName('CHECK_DATE').AsDateTime := DM.evcsImportMaster.FieldByName('CHECK_DATE').AsDateTime;
    FieldByName('CHECK_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
    FieldByName('SCHEDULED').AsString := 'N';
    FieldByName('PAYROLL_TYPE').AsString := PAYROLL_TYPE_IMPORT;
{ Uncomment to status the payroll so that it will show on the que for automatically processing
    if evCheckBox1.Checked then
      FieldByName('STATUS').AsString := PAYROLL_STATUS_COMPLETED
    else
      FieldByName('STATUS').AsString := PAYROLL_STATUS_PENDING;
}

    FieldByName('STATUS').AsString := PAYROLL_STATUS_PENDING;
    FieldByName('STATUS_DATE').AsDateTime := now;
    FieldByName('EXCLUDE_ACH').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_TAX_DEPOSITS').AsString := GROUP_BOX_BOTH2;
    FieldByName('EXCLUDE_AGENCY').AsString := GROUP_BOX_YES;
    FieldByName('MARK_LIABS_PAID_DEFAULT').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_BILLING').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_R_C_B_0R_N').AsString := GROUP_BOX_BOTH2;
    FieldByName('INVOICE_PRINTED').AsString := 'N';
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.InsertPRBatch;
begin
  DM_Payroll.PR_BATCH.DataRequired('PR_NBR='+DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
  DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR='+DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);

  with DM_PAYROLL.PR_BATCH, DM_PAYROLL do
  begin
    Insert;
    FieldByName('PR_NBR').AsInteger := PR.FieldByName('PR_NBR').AsInteger;
    FieldByName('PERIOD_BEGIN_DATE').AsDateTime := DM.evcsImportMaster.FieldByName('PERIOD_BEGIN_DATE').AsDateTime;//StrToDate(PeriodBegin);
    FieldByName('PERIOD_BEGIN_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
    FieldByName('PERIOD_END_DATE').AsDateTime := DM.evcsImportMaster.FieldByName('PERIOD_END_DATE').AsDateTime;
    FieldByName('PERIOD_END_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
    FieldByName('FREQUENCY').AsString := FREQUENCY_TYPE_WEEKLY;//DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString;
    FieldByName('PAY_SALARY').AsString := GROUP_BOX_NO;
    FieldByName('PAY_STANDARD_HOURS').AsString := GROUP_BOX_NO;
    FieldByName('LOAD_DBDT_DEFAULTS').AsString := GROUP_BOX_YES;
    Post;
  end;

end;

procedure TEDIT_IM_RAPID.WriteToLog(ImType, CuEENo, FName, LName, Field_Name, Reason: String);
begin
  with DM, DM.evcsErrorLog do
  begin
    Insert;
    FieldByName('NAME').AsString := evcsImportMaster.FieldByName('NAME').AsString;
    FieldByName('CUSTOM_COMPANY_NUMBER').AsString := evcsImportMaster.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
    FieldByName('IMTYPE').AsString := IMTYPE;
    FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString := CuEENo;
    FieldByName('FNAME').AsString := FName;
    FieldByName('LNAME').AsString := LName;
    FieldByName('FIELD_NAME').AsString := Field_Name;
    FieldByName('REASON').AsString := Reason;
    FieldByName('WHEN').AsDateTime := now;
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.evbtViewLogClick(Sender: TObject);
var
  ViewLog: TEDIT_IM_VIEWLOG;
begin
  inherited;
  ViewLog := TEDIT_IM_VIEWLOG.Create(Self);
  try
    ViewLog.ShowModal;
  finally
    ViewLog.Free;
  end;
end;

procedure TEDIT_IM_RAPID.TurnTabsOff;
var
  I: Integer;
begin
  for I := 0 to evPageControl1.PageCount - 1 do
    evPageControl1.Pages[I].TabVisible := False;
end;

procedure TEDIT_IM_RAPID.ScanForEarnSets;
begin
  with DM do
  begin

   if not evcsRapidSpecial.Active then
     evcsRapidSpecial.CreateDataSet;

   evcsRapidSpecial.Filtered := False;

    evcsAutoLabor.First;
    while not evcsAutoLabor.Eof do
    begin
     if Trim(evcsAutoLabor.FieldByName('EARNSET').Value) <> '' then
       if not evcsRapidSpecial.Locate('SpecialOrPercent', evcsAutoLabor.FieldByName('EARNSET').Value, []) then
       begin
         evcsRapidSpecial.Insert;
         evcsRapidSpecial.FieldByName('SpecialOrPercent').AsString := evcsAutoLabor.FieldByName('EARNSET').AsString;
         evcsRapidSpecial.FieldByName('HASBEENMATCHED').AsString := 'F';
         evcsRapidSpecial.Post;
       end;
      evcsAutoLabor.Next;
    end;
    evcsRapidSpecial.Filtered := True;
  end;
end;

procedure TEDIT_IM_RAPID.ImportAutoLabor;
var
  MyTransManager: TTransactionManager;
begin
  MyTransManager := TTransactionManager.Create(Nil);
  MyTransManager.Initialize([DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION]);
  with DM, DM_CLIENT, DM_COMPANY, DM_EMPLOYEE do
  begin
    ctx_DataAccess.Importing := True;

    EE.OpenWithLookups := False;
    CO.OpenWithLookups := False;
    CO_DIVISION.OpenWithLookups := False;
    CO_BRANCH.OpenWithLookups := False;
    CO_DEPARTMENT.OpenWithLookups := False;
    CO_JOBS.OpenWithLookups := False;
    CL_E_D_GROUPS.OpenWithLookups := False;
    EE_AUTOLABOR_DISTRIBUTION.OpenWithLookups := False;

    EE.DisableControls;
    CO.DisableControls;
    CO_DIVISION.DisableControls;
    CO_BRANCH.DisableControls;
    CO_DEPARTMENT.DisableControls;
    CO_JOBS.DisableControls;
    CL_E_D_GROUPS.DisableControls;
    EE_AUTOLABOR_DISTRIBUTION.DisableControls;

    CL_E_D_GROUPS.DataRequired('ALL');
    CO.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);

    with TExecDSWrapper.Create('GenericSelect3CurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 't1.CO_NBR, t1.CO_DIVISION_NBR, t1.CUSTOM_DIVISION_NUMBER, t2.CO_BRANCH_NBR, t2.CUSTOM_BRANCH_NUMBER, t3.CO_DEPARTMENT_NBR, t3.CUSTOM_DEPARTMENT_NUMBER');
      SetMacro('TABLE1', 'CO_DIVISION');
      SetMacro('TABLE2', 'CO_BRANCH');
      SetMacro('TABLE3', 'CO_DEPARTMENT');
      SetMacro('JOINFIELD2', 'CO_BRANCH');
      SetMacro('JOINFIELD', 'CO_DIVISION');
      SetMacro('CONDITION', 't1.CO_NBR = :CoNbr');
      SetParam('CoNbr', DM.evcsImportMaster.FieldByName('CO_NBR').AsInteger);
      ctx_DataAccess.GetCustomData(DM.csLevelStructure, AsVariant);
    end;

    CO_DIVISION.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    CO_BRANCH.DataRequired('ALL');
    CO_DEPARTMENT.DataRequired('ALL');
    CO_TEAM.DataRequired('ALL');
    CO_JOBS.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    EE.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    EE_AUTOLABOR_DISTRIBUTION.DataRequired('CO_NBR='+evcsImportMaster.FieldByName('CO_NBR').AsString);
    if evcsAutoLabor.RecordCount <> 0 then
      try
        evcsAutoLabor.First;
        ctx_StartWait('Please wait while auto labor information is being imported.', DM.evcsAutoLabor.RecordCount-1);
        ctx_UpdateWait('Please wait while auto labor information is being imported for '+DM.evcsAutoLabor.FieldByName('EMP').AsString+'.', DM.evcsAutoLabor.RecNo);

        while not evcsAutoLabor.Eof do
        begin
          InsertAutoLabor;
          evcsAutoLabor.Next;
          ctx_UpdateWait('Please wait while auto labor information is being imported for '+DM.evcsAutoLabor.FieldByName('EMP').AsString+'.', DM.evcsAutoLabor.RecNo);
        end;
        ctx_UpdateWait('Please wait while the imported data is being committed!');
        MyTransManager.ApplyUpdates;
      finally
        EE.OpenWithLookups := True;
        CO.OpenWithLookups := True;
        CO_DIVISION.OpenWithLookups := True;
        CO_BRANCH.OpenWithLookups := True;
        CO_DEPARTMENT.OpenWithLookups := True;
        CO_JOBS.OpenWithLookups := True;
        CL_E_D_GROUPS.OpenWithLookups := True;
        EE_AUTOLABOR_DISTRIBUTION.OpenWithLookups := True;

        EE.EnableControls;
        CO.EnableControls;        
        CO_DIVISION.EnableControls;
        CO_BRANCH.EnableControls;
        CO_DEPARTMENT.EnableControls;
        CO_JOBS.EnableControls;
        CL_E_D_GROUPS.EnableControls;
        EE_AUTOLABOR_DISTRIBUTION.EnableControls;

        MyTransManager.Free;
        ctx_DataAccess.Importing := False;
        ctx_EndWait;
      end;
  end;
end;

procedure TEDIT_IM_RAPID.InsertAutoLabor;
var
  I, RapidTeer: Integer;
  V: Variant;
begin
  with DM, DM_EMPLOYEE, DM_COMPANY do
  begin
    if EE.Locate('CUSTOM_EMPLOYEE_NUMBER', PadStringLeft(evcsAutoLabor.FieldByName('EMP').Value, ' ', 9), []) then
    begin

      if evcsAutoLabor.FieldByName('EARNSET').AsString <> '' then
        if not ED_GROUP_MATCHING.Locate('SpecialOrPercent', evcsAutoLabor.FieldByName('EARNSET').Value, []) then
        begin
          WriteToLog('Auto Labor', EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, '', '', 'EARNSET', 'Could not find a match for the earnset '+Trim(evcsAutoLabor.FieldByName('EARNSET').AsString)+'.');
          Exit;
        end;
      for I := 1 to 10 do
      begin
        EE_AUTOLABOR_DISTRIBUTION.Insert;
        if Trim(evcsAutoLabor.FieldByName('Percent'+IntToStr(I)).AsString) <> '0.00' then
        begin

          if DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString <> CLIENT_LEVEL_COMPANY then
          begin

            if evcsAutoLabor.FieldByName('Division'+IntToStr(I)).AsString <> '' then
              RapidTeer := 1
            else if evcsAutoLabor.FieldByName('Branch'+IntToStr(I)).AsString <> '' then
              RapidTeer := 2
            else if evcsAutoLabor.FieldByName('Department'+IntToStr(I)).AsString <> '' then
              RapidTeer := 3
            else
              RapidTeer := 0;

            if not FilterRapidTeer(RapidTeer, DM_EMPLOYEE.EE_AUTOLABOR_DISTRIBUTION, evcsAutoLabor, 'Division'+IntToStr(I), 'Branch'+IntToStr(I), 'Department'+IntToStr(I)) then
            begin
              WriteToLog('Auto Labor', EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, '', '', 'DBDT', 'There was a DBDT setup problem.');
              Exit;
            end;
          end;

          EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_NBR').AsInteger := evcsImportMaster.FieldbyName('CO_NBR').AsInteger;
          EE_AUTOLABOR_DISTRIBUTION.FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;
          if evcsAutoLabor.FieldByName('EARNSET').AsString <> '' then
          begin
            DM_EMPLOYEE.EE.Edit;
            if DM_COMPANY.CO.FieldByName('DISTRIBUTE_DEDUCTIONS_DEFAULT').AsString <> 'Y' then
              DM_EMPLOYEE.EE.FieldByName('DISTRIBUTE_TAXES').AsString := DM_COMPANY.CO.FieldByName('DISTRIBUTE_DEDUCTIONS_DEFAULT').AsString
            else
              DM_EMPLOYEE.EE.FieldByName('DISTRIBUTE_TAXES').AsString := DISTRIBUTE_BOTH;
            DM_EMPLOYEE.EE.FieldByName('ALD_CL_E_D_GROUPS_NBR').AsInteger := ED_GROUP_MATCHING.FieldByName('CL_E_D_GROUPS_NBR').AsInteger;
            DM_EMPLOYEE.EE.Post;
          end;
//            EE_AUTOLABOR_DISTRIBUTION.FieldByName('CL_E_D_GROUPS_NBR').AsInteger := ED_GROUP_MATCHING.FieldByName('CL_E_D_GROUPS_NBR').AsInteger;

          V := CO_JOBS.Lookup('DESCRIPTION', evcsAutoLabor.FieldByName('JobNumber'+IntToStr(I)).Value, 'CO_JOBS_NBR');
          if not VarIsNull(V) then
            EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_JOBS_NBR').AsInteger := V;

          EE_AUTOLABOR_DISTRIBUTION.FieldByName('PERCENTAGE').AsFloat := StrToFloat(Trim(evcsAutoLabor.FieldByName('PERCENT'+IntToStr(I)).AsString));

          if not EE_AUTOLABOR_DISTRIBUTION.FieldByName('CO_NBR').IsNull then
            EE_AUTOLABOR_DISTRIBUTION.Post
          else
            EE_AUTOLABOR_DISTRIBUTION.Cancel;
        end;
      end;
    end
    else
     WriteToLog('Auto Labor', evcsAutoLabor.FieldByName('EMP').AsString, '', '', 'Employee', 'Could not find the referenced employee in evolution.');
  end;
end;

procedure TEDIT_IM_RAPID.Deactivate;
begin
  inherited;
  with DM_CLIENT, DM_EMPLOYEE do
  begin
    CL_E_DS.DataRequired('ALL');
    EE_DIRECT_DEPOSIT.EnableControls;
    EE_SCHEDULED_E_DS.EnableControls;
    EE.EnableControls;
  end;
  DM.Free;
end;

procedure TEDIT_IM_RAPID.TabSheet8Show(Sender: TObject);
begin
  inherited;
  evPanel2.Caption := 'Match rapid deductions to evolution taxes.';
end;

procedure TEDIT_IM_RAPID.AddTaxes;
begin
  with dm.evcsEvoTaxes do
  begin
    Insert;
    FieldByName('Tax').AsString := 'FEDERAL';
    Post;

    Insert;
    FieldByName('Tax').AsString := 'EE OASDI';
    Post;

    Insert;
    FieldByName('Tax').AsString := 'EE MEDICARE';
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.BitBtn7Click(Sender: TObject);
var
  I: Integer;
  SL: TStringList;
begin
  inherited;

  SL := TStringList.Create;
  try
    if evDBGrid1.SelectedList.Count = 0 then
    begin
      EvMessage('You have not yet selected any items in the file list to match! Please do so at this time!');
      Exit;
    end;

    with DM, evDBGrid1 do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
         evcs3rdParty.GotoBookmark(SelectedList.items[I]);
         SL.Add(evcs3rdParty.FieldByName('Deduction').AsString);
      end;
      UnselectAll;

      for I := 0 to SL.Count - 1 do
      begin
        if (evcs3rdPartyHasBeenMatched.AsString <> 'T') and (evcs3rdParty.Locate('Deduction', SL[I],[])) then
        begin
          AddPartyMatch;
          HasBeenMatched('T', 'P');
        end;
      end;

    end;
  finally
    SL.Free;
  end;
end;

procedure TEDIT_IM_RAPID.BitBtn8Click(Sender: TObject);
var
  I: Integer;
  SL: TStringList;
begin
  inherited;
  //Add code for removing matches for 3rd Party

  SL := TStringList.Create;
  try
    DM.evcs3rdParty.Filtered := False;
    with DM, wwDBGrid6 do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        PARTY_MATCHING.GotoBookmark(SelectedList.items[I]);
        SL.Add(PARTY_MATCHING.FieldByName('Deduction').AsString);
        if evcs3rdParty.Locate('Deduction', PARTY_MATCHING.FieldByName('Deduction').Value, []) then
        begin
          HasBeenMatched('F', 'P');
        end;
      end;
      UnselectAll;

      for I := 0 to SL.Count - 1  do
      begin
        if PARTY_MATCHING.Locate('Deduction', SL[I], []) then
          PARTY_MATCHING.Delete;
      end;
    end;
  finally
    DM.evcs3rdParty.Filtered := True;
    SL.Free;
  end;
end;

procedure TEDIT_IM_RAPID.evDBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  with DM do
  begin
    if evcs3rdPartyHasBeenMatched.AsString = 'T' then
    begin
      ABrush.Color := clAqua;
      if highlight then
        Afont.color := clblack;
    end
    else if ABrush.Color = clRed then
    begin
      ABrush.Color := clWhite;
      if highlight then
        Afont.color := clblack;
    end;
  end;
end;

procedure TEDIT_IM_RAPID.AddPartyMatch;
begin
  with DM, DM.PARTY_MATCHING do
  begin
    Insert;
      FieldByName('CL_NBR').AsInteger := evcsImportMaster.FieldByName('CL_NBR').AsInteger;
      FieldByName('CO_NBR').AsInteger := evcsImportMaster.FieldByName('CO_NBR').AsInteger;
      FieldbyName('Deduction').AsString := evcs3rdParty.FieldbyName('Deduction').AsString;
      FieldbyName('Tax').AsString := evcsEvoTaxes.FieldbyName('Tax').AsString;
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.CreateManualCheck(EENbr: Integer);
begin
  with DM_PAYROLL, DM_PAYROLL.PR_CHECK do
  begin
    Insert;
    FieldByName('EE_NBR').AsInteger := EENbr;
    FieldByName('PR_NBR').AsInteger := PR.FieldByName('PR_NBR').AsInteger;
    FieldByName('PR_BATCH_NBR').AsInteger := PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger;
    FieldByName('CHECK_TYPE').AsString := CHECK_TYPE2_MANUAL;
    FieldByName('UPDATE_BALANCE').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_DD').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_DD_EXCEPT_NET').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_TIME_OFF_ACCURAL').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_AUTO_DISTRIBUTION').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_SCH_E_D_FROM_AGCY_CHK').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_SCH_E_D_EXCEPT_PENSION').AsString := GROUP_BOX_NO;
    FieldByName('PRORATE_SCHEDULED_E_DS').AsString := GROUP_BOX_NO;
    FieldByName('OR_CHECK_FEDERAL_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
    FieldByName('EXCLUDE_FEDERAL').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_EMPLOYEE_OASDI').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYER_OASDI').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYEE_MEDICARE').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYER_MEDICARE').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYEE_EIC').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYER_FUI').AsString := GROUP_BOX_NO;
    FieldByName('CHECK_TYPE_945').AsString := GROUP_BOX_NO;
    FieldByName('TAX_AT_SUPPLEMENTAL_RATE').AsString := GROUP_BOX_NO;
    FieldByName('TAX_FREQUENCY').AsString := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').AsString;
    FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_CLEARED;
    FieldByName('STATUS_CHANGE_DATE').AsDateTime := now;
    FieldByName('EXCLUDE_FROM_AGENCY').AsString := GROUP_BOX_YES;
    //Will get this value from a screen setting during the import setup....
    FieldByName('CALCULATE_OVERRIDE_TAXES').Value := GROUP_BOX_NO;
    FieldByName('FEDERAL_TAX').AsFloat := 0;
    FieldByName('EE_OASDI_TAX').AsFloat := 0;
    FieldByName('EE_MEDICARE_TAX').AsFloat := 0;
    FieldByName('EE_EIC_TAX').AsFloat := 0;
    post;
  end;
end;

procedure TEDIT_IM_RAPID.Edit3rdPartyCheck(SumAmounts, SumHours: Double);
var
  aString: String;
begin
  with DM, DM_PAYROLL, DM_PAYROLL.PR_CHECK do
  begin
    if dm.evcsYearToDate.FieldByName('DET').AsString = 'D' then
    begin
      if DM.PARTY_MATCHING.Locate('Deduction', evcsYearToDate.FieldByName('DECODE').Value, []) then
      begin
        aString := PARTY_MATCHING.FieldByName('TAX').AsString;
        if aString = 'FEDERAL' then
        begin
          edit;
          FieldByName('FEDERAL_TAX').AsFloat := SumAmounts;
          Post;
        end;
        if aString = 'EE OASDI' then
        begin
          edit;
          FieldByName('EE_OASDI_TAX').AsFloat := SumAmounts;
          post;
        end;
        if aString =  'EE MEDICARE' then
        begin
          edit;
          FieldByName('EE_MEDICARE_TAX').AsFloat := SumAmounts;
          post;
        end;
      end;
    end;
  end;
end;

procedure TEDIT_IM_RAPID.EditManualCheck(SumAmounts, SumHours: Double);
var
  aString: String;
begin
  with DM_PAYROLL, DM_PAYROLL.PR_CHECK do
  begin
    if dm.evcsYearToDate.FieldByName('DET').AsString = 'T' then
    begin
      aString := Trim(dm.evcsYearToDate.FieldByName('DECODE').AsString);
      if aString = '1' then
      begin
        edit;
        FieldByName('FEDERAL_TAX').AsFloat := SumAmounts;
        Post;
      end;
      if aString = '2' then
      begin
        edit;
        FieldByName('EE_OASDI_TAX').AsFloat := SumAmounts;
        post;
      end;
      if aString =  '3' then
      begin
        edit;
        FieldByName('EE_MEDICARE_TAX').AsFloat := SumAmounts;
        post;
      end;
      if aString = '4' then
      begin
        edit;
        FieldByName('EE_EIC_TAX').AsFloat := SumAmounts*-1;
        post;
      end;
    end;
  end;
end;

procedure TEDIT_IM_RAPID.Create3rdPartyCheck(EENbr: Integer);
begin
  with DM_PAYROLL, DM_PAYROLL.PR_CHECK do
  begin
    Insert;
    FieldByName('EE_NBR').AsInteger := EENbr;
    FieldByName('PR_NBR').AsInteger := PR.FieldByName('PR_NBR').AsInteger;
    FieldByName('PR_BATCH_NBR').AsInteger := PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger;
    FieldByName('CHECK_TYPE').AsString := CHECK_TYPE2_3RD_PARTY;
    FieldByName('EXCLUDE_DD').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_DD_EXCEPT_NET').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_TIME_OFF_ACCURAL').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_AUTO_DISTRIBUTION').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_SCH_E_D_FROM_AGCY_CHK').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_SCH_E_D_EXCEPT_PENSION').AsString := GROUP_BOX_NO;
    FieldByName('PRORATE_SCHEDULED_E_DS').AsString := GROUP_BOX_NO;
    FieldByName('OR_CHECK_FEDERAL_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
    FieldByName('EXCLUDE_FEDERAL').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').AsString := GROUP_BOX_YES;
    FieldByName('EXCLUDE_EMPLOYEE_OASDI').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYER_OASDI').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYEE_MEDICARE').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYER_MEDICARE').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYEE_EIC').AsString := GROUP_BOX_NO;
    FieldByName('EXCLUDE_EMPLOYER_FUI').AsString := GROUP_BOX_NO;
    FieldByName('CHECK_TYPE_945').AsString := CHECK_TYPE_945_NONE;
    FieldByName('TAX_AT_SUPPLEMENTAL_RATE').AsString := GROUP_BOX_NO;
    FieldByName('TAX_FREQUENCY').AsString := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').AsString;
    FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_CLEARED;
    FieldByName('STATUS_CHANGE_DATE').AsDateTime := now;
    FieldByName('EXCLUDE_FROM_AGENCY').AsString := GROUP_BOX_YES;
    FieldByName('CALCULATE_OVERRIDE_TAXES').Value := GROUP_BOX_NO;        
    FieldByName('FEDERAL_TAX').AsFloat := 0;
    FieldByName('EE_OASDI_TAX').AsFloat := 0;
    FieldByName('EE_MEDICARE_TAX').AsFloat := 0;
    FieldByName('EE_EIC_TAX').AsFloat := 0;
    post;
  end;
end;

procedure TEDIT_IM_RAPID.CreateCheckLine(EDNumber: Integer; SumHours, SumAmounts : Double; EEState: Integer);
begin
  with DM, DM_PAYROLL, DM_PAYROLL.PR_CHECK_LINES do
  begin
    insert;
    FieldByName('CL_E_DS_NBR').AsInteger := GetEDNumber(evcsYearToDate);
    FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
    FieldByName('LINE_TYPE').Value := CHECK_LINE_TYPE_USER;
    FieldByName('HOURS_OR_PIECES').AsFloat := SumHours;
    if (evcsYearToDate.FieldByName('DETYPE').AsString = 'K') or
       (evcsYearToDate.FieldByName('DETYPE').AsString = 'J') or
       (evcsYearToDate.FieldByName('DETYPE').AsString = 'N') or
       (evcsYearToDate.FieldByName('DETYPE').AsString = 'Q') or
       (evcsYearToDate.FieldByName('DETYPE').AsString = 'X') then
       SumAmounts := -SumAmounts;
    FieldByName('AMOUNT').AsFloat := SumAmounts;
    if Trim(dm.evcsYearToDate.FieldByName('STATETAXCODE1').AsString) <> '' then
    begin
       FieldByName('EE_STATES_NBR').AsInteger := GetEEStatesNumber(evcsYearToDate);
       if FieldByName('EE_STATES_NBR').AsInteger = 0 then
       begin
         Cancel;
         WriteToLog('Year to Dates', evcsYearToDate.FieldByName('EMPL').AsString, '',
                    '', 'CO_STATES_NBR', 'The state '+evcsYearToDate.FieldByName('Statetaxcode1').AsString+' is not setup in evolution.');
         Exit;
       end;
    end
    else
    begin
//    Attatch EE Home Tax State
      FieldByName('EE_STATES_NBR').AsInteger := DM_EMPLOYEE.EE.FieldByName('HOME_TAX_EE_STATES_NBR').AsInteger;
      if FieldByName('EE_STATES_NBR').AsInteger = 0 then
      begin
         Cancel;
         WriteToLog('Year to Dates', evcsYearToDate.FieldByName('EMPL').AsString, '',
                    '', 'EE_STATES_NBR', 'The Employee does not have a home TAX state attatched.');
         Exit;
      end
    end;

    if (State in [dsInsert, dsEdit]) and (Trim(evcsYearToDate.FieldByName('STATETAXCODE3').AsString) <> '') then
    begin
      FieldByName('EE_SUI_STATES_NBR').AsInteger := CheckForAndInsertEEStates(PR_CHECK['EE_NBR'], evcsYearToDate.FieldByName('Statetaxcode1').AsString, evcsYearToDate.FieldByName('Statetaxcode2').AsString, evcsYearToDate.FieldByName('Statetaxcode3').AsString);//GetEEStatesNumber('STATETAXCODE3', evcsYearToDate);
      if FieldByName('EE_SUI_STATES_NBR').AsInteger = 0 then
      begin
        Cancel;
        WriteToLog('Year to Dates', evcsYearToDate.FieldByName('EMPL').AsString, '',
                   '', 'CO_STATES_NBR', 'The state '+evcsYearToDate.FieldByName('Statetaxcode3').AsString+' is not setup in evolution for SUI.');
        Exit;
      end;
    end
    else
    begin
      DM_EMPLOYEE.EE_STATES.DataRequired('EE_STATES_NBR=' + FieldByName('EE_STATES_NBR').AsString);
      FieldByName('EE_SUI_STATES_NBR').AsInteger := DM_EMPLOYEE.EE_STATES.SUI_APPLY_CO_STATES_NBR.AsInteger;
      if FieldByName('EE_SUI_STATES_NBR').AsInteger = 0 then
      begin
         Cancel;
         WriteToLog('Year to Dates', evcsYearToDate.FieldByName('EMPL').AsString, '',
                    '', 'EE_STATES_NBR', 'The Employee''s home state does not have a SUI state attatched.');
         Exit;
      end
    end;

    if State in [dsInsert, dsEdit] then
      Post;
  end;
end;

procedure TEDIT_IM_RAPID.HandleNYSDI(EDNumber: Integer; SumHours,
  SumAmounts: Double);
var
  aString: String;
begin
  with DM, DM_PAYROLL, DM_PAYROLL.PR_CHECK_LINES, DM_CLIENT do
  begin
    aString := Trim(evcsYearToDate.FieldByName('DECODE').AsString);
    if (DM_CLIENT.CL_E_DS.Locate('E_D_CODE_TYPE', 'DZ', [])) and (aString = '7') then
    begin
      Insert;
      FieldByName('CL_E_DS_NBR').AsInteger := CL_E_DS.FieldByName('CL_E_DS_NBR').AsInteger;
      FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
      FieldByName('LINE_TYPE').AsString := CHECK_LINE_TYPE_USER;
      FieldByName('HOURS_OR_PIECES').AsFloat := SumHours;
      if (evcsYearToDate.FieldByName('DETYPE').AsString = 'K') or
         (evcsYearToDate.FieldByName('DETYPE').AsString = 'J') or
         (evcsYearToDate.FieldByName('DETYPE').AsString = 'Q') or
         (evcsYearToDate.FieldByName('DETYPE').AsString = 'X') then
        SumAmounts := -SumAmounts;
      FieldByName('AMOUNT').AsFloat := SumAmounts;
      if Trim(evcsYearToDate.FieldByName('STATETAXCODE1').AsString) <> '' then
      begin
         FieldByName('EE_STATES_NBR').AsInteger := GetEEStatesNumber(evcsYearToDate);
         if FieldByName('EE_STATES_NBR').AsInteger = 0 then
         begin
           Cancel;
           WriteToLog('Year to Dates', evcsYearToDate.FieldByName('EMPL').AsString, '',
                      '', 'CO_STATES_NBR', 'The state '+evcsYearToDate.FieldByName('Statetaxcode1').AsString+' is not setup in evolution.');
           Exit;
         end;
      end;
      if (State in [dsInsert, dsEdit]) and (Trim(evcsYearToDate.FieldByName('STATETAXCODE3').AsString) <> '') then
      begin
        FieldByName('EE_SUI_STATES_NBR').AsInteger := CheckForAndInsertEEStates(PR_CHECK['EE_NBR'], evcsYearToDate.FieldByName('Statetaxcode1').AsString, evcsYearToDate.FieldByName('Statetaxcode2').AsString, evcsYearToDate.FieldByName('Statetaxcode3').AsString); //GetEEStatesNumber('STATETAXCODE3' ,evcsYearToDate);
        if FieldByName('EE_SUI_STATES_NBR').AsInteger = 0 then
        begin
          Cancel;
           WriteToLog('Year to Dates', evcsYearToDate.FieldByName('EMPL').AsString, '',
                      '', 'CO_STATES_NBR', 'The state '+evcsYearToDate.FieldByName('Statetaxcode3').AsString+' is not setup in evolution for SUI.');
          exit;
        end;
      end;

      if State in [dsInsert, dsEdit] then
        Post;
    end;
  end;
end;

function TEDIT_IM_RAPID.InsertYTDState: Integer;
var
  V: Variant;
begin

  Result := 0;
  with DM_SYSTEM_STATE, DM_CLIENT, DM_COMPANY, DM_EMPLOYEE, DM_EMPLOYEE.EE_STATES, DM do
  begin
    if DM_COMPANY.CO_STATES.Locate('STATE', evcsYearToDate.FieldByName('STATETAXCODE1').Value, []) then
    begin
      Insert;
      FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;
      FieldByName('CO_STATES_NBR').AsInteger := CO_STATES.FieldByName('CO_STATES_NBR').AsInteger;;
      FieldByName('STATE_MARITAL_STATUS').AsString := SY_STATE_MARITAL_STATUS.LookUp('SY_STATES_NBR', CO_STATES.FieldByName('SY_STATES_NBR').Value, 'STATUS_TYPE');
      FieldByName('SY_STATE_MARITAL_STATUS_NBR').AsString := SY_STATE_MARITAL_STATUS.LookUp('SY_STATES_NBR', CO_STATES.FieldByName('SY_STATES_NBR').Value, 'SY_STATE_MARITAL_STATUS_NBR');
      FieldByName('STATE_NUMBER_WITHHOLDING_ALLOW').AsInteger := EE.FieldByName('NUMBER_OF_DEPENDENTS').AsInteger;
      FieldByName('IMPORTED_MARITAL_STATUS').Assign(EE.FieldByName('FEDERAL_MARITAL_STATUS'));
      FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
      if evcsYearToDate.FieldByName('Statetaxcode2').AsString <> '' then
      begin
        V := DM_COMPANY.CO_STATES.Lookup('STATE', evcsYearToDate.FieldByName('Statetaxcode2').Value, 'CO_STATES_NBR');
        if not VARISNULL(V) then
          FieldByName('SDI_APPLY_CO_STATES_NBR').AsInteger := V
        else
          WriteToLog('Year To Date(STATES)', evcsYearToDate.FieldByName('EMPL').AsString, EE.FieldByName('FIRST_NAME').AsString,
                  EE.FieldByName('LAST_NAME').AsString, 'CO_STATES_NBR', 'The state '+evcsYearToDate.FieldByName('Statetaxcode2').AsString+' is not setup in evolution at the company level. Unable to attach SDI state');
      end;

      if evcsYearToDate.FieldByName('Statetaxcode3').AsString <> '' then
      begin
        V := DM_COMPANY.CO_STATES.Lookup('STATE', evcsYearToDate.FieldByName('Statetaxcode3').Value, 'CO_STATES_NBR');
        if not VARISNULL(V) then
          FieldByName('SUI_APPLY_CO_STATES_NBR').AsInteger := V
        else
          WriteToLog('Year To Date(STATES)', evcsYearToDate.FieldByName('EMPL').AsString, EE.FieldByName('FIRST_NAME').AsString,
                  EE.FieldByName('LAST_NAME').AsString, 'CO_STATES_NBR', 'The state '+evcsYearToDate.FieldByName('Statetaxcode3').AsString+' is not setup in evolution at the company level. Unable to attach SUI state');
      end;

      FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('ER_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('RECIPROCAL_METHOD').AsString := STATE_RECIPROCAL_TYPE_NONE;
      FieldByName('CALCULATE_TAXABLE_WAGES_1099').AsString := GROUP_BOX_NO;
      Post;

      Result := FieldByName('EE_STATES_NBR').AsInteger;
    end
    else
      WriteToLog('Year To Date(STATES)', evcsYearToDate.FieldByName('EMPL').AsString, CL_PERSON.FieldByName('FIRST_NAME').AsString,
                  CL_PERSON.FieldByName('LAST_NAME').AsString, 'CO_STATES_NBR', 'The state '+evcsYearToDate.FieldByName('Statetaxcode1').AsString+' is not setup in evolution at the company level.');
  end;

end;

procedure TEDIT_IM_RAPID.wwDBGrid7RowChanged(Sender: TObject);
begin
  inherited;
  evBitBtn2.Enabled := (LastClientSetup = DM.evdsImportMaster.DataSet.FieldByName('CL_NBR').AsInteger);
end;

procedure TEDIT_IM_RAPID.evrgNewOldSetupClick(Sender: TObject);
begin
  inherited;

    evdgSelectCompany.Enabled := not (evrgNewOldSetup.ItemIndex = 1);
    evdgSelectCompany.SecurityRO := not evdgSelectCompany.Enabled;
    evclImportChoices.Enabled := not (evrgNewOldSetup.ItemIndex = 1);

end;

procedure TEDIT_IM_RAPID.TabSheet9Show(Sender: TObject);
begin
  inherited;
  DM_COMPANY.CO_SUI.DataRequired('CO_NBR='+DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString);
  DM.evdsCOSUI.DataSet := DM_COMPANY.CO_SUI;
  evDBGrid5.DataSource := DM.evdsCOSUI;
  evPanel2.Caption := 'Please match taxes 7,8,9,10,16 to a SUI.';
end;

procedure TEDIT_IM_RAPID.ScanForYTDT7_T16;
begin

   with DM do
   begin
     if not evcsNJTaxSui.Active then
       evcsNJTaxSui.CreateDataSet;

     evcsNJTaxSui.Filtered := False;

     evcsYearToDate.First;
     while not evcsYearToDate.Eof do
     begin
       if ((evcsYearToDate['DET'] = 'T') or  (evcsYearToDate['DET'] = 'T')) and
          ((Trim(evcsYearToDate['DECODE']) = '7') or (Trim(evcsYearToDate['DECODE']) = '9') or (Trim(evcsYearToDate['DECODE']) = '10') or (evcsYearToDate['DECODE'] = '16')) and
          (evcsYearToDate['STATETAXCODE2'] = 'NJ') then
       begin
         if not evcsNJTaxSui.Locate('TaxType', Trim(evcsYearToDate['DECODE']), []) then
         begin
           evcsNJTaxSui.Insert;
           evcsNJTaxSui.FieldByName('TaxType').AsString := Trim(evcsYearToDate.FieldByName('DECODE').AsString);
           evcsNJTaxSui.FieldByName('STATE').AsString := Trim(evcsYearToDate.FieldByName('STATETAXCODE2').AsString);
           evcsNJTaxSui.FieldByName('HASBEENMATCHED').AsString := 'F';
           evcsNJTaxSui.Post;
         end;
       end;
       if (evcsYearToDate['DET'] = 'T') and  (Trim(evcsYearToDate['DECODE']) = '8') then
       begin
         if not evcsNJTaxSui.Locate('TaxType', Trim(evcsYearToDate['DECODE']), []) then
         begin
           evcsNJTaxSui.Insert;
           evcsNJTaxSui.FieldByName('TaxType').AsString := Trim(evcsYearToDate.FieldByName('DECODE').AsString);
           evcsNJTaxSui.FieldByName('STATE').AsString := Trim(evcsYearToDate.FieldByName('STATETAXCODE2').AsString);
           evcsNJTaxSui.FieldByName('HASBEENMATCHED').AsString := 'F';
           evcsNJTaxSui.Post;
         end;
       end;
       evcsYearToDate.Next;
     end;
     evcsNJTaxSui.Filtered := True;
   end;
end;

procedure TEDIT_IM_RAPID.BitBtn9Click(Sender: TObject);
var
 I: Integer;
 SL: TStringList;
begin
  inherited;

  SL := TStringList.Create;
  try
    if evDBGrid4.SelectedList.Count = 0 then
    begin
      EvMessage('You have not yet selected any items in the file list to match! Please do so at this time!');
      Exit;
    end;

    with DM, evDBGrid4 do
    begin
      for I := 0 to SelectedList.Count - 1 do
      begin
        evcsNJTaxSui.GotoBookmark(SelectedList.items[I]);
        SL.Add(evcsNJTaxSui.FieldByName('TaxType').AsString);
      end;
      UnselectAll;

      for I := 0 to SL.Count - 1 do
      begin
        if (evcsNJTaxSuiHasBeenMatched.AsString <> 'T') and (evcsNJTaxSui.Locate('TaxType', SL[I],[])) then
        begin
          AddNJTaxMatch;
          HasBeenMatched('T', 'N');
        end;
      end;

    end;
  finally
    SL.Free;
  end;
end;

procedure TEDIT_IM_RAPID.AddNJTaxMatch;
begin
  with DM, DM.NJ_TAX, DM_COMPANY do
  begin
    Insert;
    FieldByName('CL_NBR').AsInteger := evcsImportMaster.FieldByName('CL_NBR').AsInteger;
    FieldByName('CO_NBR').AsInteger := evcsImportMaster.FieldByName('CO_NBR').AsInteger;    
    FieldByName('TaxType').AsString := evcsNJTaxSui.FieldByName('TaxType').AsString;
    FieldByName('STATE').AsString := evcsNJTaxSui.FieldByName('STATE').AsString;
    FieldByName('DESCRIPTION').AsString := CO_SUI.FieldByName('DESCRIPTION').AsString;
    FieldByName('CO_SUI_NBR').AsInteger := CO_SUI.FieldByName('CO_SUI_NBR').AsInteger;
    Post;
  end;
end;

procedure TEDIT_IM_RAPID.Handle_NJ_T7_T16(SumAmounts: Double);
begin
  with DM, DM_PAYROLL, DM_PAYROLL.PR_CHECK_SUI do
  begin
    if NJ_TAX.Locate('TAXTYPE', Trim(evcsYearToDate.FieldByName('DECODE').Value), []) then
    begin
      Insert;
      FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
      FieldByName('CO_SUI_NBR').AsInteger := NJ_TAX.FieldByName('CO_SUI_NBR').AsInteger;
      FieldByName('SUI_TAX').AsFloat := SumAmounts;
      Post;
    end;
  end;
end;

function TEDIT_IM_RAPID.CheckForAndInsertEEStates(EENbr: Integer; HomeState, SDI,
  SUI: String): Integer;
var
  EEState, CurSUI, CoStateNbr: Integer;
  function GetCoStateNbr(sState: String): Integer;
  var
    V: Variant;
  begin
    V := DM_COMPANY.CO_STATES.Lookup('STATE', sState, 'CO_STATES_NBR');
    if not VarIsNull(V) then
      result := V
    else
     result := 0;
  end;
begin
  Result := 0;
  EEState := GetEEStatesNumber('STATETAXCODE3', DM.evcsYearToDate);
  if EEState <> 0 then
  begin
    Result := EEState;
    Exit;
  end;

  with DM_EMPLOYEE do
  begin
   CoStateNbr := GetCoStateNbr(SUI);
   if CoStateNbr <> 0 then
     if EE_STATES.Locate('EE_NBR;CO_STATES_NBR', VarArrayOf([EENbr, CoStateNbr]), []) then
     begin
       CurSUI := EE_STATES.FieldByName('SUI_APPLY_CO_STATES_NBR').AsInteger;
       Result := InsertYTDState(CurSui, CurSui, CoStateNbr);
     end
     else
      Result := InsertYTDState(CoStateNbr, CoStateNbr, CoStateNbr);
  end;

end;

function TEDIT_IM_RAPID.InsertYTDState(HomeNbr, SDINbr, SUINbr: Integer): Integer;
begin

  with DM_SYSTEM_STATE, DM_CLIENT, DM_COMPANY, DM_EMPLOYEE, DM_EMPLOYEE.EE_STATES, DM do
  begin
    if not Locate('EE_NBR;CO_STATES_NBR', VarArrayOf([EE.FieldByName('EE_NBR').Value, HomeNbr]), []) then
    begin
      Insert;
      FieldByName('EE_NBR').AsInteger := EE.FieldByName('EE_NBR').AsInteger;
      FieldByName('CO_STATES_NBR').AsInteger := HomeNbr;
      FieldByName('STATE_MARITAL_STATUS').AsString := SY_STATE_MARITAL_STATUS.LookUp('SY_STATES_NBR', CO_STATES.FieldByName('SY_STATES_NBR').Value, 'STATUS_TYPE');
      FieldByName('SY_STATE_MARITAL_STATUS_NBR').AsString := SY_STATE_MARITAL_STATUS.LookUp('SY_STATES_NBR', CO_STATES.FieldByName('SY_STATES_NBR').Value, 'SY_STATE_MARITAL_STATUS_NBR');
      FieldByName('STATE_NUMBER_WITHHOLDING_ALLOW').AsInteger := EE.FieldByName('NUMBER_OF_DEPENDENTS').AsInteger;
      FieldByName('IMPORTED_MARITAL_STATUS').Assign(EE.FieldByName('FEDERAL_MARITAL_STATUS'));
      FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
      FieldByName('SDI_APPLY_CO_STATES_NBR').AsInteger := SDINbr;
      FieldByName('SUI_APPLY_CO_STATES_NBR').AsInteger := SUINbr;
      FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('ER_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE;
      FieldByName('RECIPROCAL_METHOD').AsString := STATE_RECIPROCAL_TYPE_NONE;
      FieldByName('CALCULATE_TAXABLE_WAGES_1099').AsString := GROUP_BOX_NO;
      Post;
    end;
    Result := FieldByName('EE_STATES_NBR').AsInteger;
  end;

end;

procedure TEDIT_IM_RAPID.evrbOtherClick(Sender: TObject);
begin
  inherited;
  evedOther.Enabled := True;
end;

procedure TEDIT_IM_RAPID.evrbFixedClick(Sender: TObject);
begin
  inherited;
  evedOther.Enabled := False;
end;

procedure TEDIT_IM_RAPID.evrbCommaClick(Sender: TObject);
begin
  inherited;
  evedOther.Enabled := False;
end;

procedure TEDIT_IM_RAPID.evrbSemiColonClick(Sender: TObject);
begin
  inherited;
  evedOther.Enabled := False;
end;

procedure TEDIT_IM_RAPID.evrbTabClick(Sender: TObject);
begin
  inherited;
  evedOther.Enabled := False;
end;

function TEDIT_IM_RAPID.LoadSetupFile: Boolean;
var
  I: IevDualStream;
  F: String;
begin
  Result := True;

  if opdgSetupFile.Execute then
    F := opdgSetupFile.FileName
  else
  begin
    Result := False;
    Exit;
  end;

  I := TEvDualStreamHolder.CreateFromFile(F);
  DM.evcsImportMaster.LoadFromUniversalStream(I.ReadStream(nil));
  DM.LOCAL_MATCHING.LoadFromUniversalStream(I.ReadStream(nil));
  DM.ED_MATCHING.LoadFromUniversalStream(I.ReadStream(nil));
  DM.ED_GROUP_MATCHING.LoadFromUniversalStream(I.ReadStream(nil));
  DM.DD_MATCHING.LoadFromUniversalStream(I.ReadStream(nil));
  DM.PARTY_MATCHING.LoadFromUniversalStream(I.ReadStream(nil));
  DM.NJ_TAX.LoadFromUniversalStream(I.ReadStream(nil));

end;

procedure TEDIT_IM_RAPID.CheckPrCheckStates;
var
  PrNbr: Integer;
  PrCheckFilter: string;
  PrCheckFiltered: Boolean;
  PrCheckStatesIndex: string;
  EEIndex: string;
begin
  PrNbr := DM_PAYROLL.PR.PR_NBR.Value;
  PrCheckFilter := DM_PAYROLL.PR_CHECK.Filter;
  PrCheckFiltered := DM_PAYROLL.PR_CHECK.Filtered;
  PrCheckStatesIndex := DM_PAYROLL.PR_CHECK_STATES.IndexFieldNames;
  EEIndex := DM_EMPLOYEE.EE.IndexFieldNames;
  try
    DM_PAYROLL.PR_CHECK.Filter := 'PR_NBR=' + IntToStr(PrNbr);
    DM_PAYROLL.PR_CHECK.Filtered := True;
    DM_PAYROLL.PR_CHECK_STATES.IndexFieldNames := 'PR_CHECK_NBR';
    DM_EMPLOYEE.EE.IndexFieldNames := 'EE_NBR';

    DM_PAYROLL.PR_CHECK.First;
    while not DM_PAYROLL.PR_CHECK.Eof do
    begin
      if not DM_PAYROLL.PR_CHECK_STATES.FindKey([DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value]) then
        with DM_PAYROLL.PR_CHECK_STATES do
        begin
          Assert(DM_EMPLOYEE.EE.FindKey([DM_PAYROLL.PR_CHECK.EE_NBR.Value]));
          Append;
          PR_CHECK_NBR.Value := DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value;
          EE_STATES_NBR.Value := DM_EMPLOYEE.EE.HOME_TAX_EE_STATES_NBR.Value;
          TAX_AT_SUPPLEMENTAL_RATE.Value := GROUP_BOX_NO;
          EXCLUDE_STATE.Value := GROUP_BOX_NO;
          EXCLUDE_ADDITIONAL_STATE.Value := GROUP_BOX_YES;
          EXCLUDE_SDI.Value := GROUP_BOX_NO;
          EXCLUDE_SUI.Value := GROUP_BOX_NO;
          STATE_OVERRIDE_TYPE.Value := OVERRIDE_VALUE_TYPE_NONE;
          STATE_TAX.Value := 0;
          EE_SDI_TAX.Value := 0;
          ER_SDI_TAX.Value := 0;
          Post;
        end;
      DM_PAYROLL.PR_CHECK.Next;
    end;
  finally
    DM_PAYROLL.PR_CHECK.Filter := PrCheckFilter;
    DM_PAYROLL.PR_CHECK.Filtered := PrCheckFiltered;
    DM_PAYROLL.PR_CHECK_STATES.IndexFieldNames := PrCheckStatesIndex;
    DM_EMPLOYEE.EE.IndexFieldNames := EEIndex;
  end;
end;

initialization
  RegisterClass(TEDIT_IM_RAPID);

end.
