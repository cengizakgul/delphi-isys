inherited EDIT_SB_MAIL_BOX: TEDIT_SB_MAIL_BOX
  inherited PC: TevPageControl
    OnChange = PCChange
    inherited tshtBrowse: TTabSheet
      object Splitter1: TSplitter [0]
        Left = 313
        Top = 0
        Width = 8
        Height = 237
        Beveled = True
        Color = clGradientInactiveCaption
        ParentColor = False
      end
      object Panel1: TPanel [1]
        Left = 0
        Top = 0
        Width = 313
        Height = 237
        Align = alLeft
        Caption = 'Panel1'
        TabOrder = 1
        object EvDBMemo2: TEvDBMemo
          Left = 1
          Top = 137
          Width = 311
          Height = 99
          Align = alClient
          DataField = 'NOTE'
          DataSource = wwdsDetail
          TabOrder = 0
        end
        object PC2: TevPageControl
          Left = 1
          Top = 1
          Width = 311
          Height = 136
          ActivePage = tsUnreleased
          Align = alTop
          TabOrder = 1
          OnChange = PC2Change
          object tsUnreleased: TTabSheet
            Caption = 'Unreleased'
            object lbVoucherBlocked: TevLabel
              Left = 6
              Top = 64
              Width = 354
              Height = 16
              Caption = 
                '~Send vouchers was blocked for this mail box. Use ''Voucher Email' +
                'ing'' tab.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              Visible = False
              WordWrap = True
            end
            object evButton1: TevButton
              Left = 8
              Top = 8
              Width = 75
              Height = 25
              Caption = 'Release'
              TabOrder = 0
              OnClick = evButton1Click
              Color = clBlack
              Margin = 0
            end
            object cbBlockVouchers: TCheckBox
              Left = 6
              Top = 44
              Width = 227
              Height = 17
              Caption = 'Also Send Automatically Emailed Vouchers'
              Checked = True
              State = cbChecked
              TabOrder = 1
              OnClick = cbBlockVouchersClick
            end
          end
          object tsReleased: TTabSheet
            Caption = 'Ready for Remote Printing'
            ImageIndex = 5
            object bRevert2: TevButton
              Left = 16
              Top = 8
              Width = 137
              Height = 25
              Caption = 'Revert to Unreleased'
              TabOrder = 0
              OnClick = bRevertClick
              Color = clBlack
              Margin = 0
            end
          end
          object tsPrinted: TTabSheet
            Caption = 'Printed'
            ImageIndex = 1
            object bRevert: TevButton
              Left = 16
              Top = 8
              Width = 137
              Height = 25
              Caption = 'Revert to Unreleased'
              TabOrder = 0
              OnClick = bRevertClick
              Color = clBlack
              Margin = 0
            end
          end
          object tsScanned: TTabSheet
            Caption = 'Scanned'
            ImageIndex = 2
          end
          object tsCleanup: TTabSheet
            Caption = 'Clean'
            ImageIndex = 3
            object evbtCleanMailBox: TevButton
              Left = 32
              Top = 29
              Width = 105
              Height = 25
              Caption = 'Clean Mail Box'
              TabOrder = 0
              OnClick = evbtCleanMailBoxClick
              Color = clBlack
              Margin = 0
            end
          end
          object tsEmailing: TTabSheet
            Caption = 'Voucher Emailing'
            ImageIndex = 4
            object Label3: TLabel
              Left = 6
              Top = 2
              Width = 81
              Height = 13
              Caption = 'Check date from:'
            end
            object Label4: TLabel
              Left = 6
              Top = 39
              Width = 70
              Height = 13
              Caption = 'Check date to:'
            end
            object dtFrom: TDateTimePicker
              Left = 5
              Top = 16
              Width = 114
              Height = 21
              Date = 39328.517383715280000000
              Time = 39328.517383715280000000
              TabOrder = 0
            end
            object dtTo: TDateTimePicker
              Left = 5
              Top = 53
              Width = 114
              Height = 21
              Date = 39328.517445555550000000
              Time = 39328.517445555550000000
              TabOrder = 1
            end
            object evButton4: TevButton
              Tag = 1
              Left = 85
              Top = 80
              Width = 91
              Height = 25
              Caption = 'Send vouchers'
              TabOrder = 2
              OnClick = evButton1Click
              Color = clBlack
              Margin = 0
            end
            object evButton2: TevButton
              Left = 5
              Top = 80
              Width = 75
              Height = 25
              Caption = 'Build List'
              TabOrder = 3
              OnClick = PC2Change
              Color = clBlack
              Margin = 0
            end
          end
        end
      end
      inherited gBrowse: TevDBGrid
        Left = 321
        Width = 106
        Selected.Strings = (
          'calcCleanUp'#9'2'#9'Clean Up'#9'F'
          'DESCRIPTION'#9'20'#9'Description'#9'F'
          'calcStatus'#9'25'#9'Status'#9'F'
          'luServiceName'#9'10'#9'Service'#9'F'
          'luMethodName'#9'10'#9'Method'#9'F'
          'luMediaName'#9'10'#9'Media'#9'F'
          'PAGE_COUNT'#9'5'#9'Pages'#9'F'
          'luClNbr'#9'8'#9'Cl#'#9'F'
          'luClName'#9'20'#9'Client'#9'F'
          'luPrCheckDate'#9'10'#9'Payroll'#9'F'
          'luPrRunNumber'#9'2'#9'Pr. Run#'#9'F'
          'CREATED_TIME'#9'18'#9'Created'#9'F'
          'RELEASED_TIME'#9'18'#9'Released'#9'F'
          'PRINTED_TIME'#9'18'#9'Printed'#9'F'
          'SCANNED_TIME'#9'18'#9'Scanned'#9'F'
          'BARCODE'#9'8'#9'Barcode'#9'F'
          'TRACKING_INFO'#9'25'#9'Tracking Info'#9'F'
          'ADDRESSEE'#9'512'#9'Addressee'#9'F')
        IniAttributes.SectionName = 'TEDIT_SB_MAIL_BOX\gBrowse'
        Align = alClient
        DataSource = wwdsMaster
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        OnCalcCellColors = gBrowseCalcCellColors
      end
    end
    inherited tshtDetails: TTabSheet
      object PC3: TPageControl
        Left = 185
        Top = 0
        Width = 242
        Height = 237
        ActivePage = tsDetailDetail
        Align = alClient
        TabOrder = 0
        object tsDetailDetail: TTabSheet
          Caption = 'Details'
          object evLabel5: TevLabel
            Left = 8
            Top = 8
            Width = 62
            Height = 16
            Caption = '~Description'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel6: TevLabel
            Left = 8
            Top = 32
            Width = 73
            Height = 16
            Caption = '~Auto Release'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object evLabel7: TevLabel
            Left = 8
            Top = 64
            Width = 84
            Height = 13
            Caption = 'Notification E-mail'
          end
          object evLabel8: TevLabel
            Left = 8
            Top = 88
            Width = 100
            Height = 13
            Caption = 'System Cover Report'
          end
          object evLabel9: TevLabel
            Left = 8
            Top = 112
            Width = 85
            Height = 13
            Caption = 'S/B Cover Report'
          end
          object evLabel11: TevLabel
            Left = 8
            Top = 232
            Width = 85
            Height = 13
            Caption = 'Uplinked Mail Box'
          end
          object evLabel10: TevLabel
            Left = 8
            Top = 136
            Width = 23
            Height = 13
            Caption = 'Note'
          end
          object evLabel2: TevLabel
            Left = 8
            Top = 272
            Width = 80
            Height = 13
            Caption = 'Mail Box Created'
          end
          object evLabel4: TevLabel
            Left = 8
            Top = 296
            Width = 88
            Height = 13
            Caption = 'Mail Box Released'
          end
          object evLabel12: TevLabel
            Left = 8
            Top = 352
            Width = 61
            Height = 13
            Caption = 'Linked Client'
          end
          object evLabel13: TevLabel
            Left = 8
            Top = 376
            Width = 66
            Height = 13
            Caption = 'Linked Payroll'
          end
          object lLinkedClient: TevLabel
            Left = 120
            Top = 352
            Width = 75
            Height = 13
            Caption = 'Linked Client'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lLinkedPayroll: TevLabel
            Left = 120
            Top = 376
            Width = 81
            Height = 13
            Caption = 'Linked Payroll'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object evLabel14: TevLabel
            Left = 8
            Top = 320
            Width = 41
            Height = 13
            Caption = 'Location'
          end
          object evDBEdit1: TevDBEdit
            Left = 120
            Top = 8
            Width = 241
            Height = 21
            DataField = 'DESCRIPTION'
            DataSource = wwdsDetail
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object evDBComboBox1: TevDBComboBox
            Left = 120
            Top = 32
            Width = 241
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'AUTO_RELEASE_TYPE'
            DataSource = wwdsDetail
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 370
            Top = 268
            Width = 145
            Height = 41
            Caption = '~Required Part'
            Columns = 2
            DataField = 'REQUIRED'
            DataSource = wwdsDetail
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
          object evDBEdit2: TevDBEdit
            Left = 120
            Top = 64
            Width = 241
            Height = 21
            DataField = 'NOTIFICATION_EMAIL'
            DataSource = wwdsDetail
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object edSyCoverReport: TevDBLookupCombo
            Left = 120
            Top = 88
            Width = 241
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'REPORT_DESCRIPTION'#9'F')
            DataField = 'SY_COVER_LETTER_REPORT_NBR'
            DataSource = wwdsDetail
            LookupField = 'NBR'
            Style = csDropDownList
            TabOrder = 4
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object edSbCoverReport: TevDBLookupCombo
            Left = 120
            Top = 112
            Width = 241
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'REPORT_DESCRIPTION'#9'F')
            DataField = 'SB_COVER_LETTER_REPORT_NBR'
            DataSource = wwdsDetail
            LookupField = 'NBR'
            Style = csDropDownList
            TabOrder = 5
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object EvDBMemo1: TEvDBMemo
            Left = 120
            Top = 136
            Width = 449
            Height = 89
            DataField = 'NOTE'
            DataSource = wwdsDetail
            TabOrder = 6
          end
          object edUpLinkMailBoxGroup: TevDBLookupCombo
            Left = 120
            Top = 232
            Width = 241
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
            DataField = 'UP_LEVEL_MAIL_BOX_NBR'
            DataSource = wwdsDetail
            LookupField = 'SB_MAIL_BOX_NBR'
            Style = csDropDownList
            Enabled = False
            TabOrder = 7
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object evDBDateTimePicker1: TevDBDateTimePicker
            Left = 120
            Top = 272
            Width = 241
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'CREATED_TIME'
            DataSource = wwdsDetail
            Epoch = 1950
            ReadOnly = True
            ShowButton = True
            TabOrder = 8
            DisplayFormat = 'MM/DD/YY HH:NN:SS a/p'
          end
          object evDBDateTimePicker2: TevDBDateTimePicker
            Left = 120
            Top = 296
            Width = 241
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            DataField = 'RELEASED_TIME'
            DataSource = wwdsDetail
            Epoch = 1950
            ReadOnly = True
            ShowButton = True
            TabOrder = 9
            DisplayFormat = 'MM/DD/YY HH:NN:SS a/p'
          end
          object cbVmrPrinterLocation: TevComboBox
            Left = 120
            Top = 320
            Width = 307
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 0
            TabOrder = 10
            OnChange = AdditionalOptionChange
            OnKeyDown = cbVmrPrinterLocationKeyDown
          end
          object cbIncludeDescInEmails: TevCheckBox
            Left = 367
            Top = 10
            Width = 201
            Height = 17
            Caption = 'Include Description in Emails'
            TabOrder = 11
            OnClick = AdditionalOptionChange
          end
        end
        object tsContent: TTabSheet
          Caption = 'Content'
          ImageIndex = 1
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 590
            Height = 349
            Align = alClient
            TabOrder = 0
            object evSplitter2: TevSplitter
              Left = 1
              Top = 169
              Width = 588
              Height = 7
              Cursor = crVSplit
              Align = alTop
              Beveled = True
            end
            object evDBGrid1: TevDBGrid
              Left = 1
              Top = 1
              Width = 588
              Height = 168
              DisableThemesInTitle = False
              Selected.Strings = (
                'JOB_DESCR'#9'25'#9'Job Description'#9'F'
                'REP_DESCR'#9'25'#9'Report Description'#9'F'
                'FILE_NAME'#9'45'#9'File Name'#9'F'
                'MEDIA_TYPE'#9'2'#9'Type'#9'F'
                'PAGE_COUNT'#9'5'#9'Pages'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_SB_MAIL_BOX\evDBGrid1'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alTop
              DataSource = dsContent
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = evDBGrid1DblClick
              PaintOptions.AlternatingRowColor = clCream
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
            object pPreViewPanel: TPanel
              Left = 1
              Top = 176
              Width = 588
              Height = 172
              Align = alClient
              BevelOuter = bvNone
              Caption = 'Double click on the content grid to see preview'
              TabOrder = 1
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Primary Settings'
          ImageIndex = 2
          object pcPrimarySettings: TevPageControl
            Left = 0
            Top = 57
            Width = 234
            Height = 152
            ActivePage = tsPriSbMedia
            Align = alClient
            TabOrder = 0
            object tsPriService: TTabSheet
              Caption = 'Service Settings'
            end
            object tsPriMedia: TTabSheet
              Caption = 'Media Settings'
              ImageIndex = 1
            end
            object tsPriSbType: TTabSheet
              Caption = 'SB Service Settings (Read Only)'
              ImageIndex = 2
            end
            object tsPriSbMedia: TTabSheet
              Caption = 'SB Media Settings (Read Only)'
              ImageIndex = 3
            end
          end
          object evPanel1: TevPanel
            Left = 0
            Top = 0
            Width = 234
            Height = 57
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object evLabel1: TevLabel
              Left = 8
              Top = 8
              Width = 86
              Height = 16
              Caption = '~Delivery Method'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evLabel3: TevLabel
              Left = 8
              Top = 32
              Width = 79
              Height = 16
              Caption = '~Delivery Media'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object edType: TevDBLookupCombo
              Left = 128
              Top = 8
              Width = 377
              Height = 21
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'NAME'#9'40'#9'NAME'#9'F')
              DataField = 'SB_DELIVERY_METHOD_NBR'
              DataSource = wwdsDetail
              LookupTable = cdTypes
              LookupField = 'SB_DELIVERY_METHOD_NBR'
              Style = csDropDownList
              TabOrder = 0
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = False
              OnChange = edTypeChange
            end
            object edMedia: TevDBLookupCombo
              Left = 128
              Top = 32
              Width = 377
              Height = 21
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'luMediaName'#9'40'#9'luMediaName'#9'F')
              DataField = 'SB_MEDIA_TYPE_NBR'
              DataSource = wwdsDetail
              LookupTable = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
              LookupField = 'SB_MEDIA_TYPE_NBR'
              Style = csDropDownList
              TabOrder = 1
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = False
              OnBeforeDropDown = edMediaBeforeDropDown
              OnCloseUp = edMediaCloseUp
            end
          end
        end
      end
      object evDBTreeView1: TevDBTreeView
        Left = 0
        Top = 0
        Width = 185
        Height = 237
        Align = alLeft
        AutoExpand = True
        DragMode = dmAutomatic
        HideSelection = False
        Indent = 19
        ReadOnly = True
        RowSelect = True
        TabOrder = 1
        CaptionField = 'DESCRIPTION'
        KeyField = 'SB_MAIL_BOX_NBR'
        UpLinkField = 'UP_LEVEL_MAIL_BOX_NBR'
      end
    end
    object tsDetails: TTabSheet
      Caption = 'Content'
      ImageIndex = 3
      TabVisible = False
      object Panel5: TevPanel
        Left = 0
        Top = 0
        Width = 846
        Height = 663
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Double click on the content grid to see preview'
        TabOrder = 0
        object Splitter2: TSplitter
          Left = 185
          Top = 0
          Width = 7
          Height = 663
          Beveled = True
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 185
          Height = 663
          Align = alLeft
          Caption = 'Panel2'
          TabOrder = 0
        end
      end
    end
    object tshtDetails1: TTabSheet
      Caption = 'Primary Settings'
      TabVisible = False
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = cdMaster
    OnDataChange = wwdsMasterDataChange
    Left = 213
    Top = 58
  end
  inherited wwdsDetail: TevDataSource
    DataSet = cdBrowse
    Enabled = False
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    Left = 238
    Top = 122
  end
  inherited wwdsList: TevDataSource
    Left = 354
    Top = 122
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 272
    Top = 128
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 400
    Top = 32
  end
  object cdTypes: TevClientDataSet
    Left = 328
    Top = 64
    object cdTypesSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
    end
    object cdTypesCLASS_NAME: TStringField
      FieldName = 'CLASS_NAME'
      Size = 40
    end
    object cdTypesNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
  end
  object cdBrowse: TevClientDataSet
    IndexName = 'SORT'
    IndexDefs = <
      item
        Name = 'SORT'
        Fields = 'UP_NBR;DESCRIPTION'
      end
      item
        Name = 'BOX_NBR'
        Fields = 'UP_NBR'
      end>
    MasterFields = 'SB_MAIL_BOX_NBR'
    Left = 352
    Top = 64
    object cdBrowseSB_MAIL_BOX_NBR: TIntegerField
      FieldName = 'SB_MAIL_BOX_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."SB_MAIL_BOX_NBR"'
    end
    object cdBrowseCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."CL_NBR"'
    end
    object cdBrowseCL_MAIL_BOX_GROUP_NBR: TIntegerField
      FieldName = 'CL_MAIL_BOX_GROUP_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."CL_MAIL_BOX_GROUP_NBR"'
    end
    object cdBrowseCOST: TFloatField
      FieldName = 'COST'
      Origin = '"SB_MAIL_BOX_HIST"."COST"'
      DisplayFormat = '#,##0.00'
    end
    object cdBrowseREQUIRED: TStringField
      FieldName = 'REQUIRED'
      Origin = '"SB_MAIL_BOX_HIST"."REQUIRED"'
      FixedChar = True
      Size = 1
    end
    object cdBrowseNOTIFICATION_EMAIL: TStringField
      FieldName = 'NOTIFICATION_EMAIL'
      Origin = '"SB_MAIL_BOX_HIST"."NOTIFICATION_EMAIL"'
      Size = 80
    end
    object cdBrowsePR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."PR_NBR"'
    end
    object cdBrowseRELEASED_TIME: TDateTimeField
      FieldName = 'RELEASED_TIME'
      Origin = '"SB_MAIL_BOX_HIST"."RELEASED_TIME"'
    end
    object cdBrowsePRINTED_TIME: TDateTimeField
      FieldName = 'PRINTED_TIME'
      Origin = '"SB_MAIL_BOX_HIST"."PRINTED_TIME"'
    end
    object cdBrowseSB_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SB_COVER_LETTER_REPORT_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."SB_COVER_LETTER_REPORT_NBR"'
    end
    object cdBrowseSCANNED_TIME: TDateTimeField
      FieldName = 'SCANNED_TIME'
      Origin = '"SB_MAIL_BOX_HIST"."SCANNED_TIME"'
    end
    object cdBrowseSHIPPED_TIME: TDateTimeField
      FieldName = 'SHIPPED_TIME'
      Origin = '"SB_MAIL_BOX_HIST"."SHIPPED_TIME"'
    end
    object cdBrowseSY_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SY_COVER_LETTER_REPORT_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."SY_COVER_LETTER_REPORT_NBR"'
    end
    object cdBrowseNOTE: TStringField
      FieldName = 'NOTE'
      Origin = '"SB_MAIL_BOX_HIST"."NOTE"'
      Size = 512
    end
    object cdBrowseUP_LEVEL_MAIL_BOX_NBR: TIntegerField
      FieldName = 'UP_LEVEL_MAIL_BOX_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."UP_LEVEL_MAIL_BOX_NBR"'
    end
    object cdBrowseTRACKING_INFO: TStringField
      FieldName = 'TRACKING_INFO'
      Origin = '"SB_MAIL_BOX_HIST"."TRACKING_INFO"'
      Size = 40
    end
    object cdBrowseAUTO_RELEASE_TYPE: TStringField
      FieldName = 'AUTO_RELEASE_TYPE'
      Origin = '"SB_MAIL_BOX_HIST"."AUTO_RELEASE_TYPE"'
      FixedChar = True
      Size = 1
    end
    object cdBrowseBARCODE: TStringField
      FieldName = 'BARCODE'
      Origin = '"SB_MAIL_BOX_HIST"."BARCODE"'
      Size = 40
    end
    object cdBrowseCREATED_TIME: TDateTimeField
      FieldName = 'CREATED_TIME'
      Origin = '"SB_MAIL_BOX_HIST"."CREATED_TIME"'
    end
    object cdBrowseDISPOSE_CONTENT_AFTER_SHIPPING: TStringField
      FieldName = 'DISPOSE_CONTENT_AFTER_SHIPPING'
      Origin = '"SB_MAIL_BOX_HIST"."DISPOSE_CONTENT_AFTER_SHIPPING"'
      FixedChar = True
      Size = 1
    end
    object cdBrowseADDRESSEE: TStringField
      FieldName = 'ADDRESSEE'
      Origin = '"SB_MAIL_BOX_HIST"."ADDRESSEE"'
      Size = 512
    end
    object cdBrowseDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = '"SB_MAIL_BOX_HIST"."DESCRIPTION"'
      Size = 40
    end
    object cdBrowseSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."SB_DELIVERY_METHOD_NBR"'
    end
    object cdBrowseSB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SB_MEDIA_TYPE_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."SB_MEDIA_TYPE_NBR"'
    end
    object cdBrowseluMethodName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMethodName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luMethodName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluMediaName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMediaName'
      LookupDataSet = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
      LookupKeyFields = 'SB_MEDIA_TYPE_NBR'
      LookupResultField = 'luMediaName'
      KeyFields = 'SB_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluServiceName: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luServiceName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluClNbr: TStringField
      FieldKind = fkLookup
      FieldName = 'luClNbr'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluClName: TStringField
      FieldKind = fkLookup
      FieldName = 'luClName'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluPrCheckDate: TDateField
      FieldKind = fkLookup
      FieldName = 'luPrCheckDate'
      LookupDataSet = DM_TMP_PR.TMP_PR
      LookupKeyFields = 'CL_NBR;PR_NBR'
      LookupResultField = 'CHECK_DATE'
      KeyFields = 'CL_NBR;PR_NBR'
      Lookup = True
    end
    object cdBrowseluPrRunNumber: TIntegerField
      FieldKind = fkLookup
      FieldName = 'luPrRunNumber'
      LookupDataSet = DM_TMP_PR.TMP_PR
      LookupKeyFields = 'CL_NBR;PR_NBR'
      LookupResultField = 'RUN_NUMBER'
      KeyFields = 'CL_NBR;PR_NBR'
      Lookup = True
    end
    object cdBrowseUP_NBR: TIntegerField
      FieldName = 'UP_NBR'
    end
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 312
    Top = 128
  end
  object cdMaster: TevClientDataSet
    FieldDefs = <
      item
        Name = 'SB_MAIL_BOX_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_NBR'
        DataType = ftInteger
      end
      item
        Name = 'RELEASED_TIME'
        DataType = ftDateTime
      end
      item
        Name = 'NOTE'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'AUTO_RELEASE_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CREATED_TIME'
        DataType = ftDateTime
      end
      item
        Name = 'ADDRESSEE'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'SB_DELIVERY_METHOD_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SB_MEDIA_TYPE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'calcStatus'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'PAGE_COUNT'
        DataType = ftInteger
      end
      item
        Name = 'EMPTY_REQUIRED'
        DataType = ftInteger
      end
      item
        Name = 'PRINTED_TIME'
        DataType = ftDateTime
      end
      item
        Name = 'SCANNED_TIME'
        DataType = ftDateTime
      end
      item
        Name = 'SHIPPED_TIME'
        DataType = ftDateTime
      end
      item
        Name = 'TRACKING_INFO'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'BARCODE'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'D_EMAILED'
        DataType = ftString
        Size = 20
      end>
    AfterScroll = cdMasterAfterScroll
    OnCalcFields = cdMasterCalcFields
    Left = 392
    Top = 72
    object cdMasterSB_MAIL_BOX_NBR: TIntegerField
      FieldName = 'SB_MAIL_BOX_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."SB_MAIL_BOX_NBR"'
    end
    object cdMasterCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."CL_NBR"'
    end
    object cdMasterPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."PR_NBR"'
    end
    object cdMasterRELEASED_TIME: TDateTimeField
      FieldName = 'RELEASED_TIME'
      Origin = '"SB_MAIL_BOX_HIST"."RELEASED_TIME"'
    end
    object cdMasterNOTE: TStringField
      FieldName = 'NOTE'
      Origin = '"SB_MAIL_BOX_HIST"."NOTE"'
      Size = 512
    end
    object cdMasterAUTO_RELEASE_TYPE: TStringField
      FieldName = 'AUTO_RELEASE_TYPE'
      Origin = '"SB_MAIL_BOX_HIST"."AUTO_RELEASE_TYPE"'
      FixedChar = True
      Size = 1
    end
    object cdMasterCREATED_TIME: TDateTimeField
      FieldName = 'CREATED_TIME'
      Origin = '"SB_MAIL_BOX_HIST"."CREATED_TIME"'
    end
    object cdMasterADDRESSEE: TStringField
      FieldName = 'ADDRESSEE'
      Origin = '"SB_MAIL_BOX_HIST"."ADDRESSEE"'
      Size = 512
    end
    object cdMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = '"SB_MAIL_BOX_HIST"."DESCRIPTION"'
      Size = 40
    end
    object cdMasterSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."SB_DELIVERY_METHOD_NBR"'
    end
    object cdMasterSB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SB_MEDIA_TYPE_NBR'
      Origin = '"SB_MAIL_BOX_HIST"."SB_MEDIA_TYPE_NBR"'
    end
    object cdMasterluMethodName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMethodName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luMethodName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdMasterluMediaName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMediaName'
      LookupDataSet = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
      LookupKeyFields = 'SB_MEDIA_TYPE_NBR'
      LookupResultField = 'luMediaName'
      KeyFields = 'SB_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
    object cdMasterluServiceName: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luServiceName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdMasterluClNbr: TStringField
      FieldKind = fkLookup
      FieldName = 'luClNbr'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdMasterluClName: TStringField
      FieldKind = fkLookup
      FieldName = 'luClName'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdMasterluPrCheckDate: TDateField
      FieldKind = fkLookup
      FieldName = 'luPrCheckDate'
      LookupDataSet = DM_TMP_PR.TMP_PR
      LookupKeyFields = 'CL_NBR;PR_NBR'
      LookupResultField = 'CHECK_DATE'
      KeyFields = 'CL_NBR;PR_NBR'
      Lookup = True
    end
    object cdMasterluPrRunNumber: TIntegerField
      FieldKind = fkLookup
      FieldName = 'luPrRunNumber'
      LookupDataSet = DM_TMP_PR.TMP_PR
      LookupKeyFields = 'CL_NBR;PR_NBR'
      LookupResultField = 'RUN_NUMBER'
      KeyFields = 'CL_NBR;PR_NBR'
      Lookup = True
    end
    object cdMastercalcStatus: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'calcStatus'
      Size = 80
    end
    object cdMastercalcColor: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'calcColor'
      Calculated = True
    end
    object cdMasterPAGE_COUNT: TIntegerField
      FieldName = 'PAGE_COUNT'
    end
    object cdMasterEMPTY_REQUIRED: TIntegerField
      FieldName = 'EMPTY_REQUIRED'
    end
    object cdMasterPRINTED_TIME: TDateTimeField
      FieldName = 'PRINTED_TIME'
    end
    object cdMasterSCANNED_TIME: TDateTimeField
      FieldName = 'SCANNED_TIME'
    end
    object cdMasterSHIPPED_TIME: TDateTimeField
      FieldName = 'SHIPPED_TIME'
    end
    object cdMasterTRACKING_INFO: TStringField
      FieldName = 'TRACKING_INFO'
      Size = 40
    end
    object cdMasterBARCODE: TStringField
      FieldName = 'BARCODE'
      Size = 40
    end
    object cdMastercalcCleanUp: TStringField
      FieldKind = fkCalculated
      FieldName = 'calcCleanUp'
      Size = 2
      Calculated = True
    end
    object cdMasterD_EMAILED: TStringField
      FieldName = 'D_EMAILED'
    end
    object cdMasterBlockVouchers: TStringField
      FieldName = 'BlockVouchers'
    end
  end
  object dsContent: TevDataSource
    DataSet = cdContent
    OnDataChange = dsContentDataChange
    Left = 232
    Top = 176
  end
  object cdContent: TevClientDataSet
    Left = 424
    Top = 72
  end
end
