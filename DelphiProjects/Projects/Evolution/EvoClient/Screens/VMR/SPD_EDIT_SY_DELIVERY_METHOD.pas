// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_DELIVERY_METHOD;

interface

uses
  SDataStructure,  wwdbedit, Wwdotdot,
  Wwdbcomb, wwdbdatetimepicker, Mask, DBCtrls, StdCtrls, wwdblook,
  ExtCtrls, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls,
  Classes, SFrameEntry, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SY_DELIVERY_METHOD = class(TFrameEntry)
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evDBGrid2: TevDBGrid;
    evPanel2: TevPanel;
    evDBGrid1: TevDBGrid;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBComboBox1: TevDBComboBox;
    evDBEdit1: TevDBEdit;
    evSplitter1: TevSplitter;
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

uses SVmrClasses, Variants;

{$R *.DFM}

procedure TEDIT_SY_DELIVERY_METHOD.Activate;
begin
  inherited;
end;

function TEDIT_SY_DELIVERY_METHOD.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_DELIVERY_METHOD;
end;

function TEDIT_SY_DELIVERY_METHOD.GetInsertControl: TWinControl;
begin
  Result := evDBComboBox1;
end;

procedure TEDIT_SY_DELIVERY_METHOD.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SYSTEM_MISC.SY_DELIVERY_SERVICE, SupportDataSets);
end;

procedure TEDIT_SY_DELIVERY_METHOD.wwdsMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if not Assigned(Field) and wwdsMaster.DataSet.Active then
  begin
    wwdsDetail.AutoEdit := not VarIsNull(wwdsMaster.DataSet['CLASS_NAME']);
    if wwdsDetail.AutoEdit then
      evDBComboBox1.Items.CommaText := ListVmrClasses(TClassArray(TVmrDeliveryServiceClass(FindVmrClass(wwdsMaster.DataSet['CLASS_NAME'])).GetMethodClasses));
  end;
end;

initialization
  RegisterClass(TEDIT_SY_DELIVERY_METHOD);

end.
