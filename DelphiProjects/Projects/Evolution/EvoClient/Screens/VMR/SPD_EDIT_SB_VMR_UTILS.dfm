inherited EDIT_SB_VMR_UTILS: TEDIT_SB_VMR_UTILS
  Width = 739
  Height = 400
  object evplClientList: TevPanel [0]
    Left = 0
    Top = 0
    Width = 329
    Height = 400
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object wwdbgridSelectClient: TevDBGrid
      Left = 0
      Top = 0
      Width = 320
      Height = 400
      DisableThemesInTitle = False
      Selected.Strings = (
        'CUSTOM_CLIENT_NUMBER'#9'20'#9'Client Number'
        'NAME'#9'40'#9'Name'
        'CL_NBR'#9'10'#9'Internal CL#')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TEDIT_SB_VMR_UTILS\wwdbgridSelectClient'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alLeft
      DataSource = wwdsList
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
      PaintOptions.ActiveRecordColor = clBlack
      NoFire = False
    end
  end
  object evbtClean: TevBitBtn [1]
    Left = 336
    Top = 16
    Width = 161
    Height = 25
    Caption = 'Clean Up Mailboxes'
    TabOrder = 1
    OnClick = evbtCleanClick
    Color = clBlack
    Margin = 0
  end
  object evbtPrintedToScaned: TevBitBtn [2]
    Left = 336
    Top = 83
    Width = 161
    Height = 25
    Caption = 'Clean Up Printer Pickup Queue'
    TabOrder = 2
    OnClick = evbtPrintedToScanedClick
    Color = clBlack
    Margin = 0
  end
  object evDeleteMailBox: TevBitBtn [3]
    Left = 336
    Top = 49
    Width = 161
    Height = 25
    Caption = 'Delete Mailboxes'
    TabOrder = 3
    OnClick = evDeleteMailBoxClick
    Color = clBlack
    Margin = 0
  end
  object btnUpdatePassword: TevBitBtn [4]
    Left = 336
    Top = 148
    Width = 161
    Height = 25
    Caption = 'Update Password'
    TabOrder = 5
    OnClick = btnUpdatePasswordClick
    Color = clBlack
    Margin = 0
  end
  object btnRefreshPrinters: TevBitBtn [5]
    Left = 336
    Top = 116
    Width = 161
    Height = 25
    Caption = 'Refresh Printer List'
    TabOrder = 4
    OnClick = btnRefreshPrintersClick
    Color = clBlack
    Margin = 0
  end
  inherited wwdsMaster: TevDataSource
    Left = 261
    Top = 90
  end
  inherited wwdsDetail: TevDataSource
    Left = 222
    Top = 130
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_TMP_CL.TMP_CL
    Left = 210
    Top = 90
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 288
    Top = 56
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 296
    Top = 88
  end
end
