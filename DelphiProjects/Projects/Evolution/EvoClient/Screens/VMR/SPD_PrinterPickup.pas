// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PrinterPickup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SDataStructure, DB, Wwdatsrc, EvTypes,
   StdCtrls, wwdblook, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, SFrameEntry, SVmrClasses, ISBasicClasses, Buttons,Registry,
  RegistryFunctions,sPickupSheetsXMLData,XMLDoc,EvContext,XMLIntf, EvExceptions,
  EvDataAccessComponents, EvUIComponents, EvClientDataSet, isUIEdit,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TPrinterPickup = class(TFrameEntry)
    Panel1: TPanel;
    bCheck: TevBitBtn;
    edBarcode: TevEdit;
    evLabel1: TevLabel;
    mMessages: TevMemo;
    btnFillGrid: TButton;
    DataSource1: TDataSource;
    PP: TStringGrid;
    Splitter1: TSplitter;
    procedure edBarcodeKeyPress(Sender: TObject; var Key: Char);
    procedure bCheckClick(Sender: TObject);
    procedure btnFillGridClick(Sender: TObject);
    procedure PPDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure PPDblClick(Sender: TObject);
  private
    { Private declarations }
    XMLPickupData:IXMLPickupDataType;
    PickupDoc:IXMLDocument;
  public
    procedure Activate; override;
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses EvUtils, EvBasicUtils;

procedure TPrinterPickup.edBarcodeKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = #13 then
    bCheck.Click;
end;

procedure TPrinterPickup.bCheckClick(Sender: TObject);
var
  cdBoxes, cdContents: TevClientDataSet;
  sExpectedCode, sLastScannedCode: string;
  sBox: string;
  s, s2,s3,BC,sp: string;
  i, j, k, iSbNbr, iPartNumber, iPartCount: Integer;
  dm: TVmrRealDelivery;
  mt: TVmrMediaType;
  bPacked: Boolean;


  function BoxIsWaiting(const SbNbr: Integer): Boolean;
  var
    cdClonedBoxes: TevClientDataSet;
  begin
    Result := False;
    //cdClonedBoxes := TevClientDataSet.Create(nil);
    cdClonedBoxes := CreateLookupDataset(cdBoxes, nil);
    try
      //cdClonedBoxes.CloneCursor(cdBoxes, True);
      cdClonedBoxes.IndexFieldNames := 'UP_LEVEL_MAIL_BOX_NBR';
      cdClonedBoxes.SetRange([SbNbr], [SbNbr]);
      cdClonedBoxes.First;
      while not cdClonedBoxes.Eof do
      begin
        Result := (VarIsNull(cdClonedBoxes['SCANNED_TIME'])
          and cdContents.FindKey([cdClonedBoxes['SB_MAIL_BOX_NBR']])
          and (ConvertNull(cdContents['content_count'], 0) > 0)
        )
          or BoxIsWaiting(cdClonedBoxes['SB_MAIL_BOX_NBR']);
        if Result then
          Break;
        cdClonedBoxes.Next;
      end;
    finally
      cdClonedBoxes.Free;
    end;
  end;
  procedure GetBoxDataSets;
  begin
     with TExecDSWrapper.Create('GenericSelectWithCondition') do
     begin
         SetMacro('COLUMNS', 'UP_NBR');
         SetMacro('TABLENAME', 'LIST_TOP_MAIL_BOXES2');
         SetMacro('CONDITION', 'NBR=:SbBoxNbr');
         SetParam('SbBoxNbr', iSbNbr);
         cdBoxes.Close;
         cdBoxes.DataRequest(AsVariant);
     end;
     cdBoxes.Open;
     Assert(not VarIsNull(cdBoxes['UP_NBR']));
     with TExecDSWrapper.Create('GenericSelectWithCondition') do
     begin
        SetMacro('COLUMNS', 'NBR');
        SetMacro('TABLENAME', 'LIST_TOP_MAIL_BOXES2');
        SetMacro('CONDITION', 'UP_NBR=:SbBoxNbr');
        SetParam('SbBoxNbr', cdBoxes['UP_NBR']);
        cdBoxes.Close;
        cdBoxes.DataRequest(AsVariant);
     end;
     cdBoxes.Open;
     with cdBoxes.GetFieldValueList('NBR', True) do
     try
          with TExecDSWrapper.Create('GenericSelectCurrentWithConditionGrouped') do
          begin
            SetMacro('COLUMNS', 'SB_MAIL_BOX_NBR, count(SB_MAIL_BOX_CONTENT_NBR) content_count');
            SetMacro('TABLENAME', 'SB_MAIL_BOX_CONTENT');
            SetMacro('CONDITION', 'SB_MAIL_BOX_NBR in ('+ CommaText+ ')');
            SetMacro('Group', 'SB_MAIL_BOX_NBR');
            cdContents.Close;
            cdContents.DataRequest(AsVariant);
          end;
          with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
          begin
            SetMacro('COLUMNS', '*');
            SetMacro('TABLENAME', 'SB_MAIL_BOX');
            SetMacro('CONDITION', 'SB_MAIL_BOX_NBR in ('+ CommaText+ ')');
            cdBoxes.Close;
            cdBoxes.DataRequest(AsVariant);
          end;
     finally
          Free;
     end;
     ctx_DataAccess.OpenDataSets([cdBoxes, cdContents]);
     Assert(cdBoxes.Recordcount > 0);
  end;
  function AllBoxesScanned:boolean;
  begin
    Result := true;
    cdBoxes.First;
    while not cdBoxes.eof do
    begin
      BC := 'P'+cdBoxes['BARCODE'];
      if ctx_VMRRemote.FindInBoxes(BC) = '' then
      begin
        Result := false;
        break;
      end;
      cdBoxes.Next;
    end
  end;

begin
  inherited;
  Beep;
  try
    iSbNbr := StrToInt(Copy(edBarcode.Text, 1, Length(edBarcode.Text)-4));
    iPartNumber := StrToInt(Copy(edBarcode.Text, Length(edBarcode.Text)-3, 2));
    iPartCount := StrToInt(Copy(edBarcode.Text, Length(edBarcode.Text)-1, 2));
  except
    raise EInvalidParameters.CreateHelp('Code is invalid', IDH_InvalidParameters);
  end;
  cdBoxes := TevClientDataSet.Create(nil);
  cdContents := TevClientDataSet.Create(nil);
  try
    cdBoxes.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
    cdContents.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
    DM_SERVICE_BUREAU.SB_MAIL_BOX.Close;
    DM_SERVICE_BUREAU.SB_MAIL_BOX.DataRequired('SB_MAIL_BOX_NBR='+ IntToStr(iSbNbr));
    {if DM_SERVICE_BUREAU.SB_MAIL_BOX.RecordCount <> 1 then
      raise EInvalidParameters.CreateHelp('Code is unknown', IDH_InvalidParameters);
    if VarIsNull(DM_SERVICE_BUREAU.SB_MAIL_BOX['PRINTED_TIME']) then
      raise EInvalidParameters.CreateHelp('Code is not marked as printed', IDH_InvalidParameters);
    if not VarIsNull(DM_SERVICE_BUREAU.SB_MAIL_BOX['SCANNED_TIME']) then
      raise EInvalidParameters.CreateHelp('Code is already marked as scanned', IDH_InvalidParameters);
    }
    mMessages.Lines.Clear;
    mMessages.Update;

    if not ctx_VMRRemote.CheckScannedBarcode(edBarcode.Text, sExpectedCode, sLastScannedCode) then
    begin
      mMessages.Lines.Append('System detects that one of pickup sheets is missing');
      mMessages.Lines.Append('Scanned Sheet Code is '+ edBarcode.Text);
      if sExpectedCode <> '' then
        mMessages.Lines.Append('Expected Sheet Code is '+ sExpectedCode);
      if sLastScannedCode <> '' then
      begin
        mMessages.Lines.Append('Last scanned sheet code is '+ sLastScannedCode);
        sBox := ctx_VMRRemote.FindInBoxes(sLastScannedCode);
        if sBox <> '' then
          mMessages.Lines.Append('It is sitting in '+ sBox)
        else
          mMessages.Lines.Append('It was packaged. Check out outgoing bin');
      end;
      mMessages.Lines.Append('');
      mMessages.Lines.Append('You need to find the missing sheet and scan it before you can proceed further');
      mMessages.Font.Color := clRed;
    end
    else
    begin
      j := 1;
      k := 0;
      for i := 1 to iPartCount do
        if i <> iPartNumber then
        begin
          sBox := ctx_VMRRemote.FindInBoxes(FormatBarCode(iSbNbr, i, iPartCount));
          if sBox <> '' then
          begin
            Inc(j);
            k := i;
          end;
        end;
      if j = iPartCount then // all parts are scanned
      begin
        bPacked := False;
        dm := InstanceSbDeliveryMethod(DM_SERVICE_BUREAU.SB_MAIL_BOX) as TVmrRealDelivery;
        try
          mt := InstanceSbMedia(DM_SERVICE_BUREAU.SB_MAIL_BOX);
          try
            mMessages.Font.Color := clGreen;
            sp := mt.OnPickupSheetScan(DM_SERVICE_BUREAU.SB_MAIL_BOX) +#13;
            GetBoxDataSets;
            BC := 'P'+ DM_SERVICE_BUREAU.SB_MAIL_BOX['BARCODE'];
            sBox := ctx_VMRRemote.FindInBoxes(BC);
            if sBox <> '' then
            begin
                mMessages.Font.Color := clBlack;
                s := ('It is sitting in '+ sBox) +#13;
                s := s + ('It was packaged. Check out outgoing bin');
            end
            else
            if k<>0  then
            begin
              s2 := ctx_VMRRemote.PutInBoxes(BC,FormatBarCode(iSbNbr, k, iPartCount));
              s := s+ 'Combine all of that with '+ s2+ ', affix label from the pickup sheet'#13#13
            end
            else
            begin
              s := 'Put all of that in a generic envelope, affix label from the pickup sheet'#13;
              s := s +'Please put it in '+ ctx_VMRRemote.PutInBoxes(BC,'')+#13#13;
            end;
            s2 :='';
            if (AllBoxesScanned) then
            begin
              if cdBoxes.RecordCount > 1 then
                s2 := 'Combine next boxes:'#13;
              cdBoxes.First;
              while not cdBoxes.eof do
              begin
                  if cdBoxes.RecordCount > 1 then
                     s3 := s3 + ctx_VMRRemote.FindInBoxes('P'+ cdBoxes['BARCODE'])+ #13;
                  if VarIsNull(cdBoxes['UP_LEVEL_MAIL_BOX_NBR']) then
                     sp := sp + dm.OnPickupSheetScan(DM_SERVICE_BUREAU.SB_MAIL_BOX)  +#13;
                  ctx_VMRRemote.RemoveScannedBarcodes(cdBoxes['BARCODE']);
                  ctx_VMRRemote.RemoveFromBoxes('P'+ cdBoxes['BARCODE']);
                  ctx_VMRRemote.RemoveFromBoxes(cdBoxes['BARCODE']+'*');
                  cdBoxes.Next;
              end;
            end else
            begin

            end;
            s := s + s2 + s3+ sp;
            mMessages.Lines.Text := s;
            Update;
            if bPacked then
              {dm.OnShipmentPacked(DM_SERVICE_BUREAU.SB_MAIL_BOX)};
          finally
            mt.Free;
          end;
        finally
          dm.Free;
        end;
        DM_SERVICE_BUREAU.SB_MAIL_BOX.Edit;
        DM_SERVICE_BUREAU.SB_MAIL_BOX['SCANNED_TIME'] := SysTime;
        DM_SERVICE_BUREAU.SB_MAIL_BOX.Post;
        ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX]);
      end
      else
      begin
        mMessages.Font.Color := clBlack;
        if k = 0 then
        begin
          sBox := ctx_VMRRemote.FindInBoxes(FormatBarCode(iSbNbr, iPartNumber, iPartCount));
          if sBox <> '' then
          begin
             mMessages.Lines.Text := 'This is a part of bigger package.'#13''+
                                     'It is sitting in '+ sBox;
          end else
          begin
             mMessages.Lines.Text := 'This is a part of bigger package.'#13'Please put it in '+
                ctx_VMRRemote.PutInBoxes(edBarcode.Text, '')+ ' to wait for other parts';
          end;
        end
        else
        begin
          mMessages.Lines.Text := 'This is a part of bigger package.'#13'Please put it in '+
            ctx_VMRRemote.PutInBoxes(edBarcode.Text, FormatBarCode(iSbNbr, k, iPartCount))+ ' with other parts to wait';
        end;
      end;
      //RegDaemon.RemoveScannedBarcode(edBarcode.Text);
    end;
    edBarcode.Clear;
    edBarcode.SetFocus;
  finally
    cdContents.Free;
    cdBoxes.Free;
  end;
  edBarcode.SetFocus;
  btnFillGridClick(Sender);
end;


{
            try
              cdBoxes.CancelRange;
              cdDownLinks.Tag := 0;
              cdDownLinks.First;
              while not cdDownLinks.Eof do
              begin
                Assert(not VarIsNull(cdDownLinks['PRINTED_TIME']));
                if not BoxIsWaiting(cdDownLinks['SB_MAIL_BOX_NBR']) then
                begin
                  s := s+ 'Add envelope from '+ RegDaemon.FindInBoxes('P'+ cdDownLinks['BARCODE'])+ #13#13;
                  RegDaemon.RemoveFromBoxes('P'+ cdDownLinks['BARCODE']);
                end
                else
                  cdDownLinks.Tag := 1;
                cdDownLinks.Next;
              end;
              cdBoxes.IndexFieldNames := 'SB_MAIL_BOX_NBR';
              Assert(cdBoxes.FindKey([DM_SERVICE_BUREAU.SB_MAIL_BOX['SB_MAIL_BOX_NBR']]));
              repeat
                s2 := RegDaemon.FindInBoxes('P'+ cdBoxes['BARCODE']);
                if BoxIsWaiting(cdBoxes['SB_MAIL_BOX_NBR']) then // waiting for more subboxes
                begin
                  if s2 <> '' then
                    s := s+ 'Add all of that to '+ s2
                  else
                    s := s+ 'Put all of that in '+ RegDaemon.PutInBoxes('P'+ cdBoxes['BARCODE'], '');
                  Break;
                end
                else
                if VarIsNull(cdBoxes['UP_LEVEL_MAIL_BOX_NBR']) then
                begin
                  if s2 <> '' then
                  begin
                    s := s+ 'Combine all of that with '+ s2+ #13#13;
                    RegDaemon.RemoveFromBoxes('P'+ cdBoxes['BARCODE']);
                  end;
                  s := s+ dm.OnPickupSheetScan(DM_SERVICE_BUREAU.SB_MAIL_BOX);
                  bPacked := True;
                  Break;
                end
                else
                begin
                  if s2 <> '' then
                  begin
                    s := s+ 'Combine all of that with '+ s2+ #13#13;
                    RegDaemon.RemoveFromBoxes('P'+ cdBoxes['BARCODE']);
                  end;
                  Assert(cdBoxes.FindKey([cdBoxes['UP_LEVEL_MAIL_BOX_NBR']]));
                  s := s+ 'Put all of that in a generic envelope, affix label from the pickup sheet'#13#13;
                end;
              until False;
            finally
              cdDownLinks.Free;
            end;
            }

procedure TPrinterPickup.btnFillGridClick(Sender: TObject);
var
  b: TStringList;
  i,c,j:integer;
  P,SbNbr,s :string;
  pq:IXMLPrinterQueueType;
begin
  inherited;
  b := TStringList.Create;
  b.Sorted := true;
  b.Duplicates := dupIgnore;
//  XMLPickupData := NewPickupData;
//  XMLPickupData.OwnerDocument.LoadFromStream(RegDaemon.RetrievePickupSheetsXMLData.RealStream);

  PickupDoc.LoadFromStream(ctx_VMRRemote.RetrievePickupSheetsXMLData.RealStream);
  XMLPickupData := GetPickupData(PickupDoc);

  pq := XMLPickupData.PrinterQueue;


  PP.ColCount := pq.Count + 1;
  PP.RowCount := 1;
  PP.ColWidths[0] := 50;
  try
      for i:=1 to PP.ColCount - 1 do
        PP.ColWidths[i] := 150;
      for i := 0 to pq.Count-1 do
      begin
        s := pq.Printer[i].Data;
        while s<>'' do
        begin
           P := Fetch(s,';');
           b.Add(Copy(P, 1, Length(P)-4));
        end;
      end;
      PP.RowCount := b.Count +1;
      for i :=1 to b.Count  do
      begin
         PP.Cells[0,i] := b.Strings[i-1];
         for j := 1 to PP.ColCount do
           PP.Cells[j,i] := '';
      end;
      for i := 0 to pq.Count-1 do
      begin
        PP.Cells[i+1,0] := pq.Printer[i].Name;
        s := pq.Printer[i].Data;
        while s<>'' do
        begin
           P := Fetch(s,';');
           SbNbr := Copy(P, 1, Length(P)-4);
           c := b.IndexOf(SbNbr);
           PP.Cells[i+1,c+1] := P;
        end;
      end;
    finally

    end;
end;

procedure TPrinterPickup.PPDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
 vbs :IXMLVMRBoxesType;
begin
  inherited;

  if State = [gdSelected]	then
     PP.Canvas.DrawFocusRect(Rect);
  if (ACol > 0) and (ARow >0) then
  begin
    PP.Canvas.Brush.Color := clWhite;
    if Trim(PP.Cells[ACol, ARow])<>'' then
    begin
       vbs :=  XMLPickupData.VMRBoxes;
       if vbs.FindInBoxes(PP.Cells[ACol, ARow]) then
         PP.Canvas.Brush.Color := clGray;
       if vbs.FindInBoxes('P'+PP.Cells[0,ARow]) then
         PP.Canvas.Brush.Color := clGreen;
    end;
  end
  else
  begin
      PP.Canvas.Brush.Color := clBtnFace;
  end;
  PP.Canvas.TextRect(Rect,Rect.Left+5,Rect.Top + 5,PP.Cells[ACol,ARow]);
end;

procedure TPrinterPickup.Activate;
begin
  inherited;
  PickupDoc := NewXMLDocument;
  btnFillGridClick(nil);
end;

procedure TPrinterPickup.PPDblClick(Sender: TObject);
begin
  inherited;
  edBarcode.Text := PP.Cells[pp.selection.Left,pp.selection.Top];
end;

initialization
  RegisterClass(TPrinterPickup);

end.
