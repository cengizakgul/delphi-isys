// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_DELIVERY_SERVICE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_OPTION_BASE, SDataStructure, DB, Wwdatsrc,
   StdCtrls, wwdblook, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, SVmrClasses, SVmrOptionEditFrame, SDDClasses,
  ISBasicClasses, SDataDictsystem, SDataDictbureau, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SB_DELIVERY_SERVICE = class(TEDIT_OPTION_BASE)
  private
    { Private declarations }
  protected                 
    function CreateOptionedObject(const c: TVmrOptionedTypeClass): TVmrOptionedType; override;
    function CreateOptionEditFrame(const o: TVmrOptionedType): TVmrOptionEditFrame; override;
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.dfm}

{ TEDIT_SB_DELIVERY_SERVICE }

function TEDIT_SB_DELIVERY_SERVICE.CreateOptionEditFrame(
  const o: TVmrOptionedType): TVmrOptionEditFrame;
begin
  if o.GetOptionEditFrameClass <> nil then
    Result := o.GetOptionEditFrameClass.CreateBound(Self, o.Options, o, wwdsDetail)
  else
    Result := nil;
end;

function TEDIT_SB_DELIVERY_SERVICE.CreateOptionedObject(
  const c: TVmrOptionedTypeClass): TVmrOptionedType;
begin
  Result := c.CreateSbSetup(wwdsDetail.DataSet['SB_DELIVERY_SERVICE_NBR']);
end;

function TEDIT_SB_DELIVERY_SERVICE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE;
end;

procedure TEDIT_SB_DELIVERY_SERVICE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SYSTEM_MISC.SY_DELIVERY_SERVICE, SupportDataSets);
end;

initialization
  RegisterClass(TEDIT_SB_DELIVERY_SERVICE);

end.
