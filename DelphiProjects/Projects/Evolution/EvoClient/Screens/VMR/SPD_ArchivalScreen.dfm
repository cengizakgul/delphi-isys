inherited ArchivalScreen: TArchivalScreen
  object evPanel1: TevPanel [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 97
    Align = alTop
    TabOrder = 0
    object evLabel2: TevLabel
      Left = 8
      Top = 8
      Width = 67
      Height = 13
      Caption = 'Search Period'
    end
    object evLabel3: TevLabel
      Left = 208
      Top = 8
      Width = 3
      Height = 13
      Caption = '-'
    end
    object edPeriodEnd: TevDateTimePicker
      Left = 224
      Top = 8
      Width = 97
      Height = 21
      Date = 37800.000000000000000000
      Time = 37800.000000000000000000
      TabOrder = 0
      OnChange = edPeriodBeginChange
    end
    object edPeriodBegin: TevDateTimePicker
      Left = 96
      Top = 8
      Width = 97
      Height = 21
      Date = 37800.000000000000000000
      Time = 37800.000000000000000000
      TabOrder = 1
      OnChange = edPeriodBeginChange
    end
    object bLoad: TevBitBtn
      Left = 8
      Top = 32
      Width = 79
      Height = 25
      Caption = 'Load'
      TabOrder = 2
      OnClick = bLoadClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
        DDDDDFFFFFFFFFFDDDDD000000000008DDDD88888888888FDDDD00BBBBBBBBB0
        8DDD8D8888888888FDDD030BBBBBBBBB08DD8DD8888888888FDD0330BBBBBBBB
        B08D8DDD8888888888FD03330BBBBBBBBB088DDDD8888888888F0330C0000000
        00008DD88D8888888888030BB7777777770D8D8DDDDDDDDDDDDD030B1BF787FF
        FF0D8D8D8DDD8DDDDD8DD00BBBF8D8FFFF0DD88DDDD8D8DDDD8DDD0CBCF787FF
        FF0DDD88D8DD8DDDDD8DDD0BBB1FFFFFFF0DDD8DDD8DDDDDDD8DDD80B11FFFFF
        F0DDDDD8D88DDDDDD8DDDDD8011FFFFF0DDDDDDD888DDDDD8DDDDDDD80FFFFF0
        DDDDDDDDD8DDDDD8DDDDDDDDD800000DDDDDDDDDDD88888DDDDD}
      NumGlyphs = 2
    end
    object bBurnCD: TevBitBtn
      Left = 104
      Top = 32
      Width = 97
      Height = 25
      Caption = 'Create CDs'
      TabOrder = 3
      OnClick = bBurnCDClick
      Glyph.Data = {
        86010000424D8601000000000000760000002800000020000000110000000100
        0400000000001001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDFFFFFFDDDDDDDDDD888888DDDDDDDDFF888888FFDDDDDD88CC7FFF8
        8DDDDDF8888DDDD88FDDDD8CCC7FFFFFF8DDDF8888DDDDDDD8FDD8FCC7FFFFFF
        FF8DD8888DDDDDDDDD8DD8B77FFFFFFFFF8DF8D8DDDDDDDDDD8F8BBB7FFFFFFF
        FFF88DDD8DDFFDDDDDD88BB1B7F88FFFFFF88DD8DDF88FDDDDD88BBBB78DD8FF
        FFF88DDDDD8DD8DDDDD88BBBB78DD8FFFFF88DDDDD8FF8DDDDD88BCBC7F88FFF
        FFF88D8D8DD88DDDDDD88BBBB17FFFFFFFF88DDDD8DDDDDDDDF8D8BB117FFFFF
        FF8DD8DD88DDDDDDDD8DD811137FFFFFFF8DD88883DDDDDDDF8DDD81337FFFFF
        F8DDDD8833DDDDDFF8DDDDD887FFFFF88DDDDDD88FFFFFF88DDDDDDDD888888D
        DDDDDDDDD888888DDDDD}
      NumGlyphs = 2
    end
    object bCreateFileTree: TevBitBtn
      Left = 208
      Top = 32
      Width = 97
      Height = 25
      Caption = 'Create file tree'
      TabOrder = 4
      OnClick = bCreateFileTreeClick
      NumGlyphs = 2
    end
    object cbAddRwPreview: TevCheckBox
      Left = 8
      Top = 61
      Width = 177
      Height = 14
      Caption = 'Add RwPreview.exe to results'
      TabOrder = 5
    end
    object cbConvertToPdf: TevCheckBox
      Left = 8
      Top = 77
      Width = 177
      Height = 14
      Caption = 'Convert to PDF format'
      TabOrder = 6
    end
  end
  object gClients: TevDBCheckGrid [1]
    Left = 0
    Top = 97
    Width = 443
    Height = 173
    DisableThemesInTitle = False
    Selected.Strings = (
      'luClNbr'#9'8'#9'Cl#'#9'F'
      'luClName'#9'20'#9'Client'#9'F'
      'PAGE_COUNT'#9'5'#9'Pages'#9'F')
    IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
    IniAttributes.SectionName = 'TArchivalScreen\gClients'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = evDataSource1
    MultiSelectOptions = [msoShiftSelect]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    ReadOnly = True
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    Visible = False
    PaintOptions.AlternatingRowColor = clCream
  end
  object pnCDBurn: TPanel [2]
    Left = 320
    Top = 1
    Width = 401
    Height = 96
    Color = clInactiveCaptionText
    TabOrder = 2
    Visible = False
    object evLabel1: TevLabel
      Left = 8
      Top = 8
      Width = 76
      Height = 13
      Caption = 'Choose Device:'
    end
    object lMessage: TevLabel
      Left = 9
      Top = 24
      Width = 148
      Height = 13
      Caption = 'Load blank CD media and click'
    end
    object cbChooseDrive: TevComboBox
      Left = 88
      Top = 3
      Width = 193
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
    object bGo: TevButton
      Left = 320
      Top = 4
      Width = 75
      Height = 21
      Caption = 'Write'
      Default = True
      TabOrder = 1
      OnClick = bGoClick
    end
    object bCancel: TevButton
      Left = 320
      Top = 30
      Width = 75
      Height = 19
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
      OnClick = bCancelClick
    end
    object cbAutoBurn: TevCheckBox
      Left = 8
      Top = 38
      Width = 129
      Height = 17
      Caption = 'Start automatically'
      TabOrder = 3
    end
    object PrBar: TProgressBar
      Left = 1
      Top = 78
      Width = 399
      Height = 17
      Align = alBottom
      TabOrder = 4
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 333
    Top = 178
  end
  inherited wwdsDetail: TevDataSource
    Left = 390
    Top = 178
  end
  inherited wwdsList: TevDataSource
    Left = 282
    Top = 178
  end
  object cdClients: TevClientDataSet
    ClientID = -1
    Left = 272
    Top = 16
    object cdClientsCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object cdClientsPAGE_COUNT: TIntegerField
      DisplayWidth = 10
      FieldName = 'PAGE_COUNT'
    end
    object cdClientsluClNbr: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'luClNbr'
      LookupDataSet = cdTmpCl
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdClientsluClName: TStringField
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'luClName'
      LookupDataSet = cdTmpCl
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
  end
  object evDataSource1: TevDataSource
    DataSet = cdClients
    Left = 120
    Top = 64
  end
  object cdTmpCl: TevClientDataSet
    ProviderName = 'TMP_CL_PROV'
    ClientID = -1
    Left = 304
    Top = 16
    object cdTmpClCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdTmpClCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdTmpClNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
  end
  object cdBoxes: TevClientDataSet
    Left = 272
    Top = 48
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = Timer1Timer
    Left = 640
    Top = 88
  end
  object MCDB: TMCDBurner
    DeviceBufferSize = 0
    DeviceFreeBufferSize = 0
    UnderrunProtection = True
    ReadSpeed = 0
    WriteSpeed = 0
    FinalizeDisc = False
    TestWrite = False
    ReplaceFile = False
    PerformOPC = False
    CacheSize = 4194304
    OnWriteDone = MCDBWriteDone
    Version = '1.1.7'
    WritePostGap = True
    Left = 608
    Top = 88
  end
end
