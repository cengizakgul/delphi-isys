// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ArchivalScreen;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FileCtrl, SPD_CourierPickup, DB,  evConsts, EvContext,
  SDataStructure, Wwdatsrc, StdCtrls, DBCtrls, ExtCtrls, Grids, Wwdbigrd, isBaseClasses,
  Wwdbgrid, SFrameEntry, ComCtrls, wwdblook, kbmMemTable, ISKbmMemDataSet, EvLegacy, isVCLBugFix,
  ISDataAccessComponents, EvDataAccessComponents, ISBasicClasses, Buttons, isBasicUtils,
  EvExceptions, mbDrvLib, mbCDBC,evMainboard,EvStreamUtils, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;
const
  WM_WRITE_DONE = WM_USER+ $55;
type
  TArchivalScreen = class(TFrameEntry)
    cdClients: TevClientDataSet;
    evPanel1: TevPanel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    edPeriodEnd: TevDateTimePicker;
    edPeriodBegin: TevDateTimePicker;
    gClients: TevDBCheckGrid;
    evDataSource1: TevDataSource;
    cdClientsCL_NBR: TIntegerField;
    cdClientsPAGE_COUNT: TIntegerField;
    cdTmpCl: TevClientDataSet;
    cdTmpClCL_NBR: TIntegerField;
    cdTmpClCUSTOM_CLIENT_NUMBER: TStringField;
    cdTmpClNAME: TStringField;
    cdClientsluClNbr: TStringField;
    cdClientsluClName: TStringField;
    cdBoxes: TevClientDataSet;
    bLoad: TevBitBtn;
    bBurnCD: TevBitBtn;
    bCreateFileTree: TevBitBtn;
    cbAddRwPreview: TevCheckBox;
    Timer1: TTimer;
    MCDB: TMCDBurner;
    pnCDBurn: TPanel;
    evLabel1: TevLabel;
    lMessage: TevLabel;
    cbChooseDrive: TevComboBox;
    bGo: TevButton;
    bCancel: TevButton;
    cbAutoBurn: TevCheckBox;
    PrBar: TProgressBar;
    cbConvertToPdf: TevCheckBox;
    procedure edPeriodBeginChange(Sender: TObject);
    procedure bLoad_Click(Sender: TObject);
    procedure bLoadClick(Sender: TObject);
    procedure bBurnCDClick(Sender: TObject);
    procedure bCreateFileTreeClick(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure MCDBWriteDone(Sender: TObject; Error: String);
    procedure Timer1Timer(Sender: TObject);
    procedure bGoClick(Sender: TObject);
  private
    { Private declarations }
    bBurnCDReadOnly: Boolean;
    FErrorMessage:string;
    FPath:string;
    bWriting:boolean;
    procedure Process(const BurnCD: Boolean);
    procedure BurnDirectoryToCd(const Path: string);
    function CheckIfCdRwIsPresented: Boolean;
    procedure DoWmWriteDone(var Msg: TMsg); message WM_WRITE_DONE;

  public
    { Public declarations }
    procedure Activate; override;
  end;

implementation

uses EvUtils, EvTypes, EvBasicUtils, SReportSettings, SVmrClasses, SCdMediaWriterForm;

{$R *.dfm}
function IsASCIIFormat(AData: IEvDualStream):boolean;
begin

  if IsRWAFormat(aData) then
    Result :=  false
  else
    Result := true;

end;
procedure CreateFiles(var Path :string;const R: TrwReportResults;ConvertToPDF:boolean);

  function ConvertDataToPDF(R: TrwReportResult):boolean;
  var
     rwPDFInfoRec: TrwPDFDocInfo;
  begin
    Result := true;
    if IsASCIIFormat(R.Data) then
    begin
      Result := false;
      exit;
    end;
    rwPDFInfoRec.Author := '';
    rwPDFInfoRec.Creator := '';
    rwPDFInfoRec.Subject := '';
    rwPDFInfoRec.Title := R.ReportName;
    rwPDFInfoRec.Compression := True;
    rwPDFInfoRec.OwnerPassword := '';
    rwPDFInfoRec.UserPassword := '';
    rwPDFInfoRec.ProtectionOptions := [];
    try
       R.Data := ctx_RWLocalEngine.ConvertRWAtoPDF(R.Data, rwPDFInfoRec);
    except
      Result := false;
    end;
  end;
var
  i, j: Integer;
  Ext:string;
begin
  if Path ='' then
  begin
    repeat
      Path := AppTempFolder + IntToStr(RandomInteger(0, High(Integer)));
    until CreateDir(Path);
  end;
  Path := IncludeTrailingPathDelimiter(Path);

  for i := 0 to R.Count-1 do
  begin
    if not Assigned(R[i].Data) then
      Continue;
    Ext := '.rwa';
    if ConvertToPDF  then
    begin
       if ConvertDataToPDF(R[i])  then
          Ext := '.pdf'
       else
          Ext := '.txt';
    end;
    R[i].VmrFileName := ValidateFileName(IncludeTrailingPathDelimiter(R[i].VmrJobDescr)+ R[i].ReportName);
    if FileExists(Path+ R[i].VmrFileName+ Ext) then
    begin
      j := 2;
      while FileExists(Path+ R[i].VmrFileName+ '('+ IntToStr(j)+ ')'+ Ext) do
        Inc(j);
      R[i].VmrFileName := R[i].VmrFileName+ '('+ IntToStr(j)+ ')';
    end;
    ForceDirectories(ExtractFilePath(Path+ R[i].VmrFileName));
    R[i].Data.SaveToFile(Path+ R[i].VmrFileName+ Ext);
  end;
end;


{ TArchivalScreen }

procedure TArchivalScreen.Activate;
begin
  inherited;
  edPeriodEnd.DateTime := GetEndYear(GetBeginQuarter(Now)-1);
  edPeriodBegin.DateTime := GetBeginYear(edPeriodEnd.DateTime);
  if not CheckIfCdRwIsPresented then
  begin
    EvErrMessage('You do not have a CDRW drive on this machine and can not use "Create CDs" button');
    bBurnCDReadOnly := True;
  end;
  cdTmpCl.Open;
end;

procedure TArchivalScreen.edPeriodBeginChange(Sender: TObject);
begin
  inherited;
  gClients.Visible := False;
  bBurnCD.Enabled := False;
  bCreateFileTree.Enabled := False;
end;

procedure TArchivalScreen.bLoad_Click(Sender: TObject);
begin
  inherited;
  cdClients.Close;
  with TExecDSWrapper.Create('GenericSelect2CurrentWithConditionGrouped') do
  begin
    SetMacro('COLUMNS', 'T1.CL_NBR, cast(SUM(T2.PAGE_COUNT) as INTEGER) PAGE_COUNT');
    SetMacro('TABLE1', 'SB_MAIL_BOX');
    SetMacro('TABLE2', 'SB_MAIL_BOX_CONTENT');
    SetMacro('JOINFIELD', 'SB_MAIL_BOX');
    SetMacro('CONDITION', 'T2.PAGE_COUNT > 0 and Trim(StrCopy(T2.DESCRIPTION, 200, 8)) between :BegDate and :EndDate and SHIPPED_TIME is not NULL');
    SetMacro('Group', 'T1.CL_NBR');
    SetParam('BegDate', FormatDateTime('YYYYMMDD', edPeriodBegin.Date));
    SetParam('EndDate', FormatDateTime('YYYYMMDD', edPeriodEnd.Date));
    cdClients.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
    cdClients.DataRequest(AsVariant);
  end;
  cdClients.Open;
  gClients.Visible := True;
  if not bBurnCDReadOnly then
    bBurnCD.Enabled := True;
  bCreateFileTree.Enabled := True;
end;

procedure TArchivalScreen.bLoadClick(Sender: TObject);
begin
  inherited;
  cdClients.Close;
  with TExecDSWrapper.Create('GenericSelect2CurrentWithConditionGrouped') do
  begin
    SetMacro('COLUMNS', 'T1.CL_NBR, cast(SUM(T2.PAGE_COUNT) as INTEGER) PAGE_COUNT');
    SetMacro('TABLE1', 'SB_MAIL_BOX');
    SetMacro('TABLE2', 'SB_MAIL_BOX_CONTENT');
    SetMacro('JOINFIELD', 'SB_MAIL_BOX');
    SetMacro('CONDITION', 'T2.PAGE_COUNT > 0 and Trim(StrCopy(T2.DESCRIPTION, 200, 8)) between :BegDate and :EndDate'); // and SHIPPED_TIME is not NULL');
//    SetMacro('CONDITION', 'T2.PAGE_COUNT >= 0 '); // and SHIPPED_TIME is not NULL');
    SetMacro('Group', 'T1.CL_NBR');
    SetParam('BegDate', FormatDateTime('YYYYMMDD', edPeriodBegin.Date));
    SetParam('EndDate', FormatDateTime('YYYYMMDD', edPeriodEnd.Date));
    cdClients.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
    cdClients.DataRequest(AsVariant);
  end;
  cdClients.Open;
  gClients.Visible := True;
  if not bBurnCDReadOnly then
    bBurnCD.Enabled := True;
  bCreateFileTree.Enabled := True;
end;

procedure TArchivalScreen.bBurnCDClick(Sender: TObject);
begin
  inherited;
  Process(True);
end;

procedure TArchivalScreen.Process(const BurnCD: Boolean);
  function StripControlChars(const s: string): string;
  var
    i: Integer;
  begin
    Result := Trim(s);
    for i := 1 to Length(Result) do
      if Result[i] in ['\', '/', ':'] then
        Result[i] := '_';
  end;
var
  i: Integer;
  iProgressBar: Integer;
  iRecNo: Integer;
  pBookMark: TBookmark;
  RL: TrwReportResults;
  R: TrwReportResult;
  s: string;
  lOriginalStrings, lReplaceStrings: TStringList;
  function ReplaceStringIfLonger(s: string; const maxl: Integer): string;
  var
    i: Integer;
    sShort: string;
  begin
    s := Trim(s);
    if Length(s) <= maxl then
      Result := s
    else
    begin
      i := lOriginalStrings.IndexOf(s);
      if i <> -1 then
        Result := lReplaceStrings[i]
      else
      begin
        sShort := Copy(s, 1, maxl);
        if lReplaceStrings.IndexOf(sShort) = -1 then
        begin
          lOriginalStrings.Append(s);
          Result := sShort;
          lReplaceStrings.Append(Result);
        end
        else
        begin
          i := 2;
          while lReplaceStrings.IndexOf(sShort+ IntToStr(i)) <> -1 do
            Inc(i);
          begin
            lOriginalStrings.Append(s);
            Result := sShort+ IntToStr(i);
            lReplaceStrings.Append(Result);
          end;
        end;
      end;
    end;
  end;
  const
    RwPreviewFileName = 'isRWPreview.exe';
  procedure AddRwPreview(DestPath: string);
  var
    FromStr, ToStr: string;
  begin
    FromStr := ExtractFilePath(ParamStr(0))+ RwPreviewFileName;
    if FileExists(FromStr) then
    begin
      ToStr := IncludeTrailingPathDelimiter(DestPath)+ RwPreviewFileName;
      if DirectoryExists(ExtractFilePath(ToStr)) then
        CopyFile(FromStr, ToStr, False);
    end;
  end;
  procedure RemoveRwPreview(DestPath: string);
  var
    ToStr: string;
  begin
    ToStr := IncludeTrailingPathDelimiter(DestPath)+ RwPreviewFileName;
    if FileExists(ToStr) then
      DeleteFile(ToStr);
  end;
var
  sDestPath: string;
  sql : string;
  P: IisListOfValues;
  cdBoxes: TEvBasicClientDataSet;
  AFile: IEvDualStream;
begin
sql :=
' SELECT '+
'   Trim(STRCOPY(t2.Description, 0, 100))  JOB_DESCR, '+
'   Trim(STRCOPY(t2.Description, 100, 100))  REP_DESCR, '+
'   STRCOPY(t2.Description, 200, 8)  EVENT_DATE, '+
'   t1.Description, '+
'   t2.File_Name '+
'FROM  Sb_Mail_Box t1,  Sb_Mail_Box_Content  t2,  Sb_Mail_Box_Option  t3 '+
'WHERE '+
'   t1.Sb_Mail_Box_Nbr = t2.Sb_Mail_Box_Nbr  AND '+
'   t1.Sb_Mail_Box_Nbr = t3.Sb_Mail_Box_Nbr  AND '+
'   t2.Sb_Mail_Box_Content_Nbr = t3.Sb_Mail_Box_Content_Nbr '+
'   AND '+
'   t1.Cl_Nbr = :ClNbr  AND '+
'   t3.Option_Tag = ''REPORT_TYPE'' AND t3.Option_Integer_Value <> :ReportType AND '+
'   STRCOPY(t2.Description, 200, 8)  BETWEEN :BegDate AND :EndDate and '+
'   {AsOfNow<t1>} and {AsOfNow<t2>} and {AsOfNow<t3>} ; ';


  if gClients.SelectedList.Count = 0 then
    raise EInvalidParameters.CreateHelp('You need to have at least one client selected', IDH_InvalidParameters);
  if not BurnCD then
    if not SelectDirectory('Select directory to create history file tree in', '.', sDestPath) then
      AbortEx;
  cdClients.DisableControls;
  iRecNo := cdClients.RecNo;
  if not BurnCD then
    ctx_StartWait('Processing', gClients.SelectedList.Count)
  else
  begin
    PrBar.Max := gClients.SelectedList.Count;
    pnCDBurn.Visible := true;
  end;

  lOriginalStrings := TStringList.Create;
  lReplaceStrings := TStringList.Create;
  iProgressBar := 1;
  bWriting := false;
  cdBoxes := TEvBasicClientDataSet.Create(nil);
  P := TisListOfValues.Create;
  try
    cdClients.First;
    while not cdClients.Eof do
    begin
      pBookMark := cdClients.GetBookmark;
      try
      for i := 0 to gClients.SelectedList.Count-1 do
        if cdClients.CompareBookmarks(gClients.SelectedList[i], pBookMark) = 0 then
        begin
          if not BurnCD then
            ctx_UpdateWait('Processing '+ cdClientsluClName.AsString, iProgressBar)
          else
            PrBar.Position := iProgressBar-1;
          Inc(iProgressBar);
          ctx_DataAccess.OpenClient(cdClientsCL_NBR.AsInteger);
          P.AddValue('ReportType', Integer(ord(rtCheck)));
          P.AddValue('EndDate', FormatDateTime('YYYYMMDD', edPeriodEnd.Date));
          P.AddValue('BegDate', FormatDateTime('YYYYMMDD', edPeriodBegin.Date));
          P.AddValue('ClNbr', cdClientsCL_NBR.AsInteger);
          CustomQuery(cdBoxes, sql ,CH_DATABASE_SERVICE_BUREAU, P);
          //SetParam('EndDate', FormatDateTime('YYYYMMDD', edPeriodEnd.Date));
          //ctx_DataAccess.GetSbCustomData(cdBoxes, AsVariant);
          cdBoxes.IndexFieldNames := 'EVENT_DATE';
          RL := TrwReportResults.Create;
          try
            RL.Descr := 'Archive for '+ cdClientsluClName.AsString+ ' '+ DateToStr(edPeriodBegin.Date)+ '-'+ DateToStr(edPeriodEnd.Date);
            cdBoxes.First;
            while not cdBoxes.Eof do
            begin
              try
                AFile := ctx_VMRRemote.RetrieveFile(cdBoxes.FieldByName('FILE_NAME').AsString);
              except
              on e:exception do
                raise exception.create('Evolution has found that a file from VMR history is missing.'#13#10+
                'Please rebuild the VMR history and try again. You may need to delete the folder specified, cleanup the mailbox, and then rebuild history to completely solve the problem.('+
                 e.Message + ')');
              end;

              R := RL.AddReportResult(ReplaceStringIfLonger(StripControlChars(cdBoxes.FieldByName('REP_DESCR').AsString), 40),
                rtUnknown, AFile);

              R.VmrJobDescr :=
                IncludeTrailingPathDelimiter(ReplaceStringIfLonger(StripControlChars(cdBoxes['DESCRIPTION']), 18))+
                IncludeTrailingPathDelimiter(Copy(cdBoxes['EVENT_DATE'], 5, 2)+ '-'+ Copy(cdBoxes['EVENT_DATE'], 7, 2)+ '-'+ Copy(cdBoxes['EVENT_DATE'], 1, 4))+
                ReplaceStringIfLonger(StripControlChars(cdBoxes.FieldByName('JOB_DESCR').AsString), 18);
              if IsASCIIFormat(R.Data) then
                R.ReportType := rtASCIIFile else  R.ReportType := rtReport;
              cdBoxes.Next;
            end;
            if BurnCD then
            begin
              s := '';
              CreateFiles(s, RL,cbConvertToPdf.Checked);
              ctx_UpdateWait;
              try
                if cbAddRwPreview.Checked then
                  AddRwPreview(s);
                BurnDirectoryToCd(s);
              finally
                if cbAddRwPreview.Checked then
                  RemoveRwPreview(s);
                DeleteTempFiles(s, RL);
              end;
            end
            else
            begin
              //CreateResultFiles(IncludeTrailingPathDelimiter(sDestPath)+ cdClientsluClNbr.AsString, RL);
              s :=  IncludeTrailingPathDelimiter(sDestPath)+ Trim(cdClientsluClNbr.AsString);
              CreateFiles(s, RL,cbConvertToPdf.Checked);

              if cbAddRwPreview.Checked then
                AddRwPreview(s);
            end;
            Update;
          finally
            RL.Free;
          end;
        end;
      finally
        cdClients.FreeBookmark(pBookMark);
      end;
      cdClients.Next;
    end;
  finally
    cdBoxes.Free;
    lReplaceStrings.Free;
    lOriginalStrings.Free;
    ctx_EndWait;
    cdClients.RecNo := iRecNo;
    pnCDBurn.Visible := false;
    cdClients.EnableControls;
  end;
end;

procedure TArchivalScreen.bCreateFileTreeClick(Sender: TObject);
begin
  inherited;
  Process(False);
end;

procedure TArchivalScreen.BurnDirectoryToCd(const Path: string);
var
i:integer;
begin
  bWriting := true;
  pnCDBurn.Visible := true;
  bLoad.Enabled := false;
  bBurnCD.Enabled := false;
  bCreateFileTree.Enabled := false;
  evDataSource1.DataSet.DisableControls;
  FPath := Path;
  lMessage.Caption := '';
  if mcdb.InitializeASPI(True) then
  begin
    cbChooseDrive.Items.Assign(mcdb.Devices);
    for i := Pred(cbChooseDrive.Items.Count) downto 0 do
    begin
      mcdb.Device := cbChooseDrive.Items[i];
      if mcdb.DeviceCapabilities * [dcWriteCDR, dcWriteCDRW, dcWriteDVDR] = [] then
        cbChooseDrive.Items.Delete(i);
    end;
    cbChooseDrive.Enabled := cbChooseDrive.Items.Count > 0;
  end
  else
    cbChooseDrive.Enabled := False;
  if not cbChooseDrive.Enabled then
    raise EDeviceNotAvailable.Create('You do not have any CD/DVD writers available');
  cbChooseDrive.ItemIndex := mb_AppSettings.AsInteger['Settings\LastUsedCdWriter'];
  cbAutoBurn.Checked := mb_AppSettings['Settings\AutoBurnStart'] = 'Y';

  mcdb.LoadMedium(True);
  bGo.Enabled := False;
  Timer1.Enabled := True;
  while bWriting do
  begin
    Application.HandleMessage;
  end;
end;

function TArchivalScreen.CheckIfCdRwIsPresented: Boolean;
var
  i: Integer;
  mcdb: TMCDBurner;
begin
  Result := False;
  mcdb := TMCDBurner.Create(nil);
  try
    if mcdb.InitializeASPI(True) then
    begin
      for i := Pred(mcdb.Devices.Count) downto 0 do
      begin
        mcdb.Device := mcdb.Devices[i];
        if mcdb.DeviceCapabilities * [dcWriteCDR, dcWriteCDRW, dcWriteDVDR] <> [] then
        begin
          Result := True;
          Break;
        end;
      end;
    end;
  finally
    mcdb.Free;
  end;
end;

procedure TArchivalScreen.bCancelClick(Sender: TObject);
begin
  inherited;
   bWriting := false;
   bLoad.Enabled := true;
   bBurnCD.Enabled := true;
   bCreateFileTree.Enabled := true;
   evDataSource1.DataSet.EnableControls;
   Timer1.Enabled := false;
   ctx_EndWait;
end;

procedure TArchivalScreen.MCDBWriteDone(Sender: TObject; Error: String);
begin
  inherited;
  FErrorMessage := Error;
  PostMessage(Handle, WM_WRITE_DONE, 0, 0);
end;

procedure TArchivalScreen.Timer1Timer(Sender: TObject);
begin
  inherited;
  mcdb.Device := cbChooseDrive.Text;
  if not mcdb.TestUnitReady then
    lMessage.Caption := 'Load compatible blank media'
  else
    if not (mcdb.DiscType in [2,3,5,7,8]) then
    begin
      lMessage.Caption := 'Load compatible blank media';
      mcdb.LoadMedium(True);
    end
    else
    if mcdb.SessionsOnDisc > 0 then
    begin
      lMessage.Caption := 'Media is not blank.';
      lMessage.Update;
      mcdb.LoadMedium(True);
      Sleep(4000);
    end
    else
    begin
      lMessage.Caption := 'Ready. Press "Write" button';
      bGo.Enabled := True;
      if cbAutoBurn.Checked then
        bGo.Click;
    end;
end;

procedure TArchivalScreen.bGoClick(Sender: TObject);
begin
  Timer1.Enabled := False;
  mb_AppSettings.AsInteger['Settings\LastUsedCdWriter'] := cbChooseDrive.ItemIndex;
  mb_AppSettings['Settings\AutoBurnStart'] := BoolToYN(cbAutoBurn.Checked);
  lMessage.Caption := '';
  lMessage.Update;
  ctx_StartWait('Preparing to write ...');
  {bGo.Enabled := False;
  bGo.Update;
  bCancel.Enabled := False;
  bCancel.Update;}
  try
    MCDB.ClearAll;
    {for i := 0 to Files.Count-1 do
    begin
      Assert(Assigned(MCDB.CreateDir()}
    MCDB.InsertDir('', FPath);
    Assert(MCDB.Prepare(False));
    Assert(MCDB.BurnCD);
    lMessage.Caption := 'Writing';
    ctx_UpdateWait('Writing...');
  except
    //bCancel.Enabled := True;
    ctx_EndWait;
    mcdb.LoadMedium(True);
    raise;
  end;
end;

procedure TArchivalScreen.DoWmWriteDone(var Msg: TMsg);
begin
  bWriting := false;
  ctx_EndWait;
  mcdb.LoadMedium(True);
  if FErrorMessage <> '' then
    raise EDeviceIOException.Create(FErrorMessage);
  bCancel.Click;
end;



initialization
  RegisterClass(TArchivalScreen);

end.
