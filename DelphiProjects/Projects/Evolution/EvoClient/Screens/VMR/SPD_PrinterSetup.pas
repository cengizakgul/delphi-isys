// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PrinterSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SDataStructure, DB, Wwdatsrc, EvTypes, EvContext, EvClasses, EvCommonInterfaces,
   wwdbedit, StdCtrls, Mask, Wwdotdot, Wwdbcomb, ExtCtrls,
  Grids, Wwdbigrd, Wwdbgrid, SFrameEntry, wwdblook, ISBasicClasses, ImgList, isBaseClasses, EvUIComponents, EvClientDataSet,
  Buttons, ComCtrls;

type
  TPrinterSetup = class(TFrameEntry)
    evImageList1: TevImageList;
    Panel1: TPanel;
    sbUpdateVMRPrinters: TSpeedButton;
    lblRebuildPrinter: TLabel;
    PageControl1: TPageControl;
    tsPrinter: TTabSheet;
    SB0: TScrollBox;
    procedure sbUpdateVMRPrintersClick(Sender: TObject);
  private
    p: IevVMRPrintersList;
    sLocations: string;
    UserSidePrinterTag : Integer;
    procedure LoadSetup;
    procedure ActiveButtonClick(Sender: TObject);
    procedure TrayInfoChange(Sender: TObject);
    procedure HOffsetChange(Sender: TObject);
    procedure VOffsetChange(Sender: TObject);
    procedure LocationInfoChange(Sender: TObject);
    procedure DelKeyPressed(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SetModified;
    function  ButtonCaption(const b: Boolean): string;
    procedure EnableBtnControl(const aScrollBox : TScrollBox; const SelectedTag : integer; const Value : boolean);
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

uses EvUtils, StrUtils, Math, SPackageEntry, Spin, evConsts, evUIUtils, ISBasicUtils;

{$R *.dfm}

{ TPrinterSetup }


const SBPRINTERSCROLLBOX = 'SB0';

procedure TPrinterSetup.EnableBtnControl(const aScrollBox : TScrollBox; const SelectedTag : integer; const Value : boolean);
var j : integer;
  b : Boolean;
begin
  for j := 0 to aScrollBox.ControlCount - 1 do
  begin
    if (aScrollBox.Controls[j] is  TevBitBtn) then
    begin
      if (aScrollBox.Name = SBPRINTERSCROLLBOX) and (SelectedTag = UserSidePrinterTag) then
      begin
        b := not value;
        if ( TevBitBtn(aScrollBox.Controls[j]).Tag <> UserSidePrinterTag )  then
        begin
          TevBitBtn(aScrollBox.Controls[j]).Enabled := b;
          if not b then
          begin
            p[TevBitBtn(aScrollBox.Controls[j]).Tag].Active := b;
            TevBitBtn(aScrollBox.Controls[j]).Glyph   := nil;
            TevBitBtn(aScrollBox.Controls[j]).Caption := 'Disabled (click to enable)';
            evImageList1.GetBitmap(0, TevBitBtn(aScrollBox.Controls[j]).Glyph);
          end;
        end;
      end
      else if ( Value ) and
              (((aScrollBox.Name = SBPRINTERSCROLLBOX) and (TevBitBtn(aScrollBox.Controls[j]).Tag = UserSidePrinterTag)))
      then
      begin
        p[TevBitBtn(aScrollBox.Controls[j]).Tag].Active := False;
        TevBitBtn(aScrollBox.Controls[j]).Glyph   := nil;
        TevBitBtn(aScrollBox.Controls[j]).Caption := 'Disabled (click to enable)';
        evImageList1.GetBitmap(0, TevBitBtn(aScrollBox.Controls[j]).Glyph);
      end;
    end;
  end;
end;

procedure TPrinterSetup.Activate;
begin
  inherited;
  if DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', VmrSbLocation, []) then
    sLocations := DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString
  else
    sLocations := '';
  LoadSetup;
end;

procedure TPrinterSetup.ActiveButtonClick(Sender: TObject);
var
  x: integer;
begin
  p[TevBitBtn(Sender).Tag].Active := not p[TevBitBtn(Sender).Tag].Active;
  EnableBtnControl(TScrollBox(TevBitBtn(Sender).Parent),  TevBitBtn(Sender).Tag,  p[TevBitBtn(Sender).Tag].Active );
  TevBitBtn(Sender).Caption := ButtonCaption(p[TevBitBtn(Sender).Tag].Active);
  x := Pos('Enable', TevBitBtn(Sender).Caption);
  TevBitBtn(Sender).Glyph := nil; //refresh
  if x = 1 then
    evImageList1.GetBitmap(1, TevBitBtn(Sender).Glyph)
  else
    evImageList1.GetBitmap(0, TevBitBtn(Sender).Glyph);
  SetModified;
end;

function TPrinterSetup.ButtonCaption(const b: Boolean): string;
begin
  if b then
    Result := 'Enabled (click to disable)'
  else
    Result := 'Disabled (click to enable)';
end;

procedure TPrinterSetup.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  Handled := True;
  if Kind = navOk then
    ctx_VMRRemote.SetPrinterList('ALL', P, False)
  else
  if (Kind = navCancel)
  or (Kind = navRefresh) then
    LoadSetup
  else
    Assert(False);
  (Owner as TFramePackageTmpl).SetButton(NavOK, False);
  (Owner as TFramePackageTmpl).SetButton(NavCancel, False);
end;

procedure TPrinterSetup.DelKeyPressed(Sender: TObject; var Key: Word;
      Shift: TShiftState);
begin
  if Sender is TevComboBox then
    if Key in [46,8] then
    begin
      TevComboBox(Sender).ItemIndex := -1;
      TevComboBox(Sender).OnChange(Sender);
    end;
end;

procedure TPrinterSetup.LoadSetup;
var
  i, j, x: Integer;
  PosX, PosY, PosY2: Integer;
  l, ll: TLabel;
  cb: TevDBLookupCombo;
  cb2: TevComboBox;
  b: TevBitBtn;
  se:TevSpinEdit;
  prName:string;
  aScrollBox, PrevScrollBox : TScrollBox;

  function GettsRemotePrinter( aLocation : string):TScrollBox;
  var
    y:integer;
    aTabSheet :TTabSheet;
  begin
    Result := nil;
    if (Length(aLocation)>0) and (aLocation[1] = '@') then
    begin
      for y:= 0 to PageControl1.PageCount -1 do
      begin
        if PageControl1.Pages[y].Caption = aLocation then
        begin
          Result:= TScrollBox(TTabSheet(PageControl1.Pages[y]).FindChildControl('SB'+inttostr(y)));
          break;
        end;
      end;
      if not Assigned(Result) then
      begin
        aTabSheet := TTabSheet.Create(PageControl1);
        aTabSheet.Name := 'TabSheet'+ IntTostr(PageControl1.PageCount);

        Result := TScrollBox.Create(aTabSheet);
        Result.Name := 'SB'+ IntTostr(PageControl1.PageCount);

        aTabSheet.Caption := aLocation;
        aTabSheet.PageControl := PageControl1;
        aTabSheet.Parent := PageControl1;
        Result.Align := alClient;
        Result.Parent := aTabSheet;
      end;
    end;
  end;
begin
  // New TabSheet
  PosX := 10;
  PosY := 10;
  PrevScrollBox := nil;
  ctx_StartWait;
  try
    for i := PageControl1.PageCount - 1 downto 0 do
    begin
      aScrollBox := TScrollBox(TTabSheet(PageControl1.Pages[i]).FindChildControl('SB'+IntTostr(i)));
      if assigned(aScrollBox) then
      begin
        for j := aScrollBox.ControlCount-1 downto 0 do
           aScrollBox.Controls[j].Parent := nil;
         aScrollBox.DestroyComponents;
      end;
    end;
    p := ctx_VMRRemote.GetPrinterList('ALL');
    for i := 0 to p.Count - 1 do
    begin
      aScrollBox := GettsRemotePrinter(p[i].Location);
      // default Printer
      if not Assigned(aScrollBox) then
        aScrollBox := sb0;

      if not Assigned(PrevScrollBox) or
        (PrevScrollBox.Name <> aScrollBox.Name) then
      begin
        // New TabSheet
        PosX := 10;
        PosY := 10;
        PrevScrollBox := aScrollBox;
      end;

      l := TLabel.Create(aScrollBox);
      with l do
      begin
        Name := '';
        Left := PosX;
        Top  := PosY;

        prName := p[i].Name;
        GetNextStrValue(prName, '^');
        Caption := prName;
        if prName = VMR_USER_SIDE_PRINTER_NAME then
          UserSidePrinterTag := i;
        Font.Style := [fsBold];
        Parent := aScrollBox;
      end;
      b := TevBitBtn.Create(aScrollBox);
      b.Tag := i;
      b.Caption := ButtonCaption(p[i].Active);
      x := Pos('Enable', b.Caption);
      if x = 1 then
        evImageList1.GetBitmap(1, b.Glyph)
      else
        evImageList1.GetBitmap(0, b.Glyph);
      b.Left := PosX+ Max(180, l.Width);
      b.Top  := PosY;
      b.Width := 170;
      b.OnClick := ActiveButtonClick;
      b.Parent := aScrollBox;
      PosY := PosY+ Max(l.Height, b.Height)+ 5;
      if ( prName <> VMR_USER_SIDE_PRINTER_NAME ) then
      begin
        l := TLabel.Create(aScrollBox);
        with l do
        begin
          Name := '';
          Left := PosX;
          Top  := PosY;
          Caption := 'Location:';
          Parent := aScrollBox;
        end;

        //Evo Printer Relay
        if ( Length(p[i].Location)> 0) and (p[i].Location[1]='@') then
        begin
          ll := TLabel.Create(aScrollBox);
          with ll do
          begin
            Name := '';
            Left := l.Left+ 180;
            Top  := PosY;
            width := 100;
            Caption := p[i].Location;
            Font.Color := clRed;
            Parent := aScrollBox;
          end;
          PosY := PosY+ l.Height + 5;
        end
        else begin
          cb2 := TevComboBox.Create(aScrollBox);
          cb2.Parent := aScrollBox;
          cb2.Tag := i;
          cb2.Items.CommaText := sLocations;
          cb2.Left := l.Left+ 180;
          cb2.Top  := PosY;
          cb2.Width := 100;
          cb2.ItemIndex := cb2.Items.IndexOf(p[i].Location);
          if (p[i].Location <> '') and (cb2.ItemIndex = -1) then
            l.Font.Color := clRed;
          cb2.OnChange := LocationInfoChange;
          cb2.OnKeyDown := DelKeyPressed;
          PosY := PosY+ Max(l.Height, cb2.Height)+ 5;
        end;

        l := TLabel.Create(aScrollBox);
        with l do
        begin
          Name := '';
          Left := PosX;
          Top  := PosY;
          Caption := 'Vertical offset:';
          Parent := aScrollBox;
        end;
        se := TevSpinEdit.Create(aScrollBox);
        with se do
        begin
          Parent := aScrollBox;
          Tag := i;
          Left := l.Left+ 180;
          Top  := PosY;
          Width := 100;
          Value := p[i].VOffset;
          onChange := VOffsetChange;
        end;

        PosY := PosY+ Max(l.Height, se.Height)+ 5;

        l := TLabel.Create(aScrollBox);
        with l do
        begin
          Name := '';
          Left := PosX;
          Top  := PosY;
          Caption := 'Horizontal offset:';
          Parent := aScrollBox;
        end;
        se := TevSpinEdit.Create(aScrollBox);
        with se do
        begin
          Parent := aScrollBox;
          Tag := i;
          Left := l.Left+ 180;
          Top  := PosY;
          Width := 100;
          Value := p[i].HOffset;
          onChange := HOffsetChange;
        end;

        PosY2 := PosY+ Max(l.Height, b.Height)+ 5;
        //PosY := PosY+ Max(l.Height, b.Height)+ 5;

        for j := 0 to p[i].BinCount - 1 do
          if p[i].Bins[j].Name <> '' then
          begin
            l := TLabel.Create(aScrollBox);
            with l do
            begin
              Name := '';
              Left := PosX;
              Top  := PosY2;
              Caption := p[i].Bins[j].Name;
              Parent := aScrollBox;
            end;
            cb := TevDBLookupCombo.Create(aScrollBox);
            cb.Name := '';
            cb.Left := PosX+ Max(180, l.Width);
            cb.Top := PosY2;
            cb.Width := 300;
            cb.Parent := aScrollBox;
            cb.Selected.Append('DESCRIPTION'#9'40'#9'Description'#9'F');
            cb.LookupTable := DM_SERVICE_BUREAU.SB_PAPER_INFO;
            cb.LookupField := 'SB_PAPER_INFO_NBR';
            cb.LookupValue := IntToStr(p[i].Bins[j].SbPaperNbr);
            cb.AllowClearKey := True;
            cb.Tag := i* 100+ j;
            cb.OnChange := TrayInfoChange;
            Inc(PosY2, cb.Height+ 4);
          end;
        PosY := PosY2+ 50;
      end;
    end;
  finally
   ctx_EndWait;
  end;
end;

procedure TPrinterSetup.LocationInfoChange(Sender: TObject);
begin
  with TevComboBox(Sender) do
  begin
    if ItemIndex = -1 then
      p[Tag].Location := ''
    else
      p[Tag].Location := Items[ItemIndex];
  end;
  SetModified;
end;

procedure TPrinterSetup.HOffsetChange(Sender: TObject);
begin
  p[(Sender as TComponent).Tag].HOffset := TevSpinEdit(Sender).Value;
  SetModified;
end;

procedure TPrinterSetup.VOffsetChange(Sender: TObject);
begin
  p[(Sender as TComponent).Tag].VOffset := TevSpinEdit(Sender).Value;
  SetModified;
end;


procedure TPrinterSetup.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS([DM_SERVICE_BUREAU.SB_PAPER_INFO, DM_SERVICE_BUREAU.SB_OPTION], SupportDataSets);
end;

procedure TPrinterSetup.SetModified;
begin
  (Owner as TFramePackageTmpl).SetButton(NavOK, True);
  (Owner as TFramePackageTmpl).SetButton(NavCancel, True);
end;

procedure TPrinterSetup.TrayInfoChange(Sender: TObject);
var
  iPr, iTr: Integer;
begin
  with TevDBLookupCombo(Sender) do
  begin
    iTr := Tag mod 100;
    iPr := Tag div 100;
    if Text <> '' then
      p[iPr].Bins[iTr].SbPaperNbr := StrToIntDef(LookupValue, 0)
    else
      p[iPr].Bins[iTr].SbPaperNbr := 0;
  end;
  SetModified;
end;

procedure TPrinterSetup.sbUpdateVMRPrintersClick(Sender: TObject);
var
  aPrList: IevVMRPrintersList;
begin
   if EvMessage('This will delete current setup for each printer, you will need to set up all again.'#13#10+
                'Are you sure you want to continue ?', mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      try
        //Get Server Side Printers and User Side Printer
        //Not Evo printer relay
        //aPrList.name has ^
        aPrList := ctx_VMRRemote.GetSBPrinterList;
        ctx_VMRRemote.SetPrinterList(VMR_SB_SIDE_PRINTER_NAME, aPrList, True);
        LoadSetup;
      except
        on E:Exception do
          EvExceptMessage(E);
      end;
    end;
end;


initialization
  RegisterClass(TPrinterSetup);

end.
