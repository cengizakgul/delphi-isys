inherited EDIT_SY_MEDIA_TYPE: TEDIT_SY_MEDIA_TYPE
  object evDBGrid1: TevDBGrid [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 200
    DisableThemesInTitle = False
    Selected.Strings = (
      'CLASS_NAME'#9'40'#9'Class name'#9'F'
      'NAME'#9'40'#9'Name'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SY_MEDIA_TYPE\evDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsDetail
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 200
    Width = 435
    Height = 65
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object evLabel1: TevLabel
      Left = 16
      Top = 8
      Width = 60
      Height = 16
      Caption = '~Classname'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel2: TevLabel
      Left = 320
      Top = 8
      Width = 37
      Height = 16
      Caption = '~Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evDBComboBox1: TevDBComboBox
      Left = 16
      Top = 24
      Width = 257
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'CLASS_NAME'
      DataSource = wwdsDetail
      DropDownCount = 8
      ItemHeight = 0
      Items.Strings = (
        'Test1'
        'Test2')
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 0
      UnboundDataType = wwDefault
    end
    object evDBEdit1: TevDBEdit
      Left = 320
      Top = 24
      Width = 249
      Height = 21
      DataField = 'NAME'
      DataSource = wwdsDetail
      Picture.PictureMaskFromDataSet = False
      Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 256
    Top = 24
  end
end
