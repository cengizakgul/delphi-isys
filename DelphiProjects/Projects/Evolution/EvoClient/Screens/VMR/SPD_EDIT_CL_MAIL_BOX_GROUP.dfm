inherited EDIT_CL_MAIL_BOX_GROUP: TEDIT_CL_MAIL_BOX_GROUP
  Width = 711
  Height = 482
  inherited Panel1: TevPanel
    Width = 711
    inherited pnlFavoriteReport: TevPanel
      Left = 596
    end
    inherited pnlTopRight: TevPanel
      Width = 596
    end
  end
  inherited PageControl1: TevPageControl
    Width = 711
    Height = 428
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 703
        Height = 399
        inherited pnlBorder: TevPanel
          Width = 699
          Height = 395
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 699
          Height = 395
          inherited Panel3: TevPanel
            Width = 651
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 651
            Height = 288
            inherited Splitter1: TevSplitter
              Height = 284
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 284
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 204
                IniAttributes.SectionName = 'TEDIT_CL_MAIL_BOX_GROUP\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 295
              Height = 284
              Title = 'Mail Box Groups'
              object evDBTreeView1: TevDBTreeView
                Left = 18
                Top = 48
                Width = 245
                Height = 204
                Align = alClient
                AutoExpand = True
                DragMode = dmAutomatic
                HideSelection = False
                Indent = 19
                ReadOnly = True
                RowSelect = True
                TabOrder = 0
                CaptionField = 'DESCRIPTION'
                KeyField = 'CL_MAIL_BOX_GROUP_NBR'
                UpLinkField = 'UP_LEVEL_MAIL_BOX_GROUP_NBR'
                DataSource = wwdsDetail
              end
            end
          end
        end
      end
    end
    object tsDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 3
      object evLabel5: TevLabel
        Left = 24
        Top = 8
        Width = 62
        Height = 16
        Alignment = taRightJustify
        Caption = '~Description'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel6: TevLabel
        Left = 24
        Top = 32
        Width = 73
        Height = 16
        Alignment = taRightJustify
        Caption = '~Auto Release'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel7: TevLabel
        Left = 29
        Top = 64
        Width = 84
        Height = 13
        Caption = 'Notification E-mail'
      end
      object evLabel8: TevLabel
        Left = 13
        Top = 88
        Width = 100
        Height = 13
        Caption = 'System Cover Report'
      end
      object evLabel9: TevLabel
        Left = 28
        Top = 112
        Width = 85
        Height = 13
        Caption = 'S/B Cover Report'
      end
      object evLabel10: TevLabel
        Left = 90
        Top = 136
        Width = 23
        Height = 13
        Caption = 'Note'
      end
      object evLabel11: TevLabel
        Left = 28
        Top = 232
        Width = 85
        Height = 13
        Caption = 'Uplinked Mail Box'
      end
      object evLabel12: TevLabel
        Left = 29
        Top = 264
        Width = 84
        Height = 13
        Caption = 'Secondary Period'
      end
      object evLabel13: TevLabel
        Left = 256
        Top = 264
        Width = 3
        Height = 13
        Caption = '-'
      end
      object evLabel14: TevLabel
        Left = 18
        Top = 299
        Width = 96
        Height = 13
        Caption = 'Reply Email Address'
      end
      object lbCycledRef: TLabel
        Left = 368
        Top = 232
        Width = 201
        Height = 25
        AutoSize = False
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        WordWrap = True
      end
      object lVmrPrinterLocation: TevLabel
        Left = 258
        Top = 331
        Width = 41
        Height = 13
        Caption = 'Location'
      end
      object Label1: TLabel
        Left = 16
        Top = 387
        Width = 385
        Height = 13
        AutoSize = False
        Caption = 'Text to be included with Emails (120 chars max.)'
      end
      object evDBEdit1: TevDBEdit
        Left = 120
        Top = 8
        Width = 241
        Height = 21
        DataField = 'DESCRIPTION'
        DataSource = wwdsDetail
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object evDBComboBox1: TevDBComboBox
        Left = 120
        Top = 32
        Width = 241
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'AUTO_RELEASE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 404
        Top = 253
        Width = 145
        Height = 41
        Caption = '~Required Part'
        Columns = 2
        DataField = 'REQUIRED'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBEdit2: TevDBEdit
        Left = 120
        Top = 64
        Width = 241
        Height = 21
        DataField = 'NOTIFICATION_EMAIL'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object edSyCoverReport: TevDBLookupCombo
        Left = 120
        Top = 88
        Width = 241
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'REPORT_DESCRIPTION'#9'F')
        DataField = 'SY_COVER_LETTER_REPORT_NBR'
        DataSource = wwdsDetail
        LookupField = 'NBR'
        Style = csDropDownList
        TabOrder = 4
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object edSbCoverReport: TevDBLookupCombo
        Left = 120
        Top = 112
        Width = 241
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'REPORT_DESCRIPTION'#9'F')
        DataField = 'SB_COVER_LETTER_REPORT_NBR'
        DataSource = wwdsDetail
        LookupField = 'NBR'
        Style = csDropDownList
        TabOrder = 5
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object EvDBMemo1: TEvDBMemo
        Left = 120
        Top = 136
        Width = 449
        Height = 89
        DataField = 'NOTE'
        DataSource = wwdsDetail
        TabOrder = 6
      end
      object edUpLinkMailBoxGroup: TevDBLookupCombo
        Left = 120
        Top = 232
        Width = 241
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F')
        DataField = 'UP_LEVEL_MAIL_BOX_GROUP_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
        LookupField = 'CL_MAIL_BOX_GROUP_NBR'
        Style = csDropDownList
        TabOrder = 7
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object evDBDateTimePicker1: TevDBDateTimePicker
        Left = 120
        Top = 264
        Width = 121
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'SEC_METHOD_ACTIVE_FROM'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 8
      end
      object evDBDateTimePicker2: TevDBDateTimePicker
        Left = 272
        Top = 264
        Width = 121
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'SEC_METHOD_ACTIVE_TO'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 9
      end
      object cbDirectEmailVouchers: TevCheckBox
        Left = 16
        Top = 328
        Width = 233
        Height = 17
        Caption = 'Email Payroll Vouchers directly to employees'
        TabOrder = 10
        OnClick = AdditionalOptionChange
      end
      object cbBlockAutoEmailVouchers: TevCheckBox
        Left = 16
        Top = 344
        Width = 201
        Height = 17
        Caption = 'Block automatic emailing for vouchers'
        TabOrder = 11
        OnClick = AdditionalOptionChange
      end
      object edVmrReplyEmail: TevEdit
        Left = 120
        Top = 296
        Width = 449
        Height = 21
        TabOrder = 12
        OnChange = AdditionalOptionChange
      end
      object cbVmrPrinterLocation: TevComboBox
        Left = 306
        Top = 327
        Width = 263
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        ItemHeight = 0
        TabOrder = 13
        OnChange = AdditionalOptionChange
        OnKeyDown = cbVmrPrinterLocationKeyDown
      end
      object cbEmailSelfServe: TevCheckBox
        Left = 16
        Top = 360
        Width = 201
        Height = 17
        Caption = 'Email Self Serve Employees'
        TabOrder = 14
        OnClick = AdditionalOptionChange
      end
      object cbIncludeDescInEmails: TevCheckBox
        Left = 374
        Top = 10
        Width = 201
        Height = 17
        Caption = 'Include Description in Emails'
        TabOrder = 16
        OnClick = AdditionalOptionChange
      end
      object edSelfServeEmailText: TevMemo
        Left = 16
        Top = 405
        Width = 553
        Height = 41
        Lines.Strings = (
          'edSelfServeEmailText')
        TabOrder = 15
        OnChange = AdditionalOptionChange
      end
    end
    object tshtDetails: TTabSheet
      Caption = 'Primary Settings'
      ImageIndex = 11
      object evPanel1: TevPanel
        Left = 0
        Top = 0
        Width = 703
        Height = 57
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel1: TevLabel
          Left = 8
          Top = 8
          Width = 86
          Height = 16
          Caption = '~Delivery Method'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel3: TevLabel
          Left = 8
          Top = 32
          Width = 79
          Height = 16
          Caption = '~Delivery Media'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edType: TevDBLookupCombo
          Left = 128
          Top = 8
          Width = 377
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME'#9'F')
          DataField = 'PRIM_SB_DELIVERY_METHOD_NBR'
          DataSource = wwdsDetail
          LookupTable = cdTypes
          LookupField = 'SB_DELIVERY_METHOD_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object edMedia: TevDBLookupCombo
          Left = 128
          Top = 32
          Width = 377
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'luMediaName'#9'40'#9'luMediaName'#9'F')
          DataField = 'PRIM_SB_MEDIA_TYPE_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
          LookupField = 'SB_MEDIA_TYPE_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnBeforeDropDown = edMediaBeforeDropDown
          OnCloseUp = edMediaCloseUp
        end
      end
      object pcPrimarySettings: TevPageControl
        Left = 0
        Top = 57
        Width = 703
        Height = 342
        ActivePage = tsPriService
        Align = alClient
        TabOrder = 1
        object tsPriService: TTabSheet
          Caption = 'Service Settings'
        end
        object tsPriMedia: TTabSheet
          Caption = 'Media Settings'
          ImageIndex = 1
        end
        object tsPriSbType: TTabSheet
          Caption = 'SB Service Settings (Read Only)'
          ImageIndex = 2
        end
        object tsPriSbMedia: TTabSheet
          Caption = 'SB Media Settings (Read Only)'
          ImageIndex = 3
        end
      end
    end
    object tshtDetails2: TTabSheet
      Caption = 'Secondary Settings'
      ImageIndex = 2
      object evPanel4: TevPanel
        Left = 0
        Top = 0
        Width = 703
        Height = 57
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel2: TevLabel
          Left = 8
          Top = 8
          Width = 77
          Height = 13
          Caption = 'Delivery Method'
        end
        object evLabel4: TevLabel
          Left = 8
          Top = 32
          Width = 70
          Height = 13
          Caption = 'Delivery Media'
        end
        object edSecType: TevDBLookupCombo
          Left = 128
          Top = 8
          Width = 377
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME'#9'F')
          DataField = 'SEC_SB_DELIVERY_METHOD_NBR'
          DataSource = wwdsDetail
          LookupTable = cdTypes
          LookupField = 'SB_DELIVERY_METHOD_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object edSecMedia: TevDBLookupCombo
          Left = 128
          Top = 32
          Width = 377
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'luMediaName'#9'40'#9'luMediaName'#9'F')
          DataField = 'SEC_SB_MEDIA_TYPE_NBR'
          DataSource = wwdsDetail
          LookupTable = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
          LookupField = 'SB_MEDIA_TYPE_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnBeforeDropDown = edSecMediaBeforeDropDown
          OnCloseUp = edSecMediaCloseUp
        end
      end
      object pcSecondarySettings: TevPageControl
        Left = 0
        Top = 57
        Width = 703
        Height = 342
        ActivePage = tsSecSbMedia
        Align = alClient
        TabOrder = 1
        object tsSecService: TTabSheet
          Caption = 'Service Settings'
        end
        object tsSecMedia: TTabSheet
          Caption = 'Media Settings'
          ImageIndex = 1
        end
        object tsSecSbType: TTabSheet
          Caption = 'SB Service Settings (Read Only)'
          ImageIndex = 2
        end
        object tsSecSbMedia: TTabSheet
          Caption = 'SB Media Settings (Read Only)'
          ImageIndex = 3
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
    Enabled = False
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    OnUpdateData = wwdsDetailUpdateData
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 168
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 400
  end
  object cdTypes: TevClientDataSet
    FieldDefs = <
      item
        Name = 'SB_DELIVERY_METHOD_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CLASS_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end>
    Left = 288
    Top = 16
    object cdTypesSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
    end
    object cdTypesCLASS_NAME: TStringField
      FieldName = 'CLASS_NAME'
      Size = 40
    end
    object cdTypesNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
  end
end
