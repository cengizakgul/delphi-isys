// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SVmrScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls, Graphics,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList,
  ISBasicClasses, EvCommonInterfaces, EvContext, EvMainboard,
  EvUIComponents, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;


type
  TVmrFrm = class(TFramePackageTmpl, IevVMRScreens)
  protected
    function InitPackage: Boolean; override;
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;
  end;

implementation

{$R *.DFM}

var
  VmrFrm: TVmrFrm;

function GetVMRScreens: IevVMRScreens;
begin
  if not Assigned(VmrFrm) then
    VmrFrm := TVmrFrm.Create(nil);
  Result := VmrFrm;
end;



{ TSystemFrm }

function TVmrFrm.InitPackage: Boolean;
begin
  if Context.License.VMR then
    Result := inherited InitPackage
  else
    Result := False;
end;

function TVmrFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TVmrFrm.PackageCaption: string;
begin
  Result := 'VMR';
end;

function TVmrFrm.PackageSortPosition: string;
begin
  Result := '002';
end;

procedure TVmrFrm.UserPackageStructure;
begin
  AddStructure('\&iSystems\Virtual Mail Room|090');
  AddStructure('\&iSystems\Virtual Mail Room\Delivery Services|110', 'TEDIT_SY_DELIVERY_SERVICE');
  AddStructure('\&iSystems\Virtual Mail Room\Delivery Methods|120', 'TEDIT_SY_DELIVERY_METHOD');
  AddStructure('\&iSystems\Virtual Mail Room\Media Types|130', 'TEDIT_SY_MEDIA_TYPE');
  AddStructure('\&Bureau\Virtual Mail Room|770');  
  AddStructure('\&Bureau\Virtual Mail Room\General Setup|009', 'TVmrPrinterLocationSetup');
  AddStructure('\&Bureau\Virtual Mail Room\Delivery Services|010', 'TEDIT_SB_DELIVERY_SERVICE');
  AddStructure('\&Bureau\Virtual Mail Room\Delivery Methods|020', 'TEDIT_SB_DELIVERY_METHOD');
  AddStructure('\&Bureau\Virtual Mail Room\Media Types|030', 'TEDIT_SB_MEDIA_TYPE');
  AddStructure('\&Bureau\Virtual Mail Room\Paper Info|040', 'TEDIT_SB_PAPER_INFO');
  AddStructure('\&Bureau\Virtual Mail Room\Printer Info|040', 'TPrinterSetup');
  AddStructure('\&Bureau\Virtual Mail Room\Courier Bill Entry|050', 'TCourierBillEntry');
  AddStructure('\&Bureau\Virtual Mail Room\Create Client Archive CDs|060', 'TArchivalScreen');
  AddStructure('\&Bureau\Virtual Mail Room\Rebuild VMR history|070', 'TReprintPayrollsToVmr');
  AddStructure('\&Bureau\Virtual Mail Room\Utilities|080', 'TEDIT_SB_VMR_UTILS');  
  AddStructure('\C&lient\Virtual Mail Room\Mail Box Groups|030', 'TEDIT_CL_MAIL_BOX_GROUP');
  AddStructure('\Operatio&ns\Virtual Mail Room\Unreleased Mail Boxes|030', 'TEDIT_SB_MAIL_BOX');
  AddStructure('\Operatio&ns\Virtual Mail Room\Tracking by Job|040', 'TTrackingByJob');
  AddStructure('\Operatio&ns\Virtual Mail Room\Tracking by Address|050', 'TTrackingByAddresse');
  AddStructure('\Operatio&ns\Virtual Mail Room\Courier Pickup|060', 'TCourierPickup');
  AddStructure('\Operatio&ns\Virtual Mail Room\Printer Pickup|070', 'TPrinterPickup');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetVMRScreens, IevVMRScreens, 'Screens VMR');

finalization
  VmrFrm.Free;

end.
