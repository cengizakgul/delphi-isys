// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_OPTION_BASE;

interface

uses
  SDataStructure,  wwdbedit, Wwdotdot,
  Wwdbcomb, wwdbdatetimepicker, Mask, DBCtrls, StdCtrls, wwdblook,
  ExtCtrls, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls,
  Classes, SFrameEntry, SPD_EDIT_BROWSE_DETAIL_BASE, SVmrClasses, SVmrOptionEditFrame,
  SDDClasses, ISBasicClasses, SDataDictsystem, SDataDictbureau, EvExceptions, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo;

type
  TEDIT_OPTION_BASE = class(TEDIT_BROWSE_DETAIL_BASE)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    edType: TevDBLookupCombo;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evPanel2: TevPanel;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
  private
    FOptionedObject: TVmrOptionedType;
    FOptionEditFrame: TVmrOptionEditFrame;
  protected
    function CreateOptionedObject(const c: TVmrOptionedTypeClass): TVmrOptionedType; virtual; abstract;
    function CreateOptionEditFrame(const o: TVmrOptionedType): TVmrOptionEditFrame; virtual; abstract;
    property OptionEditFrame: TVmrOptionEditFrame read FOptionEditFrame;
  public
    property OptionedObject: TVmrOptionedType read FOptionedObject;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses Variants, SPackageEntry, SysUtils, EvTypes, Math, EvUtils;

{$R *.DFM}

procedure TEDIT_OPTION_BASE.Activate;
begin
  inherited;
  wwdsDetail.Enabled := True;
end;

function TEDIT_OPTION_BASE.GetInsertControl: TWinControl;
begin
  Result := edType;
end;

procedure TEDIT_OPTION_BASE.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  if Assigned(OptionEditFrame) then OptionEditFrame.ButtonClicked(Kind, Handled);
  if not Handled then
  begin
    if Assigned(FOptionedObject) then
      if Kind = NavOK then
      begin
        if FOptionedObject.Options.Active then
        begin
          FOptionedObject.Options.Apply;
          //FOptionedObject.Options.OptionDataSet.ParentDataSet := TevClientDataSet(wwdsDetail.DataSet);
          ctx_DataAccess.TempCommitDisable := True;
          try
            TFramePackageTmpl(Owner).AddPostDataSet(FOptionedObject.Options.OptionDataSet);
          finally
            ctx_DataAccess.TempCommitDisable := False;
          end;
          TFramePackageTmpl(Owner).ClickButton(NavCommit);
          Handled := True;
        end;
      end
      else
      if Kind = NavCancel then
      begin
        if FOptionedObject.Options.Active then
        begin
          if FOptionedObject.Options.Modified then
            FOptionedObject.Options.Cancel;
          FOptionedObject.Options.CancelUpdates;
        end;
      end;
  end;
end;

procedure TEDIT_OPTION_BASE.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
  procedure lFreeAndNil(var Obj);
  {var
    Temp: TObject;
    a: TArrayDS;}
  begin
    {Temp := TObject(Obj);
    if Temp is TVmrOptionedType then
    begin
      RemoveDS(TVmrOptionedType(Temp).Options.OptionDataSet, FDataSetsToPost);
      RemoveDS(TVmrOptionedType(Temp).Options, FDataSetsToPost);
      a := TFramePackageTmpl(Owner).EditDataSets;
      RemoveDS(TVmrOptionedType(Temp).Options.OptionDataSet, a);
      RemoveDS(TVmrOptionedType(Temp).Options, a);
      TFramePackageTmpl(Owner).EditDataSets := a;
    end;}
    SysUtils.FreeAndNil(Obj);
  end;
var
  //a: TArrayDS;
  c: TClass;
  cd: TevClientDataSet;
begin
  inherited;
  if (not Assigned(Field)
  and (not Assigned(FOptionedObject) or (FOptionedObject.KeyValue <> wwdsDetail.KeyValue) or (Tag <> StrToIntDef(edType.LookupValue, 0)))
  {and (wwdsDetail.State = dsBrowse)})
  or (Assigned(Field) and (Field.FieldName = edType.LookupField)) then
  begin
    lFreeAndNil(FOptionEditFrame);
    lFreeAndNil(FOptionedObject);
    Tag := StrToIntDef(edType.LookupValue, 0);
    if wwdsDetail.DataSet.RecordCount > 0 then
    begin
      if Assigned(Field) then
      begin
        cd := TevClientDataSet.Create(nil);
        try
          cd.CloneCursor(TevClientDataSet(Field.DataSet), False);
          if not VarIsNull(cd.Lookup(edType.LookupField, Field.AsInteger, edType.LookupField)) then
            raise EInconsistentData.CreateHelp('You already have this type setup', IDH_ConsistencyViolation);
        finally
          cd.Free;
        end;
      end;
      c := FindVmrClass(edType.LookupTable['CLASS_NAME']);
      if Assigned(c) then
        if c.InheritsFrom(TVmrOptionedType) then
        begin
          FOptionedObject := CreateOptionedObject(TVmrOptionedTypeClass(c));
          {if FOptionedObject.Options.Active then
          begin
            a := TFramePackageTmpl(Owner).EditDataSets;
            AddDS([FOptionedObject.Options, FOptionedObject.Options.OptionDataSet], a);
            TFramePackageTmpl(Owner).EditDataSets := a;
          end;}
          FOptionedObject.KeyValue := wwdsDetail.KeyValue;
          FOptionEditFrame := CreateOptionEditFrame(FOptionedObject);
          if Assigned(FOptionEditFrame) then
          begin
            FOptionEditFrame.Align := alClient;
            FOptionEditFrame.Parent := evPanel2;
          end;
        end;
    end;
  end;
end;

end.
