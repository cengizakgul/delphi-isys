inherited CourierPickup: TCourierPickup
  object gBrowse: TevDBGrid [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 136
    DisableThemesInTitle = False
    Selected.Strings = (
      'DESCRIPTION'#9'25'#9'Description'#9'F'
      'ADDRESSEE'#9'40'#9'Addressee'#9'F'
      'luClNbr'#9'8'#9'Cl#'#9'F'
      'luClName'#9'20'#9'Client'#9'F'
      'luServiceName'#9'10'#9'Service'#9'F'
      'luMethodName'#9'10'#9'Method'#9'F'
      'luMediaName'#9'10'#9'Media'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TCourierPickup\gBrowse'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsMaster
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnDblClick = gBrowseDblClick
    PaintOptions.AlternatingRowColor = clCream
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 225
    Width = 435
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bMark: TevBitBtn
      Left = 8
      Top = 8
      Width = 145
      Height = 25
      Caption = 'Mark as Picked Up'
      TabOrder = 0
      OnClick = bMarkClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD888888888
        8888DDDD888888888888DDD87777777FFFF8DFFFFFFFFFFDDDD8DCCCCCCCCCC7
        FFF8F8888888888FDDD8CCCCCCCCCCCCFFF8888888888888DDD8CCFFFFFFCCCC
        FFF888FFFFFF8888DDD8CCCCCCCCCCCCFFF8888888888888DDD8CCCCFFFFFCCC
        FFF88888FFFFF888DDD8CCCCCCCCCCCCFFF8888888888888DDD8CCCCCCFFFFCC
        FFF8888888FFFF88DDD8CCCCCCCCCCCCFFF8888888888888DDD8DCCCCCCCCCCF
        FFF8D8888888888DDDD8DDD8FFFFFFF88888DDDDDDDDDDD88888DDD8FFFFFF8F
        FF7DDDDD8DDDDD8DDD8DDDD8FFFFFF8FF7DDDDDD8DDDDD8DD8DDDDD8FFFFFF8F
        7DDDDDDD8DDDDD8D8DDDDDD888888887DDDDDDDD88888888DDDD}
      NumGlyphs = 2
    end
    object bLocate: TevBitBtn
      Left = 184
      Top = 8
      Width = 177
      Height = 25
      Caption = 'Locate and mark By Barcode'
      TabOrder = 1
      OnClick = bLocateClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
        DDDDDFFFFFFFFFFDDDDD8CCCCCCCCCC8DDDDF8888888888FDDDDCCCCCCCCCCCC
        DDDD888888888888DDDDCCFFFFFFCCCCFFFF88FFFFFF8888DDDDCCCCCCCCCCCC
        FFFF888888888888DDDDCCCCFFFFFCCCFFFF8888FFFFF888DDDDCCCCCCCCCCCC
        FFFF888888888888DDDDCCCCCCFFFFCCFFFF888888FFFF88DDDDCCCCCCCCCCCC
        FFFF888888888888DDDDDCCCCCCCCCCFFFFFD8888888888DDDDDDDFFFFFFFFFF
        FFFFDDDDDDDDDDDDDDDDDDFF08070708070FDDDD88D8D8D88D8DDDFF08070708
        070FDDDD88D8D8D88D8DDDFF08070708070FDDDD88D8D8D88D8DDDFF08070708
        070FDDDD88D8D8D88D8DDDFFFFFFFFFFFFFFDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
    end
  end
  object EvDBMemo1: TEvDBMemo [2]
    Left = 0
    Top = 136
    Width = 435
    Height = 89
    Align = alBottom
    DataField = 'NOTE'
    DataSource = wwdsMaster
    TabOrder = 2
  end
  inherited wwdsMaster: TevDataSource
    DataSet = cdBrowse
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 280
    Top = 8
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 312
    Top = 8
  end
  object cdBrowse: TevClientDataSet
    ProviderName = 'SB_MAIL_BOX_PROV'
    ClientID = -1
    Left = 160
    Top = 32
    object cdBrowseSB_MAIL_BOX_NBR: TIntegerField
      FieldName = 'SB_MAIL_BOX_NBR'
    end
    object cdBrowseCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdBrowseCL_MAIL_BOX_GROUP_NBR: TIntegerField
      FieldName = 'CL_MAIL_BOX_GROUP_NBR'
    end
    object cdBrowseCOST: TFloatField
      FieldName = 'COST'
    end
    object cdBrowseREQUIRED: TStringField
      FieldName = 'REQUIRED'
      FixedChar = True
      Size = 1
    end
    object cdBrowseNOTIFICATION_EMAIL: TStringField
      FieldName = 'NOTIFICATION_EMAIL'
      Size = 80
    end
    object cdBrowsePR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdBrowseRELEASED_TIME: TDateTimeField
      FieldName = 'RELEASED_TIME'
    end
    object cdBrowsePRINTED_TIME: TDateTimeField
      FieldName = 'PRINTED_TIME'
    end
    object cdBrowseSB_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SB_COVER_LETTER_REPORT_NBR'
    end
    object cdBrowseSCANNED_TIME: TDateTimeField
      FieldName = 'SCANNED_TIME'
    end
    object cdBrowseSHIPPED_TIME: TDateTimeField
      FieldName = 'SHIPPED_TIME'
    end
    object cdBrowseSY_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SY_COVER_LETTER_REPORT_NBR'
    end
    object cdBrowseNOTE: TStringField
      FieldName = 'NOTE'
      Size = 512
    end
    object cdBrowseUP_LEVEL_MAIL_BOX_NBR: TIntegerField
      FieldName = 'UP_LEVEL_MAIL_BOX_NBR'
    end
    object cdBrowseTRACKING_INFO: TStringField
      FieldName = 'TRACKING_INFO'
      Size = 40
    end
    object cdBrowseAUTO_RELEASE_TYPE: TStringField
      FieldName = 'AUTO_RELEASE_TYPE'
      FixedChar = True
      Size = 1
    end
    object cdBrowseBARCODE: TStringField
      FieldName = 'BARCODE'
      Size = 40
    end
    object cdBrowseCREATED_TIME: TDateTimeField
      FieldName = 'CREATED_TIME'
    end
    object cdBrowseDISPOSE_CONTENT_AFTER_SHIPPING: TStringField
      FieldName = 'DISPOSE_CONTENT_AFTER_SHIPPING'
      FixedChar = True
      Size = 1
    end
    object cdBrowseADDRESSEE: TStringField
      FieldName = 'ADDRESSEE'
      Size = 512
    end
    object cdBrowseDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdBrowseSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
    end
    object cdBrowseSB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SB_MEDIA_TYPE_NBR'
    end
    object cdBrowseluMethodName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMethodName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luMethodName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluMediaName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMediaName'
      LookupDataSet = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
      LookupKeyFields = 'SB_MEDIA_TYPE_NBR'
      LookupResultField = 'luMediaName'
      KeyFields = 'SB_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluServiceName: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luServiceName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluClNbr: TStringField
      FieldKind = fkLookup
      FieldName = 'luClNbr'
      LookupDataSet = cdTmpCl
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluClName: TStringField
      FieldKind = fkLookup
      FieldName = 'luClName'
      LookupDataSet = cdTmpCl
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
  end
  object cdTmpCl: TevClientDataSet
    ProviderName = 'TMP_CL_PROV'
    ClientID = -1
    Left = 224
    Top = 32
    object cdTmpClCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Origin = '"TMP_CL"."CL_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdTmpClCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
      Origin = '"TMP_CL"."CUSTOM_CLIENT_NUMBER"'
      Required = True
    end
    object cdTmpClNAME: TStringField
      FieldName = 'NAME'
      Origin = '"TMP_CL"."NAME"'
      Required = True
      Size = 40
    end
  end
end
