// Copyright c 2000-2004 iSystems LLC. All rights reserved.

//******************************************************************************
//
// File Name      : SPD_EDIT_SB_VMR_UTILS.pas
// Creation Date  : 02 Febuary 2007
// Projects       : Evolution
// Created By     : Travis Stafford
//
// Descritpion:
//   This is a screen that facilitates running utilities to perform some actions
//   against VMR. Example:  Cean Up Mail Boxes!
//
//******************************************************************************

unit SPD_EDIT_SB_VMR_UTILS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_SB_PAPER_INFO, SDDClasses, EvLegacy, EvContext,
  SDataStructure, DB, Wwdatsrc, ISBasicClasses,  StdCtrls,
  Wwdotdot, Wwdbcomb, Mask, wwdbedit, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid,
  SFrameEntry, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictbureau, SDataDicttemp,
  Buttons,sPickupSheetsXMLData,XMLDoc,XMLIntf, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_SB_VMR_UTILS = class(TFrameEntry)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evplClientList: TevPanel;
    DM_TEMPORARY: TDM_TEMPORARY;
    wwdbgridSelectClient: TevDBGrid;
    evbtClean: TevBitBtn;
    evbtPrintedToScaned: TevBitBtn;
    evDeleteMailBox: TevBitBtn;
    btnUpdatePassword: TevBitBtn;
    btnRefreshPrinters: TevBitBtn;
  
    procedure evbtCleanClick(Sender: TObject);
    procedure evbtPrintedToScanedClick(Sender: TObject);
    procedure evDeleteMailBoxClick(Sender: TObject);
    procedure btnUpdatePasswordClick(Sender: TObject);
    procedure btnRefreshPrintersClick(Sender: TObject);
  private
    { Private declarations }
    procedure DoPurge (aForceDelete : Boolean);
    procedure PrintedToScanned;
    procedure CleanUpPrinterQueue;
  public
    { Public declarations }
    procedure Activate; override;
  end;

var
  EDIT_SB_VMR_UTILS: TEDIT_SB_VMR_UTILS;

implementation

uses EvUtils,EvBasicUtils, EvTypes, EvCommonInterfaces, EvDataSet, EvConsts, strUtils;

{$R *.dfm}

procedure TEDIT_SB_VMR_UTILS.Activate;
begin
  //Make sure the list of clients is availabe for the user to choose from.
  ctx_DataAccess.OpenDataSets([DM_TEMPORARY.TMP_CL]);
  inherited;

  evDeleteMailBox.Enabled := ( ctx_AccountRights.Functions.GetState('USER_CAN_ACCESS_DELETEVMRMAILBOX') = stEnabled );
end;

//******************************************************************************
//  Method Name  : evbtCleanClick
//  Created By   : Travis Stafford
//
//  Parameters   : Sender - TObject
//
//  Description:
//     Loop through the list of selected clients on the screen and run a cleanup
//     on them.  This is the Onclick event of the button evbtClean.
//
//******************************************************************************
procedure TEDIT_SB_VMR_UTILS.evbtCleanClick(Sender: TObject);
begin
  DoPurge(False);
end;

//Shouldn't use GenericUpdateWithCondition
//it is against Evo DB CRUD rule
//So it causes ADR crushed
//with TExecDSWrapper.Create(';GenericUpdateWithCondition') do
procedure TEDIT_SB_VMR_UTILS.PrintedToScanned;
var
  qBoxes: IevQuery;
  cdUpdate: TevClientDataSet;
begin
  ctx_StartWait('Change the status of Printed VMR jobs to Picked Up for all the clients');

  qBoxes := TevQuery.Create('VmrSbTopMailBoxes');
  qBoxes.Macros.AddValue('CONDITION', ' and s.SCANNED_TIME is null and s.PRINTED_TIME is not null');
  qBoxes.Execute;

  cdUpdate := TevClientDataSet.Create(nil);
  cdUpdate.ProviderName := 'SB_CUSTOM_PROV';
  cdUpdate.CustomProviderTableName := 'SB_MAIL_BOX';
  cdUpdate.ClientID := -1;

  try
    while not qBoxes.Result.EOF do
    begin
        with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
        begin
          SetMacro('Columns', '*');
          SetMacro('TABLENAME', 'SB_MAIL_BOX');
          SetMacro('CONDITION', 'SB_MAIL_BOX_NBR=:Nbr');
          SetParam('Nbr', qBoxes.result['SB_MAIL_BOX_NBR']);
          cdUpdate.DataRequest(AsVariant);
        end;

        cdUpdate.Close;
        cdUpdate.Open;
        if not cdUpdate.IsEmpty then
        begin
          cdUpdate.Edit;
          cdUpdate['SCANNED_TIME'] := now;
          cdUpdate['SHIPPED_TIME'] := now;
          cdUpdate.post;
        end;

        ctx_DataAccess.PostDataSets([cdUpdate]);
      ctx_VMRRemote.RemoveScannedBarcodes(qBoxes.Result['BARCODE']);
      ctx_VMRRemote.RemoveFromBoxes('P'+ qBoxes.Result['BARCODE']);
      ctx_VMRRemote.RemoveFromBoxes(qBoxes.Result['BARCODE']+'*');
      qBoxes.Result.Next;
    end;

  finally
    cdUpdate.Close;
    cdUpdate.Free;
    ctx_EndWait;
  end;
end;


procedure TEDIT_SB_VMR_UTILS.evbtPrintedToScanedClick(Sender: TObject);
begin
  inherited;
  if EvMessage('Using this utility will take all VMR jobs that are in the Printer Pickup queue ' + #10#13 +
            'and change their status to Picked Up.  Are you sure you want to do this?', mtConfirmation, mbOKCancel, mbOk) = mrOK then
  begin
    PrintedToScanned;
    CleanUpPrinterQueue;
  end;
end;

procedure TEDIT_SB_VMR_UTILS.CleanUpPrinterQueue;
var
  i:integer;
  P,s :string;
  pq:IXMLPrinterQueueType;
  XMLPickupData:IXMLPickupDataType;
  vb:IXMLVMRBoxesType;
  PickupDoc:IXMLDocument;
begin
  inherited;
  PickupDoc := NewXMLDocument;
  PickupDoc.LoadFromStream(ctx_VMRRemote.RetrievePickupSheetsXMLData.RealStream);
  XMLPickupData := GetPickupData(PickupDoc);

  pq := XMLPickupData.PrinterQueue;
  vb := XMLPickupData.VMRBoxes;
  try
      for i := 0 to pq.Count-1 do
      begin
        s := pq.Printer[i].Data;
        while s<>'' do
        begin
           P := Fetch(s,';');
           //SbNbr := Copy(P, 1, Length(P)-4);
           ctx_VMRRemote.RemoveScannedBarcode(P);
        end;
      end;
      for i := 0 to vb.Count-1 do
      begin
        s := vb.BOX[i].Data;
        while s<>'' do
        begin
           P := Fetch(s,';');
           ctx_VMRRemote.RemoveFromBoxes(P);
        end;
      end;

  finally

  end;
end;

procedure TEDIT_SB_VMR_UTILS.DoPurge (aForceDelete : Boolean);

  procedure RunPurge;
  var i: Integer;
  begin
    ctx_StartWait('It might take awhile to be finished');
    try
      for i:=0 to wwdbgridSelectClient.SelectedList.Count-1 do
      begin
        DM_TEMPORARY.TMP_CL.GotoBookmark(wwdbgridSelectClient.SelectedList.Items[i]);
        ctx_VmrRemote.PurgeClientMailBox(DM_TEMPORARY.TMP_CL['CL_NBR'], aForceDelete);
        if aForceDelete then ctx_VmrRemote.DeleteDirectory(DM_TEMPORARY.TMP_CL['CL_NBR']);
      end;
      if wwdbgridSelectClient.SelectedList.Count>1 then
        EvMessage(Format('Finished! %d mailboxes cleaned. You need to rebuild VMR History before creating Archive CDs',[wwdbgridSelectClient.SelectedList.Count]))
      else
        EvMessage(Format('Finished! Mailbox cleaned. You need to rebuild VMR History before creating Archive CDs',[wwdbgridSelectClient.SelectedList.Count]));
    finally
      ctx_EndWait;
    end;
  end;
begin

  if wwdbgridSelectClient.SelectedList.Count = 0 then
  begin
    EvMessage('Please select client first');
    exit;
  end;

  if aForceDelete then
  begin
     if EvMessage('Are you sure to delete all mailboxes and files in the Selected Clients?', mtConfirmation, [mbYes, mbNo]) = mrYes
     then RunPurge
  end
  else begin
     if EvMessage('This function will delete links to any files missing from the VMR folder. Continue?', mtConfirmation, [mbYes, mbNo]) = mrYes
     then RunPurge;
  end;
end;


procedure TEDIT_SB_VMR_UTILS.evDeleteMailBoxClick(Sender: TObject);
begin
  inherited;
  DoPurge(True);
end;

// We used to have View_Password and Print_password for rwa, excel, txt and PDF
// Now we use only one password regardless Print or View, April 2016
// we decided that View_password will be deleted all
procedure TEDIT_SB_VMR_UTILS.btnUpdatePasswordClick(Sender: TObject);

  procedure DoProcess (aTableName: string);
  var
    s, tag: string;
    qBoxes : IevQuery;
    cdUpdate: TevClientDataSet;
  begin
    s:= ' Select #_nbr, option_tag, OPTION_STRING_VALUE VIEWPASSWORD '+
        ' from #_OPTION where option_tag like ''_M%VIEW_PASSWORD'' and  OPTION_STRING_VALUE <> '''' ';

    qBoxes := TevQuery.Create(StringReplace(s,'#',aTableName, [rfReplaceall]));
    qBoxes.Execute;
    qBoxes.Result.First;

    cdUpdate := TevClientDataSet.Create(nil);
    cdUpdate.CustomProviderTableName := aTableName+ '_OPTION';
    if aTableName = 'CL_MAIL_BOX_GROUP' then
      cdUpdate.ProviderName := 'CL_CUSTOM_PROV'
    else
      cdUpdate.ProviderName := 'SB_CUSTOM_PROV';

    try
      while not qBoxes.Result.EOF do
      begin
          cdUpdate.Close;
          tag := qBoxes.result['OPTION_TAG'];
          tag := AnsiReplacestr(tag, 'VIEW_PASSWORD','PRINT_PASSWORD');
          // Update Print_password
          with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
          begin
            SetMacro('Columns', '*');
            SetMacro('TABLENAME', aTableName + '_OPTION');
            SetMacro('CONDITION', aTableName+'_NBR=:Nbr and option_tag like '''+ tag +'''');
            SetParam('Nbr', qBoxes.result[aTableName+'_NBR']);
            cdUpdate.DataRequest(AsVariant);
          end;

          cdUpdate.Open;
          if ( not cdUpdate.IsEmpty) then //Print_password exists
          begin
             if (cdUpdate.FieldByName('OPTION_STRING_VALUE').asstring = '') then //Print_password
             begin
              cdUpdate.Edit;
              cdUpdate['OPTION_STRING_VALUE'] := qBoxes.result['VIEWPASSWORD'];
              cdUpdate.post;
             end
          end
          else //Print_password not exists
          begin
            cdUpdate.append;
            cdUpdate[aTableName+'_NBR'] := qBoxes.result[aTableName+'_NBR'];
            cdUpdate['OPTION_TAG'] := tag;
            cdUpdate['OPTION_STRING_VALUE'] := qBoxes.result['VIEWPASSWORD'];
            cdUpdate.post;
          end;
        ctx_DataAccess.PostDataSets([cdUpdate]);
        qBoxes.Result.Next;
      end;

  (*    cdUpdate.Close;
      //Delete VIEW_password
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('Columns', '*');
        SetMacro('TABLENAME', aTableName+'_OPTION');
        SetMacro('CONDITION', 'option_tag like ''_M%VIEW_PASSWORD'' ');
        cdUpdate.DataRequest(AsVariant);
      end;
      cdUpdate.Open;
      While not cdUpdate.Eof do
        cdUpdate.Delete;
      ctx_DataAccess.PostDataSets([cdUpdate]);
   *)
    finally
      cdUpdate.Close;
      cdUpdate.Free;
    end;
  end;

begin
  if EvMessage('If print password is empty, the print password will be replaced with view password.',
               mtConfirmation, mbOKCancel, mbOk) <> mrOK then exit;

  ctx_StartWait('processing...');
  wwdbgridSelectClient.DataSource.DataSet.DisableControls;
  try
    DM_TEMPORARY.TMP_CL.First;
    while not DM_TEMPORARY.TMP_CL.Eof do
    begin
      if ConvertNull(DM_TEMPORARY.TMP_CL.FieldValues['CL_NBR'],0) > 0 then
      begin
        ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CL.FieldValues['CL_NBR']);
        DoProcess('CL_MAIL_BOX_GROUP');
      end;
      DM_TEMPORARY.TMP_CL.Next;
    end;

    ctx_DataAccess.OpenClient(0);
    DoProcess('SB_MAIL_BOX');
  finally
    DM_TEMPORARY.TMP_CL.First;
    wwdbgridSelectClient.DataSource.DataSet.EnableControls;
    ctx_EndWait;
  end;
end;


procedure TEDIT_SB_VMR_UTILS.btnRefreshPrintersClick(Sender: TObject);
begin
  inherited;

  if not CheckRemoteVMRActive then
    ctx_VMRRemote.GetPrinterList('NON$AA$');

end;

initialization
  RegisterClass(TEDIT_SB_VMR_UTILS);

end.
