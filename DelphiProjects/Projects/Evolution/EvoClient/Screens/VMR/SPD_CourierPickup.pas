// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_CourierPickup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SDataStructure, DB, Wwdatsrc,
   StdCtrls, wwdblook, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, SFrameEntry, DBCtrls, kbmMemTable, EvContext,
  ISKbmMemDataSet, ISBasicClasses, SDDClasses, Buttons,
  ISDataAccessComponents, EvDataAccessComponents, SDataDicttemp,
  SDataDictbureau, sReportSettings, EvExceptions, EvUIUtils, EvConsts, EvUIComponents, EvClientDataSet,
  isUIDBMemo, LMDCustomButton, LMDButton, isUILMDButton;

type
  TCourierPickup = class(TFrameEntry)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_TEMPORARY: TDM_TEMPORARY;
    gBrowse: TevDBGrid;
    evPanel1: TevPanel;
    EvDBMemo1: TEvDBMemo;
    cdBrowse: TevClientDataSet;
    cdBrowseSB_MAIL_BOX_NBR: TIntegerField;
    cdBrowseCL_NBR: TIntegerField;
    cdBrowseCL_MAIL_BOX_GROUP_NBR: TIntegerField;
    cdBrowseCOST: TFloatField;
    cdBrowseREQUIRED: TStringField;
    cdBrowseNOTIFICATION_EMAIL: TStringField;
    cdBrowsePR_NBR: TIntegerField;
    cdBrowseRELEASED_TIME: TDateTimeField;
    cdBrowsePRINTED_TIME: TDateTimeField;
    cdBrowseSB_COVER_LETTER_REPORT_NBR: TIntegerField;
    cdBrowseSCANNED_TIME: TDateTimeField;
    cdBrowseSHIPPED_TIME: TDateTimeField;
    cdBrowseSY_COVER_LETTER_REPORT_NBR: TIntegerField;
    cdBrowseNOTE: TStringField;
    cdBrowseUP_LEVEL_MAIL_BOX_NBR: TIntegerField;
    cdBrowseTRACKING_INFO: TStringField;
    cdBrowseAUTO_RELEASE_TYPE: TStringField;
    cdBrowseBARCODE: TStringField;
    cdBrowseCREATED_TIME: TDateTimeField;
    cdBrowseDISPOSE_CONTENT_AFTER_SHIPPING: TStringField;
    cdBrowseADDRESSEE: TStringField;
    cdBrowseDESCRIPTION: TStringField;
    cdBrowseSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdBrowseSB_MEDIA_TYPE_NBR: TIntegerField;
    cdTmpCl: TevClientDataSet;
    cdTmpClCL_NBR: TIntegerField;
    cdTmpClCUSTOM_CLIENT_NUMBER: TStringField;
    cdTmpClNAME: TStringField;
    cdBrowseluClName: TStringField;
    cdBrowseluClNbr: TStringField;
    cdBrowseluServiceName: TStringField;
    cdBrowseluMediaName: TStringField;
    cdBrowseluMethodName: TStringField;
    bMark: TevBitBtn;
    bLocate: TevBitBtn;
    procedure bMarkClick(Sender: TObject);
    procedure bLocateClick(Sender: TObject);
    procedure gBrowseDblClick(Sender: TObject);
  private
    { Private declarations }
    sSmtpHost, sFromAddress: string;
    sUser:string;
    sPass:string;
  public
    { Public declarations }
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.dfm}

uses EvUtils, EvTypes, EvBasicUtils, SVmrClasses;

{ TCourierPickup }

procedure TCourierPickup.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  NavigationDataSet := cdBrowse;
  AddDS(cdBrowse, EditDataSets);
  AddDS([DM_SERVICE_BUREAU.SB_DELIVERY_METHOD, DM_SERVICE_BUREAU.SB_OPTION], SupportDataSets);
  if not cdBrowse.Active then
    cdBrowse.CreateDataSet;
end;

procedure TCourierPickup.bMarkClick(Sender: TObject);
type
  TVmrRealDelivertClass = class of TVmrRealDelivery;
var
  i: Integer;
  b: TBookmark;
  sMessage,Email: string;
  dmc: TVmrRealDelivertClass;
begin
  inherited;
  if gBrowse.SelectedList.Count = 0 then
    raise EInvalidParameters.CreateHelp('You need to select at least one shipment first', IDH_InvalidParameters);
  TevClientDataSet(gBrowse.DataSource.DataSet).DisableControls;
  try
    try
      gBrowse.DataSource.DataSet.First;
      while not gBrowse.DataSource.DataSet.Eof do
      begin
        b := gBrowse.DataSource.DataSet.GetBookmark;
        try
          for i := 0 to gBrowse.SelectedList.Count-1 do
            if gBrowse.DataSource.DataSet.CompareBookmarks(gBrowse.SelectedList[i], b) = 0 then
            begin
              Assert(DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Locate('SB_DELIVERY_METHOD_NBR',
                gBrowse.DataSource.DataSet['SB_DELIVERY_METHOD_NBR'], []));
              dmc := TVmrRealDelivertClass(FindVmrClass(DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.FieldByName('CLASS_NAME').AsString));
              Assert(Assigned(dmc));
              dmc.OnShipmentPacked(TevClientDataSet(gBrowse.DataSource.DataSet));
              if dmc.OnPickup(TevClientDataSet(gBrowse.DataSource.DataSet)) then
              try
                gBrowse.DataSource.DataSet.Edit;
                gBrowse.DataSource.DataSet['SHIPPED_TIME'] := SysTime;
                gBrowse.DataSource.DataSet.Post;
                Email := '';
                if not VarIsNull(gBrowse.DataSource.DataSet['NOTIFICATION_EMAIL']) then
                   Email := Trim(gBrowse.DataSource.DataSet['NOTIFICATION_EMAIL']);
                if Email <> '' then
                begin
                  sMessage := dmc.GetShippedMessage(TevClientDataSet(gBrowse.DataSource.DataSet));
                  dmc.SetupEmailerByFromAddress(sFromAddress);
                  try
                    ctx_EMailer.SendQuickMail(gBrowse.DataSource.DataSet['NOTIFICATION_EMAIL'], sFromAddress,
                      'Status update from your payroll service bureau', sMessage);
                  finally
                    ctx_EMailer.SmtpServer := '';
                    ctx_EMailer.SmtpPort := 0;
                    ctx_EMailer.UserName := '';
                    ctx_EMailer.Password := '';
                  end;
                end;
              except
                on E: Exception do
                  EvErrMessage('Could not send email notification, please check setup on Gereral Setup screen'#13+ E.Message);
              end;
              Break;
            end;
        finally
          gBrowse.DataSource.DataSet.FreeBookmark(b);
        end;
        gBrowse.DataSource.DataSet.Next;
      end;
      ctx_DataAccess.PostDataSets([TevClientDataSet(gBrowse.DataSource.DataSet)]);
      gBrowse.UnselectAll;
      with TevClientDataSet(gBrowse.DataSource.DataSet) do
      begin
        First;
        while not Eof do
        begin
          if FieldByName('SHIPPED_TIME').IsNull then
            Next
          else
            Delete;
        end;
        MergeChangeLog;
        First;
      end;
    except
      ctx_DataAccess.CancelDataSets([TevClientDataSet(gBrowse.DataSource.DataSet)]);
      raise;
    end;
  finally
    TevClientDataSet(gBrowse.DataSource.DataSet).EnableControls;
  end;
end;

procedure TCourierPickup.bLocateClick(Sender: TObject);
var
  s: string;
begin
  inherited;
  if EvDialog('Type in or scan the barcode label', 'Barcode:', s) then
    if not gBrowse.DataSource.DataSet.Locate('BARCODE', s, [])
    and not gBrowse.DataSource.DataSet.Locate('BARCODE', Copy(s, 1, Length(s)-4), []) then
      EvMessage('Barcode '+ s+ ' is not found')
    else
    begin
      gBrowse.UnselectAll;
      gBrowse.SelectRecord;
      bMark.Click;
    end;
end;

procedure TCourierPickup.Activate;
begin
  inherited;
  cdBrowse.CheckDSCondition('SCANNED_TIME is not NULL and SHIPPED_TIME is NULL and UP_LEVEL_MAIL_BOX_NBR is NULL');
  ctx_DataAccess.OpenDataSets([cdBrowse]);
  sSmtpHost := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', SmtpHostTag, 'OPTION_STRING_VALUE'), '');
  sFromAddress := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', VmrSmtpFromTag, 'OPTION_STRING_VALUE'), '');
  sUser := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', VmrSmtpUserTag, 'OPTION_STRING_VALUE'), '');
  sPass := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', VmrSmtpPassTag, 'OPTION_STRING_VALUE'), '');
  if (sSmtpHost = '') or (sFromAddress = '') then
    EvErrMessage('Email notification setup is blank. Please update it on General Setup screen');
end;

procedure TCourierPickup.gBrowseDblClick(Sender: TObject);
 var
   Pa: TReportParamArrayType;
   P: TrwReportParams;
   R: TrwReportResults;
begin
  inherited;
  if cdBrowse.FieldByName('SB_MAIL_BOX_NBR').AsInteger = 0 then
    exit;
  SetLength(Pa, 3);
  Pa[0].ParamName := 'SbMailBoxNbr';
  Pa[0].ParamValue := cdBrowse.FieldByName('SB_MAIL_BOX_NBR').AsInteger;
  Pa[1].ParamName := 'Clients';
  Pa[1].ParamValue := VarArrayOf([]);
  Pa[2].ParamName := 'Companies';
  Pa[2].ParamValue := VarArrayOf([]);

  ctx_RWLocalEngine.StartGroup;
  P := TrwReportParams.Create;
  P.Clear;
  P.LoadFromParamArray(Pa);
  ctx_RWLocalEngine.CalcPrintReport(762,'S',P,'Package content');
  R := TrwReportResults.Create(ctx_RWLocalEngine.EndGroup(rdtNone));
  ctx_RWLocalEngine.Preview(R);
end;

initialization
  RegisterClass(TCourierPickup);

end.

