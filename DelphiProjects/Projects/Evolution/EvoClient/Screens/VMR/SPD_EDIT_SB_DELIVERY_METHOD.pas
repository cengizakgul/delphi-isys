// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_DELIVERY_METHOD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_OPTION_BASE, SDataStructure, DB, Wwdatsrc,
   StdCtrls, wwdblook, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, SVmrClasses, SVmrOptionEditFrame, SDDClasses,
  ISBasicClasses, SDataDictsystem, SDataDictbureau, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SB_DELIVERY_METHOD = class(TEDIT_OPTION_BASE)
    lMethodName: TLabel;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    function CreateOptionedObject(const c: TVmrOptionedTypeClass): TVmrOptionedType; override;
    function CreateOptionEditFrame(const o: TVmrOptionedType): TVmrOptionEditFrame; override;
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

uses SFrameEntry;

{$R *.dfm}

{ TEDIT_SB_DELIVERY_METHOD }

function TEDIT_SB_DELIVERY_METHOD.CreateOptionEditFrame(
  const o: TVmrOptionedType): TVmrOptionEditFrame;
begin
  if o.GetOptionEditFrameClass <> nil then
    Result := o.GetOptionEditFrameClass.CreateBound(Self, o.Options, o, wwdsDetail)
  else
    Result := nil;
end;

function TEDIT_SB_DELIVERY_METHOD.CreateOptionedObject(
  const c: TVmrOptionedTypeClass): TVmrOptionedType;
begin
  Result := c.CreateSbSetup(wwdsDetail.DataSet['SB_DELIVERY_METHOD_NBR']);
end;

function TEDIT_SB_DELIVERY_METHOD.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_DELIVERY_METHOD;
end;

procedure TEDIT_SB_DELIVERY_METHOD.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS([DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE, DM_SYSTEM_MISC.SY_DELIVERY_SERVICE,
    DM_SYSTEM_MISC.SY_DELIVERY_METHOD], SupportDataSets);
end;

procedure TEDIT_SB_DELIVERY_METHOD.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if not Assigned(Field) then
    lMethodName.Caption := wwdsDetail.DataSet.FieldByName('luMethodName').AsString;
end;

initialization
  RegisterClass(TEDIT_SB_DELIVERY_METHOD);

end.
