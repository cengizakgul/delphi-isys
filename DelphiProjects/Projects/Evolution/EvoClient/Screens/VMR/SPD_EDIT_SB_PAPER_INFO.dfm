inherited EDIT_SB_PAPER_INFO: TEDIT_SB_PAPER_INFO
  object evDBGrid1: TevDBGrid [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 146
    DisableThemesInTitle = False
    ControlType.Strings = (
      'MEDIA_TYPE;CustomEdit;evDBComboBox2;F')
    Selected.Strings = (
      'DESCRIPTION'#9'40'#9'Description'#9'F'
      'MEDIA_TYPE'#9'10'#9'Media'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_SB_PAPER_INFO\evDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsDetail
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 146
    Width = 435
    Height = 119
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object evLabel1: TevLabel
      Left = 16
      Top = 8
      Width = 62
      Height = 16
      Caption = '~Description'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel2: TevLabel
      Left = 320
      Top = 8
      Width = 64
      Height = 16
      Caption = '~Forms Type'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel3: TevLabel
      Left = 16
      Top = 56
      Width = 40
      Height = 16
      Caption = '~Height'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object evLabel4: TevLabel
      Left = 168
      Top = 56
      Width = 37
      Height = 16
      Caption = '~Width'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object evLabel5: TevLabel
      Left = 320
      Top = 56
      Width = 157
      Height = 16
      Caption = '~Weight (lb for 500 sheets pack)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evDBEdit1: TevDBEdit
      Left = 16
      Top = 24
      Width = 249
      Height = 21
      DataField = 'DESCRIPTION'
      DataSource = wwdsDetail
      Picture.PictureMaskFromDataSet = False
      Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
    object evDBComboBox1: TevDBComboBox
      Left = 320
      Top = 24
      Width = 201
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'MEDIA_TYPE'
      DataSource = wwdsDetail
      DropDownCount = 8
      ItemHeight = 0
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 1
      UnboundDataType = wwDefault
    end
    object evDBEdit2: TevDBEdit
      Left = 16
      Top = 72
      Width = 97
      Height = 21
      DataField = 'HEIGHT'
      DataSource = wwdsDetail
      Picture.PictureMaskFromDataSet = False
      Picture.PictureMask = 
        '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
        '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
        '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
      TabOrder = 2
      UnboundDataType = wwDefault
      Visible = False
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
    object evDBEdit3: TevDBEdit
      Left = 168
      Top = 72
      Width = 97
      Height = 21
      DataField = 'WIDTH'
      DataSource = wwdsDetail
      Picture.PictureMaskFromDataSet = False
      Picture.PictureMask = 
        '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
        '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
        '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
      TabOrder = 3
      UnboundDataType = wwDefault
      Visible = False
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
    object edWeight: TevDBEdit
      Left = 320
      Top = 72
      Width = 97
      Height = 21
      DataField = 'WEIGHT'
      DataSource = wwdsDetail
      Picture.PictureMaskFromDataSet = False
      Picture.PictureMask = 
        '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
        '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
        '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
      TabOrder = 4
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
      Glowing = False
    end
    object bOuncesToFractions: TevBitBtn
      Left = 430
      Top = 72
      Width = 110
      Height = 20
      Caption = 'Convert ounces'
      TabOrder = 5
      OnClick = bOuncesToFractionsClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD3333333DD
        DDDDDDDFFFFFFFDDDDDDDD3BBBBBBB3DDDDDDDF8888888FDDDDDDFBB3DDFBBB3
        DDDDDD88DDDD888FDDDDDFB3DDDDFBBB3DDDDD8DDDDDD888FDDDDDDDDDDDFBBB
        3DDDDDDDDDDDD888FDDDDDDDDDDD3BBB3DDDDDDDDDDDF888FDDDDD333333BBB3
        DDDDDDFFFFFF888FDDDDDFBBBBBBBB3DDDDDDD88888888FDDDDDDDDFBBB33DDD
        DDDDDDDD888FFDDDDDDDDDDDDFBBB33DDDDDDDDDDD888FFDDDDDDD33333FBBB3
        3DDDDDFFFFFF888FFDDDDFBBBBBBBBBBDDDDDD8888888888DDDDDDDFBBB33DDD
        DDDDDDDD888FFDDDDDDDDD3DDFBBB33DDDDDDDFDDD888FFDDDDDDFB3333FBBB3
        3DDDDD8FFFFF888FFDDDDFBBBBBBBBBBDDDDDD8888888888DDDD}
      NumGlyphs = 2
      Margin = 0
    end
  end
  object evDBComboBox2: TevDBComboBox [2]
    Left = 384
    Top = 80
    Width = 121
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DataField = 'MEDIA_TYPE'
    DataSource = wwdsDetail
    DropDownCount = 8
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 2
    UnboundDataType = wwDefault
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_PAPER_INFO.SB_PAPER_INFO
    OnStateChange = wwdsDetailStateChange
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 344
    Top = 32
  end
end
