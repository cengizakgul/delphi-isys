inherited TrackingByAddresse: TTrackingByAddresse
  inherited PC: TevPageControl
    ActivePage = tshtDetails
    OnChange = PCChange
    inherited tshtBrowse: TTabSheet
      object evSplitter1: TevSplitter [0]
        Left = 0
        Top = 117
        Width = 427
        Height = 3
        Cursor = crVSplit
        Align = alBottom
      end
      inherited gBrowse: TevDBGrid
        Top = 31
        Width = 427
        Height = 86
        Selected.Strings = (
          'ADDRESSEE'#9'30'#9'Addressee'#9'F'
          'DESCRIPTION'#9'20'#9'Description'#9'F'
          'luClNbr'#9'8'#9'Cl#'#9'F'
          'luClName'#9'20'#9'Client'#9'F'
          'calcStatus'#9'25'#9'Status'#9'F'
          'luServiceName'#9'10'#9'Service'#9'F'
          'luMethodName'#9'10'#9'Method'#9'F'
          'luMediaName'#9'10'#9'Media'#9'F'
          'luPrCheckDate'#9'10'#9'Payroll'#9'F'
          'luPrRunNumber'#9'2'#9'Pr. Run#'#9'F'
          'CREATED_TIME'#9'18'#9'Created'#9'F'
          'RELEASED_TIME'#9'18'#9'Released'#9'F'
          'SHIPPED_TIME'#9'18'#9'Shipped'#9'F'
          'TRACKING_INFO'#9'25'#9'Tracking Info'#9'F')
        IniAttributes.SectionName = 'TTrackingByAddresse\gBrowse'
        Align = alClient
        DataSource = wwdsList
        TabOrder = 1
        OnCalcCellColors = gBrowseCalcCellColors
      end
      object gShipments: TevDBGrid
        Left = 0
        Top = 120
        Width = 427
        Height = 117
        DisableThemesInTitle = False
        Selected.Strings = (
          'DESCRIPTION'#9'25'#9'Description'#9'F'
          'ADDRESSEE'#9'40'#9'Addressee'#9'F'
          'luServiceName'#9'15'#9'Service'#9'F'
          'luMethodName'#9'15'#9'Method'#9'F'
          'luMediaName'#9'15'#9'Media'#9'F'
          'PAGE_COUNT'#9'5'#9'Pages'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TTrackingByAddresse\gShipments'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alBottom
        DataSource = wwdsMaster
        TabOrder = 2
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = gShipmentsDblClick
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object evPanel1: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 31
        Align = alTop
        TabOrder = 0
        object evLabel1: TevLabel
          Left = 8
          Top = 8
          Width = 143
          Height = 13
          Caption = 'Words to search in addresses:'
        end
        object edSearchWords: TevEdit
          Left = 160
          Top = 4
          Width = 417
          Height = 21
          TabOrder = 0
          OnKeyPress = edSearchWordsKeyPress
        end
        object bSearch: TevBitBtn
          Left = 589
          Top = 4
          Width = 75
          Height = 25
          Caption = 'Search'
          TabOrder = 1
          OnClick = bSearchClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD8888888888
            8888DD88888888DFFD88DD8FF7FFFF744FF8DD8DD8DDDDF88DD8DD877877784C
            C778DD88DD88DF888D88DD8F78FF74CCFFF8DDDDFFDDF888DDD8DD8844884CC8
            7778DDFF88FF888D8888D844CC44CCF7FFF8DF88888888D8DDD8D4CC78CC4778
            7778D888DD888D8888888CFFF7FFC8F7FFF8D8DDD8DD8FD8DDD84C777877C478
            777888D8888D88D88888C8FFF7FFFCF7FFF88DDDD8DDD8D8DDD8C87778777C78
            77788FD8888DF8D88888DCFFF7FFCFF7FFF8D8DDDDDD8DD8DDD8DC888888C888
            8888D8FFDDFF8D888888DDCC78CC77787778DD88FF88DDD8DDD8DD87CC777778
            7778DDDD88DDDDD8DDD8DD88888888888888DD8DDDD888888888}
          NumGlyphs = 2
          Margin = 0
        end
      end
    end
    inherited tshtDetails: TTabSheet
      object evSplitter2: TevSplitter
        Left = 0
        Top = 121
        Width = 427
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object pPreViewPanel: TevPanel
        Left = 0
        Top = 124
        Width = 427
        Height = 113
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Double click on the content grid to see preview'
        TabOrder = 0
      end
      object evDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 427
        Height = 121
        DisableThemesInTitle = False
        Selected.Strings = (
          'JOB_DESCR'#9'25'#9'Job Description'#9'F'
          'REP_DESCR'#9'25'#9'Report Description'#9'F'
          'FILE_NAME'#9'45'#9'File Name'#9'F'
          'MEDIA_TYPE'#9'2'#9'Type'#9'F'
          'PAGE_COUNT'#9'5'#9'Pages'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TTrackingByAddresse\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alTop
        DataSource = dsContent
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = evDBGrid1DblClick
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = cdShipments
    OnDataChange = wwdsMasterDataChange
    Left = 416
    Top = 66
  end
  inherited wwdsDetail: TevDataSource
    Left = 470
    Top = 66
  end
  inherited wwdsList: TevDataSource
    DataSet = cdMaster
    OnDataChange = wwdsListDataChange
    Left = 362
    Top = 66
  end
  object cdJobs: TevClientDataSet
    OnCalcFields = cdJobsCalcFields
    Left = 344
    Top = 80
    object cdJobsSB_MAIL_BOX_NBR: TIntegerField
      FieldName = 'SB_MAIL_BOX_NBR'
    end
    object cdJobsCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdJobsCL_MAIL_BOX_GROUP_NBR: TIntegerField
      FieldName = 'CL_MAIL_BOX_GROUP_NBR'
    end
    object cdJobsCOST: TFloatField
      FieldName = 'COST'
    end
    object cdJobsREQUIRED: TStringField
      FieldName = 'REQUIRED'
      FixedChar = True
      Size = 1
    end
    object cdJobsNOTIFICATION_EMAIL: TStringField
      FieldName = 'NOTIFICATION_EMAIL'
      Size = 80
    end
    object cdJobsPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdJobsRELEASED_TIME: TDateTimeField
      FieldName = 'RELEASED_TIME'
    end
    object cdJobsPRINTED_TIME: TDateTimeField
      FieldName = 'PRINTED_TIME'
    end
    object cdJobsSB_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SB_COVER_LETTER_REPORT_NBR'
    end
    object cdJobsSCANNED_TIME: TDateTimeField
      FieldName = 'SCANNED_TIME'
    end
    object cdJobsSHIPPED_TIME: TDateTimeField
      FieldName = 'SHIPPED_TIME'
    end
    object cdJobsSY_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SY_COVER_LETTER_REPORT_NBR'
    end
    object cdJobsNOTE: TStringField
      FieldName = 'NOTE'
      Size = 512
    end
    object cdJobsUP_LEVEL_MAIL_BOX_NBR: TIntegerField
      FieldName = 'UP_LEVEL_MAIL_BOX_NBR'
    end
    object cdJobsTRACKING_INFO: TStringField
      FieldName = 'TRACKING_INFO'
      Size = 40
    end
    object cdJobsAUTO_RELEASE_TYPE: TStringField
      FieldName = 'AUTO_RELEASE_TYPE'
      FixedChar = True
      Size = 1
    end
    object cdJobsBARCODE: TStringField
      FieldName = 'BARCODE'
      Size = 40
    end
    object cdJobsCREATED_TIME: TDateTimeField
      FieldName = 'CREATED_TIME'
    end
    object cdJobsDISPOSE_CONTENT_AFTER_SHIPPING: TStringField
      FieldName = 'DISPOSE_CONTENT_AFTER_SHIPPING'
      FixedChar = True
      Size = 1
    end
    object cdJobsADDRESSEE: TStringField
      FieldName = 'ADDRESSEE'
      Size = 512
    end
    object cdJobsDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdJobsSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
    end
    object cdJobsSB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SB_MEDIA_TYPE_NBR'
    end
    object cdJobsluMethodName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMethodName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luMethodName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdJobsluMediaName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMediaName'
      LookupDataSet = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
      LookupKeyFields = 'SB_MEDIA_TYPE_NBR'
      LookupResultField = 'luMediaName'
      KeyFields = 'SB_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
    object cdJobsluServiceName: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luServiceName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdJobsluClNbr: TStringField
      FieldKind = fkLookup
      FieldName = 'luClNbr'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdJobsluClName: TStringField
      FieldKind = fkLookup
      FieldName = 'luClName'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdJobsluPrCheckDate: TDateField
      FieldKind = fkLookup
      FieldName = 'luPrCheckDate'
      LookupDataSet = DM_TMP_PR.TMP_PR
      LookupKeyFields = 'CL_NBR;PR_NBR'
      LookupResultField = 'CHECK_DATE'
      KeyFields = 'CL_NBR;JOB_PR_NBR'
      Lookup = True
    end
    object cdJobsluPrRunNumber: TIntegerField
      FieldKind = fkLookup
      FieldName = 'luPrRunNumber'
      LookupDataSet = DM_TMP_PR.TMP_PR
      LookupKeyFields = 'CL_NBR;PR_NBR'
      LookupResultField = 'RUN_NUMBER'
      KeyFields = 'CL_NBR;JOB_PR_NBR'
      Lookup = True
    end
    object cdJobsluCoNbr: TStringField
      FieldKind = fkLookup
      FieldName = 'luCoNbr'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;JOB_CO_NBR'
      Lookup = True
    end
    object cdJobsluCoName: TStringField
      FieldKind = fkLookup
      FieldName = 'luCoName'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR;JOB_CO_NBR'
      Size = 40
      Lookup = True
    end
    object cdJobsDESCR: TStringField
      FieldName = 'JOB_DESCR'
      Size = 40
    end
    object cdJobsCO_NBR: TIntegerField
      FieldName = 'JOB_CO_NBR'
    end
    object cdJobsJOB_PR_NBR: TIntegerField
      FieldName = 'JOB_PR_NBR'
    end
    object cdJobscalcStatus: TStringField
      FieldKind = fkCalculated
      FieldName = 'calcStatus'
      Size = 40
      Calculated = True
    end
    object cdJobscalcColor: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'calcColor'
      Calculated = True
    end
  end
  object cdShipments: TevClientDataSet
    ProviderName = 'SB_MAIL_BOX_PROV'
    ClientID = -1
    Left = 400
    Top = 80
    object cdShipmentsSB_MAIL_BOX_NBR: TIntegerField
      FieldName = 'SB_MAIL_BOX_NBR'
    end
    object cdShipmentsCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdShipmentsPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdShipmentsADDRESSEE: TStringField
      FieldName = 'ADDRESSEE'
      Size = 512
    end
    object cdShipmentsDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdShipmentsSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
    end
    object cdShipmentsSB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SB_MEDIA_TYPE_NBR'
    end
    object cdShipmentsluServiceName: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luServiceName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdShipmentsluMediaName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMediaName'
      LookupDataSet = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
      LookupKeyFields = 'SB_MEDIA_TYPE_NBR'
      LookupResultField = 'luMediaName'
      KeyFields = 'SB_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
    object cdShipmentsluMethodName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMethodName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luMethodName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdShipmentspage_count: TIntegerField
      FieldName = 'page_count'
    end
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 256
    Top = 8
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 288
    Top = 8
  end
  object cdContent: TevClientDataSet
    Left = 256
    Top = 48
  end
  object dsContent: TevDataSource
    DataSet = cdContent
    OnDataChange = dsContentDataChange
    Left = 288
    Top = 48
  end
  object cdMaster: TevClientDataSet
    ProviderName = 'SB_MAIL_BOX_PROV'
    OnCalcFields = cdJobsCalcFields
    ClientID = -1
    Left = 312
    Top = 80
    object cdMasterSB_MAIL_BOX_NBR: TIntegerField
      FieldName = 'SB_MAIL_BOX_NBR'
      Origin = '"SB_MAIL_BOX"."SB_MAIL_BOX_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdMasterCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Origin = '"SB_MAIL_BOX"."CL_NBR"'
    end
    object cdMasterPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Origin = '"SB_MAIL_BOX"."PR_NBR"'
    end
    object cdMasterRELEASED_TIME: TDateTimeField
      FieldName = 'RELEASED_TIME'
      Origin = '"SB_MAIL_BOX"."RELEASED_TIME"'
    end
    object cdMasterNOTE: TStringField
      FieldName = 'NOTE'
      Origin = '"SB_MAIL_BOX"."NOTE"'
      Size = 512
    end
    object cdMasterAUTO_RELEASE_TYPE: TStringField
      FieldName = 'AUTO_RELEASE_TYPE'
      Origin = '"SB_MAIL_BOX"."AUTO_RELEASE_TYPE"'
      Required = True
      FixedChar = True
      Size = 1
    end
    object cdMasterCREATED_TIME: TDateTimeField
      FieldName = 'CREATED_TIME'
      Origin = '"SB_MAIL_BOX"."CREATED_TIME"'
      Required = True
    end
    object cdMasterADDRESSEE: TStringField
      FieldName = 'ADDRESSEE'
      Origin = '"SB_MAIL_BOX"."ADDRESSEE"'
      Required = True
      Size = 512
    end
    object cdMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = '"SB_MAIL_BOX"."DESCRIPTION"'
      Required = True
      Size = 40
    end
    object cdMasterSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
      Origin = '"SB_MAIL_BOX"."SB_DELIVERY_METHOD_NBR"'
      Required = True
    end
    object cdMasterSB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SB_MEDIA_TYPE_NBR'
      Origin = '"SB_MAIL_BOX"."SB_MEDIA_TYPE_NBR"'
      Required = True
    end
    object cdMasterPRINTED_TIME: TDateTimeField
      FieldName = 'PRINTED_TIME'
    end
    object cdMasterSHIPPED_TIME: TDateTimeField
      FieldName = 'SHIPPED_TIME'
    end
    object cdMasterTRACKING_INFO: TStringField
      FieldName = 'TRACKING_INFO'
      Size = 40
    end
    object cdMasterSCANNED_TIME: TDateTimeField
      FieldName = 'SCANNED_TIME'
    end
    object cdMastercalcColor: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'calcColor'
      Calculated = True
    end
    object cdMastercalcStatus: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'calcStatus'
      Size = 80
    end
    object cdMasterluPrRunNumber: TIntegerField
      FieldKind = fkLookup
      FieldName = 'luPrRunNumber'
      LookupDataSet = DM_TMP_PR.TMP_PR
      LookupKeyFields = 'CL_NBR;PR_NBR'
      LookupResultField = 'RUN_NUMBER'
      KeyFields = 'CL_NBR;PR_NBR'
      Lookup = True
    end
    object cdMasterluPrCheckDate: TDateField
      FieldKind = fkLookup
      FieldName = 'luPrCheckDate'
      LookupDataSet = DM_TMP_PR.TMP_PR
      LookupKeyFields = 'CL_NBR;PR_NBR'
      LookupResultField = 'CHECK_DATE'
      KeyFields = 'CL_NBR;PR_NBR'
      Lookup = True
    end
    object cdMasterluClName: TStringField
      FieldKind = fkLookup
      FieldName = 'luClName'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdMasterluClNbr: TStringField
      FieldKind = fkLookup
      FieldName = 'luClNbr'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdMasterluServiceName: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luServiceName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdMasterluMediaName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMediaName'
      LookupDataSet = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
      LookupKeyFields = 'SB_MEDIA_TYPE_NBR'
      LookupResultField = 'luMediaName'
      KeyFields = 'SB_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
    object cdMasterluMethodName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMethodName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luMethodName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
  end
end
