// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ReprintPayrollsToVmr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB,   EvUtils, EvCommonInterfaces, EvMainboard, EvConsts,
  Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, StdCtrls, ComCtrls, ExtCtrls, SFrameEntry,
  Buttons, ISBasicClasses, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TReprintPayrollsToVmr = class(TFrameEntry)
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    edBegDate: TevDateTimePicker;
    evLabel2: TevLabel;
    edEndDate: TevDateTimePicker;
    evLabel3: TevLabel;
    gCompanies: TevDBCheckGrid;
    bGo: TevBitBtn;
    procedure bGo_Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.dfm}

uses SDataStructure;

{ TReprintPayrollsToVmr }

procedure TReprintPayrollsToVmr.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_TEMPORARY.TMP_CO, SupportDataSets);
  NavigationDataSet := DM_TEMPORARY.TMP_CO;
  wwdsDetail.DataSet := NavigationDataSet;
end;

procedure TReprintPayrollsToVmr.bGo_Click(Sender: TObject);
var
  i: Integer;
  t: IevRebuildVMRHistoryTask;
  b: TBookmarkStr;
  s: String;
begin
  inherited;
  if gCompanies.SelectedList.Count > 0 then
  begin
    t := mb_TaskQueue.CreateTask(QUEUE_TASK_REBUILD_VMR_HIST, True) as IevRebuildVMRHistoryTask;
    t.PeriodBegDate := edBegDate.Date;
    t.PeriodEndDate := edEndDate.Date;

    b := DM_TEMPORARY.TMP_CO.Bookmark;
    DM_TEMPORARY.TMP_CO.DisableControls;
    try
      for i := 0 to gCompanies.SelectedList.Count-1 do
      begin
        DM_TEMPORARY.TMP_CO.GotoBookmark(gCompanies.SelectedList[i]);
        s := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsString + ',' +
             DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsString;
        t.CoList.Add(s);
      end;
    finally
      DM_TEMPORARY.TMP_CO.Bookmark := b;
      DM_TEMPORARY.TMP_CO.EnableControls;
    end;

    ctx_EvolutionGUI.AddTaskQueue(t);

    gCompanies.SelectedList.Clear;
  end
  else
    EvMessage('You need to select companies first');
end;

initialization
  RegisterClass(TReprintPayrollsToVmr);

end.
