// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_CourierBillEntry;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SDataStructure, DB, Wwdatsrc,
   StdCtrls, wwdblook, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, SFrameEntry, DBCtrls, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDDClasses, ISUtils, ISDataAccessComponents,
  EvDataAccessComponents, SDataDicttemp, SDataDictbureau, EvExceptions,
  EvContext, EvUIComponents, EvClientDataSet, isUIEdit, LMDCustomButton,
  LMDButton, isUILMDButton, isUIwwDBLookupCombo;

type
  TCourierBillEntry = class(TFrameEntry)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_TEMPORARY: TDM_TEMPORARY;
    gBrowse: TevDBGrid;
    cdBrowse: TevClientDataSet;
    cdBrowseSB_MAIL_BOX_NBR: TIntegerField;
    cdBrowseCL_NBR: TIntegerField;
    cdBrowseCL_MAIL_BOX_GROUP_NBR: TIntegerField;
    cdBrowseCOST: TFloatField;
    cdBrowseREQUIRED: TStringField;
    cdBrowseNOTIFICATION_EMAIL: TStringField;
    cdBrowsePR_NBR: TIntegerField;
    cdBrowseRELEASED_TIME: TDateTimeField;
    cdBrowsePRINTED_TIME: TDateTimeField;
    cdBrowseSB_COVER_LETTER_REPORT_NBR: TIntegerField;
    cdBrowseSCANNED_TIME: TDateTimeField;
    cdBrowseSHIPPED_TIME: TDateTimeField;
    cdBrowseSY_COVER_LETTER_REPORT_NBR: TIntegerField;
    cdBrowseNOTE: TStringField;
    cdBrowseUP_LEVEL_MAIL_BOX_NBR: TIntegerField;
    cdBrowseTRACKING_INFO: TStringField;
    cdBrowseAUTO_RELEASE_TYPE: TStringField;
    cdBrowseBARCODE: TStringField;
    cdBrowseCREATED_TIME: TDateTimeField;
    cdBrowseDISPOSE_CONTENT_AFTER_SHIPPING: TStringField;
    cdBrowseADDRESSEE: TStringField;
    cdBrowseDESCRIPTION: TStringField;
    cdBrowseSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdBrowseSB_MEDIA_TYPE_NBR: TIntegerField;
    cdTmpCl: TevClientDataSet;
    cdTmpClCL_NBR: TIntegerField;
    cdTmpClCUSTOM_CLIENT_NUMBER: TStringField;
    cdTmpClNAME: TStringField;
    cdBrowseluClName: TStringField;
    cdBrowseluClNbr: TStringField;
    cdBrowseluServiceName: TStringField;
    cdBrowseluMediaName: TStringField;
    cdBrowseluMethodName: TStringField;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    edDeliveryMethod: TevDBLookupCombo;
    evLabel2: TevLabel;
    edPeriodBegin: TevDateTimePicker;
    edPeriodEnd: TevDateTimePicker;
    evLabel3: TevLabel;
    bLoad: TevButton;
    evLabel4: TevLabel;
    edBillTotal: TevEdit;
    procedure gBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure bLoadClick(Sender: TObject);
    procedure EnableLoadButton(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Activate; override;
    procedure Deactivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.dfm}

uses EvUtils, EvTypes, EvBasicUtils, SPackageEntry;

{ TCourierPickup }

procedure TCourierBillEntry.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  NavigationDataSet := cdBrowse;
  AddDS(cdBrowse, EditDataSets);
  cdBrowse.CheckDSCondition('SB_MAIL_BOX_NBR=0');
  gBrowse.Visible := False;
  AddDS([DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE, DM_SERVICE_BUREAU.SB_DELIVERY_METHOD], SupportDataSets);
end;

procedure TCourierBillEntry.gBrowseCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  if Field.FieldName <> 'COST' then
    AFont.Color := ShadeColor(AFont.Color, 1);
end;

procedure TCourierBillEntry.bLoadClick(Sender: TObject);
var
  s: string;
begin
  inherited;
  if (edDeliveryMethod.LookupValue = '') or (edDeliveryMethod.DisplayValue = '') then
    raise EInvalidParameters.CreateHelp('You need to select delivery service first', IDH_InvalidParameters);
  //if StrToFloatDef(edBillTotal.Text, 0) = 0 then
  //  raise EInvalidParameters.CreateHelp('You need to enter bill total first', IDH_InvalidParameters);
  s := '';
  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.First;
  while not DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Eof do
  begin
    if DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['luServiceName'] = edDeliveryMethod.DisplayValue then
      if s = '' then
        s := VarToStr(DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['SB_DELIVERY_METHOD_NBR'])
      else
        s := s+ ','+ VarToStr(DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['SB_DELIVERY_METHOD_NBR']);
    DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Next;
  end;
  cdBrowse.Close;
  cdBrowse.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
  with TExecDSWrapper.Create('SB_MAIL_BOX;GenericSelectCurrentWithCondition') do
  begin
    SetMacro('TABLENAME', 'SB_MAIL_BOX');
    SetMacro('COLUMNS', '*');
    //SetMacro('CONDITION', '2=2');
    SetMacro('CONDITION', 'SHIPPED_TIME is not NULL and '+
      'SHIPPED_TIME >= :BegDate and SHIPPED_TIME < :EndDate and '+
      'SB_DELIVERY_METHOD_NBR in ('+ s+ ') and UP_LEVEL_MAIL_BOX_NBR is NULL');
      //'SB_DELIVERY_METHOD_NBR in ('+ s+ ') and COST is NULL and UP_LEVEL_MAIL_BOX_NBR is NULL');
    SetParam('BegDate', edPeriodBegin.Date);
    SetParam('EndDate', edPeriodEnd.Date+ 1);
    cdBrowse.DataRequest(AsVariant);
  end;
  ctx_DataAccess.OpenDataSets([cdBrowse]);
  bLoad.Enabled := False;
  gBrowse.Visible := True;
end;

procedure TCourierBillEntry.EnableLoadButton(Sender: TObject);
begin
  inherited;
  bLoad.Enabled := True;
  cdBrowse.Close;
  gBrowse.Visible := False;
end;

procedure TCourierBillEntry.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
  procedure CheckTotal;
  var
    cd: TevClientDataSet;
    t: Currency;
  begin
    t := 0;
    cd := TevClientDataSet.Create(nil);
    try
      cd.CloneCursor(cdBrowse, True);
      cd.First;
      while not cd.Eof do
      begin
        t := t+ cd.FieldByName('COST').AsCurrency;
        cd.Next;
      end;
    finally
      cd.Free;
    end;
    if t <> StrToFloat(edBillTotal.Text) then
      raise EInvalidParameters.CreateFmtHelp('Total of entered shipping costs (%m) is not matching bill total you provided', [t], IDH_InvalidParameters);
  end;
begin
  inherited;
  if Kind = navCommit then
  begin
    CheckTotal;
    ctx_DataAccess.PostDataSets([cdBrowse]);
  end
  else
  if Kind = navOk then
    Handled := True;
end;

procedure TCourierBillEntry.Activate;
begin
  inherited;
  ctx_DataAccess.TempCommitDisable := True;
end;

procedure TCourierBillEntry.Deactivate;
begin
  inherited;
  ctx_DataAccess.TempCommitDisable := False;
end;

initialization
  RegisterClass(TCourierBillEntry);

end.
