inherited ReprintPayrollsToVmr: TReprintPayrollsToVmr
  object evLabel2: TevLabel [0]
    Left = 8
    Top = 8
    Width = 89
    Height = 13
    Caption = 'Period Begin Date:'
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 0
    Width = 443
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object evLabel1: TevLabel
      Left = 8
      Top = 8
      Width = 89
      Height = 13
      Caption = 'Period Begin Date:'
    end
    object evLabel3: TevLabel
      Left = 8
      Top = 32
      Width = 81
      Height = 13
      Caption = 'Period End Date:'
    end
    object edBegDate: TevDateTimePicker
      Left = 128
      Top = 8
      Width = 89
      Height = 21
      Date = 38020.000000000000000000
      Time = 38020.000000000000000000
      TabOrder = 0
    end
    object bGo: TevBitBtn
      Left = 232
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Go'
      TabOrder = 1
      OnClick = bGo_Click
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D4444444444F
        F8DDD888888888888FDD4CCCCCCCCCF44F8D8DDDDDDDDD8DD8FDFFFFFFFFFFFC
        CF8D88888888888DD8FDDD74444444444F8DDD8DDDDDDDDDD8FDDDFCCCCCCCCC
        CF8DDD8DDDDDDDDDD8FDDDFCCCCCCCCCCF8DDD8DDDDDDDDDD8FDDDFCCCCCCCCC
        CF8DDD8DFFFFFDDDD8FDDDFCFFFFFCCCCF8DDD8D88888DDDD8FDDDFC44444CCC
        CF8DDD8DDDFFFFDDD8FDDDFCCCFFFFCCCF8DDD8DDD8888DDD8FDDDFCCC4444CC
        CF8DDD8DDDDDFFFDD8FDDDFCCCCCFFFCCF8DDD8DDDDD888DD8FDDDFCCCCC444C
        CF88DD8DDDDDDDDDD8FFDDFCCCCCCCCCCF7FDD8DDDDDDDDDD888DDFCCCCCCCCC
        CFCFDD8DDDDDDDDDD8D8DDD74444444444FDDDD888888888888D}
      NumGlyphs = 2
    end
  end
  object edEndDate: TevDateTimePicker [2]
    Left = 128
    Top = 32
    Width = 89
    Height = 21
    Date = 38020.000000000000000000
    Time = 38020.000000000000000000
    TabOrder = 1
  end
  object gCompanies: TevDBCheckGrid [3]
    Left = 0
    Top = 65
    Width = 443
    Height = 212
    DisableThemesInTitle = False
    Selected.Strings = (
      'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom Company #'
      'NAME'#9'40'#9'Name')
    IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
    IniAttributes.SectionName = 'TReprintPayrollsToVmr\gCompanies'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsDetail
    MultiSelectOptions = [msoShiftSelect]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    ReadOnly = True
    TabOrder = 2
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
end
