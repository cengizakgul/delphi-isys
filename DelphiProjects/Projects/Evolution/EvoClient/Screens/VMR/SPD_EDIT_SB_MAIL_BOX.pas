// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_MAIL_BOX;

interface

uses
  SDataStructure,  wwdbedit, Wwdotdot,
  Wwdbcomb, wwdbdatetimepicker, Mask, DBCtrls, StdCtrls, wwdblook,
  ExtCtrls, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls,
  Classes, SFrameEntry, SVmrClasses, Graphics, SVmrOptionEditFrame, EvStreamUtils,
   SPD_EDIT_CL_BASE, Buttons, SPD_EDIT_BROWSE_DETAIL_BASE,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvLegacy,
  EvDataAccessComponents, ISDataAccessComponents, Forms, EvContext,
  rwPreviewContainerFrm, SDataDicttemp, SDataDictsystem, SDataDictbureau,
  EvMainboard,StrUtils,ISErrorUtils, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBLookupCombo, isUIwwDBComboBox,
  isUIwwDBEdit, LMDCustomButton, LMDButton, isUILMDButton, isUIDBMemo;

type
  TEDIT_SB_MAIL_BOX = class(TEDIT_BROWSE_DETAIL_BASE)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    cdTypes: TevClientDataSet;  
    cdTypesSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdTypesCLASS_NAME: TStringField;
    cdTypesNAME: TStringField;
    tsDetails: TTabSheet;
    tshtDetails1: TTabSheet;
    cdBrowse: TevClientDataSet;
    cdBrowseSB_MAIL_BOX_NBR: TIntegerField;
    cdBrowseCL_NBR: TIntegerField;
    cdBrowseCL_MAIL_BOX_GROUP_NBR: TIntegerField;
    cdBrowseCOST: TFloatField;
    cdBrowseREQUIRED: TStringField;
    cdBrowseNOTIFICATION_EMAIL: TStringField;
    cdBrowsePR_NBR: TIntegerField;
    cdBrowseRELEASED_TIME: TDateTimeField;
    cdBrowsePRINTED_TIME: TDateTimeField;
    cdBrowseSB_COVER_LETTER_REPORT_NBR: TIntegerField;
    cdBrowseSCANNED_TIME: TDateTimeField;
    cdBrowseSHIPPED_TIME: TDateTimeField;
    cdBrowseSY_COVER_LETTER_REPORT_NBR: TIntegerField;
    cdBrowseNOTE: TStringField;
    cdBrowseUP_LEVEL_MAIL_BOX_NBR: TIntegerField;
    cdBrowseTRACKING_INFO: TStringField;
    cdBrowseAUTO_RELEASE_TYPE: TStringField;
    cdBrowseBARCODE: TStringField;
    cdBrowseCREATED_TIME: TDateTimeField;
    cdBrowseDISPOSE_CONTENT_AFTER_SHIPPING: TStringField;
    cdBrowseADDRESSEE: TStringField;
    cdBrowseDESCRIPTION: TStringField;
    cdBrowseSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdBrowseSB_MEDIA_TYPE_NBR: TIntegerField;
    cdBrowseluMethodName: TStringField;
    cdBrowseluMediaName: TStringField;
    cdBrowseluServiceName: TStringField;
    cdBrowseluClNbr: TStringField;
    cdBrowseluClName: TStringField;
    cdBrowseluPrCheckDate: TDateField;
    cdBrowseluPrRunNumber: TIntegerField;
    DM_TEMPORARY: TDM_TEMPORARY;
    cdMaster: TevClientDataSet;
    cdBrowseUP_NBR: TIntegerField;
    cdMasterSB_MAIL_BOX_NBR: TIntegerField;
    cdMasterCL_NBR: TIntegerField;
    cdMasterPR_NBR: TIntegerField;
    cdMasterRELEASED_TIME: TDateTimeField;
    cdMasterNOTE: TStringField;
    cdMasterAUTO_RELEASE_TYPE: TStringField;
    cdMasterCREATED_TIME: TDateTimeField;
    cdMasterADDRESSEE: TStringField;
    cdMasterDESCRIPTION: TStringField;
    cdMasterSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdMasterSB_MEDIA_TYPE_NBR: TIntegerField;
    cdMasterluMethodName: TStringField;
    cdMasterluMediaName: TStringField;
    cdMasterluServiceName: TStringField;
    cdMasterluClNbr: TStringField;
    cdMasterluClName: TStringField;
    cdMasterluPrCheckDate: TDateField;
    cdMasterluPrRunNumber: TIntegerField;
    cdMastercalcStatus: TStringField;
    cdMastercalcColor: TIntegerField;
    cdMasterEMPTY_REQUIRED: TIntegerField;
    cdMasterPAGE_COUNT: TIntegerField;
    Panel5: TevPanel;
    dsContent: TevDataSource;
    cdContent: TevClientDataSet;
    cdMasterPRINTED_TIME: TDateTimeField;
    cdMasterSCANNED_TIME: TDateTimeField;
    cdMasterSHIPPED_TIME: TDateTimeField;
    cdMasterTRACKING_INFO: TStringField;
    cdMasterBARCODE: TStringField;
    cdMastercalcCleanUp: TStringField;
    Splitter1: TSplitter;
    Panel1: TPanel;
    EvDBMemo2: TEvDBMemo;
    PC2: TevPageControl;
    tsUnreleased: TTabSheet;
    evButton1: TevButton;
    tsPrinted: TTabSheet;
    bRevert: TevButton;
    tsScanned: TTabSheet;
    tsCleanup: TTabSheet;
    evbtCleanMailBox: TevButton;
    tsEmailing: TTabSheet;
    cbBlockVouchers: TCheckBox;
    Label3: TLabel;
    dtFrom: TDateTimePicker;
    Label4: TLabel;
    dtTo: TDateTimePicker;
    evButton4: TevButton;
    evButton2: TevButton;
    Panel2: TPanel;
    Splitter2: TSplitter;
    cdMasterD_EMAILED: TStringField;
    cdMasterBlockVouchers: TStringField;
    lbVoucherBlocked: TevLabel;
    PC3: TPageControl;
    tsDetailDetail: TTabSheet;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evLabel9: TevLabel;
    evLabel11: TevLabel;
    evLabel10: TevLabel;
    evLabel2: TevLabel;
    evLabel4: TevLabel;
    evLabel12: TevLabel;
    evLabel13: TevLabel;
    lLinkedClient: TevLabel;
    lLinkedPayroll: TevLabel;
    evLabel14: TevLabel;
    evDBEdit1: TevDBEdit;
    evDBComboBox1: TevDBComboBox;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBEdit2: TevDBEdit;
    edSyCoverReport: TevDBLookupCombo;
    edSbCoverReport: TevDBLookupCombo;
    EvDBMemo1: TEvDBMemo;
    edUpLinkMailBoxGroup: TevDBLookupCombo;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBDateTimePicker2: TevDBDateTimePicker;
    cbVmrPrinterLocation: TevComboBox;
    tsContent: TTabSheet;
    TabSheet3: TTabSheet;
    evDBTreeView1: TevDBTreeView;
    Panel3: TPanel;
    evSplitter2: TevSplitter;
    evDBGrid1: TevDBGrid;
    pPreViewPanel: TPanel;
    pcPrimarySettings: TevPageControl;
    tsPriService: TTabSheet;
    tsPriMedia: TTabSheet;
    tsPriSbType: TTabSheet;
    tsPriSbMedia: TTabSheet;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    evLabel3: TevLabel;
    edType: TevDBLookupCombo;
    edMedia: TevDBLookupCombo;
    cbIncludeDescInEmails: TevCheckBox;
    tsReleased: TTabSheet;
    bRevert2: TevButton;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure edTypeChange(Sender: TObject);
    procedure edMediaBeforeDropDown(Sender: TObject);
    procedure edMediaCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure cdMasterAfterScroll(DataSet: TDataSet);
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
    procedure cdMasterCalcFields(DataSet: TDataSet);
    procedure gBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure evDBGrid1DblClick(Sender: TObject);
    procedure dsContentDataChange(Sender: TObject; Field: TField);
    procedure PCChange(Sender: TObject);
    procedure evButton1Click(Sender: TObject);
    procedure PC2Change(Sender: TObject);
    procedure bRevertClick(Sender: TObject);
    procedure PreviewContainerResize(Sender: TObject);
    procedure evbtCleanMailBoxClick(Sender: TObject);
    procedure AdditionalOptionChange(Sender: TObject);
    procedure cbBlockVouchersClick(Sender: TObject);
    procedure cbVmrPrinterLocationKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FKeys: array [1..2] of Integer;
    FDeliveryMethodClass: TVmrDeliveryMethodClass;
    FPrimaryDeliveryMethod: TVmrOptionedType;
    FPrimaryDeliveryMethodFrame: TVmrOptionEditFrame;
    FPrimaryDeliveryMedia: TVmrOptionedType;
    FPrimaryDeliveryMediaFrame: TVmrOptionEditFrame;
    FSbPrimaryDeliveryMethodFrame: TVmrOptionEditFrame;
    FSbPrimaryDeliveryMediaFrame: TVmrOptionEditFrame;
    FPreviewContainer: TrwPreviewContainer;
    FTmpClUserFilter:string;
    FTmpClUserFiltered:boolean;
    FTmpClFiltered:boolean;
    FClientID : Integer;
    procedure LoadContent(const Force: Boolean = False);
    procedure LoadBoxes(const DoOpen: Boolean);
    procedure RebuildTreeView;
    procedure DoPurge(aForceDelete : Boolean);
  protected
    procedure Loaded; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses Variants, SPackageEntry, SysUtils, EvTypes, EvUtils, SFieldCodeValues, Dialogs,
  SPD_EDIT_Open_BASE, SVmrDeliveryServices, sReportSettings,
  EvCommonInterfaces, EvBasicUtils, EvConsts, EvDataSet,ISBasicUtils, isBaseClasses;

{$R *.DFM}

procedure TEDIT_SB_MAIL_BOX.Activate;
var
 aList : IisStringList;
begin
  evDBComboBox1.Items.Text := MailBoxAutoRelease_ComboChoices;
  cbBlockVouchers.Checked := mb_AppSettings['Settings\BlockSendVouchers'] = 'Y';
  edUpLinkMailBoxGroup.LookupTable := TevClientDataSet.Create(Self);
  cdTypes.CreateDataSet;
  cdTypes.LogChanges := False;
  dtFrom.Date := Now;
  dtTo.Date := Now;
  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.First;
  while not DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Eof do
  begin
    cdTypes.AppendRecord([DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['SB_DELIVERY_METHOD_NBR'],
      DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['CLASS_NAME'],
      DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['luServiceName']+ ' - '+
        DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['luMethodName']]);
    DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Next;
  end;
  cdTypes.IndexFieldNames := 'NAME';
  edType.LookupTable := CreateLookupDataset(cdTypes, Self);
  edMedia.LookupTable := CreateLookupDataset(DM_SERVICE_BUREAU.SB_MEDIA_TYPE, Self);
  edSyCoverReport.LookupTable := GetReportsByAncestor('TrwlBaseCoverLetterReport', 'S', Self);
  edSbCoverReport.LookupTable := GetReportsByAncestor('TrwlBaseCoverLetterReport', 'B', Self);
  inherited;
  pcPrimarySettings.ActivePageIndex := 0;
  wwdsDetail.Enabled := True;
//to get location lists from SB_option table
  if DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', VmrSbLocation, []) then
    cbVmrPrinterLocation.Items.CommaText := DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString
  else
    cbVmrPrinterLocation.Items.CommaText := '';

  if CheckRemoteVMRActive then
  begin
    aList := GetRemotePrinterList;
    if (cbVmrPrinterLocation.Items.Count > 0) and (aList.count > 0) then
      cbVmrPrinterLocation.Items.CommaText := cbVmrPrinterLocation.Items.CommaText +',' + aList.CommaText
    else if (aList.count > 0) then
      cbVmrPrinterLocation.Items.CommaText := aList.CommaText;
  end;

  LoadBoxes(True);

  //Content
  evDBGrid1.Selected.Text :=
                'JOB_DESCR'#9'25'#9'Job Description'#9'F'#13#10+
                'REP_DESCR'#9'25'#9'Report Description'#9'F'#13#10+
                'FILE_NAME'#9'45'#9'File Name'#9'F'#13#10+
                'MEDIA_TYPE'#9'1'#9'Type'#9'F'#13#10+
                'PAGE_COUNT'#9'5'#9'Pages'#9'F'#13#10+
                ifthen(CheckRemoteVMRActive, 'REMOTEPRINTDATE'#9'25'#9'Remote Print Date'#9'F', '');
  evDBGrid1.ApplySelected;

end;

function TEDIT_SB_MAIL_BOX.GetInsertControl: TWinControl;
begin
  Result := evDBEdit1;
end;

procedure TEDIT_SB_MAIL_BOX.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
  procedure Process(const o: TVmrOptionedType);
  begin
    if Assigned(o) and o.ExtraOptions.Active then
    begin
      o.ExtraOptions.Apply;
      TFramePackageTmpl(Owner).AddPostDataSet(o.ExtraOptions.OptionDataSet);
    end;
  end;
  procedure ProcessCancel(const o: TVmrOptionedType);
  begin
    if Assigned(o) and o.ExtraOptions.Active then
    begin
      if o.ExtraOptions.State in [dsEdit, dsInsert] then
        o.ExtraOptions.Cancel;
      o.ExtraOptions.CancelUpdates;
    end;
  end;
  Procedure ApplyOption(option:string;field:string;value:variant);
  begin
    if DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Locate('SB_MAIL_BOX_NBR;OPTION_TAG',
                                                  VarArrayOf([cdBrowse.FieldByName('SB_MAIL_BOX_NBR').Asstring,option]),[]) then
    begin
      DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Edit;
      if DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.FieldByName(field).Value = value then
        DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Cancel
      else
      begin
        DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.FieldByName(field).Value := value;
        DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Post;
      end;
    end;
  end;
begin
  if Kind = NavOK then
  begin
    Process(FPrimaryDeliveryMethod);
    Process(FPrimaryDeliveryMedia);
    cbVmrPrinterLocation.Tag := 1;
    cbIncludeDescInEmails.Tag := 1;
    ApplyOption(VmrSbLocation,'OPTION_STRING_VALUE',Trim(cbVmrPrinterLocation.Text));
    ApplyOption(VmrIncludeDescriptioninEmails,'OPTION_STRING_VALUE',BoolToYN(cbIncludeDescInEmails.Checked));
    cbVmrPrinterLocation.Tag := 0;
    cbIncludeDescInEmails.Tag := 0;
  end
  else
  if Kind = NavCancel then
  begin
    ProcessCancel(FPrimaryDeliveryMethod);
    ProcessCancel(FPrimaryDeliveryMedia);
  end;
end;

procedure TEDIT_SB_MAIL_BOX.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
  procedure lFreeAndNil(var Obj);
  begin
    SysUtils.FreeAndNil(Obj);
  end;
  procedure ProcessCreated(const o: TVmrOptionedType; var oCl: TVmrOptionedType;
    var fCl, fSb: TVmrOptionEditFrame;
    const fClParent, fSbParent: TTabSheet; const SbNbr: Variant);
  begin
    if Assigned(o) then
    begin
      oCl := o;
      if oCl.GetExtraOptionEditFrameClass <> nil then
      begin
        fCl := oCl.GetExtraOptionEditFrameClass.CreateBound(Self, oCl.ExtraOptions, oCl, wwdsDetail);
        fCl.Name := '';
        fCl.Align := alClient;
        fCl.Parent := fClParent;
      end;
      if oCl.GetOptionEditFrameClass <> nil then
      begin
        fSb := oCl.GetOptionEditFrameClass.CreateBound(Self, oCl.Options, oCl, nil);
        fSb.Name := '';
        fSb.Align := alClient;
        fSb.Parent := fSbParent;
        fSb.Enabled := False;
      end
    end;
  end;

  function GetOptionValue(option:string; field:string;DefValue:Variant):Variant;
  begin
    if DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Locate('SB_MAIL_BOX_NBR;OPTION_TAG',
                               VarArrayOf([cdBrowse['SB_MAIL_BOX_NBR'],option]),[])
       and not (DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.FieldByName(field).IsNull) then
       Result := DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.FieldByName(field).Value
    else
       result := DefValue;
  end;

  procedure GetCheckBoxOptionValue(c:TCheckBox;Option:string) ;
  begin
     if (c.Tag = 0) then
     begin
       c.Tag := 1;
       try
          c.Checked := GetOptionValue(Option,'OPTION_STRING_VALUE',GROUP_BOX_NO)=GROUP_BOX_YES;
       finally
         c.Tag := 0;
       end;
     end;
  end;

  procedure GetComboBoxOptionValue(c:TComboBox;Option:string);
  begin
     if (c.Tag = 0) then
     begin
       c.Tag := 1;
       try
          c.ItemIndex := c.Items.IndexOf(Trim(GetOptionValue(Option,'OPTION_STRING_VALUE','')));
       finally
         c.Tag := 0;
       end;
     end;
   end;

  var
   UpSbDeliveryMethodNbr:variant;
begin
  inherited;
  if (csLoading in ComponentState) or (wwdsDetail.Tag = 1) then
    exit;
  if wwdsDetail.Active and not Assigned(Field)
     and not VarIsNull(cdBrowse['SB_MAIL_BOX_NBR']) then
  begin
    DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.DataRequired(' SB_MAIL_BOX_NBR='+ cdBrowse.FieldByName('SB_MAIL_BOX_NBR').Asstring );
    GetComboBoxOptionValue(cbVmrPrinterLocation,VmrSbLocation);
    GetCheckBoxOptionValue(cbIncludeDescInEmails,VmrIncludeDescriptioninEmails);
  end;

  FDeliveryMethodClass := nil;
  if ((wwdsDetail.State = dsBrowse) and not Assigned(Field))
  or ((Assigned(Field) and  ((Field.FieldName = 'UP_LEVEL_MAIL_BOX_NBR')  or
   (Field.FieldName = 'SB_MEDIA_TYPE_NBR')  or
   (Field.FieldName = 'SB_DELIVERY_METHOD_NBR')
  ))) then
    if cdBrowse.FieldByName('UP_LEVEL_MAIL_BOX_NBR').IsNull then
    begin
      FDeliveryMethodClass := TVmrDeliveryMethodClass(FindVmrClass(VarToStr(DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.
        Lookup('SB_DELIVERY_METHOD_NBR', cdBrowse['SB_DELIVERY_METHOD_NBR'], 'CLASS_NAME'))));
      edType.ReadOnly := False;
      edType.RefreshDisplay;
    end
    else
    begin
      //UpSbDeliveryMethodNbr := ClonedLookup(cdBrowse,'SB_MAIL_BOX_NBR', cdBrowse['UP_NBR'], 'SB_DELIVERY_METHOD_NBR');
      UpSbDeliveryMethodNbr := cdBrowse.Lookup('SB_MAIL_BOX_NBR', cdBrowse['UP_NBR'], 'SB_DELIVERY_METHOD_NBR');
      FDeliveryMethodClass :=
          TVmrDeliveryMethodClass(
            FindVmrClass(
               VarToStr(
                  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Lookup('SB_DELIVERY_METHOD_NBR',UpSbDeliveryMethodNbr,'CLASS_NAME')
               )
            )
          );
      edType.ReadOnly := True;
      edType.Text := 'You can not change delivery method if the box is a part of another box';
    end;
  if not Assigned(Field) and (wwdsDetail.State = dsBrowse) then
  begin
    if cdBrowseCL_NBR.IsNull then
    begin
      lLinkedClient.Caption := 'None';
      lLinkedPayroll.Caption := 'None';
    end
    else
    begin
      lLinkedClient.Caption := cdBrowseluClNbr.AsString+ '- '+ cdBrowseluClName.AsString;
      lLinkedPayroll.Caption := cdBrowseluPrCheckDate.AsString+ '-'+ cdBrowseluPrRunNumber.AsString;
    end;
    LoadContent;
  end;
  if (not Assigned(Field)
  and (not Assigned(FPrimaryDeliveryMethod) or (Tag <> cdBrowse.Fields[0].AsInteger)
    or (FKeys[1] <> StrToIntDef(edType.LookupValue, 0)) or (FKeys[2] <> StrToIntDef(edMedia.LookupValue, 0)))
  {and (wwdsDetail.State = dsBrowse)})
  or (Assigned(Field)
    and ((Field.FieldName = edType.DataField) or (Field.FieldName = edMedia.DataField))
    ) then
  begin
    if not Assigned(Field) or (Field.FieldName = edType.DataField) then
    begin
      lFreeAndNil(FPrimaryDeliveryMethodFrame);
      lFreeAndNil(FSbPrimaryDeliveryMethodFrame);
      lFreeAndNil(FPrimaryDeliveryMethod);
    end;
    if not Assigned(Field) or (Field.FieldName = edType.DataField) or (Field.FieldName = edMedia.DataField) then
    begin
      lFreeAndNil(FPrimaryDeliveryMediaFrame);
      lFreeAndNil(FSbPrimaryDeliveryMediaFrame);
      lFreeAndNil(FPrimaryDeliveryMedia);
    end;
    FKeys[1] := StrToIntDef(edType.LookupValue, 0);
    FKeys[2] := StrToIntDef(edMedia.LookupValue, 0);
    Tag := cdBrowse.Fields[0].AsInteger;
    if not edType.ReadOnly then
      ProcessCreated(InstanceSbDeliveryMethod(TevClientDataSet(cdBrowse)), FPrimaryDeliveryMethod, FPrimaryDeliveryMethodFrame,
        FSbPrimaryDeliveryMethodFrame, tsPriService, tsPriSbType,
        edType.DataSource.DataSet[edType.DataField]);
    if Assigned(FDeliveryMethodClass) then
    begin
      if CheckIfListed(TClassArray(FDeliveryMethodClass.GetMediaClasses),
        FindVmrClass(ConvertNull(edMedia.LookupTable.Lookup(edMedia.LookupField,
        edMedia.DataSource.DataSet[edMedia.DataField], 'CLASS_NAME'), ''))) then
          ProcessCreated(InstanceSbMedia(TevClientDataSet(cdBrowse)), FPrimaryDeliveryMedia, FPrimaryDeliveryMediaFrame,
            FSbPrimaryDeliveryMediaFrame, tsPriMedia, tsPriSbMedia,
            edMedia.DataSource.DataSet[edMedia.DataField])
      else
      if not VarIsNull(edMedia.LookupTable.Lookup(edMedia.LookupField,
        edMedia.DataSource.DataSet[edMedia.DataField], 'CLASS_NAME'))
      and (edMedia.DataSource.State in [dsEdit, dsInsert]) then
      begin
        edMedia.DataSource.DataSet[edMedia.DataField] := Null;
        edMedia.Text := 'Choice is not valid. Choose again';
      end;
    end;


    pcPrimarySettings.SetTabVisible(tsPriSbType, Assigned(FSbPrimaryDeliveryMethodFrame));
    pcPrimarySettings.SetTabVisible(tsPriMedia, Assigned(FPrimaryDeliveryMediaFrame));
    pcPrimarySettings.SetTabVisible(tsPriSbMedia, Assigned(FSbPrimaryDeliveryMediaFrame));
    pcPrimarySettings.SetTabVisible(tsPriService, Assigned(FPrimaryDeliveryMethodFrame));

    pcPrimarySettings.ActivatePage(tsPriService);
  end;
  if (wwdsDetail.State in [dsEdit, dsInsert])
  and Assigned(Field)
  and (Field.FieldName = 'UP_LEVEL_MAIL_BOX_NBR')
  and not Field.IsNull then
    cdBrowse['SB_DELIVERY_METHOD_NBR'] := ClonedLookup(TevClientDataSet(cdBrowse),
      'SB_MAIL_BOX_NBR', cdBrowse['UP_NBR'], 'SB_DELIVERY_METHOD_NBR');
end;

procedure TEDIT_SB_MAIL_BOX.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  NavigationDataSet := cdMaster;
  AddDS(cdBrowse, EditDataSets);

  if not cdBrowse.Active then
    cdBrowse.CreateDataSet;
  if not cdMaster.Active then
    cdMaster.CreateDataSet;
  AddDS([DM_SERVICE_BUREAU.SB_OPTION, DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE, DM_SERVICE_BUREAU.SB_DELIVERY_METHOD,
    DM_SYSTEM_MISC.SY_DELIVERY_SERVICE, DM_SYSTEM_MISC.SY_DELIVERY_METHOD,DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION,
    DM_SERVICE_BUREAU.SB_MEDIA_TYPE, DM_SYSTEM_MISC.SY_MEDIA_TYPE, cdMaster], SupportDataSets);

  FTmpClUserFiltered := DM_TEMPORARY.TMP_CL.UserFiltered;
  FTmpClFiltered     := DM_TEMPORARY.TMP_CL.Filtered;
  FTmpClUserFilter := DM_TEMPORARY.TMP_CL.UserFilter;
  DM_TEMPORARY.TMP_CL.UserFiltered := false;
  DM_TEMPORARY.TMP_CL.Filtered  := false;

  DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.CheckDSCondition('');
  if not DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.Active then
    DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.CreateDataSet;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION, EditDataSets, '');

end;

procedure TEDIT_SB_MAIL_BOX.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if TwwDataSource(Sender).State = dsBrowse then
    TevClientDataSet(edUpLinkMailBoxGroup.LookupTable).Data := cdBrowse.Data;
end;

procedure TEDIT_SB_MAIL_BOX.edTypeChange(Sender: TObject);
begin
  inherited;
  ;
end;

procedure TEDIT_SB_MAIL_BOX.edMediaBeforeDropDown(Sender: TObject);
var
  a: TVmrMediaTypeClassArray;
  s: string;
  i: Integer;
begin
  inherited;
  SetLength(a, 0);
  if Assigned(FDeliveryMethodClass) then
  begin
    a := FDeliveryMethodClass.GetMediaClasses;
    for i := 0 to High(a) do
    begin
      if s <> '' then
        s := s+ ' or ';
      s := s+ '(CLASS_NAME='+ QuotedStr(a[i].ClassName)+ ')';
    end;
    edMedia.LookupTable.Filter := s;
    edMedia.LookupTable.Filtered := True;
  end
end;

procedure TEDIT_SB_MAIL_BOX.edMediaCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  edMedia.LookupTable.Filtered := False;
end;

procedure TEDIT_SB_MAIL_BOX.cdMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if PC.ActivePage = tshtDetails then
    PCChange(nil);
end;

procedure TEDIT_SB_MAIL_BOX.wwdsMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (csLoading in ComponentState) or (Field <> nil) then
    exit;

  cbBlockVouchers.Enabled :=  ConvertNull(cdMaster['BlockVouchers'],'N') = 'N' ;
  lbVoucherBlocked.Visible := ConvertNull(cdMaster['BlockVouchers'],'N') = 'Y' ;
end;

procedure TEDIT_SB_MAIL_BOX.cdMasterCalcFields(DataSet: TDataSet);
begin
  inherited;
  if VarIsNull(DataSet['RELEASED_TIME']) then
  begin
    if DataSet['EMPTY_REQUIRED'] > 0 then
    begin
      DataSet['calcStatus'] := 'Waiting for required parts';
      DataSet['calcColor'] := clOlive;
    end
    else
    begin
      DataSet['calcStatus'] := 'Ready. Waiting to be manually released';
      DataSet['calcColor'] := clYellow;
    end;
  end
  else
  if VarIsNull(DataSet['PRINTED_TIME']) then
  begin
    DataSet['calcStatus'] := 'Waiting to be printed';
    DataSet['calcColor'] := clOlive;
  end
  else
  if VarIsNull(DataSet['SCANNED_TIME']) then
  begin
    DataSet['calcStatus'] := 'Waiting for scan';
    DataSet['calcColor'] := clOlive;
  end
  else
  if VarIsNull(DataSet['SHIPPED_TIME']) then
  begin
    DataSet['calcStatus'] := 'Scanned/W. for pickup';
    DataSet['calcColor'] := clYellow;
  end
  else
  begin
    DataSet['calcStatus'] := 'Picked up';
    DataSet['calcColor'] := clLime;
  end;

  if VarIsNull(DataSet['calcCleanUp']) then
  begin
    DataSet['calcCleanUp'] := 'N';
  end;
end;

procedure TEDIT_SB_MAIL_BOX.gBrowseCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  if Field.FieldName = 'calcStatus' then
    ABrush.Color := TColor(VarToInt(Field.DataSet['calcColor']));
  if cdMaster.FindField('D_EMAILED') <> nil then
    if  VarIsNull(cdMaster['D_EMAILED']) and (PC2.ActivePage = tsEmailing ) then
       AFont.Style := AFont.Style + [fsBold];
end;

procedure TEDIT_SB_MAIL_BOX.evDBGrid1DblClick(Sender: TObject);
var
  FResult: TrwReportResults;
  AFile: IEvDualStream;
  dsOptionsToBeDeleted: TevClientDataSet;
begin
  inherited;
  // To check Client security
  if (FClientID <> 0 ) then
  begin
    ErrorIf(ctx_AccountRights.Clients.GetState(IntToStr(FClientID)) <> stEnabled,
      'Access to client denied (' + IntToStr(FClientID) + ')', ENoRightsToClient, IDH_SecurityViolation);
  end;
  FreeAndNil(FPreviewContainer);
  try
    AFile := ctx_VMRRemote.RetrieveFile(dsContent.DataSet['FILE_NAME']);
  except
    on E: Exception do
      if Pos('Cannot open file', E.Message) = 0 then
        raise
      else
      if EvMessage('This part of content has been deleted from storage and is not available anymore.'#13+
        'Do you want to remove it from the content list?', mtError, [mbYes, mbNo], mbYes) = mrYes then
      begin
        dsOptionsToBeDeleted := TevClientDataSet.Create(nil);
        try
          dsOptionsToBeDeleted.ProviderName := DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.ProviderName;
          dsOptionsToBeDeleted.ClientID := -1;
          dsOptionsToBeDeleted.DataRequired('SB_MAIL_BOX_CONTENT_NBR='+ IntToStr(dsContent.DataSet['SB_MAIL_BOX_CONTENT_NBR']));
          while dsOptionsToBeDeleted.RecordCount > 0 do
            dsOptionsToBeDeleted.Delete;
          dsContent.DataSet.Delete;
          TevClientDataSet(dsContent.DataSet).CustomProviderTableName := 'SB_MAIL_BOX_CONTENT';
          ctx_DataAccess.PostDataSets([dsOptionsToBeDeleted, TevClientDataSet(dsContent.DataSet)]);
        finally
          dsOptionsToBeDeleted.Free;
        end;
        Exit;
      end;
  end;
  FResult := TrwReportResults.Create;
  try
    FResult.AddReportResult(dsContent.DataSet['DESCRIPTION'], TReportType(dsContent.DataSet['REPORT_TYPE']),
      AFile).Layers := StrToLayers(dsContent.DataSet.FieldByName('LAYERS').asString);

    FPreviewContainer := TrwPreviewContainer.Create(Self);
    FPreviewContainer.Parent := pPreViewPanel;
    FPreviewContainer.Align := alClient;

    ctx_RWLocalEngine.Preview(FResult, FPreviewContainer, True);
  finally
    FResult.Free;
  end;
end;

procedure TEDIT_SB_MAIL_BOX.dsContentDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  FreeAndNil(FPreviewContainer);
end;

procedure TEDIT_SB_MAIL_BOX.PCChange(Sender: TObject);
begin
  inherited;
  LoadContent;
  RebuildTreeView;
  if (PC.ActivePage = tshtDetails) and (PC3.ActivePage = tsDetailDetail)then
  begin
    if not Assigned(edSyCoverReport.LookupTable) then
      edSyCoverReport.LookupTable := GetReportsByAncestor('TrwlBaseCoverLetterReport', 'S', Self);
    if not Assigned(edSbCoverReport.LookupTable) then
      edSbCoverReport.LookupTable := GetReportsByAncestor('TrwlBaseCoverLetterReport', 'B', Self);
  end;
end;

procedure TEDIT_SB_MAIL_BOX.LoadContent(const Force: Boolean = False);
var
  qry : IevQuery;
begin
  if PC.ActivePage <> tshtDetails then exit;
  ctx_StartWait('Retrieve data. Please, wait.');
  try

  if Force or (cdContent.Tag <> cdBrowseSB_MAIL_BOX_NBR.AsInteger) then
  begin
    if cdBrowseSB_MAIL_BOX_NBR.AsInteger > 0 then
    begin
      qry := TevQuery.create('select CL_NBR from SB_MAIL_BOX where {AsOfNow<SB_MAIL_BOX>} and SB_MAIL_BOX_NBR=' + cdBrowseSB_MAIL_BOX_NBR.AsString);
      qry.Execute;
      FClientID := qry.Result.FieldByName('CL_NBR').asInteger;
    end;
    cdContent.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
    cdContent.DisableControls;
    try
      cdContent.Close;
      if not cdBrowseSB_MAIL_BOX_NBR.IsNull then
      begin
        with TExecDSWrapper.Create('VmrSbMailBoxContents') do
        begin
          SetParam('BoxNbr', cdBrowseSB_MAIL_BOX_NBR.AsInteger);
          cdContent.DataRequest(AsVariant);
        end;
        ctx_DataAccess.OpenDataSets([cdContent]);
      end;
    finally
      cdContent.EnableControls;
    end;
    cdContent.Tag := cdBrowseSB_MAIL_BOX_NBR.AsInteger;
  end;
  DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT.CheckDSCondition('SB_MAIL_BOX_NBR='+ cdBrowseSB_MAIL_BOX_NBR.AsString);

  if (PC.ActivePage = tsDetails) and not cdBrowseSB_MAIL_BOX_NBR.IsNull then
    ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_MAIL_BOX_CONTENT]);
  finally
     ctx_EndWait;
  end;
end;

procedure TEDIT_SB_MAIL_BOX.evButton1Click(Sender: TObject);
var
  i: Integer;
  b: TBookmark;
  CleanUpNeeded: Boolean;
  RelType :TVmrReleaseTypeSet;
  UnpritedReports: TrwReportResults;
begin
  inherited;
  CleanUpNeeded := False;
  if ctx_VmrEngine = nil then
     raise Exception.CreateHelp('VMR engine is not loaded', IDH_InvalidParameters);
  if gBrowse.SelectedList.Count = 0 then
    raise EInvalidParameters.CreateHelp('You need to select at list one shipment first', IDH_InvalidParameters);
  gBrowse.DataSource.DataSet.DisableControls;
  try
    gBrowse.DataSource.DataSet.First;
    while not gBrowse.DataSource.DataSet.Eof do
    begin
      if (gBrowse.DataSource.DataSet.FieldByName('RELEASED_TIME').IsNull) or (PC2.ActivePage = tsEmailing) then
      begin
        b := gBrowse.DataSource.DataSet.GetBookmark;
        try
          for i := 0 to gBrowse.SelectedList.Count-1 do
            if gBrowse.DataSource.DataSet.CompareBookmarks(gBrowse.SelectedList[i], b) = 0 then
            begin
              ctx_StartWait('Processing '+
                gBrowse.DataSource.DataSet.FieldByName('luClName').AsString+
                ' '+ gBrowse.DataSource.DataSet.FieldByName('DESCRIPTION').AsString);
              try
                try
                  if PC2.ActivePage = tsEmailing then
                  begin
                      RelType := [vrtVouchers];
                  end else
                  begin
                     if (cbBlockVouchers.Checked) and (ConvertNull(cdMaster['BlockVouchers'],'N')='N') then
                        RelType := [vrtPrinter,vrtVouchers]
                     else
                        RelType := [vrtPrinter];
                  end;

                  ctx_VmrRemote.ProcessRelease(gBrowse.DataSource.DataSet['SB_MAIL_BOX_NBR'],RelType, UnpritedReports);
                  try
                    if Assigned(UnpritedReports) then
                      ctx_RWLocalEngine.Print(UnpritedReports);
                  finally
                    FreeAndNil(UnpritedReports);
                  end;

                  if (vrtPrinter in RelType) then
                     ctx_VmrEngine.ProcessRelease(gBrowse.DataSource.DataSet['SB_MAIL_BOX_NBR'],[vrtStoreASCIIFiles]);
                except
                  {on E:EPrinterNotDefined do
                  begin
                    if EvMessage(E.Message + '.' + #13#10 +
                         'You will be brought to the Bureau - VMR - Printer Info screen.', mtError, [mbOk, mbCancel], mbOk) = mrOk then
                    begin
                      ActivateFrameAs('Bureau - Virtual Mail Room - Printer Info', []);
                      //AbortEx;
                    end
                  end;}
                  on E: Exception do
                  begin
                    if Pos('Cannot open file', E.Message) > 0 then
                    begin
                      //On can't open file exception from the Release mail box function, set cleanup flag to Y.
                      CleanUpNeeded := True;
                      cdMaster.Edit;
                      cdMaster['calcCleanUp'] := 'Y';
                      cdMaster.Post;
                      raise;
                      
                    end
                    else if Pos('Could not find a printer for media type', E.Message) > 0 then
                    begin
                      if EvMessage(StringReplace(E.Message,#$D,'',[rfReplaceAll]) + '.' + #13#10 +
                         'You will be brought to the Bureau - VMR - Printer Info screen.', mtError, [mbOk, mbCancel], mbOk) = mrOk then
                      begin
                         ActivateFrameAs('Bureau - Virtual Mail Room - Printer Info', []);
                    end
                    end
                    else
                    begin
                      Raise;
                    end;
                  end;
                end;
              finally
                ctx_EndWait;
              end;
              Break;
            end;
        finally
          gBrowse.DataSource.DataSet.FreeBookmark(b);
        end;
      end;
      gBrowse.DataSource.DataSet.Next;
    end;
  finally
    if CleanUpNeeded then
      EvMessage('There are mailboxes that have missing content files!')
    else
        LoadBoxes(True);
    gBrowse.SelectedList.Clear;
    gBrowse.DataSource.DataSet.EnableControls;
  end;
end;

procedure TEDIT_SB_MAIL_BOX.LoadBoxes(const DoOpen: Boolean);
var
  MasterCondition, DetailCondition: string;
begin

 wwdsMaster.Enabled := false;
 wwdsDetail.Enabled := false;
 try

  ctx_StartWait('Retrieve data. Please, wait.');

  if PC2.ActivePage = tsUnreleased then
  begin
    DetailCondition := ' and t1.RELEASED_TIME is null';
    MasterCondition := ' and s.RELEASED_TIME is null';
  end
  else
  if PC2.ActivePage = tsReleased then
  begin
    DetailCondition := ' and t1.SCANNED_TIME is null and t1.PRINTED_TIME is null and t1.RELEASED_TIME is not null';
    MasterCondition := ' and s.SCANNED_TIME is null and s.PRINTED_TIME is null and s.RELEASED_TIME is not null';
  end
  else
  if PC2.ActivePage = tsPrinted then
  begin
    DetailCondition := ' and t1.SCANNED_TIME is null and t1.PRINTED_TIME is not null';
    MasterCondition := ' and s.SCANNED_TIME is null and s.PRINTED_TIME is not null';
  end
  else
  if PC2.ActivePage = tsScanned then
  begin
    DetailCondition := ' and t1.SCANNED_TIME is not null and t1.SHIPPED_TIME is null';
    MasterCondition := ' and s.SCANNED_TIME is not null and s.SHIPPED_TIME is null';
  end
  else
  if PC2.ActivePage = tsCleanup then
  begin
    DetailCondition := ' and t1.RELEASED_TIME is null';
    MasterCondition := ' and s.RELEASED_TIME is null';
  end
  else
  if PC2.ActivePage = tsEmailing then
  begin
    //Do nothing ....
    //Exit;
  end
  else
    Assert(False);



  cdBrowse.Close;
  cdBrowse.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
  cdMaster.Close;
  cdMaster.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;

  cdBrowse.DisableControls;
  cdMaster.DisableControls;
  cdContent.DisableControls;

  if  PC2.ActivePage = tsEmailing then
  begin
    with TExecDSWrapper.Create('VmrSbTopMailBoxesWithVouchers') do
    begin
      SetMacro('CONDITION', MasterCondition);
      SetMacro('DateFrom', ''''+FormatDateTime('yyyymmdd',dtFrom.DateTime)+'''');
      SetMacro('DateTo', ''''+FormatDateTime('yyyymmdd',dtTo.DateTime)+'''');
      SetMacro('ShowSqlText', 'Y');
      cdMaster.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([cdMaster]);
    DetailCondition := ' and '+UplinkFieldCondition('SB_MAIL_BOX_NBR',cdMaster);
    DetailCondition :=StringReplace(DetailCondition, 'SB_MAIL_BOX_NBR', 'UP_NBR', [rfReplaceAll, rfIgnoreCase]);
    with TExecDSWrapper.Create('SB_MAIL_BOX;GenericSelect2DiffNbrWithCondition') do
    begin
//      SetMacro('COLUMNS', 't1.*, ConvertNullInteger(t2.up_nbr, t1.SB_MAIL_BOX_NBR) up_nbr');
      SetMacro('COLUMNS', 't1.*, t2.up_nbr up_nbr');
      SetMacro('TABLE1', 'SB_MAIL_BOX');
      SetMacro('TABLE2', 'LIST_TOP_MAIL_BOXES2');
      SetMacro('CONDITION', '{AsOfNow<t1>} and t2.nbr=t1.SB_MAIL_BOX_NBR '+ DetailCondition);
      cdBrowse.DataRequest(AsVariant);
    end;
  end  else
  begin
    with TExecDSWrapper.Create('SB_MAIL_BOX;GenericSelect2DiffNbrWithCondition') do
    begin
      SetMacro('COLUMNS', 't1.*, ConvertNullInteger(t2.up_nbr, t1.SB_MAIL_BOX_NBR) up_nbr');
      SetMacro('TABLE1', 'SB_MAIL_BOX');
      SetMacro('TABLE2', 'LIST_TOP_MAIL_BOXES');
      SetMacro('CONDITION', '{AsOfNow<t1>} and t2.nbr=t1.SB_MAIL_BOX_NBR '+ DetailCondition);
      cdBrowse.DataRequest(AsVariant);
    end;
    with TExecDSWrapper.Create('VmrSbTopMailBoxes') do
    begin
      SetMacro('CONDITION', MasterCondition);
      cdMaster.DataRequest(AsVariant);
    end;
  end;

  if DoOpen then
  begin
    ctx_DataAccess.OpenDataSets([cdMaster]);
    ctx_DataAccess.OpenDataSets([cdBrowse]);
    gBrowse.SelectedList.Clear;
  end;
 finally

      //evDBTreeView1.DataSource := wwdsDetail;
     wwdsMaster.Enabled := true;
     wwdsDetail.Enabled := true;
     cdMaster.EnableControls;
     cdBrowse.EnableControls;
     cdContent.EnableControls;

     ctx_EndWait;
 end;
end;

procedure TEDIT_SB_MAIL_BOX.PC2Change(Sender: TObject);
begin
  inherited;
  LoadBoxes(True);
end;

procedure TEDIT_SB_MAIL_BOX.bRevertClick(Sender: TObject);
var
  i: Integer;
  b: TBookmark;
begin
  inherited;
  if gBrowse.SelectedList.Count = 0 then
    raise EInvalidParameters.CreateHelp('You need to select at list one shipment first', IDH_InvalidParameters);

  if ctx_VmrEngine = nil then
     raise Exception.CreateHelp('VMR engine is not loaded', IDH_InvalidParameters);

  gBrowse.DataSource.DataSet.DisableControls;
  try
    gBrowse.DataSource.DataSet.First;
    while not gBrowse.DataSource.DataSet.Eof do
    begin
      b := gBrowse.DataSource.DataSet.GetBookmark;
      try
        for i := 0 to gBrowse.SelectedList.Count-1 do
          if gBrowse.DataSource.DataSet.CompareBookmarks(gBrowse.SelectedList[i], b) = 0 then
          begin
            ctx_StartWait('Reverting '+
              gBrowse.DataSource.DataSet.FieldByName('luClName').AsString+
              ' '+ gBrowse.DataSource.DataSet.FieldByName('DESCRIPTION').AsString);
            try
              //ctx_VmrRemote.ProcessRevert(gBrowse.DataSource.DataSet['SB_MAIL_BOX_NBR']);
              ctx_VmrEngine.ProcessRevert(gBrowse.DataSource.DataSet['SB_MAIL_BOX_NBR']);
            finally
              ctx_EndWait;
            end;
            Break;
          end;
      finally
        gBrowse.DataSource.DataSet.FreeBookmark(b);
      end;
      gBrowse.DataSource.DataSet.Next;
    end;
  finally
    LoadBoxes(True);
    gBrowse.SelectedList.Clear;
    gBrowse.DataSource.DataSet.EnableControls;
  end;
end;

procedure TEDIT_SB_MAIL_BOX.Loaded;
begin
  PC2.ActivePage := tsUnreleased;
  tsReleased.TabVisible := CheckRemoteVMRActive;
  inherited;
end;

procedure TEDIT_SB_MAIL_BOX.evbtCleanMailBoxClick(Sender: TObject);
begin
  DoPurge(False);
end;

procedure TEDIT_SB_MAIL_BOX.PreviewContainerResize(Sender: TObject);
begin
  inherited;
  //PreviewContainer.FrameResize(Sender);
end;

procedure TEDIT_SB_MAIL_BOX.AdditionalOptionChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.Active and not VarIsNull(cdBrowse['SB_MAIL_BOX_NBR']) and ((Sender as TComponent).Tag = 0) then
  begin
   (Sender as TComponent).Tag := 1;
    cdBrowse.Edit;
   (Sender as TComponent).Tag := 0;
  end;
end;

procedure TEDIT_SB_MAIL_BOX.cbBlockVouchersClick(Sender: TObject);
begin
  inherited;
  if cbBlockVouchers.Checked then
    mb_AppSettings['Settings\BlockSendVouchers'] := 'Y'
  else
    mb_AppSettings['Settings\BlockSendVouchers'] := 'N'

end;

procedure TEDIT_SB_MAIL_BOX.Deactivate;
begin
  inherited;
  DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.RetrieveCondition := '';
  DM_TEMPORARY.TMP_CL.UserFilter := FTmpClUserFilter;
  DM_TEMPORARY.TMP_CL.UserFiltered := FTmpClUserFiltered;
  DM_TEMPORARY.TMP_CL.Filtered := FTmpClFiltered;
end;

procedure TEDIT_SB_MAIL_BOX.RebuildTreeView;

begin
  if not cdBrowse.Active then Exit;
  wwdsDetail.Tag := 1;
  cdBrowse.DisableControls;
  try
     evDBTreeView1.Items.BeginUpdate;
     cdBrowse.SetRange([cdMaster['SB_MAIL_BOX_NBR']], [cdMaster['SB_MAIL_BOX_NBR']]);
     cdBrowse.Filter := 'UP_NBR = '+ IntToStr(cdMaster.FieldbyName('SB_MAIL_BOX_NBR').AsInteger);
     cdBrowse.Filtered := true;
     cdBrowse.First;

     evDBTreeView1.DataSource := wwdsDetail;
     evDBTreeView1.Items.Clear;
     evDBTreeView1.RebuildTreeView;
     //evDBTreeView1.DataSource := nil;
  finally
     wwdsDetail.Tag := 0;
     //cdBrowse.Filtered := False;
     evDBTreeView1.Items.EndUpdate;
     cdBrowse.EnableControls;
  end;
end;



procedure TEDIT_SB_MAIL_BOX.cbVmrPrinterLocationKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
 if Key in [46,8] then
  begin
    cbVmrPrinterLocation.ItemIndex := -1;
    AdditionalOptionChange(cbVmrPrinterLocation);
  end;
end;

procedure TEDIT_SB_MAIL_BOX.DoPurge(aForceDelete : Boolean);
var
  i: Integer;
begin
  inherited;
  if ctx_VmrEngine = nil then
     raise Exception.CreateHelp('VMR engine is not loaded', IDH_InvalidParameters);

  if gBrowse.SelectedList.Count = 0 then
  begin
    EvMessage('Please select mailbox first');
    exit;
  end;

  if aForceDelete then
  begin
    //in here it won't delete folder with Client Internal number
    if EvMessage('Are you sure to delete seleted mailboxes and all included files ?', mtConfirmation, [mbYes, mbNo]) = mrNo
    then exit;
  end
  else begin
     if EvMessage('This function will delete links to any files missing from the VMR folder. Continue?', mtConfirmation, [mbYes, mbNo]) = mrNo
     then exit;
  end;

  ctx_StartWait('It might take awhile to be finished');
  gBrowse.DataSource.DataSet.DisableControls;
  try
    for i := 0 to gBrowse.SelectedList.Count - 1 do
    begin
      gBrowse.DataSource.Dataset.GotoBookmark(gBrowse.SelectedList[i]);

      // Purge operation uses global datasets, so we need to use a separate context
      Mainboard.ContextManager.StoreThreadContext;
      try
        Mainboard.ContextManager.SetThreadContext(Mainboard.ContextManager.CopyContext(Context));
        ctx_VmrRemote.PurgeMailBox(gBrowse.DataSource.Dataset['SB_MAIL_BOX_NBR'], aForceDelete);
      finally
        Mainboard.ContextManager.RestoreThreadContext;
      end;
      cdMaster.Edit;
      cdMaster['calcCleanUp'] := 'N';
      cdMaster.Post;
    end;
  finally
    if aForceDelete then
      LoadBoxes(True);

    gBrowse.SelectedList.Clear;
    gBrowse.DataSource.DataSet.EnableControls;
    ctx_EndWait;
    EvMessage('Finished! You need to rebuild VMR History before creating Archive CDs')
  end;
end;

initialization
  RegisterClass(TEDIT_SB_MAIL_BOX);

end.

