inherited EDIT_SB_DELIVERY_METHOD: TEDIT_SB_DELIVERY_METHOD
  inherited PC: TevPageControl
    ActivePage = tshtDetails
    inherited tshtBrowse: TTabSheet
      inherited gBrowse: TevDBGrid
        Selected.Strings = (
          'luServiceName'#9'40'#9'Service Name'#9'F'
          'luMethodName'#9'40'#9'Method Name'#9'F')
        IniAttributes.SectionName = 'TEDIT_SB_DELIVERY_METHOD\gBrowse'
      end
    end
    inherited tshtDetails: TTabSheet
      inherited evPanel1: TevPanel
        object lMethodName: TLabel [1]
          Left = 488
          Top = 8
          Width = 78
          Height = 13
          Caption = 'lMethodName'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        inherited edType: TevDBLookupCombo
          Width = 353
          Selected.Strings = (
            'SERVICE_NAME'#9'40'#9'Service Name'#9'F'
            'NAME'#9'40'#9'Method Name'#9'F')
          DataField = 'SY_DELIVERY_METHOD_NBR'
          LookupTable = DM_SY_DELIVERY_METHOD.SY_DELIVERY_METHOD
          LookupField = 'SY_DELIVERY_METHOD_NBR'
          Options = [loColLines, loTitles]
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
  end
end
