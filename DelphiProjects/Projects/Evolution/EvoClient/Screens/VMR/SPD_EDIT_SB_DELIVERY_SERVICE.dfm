inherited EDIT_SB_DELIVERY_SERVICE: TEDIT_SB_DELIVERY_SERVICE
  inherited PC: TevPageControl
    inherited tshtBrowse: TTabSheet
      inherited gBrowse: TevDBGrid
        Selected.Strings = (
          'luServiceName'#9'40'#9'Service Name'#9'F')
        IniAttributes.SectionName = 'TEDIT_SB_DELIVERY_SERVICE\gBrowse'
      end
    end
    inherited tshtDetails: TTabSheet
      inherited evPanel1: TevPanel
        inherited edType: TevDBLookupCombo
          Selected.Strings = (
            'NAME'#9'40'#9'NAME'#9'F')
          DataField = 'SY_DELIVERY_SERVICE_NBR'
          LookupTable = DM_SY_DELIVERY_SERVICE.SY_DELIVERY_SERVICE
          LookupField = 'SY_DELIVERY_SERVICE_NBR'
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_DELIVERY_SERVICE.SB_DELIVERY_SERVICE
  end
end
