// Screens of "VMR"
unit scr_VMR;

interface

uses
  SVmrScr,
  SPD_EDIT_SY_MEDIA_TYPE,
  SPD_EDIT_SY_DELIVERY_SERVICE,
  SPD_EDIT_SY_DELIVERY_METHOD,
  SPD_EDIT_SB_DELIVERY_SERVICE,
  SPD_EDIT_SB_DELIVERY_METHOD,
  SPD_EDIT_SB_MEDIA_TYPE,
  SPD_EDIT_OPTION_BASE,
  SPD_EDIT_SB_MAIL_BOX,
  SPD_EDIT_CL_MAIL_BOX_GROUP,
  SPD_EDIT_SB_PAPER_INFO,
  SPD_PrinterSetup,
  SPD_TrackingByAddresse,
  SPD_TrackingByJob,
  SPD_CourierBillEntry,
  SPD_CourierPickup,
  SPD_PrinterPickup,
  SPD_ArchivalScreen,
  SPD_ReprintPayrollsToVmr,
  SPD_LocationSetup,
  SPD_EDIT_SB_VMR_UTILS;

implementation

end.
