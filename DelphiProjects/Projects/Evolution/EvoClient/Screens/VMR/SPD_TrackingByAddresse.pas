// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_TrackingByAddresse;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_BROWSE_DETAIL_BASE, SDataStructure, DB, Wwdatsrc,
   StdCtrls, wwdblook, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, SDDClasses, kbmMemTable, ISKbmMemDataSet, EvContext, EvLegacy,
  ISBasicClasses, EvDataAccessComponents, Buttons, ISDataAccessComponents,
  rwPreviewContainerFrm, SDataDictbureau, SDataDicttemp, isBasicUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIEdit;

type
  TTrackingByAddresse = class(TEDIT_BROWSE_DETAIL_BASE)
    cdJobs: TevClientDataSet;
    cdShipments: TevClientDataSet;
    cdJobsSB_MAIL_BOX_NBR: TIntegerField;
    cdJobsCL_NBR: TIntegerField;
    cdJobsCL_MAIL_BOX_GROUP_NBR: TIntegerField;
    cdJobsCOST: TFloatField;
    cdJobsREQUIRED: TStringField;
    cdJobsNOTIFICATION_EMAIL: TStringField;
    cdJobsPR_NBR: TIntegerField;
    cdJobsRELEASED_TIME: TDateTimeField;
    cdJobsPRINTED_TIME: TDateTimeField;
    cdJobsSB_COVER_LETTER_REPORT_NBR: TIntegerField;
    cdJobsSCANNED_TIME: TDateTimeField;
    cdJobsSHIPPED_TIME: TDateTimeField;
    cdJobsSY_COVER_LETTER_REPORT_NBR: TIntegerField;
    cdJobsNOTE: TStringField;
    cdJobsUP_LEVEL_MAIL_BOX_NBR: TIntegerField;
    cdJobsTRACKING_INFO: TStringField;
    cdJobsAUTO_RELEASE_TYPE: TStringField;
    cdJobsBARCODE: TStringField;
    cdJobsCREATED_TIME: TDateTimeField;
    cdJobsDISPOSE_CONTENT_AFTER_SHIPPING: TStringField;
    cdJobsADDRESSEE: TStringField;
    cdJobsDESCRIPTION: TStringField;
    cdJobsSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdJobsSB_MEDIA_TYPE_NBR: TIntegerField;
    cdJobsDESCR: TStringField;
    cdJobsCO_NBR: TIntegerField;
    cdJobsJOB_PR_NBR: TIntegerField;
    DM_TEMPORARY: TDM_TEMPORARY;
    cdJobsluMethodName: TStringField;
    cdJobsluMediaName: TStringField;
    cdJobsluServiceName: TStringField;
    cdJobsluClNbr: TStringField;
    cdJobsluClName: TStringField;
    cdJobsluPrCheckDate: TDateField;
    cdJobsluPrRunNumber: TIntegerField;
    cdJobsluCoNbr: TStringField;
    cdJobsluCoName: TStringField;
    cdJobscalcStatus: TStringField;
    cdJobscalcColor: TIntegerField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    cdShipmentsSB_MAIL_BOX_NBR: TIntegerField;
    cdShipmentsCL_NBR: TIntegerField;
    cdShipmentsPR_NBR: TIntegerField;
    cdShipmentsADDRESSEE: TStringField;
    cdShipmentsDESCRIPTION: TStringField;
    cdShipmentsSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdShipmentsSB_MEDIA_TYPE_NBR: TIntegerField;
    cdShipmentsluMethodName: TStringField;
    cdShipmentsluMediaName: TStringField;
    cdShipmentsluServiceName: TStringField;
    gShipments: TevDBGrid;
    evSplitter1: TevSplitter;
    cdShipmentspage_count: TIntegerField;
    cdContent: TevClientDataSet;
    dsContent: TevDataSource;
    pPreViewPanel: TevPanel;
    evDBGrid1: TevDBGrid;
    evSplitter2: TevSplitter;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    edSearchWords: TevEdit;
    cdMaster: TevClientDataSet;
    cdMasterSB_MAIL_BOX_NBR: TIntegerField;
    cdMasterCL_NBR: TIntegerField;
    cdMasterPR_NBR: TIntegerField;
    cdMasterRELEASED_TIME: TDateTimeField;
    cdMasterNOTE: TStringField;
    cdMasterAUTO_RELEASE_TYPE: TStringField;
    cdMasterCREATED_TIME: TDateTimeField;
    cdMasterADDRESSEE: TStringField;
    cdMasterDESCRIPTION: TStringField;
    cdMasterSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdMasterSB_MEDIA_TYPE_NBR: TIntegerField;
    cdMasterluMethodName: TStringField;
    cdMasterluMediaName: TStringField;
    cdMasterluServiceName: TStringField;
    cdMasterluClNbr: TStringField;
    cdMasterluClName: TStringField;
    cdMasterluPrCheckDate: TDateField;
    cdMasterluPrRunNumber: TIntegerField;
    cdMastercalcStatus: TStringField;
    cdMastercalcColor: TIntegerField;
    cdMasterPRINTED_TIME: TDateTimeField;
    cdMasterSHIPPED_TIME: TDateTimeField;
    cdMasterTRACKING_INFO: TStringField;
    cdMasterSCANNED_TIME: TDateTimeField;
    bSearch: TevBitBtn;
    procedure cdJobsCalcFields(DataSet: TDataSet);
    procedure gBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure PCChange(Sender: TObject);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure gBrowseDblClick(Sender: TObject);
    procedure gShipmentsDblClick(Sender: TObject);
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
    procedure evDBGrid1DblClick(Sender: TObject);
    procedure edSearchWordsKeyPress(Sender: TObject; var Key: Char);
    procedure bSearchClick(Sender: TObject);
    procedure dsContentDataChange(Sender: TObject; Field: TField);
  private
    FPreviewContainer: TrwPreviewContainer;
    procedure LoadContent;
  public
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
  end;

implementation

{$R *.dfm}

uses EvUtils, SVmrClasses, SFrameEntry, SReportSettings, EvTypes, StrUtils;

{ TTrackingByJob }

procedure TTrackingByAddresse.Activate;
begin
  inherited;
  //Content
  evDBGrid1.Selected.Text :=
                'JOB_DESCR'#9'25'#9'Job Description'#9'F'#13#10+
                'REP_DESCR'#9'25'#9'Report Description'#9'F'#13#10+
                'FILE_NAME'#9'45'#9'File Name'#9'F'#13#10+
                'MEDIA_TYPE'#9'1'#9'Type'#9'F'#13#10+
                'PAGE_COUNT'#9'5'#9'Pages'#9'F'#13#10+
                ifthen(CheckRemoteVMRActive, 'REMOTEPRINTDATE'#9'25'#9'Remote Print Date'#9'F', '');
  evDBGrid1.ApplySelected;

end;

procedure TTrackingByAddresse.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  if not cdMaster.Active then
    cdMaster.CreateDataSet;
  NavigationDataSet := cdMaster;
end;

procedure TTrackingByAddresse.cdJobsCalcFields(DataSet: TDataSet);
begin
  inherited;
  if VarIsNull(DataSet['RELEASED_TIME']) then
  begin
    DataSet['calcStatus'] := 'Waiting to be released';
    DataSet['calcColor'] := clGray;
  end
  else
  if VarIsNull(DataSet['PRINTED_TIME']) then
  begin
    DataSet['calcStatus'] := 'Waiting to be printed';
    DataSet['calcColor'] := clOlive;
  end
  else
  if VarIsNull(DataSet['SCANNED_TIME']) then
  begin
    DataSet['calcStatus'] := 'Waiting for scan';
    DataSet['calcColor'] := clOlive;
  end
  else
  if VarIsNull(DataSet['SHIPPED_TIME']) then
  begin
    DataSet['calcStatus'] := 'Scanned/W. for pickup';
    DataSet['calcColor'] := clYellow;
  end
  else
  begin
    DataSet['calcStatus'] := 'Picked up';
    DataSet['calcColor'] := clLime;
  end
end;

procedure TTrackingByAddresse.gBrowseCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  if Field.FieldName = 'calcStatus' then
    ABrush.Color := TColor(VarToInt(Field.DataSet['calcColor']));
end;

procedure TTrackingByAddresse.PCChange(Sender: TObject);
begin
  inherited;
  LoadContent;
end;

procedure TTrackingByAddresse.wwdsListDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if wwdsList.Active and not wwdsList.DataSet.ControlsDisabled then
  begin
    cdShipments.Close;
  end;
end;

procedure TTrackingByAddresse.gBrowseDblClick(Sender: TObject);
begin
  cdShipments.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
  with TExecDSWrapper.Create('VmrSbMailBoxes') do
  begin
    SetParam('BoxNbr', wwdsList.DataSet.FieldByName('SB_MAIL_BOX_NBR').AsInteger);
    cdShipments.DataRequest(AsVariant);
  end;

  cdShipments.DisableControls;
  try
    cdShipments.Close;
    ctx_DataAccess.OpenDataSets([cdShipments]);
  finally
    cdShipments.EnableControls;
  end;
  gShipments.SetFocus;
end;

procedure TTrackingByAddresse.gShipmentsDblClick(Sender: TObject);
begin
  inherited gBrowseDblClick(Sender);
end;

procedure TTrackingByAddresse.LoadContent;
begin
  if cdShipments.Active and not cdShipments.ControlsDisabled then
    if cdContent.Tag <> cdShipmentsSB_MAIL_BOX_NBR.AsInteger then
    begin
      cdContent.ProviderName := ctx_DataAccess.SB_CUSTOM_VIEW.ProviderName;
      cdContent.DisableControls;
      try
        cdContent.Close;
        with TExecDSWrapper.Create('VmrSbMailBoxContents') do
        begin
          SetParam('BoxNbr', cdShipmentsSB_MAIL_BOX_NBR.AsInteger);
          cdContent.DataRequest(AsVariant);
        end;
        ctx_DataAccess.OpenDataSets([cdContent]);
      finally
        cdContent.EnableControls;
      end;
      cdContent.Tag := cdShipmentsSB_MAIL_BOX_NBR.AsInteger;
    end;
end;

procedure TTrackingByAddresse.wwdsMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  LoadContent;
end;

procedure TTrackingByAddresse.evDBGrid1DblClick(Sender: TObject);
var
  FResult: TrwReportResults;
  lsLayers :String;
begin
  inherited;

  if dsContent.DataSet.IsEmpty then exit;

  FreeAndNil(FPreviewContainer);

  FResult := TrwReportResults.Create;
  try
    if VarIsNull(dsContent.DataSet['LAYERS']) then
      lsLayers := ''
    else
      lsLayers:= dsContent.DataSet['LAYERS'];
    FResult.AddReportResult(dsContent.DataSet['DESCRIPTION'], TReportType(dsContent.DataSet['REPORT_TYPE']),
       ctx_VMRRemote.RetrieveFile(dsContent.DataSet['FILE_NAME'])).Layers := StrToLayers(lsLayers);

    FPreviewContainer := TrwPreviewContainer.Create(Self);
    FPreviewContainer.Parent := pPreViewPanel;
    FPreviewContainer.Align := alClient;

    ctx_RWLocalEngine.Preview(FResult, FPreviewContainer, True);
  finally
    FResult.Free;
  end;
end;

procedure TTrackingByAddresse.edSearchWordsKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if Key = #13 then
    bSearch.Click;
end;

procedure TTrackingByAddresse.bSearchClick(Sender: TObject);
var
  s, sWord, sCond: string;
begin
  inherited;
  s := edSearchWords.Text;
  sCond := '';
  while s <> '' do
  begin
    sWord := GetNextStrValue(s, ' ');
    if sCond <> '' then
      sCond := sCond+ ' or ';
    sCond := sCond+ '(UpperCase(ADDRESSEE) like ''%'+ UpperCase(sWord)+ '%'')';
  end;
  if sCond <> '' then
    sCond := ' and ('+ sCond+ ')';
  cdMaster.DisableControls;
  try
    cdMaster.CheckDSCondition('UP_LEVEL_MAIL_BOX_NBR is null'+ sCond);
    ctx_DataAccess.OpenDataSets([cdMaster]);
  finally
    cdMaster.EnableControls;
  end;
end;

procedure TTrackingByAddresse.dsContentDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  FreeAndNil(FPreviewContainer);
end;

initialization
  RegisterClass(TTrackingByAddresse);

end.
