inherited VmrPrinterLocationSetup: TVmrPrinterLocationSetup
  object ScrollBox1: TScrollBox [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 265
    Align = alClient
    TabOrder = 0
    object evPageControl1: TevPageControl
      Left = 0
      Top = 0
      Width = 431
      Height = 261
      ActivePage = tsRemotePrinterLocations
      Align = alClient
      TabOrder = 0
      OnChange = evPageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Printer locations'
        object evDBGrid1: TevDBGrid
          Left = 0
          Top = 33
          Width = 423
          Height = 200
          DisableThemesInTitle = False
          Selected.Strings = (
            'LOCATION'#9'40'#9'Location'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TVmrPrinterLocationSetup\evDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = evDataSource1
          ReadOnly = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 423
          Height = 33
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object SpeedButton2: TevSpeedButton
            Left = 32
            Top = 0
            Width = 25
            Height = 25
            HideHint = True
            AutoSize = False
            OnClick = SpeedButton2Click
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
              888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
              C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
              C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
          object SpeedButton1: TevSpeedButton
            Left = 0
            Top = 0
            Width = 25
            Height = 25
            HideHint = True
            AutoSize = False
            OnClick = SpeedButton1Click
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
              888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
              A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
              A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
          object BtnLocationCleanUp: TevSpeedButton
            Left = 64
            Top = 0
            Width = 25
            Height = 25
            Hint = 'Clean Up Location'
            HideHint = True
            AutoSize = False
            OnClick = BtnLocationCleanUpClick
            ImageIndex = 4
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00009CE700009C
              E7000094E7000094E7000094E700009CEF00009CEF00009CEF000094E7000094
              E7000094E7000094E7000094E7000094E700009CE700009CE700089CE70039DE
              F70039DEF70031DEF70031E7F70039B5C6004A736B0031EFFF0031E7F70031DE
              F70031DEF70031DEF70031DEF70039DEF70039E7F700089CE70084CEF70018B5
              EF0042DEF70031D6F70031D6F70039ADC60052211000426B6B0031DEFF0031D6
              F70031D6F70031D6F70031D6F70042DEF70018B5EF0084CEF700FFFFFF000094
              E7004AD6F70039D6F70031D6F70031DEFF0031DEFF004A31210031B5CE0031D6
              FF0031D6F70031CEF70031D6F7004AD6F7000094E700FFFFFF00FFFFFF0084CE
              F70018ADEF0052D6F70031C6EF0031A5C60031B5D60042636B004A31210031C6
              E70029CEFF0029CEF70052D6F70021ADEF0084CEF700FFFFFF00FFFFFF00FFFF
              FF00089CE70052CEF70042CEEF00423129004A2918004A2918004A2110004239
              310029BDEF0042CEF70052CEF700089CE700FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00ADDEF70018A5EF0073DEFF00398CAD0042211000396B7B0029D6FF0029CE
              FF0031C6F70073D6FF0018A5EF00ADDEF700FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00089CE7005ACEF70063DEFF003939390042211000398CAD0031CE
              FF005ACEF70063CEF700089CE700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00CEEFFF00089CEF009CE7FF004AA5CE0039180800392118004AAD
              DE009CE7FF0010A5EF00CEEFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF0029ADEF006BCEF70094E7FF0039394200310800004A31
              290063C6EF0029ADEF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00CEEFFF0008A5E700B5E7FF0073D6FF0073D6FF00C6F7
              FF0010A5EF00CEEFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF005ABDEF005AC6F700BDE7FF00B5E7FF006BC6
              F7005ABDEF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00F7FFFF00009CE700CEEFFF00CEEFFF00009C
              E700F7FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0052BDEF0073C6F70073C6F70052BD
              EF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00009CE700009CE700FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Email settings'
        ImageIndex = 1
        object evLabel1: TevLabel
          Left = 344
          Top = 264
          Width = 56
          Height = 13
          Caption = 'SMTP host:'
          Visible = False
        end
        object evLabel3: TevLabel
          Left = 344
          Top = 312
          Width = 54
          Height = 13
          Caption = 'User name:'
          Visible = False
        end
        object evLabel4: TevLabel
          Left = 344
          Top = 356
          Width = 49
          Height = 13
          Caption = 'Password:'
          Visible = False
        end
        object evDBGrid2: TevDBGrid
          Left = 0
          Top = 25
          Width = 423
          Height = 216
          DisableThemesInTitle = False
          ControlType.Strings = (
            'Pass;CustomEdit;evPassword;T')
          Selected.Strings = (
            'From'#9'40'#9'From Email Address'#9'F'
            'Host'#9'60'#9'Host'#9'F'
            'Port'#9'10'#9'Port'#9'F'
            'User'#9'40'#9'Auth User'#9'F'
            'Pass'#9'40'#9'Auth Pass'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TVmrPrinterLocationSetup\evDBGrid2'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alTop
          DataSource = evDataSource2
          ReadOnly = False
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 423
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object evSpeedButton1: TevSpeedButton
            Left = 32
            Top = 0
            Width = 25
            Height = 25
            HideHint = True
            AutoSize = False
            OnClick = evSpeedButton1Click
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
              888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
              C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
              C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
          object evSpeedButton2: TevSpeedButton
            Left = 0
            Top = 0
            Width = 25
            Height = 25
            HideHint = True
            AutoSize = False
            OnClick = evSpeedButton2Click
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
              888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
              A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
              A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
        end
        object GroupBox1: TGroupBox
          Left = 16
          Top = 256
          Width = 313
          Height = 65
          Caption = 'Notification email settings'
          TabOrder = 2
          object evLabel2: TevLabel
            Left = 8
            Top = 16
            Width = 84
            Height = 13
            Caption = '"FROM" address:'
          end
          object edFrom: TevEdit
            Left = 8
            Top = 32
            Width = 289
            Height = 21
            TabOrder = 0
            Text = 'evFrom'
            OnChange = edHostChange
          end
        end
        object edHost: TevEdit
          Left = 344
          Top = 280
          Width = 289
          Height = 21
          TabOrder = 3
          Text = 'edHost'
          Visible = False
          OnChange = edHostChange
        end
        object edUser: TevEdit
          Left = 344
          Top = 324
          Width = 289
          Height = 21
          TabOrder = 4
          Text = 'edUser'
          Visible = False
          OnChange = edHostChange
        end
        object edPass: TevEdit
          Left = 344
          Top = 372
          Width = 289
          Height = 21
          TabOrder = 5
          Text = 'edPass'
          Visible = False
          OnChange = edHostChange
        end
        object evPassword: TevDBEdit
          Left = 440
          Top = 160
          Width = 121
          Height = 21
          DataField = 'Pass'
          DataSource = evDataSource2
          PasswordChar = '*'
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object tsGeneralSetup: TTabSheet
        Caption = 'General setup'
        ImageIndex = 2
        object cbBlockVMR: TCheckBox
          Left = 8
          Top = 8
          Width = 97
          Height = 17
          Caption = 'Block VMR'
          TabOrder = 0
          OnClick = edHostChange
        end
        object cbRemoteVMR: TCheckBox
          Left = 8
          Top = 32
          Width = 97
          Height = 17
          Caption = 'Remote VMR'
          TabOrder = 1
          OnClick = edHostChange
        end
      end
      object tsRemotePrinterLocations: TTabSheet
        Caption = 'Remote Printer Locations'
        ImageIndex = 3
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 423
          Height = 33
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object evSpeedButton3: TevSpeedButton
            Left = 32
            Top = 0
            Width = 25
            Height = 25
            HideHint = True
            AutoSize = False
            OnClick = evSpeedButton3Click
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
              888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
              C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
              C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
          object evSpeedButton4: TevSpeedButton
            Left = 0
            Top = 0
            Width = 25
            Height = 25
            HideHint = True
            AutoSize = False
            OnClick = evSpeedButton4Click
            NumGlyphs = 2
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
              888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
              A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
              A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
              DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
          object BtnRemoteLocationCleanUp: TevSpeedButton
            Left = 64
            Top = 0
            Width = 25
            Height = 25
            Hint = 'Remote Clean Up Location'
            HideHint = True
            AutoSize = False
            OnClick = BtnRemoteLocationCleanUpClick
            ImageIndex = 4
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00009CE700009C
              E7000094E7000094E7000094E700009CEF00009CEF00009CEF000094E7000094
              E7000094E7000094E7000094E7000094E700009CE700009CE700089CE70039DE
              F70039DEF70031DEF70031E7F70039B5C6004A736B0031EFFF0031E7F70031DE
              F70031DEF70031DEF70031DEF70039DEF70039E7F700089CE70084CEF70018B5
              EF0042DEF70031D6F70031D6F70039ADC60052211000426B6B0031DEFF0031D6
              F70031D6F70031D6F70031D6F70042DEF70018B5EF0084CEF700FFFFFF000094
              E7004AD6F70039D6F70031D6F70031DEFF0031DEFF004A31210031B5CE0031D6
              FF0031D6F70031CEF70031D6F7004AD6F7000094E700FFFFFF00FFFFFF0084CE
              F70018ADEF0052D6F70031C6EF0031A5C60031B5D60042636B004A31210031C6
              E70029CEFF0029CEF70052D6F70021ADEF0084CEF700FFFFFF00FFFFFF00FFFF
              FF00089CE70052CEF70042CEEF00423129004A2918004A2918004A2110004239
              310029BDEF0042CEF70052CEF700089CE700FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00ADDEF70018A5EF0073DEFF00398CAD0042211000396B7B0029D6FF0029CE
              FF0031C6F70073D6FF0018A5EF00ADDEF700FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00089CE7005ACEF70063DEFF003939390042211000398CAD0031CE
              FF005ACEF70063CEF700089CE700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00CEEFFF00089CEF009CE7FF004AA5CE0039180800392118004AAD
              DE009CE7FF0010A5EF00CEEFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF0029ADEF006BCEF70094E7FF0039394200310800004A31
              290063C6EF0029ADEF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00CEEFFF0008A5E700B5E7FF0073D6FF0073D6FF00C6F7
              FF0010A5EF00CEEFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF005ABDEF005AC6F700BDE7FF00B5E7FF006BC6
              F7005ABDEF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00F7FFFF00009CE700CEEFFF00CEEFFF00009C
              E700F7FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0052BDEF0073C6F70073C6F70052BD
              EF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00009CE700009CE700FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
        end
        object evDBGrid3: TevDBGrid
          Left = 0
          Top = 33
          Width = 423
          Height = 200
          DisableThemesInTitle = False
          Selected.Strings = (
            'LOCATION'#9'40'#9'Location'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TVmrPrinterLocationSetup\evDBGrid3'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = evDataSource3
          ReadOnly = False
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
    end
  end
  object evDataSource1: TevDataSource
    DataSet = evClientDataSet1
    Left = 304
    Top = 32
  end
  object evClientDataSet1: TevClientDataSet
    FieldDefs = <
      item
        Name = 'LOCATION'
        DataType = ftString
        Size = 40
      end>
    AfterInsert = evClientDataSet1AfterInsert
    AfterEdit = evClientDataSet1AfterEdit
    BeforePost = evClientDataSet1BeforePost
    AfterDelete = evClientDataSet1AfterDelete
    Left = 344
    Top = 32
    object evClientDataSet1LOCATION: TStringField
      FieldName = 'LOCATION'
      Size = 40
    end
  end
  object evDataSource2: TevDataSource
    DataSet = evClientDataSet2
    Left = 304
    Top = 80
  end
  object evClientDataSet2: TevClientDataSet
    FieldDefs = <
      item
        Name = 'LOCATION'
        DataType = ftString
        Size = 40
      end>
    AfterInsert = evClientDataSet2AfterInsert
    AfterEdit = evClientDataSet2AfterEdit
    AfterDelete = evClientDataSet2AfterDelete
    Left = 344
    Top = 80
    object evClientDataSet2Host: TStringField
      FieldName = 'Host'
      Size = 60
    end
    object evClientDataSet2From: TStringField
      FieldName = 'From'
      Size = 40
    end
    object evClientDataSet2User: TStringField
      FieldName = 'User'
      Size = 40
    end
    object evClientDataSet2Pass: TStringField
      FieldName = 'Pass'
      Size = 40
    end
    object evClientDataSet2Port: TIntegerField
      FieldName = 'Port'
    end
  end
  object evDataSource3: TevDataSource
    DataSet = evClientDataSet3
    Left = 312
    Top = 160
  end
  object evClientDataSet3: TevClientDataSet
    FieldDefs = <
      item
        Name = 'LOCATION'
        DataType = ftString
        Size = 40
      end>
    AfterInsert = evClientDataSet3AfterInsert
    AfterEdit = evClientDataSet3AfterEdit
    BeforePost = evClientDataSet3BeforePost
    AfterDelete = evClientDataSet3AfterDelete
    Left = 352
    Top = 160
    object RemoteLocationField: TStringField
      FieldName = 'LOCATION'
      Size = 40
    end
  end
end
