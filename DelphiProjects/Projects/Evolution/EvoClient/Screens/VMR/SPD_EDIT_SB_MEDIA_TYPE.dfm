inherited EDIT_SB_MEDIA_TYPE: TEDIT_SB_MEDIA_TYPE
  inherited PC: TevPageControl
    ActivePage = tshtDetails
    inherited tshtBrowse: TTabSheet
      inherited gBrowse: TevDBGrid
        Selected.Strings = (
          'luMediaName'#9'40'#9'Media Name'#9'F')
        IniAttributes.SectionName = 'TEDIT_SB_MEDIA_TYPE\gBrowse'
      end
    end
    inherited tshtDetails: TTabSheet
      inherited evPanel1: TevPanel
        inherited evLabel1: TevLabel
          Width = 56
          Caption = 'Media Type'
        end
        inherited edType: TevDBLookupCombo
          Selected.Strings = (
            'NAME'#9'40'#9'NAME'#9'F')
          DataField = 'SY_MEDIA_TYPE_NBR'
          LookupTable = DM_SY_MEDIA_TYPE.SY_MEDIA_TYPE
          LookupField = 'SY_MEDIA_TYPE_NBR'
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
  end
end
