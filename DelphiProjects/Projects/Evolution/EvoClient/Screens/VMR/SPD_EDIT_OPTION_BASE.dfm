inherited EDIT_OPTION_BASE: TEDIT_OPTION_BASE
  inherited PC: TevPageControl
    inherited tshtBrowse: TTabSheet
      inherited gBrowse: TevDBGrid
        Width = 435
        IniAttributes.SectionName = 'TEDIT_OPTION_BASE\gBrowse'
        Align = alClient
      end
    end
    inherited tshtDetails: TTabSheet
      object evPanel1: TevPanel
        Left = 0
        Top = 0
        Width = 435
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel1: TevLabel
          Left = 8
          Top = 8
          Width = 77
          Height = 13
          Caption = 'Delivery Service'
        end
        object edType: TevDBLookupCombo
          Left = 128
          Top = 8
          Width = 377
          Height = 21
          DropDownAlignment = taLeftJustify
          DataSource = wwdsDetail
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
      object evPanel2: TevPanel
        Left = 0
        Top = 33
        Width = 435
        Height = 209
        Align = alClient
        BevelOuter = bvLowered
        Caption = 'No Parameters Available'
        TabOrder = 1
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    Enabled = False
    OnDataChange = wwdsDetailDataChange
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 256
    Top = 16
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 400
  end
end
