inherited CourierBillEntry: TCourierBillEntry
  object gBrowse: TevDBGrid [0]
    Left = 0
    Top = 85
    Width = 435
    Height = 181
    DisableThemesInTitle = False
    Selected.Strings = (
      'DESCRIPTION'#9'25'#9'Description'#9'T'
      'ADDRESSEE'#9'40'#9'Addressee'#9'T'
      'COST'#9'10'#9'Cost'#9'F'
      'luClNbr'#9'8'#9'Cl#'#9'T'
      'luClName'#9'20'#9'Client'#9'T'
      'luServiceName'#9'10'#9'Service'#9'T'
      'luMethodName'#9'10'#9'Method'#9'T'
      'luMediaName'#9'10'#9'Media'#9'T')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TCourierBillEntry\gBrowse'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsMaster
    KeyOptions = [dgEnterToTab]
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnCalcCellColors = gBrowseCalcCellColors
    PaintOptions.AlternatingRowColor = clCream
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 0
    Width = 435
    Height = 85
    Align = alTop
    TabOrder = 1
    object evLabel1: TevLabel
      Left = 8
      Top = 8
      Width = 77
      Height = 13
      Caption = 'Delivery Service'
    end
    object evLabel2: TevLabel
      Left = 8
      Top = 32
      Width = 46
      Height = 13
      Caption = 'Bill Period'
    end
    object evLabel3: TevLabel
      Left = 208
      Top = 32
      Width = 3
      Height = 13
      Caption = '-'
    end
    object evLabel4: TevLabel
      Left = 8
      Top = 56
      Width = 40
      Height = 13
      Caption = 'Bill Total'
    end
    object edDeliveryMethod: TevDBLookupCombo
      Left = 96
      Top = 8
      Width = 249
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'luServiceName'#9'40'#9'luServiceName'#9'F')
      LookupTable = DM_SB_DELIVERY_SERVICE.SB_DELIVERY_SERVICE
      LookupField = 'SB_DELIVERY_SERVICE_NBR'
      Style = csDropDownList
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnChange = EnableLoadButton
    end
    object edPeriodEnd: TevDateTimePicker
      Left = 224
      Top = 32
      Width = 97
      Height = 21
      Date = 37800.000000000000000000
      Time = 37800.000000000000000000
      TabOrder = 1
      OnChange = EnableLoadButton
    end
    object bLoad: TevButton
      Left = 360
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Load'
      TabOrder = 2
      OnClick = bLoadClick
    end
    object edBillTotal: TevEdit
      Left = 96
      Top = 56
      Width = 97
      Height = 21
      TabOrder = 3
      Text = '0.00'
    end
  end
  object edPeriodBegin: TevDateTimePicker [2]
    Left = 96
    Top = 32
    Width = 97
    Height = 21
    Date = 37800.000000000000000000
    Time = 37800.000000000000000000
    TabOrder = 2
    OnChange = EnableLoadButton
  end
  inherited wwdsMaster: TevDataSource
    DataSet = cdBrowse
    Left = 229
    Top = 114
  end
  inherited wwdsDetail: TevDataSource
    Left = 286
    Top = 114
  end
  inherited wwdsList: TevDataSource
    Left = 178
    Top = 114
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 280
    Top = 8
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 312
    Top = 8
  end
  object cdBrowse: TevClientDataSet
    ProviderName = 'SB_MAIL_BOX_PROV'
    ClientID = -1
    Left = 248
    Top = 128
    object cdBrowseSB_MAIL_BOX_NBR: TIntegerField
      FieldName = 'SB_MAIL_BOX_NBR'
    end
    object cdBrowseCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdBrowseCL_MAIL_BOX_GROUP_NBR: TIntegerField
      FieldName = 'CL_MAIL_BOX_GROUP_NBR'
    end
    object cdBrowseCOST: TFloatField
      FieldName = 'COST'
    end
    object cdBrowseREQUIRED: TStringField
      FieldName = 'REQUIRED'
      FixedChar = True
      Size = 1
    end
    object cdBrowseNOTIFICATION_EMAIL: TStringField
      FieldName = 'NOTIFICATION_EMAIL'
      Size = 80
    end
    object cdBrowsePR_NBR: TIntegerField
      FieldName = 'PR_NBR'
    end
    object cdBrowseRELEASED_TIME: TDateTimeField
      FieldName = 'RELEASED_TIME'
    end
    object cdBrowsePRINTED_TIME: TDateTimeField
      FieldName = 'PRINTED_TIME'
    end
    object cdBrowseSB_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SB_COVER_LETTER_REPORT_NBR'
    end
    object cdBrowseSCANNED_TIME: TDateTimeField
      FieldName = 'SCANNED_TIME'
    end
    object cdBrowseSHIPPED_TIME: TDateTimeField
      FieldName = 'SHIPPED_TIME'
    end
    object cdBrowseSY_COVER_LETTER_REPORT_NBR: TIntegerField
      FieldName = 'SY_COVER_LETTER_REPORT_NBR'
    end
    object cdBrowseNOTE: TStringField
      FieldName = 'NOTE'
      Size = 512
    end
    object cdBrowseUP_LEVEL_MAIL_BOX_NBR: TIntegerField
      FieldName = 'UP_LEVEL_MAIL_BOX_NBR'
    end
    object cdBrowseTRACKING_INFO: TStringField
      FieldName = 'TRACKING_INFO'
      Size = 40
    end
    object cdBrowseAUTO_RELEASE_TYPE: TStringField
      FieldName = 'AUTO_RELEASE_TYPE'
      FixedChar = True
      Size = 1
    end
    object cdBrowseBARCODE: TStringField
      FieldName = 'BARCODE'
      Size = 40
    end
    object cdBrowseCREATED_TIME: TDateTimeField
      FieldName = 'CREATED_TIME'
    end
    object cdBrowseDISPOSE_CONTENT_AFTER_SHIPPING: TStringField
      FieldName = 'DISPOSE_CONTENT_AFTER_SHIPPING'
      FixedChar = True
      Size = 1
    end
    object cdBrowseADDRESSEE: TStringField
      FieldName = 'ADDRESSEE'
      Size = 512
    end
    object cdBrowseDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdBrowseSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
    end
    object cdBrowseSB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SB_MEDIA_TYPE_NBR'
    end
    object cdBrowseluMethodName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMethodName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luMethodName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluMediaName: TStringField
      FieldKind = fkLookup
      FieldName = 'luMediaName'
      LookupDataSet = DM_SB_MEDIA_TYPE.SB_MEDIA_TYPE
      LookupKeyFields = 'SB_MEDIA_TYPE_NBR'
      LookupResultField = 'luMediaName'
      KeyFields = 'SB_MEDIA_TYPE_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluServiceName: TStringField
      FieldKind = fkLookup
      FieldName = 'luServiceName'
      LookupDataSet = DM_SB_DELIVERY_METHOD.SB_DELIVERY_METHOD
      LookupKeyFields = 'SB_DELIVERY_METHOD_NBR'
      LookupResultField = 'luServiceName'
      KeyFields = 'SB_DELIVERY_METHOD_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluClNbr: TStringField
      FieldKind = fkLookup
      FieldName = 'luClNbr'
      LookupDataSet = cdTmpCl
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object cdBrowseluClName: TStringField
      FieldKind = fkLookup
      FieldName = 'luClName'
      LookupDataSet = cdTmpCl
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
  end
  object cdTmpCl: TevClientDataSet
    ProviderName = 'TMP_CL_PROV'
    ClientID = -1
    Left = 312
    Top = 128
    object cdTmpClCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Origin = '"TMP_CL"."CL_NBR"'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdTmpClCUSTOM_CLIENT_NUMBER: TStringField
      FieldName = 'CUSTOM_CLIENT_NUMBER'
      Origin = '"TMP_CL"."CUSTOM_CLIENT_NUMBER"'
      Required = True
    end
    object cdTmpClNAME: TStringField
      FieldName = 'NAME'
      Origin = '"TMP_CL"."NAME"'
      Required = True
      Size = 40
    end
  end
end
