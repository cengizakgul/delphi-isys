inherited EDIT_SY_DELIVERY_SERVICE: TEDIT_SY_DELIVERY_SERVICE
  object evDBGrid1: TevDBGrid [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 212
    DisableThemesInTitle = False
    Selected.Strings = (
      'CLASS_NAME'#9'40'#9'Class name'#9'F'
      'NAME'#9'40'#9'Name'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsDetail
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
  end
  object evPanel1: TevPanel [1]
    Left = 0
    Top = 212
    Width = 443
    Height = 65
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object evLabel1: TevLabel
      Left = 16
      Top = 8
      Width = 81
      Height = 13
      Caption = '~Classname'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evLabel2: TevLabel
      Left = 320
      Top = 8
      Width = 44
      Height = 13
      Caption = '~Name'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object evDBComboBox1: TevDBComboBox
      Left = 16
      Top = 24
      Width = 257
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'CLASS_NAME'
      DataSource = wwdsDetail
      DropDownCount = 8
      ItemHeight = 0
      Items.Strings = (
        'Test1'
        'Test2')
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 0
      UnboundDataType = wwDefault
    end
    object evDBEdit1: TevDBEdit
      Left = 320
      Top = 24
      Width = 249
      Height = 21
      DataField = 'NAME'
      DataSource = wwdsDetail
      Picture.PictureMaskFromDataSet = False
      Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
      TabOrder = 1
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 256
    Top = 24
  end
end
