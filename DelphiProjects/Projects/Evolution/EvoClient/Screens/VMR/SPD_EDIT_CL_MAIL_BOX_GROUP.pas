// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CL_MAIL_BOX_GROUP;

interface

uses
  SDataStructure,  wwdbedit, Wwdotdot, Dialogs,
  Wwdbcomb, wwdbdatetimepicker, Mask, DBCtrls, StdCtrls, wwdblook,
  ExtCtrls, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls,
  Classes, SFrameEntry, SVmrClasses, SVmrOptionEditFrame,
   SPD_EDIT_CL_BASE, Buttons, EvContext,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  EvDataAccessComponents, ISDataAccessComponents, SDataDictsystem,
  SDataDictbureau, SDataDictclient, SDataDicttemp, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, isUIwwDBDateTimePicker, isUIDBMemo, isUIwwDBLookupCombo,
  isUIwwDBComboBox, isUIwwDBEdit, ImgList, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEDIT_CL_MAIL_BOX_GROUP = class(TEDIT_CL_BASE)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    edType: TevDBLookupCombo;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    tshtDetails2: TTabSheet;
    evLabel3: TevLabel;
    edMedia: TevDBLookupCombo;
    evPanel4: TevPanel;
    evLabel2: TevLabel;
    evLabel4: TevLabel;
    edSecType: TevDBLookupCombo;
    edSecMedia: TevDBLookupCombo;
    pcPrimarySettings: TevPageControl;
    tsPriService: TTabSheet;
    tsPriMedia: TTabSheet;
    pcSecondarySettings: TevPageControl;
    tsSecService: TTabSheet;
    tsSecMedia: TTabSheet;
    tsPriSbType: TTabSheet;
    tsPriSbMedia: TTabSheet;
    tsSecSbType: TTabSheet;
    tsSecSbMedia: TTabSheet;
    cdTypes: TevClientDataSet;
    cdTypesSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdTypesCLASS_NAME: TStringField;
    cdTypesNAME: TStringField;
    tsDetails: TTabSheet;
    evLabel5: TevLabel;
    evDBEdit1: TevDBEdit;
    evLabel6: TevLabel;
    evDBComboBox1: TevDBComboBox;
    evDBRadioGroup1: TevDBRadioGroup;
    evLabel7: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel8: TevLabel;
    edSyCoverReport: TevDBLookupCombo;
    edSbCoverReport: TevDBLookupCombo;
    evLabel9: TevLabel;
    evLabel10: TevLabel;
    EvDBMemo1: TEvDBMemo;
    evLabel11: TevLabel;
    edUpLinkMailBoxGroup: TevDBLookupCombo;
    evDBTreeView1: TevDBTreeView;
    evLabel12: TevLabel;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evLabel13: TevLabel;
    evDBDateTimePicker2: TevDBDateTimePicker;
    cbDirectEmailVouchers: TevCheckBox;
    cbBlockAutoEmailVouchers: TevCheckBox;
    edVmrReplyEmail: TevEdit;
    evLabel14: TevLabel;
    lbCycledRef: TLabel;
    cbVmrPrinterLocation: TevComboBox;
    lVmrPrinterLocation: TevLabel;
    cbEmailSelfServe: TevCheckBox;
    Label1: TLabel;
   cbIncludeDescInEmails: TevCheckBox;
    edSelfServeEmailText: TevMemo;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure edMediaBeforeDropDown(Sender: TObject);
    procedure edMediaCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure edSecMediaBeforeDropDown(Sender: TObject);
    procedure edSecMediaCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure AdditionalOptionChange(Sender: TObject);
    procedure cbVmrPrinterLocationKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FKeys: array [1..4] of Integer;
    FPrimDeliveryMethodClass,
    FSecDeliveryMethodClass: TVmrDeliveryMethodClass;
    FPrimaryDeliveryMethod: TVmrOptionedType;
    FPrimaryDeliveryMethodFrame: TVmrOptionEditFrame;
    FPrimaryDeliveryMedia: TVmrOptionedType;
    FPrimaryDeliveryMediaFrame: TVmrOptionEditFrame;
    FSecondaryDeliveryMethod: TVmrOptionedType;
    FSecondaryDeliveryMethodFrame: TVmrOptionEditFrame;
    FSecondaryDeliveryMedia: TVmrOptionedType;
    FSecondaryDeliveryMediaFrame: TVmrOptionEditFrame;
    FSbPrimaryDeliveryMethodFrame: TVmrOptionEditFrame;
    FSbPrimaryDeliveryMediaFrame: TVmrOptionEditFrame;
    FSbSecondaryDeliveryMethodFrame: TVmrOptionEditFrame;
    FSbSecondaryDeliveryMediaFrame: TVmrOptionEditFrame;
    function TopLevelBoxNbr(): Variant;
    function BoxesHasCycleRefs:boolean;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses Variants, SPackageEntry, SysUtils, EvTypes, EvUtils, EvConsts,
  SPD_EDIT_Open_BASE, SVmrDeliveryServices, EvBasicUtils, isBaseClasses;

{$R *.DFM}

procedure TEDIT_CL_MAIL_BOX_GROUP.Activate;
var
 aList : IisStringList;
begin
  cdTypes.CreateDataSet;
  cdTypes.LogChanges := False;
  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.First;
  while not DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Eof do
  begin
    cdTypes.AppendRecord([DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['SB_DELIVERY_METHOD_NBR'],
      DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['CLASS_NAME'],
      DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['luServiceName']+ ' - '+
        DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['luMethodName']]);
    DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Next;
  end;
  cdTypes.IndexFieldNames := 'NAME';
  edType.LookupTable := CreateLookupDataset(cdTypes, Self);
  edSecType.LookupTable := CreateLookupDataset(cdTypes, Self);
  edMedia.LookupTable := CreateLookupDataset(DM_SERVICE_BUREAU.SB_MEDIA_TYPE, Self);
  edSecMedia.LookupTable := CreateLookupDataset(DM_SERVICE_BUREAU.SB_MEDIA_TYPE, Self);
  //To make faster
  edSyCoverReport.LookupTable := GetReportsByAncestor('TrwlBaseCoverLetterReport', 'S', Self);
  edSbCoverReport.LookupTable := GetReportsByAncestor('TrwlBaseCoverLetterReport', 'B', Self);
  edUpLinkMailBoxGroup.LookupTable := TevClientDataSet.Create(Self);

  pcPrimarySettings.ActivePageIndex := 0;
  pcSecondarySettings.ActivePageIndex := 0;

  if DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', VmrSbLocation, []) then
    cbVmrPrinterLocation.Items.CommaText := DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString
  else
    cbVmrPrinterLocation.Items.CommaText := '';

  if CheckRemoteVMRActive then
  begin
    aList := GetRemotePrinterList;
    if (cbVmrPrinterLocation.Items.Count > 0) and (aList.count > 0) then
      cbVmrPrinterLocation.Items.CommaText := cbVmrPrinterLocation.Items.CommaText +',' + aList.CommaText
    else if (aList.count > 0) then
      cbVmrPrinterLocation.Items.CommaText := aList.CommaText;
  end;
  wwdsDetail.Enabled := True;

  inherited;
end;

function TEDIT_CL_MAIL_BOX_GROUP.GetInsertControl: TWinControl;
begin
  Result := evDBEdit1;
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
  procedure Process(const o: TVmrOptionedType);
  var
    b1 : boolean;
  begin
    if Assigned(o) and o.ExtraOptions.Active then
    begin
      o.ExtraOptions.Apply;
      //o.ExtraOptions.OptionDataSet.ParentDataSet := TevClientDataSet(wwdsDetail.DataSet);
      b1 := ctx_DataAccess.TempCommitDisable;
      try
        ctx_DataAccess.TempCommitDisable := True;
        TFramePackageTmpl(Owner).AddPostDataSet(o.ExtraOptions.OptionDataSet);
      finally
        ctx_DataAccess.TempCommitDisable := b1;
      end;
    end;
  end;
  procedure ProcessCancel(const o: TVmrOptionedType);
  begin
    if Assigned(o) and o.ExtraOptions.Active then
    begin
      if o.ExtraOptions.State in [dsEdit, dsInsert] then
        o.ExtraOptions.Cancel;
      o.ExtraOptions.CancelUpdates;
    end;
  end;
  Procedure ApplyOption(option:string;field:string;value:variant);
  begin
      if DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Locate('CL_MAIL_BOX_GROUP_NBR;OPTION_TAG',
                                                    VarArrayOf([wwdsDetail.DataSet['CL_MAIL_BOX_GROUP_NBR'],option]),[]) then
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Edit
      else
      begin
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Append;
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.OPTION_TAG.AsString := option;
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.CL_MAIL_BOX_GROUP_NBR.AsInteger := wwdsDetail.DataSet['CL_MAIL_BOX_GROUP_NBR'];
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.FieldByName(field).Value := '';
      end;
      if DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.FieldByName(field).Value = value then
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Cancel
      else
      begin
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.FieldByName(field).Value := value;
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Post;
      end;
  end;
  procedure CheckSetup;
  var
   ds,topbox:TevClientDataSet;
   lMethodMediaTypes: TVmrMediaTypeClassArray;
   aMethod: TVmrDeliveryMethod;
   MT:TVmrMediaType;
   i: integer;
   function FindMediaType(const m: TVmrMediaType): Boolean;
   var
     j: Integer;
   begin
      Result := False;
      for j := 0 to High(lMethodMediaTypes) do
      begin
         if m is lMethodMediaTypes[j] then
         begin
           Result := True;
           Break;
         end;
      end
  end;
  begin
     if DM_CLIENT.CL_MAIL_BOX_GROUP.State = dsBrowse then
        exit;
     if BoxesHasCycleRefs then
       raise EInconsistentData.CreateHelp('Cyclical references in uplinked mailboxes', IDH_ConsistencyViolation);
     ds := TevClientDataSet.Create(self);
     topbox := TevClientDataSet.Create(self);
     ds.Data :=  DM_CLIENT.CL_MAIL_BOX_GROUP.Data;
     // update edited record
     ds.Locate('CL_MAIL_BOX_GROUP_NBR',DM_CLIENT.CL_MAIL_BOX_GROUP['CL_MAIL_BOX_GROUP_NBR'],[]);
     ds.Edit;
     for i:= 0 to ds.Fields.Count-1 do
       if ds.Fields[i].FieldKind = fkData then
         ds[ds.Fields[i].FieldName] := DM_CLIENT.CL_MAIL_BOX_GROUP[ds.Fields[i].FieldName];
     ds.Post;
     topbox.Data :=  ds.Data;
     try
        ds.IndexFieldNames := 'UP_LEVEL_MAIL_BOX_GROUP_NBR;CL_MAIL_BOX_GROUP_NBR';
        topbox.IndexFieldNames := 'UP_LEVEL_MAIL_BOX_GROUP_NBR;CL_MAIL_BOX_GROUP_NBR';
        ds.First;
        while not ds.Eof do
        begin
            topbox.Locate('CL_MAIL_BOX_GROUP_NBR',ds['CL_MAIL_BOX_GROUP_NBR'],[]);
            while topbox['UP_LEVEL_MAIL_BOX_GROUP_NBR'] <> null do
            begin
               topbox.Locate('CL_MAIL_BOX_GROUP_NBR',topbox['UP_LEVEL_MAIL_BOX_GROUP_NBR'],[]);
            end;
            aMethod := InstanceClPrimaryDeliveryMethod(topbox);
            if Assigned(aMethod) then
            begin
              lMethodMediaTypes := aMethod.GetMediaClasses;
              MT := InstanceClPrimaryMedia(ds);
              if not Assigned(MT) then
                raise Exception.Create('Primary Settings:Media type do not assigned for mail box '+ ds.fieldByname('Description').AsString);
              if not FindMediaType(MT) then
                raise Exception.Create('Primary Settings:Media type is incompatible with delivery method for mail box '+ ds.fieldByname('Description').AsString);
            end;
            
            aMethod := InstanceClSecondaryDeliveryMethod(topbox);
            if Assigned(aMethod) then
            begin
              lMethodMediaTypes := aMethod.GetMediaClasses;
              MT := InstanceClSecondaryMedia(ds);
              if not Assigned(MT) then
                 raise Exception.Create('Secondary Settings:Media type do not assigned for mail box '+ ds.fieldByname('Description').AsString);
              if not FindMediaType(MT) then
                 raise Exception.Create('Secondary Settings:Media type is incompatible with delivery method for mail box '+ ds.fieldByname('Description').AsString);
            end;
            ds.Next;
        end;
     finally
       ds.Free;
       topbox.Free;
     end;
  end;

begin
  if Kind = NavOK then
  begin
    DM_CLIENT.CL_MAIL_BOX_GROUP.UpdateRecord;
    CheckSetup;

    cbBlockAutoEmailVouchers.Tag := 1;
    cbDirectEmailVouchers.Tag  := 1;
    edVmrReplyEmail.Tag := 1;
    cbVmrPrinterLocation.Tag :=1;
    cbIncludeDescInEmails.Tag := 1;
    cbEmailSelfServe.Tag := 1;
    edSelfServeEmailText.Tag := 1;

    Process(FPrimaryDeliveryMethod);
    Process(FPrimaryDeliveryMedia);
    Process(FSecondaryDeliveryMethod);
    Process(FSecondaryDeliveryMedia);

    ApplyOption(VmrDirectEmailVouchers,'OPTION_STRING_VALUE',BoolToYN(cbDirectEmailVouchers.Checked));
    ApplyOption(VmrBlockAutoEmailVouchers,'OPTION_STRING_VALUE',BoolToYN(cbBlockAutoEmailVouchers.Checked));
    ApplyOption(VmrReplyEmailAddress,'OPTION_STRING_VALUE',Trim(edVmrReplyEmail.Text));
    ApplyOption(VmrSbLocation,'OPTION_STRING_VALUE',Trim(cbVmrPrinterLocation.Text));

    ApplyOption(VmrEmailSelfServe,'OPTION_STRING_VALUE',BoolToYN(cbEmailSelfServe.Checked));
    ApplyOption(VmrSelfServeEmailText,'OPTION_STRING_VALUE',Trim(edSelfServeEmailText.Lines.Text));
    ApplyOption(VmrIncludeDescriptioninEmails,'OPTION_STRING_VALUE',BoolToYN(cbIncludeDescInEmails.Checked));


    edVmrReplyEmail.Tag := 0;
    cbBlockAutoEmailVouchers.Tag := 0;
    cbDirectEmailVouchers.Tag  := 0;
    cbVmrPrinterLocation.Tag :=0;
    cbIncludeDescInEmails.Tag := 0;
    cbEmailSelfServe.Tag := 0;
    edSelfServeEmailText.Tag := 0;

    TopLevelBoxNbr;

  end
  else if Kind = NavCancel then
  begin
    ProcessCancel(FPrimaryDeliveryMethod);
    ProcessCancel(FPrimaryDeliveryMedia);
    ProcessCancel(FSecondaryDeliveryMethod);
    ProcessCancel(FSecondaryDeliveryMedia);
  end
  else if Kind = NavDelete then
    if EvMessage('Are you sure?', mtConfirmation, mbOKCancel) = mrOK then
    begin
      try
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Close;
        DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Open;

        while DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Locate('CL_MAIL_BOX_GROUP_NBR', DM_CLIENT.CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP_NBR.AsInteger, []) do
          DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Delete;
        CommitData;
        DM_CLIENT.CL_MAIL_BOX_GROUP.Delete;
        CommitData;
        evDBTreeView1.DataSource := nil;
        evDBTreeView1.DataSource := wwdsDetail;
      except
        raise;
      end;
      Handled := True;
    end;
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
  procedure lFreeAndNil(var Obj);
  {var
    Temp: TObject;
    a: TArrayDS;}
  begin
    {Temp := TObject(Obj);
    if Temp is TVmrOptionedType then
    begin
      RemoveDS(TVmrOptionedType(Temp).ExtraOptions.OptionDataSet, FDataSetsToPost);
      RemoveDS(TVmrOptionedType(Temp).ExtraOptions, FDataSetsToPost);
      a := TFramePackageTmpl(Owner).EditDataSets;
      RemoveDS(TVmrOptionedType(Temp).ExtraOptions.OptionDataSet, a);
      RemoveDS(TVmrOptionedType(Temp).ExtraOptions, a);
      TFramePackageTmpl(Owner).EditDataSets := a;
    end;}
    SysUtils.FreeAndNil(Obj);
  end;
  procedure ProcessCreated(const o: TVmrOptionedType; var oCl: TVmrOptionedType;
    var fCl, fSb: TVmrOptionEditFrame;
    const fClParent, fSbParent: TTabSheet);
  {var
    a: TArrayDS;}
  begin
    if Assigned(o) then
    begin
      oCl := o;
      if (oCl.GetExtraOptionEditFrameClass <> nil) then
      begin
        fCl := oCl.GetExtraOptionEditFrameClass.CreateBound(Self, oCl.ExtraOptions, oCl, wwdsDetail);
        fCl.Name := '';
        fCl.Align := alClient;
        fCl.Parent := fClParent;
        {a := TFramePackageTmpl(Owner).EditDataSets;
        AddDS([oCl.ExtraOptions.OptionDataSet, oCl.ExtraOptions], a);
        TFramePackageTmpl(Owner).EditDataSets := a;}
      end;
      if (oCl.GetOptionEditFrameClass <> nil) then
      begin
        fSb := oCl.GetOptionEditFrameClass.CreateBound(Self, oCl.Options, oCl, nil);
        fSb.Name := '';
        fSb.Align := alClient;
        fSb.Parent := fSbParent;
        fSb.Enabled := False;
      end
    end;
  end;
  function GetOptionValue(option:string;field:string;DefValue:Variant):Variant;
  begin
    if DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Locate('CL_MAIL_BOX_GROUP_NBR;OPTION_TAG',
                                                  VarArrayOf([
                                                              wwdsDetail.DataSet['CL_MAIL_BOX_GROUP_NBR'],
                                                              option]),
                                                  []
                                                 ) then
        result := DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.FieldByName(field).Value
    else
        result := DefValue;
  end;
  procedure GetCheckBoxOptionValue(c:TCheckBox;Option:string) ;
  begin
     if (c.Tag = 0) then
     begin
       c.Tag := 1;
       try
          c.Checked := GetOptionValue(Option,'OPTION_STRING_VALUE',GROUP_BOX_NO)=GROUP_BOX_YES;
       finally
         c.Tag := 0;
       end;
     end;
  end;

  procedure GetTextBoxOptionValue(c:TEdit;Option:string);
  begin
     if (c.Tag = 0) then
     begin
       c.Tag := 1;
       try
          c.Text := Trim(ConvertNull(GetOptionValue(Option,'OPTION_STRING_VALUE',''),''));
       finally
         c.Tag := 0;
       end;
     end;
   end;
   procedure GetMemoBoxOptionValue(c:TMemo;Option:string);
   begin
     if (c.Tag = 0) then
     begin
       c.Tag := 1;
       try
          c.Lines.Text := Trim(ConvertNull(GetOptionValue(Option,'OPTION_STRING_VALUE',''),''));
       finally
         c.Tag := 0;
       end;
     end;
   end;
   procedure GetComboBoxOptionValue(c:TevComboBox;Option:string);
   begin
     if (c.Tag = 0) then
     begin
       c.Tag := 1;
       try
          c.ItemIndex := c.Items.IndexOf(ConvertNull(GetOptionValue(Option,'OPTION_STRING_VALUE',''), -1));
       finally
          c.Tag := 0;
       end;
     end;
   end;
var
  v2: Variant;
begin
  inherited;
  if wwdsDetail.Active then
  begin
    if not Assigned(Field) or (Field.FieldName = 'UP_LEVEL_MAIL_BOX_GROUP_NBR')  then
    begin
       try
        if VarIsNull(wwdsDetail.DataSet['UP_LEVEL_MAIL_BOX_GROUP_NBR']) then
           edUpLinkMailBoxGroup.Clear;
        TopLevelBoxNbr;
       except
         exit;
       end;
       lbCycledRef.Caption := '';
    end;
  end;
  if wwdsDetail.Active
     and DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.Active
     and not Assigned(Field)
     and not VarIsNull(wwdsDetail.DataSet['CL_MAIL_BOX_GROUP_NBR'])  then
  begin
     GetCheckBoxOptionValue(cbDirectEmailVouchers,VmrDirectEmailVouchers);
     GetCheckBoxOptionValue(cbBlockAutoEmailVouchers,VmrBlockAutoEmailVouchers);
     GetCheckBoxOptionValue(cbEmailSelfServe,VmrEmailSelfServe);
     GetMemoBoxOptionValue(edSelfServeEmailText,VmrSelfServeEmailText);
     GetTextBoxOptionValue(edVmrReplyEmail,VmrReplyEmailAddress);
     GetComboBoxOptionValue(cbVmrPrinterLocation,VmrSbLocation);
     GetCheckBoxOptionValue(cbIncludeDescInEmails,VmrIncludeDescriptioninEmails);
  end;

  FPrimDeliveryMethodClass := nil;
  FSecDeliveryMethodClass := nil;
  if ((wwdsDetail.Active) and not Assigned(Field))
  or (Assigned(Field) and (
    (Field.FieldName = 'UP_LEVEL_MAIL_BOX_GROUP_NBR') or
    (Field.FieldName = 'SEC_SB_DELIVERY_METHOD_NBR')  or
    (Field.FieldName = 'PRIM_SB_DELIVERY_METHOD_NBR') or
    (Field.FieldName = 'PRIM_SB_MEDIA_TYPE_NBR') or
    (Field.FieldName = 'SEC_SB_MEDIA_TYPE_NBR')

    )) then
  begin
    if wwdsDetail.DataSet.FieldByName('UP_LEVEL_MAIL_BOX_GROUP_NBR').IsNull then
    begin
      FPrimDeliveryMethodClass := TVmrDeliveryMethodClass(FindVmrClass(VarToStr(DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.
        Lookup('SB_DELIVERY_METHOD_NBR', wwdsDetail.DataSet['PRIM_SB_DELIVERY_METHOD_NBR'], 'CLASS_NAME'))));
      FSecDeliveryMethodClass :=
         TVmrDeliveryMethodClass(
               FindVmrClass(
                   VarToStr(
                      DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Lookup(
                               'SB_DELIVERY_METHOD_NBR',
                               wwdsDetail.DataSet['SEC_SB_DELIVERY_METHOD_NBR'],
                               'CLASS_NAME'
                      )
                   )
               )
         );
      {evMessage(VarToStr(DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Lookup(
                               'SB_DELIVERY_METHOD_NBR',
                               wwdsDetail.DataSet['SEC_SB_DELIVERY_METHOD_NBR'],
                               'CLASS_NAME'
                      )));
      }
      edType.ReadOnly := False;
      edType.RefreshDisplay;
      edSecType.ReadOnly := False;
      edSecType.RefreshDisplay;
    end
    else
    begin
      v2 := ClonedLookup(TevClientDataSet(wwdsDetail.DataSet), 'CL_MAIL_BOX_GROUP_NBR',
        TopLevelBoxNbr, 'PRIM_SB_DELIVERY_METHOD_NBR;SEC_SB_DELIVERY_METHOD_NBR');
      FPrimDeliveryMethodClass := TVmrDeliveryMethodClass(FindVmrClass(VarToStr(DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.
        Lookup('SB_DELIVERY_METHOD_NBR', v2[0], 'CLASS_NAME'))));
      FSecDeliveryMethodClass := TVmrDeliveryMethodClass(FindVmrClass(VarToStr(DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.
        Lookup('SB_DELIVERY_METHOD_NBR', v2[1], 'CLASS_NAME'))));
      edType.ReadOnly := True;
      edType.Text := 'You can not change delivery method if the box is a part of another box';
      edSecType.ReadOnly := True;
      edSecType.Text := edType.Text;
    end;
  end
  else
  begin
    edType.ReadOnly := False;
    edType.RefreshDisplay;
    edSecType.ReadOnly := False;
    edSecType.RefreshDisplay;
  end;

  if (not Assigned(Field)
  and ((not Assigned(FPrimaryDeliveryMethod) and not edType.ReadOnly) or (Tag <> wwdsDetail.KeyValue)
    or (FKeys[1] <> StrToIntDef(edType.LookupValue, 0)) or (FKeys[2] <> StrToIntDef(edMedia.LookupValue, 0))
    or (FKeys[3] <> StrToIntDef(edSecType.LookupValue, 0)) or (FKeys[4] <> StrToIntDef(edSecMedia.LookupValue, 0)))
  {and (wwdsDetail.State = dsBrowse)})
  or (Assigned(Field)
    and ((Field.FieldName = edType.DataField) or (Field.FieldName = edMedia.DataField) or
      (Field.FieldName = edSecType.DataField) or (Field.FieldName = edSecMedia.DataField))
    ) then
  begin
    if not Assigned(Field) or (Field.FieldName = edType.DataField) then
    begin
      lFreeAndNil(FPrimaryDeliveryMethodFrame);
      lFreeAndNil(FSbPrimaryDeliveryMethodFrame);
      lFreeAndNil(FPrimaryDeliveryMethod);
    end;
    if not Assigned(Field) or (Field.FieldName = edType.DataField) or (Field.FieldName = edMedia.DataField) then
    begin
      lFreeAndNil(FPrimaryDeliveryMediaFrame);
      lFreeAndNil(FSbPrimaryDeliveryMediaFrame);
      lFreeAndNil(FPrimaryDeliveryMedia);
    end;
    if not Assigned(Field) or (Field.FieldName = edSecType.DataField) then
    begin
      lFreeAndNil(FSecondaryDeliveryMethodFrame);
      lFreeAndNil(FSbSecondaryDeliveryMethodFrame);
      lFreeAndNil(FSecondaryDeliveryMethod);
    end;
    if not Assigned(Field) or (Field.FieldName = edSecType.DataField) or (Field.FieldName = edSecMedia.DataField) then
    begin
      lFreeAndNil(FSecondaryDeliveryMediaFrame);
      lFreeAndNil(FSbSecondaryDeliveryMediaFrame);
      lFreeAndNil(FSecondaryDeliveryMedia);
    end;
    FKeys[1] := StrToIntDef(edType.LookupValue, 0);
    FKeys[2] := StrToIntDef(edMedia.LookupValue, 0);
    FKeys[3] := StrToIntDef(edSecType.LookupValue, 0);
    FKeys[4] := StrToIntDef(edSecMedia.LookupValue, 0);
    if (wwdsDetail.KeyValue <> 0) then
    begin
      Tag := wwdsDetail.KeyValue;
      if not edType.ReadOnly then
        ProcessCreated(InstanceClPrimaryDeliveryMethod(TevClientDataSet(wwdsDetail.DataSet)),
          FPrimaryDeliveryMethod, FPrimaryDeliveryMethodFrame,
          FSbPrimaryDeliveryMethodFrame, tsPriService, tsPriSbType);
      if Assigned(FPrimDeliveryMethodClass) then
      begin
        if CheckIfListed(TClassArray(FPrimDeliveryMethodClass.GetMediaClasses),
          FindVmrClass(ConvertNull(edMedia.LookupTable.Lookup(edMedia.LookupField,
          edMedia.DataSource.DataSet[edMedia.DataField], 'CLASS_NAME'), ''))) then
            ProcessCreated(InstanceClPrimaryMedia(TevClientDataSet(wwdsDetail.DataSet)),
             FPrimaryDeliveryMedia, FPrimaryDeliveryMediaFrame,
              FSbPrimaryDeliveryMediaFrame, tsPriMedia, tsPriSbMedia)
        else
        if not VarIsNull(edMedia.LookupTable.Lookup(edMedia.LookupField,
          edMedia.DataSource.DataSet[edMedia.DataField], 'CLASS_NAME'))
        and (edMedia.DataSource.State in [dsEdit, dsInsert]) then
        begin
          edMedia.DataSource.DataSet[edMedia.DataField] := Null;
          edMedia.Text := 'Choice is not valid. Choose again';
        end;
      end;
      pcPrimarySettings.SetTabVisible(tsPriService, Assigned(FPrimaryDeliveryMethodFrame));
      pcPrimarySettings.SetTabVisible(tsPriSbType, Assigned(FSbPrimaryDeliveryMethodFrame));
      pcPrimarySettings.SetTabVisible(tsPriMedia, Assigned(FPrimaryDeliveryMediaFrame));
      pcPrimarySettings.SetTabVisible(tsPriSbMedia, Assigned(FSbPrimaryDeliveryMediaFrame));
      if not edSecType.ReadOnly then
        ProcessCreated(InstanceClSecondaryDeliveryMethod(TevClientDataSet(wwdsDetail.DataSet)),
          FSecondaryDeliveryMethod, FSecondaryDeliveryMethodFrame,
          FSbSecondaryDeliveryMethodFrame, tsSecService, tsSecSbType);
      if Assigned(FSecDeliveryMethodClass) then
        if CheckIfListed(TClassArray(FSecDeliveryMethodClass.GetMediaClasses),
          FindVmrClass(ConvertNull(edSecMedia.LookupTable.Lookup(edSecMedia.LookupField,
          edSecMedia.DataSource.DataSet[edSecMedia.DataField], 'CLASS_NAME'), ''))) then
            ProcessCreated(InstanceClSecondaryMedia(TevClientDataSet(wwdsDetail.DataSet)),
              FSecondaryDeliveryMedia, FSecondaryDeliveryMediaFrame,
              FSbSecondaryDeliveryMediaFrame, tsSecMedia, tsSecSbMedia)
        else
        if not VarIsNull(edSecMedia.LookupTable.Lookup(edSecMedia.LookupField,
          edSecMedia.DataSource.DataSet[edSecMedia.DataField], 'CLASS_NAME'))
        and (edSecMedia.DataSource.State in [dsEdit, dsInsert]) then
        begin
          edSecMedia.DataSource.DataSet[edSecMedia.DataField] := Null;
          edSecMedia.Text := 'Choice is not valid. Choose again';
        end;
    end;
    try
      pcSecondarySettings.SetTabVisible(tsSecService, Assigned(FSecondaryDeliveryMethodFrame));
      pcSecondarySettings.SetTabVisible(tsSecSbType, Assigned(FSbSecondaryDeliveryMethodFrame));
      pcSecondarySettings.SetTabVisible(tsSecMedia, Assigned(FSecondaryDeliveryMediaFrame));
      pcSecondarySettings.SetTabVisible(tsSecSbMedia, Assigned(FSbSecondaryDeliveryMediaFrame));
    except
    end;
  end;
  if (wwdsDetail.State in [dsEdit, dsInsert])
  and Assigned(Field)
  and (Field.FieldName = 'UP_LEVEL_MAIL_BOX_GROUP_NBR')
  and not Field.IsNull then
    wwdsDetail.DataSet['PRIM_SB_DELIVERY_METHOD_NBR;SEC_SB_DELIVERY_METHOD_NBR'] := ClonedLookup(TevClientDataSet(wwdsDetail.DataSet),
      'CL_MAIL_BOX_GROUP_NBR', TopLevelBoxNbr, 'PRIM_SB_DELIVERY_METHOD_NBR;SEC_SB_DELIVERY_METHOD_NBR');
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION, EditDataSets, '');
  AddDS([DM_SERVICE_BUREAU.SB_OPTION,DM_SERVICE_BUREAU.SB_DELIVERY_SERVICE, DM_SERVICE_BUREAU.SB_DELIVERY_METHOD,
    DM_SYSTEM_MISC.SY_DELIVERY_SERVICE, DM_SYSTEM_MISC.SY_DELIVERY_METHOD,
    DM_SERVICE_BUREAU.SB_MEDIA_TYPE, DM_SYSTEM_MISC.SY_MEDIA_TYPE{,
    DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS, DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS}], SupportDataSets);
end;

function TEDIT_CL_MAIL_BOX_GROUP.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_CLIENT.CL_MAIL_BOX_GROUP;
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;

  if TwwDataSource(Sender).State = dsBrowse then
    TevClientDataSet(edUpLinkMailBoxGroup.LookupTable).Data := TevClientDataSet(TwwDataSource(Sender).DataSet).Data
  else if TwwDataSource(Sender).State = dsInactive then
    Tag := 0;
  edUpLinkMailBoxGroup.Enabled := TwwDataSource(Sender).State <> dsInsert;


end;

procedure TEDIT_CL_MAIL_BOX_GROUP.wwdsDetailUpdateData(Sender: TObject);
begin
  inherited;
  if VarIsNull(wwdsDetail.DataSet['PRIM_SB_DELIVERY_METHOD_NBR']) then
    raise EInconsistentData.CreateHelp('Primary Delivery Method has to be chosen', IDH_ConsistencyViolation);
  if VarIsNull(wwdsDetail.DataSet['PRIM_SB_MEDIA_TYPE_NBR']) then
    raise EInconsistentData.CreateHelp('Primary Settings:Media type is incompatible with delivery method', IDH_ConsistencyViolation);
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.edMediaBeforeDropDown(Sender: TObject);
var
  a: TVmrMediaTypeClassArray;
  s: string;
  i: Integer;
begin
  inherited;
  SetLength(a, 0);
  if Assigned(FPrimDeliveryMethodClass) then
  begin
    a := FPrimDeliveryMethodClass.GetMediaClasses;
    for i := 0 to High(a) do
    begin
      if s <> '' then
        s := s+ ' or ';
      s := s+ '(CLASS_NAME='+ QuotedStr(a[i].ClassName)+ ')';
    end;
    edMedia.LookupTable.Filter := s;
    edMedia.LookupTable.Filtered := True;
  end
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.edMediaCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  edMedia.LookupTable.Filtered := False;
end;

function TEDIT_CL_MAIL_BOX_GROUP.BoxesHasCycleRefs: boolean;
 var
   cd: TevClientDataSet;
   RefLog: array of Integer;
   i: Integer;
begin
   Result:= false;
   if wwdsDetail.DataSet.State = dsInsert then
     exit;
   SetLength(RefLog, 0);
   cd := TevClientDataSet.Create(nil);
   try
      SetLength(RefLog, 0);
//      cd.CloneCursor(TevClientDataSet(wwdsDetail.DataSet), True);
      cd.Data := TevClientDataSet(wwdsDetail.DataSet).Data;
      cd.Locate('CL_MAIL_BOX_GROUP_NBR',wwdsDetail.DataSet['CL_MAIL_BOX_GROUP_NBR'] , []);
      cd.Edit;
      cd['UP_LEVEL_MAIL_BOX_GROUP_NBR'] := wwdsDetail.DataSet['UP_LEVEL_MAIL_BOX_GROUP_NBR'];
      cd.Post;
      while not VarIsNull(cd['UP_LEVEL_MAIL_BOX_GROUP_NBR']) do
      begin
         for i := 0 to High(RefLog) do
           if RefLog[i] = cd['UP_LEVEL_MAIL_BOX_GROUP_NBR'] then
           begin
              Result := true;
              Exit;
          end;
          SetLength(RefLog, Length(RefLog)+1);
          RefLog[High(RefLog)] := cd['UP_LEVEL_MAIL_BOX_GROUP_NBR'];
          Assert(cd.Locate('CL_MAIL_BOX_GROUP_NBR', cd['UP_LEVEL_MAIL_BOX_GROUP_NBR'], []));
      end;
   finally
     cd.Free;
   end;
end;


function TEDIT_CL_MAIL_BOX_GROUP.TopLevelBoxNbr: Variant;
var
  cd: TevClientDataSet;
  RefLog: array of Integer;
  i: Integer;
begin

  Result := wwdsDetail.DataSet['UP_LEVEL_MAIL_BOX_GROUP_NBR'];
  if not VarIsNull(Result) then
  begin
    //ShowMessage('TopLevelBoxNbr:Result =' +wwdsDetail.DataSet.FieldByName('UP_LEVEL_MAIL_BOX_GROUP_NBR').AsString);
    SetLength(RefLog, 0);
    cd := TevClientDataSet.Create(nil);
    try
      cd.Data :=  TevClientDataSet(wwdsDetail.DataSet).Data;
      Assert(cd.Locate('CL_MAIL_BOX_GROUP_NBR',wwdsDetail.DataSet['CL_MAIL_BOX_GROUP_NBR'] , []));
      cd.Edit;
      cd['UP_LEVEL_MAIL_BOX_GROUP_NBR'] := Result;
      cd.Post;
      while True do
      begin
        for i := 0 to High(RefLog) do
          if RefLog[i] = Result then
          begin
            begin
            lbCycledRef.Caption := 'Cyclical references in uplinked mailboxes.';
            raise EInconsistentData.CreateHelp('Cyclical references in uplinked mailboxes', IDH_ConsistencyViolation);
            end;
          end;
        SetLength(RefLog, Length(RefLog)+1);
        RefLog[High(RefLog)] := Result;
        Assert(cd.Locate('CL_MAIL_BOX_GROUP_NBR', Result, []));
        if VarIsNull(cd['UP_LEVEL_MAIL_BOX_GROUP_NBR']) then
          Break
        else
          Result := cd['UP_LEVEL_MAIL_BOX_GROUP_NBR'];
      end;
    finally
      cd.Free;
    end;
  end;
end;


procedure TEDIT_CL_MAIL_BOX_GROUP.edSecMediaBeforeDropDown(
  Sender: TObject);
var
  a: TVmrMediaTypeClassArray;
  s: string;
  i: Integer;
begin
  inherited;
  SetLength(a, 0);
  if Assigned(FSecDeliveryMethodClass) then
  begin
    a := FSecDeliveryMethodClass.GetMediaClasses;
    for i := 0 to High(a) do
    begin
      if s <> '' then
        s := s+ ' or ';
      s := s+ '(CLASS_NAME='+ QuotedStr(a[i].ClassName)+ ')';
    end;
    edSecMedia.LookupTable.Filter := s;
    edSecMedia.LookupTable.Filtered := True;
  end
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.edSecMediaCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  edSecMedia.LookupTable.Filtered := False;
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.AdditionalOptionChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.Active and not VarIsNull(wwdsDetail.DataSet['CL_MAIL_BOX_GROUP_NBR']) and ((Sender as TComponent).Tag = 0) then
  begin
   (Sender as TComponent).Tag := 1;
    wwdsDetail.DataSet.Edit;
   (Sender as TComponent).Tag := 0;
  end;
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  inherited;
  AddDS(DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION, aDS);
end;

procedure TEDIT_CL_MAIL_BOX_GROUP.cbVmrPrinterLocationKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key in [46,8] then
  begin
    cbVmrPrinterLocation.ItemIndex := -1;
    AdditionalOptionChange(cbVmrPrinterLocation);
  end;
end;




initialization
  RegisterClass(TEDIT_CL_MAIL_BOX_GROUP);

end.
