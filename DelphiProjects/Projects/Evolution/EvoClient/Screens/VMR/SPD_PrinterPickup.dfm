inherited PrinterPickup: TPrinterPickup
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 329
    Width = 435
    Height = 8
    Cursor = crVSplit
    Align = alTop
    Beveled = True
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 0
    Width = 435
    Height = 329
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 0
    object evLabel1: TevLabel
      Left = 8
      Top = 16
      Width = 183
      Height = 13
      Caption = 'Scan bar code or enter code manually:'
    end
    object bCheck: TevBitBtn
      Left = 224
      Top = 32
      Width = 75
      Height = 25
      Caption = 'Check'
      TabOrder = 1
      OnClick = bCheckClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
        DDDDDFFFFFFFFFFDDDDD8CCCCCCCCCC8DDDDF8888888888FDDDDCCCCCCCCCCCC
        DDDD888888888888DDDDCCFFFFFFCCCCFFFF88FFFFFF8888DDDDCCCCCCCCCCCC
        FFFF888888888888DDDDCCCCFFFFFCCCFFFF8888FFFFF888DDDDCCCCCCCCCCCC
        FFFF888888888888DDDDCCCCCCFFFFCCFFFF888888FFFF88DDDDCCCCCCCCCCCC
        FFFF888888888888DDDDDCCCCCCCCCCFFFFFD8888888888DDDDDDDFFFFFFFFFF
        FFFFDDDDDDDDDDDDDDDDDDFF08070708070FDDDD88D8D8D88D8DDDFF08070708
        070FDDDD88D8D8D88D8DDDFF08070708070FDDDD88D8D8D88D8DDDFF08070708
        070FDDDD88D8D8D88D8DDDFFFFFFFFFFFFFFDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object edBarcode: TevEdit
      Left = 8
      Top = 32
      Width = 193
      Height = 21
      TabOrder = 0
      OnKeyPress = edBarcodeKeyPress
    end
    object mMessages: TevMemo
      Left = 1
      Top = 62
      Width = 433
      Height = 266
      TabStop = False
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
    end
    object btnFillGrid: TButton
      Left = 304
      Top = 32
      Width = 75
      Height = 25
      Caption = 'FillGrid'
      TabOrder = 3
      Visible = False
      OnClick = btnFillGridClick
    end
  end
  object PP: TStringGrid [2]
    Left = 0
    Top = 337
    Width = 435
    Height = 159
    Align = alClient
    ColCount = 1
    DefaultColWidth = 70
    DefaultDrawing = False
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSizing, goColSizing]
    TabOrder = 1
    OnDblClick = PPDblClick
    OnDrawCell = PPDrawCell
  end
  inherited wwdsMaster: TevDataSource
    Left = 341
    Top = 10
  end
  inherited wwdsDetail: TevDataSource
    Left = 398
    Top = 10
  end
  inherited wwdsList: TevDataSource
    Left = 290
    Top = 10
  end
  object DataSource1: TDataSource
    Left = 352
    Top = 272
  end
end
