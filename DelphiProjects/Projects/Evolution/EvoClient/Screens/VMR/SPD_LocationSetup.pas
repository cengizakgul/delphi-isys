// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_LocationSetup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SDataStructure, DB, Wwdatsrc, EvTypes,
   wwdbedit, StdCtrls, Mask, Wwdotdot, Wwdbcomb, ExtCtrls,
  Grids, Wwdbigrd, Wwdbgrid, SFrameEntry, wwdblook, ISBasicClasses,
  kbmMemTable, ISKbmMemDataSet, Buttons, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions,EvContext,EvMainboard, ComCtrls,
  EvUIUtils, EvConsts, EvUIComponents, EvClientDataSet, isUIwwDBEdit,
  isUIEdit, LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TVmrPrinterLocationSetup = class(TFrameEntry)
    ScrollBox1: TScrollBox;
    evDataSource1: TevDataSource;
    evClientDataSet1: TevClientDataSet;
    evClientDataSet1LOCATION: TStringField;
    evPageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    evDBGrid1: TevDBGrid;
    evDBGrid2: TevDBGrid;
    Panel1: TPanel;
    SpeedButton2: TevSpeedButton;
    SpeedButton1: TevSpeedButton;
    Panel2: TPanel;
    evSpeedButton1: TevSpeedButton;
    evSpeedButton2: TevSpeedButton;
    evDataSource2: TevDataSource;
    evClientDataSet2: TevClientDataSet;
    evClientDataSet2Host: TStringField;
    evClientDataSet2From: TStringField;
    evClientDataSet2User: TStringField;
    evClientDataSet2Pass: TStringField;
    GroupBox1: TGroupBox;
    evLabel2: TevLabel;
    edFrom: TevEdit;
    evClientDataSet2Port: TIntegerField;
    evLabel1: TevLabel;
    edHost: TevEdit;
    evLabel3: TevLabel;
    edUser: TevEdit;
    evLabel4: TevLabel;
    edPass: TevEdit;
    tsGeneralSetup: TTabSheet;
    cbBlockVMR: TCheckBox;
    evPassword: TevDBEdit;
    cbRemoteVMR: TCheckBox;
    tsRemotePrinterLocations: TTabSheet;
    Panel3: TPanel;
    evSpeedButton3: TevSpeedButton;
    evSpeedButton4: TevSpeedButton;
    evDBGrid3: TevDBGrid;
    evDataSource3: TevDataSource;
    evClientDataSet3: TevClientDataSet;
    RemoteLocationField: TStringField;
    BtnLocationCleanUp: TevSpeedButton;
    BtnRemoteLocationCleanUp: TevSpeedButton;
    procedure evClientDataSet1AfterDelete(DataSet: TDataSet);
    procedure edHostChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure evClientDataSet1AfterInsert(DataSet: TDataSet);
    procedure evClientDataSet1AfterEdit(DataSet: TDataSet);
    procedure evClientDataSet1BeforePost(DataSet: TDataSet);
    procedure evDBEdit3Change(Sender: TObject);
    procedure evSpeedButton2Click(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure evClientDataSet2AfterDelete(DataSet: TDataSet);
    procedure evClientDataSet2AfterEdit(DataSet: TDataSet);
    procedure evClientDataSet2AfterInsert(DataSet: TDataSet);
    procedure evSpeedButton4Click(Sender: TObject);
    procedure evClientDataSet3AfterDelete(DataSet: TDataSet);
    procedure evClientDataSet3AfterEdit(DataSet: TDataSet);
    procedure evClientDataSet3AfterInsert(DataSet: TDataSet);
    procedure evClientDataSet3BeforePost(DataSet: TDataSet);
    procedure evPageControl1Change(Sender: TObject);
    procedure evSpeedButton3Click(Sender: TObject);
    procedure BtnLocationCleanUpClick(Sender: TObject);
    procedure BtnRemoteLocationCleanUpClick(Sender: TObject);
  private
    { Private declarations }
    IsLocationDeleted : boolean;
    IsRemoteLocationDeleted : boolean;
    FDelLocation : string;
    procedure SaveOption(op_n:string;op_v:string);
    procedure LoadLocations;
    procedure SaveLocations;
    procedure LoadEmailSetup;
    procedure SaveEmailSetup;
    procedure LoadCommonOptions;
    procedure SaveCommonOptions;
    procedure LoadRemoteLocations;
    procedure SaveRemoteLocations;
    procedure MarkChanged;
    procedure DoLoadLocations(cs:TevClientDataSet; const optiontag: string );
    procedure DoSaveLocations(cs:TevClientDataSet; const optiontag: string );
    procedure DoCleanUpLocations( cs:TevClientDataSet; const optiontag: string );
  public
    { Public declarations }
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

uses EvUtils, EvBasicUtils, StrUtils, Math, SPackageEntry,
  EvCommonInterfaces, EvDataset, EvStreamUtils, IsBasicUtils, isBaseClasses,
  SDataDictclient;

{$R *.dfm}

{ TLocationSetup }

procedure TVmrPrinterLocationSetup.Activate;
begin
  inherited;
  IsLocationDeleted := False;
  IsRemoteLocationDeleted := False;
  LoadLocations;
  LoadEmailSetup;
  LoadCommonOptions;
  (Owner as TFramePackageTmpl).SetButton(NavHistory, False);

  tsRemotePrinterLocations.TabVisible := cbRemoteVMR.Checked;
  LoadRemoteLocations;
end;

procedure TVmrPrinterLocationSetup.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  Handled := True;
  if Kind = navOk then
  begin
    SaveLocations;
    SaveEmailSetup;
    SaveCommonOptions;
    if cbRemoteVMR.Checked then
      SaveRemoteLocations;
    tsRemotePrinterLocations.TabVisible := cbRemoteVMR.Checked;
  end
  else
  if (Kind = navCancel)
  or (Kind = navRefresh) then
  begin
    LoadLocations;
    LoadEmailSetup;
    LoadCommonOptions;
    if cbRemoteVMR.Checked then
      LoadRemoteLocations;
  end
  else
    Assert(False);
  (Owner as TFramePackageTmpl).SetButton(NavOK, False);
  (Owner as TFramePackageTmpl).SetButton(NavCancel, False);
  (Owner as TFramePackageTmpl).SetButton(NavHistory, False);
end;

procedure TVmrPrinterLocationSetup.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_OPTION, SupportDataSets, '');
end;

procedure TVmrPrinterLocationSetup.evClientDataSet1AfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  if evClientDataSet1.Tag = 0 then
  begin
    MarkChanged;
    IsLocationDeleted := True;
  end;
end;

procedure TVmrPrinterLocationSetup.SaveLocations;
begin
  evClientDataSet1.CheckBrowseMode;
  if ((evClientDataSet1.Tag = 0) and (evClientDataSet1.ChangeCount > 0)) then
  begin
    DoSaveLocations (evClientDataSet1, VmrSbLocation);
    evClientDataSet1.MergeChangeLog;
  end;
end;

//SB_OPTION: VmrSbLocation, VmrRemoteLocation
//CL_MAIL_BOX_GROUP_OPTION: VmrSbLocation
//SB_MAIL_BOX_OPTION: VmrSbLocation

procedure TVmrPrinterLocationSetup.DoSaveLocations( cs:TevClientDataSet; const optiontag: string );
var
  slist: tStringlist;
  OldLocation, NewLocation, Buildsql : string;
  Update_cancel: boolean;

  function BuildCondition(const aLoc:string) : string;
  begin
    Result := '';

    if AnsiSametext( optiontag, VmrSbLocation ) then
      Result := Result + ' and (TRIM(OPTION_STRING_VALUE) not starting with ''@'') '
    else
      Result := Result + ' and (TRIM(OPTION_STRING_VALUE) starting with ''@'') ';

    Result := Result + ' and  Upper(TRIM(OPTION_STRING_VALUE)) = '''+aLoc+'''';
  end;

  procedure DELETERemoteLocationPrinters (aOld : string);
  begin
    if (Length(aOld)> 0) and (aOld[1] = '@') then
    begin
      ctx_DBAccess.ExecQuery('Delete from SB_OPTION WHERE OPTION_TAG starting with '''+aOld+'|''', nil,nil);
    end;
  end;

  procedure DELETELocationPrinters (aOld,aNew: string);
  begin
    ctx_DBAccess.ExecQuery(' Update SB_OPTION SET OPTION_STRING_VALUE = '''+aNew+''' WHERE ( (OPTION_TAG like ''PRINTER_LOCATION'') or (OPTION_TAG like ''PRINTER__LOCATION'')) '+ Buildsql, nil,nil);
  end;

  procedure DeleteCLFillerLocation(aOld,aNew: string);
  var
    cd: TevClientDataSet;
    sf: string;
  begin
    cd := TevClientDataSet.Create(nil);
    try
        cd.Close;
        cd.ProviderName := DM_CLIENT.CL.ProviderName;
        cd.DataRequired('');
        cd.First;
        sf := Uppercase(Trim(Copy(cd.FieldByName('FILLER').AsString, 1, 40)));
        if ( ( AnsiSametext( optiontag, VmrSbLocation )
               and ((Length(sf) > 0) and (sf[1] <> '@'))
               and AnsiSameText(sf, aOld)
              )
            or ( AnsiSametext( optiontag, VmrRemoteLocation )
                 and ((Length(sf) > 0) and (sf[1] = '@'))
                 and AnsiSameText(sf, aOld)
              ) )then
        begin
          cd.Edit;
          cd.FieldByName('FILLER').AsString := PutIntoFiller(cd.FieldByName('FILLER').AsString, aNew, 1, 40);
          cd.Post;
        end;
        ctx_DataAccess.PostDataSets([cd]);
    finally
      cd.Free;
    end;
  end;

  procedure DeleteChildRecords (aOld, aNew: string);
  begin
    BuildSql := BuildCondition(aOld);
    DM_TEMPORARY.TMP_CL.Close;
    ctx_DataAccess.OpenDataSets([DM_TEMPORARY.TMP_CL]);
    DM_TEMPORARY.TMP_CL.First;
    while not DM_TEMPORARY.TMP_CL.Eof do
    begin
      if ConvertNull(DM_TEMPORARY.TMP_CL.FieldValues['CL_NBR'],0) <= 0 then
         continue;
      ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CL.FieldValues['CL_NBR']);

      ctx_DataAccess.StartNestedTransaction([dbtClient]);
      try
        DeleteCLFillerLocation(aOld, aNew);
        ctx_DBAccess.ExecQuery('Update cl_mail_box_group_Option Set Option_String_Value = '''+aNew+''' where (OPTION_TAG = ''VMR_PRINT_LOCATIONS'')and(Option_String_Value is not null)and(Option_String_Value <> '''') '+Buildsql, nil, nil);
        ctx_DataAccess.CommitNestedTransaction;
      except
        ctx_DataAccess.RollbackNestedTransaction;
        raise
      end;

      DM_TEMPORARY.TMP_CL.next;
    end;
    ctx_DataAccess.OpenClient(0);
    DM_TEMPORARY.TMP_CL.Close;

    ctx_DataAccess.StartNestedTransaction([dbtBureau]);
    try
      ctx_DBAccess.ExecQuery('UPDATE SB_MAIL_BOX_OPTION SET Option_String_Value = '''+aNew+''' where (OPTION_TAG = ''VMR_PRINT_LOCATIONS'')and(Option_String_Value is not null)and(Option_String_Value <> '''') '+Buildsql, nil,nil);
      if AnsiSametext( optiontag, VmrSbLocation ) then
        DELETELocationPrinters(aOld, aNew)
      else
        DELETERemoteLocationPrinters(aOld);

      ctx_DataAccess.CommitNestedTransaction;
    except
      ctx_DataAccess.RollbackNestedTransaction;
      raise
    end;
  end;

begin
  Update_Cancel:= False;
  slist := cs.GetFieldValueList('LOCATION', True);
  slist.Sorted := True;
  slist.Duplicates := dupIgnore;
  slist.CommaText := Uppercase(slist.CommaText);

  try
      ctx_StartWait('Please wait...');
      try
        //Delete
        if IsLocationDeleted  or IsRemoteLocationDeleted then
        begin
          FDelLocation := UpperCase(FDelLocation);
          DeleteChildRecords(FDelLocation,'');
          FDelLocation := '';
          IsLocationDeleted := False;
          IsRemoteLocationDeleted := False;
        end
        else begin //Update
          OldLocation :=  UpperCase(cs.FieldByName('LOCATION').OldValue);
          NewLocation :=  UpperCase(cs.FieldByName('LOCATION').Value);

          if Not AnsiSameText(OldLocation,NewLocation) then
          begin
            // Update
            if EvMessage(format('It will be updated from "%s" to "%s" location '#13 +
                         '%sDo you want to continue?', [OldLocation, NewLocation, ifthen(NewLocation[1]='@', 'You have to update printer list from remote VMR Application#13 ', '')]),
                          mtConfirmation, mbOKCancel, mbCancel) = mrOK then
            begin
              DeleteChildRecords(OldLocation,NewLocation);
            end
            else
              Update_Cancel := True;
          end;

        end;

        if not Update_Cancel then
        begin
          //Add, Update, Delete
          if DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', optiontag, []) then
            DM_SERVICE_BUREAU.SB_OPTION.Edit
          else
          begin
            DM_SERVICE_BUREAU.SB_OPTION.Append;
            DM_SERVICE_BUREAU.SB_OPTION.OPTION_TAG.AsString := optiontag;
          end;

          DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString := slist.Commatext;
          DM_SERVICE_BUREAU.SB_OPTION.Post;
          ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_OPTION]);
        end;

      finally
        if AnsiSametext( optiontag, VmrSbLocation ) then
          LoadLocations
        else
          LoadRemoteLocations;
        ctx_EndWait;
      end;
  finally
    slist.free;
  end;
end;

function TVmrPrinterLocationSetup.GetDefaultDataSet: TevClientDataSet;
begin
  Result := nil;
end;

procedure TVmrPrinterLocationSetup.DoLoadLocations(cs:TevClientDataSet; const optiontag: string );
var
  i,cnt: Integer;
  l: TStringList;
  nlist: IisStringList;
  Options: IevDataSet;
begin
  l := TStringList.Create;
  cs.Tag := 1;
  cs.DisableControls;
  try
    cs.Close;
    cs.CreateDataSet;

    if DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', optiontag, []) then
    begin
      l.CommaText := DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString;
    end;

    if optiontag = VmrRemoteLocation then
    begin
      cnt := l.count;
      Options := GetSBOption('@%|0LOCATION');
      nlist := Options.GetColumnValues('OPTION_STRING_VALUE');

      for i := 0 to nlist.count-1 do
        if l.IndexOf(nlist[i]) < 0 then
          l.CommaText := l.CommaText + ifthen(l.count=0,'',',')+nlist[i];

      if l.count > cnt then
      begin
        if DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', optiontag, []) then
          DM_SERVICE_BUREAU.SB_OPTION.Edit
        else
        begin
          DM_SERVICE_BUREAU.SB_OPTION.Append;
          DM_SERVICE_BUREAU.SB_OPTION.OPTION_TAG.AsString := optiontag;
        end;
        DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString := l.CommaText;
        DM_SERVICE_BUREAU.SB_OPTION.Post;
        ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_OPTION]);
      end;
    end;

    for i := 0 to l.Count-1 do
      cs.AppendRecord([l[i]]);
    cs.MergeChangeLog;
    cs.First;
  finally
    cs.EnableControls;
    cs.Tag := 0;
    l.Free;
  end;
end;

procedure TVmrPrinterLocationSetup.LoadLocations;
begin
  DoLoadLocations(evClientDataSet1,VmrSbLocation);
end;

procedure TVmrPrinterLocationSetup.LoadEmailSetup;
var
  i: Integer;
  l: TStringList;
begin
  edHost.Tag := 1;
  try
    edHost.Text := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', SmtpHostTag, 'OPTION_STRING_VALUE'), '');
    edFrom.Text := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', VmrSmtpFromTag, 'OPTION_STRING_VALUE'), '');
    edUser.Text := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', VmrSmtpUserTag, 'OPTION_STRING_VALUE'), '');
    edPass.Text := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', VmrSmtpPassTag, 'OPTION_STRING_VALUE'), '');
  finally
    edHost.Tag := 0;
  end;

  l := TStringList.Create;
  evClientDataSet2.Tag := 1;
  evClientDataSet2.DisableControls;
  try
    evClientDataSet2.Close;
    evClientDataSet2.CreateDataSet;
    i := 1;
    while DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', VmrSmtpSetupTag+'_'+IntToStr(i), []) do
    begin
      l.CommaText := DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString;
      while l.Count < 5 do l.Add('');
      evClientDataSet2.AppendRecord([l[0],l[1],l[2],l[3],l[4]]);
      inc(i);
    end;
    evClientDataSet2.MergeChangeLog;
    evClientDataSet2.First;
  finally
    evClientDataSet2.EnableControls;
    evClientDataSet2.Tag := 0;
    l.Free;
  end;
end;

procedure TVmrPrinterLocationSetup.SaveEmailSetup;

var
  l: TStringList;
  i:integer;
begin
  if edHost.Tag = 0 then
  begin
    edFrom.Text := Trim(edFrom.Text);
    SaveOption(SmtpHostTag,edHost.Text);
    SaveOption(VmrSmtpFromTag,edFrom.Text);
    SaveOption(VmrSmtpUserTag,edUser.Text);
    SaveOption(VmrSmtpPassTag,edPass.Text);

    l := TStringList.Create;
    evClientDataSet2.DisableControls;
    try
      i := 1;
      evClientDataSet2.First;
      while not evClientDataSet2.Eof do
      begin
        l.CommaText := '';
        l.Add(Trim(evClientDataSet2Host.AsString));
        l.Add(Trim(evClientDataSet2From.AsString));
        l.Add(Trim(evClientDataSet2User.AsString));
        l.Add(Trim(evClientDataSet2Pass.AsString));
        l.Add(Trim(evClientDataSet2Port.AsString));
        SaveOption(VmrSmtpSetupTag+'_'+IntToStr(i),l.CommaText);
        inc(i);
        evClientDataSet2.Next;
      end;
      while DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', VmrSmtpSetupTag+'_'+IntToStr(i), []) do
      begin
        DM_SERVICE_BUREAU.SB_OPTION.Delete;
        inc(i);
      end;
    finally
    evClientDataSet2.EnableControls;
    evClientDataSet2.Tag := 0;
    l.Free;
    end;
    ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_OPTION]);
  end;
end;

procedure TVmrPrinterLocationSetup.edHostChange(Sender: TObject);
begin
  inherited;
  if edHost.Tag = 0 then
    MarkChanged;
end;

procedure TVmrPrinterLocationSetup.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  evClientDataSet1.Append;
end;

procedure TVmrPrinterLocationSetup.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  if EvMessage(' You are about to delete this location from all client mailboxes.'#13 +
                     ' This may take several minutes.  Do you want to continue?',
                       mtConfirmation, mbOKCancel, mbCancel) = mrOK then
  begin
    FDelLocation := evClientDataSet1.FieldByName('Location').AsString;
    evClientDataSet1.Delete;
  end;
end;

procedure TVmrPrinterLocationSetup.MarkChanged;
begin
  (Owner as TFramePackageTmpl).SetButton(NavOK, True);
  (Owner as TFramePackageTmpl).SetButton(NavCancel, True);
end;

procedure TVmrPrinterLocationSetup.evClientDataSet1AfterInsert(
  DataSet: TDataSet);
begin
  inherited;
  if evClientDataSet1.Tag = 0 then
    MarkChanged;
end;

procedure TVmrPrinterLocationSetup.evClientDataSet1AfterEdit(
  DataSet: TDataSet);
begin
  inherited;
  if evClientDataSet1.Tag = 0 then
    MarkChanged;
end;

procedure TVmrPrinterLocationSetup.evClientDataSet1BeforePost(
  DataSet: TDataSet);
begin
  inherited;
  if (evClientDataSet1.Tag = 0) then
  begin
    if (Trim(DataSet.FieldByName('location').AsString) = '') then
       raise EInvalidParameters.Create('Location can not be empty');

    if Contains(DataSet.FieldByName('location').AsString, '@') then
       raise EInvalidParameters.Create('Location can not include "@" ');
  end;
end;

procedure TVmrPrinterLocationSetup.evDBEdit3Change(Sender: TObject);
begin
  inherited;
  if edHost.Tag = 0 then
    MarkChanged;
end;

procedure TVmrPrinterLocationSetup.evSpeedButton2Click(Sender: TObject);
begin
  inherited;
  evClientDataSet2.Append;
end;

procedure TVmrPrinterLocationSetup.evSpeedButton1Click(Sender: TObject);
begin
  inherited;
  evClientDataSet2.Delete;
end;

procedure TVmrPrinterLocationSetup.evClientDataSet2AfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  if evClientDataSet2.Tag = 0 then
    MarkChanged;
end;

procedure TVmrPrinterLocationSetup.evClientDataSet2AfterEdit(
  DataSet: TDataSet);
begin
  inherited;
    if evClientDataSet2.Tag = 0 then
    MarkChanged;
end;

procedure TVmrPrinterLocationSetup.evClientDataSet2AfterInsert(
  DataSet: TDataSet);
begin
  inherited;
    if evClientDataSet2.Tag = 0 then
    MarkChanged;
end;

procedure TVmrPrinterLocationSetup.LoadCommonOptions;
begin
   edHost.Tag := 1;
   try
     cbBlockVMR.Checked := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', BlockVmrTag, 'OPTION_STRING_VALUE'), 'N')='Y';
     cbRemoteVMR.Checked := ConvertNull(DM_SERVICE_BUREAU.SB_OPTION.Lookup('OPTION_TAG', RemoteVmrTag, 'OPTION_STRING_VALUE'), 'N')='Y';
   finally
     edHost.Tag := 0;
   end;
end;

procedure TVmrPrinterLocationSetup.SaveCommonOptions;
begin
  edHost.tag := 1;
  try
    if cbBlockVMR.Checked then
      SaveOption(BlockVmrTag,'Y')
    else
      SaveOption(BlockVmrTag,'N');

    if cbRemoteVMR.Checked then
      SaveOption(RemoteVmrTag,'Y')
    else
      SaveOption(RemoteVmrTag,'N');

    ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_OPTION]);
  finally
    edHost.Tag := 0;
  end;
end;

procedure TVmrPrinterLocationSetup.SaveOption(op_n, op_v: string);
begin
   if DM_SERVICE_BUREAU.SB_OPTION.Locate('OPTION_TAG', op_n, []) then
      DM_SERVICE_BUREAU.SB_OPTION.Edit
   else
   begin
      DM_SERVICE_BUREAU.SB_OPTION.Append;
      DM_SERVICE_BUREAU.SB_OPTION.OPTION_TAG.AsString := op_n;
   end;
   if DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString <> op_v then
   begin
     DM_SERVICE_BUREAU.SB_OPTION.OPTION_STRING_VALUE.AsString := op_v;
     DM_SERVICE_BUREAU.SB_OPTION.Post;
   end
   else
     DM_SERVICE_BUREAU.SB_OPTION.Cancel;
end;



procedure TVmrPrinterLocationSetup.evSpeedButton4Click(Sender: TObject);
begin
  inherited;
  evClientDataSet3.Append;
end;

procedure TVmrPrinterLocationSetup.evClientDataSet3AfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  if evClientDataSet3.Tag = 0 then
  begin
    MarkChanged;
    IsRemoteLocationDeleted := True;
  end;
end;

procedure TVmrPrinterLocationSetup.evClientDataSet3AfterEdit(
  DataSet: TDataSet);
begin
  inherited;
  if evClientDataSet3.Tag = 0 then
    MarkChanged;
end;

procedure TVmrPrinterLocationSetup.evClientDataSet3AfterInsert(
  DataSet: TDataSet);
begin
  inherited;
  if evClientDataSet3.Tag = 0 then
    MarkChanged;
end;

procedure TVmrPrinterLocationSetup.evClientDataSet3BeforePost(DataSet: TDataSet);
begin
  inherited;
  if (evClientDataSet3.Tag = 0) then
  begin
    if (Trim(DataSet.FieldByName('location').AsString) = '') then
       raise EInvalidParameters.Create('Location can not be empty')
    else begin
      if (DataSet.FieldByName('location').AsString[1]<> '@') then
        raise EInvalidParameters.Create('Location name should be starting with "@" ');
      if Contains(DataSet.FieldByName('location').AsString, '|') then
        raise EInvalidParameters.Create('Location name shouldn''t have "|" ');
    end;
  end;
end;

procedure TVmrPrinterLocationSetup.LoadRemoteLocations;
begin
  DoLoadLocations(evClientDataSet3,VmrRemoteLocation);
end;

procedure TVmrPrinterLocationSetup.SaveRemoteLocations;
begin
  evClientDataSet3.CheckBrowseMode;
  if ((evClientDataSet3.Tag = 0) and (evClientDataSet3.ChangeCount > 0)) then
  begin
    DoSaveLocations(evClientDataSet3,VmrRemoteLocation);
    evClientDataSet3.MergeChangeLog;
  end;
end;

procedure TVmrPrinterLocationSetup.evPageControl1Change(Sender: TObject);
begin
  inherited;
  tsRemotePrinterLocations.TabVisible := cbRemoteVMR.Checked;
end;

procedure TVmrPrinterLocationSetup.evSpeedButton3Click(Sender: TObject);
begin
  inherited;
  if EvMessage(' You are about to delete this Remote location from all client mailboxes.'#13 +
                     ' This may take several minutes.  Do you want to continue?',
                       mtConfirmation, mbOKCancel, mbCancel) = mrOK then
  begin
    FDelLocation := evClientDataSet3.FieldByName('Location').AsString;
    evClientDataSet3.Delete;
  end;
end;

procedure TVmrPrinterLocationSetup.Deactivate;
begin
  inherited;
end;

procedure TVmrPrinterLocationSetup.BtnLocationCleanUpClick(
  Sender: TObject);
begin
  if EvMessage(' This will delete all orphan locations.'#13 +
               ' It might take some time.  Do you want to continue?',
                mtConfirmation, mbOKCancel, mbCancel) = mrOK then
  begin
    DoCleanUpLocations(evClientDataSet1, VmrSbLocation);
  end;
end;

procedure TVmrPrinterLocationSetup.BtnRemoteLocationCleanUpClick(
  Sender: TObject);
begin
  if EvMessage(' This will delete all orphan locations.'#13 +
               ' It might take some time.  Do you want to continue?',
                mtConfirmation, mbOKCancel, mbCancel) = mrOK then
  begin
    DoCleanUpLocations(evClientDataSet3,VmrRemoteLocation);
  end;
end;

procedure TVmrPrinterLocationSetup.DoCleanUpLocations( cs:TevClientDataSet; const optiontag: string );
var
  slist: tStringlist;
  cd: TevClientDataset;
  F: TextFile;
  Buildsql : string;

  function BuildCondition : string;
  var i : integer;
  begin
    Result := '';

    if AnsiSametext( optiontag, VmrSbLocation ) then
      Result := Result + ' and (TRIM(OPTION_STRING_VALUE) not starting with ''@'') '
    else
      Result := Result + ' and (TRIM(OPTION_STRING_VALUE) starting with ''@'') ';

    if sList.Count > 0 then
    begin
      Result := Result + ' and  Upper(TRIM(OPTION_STRING_VALUE)) not in ( ';
      for i := 0 to sList.Count - 1 do
        Result := Result + QuotedStr(sList[i]) + ',';
      Result[length(result)] := ')';
    end;
  end;

  procedure CleanUpRemoteLocationPrinters;
  var
    s: string;
    i : integer;
  begin
    s:= '';
    if sList.Count > 0 then
    begin
      s := s + 'and not ( ';
      for i := 0 to sList.Count - 1 do
        s := s + '(OPTION_TAG starting with '''+sList[i]+''') or';
      s := Copy(s, 1, Length(s)-2)  + ')';
    end;

    cd.Close;
    cd.ProviderName := DM_SERVICE_BUREAU.SB_OPTION.ProviderName;
    cd.DataRequired('(OPTION_TAG starting with ''@'') '+s);
    cd.First;
    while not cd.Eof do
    begin
      writeln(f, 'Remote Printer Location:'+cd.FieldByName('OPTION_TAG').asString);
      cd.Delete;
    end;
    ctx_DataAccess.PostDataSets([cd]);
  end;

  procedure CleanUpLocationPrinters;
  begin
    cd.Close;
    cd.ProviderName := DM_SERVICE_BUREAU.SB_OPTION.ProviderName;
    cd.DataRequired(' ( (OPTION_TAG like ''PRINTER_LOCATION'') or (OPTION_TAG like ''PRINTER__LOCATION'')) '+ Buildsql);
    cd.First;
    while not cd.Eof do
    begin
      writeln(f, 'Printer Location:'+cd.FieldByName('OPTION_STRING_VALUE').asString);
      cd.Edit;
      cd.FieldByName('OPTION_STRING_VALUE').asString := '';
      cd.Post;
      cd.next;
    end;
    ctx_DataAccess.PostDataSets([cd]);
  end;

  procedure CleanUpCLFillerLocation;
  var
    sf: string;
  begin
    cd.Close;
    cd.ProviderName := DM_CLIENT.CL.ProviderName;
    cd.DataRequired('');
    cd.First;
    sf := Uppercase(Trim(Copy(cd.FieldByName('FILLER').AsString, 1, 40)));
    if ( (AnsiSametext( optiontag, VmrSbLocation ) and ((Length(sf) > 0) and (sf[1] <> '@')) and (slist.IndexOf(sf) < 0 ))
        or (AnsiSametext( optiontag, VmrRemoteLocation ) and  ((Length(sf) > 0) and (sf[1] = '@')) and (slist.IndexOf(sf) < 0 )) ) then
    begin
      writeln(f, 'Client#:'+ Inttostr(ctx_DataAccess.ClientID)+'-Client Location:'+sf);
      cd.Edit;
      cd.FieldByName('FILLER').AsString := PutIntoFiller(cd.FieldByName('FILLER').AsString, '', 1, 40);
      cd.Post;
    end;
    ctx_DataAccess.PostDataSets([cd]);
  end;

  procedure CleanUpChildRecords;
  begin
    DM_TEMPORARY.TMP_CL.Close;
    ctx_DataAccess.OpenDataSets([DM_TEMPORARY.TMP_CL]);
    DM_TEMPORARY.TMP_CL.First;
    while not DM_TEMPORARY.TMP_CL.Eof do
    begin
      if ConvertNull(DM_TEMPORARY.TMP_CL.FieldValues['CL_NBR'],0) <= 0 then
         continue;
      ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CL.FieldValues['CL_NBR']);

      CleanUpCLFillerLocation;
      cd.Close;
      cd.ProviderName := DM_CLIENT.CL_MAIL_BOX_GROUP_OPTION.ProviderName;
      cd.DataRequired(' (OPTION_TAG = ''VMR_PRINT_LOCATIONS'')and(Option_String_Value is not null)and(Option_String_Value <> '''') '+Buildsql);
      cd.First;
      while not cd.Eof do
      begin
        writeln(f, 'Client#:'+Inttostr(ctx_DataAccess.ClientID)+'-CL Mail Box Location:'+cd.FieldByName('Option_string_value').asString);
        cd.Edit;
        cd.FieldByName('OPTION_STRING_VALUE').asString := '';
        cd.Post;
        cd.next;
      end;
      ctx_DataAccess.PostDataSets([cd]);

      DM_TEMPORARY.TMP_CL.next;
    end;
    ctx_DataAccess.OpenClient(0);
    DM_TEMPORARY.TMP_CL.Close;

    cd.Close;
    cd.ProviderName := DM_SERVICE_BUREAU.SB_MAIL_BOX_OPTION.ProviderName;
    cd.DataRequired(' (OPTION_TAG = ''VMR_PRINT_LOCATIONS'')and(Option_String_Value is not null)and(Option_String_Value <> '''') '+Buildsql);
    cd.First;
    while not cd.Eof do
    begin
      writeln(f, 'SB Mail Box Location:'+cd.FieldByName('Option_string_value').asString);
      cd.Edit;
      cd.FieldByName('OPTION_STRING_VALUE').asString := '';
      cd.Post;
      cd.next;
    end;
    ctx_DataAccess.PostDataSets([cd]);

    if AnsiSametext( optiontag, VmrSbLocation ) then
      CleanUpLocationPrinters
    else
      CleanUpRemoteLocationPrinters;
  end;
begin
  slist := cs.GetFieldValueList('LOCATION', True);
  slist.Sorted := True;
  slist.Duplicates := dupIgnore;
  slist.CommaText := Uppercase(slist.CommaText);
  cd := TevClientDataSet.Create(nil);
  Buildsql := BuildCondition();

  Assignfile(f,AppDir+'VMRlocation.text');
  if FileExists(AppDir+'VMRlocation.text')then
    Append(f)
  else
    Rewrite(f);
  try
    ctx_StartWait('Clean up Locations...');
    try
      if AnsiSametext( optiontag, VmrSbLocation ) then
         writeln(f, '===========LOCATION CLEAN UP:'+ DateTimeToStr(Now) )
      else
        writeln(f,  '===========REMOTE LOCATION CLEAN UP:'+ DateTimeToStr(Now) );
      writeln(f, 'Location List:'+sList.CommaText);

      CleanUpChildRecords;
    finally
      writeln(f, 'Orphan '+ifthen(AnsiSametext( optiontag, VmrSbLocation ), '', 'Remote ') +'Locations are cleaned up');
      ctx_EndWait;
    end;
  finally
    cd.Free;
    slist.free;
    close(f);
  end;
end;

initialization
  RegisterClass(TVmrPrinterLocationSetup);

end.



