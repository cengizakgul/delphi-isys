// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_PAPER_INFO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_SY_MEDIA_TYPE, SDataStructure, DB, Wwdatsrc,
   wwdbedit, StdCtrls, Mask, Wwdotdot, Wwdbcomb, ExtCtrls,
  Grids, Wwdbigrd, Wwdbgrid, SFrameEntry, SDDClasses, 
  ISBasicClasses, Buttons, SDataDictbureau, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIwwDBComboBox, isUIwwDBEdit;

type
  TEDIT_SB_PAPER_INFO = class(TFrameEntry)
    evDBGrid1: TevDBGrid;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evDBComboBox2: TevDBComboBox;
    evLabel2: TevLabel;
    evDBComboBox1: TevDBComboBox;
    evLabel3: TevLabel;
    evDBEdit2: TevDBEdit;
    evDBEdit3: TevDBEdit;
    evLabel4: TevLabel;
    edWeight: TevDBEdit;
    evLabel5: TevLabel;
    bOuncesToFractions: TevBitBtn;
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure bOuncesToFractionsClick(Sender: TObject);
  private
    { Private declarations }
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

{$R *.dfm}

uses SFieldCodeValues, EvUtils;

{ TEDIT_SB_PAPER_INFO }

procedure TEDIT_SB_PAPER_INFO.Activate;
begin
  inherited;
  //evDBComboBox1.Items.Text := MediaType_ComboChoices;
  //evDBComboBox2.Items.Text := MediaType_ComboChoices;
end;

function TEDIT_SB_PAPER_INFO.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_PAPER_INFO;
end;

function TEDIT_SB_PAPER_INFO.GetInsertControl: TWinControl;
begin
  Result := evDBEdit1;
end;

procedure TEDIT_SB_PAPER_INFO.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.State = dsInsert then
  begin
    wwdsDetail.DataSet['HEIGHT'] := 0;
    wwdsDetail.DataSet['WIDTH'] := 0;
    wwdsDetail.DataSet['WEIGHT'] := 0;
  end;
end;

procedure TEDIT_SB_PAPER_INFO.bOuncesToFractionsClick(Sender: TObject);
var
  s: string;
begin
  inherited;
  if EvDialog('Converting ounces to fractions of pound', 'Ounces:', s) then
  begin
    wwdsDetail.Edit;
    //wwdsDetail.DataSet['WEIGHT'] := ConvertNull(wwdsDetail.DataSet['WEIGHT'], 0)+ StrToFloatDef(s, 0)/ 16;
    wwdsDetail.DataSet['WEIGHT'] := StrToFloatDef(s, 0)/ 16;
  end;
end;

initialization
  RegisterClass(TEDIT_SB_PAPER_INFO);

end.
