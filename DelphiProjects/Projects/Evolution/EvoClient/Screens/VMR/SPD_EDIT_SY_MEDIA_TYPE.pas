// Copyright c 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SY_MEDIA_TYPE;

interface

uses
  SDataStructure,  wwdbedit, Wwdotdot,
  Wwdbcomb, wwdbdatetimepicker, Mask, DBCtrls, StdCtrls, wwdblook,
  ExtCtrls, Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls,
  Classes, SFrameEntry, SDDClasses, SDataDictsystem, ISBasicClasses, EvUIComponents, EvClientDataSet,
  isUIwwDBEdit, isUIwwDBComboBox;

type
  TEDIT_SY_MEDIA_TYPE = class(TFrameEntry)
    evDBGrid1: TevDBGrid;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evPanel1: TevPanel;
    evLabel1: TevLabel;
    evDBComboBox1: TevDBComboBox;
    evLabel2: TevLabel;
    evDBEdit1: TevDBEdit;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
  end;

implementation

uses SVmrClasses;

{$R *.DFM}

procedure TEDIT_SY_MEDIA_TYPE.Activate;
begin
  inherited;
  evDBComboBox1.Items.CommaText := ListVmrInheritedClasses(TVmrMediaType);
end;

function TEDIT_SY_MEDIA_TYPE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SYSTEM_MISC.SY_MEDIA_TYPE;
end;

function TEDIT_SY_MEDIA_TYPE.GetInsertControl: TWinControl;
begin
  Result := evDBComboBox1;
end;

initialization
  RegisterClass(TEDIT_SY_MEDIA_TYPE);

end.
