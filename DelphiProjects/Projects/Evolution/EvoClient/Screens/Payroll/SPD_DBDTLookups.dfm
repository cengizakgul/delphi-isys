object DBDTLookups: TDBDTLookups
  Left = 0
  Top = 0
  Width = 223
  Height = 50
  TabOrder = 0
  Visible = False
  object CO_DIVISIONClone: TevClientDataSet
    OnFilterRecord = CloneFilterRecord
    Left = 8
    Top = 8
  end
  object CO_BRANCHClone: TevClientDataSet
    OnFilterRecord = CloneFilterRecord
    Left = 48
    Top = 8
  end
  object CO_DEPARTMENTCLone: TevClientDataSet
    OnFilterRecord = CloneFilterRecord
    Left = 88
    Top = 8
  end
  object CO_TEAMClone: TevClientDataSet
    OnFilterRecord = CloneFilterRecord
    Left = 128
    Top = 8
  end
  object cdDBDT: TevClientDataSet
    Left = 168
    Top = 8
    object cdDBDTDIVISION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_DIVISION_NBR'
      Visible = False
    end
    object cdDBDTDIVISION_NAME: TStringField
      DisplayLabel = 'Division Name'
      DisplayWidth = 20
      FieldName = 'DIVISION_NAME'
      Size = 40
    end
    object cdDBDTBRANCH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_BRANCH_NBR'
      Visible = False
    end
    object cdDBDTBRANCH_NAME: TStringField
      DisplayLabel = 'Branch Name'
      DisplayWidth = 20
      FieldName = 'BRANCH_NAME'
      Size = 40
    end
    object cdDBDTDEPARTMENT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_DEPARTMENT_NBR'
      Visible = False
    end
    object cdDBDTDEPARTMENT_NAME: TStringField
      DisplayLabel = 'Department Name'
      DisplayWidth = 20
      FieldName = 'DEPARTMENT_NAME'
      Size = 40
    end
    object cdDBDTTEAM_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TEAM_NBR'
      Visible = False
    end
    object cdDBDTTEAM_NAME: TStringField
      DisplayLabel = 'Team Name'
      DisplayWidth = 20
      FieldName = 'TEAM_NAME'
      Size = 40
    end
    object cdDBDTDBDT_CUSTOM_NUMBERS: TStringField
      DisplayLabel = 'Custom D/B/D/T Numbers'
      DisplayWidth = 30
      FieldName = 'DBDT_CUSTOM_NUMBERS'
      Size = 80
    end
    object cdDBDTCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
  end
end
