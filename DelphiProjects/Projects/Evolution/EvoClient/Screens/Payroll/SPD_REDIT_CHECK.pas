// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_REDIT_CHECK;

interface

uses
  Windows, SDataStructure, SPD_REDIT_WIZARD_BASE, Menus,  DBCtrls,
  Grids, Wwdotdot, Wwdbcomb, StdCtrls, ExtCtrls, Wwdbigrd, Wwdbgrid, EvBasicUtils,
  wwdbdatetimepicker, Mask, wwdbedit, wwdblook, Buttons, Controls,
  ComCtrls, Classes, ActnList, Db, Wwdatsrc, Graphics, SysUtils, evTypes,
  EvConsts, SPackageEntry, EvUtils, SPopulateRecords, EvContext,
  Dialogs,  DBActns, SPD_Payroll_Expert, Forms, isVCLBugFix,
  Messages, SPD_Labor_Distribution_Preview, Variants,  EvMainboard,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, IsBaseClasses,
  EvDataAccessComponents, ISDataAccessComponents, SDataDictclient, EvExceptions,
  EvCommonInterfaces, SFieldCodeValues, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIDBMemo, LMDCustomButton, LMDButton, isUILMDButton, isUIwwDBComboBox,
  isUIwwDBDateTimePicker, isUIwwDBEdit, isUIwwDBLookupCombo,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, ImgList,
  isUIFashionPanel, XLSFile, wwexport, XLSWorkbook, EvPayroll, EvDataset, StrUtils;

type
 TREDIT_CHECK = class(TREDIT_WIZARD_BASE)
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    ManualTaxTabSheet: TTabSheet;
    wwDBGrid1: TevDBGrid;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    tsPreview: TTabSheet;
    tsNotes: TTabSheet;
    Panel9: TevPanel;
    Label45: TevLabel;
    dblkupEE_NBR: TevDBLookupCombo;
    HeaderPanel: TevPanel;    
    strPreviewGrid: TevStringGrid;
    DBMemo1: TEvDBMemo;
    Label10: TevLabel;
    CheckLineGridPopup: TevPopupMenu;
    DS_PR_CHECK: TevDataSource;
    DBEdit4: TevDBEdit;
    CHECK_TYPE: TevDBComboBox;
    DS_PR_CHECK_LINES: TevDataSource;
    DS_EE_CL_PERSON: TevDataSource;
    DS_PR_CHECK_STATES: TevDataSource;
    DS_PR_CHECK_LINE_LOCALS: TevDataSource;
    ED_CL_E_DS_LookupCombo: TevDBLookupCombo;
    DS_CL_E_DS: TevDataSource;
    BitBtn1: TevSpeedButton;
    PR_Label: TevLabel;
    Batch_Label: TevLabel;    
    lblCheck: TevLabel;
    cbRates: TevDBComboBox;
    dbtxtname: TevDBLookupCombo;
    lblBatch: TevLabel;
    DM_COMPANY: TDM_COMPANY;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    DM_PAYROLL: TDM_PAYROLL;
    DM_CLIENT: TDM_CLIENT;
    EEStatesSrc: TevDataSource;
    EELocalsSrc: TevDataSource;
    EERatesSrc: TevDataSource;
    ManualTaxClientDataSet: TevClientDataSet;
    ManualTaxClientDataSetLabel: TStringField;
    ManualTaxClientDataSetAmountFieldName: TStringField;
    ManualTaxClientDataSetRowNbr: TIntegerField;
    ManualTaxClientDataSetDataSetName: TStringField;
    ManualTaxClientDataSetIDFieldName: TStringField;
    ManualTaxClientDataSetID: TIntegerField;
    ManualTaxClientDataSetAmount: TFloatField;
    FocusNbr: TAction;
    FocusName: TAction;
    DS_PR_CHECK_SUI: TevDataSource;
    BatchSrc: TevDataSource;
    NextCheck: TAction;
    PriorCheck: TAction;
    PR_CHECKClone: TevClientDataSet;
    FocusMain: TAction;
    CalculateCheckLine: TAction;
    ShowLocals: TAction;
    sbtnTaxCalc: TevSpeedButton;
    EEClone: TevClientDataSet;
    sbtnCheckFinder: TevSpeedButton;
    ManualTaxClientDataSetRO: TBooleanField;
    Button2: TevBitBtn;
    Button1: TevBitBtn;
    Button3: TevBitBtn;
    bNextCheck: TevBitBtn;
    bVoidCheck: TevBitBtn;
    bManualCheck: TevBitBtn;
    aCreateNextCheck: TAction;
    aVoidNextCheck: TAction;
    aCreateManualCheck: TAction;
    aCheckTemplate: TAction;
    aDuplicate: TAction;
    bEE: TevSpeedButton;
    bScheduledEDs: TevSpeedButton;
    bEERates: TevSpeedButton;
    evPanel1: TevPanel;
    evBitBtn1: TevBitBtn;
    lblDBDT: TevLabel;
    lablClient: TevLabel;
    evLabel7: TevLabel;
    dbtClNumber: TevDBText;
    dbtCoNumber: TevDBText;
    dbtCoName: TevDBText;
    dbtClName: TevDBText;
    lEeFilter: TLabel;    
    dsCL: TevDataSource;
    PageControlImages: TevImageList;
    sbCheckLines: TScrollBox;
    fplListofCheckLines: TisUIFashionPanel;
    fpManualTax: TisUIFashionPanel;
    wwDBGrid4: TevDBGrid;
    btnAddStates: TevBitBtn;
    btnAddLocals: TevBitBtn;
    pnlOptions: TisUIFashionPanel;
    evDBRadioGroup1: TevDBRadioGroup;
    drgpReciprocate_SUI: TevDBRadioGroup;
    butnProrateFICA: TevBitBtn;
    evDBRadioGroup3: TevDBRadioGroup;
    sbManualTax: TScrollBox;
    pnlGeneralSettings: TisUIFashionPanel;
    Label8: TevLabel;
    lblCheckType945: TevLabel;
    DBRadioGroup2: TevDBRadioGroup;
    DBRadioGroup3: TevDBRadioGroup;
    DBRadioGroup4: TevDBRadioGroup;
    DBRadioGroup5: TevDBRadioGroup;
    DBRadioGroup7: TevDBRadioGroup;
    DBRadioGroup6: TevDBRadioGroup;
    DBRadioGroup1: TevDBRadioGroup;
    DBMemo2: TEvDBMemo;
    cbCheckType945: TevDBComboBox;
    evDBRadioGroup2: TevDBRadioGroup;
    fpActions: TisUIFashionPanel;
    sbGeneral: TScrollBox;
    sbNotes: TScrollBox;
    evPanel3: TevPanel;
    pnlNotes: TisUIFashionPanel;
    pnlNotesBody: TevPanel;
    ScrollBox1: TScrollBox;
    evPanel2: TevPanel;
    pnlPreview: TisUIFashionPanel;
    pnlPreviewBody: TevPanel;
    fpFederal: TisUIFashionPanel;
    Label28: TevLabel;
    Label13: TevLabel;
    Label9: TevLabel;
    GroupBox1: TevGroupBox;
    evLabel2: TevLabel;
    evDBComboBox1: TevDBComboBox;
    OverideFederalType: TevDBComboBox;
    DBEdit12: TevDBEdit;
    TaxFrequency: TevDBComboBox;
    DBRadioGroup18: TevDBRadioGroup;
    fpState: TisUIFashionPanel;
    Label2: TevLabel;
    wwDBGrid2: TevDBGrid;
    StateOverrideCombo: TevDBComboBox;
    SupplRateOverrideCombo: TevDBComboBox;
    State_LookupCombo: TevDBLookupCombo;
    ExclSDIOvrd: TevDBComboBox;
    ExclStateOvrd: TevDBComboBox;
    ExclSUIOvrd: TevDBComboBox;
    ExclAddStateOvrd: TevDBComboBox;
    fpLocal: TisUIFashionPanel;
    Label3: TevLabel;
    wwDBGrid3: TevDBGrid;
    YNOverrideCombo_Grid3: TevDBComboBox;
    LocalLookupCombo: TevDBLookupCombo;
    sbOverrides: TScrollBox;
    ilGridPictures: TevImageList;
    pnlFavoriteReport: TevPanel;
    evLabel47: TevLabel;
    pnlTopLeft: TevPanel;
    DBRadioGroup11: TevDBRadioGroup;
    DBRadioGroup13: TevDBRadioGroup;
    DBRadioGroup15: TevDBRadioGroup;
    DBRadioGroup9: TevDBRadioGroup;
    DBRadioGroup14: TevDBRadioGroup;
    DBRadioGroup16: TevDBRadioGroup;
    DBRadioGroup17: TevDBRadioGroup;
    pnlDropDowns: TevPanel;
    edPreviewHiddenField: TEdit;
    LocalOverrideAmountEdit: TevDBEdit;
    fpCheckLineDetail: TisUIFashionPanel;
    PageControl2: TevPageControl;
    TabSheet5: TTabSheet;
    sbAllFields: TScrollBox;
    SpeedButton1: TevSpeedButton;
    YTDBtn: TevSpeedButton;
    sbAddJob: TevSpeedButton;
    Label5: TevLabel;
    Label26: TevLabel;
    Label25: TevLabel;
    Label24: TevLabel;
    Label23: TevLabel;
    Label22: TevLabel;
    Label21: TevLabel;
    Label20: TevLabel;
    Label19: TevLabel;
    Label18: TevLabel;
    Label17: TevLabel;
    Label16: TevLabel;
    Label15: TevLabel;
    Label14: TevLabel;
    Label12: TevLabel;
    evLabel1: TevLabel;
    butnALD_or_Change: TevSpeedButton;
    btnLocals: TevSpeedButton;
    bvVertLine: TEvBevel;
    lkupWorkComp: TevDBLookupCombo;
    lkupTeam: TevDBLookupCombo;
    lkupSUI: TevDBLookupCombo;
    lkupStates: TevDBLookupCombo;
    lkupShift: TevDBLookupCombo;
    lkupPiece: TevDBLookupCombo;
    lkupJobs: TevDBLookupCombo;
    lkupDivision: TevDBLookupCombo;
    lkupDepartment: TevDBLookupCombo;
    lkupBranch: TevDBLookupCombo;
    lkupAgency: TevDBLookupCombo;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo2: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    evDBLookupCombo4: TevDBLookupCombo;
    edtRatePay: TevDBEdit;
    edtRateNo: TevDBEdit;
    edtHours: TevDBEdit;
    dbtpEndDate: TevDBDateTimePicker;
    dbtpDate: TevDBDateTimePicker;
    WorkAtHome: TevDBRadioGroup;
    lsCLLocals: TTabSheet;
    evDBGrid1: TevDBGrid;
    cbCLOverride: TevDBComboBox;
    lcCLLocalName: TevDBLookupCombo;
    procedure CheckLineGridPopupPopup(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure ED_CL_E_DS_LookupComboNotInList(Sender: TObject;
      LookupTable: TDataSet; NewValue: string; var Accept: Boolean);
    procedure wwDBGrid1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure wwDBGrid1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure BitBtn1Click(Sender: TObject);
    procedure lkupDivisionDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure CHECK_TYPEChange(Sender: TObject);
    procedure dblkupEE_NBRCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure SpeedButton1Click(Sender: TObject);
    procedure wwDBGrid1ColumnMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure wwDBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure wwDBGrid1CellChanged(Sender: TObject);
    procedure wwDBGrid1DrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure Button3Click(Sender: TObject);
    procedure YTDBtnClick(Sender: TObject);
    procedure wwdsMasterDataChangeBeforeChildren(Sender: TObject;
      Field: TField);
    procedure ManualTaxClientDataSetAfterPost(DataSet: TDataSet);
    procedure FocusNbrExecute(Sender: TObject);
    procedure FocusNameExecute(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure wwDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure NextCheckExecute(Sender: TObject);
    procedure PriorCheckExecute(Sender: TObject);
    procedure EEShowFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure FocusMainExecute(Sender: TObject);
    procedure CalculateCheckLineExecute(Sender: TObject);
    procedure wwDBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure btnAddStatesClick(Sender: TObject);
    procedure btnAddLocalsClick(Sender: TObject);
    procedure ShowLocalsExecute(Sender: TObject);
    procedure ShowLocalsUpdate(Sender: TObject);
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
    procedure lkupDBDTDropDown(Sender: TObject);
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure DS_PR_CHECK_LINESDataChange(Sender: TObject; Field: TField);
    procedure sbtnTaxCalcClick(Sender: TObject);
    procedure butnALD_or_ChangeClick(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure sbAddJobClick(Sender: TObject);
    procedure lkupJobsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbWizardClick(Sender: TObject);
    procedure sbtnCheckFinderClick(Sender: TObject);
    procedure wwdsDetailDataChangeBeforeChildren(Sender: TObject;
      Field: TField);
    procedure ManualTaxClientDataSetBeforeEdit(DataSet: TDataSet);
    procedure aCreateNextCheckExecute(Sender: TObject);
    procedure aVoidNextCheckExecute(Sender: TObject);
    procedure aCreateManualCheckExecute(Sender: TObject);
    procedure aCheckTemplateExecute(Sender: TObject);
    procedure aDuplicateExecute(Sender: TObject);
    procedure lkupStatesEnter(Sender: TObject);
    procedure lkupStatesExit(Sender: TObject);
    procedure lkupSUIEnter(Sender: TObject);
    procedure lkupSUIExit(Sender: TObject);
    procedure bEEClick(Sender: TObject);
    procedure bEERatesClick(Sender: TObject);
    procedure bScheduledEDsClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure ManualTaxClientDataSetBeforePost(DataSet: TDataSet);
    procedure cbCLOverrideChange(Sender: TObject);
    procedure butnProrateFICAClick(Sender: TObject);
    procedure aVoidNextCheckUpdate(Sender: TObject);
    procedure ED_CL_E_DS_LookupComboEnter(Sender: TObject);
    procedure ED_CL_E_DS_LookupComboExit(Sender: TObject);
    procedure cbCLOverrideDropDown(Sender: TObject);
    procedure sbCheckLinesResize(Sender: TObject);
    procedure pnlNotesResize(Sender: TObject);
    procedure pnlPreviewResize(Sender: TObject);
    procedure wwDBGrid1AfterDrawCell(Sender: TwwCustomDBGrid;
      DrawCellInfo: TwwCustomDrawGridCellInfo);
    procedure PageControl1Resize(Sender: TObject);
    procedure dblkupEE_NBRChange(Sender: TObject);
    procedure wwDBGrid1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure wwDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LocalOverrideAmountEditChange(Sender: TObject);//gdy5
  private
    bDontCheck: Boolean;
    EEStatesFilter, EESUIFilter: Integer;
    InitializingManualTaxClientDataSet: Boolean;
    FRoOasdiMedicare: Boolean;
    CurrentCheckEE: Integer;
    CheckNbrBeforeActivate:Variant;
    FCustomAfterPost, FCustomAfterDelete: TDataSetNotifyEvent;
    FCustomValidate: TFieldNotifyEvent;
    EmployeeBeforeInsert: String;
    FEeLocalList: IisListOfValues;
    procedure OnCheckScroll;
    procedure FilterEeStates;
    procedure SetCheckLineControlsReadOnly(Value: Boolean);
    procedure AfterChangePost(DataSet: TDataSet);
    procedure AfterChangeDelete(DataSet: TDataSet);
    procedure ValidateRate(Sender: TField);
    procedure RecalcSum;
    procedure LoadManualTaxData;
    procedure UnloadManualTaxData;
    procedure CheckChanged;
    procedure RecalculateCheck;
    procedure RecalcCheck;
    procedure PaLocalWarning;
    procedure SetGridPosition(sName: string);
    procedure CreateCustomControl(sFieldName: string; AControl: TevDBLookupCombo);
    procedure CalcCheckLine(Sender: TObject);
    procedure CreateEEFilter;
    procedure RedistributeDBDT(Sender: TObject);
    procedure InitNamedDSStates(const Name: string; DS: array of TevClientDataSet);
    procedure LoadNamedDSStates(const Name: string; DS: array of TevClientDataSet);
    procedure SaveNamedDSStates(const Name: string; DS: array of TevClientDataSet; const SaveData: Boolean = False);
  public
    { Public declarations }
    procedure RefreshPreview;
    procedure MenuCLickHdl(Sender: TObject);
    procedure DeleteMenuCLickHdl(Sender: TObject);
    function GetDropControlPrototype(Description: string): TControl;
    procedure AddToGrid(DragDropIndex: integer);
    procedure Activate; override;
    procedure Deactivate; override;
    function CanOpen: Boolean; override;
    function CanClose: Boolean; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure AfterClick(Kind: Integer); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
//SEG BEGIN
    procedure InitDBDTList(var Key: Char; Level: Integer);
//SEG BEGIN
  protected
    function GetIfReadOnly: Boolean; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetDataSetConditions(sName: string): string; override;
    procedure SetReadOnly(Value: Boolean); override;
//begin gdy5
  private
    CO_DIVISIONClone: TevClientDataSet;
    CO_BRANCHClone: TevClientDataSet;
    CO_DEPARTMENTClone: TevClientDataSet;
    CO_TEAMClone: TevClientDataSet;
    DBDTFilter: TDBDTFilterContext;
    procedure LocateUserLine;
    procedure ActivateDBDTSupport;
    procedure DeactivateDBDTSupport;
//end gdy5
  end;

implementation

uses SPayrollScr, SPD_EDIT_SELECT_TEMPLATES_TO_APPLY,
  SPD_TaxCalculator, SPD_CheckFinder,
  SPD_DBDTSelectionListFiltr, //SEG
  SPD_YTD_Totals, SPD_SelectSL, SSecurityInterface, SPD_FORM_ADD2CHECK_LINES_LOCALS,
  SCreateNextCheck, SCreateManualCheck, SPD_RedistrDBDT, SFrameEntry;

{$R *.DFM}

const
  NumberOfDragDropFields = 15; //SEG 6 -> 15

var
  DragDropFields: array[0..NumberOfDragDropFields - 1] of record
    D: string;
    F: string;
    L: integer;
    V: integer;
  end =
  ((D: 'DEPARTMENT'; F: 'CO_DEPARTMENT_NBR_DESC'; L: 20; V: - 1),
    (D: 'DIVISION'; F: 'CO_DIVISION_NBR_DESC'; L: 20; V: - 1),
    (D: 'BRANCH'; F: 'CO_BRANCH_NBR_DESC'; L: 20; V: - 1),
    (D: 'TEAM'; F: 'CO_TEAM_NBR_DESC'; L: 20; V: - 1),
    (D: 'JOB'; F: 'CO_JOBS_NBR_DESC'; L: 20; V: - 1),
    (D: 'RATE NBR'; F: 'RATE_NUMBER'; L: 10; V: - 1),
//SEG BEGIN
//lookups
    (D: 'STATES'; F: 'STATE_LOOKUP'; L: 20; V: - 1),
    (D: 'SUI STATES'; F: 'SUI_STATE_LOOKUP'; L: 20; V: - 1),
    (D: 'AGENCY NAME'; F: 'CL_AGENCY_LOOKUP'; L: 20; V: - 1),
    (D: 'WORKERS COMP'; F: 'CO_WORKERS_COMP_LOOKUP'; L: 20; V: - 1),
    (D: 'SHIFTS'; F: 'CO_SHIFTS_LOOKUP'; L: 20; V: - 1),
    (D: 'PIECES'; F: 'CL_PIECES_LOOKUP'; L: 20; V: - 1),
//edits
    (D: 'RATE OF PAY'; F: 'RATE_OF_PAY'; L: 20; V: - 1),
    (D: 'HOURS OR PIECES'; F: 'HOURS_OR_PIECES'; L: 20; V: - 1),
    (D: 'LINE ITEM DATE'; F: 'LINE_ITEM_DATE'; L: 20; V: - 1)
//SEG END
    );
  sEEFilter: string;

procedure TREDIT_CHECK.Activate;
var
  t: IisStringListRO;
  i, j: integer;
  aControl: TControl;
begin
  DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR',CheckNbrBeforeActivate,[]);
  inherited;
  lEeFilter.Caption := ctx_PayrollCalculation.GetEeFilterCaption;
  EEStatesFilter := 0;
  EESUIFilter := 0;
  FRoOasdiMedicare := ctx_AccountRights.Functions.GetState('MANUAL_OASDI_MEDICARE') <> stEnabled;
  DBRadioGroup13.Enabled := not FRoOasdiMedicare;
  DBRadioGroup14.Enabled := DBRadioGroup13.Enabled;
  DBRadioGroup15.Enabled := DBRadioGroup13.Enabled;
  DBRadioGroup16.Enabled := DBRadioGroup13.Enabled;

  DM_CLIENT.CO_LOCAL_TAX.DataRequired('CO_NBR=' + DM_CLIENT.CO.CO_NBR.AsString);
  DM_SYSTEM.SY_STATES.DataRequired('');
  DM_SYSTEM.SY_LOCALS.DataRequired('');
  DM_CLIENT.EE_LOCALS.DataRequired('');

  FEeLocalList := TisListOfValues.Create;  
  ctx_DataAccess.CheckMaxAmountAndHours := True;

  cbRates.DropDownWidth := 350;

  ActivateDBDTSupport; //gdy5

  PR_CHECKClone.CloneCursor(DM_PAYROLL.PR_CHECK, False);
  CreateEEFilter;
  EEClone.CloneCursor(DM_EMPLOYEE.EE, False);
  EEClone.OnFilterRecord := EEShowFilterRecord;
  EEClone.Filtered := True;
  DM_EMPLOYEE.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
  EEClone.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
  if CalcForBatch then
    wwdsMaster.MasterDataSource := BatchSrc;

  InitializingManualTaxClientDataSet := False;

  ManualTaxClientDataSet.CreateDataSet;
  ManualTaxClientDataSet.AddIndex('iRowNbr', 'RowNbr', [ixCaseInsensitive], '', '', 0);
  ManualTaxClientDataSet.IndexName := 'iRowNbr';
  ManualTaxClientDataSet.IndexDefs.Update;
  
  PR_LABEL.Caption := GetPayrollDescription;
  Batch_Label.Caption := GetBatchDescription;

  dblkupEE_NBR.ReadOnly := False;
  dblkupEE_NBR.Enabled := True;
  dblkupEE_NBR.Color := clWindow;
  dbtxtname.ReadOnly := False;
  dbtxtname.Enabled := True;
  dbtxtname.Color := clWindow;
  Button2.Enabled := not GetIfReadOnly;

  dblkupEE_NBR.Value := DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsString;
  dbtxtname.Value := DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsString;

  DM_COMPANY.CO_JOBS.Filter := 'JOB_ACTIVE=''Y''';
  DM_COMPANY.CO_JOBS.Filtered := True;

  if mb_AppSettings.AsInteger['Misc\' + ClassName + '\wwDBGrid1\Version'] < wwDBGrid1.Tag then
    mb_AppSettings.DeleteNode('Misc\' + Self.ClassName + '\wwDBGrid1');

  t := mb_AppSettings.GetValueNames('Misc\' + Self.ClassName + '\wwDBGrid1\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));
  if t.Count > 0 then
  begin
    wwDBGrid1.Selected.Clear;
    for i := 0 to Pred(t.Count) do
      wwDBGrid1.Selected.Add(mb_AppSettings['Misc\' + Self.ClassName + '\wwDBGrid1\CL' +
        IntToStr(ctx_DataAccess.ClientID) + 'CO' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger) + '\' + t[i]]);
    wwDBGrid1.ColumnByName('Amount').ReadOnly := ctx_AccountRights.Functions.GetState('FIELDS_WAGES') <> stEnabled;
    wwDBGrid1.ApplySelected;
  end;

  for i := 0 to Pred(NumberOfDragDropFields) do
    DragDropFields[i].V := -1;

  for j := 0 to Pred(wwDBGrid1.Selected.Count) do
    for i := 0 to Pred(NumberOfDragDropFields) do
      if AnsiCompareText(DragDropFields[i].F, Copy(wwDBGrid1.Selected[j], 1, Pred(Pos(#9, wwDBGrid1.Selected[j])))) = 0 then
      begin
        aControl := GetDropControlPrototype(DragDropFields[i].D);
        if aControl is TevDBLookupCombo then
          CreateCustomControl(DragDropFields[i].F, TevDBLookupCombo(AControl));
        DragDropFields[i].V := j;
        Break;
      end;

  DBMemo1.LoadMemo;
  if Length(Trim(DBMemo1.Lines.Text)) <> 0 then
    PageControl1.ActivePage := tsNotes
  else
    PageControl1.ActivePage := TabSheet1;
  DM_PAYROLL.PR_CHECK_LINES.IndexFieldNames := 'LineTypeSort; Sorting1; Sorting2';
  LocateUserLine;
  RecalcSum;

  SetReadOnly(GetIfReadOnly or (Trim(CHECK_TYPE.Text) = 'Void'));

  if DM_PAYROLL.PR_CHECK.RecordCount = 0 then
  try
    Button2.Click;
  except
    BitBtn4.Click;
    raise;
  end;
  FCustomAfterPost := DM_PAYROLL.PR_CHECK_LINES.AfterPost;
  DM_PAYROLL.PR_CHECK_LINES.AfterPost := AfterChangePost;
  FCustomAfterDelete := DM_PAYROLL.PR_CHECK_LINES.AfterDelete;
  DM_PAYROLL.PR_CHECK_LINES.AfterDelete := AfterChangeDelete;
  FCustomValidate := DM_PAYROLL.PR_CHECK_LINES.FieldByName('RATE_OF_PAY').OnValidate;
  DM_PAYROLL.PR_CHECK_LINES.FieldByName('RATE_OF_PAY').OnValidate := ValidateRate;
  CheckChanged;
//  ctx_DataAccess.RecalcLineBeforePost := cbCalcCL.Checked;
  ctx_DataAccess.RecalcLineBeforePost := True;
  if (iWizardPosition > WIZ_UNKNOWN) and (iWizardPosition < WIZ_PREPROCESSED) then
  begin
    sbWizard.Visible := True;
    iWizardPosition := WIZ_BATCH_CREATED;
  end
  else
    sbWizard.Visible := False;
  with DM_COMPANY do
    InitNamedDSStates('GrossToNet', [CL_PERSON, CL_E_DS, CO, CO_STATES, CO_SUI, CO_LOCAL_TAX, CO_DIVISION_LOCALS, CO_BRANCH_LOCALS, CO_DEPARTMENT_LOCALS, CO_TEAM_LOCALS, CO_JOBS_LOCALS, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, EE_STATES, EE_LOCALS, EE_RATES, EE_SCHEDULED_E_DS, EE_WORK_SHIFTS, EE_PENSION_FUND_SPLITS]);

  PageControl1.OnChange(nil);  //fire the column spacing
end;

procedure TREDIT_CHECK.RecalcSum;
var
  fAmountSum: Currency;
  fHoursSum: Currency;
  bPR: Boolean;
  bNotPR: Boolean;
begin
  bPR := False;
  bNotPR := False;
  EEStatesFilter := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
  EESUIFilter := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('SUI_State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
  if DM_PAYROLL.PR_CHECK_LINES.RetrieveCondition <> '' then
  begin
    with TevClientDataSet.Create(nil) do
    try
      CloneCursor(DM_PAYROLL.PR_CHECK_LINES, False);
      First;
      fAmountSum := 0;
      fHoursSum := 0;
      while not Eof do
      begin
        bPR := bPR or
               (FieldValues['EE_STATES_NBR'] = EEStatesFilter) or
               (FieldValues['EE_SUI_STATES_NBR'] = EESUIFilter);
        bNotPR := bNotPR or
               (FieldValues['EE_STATES_NBR'] <> EEStatesFilter) and
               not FieldByName('EE_STATES_NBR').IsNull or
               (FieldValues['EE_SUI_STATES_NBR'] <> EESUIFilter) and
               not FieldByName('EE_SUI_STATES_NBR').IsNull;
        if Copy(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').AsInteger, 'e_d_code_type'), 1, 1) = 'D' then
        begin
          fAmountSum := fAmountSum - FieldByName('AMOUNT').AsFloat;
          fHoursSum := fHoursSum - FieldByName('HOURS_OR_PIECES').AsFloat;
        end
        else
        begin
          fAmountSum := fAmountSum + FieldByName('AMOUNT').AsFloat;
          fHoursSum := fHoursSum + FieldByName('HOURS_OR_PIECES').AsFloat;
        end;
        Next;
      end;
    finally
      Free;
    end;
    wwDBGrid1.ColumnByName('Hours_or_Pieces').FooterValue := FloatToStr(fHoursSum);
    wwDBGrid1.ColumnByName('Amount').FooterValue := FormatFloat('#,##0.00', fAmountSum);
  end;
  if bNotPR then
  begin
    EEStatesFilter := -EEStatesFilter;
    EESUIFilter := -EESUIFilter
  end
  else if not bPR then
  begin
    EEStatesFilter := 0;
    EESUIFilter := 0
  end
end;

procedure TREDIT_CHECK.DeleteMenuCLickHdl(Sender: TObject);
var
  aComponent: TComponent;
  aDataSource: TDataSource;
  I, J: integer;
begin
  for I := 0 to NumberOfDragDropFields - 1 do
    if TMenuItem(Sender).Caption = DragDropFields[I].D then
    begin
      with wwDBGrid1 do
      begin
        aDataSource := DataSource;
        DataSource := nil;
        for J := Selected.Count - 1 downto 0 do
          if POS(DragDropFields[I].F, Selected.Strings[J]) > 0 then
          begin
            Selected.Delete(J);
          end;

        DataSource := aDataSource;
        Invalidate;
      end;

      aComponent := FindComponent('ED_' + GetDropControlPrototype(DragDropFields[I].D).Name);
      if assigned(aComponent) then
        aComponent.Free;
      DragDropFields[I].V := -1;
    end;
end;

procedure TREDIT_CHECK.MenuCLickHdl(Sender: TObject);
var //TmpLkup : TevDBLookupCombo ;
//S : String ;
//aDataSource : TevDataSource ;
  I: integer;
begin

  for I := 0 to NumberOfDragDropFields - 1 do
    if TMenuItem(Sender).Caption = DragDropFields[I].D then
    begin
      AddToGrid(I);
      exit;
    end;

end;

function TREDIT_CHECK.GetDropControlPrototype(Description: string): TControl;
begin
  result := nil;

  if Description = 'DEPARTMENT' then
    result := lkupDepartment;

  if Description = 'DIVISION' then
    result := lkupDivision;

  if Description = 'BRANCH' then
    result := lkupBranch;

  if Description = 'TEAM' then
    result := lkupTeam;

  if Description = 'JOB' then
    result := lkupJobs;

  if Description = 'RATE NBR' then
    result := edtRateNo;

//SEG BEGIN
//lookups
  if Description = 'STATES' then
    result := lkupStates;

  if Description = 'SUI STATES' then
    result := lkupSUI;

  if Description = 'AGENCY NAME' then
    result := lkupAgency;

  if Description = 'WORKERS COMP' then
    result := lkupWorkComp;

  if Description = 'SHIFTS' then
    result := lkupShift;

  if Description = 'PIECES' then
    result := lkupPiece;

//edits
  if Description = 'RATE OF PAY' then
    result := edtRatePay;

  if Description = 'HOURS OR PIECES' then
    result := edtHours;

  if Description = 'LINE ITEM DATE' then
    result := dbtpDate;
//SEG END

end;

procedure TREDIT_CHECK.CalcCheckLine(Sender: TObject);
begin
  if (PageControl1.ActivePage = TabSheet1) and
     (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL) and
     (DM_PAYROLL.PR_CHECK_LINES.RecordCount > 0) then
  begin
    ctx_StartWait;

    with DM_PAYROLL do
    try
      PR_CHECK_LINES.Edit;
      if PR_CHECK_LINES.EE_SCHEDULED_E_DS_NBR.IsNull then
        ctx_PayrollCalculation.CalculateCheckLine(PR, PR_CHECK, PR_CHECK_LINES, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_E_D_GROUP_CODES, DM_CLIENT.CL_PERSON,
              DM_CLIENT.CL_PIECES, DM_COMPANY.CO_SHIFTS, DM_COMPANY.CO_STATES, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
              DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_WORK_SHIFTS, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
              DM_SYSTEM_STATE.SY_STATES)
      else
      begin
        Assert(EE_SCHEDULED_E_DS.Locate('EE_SCHEDULED_E_DS_NBR', PR_CHECK_LINES.EE_SCHEDULED_E_DS_NBR.Value, []));
        ctx_PayrollCalculation.ScheduledEDRecalc(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', PR_CHECK_LINES.CL_E_DS_NBR.Value, 'E_D_CODE_TYPE'),
              PR_CHECK.GROSS_WAGES.AsFloat, PR_CHECK.NET_WAGES.AsFloat, PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES,
              PR_CHECK_SUI, PR_CHECK_LOCALS, DM_CLIENT.CL_E_DS, DM_CLIENT.CL_PENSION, DM_COMPANY.CO_STATES,
              DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
              DM_EMPLOYEE.EE_STATES, DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
              DM_SYSTEM_STATE.SY_STATES);
      end;
      PR_CHECK_LINES.Post;
    finally
      ctx_EndWait;
    end;
  end;
end;

procedure TREDIT_CHECK.CheckLineGridPopupPopup(Sender: TObject);
var
  T, T2: TMenuItem;
  I, J: integer;
  bFieldOnGrid: Boolean;
begin
  inherited;

  while CheckLineGridPopup.Items.Count > 0 do
    CheckLineGridPopup.Items.Delete(0);

  if DM_PAYROLL.PR['STATUS'] = PAYROLL_STATUS_PROCESSED then
  begin
    T := TMenuItem.Create(Self);
    T.Caption := 'Redistribute DBDT';
    T.OnClick := RedistributeDBDT;
    CheckLineGridPopup.Items.Add(T);

    T := TMenuItem.Create(Self);
    T.Caption := '-';
    CheckLineGridPopup.Items.Add(T);
  end;

  if (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_MANUAL) and
     (DM_PAYROLL.PR_CHECK_LINES.RecordCount > 0) then
  begin
    T := TMenuItem.Create(Self);
    T.Action := CalculateCheckLine;
    CheckLineGridPopup.Items.Add(T);
  end;

  T := TMenuItem.Create(Self);
  T.Caption := 'Add';

  //Reso#49113. Andrew
  for I := 0 to NumberOfDragDropFields - 1 do begin
    bFieldOnGrid := False;
    for J := 0 to wwDBGrid1.Selected.Count - 1 do
      if pos(DragDropFields[I].F, wwDBGrid1.Selected.Strings[J]) = 1 then begin  // Field's name must be at start position
        bFieldOnGrid := True;
        break;
      end;
    if not (bFieldOnGrid) then begin
      T2 := TMenuItem.Create(Self);
      T2.Caption := DragDropFields[I].D;
      T2.OnClick := MenuCLickHdl;
      T.Add(T2);
    end;
  end;

  if T.Count > 0 then
    CheckLineGridPopup.Items.Add(T)
  else
    T.Free;

  T := TMenuItem.Create(Self);
  T.Caption := 'Delete';

  //Reso#49113. Andrew
  for I := 0 to NumberOfDragDropFields - 1 do
    for J := 0 to wwDBGrid1.Selected.Count - 1 do
      if pos(DragDropFields[I].F, wwDBGrid1.Selected.Strings[J]) = 1 then begin  // Field's name must be at start position
        T2 := TMenuItem.Create(Self);
        T2.Caption := DragDropFields[I].D;
        T2.OnClick := DeleteMenuCLickHdl;
        T.Add(T2);
        Break;
      end;

  if T.Count > 0 then
    CheckLineGridPopup.Items.Add(T)
  else
    T.Free;
end;

procedure TREDIT_CHECK.BitBtn4Click(Sender: TObject);
begin
  inherited;
//  if CalcForBatch then
    (Owner as TFramePackageTmpl).OpenFrame('TREDIT_BATCH')
//  else
//    (Owner as TFramePackageTmpl).OpenFrame('TREDIT_PAYROLL');
end;

procedure TREDIT_CHECK.RefreshPreview;
var
  s: string;
begin
  if PageControl1.ActivePage = tsPreview then
  begin
    s := DM_PAYROLL.PR_CHECK_LINES.IndexFieldNames;
    PopulateStringGrid(strPreviewGrid,
      DM_PAYROLL.PR_CHECK,
      DM_PAYROLL.PR_CHECK_LINES,
      DM_PAYROLL.PR_CHECK_STATES,
      DM_PAYROLL.PR_CHECK_SUI,
      DM_PAYROLL.PR_CHECK_LOCALS);
    DM_PAYROLL.PR_CHECK_LINES.IndexFieldNames := s;
  end;
end;

procedure TREDIT_CHECK.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = ManualTaxTabSheet then
    LoadManualTaxData;
  RefreshPreview;

  if PageControl1.ActivePage = TabSheet1 then
    SetGridPosition('HOURS_OR_PIECES');

  //GUI 2.0. Won't change any other way.
  wwDBGrid2.Columns[7].DisplayWidth := 8;
  if PageControl1.ActivePageIndex = 4 then
    strPreviewGrid.SetFocus;
end;

procedure TREDIT_CHECK.SetGridPosition(sName: string);
var
  i: Integer;
begin
  if wwdbGrid1.DataSource.DataSet.RecordCount > 0 then
  begin
    for i := 0 to Pred(wwDBGrid1.Selected.Count) do
      if Pos(sName + #9, wwDBGrid1.Selected[i]) > 0 then
      begin
        wwDBGrid1.SelectedIndex := i;
        Break;
      end;
  end
  else
    wwDBGrid1.SelectedIndex := 0;
end;

procedure TREDIT_CHECK.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  AllowChange := (wwdsList.Dataset.recordcount >0);

  if PageControl1.ActivePage = TabSheet1 then
    DM_PAYROLL.PR_CHECK_LINES.CheckBrowseMode;
  if PageControl1.ActivePage = ManualTaxTabSheet then
    UnloadManualTaxData;
end;

procedure TREDIT_CHECK.ED_CL_E_DS_LookupComboNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: string; var Accept: Boolean);
begin
  inherited;
  Accept := False;
end;

procedure TREDIT_CHECK.CreateCustomControl(sFieldName: string; AControl: TevDBLookupCombo);
var
  TmpLkup: TevDBLookupCombo;
  ctrl: TComponent;
begin
  if aControl = lkupDivision then
    lkupDivision.LookupTable := DM_CLIENT.CO_DIVISION;

  if aControl = lkupBranch then
    lkupBranch.LookupTable := DM_CLIENT.CO_BRANCH;

  if aControl = lkupDepartment then
    lkupDepartment.LookupTable := DM_CLIENT.CO_DEPARTMENT;

  if aControl = lkupTeam then
    lkupTeam.LookupTable := DM_CLIENT.CO_TEAM;

  TmpLkup := TevDBLookupCombo.Create(Self);
  ctrl := FindComponent('ED_' + aControl.Name);
  if Assigned(ctrl) then
    RemoveComponent(ctrl);

  TmpLkup.Visible := False;
  TmpLkup.Name := 'ED_' + aControl.Name;
  TmpLkup.Parent := Self;
  TmpLkup.DropDownAlignment := aControl.DropDownAlignment;
  TmpLkup.Style := aControl.Style;
  TmpLkup.AutoDropDown := aControl.AutoDropDown;
  TmpLkup.AllowClearKey := aControl.AllowClearKey;
  TmpLkup.Selected.Text := aControl.Selected.Text;
  if aControl.LookupTable = DM_EMPLOYEE.EE_WORK_SHIFTS then
    TmpLkup.LookupTable := DM_EMPLOYEE.EE_WORK_SHIFTS
  else
  begin
    if not aControl.LookupTable.Active then
      aControl.LookupTable.Active := True;
    TmpLkup.LookupTable := CreateLookupDataset(TevClientDataSet(aControl.LookupTable), Self);
    TevClientDataSet(TmpLkup.LookupTable).OnFilterRecord := aControl.LookupTable.OnFilterRecord;//gdy5
    TevClientDataSet(TmpLkup.LookupTable).Filtered := aControl.LookupTable.Filtered;//gdy5
  end;
  TmpLkup.LookupField := aControl.LookupField;
  TmpLkup.ShowButton := aControl.ShowButton;
  TmpLkup.UseTFields := aControl.UseTFields;
  TmpLkup.ControlInfoInDataSet := aControl.ControlInfoInDataSet;
  TmpLkup.DataSource := aControl.DataSource;
  TmpLkup.DataField := aControl.DataField;
  TmpLkup.ReadOnly := GetIfReadOnly;
  TmpLkup.OnDropDown := aControl.OnDropDown; //gdy5

  wwDBGrid1.SetControlType(sFieldName, fctCustom, TmpLkup.Name);

end;

procedure TREDIT_CHECK.AddToGrid(DragDropIndex: integer);
var
  aDataSource: TDataSource;
  S: string;
  aControl: TControl;
  i, iInd: integer;
begin
  aControl := GetDropControlPrototype(DragDropFields[DragDropIndex].D);

  iInd := DragDropFields[DragDropIndex].V;
  if DragDropFields[DragDropIndex].V <> -1 then
    for i := Succ(DragDropIndex) to Pred(NumberOfDragDropFields) do
      if (DragDropFields[i].V <> -1) and (DragDropFields[i].V < DragDropFields[DragDropIndex].V) then
        Dec(iInd);

  if aControl is TevDBLookupCombo then
    CreateCustomControl(DragDropFields[DragDropIndex].F, TevDBLookupCombo(AControl));
  with wwDBGrid1 do
  begin
    aDataSource := DataSource;
    DataSource := nil;
    if DragDropFields[DragDropIndex].V = -1 then
    begin
      S := Selected.Text;
      Selected.Text := DragDropFields[DragDropIndex].F + #9 + IntToStr(DragDropFields[DragDropIndex].L) + #9 +
        DragDropFields[DragDropIndex].D + #9 + 'F' + #13 + #10 + S;
      S := Selected.Text;
    end
    else
      Selected.Insert(iInd, DragDropFields[DragDropIndex].F + #9 + IntToStr(DragDropFields[DragDropIndex].L) + #9 +
        DragDropFields[DragDropIndex].D + #9 + 'F');
    DataSource := aDataSource;
    Invalidate;
  end;
  if DragDropFields[DragDropIndex].V = -1 then
  begin
    DragDropFields[DragDropIndex].V := 0;
    for i := 0 to Pred(NumberOfDragDropFields) do
      if (i <> DragDropIndex) and (DragDropFields[i].V <> -1) then
        Inc(DragDropFields[i].V);
  end;
end;

procedure TREDIT_CHECK.wwDBGrid1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
var //TmpLkup : TevDBLookupCombo ;
//aDataSource : TevDataSource ;
//S : String ;
  I: integer;
begin
  inherited;

  for I := 0 to NumberOfDragDropFields - 1 do
    if TControl(Source) = GetDropControlPrototype(DragDropFields[I].D) then
    begin
      AddToGrid(I);
      exit;
    end;

end;

procedure TREDIT_CHECK.wwDBGrid1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var
  I: integer;
begin
  inherited;
  Accept := False;

  for I := 0 to NumberOfDragDropFields - 1 do
    if TControl(Source) = GetDropControlPrototype(DragDropFields[I].D) then
    begin
      Accept := (Pos(DragDropFields[I].F, wwDBGrid1.Selected.Text) = 0);
      exit;
    end;

end;

procedure TREDIT_CHECK.BitBtn1Click(Sender: TObject);
var
  LT: TDataSet;

  procedure CheckMaxAmountHours;
  var
    lHours, lDollars: Double;
    eeNumber, eeName, ErrorMessage: String;
  begin
    if not DM_COMPANY.CO.MAXIMUM_HOURS_ON_CHECK.IsNull or not DM_COMPANY.CO.MAXIMUM_DOLLARS_ON_CHECK.IsNull then
    begin
      lHours := 0;
      lDollars := 0;
      DM_PAYROLL.PR_CHECK_LINES.First;
      while not DM_PAYROLL.PR_CHECK_LINES.EOF do
      begin
        if LeftStr(DM_CLIENT.CL_E_DS.NewLookup('CL_E_DS_NBR', DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'], 'E_D_CODE_TYPE'), 1) = 'D' then
        begin
          lHours := lHours - ConvertNull(DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES'], 0);
          lDollars := lDollars - ConvertNull(DM_PAYROLL.PR_CHECK_LINES['AMOUNT'], 0);
        end
        else
        begin
          lHours := lHours + ConvertNull(DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES'], 0);
          lDollars := lDollars + ConvertNull(DM_PAYROLL.PR_CHECK_LINES['AMOUNT'], 0);
        end;
        DM_PAYROLL.PR_CHECK_LINES.Next;
      end;
      if (not DM_COMPANY.CO.MAXIMUM_HOURS_ON_CHECK.IsNull and (lHours > DM_COMPANY.CO.MAXIMUM_HOURS_ON_CHECK.AsCurrency))
      or (not DM_COMPANY.CO.MAXIMUM_DOLLARS_ON_CHECK.IsNull and (lDollars > DM_COMPANY.CO.MAXIMUM_DOLLARS_ON_CHECK.AsCurrency)) then
      begin
        eeNumber := Trim(DM_CLIENT.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString);
        DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_CLIENT.EE['CL_PERSON_NBR'], []);
        eeName := Trim(DM_CLIENT.CL_PERSON.LAST_NAME.AsString)+' '+Trim(DM_CLIENT.CL_PERSON.FIRST_NAME.AsString);
        ErrorMessage := eeNumber +': ' +eeName;

        if (not DM_COMPANY.CO.MAXIMUM_HOURS_ON_CHECK.IsNull and (lHours > DM_COMPANY.CO.MAXIMUM_HOURS_ON_CHECK.AsCurrency))
        and (not DM_COMPANY.CO.MAXIMUM_DOLLARS_ON_CHECK.IsNull and (lDollars > DM_COMPANY.CO.MAXIMUM_DOLLARS_ON_CHECK.AsCurrency)) then
          ErrorMessage := ErrorMessage + #13 +'Total amount and hours exceed the maximum.'
        else
        if (not DM_COMPANY.CO.MAXIMUM_HOURS_ON_CHECK.IsNull and (lHours > DM_COMPANY.CO.MAXIMUM_HOURS_ON_CHECK.AsCurrency)) then
          ErrorMessage := ErrorMessage + #13 + 'Total hours exceeds the maximum.'
        else
          ErrorMessage := ErrorMessage + #13 + 'Total amount exceeds the maximum.';

        if DM_COMPANY.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_WARN then
        begin
          if (EvMessage(ErrorMessage+'. Would you like to continue?', mtConfirmation, [mbYes, mbNo]) <> mrYes) then
            AbortEx;
        end
        else
        if DM_COMPANY.CO.CO_PAYROLL_PROCESS_LIMITATIONS.AsString = CO_PAYROLL_PROCESS_LIMITATIONS_STOP then
        begin
          EvMessage(ErrorMessage, mtWarning, [mbOk]);
          AbortEx;
        end;
      end

    end;
  end;

begin
  DM_PAYROLL.PR_CHECK_LOCALS.DisableControls;
  try

    if not DM_CLIENT.PR_CHECK.FieldByName('CHECK_TYPE').IsNull
    and not ((DM_CLIENT.PR_CHECK.FieldByName('CHECK_TYPE').AsString[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_YTD, CHECK_TYPE2_QTD])
    and (DM_CLIENT.PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_USER)) then
      CheckMaxAmountHours;

    inherited;
    if (DM_PAYROLL.PR_CHECK.CHECK_TYPE.AsString = CHECK_TYPE2_MANUAL)
    and (DM_PAYROLL.PR_CHECK.UPDATE_BALANCE.AsString = GROUP_BOX_YES) then
    begin
      DM_PAYROLL.PR_CHECK_LINES.First;
      while not DM_PAYROLL.PR_CHECK_LINES.EOF do
      begin
        DM_PAYROLL.PR_CHECK_LINES.Edit;
        DM_PAYROLL.PR_CHECK_LINES.DEDUCTION_SHORTFALL_CARRYOVER.Clear;
        DM_PAYROLL.PR_CHECK_LINES.Post;
        DM_PAYROLL.PR_CHECK_LINES.Next;
      end;
      DM_PAYROLL.PR_CHECK_LINES.First;
    end;
    DM_COMPANY.PR_CHECK_LINES.DisableControls;
    LT := lcCLLocalName.LookupTable;
    lcCLLocalName.LookupTable := nil;
    try
      RecalculateCheck;
    finally
      lcCLLocalName.LookupTable := LT;
      DM_COMPANY.PR_CHECK_LINES.EnableControls;
    end;
    PageControl1.ActivePage := tsPreview;
    RefreshPreview;
  finally
    DM_PAYROLL.PR_CHECK_LOCALS.EnableControls;
  end;
end;

procedure TREDIT_CHECK.lkupDivisionDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  inherited;
  Accept := Sender = Source;
end;

procedure TREDIT_CHECK.dblkupEE_NBRChange(Sender: TObject);
begin
  inherited;
  if TevDBLookupCombo(Sender).Focused and (Trim(TevDBLookupCombo(Sender).Text) <> '') and
    (TevDBLookupCombo(Sender).LookupTable[TevDBLookupCombo(Sender).LookupField] <> DM_PAYROLL.PR_CHECK['EE_NBR']) then
  begin
    RecalcCheck;
    if DM_PAYROLL.PR_CHECK.Locate('EE_NBR', TevDBLookupCombo(Sender).LookupTable[TevDBLookupCombo(Sender).LookupField], []) then
      CheckChanged;
    if Sender = dbtxtname then
      dblkupEE_NBR.Value := TevDBLookupCombo(Sender).LookupTable.FieldbyName(TevDBLookupCombo(Sender).LookupField).AsString
    else
      dbtxtname.Value := TevDBLookupCombo(Sender).LookupTable.FieldbyName(TevDBLookupCombo(Sender).LookupField).AsString;
  end;
end;

procedure TREDIT_CHECK.CHECK_TYPEChange(Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then exit;  
  SetReadOnly(GetIfReadOnly or (Trim(CHECK_TYPE.Text) = 'Void'));
  if (ED_CL_E_DS_LookupCombo.LookupTable <> nil) then
    with TevClientDataSet(ED_CL_E_DS_LookupCombo.LookupTable) do
      if Active then
      begin
        RecalcOnIndex := True;
        Filter := 'E_D_CODE_TYPE=''' + ED_3RD_SHORT +
          ''' or E_D_CODE_TYPE=''' + ED_3RD_LONG +
          ''' or E_D_CODE_TYPE=''' + ED_3RD_NON_TAX + '''';
        Filtered := Trim(CHECK_TYPE.Text) = '3rd Party';
        RecalcOnIndex := False;
      end;
end;

procedure TREDIT_CHECK.dblkupEE_NBRCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  dblkupEE_NBRChange(Sender);
  inherited;
  if not Modified then
    TevDBLookupCombo(Sender).LookupValue := TevDBLookupCombo(Sender).OldLookup;
  if TevDBLookupCombo(Sender).LookupValue <> '' then
    TevDBLookupCombo(Sender).LookupTable.Locate(TevDBLookupCombo(Sender).LookupField, TevDBLookupCombo(Sender).LookupValue, []);
  if TevDBLookupCombo(Sender).Focused and (Trim(TevDBLookupCombo(Sender).Text) <> '') and
    (TevDBLookupCombo(Sender).LookupTable[TevDBLookupCombo(Sender).LookupField] <> DM_PAYROLL.PR_CHECK['EE_NBR']) then
  begin
    RecalcCheck;
    if not DM_PAYROLL.PR_CHECK.Locate('EE_NBR', TevDBLookupCombo(Sender).LookupTable[TevDBLookupCombo(Sender).LookupField], []) then
      TevDBLookupCombo(Sender).LookupTable.Locate(TevDBLookupCombo(Sender).LookupField, DM_PAYROLL.PR_CHECK['EE_NBR'], [])
    else
      CheckChanged;
  end;
  TevDBLookupCombo(Sender).LookupValue := TevDBLookupCombo(Sender).LookupTable['EE_NBR'];
  if Sender = dbtxtname then
    TevDBLookupCombo(Sender).Text := TevDBLookupCombo(Sender).LookupTable['Employee_Name_Calculate']
  else
    TevDBLookupCombo(Sender).Text := TevDBLookupCombo(Sender).LookupTable['Trim_NUMBER'];
end;

procedure TREDIT_CHECK.SpeedButton1Click(Sender: TObject);
var
  DBDTSelectionList: TDBDTSelectionListFiltr;
begin
  inherited;
  if DM_EMPLOYEE.EE_RATES.Active and
     DM_EMPLOYEE.EE_RATES.Locate('EE_NBR;RATE_NUMBER',
        VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'], DM_PAYROLL.PR_CHECK_LINES['RATE_NUMBER']]), []) and
     (not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DIVISION_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_DIVISION_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DIVISION_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_DIVISION_NBR').Value) or
      not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_BRANCH_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_BRANCH_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_BRANCH_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_BRANCH_NBR').Value) or
      not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DEPARTMENT_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_DEPARTMENT_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DEPARTMENT_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_DEPARTMENT_NBR').Value) or
      not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_TEAM_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_TEAM_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_TEAM_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_TEAM_NBR').Value)) then
    raise ECanNotPerformOperation.CreateHelp('This rate has DBDT override on EE rates level. Can not override on check line level', IDH_CanNotPerformOperation);

  DBDTSelectionList := TDBDTSelectionListFiltr.Create(nil);
  try
    with DM_COMPANY, DBDTSelectionList do
    begin
      CO_DIVISION.DataRequired('CO_NBR=' + CO.FieldByName('CO_NBR').AsString);
      DBDTSelectionList.Setup(TevClientDataSet(CO_DIVISION), TevClientDataSet(CO_BRANCH),
        TevClientDataSet(CO_DEPARTMENT), TevClientDataSet(CO_TEAM),
        DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value
        , DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value, DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value
        , DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionList.ShowModal = mrOK then
      begin
        if (DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value <> wwcsTempDBDT.FieldByName('DIVISION_NBR').Value)
          or (DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value <> wwcsTempDBDT.FieldByName('BRANCH_NBR').Value)
          or (DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value <> wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value)
          or (DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value <> wwcsTempDBDT.FieldByName('TEAM_NBR').Value) then
        begin
          if not (DM_PAYROLL.PR_CHECK_LINES.State in [dsInsert, dsEdit]) then
          begin
            if DM_PAYROLL.PR_CHECK_LINES.RecordCount = 0 then
              DM_PAYROLL.PR_CHECK_LINES.Insert
            else
              DM_PAYROLL.PR_CHECK_LINES.Edit;
          end;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value := wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value := wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value := wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value := wwcsTempDBDT.FieldByName('TEAM_NBR').Value;
        end;
      end;
    end;
  finally
    DBDTSelectionList.Free;
  end;
end;

procedure TREDIT_CHECK.wwDBGrid1ColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
var
  i, iCh: integer;
begin
  inherited;
  iCh := -1;
  for i := 0 to Pred(NumberOfDragDropFields) do
    if DragDropFields[i].V = FromIndex then
      iCh := i
    else
      if (FromIndex < ToIndex) and
      (DragDropFields[i].V > FromIndex) and
      (DragDropFields[i].V <= ToIndex) then
      Dec(DragDropFields[i].V)
    else
      if (FromIndex > ToIndex) and
      (DragDropFields[i].V < FromIndex) and
      (DragDropFields[i].V >= ToIndex) then
      Inc(DragDropFields[i].V);
  if iCh <> -1 then
    DragDropFields[iCh].V := ToIndex;
end;

procedure TREDIT_CHECK.wwDBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  //The following is now obsolete because the red is now replaced by the lock bitmap.
  {if (wwDBGrid1.DataSource <> nil) and
    wwDBGrid1.DataSource.DataSet.Active and
    (wwDBGrid1.DataSource.DataSet.FieldByName('LineTypeSort').AsString <> CHECK_LINE_TYPE_USER) then
    AFont.Color := clRed;   }
end;

procedure TREDIT_CHECK.wwDBGrid1CellChanged(Sender: TObject);
var
  i: integer;
begin
  inherited;
  wwDBGrid1.ReadOnly := GetIfReadOnly or (Trim(CHECK_TYPE.Text) = 'Void') or (wwDBGrid1.DataSource <> nil) and
    wwDBGrid1.DataSource.DataSet.Active and
    (wwDBGrid1.DataSource.DataSet.FieldByName('LineTypeSort').AsString <> CHECK_LINE_TYPE_USER) and
    (wwDBGrid1.DataSource.DataSet.FieldByName('LineTypeSort').AsString <> '');
  if not GetIfReadOnly then
    wwDBGrid1.ForceKeyOptions([dgAllowInsert, dgEnterToTab])
  else
    wwDBGrid1.ForceKeyOptions([dgEnterToTab]);
  wwDBGrid1.EditorMode := wwDBGrid1.EditorMode and not wwDBGrid1.ReadOnly;
  wwDBGrid1.InvalidateCurrentRow;
  SetCheckLineControlsReadOnly(wwDBGrid1.ReadOnly);
  if wwDBGrid1.ReadOnly then
    for i := 0 to Pred(ComponentCount) do
      if (Components[i] is TevDBLookupCombo) and
        (Copy(Components[i].Name, 1, 3) = 'ED_') and
        TevDBLookupCombo(Components[i]).Visible then
        TevDBLookupCombo(Components[i]).Visible := False;
end;

procedure TREDIT_CHECK.wwDBGrid1DrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
  inherited;
  if wwDBGrid1.ReadOnly and
    (wwDBGrid1.DataSource <> nil) and
    wwDBGrid1.DataSource.DataSet.Active and
    (wwDBGrid1.DataSource.DataSet.State = dsInsert) then
  begin
    wwDBGrid1.ReadOnly := GetIfReadOnly;
    if not GetIfReadOnly then
      wwDBGrid1.ForceKeyOptions([dgAllowInsert, dgEnterToTab])
    else
      wwDBGrid1.ForceKeyOptions([dgEnterToTab]);
    SetCheckLineControlsReadOnly(wwDBGrid1.ReadOnly);
  end;
end;

procedure TREDIT_CHECK.Button3Click(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  if (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED])
  or ((DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_REGULAR)
  and (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_MANUAL)) then
    Exit;
  if EvMessage('Are you sure you want to refresh Scheduled EDs for this check?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;

  ctx_StartWait;
  DM_PAYROLL.PR_CHECK_LINES.DisableControls;
  wwdbGrid1.DataSource := Nil;
  try
    with DM_PAYROLL do
    begin
      PR_CHECK_LINES.First;
      while not PR_CHECK_LINES.EOF do
      begin
        if (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED)
        or (PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_DISTR_SCHEDULED) then
        begin
          PR_CHECK_LINES.Delete;
          i := PR_CHECK_LINES.RecNo;
          PR_CHECK_LINES.ReSync([]);
          PR_CHECK_LINES.RecNo := i;
        end
        else
          PR_CHECK_LINES.Next;
      end;

      ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
        PR,
        PR_BATCH,
        PR_CHECK,
        PR_CHECK_LINES,
        PR_CHECK_STATES,
        PR_CHECK_SUI,
        PR_CHECK_LOCALS,
        PR_CHECK_LINE_LOCALS,
        PR_SCHEDULED_E_DS,
        DM_CLIENT.CL_E_DS,
        DM_CLIENT.CL_PERSON,
        DM_CLIENT.CL_PENSION,
        DM_COMPANY.CO,
        DM_COMPANY.CO_E_D_CODES,
        DM_COMPANY.CO_STATES,
        DM_EMPLOYEE.EE,
        DM_EMPLOYEE.EE_SCHEDULED_E_DS,
        DM_EMPLOYEE.EE_STATES,
        DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
        DM_SYSTEM_STATE.SY_STATES,
        False,
        (PR_BATCH.FieldByName('PAY_SALARY').AsString = 'Y'),
        (PR_BATCH.FieldByName('PAY_STANDARD_HOURS').AsString = 'Y'),
        True
      );

    end;
  finally
    wwdbGrid1.DataSource := wwdsDetail;
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
    ctx_EndWait;
  end;
  AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
end;

function TREDIT_CHECK.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly or not DM_PAYROLL.PR.Active or
    (DM_PAYROLL.PR.FieldByName('Status').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_COMPLETED,
    PAYROLL_STATUS_HOLD, PAYROLL_STATUS_VOIDED, PAYROLL_STATUS_PROCESSING, PAYROLL_STATUS_PREPROCESSING]);
  if ctx_DataAccess.GetClCoReadOnlyRight then
     Result := True;
end;

procedure TREDIT_CHECK.YTDBtnClick(Sender: TObject);
begin
  inherited;
  with CreateCoYTD do
  begin
    ClNbr := ctx_DataAccess.ClientID;
    CoNbr := DM_COMPANY.CO['co_nbr'];
    EeNbr := DM_PAYROLL.PR_CHECK['ee_nbr'];
    ShowModal;
//    Free;
  end;
end;

function TREDIT_CHECK.CanOpen: Boolean;
begin
  Result := (DetailPayrollOpen <> 0) and
            DM_TEMPORARY.TMP_CO.Active and DM_TEMPORARY.TMP_CO.Locate('CL_NBR; CO_NBR', VarArrayOf([DM_COMPANY.CO.ClientID, DM_COMPANY.CO['CO_NBR']]), []) and
            DM_PAYROLL.PR.Active and DM_PAYROLL.PR.Locate('PR_NBR', DetailPayrollOpen, []);
  if not Result then
  begin
    DetailPayrollOpen := 0;
    (Owner as TFramePackageTmpl).OpenFrame('TREDIT_PAYROLL');
  end;
  if Result and (DM_PAYROLL.PR_BATCH.RecordCount = 0) then
  begin
    EvMessage('There are no batches in payroll');
    (Owner as TFramePackageTmpl).OpenFrame('TREDIT_BATCH');
    Result := False;
  end;
end;

procedure TREDIT_CHECK.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  CheckNbrBeforeActivate := DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value;
  inherited;
  AddDS(DM_PAYROLL.PR_CHECK_LINES, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK_LINE_LOCALS, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK_LOCALS, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK_STATES, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK_SUI, EditDataSets);
  AddDS(DM_EMPLOYEE.EE, EditDataSets);
  AddDS(DM_PAYROLL.PR_SCHEDULED_EVENT, EditDataSets);
end;

procedure TREDIT_CHECK.wwdsMasterDataChangeBeforeChildren(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_EMPLOYEE.EE.Active then
    DM_EMPLOYEE.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
  if EEClone.Active then
    EEClone.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
end;

procedure TREDIT_CHECK.OnCheckScroll;
var
  i, j: Integer;
  aControl: TControl;
begin
  DM_EMPLOYEE.EE_STATES.Close;
  DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR='+DM_PAYROLL.PR_CHECK.EE_NBR.AsString);
  for i := 0 to Pred(NumberOfDragDropFields) do
    DragDropFields[i].V := -1;

  for j := 0 to Pred(wwDBGrid1.Selected.Count) do
  begin
    for i := 0 to Pred(NumberOfDragDropFields) do
    begin
      if AnsiCompareText(DragDropFields[i].F, Copy(wwDBGrid1.Selected[j], 1, Pred(Pos(#9, wwDBGrid1.Selected[j])))) = 0 then
      begin
        aControl := GetDropControlPrototype(DragDropFields[i].D);
        if aControl is TevDBLookupCombo then
          CreateCustomControl(DragDropFields[i].F, TevDBLookupCombo(AControl));
        DragDropFields[i].V := j;
        Break;
      end;
    end;
  end;
end;

procedure TREDIT_CHECK.AfterClick(Kind: Integer);
var
  SalaryCreate, HourlyCreate: Boolean;
  i, Template: Integer;
  Q: IevQuery;
begin
  inherited;
  if Kind in [NavFirst, NavPrevious, NavNext, NavLast, NavCancel] then
    OnCheckScroll;

  if Kind = NavDelete then
  begin
    if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
    begin
      ctx_DBAccess.StartTransaction([dbtClient]);
      try
        Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
        Q.Execute;
        (Owner as TFramePackageTmpl).ClickButton(NavCommit);
        ctx_DBAccess.CommitTransaction;
      except
        ctx_DBAccess.RollbackTransaction;
        raise;
      end;
    end;
  end;

  if Kind = NavInsert then
  begin
    DM_PAYROLL.PR_CHECK.DisableControls;
    DM_PAYROLL.PR_CHECK_LINES.DisableControls;
    DM_PAYROLL.PR_CHECK_LINE_LOCALS.DisableControls;
    DM_PAYROLL.PR_CHECK_LOCALS.DisableControls;
    DM_PAYROLL.PR_CHECK_STATES.DisableControls;
    DM_PAYROLL.PR_CHECK_SUI.DisableControls;
    DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger := CurrentCheckEE;
    DM_PAYROLL.PR_CHECK.FieldByName('TAX_FREQUENCY').AsString := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').AsString;
    EEClone.Filtered := False;
    EEClone.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
    try
      Template := Generic_PopulatePRCheckRecord(EmployeeBeforeInsert);
      SalaryCreate := (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_COBRA_CREDIT) and
                      ((DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_REGULAR) or
                      (EvMessage('Do you want to pay salary?', mtConfirmation, [mbYes, mbNo]) = mrYes));
      HourlyCreate := (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_COBRA_CREDIT) and
                      ((DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_REGULAR) or
                      (EvMessage('Do you want to pay regular hours?', mtConfirmation, [mbYes, mbNo]) = mrYes));

      ctx_DataAccess.StartNestedTransaction([dbtClient]);
      try
        with DM_PAYROLL, DM_CLIENT, DM_COMPANY, DM_SYSTEM_FEDERAL, DM_SYSTEM_STATE, DM_EMPLOYEE do
        try
          PR_CHECK.DisableControls;
          PR_CHECK_LINES.DisableControls;
          PR_CHECK.LookupsEnabled := False;
          PR_CHECK_LINES.LookupsEnabled := False;
          PR_CHECK.Post;
          ctx_DataAccess.TemplateNumber := Template;
          try
            ctx_PayrollCalculation.Generic_CreatePRCheckDetails(PR, PR_BATCH, PR_CHECK,
              PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS,
              PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES, CO_STATES,
              EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES, False, SalaryCreate, HourlyCreate);

            if (Template <> 0) and (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_STATUS').AsString <> CHECK_STATUS_VOID) then
              ctx_PayrollCalculation.CopyPRTemplateDetails(ctx_DataAccess.TemplateNumber, PR_CHECK, PR_CHECK_STATES,
                PR_CHECK_LOCALS, CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_STATES, EE_LOCALS);

          finally
            ctx_DataAccess.TemplateNumber := 0;
          end;
          if PR_CHECK.State <> dsBrowse then
            PR_CHECK.Post;
        finally
          PR_CHECK.EnableControls;
          PR_CHECK_LINES.EnableControls;
          PR_CHECK.LookupsEnabled := True;
          PR_CHECK_LINES.LookupsEnabled := True;
        end;

        if ctx_DataAccess.TemplateNumber <> 0 then
        begin
          with DM_PAYROLL, DM_COMPANY, DM_EMPLOYEE do
            ctx_PayrollCalculation.CopyPRTemplateDetails(ctx_DataAccess.TemplateNumber, PR_CHECK, PR_CHECK_STATES,
              PR_CHECK_LOCALS, CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_STATES, EE_LOCALS);
          ctx_DataAccess.TemplateNumber := 0;
        end;

        with DM_PAYROLL do
          ctx_DataAccess.PostDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS]);
        (Owner as TFramePackageTmpl).ClickButton(NavCommit);
        ctx_DataAccess.CommitNestedTransaction;

      except
        with DM_PAYROLL do
          ctx_DataAccess.CancelDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS]);
        ctx_DataAccess.RollbackNestedTransaction;
        raise;
      end;

    finally
      PR_CHECKClone.CloneCursor(DM_PAYROLL.PR_CHECK, False);
      CreateEEFilter;
      EEClone.CloneCursor(DM_EMPLOYEE.EE, False);
      EEClone.Filtered := True;
      EEClone.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);

      if DM_PAYROLL.PR_CHECK.State <> dsBrowse then
        DM_PAYROLL.PR_CHECK.Cancel;
      if DM_PAYROLL.PR_CHECK_LINES.State <> dsBrowse then
        DM_PAYROLL.PR_CHECK_LINES.Cancel;
      DM_PAYROLL.PR_CHECK.EnableControls;
      DM_PAYROLL.PR_CHECK_LINES.EnableControls;
      DM_PAYROLL.PR_CHECK_SUI.EnableControls;
      DM_PAYROLL.PR_CHECK_STATES.EnableControls;
      DM_PAYROLL.PR_CHECK_LOCALS.EnableControls;
      DM_PAYROLL.PR_CHECK_LINE_LOCALS.EnableControls;
      SetReadOnly(GetIfReadOnly or (Trim(CHECK_TYPE.Text) = 'Void'));  
    end;
    (Owner as TFramePackageTmpl).ClickButton(NavOK);

    try
      (Owner as TFramePackageTmpl).ClickButton(NavCommit);
       DM_PAYROLL.PR_CHECK_LINES.Indexes.ReBuildAll;
       DM_PAYROLL.PR_CHECK_LINES.Refresh;
    except
      with DM_PAYROLL do
        ctx_DataAccess.CancelDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS]);
      DM_PAYROLL.PR_CHECK_LINES.Close;
      DM_PAYROLL.PR_CHECK_LINES.Open;
      raise;
    end;
    BATCH_QUICK_PAY_CALCULATED := False;
  end;
  if Kind in [NavAbort] then
    RecalcSum;
  if Kind in [NavOK, NavCommit] then
    AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);

  if (Kind = NavAbort) and ctx_DataAccess.DelayedCommit then
    ResetCalc;
  if Kind = NavCommit then
    ManualTaxClientDataSet.MergeChangeLog;
  if Kind = NavAbort then
    ManualTaxClientDataSet.CancelUpdates;
  if not (Kind in [NavHistory, NavCommit]) then
    CheckChanged;
  if Kind = NavCommit then
    for i := 0 to Pred(NumberOfDragDropFields) do
      if DragDropFields[i].V <> -1 then
        wwDBGrid1.SetControlType(DragDropFields[i].F, fctCustom, 'ED_'+ GetDropControlPrototype(DragDropFields[i].D).Name);
end;

procedure TREDIT_CHECK.CheckChanged;
var
  TempDataSet: TevClientDataSet;
  i: Integer;
  s: string;
  sDBDT: string;
begin

  RecalcSum;

  if lkupStates.Focused then
    lkupStatesEnter(Self);

  if lkupSUI.Focused then
    lkupSUIEnter(Self);

  DBMemo1.LoadMemo;    
  if Length(Trim(DBMemo1.Lines.Text)) <> 0 then
    PageControl1.ActivePage := tsNotes;

   
  if DM_PAYROLL.PR_CHECK.State = dsBrowse then
  begin
    TempDataSet := TevClientDataSet.Create(Self);
    try
      TempDataSet.Data := TevClientDataSet(DM_PAYROLL.PR_CHECK).Data;
      if DM_PAYROLL.PR_CHECK.Filtered and (Trim(DM_PAYROLL.PR_CHECK.Filter) <> '') then
        TempDataSet.Filter := '(' + Trim(DM_PAYROLL.PR_CHECK.Filter) + ') and ' +
          'EE_NBR=' + IntToStr(DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger)
      else
        TempDataSet.Filter := 'EE_NBR=' + IntToStr(DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger);
      TempDataSet.Filtered := True;
      TempDataSet.Locate('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger, []);
      lblCheck.Caption := 'Check ' + IntToStr(TempDataSet.RecNo) + ' of ' + IntToStr(TempDataSet.RecordCount);
      TempDataSet.Filtered := False;
      if DM_PAYROLL.PR_CHECK.FieldByName('PR_BATCH_NBR').AsInteger <> DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger then
      begin
        TempDataSet.Data := TevClientDataSet(DM_PAYROLL.PR_BATCH).Data;
        TempDataSet.Locate('PR_BATCH_NBR', DM_PAYROLL.PR_CHECK.FieldByName('PR_BATCH_NBR').AsInteger, []);
        lblBatch.Caption := 'Batch ' + TempDataSet.FieldByName('PERIOD_BEGIN_DATE').AsString + '-' +
          TempDataSet.FieldByName('PERIOD_END_DATE').AsString;
      end
      else
        lblBatch.Caption := 'Batch ' + DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').AsString + '-' +
          DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').AsString;
    finally
      TempDataSet.Free;
    end;
  end;

  DM_EMPLOYEE.EE_WORK_SHIFTS.RetrieveCondition := 'EE_NBR = ' + IntToStr(DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger);

  cbRates.Items.Clear;
  DM_EMPLOYEE.EE_RATES.First;
  i := 0;
  while not DM_EMPLOYEE.EE_RATES.Eof do
  begin
    if DM_EMPLOYEE.EE_RATES.FieldByName('EE_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger then
    begin
      if DM_EMPLOYEE.EE_RATES.FieldByName('PRIMARY_RATE').AsString = 'Y' then
        s := 'P'
      else
        s := ' ';

      sDBDT := '';
      if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_DIVISION_NBR').IsNull and
         ((DM_EMPLOYEE.EE_RATES['CO_DIVISION_NBR'] = DM_COMPANY.CO_DIVISION['CO_DIVISION_NBR']) or
           DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', DM_EMPLOYEE.EE_RATES['CO_DIVISION_NBR'], [])) and
         (DM_COMPANY.CO_DIVISION.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
        sDBDT := sDBDT + Trim(DM_COMPANY.CO_DIVISION.FieldByName('CUSTOM_DIVISION_NUMBER').AsString);
      if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_BRANCH_NBR').IsNull and
         ((DM_EMPLOYEE.EE_RATES['CO_BRANCH_NBR'] = DM_COMPANY.CO_BRANCH['CO_BRANCH_NBR']) or
           DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', DM_EMPLOYEE.EE_RATES['CO_BRANCH_NBR'], [])) and
         (DM_COMPANY.CO_BRANCH.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
        if sDBDT <> '' then
          sDBDT := sDBDT + ' | ';
        sDBDT := sDBDT + Trim(DM_COMPANY.CO_BRANCH.FieldByName('CUSTOM_BRANCH_NUMBER').AsString);
      end;
      if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_DEPARTMENT_NBR').IsNull and
         ((DM_EMPLOYEE.EE_RATES['CO_DEPARTMENT_NBR'] = DM_COMPANY.CO_DEPARTMENT['CO_DEPARTMENT_NBR']) or
           DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', DM_EMPLOYEE.EE_RATES['CO_DEPARTMENT_NBR'], [])) and
         (DM_COMPANY.CO_DEPARTMENT.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
        if sDBDT <> '' then
          sDBDT := sDBDT + ' | ';
        sDBDT := sDBDT + Trim(DM_COMPANY.CO_DEPARTMENT.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString);
      end;
      if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_TEAM_NBR').IsNull and
         ((DM_EMPLOYEE.EE_RATES['CO_TEAM_NBR'] = DM_COMPANY.CO_TEAM['CO_TEAM_NBR']) or
           DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', DM_EMPLOYEE.EE_RATES['CO_TEAM_NBR'], [])) and
         (DM_COMPANY.CO_TEAM.FieldByName('PRINT_GROUP_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
      begin
        if sDBDT <> '' then
          sDBDT := sDBDT + ' | ';
        sDBDT := sDBDT + Trim(DM_COMPANY.CO_TEAM.FieldByName('CUSTOM_TEAM_NUMBER').AsString);
      end;

      cbRates.Items.Add(Format('%2d%1.1s %9.9s    %0.40s %0.40s %0.5s',
        [DM_EMPLOYEE.EE_RATES.FieldByName('RATE_NUMBER').AsInteger, s,
         FormatFloat(TFloatField(DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT')).DisplayFormat,
                     DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat),
         sDBDT,
         VarToStr(DM_COMPANY.CO_JOBS.Lookup('CO_JOBS_NBR', DM_EMPLOYEE.EE_RATES['CO_JOBS_NBR'], 'DESCRIPTION')),
         VarToStr(DM_COMPANY.CO_WORKERS_COMP.Lookup('CO_WORKERS_COMP_NBR', DM_EMPLOYEE.EE_RATES['CO_WORKERS_COMP_NBR'], 'WORKERS_COMP_CODE'))
         ]));
      if s = 'P' then i := Pred(cbRates.Items.Count);
    end;
    DM_EMPLOYEE.EE_RATES.Next;
  end;
  if cbRates.Items.Count > 0 then cbRates.ItemIndex := i;
  Label45.Caption := Format('%9m', [DM_EMPLOYEE.EE.FieldByName('SALARY_AMOUNT').AsFloat]);
  RefreshPreview;

  dblkupEE_NBR.Value := ConvertNull(DM_PAYROLL.PR_CHECK['EE_NBR'], '');
  dbtxtname.Value := ConvertNull(DM_PAYROLL.PR_CHECK['EE_NBR'], '');

  s := '';
  if not DM_EMPLOYEE.EE.FieldByName('CO_DIVISION_NBR').IsNull and
     ((DM_EMPLOYEE.EE['CO_DIVISION_NBR'] = DM_COMPANY.CO_DIVISION['CO_DIVISION_NBR']) or
       DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', DM_EMPLOYEE.EE['CO_DIVISION_NBR'], [])) and
     (DM_COMPANY.CO_DIVISION.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
    s := s + Trim(DM_COMPANY.CO_DIVISION.FieldByName('CUSTOM_DIVISION_NUMBER').AsString);
  if not DM_EMPLOYEE.EE.FieldByName('CO_BRANCH_NBR').IsNull and
     ((DM_EMPLOYEE.EE['CO_BRANCH_NBR'] = DM_COMPANY.CO_BRANCH['CO_BRANCH_NBR']) or
       DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', DM_EMPLOYEE.EE['CO_BRANCH_NBR'], [])) and
     (DM_COMPANY.CO_BRANCH.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
  begin
    if s <> '' then
      s := s + ' | ';
    s := s + Trim(DM_COMPANY.CO_BRANCH.FieldByName('CUSTOM_BRANCH_NUMBER').AsString);
  end;
  if not DM_EMPLOYEE.EE.FieldByName('CO_DEPARTMENT_NBR').IsNull and
     ((DM_EMPLOYEE.EE['CO_DEPARTMENT_NBR'] = DM_COMPANY.CO_DEPARTMENT['CO_DEPARTMENT_NBR']) or
       DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', DM_EMPLOYEE.EE['CO_DEPARTMENT_NBR'], [])) and
     (DM_COMPANY.CO_DEPARTMENT.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
  begin
    if s <> '' then
      s := s + ' | ';
    s := s + Trim(DM_COMPANY.CO_DEPARTMENT.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString);
  end;
  if not DM_EMPLOYEE.EE.FieldByName('CO_TEAM_NBR').IsNull and
     ((DM_EMPLOYEE.EE['CO_TEAM_NBR'] = DM_COMPANY.CO_TEAM['CO_TEAM_NBR']) or
       DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', DM_EMPLOYEE.EE['CO_TEAM_NBR'], [])) and
     (DM_COMPANY.CO_TEAM.FieldByName('PRINT_GROUP_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
  begin
    if s <> '' then
      s := s + ' | ';
    s := s + Trim(DM_COMPANY.CO_TEAM.FieldByName('CUSTOM_TEAM_NUMBER').AsString);
  end;
  lblDBDT.Caption := s;

  if (DM_EMPLOYEE.EE_STATES.RecordCount = 0) then
  begin
    DM_EMPLOYEE.EE_STATES.Close;
    DM_EMPLOYEE.EE_STATES.Open;
  end;

//  if PageControl1.ActivePage = TabSheet1 then
  SetGridPosition('HOURS_OR_PIECES');
  LocateUserLine;
  if PageControl1.ActivePage = ManualTaxTabSheet then
  begin
    UnloadManualTaxData;
    LoadManualTaxData;
  end;
end;

procedure TREDIT_CHECK.Deactivate;
var
  i: Integer;
begin
  RecalcCheck;
  ED_CL_E_DS_LookupCombo.LookupTable.Filter := '';
  TevClientDataSet(ED_CL_E_DS_LookupCombo.LookupTable).Filtered := False;
  ctx_DataAccess.CheckMaxAmountAndHours := False;

  EEClone.Data := null;
  EEClone.Filtered := False;
  EEClone.OnFilterRecord := nil;
  if DM_EMPLOYEE.EE.Active then
    DM_EMPLOYEE.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
  DM_COMPANY.CO_JOBS.Filtered := False;
  FreeCoYTD;
  DM_PAYROLL.PR_CHECK_LINES.AfterPost := FCustomAfterPost;
  DM_PAYROLL.PR_CHECK_LINES.AfterDelete := FCustomAfterDelete;
  DM_PAYROLL.PR_CHECK_LINES.FieldByName('RATE_OF_PAY').OnValidate := FCustomValidate;

  mb_AppSettings.DeleteNode('Misc\' + Self.ClassName + '\wwDBGrid1\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
    IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));

  mb_AppSettings.AsInteger['Misc\' + Self.ClassName + '\wwDBGrid1\Version'] := wwDBGrid1.Tag;
  for i := 0 to Pred(wwDBGrid1.Selected.Count) do
    mb_AppSettings['Misc\' + Self.ClassName + '\wwDBGrid1\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
      IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger) + '\Column#' + IntToStr(i)] := wwDBGrid1.Selected[i];

  DeactivateDBDTSupport; //gdy5

  ctx_DataAccess.RecalcLineBeforePost := True;
  DM_EMPLOYEE.EE_STATES.Filter := '';
  DM_EMPLOYEE.EE_STATES.Filtered := False;
  DM_EMPLOYEE.EE_WORK_SHIFTS.RetrieveCondition := '';

  InitializingManualTaxClientDataSet := True;

  if PageControl1.ActivePage = ManualTaxTabSheet then
    UnloadManualTaxData;

  inherited;

end;

procedure TREDIT_CHECK.UnloadManualTaxData;
begin
  ManualTaxClientDataSet.CheckBrowseMode;
end;

procedure TREDIT_CHECK.LoadManualTaxData;
var
//ControlLabel : string ;
//ID : integer ;
  RowNbr: integer;

  procedure AddControl(
    LabelCaption: string;
    DataSetName: string;
    IDFieldName: string;
    ID: integer;
    AmountFieldName: string;
    Amount: Variant;
    const RO: Boolean);
  begin
    RowNbr := RowNbr + 1;

    with ManualTaxClientDataSet do
    begin
      Insert;
      FieldByName('Label').AsString := LabelCaption;
      FieldByName('Amount').Value := Amount;
      FieldByName('DataSetName').AsString := DataSetName;
      FieldByName('IDFieldName').AsString := IDFieldName;
      FieldByName('AmountFieldName').AsString := AmountFieldName;
      FieldByName('ID').AsInteger := ID;
      FieldByName('RowNbr').AsInteger := RowNbr;
      FieldByName('RO').AsBoolean := RO;
      Post;
    end;
  end;

  procedure AddField(LabelCaption: string;
    IDFieldName: string;
    aField: TField;
    const RO: Boolean = False);
  begin

    AddControl(
      LabelCaption,
      aField.DataSet.Name,
      IDFieldName,
      aField.DataSet.FieldByName(IDFieldName).AsInteger,
      aField.FieldName,
      aField.Value,
      RO);

  end;
begin
  RowNbr := 0;
  InitializingManualTaxClientDataSet := True;
  ManualTaxClientDataSet.DisableControls;

  DM_PAYROLL.PR_CHECK_LOCALS.DisableControls;
  DM_PAYROLL.PR_CHECK_STATES.DisableControls;
  DM_PAYROLL.PR_CHECK_SUI.DisableControls;

  try
    with ManualTaxClientDataSet do
      EmptyDataSet;

    if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) or
       (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString = OVERRIDE_VALUE_TYPE_NONE) then
      AddField('Federal',
        'PR_CHECK_NBR',
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE'));

    AddField('OASDI',
      'PR_CHECK_NBR',
      DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI'),
      FRoOasdiMedicare);

    AddField('Medicare',
      'PR_CHECK_NBR',
      DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE'),
      FRoOasdiMedicare);

    AddField('EIC',
      'PR_CHECK_NBR',
      DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_EIC'));

    AddField('Back-up Withholding',
      'PR_CHECK_NBR',
      DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING'));

    with DM_PAYROLL.PR_CHECK_STATES do
    begin
      First;
      while not EOF do
      begin
        if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) or
           (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString = OVERRIDE_VALUE_TYPE_NONE) then
          AddField('STATE - ' + FieldByName('STATE_Lookup').AsString,
            'EE_STATES_NBR',
            FieldByName('STATE_OVERRIDE_VALUE'));

        AddField('SDI - ' + FieldByName('SDI_Lookup').AsString,
          'EE_STATES_NBR',
          FieldByName('EE_SDI_OVERRIDE'));
        Next;
      end;
    end;

    with DM_PAYROLL.PR_CHECK_SUI do
    begin
      First;
      while not EOF do
      begin
        AddField('SUI - ' + FieldByName('SUI_Lookup').AsString,
          'CO_SUI_NBR',
          FieldByName('OVERRIDE_AMOUNT'));

        Next;
      end;
    end;

    with DM_PAYROLL.PR_CHECK_LOCALS do
    begin
      First;
      while not EOF do
      begin
        AddField('Local - ' + FieldByName('Local_Name_Lookup').AsString,
          'EE_LOCALS_NBR',
          FieldByName('OVERRIDE_AMOUNT'));
        Next;
      end;
    end;

  finally
    ManualTaxClientDataSet.EnableControls;
    ManualTaxClientDataSet.MergeChangeLog;
    ManualTaxClientDataSet.First;
    DM_PAYROLL.PR_CHECK_LOCALS.EnableControls;
    DM_PAYROLL.PR_CHECK_STATES.EnableControls;
    DM_PAYROLL.PR_CHECK_SUI.EnableControls;
    InitializingManualTaxClientDataSet := False;
  end;
end;

procedure TREDIT_CHECK.ManualTaxClientDataSetAfterPost(DataSet: TDataSet);
begin
  if InitializingManualTaxClientDataSet then
    exit;

  AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
  (Owner as TFramePackageTmpl).ClickButton(NavOK);
end;

function TREDIT_CHECK.GetDefaultDataSet: TevClientDataSet;
begin


  if DM_PAYROLL.PR.Active then
  begin
      DM_PAYROLL.PR_CHECK.PrepareLookups;
      ctx_DataAccess.OpenEEDataSetForCompany(DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR['CO_NBR'], 'PR_NBR = ' + DM_PAYROLL.PR.PR_NBR.AsString);
      DM_PAYROLL.PR_CHECK.UnPrepareLookups;
  end;
  Result := DM_PAYROLL.PR_CHECK;
end;

procedure TREDIT_CHECK.FocusNbrExecute(Sender: TObject);
begin
  inherited;
  dblkupEE_NBR.SetFocus;
end;

procedure TREDIT_CHECK.FocusNameExecute(Sender: TObject);
begin
  inherited;
  dbtxtname.SetFocus;
end;

procedure TREDIT_CHECK.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  Q: IevQuery;
begin
  inherited;

  if (Kind in [NavOk, NavCommit]) and (ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') <> stEnabled) then
    VerifyManualCheckChange(ConvertNull(DM_CLIENT.PR.PR_NBR.Value, '0'), ConvertNull(DM_CLIENT.PR_CHECK.PR_CHECK_NBR.Value, '0'));

  if (Kind in [NavOk, NavCommit]) and (DM_EMPLOYEE.EE_STATES.RecordCount = 0) then
  begin
    DM_EMPLOYEE.EE_STATES.Close;
    DM_EMPLOYEE.EE_STATES.Open;
  end;


  if Kind in [NavInsert, NavFirst, NavPrevious, NavNext, NavLast] then
    RecalcCheck;
  if Kind = NavDelete then
  begin
    if EvMessage('Are you sure you want to delete this check?', mtConfirmation, mbOKCancel) = mrOK then
    begin
      AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
      EEClone.Filtered := False;
      EEClone.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
      try
        if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
        begin
          (Owner as TFramePackageTmpl).ClickButton(NavOK);
          (Owner as TFramePackageTmpl).ClickButton(NavCommit);
          ctx_DBAccess.StartTransaction([dbtClient]);
          try
            Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
            Q.Execute;
            DM_PAYROLL.PR_CHECK.Delete;
            (Owner as TFramePackageTmpl).ClickButton(NavOK);
            (Owner as TFramePackageTmpl).ClickButton(NavCommit);
            ctx_DBAccess.CommitTransaction;
          except
            ctx_DBAccess.RollbackTransaction;
            raise;
          end;
        end
        else
          DM_PAYROLL.PR_CHECK.Delete;
      finally
        CreateEEFilter;
        EEClone.Filtered := True;
        EEClone.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
      end;
    end;
    AfterClick(NavDelete);
    Handled := True;
  end
  else if Kind = NavInsert then
  begin
    if not CalcForBatch and (DM_PAYROLL.PR_BATCH.RecordCount <> 1) then
    begin
      EvErrMessage('You must open check from the batch screen');
      Handled := True;
    end;
    CurrentCheckEE := DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger;
    EmployeeBeforeInsert := Trim(DM_PAYROLL.PR_CHECK.FieldByName('EE_Custom_Number').AsString);
  end;
end;

procedure TREDIT_CHECK.Button2Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavInsert);
end;

function TREDIT_CHECK.GetDataSetConditions(sName: string): string;
begin
{  //'CO_NBR = 9 AND (PR_NBR = 414)'
  Result := CheckUplinkedRetrieveCondition(sName,
     //'CO_NBR = '+IntToStr(DM_PAYROLL.CO.CO_NBR.AsInteger)+' AND (PR_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger)+')');
     'PR_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger)+'');
}
  Result := DM_PAYROLL.PR_CHECK.OpenCondition;
end;

procedure TREDIT_CHECK.SetReadOnly(Value: Boolean);
begin
  if not Value then
     ClearReadOnly;
  inherited;
  DBEdit4.SecurityRO := GetIfReadOnly or (Trim(CHECK_TYPE.Text) = 'Regular') or (Trim(CHECK_TYPE.Text) = 'Void');
  if DBEdit4.ReadOnly then
    DBEdit4.Color := clBtnFace
  else
    DBEdit4.Color := clWindow;

  dblkupEE_NBR.SecurityRO := False;
  dblkupEE_NBR.Color := clWindow;
  dbtxtname.SecurityRO := False;
  dbtxtname.Color := clWindow;

  Button3.SecurityRO := GetIfReadOnly or (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT) or
                                     ((Trim(CHECK_TYPE.Text) <> 'Regular') and (Trim(CHECK_TYPE.Text) <> 'Manual'));
  strPreviewGrid.SecurityRO := True;
  wwDBGrid4.KeyOptions := wwDBGrid4.KeyOptions - [dgAllowDelete, dgAllowInsert];
  sbtnCheckFinder.SecurityRO := ctx_DataAccess.GetClCoReadOnlyRight;
  sbtnTaxCalc.SecurityRO :=  sbtnCheckFinder.SecurityRO;
end;

procedure TREDIT_CHECK.wwDBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //the following code was brought forward from the isClasses ancestor
  //for some unknown reason, likely the painting routines, this never
  //gets fired.
  inherited;

  if not GetIfReadOnly and (Key = VK_DELETE) and (Shift = [ssCtrl]) and
    (wwDBGrid1.DataSource.DataSet.RecordCount > 0) and (Trim(CHECK_TYPE.Text) <> 'Void') then
  begin
    if EvMessage('Delete this check line?', mtConfirmation, mbOKCancel) = mrOK then
    begin
      if TypeIsPension(DM_CLIENT.CL_E_DS.Lookup('CL_E_DS_NBR', DM_PAYROLL.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').Value, 'E_D_CODE_TYPE')) then
        EvMessage('Please, do not forget to delete the Match code.');
      wwDBGrid1.DataSource.DataSet.Delete;
      AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
    end;
    Key := 0;
  end;
  if not GetIfReadOnly and (Key = VK_INSERT) and (Trim(CHECK_TYPE.Text) <> 'Void') then
  begin
    wwDBGrid1.DataSource.DataSet.Insert;
    Key := 0;
  end;
  if (Char(Key) = 'J') and (Shift = [ssCtrl]) then
    sbAddJob.Click;
end;

procedure TREDIT_CHECK.NextCheckExecute(Sender: TObject);
var
  EE_NBR: String;
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavOK);
  (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  RecalcCheck;
  EE_NBR := DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsString;
  while (not DM_PAYROLL.PR_CHECK.Eof) and
    (EE_NBR = DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsString) do
    DM_PAYROLL.PR_CHECK.Next;
  DM_PAYROLL.PR_CHECK.DisableControls;
  DM_PAYROLL.PR_CHECK.EnableControls;
  CheckChanged;
  OnCheckScroll;
end;

procedure TREDIT_CHECK.PriorCheckExecute(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavOK);
  RecalcCheck;
  DM_PAYROLL.PR_CHECK.Prior;
  CheckChanged;
end;

procedure TREDIT_CHECK.AfterChangeDelete(DataSet: TDataSet);
begin
  RecalcSum;
  if Assigned(FCustomAfterDelete) then
    FCustomAfterDelete(DataSet);
  DM_EMPLOYEE.EE_WORK_SHIFTS.RetrieveCondition := 'EE_NBR = ' + IntToStr(DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger);
end;

procedure TREDIT_CHECK.AfterChangePost(DataSet: TDataSet);
begin
  RecalcSum;
  if Assigned(FCustomAfterPost) then
    FCustomAfterPost(DataSet);
  DM_EMPLOYEE.EE_WORK_SHIFTS.RetrieveCondition := 'EE_NBR = ' + IntToStr(DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger);
end;

procedure TREDIT_CHECK.ValidateRate(Sender: TField);
begin
  if not Sender.DataSet.ControlsDisabled and ctx_DataAccess.Validate and
     (Sender.DataSet.FieldbyName('RATE_NUMBER').AsInteger <> 0) then
    Sender.DataSet.FieldbyName('RATE_NUMBER').Clear;
  if Assigned(FCustomValidate) then
    FCustomValidate(Sender);
end;

procedure TREDIT_CHECK.EEShowFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
//  Accept := PR_CHECKClone.Locate('EE_NBR', DataSet['EE_NBR'], []);
  Accept := Pos(',' + DataSet.FieldByName('EE_NBR').AsString + ',', sEEFilter) > 0;
end;

procedure TREDIT_CHECK.OnSetButton(Kind: Integer; var Value: Boolean);
begin
  inherited;
  if Kind = NavOK then
    BitBtn1.Enabled := not GetIfReadOnly and (Trim(CHECK_TYPE.Text) <> 'Void') and not Value;
end;

procedure TREDIT_CHECK.FocusMainExecute(Sender: TObject);
begin
  inherited;
  PutFocus(Self);
end;

procedure TREDIT_CHECK.CalculateCheckLineExecute(Sender: TObject);
begin
  inherited;
  CalcCheckLine(Self);
end;

procedure TREDIT_CHECK.wwDBGrid1KeyPress(Sender: TObject; var Key: Char);
//var
//  Combo: TevDBLookupCombo; //gdy
begin
//  inherited;
//SEG BEGIN

  case Key of
  #9, #13,#27: exit;
  end;
{
//begin modified by gdy
  if Sender = wwDBGrid1 then
    with TwwDBGrid(Sender) do
      if Assigned(DataSource) and Assigned(DataSource.DataSet) and DataSource.DataSet.Active  then
      begin
        Combo := nil;
        if GetActiveField.FieldName = 'CO_DIVISION_NBR_DESC' then
          Combo := lkupDivision
        else if GetActiveField.FieldName = 'CO_DEPARTMENT_NBR_DESC' then
          Combo := lkupDepartment
        else if GetActiveField.FieldName = 'CO_BRANCH_NBR_DESC' then
          Combo := lkupBranch
        else if GetActiveField.FieldName = 'CO_TEAM_NBR_DESC' then
          Combo := lkupTeam;
        if assigned(Combo) then
          if Combo.LookupTable.Locate( Combo.Lookupfield, DM_PAYROLL.PR_CHECK_LINES[Combo.DataField],[] ) then
            InitDBDTList(Key,Combo.LookupTable['NAME'])
          else
            InitDBDTList(Key,'');

      end;

  if (Sender = lkupDivision) or (Sender = lkupBranch) or
     (Sender = lkupDepartment) or (Sender = lkupTeam) then
  with TevDBLookupCombo(Sender) do
  if Assigned(DataSource) and Assigned(DataSource.DataSet) and
     DataSource.DataSet.Active  then
    if LookupTable.Locate( Lookupfield, DM_PAYROLL.PR_CHECK_LINES[DataField],[] ) then
      InitDBDTList(Key,LookupTable['NAME'])
    else
      InitDBDTList(Key,'');
//end modified by gdy
}

  if Sender = wwDBGrid1 then
    with TwwDBGrid(Sender) do
      if Assigned(DataSource) and Assigned(DataSource.DataSet) and
         DataSource.DataSet.Active then
        if GetActiveField.FieldName = 'CO_DEPARTMENT_NBR_DESC' then
          InitDBDTList(Key, 3)
        else if GetActiveField.FieldName = 'CO_TEAM_NBR_DESC' then
          InitDBDTList(Key, 4)
        else if GetActiveField.FieldName = 'CO_BRANCH_NBR_DESC' then
          InitDBDTList(Key, 2)
        else if GetActiveField.FieldName = 'CO_DIVISION_NBR_DESC' then
          InitDBDTList(Key, 1);

  if (Sender = lkupDivision) or (Sender = lkupBranch) or
     (Sender = lkupDepartment) or (Sender = lkupTeam) then
    with TevDBLookupCombo(Sender) do
      if Assigned(DataSource) and Assigned(DataSource.DataSet) and
         DataSource.DataSet.Active then
        if Sender = lkupDivision then
          InitDBDTList(Key,1)
        else if Sender = lkupBranch then
          InitDBDTList(Key,2)
        else if Sender = lkupDepartment then
          InitDBDTList(Key,3)
        else if Sender = lkupTeam then
          InitDBDTList(Key,4);

//SEG END
end;

//SEG BEGIN
procedure TREDIT_CHECK.InitDBDTList(var Key: Char; Level: Integer);
var
  initfilter: string;
  sstart, slength: Integer;
  DBDTSelectionListFiltr: TDBDTSelectionListFiltr;
begin
  DBDTSelectionListFiltr := TDBDTSelectionListFiltr.Create(nil);
  try
    if key <> #32 then
    begin
      initfilter := Trim(DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR_DESC').AsString);
      sstart := 0;
      slength := Length(initfilter);
      if Level >= 2 then
      begin
        sstart := Length(initfilter);
        slength := Length(Trim(DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR_DESC').AsString));
        initfilter := initfilter + Trim(DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR_DESC').AsString);
      end;
      if Level >= 3 then
      begin
        sstart := Length(initfilter);
        slength := Length(Trim(DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR_DESC').AsString));
        initfilter := initfilter + Trim(DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR_DESC').AsString);
      end;
      if Level >= 4 then
      begin
        sstart := Length(initfilter);
        slength := Length(Trim(DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR_DESC').AsString));
        initfilter := initfilter + Trim(DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR_DESC').AsString);
      end;
      DBDTSelectionListFiltr.edtFiltr.Text := initfilter;
      DBDTSelectionListFiltr.edtFiltr.SelStart := sstart;
      DBDTSelectionListFiltr.edtFiltr.SelLength := slength;
    end
    else
      DBDTSelectionListFiltr.edtFiltr.Clear;
    if (key <> #0) and (key <> #32) {and (initfilter='')} then  //gdy
      SendMessage(DBDTSelectionListFiltr.edtFiltr.Handle, WM_Char, word(Key), 0);
    Key := #0;
    with DM_COMPANY, DBDTSelectionListFiltr do
    begin
      CO_DIVISION.DataRequired('CO_NBR=' + CO.FieldByName('CO_NBR').AsString);
      DBDTSelectionListFiltr.Setup(TevClientDataSet(CO_DIVISION), TevClientDataSet(CO_BRANCH),
        TevClientDataSet(CO_DEPARTMENT), TevClientDataSet(CO_TEAM),
        DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value
        , DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value, DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value
        , DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionListFiltr.ShowModal = mrOK then
      begin
        if (DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value <> wwcsTempDBDT.FieldByName('DIVISION_NBR').Value)
          or (DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value <> wwcsTempDBDT.FieldByName('BRANCH_NBR').Value)
          or (DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value <> wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value)
          or (DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value <> wwcsTempDBDT.FieldByName('TEAM_NBR').Value) then
        begin
          if not (DM_PAYROLL.PR_CHECK_LINES.State in [dsInsert, dsEdit]) then
          begin
            if DM_PAYROLL.PR_CHECK_LINES.RecordCount = 0 then
              DM_PAYROLL.PR_CHECK_LINES.Insert
            else
              DM_PAYROLL.PR_CHECK_LINES.Edit;
          end;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value := wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value := wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value := wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
          DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value := wwcsTempDBDT.FieldByName('TEAM_NBR').Value;
        end;
      end;
    end;
  finally
    FreeAndNil(DBDTSelectionListFiltr); //gdy
  end;
end;
//SEG END

procedure TREDIT_CHECK.btnAddStatesClick(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  if DM_EMPLOYEE.EE_STATES.RecordCount = 0 then
    Exit
  else if DM_EMPLOYEE.EE_STATES.RecordCount = 1 then
    AddState
  else
    with TSelectSLDlg.Create(Self) do
    begin
      grdSelect.Selected.Text := 'State_Lookup'#9'10'#9'State';
      grdSelect.DataSource := EEStatesSrc;
      if ShowModal = mrOK then
      begin
        if grdSelect.SelectedList.Count = 0 then
          AddState
        else
          for i := 0 to Pred(grdSelect.SelectedList.Count) do
          begin
            EEStatesSrc.DataSet.GotoBookmark(grdSelect.SelectedList[i]);
            AddState;
          end;
      end;
      Free;
    end;
  UnloadManualTaxData;
  LoadManualTaxData;
  AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_CHECK.btnAddLocalsClick(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  if DM_EMPLOYEE.EE_LOCALS.RecordCount = 0 then
    Exit
  else if DM_EMPLOYEE.EE_LOCALS.RecordCount = 1 then
    AddLocal
  else
    with TSelectSLDlg.Create(Self) do
    begin
      grdSelect.Selected.Text := 'Local_Name_Lookup'#9'30'#9'Locality';
      grdSelect.DataSource := EELocalsSrc;
      if ShowModal = mrOK then
      begin
        if grdSelect.SelectedList.Count = 0 then
          AddLocal
        else
          for i := 0 to Pred(grdSelect.SelectedList.Count) do
          begin
            EELocalsSrc.DataSet.GotoBookmark(grdSelect.SelectedList[i]);
            AddLocal;
          end;
      end;
      Free;
    end;
  UnloadManualTaxData;
  LoadManualTaxData;
  AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_CHECK.ShowLocalsExecute(Sender: TObject);
begin
  inherited;
//SEG BEGIN
  FORM_ADD2CHECK_LINES_LOCALS := TFORM_ADD2CHECK_LINES_LOCALS.Create(Self);
  FORM_ADD2CHECK_LINES_LOCALS.ShowModal;
//SEG END
end;

procedure TREDIT_CHECK.ShowLocalsUpdate(Sender: TObject);
begin
  inherited;
//SEG BEGIN
  (Sender as TAction).Enabled :=
     (DM_PAYROLL.PR_CHECK_LINES.State in [dsBrowse]) and
     (DM_PAYROLL.PR_CHECK_LINES.RecordCount > 0);
//SEG BEGIN
end;

procedure TREDIT_CHECK.CreateEEFilter;
begin
  PR_CHECKClone.First;
  sEEFilter := ',';
  while not PR_CHECKClone.Eof do
  begin
    sEEFilter := sEEFilter + PR_CHECKClone.FieldByName('EE_NBR').AsString + ',';
    PR_CHECKClone.Next;
  end;
end;

procedure TREDIT_CHECK.wwdsMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  SaveLastCheckPosition(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
end;

//begin gdy5
procedure TREDIT_CHECK.ActivateDBDTSupport;
var
  pvout: IDBDTCache;
begin
  CO_DIVISIONClone := nil;
  CO_BRANCHClone := nil;
  CO_DEPARTMENTClone := nil;
  CO_TEAMClone := nil;
  DBDTFilter := nil;

  if DM_PAYROLL.PR_CHECK_LINES.Owner.GetInterface(IDBDTCache,pvout) then
  begin
    pvout.EnableDBDTCache;

    DBDTFilter := TDBDTFilterContext.Create( pvout );

    CO_DIVISIONClone := TEvClientDataSet.Create( Self );
    CO_BRANCHClone := TEvClientDataSet.Create( Self );
    CO_DEPARTMENTClone := TEvClientDataSet.Create( Self );
    CO_TEAMClone := TEvClientDataSet.Create( Self );

    CO_DIVISIONClone.CloneCursor( DM_COMPANY.CO_DIVISION, false, false );
    CO_BRANCHClone.CloneCursor( DM_COMPANY.CO_BRANCH, false, false );
    CO_DEPARTMENTClone.CloneCursor( DM_COMPANY.CO_DEPARTMENT, false, false );
    CO_TEAMClone.CloneCursor( DM_COMPANY.CO_TEAM, false, false );

    CO_DIVISIONClone.OnFilterRecord := DBDTFilter.CO_DIVISIONFilterRecord;
    CO_BRANCHClone.OnFilterRecord := DBDTFilter.CO_BRANCHFilterRecord;
    CO_DEPARTMENTClone.OnFilterRecord := DBDTFilter.CO_DEPARTMENTFilterRecord;
    CO_TEAMClone.OnFilterRecord := DBDTFilter.CO_TEAMFilterRecord;

    lkupDivision.LookupTable := CO_DIVISIONClone;
    lkupBranch.LookupTable := CO_BRANCHClone;
    lkupDepartment.LookupTable := CO_DEPARTMENTClone;
    lkupTeam.LookupTable := CO_TEAMClone;

  end;
end;

procedure TREDIT_CHECK.DeactivateDBDTSupport;
var
  pvout: IDBDTCache;
begin
  lkupDivision.LookupTable := DM_COMPANY.CO_DIVISION;
  lkupBranch.LookupTable := DM_COMPANY.CO_BRANCH;
  lkupDepartment.LookupTable := DM_COMPANY.CO_DEPARTMENT;
  lkupTeam.LookupTable := DM_COMPANY.CO_TEAM;

  FreeAndNil( CO_DIVISIONClone );
  FreeAndNil( CO_BRANCHClone );
  FreeAndNil( CO_DEPARTMENTClone );
  FreeAndNil( CO_TEAMClone );

  FreeAndNil( DBDTFilter );

  if DM_PAYROLL.PR_CHECK_LINES.Owner.GetInterface(IDBDTCache,pvout) then
    pvout.DisableDBDTCache;
end;

procedure TREDIT_CHECK.lkupDBDTDropDown(Sender: TObject);
var
  FilterString: String;
  LT: TevClientDataSet;
begin
  if not bDontCheck and
     DM_EMPLOYEE.EE_RATES.Active and
     DM_EMPLOYEE.EE_RATES.Locate('EE_NBR;RATE_NUMBER',
        VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'], DM_PAYROLL.PR_CHECK_LINES['RATE_NUMBER']]), []) and
     (not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DIVISION_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_DIVISION_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DIVISION_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_DIVISION_NBR').Value) or
      not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_BRANCH_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_BRANCH_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_BRANCH_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_BRANCH_NBR').Value) or
      not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DEPARTMENT_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_DEPARTMENT_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DEPARTMENT_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_DEPARTMENT_NBR').Value) or
      not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_TEAM_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_TEAM_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_TEAM_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_TEAM_NBR').Value)) then
    raise ECanNotPerformOperation.CreateHelp('This rate has DBDT override on EE rates level. Can not override on check line level', IDH_CanNotPerformOperation);

  LT := TevClientDataSet((Sender as TevDbLookupcombo).LookupTable);

  FilterString := LT.Filter;

  DBDTFilter.SetParams( DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR') as TIntegerField,
                        DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR') as TIntegerField,
                        DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR') as TIntegerField,
                        DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR') as TIntegerField );


  if (TevDBLookupCombo(Sender).DataField = 'CO_BRANCH_NBR_DESC') then
  begin
    if DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DIVISION_NBR').IsNull then
      LT.Filtered := False
    else
    begin
      FilterString := 'CO_DIVISION_NBR='+DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DIVISION_NBR').AsString;
      if (LT.Filter <> FilterString) or not LT.Filtered then
      begin
        LT.Filter := FilterString;
        LT.Filtered := True;
      end;
    end;
  end
  else
  if (TevDBLookupCombo(Sender).DataField = 'CO_DEPARTMENT_NBR_DESC') then
  begin
    if DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_BRANCH_NBR').IsNull then
      LT.Filtered := False
    else
    begin
      FilterString := 'CO_BRANCH_NBR='+DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_BRANCH_NBR').AsString;
      if (LT.Filter <> FilterString) or not LT.Filtered then
      begin
        LT.Filter := FilterString;
        LT.Filtered := True;
      end;
    end;  
  end
  else
  if (TevDBLookupCombo(Sender).DataField = 'CO_TEAM_NBR_DESC') then
  begin
    if DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DEPARTMENT_NBR').IsNull then
      LT.Filtered := False
    else
    begin
      FilterString := 'CO_DEPARTMENT_NBR='+DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DEPARTMENT_NBR').AsString;
      if (LT.Filter <> FilterString) or not LT.Filtered then
      begin
        LT.Filter := FilterString;
        LT.Filtered := True;
      end;
    end;  
  end;

(*
  if (FilterString <> LT.Filter) or not LT.Filtered then
  begin
    if LT.Filtered then
      LT.Filtered := not LT.Filtered;
    LT.Filtered := not LT.Filtered;
  end;
*)
end;
//end gdy5

procedure TREDIT_CHECK.wwdsDetailUpdateData(Sender: TObject);
begin
  inherited;
  AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_CHECK.DS_PR_CHECK_LINESDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_COMPANY.CO_DIVISION.Lookup('CO_DIVISION_NBR', DS_PR_CHECK_LINES.DataSet['CO_DIVISION_NBR'], 'PRINT_DIV_ADDRESS_ON_CHECKS') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
    lkupDivision.DataSource := nil
  else
    lkupDivision.DataSource := DS_PR_CHECK_LINES;
  if DM_COMPANY.CO_BRANCH.Lookup('CO_BRANCH_NBR', DS_PR_CHECK_LINES.DataSet['CO_BRANCH_NBR'], 'PRINT_BRANCH_ADDRESS_ON_CHECK') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
    lkupBranch.DataSource := nil
  else
    lkupBranch.DataSource := DS_PR_CHECK_LINES;
  if DM_COMPANY.CO_DEPARTMENT.Lookup('CO_DEPARTMENT_NBR', DS_PR_CHECK_LINES.DataSet['CO_DEPARTMENT_NBR'], 'PRINT_DEPT_ADDRESS_ON_CHECKS') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
    lkupDepartment.DataSource := nil
  else
    lkupDepartment.DataSource := DS_PR_CHECK_LINES;
  if DM_COMPANY.CO_TEAM.Lookup('CO_TEAM_NBR', DS_PR_CHECK_LINES.DataSet['CO_TEAM_NBR'], 'PRINT_GROUP_ADDRESS_ON_CHECK') = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
    lkupTeam.DataSource := nil
  else
    lkupTeam.DataSource := DS_PR_CHECK_LINES;
end;

procedure TREDIT_CHECK.sbtnTaxCalcClick(Sender: TObject);
var
  i: Integer;
  bRefreshPr, bRefreshBatch: Boolean;
  Nbr: Integer;
  s: string;
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavCommit);  
  TaxCalculator := TTaxCalculator.Create(Nil);
  try
    TaxCalculator.ShowModal;

    bRefreshPr := False;
    bRefreshBatch := False;
    for i := 0 to High(TaxCalculator.ModifiedNbr) do
    begin
      if TaxCalculator.ModifiedNbr[i].PrNbr <> DM_PAYROLL.PR['PR_NBR'] then
        bRefreshPr := True;
      if (TaxCalculator.ModifiedNbr[i].PrNbr = DM_PAYROLL.PR['PR_NBR']) and
         (TaxCalculator.ModifiedNbr[i].PrBatchNbr <> DM_PAYROLL.PR_BATCH['PR_BATCH_NBR']) then
        bRefreshBatch := True;
    end;

    if bRefreshPr then
    begin
      DM_PAYROLL.PR.DisableControls;
      Nbr := DM_PAYROLL.PR['PR_NBR'];
      try
        s := DM_PAYROLL.PR.RetrieveCondition;
        DM_PAYROLL.PR.RetrieveCondition := DM_PAYROLL.PR.OpenCondition;
        DM_PAYROLL.PR.Close;
        DM_PAYROLL.PR.Open;
        DM_PAYROLL.PR.RetrieveCondition := s;
      finally
        DM_PAYROLL.PR.Locate('PR_NBR', Nbr, []);
        DM_PAYROLL.PR.EnableControls;
      end;
    end;

    if bRefreshBatch or TaxCalculator.NewCheckAdded then
    begin
      DM_PAYROLL.PR_BATCH.DisableControls;
      Nbr := DM_PAYROLL.PR_BATCH['PR_BATCH_NBR'];
      try
        s := DM_PAYROLL.PR_BATCH.RetrieveCondition;
        DM_PAYROLL.PR_BATCH.RetrieveCondition := DM_PAYROLL.PR_BATCH.OpenCondition;
        DM_PAYROLL.PR_BATCH.Close;
        DM_PAYROLL.PR_BATCH.Open;
        DM_PAYROLL.PR_BATCH.RetrieveCondition := s;
      finally
        DM_PAYROLL.PR_BATCH.Locate('PR_BATCH_NBR', Nbr, []);
        DM_PAYROLL.PR_BATCH.EnableControls;
      end;
    end;

  finally
    TaxCalculator.Free;
  end;
end;

procedure TREDIT_CHECK.butnALD_or_ChangeClick(Sender: TObject);
var
  TempStr: String;
  AllOK: Boolean;
begin
  if butnALD_or_Change.Caption = 'Preview ALD (F6)' then
  begin
    if wwdsMaster.DataSet.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID then
      Exit;

    ctx_StartWait;
    try
      with DM_PAYROLL, DM_EMPLOYEE, DM_CLIENT, DM_COMPANY do
      begin
        CL_E_D_GROUP_CODES.DataRequired('ALL');
        EE_AUTOLABOR_DISTRIBUTION.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);

        Labor_Distribution_Preview := TLabor_Distribution_Preview.Create(Self);
        try
          Labor_Distribution_Preview.ShowDistribution(PR_CHECK.FieldByName('EE_NBR').AsInteger, PR_CHECK_LINES, EE,
            EE_AUTOLABOR_DISTRIBUTION, CL_E_DS, CL_E_D_GROUP_CODES, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, CO_JOBS,
              CO_WORKERS_COMP);
        finally
          Labor_Distribution_Preview.Free;
        end;
      end;
    finally
      ctx_EndWait;
    end;
  end
  else
    with DM_PAYROLL.PR_CHECK_LINES do
    begin
      ctx_DataAccess.UnlockPR;
      ctx_DataAccess.RecalcLineBeforePost := False;
      AllOK := False;
      try
        Edit;
        if FieldByName('AGENCY_STATUS').Value = CHECK_LINE_AGENCY_STATUS_PENDING then
        begin
          FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_IGNORE;
          FieldByName('CL_AGENCY_NBR').Value := Null;
        end
        else
        begin
          FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_PENDING;
          Payroll_Expert := TPayroll_Expert.Create(Self);
          with Payroll_Expert do
          try
            wwdsTemp.DataSet := DM_PAYROLL.PR_CHECK_LINES;
// Get Client Agency Number from a user
            repeat
              TempStr := DisplayExpert(crLookupCombo, 'Please, select an agency for this check line.', DM_CLIENT.CL_AGENCY,
                VarArrayOf(['CL_AGENCY_NBR', 'AGENCY_NAME' + #9 + '40' + #9 + 'AGENCY_NAME', 'CL_AGENCY_NBR']));
            until TempStr <> '';
          finally
            Payroll_Expert.Free;
          end;
        end;
        Post;
        AllOK := True;
      finally
        ctx_DataAccess.RecalcLineBeforePost := True;
        ctx_DataAccess.LockPR(AllOK);
      end;
    end;
end;

procedure TREDIT_CHECK.wwdsDetailDataChange(Sender: TObject; Field: TField);
var
  PayrollScr: IevPayrollScreens;
begin
  inherited;

  PayrollScr := Context.GetScreenPackage(IevPayrollScreens) as IevPayrollScreens;

  if Assigned(Field) then
  begin
    if (Field.FieldName = 'HOURS_OR_PIECES')
    or (Field.FieldName = 'CL_E_DS_NBR') then
      if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString[1] in [PAYROLL_TYPE_REGULAR,
        PAYROLL_TYPE_SUPPLEMENTAL, PAYROLL_TYPE_CL_CORRECTION, PAYROLL_TYPE_SB_CORRECTION] then
        if not Field.DataSet.FieldByName('HOURS_OR_PIECES').IsNull
        and not Field.DataSet.FieldByName('CL_E_DS_NBR').IsNull then
          PayrollScr.CheckToaBalance;
    if (Field.FieldName = 'EE_STATES_NBR') or
       (Field.FieldName = 'EE_SUI_STATES_NBR') then
      if DM_PAYROLL.PR_CHECK_LINES.FieldByName('EE_STATES_NBR').IsNull and
         DM_PAYROLL.PR_CHECK_LINES.FieldByName('EE_SUI_STATES_NBR').IsNull then
      begin
//        EEStatesFilter := 0;
//        EESUIFilter := 0
      end
      else if (DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', DM_PAYROLL.PR_CHECK_LINES['EE_STATES_NBR'], 'State_Lookup') = 'PR') or
              (DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', DM_PAYROLL.PR_CHECK_LINES['EE_SUI_STATES_NBR'], 'State_Lookup') = 'PR') then
      begin
        EEStatesFilter := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
        EESUIFilter := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('SUI_State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
      end
      else
      begin
        EEStatesFilter := -ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
        EESUIFilter := -ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('SUI_State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
      end;
  end;
  butnALD_or_Change.Enabled := not DM_PAYROLL.PR_CHECK_LINES.FieldByName('PR_CHECK_LINES_NBR').IsNull;
  if (Length(DM_PAYROLL.PR.FieldByName('STATUS').AsString) > 0) and
  (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED]) then
  begin
    if DM_PAYROLL.PR_CHECK_LINES.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').IsNull then
    begin
      butnALD_or_Change.Enabled := True;
      if DM_PAYROLL.PR_CHECK_LINES.FieldByName('AGENCY_STATUS').Value = CHECK_LINE_AGENCY_STATUS_PENDING then
        butnALD_or_Change.Caption := 'Make "Ignore" (F6)'
      else
        butnALD_or_Change.Caption := 'Make "Pending" (F6)';
    end
    else
    begin
      butnALD_or_Change.Enabled := False;
      butnALD_or_Change.Caption := 'Can''t change';
    end;
  end
  else
    butnALD_or_Change.Caption := 'Preview ALD (F6)';
  //Added for reso #47048 by Andrew. Disables Recalculate Check button for void checks.
  BitBtn1.Enabled := DM_PAYROLL.PR_CHECK.FieldByName('CHECK_STATUS').Value <> CHECK_STATUS_VOID;
end;

procedure TREDIT_CHECK.SetCheckLineControlsReadOnly(Value: Boolean);
begin
  lkupBranch.ReadOnly := Value;
  lkupDepartment.ReadOnly := Value;
  lkupDivision.ReadOnly := Value;
  lkupTeam.ReadOnly := Value;

  SpeedButton1.Enabled := not (Trim(CHECK_TYPE.Text) = 'Void');

  lkupStates.ReadOnly := Value;
  lkupSUI.ReadOnly := Value;
  lkupWorkComp.ReadOnly := GetIfReadOnly or (Trim(CHECK_TYPE.Text) = 'Void');
  lkupJobs.ReadOnly := GetIfReadOnly or (Trim(CHECK_TYPE.Text) = 'Void');

  sbAddJob.Enabled := not (Trim(CHECK_TYPE.Text) = 'Void');

  lkupAgency.ReadOnly := Value;
  lkupShift.ReadOnly := Value;
  lkupPiece.ReadOnly := Value;

  edtRateNo.ReadOnly := Value;
  edtHours.ReadOnly := Value;
  edtRatePay.ReadOnly := Value;
  dbtpDate.ReadOnly := Value;
  dbtpEndDate.ReadOnly := Value;

  evDBGrid1.ReadOnly := Value;
end;

procedure TREDIT_CHECK.sbAddJobClick(Sender: TObject);
var
  s: string;
  i: Integer;
begin
  inherited;
  s := EvDefaultDialog('Job Code', 'Enter new Job Code', '');
  if s <> '' then
    with DM_COMPANY.CO_JOBS do
    begin
      Append;
      FieldValues['DESCRIPTION'] := s;
      FieldValues['TRUE_DESCRIPTION'] := s;
      FieldValues['JOB_ACTIVE'] := 'Y';
      FieldValues['CERTIFIED'] := 'N';
      Post;
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO_JOBS]);
      if Assigned(Self.FindComponent('ED_' + lkupJobs.Name)) then
      begin
        TevClientDataSet(TevDBLookupCombo(Self.FindComponent('ED_' + lkupJobs.Name)).LookupTable).Data := Data;
        for i := 0 to Pred(wwDBGrid1.Selected.Count) do
          if Pos('CO_JOBS_NBR_DESC'#9, wwDBGrid1.Selected[i]) > 0 then
          begin
            wwDBGrid1.SelectedIndex := i;
            Break;
          end;
      end;

      if DM_PAYROLL.PR_CHECK_LINES.State = dsBrowse then
        DM_PAYROLL.PR_CHECK_LINES.Edit;
      DM_PAYROLL.PR_CHECK_LINES['CO_JOBS_NBR'] := FieldValues['CO_JOBS_NBR'];

    end;
end;

procedure TREDIT_CHECK.lkupJobsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Char(Key) = 'J') and (Shift = [ssCtrl]) then
    sbAddJob.Click;
end;

procedure TREDIT_CHECK.sbWizardClick(Sender: TObject);
begin
  inherited;
  BitBtn4.Click;
end;

procedure TREDIT_CHECK.sbtnCheckFinderClick(Sender: TObject);
var
  OldCheckNbr: Integer;
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  OldCheckNbr := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
  CheckFinder := TCheckFinder.Create(Self);
  try
    CheckFinder.editEENbr.Text := Trim(DM_PAYROLL.PR_CHECK.FieldByName('EE_Custom_Number').AsString);
    CheckFinder.DisableOKButton := True;
    CheckFinder.ShowModal;
  finally
    CheckFinder.Free;
  end;
  OpenDetailPayroll;
  DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', OldCheckNbr, []);
end;

procedure TREDIT_CHECK.wwdsDetailDataChangeBeforeChildren(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Assigned(DBDTFilter) then
  begin
    bDontCheck := True;
    try
      lkupDBDTDropDown( lkupDivision );
      lkupDBDTDropDown( lkupBranch );
      lkupDBDTDropDown( lkupDepartment );
      lkupDBDTDropDown( lkupTeam );
    finally
      bDontCheck := False;
    end;
  end;
end;

procedure TREDIT_CHECK.ManualTaxClientDataSetBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if DataSet.FieldByName('RO').AsBoolean then
    AbortEx;
end;

procedure TREDIT_CHECK.aCreateNextCheckExecute(Sender: TObject);
var
  eeNbr, batchNbr: Integer;
  nc: TCreateNextCheck;
begin
  inherited;
  RecalcCheck;
  nc := TCreateNextCheck.Create(nil);
  with nc do
  try
    if ShowModal = mrOK then
    begin
      eeNbr := DM_PAYROLL.PR_CHECK['EE_NBR'];
      batchNbr := DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'];
      DM_PAYROLL.PR_CHECK.Append;
      DM_PAYROLL.PR_CHECK['EE_NBR'] := eeNbr;
      DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'] := batchNbr;
      DM_PAYROLL.PR_CHECK['CHECK_TYPE'] := CHECK_TYPE2_REGULAR;
      with ctx_PayrollCalculation do
      begin
        DM_PAYROLL.PR_CHECK['CUSTOM_PR_BANK_ACCT_NUMBER'] := GetCustomBankAccountNumber(DM_PAYROLL.PR_CHECK);
        DM_PAYROLL.PR_CHECK['ABA_NUMBER'] := GetABANumber;
        DM_PAYROLL.PR_CHECK.UpdateBlobData('NOTES_NBR', DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').AsString);

        if lcTemplate.LookupValue <> '' then
          AssignCheckDefaults(StrToInt(lcTemplate.LookupValue), GetNextPaymentSerialNbr)
        else
          AssignCheckDefaults(0, GetNextPaymentSerialNbr);
      end;
      DM_PAYROLL.PR_CHECK.Post;
      with DM_CLIENT, DM_SYSTEM_FEDERAL, DM_SYSTEM_STATE do
      try
        PR_CHECK.DisableControls;
        PR_CHECK_LINES.DisableControls;
        PR_CHECK.LookupsEnabled := False;
        PR_CHECK_LINES.LookupsEnabled := False;
        ctx_PayrollCalculation.Generic_CreatePRCheckDetails(PR, PR_BATCH, PR_CHECK,
          PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS,
          PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES, CO_STATES,
          EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES, False, gSalary.ItemIndex = 0, gStandardHours.ItemIndex = 0);
        if PR_CHECK.State <> dsBrowse then
          PR_CHECK.Post;
      finally
        PR_CHECK.EnableControls;
        PR_CHECK_LINES.EnableControls;
        PR_CHECK.LookupsEnabled := True;
        PR_CHECK_LINES.LookupsEnabled := True;
      end;

      if lcTemplate.LookupValue <> '' then
      begin
        ctx_DataAccess.TemplateNumber := StrToInt(lcTemplate.LookupValue);
        with DM_PAYROLL, DM_COMPANY, DM_EMPLOYEE do
          ctx_PayrollCalculation.CopyPRTemplateDetails(ctx_DataAccess.TemplateNumber, PR_CHECK, PR_CHECK_STATES,
            PR_CHECK_LOCALS, CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_STATES, EE_LOCALS);
        ctx_DataAccess.TemplateNumber := 0;
      end;
    end;
  finally
    Free;
  end;
  try
    (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  except
    with DM_PAYROLL do
      ctx_DataAccess.CancelDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS]);
    DM_PAYROLL.PR_CHECK_LINES.Close;
    DM_PAYROLL.PR_CHECK_LINES.Open;
    raise;
  end;
  CheckChanged;
end;

procedure TREDIT_CHECK.aVoidNextCheckExecute(Sender: TObject);
var
  CheckToVoid: Integer;
  eeNbr, batchNbr: Integer;
  eeCustom: string;
  QtrOfVoided, QtrOfVoiding: Integer;
  Q: IevQuery;
begin
  inherited;
  RecalcCheck;
  eeCustom := Trim(DM_PAYROLL.PR_CHECK.FieldByName('EE_Custom_Number').AsString);
  batchNbr := DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'];

  CheckToVoid := 0;
  CheckFinder := TCheckFinder.Create(nil);
  try
    CheckFinder.editEENbr.Text := eeCustom;
    CheckFinder.DisableShowButton := True;
    CheckFinder.AdditionalFilter := '((C.CHECK_STATUS=''' + CHECK_STATUS_OUTSTANDING + ''') or (C.CHECK_STATUS=''' + CHECK_STATUS_OBC_SENT + '''))';
    CheckFinder.dtpkBeginCheckDate.Checked := True;
    CheckFinder.VoidCheckControl := True;
    CheckFinder.dtpkEndCheckDate.Checked := True;
    CheckFinder.dtpkEndCheckDate.Enabled := True;
    CheckFinder.dtpkBeginCheckDate.Date := GetBeginQuarter(DM_PAYROLL.PR['CHECK_DATE']);
    CheckFinder.dtpkEndCheckDate.Date := GetEndQuarter(DM_PAYROLL.PR['CHECK_DATE']);
    if CheckFinder.ShowModal = mrOK then
      CheckToVoid := CheckFinder.CheckNumber;
  finally
    CheckFinder.Free;
  end;
  if CheckToVoid <> 0 then
  begin
    ctx_DataAccess.StartNestedTransaction([dbtClient]);
    try
      Q := TevQuery.Create('select EE_NBR from PR_CHECK where {AsOfNow<PR_CHECK>} and PR_CHECK_NBR='+IntToStr(CheckToVoid));
      eeNbr := Q.Result['EE_NBR'];
      if ctx_DataAccess.CUSTOM_VIEW.Active then
        ctx_DataAccess.CUSTOM_VIEW.Close;
      Q := TevQuery.Create('GenericSelect2CurrentNbr');
      Q.Macro['Columns'] := 'CHECK_DATE';
      Q.Macro['Table1'] := 'PR';
      Q.Macro['Table2'] := 'PR_CHECK';
      Q.Macro['NbrField'] := 'PR_CHECK';
      Q.Param['RecordNbr'] := CheckToVoid;
      Q.Macro['JoinField'] := 'PR';

      if GetYear(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value) <> GetYear(Q.Result['CHECK_DATE']) then
        raise ECanNotPerformOperation.CreateHelp('Voided check date year is not the same as your payroll check date year.', IDH_CanNotPerformOperation);

      QtrOfVoided := GetQuarterNumber(Q.Result['CHECK_DATE']);
      QtrOfVoiding := GetQuarterNumber(DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value);

      if (QtrOfVoided < QtrOfVoiding) and (ctx_AccountRights.Functions.GetState('USER_CAN_VOID_PREV_QTR_CHECK') <> stEnabled) then
      begin
        EvMessage('This check is from a previous quarter and you do not have privileges to void checks from previous quarters', mtWarning, [mbOk]);
        DM_PAYROLL.PR_CHECK.Cancel;
        AbortEx;
      end;

      if QtrOfVoiding <> QtrOfVoided then
        if EvMessage('Voided check date quarter is not the same as your payroll check date quarter.' + #10 + #13
          + 'Would you like to continue?', mtWarning, [mbYes, mbNo]) <> mrYes then
        begin
          DM_PAYROLL.PR_CHECK.Cancel;
          AbortEx;
        end;

      DM_PAYROLL.PR_CHECK.Append;
      DM_PAYROLL.PR_CHECK['EE_NBR'] := eeNbr;
      DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'] := batchNbr;
      DM_PAYROLL.PR_CHECK['CHECK_TYPE'] := CHECK_TYPE2_VOID;
      DM_PAYROLL.PR_CHECK['DATA_NBR'] := Null;
      DM_PAYROLL.PR_CHECK['NOTES_NBR'] := Null;
      DM_PAYROLL.PR_CHECK['FILLER'] := PutIntoFiller(ConvertNull(DM_PAYROLL.PR_CHECK.FieldByName('FILLER').Value, ''), IntToStr(CheckToVoid), 1, 8);
      DM_PAYROLL.PR_CHECK.Post;

      try
        DM_CLIENT.PR_CHECK.DisableControls;
        DM_CLIENT.PR_CHECK_LINES.DisableControls;
        DM_CLIENT.PR_CHECK.LookupsEnabled := False;
        DM_CLIENT.PR_CHECK_LINES.LookupsEnabled := False;
        with DM_CLIENT, DM_SYSTEM_FEDERAL, DM_SYSTEM_STATE do
          ctx_PayrollCalculation.Generic_CreatePRCheckDetails(PR, PR_BATCH, PR_CHECK,
            PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS,
            PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES, CO_STATES,
            EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES, False, False, False);
        if DM_CLIENT.PR_CHECK.State <> dsBrowse then
          DM_CLIENT.PR_CHECK.Post;
        with DM_PAYROLL do
          ctx_DataAccess.PostDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS]);
        (Owner as TFramePackageTmpl).ClickButton(NavCommit);
        ctx_DataAccess.CommitNestedTransaction;
      finally
        DM_CLIENT.PR_CHECK.EnableControls;
        DM_CLIENT.PR_CHECK_LINES.EnableControls;
        DM_CLIENT.PR_CHECK.LookupsEnabled := True;
        DM_CLIENT.PR_CHECK_LINES.LookupsEnabled := True;
      end;

    except
      with DM_PAYROLL do
        ctx_DataAccess.CancelDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS]);
      DM_PAYROLL.PR_CHECK_LINES.Close;
      DM_PAYROLL.PR_CHECK_LINES.Open;
      ctx_DataAccess.RollbackNestedTransaction;
      raise;
    end;
    CheckChanged;
  end;
end;

procedure TREDIT_CHECK.aCreateManualCheckExecute(Sender: TObject);
var
  eeNbr, batchNbr: Integer;
begin
  inherited;
  RecalcCheck;
  with TCreateManualCheck.Create(nil) do
  try
    if ShowModal = mrOK then
    begin
      eeNbr := DM_PAYROLL.PR_CHECK['EE_NBR'];
      batchNbr := DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'];
      DM_PAYROLL.PR_CHECK.Append;
      DM_PAYROLL.PR_CHECK['EE_NBR'] := eeNbr;

      if EvMessage('Do you want the balance of the scheduled EDs for the manual check to be updated?', mtConfirmation, [mbYes,mbNo]) = mrYes then
        DM_PAYROLL.PR_CHECK.UPDATE_BALANCE.AsString := GROUP_BOX_YES
      else
        DM_PAYROLL.PR_CHECK.UPDATE_BALANCE.AsString := GROUP_BOX_NO;

      DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'] := batchNbr;
      if gType.ItemIndex = 0 then
        DM_PAYROLL.PR_CHECK['CHECK_TYPE'] := CHECK_TYPE2_MANUAL
      else
        DM_PAYROLL.PR_CHECK['CHECK_TYPE'] := CHECK_TYPE2_3RD_PARTY;
      DM_PAYROLL.PR_CHECK['PAYMENT_SERIAL_NUMBER'] := eNumber.Text;
      ctx_PayrollCalculation.AssignCheckDefaults(0, 0);
      DM_PAYROLL.PR_CHECK.Post;
      with DM_CLIENT, DM_SYSTEM_FEDERAL, DM_SYSTEM_STATE do
      try
        PR_CHECK.DisableControls;
        PR_CHECK_LINES.DisableControls;
        PR_CHECK.LookupsEnabled := False;
        PR_CHECK_LINES.LookupsEnabled := False;
        ctx_PayrollCalculation.Generic_CreatePRCheckDetails(PR, PR_BATCH, PR_CHECK,
          PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS,
          PR_SCHEDULED_E_DS, CL_E_DS, CL_PERSON, CL_PENSION, CO, CO_E_D_CODES, CO_STATES,
          EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE, SY_STATES, False, False, False);
        if PR_CHECK.State <> dsBrowse then
          PR_CHECK.Post;
      finally
        PR_CHECK.EnableControls;
        PR_CHECK_LINES.EnableControls;
        PR_CHECK.LookupsEnabled := True;
        PR_CHECK_LINES.LookupsEnabled := True;
      end;
    end;
  finally
    Free;
  end;
  try
    (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  except
    with DM_PAYROLL do
      ctx_DataAccess.CancelDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS]);
    DM_PAYROLL.PR_CHECK_LINES.Close;
    DM_PAYROLL.PR_CHECK_LINES.Open;
    raise;
  end;
  CheckChanged;
end;

procedure TREDIT_CHECK.aCheckTemplateExecute(Sender: TObject);
begin
  inherited;
  with TEDIT_SELECT_TEMPLATES_TO_APPLY.Create(Self) do
  try
    if ShowModal = mrOK then
    try
      DM_CLIENT.PR_CHECK.Edit;
      ctx_DataAccess.TemplateNumber := DM_CLIENT.CO_PR_CHECK_TEMPLATES.FieldByName('CO_PR_CHECK_TEMPLATES_NBR').AsInteger;
      ctx_PayrollCalculation.CopyPRTemplate(ctx_DataAccess.TemplateNumber);
      ctx_PayrollCalculation.CopyPRTemplateDetails(ctx_DataAccess.TemplateNumber, DM_CLIENT.PR_CHECK, DM_CLIENT.PR_CHECK_STATES,
        DM_CLIENT.PR_CHECK_LOCALS, DM_CLIENT.CO_BATCH_STATES_OR_TEMPS, DM_CLIENT.CO_BATCH_LOCAL_OR_TEMPS, DM_CLIENT.EE_STATES, DM_CLIENT.EE_LOCALS);
      ctx_DataAccess.PostDataSets([DM_CLIENT.PR_CHECK, DM_CLIENT.PR_CHECK_LINES, DM_CLIENT.PR_CHECK_STATES, DM_CLIENT.PR_CHECK_LOCALS]);
    finally
      DM_CLIENT.PR_CHECK_LINES.DataRequired('PR_NBR='+DM_CLIENT.PR.PR_NBR.AsString);
    end;
  finally
    Free;
  end;
  AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_CHECK.aDuplicateExecute(Sender: TObject);
var
  v: array of Variant;
  i: Integer;
  CopyLocal: Boolean;
  TmpDS: TEvBasicClientDataSet;
begin
  inherited;
  TmpDS := TEvBasicClientDataSet.Create(Nil);
  if PageControl1.ActivePage = TabSheet1 then
  try
    CopyLocal := False;
    ctx_StartWait;
    DM_PAYROLL.PR_CHECK_LINES.DisableControls;
    DM_PAYROLL.PR_CHECK_LINE_LOCALS.DisableControls;
    SetLength(v, DM_PAYROLL.PR_CHECK_LINES.FieldCount);

    if DM_PAYROLL.PR_CHECK_LINE_LOCALS.RecordCount <> 0 then
    begin
      CopyLocal := True;
      TmpDS.LoadFromDataSet(DM_PAYROLL.PR_CHECK_LINE_LOCALS, [mtcpoStructure]);
    end;

    for i := 0 to Pred(DM_PAYROLL.PR_CHECK_LINES.FieldCount) do
      v[i] := DM_PAYROLL.PR_CHECK_LINES.Fields[i].Value;

    DM_PAYROLL.PR_CHECK_LINES.Append;
    for i := 0 to Pred(DM_PAYROLL.PR_CHECK_LINES.FieldCount) do
      if DM_PAYROLL.PR_CHECK_LINES.Fields[i].FieldName <> 'PR_CHECK_LINES_NBR' then
        DM_PAYROLL.PR_CHECK_LINES.Fields[i].Value := v[i];
    DM_PAYROLL.PR_CHECK_LINES.FieldByName('LINE_TYPE').Value := CHECK_LINE_TYPE_USER;
    DM_PAYROLL.PR_CHECK_LINES.Post;

    if CopyLocal then
    begin
      TmpDS.First;
      while not TmpDS.Eof do
      begin
        DM_PAYROLL.PR_CHECK_LINE_LOCALS.Append;
        for i := 0 to Pred(DM_PAYROLL.PR_CHECK_LINE_LOCALS.FieldCount) do
          if ((DM_PAYROLL.PR_CHECK_LINE_LOCALS.Fields[i].FieldName <> 'PR_CHECK_LINES_NBR') and (DM_PAYROLL.PR_CHECK_LINE_LOCALS.Fields[i].FieldName <> 'PR_CHECK_LINE_LOCALS_NBR')) then
            DM_PAYROLL.PR_CHECK_LINE_LOCALS.Fields[i].Value := TmpDS.Fields[i].Value;
        DM_PAYROLL.PR_CHECK_LINE_LOCALS.PR_CHECK_LINES_NBR.AsInteger := DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_LINES_NBR.AsInteger;;
        DM_PAYROLL.PR_CHECK_LINE_LOCALS.Post;
        TmpDS.Next;
      end;
    end;

  finally
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
    DM_PAYROLL.PR_CHECK_LINE_LOCALS.EnableControls;
    TmpDS.Free;
    ctx_EndWait;
  end;
end;

procedure TREDIT_CHECK.FilterEeStates;
begin
  if EEStatesFilter > 0 then
  begin
    DM_EMPLOYEE.EE_STATES.Filter := 'EE_STATES_NBR = ' + IntToStr(EEStatesFilter);
    DM_EMPLOYEE.EE_STATES.Filtered := True
  end
  else if EEStatesFilter < 0 then
  begin
    DM_EMPLOYEE.EE_STATES.Filter := 'EE_STATES_NBR <> ' + IntToStr(-EEStatesFilter);
    DM_EMPLOYEE.EE_STATES.Filtered := True
  end
  else
  begin
    DM_EMPLOYEE.EE_STATES.Filter := '';
    DM_EMPLOYEE.EE_STATES.Filtered := False
  end;

  DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR='+DM_PAYROLL.PR_CHECK.EE_NBR.AsString);
end;

procedure TREDIT_CHECK.lkupStatesEnter(Sender: TObject);
begin
  inherited;
  FilterEeStates;
end;

procedure TREDIT_CHECK.lkupStatesExit(Sender: TObject);
begin
  inherited;
  DM_EMPLOYEE.EE_STATES.Filter := '';
  DM_EMPLOYEE.EE_STATES.Filtered := False
end;

procedure TREDIT_CHECK.lkupSUIEnter(Sender: TObject);
begin
  inherited;
  if EESUIFilter > 0 then
  begin
    DM_EMPLOYEE.EE_STATES.Filter := 'EE_STATES_NBR = ' + IntToStr(EESUIFilter);
    DM_EMPLOYEE.EE_STATES.Filtered := True
  end
  else if EESUIFilter < 0 then
  begin
    DM_EMPLOYEE.EE_STATES.Filter := 'EE_STATES_NBR <> ' + IntToStr(-EESUIFilter);
    DM_EMPLOYEE.EE_STATES.Filtered := True
  end
  else
  begin
    DM_EMPLOYEE.EE_STATES.Filter := '';
    DM_EMPLOYEE.EE_STATES.Filtered := False
  end
end;

procedure TREDIT_CHECK.lkupSUIExit(Sender: TObject);
begin
  inherited;
  DM_EMPLOYEE.EE_STATES.Filter := '';
  DM_EMPLOYEE.EE_STATES.Filtered := False
end;

procedure TREDIT_CHECK.bEEClick(Sender: TObject);
begin
  inherited;
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Employee - Employee' + #0)), 0);
end;

procedure TREDIT_CHECK.bEERatesClick(Sender: TObject);
begin
  inherited;
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Employee - Pay rate info' + #0)), 0);
end;

procedure TREDIT_CHECK.bScheduledEDsClick(Sender: TObject);
begin
  inherited;
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Employee - Scheduled E/Ds' + #0)), 0);
end;

procedure TREDIT_CHECK.RedistributeDBDT(Sender: TObject);
var
  Hours, Amount: Currency;
  v, vlt: Variant;
  t: TStringList;
  FieldList: string;
  DivisionNbr, BranchNbr, DepartmentNbr, TeamNbr, WorkersCompNbr, JobsNbr, LineItemDate: Variant;
  bp: TDataSetNotifyEvent;
begin
  with TdlgRedistrDBDT.Create(Self) do
  try
    evLabel3.Caption := FormatFloat('#,##0.00', DM_PAYROLL.PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').AsFloat);
    evLabel4.Caption := FormatFloat('#,##0.00', DM_PAYROLL.PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat);
    evLabel5.Caption := DM_PAYROLL.PR_CHECK_LINES.FieldByName('E_D_Description_Lookup').AsString;
    evLabel7.Caption := DM_PAYROLL.PR_CHECK_LINES.FieldByName('E_D_Code_Lookup').AsString;

    evClientDataSet1.CreateDataSet;
    evClientDataSet1.AppendRecord(['', '', '',
                                   DM_PAYROLL.PR_CHECK_LINES['CO_WORKERS_COMP_NBR'], DM_PAYROLL.PR_CHECK_LINES['CO_JOBS_NBR'],
                                   DM_PAYROLL.PR_CHECK_LINES['CO_DIVISION_NBR'], DM_PAYROLL.PR_CHECK_LINES['CO_BRANCH_NBR'],
                                   DM_PAYROLL.PR_CHECK_LINES['CO_DEPARTMENT_NBR'], DM_PAYROLL.PR_CHECK_LINES['CO_TEAM_NBR'],
                                   DM_PAYROLL.PR_CHECK_LINES['LINE_ITEM_DATE'],
                                   DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES'], DM_PAYROLL.PR_CHECK_LINES['AMOUNT']]);
    if ShowModal = mrOK then
    begin
      ctx_DataAccess.UnlockPR;
      t := TStringList.Create;
      DM_PAYROLL.PR_CHECK_LINES.DisableControls;
      bp := DM_PAYROLL.PR_CHECK_LINES.BeforePost;
      DM_PAYROLL.PR_CHECK_LINES.BeforePost := nil;
      try
        try
          DM_PAYROLL.PR_CHECK_LINES.GetFieldNames(t);
          t.Delete(t.IndexOf('PR_CHECK_LINES_NBR'));
          FieldList := StringReplace(t.CommaText, ',', ';', [rfReplaceAll]);
          v := DM_PAYROLL.PR_CHECK_LINES[FieldList];
          vlt := DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'];
          Hours := 0;
          Amount := 0;
          DivisionNbr := DM_PAYROLL.PR_CHECK_LINES['CO_DIVISION_NBR'];
          BranchNbr := DM_PAYROLL.PR_CHECK_LINES['CO_BRANCH_NBR'];
          DepartmentNbr := DM_PAYROLL.PR_CHECK_LINES['CO_DEPARTMENT_NBR'];
          TeamNbr := DM_PAYROLL.PR_CHECK_LINES['CO_TEAM_NBR'];
          WorkersCompNbr := DM_PAYROLL.PR_CHECK_LINES['CO_WORKERS_COMP_NBR'];
          JobsNbr := DM_PAYROLL.PR_CHECK_LINES['CO_JOBS_NBR'];
          LineItemDate := DM_PAYROLL.PR_CHECK_LINES['LINE_ITEM_DATE'];
          evClientDataSet1.First;
          while not evClientDataSet1.Eof do
          begin
            if (DivisionNbr = evClientDataSet1['CO_DIVISION_NBR']) and
               (BranchNbr = evClientDataSet1['CO_BRANCH_NBR']) and
               (DepartmentNbr = evClientDataSet1['CO_DEPARTMENT_NBR']) and
               (TeamNbr = evClientDataSet1['CO_TEAM_NBR']) and
               (WorkersCompNbr = evClientDataSet1['CO_WORKERS_COMP_NBR']) and
               (JobsNbr = evClientDataSet1['CO_JOBS_NBR']) and
               (LineItemDate = evClientDataSet1['LINE_ITEM_DATE'])
                then
            begin
(*              if (LineItemDate <> evClientDataSet1['LINE_ITEM_DATE']) then
              begin
                DM_PAYROLL.PR_CHECK_LINES.Edit;
                DM_PAYROLL.PR_CHECK_LINES['LINE_ITEM_DATE'] := evClientDataSet1['LINE_ITEM_DATE'];
                DM_PAYROLL.PR_CHECK_LINES.Post;
              end;
*)              
              evClientDataSet1.Next;
              Continue;
            end
            else
            begin
              Hours := Hours - evClientDataSet1.FieldByName('Hours').AsCurrency;
              Amount := Amount - evClientDataSet1.FieldByName('Amount').AsCurrency;
            end;
            DM_PAYROLL.PR_CHECK_LINES.Insert;
            DM_PAYROLL.PR_CHECK_LINES[FieldList] := v;
            DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := vlt;
            DM_PAYROLL.PR_CHECK_LINES['CO_DIVISION_NBR'] := evClientDataSet1['CO_DIVISION_NBR'];
            DM_PAYROLL.PR_CHECK_LINES['CO_BRANCH_NBR'] := evClientDataSet1['CO_BRANCH_NBR'];
            DM_PAYROLL.PR_CHECK_LINES['CO_DEPARTMENT_NBR'] := evClientDataSet1['CO_DEPARTMENT_NBR'];
            DM_PAYROLL.PR_CHECK_LINES['CO_TEAM_NBR'] := evClientDataSet1['CO_TEAM_NBR'];
            DM_PAYROLL.PR_CHECK_LINES['CO_WORKERS_COMP_NBR'] := evClientDataSet1['CO_WORKERS_COMP_NBR'];
            DM_PAYROLL.PR_CHECK_LINES['CO_JOBS_NBR'] := evClientDataSet1['CO_JOBS_NBR'];
            DM_PAYROLL.PR_CHECK_LINES['LINE_ITEM_DATE'] := evClientDataSet1['LINE_ITEM_DATE'];
            if evClientDataSet1.FieldByName('Hours').AsCurrency <> 0 then
              DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES'] := evClientDataSet1['Hours']
            else
              DM_PAYROLL.PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Clear;
            if evClientDataSet1.FieldByName('AMOUNT').AsCurrency <> 0 then
              DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := evClientDataSet1['AMOUNT']
            else
              DM_PAYROLL.PR_CHECK_LINES.FieldByName('AMOUNT').Clear;
            DM_PAYROLL.PR_CHECK_LINES.Post;
            evClientDataSet1.Next;
          end;
          if (Hours <> 0) or (Amount <> 0) then
          begin
            DM_PAYROLL.PR_CHECK_LINES.Insert;
            DM_PAYROLL.PR_CHECK_LINES[FieldList] := v;
            DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := vlt;
            if Hours <> 0 then
              DM_PAYROLL.PR_CHECK_LINES['HOURS_OR_PIECES'] := Hours
            else
              DM_PAYROLL.PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Clear;
            if Amount <> 0 then
              DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := Amount
            else
              DM_PAYROLL.PR_CHECK_LINES.FieldByName('AMOUNT').Clear;
            DM_PAYROLL.PR_CHECK_LINES.Post;
          end;
          ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK_LINES]);
        except
          DM_PAYROLL.PR_CHECK_LINES.CancelUpdates;
          raise;
        end;
      finally
        DM_PAYROLL.PR_CHECK_LINES.BeforePost := bp;
        DM_PAYROLL.PR_CHECK_LINES.EnableControls;
        t.Free;
        ctx_DataAccess.LockPR(True);
      end;
    end;
  finally
    Free;
  end;
end;

procedure TREDIT_CHECK.evBitBtn1Click(Sender: TObject);
begin
  if DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAX').IsNull then
    raise EUpdateError.CreateHelp('Recalculate Check prior to using the Plug Taxes button', IDH_ConsistencyViolation);

  if DM_PAYROLL.PR_CHECK.State = dsBrowse then
    DM_PAYROLL.PR_CHECK.Edit;

  DM_PAYROLL.PR_CHECK.DisableControls;
  DM_PAYROLL.PR_CHECK_LOCALS.DisableControls;
  DM_PAYROLL.PR_CHECK_STATES.DisableControls;
  DM_PAYROLL.PR_CHECK_SUI.DisableControls;
  try
    DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
    DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_VALUE'] := DM_PAYROLL.PR_CHECK['FEDERAL_TAX'];
    DM_PAYROLL.PR_CHECK['OR_CHECK_OASDI'] := DM_PAYROLL.PR_CHECK['EE_OASDI_TAX'];
    DM_PAYROLL.PR_CHECK['OR_CHECK_MEDICARE'] := DM_PAYROLL.PR_CHECK['EE_MEDICARE_TAX'];
    DM_PAYROLL.PR_CHECK['OR_CHECK_EIC'] := DM_PAYROLL.PR_CHECK['EE_EIC_TAX'];
    DM_PAYROLL.PR_CHECK.Post;


    DM_PAYROLL.PR_CHECK_STATES.First;
    while not DM_PAYROLL.PR_CHECK_STATES.EOF do
    begin
      DM_PAYROLL.PR_CHECK_STATES.Edit;
      DM_PAYROLL.PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
      DM_PAYROLL.PR_CHECK_STATES['STATE_OVERRIDE_VALUE'] := DM_PAYROLL.PR_CHECK_STATES['STATE_TAX'];
      DM_PAYROLL.PR_CHECK_STATES['EE_SDI_OVERRIDE'] := DM_PAYROLL.PR_CHECK_STATES['EE_SDI_TAX'];
      DM_PAYROLL.PR_CHECK_STATES.Post;
      DM_PAYROLL.PR_CHECK_STATES.Next;
    end;

    DM_PAYROLL.PR_CHECK_SUI.First;
    while not DM_PAYROLL.PR_CHECK_SUI.EOF do
    begin
      DM_PAYROLL.PR_CHECK_SUI.Edit;
      DM_PAYROLL.PR_CHECK_SUI['OVERRIDE_AMOUNT'] := DM_PAYROLL.PR_CHECK_SUI['SUI_TAX'];
      DM_PAYROLL.PR_CHECK_SUI.Post;
      DM_PAYROLL.PR_CHECK_SUI.Next;
    end;

    DM_PAYROLL.PR_CHECK_LOCALS.First;
    while not DM_PAYROLL.PR_CHECK_LOCALS.EOF do
    begin
      DM_PAYROLL.PR_CHECK_LOCALS.Edit;
      DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := DM_PAYROLL.PR_CHECK_LOCALS['LOCAL_TAX'];
      DM_PAYROLL.PR_CHECK_LOCALS.Post;
      DM_PAYROLL.PR_CHECK_LOCALS.Next;
    end;

    PageControl1.ActivatePage(ManualTaxTabSheet);
  finally
    DM_PAYROLL.PR_CHECK.EnableControls;
    DM_PAYROLL.PR_CHECK_LOCALS.EnableControls;
    DM_PAYROLL.PR_CHECK_STATES.EnableControls;
    DM_PAYROLL.PR_CHECK_SUI.EnableControls;
  end;
end;

procedure TREDIT_CHECK.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if (DM_COMPANY.PR_CHECK_LINES.State = dsEdit) and
     ((DM_COMPANY.PR_CHECK_LINES.LINE_TYPE.Value = CHECK_LINE_TYPE_DISTR_USER) or
     (DM_COMPANY.PR_CHECK_LINES.LINE_TYPE.Value = CHECK_LINE_TYPE_DISTR_SCHEDULED)) then
    EvMessage('This line has already been distributed for labor. If you change this line, it will not change other distributed lines. If you would like this line to automatically distribute labor, enter a new check line and add the amount on the new line.')
end;

procedure TREDIT_CHECK.LocateUserLine;
var
  Nbr: Integer;
begin
  Nbr := 999999999;
  if DM_PAYROLL.PR_CHECK_LINES.Locate('LINE_TYPE', CHECK_LINE_TYPE_USER, []) then
    Nbr := DM_PAYROLL.PR_CHECK_LINES.RecNo;
  if DM_PAYROLL.PR_CHECK_LINES.Locate('LINE_TYPE', CHECK_LINE_TYPE_DISTR_USER, []) and
     (DM_PAYROLL.PR_CHECK_LINES.RecNo > Nbr) then
    DM_PAYROLL.PR_CHECK_LINES.RecNo := Nbr;
end;

procedure TREDIT_CHECK.ManualTaxClientDataSetBeforePost(DataSet: TDataSet);
var
  aDataSet: TDataSet;

  function FindDataSet(DataSetName: string): TDataSet;
  var
    I: integer;

  begin
    result := nil;

    for I := 0 to ComponentCount - 1 do
      if Components[I] is twwDataSource then
        with Components[I] as twwDataSource do
          if assigned(DataSet) then
            if DataSet.Name = DataSetName then
            begin
              result := DataSet;
              break;
            end;
  end;

begin
  if InitializingManualTaxClientDataSet then
    exit;

  with ManualTaxClientDataSet do
  begin
    aDataSet := FindDataSet(FieldByName('DataSetName').AsString);

    if not assigned(aDataSet) then
      exit;

    if not aDataSet.Locate('PR_CHECK_NBR;' + FieldByName('IDFieldName').AsString, VarArrayOf([DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'],
      FieldbyName('ID').AsString]), [loCaseInsensitive]) then
      exit;

    if (aDataSet.FieldByName(FieldByName('AmountFieldName').AsString).Value = FieldByName('Amount').Value) then
      exit;

    if not (aDataSet.State in [dsEdit, dsInsert]) then
      aDataSet.Edit;

    aDataSet.FieldByName(FieldByName('AmountFieldName').AsString).Value := FieldByName('Amount').Value;
    if aDataSet = DM_PAYROLL.PR_CHECK_STATES then
      if FieldByName('Amount').IsNull then
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE
      else
        DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;

    if (aDataSet = DM_PAYROLL.PR_CHECK) and (FieldByName('AmountFieldName').AsString = 'OR_CHECK_FEDERAL_VALUE') then
      if FieldByName('Amount').IsNull then
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE
      else
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;

    try
      aDataSet.Post;
    except
      ManualTaxClientDataSet.Cancel;
      aDataSet.Cancel;
      raise;
    end;
  end;
end;

procedure TREDIT_CHECK.RecalcCheck;
begin
  if (DM_PAYROLL.PR_CHECK.CALCULATE_OVERRIDE_TAXES.Value = GROUP_BOX_NO) and
     (DM_PAYROLL.PR_CHECK.CHECK_TYPE.Value = CHECK_TYPE2_REGULAR) and
     (DM_PAYROLL.PR.STATUS.Value = PAYROLL_STATUS_PENDING) then
  begin
    RecalculateCheck;
    if (DM_PAYROLL.PR_CHECK.NET_WAGES.Value < 0) and
       (EvMessage('This check has negative net. Do you want to continue?', mtWarning, mbOKCancel) <> mrOk) then
      AbortEx;
  end;
end;

function TREDIT_CHECK.CanClose: Boolean;
begin
  Result := True;
  with Owner as TFramePackageTmpl do
    if btnNavOk.Enabled or btnNavCancel.Enabled then
    begin
      raise EevException.Create('You must post or cancel changes.');
    end;
end;

procedure TREDIT_CHECK.RecalculateCheck;
var
  b: Boolean;
  S: String;
  s1, s2: string;
  sInd: string;
  Warnings: String;
begin
  if Trim(CHECK_TYPE.Text) = 'Void' then AbortEx;
  try
    (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  except
    with DM_PAYROLL do
      ctx_DataAccess.CancelDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS]);
    DM_PAYROLL.PR_CHECK_LINES.Close;
    DM_PAYROLL.PR_CHECK_LINES.Open;
    raise;
  end;
  EEClone.Filtered := False;
  EEClone.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
  ctx_StartWait;
  b := ctx_DataAccess.RecalcLineBeforePost;
  sInd := DM_PAYROLL.PR_CHECK_LINES.IndexFieldNames;
  ctx_DataAccess.CheckMaxAmountAndHours := False;
  DM_PAYROLL.PR_CHECK_LINE_LOCALS.DisableControls;
  try
    ctx_DataAccess.RecalcLineBeforePost := True;
    if PageControl1.ActivePage = ManualTaxTabSheet then
      UnloadManualTaxData;
    S := DM_PAYROLL.PR_CHECK_LINE_LOCALS.RetrieveCondition;
    DM_PAYROLL.PR_CHECK_LINE_LOCALS.RetrieveCondition := '';
    s1 := DM_EMPLOYEE.EE_STATES.RetrieveCondition;
    s2 := DM_EMPLOYEE.EE_LOCALS.RetrieveCondition;
    SaveDSStates([DM_EMPLOYEE.CO_BRANCH, DM_EMPLOYEE.CO_DEPARTMENT, DM_EMPLOYEE.CO_TEAM,
      DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_LOCALS, DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
      DM_EMPLOYEE.EE_WORK_SHIFTS, DM_EMPLOYEE.EE_PENSION_FUND_SPLITS], True);
    LoadNamedDSStates('GrossToNet', [DM_EMPLOYEE.CO_STATES, DM_EMPLOYEE.CO_SUI, DM_EMPLOYEE.CO_LOCAL_TAX,
      DM_EMPLOYEE.CO_DIVISION_LOCALS, DM_EMPLOYEE.CO_BRANCH_LOCALS, DM_EMPLOYEE.CO_DEPARTMENT_LOCALS,
      DM_EMPLOYEE.CO_TEAM_LOCALS, DM_EMPLOYEE.CO_JOBS_LOCALS, DM_EMPLOYEE.CO_BRANCH, DM_EMPLOYEE.CO_DEPARTMENT,
      DM_EMPLOYEE.CO_TEAM, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_LOCALS, DM_EMPLOYEE.EE_RATES,
      DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_WORK_SHIFTS, DM_EMPLOYEE.EE_PENSION_FUND_SPLITS]);
    try
      ctx_PayrollCalculation.GrossToNet(Warnings, DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.AsInteger, False, True, False, False);
    finally
      SaveNamedDSStates('GrossToNet', [DM_EMPLOYEE.CO_STATES, DM_EMPLOYEE.CO_SUI, DM_EMPLOYEE.CO_LOCAL_TAX,
        DM_EMPLOYEE.CO_DIVISION_LOCALS, DM_EMPLOYEE.CO_BRANCH_LOCALS, DM_EMPLOYEE.CO_DEPARTMENT_LOCALS,
        DM_EMPLOYEE.CO_TEAM_LOCALS, DM_EMPLOYEE.CO_JOBS_LOCALS, DM_EMPLOYEE.CO_BRANCH,
        DM_EMPLOYEE.CO_DEPARTMENT, DM_EMPLOYEE.CO_TEAM, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_LOCALS,
        DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_SCHEDULED_E_DS, DM_EMPLOYEE.EE_WORK_SHIFTS,
        DM_EMPLOYEE.EE_PENSION_FUND_SPLITS], True);
      LoadDSStates([DM_EMPLOYEE.CO_BRANCH, DM_EMPLOYEE.CO_DEPARTMENT, DM_EMPLOYEE.CO_TEAM,
        DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_LOCALS, DM_EMPLOYEE.EE_RATES, DM_EMPLOYEE.EE_SCHEDULED_E_DS,
        DM_EMPLOYEE.EE_WORK_SHIFTS, DM_EMPLOYEE.EE_PENSION_FUND_SPLITS]);
    end;
    DM_PAYROLL.PR_CHECK_LINE_LOCALS.RetrieveCondition := S;
    DM_PAYROLL.PR_CHECK_LINES.Filter := '';
    CheckSupportDatasets(DM_COMPANY.CO.ClientId, DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger);
    if PageControl1.ActivePage = ManualTaxTabSheet then
      LoadManualTaxData;
  finally
    ctx_DataAccess.CheckMaxAmountAndHours := True;
    ctx_DataAccess.RecalcLineBeforePost := b;
//    RefreshPreview;
    ctx_EndWait;
    PR_CHECKClone.CloneCursor(DM_PAYROLL.PR_CHECK, False);
    CreateEEFilter;
    EEClone.Filtered := True;
    EEClone.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
    //OpenEEDataSetForCompany(DM_EMPLOYEE.EE_STATES, DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger);
    //OpenEEDataSetForCompany(DM_EMPLOYEE.EE_LOCALS, DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger);
    DM_EMPLOYEE.EE_STATES.RetrieveCondition := s1;
    DM_EMPLOYEE.EE_LOCALS.RetrieveCondition := s2;
    DM_PAYROLL.PR_CHECK_LINES.IndexFieldNames := sInd;
    DM_PAYROLL.PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'], []);  // to refresh lookups
    CHECK_TYPEChange(nil);
    while DM_PAYROLL.PR_CHECK_LINES.ControlsDisabled do
      DM_PAYROLL.PR_CHECK_LINES.EnableControls;
    while DM_PAYROLL.PR_CHECK.ControlsDisabled do
      DM_PAYROLL.PR_CHECK.EnableControls;
    while DM_PAYROLL.PR_CHECK_LINE_LOCALS.ControlsDisabled do
      DM_PAYROLL.PR_CHECK_LINE_LOCALS.EnableControls;
  end;
  AddCheckNbrToLists(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_CHECK.cbCLOverrideChange(Sender: TObject);
begin
  inherited;
  if TWinControl(Sender).Focused then
  begin
    if DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', lcCLLocalName.LookupValue, []) and
       (DM_EMPLOYEE.EE_LOCALS.EXEMPT_EXCLUDE.Value = GROUP_BOX_EXEMPT) and
       (cbCLOverride.Value = GROUP_BOX_INCLUDE) then
      EvMessage('This employee is exempt from this local. Make sure that you REALLY want to include it in this check?');
  end;
end;

procedure TREDIT_CHECK.butnProrateFICAClick(Sender: TObject);
var
  ProrateRatio: Double;
begin
  inherited;
  if EvMessage('Are you sure you want to prorate FICA for this check?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  ctx_StartWait;
  try
    DM_SYSTEM.SY_FED_TAX_TABLE.AsOfDate := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
    DM_SYSTEM.SY_FED_TAX_TABLE.Open;
    ProrateRatio := DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value / (DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value + DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value);
    if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').IsNull and not DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').IsNull then
    begin
      if not (DM_PAYROLL.PR_CHECK.State in [dsInsert, dsEdit]) then
        DM_PAYROLL.PR_CHECK.Edit;
      DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value := DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value * ProrateRatio;
      DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value := DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value - DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value;
      DM_PAYROLL.PR_CHECK.Post;
    end;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK]);
    UnloadManualTaxData;
    LoadManualTaxData;
  finally
    ctx_EndWait;
  end;
end;

procedure TREDIT_CHECK.aVoidNextCheckUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Enabled := ctx_AccountRights.Functions.GetState('USER_CAN_VOID_CHECK') = stEnabled;
end;

procedure TREDIT_CHECK.ED_CL_E_DS_LookupComboEnter(Sender: TObject);
begin
  inherited;
  if wwdsMaster.DataSet.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_3RD_PARTY then
  begin
    DM_CLIENT.CO_E_D_CODES.Filter := 'E_D_CODE_TYPE in ('+ List3rdPartyTypes +')';
    DM_CLIENT.CO_E_D_CODES.Filtered := True;
  end
  else if wwdsMaster.DataSet.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_COBRA_CREDIT then
  begin
    DM_CLIENT.CO_E_D_CODES.Filter := 'E_D_CODE_TYPE=''' + ED_MEMO_COBRA_CREDIT + ''' or E_D_CODE_TYPE=''' + ED_MEMO_SIMPLE + ''' and CodeDescription like ''%Cobra%''';
    DM_CLIENT.CO_E_D_CODES.Filtered := True;
  end
  else //if wwdsMaster.DataSet.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_REGULAR then
  begin
    DM_CLIENT.CO_E_D_CODES.Filter := 'E_D_CODE_TYPE<>''' + ED_MEMO_COBRA_CREDIT + '''';
    DM_CLIENT.CO_E_D_CODES.Filtered := True;
  end;
end;

procedure TREDIT_CHECK.ED_CL_E_DS_LookupComboExit(Sender: TObject);
begin
  inherited;
  DM_CLIENT.CO_E_D_CODES.Filtered := False;
  DM_CLIENT.CO_E_D_CODES.Filter := '';
end;

procedure TREDIT_CHECK.cbCLOverrideDropDown(Sender: TObject);
begin
  inherited;
  if DS_PR_CHECK_LINE_LOCALS.Dataset.FieldByName('EXEMPT_EXCLUDE').AsString = GROUP_BOX_PABLOCK then
    cbCLOverride.Items.Text := GroupBox_LL2ExemptExclude_ComboChoices
  else
    cbCLOverride.Items.Text := GroupBox_LL1ExemptExclude_ComboChoices;
end;

procedure TREDIT_CHECK.SaveNamedDSStates(const Name: string; DS: array of TevClientDataSet; const SaveData: Boolean = False);
var
  i: Integer;
begin
  for i := Low(DS) to High(DS) do
    DS[i].SaveNamedState(Name, SaveData);
end;

procedure TREDIT_CHECK.LoadNamedDSStates(const Name: string; DS: array of TevClientDataSet);
var
  i: Integer;
begin
  for i := Low(DS) to High(DS) do
    DS[i].LoadNamedState(Name);
end;

procedure TREDIT_CHECK.InitNamedDSStates(const Name: string; DS: array of TevClientDataSet);
var
  i: Integer;
begin
  for i := Low(DS) to High(DS) do
    DS[i].InitNamedState(Name);
end;

procedure TREDIT_CHECK.sbCheckLinesResize(Sender: TObject);
begin
  inherited;
  fplListofCheckLines.Width := sbCheckLines.Width - 16;
  if (sbCheckLines.Height > 621) then
    fplListofCheckLines.Height := sbCheckLines.Height - 321
  else
    fplListofCheckLines.Height := 300;
  fpCheckLineDetail.Top := fplListofCheckLines.top + fplListofCheckLines.Height + 8;

end;

procedure TREDIT_CHECK.pnlNotesResize(Sender: TObject);
begin
  inherited;
  pnlNotesBody.Height := pnlNotes.Height - 65;
  pnlNotesBody.Width  := pnlNotes.Width - 40;
end;

procedure TREDIT_CHECK.pnlPreviewResize(Sender: TObject);
begin
  inherited;
  pnlPreviewBody.Width  := pnlPreview.Width - 40;
  pnlPreviewBody.Height := pnlPreview.Height - 65;
end;

procedure TREDIT_CHECK.wwDBGrid1AfterDrawCell(Sender: TwwCustomDBGrid;
  DrawCellInfo: TwwCustomDrawGridCellInfo);
var
  aBitmap   : TBitmap;
  cellRect,
  paintRect : TRect;
  bmpWidth  : Integer;
  s         : String;
begin
  aBitmap := TBitmap.Create;
  try  
    cellRect := DrawCellInfo.Rect;
    if DrawCellInfo.DataRow < 0 then exit;
    if DrawCellInfo.DataCol <> 0 then exit;

      if assigned(wwDBGrid1.DataSource.DataSet) and (wwDBGrid1.DataSource.DataSet.RecordCount > 0) then
        begin
          s := wwDBGrid1.DataSource.DataSet.FieldByName('LineTypeSort').asString;
          if s = 'S' then
          begin
              ilGridPictures.GetBitmap(0,aBitmap);
              aBitmap.Transparent := True;
              bmpWidth := (cellRect.Bottom - cellRect.top);
              paintRect.Left := cellRect.Right - bmpWidth -1;
              paintRect.Right := cellRect.Right -1;
              paintRect.Top := cellRect.Top + 1;
              paintRect.Bottom := cellRect.Bottom - 1;
              wwDBGrid1.Canvas.StretchDraw(paintRect,aBitmap);
          end;
        end;

  finally
    aBitmap.Free;
  end;

end;

procedure TREDIT_CHECK.PageControl1Resize(Sender: TObject);
begin
  inherited;
  //GUI 2.0   //turned off to fix item 6 on Res # 97204
 // fpCheckLineDetail.Top      := PageControl1.Height - 337;
 // fplListofCheckLines.Height := PageControl1.Height - 354;
 // wwDBGrid1.Height           := fplListofCheckLines.Height - 65;
end;



procedure TREDIT_CHECK.wwDBGrid1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  s: String;

begin
  inherited;
  //needed to fix CTRL-E when the cells the lock icon are in focus.
  //redirect focus to next field
  s := wwDBGrid1.DataSource.DataSet.FieldByName('LineTypeSort').asString;
  if s <> 'S' then exit;
  if wwDBGrid1.SelectedIndex = 0 then
     wwDBGrid1.SelectedIndex := 1;
end;

procedure TREDIT_CHECK.wwDBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  s: String;

begin
  inherited;
  //needed to fix CTRL-E when the cells the lock icon are in focus.
  //redirect focus to next field
  s := wwDBGrid1.DataSource.DataSet.FieldByName('LineTypeSort').asString;
  if s <> 'S' then exit;
  if wwDBGrid1.SelectedIndex = 0 then
     wwDBGrid1.SelectedIndex := 1;

end;

procedure TREDIT_CHECK.PaLocalWarning;
var
  CoLocalTaxNbr: Integer;
  SyLocalsNbr: Integer;
  State: String;
  IsAct32: Boolean;
  Q: IevQuery;
begin
  inherited;
  Q := TevQuery.Create('select CO_LOCAL_TAX_NBR from EE_LOCALS where {AsOfNow<EE_LOCALS>} and EE_LOCALS_NBR='+DM_PAYROLL.PR_CHECK_LOCALS.EE_LOCALS_NBR.AsString);
  CoLocalTaxNbr := Q.Result.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
  SyLocalsNbr := DM_CLIENT.CO_LOCAL_TAX.Lookup('CO_LOCAL_TAX_NBR', CoLocalTaxNbr, 'SY_LOCALS_NBR');
  DM_SYSTEM.SY_LOCALS.Locate('SY_LOCALS_NBR', SyLocalsNbr, []);
  State := DM_SYSTEM.SY_STATES.Lookup('SY_STATES_NBR', DM_SYSTEM.SY_LOCALS.SY_STATES_NBR.AsInteger, 'STATE');

  IsAct32 := (State = 'PA') and ((DM_SYSTEM.SY_LOCALS.LOCAL_TYPE.AsString = 'R') or (DM_SYSTEM.SY_LOCALS.LOCAL_TYPE.AsString = 'N')
    and (DM_SYSTEM.SY_LOCALS.COMBINE_FOR_TAX_PAYMENTS.AsString = 'Y'));

  if IsAct32 then
  begin
    MessageBox(Handle, 'PA Local Tax Amounts entered on the Manual Tax tab will not have the correct Location and Non Res tax assigned to them.'+
      ' These override amounts should be entered on the Aux. Payroll - Locals screen, or contact your Service Bureau to have them entered correctly.',
      'PA Local Tax Override', MB_ICONWARNING);
  end;
end;

procedure TREDIT_CHECK.LocalOverrideAmountEditChange(Sender: TObject);
begin
  if not DM_PAYROLL.PR_CHECK_LOCALS.EE_LOCALS_NBR.IsNull
  and (DM_CLIENT.PR_CHECK.CHECK_TYPE.AsString <> CHECK_TYPE2_VOID)
  and Assigned(FEeLocalList)
  and not FEeLocalList.ValueExists(DM_PAYROLL.PR_CHECK_LOCALS.EE_LOCALS_NBR.AsString) then
  begin
    FEeLocalList.AddValue(DM_PAYROLL.PR_CHECK_LOCALS.EE_LOCALS_NBR.AsString, 0);
    PaLocalWarning;
  end;
end;

initialization
  Classes.RegisterClass(TREDIT_CHECK);

end.
