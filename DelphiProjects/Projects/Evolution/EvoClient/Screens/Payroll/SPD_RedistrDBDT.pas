// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_RedistrDBDT;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, EvConsts,
  Buttons, ExtCtrls, DB,   Wwdatsrc, Grids, SPD_DBDTSelectionListFiltr,
  Wwdbigrd, Wwdbgrid, EvUtils, Mask, wwdbedit, Wwdotdot, SDataStructure,
  wwdblook, SDDClasses, kbmMemTable, ISKbmMemDataSet,
  ISBasicClasses, SDataDictclient, ISDataAccessComponents,
  EvDataAccessComponents, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo;

type
  TdlgRedistrDBDT = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    evDBGrid1: TevDBGrid;
    evDataSource1: TevDataSource;
    evClientDataSet1: TevClientDataSet;
    evClientDataSet1DBDT: TStringField;
    evClientDataSet1Hours: TCurrencyField;
    evClientDataSet1Amount: TCurrencyField;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    evDBComboDlg1: TevDBComboDlg;
    evClientDataSet1CO_DIVISION_NBR: TIntegerField;
    evClientDataSet1CO_BRANCH_NBR: TIntegerField;
    evClientDataSet1CO_DEPARTMENT_NBR: TIntegerField;
    evClientDataSet1CO_TEAM_NBR: TIntegerField;
    evClientDataSet1CO_WORKERS_COMP_NBR: TIntegerField;
    evClientDataSet1CO_JOBS_NBR: TIntegerField;
    lkupWorkComp: TevDBLookupCombo;
    lkupJobs: TevDBLookupCombo;
    evClientDataSet1WorkersComp: TStringField;
    evClientDataSet1Job: TStringField;
    DM_COMPANY: TDM_COMPANY;
    evClientDataSet1Line_Item_Date: TDateField;
    procedure FormShow(Sender: TObject);
    procedure evDBComboDlg1CustomDlg(Sender: TObject);
    procedure evClientDataSet1CalcFields(DataSet: TDataSet);
    procedure OKBtnClick(Sender: TObject);
    procedure evClientDataSet1NewRecord(DataSet: TDataSet);
    procedure evDataSource1DataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public

  end;

implementation

uses DateUtils;

{$R *.dfm}

procedure TdlgRedistrDBDT.FormShow(Sender: TObject);
begin
  evClientDataSet1.Edit;
  evClientDataSet1['Amount'] := evClientDataSet1['Amount'];
  evClientDataSet1.Post;
end;

procedure TdlgRedistrDBDT.evDBComboDlg1CustomDlg(Sender: TObject);
var
  DBDTSelectionList: TDBDTSelectionListFiltr;
begin
  DBDTSelectionList := TDBDTSelectionListFiltr.Create(nil);
  try
    with DM_COMPANY, DBDTSelectionList do
    begin
      CO_DIVISION.DataRequired('CO_NBR=' + CO.FieldByName('CO_NBR').AsString);
      DBDTSelectionList.Setup(TevClientDataSet(CO_DIVISION), TevClientDataSet(CO_BRANCH),
        TevClientDataSet(CO_DEPARTMENT), TevClientDataSet(CO_TEAM),
        evClientDataSet1.FieldByName('CO_DIVISION_NBR').Value
        , evClientDataSet1.FieldByName('CO_BRANCH_NBR').Value, evClientDataSet1.FieldByName('CO_DEPARTMENT_NBR').Value
        , evClientDataSet1.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionList.ShowModal = mrOK then
      begin
        if (evClientDataSet1.FieldByName('CO_DIVISION_NBR').Value <> wwcsTempDBDT.FieldByName('DIVISION_NBR').Value)
          or (evClientDataSet1.FieldByName('CO_BRANCH_NBR').Value <> wwcsTempDBDT.FieldByName('BRANCH_NBR').Value)
          or (evClientDataSet1.FieldByName('CO_DEPARTMENT_NBR').Value <> wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value)
          or (evClientDataSet1.FieldByName('CO_TEAM_NBR').Value <> wwcsTempDBDT.FieldByName('TEAM_NBR').Value) then
        begin
          if not (evClientDataSet1.State in [dsInsert, dsEdit]) then
          begin
            if evClientDataSet1.RecordCount = 0 then
              evClientDataSet1.Insert
            else
              evClientDataSet1.Edit;
          end;
          evClientDataSet1.FieldByName('CO_DIVISION_NBR').Value := wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
          evClientDataSet1.FieldByName('CO_BRANCH_NBR').Value := wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
          evClientDataSet1.FieldByName('CO_DEPARTMENT_NBR').Value := wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
          evClientDataSet1.FieldByName('CO_TEAM_NBR').Value := wwcsTempDBDT.FieldByName('TEAM_NBR').Value;
        end;
      end;
    end;
  finally
    DBDTSelectionList.Free;
  end;
end;

procedure TdlgRedistrDBDT.evClientDataSet1CalcFields(DataSet: TDataSet);
var
  s: string;
begin
  s := '';
  if not evClientDataSet1.FieldByName('CO_DIVISION_NBR').IsNull and
     ((evClientDataSet1['CO_DIVISION_NBR'] = DM_COMPANY.CO_DIVISION['CO_DIVISION_NBR']) or
       DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', evClientDataSet1['CO_DIVISION_NBR'], [])) and
     (DM_COMPANY.CO_DIVISION.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
    s := s + Trim(DM_COMPANY.CO_DIVISION.FieldByName('CUSTOM_DIVISION_NUMBER').AsString);
  if not evClientDataSet1.FieldByName('CO_BRANCH_NBR').IsNull and
     ((evClientDataSet1['CO_BRANCH_NBR'] = DM_COMPANY.CO_BRANCH['CO_BRANCH_NBR']) or
       DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', evClientDataSet1['CO_BRANCH_NBR'], [])) and
     (DM_COMPANY.CO_BRANCH.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
  begin
    if s <> '' then
      s := s + ' | ';
    s := s + Trim(DM_COMPANY.CO_BRANCH.FieldByName('CUSTOM_BRANCH_NUMBER').AsString);
  end;
  if not evClientDataSet1.FieldByName('CO_DEPARTMENT_NBR').IsNull and
     ((evClientDataSet1['CO_DEPARTMENT_NBR'] = DM_COMPANY.CO_DEPARTMENT['CO_DEPARTMENT_NBR']) or
       DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', evClientDataSet1['CO_DEPARTMENT_NBR'], [])) and
     (DM_COMPANY.CO_DEPARTMENT.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
  begin
    if s <> '' then
      s := s + ' | ';
    s := s + Trim(DM_COMPANY.CO_DEPARTMENT.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString);
  end;
  if not evClientDataSet1.FieldByName('CO_TEAM_NBR').IsNull and
     ((evClientDataSet1['CO_TEAM_NBR'] = DM_COMPANY.CO_TEAM['CO_TEAM_NBR']) or
       DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', evClientDataSet1['CO_TEAM_NBR'], [])) and
     (DM_COMPANY.CO_TEAM.FieldByName('PRINT_GROUP_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
  begin
    if s <> '' then
      s := s + ' | ';
    s := s + Trim(DM_COMPANY.CO_TEAM.FieldByName('CUSTOM_TEAM_NUMBER').AsString);
  end;
  DataSet['DBDT'] := s;
end;

procedure TdlgRedistrDBDT.OKBtnClick(Sender: TObject);
begin
  evClientDataSet1.CheckBrowseMode;

  if (FormatFloat('#,##0.00', ConvertNull(evClientDataSet1.Aggregates[0].Value, 0)) <> evLabel3.Caption) or
     (FormatFloat('#,##0.00', ConvertNull(evClientDataSet1.Aggregates[1].Value, 0)) <> evLabel4.Caption) then
  begin
    ModalResult := mrNone;
    EvErrMessage('Sum of hours and amounts must be equal to original hours and amount');
  end;
end;

procedure TdlgRedistrDBDT.evClientDataSet1NewRecord(DataSet: TDataSet);
var
  Hours, Amount: Currency;
begin
  Hours := StrToFloat(StringReplace(evLabel3.Caption, ',', '', [rfReplaceAll])) - ConvertNull(evClientDataSet1.Aggregates[0].Value, 0);
  Amount := StrToFloat(StringReplace(evLabel4.Caption, ',', '', [rfReplaceAll])) - ConvertNull(evClientDataSet1.Aggregates[1].Value, 0);
  if Hours <> 0 then
    DataSet['Hours'] := Hours;
  if Amount <> 0 then
    DataSet['Amount'] := Amount;
end;

procedure TdlgRedistrDBDT.evDataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  evDBGrid1.ColumnByName('Hours').FooterValue := FormatFloat('#,##0.00', ConvertNull(evClientDataSet1.Aggregates[0].Value, 0));
  evDBGrid1.ColumnByName('Amount').FooterValue := FormatFloat('#,##0.00', ConvertNull(evClientDataSet1.Aggregates[1].Value, 0));
end;

procedure TdlgRedistrDBDT.FormCreate(Sender: TObject);
var
  aggragate: TevKbmAggregate;
begin
  aggragate := evClientDataSet1.Aggregates.Add;
  aggragate.AggregateName := 'SumHours';
  aggragate.Active := True;
  aggragate.Expression := 'Sum(Hours)';

  aggragate := evClientDataSet1.Aggregates.Add;
  aggragate.AggregateName := 'SumAmount';
  aggragate.Active := True;
  aggragate.Expression := 'Sum(Amount)';
end;

end.
