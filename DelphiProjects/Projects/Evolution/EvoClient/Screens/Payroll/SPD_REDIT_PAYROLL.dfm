inherited REDIT_PAYROLL: TREDIT_PAYROLL
  Width = 873
  Height = 579
  object Panel5: TevPanel [0]
    Left = 0
    Top = 0
    Width = 873
    Height = 54
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvLowered
    Color = 4276545
    TabOrder = 0
    object pnlFavoriteReport: TevPanel
      Left = 705
      Top = 2
      Width = 166
      Height = 50
      Align = alRight
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      TabOrder = 0
      DesignSize = (
        166
        50)
      object evLabel47: TevLabel
        Left = 2
        Top = 2
        Width = 162
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Additional Tools'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object spbFavoriteReports: TevSpeedButton
        Left = 77
        Top = 16
        Width = 26
        Height = 25
        Hint = 'Favorite Reports(CTRL+F7)'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = spbFavoriteReportsClick
        Anchors = [akTop, akRight]
        NumGlyphs = 2
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFDDDDDDCCCCCCCCCCCCDDDDDDFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDECD
          CCCCCDCCCCDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDCDC
          CCCCCCCCCCCCCCCCCCCCCCCC5BA4CE2C8FC63191C767A9CDCCCCCCCCCCCCCCCC
          CCCCCCCCDCDCDCFFFFFFFFFFFFDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCACACAC9B
          9B9B9D9D9DB0AFAFCDCCCCCDCCCCCDCCCCCDCCCCDDDDDDFFFFFFFFFFFFC09F7A
          B88956B88A57BA8954C084472196D944B3EA69D2FE5FC3EF2C97D5D08843C98A
          4BC78A4EC8A076FFFFFFFFFFFF9B9B9B85848486858583838380807FA5A5A5BE
          BEBEDADADACBCBCBA5A5A58383838685858685859C9C9CFFFFFFCCCCCCBC884E
          FFDFAABC8D58B5844EFFF9E9FFF4DA1E8ECD5DC2EF82E0FF5EC4F11F88C61F8D
          CD2091D3718F94E1E1E1CDCCCC828282D8D8D88787877F7E7EF7F7F7F0F0F09C
          9C9CCACACAE4E4E4CCCCCC9796969C9C9CA1A0A08F8F8FE2E2E28D8D90C5CAD1
          57616EC6975EB4824BFFF2E3FFE9D3FFECD01D8DCD59C1EE6ED8FE53C1F457C7
          FA55C4F939A1D974AECF8E8E8ECDCCCC6464649191917D7C7CF1F1F1E7E7E6E8
          E8E89C9C9CCAC9C9DEDEDECBCBCBD1D1D1CECECEADADADB4B4B4FFFFFFBF894D
          AB7A3ECB9F65B2814BFCF1E3F7E4CEFEE5CCFFEBCB1A82BF6ACEF96ED2FB71D5
          FD72D7FF6FD4FC389AD0FFFFFF8383837574749898987C7B7BF0F0F0E2E2E2E3
          E3E2E7E7E6909090D6D6D6D9D9D9DCDBDBDEDEDEDBDADAA5A5A5CCCCCCBD874C
          FFDFA4D0A568B2804AFDF1E3F5E0C7F7E0C5FFE6C31A86C481E0FF82E1FF2F73
          B33074B487E6FF399DD3CDCCCC828181D7D7D79E9E9E7B7B7BF0F0F0DDDDDDDD
          DDDDE2E2E2959594E4E4E4E5E5E5828282838383E9E9E9A9A8A88E8D8FC5C9D1
          565F6DD9AE6FB2804AFCF1E4F3DBC1FFF7EFFFFFFF1B8AC872D7FD79DDFF3A7B
          B63C7CB87ADEFF3CA1D78E8E8ECCCCCC626262A7A7A77B7B7BF0F0F0D8D8D8F7
          F7F7FFFFFF989898DDDDDDE2E2E28989898A8A8AE3E3E2ACACACFFFFFFBF884D
          A67B3ADEB576B17F49FDF2E4F2D9BBFFFFFFC3BEBC6DA7C63CA9DE68D4FC6FDA
          FF6BD8FF49B5E98ECBECFFFFFF828282757474ADADAD7A7979F1F1F1D6D6D6FF
          FFFFBFBFBFADADADB4B4B4DBDADAE0DFDFDFDFDEBFBFBFD1D1D1CCCCCCBD874C
          FFE09EE3BB7AB17E49FDF3E7F2D6B6FFFFFF858383C9C1BD6CA7C82496D52A9A
          D92CA0E077969AFFFFFFCDCCCC828181D7D6D6B3B3B37A7979F3F2F2D3D2D2FF
          FFFF838383C2C2C1ADADADA4A4A4A8A8A7AEAEAE969595FFFFFF8E8D90C5C9D1
          555D6CEBC47FB17E49FEF5EAF1D3B3FEF4EBFFFFFFFFFFFFFFFFFFFFF9EBFFD9
          B1FFFAE7C08344FFFFFF8E8E8ECCCCCC616160BBBBBB7A7979F5F5F5CFCFCFF4
          F4F4FFFFFFFFFFFFFFFFFFF8F8F8D5D5D5F8F8F87F7E7EFFFFFFFFFFFFBF884D
          A57C35ECC581B07E49FEF8EEF0D2B1F1D3B1F3D5B3F3D5B2F3D4B1F3D3B1F3D3
          B0FFF9EEB5824BFFFFFFFFFFFF828282757474BDBCBC7A7979F7F7F7CECECECF
          CFCFD1D1D1D1D1D1D0D0D0CFCFCFCFCFCFF9F9F87D7C7CFFFFFFCCCCCCBD874C
          F9D08BEBC47FB07D48FFFAF2EFD0ADF1D4B2AB773ECFA67ACDA477F0D2B0EFD0
          ACFFFBF3B4824CFFFFFFCDCCCC828181C8C8C7BBBBBB797979FAFAFACCCCCCD0
          D0D0737373A1A1A1A09F9FCECECECCCCCCFBFBFB7D7C7CFFFFFF8D8D90C3C8CF
          525A67EBC37BB07D46FFFDF8EECCA6EFCEA9F0D1ACF0D1ACF0CFABEECDA8EECC
          A6FFFEF9B4834CFFFFFF8E8E8ECACACA5C5C5CBABABA787877FEFDFDC8C8C7CA
          CACACDCCCCCDCCCCCBCBCBC9C8C8C8C8C7FEFEFE7E7E7DFFFFFFFFFFFFBC864B
          FFF2D4FEF1D4B27F49FFFFFFFFFEFAFFFEFAFFFFFBFFFFFBFFFEFAFFFEFAFFFE
          FAFFFFFFB5844FFFFFFFFFFFFF818080EEEEEEEDEDED7B7B7BFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7E7EFFFFFFFFFFFFD0AF8A
          B78752B68652B78752B5844EB4824CB4824BB4824BB4824BB4824BB4824BB482
          4CB5844FCEAE8AFFFFFFFFFFFFAAAAAA8281818180808281817F7E7E7D7C7C7D
          7C7C7D7C7C7D7C7C7D7C7C7D7C7C7D7C7C7F7E7EAAA9A9FFFFFF}
        ParentColor = False
        ShortCut = 16502
      end
      object btnSwipeClock: TevSpeedButton
        Left = 104
        Top = 16
        Width = 26
        Height = 25
        Hint = 'EvoClock login'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = btnSwipeClockClick
        NumGlyphs = 2
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          1800000000000006000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFEFFFFECF7FFE8EFF9F2F5FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFDFDFDFA
          FAFAFCFCFCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFC4B6A5AD772FAB6000A85000A45600AE722FC2B09AFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE2E2E2BFBFBFAEAEAEA4
          A4A4A8A8A8BDBDBDE0E0E0FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFAF783CC57B18DDA652E7B466E2B878D8963DBB6E0ADABC9BB28B56FF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC1C1C1BFBFBFD6D6D6DDDDDDE0
          E0E0CFCFCFB6B6B6E4E4E4CCCCCCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          AD7934D89B3DF1D08EE5C989DDA558D19A49C78835F7FBFBF6D996BE6902B084
          50FF00FFFF00FFFF00FFFF00FFFF00FFC0C0C0D0D0D0E9E9E9E7E7E7D7D7D7D1
          D1D1C8C8C8FEFEFEEDEDEDB3B3B3C8C8C8FF00FFFF00FFFF00FFFF00FFC9A991
          D2922EF0D28FE6B86DDAA754D09549C3882EF7FDFFEECF8CEAC178EAB86CB865
          00CDC9C4FF00FFFF00FFFF00FFDDDDDDCBCBCBEAEAEADFDFDFD7D7D7CFCFCFC7
          C7C7FEFEFEE9E9E9E3E3E3DFDFDFB1B1B1EBEBEBFF00FFFF00FFFF00FFC78741
          DCBA6AE9C287DAA75BCC9B46C68019F3F3FFE7D4A1EDC070E2AF64E0B77CCA8C
          2CB2874EFF00FFFF00FFFF00FFC9C9C9DFDFDFE5E5E5D8D8D8D1D1D1C1C1C1FC
          FCFCECECECE2E2E2DBDBDBE0E0E0C8C8C8C9C9C9FF00FFFF00FFEDEEEEB9751E
          F1C171DDA658D29849C67E1ADDD4BDE8E4DBEDBD68DCAE67DCA655D09841C481
          2DBB7E26F7FFFFFF00FFF9F9F9BCBCBCE2E2E2D7D7D7D0D0D0C0C0C0EEEEEEF5
          F5F5E0E0E0DBDBDBD7D7D7D0D0D0C4C4C4C1C1C1FF00FFFF00FFE3DCD3C98020
          E5BD84D19746CB8631C4954EFAFFFFE0B86EE1AE66DAA555D19946CA9042CA8D
          2EBC7820F1F8FCFF00FFF2F2F2C2C2C2E3E3E3D0D0D0C7C7C7CFCFCFFFFFFFDF
          DFDFDBDBDBD6D6D6D0D0D0CCCCCCC9C9C9BEBEBEFDFDFDFF00FFE5E4DCC47918
          D99E4ACC8D35BC7100F3F5FCDCD0BDE8AF57D9A356D39845CD9036C6841FBC72
          00C18434F3FDFFFF00FFF5F5F5BEBEBED3D3D3CACACAB6B6B6FCFCFCEDEDEDDA
          DADAD6D6D6D0D0D0CBCBCBC3C3C3B7B7B7C5C5C5FEFEFEFF00FFFF00FFB56E0B
          C98D32C68C25C89C5AFCFFFFDFB167D7A255D09745C98C37C5831FBB7908BD7A
          16C48D4CFDFFFFFF00FFFF00FFB6B6B6C9C9C9C7C7C7D3D3D3FFFFFFDCDCDCD5
          D5D5D0D0D0C9C9C9C3C3C3BCBCBCBEBEBECCCCCCFF00FFFF00FFFF00FFC69050
          B9770EB36C00F0EEF1EDEDEAD09743D39744CB8D34C4811BBA7800BA7C1BCC87
          34CCB79BFF00FFFF00FFFF00FFCDCDCDBBBBBBB4B4B4FAFAFAF8F8F8CFCFCFD0
          D0D0CACACAC1C1C1BABABABFBFBFC7C7C7E2E2E2FF00FFFF00FFFF00FFEFEAE4
          B25D00BB7E32FF00FFDCCEB7CD8F32C98932C28015C18A23B86600BD7514BA7F
          30F7FFFFFF00FFFF00FFFF00FFF7F7F7ACACACC3C3C3FF00FFECECECCACACAC8
          C8C8C0C0C0C6C6C6B1B1B1BCBCBCC3C3C3FF00FFFF00FFFF00FFFF00FFFF00FF
          D5B78FCDB896FF00FFD9BC95C8842ACE953FBE7400B66C0CBE7612BB7A25EBF1
          F4FF00FFFF00FFFF00FFFF00FFFF00FFE2E2E2E2E2E2FF00FFE4E4E4C5C5C5CE
          CECEB8B8B8B6B6B6BCBCBCC0C0C0FAFAFAFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFCA995FB15B00B56600B46900B66B08C59861F6FCFFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFD3D3D3ABABABB1
          B1B1B2B2B2B4B4B4D3D3D3FEFEFEFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFEDEAE5D9BFA5D9BC97DCCCBAF8FBFFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF7F7F7E6E6E6E4
          E4E4EBEBEBFEFEFEFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
        ParentColor = False
        ShortCut = 0
      end
      object btnMRCLogIn: TevSpeedButton
        Left = 131
        Top = 16
        Width = 26
        Height = 25
        Hint = 'MRC login'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = btnMRCLogInClick
        NumGlyphs = 2
        Glyph.Data = {
          56060000424D5606000000000000360400002800000020000000110000000100
          08000000000020020000C40E0000C40E00000001000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
          A6000020400000206000002080000020A0000020C0000020E000004000000040
          20000040400000406000004080000040A0000040C0000040E000006000000060
          20000060400000606000006080000060A0000060C0000060E000008000000080
          20000080400000806000008080000080A0000080C0000080E00000A0000000A0
          200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
          200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
          200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
          20004000400040006000400080004000A0004000C0004000E000402000004020
          20004020400040206000402080004020A0004020C0004020E000404000004040
          20004040400040406000404080004040A0004040C0004040E000406000004060
          20004060400040606000406080004060A0004060C0004060E000408000004080
          20004080400040806000408080004080A0004080C0004080E00040A0000040A0
          200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
          200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
          200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
          20008000400080006000800080008000A0008000C0008000E000802000008020
          20008020400080206000802080008020A0008020C0008020E000804000008040
          20008040400080406000804080008040A0008040C0008040E000806000008060
          20008060400080606000806080008060A0008060C0008060E000808000008080
          20008080400080806000808080008080A0008080C0008080E00080A0000080A0
          200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
          200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
          200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
          2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
          2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
          2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
          2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
          2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
          2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
          2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FDFDFDFDFDFD
          FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD
          9D5657A757979FA754FDFDFDFDFDFDFDF7F7F707F7F70707F7FDFDFDFDFDFDFD
          9EEFEFEFEFEFEFEF56FDFDFDFDFDFDFD0708080808080808F7FDFDFDFDFDFDFD
          9F9FEF474757479F56FDFDFDFDFDFDFD070708A4A4F7A407F7FDFDFDFDFDFDFD
          9F4757579FA7974756FDFDFDFDFDFDFD07A4F7F70707F7A4F7FDFDFDFDFDFDFD
          9F479FEFEFEFA74F56FDFDFDFDFDFDFD07A40708080807A4F7FDFDFDFDFDFDFD
          9E479FA747A74F4756FDFDFDFDFDFDFD07A40707A407A4A4F7FDFDFDFDFDFDFD
          07EFEF9EEF9EEFEF07FDFDFDFDFDFDFD080808070807080808FDFDFDFDFDFDFD
          FDFDFDF7FF07FDFDFDFDFDFDFDFDFDFDFDFDFD07FF08FDFDFDFDF69BA4F7F7F7
          9CA4A4F5A2F2EEF79BFDFDF707070707F7070708F7080807F707FD08FDF508F6
          EBFFEBFFFFF109F6FDFDFDF6FD08F6FF07FF07FDFD07F6FDFDFDFDFDFDA3F7EB
          90FF58FF09E9F3FDFDFDFDFDFD070707A4FFA4FDF60708FDFDFDFDFDFDA3F759
          A20898FFEB09E9FDFDFDFDFDFD0707F7F7F6F7FD07F607FDFDFDFDFDFDA3A359
          07EB98FF98FDF4F4FDFDFDFDFD0707F70807F7FDF7FD0808FDFDFDFDFDA350AC
          FD5998F608FDF609FDFDFDFDFD07A407FDF7F7FDF6FDFDF6FDFDFDFDFDA34808
          FD5990FDFDFDFDFDFDFDFDFDFD075BF6FDF7A4FDFDFDFDFDFDFDFDFDFDFDFDFD
          FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD}
        ParentColor = False
        ShortCut = 0
      end
    end
    object pnlTopLeft: TevPanel
      Left = 2
      Top = 2
      Width = 703
      Height = 50
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      TabOrder = 1
      object PR_LABEL: TevLabel
        Left = 272
        Top = 12
        Width = 5
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lEeFilter: TLabel
        Left = 350
        Top = 28
        Width = 47
        Height = 13
        Caption = 'lEeFilter'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblChecks: TevLabel
        Left = 549
        Top = 12
        Width = 42
        Height = 13
        Caption = 'Checks: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lablClient: TevLabel
        Left = 8
        Top = 10
        Width = 26
        Height = 13
        Caption = 'Client'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TevLabel
        Left = 8
        Top = 26
        Width = 44
        Height = 13
        Caption = 'Company'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtCoNumber: TevDBText
        Left = 56
        Top = 26
        Width = 81
        Height = 16
        DataField = 'CUSTOM_COMPANY_NUMBER'
        DataSource = CoNotesDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtCoName: TevDBText
        Left = 136
        Top = 26
        Width = 201
        Height = 16
        DataField = 'NAME'
        DataSource = CoNotesDS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtClNumber: TevDBText
        Left = 56
        Top = 10
        Width = 81
        Height = 16
        DataField = 'CUSTOM_CLIENT_NUMBER'
        DataSource = dsCL
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtClName: TevDBText
        Left = 136
        Top = 10
        Width = 201
        Height = 16
        DataField = 'NAME'
        DataSource = dsCL
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object pcPayroll: TevPageControl [1]
    Left = 0
    Top = 54
    Width = 873
    Height = 482
    HelpContext = 1502
    ActivePage = tsCompany
    Align = alClient
    Images = PageControlImages
    PopupMenu = CalcPopup
    TabOrder = 1
    OnChange = pcPayrollChange
    OnChanging = pcPayrollChanging
    object tsCompany: TTabSheet
      Caption = 'Company'
      ImageIndex = 19
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 865
        Height = 453
        Align = alClient
        TabOrder = 0
        object evPanel1: TevPanel
          Left = 0
          Top = 0
          Width = 861
          Height = 449
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
        end
        object pnlBrowsePayroll: TisUIFashionPanel
          Left = 0
          Top = 0
          Width = 861
          Height = 449
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlBrowsePayroll'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Payroll'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          Margins.Top = 8
          Margins.Left = 8
          Margins.Right = 8
          Margins.Bottom = 8
          object pnlBrowseBody: TevPanel
            Left = 20
            Top = 44
            Width = 813
            Height = 377
            Align = alClient
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 1
            object Panel2: TevPanel
              Left = 0
              Top = 0
              Width = 813
              Height = 41
              Align = alTop
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object btnOpenCompany: TevBitBtn
                Left = 0
                Top = 8
                Width = 150
                Height = 25
                Caption = 'Open Company'
                TabOrder = 0
                OnClick = btnOpenCompanyClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                  8888D8888888888888888EEEEEEEE668888888888888888888888EE66666666F
                  FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                  FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                  FFF88DDDDDDDD88DDDD88EEEEEEEE6F777F88888888888DDDDD8877777767F7F
                  C7F88DDDDDD8DDD8FD8D8FFFFFF6777FC78D8DDDDDD8DDD8F8DD88888886FC8F
                  C8FC888DD88D8FD8FD8FDDDEE8D6DFCDDFCDDDD88DD8D8FDD8FDDDDEEEE6DDDD
                  DDDDDDD88888DDDDDDDDDDDDEEE6DFCFCFCDDDDD8888D8F8F8FDDDDDDDE6FCDF
                  CDFCDDDDDD8D8FD8FD8FDDDDDDD8DDDFCDDDDDDDDDD8DDD8FDDD}
                NumGlyphs = 2
                Margin = 0
              end
              object bOpenFilteredByDBDTG: TevBitBtn
                Left = 158
                Top = 8
                Width = 150
                Height = 25
                Caption = 'Open By D/B/D/T/PG'
                TabOrder = 1
                OnClick = bOpenFilteredByDBDTGClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                  8888D8888888888888888EEEEEEEE668888888888888888888888EE66666666F
                  FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                  FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                  FFF88DDDDDDDD88DDDD88EEEEEEEE6F777F88888888888DDDDD8877777767F7F
                  C7F88DDDDDD8DDD8FD8D8FFFFFF6777FC78D8DDDDDD8DDD8F8DD88888886FC8F
                  C8FC888DD88D8FD8FD8FDDDEE8D6DFCDDFCDDDD88DD8D8FDD8FDDDDEEEE6DDDD
                  DDDDDDD88888DDDDDDDDDDDDEEE6DFCFCFCDDDDD8888D8F8F8FDDDDDDDE6FCDF
                  CDFCDDDDDD8D8FD8FD8FDDDDDDD8DDDFCDDDDDDDDDD8DDD8FDDD}
                NumGlyphs = 2
                Margin = 0
              end
              object bOpenFilteredByEe: TevBitBtn
                Left = 316
                Top = 8
                Width = 150
                Height = 25
                Caption = 'Open By EE#,SSN,Name'
                TabOrder = 2
                OnClick = bOpenFilteredByEeClick
                Color = clBlack
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                  8888D8888888888888888EEEEEEEE668888888888888888888888EE66666666F
                  FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                  FFF88DDDDDDDD88DDDD88EEEEEEEE66777F88DD88888888D88D88EE66666666F
                  FFF88DDDDDDDD88DDDD88EEEEEEEE6F777F88888888888DDDDD8877777767F7F
                  C7F88DDDDDD8DDD8FD8D8FFFFFF6777FC78D8DDDDDD8DDD8F8DD88888886FC8F
                  C8FC888DD88D8FD8FD8FDDDEE8D6DFCDDFCDDDD88DD8D8FDD8FDDDDEEEE6DDDD
                  DDDDDDD88888DDDDDDDDDDDDEEE6DFCFCFCDDDDD8888D8F8F8FDDDDDDDE6FCDF
                  CDFCDDDDDD8D8FD8FD8FDDDDDDD8DDDFCDDDDDDDDDD8DDD8FDDD}
                NumGlyphs = 2
                Margin = 0
              end
            end
            object sbListOfCompanies: TScrollBox
              Left = 0
              Top = 41
              Width = 813
              Height = 336
              Align = alClient
              Color = clBtnFace
              ParentColor = False
              TabOrder = 1
              object pnlListOfCompaniesBorder: TevPanel
                Left = 0
                Top = 0
                Width = 809
                Height = 332
                Align = alClient
                BevelOuter = bvNone
                BorderWidth = 12
                TabOrder = 0
                object fpListOfCompanies: TisUIFashionPanel
                  Left = 12
                  Top = 12
                  Width = 785
                  Height = 308
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 12
                  Caption = 'fpListOfCompanies'
                  Color = 14737632
                  TabOrder = 0
                  RoundRect = True
                  ShadowDepth = 8
                  ShadowSpace = 8
                  ShowShadow = True
                  ShadowColor = clSilver
                  TitleColor = clGrayText
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWhite
                  TitleFont.Height = -13
                  TitleFont.Name = 'Arial'
                  TitleFont.Style = [fsBold]
                  Title = 'Company'
                  LineWidth = 0
                  LineColor = clWhite
                  Theme = ttCustom
                  object wwDBGrid7: TevDBGrid
                    Left = 12
                    Top = 36
                    Width = 753
                    Height = 252
                    DisableThemesInTitle = False
                    Selected.Strings = (
                      'CUSTOM_COMPANY_NUMBER'#9'20'#9'Company Number'#9'F'
                      'NAME'#9'40'#9'Company Name'#9'F')
                    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                    IniAttributes.SectionName = 'TREDIT_PAYROLL\wwDBGrid7'
                    IniAttributes.Delimiter = ';;'
                    ExportOptions.ExportType = wwgetSYLK
                    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                    TitleColor = clBtnFace
                    FixedCols = 0
                    ShowHorzScrollBar = True
                    Align = alClient
                    DataSource = wwdsList
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                    TabOrder = 0
                    TitleAlignment = taLeftJustify
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    TitleLines = 1
                    OnDblClick = wwDBGrid7DblClick
                    OnKeyDown = wwDBGrid7KeyDown
                    PaintOptions.AlternatingRowColor = 14544093
                    PaintOptions.ActiveRecordColor = clBlack
                    NoFire = False
                  end
                end
              end
            end
          end
          object edHiddenPayrollField: TEdit
            Left = 2000
            Top = 56
            Width = 121
            Height = 21
            TabOrder = 0
            Text = 'edHiddenPayrollField'
          end
        end
      end
    end
    object tsCoNotes: TTabSheet
      Caption = 'Notes'
      ImageIndex = 12
      object sbNotes: TScrollBox
        Left = 0
        Top = 0
        Width = 865
        Height = 453
        Align = alClient
        TabOrder = 0
        object evPanel3: TevPanel
          Left = 0
          Top = 0
          Width = 861
          Height = 449
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          object pnlNotes: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 845
            Height = 433
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Company Notes'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlNotesBody: TevPanel
              Left = 12
              Top = 36
              Width = 813
              Height = 377
              Align = alClient
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object CoNotesMemo: TEvDBMemo
                Left = 0
                Top = 0
                Width = 813
                Height = 377
                HelpContext = 1502
                Align = alClient
                BevelInner = bvNone
                BevelOuter = bvNone
                BevelKind = bkFlat
                DataField = 'NATURE_OF_BUSINESS'
                DataSource = CoNotesDS
                TabOrder = 0
              end
            end
          end
        end
      end
    end
    object tsPayroll: TTabSheet
      Caption = 'Payroll'
      ImageIndex = 20
      OnShow = tsPayrollShow
      object sbmain: TScrollBox
        Left = 0
        Top = 0
        Width = 865
        Height = 453
        Align = alClient
        TabOrder = 0
        object pnlBorder: TevPanel
          Left = 0
          Top = 0
          Width = 861
          Height = 234
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel3: TevPanel
            Left = 0
            Top = 405
            Width = 1138
            Height = 216
            HelpContext = 1502
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
          end
          object Panel4: TevPanel
            Left = 848
            Top = 112
            Width = 534
            Height = 458
            HelpContext = 1502
            BevelOuter = bvNone
            TabOrder = 1
            Visible = False
          end
          object pnlList: TisUIFashionPanel
            Left = 0
            Top = 0
            Width = 672
            Height = 234
            Align = alLeft
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            Constraints.MinHeight = 186
            TabOrder = 2
            OnResize = pnlListResize
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            Margins.Top = 8
            Margins.Left = 8
            Margins.Right = 8
            Margins.Bottom = 8
            object pnlListBody: TevPanel
              Left = 20
              Top = 44
              Width = 624
              Height = 34
              Align = alTop
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object Label9: TevLabel
                Left = 0
                Top = 4
                Width = 105
                Height = 13
                Caption = 'Display Payrolls As Of '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object btnOpenPayroll: TevBitBtn
                Left = 472
                Top = 0
                Width = 150
                Height = 21
                Caption = 'Open Payroll'
                TabOrder = 0
                OnClick = btnOpenPayrollClick
                Color = clBlack
                Glyph.Data = {
                  36060000424D3606000000000000360000002800000020000000100000000100
                  18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFB8C6D0B0BFCCACBBCAADBCCAADBCCAADBCCAADBCCAADBC
                  CAABBBC9C6D1D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9C8C8C2C2C1BE
                  BEBEBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBEBEBED3D3D3FFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFF4598D157ABDA6CC2E565BDE466BDE465BDE465BDE465BD
                  E46BC0E573B5DCF2F5F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA5A5A5B4B4B4C8
                  C8C7C4C4C4C4C4C4C4C4C4C4C4C4C4C4C4C7C7C7BDBCBCF7F7F7DCDCDCCCCCCC
                  CCCCCCCCCCCCCCCCCC4197D170C1E6A3F3FF85EBFF86EBFF8CEDFF8CEDFF8BED
                  FF97F2FF80CAEAA8BECFDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCA4A4A4C8C8C7F3
                  F3F3ECECECECECECEEEEEEEEEEEEEEEEEEF2F2F1CFCFCFC2C2C18CBAA36DB08F
                  6EB18F6FB18F6EB18F4DA1D767BAE6B0E8F9A7EFFFA2ECFF7BDFFC79DDFB78DD
                  FB7CDFFD93DCF66AA7B7B4B4B4A7A7A7A8A8A7A8A8A7A8A8A7ACACACC3C2C2EB
                  EBEAF0F0F0EEEEEEE3E3E2E1E1E1E1E1E1E4E3E3E0E0E0A9A8A86AAD8BCBF4DB
                  8CC7B583BEA793CBBB57A9DA76C7EC5FA9D970B1DC6EB0DD66CAF176DDFC77DC
                  FC74DBFC99E6FE74B4CBA3A3A3EEEEEEC1C1C1B8B7B7C6C6C6B3B3B3CECDCDB3
                  B3B3BABABAB9B9B9D1D1D1E1E1E1E0E0E0E0DFDFE9E9E9B8B7B765A986CAF3DC
                  5FA27C4E9368B8EBCF59A6D899E8FD64CCF366CAF266CAF150AFE085C6E9A3D3
                  ED9AD0EDA6DAF070B5DFA09F9FEDEDED989898888888E4E3E3B1B0B0EBEBEAD3
                  D2D2D1D1D1D1D1D1B8B8B8CDCCCCD8D8D8D6D6D6DEDEDEBEBDBD62A581CDF2E0
                  73B593B3E7CDB0E4CA5BA7D89CE7FC66D5FA71DAFB75DDFE6ED5F852B3E44AAE
                  E26CB9E5479AD04E9AA49B9B9BEDEDEDABABABE0E0E0DDDDDDB2B1B1EAEAEADB
                  DADADFDFDEE2E2E2DBDADABDBCBCB8B8B8C2C2C1A6A6A69A9A9A5FA37ED0F2E4
                  6CAD8BABE2C851946D5FA8D8B4EFFF87E0FCB5E8FAA5D8F1A6DBF3A8DDF3A7DC
                  F3AEE0F364AED85DA27F999999EFEEEEA4A4A4DBDADA8A8A8AB2B2B2F1F1F1E4
                  E4E4EBEBEADCDCDCE0DFDFE0E0E0E0E0E0E3E3E2B6B6B59898985CA07AD8F5E8
                  67AA85A7E0C6A5DEC45BA9D485C2E589C3E569AFDC4293A871B9C771B9C772BA
                  C94F9DA6ABD9E25CA07A969595F2F2F1A1A0A0D9D9D9D7D6D6B2B1B1C9C8C8CA
                  C9C9B8B8B8969595BABABABABABABBBBBB9D9D9DDADADA969595599D75DFF7EF
                  549A705EA27D9FDBC39FDBC280BFA07EB89478B38F80BFA09FDBC29FDBC35EA2
                  7D559B73DFF7EF599D75939393F5F5F58F8F8F989898D4D4D4D4D4D4B7B6B6AF
                  AFAFAAA9A9B7B6B6D4D4D4D4D4D4989898919191F5F5F5939393569B72E5F9F4
                  448B5E468C615A9E765A9D755A9E765B9F775B9F775A9E765A9D755A9E76468C
                  61448B5EE5F9F4569B72919191F8F8F880807F82818194939393939394939395
                  959495959494939393939394939382818180807FF8F8F891919154976FE7FAF5
                  E3F6F1E3F6F1E3F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E3F5F0E3F6
                  F1E3F6F1E7FAF554976F8D8D8DFAF9F9F5F5F5F5F5F5F4F4F4F4F4F4F4F4F4F4
                  F4F4F4F4F4F4F4F4F4F4F4F4F4F4F5F5F5F5F5F5FAF9F98D8D8D8CB99E89C6A6
                  B0E7CCAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5
                  CAB0E7CC89C6A68CB99EB2B2B2BEBDBDE0DFDFDEDEDEDEDEDEDEDEDEDEDEDEDE
                  DEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEE0DFDFBEBDBDB2B2B2FFFFFF8AB79B
                  5092694F91684F91684F91684F91684F91684F91684F91684F91684F91684F91
                  685092698AB79BFFFFFFFFFFFFB0AFAF87878786868686868686868686868686
                  8686868686868686868686868686868686878787B0AFAFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                  FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
                NumGlyphs = 2
                Margin = 0
              end
              object dtPayrollAsOfDate: TevDBDateTimePicker
                Left = 113
                Top = 0
                Width = 121
                Height = 21
                CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                CalendarAttributes.Font.Color = clWindowText
                CalendarAttributes.Font.Height = -11
                CalendarAttributes.Font.Name = 'MS Sans Serif'
                CalendarAttributes.Font.Style = []
                CalendarAttributes.PopupYearOptions.StartYear = 2000
                Epoch = 1950
                ShowButton = True
                TabOrder = 1
                OnCloseUp = dtPayrollAsOfDateCloseUp
                OnChange = dtPayrollAsOfDateChange
              end
              object edHiddenField: TEdit
                Left = 500
                Top = 144
                Width = 121
                Height = 21
                TabOrder = 2
                Text = 'edHiddenField'
              end
            end
            object wwDBGrid6: TevDBGrid
              Left = 20
              Top = 78
              Width = 624
              Height = 128
              HelpContext = 1502
              DisableThemesInTitle = False
              Selected.Strings = (
                'RUN_NUMBER'#9'10'#9'Run'#9'F'
                'CHECK_DATE'#9'15'#9'Check Date'#9'F'
                'CHECK_DATE_STATUS'#9'20'#9'Check Date Status'#9'F'
                'SCHEDULED'#9'10'#9'Scheduled'#9'F'
                'PAYROLL_TYPE'#9'7'#9'Type'#9'F'
                'STATUS'#9'9'#9'Status'#9'F'
                'STATUS_DATE'#9'21'#9'Status Date'#9'F')
              IniAttributes.Enabled = False
              IniAttributes.SaveToRegistry = False
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TREDIT_PAYROLL\wwDBGrid6'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = wwdsMaster
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 1
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = wwDBGrid6DblClick
              OnKeyDown = wwDBGrid6KeyDown
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
        end
        object pnlDetailsControls: TPanel
          Left = 0
          Top = 234
          Width = 861
          Height = 215
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object pnlPayrollInfo: TisUIFashionPanel
            Left = 0
            Top = 0
            Width = 520
            Height = 215
            Align = alLeft
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Payroll Detail'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            Margins.Left = 8
            Margins.Right = 4
            Margins.Bottom = 8
            object Label6: TevLabel
              Left = 396
              Top = 109
              Width = 66
              Height = 16
              Caption = '~Check Date'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label7: TevLabel
              Left = 396
              Top = 31
              Width = 69
              Height = 16
              Caption = '~Run Number'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label10: TevLabel
              Left = 396
              Top = 70
              Width = 33
              Height = 16
              Caption = '~Type'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label13: TevLabel
              Left = 21
              Top = 31
              Width = 117
              Height = 13
              Caption = 'Payroll Check Comments'
            end
            object evLabel4: TevLabel
              Left = 396
              Top = 148
              Width = 88
              Height = 13
              Caption = 'Actual Call-In Date'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object wwDBComboBox1: TevDBComboBox
              Left = 396
              Top = 85
              Width = 100
              Height = 21
              HelpContext = 1502
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'PAYROLL_TYPE'
              DataSource = wwdsMaster
              DropDownCount = 8
              ItemHeight = 0
              Items.Strings = (
                'Regular'#9'R'
                'Supplemental'#9'S'
                'Reversal'#9'V'
                'Client Correction'#9'C'
                'SB Correction'#9'B'
                'Setup Run'#9'E'
                'Misc Check Adjustment'#9'M'
                'Tax Adjustment'#9'T'
                'Wage Adj with Qtr'#9'Q'
                'Wage Adj no Qtr'#9'N'
                'Qtr End Tax Adjustment'#9'A'
                'Tax Deposit'#9'D'
                'Import'#9'I'
                'Backdated Setup'#9'K')
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 5
              UnboundDataType = wwDefault
              OnChange = wwDBComboBox1Change
            end
            object DBMemo1: TEvDBMemo
              Left = 21
              Top = 46
              Width = 367
              Height = 57
              HelpContext = 1502
              DataField = 'PAYROLL_COMMENTS'
              DataSource = wwdsMaster
              TabOrder = 0
              OnChange = DBMemo1Change
            end
            object wwDBDateTimePicker1: TevDBDateTimePicker
              Left = 396
              Top = 124
              Width = 100
              Height = 21
              HelpContext = 1502
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'CHECK_DATE'
              DataSource = wwdsMaster
              Epoch = 1950
              ShowButton = True
              TabOrder = 6
              OnKeyDown = wwDBDateTimePicker1KeyDown
            end
            object evDBRadioGroup1: TevDBRadioGroup
              Left = 257
              Top = 109
              Width = 132
              Height = 75
              Caption = '~Block Checks/Reports'
              Columns = 2
              DataField = 'EXCLUDE_R_C_B_0R_N'
              DataSource = wwdsMaster
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Reports'
                'Checks'
                'Both'
                'None')
              ParentFont = False
              TabOrder = 3
              Values.Strings = (
                'R'
                'C'
                'B'
                'N')
            end
            object evDBRadioGroup2: TevDBRadioGroup
              Left = 21
              Top = 109
              Width = 100
              Height = 75
              HelpContext = 1502
              Caption = '~Block Agencies'
              DataField = 'EXCLUDE_AGENCY'
              DataSource = wwdsMaster
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'Yes'
                'No')
              ParentFont = False
              TabOrder = 1
              Values.Strings = (
                'Y'
                'N')
            end
            object evDBDateTimePicker1: TevDBDateTimePicker
              Left = 396
              Top = 163
              Width = 100
              Height = 21
              HelpContext = 1502
              CalendarAttributes.Font.Charset = DEFAULT_CHARSET
              CalendarAttributes.Font.Color = clWindowText
              CalendarAttributes.Font.Height = -11
              CalendarAttributes.Font.Name = 'MS Sans Serif'
              CalendarAttributes.Font.Style = []
              CalendarAttributes.PopupYearOptions.StartYear = 2000
              DataField = 'ACTUAL_CALL_IN_DATE'
              DataSource = wwdsMaster
              Epoch = 1950
              ShowButton = True
              TabOrder = 7
              OnKeyDown = wwDBDateTimePicker1KeyDown
            end
            object evDBRadioGroup3: TevDBRadioGroup
              Left = 129
              Top = 109
              Width = 120
              Height = 75
              Caption = '~Block TO Accrual'
              Columns = 2
              DataField = 'EXCLUDE_TIME_OFF'
              DataSource = wwdsMaster
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Items.Strings = (
                'All'
                'Accrual'
                'No')
              ParentFont = False
              TabOrder = 2
              Values.Strings = (
                'F'
                'Y'
                'N')
            end
            object evDBSpinEdit1: TevDBSpinEdit
              Left = 396
              Top = 46
              Width = 100
              Height = 21
              Increment = 1.000000000000000000
              MaxValue = 99.000000000000000000
              MinValue = 1.000000000000000000
              Value = 1.000000000000000000
              DataField = 'RUN_NUMBER'
              DataSource = wwdsMaster
              TabOrder = 4
              UnboundDataType = wwDefault
            end
          end
          object pnlControls: TisUIFashionPanel
            Left = 520
            Top = 0
            Width = 152
            Height = 215
            Align = alLeft
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'pnlControls'
            Color = 14737632
            TabOrder = 1
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Controls'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            Margins.Left = 4
            Margins.Right = 8
            Margins.Bottom = 8
            object CreatePayrollBtn: TevBitBtn
              Left = 12
              Top = 30
              Width = 114
              Height = 25
              Caption = 'Create Payroll'
              TabOrder = 0
              OnClick = CreatePayrollBtnClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCC
                CCCCCCCCE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFE2E2E2CDCCCCCDCCCCCDCCCCE2E2E2FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E153A882008C4C008B
                4A008C4C53A882E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFE2E2E29E9E9E7E7E7D7C7B7B7E7E7D9E9E9EE2E2E2FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF54A88100995B00BB8677E0
                C600BB8600995C53A882FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF9D9D9D8A8A8AACACACD7D6D6ACACAC8A8A8A9E9E9EDCDCDCCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00894600C18D00BC83FFFF
                FF00BC8300C18D008C4CDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
                CCCCCDCCCC7A7979B2B2B2ACACACFFFFFFACACACB2B2B27E7E7D8CBAA36DB08F
                6EB18F6FB18F6EB18F6EB08E6EB18E70B1907AB49500864175E6CDFFFFFFFFFF
                FFFFFFFF77E7CE008A49B4B4B4A7A7A7A8A8A7A8A8A7A8A8A7A7A7A7A8A8A7A8
                A8A7ABABAB777676DDDDDDFFFFFFFFFFFFFFFFFFDEDEDE7B7B7B6AAD8BCBF4DB
                8CC7B583BEA793CBBB92CAB992CBBA95CCBCA0CFC200854000CB9700C990FFFF
                FF00C99000CC98008A49A3A3A3EEEEEEC1C1C1B8B7B7C6C6C6C4C4C4C5C5C5C7
                C7C7CBCBCB767675BCBCBBB9B9B9FFFFFFB9B9B9BDBCBC7B7B7B65A986CAF3DC
                5FA27C4E9368B8EBCFB7E9CD98D1B288C4A391C8A92C9B67009F6000D39D74EE
                D400D39C00A061309B68A09F9FEDEDED989898888888E4E3E3E2E2E2C9C8C8BB
                BBBBC0C0C08E8E8E909090C4C3C3E4E4E4C4C3C39191918E8E8E62A581CDF2E0
                73B593B3E7CDB0E4CAB0E4CA6BAC89B3E4C7B6E5C97AB2924AB28200843F0083
                3E00833E56B78A72AB8A9B9B9BEDEDEDABABABE0E0E0DDDDDDDDDDDDA2A2A2DC
                DCDCDEDEDEAAA9A9A5A5A5757474747373747373AAAAAAA2A2A25FA37ED0F2E4
                6CAD8BABE2C851946DABE2C86DAC88B0E2C6B1E2C671AE8AB5E6CF609B76BAE8
                D179B292DAF6E963A580999999EFEEEEA4A4A4DBDADA8A8A8ADBDADAA2A2A2DB
                DADADBDADAA4A4A4E0DFDF929292E2E2E2AAA9A9F3F3F39B9B9B5CA07AD8F5E8
                67AA85A7E0C6A5DEC4A5DEC461A17CB2E3C6B2E3C661A27CA7DFC6A7DFC6A9E1
                C869AB86D9F5E95DA07A969595F2F2F1A1A0A0D9D9D9D7D6D6D7D6D6979797DC
                DBDBDCDBDB989898D8D8D8D8D8D8DADADAA1A1A1F2F2F1969595599D75DFF7EF
                549A705EA27D9FDBC39FDBC280BFA07EB89478B38F80BFA09FDBC29FDBC35EA2
                7D559B73DFF7EF599D75939393F5F5F58F8F8F989898D4D4D4D4D4D4B7B6B6AF
                AFAFAAA9A9B7B6B6D4D4D4D4D4D4989898919191F5F5F5939393569B72E5F9F4
                448B5E468C615A9E765A9D755A9E765B9F775B9F775A9E765A9D755A9E76468C
                61448B5EE5F9F4569B72919191F8F8F880807F82818194939393939394939395
                959495959494939393939394939382818180807FF8F8F891919154976FE7FAF5
                E3F6F1E3F6F1E3F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E3F5F0E3F6
                F1E3F6F1E7FAF554976F8D8D8DFAF9F9F5F5F5F5F5F5F4F4F4F4F4F4F4F4F4F4
                F4F4F4F4F4F4F4F4F4F4F4F4F4F4F5F5F5F5F5F5FAF9F98D8D8D8CB99E89C6A6
                B0E7CCAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5
                CAB0E7CC89C6A68CB99EB2B2B2BEBDBDE0DFDFDEDEDEDEDEDEDEDEDEDEDEDEDE
                DEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEE0DFDFBEBDBDB2B2B2FFFFFF8AB79B
                5092694F91684F91684F91684F91684F91684F91684F91684F91684F91684F91
                685092698AB79BFFFFFFFFFFFFB0AFAF87878786868686868686868686868686
                8686868686868686868686868686868686878787B0AFAFFFFFFF}
              NumGlyphs = 2
              Margin = 0
            end
            object SBReviewBtn: TevBitBtn
              Left = 12
              Top = 57
              Width = 114
              Height = 25
              Caption = 'SB Review '
              TabOrder = 1
              OnClick = SBReviewBtnClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360400002800000020000000100000000100
                08000000000000020000130B0000130B00000001000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
                A6000020400000206000002080000020A0000020C0000020E000004000000040
                20000040400000406000004080000040A0000040C0000040E000006000000060
                20000060400000606000006080000060A0000060C0000060E000008000000080
                20000080400000806000008080000080A0000080C0000080E00000A0000000A0
                200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
                200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
                200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
                20004000400040006000400080004000A0004000C0004000E000402000004020
                20004020400040206000402080004020A0004020C0004020E000404000004040
                20004040400040406000404080004040A0004040C0004040E000406000004060
                20004060400040606000406080004060A0004060C0004060E000408000004080
                20004080400040806000408080004080A0004080C0004080E00040A0000040A0
                200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
                200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
                200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
                20008000400080006000800080008000A0008000C0008000E000802000008020
                20008020400080206000802080008020A0008020C0008020E000804000008040
                20008040400080406000804080008040A0008040C0008040E000806000008060
                20008060400080606000806080008060A0008060C0008060E000808000008080
                20008080400080806000808080008080A0008080C0008080E00080A0000080A0
                200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
                200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
                200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
                2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
                2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
                2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
                2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
                2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
                2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
                2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00080909090909
                090909090909090909080807070707070707070707070707070809F3F3F3F4E0
                999899E009E0999999E00707F70707A4A4A49BA407A4A4A49BA4ECFFFFFFFF98
                09F4E051FF9809F4E091F7FFFFFFFFA40707A45BFFA40707A49BF3FFFFFFFF99
                F4F4E0519999F4F4E091F7FFFFFFFFA40707A45BA4A40707A45BF3FFFFFFFF99
                F608F451ECD808F6099107FFFFFFFFA40808075BF5A40808075BF3FFF6F6FF91
                999191EBECF49999999107FFF6F6FF9BA49B9BF7F707A49B9B9BF3FFF6F6FFFF
                91F5E851EC91F5E291FF07FFF6F6FFFF9B07F75BF7A407F79BFFF3FFF6F6F6FF
                E39991EA99EC9991E8FF07FFF6F6F6FFF7A49BF7A3F7A49BF7FFF3FFF6F6F6F6
                FF91EC51FF99EC99FFFF07FFF6F6F6F6FF9BF75BFFA4F79BFFFFF3FFF6F6F6F6
                F6999151FF999991FFFF07FFF6F6F6F6F6A35B52FFA4A39BFFFFF3FFF6F6F6F6
                F6FFFFFFFFFFF4FFFFFF07FFF6F6F6F6F6FFFFFFFFFF07FFFFFFF3FFF6F6F6F6
                F6FF09F2F2FFF3FFFFFF07FF08F6F6F6F6FF0707F5FF07FFFFFFF3FFF6F6F6F6
                F6FFF2F6FFF609FFFFFFF7FFF608F608F6FFF7F6FFF607FFFFFFF3FF08080808
                08FFF2FFF609FFFFFFFF07FF0808080808FFF7FFF607FFFFFFFFF3FFFFFFFFFF
                FFFFFFF609FFFFFFFFFF07FFFFFFFFFFFFFFFFF607FFFFFFFFFFF4ECF3F3F3F3
                F3F3F3F4FFFFFFFFFFFF07F707F7F7F707F70707FFFFFFFFFFFF}
              NumGlyphs = 2
              Margin = 0
            end
            object SubmitBtn: TevBitBtn
              Left = 12
              Top = 84
              Width = 114
              Height = 25
              Caption = 'Submit Payroll'
              TabOrder = 2
              OnClick = SubmitBtnClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360400002800000020000000100000000100
                08000000000000020000C30E0000C30E00000001000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
                A6000020400000206000002080000020A0000020C0000020E000004000000040
                20000040400000406000004080000040A0000040C0000040E000006000000060
                20000060400000606000006080000060A0000060C0000060E000008000000080
                20000080400000806000008080000080A0000080C0000080E00000A0000000A0
                200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
                200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
                200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
                20004000400040006000400080004000A0004000C0004000E000402000004020
                20004020400040206000402080004020A0004020C0004020E000404000004040
                20004040400040406000404080004040A0004040C0004040E000406000004060
                20004060400040606000406080004060A0004060C0004060E000408000004080
                20004080400040806000408080004080A0004080C0004080E00040A0000040A0
                200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
                200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
                200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
                20008000400080006000800080008000A0008000C0008000E000802000008020
                20008020400080206000802080008020A0008020C0008020E000804000008040
                20008040400080406000804080008040A0008040C0008040E000806000008060
                20008060400080606000806080008060A0008060C0008060E000808000008080
                20008080400080806000808080008080A0008080C0008080E00080A0000080A0
                200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
                200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
                200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
                2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
                2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
                2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
                2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
                2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
                2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
                2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFF607F7ACACF7F6FFFFFFFFFFFFFFFFFFFF07F7F7ADF7FFFFFFFFFFFFFFFF
                FF086C232323232263F6FFFFFFFFFFFFFF07A49B5B9B9B5B9BF6FFFFFFFFFFFF
                F66C2C343D3D352C2B63FFFFFFFFFFFFF6F7A4AD0707F7A4A4A4080707070707
                6B2C3D3DBEBE3D3D2C22080707070707A4A407ED07080707A45BB5B4B4B4B4B4
                1A357DF6F6FFBE353523EEF7F7F7F7F752F707F6FFFF07F7F79BAC08B6B5B6B6
                2B7D08F675BEFF083523AD0707070707A40707FFF708FF07F75BAC086C6B0808
                2B7E75353535BEFF3523F707F7A40707A407F7F7F7F707FFF79BAC08B5080808
                637536353535357E3463F707070708089CF707F7F7F7F707F7A46CF6AC086B08
                AC6CBE7E2E35757403ACA4F6F707F707F7A50807F7F7F7F79CAD6CF6AC080808
                AC0874746D746C6B086CA4FFA40807F6A408F7F7ADF7ADA408A46CF66C6CBEBE
                B5B5B4B5BEBE6C6CF66CA4FFA5F70707EDEEF7070707F7A4FFA46CF663636C6C
                6C6C6C6C6C6C6363F66CA4FF9BA4A5F7A4ACA5A4F7A49BA4F6F76BF6F6F6F6F6
                F6F6F6F6F6F6F6F6F66BA4FFFFF6FFF6FFFFFFFFFFFFFFF6F6A4B5B508080808
                0808080808080808B5B507F708070807080708070807080707F7FFB56B6B6B6B
                6B6B6B6B6B6B6B6BB5FFF6EEA4A4F7A4A4A5A4F7A4A5A4A5EDFF}
              NumGlyphs = 2
              Margin = 0
            end
            object PreprocessBtn: TevBitBtn
              Left = 12
              Top = 112
              Width = 114
              Height = 25
              Caption = 'Pre-Process'
              TabOrder = 3
              OnClick = PreprocessBtnClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCC
                CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                E5E5E5CCCCCCCCCCCCE9E9E95A70814A67855191DACCCCCCCCCCCCCCCCCCE4E4
                E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E5CCCCCCCCCCCCE9E9E97473736D
                6D6DA3A3A3CCCCCCCCCCCCCCCCCCE4E4E4FFFFFFFFFFFFFFFFFFFFFFFFCCCCCC
                8F8D8E514F4F524F4F9A96955F8AA881A8B88FD6FF346CA15A4D4355504D8D8B
                8BEDEDEDFFFFFFFFFFFFFFFFFFCCCCCC8E8E8E4F4F4F4F4F4F979696909090AA
                AAAADEDEDE7979794B4B4B5050508A8A8AEDEDEDFFFFFFFFFFFFFFFFFF8A8785
                5D5B5A8A88878B888761595547B7FD8CE8FF81D4FF149CFF376EA3988B81615C
                59C0BFBEFFFFFFFFFFFFFFFFFF8787875B5B5B888888888888595959C7C7C7EB
                EBEADCDCDCB6B6B57B7B7B8A8A8A5B5B5BBFBFBFFFFFFFFFFFFFFFFFFFC9C7C5
                6C6968CCCAC9CCCAC96F6865D6C7BD2B79C944C7FF2CABFF189EFF356DA17B6E
                64A39E9AFFFFFFFFFFFFFFFFFFC8C8C7696969CBCBCBCBCBCB686868C7C7C78E
                8E8ED2D2D1BFBFBFB7B6B67A79796D6D6D9E9E9EFFFFFFFFFFFFFFFFFFFFFFFF
                BEBDBC817E7D817E7DC1BFBDFFFFFFFFFFFF357DC849CAFF2DACFF149DFF2C6A
                A3A49C95FFFFFFFFFFFFFFFFFFFFFFFFBDBCBC7F7E7E7F7E7EBFBFBFFFFFFFFF
                FFFF909090D4D4D4C0C0C0B7B6B67878779C9C9CFFFFFFFFFFFFFFFFFFFFFFFF
                DDDDDDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC327CC943C9FF21ABFF83B0
                D77D7770CCCCCCFFFFFFFFFFFFFFFFFFDDDDDDCCCCCCCCCCCCCCCCCCCCCCCCCC
                CCCCCCCCCC909090D3D3D3BFBFBFB8B8B8777676CCCCCCFFFFFFFFFFFFDEDEDE
                9897959B989691929290919190909093908D97908A9C90862478CCAFDCF19288
                80C1BFB8777C6ECCCCCCFFFFFFDEDEDE97979798989893939392929291919190
                90909090908F8F8F8E8E8EE0DFDF878787BEBEBE7A7979CCCCCCDDDDDDABAAA8
                A8A7A5F0EFEEB7B6B5EDECEBB6B5B3EDECEAB7B5B3F0ECE8BEB7B2867F79EBE8
                E4888C82BA79B69869CADDDDDDAAA9A9A8A8A7F0F0F0B7B6B6EDEDEDB6B6B5ED
                EDEDB6B6B5EDEDEDB8B7B77F7E7EE8E8E88A8A8A8F8F8F8A8A8AC2C0BFB9B7B4
                EAE9E8B0B1B0E2E1E0ADACABE2E1E0ADACABE2E1E0ADACABE5E3E2B1B0AF7E80
                79E1B0E0CA96C6AE7DCEC1C1C1B8B7B7EAEAEAB2B1B1E2E2E2ADADADE2E2E2AD
                ADADE2E2E2ADADADE4E4E4B1B0B07F7E7EC0C0C0A7A7A7979797A19D9BE7E6E4
                ADACABDCDAD8A8A6A5DCDAD8A8A6A5DCDAD8A8A6A5DCDAD8A8A6A5DDDBD8E2E5
                DFC084D6C089D4FFFFFF9E9E9EE7E7E6ADADADDBDADAA7A7A7DBDADAA7A7A7DB
                DADAA7A7A7DBDADAA7A7A7DCDBDBE5E5E5A09F9FA2A2A2FFFFFFA4A2A0F3F3F2
                D1CFCD9F9E9CD4D1CF9F9F9CD4D1CF9F9F9CD4D1CF9F9F9CD3D1CF9F9E9CE5E4
                E0949487FFFFFFFFFFFFA2A2A2F4F4F4D0D0D09F9F9ED2D2D19F9F9ED2D2D19F
                9F9ED2D2D19F9F9ED2D2D19F9F9EE4E4E4929292FFFFFFFFFFFFAAA6A4FAF9F8
                F7F7F6F8F7F6F8F7F6F8F7F7F8F7F6F8F7F7F8F7F6F8F7F6F7F6F6F7F6F5E9E6
                E39E9388CCCCCCCCCCCCA7A7A7FAFAFAF8F8F8F8F8F8F8F8F8F9F9F8F8F8F8F9
                F9F8F8F8F8F8F8F8F7F7F7F7F7F7E7E7E6929292CCCCCCCCCCCCC7C6C5ADABA8
                ABA9A7ABA9A7ABA9A7ABA9A7ABA9A7ABA9A7ABA9A7ABA9A7A9A7A5D4D2D1F3ED
                E84685B7317BBA3079B8C7C7C7ABABABAAA9A9AAA9A9AAA9A9AAA9A9AAA9A9AA
                A9A9AAA9A9AAA9A9A8A8A7D3D3D3EEEEEE9090908A8A8A888888FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBBB8E7DE
                D82E7ABB71E2FF3177B8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFBCBCBBDFDFDE8A8A8AE5E5E5878787FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9C0
                B83D83BD357BBA3F82BDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C09191918A8A8A909090}
              NumGlyphs = 2
              Margin = 0
            end
            object DeletePayrollBtn: TevBitBtn
              Left = 12
              Top = 139
              Width = 114
              Height = 25
              Caption = 'Delete Payroll'
              TabOrder = 4
              OnClick = DeletePayrollBtnClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCC
                CCCCCCCCE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFE2E2E2CDCCCCCDCCCCCDCCCCE2E2E2FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E16872BC2233AD2232
                AD2233AD6872BCE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFE2E2E2878787616160606060616160878787E2E2E2FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF666FBC283DC13960FA3961
                FE3960FA283EC16772BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFF8685856D6D6D9695959797979695956D6D6D878787DCDCDCCCCCCC
                CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC1826B04265FD3D61FB3A5E
                F93D61FB4366FD1E30ADDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
                CCCCCDCCCC5E5D5D9999999796969493939796969999996060608CBAA36DB08F
                6EB18F6FB18F6EB18F6EB08E6EB18E6FB38E76BE8B131FAEAABBFFFFFFFFFFFF
                FFFFFFFFACBCFF1B2CACB4B4B4A7A7A7A8A8A7A8A8A7A8A8A7A7A7A7A8A8A7AA
                A9A9B2B1B15B5B5BCDCCCCFFFFFFFFFFFFFFFFFFCECDCD5E5D5D6AAD8BCBF4DB
                8CC7B583BEA793CBBB92CAB992CBBA95CDBB9CD7B9101DAF5B77FF5877FF5473
                FE5877FF5D79FF1A29AEA3A3A3EEEEEEC1C1C1B8B7B7C6C6C6C4C4C4C5C5C5C8
                C8C7CFCFCF5B5B5BA2A2A2A2A2A2A1A0A0A2A2A2A4A4A45E5D5D65A986CAF3DC
                5FA27C4E9368B8EBCFB7E9CD98D1B288C5A38ECDA34065A22F3FC96E86FF748C
                FF6E86FF3042CA40689DA09F9FEDEDED989898888888E4E3E3E2E2E2C9C8C8BC
                BCBBC3C2C2757474727171ABABABAFAFAFABABAB74737376767562A581CDF2E0
                73B593B3E7CDB0E4CAB0E4CA6BAC89B3E4C7B5E7C776BA895C7BBB111DAE111E
                AD0F1CAD6880C56DB47F9B9B9BEDEDEDABABABE0E0E0DDDDDDDDDDDDA2A2A2DC
                DCDCDFDFDEAEAEAE8C8C8C5A5A5A5A5A5A5A5A5A939393A7A7A75FA37ED0F2E4
                6CAD8BABE2C851946DABE2C86DAC88B0E2C6B1E3C670B088B2ECC95CA36DB6F1
                C775BA89D7FBE362A77E999999EFEEEEA4A4A4DBDADA8A8A8ADBDADAA2A2A2DB
                DADADCDBDBA6A6A6E3E3E2979696E7E7E6AEAEAEF5F5F59C9C9C5CA07AD8F5E8
                67AA85A7E0C6A5DEC4A5DEC461A17CB2E3C6B2E3C661A27CA6E0C5A7E1C4A9E3
                C668AC85D9F6E95DA07A969595F2F2F1A1A0A0D9D9D9D7D6D6D7D6D6979797DC
                DBDBDCDBDB989898D8D8D8D9D9D9DCDBDBA1A1A1F3F2F2969595599D75DFF7EF
                549A705EA27D9FDBC39FDBC280BFA07EB89478B38F80BFA09FDBC29FDBC35EA2
                7D559B73DFF7EF599D75939393F5F5F58F8F8F989898D4D4D4D4D4D4B7B6B6AF
                AFAFAAA9A9B7B6B6D4D4D4D4D4D4989898919191F5F5F5939393569B72E5F9F4
                448B5E468C615A9E765A9D755A9E765B9F775B9F775A9E765A9D755A9E76468C
                61448B5EE5F9F4569B72919191F8F8F880807F82818194939393939394939395
                959495959494939393939394939382818180807FF8F8F891919154976FE7FAF5
                E3F6F1E3F6F1E3F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E3F5F0E3F6
                F1E3F6F1E7FAF554976F8D8D8DFAF9F9F5F5F5F5F5F5F4F4F4F4F4F4F4F4F4F4
                F4F4F4F4F4F4F4F4F4F4F4F4F4F4F5F5F5F5F5F5FAF9F98D8D8D8CB99E89C6A6
                B0E7CCAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5
                CAB0E7CC89C6A68CB99EB2B2B2BEBDBDE0DFDFDEDEDEDEDEDEDEDEDEDEDEDEDE
                DEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEE0DFDFBEBDBDB2B2B2FFFFFF8AB79B
                5092694F91684F91684F91684F91684F91684F91684F91684F91684F91684F91
                685092698AB79BFFFFFFFFFFFFB0AFAF87878786868686868686868686868686
                8686868686868686868686868686868686878787B0AFAFFFFFFF}
              NumGlyphs = 2
              Margin = 0
            end
            object btnCopyPayroll: TevBitBtn
              Left = 12
              Top = 166
              Width = 114
              Height = 25
              Caption = 'Copy Payroll'
              TabOrder = 5
              OnClick = btnCopyPayrollClick
              Color = clBlack
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
                D2D7D4BAC5C0BDC7C2BEC7C2BDC7C1BDC7C1BDC7C1BDC7C1BDC7C1BDC7C1BDC7
                C2BEC8C2B9C4BFFFFFFFFFFFFFD7D7D7C4C4C4C6C6C6C6C6C6C6C6C6C6C6C6C6
                C6C6C6C6C6C6C6C6C6C6C6C6C6C6C7C7C7C4C3C3D5D5D5FFFFFFFFFFFFFFFFFF
                82B79C8CC6A979B99E79B89C79B89D7ABA9E7CBB9F7CBBA07ABA9E79B89D78B8
                9C76B6998AC6A8FFFFFFFFFFFFB0AFAFBEBEBEB2B1B1B0AFAFB1B0B0B2B2B2B3
                B3B3B3B3B3B2B2B2B1B0B0B0AFAFAEAEAEBEBDBDB2B2B2FFFFFFFFFFFFFFFFFF
                70B190C1ECD464A68681BD9FB2E5CF9FD5BD8EC8AE8DC7AE9BD3BAB1E4CE8CC5
                A95A9E7BB8E6CDFFFFFFFFFFFFA8A8A7E6E6E69D9D9DB5B5B5DFDFDECECECEC1
                C1C1C0C0C0CCCCCCDEDEDEBEBDBD959594E0DFDFB2B1B1FFFFFFFFFFFFFFFFFF
                6BAB89BBE6D179B998A6DCBEB0E3C983BF9EA0D5B7A8DCBE7DBA98B1E4CAA6DB
                BF7FBE9CB3E0CAFFFFFFFFFFFFA1A1A1E0E0E0B0AFAFD4D4D4DCDCDCB6B6B5CD
                CCCCD4D4D4B1B0B0DDDDDDD3D3D3B5B5B5DADADAABABABFFFFFFFFFFFFFFFFFF
                67A985C0E6D47EBD9D94CEB190CBAD79B795A5D9BBB0E2C572B18E98D2B58BC6
                A887C4A5B6DFCCFFFFFFFFFFFFA09F9FE1E1E1B4B4B4C6C6C6C3C2C2AEAEAED1
                D1D1DBDADAA8A8A7CACACABEBDBDBCBCBBDADADAA9A8A8FFFFFFFFFFFFFFFFFF
                65A680C6E7D8599F778CCAADA7E2CA7EBD9D89C2A08AC2A079B998A6E0C893D0
                B45AA27ABBE0CEFFFFFFFFFFFF9C9C9CE4E3E3959594C2C2C1DBDADAB4B4B4B9
                B9B9B9B9B9B0AFAFD9D9D9C8C8C7979797DCDBDBA6A6A6FFFFFFFFFFFFFFFFFF
                63A27DD2ECE254956C67A48177B29274B08F71AC8A6FAB8874AF8E77B1916BA7
                8451936AC7E3D7FFFFFFFFFFFF989898EAEAEA8A8A8A9B9B9BAAA9A9A7A7A7A3
                A3A3A1A1A1A6A6A6A9A8A89E9E9E888888E0E0E0A4A4A4FFFFFFD2D7D4BAC5C0
                629F7BC8EADCD8F7EBD6F5E9D5F4E8D5F4E8D6F5E9D6F5E9D5F4E8D5F4E8D6F4
                E8D9F7EBCFF0E2D7D7D7C4C4C4969595E7E7E6F4F4F4F2F2F1F1F1F1F1F1F1F2
                F2F1F2F2F1F1F1F1F1F1F1F1F1F1F4F4F4ECECECA2A2A2FFFFFF82B79C8CC6A9
                70B1926AAC8771B08B6FAF896FAF8A6FAF8A6FAF8A6FAF8A6FAF8A6FAF8A6FAF
                8A71B08B7CB594B0AFAFBEBEBEA9A8A8A2A2A2A6A6A6A5A5A5A5A5A5A5A5A5A5
                A5A5A5A5A5A5A5A5A5A5A5A5A5A5A6A6A6ACACACDFDFDEFFFFFF70B190C1ECD4
                64A68681BD9FB2E5CF9FD5BD8EC8AE8DC7AE9BD3BAB1E4CE8CC5A95A9E7BB8E6
                CD7BBA9AFFFFFFA8A8A7E6E6E69D9D9DB5B5B5DFDFDECECECEC1C1C1C0C0C0CC
                CCCCDEDEDEBEBDBD959594E0DFDFB2B1B1FFFFFFFFFFFFFFFFFF6BAB89BBE6D1
                79B998A6DCBEB0E3C983BF9EA0D5B7A8DCBE7DBA98B1E4CAA6DBBF7FBE9CB3E0
                CA76B494FFFFFFA1A1A1E0E0E0B0AFAFD4D4D4DCDCDCB6B6B5CDCCCCD4D4D4B1
                B0B0DDDDDDD3D3D3B5B5B5DADADAABABABFFFFFFFFFFFFFFFFFF67A985C0E6D4
                7EBD9D94CEB190CBAD79B795A5D9BBB0E2C572B18E98D2B58BC6A887C4A5B6DF
                CC75B191FFFFFFA09F9FE1E1E1B4B4B4C6C6C6C3C2C2AEAEAED1D1D1DBDADAA8
                A8A7CACACABEBDBDBCBCBBDADADAA9A8A8FFFFFFFFFFFFFFFFFF65A680C6E7D8
                599F778CCAADA7E2CA7EBD9D89C2A08AC2A079B998A6E0C893D0B45AA27ABBE0
                CE74AF8EFFFFFF9C9C9CE4E3E3959594C2C2C1DBDADAB4B4B4B9B9B9B9B9B9B0
                AFAFD9D9D9C8C8C7979797DCDBDBA6A6A6FFFFFFFFFFFFFFFFFF63A27DD2ECE2
                54956C67A48177B29274B08F71AC8A6FAB8874AF8E77B1916BA78451936AC7E3
                D773AD8BFFFFFF989898EAEAEA8A8A8A9B9B9BAAA9A9A7A7A7A3A3A3A1A1A1A6
                A6A6A9A8A89E9E9E888888E0E0E0A4A4A4FFFFFFFFFFFFFFFFFF68A481C8EADC
                D8F7EBD6F5E9D5F4E8D5F4E8D6F5E9D6F5E9D5F4E8D5F4E8D6F4E8D9F7EBCFF0
                E271AB89FFFFFF9B9B9BE7E7E6F4F4F4F2F2F1F1F1F1F1F1F1F2F2F1F2F2F1F1
                F1F1F1F1F1F1F1F1F4F4F4ECECECA2A2A2FFFFFFFFFFFFFFFFFFD4E5DB82B899
                71B08B6FAF896FAF8A6FAF8A6FAF8A6FAF8A6FAF8A6FAF8A6FAF8A71B08B7CB5
                94CCE1D5FFFFFFE3E3E2B0AFAFA6A6A6A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5
                A5A5A5A5A5A5A5A5A6A6A6ACACACDFDFDEFFFFFFFFFFFFFFFFFF}
              NumGlyphs = 2
              Margin = 0
            end
          end
        end
      end
    end
    object tsEDExcl: TTabSheet
      Caption = 'Block E/Ds'
      ImageIndex = 14
      object sbBlockEDs: TScrollBox
        Left = 0
        Top = 0
        Width = 865
        Height = 453
        Align = alClient
        TabOrder = 0
        object fpDetail: TisUIFashionPanel
          Left = 8
          Top = 219
          Width = 332
          Height = 132
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpDetail'
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Scheduled E/D Details'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evLabel1: TevLabel
            Left = 12
            Top = 35
            Width = 128
            Height = 16
            Caption = '~EE Scheduled E/D Code'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object SpeedButton1: TevSpeedButton
            Left = 12
            Top = 82
            Width = 115
            Height = 25
            Caption = 'Create'
            HideHint = True
            AutoSize = False
            OnClick = SpeedButton1Click
            NumGlyphs = 2
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
              CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
              E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
              7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
              82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
              9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
              55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
              5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
              CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
              0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
              76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
              FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
              16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
              8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
              FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
              38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
              9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
              FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
              51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
              9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
              66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
              9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
              76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
              9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
              FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
              59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
              9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
              FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
              0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
              87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
              FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
              55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
              556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
              B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
              88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
              E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
              78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
          object SpeedButton2: TevSpeedButton
            Left = 196
            Top = 82
            Width = 115
            Height = 25
            Caption = 'Delete'
            HideHint = True
            AutoSize = False
            OnClick = SpeedButton2Click
            NumGlyphs = 2
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
              CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
              F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
              6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
              A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
              CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
              A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
              2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
              C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
              9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
              5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
              F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
              9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
              576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
              FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
              97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
              4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
              F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
              8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
              3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
              F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
              334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
              F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
              2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
              EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
              3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
              EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
              84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
              8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
              F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
              8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
              9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
              F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
              8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
              2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
              C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
              807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
              C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
              EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
              CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
              6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Flat = True
            ParentColor = False
            ShortCut = 0
          end
          object wwDBLookupCombo1: TevDBLookupCombo
            Left = 12
            Top = 50
            Width = 176
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ED_Lookup'#9'20'#9'E/D Code'
              'CodeDescription'#9'20'#9'E/D Description')
            DataField = 'CO_E_D_CODES_NBR'
            DataSource = dsEDs
            LookupTable = DM_CO_E_D_CODES.CO_E_D_CODES
            LookupField = 'CO_E_D_CODES_NBR'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object DBRadioGroup1: TevDBRadioGroup
            Left = 196
            Top = 35
            Width = 115
            Height = 36
            Caption = '~Block'
            Columns = 2
            DataField = 'EXCLUDE'
            DataSource = dsEDs
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object fpSummary: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 332
          Height = 203
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evDBGrid1: TevDBGrid
            Left = 12
            Top = 35
            Width = 297
            Height = 144
            TabStop = False
            DisableThemesInTitle = False
            Selected.Strings = (
              'ED_Lookup'#9'30'#9'EE Scheduled E/D Code'#9'No'
              'EXCLUDE'#9'12'#9'Block'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TREDIT_PAYROLL\evDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsEDs
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
    object tsChecks: TTabSheet
      Caption = 'Checks'
      ImageIndex = 17
      object sbChecks: TScrollBox
        Left = 0
        Top = 0
        Width = 865
        Height = 453
        Align = alClient
        TabOrder = 0
        OnConstrainedResize = sbChecksConstrainedResize
        object evPanel5: TevPanel
          Left = 2200
          Top = 0
          Width = 100
          Height = 525
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          Visible = False
        end
        object pnlListOfChecks: TisUIFashionPanel
          Left = 0
          Top = 0
          Width = 806
          Height = 449
          Align = alLeft
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlListOfChecks'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          Margins.Top = 8
          Margins.Left = 8
          Margins.Right = 8
          Margins.Bottom = 8
          object pnlListOfChecksBody: TevPanel
            Left = 20
            Top = 44
            Width = 758
            Height = 377
            Align = alClient
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object wwDBGrid1: TevDBGrid
              Left = 0
              Top = 0
              Width = 758
              Height = 377
              HelpContext = 1502
              DisableThemesInTitle = False
              ControlType.Strings = (
                'CHECK_TYPE;CustomEdit;cbCheckType'
                'CHECK_STATUS;CustomEdit;cbCheckStatus')
              Selected.Strings = (
                'PR_BATCH_NBR'#9'10'#9'Batch'
                'EE_Custom_Number'#9'9'#9'EE Code'
                'Employee_Name_Calculate'#9'30'#9'Employee Name'
                'GROSS_WAGES'#9'10'#9'Gross Wages'
                'NET_WAGES'#9'10'#9'Net Wages'
                'CHECK_TYPE'#9'15'#9'Check Type'
                'PAYMENT_SERIAL_NUMBER'#9'15'#9'Serial Nbr'
                'CHECK_STATUS'#9'15'#9'Check Status')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TREDIT_PAYROLL\wwDBGrid1'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = CheckSrc
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgNoLimitColSize, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnDblClick = wwDBGrid1DblClick
              OnKeyDown = wwDBGrid1KeyDown
              PaintOptions.AlternatingRowColor = 14544093
              PaintOptions.ActiveRecordColor = clBlack
              DefaultSort = '-'
              NoFire = False
            end
            object cbCheckType: TevDBComboBox
              Left = 417
              Top = 99
              Width = 121
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'CHECK_TYPE'
              DataSource = CheckSrc
              DropDownCount = 8
              ItemHeight = 0
              Items.Strings = (
                'Regular'#9'R'
                'Void'#9'V'
                '3rd Party'#9'3'
                'Manual'#9'M'
                'Setup YTD'#9'Y'
                'Setup QTD'#9'Q')
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 1
              UnboundDataType = wwDefault
            end
            object cbCheckStatus: TevDBComboBox
              Left = 536
              Top = 96
              Width = 121
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'CHECK_STATUS'
              DataSource = CheckSrc
              DropDownCount = 8
              ItemHeight = 0
              Items.Strings = (
                'Outstanding'#9'O'
                'OBC Sent'#9'B'
                'Cleared'#9'C'
                'NSF'#9'N'
                'NSF Paid'#9'P'
                'Void'#9'V'
                'Deleted'#9'E'
                'Returned'#9'R'
                'Stop Payment'#9'S'
                'Dead'#9'D')
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 2
              UnboundDataType = wwDefault
            end
          end
        end
      end
    end
    object tsEarnings: TTabSheet
      Caption = 'Earnings and Deductions'
      ImageIndex = 16
      inline EDTotals: TEDTotals
        Left = 0
        Top = 0
        Width = 865
        Height = 453
        Align = alClient
        TabOrder = 0
        inherited ScrollBox3: TScrollBox
          Width = 865
          Height = 453
          inherited evPanel6: TevPanel
            Width = 909
            Height = 577
            inherited pnlEDs: TisUIFashionPanel
              Width = 893
              Height = 561
              Title = 'Summary'
              inherited pnlEDsBody: TevPanel
                Width = 837
                Height = 466
                inherited sbEDTotals: TScrollBox
                  Width = 837
                  Height = 466
                  Color = clBtnFace
                  ParentColor = False
                  inherited Panel6: TevPanel
                    Width = 833
                    inherited cbEDSeparateByLineType: TevCheckBox
                      Left = 12
                      Width = 172
                    end
                    inherited evCheckBox1: TevCheckBox
                      Left = 187
                    end
                  end
                  inherited pnlEDTotalsBorder: TevPanel
                    Width = 833
                    Height = 437
                    BorderWidth = 4
                    inherited Splitter1: TevSplitter
                      Visible = False
                    end
                    inherited evSplitter1: TevSplitter
                      Left = 471
                      Top = 5
                      Height = 427
                    end
                    inherited pnlEDLeft: TevPanel
                      Left = 5
                      Top = 5
                      Width = 466
                      Height = 427
                      BorderWidth = 4
                      inherited fpLeft: TisUIFashionPanel
                        Left = 4
                        Top = 4
                        Width = 458
                        Height = 419
                        Title = 'Earning and Deduction Code'
                        inherited pnlFPLeftBody: TevPanel
                          Top = 36
                          Width = 426
                          Height = 363
                          Align = alClient
                          inherited dgEDTotal: TevDBGrid
                            Width = 426
                            Height = 363
                          end
                        end
                      end
                    end
                    inherited evPanel1: TevPanel
                      Left = 474
                      Top = 5
                      Width = 354
                      Height = 427
                      BorderWidth = 4
                      inherited fpRight: TisUIFashionPanel
                        Left = 4
                        Top = 4
                        Width = 346
                        Height = 419
                        inherited pnlFPRightBody: TevPanel
                          Top = 36
                          Width = 314
                          Height = 363
                          Align = alClient
                          inherited dgEDDetail: TevDBGrid
                            Width = 314
                            Height = 363
                          end
                        end
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
    object tsTax: TTabSheet
      Caption = 'Tax'
      ImageIndex = 11
      object sbTax: TScrollBox
        Left = 0
        Top = 0
        Width = 926
        Height = 563
        Align = alClient
        TabOrder = 0
        object pnlTax: TevPanel
          Left = 0
          Top = 0
          Width = 922
          Height = 559
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          object pnlTaxFashion: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 906
            Height = 543
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlTaxBody: TevPanel
              Left = 12
              Top = 36
              Width = 874
              Height = 487
              Align = alClient
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object Splitter2: TevSplitter
                Left = 360
                Top = 85
                Height = 460
                Align = alNone
                Visible = False
              end
              object sbTaxInnerBody: TScrollBox
                Left = 0
                Top = 0
                Width = 874
                Height = 487
                Align = alClient
                Color = clBtnFace
                ParentColor = False
                TabOrder = 0
                object Panel7: TevPanel
                  Left = 0
                  Top = 0
                  Width = 870
                  Height = 25
                  Align = alTop
                  BevelOuter = bvLowered
                  ParentColor = True
                  TabOrder = 0
                  object cbSeperateOverrides: TevCheckBox
                    Tag = 4
                    Left = 12
                    Top = 4
                    Width = 257
                    Height = 17
                    HelpContext = 1502
                    Caption = 'Display User Entries Separately'
                    TabOrder = 0
                    OnClick = cbSeperateOverridesClick
                  end
                end
                object pnlTaxInnerBodyBorder: TevPanel
                  Left = 0
                  Top = 25
                  Width = 870
                  Height = 458
                  Align = alClient
                  BevelOuter = bvLowered
                  BorderWidth = 6
                  TabOrder = 1
                  object evSplitter1: TevSplitter
                    Left = 402
                    Top = 7
                    Height = 444
                  end
                  object pnlTaxBodyLeft: TevPanel
                    Left = 7
                    Top = 7
                    Width = 395
                    Height = 444
                    Align = alLeft
                    BevelOuter = bvNone
                    BorderWidth = 2
                    TabOrder = 0
                    object fpTaxLeft: TisUIFashionPanel
                      Left = 2
                      Top = 2
                      Width = 391
                      Height = 440
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Caption = 'fpTaxLeft'
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Tax'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      Margins.Right = 3
                      object pnlFPBodyLeft: TevPanel
                        Left = 12
                        Top = 35
                        Width = 285
                        Height = 366
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                        Visible = False
                      end
                      object wwDBGrid4: TevDBGrid
                        Left = 12
                        Top = 36
                        Width = 356
                        Height = 384
                        HelpContext = 1502
                        DisableThemesInTitle = False
                        IniAttributes.Enabled = False
                        IniAttributes.SaveToRegistry = False
                        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                        IniAttributes.SectionName = 'TREDIT_PAYROLL\wwDBGrid4'
                        IniAttributes.Delimiter = ';;'
                        ExportOptions.ExportType = wwgetSYLK
                        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                        TitleColor = clBtnFace
                        FixedCols = 0
                        ShowHorzScrollBar = True
                        Align = alClient
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgNoLimitColSize, dgTrailingEllipsis, dgDblClickColSizing]
                        TabOrder = 1
                        TitleAlignment = taLeftJustify
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        TitleLines = 1
                        UseTFields = True
                        PaintOptions.AlternatingRowColor = 14544093
                        PaintOptions.ActiveRecordColor = clBlack
                        NoFire = False
                      end
                    end
                  end
                  object pnlTaxBodyRight: TevPanel
                    Left = 405
                    Top = 7
                    Width = 458
                    Height = 444
                    Align = alClient
                    BevelOuter = bvNone
                    BorderWidth = 2
                    TabOrder = 1
                    object fpTaxRight: TisUIFashionPanel
                      Left = 2
                      Top = 2
                      Width = 454
                      Height = 440
                      Align = alClient
                      BevelOuter = bvNone
                      BorderWidth = 12
                      Color = 14737632
                      TabOrder = 0
                      RoundRect = True
                      ShadowDepth = 8
                      ShadowSpace = 8
                      ShowShadow = True
                      ShadowColor = clSilver
                      TitleColor = clGrayText
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWhite
                      TitleFont.Height = -13
                      TitleFont.Name = 'Arial'
                      TitleFont.Style = [fsBold]
                      Title = 'Employee'
                      LineWidth = 0
                      LineColor = clWhite
                      Theme = ttCustom
                      Margins.Left = 3
                      object pnlFPBodyRight: TevPanel
                        Left = 12
                        Top = 35
                        Width = 285
                        Height = 366
                        BevelOuter = bvNone
                        ParentColor = True
                        TabOrder = 0
                        Visible = False
                      end
                      object wwDBGrid5: TevDBGrid
                        Left = 15
                        Top = 36
                        Width = 419
                        Height = 384
                        HelpContext = 1502
                        DisableThemesInTitle = False
                        Selected.Strings = (
                          'EE_Custom_Number'#9'12'#9'EE Code'#9'F'
                          'Name'#9'30'#9'Employee Name'#9'F'
                          'Amount'#9'15'#9'Amount'#9'F')
                        IniAttributes.Enabled = False
                        IniAttributes.SaveToRegistry = False
                        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                        IniAttributes.SectionName = 'TREDIT_PAYROLL\wwDBGrid5'
                        IniAttributes.Delimiter = ';;'
                        ExportOptions.ExportType = wwgetSYLK
                        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                        TitleColor = clBtnFace
                        FixedCols = 0
                        ShowHorzScrollBar = True
                        Align = alClient
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgNoLimitColSize, dgTrailingEllipsis, dgDblClickColSizing]
                        TabOrder = 1
                        TitleAlignment = taLeftJustify
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        TitleLines = 1
                        OnDblClick = wwDBGrid5DblClick
                        OnKeyDown = wwDBGrid5KeyDown
                        PaintOptions.AlternatingRowColor = 14544093
                        PaintOptions.ActiveRecordColor = clBlack
                        DefaultSort = '-'
                        NoFire = False
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
    object tsTimeOff: TTabSheet
      Caption = 'Time Off'
      ImageIndex = 18
      object sbTimeOff: TScrollBox
        Left = 0
        Top = 0
        Width = 865
        Height = 453
        Align = alClient
        TabOrder = 0
        object evPanel7: TevPanel
          Left = 0
          Top = 0
          Width = 861
          Height = 449
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          object pnlTimeOff: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 322
            Height = 433
            Align = alLeft
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'pnlListOfChecks'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlTimeOffBody: TevPanel
              Left = 12
              Top = 35
              Width = 287
              Height = 376
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 1
              object LabelHint: TevLabel
                Left = 0
                Top = 363
                Width = 287
                Height = 13
                Align = alBottom
                Alignment = taCenter
                AutoSize = False
                Caption = 'Double-click on employee to go to employee'#39's check'
                Color = clInfoBk
                ParentColor = False
                Visible = False
              end
              object evPanel2: TevPanel
                Left = 0
                Top = 0
                Width = 287
                Height = 41
                Align = alTop
                BevelInner = bvLowered
                BevelOuter = bvLowered
                ParentColor = True
                TabOrder = 0
                object bRunToaBalanceCheck: TevBitBtn
                  Left = 9
                  Top = 8
                  Width = 263
                  Height = 25
                  Caption = 'Run Balance Check'
                  TabOrder = 0
                  TabStop = False
                  OnClick = bRunToaBalanceCheckClick
                  Color = clBlack
                  NumGlyphs = 2
                  Margin = 0
                end
              end
              object gToaBalanceCheck: TevDBGrid
                Left = 0
                Top = 41
                Width = 287
                Height = 322
                Hint = 'Double-click on employee to go to employee'#39's check'
                DisableThemesInTitle = False
                Selected.Strings = (
                  'EE_CUSTOM_NUMBER'#9'22'#9'EE Code'#9'F'
                  'CHECK_NUMBER'#9'17'#9'Serial Number'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SaveToRegistry = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TREDIT_PAYROLL\gToaBalanceCheck'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsToaBalanceCheckResults
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgNoLimitColSize, dgTrailingEllipsis, dgDblClickColSizing]
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                Visible = False
                OnDblClick = gToaBalanceCheckDblClick
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
            object edSneakyField: TevEdit
              Left = 360
              Top = 96
              Width = 121
              Height = 21
              TabOrder = 0
              Text = 'edSneakyField'
            end
          end
        end
      end
    end
    object tsContacts: TTabSheet
      Caption = 'Company Contacts'
      ImageIndex = 21
      object ScrollBox3: TScrollBox
        Left = 0
        Top = 0
        Width = 865
        Height = 453
        Align = alClient
        TabOrder = 0
        object evPanel6: TevPanel
          Left = 0
          Top = 0
          Width = 861
          Height = 449
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          object pnlContacts: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 770
            Height = 410
            BevelOuter = bvNone
            BorderWidth = 12
            Caption = 'pnlListOfChecks'
            Color = 14737632
            TabOrder = 0
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Summary'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlContactsBody: TevPanel
              Left = 12
              Top = 35
              Width = 737
              Height = 351
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object wwDBGrid2x: TevDBGrid
                Left = 0
                Top = 0
                Width = 737
                Height = 351
                DisableThemesInTitle = False
                Selected.Strings = (
                  'DESCRIPTION'#9'20'#9'Description'#9'F'
                  'PHONE_NUMBER'#9'20'#9'Phone Number'#9'F'
                  'PHONE_EXT'#9'10'#9'Phone Extension'#9'F'
                  'CONTACT'#9'30'#9'Contact'#9'F'
                  'PHONE_TYPE'#9'1'#9'Phone Type'#9'F'
                  'EMAIL_ADDRESS'#9'19'#9'Email address'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TREDIT_PAYROLL\wwDBGrid2x'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsCoPhone
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
  end
  inherited Panel1: TevPanel
    Top = 536
    Width = 873
    Height = 43
    TabOrder = 2
    inherited BitBtn4: TevSpeedButton
      Left = 14
      Width = 121
      Caption = 'Payroll'
      Visible = False
      OnClick = BitBtn4Click
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDCDCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        CCCCCCCCCCCCCCDCDCDCDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
        CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCDDDDDD8CBAA36DB08F
        6EB18F6FB18F6EB18F6EB08E6EB18E6EB18E6EB18E6EB18E6EB08E6EB18F6FB1
        906EB18F6DB08F8CBAA3B4B4B4A7A7A7A8A8A7A8A8A7A8A8A7A7A7A7A8A8A7A8
        A8A7A8A8A7A8A8A7A7A7A7A8A8A7A8A8A7A8A8A7A7A7A7B4B4B46AAD8BCBF4DB
        8CC7B583BEA793CBBB92CAB992CBBA93CBBB93CBBB92CBBA92CAB993CBBB7DBB
        A085C1ACCBF4DC6AAE8BA3A3A3EEEEEEC1C1C1B8B7B7C6C6C6C4C4C4C5C5C5C6
        C6C6C6C6C6C5C5C5C4C4C4C6C6C6B3B3B3BABABAEEEEEEA4A4A465A986CAF3DC
        5FA27C4E9368B8EBCFB7E9CD98D1B287C4A387C4A398D1B2B7E9CDB8EBCF4F92
        69599F77CAF3DD65A986A09F9FEDEDED989898888888E4E3E3E2E2E2C9C8C8BB
        BBBBBBBBBBC9C8C8E2E2E2E4E3E3878787959594EDEDEDA09F9F62A581CDF2E0
        73B593B3E7CDB0E4CAB0E4CA6BAC89B2E3C7B2E3C76BAC89B0E4CAB0E4CAB3E7
        CD74B593CDF2E062A5819B9B9BEDEDEDABABABE0E0E0DDDDDDDDDDDDA2A2A2DC
        DBDBDCDBDBA2A2A2DDDDDDDDDDDDE0E0E0ACACACEDEDED9B9B9B5FA37ED0F2E4
        6CAD8BABE2C851946DABE2C86DAC88B0E2C6B0E2C66DAC88ABE2C851946DABE2
        C86CAD8BD0F2E45FA37E999999EFEEEEA4A4A4DBDADA8A8A8ADBDADAA2A2A2DB
        DADADBDADAA2A2A2DBDADA8A8A8ADBDADAA4A4A4EFEEEE9999995CA07AD8F5E8
        67AA85A7E0C6A5DEC4A5DEC461A17CB2E3C6B2E3C661A27CA5DEC4A5DEC4A7E0
        C667AA85D8F5E85CA07A969595F2F2F1A1A0A0D9D9D9D7D6D6D7D6D6979797DC
        DBDBDCDBDB989898D7D6D6D7D6D6D9D9D9A1A0A0F2F2F1969595599D75DFF7EF
        549A705EA27D9FDBC39FDBC280BFA07EB89478B38F80BFA09FDBC29FDBC35EA2
        7D559B73DFF7EF599D75939393F5F5F58F8F8F989898D4D4D4D4D4D4B7B6B6AF
        AFAFAAA9A9B7B6B6D4D4D4D4D4D4989898919191F5F5F5939393569B72E5F9F4
        448B5E468C615A9E765A9D755A9E765B9F775B9F775A9E765A9D755A9E76468C
        61448B5EE5F9F4569B72919191F8F8F880807F82818194939393939394939395
        959495959494939393939394939382818180807FF8F8F891919154976FE7FAF5
        E3F6F1E3F6F1E3F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E3F5F0E3F6
        F1E3F6F1E7FAF554976F8D8D8DFAF9F9F5F5F5F5F5F5F4F4F4F4F4F4F4F4F4F4
        F4F4F4F4F4F4F4F4F4F4F4F4F4F4F5F5F5F5F5F5FAF9F98D8D8D8CB99E89C6A6
        B0E7CCAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5
        CAB0E7CC89C6A68CB99EB2B2B2BEBDBDE0DFDFDEDEDEDEDEDEDEDEDEDEDEDEDE
        DEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEE0DFDFBEBDBDB2B2B2FFFFFF8AB79B
        5092694F91684F91684F91684F91684F91684F91684F91684F91684F91684F91
        685092698AB79BFFFFFFFFFFFFB0AFAF87878786868686868686868686868686
        8686868686868686868686868686868686878787B0AFAFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    inherited BitBtn3: TevSpeedButton
      Left = 272
      Width = 121
      Caption = 'Batch'
      OnClick = BitBtn3Click
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCDDDD
        DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFCDCCCCCDCCCCDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008A4A00C684429E
        72DEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF7C7B7BB5B5B5929292E0DFDFFFFFFFFFFFFFFFFFFFFEFEFE
        ECECECCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00884700E4A600BE
        8046A075DEDEDEFFFFFFFFFFFFFFFFFFEDEDEDCECECECDCCCCCDCCCCCDCCCCCD
        CCCCCDCCCCCDCCCC7A7979D3D2D2AEAEAE959594E0DFDFFFFFFFFFFFFFF2F2F2
        B5B5B589888993878C308A6100894600884700884700874600834200D9A100D8
        A000BC8046A075DDDDDDFFFFFFF3F3F3B6B6B58989898989897F7E7E7A79797A
        79797A7979797979757474C9C8C8C8C8C7ACACAC959594DEDEDEFFFFFFD5D5D5
        9B9C9CF4F0F0FFF2F800853B3FE8C000D7A000D7A100D7A100D59F00D09C00D0
        9C00D39F00B98142A074FFFFFFD6D6D69D9D9DF2F2F1F6F6F5767675DBDADAC7
        C7C7C8C8C7C8C8C7C5C5C5C1C1C1C1C1C1C4C3C3AAA9A9959594FFFFFFBDBDBD
        AEADADF0EDEC9A888D0083396CE8CE00C89900C89900C89A00C89900C79700C8
        9800CA9A63E6CD008A47FFFFFFBEBDBDAEAEAEEFEEEE8B8B8B747373DFDFDEBA
        BABABABABABABABABABABAB8B8B8BABABABCBCBBDCDCDC7B7B7BFFFFFFA9A9A9
        BBB8B7DAD6D6E1D1D500843992EEE04BE8D34FE8D34FE8D34DE6D193E8D800C3
        9760E0C600B28153B184FFFFFFAAA9A9B9B9B9D7D7D7D4D4D4757474E8E8E8DF
        DFDEDFDFDEDFDFDEDDDDDDE2E2E2B6B6B5D7D6D6A4A4A4A5A5A5FFFFFF969696
        C5C3C2CECAC99C91923EA07300853900843A00843A00843C00813981E4D557DA
        C300AD7D5BB387FFFFFFFFFFFF979696C4C4C4CBCBCB93939394939376767575
        7474757474757474727171DDDDDDD1D1D1A09F9FA7A7A7FFFFFFFFFFFF888888
        CAC8C6959391CCC7C6A09697D8C8CCA7969ADCC9CEB3989F00863C73E2D400A9
        7934875EFFFFFFFFFFFFFFFFFF888888C9C8C8949393C9C8C8979797CBCBCB98
        9898CDCCCC9C9C9C777676DCDBDB9B9B9B7D7C7CFFFFFFFFFFFFD1D1D1858585
        A8A5A4A6A4A3A7A3A2A8A4A2A9A3A2A9A4A3ABA4A4B6A6A900843700A5783290
        6297858DD1D1D1FFFFFFD3D2D2868585A6A6A6A5A5A5A4A4A4A4A4A4A4A4A4A5
        A5A5A5A5A5A9A8A8757474989898858484888888D3D2D2FFFFFF8D8D8EE9E8E9
        D9DCDFD9DCE2D9DBDDDAD9D9DAD9D7DAD9D7DAD9D7DFDADBE6DCE2E8DDE7E4DC
        E3EEE9EC918E8FFFFFFF8E8E8EEAEAEADEDEDEDFDFDEDCDCDCDADADADADADADA
        DADADADADADCDBDBE0DFDFE1E1E1E0DFDFECEBEB8F8F8FFFFFFF858687E7E9ED
        D4AA66C66E00C1C7D3C1C2C5C2C0BFC2C0BEC2C0BFC2C2C6C3C7D3C86E00D6AA
        67E8E9ED858687FFFFFF868686ECEBEBA2A2A26B6B6BCACACAC4C3C3C1C1C1C1
        C1C1C1C1C1C4C3C3CACACA6B6B6BA2A2A2ECEBEB868686FFFFFFAEAEAFE6E8EA
        FFEDABDD9C33C57000DFE7F3E2E4E7E3E3E3E2E4E7DFE7F3C57000DD9C33FFED
        ABE6E8EA919192FFFFFFAFAFAFEAEAEAE3E3E29393936C6C6CEBEBEAE6E6E6E4
        E4E4E6E6E6EBEBEA6C6C6C939393E3E3E2EAEAEA929292FFFFFFFFFFFFADAEB0
        7C828CFFEDB2DA9B35C772007C859580858F7C8595C77200DA9B35FFEDB27C82
        8CADAFB0FFFFFFFFFFFFFFFFFFAFAFAF858484E4E4E49292926E6E6E89898987
        87878989896E6E6E929292E4E4E4858484B0AFAFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFE8B1DB9C37CB7600FFFFFFCB7600DB9C37FFE8B1FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0E0E0939393727171FF
        FFFF727171939393E0E0E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFE9B3DB9E3BFFFFFFDB9E3BFFE9B3FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1959594FF
        FFFF959594E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Layout = blGlyphLeft
    end
    object BitBtn1: TevSpeedButton [2]
      Left = 143
      Top = 8
      Width = 121
      Height = 25
      Caption = 'Ne&w Payroll Expert'
      Enabled = False
      HideHint = True
      AutoSize = False
      OnClick = BitBtn1Click
      NumGlyphs = 2
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        F0F0F0DEDEDED4D4D4CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCD4D4D4DEDE
        DEF0F0F0FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1E0DFDFD5D5D5CDCCCCCDCCCCCD
        CCCCCDCCCCCDCCCCCDCCCCD5D5D5E0DFDFF1F1F1FFFFFFFFFFFFF8F8F8CFCFCF
        BAAAA8A4817C9666608C554E8C554D8C554D8C554D8C554D8C554E966660A481
        7CBAAAA8CFCFCFF4F4F4FAF9F9D0D0D0ABABAB82828268686858585858585858
        5858585858585858585858686868828282ABABABD0D0D0F6F6F5C3BBBA915E57
        97645DAB857FBA9B98C7ACA9C9A8A4C6ACA9C5ADABC6ACA9C9A8A5BB9996AB85
        7F97645D915E57BFB3B1BCBCBB6161606767678787879D9D9DAEAEAEAAAAAAAE
        AEAEAFAFAFAEAEAEAAAAAA9B9B9B878787676767616160B4B4B48E5750A97E78
        BB908BB67368AE5444AB321D30E6E6AD341FA93B26AE362032E8E8B35140B674
        68BC908BA97E788E57505A5A5A8180809393937776765A5A5A3F3F3FE1E1E141
        4141454545424241E4E3E35858587878779393938180805A5A5A8E5750B0827D
        32E5E6B26155D39590E4A9AADF9B9ED18F92C48083B96C6FB65F5FC07671AF5D
        5233E6E6B0827D8E57505A5A5A858484E0E0E0666666999999AFAFAFA1A1A196
        95958686867473736767677C7B7B636363E1E1E18584845A5A5A8E58519A6863
        C96A56FFD6D8F2C0C1E3A39DCB8379B15D4BA1412BA3493BAF5D55D09293F5C1
        C4C669569A68638E58515B5B5B6B6B6B6F6F6FDBDADAC5C5C5A8A8A787878762
        6262484848505050636363979797C6C6C66E6E6E6B6B6B5B5B5BE7DBD995625C
        905A52C59593E0AC9F2DE0E1BF6751A94B3237F1F4912105BB6654BF7567C08F
        8D905B5395625CE7DBD9DCDCDC6565655C5C5C989898AEAEAEDCDBDB6B6B6B50
        5050EDEDED2F2F2F6A6A6A7979799393935E5D5D656565DCDCDCFFFFFFFFFFFF
        DECECCB6948FB9837AEBB7ACBF6650A84D34942F1296280C35ECEFAB6054B695
        91DECECCFFFFFFFFFFFFFFFFFFFFFFFFD0D0D0969595868585B9B9B96A6A6A53
        5252383738343434E8E7E7646464979797D0D0D0FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFAE8781E4ADA231E6E9AA482F922C109C3C23C47769AD827CFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF898989B0AFAFE2E2E24F
        4F4F3535354343437C7B7B858484FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE7DBD9C18C81C97E6AA6432A38F1F4A4462FAC6A5EE7DBDAFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDCDC8E8E8E8180804A
        4A4AEDEDED4D4C4C6D6D6DDDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFA37770CA8876A5432A91280BA9533EA0706AFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7A79798A8A8A4A
        4A4A333333585858737373FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFD6C3C1C17B6B36EDF1922D11A45948D7C4C2FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C5C57E7E7DE9
        E9E93737375C5C5CC6C6C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFEFEA56D63A8482E93290C9C5F54FFFEFEFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6F4E
        4E4E343434626262FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFBB9B99A3483039EFF1BD9692FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E9E9E4E
        4E4EEBEBEA989898FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFF8F4F4934B3D914333F8F4F4FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F54F
        4F4F484848F6F6F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA37872A37872FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B
        7B7B7B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentColor = False
      ShortCut = 0
    end
    object QuickPayrollBtn: TevSpeedButton [3]
      Left = 399
      Top = 8
      Width = 121
      Height = 25
      Caption = '&Quick Payroll'
      HideHint = True
      AutoSize = False
      OnClick = QuickPayrollBtnClick
      NumGlyphs = 2
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        CCCCCCCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCDDDDDDFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCCCCD0D0D0FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFCDCCCCCDCCCCDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        329EDE3CA2DDD2D2D2FFFFFFFFFFFFFFFFFFFFFFFF008A4A00C684429E72DEDE
        DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFACACACAFAFAFD3D3D3FFFFFFFFFFFFFF
        FFFFFFFFFF7C7B7BB5B5B5929292E0DFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        61B2E1329DDE43A2E0CCCCCCCCCCCCCCCCCCCCCCCC00884700E4A600BE8046A0
        75DEDEDEFFFFFFFFFFFFFFFFFFFFFFFFBCBCBBABABABB0AFAFCDCCCCCDCCCCCD
        CCCCCDCCCC7A7979D3D2D2AEAEAE959594E0DFDFFFFFFFFFFFFFFFFFFFFFFFFF
        B7DDF4329CDE6CC9F70E907C00884000884600874600834200D9A100D8A000BC
        8046A075DDDDDDFFFFFFFFFFFFFFFFFFE2E2E2ABABABD2D2D188888879797979
        7979797979757474C9C8C8C8C8C7ACACAC959594DEDEDEFFFFFFFFFFFFFFFFFF
        FFFFFF339BDD8DDCFF00843440E7BE00D7A000D7A000D59F00D09C00D09C00D3
        9F00B98142A074FFFFFFFFFFFFFFFFFFFFFFFFAAAAAAE2E2E2747373DADADAC7
        C7C7C7C7C7C5C5C5C1C1C1C1C1C1C4C3C3AAA9A9959594FFFFFFFFFFFFFFFFFF
        FFFFFF6AB4E465C5FA0083346DE7CD00C89900C89900C89900C79700C89800CA
        9A63E6CD008A47FFFFFFFFFFFFFFFFFFFFFFFFBEBDBDD0D0D0737373DEDEDEBA
        BABABABABABABABAB8B8B8BABABABCBCBBDCDCDC7B7B7BFFFFFFFFFFFFCCCCCC
        CCCCCCA3BFD150B0F000833295EDDE4CE7D24FE7D24DE6D193E8D700C39760E0
        C700B28153B184FFFFFFFFFFFFCDCCCCCDCCCCC3C2C2BEBEBE737373E7E7E6DE
        DEDEDEDEDEDDDDDDE1E1E1B6B6B5D7D6D6A4A4A4A5A5A5FFFFFFFFFFFF389BDC
        48ACE446AAE448AAEA3DB082007F30007F32008035007F3681E4D559DAC300AD
        7D58B285FFFFFFFFFFFFFFFFFFAAA9A9B8B7B7B7B6B6B8B8B8A3A3A36F6F6F6F
        6F6F7170706F6F6FDDDDDDD1D1D1A09F9FA6A6A6FFFFFFFFFFFFFFFFFF3799DB
        9EF1FD81E5FA77E2FC78E3FF78E3FF7AE5FF81E9FF00823677E2D200A97756A9
        79FFFFFFFFFFFFFFFFFFFFFFFFA8A8A7F1F1F1E7E7E6E5E5E5E7E7E6E7E7E6E8
        E7E7EBEBEA737373DBDADA9B9B9B9D9D9DFFFFFFFFFFFFFFFFFFFFFFFF5AA7DE
        85DCF589E5F869DAF769DBF8BBF2FFBCF4FFBFF9FF00842E00A87215947977B0
        E3FFFFFFFFFFFFFFFFFFFFFFFFB3B3B3E0DFDFE7E7E6DEDEDEDFDFDEF3F3F3F5
        F5F5F9F9F87473739999998B8B8BBBBBBBFFFFFFFFFFFFFFFFFFFFFFFF98C7E8
        6DC5EC9CE8FA5FD6F461D7F446B4E63292D93998DE429CE5459DE6449EE5429E
        E1FFFFFFFFFFFFFFFFFFFFFFFFCECDCDCCCCCCEAEAEADADADADBDADABEBDBDA3
        A3A3A9A8A8ADADADAFAFAFAFAFAFADADADFFFFFFFFFFFFFFFFFFFFFFFFDAECF9
        4CAEE4B0EEFB66D8F457D3F3ACEDFB63B5E58FB6D3FEFEFEFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF0EFEFB9B9B9F0EFEFDCDBDBD7D7D7EFEEEEBE
        BEBEBCBCBBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        3B9ADAACEEFB82DDF54CCEF182DFF6ABE6F83996D9D4D5D7FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9A8A8F0EFEFE0DFDFD3D2D2E2E2E2E8
        E8E8A6A6A6D7D6D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        5EA7DD8BDAF3ACEAF93FCBF045CDF0BBF0FB6DBAE758A3D7F1F1F1FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B3DDDDDDECEBEBD0D0D0D2D2D1F1
        F1F1C3C2C2AEAEAEF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        99C6E76CC5EBD7FAFFCDF6FDC3F3FDD2F8FFC6F1FB409ADAA5BED0FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCCCCCCCCCCFAFAFAF7F6F6F4F4F4F9
        F9F8F3F2F2A9A8A8C2C2C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        D5E9F8439ADA3F97D93E96D93E96D93E97D94099DA459CDB489EDCFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECECECA9A8A8A6A6A6A6A6A6A6A6A6A6
        A6A6A8A8A7AAAAAAACACACFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentColor = False
      ShortCut = 0
    end
    inherited cbCalcCL: TevCheckBox
      Left = 14
      Top = 44
      Width = 127
    end
    inherited evPanel4: TevPanel
      Left = 399
      Top = 50
      Width = 125
      Height = 39
      inherited sbWizard: TevSpeedButton
        Left = 1
        Width = 121
        Caption = '&Wizard'
        OnClick = sbWizardClick
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
          F0F0F0DEDEDED4D4D4CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCD4D4D4DEDE
          DEF0F0F0FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1E0DFDFD5D5D5CDCCCCCDCCCCCD
          CCCCCDCCCCCDCCCCCDCCCCD5D5D5E0DFDFF1F1F1FFFFFFFFFFFFF8F8F8CFCFCF
          BAAAA8A4817C9666608C554E8C554D8C554D8C554D8C554D8C554E966660A481
          7CBAAAA8CFCFCFF4F4F4FAF9F9D0D0D0ABABAB82828268686858585858585858
          5858585858585858585858686868828282ABABABD0D0D0F6F6F5C3BBBA915E57
          97645DAB857FBA9B98C7ACA9C9A8A4C6ACA9C5ADABC6ACA9C9A8A5BB9996AB85
          7F97645D915E57BFB3B1BCBCBB6161606767678787879D9D9DAEAEAEAAAAAAAE
          AEAEAFAFAFAEAEAEAAAAAA9B9B9B878787676767616160B4B4B48E5750A97E78
          BB908BB67368AE5444AB321D30E6E6AD341FA93B26AE362032E8E8B35140B674
          68BC908BA97E788E57505A5A5A8180809393937776765A5A5A3F3F3FE1E1E141
          4141454545424241E4E3E35858587878779393938180805A5A5A8E5750B0827D
          32E5E6B26155D39590E4A9AADF9B9ED18F92C48083B96C6FB65F5FC07671AF5D
          5233E6E6B0827D8E57505A5A5A858484E0E0E0666666999999AFAFAFA1A1A196
          95958686867473736767677C7B7B636363E1E1E18584845A5A5A8E58519A6863
          C96A56FFD6D8F2C0C1E3A39DCB8379B15D4BA1412BA3493BAF5D55D09293F5C1
          C4C669569A68638E58515B5B5B6B6B6B6F6F6FDBDADAC5C5C5A8A8A787878762
          6262484848505050636363979797C6C6C66E6E6E6B6B6B5B5B5BE7DBD995625C
          905A52C59593E0AC9F2DE0E1BF6751A94B3237F1F4912105BB6654BF7567C08F
          8D905B5395625CE7DBD9DCDCDC6565655C5C5C989898AEAEAEDCDBDB6B6B6B50
          5050EDEDED2F2F2F6A6A6A7979799393935E5D5D656565DCDCDCFFFFFFFFFFFF
          DECECCB6948FB9837AEBB7ACBF6650A84D34942F1296280C35ECEFAB6054B695
          91DECECCFFFFFFFFFFFFFFFFFFFFFFFFD0D0D0969595868585B9B9B96A6A6A53
          5252383738343434E8E7E7646464979797D0D0D0FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFAE8781E4ADA231E6E9AA482F922C109C3C23C47769AD827CFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF898989B0AFAFE2E2E24F
          4F4F3535354343437C7B7B858484FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE7DBD9C18C81C97E6AA6432A38F1F4A4462FAC6A5EE7DBDAFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDCDC8E8E8E8180804A
          4A4AEDEDED4D4C4C6D6D6DDDDDDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFA37770CA8876A5432A91280BA9533EA0706AFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7A79798A8A8A4A
          4A4A333333585858737373FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFD6C3C1C17B6B36EDF1922D11A45948D7C4C2FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C5C57E7E7DE9
          E9E93737375C5C5CC6C6C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFEFEA56D63A8482E93290C9C5F54FFFEFEFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6F4E
          4E4E343434626262FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFBB9B99A3483039EFF1BD9692FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E9E9E4E
          4E4EEBEBEA989898FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFF8F4F4934B3D914333F8F4F4FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F54F
          4F4F484848F6F6F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA37872A37872FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7B
          7B7B7B7B7BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_PR.PR
    OnStateChange = wwdsMasterStateChange
    OnDataChange = wwdsMasterPayrollDataChange
    Top = 2
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
    Top = 2
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    OnDataChange = wwdsListDataChange
    Top = 2
  end
  inherited ActionList: TevActionList
    Top = 2
  end
  object CoNotesDS: TevDataSource
    DataSet = DM_CO.CO
    Left = 290
    Top = 1
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 438
    Top = 18
  end
  object DM_PAYROLL: TDM_PAYROLL
    Left = 483
    Top = 18
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 522
    Top = 13
  end
  object CheckSrc: TevDataSource
    DataSet = DM_PR_CHECK.PR_CHECK
    OnDataChange = CheckSrcDataChange
    MasterDataSource = wwdsMaster
    Left = 317
    Top = 7
  end
  object dsEDs: TevDataSource
    DataSet = DM_PR_SCHEDULED_E_DS.PR_SCHEDULED_E_DS
    OnDataChange = dsEDsDataChange
    MasterDataSource = wwdsMaster
    Left = 343
    Top = 7
  end
  object CalcPopup: TevPopupMenu
    Alignment = paCenter
    AutoHotkeys = maManual
    OnPopup = CalcPopupPopup
    Left = 499
    Top = 411
    object Test1: TMenuItem
      Caption = 'Test'
    end
  end
  object CalcClone: TevClientDataSet
    IndexFieldNames = 'SCHEDULED_CHECK_DATE'
    Filter = 'PR_NBR is null'
    Filtered = True
    Left = 503
    Top = 441
  end
  object cdToaBalanceCheckResults: TevClientDataSet
    IndexName = 'SORT'
    IndexDefs = <
      item
        Name = 'SORT'
        Fields = 'EE_CUSTOM_NUMBER;CHECK_NUMBER'
      end>
    Left = 576
    Top = 16
    object cdToaBalanceCheckResultsPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object cdToaBalanceCheckResultsEE_CUSTOM_NUMBER: TStringField
      FieldName = 'EE_CUSTOM_NUMBER'
    end
    object cdToaBalanceCheckResultsCHECK_NUMBER: TIntegerField
      FieldName = 'CHECK_NUMBER'
    end
    object cdToaBalanceCheckResultsMESSAGE: TStringField
      FieldName = 'MESSAGE'
      Size = 1024
    end
  end
  object dsToaBalanceCheckResults: TevDataSource
    DataSet = cdToaBalanceCheckResults
    Left = 608
    Top = 16
  end
  object wwdsCoPhone: TevDataSource
    DataSet = DM_CO_PHONE.CO_PHONE
    Left = 502
    Top = 98
  end
  object dsCL: TevDataSource
    DataSet = DM_CL.CL
    Left = 375
    Top = 7
  end
  object PageControlImages: TevImageList
    Left = 280
    Top = 140
    Bitmap = {
      494C010116001800040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000006000000001002000000000000060
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      00000000000000000000DDDDDD00CCCCCC00CCCCCC00DDDDDD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000002C5172002B4F71002A4F70002B50
      71002C5071002C5071002C5071002C5071002C5071002C5071002B5071002B4F
      7000294E70002A4F70002C517200FFFFFF00DCDCDC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00C39A6700BF7E2F00BF7E2F00C39A6700CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00DCDCDC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006E869B00758CA30056738F004263
      820043648200436584004464840044658400446484004565840043638200506F
      8B00728AA200758CA3006E869B00FFFFFF00C2976300BF7D2E00C07D2B00C17C
      2A00C17D2B00C17C2A00C37B25007BA0C5007BA0C500C37B2500C07C2A00C07C
      2A00C07C2A00C07C2B00BF7D2E00C29763000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007F91A000355878007D92A8005270
      8C004365860045678800456589004466890046688B0047688A00456787004263
      820054728E002F5375007F91A000FFFFFF00BE7D2E00729DC4007499BA007498
      B8007699B9007699B9007498B6008DA2B6008EA2B6007498B6007197B7006E95
      B7006D94B6007096B800709BC300BE7D2D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000002C50710040617F00426483004668
      8900486B8E005479990071A1B000A9F0DD00597F9E00486A90004A6D9000486A
      8B0044668600406181002C507100FFFFFF00BE7A2900A0C4E400C4E8FE00B1D8
      F1009BC7E3008FB9D500A8C9DB00D3E5EC0095BBD400A9C8DD00C0D7E600FDFF
      FF0000000000F8FFFF00A6C8E600BD7A29000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000002B4F70004B6C8A00486A8B004C6F
      93004E709500A9EBDC0099D3D00097D0CE00AAEADD0095CECD004E7096004D71
      9400496C8E0052728F002A4F7000FFFFFF00BD7A28009EC2DF00C2E4FA00AED5
      ED009CC8E30095C3DD00BADBEC00E4F7FB0092BFDC00B7D6E800D7E9F400F7FC
      FF0000000000F3FFFF00A1C3DF00BC7927000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000294D6F006E89A4004A6E92005174
      980056779B005B7A9F00789DB2008CB9C300AEE9DE00ACE7DD0057779C005376
      99004B7093007690AA00294D6F00FFFFFF00BD792800A1C3E100B2C1CB009CAE
      B900899DAA0095C4DF00B9DBEC00E3F6FA0091BEDB00B7D6E900D8EBF600BFC0
      C100CBCACA00C8CED200A4C6E100BC7827000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000002C507100859DB2006082A3005879
      9C005E7CA000B0E9DE00B3EBE1009BC8CE0087ADBE006D8BAB00617FA2005A7B
      9E006383A4007690A9004F6C8800FFFFFF00BD792800A4C6E300BEDCF000AACD
      E30097BFD9008BA0AD00A9B7BD00E3F7FB0092C0DD0095A4AC00ACB3B900EDF2
      F700F7FAFD00EAF6FF00A8C9E400BC7827000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7EBEF0033577800A0B3C7006F8C
      AB006481A500A5D6D600BAEDE400ACD9D900B4E2DE009ECCD0006784A700728E
      AD00A2B5C80034587900E5EAEE00FFFFFF00BD792800A6C8E400B2C1CC009CAF
      BB008A9FAC0093BED600B5D3E300E3F7FC0092C0DE00B2CDE000CFE3F100AFB2
      B800B9BBC200BABFC800ABCBE900BB7828000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00B4C1CD0034577800ABBD
      CE007C95B500738BAF008DA8C100B5DCDE0099B7C900768DB0007E96B500ADBD
      D00034577800B3C1CC00FFFFFF00FFFFFF00BD792700AACBE500BDDCF000AACD
      E30097BFDA008BA1AE00A9B8BF00E3F7FD0092C1E1008895A1009AA7B400EDBD
      6000F6C56700FFD16C00ADCEEF00BB7828000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00B5C2CD003558
      7900AEBED1007E96B6008397B9007EADC8007183A50097D5ED00B3C3D7003A5C
      7E00B5C2CD00FFFFFF00FFFFFF00FFFFFF00BC792700ADCCE600B1C1CB009CAF
      BB008A9FAC0093BED600B5D3E300E3F7FD0090C1E500DDAA4D00E5B45B00EBC1
      7300F7DAA100EDCB8B00AFD0F000BB7828000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00A5B5
      C2005E7B9800C2CEDE00BBC5D700A6E8FA0080ADC600A9DFF20058749100A4B3
      C200FFFFFF00FFFFFF00FFFFFF00FFFFFF00BC782700B0CEE800BDDCF000AACD
      E30097BFDA008BA1AE00A9B8BF00E3F7FD0091C1E500C5A36700E3C68F00F2D7
      A400FBE1AE00E7C99200B3D3F000BB7828000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF003B5D7B00426485003B4F70007CB3CB009CDFF4005984A2006F869C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BC782700B2D1E900B0C0CB009CAF
      BA00899EAC0093BED600B5D3E300E3F7FD0092C1E300C1A16A0038E8FF00BCB2
      A800C2BBB100C3BFB400B7D5EF00BB7828000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007487990098D8EE00A1E2F700A7EFFF00AEC1CB00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BB782800B6D4ED00BCDBF000A9CC
      E20096BED8008AA1AE00A9B8BF00E3F8FD0093C1E10098948A0089B3C000ECED
      F100F3F5F700E5F2FA00B9D6EE00BB7828000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00325678005A799C008097B600617DA10062798F00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0813700BABDBB00B2CDE000A9C5
      D7009DC4DB0095BAD100B6D5E500E0F5FA0095C2DE00B0CADA00C6D4DE00E7EE
      F400D1DBE300C9DAE700BEC0BC00BF8136000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CDD6DE0091A4B50069839B003C5F800030537600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000EEF0
      F300E2E6EB00AAB8C300A6B5C200BFCAD300B0BFC800A7B5C200B5C2CD00DFE5
      EA00ECF0F3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E1E1E100CCCC
      CC00CCCCCC00CCCCCC00E1E1E100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F0FBFF00C0C0
      C000C0C0C000C0C0C000F0FBFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CCCCCC00CDCDCD00EFEF
      EF00E6E6E600CECECE00CCCCCC00CCCCCC00CCCCCC00CCCCCC007384D100374D
      CC00384DCB00384DCC007482CC00E1E1E1002E9272002E9171002E9070002B90
      6F001F906E00A6836B00A3866A00A4876A00A3866A00A6836B001F906E002B90
      6F002E9070002E9171002E927200FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000F0FBFF0040A080000080
      4000008040000080400040A08000F0FBFF00CFCFCF00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CFCFCF000000000045A2EF0042A5F3009CC3
      DC00C2839000AF405F00AC3C5D00AE3C5C00B63B54006545A0003352DC00375D
      F900375DFA00385DF9003852D7007381CC002D8463002BB2810029B3830023A8
      790015795500FFFBEB00217DC5003B8CCD00217DC500FFFBEB001579550023AD
      7C0029B383002BAC7E002D846300FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000040A0800000A0600000C0
      800080E0C00000C0800000A0600040A08000A9958600A28C7C00A08B7A00A08A
      7A00A08B7A00A18B7A00A08B7A00A08A7A00A08A7A00A08A7A00A08A7A00A08A
      7A00A08A7A00A08B7A00A28C7C00A99586000000000047ADF60086E8FF00A04A
      6D00B6415800B8555E00B95A5F00BB5A5D00C65B55002449D8003E65FE003B60
      FA003A5DF8003C60FA004165FB00344BCC00EBF3EF0051785D00178B5D006197
      7A00DAD6C500FFF4E0000669B300438DC9000669B300FFF4E000DAD6C5006191
      7600178B5D00517E5F00EBF3EF00FFFFFF00C0DCC000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C0000080400000C0800000C0
      80000000000000C0800000C0800000804000A28C7C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A28C7C0000000000B0DAEE00A14C6E00B948
      5700C05D6100D3727300E07C7D00E37D7C00EE8175002040D100A6B8FF000000
      00000000000000000000A9BAFF003148CA00FFFFFF00AC887300FFF6ED00FEEA
      DD00FBE9DC00FFF1DE000057AB003D8DCD000057AB00FFF1DE00FBE9DC00FEEA
      DD00FFF6ED00AC887300FFFFFF00FFFFFF0080C0A00080C0800080C0800080C0
      800080C0800080C0800080C0800080C0A00080C0A0000080400080E0C0000000
      0000000000000000000080E0C00000804000A08A7A00000000009C9D9D00AFB0
      B0009FA0A0008F8F9000A0A1A200A0A1A100A0A0A1009FA0A0009F9FA000A7A8
      A800A6A7A7009C9D9D0000000000A08A7A0000000000C3879100BA4A5C00C669
      6E00DF7B7C00E2CFCF00E9FFFF009BAEAB00F6FFFF001E37CA005874FE005775
      FE005473FD005776FE005D79FF00334ACB00FFFFFF00A1887100FDF3E800F4E5
      D700F6E8DA0097806600A3886A00A78A6B00A3886A0097806600F6E8DA00F4E5
      D700FDF3E800A1887100FFFFFF00FFFFFF0080A08000C0DCC00080C0C00080C0
      A00080C0C00080C0C00080C0C00080C0C000C0C0C0000080400000C0A00000C0
      A0000000000000C0A00000C0A00000804000A08A7A0000000000000000000000
      00000000000000000000E0E2E30000000000E0E2E2000000000000000000FCFB
      FA00000000000000000000000000A08A7A0000000000B2485D00C66F7A00DB7D
      7F00DFCDCE00DFFFFF00E2FFFF00E3FFFF00E8FFFF007993E0003951DB006B86
      FF00718BFF006E89FF00465EDE008795E100FFFFFF009F897400FCF3EB00C7B6
      A200CABAA600CEBEAA00D1C0AC00D2C1AD00D1C0AC00CEBEAA00CABAA600C7B6
      A200FCF3EB009F897400FFFFFF00FFFFFF0080A08000C0DCC00040A0800040A0
      6000C0DCC000C0DCC00080E0C00080C0A00080C0A00040A0600000A0600000E0
      A00080E0E00000E0A00000A0600040A06000A08B7A0000000000BC9A9700DCCD
      CC00BD9B9900DDCECD00BF9C9A00DECFCE00BF9C9A00DDCDCC00D3BDBD00B793
      8F00B7928F00D2BBBB0000000000A08B7A0000000000B0455900D3899300E781
      8000D5FFFF00DAFFFF0054494700B6CED000DCFFFF00E3FFFF007695E400203F
      CF002344D1002849D2008A9AE50000000000FFFFFF00A18C7600FCF4EC00F2E3
      D400CAB8A500F5E6D900CBBAA600F5E7D900CBBAA600F5E6D900CAB8A500F2E3
      D400FCF4EC00A18C7600FFFFFF00FFFFFF0080A08000C0DCC00080C0A000C0DC
      C000C0DCC000C0DCC00080A08000C0DCC000C0DCC00080C0A00040C080000080
      4000008040000080400040C0800080A08000A18B7B0000000000B7939100F9FF
      FE00B8949200F6F8F700B8959200F6F8F700B9959300F0F0EF00B8939100F8FD
      FC00F8FDFC00B792900000000000A18B7B0000000000B1475700DB98A300EA82
      8100809B9C00D4FFFF00B5D1D4005A504E00AEC7C900D3FCFE00889F9800F887
      7C00EB9B9A00C44746000000000000000000FFFFFF00A38E7800FCF5EE00C7B5
      A200C9B8A500C9B8A600C9B8A600C9B8A600C9B8A600C9B8A600C9B8A500C7B5
      A200FCF5EE00A38E7800FFFFFF00FFFFFF0040A08000F0FBFF0080A08000C0DC
      C00040A06000C0DCC00080A08000C0DCC000C0DCC00080A08000C0DCC00080A0
      8000C0DCC00080C0A000F0FBFF0080A08000A18B7B0000000000B8939000F6FC
      FB00B9959100F1F5F200B9959200F1F5F300B9969200EDECEA00B8949100F5FA
      F900F4F9F900B7938F00FBFCFE00A18B7B0000000000B5455200E5A8B300ED83
      8300C4FFFF00AACCD0005E545100ADC9CD00C8F5FC00C5F4F900C2FDFF00EE83
      8100E8A9B200BA464D000000000000000000FFFFFF00A5917A00FDF6EF00EEDF
      CF00C9B7A400F1E3D300C9B8A500F1E3D300C9B8A500F1E3D300C9B7A400EEDF
      CF00FDF6EF00A5917A00FFFFFF00FFFFFF0040A08000F0FBFF0080A08000C0DC
      C000C0DCC000C0DCC00080A08000C0DCC000C0DCC00080A08000C0DCC000C0DC
      C000C0DCC00080A08000F0FBFF0040A08000A18B7A0000000000BA979500D6C7
      C600BB999700D6C7C600BB999700D6C7C600BB999700D6C7C600CCB8B500B691
      8F00B6908E00CBB6B400FEFFFF00A18B7A00DDDDDD00B44A5500E5A1AD00ED94
      9700D2C9CD005A575400A4C8CF00C1F6FF00BCF0F800B7F1FA00CDC3C600EC93
      9600E5A1AC00B54A5500DDDDDD0000000000FFFFFF00A7937C00FFF9F300C5B4
      9F00C8B7A300C8B7A400C8B7A400C8B7A400C8B7A400C8B7A400C8B7A300C5B4
      9F00FFF9F300A7937C00FFFFFF00FFFFFF0040A08000F0FBFF0040A0800040A0
      800080E0C00080E0C00080C0A00080C0A00080C0800080C0A00080E0C00080E0
      C00040A0800040A08000F0FBFF0040A08000A18B7A00FEFFFF00E5E3E300DDD5
      D400E5E5E400DDD5D500E5E5E400DDD5D500E5E5E400DDD5D400EAECEB00EAEC
      EB00EAEBEB00E9EBEA00FEFFFF00A18B7A0071B7EB007E79A200CE6B7100F6BE
      CB00F1898A00CEC5CC00B0F6FF0065858A00ADF2FC00CABFC500EF868600F5BD
      CA00CE6B71007E79A20071B7EB0000000000FFFFFF00A9957F00FEFAF500ECDB
      CA00C8B7A200EFDFCF00C8B7A300EFDFCF00C8B7A300EFDFCF00C8B7A200ECDB
      CA00FEFAF500A9957F00FFFFFF00FFFFFF0040A08000F0FBFF00408060004080
      600040A0800040A0800040A0800040A0800040A0800040A0800040A0800040A0
      80004080600040806000F0FBFF0040A08000A28D7D0000000000FDFFFF00FDFF
      FF00FDFFFF00FDFFFF00FDFFFF00FDFFFF00FDFFFF00FDFFFF00FDFFFF00FDFF
      FF00FCFFFF00FDFFFF0000000000A28D7D0048AFF40051C5FF00AB5B6500DC8C
      9300FECDDA00F5A0A300F2868400F4868400F2868300F59FA200FDCCDA00DC8B
      9300AB5B650051C5FF0048AFF40000000000FFFFFF00AC978100FFFCF700C4B1
      9C00C6B5A000C7B6A100C7B6A100C7B6A100C7B6A100C7B6A100C6B5A000C4B1
      9C00FFFCF700AC978100FFFFFF00FFFFFF0040A06000F0FBFF00F0FBFF00F0FB
      FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
      FF00F0FBFF00F0FBFF00F0FBFF0040A06000C2B3A900A28D7D00A18B7A00A08B
      7A00A08B7A00A08B7A00A08B7A00A08B7A00A08B7A00A08B7A00A08A7A00A08A
      7A00A08A7A00A08B7A00A28D7D00C2B3A90050B3F20067D7FD0068D7FB00AF61
      6500D3747900F8C8D600FFE7F900FFE7F900FFE7F900F8C8D600D3747900AF61
      650068D7FB0067D7FD0050B3F20000000000FFFFFF00AD998300FFFEFB00E7D3
      C100C3B09C00E9D6C400C4B19D00E9D6C400C4B19D00E9D6C400C3B09C00E7D3
      C100FFFEFB00AD998300FFFFFF00FFFFFF0080C0A00080C0A000C0DCC000C0DC
      C000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0DCC000C0DC
      C000C0DCC000C0DCC00080C0A00080C0A0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0E5FB005AC6F70079F1FF007AFF
      FF00858EA700B1565800B6515000B5525100B6515000B1565800858EA7007AFF
      FF0079F1FF005AC6F700C0E5FB0000000000FFFFFF00B09D8700FFFFFC00FFFC
      F800FFFDF900FFFDFA00FFFDFA00FFFDFA00FFFDFA00FFFDFA00FFFDF900FFFC
      F800FFFFFC00B09D8700FFFFFF00FFFFFF000000000080C0A00040A0600040A0
      600040A0600040A0600040A0600040A0600040A0600040A0600040A0600040A0
      600040A0600040A0600080C0A000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0E7FB0054BDF5004EC0
      FB0084DBFF00000000000000000000000000000000000000000084DBFF004EC0
      FB0054BDF500C0E7FB000000000000000000FFFFFF00CCC0B000B29F8A00B19E
      8800B19E8700B19E8700B19E8700B19E8700B19E8700B19E8700B19E8700B19E
      8800B29F8A00CCC0B000FFFFFF00FFFFFF0000000000CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00D3D3D300FFFFFF0000000000CDCDCD00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CDCDCD0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF00000000000000000000000000FFFFFF000000000000000000DCDCDC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00DCDCDC000000000067A4CF003890CE00388FCE003990
      CE003991D0003795D6005D6975004B668200518ED6003693D1003690CF00358E
      CD00358DCC00368FCE004E9BD000FFFFFF00C3AB9600AA570F00A7530B00A752
      0A00A7520A00A7520A00A7520A00A7530B00AA570F00C3AB9600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00967A6100967A6100FFFFFF00967A
      6100967A6100FFFFFF00967A6100967A6100FFFFFF00967A6100967A6100FFFF
      FF00967A6100967A6100967A6100FFFFFF000000000000000000BBBBB900B0B0
      AD00B0B0AC00B3B2AC00B0B0AC00AEAEAB00ADADAB00ADADAB00ADADAB00ADAD
      AB00AEAEAB00AFAFAD00BBBBB900000000003890CE0074DEFF005ED1FA004CC8
      F4003BAFE50054B4E80062839A0082A7B5008FD4FE003462930083E9FF007EE3
      FF007DE1FF0085E7FF00368FCE00FFFFFF00B25F1800D8995D00E3A97100E3A8
      7000E3A77000E3A77000E3A87000E4A97100D9995E00B2601900FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00967A6100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00967A6100FFFFFF000000000000000000B0B0AE000000
      000000000000868CF00000000000000000000000000000000000000000000000
      00000000000000000000B0B0AE0000000000388FCE005ED1FA004AC5F2003BA9
      E00039BDF200368CCC004FB0EE008FE6FF0081D3FF001398FF003660900078E3
      FF0073DDFF0084E3FF00358DCC00FFFFFF00C2793700EAC09700E1A26100E19F
      5C00E19D5900E19D5900E19F5D00E2A36100EAC19900BA6B2200FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000AFAFAD000000
      000000000000363DE00000000000000000000000000000000000000000000000
      00000000000000000000AFAFAD00000000003990CE004CC9F4003BA9E00038BA
      EF0047D5FF003589C90074E2FF003072C00044C5FF002BAAFF001899FF003660
      8F0072E1FF0086E7FF00358ECD00FFFFFF00CF8D4F00F1D6BC00F0B77600F0BA
      7B00F5FAFD00EBEFF200F0B97A00F2BA7900F3D9C000C4762D00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00967A6100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00967A6100FFFFFF000000000000000000AFB0AE00E4D4
      BF00D0B58D002423BA00D0B68F00C6AC8F00C3AA8F00C3AA8F00C3AA8F00C3AA
      8F00C3A98D00E2D2C000AFB0AE00000000003A91CF003BAEE40038BDF20047D5
      FF0067DFFF003487C8006DDDFF006DDDFF003574BF0049C7FF002DABFF001599
      FF00306094008AEEFF003390D100FFFFFF00D0884300F2DBC500FAE6CE00FFF8
      EA00FFFEFE00F6F5F400F1E6D600EACAA400EFD6BC00D1864000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00967A6100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00967A6100FFFFFF000000000000000000AFB0AE000000
      0000000000003A42E3000000000000000000FEFFFF00FEFFFF00FEFFFF00FDFF
      FF00FDFFFF0000000000AFB0AE00000000003991CF005AB1E3003689C9003688
      C8003587C80046A6DE0065D8FF0064D7FF0067DCFF003574BF0046C7FF0022AA
      FF0084ADD200857365002C94D900FFFFFF00FDFAF600E0A16300EEBA8600AFC7
      CE005D9FC3003C7CA10076888D00BA7F3D00BA874D00C8B7A500EBEBEB00F5F5
      F500FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000AFB0AF00E5D5
      C100D1B78E002625BC00D2B88F00C7AF9000C5AC9000C5AC9000C5AC9000C5AC
      8F00C4AB8E00E2D3C100AFB0AF00000000003790CE0059AFE00061DBFF0062D9
      FF0061D8FF0060D7FF005FD5FF005FD4FF0060D6FF0062DBFF002F74C200B3DB
      EE0093887E00C3BEB5007D7A6900CCCCCC00FFFFFF00FFFFFF00FBEEE10096AA
      AB0067A3C500508AAA0071808200D2AB8000FFF9E400CCA97D00C3AE9700C9BF
      B400CCCCCC00CDCDCD00EDEDED00FFFFFF00967A610000000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF000000000000000000967A6100FFFFFF000000000000000000AFB0AE000000
      0000FFFFFC003740E200FFFFFE00FBFEFE00F9FBFE00F9FBFE00F9FBFE00F8FB
      FE00F8FAFD0000000000AFB0AE0000000000358ECD0096EBFF0057D4FF0059D3
      FF0059D3FF0059D3FF0059D2FF0059D2FF0059D3FF0059D5FF0056D9FF00947D
      7100EFE7E1008A8B8100BB79B5009968C900FFFFFF00FFFFFF00C4CACE0078BD
      E2007FC0E3006EAFD4004C95C400ADA29300D6B08300FFFFFA00D3AF8200CBA1
      7000B7814500BA864E00C6B39F00EDEDED00967A6100967A6100FFFFFF00967A
      6100967A6100FFFFFF00967A6100967A6100FFFFFF00967A6100967A6100FFFF
      FF00967A6100967A6100967A6100FFFFFF000000000000000000AFB0AF00E5D5
      C100D0B68E002625BD00D1B89000C7AE9100C5AC9100C5AC9100C5AC9100C4AB
      9100C3AA8F00E3D3C100AFB0AF0000000000338CCC0099EAFF004FD1FF0052D0
      FF0053D1FF0053D1FF0053D1FF0053D1FF0053D1FF0053D1FF0051D3FF004AD7
      FF008D7E7200E6B0DD00CD95C500B07CCE00FFFFFF00FFFFFF00163D6E00A7EF
      FF008ED1F10078BEE200889D9F00CF985E00F3DDBD00FFFAE500FFFBE600FFFC
      E800FFFFED00F4E2C500C99B6900C7B4A00000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000AFB0AE000000
      0000FFFFF600373FE000FFFFF800F5F8F900F3F5F900F3F5F900F3F5F900F3F5
      F800F2F4F60000000000AFB0AE0000000000338CCC009DEAFF0047CEFF004ACE
      FF004BCFFF004BCFFF004BCFFF004BCFFF004BCFFF004BCFFF004BCFFF0048D2
      FF003FD6FF00CD81CD00CA86D100FFFFFF00FFFFFF00FFFFFF000A3A6A0069AA
      CE0078B5D80081C9F100C5894A00FDE7CA00FFF5DC00FFF3D900FFF2D900FFF0
      D700FFF0D600FFF5DE00FAE8CE00BF895000967A6100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00967A6100FFFFFF000000000000000000AFB0AF00E5D5
      C100D1B68D002726BC00D2B89000C8AF9100C5AD9100C5AD9100C5AD9100C5AD
      9000C4AB8E00E3D3C100AFB0AF0000000000328CCC00A2EDFF008CE4FF008FE4
      FF0090E4FF0090E4FF0090E4FF0090E4FF0090E4FF0090E4FF0090E4FF008FE4
      FF008AE6FF009CF1FF00288FCE00FFFFFF00FFFFFF00FFFFFF00114374002D77
      A9004B89B8004273A800CE894500FFF9E100FFECCE00FFEDD000FFEDD000FFEC
      D000FFEACA00FFECCC00FFFAE300C1894F00967A6100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00967A6100FFFFFF000000000000000000AFB0AE000000
      0000F8FAEF00353DDF00FAFBF200F0F2F300EDF0F300EDF0F300EDF0F300EDEF
      F200ECEEF00000000000AFB0AE0000000000328CCC00A9EFFF0037B5EE003BB6
      EE003CB7EE003CB7EE003CB7EE003CB7EE003CB7EE003CB7EE003CB7EE003BB6
      EE0036B6EE00A8F0FF00318CCC00FFFFFF00FFFFFF00FFFFFF0012407000287B
      AE003E88BA002F73AC00D4904B00FFF7E100FFE7C500FFE9CA00FFE9CA00FFE9
      CA00FFE8C800FFE6C200FFF7E200C58D530000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000AFB0AE00E5D5
      C100CFB58D002624BC00D0B78F00C7AD9000C4AC9000C4AC9000C4AC9000C4AB
      9000C3A98D00E3D3C100AFB0AE0000000000338CCC00B3F3FF0037CDFF003BCD
      FF003CCEFF003CCEFF003CCEFF003CCEFF003CCEFF003CCEFF003CCEFF003BCD
      FF0037CDFF00B3F3FF00338CCC00FFFFFF00FFFFFF00FFFFFF00E0E6ED001449
      7A001A5D9200094C8600CB8E5300FEE8CF00FFECCF00FFE7C300FFE6C200FFE6
      C200FFE7C300FFE9C600FBE6CE00C98F5700967A6100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00967A6100FFFFFF000000000000000000AFAFAD000000
      0000EEEDE4002C34D700EFEFE500E5E6E500E3E4E500E3E4E500E3E4E500E3E4
      E400E2E3E30000000000AFAFAD0000000000368ECE0085EAFF0085E7FF0086E6
      FF0087E6FF0087E6FF0087E6FF0087E6FF0087E6FF0087E6FF0087E6FF0086E6
      FF0085E7FF0085EAFF00368ECE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F5DFC900DAA67300F6E1C900FFF6E400FFF4E000FFF4
      E000FFF6E400F6E2CA00D7A57400EDD9C5000000000000000000FFFFFF000000
      000000000000FFFFFF000000000000000000FFFFFF000000000000000000FFFF
      FF000000000000000000967A6100FFFFFF000000000000000000B0B0AE000000
      000000000000898DF20000000000000000000000000000000000000000000000
      00000000000000000000B0B0AE00000000004599D200368ECE00358DCC00358C
      CC00358CCC00358CCC00358CCC00358CCC00358CCC00358CCC00358CCC00358C
      CC00358DCC00368ECE004599D200FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F0DBC600D1986100CF935A00CE935A00CE93
      5A00CF935A00D1986100EFDAC600FFFFFF00967A6100967A6100FFFFFF00967A
      6100967A6100FFFFFF00967A6100967A6100FFFFFF00967A6100967A6100FFFF
      FF00967A6100967A6100FFFFFF00FFFFFF000000000000000000B8B8B600B0B0
      AD00B1B0AC00B3B3AC00B0B0AC00AEAEAB00AEAEAB00AEAEAB00AEAEAB00AEAE
      AB00AEAEAC00B0B0AD00B8B8B600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E1E1E100CCCC
      CC00CCCCCC00CCCCCC00E1E1E1000000000000000000CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC0000000000FFFFFF00FFFFFF0000000000CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F4F4F400F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F6F6F600DFDFDF0053B28C00009E
      5E00009D5D00009E5E0053B28C00E1E1E1009493910076747200706D6C006D6A
      6900696765006663620083818000FFFFFF00FFFFFF009493910076747200706D
      6C006D6A69006967650066636200838180000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E1E1E100CCCC
      CC00CCCCCC00CCCCCC00E1E1E10000000000CFCFCF00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CFCFCF00D0D0D000CDC4B800CCC4B900CCC4
      B900CCC4B900CCC4B900CCC4B900CDC5B900D1C5BB004DB0880000A5690000BA
      860077DFC40000BA860000A66A0053B28C007A787600817E7D009C9996008985
      8300716F6C0059575600605D5C00FFFFFF00FFFFFF007A787600817E7D009C99
      960089858300716F6C0059575600615E5D000000000000000000000000000000
      00000000000000000000000000000000000000000000E1E1E1006872BC002233
      AD002232AD002233AD006872BC00E1E1E1000D0DDF000000DD000000DC000000
      DC000000DC000000DC000000DC000000DC000000DC000000DC000000DC000000
      DC000000DC000000DC000000DD000D0DDF00CDA67100CCA06600CA9E6300CA9E
      6300CA9E6300CA9E6200CA9E6200CD9E6300E0A06600009B5A0000BF8B0000BB
      82000000000000BB820000C08C00009E5E0077757300CFCFCF00D9D7D600D0CF
      CE00C8C8C700C3C1C1005E5A5900FFFFFF00FFFFFF0077757300D0CFCF00D9D7
      D600D0CFCE00C8C8C700C3C1C1005E5B5A000000000000000000000000000000
      00000000000000000000000000000000000000000000666FBC00283DC1003960
      FA003961FE003960FA00283EC1006772BC00DEDDB300FFFFF100FFFFED00FFFF
      ED00FFFFED00FFFFED00FFFFED00FFFFED00FFFFED00FFFFED00FFFFED00FFFF
      ED00FFFFED00FFFFED00FFFFF000DEDDB400CCA06600FFFFF500FFFFED00FFFE
      EB00FFFDEB00FFFDEA00FFFDE900FFFDEB00FFFFF2000096520073E4CA000000
      0000000000000000000077E5CC00009C5C0076747200B3B1AF00CAC9C700B0AE
      AD0091908E00757472005E5A5900CCCCCC00CCCCCC0077757300B3B1AF00CAC9
      C700B0AEAD0091908E00757572005F5C5B00DCDCDC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC001826B0004265FD003D61
      FB003A5EF9003D61FB004366FD001E30AD000000E0001F4EFF000031FF000031
      FF000031FF000031FF000031FF000031FF000031FF000031FF000031FF000031
      FF000031FF000031FF001F4EFF000000E000CA9E6300FFFEED00E6CAA100F7E4
      C500FFF6DF00FFF4DC00FFF3DB00FFF4DD00FFF9E50000934F0000CA940000C7
      8E000000000000C88F0000CC9800009C5D0076747200B3B1AF00CAC9C700B1AE
      AE0093929000777674005E5A59007A7876006462600077757300B3B1AF00CAC9
      C700B1AEAE009392900077767400605D5C008CBAA3006DB08F006EB18F006FB1
      8F006EB18F006EB08E006EB18E006FB38E0076BE8B00131FAE00AABBFF000000
      00000000000000000000ACBCFF001B2CAC00D9D8B500FFFFF400FFFFED00FFFF
      EC00FFFFEC00FFFFEC00FFFFEC00FFFFEC00FFFFEC00FFFFEB00FFFFEA00FFFF
      EA00FFFFEA00FFFFEA00FFFFEE00D9D7B300CA9E6300FFFBE900FFFBEF00DAB6
      8400E3C49700FFF3D800FFF3D900FFF5DB00FFF9E1006AC2930000AA6B0000D1
      9B0072ECD20000D29B0000AC6F005EA0640076747200B3B0AE00CAC9C700B0AD
      AD0091908E00757472005B58570076747200615F5D0076747200B3B1AF00CAC9
      C700B0ADAD0091908E00757573005F5C5B006AAD8B00CBF4DB008CC7B50083BE
      A70093CBBB0092CAB90092CBBA0095CDBB009CD7B900101DAF005B77FF005877
      FF005473FE005877FF005D79FF001A29AE000000F1003D64FF000026FF000026
      FF000027FF000027FF000027FF000027FF000026FF000024FF000023FF000022
      FF000022FF000021FF003F60FF000000E000CA9E6300FFF9E800FFEFD000FFFD
      F500DBB58200FFF4D800DCB98800C5965600CA975800F2BF90006BC291000094
      500000934F00009450006CC69B00E3A2690077747200DEDDDD00EBEBE900DBDA
      D900CAC9C800BBBAB90057545300D6D5D3008C8B890075737100DEDDDC00EBEB
      E900DBDAD900CAC9C800BCBCBB005E5B5A0065A98600CAF3DC005FA27C004E93
      6800B8EBCF00B7E9CD0098D1B20088C5A3008ECDA3004065A2002F3FC9006E86
      FF00748CFF006E86FF003042CA0040689D00A15E0000FFFFE100E66A0000FFF0
      C200E66B0000FFF0C200E66B0000FFF0C100EB6B0000FFFFF700FFFFEC00FFFF
      EA00FFFFEA00FFFFEA00FFFFEE00D8D7B300CA9E6300FFFAE800FFEBCA00FFEC
      CB00FFFFFE00E4C39300DCBA88000000000000000000E2BC8B00F3C698000000
      0000FFF1D400FFF1D300FFFEEE00D1A065007A7876006F6D6B0066626100605D
      5C005E5C5A005E5A5A00908F8F00D3D2D0008A898700A09F9E006C6A68006461
      6000625F5E00615F5D00615E5D009795940062A58100CDF2E00073B59300B3E7
      CD00B0E4CA00B0E4CA006BAC8900B3E4C700B5E7C70076BA89005C7BBB00111D
      AE00111EAD000F1CAD006880C5006DB47F0096590000E8A45600FAE7CD00CD5D
      0000FAE9D100CD5E0000FAE9D100CE5E0000FFEAC1000015FF000016FF000017
      FF000016FF000014FF006176FF000000E000CA9E6300FFFAEA00FFE9C500FFEA
      C700EBCB9E00DBB6840000000000FFEFCF00FFEFCF0000000000DDB78400EECC
      9F00FFEBC900FFEAC700FFFAEB00CB9E6300FFFFFF0077747300BCBAB800CDCB
      CA00A5A3A2007E7C7B0059565500D6D5D3008C8C890076737200BCBAB700CCCA
      C900A4A2A1007E7C7B005F5C5B00FFFFFF005FA37E00D0F2E4006CAD8B00ABE2
      C80051946D00ABE2C8006DAC8800B0E2C600B1E3C60070B08800B2ECC9005CA3
      6D00B6F1C70075BA8900D7FBE30062A77E0094570000FFFEFE00BE530000F5E5
      D300BE550000F6E6D400BE550000F7E6D100C7560000FFFFFB00FFFFEC00FFFF
      EA00FFFFEA00FFFFEA00FFFFEE00D7D6B300CA9E6300FFFBEC00FFE8C100F6D9
      AF00DEBA8700FFFFFA00FFE9C700FFE9C600FFE9C600FFE9C700FFFFFA00DEBA
      8700F6D9AF00FFE8C100FFFBEC00CA9E6300FFFFFF0077747300B9B7B500C9C7
      C600A2A09F007D7B7A005C58580077747200615F5D0077747300B9B8B500C9C7
      C600A2A09F007E7C7B00605D5C00FFFFFF005CA07A00D8F5E80067AA8500A7E0
      C600A5DEC400A5DEC40061A17C00B2E3C600B2E3C60061A27C00A6E0C500A7E1
      C400A9E3C60068AC8500D9F6E9005DA07A0094570000E2B18000F3E1CD00B24D
      0000F3E4D200B34D0000F3E4D200B44D0000FCE5C2000007FF000009FF000009
      FF000008FF000005FF00828FFF000000E000CA9E6300FFFCF100FEE1B500DEB6
      8200FFFBEF00FFE6BD00FFE6BD00FFE6BE00FFE6BE00FFE6BD00FFE6BD00FFFB
      EF00DEB68200FEE1B500FFFCF100CA9E6300FFFFFF0078757400BDBBB900CECC
      CB00A5A3A2007E7C7B005E5B5A007B7977006462600079767500BCBBB800CECC
      CB00A6A4A300807E7D00605D5C00FFFFFF00599D7500DFF7EF00549A70005EA2
      7D009FDBC3009FDBC20080BFA0007EB8940078B38F0080BFA0009FDBC2009FDB
      C3005EA27D00559B7300DFF7EF00599D75009557000000000000A6410000F0DE
      CC00A7430000F0DECD00A7430000F1DECA00AD440000FFFFFA00FFFFEC00FFFF
      EA00FFFFEA00FFFFEB00FFFFEE00D8D8B300CA9F6300FFFEF400E6BE8800FFF5
      E200FFE1B200FFE0B200FFE1B300FFE1B300FFE1B300FFE1B300FFE0B200FFE1
      B200FFF5E200E6BE8800FFFEF400CA9F6300FFFFFF00A4A3A100716E6D006966
      650064605F00625E5D008D8C8B00FFFFFF00FFFFFF009C9B9A006F6C6B006966
      6500676463006562610095939200FFFFFF00569B7200E5F9F400448B5E00468C
      61005A9E76005A9D75005A9E76005B9F77005B9F77005A9E76005A9D75005A9E
      7600468C6100448B5E00E5F9F400569B720097590000E5C6AB0000000000E1C0
      A20000000000E1C1A20000000000E2C1A000FFFFFC009FABFF00A6AEFF00A7AE
      FF00A7AEFF00A8AFFF00AFB7FF000000DE00CCA06500FFFFFD00FFFFF800FFFF
      F700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFFF700FFFF
      F700FFFFF700FFFFF800FFFFFD00CCA06500FFFFFF00FFFFFF00CCCCCC007875
      7400CBCAC7009B9A9900625E5D00FFFFFF00FFFFFF0078757400CAC8C6009897
      9600605D5C00CCCCCC00FFFFFF00FFFFFF0054976F00E7FAF500E3F6F100E3F6
      F100E3F5F000E2F5F000E2F5F000E2F5F000E2F5F000E2F5F000E2F5F000E3F5
      F000E3F6F100E3F6F100E7FAF50054976F00BA904F0097590000955600009456
      000094560000945600009456000097580000A25E00000000EE000000DE000000
      DB000000DB000000DB000000DC003A3AE600DDC09900CCA06500CA9E6300CA9E
      6200CA9E6200CA9E6200CA9E6200CA9E6200CA9E6200CA9E6200CA9E6200CA9E
      6200CA9E6200CA9E6300CCA06500D5B18000FFFFFF00FFFFFF007A797600908D
      8C00908E8C0065646100615E5D00FFFFFF00FFFFFF00797775008E8B8A00918F
      8D006866640063605F00FFFFFF00FFFFFF008CB99E0089C6A600B0E7CC00AEE5
      CA00AEE5CA00AEE5CA00AEE5CA00AEE5CA00AEE5CA00AEE5CA00AEE5CA00AEE5
      CA00AEE5CA00B0E7CC0089C6A6008CB99E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF007B7977007371
      6F006D6B690069666500625F5E00FFFFFF00FFFFFF007B79770073716F006D6B
      69006966650063605F00FFFFFF00FFFFFF00000000008AB79B00509269004F91
      68004F9168004F9168004F9168004F9168004F9168004F9168004F9168004F91
      68004F916800509269008AB79B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D4D4D400BFBFBF00BFBFBF00D4D4D400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BFBFBF00BFBFBF00BFBFBF00F4F4F40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DDDDDD00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00DCDCDC0000000000D3D3D300BFBFBF00BFBF
      BF00BFBFBF00BFBFBF009999990086868600888888009D9D9D00BFBFBF00BFBF
      BF00BFBFBF00BFBFBF00D3D3D3000000000000000000ECECEC00C1C1C100BFBF
      BF00BFBFBF005E5E5E00575757008E8E8E00B5B5B500C1C1C100ECECEC000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DCDCDC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC005C77A100274B8500294B84002A4B84002A4C84002A4C
      84002A4B8400294B8400284B84005B749C0000000000868686006E6E6E006F6F
      6F006D6D6D006969690091919100AEAEAE00CFCFCF00BDBDBD00919191006D6D
      6D006F6F6F006F6F6F0087878700000000000000000095959500414141003D3D
      3D003C3C3C007B7B7B008F8F8F00D4D4D4006363630043434300959595000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840063646400636363006464
      6400666564006B676100214988005B93B900207DAF00227EB0002077A4002077
      A400227EB000207DAF005C93B900274B8400BFBFBF006C6C6C00CDCDCD007171
      710068686800F4F4F400EBEBEB0087878700BCBCBC00DCDCDC00BEBEBE008181
      8100878787008C8C8C0079797900D9D9D90000000000484848007C7C7C008D8D
      8D008A8A8A00B7B7B700D3D3D300D3D3D300A4A4A400676767004C4C4C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E0E0
      E000BFBFBF0000000000000000000000000066656500706E6D00797574007B77
      760067656500706B6500214887003380AA0011669800136697000F527A000F52
      7A0013669700116698003481A900274A840078787800BFBFBF004E4E4E007B7B
      7B0066666600ECECEC00DFDFDF00E1E1E10087878700BBBBBB00D4D4D400BDBD
      BD00C4C4C400C1C1C1009A9A9A00A2A2A200000000005C5C5C00A3A3A3008383
      8300808080007F7F7F0075757500C3C3C300AFAFAF00A5A5A50065656500BFBF
      BF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009191
      910066666600BFBFBF00000000000000000070707000A19F9D007C7877006666
      6600565554005B575200224888003D88B200146C9E00166B9C0040C2FF0040C2
      FF00166B9C00146C9E003E89B100274A8300000000006D6D6D005E5E5E008383
      830065656500EBEBEB00D9D9D900DADADA00DFDFDF007A7A7A00CACACA00CECE
      CE00D1D1D100D4D4D400D0D0D00091919100000000006B6B6B00B9B9B9009393
      930095959500F9F9F900ECECEC0073737300C5C5C500B0B0B000A5A5A5006262
      6200BFBFBF00000000000000000000000000D8D8D800BFBFBF00BFBFBF00BFBF
      BF00BFBFBF00BFBFBF00BFBFBF00BFBFBF00BFBFBF00BFBFBF00BFBFBF006363
      6300C0C0C00062626200BFBFBF000000000066666500C4C2C100656565007474
      7400333231004A4640002047870065A2C2000D6699000F6698000F6496000F64
      96000F6698000D66990066A3C20026498200BFBFBF006B6B6B00CCCCCC008989
      890064646400EBEBEB00D3D3D300D3D3D300D9D9D9007F7F7F00DCDCDC00DDDD
      DD006C6C6C006D6D6D00E2E2E20095959500000000007A7A7A00D4D4D400A7A7
      A700EAEAEA0000000000F3F3F300D2D2D20075757500C5C5C500AFAFAF00A8A8
      A80061616100BFBFBF0000000000000000008686860064646400636363006363
      6300636363006363630063636300636363006363630062626200616161009696
      9600B9B9B900BEBEBE0061616100BFBFBF00C6C6C6006F6F6D0081817F00C3C2
      C100908E8D0098938E001B4485006CA0BD006398B7006599B8006598B7006598
      B7006599B8006499B8006FA2BE00244A840078787800BEBEBE004C4C4C009393
      930064646400EBEBEB00CDCDCD00F4F4F4000000000083838300D3D3D300D9D9
      D9007373730074747400DADADA00999999000000000077777700B0B0B000E3E3
      E300D3D3D30094949400707070008C8C8C00EAEAEA007B7B7B00D7D7D7007171
      7100AEAEAE0063636300BFBFBF000000000064646400B8B8B800B3B3B300B2B2
      B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B1B1B100AEAE
      AE00ACACAC00AEAEAE00C4C4C4006464640000000000FAFAFA00706D6C00E9EE
      F00032302F0033302B005675A000133C7E00163E7E001B41800019407E00193F
      7E001A417F00163E8000184386006D88B100000000006C6C6C005E5E5E009A9A
      9A0063636300ECECEC00CACACA0000000000AFAFAF009A9A9A00A2A2A200D0D0
      D000D6D6D600D5D5D500AFAFAF00C4C4C4000000000000000000A1A1A1009090
      9000B2B2B2008A8A8A00696969007D7D7D0096969600AAAAAA006F6F6F00E4E4
      E4007474740079797900747474000000000064646400D3D3D300CECECE00CDCD
      CD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CBCBCB00CACA
      CA00A5A5A500A7A7A700CBCBCB00646464000000000000000000FEFEFE00B1CE
      DE006EAFD0005593B500AFC6D500F7F0F000F3EDEE000D3A8400FFF5EB00FEF5
      E9000B387E0000000000CE87000000000000BFBFBF006B6B6B00CBCBCB00A1A1
      A10063636300EEEEEE00C6C6C600000000006D6D6D00B2B2B2009A9A9A009090
      9000949494009B9B9B008080800000000000000000000000000000000000D6D6
      D6009E9E9E00B1B1B1009F9F9F007F7F7F00E0E0E00000000000000000007171
      7100B3B3B3009393930082828200000000009191910064646400626262006262
      6200626262006262620062626200626262006262620061616100606060008989
      8900A2A2A200D0D0D00061616100000000000000000000000000D5D7D80088CC
      EE0080C1E3006FAFD20062A1CD00F1CC1C00FFD90D0063705A00043587000434
      8500626D4E0000000000BC7F06000000000078787800BEBEBE004B4B4B00AAAA
      AA0063636300F1F1F100C2C2C200F0F0F000000000000000000000000000F5F5
      F500C9C9C900F5F5F50068686800000000000000000000000000000000006666
      6600DFDFDF00BFBFBF00ACACAC00A2A2A2007373730000000000000000000000
      0000919191008E8E8E0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006363
      6300D6D6D60061616100000000000000000000000000000000003B5C8100A8EF
      FF008FD2F1007EBFE0006DB0D90042524300FBD74300F6D43F00F9D43B00F8D4
      3A00F4D0350000000000B57C090000000000000000006C6C6C005E5E5E00ACAC
      AC0063636300F4F4F400C1C1C100C2C2C200C4C4C400C4C4C400C3C3C300C2C2
      C200C2C2C200F6F6F60066666600000000000000000000000000000000003030
      3000F3F3F300D5D5D500C0C0C000B8B8B8002727270000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C9C
      9C006666660000000000000000000000000000000000000000000A3B6B006AAE
      D20074ADD10088C6E50075B4D800001654000000000000000000000000000000
      00000000000000000000B47B090000000000BFBFBF006B6B6B00B9B9B900AAAA
      AA0062626200F8F8F800BEBEBE00C3C3C3005C5C5C008D8D8D008B8B8B00C1C1
      C100BEBEBE00F9F9F90066666600000000000000000000000000000000003838
      3800555555007B7B7B00A8A8A8008E8E8E002B2B2B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000114574002D79
      A9004E8AB900517AA7002543710003255900CDC7C200E4E3E40000000000FEEF
      AC00F9D9440000000000B47B0A000000000078787800BCBCBC0047474700A9A9
      A90061616100FCFCFC00B9B9B900BCBCBC00BFBFBF00BFBFBF00BDBDBD00BABA
      BA00B9B9B900FDFDFD0067676700000000000000000000000000000000003D3D
      3D00727272008383830073737300414141002E2E2E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008399B100277A
      AD003E89BA003A79A900123D6F005C6A8100C9C4C100BEBBBB00B9B7BD00F5E1
      8000E3B7000000000000B47C0B0000000000000000006A6A6A00E8E8E800E7E7
      E7006464640000000000FEFEFE00FEFEFE000000000000000000FEFEFE00FEFE
      FE00FEFEFE000000000068686800000000000000000000000000000000005151
      51006C6C6C007D7D7D006E6E6E00404040004C4C4C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007396
      B300175C94000A4E8A007A98BE00000000000000000000000000000000000000
      00000000000000000000B67E0F000000000000000000979797006B6B6B006A6A
      6A006B6B6B006868680066666600666666006666660066666600666666006666
      660066666600686868009696960000000000000000000000000000000000E0E0
      E0004A4A4A004E4E4E004848480046464600E0E0E00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E3B45C00C4860E00BA7F0B00B67C0900B57B0900B57B0900B47B
      0900B57C0A00B67E0F00CFAA6000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CCCCCC00CCCCCC00CCCCCC00F6F6F60000000000000000000000
      00000000000000000000000000000000000000000000CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00000000000000000000000000000000000000
      00000000000000000000CCCCCC00CCCCCC00CCCCCC00CCCCCC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F0F0F000CDCDCD00CCCC
      CC00CCCCCC0051708900496786004E90D900B4C1D000CDCDCD00F0F0F0000000
      000000000000000000000000000000000000BD944400B67E0E00B57C0900B57C
      0900B57C0900B57B0800B47B0800B57B0800B57B0800B47B0800B57B0800B57B
      0800B57B0800B57C0900B67E0E00BD9444000000000000000000000000000000
      000000000000000000007D7E7E00E7E6E300B5B3B10080808100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C3AB9500A9561000A951
      0800B04E00005589AE00759FB5008ED6FF002D6BA500BE550600C5AB94000000
      000000000000000000000000000000000000B67E0E00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B67E0E00DCDCDC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC0081828100EDECE900BAB8B50082838300CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00DCDCDC00000000000000000000000000BFBF
      BF00CECECE000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B15F1600D7975A00E4A6
      6C00EBA461003BB5FF0078D6FF0080D5FF00119CFF002E6FAC00C96105000000
      000000000000000000000000000000000000B57C0900FFFFFF00A09D9E008F8F
      8F0093929100D2D1D200FFFFFF00C1C2C300C1C2C400FFFFFF00C1C2C400C3C4
      C600C3C4C600C2C4C700FFFFFF00B57B09008C8A89006C6968006B6767006A67
      66006B6866006F6C6600716D63006A676100686563006A6766006C6968006D6A
      69006D6A69006D6A69006E6B6A008D8A8A000000000000000000BFBFBF006666
      6600787878000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0763300E8B98D00E09E
      5C00E49B5400F09944001F74CA003EC5FF002AABFF00169EFF00336EA600CCCC
      CC0000000000000000000000000000000000B57B0800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B47B0800716E6D00CBCAC800C4C2C000C3C1
      BF00D7D5D200A9A697000000C700ABA79900A29F9B00A19E9B00A19E9C00A19E
      9C00A19E9C00A19E9C00ABA9A700726F6E0000000000BFBFBF0062626200C0C0
      C00063636300BFBFBF00BFBFBF00BFBFBF00BFBFBF00BFBFBF00BFBFBF00BFBF
      BF00BFBFBF00BFBFBF00BFBFBF00D3D3D30000000000CA864800EDCBAA00EAAD
      6B00EBAF6E00FCFAF900FFF2E2002573C50043C7FF002BACFF00159EFF00306B
      A400CCCCCC00000000000000000000000000B57B0800FFFFFF00999696008B88
      86008F8C8A00CAC9C800FCFCFB00BBBBBB00BBBBBB00FCFCFB00BBBBBB00BDBD
      BD00BDBDBD00BBBCBE00FFFFFF00B57B080074717000C5C3C100B5B3B100B4B2
      B000CECCC800B8B4A2000000C600BAB7A400AEABA600ABA8A600ABA8A600ABA8
      A600ABA8A600ABA8A600BEBCBB0074717000BFBFBF0061616100BEBEBE00B9B9
      B900969696006161610062626200636363006363630063636300636363006363
      63006363630063636300646464007E7E7E0000000000D3955900F4DFCD00F6BF
      7D00FFF1DE00FFFFFD00FDF6EF00FFE1BB002675C80040C7FF0021ABFF0084B1
      D8007F797100CCCCCC000000000000000000B57B0800FFFFFF00FCFDFE00FFFF
      FF00FFFFFE00FAFAF900F7F7F600F8F8F700F8F8F700F7F7F600F8F9F700FAFA
      F900FAFAF900F8F9F900FFFFFF00B57B080078747400C2C0BE00ACAAA800AAA8
      A600C9C8C300C7C5B1000000C600C8C6B300BBB9B400B8B6B400B8B6B400B8B6
      B400B8B6B400B9B7B500D0CFCD007673720064646400C4C4C400AEAEAE00ACAC
      AC00AEAEAE00B1B1B100B2B2B200B2B2B200B2B2B200B2B2B200B2B2B200B2B2
      B200B2B2B200B3B3B300B8B8B8006464640000000000D5924F00EBC49F00FFEC
      D600B3D9EF005DA1C5003E7FA300829EAD00FFF5CF001F78D200AEDCF4009288
      8000C1BFB800777C6E00CCCCCC0000000000B57B0800FFFFFF0092908F008683
      810088868400C4C1BF00F3F2F100B3B2B200B3B2B200F4F2F100B3B2B200B5B4
      B400B5B4B400B2B2B400FFFFFF00B57B08007C787800CFCFCD00A3A19F00A19F
      9D00D9D8D400D6D4C0000000C800D6D4C100C8C6C100C5C3C100C5C3C100C5C3
      C100C5C2C000CAC7C500E1E0DF007976750064646400CBCBCB00A7A7A700A5A5
      A500CACACA00CBCBCB00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCDCD00CDCD
      CD00CDCDCD00CECECE00D3D3D300646464000000000000000000EBB88700F0AA
      6300BDC3BB006099B800437A980094948A00F9AF6400F6C08A0087858300ECEA
      E700898C8300BA7AB6009869CA0000000000B57B0800FFFFFF00F2F1F200F6F5
      F500F5F4F400F0F0EF00EEEDEC00EFEEED00EFEEED00EEEDEC00EFEEED00F1F0
      EF00F1F0EF00EEEEEE00FFFFFF00B57B0800817E7C00D5D3D2009A9795009795
      9300DAD9D500E4E3CF000000C900E4E3CE00D4D3CE00D1D0CE00D1D0CE00D1D0
      CE00D0CFCD00DAD9D700EFEFEE007E7B79000000000061616100D0D0D000A2A2
      A200898989006060600061616100626262006262620062626200626262006262
      620062626200626262006464640091919100000000000000000000000000DDDE
      DE007CADC40078BADE006AABCF006290A800E6E6E60000000000000000008688
      8200E3B3E200CB96C600AE7DCE0000000000B57C0900FFFFFF008B898800807E
      7C0083817F00BABAB700E9E8E700ACABAA00ADACAB00E9E8E700ADACAB00AFAE
      AD00AEADAC00ABABAB00FFFFFF00B57B080084817F00E6E6E50095929000918F
      8D00F3F3EF00F3F2DE000000CB00F3F2DE00E3E2DD00E0DFDD00E0DFDD00E0DF
      DD00E1E0DE00F1F0EF00EAE9E900827F7D00000000000000000061616100D6D6
      D600636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005976
      96009AE2FF0086C7E80077B6D8006DAED0006983A00000000000000000000000
      0000C48BD900BF8AD3000000000000000000B57B0900FFFFFF00E6E6E500EBEA
      E800EAE9E700E5E4E200E2E1DF00E4E3E100E4E3E100E3E2DF00E4E3E100E6E5
      E200E5E4E200E2E2E100FFFFFF00B57B0800B1AFAD00B5B3B100E5E6E500F5F4
      F500FEFDFA00FFFFF1000000D300FFFFF100F8F7F200F5F4F200F5F4F200F5F4
      F200F5F4F200F7F7F500B1AFAD00B0AEAD000000000000000000000000006666
      66007A7A7A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000634
      6500AFF7FF0096D9F80086C8E7007FC1E00006285B0000000000000000000000
      000000000000000000000000000000000000B57C0900FFFFFF00807E7D007673
      720079777600AEADAC00DADAD900A1A09F00A1A09F00DADADA00A1A1A000A2A2
      A100A2A2A1009F9FA000FFFFFF00B57B090000000000989693008D8A88008B88
      86008E8A84009A9780000000DD009B9880008F8C83008D8983008C8983008C88
      84008B8885008C88860097959300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000E3F
      6F00226190005488B2007CB3D500679BBE000E2E5E0000000000000000000000
      000000000000000000000000000000000000B57C0B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B57C0B000000000000000000000000000000
      000000000000000000000000E100CCCCCC00CCCCCC00CCCCCC00DCDCDC000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001246
      7500307DAF004E8EBC00517FAC002D4B77001132610000000000000000000000
      000000000000000000000000000000000000B67F1000F5DDB700DC9D3700DC9E
      3900DC9F3A00DB9E3900DB9E3900DB9E3900DB9E3900DB9E3900DB9E3900DC9E
      3900DB9E3900DB9D3700F5DDB700B67F10000000000000000000000000000000
      000000000000000000000000E6000000E7000000E9000000EA004242E2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000395E
      86002577A9003C88BA003C7AA900224A75003958810000000000000000000000
      000000000000000000000000000000000000B8821600EFD09E00ECCD9900ECCD
      9900ECCD9A00ECCD9A00ECCD9A00ECCD9A00ECCD9A00ECCD9A00ECCD9A00ECCD
      9A00ECCD9900ECCD9900EFD09E00B88216000000000000000000000000000000
      000000000000000000000000E5000000E3000000D0000000BD000000E6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DFE5
      EC00265581001A588A001951820027507D00DFE5EC0000000000000000000000
      000000000000000000000000000000000000CCA35300B8821700B7811500B681
      1400B6811500B6811500B6811500B6811500B6811500B6811500B6811500B681
      1500B6811400B7811500B8821700CCA353000000000000000000000000000000
      000000000000000000000000E5000000E5000000E1000000DD004E4EEF000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000600000000100010000000000000300000000000000000000
      000000000000000000000000FFFFFF00FFFEFC3F000000000000000000000000
      0000000000000000000000000000000000000008000000000000000800000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E00700000000FFFFFFFFFFC1FFFEFFC1FFFF80000000
      FF80000080000000FF8000008000000000087FFE801C0000001C400280000000
      00087D6E80000000000040028001000000004002800300000000400080030000
      0000400000010000000000000001000000004002000100000000000000010000
      0000FFFF000100008001FFFF87C3000080008040DB6EC001000000000000C001
      000000000000DBFD000000008002DBFD000000000000C001000000000000DB05
      000000008002C001000000005B6CD005000000000000C001000000008002D005
      000000000000C001000000000000D005000000008002C001000000000000D005
      00000000DB6CDBFD000000000000C001FFC18241FFFFFFFF80000000FFC10000
      00000000FF80000000080000FF800000001C00000000000000080000001C0000
      0000000000000000000000000000000001900000000000000240000000000000
      000000000000000000000000000040000000000000002A000000000000000000
      000000000000FFFFFFFF00008001FFFFFC3FF87FFFFFFC008001801FFFFF0000
      8001801FFFFF00000000801FFFE700000000800FFFE300008000800700010000
      000084030000000000808001000080008100C0010000C0050101E0610001C005
      00E1E073FFE3C0058001E07FFFE7C0FD0001E07FFFFFC0250001E07FFFFFC005
      84C5E07FFFFFE1FD8001E07FFFFFF801FFFFF87F8001FC3FFFFF801F0000FC3F
      FFFF801F00000000E7FF801F00000000C7FF800F000000008000800700000000
      000080030000000000008001000000000000C001000000008000E06100000000
      C7FFE07300000000E7FFE07F00008001FFFFE07F0000FC1FFFFFE07F0000FC1F
      FFFFE07F0000FC1FFFFFE07F0000FC1F00000000000000000000000000000000
      000000000000}
  end
end
