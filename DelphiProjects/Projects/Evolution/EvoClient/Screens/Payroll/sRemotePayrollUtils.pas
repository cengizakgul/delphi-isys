// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sRemotePayrollUtils;

interface

uses
  db, classes, menus, SPackageEntry, evUtils, EvContext, EvCommonInterfaces, evExceptions;

function CreateCurrencyField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TCurrencyField;
function CreateFloatField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TFloatField;
function CreateIntegerField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TIntegerField;
function CreateStringField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string; aSize: integer): TStringField;

type
  TSelectedRec = record
    FieldName: string;
    Width: integer;
    Caption: string;
    ReadOnly: boolean;
  end;
  TSelectedRecArray = array of TSelectedRec;

function IndexOfSelectedField( sel: TSelectedRecArray; fname: string ): integer;

function ParseSelected( aSelected: TStrings): TSelectedRecArray; overload;
function ParseSelected( aSelectedText: string): TSelectedRecArray; overload;

function BuildSelected( aFieldName: string; aWidth: integer; aCaption: string; bRO: boolean = false): string;
procedure AddSelected( aSelected: TStrings; aFieldName: string; aWidth: integer; aCaption: string; bRO: boolean = false); overload;
procedure AddSelected( aSelected: TStrings; const sel:  TSelectedRec ); overload;
function ValidateFields( ds: TDataSet; s: string ): string;

//menu
function CountVisibleItems(Item: TMenuItem): integer;
function AddMenuItem( aParent: TMenuItem; aCaption: string; handler: TNotifyEvent; aVisible: boolean; aTag: integer ): TMenuItem;
function FindByTag( aParent: TMenuItem; tag: integer): TMenuItem;

function CountChar(s: string; c: char): integer;

procedure GotoChecks(comp: TComponent; aPrCheckNumber: integer);

function ValuesAreEqual( va1, va2: Variant; len: integer ): boolean;

function CheckRightsForContactsTab:boolean;

implementation

uses
  wwstr, sysutils, sdatastructure;

function CheckRightsForContactsTab:boolean;
begin
  Result := Context.UserAccount.AccountType in [uatServiceBureau, uatSBAdmin];
end;

function ValuesAreEqual( va1, va2: Variant; len: integer ): boolean;
var
  i: integer;
begin
  if len = 1 then
    Result := va1 = va2
  else
  begin
    Result := true;
//    Assert(VarArrayLowBound(va1, 1) = 0);
//    Assert(VarArrayHighBound(va1, 1) = len-1);
//    Assert(VarArrayLowBound(va2, 1) = 0);
//    Assert(VarArrayHighBound(va2, 1) = len-1);
    for i := 0 to len-1 do
      if va1[i] <> va2[i] then
      begin
        Result := false;
        break;
      end
  end;
end;

function CountChar(s: string; c: char): integer;
var
  i: integer;
begin
  Result := 0;
  for i := 1 to Length(s) do
    if s[i] = c then
      inc(Result);
end;

function IndexOfSelectedField( sel: TSelectedRecArray; fname: string ): integer;
var
  i: integer;
begin
  Result := -1;
  for i := low(sel) to high(sel) do
    if sel[i].FieldName = fname then
    begin
      Result := i;
      break;
    end
end;

function ParseSelected( aSelected: TStrings): TSelectedRecArray;
var
  i: integer;
  aPos: integer;
begin
  SetLength( Result, aSelected.Count );
  for i := low(Result) to high(Result) do
  begin
    aPos := 1;
    Result[i].FieldName := strGetToken( aSelected[i], #9, aPos );
    Result[i].Width := StrToInt(strGetToken( aSelected[i], #9, aPos ));
    Result[i].Caption := strGetToken( aSelected[i], #9, aPos );
    Result[i].ReadOnly := 'T' = strGetToken( aSelected[i], #9, aPos );
  end
end;

function ParseSelected( aSelectedText: string): TSelectedRecArray;
var
  sl: TStringList;
begin
  sl := TStringList.Create;
  try
    sl.Text := aSelectedText;
    Result := ParseSelected( sl );
  finally
    FreeAndNil(sl);
  end;
end;

function BuildSelected( aFieldName: string; aWidth: integer; aCaption: string; bRO: boolean): string;
begin
  Result := aFieldName+#9+IntToStr(aWidth)+#9+aCaption+#9;
  if bRO then
    Result := Result + 'T'
  else
    Result := Result + 'F';
end;

procedure AddSelected( aSelected: TStrings; aFieldName: string; aWidth: integer; aCaption: string; bRO: boolean);
begin
  aSelected.Add( BuildSelected(aFieldName, aWidth, aCaption, bRO) );
end;

procedure AddSelected( aSelected: TStrings; const sel:  TSelectedRec );
begin
  AddSelected( aSelected, sel.FieldName, sel.Width, sel.Caption, sel.ReadOnly );
end;

function CreateCurrencyField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TCurrencyField;
var
  TC: TCurrencyField;
begin
  TC := TCurrencyField.Create(aDataSet);
  with TC do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;

  result := TC;
end;

function CreateFloatField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TFloatField;
var
  TF: TFloatField;
begin
  TF := TFloatField.Create(aDataSet);
  with TF do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;

  result := TF;
end;

function CreateIntegerField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string): TIntegerField;
var
  TI: TIntegerField;
begin
  TI := TIntegerField.Create(aDataSet);
  with TI do
  begin
    FieldName := aFieldName;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;

  result := TI;
end;

function CreateStringField(aDataSet: TDataSet; aFieldName: string; aDisplayLabel: string; aSize: integer): TStringField;
var
  TS: TStringField;
begin
  TS := TStringField.Create(aDataSet);
  with TS do
  begin
    FieldName := aFieldName;
    Size := aSize;
    DisplayLabel := aDisplayLabel;
    DataSet := aDataSet;
  end;
  result := TS;
end;

function CountVisibleItems(Item: TMenuItem): integer;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to Pred(Item.Count) do
    if Item.Items[i].Visible then
      Inc(Result);
end;

function AddMenuItem( aParent: TMenuItem; aCaption: string; handler: TNotifyEvent; aVisible: boolean; aTag: integer ): TMenuItem;
begin
  Result := TMenuItem.Create( aParent );
  Result.Caption := aCaption;
  Result.OnClick := handler;
  Result.Visible := aVisible;
  Result.Tag := aTag;
  aParent.Add( Result );
end;

function FindByTag( aParent: TMenuItem; tag: integer): TMenuItem;
var
  i: integer;
begin
  for i := 0 to aParent.Count-1 do
    if aParent.Items[i].tag = tag then
    begin
      Result := aParent.Items[i];
      exit;
    end;
  raise EevException.CreateFmt('Cannot find menu item with tag = %d',[tag]);
end;

function ValidateFields( ds: TDataSet; s: string ): string;
var
  i: integer;
  len: integer;
  fname: string;
begin
  Result := '';
  i := 1;
  len := Length(s);
  while i <= len do
  begin
    fname := ExtractFieldName(s, i);
    if Assigned(ds.FindField(fname)) then
      Result := Result + fname + ';';
  end;
  Delete(Result, length(Result), 1);
end;

function DigOutPackage( c: TComponent ): TFramePackageTmpl;
begin
  while assigned(c) and not (c is TFramePackageTmpl) do
    c := c.Owner;
  Result := c as TFramePackageTmpl;
  Assert( assigned(Result) );
end;

procedure GotoChecks(comp: TComponent; aPrCheckNumber: integer);
begin
  if aPrCheckNumber <> 0 then
  begin
    if DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger <> aPrCheckNumber then
    begin
      DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
      DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', aPrCheckNumber, []);
      DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
    end;
    DigOutPackage(comp).OpenFrame('TREDIT_CHECK');
  end;
end;


end.
