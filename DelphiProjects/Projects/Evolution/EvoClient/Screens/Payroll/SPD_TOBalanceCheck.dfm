object TOBalanceCheck: TTOBalanceCheck
  Left = 436
  Top = 278
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Negative Balance Checks'
  ClientHeight = 366
  ClientWidth = 476
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    476
    366)
  PixelsPerInch = 96
  TextHeight = 13
  object bvlFrame: TEvBevel
    Left = 8
    Top = 8
    Width = 467
    Height = 320
    Anchors = [akLeft, akTop, akRight, akBottom]
    Shape = bsFrame
  end
  object LabelHint: TevLabel
    Left = 10
    Top = 305
    Width = 456
    Height = 15
    Alignment = taCenter
    Anchors = [akLeft, akRight, akBottom]
    AutoSize = False
    Caption = 'Double-click on employee to go to employee'#39's check'
    Color = clInfoBk
    ParentColor = False
  end
  object gToaBalanceCheck: TevDBGrid
    Left = 12
    Top = 12
    Width = 456
    Height = 292
    DisableThemesInTitle = False
    Selected.Strings = (
      'EE_CUSTOM_NUMBER'#9'20'#9'Ee Code'#9'F'
      'CHECK_NUMBER'#9'10'#9'Serial Nbr'#9'F'
      'EE_NAME'#9'37'#9'Employee Name'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TTOBalanceCheck\gToaBalanceCheck'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsToaBalanceCheckResults
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnDblClick = gToaBalanceCheckDblClick
    PaintOptions.AlternatingRowColor = 14544093
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object btnDontContinue: TevBitBtn
    Left = 330
    Top = 332
    Width = 139
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Don'#39't Continue '
    Default = True
    ModalResult = 2
    TabOrder = 1
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
      9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
      DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
      DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
      DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
      91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
      999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
    NumGlyphs = 2
    Margin = 0
  end
  object btnContinue: TevBitBtn
    Left = 120
    Top = 332
    Width = 201
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Continue with Negative Balances'
    ModalResult = 1
    TabOrder = 2
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
      DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
      DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
      DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
      DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
      DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
      2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
      A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
      AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
    NumGlyphs = 2
    Margin = 0
  end
  object cdToaBalanceCheckResults: TevClientDataSet
    FieldDefs = <
      item
        Name = 'PR_CHECK_NBR'
        DataType = ftInteger
      end
      item
        Name = 'EE_CUSTOM_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'EE_NAME'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'CHECK_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'MESSAGE'
        DataType = ftString
        Size = 1024
      end>
    Left = 106
    Top = 56
    object cdToaBalanceCheckResultsPR_CHECK_NBR: TIntegerField
      FieldName = 'PR_CHECK_NBR'
    end
    object cdToaBalanceCheckResultsEE_CUSTOM_NUMBER: TStringField
      FieldName = 'EE_CUSTOM_NUMBER'
    end
    object cdToaBalanceCheckResultsEE_NAME: TStringField
      DisplayLabel = 'Employee Name'
      FieldName = 'EE_NAME'
      Size = 50
    end
    object cdToaBalanceCheckResultsCHECK_NUMBER: TIntegerField
      FieldName = 'CHECK_NUMBER'
    end
    object cdToaBalanceCheckResultsMESSAGE: TStringField
      FieldName = 'MESSAGE'
      Size = 1024
    end
  end
  object dsToaBalanceCheckResults: TevDataSource
    DataSet = cdToaBalanceCheckResults
    Left = 138
    Top = 56
  end
end
