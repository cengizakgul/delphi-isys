object dlgRedistrDBDT: TdlgRedistrDBDT
  Left = 397
  Top = 169
  BorderStyle = bsDialog
  Caption = 'Redistribute DBDT'
  ClientHeight = 449
  ClientWidth = 580
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    580
    449)
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 177
    Top = 191
    Width = 71
    Height = 13
    Caption = 'Total Hours:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object evLabel2: TevLabel
    Left = 177
    Top = 208
    Width = 80
    Height = 13
    Caption = 'Total Amount:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object evLabel3: TevLabel
    Left = 330
    Top = 191
    Width = 8
    Height = 13
    Alignment = taRightJustify
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object evLabel4: TevLabel
    Left = 312
    Top = 208
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = '0.00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object evLabel5: TevLabel
    Left = 246
    Top = 174
    Width = 5
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object evLabel6: TevLabel
    Left = 177
    Top = 174
    Width = 69
    Height = 13
    Caption = 'Description:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object evLabel7: TevLabel
    Left = 243
    Top = 158
    Width = 5
    Height = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object evLabel8: TevLabel
    Left = 177
    Top = 158
    Width = 55
    Height = 13
    Caption = 'ED Code:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object OKBtn: TButton
    Left = 180
    Top = 415
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 260
    Top = 415
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object evDBGrid1: TevDBGrid
    Left = 8
    Top = 8
    Width = 564
    Height = 145
    DisableThemesInTitle = False
    ControlType.Strings = (
      'DBDT;CustomEdit;evDBComboDlg1;F'
      'WorkersComp;CustomEdit;lkupWorkComp;F'
      'Job;CustomEdit;lkupJobs;F')
    Selected.Strings = (
      'DBDT'#9'16'#9'Dbdt'#9'F'
      'WorkersComp'#9'5'#9'WComp'#9'F'
      'Job'#9'16'#9'Job'#9'F'
      'Line_Item_Date'#9'10'#9'Line Item Date'#9'F'
      'Hours'#9'10'#9'Hours'#9#9
      'Amount'#9'10'#9'Amount'#9#9)
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TdlgRedistrDBDT\evDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Anchors = [akLeft, akTop, akRight]
    DataSource = evDataSource1
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgShowFooter, dgFooter3DCells, dgTrailingEllipsis, dgDblClickColSizing]
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = 14544093
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object evDBComboDlg1: TevDBComboDlg
    Left = 104
    Top = 136
    Width = 121
    Height = 21
    OnCustomDlg = evDBComboDlg1CustomDlg
    ShowButton = True
    Style = csDropDownList
    Picture.PictureMaskFromDataSet = False
    TabOrder = 3
    WordWrap = False
    UnboundDataType = wwDefault
  end
  object lkupWorkComp: TevDBLookupCombo
    Left = 159
    Top = 108
    Width = 143
    Height = 21
    HelpContext = 1508
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'Co_State_Lookup'#9'3'#9'Co_State_Lookup'#9'F'
      'WORKERS_COMP_CODE'#9'5'#9'WORKERS_COMP_CODE'
      'DESCRIPTION'#9'40'#9'DESCRIPTION')
    DataField = 'CO_WORKERS_COMP_NBR'
    DataSource = evDataSource1
    LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
    LookupField = 'CO_WORKERS_COMP_NBR'
    Style = csDropDownList
    TabOrder = 4
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  object lkupJobs: TevDBLookupCombo
    Left = 177
    Top = 76
    Width = 117
    Height = 21
    HelpContext = 1508
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'Description'#9'40'#9'Description')
    DataField = 'CO_JOBS_NBR'
    DataSource = evDataSource1
    LookupTable = DM_CO_JOBS.CO_JOBS
    LookupField = 'CO_JOBS_NBR'
    Style = csDropDownList
    DragMode = dmAutomatic
    TabOrder = 5
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    ShowMatchText = True
  end
  object evDataSource1: TevDataSource
    DataSet = evClientDataSet1
    OnDataChange = evDataSource1DataChange
    Left = 8
    Top = 160
  end
  object evClientDataSet1: TevClientDataSet
    AggregatesActive = True
    FieldDefs = <
      item
        Name = 'DBDT'
        DataType = ftString
        Size = 256
      end
      item
        Name = 'CO_WORKERS_COMP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_JOBS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_DIVISION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_BRANCH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_DEPARTMENT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TEAM_NBR'
        DataType = ftInteger
      end
      item
        Name = 'Hours'
        DataType = ftCurrency
      end
      item
        Name = 'Amount'
        DataType = ftCurrency
      end>
    OnCalcFields = evClientDataSet1CalcFields
    OnNewRecord = evClientDataSet1NewRecord
    Left = 40
    Top = 160
    object evClientDataSet1DBDT: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'DBDT'
      Size = 256
    end
    object evClientDataSet1WorkersComp: TStringField
      FieldKind = fkLookup
      FieldName = 'WorkersComp'
      LookupDataSet = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
      LookupKeyFields = 'CO_WORKERS_COMP_NBR'
      LookupResultField = 'WORKERS_COMP_CODE'
      KeyFields = 'CO_WORKERS_COMP_NBR'
      Size = 5
      Lookup = True
    end
    object evClientDataSet1Job: TStringField
      FieldKind = fkLookup
      FieldName = 'Job'
      LookupDataSet = DM_CO_JOBS.CO_JOBS
      LookupKeyFields = 'CO_JOBS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CO_JOBS_NBR'
      Size = 40
      Lookup = True
    end
    object evClientDataSet1CO_WORKERS_COMP_NBR: TIntegerField
      FieldName = 'CO_WORKERS_COMP_NBR'
    end
    object evClientDataSet1CO_JOBS_NBR: TIntegerField
      FieldName = 'CO_JOBS_NBR'
    end
    object evClientDataSet1CO_DIVISION_NBR: TIntegerField
      FieldName = 'CO_DIVISION_NBR'
    end
    object evClientDataSet1CO_BRANCH_NBR: TIntegerField
      FieldName = 'CO_BRANCH_NBR'
    end
    object evClientDataSet1CO_DEPARTMENT_NBR: TIntegerField
      FieldName = 'CO_DEPARTMENT_NBR'
    end
    object evClientDataSet1CO_TEAM_NBR: TIntegerField
      FieldName = 'CO_TEAM_NBR'
    end
    object evClientDataSet1Line_Item_Date: TDateField
      FieldName = 'Line_Item_Date'
    end
    object evClientDataSet1Hours: TCurrencyField
      FieldName = 'Hours'
    end
    object evClientDataSet1Amount: TCurrencyField
      FieldName = 'Amount'
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 64
    Top = 208
  end
end
