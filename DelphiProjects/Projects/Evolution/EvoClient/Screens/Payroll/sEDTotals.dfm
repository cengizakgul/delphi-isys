object EDTotals: TEDTotals
  Left = 0
  Top = 0
  Width = 900
  Height = 689
  TabOrder = 0
  object ScrollBox3: TScrollBox
    Left = 0
    Top = 0
    Width = 900
    Height = 689
    Align = alClient
    TabOrder = 0
    object evPanel6: TevPanel
      Left = 0
      Top = 0
      Width = 896
      Height = 685
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 8
      TabOrder = 0
      object pnlEDs: TisUIFashionPanel
        Left = 8
        Top = 8
        Width = 880
        Height = 669
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'pnlListOfChecks'
        Color = 14737632
        TabOrder = 0
        OnResize = pnlEDsResize
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Earnings and Deductions'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object pnlEDsBody: TevPanel
          Left = 12
          Top = 35
          Width = 657
          Height = 389
          BevelOuter = bvNone
          ParentColor = True
          TabOrder = 0
          object sbEDTotals: TScrollBox
            Left = 0
            Top = 0
            Width = 657
            Height = 389
            Align = alClient
            TabOrder = 0
            object Panel6: TevPanel
              Left = 0
              Top = 0
              Width = 653
              Height = 25
              Align = alTop
              BevelOuter = bvLowered
              ParentColor = True
              TabOrder = 0
              object cbEDSeparateByLineType: TevCheckBox
                Left = 8
                Top = 4
                Width = 177
                Height = 17
                Action = EDSeparateByLineType
                TabOrder = 0
              end
              object evCheckBox1: TevCheckBox
                Left = 205
                Top = 4
                Width = 177
                Height = 17
                Action = EDSeparateByCheckType
                TabOrder = 1
              end
            end
            object pnlEDTotalsBorder: TevPanel
              Left = 0
              Top = 25
              Width = 653
              Height = 360
              Align = alClient
              BevelOuter = bvLowered
              BorderWidth = 6
              TabOrder = 1
              object Splitter1: TevSplitter
                Left = 364
                Top = -4
                Height = 364
                Align = alNone
              end
              object evSplitter1: TevSplitter
                Left = 302
                Top = 7
                Height = 346
              end
              object pnlEDLeft: TevPanel
                Left = 7
                Top = 7
                Width = 295
                Height = 346
                Align = alLeft
                BevelOuter = bvNone
                BorderWidth = 2
                TabOrder = 0
                object fpLeft: TisUIFashionPanel
                  Left = 2
                  Top = 2
                  Width = 291
                  Height = 342
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 12
                  Color = 14737632
                  TabOrder = 0
                  OnResize = fpLeftResize
                  RoundRect = True
                  ShadowDepth = 8
                  ShadowSpace = 8
                  ShowShadow = True
                  ShadowColor = clSilver
                  TitleColor = clGrayText
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWhite
                  TitleFont.Height = -13
                  TitleFont.Name = 'Arial'
                  TitleFont.Style = [fsBold]
                  Title = 'Code'
                  LineWidth = 0
                  LineColor = clWhite
                  Theme = ttCustom
                  object pnlFPLeftBody: TevPanel
                    Left = 12
                    Top = 35
                    Width = 257
                    Height = 266
                    BevelOuter = bvNone
                    ParentColor = True
                    TabOrder = 0
                    object dgEDTotal: TevDBGrid
                      Left = 0
                      Top = 0
                      Width = 257
                      Height = 266
                      DisableThemesInTitle = False
                      IniAttributes.Enabled = False
                      IniAttributes.SaveToRegistry = False
                      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                      IniAttributes.SectionName = 'TEDTotals\dgEDTotal'
                      IniAttributes.Delimiter = ';;'
                      ExportOptions.ExportType = wwgetSYLK
                      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                      TitleColor = clBtnFace
                      FixedCols = 0
                      ShowHorzScrollBar = True
                      Align = alClient
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgNoLimitColSize, dgTrailingEllipsis, dgDblClickColSizing]
                      TabOrder = 0
                      TitleAlignment = taLeftJustify
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      TitleLines = 1
                      UseTFields = True
                      PaintOptions.AlternatingRowColor = 14544093
                      PaintOptions.ActiveRecordColor = clBlack
                      NoFire = False
                    end
                  end
                end
              end
              object evPanel1: TevPanel
                Left = 305
                Top = 7
                Width = 341
                Height = 346
                Align = alClient
                BevelOuter = bvNone
                BorderWidth = 2
                TabOrder = 1
                object fpRight: TisUIFashionPanel
                  Left = 2
                  Top = 2
                  Width = 337
                  Height = 342
                  Align = alClient
                  BevelOuter = bvNone
                  BorderWidth = 12
                  Color = 14737632
                  TabOrder = 0
                  OnResize = fpRightResize
                  RoundRect = True
                  ShadowDepth = 8
                  ShadowSpace = 8
                  ShowShadow = True
                  ShadowColor = clSilver
                  TitleColor = clGrayText
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWhite
                  TitleFont.Height = -13
                  TitleFont.Name = 'Arial'
                  TitleFont.Style = [fsBold]
                  Title = 'Employee'
                  LineWidth = 0
                  LineColor = clWhite
                  Theme = ttCustom
                  object pnlFPRightBody: TevPanel
                    Left = 12
                    Top = 35
                    Width = 302
                    Height = 266
                    BevelOuter = bvNone
                    ParentColor = True
                    TabOrder = 0
                    object dgEDDetail: TevDBGrid
                      Left = 0
                      Top = 0
                      Width = 302
                      Height = 266
                      DisableThemesInTitle = False
                      Selected.Strings = (
                        'EE_Custom_Number'#9'8'#9'EE Code'#9'F'
                        'Name'#9'18'#9'Employee Name'#9'F'
                        'Hours_Pieces'#9'13'#9'Hours/Pieces'#9'F'
                        'Amount'#9'9'#9'Amount'#9'F')
                      IniAttributes.Enabled = False
                      IniAttributes.SaveToRegistry = False
                      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                      IniAttributes.SectionName = 'TEDTotals\dgEDDetail'
                      IniAttributes.Delimiter = ';;'
                      ExportOptions.ExportType = wwgetSYLK
                      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                      TitleColor = clBtnFace
                      FixedCols = 0
                      ShowHorzScrollBar = True
                      Align = alClient
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgNoLimitColSize, dgTrailingEllipsis, dgDblClickColSizing]
                      TabOrder = 0
                      TitleAlignment = taLeftJustify
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'MS Sans Serif'
                      TitleFont.Style = []
                      TitleLines = 1
                      OnDblClick = dgEDDetailDblClick
                      OnKeyDown = dgEDDetailKeyDown
                      PaintOptions.AlternatingRowColor = 14544093
                      PaintOptions.ActiveRecordColor = clBlack
                      DefaultSort = '-'
                      NoFire = False
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object EDActionList: TevActionList
    Left = 64
    Top = 32
    object EDSeparateByLineType: TAction
      AutoCheck = True
      Caption = 'Display User Entries Separately'
      OnExecute = EDSeparateByLineTypeExecute
      OnUpdate = EDSeparateByLineTypeUpdate
    end
    object EDSeparateByCheckType: TAction
      AutoCheck = True
      Caption = 'Display Totals By Check Type'
      OnExecute = EDSeparateByCheckTypeExecute
      OnUpdate = EDSeparateByCheckTypeUpdate
    end
  end
end
