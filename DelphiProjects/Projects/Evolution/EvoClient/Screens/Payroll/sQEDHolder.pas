// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SQEDHolder;

interface

uses
   db, classes, gdystrset, SDataDictClient, EvContext, EvExceptions,
  evCommonInterfaces, EvUIUtils, EvClientDataSet;

type
  TEDFieldType = ( qftAmount, qftHours );

  TEDFieldDesc = record
    FieldName: string;
    Caption: string;
    FieldType: TEDFieldType;
    CL_E_DS_NBR: integer;
    CUSTOM_E_D_CODE_NUMBER: string;
    E_D_CODE_TYPE: string;
  end;

  PEDFieldDesc = ^TEDFieldDesc;
  TEDFieldDescs = array of TEDFieldDesc;

  TIntegerArray = array of integer;

  TEECheckRec = record
    PrCheckNbr: integer;
    PaymentSerialNumber: integer;
    CheckNumber: integer;
    MaxCheckLineUserGroup: integer;
  end;
  TEECheckRecs = array of TEECheckRec;
  PEECheckRec = ^TEECheckRec;

  TEEChecks = class
  private
    FEECheckRecs: TEECheckRecs;
    FLastEditedIdx: integer;
    function GetMaxCheckNumber: integer;
    function Find(prchecknbr: integer): integer;
    function Get(prchecknbr: integer): PEECheckRec;
  public
    constructor Create;
    procedure Add( prchecknbr: integer; payment_serial: integer );
    procedure CalculateCheckNumbers;
    function GetCheckNumber( prchecknbr: integer): Variant;
    function GetPrCheckNbr( checknumber: integer): Variant;
    function SuggestCheckNumber: integer;
    procedure PrCheckEdited( prchecknbr: integer );
    function Remove( prchecknbr: integer ): boolean;
    procedure UpdateMaxCheckLineUserGroup( prchecknbr: integer; usergroup: Variant );
  end;

  TEEChecksList = class
  private
    FEEChecks: TStringList; //list of EE_NBR
    function Get( eenbr: integer ): TEEChecks;
  public
    constructor Create;
    destructor Destroy; override;
    function GetAllPrCheckNbrs: TIntegerArray;
    procedure RemovePrChecks( invalid: TStringList);

    procedure Add( eenbr: integer; prchecknbr: integer; payment_serial: integer);
    procedure Remove( eenbr: integer; prchecknbr: integer );
    procedure CalculateCheckNumbers;
    procedure CalculateCheckNumbersForEE( eenbr: integer );
    procedure Clear;
    function GetCheckNumber( eenbr: integer; prchecknbr: integer): Variant;
    function GetPrCheckNbr( eenbr: integer; checknumber: integer): Variant;
    function SuggestCheckNumber( eenbr: integer ): integer;
    procedure PrCheckEdited( eenbr: integer; prchecknbr: integer );
    procedure UpdateMaxCheckLineUserGroup( eenbr: integer; prchecknbr: integer; usergroup: Variant );
    function GetNewCheckLineUserGroup( eenbr: integer; prchecknbr: integer): integer;
  end;

  TUserFields = record
    PrCheckLinesFields: TStringSet;
    EdFieldDescs: TEDFieldDescs;
  end;

  TPrCheckLinesFields = record
    User: TStringSet;
    ToLoad: string;
    ToLoadCount: integer;
    ToStore: string;
  end;

  TQEDHolder = class (TComponent) {to get FreeNotification}
  private
    FDBDT: TevClientDataSet; //external ref

    //These members are not changed during lifetime of FQED
    FPR_BATCH_NBR: Variant;
    FEDList: TStringList; //list of CL_E_D_S.CUSTOM_E_D_CODE_NUMBER; CL_E_DS_NBR is stored as Object
    FPrCheckLinesFields: TPrCheckLinesFields;

    FQED: TevClientDataSet;
    FEEChecksList: TEEChecksList;

    FInvalidatedChecks: TStringList;

    FIndexDefs: TIndexDefs;

    FOldValues: array of Variant;

    FEDListToUpdate: TStringList; //temporary object
    FValidChecks: TIntegerArray; //temporary object
    FOneCheck: integer; //temporary object
    FChecklinesToReload: TIntegerArray; //temporary object

    FLastCheckNbr: Integer;
    //these two fields are needed because GetBookmark doesn't work in BeforePost;
    //maybe I should add ID field to the QED dataset, fill it with unique subsequent values in AfterInsert
    //and use it instead of using bookmarks
    FOneCheckCreated: boolean; //communicate between Before and AfterPost
    FReloadCheckLines: boolean; //communicate between Before and AfterPost

    procedure FilterOutValidChecks( DataSet: TDataSet; var Accept: Boolean);
    procedure FilterAcceptOneCheck( DataSet: TDataSet; var Accept: Boolean);
    procedure FilterAcceptChecklinesToReload( DataSet: TDataSet; var Accept: Boolean);

    procedure BuildDataSet(uf: TUserFields);
    procedure FillDataSet( bInsertingNewCheck: boolean );
    procedure SetEventHandlers;
    procedure RemoveEventHandlers;
    procedure UpdateDataSet;

    procedure SetROField( fieldname: string; Value: Variant);
    function EDCodeFromNbr( aEDNbr: integer ): string; //returns '' if aEDNbr is not in FEDList
    procedure BuildEDListToUpdate;
    function AreUserFieldsUptodate(const userFields: TUserFields): boolean;
    procedure UpdateCheckNumbers;
    procedure NewCheckCreated( bmk: Variant );
    procedure UpdateNewCheck;
    procedure ReloadInvalidLines( bmk: Variant );
  private
    function GetQEDBookmark: Variant;
    procedure GotoQEDBookmark( vbmk: Variant );
    procedure GotoAndFreeQEDBookmark( vbmk: Variant );
  private

    procedure QEDEE_Custom_NumberChange(Sender: TField);

    procedure QEDCalcFields(DataSet: TDataSet);
    procedure QEDBeforeEdit(DataSet: TDataSet);

    procedure QEDBeforeInsert(DataSet: TDataSet);
    procedure QEDAfterInsert(DataSet: TDataSet);

    procedure QEDBeforeDelete(DataSet: TDataSet);

    procedure QEDBeforePost(DataSet: TDataSet);
    procedure QEDAfterPost(DataSet: TDataSet);

    procedure QEDBeforeScroll(DataSet: TDataSet);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    FIndexFields : String;  
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;

    procedure InvalidateAll;
    procedure InvalidateCheck( CheckNbr: integer );

    //must be called only from QEDGrid (otherwise QEDGrid.dsQED.Dataset wouldn't be updated)
    procedure EnsureIsValid( const userFields: TUserFields );
    function  IsNewCheckJustEntered: boolean;

    procedure SetDBDT( ds: TevClientDataSet );
    property QED: TevClientDataSet read FQED;
  end;

type
  TPrCheckLinesFieldDesc = record
    Caption: string;
    FieldName: string;
    FieldType: TFieldType;
    Width: integer;
    DisplayWidth: integer;
    LookupDataset: string;
    LookupResult: string;
    Hidden: boolean;
  end;
    PPrCheckLinesFieldDesc = ^TPrCheckLinesFieldDesc;

const
  PrCheckLinesFieldDescs: array [0..15] of TPrCheckLinesFieldDesc =
  (
  {
    (Caption: 'Division'; FieldName: 'CO_DIVISION_NBR';              FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_DIVISION'; LookupResult: 'CUSTOM_DIVISION_NUMBER'),
    (Caption: 'Branch'; FieldName: 'CO_BRANCH_NBR';                  FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_BRANCH'; LookupResult: 'CUSTOM_BRANCH_NUMBER'),
    (Caption: 'Department'; FieldName: 'CO_DEPARTMENT_NBR';          FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_DEPARTMENT'; LookupResult: 'CUSTOM_DEPARTMENT_NUMBER'),
    (Caption: 'Team'; FieldName: 'CO_TEAM_NBR';                      FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_TEAM'; LookupResult: 'CUSTOM_TEAM_NUMBER'),
  }
  {
    (Caption: 'Division'; FieldName: 'CO_DIVISION_NBR';              FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: ''; LookupResult: ''; Hidden: true),
    (Caption: 'Branch'; FieldName: 'CO_BRANCH_NBR';                  FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: ''; LookupResult: ''; Hidden: true),
    (Caption: 'Department'; FieldName: 'CO_DEPARTMENT_NBR';          FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: ''; LookupResult: ''; Hidden: true),
    (Caption: 'Team'; FieldName: 'CO_TEAM_NBR';                      FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: ''; LookupResult: ''; Hidden: true),
  }
    (Caption: 'Division'; FieldName: 'CO_DIVISION_NBR';              FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_DIVISION'; LookupResult: 'CUSTOM_DIVISION_NUMBER'; Hidden: true),
    (Caption: 'Branch'; FieldName: 'CO_BRANCH_NBR';                  FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_BRANCH'; LookupResult: 'CUSTOM_BRANCH_NUMBER'; Hidden: true),
    (Caption: 'Department'; FieldName: 'CO_DEPARTMENT_NBR';          FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_DEPARTMENT'; LookupResult: 'CUSTOM_DEPARTMENT_NUMBER'; Hidden: true),
    (Caption: 'Team'; FieldName: 'CO_TEAM_NBR';                      FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_TEAM'; LookupResult: 'CUSTOM_TEAM_NUMBER'; Hidden: true),

    (Caption: 'D/B/D/T'; FieldName: 'DBDT_CUSTOM_NUMBERS';           FieldType: ftString; Width: 80; DisplayWidth: 30 ),

    (Caption: 'Agency'; FieldName: 'CL_AGENCY_NBR';                  FieldType: ftInteger; Width: 0; DisplayWidth: 40; LookupDataSet: 'CL_AGENCY'; LookupResult: 'Agency_Name'),
    (Caption: 'Job'; FieldName: 'CO_JOBS_NBR';                       FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_JOBS'; LookupResult: 'DESCRIPTION'),
    (Caption: 'Line Item Date'; FieldName: 'LINE_ITEM_DATE';         FieldType: ftDate; Width: 0; DisplayWidth: 20),
    (Caption: 'Line Item End Date'; FieldName: 'LINE_ITEM_END_DATE'; FieldType: ftDate; Width: 0; DisplayWidth: 20),
    (Caption: 'Piece'; FieldName: 'CL_PIECES_NBR';                   FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CL_PIECES'; LookupResult: 'NAME'),
    (Caption: 'Rate Nbr'; FieldName: 'RATE_NUMBER';                  FieldType: ftInteger; Width: 0; DisplayWidth: 10),
    (Caption: 'Rate Of Pay'; FieldName: 'RATE_OF_PAY';               FieldType: ftCurrency; Width: 0; DisplayWidth: 10),
    (Caption: 'Shift'; FieldName: 'CO_SHIFTS_NBR';                   FieldType: ftInteger; Width: 0; DisplayWidth: 20; LookupDataSet: 'CO_SHIFTS'; LookupResult: 'NAME'),
    (Caption: 'State'; FieldName: 'EE_STATES_NBR';                   FieldType: ftInteger; Width: 0; DisplayWidth: 2; LookupDataSet: 'EE_STATES'; LookupResult: 'State_Lookup'),
    (Caption: 'SUI'; FieldName: 'EE_SUI_STATES_NBR';                 FieldType: ftInteger; Width: 0; DisplayWidth: 2; LookupDataSet: 'EE_STATES'; LookupResult: 'SUI_State_Lookup'),
    (Caption: 'Workers Comp'; FieldName: 'CO_WORKERS_COMP_NBR';      FieldType: ftInteger; Width: 0; DisplayWidth: 5; LookupDataSet: 'CO_WORKERS_COMP'; LookupResult: 'WORKERS_COMP_CODE')
  );

function FieldNameForGrid( const f: TPrCheckLinesFieldDesc ): string;
function TrimGridFieldName( fieldname: string ): string;
function IndexOfPrCheckLinesFieldDesc( fieldname: string ): integer;

{
  other PR_CHECK_LINES fields:
    AGENCY_STATUS	CHARACTER	No
    E_D_DIFFERENTIAL_AMOUNT	NUMERIC(18,6)	No
    EE_SCHEDULED_E_DS_NBR	INTEGER	No
    PR_MISCELLANEOUS_CHECKS_NBR	INTEGER	No
    DEDUCTION_SHORTFALL_CARRYOVER	NUMERIC(18,6)	No
    FILLER	VARCHAR(512)	No
    USER_OVERRIDE	CHARACTER	Yes
}


implementation

uses
  sysutils, sdatastructure, variants, evutils, sRemotePayrollUtils,
  Math, evtypes, evconsts, strutils, SPayrollScr, typinfo, evDeferredCall, SPD_DBDTLookups;

function FieldNameForGrid( const f: TPrCheckLinesFieldDesc ): string;
begin
  Result := f.FieldName;
  if f.LookupDataset <> '' then
    Result := f.FieldName + '_DESC';
end;

function TrimGridFieldName( fieldname: string ): string;
begin
  if RightStr(fieldname, Length('_DESC')) = '_DESC' then
    Result := Copy(fieldname, 1, Length(fieldname) - Length('_DESC') )
  else
    Result := fieldname;
end;

function IndexOfPrCheckLinesFieldDesc( fieldname: string ): integer;
var
  i: integer;
begin
  Result := -1;
  for i := low(PrCheckLinesFieldDescs) to high(PrCheckLinesFieldDescs) do
    if fieldname = PrCheckLinesFieldDescs[i].FieldName then
    begin
      Result := i;
      break;
    end;
end;

{ TQEDHolder }

procedure TQEDHolder.BuildDataSet(uf: TUserFields);
  procedure AddFloatFieldDef( fname: string );
  begin
    with QED.FieldDefs.AddFieldDef do
    begin
      DataType := ftFloat;
      Name := fname;
    end;
    with QED.Aggregates.Add do
    begin
      AggregateName := fname;
      Expression := 'Sum(' + fname + ')';
      Active := True;
    end;
  end;
  procedure AddLookup( f: TPrCheckLinesFieldDesc );
  var
    ds: TevClientDataSet;
  begin
    ds := GetObjectProp(DM_CLIENT, f.LookupDataset, TevClientDataSet) as TevClientDataSet;
    with CreateStringField( QED, f.FieldName + '_DESC', '', ds.FieldByName(f.LookupResult).DataSize) do
    begin
      FieldKind := fkLookup;
      LookupDataSet := ds;
      LookupKeyFields := f.LookupDataset + '_NBR';
      LookupResultField := f.LookupResult;
      KeyFields := f.FieldName;
    end;
  end;
var
  i: integer;
  t: TStringSet;
  s: string;
begin
  Assert( QED = nil );
  Assert( FEDList.Count = 0 );

  for i := low(uf.EdFieldDescs) to high(uf.EdFieldDescs) do
    FEDList.AddObject( uf.EdFieldDescs[i].CUSTOM_E_D_CODE_NUMBER, TObject(uf.EdFieldDescs[i].CL_E_DS_NBR) );

  SetAssign( FPrCheckLinesFields.User, uf.PrCheckLinesFields );
  FPrCheckLinesFields.ToLoad := 'LineTypeSort;USER_GROUPING';
  FPrCheckLinesFields.ToLoadCount := 2;
  SetAssign( t, FPrCheckLinesFields.User );
  SetExclude( t, 'DBDT_CUSTOM_NUMBERS' );
  if Length(t) > 0 then
  begin
    FPrCheckLinesFields.ToLoad := FPrCheckLinesFields.ToLoad + ';' + SetToStr(t,';');
    FPrCheckLinesFields.ToLoadCount := FPrCheckLinesFields.ToLoadCount + Length(t);
  end;
  FPrCheckLinesFields.ToStore := 'USER_GROUPING';
  SetAssign( t, FPrCheckLinesFields.User );
  SetExclude( t, 'DBDT_CUSTOM_NUMBERS' );
  if Length(t) > 0 then
  begin
    FPrCheckLinesFields.ToStore := FPrCheckLinesFields.ToStore + ';' + SetToStr(t,';');
  end;

  FQED := TevClientDataSet.Create(nil);
  FQED.EnableVersioning := false;
  FQED.AggregatesActive := True;

  with QED.FieldDefs do
  begin
    Add('EE_NBR', ftInteger, 0, False);
    Add('Notes', ftString, 1, False);
    Add('PR_CHECK_NBR', ftInteger, 0, False);
    Add('PR_CHECK_LINES_NBR', ftInteger, 0, False);
    Add('CHECK_TYPE', ftString, 1, False);
//    Add('PAYMENT_SERIAL_NUMBER', ftInteger, 0, False);
    Add('Check_Number', ftInteger, 0, False);

    Add('LineTypeSort', ftString, 1, False);
    Add('USER_GROUPING', ftString, 1, False);

    Add('EE_Custom_Number', ftString, 10, False); //like PR_CHECK.EE_Custom_Number

    //EE fields copied from PR_CHECK
    Add('SOCIAL_SECURITY_NUMBER', ftString, 11, False);

    Add('CUSTOM_COMPANY_NUMBER', ftString, 20, False);
    Add('CUSTOM_DIVISION_NUMBER', ftString, 20, False);
    Add('CUSTOM_BRANCH_NUMBER', ftString, 20, False);
    Add('CUSTOM_DEPARTMENT_NUMBER', ftString, 20, False);
    Add('CUSTOM_TEAM_NUMBER', ftString, 20, False);

    Add('CUSTOM_COMPANY_NAME', ftString, 40, False);
    Add('CUSTOM_DIVISION_NAME', ftString, 40, False);
    Add('CUSTOM_BRANCH_NAME', ftString, 40, False);
    Add('CUSTOM_DEPARTMENT_NAME', ftString, 40, False);
    Add('CUSTOM_TEAM_NAME', ftString, 40, False);

    for i := low(PrCheckLinesFieldDescs) to high(PrCheckLinesFieldDescs) do
      if InSet( PrCheckLinesFieldDescs[i].FieldName, FPrCheckLinesFields.User ) then
        Add( PrCheckLinesFieldDescs[i].FieldName, PrCheckLinesFieldDescs[i].FieldType, PrCheckLinesFieldDescs[i].Width, false);

    for i := 0 to FEDList.Count-1 do
    begin
      AddFloatFieldDef( 'Amt_'+FEDList[i] );
      AddFloatFieldDef( 'Hrs_'+FEDList[i] );
      with AddFieldDef do
      begin
        DataType := ftInteger;
        Name := 'Nbr_'+FEDList[i]; //PR_CHECK_LINES_NBR will be stored here
      end;
    end;
  end;

  QED.CreateFields;
  QED.FieldDefs.Clear;
  SetLength( FOldValues, QED.FieldCount );

  for i := 0 to FEDList.Count-1 do
    (QED.FieldByName( 'Amt_'+FEDList[i] ) as TNumericField).DisplayFormat := '#,##0.00';

  if QED.FindField('DBDT_CUSTOM_NUMBERS') <> nil then
    QED.FieldByName('DBDT_CUSTOM_NUMBERS').FieldKind := fkCalculated;

  with CreateStringField( QED, 'Employee_Name_Calculate', 'Name', 40) do
  begin
    FieldKind := fkLookup;
    LookupDataSet := DM_EMPLOYEE.EE;
    LookupKeyFields := 'CUSTOM_EMPLOYEE_NUMBER';
    LookupResultField := 'Employee_Name_Calculate';
    KeyFields := 'EE_Custom_Number';
  end;

  for i := low(PrCheckLinesFieldDescs) to high(PrCheckLinesFieldDescs) do
    if PrCheckLinesFieldDescs[i].LookupDataset <> '' then
      if InSet( PrCheckLinesFieldDescs[i].FieldName, FPrCheckLinesFields.User ) then
        AddLookup( PrCheckLinesFieldDescs[i] );

  if DM_PAYROLL.PR_CHECK.IndexName = 'SORT' then
    s := ''
  else
    s := DM_PAYROLL.PR_CHECK.IndexFieldNames;
  if s <> '' then
    s := s + ';PR_CHECK_NBR;PR_CHECK_LINES_NBR'
  else
    s := 'PR_CHECK_NBR;PR_CHECK_LINES_NBR';

  QED.CreateDataSet;
  if FIndexDefs <> nil then
  begin
    s := ValidateFields( QED, FIndexDefs.Find('SORT').Fields );
    if s <> '' then
    begin
      FIndexFields := s;
      QED.IndexDefs.Assign(FIndexDefs);
      QED.IndexName := 'SORT';
    end;
    FreeAndNil( FIndexDefs );
  end
  else
  if FIndexFields <> '' then
    QED.IndexFieldNames := ValidateFields( QED, FIndexFields)
  else
  begin
    QED.IndexFieldNames := ValidateFields( QED, s );
    FIndexFields := QED.IndexFieldNames;
  end;

  QED.FieldByName('Notes').ReadOnly := True;
  QED.FieldByName('CHECK_TYPE').ReadOnly := True;

  QED.FieldByName('EE_Custom_Number').OnChange := QEDEE_Custom_NumberChange;

  if (QED.FindField('DBDT_CUSTOM_NUMBERS') <> nil) and (DBDTLevel >= 0){CO_NBR => -1} then
    QED.OnCalcFields := QEDCalcFields;
    
  QED.BeforeScroll := QEDBeforeScroll;

  SetEventHandlers;
end;

procedure TQEDHolder.SetEventHandlers;
begin
  QED.BeforeEdit := QEDBeforeEdit;

  QED.BeforeInsert := QEDBeforeInsert;
  QED.AfterInsert := QEDAfterInsert;

  QED.BeforeDelete := QEDBeforeDelete;

  QED.BeforePost := QEDBeforePost;
  QED.AfterPost := QEDAfterPost;
end;

constructor TQEDHolder.Create(Owner: TComponent);
begin
  FPR_BATCH_NBR := Null;

  FInvalidatedChecks := TStringList.Create;
  FInvalidatedChecks.Sorted := true;
  FInvalidatedChecks.Duplicates := dupIgnore;

  FEDList := TStringList.Create;
  FEDList.Sorted := true;
  FEDList.Duplicates := dupIgnore;

  FEDListToUpdate := TStringList.Create;
  FEDListToUpdate.Sorted := true;
  FEDListToUpdate.Duplicates := dupIgnore;

  FEEChecksList := TEEChecksList.Create;
end;

destructor TQEDHolder.Destroy;
begin
  InvalidateAll;
  inherited;
  FreeAndNil( FInvalidatedChecks );
  FreeAndNil( FEDList );
  FreeAndNil( FEDListToUpdate );
  FreeAndNil( FEEChecksList );
end;

procedure TQEDHolder.InvalidateCheck(CheckNbr: integer);
begin
  if assigned(QED) then
    FInvalidatedChecks.Add( inttostr(CheckNbr) );
  //deal with FEEChecksList later in UpdateDataset
end;

procedure TQEDHolder.InvalidateAll;
var
  i : integer;
begin
  if assigned(QED) then
  begin
    FreeAndNil( FIndexDefs );
    if QED.IndexName = 'SORT' then //save index generated by grid
    begin
      FIndexDefs := TIndexDefs.Create(nil);
      i := QED.IndexDefs.IndexOf('SORT');
      if i <> -1 then
         FIndexFields := QED.IndexDefs[i].Fields;
      QED.IndexDefs.Update;
      FIndexDefs.Assign( QED.IndexDefs );
    end;
  end;
  FEDList.Clear;
  FEEChecksList.Clear;
  FOldValues := nil;
  FreeAndNil( FQED );
  FInvalidatedChecks.Clear;
end;

function TQEDHolder.AreUserFieldsUptodate( const userFields: TUserFields): boolean;
var
  ueds: TStringSet;
  i: integer;
begin
  for i := low(userFields.edFieldDescs) to high(userFields.edFieldDescs) do
    SetInclude(ueds, userFields.edFieldDescs[i].CUSTOM_E_D_CODE_NUMBER);
  Result := Length(ueds) = FEDList.Count;
  if Result then
    for i := low(ueds) to high(ueds) do
      if FEDList.IndexOf(ueds[i]) = -1 then
      begin
        Result := false;
        break;
      end;
  Result := Result and SetEqual( FPrCheckLinesFields.User, userFields.PrCheckLinesFields );
end;

procedure TQEDHolder.EnsureIsValid( const userFields: TUserFields );
begin
  if FPR_BATCH_NBR <> DM_PAYROLL.PR_BATCH.PR_BATCH_NBR.Value then
    InvalidateAll;

  if not AreUserFieldsUptodate(userFields) then
    InvalidateAll;

  if QED = nil then
  begin
    BuildDataset(userFields);
    FillDataSet( false);
  end
  else
    UpdateDataSet;
end;

const
  sPrCheckEECalcFields = 'SOCIAL_SECURITY_NUMBER;'+
      'CUSTOM_COMPANY_NUMBER;CUSTOM_DIVISION_NUMBER;CUSTOM_BRANCH_NUMBER;CUSTOM_DEPARTMENT_NUMBER;CUSTOM_TEAM_NUMBER;'+
      'CUSTOM_COMPANY_NAME;CUSTOM_DIVISION_NAME;CUSTOM_BRANCH_NAME;CUSTOM_DEPARTMENT_NAME;CUSTOM_TEAM_NAME';
  sPrCheckFields = 'EE_Custom_Number;PR_CHECK_NBR;CHECK_TYPE;'+ sPrCheckEECalcFields; //PAYMENT_SERIAL_NUMBER

procedure TQEDHolder.FillDataSet( bInsertingNewCheck: boolean );
var
  QEDPrCheckLineValues: Variant;
  PrCheckLineValues: Variant;
  PR_CHECK: TPR_CHECK;
  PR_CHECK_LINES: TPR_CHECK_LINES;

  procedure NewQEDRow;
  begin
    if QED.State <> dsBrowse then
      QED.Post;

    if bInsertingNewCheck then
      QED.Insert
    else
      QED.Append;
    QEDPrCheckLineValues := Null;
    QED[sPrCheckFields] := PR_CHECK[sPrCheckFields]; //EE_NBR and Notes will be set by QEDNumberFieldChange
    QED['LineTypeSort'] := CHECK_LINE_TYPE_USER; //in case this check has no checklines
  end;
var
  EDCode: string;
  ro: Boolean;
begin
  PR_CHECK := DM_PAYROLL.PR_CHECK;
  PR_CHECK_LINES := DM_PAYROLL.PR_CHECK_LINES;

  FPR_BATCH_NBR := DM_PAYROLL.PR_BATCH.PR_BATCH_NBR.Value;

  RemoveEventHandlers;

  QED.DisableControls;
  SaveDSStates( [PR_CHECK, PR_CHECK_LINES] );
  PR_CHECK.LookupsEnabled := False;
  PR_CHECK_LINES.LookupsEnabled := False;

  QED.FieldByName('CHECK_TYPE').ReadOnly := false;
  try
    PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
    PR_CHECK_LINES.IndexFieldNames := 'PR_CHECK_NBR;' + FPrCheckLinesFields.ToLoad;

    PR_CHECK.First;
    PR_CHECK_LINES.First;
    Assert( not bInsertingNewCheck or (PR_CHECK.RecordCount=1) );
    while not PR_CHECK.EOF do
    begin
      FEEChecksList.Add( PR_CHECK.EE_NBR.AsInteger, PR_CHECK.PR_CHECK_NBR.AsInteger, PR_CHECK.PAYMENT_SERIAL_NUMBER.AsInteger );
      QEDPrCheckLineValues := Null;
      while (PR_CHECK_LINES.PR_CHECK_NBR.AsInteger < PR_CHECK.PR_CHECK_NBR.AsInteger) and
              not PR_CHECK_LINES.Eof do
          PR_CHECK_LINES.Next;

      while (PR_CHECK_LINES.PR_CHECK_NBR.AsInteger = PR_CHECK.PR_CHECK_NBR.AsInteger) and
              not PR_CHECK_LINES.Eof do
      begin
        FEEChecksList.UpdateMaxCheckLineUserGroup( PR_CHECK.EE_NBR.Value, PR_CHECK.PR_CHECK_NBR.Value, PR_CHECK_LINES.USER_GROUPING.AsVariant );

        EDCode := EDCodeFromNbr( PR_CHECK_LINES.CL_E_DS_NBR.AsInteger );
        if EDCode <> '' then
        begin
          PrCheckLineValues := PR_CHECK_LINES[FPrCheckLinesFields.ToLoad];
          if VarIsNull(QEDPrCheckLineValues)
             or not ValuesAreEqual(QEDPrCheckLineValues, PrCheckLineValues, FPrCheckLinesFields.ToLoadCount)
             or not QED.FieldByName('Nbr_'+EDCode).IsNull then
          begin
            NewQEDRow;
            QED[FPrCheckLinesFields.ToLoad] := PrCheckLineValues;
            QEDPrCheckLineValues := PrCheckLineValues;
          end;

          // if You are using property Value  than NULL = 0, it is wrong
          //QED['Hrs_'+EDCode] := PR_CHECK_LINES.HOURS_OR_PIECES.Value;
          //QED['Amt_'+EDCode] := PR_CHECK_LINES.AMOUNT.Value;

          QED['Hrs_'+EDCode] := PR_CHECK_LINES.HOURS_OR_PIECES.AsVariant;
          QED['Amt_'+EDCode] := PR_CHECK_LINES.AMOUNT.AsVariant;
          QED['Nbr_'+EDCode] := PR_CHECK_LINES.PR_CHECK_LINES_NBR.Value;
          QED['PR_CHECK_LINES_NBR'] := PR_CHECK_LINES.PR_CHECK_LINES_NBR.Value;
          ro := QED.FieldByName('EE_NBR').ReadOnly;
          QED.FieldByName('EE_NBR').ReadOnly := False;
          QED['EE_NBR'] := PR_CHECK.EE_NBR.AsInteger;
          QED.FieldByName('EE_NBR').ReadOnly := ro;
          if QED.FieldByName('LineTypeSort').IsNull then
          begin
            if (PR_CHECK_LINES['LINE_TYPE'] <> CHECK_LINE_TYPE_USER) and
               (PR_CHECK_LINES['LINE_TYPE'] <> CHECK_LINE_TYPE_DISTR_USER) and
               (Trim(PR_CHECK_LINES['LINE_TYPE']) <> '') then
              QED['LineTypeSort'] := CHECK_LINE_TYPE_SCHEDULED
            else
              QED['LineTypeSort'] := CHECK_LINE_TYPE_USER;
          end;
        end;
        PR_CHECK_LINES.Next;
      end;

      if not bInsertingNewCheck and VarIsNull(QEDPrCheckLineValues) then
        NewQEDRow;

      if QED.State <> dsBrowse then
        QED.Post;
      PR_CHECK.Next;
    end;

    FEEChecksList.CalculateCheckNumbers;
    UpdateCheckNumbers;
  finally
    QED.FieldByName('CHECK_TYPE').ReadOnly := True;

    PR_CHECK.LookupsEnabled := True;
    PR_CHECK_LINES.LookupsEnabled := True;
    LoadDSStates([PR_CHECK, PR_CHECK_LINES]);
    QED.EnableControls;
  end;
  SetEventHandlers;
end;

procedure TQEDHolder.UpdateDataSet;
var
  PR_CHECK_LINES: TPR_CHECK_LINES;
  PR_CHECK: TPR_CHECK;
begin
  if FInvalidatedChecks.Count <> 0 then
  begin
    PR_CHECK_LINES := DM_PAYROLL.PR_CHECK_LINES;
    PR_CHECK := DM_PAYROLL.PR_CHECK;
    if QED.State <> dsBrowse then
      QED.Cancel;

    RemoveEventHandlers; //I don't want event handlers to be restored in case of failure so I don't use try/finally here

    FEEChecksList.RemovePrChecks( FInvalidatedChecks );
    FInvalidatedChecks.Clear;
    FValidChecks := FEEChecksList.GetAllPrCheckNbrs;

    QED.DisableControls;
    try
      SaveDSStates([QED, PR_CHECK, PR_CHECK_LINES]);
      try
        QED.First;
        while not QED.Eof do
          if not InList( QED['PR_CHECK_NBR'], FValidChecks ) then
            QED.Delete
          else
            QED.Next;
        Assert( not assigned(PR_CHECK_LINES.OnFilterRecord) );
        Assert( not PR_CHECK_LINES.Filtered );
        Assert( not assigned(PR_CHECK.OnFilterRecord) );
        Assert( not PR_CHECK.Filtered );
        PR_CHECK_LINES.OnFilterRecord := FilterOutValidChecks; //uses FValidChecks
        PR_CHECK.OnFilterRecord := FilterOutValidChecks; //uses FValidChecks
        try
          PR_CHECK_LINES.Filtered := true;
          PR_CHECK.Filtered := true;
          FillDataSet( false );
        finally
          PR_CHECK_LINES.OnFilterRecord := nil;
          PR_CHECK.OnFilterRecord := nil;
        end;
      finally
        LoadDSStates([QED, PR_CHECK, PR_CHECK_LINES]);
      end;
    finally
      QED.EnableControls;
    end;
    SetEventHandlers;
  end
end;

procedure TQEDHolder.UpdateNewCheck;
var
  PR_CHECK_LINES: TPR_CHECK_LINES;
  PR_CHECK: TPR_CHECK;
begin
  Assert( FInvalidatedChecks.Count = 1 );
  Assert( FInvalidatedChecks[0] = inttostr(QED['PR_CHECK_NBR']) );

  PR_CHECK_LINES := DM_PAYROLL.PR_CHECK_LINES;
  PR_CHECK := DM_PAYROLL.PR_CHECK;

  if QED.State <> dsBrowse then
    QED.Cancel;

  FInvalidatedChecks.Clear;

  RemoveEventHandlers; //I don't want event handlers to be restored in case of failure so I don't use try/finally here

  QED.DisableControls;
  try
    SaveDSStates([QED, PR_CHECK, PR_CHECK_LINES]);
    try
      Assert( not assigned(PR_CHECK_LINES.OnFilterRecord) );
      Assert( not PR_CHECK_LINES.Filtered );
      Assert( not assigned(PR_CHECK.OnFilterRecord) );
      Assert( not PR_CHECK.Filtered );
      FOneCheck := QED['PR_CHECK_NBR'];
      PR_CHECK_LINES.OnFilterRecord := FilterAcceptOneCheck; //uses FOneCheck
      PR_CHECK.OnFilterRecord := FilterAcceptOneCheck; //uses FOneCheck
      try
        PR_CHECK_LINES.Filtered := true;
        PR_CHECK.Filtered := true;
        FillDataSet( true );
      finally
        PR_CHECK_LINES.OnFilterRecord := nil;
        PR_CHECK.OnFilterRecord := nil;
      end;
    finally
      LoadDSStates([QED, PR_CHECK, PR_CHECK_LINES]);
    end;
  finally
    QED.EnableControls;
  end;
  SetEventHandlers;
end;

procedure TQEDHolder.SetROField(fieldname: string; Value: Variant);
begin
  QED.FieldByName(fieldname).ReadOnly := False;
  QED.FieldByName(fieldname).AsVariant := Value;
  QED.FieldByName(fieldname).ReadOnly := True;
end;

procedure TQEDHolder.QEDEE_Custom_NumberChange(Sender: TField);
var
  v: Variant;
begin
  v := DM_EMPLOYEE.EE.Lookup('CUSTOM_EMPLOYEE_NUMBER', Sender.Value, 'EE_NBR;NOTES');
  SetROField( 'EE_NBR', v[0]);
  if not VarIsNull(v[1]) and (Trim(v[1]) <> '') then
    SetROField( 'Notes', 'Y')
  else
    SetROField( 'Notes', 'N');
  if QED.FieldByName('Check_Number').IsNull then
    QED['Check_Number'] := FLastCheckNbr;// FEEChecksList.SuggestCheckNumber( QED['EE_NBR'] );
end;

function TQEDHolder.EDCodeFromNbr(aEDNbr: integer): string;
var
  i: integer;
begin
  Result := '';
  for i := 0 to FEDList.Count-1 do
    if integer(FEDList.Objects[i]) = aEDNbr then
    begin
      Result := FEDList[i];
      break;
    end;
end;

procedure TQEDHolder.QEDBeforeEdit(DataSet: TDataSet);
var
  i: integer;
begin
  Assert( QED['LineTypeSort'] = CHECK_LINE_TYPE_USER );
  for i := 0 to high(FOldValues) do
    FOldValues[i] := QED.Fields[i].Value;
end;

procedure TQEDHolder.QEDBeforeInsert(DataSet: TDataSet);
var
  i: integer;
begin
  FLastCheckNbr := QED.FieldByName('Check_Number').Value;
  for i := 0 to high(FOldValues) do
    FOldValues[i] := Null;
end;

procedure TQEDHolder.QEDAfterInsert(DataSet: TDataSet);
begin
  QED['LineTypeSort'] := CHECK_LINE_TYPE_USER;
end;

procedure TQEDHolder.QEDBeforeDelete(DataSet: TDataSet);
var
  PR_CHECK_LINES: TPR_CHECK_LINES;
  PR_CHECK: TPR_CHECK;
  i: integer;
  saveFiltered: boolean;
  saveFilter: string;
begin
  PR_CHECK_LINES := DM_PAYROLL.PR_CHECK_LINES;
  PR_CHECK := DM_PAYROLL.PR_CHECK;

  InvalidateCheck( QED['PR_CHECK_NBR'] );
  SPayrollScr.AddCheckNbrToLists( QED['PR_CHECK_NBR'] ); // calls back InvalidateCheck once again
  DeferredCall( UpdateDataSet );

  for i := 0 to QED.FieldCount-1 do
    if LeftStr(QED.Fields[i].FieldName, Length('Nbr_')) = 'Nbr_' then
      if not QED.Fields[i].IsNull then
        if PR_CHECK_LINES.Locate( 'PR_CHECK_LINES_NBR', QED.Fields[i].Value, [] ) then
          PR_CHECK_LINES.Delete
        else
        begin
          // we have nbr of check line but it is not in PR_CHECK_LINES now
          // where could it be?
          // if it is deleted by someone else then we must get InvalidateCheck called
          //Assert(false); //just give up
        end;

//commented as John requested
//>You SHOULD NOT be able to delete a check from this view. You can be able
//>to delete check lines, but not a check.
//--removed comment
  saveFiltered := PR_CHECK_LINES.Filtered;
  saveFilter := PR_CHECK_LINES.Filter;
  try
    PR_CHECK_LINES.Filter := 'PR_CHECK_NBR='+QED.FieldByName('PR_CHECK_NBR').AsString;
    PR_CHECK_LINES.Filtered := true;
    if PR_CHECK_LINES.RecordCount = 0 then
      if PR_CHECK.Locate( 'PR_CHECK_NBR', QED['PR_CHECK_NBR'], []) then
        PR_CHECK.Delete; //else give up
  finally
    PR_CHECK_LINES.Filter := saveFilter;
    PR_CHECK_LINES.Filtered := saveFiltered;
  end;

end;

procedure TQEDHolder.QEDAfterPost(DataSet: TDataSet);
begin
  if FOneCheckCreated then
    DeferredCall( NewCheckCreated, GetQEDBookmark );
  if FReloadCheckLines then
    DeferredCall( ReloadInvalidLines, GetQEDBookmark );
end;

procedure TQEDHolder.BuildEDListToUpdate;
  function FieldIsChanged(fname: string): boolean;
  var
    v1, v2: Variant;
  begin
    v1 := FOldValues[ QED.FieldByName(fname).Index ];
    v2 := QED[FName];
    Result := not (VarIsNull(v1) and VarIsNull(v2) or (v1 = v2));
  end;
var
  i: integer;
  allexisting: boolean;
begin
  FEDListToUpdate.Clear;
  allexisting := false;
  for i := 0 to high(FOldValues) do
    if (IndexOfPrCheckLinesFieldDesc(QED.Fields[i].FieldName)<>-1) and
       FieldIsChanged(QED.Fields[i].FieldName) then
    begin
      allexisting := true;
      break;
    end;

  for i := 0 to FEDList.Count-1 do
    if FieldIsChanged('Amt_'+FEDList[i]) or
       FieldIsChanged('Hrs_'+FEDList[i]) or
       allexisting and not QED.FieldByName('Nbr_'+FEDList[i]).IsNull then
    begin
//      ODS( '<' + QED.FieldByName('Nbr_'+FEDList[i]).AsString + '>' );
      FEDListToUpdate.AddObject( FEDList[i], FEDList.Objects[i] );
    end;
end;

procedure TQEDHolder.QEDBeforePost(DataSet: TDataSet);
const
  sPrCheckEEFields = 'EE_NBR;'+ sPrCheckEECalcFields;
var
  PR_CHECK: TPR_CHECK;
  PR_CHECK_LINES: TPR_CHECK_LINES;
  PayrollScr: IevPayrollScreens;

  procedure NewCheck;
  begin
    PR_CHECK.Insert;
    try
      //QED['EE_NBR'] is set by QEDNumberFieldChange
      if DM_EMPLOYEE.EE.Locate('EE_NBR', QED['EE_NBR'], []) then
        PR_CHECK[sPrCheckEEFields] := DM_EMPLOYEE.EE[sPrCheckEEFields]; //!! Why I copy internalCalc (sPrCheckEECalcFields) fields? Just because the old grid is doing it this way
    except
      PR_CHECK.Cancel;
      raise EUpdateError.CreateHelp('Employee is required!', IDH_ConsistencyViolation);
    end;
    with ctx_PayrollCalculation do
      AssignCheckDefaults(0, GetNextPaymentSerialNbr);
    PR_CHECK.CHECK_TYPE.Value := CHECK_TYPE2_REGULAR;
    PR_CHECK.UpdateBlobData('NOTES_NBR', DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').AsString);
    PR_CHECK.Post;

    FEEChecksList.Add( PR_CHECK.EE_NBR.AsInteger, PR_CHECK.PR_CHECK_NBR.AsInteger, PR_CHECK.PAYMENT_SERIAL_NUMBER.AsInteger );
    FEEChecksList.CalculateCheckNumbersForEE( QED['EE_NBR'] );
    InvalidateCheck( PR_CHECK.PR_CHECK_NBR.AsInteger );

    ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
      DM_PAYROLL.PR,
      DM_PAYROLL.PR_BATCH,
      DM_PAYROLL.PR_CHECK,
      DM_PAYROLL.PR_CHECK_LINES,
      DM_PAYROLL.PR_CHECK_STATES,
      DM_PAYROLL.PR_CHECK_SUI,
      DM_PAYROLL.PR_CHECK_LOCALS,
      DM_PAYROLL.PR_CHECK_LINE_LOCALS,
      DM_PAYROLL.PR_SCHEDULED_E_DS,
      DM_CLIENT.CL_E_DS,
      DM_CLIENT.CL_PERSON,
      DM_CLIENT.CL_PENSION,
      DM_COMPANY.CO,
      DM_COMPANY.CO_E_D_CODES,
      DM_COMPANY.CO_STATES,
      DM_EMPLOYEE.EE,
      DM_EMPLOYEE.EE_SCHEDULED_E_DS,
      DM_EMPLOYEE.EE_STATES,
      DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
      DM_SYSTEM_STATE.SY_STATES,
      False,
      (DM_PAYROLL.PR_BATCH.FieldByName('PAY_SALARY').AsString = 'Y'),
      (DM_PAYROLL.PR_BATCH.FieldByName('PAY_STANDARD_HOURS').AsString = 'Y'),
      True
      );
    FOneCheckCreated := true; //to call DeferredCall( NewCheckCreated ) in AfterPost
  end;

  procedure PostED( edcode: string; clednbr: integer );
  var
   old_Ray_Of_Pay: Variant;
  begin
    if QED.FieldByName('Nbr_'+edcode).IsNull then //new checkline for current PR_CHECK
      PR_CHECK_LINES.Insert
    else
    begin
      if not PR_CHECK_LINES.Locate( 'PR_CHECK_LINES_NBR', QED['Nbr_'+edcode], []) then
        raise EUpdateError.CreateFmtHelp('Cannot find checkline with internal number <%s>', [VarToStr(QED['Nbr_'+edcode])], IDH_ConsistencyViolation);
      PR_CHECK_LINES.Edit;
    end;

    try
      if PR_CHECK_LINES.State = dsInsert then
      begin
        PR_CHECK_LINES.PR_CHECK_NBR.AsVariant := QED['PR_CHECK_NBR'];
        PR_CHECK_LINES.CL_E_DS_NBR.AsVariant := clednbr;
        PR_CHECK_LINES.LINE_TYPE.Value := CHECK_LINE_TYPE_USER;
      end;
      old_Ray_Of_Pay := PR_CHECK_LINES['RATE_OF_PAY'];
      PR_CHECK_LINES[FPrCheckLinesFields.ToStore] := QED[FPrCheckLinesFields.ToStore];
      if old_Ray_Of_Pay <>  PR_CHECK_LINES['RATE_OF_PAY'] then
         PR_CHECK_LINES.FieldByName('RATE_NUMBER').Clear;
      PR_CHECK_LINES.HOURS_OR_PIECES.AsVariant := QED['Hrs_'+edcode];
      PR_CHECK_LINES.AMOUNT.AsVariant := QED['Amt_'+edcode];
      PayrollScr.CheckToaBalance; //mimics behaviour of the old grid
      PR_CHECK_LINES.Post;
    except
      PR_CHECK_LINES.Cancel;
      raise;
    end;

    PR_CHECK_LINES.Locate( 'PR_CHECK_LINES_NBR', QED['Nbr_'+edcode], []);
    //read back from Pr Check Lines
    QED[FPrCheckLinesFields.ToLoad] := PR_CHECK_LINES[FPrCheckLinesFields.ToLoad];
    QED['Hrs_'+edcode] := PR_CHECK_LINES.HOURS_OR_PIECES.AsVariant;
    QED['Amt_'+edcode] := PR_CHECK_LINES.AMOUNT.AsVariant;
    QED['Nbr_'+edcode] := PR_CHECK_LINES.PR_CHECK_LINES_NBR.AsVariant; //for just inserted
  end;

var
  loaded: array of Variant;

  procedure FixLine;
    function FindMode: integer;
    var
      i, j: integer;
      imax: integer;
      counts: array of integer;
    begin
      SetLength( counts, FEDList.Count );
      imax := -1;
      for i := 0 to FEDList.Count-1 do
      begin
        counts[i] := 0;
        if not VarIsNull( loaded[i] ) then
        begin
          for j := 0 to FEDList.Count-1 do
            if not VarIsNull( loaded[j] ) then
              if ValuesAreEqual(loaded[i], loaded[j], FPrCheckLinesFields.ToLoadCount) then
                counts[i] := counts[i]+1;
          if imax = -1 then
            imax := i
          else
            if counts[i] > counts[imax] then
              imax := i;
        end;
      end;
      Assert( imax <> -1); // imax = -1 means that there is now checklines in this row at all => the row could not be invalid
      Result := imax;
    end;
  var
    imode: integer;
    i: integer;
    invalidcount: integer;
    edcode: string;
  begin
    imode := FindMode;

    QED[FPrCheckLinesFields.ToLoad] := loaded[imode];

    SetLength(FChecklinesToReload, FEDList.Count);
    invalidcount := 0;

    for i := 0 to FEDList.Count-1 do
      if not VarIsNull(loaded[i]) then
        if not ValuesAreEqual(loaded[i], loaded[imode], FPrCheckLinesFields.ToLoadCount) then
        begin
          edcode := FEDList[i];
          FChecklinesToReload[invalidcount] := QED['Nbr_'+edcode];
          QED['Hrs_'+edcode] := Null;
          QED['Amt_'+edcode] := Null;
          inc(invalidcount);
        end;
    SetLength( FChecklinesToReload, invalidcount );
    FReloadCheckLines := true; //to call DeferredCall( ReloadInvalidLines ) in AfterPost
  end;
var
  prchecknbr: Variant;
  i: integer;
  bAlreadyInvalidated: boolean;
  ifirst: integer;
begin
  PayrollScr := Context.GetScreenPackage(IevPayrollScreens) as IevPayrollScreens;

  FOneCheckCreated := false;
  FReloadCheckLines := false;

  PR_CHECK := DM_PAYROLL.PR_CHECK;
  PR_CHECK_LINES := DM_PAYROLL.PR_CHECK_LINES;

  if QED.FieldByName('EE_Custom_Number').AsString = '' then
    raise EUpdateError.CreateHelp('Employee can''t be empty', IDH_ConsistencyViolation);
  if QED.FieldByName('Check_Number').AsString = '' then
    raise EUpdateError.CreateHelp('Check Number can''t be empty', IDH_ConsistencyViolation);

  if QED.State = dsInsert then
  begin
    prchecknbr := FEEChecksList.GetPrCheckNbr( QED['EE_NBR'], QED['Check_Number']);
    if VarIsNull(prchecknbr) then
      NewCheck
    else
      PR_CHECK.Locate('PR_CHECK_NBR', prchecknbr, []);

    Assert( QED.FieldByName('EE_Custom_Number').AsString = PR_CHECK.FieldByName('EE_Custom_Number').AsString );

    QED.FieldByName('CHECK_TYPE').ReadOnly := false;
    try
      QED[sPrCheckFields] := PR_CHECK[sPrCheckFields];
    finally
      QED.FieldByName('CHECK_TYPE').ReadOnly := true;
    end;
    QED['USER_GROUPING'] := FEEChecksList.GetNewCheckLineUserGroup( QED['EE_NBR'], QED['PR_CHECK_NBR'] );
  end
  else
    PR_CHECK.Locate('PR_CHECK_NBR', QED['PR_CHECK_NBR'], []);

  if PR_CHECK_LINES.State <> dsBrowse then
    PR_CHECK_LINES.Cancel;

  //NewCheck calls InvalidateCheck; in other cases we don't need the check to be invalidated
  bAlreadyInvalidated := FInvalidatedChecks.IndexOf( QED.fieldByName('PR_CHECK_NBR').AsString ) <> -1;
//  ODS('QED.fieldByName(''PR_CHECK_NBR'').AsString = <%s>',[QED.fieldByName('PR_CHECK_NBR').AsString]);
  SPayrollScr.AddCheckNbrToLists( QED['PR_CHECK_NBR'] ); // calls back InvalidateCheck
  if not bAlreadyInvalidated then
    FInvalidatedChecks.Delete( FInvalidatedChecks.IndexOf( QED.fieldByName('PR_CHECK_NBR').AsString ) );

  FEEChecksList.PrCheckEdited( QED['EE_NBR'], QED['PR_CHECK_NBR'] );

  BuildEDListToUpdate;
  if FEDListToUpdate.Count > 0 then
  begin
    SetLength( loaded, FEDList.Count );
    for i := 0 to FEDList.Count-1 do
      loaded[i] := QED[FPrCheckLinesFields.ToLoad];
    ctx_DataAccess.CheckMaxAmountAndHours := False;
    try
      for i := 0 to FEDListToUpdate.Count-1 do
      begin
        if i = FEDListToUpdate.Count-1 then
          ctx_DataAccess.CheckMaxAmountAndHours := True;
        PostED( FEDListToUpdate[i], integer(FEDListToUpdate.Objects[i]) );

        (*
        Assume (but don't check here) that PR_CHECK fields aren't changed.
        *)
        loaded[FEDList.IndexOf(FEDListToUpdate[i])] := QED[FPrCheckLinesFields.ToLoad];
      end;

      ifirst := -1;
      for i := 0 to FEDList.Count-1 do
        if QED.FieldByName('Nbr_'+FEDList[i]).IsNull then
          loaded[i] := Null
        else
        begin
          if ifirst = -1 then
            ifirst := i
          else
            if not ValuesAreEqual(loaded[ifirst], loaded[i], FPrCheckLinesFields.ToLoadCount) then
            begin
              FixLine;
              break;
            end;
        end;
    finally
      ctx_DataAccess.CheckMaxAmountAndHours := True;
    end;
  end;
end;

procedure TQEDHolder.ReloadInvalidLines( bmk: Variant );
var
  PR_CHECK_LINES: TPR_CHECK_LINES;
  PR_CHECK: TPR_CHECK;
begin
  PR_CHECK_LINES := DM_PAYROLL.PR_CHECK_LINES;
  PR_CHECK := DM_PAYROLL.PR_CHECK;

  GotoQEDBookmark( bmk );

  if QED.State <> dsBrowse then
    QED.Cancel;

  //this check was not invalidated at all or already validated by UpdateNewCheck

  RemoveEventHandlers; //I don't want event handlers to be restored in case of failure so I don't use try/finally here

  QED.DisableControls;
  try
    SaveDSStates([QED, PR_CHECK, PR_CHECK_LINES]);
    try
      Assert( not assigned(PR_CHECK_LINES.OnFilterRecord) );
      Assert( not PR_CHECK_LINES.Filtered );
      Assert( not assigned(PR_CHECK.OnFilterRecord) );
      Assert( not PR_CHECK.Filtered );
      Assert( Length(FChecklinesToReload)>0 ); //FChecklinesToReload are already filled
      FOneCheck := QED['PR_CHECK_NBR'];
      PR_CHECK_LINES.OnFilterRecord := FilterAcceptChecklinesToReload; //uses FOneCheck and FChecklinesToReload
      PR_CHECK.OnFilterRecord := FilterAcceptOneCheck; //uses FOneCheck
      try
        PR_CHECK_LINES.Filtered := true;
        PR_CHECK.Filtered := true;
        FillDataSet( true );
      finally
        PR_CHECK_LINES.OnFilterRecord := nil;
        PR_CHECK.OnFilterRecord := nil;
      end;
      SetLength(FChecklinesToReload, 0); //just to be able make the assert above
    finally
      LoadDSStates([QED, PR_CHECK, PR_CHECK_LINES]);
    end;
    GotoAndFreeQEDBookmark( bmk );
  finally
    QED.EnableControls;
  end;
  SetEventHandlers;

end;



  {
  j: integer;
  s: string;

    if not ValuesAreEqual(QEDPrCheckLineValues, temp, FPrCheckLinesFields.ToLoadCount) then
    begin
      s := FPrCheckLinesFields.ToLoad + #13#10;
      for j := 0 to FPrCheckLinesFields.ToLoadCount-1 do
      begin
        if VarIsNull(temp[j]) then
          s := s + '<Null>'
        else
          s := s + '<'+VarToStr(temp[j])+'>';
        s := s + ' ? ';
        if VarIsNull(QEDPrCheckLineValues[j]) then
          s := s + '<Null>'
        else
          s := s + '<'+VarToStr(QEDPrCheckLineValues[j])+'>';
        s := s + #13#10;
      end;
      evMessage(s);
    end
  }

procedure TQEDHolder.UpdateCheckNumbers;
var
  bmk: TBookmark;
begin
  bmk := QED.GetBookmark;
  try
    try
      QED.First;
      while not QED.Eof do
      begin
        QED.Edit;
        QED['Check_Number'] := FEEChecksList.GetCheckNumber( QED['EE_NBR'], QED['PR_CHECK_NBR'] );
        QED.Post;
        QED.Next;
      end;
    finally
      QED.GotoBookmark(bmk);
    end;  
  finally
    QED.FreeBookmark( bmk );
  end;
end;

procedure TQEDHolder.RemoveEventHandlers;
begin
  QED.BeforeEdit := nil;

  QED.AfterInsert := nil;
  QED.BeforeInsert := nil;

  QED.BeforeDelete := nil;
  QED.AfterDelete := nil;

  QED.BeforePost := nil;
  QED.AfterPost := nil;
end;

procedure TQEDHolder.FilterOutValidChecks(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := not InList( DataSet['PR_CHECK_NBR'], FValidChecks );
end;

procedure TQEDHolder.FilterAcceptOneCheck(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := DataSet.FieldByName('PR_CHECK_NBR').AsInteger = FOneCheck;
end;

procedure TQEDHolder.FilterAcceptChecklinesToReload(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := (DataSet.FieldByName('PR_CHECK_NBR').AsInteger = FOneCheck){just optimization} and
    InList( DataSet['PR_CHECK_LINES_NBR'], FChecklinesToReload );
end;

procedure TQEDHolder.QEDCalcFields(DataSet: TDataSet);
begin
  QED['DBDT_CUSTOM_NUMBERS'] := FDBDT.Lookup( DBDTLeafField, QED[DBDTLeafField], 'DBDT_CUSTOM_NUMBERS')
end;

procedure TQEDHolder.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  if (Operation = opRemove) and (AComponent = FDBDT) then
    SetDBDT( nil);
  inherited;
end;

procedure TQEDHolder.SetDBDT(ds: TevClientDataSet);
begin
  FDBDT := ds;
end;

function TQEDHolder.IsNewCheckJustEntered: boolean;
begin
  Result := (QED.State = dsInsert)
    and not QED.FieldByName('EE_NBR').IsNull and not QED.FieldByName('Check_Number').IsNull
    and VarIsNull( FEEChecksList.GetPrCheckNbr( QED['EE_NBR'], QED['Check_Number'] ) );
end;

procedure TQEDHolder.NewCheckCreated(bmk: Variant);
begin
  GotoQEDBookmark( bmk );
  try
    UpdateNewCheck;
  finally
    GotoAndFreeQEDBookmark( bmk );
  end;
end;

procedure TQEDHolder.QEDBeforeScroll(DataSet: TDataSet);
begin
//  ODS('TQEDHolder.QEDBeforeScroll');
end;

function TQEDHolder.GetQEDBookmark: Variant;
begin
  Result := Integer(QED.GetBookmark);
end;

procedure TQEDHolder.GotoAndFreeQEDBookmark(vbmk: Variant);
var
  bmk: TBookmark;
begin
  bmk := TBookmark(Integer(vbmk));
  Assert( QED.BookmarkValid(bmk) );
  try
    QED.GotoBookmark(bmk);
  finally
    QED.FreeBookmark(bmk);
  end;
end;

procedure TQEDHolder.GotoQEDBookmark(vbmk: Variant);
var
  bmk: TBookmark;
begin
  bmk := TBookmark(Integer(vbmk));
  Assert( QED.BookmarkValid(bmk) );
  QED.GotoBookmark(bmk);
end;

{ TEEChecks }

procedure TEEChecksList.Add(eenbr, prchecknbr, payment_serial: integer);
var
  eekey: string;
  idx: integer;
begin
  eekey := inttostr(eenbr);
  idx := FEEChecks.IndexOf( eekey );
  if idx = -1 then
    idx := FEEChecks.AddObject(eekey, TEEChecks.Create );
  TEEChecks(FEEChecks.Objects[idx]).Add( prchecknbr, payment_serial );
end;

procedure TEEChecksList.CalculateCheckNumbers;
var
  i: integer;
begin
  for i := 0 to FEEChecks.Count - 1 do
    TEEChecks(FEEChecks.Objects[i]).CalculateCheckNumbers;
end;

procedure TEEChecksList.CalculateCheckNumbersForEE(eenbr: integer);
begin
  Get(eenbr).CalculateCheckNumbers;
end;

procedure TEEChecksList.Clear;
var
  i: integer;
begin
  for i := 0 to FEEChecks.Count-1 do
    FEEChecks.Objects[i].Free;
  FEEChecks.Clear;
end;

constructor TEEChecksList.Create;
begin
  FEEChecks := TStringList.Create;
  FEEChecks.Sorted := true;
  FEEChecks.Duplicates := dupIgnore;
end;

destructor TEEChecksList.Destroy;
begin
  Clear;
  FreeAndNil( FEEChecks );
  inherited;
end;

function TEEChecksList.GetCheckNumber(eenbr, prchecknbr: integer): Variant;
begin
  Result := Get(eenbr).GetCheckNumber(prchecknbr);
end;

procedure TEEChecksList.Remove(eenbr, prchecknbr: integer);
var
  eekey: string;
  idx: integer;
begin
  eekey := inttostr(eenbr);
  idx := FEEChecks.IndexOf( eekey );
  if idx <> -1 then
    TEEChecks(FEEChecks.Objects[idx]).Remove(prchecknbr)
end;

function TEEChecksList.GetPrCheckNbr(eenbr, checknumber: integer): Variant;
var
  eekey: string;
  idx: integer;
begin
  eekey := inttostr(eenbr);
  idx := FEEChecks.IndexOf( eekey );
  if idx <> -1 then
    Result := TEEChecks(FEEChecks.Objects[idx]).GetPrCheckNbr(checknumber)
  else
    Result := Null;
end;

constructor TEEChecks.Create;
begin
  FLastEditedIdx := -1;
end;

function TEEChecks.SuggestCheckNumber: integer;
begin
  if FLastEditedIdx <> -1 then
    Result := FEECheckRecs[FLastEditedIdx].CheckNumber
  else
    Result := GetMaxCheckNumber; //+ 1; // fixed by ticket# 44823
end;

function TEEChecks.GetMaxCheckNumber: integer;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to high(FEECheckRecs) do
    if FEECheckRecs[i].CheckNumber > Result then
      Result := FEECheckRecs[i].CheckNumber;
end;

procedure TEEChecks.PrCheckEdited(prchecknbr: integer);
var
  i: integer;
begin
  i := Find(prchecknbr);
  Assert(i <> -1);
  if i <> -1 then
    FLastEditedIdx := i;
end;

function TEEChecks.Remove( prchecknbr: integer ): boolean;
var
  i: integer;
  j: integer;
begin
  Result := false;
  i := Find(prchecknbr);
  if i <> -1 then
  begin
    if FLastEditedIdx = i then
      FLastEditedIdx := -1
    else if FLastEditedIdx > i then
      FLastEditedIdx := FLastEditedIdx - 1;
    for j := i+1 to high(FEECheckRecs) do
      FEECheckRecs[j-1] := FEECheckRecs[j];
    SetLength( FEECheckRecs, Length(FEECheckRecs)-1 );
    Result := true;
  end;
end;

procedure TEEChecks.UpdateMaxCheckLineUserGroup(prchecknbr: integer;
  usergroup: Variant);
var
  p: PEECheckRec;
begin
  if not VarIsNull(usergroup) then
  begin
    p := Get(prchecknbr);
    if p.MaxCheckLineUserGroup < usergroup then
      p.MaxCheckLineUserGroup := usergroup;
  end
end;

function TEEChecks.Find(prchecknbr: integer): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to high(FEECheckRecs) do
    if FEECheckRecs[i].PrCheckNbr = prchecknbr then
    begin
      Result := i;
      break;
    end
end;

function TEEChecks.Get(prchecknbr: integer): PEECheckRec;
var
  i: integer;
begin
  i := Find(prchecknbr);
  if i = -1 then
    raise EevException.CreateFmt('Cannot find info for PrCheckNbr = %d', [prchecknbr]);
  Result := @(FEECheckRecs[i]);
end;

{ TEEChecks }

procedure TEEChecks.CalculateCheckNumbers;
//!! should use check creation datetime?
  function f(psn: integer): integer;
  begin
    if psn >= 0 then
      Result := psn -100000000
    else
      Result := psn + 100000000;
  end;
var
  i: integer;
  minidx: integer;
  minval: integer;
  c: integer;
begin
  for i := 0 to high(FEECheckRecs) do
    FEECheckRecs[i].CheckNumber := -1;
  c := 0;

//  ODS( 'CalculateCheckNumbers: -----------------' );
  while c < Length(FEECheckRecs) do
  begin
    minval := MaxInt;
    minidx := -1; //to make compiler happy
    for i := 0 to high(FEECheckRecs) do
      if FEECheckRecs[i].CheckNumber = -1 then
        if f(FEECheckRecs[i].PaymentSerialNumber) < minval then //!!
        begin
          minidx := i;
          minval := f(FEECheckRecs[i].PaymentSerialNumber);
        end;
    inc(c);
    FEECheckRecs[minidx].CheckNumber := c;
//    ODS( 'CalculateCheckNumbers: %d => %d => %d',[FEECheckRecs[minidx].PaymentSerialNumber, f(FEECheckRecs[minidx].PaymentSerialNumber), FEECheckRecs[minidx].CheckNumber] );
  end;

  for i := 0 to high(FEECheckRecs) do
    Assert( FEECheckRecs[i].CheckNumber <> -1 );
end;

function TEEChecks.GetCheckNumber(prchecknbr: integer): Variant;
var
  i: integer;
begin
  Result := Null;
  i := Find(prchecknbr);
  if i <> -1 then
    Result := FEECheckRecs[i].CheckNumber;
end;

function TEEChecks.GetPrCheckNbr(checknumber: integer): Variant;
var
  i: integer;
begin
  Result := Null;
  for i := 0 to high(FEECheckRecs) do
    if FEECheckRecs[i].CheckNumber = checknumber then
    begin
      Result := FEECheckRecs[i].PrCheckNbr;
      break;
    end
end;

procedure TEEChecks.Add(prchecknbr, payment_serial: integer);
var
  idx: integer;
begin
  idx := Find(prchecknbr);
  if idx = -1 then
  begin
    SetLength(FEECheckRecs, Length(FEECheckRecs)+1);
    FEECheckRecs[high(FEECheckRecs)].PrCheckNbr := prchecknbr;
    FEECheckRecs[high(FEECheckRecs)].PaymentSerialNumber := payment_serial;
    FEECheckRecs[high(FEECheckRecs)].CheckNumber := -1; //just to init to impossible value
    FEECheckRecs[high(FEECheckRecs)].MaxCheckLineUserGroup := -1;
  end
  else
    Assert( FEECheckRecs[idx].PaymentSerialNumber = payment_serial );
end;

function TEEChecksList.SuggestCheckNumber(eenbr: integer): integer;
begin
  if FEEChecks.IndexOf(inttostr(eenbr)) <> -1 then
    Result := Get(eenbr).SuggestCheckNumber
  else
    Result := 1;
end;

function TEEChecksList.Get(eenbr: integer): TEEChecks;
var
  eekey: string;
begin
  eekey := inttostr(eenbr);
  Result := TEEChecks(FEEChecks.Objects[ FEEChecks.IndexOf( eekey ) ]);
end;

procedure TEEChecksList.PrCheckEdited(eenbr, prchecknbr: integer);
begin
  Get(eenbr).PrCheckEdited( prchecknbr );
end;

function TEEChecksList.GetAllPrCheckNbrs: TIntegerArray;
var
  k: integer;
  i: integer;
  j: integer;
  eechecks: TEEChecks;
begin
  k := 0;
  for i := 0 to FEEChecks.Count - 1 do
    k := k + Length(TEEChecks(FEEChecks.Objects[i]).FEECheckRecs);
  SetLength( Result, k );
  k := 0;
  for i := 0 to FEEChecks.Count - 1 do
  begin
    eechecks := TEEChecks(FEEChecks.Objects[i]);
    for j := low(eechecks.FEECheckRecs) to high(eechecks.FEECheckRecs) do
    begin
      Result[k] := eechecks.FEECheckRecs[j].PrCheckNbr;
      inc(k);
    end;
  end;
end;

procedure TEEChecksList.RemovePrChecks(invalid: TStringList);
var
  i: integer;
  j: integer;
  p: integer;
begin
  for i := 0 to invalid.Count-1 do
  begin
    p := strtoint(invalid[i]);
    for j := 0 to FEEChecks.Count - 1 do
      if TEEChecks(FEEChecks.Objects[j]).Remove( p ) then
        break;
  end
end;

procedure TEEChecksList.UpdateMaxCheckLineUserGroup(eenbr, prchecknbr: integer;
  usergroup: Variant);
begin
  Get(eenbr).UpdateMaxCheckLineUserGroup( prchecknbr, usergroup);
end;

function TEEChecksList.GetNewCheckLineUserGroup(eenbr,
  prchecknbr: integer): integer;
begin
  Result := Get(eenbr).Get(prchecknbr).MaxCheckLineUserGroup + 1;
  UpdateMaxCheckLineUserGroup( eenbr, prchecknbr, Result );
end;

end.


