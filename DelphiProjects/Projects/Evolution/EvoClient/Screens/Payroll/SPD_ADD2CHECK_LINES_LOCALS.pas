// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ADD2CHECK_LINES_LOCALS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_ADD_RECORDS, SDataStructure, Db, Wwdatsrc,  ActnList,
  StdCtrls, Buttons, Grids, Wwdbigrd, Wwdbgrid, ExtCtrls, Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, wwdblook, StdActns, EvUIComponents, SDDClasses,
  SDataDictclient, isUIwwDBLookupCombo, ISBasicClasses, isUIwwDBComboBox,
  LMDCustomButton, LMDButton, isUILMDButton, isUIFashionPanel;

type
  TADD2CHECK_LINES_LOCALS = class(TADD_RECORDS)
    DM_EMPLOYEE: TDM_EMPLOYEE;
    DM_PAYROLL: TDM_PAYROLL;
    cbCLOverride: TevDBComboBox;
    evDBComboBox1: TevDBComboBox;
    lcCLLocalName: TevDBLookupCombo;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoTransfer; override;
  end;

var
  ADD2CHECK_LINES_LOCALS: TADD2CHECK_LINES_LOCALS;

implementation

{$R *.DFM}

procedure TADD2CHECK_LINES_LOCALS.DoTransfer;
begin
  inherited;
  Dest.Insert;
  Dest['PR_CHECK_LINES_NBR'] := DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
  Dest['EE_LOCALS_NBR'] := Src['EE_LOCALS_NBR'];
  Dest['EXEMPT_EXCLUDE'] := Src['EXEMPT_EXCLUDE'];
  Dest.Post;
end;

initialization
  RegisterClass(TADD2CHECK_LINES_LOCALS);

end.
