// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_REDIT_PAYROLL;

interface

uses
  Windows,  SPD_REDIT_WIZARD_BASE, Dialogs, ComCtrls, StdCtrls,
  Buttons, ExtCtrls, wwdbdatetimepicker, DBCtrls, Wwdotdot, Wwdbcomb, Mask,
  wwdbedit, Grids, Wwdbigrd, Wwdbgrid, Controls, Db, Wwdatsrc, Classes,
  SDataStructure, EvUtils, SPackageEntry, SysUtils, isVCLBugFix,
  EvConsts, SPopulateRecords, ShellApi, ActnList, Messages, EvMAinboard,
   wwdblook, Menus, SSecurityInterface, EvTypes, EvSendMail, Forms,
  Wwdbspin, Variants, EvDataset,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  EvDataAccessComponents, ISDataAccessComponents, EvContext,
  sEDTotals, SDataDictclient, SDataDicttemp, EvInitApp, EvExceptions,
  DateUtils, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBDateTimePicker, isUIwwDBComboBox,
  isUIDBMemo, LMDCustomButton, LMDButton, isUILMDButton, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, ImgList, isUIEdit, isUIFashionPanel;

type
  TREDIT_PAYROLL = class(TREDIT_WIZARD_BASE)
    pcPayroll: TevPageControl;
    tsPayroll: TTabSheet;
    tsChecks: TTabSheet;
    tsEarnings: TTabSheet;
    tsTax: TTabSheet;
    wwDBGrid4: TevDBGrid;
    Splitter2: TevSplitter;
    wwDBGrid5: TevDBGrid;
    Panel3: TevPanel;
    Panel4: TevPanel;
    tsCompany: TTabSheet;
    Panel5: TevPanel;
    PR_LABEL: TevLabel;
    lblChecks: TevLabel;
    tsCoNotes: TTabSheet;
    CoNotesMemo: TEvDBMemo;
    CoNotesDS: TevDataSource;
    DM_TEMPORARY: TDM_TEMPORARY;
    DM_PAYROLL: TDM_PAYROLL;
    DM_COMPANY: TDM_COMPANY;
    CheckSrc: TevDataSource;
    cbCheckType: TevDBComboBox;
    cbCheckStatus: TevDBComboBox;
    tsEDExcl: TTabSheet;
    dsEDs: TevDataSource;
    SpeedButton1: TevSpeedButton;
    SpeedButton2: TevSpeedButton;
    CalcPopup: TevPopupMenu;
    Test1: TMenuItem;
    CalcClone: TevClientDataSet;
    BitBtn1: TevSpeedButton;
    lEeFilter: TLabel;
    tsTimeOff: TTabSheet;
    cdToaBalanceCheckResults: TevClientDataSet;
    dsToaBalanceCheckResults: TevDataSource;
    cdToaBalanceCheckResultsPR_CHECK_NBR: TIntegerField;
    cdToaBalanceCheckResultsEE_CUSTOM_NUMBER: TStringField;
    cdToaBalanceCheckResultsCHECK_NUMBER: TIntegerField;
    cdToaBalanceCheckResultsMESSAGE: TStringField;
    EDTotals: TEDTotals;
    tsContacts: TTabSheet;
    wwDBGrid2x: TevDBGrid;
    wwdsCoPhone: TevDataSource;
    lablClient: TevLabel;
    Label5: TevLabel;
    dbtClNumber: TevDBText;
    dbtCoNumber: TevDBText;
    dbtClName: TevDBText;
    dbtCoName: TevDBText;
    dsCL: TevDataSource;
    QuickPayrollBtn: TevSpeedButton;
    spbFavoriteReports: TevSpeedButton;
    sbmain: TScrollBox;
    pnlBorder: TevPanel;
    PageControlImages: TevImageList;
    ScrollBox1: TScrollBox;
    evPanel1: TevPanel;
    sbNotes: TScrollBox;
    evPanel3: TevPanel;
    pnlNotes: TisUIFashionPanel;
    pnlNotesBody: TevPanel;
    fpSummary: TisUIFashionPanel;
    evDBGrid1: TevDBGrid;
    fpDetail: TisUIFashionPanel;
    evLabel1: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    DBRadioGroup1: TevDBRadioGroup;
    sbChecks: TScrollBox;
    evPanel5: TevPanel;
    pnlListOfChecks: TisUIFashionPanel;
    pnlListOfChecksBody: TevPanel;
    wwDBGrid1: TevDBGrid;
    ScrollBox3: TScrollBox;
    evPanel6: TevPanel;
    pnlContacts: TisUIFashionPanel;
    pnlContactsBody: TevPanel;
    sbTax: TScrollBox;
    pnlTax: TevPanel;
    pnlTaxFashion: TisUIFashionPanel;
    pnlTaxBody: TevPanel;
    sbTimeOff: TScrollBox;
    evPanel7: TevPanel;
    pnlTimeOff: TisUIFashionPanel;
    pnlTimeOffBody: TevPanel;
    sbBlockEDs: TScrollBox;
    sbTaxInnerBody: TScrollBox;
    Panel7: TevPanel;
    cbSeperateOverrides: TevCheckBox;
    pnlTaxInnerBodyBorder: TevPanel;
    pnlTaxBodyLeft: TevPanel;
    evSplitter1: TevSplitter;
    pnlTaxBodyRight: TevPanel;
    fpTaxLeft: TisUIFashionPanel;
    pnlFPBodyLeft: TevPanel;
    fpTaxRight: TisUIFashionPanel;
    pnlFPBodyRight: TevPanel;
    btnSwipeClock: TevSpeedButton;
    btnMRCLogIn: TevSpeedButton;
    pnlFavoriteReport: TevPanel;
    evLabel47: TevLabel;
    pnlTopLeft: TevPanel;
    edSneakyField: TevEdit;
    evPanel2: TevPanel;
    bRunToaBalanceCheck: TevBitBtn;
    gToaBalanceCheck: TevDBGrid;
    LabelHint: TevLabel;
    pnlBrowsePayroll: TisUIFashionPanel;
    pnlBrowseBody: TevPanel;
    Panel2: TevPanel;
    btnOpenCompany: TevBitBtn;
    bOpenFilteredByDBDTG: TevBitBtn;
    bOpenFilteredByEe: TevBitBtn;
    sbListOfCompanies: TScrollBox;
    pnlListOfCompaniesBorder: TevPanel;
    fpListOfCompanies: TisUIFashionPanel;
    wwDBGrid7: TevDBGrid;
    edHiddenPayrollField: TEdit;
    pnlDetailsControls: TPanel;
    pnlPayrollInfo: TisUIFashionPanel;
    Label6: TevLabel;
    Label7: TevLabel;
    Label10: TevLabel;
    Label13: TevLabel;
    evLabel4: TevLabel;
    wwDBComboBox1: TevDBComboBox;
    DBMemo1: TEvDBMemo;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBRadioGroup2: TevDBRadioGroup;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evDBRadioGroup3: TevDBRadioGroup;
    evDBSpinEdit1: TevDBSpinEdit;
    pnlControls: TisUIFashionPanel;
    CreatePayrollBtn: TevBitBtn;
    SBReviewBtn: TevBitBtn;
    SubmitBtn: TevBitBtn;
    PreprocessBtn: TevBitBtn;
    DeletePayrollBtn: TevBitBtn;
    btnCopyPayroll: TevBitBtn;
    pnlList: TisUIFashionPanel;
    pnlListBody: TevPanel;
    Label9: TevLabel;
    btnOpenPayroll: TevBitBtn;
    dtPayrollAsOfDate: TevDBDateTimePicker;
    edHiddenField: TEdit;
    wwDBGrid6: TevDBGrid;
    procedure btnOpenCompanyClick(Sender: TObject);
    procedure pcPayrollChange(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
    procedure wwDBGrid5DblClick(Sender: TObject);
    procedure wwdsMasterPayrollDataChange(Sender: TObject; Field: TField);
    procedure cbSeperateOverridesClick(Sender: TObject);
    procedure btnOpenPayrollClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure CreatePayrollBtnClick(Sender: TObject);
    procedure DeletePayrollBtnClick(Sender: TObject);
    procedure wwDBGrid7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure wwDBGrid7DblClick(Sender: TObject);
    procedure wwDBGrid6DblClick(Sender: TObject);
    procedure wwDBGrid6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dtPayrollAsOfDateChange(Sender: TObject);
    procedure wwDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure wwDBGrid5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SubmitBtnClick(Sender: TObject);
    procedure PreprocessBtnClick(Sender: TObject);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure pcPayrollChanging(Sender: TObject; var AllowChange: Boolean);
    procedure CheckSrcDataChange(Sender: TObject; Field: TField);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure wwDBDateTimePicker1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CalcPopupPopup(Sender: TObject);
    procedure dsEDsDataChange(Sender: TObject; Field: TField);
    procedure btnCopyPayrollClick(Sender: TObject);
    procedure SBReviewBtnClick(Sender: TObject);
    procedure sbWizardClick(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure DBMemo1Change(Sender: TObject);
    procedure bOpenFilteredByDBDTGClick(Sender: TObject);
    procedure bOpenFilteredByEeClick(Sender: TObject);
    procedure wwDBComboBox1Change(Sender: TObject);
    procedure bRunToaBalanceCheckClick(Sender: TObject);
    procedure gToaBalanceCheckDblClick(Sender: TObject);
    procedure wwdsMasterStateChange(Sender: TObject);
    procedure QuickPayrollBtnClick(Sender: TObject);
    function CreateNewPayroll: Boolean;
    procedure tsPayrollShow(Sender: TObject);
    procedure spbFavoriteReportsClick(Sender: TObject);
    procedure pnlListResize(Sender: TObject);
    procedure btnSwipeClockClick(Sender: TObject);
    procedure btnMRCLogInClick(Sender: TObject);
    procedure dtPayrollAsOfDateCloseUp(Sender: TObject);
    procedure sbChecksConstrainedResize(Sender: TObject; var MinWidth,
      MinHeight, MaxWidth, MaxHeight: Integer);
  private
    btnOpenCompanyEnabled: Boolean;
    FBeforePost: TDataSetNotifyEvent;
    function GetSBName: string;
    procedure SetDetailPayroll(const Value: Boolean);
    function GetDetailPayroll: Boolean;
    procedure MenuCLickHdl(Sender: TObject);
    function GetNextCheckDate(AfterDate: TDateTime): TDateTime;
    procedure CheckOverrides;
    function PayrollIsOutOfRange: Boolean;
    procedure SetButtonsEnabled;
  protected
    procedure SetReadOnly(Value: Boolean); override;
    property DetailPayroll: Boolean read GetDetailPayroll write SetDetailPayroll;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetDataSetConditions(sName: string): string; override;
    procedure BeforePost(DataSet: TDataSet);
    procedure OnActivateParams(const AContext: Integer; const AParams: array of Variant); override;
  public
    procedure Activate; override;
    procedure Deactivate; override;
    procedure ShowDetailTabs(ShowTabs: boolean);
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure AfterClick(Kind: Integer); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure CheckWizardButton; override;
  end;

implementation

uses SProcessTCImport, SPD_DBDTP_Filter, sRemotePayrollUtils,
  SFrameEntry, SPD_TOBalanceCheck, EvCommonInterfaces, SPayrollScr;

{$R *.DFM}

procedure TREDIT_PAYROLL.btnOpenCompanyClick(Sender: TObject);
var
   bClReadOnly : Boolean;

begin
  inherited;
  if btnOpenCompanyEnabled then
  begin
    iWizardPosition := WIZ_UNKNOWN;
    CheckStatus;
  end;

  if Assigned(Sender) then
    SetDetailPayrollOpen(0);
  if Sender = btnOpenCompany then
    ctx_PayrollCalculation.SetEeFilter('', '');

  CheckSupportDatasets(wwdsList.DataSet.FieldByName('CL_NBR').AsInteger,
                       wwdsList.DataSet.FieldByName('CO_NBR').AsInteger);

  ApplySecurity;
  bClReadOnly := false;
  if ctx_DataAccess.GetClReadOnlyRight then
  begin
     if DM_CLIENT.CL.FieldByName('READ_ONLY').AsString = READ_ONLY_MAINTENANCE then
        EvErrMessage('Your account is set to Maintenance Hold.')
     else
        EvErrMessage('Your account is set to Read Only.');
     SetReadOnly(True);
     (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
     (Owner as TFramePackageTmpl).SetButton(NavDelete, False);
     bClReadOnly := True;
  end;

  if DM_COMPANY.CO.FieldByName('CREDIT_HOLD').AsString <> '' then
    case DM_COMPANY.CO.FieldByName('CREDIT_HOLD').AsString[1] of
      CREDIT_HOLD_LEVEL_LOW:
        EvMessage('Your account is past due! Please, contact Accounts Receivable.', mtInformation, [mbOK]);
      CREDIT_HOLD_LEVEL_MEDIUM:
        EvMessage('Your account is seriously past due! You will not be able to process payrolls until this issue is resolved. Please, contact Accounts Receivable.', mtWarning, [mbOK]);
      CREDIT_HOLD_LEVEL_HIGH:
      begin
        EvErrMessage('Due to outstanding A/R issues you no longer have access to your company. Please, contact Accounts Receivable immediately.');
        if ctx_AccountRights.Functions.GetState('UNLOCK_CREDIT_HOLD') <> stEnabled then
        begin
          ctx_DataAccess.OpenClient(0);
          DM_COMPANY.CO.ClientID := 0;
          AbortEx;
        end;
      end;
      CREDIT_HOLD_LEVEL_MAINT:
      begin
        EvErrMessage('Client is under maintenance and can not be accessed at this time');
        if CheckReadOnlyCompanyFunction then
        begin
          ctx_DataAccess.OpenClient(0);
          DM_COMPANY.CO.ClientID := 0;
          AbortEx;
        end;
      end;
      CREDIT_HOLD_LEVEL_READONLY:
      begin
         if (not bClReadOnly) and ctx_DataAccess.GetCoReadOnlyRight then
         begin
           EvErrMessage('Your account is set to Read Only.');
           SetReadOnly(True);
           (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
           (Owner as TFramePackageTmpl).SetButton(NavDelete, False);
         end;
      end;
    end;


  if btnOpenCompany.Enabled then
      if (DM_COMPANY.CO.RecordCount > 0) and
         (DM_COMPANY.CO.FieldByName('PAYROLL_PASSWORD').AsString <> '') then
        EvMessage('Payroll Password: ' + DM_COMPANY.CO.FieldByName('PAYROLL_PASSWORD').AsString);

  if pcPayroll.ActivePage = tsCompany then
  begin
    if not DetailPayroll then
    begin
      DM_PAYROLL.PR.DisableControls;
      try
        DM_PAYROLL.PR.RetrieveCondition := 'CHECK_DATE >= ''' + DateToStr(Current_FromDate) + '''';
        DM_PAYROLL.PR.Last;
        while not DM_PAYROLL.PR.Bof and
              (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PENDING) and
              (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_HOLD) and
              (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_BACKDATED) and
              (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_REQUIRED_MGR_APPROVAL) and
              (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PROCESSING) and
              (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PREPROCESSING) and
              (DM_PAYROLL.PR.FieldByName('STATUS').AsString <> PAYROLL_STATUS_COMPLETED) do
          DM_PAYROLL.PR.Prior;
      finally
        DM_PAYROLL.PR.EnableControls;
      end;
    end;

    wwdsList.DoDataChange(Self, nil);
    CoNotesMemo.LoadMemo;
    if (CurrentTabSheetName = '') or
       (CurrentTabSheetName = 'tsCompany') then
      if Length(Trim(CoNotesMemo.Lines.Text)) = 0 then
        pcPayroll.ActivatePage(tsPayroll)
      else
        pcPayroll.ActivatePage(tsCoNotes)
  end;
  lEeFilter.Caption := ctx_PayrollCalculation.GetEeFilterCaption;
  btnOpenCompany.Enabled := btnOpenCompanyEnabled or (ctx_PayrollCalculation.GetEeFilter <> '');
  bOpenFilteredByDBDTG.Enabled := btnOpenCompany.Enabled;
  bOpenFilteredByEe.Enabled := btnOpenCompany.Enabled;

  if wwdsList.DataSet.recordCount <= 0 then
  begin
    (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
    (Owner as TFramePackageTmpl).SetButton(NavDelete, False);
  end;
end;

procedure TREDIT_PAYROLL.ShowDetailTabs(ShowTabs: boolean);
begin
  if ShowTabs = False then
    if (pcPayroll.ActivePage <> tsCompany) and
       (pcPayroll.ActivePage <> tsCoNotes) then
      pcPayroll.ActivatePage(tsPayroll);

  pcPayroll.ActivePageIndex := pcPayroll.ActivePageIndex;  // Borland bug fix
  tsChecks.TabVisible := ShowTabs;
  tsEarnings.TabVisible := ShowTabs;
  tsTax.TabVisible := ShowTabs;
  tsEDExcl.TabVisible := ShowTabs;
  tsTimeOff.TabVisible := ShowTabs;

end;

procedure TREDIT_PAYROLL.Activate;
var
  I: integer;
  s: string;
  aDS: TArrayDS;
begin
  FBeforePost := DM_PAYROLL.PR.BeforePost;
  DM_PAYROLL.PR.BeforePost := BeforePost;

  SetButtonsEnabled;

  lEeFilter.Caption := ctx_PayrollCalculation.GetEeFilterCaption;
  with Owner as TFramePackageTmpl do
  begin
    aDS := EditDataSets;
    AddDS([DM_PAYROLL.PR_SCHEDULED_E_DS, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK], aDS);
    EditDataSets := aDS;
  end;
//  ctx_DataAccess.RecalcLineBeforePost := cbCalcCL.Checked;
  ctx_DataAccess.RecalcLineBeforePost := True;
  DM_PAYROLL.PR_CHECK_LINES.RetrieveCondition := '';
  DM_PAYROLL.PR_CHECK_LINE_LOCALS.RetrieveCondition := '';
  DM_PAYROLL.PR_CHECK_LOCALS.RetrieveCondition := '';
  DM_PAYROLL.PR_CHECK_STATES.RetrieveCondition := '';
  DM_PAYROLL.PR_CHECK_SUI.RetrieveCondition := '';

  ShowDetailTabs(DetailPayroll);

  tsContacts.TabVisible := CheckRightsForContactsTab();

  cbSeperateOverrides.Checked := SeparateTax;

  if CoNotesDS.Active then
  begin
    if (wwdsList.DataSet.FieldByName('CO_NBR').AsInteger <> CoNotesDS.DataSet.FieldByName('CO_NBR').AsInteger) or
       (wwdsList.DataSet.FieldByName('CL_NBR').AsInteger <> TevClientDataSet(CoNotesDS.DataSet).ClientID) then
      ctx_PayrollCalculation.SetEeFilter('','');
    wwdsList.DataSet.Locate('CL_NBR;CO_NBR', VarArrayOf([TevClientDataSet(CoNotesDS.DataSet).ClientID, CoNotesDS.DataSet['CO_NBR']]), []);
    if not btnOpenCompanyEnabled then
     begin
      btnOpenCompanyClick(nil);

      if DM_PAYROLL.PR_CHECK.Active and (ctx_PayrollCalculation.GetEeFilter <> '') then
        begin
         OpenDetailPayroll;
        end;
     end;
  end;

  inherited;

  if CalcForBatch then
  begin
    ResetCalc;
    CalcForBatch := False;
  end;

  EDTotals.Activate;

  wwDBGrid4.DataSource := dsTax_Total;
  wwDBGrid5.DataSource := dsTax_Detail;

  dtPayrollAsOfDate.Date := Current_FromDate;

  wwdsListDataChange(nil, nil);

  if btnOpenCompanyEnabled then
    pcPayroll.ActivatePage(tsCompany)
  else if iWizardPosition in [WIZ_TOTALS_AFTER_CHECKS, WIZ_TOTALS_AFTER_PREPROCESS] then
    pcPayroll.ActivatePage(tsPayroll)
  else if (iWizardPosition in [WIZ_PREPROCESSED]) and
          tsEarnings.TabVisible then
    pcPayroll.ActivatePage(tsEarnings)
  else if CurrentTabSheetName = '' then
    if Length(Trim(CoNotesMemo.Lines.Text)) = 0 then
      pcPayroll.ActivatePage(tsPayroll)
    else
      pcPayroll.ActivatePage(tsCoNotes)
  else
  begin
    s := CurrentTabSheetName;
    if Length(Trim(CoNotesMemo.Lines.Text)) = 0 then
      pcPayroll.ActivatePage(tsPayroll)
    else
      pcPayroll.ActivatePage(tsCoNotes);

    for I := 0 to pcPayroll.PageCount - 1 do
      if pcPayroll.Pages[I].Name = s then
      begin
        pcPayroll.ActivatePage(pcPayroll.Pages[I]);
        break;
      end;
  end;

  bNewPayroll := False;
  btnSwipeClock.Visible := VisibleSwipeClock;
  btnMRCLogIn.Visible := VisibleMyRecruitingCenter;
end;

procedure TREDIT_PAYROLL.pcPayrollChange(Sender: TObject);
begin
  inherited;
  //the following minheight constraint was causing odd behavior on other tab
  //sheets, so don't constrain it when the tsPayroll is not in focus.
  pnlList.Constraints.MinHeight := 0;
  if pcPayroll.ActivePage = tsPayroll then
     pnlList.Constraints.MinHeight := 186;
  CurrentTabSheetName := pcPayroll.ActivePage.Name;

  if pcPayroll.ActivePage = tsChecks then
  begin
    if not DM_PAYROLL.PR_CHECK.UseLookupCache then
      DM_PAYROLL.PR_CHECK.PrepareLookups;
  end
  else if pcPayroll.ActivePage.PageIndex < tsChecks.PageIndex then
      DM_PAYROLL.PR_CHECK.UnprepareLookups;

  if pcPayroll.ActivePage = tsEarnings then
    Calculate_ED_TOTALS(SeparateEDByLineType, SeparateEDByCheckType);


  if pcPayroll.ActivePage = tsTax then
    Calculate_TAX_TOTALS(cbSeperateOverrides.Checked);

  lblChecks.Visible := (pcPayroll.ActivePage = tsChecks) or (pcPayroll.ActivePage = tsEarnings) or
    (pcPayroll.ActivePage = tsTax);

  if (iWizardPosition = WIZ_PREPROCESSED) and
     (pcPayroll.ActivePage = tsEarnings) then
    iWizardPosition := WIZ_TOTALS_AFTER_PREPROCESS;

  BitBtn4.Visible := (pcPayroll.ActivePage <> tsCompany) and
                     (pcPayroll.ActivePage <> tsCoNotes) and
                     (pcPayroll.ActivePage <> tsPayroll);

  //GUI 2.0. THIS CANNOT BE DONE AT DFM LEVEL
  EDTotals.dgEDTotal.Columns[0].DisplayWidth := 10;
  EDTotals.dgEDTotal.Columns[1].DisplayWidth := 26;
  EDTotals.dgEDTotal.Columns[2].DisplayWidth := 11;
  EDTotals.dgEDTotal.Columns[3].DisplayWidth := 8;
  EDTotals.dgEDTotal.Columns[4].DisplayWidth := 6;    

  //Tax Browse Grid
  wwDBGrid4.Columns[0].DisplayWidth := 32;
  wwDBGrid4.Columns[1].DisplayWidth := 9;
 // wwDBGrid4.Columns[0].DisplayWidth := 21;
  //wwDBGrid4.Columns[1].DisplayWidth := 12;
  //wwDBGrid4.Columns[2].DisplayWidth := 12;

end;

procedure TREDIT_PAYROLL.wwDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  GotoChecks(Self, DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_PAYROLL.wwDBGrid5DblClick(Sender: TObject);
begin
  inherited;
  GotoChecks(Self, dsTax_Detail.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_PAYROLL.wwdsMasterPayrollDataChange(Sender: TObject;
  Field: TField);
begin
  if csLoading in ComponentState then
    Exit;
  inherited;
  btnOpenPayroll.Enabled := (wwdsMaster.DataSet.RecordCount > 0) and
                            (wwdsMaster.KeyValue <> DetailPayrollOpen);
  if Visible then
    ShowDetailTabs(not btnOpenCompanyEnabled and DetailPayroll and not btnOpenPayroll.Enabled);

   PreprocessBtn.Enabled := (wwdsMaster.DataSet.State = dsBrowse)
                  and (wwdsMaster.DataSet.FieldByName('PAYROLL_TYPE').AsString<>PAYROLL_TYPE_IMPORT)
                  and (not wwdsMaster.DataSet.IsEmpty)
             //    and (wwdsMaster.DataSet.FieldByName('PAYROLL_TYPE').AsString<>PAYROLL_TYPE_SETUP )
             //    and (wwdsMaster.DataSet.FieldByName('PAYROLL_TYPE').AsString<>PAYROLL_TYPE_BACKDATED)
             ;

end;

procedure TREDIT_PAYROLL.cbSeperateOverridesClick(Sender: TObject);
begin
  inherited;
  if SeparateTax <> cbSeperateOverrides.Checked then
  begin
    SeparateTax := cbSeperateOverrides.Checked;
    ReCalculate_TAX_TOTALS(cbSeperateOverrides.Checked);
  end;
end;

procedure TREDIT_PAYROLL.btnOpenPayrollClick(Sender: TObject);
begin
  inherited;
  CheckStatus;
  OpenDetailPayroll;
  wwdsMaster.DoDataChange(Self, nil);
  iWizardPosition := WIZ_UNKNOWN;
end;

procedure TREDIT_PAYROLL.BitBtn3Click(Sender: TObject);
begin
  inherited;

  CheckOverrides;

  if DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger <> DetailPayrollOpen then
    DetailPayroll := False;
  if not DetailPayroll then
    OpenDetailPayroll;
  (Owner as TFramePackageTmpl).OpenFrame('TREDIT_BATCH');
end;

function TREDIT_PAYROLL.GetNextCheckDate(AfterDate: TDateTime): TDateTime;
begin
  DM_PAYROLL.PR_SCHEDULED_EVENT.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
  with DM_PAYROLL.PR_SCHEDULED_EVENT do
  begin
    IndexFieldNames := 'SCHEDULED_CHECK_DATE;PR_SCHEDULED_EVENT_NBR';
    First;
    while (not EOF) and ((FieldByName('SCHEDULED_CHECK_DATE').AsDateTime <= AfterDate) or (not FieldByName('PR_NBR').IsNull)) do
      Next;
    if EOF then
    begin
      Result := 0;
      Exit;
      {raise ENoNextCheckDate.CreateHelp('There are no available entries in the calendar after ' + DateToStr(AfterDate) +
          '. Please, update it.', IDH_InconsistentData);}
    end;
    Result := FieldByName('SCHEDULED_CHECK_DATE').AsDateTime;
    IndexFieldNames := '';
  end;
end;

procedure TREDIT_PAYROLL.CreatePayrollBtnClick(Sender: TObject);
var
  aDate, aDate2: TDateTime;
begin
    aDate := ctx_PayrollCalculation.NextCheckDate(Date, True);
  if aDate <> 0 then
    aDate2 := GetNextCheckDate(aDate)
  else
    aDate2 := 0;
  if aDate2 = 0 then
  begin
    DM_PAYROLL.PR.Cancel;
    AutoBackToPreviosScreen := True;
    CameFrom := 'Payroll';
      if EvMessage('There is only one available calendar date remaining. You need to setup a new calendar before you can continue. Would you like to go to the calendar setup now? ', mtConfirmation, [mbYes, mbNo]) = mrYes then
      begin
        AutoBackToPreviosScreen := True;
        CameFrom := 'Payroll';
        PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Company - General - Calendar' + #0)), 0);
        Exit;
      end;
  end
  else
    (Owner as TFramePackageTmpl).ClickButton(NavInsert);

end;

procedure TREDIT_PAYROLL.DeletePayrollBtnClick(Sender: TObject);
begin
   (Owner as TFramePackageTmpl).ClickButton(NavDelete);
end;

procedure TREDIT_PAYROLL.wwDBGrid7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    btnOpenCompany.Click;
    Key := 0;
  end;
end;

procedure TREDIT_PAYROLL.wwDBGrid7DblClick(Sender: TObject);
begin
  inherited;
  if (wwDBGrid7.DataSource.DataSet.RecordCount > 0) and
    btnOpenCompanyEnabled and btnOpenCompany.Visible then btnOpenCompany.Click;
end;

procedure TREDIT_PAYROLL.wwDBGrid6DblClick(Sender: TObject);
begin
  inherited;
  if (wwDBGrid6.DataSource.DataSet.RecordCount > 0) and
    btnOpenPayroll.Enabled and btnOpenPayroll.Visible then btnOpenPayroll.Click;
end;

procedure TREDIT_PAYROLL.wwDBGrid6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    if (wwDBGrid6.DataSource.DataSet.RecordCount > 0) and
      btnOpenPayroll.Enabled and btnOpenPayroll.Visible then btnOpenPayroll.Click;
    Key := 0;
  end;
end;

procedure TREDIT_PAYROLL.dtPayrollAsOfDateChange(Sender: TObject);
begin
  inherited;
  if dtPayrollAsOfDate.Focused then
  begin
    DM_PAYROLL.PR.RetrieveCondition := 'CHECK_DATE >= ''' + DateToStr(dtPayrollAsOfDate.Date) + '''';
    Current_FromDate := dtPayrollAsOfDate.Date;
  end;
  SetButtonsEnabled;
end;

procedure TREDIT_PAYROLL.wwDBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    GotoChecks(Self, DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
    Key := 0;
  end;
end;

procedure TREDIT_PAYROLL.wwDBGrid5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    GotoChecks(Self, dsTax_Detail.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
    Key := 0;
  end;
end;

function TREDIT_PAYROLL.PayrollIsOutOfRange: Boolean;
var
  DaysPriorToCheckDate: Integer;
  AllNull: Boolean;
begin

  Result := False;

  if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_REGULAR then
    Exit;

  DaysPriorToCheckDate := 0;

  if not DM_COMPANY.CO.Active then
    DM_COMPANY.CO.DataRequired('');

  DM_COMPANY.CO.Locate('CO_NBR', DM_PAYROLL.PR.CO_NBR.Value, []);

  if not DM_COMPANY.CO.DAYS_PRIOR_TO_CHECK_DATE.IsNull then
  begin
    AllNull := False;
    DaysPriorToCheckDate := DM_COMPANY.CO.DAYS_PRIOR_TO_CHECK_DATE.AsInteger;
  end
  else
    AllNull := True;

  if AllNull and not DM_SERVICE_BUREAU.SB.DAYS_PRIOR_TO_CHECK_DATE.IsNull then
  begin
    AllNull := False;
    DaysPriorToCheckDate := DM_SERVICE_BUREAU.SB.DAYS_PRIOR_TO_CHECK_DATE.AsInteger;
  end;

  if not AllNull and (Trunc(Now) - Trunc(DM_PAYROLL.PR.CHECK_DATE.AsDateTime) >= DaysPriorToCheckDate) then
  begin
    Result := True;
  end;

end;

procedure TREDIT_PAYROLL.SubmitBtnClick(Sender: TObject);
var
  s: string;
  t: IevProcessPayrollTask;
  PrNbr: Integer;
  PayrollStatus: String;
  Q: IevQuery;
begin
  inherited;
  if not DM_PAYROLL.PR.IsEmpty then
  begin
    PrNbr := DM_PAYROLL.PR.PR_NBR.AsInteger;
    PayrollStatus := DM_PAYROLL.PR.STATUS.AsString;
    SaveDSStates([DM_PAYROLL.PR]);
    DM_PAYROLL.PR.Close;
    LoadDSStates([DM_PAYROLL.PR]);
    DM_PAYROLL.PR.Locate('PR_NBR', PrNbr, []);
    if (DM_PAYROLL.PR.STATUS.AsString <> PAYROLL_STATUS_PENDING) then
    begin
      EvErrMessage('Can not submit this payroll');
      AbortEx;
    end;

    if not TOBalanceCheck(ifrSubmitRemote, Self) then
      Exit;
    s := 'Are you sure you want to submit this payroll?';
    if EvMessage(s, mtConfirmation, mbOKCancel) <> mrOK then
      Exit;

    Q := TevQuery.Create('select c.SY_STATES_NBR, c.STATE, m.CUSTOM_COMPANY_NUMBER'+
      ' from CO_STATES c'+
      ' join CO_STATES x on x.SY_STATES_NBR=c.SY_STATES_NBR'+
      ' join CO m on m.CO_NBR=c.CO_NBR'+
      ' join CO n on n.CO_NBR=x.CO_NBR'+
      ' join CL_COMMON_PAYMASTER z on z.CL_COMMON_PAYMASTER_NBR=m.CL_COMMON_PAYMASTER_NBR'+
      ' where c.CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString+
      ' and x.CO_NBR<>c.CO_NBR'+
      ' and x.SY_STATE_DEPOSIT_FREQ_NBR<>c.SY_STATE_DEPOSIT_FREQ_NBR'+
      ' and m.CL_COMMON_PAYMASTER_NBR=n.CL_COMMON_PAYMASTER_NBR'+
      ' and z.COMBINE_STATE_SUI=''Y''' +
      ' and {AsOfNow<c>}'+
      ' and {AsOfNow<x>}'+
      ' and {AsOfNow<m>}'+
      ' and {AsOfNow<n>}'+
      ' and {AsOfNow<z>}');
    if not Q.Result.Eof then
    begin
      s := 'Consolidated company#'+Q.Result.FieldByName('CUSTOM_COMPANY_NUMBER').AsString +
           ' has different deposit frequency for same State '+ Q.Result.FieldByName('STATE').AsString +
        #13#10'Are you sure you want to submit this payroll?';
      if EvMessage(s, mtConfirmation, mbOKCancel) <> mrOK then
        Exit;
    end;

    Q := TevQuery.Create('select m.CUSTOM_COMPANY_NUMBER'+
      ' from CO n'+
      ' join CO m on n.CO_NBR<>m.CO_NBR'+
      ' join CL_COMMON_PAYMASTER z on z.CL_COMMON_PAYMASTER_NBR=n.CL_COMMON_PAYMASTER_NBR'+
      ' where n.CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString+
      ' and m.CO_NBR<>n.CO_NBR'+
      ' and m.FEDERAL_TAX_DEPOSIT_FREQUENCY<>n.FEDERAL_TAX_DEPOSIT_FREQUENCY'+
      ' and m.CL_COMMON_PAYMASTER_NBR=n.CL_COMMON_PAYMASTER_NBR'+
      ' and (z.COMBINE_MEDICARE=''Y'' or z.COMBINE_OASDI=''Y'' or z.COMBINE_FUI=''Y'')' +
      ' and {AsOfNow<m>}'+
      ' and {AsOfNow<n>}'+
      ' and {AsOfNow<z>}');
    if not Q.Result.Eof then
    begin
      s := 'Consolidated company#'+Q.Result.FieldByName('CUSTOM_COMPANY_NUMBER').AsString +
           ' has different deposit frequency for Federal Taxes '+
        #13#10'Are you sure you want to submit this payroll?';
      if EvMessage(s, mtConfirmation, mbOKCancel) <> mrOK then
        Exit;
    end;

    if ( TypeIsCorrectionRun(DM_PAYROLL.PR.PAYROLL_TYPE.AsString)
      or ( ctx_AccountRights.Functions.GetState('MARK_PAYROLL_COMPLETED') <> stEnabled )) then
    begin
      PayrollStatus := PAYROLL_STATUS_HOLD;
    end
    else
    if (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_REGULAR) and
    (ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_BACKDATED_PAYROLLS') <> stEnabled)
      and (Trunc(Now) - Trunc(DM_PAYROLL.PR.CHECK_DATE.AsDateTime) >= 0) then
    begin
      raise EInconsistentData.CreateHelp('You do not have the ability to submit a backdated payroll.', IDH_InconsistentData);
    end
    else
    if (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_REGULAR) and PayrollIsOutOfRange then
    begin
      if EvMessage('This payroll is out of range. If you proceed with this payroll it will need to be approved in the Operations-Approve Payroll screen. Do you wish to proceed?',
                 mtConfirmation, [mbYes, mbNo]) <> mrYes then
        Exit;

      PayrollStatus := PAYROLL_STATUS_BACKDATED;
    end
    else if ( TypeIsManagerApproval(DM_PAYROLL.PR.PAYROLL_TYPE.AsString)
              and (DM_COMPANY.CO.PAYROLL_REQUIRES_MGR_APPROVAL.Value = GROUP_BOX_YES)) then
    begin
      PayrollStatus := PAYROLL_STATUS_REQUIRED_MGR_APPROVAL;
    end
    else
    begin
      PayrollStatus := PAYROLL_STATUS_COMPLETED;
    end;

    if (PayrollStatus = PAYROLL_STATUS_COMPLETED) and
       (ctx_AccountRights.Functions.GetState('SEND_PAYROLL_DIRECTLY') = stEnabled) and
       (EvMessage('Process this payroll?', mtConfirmation, [mbYes,mbNo]) = mrYes) then
    begin
      DM_PAYROLL.PR.Edit;
      DM_PAYROLL.PR.STATUS_DATE.AsDateTime := SysTime;
      DM_PAYROLL.PR.STATUS.AsString := PayrollStatus;
      DM_PAYROLL.PR.Post;

      ctx_DataAccess.PayrollIsProcessing := True;
      try
        (Owner as TFramePackageTmpl).ClickButton(NavCommit);
      finally
        ctx_DataAccess.PayrollIsProcessing := False;
      end;

      t := mb_TaskQueue.CreateTask(QUEUE_TASK_PROCESS_PAYROLL, True) as IevProcessPayrollTask;
      t.Destination := rdtPrinter;
      t.PrList := IntToStr(ctx_DataAccess.ClientId) + ';' + DM_PAYROLL.PR.PR_NBR.AsString;
      t.Caption := t.Caption + 'Co:' + Trim(DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString)
        + ' Pr:' + DM_PAYROLL.PR.CHECK_DATE.AsString + '-' + DM_PAYROLL.PR.RUN_NUMBER.AsString;
      ctx_EvolutionGUI.AddTaskQueue(t);
      t := nil;
    end
    else
    begin
      DM_PAYROLL.PR.Edit;
      DM_PAYROLL.PR.STATUS_DATE.AsDateTime := SysTime;
      DM_PAYROLL.PR.STATUS.AsString := PayrollStatus;
      DM_PAYROLL.PR.Post;

      (Owner as TFramePackageTmpl).ClickButton(NavCommit);
      EvMessage('Thank you. Your payroll has been successfully submitted to ' +
        Trim(GetSBName) + ' for processing.', mtInformation, [mbOK]);
    end;
  end;
  iWizardPosition := WIZ_UNKNOWN;
end;

procedure TREDIT_PAYROLL.PreprocessBtnClick(Sender: TObject);
var
  t: IevPreprocessPayrollTask;
begin
  inherited;
  if (wwDBGrid6.DataSource.DataSet.RecordCount > 0) then
  begin
    if wwDBGrid6.DataSource.DataSet.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PENDING then
    begin
      EvErrMessage('Can''t pre-process this payroll');
      AbortEx;
    end;
    if not TOBalanceCheck(ifrPreProcessRemote, Self) then
      Exit;
    if EvMessage('Are you sure you want to pre-process this payroll?', mtConfirmation, mbOKCancel) <> mrOK then
      Exit;

    CheckStatus;

    t := mb_TaskQueue.CreateTask(QUEUE_TASK_PREPROCESS_PAYROLL, True) as IevPreprocessPayrollTask;
    try
      t.PrList := IntToStr(ctx_DataAccess.ClientID) + ';' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString + ' ';
      t.Caption := 'Co:' + Trim(DM_COMPANY.CO.FieldbyName('CUSTOM_COMPANY_NUMBER').AsString)
        + ' Pr:' + DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString + '-' + DM_PAYROLL.PR.FieldByName('RUN_NUMBER').AsString;
      ctx_EvolutionGUI.AddTaskQueue(t);
    finally
      t := nil;
    end;

    CloseDetailPayroll;
    wwdsMasterPayrollDataChange(nil, nil);
    pcPayroll.SetFocus;
  end;

  if tsEarnings.TabVisible then
    iWizardPosition := WIZ_PREPROCESSED;
end;

procedure TREDIT_PAYROLL.wwdsListDataChange(Sender: TObject;
  Field: TField);
begin
  if csLoading in ComponentState then
    Exit;
  inherited;
  btnOpenCompanyEnabled := not Assigned(CoNotesDS.DataSet) or not CoNotesDS.DataSet.Active or
                     (BitBtn1.Enabled and (wwdsList.DataSet.RecordCount <= 0)) or
                     (wwdsList.DataSet.RecordCount > 0) and
                     ((wwdsList.KeyValue <> CoNotesDS.DataSet.FieldByName('CO_NBR').AsInteger) or
                      (wwdsList.DataSet.FieldByName('CL_NBR').AsInteger <> TevClientDataSet(CoNotesDS.DataSet).ClientID));

  if not ctx_DataAccess.GetClCoReadOnlyRight then
  begin
    (Owner as TFramePackageTmpl).SetButton(NavInsert, not btnOpenCompanyEnabled);
    (Owner as TFramePackageTmpl).SetButton(NavDelete, not btnOpenCompanyEnabled);
  end;

  btnOpenCompany.Enabled := btnOpenCompanyEnabled or (ctx_PayrollCalculation.GetEeFilter <> '');
  bOpenFilteredByDBDTG.Enabled := btnOpenCompany.Enabled;
  bOpenFilteredByEe.Enabled := btnOpenCompany.Enabled;
  BitBtn1.Enabled := not btnOpenCompanyEnabled;
  BitBtn3.Enabled := not btnOpenCompanyEnabled and (DM_PAYROLL.PR.State = dsBrowse)
    and (not DM_PAYROLL.PR.IsEmpty);

  if not btnOpenCompanyEnabled then
  begin
   if (not (( Trim(DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString) <> '' ) and (DM_COMPANY.CO.FieldByName('PAY_FREQUENCIES').AsString[1] in
            [CO_FREQ_WEEKLY,CO_FREQ_BWEEKLY,CO_FREQ_SMONTHLY,CO_FREQ_MONTHLY,CO_FREQ_DAILY,CO_FREQ_QUARTERLY]))
       ) then
     QuickPayrollBtn.Enabled := False
   else
   if not GetIfReadOnly then
     QuickPayrollBtn.Enabled := True
   else
     QuickPayrollBtn.Enabled := False;
  end
  else
     QuickPayrollBtn.Enabled := False;

  if Visible then
    ShowDetailTabs(not btnOpenCompanyEnabled and DetailPayroll);
end;

procedure TREDIT_PAYROLL.SetDetailPayroll(const Value: Boolean);
begin
  if (DetailPayrollOpen <> 0) <> Value then
  begin
    ShowDetailTabs(Value);
    if Value then
      SetDetailPayrollOpen(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger)
    else
      SetDetailPayrollOpen(0);
    PR_LABEL.Caption := GetPayrollDescription;
  end;
end;

function TREDIT_PAYROLL.GetDetailPayroll: Boolean;
begin
  Result := DetailPayrollOpen <> 0;
end;

procedure TREDIT_PAYROLL.pcPayrollChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  AllowChange := (wwdsList.Dataset.recordcount >0);

  if btnOpenCompanyEnabled then
    btnOpenCompany.Click;
end;

procedure TREDIT_PAYROLL.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_COMPANY.CO, EditDataSets);
//  AddDS(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER, EditDataSets);
  AddDS(DM_PAYROLL.PR_SCHEDULED_EVENT, EditDataSets);
//  DM_PAYROLL.PR_SCHEDULED_EVENT.ParentDataSet := DM_PAYROLL.PR;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB, SupportDataSets, '');
  AddDS(DM_TEMPORARY.TMP_CO, SupportDataSets);
  AddDS(DM_COMPANY.CO_PHONE, EditDataSets);
end;

function TREDIT_PAYROLL.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR;
end;

procedure TREDIT_PAYROLL.AfterClick(Kind: Integer);
var
  Q: IevQuery;
begin
  inherited;
  if Kind = NavDelete then
  begin
    if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
    begin
      ctx_DBAccess.StartTransaction([dbtClient]);
      try
        Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
        Q.Execute;
        (Owner as TFramePackageTmpl).ClickButton(NavCommit);
        ctx_DBAccess.CommitTransaction;
      except
        ctx_DBAccess.RollbackTransaction;
        raise;
      end;
    end;
  end;

  if Kind = NavInsert then
  try
    bNewPayroll := True;
    DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value := ctx_PayrollCalculation.NextCheckDate(Date, True);
    if bInsertWithWizard then
    begin

      Generic_PopulatePRRecord(
        ctx_DataAccess.ClientID,
        DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger,
        DM_COMPANY.CO.FieldByName('NATURE_OF_BUSINESS').AsString);

      if DM_PAYROLL.PR.FieldByName('EXCLUDE_ACH').Value = 'N' then
      begin
        DM_PAYROLL.PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
        DM_PAYROLL.PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
        DM_PAYROLL.PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
        DM_PAYROLL.PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
        DM_PAYROLL.PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
      end;

      (Owner as TFramePackageTmpl).ClickButton(NavOK);
      (Owner as TFramePackageTmpl).ClickButton(NavCommit);
    end
    else
    begin
      DM_PAYROLL.PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
      DM_PAYROLL.PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
      DM_PAYROLL.PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
      DM_PAYROLL.PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
      DM_PAYROLL.PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
      DBMemo1.Show;
      DBMemo1.SetFocus;
    end;

    DM_SERVICE_BUREAU.SB.DataRequired('');

  except
    bNewPayroll := False;
    (Owner as TFramePackageTmpl).ClickButton(NavCancel);
    raise;
  end;
  if Kind = NavCommit then
  begin
    if bNewPayroll then
    begin
      CheckStatus;
      OpenDetailPayroll;
      iWizardPosition := WIZ_PAYROLL_CREATED;
      BitBtn3.Click;
    end;
  end;
  if (Kind = NavOK) and
     bNewPayroll then
    (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  if Kind in [NavOK, NavCommit, NavAbort] then
  begin
    CheckEEStatesForSUI;
  end;
end;

procedure TREDIT_PAYROLL.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
  aDate, aDate2: TDateTime;
  Nbr: Integer;
begin
  inherited;

  if Kind = NavInsert then
  begin
    if ctx_PayrollCalculation.GetEeFilter <> '' then
      raise EInconsistentData.CreateHelp('You can not create a payroll when company is opened with a filter', IDH_InconsistentData);
    aDate := ctx_PayrollCalculation.NextCheckDate(Date, True);
    if aDate <> 0 then
      aDate2 := GetNextCheckDate(aDate)
    else
      aDate2 := 0;
    if aDate2 = 0 then
    begin
      AutoBackToPreviosScreen := True;
      CameFrom := 'Payroll';
      Handled := True;
      if EvMessage('There is only one available calendar date remaining. You need to setup a new calendar before you can continue. Would you like to go to the calendar setup now? ', mtConfirmation, [mbYes, mbNo]) = mrYes then
      begin
        AutoBackToPreviosScreen := True;
        CameFrom := 'Payroll';
        PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Company - General - Calendar' + #0)), 0);
        Exit;
      end;
    end
  end;

  if Kind = NavDelete then
  begin
    bNewPayroll := False;
    if (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED) or
       (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED) then
    begin
      EvErrMessage('Can''t delete processed payroll');
      Handled := True;

    end
    else
      if ((DM_PAYROLL.PR.FieldByName('STATUS').AsString=PAYROLL_STATUS_COMPLETED)
       or (DM_PAYROLL.PR.FieldByName('STATUS').AsString=PAYROLL_STATUS_PROCESSING)) then
      begin
        evmessage('PR is in Process, cannot delete the PR.');
        Handled := True;
      end
    else
      begin
        CloseDetailPayroll;
        wwdsMasterPayrollDataChange(nil, nil);
        SetButtonsEnabled;
      end;
  end;
  if Kind = NavOK then
  begin
    Nbr := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
    if Current_FromDate > wwDBDateTimePicker1.Date then
      Current_FromDate := wwDBDateTimePicker1.Date;

    if (DM_PAYROLL.PR.State = dsEdit) and
      (VarCompareValue(DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').OldValue, PAYROLL_TYPE_IMPORT) = vrEqual) and
      (VarCompareValue(DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').OldValue, DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').NewValue) <> vrEqual) then
      raise EInconsistentData.CreateHelp('You can not change a payroll from type import to anything else.', IDH_InconsistentData);

    if (DM_PAYROLL.PR.State = dsInsert) and (wwDBComboBox1.Value <> '') and (wwDBComboBox1.Value[1] in [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT, PAYROLL_TYPE_IMPORT, PAYROLL_TYPE_TAX_ADJUSTMENT]) then
      raise EInconsistentData.CreateHelp('You can not create a payroll of this type manually.', IDH_InconsistentData);
    try
      DM_PAYROLL.PR_SCHEDULED_EVENT.DisableControls;
      DM_PAYROLL.PR.CheckBrowseMode;
    finally
      DM_PAYROLL.PR_SCHEDULED_EVENT.EnableControls;
    end;
    DM_PAYROLL.PR.DisableControls;
    try
      DM_PAYROLL.PR.RetrieveCondition := '';
      DM_PAYROLL.PR.RetrieveCondition := 'CHECK_DATE >= ''' + DateToStr(Current_FromDate) + '''';
      DM_PAYROLL.PR.Locate('PR_NBR', Nbr, []);
      dtPayrollAsOfDate.Date := Current_FromDate;
    finally
      DM_PAYROLL.PR.EnableControls;
    end;
  end;
  if Kind = NavRefresh then
    DetailPayrollOpen := 0;
  if Kind in [NavCancel] then
  begin
    if DM_PAYROLL.PR.State = dsInsert then
      bNewPayroll := False;
  end;
end;

procedure TREDIT_PAYROLL.CheckSrcDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then
    Exit;

  lblChecks.Caption := 'Checks: ' + IntToStr(DM_PAYROLL.PR_CHECK.RecordCount);
  SaveLastCheckPosition(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
  if DM_PAYROLL.PR_BATCH.Active then
    DM_PAYROLL.PR_BATCH.Locate('PR_BATCH_NBR', DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'], []);
end;

procedure TREDIT_PAYROLL.SetReadOnly(Value: Boolean);
begin
  inherited;
  btnOpenCompany.SecurityRO := False;
  btnOpenPayroll.SecurityRO := False;
  dtPayrollAsOfDate.SecurityRO := False;
end;

function TREDIT_PAYROLL.GetDataSetConditions(sName: string): string;
begin
  if wwdsList.DataSet.Active then
    Result := GetRemoteDataSetConditions(sName, wwdsList.DataSet.FieldbyName('CO_NBR').AsInteger)
  else
    Result := '';
end;

procedure TREDIT_PAYROLL.Deactivate;
begin
  DM_PAYROLL.PR.BeforePost := FBeforePost;
  inherited;
  if DM_PAYROLL.PR_CHECK.UseLookupCache then
      DM_PAYROLL.PR_CHECK.UnprepareLookups;
  if DM_PAYROLL.PR_CHECK.Active then
    if (pcPayroll.ActivePage = tsEarnings) and
            (dsED_Detail.DataSet.RecordCount > 0) then
      DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', dsED_Detail.DataSet.FieldByName('PR_CHECK_NBR').Value, [])
    else if (pcPayroll.ActivePage = tsTax) and
            (dsTax_Detail.DataSet.RecordCount > 0) then
      DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', dsTax_Detail.DataSet.FieldByName('PR_CHECK_NBR').Value, []);
  ctx_DataAccess.RecalcLineBeforePost := True;
//  DM_PAYROLL.PR_SCHEDULED_EVENT.ParentDataSet := DM_COMPANY.CO;
end;

procedure TREDIT_PAYROLL.BitBtn1Click(Sender: TObject);
begin
  inherited;
  bInsertWithWizard := True;
  try
    (Owner as TFramePackageTmpl).ClickButton(NavInsert);
  except
    bInsertWithWizard := False;
    raise;
  end;
  CheckSupportDatasets(wwdsList.DataSet.FieldByName('CL_NBR').AsInteger,
                       wwdsList.DataSet.FieldByName('CO_NBR').AsInteger);
end;

procedure TREDIT_PAYROLL.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  DM_PAYROLL.PR_SCHEDULED_E_DS.Insert;
  wwDBLookupCombo1.SetFocus;
end;

procedure TREDIT_PAYROLL.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  if EvMessage('Are you sure?', mtConfirmation, mbOKCancel) = mrOK then
  begin
    DM_PAYROLL.PR_SCHEDULED_E_DS.Delete;
    (Owner as TFramePackageTmpl).ClickButton(NavOK);
    (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  end;
end;

procedure TREDIT_PAYROLL.wwDBDateTimePicker1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  p: TPoint;
begin
  inherited;
  p := TWinControl(Sender).Parent.ClientToScreen(Point(TWinControl(Sender).Left + 1, TWinControl(Sender).Top + TWinControl(Sender).Height + 1));
  if (Key = Ord('Z')) and (Shift = [ssCtrl]) then
  begin
    CalcPopup.Popup(p.x, p.y);
    Key := 0;
  end;
end;

procedure TREDIT_PAYROLL.CalcPopupPopup(Sender: TObject);
var
  T2: TMenuItem;
  i: Integer;
begin
  inherited;
  while CalcPopup.Items.Count > 0 do
    CalcPopup.Items.Delete(0);
  CalcClone.Data := DM_PAYROLL.PR_SCHEDULED_EVENT.Data;
  CalcClone.Filtered := True;
  i := -1;
  while not CalcClone.Eof do
  begin
    T2 := TMenuItem.Create(Self);
    T2.Caption := CalcClone.FieldByName('SCHEDULED_CHECK_DATE').AsString;
    T2.OnClick := MenuCLickHdl;
    if i = 20 then
    begin
      T2.Break := mbBarBreak;
      i := 0;
    end
    else
      Inc(i);
    CalcPopup.Items.Add(T2);
    CalcClone.Next;
  end;
end;

procedure TREDIT_PAYROLL.MenuCLickHdl(Sender: TObject);
begin
  if DM_PAYROLL.PR.State = dsBrowse then
    DM_PAYROLL.PR.Edit;
  DM_PAYROLL.PR['CHECK_DATE'] := StrToDate(TMenuItem(Sender).Caption);
end;

procedure TREDIT_PAYROLL.dsEDsDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  SpeedButton2.Enabled := dsEDs.DataSet.Active and (dsEDs.DataSet.RecordCount > 0);
end;

procedure TREDIT_PAYROLL.btnCopyPayrollClick(Sender: TObject);
var
  s, sCond: string;
  nbr: Integer;
  coNbr: integer;
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  if ctx_AccountRights.Functions.GetState('USER_CAN_COPY_PAYROLL') <> stEnabled then
    raise ENoRights.CreateHelp('You have no security rights', IDH_SecurityViolation);
  if EvMessage('Any voids or voided checks in the original payroll will be ignored' + #13 +
  'Are you sure you want to copy this payroll?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;
  with DM_PAYROLL do
  begin
    PR.CheckBrowseMode;
    PR_SCHEDULED_EVENT.DisableControls;
    PR.DisableControls;
    ctx_StartWait;
    try
      sCond := PR.RetrieveCondition;
      coNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
      try
        ctx_PayrollCalculation.CopyPayroll(PR.FieldByName('PR_NBR').Value);
      finally
        PR.RetrieveCondition := sCond;
        if not DM_COMPANY.CO.Active then
          DM_COMPANY.CO.Open;
        DM_COMPANY.CO.Locate('CO_NBR',coNbr,[]);
      end;
      try
       PR_CHECK.Close;
       PR_CHECK_LINES.Close;
       PR_CHECK_LOCALS.Close;
       PR_CHECK_LINE_LOCALS.Close;
       PR_CHECK_STATES.Close;
       PR_CHECK_SUI.Close;
       PR.Close;
       sCond := PR.RetrieveCondition;
       PR.CheckDSCondition(GetDataSetConditions(GetDefaultDataSet.Name));
       ctx_DataAccess.OpenDataSets([PR]);
       PR.RetrieveCondition := sCond;

        s := PR.IndexName;
        PR.IndexFieldNames := 'PR_NBR';
        PR.FindNearest([MaxInt]);
        nbr := PR.FieldByName('PR_NBR').AsInteger;
      finally
        PR.IndexName := s;
      end;
      if PR.Locate('PR_NBR', nbr, []) then
         btnOpenPayrollClick(nil);
    finally
      ctx_EndWait;
      PR.EnableControls;
      PR_SCHEDULED_EVENT.EnableControls;
    end;
  end;
end;

procedure TREDIT_PAYROLL.SBReviewBtnClick(Sender: TObject);
var
  s: string;
  t: TStringList;
  PrNbr: Integer;
begin
  inherited;
  if (wwDBGrid6.DataSource.DataSet.RecordCount > 0) then
  begin
    PrNbr := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
    SaveDSStates([DM_PAYROLL.PR]);
    DM_PAYROLL.PR.Close;
    LoadDSStates([DM_PAYROLL.PR]);
    DM_PAYROLL.PR.Locate('PR_NBR', PrNbr, []);
    if wwDBGrid6.DataSource.DataSet.FieldByName('STATUS').AsString <> PAYROLL_STATUS_PENDING then
    begin
      EvErrMessage('Can''t put this payroll on hold');
      AbortEx;
    end;

    s := 'For Review - This will put your payroll on hold until you contact ' + Trim(GetSBName);
    if EvMessage(s, mtConfirmation, mbOKCancel) <> mrOK then
      Exit;

    DM_PAYROLL.PR.Edit;
    DM_PAYROLL.PR.FieldByName('STATUS').AsString := PAYROLL_STATUS_HOLD;
    DM_PAYROLL.PR.Post;
    if (Owner as TFramePackageTmpl).ButtonEnabled(NavCommit) then
      (Owner as TFramePackageTmpl).ClickButton(NavCommit);

    if EvMessage('Thank you. Your payroll has been successfully submitted to ' +
      Trim(GetSBName) + ' for review.'#13#10'Would you like to e-mail your contact at ' + Trim(GetSBName) + '?', mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      s := ConvertNull(DM_SERVICE_BUREAU.SB_USER.Lookup('SB_USER_NBR', DM_COMPANY.CO['CUSTOMER_SERVICE_SB_USER_NBR'], 'EMAIL_ADDRESS'), '');
      if s <> '' then
      begin
        t := TStringList.Create;
        try
          t.Text := s;
          MailTo(t, nil, nil, nil, 'Payroll is on hold', '');
        finally
          t.Free;
        end;
      end
      else
        MailTo(nil, nil, nil, nil, 'Payroll is on hold', '');
    end;
  end;
end;

procedure TREDIT_PAYROLL.CheckWizardButton;
begin
  inherited;
  sbWizard.Visible := iWizardPosition in [WIZ_PAYROLL_CREATED, {WIZ_TOTALS_AFTER_CHECKS, }WIZ_PREPROCESSED, WIZ_TOTALS_AFTER_PREPROCESS];
end;

procedure TREDIT_PAYROLL.sbWizardClick(Sender: TObject);
begin
  inherited;
  case iWizardPosition of
  WIZ_PAYROLL_CREATED: BitBtn3.Click;
  WIZ_PREPROCESSED: pcPayroll.ActivatePage(tsEarnings);
  WIZ_TOTALS_AFTER_PREPROCESS: SubmitBtn.Click;
  end;
end;

procedure TREDIT_PAYROLL.BitBtn4Click(Sender: TObject);
begin
  inherited;

  pcPayroll.ActivatePage(tsPayroll);
end;

procedure TREDIT_PAYROLL.DBMemo1Change(Sender: TObject);
begin
  inherited;
  if (DM_PAYROLL.PR.State <> dsBrowse) and
     DBMemo1.Focused then
    DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').Tag := 1;
end;

function TREDIT_PAYROLL.GetSBName: string;
begin
  if Context.UserAccount.SBAccountantNbr = 0 then
    Result := DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString

  else
  begin
    ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_ACCOUNTANT]);
    if DM_SERVICE_BUREAU.SB_ACCOUNTANT.SB_ACCOUNTANT_NBR.AsInteger <> Context.UserAccount.SBAccountantNbr then
      Assert(DM_SERVICE_BUREAU.SB_ACCOUNTANT.Locate('SB_ACCOUNTANT_NBR', Context.UserAccount.SBAccountantNbr, []));
    Result := DM_SERVICE_BUREAU.SB_ACCOUNTANT.FieldByName('NAME').AsString;
  end;
end;

procedure TREDIT_PAYROLL.bOpenFilteredByDBDTGClick(Sender: TObject);
begin
  inherited;
  with TDBDTP_Filter.Create(nil) do
  try
    ClNbr := wwdsList.DataSet.FieldByName('CL_NBR').AsInteger;
    CoNbr := wwdsList.DataSet.FieldByName('CO_NBR').AsInteger;
    if ShowModal = mrOk then
    begin
      ctx_PayrollCalculation.SetEeFilter(Condition, Caption);
      btnOpenCompanyClick(Sender);
    end;
  finally
    Free;
  end;
end;

procedure TREDIT_PAYROLL.bOpenFilteredByEeClick(Sender: TObject);
var
  sCond, sCapt: string;
begin
  inherited;
  if AskEeFilter(wwdsList.DataSet.FieldByName('CL_NBR').AsInteger,
    wwdsList.DataSet.FieldByName('CO_NBR').AsInteger, sCond, sCapt) then
  begin
    ctx_PayrollCalculation.SetEeFilter(sCond, sCapt);
    btnOpenCompanyClick(Sender);
  end;
end;

procedure TREDIT_PAYROLL.wwDBComboBox1Change(Sender: TObject);
var
  DataSet: TevClientDataSet;
begin
  inherited;
  wwDBComboBox1.DataLink.OnActiveChange(sender);
  DataSet := DM_PAYROLL.PR;
  if DataSet.State = dsInsert then // Payroll was just created
  begin
    if (wwDBComboBox1.Value <> PAYROLL_TYPE_REGULAR)
    and (DataSet.FieldByName('EXCLUDE_TIME_OFF').AsString = TIME_OFF_EXCLUDE_ACCRUAL_NONE) then
      DataSet.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
    if wwDBComboBox1.Value = PAYROLL_TYPE_SETUP then
    begin
      DM_PAYROLL.PR.FieldByName('EXCLUDE_TAX_DEPOSITS').Value := GROUP_BOX_BOTH2;
      if DataSet.FieldByName('EXCLUDE_R_C_B_0R_N').AsString = GROUP_BOX_NO then
        DataSet.FieldByName('EXCLUDE_R_C_B_0R_N').AsString := GROUP_BOX_BOTH2;
      DM_PAYROLL.PR.FieldByName('EXCLUDE_AGENCY').Value := GROUP_BOX_YES;
      DM_PAYROLL.PR.FieldByName('EXCLUDE_ACH').Value := GROUP_BOX_YES;
      DM_PAYROLL.PR.FieldByName('TRUST_IMPOUND').Value := PAYMENT_TYPE_NONE;
      DM_PAYROLL.PR.FieldByName('TAX_IMPOUND').Value := PAYMENT_TYPE_NONE;
      DM_PAYROLL.PR.FieldByName('DD_IMPOUND').Value := PAYMENT_TYPE_NONE;
      DM_PAYROLL.PR.FieldByName('BILLING_IMPOUND').Value := PAYMENT_TYPE_NONE;
      DM_PAYROLL.PR.FieldByName('WC_IMPOUND').Value := PAYMENT_TYPE_NONE;

      DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').Value := GROUP_BOX_YES;
      if DataSet.FieldByName('EXCLUDE_TIME_OFF').AsString = TIME_OFF_EXCLUDE_ACCRUAL_NONE then
        DM_PAYROLL.PR.FieldByName('EXCLUDE_TIME_OFF').Value := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
    end;
    if wwDBComboBox1.Value = PAYROLL_TYPE_CL_CORRECTION then
    begin
      DataSet.FieldByName('APPROVED_BY_TAX').Value := PR_APPROVED_NO;
      DataSet.FieldByName('APPROVED_BY_FINANCE').Value := PR_APPROVED_NO;
      DataSet.FieldByName('APPROVED_BY_MANAGEMENT').Value := PR_APPROVED_NO;
    end;
    if wwDBComboBox1.Value = PAYROLL_TYPE_SB_CORRECTION then
      DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').Value := GROUP_BOX_YES;

  end;
  //Issue#49987
  if ( DataSet.State in [dsInsert, dsEdit] ) then
  begin
     if(wwDBComboBox1.value = PAYROLL_TYPE_BACKDATED)
     then DM_PAYROLL.PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value :=
                DM_SERVICE_BUREAU.SB.FieldByName('MARK_LIABS_PAID_DEFAULT').Value
     else DM_PAYROLL.PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := GROUP_BOX_No; //default
  end;

  if DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PENDING then
  begin
   if (DM_PAYROLL.PR.State = dsEdit) and (wwDBComboBox1.Value <> '') and (wwDBComboBox1.Value[1] in [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT, PAYROLL_TYPE_IMPORT, PAYROLL_TYPE_TAX_ADJUSTMENT ]) then
   begin
     wwDBComboBox1.Text := wwDBComboBox1.GetComboDisplay(DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString);
     wwDBComboBox1.Refresh;
     raise EInconsistentData.CreateHelp('You can not create a payroll of this type manually.', IDH_InconsistentData);
   end;
  end;
end;

procedure TREDIT_PAYROLL.bRunToaBalanceCheckClick(Sender: TObject);
var
  PayrollScr: IevPayrollScreens;
begin
  inherited;
  PayrollScr := Context.GetScreenPackage(IevPayrollScreens) as IevPayrollScreens;

  gToaBalanceCheck.Visible := False;
  LabelHint.Visible        := False;
  gToaBalanceCheck.Update;
  if not cdToaBalanceCheckResults.Active then
    cdToaBalanceCheckResults.CreateDataSet
  else
    cdToaBalanceCheckResults.EmptyDataSet;
  DM_PAYROLL.PR_CHECK.SaveState();
  DM_PAYROLL.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
  cdToaBalanceCheckResults.SaveState();
  cdToaBalanceCheckResults.IndexFieldNames := 'PR_CHECK_NBR';
  DM_PAYROLL.PR_CHECK_LINES.DisableControls;
  ctx_StartWait;
  try
    PayrollScr.ClearCheckToaSetupAccess;
    DM_PAYROLL.PR_CHECK_LINES.First;
    while not DM_PAYROLL.PR_CHECK_LINES.Eof do
    begin
      if not cdToaBalanceCheckResults.FindKey([DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_NBR.AsInteger]) then
      begin
        Assert(DM_PAYROLL.PR_CHECK.FindKey([DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_NBR.AsInteger]));
        if not PayrollScr.SilentCheckToaBalance then
        begin
          cdToaBalanceCheckResults.Append;
          cdToaBalanceCheckResultsPR_CHECK_NBR.AsInteger := DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_NBR.AsInteger;
          cdToaBalanceCheckResultsEE_CUSTOM_NUMBER.AsString :=
            DM_COMPANY.EE.Lookup('EE_NBR', StrToIntDef(DM_PAYROLL.PR_CHECK.ShadowDataSet.CachedLookup(  // Replaced DM_COMPANY.EE.ShadowDataset.CachedLookup with DM_COMPANY.EE.Lookup to fix issue #44359. Andrew
            cdToaBalanceCheckResultsPR_CHECK_NBR.AsInteger, 'EE_NBR'), 0), 'CUSTOM_EMPLOYEE_NUMBER');
          cdToaBalanceCheckResultsCHECK_NUMBER.AsString := DM_PAYROLL.PR_CHECK.ShadowDataSet.CachedLookup(
            cdToaBalanceCheckResultsPR_CHECK_NBR.AsInteger, 'PAYMENT_SERIAL_NUMBER');
          cdToaBalanceCheckResults.Post;
        end;
      end;
      DM_PAYROLL.PR_CHECK_LINES.Next;
    end;
  finally
    ctx_EndWait;
    PayrollScr.ClearCheckToaSetupAccess;
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
    DM_PAYROLL.PR_CHECK.LoadState;
    cdToaBalanceCheckResults.LoadState;
  end;
  gToaBalanceCheck.Visible := True;
  LabelHint.Visible        := True;
end;

procedure TREDIT_PAYROLL.gToaBalanceCheckDblClick(Sender: TObject);
begin
  GotoChecks(Self, cdToaBalanceCheckResultsPR_CHECK_NBR.AsInteger);
end;

procedure TREDIT_PAYROLL.CheckOverrides;
var
  VFilter, BFilter, DFilter, EFilter: String;
  VWasFiltered, BWasFiltered, DWasFiltered, EWasFiltered: Boolean;
begin
  inherited;

  VFilter := DM_COMPANY.CO_DIVISION.Filter;
  VWasFiltered := DM_COMPANY.CO_DIVISION.Filtered;
  BFilter := DM_COMPANY.CO_BRANCH.Filter;
  BWasFiltered := DM_COMPANY.CO_BRANCH.Filtered;
  DFilter := DM_COMPANY.CO_DEPARTMENT.Filter;
  DWasFiltered := DM_COMPANY.CO_DEPARTMENT.Filtered;
  EFilter := DM_COMPANY.CO_TEAM.Filter;
  EWasFiltered := DM_COMPANY.CO_TEAM.Filtered;
  try
    DM_COMPANY.CO_DIVISION.Filter := 'HOME_STATE_TYPE = '+ QuotedStr(HOME_STATE_TYPE_OVERRIDE) + ' and ( not OVERRIDE_PAY_RATE is null)';
    DM_COMPANY.CO_DIVISION.Filtered := True;

    if DM_COMPANY.CO_DIVISION.RecordCount > 0 then
    begin
      EvMessage('If employee''s home dbdt has a rate override,  you will need to enter a rate number when manually overriding the dbdt!!!', mtWarning, [mbOK]);
      Exit;
    end;

    DM_COMPANY.CO_BRANCH.Filter := 'HOME_STATE_TYPE = '+ QuotedStr(HOME_STATE_TYPE_OVERRIDE) + ' and ( not OVERRIDE_PAY_RATE is null)';
    DM_COMPANY.CO_BRANCH.Filtered := True;

    if DM_COMPANY.CO_BRANCH.RecordCount > 0 then
    begin
      EvMessage('If employee''s home dbdt has a rate override,  you will need to enter a rate number when manually overriding the dbdt!!!', mtWarning, [mbOK]);
      Exit;
    end;

    DM_COMPANY.CO_DEPARTMENT.Filter := 'HOME_STATE_TYPE = '+ QuotedStr(HOME_STATE_TYPE_OVERRIDE) + ' and ( not OVERRIDE_PAY_RATE is null)';
    DM_COMPANY.CO_DEPARTMENT.Filtered := True;

    if DM_COMPANY.CO_DEPARTMENT.RecordCount > 0 then
    begin
      EvMessage('If employee''s home dbdt has a rate override,  you will need to enter a rate number when manually overriding the dbdt!!!', mtWarning, [mbOK]);
      Exit;
    end;

    DM_COMPANY.CO_TEAM.Filter := 'HOME_STATE_TYPE = '+ QuotedStr(HOME_STATE_TYPE_OVERRIDE) + ' and ( not OVERRIDE_PAY_RATE is null)';
    DM_COMPANY.CO_TEAM.Filtered := True;

    if DM_COMPANY.CO_TEAM.RecordCount > 0 then
    begin
      EvMessage('If employee''s home dbdt has a rate override,  you will need to enter a rate number when manually overriding the dbdt!!!', mtWarning, [mbOK]);
      Exit;
    end;
  finally
    DM_COMPANY.CO_DIVISION.Filter := VFilter;
    DM_COMPANY.CO_BRANCH.Filter := BFilter;
    DM_COMPANY.CO_DEPARTMENT.Filter := DFilter;
    DM_COMPANY.CO_TEAM.Filter := EFilter;
    DM_COMPANY.CO_DIVISION.Filtered := VWasFiltered;
    DM_COMPANY.CO_BRANCH.Filtered := BWasFiltered;
    DM_COMPANY.CO_DEPARTMENT.Filtered := DWasFiltered;
    DM_COMPANY.CO_TEAM.Filtered := EWasFiltered;
  end;
end;

procedure TREDIT_PAYROLL.SetButtonsEnabled;
begin
  if csLoading in ComponentState then
     exit;
  if (wwdsMaster.Active) and (wwdsMaster.DataSet.Active) then
  begin
    CreatePayrollBtn.Enabled := (wwdsMaster.DataSet.State = dsBrowse);
    QuickPayrollBtn.Enabled := (wwdsMaster.DataSet.State = dsBrowse);
    BitBtn1.Enabled := (wwdsMaster.DataSet.State = dsBrowse);
    SBReviewBtn.Enabled := (wwdsMaster.DataSet.State = dsBrowse) and (not wwdsMaster.DataSet.IsEmpty);
    SubmitBtn.Enabled := (wwdsMaster.DataSet.State = dsBrowse) and
      (ctx_AccountRights.Functions.GetState('USER_CAN_SUBMIT_PAYROLL') = stEnabled)
      and (not wwdsMaster.DataSet.IsEmpty);
    PreprocessBtn.Enabled := (wwdsMaster.DataSet.State = dsBrowse) and (not wwdsMaster.DataSet.IsEmpty);
    DeletePayrollBtn.Enabled := (wwdsMaster.DataSet.State = dsBrowse) and (not wwdsMaster.DataSet.IsEmpty);
    btnCopyPayroll.Enabled := (wwdsMaster.DataSet.State = dsBrowse) and (not wwdsMaster.DataSet.IsEmpty);
    DBMemo1.Enabled := (wwdsMaster.DataSet.State = dsInsert) or (not wwdsMaster.DataSet.IsEmpty);
    evDBRadioGroup2.Enabled := (wwdsMaster.DataSet.State = dsInsert) or (not wwdsMaster.DataSet.IsEmpty);
    evDBRadioGroup3.Enabled := (wwdsMaster.DataSet.State = dsInsert) or (not wwdsMaster.DataSet.IsEmpty);
    evDBRadioGroup1.Enabled := (wwdsMaster.DataSet.State = dsInsert) or (not wwdsMaster.DataSet.IsEmpty);
    evDBSpinEdit1.Enabled := (wwdsMaster.DataSet.State = dsInsert) or (not wwdsMaster.DataSet.IsEmpty);
    wwDBComboBox1.Enabled := (wwdsMaster.DataSet.State = dsInsert) or (not wwdsMaster.DataSet.IsEmpty);
    wwDBDateTimePicker1.Enabled := (wwdsMaster.DataSet.State = dsInsert) or (not wwdsMaster.DataSet.IsEmpty);
    evDBDateTimePicker1.Enabled := (wwdsMaster.DataSet.State = dsInsert) or (not wwdsMaster.DataSet.IsEmpty);
    BitBtn3.Enabled := (wwdsMaster.DataSet.State = dsBrowse) and (not wwdsMaster.DataSet.IsEmpty) and
      not btnOpenCompanyEnabled;
  end;
end;

procedure TREDIT_PAYROLL.wwdsMasterStateChange(Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then
    exit;
  SetButtonsEnabled;
end;

procedure TREDIT_PAYROLL.BeforePost(DataSet: TDataSet);
begin
  if (DM_PAYROLL.PR.State in [dsInsert])
  and (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_REGULAR)
  then
  begin

    if (ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_BACKDATED_PAYROLLS') <> stEnabled)
    and (Trunc(Now) - (DM_PAYROLL.PR.CHECK_DATE.AsDateTime) >= 0) then
      raise EInconsistentData.CreateHelp('You do not have the ability to create a backdated payroll.', IDH_InconsistentData);

    if PayrollIsOutOfRange then
    begin
      if (ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_BACKDATED_PAYROLLS') <> stEnabled) then
        raise EInconsistentData.CreateHelp('You do not have the ability to create a backdated payroll.', IDH_InconsistentData);

      if EvMessage('This payroll is out of range. If you proceed with this payroll it will need to be approved in the Operations-Approve Payroll screen. Do you wish to proceed?',
                 mtConfirmation, [mbYes, mbNo]) <> mrYes then
      begin
        DM_PAYROLL.PR.Cancel;
        AbortEx;
      end;
    end;

  end;
  FBeforePost(DataSet);
end;

procedure TREDIT_PAYROLL.OnActivateParams(const AContext: Integer; const AParams: array of Variant);
begin
  inherited;
  wwdsList.DataSet.Locate('CL_NBR;CO_NBR', VarArrayOf(AParams),[]);
  btnOpenCompanyClick(btnOpenCompany);
  if AContext = 1 then
    if BitBtn1.Enabled then
      BitBtn1Click(BitBtn1);
end;

function TREDIT_PAYROLL.CreateNewPayroll: Boolean;
var
  aDate, aDate2: TDateTime;
begin
  Result := False;
  aDate := ctx_PayrollCalculation.NextCheckDate(Date, True);
  if aDate <> 0 then
    aDate2 := GetNextCheckDate(aDate)
  else
    aDate2 := 0;
  if aDate2 = 0 then
  begin
    DM_PAYROLL.PR.Cancel;
    AutoBackToPreviosScreen := True;
    CameFrom := 'Payroll';
    if EvMessage('There is only one available calendar date remaining. You need to setup a new calendar before you can continue. Would you like to go to the calendar setup now? ', mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      AutoBackToPreviosScreen := True;
      CameFrom := 'Payroll';
      PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Company - General - Calendar' + #0)), 0);
      Exit;
    end;
  end
  else
    (Owner as TFramePackageTmpl).ClickButton(NavInsert);
end;

procedure TREDIT_PAYROLL.QuickPayrollBtnClick(Sender: TObject);
var
  ScheduledCheckDate, aDate: TDateTime;
  b: Boolean;
  PrNbr: Integer;
begin
  if not DM_PAYROLL.PR.IsEmpty then
    PrNbr := DM_PAYROLL.PR.PR_NBR.AsInteger
  else
    PrNbr := 0;
  DM_PAYROLL.PR.SaveState(False);
  try

    aDate := ctx_PayrollCalculation.NextCheckDate(Date, True);
    if aDate <> 0 then
      aDate := GetNextCheckDate(aDate);

    if aDate = 0 then
    begin
      AutoBackToPreviosScreen := True;
      CameFrom := 'Payroll';
      if EvMessage('There is only one available calendar date remaining. You need to setup a new calendar before you can continue. Would you like to go to the calendar setup now? ', mtConfirmation, [mbYes, mbNo]) = mrYes then
      begin
        AutoBackToPreviosScreen := True;
        CameFrom := 'Payroll';
        PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Company - General - Calendar' + #0)), 0);
        Exit;
      end;
      Exit;
    end;

    ctx_StartWait;
    try
      DM_PAYROLL.PR.Append;
      ScheduledCheckDate := ctx_PayrollCalculation.NextCheckDate(Date, True);
      DM_PAYROLL.PR.PAYROLL_TYPE.Value := PAYROLL_TYPE_REGULAR;
      DM_PAYROLL.PR.SCHEDULED.Value := 'Y';
      DM_PAYROLL.PR.CHECK_DATE.Value := ScheduledCheckDate;
      DM_PAYROLL.PR.CHECK_DATE_STATUS.Value := DATE_STATUS_NORMAL;
      DM_PAYROLL.PR.EXCLUDE_AGENCY.Value := 'N';
      DM_PAYROLL.PR.EXCLUDE_ACH.Value := 'N';
      DM_PAYROLL.PR.EXCLUDE_BILLING.Value := 'N';
      DM_PAYROLL.PR.EXCLUDE_TAX_DEPOSITS.Value := 'N';
      DM_PAYROLL.PR.EXCLUDE_R_C_B_0R_N.Value := DM_COMPANY.CO.FieldByName('EXCLUDE_R_C_B_0R_N').Value;
      DM_PAYROLL.PR.EXCLUDE_TIME_OFF.AsString := 'N';
      DM_PAYROLL.PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
      DM_PAYROLL.PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
      DM_PAYROLL.PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
      DM_PAYROLL.PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
      DM_PAYROLL.PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
      DM_PAYROLL.PR.Post;
      DM_PAYROLL.PR_BATCH.Open;
      DM_PAYROLL.PR_BATCH.Append;
      DM_PAYROLL.PR_BATCH.PR_NBR.AsInteger := DM_PAYROLL.PR.PR_NBR.AsInteger;
      b := ctx_PayrollCalculation.FindNextPeriodBeginEndDate;
      if DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RecordCount > 0 then
      begin
        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.First;
        with BreakCompanyFreqIntoPayFreqList(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['FREQUENCY']) do
        try
          DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Strings[0];
        finally
          Free;
        end;
      end
      else
        DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Copy(ctx_PayrollCalculation.GetCompanyPayFrequencies, Pos(#9, ctx_PayrollCalculation.GetCompanyPayFrequencies) + 1, 1);
      if not b then
      begin
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := ctx_PayrollCalculation.NextPeriodBeginDate;
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := ctx_PayrollCalculation.NextPeriodEndDate;
      end
      else
      begin
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_BEGIN_DATE'];
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_END_DATE'];
      end;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_NORMAL;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_NORMAL;

      DM_PAYROLL.PR_BATCH.FieldByName('PAY_SALARY').Value := 'Y';
      DM_PAYROLL.PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'Y';
      DM_PAYROLL.PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'Y';
      DM_PAYROLL.PR_BATCH.Post;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, DM_PAYROLL.PR_SCHEDULED_EVENT, DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH]);
      PrNbr := DM_PAYROLL.PR.PR_NBR.AsInteger;
      ctx_PayrollProcessing.CreatePRBatchDetails(DM_PAYROLL.PR_BATCH.PR_BATCH_NBR.AsInteger,
        True, True, CHECK_TYPE2_REGULAR, CHECK_TYPE_945_NONE, 1, 0, Null,
        nil, nil, loByName, loFull, False, loFixed, False, False, True, False);
      (Owner as TFramePackageTmpl).SetButton(NavOK, False);
      (Owner as TFramePackageTmpl).SetButton(NavCommit, False);
      (Owner as TFramePackageTmpl).SetButton(NavAbort, False);
      CloseDetailPayroll;
      OpenDetailPayroll;
      (Owner as TFramePackageTmpl).OpenFrame('TREDIT_BATCH');
    finally
      ctx_EndWait;
    end;
  finally
    DM_PAYROLL.PR.LoadState;
    if PrNbr <> 0 then
      DM_PAYROLL.PR.Locate('PR_NBR', PrNbr, []);
  end;
end;

procedure TREDIT_PAYROLL.tsPayrollShow(Sender: TObject);
begin
  SetButtonsEnabled;
  dtPayrollAsOfDate.SetFocus;   //if it's not focused, it won't fire.
  dtPayrollAsOfDate.OnChange(nil);
end;

procedure TREDIT_PAYROLL.spbFavoriteReportsClick(Sender: TObject);
begin
  if not btnOpenCompany.Enabled then
     CallFavoriteReports;
end;

procedure TREDIT_PAYROLL.pnlListResize(Sender: TObject);
begin
  inherited;
  //only do the height on this one.

end;

procedure TREDIT_PAYROLL.btnMRCLogInClick(Sender: TObject);
begin
  if not btnOpenCompany.Enabled then
     CallMyRecruitingCenterWebSite;
end;

procedure TREDIT_PAYROLL.btnSwipeClockClick(Sender: TObject);
begin
  if not btnOpenCompany.Enabled then
     CallSwipeClockWebSite;

end;

procedure TREDIT_PAYROLL.dtPayrollAsOfDateCloseUp(Sender: TObject);
begin
  inherited;
  dtPayrollAsOfDate.OnChange(nil);
end;

procedure TREDIT_PAYROLL.sbChecksConstrainedResize(Sender: TObject;
  var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
  inherited;
//  Realign;
end;

initialization
  RegisterClass(TREDIT_PAYROLL);

end.


