inherited ADD2CHECK_LINES_LOCALS: TADD2CHECK_LINES_LOCALS
  Width = 742
  Height = 718
  inherited evPanel1: TevPanel
    Width = 742
    Height = 718
    inherited fpSource: TisUIFashionPanel
      Title = 'EE Locals'
      inherited evDBGrid1: TevDBGrid
        ControlType.Strings = (
          'EXEMPT_EXCLUDE;CustomEdit;evDBComboBox1')
        Selected.Strings = (
          'Local_Name_Lookup'#9'35'#9'Local'#9'F'
          'EXEMPT_EXCLUDE'#9'14'#9'Exclude'#9'F')
        IniAttributes.SectionName = 'TADD2CHECK_LINES_LOCALS\evDBGrid1'
      end
      inherited evPanel3: TevPanel
        Left = -64
        Width = 742
        DesignSize = (
          742
          25)
        inherited evBitBtn3: TevBitBtn
          Left = 579
        end
        inherited evBitBtn4: TevBitBtn
          Left = 659
        end
      end
    end
    inherited fpDetination: TisUIFashionPanel
      Title = 'PR Check Line Locals'
      inherited evDBGrid2: TevDBGrid
        ControlType.Strings = (
          'EXEMPT_EXCLUDE;CustomEdit;cbCLOverride'
          'EE_LOCALS_NBR;CustomEdit;lcCLLocalName'
          'Local_Name_Lookup;CustomEdit;lcCLLocalName')
        Selected.Strings = (
          'Local_Name_Lookup'#9'35'#9'Local Name'#9'F'
          'EXEMPT_EXCLUDE'#9'14'#9'Exclude'#9'F')
        IniAttributes.SectionName = 'TADD2CHECK_LINES_LOCALS\evDBGrid2'
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgTabExitsOnLastCol, dgDblClickColSizing]
        ReadOnly = False
      end
      inherited evPanel2: TevPanel
        Left = 15
        Top = 264
        Width = 666
        Caption = ''
      end
    end
    object cbCLOverride: TevDBComboBox
      Left = 212
      Top = 375
      Width = 121
      Height = 21
      TabStop = False
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'EXEMPT_EXCLUDE'
      DataSource = dsDetailDest
      DropDownCount = 8
      ItemHeight = 0
      Items.Strings = (
        'Exempt'#9'E'
        'Exclude'#9'X'
        'Include'#9'I')
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 2
      UnboundDataType = wwDefault
    end
    object evDBComboBox1: TevDBComboBox
      Left = 128
      Top = 151
      Width = 121
      Height = 21
      TabStop = False
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = False
      AutoDropDown = True
      DataField = 'EXEMPT_EXCLUDE'
      DataSource = dsDetailSrc
      DropDownCount = 8
      ItemHeight = 0
      Items.Strings = (
        'Exempt'#9'E'
        'Exclude'#9'X'
        'Include'#9'I')
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 4
      UnboundDataType = wwDefault
    end
    object lcCLLocalName: TevDBLookupCombo
      Left = 44
      Top = 378
      Width = 97
      Height = 21
      TabStop = False
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'Local_Name_Lookup'#9'20'#9'Name'#9'F')
      DataField = 'EE_LOCALS_NBR'
      DataSource = dsDetailDest
      LookupTable = DM_EE_LOCALS.EE_LOCALS
      LookupField = 'EE_LOCALS_NBR'
      Style = csDropDownList
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
    end
  end
  inherited evActionList1: TevActionList
    Left = 300
    Top = 84
  end
  inherited dsMasterSrc: TevDataSource
    DataSet = DM_EE.EE
    Left = 164
    Top = 86
  end
  inherited dsDetailSrc: TevDataSource
    DataSet = DM_EE_LOCALS.EE_LOCALS
    Left = 226
    Top = 86
  end
  inherited dsMasterDest: TevDataSource
    DataSet = DM_PR_CHECK_LINES.PR_CHECK_LINES
    Left = 176
    Top = 300
  end
  inherited dsDetailDest: TevDataSource
    DataSet = DM_PR_CHECK_LINE_LOCALS.PR_CHECK_LINE_LOCALS
    Left = 232
    Top = 300
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 84
    Top = 84
  end
  object DM_PAYROLL: TDM_PAYROLL
    Left = 96
    Top = 304
  end
end
