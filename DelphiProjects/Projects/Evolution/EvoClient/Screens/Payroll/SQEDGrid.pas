// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SQEDGrid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sdatastructure, StdCtrls, DBCtrls, EvContext,
  ISBasicClasses, EvUIComponents, ExtCtrls, DB, Wwdatsrc, wwdblook, Grids,
  Wwdbigrd, Wwdbgrid, sQEDHolder, SPayrollScr, SDDClasses, Menus, sRemotePayrollUtils,
  SPD_DBDTLookups, SDataDictclient,  EvMainboard, EvExceptions,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton, ImgList,
  EvClientDataSet, EvUIUtils;

//DefaultSort = EE_Custom_Number;Check_Number
//DefaultSort = 'CO_DEPARTMENT_NBR;EE_Custom_Number'

type
  TQuickEDGridState = (qedgInactive, qedgActive );

  TQEDPosition = record
    PrCheckNbr: Variant;
    PrCheckLineNbr: Variant;
    FieldName: string; //of active column
  end;

  TQuickEDGrid = class(TFrame)
    pnlTop: TevPanel;
    pnlGrid: TevPanel;
    dbtEENumber: TevDBText;
    dbtEEName: TevDBText;
    dsQED: TevDataSource;
    dgQED: TevDBGrid;
    lkupEE_Custom_Number: TevDBLookupCombo;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    lkupCL_AGENCY_NBR: TevDBLookupCombo;
    pmFields: TevPopupMenu;
    miAddField: TMenuItem;
    miRemoveField: TMenuItem;
    miAddED: TMenuItem;
    N1: TMenuItem;
    miRemoveED: TMenuItem;
    miAddEDDlg: TMenuItem;
    lkupCO_SHIFTS_NBR: TevDBLookupCombo;
    lkupCL_PIECES_NBR: TevDBLookupCombo;
    lkupCO_WORKERS_COMP_NBR: TevDBLookupCombo;
    lkupCO_JOBS_NBR: TevDBLookupCombo;
    lkupEE_STATES_NBR: TevDBLookupCombo;
    lkupEE_SUI_STATES_NBR: TevDBLookupCombo;
    _lkupCO_DIVISION_NBR: TevDBLookupCombo;
    _lkupCO_BRANCH_NBR: TevDBLookupCombo;
    _lkupCO_DEPARTMENT_NBR: TevDBLookupCombo;
    _lkupCO_TEAM_NBR: TevDBLookupCombo;
    AddEDsforthisBatch1: TMenuItem;
    RestoreDefaults: TMenuItem;
    N2: TMenuItem;
    DBDTLookups: TDBDTLookups;
    lkupDBDT_CUSTOM_NUMBERS: TevDBLookupCombo;
    lblEE1: TevLabel;
    ilGridPictures: TevImageList;
    procedure lkupEE_Custom_NumberCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure lkupEE_Custom_NumberNotInList(Sender: TObject;
      LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
    procedure dgQEDBeforeDrawCell(Sender: TwwCustomDBGrid;
      DrawCellInfo: TwwCustomDrawGridCellInfo);
    procedure dgQEDDblClick(Sender: TObject);
    procedure dgQEDExit(Sender: TObject);
    procedure dgQEDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dsQEDDataChange(Sender: TObject; Field: TField);
    procedure pmFieldsPopup(Sender: TObject);
    procedure miAddEDDlgClick(Sender: TObject);
    procedure RestoreDefaultsClick(Sender: TObject);
    procedure AddEDsforthisBatch1Click(Sender: TObject);
    procedure dgQEDCellChanged(Sender: TObject);
    procedure dbdtDropDown(Sender: TObject);
    procedure dgQEDKeyPress(Sender: TObject; var Key: Char);
    procedure dgQEDRowChanged(Sender: TObject);
    procedure dgQEDColExit(Sender: TObject);
    procedure dgQEDAfterDrawCell(Sender: TwwCustomDBGrid;
      DrawCellInfo: TwwCustomDrawGridCellInfo);
    procedure dgQEDCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
  private
    FEDFieldDescs: TEDFieldDescs;
    FState: TQuickEDGridState;
    FCurrentColumn: string;
    function GetComboForGridField( const fieldname: string ): TevDBLookupCombo;

    procedure AddPrCheckLinesField(idx: integer);
    function LoadSettings( defsel: TSelectedRecArray ): boolean; //!! should call SetDefaultSettings by itself; should take no arguments

    procedure EnsureValid; //!! a bit flawed; some responsibilities should be moved to SelectedChanged
    procedure SelectedChanged; //!!

    procedure BuildFieldsMenu;
    function GetUserFields: TUserFields;

    procedure HandleAddField(Sender: TObject);

    procedure HandleAddEDField(Sender: TObject);
    procedure HandleRemoveField(Sender: TObject);
    procedure HandleRemoveEDField(Sender: TObject);
    function GetQED: TevClientDataSet;
    property QED: TevClientDataSet read GetQED;
    procedure AddEDField(idx: integer);
    procedure SetDefaultSettings;
    procedure AddDefaultEDs;
    function IsGridReadonly: boolean;
    function GetPosition: TQEDPosition;
//    procedure SetPosition( prchecknbr, prchecklinenbr: Variant );
    function  GetDBDTLevelFromSender( Sender: TObject; out level: integer): boolean;
    procedure ShowDBDTList( v: Variant);
    procedure InitDBDTList(ds: TDataSet; var Key: Char; Level: Integer);
    procedure QEDPost;
    procedure UpdateGridState;
  public
    procedure Activate;
    procedure Deactivate;
    procedure PrBatchDataChanged;
    procedure Shown;

    procedure SaveSettings;
    // called from dgQED.OnExit event and
    // by the batch screen when a speed button clicked
    // (the grid retains focus and OnExit event doesn't fire)
    procedure ForceEndEditing( bCancel: boolean );
    property Position: TQEDPosition read GetPosition;
  end;

procedure TryLocatePosition( pos: TQEDPosition );

implementation

uses
  evtypes, evutils, sPackageEntry, 
  SSecurityInterface, SPD_EDIT_SELECT_ITEM, evconsts, strutils,
  gdystrset, SPD_DBDTSelectionListFiltr, evDeferredCall, Types;

{$R *.dfm}

const
  sGridVersion = '2';

function IndexOfEDFieldDesc(edd: TEDFieldDescs; fieldname: string): integer;
var
  i: integer;
begin
  Result := -1;
  for i := low(edd) to high(edd) do
    if fieldname = edd[i].FieldName then
    begin
      Result := i;
      break;
    end;
end;

function IndexOfEDFieldDescByEDNbr(edd: TEDFieldDescs; cledsnbr: integer): integer;
var
  i: integer;
begin
  Result := -1;
  for i := low(edd) to high(edd) do
    if cledsnbr = edd[i].CL_E_DS_NBR then
    begin
      Result := i;
      break;
    end;
end;

{TQuickEDGrid}

procedure TQuickEDGrid.Activate;
begin
  FState := qedgActive;
  dgQED.ReadOnly := true; //just initialize to safe value
  //to preserve Selected
  {
  lkupCO_DIVISION_NBR.LookupTable := nil;
  lkupCO_BRANCH_NBR.LookupTable := nil;
  lkupCO_DEPARTMENT_NBR.LookupTable := nil;
  lkupCO_TEAM_NBR.LookupTable := nil;
  }
  lkupDBDT_CUSTOM_NUMBERS.LookupTable := nil;

  DBDTLookups.Activate;
  QEDHolder.SetDBDT( DBDTLookups.cdDBDT );

  {
  lkupCO_DIVISION_NBR.LookupTable := DBDTLookups.cdDBDT;
  lkupCO_BRANCH_NBR.LookupTable := DBDTLookups.cdDBDT;
  lkupCO_DEPARTMENT_NBR.LookupTable := DBDTLookups.cdDBDT;
  lkupCO_TEAM_NBR.LookupTable := DBDTLookups.cdDBDT;

  lkupCO_DIVISION_NBR.LookupField := DBDTLeafField;
  lkupCO_BRANCH_NBR.LookupField := DBDTLeafField;
  lkupCO_DEPARTMENT_NBR.LookupField := DBDTLeafField;
  lkupCO_TEAM_NBR.LookupField := DBDTLeafField;

  lkupCO_DIVISION_NBR.DataField := DBDTLeafField;
  lkupCO_BRANCH_NBR.DataField := DBDTLeafField;
  lkupCO_DEPARTMENT_NBR.DataField := DBDTLeafField;
  lkupCO_TEAM_NBR.DataField := DBDTLeafField;
  }
  lkupDBDT_CUSTOM_NUMBERS.LookupTable := DBDTLookups.cdDBDT;
  lkupDBDT_CUSTOM_NUMBERS.LookupField := DBDTLeafField;
  lkupDBDT_CUSTOM_NUMBERS.DataField := DBDTLeafField;

  BuildFieldsMenu;
  SetDefaultSettings;
  if not LoadSettings( ParseSelected( dgQED.Selected) ) then
    AddDefaultEDs;
  SelectedChanged;
end;

procedure TQuickEDGrid.SetDefaultSettings;
begin
  dgQED.Selected.Clear;
  AddSelected( dgQED.Selected, 'CHECK_TYPE', 2, 'Type', true );
  AddSelected( dgQED.Selected, 'Notes', 2, 'Notes', true );
  AddSelected( dgQED.Selected, 'EE_Custom_Number', 10, 'EE Code', false );
  dgQED.SetControlType('EE_Custom_Number', fctCustom, 'lkupEE_Custom_Number');
  AddSelected( dgQED.Selected, 'Employee_Name_Calculate', 40, 'Name', true );
  AddSelected( dgQED.Selected, 'Check_Number', 2, 'Check #', false );
end;

procedure TQuickEDGrid.AddDefaultEDs;
var
  i: integer;
begin
  for i := low(FEDFieldDescs) to high(FEDFieldDescs) do
    if (FEDFieldDescs[i].E_D_CODE_TYPE = ED_OEARN_SALARY) or
       (FEDFieldDescs[i].E_D_CODE_TYPE = ED_OEARN_REGULAR) then
      AddEDField( i );
end;

procedure TQuickEDGrid.Deactivate;
begin
  if FState = qedgInactive then
    Exit;
//  Assert( FState <> qedgInactive ); //allow to go out of the screen in case of catastrophic failure

  ForceEndEditing( false{not cancel} );
  SaveSettings;
  QEDHolder.SetDBDT( nil );
  DBDTLookups.Deactivate;
  FEDFieldDescs := nil;
  FState := qedgInactive;
  DM_EMPLOYEE.EE_STATES.UserFilter := '0=0';
  DM_EMPLOYEE.EE_STATES.UserFiltered := True;
end;

procedure TQuickEDGrid.lkupEE_Custom_NumberCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  if QED.State in [dsEdit] then
  begin
    QED.Cancel;
    raise EUpdateError.CreateHelp('Can not change an employee for the existing check.', IDH_ConsistencyViolation);
  end;
end;

procedure TQuickEDGrid.lkupEE_Custom_NumberNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
  Accept := false;
end;

procedure TQuickEDGrid.dgQEDBeforeDrawCell(Sender: TwwCustomDBGrid;
  DrawCellInfo: TwwCustomDrawGridCellInfo);
begin
  if (gdFixed in DrawCellInfo.State) and
     (DrawCellInfo.DataRow >= 0) then
    Sender.Canvas.Font := TevDBGrid(Sender).Font;

end;

procedure TQuickEDGrid.dgQEDDblClick(Sender: TObject);
begin
  GotoChecks( Self, QED.FieldByName('PR_CHECK_NBR').AsInteger );
end;

procedure TQuickEDGrid.dgQEDExit(Sender: TObject);
begin
  ForceEndEditing( false {not cancel} );
end;

procedure TQuickEDGrid.dgQEDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  level: integer;
begin
  if (Key = VK_RETURN) and (Shift = [ssCtrl]) then
  begin
    GotoChecks(Self, QED.FieldByName('PR_CHECK_NBR').AsInteger);
    Key := 0;
  end;
  if not IsGridReadonly and assigned(QED) and (QED['CHECK_TYPE'] <> CHECK_TYPE2_VOID) then
  begin
    if (Key = VK_DELETE) and (Shift = [ssCtrl]) then
    begin
      if (QED.RecordCount > 0) then
        if QED['LineTypeSort'] = CHECK_LINE_TYPE_SCHEDULED then
        begin
          if (EvMessage('Delete these Scheduled E/Ds?'#13'You can refresh scheduled E/Ds for this check to bring them back.', mtConfirmation, mbOKCancel) = mrOK) then
            QED.Delete
        end
        else
        begin
          if (EvMessage('Delete these E/Ds?', mtConfirmation, mbOKCancel) = mrOK) then
            QED.Delete;
        end;
      Key := 0;
    end;
    if Key = VK_INSERT then
    begin
      QED.Insert;
      Key := 0;
    end;
    if ( Shift * [ssShift, ssAlt, ssCtrl] = [] ) and (Key = VK_F4) and
      GetDBDTLevelFromSender( Sender, level ) then
    begin
      DeferredCall( ShowDBDTList, level );
      Key := 0;
    end;
  end
end;

procedure TQuickEDGrid.PrBatchDataChanged;
begin
  if FState = qedgInactive then
    Exit;

  EnsureValid;

  //SetGridPosition;
end;

procedure TQuickEDGrid.Shown;
begin
  Assert( FState <> qedgInactive );
  if FState = qedgInactive then
    Exit;

  EnsureValid;

  if not QED.Locate('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.AsVariant, []) then
    QED.First;
end;

procedure TQuickEDGrid.dsQEDDataChange(Sender: TObject; Field: TField);
var
  i: integer;
  sel: TSelectedRecArray;
begin
  sel := ParseSelected( dgQED.Selected );
  for i := 0 to high(sel) do
  begin
    if assigned(QED) and (IndexOfEDFieldDesc( FEDFieldDescs, sel[i].FieldName ) <> -1){yes, it is ED field} then
      dgQED.Columns[i].FooterValue := FormatFloat(TFloatField(QED.FieldByName(sel[i].FieldName)).DisplayFormat,
          ConvertNull(QED.Aggregates.Find(sel[i].FieldName).Value, 0))
    else
      dgQED.Columns[i].FooterValue := '';
  end;
  SPayrollScr.SaveLastCheckPosition(QED.FieldByName('PR_CHECK_NBR').AsInteger);
  lblEE1.Caption := Trim(dbtEENumber.Caption);
end;

procedure TQuickEDGrid.ForceEndEditing(bCancel: boolean);
begin
  if FState = qedgInactive then
    Exit;

  if QED <> nil then
    if QED.State <> dsBrowse then
      if bCancel then
        QED.Cancel
      else
        QED.CheckBrowseMode;
end;

function TQuickEDGrid.LoadSettings( defsel: TSelectedRecArray ): boolean;
var
  gridkey: string;
  clcokey: string;
  i: integer;
  sel: TSelectedRecArray;
  idx: integer;
begin
  ForceEndEditing( false{not cancel} );

  gridkey := 'Misc\' + Self.ClassName + '\dgQED';
  clcokey := gridkey + '\CL ' + IntToStr(ctx_DAtaAccess.ClientID) + ' CO ' + VarToStr(DM_COMPANY.CO.CO_NBR.Value);

  if mb_AppSettings[gridkey + '\Version']  <> sGridVersion then
    mb_AppSettings.DeleteNode( gridkey );

  sel := ParseSelected(mb_AppSettings[clcokey + '\Selected']);

  dgQED.Selected.Clear;

  //filter out unknown fields
  for i := low(sel) to high(sel) do
  begin
    if IndexOfSelectedField( defsel, sel[i].FieldName ) <> -1 then
      AddSelected( dgQED.Selected, sel[i] )
    else
    begin
      idx := IndexOfPrCheckLinesFieldDesc( TrimGridFieldName(sel[i].FieldName) );
      if idx <> -1 then
      begin
        if not PrCheckLinesFieldDescs[idx].Hidden then
          AddPrCheckLinesField(idx);
      end
      else
      begin
        idx := IndexOfEDFieldDesc( FEDFieldDescs, sel[i].FieldName );
        if idx <> -1 then
          AddEDField( idx );
      end
    end
  end;

  //look at what we have after filtering
  sel := ParseSelected(dgQED.Selected);

  Result := Length(sel) > 0;

  //make sure that all default fields are shown in the grid
  for i := low(defsel) to high(defsel) do
    if IndexOfSelectedField( sel, defsel[i].FieldName ) = -1 then
      AddSelected( dgQED.Selected, defsel[i] );

  FCurrentColumn := mb_AppSettings[clcokey + '\CurrentColumn'];
  QEDHolder.FIndexFields := mb_AppSettings[clcokey + '\Index'];

end;

procedure TQuickEDGrid.SaveSettings;
var
  gridkey: string;
  clcokey: string;
  sel: TSelectedRecArray;
  activeColumn: string;
begin
  SetLength(sel, 0); //avoiding compiler hint
  if FState = qedgInactive then
    Exit;

  gridkey := 'Misc\' + Self.ClassName + '\dgQED';
  clcokey := gridkey + '\CL ' + IntToStr(ctx_DAtaAccess.ClientID) + ' CO ' + DM_COMPANY.CO.CO_NBR.AsString;
  mb_AppSettings[gridkey + '\Version'] := sGridVersion;

  if DM_COMPANY.CO.CO_NBR.AsString <> '' then
  begin
    mb_AppSettings.DeleteNode( clcokey );
    mb_AppSettings[clcokey + '\Selected'] := dgQED.Selected.Text;
    activeColumn := '';
    if dgQED.SelectedIndex <> -1 then
    begin
      sel := ParseSelected( dgQED.Selected );
      activeColumn := sel[dgQED.SelectedIndex].FieldName;
    end;
    mb_AppSettings[clcokey + '\CurrentColumn'] := activeColumn;
    mb_AppSettings[clcokey + '\Index'] :=   QEDHolder.FIndexFields;
  end;
end;

function TQuickEDGrid.GetQED: TevClientDataSet;
begin
  Result := TevClientDataSet(dsQED.DataSet);
end;

procedure TQuickEDGrid.EnsureValid;
var
  i: integer;
  idx: integer;
begin
//ignore-debug
//  if (QEDHolder.QED <> nil) and (IndexOfSelectedField( ParseSelected( dgQED.Selected ), 'CO_DEPARTMENT_NBR' ) <> -1) then
//    dgQED.SetControlType('CO_DEPARTMENT_NBR', fctField, '');

  DBDTLookups.RefDS := nil;

  dsQED.DataSet := nil;
  dgQED.ApplySelected;

  QEDHolder.EnsureIsValid( GetUserFields );

  for i := 0 to ComponentCount-1 do
    if Components[i] is TevDBLookupCombo then
      if QEDHolder.QED.FindField( Copy(Components[i].Name, Length('lkup')+1, 9999) ) = nil then
        TevDBLookupCombo(Components[i]).DataSource := nil;

  dsQED.DataSet := QEDHolder.QED;
  dgQED.ReadOnly := IsGridReadonly;

  if QEDHolder.QED.FindField('CO_DIVISION_NBR'{any DBDT}) <> nil then
    DBDTLookups.RefDS := QEDHolder.QED;

  idx := IndexOfSelectedField( ParseSelected(dgQED.Selected), FCurrentColumn );
  if idx <> -1 then
  begin
    dgQED.SelectedIndex := idx;
    FCurrentColumn := '';
  end;
end;

procedure TQuickEDGrid.BuildFieldsMenu;
var
  i: integer;
  saveIndexFieldNames: string;
  miAdd: TMenuItem;
  miRemove: TMenuItem;
begin
  for i := low(PrCheckLinesFieldDescs) to high(PrCheckLinesFieldDescs) do
    if not PrCheckLinesFieldDescs[i].Hidden then
    begin
      AddMenuItem( miAddField, PrCheckLinesFieldDescs[i].Caption, HandleAddField, true, i );
      AddMenuItem( miRemoveField, PrCheckLinesFieldDescs[i].Caption, HandleRemoveField, false, i );
    end;

  SetLength( FEDFieldDescs, DM_COMPANY.CO_E_D_CODES.RecordCount*2 );

  saveIndexFieldNames := DM_COMPANY.CO_E_D_CODES.IndexFieldNames;
  with DM_COMPANY.CO_E_D_CODES do
  try
    IndexFieldNames := 'ED_Lookup';
    First;
    i := 0;
    while not Eof do
    begin
      miAdd := AddMenuItem( miAddED, FieldByName('ED_Lookup').AsString + ' ' + FieldByName('CodeDescription').AsString, nil, true, -1 );
      miRemove := AddMenuItem( miRemoveED, FieldByName('ED_Lookup').AsString + ' ' + FieldByName('CodeDescription').AsString, nil, true, -1 );

      FEDFieldDescs[i].FieldName := 'Amt_' + FieldByName('ED_Lookup').AsString;
      FEDFieldDescs[i].Caption := 'Amt '+FieldByName('ED_Lookup').AsString + '~' + FieldByName('CodeDescription').AsString;
      FEDFieldDescs[i].FieldType := qftAmount;
      FEDFieldDescs[i].CL_E_DS_NBR := CL_E_DS_NBR.AsInteger;
      FEDFieldDescs[i].CUSTOM_E_D_CODE_NUMBER := FieldByName('ED_Lookup').AsString;
      FEDFieldDescs[i].E_D_CODE_TYPE := FieldByName('E_D_CODE_TYPE').AsString;

      AddMenuItem( miAdd, 'Amt', HandleAddEDField, true, i );
      AddMenuItem( miRemove, 'Amt', HandleRemoveEDField, true, i );
      inc(i);

      FEDFieldDescs[i].FieldName := 'Hrs_' + FieldByName('ED_Lookup').AsString;
      FEDFieldDescs[i].Caption := 'Hrs '+FieldByName('ED_Lookup').AsString + '~' + FieldByName('CodeDescription').AsString;
      FEDFieldDescs[i].FieldType := qftHours;
      FEDFieldDescs[i].CL_E_DS_NBR := CL_E_DS_NBR.AsInteger;
      FEDFieldDescs[i].CUSTOM_E_D_CODE_NUMBER := FieldByName('ED_Lookup').AsString;
      FEDFieldDescs[i].E_D_CODE_TYPE := FieldByName('E_D_CODE_TYPE').AsString;

      AddMenuItem( miAdd, 'Hrs', HandleAddEDField, true, i );
      AddMenuItem( miRemove, 'Hrs', HandleRemoveEDField, true, i );
      inc(i);

      Next;
    end;
  finally
    IndexFieldNames := saveIndexFieldNames;
  end;
end;

procedure TQuickEDGrid.SelectedChanged;
var
  i: integer;
  sel: TSelectedRecArray;
  cb: TevDBLookupCombo;
begin
  dgQED.ApplySelected; //!! is one of these reduntant?
  sel := ParseSelected( dgQED.Selected );
  for i := 0 to high(sel) do
  begin
    cb := GetComboForGridField( sel[i].FieldName );
    if cb <> nil then
    begin
      cb.DataSource := dsQED;
      dgQED.SetControlType(sel[i].FieldName, fctCustom, cb.Name);
    end;
  end;
  dgQED.ApplySelected;
end;

procedure TQuickEDGrid.AddPrCheckLinesField(idx: integer);
var
  f: PPrCheckLinesFieldDesc;
begin
  f := @PrCheckLinesFieldDescs[idx];
  AddSelected( dgQED.Selected, FieldNameForGrid(f^), f.DisplayWidth, f.Caption, false);
end;

procedure TQuickEDGrid.AddEDField( idx: integer );
var
  f: PEDFieldDesc;
begin
  f := @FEDFieldDescs[idx];
  AddSelected( dgQED.Selected, f.FieldName, 10, f.Caption, ctx_AccountRights.Functions.GetState('FIELDS_WAGES') <> stEnabled );
end;

procedure TQuickEDGrid.HandleAddField(Sender: TObject);
begin
  AddPrCheckLinesField( (Sender as TMenuItem).Tag );
  EnsureValid;
  SelectedChanged;
end;

procedure TQuickEDGrid.HandleRemoveField(Sender: TObject);
begin
  dgQED.Selected.Delete( IndexOfSelectedField( ParseSelected(dgQED.Selected), FieldNameForGrid( PrCheckLinesFieldDescs[(Sender as TMenuItem).Tag] ) ) );
  dgQED.SetControlType( FieldNameForGrid( PrCheckLinesFieldDescs[(Sender as TMenuItem).Tag] ) , fctField, '');
  EnsureValid;
  SelectedChanged;
end;

procedure TQuickEDGrid.HandleAddEDField(Sender: TObject);
begin
  AddEDField( (Sender as TMenuItem).Tag );
  EnsureValid;
  SelectedChanged;
end;

procedure TQuickEDGrid.HandleRemoveEDField(Sender: TObject);
var
  f: PEDFieldDesc;
begin
  f := @FEDFieldDescs[(Sender as TMenuItem).Tag];
  dgQED.Selected.Delete( IndexOfSelectedField( ParseSelected(dgQED.Selected), f.FieldName ) );
  dgQED.SetControlType( f.FieldName, fctField, '');
  EnsureValid;
  SelectedChanged;
end;

procedure TQuickEDGrid.pmFieldsPopup(Sender: TObject);
var
  i: integer;
  vis: boolean;
  visCount: integer;
  totalCount: integer;
  sel: TSelectedRecArray;

  procedure UpdateEDMenu(miED: TMenuItem; needvis: boolean );
  var
    i: integer;
    j: integer;
    localvisCount: integer;
  begin
    viscount := 0;
    for i := 0 to miED.Count-1 do
    begin
      localvisCount := 0;
      for j := 0 to miED.Items[i].Count-1 do
      begin
        vis := IndexOfSelectedField( sel, FEDFieldDescs[miED.Items[i].Items[j].Tag].FieldName ) <> -1;
        miED.Items[i].Items[j].Visible := vis = needvis;
        if vis = needvis then
          inc(localvisCount);
      end;
      miED.Items[i].Visible := localvisCount <> 0;
      if miED.Items[i].Visible then
        inc(visCount);
    end;
    miED.Enabled := visCount <> 0;
  end;
begin
  visCount := 0;
  totalCount := 0;
  sel := ParseSelected( dgQED.Selected );


  for i := low(PrCheckLinesFieldDescs) to high(PrCheckLinesFieldDescs) do
    if not PrCheckLinesFieldDescs[i].Hidden then
    begin
      inc(totalCount);
      vis := IndexOfSelectedField( sel, FieldNameForGrid( PrCheckLinesFieldDescs[i] ) ) <> -1;
      if vis then
        inc(visCount);

      FindByTag( miAddField, i ).Visible := not vis;
      FindByTag( miRemoveField, i ).Visible := vis;
//      miAddField.Items[i].Visible := not vis;
//      miRemoveField.Items[i].Visible := vis;
    end;
  miAddField.Enabled := visCount < totalCount;
  miRemoveField.Enabled  := visCount > 0;

  UpdateEDMenu(miAddED, false);
  UpdateEDMenu(miRemoveED, true);
  miAddEDDlg.Enabled := miAddED.Enabled;
end;

procedure TQuickEDGrid.miAddEDDlgClick(Sender: TObject);
begin
  with TEDIT_SELECT_ITEM.Create( Self ) do
  try
    RenderQPMenu( miAddED );
    if ShowModal = mrOk then
      Selected.Click;
  finally
    Free;
  end;
end;

function TQuickEDGrid.GetUserFields: TUserFields;
var
  i: integer;
  sel: TSelectedRecArray;
  edcount: integer;
  prcount: integer;
begin
  sel := ParseSelected( dgQED.Selected );

  SetLength( Result.EdFieldDescs, dgQED.Selected.Count ); //max possible
  edcount := 0;
  for i := low(FEDFieldDescs) to high(FEDFieldDescs) do
  begin
    if IndexOfSelectedField( sel, FEDFieldDescs[i].FieldName ) <> -1 then
    begin
      Result.EdFieldDescs[edcount] := FEDFieldDescs[i];
      inc(edcount);
    end;
  end;
  SetLength( Result.EdFieldDescs, edcount );

  SetLength( Result.PrCheckLinesFields, dgQED.Selected.Count ); //max possible
  prcount := 0;
  for i := low(PrCheckLinesFieldDescs) to high(PrCheckLinesFieldDescs) do
    if not PrCheckLinesFieldDescs[i].Hidden then
    begin
      if IndexOfSelectedField( sel, FieldNameForGrid( PrCheckLinesFieldDescs[i] ) ) <> -1 then
      begin
        Result.PrCheckLinesFields[prcount] := PrCheckLinesFieldDescs[i].FieldName;
        inc(prcount);
      end;
    end;
  SetLength( Result.PrCheckLinesFields, prcount );

  if InSet('DBDT_CUSTOM_NUMBERS', Result.PrCheckLinesFields ) then
  begin
    Result.PrCheckLinesFields := SetUnion( Result.PrCheckLinesFields, ['CO_DIVISION_NBR', 'CO_BRANCH_NBR', 'CO_DEPARTMENT_NBR', 'CO_TEAM_NBR'] );
  end
end;

procedure TQuickEDGrid.RestoreDefaultsClick(Sender: TObject);
begin
////  TevDBLookupCombo(Components[i]).DataSource := dsQED;
//  dgQED.SetControlType('CO_BRANCH_NBR_DESC', fctCustom, lkupBranch.Name);
//  QED.DisableControls;
//  QED.EnableControls;
//  ODS('test: QED.IndexFieldNames = <%s> QED.IndexName = <%s>',[QED.IndexFieldNames, QED.IndexName]);
//    dgQED.Selected.Clear;
//  exit;

  SetDefaultSettings;
  AddDefaultEDs;
  EnsureValid;
  SelectedChanged;
end;

procedure TQuickEDGrid.AddEDsforthisBatch1Click(Sender: TObject);
var
  PR_CHECK_LINES: TPR_CHECK_LINES;
  aEDAmtNbrList: TStringList;
  aEDHrsNbrList: TStringList;
  aEDNbrList: TStringList;
  i: integer;
  sel: TSelectedRecArray;
begin
  PR_CHECK_LINES := DM_PAYROLL.PR_CHECK_LINES;
  aEDAmtNbrList := TStringList.Create;
  aEDAmtNbrList.Sorted := true;
  aEDAmtNbrList.Duplicates := dupIgnore;
  aEDHrsNbrList := TStringList.Create;
  aEDHrsNbrList.Sorted := true;
  aEDHrsNbrList.Duplicates := dupIgnore;
  aEDNbrList := TStringList.Create;
  aEDNbrList.Sorted := true;
  aEDNbrList.Duplicates := dupIgnore;
  try
    SaveDSStates( [PR_CHECK_LINES] );
    PR_CHECK_LINES.LookupsEnabled := False;
    try
      PR_CHECK_LINES.IndexFieldNames := 'PR_CHECK_NBR';
      PR_CHECK_LINES.First;
      while not PR_CHECK_LINES.EOF do
      begin
        if not PR_CHECK_LINES.AMOUNT.IsNull and (PR_CHECK_LINES.AMOUNT.AsFloat <> 0) then
          aEDAmtNbrList.Add( PR_CHECK_LINES.CL_E_DS_NBR.AsString );
        if not PR_CHECK_LINES.HOURS_OR_PIECES.IsNull and (PR_CHECK_LINES.HOURS_OR_PIECES.AsFloat <> 0) then
          aEDHrsNbrList.Add( PR_CHECK_LINES.CL_E_DS_NBR.AsString );
        aEDNbrList.Add( PR_CHECK_LINES.CL_E_DS_NBR.AsString );
        PR_CHECK_LINES.Next;
      end;
    finally
      PR_CHECK_LINES.LookupsEnabled := True;
      LoadDSStates([PR_CHECK_LINES]);
    end;

    for i := 0 to aEDNbrList.Count-1 do
      if (aEDAmtNbrList.IndexOf( aEDNbrList[i] ) = -1) and (aEDHrsNbrList.IndexOf( aEDNbrList[i] ) = -1) then
        aEDAmtNbrList.Add( aEDNbrList[i] );

    sel := ParseSelected(dgQED.Selected);
    for i := low(FEDFieldDescs) to high(FEDFieldDescs) do
    begin
//      if FEDFieldDescs[i].CL_E_DS_NBR = 33 then
//        DebugBreak;
      if IndexOfSelectedField( sel, FEDFieldDescs[i].FieldName ) = -1 then
        case FEDFieldDescs[i].FieldType of
          qftHours:  if aEDHrsNbrList.IndexOf( inttostr(FEDFieldDescs[i].CL_E_DS_NBR) ) <> -1 then
                       AddEDField( i );
          qftAmount: if aEDAmtNbrList.IndexOf( inttostr(FEDFieldDescs[i].CL_E_DS_NBR) ) <> -1 then
                       AddEDField( i );
        end;
    end;
    EnsureValid;
    SelectedChanged;
  finally
    FreeAndNil(aEDAmtNbrList);
    FreeAndNil(aEDHrsNbrList);
    FreeAndNil(aEDNbrList);
  end;
end;

procedure TQuickEDGrid.UpdateGridState;
begin
  dgQED.ReadOnly := IsGridReadonly or
    (QED<> nil) and
    (
      (QED['CHECK_TYPE'] = CHECK_TYPE2_VOID) //just mimics SPD_REDIT_CHECK
    or
      (QED['LineTypeSort'] = CHECK_LINE_TYPE_SCHEDULED)
    or
      (dgQED.GetActiveField <> nil)
      and
      ((dgQED.GetActiveField.FieldName = 'Check_Number') or (dgQED.GetActiveField.FieldName = 'EE_Custom_Number'))
      and
      (QED.State <> dsInsert)
    );
  if not IsGridReadonly then
    dgQED.ForceKeyOptions([dgAllowInsert, dgEnterToTab])
  else
    dgQED.ForceKeyOptions([dgEnterToTab]);
end;

procedure TQuickEDGrid.dgQEDCellChanged(Sender: TObject);
begin
  UpdateGridState;
  if (dgQED.GetActiveCol > dgQED.FixedCols) and QEDHolder.IsNewCheckJustEntered then
    DeferredCall( QEDPost );
end;

function TQuickEDGrid.IsGridReadonly: boolean;
begin
  Result := DM_PAYROLL.PR_BATCH.RecordCount = 0;
end;

function TQuickEDGrid.GetPosition: TQEDPosition;
begin
  Result.PrCheckNbr := unassigned;
  Result.PrCheckLineNbr := unassigned;
  Result.FieldName := '';
  if assigned(QED) and (QED.RecordCount > 0) then
  begin
    Result.PrCheckNbr := QED['PR_CHECK_NBR'];
    if dgQED.GetActiveField <> nil then
    begin
      Result.FieldName := dgQED.GetActiveField.FieldName;
      if (LeftStr(Result.FieldName, Length('Amt_')) = 'Amt_') then
        Result.PrCheckLineNbr := QED['Nbr_' + RightStr(Result.FieldName, Length(Result.FieldName)-Length('Amt_') )]
      else if (LeftStr(Result.FieldName, Length('Hrs_')) = 'Hrs_') then
        Result.PrCheckLineNbr := QED['Nbr_' + RightStr(Result.FieldName, Length(Result.FieldName)-Length('Hrs_') )];
    end;
  end;
end;

procedure TryLocatePosition( pos: TQEDPosition );
begin
  if not VarIsEmpty(pos.PrCheckNbr) and not VarIsNull(pos.PrCheckNbr) then
    DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', pos.PrCheckNbr, []);

  //actually this code doesn't work because REDIT_CHECK screen have its own logic in Activate to set position in PR_CHECK_LINES
  if not VarIsEmpty(pos.PrCheckLineNbr) and not VarIsNull(pos.PrCheckLineNbr) then
    DM_PAYROLL.PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', pos.PrCheckLineNbr, []);
end;

{
procedure TQuickEDGrid.SetPosition(prchecknbr, prchecklinenbr: Variant);
var
  bLocated: boolean;
  edcode: Variant;
  sel: TSelectedRecArray;
  i: integer;
begin
  bLocated := false;
  if not VarIsEmpty(prchecknbr) and not VarIsNull(prchecknbr) then
    bLocated := QED.Locate('PR_CHECK_NBR', prchecknbr, []);

  if not VarIsEmpty(prchecklinenbr) and not VarIsNull(prchecklinenbr) then
  begin
    edcode := DM_PAYROLL.PR_CHECK_LINES.Lookup('PR_CHECK_LINES_NBR', prchecklinenbr, 'E_D_Code_Lookup');
    if not VarIsNull(edcode) and (QED.FindField('Nbr_'+edcode) <> nil) then
      bLocated := QED.Locate('Nbr_'+edcode, prchecklinenbr, []);
    if bLocated then
    begin
      sel := ParseSelected( dgQED.Selected );
      for i := 0 to high(sel) do
        if (sel[i].FieldName = 'Amt_'+edcode) or (sel[i].FieldName = 'Hrs_'+edcode) then
        begin
          dgQED.SelectedIndex := i;
          break;
        end;
    end

  end;

  if not bLocated then
    QED.First;
end;
}

procedure TQuickEDGrid.dbdtDropDown(Sender: TObject);
begin
{  (Sender as TevDBLookupCombo).LookupTable.Filtered := false;
  (Sender as TevDBLookupCombo).LookupTable.Filtered := true;
  }
end;

//copied from SPD_REDIT_CHECK.TREDIT_CHECK.InitDBDTList
//DM_PAYROLL.PR_CHECK_LINES replaced by ds parameter
procedure TQuickEDGrid.InitDBDTList(ds: TDataSet; var Key: Char; Level: Integer);
var
  s: TDBDTSelectionStr;
  DBDTSelectionListFiltr: TDBDTSelectionListFiltr;
begin
  DBDTSelectionListFiltr := TDBDTSelectionListFiltr.Create(nil);
  try
    if key <> #32 then
    begin
      s := DBDTLookups.GetStringForFilter( ds[DBDTLeafField]);
      DBDTSelectionListFiltr.edtFiltr.Text := s.Text;
      DBDTSelectionListFiltr.edtFiltr.SelStart := Length(s.Text)-s.LastPartLength;
      DBDTSelectionListFiltr.edtFiltr.SelLength := s.LastPartLength;
    end
    else
      DBDTSelectionListFiltr.edtFiltr.Clear;
    if (key <> #0) and (key <> #32) {and (initfilter='')} then
      SendMessage(DBDTSelectionListFiltr.edtFiltr.Handle, WM_Char, word(Key), 0);
    Key := #0;
    with DM_COMPANY, DBDTSelectionListFiltr do
    begin
      CO_DIVISION.DataRequired('CO_NBR=' + CO.FieldByName('CO_NBR').AsString);
      DBDTSelectionListFiltr.Setup(TevClientDataSet(CO_DIVISION), TevClientDataSet(CO_BRANCH),
        TevClientDataSet(CO_DEPARTMENT), TevClientDataSet(CO_TEAM),
        ds.FieldByName('CO_DIVISION_NBR').Value
        , ds.FieldByName('CO_BRANCH_NBR').Value, ds.FieldByName('CO_DEPARTMENT_NBR').Value
        , ds.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionListFiltr.ShowModal = mrOK then
      begin
        if (ds.FieldByName('CO_DIVISION_NBR').Value <> wwcsTempDBDT.FieldByName('DIVISION_NBR').Value)
          or (ds.FieldByName('CO_BRANCH_NBR').Value <> wwcsTempDBDT.FieldByName('BRANCH_NBR').Value)
          or (ds.FieldByName('CO_DEPARTMENT_NBR').Value <> wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value)
          or (ds.FieldByName('CO_TEAM_NBR').Value <> wwcsTempDBDT.FieldByName('TEAM_NBR').Value) then
        begin
          if not (ds.State in [dsInsert, dsEdit]) then
          begin
            if ds.RecordCount = 0 then
              ds.Insert
            else
              ds.Edit;
          end;
          ds.FieldByName(DBDTLeafField).Value := wwcsTempDBDT.FieldByName(Copy(DBDTLeafField, Length('CO_')+1,9999)).Value;
        end;
      end;
    end;
  finally
    FreeAndNil(DBDTSelectionListFiltr);
  end;
end;

procedure TQuickEDGrid.dgQEDKeyPress(Sender: TObject; var Key: Char);
var
  level: integer;
begin
  if not assigned(QED) then
    exit;

  if Key in [#9, #13,#27] then
    exit;

  if GetDBDTLevelFromSender(Sender, level) then
    InitDBDTList(QED, Key, level);
end;

function TQuickEDGrid.GetComboForGridField( const fieldname: string): TevDBLookupCombo;
begin
  if (fieldname = 'DBDT_CUSTOM_NUMBERS') and (DBDTLevel<0) then
    Result := nil
  else
    Result := FindComponent( 'lkup'+TrimGridFieldName(fieldname) ) as TevDBLookupCombo;
end;

function TQuickEDGrid.GetDBDTLevelFromSender(Sender: TObject; out level: integer): boolean;
var
  ds: TDataSource;
begin
  Level := -1;

  if Sender = dgQED then
    if assigned(dgQED.DataSource) and assigned(dgQED.DataSource.DataSet)
       and dgQED.DataSource.DataSet.Active then
      if dgQED.GetActiveField.FieldName = 'DBDT_CUSTOM_NUMBERS' then
        Level := DBDTLevel + 1;
        
  if Sender = lkupDBDT_CUSTOM_NUMBERS then
  begin
    ds := TevDBLookupCombo(Sender).DataSource;
    if Assigned(ds) and Assigned(ds.DataSet) and ds.DataSet.Active then
      Level := DBDTLevel + 1;
  end;

{
  if Sender = dgQED then
    if assigned(dgQED.DataSource) and assigned(dgQED.DataSource.DataSet)
       and dgQED.DataSource.DataSet.Active then
    if dgQED.GetActiveField.FieldName = 'CO_DEPARTMENT_NBR_DESC' then
      Level := 3
    else if dgQED.GetActiveField.FieldName = 'CO_TEAM_NBR_DESC' then
      Level := 4
    else if dgQED.GetActiveField.FieldName = 'CO_BRANCH_NBR_DESC' then
      Level := 2
    else if dgQED.GetActiveField.FieldName = 'CO_DIVISION_NBR_DESC' then
      Level := 1;

  if (Sender = lkupCO_DIVISION_NBR) or (Sender = lkupCO_BRANCH_NBR) or
     (Sender = lkupCO_DEPARTMENT_NBR) or (Sender = lkupCO_TEAM_NBR) then
  begin
    ds := TevDBLookupCombo(Sender).DataSource;
    if Assigned(ds) and Assigned(ds.DataSet) and ds.DataSet.Active then
      if Sender = lkupCO_DIVISION_NBR then
        Level := 1
      else if Sender = lkupCO_BRANCH_NBR then
        Level := 2
      else if Sender = lkupCO_DEPARTMENT_NBR then
        Level := 3
      else if Sender = lkupCO_TEAM_NBR then
        Level := 4;
  end;
}
  Result := Level >= 0;
end;

procedure TQuickEDGrid.ShowDBDTList(v: Variant);
var
  Key: Char;
  level: integer;
begin
  Key := #0;
  level := v;
  InitDBDTList(QED, Key, level);
end;

procedure TQuickEDGrid.QEDPost;
begin
  QED.Post;
end;

procedure TQuickEDGrid.dgQEDRowChanged(Sender: TObject);
var
 c_ee_nbr : string;
begin
  c_ee_nbr := dgQED.DataSource.DataSet.FieldByName('EE_NBR').AsString;
  if length(trim(c_ee_nbr))=0 then c_ee_nbr :='-1';
  DM_EMPLOYEE.EE_STATES.UserFilter := '0=0 AND EE_NBR=' + c_ee_nbr;
  DM_EMPLOYEE.EE_STATES.UserFiltered := True;

end;

procedure TQuickEDGrid.dgQEDColExit(Sender: TObject);
begin
  if assigned(dgQED.DataSource) and assigned(dgQED.DataSource.DataSet) and (dgQED.DataSource.DataSet.State = dsEdit) then
    ForceEndEditing( false );
end;

procedure TQuickEDGrid.dgQEDAfterDrawCell(Sender: TwwCustomDBGrid;
  DrawCellInfo: TwwCustomDrawGridCellInfo);
var
  aBitmap   : TBitmap;
  cellRect,
  paintRect : TRect;
  bmpWidth  : Integer;
  s         : String;
begin
  //don't fire this event if we are inserting a new record
  if dsQED.State in [ dsInsert ] then exit;
  aBitmap := TBitmap.Create;

  try  
    cellRect := DrawCellInfo.Rect;
    if DrawCellInfo.DataRow < 0 then exit;
    if DrawCellInfo.DataCol <> 4 then exit;

      if assigned(QED) and (QED.RecordCount > 0) then
        begin
          s := QED['LineTypeSort'];
          if s = 'S' then
          begin

              ilGridPictures.GetBitmap(0,aBitmap);
              aBitmap.Transparent := True;
              bmpWidth := (cellRect.Bottom - cellRect.top);
              paintRect.Left := cellRect.Left + 1;
              paintRect.Right := cellRect.Left + bmpWidth -2;
              paintRect.Top := cellRect.Top + 1;
              paintRect.Bottom := cellRect.Bottom - 1;
              dgQED.Canvas.StretchDraw(paintRect,aBitmap);
          end;
        end;

  finally
    aBitmap.Free;
  end;

end;

procedure TQuickEDGrid.dgQEDCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  if (gdFixed in State) and
     (ABrush.Color = TevDBGrid(Sender).TitleColor) then
    if not TevDBGrid(Sender).IsAlternatingRow(TevDBGrid(Sender).CalcCellRow) then
      ABrush.Color := TevDBGrid(Sender).Color
    else
      ABrush.Color := TevDBGrid(Sender).PaintOptions.AlternatingRowColor;

  if assigned(Field) and assigned(Field.DataSet) and Field.DataSet.Active then
  begin
    if (Field.DataSet.FieldByName('NOTES').AsString = 'Y') then
    begin

      if not Highlight then
      begin
        AFont.Color := clBlack; //!!temp fix
        ABrush.Color := clYellow;
      end
      else
      begin
        AFont.Color := clBlack; //!!temp fix
        ABrush.Color := clOlive;
      end;
    end;

  end;
end;

end.

