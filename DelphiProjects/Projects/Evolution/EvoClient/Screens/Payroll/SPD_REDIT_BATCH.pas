// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_REDIT_BATCH;

interface

uses
  Windows,  SPD_REDIT_WIZARD_BASE, Menus, StdCtrls, ExtCtrls,
  wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, DBCtrls, Mask, wwdbedit, EvBasicUtils,
  Wwdotdot, Wwdbcomb, wwdblook, Controls, ComCtrls, Classes, ActnList, Db, Forms,
  Wwdatsrc, Buttons, SPackageEntry, Graphics, SDataStructure, SysUtils, SSecurityInterface,
  Dialogs, EvUtils, EvConsts, EvTypes, Spin, Variants, isVCLBugFix,
  SDDClasses, ISBasicClasses, SQEDGrid, EvContext, IsBaseClasses,
  sEDTotals, SDataDictclient, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvMainboard, EvExceptions,
  evCommonInterfaces, SPD_TOBalanceCheck, evInitApp, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit,
  isUIwwDBDateTimePicker, LMDCustomButton, LMDButton, isUILMDButton,
  isUIwwDBComboBox, isUIwwDBLookupCombo, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, ImgList, isUIFashionPanel, XLSFile, wwexport,
  XLSWorkbook, EvDataset, isUIwwDBEdit;

type
  TREDIT_BATCH = class(TREDIT_WIZARD_BASE)
    pcBatch: TevPageControl;
    tsBatch: TTabSheet;
    tsCheck: TTabSheet;
    Panel5: TevPanel;
    tsEarnings: TTabSheet;
    tsTax: TTabSheet;
    wwDBGrid1: TevDBGrid;
    Splitter2: TevSplitter;
    wwDBGrid5: TevDBGrid;
    wwDBGrid6: TevDBGrid;
    PR_Label: TevLabel;
    Batch_Label: TevLabel;
    Panel7: TevPanel;
    cbSeperateOverrides: TevCheckBox;
    EE_LookupCombo: TevDBLookupCombo;
    EE_DataSource: TevDataSource;
    lblChecks: TevLabel;
    QPMenu: TevPopupMenu;
    AddItem: TMenuItem;
    DeleteItem: TMenuItem;
    YeartodateItem: TMenuItem;
    N1: TMenuItem;
    DM_PAYROLL: TDM_PAYROLL;
    CheckSrc: TevDataSource;
    DM_COMPANY: TDM_COMPANY;
    NewBatchExpertBtn: TevSpeedButton;
    pStatus: TevPanel;
    dbtEENumber: TevDBText;
    dbtEEName: TevDBText;
    AddItemList: TMenuItem;
    CalcPopup: TevPopupMenu;
    Test1: TMenuItem;
    tsImport: TTabSheet;
    dlgImportSourceFile: TOpenDialog;
    dlgImportReportFile: TSaveDialog;
    EDItem: TMenuItem;
    TaxItem: TMenuItem;
    AddTaxItem: TMenuItem;
    DeleteTaxItem: TMenuItem;
    evPanel1: TevPanel;
    bEE: TevSpeedButton;
    bEERates: TevSpeedButton;
    lEeFilter: TLabel;
    tsQEDGrid: TTabSheet;
    QuickEDGrid: TQuickEDGrid;
    EDTotals: TEDTotals;
    tsContacts: TTabSheet;
    wwDBGrid2x: TevDBGrid;
    wwdsCoPhone: TevDataSource;
    tsAutoReduction: TTabSheet;
    LabelAutoReductionEDGroup: TevLabel;
    DBlookAutoReductionED: TevDBLookupCombo;
    btnReduceHours: TevButton;
    btnViewExceptions: TevButton;
    DBGridExceptions: TevDBGrid;
    rgpEEsToReduce: TevRadioGroup;
    AutoReductionExceptions: TevClientDataSet;
    dsAutoReductionExceptions: TevDataSource;
    AutoReductionExceptionsEE_CODE: TStringField;
    AutoReductionExceptionsEE_NAME: TStringField;
    AutoReductionExceptionsCheckNumber: TStringField;
    AutoReductionExceptionsED_Code: TStringField;
    AutoReductionExceptionsED_Description: TStringField;
    AutoReductionExceptionsHours: TFloatField;
    PanelHint: TevPanel;
    AutoReductionExceptionsPR_CHECK_NBR: TIntegerField;
    cbRates: TevDBComboBox;
    lblSalary: TevLabel;
    sbCobraWizard: TevSpeedButton;
    lablClient: TevLabel;
    dbtClNumber: TevDBText;
    dbtClName: TevDBText;
    evLabel7: TevLabel;
    dbtCoNumber: TevDBText;
    dbtCoName: TevDBText;
    dsCL: TevDataSource;
    Panel2: TPanel;
    PreprocessBtn: TevBitBtn;
    SubmitBtn: TevBitBtn;
    spbFavoriteReports: TevSpeedButton;
    PageControlImages: TevImageList;
    sbBatch: TScrollBox;
    pnlList: TisUIFashionPanel;
    pnlBatchInfo: TisUIFashionPanel;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    Label4: TevLabel;
    Label5: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    wwDBLookupCombo2: TevDBLookupCombo;
    wwcbPayFrequency: TevDBComboBox;
    DBRadioGroup1: TevDBRadioGroup;
    DBRadioGroup2: TevDBRadioGroup;
    DBRadioGroup3: TevDBRadioGroup;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    wwDBDateTimePicker2: TevDBDateTimePicker;
    pnlControls: TisUIFashionPanel;
    BitBtn1: TevBitBtn;
    BitBtn2: TevBitBtn;
    evBitBtn2: TevBitBtn;
    btnCreateCobraBatch: TevBitBtn;
    pnlOptions: TisUIFashionPanel;
    evLabel6: TevLabel;
    evSpinEdit1: TevSpinEdit;
    rgPay: TevRadioGroup;
    chbSelectEEFromList: TevCheckBox;
    evCheckBox2: TevCheckBox;
    rgCreateChecks: TevRadioGroup;
    chbCalcScheduledEDs: TevCheckBox;
    IncludeTimeOffRequests: TevCheckBox;
    sbChecks: TScrollBox;
    evPanel5: TevPanel;
    pnlQuickEntry: TisUIFashionPanel;
    pnlQuickEntryBody: TevPanel;
    sbQE2: TScrollBox;
    evPanel6: TevPanel;
    pnlQE2: TisUIFashionPanel;
    pnlQE2Body: TevPanel;
    sbTaxes: TScrollBox;
    evPanel7: TevPanel;
    pnlTaxes: TisUIFashionPanel;
    pnlTaxesBody: TevPanel;
    pnlImport: TisUIFashionPanel;
    spbImportSourceFile: TevSpeedButton;
    evLabel1: TevLabel;
    spbImportReportFile: TevSpeedButton;
    evLabel2: TevLabel;
    editImportSourceFile: TevEdit;
    editImportReportFile: TevEdit;
    Panel8: TevPanel;
    cbFourDigits: TevCheckBox;
    btnTwImportGo: TevBitBtn;
    btnTwImportView: TevBitBtn;
    cbAutoImportJobCodes: TevCheckBox;
    cbUseEmployeePayRates: TevCheckBox;
    cbAuoRefreshEDs: TevCheckBox;
    cbIncludeDDOnly: TevCheckBox;
    sbImport: TScrollBox;
    sbContacts: TScrollBox;
    pnlContacts: TisUIFashionPanel;
    sbReduction: TScrollBox;
    pnlSettings: TisUIFashionPanel;
    pnlReductionExceptions: TisUIFashionPanel;
    GroupBox1: TGroupBox;
    radioImportLookBySSN: TevRadioButton;
    radioImportLookByName: TevRadioButton;
    radioImportLookByNumber: TevRadioButton;
    GroupBox2: TGroupBox;
    radioImportMatchDbdtFull: TevRadioButton;
    radioImportMatchDbdtPartial: TevRadioButton;
    GroupBox3: TGroupBox;
    radioImportFormatFFF: TevRadioButton;
    radioImportFormatCSF: TevRadioButton;
    sbBatchTaxes: TScrollBox;
    pnlBatchTaxesBorder: TevPanel;
    pnlBatchTaxLeft: TevPanel;
    evSplitter1: TevSplitter;
    evPanel2: TevPanel;
    fpBatchTaxRight: TisUIFashionPanel;
    pnlFPBatchTaxRight: TevPanel;
    lblEECode: TevLabel;
    lblName: TevLabel;
    lblTrimedEECode: TevLabel;
    stEENo: TStaticText;
    stEEName: TStaticText;
    btnSwipeClock: TevSpeedButton;
    BtnMRCLogIn: TevSpeedButton;
    evBitBtn1: TevBitBtn;
    pnlFavoriteReport: TevPanel;
    evLabel47: TevLabel;
    pnlTopLeft: TevPanel;
    Edit1: TEdit;
    fpBatchTaxLeft: TisUIFashionPanel;
    pnlFPBatchTaxLeft: TevPanel;
    wwDBGrid4: TevDBGrid;
    pnlSunkenTaxes: TevPanel;
    CalcPopup2: TevPopupMenu;
    MenuItem1: TMenuItem;
    btnPreProcessBatch: TevBitBtn;
    lblBatchDes: TevLabel;
    edtBatchDes: TevDBEdit;
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure pcBatchChange(Sender: TObject);
    procedure cbSeperateOverridesClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EE_LookupComboNotInList(Sender: TObject;
      LookupTable: TDataSet; NewValue: string; var Accept: Boolean);
    procedure BitBtn2Click(Sender: TObject);
    procedure wwDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure wwDBGrid1DblClick(Sender: TObject);
    procedure wwDBGrid5DblClick(Sender: TObject);
    procedure wwDBGrid5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EE_LookupComboCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure QUICK_PAY_DSDataChange(Sender: TObject; Field: TField);
    procedure QuickEDGriddsQEDDataChange(Sender: TObject; Field: TField);
    procedure YeartodateItemClick(Sender: TObject);
    procedure wwDBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CheckSrcDataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwDBGrid1Exit(Sender: TObject);
    procedure NewBatchExpertBtnClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure sbWizardClick(Sender: TObject);
    procedure AddItemListClick(Sender: TObject);
    procedure wwDBDateTimePicker1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CalcPopupPopup(Sender: TObject);
    procedure spbImportSourceFileClick(Sender: TObject);
    procedure spbImportReportFileClick(Sender: TObject);
    procedure btnTwImportGoClick(Sender: TObject);
    procedure btnTwImportViewClick(Sender: TObject);
    procedure cbFourDigitsClick(Sender: TObject);
    procedure pcBatchChanging(Sender: TObject; var AllowChange: Boolean);
    procedure wwDBGrid1BeforeDrawCell(Sender: TwwCustomDBGrid;
      DrawCellInfo: TwwCustomDrawGridCellInfo);
    procedure chbCalcScheduledEDsClick(Sender: TObject);
    procedure bEEClick(Sender: TObject);
    procedure bEERatesClick(Sender: TObject);
    procedure wwcbPayFrequencyChange(Sender: TObject);
    procedure btnReduceHoursClick(Sender: TObject);
    procedure btnViewExceptionsClick(Sender: TObject);
    procedure DBGridExceptionsDblClick(Sender: TObject);
    procedure wwDBGrid1ColExit(Sender: TObject);
    procedure sbCobraWizardClick(Sender: TObject);
    procedure btnCreateCobraBatchClick(Sender: TObject);
    procedure PreprocessBtnClick(Sender: TObject);
    procedure SubmitBtnClick(Sender: TObject);
    procedure IncludeTimeOffRequestsClick(Sender: TObject);
    procedure spbFavoriteReportsClick(Sender: TObject);
    procedure pnlQuickEntryResize(Sender: TObject);
    procedure pnlQE2Resize(Sender: TObject);
    procedure EE_DataSourceDataChange(Sender: TObject; Field: TField);
    procedure QuickEDGriddgQEDRowChanged(Sender: TObject);
    procedure BtnMRCLogInClick(Sender: TObject);
    procedure btnSwipeClockClick(Sender: TObject);
    procedure QuickEDGriddgQEDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnPreProcessBatchClick(Sender: TObject);
  private
    FQPInit: Boolean;
    NewBatch: Boolean;
    FNewCobraBatch: Boolean;
    ImportingTimeClock: Boolean;
    FEENbr: integer;

    procedure PBAfterPost(DataSet: TDataSet);
    procedure PBAfterInsert(DataSet: TDataSet);
    procedure AddClick(Sender: TObject);
    procedure DeleteClick(Sender: TObject);
    procedure AddTaxClick(Sender: TObject);
    procedure DeleteTaxClick(Sender: TObject);
    procedure MenuCLickHdl(Sender: TObject);
    procedure CommitData;
    procedure SetGridPosition;
    procedure UpdateQPMenuStatus;
    function CheckGridReadOnly: Boolean;
    procedure RefreshEErates(EE_NBR: integer);
    procedure RefreshEEStates;
    procedure EnableControls(aEnable: boolean);
    function PayrollIsOutOfRange: Boolean;
    function GetSBName: string;
    procedure GridToXls(Grid: TISDBGrid; XLS: TXLSFile);
  protected
    function GetIfReadOnly: Boolean; override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetDataSetConditions(sName: string): string; override;
    procedure SetReadOnly(Value: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    function CanOpen: Boolean; override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure AfterClick(Kind: Integer); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses
  SPD_YTD_Totals, SPopulateRecords, SPD_EDIT_SELECT_ITEM, ShellApi, SPayrollScr,
  SProcessTCImport, SPD_EmployeeSelector, wwstr, StrUtils, sRemotePayrollUtils;

{$R *.DFM}

procedure TREDIT_BATCH.BitBtn4Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TREDIT_PAYROLL');
end;

procedure TREDIT_BATCH.BitBtn3Click(Sender: TObject);
begin
  if Assigned(QUICK_PAY_DS.DataSet) and QUICK_PAY_DS.DataSet.Active and (QUICK_PAY_DS.DataSet.RecordCount > 0) then
  begin
    QUICK_PAY_DS.DataSet.CheckBrowseMode;
    GotoChecks(Self, QUICK_PAY_DS.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
  end
  else
    (Owner as TFramePackageTmpl).OpenFrame('TREDIT_CHECK');
end;

procedure TREDIT_BATCH.pcBatchChange(Sender: TObject);
begin
  inherited;
//  RefreshEEStates;

  CurrentBatchTabSheet := pcBatch.ActivePage.Name;

  //   Added By CA..
  if DM_PAYROLL.PR_BATCH.RecordCount > 0 then   //  added to control import tab if there is no record dont do any thing...
           tsImport.TabVisible := true
      else tsImport.TabVisible := false;

  if pcBatch.ActivePage = tsEarnings then
    Calculate_ED_TOTALS(SeparateEDByLineType, SeparateEDByCheckType);

  if pcBatch.ActivePage = tsTax then
    Calculate_TAX_TOTALS(cbSeperateOverrides.Checked);

  if pcBatch.ActivePage = tsCheck then
  begin

    ResetCalc;
    Calculate_BATCH_QuickPay;

    if not QUICK_PAY_DS.DataSet.Locate('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').Value, []) then
      QUICK_PAY_DS.DataSet.First;
    EE_DataSource.DataSet := QUICK_PAY_DS.DataSet;
    EE_LookupCombo.LookupTable := DM_EMPLOYEE.EE;
    EE_LookupCombo.LookupField := 'Custom_Employee_Number';

    wwDBGrid1.SetControlType('NUMBER', fctCustom, 'EE_LookupCombo');
    wwDBGrid1.ApplySelected;

    SetGridPosition;
  end
  else if pcBatch.ActivePage = tsQEDGrid then
    QuickEDGrid.Shown
  else
  begin
    if not ctx_DataAccess.DelayedCommit then
      CommitData;
  end;

  if pcBatch.ActivePage = tsAutoReduction then
  begin
    DBlookAutoReductionED.LookupValue := DM_COMPANY.CO.FieldByName('AUTO_RD_DFLT_CL_E_D_GROUPS_NBR').AsString;
    if DM_COMPANY.CO.FieldByName('AUTO_REDUCTION_DEFAULT_EES').AsString = 'H' then
      rgpEEsToReduce.ItemIndex := 0
    else if DM_COMPANY.CO.FieldByName('AUTO_REDUCTION_DEFAULT_EES').AsString = 'S' then
      rgpEEsToReduce.ItemIndex := 1
    else
      rgpEEsToReduce.ItemIndex := 2;
  end;

  lblChecks.Visible := pcBatch.ActivePage <> tsBatch;
  if (iWizardPosition > WIZ_UNKNOWN) and (iWizardPosition < WIZ_PREPROCESSED) then
    if DM_PAYROLL.PR_BATCH.RecordCount = 0 then
      iWizardPosition := WIZ_PAYROLL_CREATED
    else if pcBatch.ActivePage = tsBatch then
      iWizardPosition := WIZ_BATCH_CREATED
    else if pcBatch.ActivePage = tsCheck then
      iWizardPosition := WIZ_CHECKS_CREATED
    else if pcBatch.ActivePage = tsEarnings then
      iWizardPosition := WIZ_TOTALS_AFTER_CHECKS;

  cbRates.Visible := (pcBatch.ActivePage = tsCheck) or (pcBatch.ActivePage = tsQEDGrid);
  lblSalary.Visible := cbRates.Visible;

  if (pcBatch.ActivePage = tsCheck)  then
    RefreshEErates( QUICK_PAY_DS.DataSet.FieldByName('EE_NBR').AsInteger )
  else if (pcBatch.ActivePage = tsQEDGrid) then
    RefreshEErates( QuickEDGrid.dsQED.DataSet.FieldByName('EE_NBR').AsInteger );

  //GUI 2.0, HARD CODE BATCH COLUMN WIDTHS. CAN'T BE DONE IN .DFM
  //column widths on Earnings/Deductions
  EDTotals.dgEDTotal.Columns[0].DisplayWidth := 10;
  EDTotals.dgEDTotal.Columns[1].DisplayWidth := 24;
  EDTotals.dgEDTotal.Columns[4].DisplayWidth := 5;

  //column widths on Earnings/Deductions
  EDTotals.dgEDDetail.Columns[0].DisplayWidth := 7;
  EDTotals.dgEDDetail.Columns[1].DisplayWidth := 17;
  EDTotals.dgEDDetail.Columns[2].DisplayWidth := 12;
  EDTotals.dgEDDetail.Columns[3].DisplayWidth := 10;

  //Batch, Taxes, Tax Codes
  wwDBGrid4.Columns[0].DisplayWidth := 32;
  wwDBGrid4.Columns[1].DisplayWidth := 9;
end;

procedure TREDIT_BATCH.SetGridPosition;
var
  i: Integer;
begin
  if QUICK_PAY_DS.DataSet.RecordCount > 0 then
  begin
    for i := 0 to Pred(wwDBGrid1.Selected.Count) do
      if (Pos(#9'Hrs', wwDBGrid1.Selected[i]) > 0) or
         (Pos(#9'Amt', wwDBGrid1.Selected[i]) > 0) or
         (Pos(#9'Tax', wwDBGrid1.Selected[i]) > 0) then
      begin
        wwDBGrid1.SelectedIndex := i;
        Break;
      end;
  end
  else
    wwDBGrid1.SelectedIndex := 0;
end;


procedure TREDIT_BATCH.DeleteClick(Sender: TObject);
var
  Mark: TBookmarkStr;
  s: string;
  i: Integer;
begin
  s := Copy(TMenuItem(Sender).Parent.Caption, 1, Pred(Pos(' ', TMenuItem(Sender).Parent.Caption)));
  DeleteQPColumn(s, TMenuItem(Sender).Caption[1]);
  Mark := QUICK_PAY_DS.DataSet.Bookmark;
  ResetCalc;
  Calculate_BATCH_QuickPay;
  try
    QUICK_PAY_DS.DataSet.Bookmark := Mark;
  except
  end;

  TMenuItem(Sender).Visible := False;
  AddItem.Items[TMenuItem(Sender).Parent.MenuIndex].Items[TMenuItem(Sender).MenuIndex].Visible := True;

  TMenuItem(Sender).Parent.Visible := CountVisibleItems(TMenuItem(Sender).Parent) > 0;
  AddItem.Items[TMenuItem(Sender).Parent.MenuIndex].Visible := CountVisibleItems(AddItem.Items[TMenuItem(Sender).Parent.MenuIndex]) > 0;

  UpdateQPMenuStatus;
  
  for i := 0 to Pred(wwDBGrid1.Selected.Count) do
    if UpperCase(Copy(wwDBGrid1.Selected[i], 1, Pred(Pos(#9, wwDBGrid1.Selected[i])))) = UpperCase(s + '_' + TMenuItem(Sender).Caption) then
    begin
      wwDBGrid1.Selected.Delete(i);
      Break;
    end;
  wwDBGrid1.ApplySelected;
  QUICK_PAY_DS.DataSet.DisableControls;
  QUICK_PAY_DS.DataSet.EnableControls;
end;

procedure TREDIT_BATCH.AddClick(Sender: TObject);
var
  Mark: TBookmarkStr;
  s, s1: string;
  sReadOnly: Char;
begin
//  EvMessage(TMenuItem(Sender).Parent.Caption);
  s := Copy(TMenuItem(Sender).Parent.Caption, 1, Pred(Pos(' ', TMenuItem(Sender).Parent.Caption)));
  AddQPColumn(s, TMenuItem(Sender).Caption[1]);
  Mark := QUICK_PAY_DS.DataSet.Bookmark;
  ResetCalc;
  Calculate_BATCH_QuickPay;
  try
    QUICK_PAY_DS.DataSet.Bookmark := Mark;
  except
  end;

  TMenuItem(Sender).Visible := False;
  DeleteItem.Items[TMenuItem(Sender).Parent.MenuIndex].Items[TMenuItem(Sender).MenuIndex].Visible := True;

  TMenuItem(Sender).Parent.Visible := CountVisibleItems(TMenuItem(Sender).Parent) > 0;
  DeleteItem.Items[TMenuItem(Sender).Parent.MenuIndex].Visible := CountVisibleItems(DeleteItem.Items[TMenuItem(Sender).Parent.MenuIndex]) > 0;

  UpdateQPMenuStatus;

  s1 := TMenuItem(Sender).Parent.Caption;
  s1[Pos(' ', s1)] := '~';
  if ctx_AccountRights.Functions.GetState('FIELDS_WAGES') <> stEnabled then
    sReadOnly := 'T'
  else
    sReadOnly := 'F';
  if TMenuItem(Sender).Caption = 'Amt' then
    wwDBGrid1.Selected.Add(TMenuItem(Sender).Caption + '_' + s + #9'10'#9 + TMenuItem(Sender).Caption + ' ' + s1 + #9 + sReadOnly)
  else
    wwDBGrid1.Selected.Add(TMenuItem(Sender).Caption + '_' + s + #9'10'#9 + TMenuItem(Sender).Caption + ' ' + s1 + #9'F');
  wwDBGrid1.ApplySelected;

  QUICK_PAY_DS.DataSet.DisableControls;
  QUICK_PAY_DS.DataSet.EnableControls;
end;

//gdy
function ReplaceCaptionForField( aSelected, aFieldName, oldCaption, newCaption: string ): string;
var
  aPos: integer;
  f,w,c,ro: string;
begin
  aPos := 1;
  f := strGetToken( aSelected, #9, aPos );
  w := strGetToken( aSelected, #9, aPos );
  c := strGetToken( aSelected, #9, aPos );
  ro := strGetToken( aSelected, #9, aPos );
  if (AnsiCompareText( f, aFieldName ) = 0) and (AnsiCompareText( c, oldCaption ) = 0) then
    Result := f + #9 + w + #9 + newCaption+ #9 + ro
  else
    Result := aSelected;
end;

procedure TREDIT_BATCH.Activate;
var
  i: Integer;
  jA, jH: integer;
  mItem, mItemS: TMenuItem;
  t: IisStringListRO;
  s, sCode: Variant;
  sReadOnly: Char;
  st: string;
  CurrPage : Integer;

  function FindTaxItem(const Description: string): Integer;
  var
    i: Integer;
  begin
    Result := -1;
    for i := 0 to Pred(Length(Batch_E_DS)) do
      if (Batch_E_DS[i].Description = ReplaceChars(Description)) and
         (Batch_E_DS[i].FieldType = 'T') then
      begin
        Result := i;
        Exit;
      end;
  end;

begin
  SubmitBtn.Enabled := (DM_PAYROLL.PR.State = dsBrowse) and
      (ctx_AccountRights.Functions.GetState('USER_CAN_SUBMIT_PAYROLL') = stEnabled);

  btnPreProcessBatch.Enabled := (ctx_AccountRights.Functions.GetState('USER_CAN_PREPROCESS_BATCH') = stEnabled);

  FEENbr := -1;
  ImportingTimeClock := False;
  lEeFilter.Caption := ctx_PayrollCalculation.GetEeFilterCaption;
  ctx_DataAccess.CheckMaxAmountAndHours := True;
  chbCalcScheduledEDs.Checked := mb_AppSettings.GetValue('CalcSchedEDs', True);
  IncludeTimeOffRequests.Tag := 1;
  IncludeTimeOffRequests.Checked := mb_AppSettings.AsBoolean['IncludeTimeOffRequests'];
  IncludeTimeOffRequests.Tag := 0;
  IncludeTimeOffRequests.Enabled := not (bInsertWithCobraWizard or FNewCobraBatch);
  if ctx_AccountRights.Functions.GetState('FIELDS_WAGES') <> stEnabled then
    sReadOnly := 'T'
  else
    sReadOnly := 'F';

   //  Added this control to retrieve PR_CHECKS dataset, because on check screen dataset is broken...
  if ctx_PayrollCalculation.GetEeFilter <> '' then
    with DM_PAYROLL do
    begin
      PR_CHECK.CheckChildDSAndClient(ctx_DataAccess.ClientID, 'EE_NBR', DM_EMPLOYEE.EE, 'PR_NBR = ' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
      PR_CHECK.Filter := '';
      ctx_DataAccess.OpenDataSets([PR_CHECK]);
    end;

  s := wwDBGrid1.Selected.Text;
  DM_PAYROLL.PR_CHECK_LINES.RetrieveCondition := '';
  DM_PAYROLL.PR_CHECK_LINE_LOCALS.RetrieveCondition := '';
  DM_PAYROLL.PR_CHECK_LOCALS.RetrieveCondition := '';
  DM_PAYROLL.PR_CHECK_STATES.RetrieveCondition := '';
  DM_PAYROLL.PR_CHECK_SUI.RetrieveCondition := '';
  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.RetrieveCondition := '';

  DM_COMPANY.CO_JOBS.Filter := 'JOB_ACTIVE=''Y''';
  DM_COMPANY.CO_JOBS.Filtered := True;

  cbSeperateOverrides.Checked := SeparateTax;
  cbRates.DropDownWidth := 350;

  pcBatch.ActivatePage(tsBatch);
  tsContacts.TabVisible := CheckRightsForContactsTab();

  inherited;

  if (wwdsList.DataSet.RecordCount <=0) then exit;
  if not CalcForBatch then
  begin
    ResetCalc;
    CalcForBatch := True;
  end;

  QuickEDGrid.Activate;
  EDTotals.Activate;

  wwDBGrid1.DataSource := QUICK_PAY_DS;
  dbtEEName.DataSource := QUICK_PAY_DS;
  dbtEENumber.DataSource := QUICK_PAY_DS;

  wwDBGrid4.DataSource := dsTax_Total;
  wwDBGrid5.DataSource := dsTax_Detail;

  PR_LABEL.Caption := GetPayrollDescription;

  tsAutoReduction.TabVisible := (DetailPayrollOpen <> 0) and (DM_PAYROLL.PR.FieldByName('STATUS').AsString = 'W');

  ClearQPColumns;
  wwDBGrid1.Selected.Clear;

  if mb_AppSettings.AsInteger['Misc\' + ClassName + '\wwDBGrid1\Version'] <> wwDBGrid1.Tag then
    mb_AppSettings.DeleteNode('Misc\' + Self.ClassName + '\wwDBGrid1');

  t := mb_AppSettings.GetValueNames('Misc\' + Self.ClassName + '\wwDBGrid1\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));
  for i := 0 to t.Count - 1 do
  begin
    sCode := mb_AppSettings['Misc\' + Self.ClassName + '\wwDBGrid1\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
      IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger) + '\' + t[i]];
    s := Copy(sCode, 1, 4);
    if ((s = 'Hrs_') or (s = 'Amt_')) and
      not DM_COMPANY.CO_E_D_CODES.Locate('ED_Lookup', Copy(sCode, 5, Pos(#9, sCode) - 5), []) then
     Continue;
    wwDBGrid1.Selected.Add(ReplaceCaptionForField(sCode, 'NUMBER', 'Number', 'EE Code')); //gdy

    if Pos(#9'Hrs', wwDBGrid1.Selected[i]) > 0 then
    begin
      s := Copy(wwDBGrid1.Selected[i], Succ(Pos('_', wwDBGrid1.Selected[i])), Length(wwDBGrid1.Selected[i]));
      s := Copy(s, 1, Pred(Pos(#9, s)));
      AddQPColumn(s, 'H');
    end
    else if Copy(wwDBGrid1.Selected[i], 1, 4) = 'Tax_' then
    begin
      s := Copy(wwDBGrid1.Selected[i], 5, Pred(Pos(#9, wwDBGrid1.Selected[i])) - 4);
      AddQPColumn(s, 'T');
    end
    else if Pos(#9'Amt', wwDBGrid1.Selected[i]) > 0 then
    begin
      s := Copy(wwDBGrid1.Selected[i], Succ(Pos('_', wwDBGrid1.Selected[i])), Length(wwDBGrid1.Selected[i]));
      s := Copy(s, 1, Pred(Pos(#9, s)));
      AddQPColumn(s, 'A');
      if (Copy(wwDBGrid1.Selected[i], Pred(Length(wwDBGrid1.Selected[i])), 2) <> #9'T') and
         (Copy(wwDBGrid1.Selected[i], Pred(Length(wwDBGrid1.Selected[i])), 2) <> #9'F') then
        wwDBGrid1.Selected[i] := wwDBGrid1.Selected[i] + #9 + sReadOnly
      else
      begin
        st := wwDBGrid1.Selected[i];
        st[Length(wwDBGrid1.Selected[i])] := sReadOnly;
        wwDBGrid1.Selected[i] := st;
      end;
    end
  end;

  if wwDBGrid1.Selected.Count = 0 then
  begin
    wwDBGrid1.Selected.Add('CHECK_TYPE'#9'2'#9'Type'#9'T');
    wwDBGrid1.Selected.Add('NOTES'#9'2'#9'Notes'#9'T');
    wwDBGrid1.Selected.Add('NUMBER'#9'10'#9'EE Code'#9'F');
    wwDBGrid1.Selected.Add('NAME'#9'40'#9'Name'#9'F');
    if DM_CLIENT.CL_E_DS.Locate('E_D_CODE_TYPE', ED_OEARN_SALARY, []) and
       DM_COMPANY.CO_E_D_CODES.Locate('CL_E_DS_NBR', DM_CLIENT.CL_E_DS['CL_E_DS_NBR'], []) then
    begin
      s := DM_CLIENT.CL_E_DS['CUSTOM_E_D_CODE_NUMBER;DESCRIPTION'];
      sCode := s[0];
      s := VarToStr(s[0]) + '~' + VarToStr(s[1]);
      wwDBGrid1.Selected.Add('Amt_' + sCode + #9'10'#9 + 'Amt ' + s + #9 + sReadOnly);
      AddQPColumn(sCode, 'A')
    end;
    if DM_CLIENT.CL_E_DS.Locate('E_D_CODE_TYPE', ED_OEARN_REGULAR, []) and
       DM_COMPANY.CO_E_D_CODES.Locate('CL_E_DS_NBR', DM_CLIENT.CL_E_DS['CL_E_DS_NBR'], []) then
    begin
      s := DM_CLIENT.CL_E_DS['CUSTOM_E_D_CODE_NUMBER;DESCRIPTION'];
      sCode := s[0];
      s := VarToStr(s[0]) + '~' + VarToStr(s[1]);
      wwDBGrid1.Selected.Add('Hrs_' + sCode + #9'10'#9'Hrs ' + s + #9'F');
      AddQPColumn(sCode, 'H');
      wwDBGrid1.Selected.Add('Amt_' + sCode + #9'10'#9'Amt ' + s + #9 + sReadOnly);
      AddQPColumn(sCode, 'A')
    end;
  end;

  with DM_COMPANY.CO_E_D_CODES do
  try
    IndexFieldNames := 'ED_Lookup';
    First;
    while not Eof do
    begin
      jA := -1;
      jH := -1;
      for i := 0 to Pred(Length(Batch_E_DS)) do
        if FieldByName('ED_Lookup').AsString = Batch_E_DS[i].Description then
        begin
          if Batch_E_DS[i].FieldType = 'H' then
            jH := i;
          if Batch_E_DS[i].FieldType = 'A' then
            jA := i;
          if (jH >= 0) and (jA >= 0) then
            Break;
        end;

      mItem := TMenuItem.Create(Self);
      mItem.Caption := FieldByName('ED_Lookup').AsString + ' ' +
        FieldByName('CodeDescription').AsString;
      mItem.Visible := (jH >= 0) or (jA >= 0);
      DeleteItem.Add(mItem);
      mItemS := TMenuItem.Create(Self);
      mItemS.Caption := 'Hrs';
      mItemS.OnClick := DeleteClick;
      mItemS.Visible := jH >= 0;
      mItem.Add(mItemS);
      mItemS := TMenuItem.Create(Self);
      mItemS.Caption := 'Amt';
      mItemS.OnClick := DeleteClick;
      mItemS.Visible := jA >= 0;
      mItem.Add(mItemS);


      mItem := TMenuItem.Create(Self);
      mItem.Caption := FieldByName('ED_Lookup').AsString + ' ' +
        FieldByName('CodeDescription').AsString;
      mItem.Visible := (jH < 0) or (jA < 0);
      AddItem.Add(mItem);
      mItemS := TMenuItem.Create(Self);
      mItemS.Caption := 'Hrs';
      mItemS.OnClick := AddClick;
      mItemS.Visible := jH < 0;
      mItem.Add(mItemS);
      mItemS := TMenuItem.Create(Self);
      mItemS.Caption := 'Amt';
      mItemS.OnClick := AddClick;
      mItemS.Visible := jA < 0;
      mItem.Add(mItemS);

      Next;
    end;
  finally
    IndexFieldNames := '';
  end;

  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'Federal';
  mItem.Visible := FindTaxItem(mItem.Caption) < 0;
  mItem.OnClick := AddTaxClick;
  AddTaxItem.Add(mItem);
  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'Federal';
  mItem.Visible := FindTaxItem(mItem.Caption) >= 0;
  mItem.OnClick := DeleteTaxClick;
  DeleteTaxItem.Add(mItem);

  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'Medicare';
  mItem.Visible := FindTaxItem(mItem.Caption) < 0;
  mItem.OnClick := AddTaxClick;
  AddTaxItem.Add(mItem);
  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'Medicare';
  mItem.Visible := FindTaxItem(mItem.Caption) >= 0;
  mItem.OnClick := DeleteTaxClick;
  DeleteTaxItem.Add(mItem);

  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'OASDI';
  mItem.Visible := FindTaxItem(mItem.Caption) < 0;
  mItem.OnClick := AddTaxClick;
  AddTaxItem.Add(mItem);
  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'OASDI';
  mItem.Visible := FindTaxItem(mItem.Caption) >= 0;
  mItem.OnClick := DeleteTaxClick;
  DeleteTaxItem.Add(mItem);

  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'EIC';
  mItem.Visible := FindTaxItem(mItem.Caption) < 0;
  mItem.OnClick := AddTaxClick;
  AddTaxItem.Add(mItem);
  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'EIC';
  mItem.Visible := FindTaxItem(mItem.Caption) >= 0;
  mItem.OnClick := DeleteTaxClick;
  DeleteTaxItem.Add(mItem);

  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'Backup';
  mItem.Visible := FindTaxItem(mItem.Caption) < 0;
  mItem.OnClick := AddTaxClick;
  AddTaxItem.Add(mItem);
  mItem := TMenuItem.Create(Self);
  mItem.Caption := 'Backup';
  mItem.Visible := FindTaxItem(mItem.Caption) >= 0;
  mItem.OnClick := DeleteTaxClick;
  DeleteTaxItem.Add(mItem);

  DM_COMPANY.CO_SUI.IndexFieldNames := 'State';
  with DM_COMPANY.CO_STATES do
  try
    IndexFieldNames := 'State';
    First;
    while not Eof do
    begin
      mItem := TMenuItem.Create(Self);
      mItem.Caption := FieldByName('State').AsString + '-State';
      mItem.Visible := FindTaxItem(mItem.Caption) < 0;
      mItem.OnClick := AddTaxClick;
      AddTaxItem.Add(mItem);
      mItem := TMenuItem.Create(Self);
      mItem.Caption := FieldByName('State').AsString + '-State';
      mItem.Visible := FindTaxItem(mItem.Caption) >= 0;
      mItem.OnClick := DeleteTaxClick;
      DeleteTaxItem.Add(mItem);

      mItem := TMenuItem.Create(Self);
      mItem.Caption := FieldByName('State').AsString + '-SDI';
      mItem.Visible := FindTaxItem(mItem.Caption) < 0;
      mItem.OnClick := AddTaxClick;
      AddTaxItem.Add(mItem);
      mItem := TMenuItem.Create(Self);
      mItem.Caption := FieldByName('State').AsString + '-SDI';
      mItem.Visible := FindTaxItem(mItem.Caption) >= 0;
      mItem.OnClick := DeleteTaxClick;
      DeleteTaxItem.Add(mItem);

      DM_COMPANY.CO_SUI.FindKey([FieldValues['State']]);
      while not DM_COMPANY.CO_SUI.Eof and (DM_COMPANY.CO_SUI['State'] = FieldValues['State'])do
      begin
        mItem := TMenuItem.Create(Self);
        mItem.Caption := DM_COMPANY.CO_SUI.FieldByName('Description').AsString;
        mItem.Visible := FindTaxItem(mItem.Caption) < 0;
        mItem.OnClick := AddTaxClick;
        AddTaxItem.Add(mItem);
        mItem := TMenuItem.Create(Self);
        mItem.Caption := DM_COMPANY.CO_SUI.FieldByName('Description').AsString;
        mItem.Visible := FindTaxItem(mItem.Caption) >= 0;
        mItem.OnClick := DeleteTaxClick;
        DeleteTaxItem.Add(mItem);

        DM_COMPANY.CO_SUI.Next;
      end;

      Next;
    end;
  finally
    IndexFieldNames := '';
    DM_COMPANY.CO_SUI.IndexFieldNames := '';
  end;

  with DM_COMPANY.CO_LOCAL_TAX do
  try
    IndexFieldNames := 'LocalState';
    First;
    while not Eof do
    begin
      mItem := TMenuItem.Create(Self);
      mItem.Caption := FieldByName('LocalName').AsString;
      mItem.Visible := FindTaxItem(mItem.Caption) < 0;
      mItem.OnClick := AddTaxClick;
      AddTaxItem.Add(mItem);
      mItem := TMenuItem.Create(Self);
      mItem.Caption := FieldByName('LocalName').AsString;
      mItem.Visible := FindTaxItem(mItem.Caption) >= 0;
      mItem.OnClick := DeleteTaxClick;
      DeleteTaxItem.Add(mItem);

      Next;
    end;
  finally
    IndexFieldNames := '';
  end;

  UpdateQPMenuStatus;

  FQPInit := True;

  if iWizardPosition in [WIZ_PAYROLL_CREATED] then
    pcBatch.ActivatePage(tsBatch)
  else if iWizardPosition in [WIZ_BATCH_CREATED] then
    pcBatch.ActivatePage(tsCheck)
  else if iWizardPosition in [WIZ_CHECKS_CREATED] then
    pcBatch.ActivatePage(tsEarnings)
  else if CurrentBatchTabSheet = '' then
    if DM_PAYROLL.PR_BATCH.RecordCount <> 1 then
      pcBatch.ActivatePage(tsBatch)
    else
      pcBatch.ActivatePage(tsCheck)
  else
  begin
    s := CurrentBatchTabSheet;
    pcBatch.ActivatePage(tsBatch);

    for I := 0 to pcBatch.PageCount - 1 do
      if pcBatch.Pages[I].Name = s then
      begin
        pcBatch.ActivatePage(pcBatch.Pages[I]);
        break;
      end;
  end;


  if DM_PAYROLL.PR_BATCH.RecordCount = 0 then
         tsImport.TabVisible := false;   //   added to Control if Batch not saved dont to import...

//  wwDBGrid1.ApplySelected;

  if bNewPayroll then
  try
    try
      bNewPayroll := False;
      if bInsertWithWizard then
      begin
        wwDBLookupCombo1.DataSource := nil;
        try
          BitBtn1.Click;
        finally
          wwDBLookupCombo1.DataSource := wwdsDetail;
        end;
      end;
    except
      on E: EAbort do ;
      else
        raise;
    end;
  finally
    bInsertWithWizard := False;
  end;
  QUICK_PAY_DS.OnDataChange := QUICK_PAY_DSDataChange;
  QuickEDGrid.dsQED.OnDataChange := QuickEDGriddsQEDDataChange;

//  ctx_DataAccess.RecalcLineBeforePost := cbCalcCL.Checked;
  ctx_DataAccess.RecalcLineBeforePost := True;
  RestoreComponentState(editImportSourceFile);
  RestoreComponentState(editImportReportFile);
  RestoreComponentState(radioImportLookByNumber);
  RestoreComponentState(radioImportLookByName);
  RestoreComponentState(radioImportLookBySSN);
  RestoreComponentState(radioImportMatchDbdtFull);
  RestoreComponentState(radioImportFormatFFF);
  RestoreComponentState(radioImportFormatCSF);
  RestoreComponentState(cbFourDigits);
  RestoreComponentState(cbAutoImportJobCodes);
  RestoreComponentState(radioImportMatchDbdtPartial);
  RestoreComponentState(cbUseEmployeePayRates);
  RestoreComponentState(cbAuoRefreshEDs);
  if not radioImportLookByNumber.Checked and not radioImportLookByName.Checked and not radioImportLookBySSN.Checked then
    radioImportLookByNumber.Checked := True;
  if not radioImportMatchDbdtFull.Checked and not radioImportMatchDbdtPartial.Checked then
    radioImportMatchDbdtFull.Checked := True;
  if not radioImportFormatFFF.Checked and not radioImportFormatCSF.Checked then
    radioImportFormatFFF.Checked := True;

  s := wwDBGrid1.Selected.Text;
  DM_PAYROLL.PR_BATCH.AfterPost := PBAfterPost;
  DM_PAYROLL.PR_BATCH.AfterInsert := PBAfterInsert;
  wwDBGrid1.ReadOnly := CheckGridReadOnly;

  evBitBtn1.SecurityRO := GetIfReadOnly or (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT);
  //the totals fields aren't showing up on Quick Entry unless you fire the
  //following event. This happens the first time you arrive to this page,
  //or, if you navigate back to this screen while Quick Entry is the current tab.
  CurrPage := pcBatch.ActivePageIndex;
  pcBatch.OnChange(self);
  pcBatch.ActivePageIndex := CurrPage;
end;

procedure TREDIT_BATCH.cbSeperateOverridesClick(Sender: TObject);
begin
  inherited;
  if SeparateTax <> cbSeperateOverrides.Checked then
  begin
    SeparateTax := cbSeperateOverrides.Checked;
    ReCalculate_TAX_TOTALS(cbSeperateOverrides.Checked);
  end;
end;

procedure TREDIT_BATCH.BitBtn1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavInsert);
end;

procedure TREDIT_BATCH.EE_LookupComboNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: string; var Accept: Boolean);
begin
  inherited;
  Accept := False;
end;

procedure TREDIT_BATCH.BitBtn2Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).ClickButton(NavDelete);
end;

procedure TREDIT_BATCH.wwDBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  XLS: TXLSFile;
  Q: IevQuery;
begin
  //the following code was brought forward from the isClasses ancestor
  //for some unknown reason, likely the painting routines, this never
  //gets fired.
  if wwDBGrid1.LocateDlg and (Char(Key) = 'F') and (Shift = [ssCtrl]) then
    wwDBGrid1.ExecLocateDlg;
  if (Char(Key) = 'Y') and (Shift = [ssCtrl]) then
    wwDBGrid1.ExecHideDlg;
  if wwDBGrid1.FilterDlg and Assigned(wwDBGrid1.IndicatorButton) and (Char(Key) = 'R') and (Shift = [ssCtrl]) then
    wwDBGrid1.IndicatorButton.Click;
  if (Char(Key) = 'E') and (Shift = [ssCtrl]) then
  begin
    with TSaveDialog.Create(nil) do
    try
      Options := Options + [ofOverwritePrompt];
      DefaultExt := 'xls';
      Filter := 'XLS files (*.xls)|*.xls|SYLK files (*.slk)|*.slk';
      if Execute then
      begin
        if FilterIndex = 1 then
        begin
          XLS := TXLSFile.Create;
          try
            GridToXls(wwDBGrid1, XLS);
            XLS.SaveAs(FileName);
          finally
            XLS.Free;
          end;
        end
        else
        begin
          wwDBGrid1.ExportOptions.FileName := FileName;
          wwDBGrid1.ExportOptions.Options := wwDBGrid1.ExportOptions.Options - [esoClipboard];
          wwDBGrid1.ExportOptions.Save;
        end;
      end;
    finally
      Free;
    end;
  end;

  inherited;
  //end of ctrl-e override

  if (Key = VK_RETURN) and (Shift = [ssCtrl]) then
  begin
    GotoChecks(Self, QUICK_PAY_DS.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
    Key := 0;
  end;
  if (Key = VK_DELETE) and (Shift = [ssCtrl]) then
  begin
    if not wwDBGrid1.ReadOnly and (wwDBGrid1.DataSource.DataSet.RecordCount > 0) and
      (EvMessage('Delete this check?', mtConfirmation, mbOKCancel) = mrOK) then
    begin
      if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
      begin
        wwDBGrid1.DataSource.Dataset.DisableControls;
        try
          (Owner as TFramePackageTmpl).ClickButton(NavOK);
          (Owner as TFramePackageTmpl).ClickButton(NavCommit);
          ctx_DBAccess.StartTransaction([dbtClient]);
          try
            Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
            Q.Execute;
            wwDBGrid1.DataSource.DataSet.Delete;
            (Owner as TFramePackageTmpl).ClickButton(NavOK);
            (Owner as TFramePackageTmpl).ClickButton(NavCommit);
            ctx_DBAccess.CommitTransaction;
          except
            ctx_DBAccess.RollbackTransaction;
            raise;
          end;
        finally
          wwDBGrid1.DataSource.Dataset.EnableControls;
        end;
      end
      else
      begin
        wwDBGrid1.DataSource.Dataset.DisableControls;
        try
          wwDBGrid1.DataSource.DataSet.Delete;
          (Owner as TFramePackageTmpl).ClickButton(NavOK);
          (Owner as TFramePackageTmpl).ClickButton(NavCommit);
        finally
          wwDBGrid1.DataSource.Dataset.EnableControls;
        end;
      end;
    end;
    Key := 0;
  end;
end;

procedure TREDIT_BATCH.wwDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  GotoChecks(Self, QUICK_PAY_DS.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_BATCH.wwDBGrid5DblClick(Sender: TObject);
begin
  inherited;
  GotoChecks(Self, wwDBGrid5.DataSource.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_BATCH.wwDBGrid5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    GotoChecks(Self, wwDBGrid5.DataSource.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TREDIT_BATCH.EE_LookupComboCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if (QUICK_PAY_DS.DataSet.State in [dsInsert]) and
    (Trim(EE_LookupCombo.Text) <> '') then
  begin
    QUICK_PAY_DS.DataSet.FieldByName('NAME').ReadOnly := False;
    QUICK_PAY_DS.DataSet.FieldByName('NAME').Value := DM_EMPLOYEE.EE.Lookup('Custom_Employee_Number',
                                                      EE_LookupCombo.LookupValue, 'Employee_Name_Calculate');
    QUICK_PAY_DS.DataSet.FieldByName('NAME').ReadOnly := True;
  end;

  if QUICK_PAY_DS.DataSet.State in [dsEdit] then
  begin
    QUICK_PAY_DS.DataSet.Cancel;
    raise EUpdateError.CreateHelp('Can not change an employee for the existing check.', IDH_ConsistencyViolation);
  end;
end;

procedure TREDIT_BATCH.QuickEDGriddsQEDDataChange(Sender: TObject; Field: TField);
begin
 // RefreshEEStates;
  QuickEDGrid.dsQEDDataChange(Sender, Field);
  RefreshEErates( QuickEDGrid.dsQED.DataSet.FieldByName('EE_NBR').AsInteger );
end;

procedure TREDIT_BATCH.QUICK_PAY_DSDataChange(Sender: TObject; Field: TField);
var
  i: integer;
  Col: TwwColumn;
{// Aggregates bug fix begin
  t: TevClientDataSet;
// Aggregates bug fix end}
begin
 // RefreshEEStates;
  inherited;
{// Aggregates bug fix begin
  t := TevClientDataSet.Create(Self);
  t.CloneCursor(QUICK_PAY_DS.DataSet as TevClientDataSet, True);
  for i := 0 to Pred(TevClientDataSet(QUICK_PAY_DS.DataSet).Aggregates.Count) do
    with t.Aggregates.Add do
    begin
      AggregateName := TevClientDataSet(QUICK_PAY_DS.DataSet).Aggregates[i].AggregateName;
      Expression := 'Sum(' + TevClientDataSet(QUICK_PAY_DS.DataSet).Aggregates[i].AggregateName + ')';
      Active := True;
    end;
// Aggregates bug fix end}

  for i := QPFieldCount to Pred(QUICK_PAY_DS.DataSet.FieldCount) do
  begin
    Col := wwDBGrid1.ColumnByName(QUICK_PAY_DS.DataSet.Fields[i].DisplayLabel);
    if Col <> nil then
      Col.FooterValue := FormatFloat(TFloatField(QUICK_PAY_DS.DataSet.Fields[i]).DisplayFormat,
        ConvertNull(TevClientDataSet(QUICK_PAY_DS.DataSet).Aggregates[TevClientDataSet(QUICK_PAY_DS.DataSet).Aggregates.IndexOf(QUICK_PAY_DS.DataSet.Fields[i].FieldName)].Value, 0));
  end;

{// Aggregates bug fix begin
  t.Aggregates.Clear;
  t.Free;
// Aggregates bug fix end}
  if Assigned(QUICK_PAY_DS.DataSet) and QUICK_PAY_DS.DataSet.Active then
  begin
    SaveLastCheckPosition(QUICK_PAY_DS.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
    RefreshEErates( QUICK_PAY_DS.DataSet.FieldByName('EE_NBR').AsInteger );
  end;
end;

function TREDIT_BATCH.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly or
  (not DM_PAYROLL.PR.IsEmpty
   and (Length(DM_PAYROLL.PR.FieldByName('Status').AsString) > 0)
   and
  (DM_PAYROLL.PR.FieldByName('Status').AsString[1]
    in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_COMPLETED, PAYROLL_STATUS_HOLD,
    PAYROLL_STATUS_VOIDED, PAYROLL_STATUS_PROCESSING, PAYROLL_STATUS_PREPROCESSING])
   );
end;

procedure TREDIT_BATCH.YeartodateItemClick(Sender: TObject);
begin
  if not Assigned(QUICK_PAY_DS.DataSet) or
     not QUICK_PAY_DS.DataSet.Active or
     (QUICK_PAY_DS.DataSet.RecordCount = 0) then
    Exit;
  with CreateCoYTD do
  begin
    ClNbr := ctx_DataAccess.ClientID;
    CoNbr := DM_COMPANY.CO['co_nbr'];
    EeNbr := QUICK_PAY_DS.DataSet['ee_nbr'];
    ShowModal;
//    Free;
  end;
end;

procedure TREDIT_BATCH.wwDBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  if (gdFixed in State) and
     (ABrush.Color = TevDBGrid(Sender).TitleColor) then
    if not TevDBGrid(Sender).IsAlternatingRow(TevDBGrid(Sender).CalcCellRow) then
      ABrush.Color := TevDBGrid(Sender).Color
    else
      ABrush.Color := TevDBGrid(Sender).PaintOptions.AlternatingRowColor;

  if assigned(Field) and assigned(Field.DataSet) and Field.DataSet.Active then
  begin
    if (Field.DataSet.FieldByName('NOTES').AsString = 'Y') then
    begin

      if not Highlight then
      begin
        AFont.Color := clBlack; //!!temp fix
        ABrush.Color := clYellow;
      end
      else
      begin
        AFont.Color := clBlack; //!!temp fix
        ABrush.Color := clOlive;
      end;
    end;

  end;    
end;

procedure TREDIT_BATCH.AfterClick(Kind: Integer);
var
  sPR, sBatch, CheckType, Check945Type, EENbrList: String;
  PrNbr, PrBatchNbr,PrCheckNbr,PrCheckNbr2: Integer;
  b: Boolean;
  V: Variant;
  //iRec: Integer;
  SaveFilterCond: String;
  SaveFilterState: Boolean;
  I : integer;
  bAutoCreateCheck : Boolean;
  UpdateBalance: Boolean;
  DS: TevClientDataset;
  TOAName, EEName: String;
begin
  inherited;
  SaveFilterState := False;
  if Kind = NavInsert then
  try
    NewBatch := True;
    with ctx_PayrollCalculation do
    begin
      if DM_PAYROLL.PR.FieldByName('SCHEDULED').Value = 'Y' then
      begin
        b := FindNextPeriodBeginEndDate;
        if DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.RecordCount > 0 then
        begin
          DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.First;
          with BreakCompanyFreqIntoPayFreqList(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['FREQUENCY']) do
          try
            DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Strings[0];
          finally
            Free;
          end;
        end
        else
          DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Copy(GetCompanyPayFrequencies, Pos(#9, GetCompanyPayFrequencies) + 1, 1);

        if not b then
        begin
          DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := NextPeriodBeginDate;
          DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := NextPeriodEndDate;
        end
        else
        begin
          DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_BEGIN_DATE'];
          DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['PERIOD_END_DATE'];
        end;
      end
      else
      begin
        DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value := Copy(GetCompanyPayFrequencies, Pos(#9, GetCompanyPayFrequencies) + 1, 1);
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := NextPeriodBeginDate;
        DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := NextPeriodEndDate;
      end;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE_STATUS').Value := DATE_STATUS_NORMAL;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE_STATUS').Value := DATE_STATUS_NORMAL;
    end;
    b := ctx_DataAccess.RecalcLineBeforePost;

    if bInsertWithCobraWizard or FNewCobraBatch then
    begin
      chbCalcScheduledEDs.Checked := True;
      rgPay.ItemIndex := 0;
//      rgCreateChecks.ItemIndex := 1;
      // Set Cobra Default Values
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').Value := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
      DM_PAYROLL.PR_BATCH.FieldByName('PAY_SALARY').Value := 'N';
      DM_PAYROLL.PR_BATCH.FieldByName('PAY_STANDARD_HOURS').Value := 'N';
      DM_PAYROLL.PR_BATCH.FieldByName('LOAD_DBDT_DEFAULTS').Value := 'N';
    end;

    if bInsertWithWizard or bInsertWithCobraWizard then
    try
      ctx_DataAccess.RecalcLineBeforePost := True;
      DM_PAYROLL.PR_BATCH.DisableControls;
      try
        if bInsertWithWizard and not FNewCobraBatch then
          Generic_PopulatePRBatchRecord;
        if bInsertWithCobraWizard then
          Generic_PopulatePRBatchRecord(True);
      finally
        DM_PAYROLL.PR_BATCH.EnableControls;
      end;
      (Owner as TFramePackageTmpl).ClickButton(NavOK);
      (Owner as TFramePackageTmpl).ClickButton(NavCommit);
    finally
      ctx_DataAccess.RecalcLineBeforePost := b;
    end;
    CheckSupportDatasets(DM_COMPANY.CO.ClientID, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
  except
    NewBatch := False;
    (Owner as TFramePackageTmpl).ClickButton(NavCancel);
    raise;
  end
  else if Kind = NavCommit then
  begin
    if NewBatch then
    begin
      NewBatch := False;

      bAutoCreateCheck := False;
      if ctx_AccountRights.Functions.GetState('USER_AUTO_CREATE_CHECKS_TIME_CLOCK') = stEnabled then
      begin
        chbSelectEEFromList.Checked := false;
        bAutoCreateCheck := True;
      end
      else
        if EvMessage('Would you like to auto-create checks for this batch?', mtConfirmation, [mbYes, mbNo]) = mrYes then
          bAutoCreateCheck := True;

      V := Null;
      if bInsertWithCobraWizard or FNewCobraBatch{(FNewCobraBatch and chbSelectEEFromList.Checked)} then
      begin
        if chbSelectEEFromList.Checked then
          // if Select EE From List option is checked then it should show all employees with the status of "Involuntary Layoff"
          // and not only ones who have Cobra Credit Scheduled E/D setup
          EENbrList := GetCobraWizardEEs(V, False)
        else
          EENbrList := GetCobraWizardEEs(V);
      end    
      else
        EENbrList := '';

      if EENbrList = '-1' then
      begin
        bAutoCreateCheck := False;
        EvMessage('There are no Employees with the status of ''Involuntary Layoff''' + #10#13 +
                  'and have the ''Cobra Credit'' code type as an active Scheduled ED.' + #10#13 +
                  'No checks created.');
      end;

{      if FNewCobraBatch and not chbSelectEEFromList.Checked then
        bAutoCreateCheck := False;}

      if bAutoCreateCheck then
      begin
        if rgCreateChecks.ItemIndex = 1 then
          UpdateBalance := EvMessage('Do you want the balance of the scheduled EDs for manual checks to be updated?', mtConfirmation, [mbYes, mbNo]) = mrYes
        else
          UpdateBalance := True;
        CommitData;
        ctx_StartWait;
        DM_PAYROLL.PR_CHECK.DisableControls;
        DM_PAYROLL.PR_CHECK_LINES.DisableControls;
        DM_PAYROLL.PR_CHECK_LINE_LOCALS.DisableControls;
        DM_PAYROLL.PR_CHECK_LOCALS.DisableControls;
        DM_PAYROLL.PR_CHECK_STATES.DisableControls;
        DM_PAYROLL.PR_CHECK_SUI.DisableControls;
        DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DisableControls;
        sPR := DM_PAYROLL.PR.IndexName;
        sBatch := DM_PAYROLL.PR_BATCH.IndexName;
        PrNbr := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
        PrBatchNbr := DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger;
        SaveDSStates([DM_PAYROLL.PR]);
        try
          b := ctx_DataAccess.RecalcLineBeforePost;
          try
            if (chbSelectEEFromList.Checked) or
               (ctx_AccountRights.Functions.GetState('USER_AUTO_CREATE_CHECKS_TIME_CLOCK') = stEnabled) then
            begin
              EmployeeSelector := TEmployeeSelector.Create(Nil);
              try
                SaveFilterCond  := EmployeeSelector.dsEmployee.DataSet.Filter;
                SaveFilterState := EmployeeSelector.dsEmployee.DataSet.Filtered;

                if chbSelectEEFromList.Checked then
                begin
                  if EENbrList <> '' then
                    EmployeeSelector.dsEmployee.DataSet.Filter := 'EE_NBR in (' + EENbrList + ')'
                  else
                    EmployeeSelector.dsEmployee.DataSet.Filter := 'PAY_FREQUENCY=' + QuotedStr(DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').AsString) + ' and EE_ENABLED = ''Y''';
                  EmployeeSelector.dsEmployee.DataSet.Filtered := True;
                  if EmployeeSelector.ShowModal = mrOK then
                    V := EmployeeSelector.EENumbers
                  else
                    Exit;
                end
                else begin

                  //   Added if anyuser doesnt have right Checks will be created automaticly...

                  if EENbrList <> '' then
                    EmployeeSelector.dsEmployee.DataSet.Filter := 'EE_NBR in (' + EENbrList + ')'
                  else
                    EmployeeSelector.dsEmployee.DataSet.Filter :=
                      'PAY_FREQUENCY='+ QuotedStr(DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').AsString) +
                      ' and ( CURRENT_TERMINATION_CODE = ''A'' or ( CURRENT_TERMINATION_CODE <> ''A'' and '''+
                      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').AsString+''' <= CURRENT_TERMINATION_DATE and '''+
                      DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').AsString+''' >= CURRENT_TERMINATION_DATE ))';
                  EmployeeSelector.dsEmployee.DataSet.Filtered := True;
                  I := 0;
                  EmployeeSelector.EENumbers := VarArrayCreate([0, EmployeeSelector.dsEmployee.DataSet.RecordCount - 1], varInteger);
                  EmployeeSelector.dsEmployee.DataSet.First;
                  while not EmployeeSelector.dsEmployee.DataSet.Eof do
                  begin
                    EmployeeSelector.EENumbers[I] :=
                            EmployeeSelector.dsEmployee.DataSet.FieldByName('EE_NBR').AsInteger;
                    I := I+1;
                    EmployeeSelector.dsEmployee.DataSet.Next;
                  end;
                  V := EmployeeSelector.EENumbers;
                end;
              finally
                EmployeeSelector.dsEmployee.DataSet.Filter := SaveFilterCond;
                EmployeeSelector.dsEmployee.DataSet.Filtered := SaveFilterState;
                EmployeeSelector.Free;
              end;
            end;

            ctx_DataAccess.RecalcLineBeforePost := True;
            if evCheckBox2.Checked then
              Check945Type := CHECK_TYPE_945_945
            else
              Check945Type := CHECK_TYPE_945_NONE;
            if bInsertWithCobraWizard or FNewCobraBatch then
              ctx_PayrollProcessing.CreatePRBatchDetails(DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger,
                (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value <> PAYROLL_TYPE_SETUP) or (GetQuarterNumber(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime) <> 1) and
                (EvMessage('Would you like to auto-create quarter to date checks?', mtConfirmation, [mbYes, mbNo]) = mrYes),
                chbCalcScheduledEDs.Checked, CHECK_TYPE2_COBRA_CREDIT, Check945Type, evSpinEdit1.Value, rgPay.ItemIndex, V,
                nil, nil, loByName, loFull, False, loFixed, False, False, UpdateBalance, False)
            else begin
              case rgCreateChecks.ItemIndex of
                0: CheckType := CHECK_TYPE2_REGULAR;
                1: CheckType := CHECK_TYPE2_MANUAL;
                2: CheckType := CHECK_TYPE2_3RD_PARTY;
              end;
              ctx_PayrollProcessing.CreatePRBatchDetails(DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger,
                (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value <> PAYROLL_TYPE_SETUP) or (GetQuarterNumber(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime) <> 1) and
                (EvMessage('Would you like to auto-create quarter to date checks?', mtConfirmation, [mbYes, mbNo]) = mrYes),
                chbCalcScheduledEDs.Checked, CheckType, Check945Type, evSpinEdit1.Value, rgPay.ItemIndex, V,
                nil, nil, loByName, loFull, False, loFixed, False, False, UpdateBalance, IncludeTimeOffRequests.Checked);
            end;
          finally
            LoadDSStates([DM_PAYROLL.PR]);
            ctx_DataAccess.RecalcLineBeforePost := b;
            DM_PAYROLL.PR.IndexName := sPR;
            DM_PAYROLL.PR_BATCH.IndexName := sBatch;
            //
            CloseDetailPayroll;
            OpenDetailPayroll;
            //
            if PrNbr <> DM_PAYROLL.PR.PR_NBR.AsInteger then
              DM_PAYROLL.PR.Locate('PR_NBR', PrNbr, []);
            if PrBatchNbr <> DM_PAYROLL.PR_BATCH.PR_BATCH_NBR.AsInteger then
              DM_PAYROLL.PR_BATCH.Locate('PR_BATCH_NBR', PrBatchNbr, []);
            ctx_EndWait;
            if (DM_COMPANY.CO.CHECK_TIME_OFF_AVAIL.AsString = GROUP_BOX_YES)
            and IncludeTimeOffRequests.Checked then
            begin
              DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR='+DM_PAYROLL.PR.PR_NBR.AsString);
              DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.DataRequired('PR_NBR='+DM_PAYROLL.PR.PR_NBR.AsString);
              DM_PAYROLL.PR_CHECK.First;
              while not DM_PAYROLL.PR_CHECK.Eof do
              begin
                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Filter := 'PR_CHECK_NBR='+DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.AsString+
                  ' AND OPERATION_CODE='''+EE_TOA_OPER_CODE_REQUEST_APPROVED+'''';
                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Filtered := True;
                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.FIrst;
                if not DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.IsEmpty then
                begin
                  DS := TevClientDataset.Create(nil);
                  try
                    DS.ProviderName := ctx_DataAccess.CUSTOM_VIEW.ProviderName;
                    with TExecDSWrapper.Create('TimeOffBalanceCheck') do
                    begin
                      SetParam('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value);
                      DS.DataRequest(AsVariant);
                    end;
                    ctx_DataAccess.OpenDataSets([DS]);
                    DS.First;
                    EEName := '';
                    TOAName := '';
                    while not DS.Eof do
                    begin
                      if DS.FieldByName('USED').AsCurrency > DS.FieldByName('ACCRUED').AsCurrency then
                      begin
                        TOAName := DS.FieldByName('DESCRIPTION').AsString;
                        Assert(DM_COMPANY.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK.EE_NBR.Value, []));
                        Assert(DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_COMPANY.EE.CL_PERSON_NBR.Value, []));
                        EEName := DM_CLIENT.CL_PERSON.FIRST_NAME.AsString + ' ' + DM_CLIENT.CL_PERSON.LAST_NAME.AsString;
                        if EEName <> '' then
                        begin
                          if EvMessage('Employee '+EEName+' does not have enough '+TOAName+' hours to accommodate the request. Do you want to proceed?', mtConfirmation, [mbYes, mbNo]) = mrNo then
                          begin
                            while DM_PAYROLL.PR_CHECK_LINES.Locate('PR_CHECK_NBR;CL_E_DS_NBR',
                              VarArrayOf([DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value, DS.FieldByName('CL_E_DS_NBR').Value]), []) do
                            begin
                              DM_PAYROLL.PR_CHECK_LINES.Delete;
                            end;
                            DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.First;
                            while not DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Eof do
                            begin
                              if DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.OPERATION_CODE.AsString = EE_TOA_OPER_CODE_REQUEST_APPROVED then
                              begin
                                if not DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Active then
                                  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.DataRequired('');
                                if not DM_COMPANY.CO_TIME_OFF_ACCRUAL.Active then
                                  DM_COMPANY.CO_TIME_OFF_ACCRUAL.DataRequired('');
                                Assert(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.Locate('EE_TIME_OFF_ACCRUAL_NBR', DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.EE_TIME_OFF_ACCRUAL_NBR.Value, []));
                                Assert(DM_COMPANY.CO_TIME_OFF_ACCRUAL.Locate('CO_TIME_OFF_ACCRUAL_NBR', DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL.CO_TIME_OFF_ACCRUAL_NBR.Value, []));
                                if DM_COMPANY.CO_TIME_OFF_ACCRUAL.CL_E_DS_NBR.AsInteger = DS.FieldByName('CL_E_DS_NBR').Value then
                                begin
                                  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Edit;
                                  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_NBR'] := Null;
                                  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_CHECK_NBR'] := Null;
                                  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER['PR_BATCH_NBR'] := Null;
                                  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Post;
                                end
                                else
                                  DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Next;
                              end
                              else
                                DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.Next;
                              ctx_DataAccess.PostDataSets([DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER, DM_PAYROLL.PR_CHECK_LINES]);
                            end;
                          end;
                        end;
                      end;
                      DS.Next;
                    end;
                  finally
                    DS.Free;
                  end;
                end;
                DM_PAYROLL.PR_CHECK.Next;
              end;
            end;
            DM_PAYROLL.PR_CHECK.EnableControls;
            DM_PAYROLL.PR_CHECK_LINES.EnableControls;
            DM_PAYROLL.PR_CHECK_SUI.EnableControls;
            DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER.EnableControls;
            DM_PAYROLL.PR_CHECK_STATES.EnableControls;
            DM_PAYROLL.PR_CHECK_LOCALS.EnableControls;
            DM_PAYROLL.PR_CHECK_LINE_LOCALS.EnableControls;
          end;
        except
          (Owner as TFramePackageTmpl).ClickButton(NavAbort);
          raise;
        end;
        //CloseDetailPayroll;
        //OpenDetailPayroll;
      end;

      if FNewCobraBatch or bInsertWithCobraWizard then
      begin
        FNewCobraBatch := False;
        bInsertWithCobraWizard := False;
        EnableControls(True);
      end;

      ResetCalc;
      pcBatchChange(Self);
      iWizardPosition := WIZ_BATCH_CREATED;
    end;

    if Assigned(QUICK_PAY_DS.DataSet) then
      TevClientDataSet(QUICK_PAY_DS.DataSet).MergeChangeLog;
    //iRec := 0; //avoiding compiler warning
    PrCheckNbr := 0;//avoiding compiler warning
    PrCheckNbr2 := 0;//avoiding compiler warning
    if Assigned(QUICK_PAY_DS.DataSet) then
    begin
      if Assigned(QUICK_PAY_DS.DataSet.FindField('PR_CHECK_NBR')) then
        PrCheckNbr := ConvertNull(QUICK_PAY_DS.DataSet.FieldByName('PR_CHECK_NBR').Value,-1);
    end;
    if Assigned(QuickEDGrid.dsQED.DataSet) then
    begin
       if Assigned(QuickEDGrid.dsQED.DataSet.FindField('PR_CHECK_NBR')) then
            PrCheckNbr2 :=  ConvertNull(QuickEDGrid.dsQED.DataSet.FieldByName('PR_CHECK_NBR').Value,-1);
    end;
    ResetCalc;
    Calculate_BATCH_QuickPay;
    QuickEDGrid.PrBatchDataChanged;

    if Assigned(QUICK_PAY_DS.DataSet) then
    begin
      if Assigned(QUICK_PAY_DS.DataSet.FindField('PR_CHECK_NBR')) then
         QUICK_PAY_DS.DataSet.Locate('PR_CHECK_NBR',PrCheckNbr,[]);
    end;
    if Assigned(QuickEDGrid.dsQED.DataSet) then
    begin
       if Assigned(QuickEDGrid.dsQED.DataSet.FindField('PR_CHECK_NBR')) then
           QuickEDGrid.dsQED.DataSet.Locate('PR_CHECK_NBR',PrCheckNbr2,[]);
    end;
    if cbAuoRefreshEDs.Checked and ImportingTimeClock then
      evBitBtn1.Click;

    RefreshEEStates;
{    DM_EMPLOYEE.EE_STATES.Close;
    DM_EMPLOYEE.EE_STATES.DataRequired('ALL');}
  end
  else if Kind = NavAbort then
  begin
    if Assigned(QUICK_PAY_DS.DataSet) then
      TevClientDataSet(QUICK_PAY_DS.DataSet).CancelUpdates;
    ResetCalc;
    pcBatchChange(Self);
  end;
  if Kind = NavCancel then
  begin
    FNewCobraBatch := False;
    bInsertWithCobraWizard := False;
    EnableControls(True);
    NewBatch := False;
  end;
  if (Kind = NavOK) and
     NewBatch then
  begin
    (Owner as TFramePackageTmpl).ClickButton(NavCommit);
    FNewCobraBatch := False;
    EnableControls(True);
  end;
  if Kind in [navOk, navCommit] then
  begin
    FNewCobraBatch := False;
    bInsertWithCobraWizard := False;
    EnableControls(True);
  end;
  if Kind in [NavOK, NavDelete, NavAbort] then
  begin
    wwDBGrid1.ReadOnly := CheckGridReadOnly;
  end;
  ImportingTimeClock := False;
end;

procedure TREDIT_BATCH.CheckSrcDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if Assigned(CheckSrc.DataSet) and
     CheckSrc.DataSet.Active then
  begin
    lblChecks.Caption := 'Checks: ' + IntToStr(CheckSrc.DataSet.RecordCount);
    SaveLastCheckPosition(DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
  end;
end;

procedure TREDIT_BATCH.Deactivate;
var
  i: Integer;
  Pos: TQEDPosition;
begin

  DM_COMPANY.CO_JOBS.Filtered := False;

  if Assigned(QUICK_PAY_DS.DataSet) and
     (QUICK_PAY_DS.DataSet.State <> dsBrowse) then
    QUICK_PAY_DS.DataSet.CheckBrowseMode;
  if DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger <> 0 then
  begin
    mb_AppSettings.DeleteNode('Misc\' + Self.ClassName + '\wwDBGrid1\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
      IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));

    mb_AppSettings.AsInteger['Misc\' + Self.ClassName + '\wwDBGrid1\Version'] := wwDBGrid1.Tag;
    for i := 0 to Pred(wwDBGrid1.Selected.Count) do
      mb_AppSettings['Misc\' + Self.ClassName + '\wwDBGrid1\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
        IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger) + '\Column#' + IntToStr(i)] := wwDBGrid1.Selected[i];
  end;
  FreeCoYTD;
  QUICK_PAY_DS.OnDataChange := nil;
  QuickEDGrid.dsQED.OnDataChange := QuickEDGrid.dsQEDDataChange;

  if pcBatch.ActivePage = tsQEDGrid then
    Pos := QuickEDGrid.Position;
  QuickEDGrid.Deactivate;
//  ctx_DataAccess.TempCommitDisable := False;
  CommitData;
  inherited;
  if DM_PAYROLL.PR_CHECK.Active then
    if (pcBatch.ActivePage = tsCheck) and
       Assigned(QUICK_PAY_DS.DataSet) and
       (QUICK_PAY_DS.DataSet.RecordCount > 0) then
      DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', QUICK_PAY_DS.DataSet['PR_CHECK_NBR'], [])
    else if (pcBatch.ActivePage = tsEarnings) and
            (dsED_Detail.DataSet.RecordCount > 0) then
      DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', dsED_Detail.DataSet['PR_CHECK_NBR'], [])
    else if (pcBatch.ActivePage = tsTax) and
            (wwDBGrid5.DataSource.DataSet.RecordCount > 0) then
      DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', wwDBGrid5.DataSource.DataSet['PR_CHECK_NBR'], [])
    else if (pcBatch.ActivePage = tsQEDGrid) then
      TryLocatePosition( Pos );
  ctx_DataAccess.RecalcLineBeforePost := True;
  DM_PAYROLL.PR_BATCH.AfterPost := nil;
  DM_PAYROLL.PR_BATCH.AfterInsert := nil;
  ctx_DataAccess.CheckMaxAmountAndHours := False;
end;

procedure TREDIT_BATCH.CommitData;
begin
  with Owner as TFramePackageTmpl do
    if ButtonEnabled(NavOK) then
      ClickButton(NavOK);
  with Owner as TFramePackageTmpl do
    if ButtonEnabled(NavCommit) then
      ClickButton(NavCommit);
end;

function TREDIT_BATCH.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR_BATCH;
end;

function TREDIT_BATCH.GetDataSetConditions(sName: string): string;
begin
  Result := CheckUplinkedRetrieveCondition(sName, 'PR_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger));
end;

function TREDIT_BATCH.CanOpen: Boolean;
begin
  Result := (DetailPayrollOpen <> 0) and
            DM_TEMPORARY.TMP_CO.Active and DM_TEMPORARY.TMP_CO.Locate('CL_NBR; CO_NBR', VarArrayOf([DM_COMPANY.CO.ClientID, DM_COMPANY.CO['CO_NBR']]), []) and
            DM_PAYROLL.PR.Active and DM_PAYROLL.PR.Locate('PR_NBR', DetailPayrollOpen, []);
  if not Result then
  begin
    DetailPayrollOpen := 0; 
    (Owner as TFramePackageTmpl).OpenFrame('TREDIT_PAYROLL');
  end;
end;

procedure TREDIT_BATCH.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK_LINES, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK_LINE_LOCALS, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK_LOCALS, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK_STATES, EditDataSets);
  AddDS(DM_PAYROLL.PR_CHECK_SUI, EditDataSets);
  AddDS(DM_EMPLOYEE.EE_TIME_OFF_ACCRUAL_OPER, EditDataSets);
  AddDS(DM_COMPANY.CO_JOBS, EditDataSets);
  AddDS(DM_PAYROLL.PR_SCHEDULED_EVENT, EditDataSets);
  AddDS(DM_COMPANY.CO_PHONE, SupportDataSets);
end;

procedure TREDIT_BATCH.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if pcBatch.ActivePage = tsEarnings then
    Calculate_ED_TOTALS(SeparateEDByLineType, SeparateEDByCheckType);

  if pcBatch.ActivePage = tsTax then
    Calculate_TAX_TOTALS(cbSeperateOverrides.Checked);

  if FQPInit and (pcBatch.ActivePage = tsCheck) then
  begin
    Calculate_BATCH_QuickPay;
    SetGridPosition;
  end;

  if pcBatch.ActivePage = tsQEDGrid then
    QuickEDGrid.PrBatchDataChanged;
  wwcbPayFrequency.Enabled := VarIsNull(DM_PAYROLL.PR_CHECK.Lookup('PR_BATCH_NBR', DM_PAYROLL.PR_BATCH.PR_BATCH_NBR.Value, 'PR_BATCH_NBR'));
  btnPreProcessBatch.Enabled := (ctx_AccountRights.Functions.GetState('USER_CAN_PREPROCESS_BATCH') = stEnabled)
    and (DM_CLIENT.PR_BATCH.State = dsBrowse)
    and (DM_CLIENT.PR.FieldByName('PAYROLL_TYPE').AsString<>PAYROLL_TYPE_IMPORT)
    and (DM_CLIENT.PR.STATUS.AsString = PAYROLL_STATUS_PENDING)
    and (not DM_CLIENT.PR_BATCH.IsEmpty)
end;

procedure TREDIT_BATCH.wwDBGrid1Exit(Sender: TObject);
begin
  inherited;
  if Assigned(QUICK_PAY_DS.DataSet) and
     (QUICK_PAY_DS.DataSet.State <> dsBrowse) then
    QUICK_PAY_DS.DataSet.CheckBrowseMode;
end;

procedure TREDIT_BATCH.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  i: Integer;
  Q: IevQuery;
begin
//  if (Kind in [NavOk, NavCommit]) then
//    RefreshEEStates;
  if Assigned(QUICK_PAY_DS.DataSet) and
     (QUICK_PAY_DS.DataSet.State <> dsBrowse) then
    if (Kind = NavCancel) or (Kind = NavAbort) then
      QUICK_PAY_DS.DataSet.Cancel
    else
      QUICK_PAY_DS.DataSet.CheckBrowseMode;
  QuickEDGrid.ForceEndEditing( Kind in [NavCancel, NavAbort] {should Cancel or Post} );

  inherited;
  if Kind = NavInsert then
    if ctx_PayrollCalculation.GetEeFilter <> '' then
      raise EInconsistentData.CreateHelp('You can not create a batch when company is opened with a filter', IDH_InconsistentData);


  if (pcBatch.ActivePage = tsQEDGrid) and
     (Kind = NavDelete) then
  begin
    if EvMessage('Are you sure you want to delete this BATCH?'#13#10#13#10'To delete E/Ds use Ctrl-Del', mtConfirmation, mbOKCancel) = mrOK then
    begin
      if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
      begin
        ctx_DBAccess.StartTransaction([dbtClient]);
        try
          Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
          Q.Execute;
          DM_PAYROLL.PR_BATCH.Delete;          
          (Owner as TFramePackageTmpl).ClickButton(NavCommit);
          ctx_DBAccess.CommitTransaction;
        except
          ctx_DBAccess.RollbackTransaction;
          raise;
        end;
      end
      else
        DM_PAYROLL.PR_BATCH.Delete;
    end;
    Handled := True;
  end;

  if (pcBatch.ActivePage = tsCheck) and
     (Kind = NavDelete) then
  begin
    if EvMessage('Are you sure you want to delete this BATCH?'#13#10#13#10'To delete a check use Ctrl-Del', mtConfirmation, mbOKCancel) = mrOK then
    begin
      if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
      begin
        ctx_DBAccess.StartTransaction([dbtClient]);
        try
          Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
          Q.Execute;
          DM_PAYROLL.PR_BATCH.Delete;
          (Owner as TFramePackageTmpl).ClickButton(NavCommit);
          ctx_DBAccess.CommitTransaction;
        except
          ctx_DBAccess.RollbackTransaction;
          raise;
        end;
      end
      else
        DM_PAYROLL.PR_BATCH.Delete;
      wwDBGrid1.ReadOnly := CheckGridReadOnly;
    end;
    Handled := True;
  end;

  if (pcBatch.ActivePage = tsBatch) and
     (Kind = NavDelete) then
  begin
    if EvMessage('Are you sure you want to delete this BATCH?', mtConfirmation, mbOKCancel) = mrOK then
    begin
      if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
      begin
        ctx_DBAccess.StartTransaction([dbtClient]);
        try
          Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
          Q.Execute;
          DM_PAYROLL.PR_BATCH.Delete;
          (Owner as TFramePackageTmpl).ClickButton(NavCommit);
          ctx_DBAccess.CommitTransaction;
        except
          ctx_DBAccess.RollbackTransaction;
          raise;
        end;
      end
      else
        DM_PAYROLL.PR_BATCH.Delete;
      wwDBGrid1.ReadOnly := CheckGridReadOnly;
    end;
    Handled := True;
  end;

  if Kind = NavRefresh then
  begin
    if Assigned(QUICK_PAY_DS.DataSet) and
       (QUICK_PAY_DS.DataSet.State <> dsBrowse) then
      QUICK_PAY_DS.DataSet.CheckBrowseMode;
    mb_AppSettings.DeleteNode('Misc\' + Self.ClassName + '\wwDBGrid1\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
      IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));

    mb_AppSettings.AsInteger['Misc\' + Self.ClassName + '\wwDBGrid1\Version'] := wwDBGrid1.Tag;
    for i := 0 to Pred(wwDBGrid1.Selected.Count) do
      mb_AppSettings['Misc\' + Self.ClassName + '\wwDBGrid1\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
        IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger) + '\Column#' + IntToStr(i)] := wwDBGrid1.Selected[i];
    QuickEDGrid.ForceEndEditing( false );
    QuickEDGrid.SaveSettings;
  end;
end;

procedure TREDIT_BATCH.SetReadOnly(Value: Boolean);
begin
  inherited;
  cbSeperateOverrides.SecurityRO := False;

  btnSwipeClock.Visible := VisibleSwipeClock;
  btnMRCLogIn.Visible := VisibleMyRecruitingCenter;

  if not (inherited GetIfReadOnly) then
  begin
    spbFavoriteReports.SecurityRO := False;
    if btnSwipeClock.Visible then btnSwipeClock.SecurityRO := False;
    if btnMRCLogIn.Visible then btnMRCLogIn.SecurityRO := False;
  end;
end;

procedure TREDIT_BATCH.NewBatchExpertBtnClick(Sender: TObject);
begin
  inherited;
  bInsertWithWizard := True;
  bInsertWithCobraWizard := False;
  try
    (Owner as TFramePackageTmpl).ClickButton(NavInsert);
  except
    bInsertWithWizard := False;
    raise;
  end;
  CheckSupportDatasets(DM_COMPANY.CO.ClientID, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
end;

procedure TREDIT_BATCH.evBitBtn1Click(Sender: TObject);
var
  s: string;
begin
  inherited;
  if TComponent(Sender).Tag = 1 then
    s := 'Are you sure you want to refresh Scheduled EDs on regular checks only for this batch?'
  else
    s := 'Are you sure you want to refresh Scheduled EDs for this batch?';
  if EvMessage(s, mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;

  ctx_DataAccess.StartNestedTransaction([dbtClient]);
  try
    DM_PAYROLL.PR_CHECK.DisableControls;
    DM_PAYROLL.PR_CHECK_LINES.DisableControls;
    try
      ctx_PayrollCalculation.RefreshScheduledEDsForBatch(DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger, Boolean(TComponent(Sender).Tag));
    finally
      DM_PAYROLL.PR_CHECK_LINES.EnableControls;
      DM_PAYROLL.PR_CHECK.EnableControls;
    end;
    ctx_DataAccess.CommitNestedTransaction;
  except
    ctx_DataAccess.RollbackNestedTransaction;
    DM_PAYROLL.PR_CHECK_LINES.Cancel;
    DM_PAYROLL.PR_CHECK.Cancel;
    (Owner as TFramePackageTmpl).ClickButton(navCancel);
    (Owner as TFramePackageTmpl).ClickButton(NavAbort);
    raise;
  end;
end;

procedure TREDIT_BATCH.sbWizardClick(Sender: TObject);
begin
  inherited;
  case iWizardPosition of
  WIZ_PAYROLL_CREATED: BitBtn1.Click;
  WIZ_BATCH_CREATED: pcBatch.ActivatePage(tsCheck);
  WIZ_CHECKS_CREATED: pcBatch.ActivatePage(tsEarnings);
  WIZ_TOTALS_AFTER_CHECKS,
    WIZ_PREPROCESSED,
    WIZ_TOTALS_AFTER_PREPROCESS: BitBtn4.Click;
  end;
end;

//gdy
procedure TREDIT_BATCH.AddItemListClick(Sender: TObject);
begin
  with TEDIT_SELECT_ITEM.Create( Self ) do
  try
    RenderQPMenu( AddItem );
    if ShowModal = mrOk then
      Selected.Click;
  finally
    Free;
  end;
end;

//gdy
procedure TREDIT_BATCH.UpdateQPMenuStatus;
begin
  AddItem.Visible := CountVisibleItems(AddItem) > 0;
  AddItemList.Visible := AddItem.Visible;
  DeleteItem.Visible := CountVisibleItems(DeleteItem) > 0;
  AddTaxItem.Visible := CountVisibleItems(AddTaxItem) > 0;
  DeleteTaxItem.Visible := CountVisibleItems(DeleteTaxItem) > 0;
end;

constructor TREDIT_BATCH.Create(AOwner: TComponent);
begin
  inherited;
  FQPInit := False;
  AutoReductionExceptions.CreateDataSet;
//  EeRates.CreateDataSet;
end;

procedure TREDIT_BATCH.wwDBDateTimePicker1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  p: TPoint;
begin
  inherited;
  p := TWinControl(Sender).Parent.ClientToScreen(Point(TWinControl(Sender).Left + 1, TWinControl(Sender).Top + TWinControl(Sender).Height + 1));
  if (Key = Ord('Z')) and (Shift = [ssCtrl]) then
  begin
    CalcPopup.Popup(p.x, p.y);
    Key := 0;
  end;
end;

procedure TREDIT_BATCH.CalcPopupPopup(Sender: TObject);
var
  T2: TMenuItem;
  i: Integer;
begin
  inherited;
  if GetIfReadOnly then
    AbortEx;
  while CalcPopup.Items.Count > 0 do
    CalcPopup.Items.Delete(0);
  if DM_PAYROLL.PR_SCHEDULED_EVENT.Locate('PR_NBR', DM_PAYROLL.PR['PR_NBR'], []) then
  begin
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.DataRequired('PR_SCHEDULED_EVENT_NBR=' + DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('PR_SCHEDULED_EVENT_NBR').AsString);
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.First;
    i := -1;
    while not DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Eof do
    begin
      with BreakCompanyFreqIntoPayFreqList(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['FREQUENCY']) do
      try
        if IndexOf(DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').Value) >= 0 then
        begin
          T2 := TMenuItem.Create(Self);
          T2.Caption := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('PERIOD_BEGIN_DATE').AsString + ' - ' +
                        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('PERIOD_END_DATE').AsString;
          T2.OnClick := MenuCLickHdl;
          if i = 20 then
          begin
            T2.Break := mbBarBreak;
            i := 0;
          end
          else
            Inc(i);
          CalcPopup.Items.Add(T2);
          Break;
        end;
      finally
        Free;
      end;
      DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Next;
    end;
  end;
  if CalcPopup.Items.Count = 0 then
    AbortEx;
end;

procedure TREDIT_BATCH.MenuCLickHdl(Sender: TObject);
begin
  if DM_PAYROLL.PR_BATCH.State = dsBrowse then
    DM_PAYROLL.PR_BATCH.Edit;
  DM_PAYROLL.PR_BATCH['PERIOD_BEGIN_DATE'] := StrToDate(Copy(TMenuItem(Sender).Caption, 1, Pred(Pos(' - ', TMenuItem(Sender).Caption))));
  DM_PAYROLL.PR_BATCH['PERIOD_END_DATE'] := StrToDate(Copy(TMenuItem(Sender).Caption, Pos(' - ', TMenuItem(Sender).Caption) + 3, Length(TMenuItem(Sender).Caption)));
end;

procedure TREDIT_BATCH.spbImportSourceFileClick(Sender: TObject);
begin
  inherited;
  dlgImportSourceFile.FileName := editImportSourceFile.Text;
  if dlgImportSourceFile.Execute then
    editImportSourceFile.Text := dlgImportSourceFile.FileName;
end;

procedure TREDIT_BATCH.spbImportReportFileClick(Sender: TObject);
begin
  inherited;
  dlgImportReportFile.FileName := editImportReportFile.Text;
  if dlgImportReportFile.Execute then
    editImportReportFile.Text := dlgImportReportFile.FileName;
end;

procedure TREDIT_BATCH.btnTwImportGoClick(Sender: TObject);
var
  ImportLookupOption: TCImportLookupOption;
  DBDTOption: TCImportDBDTOption;
  FileFormatOption: TCFileFormatOption;
begin
  inherited;
  editImportSourceFile.Text := ExpandFileName(editImportSourceFile.Text);
  editImportReportFile.Text := ExpandFileName(editImportReportFile.Text);
  if radioImportLookByNumber.Checked then
    ImportLookupOption := loByNumber
  else
  if radioImportLookByName.Checked then
    ImportLookupOption := loByName
  else
    ImportLookupOption := loBySSN;
  if radioImportMatchDbdtFull.Checked then
    DBDTOption := loFull
  else
    DBDTOption := loSmart;
  if radioImportFormatFFF.Checked then
    FileFormatOption := loFixed
  else
    FileFormatOption := loCommaDelimited;
  SaveComponentState(editImportSourceFile);
  SaveComponentState(editImportReportFile);
  SaveComponentState(radioImportLookByNumber);
  SaveComponentState(radioImportLookByName);
  SaveComponentState(radioImportLookBySSN);
  SaveComponentState(radioImportMatchDbdtFull);
  SaveComponentState(radioImportMatchDbdtPartial);
  SaveComponentState(radioImportFormatFFF);
  SaveComponentState(radioImportFormatCSF);
  SaveComponentState(cbFourDigits);
  SaveComponentState(cbAutoImportJobCodes);
  SaveComponentState(cbUseEmployeePayRates);
  SaveComponentState(cbAuoRefreshEDs);
  ctx_StartWait;
  try
    DM_EMPLOYEE.EE_STATES.Close;
    DM_EMPLOYEE.EE_STATES.DataRequired('ALL');

    ProcessTCImport(editImportSourceFile.Text, editImportReportFile.Text, ImportLookupOption, DBDTOption, cbFourDigits.Checked, FileFormatOption, cbAutoImportJobCodes.Checked, cbUseEmployeePayRates.Checked, False, cbIncludeDDOnly.Checked);
  finally
    ctx_EndWait;
  end;
  ResetCalc;
  ImportingTimeClock := True;
end;

procedure TREDIT_BATCH.btnTwImportViewClick(Sender: TObject);
begin
  inherited;
  ShellExecute(0, 'open', 'notepad.exe', PChar(editImportReportFile.Text), '', SW_SHOWNORMAL);
end;

procedure TREDIT_BATCH.cbFourDigitsClick(Sender: TObject);
begin
  inherited;
  if cbFourDigits.Checked and cbFourDigits.Focused then
    EvMessage('If the import file does not use a 4 digit year, the payroll will not import accurately!');
end;

procedure TREDIT_BATCH.PBAfterPost(DataSet: TDataSet);
begin
  if Assigned(TevClientDataSet(DataSet).CustomAfterPost) then
    TevClientDataSet(DataSet).CustomAfterPost(DataSet);
  (Owner as TFramePackageTmpl).ClickButton(NavCommit);
end;

procedure TREDIT_BATCH.pcBatchChanging(Sender: TObject;
  var AllowChange: Boolean);
var
  s: string;
begin
  inherited;
  AllowChange := (wwdsList.Dataset.recordcount >0);
  s := wwDBGrid1.Selected.Text;
  s := s;
end;

procedure TREDIT_BATCH.AddTaxClick(Sender: TObject);
var
  Mark: TBookmarkStr;
begin
  AddQPColumn(ReplaceChars(TMenuItem(Sender).Caption), 'T');
  Mark := QUICK_PAY_DS.DataSet.Bookmark;
  ResetCalc;
  Calculate_BATCH_QuickPay;
  try
    QUICK_PAY_DS.DataSet.Bookmark := Mark;
  except
  end;

  TMenuItem(Sender).Visible := False;
  DeleteTaxItem.Items[TMenuItem(Sender).MenuIndex].Visible := True;

  UpdateQPMenuStatus;

  wwDBGrid1.Selected.Add('Tax_' + ReplaceChars(TMenuItem(Sender).Caption) + #9'10'#9 + TMenuItem(Sender).Caption + #9'F');
  wwDBGrid1.ApplySelected;

  QUICK_PAY_DS.DataSet.DisableControls;
  QUICK_PAY_DS.DataSet.EnableControls;
end;

procedure TREDIT_BATCH.DeleteTaxClick(Sender: TObject);
var
  Mark: TBookmarkStr;
  i: Integer;
begin
  DeleteQPColumn(ReplaceChars(TMenuItem(Sender).Caption), 'T');
  Mark := QUICK_PAY_DS.DataSet.Bookmark;
  ResetCalc;
  Calculate_BATCH_QuickPay;
  try
    QUICK_PAY_DS.DataSet.Bookmark := Mark;
  except
  end;

  TMenuItem(Sender).Visible := False;
  AddTaxItem.Items[TMenuItem(Sender).MenuIndex].Visible := True;

  UpdateQPMenuStatus;

  for i := 0 to Pred(wwDBGrid1.Selected.Count) do
    if UpperCase(Copy(wwDBGrid1.Selected[i], 1, Pred(Pos(#9, wwDBGrid1.Selected[i])))) = UpperCase('Tax_' + ReplaceChars(TMenuItem(Sender).Caption)) then
    begin
      wwDBGrid1.Selected.Delete(i);
      Break;
    end;
  wwDBGrid1.ApplySelected;
  QUICK_PAY_DS.DataSet.DisableControls;
  QUICK_PAY_DS.DataSet.EnableControls;
end;

function TREDIT_BATCH.CheckGridReadOnly: Boolean;
begin
  Result := DM_PAYROLL.PR_BATCH.RecordCount = 0;
end;

procedure TREDIT_BATCH.wwDBGrid1BeforeDrawCell(Sender: TwwCustomDBGrid;
  DrawCellInfo: TwwCustomDrawGridCellInfo);
begin
  inherited;
  if (gdFixed in DrawCellInfo.State) and
     (DrawCellInfo.DataRow >= 0) then
    Sender.Canvas.Font := TevDBGrid(Sender).Font;
end;

procedure TREDIT_BATCH.chbCalcScheduledEDsClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['CalcSchedEDs'] := chbCalcScheduledEDs.Checked;
end;

procedure TREDIT_BATCH.bEEClick(Sender: TObject);
begin
  inherited;
  DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', QUICK_PAY_DS.DataSet['PR_CHECK_NBR'], []);
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Employee - Employee' + #0)), 0);
end;

procedure TREDIT_BATCH.bEERatesClick(Sender: TObject);
begin
  inherited;
  DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', QUICK_PAY_DS.DataSet['PR_CHECK_NBR'], []);
  PostMessage(Application.MainForm.Handle, WM_ACTIVATE_PACKAGE, Integer(PChar('Employee - Pay rate info' + #0)), 0);
end;

procedure TREDIT_BATCH.wwcbPayFrequencyChange(Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then exit;  
  if GetIfReadOnly then
   AbortEx;
  if not (DM_PAYROLL.PR_BATCH.State in [dsEdit]) then
    Exit;
{  if DM_PAYROLL.PR['SCHEDULED'] = 'N' then
    Exit;}
  if DM_PAYROLL.PR_SCHEDULED_EVENT.Locate('PR_NBR', DM_PAYROLL.PR['PR_NBR'], []) then
  begin
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.DataRequired('PR_SCHEDULED_EVENT_NBR=' + DM_PAYROLL.PR_SCHEDULED_EVENT.FieldByName('PR_SCHEDULED_EVENT_NBR').AsString);
    DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.First;
    while not DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Eof do
    begin
      with BreakCompanyFreqIntoPayFreqList(DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH['FREQUENCY']) do
      try
        if IndexOf(wwcbPayFrequency.Value) >= 0 then
        begin
          DM_PAYROLL.PR_BATCH['PERIOD_BEGIN_DATE'] := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('PERIOD_BEGIN_DATE').AsString;
          DM_PAYROLL.PR_BATCH['PERIOD_END_DATE'] := DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.FieldByName('PERIOD_END_DATE').AsString;
          Break;
        end;
      finally
        Free;
      end;
      DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Next;
    end;
  end;
end;

procedure TREDIT_BATCH.PBAfterInsert(DataSet: TDataSet);
begin
  NewBatch := True;
end;


procedure TREDIT_BATCH.btnReduceHoursClick(Sender: TObject);
var
  pr_check_nbr, pr_check_lines_nbr: variant;
  PrCheckLinesClone: TevClientDataSet;
  DivisionNbr, BranchNbr, DepartmentNbr, TeamNbr, RateNumber: Variant;
  bp: TDataSetNotifyEvent;
  SalaryAmount: Double;
  ReduceHours: Double;
  sFilter: string;
  sFiltered, Reduced: boolean;
begin
  if HasBeenPreProcessed and (DM_COMPANY.CO.FieldByName('AUTO_LABOR_DIST_SHOW_DEDUCTS').AsString = GROUP_BOX_YES) then
  begin
    if EvMessage('Labor Distribution has already been processed. ' + #10#13 +
     'This auto reduction of hours will not reduce among D/B/D/T groups. ' + #10#13 +
     'Do you want to continue?', mtConfirmation, [mbYes, mbNo]) = mrNo then
     Exit;
  end;
  if not Assigned(QUICK_PAY_DS.DataSet) then Calculate_BATCH_QuickPay;
  if Assigned( QUICK_PAY_DS ) and Assigned( QUICK_PAY_DS.DataSet ) then
  begin
    ctx_StartWait('Auto Reduction');
    bp := DM_PAYROLL.PR_CHECK_LINES.BeforePost;
    PrCheckLinesClone := TevClientDataSet.Create(nil);
    sFilter := DM_PAYROLL.PR_CHECK_LINES.Filter;
    sFiltered := DM_PAYROLL.PR_CHECK_LINES.Filtered;
    with QUICK_PAY_DS.DataSet, DM_PAYROLL do
    try
      ctx_DataAccess.UnlockPR;
      DisableControls;
      PR_CHECK_LINES.DisableControls;
      PR_CHECK_LINES.BeforePost := nil;
      DBlookAutoReductionED.Enabled := False;
      rgpEEsToReduce.Enabled := False;

      pr_check_nbr := FieldByName('PR_CHECK_NBR').AsVariant;
      PrCheckLinesClone.CloneCursor(PR_CHECK_LINES, False);

      First;
      while not Eof do
      begin
        SalaryAmount := GetEESalaryAmount( FieldByName('EE_NBR').AsString );
        // check for Employees to Reduce option
        if (rgpEEsToReduce.ItemIndex = 2) or                            // Both
          ((rgpEEsToReduce.ItemIndex = 0) and (SalaryAmount = 0)) or    // Hourly
          ((rgpEEsToReduce.ItemIndex = 1) and (SalaryAmount > 0)) then  // Salaried
        begin
          PrCheckLinesClone.Filtered := False;
          PrCheckLinesClone.Filter := 'PR_CHECK_NBR=' + FieldByName('PR_CHECK_NBR').AsString;
          PrCheckLinesClone.Filtered := True;

          PR_CHECK_LINES.Filtered := False;
          PR_CHECK_LINES.Filter := 'PR_CHECK_NBR=' + FieldByName('PR_CHECK_NBR').AsString;
          PR_CHECK_LINES.Filtered := True;

          PrCheckLinesClone.First;
          while not PrCheckLinesClone.Eof do
          begin
            ReduceHours := PrCheckLinesClone.FieldByName('HOURS_OR_PIECES').AsFloat;
            // check for Auto Reduction ED Group option
            if IsEDInGroup( DBlookAutoReductionED.LookupValue, PrCheckLinesClone.FieldByName('CL_E_DS_NBR').AsString) and
              (ReduceHours > 0) and
              (PrCheckLinesClone.FieldByName('REDUCED_HOURS').AsString <> 'Y') and
              not IsRegular( PrCheckLinesClone.FieldByName('CL_E_DS_NBR').AsString ) then
            begin
              // reducing check line is found
              pr_check_lines_nbr := PrCheckLinesClone.FieldByName('PR_CHECK_LINES_NBR').AsVariant;
              DivisionNbr := PrCheckLinesClone.FieldByName('CO_DIVISION_NBR').AsVariant;
              BranchNbr := PrCheckLinesClone.FieldByName('CO_BRANCH_NBR').AsVariant;
              DepartmentNbr := PrCheckLinesClone.FieldByName('CO_DEPARTMENT_NBR').AsVariant;
              TeamNbr := PrCheckLinesClone.FieldByName('CO_TEAM_NBR').AsVariant;
              RateNumber := PrCheckLinesClone.FieldByName('RATE_NUMBER').AsVariant;
              Reduced := False;

              PR_CHECK_LINES.First;
              while not (PR_CHECK_LINES.Eof or Reduced) do
              begin
                // DBDT and Rate Number on reducing check line and regular check line (to reduce) should match
                if ( VarCompareValue(DivisionNbr, PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value) = vrEqual ) and
                  ( VarCompareValue(BranchNbr, PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value) = vrEqual ) and
                  ( VarCompareValue(DepartmentNbr, PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value) = vrEqual ) and
                  ( VarCompareValue(TeamNbr, PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value) = vrEqual ) and
                  ( VarCompareValue(RateNumber, PR_CHECK_LINES.FieldByName('RATE_NUMBER').Value) = vrEqual ) and
                  // only regular check lines can be reduced
                  IsRegular( PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsString ) and
                  // regular check line should have enough hours to reduce
                  ( PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').AsFloat >= ReduceHours ) then
                begin // reducible check line is found
                  if not (PR_CHECK_LINES.State in [dsEdit, dsInsert]) then
                    PR_CHECK_LINES.Edit;
                  PR_CHECK_LINES['HOURS_OR_PIECES'] := PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').AsFloat - ReduceHours;
                  PR_CHECK_LINES.Post;
                  Reduced := True;
                end;
                PR_CHECK_LINES.Next;
              end;
              // update reducing check line
              if PR_CHECK_LINES.Locate('PR_CHECK_LINES_NBR', pr_check_lines_nbr, []) then
              begin
                if not (PR_CHECK_LINES.State in [dsEdit, dsInsert]) then
                  PR_CHECK_LINES.Edit;
                if Reduced then
                begin
                  PR_CHECK_LINES['REDUCING_CL_E_D_GROUPS_NBR'] := DBlookAutoReductionED.LookupValue;
                  PR_CHECK_LINES['REDUCED_HOURS'] := 'Y';
                end
                else
                  PR_CHECK_LINES['REDUCED_HOURS'] := 'F';
                PR_CHECK_LINES.Post;
              end
              else
                raise EevException.Create('Reducing check line is not found!');
            end;
            PrCheckLinesClone.Next;
          end;
        end;
        Next;
      end;
      if PR_CHECK_LINES.ChangeCount > 0 then
      try
        ctx_DataAccess.PostDataSets([PR_CHECK_LINES]);
      except
        PR_CHECK_LINES.CancelUpdates;
        raise;
      end;
    finally
      PR_CHECK_LINES.Filter := sFilter;
      PR_CHECK_LINES.Filtered := sFiltered;
      PrCheckLinesClone.Free;
      Locate('PR_CHECK_NBR', pr_check_nbr, []);
      PR_CHECK_LINES.BeforePost := bp;
      PR_CHECK_LINES.EnableControls;
      EnableControls;
      DBlookAutoReductionED.Enabled := True;
      rgpEEsToReduce.Enabled := True;
      ctx_DataAccess.LockPR(True);
      ctx_EndWait;
    end;
  end;
end;

procedure TREDIT_BATCH.btnViewExceptionsClick(Sender: TObject);
var
  cl_person_nbr, ee_nbr, pr_check_nbr, cl_e_ds_nbr: integer;
begin
  AutoReductionExceptions.EmptyDataSet;
  AutoReductionExceptions.SaveState();
  DM_PAYROLL.PR_CHECK_LINES.DisableControls;
  ctx_StartWait;
  try
    DM_PAYROLL.PR_CHECK_LINES.First;
    while not DM_PAYROLL.PR_CHECK_LINES.Eof do
    begin
      if DM_PAYROLL.PR_CHECK_LINES.REDUCED_HOURS.Value = 'F' then
      begin
        pr_check_nbr := DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_NBR.AsInteger;
        ee_nbr := StrToIntDef(DM_PAYROLL.PR_CHECK.ShadowDataSet.CachedLookup(pr_check_nbr, 'EE_NBR'), 0);
        cl_person_nbr := StrToIntDef(DM_COMPANY.EE.ShadowDataSet.CachedLookup(ee_nbr, 'CL_PERSON_NBR'), 0);
        cl_e_ds_nbr := DM_PAYROLL.PR_CHECK_LINES.CL_E_DS_NBR.AsInteger;

        AutoReductionExceptions.Append;
        AutoReductionExceptionsPR_CHECK_NBR.AsInteger := pr_check_nbr;
        AutoReductionExceptionsEE_CODE.AsString := DM_COMPANY.EE.ShadowDataSet.CachedLookup(ee_nbr, 'CUSTOM_EMPLOYEE_NUMBER');
        AutoReductionExceptionsEE_NAME.AsString :=
            DM_COMPANY.CL_PERSON.ShadowDataSet.CachedLookup(cl_person_nbr, 'FIRST_NAME') + ' ' +
            DM_COMPANY.CL_PERSON.ShadowDataSet.CachedLookup(cl_person_nbr, 'LAST_NAME');
        AutoReductionExceptionsCheckNumber.AsString := DM_PAYROLL.PR_CHECK.ShadowDataSet.CachedLookup(pr_check_nbr, 'PAYMENT_SERIAL_NUMBER');
        AutoReductionExceptionsED_Code.AsString := DM_COMPANY.CL_E_DS.ShadowDataSet.CachedLookup(cl_e_ds_nbr, 'CUSTOM_E_D_CODE_NUMBER');
        AutoReductionExceptionsED_Description.AsString := DM_COMPANY.CL_E_DS.ShadowDataSet.CachedLookup(cl_e_ds_nbr, 'DESCRIPTION');
        AutoReductionExceptionsHours.AsFloat := DM_PAYROLL.PR_CHECK_LINES.HOURS_OR_PIECES.AsFloat;
        AutoReductionExceptions.Post;
      end;
      DM_PAYROLL.PR_CHECK_LINES.Next;
    end;
  finally
    ctx_EndWait;
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
    AutoReductionExceptions.LoadState;
  end;
  if not AutoReductionExceptions.IsEmpty then
    AutoReductionExceptions.First;
end;

procedure TREDIT_BATCH.DBGridExceptionsDblClick(Sender: TObject);
begin
  if not AutoReductionExceptions.IsEmpty then
  begin
    DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', AutoReductionExceptionsPR_CHECK_NBR.AsInteger, []);
    DM_PAYROLL.PR_CHECK.LookupsEnabled := True;

    (Owner as TFramePackageTmpl).OpenFrame('TREDIT_CHECK');
  end;
end;

procedure TREDIT_BATCH.RefreshEErates(EE_NBR: integer);
var
  sEERatesFilter: string;
  bEERatesFiltered: boolean;
  s, sDBDT: string;
  i: integer;
begin
  if EE_NBR <> FEENbr then
  begin
    sEERatesFilter := DM_EMPLOYEE.EE_RATES.Filter;
    bEERatesFiltered := DM_EMPLOYEE.EE_RATES.Filtered;
    DM_EMPLOYEE.EE_RATES.DisableControls;
    cbRates.Items.Clear;
    try
      DM_EMPLOYEE.EE_RATES.Filtered := False;
      DM_EMPLOYEE.EE_RATES.Filter := 'EE_NBR=' + IntToStr(EE_NBR);
      DM_EMPLOYEE.EE_RATES.Filtered := True;
      DM_EMPLOYEE.EE_RATES.First;

      lblSalary.Caption := Format('%9m', [VarToFloat(DM_EMPLOYEE.EE.Lookup('EE_NBR', EE_NBR, 'SALARY_AMOUNT'))]);
      i := 0;
      while not DM_EMPLOYEE.EE_RATES.Eof do
      begin
        if DM_EMPLOYEE.EE_RATES.FieldByName('PRIMARY_RATE').AsString = 'Y' then
          s := 'P'
        else
          s := ' ';

        sDBDT := '';
        if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_DIVISION_NBR').IsNull and
           ((DM_EMPLOYEE.EE_RATES['CO_DIVISION_NBR'] = DM_COMPANY.CO_DIVISION['CO_DIVISION_NBR']) or
             DM_COMPANY.CO_DIVISION.Locate('CO_DIVISION_NBR', DM_EMPLOYEE.EE_RATES['CO_DIVISION_NBR'], [])) and
           (DM_COMPANY.CO_DIVISION.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
          sDBDT := sDBDT + Trim(DM_COMPANY.CO_DIVISION.FieldByName('CUSTOM_DIVISION_NUMBER').AsString);
        if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_BRANCH_NBR').IsNull and
           ((DM_EMPLOYEE.EE_RATES['CO_BRANCH_NBR'] = DM_COMPANY.CO_BRANCH['CO_BRANCH_NBR']) or
             DM_COMPANY.CO_BRANCH.Locate('CO_BRANCH_NBR', DM_EMPLOYEE.EE_RATES['CO_BRANCH_NBR'], [])) and
           (DM_COMPANY.CO_BRANCH.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
        begin
          if sDBDT <> '' then
            sDBDT := sDBDT + ' | ';
          sDBDT := sDBDT + Trim(DM_COMPANY.CO_BRANCH.FieldByName('CUSTOM_BRANCH_NUMBER').AsString);
        end;
        if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_DEPARTMENT_NBR').IsNull and
           ((DM_EMPLOYEE.EE_RATES['CO_DEPARTMENT_NBR'] = DM_COMPANY.CO_DEPARTMENT['CO_DEPARTMENT_NBR']) or
             DM_COMPANY.CO_DEPARTMENT.Locate('CO_DEPARTMENT_NBR', DM_EMPLOYEE.EE_RATES['CO_DEPARTMENT_NBR'], [])) and
           (DM_COMPANY.CO_DEPARTMENT.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
        begin
          if sDBDT <> '' then
            sDBDT := sDBDT + ' | ';
          sDBDT := sDBDT + Trim(DM_COMPANY.CO_DEPARTMENT.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString);
        end;
        if not DM_EMPLOYEE.EE_RATES.FieldByName('CO_TEAM_NBR').IsNull and
           ((DM_EMPLOYEE.EE_RATES['CO_TEAM_NBR'] = DM_COMPANY.CO_TEAM['CO_TEAM_NBR']) or
             DM_COMPANY.CO_TEAM.Locate('CO_TEAM_NBR', DM_EMPLOYEE.EE_RATES['CO_TEAM_NBR'], [])) and
           (DM_COMPANY.CO_TEAM.FieldByName('PRINT_GROUP_ADDRESS_ON_CHECK').AsString <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK) then
        begin
          if sDBDT <> '' then
            sDBDT := sDBDT + ' | ';
          sDBDT := sDBDT + Trim(DM_COMPANY.CO_TEAM.FieldByName('CUSTOM_TEAM_NUMBER').AsString);
        end;

        cbRates.Items.Add(Format('%2d%1.1s %9.9s    %0.40s %0.40s %0.5s',
          [DM_EMPLOYEE.EE_RATES.FieldByName('RATE_NUMBER').AsInteger, s,
           FormatFloat(TFloatField(DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT')).DisplayFormat,
                       DM_EMPLOYEE.EE_RATES.FieldByName('RATE_AMOUNT').AsFloat),
           sDBDT,
           VarToStr(DM_COMPANY.CO_JOBS.Lookup('CO_JOBS_NBR', DM_EMPLOYEE.EE_RATES['CO_JOBS_NBR'], 'DESCRIPTION')),
           VarToStr(DM_COMPANY.CO_WORKERS_COMP.Lookup('CO_WORKERS_COMP_NBR', DM_EMPLOYEE.EE_RATES['CO_WORKERS_COMP_NBR'], 'WORKERS_COMP_CODE'))
           ]));
        if s = 'P' then i := Pred(cbRates.Items.Count);

        DM_EMPLOYEE.EE_RATES.Next;
      end;
      if cbRates.Items.Count > 0 then cbRates.ItemIndex := i;
    finally
      FEENbr := EE_NBR;
      DM_EMPLOYEE.EE_RATES.Filter := sEERatesFilter;
      DM_EMPLOYEE.EE_RATES.Filtered := bEERatesFiltered;
      DM_EMPLOYEE.EE_RATES.EnableControls;
    end;
    RefreshEEStates;
  end;
end;

procedure TREDIT_BATCH.RefreshEEStates;
begin
  if DM_EMPLOYEE.EE_STATES.Active  then
    if not DM_EMPLOYEE.EE_STATES.Locate('EE_NBR', FEENbr, []) then
    begin
      DM_EMPLOYEE.EE_STATES.Close;
//      DM_EMPLOYEE.EE_STATES.DataRequired('ALL');
      DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR=' + IntToStr(FEENbr));
    end;
end;

procedure TREDIT_BATCH.wwDBGrid1ColExit(Sender: TObject);
begin
  inherited;
  if Assigned(QUICK_PAY_DS.DataSet) and
     (QUICK_PAY_DS.DataSet.State = dsEdit) then
    QUICK_PAY_DS.DataSet.CheckBrowseMode;
end;

procedure TREDIT_BATCH.sbCobraWizardClick(Sender: TObject);
begin
  inherited;
  bInsertWithCobraWizard := True;
  bInsertWithWizard := False;
  EnableControls(False);
  try
    (Owner as TFramePackageTmpl).ClickButton(NavInsert);
  except
    bInsertWithCobraWizard := False;
    EnableControls(True);
    raise;
  end;
  CheckSupportDatasets(DM_COMPANY.CO.ClientID, DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger);
end;

procedure TREDIT_BATCH.btnCreateCobraBatchClick(Sender: TObject);
begin
  inherited;
  FNewCobraBatch := True;
  EnableControls(False);
  (Owner as TFramePackageTmpl).ClickButton(NavInsert);
end;

procedure TREDIT_BATCH.EnableControls(aEnable: boolean);
begin
  wwDBLookupCombo1.Enabled := aEnable;
  wwDBLookupCombo2.Enabled := aEnable;
  DBRadioGroup1.Enabled := aEnable;
  DBRadioGroup2.Enabled := aEnable;
  DBRadioGroup3.Enabled := aEnable;
  evSpinEdit1.Enabled := aEnable;
  chbCalcScheduledEDs.Enabled := aEnable;
  evCheckBox2.Enabled := aEnable;
  rgPay.Enabled := aEnable;
  rgCreateChecks.Enabled := aEnable;
  IncludeTimeOffRequests.Enabled := aEnable;
end;

procedure TREDIT_BATCH.PreprocessBtnClick(Sender: TObject);
var
  t: IevPreprocessPayrollTask;
begin
  if not DM_PAYROLL.PR.IsEmpty then
  begin
    if DM_PAYROLL.PR.STATUS.AsString <> PAYROLL_STATUS_PENDING then
    begin
      EvErrMessage('Can''t pre-process this payroll');
      AbortEx;
    end;
    if not TOBalanceCheck(ifrPreProcessRemote, Self) then
      Exit;
    if EvMessage('Are you sure you want to pre-process this payroll?', mtConfirmation, mbOKCancel) <> mrOK then
      Exit;

    (Owner as TFramePackageTmpl).ClickButton(NavCommit);

    t := mb_TaskQueue.CreateTask(QUEUE_TASK_PREPROCESS_PAYROLL, True) as IevPreprocessPayrollTask;
    try
      t.PrList := IntToStr(ctx_DataAccess.ClientID) + ';' + DM_PAYROLL.PR.PR_NBR.AsString + ' ';
      t.Caption := 'Co:' + Trim(DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString)
        + ' Pr:' + DM_PAYROLL.PR.CHECK_DATE.AsString + '-' + DM_PAYROLL.PR.RUN_NUMBER.AsString;
      ctx_EvolutionGUI.AddTaskQueue(t);
    finally
      t := nil;
    end;
  end;
end;

function TREDIT_BATCH.GetSBName: string;
begin
  if Context.UserAccount.SBAccountantNbr = 0 then
    Result := DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString
  else
  begin
    ctx_DataAccess.OpenDataSets([DM_SERVICE_BUREAU.SB_ACCOUNTANT]);
    if DM_SERVICE_BUREAU.SB_ACCOUNTANT.SB_ACCOUNTANT_NBR.AsInteger <> Context.UserAccount.SBAccountantNbr then
      Assert(DM_SERVICE_BUREAU.SB_ACCOUNTANT.Locate('SB_ACCOUNTANT_NBR', Context.UserAccount.SBAccountantNbr, []));
    Result := DM_SERVICE_BUREAU.SB_ACCOUNTANT.FieldByName('NAME').AsString;
  end;
end;

function TREDIT_BATCH.PayrollIsOutOfRange: Boolean;
var
  DaysPriorToCheckDate: Integer;
  AllNull: Boolean;
begin

  Result := False;

  if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_REGULAR then
    Exit;

  DaysPriorToCheckDate := 0;

  DM_COMPANY.CO.Locate('CO_NBR', DM_PAYROLL.PR.CO_NBR.Value, []);

  if not DM_COMPANY.CO.DAYS_PRIOR_TO_CHECK_DATE.IsNull then
  begin
    AllNull := False;
    DaysPriorToCheckDate := DM_COMPANY.CO.DAYS_PRIOR_TO_CHECK_DATE.AsInteger;
  end
  else
    AllNull := True;

  if AllNull and not DM_SERVICE_BUREAU.SB.DAYS_PRIOR_TO_CHECK_DATE.IsNull then
  begin
    AllNull := False;
    DaysPriorToCheckDate := DM_SERVICE_BUREAU.SB.DAYS_PRIOR_TO_CHECK_DATE.AsInteger;
  end;

  if not AllNull and (Trunc(Now) - Trunc(DM_PAYROLL.PR.CHECK_DATE.AsDateTime) >= DaysPriorToCheckDate) then
  begin
    Result := True;
  end;

end;

procedure TREDIT_BATCH.SubmitBtnClick(Sender: TObject);
var
  s: string;
  t: IevProcessPayrollTask;
  PrNbr: Integer;
  Q: IevQuery;
begin
  inherited;
  if not DM_PAYROLL.PR.IsEmpty then
  begin
    PrNbr := DM_PAYROLL.PR.PR_NBR.AsInteger;
    SaveDSStates([DM_PAYROLL.PR]);
    DM_PAYROLL.PR.Close;
    LoadDSStates([DM_PAYROLL.PR]);
    DM_PAYROLL.PR.Locate('PR_NBR', PrNbr, []);
    if (DM_PAYROLL.PR.STATUS.AsString <> PAYROLL_STATUS_PENDING) then
    begin
      EvErrMessage('Can not submit this payroll');
      AbortEx;
    end;

    if not TOBalanceCheck(ifrSubmitRemote, Self) then
      Exit;
    s := 'Are you sure you want to submit this payroll?';
    if EvMessage(s, mtConfirmation, mbOKCancel) <> mrOK then
      Exit;

    Q := TevQuery.Create('select c.SY_STATES_NBR, c.STATE, m.CUSTOM_COMPANY_NUMBER'+
      ' from CO_STATES c'+
      ' join CO_STATES x on x.SY_STATES_NBR=c.SY_STATES_NBR'+
      ' join CO m on m.CO_NBR=c.CO_NBR'+
      ' join CO n on n.CO_NBR=x.CO_NBR'+
      ' join CL_COMMON_PAYMASTER z on z.CL_COMMON_PAYMASTER_NBR=m.CL_COMMON_PAYMASTER_NBR'+
      ' where c.CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString+
      ' and x.CO_NBR<>c.CO_NBR'+
      ' and x.SY_STATE_DEPOSIT_FREQ_NBR<>c.SY_STATE_DEPOSIT_FREQ_NBR'+
      ' and m.CL_COMMON_PAYMASTER_NBR=n.CL_COMMON_PAYMASTER_NBR'+
      ' and z.COMBINE_STATE_SUI=''Y''' +
      ' and {AsOfNow<c>}'+
      ' and {AsOfNow<x>}'+
      ' and {AsOfNow<m>}'+
      ' and {AsOfNow<n>}'+
      ' and {AsOfNow<z>}');
    if not Q.Result.Eof then
    begin
      s := 'Consolidated company#'+Q.Result.FieldByName('CUSTOM_COMPANY_NUMBER').AsString +
           ' has different deposit frequency for same State '+ Q.Result.FieldByName('STATE').AsString +
        #13#10'Are you sure you want to submit this payroll?';
      if EvMessage(s, mtConfirmation, mbOKCancel) <> mrOK then
        Exit;
    end;

    Q := TevQuery.Create('select m.CUSTOM_COMPANY_NUMBER'+
      ' from CO n'+
      ' join CO m on n.CO_NBR<>m.CO_NBR'+
      ' join CL_COMMON_PAYMASTER z on z.CL_COMMON_PAYMASTER_NBR=n.CL_COMMON_PAYMASTER_NBR'+
      ' where n.CO_NBR='+DM_CLIENT.PR.CO_NBR.AsString+
      ' and m.CO_NBR<>n.CO_NBR'+
      ' and m.FEDERAL_TAX_DEPOSIT_FREQUENCY<>n.FEDERAL_TAX_DEPOSIT_FREQUENCY'+
      ' and m.CL_COMMON_PAYMASTER_NBR=n.CL_COMMON_PAYMASTER_NBR'+
      ' and (z.COMBINE_MEDICARE=''Y'' or z.COMBINE_OASDI=''Y'' or z.COMBINE_FUI=''Y'')' +
      ' and {AsOfNow<m>}'+
      ' and {AsOfNow<n>}'+
      ' and {AsOfNow<z>}');
    if not Q.Result.Eof then
    begin
      s := 'Consolidated company#'+Q.Result.FieldByName('CUSTOM_COMPANY_NUMBER').AsString +
           ' has different deposit frequency for Federal Taxes '+
        #13#10'Are you sure you want to submit this payroll?';
      if EvMessage(s, mtConfirmation, mbOKCancel) <> mrOK then
        Exit;
    end;

    DM_COMPANY.CO.DataRequired('');
    DM_COMPANY.CO.Open;
    DM_COMPANY.CO.Locate('CO_NBR', DM_PAYROLL.PR.CO_NBR.Value, []);

    if ( TypeIsCorrectionRun(DM_PAYROLL.PR.PAYROLL_TYPE.AsString)
         or ( ctx_AccountRights.Functions.GetState('MARK_PAYROLL_COMPLETED') <> stEnabled )) then
    begin
        DM_PAYROLL.PR.Edit;
        DM_PAYROLL.PR.STATUS.Value := PAYROLL_STATUS_HOLD;
        DM_PAYROLL.PR.Post;
        ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
    end
    else
    if (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_REGULAR) and
    (ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_BACKDATED_PAYROLLS') <> stEnabled)
      and (Trunc(Now) - Trunc(DM_PAYROLL.PR.CHECK_DATE.AsDateTime) >= 0) then
    begin
      raise EInconsistentData.CreateHelp('You do not have the ability to submit a backdated payroll.', IDH_InconsistentData);
    end
    else
    if (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_REGULAR) and PayrollIsOutOfRange then
    begin
      if EvMessage('This payroll is out of range. If you proceed with this payroll it will need to be approved in the Operations-Approve Payroll screen. Do you wish to proceed?',
                 mtConfirmation, [mbYes, mbNo]) <> mrYes then
        Exit;

      DM_PAYROLL.PR.Edit;
      DM_PAYROLL.PR.STATUS.Value := PAYROLL_STATUS_BACKDATED;
      DM_PAYROLL.PR.Post;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
    end
    else if ( TypeIsManagerApproval(DM_PAYROLL.PR.PAYROLL_TYPE.AsString)
              and (DM_COMPANY.CO.PAYROLL_REQUIRES_MGR_APPROVAL.Value = GROUP_BOX_YES)) then
    begin
      DM_PAYROLL.PR.Edit;
      DM_PAYROLL.PR.STATUS.Value := PAYROLL_STATUS_REQUIRED_MGR_APPROVAL;
      DM_PAYROLL.PR.Post;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
    end
    else
    begin
      DM_PAYROLL.PR.Edit;
      DM_PAYROLL.PR.STATUS.Value := PAYROLL_STATUS_COMPLETED;
      DM_PAYROLL.PR.Post;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
    end;

//    CheckStatus;
    if (Owner as TFramePackageTmpl).ButtonEnabled(NavCommit) then
      (Owner as TFramePackageTmpl).ClickButton(NavCommit);

    if (DM_PAYROLL.PR.STATUS.Value = PAYROLL_STATUS_COMPLETED) and
       (ctx_AccountRights.Functions.GetState('SEND_PAYROLL_DIRECTLY') = stEnabled) and
       (EvMessage('Process this payroll?', mtConfirmation, [mbYes,mbNo]) = mrYes) then
    begin
      DM_PAYROLL.PR.Edit;
      DM_PAYROLL.PR.STATUS_DATE.AsDateTime := SysTime;
      DM_PAYROLL.PR.Post;
      ctx_DataAccess.PayrollIsProcessing := True;
      try
        ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
      finally
        ctx_DataAccess.PayrollIsProcessing := False;
      end;

      t := mb_TaskQueue.CreateTask(QUEUE_TASK_PROCESS_PAYROLL, True) as IevProcessPayrollTask;
      t.Destination := rdtPrinter;
      t.PrList := IntToStr(ctx_DataAccess.ClientId) + ';' + DM_PAYROLL.PR.PR_NBR.AsString;
      t.Caption := t.Caption + 'Co:' + Trim(DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString)
        + ' Pr:' + DM_PAYROLL.PR.CHECK_DATE.AsString + '-' + DM_PAYROLL.PR.RUN_NUMBER.AsString;
      ctx_EvolutionGUI.AddTaskQueue(t);
      t := nil;
    end
    else
    begin
      EvMessage('Thank you. Your payroll has been successfully submitted to ' +
        Trim(GetSBName) + ' for processing.', mtInformation, [mbOK]);
    end;
  end;
end;

procedure TREDIT_BATCH.IncludeTimeOffRequestsClick(Sender: TObject);
begin
  inherited;
  if IncludeTimeOffRequests.Checked and (IncludeTimeOffRequests.Tag = 0) then
  if EvMessage('Selecting the Include Time Off Requests flag will cause the system to search for approved Time Off Requests that were submitted through Employee Self Service. Are you sure you want to continue?',
    mtWarning, [mbYes, mbNo]) <> mrYes then
  begin
    IncludeTimeOffRequests.Checked := False;
    Exit;
  end;
  mb_AppSettings.AsBoolean['IncludeTimeOffRequests'] := IncludeTimeOffRequests.Checked;
end;

procedure TREDIT_BATCH.spbFavoriteReportsClick(Sender: TObject);
begin
  CallFavoriteReports;
end;

procedure TREDIT_BATCH.pnlQuickEntryResize(Sender: TObject);
begin
  inherited;
  pnlQuickEntryBody.Height := pnlQuickEntry.Height - 65;
  pnlQuickEntryBody.Width  := pnlQuickEntry.Width - 40;
end;

procedure TREDIT_BATCH.pnlQE2Resize(Sender: TObject);
begin
  inherited;
  pnlQE2Body.Height := pnlQE2.Height - 65;
  pnlQE2Body.Width  := pnlQE2.Width - 40;
end;

procedure TREDIT_BATCH.EE_DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  //the EE CODE filed is not trimming leading spaces and makes it look
  //like the field is right aligned. Looks stupid. Hid that field and put
  //the trimmed value on the label.
  lblTrimedEECode.Caption := Trim(dbtEENumber.Caption);
 
end;

procedure TREDIT_BATCH.QuickEDGriddgQEDRowChanged(Sender: TObject);
begin
  inherited;
  QuickEDGrid.dgQEDRowChanged(Sender);

end;

procedure TREDIT_BATCH.BtnMRCLogInClick(Sender: TObject);
begin
  CallMyRecruitingCenterWebSite;
end;

procedure TREDIT_BATCH.btnSwipeClockClick(Sender: TObject);
begin
  CallSwipeClockWebSite;
end;

procedure TREDIT_BATCH.GridToXls(Grid: TISDBGrid; XLS: TXLSFile);
var
  S: TSheet;
  R, i: integer;
begin
  S := XLS.Workbook.Sheets[0];
  Grid.DataSource.DataSet.DisableControls;
  try
    for i := 0 to Grid.GetColCount - 1 do
      S.Cells[0, i].Value := Grid.Columns[i].DisplayLabel;
    R := 1;
    Grid.DataSource.DataSet.First;
    while not Grid.DataSource.DataSet.Eof do
    begin
      for i := 0 to Grid.Datalink.FieldCount - 1 do
        if not Grid.Datalink.Fields[i].IsNull then
          S.Cells[R, i].Value := Grid.Datalink.Fields[i].Value;
      Grid.DataSource.DataSet.Next;
      Inc(R);
    end;
  finally
    Grid.DataSource.DataSet.First;
    Grid.DataSource.DataSet.EnableControls;
  end;
end;

procedure TREDIT_BATCH.QuickEDGriddgQEDKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  XLS: TXLSFile;
begin
  //the following code was brought forward from the isClasses ancestor
  //for some unknown reason, likely the painting routines, this never
  //gets fired.

  if QuickEDGrid.dgQED.LocateDlg and (Char(Key) = 'F') and (Shift = [ssCtrl]) then
    QuickEDGrid.dgQED.ExecLocateDlg;
  if (Char(Key) = 'Y') and (Shift = [ssCtrl]) then
    QuickEDGrid.dgQED.ExecHideDlg;
  if QuickEDGrid.dgQED.FilterDlg and Assigned(QuickEDGrid.dgQED.IndicatorButton) and (Char(Key) = 'R') and (Shift = [ssCtrl]) then
    QuickEDGrid.dgQED.IndicatorButton.Click;
  if (Char(Key) = 'E') and (Shift = [ssCtrl]) then
  begin
    with TSaveDialog.Create(nil) do
    try
      Options := Options + [ofOverwritePrompt];
      DefaultExt := 'xls';
      Filter := 'XLS files (*.xls)|*.xls|SYLK files (*.slk)|*.slk';
      if Execute then
      begin
        if FilterIndex = 1 then
        begin
          XLS := TXLSFile.Create;
          try
            GridToXls(QuickEDGrid.dgQED, XLS);
            XLS.SaveAs(FileName);
          finally
            XLS.Free;
          end;
        end
        else
        begin
          QuickEDGrid.dgQED.ExportOptions.FileName := FileName;
          QuickEDGrid.dgQED.ExportOptions.Options := QuickEDGrid.dgQED.ExportOptions.Options - [esoClipboard];
          QuickEDGrid.dgQED.ExportOptions.Save;
        end;
      end;
    finally
      Free;
    end;
  end;

  inherited;
  //end of ctrl-e override
  QuickEDGrid.dgQEDKeyDown(Sender, Key, Shift);

end;


procedure TREDIT_BATCH.btnPreProcessBatchClick(Sender: TObject);
var
  t: IevPreprocessPayrollTask;
begin
  if EvMessage('Are you sure you want to pre-process this batch?', mtConfirmation, mbOKCancel) <> mrOK then
    Exit;

  t := mb_TaskQueue.CreateTask(QUEUE_TASK_PREPROCESS_PAYROLL, True) as IevPreprocessPayrollTask;
  try
    t.PrList := IntToStr(ctx_DataAccess.ClientID) + ';' + DM_PAYROLL.PR.PR_NBR.AsString + '*' + DM_PAYROLL.PR_BATCH.PR_BATCH_NBR.AsString + ' ';
    t.Caption := 'Co:' + Trim(DM_COMPANY.CO.CUSTOM_COMPANY_NUMBER.AsString)
      + ' Pr:' + DM_PAYROLL.PR.CHECK_DATE.AsString + '-' + DM_PAYROLL.PR.RUN_NUMBER.AsString;
    ctx_EvolutionGUI.AddTaskQueue(t);
  finally
    t := nil;
  end;
end;

initialization
  RegisterClass(TREDIT_BATCH);

end.
