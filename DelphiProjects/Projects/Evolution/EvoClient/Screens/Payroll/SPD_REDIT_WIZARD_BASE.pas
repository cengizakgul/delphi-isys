// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_REDIT_WIZARD_BASE;

interface

uses
   SFrameEntry, StdCtrls, Buttons, Controls, ExtCtrls, SDataStructure,
  Classes, Db, Wwdatsrc, ActnList, SPackageEntry, EvBasicUtils,
  ISBasicClasses, EvContext, EvMainboard, EvConsts, EvUIUtils, EvUIComponents,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

var
  FiWizardPosition: Integer;

type
  TREDIT_WIZARD_BASE = class(TFrameEntry)
    Panel1: TevPanel;
    BitBtn4: TevSpeedButton;
    BitBtn3: TevSpeedButton;
    ActionList: TevActionList;
    Next: TAction;
    Back: TAction;
    cbCalcCL: TevCheckBox;
    aWizard: TAction;
    evPanel4: TevPanel;
    sbWizard: TevSpeedButton;
    procedure NextExecute(Sender: TObject);
    procedure BackExecute(Sender: TObject);
    procedure cbCalcCLClick(Sender: TObject);
  private
    procedure SetiWizardPosition(const Value: Integer);
    function GetWizardPosition: Integer;
  protected
    procedure SetReadOnly(Value: Boolean); override;
    property iWizardPosition: Integer read GetWizardPosition write SetiWizardPosition;
    function GetIfReadOnly: Boolean; override;
  public
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
    procedure Activate; override;
    procedure CheckWizardButton; virtual;
    procedure CallFavoriteReports;
    procedure CallSwipeClockWebSite;
    procedure CallMyRecruitingCenterWebSite;
    function  VisibleSwipeClock:boolean;
    function  VisibleMyRecruitingCenter:boolean;
  end;

const
  WIZ_UNKNOWN = 0;
  WIZ_PAYROLL_CREATED = 1;
  WIZ_BATCH_CREATED = 2;
  WIZ_CHECKS_CREATED = 3;
  WIZ_TOTALS_AFTER_CHECKS = 4;
  WIZ_PREPROCESSED = 6;
  WIZ_TOTALS_AFTER_PREPROCESS = 7;

implementation

{$R *.DFM}

uses SysUtils, EvUtils, SPayrollScr,SPD_CO_FAVORITE_REPORTS, isBasicUtils, evTypes, IsBaseClasses;

procedure TREDIT_WIZARD_BASE.NextExecute(Sender: TObject);
begin
  inherited;
  BitBtn3.Click;
end;

procedure TREDIT_WIZARD_BASE.BackExecute(Sender: TObject);
begin
  inherited;
  BitBtn4.Click;
end;

procedure TREDIT_WIZARD_BASE.SetReadOnly(Value: Boolean);
begin
  inherited;
  BitBtn3.SecurityRO := False;
  BitBtn4.SecurityRO := False;
end;

procedure TREDIT_WIZARD_BASE.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
  if Kind = NavOK then
  begin
    BitBtn4.Enabled := not Value;
    BitBtn3.Enabled := not Value;
    sbWizard.Enabled := not Value;
  end;
end;

function TREDIT_WIZARD_BASE.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;
  if ctx_DataAccess.GetClCoReadOnlyRight then
     Result := True;
end;


procedure TREDIT_WIZARD_BASE.Activate;
begin
  inherited;
  cbCalcCL.Checked := mb_AppSettings.AsBoolean['CalcCheckLines'];
  CheckWizardButton;

  if (DM_CLIENT.CL.FieldByName('READ_ONLY').AsString = READ_ONLY_MAINTENANCE) or
     (DM_CLIENT.CL.FieldByName('READ_ONLY').AsString = READ_ONLY_READ_ONLY) or
     (DM_COMPANY.CO.FieldByName('CREDIT_HOLD').AsString = CREDIT_HOLD_LEVEL_READONLY) then
  begin
     if ctx_DataAccess.GetClCoReadOnlyRight then
     begin
       SetReadOnly(True);
       (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
       (Owner as TFramePackageTmpl).SetButton(NavDelete, False);
     end
     else
     if not GetIfReadOnly then
        ClearReadOnly;
  end;
end;

procedure TREDIT_WIZARD_BASE.cbCalcCLClick(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsBoolean['CalcCheckLines'] := cbCalcCL.Checked;
  ctx_DataAccess.RecalcLineBeforePost := True;
  if cbCalcCL.Focused and cbCalcCL.Checked then
  begin
    EvMessage('Checking this box will considerably slow down opening your payrolls');
    if DetailPayrollOpen <> 0 then
      ctx_PayrollCalculation.GetLimitedEDs(DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger, DetailPayrollOpen,
                       GetBeginYear(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime), DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime);
  end;
end;

procedure TREDIT_WIZARD_BASE.CheckWizardButton;
begin
  sbWizard.Visible := iWizardPosition <> WIZ_UNKNOWN;
  case iWizardPosition of
  WIZ_PAYROLL_CREATED: sbWizard.Caption := 'Create Batch';
  WIZ_BATCH_CREATED: sbWizard.Caption := 'Quick Entry';
  WIZ_CHECKS_CREATED: sbWizard.Caption := 'Batch Totals';
  WIZ_TOTALS_AFTER_CHECKS: sbWizard.Caption := 'Complete Payroll';
  WIZ_PREPROCESSED: sbWizard.Caption := 'Payroll Totals';
  WIZ_TOTALS_AFTER_PREPROCESS: sbWizard.Caption := 'Submit Payroll';
  end;
end;

procedure TREDIT_WIZARD_BASE.SetiWizardPosition(const Value: Integer);
begin
  FiWizardPosition := Value;
  CheckWizardButton;
end;

function TREDIT_WIZARD_BASE.GetWizardPosition: Integer;
begin
  Result := FiWizardPosition;
end;

procedure TREDIT_WIZARD_BASE.CallFavoriteReports;
begin
  if Assigned(DM_COMPANY.CO) and
     (DM_COMPANY.CO.Active = True) and
     ( DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger > 0) then
  begin
    TCO_FavoriteReports.execute (Trim(DM_COMPANY.CO.FieldByName('CO_NBR').Asstring),
                      Trim(DM_COMPANY.CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString));
  end;
end;

procedure TREDIT_WIZARD_BASE.CallMyRecruitingCenterWebSite;
var
  sURL: String;
begin
  if Assigned(DM_COMPANY.CO) and
     (DM_COMPANY.CO.Active = True) and
     ( DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger > 0) then
  begin
    ctx_StartWait('Please wait ...');
    try
      sURL := Context.SingleSignOn.GetMRCOneTimeURL(DM_COMPANY.CO.CO_NBR.AsInteger);
      if Trim(sURL) <> '' then
         RunIsolatedProcess(sURL, '', [])
      else
         EvErrMessage('No URL from Server Side.');
    finally
      ctx_EndWait;
    end;
  end;
end;

procedure TREDIT_WIZARD_BASE.CallSwipeClockWebSite;
var
  sURL: String;
begin
  if Assigned(DM_COMPANY.CO) and
     (DM_COMPANY.CO.Active = True) and
     ( DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger > 0) then
  begin
    ctx_StartWait('Please wait ...');
    try
      sURL := Context.SingleSignOn.GetSwipeclockOneTimeURL(DM_COMPANY.CO.CO_NBR.AsInteger);
      if Trim(sURL) <> '' then
         RunIsolatedProcess(sURL, '', [])
      else
         EvErrMessage('No URL from Server Side.');
    finally
      ctx_EndWait;
    end;
  end;
end;

function TREDIT_WIZARD_BASE.VisibleMyRecruitingCenter: Boolean;
var
  MRCAPIKey: IisListOfValues;
begin
  MRCAPIKey := Context.License.FindAPIKey(tvkEvoMyRecruitingCenter);
  Result := Assigned(MRCAPIKey) and
            (ctx_Security.AccountRights.Functions.GetState('USER_CAN_LOGIN_MY_RECRUITING_CENTER') = stEnabled);

end;

function TREDIT_WIZARD_BASE.VisibleSwipeClock : boolean;
var
  SwipeClockAPIKey: IisListOfValues;
begin
  SwipeClockAPIKey := Context.License.FindAPIKey(tvkEvoSwipeClockAdvanced);
  Result := Assigned(SwipeClockAPIKey) and
            (ctx_Security.AccountRights.Functions.GetState('USER_CAN_LOGIN_SWIPECLOCK') = stEnabled);
end;

end.
