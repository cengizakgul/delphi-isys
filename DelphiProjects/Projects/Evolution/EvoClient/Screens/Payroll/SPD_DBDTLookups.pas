unit SPD_DBDTLookups;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents,  EvUIComponents, EvClientDataSet;

type
  TDBDTDatasets = record
    Original: TEvClientDataSet;
    Clone: TEvClientDataSet;
  end;

  PDBDTEntry = ^TDBDTEntry;
  TDBDTEntry = record
    Nbr: integer;
    Parent: PDBDTEntry;
    PrintOnCheck: Char;
    CustomNumber: string;
    Name: string;
  end;

  TDBDTEntries = array of TDBDTEntry;

  TDBDTSelectionStr = record
    Text: string;
    LastPartLength: integer;
  end;

  TDBDTLookups = class(TFrame)
    CO_DIVISIONClone: TevClientDataSet;
    CO_BRANCHClone: TevClientDataSet;
    CO_DEPARTMENTClone: TevClientDataSet;
    CO_TEAMClone: TevClientDataSet;
    cdDBDT: TevClientDataSet;
    cdDBDTDIVISION_NAME: TStringField;
    cdDBDTBRANCH_NAME: TStringField;
    cdDBDTDEPARTMENT_NAME: TStringField;
    cdDBDTTEAM_NAME: TStringField;
    cdDBDTDBDT_CUSTOM_NUMBERS: TStringField;
    cdDBDTBRANCH_NBR: TIntegerField;
    cdDBDTDEPARTMENT_NBR: TIntegerField;
    cdDBDTTEAM_NBR: TIntegerField;
    cdDBDTDIVISION_NBR: TIntegerField;
    cdDBDTCO_NBR: TIntegerField;
    procedure CloneFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    FRefDS: TEvClientDataSet;
    FRefDSFields: array [0..3] of TIntegerField;
    FDS: array [0..3] of TDBDTDatasets;
    FTree: array [-1..3] of TDBDTEntries;
    FGoingUp: Boolean;
    function FindEntry( level: integer; nbr: integer ): PDBDTEntry;
    procedure FilterRecord( Level: integer; DataSet: TDataSet; var Accept: Boolean);
    procedure FieldChange( Level: integer );

    procedure FillDBDT( Level: integer );
    procedure SetRefDS(const Value: TEvClientDataSet);
    procedure CO_DIVISION_NBRFieldChange( Sender: TField);
    procedure CO_BRANCH_NBRFieldChange( Sender: TField);
    procedure CO_DEPARTMENT_NBRFieldChange( Sender: TField);
    procedure CO_TEAM_NBRFieldChange( Sender: TField);
//    procedure RefDSCalcFields(DataSet: TDataSet);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
//    constructor Create( Owner: TComponent ); override;
    procedure Activate;
    procedure Deactivate;
    function GetStringForFilter( leafnbr: Variant ): TDBDTSelectionStr;
    property RefDS: TEvClientDataSet write SetRefDS;
  end;

function DBDTLevel: integer;
function DBDTLeafField: string;

implementation

uses
  sDataStructure, evconsts, evutils;

{$R *.dfm}


type
  TDBDTInfo = record
    KeyField: string;
    PrintOnCheckField: string;
    CustomNumberField: string;
    NameField: string; //in cdDBDT
  end;

const
  DBDTInfo: array [-1..3] of TDBDTInfo = (
    (KeyField: 'CO_NBR'),
    (KeyField: 'CO_DIVISION_NBR'; PrintOnCheckField: 'PRINT_DIV_ADDRESS_ON_CHECKS'; CustomNumberField: 'CUSTOM_DIVISION_NUMBER'; NameField: 'DIVISION_NAME'),
    (KeyField: 'CO_BRANCH_NBR'; PrintOnCheckField: 'PRINT_BRANCH_ADDRESS_ON_CHECK'; CustomNumberField: 'CUSTOM_BRANCH_NUMBER';NameField: 'BRANCH_NAME'),
    (KeyField: 'CO_DEPARTMENT_NBR'; PrintOnCheckField: 'PRINT_DEPT_ADDRESS_ON_CHECKS'; CustomNumberField: 'CUSTOM_DEPARTMENT_NUMBER'; NameField: 'DEPARTMENT_NAME'),
    (KeyField: 'CO_TEAM_NBR'; PrintOnCheckField: 'PRINT_GROUP_ADDRESS_ON_CHECK'; CustomNumberField: 'CUSTOM_TEAM_NUMBER'; NameField: 'TEAM_NAME')
  );

function DBDTLevel: integer;
begin
  //!! assume that constants are '0'..'4' here; fix it later
  Assert( DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString[1] in [CLIENT_LEVEL_COMPANY..CLIENT_LEVEL_TEAM]);
  Result := strtoint(DM_COMPANY.CO.FieldByName('DBDT_LEVEL').AsString) - 1;
end;


{ TDBDTLookups }

function TDBDTLookups.FindEntry(level, nbr: integer): PDBDTEntry;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to high(FTree[level]) do
    if FTree[level][i].Nbr = nbr then
    begin
      Result := @(FTree[level][i]);
      break;
    end;
end;

procedure TDBDTLookups.Activate;
  procedure FillTreeLevel( level: integer );
  var
    i: integer;
    clone: TevClientDataSet;
    parent: PDBDTEntry;
  begin
    clone := TevClientDataSet.Create(self);
    try
      clone.CloneCursor( FDS[level].Original, true );
      clone.First;

      SetLength( FTree[level], clone.RecordCount );
      i := 0;

      while not clone.Eof do
      begin
        FTree[level][i].Nbr := clone.FieldByName( DBDTInfo[level].KeyField ).AsInteger;
        FTree[level][i].PrintOnCheck := clone.FieldByName( DBDTInfo[level].PrintOnCheckField ).AsString[1];
        FTree[level][i].CustomNumber := clone.FieldByName( DBDTInfo[level].CustomNumberField ).AsString;
        FTree[level][i].Name := clone.FieldByName( 'Name' ).AsString;

        parent := FindEntry(level-1, clone.FieldByName( DBDTInfo[level-1].KeyField ).Value);
        if parent <> nil then
        begin
          FTree[level][i].Parent := parent;
          inc(i);
        end;
        clone.Next;
      end;

      SetLength(FTree[level],i);
    finally
      FreeAndNil( clone );
    end;
  end;
var
  i: integer;
begin
  FDS[0].Original := DM_CLIENT.CO_DIVISION;
  FDS[0].Clone := CO_DIVISIONClone;
  FDS[1].Original := DM_CLIENT.CO_BRANCH;
  FDS[1].Clone := CO_BRANCHClone;
  FDS[2].Original := DM_CLIENT.CO_DEPARTMENT;
  FDS[2].Clone := CO_DEPARTMENTClone;
  FDS[3].Original := DM_CLIENT.CO_TEAM;
  FDS[3].Clone := CO_TEAMClone;

  for i := 0 to 3 do
  begin
    FDS[i].Clone.Tag := i;
    FDS[i].Clone.CloneCursor( FDS[i].Original, false, false );
  end;

  SetLength( FTree[-1], 1 );
  FTree[-1][0].Nbr := DM_COMPANY.CO.FieldbyName('CO_NBR').AsInteger;
  FTree[-1][0].Parent := nil;

  for i := 0 to 3 do
    FillTreeLevel( i );

  cdDBDT.DisableControls;
  try
    cdDBDT.CreateDataSet;
    if DBDTLevel <> -1 then
      FillDBDT( DBDTLevel );
  finally
    cdDBDT.EnableControls;
  end;
end;

procedure TDBDTLookups.Deactivate;
var
  i: integer;
begin
  RefDS := nil;
  for i := 0 to 3 do
    FDS[i].Clone.Reset;
  cdDBDT.Reset;  
end;

procedure TDBDTLookups.CloneFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  FilterRecord( DataSet.Tag, DataSet, Accept );
end;

procedure TDBDTLookups.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  if (Operation = opRemove) and (AComponent = FRefDS) then
    RefDS := nil;
  inherited;
end;

procedure TDBDTLookups.SetRefDS(const Value: TEvClientDataSet);
var
  i: integer;
begin
  if FRefDS <> Value then
  begin
    if FRefDS <> nil then
    begin
      for i := 0 to 3 do
      begin
        FRefDSFields[i].OnChange := nil;
        FRefDSFields[i] := nil;
      end;
//      FRefDS.OnCalcFields := nil;
    end;
    FRefDS := Value;

    if FRefDS <> nil then
    begin
      for i := 0 to 3 do
      begin
        FRefDSFields[i] := FRefDS.FieldByName(DBDTInfo[i].KeyField) as TIntegerField;
        Assert( not assigned(FRefDSFields[i].OnChange) );
      end;
      FRefDSFields[0].OnChange := Self.CO_DIVISION_NBRFieldChange;
      FRefDSFields[1].OnChange := Self.CO_BRANCH_NBRFieldChange;
      FRefDSFields[2].OnChange := Self.CO_DEPARTMENT_NBRFieldChange;
      FRefDSFields[3].OnChange := Self.CO_TEAM_NBRFieldChange;
//      Assert( not assigned(FRefDS.OnCalcFields) );
//      FRefDS.OnCalcFields := Self.RefDSCalcFields;
    end
  end;
end;

procedure TDBDTLookups.CO_DIVISION_NBRFieldChange(Sender: TField);
begin
  FieldChange( 0 );
end;

procedure TDBDTLookups.CO_BRANCH_NBRFieldChange(Sender: TField);
begin
  FieldChange( 1 );
end;

procedure TDBDTLookups.CO_DEPARTMENT_NBRFieldChange(Sender: TField);
begin
  FieldChange( 2 );
end;

procedure TDBDTLookups.CO_TEAM_NBRFieldChange(Sender: TField);
begin
  FieldChange( 3 );
end;

procedure TDBDTLookups.FieldChange(Level: integer);
var
  entry: PDBDTEntry;
begin
  //ODS('Field changed: %s <%s>', [FRefDSFields[level].Fieldname, FRefDSFields[level].Asstring]);
  if (level < 3) and not FGoingUp then
    if not FRefDSFields[level+1].IsNull then
      FRefDSFields[level+1].Clear;

  if (level > 0) and not FRefDSFields[level].IsNull then
  begin
    entry := FindEntry( level, FRefDSFields[level].Value );
    if entry <> nil then
      if FRefDSFields[level-1].IsNull or (FRefDSFields[level-1].Value <> entry.Parent.Nbr) then
        try
          FGoingUp := true;
          FRefDSFields[level-1].Value := entry.Parent.Nbr;
        finally
          FGoingUp := false;
        end;
  end;
end;

procedure TDBDTLookups.FilterRecord(Level: integer; DataSet: TDataSet; var Accept: Boolean);
var
  entry: PDBDTEntry;
  l: integer;
begin
  try
    Accept := false;
    if dataset.FieldByName( DBDTInfo[Level].PrintOnCheckField ).Value = EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
      exit;
    entry := FindEntry( Level, dataset.fieldbyName( DBDTInfo[Level].KeyField).AsInteger );
    if entry = nil then
      exit;
    l := Level;
    while l > 0 do
    begin
      entry := entry.Parent;
      dec(l);
      if not FRefDSFields[l].IsNull then
        if FRefDSFields[l].Value = entry.Nbr then
          break
        else
          exit;
    end;
    Accept := true;
  finally
//    ODS( 'TDBDTLookups.FilterRecord: %s: key = %d',[DataSet.Name, dataset.fieldbyName( DBDTInfo[Level].KeyField).AsInteger]);
  end;
end;

procedure TDBDTLookups.FillDBDT(Level: integer);
var
  i: integer;
  l: integer;
  entry: PDBDTEntry;
  dbdtnumbers: string;
begin
  for i := low(FTree[Level]) to high(FTree[Level]) do //for each leaf do
  begin
    entry := @(FTree[Level][i]);
    if entry.PrintOnCheck <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
    begin
      cdDBDT.Append;
      try
        dbdtnumbers := '';
        for l := Level downto 0 do
        begin
          cdDBDT[DBDTInfo[l].KeyField] := entry.Nbr;
          if entry.PrintOnCheck <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
          begin
            cdDBDT[DBDTInfo[l].NameField] := entry.Name;
            if dbdtnumbers <> '' then
              dbdtnumbers := ' | ' + dbdtnumbers;
            dbdtnumbers := trim(entry.CustomNumber) + dbdtnumbers;
          end;
          entry := entry.Parent;
        end;
        cdDBDT['DBDT_CUSTOM_NUMBERS'] := dbdtnumbers;
        cdDBDT['CO_NBR'] := DM_COMPANY.CO.CO_NBR.Value; //!!
        cdDBDT.Post;
      except
        cdDBDT.Cancel;
        raise;
      end;
//      ODS('cdDBDT: '+VarToStr(cdDBDT['DBDT_CUSTOM_NUMBERS'])+';'+VarToStr(cdDBDT['CO_DEPARTMENT_NBR']) );
    end
  end
end;

function DBDTLeafField: string;
begin
  Result := DBDTInfo[DBDTLevel].KeyField;
end;

{
procedure TDBDTLookups.RefDSCalcFields(DataSet: TDataSet);
begin
  DataSet['DBDT_CUSTOM_NUMBERS'] := 'Blah blah';
end;
}

function TDBDTLookups.GetStringForFilter(leafnbr: Variant): TDBDTSelectionStr;
var
  entry: PDBDTEntry;
  l: integer;
  s: string;
begin
  Result.Text := '';
  Result.LastPartLength := 0;
  
  if VarIsNull(leafnbr) then
    Exit;

  l := DBDTLevel;
  entry := FindEntry( l, leafnbr );
  while l >= 0  do
  begin
    if entry.PrintOnCheck <> EXCLUDE_REPT_AND_ADDRESS_ON_CHECK then
      s := Trim(entry.CustomNumber)
    else
      s := '';
    if l = DBDTLevel then
      Result.LastPartLength := Length(s);
    Result.Text := s + Result.Text;
    entry := entry.Parent;
    dec(l);
  end
end;

end.

