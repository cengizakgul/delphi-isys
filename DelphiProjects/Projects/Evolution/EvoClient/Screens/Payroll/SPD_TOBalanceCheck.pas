//******************************************************************************
// FileName      : SPD_TOBalanceCheck
// Creation Date : 2/20/2007
// Project       : Payroll Shared Package
// Created By    : Andrey Solovyov
//
// Description:   Time Off Balance Check Feature
//
//******************************************************************************

unit SPD_TOBalanceCheck;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SDataStructure, EvUtils, DB, Wwdatsrc, ISBasicClasses, EvContext,
   kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, Grids, Wwdbigrd, Wwdbgrid, EvCommonInterfaces,
  StdCtrls, ExtCtrls, Buttons, SPackageEntry, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TInitFrom = (ifrSubmitRemote, ifrPreProcessRemote, ifrPreProcess);

  TTOBalanceCheck = class(TForm)
    cdToaBalanceCheckResults: TevClientDataSet;
    cdToaBalanceCheckResultsPR_CHECK_NBR: TIntegerField;
    cdToaBalanceCheckResultsEE_CUSTOM_NUMBER: TStringField;
    cdToaBalanceCheckResultsCHECK_NUMBER: TIntegerField;
    cdToaBalanceCheckResultsMESSAGE: TStringField;
    dsToaBalanceCheckResults: TevDataSource;
    gToaBalanceCheck: TevDBGrid;
    btnDontContinue: TevBitBtn;
    btnContinue: TevBitBtn;
    bvlFrame: TEvBevel;
    LabelHint: TevLabel;
    cdToaBalanceCheckResultsEE_NAME: TStringField;
    procedure gToaBalanceCheckDblClick(Sender: TObject);
  private
    FCheckNbr: integer;
  public
    function BalanceCheck: boolean;

    property CheckNbr: integer read FCheckNbr write FCheckNbr;
  end;

  function TOBalanceCheck(InitFrom: TInitFrom; AOwner: TComponent): boolean;

implementation

{$R *.dfm}

function TOBalanceCheck(InitFrom: TInitFrom; AOwner: TComponent): boolean;
var
  s: string;
begin
  Result := True;
  if DM_COMPANY.CO.Active and (DM_COMPANY.CO.CHECK_TIME_OFF_AVAIL.AsString = 'S') and
    (EvMessage('Do you want to run a time off balance check?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
  begin
    with TTOBalanceCheck.Create(AOwner) do
    try
      if BalanceCheck then
      begin
        Result := ShowModal = mrOk;
        if not Result and (CheckNbr > -1) then
        begin
          if DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger <> CheckNbr then
          begin
            DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
            DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', CheckNbr, []);
            DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
          end;
          if InitFrom = ifrPreProcess then
            (AOwner.Owner as TFramePackageTmpl).OpenFrame('TEDIT_PR_CHECK')
          else
            (AOwner.Owner as TFramePackageTmpl).OpenFrame('TREDIT_CHECK');
        end;
      end
      else begin
        s := 'No time off balance problems found.' + #13;
        if InitFrom = ifrSubmitRemote then
          s := s + 'Are you sure you want to submit this payroll?'
        else
          s := s + 'Are you sure you want to pre-process this payroll?';
        Result := EvMessage(s, mtConfirmation, [mbYes, mbNo]) = mrYes;
      end;
    finally
      Free;
    end;
  end;
end;

function TTOBalanceCheck.BalanceCheck: boolean;
var
  cl_person_nbr, ee_nbr, pr_check_nbr: integer;
  PayrollScr: IevPayrollScreens;
begin
  FCheckNbr := -1;
  cdToaBalanceCheckResults.CreateDataSet;
//  if not DM_PAYROLL.PR_CHECK.Active then
    DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR=' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
  DM_PAYROLL.PR_CHECK.SaveState();
  DM_PAYROLL.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
  cdToaBalanceCheckResults.SaveState();
  cdToaBalanceCheckResults.IndexFieldNames := 'PR_CHECK_NBR';
  DM_PAYROLL.PR_CHECK_LINES.DisableControls;
  ctx_StartWait;
  try
    if not DM_PAYROLL.PR_CHECK_LINES.Active then
      DM_PAYROLL.PR_CHECK_LINES.DataRequired('PR_NBR=' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);

    PayrollScr := Context.GetScreenPackage(IevPayrollScreens) as IevPayrollScreens;
    PayrollScr.ClearCheckToaSetupAccess;

    DM_PAYROLL.PR_CHECK_LINES.First;
    while not DM_PAYROLL.PR_CHECK_LINES.Eof do
    begin
      if not cdToaBalanceCheckResults.FindKey([DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_NBR.AsInteger]) then
      begin
        Assert(DM_PAYROLL.PR_CHECK.FindKey([DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_NBR.AsInteger]));
        if not PayrollScr.SilentCheckToaBalance then
        begin
          pr_check_nbr := DM_PAYROLL.PR_CHECK_LINES.PR_CHECK_NBR.AsInteger;
          ee_nbr := StrToIntDef(DM_PAYROLL.PR_CHECK.ShadowDataSet.CachedLookup(pr_check_nbr, 'EE_NBR'), 0);
          cl_person_nbr := StrToIntDef(DM_COMPANY.EE.ShadowDataSet.CachedLookup(ee_nbr, 'CL_PERSON_NBR'), 0);

          cdToaBalanceCheckResults.Append;
          cdToaBalanceCheckResultsPR_CHECK_NBR.AsInteger := pr_check_nbr;
          cdToaBalanceCheckResultsEE_CUSTOM_NUMBER.AsString := DM_COMPANY.EE.ShadowDataSet.CachedLookup(ee_nbr, 'CUSTOM_EMPLOYEE_NUMBER');
          cdToaBalanceCheckResultsCHECK_NUMBER.AsString := DM_PAYROLL.PR_CHECK.ShadowDataSet.CachedLookup(pr_check_nbr, 'PAYMENT_SERIAL_NUMBER');
          cdToaBalanceCheckResultsEE_NAME.AsString :=
            DM_COMPANY.CL_PERSON.ShadowDataSet.CachedLookup(cl_person_nbr, 'FIRST_NAME') + ' ' +
            DM_COMPANY.CL_PERSON.ShadowDataSet.CachedLookup(cl_person_nbr, 'LAST_NAME');
          cdToaBalanceCheckResults.Post;
        end;
      end;
      DM_PAYROLL.PR_CHECK_LINES.Next;
    end;
  finally
    ctx_EndWait;
    PayrollScr.ClearCheckToaSetupAccess;
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
    DM_PAYROLL.PR_CHECK.LoadState;
    cdToaBalanceCheckResults.LoadState;
  end;
  Result := cdToaBalanceCheckResults.RecordCount > 0;
end;

procedure TTOBalanceCheck.gToaBalanceCheckDblClick(Sender: TObject);
begin
  FCheckNbr := cdToaBalanceCheckResultsPR_CHECK_NBR.AsInteger;
  ModalResult := mrCancel;
end;

end.
