// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_SelectSL;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, 
  ISBasicClasses, EvUIComponents, LMDCustomButton, LMDButton, isUILMDButton;

type
  TSelectSLDlg = class(TForm)
    grdSelect: TevDBGrid;
    OKBtn: TevBitBtn;
    CancelBtn: TevBitBtn;
    lblTotal: TevLabel;
    procedure grdSelectDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TSelectSLDlg.grdSelectDblClick(Sender: TObject);
begin
  OKBtn.Click;
end;

end.
