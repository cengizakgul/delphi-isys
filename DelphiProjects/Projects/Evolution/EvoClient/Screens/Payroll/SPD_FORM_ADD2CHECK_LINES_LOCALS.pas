// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_FORM_ADD2CHECK_LINES_LOCALS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons,  SPD_ADD_RECORDS,
  SPD_ADD2CHECK_LINES_LOCALS, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton, ISBasicClasses;

type
  TFORM_ADD2CHECK_LINES_LOCALS = class(TForm)
    evBitBtn1: TevBitBtn;
    ADD2CHECK_LINES_LOCALS1: TADD2CHECK_LINES_LOCALS;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FORM_ADD2CHECK_LINES_LOCALS: TFORM_ADD2CHECK_LINES_LOCALS;

implementation

{$R *.DFM}

procedure TFORM_ADD2CHECK_LINES_LOCALS.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  FORM_ADD2CHECK_LINES_LOCALS := nil;
end;

{initialization
  RegisterClass(TFORM_ADD2CHECK_LINES_LOCALS);
 }
end.
