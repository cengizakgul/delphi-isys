// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sEDTotals;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, ISBasicClasses,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, ActnList, EvUIComponents, isUIFashionPanel;

type
  TEDTotals = class(TFrame)
    Splitter1: TevSplitter;
    dgEDTotal: TevDBGrid;
    dgEDDetail: TevDBGrid;
    Panel6: TevPanel;
    EDActionList: TevActionList;
    EDSeparateByLineType: TAction;
    EDSeparateByCheckType: TAction;
    cbEDSeparateByLineType: TevCheckBox;
    evCheckBox1: TevCheckBox;
    ScrollBox3: TScrollBox;
    evPanel6: TevPanel;
    pnlEDs: TisUIFashionPanel;
    pnlEDsBody: TevPanel;
    sbEDTotals: TScrollBox;
    pnlEDTotalsBorder: TevPanel;
    fpLeft: TisUIFashionPanel;
    pnlEDLeft: TevPanel;
    evSplitter1: TevSplitter;
    evPanel1: TevPanel;
    fpRight: TisUIFashionPanel;
    pnlFPLeftBody: TevPanel;
    pnlFPRightBody: TevPanel;
    procedure EDSeparateByLineTypeUpdate(Sender: TObject);
    procedure EDSeparateByLineTypeExecute(Sender: TObject);
    procedure EDSeparateByCheckTypeUpdate(Sender: TObject);
    procedure EDSeparateByCheckTypeExecute(Sender: TObject);
    procedure dgEDDetailDblClick(Sender: TObject);
    procedure dgEDDetailKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure pnlEDsResize(Sender: TObject);
    procedure fpLeftResize(Sender: TObject);
    procedure fpRightResize(Sender: TObject);
  public
    { Public declarations }
    procedure Activate;

  end;

implementation

uses sRemotePayrollUtils, SPayrollScr;

{$R *.dfm}

procedure TEDTotals.EDSeparateByLineTypeUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Checked := SeparateEDByLineType;
end;

procedure TEDTotals.EDSeparateByLineTypeExecute(Sender: TObject);
begin
  //note that Action has AutoCheck = true so Checked is already changed
  if SeparateEDByLineType <> (Sender as TCustomAction).Checked then
    ReCalculate_ED_TOTALS((Sender as TCustomAction).Checked, SeparateEDByCheckType);
end;

procedure TEDTotals.EDSeparateByCheckTypeUpdate(Sender: TObject);
begin
  (Sender as TCustomAction).Checked := SeparateEDByCheckType;
end;

procedure TEDTotals.EDSeparateByCheckTypeExecute(Sender: TObject);
begin
  //note that Action has AutoCheck = true so Checked is already changed
  if SeparateEDByCheckType <> (Sender as TCustomAction).Checked then
    ReCalculate_ED_TOTALS(SeparateEDByLineType, (Sender as TCustomAction).Checked);
end;

procedure TEDTotals.dgEDDetailDblClick(Sender: TObject);
begin
  GotoChecks(Self, (Sender as TevDBGrid).DataSource.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
end;

procedure TEDTotals.dgEDDetailKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    GotoChecks(Self, (Sender as TevDBGrid).DataSource.DataSet.FieldByName('PR_CHECK_NBR').AsInteger);
    Key := 0;
  end
end;

procedure TEDTotals.Activate;
begin
  dgEDTotal.DataSource := dsED_Total;
  dgEDDetail.DataSource := dsED_Detail;
end;

procedure TEDTotals.pnlEDsResize(Sender: TObject);
begin
  pnlEDsBody.Height := pnlEDs.Height - 65;
  pnlEDsBody.Width  := pnlEDs.Width - 40;
end;

procedure TEDTotals.fpLeftResize(Sender: TObject);
begin
  pnlFPLeftBody.Width  := fpLeft.Width - 40;
  pnlFPLeftBody.Height := fpLeft.Height - 65;
end;

procedure TEDTotals.fpRightResize(Sender: TObject);
begin
  pnlFPRightBody.Width  := fpRight.Width - 40;
  pnlFPRightBody.Height := fpRight.Height - 65;

end;

end.
