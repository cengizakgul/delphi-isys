inherited REDIT_CHECK: TREDIT_CHECK
  Width = 966
  Height = 749
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 88
    Width = 966
    Height = 606
    ActivePage = TabSheet1
    Align = alClient
    Constraints.MinHeight = 475
    Images = PageControlImages
    TabOrder = 0
    OnChange = PageControl1Change
    OnChanging = PageControl1Changing
    OnResize = PageControl1Resize
    object TabSheet1: TTabSheet
      Caption = 'Check Line&s'
      ImageIndex = 17
      object sbCheckLines: TScrollBox
        Left = 0
        Top = 0
        Width = 958
        Height = 577
        Align = alClient
        Anchors = []
        TabOrder = 0
        OnResize = sbCheckLinesResize
        object fplListofCheckLines: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 1034
          Height = 300
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          Constraints.MinHeight = 300
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Summary'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwDBGrid1: TevDBGrid
            Left = 12
            Top = 36
            Width = 1002
            Height = 244
            HelpContext = 1508
            DisableThemesInTitle = False
            ControlType.Strings = (
              'E_D_Code_Lookup;CustomEdit;ED_CL_E_DS_LookupCombo;F')
            Selected.Strings = (
              'E_D_Code_Lookup'#9'25'#9'E/D Code'#9'F'
              'E_D_Description_Lookup'#9'10'#9'E/D Description'#9'T'
              'HOURS_OR_PIECES'#9'17'#9'Hours/Pieces'#9'F'
              'RATE_OF_PAY'#9'17'#9'Rate of Pay'#9'F'
              'Amount'#9'17'#9'Amount'#9)
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TREDIT_CHECK\wwDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            OnCellChanged = wwDBGrid1CellChanged
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsDetail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgShowFooter, dgFooter3DCells, dgTrailingEllipsis, dgDblClickColSizing]
            PopupMenu = CheckLineGridPopup
            ReadOnly = False
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            TitleButtons = False
            OnCalcCellColors = wwDBGrid1CalcCellColors
            OnDrawDataCell = wwDBGrid1DrawDataCell
            OnDragDrop = wwDBGrid1DragDrop
            OnDragOver = wwDBGrid1DragOver
            OnKeyDown = wwDBGrid1KeyDown
            OnKeyPress = wwDBGrid1KeyPress
            OnKeyUp = wwDBGrid1KeyUp
            OnMouseDown = wwDBGrid1MouseDown
            OnColumnMoved = wwDBGrid1ColumnMoved
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clGrayText
            OnAfterDrawCell = wwDBGrid1AfterDrawCell
            Sorting = False
            NoFire = True
          end
          object ED_CL_E_DS_LookupCombo: TevDBLookupCombo
            Left = 48
            Top = 103
            Width = 281
            Height = 21
            HelpContext = 1508
            TabStop = False
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ED_Lookup'#9'20'#9'ED_Lookup'#9'F'
              'CodeDescription'#9'40'#9'CodeDescription'#9'F')
            DataField = 'CL_E_DS_NBR'
            DataSource = wwdsDetail
            LookupTable = DM_CO_E_D_CODES.CO_E_D_CODES
            LookupField = 'CL_E_DS_NBR'
            Style = csDropDownList
            TabOrder = 1
            Visible = False
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            ShowMatchText = True
            OnEnter = ED_CL_E_DS_LookupComboEnter
            OnExit = ED_CL_E_DS_LookupComboExit
            OnNotInList = ED_CL_E_DS_LookupComboNotInList
          end
        end
        object fpCheckLineDetail: TisUIFashionPanel
          Left = 8
          Top = 316
          Width = 785
          Height = 299
          Hint = '8'
          Alignment = taLeftJustify
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Check Line Detail'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object PageControl2: TevPageControl
            Left = 12
            Top = 36
            Width = 753
            Height = 243
            ActivePage = TabSheet5
            Align = alClient
            Images = PageControlImages
            TabOrder = 0
            object TabSheet5: TTabSheet
              HelpContext = 1508
              Caption = 'All Fields'
              ImageIndex = 14
              object sbAllFields: TScrollBox
                Left = 0
                Top = 0
                Width = 745
                Height = 214
                Align = alClient
                TabOrder = 0
                object SpeedButton1: TevSpeedButton
                  Left = 517
                  Top = 8
                  Width = 212
                  Height = 21
                  Hint = 'Choose DBDT (F4)'
                  Caption = 'Assign D/B/D/T'
                  HideHint = True
                  AutoSize = False
                  OnClick = SpeedButton1Click
                  NumGlyphs = 2
                  Glyph.Data = {
                    76010000424D7601000000000000760000002800000020000000100000000100
                    0400000000000001000000000000000000001000000010000000000000000000
                    80000080000000808000800000008000800080800000C0C0C000808080000000
                    FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                    8888DFFFFFFFFFF8888800000000008777788888888888FDDDD80FFFFFFFF087
                    77788888888888FDDDD80FFFFFFFF08888888888888888F88888000000000077
                    7778888F888888DDDDD8DD8A288777777778DDF8FDDDDDDDDDD8D8AAA2888888
                    8888DF888F8888888888DAAAAA2877777778D88888FDDDDDDDD8DDDA2D877777
                    7778DDD8FDDDDDDDDDD8DDDA2D2222222222DDD8FDDDDDDDDDDDDDDA222AAAAA
                    AAA2DDD8FFF88888888DDDDAAAAAAAAAAAA2DDD888888888888DDDDDDD222222
                    2222DDDDDDDDDDDDDDDDDDDDDD8777777778DDDDDD8DDDDDDDD8DDDDDD877777
                    7778DDDDDD8DDDDDDDD8DDDDDD8888888888DDDDDD8888888888}
                  ParentColor = False
                  ShortCut = 115
                end
                object YTDBtn: TevSpeedButton
                  Left = 288
                  Top = 151
                  Width = 132
                  Height = 21
                  Caption = 'YTD (F3)'
                  HideHint = True
                  AutoSize = False
                  OnClick = YTDBtnClick
                  NumGlyphs = 2
                  ParentColor = False
                  ShortCut = 114
                end
                object sbAddJob: TevSpeedButton
                  Left = 288
                  Top = 178
                  Width = 132
                  Height = 22
                  Hint = 'Add Job (Ctrl-J)'
                  Caption = 'Add Job'
                  HideHint = True
                  AutoSize = False
                  OnClick = sbAddJobClick
                  NumGlyphs = 2
                  ParentColor = False
                  ShortCut = 0
                end
                object Label5: TevLabel
                  Left = 457
                  Top = 39
                  Width = 37
                  Height = 13
                  Caption = 'Division'
                end
                object Label26: TevLabel
                  Left = 8
                  Top = 164
                  Width = 99
                  Height = 13
                  Caption = 'Line Item Begin Date'
                  FocusControl = dbtpDate
                end
                object Label25: TevLabel
                  Left = 148
                  Top = 47
                  Width = 27
                  Height = 13
                  Caption = 'Piece'
                  FocusControl = lkupPiece
                end
                object Label24: TevLabel
                  Left = 8
                  Top = 86
                  Width = 36
                  Height = 13
                  Caption = 'Agency'
                  FocusControl = lkupAgency
                end
                object Label23: TevLabel
                  Left = 627
                  Top = 164
                  Width = 70
                  Height = 13
                  Caption = 'Workers Comp'
                  FocusControl = lkupWorkComp
                end
                object Label22: TevLabel
                  Left = 288
                  Top = 47
                  Width = 18
                  Height = 13
                  Caption = 'SUI'
                  FocusControl = lkupSUI
                end
                object Label21: TevLabel
                  Left = 288
                  Top = 8
                  Width = 25
                  Height = 13
                  Caption = 'State'
                  FocusControl = lkupStates
                end
                object Label20: TevLabel
                  Left = 8
                  Top = 125
                  Width = 21
                  Height = 13
                  Caption = 'Shift'
                  FocusControl = lkupShift
                end
                object Label19: TevLabel
                  Left = 8
                  Top = 8
                  Width = 63
                  Height = 13
                  Caption = 'Rate Number'
                  FocusControl = edtRateNo
                end
                object Label18: TevLabel
                  Left = 148
                  Top = 8
                  Width = 69
                  Height = 13
                  Caption = 'Hours (Pieces)'
                  FocusControl = edtHours
                end
                object Label17: TevLabel
                  Left = 8
                  Top = 47
                  Width = 56
                  Height = 13
                  Caption = 'Rate of Pay'
                  FocusControl = edtRatePay
                end
                object Label16: TevLabel
                  Left = 457
                  Top = 164
                  Width = 17
                  Height = 13
                  Caption = 'Job'
                  FocusControl = lkupJobs
                end
                object Label15: TevLabel
                  Left = 457
                  Top = 117
                  Width = 27
                  Height = 13
                  Caption = 'Team'
                end
                object Label14: TevLabel
                  Left = 457
                  Top = 91
                  Width = 55
                  Height = 13
                  Caption = 'Department'
                end
                object Label12: TevLabel
                  Left = 457
                  Top = 65
                  Width = 34
                  Height = 13
                  Caption = 'Branch'
                end
                object evLabel1: TevLabel
                  Left = 148
                  Top = 164
                  Width = 91
                  Height = 13
                  Caption = 'Line Item End Date'
                  FocusControl = dbtpEndDate
                end
                object butnALD_or_Change: TevSpeedButton
                  Left = 517
                  Top = 140
                  Width = 212
                  Height = 21
                  Caption = 'Preview ALD (F6)'
                  HideHint = True
                  AutoSize = False
                  OnClick = butnALD_or_ChangeClick
                  NumGlyphs = 2
                  ParentColor = False
                  ShortCut = 117
                end
                object btnLocals: TevSpeedButton
                  Left = 288
                  Top = 125
                  Width = 132
                  Height = 21
                  HideHint = True
                  AutoSize = False
                  Action = ShowLocals
                  NumGlyphs = 2
                  ParentColor = False
                  ShortCut = 118
                end
                object bvVertLine: TEvBevel
                  Left = 439
                  Top = 23
                  Width = 3
                  Height = 177
                  Shape = bsLeftLine
                end
                object lkupWorkComp: TevDBLookupCombo
                  Left = 627
                  Top = 179
                  Width = 102
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'WORKERS_COMP_CODE'#9'5'#9'W/C Code'#9'F'
                    'DESCRIPTION'#9'40'#9'DESCRIPTION'#9'F'
                    'Co_State_Lookup'#9'2'#9'Co_State_Lookup'#9'F')
                  DataField = 'CO_WORKERS_COMP_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_WORKERS_COMP.CO_WORKERS_COMP
                  LookupField = 'CO_WORKERS_COMP_NBR'
                  Style = csDropDownList
                  TabOrder = 12
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                end
                object lkupTeam: TevDBLookupCombo
                  Left = 517
                  Top = 113
                  Width = 102
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'CUSTOM_TEAM_NUMBER'#9'20'#9'CUSTOM_TEAM_NUMBER'#9'F'
                    'NAME'#9'40'#9'NAME')
                  DataField = 'CO_TEAM_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_TEAM.CO_TEAM
                  LookupField = 'CO_TEAM_NBR'
                  Style = csDropDownList
                  DragMode = dmAutomatic
                  Enabled = False
                  TabOrder = 19
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnDragOver = lkupDivisionDragOver
                  OnDropDown = lkupDBDTDropDown
                end
                object lkupSUI: TevDBLookupCombo
                  Left = 288
                  Top = 62
                  Width = 132
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'State_Lookup'#9'2'#9'State_Lookup'#9'F')
                  DataField = 'EE_SUI_STATES_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_EE_STATES.EE_STATES
                  LookupField = 'EE_STATES_NBR'
                  Style = csDropDownList
                  TabOrder = 5
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnEnter = lkupSUIEnter
                  OnExit = lkupSUIExit
                end
                object lkupStates: TevDBLookupCombo
                  Left = 288
                  Top = 23
                  Width = 132
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'State_Lookup'#9'2'#9'State_Lookup')
                  DataField = 'EE_STATES_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_EE_STATES.EE_STATES
                  LookupField = 'EE_STATES_NBR'
                  Style = csDropDownList
                  TabOrder = 2
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnEnter = lkupStatesEnter
                  OnExit = lkupStatesExit
                end
                object lkupShift: TevDBLookupCombo
                  Left = 8
                  Top = 140
                  Width = 272
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'NameLKUP'#9'40'#9'NameLKUP'#9'F')
                  DataField = 'CO_SHIFTS_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_EE_WORK_SHIFTS.EE_WORK_SHIFTS
                  LookupField = 'CO_SHIFTS_NBR'
                  Style = csDropDownList
                  TabOrder = 8
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                end
                object lkupPiece: TevDBLookupCombo
                  Left = 148
                  Top = 62
                  Width = 132
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'NAME'#9'40'#9'NAME')
                  DataField = 'CL_PIECES_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CL_PIECES.CL_PIECES
                  LookupField = 'CL_PIECES_NBR'
                  Style = csDropDownList
                  TabOrder = 4
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                end
                object lkupJobs: TevDBLookupCombo
                  Left = 457
                  Top = 179
                  Width = 161
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'Description'#9'40'#9'Code'#9'F'
                    'TRUE_DESCRIPTION'#9'40'#9'Description'#9'F')
                  DataField = 'CO_JOBS_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_JOBS.CO_JOBS
                  LookupField = 'CO_JOBS_NBR'
                  Style = csDropDownList
                  DragMode = dmAutomatic
                  TabOrder = 11
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  ShowMatchText = True
                  OnDragOver = lkupDivisionDragOver
                  OnKeyDown = lkupJobsKeyDown
                end
                object lkupDivision: TevDBLookupCombo
                  Left = 517
                  Top = 35
                  Width = 102
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'CUSTOM_DIVISION_NUMBER'#9'20'#9'CUSTOM_DIVISION_NUMBER'#9'F'
                    'NAME'#9'40'#9'NAME')
                  DataField = 'CO_DIVISION_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_DIVISION.CO_DIVISION
                  LookupField = 'CO_DIVISION_NBR'
                  Style = csDropDownList
                  DragMode = dmAutomatic
                  Enabled = False
                  TabOrder = 13
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnDragOver = lkupDivisionDragOver
                  OnDropDown = lkupDBDTDropDown
                end
                object lkupDepartment: TevDBLookupCombo
                  Left = 517
                  Top = 87
                  Width = 102
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'CUSTOM_DEPARTMENT_NUMBER'#9'20'#9'CUSTOM_DEPARTMENT_NUMBER'#9'F'
                    'NAME'#9'40'#9'NAME')
                  DataField = 'CO_DEPARTMENT_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_DEPARTMENT.CO_DEPARTMENT
                  LookupField = 'CO_DEPARTMENT_NBR'
                  Style = csDropDownList
                  DragMode = dmAutomatic
                  Enabled = False
                  TabOrder = 17
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnDragOver = lkupDivisionDragOver
                  OnDropDown = lkupDBDTDropDown
                end
                object lkupBranch: TevDBLookupCombo
                  Left = 517
                  Top = 61
                  Width = 102
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'CUSTOM_BRANCH_NUMBER'#9'20'#9'CUSTOM_BRANCH_NUMBER'#9'F'
                    'NAME'#9'40'#9'NAME')
                  DataField = 'CO_BRANCH_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_BRANCH.CO_BRANCH
                  LookupField = 'CO_BRANCH_NBR'
                  Style = csDropDownList
                  DragMode = dmAutomatic
                  Enabled = False
                  TabOrder = 15
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnDragOver = lkupDivisionDragOver
                  OnDropDown = lkupDBDTDropDown
                end
                object lkupAgency: TevDBLookupCombo
                  Left = 8
                  Top = 101
                  Width = 272
                  Height = 21
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'Agency_Name'#9'40'#9'Agency_Name'#9'F')
                  DataField = 'CL_AGENCY_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CL_AGENCY.CL_AGENCY
                  LookupField = 'CL_AGENCY_NBR'
                  Style = csDropDownList
                  TabOrder = 6
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                end
                object edtRatePay: TevDBEdit
                  Left = 8
                  Top = 62
                  Width = 132
                  Height = 21
                  HelpContext = 1508
                  DataField = 'RATE_OF_PAY'
                  DataSource = DS_PR_CHECK_LINES
                  TabOrder = 3
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                  Glowing = False
                end
                object edtRateNo: TevDBEdit
                  Left = 8
                  Top = 23
                  Width = 132
                  Height = 21
                  HelpContext = 1508
                  DataField = 'RATE_NUMBER'
                  DataSource = DS_PR_CHECK_LINES
                  TabOrder = 0
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                  OnDragOver = lkupDivisionDragOver
                  Glowing = False
                end
                object edtHours: TevDBEdit
                  Left = 148
                  Top = 23
                  Width = 132
                  Height = 21
                  HelpContext = 1508
                  DataField = 'HOURS_OR_PIECES'
                  DataSource = DS_PR_CHECK_LINES
                  TabOrder = 1
                  UnboundDataType = wwDefault
                  WantReturns = False
                  WordWrap = False
                  Glowing = False
                end
                object dbtpEndDate: TevDBDateTimePicker
                  Left = 148
                  Top = 179
                  Width = 132
                  Height = 21
                  HelpContext = 1508
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  CalendarAttributes.PopupYearOptions.StartYear = 2000
                  DataField = 'LINE_ITEM_END_DATE'
                  DataSource = DS_PR_CHECK_LINES
                  Epoch = 1950
                  ShowButton = True
                  TabOrder = 10
                end
                object dbtpDate: TevDBDateTimePicker
                  Left = 8
                  Top = 179
                  Width = 132
                  Height = 21
                  HelpContext = 1508
                  CalendarAttributes.Font.Charset = DEFAULT_CHARSET
                  CalendarAttributes.Font.Color = clWindowText
                  CalendarAttributes.Font.Height = -11
                  CalendarAttributes.Font.Name = 'MS Sans Serif'
                  CalendarAttributes.Font.Style = []
                  CalendarAttributes.PopupYearOptions.StartYear = 2000
                  DataField = 'LINE_ITEM_DATE'
                  DataSource = DS_PR_CHECK_LINES
                  Epoch = 1950
                  ShowButton = True
                  TabOrder = 9
                end
                object evDBLookupCombo1: TevDBLookupCombo
                  Left = 627
                  Top = 35
                  Width = 102
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'CUSTOM_DIVISION_NUMBER'#9'40'#9'CUSTOM_DIVISION_NUMBER'#9'F'
                    'NAME'#9'40'#9'NAME')
                  DataField = 'CO_DIVISION_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_DIVISION.CO_DIVISION
                  LookupField = 'CO_DIVISION_NBR'
                  Style = csDropDownList
                  DragMode = dmAutomatic
                  Enabled = False
                  TabOrder = 14
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnDragOver = lkupDivisionDragOver
                  OnDropDown = lkupDBDTDropDown
                end
                object evDBLookupCombo2: TevDBLookupCombo
                  Left = 627
                  Top = 61
                  Width = 102
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'CUSTOM_BRANCH_NUMBER'#9'20'#9'CUSTOM_BRANCH_NUMBER'#9'F'
                    'NAME'#9'40'#9'NAME')
                  DataField = 'CO_BRANCH_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_BRANCH.CO_BRANCH
                  LookupField = 'CO_BRANCH_NBR'
                  Style = csDropDownList
                  DragMode = dmAutomatic
                  Enabled = False
                  TabOrder = 16
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnDragOver = lkupDivisionDragOver
                  OnDropDown = lkupDBDTDropDown
                end
                object evDBLookupCombo3: TevDBLookupCombo
                  Left = 627
                  Top = 87
                  Width = 102
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'CUSTOM_DEPARTMENT_NUMBER'#9'20'#9'CUSTOM_DEPARTMENT_NUMBER'#9'F'
                    'NAME'#9'40'#9'NAME')
                  DataField = 'CO_DEPARTMENT_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_DEPARTMENT.CO_DEPARTMENT
                  LookupField = 'CO_DEPARTMENT_NBR'
                  Style = csDropDownList
                  DragMode = dmAutomatic
                  Enabled = False
                  TabOrder = 18
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnDragOver = lkupDivisionDragOver
                  OnDropDown = lkupDBDTDropDown
                end
                object evDBLookupCombo4: TevDBLookupCombo
                  Left = 627
                  Top = 113
                  Width = 102
                  Height = 21
                  HelpContext = 1508
                  DropDownAlignment = taLeftJustify
                  Selected.Strings = (
                    'CUSTOM_TEAM_NUMBER'#9'20'#9'CUSTOM_TEAM_NUMBER'#9'F'
                    'NAME'#9'40'#9'NAME')
                  DataField = 'CO_TEAM_NBR'
                  DataSource = DS_PR_CHECK_LINES
                  LookupTable = DM_CO_TEAM.CO_TEAM
                  LookupField = 'CO_TEAM_NBR'
                  Style = csDropDownList
                  DragMode = dmAutomatic
                  Enabled = False
                  TabOrder = 20
                  AutoDropDown = True
                  ShowButton = True
                  PreciseEditRegion = False
                  AllowClearKey = False
                  OnDragOver = lkupDivisionDragOver
                  OnDropDown = lkupDBDTDropDown
                end
                object WorkAtHome: TevDBRadioGroup
                  Left = 288
                  Top = 86
                  Width = 132
                  Height = 36
                  HelpContext = 10511
                  Caption = ' Work Address Override'
                  Columns = 2
                  DataField = 'WORK_ADDRESS_OVR'
                  DataSource = DS_PR_CHECK_LINES
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  Items.Strings = (
                    'Yes'
                    'No')
                  ParentFont = False
                  TabOrder = 7
                  Values.Strings = (
                    'Y'
                    'N')
                end
              end
            end
            object lsCLLocals: TTabSheet
              Caption = 'Check Line Local Overrides'
              ImageIndex = 26
              object evDBGrid1: TevDBGrid
                Left = 0
                Top = 0
                Width = 745
                Height = 214
                HelpContext = 1511
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'Local_Name_Lookup;CustomEdit;lcCLLocalName'
                  'EXEMPT_EXCLUDE;CustomEdit;cbCLOverride')
                Selected.Strings = (
                  'Local_Name_Lookup'#9'96'#9'Local Name'#9'F'
                  'EXEMPT_EXCLUDE'#9'20'#9'Exclude'#9'F')
                IniAttributes.Enabled = False
                IniAttributes.SaveToRegistry = False
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TREDIT_CHECK\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = DS_PR_CHECK_LINE_LOCALS
                Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
                ReadOnly = False
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clMoneyGreen
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object cbCLOverride: TevDBComboBox
                Left = 264
                Top = 91
                Width = 121
                Height = 21
                TabStop = False
                ShowButton = True
                Style = csDropDownList
                MapList = True
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'EXEMPT_EXCLUDE'
                DataSource = DS_PR_CHECK_LINE_LOCALS
                DropDownCount = 8
                ItemHeight = 0
                Items.Strings = (
                  'Exempt'#9'E'
                  'Exclude'#9'X'
                  'Include'#9'I')
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 1
                UnboundDataType = wwDefault
                OnChange = cbCLOverrideChange
                OnDropDown = cbCLOverrideDropDown
              end
              object lcCLLocalName: TevDBLookupCombo
                Left = 128
                Top = 70
                Width = 97
                Height = 21
                TabStop = False
                DropDownAlignment = taLeftJustify
                Selected.Strings = (
                  'Local_Name_Lookup'#9'20'#9'Name'#9'F')
                DataField = 'EE_LOCALS_NBR'
                DataSource = DS_PR_CHECK_LINE_LOCALS
                LookupTable = DM_EE_LOCALS.EE_LOCALS
                LookupField = 'EE_LOCALS_NBR'
                Style = csDropDownList
                TabOrder = 2
                AutoDropDown = True
                ShowButton = True
                PreciseEditRegion = False
                AllowClearKey = False
                OnChange = cbCLOverrideChange
              end
            end
          end
        end
      end
    end
    object ManualTaxTabSheet: TTabSheet
      Caption = 'Man&ual Tax'
      ImageIndex = 26
      object sbManualTax: TScrollBox
        Left = 0
        Top = 0
        Width = 958
        Height = 577
        Align = alClient
        TabOrder = 0
        object fpManualTax: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 415
          Height = 347
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Manual Taxes'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object wwDBGrid4: TevDBGrid
            Left = 12
            Top = 35
            Width = 379
            Height = 260
            HelpContext = 1509
            DisableThemesInTitle = False
            Selected.Strings = (
              'RowNbr'#9'5'#9'Row'#9'T'
              'Label'#9'40'#9'Description'#9'T'
              'Amount'#9'10'#9'Amount'#9)
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TREDIT_CHECK\wwDBGrid4'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = ManualTaxSrc
            ReadOnly = False
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clWhite
            NoFire = False
          end
          object btnAddStates: TevBitBtn
            Left = 214
            Top = 302
            Width = 89
            Height = 25
            Caption = 'Add States'
            TabOrder = 1
            OnClick = btnAddStatesClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCC
              CCCCCCCCE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCCCCCCCCCCE1E1E1FFFFFFCFCFCFCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC56B38F009E5F009D
              5D009E5E53B28CE1E1E1CFCFCFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCA9A8A88F8F8F8D8D8D8E8E8EA6A6A6E1E1E1C69749C48F38
              C48D34C48D34C48D34C58C33C48C33C78D34D48E3657984F00A66A00BA8677DF
              C400BA8600A66A53B28C8F8F8F86868685848485848485848485848485848486
              8585878787898989979696ABABABD6D6D6ABABAB979696A6A6A6C48E38FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF009A5600BF8B00BB82FFFF
              FF00BB8200C08C009E5E868686FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF8A8A8AB1B0B0ABABABFFFFFFABABABB2B1B18E8E8EC48D34FFFFFF
              27A98B849E990DA78203D9A511E0AF18C29B98A7A800995374E5CBFFFFFFFFFF
              FFFFFFFF77E5CC009C5C858484FFFFFF9F9F9E9C9C9C9B9B9BCAC9C9D1D1D1B6
              B6B5A7A7A7898989DCDBDBFFFFFFFFFFFFFFFFFFDCDCDC8C8C8CC58D34FFFFFF
              00A17154938397848838A98E1BBF96848C8A759E9600985200CB9400C78EFFFF
              FF00C88F00CC98009D5D858484FFFFFF9493938D8D8D868686A09F9FB2B2B28B
              8B8B9B9B9B888888BCBCBBB8B7B7FFFFFFB8B8B8BDBCBC8D8D8DC58D34FFFFFF
              00C08A00CE9B37A589B09DA1AE9DA06E9A8F35F6C845C19E00A96800D29B73EC
              D300D39E00AF7368C6A2858484FFFFFFB1B0B0BFBFBF9C9C9CA09F9FA09F9F97
              9696E8E7E7B6B6B5999999C3C2C2E3E3E2C4C3C3A09F9FBBBBBBC58D35FFFFFF
              00C08B07CD9A0DB78817E4A948D4AD28C39B58F1D4E3F6FF49B2950096520097
              55009B5C5FC299FFFFFF868585FFFFFFB2B1B1BEBEBEAAA9A9D3D3D3C8C8C7B7
              B6B6E6E6E6F8F8F8A9A8A88686868787878C8C8CB6B6B5FFFFFFC58D35FFFFFF
              00A07307AE8113B28C2334D71EAD9E27C69D9CC8DCB1CFE7C0D5E8E5B363FFFF
              FFD78E36FFFFFFFFFFFF868585FFFFFF949393A1A0A0A6A6A6757474A6A6A6B9
              B9B9CCCCCCD4D4D4D9D9D9AAAAAAFFFFFF878787FFFFFFFFFFFFC58D35FFFFFF
              00C18B08D0971F55C12B44FB20E9A979C4CCC2EEFFCBF3FFD0B36CFECC60FFFF
              FFC58D37FFFFFFFFFFFF868585FFFFFFB2B2B2C0C0C07878778B8B8BD7D7D7C4
              C3C3F1F1F1F5F5F5AAA9A9BFBFBFFFFFFF868585FFFFFFFFFFFFC58D36FFFFFA
              00C28A08D19713A39018D5A97BE5EA9CC5E9C8E9F4FAD277D4AD56FACC65FFFF
              FEC18D37FFFFFFFFFFFF868585FFFFFFB2B2B2C1C1C19B9B9BC7C7C7E3E3E2CD
              CCCCEBEBEAC7C7C7A2A2A2C0C0C0FFFFFF858484FFFFFFFFFFFFC58E36FFFFF5
              00A76F1273A70EAF8846BCB092C0EBAABEBCD6AE5BD5AE59D4AF5CD1AB55FFFD
              F9C18D37FFFFFFFFFFFF868585FEFEFE9898987F7E7EA3A3A3B6B6B5CAC9C9BE
              BDBDA4A4A4A4A4A4A5A5A5A1A0A0FEFDFD858484FFFFFFFFFFFFC58E36FFFEEE
              00B1962C0FFF13BB8899DEFFC1DCDDDAAD4FFCCF69FACF6DD2AE5BF9CD66FFFA
              F4C18D37FFFFFFFFFFFF868585FCFCFBA7A7A7838383ADADADE4E3E3DCDCDCA2
              A2A2C3C2C2C3C2C2A3A3A3C1C1C1FAFAFA858484FFFFFFFFFFFFC48E37FFFBE9
              00A09E0563DA23B5A493DBFFFCCD64D2AA50F8CC66F7CC67CFAB54F7CB61FFF8
              EEC18E38FFFFFFFFFFFF868585F9F9F89C9C9C888888ADADADE2E2E2C1C1C1A0
              9F9FC0C0C0C0C0C0A1A0A0BEBEBEF8F8F8868585FFFFFFFFFFFFC4903AFFFAEB
              FFF9E3FFFAE1FFF8E1FFF5E1FFF5E6FFF6EAFFF6EAFFF6EAFFF6EAFFF6E9FFF9
              EDC3903BFFFFFFFFFFFF878787F9F9F8F7F6F6F7F6F6F5F5F5F3F2F2F4F4F4F6
              F6F5F6F6F5F6F6F5F6F6F5F5F5F5F8F8F8878787FFFFFFFFFFFFC99B4DC4903A
              C48F37C48E37C48E37C38E37C28E37C28E38C18E38C18E38C18E38C28E38C390
              3BCA9C4FFFFFFFFFFFFF93939387878786868686858586858586858586858586
              8585868585868585868585868585878787949393FFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
          object btnAddLocals: TevBitBtn
            Left = 304
            Top = 302
            Width = 89
            Height = 25
            Caption = 'Add Locals'
            TabOrder = 2
            OnClick = btnAddLocalsClick
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCC
              CCCCCCCCE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFE1E1E1CCCCCCCCCCCCCCCCCCE1E1E1FFFFFFCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC56B48E009E5E009D
              5D009E5E53B28CE1E1E1CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
              CCCCCCCCCCCCCCCCA8A8A78E8E8E8D8D8D8E8E8EA6A6A6E1E1E12E92722E9171
              2E90702B906F1F906EA6836BA3866AA7876AAF866D47946400A76800BA8677DF
              C400BA8600A66A53B28C88888887878786868686868686858582818182828285
              8484858484888888979797ABABABD6D6D6ABABAB979696A6A6A62D84632BB281
              29B38323A879157955FFFBEB217DC53E8DCF3481CC00A15600C18B00BB82FFFF
              FF00BB8200C08C009E5E7B7B7BA4A4A4A6A6A69B9B9B6E6E6EFAF9F98F8F8F9D
              9D9D949393909090B2B2B2ABABABFFFFFFABABABB2B1B18E8E8EEBF3F051785D
              188B5D61977BD9D6C6FFF3E00669B3478ECA1C6EBD00A05376E6CBFFFFFFFFFF
              FFFFFFFF77E5CC009C5CF3F2F27170707F7E7E8F8F8FD4D4D4F1F1F17D7C7C9C
              9C9C8383838F8F8FDCDCDCFFFFFFFFFFFFFFFFFFDCDCDC8C8C8CFFFFFFAC8875
              FFF8EFFFEBDEFDEBDCFFF2DE0057AB408DCF005BB300A15500CC9600C88FFFFF
              FF00C88F00CC98009D5DFFFFFF878787F8F8F8EBEBEAEAEAEAF0F0F06F6F6F9D
              9D9D757474909090BDBCBCB8B8B8FFFFFFB8B8B8BDBCBC8D8D8DFFFFFFA18973
              FEF4EBCAB7A5CDBAA8978066A3876AA8896BAF886D3F936100AD6D00D29C73EC
              D300D39D00AE7268C6A1FFFFFF868686F4F4F4B5B5B5B8B8B87D7C7C83838386
              85858686868686869D9D9DC3C2C2E3E3E2C4C3C39F9F9EBBBBBBFFFFFF9F8B75
              FDF4EBF2E4D5CAB9A7F8EBDCD0BEABFCEDDED5BEACFFECE152A97C0098520097
              52009B586FC9A4FFFFFFFFFFFF888888F4F4F4E3E3E2B8B7B7EAEAEABCBCBBEC
              ECECBDBCBCECECEC9D9D9D8888888787878B8B8BBEBEBEFFFFFFFFFFFFA08D77
              FCF5EDC7B6A2C9B9A5CABAA6CABAA7CABAA6CBBAA7CEBAA8D6BBAADBBAAAFFF9
              F5BC8C7DFFFFFFFFFFFFFFFFFF8A8A8AF5F5F5B4B4B4B7B6B6B8B7B7B8B8B8B8
              B7B7B8B8B8B8B8B8BABABABABABAFAF9F98D8D8DFFFFFFFFFFFFFFFFFFA49079
              FDF6EFF0E0D0C9B7A4F2E4D4C9B8A5F2E4D4C9B8A5F3E4D5CAB8A5F3E1D2FFF7
              F1AB907AFFFFFFFFFFFFFFFFFF8D8D8DF7F6F6DFDFDEB5B5B5E3E3E2B6B6B5E3
              E3E2B6B6B5E3E3E2B6B6B5E0E0E0F7F7F78E8E8EFFFFFFFFFFFFFFFFFFA6927B
              FDF7F2C7B49FC9B7A3C9B7A4CAB7A4C9B7A4CAB7A4C9B7A4C9B7A3C7B49FFDF7
              F2A7927BFFFFFFFFFFFFFFFFFF8F8F8FF8F8F8B2B1B1B5B5B5B5B5B5B5B5B5B5
              B5B5B5B5B5B5B5B5B5B5B5B2B1B1F8F8F88F8F8FFFFFFFFFFFFFFFFFFFA8947E
              FEFAF4ECDBCBC8B7A3EFDFD0C8B7A4EFDFD0C8B7A4EFDFD0C8B7A3ECDBCBFEFA
              F4A8947EFFFFFFFFFFFFFFFFFF929292FAFAFADADADAB5B5B5DEDEDEB5B5B5DE
              DEDEB5B5B5DEDEDEB5B5B5DADADAFAFAFA929292FFFFFFFFFFFFFFFFFFAA9680
              FFFCF8C4B19CC6B5A0C7B6A1C7B6A1C7B6A1C7B6A1C7B6A1C6B5A0C4B19CFFFC
              F8AA9680FFFFFFFFFFFFFFFFFF949393FDFDFCAFAFAFB2B2B2B3B3B3B3B3B3B3
              B3B3B3B3B3B3B3B3B2B2B2AFAFAFFDFDFC949393FFFFFFFFFFFFFFFFFFAD9983
              FFFEFBE7D4C1C3B09CE9D7C4C4B19DE9D7C4C4B19DE9D7C4C3B09CE7D4C1FFFE
              FBAD9983FFFFFFFFFFFFFFFFFF979696FFFFFFD3D2D2AEAEAED5D5D5AFAFAFD5
              D5D5AFAFAFD5D5D5AEAEAED3D2D2FFFFFF979696FFFFFFFFFFFFFFFFFFB09D87
              FFFFFCFFFCF8FFFCF9FFFDFAFFFDFAFFFDFAFFFDFAFFFDFAFFFCF9FFFCF8FFFF
              FCB09D87FFFFFFFFFFFFFFFFFF9A9A9AFFFFFFFDFDFCFDFDFCFEFEFEFEFEFEFE
              FEFEFEFEFEFEFEFEFDFDFCFDFDFCFFFFFF9A9A9AFFFFFFFFFFFFFFFFFFCCC0B1
              B29F8AB19E88B19E87B19E87B19E87B19E87B19E87B19E87B19E87B19E88B29F
              8ACCC0B1FFFFFFFFFFFFFFFFFFBEBDBD9D9D9D9B9B9B9B9B9B9B9B9B9B9B9B9B
              9B9B9B9B9B9B9B9B9B9B9B9B9B9B9D9D9DBEBDBDFFFFFFFFFFFF}
            NumGlyphs = 2
            Margin = 0
          end
        end
        object pnlOptions: TisUIFashionPanel
          Left = 431
          Top = 8
          Width = 201
          Height = 347
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'pnlOptions'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Options'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object evDBRadioGroup1: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 169
            Height = 41
            HelpContext = 1510
            Caption = '~Calculate Override Taxes'
            Columns = 2
            DataField = 'CALCULATE_OVERRIDE_TAXES'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object drgpReciprocate_SUI: TevDBRadioGroup
            Left = 12
            Top = 84
            Width = 169
            Height = 41
            HelpContext = 5007
            Caption = '~Reciprocate SUI'
            Columns = 2
            DataField = 'RECIPROCATE_SUI'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object butnProrateFICA: TevBitBtn
            Left = 12
            Top = 302
            Width = 169
            Height = 25
            Caption = 'Prorate FICA'
            TabOrder = 3
            OnClick = butnProrateFICAClick
            Color = clBlack
            NumGlyphs = 2
            Margin = 0
          end
          object evDBRadioGroup3: TevDBRadioGroup
            Left = 12
            Top = 133
            Width = 169
            Height = 41
            HelpContext = 5007
            Caption = '~Disable Shortfalls'
            Columns = 2
            DataField = 'DISABLE_SHORTFALLS'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = '&General'
      ImageIndex = 2
      object sbGeneral: TScrollBox
        Left = 0
        Top = 0
        Width = 958
        Height = 577
        Align = alClient
        TabOrder = 0
        object pnlGeneralSettings: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 435
          Height = 337
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'General Settings'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label8: TevLabel
            Left = 12
            Top = 230
            Width = 120
            Height = 13
            Caption = 'Current Check Comments'
          end
          object lblCheckType945: TevLabel
            Left = 204
            Top = 191
            Width = 61
            Height = 16
            Caption = '~945 Check'
            FocusControl = cbCheckType945
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBRadioGroup2: TevDBRadioGroup
            Left = 12
            Top = 35
            Width = 184
            Height = 36
            HelpContext = 1510
            Caption = '~Block DD'
            Columns = 2
            DataField = 'EXCLUDE_DD'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 0
            Values.Strings = (
              'Y'
              'N')
          end
          object DBRadioGroup3: TevDBRadioGroup
            Left = 12
            Top = 74
            Width = 184
            Height = 36
            HelpContext = 1510
            Caption = '~Block DD Except Net'
            Columns = 2
            DataField = 'EXCLUDE_DD_EXCEPT_NET'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 1
            Values.Strings = (
              'Y'
              'N')
          end
          object DBRadioGroup4: TevDBRadioGroup
            Left = 12
            Top = 152
            Width = 184
            Height = 76
            HelpContext = 1510
            Caption = '~Block Time Off Accrual'
            DataField = 'EXCLUDE_TIME_OFF_ACCURAL'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 3
            Values.Strings = (
              'Y'
              'N')
          end
          object DBRadioGroup5: TevDBRadioGroup
            Left = 12
            Top = 113
            Width = 184
            Height = 36
            HelpContext = 1510
            Caption = '~Block Auto Distribution'
            Columns = 2
            DataField = 'EXCLUDE_AUTO_DISTRIBUTION'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
          object DBRadioGroup7: TevDBRadioGroup
            Left = 204
            Top = 74
            Width = 210
            Height = 36
            HelpContext = 1510
            Caption = '~Block Sched E/Ds Except Pension'
            Columns = 2
            DataField = 'EXCLUDE_SCH_E_D_EXCEPT_PENSION'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 5
            Values.Strings = (
              'Y'
              'N')
          end
          object DBRadioGroup6: TevDBRadioGroup
            Left = 204
            Top = 35
            Width = 210
            Height = 36
            HelpContext = 1510
            Caption = '~Block Sched E/Ds Except DD'
            Columns = 2
            DataField = 'EXCLUDE_ALL_SCHED_E_D_CODES'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 4
            Values.Strings = (
              'Y'
              'N')
          end
          object DBRadioGroup1: TevDBRadioGroup
            Left = 204
            Top = 113
            Width = 210
            Height = 36
            HelpContext = 1510
            Caption = '~Block Scheduled E/Ds From Agency'
            Columns = 2
            DataField = 'EXCLUDE_SCH_E_D_FROM_AGCY_CHK'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 6
            Values.Strings = (
              'Y'
              'N')
          end
          object DBMemo2: TEvDBMemo
            Left = 12
            Top = 245
            Width = 402
            Height = 70
            DataField = 'NOTES_NBR'
            DataSource = DS_PR_CHECK
            TabOrder = 9
          end
          object cbCheckType945: TevDBComboBox
            Left = 204
            Top = 206
            Width = 210
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CHECK_TYPE_945'
            DataSource = DS_PR_CHECK
            DropDownCount = 8
            ItemHeight = 13
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 8
            UnboundDataType = wwDefault
          end
          object evDBRadioGroup2: TevDBRadioGroup
            Left = 204
            Top = 152
            Width = 210
            Height = 36
            HelpContext = 1510
            Caption = '~Update Scheduled E/D Balance'
            Columns = 2
            DataField = 'UPDATE_BALANCE'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 7
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object fpActions: TisUIFashionPanel
          Left = 451
          Top = 8
          Width = 202
          Height = 337
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Actions'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Button2: TevBitBtn
            Left = 12
            Top = 36
            Width = 169
            Height = 25
            Caption = 'Create New Check'
            TabOrder = 0
            OnClick = Button2Click
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3EFE958B58E099C6123A2
              6FA1D2BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFEEEDEDAAAAAA8C8C8C959595CCCCCCFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF3EE3FB38700BC8139D6A915CB
              960DAE769DD2BBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFF2F2F2A7A7A7ACACACC9C9C9BDBDBD9F9F9FCBCBCBFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF88CBAF00BE8600D08FCBF6E845DD
              AF00CC8D1AAD7AE5F1EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFC3C3C3AEAEAEBEBEBDF2F2F2D1D1D0BABABA9F9F9FF0F0F0FFFEFEFDFCFB
              FDFBFBFDFBFBFDFBFBFDFBFBFDFBFBFEFBFB5DBD9647DAB4C7F3E5F3FCF9D7F6
              EC94EDD400AD73C6E3D4FFFFFFFCFCFCFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFC
              FCFCB3B2B2CFCECEEFEFEFFBFBFBF3F3F3E6E6E69D9D9DE0DFDFE6E9EABDC8CE
              B6C2C9B4C1C7B4C1C6B4C1C6B4C1C6B6C0C75DB19335D6AD45D2A9DDF7EF7DE0
              C435D4AB02A973AFCCC2F2EEF0C8C8C8C3C3C3C2C2C1C2C2C1C2C2C1C2C2C1C2
              C2C1A9A8A8C9C9C8C6C7C7F5F5F5D9D9D9C8C8C89A9A9ACACACAC5D4DCADD4DB
              AFD9D7C1E4EFBFE3EFBFE3EFBFE3EFC3E5EF9ED5CD52CEA90FCC9F92E6D131D1
              AB00BC8847B891B2CFDADCDBDBD4D4D4D8D8D7E6E6E6E5E6E5E5E6E5E5E6E5E7
              E7E7D3D3D2C4C3C3BEBFBEDFDFDFC6C6C6ADACACAEAEAED0D0D0C7D6E0A7D2CD
              7FBD9DD3F0FDC3E1ECC4E1EDC4E1EDC4E2EDC3E0EB8BCABC68D0B145C9A133C0
              954FBA97BCE6E8B5D3E1DDDCDCD0CFCFB5B4B4F4F3F3E3E2E2E4E3E3E4E3E3E4
              E3E3E2E1E1C5C5C5C7C7C6BEBDBDB4B3B3B2B1B1E6E6E6D4D4D4C7D6E0A5CFCC
              7BB89BCAEAFCBFE1EFBFE1EFBFE1EFBFE1EFC0E2F0C4E4F3ABD6DA92C9C49ACD
              CBC0E2EECCEDFDB1D0DEDDDCDCCDCDCDB0B0B0EEEEEEE4E3E3E4E4E4E4E4E4E4
              E4E4E5E4E4E7E7E7D6D6D6C8C7C7CCCCCCE4E4E3F0F0F0D2D2D2C7D7E1A4CDCA
              75B296C2E6FAB9DFEEB9DEEEB9DFEEB8DCEBB8DCECBADFEFBCE0F1BFE1F3BEE1
              F2B9DFF0C3E8F6B2D0DDDDDCDCCBCBCBABABAAEBEAEAE2E2E2E1E1E1E2E1E1DF
              DFDFDFDFDFE3E2E2E3E3E3E5E5E5E4E4E4E2E2E2EAEAEAD2D2D2C7D7E1A4CCC8
              70AE91B7DBF0A4C2D0AACBDBA8C8D7B3DBECB2D8EAA4C3D0AACBDBA9CAD8A8C9
              D7AACDDDBFE4F4B3D1DDDDDDDCCACACAA6A6A6DFDFDFC5C5C5CFCFCECBCBCBDE
              DEDEDCDCDBC5C5C5CECECECCCCCCCCCCCBD0D0D0E7E7E7D2D2D2C8D7E1A3CAC5
              66A688AAD2E88EABB798B8C89DC1D2A8D4E7A4CDE08FABB898B8C89EC2D49CC1
              D29EC6D8B7DFF1B5D1DDDDDDDCC8C8C79E9E9ED5D5D6ADADADBCBBBBC5C5C5D7
              D7D7D1D1D0ADADADBBBBBBC6C6C6C4C4C4C9C9C9E2E2E2D2D2D2C5D6E0ACCFCE
              82B69FB6DCF0ADD0E1AFD3E4B5DEF1B3DBEEB2D8EAADD0E1AFD3E4B5DEF1B5DD
              F0B3DCEEBFE1F1B4D1DDDDDCDCCECDCDB0B0B0E0E0E0D3D3D3D7D6D6E2E2E2DF
              DFDFDCDCDCD3D3D3D7D6D6E2E1E1E1E1E1DFDFDFE4E4E4D2D2D2DBE8EDB6D3E0
              BED9E6BAD6E2BBD8E3BBD7E3BAD6E1BAD6E1BAD6E2BBD8E3BBD7E3BAD6E1BAD6
              E1BAD6E1B8D5E1C0D9E4EFEDEDD4D4D5DCDCDCD8D8D8DADADAD9D9D9D8D8D8D8
              D8D8D8D8D8DADADAD9D9D9D8D8D8D8D8D8D8D8D8D7D7D7DADADAFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
          object bManualCheck: TevBitBtn
            Left = 12
            Top = 185
            Width = 169
            Height = 25
            Action = aCreateManualCheck
            TabOrder = 5
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEEEE5097BD5FC9
              F74DC3F55699BCFEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFE8E8E8B6B6B6CECECDC6C6C6BEBEBEFCFCFCFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F76C96AE64C5F25AC2
              EF4ABDEE45AADAEDF4F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFF3F3F3BEBEBECAC9C9CACAC9C4C4C4BCBBBBF0F0F0FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2BDCD6DC9F568C7F154BA
              E946B5E833A9E1DDEFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFD5D5D5C7C7C7CECDCDC2C2C2BEBEBEB5B5B5EAE9E9FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7CBEE155B0E155B6E554B8
              E747B6E936AEE5D8E9F2FEFEFEFCFCFCFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFB
              FBFBFBFBFBC9C9C9B5B4B4BFBEBEC2C1C1C1C0C0B7B7B7E8E8E8EBEBEBC3C5C6
              BCBEBFBBBDBEBCBDBEBCBDBEBCBDBEBCBEBFC1C3C46E9AB24D9ECD51B4E34DB5
              E642B1E436AEE7AEB8BDEAE9E9C9C9C9C3C3C3C2C2C1C2C2C1C2C2C1C2C2C1C2
              C2C1C2C2C2B3B3B3A8A8A8BCBCBCBDBDBDBAB9B9B2B2B2D1D1D1B3B9BBA4CAD4
              ADD2D8B0D5E1B1D6E2B1D6E2B1D6E2B1D6E2B2D6E2B4D9E491C7DF47AFE245AF
              E13BACE153BBEB86A4B2D6D6D6D5D4D4D8D8D7E6E6E6E5E6E5E5E6E5E5E6E5E5
              E6E5E5E6E5E9E9E9D5D5D4B8B8B8B9B9B9B7B7B7C1C1C1C8C8C8ABB2B6ACD6D1
              89C4A6D7F4FFC9E5EFC9E6F0C9E6F0C9E6F0C9E6F0CDE8EFA4D1E74CAEDF48B2
              E440ACE099D5F399AEB7D8D8D8D0D0D0B5B4B4F4F3F3E3E2E2E4E3E3E4E3E3E4
              E3E3E4E3E3E5E5E4D7D6D6B8B8B8BDBDBDB4B4B4D9D9D9D5D5D5ACB2B6A5D0CC
              7BB89CCCECFDC3E5F3C3E5F3C3E5F3C3E5F3C3E5F3C4E6F3BFE3F27CBFE34FAC
              DB6BB7DFC5ECFD97AEB7D8D8D8CDCECEB0B0B0EEEEEEE4E3E3E4E4E4E4E4E4E5
              E4E4E5E4E4E5E4E4E5E4E5CECECEB6B5B5CACAC9ECECECD2D2D2ACB2B6A4CECB
              77B498C3E6F9B5D7E5B5D8E6B5D8E6B5D8E6B5D8E6B5D8E6B7D8E6B8D9E6A7CE
              E0B8DBE9CEF1FE96ACB5D9D9D9CCCCCCABABAAEBEAEAE2E2E2E1E1E1E2E1E1DF
              DFDFDFDFDFE3E2E2E2E1E1E4E3E3E3E2E2E4E4E4EBEBEAD2D2D2ACB2B7A3CCC9
              71AE92BEE4F9B7DEEFB8E0F1B8DFF1B9E1F2B9E0F2B8DEF0B8E0F1B8E0F1BAE1
              F1B8DFF0C7ECFB96ACB5D9D9D9CBCBCBA6A6A6DFDFDFC5C5C5CFCFCECBCBCBDE
              DEDEDCDCDBC5C5C5CECECECCCCCCCCCCCBD0D0D0E7E7E7D2D2D2ACB3B7A4CBC8
              6DAC8FADD1E58BA1AB97B3C195B0BDABD3E4A8CDDF8CA2AC97B3C197B2BF95B1
              BD9BBBC9C3EAFA97ADB5D9D9D9C9C9C99E9E9ED5D5D6ADADADBCBBBBC5C5C5D7
              D7D7D1D1D0ADADADBBBBBBC6C6C6C4C4C4C9C9C9E2E2E2D2D2D2ACB2B6A5CBC5
              63A383ABD6ED9BBFD09FC6D8ABDBF1A9D7EDA5D1E59BBFD09FC6D8ABDAF1ABD9
              EFA8D7EDBEE4F799AEB6D8D8D8CFCFCEB0B0B0E0E0E0D3D3D3D7D6D6E2E2E2DF
              DFDFDCDCDCD3D3D3D7D6D6E2E1E1E1E1E1DFDFDFE4E4E4D2D2D2B1B7BAAFD1DB
              AFD1D3BBDAE9BADBE9BADAE8B9D8E6B9D8E6B9D9E7BADBE9BADAE8B9D8E6B9D8
              E6B8D8E6C3E2ED95ACB4E9E8E8D5D5D5DCDCDCD8D8D8DADADAD9D9D9D8D8D8D8
              D8D8D8D8D8DADADAD9D9D9D8D8D8D8D8D8D8D8D8D7D7D7DADADAFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
          object bVoidCheck: TevBitBtn
            Left = 12
            Top = 155
            Width = 169
            Height = 25
            Action = aVoidNextCheck
            TabOrder = 4
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9F67B83CE3B4CD34151D43B4C
              D37B83CEFAF9F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFF9F9F99C9C9C7C7C7C8080807C7C7C9C9C9CF9F9F9FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFA969CD44D5FE7576DFD5D71FD576D
              FD4D5FE7969CD4FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA
              FAFAAFAEAE8D8D8D9D9C9C9F9F9F9D9C9C8D8D8DAFAEAEFAFAFAFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD0D3EE3E52E07486FCC1C8FD5267F7C1C8
              FD7486FC3E51E0D0D3EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDD
              DDDD828282AFAEAEDBDBDB9B9B9BDBDBDBAFAEAE828282DEDEDEFFFEFEFDFCFB
              FDFBFBFDFBFBFDFBFBFDFBFBFEFDFBC6C9EA4D62E95167F5CED4FBE9ECFECED4
              FB5167F54B5FE9C8CBECFFFFFEFDFCFCFCFCFCFCFCFCFCFCFCFCFCFCFDFDFDD6
              D6D68F8F8F989898E1E1E1F3F3F3E1E1E19898988E8E8ED9D9D9E6E9EABDC8CE
              B6C2C9B4C1C7B4C1C6B4C1C6B9C6C78F9DC35E70E83C55F0C4CCFAF1F3FEC4CC
              FA3C55F0586BE6ABB2D6EAEAEACACACAC5C4C4C3C2C2C3C3C2C3C3C2C6C6C5AB
              AAAA9898988E8E8EDCDCDCF6F6F6DCDCDC8E8E8E949494C2C2C2C5D4DCADD4DB
              AFD9D7C1E4EFBFE3EFBFE3EFC4E8EF9EBEE56574DF7588F3C2CAF84860EBC2CA
              F87588F36575DF89A5CDD7D7D7D5D4D4D8D8D7E6E6E6E5E5E5E5E5E5E8E8E8CA
              CACA979797AEADADD9D9D9939292D9D9D9AEADAD979797B4B4B4C7D6E0A7D2CD
              7FBD9DD3F0FDC3E1ECC4E1EDC4E2EDC3E0EC859CDA6E7DE35469EE4159EA5469
              EE6C7CE38EA7E5AFCFDBDADAD9D0D0D0B5B4B4F4F3F3E3E2E2E4E3E3E4E4E3E3
              E2E2B0AFAF9D9D9C989797908F8F9897979C9B9BBABABAD2D2D2C7D6E0A5CFCC
              7BB89BCAEAFCBFE1EFBFE1EFBFE1EFC0E3F0C2E4F0839CD97485D67D8ED97485
              D6839DDBCBEEFBB0CFDBDADAD9CDCECEB0B0B0EEEEEEE4E3E3E4E4E4E4E4E4E5
              E5E5E6E6E6AFAFAF9F9F9EA6A5A59F9F9EB0B0B0F0F0EFD3D3D3C7D7E1A4CDCA
              75B296C2E6FAB9DFEEB9DEEEB9DFEEB8DCEBB8DDECBDE3F1BAE0EEBAE0EEBAE0
              EEBCE3F0C4E8F7B0CEDBDADAD9CCCCCCABABAAEBEAEAE2E2E2E1E1E1E2E1E1DF
              DFDFE0E0E0E6E5E5E3E3E3E3E2E2E3E2E2E5E5E4EAEAEAD2D2D2C7D7E1A4CCC8
              70AE91B7DBF0A4C2D0AACBDBA8C8D7B3DBECB2D8EAA4C3D0AACBDBA9CAD8A8C9
              D7AACDDDBFE4F4B1CFDBDADADACBCBCBA6A6A6DFDFDFC5C5C5CFCFCECBCBCBDE
              DEDEDCDCDBC5C5C5CECECECCCCCCCCCCCBD0D0D0E7E7E7D3D3D2C8D7E1A3CAC5
              66A688AAD2E88EABB798B8C89DC1D2A8D4E7A4CDE08FABB898B8C89EC2D49CC1
              D29EC6D8B7DFF1B2CFDBDADADAC9C9C99E9E9ED5D5D6ADADADBCBBBBC5C5C5D7
              D7D7D1D1D0ADADADBBBBBBC6C6C6C4C4C4C9C9C9E2E2E2D3D3D3C5D6E0ACCFCE
              82B69FB6DCF0ADD0E1AFD3E4B5DEF1B3DBEEB2D8EAADD0E1AFD3E4B5DEF1B5DD
              F0B3DCEEBFE1F1B2CFDBD9D9D9CFCFCEB0B0B0E0E0E0D3D3D3D7D6D6E2E2E2DF
              DFDFDCDCDCD3D3D3D7D6D6E2E1E1E1E1E1DFDFDFE4E4E4D3D3D2DBE8EDB6D3E0
              BED9E6BAD6E2BBD8E3BBD7E3BAD6E1BAD6E1BAD6E2BBD8E3BBD7E3BAD6E1BAD6
              E1BAD6E1B8D5E1BFD7E2E9E9E9D5D5D5DCDCDCD8D8D8DADADAD9D9D9D8D8D8D8
              D8D8D8D8D8DADADAD9D9D9D8D8D8D8D8D8D8D8D8D7D7D7DADADAFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
          object bNextCheck: TevBitBtn
            Left = 12
            Top = 125
            Width = 169
            Height = 25
            Action = aCreateNextCheck
            TabOrder = 3
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3EFE958B58E099C6123A2
              6FA1D2BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFEEEDEDAAAAAA8C8C8C959595CCCCCCFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF3EE3FB38700BC8139D6A915CB
              960DAE769DD2BBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFF2F2F2A7A7A7ACACACC9C9C9BDBDBD9F9F9FCBCBCBFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF88CBAF00BE8600D08FCBF6E845DD
              AF00CC8D1AAD7AE5F1EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFC3C3C3AEAEAEBEBEBDF2F2F2D1D1D0BABABA9F9F9FF0F0F0FFFEFEFDFCFB
              FDFBFBFDFBFBFDFBFBFDFBFBFDFBFBFEFBFB5DBD9647DAB4C7F3E5F3FCF9D7F6
              EC94EDD400AD73C6E3D4FFFFFFFCFCFCFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFC
              FCFCB3B2B2CFCECEEFEFEFFBFBFBF3F3F3E6E6E69D9D9DE0DFDFE6E9EABDC8CE
              B6C2C9B4C1C7B4C1C6B4C1C6B4C1C6B6C0C75DB19335D6AD45D2A9DDF7EF7DE0
              C435D4AB02A973AFCCC2F2EEF0C8C8C8C3C3C3C2C2C1C2C2C1C2C2C1C2C2C1C2
              C2C1A9A8A8C9C9C8C6C7C7F5F5F5D9D9D9C8C8C89A9A9ACACACAC5D4DCADD4DB
              AFD9D7C1E4EFBFE3EFBFE3EFBFE3EFC3E5EF9ED5CD52CEA90FCC9F92E6D131D1
              AB00BC8847B891B2CFDADCDBDBD4D4D4D8D8D7E6E6E6E5E6E5E5E6E5E5E6E5E7
              E7E7D3D3D2C4C3C3BEBFBEDFDFDFC6C6C6ADACACAEAEAED0D0D0C7D6E0A7D2CD
              7FBD9DD3F0FDC3E1ECC4E1EDC4E1EDC4E2EDC3E0EB8BCABC68D0B145C9A133C0
              954FBA97BCE6E8B5D3E1DDDCDCD0CFCFB5B4B4F4F3F3E3E2E2E4E3E3E4E3E3E4
              E3E3E2E1E1C5C5C5C7C7C6BEBDBDB4B3B3B2B1B1E6E6E6D4D4D4C7D6E0A5CFCC
              7BB89BCAEAFCBFE1EFBFE1EFBFE1EFBFE1EFC0E2F0C4E4F3ABD6DA92C9C49ACD
              CBC0E2EECCEDFDB1D0DEDDDCDCCDCDCDB0B0B0EEEEEEE4E3E3E4E4E4E4E4E4E4
              E4E4E5E4E4E7E7E7D6D6D6C8C7C7CCCCCCE4E4E3F0F0F0D2D2D2C7D7E1A4CDCA
              75B296C2E6FAB9DFEEB9DEEEB9DFEEB8DCEBB8DCECBADFEFBCE0F1BFE1F3BEE1
              F2B9DFF0C3E8F6B2D0DDDDDCDCCBCBCBABABAAEBEAEAE2E2E2E1E1E1E2E1E1DF
              DFDFDFDFDFE3E2E2E3E3E3E5E5E5E4E4E4E2E2E2EAEAEAD2D2D2C7D7E1A4CCC8
              70AE91B7DBF0A4C2D0AACBDBA8C8D7B3DBECB2D8EAA4C3D0AACBDBA9CAD8A8C9
              D7AACDDDBFE4F4B3D1DDDDDDDCCACACAA6A6A6DFDFDFC5C5C5CFCFCECBCBCBDE
              DEDEDCDCDBC5C5C5CECECECCCCCCCCCCCBD0D0D0E7E7E7D2D2D2C8D7E1A3CAC5
              66A688AAD2E88EABB798B8C89DC1D2A8D4E7A4CDE08FABB898B8C89EC2D49CC1
              D29EC6D8B7DFF1B5D1DDDDDDDCC8C8C79E9E9ED5D5D6ADADADBCBBBBC5C5C5D7
              D7D7D1D1D0ADADADBBBBBBC6C6C6C4C4C4C9C9C9E2E2E2D2D2D2C5D6E0ACCFCE
              82B69FB6DCF0ADD0E1AFD3E4B5DEF1B3DBEEB2D8EAADD0E1AFD3E4B5DEF1B5DD
              F0B3DCEEBFE1F1B4D1DDDDDCDCCECDCDB0B0B0E0E0E0D3D3D3D7D6D6E2E2E2DF
              DFDFDCDCDCD3D3D3D7D6D6E2E1E1E1E1E1DFDFDFE4E4E4D2D2D2DBE8EDB6D3E0
              BED9E6BAD6E2BBD8E3BBD7E3BAD6E1BAD6E1BAD6E2BBD8E3BBD7E3BAD6E1BAD6
              E1BAD6E1B8D5E1C0D9E4EFEDEDD4D4D5DCDCDCD8D8D8DADADAD9D9D9D8D8D8D8
              D8D8D8D8D8DADADAD9D9D9D8D8D8D8D8D8D8D8D8D7D7D7DADADAFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
          object Button3: TevBitBtn
            Left = 12
            Top = 95
            Width = 169
            Height = 25
            Caption = 'Refresh scheduled E&/Ds'
            TabOrder = 2
            OnClick = Button3Click
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              CCCCCCCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCDDDDDDFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCCCCD0D0D0FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFCDCCCCCDCCCCDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              329EDE3CA2DDD2D2D2FFFFFFFFFFFFFFFFFFFFFFFF008A4A00C684429E72DEDE
              DEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFACACACAFAFAFD3D3D3FFFFFFFFFFFFFF
              FFFFFFFFFF7C7B7BB5B5B5929292E0DFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              61B2E1329DDE43A2E0CCCCCCCCCCCCCCCCCCCCCCCC00884700E4A600BE8046A0
              75DEDEDEFFFFFFFFFFFFFFFFFFFFFFFFBCBCBBABABABB0AFAFCDCCCCCDCCCCCD
              CCCCCDCCCC7A7979D3D2D2AEAEAE959594E0DFDFFFFFFFFFFFFFFFFFFFFFFFFF
              B7DDF4329CDE6CC9F70E907C00884000884600874600834200D9A100D8A000BC
              8046A075DDDDDDFFFFFFFFFFFFFFFFFFE2E2E2ABABABD2D2D188888879797979
              7979797979757474C9C8C8C8C8C7ACACAC959594DEDEDEFFFFFFFFFFFFFFFFFF
              FFFFFF339BDD8DDCFF00843440E7BE00D7A000D7A000D59F00D09C00D09C00D3
              9F00B98142A074FFFFFFFFFFFFFFFFFFFFFFFFAAAAAAE2E2E2747373DADADAC7
              C7C7C7C7C7C5C5C5C1C1C1C1C1C1C4C3C3AAA9A9959594FFFFFFFFFFFFFFFFFF
              FFFFFF6AB4E465C5FA0083346DE7CD00C89900C89900C89900C79700C89800CA
              9A63E6CD008A47FFFFFFFFFFFFFFFFFFFFFFFFBEBDBDD0D0D0737373DEDEDEBA
              BABABABABABABABAB8B8B8BABABABCBCBBDCDCDC7B7B7BFFFFFFFFFFFFCCCCCC
              CCCCCCA3BFD150B0F000833295EDDE4CE7D24FE7D24DE6D193E8D700C39760E0
              C700B28153B184FFFFFFFFFFFFCDCCCCCDCCCCC3C2C2BEBEBE737373E7E7E6DE
              DEDEDEDEDEDDDDDDE1E1E1B6B6B5D7D6D6A4A4A4A5A5A5FFFFFFFFFFFF389BDC
              48ACE446AAE448AAEA3DB082007F30007F32008035007F3681E4D559DAC300AD
              7D58B285FFFFFFFFFFFFFFFFFFAAA9A9B8B7B7B7B6B6B8B8B8A3A3A36F6F6F6F
              6F6F7170706F6F6FDDDDDDD1D1D1A09F9FA6A6A6FFFFFFFFFFFFFFFFFF3799DB
              9EF1FD81E5FA77E2FC78E3FF78E3FF7AE5FF81E9FF00823677E2D200A97756A9
              79FFFFFFFFFFFFFFFFFFFFFFFFA8A8A7F1F1F1E7E7E6E5E5E5E7E7E6E7E7E6E8
              E7E7EBEBEA737373DBDADA9B9B9B9D9D9DFFFFFFFFFFFFFFFFFFFFFFFF5AA7DE
              85DCF589E5F869DAF769DBF8BBF2FFBCF4FFBFF9FF00842E00A87215947977B0
              E3FFFFFFFFFFFFFFFFFFFFFFFFB3B3B3E0DFDFE7E7E6DEDEDEDFDFDEF3F3F3F5
              F5F5F9F9F87473739999998B8B8BBBBBBBFFFFFFFFFFFFFFFFFFFFFFFF98C7E8
              6DC5EC9CE8FA5FD6F461D7F446B4E63292D93998DE429CE5459DE6449EE5429E
              E1FFFFFFFFFFFFFFFFFFFFFFFFCECDCDCCCCCCEAEAEADADADADBDADABEBDBDA3
              A3A3A9A8A8ADADADAFAFAFAFAFAFADADADFFFFFFFFFFFFFFFFFFFFFFFFDAECF9
              4CAEE4B0EEFB66D8F457D3F3ACEDFB63B5E58FB6D3FEFEFEFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFF0EFEFB9B9B9F0EFEFDCDBDBD7D7D7EFEEEEBE
              BEBEBCBCBBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              3B9ADAACEEFB82DDF54CCEF182DFF6ABE6F83996D9D4D5D7FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9A8A8F0EFEFE0DFDFD3D2D2E2E2E2E8
              E8E8A6A6A6D7D6D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              5EA7DD8BDAF3ACEAF93FCBF045CDF0BBF0FB6DBAE758A3D7F1F1F1FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B3DDDDDDECEBEBD0D0D0D2D2D1F1
              F1F1C3C2C2AEAEAEF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              99C6E76CC5EBD7FAFFCDF6FDC3F3FDD2F8FFC6F1FB409ADAA5BED0FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCCCCCCCCCCFAFAFAF7F6F6F4F4F4F9
              F9F8F3F2F2A9A8A8C2C2C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              D5E9F8439ADA3F97D93E96D93E96D93E97D94099DA459CDB489EDCFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECECECA9A8A8A6A6A6A6A6A6A6A6A6A6
              A6A6A8A8A7AAAAAAACACACFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
          object Button1: TevBitBtn
            Left = 12
            Top = 65
            Width = 169
            Height = 25
            Action = aCheckTemplate
            TabOrder = 1
            Color = clBlack
            Glyph.Data = {
              36060000424D3606000000000000360000002800000020000000100000000100
              18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7C0BB2C9B6B0095590597
              5A6E9E86FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFCBCACA909090858484868686A8A8A8F1F1F1FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDADEDC249A6A00BD8200C98B00C7
              8C06AC7283AC99FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFE2E1E1969595ACACACBAB9B9B7B7B79B9B9BB2B2B2FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF68AC8E03C48B40DFB0B8F4E140DE
              AF00CE940C9D65F6FAF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFB1B0B1ADADADD6D6D6F2F2F2D5D4D4BDBCBC969696FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49B08440DAAFECFBF6A4E9D2EBFB
              F53EDBAF00AB70E9F1EDFFFFFFFEFEFEFCFCFCFBFBFBFBFBFBFBFBFBFBFBFBFB
              FBFBFCFCFCABABABCBCBCAF9F9F9DADADAFAFAFAD5D4D4999898EBEBEBC3C5C6
              BCBEBFBBBDBEBCBDBEBCBDBEBBBEBEC1C3C446967540DCB44FD7B200C58D75DF
              C2C6F5E900AC71AAB6B1FFFFFFEAE9E9C9C9C9C3C3C3C2C2C1C2C2C1C2C2C1C2
              C2C1C2C2C1A1A1A1C9C9C8C4C4C3B5B5B5CFCFCFEFEFEF959494B3B9BBA4CAD4
              ADD2D8B0D5E1B1D6E2B1D6E2B1D6E2B6D7E374C1AD46D0A904CB9C00C69600C6
              953BCEA724AF7E8CA7A9FFFFFFD6D6D6D5D4D4D8D8D7E6E6E6E5E6E5E5E6E5E5
              E6E5E8E8E8C5C5C5BFBEBEC4C3C3BABABAB9B9B9B6B6B6A2A1A1ABB2B6ACD6D1
              89C4A6D7F4FFC9E5EFC9E6F0C9E6F0CBE7F0B6DADF59B69556D2AF3CCFA924C5
              9918AA7794D7C99AAEBAFFFFFFD8D8D8D0D0D0B5B4B4F4F3F3E3E2E2E4E3E3E4
              E3E3E5E4E4DFDEDEB7B7B7BFBFBFBDBDBCAFAFAFA3A3A3DADADAACB2B6A5D0CC
              7BB89CCCECFDC3E5F3C3E5F3C3E5F3C3E5F3C6E7F6BCE3ED82C8B96EBBA26ABC
              A4A1D6D6D6F6FF95ACB6FFFFFFD8D8D8CDCECEB0B0B0EEEEEEE4E3E3E4E4E4E4
              E4E4E4E4E4E6E5E5E6E5E5CECECEC4C3C3C6C6C6DFDEDEF1F1F1ACB2B6A4CECB
              77B498C3E6F9B5D7E5B5D8E6B5D8E6B5D8E6B5D8E6B7D8E7BBDAEBBDDBEDBDDB
              ECBADCECCDF0FE96ACB5FFFFFFD9D9D9CCCCCCABABAAEBEAEAE2E2E2E1E1E1E2
              E1E1DFDFDFDFDFDFE3E2E2E4E4E4E5E5E5E5E5E5E3E3E2EAEAEAACB2B7A3CCC9
              71AE92BEE4F9B7DEEFB8E0F1B8DFF1B9E1F2B9E0F2B8DEF0B8E0F1B8E0F1B8E0
              F1B7DEF0C7ECFB96ACB5FFFFFFD9D9D9CBCBCBA6A6A6DFDFDFC5C5C5CFCFCECB
              CBCBDEDEDEDCDCDBC5C5C5CECECECCCCCCCCCCCBD0D0D0E7E7E7ACB3B7A4CBC8
              6DAC8FADD1E58BA1AB97B3C195B0BDABD3E4A8CDDF8CA2AC97B3C197B2BF95B1
              BD9BBBC9C3EAFA97ADB5FFFFFFD9D9D9C9C9C99E9E9ED5D5D6ADADADBCBBBBC5
              C5C5D7D7D7D1D1D0ADADADBBBBBBC6C6C6C4C4C4C9C9C9E2E2E2ACB2B6A5CBC5
              63A383ABD6ED9BBFD09FC6D8ABDBF1A9D7EDA5D1E59BBFD09FC6D8ABDAF1ABD9
              EFA8D7EDBEE4F799AEB6FFFFFFD8D8D8CFCFCEB0B0B0E0E0E0D3D3D3D7D6D6E2
              E2E2DFDFDFDCDCDCD3D3D3D7D6D6E2E1E1E1E1E1DFDFDFE4E4E4B1B7BAAFD1DB
              AFD1D3BBDAE9BADBE9BADAE8B9D8E6B9D8E6B9D9E7BADBE9BADAE8B9D8E6B9D8
              E6B8D8E6C3E2ED95ACB4FFFFFFE9E8E8D5D5D5DCDCDCD8D8D8DADADAD9D9D9D8
              D8D8D8D8D8D8D8D8DADADAD9D9D9D8D8D8D8D8D8D8D8D8D7D7D7FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            NumGlyphs = 2
            Layout = blGlyphTop
            Margin = 0
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Tax O&verrides'
      ImageIndex = 23
      object sbOverrides: TScrollBox
        Left = 0
        Top = 0
        Width = 958
        Height = 577
        Align = alClient
        TabOrder = 0
        object fpFederal: TisUIFashionPanel
          Left = 8
          Top = 8
          Width = 664
          Height = 210
          BevelOuter = bvNone
          BorderWidth = 12
          Color = 14737632
          TabOrder = 0
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Federal'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label28: TevLabel
            Left = 15
            Top = 35
            Width = 80
            Height = 16
            Caption = '~Tax Frequency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label13: TevLabel
            Left = 15
            Top = 152
            Width = 65
            Height = 13
            Caption = 'Federal Value'
            FocusControl = DBEdit12
          end
          object Label9: TevLabel
            Left = 15
            Top = 113
            Width = 71
            Height = 16
            Caption = '~Federal Type'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object GroupBox1: TevGroupBox
            Left = 250
            Top = 35
            Width = 393
            Height = 154
            HelpContext = 1511
            Caption = '~Block '
            TabOrder = 4
            object evLabel2: TevLabel
              Left = 9
              Top = 61
              Width = 84
              Height = 13
              Caption = 'Additional Federal'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object evDBComboBox1: TevDBComboBox
              Left = 9
              Top = 77
              Width = 120
              Height = 21
              HelpContext = 1511
              ShowButton = True
              Style = csDropDownList
              MapList = False
              AllowClearKey = False
              AutoDropDown = True
              DataField = 'EXCLUDE_ADDITIONAL_FEDERAL'
              DataSource = DS_PR_CHECK
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 1
              UnboundDataType = wwDefault
            end
            object DBRadioGroup11: TevDBRadioGroup
              Left = 8
              Top = 23
              Width = 120
              Height = 35
              HelpContext = 1511
              Caption = 'Federal'
              Columns = 2
              DataField = 'EXCLUDE_FEDERAL'
              DataSource = DS_PR_CHECK
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 0
              Values.Strings = (
                'Y'
                'N')
            end
            object DBRadioGroup13: TevDBRadioGroup
              Left = 136
              Top = 23
              Width = 120
              Height = 35
              HelpContext = 1511
              Caption = 'EE OASDI'
              Columns = 2
              DataField = 'EXCLUDE_EMPLOYEE_OASDI'
              DataSource = DS_PR_CHECK
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 2
              Values.Strings = (
                'Y'
                'N')
            end
            object DBRadioGroup15: TevDBRadioGroup
              Left = 136
              Top = 63
              Width = 120
              Height = 35
              HelpContext = 1511
              Caption = 'EE Medicare'
              Columns = 2
              DataField = 'EXCLUDE_EMPLOYEE_MEDICARE'
              DataSource = DS_PR_CHECK
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 3
              Values.Strings = (
                'Y'
                'N')
            end
            object DBRadioGroup9: TevDBRadioGroup
              Left = 136
              Top = 103
              Width = 120
              Height = 35
              HelpContext = 1511
              Caption = 'EE EIC'
              Columns = 2
              DataField = 'EXCLUDE_EMPLOYEE_EIC'
              DataSource = DS_PR_CHECK
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 4
              Values.Strings = (
                'Y'
                'N')
            end
            object DBRadioGroup14: TevDBRadioGroup
              Left = 264
              Top = 23
              Width = 120
              Height = 35
              HelpContext = 1511
              Caption = 'ER OASDI'
              Columns = 2
              DataField = 'EXCLUDE_EMPLOYER_OASDI'
              DataSource = DS_PR_CHECK
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 5
              Values.Strings = (
                'Y'
                'N')
            end
            object DBRadioGroup16: TevDBRadioGroup
              Left = 264
              Top = 63
              Width = 120
              Height = 35
              HelpContext = 1511
              Caption = 'ER Medicare'
              Columns = 2
              DataField = 'EXCLUDE_EMPLOYER_MEDICARE'
              DataSource = DS_PR_CHECK
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 6
              Values.Strings = (
                'Y'
                'N')
            end
            object DBRadioGroup17: TevDBRadioGroup
              Left = 264
              Top = 103
              Width = 120
              Height = 35
              HelpContext = 1511
              Caption = 'ER FUI'
              Columns = 2
              DataField = 'EXCLUDE_EMPLOYER_FUI'
              DataSource = DS_PR_CHECK
              Items.Strings = (
                'Yes'
                'No')
              TabOrder = 7
              Values.Strings = (
                'Y'
                'N')
            end
          end
          object OverideFederalType: TevDBComboBox
            Left = 15
            Top = 128
            Width = 227
            Height = 21
            HelpContext = 1511
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'OR_CHECK_FEDERAL_TYPE'
            DataSource = DS_PR_CHECK
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object DBEdit12: TevDBEdit
            Left = 15
            Top = 167
            Width = 227
            Height = 21
            HelpContext = 1511
            DataField = 'OR_CHECK_FEDERAL_VALUE'
            DataSource = DS_PR_CHECK
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object TaxFrequency: TevDBComboBox
            Left = 15
            Top = 50
            Width = 227
            Height = 21
            HelpContext = 1511
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TAX_FREQUENCY'
            DataSource = DS_PR_CHECK
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object DBRadioGroup18: TevDBRadioGroup
            Left = 15
            Top = 74
            Width = 227
            Height = 36
            HelpContext = 1511
            Caption = '~Tax Supplemt Rate'
            Columns = 2
            DataField = 'TAX_AT_SUPPLEMENTAL_RATE'
            DataSource = DS_PR_CHECK
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Items.Strings = (
              'Yes'
              'No')
            ParentFont = False
            TabOrder = 2
            Values.Strings = (
              'Y'
              'N')
          end
        end
        object fpState: TisUIFashionPanel
          Left = 8
          Top = 226
          Width = 664
          Height = 210
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpState'
          Color = 14737632
          TabOrder = 1
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'State'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label2: TevLabel
            Left = 12
            Top = 35
            Width = 71
            Height = 13
            Caption = 'State o&verrides'
            FocusControl = wwDBGrid2
          end
          object wwDBGrid2: TevDBGrid
            Left = 12
            Top = 50
            Width = 630
            Height = 137
            HelpContext = 1511
            DisableThemesInTitle = False
            ControlType.Strings = (
              'State_Lookup;CustomEdit;State_LookupCombo;F'
              'EXCLUDE_ADDITIONAL_STATE;CustomEdit;ExclAddStateOvrd'
              'EXCLUDE_SDI;CustomEdit;ExclSDIOvrd'
              'EXCLUDE_STATE;CustomEdit;ExclStateOvrd'
              'EXCLUDE_SUI;CustomEdit;ExclSUIOvrd'
              'STATE_OVERRIDE_TYPE;CustomEdit;StateOverrideCombo'
              'TAX_AT_SUPPLEMENTAL_RATE;CustomEdit;SupplRateOverrideCombo')
            Selected.Strings = (
              'State_Lookup'#9'4'#9'State'#9'F'
              'EXCLUDE_ADDITIONAL_STATE'#9'15'#9'Excl Addtl'#9'F'
              'TAX_AT_SUPPLEMENTAL_RATE'#9'14'#9'Supplmt Rate'#9'F'
              'EXCLUDE_SDI'#9'4'#9'Excl SDI'#9'F'
              'EXCLUDE_STATE'#9'10'#9'Excl State'#9'F'
              'EXCLUDE_SUI'#9'6'#9'Excl SUI'#9'F'
              'STATE_OVERRIDE_TYPE'#9'17'#9'Type'#9'F'
              'STATE_OVERRIDE_VALUE'#9'9'#9'Value'#9'F'
              'EE_SDI_OVERRIDE'#9'7'#9'EE SDI Over'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TREDIT_CHECK\wwDBGrid2'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = DS_PR_CHECK_STATES
            ReadOnly = False
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clWhite
            NoFire = False
          end
          object StateOverrideCombo: TevDBComboBox
            Left = 355
            Top = 111
            Width = 121
            Height = 21
            TabStop = False
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATE_OVERRIDE_TYPE'
            DataSource = DS_PR_CHECK_STATES
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object SupplRateOverrideCombo: TevDBComboBox
            Left = 130
            Top = 105
            Width = 59
            Height = 21
            TabStop = False
            ShowButton = True
            Style = csDropDownList
            MapList = False
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TAX_AT_SUPPLEMENTAL_RATE'
            DataSource = DS_PR_CHECK_STATES
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object State_LookupCombo: TevDBLookupCombo
            Left = 22
            Top = 107
            Width = 97
            Height = 21
            TabStop = False
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'STATE_LOOKUP'#9'10'#9'State'#9'F')
            DataField = 'EE_STATES_NBR'
            DataSource = DS_PR_CHECK_STATES
            LookupTable = DM_EE_STATES.EE_STATES
            LookupField = 'EE_STATES_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object ExclSDIOvrd: TevDBComboBox
            Left = 208
            Top = 108
            Width = 59
            Height = 21
            TabStop = False
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'EXCLUDE_SDI'
            DataSource = DS_PR_CHECK_STATES
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Yes'#9'Y'
              'No'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
          object ExclStateOvrd: TevDBComboBox
            Left = 271
            Top = 105
            Width = 59
            Height = 21
            TabStop = False
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'EXCLUDE_STATE'
            DataSource = DS_PR_CHECK_STATES
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Yes'#9'Y'
              'No'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 5
            UnboundDataType = wwDefault
          end
          object ExclSUIOvrd: TevDBComboBox
            Left = 319
            Top = 123
            Width = 59
            Height = 21
            TabStop = False
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'EXCLUDE_SUI'
            DataSource = DS_PR_CHECK_STATES
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Yes'#9'Y'
              'No'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 6
            UnboundDataType = wwDefault
          end
          object ExclAddStateOvrd: TevDBComboBox
            Left = 76
            Top = 132
            Width = 59
            Height = 21
            TabStop = False
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'EXCLUDE_ADDITIONAL_STATE'
            DataSource = DS_PR_CHECK_STATES
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Additional'#9'Y'
              'Override Regular'#9'R'
              'Both'#9'B'
              'None'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
          end
        end
        object fpLocal: TisUIFashionPanel
          Left = 8
          Top = 444
          Width = 664
          Height = 210
          BevelOuter = bvNone
          BorderWidth = 12
          Caption = 'fpLocal'
          Color = 14737632
          TabOrder = 2
          RoundRect = True
          ShadowDepth = 8
          ShadowSpace = 8
          ShowShadow = True
          ShadowColor = clSilver
          TitleColor = clGrayText
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWhite
          TitleFont.Height = -13
          TitleFont.Name = 'Arial'
          TitleFont.Style = [fsBold]
          Title = 'Local'
          LineWidth = 0
          LineColor = clWhite
          Theme = ttCustom
          object Label3: TevLabel
            Left = 12
            Top = 35
            Width = 72
            Height = 13
            Caption = 'L&ocal overrides'
            FocusControl = wwDBGrid3
          end
          object wwDBGrid3: TevDBGrid
            Left = 12
            Top = 50
            Width = 630
            Height = 137
            HelpContext = 1511
            DisableThemesInTitle = False
            ControlType.Strings = (
              'Local_Name_Lookup;CustomEdit;LocalLookupCombo;F'
              'EXCLUDE_LOCAL;CustomEdit;YNOverrideCombo_Grid3;F'
              'OVERRIDE_AMOUNT;CustomEdit;LocalOverrideAmountEdit;F')
            Selected.Strings = (
              'Local_Name_Lookup'#9'69'#9'Local Name'#9'F'
              'EXCLUDE_LOCAL'#9'14'#9'Exclude'#9'F'
              'OVERRIDE_AMOUNT'#9'14'#9'Amount'#9'F')
            IniAttributes.Enabled = False
            IniAttributes.SaveToRegistry = False
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TREDIT_CHECK\wwDBGrid3'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = DS_PR_CHECK_LOCALS
            ReadOnly = False
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object YNOverrideCombo_Grid3: TevDBComboBox
            Left = 230
            Top = 102
            Width = 121
            Height = 21
            TabStop = False
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'EXCLUDE_LOCAL'
            DataSource = DS_PR_CHECK_LOCALS
            DropDownCount = 8
            ItemHeight = 0
            Items.Strings = (
              'Yes'#9'Y'
              'No'#9'N')
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object LocalLookupCombo: TevDBLookupCombo
            Left = 94
            Top = 101
            Width = 97
            Height = 21
            TabStop = False
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'Local_Name_Lookup'#9'20'#9'Name'#9'F')
            DataField = 'EE_LOCALS_NBR'
            DataSource = DS_PR_CHECK_LOCALS
            LookupTable = DM_EE_LOCALS.EE_LOCALS
            LookupField = 'EE_LOCALS_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
          end
          object LocalOverrideAmountEdit: TevDBEdit
            Left = 448
            Top = 112
            Width = 121
            Height = 21
            DataSource = DS_PR_CHECK_LOCALS
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            OnChange = LocalOverrideAmountEditChange
            Glowing = False
          end
        end
      end
    end
    object tsPreview: TTabSheet
      Caption = 'Previe&w'
      ImageIndex = 9
      object evPanel1: TevPanel
        Left = 0
        Top = 506
        Width = 846
        Height = 32
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object evBitBtn1: TevBitBtn
          Left = 8
          Top = 6
          Width = 120
          Height = 25
          Caption = 'Plug taxes'
          TabOrder = 0
          OnClick = evBitBtn1Click
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD444444
            4444DDDDDD8888888888DDDDDD4777777774DDDDDD8DDDDDDDD8D88888488888
            7774DDDDDD8DDDDDDDD80000000000044774DDDDDD8DD8888DD80FFFFFFFFF08
            7774FFFFFFFFFFDDDDD80FFFFFFFFF044774888888888F888DD80F7000007F08
            7774888888888FDDDDD80FFFFFFFFF04477488DDDDD88F888DD80F7000007F08
            7774888888888FDDDDD80FFFFFFFFF04477488DDDDD88F888DD80F7000007F08
            7774888888888FDDDDD80FFFFFFFFF08777488DDDDD88FDDDDD80F7000007F04
            4444888888888F8888880FFFFFFFFF08DDDD88DDDDD88FDDDDDD0FFFFFFFFF08
            DDDD888888888FDDDDDD00000000000DDDDD888888888FDDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
      end
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 846
        Height = 506
        Align = alClient
        TabOrder = 1
        object evPanel2: TevPanel
          Left = 0
          Top = 0
          Width = 842
          Height = 502
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          object pnlPreview: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 826
            Height = 486
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            OnResize = pnlPreviewResize
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Check Preview'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlPreviewBody: TevPanel
              Left = 12
              Top = 36
              Width = 757
              Height = 373
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object strPreviewGrid: TevStringGrid
                Left = 0
                Top = 0
                Width = 757
                Height = 373
                HelpContext = 1512
                Align = alClient
                ColCount = 2
                DefaultColWidth = 300
                DefaultRowHeight = 15
                FixedCols = 0
                RowCount = 18
                FixedRows = 0
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
                TabOrder = 1
                SortOptions.CanSort = False
                SortOptions.SortStyle = ssNormal
                SortOptions.SortCaseSensitive = False
                SortOptions.SortCol = -1
                SortOptions.SortDirection = sdAscending
              end
              object edPreviewHiddenField: TEdit
                Left = 2000
                Top = 184
                Width = 121
                Height = 21
                TabOrder = 0
                Text = 'edPreviewHiddenField'
              end
            end
          end
        end
      end
    end
    object tsNotes: TTabSheet
      Caption = 'Employee Notes'
      ImageIndex = 12
      object sbNotes: TScrollBox
        Left = 0
        Top = 0
        Width = 958
        Height = 577
        Align = alClient
        TabOrder = 0
        object evPanel3: TevPanel
          Left = 0
          Top = 0
          Width = 954
          Height = 573
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 8
          TabOrder = 0
          object pnlNotes: TisUIFashionPanel
            Left = 8
            Top = 8
            Width = 938
            Height = 557
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 12
            Color = 14737632
            TabOrder = 0
            OnResize = pnlNotesResize
            RoundRect = True
            ShadowDepth = 8
            ShadowSpace = 8
            ShowShadow = True
            ShadowColor = clSilver
            TitleColor = clGrayText
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWhite
            TitleFont.Height = -13
            TitleFont.Name = 'Arial'
            TitleFont.Style = [fsBold]
            Title = 'Employee Notes'
            LineWidth = 0
            LineColor = clWhite
            Theme = ttCustom
            object pnlNotesBody: TevPanel
              Left = 12
              Top = 36
              Width = 757
              Height = 373
              BevelOuter = bvNone
              ParentColor = True
              TabOrder = 0
              object Label10: TevLabel
                Left = 144
                Top = 96
                Width = 38
                Height = 13
                Caption = 'Label10'
              end
              object DBMemo1: TEvDBMemo
                Left = 0
                Top = 0
                Width = 757
                Height = 373
                HelpContext = 1513
                Align = alClient
                DataField = 'NOTES'
                DataSource = DS_EE_CL_PERSON
                TabOrder = 0
              end
            end
          end
        end
      end
    end
  end
  object Panel9: TevPanel [1]
    Left = 0
    Top = 0
    Width = 966
    Height = 88
    Align = alTop
    Color = 4276545
    TabOrder = 1
    object pnlFavoriteReport: TevPanel
      Left = 815
      Top = 1
      Width = 150
      Height = 86
      Align = alRight
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      TabOrder = 0
      object evLabel47: TevLabel
        Left = 2
        Top = 2
        Width = 146
        Height = 13
        Align = alTop
        Alignment = taCenter
        Caption = 'Additional Tools'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object sbtnTaxCalc: TevSpeedButton
        Left = 115
        Top = 16
        Width = 26
        Height = 25
        Hint = 'Tax and Check Line Calculator (F9)'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = sbtnTaxCalcClick
        NumGlyphs = 2
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFDCDCDC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCD3D3D3FFFFFFFFFFFFDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
          CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCD4D4D4FFFFFFFFFFFFB0B0B0
          9F9F9F9E9E9E9E9E9E9E9F9F9E9E9E9E9E9E9E9F9F9E9E9E9D9E9F9D9E9F9D9E
          9E9F9F9FAAAAAAFFFFFFFFFFFFB1B0B0A09F9F9F9F9E9F9F9EA09F9F9F9F9E9F
          9F9EA09F9F9F9F9E9F9F9E9F9F9E9F9F9EA09F9FAAAAAAFFFFFFFFFFFF9F9F9F
          FFFEFEFBFAFAFEFDFDFEFDFDFDFCFCFEFDFDFEFDFEFBFCFDF9FCFFF9FCFFF9FA
          FCFFFEFE9F9F9FFFFFFFFFFFFFA09F9FFFFFFFFCFCFBFEFEFEFEFEFEFEFDFDFE
          FEFEFFFFFFFEFDFDFEFEFEFEFEFEFCFCFBFFFFFFA09F9FFFFFFFFFFFFF9E9E9E
          F7F6F6F2F1F08D8A886D6B68F7F6F58D8B896C6B69F2F6F9E49F55E49F55EEF2
          F5F7F6F79E9E9EFFFFFFFFFFFF9F9F9EF7F7F7F3F2F28A8A8A6A6A6AF7F7F78B
          8B8B6B6B6BF8F8F8999999999999F4F4F4F8F8F89F9F9EFFFFFFFFFFFF9D9E9E
          F4F3F2EDECEBF2F1F0F3F3F2F0F0EFF3F2F1F3F3F3EDF1F6EEAD66E4A25BE9ED
          F2F4F4F39D9E9EFFFFFFFFFFFF9F9F9EF4F4F4EDEDEDF3F2F2F4F4F4F1F1F1F3
          F3F3F4F4F4F3F3F3A7A7A79C9C9CF0EFEFF5F5F59F9F9EFFFFFFFFFFFF9E9E9E
          F3F2F0E9E8E78E8C8A6E6C6AEDECEB8F8D8A6E6C6BE9ECF0F0AC62F0AC62E5E8
          ECF2F2F19E9E9EFFFFFFFFFFFF9F9F9EF3F3F3E9E9E98C8C8C6C6C6CEDEDED8D
          8D8D6C6C6CEEEEEEA6A6A6A6A6A6EAEAEAF3F3F39F9F9EFFFFFFFFFFFF9E9E9E
          F0EEEEE4E2E2E9E7E7EAE8E9E8E6E6E9E7E8EAE8E9E6E5E7E5E6EBE4E6EBE2E2
          E4F0EEEE9E9E9EFFFFFFFFFFFF9F9F9EF0EFEFE4E3E3E8E8E8EAEAEAE8E7E7E8
          E8E8EAEAEAE7E7E6E8E8E8E8E8E8E4E3E3F0EFEF9F9F9EFFFFFFFFFFFF9E9F9F
          EEEDEDE0DEDD918E8A716F6BE5E2E2918F8C706E6CE4E2E2918F8C706E6AE1DF
          DEEEEDEE9E9F9FFFFFFFFFFFFFA09F9FEFEEEEE0DFDF8E8E8E6E6E6EE4E3E38F
          8F8F6D6D6DE4E3E38F8F8F6D6D6DE0E0E0EFEEEEA09F9FFFFFFFFFFFFF9F9F9F
          EEECECDDD9D5E4DED6E5DFD7E0DDD9E0DEDDE1DFDEE1DDD9E5DFD7E5DFD6DDD9
          D5EEEDEC9F9F9FFFFFFFFFFFFFA09F9FEDEDEDDADADADEDEDEDFDFDEDEDEDEE0
          DFDFE0E0E0DEDEDEDFDFDEDFDFDEDADADAEEEEEEA09F9FFFFFFFFFFFFF9F9F9F
          EDECEADAD5CE1F7FFF2266FFDDD8D1928F8C716F6CDED8D22080FF2265FFDAD5
          CEEEECEA9F9F9FFFFFFFFFFFFFA09F9FEDEDEDD5D5D5A6A6A69A9A9AD8D8D88F
          8F8F6E6E6ED8D8D8A6A6A6999999D5D5D5EDEDEDA09F9FFFFFFFFFFFFFA0A1A1
          F1F0F0F1EFEDF6F1ECF6F2ECF2F1EFF1F1F2F2F2F3F2F1EFF6F2ECF6F2ECF1F0
          EDF1F0F0A0A1A1FFFFFFFFFFFFA1A1A1F2F2F1F0F0F0F2F2F1F3F2F2F2F2F1F3
          F2F2F3F3F3F2F2F1F3F2F2F3F2F2F1F1F1F2F2F1A1A1A1FFFFFFFFFFFFA2A3A5
          7C7E7F7B7D807C7E807C7E817B7E817B7E827B7E827B7E817C7E817C7E807B7D
          807C7E7FA2A3A5FFFFFFFFFFFFA4A4A47F7E7E7E7E7D7F7E7E7F7E7E7F7E7E80
          807F80807F7F7E7E7F7E7E7F7E7E7E7E7D7F7E7EA4A4A4FFFFFFFFFFFFA0A3A7
          FFCA8BF9C27EF9C27FF9C280F9C280F9C280F9C280F9C280F9C280F9C27FF9C2
          7EFFCA8BA0A3A7FFFFFFFFFFFFA4A4A4C4C3C3BBBBBBBBBBBBBBBBBBBBBBBBBB
          BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBC4C3C3A4A4A4FFFFFFFFFFFF9EA2A6
          F9D0A4E8A760E7A762E7A863E7A863E7A863E7A863E7A863E7A863E7A762E8A7
          60F9D0A49EA2A6FFFFFFFFFFFFA3A3A3CBCBCBA1A0A0A1A0A0A1A1A1A1A1A1A1
          A1A1A1A1A1A1A1A1A1A1A1A1A0A0A1A0A0CBCBCBA3A3A3FFFFFFFFFFFF9FA2A5
          FDDBB6F8DAB8F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DA
          B8FDDBB69FA2A5FFFFFFFFFFFFA3A3A3D7D7D7D7D6D6D7D6D6D7D6D6D7D6D6D7
          D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7D7D7A3A3A3FFFFFFFFFFFFA9AAAB
          9FA2A59EA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59EA1
          A59FA2A5A9AAABFFFFFFFFFFFFABABABA3A3A3A2A2A2A2A2A2A2A2A2A2A2A2A2
          A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A3A3A3ABABABFFFFFF}
        ParentColor = False
        ShortCut = 120
      end
      object sbtnCheckFinder: TevSpeedButton
        Left = 88
        Top = 16
        Width = 26
        Height = 25
        Hint = 'Check Finder (F12)'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = sbtnCheckFinderClick
        NumGlyphs = 2
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCC
          CCCCCCCCCCCCCCF4F4F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFCDCCCCCDCCCCCDCCCCF6F6F5D0D0D0CCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC4880
          AC4980AA4982ACB0BDC7D1D1D1CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
          CCCCCDCCCCCDCCCCCDCCCCCDCCCC8989898989898B8B8BBFBFBFE9BB7DE9B773
          E9B672E9B672E9B672EBB772EFBA74F2BD76F4BF77F4BF76FFC273377EB6739E
          BF21ACFF94CEEB4591C6B4B4B4B0AFAFAFAFAFAFAFAFAFAFAFB0AFAFB2B2B2B5
          B5B5B8B7B7B7B6B6BABABA8B8B8BA5A5A5C0C0C0D3D3D39D9D9DE9B773FFEDCD
          FFE8C5FFE8C5FFEBC7FFF0CBC0B4A37B7C7E7375797375797777798E8985B0A6
          9E9EC1D7AEE9FF4091C6B0AFAFE9E9E9E4E3E3E4E3E3E6E6E6EBEBEAB2B2B27D
          7C7C777676777676787877898989A6A6A6C5C5C5ECECEC9D9D9DE9B671FFE9CA
          FFF2E1FFF3E2FFE9BE8E8983979798D6D6DAE1E3E7E1E2E6D7D6D99C9A998A85
          82F2ECE7BCDDEA4092CAAFAFAFE5E5E5F0F0F0F2F2F1E3E3E2888888989898D8
          D8D8E5E5E5E4E4E4D8D8D89A9A9A868585ECECECE0DFDF9F9F9EE9B671FFE9CC
          FFF1DFFFF5E2C0AF999B9B9DE6E6E6E8C790F1CC81F7DA8EF5E1A8E7E7E69E9C
          9BB5AEA83891CBFFFFFFAFAFAFE6E6E6F0EFEFF3F3F3ACACAC9C9C9CE8E7E7C0
          C0C0C3C2C2CFCFCFD8D8D8E8E8E89D9D9DAEAEAE9E9E9EFFFFFFE9B671FFECD0
          FFDCB1FFE3B5838182E0E2E5E6BF8BEECA8CF1D48EF9E59EFFEEA5F5E3A9E0E0
          E48B827CFFFFFFFFFFFFAFAFAFE8E8E8D7D7D7DDDDDD828181E4E3E3B9B9B9C2
          C2C1CACACADBDADAE3E3E2DADADAE2E2E2828181FFFFFFFFFFFFE9B671FFECD3
          FFDAACFFE1B17C7D7FF2F5F9E4B675F3DDB7F3DAA3F6DF99F9E59EF6D98DF2F3
          F8888582FFFFFFFFFFFFAFAFAFE9E9E9D5D5D5DBDADA7E7E7DF7F7F7AFAFAFD8
          D8D8D3D2D2D5D5D5DBDADACECECEF6F6F5868585FFFFFFFFFFFFE9B670FFECD5
          FFEDD8FFF4DD807F7FF8FBFFE3B377F7E9D2F4DEB7F3DAA2F2D48FEFCA7FF8FA
          FE8A8785FFFFFFFFFFFFAFAFAFEAEAEAEBEBEAF1F1F180807FFEFDFDACACACE7
          E7E6D9D9D9D2D2D1CACACAC0C0C0FCFCFB878787FFFFFFFFFFFFE9B670FFEFDA
          FFECD5FFF2DA8A8784EEF2F5E3B682FAEFE2F7E8D1F3DCB5EDCB90EDCB94EFF2
          F6928C88FFFFFFFFFFFFAFAFAFEDEDEDEAEAEAF0EFEF878787F4F4F4B1B0B0EE
          EEEEE5E5E5D7D6D6C4C3C3C4C4C4F4F4F48C8C8CFFFFFFFFFFFFE9B670FFF1E1
          FFD3A1FFD7A3C7BCB0AEAEAFFFFFFFE3B782E5BA85E6BE87EAC591FFFFFFAEAF
          B2C1A580FFFFFFFFFFFFAFAFAFF0EFEFCECDCDD1D1D1BBBBBBAFAFAFFFFFFFB2
          B1B1B4B4B4B8B7B7BEBEBEFFFFFFB1B0B0A1A0A0FFFFFFFFFFFFE9B56FFFF3E7
          FFD09BFFD29EFFF1D8A09D9AB1B1B4FAFCFFFFFFFFFFFFFFF9FBFFB1B1B2A1A0
          A0F2BB72FFFFFFFFFFFFADADADF3F2F2CACACACDCCCCEEEEEE9D9D9DB2B2B2FE
          FEFEFFFFFFFFFFFFFEFDFDB2B2B2A1A0A0B3B3B3FFFFFFFFFFFFE9B56FFFF5EA
          FFE8CEFFEAD2FFCF99FFCE9ACABEB19391908C8C8E8C8C8E939190CABBADFFFA
          EDEBB770FFFFFFFFFFFFADADADF5F5F5E5E5E5E8E7E7CAC9C9C9C8C8BDBCBC92
          92928D8D8D8D8D8D929292BABABAF9F9F8B0AFAFFFFFFFFFFFFFE9B56FFFF8F0
          FFE4C8FFE6CCFFCA92FFCB93FFEACEFFECCFFFCF93FFCF93FFEBCEFFE8CAFFF9
          F1E9B56FFFFFFFFFFFFFADADADF8F8F8E1E1E1E4E3E3C4C4C4C5C5C5E7E7E6E8
          E8E8C9C8C8C9C8C8E8E7E7E4E4E4F9F9F8ADADADFFFFFFFFFFFFE9B670FFFDFC
          FFFAF6FFFBF7FFFCF9FFFCF9FFFBF7FFFBF7FFFCF9FFFCF9FFFBF7FFFBF6FFFE
          FCE9B670FFFFFFFFFFFFAFAFAFFEFEFEFBFBFBFCFCFBFDFDFCFDFDFCFCFCFBFC
          FCFBFDFDFCFDFDFCFCFCFBFCFCFBFFFFFFAFAFAFFFFFFFFFFFFFEDBF81E9B670
          E9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B56EE9B6
          70EDBF81FFFFFFFFFFFFB8B7B7AFAFAFADADADADADADADADADADADADADADADAD
          ADADADADADADADADADADADADADADAFAFAFB8B7B7FFFFFFFFFFFF}
        ParentColor = False
        ShortCut = 123
      end
      object bEE: TevSpeedButton
        Left = 7
        Top = 16
        Width = 26
        Height = 25
        Hint = 'Employee'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = bEEClick
        NumGlyphs = 2
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFF5E7DA
          F0DDC8F0DDCAF0DDCAF0DDCAF0DDCAF0DECAF0DECAF0DECAF0DECAF0DECAF0DE
          CAF0DECAF0DDC9FCF7F2FFFFFFE6E6E6DBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDC
          DCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDBDBDBF7F7F7FFFFFFE4BB91
          E0A564E2A96CE2A96CE2A96BE2A96BE2A96AE1A869E1A869E1A869E1A768E1A7
          68E1A768DCA061F4E7DAFFFFFFB7B7B79F9F9FA3A3A3A3A3A3A3A4A4A3A3A3A3
          A3A3A2A2A2A2A2A2A2A1A1A1A1A1A1A1A1A1A1A19B9A9AE7E7E7FFFFFFE9C7A3
          E6AE6EEAB679EAB679EAB679E9B478E9B376E9B274E8B172E8B071E8AF70E8AF
          6FE8AE6EE2A96DF7EEE4FFFFFFC3C3C3A7A7A7B0B0B0B0B0B0B0AFAFAEAEAEAD
          ADADACACACABAAAAAAA9A9A9A9A8A9A9A7A8A8A7A3A3A3EDEDEDFFFFFFF0DBC5
          E5B071EBBB80EBBB80EBBA80EAB97DEAB67AE9B477E8B274E8B173E7AF71E7AF
          70E6AA6AE3B582FCF9F6FFFFFFDADADAA9A9A9B4B4B4B5B4B4B4B3B3B2B2B2B0
          B0B0AEAEAEACACACAAAAAAA9A8A9A9A8A8A4A4A4B0B0B0F9F9F9FFFFFFFBF6F1
          E5BC8BEDC086EDC18AEEC28AEDC28BECBF88EBBD83EAB87EE9B477E8B172E8AF
          70E3AB69EED6BBFFFFFFFFFFFFF6F6F6B7B6B6B9B9B9BBBBBBBCBBBBBBBBBBBA
          BAB9B6B6B6B2B2B2AEAEAEAAAAAAA9A8A8A4A3A3D3D3D3FFFFFFFFFFFFFFFFFF
          F1DECBE5BB87EFC893E9C18EE7C398E8C69EE8C69EE7C295E5B983E7B275E5AE
          6EE5BE92FCF9F5FFFFFFFFFFFFFFFFFFDDDDDCB5B5B5C2C2C2BBBBBBBEBEBEC2
          C2C2C2C2C2BDBDBCB3B3B3ACACACA7A7A7BABABAF9F9F8FFFFFFFFFFFFFFFFFF
          FFFEFEEED8C0E6C196EFDCC5F7EFE6F6EBDFF4E8DBF7EEE4F6ECE1E8C79EE5BF
          94F9F2EBFFFFFFFFFFFFFFFFFFFFFFFFFEFEFED6D6D5BCBCBCD9D9D9EEEEEEEA
          EAEAE8E8E8EDEDEDEBEBEAC2C2C2BBBBBAF1F1F1FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFBF7F3F2E0CEE3B483E0A769E0A566E0AA70E8C8A5F9F2EBFDFB
          F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F7DEDEDEB0B0B0A2
          A1A19F9F9FA5A5A5C4C4C4F1F1F1FBFBFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF2E0CDE0A96CE7AC6CE8B071E8B072E7AE6EE2A664E4BC90FDFA
          F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEA3A3A2A6A6A6AA
          AAAAAAAAAAA8A8A79F9F9FB7B7B7FAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFCF8F5E3B482E7AF6FE9B477E9B376E8B274E8B173E8B071E1A463EFD9
          C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8B0B0AFA9A9A9AEADAEAD
          ADADACACACABABABAAAAAA9E9E9ED6D6D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFF6EBE0E2AC70EAB87CEAB87CEAB77BE9B579E9B376E8B274E5A968E6C0
          98FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEAA6A6A6B2B1B1B2B1B1B1
          B0B0AFAFAFADADADABABABA3A3A3BCBCBCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFF4E7D9E3AF72EDBE85ECBE85EBBC82EAB97EE9B67AE9B376E6AC6BE4BB
          90FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6A9A9A8B8B8B8B8B8B8B6
          B6B5B3B3B3B0AFAFADADADA6A5A5B6B6B6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFF9F1E9E5B882EEC48CEEC48DEDC28AECBE85EAB97EEAB579E5AB6AEACB
          AAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0F0B2B2B2BDBDBDBEBEBEBC
          BBBBB8B7B7B3B2B2AFAFAFA5A4A4C8C8C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFEFEFDEACCABEBC48EF0CC97EEC791EDC189EBBC82EAB77AE3B27AF6EC
          E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDC8C8C8BDBDBDC5C4C4C0
          C0C0BBBBBAB6B6B5B1B0B0ACACABEAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF9F3EDE7C6A0EBC796EECA98EEC692EABE86E5B983EFDBC4FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F3C2C2C2C0C0C0C4
          C4C4C1C0C0B8B8B8B3B3B2D8D8D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFCF9F6F0DDC8EACCABE8C8A4ECD2B4F6ECE1FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8DBDBDBC9
          C9C9C4C4C4CFCFCFEBEBEBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        ParentColor = False
        ShortCut = 0
      end
      object bEERates: TevSpeedButton
        Left = 34
        Top = 16
        Width = 26
        Height = 25
        Hint = 'Employee Rates'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = bEERatesClick
        NumGlyphs = 2
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDCDCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCDCDCDCDDDDDDCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
          CCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCDDDDDD8CBAA36DB08F
          6EB18F6FB18F6EB18F6EB08E6EB18E6EB18E6EB18E6EB18E6EB08E6EB18F6FB1
          906EB18F6DB08F8CBAA3B4B4B4A7A7A7A8A8A7A8A8A7A8A8A7A7A7A7A8A8A7A8
          A8A7A8A8A7A8A8A7A7A7A7A8A8A7A8A8A7A8A8A7A7A7A7B4B4B46AAD8BCBF4DB
          8CC7B583BEA793CBBB92CAB992CBBA93CBBB93CBBB92CBBA92CAB993CBBB7DBB
          A085C1ACCBF4DC6AAE8BA3A3A3EEEEEEC1C1C1B8B7B7C6C6C6C4C4C4C5C5C5C6
          C6C6C6C6C6C5C5C5C4C4C4C6C6C6B3B3B3BABABAEEEEEEA4A4A465A986CAF3DC
          5FA27C4E9368B8EBCFB7E9CD98D1B287C4A387C4A398D1B2B7E9CDB8EBCF4F92
          69599F77CAF3DD65A986A09F9FEDEDED989898888888E4E3E3E2E2E2C9C8C8BB
          BBBBBBBBBBC9C8C8E2E2E2E4E3E3878787959594EDEDEDA09F9F62A581CDF2E0
          73B593B3E7CDB0E4CAB0E4CA6BAC89B2E3C7B2E3C76BAC89B0E4CAB0E4CAB3E7
          CD74B593CDF2E062A5819B9B9BEDEDEDABABABE0E0E0DDDDDDDDDDDDA2A2A2DC
          DBDBDCDBDBA2A2A2DDDDDDDDDDDDE0E0E0ACACACEDEDED9B9B9B5FA37ED0F2E4
          6CAD8BABE2C851946DABE2C86DAC88B0E2C6B0E2C66DAC88ABE2C851946DABE2
          C86CAD8BD0F2E45FA37E999999EFEEEEA4A4A4DBDADA8A8A8ADBDADAA2A2A2DB
          DADADBDADAA2A2A2DBDADA8A8A8ADBDADAA4A4A4EFEEEE9999995CA07AD8F5E8
          67AA85A7E0C6A5DEC4A5DEC461A17CB2E3C6B2E3C661A27CA5DEC4A5DEC4A7E0
          C667AA85D8F5E85CA07A969595F2F2F1A1A0A0D9D9D9D7D6D6D7D6D6979797DC
          DBDBDCDBDB989898D7D6D6D7D6D6D9D9D9A1A0A0F2F2F1969595599D75DFF7EF
          549A705EA27D9FDBC39FDBC280BFA07EB89478B38F80BFA09FDBC29FDBC35EA2
          7D559B73DFF7EF599D75939393F5F5F58F8F8F989898D4D4D4D4D4D4B7B6B6AF
          AFAFAAA9A9B7B6B6D4D4D4D4D4D4989898919191F5F5F5939393569B72E5F9F4
          448B5E468C615A9E765A9D755A9E765B9F775B9F775A9E765A9D755A9E76468C
          61448B5EE5F9F4569B72919191F8F8F880807F82818194939393939394939395
          959495959494939393939394939382818180807FF8F8F891919154976FE7FAF5
          E3F6F1E3F6F1E3F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E2F5F0E3F5F0E3F6
          F1E3F6F1E7FAF554976F8D8D8DFAF9F9F5F5F5F5F5F5F4F4F4F4F4F4F4F4F4F4
          F4F4F4F4F4F4F4F4F4F4F4F4F4F4F5F5F5F5F5F5FAF9F98D8D8D8CB99E89C6A6
          B0E7CCAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5CAAEE5
          CAB0E7CC89C6A68CB99EB2B2B2BEBDBDE0DFDFDEDEDEDEDEDEDEDEDEDEDEDEDE
          DEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEE0DFDFBEBDBDB2B2B2FFFFFF8AB79B
          5092694F91684F91684F91684F91684F91684F91684F91684F91684F91684F91
          685092698AB79BFFFFFFFFFFFFB0AFAF87878786868686868686868686868686
          8686868686868686868686868686868686878787B0AFAFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        ParentColor = False
        ShortCut = 0
      end
      object bScheduledEDs: TevSpeedButton
        Left = 61
        Top = 16
        Width = 26
        Height = 25
        Hint = 'Scheduled E/Ds'
        HideHint = True
        ParentShowHint = False
        ShowHint = True
        AutoSize = False
        OnClick = bScheduledEDsClick
        NumGlyphs = 2
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
          DCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCDCDCDCFFFFFFFFFFFFFFFFFFDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCC
          CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCDCDCDCFFFFFFFFFFFFFFFFFF
          BBBBB9B0B0ADB0B0ACB3B2ACB0B0ACAEAEABADADABADADABADADABADADABAEAE
          ABAFAFADBBBBB9FFFFFFFFFFFFFFFFFFBBBBBBB1B0B0B0AFAFB2B1B1B0AFAFAE
          AEAEADADADADADADADADADADADADAEAEAEB0AFAFBBBBBBFFFFFFFFFFFFFFFFFF
          B0B0AEFFFFFFFFFFFF868CF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFB0B0AEFFFFFFFFFFFFFFFFFFB1B0B0FFFFFFFFFFFFAAA9A9FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0FFFFFFFFFFFFFFFFFF
          AFAFADFFFFFFFFFFFF363DE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFAFAFADFFFFFFFFFFFFFFFFFFB0AFAFFFFFFFFFFFFF7D7C7CFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB0AFAFFFFFFFFFFFFFFFFFFF
          AFB0AEE4D4BFD0B58D2423BAD0B68FC6AC8FC3AA8FC3AA8FC3AA8FC3AA8FC3A9
          8DE2D2C0AFB0AEFFFFFFFFFFFFFFFFFFB1B0B0D2D2D1B0AFAF626262B1B0B0A9
          A8A8A7A7A7A7A7A7A7A7A7A7A7A7A6A6A6D0D0D0B1B0B0FFFFFFFFFFFFFFFFFF
          AFB0AEFFFFFFFFFFFF3A42E3FFFFFFFFFFFFFEFFFFFEFFFFFEFFFFFDFFFFFDFF
          FFFFFFFFAFB0AEFFFFFFFFFFFFFFFFFFB1B0B0FFFFFFFFFFFF80807FFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0FFFFFFFFFFFFFFFFFF
          AFB0AFE5D5C1D1B78E2625BCD2B88FC7AF90C5AC90C5AC90C5AC90C5AC8FC4AB
          8EE2D3C1AFB0AFFFFFFFFFFFFFFFFFFFB1B0B0D3D3D3B2B1B1646464B2B2B2AB
          ABABA9A8A8A9A8A8A9A8A8A9A8A8A8A8A7D2D2D1B1B0B0FFFFFFFFFFFFFFFFFF
          AFB0AEFFFFFFFFFFFC3740E2FFFFFEFBFEFEF9FBFEF9FBFEF9FBFEF8FBFEF8FA
          FDFFFFFFAFB0AEFFFFFFFFFFFFFFFFFFB1B0B0FFFFFFFFFFFF7F7E7EFFFFFFFF
          FFFFFDFDFCFDFDFCFDFDFCFDFDFCFCFCFBFFFFFFB1B0B0FFFFFFFFFFFFFFFFFF
          AFB0AFE5D5C1D0B68E2625BDD1B890C7AE91C5AC91C5AC91C5AC91C4AB91C3AA
          8FE3D3C1AFB0AFFFFFFFFFFFFFFFFFFFB1B0B0D3D3D3B1B0B0646464B2B2B2AA
          AAAAA9A8A8A9A8A8A9A8A8A8A8A7A7A7A7D2D2D1B1B0B0FFFFFFFFFFFFFFFFFF
          AFB0AEFFFFFFFFFFF6373FE0FFFFF8F5F8F9F3F5F9F3F5F9F3F5F9F3F5F8F2F4
          F6FFFFFFAFB0AEFFFFFFFFFFFFFFFFFFB1B0B0FFFFFFFEFEFE7D7C7CFFFFFFFA
          F9F9F7F7F7F7F7F7F7F7F7F7F7F7F6F6F5FFFFFFB1B0B0FFFFFFFFFFFFFFFFFF
          AFB0AFE5D5C1D1B68D2726BCD2B890C8AF91C5AD91C5AD91C5AD91C5AD90C4AB
          8EE3D3C1AFB0AFFFFFFFFFFFFFFFFFFFB1B0B0D3D3D3B1B0B0646464B3B3B3AB
          ABABAAA9A9AAA9A9AAA9A9AAA9A9A8A8A7D2D2D1B1B0B0FFFFFFFFFFFFFFFFFF
          AFB0AEFFFFFFF8FAEF353DDFFAFBF2F0F2F3EDF0F3EDF0F3EDF0F3EDEFF2ECEE
          F0FFFFFFAFB0AEFFFFFFFFFFFFFFFFFFB1B0B0FFFFFFF9F9F87C7B7BFAFAFAF3
          F3F3F2F2F1F2F2F1F2F2F1F1F1F1F0EFEFFFFFFFB1B0B0FFFFFFFFFFFFFFFFFF
          AFB0AEE5D5C1CFB58D2624BCD0B78FC7AD90C4AC90C4AC90C4AC90C4AB90C3A9
          8DE3D3C1AFB0AEFFFFFFFFFFFFFFFFFFB1B0B0D3D3D3B0AFAF646464B2B1B1AA
          A9A9A9A8A8A9A8A8A9A8A8A8A8A7A6A6A6D2D2D1B1B0B0FFFFFFFFFFFFFFFFFF
          AFAFADFFFFFFEEEDE42C34D7EFEFE5E5E6E5E3E4E5E3E4E5E3E4E5E3E4E4E2E3
          E3FFFFFFAFAFADFFFFFFFFFFFFFFFFFFB0AFAFFFFFFFECECEC767675EEEEEEE7
          E7E6E5E5E5E5E5E5E5E5E5E5E5E5E4E4E4FFFFFFB0AFAFFFFFFFFFFFFFFFFFFF
          B0B0AEFFFFFFFFFFFF898DF2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFB0B0AEFFFFFFFFFFFFFFFFFFB1B0B0FFFFFFFFFFFFABABABFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0FFFFFFFFFFFFFFFFFF
          B8B8B6B0B0ADB1B0ACB3B3ACB0B0ACAEAEABAEAEABAEAEABAEAEABAEAEABAEAE
          ACB0B0ADB8B8B6FFFFFFFFFFFFFFFFFFB8B8B8B1B0B0B0AFAFB2B2B2B0AFAFAE
          AEAEAEAEAEAEAEAEAEAEAEAEAEAEAFAFAFB1B0B0B8B8B8FFFFFF}
        ParentColor = False
        ShortCut = 0
      end
    end
    object pnlTopLeft: TevPanel
      Left = 1
      Top = 1
      Width = 814
      Height = 86
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvLowered
      Color = 4276545
      TabOrder = 1
      object Label45: TevLabel
        Left = 449
        Top = 50
        Width = 39
        Height = 13
        Caption = '$830.00'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblCheck: TevLabel
        Left = 511
        Top = 62
        Width = 34
        Height = 13
        Caption = 'Check '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblBatch: TevLabel
        Left = 511
        Top = 50
        Width = 31
        Height = 13
        Caption = 'Batch '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblDBDT: TevLabel
        Left = 9
        Top = 35
        Width = 48
        Height = 13
        Caption = 'lblDBDT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object HeaderPanel: TevPanel
        Left = 2
        Top = 2
        Width = 581
        Height = 32
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object PR_Label: TevLabel
          Left = 340
          Top = 3
          Width = 105
          Height = 16
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Batch_Label: TevLabel
          Left = 432
          Top = 3
          Width = 148
          Height = 16
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lablClient: TevLabel
          Left = 8
          Top = 3
          Width = 26
          Height = 13
          Caption = 'Client'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel7: TevLabel
          Left = 8
          Top = 17
          Width = 44
          Height = 13
          Caption = 'Company'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dbtClNumber: TevDBText
          Left = 62
          Top = 3
          Width = 70
          Height = 16
          DataField = 'CUSTOM_CLIENT_NUMBER'
          DataSource = dsCL
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dbtCoNumber: TevDBText
          Left = 62
          Top = 17
          Width = 70
          Height = 16
          DataField = 'CUSTOM_COMPANY_NUMBER'
          DataSource = wwdsList
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dbtCoName: TevDBText
          Left = 136
          Top = 17
          Width = 201
          Height = 16
          DataField = 'NAME'
          DataSource = wwdsList
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object dbtClName: TevDBText
          Left = 136
          Top = 3
          Width = 201
          Height = 16
          DataField = 'NAME'
          DataSource = dsCL
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lEeFilter: TLabel
          Left = 349
          Top = 17
          Width = 47
          Height = 13
          Caption = 'lEeFilter'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object pnlDropDowns: TevPanel
        Left = 9
        Top = 50
        Width = 428
        Height = 27
        BevelOuter = bvLowered
        TabOrder = 1
        object cbRates: TevDBComboBox
          Left = 340
          Top = 3
          Width = 84
          Height = 22
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          AutoSelect = False
          Ctl3D = True
          DropDownCount = 8
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ItemHeight = 0
          ParentCtl3D = False
          ParentFont = False
          Picture.PictureMaskFromDataSet = False
          ReadOnly = True
          Sorted = False
          TabOrder = 0
          UnboundDataType = wwDefault
        end
        object dblkupEE_NBR: TevDBLookupCombo
          Left = 4
          Top = 3
          Width = 65
          Height = 21
          Ctl3D = True
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'Trim_Number'#9'9'#9'Aligned EE Code'#9'F'
            'Employee_Name_Calculate'#9'40'#9'Name'#9'F')
          LookupTable = EEClone
          LookupField = 'EE_NBR'
          Style = csDropDownList
          ParentCtl3D = False
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = dblkupEE_NBRChange
          OnCloseUp = dblkupEE_NBRCloseUp
        end
        object dbtxtname: TevDBLookupCombo
          Left = 72
          Top = 3
          Width = 129
          Height = 21
          Ctl3D = True
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'Employee_Name_Calculate'#9'40'#9'Name'#9'F')
          LookupTable = EEClone
          LookupField = 'EE_NBR'
          Style = csDropDownList
          Frame.FocusStyle = efsFrameSingle
          ParentCtl3D = False
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = dblkupEE_NBRChange
          OnCloseUp = dblkupEE_NBRCloseUp
        end
        object CHECK_TYPE: TevDBComboBox
          Left = 204
          Top = 3
          Width = 57
          Height = 21
          ShowButton = True
          Style = csDropDownList
          MapList = True
          AllowClearKey = False
          AutoDropDown = True
          Ctl3D = True
          DataField = 'CHECK_TYPE'
          DataSource = DS_PR_CHECK
          DropDownCount = 8
          Enabled = False
          ItemHeight = 0
          ParentCtl3D = False
          Picture.PictureMaskFromDataSet = False
          Sorted = False
          TabOrder = 3
          UnboundDataType = wwDefault
          OnChange = CHECK_TYPEChange
        end
        object DBEdit4: TevDBEdit
          Left = 264
          Top = 3
          Width = 73
          Height = 21
          Ctl3D = True
          DataField = 'PAYMENT_SERIAL_NUMBER'
          DataSource = DS_PR_CHECK
          ParentCtl3D = False
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
    end
  end
  inherited Panel1: TevPanel
    Top = 694
    Width = 966
    Height = 55
    TabOrder = 2
    inherited BitBtn4: TevSpeedButton
      Left = 12
      Width = 120
      Caption = 'Batch'
      OnClick = BitBtn4Click
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCDDDD
        DDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFCDCCCCCDCCCCDEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008A4A00C684429E
        72DEDEDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF7C7B7BB5B5B5929292E0DFDFFFFFFFFFFFFFFFFFFFFEFEFE
        ECECECCDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00884700E4A600BE
        8046A075DEDEDEFFFFFFFFFFFFFFFFFFEDEDEDCECECECDCCCCCDCCCCCDCCCCCD
        CCCCCDCCCCCDCCCC7A7979D3D2D2AEAEAE959594E0DFDFFFFFFFFFFFFFF2F2F2
        B5B5B589888993878C308A6100894600884700884700874600834200D9A100D8
        A000BC8046A075DDDDDDFFFFFFF3F3F3B6B6B58989898989897F7E7E7A79797A
        79797A7979797979757474C9C8C8C8C8C7ACACAC959594DEDEDEFFFFFFD5D5D5
        9B9C9CF4F0F0FFF2F800853B3FE8C000D7A000D7A100D7A100D59F00D09C00D0
        9C00D39F00B98142A074FFFFFFD6D6D69D9D9DF2F2F1F6F6F5767675DBDADAC7
        C7C7C8C8C7C8C8C7C5C5C5C1C1C1C1C1C1C4C3C3AAA9A9959594FFFFFFBDBDBD
        AEADADF0EDEC9A888D0083396CE8CE00C89900C89900C89A00C89900C79700C8
        9800CA9A63E6CD008A47FFFFFFBEBDBDAEAEAEEFEEEE8B8B8B747373DFDFDEBA
        BABABABABABABABABABABAB8B8B8BABABABCBCBBDCDCDC7B7B7BFFFFFFA9A9A9
        BBB8B7DAD6D6E1D1D500843992EEE04BE8D34FE8D34FE8D34DE6D193E8D800C3
        9760E0C600B28153B184FFFFFFAAA9A9B9B9B9D7D7D7D4D4D4757474E8E8E8DF
        DFDEDFDFDEDFDFDEDDDDDDE2E2E2B6B6B5D7D6D6A4A4A4A5A5A5FFFFFF969696
        C5C3C2CECAC99C91923EA07300853900843A00843A00843C00813981E4D557DA
        C300AD7D5BB387FFFFFFFFFFFF979696C4C4C4CBCBCB93939394939376767575
        7474757474757474727171DDDDDDD1D1D1A09F9FA7A7A7FFFFFFFFFFFF888888
        CAC8C6959391CCC7C6A09697D8C8CCA7969ADCC9CEB3989F00863C73E2D400A9
        7934875EFFFFFFFFFFFFFFFFFF888888C9C8C8949393C9C8C8979797CBCBCB98
        9898CDCCCC9C9C9C777676DCDBDB9B9B9B7D7C7CFFFFFFFFFFFFD1D1D1858585
        A8A5A4A6A4A3A7A3A2A8A4A2A9A3A2A9A4A3ABA4A4B6A6A900843700A5783290
        6297858DD1D1D1FFFFFFD3D2D2868585A6A6A6A5A5A5A4A4A4A4A4A4A4A4A4A5
        A5A5A5A5A5A9A8A8757474989898858484888888D3D2D2FFFFFF8D8D8EE9E8E9
        D9DCDFD9DCE2D9DBDDDAD9D9DAD9D7DAD9D7DAD9D7DFDADBE6DCE2E8DDE7E4DC
        E3EEE9EC918E8FFFFFFF8E8E8EEAEAEADEDEDEDFDFDEDCDCDCDADADADADADADA
        DADADADADADCDBDBE0DFDFE1E1E1E0DFDFECEBEB8F8F8FFFFFFF858687E7E9ED
        D4AA66C66E00C1C7D3C1C2C5C2C0BFC2C0BEC2C0BFC2C2C6C3C7D3C86E00D6AA
        67E8E9ED858687FFFFFF868686ECEBEBA2A2A26B6B6BCACACAC4C3C3C1C1C1C1
        C1C1C1C1C1C4C3C3CACACA6B6B6BA2A2A2ECEBEB868686FFFFFFAEAEAFE6E8EA
        FFEDABDD9C33C57000DFE7F3E2E4E7E3E3E3E2E4E7DFE7F3C57000DD9C33FFED
        ABE6E8EA919192FFFFFFAFAFAFEAEAEAE3E3E29393936C6C6CEBEBEAE6E6E6E4
        E4E4E6E6E6EBEBEA6C6C6C939393E3E3E2EAEAEA929292FFFFFFFFFFFFADAEB0
        7C828CFFEDB2DA9B35C772007C859580858F7C8595C77200DA9B35FFEDB27C82
        8CADAFB0FFFFFFFFFFFFFFFFFFAFAFAF858484E4E4E49292926E6E6E89898987
        87878989896E6E6E929292E4E4E4858484B0AFAFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFE8B1DB9C37CB7600FFFFFFCB7600DB9C37FFE8B1FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0E0E0939393727171FF
        FFFF727171939393E0E0E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFE9B3DB9E3BFFFFFFDB9E3BFFE9B3FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1959594FF
        FFFF959594E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    inherited BitBtn3: TevSpeedButton
      Left = 268
      Width = 120
      Visible = False
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE6E6E6CCCC
        CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFE8E7E7CDCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF69AE8F008C
        4BCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFA5A5A57D7C7CCDCCCCFFFFFFFFFFFFE0E0E0CCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC00884754DA
        B0008746CCCCCCFFFFFFE1E1E1CDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCDCCCCCD
        CCCCCDCCCCCDCCCCCDCCCC7A7979CECDCD797979CDCCCCFFFFFF51A67F008A49
        00894700894700894700894700894700894700894700884600864400B97F00D8
        A065D7B3008744CCCCCC9B9B9B7B7B7B7A79797A79797A79797A79797A79797A
        79797A7979797979787877AAA9A9C8C8C7CCCCCC787877CDCCCC008A4900D5A7
        00D1A100D0A000D0A000D0A000D0A000D0A000D0A000D0A000CF9F00CD9C00CB
        9A00CD9C74DABD008A487B7B7BC7C7C7C3C2C2C2C2C1C2C2C1C2C2C1C2C2C1C2
        C2C1C2C2C1C2C2C1C1C1C1BEBEBEBDBCBCBEBEBED1D1D17B7B7B008A4865E6D1
        61E2CB61E1CA61E1CA61E1CA61E1CA61E1CA61E1CA61E1CA61E0C864DFC700C4
        9A00C59C86DEC8008A487B7B7BDDDDDDD9D9D9D8D8D8D8D8D8D8D8D8D8D8D8D8
        D8D8D8D8D8D8D8D8D7D6D6D6D6D6B7B6B6B8B8B8D7D6D67B7B7B50B184008A47
        00884500884400884400884400884400884400884400874400864100AB7D00C0
        9E9BE0D0008743FFFFFFA5A5A57B7B7B79797979797979797979797979797979
        79797979797878777776769E9E9EB4B4B4DBDADA787877FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008945A4E4
        D9008743FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF7A7979E0DFDF787877FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF65BA93008C
        49FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFAFAFAF7D7C7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object BitBtn1: TevSpeedButton [2]
      Left = 140
      Top = 8
      Width = 120
      Height = 25
      Caption = 'Recalculate Chec&k'
      HideHint = True
      AutoSize = False
      OnClick = BitBtn1Click
      NumGlyphs = 2
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCCCCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCCCC
        CCCCCCCCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCFCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCC51687A4966845090D9CCCCCCCCCCCCD3D3D3FFFF
        FFFFFFFFFFFFFFFFFFFFCFCFCFCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC6B6B6B6C
        6C6CA2A2A2CCCCCCCCCCCCD3D3D3FFFFFFFFFFFFFFFFFFFFFFFFA6A6A6A0A0A0
        9F9F9F9E9F9FA09F9EA7A39F56819F80A7B68ED5FF2E669BB5AAA1B1ADAAFFFF
        FFFFFFFFFFFFFFFFFFFFA7A7A7A1A0A0A09F9FA09F9FA09F9FA3A3A3878787AA
        A9A9DDDDDD737373AAA9A9ADADADFFFFFFFFFFFFFFFFFFFFFFFFA0A0A0FFFFFF
        FDFCFCFDFCFCFDFBFBFFFFFC3EAFF58BE7FF80D4FF1199FF30669BB6ABA1FFFF
        FFFFFFFFFFFFFFFFFFFFA1A0A0FFFFFFFEFDFDFEFDFDFDFDFCFFFFFFBEBEBEEA
        EAEADCDCDCB4B4B4737373AAAAAAFFFFFFFFFFFFFFFFFFFFFFFF9E9F9FFBFAF9
        8E8B896D6B69F6F5F3938E897F72692374C83FC5FF2AAAFF169CFF34699BCCCC
        CCFFFFFFFFFFFFFFFFFFA09F9FFBFBFB8B8B8B6B6B6BF6F6F58E8E8E7271718A
        8A8AD0D0D0BEBEBEB6B6B5757474CCCCCCFFFFFFFFFFFFFFFFFF9E9F9FF7F4F6
        F0F0F0F1F0F1EEEDEEF2F0F0F7F3F1FFF5ED2475C743C7FF2CABFF159DFF306B
        A2CCCCCCFFFFFFFFFFFFA09F9FF6F6F5F1F1F1F2F2F1EFEEEEF2F2F1F4F4F4F5
        F5F58B8B8BD2D2D1BFBFBFB7B6B6797979CCCCCCFFFFFFFFFFFF9F9F9FF6F3F2
        908D89706D69EBE9E8908E8B706E6BEFEBEAFFB1552675C741C7FF21ABFF84B1
        D77F7971CCCCCCFFFFFFA09F9FF4F4F48D8D8D6C6C6CEAEAEA8E8E8E6D6D6DEC
        ECECAAA9A98B8B8BD2D2D1BFBFBFB9B9B9787877CCCCCCFFFFFF9F9F9FF4F2EE
        ECE5DDECE7DEE7E4E0E8E5E4E8E6E5E7E4E1EEE7DEFBEBDB2275C6AEDBEF9288
        80C1BFB8777C6ECCCCCCA09F9FF3F2F2E5E5E5E7E7E6E4E4E4E6E6E6E8E7E7E5
        E5E5E7E7E6EAEAEA8A8A8ADFDFDE878787BEBEBE7A7979CCCCCC9F9F9EF4F0EA
        1E7EFF2165FFE3DED7918F8B716E6BE4DED81F7FFF2667FFFEF4EA867E77EBE9
        E5898C83BA7AB69869CAA09F9FF0F0F0A5A5A5999999DEDEDE8F8F8F6D6D6DDE
        DEDEA6A6A69A9A9AF4F4F47E7E7DEAEAEA8A8A8A8F8F8F8A8A8A9F9F9FF0EEEB
        DCD7CFDCD7CFD8D6D2D8D7D5D8D7D6D9D6D2DCD7CFDDD8CFF2EFEBA1A1A08384
        7DE3B2E1CB96C6AE7DCEA09F9FEFEEEED7D6D6D7D6D6D7D6D6D7D7D7D8D8D8D7
        D6D6D7D6D6D7D7D7F0EFEFA1A1A1828282C2C2C1A7A7A7979797A1A1A1F1EFEF
        EFEEEEEFEEEEEEEEEEEEEEEFEEEEEFEEEEEEEFEEEEEFEEEEF1F0F0A2A3A2FFFF
        FFC38AD8BF8AD3FFFFFFA1A1A1F0F0F0F0EFEFF0EFEFF0EFEFF0EFEFF0EFEFF0
        EFEFF0EFEFF0EFEFF2F2F1A3A3A3FFFFFFA4A4A4A2A2A2FFFFFFA2A3A57C7E7F
        7A7D807A7D817A7D817A7D817A7D817A7D817A7D817A7D807C7E80A2A5A5FFFF
        FFFFFFFFFFFFFFFFFFFFA4A4A47F7E7E7E7E7D7F7E7E7F7E7E7F7E7E7F7E7E7F
        7E7E7F7E7E7E7E7D7F7E7EA6A6A6FFFFFFFFFFFFFFFFFFFFFFFFA0A3A7FFCA8B
        F9C27EF9C27FF9C280F9C280F9C280F9C280F9C27FF9C27EFFCA8B9FA3A7FFFF
        FFFFFFFFFFFFFFFFFFFFA4A4A4C4C3C3BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
        BBBBBBBBBBBBBBBBC4C3C3A4A4A4FFFFFFFFFFFFFFFFFFFFFFFF9EA2A6F9D0A4
        E8A760E7A762E7A863E7A863E7A863E7A863E7A762E8A760F9D0A49EA2A6FFFF
        FFFFFFFFFFFFFFFFFFFFA3A3A3CBCBCBA1A0A0A1A0A0A1A1A1A1A1A1A1A1A1A1
        A1A1A1A0A0A1A0A0CBCBCBA3A3A3FFFFFFFFFFFFFFFFFFFFFFFF9FA2A5FDDBB6
        F8DAB8F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB9F8DAB8FDDBB69FA2A5FFFF
        FFFFFFFFFFFFFFFFFFFFA3A3A3D7D7D7D7D6D6D7D6D6D7D6D6D7D6D6D7D6D6D7
        D6D6D7D6D6D7D6D6D7D7D7A3A3A3FFFFFFFFFFFFFFFFFFFFFFFFA9AAAB9FA2A5
        9EA1A59DA1A59DA1A59DA1A59DA1A59DA1A59DA1A59EA1A59FA2A5A9AAABFFFF
        FFFFFFFFFFFFFFFFFFFFABABABA3A3A3A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2A2
        A2A2A2A2A2A2A2A2A3A3A3ABABABFFFFFFFFFFFFFFFFFFFFFFFF}
      ParentColor = False
      ShortCut = 0
    end
    inherited cbCalcCL: TevCheckBox
      Left = 12
      Top = 35
    end
    inherited evPanel4: TevPanel
      Left = 395
      Top = 8
      Width = 120
      Height = 33
      inherited sbWizard: TevSpeedButton
        Left = 0
        Top = 0
        Width = 120
        OnClick = sbWizardClick
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_PR_CHECK.PR_CHECK
    OnDataChange = wwdsMasterDataChange
    OnDataChangeBeforeChildren = wwdsMasterDataChangeBeforeChildren
    Left = 821
    Top = 234
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_PR_CHECK_LINES.PR_CHECK_LINES
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    OnUpdateData = wwdsDetailUpdateData
    OnDataChangeBeforeChildren = wwdsDetailDataChangeBeforeChildren
    Left = 726
    Top = 266
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_CO.CO
    Left = 770
    Top = 234
  end
  inherited ActionList: TevActionList
    Left = 799
    Top = 130
    object FocusNbr: TAction
      Caption = 'FocusNbr'
      ShortCut = 16506
      OnExecute = FocusNbrExecute
    end
    object FocusName: TAction
      Caption = 'FocusName'
      ShortCut = 16507
      OnExecute = FocusNameExecute
    end
    object NextCheck: TAction
      Category = 'Dataset'
      Caption = '&Next'
      Hint = 'Next'
      ImageIndex = 2
      ShortCut = 107
      OnExecute = NextCheckExecute
    end
    object PriorCheck: TAction
      Category = 'Dataset'
      Caption = '&Prior'
      Hint = 'Prior'
      ImageIndex = 1
      ShortCut = 106
      OnExecute = PriorCheckExecute
    end
    object FocusMain: TAction
      Caption = 'FocusMain'
      ShortCut = 16505
      OnExecute = FocusMainExecute
    end
    object CalculateCheckLine: TAction
      Caption = 'Calculate Check Line'
      ShortCut = 16500
      OnExecute = CalculateCheckLineExecute
    end
    object ShowLocals: TAction
      Caption = 'Locals'
      OnExecute = ShowLocalsExecute
      OnUpdate = ShowLocalsUpdate
    end
    object aCreateNextCheck: TAction
      Caption = 'Create Next Check'
      ShortCut = 16462
      OnExecute = aCreateNextCheckExecute
    end
    object aVoidNextCheck: TAction
      Caption = 'Void Next Check'
      ShortCut = 16470
      OnExecute = aVoidNextCheckExecute
      OnUpdate = aVoidNextCheckUpdate
    end
    object aCreateManualCheck: TAction
      Caption = 'Create Manual Check'
      ShortCut = 16461
      OnExecute = aCreateManualCheckExecute
    end
    object aCheckTemplate: TAction
      Caption = 'Apply Check Template'
      ShortCut = 16449
      OnExecute = aCheckTemplateExecute
    end
    object aDuplicate: TAction
      Caption = 'aDuplicate'
      ShortCut = 16452
      OnExecute = aDuplicateExecute
    end
  end
  object CheckLineGridPopup: TevPopupMenu
    AutoHotkeys = maManual
    OnPopup = CheckLineGridPopupPopup
    Left = 804
    Top = 225
  end
  object DS_PR_CHECK: TevDataSource
    DataSet = DM_PR_CHECK.PR_CHECK
    Left = 764
    Top = 273
  end
  object DS_PR_CHECK_LINES: TevDataSource
    DataSet = DM_PR_CHECK_LINES.PR_CHECK_LINES
    OnDataChange = DS_PR_CHECK_LINESDataChange
    Left = 812
    Top = 209
  end
  object DS_EE_CL_PERSON: TevDataSource
    DataSet = DM_EE.EE
    Left = 692
    Top = 281
  end
  object DS_PR_CHECK_STATES: TevDataSource
    DataSet = DM_PR_CHECK_STATES.PR_CHECK_STATES
    MasterDataSource = wwdsMaster
    Left = 700
    Top = 201
  end
  object DS_PR_CHECK_LINE_LOCALS: TevDataSource
    DataSet = DM_PR_CHECK_LINE_LOCALS.PR_CHECK_LINE_LOCALS
    MasterDataSource = DS_PR_CHECK_LINES
    Left = 628
    Top = 292
  end
  object DS_PR_CHECK_LOCALS: TevDataSource
    DataSet = DM_PR_CHECK_LOCALS.PR_CHECK_LOCALS
    MasterDataSource = wwdsMaster
    Left = 641
    Top = 350
  end
  object DS_CL_E_DS: TevDataSource
    DataSet = DM_CL_E_DS.CL_E_DS
    Left = 700
    Top = 180
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 731
    Top = 230
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 748
    Top = 131
  end
  object DM_PAYROLL: TDM_PAYROLL
    Left = 768
    Top = 159
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 700
    Top = 239
  end
  object EEStatesSrc: TevDataSource
    DataSet = DM_EE_STATES.EE_STATES
    MasterDataSource = DS_EE_CL_PERSON
    Left = 320
    Top = 64
  end
  object EELocalsSrc: TevDataSource
    DataSet = DM_EE_LOCALS.EE_LOCALS
    MasterDataSource = DS_EE_CL_PERSON
    Left = 585
    Top = 57
  end
  object EERatesSrc: TevDataSource
    DataSet = DM_EE_RATES.EE_RATES
    MasterDataSource = DS_EE_CL_PERSON
    Left = 694
    Top = 152
  end
  object ManualTaxClientDataSet: TevClientDataSet
    BeforeEdit = ManualTaxClientDataSetBeforeEdit
    BeforePost = ManualTaxClientDataSetBeforePost
    AfterPost = ManualTaxClientDataSetAfterPost
    Left = 708
    Top = 129
    object ManualTaxClientDataSetLabel: TStringField
      DisplayLabel = 'Tax description'
      DisplayWidth = 40
      FieldName = 'Label'
      Size = 40
    end
    object ManualTaxClientDataSetAmountFieldName: TStringField
      DisplayWidth = 40
      FieldName = 'AmountFieldName'
      Visible = False
      Size = 40
    end
    object ManualTaxClientDataSetRowNbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'RowNbr'
      Visible = False
    end
    object ManualTaxClientDataSetDataSetName: TStringField
      DisplayWidth = 60
      FieldName = 'DataSetName'
      Visible = False
      Size = 60
    end
    object ManualTaxClientDataSetIDFieldName: TStringField
      DisplayWidth = 40
      FieldName = 'IDFieldName'
      Visible = False
      Size = 40
    end
    object ManualTaxClientDataSetID: TIntegerField
      DisplayWidth = 10
      FieldName = 'ID'
      Visible = False
    end
    object ManualTaxClientDataSetAmount: TFloatField
      DisplayWidth = 10
      FieldName = 'Amount'
      Visible = False
      DisplayFormat = '0.00'
    end
    object ManualTaxClientDataSetRO: TBooleanField
      FieldName = 'RO'
    end
  end
  object ManualTaxSrc: TevDataSource
    DataSet = ManualTaxClientDataSet
    Left = 678
    Top = 160
  end
  object DS_PR_CHECK_SUI: TevDataSource
    DataSet = DM_PR_CHECK_SUI.PR_CHECK_SUI
    MasterDataSource = DS_PR_CHECK
    Left = 720
    Top = 145
  end
  object BatchSrc: TevDataSource
    DataSet = DM_PR_BATCH.PR_BATCH
    Left = 689
    Top = 327
  end
  object PR_CHECKClone: TevClientDataSet
    Left = 627
    Top = 51
  end
  object EEClone: TevClientDataSet
    Left = 753
    Top = 207
  end
  object dsCL: TevDataSource
    DataSet = DM_CL.CL
    Left = 735
    Top = 295
  end
  object PageControlImages: TevImageList
    Left = 640
    Top = 120
    Bitmap = {
      494C01011B001D00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000008000000001001000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000039673967396739673967
      3967396739673967396739673967000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000F231D129B129B129B129
      B129B129B129B129B129D129F231000000000000000000000000000000000000
      000000000000000000000000000000000000AB3DA941496E2A6A0000FF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000D1290000000000000000
      000000000000000000000000D129000000000000000000000000000000000000
      0000000000000000000000000000000000000B4E905A527FEB7EA74D0000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000B1290000FF7FFF7F0000
      D515D5150000FF7FFF7F0000B129000000000000000000000000000000000000
      000000000000000000000000000000000000A976937F737F647E627EA74D0000
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000B1290000FF7FFF7FD515
      FE3AFE3AD515FF7FFF7F0000B129000000000000000000000000000000000000
      000000000000000000000000000000000000C976917FE87E857E637E637EA74D
      0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000B1290000FE7FD515FD36
      FD3AFD3AFD36D515FE7F0000B129000000000000000000000000000000000000
      39673967DE7B000000000000000000000000FF7FE661297FE77EA57E637E637E
      A74D0000FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000B1290000D5111E431E43
      1D43FD363E471E43D5110000B129000000000000000000000000000000003967
      8C318C31F75E000000000000000000000000FF7FFF7FE7612A7FE77EA57E637E
      637EA74D0000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000B1290000983AD619F619
      3D4FBC2EF61DF619983A0000B129000000000000000000000000000039678C31
      B556B5568C31396700000000000000000000FF7FFF7FFF7FE7612A7FE77EA57E
      637E637EA74D0000FF7FFF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000B1290000BD7BDD7FD615
      7D5B3D53F619DD7FBD7B0000B12900000000000000000000000039678C31F85E
      D65AD65AF85E8C3139670000000000000000FF7FFF7FFF7FFF7FE7612A7FE77E
      A57E637E637EA74D0000FF7FFF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000B12900009C739C77983A
      D615D615983A9C779C730000B1290000000000000000000000008C317B6F9D73
      9C739C739D737B6F8C310000000000000000FF7FFF7FFF7FFF7FFF7FE7612A7F
      E77EA57E637E627EA6510000FF7FFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000B1290000000000000000
      000000000000000000000000B12900000000000000000000000031468C318C31
      8C318C318C318C3131460000000000000000FF7FFF7FFF7FFF7FFF7FFF7FE761
      2A7FE77EA57E627EAF6AF0390000FF7FFF7F0000000000000000000000000000
      000000000000000000000000000000000000000000003726381E171E171E171E
      171E171E171E171E171E381E1626000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      E761297FE67EF26E3242D756EF350000FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000058225E4FFD36FD3AFD3A
      FD3AFD3AFD3AFD3AFD365E4F5822000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FE665977B123E5A673142D659B365FF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000058261C4B9A2A9A2A9A2A
      9A2A9A2A9A2A9A2A9A2A1C4B5826000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F123EDE7730425966F759F565FF7F0000000000000000000000000000
      000000000000000000000000000000000000000000005826FB46FB46FB46FB46
      FB46FB46FB46FB46FB46FB465826000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7F103EFD769A6A376AFF7FFF7F0000000000000000000000000000
      00000000000000000000000000000000000000000000792A5826582658265826
      582658265826582658265826792A000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7F386E376AFF7FFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000039673967000000000000
      000000009C733967396739679C73000000000000000000000000000000003967
      3967396700000000000000000000000000000000000000003967396700000000
      0000000000000000000000000000000000004539253925394539453945394539
      45394539453945392539253925394539FF7F00000000666E876E5A6B00000000
      00009C73CA46602E602E602ECA469C730000000000000000000000000000AB3D
      89414A6E3967000000000000000000000000000000000000666E876E5A6B0000
      0000000000000000000000000000000000000D4E2E52CA458841884188418841
      8841884188418841AA452E522E520D4EFF7F00000000CC72666E886E3967FF7F
      0000CA468036E0426E63E0428036CA4600000000FF7FBD773967396739670B52
      905A527FA64D39673967BD77FF7F00000000000000000000CC72666E886E3967
      FF7F000000000000000000000000000000004F52663D4F56CA45884188458845
      8845A845A84588418841CA4545394F52FF7F00000000767B666E0C77666E1767
      FF7F602A0047E0420000E0420047602E00000000DE7BD65A314610421142C87A
      917F507F627EA64D5342D65ADE7B00000000000000000000767B666E0C77666E
      1767FF7F00000000000000000000000000004539883D8841A845A945EA4D8E5A
      D56FEB4DA949A949A945884188414539FF7F000000000000666E507B917F666E
      396760268E670000000000008E67602E000000005A6B734EDE77BE777B6B7D6B
      C465087FA57E627EA64D954E5A6B000000000000000000000000666E507B917F
      666E1767BD770000000000000000000000002539A945A945A949C949B56F536B
      5267B56F3267C949C949A945CA452539FF7F000000000000CD72EA76D57F6F7F
      68726026204B204700002047204F602E00000000F75EB556BD7711425B6B3142
      7D67C561287FA57E627EA551D65A000000000000000000000000CD72EA76D57F
      6F7F666EF26ABD770000000000000000000025352D52A949CA4DCA4DEB4D6F5A
      F162B56F956FCA4DCA4DC9494E562535FF7F000039673967F46AA872D77F907F
      6F7F624AA032404FAE6B404FA03A0D5300000000B556F75A5B6B3A6731465A6B
      32465C67E565087FA47ED06AEF39396700000000000039673967F46AA872D77F
      907F6E7B666EF36A9C7300000000000000004539705A0C52EB4DEB51B66FB673
      3367B05E2D56EC51EB4D0C524E56A945FF7F0000676EA972A8728772D77F8F7F
      8F7F6E7F834A6026602A602E0D53000000000000524A18633967524639675246
      3A6752463B63C465757B3242F85EEE35396700000000676EA972A8728772D77F
      8F7F8F7F6E7B8872D06A7B6F000000000000BC77463DD4622D560C52546BB773
      756F966F336B0C522E56D466663DBC77FF7F0000666ED37F907F8E7F6D7B6D7B
      6D7F8E7F6E7F8876AF6E7B6F000000000000000031463963524A1963524A1963
      524A1963534A3963103EBD733142F759B36500000000666ED37F907F8E7F6D7B
      6D7B6D7B8E7F6E7F8872AD6A7B6F00000000FF7F1667463DF5664F5A2E56B162
      766FD3662E5A4F5AF56A463D1667FF7FFF7F00008B6E707B917F6D7B6D7BD77F
      D77FD67FD57FD57FA872AD6E0000000000005A6B104295529452945294529452
      945294529452955295520F3EDC725966F669000000008B6E707B917F6D7B6D7B
      D77FD77FD67FD57FD57FA872AD6E00000000FF7FFF7F1667663DF56A4F5A505E
      AF660E525277166B673D1667FF7FFF7FFF7F000013770D77B37F4B7B4C7BC872
      466E466E466E676E676E686E0000000000003146BD777B6F7B737B6F7B6F7B6B
      7B6B7B6B7B6F7B6F7B739B6F176E186E00000000000013770D77B37F4B7B4C7B
      C872466E466E466E676E676E686E00000000FF7FFF7FFF7FD462EB4D386F176B
      B47FB062757BCB49D462FF7FFF7FFF7FFF7F0000BB7FA972B67F6C7B4A7BB57F
      CC72D16AFF7F0000000000000000000000001042BC77BA32B801186B1863185F
      185F185F1863186BB801BA32BC773042000000000000BB7FA972B67F6C7B4A7B
      B57FCC72D16AFF7F00000000000000000000FF7FFF7FFF7FFF7F673D88412739
      CF66737B0B520D4EFF7FFF7FFF7FFF7FFF7F00000000676EB57F707B297B707B
      957F476E5A6B000000000000000000000000B556BC77BF577B1AD8019B7B9C73
      9C739C739B7BD8017B1ABF57BC77524A0000000000000000676EB57F707B297B
      707B957F476E5A6B00000000000000000000FF7FFF7FFF7FFF7FFF7F0E4E7377
      947BB47F1567FF7FFF7FFF7FFF7FFF7FFF7F000000008B6E717BB57F277B287B
      D77FED728B6ADE7B000000000000000000000000B5560F46BF5B7B1AD8010F4A
      10460F4AD8017B1ABF5B0F46B55A000000000000000000008B6E717BB57F277B
      287BD77FED728B6ADE7B0000000000000000FF7FFF7FFF7FFF7FFF7F463DEB4D
      505AEC51EC45FF7FFF7FFF7FFF7FFF7FFF7F0000000013730D77FA7FD97FD87F
      FA7FD87F686EF46A000000000000000000000000000000000000BF5B7B1AD901
      0000D9017B1ABF5B0000000000000000000000000000000013730D77FA7FD97F
      D87FFA7FD87F686EF46A0000000000000000FF7FFF7FFF7FFF7FFF7F596F925A
      0D4E67414639FF7FFF7FFF7FFF7FFF7FFF7F00000000BA7B686E476E476E476E
      476E686E686E696E0000000000000000000000000000000000000000BF5B7B1E
      00007B1EBF5B000000000000000000000000000000000000BA7B686E476E476E
      476E476E686E686E696E00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C733967396739679C7300000000000000000000000000000000
      00000000000000000000000000000000FF7F0000000000000000000000000000
      000000000000FE7F186318631863FE7F00000000000000000000000000000000
      000000000000000000000000000000000000000039673967BD779C7339673967
      3967396739670E6A2665276527650E669C73453A453A453A4536433614361436
      14361436143643364536453A453A453AFF7F0000000000000000000000000000
      00000000FE7F88420022002200228842FE7F3967396739673967396739673967
      39673967396739673967396739673967396700008876887A136F184A152DF52C
      F52CF6280C51466D667D667D677D47690E660532C542C542A43EE229FF77E461
      2766E461FF77E229A43EC542A53E0532FF7F0000000000000000000000000000
      0000000088428032004390630043803288425542343E343E343E343E343E343E
      343E343E343E343E343E343E343E343E55420000A87AB07F3435162D572D772D
      772D7829246D877D877D677D877D887D2665DD77EA2D222E4C3E5B63DF73A059
      2866A059DF735B634C3A222EEA2DDD77FF7F7863186318631863186318631863
      186318630022004300430000004300430022343E000000000000000000000000
      00000000000000000000000000000000343E00007677343537297831DA39FC3D
      FC3D1D3A0469F47E000000000000F57E2665FF7F353ADF77BF6FBF6FDF6F4055
      27664055DF6FBF6FBF6FDF77353AFF7FFF7F1053104310431043104310431043
      105310530022906300000000000090630022343E0000734ED55A9352314A9452
      9452945293527352B4569452734E0000343E0000184A372DB835FB3D3C67FD7F
      B356FE7FC364CB7DCA7DCA7DCA7DEB7D2665FF7F343ADF779E6BBE6F12323436
      343634361232BE6F9E6BDF77343AFF7FFF7F9042786310631053106310631063
      106318630022005300530000005300530022343E000000000000000000009C73
      00009C7300000000FF7F000000000000343E0000362DB83DFB3D3B67FB7FFC7F
      FC7FFD7F4F72476D0D7E2E7E2D7E686D5072FF7F333ADF77D852F952F9561A57
      1A571A57F956F952D852DF77333AFF7FFF7F9042786388428832786378639063
      105310538832803280539073805380328832343E0000774A3B67774E3B67774E
      3B67774E3B67FA5E56465646FA5E0000343E0000162D3A4A1C42FA7FFB7F2A21
      366BFB7FFC7F4E72E4640469256971720000FF7F343ADF779E6BF9529E6FF952
      9E6FF9529E6FF9529E6BDF77343AFF7FFF7F9042786310537863786378639042
      786378631053084300220022002208439042343E0000564AFF7F574AFE7B574A
      FE7B574ADE77574AFF7FFF7F564A0000343E000016297B521D42704EFA7F566B
      4B251567FA7F714E1F3E7D4E182100000000FF7F343EDF77D852F952F952F952
      F952F952F952F952D852DF77343EFF7FFF7F8842FE7F90427863883278639042
      7863786390427863904278631053FE7F9042343E0000574AFE7F574ADE7B574A
      DE7B574ABD77574AFE7FFE7F5646FF7F343E00001629BC5A1D42F87F356B4B29
      3567D97FD87FF87F1D42BD5A172500000000FF7F543EDF777D67D9529E6BF952
      9E6BF9529E6BD9527D67DF77543EFF7FFF7F8842FE7F90427863786378639042
      7863786390427863786378639042FE7F8842343E0000574A1A63774A1A63774A
      1A63774A1A63F95A56465646D95AFF7F343E7B6F36299C565D4A3A674B293467
      D87FD77FD67F19635D4A9C5636297B6F0000FF7F543EFF7BD84ED952D952D952
      D952D952D952D952D84EFF7B543EFF7FFF7F8842FE7F88428842906390631053
      1053104310539063906388428842FE7F8842343EFF7F9C735B6B9C735B6B9C73
      5B6B9C735B6BBD77BD77BD77BD77FF7F343ECE76EF51B939FE663E461967D67F
      0C46D57FF9621D42FE66B939EF51CE760000FF7F553EFF7B7D67D9527D67D952
      7D67D9527D67D9527D67FF7B553EFF7FFF7F8842FE7F08320832884288428842
      8842884288428842884208320832FE7F8842343E0000FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000343EA97A0A7F75313B4A3F6F9E521E42
      1E421E427E523F6F3B4A75310A7FA97A0000FF7F5542FF7BD84ED852D852D852
      D852D852D852D852D84EFF7B5542FF7FFF7F8832FE7FFE7FFE7FFE7FFE7FFE7F
      FE7FFE7FFE7FFE7FFE7FFE7FFE7FFE7F8832D856343E343E343E343E343E343E
      343E343E343E343E343E343E343E343ED856CA7A4C7F4D7F9531DA3D3F6B9F7F
      9F7F9F7F3F6BDA3D95314D7F4C7FCA7A0000FF7F7542FF7F5C63D84E5D63D84E
      5D63D84E5D63D84E5C63FF7F7542FF7FFF7F1053105378637863786378637863
      7863786378637863786378637863105310530000000000000000000000000000
      000000000000000000000000000000000000987F0B7BCF7FEF7F3052562D5629
      56295629562D3052EF7FCF7F0B7B987F0000FF7F7642FF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7F7642FF7FFF7F0000105388328832883288328832
      8832883288328832883288328832105300000000000000000000000000000000
      0000000000000000000000000000000000000000987FEA7A097F707F00000000
      000000000000707F097FEA7A987F00000000FF7F195B76467646764276427642
      764276427642764276467646195BFF7FFF7F0000396739673967396739673967
      39673967396739673967396739675A6BFF7F0000396739673967396739673967
      396739670000FF7FFF7FFF7FFF7FFF7FFF7F00000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F000000000000FF7F000000007B6F3967396739673967
      39673967396739673967396739677B6F00008C66476627664766476A466AAB39
      89412A6A466A4666266626662666696AFF7FB84A550554055405540554055405
      54055505B84AFF7FFF7FFF7FFF7FFF7FFF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000F75ED656D656D656D656
      B556B556B556B556B556B556B556F75E000047666E7F4B7F297BA772CA760C4E
      905A517F8649B07F8F7F8F7F907F2666FF7F760D7B2EBC3ABC3A9C3A9C3ABC3A
      BC3A7B2E960DFF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D65600000000307A0000
      0000000000000000000000000000D656000027664B7F097BA772E77A2666C976
      917F507F627E86498F7F6E7F907F2666FF7FF8191D4B9C327C2E7C2E7C2E7C2E
      9C321D4FB711FF7FFF7FFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000B55600000000E6700000
      0000000000000000000000000000B55600004766297BA772E776487F26668E7F
      C661087FA57E637E86458E7F907F2666FF7F39265E5FDE3AFE3EFE7FBD7BFE3E
      FE3E7E63D815FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C5FDA46845CDA46
      B846B846B846B846B846B8465C63D55600004766A772E77A487F6C7F06666D7F
      6D7FC65D097FA57E627E8649B17F466AFF7F3A227E639F67FF77FF7FDE7B9E6B
      3D535D5F1A22FF7FFF7FFF7FFF7FFF7FFF7FF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000000007710000
      0000FF7FFF7FFF7FFF7FFF7F0000D55600004766CB72266626660666886E6C7F
      4C7F6C7FC65D087FA47EB06AD031456EFF7FFF7B9C32FD4215676B62E7512E46
      F71D1726D952BD77DE7BFF7FFF7FFF7FFF7F0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63DA46845CFA46
      B84AB84AB84AB84AB846B8465C63D55600004666AB726C7F6C7F6C7F4C7F4B7F
      4B7F4C7F6C7FC5617677323EF85AEF353967FF7FFF7FBF73B2568C622A560E42
      BA42FF73B93EB84AF95A39673967BD77FF7FF2310000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D5560000FF7F0671FF7F
      FF7FFF7FFF7FFF7FFF7FFF7F0000D55600002666B27F4A7F4B7F4B7F4B7F4B7F
      4B7F4B7F4B7F6A7FF2399D733142F759B365FF7FFF7F3867EF720F73AD6A4962
      954ADA42FF7FBA42993A16221726D84EBD77F231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231F231FF7F00000000D5565C63DA46845CFA4A
      B84AB84AB84AB84AB84AB8465C63D55600002666B37F497F4A7F4A7F4A7F4A7F
      4A7F4A7F4A7F4A7F497FF139DC6E5962F665FF7FFF7FE234B47F517BEF72714E
      792E7E5FFF73FF73FF77FF779E637936D8520000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5560000FF7BE670FF7F
      FE7FDE7FDE7FDE7FDE7FDE7B0000D55600002666B37F287F297F297F297F297F
      297F297F297F297F497F477F1966196AFF7FFF7FFF7FE134AD66CF6E307B3826
      9F67DF6FDF6FDF6FDF6BDF6BDF6FBF67372AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5565C63DA46845CFA4A
      B94AB84AB84AB84AB84AB8465C63D55600002666B47F917F917F927F927F927F
      927F927F927F927F917F917FD37F2566FF7FFF7FFF7F0239C555295EC8553922
      FF73BF67BF6BBF6BBF6BBF67BF67FF733826F231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000D5560000FF77E66CFF7B
      DE7BDD7BDD7BDD7BBD7BBD7B0000D55600002666B57FC676C776C776C776C776
      C776C776C776C776C776C676D57F2666FF7FFF7FFF7F0239E555275EC5555A26
      DF739F63BF67BF67BF67BF679F63DF73382A0000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00000000D5565C63D946845CDA46
      B84AB84AB84AB84AB84AB8465C63D55600002666D67F267F277F277F277F277F
      277F277F277F277F277F267FD67F2666FF7FFF7FFF7F9C77223D63492141392A
      BF67BF679F639F639F639F63BF639F67392AF231FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FF231FF7F00000000B5560000BD73C568BD73
      9C739C739C739C739C739C730000B55600002666B07F907F907F907F907F907F
      907F907F907F907F907F907FB07F2666FF7FFF7FFF7FFF7FFF7FFF7FFF7F7E67
      9B3A9E67DF73DF73DF73DF739E679A3A7D6300000000FF7F00000000FF7F0000
      0000FF7F00000000FF7F00000000F231FF7F00000000D65600000000317A0000
      0000000000000000000000000000D6560000686A266626662666266626662666
      2666266626662666266626662666686AFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      7E637A32592E592E592E592E7A327D63FF7FF231F231FF7FF231F231FF7FF231
      F231FF7FF231F231FF7FF231F231FF7FFF7F00000000F75AD656D656D656D656
      B556B556B556B556B556B556D656F75A00000000000000000000000000000000
      0000000000009C733967396739679C7300000000396739673967396739670000
      FF7FFF7F00003967396739673967396700000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000DE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7B7B6FCA46602E602E602ECA469C73524ACE39AE35AD358D318C311042
      FF7FFF7F524ACE39AE35AD358D318C3110420000000000000000000000000000
      0000000000009C733967396739679C7300003967396739673967396739673967
      3967396739673967396739673967396739675A6B195F195F195F195F195F195F
      195F1A5FC9468036E0426E63E0428036CA46EF39F03D734A1142AE354B296C2D
      FF7FFF7FEF39F03D734A1142AE354B296C2D0000000000000000000000000000
      000000009C73CD5DC454C454C454CD5D9C73216C006C006C006C006C006C006C
      006C006C006C006C006C006C006C006C216C993A993279327932793279327932
      79329C32602EE046E0420000E0420047602ECE3939675B6B3A67396318636B2D
      FF7FFF7FCE393A675B6B3A67396318636B2D0000000000000000000000000000
      00000000AC5DE560877D877D877DE560CC5D7B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF7B7B5B9932FF7BFF77FF77FF77FF77FF77
      FF77FF7B402A8E670000000000008E67602ECE39D6563963B6565246CE396B2D
      39673967CE39D6563963B6565246CE396B2D7B6F396739673967396739673967
      396739678358887D877D677D877D887DC3540070237DC07CC07CC07CC07CC07C
      C07CC07CC07CC07CC07CC07CC07C237D00707932FF773C539E63DF6FDF6FDF6F
      DF6FFF734026204B004700002047204F602ECE39D6563963B656524ACE396B2D
      EF398C31CE39D6563963B656524ACE396C2DF152CD46CD46CD46CD46CD46CD46
      CD46EE466254F57E000000000000F57EA3547B5BFF7BFF77FF77FF77FF77FF77
      FF77FF77FF77FF77FF77FF77FF77FF775B5B7932FF77FF77DB421C4BDF6FDF6F
      DF6FFF730D4BA036404FAE6B404FA0368B32CE39D6563963B6565246CE396B29
      CE396C2DCE39D6563963B6565246CE396B2DAD46D96F115BF052325F325F325F
      325F535F6254CB7DCB7DCA7DCB7DEB7DA3540078877D807C807C807C807C807C
      807C807C807C807C807C807C807C877D00707932FF77BF6BFF7BDB42DF6FFB46
      582A592EFE4A0D4B402A4026402A0D4F9C36CE397B6FBD777B6F3967F75E4A29
      5A6B3146CE397B6FBD777B6F3967F75E6B2DAC42D96F8B3E4936B767B667535B
      115331538851E5640D7E2E7E0D7E0665A84D7401FF73BC01DF63BC01DF63BC01
      DF63BD01FF7BFF77FF77FF77FF77FF775B5B7932FF77BF67BF67FF7F1C4BFB46
      00000000FC461E4F0000DF6BDF6BFF779A32EF39AD358C316C2D6B2D6B2D3246
      5A6B3142744EAD358C316C2D6C2D6C2D524A8C42D973CE4A966796679667AD46
      96639663EE46EB5D6254625461540D62CD3E72019D2A9F677901BF6B7901BF6B
      7901BF63407C407C407C407C407CCC7D00707932FF77BF63BF633D4FDB420000
      BF67BF670000DB423D4FBF67BF63FF777932FF7FCE39F75E39679452EF3D4B29
      5A6B3146CE39F75A39679452EF3D6B2DFF7F8B3EDA73AD4695674A369567AD46
      96639663CE46B6678B36D663EE46FA738C3E5201FF7F57019E6B57019E6B5701
      9E6B5801FF7FFF77FF77FF77FF77FF775A5B7932FF77BF637E57FB42FF7FBF63
      BF63BF63BF63FF7FFB427E57BF63FF777932FF7FCE39D75A1963944EEF3D6B2D
      CE396C2DCE39F75A1963944EEF3D6C2DFF7F8B3EDB77AC429463746374638C3E
      966396638C3E946394639563AD42DB778B3E5201DC429E6736019E6B36019E6B
      36019F63007C207C207C207C007C307E00707932FF7B9F5BDB42FF779F5F9F5F
      9F5F9F5F9F5F9F5FFF77DB429F5BFF7B7932FF7FCF39F75E39679452EF3D6B2D
      EF398C31CF39F75E39679452F03D6C2DFF7F6B3ADB776A3A8B3E73637363F052
      EF4ACF46F052736373638B3E6A3ADB776B3A5201000014017E6714017E671401
      7E671501FF7FFF77FF77FF77FF77FF777B5B7932FF7BFC46DF739F5B9F5B9F5B
      9F5B9F5B9F5B9F5B9F5BDF73FC46FF7B7932FF7F9452AE358D318C2D6C2D3146
      FF7FFF7F734EAD358D318C318C31524AFF7F6A3AFC7B282E28326B3A6B3A6B3A
      6B3A6B3A6B3A6B3A6B3A2832282EFC7B6A3A72011C5700001C5300001C530000
      1C53FF7FB37EB47EB47EB47EB57ED57E006C9932FF7FFF7FFF7BFF7BFF7BFF7B
      FF7BFF7BFF7BFF7BFF7BFF7BFF7FFF7F9932FF7FFF7F3967CF393963734E6C2D
      FF7FFF7FCF393963534A6C2D3967FF7FFF7F4A36FC7BDC7BDC7BDC7BDC7BDC7B
      DC7BDC7BDC7BDC7BDC7BDC7BDC7BFC7B4A365726720152015201520152015201
      720174010074006C006C006C006C006CE7701B4F993279327932793279327932
      79327932793279327932793279329932DA42FF7FFF7FEF39324632468C316C2D
      FF7FFF7FCF39314632468D318C2DFF7FFF7FF14E115396679567956795679567
      95679567956795679567956796671153F14E0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FEF39CE35AD358D316C2D
      FF7FFF7FEF39CE35AD358D318C2DFF7FFF7F0000D14E4A364936493649364936
      4936493649364936493649364A36D14E00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005A6B
      F75EF75E5A6B00000000000000000000000000000000000000000000F75EF75E
      F75EDE7B00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B6F
      396739673967396739673967396739677B6F00005A6BF75EF75EF75EF75E734E
      10423146734EF75EF75EF75EF75E5A6B00000000BD771863F75EF75E6B2D4A29
      3146D65A1863BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B6F39673967396739673967CB51
      24412541254125412541254125412541CB4D00001042AD35AD35AD35AD35524A
      B5563967F75E524AAD35AD35AD35104200000000524A0821E71CE71CEF3D3146
      5A6B8C310821524A000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000010428C318C318C318C318D312445
      4B5EE455E459C451C451E459E4554B5E2441F75EAD353967CE39AD35DE7BBD77
      1042F75E7B6FF75E104210423146EF3D7B6F00002925EF3D31463146D65A5A6B
      5A6B94528C312925000000000000000000000000000000000000000000000000
      00000000000000009C73F75E0000000000008C31AE35CF39CF398C31AE312441
      0656824D8249413D413D8249824D06562441EF3DF75E2925EF3D8C31BD777B6F
      9C731042F75E5A6BF75E18631863734E945200006B2D945210421042EF3DCE39
      1863B55694528C31F75E00000000000000000000000000000000000000000000
      0000000000000000524A8C31F75E00000000CE39744EEF398C314A294B292445
      275AA24DA24D087F087FA24DA24D275A24410000AD356B2D10428C31BD777B6F
      7B6F7B6FEF3D396739675A6B5A6B5A6B524A0000AD35F75E524A524AFF7FBD77
      CE391863D65A94528C31F75E0000000000007B6FF75EF75EF75EF75EF75EF75E
      F75EF75EF75EF75E8C3118638C31F75E00008C3118638C31CE39C61809210441
      8C62814D814D81498149814D814D8C622441F75EAD35396731468C31BD775A6B
      5A6B7B6FEF3D7B6F7B6FAD35AD359C73524A0000EF3D5A6B9452BD770000DE7B
      5A6BCE391863B556B5568C31F75E0000000010428C318C318C318C318C318C31
      8C318C318C318C31524AF75EF75E8C31F75E1863AD35103E1863324653460341
      8D5E6C5A6C5E6C5A6C5A6C5E6C5E8D5E2441EF3DF75E2925524A8C31BD773967
      DE7B000010425A6B7B6FCE39CE397B6F734E0000CE39D65A9C735A6B524ACE39
      3146BD77EF3D5A6BCE39B5568C31F75E00008C31F75ED65AD65AD65AD65AD65A
      D65AD65AD65AD65AB556B556B55618638C310000FF7FAE35BD7BC614C614CA51
      E23CE23C0341033DE33C033DE24003412D5A0000AD356B2D734E8C31BD773967
      0000B556734E94525A6B5A6B5A6BB5561863000000009452524AD65A3146AD35
      EF3D524AB556AD359C73CE39EF3DCE3900008C315A6B39673967396739673967
      396739673967396739679452945239678C3100000000FF7F366FAD6A4A5A156B
      DE7BBE77E140DF77DF77E13C000019020000F75EAD35396794528C31BD771863
      0000AD35D65A734E524A524A734E104200000000000000005A6B734ED65A734E
      EF3D9C7300000000CE39D65A524A10420000524A8C318C318C318C318C318C31
      8C318C318C318C31314694525A6B8C310000000000005A6F31771073AD6A8C66
      3E0F7F07CC2DC040C040AC250000F7010000EF3DF75E2925B5568C31DE7B1863
      DE7B000000000000DE7B3967DE7BAD3500000000000000008C317B6FF75EB556
      9452CE39000000000000524A3146000000000000000000000000000000000000
      00000000000000008C315A6B8C3100000000000000006741B57F517BEF72CD6E
      48215F235E1F5F1F5F1F5E1B0000F60500000000AD356B2DB5568C31DE7B1863
      186318631863186318631863DE7B8C310000000000000000C618DE7B5A6B1863
      F75E841000000000000000000000000000000000000000000000000000000000
      0000000000000000734E8C3100000000000000000000E134AD6AAE6A1173CE6E
      4028000000000000000000000000F6050000F75EAD35F75EB5568C31FF7FF75E
      18636B2D314631461863F75EFF7F8C310000000000000000E71C4A29EF3DB556
      3146A51400000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000239E555295EEA510439
      802C19639C730000BF577F230000F6050000EF3DF75E0821B5568C31FF7FF75E
      F75EF75EF75EF75EF75EF75EFF7F8C310000000000000000E71CCE391042CE39
      0821A51400000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000705AE455275EE755E234
      AB411963F75ED75E9E43DC020000F60500000000AD35BD779C738C310000FF7F
      FF7F00000000FF7FFF7FFF7F0000AD3500000000000000004A29AD35EF3DAD35
      0821292500000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000004E5A624921456F5E
      0000000000000000000000000000F60500000000524AAD35AD35AD35AD358C31
      8C318C318C318C318C318C31AD35524A00000000000000009C73292529252925
      08219C7300000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000DC2E1806
      F705F605F605F605F605F605F605B93200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000039673967
      3967DE7B00000000000000000000000000000000396739673967396739673967
      3967396739673967396739673967396700000000000000000000000000003967
      3967396739670000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000DE7B396739673967CA458941
      496E166B3967DE7B000000000000000000005722F605F605F605F605F605F605
      F605F605F605F605F605F605F605F6055722000000000000000000000000EF3D
      9C73D65A10420000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000B84A5509550536012A566E5A
      517FA5515701B84A00000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6057B6F396739673967396739671042
      BD77F75A1042396739673967396739677B6F000000000000F75E396700000000
      000000000000000000000000000000000000000076095A2E9C369D32C77E4F7F
      507F627EA555990100000000000000000000F605FF7F744E3146524A5A6BFF7F
      18631863FF7F1863186318631863FF7FF6053146AD358D318D31AD31AD31AE31
      8D318D318D31AD35AD35AD35AD35AD35314600000000F75E8C31EF3D00000000
      0000000000000000000000000000000000000000D819FD467C2E7C2A7E22C365
      077FA57E627EA65139670000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF605AE3539671863185F5A6B954A0060
      954E744E744E744E744E744E744EB552AE350000F75E8C3118638C31F75EF75E
      F75EF75EF75EF75EF75EF75EF75EF75E5A6B000019263D57BD36BD36FF7FDF73
      C461087FA57E627EA6513967000000000000F605FF7F534A314231463967FF7F
      F75EF75EFF7FF75EF75EF75EF75EFF7FF605CE391863D65AD65A3967D7520060
      D752B552B552B552B552B552B552F75ECE39F75E8C31F75EF75E524A8C318C31
      8C318C318C318C318C318C318C318C31EF3D00005A2E7E67FE3EDF6FFF7FDF77
      9F5FC465087FA47ED06EEF39396700000000F605FF7FFF7FFF7FFF7FFF7FDE7B
      FF7BFF7BDE7BFF7BFF7FFF7FFF7FFF7FF605CF39185FB556B5523963185B0060
      195BF75AD75AD75AD75AD75AD75A3A67CE398C311863B556B556B556D65AD65A
      D65AD65AD65AD65AD65AD65AD65AF75E8C3100005A261D4FBF6B76778B62E751
      7056DF67E369757B3242F85EEE3539670000F605FF7F524610421142185FDE7B
      D65AD65ADE7BD65AD65AD65AD65AFF7FF605EF3D3967944E744E7B6B5A630064
      5A631963186318631863186319639C6FCF398C31396794529452396739673967
      39673967396739673967396739675A6B8C3100000000FD42BE32175F6C5EE84D
      5246BF321E471042BD733142F759B3650000F605FF7FDE7BDE7BDE7BDE77BD77
      BD77BD77BD77BD77DE77DE77BD77FF7FF605F03D5A6B534A524A7B6B9C670064
      9C675A675A675A675A673A677B6BBD77EF3D00008C315A6B945231468C318C31
      8C318C318C318C318C318C318C318C31524A0000000000007B6FAF62EF6EAD66
      4C569C73000000003042DC725962F5650000F605FF7F3146F03D103EF75ABD73
      B556B556BD73B556B556B556B556FF7FF605103E9C73524A3246DE77DE6F0064
      DE6F9C6F7C6F7C6F7C6F9C6FDE77BD77F03D000000008C315A6B8C3100000000
      000000000000000000000000000000000000000000000000CB49937F1077CE6E
      AD6A0D52000000000000386E376A00000000F605FF7F9C73BD77BD739C739C6F
      9C739C739C6F9C739C739C739C73FF7FF605B656D65A9C73DE7BFF7FFF7B0068
      FF7BDF7BDE7BDE7BDE7BDE7BDE7BB656B6560000000000008C31EF3D00000000
      000000000000000000000000000000000000000000000000C030D57F727F3073
      0F73A02C0000000000000000000000000000F605FF7FF03DCE39CF39B5567B6F
      944E944E7B6F9452945294527352FF7FF6050000534A3146314231425342006C
      7342314231423142314231423142524A00000000000000000000000000000000
      000000000000000000000000000000000000000000000000E13484492A5ACF6A
      6C5EA12C0000000000000000000000000000F605FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FF6050000000000000000000000000070
      3967396739677B6F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000239E655295EEA55
      2539C2300000000000000000000000000000F6097E5B7B1A7B1E7B1E7B1E7B1E
      7B1E7B1E7B1E7B1E7B1E7B1E7B1A7E5BF6090000000000000000000000000070
      0070007400740871000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000006741C455275EE755
      243967410000000000000000000000000000170A5D4F3D4F3D4F3D4F3D4F3D4F
      3D4F3D4F3D4F3D4F3D4F3D4F3D4F5D4F170A0000000000000000000000000070
      00700068005C0070000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000009B77444163454341
      443D9B770000000000000000000000000000992A170A160A160A160A160A160A
      160A160A160A160A160A160A160A170A992A0000000000000000000000000070
      00700070006C297500000000000000000000424D3E000000000000003E000000
      2800000040000000800000000100010000000000000400000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000C003FFFFF0000000C003FFFF08000000
      DFFBFFFF04000000D24BFFFF02000000D00BFFFF01000000D00BFE3F00800000
      D00BFC3F00400000D00BF81F00200000D00BF00F00100000D00BF00F00080000
      DFFBF00F00040000C003FFFF00020000C003FFFF00000000C003FFFF00000000
      C003FFFF00000000C003FFFF00000000FFFECF83FC7FE7FF0000C701FC3FE3FF
      0000C1018003E0FF0000C0118003E07F0000E0398003F03F0000E0118003F01F
      000080018001C00F000080038000C007000080078000C003000080070000C003
      000080070001C0030000803F0001C01F0000C03F0001E01F0000C01F8003E00F
      0000C01FF11FE00F0000C01FF93FE00FFFFFFFFFFFC1FFFEFFC1FFFF80000000
      FF80000080000000FF8000008000000000087FFE801C0000001C400280000000
      00087D6E80000000000040028001000000004002800300000000400080030000
      0000400000010000000000000001000000004002000100000000000000010000
      0000FFFF000100008001FFFF87C3000080008040DB6EC001000000000000C001
      000000000000DBFD000000008002DBFD000000000000C001000000000000DB05
      000000008002C001000000005B6CD005000000000000C001000000008002D005
      000000000000C001000000000000D005000000008002C001000000000000D005
      00000000DB6CDBFD000000000000C001FFC18241FFFFFFFF80000000FFC10000
      00000000FF80000000080000FF800000001C00000000000000080000001C0000
      0000000000000000000000000000000001900000000000000240000000000000
      000000000000000000000000000040000000000000002A000000000000000000
      000000000000FFFFFFFF00008001FFFFFC3FF87FFFFFFC008001801FFFFF0000
      8001801FFFFF00000000801FFFE700000000800FFFE300008000800700010000
      000084030000000000808001000080008100C0010000C0050101E0610001C005
      00E1E073FFE3C0058001E07FFFE7C0FD0001E07FFFFFC0250001E07FFFFFC005
      84C5E07FFFFFE1FD8001E07FFFFFF801FFFFF87F8001FC3FFFFF801F0000FC3F
      FFFF801F00000000E7FF801F00000000C7FF800F000000008000800700000000
      000080030000000000008001000000000000C001000000008000E06100000000
      C7FFE07300000000E7FFE07F00008001FFFFE07F0000FC1FFFFFE07F0000FC1F
      FFFFE07F0000FC1FFFFFE07F0000FC1F00000000000000000000000000000000
      000000000000}
  end
  object ilGridPictures: TevImageList
    DrawingStyle = dsTransparent
    Height = 32
    Width = 32
    Left = 632
    Top = 192
    Bitmap = {
      494C010101000400040020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000800000002000000001001000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      DE7BBD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77
      BD77DE7BFF7F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7F9C73
      3967F75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75EF75E
      F75E39679C73FF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000DE7B3967
      E555A155A155A155A155A155A155A155A155A155A155A155A155A155A155A155
      A155E5553967DE7B000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77E559
      656A097F097F097F097F097F097F097F097F097F097F097F097F097F097F087F
      087F656AE559BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77C259
      2A7FC67A636E216641664166416A416A416A416A416A416A416A416A626AC676
      2A7F297FC259BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77C259
      2A7FC67AE87E087F087F087F087F087F087F087F087F087F087FE87EE87EE77A
      0A7F2A7FC259BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77C259
      2B7FC67A636E216641664166416A416A416A416A416A416A416A416A626AC672
      0A7F2B7FC259BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77C259
      4C7FC67AE87E087F087F087F087F426A626E626E426A087F087F087FE87EE77A
      0A7F4C7FC259BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77C25D
      4D7FC67A636E216641664166416A626A40514051626A416A416A416A626AC672
      0A7F4D7FC25DBD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77E25D
      6E7FC67AE87E087F087F087F087F626E40494049626E087F087F087FE87EE77A
      0A7F4E7FE25DBD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77E25D
      6F7FC67A626E216641664166426A626E20452045626E426A416A416A626AC672
      097F6F7FE25DBD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77E261
      707FC676E87E087F087F087F626AC15940454045C159626E087F087FE87EE77A
      097F707FE261BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD77E261
      917FC67A626E21664166416A626E2041404540452041626E416A416A626AC672
      097F917FE261BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD770262
      927FC576E87E087F087F087F626E0039203D203D0039626E087F087FE87EE77A
      097F927F0262BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD770362
      B37FC576626E21664166416A626EA151E034E034A151626E416A416A626AC672
      097F937F0262BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000BD770366
      B47FC576E77EE87E087F087F416A626E626E626E626E426A087FE87EE87EE77A
      097FB47F0366BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000DE7B0366
      D57FC676416E206620662066206A406A406A406A406A406A206A206A416AA572
      097FD57F0366DE7B000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7F676A
      EC76D67FD67FD67FD67FD67FD67FD67FD67FD67FD67FD67FD67FD67FD67FD67F
      D67FEC76676AFF7F000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF7F
      686E026A016A016A226A236A236A236A236A236A236A236A236A026A016A016A
      226A686EFF7F0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      BD77F135FF7BD856F135BD77000000000000000000000000BD77F135BE731A63
      F135BD7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      BD77F039DF7BD75A103EBD77000000000000000000000000BD77F0399D731863
      F03DBD7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      BD77103EDE7BD65A10429C73FF7F0000000000000000FF7F9C73103E9C731863
      1042BD7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      BD771042DE7BD65A11427B6FFF7F0000000000000000FF7F7B6F31429C733967
      1142BD7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      BD771142DE7BD65A524A1863DE7BFF7F00000000FF7FDE7B1863734E9C735A6B
      3142BD7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      BD773146DE7BD65A734E94527B6FDE7BFF7FFF7FDE7B7B6F945295529C6F9C73
      3146BD7700000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      DE7B734E7B6F1963D656524AD65A5A6B7B6F7B6F5A6BD65A524A3A679C6F9C73
      734EDE7B00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF7F3967B5569D73D65AB552534A9452D65AD65A9452534AD75A5A6BDE7B185F
      3967FF7F00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF7F744E3A677B6FD65AD656944E744E744E9552185F3967BD77BD779452
      FF7F000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000DE7B534A3967BD773967185F185F186319637C6FDE7BBD779552DE7B
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DE7B9452D6565B6BDE7BDE7BDE7BDE7B9C731863B552DE7B0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF7F7B6FB656744E744E744E744EB5567B6BFF7F00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000200000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF000000000000000000000000
      FC00003F000000000000000000000000F800001F000000000000000000000000
      F800001F000000000000000000000000F800001F000000000000000000000000
      F800001F000000000000000000000000F800001F000000000000000000000000
      F800001F000000000000000000000000F800001F000000000000000000000000
      F800001F000000000000000000000000F800001F000000000000000000000000
      F800001F000000000000000000000000F800001F000000000000000000000000
      F800001F000000000000000000000000F800001F000000000000000000000000
      F800001F000000000000000000000000F800001F000000000000000000000000
      F800001F000000000000000000000000F800001F000000000000000000000000
      FC00003F000000000000000000000000FE07E07F000000000000000000000000
      FE07E07F000000000000000000000000FE03C07F000000000000000000000000
      FE03C07F000000000000000000000000FE01807F000000000000000000000000
      FE00007F000000000000000000000000FE00007F000000000000000000000000
      FE00007F000000000000000000000000FF0000FF000000000000000000000000
      FF8001FF000000000000000000000000FFC003FF000000000000000000000000
      FFE007FF00000000000000000000000000000000000000000000000000000000
      000000000000}
  end
end
