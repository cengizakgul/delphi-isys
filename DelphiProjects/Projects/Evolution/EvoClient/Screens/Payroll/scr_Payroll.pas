// Screens of "Payroll"
unit scr_Payroll;

interface

uses
  SPayrollScr,
  SPD_REDIT_WIZARD_BASE,
  SPD_REDIT_PAYROLL,
  SPD_REDIT_BATCH,
  SPD_REDIT_CHECK,
  SPD_EDIT_SELECT_TEMPLATES_TO_APPLY,
  SPD_SelectSL,
  SPD_ADD2CHECK_LINES_LOCALS,
  SPD_FORM_ADD2CHECK_LINES_LOCALS,
  SPD_EDIT_SELECT_ITEM,
  SCreateNextCheck,
  SCreateManualCheck,
  SPD_RedistrDBDT,
  SQEDGrid,
  SQEDHolder,
  sRemotePayrollUtils,
  sEDTotals,
  SPD_DBDTLookups,
  SPD_TOBalanceCheck;

implementation

end.
