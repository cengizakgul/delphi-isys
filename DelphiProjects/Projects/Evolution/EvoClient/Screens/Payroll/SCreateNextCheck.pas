// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SCreateNextCheck;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, SDataStructure,  wwdblook, EvUIComponents, SDDClasses,
  SDataDictclient, isUIwwDBLookupCombo, ISBasicClasses, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel;

type
  TCreateNextCheck = class(TForm)
    OKBtn: TevBitBtn;
    CancelBtn: TevBitBtn;
    DM_COMPANY: TDM_COMPANY;
    fpNextCheck: TisUIFashionPanel;
    lTemplate: TevLabel;
    gSalary: TevRadioGroup;
    gStandardHours: TevRadioGroup;
    lcTemplate: TevDBLookupCombo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

end.
