// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SELECT_ITEM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, EvUIComponents, ExtCtrls, StdCtrls, Buttons, menus, ActnList,
  ISBasicClasses;

type
  TEDIT_SELECT_ITEM = class(TForm)
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    evPanel3: TevPanel;
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    EvBevel1: TEvBevel;
    QPList: TevListView;
    evActionList1: TevActionList;
    OK: TAction;
    procedure OKUpdate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QPListMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure QPListDblClick(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
    procedure evBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    FSelected: TMenuItem;
  public
    { Public declarations }
    procedure RenderQPMenu( qpm: TMenuItem  );
    property Selected: TMenuItem read FSelected;
  end;


implementation

uses
  commctrl;
{$R *.DFM}

{ TEDIT_SELECT_ITEM }

procedure TEDIT_SELECT_ITEM.RenderQPMenu(qpm: TMenuItem);
var
  i, j: integer;
  s: string;
  rc: TRect;
  totalHeight: integer;
begin
  for i := 0 to qpm.Count - 1 do
    for j := 0 to qpm[i].Count-1 do
      if qpm[i].Items[j].Visible then
      begin
        with QPList.Items.Add do
        begin
          s := StripHotkey( qpm[i].Caption );
          Caption := Copy( s, 1, Pred(Pos(' ', s) ));
          SubItems.Add( Copy( s, Succ(Pos(' ', s)), Length(s) ));
          SubItems.Add( qpm[i].Items[j].Caption );
          Data := pointer(qpm[i].Items[j])
        end;
      end;

  if (QPList.Items.Count > 0) and ListView_GetItemRect( QPList.Handle, 0, rc, LVIR_BOUNDS ) then
  begin
    totalHeight := (rc.Bottom - rc.Top)*(QPList.Items.Count+1); //looks better
    if totalHeight > QPList.Height then
    begin
      if totalHeight > (Owner as TWinControl).Height then
        totalHeight := (Owner as TWinControl).Height;

      Height := Height - QPList.Height + totalHeight + GetSystemMetrics( SM_CYHSCROLL)*2; //assume that headers have the same metrics
      //Elegant code, but the resulting window has gotten too tall, taller than
      //the parent window. Reduced per Jake.
      if Height > 450 then Height := 450;
    end;
  end;
  for i := 0 to QPList.Columns.Count - 1 do
    QPList.Columns[i].Width := QPList.Columns[i].MinWidth;
end;

procedure TEDIT_SELECT_ITEM.OKUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := QPList.Selected <> nil;
end;

procedure TEDIT_SELECT_ITEM.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if QPList.Selected <> nil then
    FSelected := TMenuItem(QPList.Selected.Data)
  else
    FSelected := nil;
end;

procedure TEDIT_SELECT_ITEM.QPListMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
//  if ( Button = mbLeft ) and ( ssDouble in Shift) and ( QPList.GetItemAt(x,y) <> nil ) then
//    OK.Execute;
end;

procedure TEDIT_SELECT_ITEM.QPListDblClick(Sender: TObject);
{var
  p: TPoint;}
begin
{  if GetCursorPos( p ) then
  begin
    p := QPList.ScreenToClient( p );
    if ( QPList.GetItemAt(p.x,p.y) <> nil ) then
      OK.Execute;
  end;}
end;

procedure TEDIT_SELECT_ITEM.evBitBtn1Click(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TEDIT_SELECT_ITEM.evBitBtn2Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
