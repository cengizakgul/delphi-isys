// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SELECT_TEMPLATES_TO_APPLY;

interface

uses
   StdCtrls, Buttons, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Forms, SDataStructure, Db, Wwdatsrc, EvUIComponents, SDDClasses,
  SDataDictclient, ISBasicClasses, LMDCustomButton, LMDButton,
  isUILMDButton, ExtCtrls, isUIFashionPanel;

type
  TEDIT_SELECT_TEMPLATES_TO_APPLY = class(TForm)
    evDataSource1: TevDataSource;
    DM_COMPANY: TDM_COMPANY;
    pnlTemplate: TisUIFashionPanel;
    wwDBGrid1: TevDBGrid;
    BitBtn1: TevBitBtn;
    BitBtn2: TevBitBtn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TEDIT_SELECT_TEMPLATES_TO_APPLY.FormCreate(Sender: TObject);
begin
  with DM_COMPANY.CO_PR_CHECK_TEMPLATES do
    BitBtn1.Enabled := not (EOF and BOF);
end;

initialization
  RegisterClass(TEDIT_SELECT_TEMPLATES_TO_APPLY);

end.
