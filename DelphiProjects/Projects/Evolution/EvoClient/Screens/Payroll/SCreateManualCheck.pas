// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SCreateManualCheck;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls,  EvUtils, EvUIUtils, EvUIComponents, isUIEdit,
  ISBasicClasses, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel;

type
  TCreateManualCheck = class(TForm)
    OKBtn: TevBitBtn;
    CancelBtn: TevBitBtn;
    fpManualCheck: TisUIFashionPanel;
    gType: TevRadioGroup;
    lNumber: TevLabel;
    eNumber: TevEdit;
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CreateManualCheck: TCreateManualCheck;

implementation

{$R *.DFM}

procedure TCreateManualCheck.OKBtnClick(Sender: TObject);
begin
  if Trim(eNumber.Text) = '' then
  begin
    ModalResult := mrNone;
    EvErrMessage('Please, enter check number');
  end;
end;

end.
