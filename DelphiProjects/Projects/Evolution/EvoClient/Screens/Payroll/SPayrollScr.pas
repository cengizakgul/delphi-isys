// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPayrollScr;

interface

uses
  Windows, SPackageEntry, ComCtrls,  StdCtrls, Graphics, STimeOffUtils,
  Controls, Buttons, Classes, ExtCtrls, Db, SysUtils, Wwdatsrc, SDataStructure, EvBasicUtils,
  EvUtils, EvConsts, SPopulateRecords, Dialogs, EvContext, isBasicUtils,
  Forms, Menus, ImgList, ToolWin,  EvTypes, ActnList, Variants, SSecurityInterface, isVCLBugFix,
  ISBasicClasses, sQEDHolder, EvStreamUtils, EvCommonInterfaces, EvMainBoard, EvExceptions, EvDataAccessComponents, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  DS_ROW_TYPES = (
    DS_FED_OASDI_TAX,
    DS_FED_OASDI_WAGES,
    DS_FED_OASDI_TIPS,
    DS_FED_EE_MEDICARE_WAGES,
    DS_FED_EE_MEDICARE_TAX,
    DS_FED_SHORTFALL,
    DS_FED_WAGES,
    DS_FED_TAX,
    DS_FED_EE_EIC_TAX,
    DS_FED_OR_CHECK_BACK_UP_WITHHOLDING,
    DS_STATE_WAGE,
    DS_STATE_SHORTFALL,
    DS_STATE_TAX,
    DS_STATE_EE_SDI_WAGE,
    DS_STATE_EE_SDI_SHORTFALL,
    DS_STATE_EE_SDI_TAX,
    DS_STATE_SUI_WAGE,
    DS_STATE_SUI_TAX,
    DS_LOCAL_WAGE,
    DS_LOCAL_TAX,
    DS_LOCAL_SHORTFALL
    );

  TPayrollFrm = class(TFramePackageTmpl, IevPayrollScreens)
    procedure btnRefreshClick(Sender: TObject);
  private
    FCheckToaSetupAccess: TPayrollEntryCheckToaSetupAccess;
    FCheckToaSetupAccessActivated: Boolean;

    procedure QuickPayDSFieldChange(Sender: TField);
    procedure QuickPayDSBeforeInsert(DataSet: TDataSet);
    procedure QuickPayDSBeforeEdit(DataSet: TDataSet);
    procedure QuickPayDSBeforePost(DataSet: TDataSet);
    procedure QuickPayDSBeforeDelete(DataSet: TDataSet);
    procedure QuickPayDSAfterDelete(DataSet: TDataSet);
    procedure QuickPayDSAfterPost(DataSet: TDataSet);
    procedure QuickPayDSAfterCancel(DataSet: TDataSet);
    procedure QuickPayDSCalculate(DataSet: TDataSet);
    procedure InitVars;
  protected
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function ActivatePackage(WorkPlace: TWinControl): Boolean; override;
    function DeactivatePackage: Boolean; override;
    function InitPackage: Boolean; override;
    function UninitPackage: Boolean; override;
    function  PackageBitmap: TBitmap; override;
    procedure OpenPayroll;
    function  SilentCheckToaBalance: Boolean;
    procedure CheckToaBalance;
    procedure ClearCheckToaSetupAccess;

  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
  end;

  Batch_E_D = record
    Description: string;
    FieldType: string; {Hours or Amount}
  end;

var
  OldValues: Variant;
  Batch_E_DS: array of Batch_E_D;
  DetailPayrollOpen: Integer;
  SeparateTax: Boolean;
  CurrentTabSheetName: string;
  CurrentBatchTabSheet: string;
  Current_FromDate: TDateTime;
  ED_TOTAL_CALCULATED: Boolean;
  TAX_TOTAL_CALCULATED: Boolean;
  BATCH_QUICK_PAY_CALCULATED: Boolean;
  CalcForBatch: Boolean;
  bNewPayroll: Boolean;
  bInsertWithWizard: Boolean;
  bInsertWithCobraWizard: Boolean;
  DeletingCheck: Boolean;

  dsED_Detail: TevDataSource;
  dsTax_Detail: TevDataSource;
  dsTax_Total: TevDataSource;
  dsED_Total: TevDataSource;
  QUICK_PAY_DS: TevDataSource;

  QEDHolder: TQEDHolder;


function SeparateEDByLineType: Boolean;
function SeparateEDByCheckType: Boolean;

function ReplaceChars(Text: string): string;
procedure ResetCalc;
function Calculate_TAX_TOTALS(Separate: boolean): Boolean;
function Calculate_ED_TOTALS(sepByLineType, sepByCheckType: boolean): Boolean;
function Calculate_BATCH_QuickPay: boolean;
procedure AddCheckNbrToLists(CheckNbr: Integer);

procedure ReCalculate_TAX_TOTALS(Separate: boolean);
procedure ReCalculate_ED_TOTALS(sepByLineType, sepByCheckType: boolean);

procedure ClearQPColumns;
procedure AddQPColumn(sType, AH: string);
procedure DeleteQPColumn(sType, AH: string);

procedure OpenDetailPayroll;
procedure CloseDetailPayroll;
function GetPayrollDescription: string;
function GetBatchDescription: string;

function HasBeenPreProcessed: boolean;
//has the current payroll ever been Pre-Processed (Status = Y)
function IsEDInGroup(CL_E_Ds_Group_NBR, CL_E_Ds_NBR: string): boolean;
function GetEESalaryAmount(EE_NBR: string): Double;
function IsRegular(CL_E_Ds_NBR: string): boolean;

function MoveToPR_CHECK_LOCALS(CO_LOCAL_TAX_NBR: integer): boolean;
function MoveToPR_CHECK_STATES(CO_STATES_NBR: integer): boolean;
function MoveToPR_CHECK_SUI(CO_SUI_NBR: integer): boolean;

procedure SetDetailPayrollOpen(PrNbr: Integer);
procedure CheckSupportDatasets(const Cl_Nbr, Co_Nbr: Integer);
function GetRemoteDataSetConditions(sName: string; Co_Nbr: Integer): string;

function CheckUplinkedRetrieveCondition(const Name: string; const OriginalCondition: string): string;

procedure AddState;
procedure AddLocal;

procedure CheckStatus;

procedure SaveLastCheckPosition(PrCheckNbr: Integer);
procedure RestoreLastCheckPosition;

const
  QPFieldCount = 17;

implementation

uses SPD_REDIT_WIZARD_BASE, StrUtils, DateUtils, SDDClasses, sRemotePayrollUtils, SPD_DM_HISTORICAL_TAXES, evDataSet;

{$R *.DFM}

var
  RemotePayrollFrm: TPayrollFrm;
  EDRefreshList, TaxRefreshList, CheckRefreshList, QBRefreshList: TStringList;
  Payroll_TAX_Master, Payroll_TAX_Detail, Payroll_ED_Master, Payroll_ED_Detail: TevClientDataSet;
  QuickPayDS: TevClientDataSet;
  ED_Master_ID: integer;
  TAX_Master_ID: integer;

  ED_BATCH_NBR: Integer;
  FSeparateEDByLineType: boolean;
  FSeparateEDByCheckType: boolean;

  TAX_BATCH_NBR: Integer;
  QUICK_PAY_BATCH_NBR: Integer;
  ClNbr, CoNbr: Integer;

  OASDI_MEDICARE_Readonly: boolean;

//  sFileName: string;
  sFilterString: string;


function GetPayrollScreens: IevPayrollScreens;
begin
  if not Assigned(RemotePayrollFrm) then
    RemotePayrollFrm := TPayrollFrm.Create(nil);
  Result := RemotePayrollFrm;
end;


function SeparateEDByLineType: Boolean;
begin
  Result := FSeparateEDByLineType;
end;

function SeparateEDByCheckType: Boolean;
begin
  Result := FSeparateEDByCheckType;
end;

function ReplaceChars(Text: string): string;
var
  i: Integer;
begin
  Text := Copy(Text, 1, 27);
  for i := 1 to Length(Text) do
    if IsDelimiter(' -+/*()', Text, i) then
      Text[i] := '_';
  Result := Text;
end;

procedure SaveLastCheckPosition(PrCheckNbr: Integer);
begin
  if (PrCheckNbr > 0) and (DetailPayrollOpen <> 0) and
     DM_COMPANY.CO.Active and (DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger > 0) and
     DM_PAYROLL.PR.Active and (DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger > 0) and
     (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PENDING) then
    mb_AppSettings.AsInteger['Misc\LastCheckPosition\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
      IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger) + '\' +
      IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger)] := PrCheckNbr;
end;

procedure RestoreLastCheckPosition;
var
  PrCheckNbr: Integer;
begin
  if DM_COMPANY.CO.Active and (DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger > 0) and
     DM_PAYROLL.PR.Active and (DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger > 0) and
     DM_PAYROLL.PR_BATCH.Active and
     DM_PAYROLL.PR_CHECK.Active then
    if DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PENDING then
    begin
      PrCheckNbr := mb_AppSettings.GetValue('Misc\LastCheckPosition\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
        IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger) + '\' + IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger),
        DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger);
      if PrCheckNbr > 0 then
      begin
        DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', PrCheckNbr, []);
        DM_PAYROLL.PR_BATCH.Locate('PR_BATCH_NBR', DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'], []);
      end;
    end
    else
      mb_AppSettings['Misc\LastCheckPosition\CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' +
        IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger) + '\' +
        IntToStr(DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger)] := '';
end;

procedure SetDetailPayrollOpen(PrNbr: Integer);
begin
  DetailPayrollOpen := PrNbr;
  if (PrNbr <> 0) and DM_COMPANY.CO.Active then
  begin
    ClNbr := DM_COMPANY.CO.ClientID;
    CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
  end
  else
  begin
    ClNbr := 0;
    CoNbr := 0;
  end;
end;

procedure AddLocal;
var
  sFilter: string;
  bFiltered: Boolean;
  Nbr, Nbr1: Integer;
begin
  sFilter := DM_PAYROLL.PR_CHECK_LOCALS.Filter;
  bFiltered := DM_PAYROLL.PR_CHECK_LOCALS.Filtered;
  Nbr := 0;
  try
    Nbr1 := DM_EMPLOYEE.EE_LOCALS.RecNo;
    DM_PAYROLL.PR_CHECK_LOCALS.Filter := 'PR_CHECK_NBR = ' + DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
    DM_PAYROLL.PR_CHECK_LOCALS.Filtered := True;
    DM_EMPLOYEE.EE_LOCALS.RecNo := Nbr1;
    if not DM_PAYROLL.PR_CHECK_LOCALS.Locate('EE_LOCALS_NBR', DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'], []) then
    begin
      DM_PAYROLL.PR_CHECK_LOCALS.Insert;
      DM_PAYROLL.PR_CHECK_LOCALS['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
      DM_PAYROLL.PR_CHECK_LOCALS['EE_LOCALS_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'];
      DM_PAYROLL.PR_CHECK_LOCALS['EXCLUDE_LOCAL'] := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK_LOCALS.Post;
    end;
    Nbr := DM_PAYROLL.PR_CHECK_LOCALS['PR_CHECK_LOCALS_NBR'];
  finally
    DM_PAYROLL.PR_CHECK_LOCALS.Filter := sFilter;
    DM_PAYROLL.PR_CHECK_LOCALS.Filtered := bFiltered;
    DM_PAYROLL.PR_CHECK_LOCALS.Locate('PR_CHECK_LOCALS_NBR', Nbr, []);
  end;
end;

procedure AddState;
var
  sFilter1, sFilter2: string;
  bFiltered1, bFiltered2: Boolean;
  Nbr, Nbr1: Integer;
begin
  sFilter1 := DM_PAYROLL.PR_CHECK_STATES.Filter;
  bFiltered1 := DM_PAYROLL.PR_CHECK_STATES.Filtered;
  sFilter2 := DM_PAYROLL.PR_CHECK_SUI.Filter;
  bFiltered2 := DM_PAYROLL.PR_CHECK_SUI.Filtered;
  Nbr := 0;
  try
    Nbr1 := DM_EMPLOYEE.EE_STATES.RecNo;
    DM_PAYROLL.PR_CHECK_STATES.Filter := 'PR_CHECK_NBR = ' + DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
    DM_PAYROLL.PR_CHECK_STATES.Filtered := True;
    DM_PAYROLL.PR_CHECK_SUI.Filter := 'PR_CHECK_NBR = ' + DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
    DM_PAYROLL.PR_CHECK_SUI.Filtered := True;
    DM_EMPLOYEE.EE_STATES.RecNo := Nbr1;
    if not DM_PAYROLL.PR_CHECK_STATES.Locate('EE_STATES_NBR', DM_EMPLOYEE.EE_STATES['EE_STATES_NBR'], []) then
    begin
      DM_PAYROLL.PR_CHECK_STATES.Insert;
      DM_PAYROLL.PR_CHECK_STATES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
      DM_PAYROLL.PR_CHECK_STATES['EE_STATES_NBR'] := DM_EMPLOYEE.EE_STATES['EE_STATES_NBR'];
      DM_PAYROLL.PR_CHECK_STATES['TAX_AT_SUPPLEMENTAL_RATE'] := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK_STATES['EXCLUDE_STATE'] := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK_STATES['EXCLUDE_SDI'] := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK_STATES['EXCLUDE_SUI'] := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK_STATES['EXCLUDE_ADDITIONAL_STATE'] := GROUP_BOX_NO;
      DM_PAYROLL.PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_NONE;
      DM_PAYROLL.PR_CHECK_STATES.Post;
    end;
    Nbr := DM_PAYROLL.PR_CHECK_STATES['PR_CHECK_STATES_NBR'];

    DM_COMPANY.CO_SUI.First;
    while not DM_COMPANY.CO_SUI.Eof do
    begin
      if (DM_COMPANY.CO_SUI['CO_STATES_NBR'] = DM_EMPLOYEE.EE_STATES['SUI_APPLY_CO_STATES_NBR']) and
         not DM_PAYROLL.PR_CHECK_SUI.Locate('CO_SUI_NBR', DM_COMPANY.CO_SUI['CO_SUI_NBR'], []) then
      begin
        DM_PAYROLL.PR_CHECK_SUI.Insert;
        DM_PAYROLL.PR_CHECK_SUI['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
        DM_PAYROLL.PR_CHECK_SUI['CO_SUI_NBR'] := DM_COMPANY.CO_SUI['CO_SUI_NBR'];
        DM_PAYROLL.PR_CHECK_SUI.Post;
      end;
      DM_COMPANY.CO_SUI.Next;
    end;
  finally
    DM_PAYROLL.PR_CHECK_STATES.Filter := sFilter1;
    DM_PAYROLL.PR_CHECK_STATES.Filtered := bFiltered1;
    DM_PAYROLL.PR_CHECK_SUI.Filter := sFilter2;
    DM_PAYROLL.PR_CHECK_SUI.Filtered := bFiltered2;
    DM_PAYROLL.PR_CHECK_STATES.Locate('PR_CHECK_STATES_NBR', Nbr, []);
  end;
end;

function GetRemoteDataSetConditions(sName: string; Co_Nbr: Integer): string;
begin
  if (Copy(sName, 1, 3) <> 'EE_') and
     (Copy(sName, 1, 2) <> 'CL') and
     (sName <> 'CO_BRANCH') and
     (sName <> 'CO_DEPARTMENT') and
     (sName <> 'CO_TEAM') then
    Result := 'CO_NBR = ' + IntToStr(Co_Nbr)
  else
    Result := '';
end;

function CheckUplinkedRetrieveCondition(const Name: string; const OriginalCondition: string): string;
begin
  if ctx_PayrollCalculation.GetEeFilter = '' then
    Result := OriginalCondition
  else
    if Name = 'PR_CHECK' then
      Result := UplinkRetrieveCondition('EE_NBR', DM_EMPLOYEE.EE, OriginalCondition)
    else if Name = 'PR_CHECK_LINES' then
      Result := UplinkRetrieveCondition('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK)
    else if Name = 'PR_CHECK_LINE_LOCALS' then
      Result := UplinkRetrieveCondition('PR_CHECK_LINES_NBR', DM_PAYROLL.PR_CHECK_LINES)
    else if Name = 'PR_CHECK_LOCALS' then
      Result := UplinkRetrieveCondition('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK)
    else if Name = 'PR_CHECK_STATES' then
      Result := UplinkRetrieveCondition('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK)
    else if Name = 'PR_CHECK_SUI' then
      Result := UplinkRetrieveCondition('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK)
    else if Name = 'EE' then
      Result := ctx_PayrollCalculation.GetEeFilter
    else
      Result := OriginalCondition;
end;

procedure CheckSupportDatasets(const Cl_Nbr, Co_Nbr: Integer);
  procedure CheckAndAddDs(const ds: TevClientDataSet; var a: TArrayDS; const ExtraFilter: string = '');
  var
    s: string;
  begin
    s := GetRemoteDataSetConditions(ds.TableName, Co_Nbr);
    if ExtraFilter <> '' then
      if s = '' then
        s := ExtraFilter
      else
        s := s+ ' and '+ ExtraFilter;
    ds.CheckDSConditionAndClient(ctx_DataAccess.ClientID, s);
    AddDS(ds, a);
  end;
  procedure CheckAndAddChildDs(const ds: TevClientDataSet; const UpLinkFieldName: string; const UpLinkDataSet: TevClientDataSet; var a: TArrayDS);
  begin
    ds.CheckChildDSAndClient(ctx_DataAccess.ClientId, UpLinkFieldName, UpLinkDataSet);
    AddDS(ds, a);
  end;
var
  aDS: TArrayDS;
  bMultipleCompanies: Boolean;
  OrigOpenCondition : String;
begin
  ctx_DataAccess.OpenClient(Cl_Nbr);
  with DM_TEMPORARY.TMP_CO.ShadowDataSet do
  try
    SetRange([Cl_Nbr], [Cl_Nbr]);
    bMultipleCompanies := RecordCount > 1;
  finally
    CancelRange;
  end;
  CheckAndAddDs(DM_CLIENT.CL, aDS);
  CheckAndAddDs(DM_CLIENT.CL_E_DS, aDS);
  CheckAndAddDs(DM_CLIENT.CL_PIECES, aDS);
  CheckAndAddDs(DM_CLIENT.CL_AGENCY, aDS);
  CheckAndAddDs(DM_COMPANY.CO, aDS);
  CheckAndAddDs(DM_COMPANY.CO_STATES, aDS);
  CheckAndAddDs(DM_COMPANY.CO_SUI, aDS);
  CheckAndAddDs(DM_COMPANY.CO_PR_CHECK_TEMPLATES, aDS);
  CheckAndAddDs(DM_COMPANY.CO_PR_FILTERS, aDS);
  CheckAndAddDs(DM_COMPANY.CO_DIVISION, aDS);
  CheckAndAddDs(DM_COMPANY.CO_PHONE, aDS);

  if bMultipleCompanies then
  begin
    CheckAndAddChildDs(DM_COMPANY.CO_BRANCH, 'CO_DIVISION_NBR', DM_COMPANY.CO_DIVISION, aDS);
    CheckAndAddChildDs(DM_COMPANY.CO_DEPARTMENT, 'CO_BRANCH_NBR', DM_COMPANY.CO_BRANCH, aDS);
    CheckAndAddChildDs(DM_COMPANY.CO_TEAM, 'CO_DEPARTMENT_NBR', DM_COMPANY.CO_DEPARTMENT, aDS);
  end
  else
  begin
    CheckAndAddDs(DM_COMPANY.CO_BRANCH, aDS);
    CheckAndAddDs(DM_COMPANY.CO_DEPARTMENT, aDS);
    CheckAndAddDs(DM_COMPANY.CO_TEAM, aDS);
  end;
  CheckAndAddDs(DM_COMPANY.CO_JOBS, aDS);
  CheckAndAddDs(DM_COMPANY.CO_SHIFTS, aDS);
  CheckAndAddDs(DM_COMPANY.CO_WORKERS_COMP, aDS);
  CheckAndAddDs(DM_COMPANY.CO_E_D_CODES, aDS);
  CheckAndAddDs(DM_COMPANY.CO_LOCAL_TAX, aDS);
  CheckAndAddDs(DM_PAYROLL.PR, aDS);
  CheckAndAddDs(DM_PAYROLL.PR_SCHEDULED_EVENT, aDS);
  CheckAndAddDs(DM_EMPLOYEE.EE, aDS, ctx_PayrollCalculation.GetEeFilter);
  //this routine gets called several times durinb the payroll wizard
  //unfortunately, about 1/2 way through, the ctx_dataaccess.opendatasets()
  //overwrites the OpenCondition with the RetreiveCondition, which is not desired.
  //store it here.
  OrigOpenCondition := DM_PAYROLL.PR.OpenCondition;
  if bMultipleCompanies or (ctx_PayrollCalculation.GetEeFilter <> '') then
  begin
    CheckAndAddChildDs(DM_CLIENT.CL_PERSON, 'CL_PERSON_NBR', DM_EMPLOYEE.EE, aDS);
    CheckAndAddChildDs(DM_EMPLOYEE.EE_STATES, 'EE_NBR', DM_EMPLOYEE.EE, aDS);
    CheckAndAddChildDs(DM_EMPLOYEE.EE_LOCALS, 'EE_NBR', DM_EMPLOYEE.EE, aDS);
    CheckAndAddChildDs(DM_EMPLOYEE.EE_RATES, 'EE_NBR', DM_EMPLOYEE.EE, aDS);
    CheckAndAddChildDs(DM_EMPLOYEE.EE_SCHEDULED_E_DS, 'EE_NBR', DM_EMPLOYEE.EE, aDS);
    CheckAndAddChildDs(DM_EMPLOYEE.EE_WORK_SHIFTS, 'EE_NBR', DM_EMPLOYEE.EE, aDS);
  end
  else
  begin
    CheckAndAddDs(DM_CLIENT.CL_PERSON, aDS);
    CheckAndAddDs(DM_EMPLOYEE.EE_STATES, aDS);
    CheckAndAddDs(DM_EMPLOYEE.EE_LOCALS, aDS);
    CheckAndAddDs(DM_EMPLOYEE.EE_RATES, aDS);
    CheckAndAddDs(DM_EMPLOYEE.EE_SCHEDULED_E_DS, aDS);
    CheckAndAddDs(DM_EMPLOYEE.EE_WORK_SHIFTS, aDS);
  end;
  if not DM_COMPANY.CO.Active then
    SetDetailPayrollOpen(0);
  //reset the Retrieve condition to the Original Open Condition so the Opendatasets() does not fail.
  if OrigOpenCondition <> '' then
    DM_PAYROLL.PR.RetrieveCondition := OrigOpenCondition;
  ctx_DataAccess.OpenDataSets(aDS);
end;

function MoveToPR_CHECK_SUI(CO_SUI_NBR: integer): boolean;
begin
  result := false;
end;

function MoveToPR_CHECK_STATES(CO_STATES_NBR: integer): boolean;
var
  TempNbr: variant;
begin
  TempNbr := DM_EMPLOYEE.EE_STATES.Lookup('CO_STATES_NBR;EE_NBR', VarArrayOf([CO_STATES_NBR,
    DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value]), 'EE_STATES_NBR');

  if VarIsNull(TempNbr) then
    result := False
  else
  begin
    with DM_PAYROLL.PR_CHECK_STATES do
      if not (Locate('EE_STATES_NBR', TempNbr, [])) then
      begin
        Insert;
        FieldByName('EE_STATES_NBR').Value := TempNbr;
        FieldByName('PR_CHECK_NBR').Value := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
      end;
    result := True;
  end;
end;

function MoveToPR_CHECK_LOCALS(CO_LOCAL_TAX_NBR: integer): boolean;
var
  TempNbr: variant;
begin
  TempNbr := DM_EMPLOYEE.EE_LOCALS.Lookup('CO_LOCAL_TAX_NBR;EE_NBR', VarArrayOf([CO_LOCAL_TAX_NBR,
    DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value]), 'EE_LOCALS_NBR');
  if VarIsNull(TempNbr) then
    result := False
  else
  begin
    with DM_PAYROLL.PR_CHECK_LOCALS do
      if not (Locate('EE_LOCALS_NBR', TempNbr, [])) then
      begin
        Insert;
        FieldByName('EE_LOCALS_NBR').Value := TempNbr;
        FieldByName('PR_CHECK_NBR').Value := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
      end;
    result := True;
  end;
end;

procedure CheckStatus;
begin
  with RemotePayrollFrm do
  begin
    if ButtonEnabled(NavOK) then
      ClickButton(NavOK);

    if ButtonEnabled(NavCommit) then
      case EvMessage('Save changes in payroll?', mtConfirmation, mbYesNoCancel) of
      mrYes:
        ClickButton(NavCommit);
      mrNo:
        if EvMessage('You are about to lose your changes', mtConfirmation, mbOKCancel) <> mrOK then
          AbortEx;
      mrCancel:
        AbortEx;
      end;
  end;
end;

procedure CloseDetailPayroll;
begin
  DM_PAYROLL.PR_BATCH.Close;
  DM_PAYROLL.PR_CHECK.Close;
  DM_PAYROLL.PR_CHECK_LINES.Close;
  DM_PAYROLL.PR_CHECK_LINE_LOCALS.Close;
  DM_PAYROLL.PR_CHECK_LOCALS.Close;
  DM_PAYROLL.PR_CHECK_STATES.Close;
  DM_PAYROLL.PR_CHECK_SUI.Close;
  DM_COMPANY.EE_LOCALS.Close;
  DM_HISTORICAL_TAXES.EE_LOCALS.Close;
  SetDetailPayrollOpen(0);
  FiWizardPosition := WIZ_UNKNOWN;
  with RemotePayrollFrm do
    if Assigned(ActiveFrame) and
       (ActiveFrame is TREDIT_WIZARD_BASE) then
      TREDIT_WIZARD_BASE(ActiveFrame).CheckWizardButton;
  QUICK_PAY_DS.DataSet := nil;
  QEDHolder.InvalidateAll;

end;

procedure OpenDetailPayroll;
var
  oldPrNbr, OldCheckNbr : integer;
begin
  GetPayrollScreens.ClearCheckToaSetupAccess;
  DM_PAYROLL.PR_CHECK.DisableControls;
  oldPrNbr := DM_PAYROLL.PR_CHECK.FieldByName('PR_NBR').AsInteger;
  OldCheckNbr := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;
  with DM_PAYROLL do
  try
    ctx_PayrollCalculation.OpenDetailPayroll(PR.FieldByName('PR_NBR').AsInteger);
    if DetailPayrollOpen <> PR.FieldByName('PR_NBR').AsInteger then
      ResetCalc;
    SetDetailPayrollOpen(PR.FieldByName('PR_NBR').AsInteger);
    if ctx_DataAccess.RecalcLineBeforePost then
      ctx_PayrollCalculation.GetLimitedEDs(PR.FieldByName('CO_NBR').AsInteger, DetailPayrollOpen,
                       GetBeginYear(PR.FieldByName('CHECK_DATE').AsDateTime), PR.FieldByName('CHECK_DATE').AsDateTime);
    if PR_BATCH.IndexDefs.IndexOf('SORT') < 0 then
    begin
      PR_BATCH.AddIndex('SORT', 'PERIOD_BEGIN_DATE', [ixCaseInsensitive], 'PERIOD_BEGIN_DATE'); //gdy23
      PR_BATCH.IndexDefs.Update;
    end;

   if  DM_PAYROLL.PR.Active and DM_PAYROLL.PR.Locate('PR_NBR', oldPrNbr, []) and (oldPrNbr > 0) and
        DM_PAYROLL.PR_BATCH.Active and DM_PAYROLL.PR_CHECK.Active and
       (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PENDING) and
        (OldCheckNbr > 0 ) then
   begin
        DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', OldCheckNbr, []);
        DM_PAYROLL.PR_BATCH.Locate('PR_BATCH_NBR', DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'], []);
   end;

    CheckEEStatesForSUI;
  finally
    DM_PAYROLL.PR_CHECK.EnableControls;
  end;
  QUICK_PAY_DS.DataSet := nil;
  QEDHolder.InvalidateAll;
end;

function GetBatchDescription: string;
begin
  with DM_PAYROLL.PR_BATCH do
    Result := FieldByName('PERIOD_BEGIN_DATE').AsString + ' - ' + FieldByName('PERIOD_END_DATE').AsString;
end;

function GetPayrollDescription: string;
begin
  if DetailPayrollOpen <> 0 then
    Result := '#' + DM_PAYROLL.PR.FieldByName('RUN_NUMBER').AsString + ' ' +
                    DateToStr(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime)
  else
    Result := '';
end;

function HasBeenPreProcessed: boolean;
begin
  Result := False;
  if DetailPayrollOpen <> 0 then
  begin
    ctx_StartWait;
    try
      with TExecDSWrapper.Create('GenericSelectWithCondition') do
      begin
        SetMacro('COLUMNS', 'Count(STATUS)');
        SetMacro('TABLENAME', 'PR');
        SetMacro('Condition', 'PR_NBR=:RecordNbr and STATUS=:Status');
        SetParam('RecordNbr', DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger);
        SetParam('Status', 'Y');
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
      end;
      ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
      if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
        Result := True;

      ctx_DataAccess.CUSTOM_VIEW.Close;
    finally
      ctx_EndWait;
    end;
  end;
end;

function IsEDInGroup(CL_E_Ds_Group_NBR, CL_E_Ds_NBR: string): boolean;
begin
  Result := False;
  try
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 'Count(CL_E_D_GROUP_CODES_NBR)');
      SetMacro('TABLENAME', 'CL_E_D_GROUP_CODES');
      SetMacro('Condition', 'CL_E_DS_NBR=:ClEDsNbr and CL_E_D_GROUPS_NBR=:ClEDGroupsNbr');
      if Trim(CL_E_Ds_NBR) = '' then
        SetParam('ClEDsNbr', '-1')
      else
        SetParam('ClEDsNbr', CL_E_Ds_NBR);
      if Trim(CL_E_Ds_Group_NBR) = '' then
        SetParam('ClEDGroupsNbr', '-1')
      else
        SetParam('ClEDGroupsNbr', CL_E_Ds_Group_NBR);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      Result := True;
  finally
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;
end;

function GetEESalaryAmount(EE_NBR: string): Double;
begin
  try
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 'SALARY_AMOUNT');
      SetMacro('TABLENAME', 'EE');
      SetMacro('Condition', 'EE_NBR=:EeNbr');
      SetParam('EeNbr', EE_NBR);
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    Result := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsFloat;
  finally
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;
end;

function IsRegular(CL_E_Ds_NBR: string): boolean;
begin
  Result := False;
  try
    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('COLUMNS', 'Count(CL_E_DS_NBR)');
      SetMacro('TABLENAME', 'CL_E_DS');
      SetMacro('Condition', 'CL_E_DS_NBR=:ClEDsNbr and E_D_CODE_TYPE=:EDCodeType');
      if Trim(CL_E_Ds_NBR) = '' then
        SetParam('ClEDsNbr', '-1')
      else
        SetParam('ClEDsNbr', CL_E_Ds_NBR);
      SetParam('EDCodeType', 'EB');
      ctx_DataAccess.CUSTOM_VIEW.Close;
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.OpenDataSets([ctx_DataAccess.CUSTOM_VIEW]);
    if ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger > 0 then
      Result := True;
  finally
    ctx_DataAccess.CUSTOM_VIEW.Close;
  end;
end;

procedure AddQPColumn(sType, AH: string);
begin
  if sType <> '' then
  begin
    SetLength(Batch_E_DS, Succ(Length(Batch_E_DS)));
    Batch_E_DS[Pred(Length(Batch_E_DS))].Description := sType;
    Batch_E_DS[Pred(Length(Batch_E_DS))].FieldType := AH;
  end;
end;

procedure ClearQPColumns;
begin
  SetLength(Batch_E_DS, 0);
end;

procedure DeleteQPColumn(sType, AH: string);
var
  i, j: integer;
begin
  j := -1;
  for i := 0 to Pred(Length(Batch_E_DS)) do
    if (Batch_E_DS[i].Description = sType) and
      (Batch_E_DS[i].FieldType = AH) then
    begin
      j := i;
      Break;
    end;

  if j >= 0 then
  begin
    for i := j to Length(Batch_E_DS) - 2 do
      Batch_E_DS[i] := Batch_E_DS[Succ(i)];
    SetLength(Batch_E_DS, Pred(Length(Batch_E_DS)));
  end;
end;

function GetValueForBatch_E_D(CheckNbr, aFieldName: string): Variant;
var
  RealFieldName, RealFieldType: string;
  CL_E_DS_NBR: Integer;
  sFilter: string;
  bFiltered: boolean;
begin
  if Copy(aFieldName, 1, 4) = 'Hrs_' then
  begin
    RealFieldName := Copy(aFieldName, 5, Length(aFieldName));
    RealFieldType := 'HOURS_OR_PIECES';
  end
  else
  begin
    RealFieldName := Copy(aFieldName, 5, Length(aFieldName));
    RealFieldType := 'AMOUNT';
  end;

//  Result:=0;
  CL_E_DS_NBR := ConvertNull(DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', RealFieldName, 'CL_E_DS_NBR'), 0);
  sFilter := DM_PAYROLL.PR_CHECK_LINES.Filter;
  bFiltered := DM_PAYROLL.PR_CHECK_LINES.Filtered;
  DM_PAYROLL.PR_CHECK_LINES.Filter := 'PR_CHECK_NBR=' + CheckNbr + ' and CL_E_DS_NBR=' + IntToStr(CL_E_DS_NBR);
  DM_PAYROLL.PR_CHECK_LINES.Filtered := True;
  DM_PAYROLL.PR_CHECK_LINES.First;
  Result := null;
  while not DM_PAYROLL.PR_CHECK_LINES.Eof do
  begin
    if not DM_PAYROLL.PR_CHECK_LINES.FieldByName(RealFieldType).IsNull then
      Result := VarToFloat(Result) + DM_PAYROLL.PR_CHECK_LINES.FieldByName(RealFieldType).AsFloat;
    DM_PAYROLL.PR_CHECK_LINES.Next;
  end;
  DM_PAYROLL.PR_CHECK_LINES.Filter := sFilter;
  DM_PAYROLL.PR_CHECK_LINES.Filtered := bFiltered;
end;

procedure AddCheckNbrToLists(CheckNbr: Integer);
begin
  QBRefreshList.Add(IntToStr(CheckNbr));
  CheckRefreshList.Add(IntToStr(CheckNbr));
  TAX_TOTAL_CALCULATED := False;
  ED_TOTAL_CALCULATED := False;
  QEDHolder.InvalidateCheck( CheckNbr );
end;

procedure SetValueForBatch_E_D(CheckNbr, aFieldName: string; aValue: Variant);
var
  RealFieldName, RealFieldType: string;
  iCL_E_DS_NBR: Integer;
  sFilter: string;
  bFiltered: Boolean;
  i: Integer;

  function LocateRecord(DS: TevClientDataSet; Field, Value: string; FilterBy: string = ''): Boolean;
  var
    Nbr: Integer;
  begin
    Result := False;
    Nbr := 0;
    with DS do
    begin
      sFilter := Filter;
      bFiltered := Filtered;
      try
        if FilterBy = '' then
          Filter := 'PR_CHECK_NBR = ' + CheckNbr
        else
          Filter := FilterBy;
        Filtered := True;
        while not Eof do
        begin
          if ReplaceChars(FieldByName(Field).AsString) = Value then
          begin
            Result := True;
            Nbr := DS[DS.Name + '_NBR'];
            Break;
          end;
          Next;
        end;
      finally
        Filter := sFilter;
        Filtered := bFiltered;
      end;
      Result := Result and DS.Locate(DS.Name + '_NBR', Nbr, []);
    end;
  end;
begin
  if Copy(aFieldName, 1, 4) = 'Tax_' then
  begin
    RealFieldName := RightStr(aFieldName, Length(aFieldName) - 4);
    if (RealFieldName = 'Federal') or
       (RealFieldName = 'OASDI') or
       (RealFieldName = 'Medicare') or
       (RealFieldName = 'EIC') or
       (RealFieldName = 'Backup') then
    begin
      DM_PAYROLL.PR_CHECK.Edit;
      try
        if RealFieldName = 'Federal' then
        begin
          DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_VALUE'] := aValue;
          DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
        end
        else if RealFieldName = 'OASDI' then
        begin
          if not OASDI_MEDICARE_Readonly then
            DM_PAYROLL.PR_CHECK.OR_CHECK_OASDI.AsVariant := aValue
        end
        else if RealFieldName = 'Medicare' then
        begin
          if not OASDI_MEDICARE_Readonly then
            DM_PAYROLL.PR_CHECK.OR_CHECK_MEDICARE.AsVariant := aValue
        end
        else if RealFieldName = 'EIC' then
          DM_PAYROLL.PR_CHECK['OR_CHECK_EIC'] := aValue
        else //if RealFieldName = 'Backup' then
          DM_PAYROLL.PR_CHECK['OR_CHECK_BACK_UP_WITHHOLDING'] := aValue;
        DM_PAYROLL.PR_CHECK.Post;
      except
        DM_PAYROLL.PR_CHECK.Cancel;
        raise;
      end;
    end
    else if RightStr(RealFieldName, Length(RealFieldName)-2) = '_State' then
    begin
      if not LocateRecord(DM_PAYROLL.PR_CHECK_STATES, 'STATE_Lookup', LeftStr(RealFieldName, 2)) then
      begin
        if not LocateRecord(DM_EMPLOYEE.EE_STATES, 'STATE_Lookup', LeftStr(RealFieldName, 2), 'EE_NBR = ' + DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsString) then
        begin
          QuickPayDS.FieldByName(aFieldName).Value := OldValues[QuickPayDS.FieldByName(aFieldName).Index];
          QuickPayDS.FieldByName(aFieldName).Tag := 0;
          raise ECanNotPerformOperation.CreateHelp('State ' + LeftStr(RealFieldName, 2) + ' is not setup for this employee', IDH_CanNotPerformOperation);
        end;
        AddState;
      end;
      DM_PAYROLL.PR_CHECK_STATES.Edit;
      try
        DM_PAYROLL.PR_CHECK_STATES['STATE_OVERRIDE_VALUE'] := aValue;
        DM_PAYROLL.PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
        DM_PAYROLL.PR_CHECK_STATES.Post;
      except
        DM_PAYROLL.PR_CHECK_STATES.Cancel;
        raise;
      end;
    end
    else if RightStr(RealFieldName, Length(RealFieldName)-2) = '_SDI' then
    begin
      if not LocateRecord(DM_PAYROLL.PR_CHECK_STATES, 'STATE_Lookup', LeftStr(RealFieldName, 2)) then
      begin
        if not LocateRecord(DM_EMPLOYEE.EE_STATES, 'STATE_Lookup', LeftStr(RealFieldName, 2), 'EE_NBR = ' + DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsString) then
        begin
          QuickPayDS.FieldByName(aFieldName).Value := OldValues[QuickPayDS.FieldByName(aFieldName).Index];
          QuickPayDS.FieldByName(aFieldName).Tag := 0;
          raise ECanNotPerformOperation.CreateHelp('State ' + LeftStr(RealFieldName, 2) + ' is not setup for this employee', IDH_CanNotPerformOperation);
        end;
        AddState;
      end;
      DM_PAYROLL.PR_CHECK_STATES.Edit;
      try
        DM_PAYROLL.PR_CHECK_STATES['EE_SDI_OVERRIDE'] := aValue;
        DM_PAYROLL.PR_CHECK_STATES.Post;
      except
        DM_PAYROLL.PR_CHECK_STATES.Cancel;
        raise;
      end;
    end
    else if LocateRecord(DM_PAYROLL.PR_CHECK_SUI, 'SUI_Lookup', RealFieldName) or
            LocateRecord(DM_COMPANY.CO_SUI, 'Description', RealFieldName, '1=1') then
    begin
      if ReplaceChars(DM_PAYROLL.PR_CHECK_SUI['SUI_Lookup']) <> RealFieldName then
      begin
        if not LocateRecord(DM_EMPLOYEE.EE_STATES, 'CO_STATES_NBR', DM_COMPANY.CO_SUI.FieldByName('CO_STATES_NBR').AsString, 'EE_NBR = ' + DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsString) then
        begin
          QuickPayDS.FieldByName(aFieldName).Value := OldValues[QuickPayDS.FieldByName(aFieldName).Index];
          QuickPayDS.FieldByName(aFieldName).Tag := 0;
          raise ECanNotPerformOperation.CreateHelp('State ' + DM_COMPANY.CO_SUI['State'] + ' is not setup for this employee', IDH_CanNotPerformOperation);
        end;
        AddState;
        Assert(LocateRecord(DM_PAYROLL.PR_CHECK_SUI, 'SUI_Lookup', RealFieldName));
      end;
      DM_PAYROLL.PR_CHECK_SUI.Edit;
      try
        DM_PAYROLL.PR_CHECK_SUI['OVERRIDE_AMOUNT'] := aValue;
        DM_PAYROLL.PR_CHECK_SUI.Post;
      except
        DM_PAYROLL.PR_CHECK_SUI.Cancel;
        raise;
      end;
    end
    else
    begin
      if not LocateRecord(DM_PAYROLL.PR_CHECK_LOCALS, 'Local_Name_Lookup', RealFieldName) then
      begin
        if not LocateRecord(DM_EMPLOYEE.EE_LOCALS, 'Local_Name_Lookup', RealFieldName, 'EE_NBR = ' + DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsString) then
        begin
          QuickPayDS.FieldByName(aFieldName).Value := OldValues[QuickPayDS.FieldByName(aFieldName).Index];
          QuickPayDS.FieldByName(aFieldName).Tag := 0;
          raise ECanNotPerformOperation.CreateHelp('Locality ' + RealFieldName + ' is not setup for this employee', IDH_CanNotPerformOperation);
        end;
        AddLocal;
      end;
      DM_PAYROLL.PR_CHECK_LOCALS.Edit;
      try
        DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := aValue;
        DM_PAYROLL.PR_CHECK_LOCALS.Post;
      except
        DM_PAYROLL.PR_CHECK_LOCALS.Cancel;
        raise;
      end;
    end
  end
  else
  begin
    if Copy(aFieldName, 1, 4) = 'Hrs_' then
    begin
      RealFieldName := Copy(aFieldName, 5, Length(aFieldName));
      RealFieldType := 'HOURS_OR_PIECES';
    end
    else
    begin
      RealFieldName := Copy(aFieldName, 5, Length(aFieldName));
      RealFieldType := 'AMOUNT';
    end;

    iCL_E_DS_NBR := ConvertNull(DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', RealFieldName, 'CL_E_DS_NBR'), 0);

    with DM_PAYROLL.PR_CHECK_LINES do
    begin
      sFilter := Filter;
      bFiltered := Filtered;
      try
        Filter := 'PR_CHECK_NBR = ' + ConvertNull(CheckNbr, '0') + ' and CL_E_DS_NBR = ' + IntToStr(iCL_E_DS_NBR);
        Filtered := True;
        for i := 1 to Pred(RecordCount) do
          Delete;
      finally
        Filter := sFilter;
        Filtered := bFiltered;
      end;
      if Locate('PR_CHECK_NBR;CL_E_DS_NBR', VarArrayOf([CheckNbr, iCL_E_DS_NBR]), []) then
      begin
        Edit;
        FieldByName('LINE_TYPE').AsString := CHECK_LINE_TYPE_USER;
      end
      else
      begin
        Insert;
        FieldByName('PR_CHECK_NBR').Value := CheckNbr;
        FieldByName('CL_E_DS_NBR').Value := iCL_E_DS_NBR;
      end;
      try
        if (RealFieldType = 'HOURS_OR_PIECES') and VarIsNull(aValue) then
        begin
          FieldByName(RealFieldType).Clear;
          FieldByName('AMOUNT').Clear;
        end
        else
          FieldByName(RealFieldType).Value := aValue;
        GetPayrollScreens.CheckToaBalance;
        Post;
      except
        Cancel;
        raise;
      end;
    end;
  end;
end;


function Calculate_BATCH_QuickPay: boolean;
var
  I: Integer;
  s: string;
  EDNbrList: TStringList;
  EDNbr: string;
  aFieldName, RealFieldName: string;
  IDefs: TIndexDefs;
  bManSorted: Boolean;
  iPos: Integer;
  sNewFields: string;
  EEClone: IevQuery;
begin
  IDefs := nil;
  bManSorted := False;
  result := False;
  if ((QUICK_PAY_BATCH_NBR <> DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger) or
     not Assigned(QUICK_PAY_DS.DataSet)) and not(DeletingCheck) then
    BATCH_QUICK_PAY_CALCULATED := False;

  if (BATCH_QUICK_PAY_CALCULATED and (QBRefreshList.Count = 0)) then
    Exit;

  if Assigned(QUICK_PAY_DS.DataSet) then
    QUICK_PAY_DS.DataSet.CheckBrowseMode;

  ctx_StartWait;
  try

    if not BATCH_QUICK_PAY_CALCULATED then
    begin

      QUICK_PAY_DS.DataSet := nil;

      if assigned(QuickPayDS) then
      begin
        bManSorted := QuickPayDS.IndexName = 'SORT';
        if bManSorted then
        begin
          IDefs := TIndexDefs.Create(nil);
          QuickPayDS.IndexDefs.Update;
          IDefs.Assign(QuickPayDS.IndexDefs);
        end;
        QuickPayDS.Aggregates.Clear;
        QuickPayDS.Free;
      end;

      if DM_EMPLOYEE.EE.RetrieveCondition <> '' then
        EEClone := TevQuery.Create('Select EE_NBR from EE where {AsOfNow<EE>} and (NOTES IS NOT NULL and NOTES <> '''') and ' + DM_EMPLOYEE.EE.RetrieveCondition )
      else
        EEClone := TevQuery.Create('Select EE_NBR from EE where {AsOfNow<EE>} and (NOTES IS NOT NULL and NOTES <> '''') ');
      EEClone.Execute;
      sFilterString := ';';
      while not EEClone.Result.Eof do
      begin
        sFilterString := sFilterString + EEClone.Result.FieldByName('EE_NBR').AsString + ';';
        EEClone.Result.Next;
      end;

      QuickPayDS := tevClientDataSet.Create(RemotePayrollFrm);
      QuickPayDS.AggregatesActive := True;

      with QuickPayDS.FieldDefs do
      begin
        Clear;

        Add('NUMBER', ftString, 10, False);
        Add('NAME', ftString, 40, False);
        Add('NOTES', ftString, 1, False);
        Add('PR_CHECK_NBR', ftInteger, 0, False);
        Add('CHECK_TYPE', ftString, 1, False);
        Add('CUSTOM_COMPANY_NUMBER', ftString, 20, False);
        Add('EE_NBR', ftInteger, 0, False);
        Add('CUSTOM_DIVISION_NUMBER', ftString, 20, False);
        Add('CUSTOM_BRANCH_NUMBER', ftString, 20, False);
        Add('CUSTOM_DEPARTMENT_NUMBER', ftString, 20, False);
        Add('CUSTOM_TEAM_NUMBER', ftString, 20, False);
        Add('SOCIAL_SECURITY_NUMBER', ftString, 11, False);
        Add('CUSTOM_COMPANY_NAME', ftString, 40, False);
        Add('CUSTOM_DIVISION_NAME', ftString, 40, False);
        Add('CUSTOM_BRANCH_NAME', ftString, 40, False);
        Add('CUSTOM_DEPARTMENT_NAME', ftString, 40, False);
        Add('CUSTOM_TEAM_NAME', ftString, 40, False);

        for I := 0 to Length(Batch_E_DS) - 1 do
        begin
          with AddFieldDef do
          begin
            DataType := ftFloat;
            if Batch_E_DS[I].FieldType = 'H' then
              Name := 'Hrs_' + Batch_E_DS[I].Description
            else if Batch_E_DS[I].FieldType = 'T' then
              Name := 'Tax_' + Batch_E_DS[I].Description
            else
              Name := 'Amt_' + Batch_E_DS[I].Description;
            with QuickPayDS.Aggregates.Add do
            begin
              AggregateName := Name;
              Expression := 'Sum(' + Name + ')';
              Active := True;
            end;
          end;
        end;
      end;

      if DM_PAYROLL.PR_CHECK.IndexName = 'SORT' then
        s := ''
      else
        s := DM_PAYROLL.PR_CHECK.IndexFieldNames;
      s := StringReplace(s, 'EE_Custom_Number', 'NUMBER', [rfIgnoreCase]);
      s := StringReplace(s, 'Employee_Name_Calculate', 'NAME', [rfIgnoreCase]);
      QuickPayDS.CreateFields;
      QuickPayDS.FieldByName('Notes').FieldKind := fkInternalCalc;
      QuickPayDS.FieldDefs.Clear;
      if bManSorted and Assigned(IDefs) then
      begin
        QuickPayDS.OnCalcFields := RemotePayrollFrm.QuickPayDSCalculate;
        QuickPayDS.CreateDataSet;
        QuickPayDS.IndexDefs.Assign(IDefs);
        IDefs.Free;

        s := QuickPayDS.IndexDefs[QuickPayDS.IndexDefs.IndexOf('SORT')].Fields;
        sNewFields := '';
        iPos := 1;
        while iPos <= Length(s) do
        begin
          aFieldName := ExtractFieldName(s, iPos);
          if Assigned(QuickPayDS.FindField(aFieldName)) then
            sNewFields := sNewFields + aFieldName + ';';
        end;
        Delete(sNewFields, length(sNewFields), 1);
        if sNewFields <> '' then
        begin
          QuickPayDS.IndexDefs[QuickPayDS.IndexDefs.IndexOf('SORT')].Fields := sNewFields;

          s := QuickPayDS.IndexDefs[QuickPayDS.IndexDefs.IndexOf('SORT')].DescFields;
          sNewFields := '';
          iPos := 1;
          while iPos <= Length(s) do
          begin
            aFieldName := ExtractFieldName(s, iPos);
            if Assigned(QuickPayDS.FindField(aFieldName)) then
              sNewFields := sNewFields + aFieldName + ';';
          end;
          Delete(sNewFields, length(sNewFields), 1);
          QuickPayDS.IndexDefs[QuickPayDS.IndexDefs.IndexOf('SORT')].DescFields := sNewFields;

          QuickPayDS.IndexName := 'SORT';
        end
        else
        begin
          bManSorted := False;
          s := '';
          QuickPayDS.Close;
          QuickPayDS.IndexDefs.Clear;
        end;
      end;
      if not bManSorted or not Assigned(IDefs) then
      begin
        if s <> '' then
          QuickPayDS.IndexFieldNames := s + ';PR_CHECK_NBR'
        else
          QuickPayDS.IndexFieldNames := 'PR_CHECK_NBR';
        QuickPayDS.OnCalcFields := RemotePayrollFrm.QuickPayDSCalculate;
        QuickPayDS.CreateDataSet;
      end;
      QuickPayDS.FieldByName('PR_CHECK_NBR').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_COMPANY_NUMBER').Visible := False;
      QuickPayDS.FieldByName('EE_NBR').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_DIVISION_NUMBER').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_BRANCH_NUMBER').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_DEPARTMENT_NUMBER').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_TEAM_NUMBER').Visible := False;
      QuickPayDS.FieldByName('SOCIAL_SECURITY_NUMBER').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_COMPANY_NAME').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_DIVISION_NAME').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_BRANCH_NAME').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_DEPARTMENT_NAME').Visible := False;
      QuickPayDS.FieldByName('CUSTOM_TEAM_NAME').Visible := False;
      for i := 0 to Pred(QuickPayDS.FieldCount) do
        if Pos('Amt', QuickPayDS.Fields[i].DisplayName) > 0 then
          TNumericField(QuickPayDS.Fields[i]).DisplayFormat := '#,##0.00';

      QuickPayDS.FieldByName('Name').ReadOnly := True;
      QuickPayDS.FieldByName('Check_Type').ReadOnly := True;
  //    QuickPayDS.FieldByName('Notes').ReadOnly := True;
      for I := QPFieldCount to QuickPayDS.FieldCount - 1 do
        QuickPayDS.Fields[i].OnChange := RemotePayrollFrm.QuickPayDSFieldChange;
    end;

    if QuickPayDS.FindField('Tax_OASDI') <> nil then
      QuickPayDS.FieldByName('Tax_OASDI').ReadOnly := false;
    if QuickPayDS.FindField('Tax_Medicare') <> nil then
      QuickPayDS.FieldByName('Tax_Medicare').ReadOnly := false;

    SaveDSStates([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_STATES,
                  DM_PAYROLL.PR_CHECK_SUI, DM_PAYROLL.PR_CHECK_LOCALS]);
    DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := False;

    QuickPayDS.DisableControls;
    DM_PAYROLL.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_LINES.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_STATES.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_SUI.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_LOCALS.IndexFieldNames := 'PR_CHECK_NBR';

    if BATCH_QUICK_PAY_CALCULATED then
    begin
      s := '';
      for i := 0 to Pred(QBRefreshList.Count) do
      begin
        s := s + 'PR_CHECK_NBR = ' + QBRefreshList[i];
        if i <> Pred(QBRefreshList.Count) then
          s := s + ' or ';
      end;
      DM_PAYROLL.PR_CHECK.Filter := s;
      DM_PAYROLL.PR_CHECK.Filtered := True;
      DM_PAYROLL.PR_CHECK_LINES.Filter := s;
      DM_PAYROLL.PR_CHECK_LINES.Filtered := True;
    end;

    EDNbrList := TStringList.Create;
    with DM_PAYROLL.PR_CHECK do
    try
      for i := QPFieldCount to QuickPayDS.FieldCount - 1 do
      begin
        aFieldName := QuickPayDS.Fields[I].FieldName;
        if Copy(aFieldName, 1, 4) = 'Hrs_' then
        begin
          RealFieldName := Copy(aFieldName, 5, Length(aFieldName));
          EDNbr := 'H' + IntToStr(ConvertNull(DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', RealFieldName, 'CL_E_DS_NBR'), 0));
        end
        else
        begin
          RealFieldName := Copy(aFieldName, 5, Length(aFieldName));
          EDNbr := 'A' + IntToStr(ConvertNull(DM_CLIENT.CL_E_DS.Lookup('CUSTOM_E_D_CODE_NUMBER', RealFieldName, 'CL_E_DS_NBR'), 0));
        end;
        if Length(EDNbr) > 1 then
          EDNbrList.Values[EDNbr] := aFieldName;
      end;

      First;
      DM_PAYROLL.PR_CHECK_LINES.First;
      DM_PAYROLL.PR_CHECK_STATES.First;
      DM_PAYROLL.PR_CHECK_SUI.First;
      DM_PAYROLL.PR_CHECK_LOCALS.First;
      while not EOF do
      begin
        if BATCH_QUICK_PAY_CALCULATED and
           QuickPayDS.Locate('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'], []) then
          QuickPayDS.Edit
        else
          QuickPayDS.Append;

        if BATCH_QUICK_PAY_CALCULATED then
          QBRefreshList.Delete(QBRefreshList.IndexOf(FieldByName('PR_CHECK_NBR').AsString));


        QuickPayDS.FieldByName('NUMBER').Value := FieldByName('EE_Custom_Number').Value;
        QuickPayDS.FieldByName('NAME').ReadOnly := False;
        QuickPayDS.FieldByName('NAME').Value := FieldByName('Employee_Name_Calculate').Value;
        QuickPayDS.FieldByName('NAME').ReadOnly := True;
        QuickPayDS.FieldByName('PR_CHECK_NBR').Value := FieldByName('PR_CHECK_NBR').Value;
        QuickPayDS.FieldByName('CHECK_TYPE').ReadOnly := False;
        QuickPayDS.FieldByName('CHECK_TYPE').Value := FieldByName('CHECK_TYPE').Value;
        QuickPayDS.FieldByName('CHECK_TYPE').ReadOnly := True;
        QuickPayDS.FieldByName('CUSTOM_COMPANY_NUMBER').Value := FieldByName('CUSTOM_COMPANY_NUMBER').Value;
        QuickPayDS.FieldByName('EE_NBR').Value := FieldByName('EE_NBR').Value;
        QuickPayDS.FieldByName('CUSTOM_DIVISION_NUMBER').Value := FieldByName('CUSTOM_DIVISION_NUMBER').Value;
        QuickPayDS.FieldByName('CUSTOM_BRANCH_NUMBER').Value := FieldByName('CUSTOM_BRANCH_NUMBER').Value;
        QuickPayDS.FieldByName('CUSTOM_DEPARTMENT_NUMBER').Value := FieldByName('CUSTOM_DEPARTMENT_NUMBER').Value;
        QuickPayDS.FieldByName('CUSTOM_TEAM_NUMBER').Value := FieldByName('CUSTOM_TEAM_NUMBER').Value;
        QuickPayDS.FieldByName('SOCIAL_SECURITY_NUMBER').Value := FieldByName('SOCIAL_SECURITY_NUMBER').Value;
        QuickPayDS.FieldByName('CUSTOM_COMPANY_NAME').Value := FieldByName('CUSTOM_COMPANY_NAME').Value;
        QuickPayDS.FieldByName('CUSTOM_DIVISION_NAME').Value := FieldByName('CUSTOM_DIVISION_NAME').Value;
        QuickPayDS.FieldByName('CUSTOM_BRANCH_NAME').Value := FieldByName('CUSTOM_BRANCH_NAME').Value;
        QuickPayDS.FieldByName('CUSTOM_DEPARTMENT_NAME').Value := FieldByName('CUSTOM_DEPARTMENT_NAME').Value;
        QuickPayDS.FieldByName('CUSTOM_TEAM_NAME').Value := FieldByName('CUSTOM_TEAM_NAME').Value;


        if ((DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) or
            (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString = OVERRIDE_VALUE_TYPE_NONE)) and
           Assigned(QuickPayDS.FindField('Tax_Federal')) then
          QuickPayDS['Tax_Federal'] := FieldValues['OR_CHECK_FEDERAL_VALUE'];
        if Assigned(QuickPayDS.FindField('Tax_OASDI')) then
          QuickPayDS['Tax_OASDI'] := FieldValues['OR_CHECK_OASDI'];
        if Assigned(QuickPayDS.FindField('Tax_Medicare')) then
          QuickPayDS['Tax_Medicare'] := FieldValues['OR_CHECK_MEDICARE'];
        if Assigned(QuickPayDS.FindField('Tax_EIC')) then
          QuickPayDS['Tax_EIC'] := FieldValues['OR_CHECK_EIC'];
        if Assigned(QuickPayDS.FindField('Tax_Backup')) then
          QuickPayDS['Tax_Backup'] := FieldValues['OR_CHECK_BACK_UP_WITHHOLDING'];

        while (DM_PAYROLL.PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) and
              not DM_PAYROLL.PR_CHECK_STATES.Eof do
          DM_PAYROLL.PR_CHECK_STATES.Next;

        while (DM_PAYROLL.PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) and
              not DM_PAYROLL.PR_CHECK_STATES.Eof do
        begin
          if ((DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) or
              (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString = OVERRIDE_VALUE_TYPE_NONE)) and
             Assigned(QuickPayDS.FindField('Tax_' + DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_Lookup').AsString + '_State')) then
            QuickPayDS['Tax_' + DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_Lookup').AsString + '_State'] := DM_PAYROLL.PR_CHECK_STATES['STATE_OVERRIDE_VALUE'];
          if Assigned(QuickPayDS.FindField('Tax_' + DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_Lookup').AsString + '_SDI')) then
            QuickPayDS['Tax_' + DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_Lookup').AsString + '_SDI'] := DM_PAYROLL.PR_CHECK_STATES['EE_SDI_OVERRIDE'];
          DM_PAYROLL.PR_CHECK_STATES.Next;
        end;

        while (DM_PAYROLL.PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) and
              not DM_PAYROLL.PR_CHECK_SUI.Eof do
          DM_PAYROLL.PR_CHECK_SUI.Next;

        while (DM_PAYROLL.PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) and
              not DM_PAYROLL.PR_CHECK_SUI.Eof do
        begin
          if Assigned(QuickPayDS.FindField('Tax_' + ReplaceChars(DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_Lookup').AsString))) then
            QuickPayDS['Tax_' + ReplaceChars(DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_Lookup').AsString)] := DM_PAYROLL.PR_CHECK_SUI['OVERRIDE_AMOUNT'];
          DM_PAYROLL.PR_CHECK_SUI.Next;
        end;

        while (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) and
              not DM_PAYROLL.PR_CHECK_LOCALS.Eof do
          DM_PAYROLL.PR_CHECK_LOCALS.Next;

        while (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) and
              not DM_PAYROLL.PR_CHECK_LOCALS.Eof do
        begin
          if Assigned(QuickPayDS.FindField('Tax_' + ReplaceChars(DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('Local_Name_Lookup').AsString))) then
            QuickPayDS['Tax_' + ReplaceChars(DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('Local_Name_Lookup').AsString)] := DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'];
          DM_PAYROLL.PR_CHECK_LOCALS.Next;
        end;

        while (DM_PAYROLL.PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) and
              not DM_PAYROLL.PR_CHECK_LINES.Eof do
          DM_PAYROLL.PR_CHECK_LINES.Next;

        for i := QPFieldCount to QuickPayDS.FieldCount - 1 do
          if Copy(QuickPayDS.Fields[i].FieldName, 1, 4) <> 'Tax_' then
            QuickPayDS.Fields[i].Clear;

//        DM_PAYROLL.PR_CHECK_LINES.Filter := 'PR_CHECK_NBR = ' + IntToStr(FieldByName('PR_CHECK_NBR').AsInteger);
//        DM_PAYROLL.PR_CHECK_LINES.Filtered := True;
//        DM_PAYROLL.PR_CHECK_LINES.First;

        while (DM_PAYROLL.PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) and
              not DM_PAYROLL.PR_CHECK_LINES.Eof do
        begin
          aFieldName := EDNbrList.Values['H' + DM_PAYROLL.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsString];
          if Assigned(QuickPayDS.FindField(aFieldName)) then
            if not DM_PAYROLL.PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').IsNull then
              QuickPayDS.FieldByName(aFieldName).AsFloat := QuickPayDS.FieldByName(aFieldName).AsFloat + DM_PAYROLL.PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').AsFloat;
          aFieldName := EDNbrList.Values['A' + DM_PAYROLL.PR_CHECK_LINES.FieldByName('CL_E_DS_NBR').AsString];
          if Assigned(QuickPayDS.FindField(aFieldName)) then
            if not DM_PAYROLL.PR_CHECK_LINES.FieldByName('AMOUNT').IsNull then
              QuickPayDS.FieldByName(aFieldName).AsFloat := QuickPayDS.FieldByName(aFieldName).AsFloat + DM_PAYROLL.PR_CHECK_LINES.FieldByName('AMOUNT').AsFloat;
          DM_PAYROLL.PR_CHECK_LINES.Next;
        end;

        try
          QuickPayDS.BeforePost := nil;
          QuickPayDS.AfterPost := nil;
          QuickPayDS.Post;
          OldValues := Unassigned;
        finally
          QuickPayDS.BeforePost := RemotePayrollFrm.QuickPayDSBeforePost;
          QuickPayDS.AfterPost := RemotePayrollFrm.QuickPayDSAfterPost;
        end;
        Next;
      end;

      for i := QPFieldCount to QuickPayDS.FieldCount - 1 do
        QuickPayDS.Fields[i].Tag := 0;

      for i := 0 to Pred(QBRefreshList.Count) do
        if BATCH_QUICK_PAY_CALCULATED and
           QuickPayDS.Locate('PR_CHECK_NBR', StrToInt(QBRefreshList[i]), []) then
          QuickPayDS.Delete;

    finally
      QuickPayDS.EnableControls;
      DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK_LINES.LookupsEnabled := True;
//      DM_PAYROLL.PR_CHECK_STATES.LookupsEnabled := True;
//      DM_PAYROLL.PR_CHECK_SUI.LookupsEnabled := True;
//      DM_PAYROLL.PR_CHECK_LOCALS.LookupsEnabled := True;
      LoadDSStates([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_STATES,
                    DM_PAYROLL.PR_CHECK_SUI, DM_PAYROLL.PR_CHECK_LOCALS]);
      EDNbrList.Free;
    end;

    QUICK_PAY_DS.DataSet := QuickPayDS;
    QUICK_PAY_DS.DataSet.Locate('PR_CHECK_NBR', DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'], []);

    BATCH_QUICK_PAY_CALCULATED := True;
    QBRefreshList.Clear;

    QuickPayDS.BeforeInsert := RemotePayrollFrm.QuickPayDSBeforeInsert;
    QuickPayDS.BeforeEdit := RemotePayrollFrm.QuickPayDSBeforeEdit;
    QuickPayDS.BeforePost := RemotePayrollFrm.QuickPayDSBeforePost;
    QuickPayDS.BeforeDelete := RemotePayrollFrm.QuickPayDSBeforeDelete;
    QuickPayDS.AfterPost := RemotePayrollFrm.QuickPayDSAfterPost;
    QuickPayDS.AfterDelete := RemotePayrollFrm.QuickPayDSAfterDelete;
    QuickPayDS.AfterCancel := RemotePayrollFrm.QuickPayDSAfterCancel;

    result := True;
    QUICK_PAY_BATCH_NBR := DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger;
    QuickPayDS.MergeChangeLog;

    if OASDI_MEDICARE_Readonly then
    begin
      if QuickPayDS.FindField('Tax_OASDI') <> nil then
        QuickPayDS.FieldByName('Tax_OASDI').ReadOnly := true;
      if QuickPayDS.FindField('Tax_Medicare') <> nil then
        QuickPayDS.FieldByName('Tax_Medicare').ReadOnly := true;
    end;
  finally
    ctx_EndWait;
  end;
end;

function Calculate_TAX_TOTALS(Separate: Boolean): Boolean;
var
  ManualOverride: string;
  s: string;

  procedure AddToTAX_MASTERTable(RowType: DS_ROW_TYPES; Description: string;
    Amount: Currency);
  var
    CO_STATES_NBR: integer;
    CO_LOCAL_TAX_NBR: integer;
    CO_SUI_NBR: integer;
    PR_CHECK_NBR: integer;
    V: Variant;
  begin
    CO_STATES_NBR := 0;
    CO_LOCAL_TAX_NBR := 0;
    CO_SUI_NBR := 0;

    begin
      PR_CHECK_NBR := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger;

      case RowType of
      DS_STATE_WAGE,
      DS_STATE_SHORTFALL,
      DS_STATE_TAX,
      DS_STATE_EE_SDI_WAGE,
      DS_STATE_EE_SDI_SHORTFALL,
      DS_STATE_EE_SDI_TAX:
      begin
        V := '';
        if DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_STATES_NBR').Value, []) then
          V := DM_EMPLOYEE.EE_STATES.FieldByName('STATE_LOOKUP').AsString;
        Description := 'STATE (' + V + ') ' + Description;
        CO_STATES_NBR := DM_EMPLOYEE.EE_STATES.FieldByName('CO_STATES_NBR').AsInteger;
      end;

      DS_STATE_SUI_WAGE,
      DS_STATE_SUI_TAX:
      begin
        CO_SUI_NBR := DM_PAYROLL.PR_CHECK_SUI.FieldByName('CO_SUI_NBR').AsInteger;

        if not DM_COMPANY.CO_SUI.Locate('CO_SUI_NBR', CO_SUI_NBR, [loCaseInsensitive]) then
          raise EInconsistentData.CreateHelp('Co_SUI not found', IDH_InconsistentData);

        CO_STATES_NBR := DM_COMPANY.CO_SUI.FieldByName('CO_STATES_NBR').AsInteger;
        V := DM_COMPANY.CO_STATES.Lookup('CO_STATES_NBR', CO_STATES_NBR, 'STATE');
        Description := 'STATE (' + string(V) + ') ' + DM_COMPANY.CO_SUI.FieldByName('Description').AsString + ' ' + Description;
      end;

      DS_LOCAL_WAGE,
      DS_LOCAL_TAX,
      DS_LOCAL_SHORTFALL:
      begin
        if DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value, []) then
          V := DM_EMPLOYEE.EE_LOCALS.FieldByName('Local_Name_Lookup').AsString;
        Description := 'LOCAL (' + V + ') ' + Description;
        CO_LOCAL_TAX_NBR := DM_EMPLOYEE.EE_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsInteger;
      end;

      end;

      if not Payroll_TAX_Master.Locate('OVRD;RowType;CO_STATES_NBR;CO_LOCAL_TAX_NBR;CO_SUI_NBR',
        VarArrayOf([ManualOverride, RowType, CO_STATES_NBR, CO_LOCAL_TAX_NBR, CO_SUI_NBR]), [loCaseInsensitive]) then
      begin
        Tax_Master_ID := Tax_Master_ID + 1;
        Payroll_TAX_Master.Append;

        Payroll_TAX_Master.FieldByName('TAX_NBR').AsInteger := Tax_Master_ID;
        Payroll_TAX_Master.FieldByName('OVRD').AsString := ManualOverride;
        Payroll_TAX_Master.FieldByName('RowType').AsInteger := ord(RowType);
        Payroll_TAX_Master.FieldByName('Description').AsString := Description;
        Payroll_TAX_Master.FieldByName('CO_STATES_NBR').AsInteger := CO_STATES_NBR;
        Payroll_TAX_Master.FieldByName('CO_LOCAL_TAX_NBR').AsInteger := CO_LOCAL_TAX_NBR;
        Payroll_TAX_Master.FieldByName('CO_SUI_NBR').AsInteger := CO_SUI_NBR;
        Payroll_TAX_Master.FieldByName('Amount').AsCurrency := Amount;
        Payroll_TAX_Master.FieldByName('Count').AsInteger := 1;
        Payroll_TAX_Master.Post;
      end
      else
      begin
        Payroll_TAX_Master.Edit;
        Payroll_TAX_Master.FieldByName('COUNT').AsInteger := Payroll_TAX_Master.FieldByName('COUNT').AsInteger + 1;
        Payroll_TAX_Master.FieldByName('AMOUNT').AsCurrency := Payroll_TAX_Master.FieldByName('AMOUNT').AsCurrency + Amount;
        Payroll_TAX_Master.Post;
      end;

      Payroll_TAX_Detail.Append;
      Payroll_TAX_Detail.FieldByName('TAX_NBR').AsInteger := Payroll_TAX_Master.FieldByName('TAX_NBR').AsInteger;
      Payroll_TAX_Detail.FieldByName('Amount').AsCurrency := Amount;
      Payroll_TAX_Detail.FieldByName('PR_CHECK_NBR').AsInteger := PR_CHECK_NBR;
      Payroll_TAX_Detail.FieldByName('Name').AsString := DM_PAYROLL.PR_CHECK.FieldByName('Employee_Name_Calculate').AsString;
      Payroll_TAX_Detail.FieldByName('EE_Custom_Number').AsString := DM_PAYROLL.PR_CHECK.FieldByName('EE_Custom_Number').AsString;
      Payroll_TAX_Detail.FieldByName('SOCIAL_SECURITY_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_DIVISION_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DIVISION_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_BRANCH_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_BRANCH_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DEPARTMENT_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_TEAM_NUMBER').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_TEAM_NUMBER').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_COMPANY_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_COMPANY_NAME').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_DIVISION_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DIVISION_NAME').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_BRANCH_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_BRANCH_NAME').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_DEPARTMENT_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DEPARTMENT_NAME').AsString;
      Payroll_TAX_Detail.FieldByName('CUSTOM_TEAM_NAME').AsString := DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_TEAM_NAME').AsString;
      Payroll_TAX_Detail.Post;
    end;
  end;

begin
  Result := False;

  if Assigned(Payroll_TAX_Master) then
  begin
    if Payroll_TAX_Master.UserFiltered then
    begin
      Payroll_TAX_Master.UserFiltered := False;
      Payroll_TAX_Master.UserFilter := '';
    end;
  end;

  CalcForBatch := Pos('PR_BATCH_NBR', DM_PAYROLL.PR_CHECK.RetrieveCondition) > 0;
  if CalcForBatch and
     (TAX_BATCH_NBR <> DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger) then
    TAX_TOTAL_CALCULATED := False;

  if TAX_TOTAL_CALCULATED then
    exit;

  DM_COMPANY.CO_STATES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger));
  DM_COMPANY.CO_SUI.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR = ' + IntToStr(DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger));
  DM_EMPLOYEE.EE_STATES.CheckDSCondition('');
  DM_EMPLOYEE.EE_LOCALS.CheckDSCondition('');

  ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_STATES, DM_COMPANY.CO_SUI, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_LOCALS]);

  ctx_StartWait;
  try

    if not Assigned(Payroll_TAX_Master) then
    begin
      Payroll_TAX_Master := TevClientDataSet.Create(RemotePayrollFrm);
      Payroll_TAX_Master.Name := 'TAX';
      Payroll_TAX_Master.FieldDefs.Add('TAX_NBR', ftInteger, 0);
      Payroll_TAX_Master.FieldDefs.Add('OVRD', ftString, 1);
      Payroll_TAX_Master.FieldDefs.Add('RowType', ftInteger, 0);
      Payroll_TAX_Master.FieldDefs.Add('Description', ftString, 40);
      Payroll_TAX_Master.FieldDefs.Add('CO_STATES_NBR', ftInteger, 0);
      Payroll_TAX_Master.FieldDefs.Add('CO_LOCAL_TAX_NBR', ftInteger, 0);
      Payroll_TAX_Master.FieldDefs.Add('CO_SUI_NBR', ftInteger, 0);
      Payroll_TAX_Master.FieldDefs.Add('Amount', ftCurrency, 0);
      Payroll_TAX_Master.FieldDefs.Add('Count', ftInteger, 0);
      Payroll_TAX_Master.CreateDataSet;
      Payroll_TAX_Master.LogChanges := False;
      Payroll_TAX_Master.FieldByName('RowType').Visible := False;
      Payroll_TAX_Master.FieldByName('TAX_NBR').Visible := False;
      Payroll_TAX_Master.FieldByName('CO_STATES_NBR').Visible := False;
      Payroll_TAX_Master.FieldByName('CO_LOCAL_TAX_NBR').Visible := False;
      Payroll_TAX_Master.FieldByName('CO_SUI_NBR').Visible := False;
    end;

    Payroll_TAX_Master.FieldByName('OVRD').Visible := Separate;

    if not Assigned(Payroll_TAX_Detail) then
    begin
      Payroll_TAX_Detail := TevClientDataSet.Create(RemotePayrollFrm);

      CreateIntegerField(Payroll_TAX_Detail, 'TAX_NBR', 'PARENT_ID');
      CreateCurrencyField(Payroll_TAX_Detail, 'Amount', 'Amount');
      CreateIntegerField(Payroll_TAX_Detail, 'PR_CHECK_NBR', 'PR_CHECK_NBR');
      CreateStringField(Payroll_TAX_Detail, 'Name', 'Name', 40);
      CreateStringField(Payroll_TAX_Detail, 'EE_Custom_Number', 'EE_Custom_Number', 9);
      CreateStringField(Payroll_TAX_Detail, 'SOCIAL_SECURITY_NUMBER', 'SOCIAL_SECURITY_NUMBER', 11);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_COMPANY_NUMBER', 'CUSTOM_COMPANY_NUMBER', 20);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_DIVISION_NUMBER', 'CUSTOM_DIVISION_NUMBER', 20);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_BRANCH_NUMBER', 'CUSTOM_BRANCH_NUMBER', 20);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_DEPARTMENT_NUMBER', 'CUSTOM_DEPARTMENT_NUMBER', 20);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_TEAM_NUMBER', 'CUSTOM_TEAM_NUMBER', 20);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_COMPANY_NAME', 'CUSTOM_COMPANY_NAME', 40);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_DIVISION_NAME', 'CUSTOM_DIVISION_NAME', 40);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_BRANCH_NAME', 'CUSTOM_BRANCH_NAME', 40);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_DEPARTMENT_NAME', 'CUSTOM_DEPARTMENT_NAME', 40);
      CreateStringField(Payroll_TAX_Detail, 'CUSTOM_TEAM_NAME', 'CUSTOM_TEAM_NAME', 40);

      Payroll_TAX_Detail.CreateDataSet;
      Payroll_TAX_Detail.LogChanges := False;
    end;
    if DM_PAYROLL.PR_CHECK.IndexName = 'SORT' then
      s := ''
    else
      s := DM_PAYROLL.PR_CHECK.IndexFieldNames;
    s := StringReplace(s, 'Employee_Name_Calculate', 'NAME', [rfIgnoreCase]);
    Payroll_TAX_Detail.IndexFieldNames := s;
    if Payroll_TAX_Master.Active then
      Payroll_TAX_Master.EmptyDataSet;
    if Payroll_TAX_Detail.Active then
      Payroll_TAX_Detail.EmptyDataSet;
    TAX_Master_ID := 0;

    Payroll_TAX_Master.DisableControls;
    Payroll_TAX_Detail.DisableControls;

    SaveDSStates([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_STATES, DM_PAYROLL.PR_CHECK_LOCALS,
                  DM_PAYROLL.PR_CHECK_SUI]);
    DM_PAYROLL.PR_CHECK.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_STATES.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_LOCALS.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK_SUI.LookupsEnabled := False;
    DM_PAYROLL.PR_CHECK.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_STATES.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_LOCALS.IndexFieldNames := 'PR_CHECK_NBR';
    DM_PAYROLL.PR_CHECK_SUI.IndexFieldNames := 'PR_CHECK_NBR';

    try
      DM_PAYROLL.PR_CHECK.First;
      DM_PAYROLL.PR_CHECK_STATES.First;
      DM_PAYROLL.PR_CHECK_LOCALS.First;
      DM_PAYROLL.PR_CHECK_SUI.First;
      ManualOverride := #9;

      while not DM_PAYROLL.PR_CHECK.EOF do
      begin
        if Separate then
          if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').AsCurrency = 0 then
            ManualOverride := 'N'
          else
            ManualOverride := 'Y';

        if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').AsCurrency <> 0) and
           (DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAX').AsCurrency < 0) then
          AddToTAX_MASTERTable(DS_FED_OASDI_TAX, 'FEDERAL OASDI TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAX').AsCurrency)
        else if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').AsCurrency <> 0 then
          AddToTAX_MASTERTable(DS_FED_OASDI_TAX, 'FEDERAL OASDI TAX', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').AsCurrency)
        else if DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAX').AsCurrency <> 0 then
        begin
          AddToTAX_MASTERTable(DS_FED_OASDI_TAX, 'FEDERAL OASDI TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAX').AsCurrency);
          AddToTAX_MASTERTable(DS_FED_OASDI_WAGES, 'FEDERAL EE OASDI WAGES', DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAXABLE_WAGES').AsCurrency);
          AddToTAX_MASTERTable(DS_FED_OASDI_TIPS, 'FEDERAL EE OASDI TIPS', DM_PAYROLL.PR_CHECK.FieldByName('EE_OASDI_TAXABLE_TIPS').AsCurrency);
        end;

        if Separate then
          if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').AsCurrency = 0 then
            ManualOverride := 'N'
          else
            ManualOverride := 'Y';

        if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').AsCurrency <> 0) and
           (DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAX').AsCurrency < 0) then
          AddToTAX_MASTERTable(DS_FED_EE_MEDICARE_TAX, 'FEDERAL EE_MEDICARE_TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAX').AsCurrency)
        else if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').AsCurrency <> 0 then
          AddToTAX_MASTERTable(DS_FED_EE_MEDICARE_TAX, 'FEDERAL EE_MEDICARE_TAX', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').AsCurrency)
        else if DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAX').AsCurrency <> 0 then
        begin
          AddToTAX_MASTERTable(DS_FED_EE_MEDICARE_TAX, 'FEDERAL EE_MEDICARE_TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAX').AsCurrency);
          AddToTAX_MASTERTable(DS_FED_EE_MEDICARE_WAGES, 'FEDERAL EE_MEDICARE_TAXABLE_WAGES', DM_PAYROLL.PR_CHECK.FieldByName('EE_MEDICARE_TAXABLE_WAGES').AsCurrency);
        end;

        if Separate then
          if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').AsCurrency = 0 then
            ManualOverride := 'N'
          else
            ManualOverride := 'Y';

        if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').AsCurrency <> 0) and
           (DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAX').AsCurrency < 0) and
           (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString <> OVERRIDE_VALUE_TYPE_NONE) then
          AddToTAX_MASTERTable(DS_FED_TAX, 'FEDERAL_TAX', DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAX').AsCurrency)
        else if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').AsCurrency <> 0) and
                (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) then
          AddToTAX_MASTERTable(DS_FED_TAX, 'FEDERAL_TAX', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').AsCurrency)
        else if (DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAX').AsCurrency <> 0) or
                (DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_SHORTFALL').AsCurrency <> 0) then
        begin
          AddToTAX_MASTERTable(DS_FED_TAX, 'FEDERAL_TAX', DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAX').AsCurrency);
          AddToTAX_MASTERTable(DS_FED_WAGES, 'FEDERAL_TAXABLE_WAGES', DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_TAXABLE_WAGES').AsCurrency);
          if (DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_SHORTFALL').AsCurrency <> 0) then
            AddToTAX_MASTERTable(DS_FED_SHORTFALL, 'FEDERAL_SHORTFALL', DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_SHORTFALL').AsCurrency);
        end;

        if Separate then
          if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_EIC').AsCurrency = 0 then
            ManualOverride := 'N'
          else
            ManualOverride := 'Y';

        if (DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_EIC').AsCurrency <> 0) and
           (DM_PAYROLL.PR_CHECK.FieldByName('EE_EIC_TAX').AsCurrency >= 0) then
          AddToTAX_MASTERTable(DS_FED_EE_EIC_TAX, 'FEDERAL EE_EIC_TAX', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_EIC').AsCurrency)
        else if DM_PAYROLL.PR_CHECK.FieldByName('EE_EIC_TAX').AsCurrency <> 0 then
          AddToTAX_MASTERTable(DS_FED_EE_EIC_TAX, 'FEDERAL EE_EIC_TAX', DM_PAYROLL.PR_CHECK.FieldByName('EE_EIC_TAX').AsCurrency);

        if Separate then
          if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').AsCurrency = 0 then
            ManualOverride := 'N'
          else
            ManualOverride := 'Y';

        if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').AsCurrency <> 0 then
          AddToTAX_MASTERTable(DS_FED_OR_CHECK_BACK_UP_WITHHOLDING, 'FEDERAL OR_CHECK_BACK_UP_WITHHOLDING', DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').AsCurrency);

        while not DM_PAYROLL.PR_CHECK_STATES.Eof and
              (DM_PAYROLL.PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
          DM_PAYROLL.PR_CHECK_STATES.Next;

        while not DM_PAYROLL.PR_CHECK_STATES.EOF and
              (DM_PAYROLL.PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
        begin

          if Separate then
            if DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsCurrency = 0 then
              ManualOverride := 'N'
            else
              ManualOverride := 'Y';

          if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsCurrency <> 0) and
             (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').AsCurrency < 0) and
             (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString <> OVERRIDE_VALUE_TYPE_NONE) then
            AddToTAX_MASTERTable(DS_STATE_TAX, 'TAX', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').AsCurrency)
          else if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsCurrency <> 0) and
                  (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_TYPE').AsString = OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT) then
            AddToTAX_MASTERTable(DS_STATE_TAX, 'TAX', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').AsCurrency)
          else if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').AsCurrency <> 0) or
                  (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').AsCurrency <> 0) then
          begin
            AddToTAX_MASTERTable(DS_STATE_TAX, 'TAX', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAX').AsCurrency);
            AddToTAX_MASTERTable(DS_STATE_WAGE, 'WAGE', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_TAXABLE_WAGES').AsCurrency);

            if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').AsCurrency <> 0) then
              AddToTAX_MASTERTable(DS_STATE_SHORTFALL, 'SHORTFALL', DM_PAYROLL.PR_CHECK_STATES.FieldByName('STATE_SHORTFALL').AsCurrency);

          end;

          if (DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAX').AsCurrency <> 0) or
             (DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').AsCurrency <> 0) then
          begin
            AddToTAX_MASTERTable(DS_STATE_EE_SDI_TAX, 'SDI TAX', DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAX').AsCurrency);
            AddToTAX_MASTERTable(DS_STATE_EE_SDI_WAGE, 'SDI WAGE', DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_TAXABLE_WAGES').AsCurrency);
            if DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').AsCurrency <> 0 then
              AddToTAX_MASTERTable(DS_STATE_EE_SDI_SHORTFALL, 'SDI SHORTFALL', DM_PAYROLL.PR_CHECK_STATES.FieldByName('EE_SDI_SHORTFALL').AsCurrency);
          end;

          DM_PAYROLL.PR_CHECK_STATES.Next;
        end;

        while not DM_PAYROLL.PR_CHECK_LOCALS.Eof and
              (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
          DM_PAYROLL.PR_CHECK_LOCALS.Next;

        while not DM_PAYROLL.PR_CHECK_LOCALS.EOF and
              (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
        begin

          if Separate then
            if DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsCurrency = 0 then
              ManualOverride := 'N'
            else
              ManualOverride := 'Y';

          if (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsCurrency <> 0) and
             (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsCurrency < 0) then
            AddToTAX_MASTERTable(DS_LOCAL_TAX, 'TAX', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsCurrency)
          {else if DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsCurrency <> 0 then
            AddToTAX_MASTERTable(DS_LOCAL_TAX, 'TAX', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').AsCurrency)}
          else if (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsCurrency <> 0) or
                  (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').AsCurrency <> 0) or
                  (DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').AsCurrency <> 0) then
          begin
            AddToTAX_MASTERTable(DS_LOCAL_TAX, 'TAX', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').AsCurrency);
            AddToTAX_MASTERTable(DS_LOCAL_WAGE, 'WAGE', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').AsCurrency);
            if DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').AsCurrency <> 0 then
              AddToTAX_MASTERTable(DS_LOCAL_SHORTFALL, 'SHORTFALL', DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').AsCurrency);
          end;
          DM_PAYROLL.PR_CHECK_LOCALS.Next;
        end;

        while not DM_PAYROLL.PR_CHECK_SUI.Eof and
              (DM_PAYROLL.PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').AsInteger < DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
          DM_PAYROLL.PR_CHECK_SUI.Next;

        while not DM_PAYROLL.PR_CHECK_SUI.EOF and
              (DM_PAYROLL.PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').AsInteger = DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsInteger) do
        begin
          if DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAX').AsCurrency <> 0 then
          begin
            AddToTAX_MASTERTable(DS_STATE_SUI_TAX, 'TAX', DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAX').AsCurrency);
            AddToTAX_MASTERTable(DS_STATE_SUI_WAGE, 'WAGE', DM_PAYROLL.PR_CHECK_SUI.FieldByName('SUI_TAXABLE_WAGES').AsCurrency);
          end;
          DM_PAYROLL.PR_CHECK_SUI.Next;

        end;

        DM_PAYROLL.PR_CHECK.Next;
      end;
    finally
      LoadDSStates([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_STATES, DM_PAYROLL.PR_CHECK_LOCALS,
                    DM_PAYROLL.PR_CHECK_SUI]);
      Payroll_TAX_Master.EnableControls;
      Payroll_TAX_Detail.EnableControls;
      DM_PAYROLL.PR_CHECK.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK_STATES.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK_LOCALS.LookupsEnabled := True;
      DM_PAYROLL.PR_CHECK_SUI.LookupsEnabled := True;
    end;

    dsTax_Total.DataSet := Payroll_Tax_Master;
    dsTax_Detail.DataSet := Payroll_Tax_Detail;

    dsTax_Total.DataSet.First;

    TAX_TOTAL_CALCULATED := TRUE;

    result := True;
    if CalcForBatch then
      TAX_BATCH_NBR := DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger;
  finally
    ctx_EndWait;
  end;
end;

function Calculate_ED_TOTALS(sepByLineType, sepByCheckType: boolean): boolean;
var
  cGroupFieldsCount: integer;
  cdsCheckLines: TevClientDataSet;
  s: string;
  FieldList: TList;
  i: Integer;

  oldCL_E_DS_NBR: Variant; //tenp var
  groupKeyValues: Variant;
  newGroupKeyValues: Variant;
  groupFieldNames: string;
begin
  result := False;

  CalcForBatch := Pos('PR_BATCH_NBR', DM_PAYROLL.PR_CHECK.RetrieveCondition) > 0;
  FSeparateEDByLineType := sepByLineType;
  FSeparateEDByCheckType := sepByCheckType;

  groupFieldNames := 'CL_E_DS_NBR';
  if FSeparateEDByCheckType then
    groupFieldNames := groupFieldNames + ';CHECK_TYPE';
  if FSeparateEDByLineType then
    groupFieldNames := groupFieldNames + ';LINE_TYPE';
  cGroupFieldsCount := CountChar(groupFieldNames,';')+1;

  if cGroupFieldsCount > 1 then
  begin
    groupKeyValues := VarArrayCreate([0, cGroupFieldsCount-1], varVariant);
    groupKeyValues[0] := Null; //!! does VarArrayCreate initialize array elements???
  end
  else
    groupKeyValues := Null;

  if CalcForBatch and
     (ED_BATCH_NBR <> DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger) then
    ED_TOTAL_CALCULATED := False;

  if ED_TOTAL_CALCULATED then
    exit;

  ctx_StartWait;
  try

    if not assigned(Payroll_ED_Master) then
    begin
      Payroll_ED_Master := tevclientdataset.Create(RemotePayrollFrm);
      Payroll_ED_Master.Name := 'ED';

      CreateIntegerField(Payroll_ED_Master, 'ED_NBR', 'ID');
      CreateStringField(Payroll_ED_Master, 'LINE_TYPE', 'Sched', 1);
      CreateStringField(Payroll_ED_Master, 'CHECK_TYPE', 'Check', 1);
      CreateStringField(Payroll_ED_Master, 'ED_CODE', 'E/D Code', 10);
      CreateStringField(Payroll_ED_Master, 'Description', 'Description', 20);
      CreateIntegerField(Payroll_ED_Master, 'CL_E_DS_NBR', 'CL_E_DS_NBR');
      CreateFloatField(Payroll_ED_Master, 'Hours_Pieces', 'Hours/Pieces');
      CreateCurrencyField(Payroll_ED_Master, 'Amount', 'Amount');
      CreateIntegerField(Payroll_ED_Master, 'Count', 'Count');

      Payroll_ED_Master.CreateDataSet;
      Payroll_ED_Master.LogChanges := False;
      Payroll_ED_Master.FieldByName('ED_NBR').Visible := False;
      Payroll_ED_Master.FieldByName('CL_E_DS_NBR').Visible := False;
    end;

    Payroll_ED_Master.FieldByName('LINE_TYPE').Visible := FSeparateEDByLineType;
    Payroll_ED_Master.FieldByName('CHECK_TYPE').Visible := FSeparateEDByCheckType;

    if DM_PAYROLL.PR_CHECK.IndexName = 'SORT' then
      s := ''
    else
      s := DM_PAYROLL.PR_CHECK.IndexFieldNames;
    s := StringReplace(s, 'Employee_Name_Calculate', 'NAME', [rfIgnoreCase]);
    if assigned(Payroll_ED_Detail) and
       (Payroll_ED_Detail.IndexFieldNames <> s) then
    begin
      Payroll_ED_Detail.Free;
      Payroll_ED_Detail := nil;
    end;

    if not assigned(Payroll_ED_Detail) then
    begin
      Payroll_ED_Detail := tevclientdataset.Create(RemotePayrollFrm);

      CreateIntegerField(Payroll_ED_Detail, 'ED_NBR', 'PARENT_ID');
      CreateCurrencyField(Payroll_ED_Detail, 'Amount', 'Amount');
      CreateFloatField(Payroll_ED_Detail, 'Hours_Pieces', 'Hours_Pieces');
      CreateIntegerField(Payroll_ED_Detail, 'PR_CHECK_NBR', 'PR_CHECK_NBR');

      with CreateStringField(Payroll_ED_Detail, 'Name', 'Name', 40) do
      begin
        FieldKind := fkLookup;
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'Employee_Name_Calculate';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'EE_Custom_Number', 'EE_Custom_Number', 9) do
      begin
        FieldKind := fkLookup;
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'EE_Custom_Number';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'SOCIAL_SECURITY_NUMBER', 'SOCIAL_SECURITY_NUMBER', 11) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'SOCIAL_SECURITY_NUMBER';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_COMPANY_NUMBER', 'CUSTOM_COMPANY_NUMBER', 20) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_COMPANY_NUMBER';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_DIVISION_NUMBER', 'CUSTOM_DIVISION_NUMBER', 20) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_DIVISION_NUMBER';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_BRANCH_NUMBER', 'CUSTOM_BRANCH_NUMBER', 20) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_BRANCH_NUMBER';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_DEPARTMENT_NUMBER', 'CUSTOM_DEPARTMENT_NUMBER', 20) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_DEPARTMENT_NUMBER';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_TEAM_NUMBER', 'CUSTOM_TEAM_NUMBER', 20) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_TEAM_NUMBER';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_COMPANY_NAME', 'CUSTOM_COMPANY_NAME', 40) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_COMPANY_NAME';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_DIVISION_NAME', 'CUSTOM_DIVISION_NAME', 40) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_DIVISION_NAME';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_BRANCH_NAME', 'CUSTOM_BRANCH_NAME', 40) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_BRANCH_NAME';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_DEPARTMENT_NAME', 'CUSTOM_DEPARTMENT_NAME', 40) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_DEPARTMENT_NAME';
        KeyFields := 'PR_CHECK_NBR';
      end;
      with CreateStringField(Payroll_ED_Detail, 'CUSTOM_TEAM_NAME', 'CUSTOM_TEAM_NAME', 40) do
      begin
        LookupDataSet := DM_PAYROLL.PR_CHECK;
        LookupKeyFields := 'PR_CHECK_NBR';
        LookupResultField := 'CUSTOM_TEAM_NAME';
        KeyFields := 'PR_CHECK_NBR';
      end;

      Payroll_ED_Detail.IndexFieldNames := s;
      FieldList := TList.Create;
      try
        Payroll_ED_Detail.GetFieldList(FieldList, s);
        for i := 0 to Pred(Payroll_ED_Detail.FieldCount) do
          if (Payroll_ED_Detail.Fields[i].FieldName = 'EE_Custom_Number') or
             (Payroll_ED_Detail.Fields[i].FieldName = 'Name') or
             (FieldList.IndexOf(Payroll_ED_Detail.Fields[i]) >= 0) then
            Payroll_ED_Detail.Fields[i].FieldKind := fkLookup
          else
            Payroll_ED_Detail.Fields[i].FieldKind := fkData;
      finally
        FieldList.Free;
      end;
      Payroll_ED_Detail.CreateDataSet;
      Payroll_ED_Detail.LogChanges := False;
    end;

    if Payroll_ED_Master.Active then
      Payroll_ED_Master.EmptyDataSet;
    if Payroll_ED_Detail.Active then
      Payroll_ED_Detail.EmptyDataSet;
    ED_Master_ID := 0;


    Payroll_ED_Master.DisableControls;
    Payroll_ED_Detail.DisableControls;
    Payroll_ED_Detail.UnprepareLookups;
    Payroll_ED_Detail.PrepareLookups;
    cdsCheckLines := TevClientDataSet.Create(nil);
    cdsCheckLines.AggregatesActive := True;
    cdsCheckLines.FieldDefs.Assign(DM_PAYROLL.PR_CHECK_LINES.FieldDefs);
    cdsCheckLines.CreateFields;
    with CreateStringField(cdsCheckLines, 'CHECK_TYPE', 'CHECK_TYPE', 1) do
    begin
      FieldKind := fkLookup;
      LookupDataSet := DM_PAYROLL.PR_CHECK;
      LookupKeyFields := 'PR_CHECK_NBR';
      LookupResultField := 'CHECK_TYPE';
      KeyFields := 'PR_CHECK_NBR';
    end;

    try
      cdsCheckLines.IndexDefs.Add('idxED', groupFieldNames, [ixCaseInsensitive]);
      cdsCheckLines.IndexDefs.Update;
      cdsCheckLines.IndexName := 'idxED';
      cdsCheckLines.Data := DM_PAYROLL.PR_CHECK_LINES.Data;
//      cdsCheckLines.PrepareLookups;

      cdsCheckLines.First;
      while not cdsCheckLines.EOF do
      begin
        with Payroll_ED_Detail do
        begin
          insert;
          FieldByName('Amount').Value := cdsCheckLines.FieldByName('Amount').Value;
          FieldByName('Hours_Pieces').Value := cdsCheckLines.FieldByName('HOURS_OR_PIECES').Value;
          FieldByName('PR_CHECK_NBR').Value := cdsCheckLines.FieldByName('PR_CHECK_NBR').Value;

          if not FieldByName('EE_Custom_Number').IsNull then
          begin
            newGroupKeyValues := cdsCheckLines[groupFieldNames];
            if not ValuesAreEqual( groupKeyValues, newGroupKeyValues, cGroupFieldsCount ) then
            begin
              if cGroupFieldsCount = 1 then
                oldCL_E_DS_NBR := groupKeyValues
              else
                oldCL_E_DS_NBR := groupKeyValues[0];
              if (oldCL_E_DS_NBR <> cdsCheckLines['CL_E_DS_NBR']) and
                 not DM_CLIENT.CL_E_DS.Locate('CL_E_DS_NBR', cdsCheckLines['CL_E_DS_NBR'], []) then
                raise EInconsistentData.CreateHelp('Could not find cl_e_ds', IDH_InconsistentData);
              groupKeyValues := newGroupKeyValues;

              inc(ED_Master_ID);

              Payroll_ED_Master.Append;
              Payroll_ED_Master.FieldByName('LINE_TYPE').AsString := cdsCheckLines.FieldByName('LINE_TYPE').AsString;
              Payroll_ED_Master.FieldByName('CHECK_TYPE').AsString := cdsCheckLines.FieldByName('CHECK_TYPE').AsString;
              Payroll_ED_Master.FieldByName('ED_NBR').AsInteger := ED_Master_ID;
              Payroll_ED_Master.FieldByName('CL_E_DS_NBR').AsInteger := cdsCheckLines.FieldByName('CL_E_DS_NBR').AsInteger;
              Payroll_ED_Master.FieldByName('ED_CODE').AsString := DM_CLIENT.CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;
              Payroll_ED_Master.FieldByName('DESCRIPTION').AsString := DM_CLIENT.CL_E_DS.FieldByName('DESCRIPTION').AsString;
            end
            else
              Payroll_ED_Master.Edit;

            Payroll_ED_Master.FieldByName('COUNT').AsInteger := Succ(Payroll_ED_Master.FieldByName('COUNT').AsInteger);
            if not cdsCheckLines.FieldByName('Amount').IsNull then
              Payroll_ED_Master.FieldByName('AMOUNT').AsFloat := Payroll_ED_Master.FieldByName('AMOUNT').AsFloat + cdsCheckLines.FieldByName('Amount').Value;
            if not cdsCheckLines.FieldByName('Hours_Or_Pieces').IsNull then
              Payroll_ED_Master.FieldByName('Hours_Pieces').AsFloat := Payroll_ED_Master.FieldByName('Hours_Pieces').AsFloat + cdsCheckLines.FieldByName('Hours_Or_Pieces').Value;
            Payroll_ED_Master.Post;

            FieldByName('ED_NBR').AsInteger := Payroll_ED_Master.FieldByName('ED_NBR').AsInteger;
            post;
          end
          else
            Cancel;
        end;

        cdsCheckLines.Next;
      end;
      Payroll_ED_Master.First;
    finally
      Payroll_ED_Master.EnableControls;
      Payroll_ED_Detail.First;
      Payroll_ED_Detail.EnableControls;
      cdsCheckLines.Free;
    end;

    ED_TOTAL_CALCULATED := True;

    dsED_Total.DataSet := Payroll_ED_Master;
    dsED_Detail.DataSet := Payroll_ED_Detail;

    result := True;
    if CalcForBatch then
      ED_BATCH_NBR := DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger;
  finally
    ctx_EndWait;
  end;
end;

procedure ReCalculate_ED_TOTALS(sepByLineType, sepByCheckType: boolean);
begin
  ED_TOTAL_CALCULATED := FALSE;
  Calculate_ED_TOTALS(sepByLineType, sepByCheckType);
end;

procedure ReCalculate_TAX_TOTALS(Separate: boolean);
begin
  TAX_TOTAL_CALCULATED := FALSE;
  Calculate_TAX_TOTALS(Separate);
end;

procedure ResetCalc;
begin
  ED_TOTAL_CALCULATED := False;
  TAX_TOTAL_CALCULATED := False;
  BATCH_QUICK_PAY_CALCULATED := False;
  ED_BATCH_NBR := 0;
  TAX_BATCH_NBR := 0;
  QUICK_PAY_BATCH_NBR := 0;
  QEDHolder.InvalidateAll;
end;

{ TSystemFrm }

procedure TPayrollFrm.InitVars;
begin
  FSeparateEDByLineType := False;
  FSeparateEDByCheckType := False;
  SeparateTax := False;
  CurrentTabSheetName := '';
  CurrentBatchTabSheet := '';
  bNewPayroll := False;
  bInsertWithWizard := False;
  bInsertWithCobraWizard := False;
  CalcForBatch := True;
  sFilterString := '';
  ResetCalc;
end;

function TPayrollFrm.DeactivatePackage: Boolean;
begin
  if (DetailPayrollOpen <> 0) and
     DM_EMPLOYEE.EE.Active and
     DM_CLIENT.CL_PERSON.Active then
  begin
    if DM_EMPLOYEE.EE.IndexDefs.IndexOf('SORT') < 0 then
    begin
      DM_EMPLOYEE.EE.AddIndex('SORT', 'CUSTOM_EMPLOYEE_NUMBER', [ixCaseInsensitive], '');
      DM_EMPLOYEE.EE.IndexDefs.Update;
    end;
    DM_EMPLOYEE.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
    DM_CLIENT.CL_PERSON.Locate('CL_PERSON_NBR', DM_EMPLOYEE.EE['CL_PERSON_NBR'], []);
  end;
  InitVars;
  Result := inherited DeactivatePackage;
  if Result then
  begin
//    PayrollCalculationInterface.EraseLimitedEDs;
    ctx_DataAccess.TempCommitDisable := False;
  end;

  DM_SYSTEM_STATE.SY_STATES.ClearAsOfDateOverride;
  DM_COMPANY.CO_STATES.ClearAsOfDateOverride;
  DM_EMPLOYEE.EE_STATES.ClearAsOfDateOverride;
  DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.ClearAsOfDateOverride;
  DM_SYSTEM_LOCAL.SY_COUNTY.ClearAsOfDateOverride;
  DM_SYSTEM_LOCAL.SY_LOCALS.ClearAsOfDateOverride;
  DM_COMPANY.CO_LOCAL_TAX.ClearAsOfDateOverride;
  DM_EMPLOYEE.EE_LOCALS.ClearAsOfDateOverride;
end;

function TPayrollFrm.InitPackage: Boolean;
begin
  if not FInitialized then
  begin
    QEDHolder := TQEDHolder.Create(nil);

    EDRefreshList := TStringList.Create;
    TaxRefreshList := TStringList.Create;
    CheckRefreshList := TStringList.Create;
    QBRefreshList := TStringList.Create;

    EDRefreshList.Sorted := True;
    TaxRefreshList.Sorted := True;
    CheckRefreshList.Sorted := True;
    QBRefreshList.Sorted := True;

    EDRefreshList.Duplicates := dupIgnore;
    TaxRefreshList.Duplicates := dupIgnore;
    CheckRefreshList.Duplicates := dupIgnore;
    QBRefreshList.Duplicates := dupIgnore;

    InitVars;
    SetDetailPayrollOpen(0);

    dsED_Detail := TevDataSource.Create(Self);
    dsTax_Detail := TevDataSource.Create(Self);
    dsTax_Total := TevDataSource.Create(Self);
    dsED_Total := TevDataSource.Create(Self);
    QUICK_PAY_DS := TevDataSource.Create(Self);

    dsED_Detail.MasterDataSource := dsED_Total;
    dsTax_Detail.MasterDataSource := dsTax_Total;
  end;

  Result := inherited InitPackage;
end;

function TPayrollFrm.PackageCaption: string;
begin
  Result := 'Pa&yroll';
end;

function TPayrollFrm.PackageSortPosition: string;
begin
  Result := '103';
end;

procedure TPayrollFrm.QuickPayDSAfterCancel(DataSet: TDataSet);
var
  i: integer;
begin
  for I := QPFieldCount to QuickPayDS.FieldCount - 1 do
    QuickPayDS.Fields[i].Tag := 0;
  OldValues := unAssigned;
end;

procedure TPayrollFrm.QuickPayDSAfterDelete(DataSet: TDataSet);
var
  i: integer;
begin
  for I := QPFieldCount to QuickPayDS.FieldCount - 1 do
    QuickPayDS.Fields[i].Tag := 0;
end;

procedure TPayrollFrm.QuickPayDSAfterPost(DataSet: TDataSet);
var
  i: integer;
begin
  for I := QPFieldCount to QuickPayDS.FieldCount - 1 do
    QuickPayDS.Fields[i].Tag := 0;
  CheckRefreshList.Add(IntToStr(DataSet.FieldByName('PR_CHECK_NBR').AsInteger));
  TAX_TOTAL_CALCULATED := False;
  ED_TOTAL_CALCULATED := False;
  QEDHolder.InvalidateCheck( DataSet.FieldByName('PR_CHECK_NBR').AsInteger );
  OldValues := unAssigned;
end;

procedure TPayrollFrm.QuickPayDSBeforeDelete(DataSet: TDataSet);
begin
  CheckRefreshList.Add(IntToStr(DataSet.FieldByName('PR_CHECK_NBR').AsInteger));
  TAX_TOTAL_CALCULATED := False;
  ED_TOTAL_CALCULATED := False;
  QEDHolder.InvalidateCheck( DataSet.FieldByName('PR_CHECK_NBR').AsInteger );
  DeletingCheck := True;
  if DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', DataSet.FieldByName('PR_CHECK_NBR').Value, []) then
    DM_PAYROLL.PR_CHECK.Delete;
  DeletingCheck := False;
end;

procedure TPayrollFrm.QuickPayDSBeforePost(DataSet: TDataSet);
var
  I: Integer;
  d: Variant;
  li: Integer;
begin
  if DataSet.FieldByName('NUMBER').AsString = '' then
    raise EUpdateError.CreateHelp('Employee can''t be empty', IDH_ConsistencyViolation);
  with DataSet do
  begin
    if State in [dsInsert] then
    begin
      DM_PAYROLL.PR_CHECK.Insert;
      try
        if DM_EMPLOYEE.EE.Locate('Custom_Employee_Number', FieldByName('NUMBER').AsString, []) then
        begin
          DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value := DM_EMPLOYEE.EE.FieldByName('EE_NBR').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_COMPANY_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DIVISION_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_DIVISION_NUMBER').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_BRANCH_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_BRANCH_NUMBER').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DEPARTMENT_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_DEPARTMENT_NUMBER').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_TEAM_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_TEAM_NUMBER').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_COMPANY_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_COMPANY_NAME').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DIVISION_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_DIVISION_NAME').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_BRANCH_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_BRANCH_NAME').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_DEPARTMENT_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_DEPARTMENT_NAME').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_TEAM_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_TEAM_NAME').Value;
          DM_PAYROLL.PR_CHECK.FieldByName('SOCIAL_SECURITY_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('SOCIAL_SECURITY_NUMBER').Value;
          FieldByName('CUSTOM_COMPANY_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_COMPANY_NUMBER').Value;
          FieldByName('CUSTOM_DIVISION_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_DIVISION_NUMBER').Value;
          FieldByName('CUSTOM_BRANCH_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_BRANCH_NUMBER').Value;
          FieldByName('CUSTOM_DEPARTMENT_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_DEPARTMENT_NUMBER').Value;
          FieldByName('CUSTOM_TEAM_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_TEAM_NUMBER').Value;
          FieldByName('CUSTOM_COMPANY_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_COMPANY_NAME').Value;
          FieldByName('CUSTOM_DIVISION_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_DIVISION_NAME').Value;
          FieldByName('CUSTOM_BRANCH_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_BRANCH_NAME').Value;
          FieldByName('CUSTOM_DEPARTMENT_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_DEPARTMENT_NAME').Value;
          FieldByName('CUSTOM_TEAM_NAME').Value := DM_EMPLOYEE.EE.FieldByName('CUSTOM_TEAM_NAME').Value;
          FieldByName('SOCIAL_SECURITY_NUMBER').Value := DM_EMPLOYEE.EE.FieldByName('SOCIAL_SECURITY_NUMBER').Value;
        end;
      except
        DM_PAYROLL.PR_CHECK.Cancel;
        raise EUpdateError.CreateHelp('Employee is required!', IDH_ConsistencyViolation);
      end;
      with ctx_PayrollCalculation do
        AssignCheckDefaults(0, GetNextPaymentSerialNbr);
      DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_REGULAR;
      DM_PAYROLL.PR_CHECK.UpdateBlobData('NOTES_NBR', DM_PAYROLL.PR.FieldByName('PAYROLL_COMMENTS').AsString);
      DM_PAYROLL.PR_CHECK.Post;
      DataSet.FieldByName('CHECK_TYPE').ReadOnly := False;
      DataSet.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_REGULAR;
      DataSet.FieldByName('CHECK_TYPE').ReadOnly := True;

      FieldByName('PR_CHECK_NBR').Value := DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').Value;

      if DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.Value < 0 then
        QBRefreshList.Add(DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.AsString);

      ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
        DM_PAYROLL.PR,
        DM_PAYROLL.PR_BATCH,
        DM_PAYROLL.PR_CHECK,
        DM_PAYROLL.PR_CHECK_LINES,
        DM_PAYROLL.PR_CHECK_STATES,
        DM_PAYROLL.PR_CHECK_SUI,
        DM_PAYROLL.PR_CHECK_LOCALS,
        DM_PAYROLL.PR_CHECK_LINE_LOCALS,
        DM_PAYROLL.PR_SCHEDULED_E_DS,
        DM_CLIENT.CL_E_DS,
        DM_CLIENT.CL_PERSON,
        DM_CLIENT.CL_PENSION,
        DM_COMPANY.CO,
        DM_COMPANY.CO_E_D_CODES,
        DM_COMPANY.CO_STATES,
        DM_EMPLOYEE.EE,
        DM_EMPLOYEE.EE_SCHEDULED_E_DS,
        DM_EMPLOYEE.EE_STATES,
        DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
        DM_SYSTEM_STATE.SY_STATES,
        False,
        (DM_PAYROLL.PR_BATCH.FieldByName('PAY_SALARY').AsString = 'Y'),
        (DM_PAYROLL.PR_BATCH.FieldByName('PAY_STANDARD_HOURS').AsString = 'Y'),
        True
        );
    end
    else
      DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', FieldByName('PR_CHECK_NBR').Value, []);

    if DM_PAYROLL.PR_CHECK_LINES.State <> dsBrowse then
      DM_PAYROLL.PR_CHECK_LINES.Cancel;
    li := -1;
    for I := QPFieldCount to QuickPayDS.FieldCount - 1 do
      if QuickPayDS.Fields[I].Tag = 1 then
        li := i;

    if li <> -1 then
      ctx_DataAccess.CheckMaxAmountAndHours := False;
    try
      for I := QPFieldCount to QuickPayDS.FieldCount - 1 do
        if QuickPayDS.Fields[I].Tag = 1 then
        begin
          if li = i then
            ctx_DataAccess.CheckMaxAmountAndHours := True;
          SetValueForBatch_E_D(FieldByName('PR_CHECK_NBR').Value, QuickPayDS.Fields[I].FieldName, QuickPayDS.Fields[I].Value);
        end;
    finally
      ctx_DataAccess.CheckMaxAmountAndHours := True;
    end;
    for I := QPFieldCount to QuickPayDS.FieldCount - 1 do
    begin
      if Copy(QuickPayDS.Fields[I].FieldName, 1, 4) <> 'Tax_' then
      begin
        d := GetValueForBatch_E_D(FieldByName('PR_CHECK_NBR').Value, QuickPayDS.Fields[I].FieldName);
        if QuickPayDS.Fields[I].AsCurrency <> VarAsType(ConvertNull(d, 0), varCurrency) then
          QuickPayDS.Fields[I].Value := d;
      end;
    end;
  end;
end;

procedure TPayrollFrm.QuickPayDSFieldChange(Sender: TField);
begin
  if not VarIsEmpty(OldValues) and
     ({VarIsNull(Sender.Value) or }(Sender.Value <> OldValues[Sender.Index])) then
    Sender.Tag := 1
  else
    Sender.Tag := 0;
end;

function TPayrollFrm.UninitPackage: Boolean;
begin
  if FInitialized then
  begin
    EDRefreshList.Free;
    TaxRefreshList.Free;
    CheckRefreshList.Free;
    QBRefreshList.Free;
    if assigned(QuickPayDS) then
    begin
      QuickPayDS.Aggregates.Clear;
      QuickPayDS.Free;
      QuickPayDS := nil;
    end;
    FreeAndNil( QEDHolder );
  end;

  Result := inherited UninitPackage;
end;

procedure TPayrollFrm.UserPackageStructure;
begin
  AddStructure('Payroll|010', 'TREDIT_PAYROLL', True);
  AddStructure('Batch|020', 'TREDIT_BATCH');
  AddStructure('Check|030', 'TREDIT_CHECK');
end;

function TPayrollFrm.ActivatePackage;
begin
  OASDI_MEDICARE_Readonly := ctx_AccountRights.Functions.GetState('MANUAL_OASDI_MEDICARE') <> stEnabled;

  ctx_DataAccess.RecalcLineBeforePost := True;
  DM_TEMPORARY.TMP_CL.Activate;
  if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
    raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);

  if not DM_COMPANY.CO.Active or
     (DM_COMPANY.CO.ClientID <> ClNbr) or
     (DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger <> CoNbr) or
     not DM_PAYROLL.PR.Active or
     (DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger <> DetailPayrollOpen) then
    SetDetailPayrollOpen(0);

  Result := inherited ActivatePackage(WorkPlace);

  if DetailPayrollOpen <> 0 then
  begin
    CheckSupportDatasets(ClNbr, CoNbr);
    OpenDetailPayroll;
  end;
  ctx_DataAccess.TempCommitDisable := True;
end;

procedure TPayrollFrm.QuickPayDSCalculate(DataSet: TDataSet);
begin
  if Pos(';' + DataSet.FieldByName('EE_NBR').AsString + ';', sFilterString) > 0 then
    DataSet.FieldByName('NOTES').Value := 'Y'
  else
    DataSet.FieldByName('NOTES').Value := 'N';
end;

function TPayrollFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

procedure TPayrollFrm.OpenPayroll;
begin
  OpenDetailPayroll;
end;

procedure TPayrollFrm.btnRefreshClick(Sender: TObject);
begin
  if CanClose and AskCommitChanges then
  begin
    CloseDetailPayroll;
    ResetCalc;
    inherited;
  end;
end;

procedure TPayrollFrm.QuickPayDSBeforeEdit(DataSet: TDataSet);
var
  i: Integer;
begin
  OldValues := VarArrayCreate([0, Pred(DataSet.FieldCount)], varVariant);
  for i := 0 to Pred(DataSet.FieldCount) do
    OldValues[i] := DataSet.Fields[i].Value;
end;

procedure TPayrollFrm.QuickPayDSBeforeInsert(DataSet: TDataSet);
var
  i: Integer;
begin
  OldValues := VarArrayCreate([0, Pred(DataSet.FieldCount)], varVariant);
  for i := 0 to Pred(DataSet.FieldCount) do
    OldValues[i] := null;
end;

procedure TPayrollFrm.AfterConstruction;
begin
  inherited;
end;

procedure TPayrollFrm.CheckToaBalance;

  function ToaIsUsed: Boolean;
  begin
    Result := DM_COMPANY.CO['CHECK_TIME_OFF_AVAIL'] = GROUP_BOX_YES;
    if not Result then
    begin
      DM_COMPANY.CO_TIME_OFF_ACCRUAL.DataRequired('CO_NBR='+ IntToStr(DM_COMPANY.CO['CO_NBR']));
      DM_COMPANY.CO_TIME_OFF_ACCRUAL.First;
      while not DM_COMPANY.CO_TIME_OFF_ACCRUAL.Eof and not Result do
      begin
        Result := DM_COMPANY.CO_TIME_OFF_ACCRUAL['CHECK_TIME_OFF_AVAIL'] = GROUP_BOX_YES;
        DM_COMPANY.CO_TIME_OFF_ACCRUAL.Next;
      end;
    end;
  end;


begin
  if not FCheckToaSetupAccessActivated then
  begin
    FCheckToaSetupAccessActivated := True;
    if ToaIsUsed then
      FCheckToaSetupAccess := TPayrollEntryCheckToaSetupAccess.Create(DM_PAYROLL.PR['CO_NBR'],
        DM_PAYROLL.PR['CHECK_DATE'], DM_PAYROLL.PR['CHECK_DATE']);
  end;
  if Assigned(FCheckToaSetupAccess) then
    FCheckToaSetupAccess.Check;
end;

procedure TPayrollFrm.ClearCheckToaSetupAccess;
begin
  FreeAndNil(FCheckToaSetupAccess);
  FCheckToaSetupAccessActivated := False;
end;

function TPayrollFrm.SilentCheckToaBalance: Boolean;
begin
  if not FCheckToaSetupAccessActivated then
  begin
    FCheckToaSetupAccessActivated := True;
    FCheckToaSetupAccess := TPayrollEntryCheckToaSetupAccess.Create(DM_PAYROLL.PR['CO_NBR'],
      DM_PAYROLL.PR['CHECK_DATE'], DM_PAYROLL.PR['CHECK_DATE']);
  end;
  if Assigned(FCheckToaSetupAccess) then
    Result := FCheckToaSetupAccess.CheckBalance(True)
  else
    Result := True;
end;

procedure TPayrollFrm.BeforeDestruction;
begin
  ClearCheckToaSetupAccess;
  inherited;
end;

initialization
  RegisterClass(TevDataSource);
  Current_FromDate := Date - 35;
  Mainboard.ModuleRegister.RegisterModule(@GetPayrollScreens, IevPayrollScreens, 'Screens Payroll');

finalization
  FreeandNil(RemotePayrollFrm);

end.


