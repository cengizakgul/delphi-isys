// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_PR_AGENCY_CHECK;

interface

uses
   SDataStructure, SPD_EDIT_PR_BASE, StdCtrls, DBCtrls,
  wwdblook, Mask, wwdbedit, DBActns, Classes, ActnList, Db, Wwdatsrc,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls,
  EvConsts, Dialogs, SPrintingRoutines, EvUtils, evTypes,
  SPackageEntry, SPopulateRecords, EvSecElement, SysUtils, Variants,
  ISBasicClasses, SDDClasses, SDataDictsystem, SDataDictclient,
  SDataDicttemp, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms;

type
  TEDIT_PR_AGENCY_CHECK = class(TEDIT_PR_BASE)
    wwDBGrid2: TevDBGrid;
    Label1: TevLabel;
    DBEdit1: TevDBEdit;
    Label2: TevLabel;
    DBEdit2: TevDBEdit;
    Label3: TevLabel;
    DBEdit3: TevDBEdit;
    Label4: TevLabel;
    Label5: TevLabel;
    wwlcCL_AGENCY: TevDBLookupCombo;
    DBMemo1: TEvDBMemo;
    Memo1: TevMemo;
    BitBtn11: TevBitBtn;
    BitBtn2: TevBitBtn;
    wwlcGLOBAL_TAX_AGENCY: TevDBLookupCombo;
    Label9: TevLabel;
    evPanel1: TevPanel;
    Label6: TevLabel;
    DBText1: TevDBText;
    Label7: TevLabel;
    DBText2: TevDBText;
    Label8: TevLabel;
    BitBtn3: TevBitBtn;
    TabSheet3: TTabSheet;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    evDBLookupCombo1: TevDBLookupCombo;
    evLabel2: TevLabel;
    evDBText1: TevDBText;
    btnUnvoid: TevBitBtn;
    edABANumber: TevDBEdit;
    evLabel3: TevLabel;
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure PRPageControlChange(Sender: TObject);
    procedure btnUnvoidClick(Sender: TObject);
  private
    procedure EnableControls(Enable: Boolean);
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure AfterDataSetsReopen; override;
    procedure SetReadOnly(Value: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure AfterClick(Kind: Integer); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses
  SLogCheckReprint, SPD_ChooseLiabs, Consts, SSecurityInterface;

{$R *.DFM}

procedure TEDIT_PR_AGENCY_CHECK.EnableControls(Enable: Boolean);
begin
  DBEdit1.Enabled := Enable;
  DBEdit2.Enabled := Enable;
  DBEdit3.Enabled := Enable;
  DBMemo1.Enabled := Enable;
  wwlcCL_AGENCY.Enabled := Enable;
  wwlcGLOBAL_TAX_AGENCY.Enabled := Enable;
  evDBLookupCombo1.Enabled := Enable;
end;

procedure TEDIT_PR_AGENCY_CHECK.BitBtn11Click(Sender: TObject);
var
  Header, EIN: string;
begin
  inherited;
  if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').IsNull then
    Exit;
  if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString = MISC_CHECK_TYPE_BILLING then
  begin
    EvMessage('Can not calculate stub for billing check yet!');
    Exit;
  end;
  ctx_StartWait;
  try
    if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString = MISC_CHECK_TYPE_AGENCY then
      Memo1.Text := CreateAgencyCheckStubDetails(100);
    if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString = MISC_CHECK_TYPE_TAX then
      Memo1.Text := CreateTaxCheckStubDetails(Header, EIN);
    if Memo1.Text = '' then
      Memo1.Text := 'This check has no details.';
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_PR_AGENCY_CHECK.BitBtn2Click(Sender: TObject);
begin
  inherited;
  if not (DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.State in [dsInsert, dsEdit]) then
    DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Edit;
  DBMemo1.Text := Memo1.Text;
end;

procedure TEDIT_PR_AGENCY_CHECK.BitBtn3Click(Sender: TObject);
var
  TempList: TStringList;
begin
  inherited;
  TempList := TStringList.Create;
  try
    TempList.Add(DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
    LogCheckReprint(TempList, True, DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger);
    ctx_StartWait;
    try
      case DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] of
        MISC_CHECK_TYPE_AGENCY, MISC_CHECK_TYPE_AGENCY_VOID:
          ctx_PayrollCheckPrint.PrintChecks(0, TempList, ctAgency, False, True, DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString);
        MISC_CHECK_TYPE_TAX, MISC_CHECK_TYPE_TAX_VOID:
          ctx_PayrollCheckPrint.PrintChecks(0, TempList, ctTax, False, True, DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString);
        MISC_CHECK_TYPE_BILLING, MISC_CHECK_TYPE_BILLING_VOID:
          ctx_PayrollCheckPrint.PrintChecks(0, TempList, ctBilling, False, True, DM_COMPANY.CO.FieldByName('MISC_CHECK_FORM').AsString);
      end;
    finally
      ctx_EndWait;
    end;
  finally
    TempList.Free;
  end;
end;

procedure TEDIT_PR_AGENCY_CHECK.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;

  if csDestroying in wwdsDetail.ComponentState then
    Exit;

  if DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PROCESSED then
    BitBtn3.Enabled := ctx_AccountRights.Functions.GetState('USER_CAN_REPRINT_MISC_CHECKS') = stEnabled
  else
    BitBtn3.Enabled := False;
  if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString = MISC_CHECK_TYPE_AGENCY then
  begin
    EnableControls(True);
    wwlcGLOBAL_TAX_AGENCY.Enabled := False;
    evDBLookupCombo1.Enabled := False;
    Label8.Caption := 'Agency Check';
  end
  else
  begin
    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT then
    begin
      EnableControls(True);
      wwlcCL_AGENCY.Enabled := False;
      if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString = MISC_CHECK_TYPE_BILLING then
      begin
        wwlcGLOBAL_TAX_AGENCY.Enabled := False;
        evDBLookupCombo1.Enabled := False;
      end;
    end
    else
    begin
      EnableControls(False);
    end;

    if not DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').IsNull then
      case DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').AsString[1] of
        MISC_CHECK_TYPE_TAX:
          Label8.Caption := 'Tax Check';
        MISC_CHECK_TYPE_BILLING:
          Label8.Caption := 'Billing Check';
        MISC_CHECK_TYPE_AGENCY_VOID:
          Label8.Caption := 'Agency Check (Void)';
        MISC_CHECK_TYPE_TAX_VOID:
          Label8.Caption := 'Tax Check (Void)';
        MISC_CHECK_TYPE_BILLING_VOID:
          Label8.Caption := 'Billing Check (Void)';
      end
    else
      Label8.Caption := '';
  end;
  btnUnvoid.Enabled := DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('CHECK_STATUS').AsString <> MISC_CHECK_STATUS_VOID;
  if wwdsDetail.DataSet.Active and
     DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Active then
    DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.RetrieveCondition := 'SY_GLOBAL_AGENCY_NBR = ' + IntToStr(wwdsDetail.DataSet.FieldByName('SY_GLOBAL_AGENCY_NBR').AsInteger);
  Memo1.Clear;
end;

procedure TEDIT_PR_AGENCY_CHECK.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  inherited;
end;

function TEDIT_PR_AGENCY_CHECK.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR_MISCELLANEOUS_CHECKS;
end;

function TEDIT_PR_AGENCY_CHECK.GetInsertControl: TWinControl;
begin
  Result := wwlcCL_AGENCY;
end;

procedure TEDIT_PR_AGENCY_CHECK.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  AddDS(DM_COMPANY.CO_TAX_DEPOSITS, EditDatasets);
  inherited;
  AddDS(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets);
  AddDS(DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE, SupportDataSets);
  AddDS([DM_COMPANY.CO_FED_TAX_LIABILITIES, DM_COMPANY.CO_STATE_TAX_LIABILITIES,
         DM_COMPANY.CO_SUI_LIABILITIES, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES], EditDatasets);
end;

procedure TEDIT_PR_AGENCY_CHECK.PRPageControlChange(Sender: TObject);
begin
  inherited;
  Panel1.Visible := PageControl1.ActivePage = TabSheet1;
  evPanel1.Visible := not Panel1.Visible;
end;

procedure TEDIT_PR_AGENCY_CHECK.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = NavInsert then
    PopulatePRMiscCheckRecord;
end;

procedure TEDIT_PR_AGENCY_CHECK.AfterDataSetsReopen;
begin
  inherited;
  DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.IndexFieldNames := 'STATE; ADDITIONAL_NAME';
end;

procedure TEDIT_PR_AGENCY_CHECK.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
  i: Integer;
  Mark: TBookmarkStr;
  ds: TevClientDataSet;
  Sum: Currency;
  iTaxDepositNbr: Integer;

  procedure UnassignTaxLiab(ds: TevClientDataSet);
  begin
    while ds.Locate('CO_TAX_DEPOSITS_NBR', iTaxDepositNbr, []) do
    begin
      ds.Edit;
      if DM_COMPANY.CO['TAX_SERVICE'] = GROUP_BOX_YES then
        ds['STATUS'] := TAX_DEPOSIT_STATUS_IMPOUNDED
      else
        ds['STATUS'] := TAX_DEPOSIT_STATUS_PENDING;
      ds['CO_TAX_DEPOSITS_NBR'] := null;
      ds.Post;
    end;
  end;

begin
  inherited;
  if (Kind = NavOK) and
     (DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.State = dsInsert) and
     DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull and
     (DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value = MISC_CHECK_TYPE_TAX) and
     (EvMessage('Do you want to attach tax deposit to this check?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
    with TChooseLiabs.Create(nil) do
    try
      CoNbr := DM_COMPANY.CO['CO_NBR'];
      if ShowModal = mrOK then
      begin

        ctx_StartWait;
        cdsLiabs.DisableControls;
        Mark := cdsLiabs.Bookmark;
        ctx_DataAccess.TempCommitDisable := True;
        try
          DM_COMPANY.CO_FED_TAX_LIABILITIES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR = ' + IntToStr(CoNbr));
          DM_COMPANY.CO_STATE_TAX_LIABILITIES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR = ' + IntToStr(CoNbr));
          DM_COMPANY.CO_SUI_LIABILITIES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR = ' + IntToStr(CoNbr));
          DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR = ' + IntToStr(CoNbr));
          DM_COMPANY.CO_TAX_DEPOSITS.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR = ' + IntToStr(CoNbr));
          ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_FED_TAX_LIABILITIES, DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                        DM_COMPANY.CO_SUI_LIABILITIES, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,
                        DM_COMPANY.CO_TAX_DEPOSITS]);

          DM_COMPANY.CO_TAX_DEPOSITS.Append;
          Sum := 0;

          for i := 0 to Pred(grdLiabs.SelectedList.Count) do
          begin
            cdsLiabs.GotoBookmark(grdLiabs.SelectedList[i]);
            if cdsLiabs['Level_'] = 'F' then
              ds := DM_COMPANY.CO_FED_TAX_LIABILITIES
            else if cdsLiabs['Level_'] = 'S' then
              ds := DM_COMPANY.CO_STATE_TAX_LIABILITIES
            else if cdsLiabs['Level_'] = 'U' then
              ds := DM_COMPANY.CO_SUI_LIABILITIES
            else
              ds := DM_COMPANY.CO_LOCAL_TAX_LIABILITIES;

            if ds.Locate(ds.Name + '_NBR', cdsLiabs['Nbr'], []) then
            begin
              ds.Edit;
              ds['CO_TAX_DEPOSITS_NBR'] := DM_COMPANY.CO_TAX_DEPOSITS['CO_TAX_DEPOSITS_NBR'];
              ds['STATUS'] := TAX_DEPOSIT_STATUS_PENDING;
              ds.Post;
              Sum := Sum + ds.FieldbyName('Amount').AsCurrency;
            end
          end;

          DM_COMPANY.CO_TAX_DEPOSITS['TAX_PAYMENT_REFERENCE_NUMBER'] := 'Check# unknown '+ DateTimeToStr(Now);
          DM_COMPANY.CO_TAX_DEPOSITS['STATUS'] := TAX_DEPOSIT_STATUS_PENDING;
          DM_COMPANY.CO_TAX_DEPOSITS['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
          DM_COMPANY.CO_TAX_DEPOSITS['DEPOSIT_TYPE'] := TAX_DEPOSIT_TYPE_CHECK;

          DM_COMPANY.CO_TAX_DEPOSITS.Post;

          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['CO_TAX_DEPOSITS_NBR'] := DM_COMPANY.CO_TAX_DEPOSITS['CO_TAX_DEPOSITS_NBR'];
          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['MISCELLANEOUS_CHECK_AMOUNT'] := Sum;
        finally
          cdsLiabs.Bookmark := Mark;
          cdsLiabs.EnableControls;
          ctx_DataAccess.TempCommitDisable := False;
          ctx_EndWait;
        end;
      end;
    finally
      Free;
    end;
  if ((Kind = NavCancel) and
       (DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.State = dsInsert) or
      (Kind = NavDelete)) and
     not DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull and
     (DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('MISCELLANEOUS_CHECK_TYPE').Value = MISC_CHECK_TYPE_TAX) then
  begin
    Handled := True;
    if (Kind = NavDelete) and
       (EvMessage('Are you sure?', mtConfirmation, mbOKCancel) <> mrOK) then
      Exit;
    ctx_StartWait;
    ctx_DataAccess.TempCommitDisable := True;
    try
      DM_COMPANY.CO_FED_TAX_LIABILITIES.CheckDSCondition('CO_NBR = ' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));
      DM_COMPANY.CO_STATE_TAX_LIABILITIES.CheckDSCondition('CO_NBR = ' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));
      DM_COMPANY.CO_SUI_LIABILITIES.CheckDSCondition('CO_NBR = ' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));
      DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.CheckDSCondition('CO_NBR = ' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));
      DM_COMPANY.CO_TAX_DEPOSITS.CheckDSCondition('CO_NBR = ' + IntToStr(DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger));
      ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_FED_TAX_LIABILITIES, DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                    DM_COMPANY.CO_SUI_LIABILITIES, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,
                    DM_COMPANY.CO_TAX_DEPOSITS]);

      iTaxDepositNbr := DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;

      UnassignTaxLiab(DM_COMPANY.CO_FED_TAX_LIABILITIES);
      UnassignTaxLiab(DM_COMPANY.CO_STATE_TAX_LIABILITIES);
      UnassignTaxLiab(DM_COMPANY.CO_SUI_LIABILITIES);
      UnassignTaxLiab(DM_COMPANY.CO_LOCAL_TAX_LIABILITIES);

      if Kind = NavDelete then
        DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Delete
      else
        DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Cancel;
    finally
      ctx_DataAccess.TempCommitDisable := False;
      ctx_EndWait;
    end;
    if DM_COMPANY.CO_TAX_DEPOSITS.Locate('CO_TAX_DEPOSITS_NBR', iTaxDepositNbr, []) then
      DM_COMPANY.CO_TAX_DEPOSITS.Delete;
  end;
end;

procedure TEDIT_PR_AGENCY_CHECK.btnUnvoidClick(Sender: TObject);
//begin gdy
  function Confirm: boolean;
  var
    i: integer;
  begin
    with CreateMessageDialog('Unvoiding checks is extremely dangerous.  Are you sure you want to continue?', mtConfirmation, [mbYes, mbNo]) do
      try
        for I := 0 to ComponentCount - 1 do
          if Components[I] is TButton then
          begin
            (Components[I] as TButton).Default := (Components[I] as TButton).Caption = SMsgDlgNo;
            if (Components[I] as TButton).Default then
              ActiveControl := Components[I] as TButton;
          end;
        Result := ShowModal = mrYes;
      finally
        Free;
      end;
  end;
//end gdy
var
  AllOK: Boolean;
begin
  inherited;
  if Confirm then //gdy
  begin
    ctx_DataAccess.UnlockPR;
    AllOK := False;
    try
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Edit;
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['CHECK_STATUS'] := MISC_CHECK_STATUS_OUTSTANDING;
      DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Post;
      AllOK := True;
    finally
      ctx_DataAccess.LockPR(AllOK);
    end;
// Clear Bank Account Register
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR='
      + DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString);
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
    while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.EOF do
    begin
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Outstanding;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
  end;
end;

procedure TEDIT_PR_AGENCY_CHECK.SetReadOnly(Value: Boolean);
begin
  inherited;
  btnUnvoid.SecurityRO := btnUnvoid.SecurityRO or (ctx_AccountRights.Functions.GetState('ABILITY_UNVOID_CHECK') <> stEnabled);
end;

initialization
  RegisterClass(TEDIT_PR_AGENCY_CHECK);

end.
