// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_PR_CHECK;

interface

uses
   SDataStructure, SPD_EDIT_PR_BASE, StdCtrls, ExtCtrls, DBCtrls, Wwdotdot,
  Wwdbcomb, wwdblook, Mask, wwdbedit, DBActns, Classes, ActnList, Db, EvTypes,
  Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls, EvContext,
  EvConsts, EvUtils, SysUtils, SPackageEntry, SPopulateRecords, Dialogs,
  EvSecElement, SDDClasses, ISBasicClasses, SDataDictclient, SDataDicttemp,
  EvUIUtils, EvUIComponents, EvClientDataSet, SPAMovingWagesParams, SPAYear,
  isUIwwDBComboBox, isUIDBMemo, isUIwwDBLookupCombo, isUIwwDBEdit, ImgList,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, evDataset, EvCommonInterfaces;

type
  TEDIT_PR_CHECK = class(TEDIT_PR_BASE)
    TabSheet2: TTabSheet;
    wwDBGrid3: TevDBGrid;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBRadioGroup2: TevDBRadioGroup;
    DBRadioGroup3: TevDBRadioGroup;
    DBRadioGroup4: TevDBRadioGroup;
    DBRadioGroup5: TevDBRadioGroup;
    DBRadioGroup8: TevDBRadioGroup;
    DBRadioGroup7: TevDBRadioGroup;
    DBRadioGroup6: TevDBRadioGroup;
    Label13: TevLabel;
    DBEdit12: TevDBEdit;
    DBEdit14: TevDBEdit;
    DBEdit15: TevDBEdit;
    DBEdit16: TevDBEdit;
    DBEdit17: TevDBEdit;
    Label18: TevLabel;
    Label17: TevLabel;
    Label16: TevLabel;
    Label15: TevLabel;
    DBRadioGroup11: TevDBRadioGroup;
    DBRadioGroup13: TevDBRadioGroup;
    DBRadioGroup14: TevDBRadioGroup;
    DBRadioGroup17: TevDBRadioGroup;
    DBRadioGroup9: TevDBRadioGroup;
    DBRadioGroup16: TevDBRadioGroup;
    DBRadioGroup15: TevDBRadioGroup;
    wwDBComboBox1: TevDBComboBox;
    Label28: TevLabel;
    DBRadioGroup18: TevDBRadioGroup;
    Label9: TevLabel;
    wwDBComboBox3: TevDBComboBox;
    DBRadioGroup1: TevDBRadioGroup;
    DBRadioGroup10: TevDBRadioGroup;
    Panel111: TevPanel;
    Label11: TevLabel;
    wwDBLookupCombo2: TevDBLookupCombo;
    evPanel1: TevPanel;
    Label10: TevLabel;
    DBText3: TevDBText;
    bbtnCheckFinder: TevSpeedButton;
    wwDBComboBox4: TevDBComboBox;
    wwDBGrid2: TevDBGrid;
    evSplitter1: TevSplitter;
    wwdsSubMaster1: TevDataSource;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    evSpeedButton1: TevSpeedButton;
    pnlLocation: TevPanel;
    evLabel2: TevLabel;
    evDBText1: TevDBText;
    DBText1: TevDBText;
    evPanel2: TevPanel;
    evLabel3: TevLabel;
    evDBText2: TevDBText;
    evLabel4: TevLabel;
    evDBText3: TevDBText;
    cbCheckType945: TevDBComboBox;
    lblCheckType945: TevLabel;
    evButton1: TevBitBtn;
    Button1: TevBitBtn;
    evPanel3: TevPanel;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    Label5: TevLabel;
    Label31: TevLabel;
    Label8: TevLabel;
    DBEdit2: TevDBEdit;
    DBEdit3: TevDBEdit;
    DBEdit5: TevDBEdit;
    wwlcEmployee: TevDBLookupCombo;
    DBMemo2: TEvDBMemo;
    wwDBComboBox2: TevDBComboBox;
    btnUnvoid: TevBitBtn;
    btnReattachStatesAndLocals: TevSpeedButton;
    evDBRadioGroup1: TevDBRadioGroup;
    drgpReciprocate_SUI: TevDBRadioGroup;
    butnProrateFICA: TevBitBtn;
    evDBRadioGroup2: TevDBRadioGroup;
    lblABANumber: TevLabel;
    edAbaNumber: TevDBEdit;
    evDBRadioGroup3: TevDBRadioGroup;
    btnPAReattachLocals: TevSpeedButton;
    procedure bbtnCheckFinderClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure PRPageControlChange(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwdsSubMaster1DataChange(Sender: TObject; Field: TField);
    procedure PRNextExecute(Sender: TObject);
    procedure PRPriorExecute(Sender: TObject);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
    procedure evButton1Click(Sender: TObject);
    procedure btnUnvoidClick(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnReattachStatesAndLocalsClick(Sender: TObject);
    procedure butnProrateFICAClick(Sender: TObject);
    procedure btnPAReattachLocalsClick(Sender: TObject);
  private
    EmployeeBeforeInsert: String;
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure ReopenPRCheckWithLines; override;
    procedure SetReadOnly(Value: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Deactivate; override;
    procedure Activate; override;
    procedure AfterClick(Kind: Integer); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses
  SPD_CheckFinder, SPD_AdjustShortfalls, SSecurityInterface, SPD_TaxCalculator, consts,
  SPD_EDIT_Open_BASE, SPD_ReattachStatesLocals, SPD_PAReattachLocals;

{$R *.DFM}

var
  OldCheckNbr, OldBatchNbr, OldPayrollNbr: Integer;
  
procedure TEDIT_PR_CHECK.bbtnCheckFinderClick(Sender: TObject);
begin
  inherited;
  if bbtnCheckFinder.Hint = 'Check Finder (F12)' then
  begin
    CheckFinder := TCheckFinder.Create(Self);
    with CheckFinder, DM_PAYROLL do
    try
      CheckFinder.editEENbr.Text := Trim(DM_PAYROLL.PR_CHECK.FieldByName('EE_Custom_Number').AsString);
      if ShowModal = mrOK then
        if CheckNumber <> 0 then
        begin
          OldPayrollNbr := PR.FieldByName('PR_NBR').Value;
          OldBatchNbr := PR_BATCH.FieldByName('PR_BATCH_NBR').Value;
          OldCheckNbr := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
          PR.Locate('PR_NBR', wwcsFinder.FieldByName('PR_NBR').Value, []);
          PR_BATCH.Locate('PR_BATCH_NBR', wwcsFinder.FieldByName('PR_BATCH_NBR').Value, []);
          ReopenPRChecks;
          PR_CHECK.Locate('PR_CHECK_NBR', wwcsFinder.FieldByName('PR_CHECK_NBR').Value, []);
          bbtnCheckFinder.Hint := 'Get Back (F12)';
        end;
    finally
      CheckFinder.Free;
    end;
  end
  else
    with DM_PAYROLL do
    begin
      PR.Locate('PR_NBR', OldPayrollNbr, []);
      PR_BATCH.Locate('PR_BATCH_NBR', OldBatchNbr, []);
      ReopenPRChecks;
      PR_CHECK.Locate('PR_CHECK_NBR', OldCheckNbr, []);
      bbtnCheckFinder.Hint := 'Check Finder (F12)';
    end;
end;

procedure TEDIT_PR_CHECK.Button1Click(Sender: TObject);
begin
  inherited;
  if wwDBLookupCombo2.LookupValue <> '' then
    with wwdsDetail.DataSet, DM_PAYROLL, DM_COMPANY, DM_EMPLOYEE do
      if (not (BOF and EOF)) and (wwDBLookupCombo2.LookupValue <> '') then
      begin
        Edit;
        ctx_DataAccess.TemplateNumber := StrToInt(wwDBLookupCombo2.LookupValue);
        ctx_PayrollCalculation.CopyPRTemplate(ctx_DataAccess.TemplateNumber);
        PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
        PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
        DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);
        DM_EMPLOYEE.EE_LOCALS.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);
        ctx_PayrollCalculation.CopyPRTemplateDetails(ctx_DataAccess.TemplateNumber, PR_CHECK, PR_CHECK_STATES,
          PR_CHECK_LOCALS, CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_STATES, EE_LOCALS);
        ctx_DataAccess.TemplateNumber := 0;
        ctx_DataAccess.PostDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_LOCALS]);
      end;
end;

procedure TEDIT_PR_CHECK.PRPageControlChange(Sender: TObject);
begin
  inherited;
  Panel1.Visible := PageControl1.ActivePage = TabSheet1;
  evPanel1.Visible := not Panel1.Visible;
  if evPanel1.Visible then
  begin
    btnReattachStatesAndLocals.Visible := DM_PAYROLL.PR['STATUS'] = PAYROLL_STATUS_PENDING;
    btnPAReattachLocals.Visible := btnReattachStatesAndLocals.Visible;
  end;
end;

procedure TEDIT_PR_CHECK.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_EMPLOYEE.EE, aDS);
  AddDS(DM_COMPANY.CO_PR_CHECK_TEMPLATES, aDS);
  AddDS(DM_PAYROLL.PR_BATCH, aDS);
  inherited;
end;

function TEDIT_PR_CHECK.GetInsertControl: TWinControl;
begin
  Result := wwDBComboBox2;
end;

procedure TEDIT_PR_CHECK.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
var
  s: string;
begin
  inherited;
  s := DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString;
  DBEdit3.Enabled := (Length(s) > 0) and (s[1] in [CHECK_TYPE2_MANUAL, CHECK_TYPE2_VOUCHER, CHECK_TYPE2_3RD_PARTY]);
  btnUnvoid.Enabled := DM_PAYROLL.PR_CHECK.FieldByName('CHECK_STATUS').AsString = CHECK_STATUS_VOID;
  butnProrateFICA.Enabled := DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PENDING;
end;

procedure TEDIT_PR_CHECK.wwdsSubMaster1DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if not (csLoading in wwdsSubMaster1.ComponentState)  then
  begin
    DM_EMPLOYEE.EE.Filter := 'PAY_FREQUENCY=''' + DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').AsString + '''';
    DM_EMPLOYEE.EE.Filtered := True;
  end;
end;

procedure TEDIT_PR_CHECK.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_PAYROLL.PR_BATCH, SupportDataSets);
end;

procedure TEDIT_PR_CHECK.Deactivate;
begin
  DM_EMPLOYEE.EE.Filtered := False;
  DM_EMPLOYEE.EE.Filter :='';
  inherited;
end;

procedure TEDIT_PR_CHECK.PRNextExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Next;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRChecks;
end;

procedure TEDIT_PR_CHECK.PRPriorExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Prior;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRChecks;
end;

procedure TEDIT_PR_CHECK.ReopenPRCheckWithLines;
begin
  inherited;
  wwdsSubMaster1.DataSet := wwdsSubMaster1.DataSet;
end;

procedure TEDIT_PR_CHECK.AfterClick(Kind: Integer);
var
  SalaryCreate, HourlyCreate: Boolean;
  Q: IevQuery;
begin
  inherited;

  if Kind = NavDelete then
  begin
    if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
    begin
      ctx_DBAccess.StartTransaction([dbtClient]);
      try
        Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
        Q.Execute;
        (Owner as TFramePackageTmpl).ClickButton(NavCommit);
        ctx_DBAccess.CommitTransaction;
      except
        ctx_DBAccess.RollbackTransaction;
        raise;
      end;
    end;
  end;

  if Kind = NavInsert then
  begin
    ctx_DataAccess.TemplateNumber := Generic_PopulatePRCheckRecord(EmployeeBeforeInsert);
    SalaryCreate := (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_REGULAR) or
                 (EvMessage('Do you want to pay salary?', mtConfirmation, [mbYes, mbNo]) = mrYes);
    HourlyCreate := (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_REGULAR) or
                 (EvMessage('Do you want to pay regular hours?', mtConfirmation, [mbYes, mbNo]) = mrYes);
    with DM_PAYROLL, DM_CLIENT, DM_COMPANY, DM_SYSTEM_FEDERAL, DM_SYSTEM_STATE, DM_EMPLOYEE do
    try
      PR_CHECK.DisableControls;
      PR_CHECK_LINES.DisableControls;
      PR_CHECK.LookupsEnabled := False;
      PR_CHECK_LINES.LookupsEnabled := False;
      PR_CHECK.Post;
      ctx_PayrollCalculation.Generic_CreatePRCheckDetails(PR, PR_BATCH, PR_CHECK, PR_CHECK_LINES,
        PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS, PR_CHECK_LINE_LOCALS, PR_SCHEDULED_E_DS, CL_E_DS,
        CL_PERSON, CL_PENSION, CO, CO_E_D_CODES, CO_STATES, EE, EE_SCHEDULED_E_DS, EE_STATES, SY_FED_TAX_TABLE,
        SY_STATES, False, SalaryCreate, HourlyCreate);

      if ctx_DataAccess.TemplateNumber <> 0 then
      begin
        PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
        PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
        DM_EMPLOYEE.EE_STATES.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);
        DM_EMPLOYEE.EE_LOCALS.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);
        ctx_PayrollCalculation.CopyPRTemplateDetails(ctx_DataAccess.TemplateNumber, PR_CHECK, PR_CHECK_STATES,
          PR_CHECK_LOCALS, CO_BATCH_STATES_OR_TEMPS, CO_BATCH_LOCAL_OR_TEMPS, EE_STATES, EE_LOCALS);
        ctx_DataAccess.TemplateNumber := 0;
      end;

      ctx_DataAccess.PostDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS]);
    finally
      PR_CHECK.EnableControls;
      PR_CHECK_LINES.EnableControls;
      PR_CHECK.LookupsEnabled := True;
      PR_CHECK_LINES.LookupsEnabled := True;
    end;
  end;
end;

procedure TEDIT_PR_CHECK.wwdsSubMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_PAYROLL.PR.Active then
    wwDBGrid3.ReadOnly := (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT);
end;

procedure TEDIT_PR_CHECK.Activate;
var
  aDS: TArrayDS;
begin
  inherited;
  with (Owner as TFramePackageTmpl) do
  begin
    DeleteDataSet := DM_PAYROLL.PR_CHECK;
    NavigationDataSet := DM_PAYROLL.PR_CHECK;
    aDS := EditDataSets;
    AddDS(DM_PAYROLL.PR_CHECK, aDS);
    AddDS(DM_PAYROLL.PR_CHECK_LINES, aDS);
    EditDataSets := aDS;
    aDS := InsertDataSets;
    AddDS(DM_PAYROLL.PR_CHECK, aDS);
    InsertDataSets := aDS;
  end;
  DBRadioGroup13.Enabled := ctx_AccountRights.Functions.GetState('MANUAL_OASDI_MEDICARE') = stEnabled;
  DBRadioGroup14.Enabled := DBRadioGroup13.Enabled;
  DBRadioGroup15.Enabled := DBRadioGroup13.Enabled;
  DBRadioGroup16.Enabled := DBRadioGroup13.Enabled;
  DBEdit14.Enabled := DBRadioGroup13.Enabled;
  DBEdit15.Enabled := DBEdit14.Enabled;
end;

procedure TEDIT_PR_CHECK.evButton1Click(Sender: TObject);
begin
  inherited;
  AdjustShortfalls := TAdjustShortfalls.Create(Self);
  try
    AdjustShortfalls.DoShortfallAdjustments;
  finally
    AdjustShortfalls.Free;
  end;
end;

procedure TEDIT_PR_CHECK.btnUnvoidClick(Sender: TObject);
//begin gdy
  function Confirm: boolean;
  var
    i: integer;
  begin
    with CreateMessageDialog('Unvoiding checks is extremely dangerous.  Are you sure you want to continue?', mtConfirmation, [mbYes, mbNo]) do
      try
        for I := 0 to ComponentCount - 1 do
          if Components[I] is TButton then
          begin
            (Components[I] as TButton).Default := (Components[I] as TButton).Caption = SMsgDlgNo;
            if (Components[I] as TButton).Default then
              ActiveControl := Components[I] as TButton;
          end;
        Result := ShowModal = mrYes;
      finally
        Free;
      end;
  end;
//end gdy
var
  AllOK: Boolean;
begin
  inherited;
  if Confirm then //gdy
  begin
    ctx_DataAccess.UnlockPR;
    AllOK := False;
    try
      DM_PAYROLL.PR_CHECK.Edit;
      DM_PAYROLL.PR_CHECK['CHECK_STATUS'] := CHECK_STATUS_OUTSTANDING;
      DM_PAYROLL.PR_CHECK.Post;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK]);
      AllOK := True;
    finally
      ctx_DataAccess.LockPR(AllOK);
    end;
// Clear Bank Account Register
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('PR_CHECK_NBR='
      + DM_PAYROLL.PR_CHECK.FieldByName('PR_CHECK_NBR').AsString);
    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
    while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.EOF do
    begin
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Outstanding;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
  end;
end;

procedure TEDIT_PR_CHECK.SetReadOnly(Value: Boolean);
begin
  inherited;
  btnUnvoid.SecurityRO := btnUnvoid.SecurityRO or (ctx_AccountRights.Functions.GetState('ABILITY_UNVOID_CHECK') <> stEnabled);
  wwDBComboBox2.SecurityRO := true; //RE 23355
end;

procedure TEDIT_PR_CHECK.evSpeedButton1Click(Sender: TObject);
var
  i: Integer;
  bRefreshPr, bRefreshBatch: Boolean;
  Nbr: Integer;
  s: string;
begin
  inherited;
  TaxCalculator := TTaxCalculator.Create(Nil);
  try
    TaxCalculator.ShowModal;
    bRefreshPr := False;
    bRefreshBatch := False;
    for i := 0 to High(TaxCalculator.ModifiedNbr) do
    begin
      if TaxCalculator.ModifiedNbr[i].PrNbr <> DM_PAYROLL.PR['PR_NBR'] then
        bRefreshPr := True;
      if (TaxCalculator.ModifiedNbr[i].PrNbr = DM_PAYROLL.PR['PR_NBR']) and
         (TaxCalculator.ModifiedNbr[i].PrBatchNbr <> DM_PAYROLL.PR_BATCH['PR_BATCH_NBR']) then
        bRefreshBatch := True;
    end;

    if bRefreshPr then
    begin
      DM_PAYROLL.PR.DisableControls;
      Nbr := DM_PAYROLL.PR['PR_NBR'];
      try
        s := DM_PAYROLL.PR.RetrieveCondition;
        DM_PAYROLL.PR.RetrieveCondition := DM_PAYROLL.PR.OpenCondition;
        DM_PAYROLL.PR.Close;
        DM_PAYROLL.PR.Open;
        DM_PAYROLL.PR.RetrieveCondition := s;
      finally
        DM_PAYROLL.PR.Locate('PR_NBR', Nbr, []);
        DM_PAYROLL.PR.EnableControls;
      end;
    end;

    if bRefreshBatch then
    begin
      DM_PAYROLL.PR_BATCH.DisableControls;
      Nbr := DM_PAYROLL.PR_BATCH['PR_BATCH_NBR'];
      try
        s := DM_PAYROLL.PR_BATCH.RetrieveCondition;
        DM_PAYROLL.PR_BATCH.RetrieveCondition := DM_PAYROLL.PR_BATCH.OpenCondition;
        DM_PAYROLL.PR_BATCH.Close;
        DM_PAYROLL.PR_BATCH.Open;
        DM_PAYROLL.PR_BATCH.RetrieveCondition := s;
      finally
        DM_PAYROLL.PR_BATCH.Locate('PR_BATCH_NBR', Nbr, []);
        DM_PAYROLL.PR_BATCH.EnableControls;
      end;
    end;
  finally
    TaxCalculator.Free;
  end;
end;

procedure TEDIT_PR_CHECK.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  if Kind = NavInsert then
    EmployeeBeforeInsert := Trim(DM_PAYROLL.PR_CHECK.FieldByName('EE_Custom_Number').AsString);
end;

procedure TEDIT_PR_CHECK.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  if PageControl1.ActivePage = TabSheet1 then
    ReopenPRChecks;
end;

procedure TEDIT_PR_CHECK.btnReattachStatesAndLocalsClick(Sender: TObject);
begin
  inherited;
  with TReattachStatesLocals.Create(nil) do
  try
    ShowModal;
  finally
    Free;
  end;
end;

procedure TEDIT_PR_CHECK.btnPAReattachLocalsClick(Sender: TObject);
var
  Dlg: TPAMovingWagesParams;
  YearDlg: TYearDlg;
begin
  YearDlg := TYearDlg.Create(nil);
  try
    YearDlg.cbYear.Items.Add(IntToStr(CurrentYear-5));
    YearDlg.cbYear.Items.Add(IntToStr(CurrentYear-4));
    YearDlg.cbYear.Items.Add(IntToStr(CurrentYear-3));
    YearDlg.cbYear.Items.Add(IntToStr(CurrentYear-2));
    YearDlg.cbYear.Items.Add(IntToStr(CurrentYear-1));
    YearDlg.cbYear.Items.Add(IntToStr(CurrentYear));
    YearDlg.cbYear.ItemIndex := 5;

    if YearDlg.ShowModal <> mrOk then
      Exit;
    Dlg := TPAMovingWagesParams.Create(nil);
    try
      Dlg.FYear := StrToInt(YearDlg.cbYear.Text);
      if Dlg.ShowModal <> mrOK then
        Exit;
    finally
      Dlg.Free;
    end;
  finally
    YearDlg.Free;
  end;
end;

procedure TEDIT_PR_CHECK.butnProrateFICAClick(Sender: TObject);
var
  ProrateRatio: Double;
begin
  inherited;
  if EvMessage('Are you sure you want to prorate FICA for this check?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  ctx_StartWait;
  try
    DM_SYSTEM.SY_FED_TAX_TABLE.AsOfDate := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
    DM_SYSTEM.SY_FED_TAX_TABLE.Open;
    ProrateRatio := DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value / (DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value + DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value);
    if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').IsNull and not DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').IsNull then
    begin
      if not (DM_PAYROLL.PR_CHECK.State in [dsInsert, dsEdit]) then
        DM_PAYROLL.PR_CHECK.Edit;
      DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value := DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value * ProrateRatio;
      DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value := DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value - DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value;
      DM_PAYROLL.PR_CHECK.Post;
    end;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK]);
  finally
    ctx_EndWait;
  end;
end;

initialization
  RegisterClass(TEDIT_PR_CHECK);

end.
