// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PayrollAccountRegister;

interface

uses
   Db,  Wwdatsrc, Grids, Wwdbigrd,
  Wwdbgrid, Controls, Buttons, Classes, ExtCtrls, Forms, SSecurityInterface,
  EvUtils, SDataStructure, Dialogs, SysUtils, EvSecElement, EvTypes,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvDataAccessComponents, EvContext, EvConsts, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TPayrollAccountRegister = class(TForm)
    pnlNavigationButtons: TevPanel;
    btnNavFirst: TevSpeedButton;
    btnNavPrevious: TevSpeedButton;
    btnNavNext: TevSpeedButton;
    btnNavLast: TevSpeedButton;
    btnNavDelete: TevSpeedButton;
    btnNavOk: TevSpeedButton;
    btnNavCancel: TevSpeedButton;
    btnNavClose: TevSpeedButton;
    bNavDeleteall: TevSpeedButton;
    wwDBGrid1: TevDBGrid;
    wwDataSource: TevDataSource;
    wwClientDataSet: TevClientDataSet;
    wwClientDataSet2: TevClientDataSet;
    procedure FormShow(Sender: TObject);
    procedure btnNavCloseClick(Sender: TObject);
    procedure btnNavDeleteClick(Sender: TObject);
    procedure bNavDeleteallClick(Sender: TObject);
    procedure btnNavOkClick(Sender: TObject);
    procedure btnNavCancelClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    FFullAccess: Boolean;
    procedure SetButtons;
  public
    { Public declarations }
    FPayrollNbr: Integer;
  end;

var
  PayrollAccountRegister: TPayrollAccountRegister;

implementation

{$R *.DFM}

procedure TPayrollAccountRegister.FormShow(Sender: TObject);
begin
  DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('ALL');
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired('ALL');
  with ctx_DataAccess.CUSTOM_VIEW do
  try
    if Active then Close;
    with TExecDSWrapper.Create('SelectPayrollAccountRegister') do
    begin
      SetParam('PrNbr', FPayrollNbr);
      DataRequest(AsVariant);
    end;
    Open;
    wwClientDataSet.Data := Data;
  finally
    Close
  end;
  SetButtons;
  FFullAccess := ctx_AccountRights.Functions.GetState('USER_CAN_REGISTER') <> stDisabled;
  btnNavDelete.Visible := FFullAccess;
  bNavDeleteall.Visible := btnNavDelete.Visible;
end;

procedure TPayrollAccountRegister.btnNavCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TPayrollAccountRegister.btnNavDeleteClick(Sender: TObject);
begin
  if EvMessage('Delete record.  Are you sure?',
    mtConfirmation, [mbYes, mbNo]) = mrYes then
    wwClientDataSet.Delete;
  SetButtons;
end;

procedure TPayrollAccountRegister.bNavDeleteallClick(Sender: TObject);
begin
  if EvMessage('Delete record.  Are you sure?',
    mtConfirmation, [mbYes, mbNo]) = mrYes then
    while wwClientDataSet.RecordCount > 0 do
      wwClientDataSet.Delete;
  SetButtons;
end;

procedure TPayrollAccountRegister.btnNavOkClick(Sender: TObject);
var
  MyTransactionManager: TTransactionManager;
begin
  if wwClientDataSet.ChangeCount > 0 then
  begin
    wwClientDataSet2.Delta:= wwClientDataSet.Delta;
    MyTransactionManager := TTransactionManager.Create(nil);
    try
      MyTransactionManager.Initialize([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
      try
        wwClientDataSet2.First;
        while not wwClientDataSet2.Eof do
        begin
          Assert(wwClientDataSet2.UpdateStatus = usDeleted);
          if not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Locate('CO_BANK_ACCOUNT_REGISTER_NBR',
            wwClientDataSet2['CO_BANK_ACCOUNT_REGISTER_NBR'], []) then
            raise EInconsistentData.CreateHelp('Can''t find a record in CO_BANK_ACCOUNT_REGISTER table', IDH_InconsistentData);
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Delete;
          wwClientDataSet2.Next;
        end;
        MyTransactionManager.ApplyUpdates;
      except
        MyTransactionManager.CancelUpdates;
        raise;
      end;
    finally
      MyTransactionManager.Free;
      wwClientDataSet.CancelUpdates;
    end;
  end;
  Close;
end;

procedure TPayrollAccountRegister.btnNavCancelClick(Sender: TObject);
begin
  wwClientDataSet.CancelUpdates;
  SetButtons;
end;

procedure TPayrollAccountRegister.SetButtons;
begin
  btnNavOk.Visible := (wwClientDataSet.ChangeCount > 0) and FFullAccess;
  btnNavCancel.Visible := wwClientDataSet.ChangeCount > 0;
  btnNavClose.Visible := not (wwClientDataSet.ChangeCount > 0);
end;

procedure TPayrollAccountRegister.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if wwClientDataSet.ChangeCount > 0 then
    raise ECanNotClose.CreateHelp('You need to apply or cancel changes first', IDH_CanNotClose);
  CanClose := True;
end;

initialization
  RegisterClass(TPAYROLLACCOUNTREGISTER);

end.
