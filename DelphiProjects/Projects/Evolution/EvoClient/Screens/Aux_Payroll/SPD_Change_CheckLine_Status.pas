// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Change_CheckLine_Status;

interface

uses
   Db, Wwdatsrc,  Grids, Wwdbigrd,
  Wwdbgrid, StdCtrls, wwdblook, ExtCtrls, Buttons, Controls, Classes,
  Forms, EvUtils, SDataStructure, EvConsts, evTypes, Variants, SDDClasses,
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, SDataDictclient,
  ISDataAccessComponents, EvDataAccessComponents, EvContext, EvUIComponents, EvClientDataSet;

type
  TChange_CheckLine_Status = class(TForm)
    Panel1: TevPanel;
    Label1: TevLabel;
    bbtnOK: TevBitBtn;
    bbtnCancel: TevBitBtn;
    RadioGroup1: TevRadioGroup;
    wwDBGrid1: TevDBGrid;
    wwcsCheckLines: TevClientDataSet;
    wwdsCheckLines: TevDataSource;
    wwDBLookupCombo1: TevDBLookupCombo;
    DM_CLIENT: TDM_CLIENT;
    lablTotal: TevLabel;
    GroupBox1: TGroupBox;
    btnIgnore: TButton;
    btnPending: TButton;
    btnSent: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bbtnCancelClick(Sender: TObject);
    procedure bbtnChangeClick(Sender: TObject);
    procedure wwdsCheckLinesDataChange(Sender: TObject; Field: TField);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bbtnOKClick(Sender: TObject);
    procedure wwDBLookupCombo1Change(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
    AllOK: Boolean;
    procedure ApplyFilter;
  public
    { Public declarations }
  end;

var
  Change_CheckLine_Status: TChange_CheckLine_Status;

implementation

uses
  SysUtils, SPD_Payroll_Expert;

{$R *.DFM}

procedure TChange_CheckLine_Status.FormCreate(Sender: TObject);
begin
  if ctx_DataAccess.CUSTOM_VIEW.Active then
    ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('AllPRCheckLines') do
  begin
    SetParam('PrNbr', DM_PAYROLL.PR.FieldByName('PR_NBR').Value);
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  wwcsCheckLines.Data := ctx_DataAccess.CUSTOM_VIEW.Data;

  DM_SERVICE_BUREAU.SB_AGENCY.DataRequired('ALL');
  DM_CLIENT.CL_AGENCY.DataRequired('ALL');
  DM_CLIENT.CL_E_DS.DataRequired('ALL');
  OpenDataSetForPayroll(DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger);
  DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR=' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
  DM_CLIENT.CL_E_DS.IndexFieldNames := 'CUSTOM_E_D_CODE_NUMBER';

  wwcsCheckLines.First;
  while not wwcsCheckLines.EOF do
  begin
    wwcsCheckLines.Edit;
    wwcsCheckLines.FieldByName('AGENCY_NAME').Value := DM_CLIENT.CL_AGENCY.Lookup('CL_AGENCY_NBR',
      wwcsCheckLines.FieldByName('CL_AGENCY_NBR').Value, 'Agency_Name');
    wwcsCheckLines.Post;
    wwcsCheckLines.Next;
  end;
  wwcsCheckLines.First;

  DM_CLIENT.CL_E_DS.First;
  wwDBLookupCombo1.Text := DM_CLIENT.CL_E_DS.FieldByName('CUSTOM_E_D_CODE_NUMBER').AsString;

  ctx_DataAccess.UnlockPR;
  AllOK := False;
end;

procedure TChange_CheckLine_Status.FormDestroy(Sender: TObject);
begin
  DM_CLIENT.CL_E_DS.IndexFieldNames := '';
  ctx_DataAccess.LockPR(AllOK);
end;

procedure TChange_CheckLine_Status.bbtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TChange_CheckLine_Status.bbtnChangeClick(Sender: TObject);
var
  CLAgencyNbr, NewStatus: string;
  I: Integer;
begin
  if wwcsCheckLines.RecordCount = 0 then
    Exit;

  ctx_StartWait;
  try
    CLAgencyNbr := '';
    if (TButton(Sender).Caption = 'Pending') or (TButton(Sender).Caption = 'Sent') then
    begin
      if TButton(Sender).Caption = 'Pending' then
        NewStatus := CHECK_LINE_AGENCY_STATUS_PENDING
      else
        NewStatus := CHECK_LINE_AGENCY_STATUS_SENT;

      if RadioGroup1.ItemIndex = 0 then
      begin
        Payroll_Expert := TPayroll_Expert.Create(Application);
        with Payroll_Expert do
        try
          repeat
            CLAgencyNbr := DisplayExpert(crLookupCombo, 'Please, select an agency to attach to selected check lines.',
              DM_CLIENT.CL_AGENCY, VarArrayOf(['CL_AGENCY_NBR', 'Agency_Name' + #9 + '30' + #9 + 'Agency_Name']));
          until CLAgencyNbr <> '';
        finally
          Free;
        end;
      end
      else
        CLAgencyNbr := wwcsCheckLines.FieldByName('CL_AGENCY_NBR').AsString;
    end
    else
      NewStatus := CHECK_LINE_AGENCY_STATUS_IGNORE;

    ctx_DataAccess.RecalcLineBeforePost := False;

    wwcsCheckLines.DisableControls;
    for I := 0 to wwDBGrid1.SelectedList.Count - 1 do
    begin
      wwcsCheckLines.GotoBookmark(wwDBGrid1.SelectedList.Items[i]);
      with DM_PAYROLL.PR_CHECK_LINES do
      begin
        Locate('PR_CHECK_LINES_NBR', wwcsCheckLines.FieldByName('PR_CHECK_LINES_NBR').Value, []);
        Edit;
        FieldByName('AGENCY_STATUS').Value := NewStatus;
        if CLAgencyNbr = '' then
          FieldByName('CL_AGENCY_NBR').Value := Null
        else
          FieldByName('CL_AGENCY_NBR').AsString := CLAgencyNbr;
        Post;
      end;

      wwcsCheckLines.Edit;
      wwcsCheckLines.FieldByName('AGENCY_STATUS').Value := NewStatus;
      if CLAgencyNbr = '' then
      begin
        wwcsCheckLines.FieldByName('CL_AGENCY_NBR').Value := Null;
        wwcsCheckLines.FieldByName('AGENCY_NAME').Value := Null;
      end
      else
      begin
        wwcsCheckLines.FieldByName('CL_AGENCY_NBR').AsString := CLAgencyNbr;
        wwcsCheckLines.FieldByName('AGENCY_NAME').Value := DM_CLIENT.CL_AGENCY.Lookup('CL_AGENCY_NBR', CLAgencyNbr, 'Agency_Name');
      end;
      wwcsCheckLines.Post;
    end;
    wwDBGrid1.UnselectAll;
    wwcsCheckLines.EnableControls;

    ctx_DataAccess.RecalcLineBeforePost := True;

    wwcsCheckLines.First;
  finally
    ctx_EndWait;
  end;

  bbtnOK.Enabled := True;
end;

procedure TChange_CheckLine_Status.wwdsCheckLinesDataChange(
  Sender: TObject; Field: TField);
begin
  if not wwcsCheckLines.FieldByName('AGENCY_STATUS').IsNull then
  case wwcsCheckLines.FieldByName('AGENCY_STATUS').AsString[1] of
    CHECK_LINE_AGENCY_STATUS_IGNORE:
    begin
      btnIgnore.Enabled := False;
      btnPending.Enabled := True;
      btnSent.Enabled := True;
    end;
    CHECK_LINE_AGENCY_STATUS_PENDING:
    begin
      btnIgnore.Enabled := True;
      btnPending.Enabled := False;
      btnSent.Enabled := True;
    end;
    CHECK_LINE_AGENCY_STATUS_SENT:
    begin
      btnIgnore.Enabled := True;
      btnPending.Enabled := True;
      btnSent.Enabled := False;
    end;
  end;
end;

procedure TChange_CheckLine_Status.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DM_PAYROLL.PR_CHECK_LINES.CancelUpdates;
end;

procedure TChange_CheckLine_Status.bbtnOKClick(Sender: TObject);
begin
  ctx_StartWait;
  try
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK_LINES]);
    AllOK := True;
  finally
    ctx_EndWait;
  end;
  Close;
end;

procedure TChange_CheckLine_Status.wwDBLookupCombo1Change(Sender: TObject);
begin
  ApplyFilter;
end;

procedure TChange_CheckLine_Status.RadioGroup1Click(Sender: TObject);
begin
  ApplyFilter;
end;

procedure TChange_CheckLine_Status.ApplyFilter;
var
  TempAmount: Currency;
begin
  btnIgnore.Enabled := False;
  btnPending.Enabled := False;
  btnSent.Enabled := False;

  wwcsCheckLines.Filtered := False;

  wwcsCheckLines.Filter := '';
  if wwDBLookupCombo1.Text <> '' then
    wwcsCheckLines.Filter := 'CL_E_DS_NBR=' + DM_CLIENT.CL_E_DS.FieldByName('CL_E_DS_NBR').AsString;

  if RadioGroup1.ItemIndex <> 3 then
  begin
    if wwcsCheckLines.Filter <> '' then
      wwcsCheckLines.Filter := wwcsCheckLines.Filter + ' and ';
    if RadioGroup1.ItemIndex = 0 then
      wwcsCheckLines.Filter := wwcsCheckLines.Filter + 'AGENCY_STATUS=''' + CHECK_LINE_AGENCY_STATUS_IGNORE + ''''
    else
    if RadioGroup1.ItemIndex = 1 then
      wwcsCheckLines.Filter := wwcsCheckLines.Filter + 'AGENCY_STATUS=''' + CHECK_LINE_AGENCY_STATUS_PENDING + ''''
    else
      wwcsCheckLines.Filter := wwcsCheckLines.Filter + 'AGENCY_STATUS=''' + CHECK_LINE_AGENCY_STATUS_SENT
        + ''' and PR_MISCELLANEOUS_CHECKS_NBR is null';
  end;

  wwcsCheckLines.Filtered := True;

  TempAmount := 0;
  with wwcsCheckLines do
  begin
    DisableControls;
    First;
    while not EOF do
    begin
      TempAmount := TempAmount + FieldByName('AMOUNT').AsCurrency;
      Next;
    end;
    First;
    EnableControls;
  end;
  lablTotal.Caption := 'Total Check Lines Amount: ' + FloatToStrF(TempAmount, ffCurrency, 15, 2);
end;

initialization
  RegisterClass(TCHANGE_CHECKLINE_STATUS);

end.
