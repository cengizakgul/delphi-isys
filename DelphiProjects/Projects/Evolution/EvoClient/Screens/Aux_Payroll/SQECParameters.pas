// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SQECParameters;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls, EvBasicUtils,
  ISBasicClasses, isUIEdit, LMDCustomButton, LMDButton, isUILMDButton,
  EvUIComponents, EvUIUtils;

type
  TQECParameters = class(TForm)
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    cbForceCleanup: TevCheckBox;
    cbProcess: TevCheckBox;
    cbSendToQueue: TevCheckBox;
    cbPrintReports: TevCheckBox;
    cbAllEmployees: TevCheckBox;
    cbCurrentQuarterOnly: TevCheckBox;
    editEmployees: TevEdit;
    lablEmployees: TevLabel;
    lablQuarterEndDate: TevLabel;
    lablMinimumTax: TevLabel;
    dtQuarterEndDate: TevDateTimePicker;
    editMinTax: TMaskEdit;
    EvBevel1: TEvBevel;
    evGroupBox1: TevGroupBox;
    cbFederal: TevCheckBox;
    cbEEOASDI: TevCheckBox;
    cbEROASDI: TevCheckBox;
    cbEEMedicare: TevCheckBox;
    cbERMedicare: TevCheckBox;
    cbERFUI: TevCheckBox;
    cbState: TevCheckBox;
    cbEESDI: TevCheckBox;
    cbERSDI: TevCheckBox;
    cbSUI: TevCheckBox;
    cbLocal: TevCheckBox;
    updwChangeQuarter: TUpDown;
    cbQtrEndCheckDate: TevCheckBox;
    MovePaAmounts: TevCheckBox;
    procedure evBitBtn1Click(Sender: TObject);
    procedure cbAllEmployeesClick(Sender: TObject);
    procedure cbProcessClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure updwChangeQuarterClick(Sender: TObject; Button: TUDBtnType);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  QECParameters: TQECParameters;

implementation

uses
  EvUtils;

{$R *.DFM}

procedure TQECParameters.evBitBtn1Click(Sender: TObject);
begin
  if not cbAllEmployees.Checked and (Trim(editEmployees.Text) = '') then
  begin
    ModalResult := mrNone;
    EvErrMessage('Please, enter employee codes');
  end;
  if Trim(editMinTax.Text) = '' then
  begin
    ModalResult := mrNone;
    EvErrMessage('Please, enter minimum tax');
  end;
end;

procedure TQECParameters.cbAllEmployeesClick(Sender: TObject);
begin
  lablEmployees.Enabled := not cbAllEmployees.Checked;
  editEmployees.Enabled := not cbAllEmployees.Checked;
end;

procedure TQECParameters.cbProcessClick(Sender: TObject);
begin
  cbSendToQueue.Enabled := not cbProcess.Checked;
end;

procedure TQECParameters.FormShow(Sender: TObject);
begin
  dtQuarterEndDate.Date := GetEndQuarter(Date);
end;

procedure TQECParameters.updwChangeQuarterClick(Sender: TObject;
  Button: TUDBtnType);
begin
  if Button = btNext then
    dtQuarterEndDate.Date := GetEndQuarter(dtQuarterEndDate.Date + 1)
  else
    dtQuarterEndDate.Date := GetEndQuarter(GetBeginQuarter(dtQuarterEndDate.Date) - 1);
end;

end.
