inherited EDIT_PR_SCHEDULED_E_DS: TEDIT_PR_SCHEDULED_E_DS
  inherited PageControl1: TevPageControl
    ActivePage = TabSheet2
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                IniAttributes.SectionName = 'TEDIT_PR_SCHEDULED_E_DS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited PRGrid: TevDBGrid
                IniAttributes.SectionName = 'TEDIT_PR_SCHEDULED_E_DS\PRGrid'
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object Label1: TevLabel
        Left = 8
        Top = 336
        Width = 65
        Height = 13
        Caption = '~E/D Code'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 0
        Width = 617
        Height = 321
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'ED_Lookup'#9'20'#9'E/D Code'#9'No'
          'EXCLUDE'#9'7'#9'Exclude'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_SCHEDULED_E_DS\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwDBLookupCombo1: TevDBLookupCombo
        Left = 8
        Top = 352
        Width = 257
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'ED_Lookup'#9'20'#9'E/D Code'
          'CodeDescription'#9'20'#9'E/D Description')
        DataField = 'CO_E_D_CODES_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CO_E_D_CODES.CO_E_D_CODES
        LookupField = 'CO_E_D_CODES_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object DBRadioGroup1: TevDBRadioGroup
        Left = 280
        Top = 336
        Width = 129
        Height = 41
        Caption = '~Exclude'
        Columns = 2
        DataField = 'EXCLUDE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_PR_SCHEDULED_E_DS.PR_SCHEDULED_E_DS
  end
  inherited wwdsList: TevDataSource
    Left = 162
    Top = 42
  end
end
