// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_PR_SCHEDULED_E_DS;

interface

uses
   SDataStructure, SPD_EDIT_PR_BASE, StdCtrls, ExtCtrls,
  DBCtrls, wwdblook, DBActns, Classes, ActnList, Db, Wwdatsrc, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls, ISBasicClasses,
  SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBLookupCombo, ImgList, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms;

type
  TEDIT_PR_SCHEDULED_E_DS = class(TEDIT_PR_BASE)
    TabSheet2: TTabSheet;
    Label1: TevLabel;
    wwDBGrid2: TevDBGrid;
    wwDBLookupCombo1: TevDBLookupCombo;
    DBRadioGroup1: TevDBRadioGroup;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_PR_SCHEDULED_E_DS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  inherited;
end;

function TEDIT_PR_SCHEDULED_E_DS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR_SCHEDULED_E_DS;
end;

function TEDIT_PR_SCHEDULED_E_DS.GetInsertControl: TWinControl;
begin
  Result := wwDBLookupCombo1;
end;

initialization
  RegisterClass(TEDIT_PR_SCHEDULED_E_DS);

end.
