// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_AdjustShortfalls;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls,  Buttons, ExtCtrls, Mask, wwdbedit, Db,
  Wwdatsrc, SDataStructure, Grids, Wwdbigrd, Wwdbgrid, EvUtils, EvContext,
  SSecurityInterface, EvTypes, ISBasicClasses, SDDClasses, SDataDictclient,
  EvConsts, EvExceptions, EvUIComponents;

type
  TAdjustShortfalls = class(TForm)
    Panel1: TevPanel;
    Panel2: TevPanel;
    Panel3: TevPanel;
    bbtnAdjust: TevBitBtn;
    bbtnCancel: TevBitBtn;
    lablInfo: TevLabel;
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    evDBGrid1: TevDBGrid;
    evDBGrid2: TevDBGrid;
    evDBGrid3: TevDBGrid;
    DM_PAYROLL: TDM_PAYROLL;
    wwdsFederal: TevDataSource;
    wwdsState: TevDataSource;
    wwdsLocal: TevDataSource;
    evLabel1: TevLabel;
    evDBEdit1: TevDBEdit;
    evLabel2: TevLabel;
    evDBEdit2: TevDBEdit;
    evLabel3: TevLabel;
    evDBEdit3: TevDBEdit;
    evLabel4: TevLabel;
    evDBEdit4: TevDBEdit;
    wwdsCheckLines: TevDataSource;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure bbtnCancelClick(Sender: TObject);
    procedure bbtnAdjustClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoShortfallAdjustments;
  end;

var
  AdjustShortfalls: TAdjustShortfalls;

implementation

{$R *.DFM}

procedure TAdjustShortfalls.FormShow(Sender: TObject);
begin
  ctx_DataAccess.UnlockPRSimple;
  ctx_DataAccess.RecalcLineBeforePost := False;
end;

procedure TAdjustShortfalls.FormHide(Sender: TObject);
begin
  ctx_DataAccess.RecalcLineBeforePost := True;
  ctx_DataAccess.LockPRSimple;
end;

procedure TAdjustShortfalls.bbtnCancelClick(Sender: TObject);
begin
  DM_PAYROLL.PR_CHECK.Cancel;
  DM_PAYROLL.PR_CHECK_LINES.Cancel;
  DM_PAYROLL.PR_CHECK_STATES.Cancel;
  DM_PAYROLL.PR_CHECK_LOCALS.Cancel;
  with DM_PAYROLL do
    ctx_DataAccess.CancelDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_LOCALS]);
end;

procedure TAdjustShortfalls.bbtnAdjustClick(Sender: TObject);
var
  AllOK: Boolean;
begin
  ctx_StartWait;
  ctx_DataAccess.UnlockPR;
  AllOK := False;
  DM_PAYROLL.PR_CHECK.CheckBrowseMode;
  DM_PAYROLL.PR_CHECK_LINES.CheckBrowseMode;
  DM_PAYROLL.PR_CHECK_STATES.CheckBrowseMode;
  DM_PAYROLL.PR_CHECK_LOCALS.CheckBrowseMode;
  try
    with DM_PAYROLL do
      ctx_DataAccess.PostDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_LOCALS]);
    AllOK := True;
  finally
    ctx_DataAccess.LockPR(AllOK);
    ctx_EndWait;
  end;
end;

procedure TAdjustShortfalls.DoShortfallAdjustments;
begin
  if ctx_AccountRights.Functions.GetState('SHORTFALL_ADJUSTMENTS') <> stEnabled then
    raise ENoRights.CreateHelp('You have no rights to perform this operation.', IDH_SecurityViolation);
  with DM_PAYROLL do
  begin
    ctx_StartWait;
    try
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
      DM_CLIENT.CL_E_DS.DataRequired('ALL');

      PR_CHECK_LINES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString + ' and DEDUCTION_SHORTFALL_CARRYOVER is not null and DEDUCTION_SHORTFALL_CARRYOVER<>0');
      PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString + ' and STATE_SHORTFALL is not null and STATE_SHORTFALL<>0');
      PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString + ' and LOCAL_SHORTFALL is not null and LOCAL_SHORTFALL<>0');

      lablInfo.Caption := 'Check Date: ' + PR.FieldByName('CHECK_DATE').AsString
        + '    Run #' + PR.FieldByName('RUN_NUMBER').AsString
        + '    Check #' + PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').AsString;
    finally
      ctx_EndWait;
    end;
    ShowModal;
  end;
end;

procedure TAdjustShortfalls.FormDestroy(Sender: TObject);
begin
  ctx_DataAccess.RecalcLineBeforePost := True;
end;

initialization
  RegisterClass(TAdjustShortfalls);

end.
