// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_PR_BATCH;

interface

uses
   SDataStructure, SPopulateRecords, SPD_EDIT_PR_BASE,
  wwdbdatetimepicker, StdCtrls, ExtCtrls, DBCtrls, Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, wwdblook, DBActns, Classes, ActnList, Db, Wwdatsrc,
  Buttons, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls, SPackageEntry,
  EvUtils, Dialogs, EvConsts, Spin, Variants, EvBasicUtils, ISBasicClasses,
  SDDClasses, SDataDictclient, SDataDicttemp, EvContext, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBComboBox, isUIwwDBLookupCombo, ImgList,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms, EVCommonInterfaces, EvDataset,
  isUIwwDBEdit;

type
  TEDIT_PR_BATCH = class(TEDIT_PR_BASE)
    TabSheet3: TTabSheet;
    Label8: TevLabel;
    Label9: TevLabel;
    Label10: TevLabel;
    Label11: TevLabel;
    Label12: TevLabel;
    wwDBGrid3: TevDBGrid;
    wwDBLookupCombo3: TevDBLookupCombo;
    wwDBLookupCombo4: TevDBLookupCombo;
    wwDBComboBox2: TevDBComboBox;
    DBRadioGroup4: TevDBRadioGroup;
    DBRadioGroup5: TevDBRadioGroup;
    DBRadioGroup6: TevDBRadioGroup;
    edPERIOD_END_DATE: TevDBDateTimePicker;
    edPERIOD_BEGIN_DATE: TevDBDateTimePicker;
    evGroupBox1: TevGroupBox;
    evLabel6: TevLabel;
    ChecksPerEEChkBox: TevSpinEdit;
    PayAllSalaryHourly: TevRadioGroup;
    evCheckBox1: TevCheckBox;
    btnRefreshEDs: TevBitBtn;
    evCheckBox2: TevCheckBox;
    evRadioGroup2: TevRadioGroup;
    CalcScheduledEDsChkBox: TevCheckBox;
    IncludeTimeOffRequests: TevCheckBox;
    lblBatchDesc: TevLabel;
    edtBatchDes: TevDBEdit;
    procedure btnRefreshEDsClick(Sender: TObject);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
  private
    NewBatch: Boolean;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
  public
    function GetInsertControl: TWinControl; override;
    procedure AfterClick(Kind: Integer); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure Activate; override;
    procedure AfterDataSetsReopen; override;
  end;

implementation

uses
  SPD_EmployeeSelector, EvTypes, SysUtils;

{$R *.DFM}

procedure TEDIT_PR_BATCH.AfterClick(Kind: Integer);
var
  V: Variant;
  CheckType, Check945Type: String;
  SaveFilterCond: String;
  SaveFilterState: Boolean;
  UpdateBalance: Boolean;
begin
  inherited;
  SaveFilterState := False;
  if Kind = NavInsert then
  try
    Generic_PopulatePRBatchRecord;
    NewBatch := True;
//    (Owner as TFramePackageTmpl).ClickButton(NavOK);
  except
    NewBatch := False;
    (Owner as TFramePackageTmpl).ClickButton(NavCancel);
    raise;
  end
  else if Kind = NavCommit then
  begin
    if NewBatch then
    begin
      NewBatch := False;
      if EvMessage('Would you like to auto-create checks for this batch?', mtConfirmation, [mbYes, mbNo]) = mrYes then
      begin
        if evRadioGroup2.ItemIndex = 1 then
          UpdateBalance := EvMessage('Do you want the balance of the scheduled EDs for manual checks to be updated?', mtConfirmation, [mbYes, mbNo]) = mrYes
        else
          UpdateBalance := True;
        CommitData;
        ctx_StartWait;
        try
          V := Null;
          if evCheckBox1.Checked then
          begin
            EmployeeSelector := TEmployeeSelector.Create(Nil);
            try
              SaveFilterCond  := EmployeeSelector.dsEmployee.DataSet.Filter;
              SaveFilterState := EmployeeSelector.dsEmployee.DataSet.Filtered;
              EmployeeSelector.dsEmployee.DataSet.Filter := 'PAY_FREQUENCY=' + QuotedStr(DM_PAYROLL.PR_BATCH.FieldByName('FREQUENCY').AsString) + ' and EE_ENABLED = ''Y''';
              EmployeeSelector.dsEmployee.DataSet.Filtered := True;
              if EmployeeSelector.ShowModal = mrOK then
                V := EmployeeSelector.EENumbers
              else
                Exit;
            finally
              EmployeeSelector.dsEmployee.DataSet.Filter := SaveFilterCond;
              EmployeeSelector.dsEmployee.DataSet.Filtered := SaveFilterState;
              EmployeeSelector.Free;
            end;
          end;
          if evCheckBox2.Checked then
            Check945Type := CHECK_TYPE_945_945
          else
            Check945Type := CHECK_TYPE_945_NONE;
          case evRadioGroup2.ItemIndex of
            0: CheckType := CHECK_TYPE2_REGULAR;
            1: CheckType := CHECK_TYPE2_MANUAL;
            2: CheckType := CHECK_TYPE2_3RD_PARTY;
          end;
          ctx_PayrollProcessing.CreatePRBatchDetails(DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger,
            (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value <> PAYROLL_TYPE_SETUP) or (GetQuarterNumber(DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime) <> 1) and
            (EvMessage('Would you like to auto-create quarter to date checks?', mtConfirmation, [mbYes, mbNo]) = mrYes),
            CalcScheduledEDsChkBox.Checked, CheckType, Check945Type, ChecksPerEEChkBox.Value,
            PayAllSalaryHourly.ItemIndex, V, nil, nil, loByName, loFull, False, loFixed, False, False,
            UpdateBalance, IncludeTimeOffRequests.Checked);
        finally
          ctx_EndWait;
        end;
      end;
    end;
  end;
end;

procedure TEDIT_PR_BATCH.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_PR_CHECK_TEMPLATES, aDS);
  AddDS(DM_COMPANY.CO_PR_FILTERS, aDS);
  inherited;
end;

function TEDIT_PR_BATCH.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR_BATCH;
end;

function TEDIT_PR_BATCH.GetInsertControl: TWinControl;
begin
  Result := wwDBLookupCombo3;
end;

procedure TEDIT_PR_BATCH.btnRefreshEDsClick(Sender: TObject);
begin
  inherited;
  if EvMessage('Are you sure you want to refresh Scheduled EDs for this batch?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;
  DM_PAYROLL.PR_CHECK.DataRequired('PR_BATCH_NBR=' + DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsString);
  ctx_PayrollCalculation.RefreshScheduledEDsForBatch(DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').AsInteger);
  CommitData;
end;

procedure TEDIT_PR_BATCH.wwdsSubMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  btnRefreshEDs.Enabled := DM_PAYROLL.PR.Active and
       (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT);
end;

procedure TEDIT_PR_BATCH.Activate;
begin
  inherited;

end;

procedure TEDIT_PR_BATCH.AfterDataSetsReopen;
var
  VFilter, BFilter, DFilter, EFilter: String;
  VWasFiltered, BWasFiltered, DWasFiltered, EWasFiltered: Boolean;
begin
  inherited;

  VFilter := DM_COMPANY.CO_DIVISION.Filter;
  VWasFiltered := DM_COMPANY.CO_DIVISION.Filtered;
  BFilter := DM_COMPANY.CO_BRANCH.Filter;
  BWasFiltered := DM_COMPANY.CO_BRANCH.Filtered;
  DFilter := DM_COMPANY.CO_DEPARTMENT.Filter;
  DWasFiltered := DM_COMPANY.CO_DEPARTMENT.Filtered;
  EFilter := DM_COMPANY.CO_TEAM.Filter;
  EWasFiltered := DM_COMPANY.CO_TEAM.Filtered;
  try
    DM_COMPANY.CO_DIVISION.Filter := 'HOME_STATE_TYPE = '+ QuotedStr(HOME_STATE_TYPE_OVERRIDE) + ' and ( not OVERRIDE_PAY_RATE is null)';
    DM_COMPANY.CO_DIVISION.Filtered := True;

    if DM_COMPANY.CO_DIVISION.RecordCount > 0 then
    begin
      EvMessage('If employee''s home dbdt has a rate override,  you will need to enter a rate number when manually overriding the dbdt!!!', mtWarning, [mbOK]);
      Exit;
    end;

    DM_COMPANY.CO_BRANCH.Filter := 'HOME_STATE_TYPE = '+ QuotedStr(HOME_STATE_TYPE_OVERRIDE) + ' and ( not OVERRIDE_PAY_RATE is null)';
    DM_COMPANY.CO_BRANCH.Filtered := True;

    if DM_COMPANY.CO_BRANCH.RecordCount > 0 then
    begin
      EvMessage('If employee''s home dbdt has a rate override,  you will need to enter a rate number when manually overriding the dbdt!!!', mtWarning, [mbOK]);
      Exit;
    end;

    DM_COMPANY.CO_DEPARTMENT.Filter := 'HOME_STATE_TYPE = '+ QuotedStr(HOME_STATE_TYPE_OVERRIDE) + ' and ( not OVERRIDE_PAY_RATE is null)';
    DM_COMPANY.CO_DEPARTMENT.Filtered := True;

    if DM_COMPANY.CO_DEPARTMENT.RecordCount > 0 then
    begin
      EvMessage('If employee''s home dbdt has a rate override,  you will need to enter a rate number when manually overriding the dbdt!!!', mtWarning, [mbOK]);
      Exit;
    end;

    DM_COMPANY.CO_TEAM.Filter := 'HOME_STATE_TYPE = '+ QuotedStr(HOME_STATE_TYPE_OVERRIDE) + ' and ( not OVERRIDE_PAY_RATE is null)';
    DM_COMPANY.CO_TEAM.Filtered := True;

    if DM_COMPANY.CO_TEAM.RecordCount > 0 then
    begin
      EvMessage('If employee''s home dbdt has a rate override,  you will need to enter a rate number when manually overriding the dbdt!!!', mtWarning, [mbOK]);
      Exit;
    end;
  finally
    DM_COMPANY.CO_DIVISION.Filter := VFilter;
    DM_COMPANY.CO_BRANCH.Filter := BFilter;
    DM_COMPANY.CO_DEPARTMENT.Filter := DFilter;
    DM_COMPANY.CO_TEAM.Filter := EFilter;
    DM_COMPANY.CO_DIVISION.Filtered := VWasFiltered;
    DM_COMPANY.CO_BRANCH.Filtered := BWasFiltered;
    DM_COMPANY.CO_DEPARTMENT.Filtered := DWasFiltered;
    DM_COMPANY.CO_TEAM.Filtered := EWasFiltered;
  end;
end;

procedure TEDIT_PR_BATCH.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
var
  s: string;
  AllOk: boolean;
  Q: IevQuery;
begin
  inherited;

  if (Kind = NavDelete) then
  begin
    if EvMessage('Are you sure you want to delete this BATCH?', mtConfirmation, mbOKCancel) = mrOK then
    begin
      if ctx_AccountRights.Functions.GetState('USER_CAN_DELETE_PAYROLL') = stEnabled then
      begin
        ctx_DBAccess.StartTransaction([dbtClient]);
        try
          Q := TevQuery.Create('{SetTransactionVar<CL_, PR_LOCK_CHANGE, 1>}', False);
          Q.Execute;
          DM_PAYROLL.PR_BATCH.Delete;
          (Owner as TFramePackageTmpl).ClickButton(NavCommit);
          ctx_DBAccess.CommitTransaction;
        except
          ctx_DBAccess.RollbackTransaction;
          raise;
        end;
      end;
    end;
    Handled := True;
  end;

  s := DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString;
  if (((Kind = NavOK) and  (DM_PAYROLL.PR_BATCH.State in [ dsEdit, dsInsert]))   or
     ((Kind = NavCommit) and DM_PAYROLL.PR_BATCH.IsDataModified)) and not NewBatch then
  begin
    if (s[1] in [PAYROLL_TYPE_REGULAR, PAYROLL_TYPE_SUPPLEMENTAL, PAYROLL_TYPE_REVERSAL,
      PAYROLL_TYPE_CL_CORRECTION, PAYROLL_TYPE_SB_CORRECTION, PAYROLL_TYPE_SETUP, PAYROLL_TYPE_IMPORT, PAYROLL_TYPE_BACKDATED]) and
      ((VarCompareValue(DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_BEGIN_DATE').OldValue, edPERIOD_BEGIN_DATE.DateTime) <> vrEqual) or
       (VarCompareValue(DM_PAYROLL.PR_BATCH.FieldByName('PERIOD_END_DATE').OldValue, edPERIOD_END_DATE.DateTime) <> vrEqual)) then
    begin
      ctx_DataAccess.UnlockPR;
      AllOK := False;
      try
         if DM_PAYROLL.PR_BATCH.State in [ dsEdit, dsInsert] then
            DM_PAYROLL.PR_BATCH.Post
         else
           ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_BATCH]);
           AllOK := True;
      finally
        ctx_DataAccess.LockPR(AllOK);
      end;
    end
    else begin
     if ctx_DataAccess.PayrollLocked and ((DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED)
       or (DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED)) then
       raise EUpdateError.CreateHelp('You can not change processed or voided payroll!', IDH_ConsistencyViolation);
    end;
  end;
end;

initialization
  RegisterClass(TEDIT_PR_BATCH);

end.
