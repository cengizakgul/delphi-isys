// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_PR_CHECK_STATES;

interface

uses
   SDataStructure, SPD_EDIT_PR_BASE, Wwdotdot, Wwdbcomb,
  StdCtrls, ExtCtrls, DBCtrls, wwdblook, Mask, wwdbedit, DBActns, Classes,
  ActnList, Db, Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, Controls,
  ComCtrls, SysUtils, ISBasicClasses, SDDClasses, SDataDictclient,
  SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBComboBox, isUIwwDBLookupCombo, isUIwwDBEdit, ImgList,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms;

type
  TEDIT_PR_CHECK_STATES = class(TEDIT_PR_BASE)
    wwDBGrid2: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    Label2: TevLabel;
    Label9: TevLabel;
    DBEdit8: TevDBEdit;
    wwlcEE_STATE: TevDBLookupCombo;
    DBRadioGroup1: TevDBRadioGroup;
    DBRadioGroup2: TevDBRadioGroup;
    DBRadioGroup3: TevDBRadioGroup;
    DBRadioGroup4: TevDBRadioGroup;
    Label3: TevLabel;
    wwDBComboBox1: TevDBComboBox;
    Label6: TevLabel;
    DBEdit1: TevDBEdit;
    evPanel1: TevPanel;
    Label1: TevLabel;
    DBText1: TevDBText;
    Label4: TevLabel;
    DBText4: TevDBText;
    Label5: TevLabel;
    DBText5: TevDBText;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    EESrc: TevDataSource;
    EEStatesSrc: TevDataSource;
    wwdsSubMaster1: TevDataSource;
    wwdsSubMaster2: TevDataSource;
    evSplitter1: TevSplitter;
    evDBGrid1: TevDBGrid;
    pnlLocation: TevPanel;
    evLabel2: TevLabel;
    evDBText1: TevDBText;
    evDBText2: TevDBText;
    evPanel3: TevPanel;
    evDBText3: TevDBText;
    evLabel4: TevLabel;
    evDBText4: TevDBText;
    butnPrior: TevBitBtn;
    butnNext: TevBitBtn;
    procedure butnPriorClick(Sender: TObject);
    procedure butnNextClick(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure PRPageControlChange(Sender: TObject);
    procedure EESrcDataChangeBeforeChildren(Sender: TObject;
      Field: TField);
    procedure PRNextExecute(Sender: TObject);
    procedure PRPriorExecute(Sender: TObject);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure ReopenPRCheckWithLines; override;
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure Activate; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

uses
  EvConsts;

{$R *.DFM}

procedure TEDIT_PR_CHECK_STATES.butnPriorClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Prior;
end;

procedure TEDIT_PR_CHECK_STATES.butnNextClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Next;
end;

procedure TEDIT_PR_CHECK_STATES.wwdsDataChange(Sender: TObject;
  Field: TField);
begin
  butnPrior.Enabled := wwdsSubMaster2.DataSet.RecNo > 1;
  butnNext.Enabled := wwdsSubMaster2.DataSet.RecNo < wwdsSubMaster2.DataSet.RecordCount;
end;

procedure TEDIT_PR_CHECK_STATES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_EMPLOYEE.EE, aDS);
  AddDS(DM_EMPLOYEE.EE_STATES, aDS);
  AddDS(DM_PAYROLL.PR_BATCH, aDS);
  inherited;
  RemoveDS(DM_PAYROLL.PR_CHECK_STATES, aDS);
end;

function TEDIT_PR_CHECK_STATES.GetInsertControl: TWinControl;
begin
  Result := wwlcEE_STATE;
end;

procedure TEDIT_PR_CHECK_STATES.ReopenPRCheckWithLines;
begin
  inherited;
  wwdsSubMaster1.DataSet := wwdsSubMaster1.DataSet;
  wwdsSubMaster2.DataSet := wwdsSubMaster2.DataSet;
end;

procedure TEDIT_PR_CHECK_STATES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;

  DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_CHECK_STATES_NBR=-1');

  AddDS(DM_PAYROLL.PR_BATCH, SupportDataSets);
  AddDS(DM_EMPLOYEE.EE, SupportDataSets);
  AddDS(DM_EMPLOYEE.EE_STATES, SupportDataSets);
end;

procedure TEDIT_PR_CHECK_STATES.PRPageControlChange(Sender: TObject);
begin
  inherited;
  Panel1.Visible := PageControl1.ActivePage = TabSheet1;
  evPanel1.Visible := not Panel1.Visible;
  ReopenPRCheckWithLines;
end;

function TEDIT_PR_CHECK_STATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR_CHECK_STATES;
end;

procedure TEDIT_PR_CHECK_STATES.EESrcDataChangeBeforeChildren(
  Sender: TObject; Field: TField);
begin
  inherited;
  if DM_EMPLOYEE.EE.Active then
    DM_EMPLOYEE.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
  if DM_PAYROLL.PR_CHECK.Active and not DM_PAYROLL.PR_CHECK.Eof then
    DM_PAYROLL.PR_CHECK_STATES.DataRequired('PR_CHECK_NBR='+DM_PAYROLL.PR_CHECK.PR_CHECK_NBR.AsString);
end;

procedure TEDIT_PR_CHECK_STATES.PRNextExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Next;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_STATES.PRPriorExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Prior;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_STATES.wwdsSubMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_PAYROLL.PR.Active then
    wwDBGrid1.ReadOnly := (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT);
end;

procedure TEDIT_PR_CHECK_STATES.Activate;
begin
  inherited;
end;

initialization
  RegisterClass(TEDIT_PR_CHECK_STATES);

end.
