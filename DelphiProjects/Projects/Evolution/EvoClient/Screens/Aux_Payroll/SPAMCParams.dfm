object PAMCParams: TPAMCParams
  Left = 565
  Top = 240
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'PA Monthly Cleanup Parameters'
  ClientHeight = 280
  ClientWidth = 338
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lablEmployees: TevLabel
    Left = 24
    Top = 104
    Width = 152
    Height = 13
    Caption = 'EE codes separated by commas'
    Enabled = False
  end
  object lablQuarterEndDate: TevLabel
    Left = 24
    Top = 152
    Width = 75
    Height = 13
    Caption = 'Month end date'
  end
  object lablMinimumTax: TevLabel
    Left = 152
    Top = 152
    Width = 128
    Height = 13
    Caption = 'Minimum tax to be adjusted'
  end
  object EvBevel1: TEvBevel
    Left = 8
    Top = 9
    Width = 321
    Height = 217
    Shape = bsFrame
  end
  object evBitBtn1: TevBitBtn
    Left = 16
    Top = 240
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 6
    OnClick = evBitBtn1Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
      DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
      DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
      DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
      DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
      DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
      2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
      A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
      AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
    NumGlyphs = 2
    Margin = 0
  end
  object evBitBtn2: TevBitBtn
    Left = 104
    Top = 240
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 7
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
      9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
      DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
      DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
      DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
      91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
      999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
    NumGlyphs = 2
    Margin = 0
  end
  object cbProcess: TevCheckBox
    Left = 24
    Top = 32
    Width = 129
    Height = 17
    Caption = 'Process automatically'
    Checked = True
    State = cbChecked
    TabOrder = 0
  end
  object cbPrintReports: TevCheckBox
    Left = 24
    Top = 56
    Width = 81
    Height = 17
    Caption = 'Print reports'
    Checked = True
    State = cbChecked
    TabOrder = 1
  end
  object cbAllEmployees: TevCheckBox
    Left = 24
    Top = 80
    Width = 129
    Height = 17
    Caption = 'Cleanup all employees'
    Checked = True
    State = cbChecked
    TabOrder = 2
    OnClick = cbAllEmployeesClick
  end
  object editEmployees: TevEdit
    Left = 24
    Top = 120
    Width = 289
    Height = 21
    Enabled = False
    TabOrder = 3
  end
  object dtMonthEndDate: TevDateTimePicker
    Left = 24
    Top = 168
    Width = 97
    Height = 21
    Date = 37526.000000000000000000
    Time = 37526.000000000000000000
    TabOrder = 4
  end
  object editMinTax: TMaskEdit
    Left = 152
    Top = 168
    Width = 57
    Height = 21
    TabOrder = 5
    Text = '0.10'
  end
  object updwChangeMonth: TUpDown
    Left = 121
    Top = 168
    Width = 15
    Height = 21
    Hint = 'Change Quarter'
    Associate = dtMonthEndDate
    Min = -1000
    Max = 1000
    TabOrder = 8
    OnClick = updwChangeMonthClick
  end
end
