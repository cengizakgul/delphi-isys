inherited EDIT_PR_CHECK_LOCALS: TEDIT_PR_CHECK_LOCALS
  inherited PageControl1: TevPageControl
    Top = 66
    Height = 211
    ActivePage = TabSheet4
    OnChange = PRPageControlChange
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Height = 182
        inherited pnlBorder: TevPanel
          Height = 178
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Height = 178
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 276
              Width = 524
            end
            inherited pnlSubbrowse: TevPanel
              Left = 276
              Width = 524
            end
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Height = 71
            inherited Splitter1: TevSplitter
              Height = 67
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 67
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 21
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_LOCALS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Height = 67
              object evSplitter1: TevSplitter [0]
                Left = 258
                Top = 48
                Height = 21
              end
              inherited PRGrid: TevDBGrid
                Width = 240
                Height = 21
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_LOCALS\PRGrid'
                Align = alLeft
                PaintOptions.AlternatingRowColor = 14544093
              end
              object evDBGrid1: TevDBGrid
                Left = 261
                Top = 48
                Width = 154
                Height = 21
                TabStop = False
                DisableThemesInTitle = False
                Selected.Strings = (
                  'PERIOD_BEGIN_DATE'#9'10'#9'Period Begin Date'#9
                  'PERIOD_END_DATE'#9'10'#9'Period End Date'#9'No'
                  'FREQUENCY'#9'1'#9'Frequency'#9'No'
                  'PAY_SALARY'#9'1'#9'Pay Salary'#9'No'
                  'PAY_STANDARD_HOURS'#9'1'#9'Pay Standard Hours'#9'No'
                  'LOAD_DBDT_DEFAULTS'#9'1'#9'Load Dbdt Defaults'#9'No')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_LOCALS\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster1
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Checks'
      ImageIndex = 30
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 0
        Width = 435
        Height = 182
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'EE_Custom_Number'#9'9'#9'EE Custom Nbr'
          'CUSTOM_PR_BANK_ACCT_NUMBER'#9'20'#9'Custom Pr Bank Acct Number'#9
          'PAYMENT_SERIAL_NUMBER'#9'10'#9'Payment Serial Number'#9
          'CHECK_TYPE'#9'1'#9'Check Type'#9
          'CO_DELIVERY_PACKAGE_NBR'#9'10'#9'Co Delivery Package Nbr'#9
          'SALARY'#9'10'#9'Salary'#9
          'EXCLUDE_DD'#9'1'#9'Exclude Dd'#9
          'EXCLUDE_DD_EXCEPT_NET'#9'1'#9'Exclude Dd Except Net'#9
          'EXCLUDE_TIME_OFF_ACCURAL'#9'1'#9'Exclude Time Off Accural'#9
          'EXCLUDE_AUTO_DISTRIBUTION'#9'1'#9'Exclude Auto Distribution'#9
          'EXCLUDE_ALL_SCHED_E_D_CODES'#9'1'#9'Exclude All Sched E D Codes'#9
          'EXCLUDE_SCH_E_D_EXCEPT_PENSION'#9'1'#9'Exclude Sch E D Except Pension'#9
          'OR_CHECK_FEDERAL_VALUE'#9'10'#9'Or Check Federal Value'#9
          'OR_CHECK_FEDERAL_TYPE'#9'1'#9'Or Check Federal Type'#9
          'OR_CHECK_OASDI'#9'10'#9'Or Check Oasdi'#9
          'OR_CHECK_MEDICARE'#9'10'#9'Or Check Medicare'#9
          'OR_CHECK_EIC'#9'10'#9'Or Check Eic'#9
          'OR_CHECK_BACK_UP_WITHHOLDING'#9'10'#9'Or Check Back Up Withholding'#9
          'EXCLUDE_FEDERAL'#9'1'#9'Exclude Federal'#9
          'EXCLUDE_ADDITIONAL_FEDERAL'#9'1'#9'Exclude Additional Federal'#9
          'EXCLUDE_EMPLOYEE_OASDI'#9'1'#9'Exclude Employee Oasdi'#9
          'EXCLUDE_EMPLOYER_OASDI'#9'1'#9'Exclude Employer Oasdi'#9
          'EXCLUDE_EMPLOYEE_MEDICARE'#9'1'#9'Exclude Employee Medicare'#9
          'EXCLUDE_EMPLOYER_MEDICARE'#9'1'#9'Exclude Employer Medicare'#9
          'EXCLUDE_EMPLOYEE_EIC'#9'1'#9'Exclude Employee Eic'#9
          'EXCLUDE_EMPLOYER_FUI'#9'1'#9'Exclude Employer Fui'#9
          'TAX_AT_SUPPLEMENTAL_RATE'#9'1'#9'Tax At Supplemental Rate'#9
          'TAX_FREQUENCY'#9'1'#9'Tax Frequency'#9
          'CHECK_STATUS'#9'1'#9'Check Status'#9
          'STATUS_CHANGE_DATE'#9'10'#9'Status Change Date'#9
          'GROSS_WAGES'#9'10'#9'Gross Wages'#9
          'NET_WAGES'#9'10'#9'Net Wages'#9
          'FEDERAL_TAXABLE_WAGES'#9'10'#9'Federal Taxable Wages'#9
          'FEDERAL_TAX'#9'10'#9'Federal Tax'#9
          'EE_OASDI_TAXABLE_WAGES'#9'10'#9'Ee Oasdi Taxable Wages'#9
          'EE_OASDI_TAXABLE_TIPS'#9'10'#9'Ee Oasdi Taxable Tips'#9
          'EE_OASDI_TAX'#9'10'#9'Ee Oasdi Tax'#9
          'EE_MEDICARE_TAXABLE_WAGES'#9'10'#9'Ee Medicare Taxable Wages'#9
          'EE_MEDICARE_TAX'#9'10'#9'Ee Medicare Tax'#9
          'EE_EIC_TAX'#9'10'#9'Ee Eic Tax'#9
          'ER_OASDI_TAXABLE_WAGES'#9'10'#9'Er Oasdi Taxable Wages'#9
          'ER_OASDI_TAXABLE_TIPS'#9'10'#9'Er Oasdi Taxable Tips'#9
          'ER_OASDI_TAX'#9'10'#9'Er Oasdi Tax'#9
          'ER_MEDICARE_TAXABLE_WAGES'#9'10'#9'Er Medicare Taxable Wages'#9
          'ER_MEDICARE_TAX'#9'10'#9'Er Medicare Tax'#9
          'ER_FUI_TAXABLE_WAGES'#9'10'#9'Er Fui Taxable Wages'#9
          'ER_FUI_TAX'#9'10'#9'Er Fui Tax'#9
          'EXCLUDE_FROM_AGENCY'#9'1'#9'Exclude From Agency'#9
          'FEDERAL_SHORTFALL'#9'10'#9'Federal Shortfall'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_CHECK_LOCALS\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsSubMaster2
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object Label2: TevLabel
        Left = 8
        Top = 336
        Width = 35
        Height = 16
        Caption = '~Local'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TevLabel
        Left = 304
        Top = 336
        Width = 79
        Height = 13
        Caption = 'Override Amount'
        FocusControl = DBEdit3
      end
      object evLabel3: TevLabel
        Left = 8
        Top = 388
        Width = 77
        Height = 16
        Caption = '~NonRes Local'
        FocusControl = NONRES_LOCAL_Combo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel5: TevLabel
        Left = 8
        Top = 436
        Width = 50
        Height = 16
        Caption = '~Location'
        FocusControl = LOCATION_Combo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 435
        Height = 321
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'LOCAL_NAME'
          'LOCAL_TAXABLE_WAGE'#9'10'#9'Local Taxable Wages'#9
          'LOCAL_GROSS_WAGES'#9'10'#9'Local Gross Wages'#9
          'LOCAL_TAX'#9'10'#9'Local Tax'#9
          'EXCLUDE_LOCAL'#9'1'#9'Exclude Local'#9
          'OVERRIDE_AMOUNT'#9'10'#9'Override Amount'#9
          'LOCAL_SHORTFALL'#9'10'#9'Local Shortfall'#9
          'Location'#9'20'#9'Location'#9'F'
          'NonRes'#9'20'#9'Non-Res'#9'F'
          'REPORTABLE'#9'1'#9'Reportable'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_CHECK_LOCALS\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alTop
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object DBEdit3: TevDBEdit
        Left = 304
        Top = 352
        Width = 105
        Height = 21
        DataField = 'OVERRIDE_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwlcEE_LOCAL: TevDBLookupCombo
        Left = 8
        Top = 352
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup')
        DataField = 'EE_LOCALS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_EE_LOCALS.EE_LOCALS
        LookupField = 'EE_LOCALS_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object DBRadioGroup1: TevDBRadioGroup
        Left = 160
        Top = 336
        Width = 113
        Height = 41
        Caption = '~Exclude Local'
        Columns = 2
        DataField = 'EXCLUDE_LOCAL'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 304
        Top = 400
        Width = 105
        Height = 41
        Caption = '~Reportable'
        Columns = 2
        DataField = 'REPORTABLE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 4
        Values.Strings = (
          'Y'
          'N')
      end
      object NONRES_LOCAL_Combo: TevDBLookupCombo
        Left = 8
        Top = 404
        Width = 265
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup')
        DataField = 'NONRES_EE_LOCALS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_EE_LOCALS.EE_LOCALS
        LookupField = 'EE_LOCALS_NBR'
        Style = csDropDownList
        TabOrder = 5
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object LOCATION_Combo: TevDBLookupCombo
        Left = 8
        Top = 452
        Width = 265
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'ADDRESS1'#9'30'#9'Address1'#9'F'
          'CITY'#9'20'#9'City'#9'F')
        DataField = 'CO_LOCATIONS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CO_LOCATIONS.CO_LOCATIONS
        LookupField = 'CO_LOCATIONS_NBR'
        Style = csDropDownList
        TabOrder = 6
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  object evPanel1: TevPanel [2]
    Left = 0
    Top = 33
    Width = 443
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object Label1: TevLabel
      Left = 8
      Top = 0
      Width = 44
      Height = 13
      Caption = 'Check #:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText1: TevDBText
      Left = 64
      Top = 0
      Width = 73
      Height = 17
      DataField = 'PAYMENT_SERIAL_NUMBER'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TevLabel
      Left = 241
      Top = 0
      Width = 27
      Height = 13
      Caption = 'EE #:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText4: TevDBText
      Left = 277
      Top = 0
      Width = 85
      Height = 17
      DataField = 'EE_Custom_Number'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TevLabel
      Left = 8
      Top = 16
      Width = 48
      Height = 13
      Caption = 'EE Name:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText5: TevDBText
      Left = 64
      Top = 16
      Width = 156
      Height = 17
      DataField = 'Employee_Name_Calculate'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pnlLocation: TevPanel
      Left = 224
      Top = 12
      Width = 297
      Height = 25
      BevelOuter = bvNone
      TabOrder = 0
      object evLabel2: TevLabel
        Left = 0
        Top = 4
        Width = 41
        Height = 13
        Caption = 'Payroll:#'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evDBText1: TevDBText
        Left = 50
        Top = 4
        Width = 15
        Height = 25
        DataField = 'RUN_NUMBER'
        DataSource = wwdsSubMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evDBText2: TevDBText
        Left = 72
        Top = 4
        Width = 64
        Height = 13
        AutoSize = True
        DataField = 'CHECK_DATE'
        DataSource = wwdsSubMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object evPanel3: TevPanel
        Left = 144
        Top = 4
        Width = 177
        Height = 25
        BevelOuter = bvNone
        TabOrder = 0
        object evDBText3: TevDBText
          Left = 0
          Top = 0
          Width = 64
          Height = 25
          Align = alLeft
          AutoSize = True
          DataField = 'PERIOD_BEGIN_DATE'
          DataSource = wwdsSubMaster1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object evLabel4: TevLabel
          Left = 64
          Top = 0
          Width = 9
          Height = 25
          Align = alLeft
          Caption = ' - '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText4: TevDBText
          Left = 73
          Top = 0
          Width = 64
          Height = 25
          Align = alLeft
          AutoSize = True
          DataField = 'PERIOD_END_DATE'
          DataSource = wwdsSubMaster1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object butnPrior: TevBitBtn
      Left = 528
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Prior'
      TabOrder = 1
      OnClick = butnPriorClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDD
        848DDDDDDDDDDDDDF8FDDDDDDDDDDD84448DDDDDDDDDDDF888FDDDDDDDDD844F
        F48DDDDDDDDDF88DD8FDDDDDDD844FFFF48DDDDDDDF88DDDD8FDDDDD844FFFFF
        F48DDDDDF88DDDDDD8FDDDD44FFFFFFFF48DDDD88DDDDDDDD8FDDD477FFFFFFF
        F48DDD8FFDDDDDDDD8FDDDD4477FFFFFF48DDDD88FFDDDDDD8FDDDDDD4477FFF
        F48DDDDDD88FFDDDD8FDDDDDDDD4477FF48DDDDDDDD88FFDD8FDDDDDDDDDD447
        748DDDDDDDDDD88FF8FDDDDDDDDDDDD4448DDDDDDDDDDDD888FDDDDDDDDDDDDD
        D4DDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnNext: TevBitBtn
      Left = 608
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Next'
      TabOrder = 2
      OnClick = butnNextClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDDD4888DDDDDDD
        DDDDD8FFDDDDDDDDDDDDD444888DDDDDDDDDD888FFDDDDDDDDDDD47F44888DDD
        DDDDD8FD88FFDDDDDDDDD47FFF44888DDDDDD8FDDD88FFDDDDDDD47FFFFF4488
        8DDDD8FDDDDD88FFDDDDD47FFFFFFF4488DDD8FDDDDDDD88FDDDD47FFFFFFF77
        4DDDD8FDDDDDDDFF8DDDD47FFFFF7744DDDDD8FDDDDDFF88DDDDD47FFF7744DD
        DDDDD8FDDDFF88DDDDDDD47F7744DDDDDDDDD8FDFF88DDDDDDDDD47744DDDDDD
        DDDDD8FF88DDDDDDDDDDD444DDDDDDDDDDDDD888DDDDDDDDDDDDD4DDDDDDDDDD
        DDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 65530
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_PR_CHECK_LOCALS.PR_CHECK_LOCALS
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = wwdsSubMaster2
    Top = 65530
  end
  inherited wwdsList: TevDataSource
    Top = 65530
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 329
    Top = 119
  end
  inherited dsCL: TevDataSource
    Left = 287
    Top = 119
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 360
    Top = 127
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 410
    Top = 114
  end
  inherited wwdsSubMaster: TevDataSource
    OnDataChange = wwdsSubMasterDataChange
    Top = 80
  end
  inherited DM_PAYROLL: TDM_PAYROLL
    Left = 455
    Top = 114
  end
  inherited ActionList: TevActionList
    inherited PRNext: TDataSetNext
      OnExecute = PRNextExecute
    end
    inherited PRPrior: TDataSetPrior
      OnExecute = PRPriorExecute
    end
  end
  object wwdsSubMaster1: TevDataSource
    DataSet = DM_PR_BATCH.PR_BATCH
    MasterDataSource = wwdsSubMaster
    Left = 212
    Top = 104
  end
  object wwdsSubMaster2: TevDataSource
    DataSet = DM_PR_CHECK.PR_CHECK
    OnDataChange = wwdsDataChange
    MasterDataSource = wwdsSubMaster1
    OnDataChangeBeforeChildren = wwdsSubMaster2DataChangeBeforeChildren
    Left = 251
    Top = 122
  end
  object EESrc: TevDataSource
    DataSet = DM_EE.EE
    Left = 495
    Top = 72
  end
  object EEStatesSrc: TevDataSource
    DataSet = DM_EE_LOCALS.EE_LOCALS
    MasterDataSource = EESrc
    Left = 528
    Top = 72
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 502
    Top = 122
  end
end
