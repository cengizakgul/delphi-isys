object MoveOneDlg: TMoveOneDlg
  Left = 446
  Top = 276
  Width = 443
  Height = 403
  Caption = 'Move Wages'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 24
    Top = 8
    Width = 82
    Height = 13
    Caption = 'Amount To Move'
    FocusControl = AmountEdit
  end
  object evLabel2: TevLabel
    Left = 24
    Top = 80
    Width = 96
    Height = 13
    Caption = 'Non Residential Tax'
  end
  object evLabel3: TevLabel
    Left = 24
    Top = 128
    Width = 73
    Height = 13
    Caption = 'Residential Tax'
  end
  object evLabel4: TevLabel
    Left = 24
    Top = 176
    Width = 54
    Height = 13
    Caption = 'School Tax'
  end
  object EvBevel1: TEvBevel
    Left = 24
    Top = 315
    Width = 393
    Height = 2
  end
  object evLabel5: TevLabel
    Left = 24
    Top = 40
    Width = 74
    Height = 13
    Caption = 'Hours To Move'
    FocusControl = HoursEdit
  end
  object DbdtLabel: TLabel
    Left = 24
    Top = 232
    Width = 24
    Height = 13
    Caption = '///   '
  end
  object Label16: TevLabel
    Left = 26
    Top = 273
    Width = 17
    Height = 13
    Caption = 'Job'
  end
  object AmountEdit: TevEdit
    Left = 120
    Top = 8
    Width = 81
    Height = 21
    TabOrder = 0
    Text = '0'
  end
  object cbNR: TevComboBox
    Left = 25
    Top = 96
    Width = 392
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object cbRE: TevComboBox
    Left = 25
    Top = 144
    Width = 392
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
  end
  object cbSC: TevComboBox
    Left = 25
    Top = 192
    Width = 392
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
  end
  object evButton1: TevButton
    Left = 24
    Top = 336
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 5
    Margin = 0
  end
  object evButton2: TevButton
    Left = 112
    Top = 336
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
    Margin = 0
  end
  object HoursEdit: TevEdit
    Left = 120
    Top = 40
    Width = 81
    Height = 21
    TabOrder = 1
    Text = '0'
  end
  object Button1: TButton
    Left = 328
    Top = 232
    Width = 89
    Height = 25
    Caption = 'DBDT'
    TabOrder = 7
    OnClick = Button1Click
  end
  object cbJob: TevComboBox
    Left = 57
    Top = 272
    Width = 360
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 8
  end
end
