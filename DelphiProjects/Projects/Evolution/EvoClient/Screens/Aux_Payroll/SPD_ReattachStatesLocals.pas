// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ReattachStatesLocals;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls,  Grids, Wwdbigrd, Wwdbgrid, DB,
   Wwdatsrc, EvUtils, SDataStructure, EvConsts,   DateUtils, Dialogs,
  wwdblook, EvTypes, SDDClasses, kbmMemTable, EvContext,
  ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictclient,SFieldCodeValues, EvExceptions,
  isBaseClasses, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, EvDataset, EvCommonInterfaces;

type
  TReattachStatesLocals = class(TForm)
    pcMain: TevPageControl;
    tsPayrolls: TTabSheet;
    tsStatesLocals: TTabSheet;
    evPanel2: TevPanel;
    evPanel3: TevPanel;
    dsEmployees: TevDataSource;
    dsPayrolls: TevDataSource;
    cdsPayrolls: TevClientDataSet;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    evSpeedButton1: TevSpeedButton;
    wwDBGrid6: TevDBGrid;
    evDBGrid1: TevDBGrid;
    evDBGrid2: TevDBGrid;
    evSplitter1: TevSplitter;
    evSpeedButton2: TevSpeedButton;
    lcStates: TevDBLookupCombo;
    lcLocals: TevDBLookupCombo;
    dsStates: TevDataSource;
    dsLocals: TevDataSource;
    cdsStates: TevClientDataSet;
    cdsLocals: TevClientDataSet;
    cdsStatesStates: TStringField;
    cdsStatesEE_STATES_NBR: TIntegerField;
    cdsStatesDEST_EE_STATES_NBR: TIntegerField;
    cdsStatesDest_State: TStringField;
    DM_COMPANY: TDM_COMPANY;
    cdsLocalsEE_LOCALS_NBR: TIntegerField;
    cdsLocalsLocal: TStringField;
    cdsLocalsDEST_EE_LOCALS_NBR: TIntegerField;
    cdsLocalsDest_Local: TStringField;
    cdsStatesEE_NBR: TIntegerField;
    cdsLocalsEE_NBR: TIntegerField;
    dsEE_STATES: TevDataSource;
    dsEE_LOCALS: TevDataSource;
    evPanel4: TevPanel;
    wwDBGrid4: TevDBLookupCombo;
    evLabel1: TevLabel;
    lcSUI: TevDBLookupCombo;
    evDBGrid3: TevDBGrid;
    evSplitter2: TevSplitter;
    dsSUI: TevDataSource;
    cdsSUI: TevClientDataSet;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    IntegerField3: TIntegerField;
    StringField2: TStringField;
    evLabel2: TevLabel;
    cbYear: TevComboBox;
    evCbNewPayroll: TevCheckBox;
    procedure FormShow(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure pcMainChange(Sender: TObject);
    procedure wwDBGrid4Change(Sender: TObject);
    procedure evSpeedButton2Click(Sender: TObject);
    procedure cdsSUICalcFields(DataSet: TDataSet);
    procedure FormHide(Sender: TObject);
    procedure cbYearChange(Sender: TObject);
    procedure wwDBGrid4BeforeDropDown(Sender: TObject);
    procedure wwDBGrid4CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evCbNewPayrollClick(Sender: TObject);
  private
    bYearChange: boolean;
    function CreateInStatement(Grid: TevDBGrid; FieldName: string): string;
    function ConvertToString(v: Variant): string;
  public
    { Public declarations }
  end;

implementation

uses Variants;

{$R *.dfm}

procedure TReattachStatesLocals.FormShow(Sender: TObject);
begin
  cbYear.Items.Add(IntToStr(CurrentYear-5));
  cbYear.Items.Add(IntToStr(CurrentYear-4));
  cbYear.Items.Add(IntToStr(CurrentYear-3));
  cbYear.Items.Add(IntToStr(CurrentYear-2));
  cbYear.Items.Add(IntToStr(CurrentYear-1));
  cbYear.Items.Add(IntToStr(CurrentYear));
  cbYear.ItemIndex := 5;
  bYearChange := True;
  pcMain.ActivePage := tsPayrolls;
  tsStatesLocals.TabVisible := False;

  DM_EMPLOYEE.EE.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');

  cdsPayrolls.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString + ' and STATUS=''' + PAYROLL_STATUS_PROCESSED +
                                      ''' and ExtractYear(CHECK_DATE)=' + IntToStr(CurrentYear));
  cdsStates.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  cdsSUI.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  cdsLocals.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');
  DM_EMPLOYEE.EE_STATES.Close;
  DM_EMPLOYEE.EE_STATES.RetrieveCondition := ''; 
  DM_EMPLOYEE.EE_LOCALS.Close;

  DM_EMPLOYEE.EE_STATES.AsOfDate := Trunc(DM_PAYROLL.PR.CHECK_DATE.Value);
  DM_EMPLOYEE.EE_LOCALS.AsOfDate := Trunc(DM_PAYROLL.PR.CHECK_DATE.Value);

  lcStates.LookupTable := nil;
  lcSUI.LookupTable := nil;
  lcLocals.LookupTable := nil;
  ctx_DataAccess.OpenDataSets([DM_EMPLOYEE.EE, cdsPayrolls, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_LOCALS]);
  cdsStates.CreateDataSet;
  cdsSUI.CreateDataSet;
  cdsLocals.CreateDataSet;


  dsEE_STATES.MasterDataSource := dsEmployees;
  dsEE_LOCALS.MasterDataSource := dsEmployees;
  lcStates.LookupTable := DM_EMPLOYEE.EE_STATES;
  lcSUI.LookupTable := DM_EMPLOYEE.EE_STATES;
  lcLocals.LookupTable := DM_EMPLOYEE.EE_LOCALS;
  wwDBGrid6.SelectAll;
end;

procedure TReattachStatesLocals.evSpeedButton1Click(Sender: TObject);
begin
  if (wwDBGrid4.LookupValue = '') or (wwDBGrid6.SelectedList.Count = 0) then
    raise EInvalidParameters.Create('Please, select employee and payrolls first');
  tsStatesLocals.TabVisible := True;
  pcMain.ActivatePage(tsStatesLocals);
end;

function TReattachStatesLocals.CreateInStatement(Grid: TevDBGrid; FieldName: string): string;
var
  i: Integer;
  Nbr: TBookmarkStr;
  f: TField;
begin
  Grid.DataSource.DataSet.DisableControls;
  Nbr := Grid.DataSource.DataSet.Bookmark;
  try
    Result := '(t2.' + FieldName + ' in (0';
    f := Grid.DataSource.DataSet.FieldByName(FieldName);
    for i := 1 to Grid.SelectedList.Count do
    begin
      Grid.DataSource.DataSet.GotoBookmark(Grid.SelectedList[Pred(i)]);
      Result := Result + ',' + f.AsString;
      if i mod 1000 = 0 then
      begin
        Result := Result + ') or t2.' + FieldName + ' in (0';
      end;
    end;
    Result := Result + '))';
  finally
    Grid.DataSource.DataSet.Bookmark := Nbr;
    Grid.DataSource.DataSet.EnableControls;
  end;
end;

procedure TReattachStatesLocals.pcMainChange(Sender: TObject);
begin
  if pcMain.ActivePage = tsStatesLocals then
  begin
    if (cdsStates.RecordCount <> 0) or
       (cdsLocals.RecordCount <> 0) or
       (cdsSUI.RecordCount <> 0) then
      Exit;
    DM_EMPLOYEE.EE_STATES.First;
    while not DM_EMPLOYEE.EE_STATES.Eof do
    begin
      cdsStates.Append;
      cdsStates['EE_NBR'] := DM_EMPLOYEE.EE_STATES['EE_NBR'];
      cdsStates['EE_STATES_NBR'] := DM_EMPLOYEE.EE_STATES['EE_STATES_NBR'];
      cdsStates['DEST_EE_STATES_NBR'] := DM_EMPLOYEE.EE_STATES['EE_STATES_NBR'];
      cdsStates.Post;
      DM_EMPLOYEE.EE_STATES.Next;
    end;
    cdsStates.First;
    cdsSUI.Data := cdsStates.Data;

    DM_EMPLOYEE.EE_LOCALS.First;
    while not DM_EMPLOYEE.EE_LOCALS.Eof do
    begin
      cdsLocals.Append;
      cdsLocals['EE_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_NBR'];
      cdsLocals['EE_LOCALS_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'];
      cdsLocals['DEST_EE_LOCALS_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'];
      cdsLocals.Post;
      DM_EMPLOYEE.EE_LOCALS.Next;
    end;
    cdsLocals.First;
  end
  else
    tsStatesLocals.TabVisible := False;
end;

procedure TReattachStatesLocals.wwDBGrid4Change(Sender: TObject);
begin
  cdsStates.EmptyDataSet;
  cdsLocals.EmptyDataSet;
  cdsSUI.EmptyDataSet;
end;


procedure TReattachStatesLocals.evSpeedButton2Click(Sender: TObject);
var
  aStates: array of record
    EeNbr: Integer;
    OldNbr: Integer;
    NewNbr: Integer;
  end;
  aSUI: array of record
    EeNbr: Integer;
    OldNbr: Integer;
    NewNbr: Integer;
  end;
  aLocals: array of record
    EeNbr: Integer;
    OldNbr: Integer;
    NewNbr: Integer;
    OldStateNbr: Integer;
    NewStateNbr: Integer;
  end;
  StateNbr, LocalsNbr: Integer;
  i: Integer;
  bp: TDataSetNotifyEvent;
  TaxAmount : Real;

  CheckDate, PeriodBeginDate, PeriodEndDate: String;
  bTemp : boolean;
  SameYear : boolean;
  StrNbr: String;

  PayFrequency : String;
  EEFilter : String;
  CoNbr : integer;
  tmpPr_Check_nbr : integer;
  s, Cond, Cond1 : String;
  Tmp_Pr_Check_Lines_Nbr : IisListOfValues;
  Q: IevQuery;

  function CreatePrNbrList: String;
  var
    i: Integer;
    Nbr: TBookmarkStr;
  begin
    wwDBGrid6.DataSource.DataSet.DisableControls;
    Nbr := wwDBGrid6.DataSource.DataSet.Bookmark;
    try
      Result := '( c.PR_NBR in (0';
      for i := 1 to wwDBGrid6.SelectedList.Count do
      begin
        wwDBGrid6.DataSource.DataSet.GotoBookmark(wwDBGrid6.SelectedList[Pred(i)]);
        Result := Result + ',' + wwDBGrid6.DataSource.DataSet.FieldByName('PR_NBR').AsString;
        if i mod 1000 = 0 then
        begin
          Result := Result + ') or c.PR_NBR in (0';
        end;
      end;
      Result := Result + '))';
    finally
      wwDBGrid6.DataSource.DataSet.Bookmark := Nbr;
      wwDBGrid6.DataSource.DataSet.EnableControls;
    end;
  end;

begin

  if (not evCbNewPayroll.Checked) and (YearOf(DM_PAYROLL.PR['CHECK_DATE']) <> StrToInt(cbYear.Text)) then
    if EvMessage('You must move wages in a payroll with the same year as the original check date.  Would you like to create a new payroll now?!', mtConfirmation, [mbNo, mbYes], mbNo) = mrYes then
      evCbNewPayroll.Checked := True
    else
      Exit;

  SetLength(aLocals, 0);
  SetLength(aStates, 0);
  SetLength(aSUI, 0);

  StateNbr := cdsStates.RecNo;
  cdsStates.DisableControls;
  cdsStates.First;
  try
    while not cdsStates.Eof do
    begin
      if cdsStates['EE_STATES_NBR'] <> cdsStates['DEST_EE_STATES_NBR'] then
      begin
        Assert(DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', cdsStates['DEST_EE_STATES_NBR'], []));
        if (DM_EMPLOYEE.EE_STATES.STATE_EXEMPT_EXCLUDE.Value = GROUP_BOX_EXEMPT) or
           (DM_EMPLOYEE.EE_STATES.EE_SDI_EXEMPT_EXCLUDE.Value = GROUP_BOX_EXEMPT) or
           (DM_EMPLOYEE.EE_STATES.ER_SDI_EXEMPT_EXCLUDE.Value = GROUP_BOX_EXEMPT) then
          raise EInvalidOperation.Create('Unable to Move wages to ' + DM_EMPLOYEE.EE_STATES.FieldByName('State_Lookup').AsString + ' - EE is exempt');

        SetLength(aStates, Length(aStates) + 1);
        aStates[High(aStates)].EeNbr := cdsStates['EE_NBR'];
        aStates[High(aStates)].OldNbr := cdsStates['EE_STATES_NBR'];
        aStates[High(aStates)].NewNbr := cdsStates['DEST_EE_STATES_NBR'];
      end;

      cdsStates.Next;
    end;
  finally
    cdsStates.RecNo := StateNbr;
    cdsStates.EnableControls;
  end;

  StateNbr := cdsSUI.RecNo;
  cdsSUI.DisableControls;
  cdsSUI.First;
  try
    while not cdsSUI.Eof do
    begin
      if cdsSUI['EE_STATES_NBR'] <> cdsSUI['DEST_EE_STATES_NBR'] then
      begin
        Assert(DM_EMPLOYEE.EE_STATES.Locate('EE_STATES_NBR', cdsSUI['DEST_EE_STATES_NBR'], []));
        if (DM_EMPLOYEE.EE_STATES.ER_SUI_EXEMPT_EXCLUDE.Value = GROUP_BOX_EXEMPT) or
           (DM_EMPLOYEE.EE_STATES.EE_SUI_EXEMPT_EXCLUDE.Value = GROUP_BOX_EXEMPT) then
          raise EInvalidOperation.Create('Unable to Move wages to ' +
                    ConvertToString(DM_EMPLOYEE.EE_STATES['State_Lookup;SDI_State_Lookup;SUI_State_Lookup']) +
                            ' - EE is exempt');

        SetLength(aSUI, Succ(Length(aSUI)));
        with aSUI[High(aSUI)] do
        begin
          EeNbr := cdsSUI['EE_NBR'];
          OldNbr := cdsSUI['EE_STATES_NBR'];
          NewNbr := cdsSUI['DEST_EE_STATES_NBR'];
        end;
      end;
      cdsSUI.Next;
    end;
  finally
    cdsSUI.RecNo := StateNbr;
    cdsSUI.EnableControls;
  end;

  DM_COMPANY.CO_LOCAL_TAX.DataRequired();

  LocalsNbr := cdsLocals.RecNo;
  cdsLocals.DisableControls;
  cdsLocals.First;
  try
    while not cdsLocals.Eof do
    begin
      if cdsLocals['EE_LOCALS_NBR'] <> cdsLocals['DEST_EE_LOCALS_NBR'] then
      begin
        Assert(DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', cdsLocals['DEST_EE_LOCALS_NBR'], []));
        if DM_EMPLOYEE.EE_LOCALS.EXEMPT_EXCLUDE.Value = GROUP_BOX_EXEMPT then
          raise EInvalidOperation.Create('Unable to Move wages to ' + DM_EMPLOYEE.EE_LOCALS.FieldByName('Local_Name_Lookup').AsString + ' - EE is exempt');
        SetLength(aLocals, Succ(Length(aLocals)));
        with aLocals[High(aLocals)] do
        begin
          EeNbr := cdsLocals['EE_NBR'];
          OldNbr := cdsLocals['EE_LOCALS_NBR'];
          NewNbr := cdsLocals['DEST_EE_LOCALS_NBR'];

          Assert(DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'], []));
          Assert(DM_EMPLOYEE.EE_STATES.Locate('State_Lookup', DM_COMPANY.CO_LOCAL_TAX['localState'], []), 'There is a local ''' + DM_COMPANY.CO_LOCAL_TAX['localName'] + ''', but there is no ' + DM_COMPANY.CO_LOCAL_TAX['localState'] + ' state setup on for this employee');
          OldStateNbr := DM_EMPLOYEE.EE_STATES['EE_STATES_NBR'];
          Assert(DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', NewNbr, []));
          Assert(DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'], []));
          Assert(DM_EMPLOYEE.EE_STATES.Locate('State_Lookup', DM_COMPANY.CO_LOCAL_TAX['localState'], []), 'There is a local ''' + DM_COMPANY.CO_LOCAL_TAX['localName'] + ''', but there is no ' + DM_COMPANY.CO_LOCAL_TAX['localState'] + ' state setup on for this employee');
          NewStateNbr := DM_EMPLOYEE.EE_STATES['EE_STATES_NBR'];
        end;
      end;
      cdsLocals.Next;
    end;
  finally
    cdsLocals.RecNo := LocalsNbr;
    cdsLocals.EnableControls;
  end;

  if (Length(aStates) = 0) and (Length(aLocals) = 0) and (Length(aSUI) = 0) then
    raise EInvalidParameters.Create('All source states and locals are equal to destination states and locals. Nothing to do');

  bTemp := ctx_DataAccess.TempCommitDisable;
  ctx_DataAccess.TempCommitDisable := True;
  ctx_DataAccess.UnlockPRSimple;
  bp := DM_PAYROLL.PR_CHECK_LINES.BeforePost;
  try

    if evCbNewPayroll.Checked then
    begin

      SameYear := true;
      repeat
        if not EvDialog('Input required', 'Please, enter a Check Date', CheckDate) then
          Exit
        else
        if YearOf(StrToDate(CheckDate)) <> StrToInt(cbYear.Text) then
        begin
          EvMessage('Please enter the same year as that of the check dates you are moving wages.!', mtWarning, [mbOK]);
          SameYear := False;
        end
        else
          SameYear := True;
      until SameYear;

      if not EvDialog('Input required', 'Please, enter a Period Begin Date', PeriodBeginDate) then
        Exit;

      if not EvDialog('Input required', 'Please, enter a Period End Date', PeriodEndDate) then
        Exit;

      if not EvDialog('Input required', 'Please, enter a Manual Check Number', StrNbr) then
        Exit;

      PayFrequency := DM_EMPLOYEE.EE['PAY_FREQUENCY'];
      EEFilter := DM_EMPLOYEE.EE.Filter;
      CoNbr := DM_EMPLOYEE.EE['CO_NBR'];


      DM_PAYROLL.PR.Append;

      DM_PAYROLL.PR['CHECK_DATE'] := CheckDate;

      DM_PAYROLL.PR['CHECK_DATE_STATUS'] := DATE_STATUS_IGNORE;
      DM_PAYROLL.PR['SCHEDULED'] := GROUP_BOX_NO;

      Q := TevQuery.Create('GenericSelectCurrentNBRWithCondition');
      Q.Macro['Columns'] := 'max(RUN_NUMBER)';
      Q.Macro['TableName'] := 'PR';
      Q.Macro['NbrField'] := 'CO';
      Q.Macro['Condition'] := 'CHECK_DATE = :CheckDate';
      Q.Param['RecordNbr'] := CoNbr;
      Q.Param['CheckDate'] := CheckDate;

      DM_PAYROLL.PR['PAYROLL_TYPE'] := PAYROLL_TYPE_REGULAR;
      DM_PAYROLL.PR['RUN_NUMBER']   := Q.Result.Fields[0].AsInteger + 1;
      DM_PAYROLL.PR['STATUS']       := PAYROLL_STATUS_PENDING;
      DM_PAYROLL.PR['STATUS_DATE']  := SysTime;
      DM_PAYROLL.PR['EXCLUDE_ACH']  := GROUP_BOX_NO;
      DM_PAYROLL.PR['EXCLUDE_TAX_DEPOSITS'] := GROUP_BOX_NO;
      DM_PAYROLL.PR['EXCLUDE_AGENCY'] := GROUP_BOX_NO;
      DM_PAYROLL.PR['MARK_LIABS_PAID_DEFAULT'] := GROUP_BOX_NO;
      DM_PAYROLL.PR['EXCLUDE_BILLING'] := GROUP_BOX_NO;
      DM_PAYROLL.PR['EXCLUDE_R_C_B_0R_N'] := GROUP_BOX_NONE;
      DM_PAYROLL.PR['INVOICE_PRINTED'] := INVOICE_PRINT_STATUS_calculated_do_not_print;
      DM_PAYROLL.PR['SCHEDULED_CALL_IN_DATE'] := Date;
      DM_PAYROLL.PR['SCHEDULED_PROCESS_DATE'] := Date;
      DM_PAYROLL.PR['SCHEDULED_DELIVERY_DATE'] := Date;
      DM_PAYROLL.PR['EXCLUDE_TIME_OFF'] := TIME_OFF_EXCLUDE_ACCRUAL_ALL;
      DM_PAYROLL.PR['CO_NBR'] := CoNbr;
      DM_PAYROLL.PR['PRINT_ALL_REPORTS'] := GROUP_BOX_NO;
      DM_PAYROLL.PR['COMBINE_RUNS'] := GROUP_BOX_YES;
      DM_PAYROLL.PR['PAYROLL_COMMENTS'] := 'MOVE WAGES';
      DM_PAYROLL.PR['SAME_DAY_PULL_AND_REPLACE'] := GROUP_BOX_NO;

      ctx_PayrollCalculation.AttachPayrollToCalendar(DM_PAYROLL.PR);
      DM_PAYROLL.PR.Post;

      DM_PAYROLL.PR_BATCH.Append;

      DM_PAYROLL.PR_BATCH['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];

      DM_PAYROLL.PR_BATCH['PERIOD_BEGIN_DATE_STATUS'] := DATE_STATUS_IGNORE;
      DM_PAYROLL.PR_BATCH['PERIOD_END_DATE_STATUS'] := DATE_STATUS_IGNORE;
      DM_PAYROLL.PR_BATCH['PAY_SALARY'] := GROUP_BOX_NO;
      DM_PAYROLL.PR_BATCH['PAY_STANDARD_HOURS'] := GROUP_BOX_NO;
      DM_PAYROLL.PR_BATCH['LOAD_DBDT_DEFAULTS'] := GROUP_BOX_NO;
      DM_PAYROLL.PR_BATCH['EXCLUDE_TIME_OFF'] := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;

      DM_PAYROLL.PR_BATCH['FREQUENCY'] := PayFrequency;
      DM_PAYROLL.PR_BATCH['PERIOD_BEGIN_DATE'] := PeriodBeginDate;
      DM_PAYROLL.PR_BATCH['PERIOD_END_DATE'] := PeriodEndDate;
      DM_PAYROLL.PR_BATCH.Post;

      DM_EMPLOYEE.EE.Filtered := False;
      DM_EMPLOYEE.EE.Filter := EEFilter;
      DM_EMPLOYEE.EE.Filtered := True;

    end;

    DM_PAYROLL.PR_CHECK_LINES.BeforePost := nil;
    DM_PAYROLL.PR_CHECK.Append;

    if not evCbNewPayroll.Checked then
    begin
      Q := TevQuery.Create('GenericSelectCurrentNBRWithCondition');
      Q.Macro['Columns'] := 'max(PAYMENT_SERIAL_NUMBER)';
      Q.Macro['TableName'] := 'PR_CHECK';
      Q.Macro['NbrField'] := 'PR';
      Q.Macro['Condition'] := 'PAYMENT_SERIAL_NUMBER<100000001';
      Q.Param['RecordNbr'] := DM_PAYROLL.PR['PR_NBR'];
      DM_PAYROLL.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value := Q.Result.Fields[0].AsInteger + 1;
    end
    else
      DM_PAYROLL.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').AsString := StrNbr;


    DM_PAYROLL.PR_CHECK['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
    DM_PAYROLL.PR_CHECK['PR_BATCH_NBR'] := DM_PAYROLL.PR_BATCH['PR_BATCH_NBR'];
    DM_PAYROLL.PR_CHECK['EE_NBR'] := StrToInt(wwDBGrid4.LookupValue);
    DM_PAYROLL.PR_CHECK['CHECK_TYPE'] := CHECK_TYPE2_MANUAL;
    DM_PAYROLL.PR_CHECK['UPDATE_BALANCE'] := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK['OR_CHECK_FEDERAL_TYPE'] := 'N';
    DM_PAYROLL.PR_CHECK['EXCLUDE_FEDERAL'] := 'N';
    DM_PAYROLL.PR_CHECK['EXCLUDE_ADDITIONAL_FEDERAL'] := EXCLUDE_TAXES_BOTH;;
    DM_PAYROLL.PR_CHECK['EXCLUDE_EMPLOYEE_OASDI'] := 'N';
    DM_PAYROLL.PR_CHECK['EXCLUDE_EMPLOYEE_MEDICARE'] := 'N';
    DM_PAYROLL.PR_CHECK['EXCLUDE_EMPLOYEE_EIC'] := 'N';
    DM_PAYROLL.PR_CHECK['TAX_AT_SUPPLEMENTAL_RATE'] := 'N';
    DM_PAYROLL.PR_CHECK['CUSTOM_PR_BANK_ACCT_NUMBER'] := ctx_PayrollCalculation.GetCustomBankAccountNumber(DM_PAYROLL.PR_CHECK);
    DM_PAYROLL.PR_CHECK['ABA_NUMBER'] := ctx_PayrollCalculation.GetABANumber;
    DM_PAYROLL.PR_CHECK['EXCLUDE_DD'] := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK['EXCLUDE_DD_EXCEPT_NET'] := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK['EXCLUDE_TIME_OFF_ACCURAL'] := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
    DM_PAYROLL.PR_CHECK['EXCLUDE_AUTO_DISTRIBUTION'] := GROUP_BOX_YES;
    DM_PAYROLL.PR_CHECK['EXCLUDE_ALL_SCHED_E_D_CODES'] := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK['EXCLUDE_SCH_E_D_FROM_AGCY_CHK'] := GROUP_BOX_YES;
    DM_PAYROLL.PR_CHECK['EXCLUDE_SCH_E_D_EXCEPT_PENSION'] := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK['PRORATE_SCHEDULED_E_DS'] := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK['CHECK_STATUS'] := CHECK_STATUS_OUTSTANDING;
    DM_PAYROLL.PR_CHECK['STATUS_CHANGE_DATE'] := Now;
    DM_PAYROLL.PR_CHECK['EXCLUDE_FROM_AGENCY'] := GROUP_BOX_YES;
    DM_PAYROLL.PR_CHECK['ER_OASDI_TAXABLE_TIPS'] := 0.0;
    DM_PAYROLL.PR_CHECK['FEDERAL_SHORTFALL'] := 0.0;
    DM_PAYROLL.PR_CHECK['CHECK_TYPE_945'] := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK['TAX_FREQUENCY'] := DM_EMPLOYEE.EE['PAY_FREQUENCY'];
    DM_PAYROLL.PR_CHECK['CALCULATE_OVERRIDE_TAXES'] := 'N';

    DM_PAYROLL.PR_CHECK.Post;

    DM_PAYROLL.PR_CHECK_LINES.DataRequired(AlwaysFalseCond);
    DM_PAYROLL.PR_CHECK_STATES.DataRequired('ALL');

    Cond := '(select distinct EE_STATES_NBR, NEW_EE_STATES_NBR from (select 0 EE_STATES_NBR,0 NEW_EE_STATES_NBR from RDB$DATABASE';
    for i := 0 to High(aStates) do
      Cond := Cond + ' union all select '+ IntToStr(aStates[i].OldNbr)+' EE_STATES_NBR,'+ IntToStr(aStates[i].NewNbr) + ' NEW_EE_STATES_NBR from RDB$DATABASE';
    Cond := Cond + ') where EE_STATES_NBR<>0)';

    Cond1 := '(select distinct EE_SUI_STATES_NBR, NEW_EE_SUI_STATES_NBR from (select 0 EE_SUI_STATES_NBR,0 NEW_EE_SUI_STATES_NBR from RDB$DATABASE';
    for i := 0 to High(aSUI) do
      Cond1 := Cond1 + ' union all select '+ IntToStr(aSUI[i].OldNbr)+' EE_SUI_STATES_NBR,'+ IntToStr(aSUI[i].NewNbr)+' NEW_EE_SUI_STATES_NBR from RDB$DATABASE';
    Cond1 := Cond1 + ') where EE_SUI_STATES_NBR <> 0)';

    s := '/*CL_*/'+
      ' select CL_E_DS_NBR, EE_SCHEDULED_E_DS_NBR, EE_STATES_NBR, EE_SUI_STATES_NBR, NEW_EE_STATES_NBR, NEW_EE_SUI_STATES_NBR, sum(AMOUNT) AMOUNT from (' +
      ' select x.PR_CHECK_LINES_NBR, x.EE_SCHEDULED_E_DS_NBR, x.CL_E_DS_NBR, x.EE_STATES_NBR, x.EE_SUI_STATES_NBR, t.NEW_EE_STATES_NBR, s.NEW_EE_SUI_STATES_NBR, x.AMOUNT ' +
      ' from PR_CHECK_LINES x ' +
      ' join PR_CHECK c on c.PR_CHECK_NBR = x.PR_CHECK_NBR ' +
      ' join '+Cond+' t on t.EE_STATES_NBR=x.EE_STATES_NBR ' +
      ' join '+Cond1+' s on s.EE_SUI_STATES_NBR=x.EE_SUI_STATES_NBR ' +
      ' where c.EE_NBR = '+wwDBGrid4.LookupValue + ' and #CONDITION ' +
      ' and x.AMOUNT is not null' +
      ' and {AsOfNow<x>} and {AsOfNow<c>} ' +
      ' union all '+
      ' select x.PR_CHECK_LINES_NBR, x.EE_SCHEDULED_E_DS_NBR, x.CL_E_DS_NBR, x.EE_STATES_NBR, x.EE_SUI_STATES_NBR, t.NEW_EE_STATES_NBR, x.EE_SUI_STATES_NBR NEW_EE_SUI_STATES_NBR, x.AMOUNT ' +
      ' from PR_CHECK_LINES x ' +
      ' join PR_CHECK c on c.PR_CHECK_NBR = x.PR_CHECK_NBR ' +
      ' join '+Cond+' t on t.EE_STATES_NBR=x.EE_STATES_NBR ' +
      ' where c.EE_NBR = '+wwDBGrid4.LookupValue + ' and #CONDITION ' +
      ' and x.EE_SUI_STATES_NBR not in (select EE_SUI_STATES_NBR from '+Cond1+') ' +
      ' and x.AMOUNT is not null' +
      ' and {AsOfNow<x>} and {AsOfNow<c>} ' +
      ' union all '+
      ' select x.PR_CHECK_LINES_NBR, x.EE_SCHEDULED_E_DS_NBR, x.CL_E_DS_NBR, x.EE_STATES_NBR, x.EE_SUI_STATES_NBR, x.EE_STATES_NBR NEW_EE_STATES_NBR, s.NEW_EE_SUI_STATES_NBR, x.AMOUNT ' +
      ' from PR_CHECK_LINES x ' +
      ' join PR_CHECK c on c.PR_CHECK_NBR = x.PR_CHECK_NBR ' +
      ' join '+Cond1+' s on s.EE_SUI_STATES_NBR=x.EE_SUI_STATES_NBR ' +
      ' where c.EE_NBR = '+wwDBGrid4.LookupValue + ' and #CONDITION ' +
      ' and x.EE_STATES_NBR not in (select EE_STATES_NBR from '+Cond+') ' +
      ' and x.AMOUNT is not null ' +
      ' and {AsOfNow<x>} and {AsOfNow<c>} '+
      ') group by 1,2,3,4,5,6 order by 1,2,3,4,5,6 ';

    Q := TevQuery.Create(s);
    Q.Macro['Condition'] := CreatePrNbrList;

    Q.Result.First;
    while not Q.Result.Eof do
    begin
      DM_PAYROLL.PR_CHECK_LINES.Append;
      DM_PAYROLL.PR_CHECK_LINES['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := -Q.Result['AMOUNT'];
      DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'] := Q.Result['CL_E_DS_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := Q.Result['EE_SCHEDULED_E_DS_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['EE_STATES_NBR'] := Q.Result['EE_STATES_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['EE_SUI_STATES_NBR'] := Q.Result['EE_SUI_STATES_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
      DM_PAYROLL.PR_CHECK_LINES.Post;

      DM_PAYROLL.PR_CHECK_LINES.Append;
      DM_PAYROLL.PR_CHECK_LINES['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := Q.Result['AMOUNT'];
      DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'] := Q.Result['CL_E_DS_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := Q.Result['EE_SCHEDULED_E_DS_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['EE_STATES_NBR'] := Q.Result['NEW_EE_STATES_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['EE_SUI_STATES_NBR'] := Q.Result['NEW_EE_SUI_STATES_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
      DM_PAYROLL.PR_CHECK_LINES.Post;
      Q.Result.Next;
    end;

    for i := 0 to High(aStates) do
    begin
      Q := TevQuery.Create(' select sum(-x.STATE_TAX) STATE_TAX ' +
       ' from PR_CHECK_STATES x ' +
       ' join PR_CHECK c on c.PR_CHECK_NBR = x.PR_CHECK_NBR ' +
       ' where c.EE_NBR = '+wwDBGrid4.LookupValue +
       ' and x.EE_STATES_NBR = ' + IntToStr(aStates[i].OldNbr) + ' and x.STATE_TAX is not null and ' +
       ' {AsOfNow<x>} and {AsOfNow<c>} and ' +
       '  #CONDITION  ');
       
      Q.Macro['Condition'] := CreatePrNbrList;

      if not Q.Result.FieldByName('STATE_TAX').IsNull then
      begin
        DM_PAYROLL.PR_CHECK_STATES.Append;
        DM_PAYROLL.PR_CHECK_STATES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
        DM_PAYROLL.PR_CHECK_STATES['EE_STATES_NBR'] := aStates[i].OldNbr;
        DM_PAYROLL.PR_CHECK_STATES['TAX_AT_SUPPLEMENTAL_RATE'] := GROUP_BOX_NO;
        DM_PAYROLL.PR_CHECK_STATES['EXCLUDE_STATE'] := GROUP_BOX_NO;
        DM_PAYROLL.PR_CHECK_STATES['EXCLUDE_SDI'] := GROUP_BOX_NO;
        DM_PAYROLL.PR_CHECK_STATES['EXCLUDE_SUI'] := GROUP_BOX_NO;
        DM_PAYROLL.PR_CHECK_STATES['EXCLUDE_ADDITIONAL_STATE'] := EXCLUDE_TAXES_BOTH;
        DM_PAYROLL.PR_CHECK_STATES['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
        DM_PAYROLL.PR_CHECK_STATES['STATE_OVERRIDE_VALUE'] := Q.Result['STATE_TAX'];
        DM_PAYROLL.PR_CHECK_STATES['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
        DM_PAYROLL.PR_CHECK_STATES.Post;
      end;
    end;

    DM_PAYROLL.PR_CHECK_LINE_LOCALS.DataRequired(AlwaysFalseCond);
    DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('ALL');

    Tmp_Pr_Check_Lines_Nbr := TisListOfValues.Create;
    tmpPr_Check_nbr := 0;
    s := 'select sum(amount), cl_e_ds_nbr, EE_SCHEDULED_E_DS_NBR,  t2.PR_CHECK_NBR, T1.PR_CHECK_LINES_NBR ' +
                'from pr_check T2, pr_check_lines T1, PR_CHECK_LOCALS T3 ' +
                  'where T2.ee_NBR = :RecordNbr and  ' +
                   'T1.pr_check_NBR = T2.pr_check_NBR and T2.PR_CHECK_NBR = T3.PR_CHECK_NBR AND ' +
                   '{AsOfNow<T1>} and {AsOfNow<T2>} and {AsOfNow<T3>} and ' +
                   ' #CONDITION  ' +
                   'group by cl_e_ds_nbr, EE_SCHEDULED_E_DS_NBR, t2.PR_CHECK_NBR, T1.PR_CHECK_LINES_NBR ' +
                   'order by t2.PR_CHECK_NBR ';

    for i := 0 to High(aLocals) do
    begin
      with TExecDSWrapper.Create(s) do
      begin
        SetParam('RecordNbr', StrToInt(wwDBGrid4.LookupValue));
        SetMacro('Condition', CreateInStatement(wwDBGrid6, 'pr_nbr') + ' and t3.EE_LOCALS_NBR= ' + IntToStr(aLocals[i].OldNbr));
        ctx_DataAccess.CUSTOM_VIEW.Close;
        ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.CUSTOM_VIEW.Open;
      end;

      while not ctx_DataAccess.CUSTOM_VIEW.Eof do
      begin
        DM_PAYROLL.PR_CHECK_LINES.Append;
        if not ctx_DataAccess.CUSTOM_VIEW.Fields[0].IsNull then
          DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := -ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value;
        DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'] := ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value;
        DM_PAYROLL.PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := ctx_DataAccess.CUSTOM_VIEW.Fields[2].Value;
        DM_PAYROLL.PR_CHECK_LINES['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
        DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
        DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
        DM_PAYROLL.PR_CHECK_LINES.Post;

        if DM_PAYROLL.PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value , aLocals[i].OldNbr]), []) then
         Begin

          TaxAmount := DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value;

           if DM_PAYROLL.PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'] , aLocals[i].OldNbr]), []) then
             Begin
              if tmpPr_Check_nbr <> ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value then
              begin
                tmpPr_Check_nbr := ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value;
                DM_PAYROLL.PR_CHECK_LOCALS.Edit;
                DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] - TaxAmount;
                DM_PAYROLL.PR_CHECK_LOCALS.Post;
              end
             end
            else
             begin
              tmpPr_Check_nbr := ctx_DataAccess.CUSTOM_VIEW.Fields[3].Value;
              DM_PAYROLL.PR_CHECK_LOCALS.Append;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').Value := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
              DM_PAYROLL.PR_CHECK_LOCALS['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value := aLocals[i].OldNbr;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value := 0;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value := 0;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').Value := 0;
              DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EXCLUDE_LOCAL').Value := 'N';
              DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := -TaxAmount;
              DM_PAYROLL.PR_CHECK_LOCALS.Post;
             end;
         end;

        DM_EMPLOYEE.EE_LOCALS.First;
        while not DM_EMPLOYEE.EE_LOCALS.Eof do
        begin
          DM_PAYROLL.PR_CHECK_LINE_LOCALS.Append;
          DM_PAYROLL.PR_CHECK_LINE_LOCALS['PR_CHECK_LINES_NBR'] := DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
          DM_PAYROLL.PR_CHECK_LINE_LOCALS['EE_LOCALS_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'];
          if DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'] = aLocals[i].OldNbr then
            DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_INCLUDE
          else
            DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_EXEMPT;
          DM_PAYROLL.PR_CHECK_LINE_LOCALS.Post;
          DM_EMPLOYEE.EE_LOCALS.Next;
        end;

        if (not Tmp_Pr_Check_Lines_Nbr.ValueExists(ctx_DataAccess.CUSTOM_VIEW.Fields[4].AsString)) or
            ((Tmp_Pr_Check_Lines_Nbr.ValueExists(ctx_DataAccess.CUSTOM_VIEW.Fields[4].AsString) and
            (Tmp_Pr_Check_Lines_Nbr.Value[ctx_DataAccess.CUSTOM_VIEW.Fields[4].AsString] <> aLocals[i].NewNbr))) then
        begin
          Tmp_Pr_Check_Lines_Nbr.AddValue(ctx_DataAccess.CUSTOM_VIEW.Fields[4].AsString, aLocals[i].NewNbr);
          DM_PAYROLL.PR_CHECK_LINES.Append;
          DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := ctx_DataAccess.CUSTOM_VIEW.Fields[0].Value;
          DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'] := ctx_DataAccess.CUSTOM_VIEW.Fields[1].Value;
          DM_PAYROLL.PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := ctx_DataAccess.CUSTOM_VIEW.Fields[2].Value;
          DM_PAYROLL.PR_CHECK_LINES['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
          DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
          DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
          DM_PAYROLL.PR_CHECK_LINES.Post;
        end;

        DM_EMPLOYEE.EE_LOCALS.First;
        while not DM_EMPLOYEE.EE_LOCALS.Eof do
        begin
          DM_PAYROLL.PR_CHECK_LINE_LOCALS.Append;
          DM_PAYROLL.PR_CHECK_LINE_LOCALS['PR_CHECK_LINES_NBR'] := DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
          DM_PAYROLL.PR_CHECK_LINE_LOCALS['EE_LOCALS_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'];
          if DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'] = aLocals[i].NewNbr then
            DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_INCLUDE
          else
            DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_EXEMPT;
          DM_PAYROLL.PR_CHECK_LINE_LOCALS.Post;
          DM_EMPLOYEE.EE_LOCALS.Next;
        end;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;
    end;
    if DM_PAYROLL.PR_CHECK_LINES.ChangeCount > 0 then
      if not evCbNewPayroll.Checked then
        ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_LINE_LOCALS, DM_PAYROLL.PR_CHECK_STATES,DM_PAYROLL.PR_CHECK_LOCALS])
      else
        ctx_DataAccess.PostDataSets([DM_PAYROLL.PR, DM_PAYROLL.PR_BATCH, DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_LINE_LOCALS, DM_PAYROLL.PR_CHECK_STATES,DM_PAYROLL.PR_CHECK_LOCALS])
    else
      DM_PAYROLL.PR_CHECK.CancelUpdates;
  finally
    DM_PAYROLL.PR_CHECK_LINES.BeforePost := bp;
    ctx_DataAccess.TempCommitDisable := bTemp;
    ctx_DataAccess.LockPRSimple;
  end;
  Close;
end;

function TReattachStatesLocals.ConvertToString(v: Variant): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to VarArrayHighBound(v, 1) do
    Result := Result + VarToStr(v[i]) + '-';
  Delete(Result, Length(Result), 1);
end;

procedure TReattachStatesLocals.cdsSUICalcFields(DataSet: TDataSet);
begin
  DataSet['State'] := ConvertToString(DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', DataSet['EE_STATES_NBR'], 'State_Lookup;SDI_State_Lookup;SUI_State_Lookup'));
  DataSet['Dest_State'] := ConvertToString(DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', DataSet['DEST_EE_STATES_NBR'], 'State_Lookup;SDI_State_Lookup;SUI_State_Lookup'));
end;

procedure TReattachStatesLocals.FormHide(Sender: TObject);
begin
  DM_EMPLOYEE.EE_STATES.ClearAsOfDateOverride;
  DM_EMPLOYEE.EE_LOCALS.ClearAsOfDateOverride;
end;

procedure TReattachStatesLocals.cbYearChange(Sender: TObject);
begin
  if bYearChange then
  begin
    cdsPayrolls.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString + ' and STATUS=''' + PAYROLL_STATUS_PROCESSED +
                                      ''' and ExtractYear(CHECK_DATE)=' + cbYear.Text);
    ctx_DataAccess.OpenDataSets(cdsPayrolls);
  end;
end;

procedure TReattachStatesLocals.wwDBGrid4BeforeDropDown(Sender: TObject);
begin
   dsEE_STATES.MasterDataSource := nil;
   dsEE_LOCALS.MasterDataSource := nil;

   DM_EMPLOYEE.EE.Filter := 'CO_NBR ='+ DM_CLIENT.CO.CO_NBR.AsString;
   DM_EMPLOYEE.EE.Filtered := true;
end;

procedure TReattachStatesLocals.wwDBGrid4CloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
var
 bm :TBookmark;
begin
    bm :=DM_EMPLOYEE.EE.GetBookmark;
    DM_EMPLOYEE.EE.Filtered := false;
    DM_EMPLOYEE.EE.Filtered := false;
    DM_EMPLOYEE.EE.GotoBookmark(bm);
    DM_EMPLOYEE.EE.FreeBookmark(bm);

    dsEE_STATES.MasterDataSource := dsEmployees;
    dsEE_LOCALS.MasterDataSource := dsEmployees;
end;



procedure TReattachStatesLocals.evCbNewPayrollClick(Sender: TObject);
begin
   if evCbNewPayroll.Checked then   evSpeedButton2.Caption :='Create New Payroll && Checks'
    else   evSpeedButton2.Caption :='Create Checks'
end;

end.
