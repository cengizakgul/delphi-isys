// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SAuxPayrollScr;

interface

uses
  SysUtils, SPackageEntry, ComCtrls,  StdCtrls, Graphics,
  Controls, Buttons, Classes, ExtCtrls, SDataStructure, EvTypes,
  Menus, ImgList, ToolWin, ActnList, EvUtils, ISBasicClasses,
  EvCommonInterfaces, EvContext, EvMainBoard, EvExceptions,
  EvUIComponents, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TPayrollExtFrm = class(TFramePackageTmpl, IevPayrollExtScreens)
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  ActivatePackage(WorkPlace: TWinControl): Boolean; override;
    function  PackageBitmap: TBitmap; override;
  end;

implementation

var
  PayrollExtFrm: TPayrollExtFrm;


{$R *.DFM}

function GetPayrollExtScreens: IevPayrollExtScreens;
begin
  if not Assigned(PayrollExtFrm) then
    PayrollExtFrm := TPayrollExtFrm.Create(nil);
  Result := PayrollExtFrm;
end;

{ TSystemFrm }

function TPayrollExtFrm.ActivatePackage(WorkPlace: TWinControl): Boolean;
begin
  Result := inherited ActivatePackage(WorkPlace);
  if Result then
  begin
    DM_TEMPORARY.TMP_CL.Activate;
    if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
      raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);
  end;
end;

function TPayrollExtFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TPayrollExtFrm.PackageCaption: string;
begin
  Result := 'Aux &Payroll';
end;

function TPayrollExtFrm.PackageSortPosition: string;
begin
  Result := '100';
end;

procedure TPayrollExtFrm.UserPackageStructure;
begin
  AddStructure('Payroll|010', 'TEDIT_PR');
  AddStructure('E/D Excludes|020', 'TEDIT_PR_SCHEDULED_E_DS');
  AddStructure('Batch|030', 'TEDIT_PR_BATCH');
  AddStructure('Check|040', 'TEDIT_PR_CHECK');
  AddStructure('Lines (Complex)|050', 'TEDIT_PR_CHECK_LINES');
  AddStructure('Lines (Simple)|060', 'TEDIT_PR_CHECK_LINES_SIMPLE');
  AddStructure('Local Excludes|070', 'TEDIT_PR_CHECK_LINE_LOCALS');
  AddStructure('States|080', 'TEDIT_PR_CHECK_STATES');
  AddStructure('Locals|090', 'TEDIT_PR_CHECK_LOCALS');
  AddStructure('SUI|110', 'TEDIT_PR_CHECK_SUI');
  AddStructure('Misc. Check|120', 'TEDIT_PR_AGENCY_CHECK');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetPayrollExtScreens, IevPayrollExtScreens, 'Screens Payroll Ext');

finalization
  FreeAndNil(PayrollExtFrm);

end.
