unit SPAMoveOneDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ISBasicClasses, ExtCtrls,
  SPD_DBDTSelectionListFiltr, SDataStructure, EvExceptions, EvUtils, EvDataset,
  EvCommonInterfaces, isDataset, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIEdit;

type
  TMoveOneDlg = class(TForm)
    evLabel1: TevLabel;
    AmountEdit: TevEdit;
    evLabel2: TevLabel;
    cbNR: TevComboBox;
    evLabel3: TevLabel;
    cbRE: TevComboBox;
    evLabel4: TevLabel;
    cbSC: TevComboBox;
    evButton1: TevButton;
    evButton2: TevButton;
    EvBevel1: TEvBevel;
    evLabel5: TevLabel;
    HoursEdit: TevEdit;
    Button1: TButton;
    DbdtLabel: TLabel;
    Label16: TevLabel;
    cbJob: TevComboBox;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Button1Click(Sender: TObject);
  private
    procedure SelectDbdt;
  public
    MaxAmount: Double;
    MaxHours: Double;
    Division: Variant;
    Branch: Variant;
    Department: Variant;
    Team: Variant;
    SQLite: ISQLite;
  end;

implementation

{$R *.dfm}

procedure TMoveOneDlg.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrOk then
  begin
    if Abs(StrToFloat(AmountEdit.Text)) < 0.005 then
      raise Exception.Create('Please enter the taxable wages amount to move.');
    if (Abs(MaxAmount) - Abs(StrToFloat(AmountEdit.Text))) < -0.005 then
      raise Exception.Create('Amount should equal '+FloatToStr(Abs(MaxAmount)) + ' or less.');
  end;
end;

procedure TMoveOneDlg.SelectDbdt;
var
  DBDTSelectionList: TDBDTSelectionListFiltr;
  X: IevDataset;
begin
  inherited;
  if DM_EMPLOYEE.EE_RATES.Active and
     DM_EMPLOYEE.EE_RATES.Locate('EE_NBR;RATE_NUMBER',
        VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'], DM_PAYROLL.PR_CHECK_LINES['RATE_NUMBER']]), []) and
     (not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DIVISION_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_DIVISION_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DIVISION_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_DIVISION_NBR').Value) or
      not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_BRANCH_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_BRANCH_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_BRANCH_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_BRANCH_NBR').Value) or
      not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DEPARTMENT_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_DEPARTMENT_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_DEPARTMENT_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_DEPARTMENT_NBR').Value) or
      not DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_TEAM_NBR').IsNull and
      not DM_EMPLOYEE.EE_RATES.FieldByname('CO_TEAM_NBR').IsNull and
      (DM_PAYROLL.PR_CHECK_LINES.FieldByname('CO_TEAM_NBR').Value <> DM_EMPLOYEE.EE_RATES.FieldByname('CO_TEAM_NBR').Value)) then
    raise ECanNotPerformOperation.CreateHelp('This rate has DBDT override on EE rates level. Can not override on check line level', IDH_CanNotPerformOperation);

  DBDTSelectionList := TDBDTSelectionListFiltr.Create(nil);
  try
    with DM_COMPANY do
    begin
      CO_DIVISION.DataRequired('CO_NBR=' + CO.FieldByName('CO_NBR').AsString);
      DBDTSelectionList.Setup(TevClientDataSet(CO_DIVISION), TevClientDataSet(CO_BRANCH),
        TevClientDataSet(CO_DEPARTMENT), TevClientDataSet(CO_TEAM),
        DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value
        , DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value, DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value
        , DM_PAYROLL.PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionList.ShowModal = mrOK then
      begin
        Division := DBDTSelectionList.wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
        Branch := DBDTSelectionList.wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
        Department := DBDTSelectionList.wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
        Team := DBDTSelectionList.wwcsTempDBDT.FieldByName('TEAM_NBR').Value;

        DbdtLabel.Caption := '';
        if ConvertNull(Division,0) <> 0 then
        begin
          X := SQLite.Select('select CUSTOM_DIVISION_NUMBER from CO_DIVISION where CO_DIVISION_NBR=:NBR',
            [DBDTSelectionList.wwcsTempDBDT.FieldByName('DIVISION_NBR').Value]);
          DbdtLabel.Caption := X.Fields[0].AsString;
          if DBDTSelectionList.wwcsTempDBDT.FieldByName('BRANCH_NBR').AsInteger <> 0 then
          begin
            X := SQLite.Select('select CUSTOM_BRANCH_NUMBER from CO_BRANCH where CO_BRANCH_NBR=:NBR',
              [DBDTSelectionList.wwcsTempDBDT.FieldByName('BRANCH_NBR').Value]);
            DbdtLabel.Caption := DbdtLabel.Caption + '/' + X.Fields[0].AsString;
          end;
          if DBDTSelectionList.wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').AsInteger <> 0 then
          begin
            X := SQLite.Select('select CUSTOM_DEPARTMENT_NUMBER from CO_DEPARTMENT where CO_DEPARTMENT_NBR=:NBR',
              [DBDTSelectionList.wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value]);
            DbdtLabel.Caption := DbdtLabel.Caption + '/' + X.Fields[0].AsString;
          end;
          if DBDTSelectionList.wwcsTempDBDT.FieldByName('TEAM_NBR').AsInteger <> 0 then
          begin
            X := SQLite.Select('select CUSTOM_TEAM_NUMBER from CO_TEAM where CO_TEAM_NBR=:NBR',
              [DBDTSelectionList.wwcsTempDBDT.FieldByName('TEAM_NBR').Value]);
            DbdtLabel.Caption := DbdtLabel.Caption + '/' + X.Fields[0].AsString;
          end;
        end;

      end;
    end;
  finally
    DBDTSelectionList.Free;
  end;
end;

procedure TMoveOneDlg.Button1Click(Sender: TObject);
begin
  SelectDbdt;
end;

end.
