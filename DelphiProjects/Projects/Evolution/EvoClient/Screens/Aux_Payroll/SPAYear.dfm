object YearDlg: TYearDlg
  Left = 483
  Top = 327
  Width = 213
  Height = 165
  Caption = 'Select Year'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel2: TevLabel
    Left = 23
    Top = 35
    Width = 22
    Height = 13
    Caption = 'Year'
  end
  object evButton1: TevButton
    Left = 16
    Top = 96
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    Margin = 0
  end
  object evButton2: TevButton
    Left = 104
    Top = 96
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    Margin = 0
  end
  object cbYear: TevComboBox
    Left = 65
    Top = 32
    Width = 112
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
end
