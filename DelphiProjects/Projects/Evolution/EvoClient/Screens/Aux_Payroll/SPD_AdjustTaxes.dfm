object AdjustTaxes: TAdjustTaxes
  Left = 798
  Top = 254
  Width = 831
  Height = 502
  BorderIcons = []
  Caption = 'Tax Adjustment Form'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TevPanel
    Left = 0
    Top = 432
    Width = 823
    Height = 43
    Align = alBottom
    TabOrder = 0
    object bbtnAdjust: TevBitBtn
      Left = 232
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Adjust'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = bbtnAdjustClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
        DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
        DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
        DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
        DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
        DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
        2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
        A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
        AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
      NumGlyphs = 2
      ParentColor = False
      Margin = 0
    end
    object bbtnCancel: TevBitBtn
      Left = 320
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
      OnClick = bbtnCancelClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
        DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
        9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
        DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
        DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
        DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
        91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
        999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
      NumGlyphs = 2
      ParentColor = False
      Margin = 0
    end
    object btnAdjustNextEE: TevBitBtn
      Left = 96
      Top = 8
      Width = 121
      Height = 25
      Caption = 'Adjust/Next EE'
      Default = True
      TabOrder = 2
      OnClick = btnAdjustNextEEClick
      Color = clBlack
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
        DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
        DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
        DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
        DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
        DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
        2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
        A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
        AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
      NumGlyphs = 2
      ParentColor = False
      Margin = 0
    end
  end
  object Panel2: TevPanel
    Left = 0
    Top = 33
    Width = 823
    Height = 399
    Align = alClient
    TabOrder = 1
    object PageControl1: TevPageControl
      Left = 1
      Top = 1
      Width = 821
      Height = 397
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Federal'
        object lablTaxableWages: TevLabel
          Left = 80
          Top = 8
          Width = 89
          Height = 13
          Caption = 'Taxable Wages'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lablTax: TevLabel
          Left = 304
          Top = 8
          Width = 22
          Height = 13
          Caption = 'Tax'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lablFederal: TevLabel
          Left = 8
          Top = 32
          Width = 35
          Height = 13
          Caption = 'Federal'
        end
        object lablEE_oasdi: TevLabel
          Left = 8
          Top = 56
          Width = 50
          Height = 13
          Caption = 'EE OASDI'
        end
        object lablER_oasdi: TevLabel
          Left = 8
          Top = 152
          Width = 51
          Height = 13
          Caption = 'ER OASDI'
        end
        object lablEE_Medicare: TevLabel
          Left = 8
          Top = 80
          Width = 61
          Height = 13
          Caption = 'EE Medicare'
        end
        object lablER_MedicareTW: TevLabel
          Left = 8
          Top = 176
          Width = 62
          Height = 13
          Caption = 'ER Medicare'
        end
        object lablEIC: TevLabel
          Left = 8
          Top = 104
          Width = 17
          Height = 13
          Caption = 'EIC'
        end
        object lablBackupWith: TevLabel
          Left = 8
          Top = 128
          Width = 65
          Height = 13
          Caption = 'Backup With.'
        end
        object lablFUI: TevLabel
          Left = 8
          Top = 200
          Width = 17
          Height = 13
          Caption = 'FUI'
        end
        object lablTaxableTips: TevLabel
          Left = 192
          Top = 40
          Width = 61
          Height = 13
          Caption = 'Taxable Tips'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel1: TevLabel
          Left = 192
          Top = 136
          Width = 61
          Height = 13
          Caption = 'Taxable Tips'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel2: TevLabel
          Left = 192
          Top = 184
          Width = 64
          Height = 13
          Caption = 'Gross Wages'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dedtFederalTW: TevDBEdit
          Left = 80
          Top = 32
          Width = 105
          Height = 21
          DataField = 'FEDERAL_TAXABLE_WAGES'
          DataSource = wwdsFederal
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtFederalT: TevDBEdit
          Left = 304
          Top = 32
          Width = 89
          Height = 21
          DataField = 'FEDERAL_TAX'
          DataSource = wwdsFederal
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtEE_oasdiTW: TevDBEdit
          Left = 80
          Top = 56
          Width = 105
          Height = 21
          DataField = 'EE_OASDI_TAXABLE_WAGES'
          DataSource = wwdsFederal
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtEE_oasdiT: TevDBEdit
          Left = 304
          Top = 56
          Width = 89
          Height = 21
          DataField = 'EE_OASDI_TAX'
          DataSource = wwdsFederal
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtEE_MedicareTW: TevDBEdit
          Left = 80
          Top = 80
          Width = 105
          Height = 21
          DataField = 'EE_MEDICARE_TAXABLE_WAGES'
          DataSource = wwdsFederal
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtEE_MedicareT: TevDBEdit
          Left = 304
          Top = 80
          Width = 89
          Height = 21
          DataField = 'EE_MEDICARE_TAX'
          DataSource = wwdsFederal
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtEICT: TevDBEdit
          Left = 304
          Top = 104
          Width = 89
          Height = 21
          DataField = 'EE_EIC_TAX'
          DataSource = wwdsFederal
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtBackupWithT: TevDBEdit
          Left = 304
          Top = 128
          Width = 89
          Height = 21
          DataField = 'OR_CHECK_BACK_UP_WITHHOLDING'
          DataSource = wwdsFederal
          TabOrder = 8
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtER_oasdiTW: TevDBEdit
          Left = 80
          Top = 152
          Width = 105
          Height = 21
          DataField = 'ER_OASDI_TAXABLE_WAGES'
          DataSource = wwdsFederal
          TabOrder = 9
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtER_oasdiT: TevDBEdit
          Left = 304
          Top = 152
          Width = 89
          Height = 21
          DataField = 'ER_OASDI_TAX'
          DataSource = wwdsFederal
          TabOrder = 11
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtER_MedicareTW: TevDBEdit
          Left = 80
          Top = 176
          Width = 105
          Height = 21
          DataField = 'ER_MEDICARE_TAXABLE_WAGES'
          DataSource = wwdsFederal
          TabOrder = 12
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtER_MedicareT: TevDBEdit
          Left = 304
          Top = 176
          Width = 89
          Height = 21
          DataField = 'ER_MEDICARE_TAX'
          DataSource = wwdsFederal
          TabOrder = 13
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtFUITW: TevDBEdit
          Left = 80
          Top = 200
          Width = 105
          Height = 21
          DataField = 'ER_FUI_TAXABLE_WAGES'
          DataSource = wwdsFederal
          TabOrder = 14
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtFUIT: TevDBEdit
          Left = 304
          Top = 200
          Width = 89
          Height = 21
          DataField = 'ER_FUI_TAX'
          DataSource = wwdsFederal
          TabOrder = 15
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtEE_oasdiTT: TevDBEdit
          Left = 192
          Top = 56
          Width = 105
          Height = 21
          DataField = 'EE_OASDI_TAXABLE_TIPS'
          DataSource = wwdsFederal
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtER_oasdiTT: TevDBEdit
          Left = 192
          Top = 152
          Width = 105
          Height = 21
          DataField = 'ER_OASDI_TAXABLE_TIPS'
          DataSource = wwdsFederal
          TabOrder = 10
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object evDBEdit1: TevDBEdit
          Left = 192
          Top = 200
          Width = 105
          Height = 21
          DataField = 'ER_FUI_GROSS_WAGES'
          DataSource = wwdsFederal
          TabOrder = 16
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'State'
        ImageIndex = 1
        object lablStateTax: TevLabel
          Left = 192
          Top = 128
          Width = 22
          Height = 13
          Caption = 'Tax'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lablStateTaxableWages: TevLabel
          Left = 80
          Top = 128
          Width = 89
          Height = 13
          Caption = 'Taxable Wages'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lablState: TevLabel
          Left = 8
          Top = 152
          Width = 25
          Height = 13
          Caption = 'State'
        end
        object lablEE_sdi: TevLabel
          Left = 8
          Top = 176
          Width = 35
          Height = 13
          Caption = 'EE SDI'
        end
        object lablER_sdi: TevLabel
          Left = 8
          Top = 200
          Width = 36
          Height = 13
          Caption = 'ER SDI'
        end
        object wwgrState: TevDBGrid
          Left = 0
          Top = 0
          Width = 404
          Height = 120
          TabStop = False
          DisableThemesInTitle = False
          Selected.Strings = (
            'State_Lookup'#9'2'#9'State'
            'STATE_TAXABLE_WAGES'#9'10'#9'State Tax Wages'
            'STATE_TAX'#9'10'#9'State Tax'
            'EE_SDI_TAXABLE_WAGES'#9'10'#9'EE SDI Tax Wages'
            'EE_SDI_TAX'#9'10'#9'EE SDI Tax'
            'ER_SDI_TAXABLE_WAGES'#9'10'#9'ER SDI Tax Wages'
            'ER_SDI_TAX'#9'10'#9'ER SDI Tax'#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TAdjustTaxes\wwgrState'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alTop
          DataSource = wwdsState
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object dedtStateTW: TevDBEdit
          Left = 80
          Top = 152
          Width = 105
          Height = 21
          DataField = 'STATE_TAXABLE_WAGES'
          DataSource = wwdsState
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtStateT: TevDBEdit
          Left = 192
          Top = 152
          Width = 89
          Height = 21
          DataField = 'STATE_TAX'
          DataSource = wwdsState
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtEE_sdiTW: TevDBEdit
          Left = 80
          Top = 176
          Width = 105
          Height = 21
          DataField = 'EE_SDI_TAXABLE_WAGES'
          DataSource = wwdsState
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtEE_sdiT: TevDBEdit
          Left = 192
          Top = 176
          Width = 89
          Height = 21
          DataField = 'EE_SDI_TAX'
          DataSource = wwdsState
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtER_sdiTW: TevDBEdit
          Left = 80
          Top = 200
          Width = 105
          Height = 21
          DataField = 'ER_SDI_TAXABLE_WAGES'
          DataSource = wwdsState
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtER_sdiT: TevDBEdit
          Left = 192
          Top = 200
          Width = 89
          Height = 21
          DataField = 'ER_SDI_TAX'
          DataSource = wwdsState
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'SUI'
        ImageIndex = 2
        object lablSUITaxableWages: TevLabel
          Left = 8
          Top = 128
          Width = 96
          Height = 13
          Caption = 'SUI Taxable Wages'
        end
        object lablSUITax: TevLabel
          Left = 216
          Top = 128
          Width = 39
          Height = 13
          Caption = 'SUI Tax'
        end
        object evLabel4: TevLabel
          Left = 120
          Top = 128
          Width = 85
          Height = 13
          Caption = 'SUI Gross Wages'
        end
        object dedtSUITW: TevDBEdit
          Left = 8
          Top = 152
          Width = 105
          Height = 21
          DataField = 'SUI_TAXABLE_WAGES'
          DataSource = wwdsSUI
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtSUIT: TevDBEdit
          Left = 216
          Top = 152
          Width = 89
          Height = 21
          DataField = 'SUI_TAX'
          DataSource = wwdsSUI
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwgrSUI: TevDBGrid
          Left = 0
          Top = 0
          Width = 404
          Height = 120
          TabStop = False
          DisableThemesInTitle = False
          Selected.Strings = (
            'SUI_Lookup'#9'40'#9'SUI Name'
            'SUI_TAXABLE_WAGES'#9'10'#9'Taxable Wages'
            'SUI_TAX'#9'10'#9'Tax')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TAdjustTaxes\wwgrSUI'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alTop
          DataSource = wwdsSUI
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object evDBEdit2: TevDBEdit
          Left = 120
          Top = 152
          Width = 89
          Height = 21
          DataField = 'SUI_GROSS_WAGES'
          DataSource = wwdsSUI
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Local'
        ImageIndex = 3
        object lablLocalTaxableWages: TevLabel
          Left = 8
          Top = 128
          Width = 104
          Height = 13
          Caption = 'Local Taxable Wages'
        end
        object lablLocalTax: TevLabel
          Left = 120
          Top = 128
          Width = 47
          Height = 13
          Caption = 'Local Tax'
        end
        object evLabel3: TevLabel
          Left = 8
          Top = 254
          Width = 68
          Height = 13
          Caption = 'NonRes Local'
          FocusControl = NONRES_LOCAL_Combo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evLabel5: TevLabel
          Left = 8
          Top = 196
          Width = 41
          Height = 13
          Caption = 'Location'
          FocusControl = LOCATION_Combo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dedtLocalTW: TevDBEdit
          Left = 8
          Top = 152
          Width = 105
          Height = 21
          DataField = 'LOCAL_TAXABLE_WAGE'
          DataSource = wwdsLocal
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtLocalT: TevDBEdit
          Left = 120
          Top = 152
          Width = 89
          Height = 21
          DataField = 'LOCAL_TAX'
          DataSource = wwdsLocal
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwgrLocal: TevDBGrid
          Left = 0
          Top = 0
          Width = 813
          Height = 120
          TabStop = False
          DisableThemesInTitle = False
          Selected.Strings = (
            'Local_Name_Lookup'#9'40'#9'Local Name'
            'LOCAL_TAXABLE_WAGE'#9'10'#9'Taxable Wages'
            'LOCAL_TAX'#9'10'#9'Tax'
            'Location'#9'20'#9'Location'#9'F'
            'NonRes'#9'20'#9'Nonres'#9'F'
            'REPORTABLE'#9'1'#9'Reportable'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TAdjustTaxes\wwgrLocal'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alTop
          DataSource = wwdsLocal
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object NONRES_LOCAL_Combo: TevDBLookupCombo
          Left = 8
          Top = 275
          Width = 265
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup')
          DataField = 'NONRES_EE_LOCALS_NBR'
          DataSource = wwdsLocal
          LookupTable = DM_EE_LOCALS.EE_LOCALS
          LookupField = 'EE_LOCALS_NBR'
          Style = csDropDownList
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object LOCATION_Combo: TevDBLookupCombo
          Left = 8
          Top = 217
          Width = 265
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'ADDRESS1'#9'30'#9'Address1'#9'F'
            'CITY'#9'20'#9'City'#9'F')
          DataField = 'CO_LOCATIONS_NBR'
          DataSource = wwdsLocal
          LookupTable = DM_CO_LOCATIONS.CO_LOCATIONS
          LookupField = 'CO_LOCATIONS_NBR'
          Style = csDropDownList
          TabOrder = 4
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object evDBRadioGroup1: TevDBRadioGroup
          Left = 280
          Top = 144
          Width = 105
          Height = 41
          Caption = '~Reportable'
          Columns = 2
          DataField = 'REPORTABLE'
          DataSource = wwdsLocal
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 5
          Values.Strings = (
            'Y'
            'N')
        end
      end
    end
  end
  object Panel3: TevPanel
    Left = 0
    Top = 0
    Width = 823
    Height = 33
    Align = alTop
    TabOrder = 2
    object lablInfo: TevLabel
      Left = 8
      Top = 8
      Width = 174
      Height = 16
      Caption = 'All Check Info Goes Here'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object wwdsFederal: TevDataSource
    DataSet = DM_PR_CHECK.PR_CHECK
    Left = 272
    Top = 8
  end
  object wwdsState: TevDataSource
    DataSet = DM_PR_CHECK_STATES.PR_CHECK_STATES
    Left = 304
    Top = 8
  end
  object wwdsSUI: TevDataSource
    DataSet = DM_PR_CHECK_SUI.PR_CHECK_SUI
    Left = 336
    Top = 8
  end
  object wwdsLocal: TevDataSource
    DataSet = DM_PR_CHECK_LOCALS.PR_CHECK_LOCALS
    Left = 368
    Top = 8
  end
  object DM_PAYROLL: TDM_PAYROLL
    Left = 208
    Top = 8
  end
end
