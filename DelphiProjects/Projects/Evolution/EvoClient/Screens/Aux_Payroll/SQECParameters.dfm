object QECParameters: TQECParameters
  Left = 551
  Top = 232
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'QEC Parameters'
  ClientHeight = 446
  ClientWidth = 354
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lablEmployees: TevLabel
    Left = 32
    Top = 128
    Width = 152
    Height = 13
    Caption = 'EE codes separated by commas'
    Enabled = False
  end
  object lablQuarterEndDate: TevLabel
    Left = 32
    Top = 176
    Width = 80
    Height = 13
    Caption = 'Quarter end date'
  end
  object lablMinimumTax: TevLabel
    Left = 160
    Top = 176
    Width = 128
    Height = 13
    Caption = 'Minimum tax to be adjusted'
  end
  object EvBevel1: TEvBevel
    Left = 16
    Top = 16
    Width = 321
    Height = 337
    Shape = bsFrame
  end
  object evBitBtn1: TevBitBtn
    Left = 96
    Top = 408
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 10
    OnClick = evBitBtn1Click
    Color = clBlack
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
      DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
      DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
      DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
      DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
      DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
      2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
      A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
      AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
    NumGlyphs = 2
    ParentColor = False
    Margin = 0
  end
  object evBitBtn2: TevBitBtn
    Left = 184
    Top = 408
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 11
    Color = clBlack
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
      9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
      DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
      DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
      DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
      91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
      999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
    NumGlyphs = 2
    ParentColor = False
    Margin = 0
  end
  object cbForceCleanup: TevCheckBox
    Left = 32
    Top = 32
    Width = 289
    Height = 17
    Caption = 'Force cleanup even if it does not appear to be necessary'
    Checked = True
    State = cbChecked
    TabOrder = 0
  end
  object cbProcess: TevCheckBox
    Left = 32
    Top = 56
    Width = 129
    Height = 17
    Caption = 'Process automatically'
    Checked = True
    State = cbChecked
    TabOrder = 1
    OnClick = cbProcessClick
  end
  object cbSendToQueue: TevCheckBox
    Left = 184
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Send to queue'
    Enabled = False
    TabOrder = 2
  end
  object cbPrintReports: TevCheckBox
    Left = 32
    Top = 80
    Width = 81
    Height = 17
    Caption = 'Print reports'
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object cbAllEmployees: TevCheckBox
    Left = 32
    Top = 104
    Width = 129
    Height = 17
    Caption = 'Cleanup all employees'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = cbAllEmployeesClick
  end
  object cbCurrentQuarterOnly: TevCheckBox
    Left = 136
    Top = 80
    Width = 193
    Height = 17
    Caption = 'Fix current quarter only for FUI/SUI'
    TabOrder = 4
  end
  object editEmployees: TevEdit
    Left = 32
    Top = 144
    Width = 289
    Height = 21
    Enabled = False
    TabOrder = 6
  end
  object dtQuarterEndDate: TevDateTimePicker
    Left = 32
    Top = 192
    Width = 97
    Height = 21
    Date = 37526.000000000000000000
    Time = 37526.000000000000000000
    TabOrder = 7
  end
  object editMinTax: TMaskEdit
    Left = 160
    Top = 192
    Width = 57
    Height = 21
    TabOrder = 8
    Text = '0.10'
  end
  object evGroupBox1: TevGroupBox
    Left = 32
    Top = 224
    Width = 289
    Height = 113
    Caption = 'Do not cleanup'
    TabOrder = 9
    object cbFederal: TevCheckBox
      Left = 8
      Top = 16
      Width = 89
      Height = 17
      Caption = 'Federal'
      TabOrder = 0
    end
    object cbEEOASDI: TevCheckBox
      Left = 104
      Top = 16
      Width = 89
      Height = 17
      Caption = 'EE OASDI'
      TabOrder = 1
    end
    object cbEROASDI: TevCheckBox
      Left = 200
      Top = 16
      Width = 81
      Height = 17
      Caption = 'ER OASDI'
      TabOrder = 2
    end
    object cbEEMedicare: TevCheckBox
      Left = 8
      Top = 40
      Width = 89
      Height = 17
      Caption = 'EE Medicare'
      TabOrder = 3
    end
    object cbERMedicare: TevCheckBox
      Left = 104
      Top = 40
      Width = 89
      Height = 17
      Caption = 'ER Medicare'
      TabOrder = 4
    end
    object cbERFUI: TevCheckBox
      Left = 200
      Top = 41
      Width = 81
      Height = 17
      Caption = 'ER FUI'
      TabOrder = 5
    end
    object cbState: TevCheckBox
      Left = 8
      Top = 64
      Width = 89
      Height = 17
      Caption = 'State'
      TabOrder = 6
    end
    object cbEESDI: TevCheckBox
      Left = 104
      Top = 64
      Width = 89
      Height = 17
      Caption = 'EE SDI'
      TabOrder = 7
    end
    object cbERSDI: TevCheckBox
      Left = 200
      Top = 64
      Width = 81
      Height = 17
      Caption = 'ER SDI'
      TabOrder = 8
    end
    object cbSUI: TevCheckBox
      Left = 8
      Top = 88
      Width = 81
      Height = 17
      Caption = 'SUI'
      TabOrder = 9
    end
    object cbLocal: TevCheckBox
      Left = 104
      Top = 88
      Width = 81
      Height = 17
      Caption = 'Local'
      TabOrder = 10
    end
  end
  object updwChangeQuarter: TUpDown
    Left = 129
    Top = 192
    Width = 15
    Height = 21
    Hint = 'Change Quarter'
    Associate = dtQuarterEndDate
    Min = -1000
    Max = 1000
    TabOrder = 12
    OnClick = updwChangeQuarterClick
  end
  object cbQtrEndCheckDate: TevCheckBox
    Left = 176
    Top = 104
    Width = 145
    Height = 17
    Caption = 'Quarter End Check Date'
    TabOrder = 13
  end
  object MovePaAmounts: TevCheckBox
    Left = 16
    Top = 368
    Width = 193
    Height = 17
    Caption = 'Move PA local tax amounts only'
    TabOrder = 14
  end
end
