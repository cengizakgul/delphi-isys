inherited EDIT_PR: TEDIT_PR
  OnExit = FrameExit
  inherited PageControl1: TevPageControl
    ActivePage = TabSheet2
    OnChange = PageControl1Change
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        inherited pnlFashionBrowse: TisUIFashionPanel
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              inherited wwdbgridSelectClient: TevDBGrid
                IniAttributes.SectionName = 'TEDIT_PR\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              inherited PRGrid: TevDBGrid
                IniAttributes.SectionName = 'TEDIT_PR\PRGrid'
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      OnShow = TabSheet2Show
      object Label1: TevLabel
        Left = 8
        Top = 208
        Width = 47
        Height = 16
        Caption = '~PR Run'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TevLabel
        Left = 8
        Top = 288
        Width = 66
        Height = 16
        Caption = '~Check Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TevLabel
        Left = 8
        Top = 327
        Width = 88
        Height = 13
        Caption = 'Actual Call-In Date'
      end
      object Label3: TevLabel
        Left = 8
        Top = 248
        Width = 33
        Height = 16
        Caption = '~Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TevLabel
        Left = 8
        Top = 368
        Width = 83
        Height = 13
        Caption = 'Payroll Comments'
      end
      object Bevel1: TEvBevel
        Left = 512
        Top = 208
        Width = 153
        Height = 157
      end
      object Bevel2: TBevel
        Left = 176
        Top = 208
        Width = 153
        Height = 157
      end
      object Bevel3: TBevel
        Left = 344
        Top = 208
        Width = 153
        Height = 157
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'CHECK_DATE'#9'10'#9'Check Date'#9
          'RUN_NUMBER'#9'5'#9'Run Number'#9
          'SCHEDULED'#9'1'#9'Scheduled'#9
          'PAYROLL_TYPE'#9'1'#9'Type'#9
          'STATUS'#9'1'#9'Status'#9
          'PROCESS_DATE'#9'10'#9'Process Date'#9
          'EXCLUDE_ACH'#9'1'#9'Exclude Ach'#9
          'EXCLUDE_TAX_DEPOSITS'#9'1'#9'Exclude Tax Deposits'#9
          'EXCLUDE_AGENCY'#9'1'#9'Exclude Agency'#9
          'MARK_LIABS_PAID_DEFAULT'#9'1'#9'Mark Liabilities as Paid'#9
          'EXCLUDE_BILLING'#9'1'#9'Exclude Billing'#9
          'EXCLUDE_R_C_B_0R_N'#9'1'#9'Exclude R C B 0r N'#9
          'SCHEDULED_CALL_IN_DATE'#9'10'#9'Scheduled Call In Date'#9
          'ACTUAL_CALL_IN_DATE'#9'10'#9'Actual Call In Date'#9
          'SCHEDULED_PROCESS_DATE'#9'10'#9'Scheduled Process Date'#9
          'SCHEDULED_DELIVERY_DATE'#9'10'#9'Scheduled Delivery Date'#9
          'SCHEDULED_CHECK_DATE'#9'10'#9'Scheduled Check Date'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alTop
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwDBComboBox1: TevDBComboBox
        Left = 8
        Top = 264
        Width = 153
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'PAYROLL_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Regular'#9'R'
          'Supplemental'#9'S'
          'Reversal'#9'V'
          'Client Correction'#9'C'
          'SB Correction'#9'B'
          'Setup Run'#9'E'
          'Misc Check Adjustment'#9'M'
          'Tax Adjustment'#9'T'
          'Wage Adj with Qtr'#9'Q'
          'Wage Adj no Qtr'#9'N'
          'Qtr End Tax Adjustment'#9'A'
          'Tax Deposit'#9'D'
          'Import'#9'I'
          'Backdated Setup'#9'K')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
        OnChange = wwDBComboBox1Change
      end
      object DBMemo1: TEvDBMemo
        Left = 8
        Top = 384
        Width = 657
        Height = 41
        DataField = 'PAYROLL_COMMENTS'
        DataSource = wwdsDetail
        TabOrder = 6
      end
      object wwDBDateTimePicker1: TevDBDateTimePicker
        Left = 8
        Top = 304
        Width = 153
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'CHECK_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 4
      end
      object wwDBDateTimePicker2: TevDBDateTimePicker
        Left = 8
        Top = 344
        Width = 153
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'ACTUAL_CALL_IN_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 5
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 64
        Top = 208
        Width = 97
        Height = 41
        Caption = '~Scheduled'
        Columns = 2
        DataField = 'SCHEDULED'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBSpinEdit1: TevDBSpinEdit
        Left = 8
        Top = 224
        Width = 41
        Height = 21
        Increment = 1.000000000000000000
        MaxValue = 999.000000000000000000
        MinValue = 1.000000000000000000
        Value = 1.000000000000000000
        DataField = 'RUN_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object butnProcess: TevBitBtn
        Left = 184
        Top = 216
        Width = 137
        Height = 25
        Caption = 'Send to Queue'
        TabOrder = 7
        OnClick = butnProcessClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD888888888D
          DDDDDFFFFFFFFFFDDDDDD0000000008DDDDDD888888888FDDDDDD0BBBBBBB08D
          DDDDD888888888FDDDDDD0B00000B0800DDDD88DDDDD88F88DDDD0BBBBBBB087
          0DDDD888888888FD8DDDD0B00000B0870DDDD88DDDDD88FD8DDDD0BBBBBBB087
          0DDDD888888888FD8DDDD0B00000B0870000D88DDDDD88FD8D88D0BBBBBBB087
          0770D888888888FD8DD8D000000000770770D888888888FD8DD8DDDDD0777777
          0770DDDDDDDDDDDD8DD8DDDDD00000000770DDDDD88888888DD8DDDDDDDD0707
          7770DDDDDDDDDDDDDDD8DDDDDDDD07000000DDDDDDDD8D888888DDDDDDDD0777
          70DDDDDDDDDD8DDDDDDDDDDDDDDD000000DDDDDDDDDD888888DD}
        NumGlyphs = 2
        Margin = 0
      end
      object butnInvoice: TevBitBtn
        Left = 184
        Top = 255
        Width = 137
        Height = 26
        Caption = 'Create Invoice'
        TabOrder = 8
        OnClick = butnInvoiceClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD8888888868
          88DDDD8888888FFDD8DDDD8FFF77866668DDDD8DDDDFF88FFDDDDD8F88866E66
          666DDD8D8DF88D888FFDDD8F766EEE666668DD8DDF8DDD88888FDD8F86EEEE66
          6668DD8DD8DDDD88888FDD8F76EEE6E66668DD8DD8DDD8D8888FDD8F86E66EEE
          E668DD8DD8D88DDDD88FDD87766EEEEEEE68DD8DD88DDDDDDD8DDD8FC7866EEE
          668DDDD8FDD88DDD88DDDD8FC777866688DDDDD8FDDDD888DDDDFC7FC7FC7768
          78DD8FD8FD8FDDDDD8DDDFC77FC7FF77F8DDD8FDD8FDDDDDD8DDDD77777FFFFF
          F8DDDDDDDDDDDDDDD8DDDFCFCFC7888888DDD8F8F8FD888888DDFCDFCDFCDDDD
          DDDD8FD8FD8FDDDDDDDDDDDFCDDDDDDDDDDDDDD8FDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object butnPreProcess: TevBitBtn
        Left = 184
        Top = 293
        Width = 137
        Height = 25
        Caption = 'Pre-Process'
        TabOrder = 9
        OnClick = butnPreProcessClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD0DD0DDDDD
          DDDDDDDFDDFDDDDDDDDDDD0E00E0DDDDDDDDDDF8FF8FDDDDDDDDDDEEEEEE8888
          8888DD888888FFFFFFFFD00EEEE007FFFFF8DFF8888FFD88888FDEEE07EEE7FF
          FFF8D888FD888D88888FDEEE00EEE74444F8D888FF888DDDDD8FDD0EEEE07FFF
          FFF8DDF8888FD888888FDDEEEEEE744444F8DD888888DDDDDD8FD03E77E7FFFF
          FFF8DFD8DD8D8888888FDBB33F74444444F8D88DD8DDDDDDDD8F00BB3FFFFFFF
          FFF8FF88D8888888888FBBB0DFF4444444F8888FD88DDDDDDD8FBBB00FFFFFFF
          FFF8888FF8888888888FD0BB3FF4444444F8DF88D88DDDDDDD8FDBBB3FFFFFFF
          FFF8D888D8888888888FDDBD3FFFFFFFFFFDDD8DD8888888888D}
        NumGlyphs = 2
        Margin = 0
      end
      object btnCheckRecon: TevBitBtn
        Left = 184
        Top = 332
        Width = 137
        Height = 25
        Caption = 'Bank account register'
        TabOrder = 10
        OnClick = btnCheckReconClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D66666666666
          666DD88888888888888D66666666688888868DDDDDDDDDDDDDD86EBBBBEE777B
          BB768D8888DDDDD888D86E666666777777768DDDDDDDDDDDDDD86EBBBBBB7777
          77768D888888DDDDDDD86EEEE7E7777777768DDDDDDDDDDDDDD86EEE7E7E791B
          3B768DDDDDDDD88D88D86EE7E7E7E10103768DDDDDDDD8D8D8D86E7FCE7E791B
          3B768DD8FDDDD88D88D8777FC7E7E7E77776DDD8FDDDDDDDDDD8FC7FC7FC7666
          666D8FD8FD8FD888888DDFCDDFCDDDDDDDDDD8FDD8FDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDFCFCFCDDDDDDDDDD8F8F8FDDDDDDDDDFCDFCDFCDDDD
          DDDD8FD8FD8FDDDDDDDDDDDFCDDDDDDDDDDDDDD8FDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object butnCopy: TevBitBtn
        Left = 352
        Top = 216
        Width = 137
        Height = 25
        Caption = 'Copy This Payroll'
        TabOrder = 11
        OnClick = butnCopyClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD444444
          4444DDDDDD8888888888DDDDDD4777777774DDDDDD8DDDDDDDD8D88888488888
          7774DDDDDD8DDDDDDDD80000000000044774DDDDDD8DD8888DD80FFFFFFFFF08
          7774FFFFFFFFFFDDDDD80FFFFFFFFF044774888888888F888DD80F7000007F08
          7774888888888FDDDDD80FFFFFFFFF04477488DDDDD88F888DD80F7000007F08
          7774888888888FDDDDD80FFFFFFFFF04477488DDDDD88F888DD80F7000007F08
          7774888888888FDDDDD80FFFFFFFFF08777488DDDDD88FDDDDD80F7000007F04
          4444888888888F8888880FFFFFFFFF08DDDD88DDDDD88FDDDDDD0FFFFFFFFF08
          DDDD888888888FDDDDDD00000000000DDDDD888888888FDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object butnVoid: TevBitBtn
        Left = 352
        Top = 255
        Width = 137
        Height = 25
        Caption = 'Void This Payroll'
        TabOrder = 12
        OnClick = butnVoidClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD00DDDDDD0
          DDDDDDDDFDDDDDDFDDDDDD0040DDDD040DDDDDDF8FDDDDF8FDDDD004F400004F
          400DDDF888F88F888F8D0C4FFF4004FFF4CDDD88888FF88888DDCCC4FFF44FFF
          4CCD8DD8888888888D8DD0004FFFFFF4CCCDD88D88888888D88D887774FFFF4D
          DDDD8D88D888888DDDDD887704FFFF40DDDD8D88F888888FDDDD88804FFFFFF4
          0DDD8DDF88888888FDDDD804FFF44FFF40DDD8F8888888888FDD884FFF4774FF
          F40D8D88888DD88888DD8874F4E6784F4CC088D888D8DD888D8D887E4EE678C4
          CCCC88DD8D88D8D8DDD8887EEEE678CCCCCD88D8D888D88D8D8D7877777778DD
          CCDD88DDDDDDD8DD88DDD88888888DDDCDDDD88888888DDD8DDD}
        NumGlyphs = 2
        Margin = 0
      end
      object butnUnlock: TevBitBtn
        Left = 352
        Top = 293
        Width = 137
        Height = 25
        Caption = 'Unlock This Payroll'
        TabOrder = 13
        OnClick = butnUnlockClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDD0000000
          0000DDDDDDFFFFFFFFFFF74440EEEEEEEE60DDDDDD888888888FF74C40EEEE0E
          EE60DD888D8888F8888FF74C40EEE60EEE60DD888D888FD8888FF7CC70EEE00E
          EE60DD88DD888FF8888FFFCC70EEEEEEEE608D88DD888888888FDFCC70000000
          0000D888DD888888888F87773B3888880E088DDD8DDDDDDD88FD8788B3300087
          0E088DDDD8DDFF8D88FD8777BB30E0880E088D88DDD88FDD88FD8788BBB0E000
          0E088DDDDDD88FFF88FD8744B3B30EEEE08D8D88D8DD88888FDD877CBBBB8000
          088D8DD8DDDDD888FD8D8777CCC47777778D8DDD8888DDDDDD8D8888CCC48888
          888D888D8888D888888DDDDD8888DDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object butnAdjust: TevBitBtn
        Left = 352
        Top = 332
        Width = 137
        Height = 25
        Caption = 'Adjust Agency Lines'
        TabOrder = 14
        OnClick = butnAdjustClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD88888D8D8D
          8D8DDDFFFFFDFDFDFDFDD800000D0D0D0D0DFD88888D8F8F8F8F000FFF000000
          00008D88888D88888888DD0FFF0DDD44DDDDDD88888DDDDDDDDDDD70F07DD4B3
          4DDDDDD888DDDD88DDDDDDD707DDD3F3344DDDDD8DDDD8D8888DDDDD7DDDD3BB
          3DDDDDDDDDDDD8D88DDDDDD44DDD4BBBBDDDDDDDDDDD88DD8DDDDD4B34D4D7BB
          7DDDDDD88DD8DD88DDDD443F334DDDDDDDDD888D888DDDDDDDDDDD3BB3D4DD44
          DDDDDD8D88D8DDDDDDDDDDBBBBDD44B34DDDDD8DD8DD8D88DDDDDD7BB7DDD3F3
          344DDDD88DDDD8D8888DDDDDDDDDD3BB3DDDDDDDDDDDD8D88DDDDDDDDDDDDBBB
          BDDDDDDDDDDDD8DD8DDDDDDDDDDDD7BB7DDDDDDDDDDDDD88DDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object butnTaxAdjustments: TevBitBtn
        Left = 520
        Top = 216
        Width = 137
        Height = 25
        Caption = 'Tax Adjustments'
        TabOrder = 15
        OnClick = butnTaxAdjustmentsClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0DDCCDDDDD
          CCDDDD8DD88DDDDD88DDD80DDDCCDDDCDDCDD88DDD88DDD8DD8DDD0DDDDCCDDC
          DDCDDD8DDDD88DD8DD8DD80DDDDDCCDDCCDDD88DDDDD88DD88DDDD0DDDDDDCCD
          DDDDDD8DDDDDD88DDDDDD80DDDCCDDCCDDDDD88DDD88DD88DDDDDD0DDCDDCDDC
          CDDDDD8DD8DD8DD88DDDD80DDCDDCDDDCCDDD88DD8DD8DDD88DDDD0DDDCCDDDD
          DCCDDDDDDD88DDDDD88D00008DDDDDDDDDDD8888FDDDDDDDDDDD0FFF08DDDDDD
          DDDD88888FDDDDDDDDDD0FFFF0CCDCCCDCCC888888F8D888D8880FFF0DDDDDDD
          DDDD88888FDDDDDDDDDD0000DDDDDDDDDDDD8888FDDDDDDDDDDDD80DDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDD0DDDDDDDDDDDDDDD8DDDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object butnQtrEndCleanupPR: TevBitBtn
        Left = 520
        Top = 255
        Width = 137
        Height = 25
        Caption = 'Qtr End Cleanup PR'
        TabOrder = 16
        OnClick = butnQtrEndCleanupPRClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
          8888DFFFFFFFFFFFFFFF0000000000000008888888888888888F0FFFFFFFFFFF
          FF08888888888888888F0F77777777777F08888888888888888F0F8888888888
          8F0888DDDDDDDDDDD88F0F88FFFFFF777F0888DD88888888888F0FFFFFFFFF88
          8F088888888888DDD88F0FFFFFFFFFFFFF08888888888888888F000000000000
          000D888888888888888DDDD2AAAAAAAAA28DDDD8DDDDDDDDD8DDDDD111111AAA
          A28DDDD888888DDDD8DDDDD199991AAAA28DDDD888888DDDD8DDDDDD19991AAA
          28DDDDDD88888DDD8DDDDDDD19991AAA28DDDDDD88888DDD8DDDDDDDD1191A22
          8DDDDDDDD8888D88DDDDDDDDDDD1128DDDDDDDDDDDD888DDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object butnPAMonthlyCleanup: TevBitBtn
        Left = 520
        Top = 293
        Width = 137
        Height = 25
        Caption = 'PA Monthly Cleanup'
        TabOrder = 17
        OnClick = butnPAMonthlyCleanupClick
        Color = clBlack
        Glyph.Data = {
          42030000424D42030000000000003600000028000000130000000D0000000100
          1800000000000C03000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF005B00005B0000
          5B00005B00005B00005B00005B00005B00005B00005B00005B00005B00005B00
          005B00005B00005B00FFFFFFFFFFFF000000FFFFFF005B00009B13129C26129A
          250DAE251E8B2E4296507DC78A6EBB7B75C68368B27558B5671A932B89AE8FA2
          BCA7005B00005B00FFFFFF000000FFFFFF005B0000B10D0AAB210AAF2107B921
          19972B228A3386D9947CCB896DBC7887DB950BA8210C9E20529B5D4F9C5A52AB
          5F005B00FFFFFF000000FFFFFF005B0000A60E0C9D1F0CB7250DAA230AB5242A
          8A3932974239994888D39592DA9E7CC5891D822C3B974A34B847C7E3CB005B00
          FFFFFF000000FFFFFF005B0000AD0E0DAF240DAA230BB7240AB221279037298F
          39288437139F282AC3411D902F2791377AD7898AC093005B00005B00FFFFFF00
          0000FFFFFF005B0000AC0E05B81F06AB1D11A42629913925873428833525A238
          1ABD3214AE2B1AC73551D36552CE6562AF6E005B00FFFFFFFFFFFF000000FFFF
          FF005B0000A7005BC56C3ABA4C1688272B8B3A2983371CAF3417D23415AB2B13
          B82C2EC24453DC694ECC6156D16955C766005B00FFFFFF000000FFFFFF005B00
          66D37785D39282CD8E77DD881D812D2784361FAB3415BE2F17C8321DC1364DD3
          6151DF674AD15F30CF49005B00005B00FFFFFF000000FFFFFF005B006DCE7D77
          CF8566B37288D7951B7E2B1E7C2D12A82A0BB4250AB62525B43B47CF5D32A943
          31B3457DCC89005B00FFFFFFFFFFFF000000FFFFFF005B00005B00A0D6A9005B
          00005B00005B00005B00005B00005B00005B00005B00005B00005B00005B0000
          5B00005B00FFFFFFFFFFFF000000FFFFFFFFFFFF005B00005B00005B00FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF000000}
        Margin = 0
      end
      object butnProrateFICA: TevBitBtn
        Left = 520
        Top = 332
        Width = 137
        Height = 25
        Caption = 'Prorate FICA'
        TabOrder = 18
        OnClick = butnProrateFICAClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD84666664
          448DDDDDD888888888DD8DD84EEEEE466648DDDD8DDDDD8DDD8D4884EEEEE486
          66488DD8DDDDD8DDDD8D444E888888886664888DDDDDDDDDDDD84EE6FFFFFFF0
          44448DDD8888888F88884EE6FF0888F0DDDD8DDD88FDDD8FDDDD4EEE6FF08888
          DDDD8DDDD88FDDDDDDDD4EEEE4FF044444448DDDDD88F88888884444444FF04E
          EEE4888888D88F8DDDD8DDDDD8FF084EEEE4DDDDDD88FD8DDDD8DDDD8FF08488
          EEE4DDDDD88FD8DDDDD84444FF0888F0EEE4888D88FDDD8FDDD84666FFFFFFF0
          E4448DDD8888888FD88884666846666E4884D8DDDDDDDDDD8DD8846664EEEEE4
          8DD8D8DDD8DDDDD8DDDDD84446666648DDDDDD888888888DDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object chbxQueue: TevCheckBox
        Left = 512
        Top = 369
        Width = 161
        Height = 14
        Caption = 'Queue pre-process and QEC'
        Checked = True
        State = cbChecked
        TabOrder = 19
        Visible = False
      end
    end
    object tsExclusions: TTabSheet
      Caption = 'Exclusions'
      ImageIndex = 20
      OnEnter = tsExclusionsEnter
      object evLabel6: TevLabel
        Left = 348
        Top = 27
        Width = 107
        Height = 16
        Caption = '~Trust Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel8: TevLabel
        Left = 348
        Top = 123
        Width = 153
        Height = 16
        Caption = '~Workers Comp Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel7: TevLabel
        Left = 348
        Top = 99
        Width = 110
        Height = 16
        Caption = '~Billing Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel9: TevLabel
        Left = 348
        Top = 75
        Width = 150
        Height = 16
        Caption = '~Direct Deposit Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel10: TevLabel
        Left = 348
        Top = 51
        Width = 101
        Height = 16
        Caption = '~Tax Impound Type '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBRadioGroup2: TevDBRadioGroup
        Left = 16
        Top = 16
        Width = 153
        Height = 41
        Caption = '~Exclude All ACH'
        Columns = 2
        DataField = 'EXCLUDE_ACH'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 0
        Values.Strings = (
          'Y'
          'N')
        OnChange = DBRadioGroup2Change
        OnClick = DBRadioGroup2Click
      end
      object DBRadioGroup6: TevDBRadioGroup
        Left = 16
        Top = 56
        Width = 153
        Height = 57
        Caption = '~Exclude Billing'
        Columns = 2
        DataField = 'EXCLUDE_BILLING'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'All'
          'None'
          'Select')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'Y'
          'N'
          'C')
        OnChange = DBRadioGroup6Click
        OnClick = DBRadioGroup6Click
      end
      object DBRadioGroup3: TevDBRadioGroup
        Left = 16
        Top = 112
        Width = 153
        Height = 57
        Caption = '~Exclude Tax'
        Columns = 2
        DataField = 'EXCLUDE_TAX_DEPOSITS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Deposits'
          'Liabilities'
          'Both'
          'None')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'D'
          'L'
          'B'
          'N')
      end
      object DBRadioGroup4: TevDBRadioGroup
        Left = 184
        Top = 16
        Width = 153
        Height = 41
        Caption = '~Exclude Agency'
        Columns = 2
        DataField = 'EXCLUDE_AGENCY'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup7: TevDBRadioGroup
        Left = 184
        Top = 112
        Width = 153
        Height = 57
        Caption = '~Exclude'
        Columns = 2
        DataField = 'EXCLUDE_R_C_B_0R_N'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Reports'
          'Checks'
          'Both'
          'None')
        ParentFont = False
        TabOrder = 4
        Values.Strings = (
          'R'
          'C'
          'B'
          'N')
      end
      object evDBRadioGroup2: TevDBRadioGroup
        Left = 184
        Top = 56
        Width = 153
        Height = 57
        Cursor = crDrag
        Caption = '~Exclude TO Accrual'
        Columns = 2
        DataField = 'EXCLUDE_TIME_OFF'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'All'
          'Accrual'
          'No')
        ParentFont = False
        TabOrder = 5
        Values.Strings = (
          'F'
          'Y'
          'N')
      end
      object btnQTDYTD: TevBitBtn
        Left = 520
        Top = 210
        Width = 153
        Height = 25
        Caption = 'Subtract QTD from YTD'
        TabOrder = 6
        OnClick = btnQTDYTDClick
        Color = clBlack
        Margin = 0
      end
      object btnUnlockPR: TevBitBtn
        Left = 504
        Top = 170
        Width = 193
        Height = 25
        Caption = 'Mark Processed Payroll as Waiting'
        TabOrder = 7
        OnClick = btnUnlockPRClick
        Color = clBlack
        Margin = 0
      end
      object rdgMarkAsPaid: TevDBRadioGroup
        Left = 16
        Top = 176
        Width = 153
        Height = 41
        Caption = '~Mark Liabilities as Paid'
        Columns = 2
        DataField = 'MARK_LIABS_PAID_DEFAULT'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 8
        Values.Strings = (
          'Y'
          'N')
      end
      object TrustComboBox: TevDBComboBox
        Left = 525
        Top = 18
        Width = 169
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TRUST_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 9
        UnboundDataType = wwDefault
      end
      object TaxComboBox: TevDBComboBox
        Left = 525
        Top = 44
        Width = 169
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'TAX_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 10
        UnboundDataType = wwDefault
      end
      object DDComboBox: TevDBComboBox
        Left = 525
        Top = 70
        Width = 169
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'DD_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 11
        UnboundDataType = wwDefault
      end
      object BillingComboBox: TevDBComboBox
        Left = 525
        Top = 96
        Width = 169
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'BILLING_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 12
        UnboundDataType = wwDefault
      end
      object WCComboBox: TevDBComboBox
        Left = 525
        Top = 122
        Width = 169
        Height = 21
        HelpContext = 11507
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'WC_IMPOUND'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 13
        UnboundDataType = wwDefault
      end
      object gbServices: TGroupBox
        Left = 176
        Top = 176
        Width = 273
        Height = 241
        Caption = 'Select services to block'
        TabOrder = 14
        object evDBGridServices: TevDBCheckGrid
          Left = 8
          Top = 16
          Width = 257
          Height = 217
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'40'#9'Name'#9'F'#9
            'EFFECTIVE_START_DATE'#9'10'#9'Effective Start Date'#9'F'#9
            'EFFECTIVE_END_DATE'#9'10'#9'Effective End Date'#9'F'#9)
          IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
          IniAttributes.SectionName = 'TEDIT_PR\evDBGridServices'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          OnMultiSelectAllRecords = evDBGridServicesMultiSelectRecord
          OnMultiSelectRecord = evDBGridServicesMultiSelectRecord
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = dsServices
          MultiSelectOptions = [msoShiftSelect]
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
          ReadOnly = True
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
          OnMultiSelectReverseRecords = evDBGridServicesMultiSelectReverseRecords
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Special Handling'
      ImageIndex = 26
      object evLabel4: TevLabel
        Left = 16
        Top = 64
        Width = 102
        Height = 13
        Caption = 'Correction Run Notes'
      end
      object evLabel5: TevLabel
        Left = 344
        Top = 64
        Width = 124
        Height = 13
        Caption = 'Miscellaneous Instructions'
      end
      object evLabel21: TevLabel
        Left = 472
        Top = 8
        Width = 193
        Height = 49
        AutoSize = False
        Caption = 
          'Please, note that "Same Day Pull and Replace" and "Combine Runs"' +
          ' are for informational purposes only'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object evDBRadioGroup3: TevDBRadioGroup
        Left = 16
        Top = 16
        Width = 177
        Height = 41
        Caption = '~Same Day Pull and Replace'
        Columns = 2
        DataField = 'SAME_DAY_PULL_AND_REPLACE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 0
        Values.Strings = (
          'Y'
          'N')
      end
      object evDBRadioGroup4: TevDBRadioGroup
        Left = 208
        Top = 16
        Width = 121
        Height = 41
        Caption = '~Combine Runs'
        Columns = 2
        DataField = 'COMBINE_RUNS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'Y'
          'N')
        OnChange = evDBRadioGroup4Change
      end
      object grbxCombineRuns: TevGroupBox
        Left = 344
        Top = 0
        Width = 113
        Height = 65
        Caption = 'Runs To Combine'
        TabOrder = 2
        object evLabel2: TevLabel
          Left = 16
          Top = 16
          Width = 23
          Height = 13
          Caption = 'From'
        end
        object evLabel3: TevLabel
          Left = 64
          Top = 16
          Width = 13
          Height = 13
          Caption = 'To'
        end
        object evDBSpinEdit2: TevDBSpinEdit
          Left = 16
          Top = 32
          Width = 33
          Height = 21
          Increment = 1.000000000000000000
          MaxValue = 99.000000000000000000
          MinValue = 1.000000000000000000
          Value = 1.000000000000000000
          DataField = 'COMBINE_FROM_RUN'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
        end
        object evDBSpinEdit3: TevDBSpinEdit
          Left = 64
          Top = 32
          Width = 33
          Height = 21
          Increment = 1.000000000000000000
          MaxValue = 99.000000000000000000
          MinValue = 1.000000000000000000
          Value = 1.000000000000000000
          DataField = 'COMBINE_TO_RUN'
          DataSource = wwdsDetail
          TabOrder = 1
          UnboundDataType = wwDefault
        end
      end
      object evDBRadioGroup5: TevDBRadioGroup
        Left = 16
        Top = 144
        Width = 153
        Height = 41
        Caption = '~Print All Reports'
        Columns = 2
        DataField = 'PRINT_ALL_REPORTS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 3
        Values.Strings = (
          'Y'
          'N')
        OnChange = evDBRadioGroup5Change
      end
      object EvDBMemo1: TEvDBMemo
        Left = 16
        Top = 80
        Width = 313
        Height = 57
        DataField = 'CR_NOTES'
        DataSource = wwdsDetail
        TabOrder = 4
      end
      object EvDBMemo2: TEvDBMemo
        Left = 344
        Top = 80
        Width = 313
        Height = 57
        DataField = 'MISCELLANEOUS_INSTRUCTIONS'
        DataSource = wwdsDetail
        TabOrder = 5
      end
      object grbxReports: TevGroupBox
        Left = 184
        Top = 144
        Width = 313
        Height = 281
        Caption = 'Reports To Print'
        TabOrder = 6
        object lablE_D_Code: TevLabel
          Left = 16
          Top = 236
          Width = 72
          Height = 16
          Caption = '~Report Name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object grReports: TevDBGrid
          Left = 16
          Top = 16
          Width = 281
          Height = 209
          DisableThemesInTitle = False
          Selected.Strings = (
            'ReportName'#9'40'#9'Report Name'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_PR\grReports'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        inline MiniNavigationFrame: TMiniNavigationFrame
          Left = 231
          Top = 234
          Width = 65
          Height = 132
          TabOrder = 1
          inherited evActionList1: TevActionList
            Left = 16
          end
        end
        object wwlcReport: TevDBLookupCombo
          Left = 16
          Top = 252
          Width = 177
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_NAME'#9'40'#9'CUSTOM_E_D_CODE_NUMBER')
          DataField = 'CO_REPORTS_NBR'
          LookupField = 'CO_REPORTS_NBR'
          Style = csDropDownList
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
      object bbtnPrint: TevBitBtn
        Left = 16
        Top = 392
        Width = 75
        Height = 25
        Caption = '&Print'
        TabOrder = 7
        OnClick = bbtnPrintClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
          08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
          8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
          8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
          8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
          78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
          7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
          DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
          DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 240
    Top = 2
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_PR.PR
    OnStateChange = wwdsDetailStateChange
    MasterDataSource = nil
    Left = 310
    Top = 74
  end
  inherited wwdsList: TevDataSource
    Left = 34
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Top = 23
  end
  inherited dsCL: TevDataSource
    Left = 335
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 328
    Top = 65535
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 394
    Top = 2
  end
  inherited wwdsSubMaster: TevDataSource
    Left = 294
    Top = 0
  end
  inherited DM_PAYROLL: TDM_PAYROLL
    Left = 439
    Top = 2
  end
  inherited ActionList: TevActionList
    Left = 347
  end
  object dsPRReports: TevDataSource
    DataSet = DM_PR_REPORTS.PR_REPORTS
    Left = 252
    Top = 241
  end
  object evdsPrServices: TevDataSource
    DataSet = DM_PR_SERVICES.PR_SERVICES
    Left = 252
    Top = 321
  end
  object dsServices: TevDataSource
    DataSet = DM_CO_SERVICES.CO_SERVICES
    MasterDataSource = wwdsDetail
    Left = 260
    Top = 281
  end
end
