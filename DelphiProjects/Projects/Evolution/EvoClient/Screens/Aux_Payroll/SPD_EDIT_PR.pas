// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_PR;

interface

uses
  SDataStructure,  SPD_EDIT_PR_BASE, DBCtrls, StdCtrls, EvBasicUtils,
  ExtCtrls, wwdbdatetimepicker, Wwdotdot, Wwdbcomb, Mask, wwdbedit, Db,
  Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls, Classes,
  DBActns, ActnList, EvConsts, Dialogs, SysUtils, EvUtils, EvTypes, EvMainboard,
  SPackageEntry, EvSecElement, SSecurityInterface, Wwdbspin, EvCommonInterfaces,
  wwdblook, Forms, SPD_MiniNavigationFrame, Variants, EvContext,
  ISBasicClasses, SDDClasses, SDataDictclient, SDataDicttemp, EvExceptions,
  SPD_TOBalanceCheck, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBDateTimePicker, isUIDBMemo,
  isUIwwDBComboBox, ImgList, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, LMDCustomButton, LMDButton, 
  isUIFashionPanel, isUILMDButton, EvDataset;

type
  TEDIT_PR = class(TEDIT_PR_BASE)
    TabSheet2: TTabSheet;
    Label1: TevLabel;
    Label2: TevLabel;
    Label13: TevLabel;
    Label3: TevLabel;
    wwDBGrid1: TevDBGrid;
    wwDBComboBox1: TevDBComboBox;
    Label4: TevLabel;
    DBMemo1: TEvDBMemo;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    wwDBDateTimePicker2: TevDBDateTimePicker;
    Bevel1: TEvBevel;
    tsExclusions: TTabSheet;
    DBRadioGroup2: TevDBRadioGroup;
    DBRadioGroup6: TevDBRadioGroup;
    DBRadioGroup3: TevDBRadioGroup;
    DBRadioGroup4: TevDBRadioGroup;
    DBRadioGroup7: TevDBRadioGroup;
    evDBRadioGroup1: TevDBRadioGroup;
    evDBSpinEdit1: TevDBSpinEdit;
    Bevel2: TBevel;
    Bevel3: TBevel;
    evDBRadioGroup2: TevDBRadioGroup;
    TabSheet4: TTabSheet;
    evDBRadioGroup3: TevDBRadioGroup;
    evDBRadioGroup4: TevDBRadioGroup;
    grbxCombineRuns: TevGroupBox;
    evLabel2: TevLabel;
    evDBSpinEdit2: TevDBSpinEdit;
    evDBSpinEdit3: TevDBSpinEdit;
    evLabel3: TevLabel;
    evDBRadioGroup5: TevDBRadioGroup;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    EvDBMemo1: TEvDBMemo;
    EvDBMemo2: TEvDBMemo;
    grbxReports: TevGroupBox;
    lablE_D_Code: TevLabel;
    grReports: TevDBGrid;
    MiniNavigationFrame: TMiniNavigationFrame;
    wwlcReport: TevDBLookupCombo;
    dsPRReports: TevDataSource;
    butnProcess: TevBitBtn;
    butnInvoice: TevBitBtn;
    butnPreProcess: TevBitBtn;
    btnCheckRecon: TevBitBtn;
    butnCopy: TevBitBtn;
    butnVoid: TevBitBtn;
    butnUnlock: TevBitBtn;
    butnAdjust: TevBitBtn;
    butnTaxAdjustments: TevBitBtn;
    butnQtrEndCleanupPR: TevBitBtn;
    butnPAMonthlyCleanup: TevBitBtn;
    butnProrateFICA: TevBitBtn;
    chbxQueue: TevCheckBox;
    bbtnPrint: TevBitBtn;
    evLabel21: TevLabel;
    btnQTDYTD: TevBitBtn;
    btnUnlockPR: TevBitBtn;
    rdgMarkAsPaid: TevDBRadioGroup;
    evLabel6: TevLabel;
    TrustComboBox: TevDBComboBox;
    TaxComboBox: TevDBComboBox;
    DDComboBox: TevDBComboBox;
    BillingComboBox: TevDBComboBox;
    WCComboBox: TevDBComboBox;
    evLabel8: TevLabel;
    evLabel7: TevLabel;
    evLabel9: TevLabel;
    evLabel10: TevLabel;
    gbServices: TGroupBox;
    evDBGridServices: TevDBCheckGrid;
    evdsPrServices: TevDataSource;
    dsServices: TevDataSource;
    procedure PayrollAfterScroll(DataSet: TDataSet);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure butnProcessClick(Sender: TObject);
    procedure butnCopyClick(Sender: TObject);
    procedure butnVoidClick(Sender: TObject);
    procedure butnPreProcessClick(Sender: TObject);
    procedure butnInvoiceClick(Sender: TObject);
    procedure butnTaxAdjustmentsClick(Sender: TObject);
    procedure butnPAMonthlyCleanupClick(Sender: TObject);
    procedure butnQtrEndCleanupPRClick(Sender: TObject);
    procedure btnCheckReconClick(Sender: TObject);
    procedure butnAdjustClick(Sender: TObject);
    procedure butnProrateFICAClick(Sender: TObject);
    procedure butnUnlockClick(Sender: TObject);
    procedure evDBRadioGroup4Change(Sender: TObject);
    procedure evDBRadioGroup5Change(Sender: TObject);
    procedure bbtnPrintClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure btnQTDYTDClick(Sender: TObject);
    procedure btnUnlockPRClick(Sender: TObject);
    procedure FrameExit(Sender: TObject);
    procedure wwDBComboBox1Change(Sender: TObject);
    procedure evDBGridServicesMultiSelectRecord(Grid: TwwDBGrid;
      Selecting: Boolean; var Accept: Boolean);
    procedure evDBGridServicesMultiSelectReverseRecords(Grid: TwwDBGrid;
      var Accept: Boolean);
    procedure DBRadioGroup6Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure DBRadioGroup2Click(Sender: TObject);
    procedure DBRadioGroup2Change(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure tsExclusionsEnter(Sender: TObject);
  private
    WasVoidPayroll: Boolean;
    DeletedPrNbr: Integer;
    FBeforePost: TDataSetNotifyEvent;
    FSaveAfterScroll: TDataSetNotifyEvent;
    procedure MarkChanged;
    procedure LoadBillingServices;
    procedure UpdatePaymentTypes;
    procedure SaveBillingServices;
    function PayrollIsOutOfRange: Boolean;
    procedure EnablePayrollControls;
    procedure CheckButtonState;    
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetDataSetConditions(sName: string): string; override;
    procedure BeforePost(DataSet: TDataSet);
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function GetInsertControl: TWinControl; override;
    procedure AfterClick(Kind: Integer); override;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

uses
  SPD_AdjustTaxes, SPD_PayrollAccountRegister, SPD_ProcessException,
  SPD_Change_CheckLine_Status, SPopulateRecords, SReportSettings, SQECParameters,
  SFrameEntry, EvDataAccessComponents, EvStreamUtils,
  SPD_EDIT_Open_BASE, DateUtils, isVCLBugFix, SPAMCParams;

{$R *.DFM}

procedure TEDIT_PR.wwdsDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if (csLoading in ComponentState) then
    exit;
  if wwdsDetail.DataSet.Active then
  begin
    if ctx_DataAccess.GetClCoReadOnlyRight then
    begin
      SetControlsReccuring(TabSheet2, 'SecurityRO', Integer(True));
      exit;
    end;

    butnInvoice.Enabled := (wwdsDetail.DataSet.State = dsBrowse) and (DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PROCESSED) or (DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_VOIDED);
    butnProcess.Enabled := (wwdsDetail.DataSet.State = dsBrowse) and (DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PENDING);
    butnAdjust.Enabled := (wwdsDetail.DataSet.State = dsBrowse) and (DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PROCESSED) or (DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_VOIDED);
    butnProrateFICA.Enabled := (wwdsDetail.DataSet.State = dsBrowse) and (DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PENDING);
    btnQTDYTD.Visible := (wwdsDetail.DataSet.State = dsBrowse) and (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_SETUP);

    if not DM_PAYROLL.PR.FieldByName('STATUS').IsNull then
    begin
      butnPreProcess.Enabled := (wwdsDetail.DataSet.State = dsBrowse)
                            and (not (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in
                                        [PAYROLL_STATUS_PROCESSED,
                                        PAYROLL_STATUS_VOIDED,
                                        PAYROLL_STATUS_COMPLETED,
                                        PAYROLL_STATUS_PROCESSING,
                                        PAYROLL_STATUS_PREPROCESSING])
                                )
                            and (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString<>PAYROLL_TYPE_IMPORT)
                            ;
    end;
    if (Field = nil) and (DM_PAYROLL.PR_SERVICES.Active)  then
      LoadBillingServices;
     rdgMarkAsPaid.Visible  := DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value = PAYROLL_TYPE_BACKDATED;

     TabSheet4.TabVisible := TypeIsCorrectionRun(DM_PAYROLL.PR.PAYROLL_TYPE.AsString)

       or ((DM_COMPANY.CO.PAYROLL_REQUIRES_MGR_APPROVAL.AsString = GROUP_BOX_YES) and
           ((DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_REGULAR) or
            (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_SETUP) or
            (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_IMPORT) or
            (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_MISC_CHECK_ADJUSTMENT)));

    if TabSheet4.TabVisible then
    begin
      grbxCombineRuns.Visible := DM_PAYROLL.PR.FieldByName('COMBINE_RUNS').Value = GROUP_BOX_YES;
      grbxReports.Visible := DM_PAYROLL.PR.FieldByName('PRINT_ALL_REPORTS').Value = GROUP_BOX_NO;

    end;
    if btnUnlockPR.Visible then
    begin
      btnUnlockPR.Enabled := (wwdsDetail.DataSet.State = dsBrowse) and (DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PROCESSED) or (DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PENDING);
      if DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PROCESSED then
        btnUnlockPR.Caption := 'Mark Processed Payroll as Waiting';
      if DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PENDING then
        btnUnlockPR.Caption := 'Mark Waiting Payroll as Processed';
    end;
  end;

end;

function TEDIT_PR.PayrollIsOutOfRange: Boolean;
var
  DaysPriorToCheckDate: Integer;
  AllNull: Boolean;
begin

  Result := False;

  if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_REGULAR then
    Exit;

  DaysPriorToCheckDate := 0;

  DM_COMPANY.CO.Locate('CO_NBR', DM_PAYROLL.PR.CO_NBR.Value, []);

  if not DM_COMPANY.CO.DAYS_PRIOR_TO_CHECK_DATE.IsNull then
  begin
    AllNull := False;
    DaysPriorToCheckDate := DM_COMPANY.CO.DAYS_PRIOR_TO_CHECK_DATE.AsInteger;
  end
  else
    AllNull := True;

  if AllNull and not DM_SERVICE_BUREAU.SB.DAYS_PRIOR_TO_CHECK_DATE.IsNull then
  begin
    AllNull := False;
    DaysPriorToCheckDate := DM_SERVICE_BUREAU.SB.DAYS_PRIOR_TO_CHECK_DATE.AsInteger;
  end;

  if not AllNull and (Trunc(Now) - Trunc(DM_PAYROLL.PR.CHECK_DATE.AsDateTime) >= DaysPriorToCheckDate) then
  begin
    Result := True;
  end;

end;

procedure TEDIT_PR.butnProcessClick(Sender: TObject);
   procedure PrStatusSetup;
   Begin
       if ( TypeIsManagerApproval(DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString)
                and (DM_COMPANY.CO.FieldByName('PAYROLL_REQUIRES_MGR_APPROVAL').Value = GROUP_BOX_YES)) then
        DM_PAYROLL.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_REQUIRED_MGR_APPROVAL
      else
        DM_PAYROLL.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_COMPLETED;
   end;
begin
  inherited;
  if not (DM_PAYROLL.PR.State in [dsInsert, dsEdit]) then
    DM_PAYROLL.PR.Edit;

  if TypeIsCorrectionRun(DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString) then
  begin
    DM_PAYROLL.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_HOLD;
  end
  else if (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_REGULAR) then
  begin

    if (ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_BACKDATED_PAYROLLS') <> stEnabled)
    and (Trunc(Now) - Trunc(DM_PAYROLL.PR.CHECK_DATE.AsDateTime) >= 0) then
      raise EInconsistentData.CreateHelp('You do not have the ability to submit a backdated payroll.', IDH_InconsistentData);

    if PayrollIsOutOfRange then
    begin
      if EvMessage('This payroll is out of range. If you proceed with this payroll it will need to be approved in the Operations-Approve Payroll screen. Do you wish to proceed?',
                 mtConfirmation, [mbYes, mbNo]) <> mrYes then
      begin
        DM_PAYROLL.PR.Cancel;
        Exit;
      end;

      DM_PAYROLL.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_BACKDATED;
    end
    else
     PrStatusSetup;
  end
  else PrStatusSetup;
end;

procedure TEDIT_PR.butnCopyClick(Sender: TObject);
var
  s: string;
begin
  inherited;
  if ctx_AccountRights.Functions.GetState('USER_CAN_COPY_PAYROLL') <> stEnabled then
    raise ENoRights.CreateHelp('You have no security rights', IDH_SecurityViolation);
  if EvMessage('Any voids or voided checks in the original payroll will be ignored' + #13 +
  'Are you sure you want to copy this payroll?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;
  with DM_PAYROLL do
  begin
    PR.CheckBrowseMode;
    PR_SCHEDULED_EVENT.DisableControls;
    PR.DisableControls;
    ctx_StartWait;
    try
      ctx_PayrollCalculation.CopyPayroll(PR.FieldByName('PR_NBR').Value);
      try
        if not DM_COMPANY.CO.Active then
           DM_COMPANY.CO.Open;
        PR.Close;
        PR.CheckDSCondition(GetDataSetConditions(GetDefaultDataSet.Name));
        ctx_DataAccess.OpenDataSets([PR]);

        s := PR.IndexName;
        PR.IndexFieldNames := 'PR_NBR';
        PR.FindNearest([MaxInt]);
      finally
        PR.IndexName := s;
        DM_PAYROLL.PR_SERVICES.Close;
        DM_PAYROLL.PR_SERVICES.Open;
      end;
    finally
      ctx_EndWait;
      PR.EnableControls;
      PR_SCHEDULED_EVENT.EnableControls;
    end;
  end;
end;

procedure TEDIT_PR.butnVoidClick(Sender: TObject);
var
  s: string;
begin
  inherited;
  if ctx_AccountRights.Functions.GetState('USER_CAN_VOID_PAYROLL') <> stEnabled then
    raise ENoRights.CreateHelp('You have no security rights', IDH_SecurityViolation);
  if DM_PAYROLL.PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED then
    raise ENoRights.CreateHelp('You can not void a voided payroll!', IDH_ConsistencyViolation);
  if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_REVERSAL then
    raise ENoRights.CreateHelp('You can not void a void payroll!', IDH_ConsistencyViolation);
  if DM_CLIENT.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_TAX_DEPOSIT then
    raise ENoRights.CreateHelp('Cannot void a Tax Deposit type payroll.', IDH_ConsistencyViolation);

  ctx_DataAccess.CheckCompanyLock(DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger, DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime);
  if EvMessage('Any voids or voided checks in the original payroll will be ignored.' + #13 +
  'Are you sure you want to void this payroll?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;

  with DM_PAYROLL do
  begin
    PR.CheckBrowseMode;
    PR_SCHEDULED_EVENT.DisableControls;
    PR.DisableControls;
    ctx_StartWait;
    try
      ctx_PayrollCalculation.VoidPayroll(PR.FieldByName('PR_NBR').Value);
      try
         PR.Close;
        PR.CheckDSCondition(GetDataSetConditions(GetDefaultDataSet.Name));
        ctx_DataAccess.OpenDataSets([PR]);
        
        s := PR.IndexName;
        PR.IndexFieldNames := 'PR_NBR';
        PR.FindNearest([MaxInt]);
      finally
        PR.IndexName := s;
      end;
    finally
      ctx_EndWait;
      PR.EnableControls;
      PR_SCHEDULED_EVENT.EnableControls;
    end;
  end;
end;

procedure TEDIT_PR.butnPreProcessClick(Sender: TObject);
var
  N, I: Integer;
  t: IevPreprocessPayrollTask;
  Log: string;
begin
  inherited;
  if not TOBalanceCheck(ifrPreProcess, Self) then
    Exit;
  if EvMessage('Are you sure you want to pre-process this payroll?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  ctx_StartWait;
  DM_PAYROLL.PR.DisableControls;
  try
    CommitData;
    if chbxQueue.Checked then
    begin
      t := mb_TaskQueue.CreateTask(QUEUE_TASK_PREPROCESS_PAYROLL, True) as IevPreprocessPayrollTask;
      try
        t.PrList := IntToStr(ctx_DataAccess.ClientID) + ';' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString + ' ';
        t.Caption := 'Co:' + Trim(DM_COMPANY.CO.FieldbyName('CUSTOM_COMPANY_NUMBER').AsString)
          + ' Pr:' + DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString + '-' + DM_PAYROLL.PR.FieldByName('RUN_NUMBER').AsString;

{ redundant info
        t.CompanyName := DM_COMPANY.CO.FieldByName('NAME').AsString;
        t.CheckDate := DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime;
        t.RunNumber := DM_PAYROLL.PR.FieldByName('RUN_NUMBER').AsInteger;
        t.CoNbr := DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger;
}
        ctx_EvolutionGUI.AddTaskQueue(t);
      finally
        t := nil;
      end;

   end
    else
    begin
      N := mb_AppSettings.AsInteger['Settings\TimesToTest'];
      if N = 0 then
        N := 1;
      for I := 1 to N do
        ctx_PayrollProcessing.ProcessPayroll(DM_PAYROLL.PR.FieldByName('PR_NBR').Value, True, Log, False);
    end;
  finally
    DM_PAYROLL.PR.EnableControls;
    ctx_EndWait;
    TabSheet2.SetFocus;
  end;
end;

procedure TEDIT_PR.butnInvoiceClick(Sender: TObject);
begin
  inherited;
  if (DM_PAYROLL.PR.FieldByName('INVOICE_PRINTED').AsString = INVOICE_PRINT_STATUS_Pending) then
  begin
    if EvMessage('Are you sure you want to print the invoice for this payroll?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
      Exit;
  end
  else if EvMessage('Invoice was already printed for this payroll. Do you want to print another one?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;

  ctx_PayrollProcessing.ProcessBilling(
    DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger,
    TRUE,
    -1,
    FALSE);
  DM_COMPANY.CO_BILLING_HISTORY.Close;
  DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Close;
end;

procedure TEDIT_PR.butnTaxAdjustmentsClick(Sender: TObject);
begin
  inherited;
  ctx_DataAccess.CheckCompanyLock(DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger, DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsDateTime);
  AdjustTaxes := TAdjustTaxes.Create(Self);
  try
    AdjustTaxes.DoTaxAdjustments;
  finally
    AdjustTaxes.Free;
  end;
end;

procedure TEDIT_PR.butnPAMonthlyCleanupClick(Sender: TObject);
var
  PAMCParams: TPAMCParams;
  t: IevPAMCTask;
  EECustomNbr: String;
begin

  if EvMessage('Are you sure you want to create a PA monthly cleanup payroll?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;

  PAMCParams := TPAMCParams.Create(nil);
  try
    if PAMCParams.ShowModal <> mrOK then
      Exit;

    if PAMCParams.cbAllEmployees.Checked then
      EECustomNbr := ''
    else
      EECustomNbr := PAMCParams.editEmployees.Text;

    ctx_StartWait;
    try
      t := mb_TaskQueue.CreateTask(QUEUE_TASK_PAMC, True) as IevPAMCTask;
      t.ClNbr := ctx_DataAccess.ClientID;
      t.CoNbr := DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger;
      with t.Filter.Add do
      begin
        ClNbr := ctx_DataAccess.ClientID;
        CoNbr := DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger;
        Caption := 'Co #' + Trim(DM_COMPANY.CO.FieldbyName('CUSTOM_COMPANY_NUMBER').AsString)
                      + ' Month End Date: ' + DateToStr(PAMCParams.dtMonthEndDate.DateTime);
      end;
      t.MonthEndDate := PAMCParams.dtMonthEndDate.DateTime;
      t.MinTax := StrToFloat(PAMCParams.editMinTax.Text);
      t.AutoProcess := PAMCParams.cbProcess.Checked;
      t.EECustomNbr := EECustomNbr;
      t.PrintReports := PAMCParams.cbPrintReports.Checked;
      t.Caption := 'Co #' + Trim(DM_COMPANY.CO.FieldbyName('CUSTOM_COMPANY_NUMBER').AsString)
        + ' Month End Date: ' + DateToStr(PAMCParams.dtMonthEndDate.DateTime);
      ctx_EvolutionGUI.AddTaskQueue(t);

    finally
      ctx_EndWait;
    end;
  finally
    PAMCParams.Free;
  end;
end;

procedure TEDIT_PR.butnQtrEndCleanupPRClick(Sender: TObject);
var
  ReportFilePath, EECustomNbr: string;
  QuarterDate: TDateTime;
  MinTax: Double;
  CleanupAction: TCleanupPayrollAction;
  ForceCleanup, PrintReports, CurrentQuarterOnly: Boolean;
  t: IevQECTask;
  V: Variant;
  Warnings: String;
begin
  inherited;
  if EvMessage('Are you sure you want to create Quarter End Cleanup payroll?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;

  QECParameters := TQECParameters.Create(Nil);
  try
    if QECParameters.ShowModal <> mrOK then
      Exit;
    ForceCleanup := QECParameters.cbForceCleanup.Checked;
    if QECParameters.cbProcess.Checked then
      CleanupAction := cpaProcess
    else
    if QECParameters.cbSendToQueue.Checked then
      CleanupAction := cpaQueue
    else
      CleanupAction := cpaNone;
    PrintReports := QECParameters.cbPrintReports.Checked;
    CurrentQuarterOnly := QECParameters.cbCurrentQuarterOnly.Checked;
    if QECParameters.cbAllEmployees.Checked then
      EECustomNbr := ''
    else
      EECustomNbr := QECParameters.editEmployees.Text;
    QuarterDate := QECParameters.dtQuarterEndDate.Date;
    MinTax := StrToFloat(QECParameters.editMinTax.Text);
    V := VarArrayOf([
      BoolToYN(QECParameters.cbFederal.Checked),
      BoolToYN(QECParameters.cbEEOASDI.Checked),
      BoolToYN(QECParameters.cbEROASDI.Checked),
      BoolToYN(QECParameters.cbEEMedicare.Checked),
      BoolToYN(QECParameters.cbERMedicare.Checked),
      BoolToYN(QECParameters.cbERFUI.Checked),
      BoolToYN(QECParameters.cbState.Checked),
      BoolToYN(QECParameters.cbEESDI.Checked),
      BoolToYN(QECParameters.cbERSDI.Checked),
      BoolToYN(QECParameters.cbSUI.Checked),
      BoolToYN(QECParameters.cbLocal.Checked),
      BoolToYN(QECParameters.cbQtrEndCheckDate.Checked),
      BoolToYN(False),
      BoolToYN(QECParameters.MovePaAmounts.Checked)]);
  finally
    QECParameters.Free;
  end;

  if chbxQueue.Checked then
  begin
    t := mb_TaskQueue.CreateTask(QUEUE_TASK_QEC, True) as IevQECTask;

    t.ClNbr := ctx_DataAccess.ClientID;
    t.CoNbr := DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger;
    with t.Filter.Add do
    begin
      ClNbr := ctx_DataAccess.ClientID;
      CoNbr := DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger;
      Caption := 'Co #' + Trim(DM_COMPANY.CO.FieldbyName('CUSTOM_COMPANY_NUMBER').AsString)
                    + ' Quarter End Date: ' + DateToStr(QuarterDate);
    end;
    t.QTREndDate := QuarterDate;
    t.MinTax := MinTax;
    t.CleanupAction := Ord(CleanupAction);
    t.EECustomNbr := EECustomNbr;
    t.ForceCleanup := ForceCleanup;
    t.PrintReports := PrintReports;
    t.CurrentQuarterOnly := CurrentQuarterOnly;
    t.ExtraOptions := VarToStr(V[0]) + VarToStr(V[1]) + VarToStr(V[2]) + VarToStr(V[3]) + VarToStr(V[4])
      + VarToStr(V[5]) + VarToStr(V[6]) + VarToStr(V[7]) + VarToStr(V[8]) + VarToStr(V[9]) + VarToStr(V[10])
      + VarToStr(V[11]) + 'N' + VarToStr(V[13]);
    t.Caption := 'Co #' + Trim(DM_COMPANY.CO.FieldbyName('CUSTOM_COMPANY_NUMBER').AsString)
      + ' Quarter End Date: ' + DateToStr(QuarterDate);
    ctx_EvolutionGUI.AddTaskQueue(t);
  end
  else
  begin
    ReportFilePath := mb_AppSettings['QuaterEndCleanupReportPath'];
    ReportFilePath := RunFolderDialog('Please, select Report File Path', ReportFilePath);
    mb_AppSettings['QuaterEndCleanupReportPath'] := ReportFilePath;
    if not ctx_PayrollProcessing.DoQuarterEndCleanup(DM_PAYROLL.PR.FieldByName('CO_NBR').AsInteger, QuarterDate, MinTax, Ord(CleanupAction), ReportFilePath, EECustomNbr, ForceCleanup, PrintReports, CurrentQuarterOnly, rlTryOnce, V, Warnings) then
    begin
      if CleanupAction = cpaProcess then
        EvMessage('Quarter End Cleanup payroll was not processed due to pending EE tax adjustments.');
      if CleanupAction = cpaQueue then
        EvMessage('Quarter End Cleanup payroll was not sent to queue due to pending EE tax adjustments.');
    end;
  end;
end;

procedure TEDIT_PR.btnCheckReconClick(Sender: TObject);
begin
  inherited;
  with TPayrollAccountRegister.Create(nil) do
  try
    FPayrollNbr := DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger;
    ShowModal;
  finally
    Free;
  end;
end;

procedure TEDIT_PR.butnAdjustClick(Sender: TObject);
begin
  inherited;
  ctx_StartWait;
  try
    Change_CheckLine_Status := TChange_CheckLine_Status.Create(nil);
  finally
    ctx_EndWait;
  end;

  try
    Change_CheckLine_Status.ShowModal;
  finally
    Change_CheckLine_Status.Free;
  end;
end;

procedure TEDIT_PR.butnProrateFICAClick(Sender: TObject);
var
  ProrateRatio: Double;
begin
  inherited;
  if EvMessage('Are you sure you want to prorate FICA for this payroll?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  ctx_StartWait;
  try
    DM_SYSTEM.SY_FED_TAX_TABLE.AsOfDate := DM_PAYROLL.PR.FieldByName('CHECK_DATE').Value;
    DM_SYSTEM.SY_FED_TAX_TABLE.Open;
    ProrateRatio := DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value / (DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('EE_OASDI_RATE').Value + DM_SYSTEM.SY_FED_TAX_TABLE.FieldByName('MEDICARE_RATE').Value);
    DM_PAYROLL.PR_CHECK.DataRequired('PR_NBR=' + DM_PAYROLL.PR.FieldByName('PR_NBR').AsString);
    DM_PAYROLL.PR_CHECK.First;
    while not DM_PAYROLL.PR_CHECK.EOF do
    begin
      if DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').IsNull and not DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').IsNull then
      begin
        DM_PAYROLL.PR_CHECK.Edit;
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value := DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value * ProrateRatio;
        DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value := DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_OASDI').Value - DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value;
        DM_PAYROLL.PR_CHECK.Post;
      end;
      DM_PAYROLL.PR_CHECK.Next;
    end;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK]);
  finally
    ctx_EndWait;
  end;
end;

function TEDIT_PR.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR;
end;

function TEDIT_PR.GetInsertControl: TWinControl;
begin
  Result := evDBSpinEdit1;
end;

procedure TEDIT_PR.AfterClick(Kind: Integer);
begin
  inherited;


  grbxReports.Visible := evDBRadioGroup5.ItemIndex = 1;
  if grbxReports.Visible then
   Begin
    dsPRReports.MasterDataSource := wwdsDetail;
    grReports.DataSource := dsPRReports;
    wwlcReport.DataSource := dsPRReports;
    wwlcReport.LookupTable := DM_CLIENT.CO_REPORTS;
   end;


  if Kind = NavInsert then
  begin
    Generic_PopulatePRRecord(
      DM_CLIENT.CL.FieldByName('CL_NBR').AsInteger,
      DM_COMPANY.CO.FieldByName('CO_NBR').AsInteger,
      DM_COMPANY.CO.FieldByName('NATURE_OF_BUSINESS').AsString);
    if DM_PAYROLL.PR.FieldByName('EXCLUDE_ACH').Value = 'Y' then
    begin
       DM_PAYROLL.PR.FieldByName('TRUST_IMPOUND').Value := PAYMENT_TYPE_NONE;
       DM_PAYROLL.PR.FieldByName('TAX_IMPOUND').Value := PAYMENT_TYPE_NONE;
       DM_PAYROLL.PR.FieldByName('DD_IMPOUND').Value := PAYMENT_TYPE_NONE;
       DM_PAYROLL.PR.FieldByName('BILLING_IMPOUND').Value := PAYMENT_TYPE_NONE;
       DM_PAYROLL.PR.FieldByName('WC_IMPOUND').Value := PAYMENT_TYPE_NONE;

    end else
    begin
      DM_PAYROLL.PR.TAX_IMPOUND.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
      DM_PAYROLL.PR.TRUST_IMPOUND.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
      DM_PAYROLL.PR.BILLING_IMPOUND.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
      DM_PAYROLL.PR.DD_IMPOUND.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
      DM_PAYROLL.PR.WC_IMPOUND.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
    end;
  end;
  if (Kind = NavDelete) and WasVoidPayroll then
  begin
    DM_PAYROLL.PR.Close;
    DM_PAYROLL.PR.Open;
    WasVoidPayroll := False;
  end;
  if (Kind = NavAbort) and (DeletedPrNbr <> 0) then
    DM_PAYROLL.PR.Locate('PR_NBR', DeletedPrNbr, []);
end;

procedure TEDIT_PR.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin

  AddDS(DM_PAYROLL.PR_SCHEDULED_EVENT, EditDataSets);
  AddDS(DM_PAYROLL.PR_REPORTS, EditDataSets);
  AddDS(DM_PAYROLL.PR_SERVICES, EditDataSets);
  AddDS(DM_COMPANY.CO_SERVICES, SupportDataSets);
  inherited;


end;

procedure TEDIT_PR.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  s: String;
  ACHFlag: IevGlobalFlagInfo;

  function isCoPrAchHasBARRecords:boolean;
  var
    Q: IevQuery;
    s:string;
  begin
    s:= 'select r.* from CO_BANK_ACCOUNT_REGISTER r, CO_PR_ACH c ' +
        'where {AsOfNow<c>} and {AsOfNow<r>} and ' +
        'c.pr_nbr = :PrNbr and r.CO_PR_ACH_nbr = c.CO_PR_ACH_nbr ';
    Q := TevQuery.Create(s);
    Q.Params.AddValue('PrNbr', DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger);
    Q.Execute;
    result := Q.Result.RecordCount > 0;
  end;
begin
  inherited;
  WasVoidPayroll := False;

  if (Kind = NavOK) then
  begin
     SaveBillingServices;
     if(DM_PAYROLL.PR.State in [dsInsert, dsEdit]) then
     begin
       if (DM_PAYROLL.PR.State = dsEdit) and
         (VarCompareValue(DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').OldValue, PAYROLL_TYPE_IMPORT) = vrEqual) and
         (VarCompareValue(DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').OldValue, DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').NewValue) <> vrEqual) then
         raise EInconsistentData.CreateHelp('You can not change a payroll from type import to anything else.', IDH_InconsistentData);

       if (DM_PAYROLL.PR.State = dsInsert) and (wwDBComboBox1.Value <> '') and (wwDBComboBox1.Value[1] in [PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT, PAYROLL_TYPE_IMPORT, PAYROLL_TYPE_TAX_ADJUSTMENT]) then
          raise EInconsistentData.CreateHelp('You can not create a payroll of this type manually.', IDH_InconsistentData);
       try
         DM_PAYROLL.PR_SCHEDULED_EVENT.DisableControls;
         DM_PAYROLL.PR.Post;
       finally
         DM_PAYROLL.PR_SCHEDULED_EVENT.EnableControls;
       end;
     end;
  end;
  if Kind = NavCancel then
  begin
    LoadBillingServices;
  end;
  if Kind = NavDelete then
  begin
    Handled := True;
    if DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED] then
    begin
     // Make sure there is no ACH being processed
      ACHFlag := mb_GlobalFlagsManager.GetLockedFlag(GF_SB_ACH_PROCESS);
      if Assigned(ACHFlag) then
      begin
        EvMessage('Cannot delete processed payrolls while ACH is in progress.');
        Exit;
      end;
    end;

    s:='';
    if DM_COMPANY.CO.FieldByName('FEDERAL_TAX_DEPOSIT_FREQUENCY').AsString = FREQUENCY_TYPE_SEMI_WEEKLY then
       s := 'Be sure to review your due dates and tax deposit frequencies if threshold limits have already been met.'+ #13;

    if (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED])
    and (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_REVERSAL) then
      s := s + 'You are about to delete processed payroll. It will not back out payroll information.' + #13
             + 'If you would like to back out payroll information, you should void the payroll. Do you still want to delete this payroll?'
    else
    if DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString = PAYROLL_TYPE_REVERSAL then
    begin
      s := s + 'You are about to delete a void payroll. Are you sure?';
      WasVoidPayroll := True;
    end
    else
    if ((DM_PAYROLL.PR.FieldByName('STATUS').AsString=PAYROLL_STATUS_COMPLETED)
         or (DM_PAYROLL.PR.FieldByName('STATUS').AsString=PAYROLL_STATUS_PROCESSING)) then
    begin
      EvMessage('PR is in Process, cannot delete the PR.');
      Exit;
    end
    else
      s := 'Are you sure?';

    if isCoPrAchHasBARRecords then
    begin
      EvMessage('Payroll has Bank Account Register Records. Please delete them first.');
      Exit;
    end;

    if EvMessage(s, mtConfirmation, mbOKCancel) = mrOK then
    begin
      DeletedPrNbr := DM_PAYROLL.PR.FieldByName('PR_NBR').Value;
      try
        ctx_PayrollProcessing.DeletePayroll(DeletedPrNbr);
      except
        DM_PAYROLL.PR.Close;
        DM_PAYROLL.PR.CheckDSCondition(GetDataSetConditions(GetDefaultDataSet.Name));
        ctx_DataAccess.OpenDataSets([DM_PAYROLL.PR]);
        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Close;
        DM_PAYROLL.PR_SCHEDULED_EVENT.Close;
        DM_PAYROLL.PR_SCHEDULED_EVENT.Open;
        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Open;
        raise;
      end;
      try
        DM_PAYROLL.PR.Close;
        DM_PAYROLL.PR.CheckDSCondition(GetDataSetConditions(GetDefaultDataSet.Name));
        ctx_DataAccess.OpenDataSets([DM_PAYROLL.PR]);
        // refresh Scheduled Events after delete should make its check date available for a new payroll
        // on the Payroll - Payroll screen, Popup scheduled check date list (by Ctrl+Z)
        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Close;
        DM_PAYROLL.PR_SCHEDULED_EVENT.Close;
        DM_PAYROLL.PR_SCHEDULED_EVENT.Open;
        DM_PAYROLL.PR_SCHEDULED_EVENT_BATCH.Open;

        s := DM_PAYROLL.PR.IndexName;
        DM_PAYROLL.PR.IndexFieldNames := 'PR_NBR';
        DM_PAYROLL.PR.FindNearest([DeletedPrNbr]);
      finally
        DM_PAYROLL.PR.IndexName := s;
      end;
    end
    else
      WasVoidPayroll := False;
    DeletedPrNbr := 0;
  end;
end;

procedure TEDIT_PR.wwDBComboBox1Change(Sender: TObject);
begin
  inherited;
//Issue#49987
  wwDBComboBox1.DataLink.OnActiveChange(sender);
  if ( DM_PAYROLL.PR.State in [dsInsert, dsEdit] ) then
  begin
     if(wwDBComboBox1.value = PAYROLL_TYPE_BACKDATED)
     then DM_PAYROLL.PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value :=
                DM_SERVICE_BUREAU.SB.FieldByName('MARK_LIABS_PAID_DEFAULT').Value
     else DM_PAYROLL.PR.FieldByName('MARK_LIABS_PAID_DEFAULT').Value := GROUP_BOX_No; //default
  end;

  if DM_PAYROLL.PR.FieldByName('STATUS').Value = PAYROLL_STATUS_PENDING then
  begin
   if (DM_PAYROLL.PR.State = dsEdit) and (wwDBComboBox1.Value <> '') and (wwDBComboBox1.Value[1] in [PAYROLL_TYPE_IMPORT, PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT, PAYROLL_TYPE_TAX_DEPOSIT, PAYROLL_TYPE_TAX_ADJUSTMENT]) then
   begin
     wwDBComboBox1.Text := wwDBComboBox1.GetComboDisplay(DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString);
     wwDBComboBox1.Refresh;
     raise EInconsistentData.CreateHelp('You can not create a payroll of this type manually.', IDH_InconsistentData);
   end;
  end;
end;

procedure TEDIT_PR.butnUnlockClick(Sender: TObject);
var
  FlagInfo: IevGlobalFlagInfo;
begin
  inherited;
  ctx_StartWait;
  try
    if not mb_GlobalFlagsManager.TryUnlock(GF_CO_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID) + '_' + DM_PAYROLL.PR.CO_NBR.AsString, FlagInfo) then
      raise ELockedResource.CreateHelp('A payroll for this company is locked by ' + FlagInfo.UserName + '. It needs to be unlocked by the same user.', IDH_LockedResource);

    if not mb_GlobalFlagsManager.TryUnlock(GF_CL_PAYROLL_OPERATION, IntToStr(ctx_DataAccess.ClientID), FlagInfo) then
      raise ELockedResource.CreateHelp('A payroll for this client is locked by ' + FlagInfo.UserName + '. It needs to be unlocked by the same user.', IDH_LockedResource);
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_PR.Activate;
begin
  inherited;

  FBeforePost := DM_PAYROLL.PR.BeforePost;
  DM_PAYROLL.PR.BeforePost := BeforePost;

  MiniNavigationFrame.DataSource := dsPRReports;
  MiniNavigationFrame.InsertFocusControl := wwlcReport;

  dsPRReports.MasterDataSource := wwdsDetail;
  grReports.DataSource := dsPRReports;
  wwlcReport.DataSource := dsPRReports;
  wwlcReport.LookupTable := DM_CLIENT.CO_REPORTS;

  DeletedPrNbr := 0;
//  btnUnlockPR.Visible := IsDebugMode;
  btnUnlockPR.Visible := False;
 wwdsDetail.OnDataChange := wwdsDataChange;
 FSaveAfterScroll := wwdsDetail.DataSet.AfterScroll;
 wwdsDetail.DataSet.AfterScroll := PayrollAfterScroll;
 wwdsDetail.DataSet.First;
 CheckButtonState;
end;

function TEDIT_PR.GetDataSetConditions(sName: string): string;
begin
  if sName = 'PR_REPORTS' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_PR.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_PAYROLL.PR_REPORTS, aDS);
  AddDS(DM_COMPANY.CO_REPORTS, aDS);
  AddDS(DM_COMPANY.CO_SERVICES, aDS);
  AddDS(DM_PAYROLL.PR_SERVICES, aDS);

  inherited;
end;

procedure TEDIT_PR.evDBRadioGroup4Change(Sender: TObject);
begin
  inherited;
  grbxCombineRuns.Visible := evDBRadioGroup4.ItemIndex = 0;
end;

procedure TEDIT_PR.evDBRadioGroup5Change(Sender: TObject);
begin
  inherited;

  grbxReports.Visible := evDBRadioGroup5.ItemIndex = 1;
  if (grbxReports.Visible) and
  (MiniNavigationFrame.DataSource<> nil) then
   Begin
    dsPRReports.MasterDataSource := wwdsDetail;
    grReports.DataSource := dsPRReports;
    wwlcReport.DataSource := dsPRReports;
    wwlcReport.LookupTable := DM_CLIENT.CO_REPORTS;
   end;

end;

procedure TEDIT_PR.bbtnPrintClick(Sender: TObject);
var
  ReportParams: TrwReportParams;
begin
  inherited;
  if DM_PAYROLL.PR.State in [dsInsert, dsEdit] then
    DM_PAYROLL.PR.Post;
  ReportParams := TrwReportParams.Create;
  try
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('ALL');
    if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'Correction Run Form', [loCaseInsensitive]) then
      raise EMissingReport.CreateHelp('"Correction Run Form" report does not exist in the database!', IDH_MissingReport);

    ReportParams.Add('Clients', varArrayOf([ctx_DataAccess.ClientID]));
    ReportParams.Add('Payrolls',  varArrayOf([DM_PAYROLL.PR.FieldByName('PR_NBR').AsInteger]));

    ctx_RWLocalEngine.StartGroup;
    try
      ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS['SY_REPORT_WRITER_REPORTS_NBR'],
        CH_DATABASE_SYSTEM, ReportParams);
    finally
      ctx_RWLocalEngine.EndGroup(rdtPrinter);
    end;
  finally
    ReportParams.Free;
  end;
end;

procedure TEDIT_PR.wwdsDetailStateChange(Sender: TObject);
begin
  if (csLoading in ComponentState) then
    exit;
  if (wwdsDetail.Active) and wwdsDetail.DataSet.Active and ( not ctx_DataAccess.GetClCoReadOnlyRight) then
  begin
    btnCheckRecon.Enabled := (wwdsDetail.DataSet.State = dsBrowse);
    butnCopy.Enabled := (wwdsDetail.DataSet.State = dsBrowse);
    CheckButtonState;
    butnUnlock.Enabled := (wwdsDetail.DataSet.State = dsBrowse);
    butnTaxAdjustments.Enabled := (wwdsDetail.DataSet.State = dsBrowse);
    butnQtrEndCleanupPR.Enabled := (wwdsDetail.DataSet.State = dsBrowse);
    butnPAMonthlyCleanup.Enabled := (wwdsDetail.DataSet.State = dsBrowse);
  end;
end;

procedure TEDIT_PR.btnQTDYTDClick(Sender: TObject);

  procedure CopyRecord(Source, Dest: TevClientDataSet);
  var
    i: Integer;
    f: TField;
  begin
    Dest.Append;
    for i := 0 to Pred(Source.FieldCount) do
    begin
      f := Dest.FindField(Source.Fields[i].FieldName);
      if Assigned(f) and (f.FieldName <> Dest.Name + '_NBR') then
        f.Assign(Source.Fields[i]);
    end;
  end;

var
  PR_CHECK_2, PR_CHECK_LINES_2, PR_CHECK_STATES_2, PR_CHECK_SUI_2, PR_CHECK_LOCALS_2: TevClientDataSet;
begin
  inherited;
  if EvMessage('Are you sure you want to subtract QTD checks from YTD checks for this payroll and post the results into QTD checks?', mtConfirmation, [mbYes, mbNo]) = mrNo then
    Exit;
  ctx_StartWait;
  PR_CHECK_2 := TevClientDataSet.Create(Nil);
  PR_CHECK_LINES_2 := TevClientDataSet.Create(Nil);
  PR_CHECK_STATES_2 := TevClientDataSet.Create(Nil);
  PR_CHECK_SUI_2 := TevClientDataSet.Create(Nil);
  PR_CHECK_LOCALS_2 := TevClientDataSet.Create(Nil);
  with DM_PAYROLL do
  try
    PR_CHECK.DataRequired('PR_NBR=' + PR.FieldByName('PR_NBR').AsString);
    PR_CHECK_LINES.DataRequired('PR_NBR=' + PR.FieldByName('PR_NBR').AsString);
    PR_CHECK_STATES.DataRequired('PR_NBR=' + PR.FieldByName('PR_NBR').AsString);
    PR_CHECK_SUI.DataRequired('PR_NBR=' + PR.FieldByName('PR_NBR').AsString);
    PR_CHECK_LOCALS.DataRequired('PR_NBR=' + PR.FieldByName('PR_NBR').AsString);
    PR_CHECK_2.Data := PR_CHECK.Data;
    PR_CHECK_2.First;
    while not PR_CHECK_2.EOF do
    begin
      if PR_CHECK_2.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_YTD then
      begin
        if not PR_CHECK.Locate('EE_NBR;CHECK_TYPE', VarArrayOf([PR_CHECK_2.FieldByName('EE_NBR').Value, CHECK_TYPE2_QTD]), []) then
        begin
          CopyRecord(PR_CHECK_2, PR_CHECK);
          PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_QTD;
          PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value := ctx_PayrollCalculation.GetNextPaymentSerialNbr;
          PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value := PR_CHECK_2.FieldByName('OR_CHECK_FEDERAL_VALUE').Value;
          PR_CHECK.FieldByName('OR_CHECK_OASDI').Value := PR_CHECK_2.FieldByName('OR_CHECK_OASDI').Value;
          PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value := PR_CHECK_2.FieldByName('OR_CHECK_MEDICARE').Value;
          PR_CHECK.FieldByName('OR_CHECK_EIC').Value := PR_CHECK_2.FieldByName('OR_CHECK_EIC').Value;
          PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value := PR_CHECK_2.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value;
        end
        else
        begin
          PR_CHECK.Edit;
          PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value := PR_CHECK_2.FieldByName('OR_CHECK_FEDERAL_VALUE').Value -
            ConvertNull(PR_CHECK.FieldByName('OR_CHECK_FEDERAL_VALUE').Value, 0);
          PR_CHECK.FieldByName('OR_CHECK_OASDI').Value := PR_CHECK_2.FieldByName('OR_CHECK_OASDI').Value -
            ConvertNull(PR_CHECK.FieldByName('OR_CHECK_OASDI').Value, 0);
          PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value := PR_CHECK_2.FieldByName('OR_CHECK_MEDICARE').Value -
            ConvertNull(PR_CHECK.FieldByName('OR_CHECK_MEDICARE').Value, 0);
          PR_CHECK.FieldByName('OR_CHECK_EIC').Value := PR_CHECK_2.FieldByName('OR_CHECK_EIC').Value -
            ConvertNull(PR_CHECK.FieldByName('OR_CHECK_EIC').Value, 0);
          PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value := PR_CHECK_2.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value -
            ConvertNull(PR_CHECK.FieldByName('OR_CHECK_BACK_UP_WITHHOLDING').Value, 0);
        end;
        PR_CHECK.Post;

        with PR_CHECK_LINES_2 do
        begin
          Data := PR_CHECK_LINES.Data;
          Filter := 'PR_CHECK_NBR=' + PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString;
          Filtered := True;
          PR_CHECK_LINES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
          PR_CHECK_LINES.Filtered := True;
          try
            First;
            while not EOF do
            begin
              if not PR_CHECK_LINES.Locate('CL_E_DS_NBR', FieldByName('CL_E_DS_NBR').Value, []) then
              begin
                CopyRecord(PR_CHECK_LINES_2, PR_CHECK_LINES);
                PR_CHECK_LINES.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
                PR_CHECK_LINES.FieldByName('AMOUNT').Value := FieldByName('AMOUNT').Value;
                PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value := FieldByName('HOURS_OR_PIECES').Value;
              end
              else
              begin
                PR_CHECK_LINES.Edit;
                PR_CHECK_LINES.FieldByName('AMOUNT').Value := ConvertNull(FieldByName('AMOUNT').Value, 0) -
                  ConvertNull(PR_CHECK_LINES.FieldByName('AMOUNT').Value, 0);
                PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value := ConvertNull(FieldByName('HOURS_OR_PIECES').Value, 0) -
                  ConvertNull(PR_CHECK_LINES.FieldByName('HOURS_OR_PIECES').Value, 0);
              end;
              PR_CHECK_LINES.Post;
              Next;
            end;
          finally
            Filtered := False;
            Filter := '';
            PR_CHECK_LINES.Filtered := False;
            PR_CHECK_LINES.Filter := '';
          end;
        end;

        with PR_CHECK_STATES_2 do
        begin
          Data := PR_CHECK_STATES.Data;
          Filter := 'PR_CHECK_NBR=' + PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString;
          Filtered := True;
          PR_CHECK_STATES.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
          PR_CHECK_STATES.Filtered := True;
          try
            First;
            while not EOF do
            begin
              if not PR_CHECK_STATES.Locate('EE_STATES_NBR', FieldByName('EE_STATES_NBR').Value, []) then
              begin
                CopyRecord(PR_CHECK_STATES_2, PR_CHECK_STATES);
                PR_CHECK_STATES.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
                PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value := FieldByName('STATE_OVERRIDE_VALUE').Value;
                PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').Value := FieldByName('EE_SDI_OVERRIDE').Value;
              end
              else
              begin
                PR_CHECK_STATES.Edit;
                PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value := FieldByName('STATE_OVERRIDE_VALUE').Value -
                  ConvertNull(PR_CHECK_STATES.FieldByName('STATE_OVERRIDE_VALUE').Value, 0);
                PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').Value := FieldByName('EE_SDI_OVERRIDE').Value -
                  ConvertNull(PR_CHECK_STATES.FieldByName('EE_SDI_OVERRIDE').Value, 0);
              end;
              PR_CHECK_STATES.Post;
              Next;
            end;
          finally
            Filtered := False;
            Filter := '';
            PR_CHECK_STATES.Filtered := False;
            PR_CHECK_STATES.Filter := '';
          end;
        end;

        with PR_CHECK_SUI_2 do
        begin
          Data := PR_CHECK_SUI.Data;
          Filter := 'PR_CHECK_NBR=' + PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString;
          Filtered := True;
          PR_CHECK_SUI.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
          PR_CHECK_SUI.Filtered := True;
          try
            First;
            while not EOF do
            begin
              if not PR_CHECK_SUI.Locate('CO_SUI_NBR', FieldByName('CO_SUI_NBR').Value, []) then
              begin
                CopyRecord(PR_CHECK_SUI_2, PR_CHECK_SUI);
                PR_CHECK_SUI.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
                PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value := FieldByName('OVERRIDE_AMOUNT').Value;
              end
              else
              begin
                PR_CHECK_SUI.Edit;
                PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value := FieldByName('OVERRIDE_AMOUNT').Value -
                  ConvertNull(PR_CHECK_SUI.FieldByName('OVERRIDE_AMOUNT').Value, 0);
              end;
              PR_CHECK_SUI.Post;
              Next;
            end;
          finally
            Filtered := False;
            Filter := '';
            PR_CHECK_SUI.Filtered := False;
            PR_CHECK_SUI.Filter := '';
          end;
        end;

        with PR_CHECK_LOCALS_2 do
        begin
          Data := PR_CHECK_LOCALS.Data;
          Filter := 'PR_CHECK_NBR=' + PR_CHECK_2.FieldByName('PR_CHECK_NBR').AsString;
          Filtered := True;
          PR_CHECK_LOCALS.Filter := 'PR_CHECK_NBR=' + PR_CHECK.FieldByName('PR_CHECK_NBR').AsString;
          PR_CHECK_LOCALS.Filtered := True;
          try
            First;
            while not EOF do
            begin
              if not PR_CHECK_LOCALS.Locate('EE_LOCALS_NBR', FieldByName('EE_LOCALS_NBR').Value, []) then
              begin
                CopyRecord(PR_CHECK_LOCALS_2, PR_CHECK_LOCALS);
                PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').Value := PR_CHECK.FieldByName('PR_CHECK_NBR').Value;
                PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Value := FieldByName('OVERRIDE_AMOUNT').Value;
              end
              else
              begin
                PR_CHECK_LOCALS.Edit;
                PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Value := FieldByName('OVERRIDE_AMOUNT').Value -
                  ConvertNull(PR_CHECK_LOCALS.FieldByName('OVERRIDE_AMOUNT').Value, 0);
              end;
              PR_CHECK_LOCALS.Post;
              Next;
            end;
          finally
            Filtered := False;
            Filter := '';
            PR_CHECK_LOCALS.Filtered := False;
            PR_CHECK_LOCALS.Filter := '';
          end;
        end;
      end;
      PR_CHECK_2.Next;
    end;
    ctx_DataAccess.PostDataSets([PR_CHECK, PR_CHECK_LINES, PR_CHECK_STATES, PR_CHECK_SUI, PR_CHECK_LOCALS]);
  finally
    PR_CHECK_2.Free;
    PR_CHECK_LINES_2.Free;
    PR_CHECK_STATES_2.Free;
    PR_CHECK_SUI_2.Free;
    PR_CHECK_LOCALS_2.Free;
    ctx_EndWait;
  end;
end;

procedure TEDIT_PR.btnUnlockPRClick(Sender: TObject);
var
  AllOK: Boolean;
begin
  inherited;
  if btnUnlockPR.Caption = 'Mark Processed Payroll as Waiting' then
  begin
    ctx_DataAccess.UnlockPR;
    AllOK := False;
    try
      DM_PAYROLL.PR.Edit;
      DM_PAYROLL.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PENDING;
      DM_PAYROLL.PR.Post;
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
      AllOK := True;
    finally
      ctx_DataAccess.LockPR(AllOK);
    end;
    btnUnlockPR.Caption := 'Mark Waiting Payroll as Processed';
  end
  else
  begin
    DM_PAYROLL.PR.Edit;
    DM_PAYROLL.PR.FieldByName('STATUS').Value := PAYROLL_STATUS_PROCESSED;
    DM_PAYROLL.PR.Post;
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR]);
    btnUnlockPR.Caption := 'Mark Processed Payroll as Waiting';
  end;
end;

procedure TEDIT_PR.FrameExit(Sender: TObject);
begin
  inherited;

   MiniNavigationFrame.DataSource := nil;
   dsPRReports.MasterDataSource := nil;
   grReports.DataSource := nil;
   wwlcReport.DataSource := nil;
   wwlcReport.LookupTable := nil;
 
end;



procedure TEDIT_PR.evDBGridServicesMultiSelectRecord(Grid: TwwDBGrid;
  Selecting: Boolean; var Accept: Boolean);
begin
  inherited;
  with DM_PAYROLL do
  if (PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED) or
     (PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED) then
  //if (PR.FieldByName('STATUS').AsString <> 'W') then
     Accept := False
  else
    MarkChanged;
end;

procedure TEDIT_PR.evDBGridServicesMultiSelectReverseRecords(
  Grid: TwwDBGrid; var Accept: Boolean);
begin
  inherited;
    with DM_PAYROLL do
  if (PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_PROCESSED) or
     (PR.FieldByName('STATUS').AsString = PAYROLL_STATUS_VOIDED) then
  //if (PR.FieldByName('STATUS').AsString <> 'W') then
     Accept := False
  else
    MarkChanged;
end;

procedure TEDIT_PR.LoadBillingServices;
var
  bm:TBookMark;
  cd:string;
begin

   if  (DM_PAYROLL.PR.FieldByName('PR_NBR').IsNull) or (PageControl1.ActivePage <> tsExclusions) or (DM_PAYROLL.PR_SERVICES.Tag = 1) then exit;
   cd := DM_PAYROLL.PR.FieldByName('CHECK_DATE').AsString;
   DM_COMPANY.CO_SERVICES.Filter := '(EFFECTIVE_START_DATE is null or EFFECTIVE_START_DATE <= '''+cd+''' ) and (EFFECTIVE_END_DATE is null or EFFECTIVE_END_DATE>='''+cd +''')';
   DM_COMPANY.CO_SERVICES.Filtered := true;
   if DM_PAYROLL.PR.FieldByName('EXCLUDE_BILLING').AsString <> 'C' then exit;
   DM_COMPANY.CO_SERVICES.DisableControls;
   try

     with DM_COMPANY.CO_SERVICES.ShadowDataSet do
     begin
       bm := GetBookmark;
       evDBGridServices.SelectedList.Clear;
       First;
       while not Eof do
       begin
         if DM_PAYROLL.PR_SERVICES.ShadowDataSet.Locate(
                     'PR_NBR;CO_SERVICES_NBR',
                     VarArrayOf([DM_PAYROLL.PR.FieldByName('PR_NBR').Value,FieldByName('CO_SERVICES_NBR').Value]),[]) then
             if DM_PAYROLL.PR_SERVICES['EXCLUDE'] = 'Y' then
                evDBGridServices.SelectedList.Add(GetBookMark);
         Next;
       end;
     GotoBookmark(bm);
     end;
   finally
     DM_COMPANY.CO_SERVICES.FreeBookmark(bm);
     DM_COMPANY.CO_SERVICES.EnableControls;
   end;
end;

procedure TEDIT_PR.MarkChanged;
begin
  DM_PAYROLL.PR_SERVICES.Edit;
end;

procedure TEDIT_PR.SaveBillingServices;
var
  bm:TBookMark;
begin
DM_COMPANY.CO_SERVICES.DisableControls;
   if DM_PAYROLL.PR.State <> dsBrowse then
     DM_PAYROLL.PR.UpdateRecord;
   try
     DM_PAYROLL.PR_SERVICES.Tag := 1;
     if DM_PAYROLL.PR.EXCLUDE_BILLING.AsString[1] <> 'C' then
        evDBGridServices.SelectedList.Clear;

     with DM_COMPANY.CO_SERVICES do
     begin
       bm := GetBookmark;
       First;
       while not Eof do
       begin

         if evDBGridServices.IsSelectedRecord  then
         begin
             if not  DM_PAYROLL.PR_SERVICES.Locate('PR_NBR;CO_SERVICES_NBR',VarArrayOf([DM_PAYROLL.PR.FieldByName('PR_NBR').Value,FieldByName('CO_SERVICES_NBR').Value]),[]) then
             begin
                 DM_PAYROLL.PR_SERVICES.Append;
                 DM_PAYROLL.PR_SERVICES['CO_SERVICES_NBR'] := FieldByName('CO_SERVICES_NBR').Value;
                 DM_PAYROLL.PR_SERVICES['EXCLUDE'] := 'Y';
                 DM_PAYROLL.PR_SERVICES.Post;
             end else if DM_PAYROLL.PR_SERVICES['EXCLUDE'] <> 'Y' then
             begin
                 DM_PAYROLL.PR_SERVICES.Edit;
                 DM_PAYROLL.PR_SERVICES['EXCLUDE'] := 'Y';
                 DM_PAYROLL.PR_SERVICES.Post;
             end;
         end else
         begin
            if DM_PAYROLL.PR_SERVICES.Locate('PR_NBR;CO_SERVICES_NBR',VarArrayOf([DM_PAYROLL.PR.FieldByName('PR_NBR').Value,FieldByName('CO_SERVICES_NBR').Value]),[]) then
            begin
                DM_PAYROLL.PR_SERVICES.Delete;
            end;
         end;
         Next;
       end;
     GotoBookmark(bm);
     end;
   finally
     DM_PAYROLL.PR_SERVICES.Tag := 0;
     DM_COMPANY.CO_SERVICES.FreeBookmark(bm);
     DM_COMPANY.CO_SERVICES.EnableControls;
   end;
end;


procedure TEDIT_PR.DBRadioGroup6Click(Sender: TObject);
begin
  inherited;
  gbServices.Visible := DBRadioGroup6.Value = GROUP_BOX_CONDITIONAL; //DM_CLIENT.PR.EXCLUDE_BILLING.AsString
end;

procedure TEDIT_PR.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = tsExclusions then
  begin
    LoadBillingServices;
    UpdatePaymentTypes;
  end;
end;

procedure TEDIT_PR.BeforePost(DataSet: TDataSet);
begin
  if (DM_PAYROLL.PR.State in [dsInsert])
  and (DM_PAYROLL.PR.PAYROLL_TYPE.AsString = PAYROLL_TYPE_REGULAR) then
  begin
    if (ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_BACKDATED_PAYROLLS') <> stEnabled)
    and (Trunc(Now) - Trunc(DM_PAYROLL.PR.CHECK_DATE.AsDateTime) >= 0) then
      raise EInconsistentData.CreateHelp('You do not have the ability to create a backdated payroll.', IDH_InconsistentData);

    if PayrollIsOutOfRange then
    begin
      if (ctx_AccountRights.Functions.GetState('USER_CAN_CREATE_BACKDATED_PAYROLLS') <> stEnabled) then
        raise EInconsistentData.CreateHelp('You can not create a backdated payroll.', IDH_InconsistentData);

      if EvMessage('This payroll is out of range. If you proceed with this payroll it will need to be approved in the Operations-Approve Payroll screen. Do you wish to proceed?',
                 mtConfirmation, [mbYes, mbNo]) <> mrYes then
      begin
        DM_PAYROLL.PR.Cancel;
        AbortEx;
      end;
    end;  
  end;
  FBeforePost(DataSet);
end;

procedure TEDIT_PR.Deactivate;
begin
  inherited;
  DM_PAYROLL.PR.BeforePost := FBeforePost;
  wwdsDetail.OnDataChange := nil;
  wwdsDetail.DataSet.AfterScroll := FSaveAfterScroll;
end;

procedure TEDIT_PR.UpdatePaymentTypes;
begin
  if DM_COMPANY.CO.TRUST_SERVICE.AsString = TRUST_SERVICE_NO then
  begin
    if (DM_PAYROLL.PR.STATUS.AsString = PAYROLL_STATUS_PENDING)
    and (TrustCombobox.Value <> PAYMENT_TYPE_NONE) then
      TrustCombobox.Value := PAYMENT_TYPE_NONE;
    TrustCombobox.Enabled := False;
  end
  else
    TrustCombobox.Enabled := True;

  if DM_COMPANY.CO.TAX_SERVICE.AsString <> TAX_SERVICE_FULL then
  begin
    if (DM_PAYROLL.PR.STATUS.AsString = PAYROLL_STATUS_PENDING)
    and (TaxCombobox.Value <> PAYMENT_TYPE_NONE) then
      TaxCombobox.Value := PAYMENT_TYPE_NONE;
    TaxCombobox.Enabled := False;
  end
  else
    TaxCombobox.Enabled := True;
end;

procedure TEDIT_PR.DBRadioGroup2Click(Sender: TObject);
begin
  if (DM_PAYROLL.PR.STATUS.AsString <> PAYROLL_STATUS_PROCESSED)
  and (DM_PAYROLL.PR.STATUS.AsString <> PAYROLL_STATUS_VOIDED) then
  begin
    DBRadioGroup2.Tag := 1;
  end;
end;

procedure TEDIT_PR.DBRadioGroup2Change(Sender: TObject);
begin
  inherited;
  if DBRadioGroup2.Tag = 0 then
    exit;
  try
    if DBRadioGroup2.Value = 'Y' then
    begin
      TrustComboBox.Value := PAYMENT_TYPE_NONE;
      TaxComboBox.Value := PAYMENT_TYPE_NONE;
      DDComboBox.Value := PAYMENT_TYPE_NONE;
      BillingComboBox.Value := PAYMENT_TYPE_NONE;
      WCComboBox.Value := PAYMENT_TYPE_NONE;
    end else
    begin
      TrustComboBox.Value := DM_COMPANY.CO.TRUST_IMPOUND.Value;
      TaxComboBox.Value := DM_COMPANY.CO.TAX_IMPOUND.Value;
      DDComboBox.Value := DM_COMPANY.CO.DD_IMPOUND.Value;
      BillingComboBox.Value := DM_COMPANY.CO.BILLING_IMPOUND.Value;
      WCComboBox.Value := DM_COMPANY.CO.WC_IMPOUND.Value;
    end;
  finally
     DBRadioGroup2.Tag :=0;
  end;
end;

procedure TEDIT_PR.EnablePayrollControls;
begin
  evDBSpinEdit1.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  evDBRadioGroup1.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  wwDBComboBox1.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  wwDBDateTimePicker1.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  wwDBDateTimePicker2.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  DBMemo1.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  butnPreProcess.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  butnCopy.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  CheckButtonState;
  butnUnlock.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  butnTaxAdjustments.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  butnQtrEndCleanupPR.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  butnPAMonthlyCleanup.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
  btnCheckRecon.Enabled := (wwdsDetail.DataSet.State = dsInsert) or (not wwdsDetail.DataSet.IsEmpty);
end;

procedure TEDIT_PR.TabSheet2Show(Sender: TObject);
begin
  EnablePayrollControls;
end;

procedure TEDIT_PR.tsExclusionsEnter(Sender: TObject);
begin
  inherited;
  if not DM_SERVICE_BUREAU.SB.Active then
    DM_SERVICE_BUREAU.SB.DataRequired('');
  DDComboBox.Enabled := DM_SERVICE_BUREAU.SB.MICR_FONT.AsString = 'N';
end;

procedure TEDIT_PR.CheckButtonState;
begin
  butnVoid.Enabled := (wwdsDetail.DataSet.State = dsBrowse) and (DM_PAYROLL.PR.FieldByName('STATUS').Value <> PAYROLL_STATUS_PENDING)
    and (DM_PAYROLL.PR.FieldByName('STATUS').Value <> PAYROLL_STATUS_HOLD)
    and (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').Value <> PAYROLL_TYPE_TAX_DEPOSIT)
    and not wwdsDetail.DataSet.IsEmpty;
end;

procedure TEDIT_PR.PayrollAfterScroll(DataSet: TDataSet);
begin
  CheckButtonState;
end;

initialization
  RegisterClass(TEDIT_PR);

end.
