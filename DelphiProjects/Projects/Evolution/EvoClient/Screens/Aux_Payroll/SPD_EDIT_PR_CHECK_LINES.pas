// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_PR_CHECK_LINES;

interface

uses
   SDataStructure, SPD_EDIT_PR_BASE, wwdblook, StdCtrls, Mask,
  wwdbedit, Buttons, DBActns, Classes, ActnList, Db, Wwdatsrc, DBCtrls,
  Grids, Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, EvConsts,
  EvUtils, EvTypes, Dialogs, SPD_DBDTSelectionListFiltr, SysUtils, SPackageEntry,
  ImgList, Variants, SDDClasses, ISBasicClasses, SDataDictclient, EvContext,
  SDataDicttemp, wwdbdatetimepicker, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBDateTimePicker, isUIwwDBLookupCombo, isUIwwDBEdit,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, LMDCustomButton,
  LMDButton, isUILMDButton, isUIFashionPanel, Forms;

type
  TEDIT_PR_CHECK_LINES = class(TEDIT_PR_BASE)
    wwDBGrid1: TevDBGrid;
    wwDBGrid2: TevDBGrid;
    Label5: TevLabel;
    DBEdit2: TevDBEdit;
    Label6: TevLabel;
    Label7: TevLabel;
    Label8: TevLabel;
    Label9: TevLabel;
    Label10: TevLabel;
    Label11: TevLabel;
    Label12: TevLabel;
    Label13: TevLabel;
    Label14: TevLabel;
    Label15: TevLabel;
    DBEdit12: TevDBEdit;
    Label17: TevLabel;
    DBEdit14: TevDBEdit;
    Label18: TevLabel;
    Label19: TevLabel;
    Label20: TevLabel;
    wwlcEE_STATE: TevDBLookupCombo;
    wwlcEE_SUI_STATE: TevDBLookupCombo;
    wwDBLookupCombo3: TevDBLookupCombo;
    wwDBLookupCombo4: TevDBLookupCombo;
    wwDBLookupCombo5: TevDBLookupCombo;
    wwDBLookupCombo6: TevDBLookupCombo;
    wwDBLookupCombo7: TevDBLookupCombo;
    Label4: TevLabel;
    DBEdit1: TevDBEdit;
    wwDBLookupCombo8: TevDBLookupCombo;
    wwDBLookupCombo9: TevDBLookupCombo;
    wwDBLookupCombo10: TevDBLookupCombo;
    wwDBLookupCombo11: TevDBLookupCombo;
    wwDBLookupCombo12: TevDBLookupCombo;
    Label22: TevLabel;
    DBEdit3: TevDBEdit;
    SpeedButton1: TevSpeedButton;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    evPanel1: TevPanel;
    Label1: TevLabel;
    DBText1: TevDBText;
    Label16: TevLabel;
    DBText4: TevDBText;
    Label21: TevLabel;
    DBText5: TevDBText;
    evSplitter1: TevSplitter;
    evDBGrid1: TevDBGrid;
    wwdsSubMaster1: TevDataSource;
    wwdsSubMaster2: TevDataSource;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    EESrc: TevDataSource;
    EEStatesSrc: TevDataSource;
    evPanel2: TevPanel;
    Label2: TevLabel;
    Label3: TevLabel;
    DBText2: TevDBText;
    DBText3: TevDBText;
    pnlLocation: TevPanel;
    evLabel2: TevLabel;
    evDBText1: TevDBText;
    evDBText2: TevDBText;
    evPanel3: TevPanel;
    evLabel3: TevLabel;
    evDBText3: TevDBText;
    evLabel4: TevLabel;
    evDBText4: TevDBText;
    Images: TImageList;
    butnPrior: TevBitBtn;
    butnNext: TevBitBtn;
    butnRefreshSchedEDs: TevBitBtn;
    butnALD_or_Change: TevBitBtn;
    butnPreviewCheck: TevBitBtn;
    TabSheet2: TTabSheet;
    evDBDateTimePicker1: TevDBDateTimePicker;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evDBDateTimePicker2: TevDBDateTimePicker;
    procedure butnPreviewCheckClick(Sender: TObject);
    procedure butnPriorClick(Sender: TObject);
    procedure butnNextClick(Sender: TObject);
    procedure butnALD_or_ChangeClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure butnRefreshSchedEDsClick(Sender: TObject);
    procedure PRPageControlChange(Sender: TObject);
    procedure PRNextExecute(Sender: TObject);
    procedure wwdsSubMaster2DataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure PRPriorExecute(Sender: TObject);
    procedure wwdsSubMaster2DataChangeBeforeChildren(Sender: TObject;
      Field: TField);
    procedure lkupStatesEnter(Sender: TObject);
    procedure lkupStatesExit(Sender: TObject);
    procedure lkupSUIEnter(Sender: TObject);
    procedure lkupSUIExit(Sender: TObject);
    procedure EESrcDataChange(Sender: TObject; Field: TField);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
    procedure wwDBLookupCombo6Enter(Sender: TObject);
    procedure wwDBLookupCombo6Exit(Sender: TObject);
  private
    FCustomAfterPost, FCustomAfterDelete: TDataSetNotifyEvent;
    EEStatesFilter, EESUIFilter: Integer;
    procedure RecalcSum;
    procedure AfterChangeDelete(DataSet: TDataSet);
    procedure AfterChangePost(DataSet: TDataSet);
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure ReopenPRCheckWithLines; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure AfterClick(Kind: Integer); override;
  end;

implementation

uses
  SPD_Payroll_Expert, SPD_Check_Preview, SPD_Labor_Distribution_Preview,
  SPopulateRecords, Graphics, isBaseClasses;

{$R *.DFM}

procedure TEDIT_PR_CHECK_LINES.butnPreviewCheckClick(Sender: TObject);
var
  Warnings: String;
begin
  ctx_DataAccess.CheckMaxAmountAndHours := False;
  try
    if (not (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED,
    PAYROLL_STATUS_COMPLETED, PAYROLL_STATUS_PROCESSING, PAYROLL_STATUS_PREPROCESSING])) and (wwdsSubMaster2.DataSet.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_VOID) then
    try
      ctx_StartWait;
      ctx_PayrollCalculation.GrossToNet(Warnings, wwdsSubMaster2.DataSet.FieldByName('PR_CHECK_NBR').Value, True, True, True, False);
    finally
      ctx_EndWait;
    end;
    Check_Preview := TCheck_Preview.Create(Self);
    try
      Check_Preview.ShowModal;
    finally
      Check_Preview.Free;
    end;
  finally
    ctx_DataAccess.CheckMaxAmountAndHours := True;
  end;
end;

procedure TEDIT_PR_CHECK_LINES.butnPriorClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Prior;
  RecalcSum;
end;

procedure TEDIT_PR_CHECK_LINES.butnNextClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Next;
  RecalcSum;
end;

procedure TEDIT_PR_CHECK_LINES.butnALD_or_ChangeClick(Sender: TObject);
var
  TempStr: String;
  AllOK: Boolean;
begin
  if butnALD_or_Change.Caption = 'Preview ALD' then
  begin
    if wwdsMaster.DataSet.FieldByName('CHECK_TYPE').Value = CHECK_TYPE2_VOID then
      Exit;

    ctx_StartWait;
    try
      with DM_PAYROLL, DM_EMPLOYEE, DM_CLIENT, DM_COMPANY do
      begin
        CL_E_D_GROUP_CODES.DataRequired('ALL');
        EE_AUTOLABOR_DISTRIBUTION.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);

        Labor_Distribution_Preview := TLabor_Distribution_Preview.Create(Self);
        try
          Labor_Distribution_Preview.ShowDistribution(PR_CHECK.FieldByName('EE_NBR').AsInteger, PR_CHECK_LINES, EE,
            EE_AUTOLABOR_DISTRIBUTION, CL_E_DS, CL_E_D_GROUP_CODES, CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, CO_JOBS,
              CO_WORKERS_COMP);
        finally
          Labor_Distribution_Preview.Free;
        end;
      end;
    finally
      ctx_EndWait;
    end;
  end
  else
    with DM_PAYROLL.PR_CHECK_LINES do
    begin
      ctx_DataAccess.UnlockPR;
      ctx_DataAccess.RecalcLineBeforePost := False;
      AllOK := False;
      try
        Edit;
        if FieldByName('AGENCY_STATUS').Value = CHECK_LINE_AGENCY_STATUS_PENDING then
        begin
          FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_IGNORE;
          FieldByName('CL_AGENCY_NBR').Value := Null;
        end
        else
        begin
          FieldByName('AGENCY_STATUS').Value := CHECK_LINE_AGENCY_STATUS_PENDING;
          Payroll_Expert := TPayroll_Expert.Create(Self);
          with Payroll_Expert do
          try
            wwdsTemp.DataSet := DM_PAYROLL.PR_CHECK_LINES;
// Get Client Agency Number from a user
            repeat
              TempStr := DisplayExpert(crLookupCombo, 'Please, select an agency for this check line.', DM_CLIENT.CL_AGENCY,
                VarArrayOf(['CL_AGENCY_NBR', 'AGENCY_NAME' + #9 + '40' + #9 + 'AGENCY_NAME', 'CL_AGENCY_NBR']));
            until TempStr <> '';
          finally
            Payroll_Expert.Free;
          end;
        end;
        Post;
        CommitData;
        AllOK := True;
      finally
        ctx_DataAccess.RecalcLineBeforePost := True;
        ctx_DataAccess.LockPR(AllOK);
      end;
    end;
end;

procedure TEDIT_PR_CHECK_LINES.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if csLoading in ComponentState then
    Exit; 
  if (DM_PAYROLL.PR_CHECK_LINES.State = dsEdit) and ctx_DataAccess.PayrollLocked then
    if DM_PAYROLL.PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED then
    begin
      DM_PAYROLL.PR_CHECK_LINES.Cancel;
      raise EUpdateError.CreateHelp('You can not change scheduled Earning/Deduction!', IDH_ConsistencyViolation);
    end;
  if DM_PAYROLL.PR_CHECK_LINES.State = dsInsert then
  begin
    if ExtractFromFiller(DM_EMPLOYEE.EE.FieldByName('FILLER').AsString,100,1) = 'Y' then
      DM_PAYROLL.PR_CHECK_LINES.FieldByName('WORK_ADDRESS_OVR').AsString := 'Y';
  end;
  DM_EMPLOYEE.EE_WORK_SHIFTS.RetrieveCondition := 'EE_NBR = ' + IntToStr(DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger);
end;

procedure TEDIT_PR_CHECK_LINES.SpeedButton1Click(Sender: TObject);
var
  DBDTSelectionList: TDBDTSelectionListFiltr;
begin
  inherited;
  if DM_EMPLOYEE.EE_RATES.Active and
     DM_EMPLOYEE.EE_RATES.Locate('EE_NBR;RATE_NUMBER',
        VarArrayOf([DM_PAYROLL.PR_CHECK['EE_NBR'], DM_PAYROLL.PR_CHECK_LINES['RATE_NUMBER']]), []) and
     not DM_EMPLOYEE.EE_RATES.FieldByname('CO_DIVISION_NBR').IsNull then
    raise ECanNotPerformOperation.CreateHelp('This rate has DBDT override on EE rates level. Can not override on check line level', IDH_CanNotPerformOperation);

  DBDTSelectionList := TDBDTSelectionListFiltr.Create(nil);
  try
    with DM_COMPANY, DM_PAYROLL, DBDTSelectionList do
    begin
      DBDTSelectionList.Setup(CO_DIVISION, CO_BRANCH, CO_DEPARTMENT, CO_TEAM, PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value
        , PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value, PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value
        , PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value, CO.FieldByName('DBDT_LEVEL').AsString[1]);
      if DBDTSelectionList.ShowModal = mrOK then
      begin
        if (PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value <> wwcsTempDBDT.FieldByName('DIVISION_NBR').Value)
          or (PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value <> wwcsTempDBDT.FieldByName('BRANCH_NBR').Value)
          or (PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value <> wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value)
          or (PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value <> wwcsTempDBDT.FieldByName('TEAM_NBR').Value) then
        begin
          if not (PR_CHECK_LINES.State in [dsInsert, dsEdit]) then
          begin
            if PR_CHECK_LINES.RecordCount = 0 then
              PR_CHECK_LINES.Insert
            else
              PR_CHECK_LINES.Edit;
          end;
          PR_CHECK_LINES.FieldByName('CO_DIVISION_NBR').Value := wwcsTempDBDT.FieldByName('DIVISION_NBR').Value;
          PR_CHECK_LINES.FieldByName('CO_BRANCH_NBR').Value := wwcsTempDBDT.FieldByName('BRANCH_NBR').Value;
          PR_CHECK_LINES.FieldByName('CO_DEPARTMENT_NBR').Value := wwcsTempDBDT.FieldByName('DEPARTMENT_NBR').Value;
          PR_CHECK_LINES.FieldByName('CO_TEAM_NBR').Value := wwcsTempDBDT.FieldByName('TEAM_NBR').Value;
        end;
      end;
    end;
  finally
    DBDTSelectionList.Free;
  end;
end;

procedure TEDIT_PR_CHECK_LINES.butnRefreshSchedEDsClick(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  if (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED])
    or (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_REGULAR) and
       (DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').AsString <> CHECK_TYPE2_MANUAL) then
    Exit;
  if EvMessage('Are you sure you want to refresh Scheduled EDs for this check?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;

  ctx_StartWait;
  DM_PAYROLL.PR_CHECK_LINES.DisableControls;
  try
    with DM_PAYROLL do
    begin
      PR_CHECK_LINES.First;
      while not PR_CHECK_LINES.EOF do
      begin
        if (DM_PAYROLL.PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED)
        or (DM_PAYROLL.PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_DISTR_SCHEDULED) then
        begin
          PR_CHECK_LINES.Delete;
          i := PR_CHECK_LINES.RecNo;
          PR_CHECK_LINES.ReSync([]);
          PR_CHECK_LINES.RecNo := i;
        end
        else
          PR_CHECK_LINES.Next;
      end;

      ctx_PayrollCalculation.Generic_CreatePRCheckDetails(
        PR,
        PR_BATCH,
        PR_CHECK,
        PR_CHECK_LINES,
        PR_CHECK_STATES,
        PR_CHECK_SUI,
        PR_CHECK_LOCALS,
        PR_CHECK_LINE_LOCALS,
        PR_SCHEDULED_E_DS,
        DM_CLIENT.CL_E_DS,
        DM_CLIENT.CL_PERSON,
        DM_CLIENT.CL_PENSION,
        DM_COMPANY.CO,
        DM_COMPANY.CO_E_D_CODES,
        DM_COMPANY.CO_STATES,
        DM_EMPLOYEE.EE,
        DM_EMPLOYEE.EE_SCHEDULED_E_DS,
        DM_EMPLOYEE.EE_STATES,
        DM_SYSTEM_FEDERAL.SY_FED_TAX_TABLE,
        DM_SYSTEM_STATE.SY_STATES,
        False,
        (PR_BATCH.FieldByName('PAY_SALARY').AsString = 'Y'),
        (PR_BATCH.FieldByName('PAY_STANDARD_HOURS').AsString = 'Y'),
        True
      );

      CommitData;
    end;
  finally
    DM_PAYROLL.PR_CHECK_LINES.EnableControls;
    ctx_EndWait;
  end;
end;

procedure TEDIT_PR_CHECK_LINES.PRPageControlChange(Sender: TObject);
begin
  inherited;
  Panel1.Visible := PageControl1.ActivePage = TabSheet1;
  evPanel1.Visible := not Panel1.Visible;
  evPanel2.Visible := not Panel1.Visible;
  ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LINES.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_AGENCY, aDS);
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_CLIENT.CL_PIECES, aDS);
  AddDS(DM_EMPLOYEE.EE, aDS);
  AddDS(DM_EMPLOYEE.EE_STATES, aDS);
  AddDS(DM_COMPANY.CO_DIVISION, aDS);
  AddDS(DM_COMPANY.CO_BRANCH, aDS);
  AddDS(DM_COMPANY.CO_DEPARTMENT, aDS);
  AddDS(DM_COMPANY.CO_TEAM, aDS);
  AddDS(DM_COMPANY.CO_JOBS, aDS);
  AddDS(DM_COMPANY.CO_SHIFTS, aDS);
  AddDS(DM_COMPANY.CO_WORKERS_COMP, aDS);
  AddDS(DM_EMPLOYEE.EE_WORK_SHIFTS, aDS);
  AddDS(DM_PAYROLL.PR_BATCH, aDS);
  inherited;
end;

function TEDIT_PR_CHECK_LINES.GetDataSetConditions(sName: string): string;
begin
  if (sName = 'CO_BRANCH') or
     (sName = 'CO_DEPARTMENT') or
     (sName = 'EE_STATES') or
     (sName = 'CO_TEAM') then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

function TEDIT_PR_CHECK_LINES.GetInsertControl: TWinControl;
begin
  Result := wwDBLookupCombo8;
end;

procedure TEDIT_PR_CHECK_LINES.PRNextExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Next;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LINES.wwdsSubMaster2DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  butnPrior.Enabled := wwdsSubMaster2.DataSet.RecNo > 1;
  butnNext.Enabled := wwdsSubMaster2.DataSet.RecNo < wwdsSubMaster2.DataSet.RecordCount;
end;

procedure TEDIT_PR_CHECK_LINES.wwdsDetailDataChange(Sender: TObject;
  Field: TField);

  procedure AssignBitmap(ImageIndex: Integer);
  var
    aBitmap: TBitmap;
  begin
    aBitmap := TBitmap.Create;
    try
      Images.GetBitmap(ImageIndex, aBitmap);
      aBitmap.Width := 32;
      aBitmap.Height := 16;
      butnALD_or_Change.Glyph.Assign(aBitmap);
    finally
      aBitmap.Free;
    end;
  end;

begin
  inherited;
  if Assigned(Field) then
    if (Field.FieldName = 'EE_STATES_NBR') or
       (Field.FieldName = 'EE_SUI_STATES_NBR') then
      if DM_PAYROLL.PR_CHECK_LINES.FieldByName('EE_STATES_NBR').IsNull and
         DM_PAYROLL.PR_CHECK_LINES.FieldByName('EE_SUI_STATES_NBR').IsNull then
      begin
//        EEStatesFilter := 0;
//        EESUIFilter := 0
      end
      else if (DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', DM_PAYROLL.PR_CHECK_LINES['EE_STATES_NBR'], 'State_Lookup') = 'PR') or
              (DM_EMPLOYEE.EE_STATES.Lookup('EE_STATES_NBR', DM_PAYROLL.PR_CHECK_LINES['EE_SUI_STATES_NBR'], 'State_Lookup') = 'PR') then
      begin
        EEStatesFilter := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
        EESUIFilter := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('SUI_State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
      end
      else
      begin
        EEStatesFilter := -ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
        EESUIFilter := -ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('SUI_State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
      end;
  if (Length(DM_PAYROLL.PR.FieldByName('STATUS').AsString) > 0) and
  (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED]) then
  begin
    if DM_PAYROLL.PR_CHECK_LINES.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').IsNull then
    begin
      butnALD_or_Change.Enabled := True;
      if DM_PAYROLL.PR_CHECK_LINES.FieldByName('AGENCY_STATUS').Value = CHECK_LINE_AGENCY_STATUS_PENDING then
        butnALD_or_Change.Caption := 'Make "Ignore"'
      else
        butnALD_or_Change.Caption := 'Make "Pending"';
    end
    else
    begin
      butnALD_or_Change.Enabled := False;
      butnALD_or_Change.Caption := 'Can''t change';
    end;
    AssignBitmap(1);
  end
  else
  begin
    butnALD_or_Change.Caption := 'Preview ALD';
    AssignBitmap(0);
  end;
end;

procedure TEDIT_PR_CHECK_LINES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_PAYROLL.PR_BATCH, SupportDataSets);
end;

procedure TEDIT_PR_CHECK_LINES.PRPriorExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Prior;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LINES.ReopenPRCheckWithLines;
begin
  inherited;
  wwdsSubMaster1.DataSet := wwdsSubMaster1.DataSet;
  wwdsSubMaster2.DataSet := wwdsSubMaster2.DataSet;
  CheckEEStatesForSUI;
  RecalcSum;
end;

procedure TEDIT_PR_CHECK_LINES.wwdsSubMaster2DataChangeBeforeChildren(
  Sender: TObject; Field: TField);
begin
  inherited;
  if DM_EMPLOYEE.EE.Active then
    DM_EMPLOYEE.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
end;

procedure TEDIT_PR_CHECK_LINES.Activate;
var
  aDS: TArrayDS;
begin
  inherited;
  EEStatesFilter := 0;
  EESUIFilter := 0;
  ctx_DataAccess.CheckMaxAmountAndHours := True;
  with (Owner as TFramePackageTmpl) do
  begin
    DeleteDataSet := DM_PAYROLL.PR_CHECK_LINES;
    NavigationDataSet := DM_PAYROLL.PR_CHECK_LINES;
    aDS := EditDataSets;
    AddDS(DM_PAYROLL.PR_CHECK_LINES, aDS);
    EditDataSets := aDS;
    aDS := InsertDataSets;
    AddDS(DM_PAYROLL.PR_CHECK_LINES, aDS);
    InsertDataSets := aDS;
  end;
  FCustomAfterPost := DM_PAYROLL.PR_CHECK_LINES.AfterPost;
  DM_PAYROLL.PR_CHECK_LINES.AfterPost := AfterChangePost;
  FCustomAfterDelete := DM_PAYROLL.PR_CHECK_LINES.AfterDelete;
  DM_PAYROLL.PR_CHECK_LINES.AfterDelete := AfterChangeDelete;
end;

procedure TEDIT_PR_CHECK_LINES.DeActivate;
begin
  inherited;
  ctx_DataAccess.CheckMaxAmountAndHours := False;

  DM_SYSTEM_STATE.SY_STATES.ClearAsOfDateOverride;
  DM_COMPANY.CO_STATES.ClearAsOfDateOverride;
  DM_EMPLOYEE.EE_STATES.ClearAsOfDateOverride;
  
  DM_EMPLOYEE.EE_STATES.Filter := '';
  DM_EMPLOYEE.EE_STATES.Filtered := False;
  DM_PAYROLL.PR_CHECK_LINES.AfterPost := FCustomAfterPost;
  DM_PAYROLL.PR_CHECK_LINES.AfterDelete := FCustomAfterDelete;
end;

procedure TEDIT_PR_CHECK_LINES.lkupStatesEnter(Sender: TObject);
begin
  inherited;
  if EEStatesFilter > 0 then
  begin
    DM_EMPLOYEE.EE_STATES.Filter := 'EE_STATES_NBR = ' + IntToStr(EEStatesFilter);
    DM_EMPLOYEE.EE_STATES.Filtered := True
  end
  else if EEStatesFilter < 0 then
  begin
    DM_EMPLOYEE.EE_STATES.Filter := 'EE_STATES_NBR <> ' + IntToStr(-EEStatesFilter);
    DM_EMPLOYEE.EE_STATES.Filtered := True
  end
  else
  begin
    DM_EMPLOYEE.EE_STATES.Filter := '';
    DM_EMPLOYEE.EE_STATES.Filtered := False
  end
end;

procedure TEDIT_PR_CHECK_LINES.lkupStatesExit(Sender: TObject);
begin
  inherited;
  DM_EMPLOYEE.EE_STATES.Filter := '';
  DM_EMPLOYEE.EE_STATES.Filtered := False
end;

procedure TEDIT_PR_CHECK_LINES.lkupSUIEnter(Sender: TObject);
begin
  inherited;
  if EESUIFilter > 0 then
  begin
    DM_EMPLOYEE.EE_STATES.Filter := 'EE_STATES_NBR = ' + IntToStr(EESUIFilter);
    DM_EMPLOYEE.EE_STATES.Filtered := True
  end
  else if EESUIFilter < 0 then
  begin
    DM_EMPLOYEE.EE_STATES.Filter := 'EE_STATES_NBR <> ' + IntToStr(-EESUIFilter);
    DM_EMPLOYEE.EE_STATES.Filtered := True
  end
  else
  begin
    DM_EMPLOYEE.EE_STATES.Filter := '';
    DM_EMPLOYEE.EE_STATES.Filtered := False
  end
end;

procedure TEDIT_PR_CHECK_LINES.lkupSUIExit(Sender: TObject);
begin
  inherited;
  DM_EMPLOYEE.EE_STATES.Filter := '';
  DM_EMPLOYEE.EE_STATES.Filtered := False
end;

procedure TEDIT_PR_CHECK_LINES.RecalcSum;
var
  bPR: Boolean;
  bNotPR: Boolean;
begin
  bPR := False;
  bNotPR := False;
  EEStatesFilter := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
  EESUIFilter := ConvertNull(DM_EMPLOYEE.EE_STATES.Lookup('SUI_State_Lookup', 'PR', 'EE_STATES_NBR'), 0);
  if DM_PAYROLL.PR_CHECK_LINES.RetrieveCondition <> '' then
  begin
    with TevClientDataSet.Create(nil) do
    try
      CloneCursor(DM_PAYROLL.PR_CHECK_LINES, False);
      First;
      while not Eof do
      begin
        bPR := bPR or
               (FieldValues['EE_STATES_NBR'] = EEStatesFilter) or
               (FieldValues['EE_SUI_STATES_NBR'] = EESUIFilter);
        bNotPR := bNotPR or
               (FieldValues['EE_STATES_NBR'] <> EEStatesFilter) and
               not FieldByName('EE_STATES_NBR').IsNull or
               (FieldValues['EE_SUI_STATES_NBR'] <> EESUIFilter) and
               not FieldByName('EE_SUI_STATES_NBR').IsNull;
        Next;
      end;
    finally
      Free;
    end;
  end;
  if bNotPR then
  begin
    EEStatesFilter := -EEStatesFilter;
    EESUIFilter := -EESUIFilter
  end
  else if not bPR then
  begin
    EEStatesFilter := 0;
    EESUIFilter := 0
  end;
  if wwlcEE_STATE.Focused then
    lkupStatesEnter(Self);
  if wwlcEE_SUI_STATE.Focused then
    lkupSUIEnter(Self);
end;

procedure TEDIT_PR_CHECK_LINES.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind in [NavAbort, NavCancel] then
    RecalcSum;
end;

procedure TEDIT_PR_CHECK_LINES.AfterChangeDelete(DataSet: TDataSet);
begin
  RecalcSum;
  if Assigned(FCustomAfterDelete) then
    FCustomAfterDelete(DataSet);
end;

procedure TEDIT_PR_CHECK_LINES.AfterChangePost(DataSet: TDataSet);
begin
  RecalcSum;
  if Assigned(FCustomAfterPost) then
    FCustomAfterPost(DataSet);
end;

procedure TEDIT_PR_CHECK_LINES.EESrcDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if csLoading in ComponentState then
    Exit; 
  DM_EMPLOYEE.EE_WORK_SHIFTS.RetrieveCondition := 'EE_NBR = ' + IntToStr(DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').AsInteger);
end;

procedure TEDIT_PR_CHECK_LINES.wwdsSubMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  butnRefreshSchedEDs.Enabled := DM_PAYROLL.PR.Active and
       (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT);
end;

procedure TEDIT_PR_CHECK_LINES.wwDBLookupCombo6Enter(Sender: TObject);
begin
  inherited;
  if wwdsSubMaster2.DataSet.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_3RD_PARTY then
  begin
    DM_CLIENT.CO_E_D_CODES.Filter := 'E_D_CODE_TYPE in ('+ List3rdPartyTypes +')';
    DM_CLIENT.CO_E_D_CODES.Filtered := True;
  end
  else if wwdsSubMaster2.DataSet.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_COBRA_CREDIT then
  begin
    DM_CLIENT.CO_E_D_CODES.Filter := 'E_D_CODE_TYPE=''' + ED_MEMO_COBRA_CREDIT + ''' or E_D_CODE_TYPE=''' + ED_MEMO_SIMPLE + ''' and CodeDescription like ''%Cobra%''';
    DM_CLIENT.CO_E_D_CODES.Filtered := True;
  end
  else // if wwdsSubMaster2.DataSet.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_REGULAR then
  begin
    DM_CLIENT.CO_E_D_CODES.Filter := 'E_D_CODE_TYPE<>''' + ED_MEMO_COBRA_CREDIT + '''';
    DM_CLIENT.CO_E_D_CODES.Filtered := True;
  end;
end;

procedure TEDIT_PR_CHECK_LINES.wwDBLookupCombo6Exit(Sender: TObject);
begin
  inherited;
  DM_CLIENT.CO_E_D_CODES.Filtered := False;
  DM_CLIENT.CO_E_D_CODES.Filter := '';
end;

initialization
  RegisterClass(TEDIT_PR_CHECK_LINES);

end.
