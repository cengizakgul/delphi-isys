// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_PR_CHECK_LOCALS;

interface

uses
  SDataStructure, SPD_EDIT_PR_BASE, StdCtrls, ExtCtrls,
  DBCtrls, wwdblook, Mask, wwdbedit, DBActns, Classes, ActnList, Db,
  Wwdatsrc, Buttons, Grids, Wwdbigrd, Wwdbgrid, Controls, ComCtrls,
  ISBasicClasses, SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SPD_DM_HISTORICAL_TAXES, EvUIUtils, SysUtils;

type
  TEDIT_PR_CHECK_LOCALS = class(TEDIT_PR_BASE)
    wwDBGrid2: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    Label2: TevLabel;
    Label4: TevLabel;
    DBEdit3: TevDBEdit;
    wwlcEE_LOCAL: TevDBLookupCombo;
    DBRadioGroup1: TevDBRadioGroup;
    evPanel1: TevPanel;
    Label1: TevLabel;
    DBText1: TevDBText;
    Label3: TevLabel;
    DBText4: TevDBText;
    Label5: TevLabel;
    DBText5: TevDBText;
    wwdsSubMaster1: TevDataSource;
    wwdsSubMaster2: TevDataSource;
    EESrc: TevDataSource;
    EEStatesSrc: TevDataSource;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    evSplitter1: TevSplitter;
    evDBGrid1: TevDBGrid;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    pnlLocation: TevPanel;
    evLabel2: TevLabel;
    evDBText1: TevDBText;
    evDBText2: TevDBText;
    evPanel3: TevPanel;
    evDBText3: TevDBText;
    evLabel4: TevLabel;
    evDBText4: TevDBText;
    butnPrior: TevBitBtn;
    butnNext: TevBitBtn;
    evDBRadioGroup1: TevDBRadioGroup;
    NONRES_LOCAL_Combo: TevDBLookupCombo;
    evLabel3: TevLabel;
    LOCATION_Combo: TevDBLookupCombo;
    evLabel5: TevLabel;
    procedure butnPriorClick(Sender: TObject);
    procedure butnNextClick(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure PRPageControlChange(Sender: TObject);
    procedure wwdsSubMaster2DataChangeBeforeChildren(Sender: TObject;
      Field: TField);
    procedure PRNextExecute(Sender: TObject);
    procedure PRPriorExecute(Sender: TObject);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
  protected
    FPrCheckNbr: Integer;
    FPaWarning: Boolean;
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure ReopenPRCheckWithLines; override;
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure Activate; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

uses
  EvConsts;

{$R *.DFM}

procedure TEDIT_PR_CHECK_LOCALS.butnPriorClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Prior;
end;

procedure TEDIT_PR_CHECK_LOCALS.butnNextClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Next;
end;

procedure TEDIT_PR_CHECK_LOCALS.wwdsDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  butnPrior.Enabled := wwdsSubMaster2.DataSet.RecNo > 1;
  butnNext.Enabled := wwdsSubMaster2.DataSet.RecNo < wwdsSubMaster2.DataSet.RecordCount;
  if not DM_CLIENT.PR_CHECK.IsEmpty then
  begin
    DM_CLIENT.EE_LOCALS.DataRequired('EE_NBR='+DM_CLIENT.PR_CHECK.EE_NBR.AsString);
    if FPrCheckNbr <> DM_CLIENT.PR_CHECK.PR_CHECK_NBR.AsInteger then
    begin
      FPrCheckNbr := DM_CLIENT.PR_CHECK.PR_CHECK_NBR.AsInteger;
      DM_CLIENT.PR_CHECK_LOCALS.Close;
      DM_CLIENT.PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR='+DM_CLIENT.PR_CHECK.PR_CHECK_NBR.AsString);
    end;
  end;
end;

procedure TEDIT_PR_CHECK_LOCALS.PRPageControlChange(Sender: TObject);
begin
  inherited;
  Panel1.Visible := PageControl1.ActivePage = TabSheet1;
  evPanel1.Visible := not Panel1.Visible;
  ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LOCALS.wwdsSubMaster2DataChangeBeforeChildren(
  Sender: TObject; Field: TField);
begin
  inherited;
  if DM_EMPLOYEE.EE.Active then
    DM_EMPLOYEE.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
end;

procedure TEDIT_PR_CHECK_LOCALS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_EMPLOYEE.EE_LOCALS, aDS);
  AddDS(DM_EMPLOYEE.CO_LOCATIONS, aDS);
  AddDS(DM_PAYROLL.PR_BATCH, aDS);
  AddDS(DM_CLIENT.CO_LOCAL_TAX, aDS);
  AddDS(DM_SYSTEM.SY_LOCALS, aDS);
  AddDS(DM_SYSTEM.SY_STATES, aDS);
  inherited;
end;

function TEDIT_PR_CHECK_LOCALS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR_CHECK_LOCALS;
end;

function TEDIT_PR_CHECK_LOCALS.GetInsertControl: TWinControl;
begin
  Result := wwlcEE_LOCAL;
end;

procedure TEDIT_PR_CHECK_LOCALS.ReopenPRCheckWithLines;
begin
  inherited;
  wwdsSubMaster1.DataSet := wwdsSubMaster1.DataSet;
  wwdsSubMaster2.DataSet := wwdsSubMaster2.DataSet;
end;

procedure TEDIT_PR_CHECK_LOCALS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_PAYROLL.PR_BATCH, SupportDataSets);
  AddDS(DM_EMPLOYEE.EE_LOCALS, SupportDataSets);
  AddDS(DM_EMPLOYEE.CO_LOCATIONS, SupportDataSets);
  AddDS(DM_CLIENT.CO_LOCAL_TAX, SupportDataSets);
  AddDS(DM_SYSTEM.SY_LOCALS, SupportDatasets);
  AddDS(DM_SYSTEM.SY_STATES, SupportDatasets);  
end;

procedure TEDIT_PR_CHECK_LOCALS.PRNextExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Next;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LOCALS.PRPriorExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Prior;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LOCALS.wwdsSubMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_PAYROLL.PR.Active then
    wwDBGrid1.ReadOnly := (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT);
end;

procedure TEDIT_PR_CHECK_LOCALS.Activate;
begin
  FPrCheckNbr := 0;
  FPaWarning := False;
  DM_CLIENT.CO_LOCAL_TAX.Activate;
  DM_CLIENT.EE_LOCALS.Activate;
  inherited;
end;

procedure TEDIT_PR_CHECK_LOCALS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if not HT.EE_LOCALS.Active then
    HT.LoadDataAsOfDate(now);
  if not FPaWarning and not DM_CLIENT.PR_CHECK_LOCALS.IsEmpty
  and (DM_CLIENT.PR_CHECK_LOCALS.State = dsEdit)
  and not DM_CLIENT.PR_CHECK_LOCALS.EE_LOCALS_NBR.IsNull and HT.IsAct32(DM_CLIENT.PR_CHECK_LOCALS.EE_LOCALS_NBR.AsInteger) then
  begin
    FPaWarning := True;
    EvMessage('Overrides should be applied to one checkline associated with each location as needed.');
  end;
end;

initialization
  RegisterClass(TEDIT_PR_CHECK_LOCALS);

end.
