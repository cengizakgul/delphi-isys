object PAReattachLocals: TPAReattachLocals
  Left = 314
  Top = 144
  BorderStyle = bsDialog
  Caption = 'Reattach Locals'
  ClientHeight = 491
  ClientWidth = 916
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pcMain: TevPageControl
    Left = 0
    Top = 0
    Width = 916
    Height = 491
    ActivePage = tsStatesLocals
    Align = alClient
    TabOrder = 0
    OnChange = pcMainChange
    object tsPayrolls: TTabSheet
      Caption = 'Select'
      ImageIndex = 1
      object evPanel3: TevPanel
        Left = 0
        Top = 422
        Width = 908
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object evSpeedButton1: TevSpeedButton
          Left = 466
          Top = 8
          Width = 130
          Height = 25
          Caption = 'Next'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333FFF333333333333000333333333
            3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
            3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
            0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
            BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
            33337777773FF733333333333300033333333333337773333333333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            3333333333333333333333333333333333333333333333333333}
          Layout = blGlyphRight
          NumGlyphs = 2
          OnClick = evSpeedButton1Click
          ShortCut = 0
        end
      end
      object grdPayroll: TevDBGrid
        Left = 0
        Top = 39
        Width = 908
        Height = 383
        HelpContext = 1502
        DisableThemesInTitle = False
        Selected.Strings = (
          'CHECK_DATE'#9'18'#9'Check Date'#9'F'
          'RUN_NUMBER'#9'10'#9'Run'#9'F'
          'CHECK_DATE_STATUS'#9'1'#9'Check Date Status'#9'F'
          'SCHEDULED'#9'1'#9'Scheduled'#9'F'
          'PAYROLL_TYPE'#9'1'#9'Type'#9'F'
          'STATUS'#9'1'#9'Status'#9'F'
          'STATUS_DATE'#9'18'#9'Status Date'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TPAReattachLocals\grdPayroll'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsPayrolls
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        DefaultSort = 'CHECK_DATE;RUN_NUMBER'
      end
      object evPanel4: TevPanel
        Left = 0
        Top = 0
        Width = 908
        Height = 39
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel1: TevLabel
          Left = 7
          Top = 11
          Width = 46
          Height = 13
          Caption = 'Employee'
        end
        object evLabel2: TevLabel
          Left = 255
          Top = 11
          Width = 22
          Height = 13
          Caption = 'Year'
        end
        object cbEmployee: TevDBLookupCombo
          Left = 67
          Top = 8
          Width = 177
          Height = 21
          ControlType.Strings = (
            'CURRENT_TERMINATION_CODE;CustomEdit;cbEmployeeCode')
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'Trim_Number'#9'9'#9'Aligned EE Code'#9'F'
            'Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F'
            'Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F'
            'Employee_MI_Calculate'#9'2'#9'MI'#9'F'
            'SOCIAL_SECURITY_NUMBER'#9'13'#9'SSN'#9'F'
            'CURRENT_TERMINATION_CODE_DESC'#9'20'#9'Status'#9'F')
          LookupTable = DM_EE.EE
          LookupField = 'EE_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = cbEmployeeChange
          OnBeforeDropDown = cbEmployeeBeforeDropDown
          OnCloseUp = cbEmployeeCloseUp
        end
        object cbYear: TevComboBox
          Left = 289
          Top = 8
          Width = 80
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 1
          OnChange = cbYearChange
        end
      end
    end
    object tsStatesLocals: TTabSheet
      Caption = 'PA EIT'
      ImageIndex = 2
      object evLabel3: TevLabel
        Left = 7
        Top = 355
        Width = 86
        Height = 13
        Caption = 'Residential Locals'
      end
      object evLabel4: TevLabel
        Left = 232
        Top = 355
        Width = 109
        Height = 13
        Caption = 'Non Residential Locals'
      end
      object evLabel5: TevLabel
        Left = 448
        Top = 355
        Width = 86
        Height = 13
        Caption = 'Residential Locals'
      end
      object evLabel6: TevLabel
        Left = 664
        Top = 355
        Width = 109
        Height = 13
        Caption = 'Non Residential Locals'
      end
      object evLabel7: TevLabel
        Left = 199
        Top = 339
        Width = 72
        Height = 13
        Caption = 'SOURCE     '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evLabel8: TevLabel
        Left = 607
        Top = 339
        Width = 97
        Height = 13
        Caption = 'DESTINATION   '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evPanel2: TevPanel
        Left = 0
        Top = 422
        Width = 908
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object btnCreateChecks: TevSpeedButton
          Left = 690
          Top = 0
          Width = 178
          Height = 25
          Caption = 'Create Checks'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD88888DDDDD
            DDDDDF88888FDDDDDDDDD8FBB33888888DDDF8DDD888FFFFFDDD8B33333B8333
            38DD8D88888D88888FDD83FBB3338BB3338D88DDD8888DD888FD8B33333B8333
            3B8D8D88888D88888D8F83BBBBB38BB3338D88DDDDD88DD8888F8BBBBBBB8333
            3B8D8DDDDDDD88888D8F08BBBBB8FBB33380D8DDDDD8DDD8888F0F88888B3333
            3B808D88888D88888D8F0FF88883BBBBB3808DDDDD88DDDDD88F0FF7778BBBBB
            BB808DDDDD8DDDDDDD8F0FF88888BBBBB8F08DD888D8DDDDD8D80FF88FFF8888
            87F08DD88DDD88888DD80FFFFFFFFFF888F08DDDDDDDDDDDD8D80FFFFFFFFFFF
            FFF08DDDDDDDDDDDDDD800000000000000008888888888888888}
          NumGlyphs = 2
          OnClick = btnCreateChecksClick
          ShortCut = 0
        end
        object btnApply: TevSpeedButton
          Left = 10
          Top = 0
          Width = 178
          Height = 25
          Caption = 'Apply'
          NumGlyphs = 2
          OnClick = btnApplyClick
          ShortCut = 0
        end
        object btnDelete: TevSpeedButton
          Left = 234
          Top = 0
          Width = 178
          Height = 25
          Caption = 'Remove '
          NumGlyphs = 2
          OnClick = btnDeleteClick
          ShortCut = 0
        end
      end
      object grdLocalTaxList: TevDBGrid
        Left = 0
        Top = 0
        Width = 908
        Height = 329
        DisableThemesInTitle = False
        Selected.Strings = (
          'res_Local'#9'35'#9'Source Residential'#9'F'
          'nres_Local'#9'35'#9'Source Non-Residential'#9'F'
          'Dest_res_Local'#9'35'#9'Destination Residential'#9'F'
          'Dest_nres_Local'#9'35'#9'Destination  Non-Residential'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TPAReattachLocals\grdLocalTaxList'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alTop
        DataSource = dsLocals
        KeyOptions = [dgEnterToTab]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        Sorting = False
      end
      object lcres_Locals: TevDBLookupCombo
        Left = 7
        Top = 384
        Width = 217
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup'#9'F')
        LookupTable = cdResEELocals
        LookupField = 'EE_LOCALS_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object lcnres_Locals: TevDBLookupCombo
        Left = 232
        Top = 384
        Width = 201
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup'#9'F')
        LookupTable = cdNresEELocals
        LookupField = 'EE_LOCALS_NBR'
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object lcdest_res_Locals: TevDBLookupCombo
        Left = 448
        Top = 384
        Width = 201
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup'#9'F')
        LookupTable = cdDest_ResEELocals
        LookupField = 'EE_LOCALS_NBR'
        Style = csDropDownList
        TabOrder = 4
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object lcdest_nres_Locals: TevDBLookupCombo
        Left = 664
        Top = 384
        Width = 201
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup'#9'F')
        LookupTable = cdDest_NresEELocals
        LookupField = 'EE_LOCALS_NBR'
        Style = csDropDownList
        TabOrder = 5
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  object dsEmployees: TevDataSource
    DataSet = DM_EE.EE
    Left = 352
    Top = 8
  end
  object dsPayrolls: TevDataSource
    DataSet = cdsPayrolls
    Left = 392
    Top = 8
  end
  object cdsPayrolls: TevClientDataSet
    ProviderName = 'PR_PROV'
    Left = 396
    Top = 40
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 320
    Top = 8
  end
  object dsLocals: TevDataSource
    DataSet = cdsLocals
    Left = 472
    Top = 8
  end
  object cdsLocals: TevClientDataSet
    Left = 476
    Top = 40
    object cdsLocalsEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsLocalsres_EE_LOCALS_NBR: TIntegerField
      FieldName = 'res_EE_LOCALS_NBR'
    end
    object cdsLocalsnres_EE_LOCALS_NBR: TIntegerField
      FieldName = 'nres_EE_LOCALS_NBR'
    end
    object cdsLocalsres_Local: TStringField
      FieldKind = fkLookup
      FieldName = 'res_Local'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'res_EE_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object cdsLocalsnres_Local: TStringField
      FieldKind = fkLookup
      FieldName = 'nres_Local'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'nres_EE_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object cdsLocalsDEST_res_EE_LOCALS_NBR: TIntegerField
      FieldName = 'DEST_res_EE_LOCALS_NBR'
    end
    object cdsLocalsDEST_nres_EE_LOCALS_NBR: TIntegerField
      FieldName = 'DEST_nres_EE_LOCALS_NBR'
    end
    object cdsLocalsDest_res_Local: TStringField
      FieldKind = fkLookup
      FieldName = 'Dest_res_Local'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'DEST_res_EE_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object cdsLocalsDest_nres_Local: TStringField
      FieldKind = fkLookup
      FieldName = 'Dest_nres_Local'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'DEST_nres_EE_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 288
    Top = 8
  end
  object dsEE_LOCALS: TevDataSource
    DataSet = DM_EE_LOCALS.EE_LOCALS
    Left = 576
    Top = 8
  end
  object PR_LIST: TevClientDataSet
    ProviderName = 'PR_PROV'
    Left = 404
    Top = 88
  end
  object cdResEELocals: TevClientDataSet
    Left = 492
    Top = 440
    object cdResEELocalsEE_LOCALS_NBR: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object cdResEELocalsEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdResEELocalsCO_LOCAL_TAX_NBR: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
    object cdResEELocalsLOCAL_TYPE: TStringField
      FieldName = 'LOCAL_TYPE'
      Size = 1
    end
    object cdResEELocalsSY_STATES_NBR: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object cdResEELocalsLocal_Name_Lookup: TStringField
      FieldName = 'Local_Name_Lookup'
      Size = 40
    end
  end
  object cdNresEELocals: TevClientDataSet
    Left = 532
    Top = 440
    object IntegerField1: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object IntegerField2: TIntegerField
      FieldName = 'EE_NBR'
    end
    object IntegerField3: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
    object StringField1: TStringField
      FieldName = 'LOCAL_TYPE'
      Size = 1
    end
    object IntegerField4: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object StringField2: TStringField
      FieldName = 'Local_Name_Lookup'
      Size = 40
    end
  end
  object cdDest_ResEELocals: TevClientDataSet
    Left = 588
    Top = 440
    object IntegerField5: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object IntegerField6: TIntegerField
      FieldName = 'EE_NBR'
    end
    object IntegerField7: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
    object StringField3: TStringField
      FieldName = 'LOCAL_TYPE'
      Size = 1
    end
    object IntegerField8: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object StringField4: TStringField
      FieldName = 'Local_Name_Lookup'
      Size = 40
    end
  end
  object cdDest_NresEELocals: TevClientDataSet
    Left = 620
    Top = 440
    object IntegerField9: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object IntegerField10: TIntegerField
      FieldName = 'EE_NBR'
    end
    object IntegerField11: TIntegerField
      FieldName = 'CO_LOCAL_TAX_NBR'
    end
    object StringField5: TStringField
      FieldName = 'LOCAL_TYPE'
      Size = 1
    end
    object IntegerField12: TIntegerField
      FieldName = 'SY_STATES_NBR'
    end
    object StringField6: TStringField
      FieldName = 'Local_Name_Lookup'
      Size = 40
    end
  end
end
