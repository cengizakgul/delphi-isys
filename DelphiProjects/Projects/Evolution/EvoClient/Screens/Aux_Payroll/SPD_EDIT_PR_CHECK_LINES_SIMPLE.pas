// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_PR_CHECK_LINES_SIMPLE;

interface

uses
  Windows, Messages,  SDataStructure, Buttons, SPD_EDIT_PR_BASE, StdCtrls,
  wwdblook, DBActns, Classes, ActnList, Db, Wwdatsrc, DBCtrls, Grids,
  Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, EvUtils, EvConsts,
  Dialogs, SPackageEntry, EvTypes, Variants, SDDClasses, EvContext, 
  ISBasicClasses, SDataDictclient, SDataDicttemp, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo, ImgList, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms;

type
  TEDIT_PR_CHECK_LINES_SIMPLE = class(TEDIT_PR_BASE)
    wwDBGrid2: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    wwlcE_D_CODE: TevDBLookupCombo;
    evPanel1: TevPanel;
    Label1: TevLabel;
    DBText1: TevDBText;
    Label4: TevLabel;
    Label5: TevLabel;
    DBText4: TevDBText;
    DBText5: TevDBText;
    evSplitter1: TevSplitter;
    evDBGrid1: TevDBGrid;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    evPanel2: TevPanel;
    Label2: TevLabel;
    Label3: TevLabel;
    DBText2: TevDBText;
    DBText3: TevDBText;
    wwdsSubMaster1: TevDataSource;
    wwdsSubMaster2: TevDataSource;
    pnlLocation: TevPanel;
    evLabel2: TevLabel;
    evDBText1: TevDBText;
    evDBText2: TevDBText;
    evPanel3: TevPanel;
    evLabel3: TevLabel;
    evDBText3: TevDBText;
    evLabel4: TevLabel;
    evDBText4: TevDBText;
    butnPrior: TevBitBtn;
    butnNext: TevBitBtn;
    Button2: TevBitBtn;
    Button1: TevBitBtn;
    procedure Button1Click(Sender: TObject);
    procedure butnPriorClick(Sender: TObject);
    procedure butnNextClick(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure Button2Click(Sender: TObject);
    procedure wwDBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure PRPageControlChange(Sender: TObject);
    procedure wwlcE_D_CODEEnter(Sender: TObject);
    procedure wwlcE_D_CODEExit(Sender: TObject);
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure ReopenPRCheckWithLines; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure DeActivate; override;
  end;

implementation

uses SysUtils, SPD_Check_Preview, isBaseClasses;

{$R *.DFM}

procedure TEDIT_PR_CHECK_LINES_SIMPLE.Button1Click(Sender: TObject);
var
  Warnings: String;
begin
  inherited;
  ctx_DataAccess.CheckMaxAmountAndHours := False;
  try
    if (not (DM_PAYROLL.PR.FieldByName('STATUS').AsString[1] in [PAYROLL_STATUS_PROCESSED, PAYROLL_STATUS_VOIDED,
      PAYROLL_STATUS_COMPLETED, PAYROLL_STATUS_PROCESSING, PAYROLL_STATUS_PREPROCESSING])) and (wwdsSubMaster2.DataSet.FieldByName('CHECK_TYPE').Value <> CHECK_TYPE2_VOID) then
    try
      ctx_StartWait;
      ctx_PayrollCalculation.GrossToNet(Warnings, wwdsSubMaster2.DataSet.FieldByName('PR_CHECK_NBR').Value, True, True, True, False);
    finally
      ctx_EndWait;
    end;
    Check_Preview := TCheck_Preview.Create(Self);
    try
      Check_Preview.ShowModal;
    finally
      Check_Preview.Free;
    end;
  finally
    ctx_DataAccess.CheckMaxAmountAndHours := True;
  end;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.butnPriorClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Prior;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.butnNextClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Next;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.wwdsDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  butnPrior.Enabled := wwdsSubMaster2.DataSet.RecNo > 1;
  butnNext.Enabled := wwdsSubMaster2.DataSet.RecNo < wwdsSubMaster2.DataSet.RecordCount;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.Button2Click(Sender: TObject);
begin
  inherited;
// Employee screen will be launched from here...
  EvMessage('Capability is not supported yet.');
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.wwDBGrid1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  A: Variant;
  i: Integer;
begin
  inherited;

  if Key = VK_F12 then
  with wwdsDetail.DataSet do
  begin
    if State in [dsInsert, dsEdit] then Post;
    A := VarArrayCreate([0, wwDBGrid1.FieldCount - 1], varVariant);
    for i := 0 to wwDBGrid1.FieldCount - 1 do
    begin
      wwDBGrid1.SelectedIndex := i;
      A[i] := wwDBGrid1.GetActiveField.Value;
    end;
    Next;
    if EOF then
    begin
      wwDBGrid1.SelectedIndex := wwDBGrid1.FieldCount - 1;
      SendMessage(wwDBGrid1.Handle, WM_KEYDOWN, VK_TAB, 0);
      SendMessage(wwDBGrid1.Handle, WM_KEYUP, VK_TAB, 0);
    end
    else
      Insert;
    for i := 0 to wwDBGrid1.FieldCount - 1 do
    begin
      wwDBGrid1.SelectedIndex := i;
      with wwDBGrid1.GetActiveField do
        if FieldKind in [fkLookup, fkCalculated] then
        begin
          DataSet.FieldByName(KeyFields).Value :=
            LookupDataSet.Lookup(LookupResultField, A[i], LookupKeyFields);
        end
        else
          Value := A[i];
    end;
    wwDBGrid1.SelectedIndex := 0;
  end;

end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.wwdsDetailStateChange(
  Sender: TObject);
begin
  inherited;
  if DM_PAYROLL.PR_CHECK_LINES.State = dsEdit then
  if DM_PAYROLL.PR_CHECK_LINES.FieldByName('LINE_TYPE').Value = CHECK_LINE_TYPE_SCHEDULED then
  begin
    DM_PAYROLL.PR_CHECK_LINES.Cancel;
    raise EUpdateError.CreateHelp('You can not change scheduled Earning/Deduction!', IDH_ConsistencyViolation);
  end;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.GetDataSetsToReopen(
  var aDS: TArrayDS; var Close: Boolean);
begin
  AddDS(DM_CLIENT.CL_E_DS, aDS);
  AddDS(DM_COMPANY.CO_E_D_CODES, aDS);
  AddDS(DM_EMPLOYEE.EE, aDS);
  AddDS(DM_PAYROLL.PR_BATCH, aDS);
  inherited;
end;

function TEDIT_PR_CHECK_LINES_SIMPLE.GetInsertControl: TWinControl;
begin
  Result := wwDBGrid1;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.ReopenPRCheckWithLines;
begin
  inherited;
  wwdsSubMaster1.DataSet := wwdsSubMaster1.DataSet; 
  wwdsSubMaster2.DataSet := wwdsSubMaster2.DataSet; 
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_PAYROLL.PR_BATCH, SupportDataSets);
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.PRPageControlChange(Sender: TObject);
begin
  inherited;
  Panel1.Visible := PageControl1.ActivePage = TabSheet1;
  evPanel1.Visible := not Panel1.Visible;
  evPanel2.Visible := not Panel1.Visible;
  ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.Activate;
var
  aDS: TArrayDS;
begin
  inherited;
  ctx_DataAccess.CheckMaxAmountAndHours := True;
  with (Owner as TFramePackageTmpl) do
  begin
    DeleteDataSet := DM_PAYROLL.PR_CHECK_LINES;
    NavigationDataSet := DM_PAYROLL.PR_CHECK_LINES;
    aDS := EditDataSets;
    AddDS(DM_PAYROLL.PR_CHECK_LINES, aDS);
    EditDataSets := aDS;
    aDS := InsertDataSets;
    AddDS(DM_PAYROLL.PR_CHECK_LINES, aDS);
    InsertDataSets := aDS;
  end;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.DeActivate;
begin
  inherited;
  ctx_DataAccess.CheckMaxAmountAndHours := False;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.wwlcE_D_CODEEnter(Sender: TObject);
begin
  inherited;
  if wwdsSubMaster2.DataSet.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_3RD_PARTY then
  begin
    DM_CLIENT.CO_E_D_CODES.Filter := 'E_D_CODE_TYPE in ('+ List3rdPartyTypes +')';
    DM_CLIENT.CO_E_D_CODES.Filtered := True;
  end
  else if wwdsSubMaster2.DataSet.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_COBRA_CREDIT then
  begin
    DM_CLIENT.CO_E_D_CODES.Filter := 'E_D_CODE_TYPE=''' + ED_MEMO_COBRA_CREDIT + ''' or E_D_CODE_TYPE=''' + ED_MEMO_SIMPLE + ''' and CodeDescription like ''%Cobra%''';
    DM_CLIENT.CO_E_D_CODES.Filtered := True;
  end
  else //if wwdsSubMaster2.DataSet.FieldByName('CHECK_TYPE').AsString = CHECK_TYPE2_REGULAR then
  begin
    DM_CLIENT.CO_E_D_CODES.Filter := 'E_D_CODE_TYPE<>''' + ED_MEMO_COBRA_CREDIT + '''';
    DM_CLIENT.CO_E_D_CODES.Filtered := True;
  end;
end;

procedure TEDIT_PR_CHECK_LINES_SIMPLE.wwlcE_D_CODEExit(Sender: TObject);
begin
  inherited;
  DM_CLIENT.CO_E_D_CODES.Filtered := False;
  DM_CLIENT.CO_E_D_CODES.Filter := '';
end;

initialization
  RegisterClass(TEDIT_PR_CHECK_LINES_SIMPLE);

end.
