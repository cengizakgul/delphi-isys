object ChooseLiabs: TChooseLiabs
  Left = 381
  Top = 167
  BorderStyle = bsDialog
  Caption = 'Choose liabilities'
  ClientHeight = 395
  ClientWidth = 440
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 270
    Top = 339
    Width = 34
    Height = 13
    Caption = 'Total:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lAmount: TevLabel
    Left = 400
    Top = 339
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = '0.00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object grdLiabs: TevDBGrid
    Left = 9
    Top = 9
    Width = 421
    Height = 319
    DisableThemesInTitle = False
    Selected.Strings = (
      'LEVEL_'#9'1'#9'Type'#9'F'
      'DESCRIPTION'#9'33'#9'Description'#9'F'
      'AMOUNT'#9'12'#9'Amount'#9'F'
      'CHECK_DATE'#9'15'#9'Check Date'#9'F'
      'RUN_NUMBER'#9'10'#9'Run Number'#9'F'
      'DUE_DATE'#9'18'#9'Due Date'#9'F'
      'STATUS'#9'1'#9'Status'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TChooseLiabs\grdLiabs'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsLiabs
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnMouseUp = grdLiabsMouseUp
    PaintOptions.AlternatingRowColor = clCream
    DefaultSort = '-'
  end
  object OKBtn: TevBitBtn
    Left = 144
    Top = 360
    Width = 75
    Height = 25
    Caption = 'Attach'
    Default = True
    ModalResult = 1
    TabOrder = 1
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DBBBBBBBBBBB
      BBB3D8DDDDDDDDDDDDD8DBBB3333333333B3D8DD8888888888D8DBBBBBBBBBBB
      BBB3D8DDDDDDDDDDDDD8DBBB3333333333B3D8DD8888888888D8888888888888
      8883DDDDDDDDDDDDDDD8FFFFFFFF7FFFF7F388888888888888D8F22FF2227F44
      F7F38DD88DDD88FF88D8FF22F2F274FF47F388DD8D8D8F88F8D8FFF22F2274FF
      47F3888DD8DD8F88F8D8F22F22FF74FF47F38DD8DD888F88F8D8F2F2F22F74FF
      47F38D8D8DD88F88F8D8F222FF2274FF47F38DDD88DD8F88F8D8FFFFFFFF74FF
      47F3888888888F88F8D8FFFFFFFFF4FFF7F3888888888F8888D8FFFFFFFFF4FF
      F7F3888888888F8888D8DDDDDDDDDD444DDDDDDDDDDDDDFFFDDD}
    NumGlyphs = 2
  end
  object CancelBtn: TevBitBtn
    Left = 232
    Top = 360
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
      DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
      DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
      9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
      DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
      DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
      DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
      91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
      999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
    NumGlyphs = 2
  end
  object cdsLiabs: TevClientDataSet
    ProviderName = 'CL_CUSTOM_PROV'
    OnCalcFields = cdsLiabsCalcFields
    Left = 141
    Top = 27
    object cdsLiabsLEVEL_: TStringField
      FieldName = 'LEVEL_'
      Size = 1
    end
    object cdsLiabsNBR: TIntegerField
      FieldName = 'NBR'
    end
    object cdsLiabsTYPE: TStringField
      FieldName = 'TAX_TYPE'
      Size = 1
    end
    object cdsLiabsAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object cdsLiabsCHECK_DATE: TDateField
      FieldName = 'CHECK_DATE'
    end
    object cdsLiabsSTATE: TStringField
      FieldName = 'STATE'
      Size = 2
    end
    object cdsLiabsREF_NBR: TIntegerField
      FieldName = 'REF_NBR'
    end
    object cdsLiabsDESCRIPTION: TStringField
      DisplayWidth = 80
      FieldKind = fkInternalCalc
      FieldName = 'DESCRIPTION'
      Size = 80
    end
    object cdsLiabsDUE_DATE: TDateTimeField
      FieldName = 'DUE_DATE'
    end
    object cdsLiabsSTATUS: TStringField
      FieldName = 'STATUS'
      Size = 1
    end
    object cdsLiabsRUN_NUMBER: TIntegerField
      FieldName = 'RUN_NUMBER'
    end
  end
  object dsLiabs: TevDataSource
    DataSet = cdsLiabs
    Left = 180
    Top = 27
  end
end
