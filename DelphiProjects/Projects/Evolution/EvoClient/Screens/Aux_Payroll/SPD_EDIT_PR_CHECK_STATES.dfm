inherited EDIT_PR_CHECK_STATES: TEDIT_PR_CHECK_STATES
  Width = 687
  Height = 555
  inherited PageControl1: TevPageControl
    Top = 66
    Width = 687
    Height = 489
    OnChange = PRPageControlChange
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 679
        Height = 460
        inherited pnlBorder: TevPanel
          Width = 675
          Height = 456
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 675
          Height = 456
          inherited Panel3: TevPanel
            Width = 627
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 627
            Height = 349
            inherited Splitter1: TevSplitter
              Height = 345
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 345
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 265
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_STATES\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 341
              Height = 345
              object evSplitter1: TevSplitter [0]
                Left = 258
                Top = 48
                Height = 265
              end
              inherited PRGrid: TevDBGrid
                Width = 240
                Height = 265
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_STATES\PRGrid'
                Align = alLeft
                PaintOptions.AlternatingRowColor = 14544093
              end
              object evDBGrid1: TevDBGrid
                Left = 261
                Top = 48
                Width = 48
                Height = 265
                TabStop = False
                DisableThemesInTitle = False
                Selected.Strings = (
                  'PERIOD_BEGIN_DATE'#9'10'#9'Period Begin Date'#9
                  'PERIOD_END_DATE'#9'10'#9'Period End Date'#9'No'
                  'FREQUENCY'#9'1'#9'Frequency'#9'No'
                  'PAY_SALARY'#9'1'#9'Pay Salary'#9'No'
                  'PAY_STANDARD_HOURS'#9'1'#9'Pay Standard Hours'#9'No'
                  'LOAD_DBDT_DEFAULTS'#9'1'#9'Load Dbdt Defaults'#9'No')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_STATES\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster1
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Checks'
      ImageIndex = 30
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 0
        Width = 975
        Height = 633
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'EE_Custom_Number'#9'9'#9'EE Custom Nbr'
          'CUSTOM_PR_BANK_ACCT_NUMBER'#9'20'#9'Custom Pr Bank Acct Number'#9
          'PAYMENT_SERIAL_NUMBER'#9'10'#9'Payment Serial Number'#9
          'CHECK_TYPE'#9'1'#9'Check Type'#9
          'CO_DELIVERY_PACKAGE_NBR'#9'10'#9'Co Delivery Package Nbr'#9
          'SALARY'#9'10'#9'Salary'#9
          'EXCLUDE_DD'#9'1'#9'Exclude Dd'#9
          'EXCLUDE_DD_EXCEPT_NET'#9'1'#9'Exclude Dd Except Net'#9
          'EXCLUDE_TIME_OFF_ACCURAL'#9'1'#9'Exclude Time Off Accural'#9
          'EXCLUDE_AUTO_DISTRIBUTION'#9'1'#9'Exclude Auto Distribution'#9
          'EXCLUDE_ALL_SCHED_E_D_CODES'#9'1'#9'Exclude All Sched E D Codes'#9
          'EXCLUDE_SCH_E_D_EXCEPT_PENSION'#9'1'#9'Exclude Sch E D Except Pension'#9
          'OR_CHECK_FEDERAL_VALUE'#9'10'#9'Or Check Federal Value'#9
          'OR_CHECK_FEDERAL_TYPE'#9'1'#9'Or Check Federal Type'#9
          'OR_CHECK_OASDI'#9'10'#9'Or Check Oasdi'#9
          'OR_CHECK_MEDICARE'#9'10'#9'Or Check Medicare'#9
          'OR_CHECK_EIC'#9'10'#9'Or Check Eic'#9
          'OR_CHECK_BACK_UP_WITHHOLDING'#9'10'#9'Or Check Back Up Withholding'#9
          'EXCLUDE_FEDERAL'#9'1'#9'Exclude Federal'#9
          'EXCLUDE_ADDITIONAL_FEDERAL'#9'1'#9'Exclude Additional Federal'#9
          'EXCLUDE_EMPLOYEE_OASDI'#9'1'#9'Exclude Employee Oasdi'#9
          'EXCLUDE_EMPLOYER_OASDI'#9'1'#9'Exclude Employer Oasdi'#9
          'EXCLUDE_EMPLOYEE_MEDICARE'#9'1'#9'Exclude Employee Medicare'#9
          'EXCLUDE_EMPLOYER_MEDICARE'#9'1'#9'Exclude Employer Medicare'#9
          'EXCLUDE_EMPLOYEE_EIC'#9'1'#9'Exclude Employee Eic'#9
          'EXCLUDE_EMPLOYER_FUI'#9'1'#9'Exclude Employer Fui'#9
          'TAX_AT_SUPPLEMENTAL_RATE'#9'1'#9'Tax At Supplemental Rate'#9
          'TAX_FREQUENCY'#9'1'#9'Tax Frequency'#9
          'CHECK_STATUS'#9'1'#9'Check Status'#9
          'STATUS_CHANGE_DATE'#9'10'#9'Status Change Date'#9
          'GROSS_WAGES'#9'10'#9'Gross Wages'#9
          'NET_WAGES'#9'10'#9'Net Wages'#9
          'FEDERAL_TAXABLE_WAGES'#9'10'#9'Federal Taxable Wages'#9
          'FEDERAL_TAX'#9'10'#9'Federal Tax'#9
          'EE_OASDI_TAXABLE_WAGES'#9'10'#9'Ee Oasdi Taxable Wages'#9
          'EE_OASDI_TAXABLE_TIPS'#9'10'#9'Ee Oasdi Taxable Tips'#9
          'EE_OASDI_TAX'#9'10'#9'Ee Oasdi Tax'#9
          'EE_MEDICARE_TAXABLE_WAGES'#9'10'#9'Ee Medicare Taxable Wages'#9
          'EE_MEDICARE_TAX'#9'10'#9'Ee Medicare Tax'#9
          'EE_EIC_TAX'#9'10'#9'Ee Eic Tax'#9
          'ER_OASDI_TAXABLE_WAGES'#9'10'#9'Er Oasdi Taxable Wages'#9
          'ER_OASDI_TAXABLE_TIPS'#9'10'#9'Er Oasdi Taxable Tips'#9
          'ER_OASDI_TAX'#9'10'#9'Er Oasdi Tax'#9
          'ER_MEDICARE_TAXABLE_WAGES'#9'10'#9'Er Medicare Taxable Wages'#9
          'ER_MEDICARE_TAX'#9'10'#9'Er Medicare Tax'#9
          'ER_FUI_TAXABLE_WAGES'#9'10'#9'Er Fui Taxable Wages'#9
          'ER_FUI_TAX'#9'10'#9'Er Fui Tax'#9
          'EXCLUDE_FROM_AGENCY'#9'1'#9'Exclude From Agency'#9
          'FEDERAL_SHORTFALL'#9'10'#9'Federal Shortfall'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_CHECK_STATES\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsSubMaster2
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object Label2: TevLabel
        Left = 8
        Top = 264
        Width = 31
        Height = 13
        Caption = '~State'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TevLabel
        Left = 200
        Top = 264
        Width = 98
        Height = 13
        Caption = 'State Override Value'
        FocusControl = DBEdit8
      end
      object Label3: TevLabel
        Left = 392
        Top = 264
        Width = 95
        Height = 13
        Caption = 'State Override Type'
      end
      object Label6: TevLabel
        Left = 392
        Top = 312
        Width = 78
        Height = 13
        Caption = 'EE SDI Override'
        FocusControl = DBEdit1
      end
      object evLabel3: TevLabel
        Left = 8
        Top = 360
        Width = 145
        Height = 13
        Caption = '~Exclude Additional State'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 3
        Width = 617
        Height = 257
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'State_Lookup'#9'2'#9'State'
          'STATE_TAXABLE_WAGES'#9'10'#9'State Taxable Wages'#9
          'STATE_GROSS_WAGES'#9'10'#9'State Gross Wages'#9
          'STATE_TAX'#9'10'#9'State Tax'#9
          'EE_SDI_TAXABLE_WAGES'#9'10'#9'Ee Sdi Taxable Wages'#9
          'EE_SDI_GROSS_WAGES'#9'10'#9'Ee Sdi Gross Wages'#9
          'EE_SDI_TAX'#9'10'#9'Ee Sdi Tax'#9'No'
          'ER_SDI_TAXABLE_WAGES'#9'10'#9'Er Sdi Taxable Wages'#9'No'
          'ER_SDI_GROSS_WAGES'#9'10'#9'Er Sdi Gross Wages'#9
          'ER_SDI_TAX'#9'10'#9'Er Sdi Tax'#9'No'
          'STATE_SHORTFALL'#9'10'#9'State Shortfall'#9
          'EE_SDI_SHORTFALL'#9'10'#9'Ee Sdi Shortfall'#9'No')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_CHECK_STATES\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object DBEdit8: TevDBEdit
        Left = 200
        Top = 280
        Width = 81
        Height = 21
        DataField = 'STATE_OVERRIDE_VALUE'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwlcEE_STATE: TevDBLookupCombo
        Left = 8
        Top = 280
        Width = 89
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'State_Lookup'#9'2'#9'State_Lookup')
        DataField = 'EE_STATES_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_EE_STATES.EE_STATES
        LookupField = 'EE_STATES_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object DBRadioGroup1: TevDBRadioGroup
        Left = 200
        Top = 360
        Width = 170
        Height = 41
        Caption = '~Tax at Supplemental Rate'
        Columns = 2
        DataField = 'TAX_AT_SUPPLEMENTAL_RATE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 5
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup2: TevDBRadioGroup
        Left = 8
        Top = 308
        Width = 170
        Height = 41
        Caption = '~Exclude State'
        Columns = 2
        DataField = 'EXCLUDE_STATE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup3: TevDBRadioGroup
        Left = 200
        Top = 308
        Width = 170
        Height = 41
        Caption = '~Exclude SDI'
        Columns = 2
        DataField = 'EXCLUDE_SDI'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 4
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup4: TevDBRadioGroup
        Left = 392
        Top = 360
        Width = 170
        Height = 41
        Caption = '~Exclude SUI'
        Columns = 2
        DataField = 'EXCLUDE_SUI'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 8
        Values.Strings = (
          'Y'
          'N')
      end
      object wwDBComboBox1: TevDBComboBox
        Left = 392
        Top = 280
        Width = 121
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'STATE_OVERRIDE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'Regular Amount'#9'A'
          'Regular Percent'#9'P'
          'Additional Amount'#9'M'
          'Additional Percent'#9'E')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object DBEdit1: TevDBEdit
        Left = 392
        Top = 328
        Width = 81
        Height = 21
        DataField = 'EE_SDI_OVERRIDE'
        DataSource = wwdsDetail
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object evDBComboBox1: TevDBComboBox
        Left = 8
        Top = 376
        Width = 169
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'EXCLUDE_ADDITIONAL_STATE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'None'#9'N'
          'Regular Amount'#9'A'
          'Regular Percent'#9'P'
          'Additional Amount'#9'M'
          'Additional Percent'#9'E')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 9
        UnboundDataType = wwDefault
      end
    end
  end
  inherited Panel1: TevPanel
    Width = 687
    inherited pnlFavoriteReport: TevPanel
      Left = 572
    end
  end
  object evPanel1: TevPanel [2]
    Left = 0
    Top = 33
    Width = 687
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object Label1: TevLabel
      Left = 8
      Top = 0
      Width = 44
      Height = 13
      Caption = 'Check #:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText1: TevDBText
      Left = 64
      Top = 0
      Width = 73
      Height = 17
      DataField = 'PAYMENT_SERIAL_NUMBER'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TevLabel
      Left = 249
      Top = 0
      Width = 27
      Height = 13
      Caption = 'EE #:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText4: TevDBText
      Left = 285
      Top = 0
      Width = 85
      Height = 17
      DataField = 'EE_Custom_Number'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TevLabel
      Left = 8
      Top = 16
      Width = 48
      Height = 13
      Caption = 'EE Name:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText5: TevDBText
      Left = 64
      Top = 16
      Width = 156
      Height = 17
      DataField = 'Employee_Name_Calculate'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pnlLocation: TevPanel
      Left = 232
      Top = 12
      Width = 293
      Height = 25
      BevelOuter = bvNone
      TabOrder = 0
      object evLabel2: TevLabel
        Left = 0
        Top = 4
        Width = 41
        Height = 13
        Caption = 'Payroll:#'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evDBText1: TevDBText
        Left = 50
        Top = 4
        Width = 15
        Height = 25
        DataField = 'RUN_NUMBER'
        DataSource = wwdsSubMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evDBText2: TevDBText
        Left = 72
        Top = 4
        Width = 64
        Height = 13
        AutoSize = True
        DataField = 'CHECK_DATE'
        DataSource = wwdsSubMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object evPanel3: TevPanel
        Left = 148
        Top = 4
        Width = 177
        Height = 25
        BevelOuter = bvNone
        TabOrder = 0
        object evDBText3: TevDBText
          Left = 0
          Top = 0
          Width = 64
          Height = 25
          Align = alLeft
          AutoSize = True
          DataField = 'PERIOD_BEGIN_DATE'
          DataSource = wwdsSubMaster1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object evLabel4: TevLabel
          Left = 64
          Top = 0
          Width = 9
          Height = 25
          Align = alLeft
          Caption = ' - '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText4: TevDBText
          Left = 73
          Top = 0
          Width = 64
          Height = 25
          Align = alLeft
          AutoSize = True
          DataField = 'PERIOD_END_DATE'
          DataSource = wwdsSubMaster1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object butnPrior: TevBitBtn
      Left = 528
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Prior'
      TabOrder = 1
      OnClick = butnPriorClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDD
        848DDDDDDDDDDDDDF8FDDDDDDDDDDD84448DDDDDDDDDDDF888FDDDDDDDDD844F
        F48DDDDDDDDDF88DD8FDDDDDDD844FFFF48DDDDDDDF88DDDD8FDDDDD844FFFFF
        F48DDDDDF88DDDDDD8FDDDD44FFFFFFFF48DDDD88DDDDDDDD8FDDD477FFFFFFF
        F48DDD8FFDDDDDDDD8FDDDD4477FFFFFF48DDDD88FFDDDDDD8FDDDDDD4477FFF
        F48DDDDDD88FFDDDD8FDDDDDDDD4477FF48DDDDDDDD88FFDD8FDDDDDDDDDD447
        748DDDDDDDDDD88FF8FDDDDDDDDDDDD4448DDDDDDDDDDDD888FDDDDDDDDDDDDD
        D4DDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnNext: TevBitBtn
      Left = 608
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Next'
      TabOrder = 2
      OnClick = butnNextClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDDD4888DDDDDDD
        DDDDD8FFDDDDDDDDDDDDD444888DDDDDDDDDD888FFDDDDDDDDDDD47F44888DDD
        DDDDD8FD88FFDDDDDDDDD47FFF44888DDDDDD8FDDD88FFDDDDDDD47FFFFF4488
        8DDDD8FDDDDD88FFDDDDD47FFFFFFF4488DDD8FDDDDDDD88FDDDD47FFFFFFF77
        4DDDD8FDDDDDDDFF8DDDD47FFFFF7744DDDDD8FDDDDDFF88DDDDD47FFF7744DD
        DDDDD8FDDDFF88DDDDDDD47F7744DDDDDDDDD8FDFF88DDDDDDDDD47744DDDDDD
        DDDDD8FF88DDDDDDDDDDD444DDDDDDDDDDDDD888DDDDDDDDDDDDD4DDDDDDDDDD
        DDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 65530
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_PR_CHECK_STATES.PR_CHECK_STATES
    MasterDataSource = wwdsSubMaster2
    Left = 206
    Top = 2
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 329
    Top = 119
  end
  inherited dsCL: TevDataSource
    Left = 295
    Top = 111
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 376
    Top = 127
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 434
    Top = 114
  end
  inherited wwdsSubMaster: TevDataSource
    OnDataChange = wwdsSubMasterDataChange
    Top = 88
  end
  inherited DM_PAYROLL: TDM_PAYROLL
    Left = 463
    Top = 114
  end
  inherited ActionList: TevActionList
    Left = 435
    Top = 155
    inherited PRNext: TDataSetNext
      OnExecute = PRNextExecute
    end
    inherited PRPrior: TDataSetPrior
      OnExecute = PRPriorExecute
    end
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 507
    Top = 106
  end
  object EESrc: TevDataSource
    DataSet = DM_EE.EE
    Left = 495
    Top = 72
  end
  object EEStatesSrc: TevDataSource
    DataSet = DM_EE_STATES.EE_STATES
    MasterDataSource = EESrc
    Left = 528
    Top = 72
  end
  object wwdsSubMaster1: TevDataSource
    DataSet = DM_PR_BATCH.PR_BATCH
    MasterDataSource = wwdsSubMaster
    Left = 204
    Top = 104
  end
  object wwdsSubMaster2: TevDataSource
    DataSet = DM_PR_CHECK.PR_CHECK
    OnDataChange = wwdsDataChange
    MasterDataSource = wwdsSubMaster1
    OnDataChangeBeforeChildren = EESrcDataChangeBeforeChildren
    Left = 248
    Top = 114
  end
end
