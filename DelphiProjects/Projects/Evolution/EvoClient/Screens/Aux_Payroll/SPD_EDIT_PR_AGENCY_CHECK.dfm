inherited EDIT_PR_AGENCY_CHECK: TEDIT_PR_AGENCY_CHECK
  inherited PageControl1: TevPageControl
    Top = 66
    Height = 200
    ActivePage = TabSheet3
    OnChange = PRPageControlChange
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Height = 171
        inherited pnlBorder: TevPanel
          Height = 167
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Height = 167
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Height = 60
            inherited Splitter1: TevSplitter
              Height = 56
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 56
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 10
                IniAttributes.SectionName = 'TEDIT_PR_AGENCY_CHECK\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Height = 56
              inherited PRGrid: TevDBGrid
                Height = 10
                IniAttributes.SectionName = 'TEDIT_PR_AGENCY_CHECK\PRGrid'
              end
            end
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object Label1: TevLabel
        Left = 5
        Top = 208
        Width = 70
        Height = 13
        Caption = 'Check Amount'
        FocusControl = DBEdit1
      end
      object Label2: TevLabel
        Left = 105
        Top = 208
        Width = 139
        Height = 13
        Caption = 'Custom Pr Bank Account Nbr'
        FocusControl = DBEdit2
      end
      object Label3: TevLabel
        Left = 370
        Top = 208
        Width = 90
        Height = 13
        Caption = 'Payment Serial Nbr'
        FocusControl = DBEdit3
      end
      object Label4: TevLabel
        Left = 5
        Top = 160
        Width = 52
        Height = 13
        Caption = 'CL Agency'
      end
      object Label5: TevLabel
        Left = 5
        Top = 256
        Width = 95
        Height = 13
        Caption = 'Override Information'
      end
      object Label9: TevLabel
        Left = 320
        Top = 160
        Width = 90
        Height = 13
        Caption = 'Global Tax Agency'
      end
      object evLabel2: TevLabel
        Left = 475
        Top = 208
        Width = 109
        Height = 13
        Caption = 'GL Agency Field Office'
      end
      object evDBText1: TevDBText
        Left = 630
        Top = 228
        Width = 40
        Height = 17
        DataField = 'Gl_Field_Office_State'
        DataSource = wwdsDetail
      end
      object evLabel3: TevLabel
        Left = 260
        Top = 208
        Width = 61
        Height = 13
        Caption = 'ABA Number'
        FocusControl = DBEdit2
      end
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 3
        Width = 680
        Height = 153
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'CUSTOM_PR_BANK_ACCT_NUMBER'#9'20'#9'Custom Pr Bank Acct Number'#9
          'PAYMENT_SERIAL_NUMBER'#9'10'#9'Payment Serial Number'#9'No'
          'MISCELLANEOUS_CHECK_AMOUNT'#9'10'#9'Miscellaneous Check Amount'#9
          'CHECK_STATUS'#9'1'#9'Check Status'#9'No'
          'EFTPS_TAX_TYPE'#9'1'#9'Eftps Tax Type'#9
          'CL_AGENCY_NBR'#9'10'#9'Cl Agency Nbr'#9
          'CO_TAX_DEPOSITS_NBR'#9'10'#9'Co Tax Deposits Nbr'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_AGENCY_CHECK\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object DBEdit1: TevDBEdit
        Left = 5
        Top = 224
        Width = 90
        Height = 21
        DataField = 'MISCELLANEOUS_CHECK_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit2: TevDBEdit
        Left = 105
        Top = 224
        Width = 145
        Height = 21
        DataField = 'CUSTOM_PR_BANK_ACCT_NUMBER'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit3: TevDBEdit
        Left = 370
        Top = 224
        Width = 95
        Height = 21
        DataField = 'PAYMENT_SERIAL_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwlcCL_AGENCY: TevDBLookupCombo
        Left = 5
        Top = 176
        Width = 305
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Agency_Name'#9'40'#9'Agency_Name')
        DataField = 'CL_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CL_AGENCY.CL_AGENCY
        LookupField = 'CL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object DBMemo1: TEvDBMemo
        Left = 5
        Top = 272
        Width = 675
        Height = 37
        DataField = 'OVERRIDE_INFORMATION'
        DataSource = wwdsDetail
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
      end
      object Memo1: TevMemo
        Left = 5
        Top = 320
        Width = 532
        Height = 81
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 9
        WordWrap = False
      end
      object BitBtn11: TevBitBtn
        Left = 550
        Top = 320
        Width = 130
        Height = 33
        Caption = 'Show Stub Details'
        Default = True
        TabOrder = 10
        OnClick = BitBtn11Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD8888444444
          4444DD888DFFFFFFFFFFD8FBB34FFFFFFFF4D8DDDD888888888F8B33334FFFFF
          FFF48D888D888888888F83FBB34F744447F488DDDD88DDDDD88F8B33334FFFFF
          FFF48D888D888888888F83BBB34F744447F488DDDD88DDDDD88F8BBBB34FFFFF
          FFF48DDDDD888888888F08BBB34F744447F4D8DDDD88DDDDD88F0788884FFFFF
          FFF48D888D888888888F0778884F744447F48DDDDD88DDDDD88F0777784FFFFF
          FFF48DDDDD888888888F0778884FFFFFFFF48DD88D888888888F077887444444
          44448DD88DDDDDDDDDDD077777777788870D8DDDDDDDDDD888D8077777777777
          770D8DDDDDDDDDDDDDD8000000000000000D8888888888888888}
        NumGlyphs = 2
        Margin = 0
      end
      object BitBtn2: TevBitBtn
        Left = 550
        Top = 368
        Width = 130
        Height = 33
        Caption = 'Copy Stub To Override'
        TabOrder = 11
        OnClick = BitBtn2Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD444444
          4444DDDDDD8888888888DDDDDD4777777774DDDDDD8DDDDDDDD8D88888488888
          7774DDDDDD8DDDDDDDD80000000000044774DDDDDD8DD8888DD80FFFFFFFFF08
          7774FFFFFFFFFFDDDDD80FFFFFFFFF044774888888888F888DD80F7000007F08
          7774888888888FDDDDD80FFFFFFFFF04477488DDDDD88F888DD80F7000007F08
          7774888888888FDDDDD80FFFFFFFFF04477488DDDDD88F888DD80F7000007F08
          7774888888888FDDDDD80FFFFFFFFF08777488DDDDD88FDDDDD80F7000007F04
          4444888888888F8888880FFFFFFFFF08DDDD88DDDDD88FDDDDDD0FFFFFFFFF08
          DDDD888888888FDDDDDD00000000000DDDDD888888888FDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object wwlcGLOBAL_TAX_AGENCY: TevDBLookupCombo
        Left = 320
        Top = 176
        Width = 250
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Agency_Name'#9'40'#9'Agency_Name')
        DataField = 'SY_GLOBAL_AGENCY_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
        LookupField = 'SY_GLOBAL_AGENCY_NBR'
        Style = csDropDownList
        TabOrder = 2
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object evDBLookupCombo1: TevDBLookupCombo
        Left = 475
        Top = 224
        Width = 150
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'ADDITIONAL_NAME'#9'35'#9'ADDITIONAL_NAME'#9'F'
          'STATE'#9'2'#9'STATE'#9'F')
        DataField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SY_GL_AGENCY_FIELD_OFFICE.SY_GL_AGENCY_FIELD_OFFICE
        LookupField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        Style = csDropDownList
        TabOrder = 7
        AutoDropDown = True
        ShowButton = True
        OrderByDisplay = False
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object btnUnvoid: TevBitBtn
        Left = 580
        Top = 172
        Width = 100
        Height = 25
        Caption = 'Unvoid check'
        TabOrder = 12
        OnClick = btnUnvoidClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD28DDD
          DDDDDDDDDDDFDDDDDDDDDDDDDD2A28DDDDDDDDDDDDF8FDDDDDDDDDDDD2AA28DD
          DDDDDDDDDF88FDDDDDDDDDDD2AAAA28DDDDDDDDDF8888FDDDDDDDDD2AAAAAA28
          DDDDDDDF888888FDDDDDDD2AA8D2AA28DDDDDDF88DDD88FDDDDDDDA8D888AAA2
          8888DD8DDDDD888FDDDD888888000AA28000DDDDDD88D88FD888000000877AAA
          28708888888DD888FDD80FFFFFF877AAA2708DDDDDD8DD888FD80FF7777888AA
          A2808DDDDDD88D888FD80FF88888777A27708DD88888DDD8FDD80FF88FFF8778
          88708DD88DDD8DDD88D80FFFFFFFF87777708DDDDDDDD8DDDDD80FFFFFFFFF00
          00008DDDDDDDDD88888800000000008DDDDD8888888888DDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object edABANumber: TevDBEdit
        Left = 260
        Top = 224
        Width = 100
        Height = 21
        DataField = 'ABA_NUMBER'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  object evPanel1: TevPanel [2]
    Left = 0
    Top = 33
    Width = 435
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object Label6: TevLabel
      Left = 344
      Top = 8
      Width = 63
      Height = 13
      Caption = 'Run Number:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText1: TevDBText
      Left = 416
      Top = 8
      Width = 25
      Height = 17
      DataField = 'RUN_NUMBER'
      DataSource = wwdsSubMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TevLabel
      Left = 160
      Top = 8
      Width = 60
      Height = 13
      Caption = 'Check Date:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText2: TevDBText
      Left = 224
      Top = 8
      Width = 105
      Height = 17
      DataField = 'CHECK_DATE'
      DataSource = wwdsSubMaster
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TevLabel
      Left = 8
      Top = 8
      Width = 70
      Height = 13
      Caption = 'Agency Check'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object BitBtn3: TevBitBtn
      Left = 520
      Top = 8
      Width = 99
      Height = 25
      Caption = 'Print Check'
      TabOrder = 0
      Visible = False
      OnClick = BitBtn3Click
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
        08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
        8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
        8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
        8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
        78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
        7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
        DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
        DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 66
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_PR_MISCELLANEOUS_CHECKS.PR_MISCELLANEOUS_CHECKS
    OnDataChange = wwdsDetailDataChange
    Left = 206
    Top = 66
  end
  inherited wwdsList: TevDataSource
    Left = 98
    Top = 66
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Left = 305
    Top = 63
  end
  inherited dsCL: TevDataSource
    Left = 263
    Top = 71
  end
  inherited DM_CLIENT: TDM_CLIENT
    Top = 71
  end
  inherited DM_COMPANY: TDM_COMPANY
    Left = 386
    Top = 74
  end
  inherited wwdsSubMaster: TevDataSource
    Left = 182
    Top = 72
  end
  inherited DM_PAYROLL: TDM_PAYROLL
    Left = 431
    Top = 74
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 474
    Top = 57
  end
end
