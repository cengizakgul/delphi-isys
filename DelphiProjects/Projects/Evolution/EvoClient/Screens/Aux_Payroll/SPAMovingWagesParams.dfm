object PAMovingWagesParams: TPAMovingWagesParams
  Left = 335
  Top = 159
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Moving PA Local Wages Utility'
  ClientHeight = 670
  ClientWidth = 873
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lablEmployees: TevLabel
    Left = 16
    Top = 17
    Width = 46
    Height = 13
    Caption = 'Employee'
  end
  object ApplyBtn: TevBitBtn
    Left = 747
    Top = 12
    Width = 113
    Height = 25
    Caption = 'Create Checks'
    Default = True
    Enabled = False
    TabOrder = 0
    OnClick = ApplyBtnClick
    NumGlyphs = 2
    Margin = 0
  end
  object SourceGrid: TevDBGrid
    Left = 8
    Top = 50
    Width = 857
    Height = 293
    HelpContext = 1502
    DisableThemesInTitle = False
    Selected.Strings = (
      'CHECK_DATE'#9'18'#9'Check Date'#9'F'
      'E_D_CODE_TYPE'#9'8'#9'E/D'#9'F'
      'GROSS_WAGE'#9'10'#9'Gross Wages'#9'F'
      'TAXABLE_WAGE'#9'10'#9'Taxable Wages'#9'F'
      'HOURS'#9'10'#9'Hours'#9'F'
      'DIVISION'#9'10'#9'Division'#9'F'
      'BRANCH'#9'10'#9'Branch'#9'F'
      'DEPARTMENT'#9'10'#9'Department'#9'F'
      'TEAM'#9'10'#9'Team'#9'F'
      'JOB'#9'10'#9'Job'#9'F'
      'N_NAME'#9'30'#9'Non Residential Tax'#9'F'
      'R_NAME'#9'30'#9'Residential Tax'#9'F'
      'S_NAME'#9'30'#9'School Tax'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TPAMovingWagesParams\SourceGrid'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    DefaultSort = 'CHECK_DATE'
    NoFire = False
  end
  object MoveBtn: TevBitBtn
    Left = 8
    Top = 352
    Width = 105
    Height = 25
    Caption = 'Move >>'
    Default = True
    Enabled = False
    TabOrder = 2
    OnClick = MoveBtnClick
    NumGlyphs = 2
    Margin = 0
  end
  object BackBtn: TevBitBtn
    Left = 776
    Top = 352
    Width = 89
    Height = 25
    Caption = '<< Undo'
    Default = True
    Enabled = False
    TabOrder = 3
    OnClick = BackBtnClick
    NumGlyphs = 2
    Margin = 0
  end
  object DestGrid: TevDBGrid
    Left = 8
    Top = 384
    Width = 857
    Height = 273
    HelpContext = 1502
    DisableThemesInTitle = False
    Selected.Strings = (
      'CHECK_DATE'#9'18'#9'Check Date'#9'F'
      'E_D_CODE_TYPE'#9'8'#9'E/D'#9'F'
      'TAXABLE_WAGE'#9'19'#9'Taxable Wages'#9'F'
      'HOURS'#9'10'#9'Hours'#9'F'
      'DDIVISION'#9'10'#9'Division'#9'F'
      'DBRANCH'#9'10'#9'Branch'#9'F'
      'DDEPARTMENT'#9'10'#9'Department'#9'F'
      'DTEAM'#9'10'#9'Team'#9'F'
      'DJOB'#9'10'#9'Job'#9'F'
      'DN_NAME'#9'30'#9'Non Residential Tax'#9'F'
      'DR_NAME'#9'30'#9'Residential Tax'#9'F'
      'DS_NAME'#9'30'#9'School Tax'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TPAMovingWagesParams\DestGrid'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsDest
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 4
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    DefaultSort = 'CHECK_DATE'
    NoFire = False
  end
  object cbEmployee: TevComboBox
    Left = 72
    Top = 15
    Width = 401
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    OnChange = cbEmployeeChange
  end
  object dsSource: TevDataSource
    Left = 488
    Top = 16
  end
  object dsDest: TevDataSource
    Left = 520
    Top = 16
  end
end
