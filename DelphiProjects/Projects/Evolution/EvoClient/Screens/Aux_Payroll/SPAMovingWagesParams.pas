// Copyright � 2000-2012 iSystems LLC. All rights reserved.
unit SPAMovingWagesParams;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls, EvBasicUtils,
  ISBasicClasses, Grids, Wwdbigrd, Wwdbgrid, SQECLocalTaxes, EvCommonInterfaces,
  EvDataset, DateUtils, SDataStructure, EvContext, DB, Wwdatsrc,
  SPD_DBDTSelectionListFiltr, SPAMoveOneDlg, EvConsts, wwdblook, Variants,
  EvUIComponents, EvSQLite, EvDataAccessComponents, LMDCustomButton,
  LMDButton, isUILMDButton;

type
  TPAMovingWagesParams = class(TForm)
    ApplyBtn: TevBitBtn;
    lablEmployees: TevLabel;
    SourceGrid: TevDBGrid;
    MoveBtn: TevBitBtn;
    BackBtn: TevBitBtn;
    DestGrid: TevDBGrid;
    dsSource: TevDataSource;
    dsDest: TevDataSource;
    cbEmployee: TevComboBox;
    procedure FormShow(Sender: TObject);
    procedure ShowDate(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure MoveBtnClick(Sender: TObject);
    procedure ApplyBtnClick(Sender: TObject);
    procedure BackBtnClick(Sender: TObject);
    procedure cbEmployeeChange(Sender: TObject);
  private
    FSQLite: ISQLite;
    FSWages: IevDataset;
    FDWages: IevDataset;
    FEENbr: Integer;
  public
    FYear: Integer;
    procedure Load;
    procedure LoadEe;
  end;


implementation

uses
  EvUtils;

{$R *.DFM}

procedure TPAMovingWagesParams.FormShow(Sender: TObject);
begin
  Load;
end;

procedure TPAMovingWagesParams.Load;
var
  D: IevDataset;
begin
  if not DM_PAYROLL.PR_CHECK_LINES.Active then
    DM_PAYROLL.PR_CHECK_LINES.Open;
  if not DM_PAYROLL.PR_CHECK_LINE_LOCALS.Active then
    DM_PAYROLL.PR_CHECK_LINE_LOCALS.Open;

//  if not Assigned(FSQLite) then
  begin
    ctx_StartWait('Please wait...');
    try
      FSQLite := TSQLite.Create(':memory:');
      FSQLite.StartTransaction;
      LoadData(FSQLite, DM_COMPANY.CO.CO_NBR.AsInteger, 0,  EncodeDate(FYear,12,31), EncodeDate(FYear,1,1));
      CreateIndexes(FSQLite);
      CalculateLocalTaxesInt(FSQLite, DM_COMPANY.CO.CO_NBR.AsInteger, 0, EncodeDate(FYear,12,31), EncodeDate(FYear,1,1));

      FSQLite.Execute(
      '  create table SOURCE_WAGES as'+
      '  select'+
      '  x.EE_NBR EE_NBR,'+
      '  x.CHECK_DATE CHECK_DATE,'+
      '  x.CL_E_DS_NBR CL_E_DS_NBR,'+
      '  (select MAX(z.CUSTOM_E_D_CODE_NUMBER) from CL_E_DS z where z.CL_E_DS_NBR=x.CL_E_DS_NBR) E_D_CODE_TYPE,'+
      '  x.CO_DIVISION_NBR CO_DIVISION_NBR,'+
      '  x.CO_BRANCH_NBR CO_BRANCH_NBR,'+
      '  x.CO_DEPARTMENT_NBR CO_DEPARTMENT_NBR,'+
      '  x.CO_TEAM_NBR CO_TEAM_NBR,'+
      '  ifnull(x.CO_JOBS_NBR,0) CO_JOBS_NBR,'+
      '  o1.CUSTOM_DIVISION_NUMBER DIVISION,'+
      '  o2.CUSTOM_BRANCH_NUMBER BRANCH,'+
      '  o3.CUSTOM_DEPARTMENT_NUMBER DEPARTMENT,'+
      '  o4.CUSTOM_TEAM_NUMBER TEAM,'+
      '  (select MAX(q.DESCRIPTION) from CO_JOBS q where q.CO_JOBS_NBR=x.CO_JOBS_NBR) JOB,'+
      '  ifnull(n.NAME,'''') N_NAME,'+
      '  ifnull(r.NAME,'''') R_NAME,'+
      '  ifnull(s.NAME,'''') S_NAME,'+
      '  ifnull(n.EE_LOCALS_NBR,0) N_EE_LOCALS_NBR,'+
      '  ifnull(r.EE_LOCALS_NBR,0) R_EE_LOCALS_NBR,'+
      '  ifnull(s.EE_LOCALS_NBR,0) S_EE_LOCALS_NBR,'+
      '  cast('+
      '  SUM('+
      '  case'+
      '  when n.NAME is not null then n.TAXABLE_WAGE'+
      '  when r.NAME is not null then r.TAXABLE_WAGE'+
      '  when s.NAME is not null then s.TAXABLE_WAGE'+
      '  else 0'+
      '  end) as REAL) TAXABLE_WAGE,'+
      '  SUM(HOURS_OR_PIECES) HOURS,'+
      '  SUM(x.AMOUNT) GROSS_WAGE'+
      '  from PR_CHECK_LINES x'+
      '  left join QEC_PA_LOCAL_TAXES n on n.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and n.LOCAL_TYPE=''N'''+
      '  left join QEC_PA_LOCAL_TAXES r on r.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and r.LOCAL_TYPE=''R'''+
      '  left join QEC_PA_LOCAL_TAXES s on s.PR_CHECK_LINES_NBR=x.PR_CHECK_LINES_NBR and s.LOCAL_TYPE=''S'''+
      '  left join CO_DIVISION o1 on o1.CO_DIVISION_NBR=x.CO_DIVISION_NBR'+
      '  left join CO_BRANCH o2 on o2.CO_BRANCH_NBR=x.CO_BRANCH_NBR'+
      '  left join CO_DEPARTMENT o3 on o3.CO_DEPARTMENT_NBR=x.CO_DEPARTMENT_NBR'+
      '  left join CO_TEAM o4 on o4.CO_TEAM_NBR=x.CO_TEAM_NBR'+      
      '  where n.NAME is not null or r.NAME is not null or s.NAME is not null'+
      '  group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20'+
      '  order by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20');

      FSQLite.Execute(
      '  create table DEST_WAGES as'+
      '  select * from SOURCE_WAGES where 1=2');

      FSQLite.Execute('delete from SOURCE_WAGES where TAXABLE_WAGE = 0');

      FSQLite.Execute('alter table DEST_WAGES add column DN_EE_LOCALS_NBR INT');
      FSQLite.Execute('alter table DEST_WAGES add column DR_EE_LOCALS_NBR INT');
      FSQLite.Execute('alter table DEST_WAGES add column DS_EE_LOCALS_NBR INT');
      FSQLite.Execute('alter table DEST_WAGES add column DN_NAME TEXT');
      FSQLite.Execute('alter table DEST_WAGES add column DR_NAME TEXT');
      FSQLite.Execute('alter table DEST_WAGES add column DS_NAME TEXT');

      FSQLite.Execute('alter table DEST_WAGES add column DCO_DIVISION_NBR INT');
      FSQLite.Execute('alter table DEST_WAGES add column DCO_BRANCH_NBR INT');
      FSQLite.Execute('alter table DEST_WAGES add column DCO_DEPARTMENT_NBR INT');
      FSQLite.Execute('alter table DEST_WAGES add column DCO_TEAM_NBR INT');
      FSQLite.Execute('alter table DEST_WAGES add column DCO_JOBS_NBR INT');      

      D := FSQLite.Select('select e.CUSTOM_EMPLOYEE_NUMBER, e.EE_NBR, p.FIRST_NAME, p.LAST_NAME from EE e join CL_PERSON p on p.CL_PERSON_NBR=e.CL_PERSON_NBR',[]);

      cbEmployee.Clear;

      D.First;
      while not D.Eof do
      begin
        cbEmployee.Items.AddObject(
          Trim(D.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString)+
          ' ' + Trim(D.FieldByName('FIRST_NAME').AsString) +
          ' ' + Trim(D.FieldByName('LAST_NAME').AsString), TObject(D.FieldByName('EE_NBR').AsInteger));
        D.Next;
      end;

      cbEmployee.ItemIndex := 0;

      FSQLite.Commit;
    finally
      ctx_EndWait;
    end;
  end;
end;

procedure TPAMovingWagesParams.LoadEe;
begin

  SourceGrid.DataSource.Dataset := nil;

  FEENbr := Integer(cbEmployee.Items.Objects[cbEmployee.ItemIndex]);

  FSWages := FSQLite.Select('select * from SOURCE_WAGES where EE_NBR=:EE_NBR order by CHECK_DATE', [FEENbr]);
  FDWages := FSQLite.Select('select *,'+
  '(select CUSTOM_DIVISION_NUMBER from CO_DIVISION where CO_DIVISION_NBR=DCO_DIVISION_NBR) DDIVISION,'+
  '(select CUSTOM_BRANCH_NUMBER from CO_BRANCH where CO_BRANCH_NBR=DCO_BRANCH_NBR) DBRANCH,'+
  '(select CUSTOM_DEPARTMENT_NUMBER from CO_DEPARTMENT where CO_DEPARTMENT_NBR=DCO_DEPARTMENT_NBR) DDEPARTMENT,'+
  '(select CUSTOM_TEAM_NUMBER from CO_TEAM where CO_TEAM_NBR=DCO_TEAM_NBR) DTEAM,'+
  '(select DESCRIPTION from CO_JOBS where CO_JOBS_NBR=DCO_JOBS_NBR) DJOB'+      
  ' from DEST_WAGES where EE_NBR=:EE_NBR order by CHECK_DATE', [FEENbr]);

  FSWages.vclDataset.FieldByName('CHECK_DATE').OnGetText := ShowDate;
  FDWages.vclDataset.FieldByName('CHECK_DATE').OnGetText := ShowDate;

  SourceGrid.DataSource.Dataset := FSWages.vclDataset;
  DestGrid.DataSource.Dataset := FDWages.vclDataset;
  MoveBtn.Enabled := not FSWages.vclDataset.IsEmpty;
  BackBtn.Enabled := not FDWages.vclDataset.IsEmpty;

  ApplyBtn.Enabled := not FDWages.vclDataset.IsEmpty;

end;

procedure TPAMovingWagesParams.ShowDate(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if not Sender.DataSet.IsEmpty then
    Text := DateToStr(Sender.AsInteger);
end;

procedure TPAMovingWagesParams.MoveBtnClick(Sender: TObject);
var
  OneDlg: TMoveOneDlg;
  D, X: IevDataset;
  NName, RName, SName: String;
  DNName, DRName, DSName: String;
  DNNbr, DRNbr, DSNbr: Integer;
  Amount, Hours: Double;
  SQL, DBDT: String;
begin
  if not FSWages.vclDataset.IsEmpty then
  begin
    if SourceGrid.SelectedList.Count < 2 then
    begin
      OneDlg := TMoveOneDlg.Create(nil);
      try
        OneDlg.MaxAmount := FSWages.FieldByName('TAXABLE_WAGE').AsFloat;
        OneDlg.AmountEdit.Text := FSWages.FieldByName('TAXABLE_WAGE').AsString;
        OneDlg.MaxHours := StrToFloatDef(FSWages.FieldByName('HOURS').AsString,0);
        if Abs(OneDlg.MaxHours) > 0.005 then
        begin
          OneDlg.HoursEdit.Text := FSWages.FieldByName('HOURS').AsString;
          OneDlg.HoursEdit.Enabled := True;
        end
        else
        begin
          OneDlg.HoursEdit.Text := '';
          OneDlg.HoursEdit.Enabled := False;
        end;

        OneDlg.SQLite := FSQLite;

        OneDlg.Division := FSWages.FieldByName('CO_DIVISION_NBR').Value;
        OneDlg.Branch := FSWages.FieldByName('CO_BRANCH_NBR').Value;
        OneDlg.Department := FSWages.FieldByName('CO_DEPARTMENT_NBR').Value;
        OneDlg.Team := FSWages.FieldByName('CO_TEAM_NBR').Value;

        OneDlg.DbdtLabel.Caption := '';
        if FSWages.FieldByName('CO_DIVISION_NBR').AsInteger <> 0 then
        begin
          X := FSQLite.Select('select CUSTOM_DIVISION_NUMBER from CO_DIVISION where CO_DIVISION_NBR=:NBR',
            [FSWages.FieldByName('CO_DIVISION_NBR').AsInteger]);
          OneDlg.DbdtLabel.Caption := X.Fields[0].AsString;
          if FSWages.FieldByName('CO_BRANCH_NBR').AsInteger <> 0 then
          begin
            X := FSQLite.Select('select CUSTOM_BRANCH_NUMBER from CO_BRANCH where CO_BRANCH_NBR=:NBR',
              [FSWages.FieldByName('CO_BRANCH_NBR').AsInteger]);
            OneDlg.DbdtLabel.Caption := OneDlg.DbdtLabel.Caption + '/' + X.Fields[0].AsString;
          end;
          if FSWages.FieldByName('CO_DEPARTMENT_NBR').AsInteger <> 0 then
          begin
            X := FSQLite.Select('select CUSTOM_DEPARTMENT_NUMBER from CO_DEPARTMENT where CO_DEPARTMENT_NBR=:NBR',
              [FSWages.FieldByName('CO_DEPARTMENT_NBR').AsInteger]);
            OneDlg.DbdtLabel.Caption := OneDlg.DbdtLabel.Caption + '/' + X.Fields[0].AsString;
          end;
          if FSWages.FieldByName('CO_TEAM_NBR').AsInteger <> 0 then
          begin
            X := FSQLite.Select('select CUSTOM_TEAM_NUMBER from CO_TEAM where CO_TEAM_NBR=:NBR',
              [FSWages.FieldByName('CO_TEAM_NBR').AsInteger]);
            OneDlg.DbdtLabel.Caption := OneDlg.DbdtLabel.Caption + '/' + X.Fields[0].AsString;
          end;
        end;

        OneDlg.cbJob.Items.AddObject('', TObject(0));
        OneDlg.cbJob.ItemIndex := 0;

        D := FSQLite.Select('select CO_JOBS_NBR, DESCRIPTION from CO_JOBS', []);

        while not D.Eof do
        begin
          OneDlg.cbJob.Items.AddObject(D.FieldByName('DESCRIPTION').AsString,
                                      TObject(Pointer(D.FieldByName('CO_JOBS_NBR').AsInteger)));
          if D.FieldByName('CO_JOBS_NBR').AsInteger = FSWages.FieldByName('CO_JOBS_NBR').AsInteger then
            OneDlg.cbJob.ItemIndex := OneDlg.cbJob.Items.Count - 1;
            
          D.Next;
        end;

        D := FSQLite.Select(
        '  select x.EE_LOCALS_NBR, s.NAME'+
        '  from EE_LOCALS x'+
        '  join CO_LOCAL_TAX c on c.CO_LOCAL_TAX_NBR=x.CO_LOCAL_TAX_NBR'+
        '  join SY_LOCALS s on s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
        '  where x.CHECK_DATE=(select MAX(CHECK_DATE) from EE_LOCALS)'+
        '  and c.CHECK_DATE=(select MAX(CHECK_DATE) from CO_LOCAL_TAX)'+
        '  and s.CHECK_DATE=(select MAX(CHECK_DATE) from SY_LOCALS)'+
        '  and x.EE_NBR=:EE_NBR and s.LOCAL_TYPE=:LOCAL_TYPE',
        [FEENbr, 'N']);
        D.First;
        OneDlg.cbNR.Items.AddObject('', TObject(0));
        OneDlg.cbNR.ItemIndex := 0;
        while not D.Eof do
        begin
          OneDlg.cbNR.Items.AddObject(D.FieldByName('NAME').AsString, TObject(Pointer(D.FieldByName('EE_LOCALS_NBR').AsInteger)));
          if D.FieldByName('EE_LOCALS_NBR').AsInteger = FSWages.FieldByName('N_EE_LOCALS_NBR').AsInteger then
            OneDlg.cbNR.ItemIndex := OneDlg.cbNR.Items.Count - 1;
          D.Next;
        end;
        //----
        D := FSQLite.Select(
        '  select x.EE_LOCALS_NBR, s.NAME'+
        '  from EE_LOCALS x'+
        '  join CO_LOCAL_TAX c on c.CO_LOCAL_TAX_NBR=x.CO_LOCAL_TAX_NBR'+
        '  join SY_LOCALS s on s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
        '  where x.CHECK_DATE=(select MAX(CHECK_DATE) from EE_LOCALS)'+
        '  and c.CHECK_DATE=(select MAX(CHECK_DATE) from CO_LOCAL_TAX)'+
        '  and s.CHECK_DATE=(select MAX(CHECK_DATE) from SY_LOCALS)'+
        '  and x.EE_NBR=:EE_NBR and s.LOCAL_TYPE=:LOCAL_TYPE',
        [FEENbr, 'R']);
        D.First;
        OneDlg.cbRE.Items.AddObject('', TObject(0));
        OneDlg.cbRE.ItemIndex := 0;
        while not D.Eof do
        begin
          OneDlg.cbRE.Items.AddObject(D.FieldByName('NAME').AsString, TObject(Pointer(D.FieldByName('EE_LOCALS_NBR').AsInteger)));
          if D.FieldByName('EE_LOCALS_NBR').AsInteger = FSWages.FieldByName('R_EE_LOCALS_NBR').AsInteger then
            OneDlg.cbRE.ItemIndex := OneDlg.cbRE.Items.Count - 1;
          D.Next;
        end;

        //----
        D := FSQLite.Select(
        '  select x.EE_LOCALS_NBR, s.NAME'+
        '  from EE_LOCALS x'+
        '  join CO_LOCAL_TAX c on c.CO_LOCAL_TAX_NBR=x.CO_LOCAL_TAX_NBR'+
        '  join SY_LOCALS s on s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
        '  where x.CHECK_DATE=(select MAX(CHECK_DATE) from EE_LOCALS)'+
        '  and c.CHECK_DATE=(select MAX(CHECK_DATE) from CO_LOCAL_TAX)'+
        '  and s.CHECK_DATE=(select MAX(CHECK_DATE) from SY_LOCALS)'+
        '  and x.EE_NBR=:EE_NBR and s.LOCAL_TYPE=:LOCAL_TYPE',
        [FEENbr, 'S']);
        D.First;
        OneDlg.cbSC.Items.AddObject('', TObject(0));
        OneDlg.cbSC.ItemIndex := 0;
        while not D.Eof do
        begin
          OneDlg.cbSC.Items.AddObject(D.FieldByName('NAME').AsString, TObject(Pointer(D.FieldByName('EE_LOCALS_NBR').AsInteger)));
          if D.FieldByName('EE_LOCALS_NBR').AsInteger = FSWages.FieldByName('S_EE_LOCALS_NBR').AsInteger then
            OneDlg.cbSC.ItemIndex := OneDlg.cbSC.Items.Count - 1;
          D.Next;
        end;
        if OneDlg.ShowModal = mrOk then
        begin
          NName := FSWages.FieldByName('N_NAME').AsString;
          RName := FSWages.FieldByName('R_NAME').AsString;
          SName := FSWages.FieldByName('S_NAME').AsString;
          DNName := OneDlg.cbNR.Text;
          DRName := OneDlg.cbRE.Text;
          DSName := OneDlg.cbSC.Text;
          DNNbr := Integer(OneDlg.cbNR.Items.Objects[OneDlg.cbNR.ItemIndex]);
          DRNbr := Integer(OneDlg.cbRE.Items.Objects[OneDlg.cbRE.ItemIndex]);
          DSNbr := Integer(OneDlg.cbSC.Items.Objects[OneDlg.cbSC.ItemIndex]);
          Amount := RoundTwo(StrToFloat(OneDlg.AmountEdit.Text));
          Hours := RoundTwo(StrToFloatDef(OneDlg.HoursEdit.Text, 0));

          if FSWages.FieldByName('CO_DIVISION_NBR').IsNull then
            DBDT := DBDT + ' and CO_DIVISION_NBR is null '
          else
            DBDT := DBDT + ' and CO_DIVISION_NBR = ' + FSWages.FieldByName('CO_DIVISION_NBR').AsString;

          if FSWages.FieldByName('CO_BRANCH_NBR').IsNull then
            DBDT := DBDT + ' and CO_BRANCH_NBR is null '
          else
            DBDT := DBDT + ' and CO_BRANCH_NBR = ' + FSWages.FieldByName('CO_BRANCH_NBR').AsString;

          if FSWages.FieldByName('CO_DEPARTMENT_NBR').IsNull then
            DBDT := DBDT + ' and CO_DEPARTMENT_NBR is null '
          else
            DBDT := DBDT + ' and CO_DEPARTMENT_NBR = ' + FSWages.FieldByName('CO_DEPARTMENT_NBR').AsString;

          if FSWages.FieldByName('CO_TEAM_NBR').IsNull then
            DBDT := DBDT + ' and CO_TEAM_NBR is null '
          else
            DBDT := DBDT + ' and CO_TEAM_NBR = ' + FSWages.FieldByName('CO_TEAM_NBR').AsString;

          if FSWages.FieldByName('CO_JOBS_NBR').IsNull then
            DBDT := DBDT + ' and CO_JOBS_NBR is null '
          else
            DBDT := DBDT + ' and CO_JOBS_NBR = ' + FSWages.FieldByName('CO_JOBS_NBR').AsString;

          if Abs(OneDlg.MaxAmount) - Abs(Amount) < 0.005 then
          begin // move all
            FSQLite.StartTransaction;
            try
              try
                SQL := 'delete from SOURCE_WAGES where EE_NBR='+IntToStr(FEENbr)+
                ' and CHECK_DATE='+IntToStr(FSWages.FieldByName('CHECK_DATE').AsInteger)+
                ' and N_EE_LOCALS_NBR='+FSWages.FieldByName('N_EE_LOCALS_NBR').AsString+
                ' and R_EE_LOCALS_NBR='+FSWages.FieldByName('R_EE_LOCALS_NBR').AsString+
                ' and S_EE_LOCALS_NBR='+FSWages.FieldByName('S_EE_LOCALS_NBR').AsString+
                ' and CL_E_DS_NBR='+FSWages.FieldByName('CL_E_DS_NBR').AsString;


                FSQLite.Execute(SQL+DBDT);

                SQL := 'insert into DEST_WAGES select '+IntToStr(FEENbr)+
                ' , '+FSWages.FieldByName('CHECK_DATE').AsString+
                ' , '+FSWages.FieldByName('CL_E_DS_NBR').AsString+
                ' , '''+FSWages.FieldByName('E_D_CODE_TYPE').AsString+'''';

                if FSWages.FieldByName('CO_DIVISION_NBR').IsNull then
                  SQL := SQL + ' , null '
                else
                  SQL := SQL + ' , ' + FSWages.FieldByName('CO_DIVISION_NBR').AsString;

                if FSWages.FieldByName('CO_BRANCH_NBR').IsNull then
                  SQL := SQL + ' , null '
                else
                  SQL := SQL + ' , ' + FSWages.FieldByName('CO_BRANCH_NBR').AsString;

                if FSWages.FieldByName('CO_DEPARTMENT_NBR').IsNull then
                  SQL := SQL + ' , null '
                else
                  SQL := SQL + ' , ' + FSWages.FieldByName('CO_DEPARTMENT_NBR').AsString;

                if FSWages.FieldByName('CO_TEAM_NBR').IsNull then
                  SQL := SQL + ' , null '
                else
                  SQL := SQL + ' , ' + FSWages.FieldByName('CO_TEAM_NBR').AsString;

                if FSWages.FieldByName('CO_JOBS_NBR').IsNull then
                  SQL := SQL + ' , null '
                else
                  SQL := SQL + ' , ' + FSWages.FieldByName('CO_JOBS_NBR').AsString;

                SQL := SQL +

                ' , '''+FSWages.FieldByName('DIVISION').AsString+''''+
                ' , '''+FSWages.FieldByName('BRANCH').AsString+''''+
                ' , '''+FSWages.FieldByName('DEPARTMENT').AsString+''''+
                ' , '''+FSWages.FieldByName('TEAM').AsString+''''+
                ' , '''+FSWages.FieldByName('JOB').AsString+''''+                

                ' , '''+FSWages.FieldByName('N_NAME').AsString+''''+
                ' , '''+FSWages.FieldByName('R_NAME').AsString+''''+
                ' , '''+FSWages.FieldByName('S_NAME').AsString+''''+
                ' , '+FSWages.FieldByName('N_EE_LOCALS_NBR').AsString+
                ' , '+FSWages.FieldByName('R_EE_LOCALS_NBR').AsString+
                ' , '+FSWages.FieldByName('S_EE_LOCALS_NBR').AsString+
                ' , '+FSWages.FieldByName('TAXABLE_WAGE').AsString+
                ' , '+FloatToStr(StrToFloatDef(FSWages.FieldByName('HOURS').AsString,0))+
                ' , null '+
                ' , '+IntToStr(DNNbr)+
                ' , '+IntToStr(DRNbr)+
                ' , '+IntToStr(DSNbr)+
                ' , '''+DNName+''''+
                ' , '''+DRName+''''+
                ' , '''+DSName+'''';

                if VarIsNull(OneDlg.Division) then
                  SQL := SQL + ', null'
                else
                  SQL := SQL + ', '+ VarToStr(OneDlg.Division);

                if VarIsNull(OneDlg.Branch) then
                  SQL := SQL + ', null'
                else
                  SQL := SQL + ', '+ VarToStr(OneDlg.Branch);

                if VarIsNull(OneDlg.Department) then
                  SQL := SQL + ', null'
                else
                  SQL := SQL + ', '+ VarToStr(OneDlg.Department);

                if VarIsNull(OneDlg.Team) then
                  SQL := SQL + ', null'
                else
                  SQL := SQL + ', '+ VarToStr(OneDlg.Team);

                if OneDlg.cbJob.ItemIndex = 0 then
                  SQL := SQL + ', 0'
                else
                  SQL := SQL + ', '+ IntToStr(Integer(OneDlg.cbJob.Items.Objects[OneDlg.cbJob.ItemIndex]));

                FSQLite.Execute(SQL);

              except
                raise
              end;
            finally
              FSQLite.Commit;
            end;
          end
          else
          begin // move part
            if (OneDlg.MaxAmount < 0) and (Amount > 0) then
              Amount := -Amount;

            if (OneDlg.MaxHours < 0) and (Hours > 0) then
              Hours := -Hours;

            FSQLite.Execute('update SOURCE_WAGES set TAXABLE_WAGE=TAXABLE_WAGE-'+FloatToStr(Amount)+
            ', HOURS=HOURS-'+FloatToStr(Hours)+ ', GROSS_WAGE=null  '+
            ' where EE_NBR='+IntToStr(FEENbr)+
            ' and CHECK_DATE='+IntToStr(FSWages.FieldByName('CHECK_DATE').AsInteger)+
            ' and N_EE_LOCALS_NBR='+FSWages.FieldByName('N_EE_LOCALS_NBR').AsString+
            ' and R_EE_LOCALS_NBR='+FSWages.FieldByName('R_EE_LOCALS_NBR').AsString+
            ' and S_EE_LOCALS_NBR='+FSWages.FieldByName('S_EE_LOCALS_NBR').AsString+
            ' and E_D_CODE_TYPE='''+FSWages.FieldByName('E_D_CODE_TYPE').AsString+''''+DBDT);

            SQL := 'insert into DEST_WAGES select '+IntToStr(FEENbr)+
            ' , '+IntToStr(FSWages.FieldByName('CHECK_DATE').AsInteger)+
            ' , '+FSWages.FieldByName('CL_E_DS_NBR').AsString+
            ' , '''+FSWages.FieldByName('E_D_CODE_TYPE').AsString+'''';

            if FSWages.FieldByName('CO_DIVISION_NBR').IsNull then
              SQL := SQL + ' , null '
            else
              SQL := SQL + ' , ' + FSWages.FieldByName('CO_DIVISION_NBR').AsString;

            if FSWages.FieldByName('CO_BRANCH_NBR').IsNull then
              SQL := SQL + ' , null '
            else
              SQL := SQL + ' , ' + FSWages.FieldByName('CO_BRANCH_NBR').AsString;

            if FSWages.FieldByName('CO_DEPARTMENT_NBR').IsNull then
              SQL := SQL + ' , null '
            else
              SQL := SQL + ' , ' + FSWages.FieldByName('CO_DEPARTMENT_NBR').AsString;

            if FSWages.FieldByName('CO_TEAM_NBR').IsNull then
              SQL := SQL + ' , null '
            else
              SQL := SQL + ' , ' + FSWages.FieldByName('CO_TEAM_NBR').AsString;

            if FSWages.FieldByName('CO_JOBS_NBR').IsNull then
              SQL := SQL + ' , null '
            else
              SQL := SQL + ' , ' + FSWages.FieldByName('CO_JOBS_NBR').AsString;

            SQL := SQL +

            ' , '''+FSWages.FieldByName('DIVISION').AsString+''''+
            ' , '''+FSWages.FieldByName('BRANCH').AsString+''''+
            ' , '''+FSWages.FieldByName('DEPARTMENT').AsString+''''+
            ' , '''+FSWages.FieldByName('TEAM').AsString+''''+
            ' , '''+FSWages.FieldByName('JOB').AsString+''''+

            ' , '''+FSWages.FieldByName('N_NAME').AsString+''''+
            ' , '''+FSWages.FieldByName('R_NAME').AsString+''''+
            ' , '''+FSWages.FieldByName('S_NAME').AsString+''''+
            ' , '+FSWages.FieldByName('N_EE_LOCALS_NBR').AsString+
            ' , '+FSWages.FieldByName('R_EE_LOCALS_NBR').AsString+
            ' , '+FSWages.FieldByName('S_EE_LOCALS_NBR').AsString+
            ' , '+FloatToStr(Amount)+
            ' , '+FloatToStr(Hours)+
            ' , null '+
            ' , '+IntToStr(DNNbr)+
            ' , '+IntToStr(DRNbr)+
            ' , '+IntToStr(DSNbr)+
            ' , '''+DNName+''''+
            ' , '''+DRName+''''+
            ' , '''+DSName+'''';

            if VarIsNull(OneDlg.Division) then
              SQL := SQL + ', null'
            else
              SQL := SQL + ', '+ VarToStr(OneDlg.Division);

            if VarIsNull(OneDlg.Branch) then
              SQL := SQL + ', null'
            else
              SQL := SQL + ', '+ VarToStr(OneDlg.Branch);

            if VarIsNull(OneDlg.Department) then
              SQL := SQL + ', null'
            else
              SQL := SQL + ', '+ VarToStr(OneDlg.Department);

            if VarIsNull(OneDlg.Team) then
              SQL := SQL + ', null'
            else
              SQL := SQL + ', '+ VarToStr(OneDlg.Team);

            if OneDlg.cbJob.ItemIndex = 0 then
              SQL := SQL + ', null'
            else
              SQL := SQL + ', '+ IntToStr(Integer(OneDlg.cbJob.Items.Objects[OneDlg.cbJob.ItemIndex]));

            FSQLite.Execute(SQL);

          end;
        end;
      finally
        OneDlg.Free;
        LoadEe;
      end;
    end;
  end;
end;

procedure TPAMovingWagesParams.BackBtnClick(Sender: TObject);
var
  D: IevDataset;
  SQL, DBDT, DDBDT: String;
begin
  if not FSWages.vclDataset.IsEmpty then
  begin
    if DestGrid.SelectedList.Count < 2 then
    begin
      FSQLite.StartTransaction;
      try
        SQL := 'delete from DEST_WAGES where EE_NBR='+IntToStr(FEENbr)+
        ' and CHECK_DATE='+IntToStr(FDWages.FieldByName('CHECK_DATE').AsInteger)+
        ' and N_EE_LOCALS_NBR='+FDWages.FieldByName('N_EE_LOCALS_NBR').AsString+
        ' and R_EE_LOCALS_NBR='+FDWages.FieldByName('R_EE_LOCALS_NBR').AsString+
        ' and S_EE_LOCALS_NBR='+FDWages.FieldByName('S_EE_LOCALS_NBR').AsString+
        ' and DN_EE_LOCALS_NBR='+FDWages.FieldByName('DN_EE_LOCALS_NBR').AsString+
        ' and DR_EE_LOCALS_NBR='+FDWages.FieldByName('DR_EE_LOCALS_NBR').AsString+
        ' and DS_EE_LOCALS_NBR='+FDWages.FieldByName('DS_EE_LOCALS_NBR').AsString+
        ' and E_D_CODE_TYPE='''+FDWages.FieldByName('E_D_CODE_TYPE').AsString+'''';

        DDBDT := '';

        if FDWages.FieldByName('DCO_DIVISION_NBR').IsNull then
          DDBDT := DDBDT + ' and DCO_DIVISION_NBR is null '
        else
          DDBDT := DDBDT + ' and DCO_DIVISION_NBR = ' + FDWages.FieldByName('DCO_DIVISION_NBR').AsString;

        if FDWages.FieldByName('DCO_BRANCH_NBR').IsNull then
          DDBDT := DDBDT + ' and DCO_BRANCH_NBR is null '
        else
          DDBDT := DDBDT + ' and DCO_BRANCH_NBR = ' + FDWages.FieldByName('DCO_BRANCH_NBR').AsString;

        if FDWages.FieldByName('DCO_DEPARTMENT_NBR').IsNull then
          DDBDT := DDBDT + ' and DCO_DEPARTMENT_NBR is null '
        else
          DDBDT := DDBDT + ' and DCO_DEPARTMENT_NBR = ' + FDWages.FieldByName('DCO_DEPARTMENT_NBR').AsString;

        if FDWages.FieldByName('DCO_TEAM_NBR').IsNull then
          DDBDT := DDBDT + ' and DCO_TEAM_NBR is null '
        else
          DDBDT := DDBDT + ' and DCO_TEAM_NBR = ' + FDWages.FieldByName('DCO_TEAM_NBR').AsString;

        if FDWages.FieldByName('DCO_JOBS_NBR').IsNull then
          DDBDT := DDBDT + ' and DCO_JOBS_NBR is null '
        else
          DDBDT := DDBDT + ' and DCO_JOBS_NBR = ' + FDWages.FieldByName('DCO_JOBS_NBR').AsString;


        SQL := SQL +DDBDT;

        DBDT := '';

        if FDWages.FieldByName('CO_DIVISION_NBR').IsNull then
          DBDT := DBDT + ' and CO_DIVISION_NBR is null '
        else
          DBDT := DBDT + ' and CO_DIVISION_NBR = ' + FDWages.FieldByName('CO_DIVISION_NBR').AsString;

        if FDWages.FieldByName('CO_BRANCH_NBR').IsNull then
          DBDT := DBDT + ' and CO_BRANCH_NBR is null '
        else
          DBDT := DBDT + ' and CO_BRANCH_NBR = ' + FDWages.FieldByName('CO_BRANCH_NBR').AsString;

        if FDWages.FieldByName('CO_DEPARTMENT_NBR').IsNull then
          DBDT := DBDT + ' and CO_DEPARTMENT_NBR is null '
        else
          DBDT := DBDT + ' and CO_DEPARTMENT_NBR = ' + FDWages.FieldByName('CO_DEPARTMENT_NBR').AsString;

        if FDWages.FieldByName('CO_TEAM_NBR').IsNull then
          DBDT := DBDT + ' and CO_TEAM_NBR is null '
        else
          DBDT := DBDT + ' and CO_TEAM_NBR = ' + FDWages.FieldByName('CO_TEAM_NBR').AsString;

        if FDWages.FieldByName('CO_JOBS_NBR').IsNull then
          DBDT := DBDT + ' and CO_JOBS_NBR is null '
        else
          DBDT := DBDT + ' and CO_JOBS_NBR = ' + FDWages.FieldByName('CO_JOBS_NBR').AsString;

        FSQLite.Execute(SQL+DBDT);

        D := FSQLite.Select('select 1 from SOURCE_WAGES where EE_NBR='+IntToStr(FEENbr)+
        ' and CHECK_DATE='+IntToStr(FDWages.FieldByName('CHECK_DATE').AsInteger)+
        ' and N_EE_LOCALS_NBR='+FDWages.FieldByName('N_EE_LOCALS_NBR').AsString+
        ' and R_EE_LOCALS_NBR='+FDWages.FieldByName('R_EE_LOCALS_NBR').AsString+
        ' and S_EE_LOCALS_NBR='+FDWages.FieldByName('S_EE_LOCALS_NBR').AsString+
        ' and CL_E_DS_NBR='+FDWages.FieldByName('CL_E_DS_NBR').AsString+
        DBDT, []);

        if D.vclDataset.IsEmpty then
        begin

          SQL := 'insert into SOURCE_WAGES select '+IntToStr(FEENbr)+
          ' , '+IntToStr(FDWages.FieldByName('CHECK_DATE').AsInteger)+
          ' , '+FDWages.FieldByName('CL_E_DS_NBR').AsString+
          ' , '''+FDWages.FieldByName('E_D_CODE_TYPE').AsString+'''';

          if FSWages.FieldByName('CO_DIVISION_NBR').IsNull then
            SQL := SQL + ' , null '
          else
            SQL := SQL + ' , ' + FSWages.FieldByName('CO_DIVISION_NBR').AsString;

          if FSWages.FieldByName('CO_BRANCH_NBR').IsNull then
            SQL := SQL + ' , null '
          else
            SQL := SQL + ' , ' + FSWages.FieldByName('CO_BRANCH_NBR').AsString;

          if FSWages.FieldByName('CO_DEPARTMENT_NBR').IsNull then
            SQL := SQL + ' , null '
          else
            SQL := SQL + ' , ' + FSWages.FieldByName('CO_DEPARTMENT_NBR').AsString;

          if FSWages.FieldByName('CO_TEAM_NBR').IsNull then
            SQL := SQL + ' , null '
          else
            SQL := SQL + ' , ' + FSWages.FieldByName('CO_TEAM_NBR').AsString;

          if FSWages.FieldByName('CO_JOBS_NBR').IsNull then
            SQL := SQL + ' , null '
          else
            SQL := SQL + ' , ' + FSWages.FieldByName('CO_JOBS_NBR').AsString;

          SQL := SQL +

          ' , '''+FDWages.FieldByName('DIVISION').AsString+''''+
          ' , '''+FDWages.FieldByName('BRANCH').AsString+''''+
          ' , '''+FDWages.FieldByName('DEPARTMENT').AsString+''''+
          ' , '''+FDWages.FieldByName('TEAM').AsString+''''+
          ' , '''+FDWages.FieldByName('JOB').AsString+''''+

          ' , '''+FDWages.FieldByName('N_NAME').AsString+''''+
          ' , '''+FDWages.FieldByName('R_NAME').AsString+''''+
          ' , '''+FDWages.FieldByName('S_NAME').AsString+''''+
          ' , '+FDWages.FieldByName('N_EE_LOCALS_NBR').AsString+
          ' , '+FDWages.FieldByName('R_EE_LOCALS_NBR').AsString+
          ' , '+FDWages.FieldByName('S_EE_LOCALS_NBR').AsString+
          ' , '+FDWages.FieldByName('TAXABLE_WAGE').AsString+
          ' , '+FloatToStr(StrToFloatDef(FSWages.FieldByName('HOURS').AsString,0))+
          ' , null';

          FSQLite.Execute(SQL);

        end
        else
          FSQLite.Execute('update SOURCE_WAGES set '+
          'TAXABLE_WAGE=TAXABLE_WAGE+'+FDWages.FieldByName('TAXABLE_WAGE').AsString+
          ', HOURS=HOURS+' + FloatToStr(StrToFloatDef(FSWages.FieldByName('HOURS').AsString,0))+
          ' where EE_NBR='+IntToStr(FEENbr)+
          ' and CHECK_DATE='+IntToStr(FDWages.FieldByName('CHECK_DATE').AsInteger)+
          ' and CL_E_DS_NBR='+FDWages.FieldByName('CL_E_DS_NBR').AsString+
          ' and N_EE_LOCALS_NBR='+FDWages.FieldByName('N_EE_LOCALS_NBR').AsString+
          ' and R_EE_LOCALS_NBR='+FDWages.FieldByName('R_EE_LOCALS_NBR').AsString+
          ' and S_EE_LOCALS_NBR='+FDWages.FieldByName('S_EE_LOCALS_NBR').AsString+
          DBDT);
      finally
        FSQLite.Commit;
        LoadEe;
      end;
    end;  
  end;
end;

procedure TPAMovingWagesParams.ApplyBtnClick(Sender: TObject);
var
  D, L, X: IevDataset;
begin
  D := FSQLite.Select('select distinct EE_NBR from DEST_WAGES', []);
  D.First;
  while not D.Eof do
  begin

    DM_PAYROLL.PR_CHECK.Append;

    with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
    begin
      SetMacro('Columns', 'max(PAYMENT_SERIAL_NUMBER)');
      SetMacro('TableName', 'PR_CHECK');
      SetMacro('NbrField', 'PR');
      SetMacro('Condition', 'PAYMENT_SERIAL_NUMBER<100000001');
      SetParam('RecordNbr', DM_PAYROLL.PR['PR_NBR']);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;

    DM_PAYROLL.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger + 1;
    ctx_DataAccess.CUSTOM_VIEW.Close;

    DM_PAYROLL.PR_CHECK.FieldByName('PR_NBR').Value := DM_PAYROLL.PR['PR_NBR'];
    DM_PAYROLL.PR_CHECK.FieldByName('PR_BATCH_NBR').Value := DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').Value;
    DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value := D.FieldByName('EE_NBR').Value;
    DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_MANUAL;
    DM_PAYROLL.PR_CHECK.FieldByName('UPDATE_BALANCE').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_FEDERAL').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').Value := EXCLUDE_TAXES_BOTH;;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_OASDI').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_MEDICARE').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_EIC').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString := ctx_PayrollCalculation.GetCustomBankAccountNumber(DM_PAYROLL.PR_CHECK);
    DM_PAYROLL.PR_CHECK.FieldByName('ABA_NUMBER').AsString := ctx_PayrollCalculation.GetABANumber;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_DD').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_DD_EXCEPT_NET').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_TIME_OFF_ACCURAL').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_AUTO_DISTRIBUTION').AsString := GROUP_BOX_YES;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_SCH_E_D_FROM_AGCY_CHK').AsString := GROUP_BOX_YES;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_SCH_E_D_EXCEPT_PENSION').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('PRORATE_SCHEDULED_E_DS').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_OUTSTANDING;
    DM_PAYROLL.PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime := Now;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_FROM_AGENCY').AsString := GROUP_BOX_YES;
    DM_PAYROLL.PR_CHECK.FieldByName('ER_OASDI_TAXABLE_TIPS').Value := 0.0;
    DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_SHORTFALL').Value := 0.0;
    DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE_945').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('TAX_FREQUENCY').Value := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').Value;

    DM_PAYROLL.PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').AsString := 'N';

    DM_PAYROLL.PR_CHECK.Post;

    L := FSQLite.Select('select * from DEST_WAGES where EE_NBR=:EE_NBR', [D.FieldByName('EE_NBR').Value]);
    L.First;
    while not L.Eof do
    begin
      DM_PAYROLL.PR_CHECK_LINES.Append;
      DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := -L.FieldByName('TAXABLE_WAGE').AsFloat;
      DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'] := L.FieldByName('CL_E_DS_NBR').AsInteger;
      DM_PAYROLL.PR_CHECK_LINES['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;

      if L.FieldByName('CO_DIVISION_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_DIVISION_NBR'] := L.FieldByName('CO_DIVISION_NBR').AsInteger;
      if L.FieldByName('CO_BRANCH_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_BRANCH_NBR'] := L.FieldByName('CO_BRANCH_NBR').AsInteger;
      if L.FieldByName('CO_DEPARTMENT_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_DEPARTMENT_NBR'] := L.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
      if L.FieldByName('CO_TEAM_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_TEAM_NBR'] := L.FieldByName('CO_TEAM_NBR').AsInteger;
      if L.FieldByName('CO_JOBS_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_JOBS_NBR'] := L.FieldByName('CO_JOBS_NBR').AsInteger;

      DM_PAYROLL.PR_CHECK_LINES.Post;

      X := FSQLite.Select(
      '  select x.EE_LOCALS_NBR'+
      '  from EE_LOCALS x'+
      '  join CO_LOCAL_TAX c on c.CO_LOCAL_TAX_NBR=x.CO_LOCAL_TAX_NBR'+
      '  join SY_LOCALS s on s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
      '  where x.CHECK_DATE=(select MAX(CHECK_DATE) from EE_LOCALS)'+
      '  and c.CHECK_DATE=(select MAX(CHECK_DATE) from CO_LOCAL_TAX)'+
      '  and s.CHECK_DATE=(select MAX(CHECK_DATE) from SY_LOCALS)'+
      '  and x.EE_NBR='+D.FieldByName('EE_NBR').AsString, []);

      X.First;
      while not X.Eof do
      begin

        DM_PAYROLL.PR_CHECK_LINE_LOCALS.Append;
        DM_PAYROLL.PR_CHECK_LINE_LOCALS['PR_CHECK_LINES_NBR'] := DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
        DM_PAYROLL.PR_CHECK_LINE_LOCALS['EE_LOCALS_NBR'] := X.FieldByName('EE_LOCALS_NBR').AsInteger;
        if (X.FieldByName('EE_LOCALS_NBR').AsInteger = L.FieldByName('N_EE_LOCALS_NBR').AsInteger)
        or (X.FieldByName('EE_LOCALS_NBR').AsInteger = L.FieldByName('R_EE_LOCALS_NBR').AsInteger)
        or (X.FieldByName('EE_LOCALS_NBR').AsInteger = L.FieldByName('S_EE_LOCALS_NBR').AsInteger) then
           DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_INCLUDE
        else
           DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_EXEMPT;
        DM_PAYROLL.PR_CHECK_LINE_LOCALS.Post;

        X.Next;
      end;

      DM_PAYROLL.PR_CHECK_LINES.Append;
      DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := L.FieldByName('TAXABLE_WAGE').AsFloat;
      DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'] := L.FieldByName('CL_E_DS_NBR').AsInteger;
      DM_PAYROLL.PR_CHECK_LINES['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
      DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;

      if L.FieldByName('DCO_DIVISION_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_DIVISION_NBR'] := L.FieldByName('DCO_DIVISION_NBR').AsInteger;
      if L.FieldByName('DCO_BRANCH_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_BRANCH_NBR'] := L.FieldByName('DCO_BRANCH_NBR').AsInteger;
      if L.FieldByName('DCO_DEPARTMENT_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_DEPARTMENT_NBR'] := L.FieldByName('DCO_DEPARTMENT_NBR').AsInteger;
      if L.FieldByName('DCO_TEAM_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_TEAM_NBR'] := L.FieldByName('DCO_TEAM_NBR').AsInteger;
      if L.FieldByName('DCO_JOBS_NBR').AsInteger <> 0 then
        DM_PAYROLL.PR_CHECK_LINES['CO_JOBS_NBR'] := L.FieldByName('DCO_JOBS_NBR').AsInteger;

      DM_PAYROLL.PR_CHECK_LINES.Post;

      X := FSQLite.Select(
      '  select x.EE_LOCALS_NBR'+
      '  from EE_LOCALS x'+
      '  join CO_LOCAL_TAX c on c.CO_LOCAL_TAX_NBR=x.CO_LOCAL_TAX_NBR'+
      '  join SY_LOCALS s on s.SY_LOCALS_NBR=c.SY_LOCALS_NBR'+
      '  where x.CHECK_DATE=(select MAX(CHECK_DATE) from EE_LOCALS)'+
      '  and c.CHECK_DATE=(select MAX(CHECK_DATE) from CO_LOCAL_TAX)'+
      '  and s.CHECK_DATE=(select MAX(CHECK_DATE) from SY_LOCALS)'+
      '  and x.EE_NBR='+D.FieldByName('EE_NBR').AsString, []);

      X.First;
      while not X.Eof do
      begin

        DM_PAYROLL.PR_CHECK_LINE_LOCALS.Append;
        DM_PAYROLL.PR_CHECK_LINE_LOCALS['PR_CHECK_LINES_NBR'] := DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
        DM_PAYROLL.PR_CHECK_LINE_LOCALS['EE_LOCALS_NBR'] := X.FieldByName('EE_LOCALS_NBR').AsInteger;
        if (X.FieldByName('EE_LOCALS_NBR').AsInteger = L.FieldByName('DN_EE_LOCALS_NBR').AsInteger)
        or (X.FieldByName('EE_LOCALS_NBR').AsInteger = L.FieldByName('DR_EE_LOCALS_NBR').AsInteger)
        or (X.FieldByName('EE_LOCALS_NBR').AsInteger = L.FieldByName('DS_EE_LOCALS_NBR').AsInteger) then
           DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_INCLUDE
        else
           DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_EXEMPT;
        DM_PAYROLL.PR_CHECK_LINE_LOCALS.Post;

        X.Next;
      end;

      L.Next;
    end;

    ctx_DataAccess.PostDatasets([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_LINE_LOCALS]);

    D.Next;
  end;
  Close;
end;

procedure TPAMovingWagesParams.cbEmployeeChange(Sender: TObject);
begin
  LoadEe;
end;

end.
