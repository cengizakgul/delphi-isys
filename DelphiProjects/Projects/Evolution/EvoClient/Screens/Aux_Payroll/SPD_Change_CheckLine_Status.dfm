object Change_CheckLine_Status: TChange_CheckLine_Status
  Left = 240
  Top = 166
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Change Check Line Status'
  ClientHeight = 453
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TevPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 65
    Align = alTop
    TabOrder = 0
    object Label1: TevLabel
      Left = 184
      Top = 8
      Width = 90
      Height = 13
      Caption = 'Earning/Deduction'
    end
    object lablTotal: TevLabel
      Left = 488
      Top = 7
      Width = 128
      Height = 13
      Caption = 'Total Check Lines Amount:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object bbtnOK: TevBitBtn
      Left = 488
      Top = 32
      Width = 89
      Height = 25
      Caption = 'OK'
      Enabled = False
      ModalResult = 1
      TabOrder = 0
      OnClick = bbtnOKClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
        DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
        DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
        DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
        DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
        DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
        2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
        A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
        AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
      NumGlyphs = 2
    end
    object bbtnCancel: TevBitBtn
      Left = 584
      Top = 32
      Width = 91
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
      OnClick = bbtnCancelClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
        DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
        9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
        DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
        DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
        DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
        91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
        999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
      NumGlyphs = 2
    end
    object RadioGroup1: TevRadioGroup
      Left = 8
      Top = 8
      Width = 169
      Height = 41
      Caption = 'Line Status'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Ignore'
        'Pend.'
        'Sent')
      TabOrder = 2
      OnClick = RadioGroup1Click
    end
    object wwDBLookupCombo1: TevDBLookupCombo
      Left = 184
      Top = 24
      Width = 113
      Height = 21
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'CUSTOM_E_D_CODE_NUMBER'#9'20'#9'CUSTOM_E_D_CODE_NUMBER'
        'DESCRIPTION'#9'40'#9'DESCRIPTION')
      LookupTable = DM_CL_E_DS.CL_E_DS
      LookupField = 'CL_E_DS_NBR'
      Style = csDropDownList
      TabOrder = 3
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnChange = wwDBLookupCombo1Change
    end
    object GroupBox1: TGroupBox
      Left = 304
      Top = 8
      Width = 177
      Height = 49
      Caption = 'Change To'
      TabOrder = 4
      object btnIgnore: TButton
        Left = 8
        Top = 16
        Width = 49
        Height = 25
        Caption = 'Ignore'
        Enabled = False
        TabOrder = 0
        OnClick = bbtnChangeClick
      end
      object btnPending: TButton
        Left = 64
        Top = 16
        Width = 49
        Height = 25
        Caption = 'Pending'
        Enabled = False
        TabOrder = 1
        OnClick = bbtnChangeClick
      end
      object btnSent: TButton
        Left = 120
        Top = 16
        Width = 49
        Height = 25
        Caption = 'Sent'
        Enabled = False
        TabOrder = 2
        OnClick = bbtnChangeClick
      end
    end
  end
  object wwDBGrid1: TevDBGrid
    Left = 0
    Top = 65
    Width = 688
    Height = 388
    DisableThemesInTitle = False
    Selected.Strings = (
      'CUSTOM_EMPLOYEE_NUMBER'#9'10'#9'EE Code'
      'PAYMENT_SERIAL_NUMBER'#9'10'#9'Check #'
      'HOURS_OR_PIECES'#9'15'#9'Hours'
      'RATE_OF_PAY'#9'15'#9'Rate of Pay'
      'AMOUNT'#9'15'#9'Amount'
      'AGENCY_NAME'#9'30'#9'Agency Name')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TChange_CheckLine_Status\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = wwdsCheckLines
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
  end
  object wwcsCheckLines: TevClientDataSet
    Left = 96
    Top = 88
  end
  object wwdsCheckLines: TevDataSource
    DataSet = wwcsCheckLines
    OnDataChange = wwdsCheckLinesDataChange
    Left = 144
    Top = 88
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 429
    Top = 54
  end
end
