// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_PR_CHECK_SUI;

interface

uses
   SDataStructure, SPD_EDIT_PR_BASE, wwdblook, StdCtrls, Mask,
  wwdbedit, DBActns, Classes, ActnList, Db, Wwdatsrc, DBCtrls, Buttons,
  Grids, Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, ISBasicClasses,
  SDDClasses, SDataDictclient, SDataDicttemp, EvUIComponents, EvUtils, EvClientDataSet,
  isUIwwDBLookupCombo, isUIwwDBEdit, ImgList, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, LMDCustomButton, LMDButton,
  isUILMDButton, isUIFashionPanel, Forms;

type
  TEDIT_PR_CHECK_SUI = class(TEDIT_PR_BASE)
    wwDBGrid2: TevDBGrid;
    wwDBGrid1: TevDBGrid;
    Label2: TevLabel;
    Label3: TevLabel;
    DBEdit2: TevDBEdit;
    wwlcCO_SUI: TevDBLookupCombo;
    evPanel1: TevPanel;
    Label1: TevLabel;
    DBText1: TevDBText;
    Label4: TevLabel;
    DBText4: TevDBText;
    Label5: TevLabel;
    DBText5: TevDBText;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    evSplitter1: TevSplitter;
    evDBGrid1: TevDBGrid;
    wwdsSubMaster1: TevDataSource;
    wwdsSubMaster2: TevDataSource;
    evDBEdit1: TevDBEdit;
    evLabel2: TevLabel;
    pnlLocation: TevPanel;
    evLabel3: TevLabel;
    evDBText1: TevDBText;
    evDBText2: TevDBText;
    evPanel3: TevPanel;
    evDBText3: TevDBText;
    evLabel4: TevLabel;
    evDBText4: TevDBText;
    butnPrior: TevBitBtn;
    butnNext: TevBitBtn;
    procedure butnPriorClick(Sender: TObject);
    procedure butnNextClick(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure PRNextExecute(Sender: TObject);
    procedure PRPriorExecute(Sender: TObject);
    procedure PRPageControlChange(Sender: TObject);
    procedure wwdsSubMasterDataChange(Sender: TObject; Field: TField);
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    procedure ReopenPRCheckWithLines; override;
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

uses
  EvConsts;

{$R *.DFM}

procedure TEDIT_PR_CHECK_SUI.butnPriorClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Prior;
end;

procedure TEDIT_PR_CHECK_SUI.butnNextClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster2.DataSet.Next;
end;

procedure TEDIT_PR_CHECK_SUI.wwdsDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  butnPrior.Enabled := wwdsSubMaster2.DataSet.RecNo > 1;
  butnNext.Enabled := wwdsSubMaster2.DataSet.RecNo < wwdsSubMaster2.DataSet.RecordCount;
end;

procedure TEDIT_PR_CHECK_SUI.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_COMPANY.CO_SUI, aDS);
  AddDS(DM_PAYROLL.PR_BATCH, aDS);
  inherited;
end;

function TEDIT_PR_CHECK_SUI.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR_CHECK_SUI;
end;

function TEDIT_PR_CHECK_SUI.GetInsertControl: TWinControl;
begin
  Result := wwlcCO_SUI;
end;

procedure TEDIT_PR_CHECK_SUI.ReopenPRCheckWithLines;
begin
  inherited;
  wwdsSubMaster1.DataSet := wwdsSubMaster1.DataSet; 
  wwdsSubMaster2.DataSet := wwdsSubMaster2.DataSet; 
end;

procedure TEDIT_PR_CHECK_SUI.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_PAYROLL.PR_BATCH, SupportDataSets);
end;

procedure TEDIT_PR_CHECK_SUI.PRNextExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Next;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_SUI.PRPriorExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Prior;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_SUI.PRPageControlChange(Sender: TObject);
begin
  inherited;
  Panel1.Visible := PageControl1.ActivePage = TabSheet1;
  evPanel1.Visible := not Panel1.Visible;
  ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_SUI.wwdsSubMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if DM_PAYROLL.PR.Active then
    wwDBGrid1.ReadOnly := (DM_PAYROLL.PR.FieldByName('PAYROLL_TYPE').AsString <> PAYROLL_TYPE_QTR_END_TAX_ADJUSTMENT);
end;

initialization
  RegisterClass(TEDIT_PR_CHECK_SUI);

end.
