// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_AdjustTaxes;

interface

uses
   Db, Wwdatsrc, Grids, Wwdbigrd, Wwdbgrid, StdCtrls, Mask,
  wwdbedit, Controls, ComCtrls, Buttons, Classes, ExtCtrls, SysUtils, Forms,
  EvUtils, SDataStructure, SSecurityInterface, EvTypes, SDDClasses, EvContext,
  SDataDictclient, ISBasicClasses, EvUIComponents, EvConsts, EvExceptions, isUIwwDBEdit,
  LMDCustomButton, LMDButton, isUILMDButton, DBCtrls, wwdblook,
  isUIwwDBLookupCombo, EvClientDataset;

type
  TAdjustTaxes = class(TForm)
    Panel1: TevPanel;
    Panel2: TevPanel;
    bbtnAdjust: TevBitBtn;
    bbtnCancel: TevBitBtn;
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    dedtFederalTW: TevDBEdit;
    dedtFederalT: TevDBEdit;
    dedtEE_oasdiTW: TevDBEdit;
    dedtEE_oasdiT: TevDBEdit;
    dedtEE_MedicareTW: TevDBEdit;
    dedtEE_MedicareT: TevDBEdit;
    dedtEICT: TevDBEdit;
    dedtBackupWithT: TevDBEdit;
    dedtER_oasdiTW: TevDBEdit;
    dedtER_oasdiT: TevDBEdit;
    dedtER_MedicareTW: TevDBEdit;
    dedtER_MedicareT: TevDBEdit;
    dedtFUITW: TevDBEdit;
    dedtFUIT: TevDBEdit;
    lablTaxableWages: TevLabel;
    lablTax: TevLabel;
    lablFederal: TevLabel;
    lablEE_oasdi: TevLabel;
    lablER_oasdi: TevLabel;
    lablEE_Medicare: TevLabel;
    lablER_MedicareTW: TevLabel;
    lablEIC: TevLabel;
    lablBackupWith: TevLabel;
    lablFUI: TevLabel;
    lablTaxableTips: TevLabel;
    dedtEE_oasdiTT: TevDBEdit;
    dedtER_oasdiTT: TevDBEdit;
    wwgrState: TevDBGrid;
    lablStateTax: TevLabel;
    lablStateTaxableWages: TevLabel;
    lablState: TevLabel;
    lablEE_sdi: TevLabel;
    lablER_sdi: TevLabel;
    dedtStateTW: TevDBEdit;
    dedtStateT: TevDBEdit;
    dedtEE_sdiTW: TevDBEdit;
    dedtEE_sdiT: TevDBEdit;
    dedtER_sdiTW: TevDBEdit;
    dedtER_sdiT: TevDBEdit;
    dedtSUITW: TevDBEdit;
    dedtSUIT: TevDBEdit;
    dedtLocalTW: TevDBEdit;
    dedtLocalT: TevDBEdit;
    wwgrLocal: TevDBGrid;
    wwgrSUI: TevDBGrid;
    lablSUITaxableWages: TevLabel;
    lablSUITax: TevLabel;
    lablLocalTaxableWages: TevLabel;
    lablLocalTax: TevLabel;
    wwdsFederal: TevDataSource;
    wwdsState: TevDataSource;
    wwdsSUI: TevDataSource;
    wwdsLocal: TevDataSource;
    Panel3: TevPanel;
    lablInfo: TevLabel;
    DM_PAYROLL: TDM_PAYROLL;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evDBEdit1: TevDBEdit;
    evLabel4: TevLabel;
    evDBEdit2: TevDBEdit;
    btnAdjustNextEE: TevBitBtn;
    evLabel3: TevLabel;
    NONRES_LOCAL_Combo: TevDBLookupCombo;
    evLabel5: TevLabel;
    LOCATION_Combo: TevDBLookupCombo;
    evDBRadioGroup1: TevDBRadioGroup;
    procedure bbtnCancelClick(Sender: TObject);
    procedure bbtnAdjustClick(Sender: TObject);
    procedure btnAdjustNextEEClick(Sender: TObject);
  private
    { Private declarations }
    procedure PostAll;
  public
    { Public declarations }
    procedure DoTaxAdjustments;
  end;

var
  AdjustTaxes: TAdjustTaxes;

implementation

uses SPD_CheckFinder;

{$R *.DFM}

procedure TAdjustTaxes.bbtnCancelClick(Sender: TObject);
begin
  DM_PAYROLL.PR_CHECK.Cancel;
  DM_PAYROLL.PR_CHECK_STATES.Cancel;
  DM_PAYROLL.PR_CHECK_SUI.Cancel;
  DM_PAYROLL.PR_CHECK_LOCALS.Cancel;
  ctx_DataAccess.CancelDataSets([DM_PAYROLL.PR_CHECK,DM_PAYROLL.PR_CHECK_STATES,DM_PAYROLL.PR_CHECK_SUI,DM_PAYROLL.PR_CHECK_LOCALS]);
end;

procedure TAdjustTaxes.bbtnAdjustClick(Sender: TObject);
var
  AllOK: Boolean;
begin
  ctx_StartWait;
  PostAll;
  ctx_DataAccess.UnlockPR;
  AllOK := False;
  try
    ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK,DM_PAYROLL.PR_CHECK_STATES,DM_PAYROLL.PR_CHECK_SUI,DM_PAYROLL.PR_CHECK_LOCALS]);
    AllOK := True;
  finally
    ctx_DataAccess.LockPR(AllOK);
    ctx_EndWait;
  end;
end;

procedure TAdjustTaxes.DoTaxAdjustments;
begin
  if ctx_AccountRights.Functions.GetState('USER_CAN_ADJUST_TAXES') <> stEnabled then
    raise ENoRights.CreateHelp('You have no rights to perform this operation.', IDH_SecurityViolation);
  with DM_PAYROLL do
  begin
    CheckFinder := TCheckFinder.Create(Application);
    ctx_DataAccess.UnlockPRSimple;
    try
      if CheckFinder.ShowModal = mrOK then
      begin
        ctx_StartWait;   
        try
          DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
          DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);
          DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
          DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);

          DM_COMPANY.PR_CHECK.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckFinder.CheckNumber));
          DM_COMPANY.PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckFinder.CheckNumber));
          DM_COMPANY.PR_CHECK_SUI.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckFinder.CheckNumber));
          DM_COMPANY.EE.Locate('EE_NBR',PR_CHECK.FieldByName('EE_NBR').Value,[]);
          DM_COMPANY.CL_PERSON.Locate('CL_PERSON_NBR',DM_COMPANY.EE.FieldByName('CL_PERSON_NBR').Value,[]);
          DM_COMPANY.EE_LOCALS.DataRequired('EE_NBR='+PR_CHECK.FieldByName('EE_NBR').AsString);
          DM_COMPANY.PR_CHECK_LOCALS.Close;
          DM_COMPANY.PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + IntToStr(CheckFinder.CheckNumber));          
          DM_COMPANY.CO_LOCATIONS.DataRequired('CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString);

          PR.Locate('PR_NBR', PR_CHECK.FieldByName('PR_NBR').Value, []);

          lablInfo.Caption := 'Check Date: ' + PR.FieldByName('CHECK_DATE').AsString
            + '  Run #' + PR.FieldByName('RUN_NUMBER').AsString
            + '  Check #' + PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').AsString
            + '  EE#' + DM_COMPANY.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString
            + '  - ' + DM_COMPANY.CL_PERSON.FieldByName('FIRST_NAME').AsString
            + ' ' + DM_COMPANY.CL_PERSON.FieldByName('LAST_NAME').AsString;
          btnAdjustNextEE.Enabled := (CheckFinder.ChecksList.Count > 1);
        finally
          ctx_EndWait;
        end;
        ShowModal;
      end;
    finally
      ctx_DataAccess.LockPRSimple;
      CheckFinder.Free;
    end;
  end;
end;

procedure TAdjustTaxes.PostAll;
begin
  DM_PAYROLL.PR_CHECK.CheckBrowseMode;
  DM_PAYROLL.PR_CHECK_STATES.CheckBrowseMode;
  DM_PAYROLL.PR_CHECK_SUI.CheckBrowseMode;
  DM_PAYROLL.PR_CHECK_LOCALS.CheckBrowseMode;
end;

procedure TAdjustTaxes.btnAdjustNextEEClick(Sender: TObject);
var
  CheckNbr: String;
  counter : integer;
begin
  bbtnAdjustClick(nil);

  with DM_PAYROLL, CheckFinder.ChecksList do
  begin
    if IndexOf(PR_CHECK.FieldByName('PR_CHECK_NBR').AsString) = (Count - 1) then
      CheckNbr := Strings[0]
    else
      CheckNbr := Strings[IndexOf(PR_CHECK.FieldByName('PR_CHECK_NBR').AsString) + 1];

    counter := IndexOf(CheckNbr);
    if counter = Count - 1 then
           counter := 0
      else counter := counter + 1;
    if StrToInt(Strings[counter]) = CheckFinder.CheckNumber then // We reached last check in the list, so not need to go to the second circle.
      btnAdjustNextEE.Enabled := False;

    ctx_DataAccess.UnlockPRSimple;
    PR_CHECK.DataRequired('PR_CHECK_NBR=' + CheckNbr);
    PR_CHECK_STATES.DataRequired('PR_CHECK_NBR=' + CheckNbr);
    PR_CHECK_SUI.DataRequired('PR_CHECK_NBR=' + CheckNbr);
    EE_LOCALS.DataRequired('EE_NBR=' + PR_CHECK.FieldByName('EE_NBR').AsString);

    PR_CHECK_LOCALS.DataRequired('PR_CHECK_NBR=' + CheckNbr);

    DM_COMPANY.EE.Locate('EE_NBR',PR_CHECK.FieldByName('EE_NBR').Value,[]);
    DM_COMPANY.CL_PERSON.Locate('CL_PERSON_NBR',DM_COMPANY.EE.FieldByName('CL_PERSON_NBR').Value,[]);

    PR.Locate('PR_NBR', PR_CHECK.FieldByName('PR_NBR').Value, []);

    lablInfo.Caption := 'Check Date: ' + PR.FieldByName('CHECK_DATE').AsString
              + '  Run #' + PR.FieldByName('RUN_NUMBER').AsString
              + '  Check #' + PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').AsString
              + '  EE#' + DM_COMPANY.EE.FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString
              + '  - ' + DM_COMPANY.CL_PERSON.FieldByName('FIRST_NAME').AsString
              + ' ' + DM_COMPANY.CL_PERSON.FieldByName('LAST_NAME').AsString;

  end;
  
end;

initialization
  RegisterClass(TADJUSTTAXES);

end.
