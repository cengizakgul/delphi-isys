inherited EDIT_PR_CHECK_LINE_LOCALS: TEDIT_PR_CHECK_LINE_LOCALS
  Width = 831
  Height = 532
  inherited PageControl1: TevPageControl
    Top = 66
    Width = 831
    Height = 466
    OnChange = PRPageControlChange
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 823
        Height = 437
        inherited pnlBorder: TevPanel
          Width = 819
          Height = 433
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 819
          Height = 433
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 253
              Width = 547
            end
            inherited pnlSubbrowse: TevPanel
              Left = 253
              Width = 547
            end
          end
          inherited Panel3: TevPanel
            Width = 771
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 771
            Height = 326
            inherited Splitter1: TevSplitter
              Height = 322
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 322
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 242
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINE_LOCALS\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 485
              Height = 322
              object evSplitter1: TevSplitter [0]
                Left = 258
                Top = 48
                Height = 242
              end
              inherited PRGrid: TevDBGrid
                Width = 240
                Height = 242
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINE_LOCALS\PRGrid'
                Align = alLeft
                PaintOptions.AlternatingRowColor = 14544093
              end
              object evDBGrid1: TevDBGrid
                Left = 261
                Top = 48
                Width = 192
                Height = 242
                TabStop = False
                DisableThemesInTitle = False
                Selected.Strings = (
                  'PERIOD_BEGIN_DATE'#9'10'#9'Period Begin Date'#9
                  'PERIOD_END_DATE'#9'10'#9'Period End Date'#9'No'
                  'FREQUENCY'#9'1'#9'Frequency'#9'No'
                  'PAY_SALARY'#9'1'#9'Pay Salary'#9'No'
                  'PAY_STANDARD_HOURS'#9'1'#9'Pay Standard Hours'#9'No'
                  'LOAD_DBDT_DEFAULTS'#9'1'#9'Load Dbdt Defaults'#9'No')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINE_LOCALS\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster1
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Checks'
      ImageIndex = 30
      object evSplitter2: TevSplitter
        Left = 319
        Top = 0
        Height = 466
      end
      object wwDBGrid1: TevDBGrid
        Left = 322
        Top = 0
        Width = 455
        Height = 466
        DisableThemesInTitle = False
        Selected.Strings = (
          'CL_E_DS_NBR'#9'10'#9'Cl E Ds Nbr'#9
          'LINE_TYPE'#9'1'#9'Line Type'#9
          'LINE_ITEM_DATE'#9'1'#9'Line Item Date'#9
          'CO_DIVISION_NBR'#9'10'#9'Co Division Nbr'#9
          'CO_BRANCH_NBR'#9'10'#9'Co Branch Nbr'#9
          'CO_DEPARTMENT_NBR'#9'10'#9'Co Department Nbr'#9
          'CO_TEAM_NBR'#9'10'#9'Co Team Nbr'#9
          'CO_JOBS_NBR'#9'10'#9'Co Jobs Nbr'#9
          'EE_STATES_NBR'#9'10'#9'Ee States Nbr'#9
          'EE_SUI_STATES_NBR'#9'10'#9'Ee Sui States Nbr'#9
          'RATE_OF_PAY'#9'10'#9'Rate Of Pay'#9
          'CO_WORKERS_COMP_NBR'#9'10'#9'Co Workers Comp Nbr'#9
          'CL_AGENCY_NBR'#9'10'#9'Cl Agency Nbr'#9
          'AGENCY_STATUS'#9'1'#9'Agency Status'#9
          'HOURS_OR_PIECES'#9'10'#9'Hours Or Pieces'#9
          'AMOUNT'#9'10'#9'Amount'#9
          'E_D_DIFFERENTIAL_AMOUNT'#9'10'#9'E D Differential Amount'#9
          'CO_SHIFTS_NBR'#9'10'#9'Co Shifts Nbr'#9
          'CL_PIECES_NBR'#9'10'#9'Cl Pieces Nbr'#9
          'DEDUCTION_SHORTFALL_CARRYOVER'#9'10'#9'Deduction Shortfall Carryover'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINE_LOCALS\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsSubMaster3
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object evDBGrid2: TevDBGrid
        Left = 0
        Top = 0
        Width = 319
        Height = 466
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'EE_Custom_Number'#9'9'#9'EE Custom Nbr'
          'CUSTOM_PR_BANK_ACCT_NUMBER'#9'20'#9'Custom Pr Bank Acct Number'#9
          'PAYMENT_SERIAL_NUMBER'#9'10'#9'Payment Serial Number'#9
          'CHECK_TYPE'#9'1'#9'Check Type'#9
          'CO_DELIVERY_PACKAGE_NBR'#9'10'#9'Co Delivery Package Nbr'#9
          'SALARY'#9'10'#9'Salary'#9
          'EXCLUDE_DD'#9'1'#9'Exclude Dd'#9
          'EXCLUDE_DD_EXCEPT_NET'#9'1'#9'Exclude Dd Except Net'#9
          'EXCLUDE_TIME_OFF_ACCURAL'#9'1'#9'Exclude Time Off Accural'#9
          'EXCLUDE_AUTO_DISTRIBUTION'#9'1'#9'Exclude Auto Distribution'#9
          'EXCLUDE_ALL_SCHED_E_D_CODES'#9'1'#9'Exclude All Sched E D Codes'#9
          'EXCLUDE_SCH_E_D_EXCEPT_PENSION'#9'1'#9'Exclude Sch E D Except Pension'#9
          'OR_CHECK_FEDERAL_VALUE'#9'10'#9'Or Check Federal Value'#9
          'OR_CHECK_FEDERAL_TYPE'#9'1'#9'Or Check Federal Type'#9
          'OR_CHECK_OASDI'#9'10'#9'Or Check Oasdi'#9
          'OR_CHECK_MEDICARE'#9'10'#9'Or Check Medicare'#9
          'OR_CHECK_EIC'#9'10'#9'Or Check Eic'#9
          'OR_CHECK_BACK_UP_WITHHOLDING'#9'10'#9'Or Check Back Up Withholding'#9
          'EXCLUDE_FEDERAL'#9'1'#9'Exclude Federal'#9
          'EXCLUDE_ADDITIONAL_FEDERAL'#9'1'#9'Exclude Additional Federal'#9
          'EXCLUDE_EMPLOYEE_OASDI'#9'1'#9'Exclude Employee Oasdi'#9
          'EXCLUDE_EMPLOYER_OASDI'#9'1'#9'Exclude Employer Oasdi'#9
          'EXCLUDE_EMPLOYEE_MEDICARE'#9'1'#9'Exclude Employee Medicare'#9
          'EXCLUDE_EMPLOYER_MEDICARE'#9'1'#9'Exclude Employer Medicare'#9
          'EXCLUDE_EMPLOYEE_EIC'#9'1'#9'Exclude Employee Eic'#9
          'EXCLUDE_EMPLOYER_FUI'#9'1'#9'Exclude Employer Fui'#9
          'TAX_AT_SUPPLEMENTAL_RATE'#9'1'#9'Tax At Supplemental Rate'#9
          'TAX_FREQUENCY'#9'1'#9'Tax Frequency'#9
          'CHECK_STATUS'#9'1'#9'Check Status'#9
          'STATUS_CHANGE_DATE'#9'10'#9'Status Change Date'#9
          'GROSS_WAGES'#9'10'#9'Gross Wages'#9
          'NET_WAGES'#9'10'#9'Net Wages'#9
          'FEDERAL_TAXABLE_WAGES'#9'10'#9'Federal Taxable Wages'#9
          'FEDERAL_TAX'#9'10'#9'Federal Tax'#9
          'EE_OASDI_TAXABLE_WAGES'#9'10'#9'Ee Oasdi Taxable Wages'#9
          'EE_OASDI_TAXABLE_TIPS'#9'10'#9'Ee Oasdi Taxable Tips'#9
          'EE_OASDI_TAX'#9'10'#9'Ee Oasdi Tax'#9
          'EE_MEDICARE_TAXABLE_WAGES'#9'10'#9'Ee Medicare Taxable Wages'#9
          'EE_MEDICARE_TAX'#9'10'#9'Ee Medicare Tax'#9
          'EE_EIC_TAX'#9'10'#9'Ee Eic Tax'#9
          'ER_OASDI_TAXABLE_WAGES'#9'10'#9'Er Oasdi Taxable Wages'#9
          'ER_OASDI_TAXABLE_TIPS'#9'10'#9'Er Oasdi Taxable Tips'#9
          'ER_OASDI_TAX'#9'10'#9'Er Oasdi Tax'#9
          'ER_MEDICARE_TAXABLE_WAGES'#9'10'#9'Er Medicare Taxable Wages'#9
          'ER_MEDICARE_TAX'#9'10'#9'Er Medicare Tax'#9
          'ER_FUI_TAXABLE_WAGES'#9'10'#9'Er Fui Taxable Wages'#9
          'ER_FUI_TAX'#9'10'#9'Er Fui Tax'#9
          'EXCLUDE_FROM_AGENCY'#9'1'#9'Exclude From Agency'#9
          'FEDERAL_SHORTFALL'#9'10'#9'Federal Shortfall'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINE_LOCALS\evDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsSubMaster2
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object Label2: TevLabel
        Left = 8
        Top = 336
        Width = 81
        Height = 13
        Caption = '~Local'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 0
        Width = 617
        Height = 321
        DisableThemesInTitle = False
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'LOCAL NAME')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINE_LOCALS\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwDBLookupCombo1: TevDBLookupCombo
        Left = 8
        Top = 352
        Width = 337
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup')
        DataField = 'EE_LOCALS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_EE_LOCALS.EE_LOCALS
        LookupField = 'EE_LOCALS_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object drgpExempt_Exclude: TevDBRadioGroup
        Left = 361
        Top = 337
        Width = 257
        Height = 51
        Caption = '~Exempt or Block'
        Columns = 3
        DataField = 'EXEMPT_EXCLUDE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Exempt'
          'Exclude'
          'Include')
        ParentFont = False
        TabOrder = 2
        Values.Strings = (
          'E'
          'X'
          'I')
      end
    end
  end
  inherited Panel1: TevPanel
    Width = 831
    inherited pnlFavoriteReport: TevPanel
      Left = 716
    end
  end
  object evPanel1: TevPanel [2]
    Left = 0
    Top = 33
    Width = 831
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object Label1: TevLabel
      Left = 8
      Top = 0
      Width = 51
      Height = 13
      Caption = 'E/D Code:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText1: TevDBText
      Left = 106
      Top = 0
      Width = 161
      Height = 17
      DataField = 'E_D_Code_Lookup'
      DataSource = wwdsSubMaster3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TevDBText
      Left = 106
      Top = 16
      Width = 199
      Height = 17
      DataField = 'E_D_Description_Lookup'
      DataSource = wwdsSubMaster3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TevLabel
      Left = 8
      Top = 16
      Width = 79
      Height = 13
      Caption = 'E/D Description:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object pnlLocation: TevPanel
      Left = 178
      Top = 12
      Width = 340
      Height = 25
      BevelOuter = bvNone
      TabOrder = 0
      object evLabel2: TevLabel
        Left = 8
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Payroll: #'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evDBText1: TevDBText
        Left = 66
        Top = 4
        Width = 15
        Height = 25
        DataField = 'RUN_NUMBER'
        DataSource = wwdsSubMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evDBText2: TevDBText
        Left = 88
        Top = 4
        Width = 64
        Height = 13
        AutoSize = True
        DataField = 'CHECK_DATE'
        DataSource = wwdsSubMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object evPanel3: TevPanel
        Left = 160
        Top = 4
        Width = 177
        Height = 25
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel3: TevLabel
          Left = 0
          Top = 0
          Width = 36
          Height = 25
          Align = alLeft
          Caption = 'Period: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText3: TevDBText
          Left = 36
          Top = 0
          Width = 64
          Height = 25
          Align = alLeft
          AutoSize = True
          DataField = 'PERIOD_BEGIN_DATE'
          DataSource = wwdsSubMaster1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object evLabel4: TevLabel
          Left = 100
          Top = 0
          Width = 9
          Height = 25
          Align = alLeft
          Caption = ' - '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText4: TevDBText
          Left = 109
          Top = 0
          Width = 64
          Height = 25
          Align = alLeft
          AutoSize = True
          DataField = 'PERIOD_END_DATE'
          DataSource = wwdsSubMaster1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object butnPrior: TevBitBtn
      Left = 520
      Top = 4
      Width = 81
      Height = 25
      Caption = '&Prior Line'
      TabOrder = 1
      OnClick = butnPriorClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDD
        848DDDDDDDDDDDDDF8FDDDDDDDDDDD84448DDDDDDDDDDDF888FDDDDDDDDD844F
        F48DDDDDDDDDF88DD8FDDDDDDD844FFFF48DDDDDDDF88DDDD8FDDDDD844FFFFF
        F48DDDDDF88DDDDDD8FDDDD44FFFFFFFF48DDDD88DDDDDDDD8FDDD477FFFFFFF
        F48DDD8FFDDDDDDDD8FDDDD4477FFFFFF48DDDD88FFDDDDDD8FDDDDDD4477FFF
        F48DDDDDD88FFDDDD8FDDDDDDDD4477FF48DDDDDDDD88FFDD8FDDDDDDDDDD447
        748DDDDDDDDDD88FF8FDDDDDDDDDDDD4448DDDDDDDDDDDD888FDDDDDDDDDDDDD
        D4DDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnNext: TevBitBtn
      Left = 608
      Top = 4
      Width = 81
      Height = 25
      Caption = '&Next Line'
      TabOrder = 2
      OnClick = butnNextClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDDD4888DDDDDDD
        DDDDD8FFDDDDDDDDDDDDD444888DDDDDDDDDD888FFDDDDDDDDDDD47F44888DDD
        DDDDD8FD88FFDDDDDDDDD47FFF44888DDDDDD8FDDD88FFDDDDDDD47FFFFF4488
        8DDDD8FDDDDD88FFDDDDD47FFFFFFF4488DDD8FDDDDDDD88FDDDD47FFFFFFF77
        4DDDD8FDDDDDDDFF8DDDD47FFFFF7744DDDDD8FDDDDDFF88DDDDD47FFF7744DD
        DDDDD8FDDDFF88DDDDDDD47F7744DDDDDDDDD8FDFF88DDDDDDDDD47744DDDDDD
        DDDDD8FF88DDDDDDDDDDD444DDDDDDDDDDDDD888DDDDDDDDDDDDD4DDDDDDDDDD
        DDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Layout = blGlyphRight
      Margin = 0
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 10
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_PR_CHECK_LINE_LOCALS.PR_CHECK_LINE_LOCALS
    MasterDataSource = wwdsSubMaster3
    Top = 2
  end
  inherited wwdsList: TevDataSource
    Left = 82
    Top = 2
  end
  inherited DM_TEMPORARY: TDM_TEMPORARY
    Top = 7
  end
  inherited dsCL: TevDataSource
    Top = 65527
  end
  inherited DM_CLIENT: TDM_CLIENT
    Left = 328
    Top = 7
  end
  inherited DM_COMPANY: TDM_COMPANY
    Top = 2
  end
  inherited wwdsSubMaster: TevDataSource
    Left = 182
    Top = 128
  end
  inherited DM_PAYROLL: TDM_PAYROLL
    Top = 2
  end
  inherited ActionList: TevActionList
    Left = 395
    Top = 131
    inherited PRNext: TDataSetNext
      OnExecute = PRNextExecute
    end
    inherited PRPrior: TDataSetPrior
      OnExecute = PRPriorExecute
    end
  end
  object wwdsSubMaster1: TevDataSource
    DataSet = DM_PR_BATCH.PR_BATCH
    MasterDataSource = wwdsSubMaster
    Left = 212
    Top = 120
  end
  object wwdsSubMaster2: TevDataSource
    DataSet = DM_PR_CHECK.PR_CHECK
    MasterDataSource = wwdsSubMaster1
    OnDataChangeBeforeChildren = wwdsSubMaster2DataChangeBeforeChildren
    Left = 248
    Top = 122
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 522
    Top = 10
  end
  object wwdsSubMaster3: TevDataSource
    DataSet = DM_PR_CHECK_LINES.PR_CHECK_LINES
    OnDataChange = wwdsDataChange
    MasterDataSource = wwdsSubMaster2
    Left = 316
    Top = 135
  end
  object EESrc: TevDataSource
    DataSet = DM_EE.EE
    Left = 503
    Top = 104
  end
  object EEStatesSrc: TevDataSource
    DataSet = DM_EE_LOCALS.EE_LOCALS
    MasterDataSource = EESrc
    Left = 568
    Top = 120
  end
end
