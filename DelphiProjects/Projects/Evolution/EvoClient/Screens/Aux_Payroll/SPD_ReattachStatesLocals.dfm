object ReattachStatesLocals: TReattachStatesLocals
  Left = 359
  Top = 179
  ActiveControl = wwDBGrid4
  BorderStyle = bsDialog
  Caption = 'Reattach States and Locals'
  ClientHeight = 466
  ClientWidth = 651
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pcMain: TevPageControl
    Left = 0
    Top = 0
    Width = 651
    Height = 466
    ActivePage = tsPayrolls
    Align = alClient
    TabOrder = 0
    OnChange = pcMainChange
    object tsPayrolls: TTabSheet
      Caption = 'Select'
      ImageIndex = 1
      object evPanel3: TevPanel
        Left = 0
        Top = 397
        Width = 643
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object evSpeedButton1: TevSpeedButton
          Left = 466
          Top = 8
          Width = 130
          Height = 25
          Caption = 'Next'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333FFF333333333333000333333333
            3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
            3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
            0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
            BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
            33337777773FF733333333333300033333333333337773333333333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            3333333333333333333333333333333333333333333333333333}
          Layout = blGlyphRight
          NumGlyphs = 2
          OnClick = evSpeedButton1Click
          ShortCut = 0
        end
      end
      object wwDBGrid6: TevDBGrid
        Left = 0
        Top = 39
        Width = 643
        Height = 358
        HelpContext = 1502
        DisableThemesInTitle = False
        Selected.Strings = (
          'CHECK_DATE'#9'18'#9'Check Date'#9'F'
          'RUN_NUMBER'#9'10'#9'Run'#9'F'
          'CHECK_DATE_STATUS'#9'1'#9'Check Date Status'#9'F'
          'SCHEDULED'#9'1'#9'Scheduled'#9'F'
          'PAYROLL_TYPE'#9'1'#9'Type'#9'F'
          'STATUS'#9'1'#9'Status'#9'F'
          'STATUS_DATE'#9'18'#9'Status Date'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TReattachStatesLocals\wwDBGrid6'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsPayrolls
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        DefaultSort = 'CHECK_DATE;RUN_NUMBER'
      end
      object evPanel4: TevPanel
        Left = 0
        Top = 0
        Width = 643
        Height = 39
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel1: TevLabel
          Left = 7
          Top = 11
          Width = 46
          Height = 13
          Caption = 'Employee'
        end
        object evLabel2: TevLabel
          Left = 255
          Top = 11
          Width = 22
          Height = 13
          Caption = 'Year'
        end
        object wwDBGrid4: TevDBLookupCombo
          Left = 67
          Top = 8
          Width = 177
          Height = 21
          ControlType.Strings = (
            'CURRENT_TERMINATION_CODE;CustomEdit;cbEmployeeCode')
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'Trim_Number'#9'9'#9'Aligned EE Code'#9'F'
            'Employee_LastName_Calculate'#9'15'#9'Last Name'#9'F'
            'Employee_FirstName_Calculate'#9'15'#9'First Name'#9'F'
            'Employee_MI_Calculate'#9'2'#9'MI'#9'F'
            'SOCIAL_SECURITY_NUMBER'#9'13'#9'SSN'#9'F'
            'CURRENT_TERMINATION_CODE_DESC'#9'20'#9'Status'#9'F')
          LookupTable = DM_EE.EE
          LookupField = 'EE_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = wwDBGrid4Change
          OnBeforeDropDown = wwDBGrid4BeforeDropDown
          OnCloseUp = wwDBGrid4CloseUp
        end
        object cbYear: TevComboBox
          Left = 289
          Top = 8
          Width = 80
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
          OnChange = cbYearChange
        end
      end
    end
    object tsStatesLocals: TTabSheet
      Caption = 'States/Locals'
      ImageIndex = 2
      object evSplitter1: TevSplitter
        Left = 0
        Top = 105
        Width = 643
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object evSplitter2: TevSplitter
        Left = 0
        Top = 217
        Width = 643
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object evDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 643
        Height = 105
        DisableThemesInTitle = False
        ControlType.Strings = (
          'Dest_State;CustomEdit;lcStates;F')
        Selected.Strings = (
          'State'#9'15'#9'Source State'#9'T'
          'Dest_State'#9'15'#9'Destination State'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TReattachStatesLocals\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alTop
        DataSource = dsStates
        KeyOptions = [dgEnterToTab]
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        Sorting = False
      end
      object evDBGrid3: TevDBGrid
        Left = 0
        Top = 108
        Width = 643
        Height = 109
        DisableThemesInTitle = False
        ControlType.Strings = (
          'Dest_State;CustomEdit;lcSUI;F')
        Selected.Strings = (
          'State'#9'15'#9'Source SUI'#9'T'
          'Dest_State'#9'15'#9'Destination SUI'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TReattachStatesLocals\evDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alTop
        DataSource = dsSUI
        KeyOptions = [dgEnterToTab]
        ReadOnly = False
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        Sorting = False
      end
      object lcSUI: TevDBLookupCombo
        Left = 208
        Top = 160
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'State_Lookup'#9'2'#9'State_Lookup'#9'F'
          'SDI_State_Lookup'#9'2'#9'SDI_State_Lookup'#9'F'
          'SUI_State_Lookup'#9'2'#9'SUI_State_Lookup'#9'F')
        DataField = 'DEST_EE_STATES_NBR'
        DataSource = dsSUI
        LookupTable = DM_EE_STATES.EE_STATES
        LookupField = 'EE_STATES_NBR'
        Style = csDropDownList
        TabOrder = 6
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object evPanel2: TevPanel
        Left = 0
        Top = 397
        Width = 643
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 5
        object evSpeedButton2: TevSpeedButton
          Left = 426
          Top = 8
          Width = 178
          Height = 25
          Caption = 'Create Checks'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD88888DDDDD
            DDDDDF88888FDDDDDDDDD8FBB33888888DDDF8DDD888FFFFFDDD8B33333B8333
            38DD8D88888D88888FDD83FBB3338BB3338D88DDD8888DD888FD8B33333B8333
            3B8D8D88888D88888D8F83BBBBB38BB3338D88DDDDD88DD8888F8BBBBBBB8333
            3B8D8DDDDDDD88888D8F08BBBBB8FBB33380D8DDDDD8DDD8888F0F88888B3333
            3B808D88888D88888D8F0FF88883BBBBB3808DDDDD88DDDDD88F0FF7778BBBBB
            BB808DDDDD8DDDDDDD8F0FF88888BBBBB8F08DD888D8DDDDD8D80FF88FFF8888
            87F08DD88DDD88888DD80FFFFFFFFFF888F08DDDDDDDDDDDD8D80FFFFFFFFFFF
            FFF08DDDDDDDDDDDDDD800000000000000008888888888888888}
          NumGlyphs = 2
          OnClick = evSpeedButton2Click
          ShortCut = 0
        end
        object evCbNewPayroll: TevCheckBox
          Left = 320
          Top = 8
          Width = 97
          Height = 25
          Caption = 'New Payroll'
          TabOrder = 0
          OnClick = evCbNewPayrollClick
        end
      end
      object evDBGrid2: TevDBGrid
        Left = 0
        Top = 220
        Width = 643
        Height = 177
        DisableThemesInTitle = False
        ControlType.Strings = (
          'Dest_Local;CustomEdit;lcLocals;F')
        Selected.Strings = (
          'Local'#9'40'#9'Source Local'#9'T'
          'Dest_Local'#9'40'#9'Destination Local'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TReattachStatesLocals\evDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsLocals
        KeyOptions = [dgEnterToTab]
        ReadOnly = False
        TabOrder = 2
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        Sorting = False
      end
      object lcStates: TevDBLookupCombo
        Left = 216
        Top = 48
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'State_Lookup'#9'2'#9'State_Lookup'#9'F')
        DataField = 'DEST_EE_STATES_NBR'
        DataSource = dsStates
        LookupTable = DM_EE_STATES.EE_STATES
        LookupField = 'EE_STATES_NBR'
        Style = csDropDownList
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object lcLocals: TevDBLookupCombo
        Left = 216
        Top = 240
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'Local_Name_Lookup'#9'40'#9'Local_Name_Lookup'#9'F')
        DataField = 'DEST_EE_LOCALS_NBR'
        DataSource = dsLocals
        LookupTable = DM_EE_LOCALS.EE_LOCALS
        LookupField = 'EE_LOCALS_NBR'
        Style = csDropDownList
        TabOrder = 4
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
    end
  end
  object dsEmployees: TevDataSource
    DataSet = DM_EE.EE
    Left = 352
    Top = 8
  end
  object dsPayrolls: TevDataSource
    DataSet = cdsPayrolls
    Left = 392
    Top = 8
  end
  object cdsPayrolls: TevClientDataSet
    ProviderName = 'PR_PROV'
    Left = 396
    Top = 40
  end
  object DM_EMPLOYEE: TDM_EMPLOYEE
    Left = 320
    Top = 8
  end
  object dsStates: TevDataSource
    DataSet = cdsStates
    Left = 432
    Top = 8
  end
  object dsLocals: TevDataSource
    DataSet = cdsLocals
    Left = 472
    Top = 8
  end
  object cdsStates: TevClientDataSet
    ProviderName = 'EE_STATES_PROV'
    Left = 436
    Top = 40
    object cdsStatesEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsStatesEE_STATES_NBR: TIntegerField
      FieldName = 'EE_STATES_NBR'
    end
    object cdsStatesStates: TStringField
      FieldKind = fkLookup
      FieldName = 'State'
      LookupDataSet = DM_EE_STATES.EE_STATES
      LookupKeyFields = 'EE_STATES_NBR'
      LookupResultField = 'State_Lookup'
      KeyFields = 'EE_STATES_NBR'
      Size = 2
      Lookup = True
    end
    object cdsStatesDEST_EE_STATES_NBR: TIntegerField
      FieldName = 'DEST_EE_STATES_NBR'
    end
    object cdsStatesDest_State: TStringField
      FieldKind = fkLookup
      FieldName = 'Dest_State'
      LookupDataSet = DM_EE_STATES.EE_STATES
      LookupKeyFields = 'EE_STATES_NBR'
      LookupResultField = 'State_Lookup'
      KeyFields = 'DEST_EE_STATES_NBR'
      Size = 2
      Lookup = True
    end
  end
  object cdsLocals: TevClientDataSet
    ProviderName = 'EE_LOCALS_PROV'
    Left = 476
    Top = 40
    object cdsLocalsEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
    end
    object cdsLocalsEE_LOCALS_NBR: TIntegerField
      FieldName = 'EE_LOCALS_NBR'
    end
    object cdsLocalsLocal: TStringField
      FieldKind = fkLookup
      FieldName = 'Local'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'EE_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
    object cdsLocalsDEST_EE_LOCALS_NBR: TIntegerField
      FieldName = 'DEST_EE_LOCALS_NBR'
    end
    object cdsLocalsDest_Local: TStringField
      FieldKind = fkLookup
      FieldName = 'Dest_Local'
      LookupDataSet = DM_EE_LOCALS.EE_LOCALS
      LookupKeyFields = 'EE_LOCALS_NBR'
      LookupResultField = 'Local_Name_Lookup'
      KeyFields = 'DEST_EE_LOCALS_NBR'
      Size = 40
      Lookup = True
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 288
    Top = 8
  end
  object dsEE_STATES: TevDataSource
    DataSet = DM_EE_STATES.EE_STATES
    Left = 536
    Top = 8
  end
  object dsEE_LOCALS: TevDataSource
    DataSet = DM_EE_LOCALS.EE_LOCALS
    Left = 576
    Top = 8
  end
  object dsSUI: TevDataSource
    DataSet = cdsSUI
    Left = 504
    Top = 8
  end
  object cdsSUI: TevClientDataSet
    ProviderName = 'EE_STATES_PROV'
    OnCalcFields = cdsSUICalcFields
    Left = 508
    Top = 40
    object IntegerField1: TIntegerField
      FieldName = 'EE_NBR'
    end
    object IntegerField2: TIntegerField
      FieldName = 'EE_STATES_NBR'
    end
    object StringField1: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'State'
      LookupDataSet = DM_EE_STATES.EE_STATES
      LookupKeyFields = 'EE_STATES_NBR'
      LookupResultField = 'State_Lookup'
      KeyFields = 'EE_STATES_NBR'
      Size = 8
    end
    object IntegerField3: TIntegerField
      FieldName = 'DEST_EE_STATES_NBR'
    end
    object StringField2: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'Dest_State'
      LookupDataSet = DM_EE_STATES.EE_STATES
      LookupKeyFields = 'EE_STATES_NBR'
      LookupResultField = 'State_Lookup'
      KeyFields = 'DEST_EE_STATES_NBR'
      Size = 8
    end
  end
  object PR_LIST: TevClientDataSet
    ProviderName = 'PR_PROV'
    Left = 404
    Top = 88
  end
end
