inherited EDIT_PR_CHECK_LINES_SIMPLE: TEDIT_PR_CHECK_LINES_SIMPLE
  Width = 791
  Height = 612
  inherited PageControl1: TevPageControl
    Top = 132
    Width = 791
    Height = 447
    OnChange = PRPageControlChange
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 783
        Height = 418
        inherited pnlBorder: TevPanel
          Width = 779
          Height = 414
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 779
          Height = 414
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 253
              Width = 547
            end
            inherited pnlSubbrowse: TevPanel
              Left = 253
              Width = 547
            end
          end
          inherited Panel3: TevPanel
            Width = 731
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 731
            Height = 307
            inherited Splitter1: TevSplitter
              Height = 303
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 303
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 223
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINES_SIMPLE\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 445
              Height = 303
              object evSplitter1: TevSplitter [0]
                Left = 258
                Top = 48
                Height = 223
              end
              inherited PRGrid: TevDBGrid
                Width = 240
                Height = 223
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINES_SIMPLE\PRGrid'
                Align = alLeft
                PaintOptions.AlternatingRowColor = 14544093
              end
              object evDBGrid1: TevDBGrid
                Left = 261
                Top = 48
                Width = 152
                Height = 223
                TabStop = False
                DisableThemesInTitle = False
                Selected.Strings = (
                  'PERIOD_BEGIN_DATE'#9'10'#9'Period Begin Date'#9
                  'PERIOD_END_DATE'#9'10'#9'Period End Date'#9'No'
                  'FREQUENCY'#9'1'#9'Frequency'#9'No'
                  'PAY_SALARY'#9'1'#9'Pay Salary'#9'No'
                  'PAY_STANDARD_HOURS'#9'1'#9'Pay Standard Hours'#9'No'
                  'LOAD_DBDT_DEFAULTS'#9'1'#9'Load Dbdt Defaults'#9'No')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINES_SIMPLE\evDBGrid1'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = wwdsSubMaster1
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = 14544093
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
            end
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Checks'
      ImageIndex = 30
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 0
        Width = 818
        Height = 415
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'EE_Custom_Number'#9'9'#9'EE Custom Nbr'
          'CUSTOM_PR_BANK_ACCT_NUMBER'#9'20'#9'Custom Pr Bank Acct Number'#9'No'
          'PAYMENT_SERIAL_NUMBER'#9'10'#9'Payment Serial Number'#9'No'
          'CHECK_TYPE'#9'1'#9'Check Type'#9'No'
          'CO_DELIVERY_PACKAGE_NBR'#9'10'#9'Co Delivery Package Nbr'#9'No'
          'SALARY'#9'10'#9'Salary'#9'No'
          'EXCLUDE_DD'#9'1'#9'Exclude Dd'#9'No'
          'EXCLUDE_DD_EXCEPT_NET'#9'1'#9'Exclude Dd Except Net'#9'No'
          'EXCLUDE_TIME_OFF_ACCURAL'#9'1'#9'Exclude Time Off Accural'#9'No'
          'EXCLUDE_AUTO_DISTRIBUTION'#9'1'#9'Exclude Auto Distribution'#9'No'
          'EXCLUDE_ALL_SCHED_E_D_CODES'#9'1'#9'Exclude All Sched E D Codes'#9'No'
          
            'EXCLUDE_SCH_E_D_EXCEPT_PENSION'#9'1'#9'Exclude Sch E D Except Pension'#9 +
            'No'
          'OR_CHECK_FEDERAL_VALUE'#9'10'#9'Or Check Federal Value'#9'No'
          'OR_CHECK_FEDERAL_TYPE'#9'1'#9'Or Check Federal Type'#9'No'
          'OR_CHECK_OASDI'#9'10'#9'Or Check Oasdi'#9'No'
          'OR_CHECK_MEDICARE'#9'10'#9'Or Check Medicare'#9'No'
          'OR_CHECK_EIC'#9'10'#9'Or Check Eic'#9'No'
          'OR_CHECK_BACK_UP_WITHHOLDING'#9'10'#9'Or Check Back Up Withholding'#9'No'
          'EXCLUDE_FEDERAL'#9'1'#9'Exclude Federal'#9'No'
          'EXCLUDE_ADDITIONAL_FEDERAL'#9'1'#9'Exclude Additional Federal'#9'No'
          'EXCLUDE_EMPLOYEE_OASDI'#9'1'#9'Exclude Employee Oasdi'#9'No'
          'EXCLUDE_EMPLOYER_OASDI'#9'1'#9'Exclude Employer Oasdi'#9'No'
          'EXCLUDE_EMPLOYEE_MEDICARE'#9'1'#9'Exclude Employee Medicare'#9'No'
          'EXCLUDE_EMPLOYER_MEDICARE'#9'1'#9'Exclude Employer Medicare'#9'No'
          'EXCLUDE_EMPLOYEE_EIC'#9'1'#9'Exclude Employee Eic'#9'No'
          'EXCLUDE_EMPLOYER_FUI'#9'1'#9'Exclude Employer Fui'#9'No'
          'TAX_AT_SUPPLEMENTAL_RATE'#9'1'#9'Tax At Supplemental Rate'#9'No'
          'TAX_FREQUENCY'#9'1'#9'Tax Frequency'#9'No'
          'CHECK_STATUS'#9'1'#9'Check Status'#9'No'
          'STATUS_CHANGE_DATE'#9'10'#9'Status Change Date'#9'No'
          'GROSS_WAGES'#9'10'#9'Gross Wages'#9'No'
          'NET_WAGES'#9'10'#9'Net Wages'#9'No'
          'FEDERAL_TAXABLE_WAGES'#9'10'#9'Federal Taxable Wages'#9'No'
          'FEDERAL_TAX'#9'10'#9'Federal Tax'#9'No'
          'EE_OASDI_TAXABLE_WAGES'#9'10'#9'Ee Oasdi Taxable Wages'#9'No'
          'EE_OASDI_TAXABLE_TIPS'#9'10'#9'Ee Oasdi Taxable Tips'#9'No'
          'EE_OASDI_TAX'#9'10'#9'Ee Oasdi Tax'#9'No'
          'EE_MEDICARE_TAXABLE_WAGES'#9'10'#9'Ee Medicare Taxable Wages'#9'No'
          'EE_MEDICARE_TAX'#9'10'#9'Ee Medicare Tax'#9'No'
          'EE_EIC_TAX'#9'10'#9'Ee Eic Tax'#9'No'
          'ER_OASDI_TAXABLE_WAGES'#9'10'#9'Er Oasdi Taxable Wages'#9'No'
          'ER_OASDI_TAXABLE_TIPS'#9'10'#9'Er Oasdi Taxable Tips'#9'No'
          'ER_OASDI_TAX'#9'10'#9'Er Oasdi Tax'#9'No'
          'ER_MEDICARE_TAXABLE_WAGES'#9'10'#9'Er Medicare Taxable Wages'#9'No'
          'ER_MEDICARE_TAX'#9'10'#9'Er Medicare Tax'#9'No'
          'ER_FUI_TAXABLE_WAGES'#9'10'#9'Er Fui Taxable Wages'#9'No'
          'ER_FUI_TAX'#9'10'#9'Er Fui Tax'#9'No'
          'EXCLUDE_FROM_AGENCY'#9'1'#9'Exclude From Agency'#9'No'
          'FEDERAL_SHORTFALL'#9'10'#9'Federal Shortfall'#9'No')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINES_SIMPLE\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsSubMaster2
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Details'
      ImageIndex = 2
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 783
        Height = 418
        DisableThemesInTitle = False
        ControlType.Strings = (
          'E_D_Code_Lookup;CustomEdit;wwlcE_D_CODE')
        Selected.Strings = (
          'E_D_Code_Lookup'#9'20'#9'E/D CODE'
          'E_D_Description_Lookup'#9'40'#9'E/D DESCRIPTION'
          'HOURS_OR_PIECES'#9'10'#9'HOURS'
          'RATE_OF_PAY'#9'10'#9'RATE'
          'AMOUNT'#9'10'#9'Amount'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_CHECK_LINES_SIMPLE\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        KeyOptions = [dgEnterToTab, dgAllowInsert]
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnKeyDown = wwDBGrid1KeyDown
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwlcE_D_CODE: TevDBLookupCombo
        Left = 120
        Top = 24
        Width = 17
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'ED_Lookup'#9'20'#9'ED_Lookup'
          'CodeDescription'#9'40'#9'CodeDescription')
        DataField = 'CL_E_DS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CO_E_D_CODES.CO_E_D_CODES
        LookupField = 'CL_E_DS_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnEnter = wwlcE_D_CODEEnter
        OnExit = wwlcE_D_CODEExit
      end
    end
  end
  inherited Panel1: TevPanel
    Width = 791
    Height = 66
    inherited pnlFavoriteReport: TevPanel
      Left = 676
      Height = 66
    end
  end
  object evPanel1: TevPanel [2]
    Left = 0
    Top = 66
    Width = 791
    Height = 66
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object Label1: TevLabel
      Left = 16
      Top = 32
      Width = 44
      Height = 13
      Caption = 'Check #:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText1: TevDBText
      Left = 72
      Top = 32
      Width = 73
      Height = 17
      DataField = 'PAYMENT_SERIAL_NUMBER'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TevLabel
      Left = 144
      Top = 32
      Width = 27
      Height = 13
      Caption = 'EE #:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TevLabel
      Left = 16
      Top = 48
      Width = 48
      Height = 13
      Caption = 'EE Name:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText4: TevDBText
      Left = 188
      Top = 32
      Width = 85
      Height = 17
      DataField = 'EE_Custom_Number'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText5: TevDBText
      Left = 72
      Top = 48
      Width = 180
      Height = 17
      DataField = 'Employee_Name_Calculate'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object pnlLocation: TevPanel
      Left = 8
      Top = 4
      Width = 345
      Height = 25
      BevelOuter = bvNone
      TabOrder = 0
      object evLabel2: TevLabel
        Left = 8
        Top = 4
        Width = 47
        Height = 13
        Caption = 'Payroll:  #'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evDBText1: TevDBText
        Left = 66
        Top = 4
        Width = 15
        Height = 25
        DataField = 'RUN_NUMBER'
        DataSource = wwdsSubMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evDBText2: TevDBText
        Left = 88
        Top = 4
        Width = 64
        Height = 13
        AutoSize = True
        DataField = 'CHECK_DATE'
        DataSource = wwdsSubMaster
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object evPanel3: TevPanel
        Left = 160
        Top = 4
        Width = 185
        Height = 25
        BevelOuter = bvNone
        TabOrder = 0
        object evLabel3: TevLabel
          Left = 0
          Top = 0
          Width = 36
          Height = 25
          Align = alLeft
          Caption = 'Period: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText3: TevDBText
          Left = 36
          Top = 0
          Width = 64
          Height = 25
          Align = alLeft
          AutoSize = True
          DataField = 'PERIOD_BEGIN_DATE'
          DataSource = wwdsSubMaster1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object evLabel4: TevLabel
          Left = 100
          Top = 0
          Width = 9
          Height = 25
          Align = alLeft
          Caption = ' - '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBText4: TevDBText
          Left = 109
          Top = 0
          Width = 64
          Height = 25
          Align = alLeft
          AutoSize = True
          DataField = 'PERIOD_END_DATE'
          DataSource = wwdsSubMaster1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object butnPrior: TevBitBtn
      Left = 312
      Top = 41
      Width = 65
      Height = 25
      Caption = '&Prior'
      TabOrder = 1
      OnClick = butnPriorClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDD
        848DDDDDDDDDDDDDF8FDDDDDDDDDDD84448DDDDDDDDDDDF888FDDDDDDDDD844F
        F48DDDDDDDDDF88DD8FDDDDDDD844FFFF48DDDDDDDF88DDDD8FDDDDD844FFFFF
        F48DDDDDF88DDDDDD8FDDDD44FFFFFFFF48DDDD88DDDDDDDD8FDDD477FFFFFFF
        F48DDD8FFDDDDDDDD8FDDDD4477FFFFFF48DDDD88FFDDDDDD8FDDDDDD4477FFF
        F48DDDDDD88FFDDDD8FDDDDDDDD4477FF48DDDDDDDD88FFDD8FDDDDDDDDDD447
        748DDDDDDDDDD88FF8FDDDDDDDDDDDD4448DDDDDDDDDDDD888FDDDDDDDDDDDDD
        D4DDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object butnNext: TevBitBtn
      Left = 376
      Top = 41
      Width = 65
      Height = 25
      Caption = '&Next'
      TabOrder = 2
      OnClick = butnNextClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDFDDDDDDDDDDDDDD4888DDDDDDD
        DDDDD8FFDDDDDDDDDDDDD444888DDDDDDDDDD888FFDDDDDDDDDDD47F44888DDD
        DDDDD8FD88FFDDDDDDDDD47FFF44888DDDDDD8FDDD88FFDDDDDDD47FFFFF4488
        8DDDD8FDDDDD88FFDDDDD47FFFFFFF4488DDD8FDDDDDDD88FDDDD47FFFFFFF77
        4DDDD8FDDDDDDDFF8DDDD47FFFFF7744DDDDD8FDDDDDFF88DDDDD47FFF7744DD
        DDDDD8FDDDFF88DDDDDDD47F7744DDDDDDDDD8FDFF88DDDDDDDDD47744DDDDDD
        DDDDD8FF88DDDDDDDDDDD444DDDDDDDDDDDDD888DDDDDDDDDDDDD4DDDDDDDDDD
        DDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      NumGlyphs = 2
      Layout = blGlyphRight
      Margin = 0
    end
    object Button2: TevBitBtn
      Left = 440
      Top = 41
      Width = 89
      Height = 25
      Caption = 'Lookup &EE'
      TabOrder = 3
      OnClick = Button2Click
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D828D8FF4444
        44FFDDFDD8DD888888DDD2A288F4444444FFDF8FD8D8888888DDDAAA28F44444
        44FFD888F8D8888888DD44AAA2844FFF44FFDD888FD88DDD88D8C44AAA284777
        44FF8DD888F88FFF888DCC44AAA28AAA287788DD888FF888FFDDCCC48AA2A888
        AA2D888DD8888DDD88FDC77C7D277B3337A88DD8DDF8DD88DD8FC77C78A77BB3
        37A28DD8DD8D8DD88D8FDB3DDA777BBB377ADD8DD8FDDDDD8DD8DB33DA774B33
        377ADD88D8FD8D888DD8DBB3DA244BBBB77ADDD8D8F88DDDDDD84BBBD8A77444
        47A28DDDDD8DD8888D8FDCC4DDA2744447A8D888DD8FD888DD8DDCC4DD8AA277
        AA8DD888DDD88FDD88FDDDDDDDDD8AAA28DDDDDDDDDDD888FDDD}
      NumGlyphs = 2
      Margin = 0
    end
    object Button1: TevBitBtn
      Left = 528
      Top = 41
      Width = 105
      Height = 25
      Caption = 'Preview Check'
      TabOrder = 4
      OnClick = Button1Click
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        D88DDDDDDDDDDDDDDFFDDDDDDDDDDDDD84C8DDDDDDDDDDDDF88FDDDDDDDDDDD8
        4CCDDDDDDDDDDDDF888DDDDDDD888D84CCDDDDDDDDFFFDF888DDDDDD88CCC84C
        CDDDDDDDFF888F888DDDDDDDCC888C4CDDDDDDDD88DDD888DDDD8888C87777C8
        8888DDD88FDDDD8FDDDD000C8888888C000088D8FDDDDDD8F8880FFC8777777C
        8FF08DD8FDDDDDD8FDD80FFC8888888C88F08DD8FDDDDDD8F8D80FF7C87777C8
        77F08DDD8FDDDD8FDDD80FF8CC888CC888F08DDD88FFF88F88D80FF88FCCC8F7
        77F08DD8DD888DDDDDD80FFFFFFFFFF888F08DDDDDDDDDD888D80FFFFFFFFFFF
        FFF08DDDDDDDDDDDDDD800000000000000008888888888888888}
      NumGlyphs = 2
      Margin = 0
    end
  end
  object evPanel2: TevPanel [3]
    Left = 0
    Top = 579
    Width = 791
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
    object Label2: TevLabel
      Left = 22
      Top = 8
      Width = 54
      Height = 16
      Caption = 'Gross ='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TevLabel
      Left = 318
      Top = 8
      Width = 37
      Height = 16
      Caption = 'Net ='
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TevDBText
      Left = 78
      Top = 8
      Width = 60
      Height = 16
      AutoSize = True
      DataField = 'GROSS_WAGES'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText3: TevDBText
      Left = 358
      Top = 8
      Width = 60
      Height = 16
      AutoSize = True
      DataField = 'NET_WAGES'
      DataSource = wwdsSubMaster2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_PR_CHECK_LINES.PR_CHECK_LINES
    OnStateChange = wwdsDetailStateChange
    MasterDataSource = wwdsSubMaster2
  end
  inherited wwdsSubMaster: TevDataSource
    Left = 182
    Top = 120
  end
  inherited ActionList: TevActionList
    inherited PRNext: TDataSetNext
      DataSource = wwdsSubMaster2
    end
    inherited PRPrior: TDataSetPrior
      DataSource = wwdsSubMaster2
    end
  end
  object wwdsSubMaster1: TevDataSource
    DataSet = DM_PR_BATCH.PR_BATCH
    MasterDataSource = wwdsSubMaster
    Left = 212
    Top = 128
  end
  object wwdsSubMaster2: TevDataSource
    DataSet = DM_PR_CHECK.PR_CHECK
    OnDataChange = wwdsDataChange
    MasterDataSource = wwdsSubMaster1
    Left = 256
    Top = 122
  end
end
