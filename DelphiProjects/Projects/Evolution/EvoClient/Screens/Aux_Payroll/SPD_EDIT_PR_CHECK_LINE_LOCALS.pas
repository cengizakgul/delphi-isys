// Copyright � 2000-2004 iSystems LLC. All rights reserved.

unit SPD_EDIT_PR_CHECK_LINE_LOCALS;

interface

uses
   SDataStructure, SPD_EDIT_PR_BASE, StdCtrls, wwdblook,
  DBActns, Classes, ActnList, Db, Wwdatsrc, DBCtrls, Buttons, Grids,
  Wwdbigrd, Wwdbgrid, Controls, ExtCtrls, ComCtrls, SysUtils, EvUIComponents, EvUtils, EvClientDataSet,
  ISBasicClasses, isUIwwDBLookupCombo, SDDClasses, SDataDictclient,
  ImgList, SDataDicttemp, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, LMDCustomButton, LMDButton, isUILMDButton,
  isUIFashionPanel, Forms;

type
  TEDIT_PR_CHECK_LINE_LOCALS = class(TEDIT_PR_BASE)
    wwDBGrid1: TevDBGrid;
    wwDBGrid2: TevDBGrid;
    Label2: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    evPanel1: TevPanel;
    Label1: TevLabel;
    DBText1: TevDBText;
    DBText2: TevDBText;
    Label3: TevLabel;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    wwdsSubMaster1: TevDataSource;
    wwdsSubMaster2: TevDataSource;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    evSplitter1: TevSplitter;
    evDBGrid1: TevDBGrid;
    wwdsSubMaster3: TevDataSource;
    evDBGrid2: TevDBGrid;
    evSplitter2: TevSplitter;
    EESrc: TevDataSource;
    EEStatesSrc: TevDataSource;
    drgpExempt_Exclude: TevDBRadioGroup;
    pnlLocation: TevPanel;
    evLabel2: TevLabel;
    evDBText1: TevDBText;
    evDBText2: TevDBText;
    evPanel3: TevPanel;
    evLabel3: TevLabel;
    evDBText3: TevDBText;
    evLabel4: TevLabel;
    evDBText4: TevDBText;
    butnPrior: TevBitBtn;
    butnNext: TevBitBtn;
    procedure butnPriorClick(Sender: TObject);
    procedure butnNextClick(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure PRPageControlChange(Sender: TObject);
    procedure PRNextExecute(Sender: TObject);
    procedure PRPriorExecute(Sender: TObject);
    procedure wwdsSubMaster2DataChangeBeforeChildren(Sender: TObject;
      Field: TField);
  protected
    procedure GetDataSetsToReopen(var aDS: TArrayDS; var Close: Boolean); override;
    function GetDataSetConditions(sName: string): string; override;
    procedure ReopenPRCheckWithLines; override;
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

procedure TEDIT_PR_CHECK_LINE_LOCALS.butnPriorClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster3.DataSet.Prior;
end;

procedure TEDIT_PR_CHECK_LINE_LOCALS.butnNextClick(Sender: TObject);
begin
  inherited;
  wwdsSubMaster3.DataSet.Next;
end;

procedure TEDIT_PR_CHECK_LINE_LOCALS.wwdsDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  butnPrior.Enabled := wwdsSubMaster3.DataSet.RecNo > 1;
  butnNext.Enabled := wwdsSubMaster3.DataSet.RecNo < wwdsSubMaster3.DataSet.RecordCount;
end;

function TEDIT_PR_CHECK_LINE_LOCALS.GetDataSetConditions(
  sName: string): string;
begin
  if sName = 'EE_LOCALS' then
    Result := ''
  else
    Result := inherited GetDataSetConditions(sName);
end;

procedure TEDIT_PR_CHECK_LINE_LOCALS.GetDataSetsToReopen(var aDS: TArrayDS;
  var Close: Boolean);
begin
  AddDS(DM_EMPLOYEE.EE_LOCALS, aDS);
  AddDS(DM_PAYROLL.PR_BATCH, aDS);
  inherited;
end;

function TEDIT_PR_CHECK_LINE_LOCALS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_PAYROLL.PR_CHECK_LINE_LOCALS;
end;

function TEDIT_PR_CHECK_LINE_LOCALS.GetInsertControl: TWinControl;
begin
  Result := wwDBLookupCombo1;
end;

procedure TEDIT_PR_CHECK_LINE_LOCALS.ReopenPRCheckWithLines;
begin
  inherited;
  wwdsSubMaster1.DataSet := wwdsSubMaster1.DataSet; 
  wwdsSubMaster2.DataSet := wwdsSubMaster2.DataSet; 
end;

procedure TEDIT_PR_CHECK_LINE_LOCALS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_PAYROLL.PR_BATCH, SupportDataSets);
end;

procedure TEDIT_PR_CHECK_LINE_LOCALS.PRPageControlChange(Sender: TObject);
begin
  inherited;
  Panel1.Visible := PageControl1.ActivePage = TabSheet1;
  evPanel1.Visible := not Panel1.Visible;
  ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LINE_LOCALS.PRNextExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Next;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LINE_LOCALS.PRPriorExecute(Sender: TObject);
begin
  inherited;
  wwdsSubMaster.DataSet.Prior;
  if PageControl1.ActivePage <> TabSheet1 then
    ReopenPRCheckWithLines;
end;

procedure TEDIT_PR_CHECK_LINE_LOCALS.wwdsSubMaster2DataChangeBeforeChildren(
  Sender: TObject; Field: TField);
begin
  inherited;
  if DM_EMPLOYEE.EE.Active then
    DM_EMPLOYEE.EE.Locate('EE_NBR', DM_PAYROLL.PR_CHECK['EE_NBR'], []);
end;

initialization
  RegisterClass(TEDIT_PR_CHECK_LINE_LOCALS);

end.
