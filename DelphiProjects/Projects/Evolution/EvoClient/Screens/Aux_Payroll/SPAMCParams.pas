// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPAMCParams;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls, EvBasicUtils, EvUIUtils,
  ISBasicClasses, EvUIComponents, isUIEdit, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TPAMCParams = class(TForm)
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    cbProcess: TevCheckBox;
    cbPrintReports: TevCheckBox;
    cbAllEmployees: TevCheckBox;
    editEmployees: TevEdit;
    lablEmployees: TevLabel;
    lablQuarterEndDate: TevLabel;
    lablMinimumTax: TevLabel;
    dtMonthEndDate: TevDateTimePicker;
    editMinTax: TMaskEdit;
    EvBevel1: TEvBevel;
    updwChangeMonth: TUpDown;
    procedure evBitBtn1Click(Sender: TObject);
    procedure cbAllEmployeesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure updwChangeMonthClick(Sender: TObject; Button: TUDBtnType);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
  EvUtils;
{$R *.DFM}

procedure TPAMCParams.evBitBtn1Click(Sender: TObject);
begin
  if not cbAllEmployees.Checked and (Trim(editEmployees.Text) = '') then
  begin
    ModalResult := mrNone;
    EvErrMessage('Please, enter employee codes');
  end;
  if Trim(editMinTax.Text) = '' then
  begin
    ModalResult := mrNone;
    EvErrMessage('Please, enter minimum tax');
  end;
end;

procedure TPAMCParams.cbAllEmployeesClick(Sender: TObject);
begin
  lablEmployees.Enabled := not cbAllEmployees.Checked;
  editEmployees.Enabled := not cbAllEmployees.Checked;
end;

procedure TPAMCParams.FormShow(Sender: TObject);
begin
  dtMonthEndDate.Date := GetEndMonth(Date);
end;

procedure TPAMCParams.updwChangeMonthClick(Sender: TObject;
  Button: TUDBtnType);
begin
  if Button = btNext then
    dtMonthEndDate.Date := GetEndMonth(dtMonthEndDate.Date + 1)
  else
    dtMonthEndDate.Date := GetEndMonth(GetBeginMonth(dtMonthEndDate.Date) - 1);
end;

end.
