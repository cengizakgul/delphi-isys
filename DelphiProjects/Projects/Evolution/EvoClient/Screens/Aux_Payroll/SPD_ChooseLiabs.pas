// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ChooseLiabs;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid,  Db, Wwdatsrc,
   EvUtils, SFieldCodeValues, EvConsts, SDataStructure, ISBasicClasses, kbmMemTable,
  ISKbmMemDataSet, EvDataAccessComponents, ISDataAccessComponents, EvContext, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TChooseLiabs = class(TForm)
    grdLiabs: TevDBGrid;
    cdsLiabs: TevClientDataSet;
    dsLiabs: TevDataSource;
    cdsLiabsLEVEL_: TStringField;
    cdsLiabsNBR: TIntegerField;
    cdsLiabsTYPE: TStringField;
    cdsLiabsAMOUNT: TFloatField;
    cdsLiabsSTATE: TStringField;
    cdsLiabsREF_NBR: TIntegerField;
    cdsLiabsDESCRIPTION: TStringField;
    cdsLiabsDUE_DATE: TDateTimeField;
    cdsLiabsSTATUS: TStringField;
    cdsLiabsRUN_NUMBER: TIntegerField;
    evLabel1: TevLabel;
    lAmount: TevLabel;
    OKBtn: TevBitBtn;
    CancelBtn: TevBitBtn;
    cdsLiabsCHECK_DATE: TDateField;
    procedure cdsLiabsCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure grdLiabsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  public
    CoNbr: Integer;
  end;

implementation

{$R *.DFM}

procedure TChooseLiabs.cdsLiabsCalcFields(DataSet: TDataSet);
begin
  if DataSet['Level_'] = 'F' then
    DataSet['Description'] := ReturnDescription(FedTaxLiabilityType_ComboChoices, DataSet['Tax_Type'])
  else if DataSet['Level_'] = 'S' then
    DataSet['Description'] := DataSet['State'] + ' - ' + ReturnDescription(StateTaxLiabilityType_ComboChoices, DataSet['Tax_Type'])
  else if DataSet['Level_'] = 'U' then
  begin
    DM_SYSTEM_STATE.SY_SUI.Locate('SY_SUI_NBR', DataSet['Ref_Nbr'], []);
    if (DM_SYSTEM_STATE.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER'] = GROUP_BOX_EE) or
       (DM_SYSTEM_STATE.SY_SUI['EE_ER_OR_EE_OTHER_OR_ER_OTHER'] = GROUP_BOX_EE_OTHER) then
      DataSet['Description'] := DataSet['State'] + ' - EE SUI - ' + DM_SYSTEM_STATE.SY_SUI['SUI_TAX_NAME']
    else
      DataSet['Description'] := DataSet['State'] + ' - ER SUI - ' + DM_SYSTEM_STATE.SY_SUI['SUI_TAX_NAME']
  end
  else if DataSet['Level_'] = 'L' then
  begin
    DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR', DataSet['Ref_Nbr'], []);
    DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR', DM_SYSTEM_LOCAL.SY_LOCALS['SY_STATES_NBR'], []);
    DataSet['Description'] := DM_SYSTEM_STATE.SY_STATES['State'] + ' - Local - ' + DM_SYSTEM_LOCAL.SY_LOCALS['NAME'];
  end;
end;

procedure TChooseLiabs.FormShow(Sender: TObject);
begin
  DM_SYSTEM_STATE.SY_STATES.CheckDSCondition('');
  DM_SYSTEM_STATE.SY_SUI.CheckDSCondition('');
  DM_SYSTEM_LOCAL.SY_LOCALS.CheckDSCondition('');
  with TExecDSWrapper.Create('AllPendingLiabsForCo') do
  begin
    SetParam('CoNbr', CoNbr);
    cdsLiabs.DataRequest(AsVariant);
  end;
  ctx_DataAccess.OpenDataSets([DM_SYSTEM_STATE.SY_STATES, DM_SYSTEM_STATE.SY_SUI, DM_SYSTEM_LOCAL.SY_LOCALS, cdsLiabs]);
end;

procedure TChooseLiabs.grdLiabsMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  fAmount: Currency;
  i: Integer;
  nbr: Integer;
begin
  fAmount := 0;
  nbr := cdsLiabs.RecNo;
  cdsLiabs.DisableControls;
  try
    for i := 0 to Pred(grdLiabs.SelectedList.Count) do
    begin
      cdsLiabs.GotoBookmark(grdLiabs.SelectedList[i]);
      fAmount := fAmount + cdsLiabs.FieldByName('Amount').AsFloat;
    end;
    lAmount.Caption := FormatFloat('#,##0.00', fAmount);
  finally
    cdsLiabs.RecNo := nbr;
    cdsLiabs.EnableControls;
  end;
end;

end.
