inherited EDIT_PR_BATCH: TEDIT_PR_BATCH
  Width = 823
  Height = 632
  inherited PageControl1: TevPageControl
    Width = 823
    Height = 599
    inherited TabSheet1: TTabSheet
      inherited sbBrowseOpenBase: TScrollBox
        Width = 815
        Height = 570
        inherited pnlBorder: TevPanel
          Width = 811
          Height = 566
        end
        inherited pnlFashionBrowse: TisUIFashionPanel
          Width = 811
          Height = 566
          inherited pnlFashionBody: TevPanel
            inherited Panel2: TevPanel
              Left = 203
              Width = 597
            end
            inherited pnlSubbrowse: TevPanel
              Left = 203
              Width = 597
            end
          end
          inherited Panel3: TevPanel
            Width = 763
          end
          inherited sbEDIT_OPEN_BASE_Interior: TScrollBox
            Width = 763
            Height = 459
            inherited Splitter1: TevSplitter
              Height = 455
            end
            inherited fpEDIT_OPEN_BASE_LEFT: TisUIFashionPanel
              Height = 455
              inherited wwdbgridSelectClient: TevDBGrid
                Height = 375
                IniAttributes.SectionName = 'TEDIT_PR_BATCH\wwdbgridSelectClient'
              end
            end
            inherited fpEDIT_OPEN_BASE_RIGHT: TisUIFashionPanel
              Width = 477
              Height = 455
              inherited PRGrid: TevDBGrid
                Width = 427
                Height = 375
                IniAttributes.SectionName = 'TEDIT_PR_BATCH\PRGrid'
              end
            end
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Detail'
      ImageIndex = 2
      object Label8: TevLabel
        Left = 8
        Top = 224
        Width = 112
        Height = 13
        Caption = 'Payroll Check Template'
      end
      object Label9: TevLabel
        Left = 328
        Top = 224
        Width = 56
        Height = 13
        Caption = 'Payroll Filter'
      end
      object Label10: TevLabel
        Left = 8
        Top = 264
        Width = 95
        Height = 16
        Caption = '~Period Begin Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TevLabel
        Left = 152
        Top = 264
        Width = 87
        Height = 16
        Caption = '~Period End Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TevLabel
        Left = 328
        Top = 264
        Width = 59
        Height = 16
        Caption = '~Frequency'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lblBatchDesc: TevLabel
        Left = 8
        Top = 351
        Width = 87
        Height = 13
        Caption = 'Batch Description '
      end
      object wwDBGrid3: TevDBGrid
        Left = 3
        Top = 3
        Width = 617
        Height = 214
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'PERIOD_BEGIN_DATE'#9'10'#9'Period Begin Date'#9
          'PERIOD_END_DATE'#9'10'#9'Period End Date'#9'No'
          'FREQUENCY'#9'1'#9'Frequency'#9'No'
          'PAY_SALARY'#9'1'#9'Pay Salary'#9'No'
          'PAY_STANDARD_HOURS'#9'1'#9'Pay Standard Hours'#9'No'
          'LOAD_DBDT_DEFAULTS'#9'1'#9'Load Dbdt Defaults'#9'No')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_PR_BATCH\wwDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwDBLookupCombo3: TevDBLookupCombo
        Left = 8
        Top = 240
        Width = 297
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'CO_PR_CHECK_TEMPLATES_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CO_PR_CHECK_TEMPLATES.CO_PR_CHECK_TEMPLATES
        LookupField = 'CO_PR_CHECK_TEMPLATES_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwDBLookupCombo4: TevDBLookupCombo
        Left = 328
        Top = 240
        Width = 281
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'CO_PR_FILTERS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_CO_PR_FILTERS.CO_PR_FILTERS
        LookupField = 'CO_PR_FILTERS_NBR'
        Style = csDropDownList
        TabOrder = 2
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object wwDBComboBox2: TevDBComboBox
        Left = 328
        Top = 280
        Width = 113
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'FREQUENCY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'PayFrequencies')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object DBRadioGroup4: TevDBRadioGroup
        Left = 8
        Top = 304
        Width = 121
        Height = 41
        Caption = '~Pay Salary'
        Columns = 2
        DataField = 'PAY_SALARY'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 6
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup5: TevDBRadioGroup
        Left = 152
        Top = 304
        Width = 153
        Height = 41
        Caption = '~Pay Standard Hours'
        Columns = 2
        DataField = 'PAY_STANDARD_HOURS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 7
        Values.Strings = (
          'Y'
          'N')
      end
      object DBRadioGroup6: TevDBRadioGroup
        Left = 328
        Top = 304
        Width = 153
        Height = 41
        Caption = '~Load DBDT Defaults'
        Columns = 2
        DataField = 'LOAD_DBDT_DEFAULTS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 8
        Values.Strings = (
          'Y'
          'N')
      end
      object edPERIOD_END_DATE: TevDBDateTimePicker
        Left = 152
        Top = 280
        Width = 121
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'PERIOD_END_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 4
      end
      object edPERIOD_BEGIN_DATE: TevDBDateTimePicker
        Left = 8
        Top = 280
        Width = 121
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'PERIOD_BEGIN_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 3
      end
      object evGroupBox1: TevGroupBox
        Left = 8
        Top = 404
        Width = 529
        Height = 101
        Caption = 'Batch Creation Options'
        TabOrder = 9
        object evLabel6: TevLabel
          Left = 8
          Top = 16
          Width = 71
          Height = 13
          Caption = 'Checks per EE'
        end
        object ChecksPerEEChkBox: TevSpinEdit
          Left = 8
          Top = 32
          Width = 73
          Height = 22
          MaxValue = 99
          MinValue = 1
          TabOrder = 0
          Value = 1
        end
        object PayAllSalaryHourly: TevRadioGroup
          Left = 96
          Top = 48
          Width = 201
          Height = 41
          Caption = 'Pay'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'All'
            'Salary'
            'Hourly')
          TabOrder = 1
        end
        object evCheckBox1: TevCheckBox
          Left = 280
          Top = 8
          Width = 121
          Height = 17
          Caption = 'Select EEs from List'
          TabOrder = 2
        end
        object evCheckBox2: TevCheckBox
          Left = 408
          Top = 8
          Width = 113
          Height = 17
          Caption = 'Create 945 Checks'
          TabOrder = 3
        end
        object evRadioGroup2: TevRadioGroup
          Left = 320
          Top = 48
          Width = 201
          Height = 41
          Caption = 'Create Checks'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Regular'
            'Manual'
            '3rd Party')
          TabOrder = 4
        end
        object CalcScheduledEDsChkBox: TevCheckBox
          Left = 128
          Top = 8
          Width = 145
          Height = 17
          Caption = 'Calculate Scheduled EDs'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
        object IncludeTimeOffRequests: TevCheckBox
          Left = 128
          Top = 27
          Width = 153
          Height = 17
          Caption = 'Include Time Off Requests'
          TabOrder = 6
        end
      end
      object btnRefreshEDs: TevBitBtn
        Left = 456
        Top = 272
        Width = 153
        Height = 33
        Caption = 'Refresh Scheduled EDs'
        TabOrder = 10
        OnClick = btnRefreshEDsClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD84666664
          448DDDDDD888888888DD8DD84EEEEE488848DDDD8DDDDD8DDD8D48846EEEE488
          88488DDDDDDDD8DDDD8D444006EE4848888488DFFDDDDDDDDDD84E6FF0648000
          00048DD88FDDDFFFFFFD4EE6FF088FFFFF088DDD88FDD88888FD4EEE6FF08888
          888D8DDDD88FDDDDDDDD4EEEE4FF044444448DDDDD88FD8888884444004FF04E
          EEE4888DFFD88FDDDDD8DD88F088FF06EEE4DDDD8FDD88FDDDD8D800F0008FF0
          6EE4DDFF8FFFD88FDDD844FFFFF046FF06E48D88888FDD88FDD84888F084EE6F
          F4448DDD8FDDDDD88D888488F04EEEE64884D8DD8FDDDDDDDDD8848884EEEEE4
          8DD8D8DDDDDDDDD8DDDDD84446666648DDDDDD888888888DDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object edtBatchDes: TevDBEdit
        Left = 8
        Top = 368
        Width = 296
        Height = 21
        DataField = 'BATCH_DESCRIPTION'
        DataSource = wwdsDetail
        TabOrder = 11
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
  end
  inherited Panel1: TevPanel
    Width = 823
    inherited txCheckDate: TevDBText
      Left = 512
    end
    inherited txRunNumber: TevDBText
      Left = 441
    end
    inherited pnlFavoriteReport: TevPanel
      Left = 708
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 168
    Top = 26
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_PR_BATCH.PR_BATCH
  end
  inherited wwdsList: TevDataSource
    Left = 186
    Top = 18
  end
  inherited wwdsSubMaster: TevDataSource
    OnDataChange = wwdsSubMasterDataChange
  end
end
