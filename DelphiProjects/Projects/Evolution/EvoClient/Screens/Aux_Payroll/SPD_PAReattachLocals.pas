// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PAReattachLocals;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls, Grids, Wwdbigrd, Wwdbgrid, DB,
   Wwdatsrc, EvUtils, SDataStructure, EvConsts,   DateUtils, Dialogs,
  wwdblook, EvTypes, SDDClasses, kbmMemTable, EvContext,
  ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictclient,SFieldCodeValues, EvExceptions,
  isBaseClasses, EvCommonInterfaces, EvDataset, EvStreamUtils,
  EvClientDataSet, EvUIComponents, isUIwwDBLookupCombo, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TPAReattachLocals = class(TForm)
    pcMain: TevPageControl;
    tsPayrolls: TTabSheet;
    tsStatesLocals: TTabSheet;
    evPanel2: TevPanel;
    evPanel3: TevPanel;
    dsEmployees: TevDataSource;
    dsPayrolls: TevDataSource;
    cdsPayrolls: TevClientDataSet;
    DM_EMPLOYEE: TDM_EMPLOYEE;
    evSpeedButton1: TevSpeedButton;
    grdPayroll: TevDBGrid;
    grdLocalTaxList: TevDBGrid;
    btnCreateChecks: TevSpeedButton;
    lcres_Locals: TevDBLookupCombo;
    dsLocals: TevDataSource;
    cdsLocals: TevClientDataSet;
    DM_COMPANY: TDM_COMPANY;
    cdsLocalsres_EE_LOCALS_NBR: TIntegerField;
    cdsLocalsres_Local: TStringField;
    cdsLocalsDEST_res_EE_LOCALS_NBR: TIntegerField;
    cdsLocalsDest_res_Local: TStringField;
    cdsLocalsEE_NBR: TIntegerField;
    cdsLocalsnres_EE_LOCALS_NBR: TIntegerField;
    cdsLocalsnres_Local: TStringField;
    cdsLocalsDEST_nres_EE_LOCALS_NBR: TIntegerField;
    cdsLocalsDest_nres_Local: TStringField;
    dsEE_LOCALS: TevDataSource;
    evPanel4: TevPanel;
    cbEmployee: TevDBLookupCombo;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    cbYear: TevComboBox;
    lcnres_Locals: TevDBLookupCombo;
    lcdest_res_Locals: TevDBLookupCombo;
    lcdest_nres_Locals: TevDBLookupCombo;
    btnApply: TevSpeedButton;
    btnDelete: TevSpeedButton;
    cdResEELocals: TevClientDataSet;
    cdResEELocalsEE_LOCALS_NBR: TIntegerField;
    cdResEELocalsEE_NBR: TIntegerField;
    cdResEELocalsCO_LOCAL_TAX_NBR: TIntegerField;
    cdResEELocalsLOCAL_TYPE: TStringField;
    cdResEELocalsSY_STATES_NBR: TIntegerField;
    cdResEELocalsLocal_Name_Lookup: TStringField;
    cdNresEELocals: TevClientDataSet;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField1: TStringField;
    IntegerField4: TIntegerField;
    StringField2: TStringField;
    cdDest_ResEELocals: TevClientDataSet;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    StringField3: TStringField;
    IntegerField8: TIntegerField;
    StringField4: TStringField;
    cdDest_NresEELocals: TevClientDataSet;
    IntegerField9: TIntegerField;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    StringField5: TStringField;
    IntegerField12: TIntegerField;
    StringField6: TStringField;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel7: TevLabel;
    evLabel8: TevLabel;
    procedure FormShow(Sender: TObject);
    procedure evSpeedButton1Click(Sender: TObject);
    procedure pcMainChange(Sender: TObject);
    procedure cbEmployeeChange(Sender: TObject);
    procedure btnCreateChecksClick(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure cbYearChange(Sender: TObject);
    procedure cbEmployeeBeforeDropDown(Sender: TObject);
    procedure cbEmployeeCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure btnApplyClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
  private
    bYearChange: boolean;
    function CreateInStatement(Grid: TevDBGrid; FieldName: string): string;
  public
    { Public declarations }
  end;

implementation

uses Variants;

{$R *.dfm}

procedure TPAReattachLocals.FormShow(Sender: TObject);
begin
  cbYear.Items.Add(IntToStr(CurrentYear-5));
  cbYear.Items.Add(IntToStr(CurrentYear-4));
  cbYear.Items.Add(IntToStr(CurrentYear-3));
  cbYear.Items.Add(IntToStr(CurrentYear-2));
  cbYear.Items.Add(IntToStr(CurrentYear-1));
  cbYear.Items.Add(IntToStr(CurrentYear));
  cbYear.ItemIndex := 5;
  bYearChange := True;
  pcMain.ActivePage := tsPayrolls;
  tsStatesLocals.TabVisible := False;

  DM_EMPLOYEE.EE.CheckDSConditionAndClient(ctx_DataAccess.ClientID, '');

  cdsPayrolls.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString + ' and STATUS=''' + PAYROLL_STATUS_PROCESSED +
                                      ''' and ExtractYear(CHECK_DATE)=' + IntToStr(CurrentYear));

  DM_EMPLOYEE.EE_STATES.Close;
  DM_EMPLOYEE.EE_LOCALS.Close;
//Plymouth TODO:  DM_EMPLOYEE.EE_STATES.DefaultAsOfDate := False;
  DM_EMPLOYEE.EE_STATES.AsOfDate := Trunc(DM_PAYROLL.PR.CHECK_DATE.AsDateTime);
//Plymouth TODO:  DM_EMPLOYEE.EE_LOCALS.DefaultAsOfDate := False;
  DM_EMPLOYEE.EE_LOCALS.AsOfDate := Trunc(DM_PAYROLL.PR.CHECK_DATE.AsDateTime);
  ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_LOCAL_TAX, DM_EMPLOYEE.EE, DM_EMPLOYEE.EE_STATES, DM_EMPLOYEE.EE_LOCALS, cdsPayrolls]);

  cdsLocals.CreateDataSet;

  cdResEELocals.CreateDataSet;
  cdNresEELocals.CreateDataSet;
  cdDest_ResEELocals.CreateDataSet;
  cdDest_NresEELocals.CreateDataSet;

  dsEE_LOCALS.MasterDataSource := dsEmployees;
  grdPayroll.SelectAll;
end;

procedure TPAReattachLocals.evSpeedButton1Click(Sender: TObject);
begin
  if (cbEmployee.LookupValue = '') or (grdPayroll.SelectedList.Count = 0) then
    raise EInvalidParameters.Create('Please, select employee and payrolls first');
  tsStatesLocals.TabVisible := True;
  pcMain.ActivatePage(tsStatesLocals);
end;

function TPAReattachLocals.CreateInStatement(Grid: TevDBGrid; FieldName: string): string;
var
  i: Integer;
  Nbr: TBookmarkStr;
  f: TField;
begin
  Grid.DataSource.DataSet.DisableControls;
  Nbr := Grid.DataSource.DataSet.Bookmark;
  try
    Result := '(t2.' + FieldName + ' in (0';
    f := Grid.DataSource.DataSet.FieldByName(FieldName);
    for i := 1 to Grid.SelectedList.Count do
    begin
      Grid.DataSource.DataSet.GotoBookmark(Grid.SelectedList[Pred(i)]);
      Result := Result + ',' + f.AsString;
      if i mod 1000 = 0 then
      begin
        Result := Result + ') or t2.' + FieldName + ' in (0';
      end;
    end;
    Result := Result + '))';
  finally
    Grid.DataSource.DataSet.Bookmark := Nbr;
    Grid.DataSource.DataSet.EnableControls;
  end;
end;


procedure TPAReattachLocals.pcMainChange(Sender: TObject);
  Procedure AddTotmpDataset(const cd: TevClientDataSet);
  begin
        cd.Append;
        cd['EE_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_NBR'];
        cd['EE_LOCALS_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'];
        cd['CO_LOCAL_TAX_NBR'] := DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'];
        cd['LOCAL_TYPE'] := DM_EMPLOYEE.EE_LOCALS['LOCAL_TYPE'];
        cd['SY_STATES_NBR'] := DM_EMPLOYEE.EE_LOCALS['SY_STATES_NBR'];
        cd['Local_Name_Lookup'] := DM_EMPLOYEE.EE_LOCALS['Local_Name_Lookup'];
        cd.Post;
  end;

begin
  if pcMain.ActivePage = tsStatesLocals then
  begin
     cdResEELocals.EmptyDataSet;
     cdDest_ResEELocals.EmptyDataSet;
     cdNresEELocals.EmptyDataSet;
     cdDest_NresEELocals.EmptyDataSet;

     DM_EMPLOYEE.EE_LOCALS.First;
     while not DM_EMPLOYEE.EE_LOCALS.Eof do
     begin
        if DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR',DM_EMPLOYEE.EE_LOCALS.FieldByName('CO_LOCAL_TAX_NBR').AsInteger ,[]) and
           (DM_COMPANY.CO_LOCAL_TAX.FieldByName('SY_STATES_NBR').AsInteger = 41) and
           (DM_COMPANY.CO_LOCAL_TAX.FieldByName('CombineForTaxPayments').AsString = 'Y') then
        begin
            if ( DM_EMPLOYEE.EE_LOCALS.FieldByName('LOCAL_TYPE').AsString[1] in ['R']) then
            begin
              AddTotmpDataset(cdResEELocals);
              AddTotmpDataset(cdDest_ResEELocals);
            end
            else
            if ( DM_EMPLOYEE.EE_LOCALS.FieldByName('LOCAL_TYPE').AsString = 'N') then
            begin
              AddTotmpDataset(cdNresEELocals);
              AddTotmpDataset(cdDest_NresEELocals);
            end;
        end;
        DM_EMPLOYEE.EE_LOCALS.Next;
     end;
  end
  else
    tsStatesLocals.TabVisible := False;
end;

procedure TPAReattachLocals.cbEmployeeChange(Sender: TObject);
begin
  cdsLocals.EmptyDataSet;
end;

procedure TPAReattachLocals.btnCreateChecksClick(Sender: TObject);
var
  aLocals: array of record
    EeNbr: Integer;
    OldresNbr: Integer;
    OldnresNbr: Integer;
    NewresNbr: Integer;
    NewnresNbr: Integer;
    MergeNbr : String;
  end;

  LocalsNbr: Integer;
  i: Integer;
  bp: TDataSetNotifyEvent;
  resTaxAmount, nresTaxAmount : Real;

  bTemp : boolean;

  tmpPr_Check_nbr : integer;
  s : String;
  TmpPrCheckLinesNbrForNew, TmpPrCheckLinesNbrForOld : IisListOfValues;
  bPrCheckLinesNbr : Boolean;

  ClBlobNbr, tmpPR_NBR: Integer;
  Q: IevQuery;
  PaStored: IevDataset;
  BlobData: IisStream;

begin
  if (cdsLocals.RecordCount = 0) then
    raise EInvalidParameters.Create('Please add source and destination locals.');

  SetLength(aLocals, 0);

  LocalsNbr := cdsLocals.RecNo;
  cdsLocals.DisableControls;
  cdsLocals.First;
  try
    while not cdsLocals.Eof do
    begin
      if (cdsLocals['res_EE_LOCALS_NBR'] <> cdsLocals['DEST_res_EE_LOCALS_NBR']) or 
         (cdsLocals['nres_EE_LOCALS_NBR'] <> cdsLocals['DEST_nres_EE_LOCALS_NBR']) then
      begin
        Assert(DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', cdsLocals['DEST_res_EE_LOCALS_NBR'], []));

        if DM_EMPLOYEE.EE_LOCALS.EXEMPT_EXCLUDE.Value = GROUP_BOX_EXEMPT then
          raise EInvalidOperation.Create('Unable to Move wages to ' + DM_EMPLOYEE.EE_LOCALS.FieldByName('Local_Name_Lookup').AsString + ' - EE is exempt');

        SetLength(aLocals, Succ(Length(aLocals)));
        with aLocals[High(aLocals)] do
        begin
          EeNbr := cdsLocals['EE_NBR'];
          OldresNbr := cdsLocals['res_EE_LOCALS_NBR'];
          NewresNbr := cdsLocals['DEST_res_EE_LOCALS_NBR'];
          OldnresNbr := cdsLocals['nres_EE_LOCALS_NBR'];
          NewnresNbr := cdsLocals['DEST_nres_EE_LOCALS_NBR'];
          MergeNbr := cdsLocals.fieldByName('DEST_res_EE_LOCALS_NBR').AsString + ';' +cdsLocals.fieldByName('DEST_nres_EE_LOCALS_NBR').AsString;

          Assert(DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'], []));
          Assert(DM_EMPLOYEE.EE_STATES.Locate('State_Lookup', DM_COMPANY.CO_LOCAL_TAX['localState'], []), 'There is a local ''' + DM_COMPANY.CO_LOCAL_TAX['localName'] + ''', but there is no ' + DM_COMPANY.CO_LOCAL_TAX['localState'] + ' state setup on for this employee');

          Assert(DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', NewresNbr, []));
          Assert(DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'], []));
          Assert(DM_EMPLOYEE.EE_STATES.Locate('State_Lookup', DM_COMPANY.CO_LOCAL_TAX['localState'], []), 'There is a local ''' + DM_COMPANY.CO_LOCAL_TAX['localName'] + ''', but there is no ' + DM_COMPANY.CO_LOCAL_TAX['localState'] + ' state setup on for this employee');

          Assert(DM_EMPLOYEE.EE_LOCALS.Locate('EE_LOCALS_NBR', NewnresNbr, []));
          Assert(DM_COMPANY.CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', DM_EMPLOYEE.EE_LOCALS['CO_LOCAL_TAX_NBR'], []));
          Assert(DM_EMPLOYEE.EE_STATES.Locate('State_Lookup', DM_COMPANY.CO_LOCAL_TAX['localState'], []), 'There is a local ''' + DM_COMPANY.CO_LOCAL_TAX['localName'] + ''', but there is no ' + DM_COMPANY.CO_LOCAL_TAX['localState'] + ' state setup on for this employee');

        end;
      end;
      cdsLocals.Next;
    end;
  finally
    cdsLocals.RecNo := LocalsNbr;
    cdsLocals.EnableControls;
  end;

  if (Length(aLocals) = 0) then
    raise EInvalidParameters.Create('All source locals are equal to destination locals. Nothing to do');


  bTemp := ctx_DataAccess.TempCommitDisable;
  ctx_DataAccess.SetTempCommitDisable(True);
  ctx_DataAccess.UnlockPRSimple;
  bp := DM_PAYROLL.PR_CHECK_LINES.BeforePost;
  try
    DM_PAYROLL.PR_CHECK_LINES.BeforePost := nil;
    DM_PAYROLL.PR_CHECK.Append;

    with TExecDSWrapper.Create('GenericSelectCurrentNBRWithCondition') do
    begin
      SetMacro('Columns', 'max(PAYMENT_SERIAL_NUMBER)');
      SetMacro('TableName', 'PR_CHECK');
      SetMacro('NbrField', 'PR');
      SetMacro('Condition', 'PAYMENT_SERIAL_NUMBER<100000001');
      SetParam('RecordNbr', DM_PAYROLL.PR['PR_NBR']);
      ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    end;
    ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.Open;

    DM_PAYROLL.PR_CHECK.FieldByName('PAYMENT_SERIAL_NUMBER').Value := ctx_DataAccess.CUSTOM_VIEW.Fields[0].AsInteger + 1;
    ctx_DataAccess.CUSTOM_VIEW.Close;

    DM_PAYROLL.PR_CHECK.FieldByName('PR_NBR').Value := DM_PAYROLL.PR['PR_NBR'];
    DM_PAYROLL.PR_CHECK.FieldByName('PR_BATCH_NBR').Value := DM_PAYROLL.PR_BATCH.FieldByName('PR_BATCH_NBR').Value;
    DM_PAYROLL.PR_CHECK.FieldByName('EE_NBR').Value := StrToInt(cbEmployee.LookupValue);
    DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE').Value := CHECK_TYPE2_MANUAL;
    DM_PAYROLL.PR_CHECK.FieldByName('UPDATE_BALANCE').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('OR_CHECK_FEDERAL_TYPE').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_FEDERAL').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_ADDITIONAL_FEDERAL').Value := EXCLUDE_TAXES_BOTH;;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_OASDI').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_MEDICARE').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_EMPLOYEE_EIC').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('TAX_AT_SUPPLEMENTAL_RATE').Value := 'N';
    DM_PAYROLL.PR_CHECK.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString := ctx_PayrollCalculation.GetCustomBankAccountNumber(DM_PAYROLL.PR_CHECK);
    DM_PAYROLL.PR_CHECK.FieldByName('ABA_NUMBER').AsString := ctx_PayrollCalculation.GetABANumber;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_DD').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_DD_EXCEPT_NET').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_TIME_OFF_ACCURAL').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_AUTO_DISTRIBUTION').AsString := GROUP_BOX_YES;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_SCH_E_D_FROM_AGCY_CHK').AsString := GROUP_BOX_YES;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_SCH_E_D_EXCEPT_PENSION').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('PRORATE_SCHEDULED_E_DS').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_OUTSTANDING;
    DM_PAYROLL.PR_CHECK.FieldByName('STATUS_CHANGE_DATE').AsDateTime := Now;
    DM_PAYROLL.PR_CHECK.FieldByName('EXCLUDE_FROM_AGENCY').AsString := GROUP_BOX_YES;
    DM_PAYROLL.PR_CHECK.FieldByName('ER_OASDI_TAXABLE_TIPS').Value := 0.0;
    DM_PAYROLL.PR_CHECK.FieldByName('FEDERAL_SHORTFALL').Value := 0.0;
    DM_PAYROLL.PR_CHECK.FieldByName('CHECK_TYPE_945').AsString := GROUP_BOX_NO;
    DM_PAYROLL.PR_CHECK.FieldByName('TAX_FREQUENCY').Value := DM_EMPLOYEE.EE.FieldByName('PAY_FREQUENCY').Value;

    DM_PAYROLL.PR_CHECK.FieldByName('CALCULATE_OVERRIDE_TAXES').AsString := 'N';

    DM_PAYROLL.PR_CHECK.Post;

    DM_PAYROLL.PR_CHECK_LINES.DataRequired(AlwaysFalseCond);

    DM_PAYROLL.PR_CHECK_LINE_LOCALS.DataRequired(AlwaysFalseCond);
    DM_PAYROLL.PR_CHECK_LOCALS.DataRequired('ALL');

    TmpPrCheckLinesNbrForNew := TisListOfValues.Create;
    TmpPrCheckLinesNbrForOld := TisListOfValues.Create;
    tmpPr_Check_nbr := 0;
       s := 'select sum(AMOUNT) AMOUNT, CL_E_DS_NBR, EE_SCHEDULED_E_DS_NBR,  t2.PR_CHECK_NBR, T1.PR_CHECK_LINES_NBR, t2.PR_NBR ' +
                'from pr_check T2, pr_check_lines T1, PR_CHECK_LOCALS T3 ' +
                  'where T2.ee_NBR = :RecordNbr and  ' +
                   'T1.pr_check_NBR = T2.pr_check_NBR and T2.PR_CHECK_NBR = T3.PR_CHECK_NBR AND ' +
                   '{AsOfNow<T1>} and {AsOfNow<T2>} and {AsOfNow<T3>} and ' +
                   ' #CONDITION  ' +
                   'group by cl_e_ds_nbr, EE_SCHEDULED_E_DS_NBR, t2.PR_CHECK_NBR, T1.PR_CHECK_LINES_NBR, t2.PR_NBR ' +
                   'order by t2.PR_NBR, t2.PR_CHECK_NBR ';

    tmpPR_NBR := -1;
    for i := 0 to High(aLocals) do
    begin

        with TExecDSWrapper.Create(s) do
        begin
          SetParam('RecordNbr', StrToInt(cbEmployee.LookupValue));
          SetMacro('Condition', CreateInStatement(grdPayroll, 'pr_nbr') +
                   ' and (t3.EE_LOCALS_NBR= ' + IntToStr(aLocals[i].OldresNbr) + ' or t3.EE_LOCALS_NBR= ' + IntToStr(aLocals[i].OldnresNbr) + ')' );
          ctx_DataAccess.CUSTOM_VIEW.Close;
          ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
          ctx_DataAccess.CUSTOM_VIEW.Open;
        end;

        ctx_DataAccess.CUSTOM_VIEW.First;
        while not ctx_DataAccess.CUSTOM_VIEW.Eof do
        begin
            if ctx_DataAccess.CUSTOM_VIEW['PR_NBR'] <> tmpPR_NBR then
            begin
              Q := TevQuery.Create('SELECT FILLER FROM PR WHERE {AsOfNow<PR>} AND PR_NBR=:PrNbr');
              Q.Params.AddValue('PrNbr', ctx_DataAccess.CUSTOM_VIEW['PR_NBR']);
              Q.Result.First;
              if not Q.Result.Eof then
              begin
                if Trim(ExtractFromFiller(Q.Result.FieldByName('FILLER').AsString, 50, 8)) <> '' then
                begin
                  ClBlobNbr := StrToInt(Trim(ExtractFromFiller(Q.Result.FieldByName('FILLER').AsString, 50, 8)));
                  Q := TevQuery.Create('SELECT DATA FROM CL_BLOB WHERE CL_BLOB_NBR=:ClBlobNbr');
                  Q.Params.AddValue('ClBlobNbr', ClBlobNbr);
                  Q.Result.First;
                  if not Q.Result.Eof then
                  begin
                    BlobData := TisStream.Create;
                    (Q.Result.vclDataSet.FieldByName('DATA') as TBlobField).SaveToStream(BlobData.RealStream);

                    PaStored := TevDataset.Create;
                    PaStored.Data := BlobData;
                    BlobData := nil;
                    PaStored.IndexFieldNames := 'PR_NBR;EE_LOCALS_NBR;NONRES_EE_LOCALS_NBR';
                    PaStored.CancelRange;
                    PaStored.First;

                    tmpPR_NBR := ctx_DataAccess.CUSTOM_VIEW['PR_NBR'];
                  end;
                end;
              end;
            end;

            if (tmpPR_NBR = ctx_DataAccess.CUSTOM_VIEW['PR_NBR']) then
            begin
                PaStored.SetRange([cdsPayrolls.FieldByName('PR_NBR').asInteger, aLocals[i].OldresNbr,aLocals[i].OldnresNbr],
                                  [cdsPayrolls.FieldByName('PR_NBR').asInteger, aLocals[i].OldresNbr,aLocals[i].OldnresNbr]);
                PaStored.First;
                while not PaStored.Eof do
                begin
                  if (ctx_DataAccess.CUSTOM_VIEW.FieldByName('PR_CHECK_LINES_NBR').AsInteger = PaStored.FieldByName('PR_CHECK_LINES_NBR').AsInteger) then
                  begin
                      bPrCheckLinesNbr := false;
                      if (not TmpPrCheckLinesNbrForOld.ValueExists(ctx_DataAccess.CUSTOM_VIEW.fieldByName('PR_CHECK_LINES_NBR').AsString)) then
                      begin
                          TmpPrCheckLinesNbrForOld.AddValue(ctx_DataAccess.CUSTOM_VIEW.fieldByName('PR_CHECK_LINES_NBR').AsString, ctx_DataAccess.CUSTOM_VIEW.fieldByName('PR_CHECK_LINES_NBR').AsInteger);
                          bPrCheckLinesNbr := True;
                          DM_PAYROLL.PR_CHECK_LINES.Append;
                          if not ctx_DataAccess.CUSTOM_VIEW.FieldByName('AMOUNT').IsNull then
                            DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := - ctx_DataAccess.CUSTOM_VIEW['AMOUNT'];
                          DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'] := ctx_DataAccess.CUSTOM_VIEW['CL_E_DS_NBR'];
                          DM_PAYROLL.PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := ctx_DataAccess.CUSTOM_VIEW['EE_SCHEDULED_E_DS_NBR'];
                          DM_PAYROLL.PR_CHECK_LINES['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
                          DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
                          DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
                          DM_PAYROLL.PR_CHECK_LINES.Post;
                      end;

                      if DM_PAYROLL.PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([ctx_DataAccess.CUSTOM_VIEW['PR_CHECK_NBR'], aLocals[i].OldresNbr]), []) then
                      Begin

                         resTaxAmount := DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value;

                         if DM_PAYROLL.PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'] , aLocals[i].OldresNbr]), []) then
                         Begin
                            if tmpPr_Check_nbr <> ctx_DataAccess.CUSTOM_VIEW['PR_CHECK_NBR'] then
                            begin
                              tmpPr_Check_nbr := ctx_DataAccess.CUSTOM_VIEW['PR_CHECK_NBR'];
                              DM_PAYROLL.PR_CHECK_LOCALS.Edit;
                              DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] - resTaxAmount;
                              DM_PAYROLL.PR_CHECK_LOCALS.Post;
                            end
                         end
                         else
                         begin
                            tmpPr_Check_nbr := ctx_DataAccess.CUSTOM_VIEW['PR_CHECK_NBR'];
                            DM_PAYROLL.PR_CHECK_LOCALS.Append;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').Value := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
                            DM_PAYROLL.PR_CHECK_LOCALS['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value := aLocals[i].OldresNbr;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value := 0;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value := 0;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').Value := 0;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EXCLUDE_LOCAL').Value := 'N';
                            DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := -resTaxAmount;
                            DM_PAYROLL.PR_CHECK_LOCALS.Post;
                         end;
                      end;

                      if DM_PAYROLL.PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([ctx_DataAccess.CUSTOM_VIEW['PR_CHECK_NBR'], aLocals[i].OldnresNbr]), []) then
                      Begin
                         nresTaxAmount := DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value;

                         if DM_PAYROLL.PR_CHECK_LOCALS.Locate('PR_CHECK_NBR;EE_LOCALS_NBR', VarArrayOf([DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'] , aLocals[i].OldnresNbr]), []) then
                         Begin
                            if tmpPr_Check_nbr <> ctx_DataAccess.CUSTOM_VIEW['PR_CHECK_NBR'] then
                            begin
                              tmpPr_Check_nbr := ctx_DataAccess.CUSTOM_VIEW['PR_CHECK_NBR'];
                              DM_PAYROLL.PR_CHECK_LOCALS.Edit;
                              DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] - nresTaxAmount;
                              DM_PAYROLL.PR_CHECK_LOCALS.Post;
                            end
                         end
                         else
                         begin
                            tmpPr_Check_nbr := ctx_DataAccess.CUSTOM_VIEW['PR_CHECK_NBR'];
                            DM_PAYROLL.PR_CHECK_LOCALS.Append;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('PR_CHECK_NBR').Value := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
                            DM_PAYROLL.PR_CHECK_LOCALS['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EE_LOCALS_NBR').Value := aLocals[i].OldnresNbr;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAXABLE_WAGE').Value := 0;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_TAX').Value := 0;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('LOCAL_SHORTFALL').Value := 0;
                            DM_PAYROLL.PR_CHECK_LOCALS.FieldByName('EXCLUDE_LOCAL').Value := 'N';
                            DM_PAYROLL.PR_CHECK_LOCALS['OVERRIDE_AMOUNT'] := -nresTaxAmount;
                            DM_PAYROLL.PR_CHECK_LOCALS.Post;
                         end;
                      end;

                      if bPrCheckLinesNbr then
                      begin
                        DM_EMPLOYEE.EE_LOCALS.First;
                        while not DM_EMPLOYEE.EE_LOCALS.Eof do
                        begin
                          DM_PAYROLL.PR_CHECK_LINE_LOCALS.Append;
                          DM_PAYROLL.PR_CHECK_LINE_LOCALS['PR_CHECK_LINES_NBR'] := DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
                          DM_PAYROLL.PR_CHECK_LINE_LOCALS['EE_LOCALS_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'];
                          if (DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'] = aLocals[i].OldresNbr) or (DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'] = aLocals[i].OldnresNbr) then

                            DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_INCLUDE
                          else
                            DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_EXEMPT;
                          DM_PAYROLL.PR_CHECK_LINE_LOCALS.Post;
                          DM_EMPLOYEE.EE_LOCALS.Next;
                        end;
                      end;

                      if (not TmpPrCheckLinesNbrForNew.ValueExists(ctx_DataAccess.CUSTOM_VIEW.fieldByName('PR_CHECK_LINES_NBR').AsString)) or
                             (TmpPrCheckLinesNbrForNew.ValueExists(ctx_DataAccess.CUSTOM_VIEW.fieldByName('PR_CHECK_LINES_NBR').AsString) and
                             (TmpPrCheckLinesNbrForNew.Value[ctx_DataAccess.CUSTOM_VIEW.fieldByName('PR_CHECK_LINES_NBR').AsString] <>  aLocals[i].MergeNbr)) then
                      begin
                        TmpPrCheckLinesNbrForNew.AddValue(ctx_DataAccess.CUSTOM_VIEW.fieldByName('PR_CHECK_LINES_NBR').AsString, aLocals[i].MergeNbr);

                        DM_PAYROLL.PR_CHECK_LINES.Append;
                        DM_PAYROLL.PR_CHECK_LINES['AMOUNT'] := ctx_DataAccess.CUSTOM_VIEW['AMOUNT'];
                        DM_PAYROLL.PR_CHECK_LINES['CL_E_DS_NBR'] := ctx_DataAccess.CUSTOM_VIEW['CL_E_DS_NBR'];
                        DM_PAYROLL.PR_CHECK_LINES['EE_SCHEDULED_E_DS_NBR'] := ctx_DataAccess.CUSTOM_VIEW['EE_SCHEDULED_E_DS_NBR'];
                        DM_PAYROLL.PR_CHECK_LINES['PR_NBR'] := DM_PAYROLL.PR['PR_NBR'];
                        DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_NBR'] := DM_PAYROLL.PR_CHECK['PR_CHECK_NBR'];
                        DM_PAYROLL.PR_CHECK_LINES['LINE_TYPE'] := CHECK_LINE_TYPE_USER;
                        DM_PAYROLL.PR_CHECK_LINES.Post;

                        DM_EMPLOYEE.EE_LOCALS.First;
                        while not DM_EMPLOYEE.EE_LOCALS.Eof do
                        begin
                          DM_PAYROLL.PR_CHECK_LINE_LOCALS.Append;
                          DM_PAYROLL.PR_CHECK_LINE_LOCALS['PR_CHECK_LINES_NBR'] := DM_PAYROLL.PR_CHECK_LINES['PR_CHECK_LINES_NBR'];
                          DM_PAYROLL.PR_CHECK_LINE_LOCALS['EE_LOCALS_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'];
                          if (DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'] = aLocals[i].NewresNbr) or (DM_EMPLOYEE.EE_LOCALS['EE_LOCALS_NBR'] = aLocals[i].NewnresNbr) then
                             DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_INCLUDE
                          else
                             DM_PAYROLL.PR_CHECK_LINE_LOCALS['EXEMPT_EXCLUDE'] := GROUP_BOX_EXEMPT;
                          DM_PAYROLL.PR_CHECK_LINE_LOCALS.Post;
                          DM_EMPLOYEE.EE_LOCALS.Next;
                        end;
                      end;
                  end;
                  PaStored.Next;
                end;
            end;

          ctx_DataAccess.CUSTOM_VIEW.Next;
        end;

    end;

    if DM_PAYROLL.PR_CHECK_LINES.ChangeCount > 0 then
      ctx_DataAccess.PostDataSets([DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_CHECK_LINES, DM_PAYROLL.PR_CHECK_LINE_LOCALS, DM_PAYROLL.PR_CHECK_LOCALS])
    else
      DM_PAYROLL.PR_CHECK.CancelUpdates;

  finally

    DM_PAYROLL.PR_CHECK_LINES.BeforePost := bp;
    ctx_DataAccess.SetTempCommitDisable(bTemp);
    ctx_DataAccess.LockPRSimple;
  end;
  Close;
end;

procedure TPAReattachLocals.FormHide(Sender: TObject);
begin
//Plymouth TODO:
//  DM_EMPLOYEE.EE_STATES.DefaultAsOfDate := True;
//  DM_EMPLOYEE.EE_LOCALS.DefaultAsOfDate := True;
end;

procedure TPAReattachLocals.cbYearChange(Sender: TObject);
begin
  if bYearChange then
  begin
    cdsPayrolls.CheckDSConditionAndClient(ctx_DataAccess.ClientID, 'CO_NBR=' + DM_COMPANY.CO.FieldByName('CO_NBR').AsString + ' and STATUS=''' + PAYROLL_STATUS_PROCESSED +
                                      ''' and ExtractYear(CHECK_DATE)=' + cbYear.Text);
    ctx_DataAccess.OpenDataSets(cdsPayrolls);
  end;
end;

procedure TPAReattachLocals.cbEmployeeBeforeDropDown(Sender: TObject);
begin
   dsEE_LOCALS.MasterDataSource := nil;

   DM_EMPLOYEE.EE.Filter := 'CO_NBR ='+ DM_CLIENT.CO.CO_NBR.AsString;
   DM_EMPLOYEE.EE.Filtered := true;
end;

procedure TPAReattachLocals.cbEmployeeCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
var
 bm :TBookmark;
begin
    bm :=DM_EMPLOYEE.EE.GetBookmark;
    DM_EMPLOYEE.EE.Filtered := false;
    DM_EMPLOYEE.EE.Filtered := false;
    DM_EMPLOYEE.EE.GotoBookmark(bm);
    DM_EMPLOYEE.EE.FreeBookmark(bm);

    dsEE_LOCALS.MasterDataSource := dsEmployees;
end;


procedure TPAReattachLocals.btnApplyClick(Sender: TObject);
var
  LocalsNbr: Integer;
begin
  if (Trim(lcres_Locals.Text)<> '') and  (Trim(lcnres_Locals.Text) <> '') and
     (Trim(lcdest_res_Locals.Text)<>'') and (Trim(lcdest_nres_Locals.Text)<>'') then
  begin
      LocalsNbr := cdsLocals.RecNo;
      cdsLocals.DisableControls;
      try
         if not cdsLocals.Locate('res_EE_LOCALS_NBR;nres_EE_LOCALS_NBR;DEST_res_EE_LOCALS_NBR;DEST_nres_EE_LOCALS_NBR',
             VarArrayOf([lcres_Locals.LookupTable.fieldByName('EE_LOCALS_NBR').AsVariant,lcnres_Locals.LookupTable.fieldByName('EE_LOCALS_NBR').AsVariant,
                         lcdest_res_Locals.LookupTable.fieldByName('EE_LOCALS_NBR').AsVariant, lcdest_nres_Locals.LookupTable.fieldByName('EE_LOCALS_NBR').AsVariant]),[]) then
         begin
          cdsLocals.Append;
          cdsLocals['EE_NBR'] := DM_EMPLOYEE.EE_LOCALS['EE_NBR'];

          cdsLocals['res_EE_LOCALS_NBR'] := lcres_Locals.LookupTable.fieldByName('EE_LOCALS_NBR').AsVariant;
          cdsLocals['nres_EE_LOCALS_NBR'] := lcnres_Locals.LookupTable.fieldByName('EE_LOCALS_NBR').AsVariant;

          cdsLocals['DEST_res_EE_LOCALS_NBR'] := lcdest_res_Locals.LookupTable.fieldByName('EE_LOCALS_NBR').AsVariant;
          cdsLocals['DEST_nres_EE_LOCALS_NBR'] := lcdest_nres_Locals.LookupTable.fieldByName('EE_LOCALS_NBR').AsVariant;

          cdsLocals.Post;
         end;
      finally
        cdsLocals.RecNo := LocalsNbr;
        cdsLocals.EnableControls;
      end;
  end;
end;

procedure TPAReattachLocals.btnDeleteClick(Sender: TObject);
var
  i : integer;
begin
    if grdLocalTaxList.SelectedList.Count > 0 then
    begin
      cdsLocals.DisableControls;
      try
        for i := grdLocalTaxList.SelectedList.Count-1 downto 0 do
        begin
          grdLocalTaxList.DataSource.DataSet.GotoBookmark(grdLocalTaxList.SelectedList[i]);
          cdsLocals.Delete;
        end;
      finally
        cdsLocals.EnableControls;
      end;
    end
end;

end.
