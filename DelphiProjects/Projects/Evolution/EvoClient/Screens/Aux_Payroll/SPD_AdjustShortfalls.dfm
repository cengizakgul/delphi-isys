object AdjustShortfalls: TAdjustShortfalls
  Left = 334
  Top = 185
  Width = 498
  Height = 432
  BorderIcons = []
  Caption = 'Shortfall Adjustment Form'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TevPanel
    Left = 0
    Top = 0
    Width = 482
    Height = 33
    Align = alTop
    TabOrder = 0
    object lablInfo: TevLabel
      Left = 8
      Top = 8
      Width = 174
      Height = 16
      Caption = 'All Check Info Goes Here'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel2: TevPanel
    Left = 0
    Top = 33
    Width = 482
    Height = 320
    Align = alClient
    TabOrder = 1
    object PageControl1: TevPageControl
      Left = 1
      Top = 1
      Width = 480
      Height = 318
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Deductions'
        object evLabel4: TevLabel
          Left = 0
          Top = 256
          Width = 90
          Height = 13
          Caption = 'Deduction Shortfall'
        end
        object evDBGrid1: TevDBGrid
          Left = 0
          Top = 0
          Width = 472
          Height = 249
          DisableThemesInTitle = False
          Selected.Strings = (
            'E_D_Code_Lookup'#9'9'#9'E/D Code'#9'F'
            'E_D_Description_Lookup'#9'20'#9'E/D Description'#9'F'
            'HOURS_OR_PIECES'#9'10'#9'Hours'#9'F'
            'RATE_OF_PAY'#9'10'#9'Rate'#9'F'
            'AMOUNT'#9'10'#9'Amount'#9'F'
            'DEDUCTION_SHORTFALL_CARRYOVER'#9'10'#9'Shortfall'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TAdjustShortfalls\evDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alTop
          DataSource = wwdsCheckLines
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object evDBEdit4: TevDBEdit
          Left = 0
          Top = 272
          Width = 121
          Height = 21
          DataField = 'DEDUCTION_SHORTFALL_CARRYOVER'
          DataSource = wwdsCheckLines
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Taxes'
        ImageIndex = 1
        object evLabel1: TevLabel
          Left = 0
          Top = 0
          Width = 76
          Height = 13
          Caption = 'Federal Shortfall'
        end
        object evLabel2: TevLabel
          Left = 0
          Top = 129
          Width = 66
          Height = 13
          Caption = 'State Shortfall'
        end
        object evLabel3: TevLabel
          Left = 0
          Top = 257
          Width = 67
          Height = 13
          Caption = 'Local Shortfall'
        end
        object evDBGrid2: TevDBGrid
          Left = 0
          Top = 48
          Width = 481
          Height = 81
          DisableThemesInTitle = False
          Selected.Strings = (
            'State_Lookup'#9'2'#9'State'#9'F'
            'STATE_TAXABLE_WAGES'#9'10'#9'Taxable Wages'#9'F'
            'STATE_TAX'#9'10'#9'Tax'#9'F'
            'STATE_SHORTFALL'#9'10'#9'Shortfall'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TAdjustShortfalls\evDBGrid2'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsState
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object evDBGrid3: TevDBGrid
          Left = 0
          Top = 176
          Width = 481
          Height = 81
          DisableThemesInTitle = False
          Selected.Strings = (
            'Local_Name_Lookup'#9'20'#9'Local'#9'F'
            'LOCAL_TAXABLE_WAGE'#9'10'#9'Taxable Wages'#9'F'
            'LOCAL_TAX'#9'10'#9'Tax'#9'F'
            'LOCAL_SHORTFALL'#9'10'#9'Shortfall'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TAdjustShortfalls\evDBGrid3'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwdsLocal
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object evDBEdit1: TevDBEdit
          Left = 0
          Top = 16
          Width = 121
          Height = 21
          DataField = 'FEDERAL_SHORTFALL'
          DataSource = wwdsFederal
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object evDBEdit2: TevDBEdit
          Left = 0
          Top = 144
          Width = 121
          Height = 21
          DataField = 'STATE_SHORTFALL'
          DataSource = wwdsState
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object evDBEdit3: TevDBEdit
          Left = 0
          Top = 272
          Width = 121
          Height = 21
          DataField = 'LOCAL_SHORTFALL'
          DataSource = wwdsLocal
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
    end
  end
  object Panel3: TevPanel
    Left = 0
    Top = 353
    Width = 482
    Height = 41
    Align = alBottom
    TabOrder = 2
    object bbtnAdjust: TevBitBtn
      Left = 312
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Adjust'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = bbtnAdjustClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
        DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
        DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
        DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
        DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
        DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
        2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
        A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
        AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
      NumGlyphs = 2
    end
    object bbtnCancel: TevBitBtn
      Left = 400
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
      OnClick = bbtnCancelClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
        DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
        9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
        DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
        DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
        DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
        91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
        999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
      NumGlyphs = 2
    end
  end
  object DM_PAYROLL: TDM_PAYROLL
    Left = 240
    Top = 8
  end
  object wwdsFederal: TevDataSource
    DataSet = DM_PR_CHECK.PR_CHECK
    Left = 352
    Top = 8
  end
  object wwdsState: TevDataSource
    DataSet = DM_PR_CHECK_STATES.PR_CHECK_STATES
    Left = 384
    Top = 8
  end
  object wwdsCheckLines: TevDataSource
    DataSet = DM_PR_CHECK_LINES.PR_CHECK_LINES
    Left = 312
    Top = 8
  end
  object wwdsLocal: TevDataSource
    DataSet = DM_PR_CHECK_LOCALS.PR_CHECK_LOCALS
    Left = 416
    Top = 8
  end
end
