inherited evUserSchedSetupFrm: TevUserSchedSetupFrm
  Width = 749
  Height = 510
  object fpUserTasks: TisUIFashionPanel [0]
    Left = 8
    Top = 8
    Width = 274
    Height = 581
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 0
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Tasks'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    object gTasks: TevDBGrid
      Left = 12
      Top = 35
      Width = 241
      Height = 524
      DisableThemesInTitle = False
      Selected.Strings = (
        'NAME'#9'34'#9'Subject'#9'F')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TevUserSchedSetupFrm\gTasks'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      DataSource = wwdsDetail
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = 14544093
      PaintOptions.ActiveRecordColor = clBlack
      NoFire = False
    end
  end
  object fpUserDetails: TisUIFashionPanel [1]
    Left = 292
    Top = 8
    Width = 613
    Height = 581
    BevelOuter = bvNone
    BorderWidth = 12
    Color = 14737632
    TabOrder = 1
    RoundRect = True
    ShadowDepth = 8
    ShadowSpace = 8
    ShowShadow = True
    ShadowColor = clSilver
    TitleColor = clGrayText
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWhite
    TitleFont.Height = -13
    TitleFont.Name = 'Arial'
    TitleFont.Style = [fsBold]
    Title = 'Details'
    LineWidth = 0
    LineColor = clWhite
    Theme = ttCustom
    object evPanel1: TevPanel
      Left = 8
      Top = 32
      Width = 584
      Height = 527
      BevelOuter = bvNone
      Caption = 'evPanel1'
      TabOrder = 0
      inline EventFrm: TEvUserSchedEventFrm
        Left = 0
        Top = 0
        Width = 586
        Height = 529
        AutoScroll = False
        TabOrder = 0
        inherited evPageControl1: TevPageControl
          Width = 586
          Height = 529
          ActivePage = EventFrm.TabSheet2
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_USER_NOTICE.SB_USER_NOTICE
    OnDataChange = wwdsDetailDataChange
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 106
    Top = 117
  end
end
