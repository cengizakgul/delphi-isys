unit EvUserSchedReminder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ISBasicClasses,  Mask, isBaseClasses,
  wwdbedit, Wwdotdot, Wwdbcomb, SFieldCodeValues, DateUtils, ExtCtrls, EvContext,
  ImgList, EvUserSchedEditEvent, isSchedule, isBasicUtils, EvUIComponents,
  isUIwwDBComboBox, LMDCustomButton, LMDButton, isUILMDButton;

type
  TEvUserSchedReminderFrm = class(TForm)
    lSubject: TevLabel;
    lStartTime: TevLabel;
    lvEvents: TevListView;
    btnDismissAll: TevButton;
    btnOpenItem: TevButton;
    btnDismiss: TevButton;
    cbReminder: TevDBComboBox;
    btnSnooze: TevButton;
    evLabel1: TevLabel;
    Timer: TTimer;
    imItemType: TImage;
    ImageList: TImageList;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lvEventsSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure TimerTimer(Sender: TObject);
    procedure btnDismissClick(Sender: TObject);
    procedure btnDismissAllClick(Sender: TObject);
    procedure btnSnoozeClick(Sender: TObject);
    procedure btnOpenItemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lvEventsCompare(Sender: TObject; Item1, Item2: TListItem;
                              Data: Integer; var Compare: Integer);
    procedure FormShow(Sender: TObject);
  private
    procedure UpdateCaption;
    procedure CheckIfNeedToClose;
    procedure DoOnSelectEvent;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    procedure AfterConstruction; override;
    procedure RefreshList;
  end;

implementation

{$R *.dfm}

type
  TEventItem = class(TListItem)
  private
    FEvent: IisScheduleEvent;
    FEventID: String;
    FScheduledTime: TDateTime;
    FDueNow: Boolean;
    procedure UpdateTime;
    function  GetPeriod(const AMinutes: Integer): String;
  end;


{ TEventItem }

function TEventItem.GetPeriod(const AMinutes: Integer): String;
var
  n: Integer;
begin
  if AMinutes < 60 then
  begin
    n := AMinutes;
    Result := IntToStr(n) + ' minute';
  end
  else if Round(AMinutes / 60) < 24 then
  begin
    n := Round(AMinutes / 60);
    Result := IntToStr(n) + ' hour';
  end
  else if Round(AMinutes / (24 * 60)) < 365 then
  begin
    n := Round(AMinutes / (24 * 60));
    Result := IntToStr(n) + ' day';
  end
  else
  begin
    n := Round(AMinutes / (24 * 60 * 365.25));
    Result := IntToStr(n) + ' year';
  end;

  if n <> 1 then
    Result := Result + 's';
end;

procedure TEventItem.UpdateTime;
var
  s: String;
  CurrentTime: TDateTime;
  Mins: Integer;
begin
  CurrentTime := RecodeSecond(RecodeMilliSecond(Now, 0), 0);
  Mins := MinutesBetween(CurrentTime, FScheduledTime);

  if Mins = 0 then
  begin
    s := 'Due now';
    if not FDueNow then
    begin
      FDueNow := True;
      if (ListView <> nil) and (ListView.Owner <> nil) then
      begin
        (ListView.Owner as TEvUserSchedReminderFrm).WindowState := wsNormal;
        (ListView.Owner as TEvUserSchedReminderFrm).Show;
        (ListView.Owner as TEvUserSchedReminderFrm).BringToFront;
        Selected := True;
      end;
    end;
  end

  else if FScheduledTime > CurrentTime then
  begin
    s := 'Due in ' + GetPeriod(Mins);
    FDueNow := False;
  end

  else
  begin
    s := GetPeriod(Mins) + ' overdue';
    FDueNow := False;
  end;


  if FScheduledTime <= CurrentTime then
    StateIndex := 0
  else
    StateIndex := 1;

  SubItems.Clear;
  SubItems.Add(s);
end;


{ TEvUserSchedReminderFrm }

procedure TEvUserSchedReminderFrm.AfterConstruction;
begin
  inherited;
  cbReminder.Items.Text := UserSchedReminder_ComboChoices;
  cbReminder.Items.Delete(0);
end;

procedure TEvUserSchedReminderFrm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

procedure TEvUserSchedReminderFrm.lvEventsSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  DoOnSelectEvent;
end;

procedure TEvUserSchedReminderFrm.UpdateCaption;
begin
  Caption := IntToStr(lvEvents.Items.Count) + ' Reminder';
  if lvEvents.Items.Count <> 1 then
    Caption := Caption + 's';
end;

procedure TEvUserSchedReminderFrm.TimerTimer(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lvEvents.Items.Count - 1 do
    (lvEvents.Items[i] as TEventItem).UpdateTime;

  DoOnSelectEvent;    
end;

procedure TEvUserSchedReminderFrm.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
end;

procedure TEvUserSchedReminderFrm.btnDismissClick(Sender: TObject);
begin
  Context.UserScheduler.DismissEvent((lvEvents.Selected as TEventItem).FEventID);
  RefreshList;
  CheckIfNeedToClose;
end;

procedure TEvUserSchedReminderFrm.btnDismissAllClick(Sender: TObject);
var
  i: Integer;
begin
  for i := lvEvents.Items.Count - 1 downto 0 do
    Context.UserScheduler.DismissEvent((lvEvents.Items[i] as TEventItem).FEventID);

  RefreshList;
  CheckIfNeedToClose;
end;

procedure TEvUserSchedReminderFrm.btnSnoozeClick(Sender: TObject);
var
  WakeTime: TDateTime;
begin
  WakeTime := IncMinute(Now, StrToInt(cbReminder.Value));
  Context.UserScheduler.SnoozeEvent((lvEvents.Selected as TEventItem).FEventID, WakeTime);

  RefreshList;
  CheckIfNeedToClose;
end;

procedure TEvUserSchedReminderFrm.CheckIfNeedToClose;
begin
  if lvEvents.Items.Count = 0 then
    Close;
end;

procedure TEvUserSchedReminderFrm.DoOnSelectEvent;
var
  Bmp: TBitmap;
  Event: TEventItem;
begin
  Event := lvEvents.Selected as TEventItem;

  if Assigned(Event) then
  begin
    Bmp := TBitmap.Create;
    try
      ImageList.GetBitmap(Event.StateIndex, Bmp);
      imItemType.Picture.Assign(Bmp);
    finally
      Bmp.Free;
    end;

    lSubject.Caption := Event.Caption;
    lStartTime.Caption := 'Start time: ' + FormatDateTime('dddd, mmmm d, yyyy h:nn AM/PM', Event.FScheduledTime);

    cbReminder.Enabled := True;
    btnDismissAll.Enabled := True;
    btnDismiss.Enabled := True;
    btnOpenItem.Enabled := True;
    btnSnooze.Enabled := True;
  end
  else
  begin
    imItemType.Picture.Assign(nil);
    lSubject.Caption := '';
    lStartTime.Caption := '0 items are selected';

    cbReminder.Enabled := False;
    btnDismissAll.Enabled := False;
    btnDismiss.Enabled := False;
    btnOpenItem.Enabled := False;
    btnSnooze.Enabled := False;
  end;
end;

procedure TEvUserSchedReminderFrm.btnOpenItemClick(Sender: TObject);
begin
  if lvEvents.Selected <> nil then
    ShowUserSchedEvent((lvEvents.Selected as TEventItem).FEventID);
end;

procedure TEvUserSchedReminderFrm.FormCreate(Sender: TObject);
begin
  DoOnSelectEvent;
  UpdateCaption;
end;

procedure TEvUserSchedReminderFrm.RefreshList;
var
  EventList: IisParamsCollection;
  Evt: TEventItem;
  i: Integer;
  sSelectedKey: String;
begin
  EventList := Context.UserScheduler.GetEventsInfo(Now);

  if lvEvents.Selected <> nil then
    sSelectedKey := (lvEvents.Selected as TEventItem).FEventID + ' ' +
                    DateTimeToStr((lvEvents.Selected as TEventItem).FScheduledTime)
  else
    sSelectedKey := '';

  lvEvents.Items.BeginUpdate;
  try
    lvEvents.Clear;
    for i := 0 to EventList.Count - 1 do
    begin
      Evt := TEventItem.Create(lvEvents.Items);
      lvEvents.Items.AddItem(Evt, 0);

      Evt.FEvent := IInterface(EventList[i].Value['Event']) as IisScheduleEvent;
      Evt.FEventID := EventList[i].Value['ID'];
      Evt.FScheduledTime := EventList[i].Value['ScheduledTime'];
      Evt.Caption := Evt.FEvent.Description;
      Evt.UpdateTime;

      if (lvEvents.Selected = nil) and
         ((Evt.FEventID + ' ' + DateTimeToStr(Evt.FScheduledTime)) = sSelectedKey) then
        Evt.Selected := True;
    end;
  finally
    lvEvents.Items.EndUpdate;  
  end;

  if (lvEvents.Selected = nil) and (lvEvents.Items.Count > 0)  then
    lvEvents.Items[0].Selected := True;

  UpdateCaption;
end;

procedure TEvUserSchedReminderFrm.lvEventsCompare(Sender: TObject; Item1,
  Item2: TListItem; Data: Integer; var Compare: Integer);
begin
  // Desc order
  if (Item1 as TEventItem).FScheduledTime < (Item2 as TEventItem).FScheduledTime then
    Compare := 1
  else if (Item1 as TEventItem).FScheduledTime > (Item2 as TEventItem).FScheduledTime then
    Compare := -1
  else
    Compare := 0;
end;

procedure TEvUserSchedReminderFrm.FormShow(Sender: TObject);
begin
  cbReminder.ItemIndex := 0;
end;

end.
