unit EvSched;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs,
  isSchedule;

type
  TEvSchedFrm = class(TFrame)
  private
    FOnChange: TNotifyEvent;
    procedure SetEvent(const AValue: IisScheduleEvent);
    function  GetEvent: IisScheduleEvent;
  protected
    FEventDaily: IisScheduleEventDaily;
    FEventWeekly: IisScheduleEventWeekly;
    FEventMonthly: IisScheduleEventMonthly;
    FEventYearly: IisScheduleEventYearly;
    FEventOnce: IisScheduleEventOnce;

    FEvent: IisScheduleEvent;
    FSettingControls: Boolean;
    procedure DoNotifyChange; virtual;
    procedure DoSetControls; virtual;
    procedure DoCommitChanges; virtual;
  public
    procedure SetControls;
    procedure CommitChanges;
    property  Event: IisScheduleEvent read GetEvent write SetEvent;
    property  OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

implementation

{$R *.dfm}

{ TEvSchedFrm }

function TEvSchedFrm.GetEvent: IisScheduleEvent;
begin
  if Assigned(FEvent) then
    CommitChanges;
  Result := FEvent;
end;

procedure TEvSchedFrm.DoNotifyChange;
begin
  if not FSettingControls and Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TEvSchedFrm.SetEvent(const AValue: IisScheduleEvent);
begin
  FEvent := AValue;
  SetControls;
end;

procedure TEvSchedFrm.SetControls;
begin
  FSettingControls := True;
  try
    DoSetControls;
  finally
    FSettingControls := False;
  end;
end;

procedure TEvSchedFrm.DoSetControls;
begin
end;

procedure TEvSchedFrm.DoCommitChanges;
begin
end;

procedure TEvSchedFrm.CommitChanges;
begin
  DoCommitChanges;
end;

end.
 