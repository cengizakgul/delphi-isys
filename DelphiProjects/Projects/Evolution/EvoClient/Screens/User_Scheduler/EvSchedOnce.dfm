inherited EvSchedOnceFrm: TEvSchedOnceFrm
  Width = 392
  Height = 75
  object rgOnce: TevGroupBox
    Left = 0
    Top = 0
    Width = 392
    Height = 75
    Caption = 'Run Once'
    TabOrder = 0
    object evLabel6: TevLabel
      Left = 16
      Top = 34
      Width = 48
      Height = 13
      Caption = 'Start Time'
      FocusControl = edStartTime
    end
    object edStartDate: TevDateTimePicker
      Left = 82
      Top = 31
      Width = 128
      Height = 21
      Date = 39996.000000000000000000
      Format = 'ddd M/d/yyyy'
      Time = 39996.000000000000000000
      TabOrder = 0
      OnChange = NotifyChange
    end
    object edStartTime: TevDateTimePicker
      Left = 220
      Top = 31
      Width = 90
      Height = 21
      Date = 37435.040277777780000000
      Format = 'h:mm tt'
      Time = 37435.040277777780000000
      DateMode = dmUpDown
      Kind = dtkTime
      TabOrder = 1
      OnChange = NotifyChange
    end
  end
end
