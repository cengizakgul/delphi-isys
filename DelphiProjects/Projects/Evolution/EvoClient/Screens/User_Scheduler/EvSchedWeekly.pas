unit EvSchedWeekly;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ISBasicClasses,  StdCtrls, Spin, isSchedule,
  IsTypes, EvSched, EvUIComponents;

type
  TEvSchedWeeklyFrm = class(TEvSchedFrm)
    gbWeekly: TevGroupBox;
    lWeeklyWeeks: TevLabel;
    lWeeklyEvery: TevLabel;
    evLabel3: TevLabel;
    edEvery: TevSpinEdit;
    cbMon: TevCheckBox;
    cbTue: TevCheckBox;
    cbWed: TevCheckBox;
    cbThu: TevCheckBox;
    cbFri: TevCheckBox;
    cbSat: TevCheckBox;
    cbSun: TevCheckBox;
    edStartTime: TevDateTimePicker;
    gbRange: TevGroupBox;
    evLabel9: TevLabel;
    edBeginDate: TevDateTimePicker;
    cbEndDate: TevCheckBox;
    edEndDate: TevDateTimePicker;
    procedure NotifyChange(Sender: TObject);
    procedure cbEndDateClick(Sender: TObject);
  private
    function GetEvent: IisScheduleEventWeekly;  
  protected
    procedure DoSetControls; override;
    procedure DoCommitChanges; override;
  end;

implementation

{$R *.dfm}

{ TEvSchedWeeklyFrm }

procedure TEvSchedWeeklyFrm.DoCommitChanges;
begin
  inherited;

  GetEvent.StartTime := edStartTime.Time;
  GetEvent.EveryNbrWeek := edEvery.Value;
  GetEvent.StartDate := edBeginDate.Date;

  GetEvent.RunDays := [];
  if cbMon.Checked then
    GetEvent.RunDays := GetEvent.RunDays + [dwMonday];
  if cbTue.Checked then
    GetEvent.RunDays := GetEvent.RunDays + [dwTuesday];
  if cbWed.Checked then
    GetEvent.RunDays := GetEvent.RunDays + [dwWednesday];
  if cbThu.Checked then
    GetEvent.RunDays := GetEvent.RunDays + [dwThursday];
  if cbFri.Checked then
    GetEvent.RunDays := GetEvent.RunDays + [dwFriday];
  if cbSat.Checked then
    GetEvent.RunDays := GetEvent.RunDays + [dwSaturday];
  if cbSun.Checked then
    GetEvent.RunDays := GetEvent.RunDays + [dwSunday];

  if cbEndDate.Checked then
    FEvent.EndDate := edEndDate.Date
  else
    FEvent.EndDate := 0;
end;

procedure TEvSchedWeeklyFrm.DoSetControls;
begin
  inherited;

  edStartTime.Time := GetEvent.StartTime;
  edEvery.Value := GetEvent.EveryNbrWeek;

  cbMon.Checked := dwMonday in GetEvent.RunDays;
  cbTue.Checked := dwTuesday in GetEvent.RunDays;
  cbWed.Checked := dwWednesday in GetEvent.RunDays;
  cbThu.Checked := dwThursday in GetEvent.RunDays;
  cbFri.Checked := dwFriday in GetEvent.RunDays;
  cbSat.Checked := dwSaturday in GetEvent.RunDays;
  cbSun.Checked := dwSunday in GetEvent.RunDays;

  edBeginDate.DateTime := GetEvent.StartDate;
  if FEvent.EndDate <> 0 then
    edEndDate.Date := FEvent.EndDate
  else
    edEndDate.Date := Date + 1;
  cbEndDate.Checked := FEvent.EndDate <> 0;
  cbEndDateClick(nil);
end;

function TEvSchedWeeklyFrm.GetEvent: IisScheduleEventWeekly;
begin
  Result := FEvent as IisScheduleEventWeekly;
end;

procedure TEvSchedWeeklyFrm.NotifyChange(Sender: TObject);
begin
  DoNotifyChange;
end;

procedure TEvSchedWeeklyFrm.cbEndDateClick(Sender: TObject);
begin
  inherited;
  edEndDate.Enabled:= cbEndDate.Checked;
end;

end.
