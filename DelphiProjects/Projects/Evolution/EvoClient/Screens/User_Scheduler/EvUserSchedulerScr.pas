// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvUserSchedulerScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls, Graphics, Forms,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList,
  ISBasicClasses, EvCommonInterfaces, EvMainboard, EvContext, EvConsts,
  EvUserSchedReminder, EvUIComponents, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TEvUserSchedulerFrm = class(TFramePackageTmpl, IevUserSchedulerScreens)
  private
    FReminderForm: TEvUserSchedReminderFrm;
  protected
    function  InitPackage: Boolean; override;
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;

    procedure ShowRemindersWindow(const AEventID: String);
  public
    procedure ClickButton(Kind: Integer); override;
  end;

implementation

{$R *.DFM}

var
  UserSchedulerFrm: TEvUserSchedulerFrm;

function GetUserSchedulerScreens: IevUserSchedulerScreens;
begin
  if not Assigned(UserSchedulerFrm) then
    UserSchedulerFrm := TEvUserSchedulerFrm.Create(nil);
  Result := UserSchedulerFrm;
end;



{ TEvUserSchedulerFrm }

procedure TEvUserSchedulerFrm.ClickButton(Kind: Integer);
begin
  if Kind <> NavHistory then
    inherited;
end;

function TEvUserSchedulerFrm.InitPackage: Boolean;
begin
  Result := inherited InitPackage;

  // Exceptions of rules!
  // This module is not controled by security, so its screens need to be enabled manually
 // ctx_AccountRights.SetElementState(etScreen, 'TEVUSERSCHEDSETUPFRM', stEnabled, False);

  FReminderForm := TEvUserSchedReminderFrm.Create(Self);
end;

function TEvUserSchedulerFrm.PackageBitmap: TBitmap;
begin
  Result := nil;
end;

function TEvUserSchedulerFrm.PackageCaption: string;
begin
  Result := 'User Scheduler';
end;

function TEvUserSchedulerFrm.PackageSortPosition: string;
begin
  Result := '900';
end;

procedure TEvUserSchedulerFrm.ShowRemindersWindow(const AEventID: String);
begin
  FReminderForm.RefreshList;

  if (AEventID <> '') or FReminderForm.Visible then
  begin
    FReminderForm.WindowState := wsNormal;
    FReminderForm.Show;
    FReminderForm.BringToFront;
  end;
end;

procedure TEvUserSchedulerFrm.UserPackageStructure;
begin
  AddStructure('\&Misc\User Scheduler|050', 'TEVUSERSCHEDSETUPFRM');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetUserSchedulerScreens, IevUserSchedulerScreens, 'Screens User Scheduler');

finalization
  UserSchedulerFrm.Free;

end.
