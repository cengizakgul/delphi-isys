// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvUserSchedSetup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFrameEntry, Db, Wwdatsrc,  SDataStructure, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb, evDataset,
  ExtCtrls, SPackageEntry, EvTaskViewerFrm, SFieldCodeValues, DateUtils,
  wwdbdatetimepicker, Spin, DBCtrls, SSecurityInterface, isVCLBugFix,
  EvUtils, Buttons, FSchedFrame, SDDClasses, EvContext, EvTypes, EvConsts,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictbureau, EvLegacy,
  EvCommonInterfaces, EvMainboard, isBaseClasses, EvStreamUtils, isBasicUtils,
  isSchedule, EvUserSchedEvent, EvSched, EvUIComponents, EvClientDataSet,
  isUIFashionPanel;

type
  TevUserSchedSetupFrm = class(TFrameEntry)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    fpUserTasks: TisUIFashionPanel;
    gTasks: TevDBGrid;
    fpUserDetails: TisUIFashionPanel;
    evPanel1: TevPanel;
    EventFrm: TEvUserSchedEventFrm;
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
  private
    PostControl: Boolean;
    procedure OnEventChange(Sender: TObject);
  protected
    function  GetDataSetConditions(sName: string): string; override;
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure OnActivateParams(const AContext: Integer; const AParams: array of Variant); override;
  public
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure AfterClick(Kind: Integer); override;
  end;

implementation

{$R *.DFM}

{ TevUserSchedSetupFrm }

procedure TevUserSchedSetupFrm.Activate;
begin
  inherited;
  EventFrm.evPageControl1.ActivePage:= EventFrm.TabSheet1;
  EventFrm.OnChange := OnEventChange;
  PostControl:= false;  
end;

procedure TevUserSchedSetupFrm.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind = 12 then
  begin
    Context.UserScheduler.RefreshTaskList;
    PostControl:= False;
  end;
end;

procedure TevUserSchedSetupFrm.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  S: IisStream;
begin
  if Kind = NavInsert then
  begin
   EventFrm.evPageControl1.ActivePage:= EventFrm.TabSheet1;
   EventFrm.edSubject.SetFocus;
  end;

  if Kind = NavOK then
  begin
    PostControl:= True;
    DM_SERVICE_BUREAU.SB_USER_NOTICE.SB_USER_NBR.AsInteger := Context.UserAccount.InternalNbr;

    DM_SERVICE_BUREAU.SB_USER_NOTICE.NAME.AsString := EventFrm.Subject;
    DM_SERVICE_BUREAU.SB_USER_NOTICE.NOTES.AsString := EventFrm.Notes;

    DM_SERVICE_BUREAU.SB_USER_NOTICE.LAST_DISMISS.Clear;
    DM_SERVICE_BUREAU.SB_USER_NOTICE.NEXT_REMINDER.Clear;

    S := (EventFrm.Event as IisInterfacedObject).AsStream;
    S.Position := 0;
    DM_SERVICE_BUREAU.SB_USER_NOTICE.UpdateBlobData('TASK',S);

  end;
  inherited;
end;

function TevUserSchedSetupFrm.GetDataSetConditions(sName: string): string;
begin
  if sName = 'SB_USER_NOTICE' then
    Result := 'SB_USER_NBR = ' + IntToStr(Context.UserAccount.InternalNbr)
  else
    Result := inherited GetDataSetConditions(sName);
end;

function TevUserSchedSetupFrm.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_USER_NOTICE;
end;

procedure TevUserSchedSetupFrm.OnActivateParams(const AContext: Integer; const AParams: array of Variant);
begin
  inherited;

  if (Length(AParams) = 2) and AnsiSameText(AParams[0], 'NavigateTo') then
    DM_SERVICE_BUREAU.SB_USER_NOTICE.Locate('SB_USER_NOTICE_NBR', AParams[1], []);
end;

procedure TevUserSchedSetupFrm.OnEventChange(Sender: TObject);
begin
  if DM_SERVICE_BUREAU.SB_USER_NOTICE.State = dsBrowse then
    DM_SERVICE_BUREAU.SB_USER_NOTICE.Edit;

  DM_SERVICE_BUREAU.SB_USER_NOTICE.NAME.AsString := EventFrm.Subject;
end;

procedure TevUserSchedSetupFrm.wwdsDetailDataChange(Sender: TObject; Field: TField);
var
  S: IisStream;
  Evt: IisScheduleEvent;

  function CreateDefaultEvent: IisScheduleEvent;
  begin
    Result := TisScheduleEventDaily.Create;
    Result.StartTime := Now;
    Result.StartDate := Date;
    Result.TaskID := UST_MESSAGE;
    Result.TaskParams.AddValue('Reminder', 15);
  end;

begin
  inherited;
  if (DM_SERVICE_BUREAU.SB_USER_NOTICE.State in [dsBrowse, dsInsert]) and
      not Assigned(Field) and
      not PostControl then
  begin
    EventFrm.Subject := DM_SERVICE_BUREAU.SB_USER_NOTICE.NAME.AsString;

    if not DM_SERVICE_BUREAU.SB_USER_NOTICE.IsBlobLoaded('NOTES') then
       DM_SERVICE_BUREAU.SB_USER_NOTICE.GetBlobData('NOTES');
    EventFrm.Notes := DM_SERVICE_BUREAU.SB_USER_NOTICE.NOTES.AsString;

    EventFrm.CreateEvents;
    if DM_SERVICE_BUREAU.SB_USER_NOTICE.TASK.IsNull then
      EventFrm.Event := CreateDefaultEvent
    else
    begin
      S := TisStream.Create;
      S := DM_SERVICE_BUREAU.SB_USER_NOTICE.GetBlobData('TASK');
      S.Position := 0;
      try
        Evt := ObjectFactory.CreateInstanceFromStream(S) as IisScheduleEvent;
      except
        Evt := CreateDefaultEvent;
      end;

      Evt.LastRunDate := DM_SERVICE_BUREAU.SB_USER_NOTICE.LAST_DISMISS.AsDateTime;
      EventFrm.Event := Evt;
    end;
  end;
end;


initialization
  RegisterClass(TevUserSchedSetupFrm);

end.
