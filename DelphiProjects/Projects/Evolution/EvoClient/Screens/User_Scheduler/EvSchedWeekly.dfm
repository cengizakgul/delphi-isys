inherited EvSchedWeeklyFrm: TEvSchedWeeklyFrm
  Width = 392
  Height = 226
  object gbWeekly: TevGroupBox
    Left = 0
    Top = 0
    Width = 392
    Height = 133
    Caption = 'Weekly Recurrence'
    TabOrder = 0
    object lWeeklyWeeks: TevLabel
      Left = 285
      Top = 34
      Width = 55
      Height = 13
      Caption = 'week(s) on:'
    end
    object lWeeklyEvery: TevLabel
      Left = 192
      Top = 34
      Width = 27
      Height = 13
      Caption = 'Every'
    end
    object evLabel3: TevLabel
      Left = 16
      Top = 34
      Width = 48
      Height = 13
      Caption = 'Start Time'
      FocusControl = edStartTime
    end
    object edEvery: TevSpinEdit
      Left = 227
      Top = 31
      Width = 52
      Height = 22
      MaxValue = 9999
      MinValue = 1
      TabOrder = 0
      Value = 1
      OnChange = NotifyChange
    end
    object cbMon: TevCheckBox
      Left = 36
      Top = 73
      Width = 49
      Height = 17
      Caption = 'Mon'
      TabOrder = 1
      OnClick = NotifyChange
    end
    object cbTue: TevCheckBox
      Left = 98
      Top = 73
      Width = 49
      Height = 17
      Caption = 'Tue'
      TabOrder = 2
      OnClick = NotifyChange
    end
    object cbWed: TevCheckBox
      Left = 161
      Top = 73
      Width = 49
      Height = 17
      Caption = 'Wed'
      TabOrder = 3
      OnClick = NotifyChange
    end
    object cbThu: TevCheckBox
      Left = 224
      Top = 73
      Width = 49
      Height = 17
      Caption = 'Thu'
      TabOrder = 4
      OnClick = NotifyChange
    end
    object cbFri: TevCheckBox
      Left = 287
      Top = 73
      Width = 49
      Height = 17
      Caption = 'Fri'
      TabOrder = 5
      OnClick = NotifyChange
    end
    object cbSat: TevCheckBox
      Left = 131
      Top = 102
      Width = 49
      Height = 17
      Caption = 'Sat'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = NotifyChange
    end
    object cbSun: TevCheckBox
      Left = 194
      Top = 102
      Width = 49
      Height = 17
      Caption = 'Sun'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = NotifyChange
    end
    object edStartTime: TevDateTimePicker
      Left = 82
      Top = 31
      Width = 90
      Height = 21
      Date = 37435.040277777780000000
      Format = 'h:mm tt'
      Time = 37435.040277777780000000
      DateMode = dmUpDown
      Kind = dtkTime
      TabOrder = 8
      OnChange = NotifyChange
    end
  end
  object gbRange: TevGroupBox
    Left = 0
    Top = 150
    Width = 392
    Height = 76
    Caption = 'Range of Recurrence'
    TabOrder = 1
    object evLabel9: TevLabel
      Left = 11
      Top = 18
      Width = 53
      Height = 13
      Caption = 'Begin Date'
    end
    object edBeginDate: TevDateTimePicker
      Left = 11
      Top = 39
      Width = 122
      Height = 21
      Date = 39996.000000000000000000
      Format = 'ddd M/d/yyyy'
      Time = 39996.000000000000000000
      TabOrder = 0
      OnChange = NotifyChange
    end
    object cbEndDate: TevCheckBox
      Left = 231
      Top = 14
      Width = 97
      Height = 17
      Caption = 'End Date:'
      TabOrder = 1
      OnClick = cbEndDateClick
    end
    object edEndDate: TevDateTimePicker
      Left = 231
      Top = 39
      Width = 121
      Height = 21
      Date = 42102.000000000000000000
      Format = 'ddd M/d/yyyy'
      Time = 42102.000000000000000000
      Enabled = False
      TabOrder = 2
      OnChange = NotifyChange
    end
  end
end
