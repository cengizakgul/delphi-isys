unit EvSchedYearly;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ISBasicClasses, isSchedule, EvSched,  ComCtrls,
  isTypes, Spin, EvUIComponents;

type
  TEvSchedYearlyFrm = class(TEvSchedFrm)
    gbRange: TevGroupBox;
    evLabel9: TevLabel;
    edBeginDate: TevDateTimePicker;
    gbYearly: TevGroupBox;
    evLabel5: TevLabel;
    rbDay: TevRadioButton;
    rbThe: TevRadioButton;
    edDay: TevComboBox;
    cbThe: TevComboBox;
    cbDayOfWeek: TevComboBox;
    edStartTime: TevDateTimePicker;
    cbMonth1: TevComboBox;
    cbMonth2: TevComboBox;
    lEvery: TevLabel;
    edEvery: TevSpinEdit;
    lDailyDays: TevLabel;
    cbEndDate: TevCheckBox;
    edEndDate: TevDateTimePicker;
    procedure NotifyChange(Sender: TObject);
    procedure rbDayClick(Sender: TObject);
    procedure cbEndDateClick(Sender: TObject);
  private
    function GetEvent: IisScheduleEventYearly;
  protected
    procedure DoSetControls; override;
    procedure DoCommitChanges; override;
  end;

implementation

{$R *.dfm}

{ TEvSchedYearlyFrm }

procedure TEvSchedYearlyFrm.DoCommitChanges;
begin
  inherited;

  GetEvent.StartTime := edStartTime.Time;
  GetEvent.StartDate := edBeginDate.Date;
  GetEvent.EveryNbrYear := edEvery.Value;

  if cbEndDate.Checked then
    FEvent.EndDate := edEndDate.Date
  else
    FEvent.EndDate := 0;

  GetEvent.DayOfMonth := TDayNumber(edDay.ItemIndex + 1);

  if rbThe.Checked then
  begin
    GetEvent.WeekOfMonth := TWeekOfMonth(cbThe.ItemIndex + 1);
    GetEvent.DayOfWeek := TDayOfWeek(cbDayOfWeek.ItemIndex + 1);
    GetEvent.Month := TMonth(cbMonth2.ItemIndex + 1);
  end
  else
  begin
    GetEvent.WeekOfMonth := wmUnknown;
    GetEvent.DayOfWeek := dwUnknown;
    GetEvent.Month := TMonth(cbMonth1.ItemIndex + 1);
  end;
end;

procedure TEvSchedYearlyFrm.DoSetControls;
begin
  inherited;

  edStartTime.Time := GetEvent.StartTime;
  edEvery.Value := GetEvent.EveryNbrYear;

  rbDay.Checked := GetEvent.DayOfMonth > 0;
  rbThe.Checked := not rbDay.Checked;
  edDay.ItemIndex := GetEvent.DayOfMonth - 1;
  cbThe.ItemIndex := Ord(GetEvent.WeekOfMonth) - 1;
  cbDayOfWeek.ItemIndex := Ord(GetEvent.DayOfWeek) - 1;


  edDay.Enabled := rbDay.Checked;
  cbMonth1.Enabled := rbDay.Checked;

  cbThe.Enabled := rbThe.Checked;
  cbDayOfWeek.Enabled := rbThe.Checked;
  cbMonth2.Enabled := rbThe.Checked;


  if rbDay.Checked then
    cbMonth1.ItemIndex := Ord(GetEvent.Month) - 1
  else
    cbMonth2.ItemIndex := Ord(GetEvent.Month) - 1;

  edBeginDate.DateTime := GetEvent.StartDate;
  if FEvent.EndDate <> 0 then
    edEndDate.Date := GetEvent.EndDate
  else
    edEndDate.Date := Date + 1;
  cbEndDate.Checked := GetEvent.EndDate <> 0;
  cbEndDateClick(nil);
end;

function TEvSchedYearlyFrm.GetEvent: IisScheduleEventYearly;
begin
  Result := FEvent as IisScheduleEventYearly;
end;

procedure TEvSchedYearlyFrm.NotifyChange(Sender: TObject);
begin
  DoNotifyChange;
end;

procedure TEvSchedYearlyFrm.rbDayClick(Sender: TObject);
begin
  edDay.Enabled := rbDay.Checked;
  cbMonth1.Enabled := rbDay.Checked;

  cbThe.Enabled := rbThe.Checked;
  cbDayOfWeek.Enabled := rbThe.Checked;
  cbMonth2.Enabled := rbThe.Checked;

  if rbDay.Checked then
  begin
    cbThe.ItemIndex := -1;
    cbDayOfWeek.ItemIndex := -1;
    cbMonth2.ItemIndex := -1;
    edDay.ItemIndex := 0;
    cbMonth1.ItemIndex := 0;
  end
  else
  begin
    edDay.ItemIndex := -1;
    cbMonth1.ItemIndex := -1;
    cbThe.ItemIndex := 0;
    cbDayOfWeek.ItemIndex := 0;
    cbMonth2.ItemIndex := 0;
  end;

  DoNotifyChange;
end;

procedure TEvSchedYearlyFrm.cbEndDateClick(Sender: TObject);
begin
  inherited;
  edEndDate.Enabled:= cbEndDate.Checked;
end;

end.
