inherited EvSchedDailyFrm: TEvSchedDailyFrm
  Width = 392
  Height = 166
  object gDaily: TevGroupBox
    Left = 0
    Top = 0
    Width = 392
    Height = 76
    Caption = 'Daily Recurrence'
    TabOrder = 0
    object lEvery: TevLabel
      Left = 208
      Top = 34
      Width = 27
      Height = 13
      Caption = 'Every'
    end
    object lDailyDays: TevLabel
      Left = 317
      Top = 34
      Width = 28
      Height = 13
      Caption = 'day(s)'
    end
    object lStartTime: TevLabel
      Left = 12
      Top = 34
      Width = 48
      Height = 13
      Caption = 'Start Time'
      FocusControl = edStartTime
    end
    object edEvery: TevSpinEdit
      Left = 259
      Top = 31
      Width = 54
      Height = 22
      MaxValue = 9999
      MinValue = 1
      TabOrder = 0
      Value = 1
      OnChange = NotifyChange
    end
    object edStartTime: TevDateTimePicker
      Left = 71
      Top = 31
      Width = 95
      Height = 22
      Date = 41273.040277777780000000
      Format = 'h:mm tt'
      Time = 41273.040277777780000000
      DateMode = dmUpDown
      Kind = dtkTime
      TabOrder = 1
      OnChange = NotifyChange
    end
  end
  object gbRange: TevGroupBox
    Left = 0
    Top = 90
    Width = 392
    Height = 76
    Caption = 'Range of Recurrence'
    TabOrder = 1
    object evLabel9: TevLabel
      Left = 12
      Top = 18
      Width = 53
      Height = 13
      Caption = 'Begin Date'
    end
    object edBeginDate: TevDateTimePicker
      Left = 12
      Top = 39
      Width = 125
      Height = 21
      Date = 39996.000000000000000000
      Format = 'ddd M/d/yyyy'
      Time = 39996.000000000000000000
      TabOrder = 0
      OnChange = NotifyChange
    end
    object edEndDate: TevDateTimePicker
      Left = 231
      Top = 39
      Width = 121
      Height = 21
      Date = 42102.000000000000000000
      Format = 'ddd M/d/yyyy'
      Time = 42102.000000000000000000
      Enabled = False
      TabOrder = 1
      OnChange = NotifyChange
    end
    object cbEndDate: TevCheckBox
      Left = 231
      Top = 14
      Width = 97
      Height = 17
      Caption = 'End Date:'
      TabOrder = 2
      OnClick = cbEndDateClick
    end
  end
end
