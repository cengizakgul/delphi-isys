unit EvSchedOnce;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ComCtrls, ISBasicClasses,  StdCtrls, isSchedule,
  EvSched, EvUIComponents;

type
  TEvSchedOnceFrm = class(TEvSchedFrm)
    rgOnce: TevGroupBox;
    evLabel6: TevLabel;
    edStartDate: TevDateTimePicker;
    edStartTime: TevDateTimePicker;
    procedure NotifyChange(Sender: TObject);
  protected
    procedure DoSetControls; override;
    procedure DoCommitChanges; override;
  end;

implementation

{$R *.dfm}

{ TEvSchedOnceFrm }

procedure TEvSchedOnceFrm.DoCommitChanges;
begin
  inherited;

  FEvent.StartDate := edStartDate.Date;
  FEvent.StartTime := edStartTime.Time;
end;

procedure TEvSchedOnceFrm.DoSetControls;
begin
  inherited;
  
  edStartDate.Date := FEvent.StartDate;
  edStartTime.Time := FEvent.StartTime;
end;

procedure TEvSchedOnceFrm.NotifyChange(Sender: TObject);
begin
  DoNotifyChange;
end;

end.
