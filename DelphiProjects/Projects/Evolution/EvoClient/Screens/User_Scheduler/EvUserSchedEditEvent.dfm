object evUserSchedEditEventFrm: TevUserSchedEditEventFrm
  Left = 481
  Top = 240
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'User Scheduled Task'
  ClientHeight = 523
  ClientWidth = 452
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inline EventFrm: TEvUserSchedEventFrm
    Left = 7
    Top = 6
    Width = 438
    Height = 473
    AutoScroll = False
    TabOrder = 0
    inherited evPageControl1: TevPageControl
      Width = 438
      Height = 473
      inherited TabSheet2: TTabSheet
        inherited pcRecurrence: TevPageControl
          inherited tsRecurrWeekly: TTabSheet
            inherited RecurrWeekly: TEvSchedWeeklyFrm
              inherited gbRange: TevGroupBox
                inherited edEndDate: TevDateTimePicker
                  Checked = False
                end
              end
            end
          end
        end
      end
    end
  end
  object btnOK: TevButton
    Left = 235
    Top = 489
    Width = 96
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    Color = clBlack
    Margin = 0
  end
  object btnCancel: TevButton
    Left = 349
    Top = 489
    Width = 96
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    Color = clBlack
    Margin = 0
  end
end
