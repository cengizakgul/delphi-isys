// Screens of "User Scheduler"
unit scr_User_Scheduler;

interface

uses
  EvUserSchedulerScr,
  EvUserSchedSetup,
  EvSched,
  EvUserSchedEvent,
  EvSchedDaily,
  EvSchedWeekly,
  EvSchedMonthly,
  EvSchedYearly,
  EvSchedOnce,
  EvUserSchedReminder,
  EvUserSchedEditEvent;

implementation

end.
