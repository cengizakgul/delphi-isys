inherited EvSchedMonthlyFrm: TEvSchedMonthlyFrm
  Width = 392
  Height = 328
  object gbMonthly: TevGroupBox
    Left = 0
    Top = 0
    Width = 391
    Height = 232
    Caption = 'Monthly Recurrence'
    TabOrder = 0
    object lMonthlyMonths2: TevLabel
      Left = 159
      Top = 124
      Width = 73
      Height = 13
      Caption = 'of the month(s):'
    end
    object evLabel5: TevLabel
      Left = 16
      Top = 35
      Width = 48
      Height = 13
      Caption = 'Start Time'
      FocusControl = edStartTime
    end
    object rbMonthlyDay: TevRadioButton
      Left = 199
      Top = 34
      Width = 43
      Height = 17
      Caption = 'Day'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbMonthlyDayClick
    end
    object rbMonthlyThe: TevRadioButton
      Left = 199
      Top = 71
      Width = 46
      Height = 17
      Caption = 'The'
      TabOrder = 1
      OnClick = rbMonthlyDayClick
    end
    object edMonthly: TevComboBox
      Left = 252
      Top = 31
      Width = 69
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      TabOrder = 2
      OnChange = NotifyChange
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31'
        'Last')
    end
    object cbMonthlyFirst: TevComboBox
      Left = 252
      Top = 68
      Width = 70
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      OnChange = NotifyChange
      Items.Strings = (
        'first'
        'second'
        'third'
        'fourth'
        'last')
    end
    object cbDayOfWeek: TevComboBox
      Left = 252
      Top = 96
      Width = 103
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 4
      OnChange = NotifyChange
      Items.Strings = (
        'Monday'
        'Tuesday'
        'Wednesday'
        'Thursday'
        'Friday'
        'Saturday'
        'Sunday')
    end
    object cbApr: TevCheckBox
      Left = 111
      Top = 152
      Width = 67
      Height = 17
      Caption = 'April'
      TabOrder = 8
      OnClick = NotifyChange
    end
    object cbMar: TevCheckBox
      Left = 16
      Top = 194
      Width = 67
      Height = 17
      Caption = 'March'
      TabOrder = 7
      OnClick = NotifyChange
    end
    object cbFeb: TevCheckBox
      Left = 16
      Top = 173
      Width = 67
      Height = 17
      Caption = 'February'
      TabOrder = 6
      OnClick = NotifyChange
    end
    object cbJan: TevCheckBox
      Left = 16
      Top = 152
      Width = 67
      Height = 17
      Caption = 'January'
      TabOrder = 5
      OnClick = NotifyChange
    end
    object cbMay: TevCheckBox
      Left = 111
      Top = 173
      Width = 67
      Height = 17
      Caption = 'May'
      TabOrder = 9
      OnClick = NotifyChange
    end
    object cbJun: TevCheckBox
      Left = 111
      Top = 194
      Width = 67
      Height = 17
      Caption = 'June'
      TabOrder = 10
      OnClick = NotifyChange
    end
    object cbJul: TevCheckBox
      Left = 206
      Top = 152
      Width = 67
      Height = 17
      Caption = 'July'
      TabOrder = 11
      OnClick = NotifyChange
    end
    object cbAug: TevCheckBox
      Left = 206
      Top = 173
      Width = 67
      Height = 17
      Caption = 'August'
      TabOrder = 12
      OnClick = NotifyChange
    end
    object cbSep: TevCheckBox
      Left = 206
      Top = 194
      Width = 76
      Height = 17
      Caption = 'September'
      TabOrder = 13
      OnClick = NotifyChange
    end
    object cbOct: TevCheckBox
      Left = 302
      Top = 152
      Width = 67
      Height = 17
      Caption = 'October'
      TabOrder = 14
      OnClick = NotifyChange
    end
    object cbNov: TevCheckBox
      Left = 302
      Top = 173
      Width = 67
      Height = 17
      Caption = 'November'
      TabOrder = 15
      OnClick = NotifyChange
    end
    object cbDec: TevCheckBox
      Left = 302
      Top = 194
      Width = 79
      Height = 17
      Caption = 'December'
      TabOrder = 16
      OnClick = NotifyChange
    end
    object edStartTime: TevDateTimePicker
      Left = 82
      Top = 31
      Width = 90
      Height = 21
      Date = 37435.040277777780000000
      Format = 'h:mm tt'
      Time = 37435.040277777780000000
      DateMode = dmUpDown
      Kind = dtkTime
      TabOrder = 17
      OnChange = NotifyChange
    end
  end
  object gbRange: TevGroupBox
    Left = 0
    Top = 252
    Width = 392
    Height = 76
    Caption = 'Range of Recurrence'
    TabOrder = 1
    object evLabel9: TevLabel
      Left = 11
      Top = 18
      Width = 53
      Height = 13
      Caption = 'Begin Date'
    end
    object edBeginDate: TevDateTimePicker
      Left = 11
      Top = 39
      Width = 122
      Height = 21
      Date = 39996.000000000000000000
      Format = 'ddd M/d/yyyy'
      Time = 39996.000000000000000000
      TabOrder = 0
      OnChange = NotifyChange
    end
    object cbEndDate: TevCheckBox
      Left = 231
      Top = 14
      Width = 97
      Height = 17
      Caption = 'End Date:'
      TabOrder = 1
      OnClick = cbEndDateClick
    end
    object edEndDate: TevDateTimePicker
      Left = 231
      Top = 39
      Width = 121
      Height = 21
      Date = 42102.000000000000000000
      Format = 'ddd M/d/yyyy'
      Time = 42102.000000000000000000
      Enabled = False
      TabOrder = 2
      OnChange = NotifyChange
    end
  end
end
