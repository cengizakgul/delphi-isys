inherited EvUserSchedEventFrm: TEvUserSchedEventFrm
  Width = 782
  Height = 676
  AutoSize = False
  object evPageControl1: TevPageControl
    Left = 0
    Top = 0
    Width = 782
    Height = 676
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnChange = evPageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Event'
      object lSchedInfo: TevLabel
        Left = 12
        Top = 74
        Width = 66
        Height = 13
        Caption = 'Schedule Info'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lRuns: TevLabel
        Left = 12
        Top = 92
        Width = 41
        Height = 13
        Caption = 'Run Info'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel2: TevLabel
        Left = 12
        Top = 152
        Width = 34
        Height = 13
        Caption = 'Notes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evLabel13: TevLabel
        Left = 12
        Top = 118
        Width = 45
        Height = 13
        Caption = 'Reminder'
      end
      object evLabel1: TevLabel
        Left = 12
        Top = 38
        Width = 44
        Height = 13
        Caption = 'Subject'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cbReminder: TevDBComboBox
        Left = 72
        Top = 115
        Width = 122
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
        OnChange = NotifyChange
      end
      object edSubject: TevEdit
        Left = 72
        Top = 35
        Width = 338
        Height = 21
        TabOrder = 1
        OnChange = NotifyChange
      end
      object edNotes: TevMemo
        Left = 12
        Top = 171
        Width = 392
        Height = 264
        ScrollBars = ssVertical
        TabOrder = 2
        OnChange = NotifyChange
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Schedule'
      ImageIndex = 2
      object lblSchedule: TevLabel
        Left = 4
        Top = 16
        Width = 54
        Height = 13
        Caption = 'Schedule'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object chbActive: TevCheckBox
        Left = 356
        Top = 5
        Width = 55
        Height = 17
        Caption = 'Active'
        TabOrder = 0
        OnClick = NotifyChange
      end
      object pcRecurrence: TevPageControl
        Left = 4
        Top = 40
        Width = 412
        Height = 377
        ActivePage = tsRecurrDaily
        TabOrder = 1
        OnChange = pcRecurrenceChange
        OnChanging = pcRecurrenceChanging
        object tsRecurrDaily: TTabSheet
          Caption = 'Daily'
          ImageIndex = 1
          inline RecurrDaily: TEvSchedDailyFrm
            Left = 0
            Top = 0
            Width = 392
            Height = 166
            AutoScroll = False
            AutoSize = True
            TabOrder = 0
            inherited gbRange: TevGroupBox
              inherited edEndDate: TevDateTimePicker
                OnChange = nil
              end
            end
          end
        end
        object tsRecurrWeekly: TTabSheet
          Caption = 'Weekly'
          ImageIndex = 4
          inline RecurrWeekly: TEvSchedWeeklyFrm
            Left = 0
            Top = 0
            Width = 392
            Height = 226
            AutoScroll = False
            AutoSize = True
            TabOrder = 0
          end
        end
        object tsRecurrMonthly: TTabSheet
          Caption = 'Monthly'
          ImageIndex = 2
          inline RecurrMonthly: TEvSchedMonthlyFrm
            Left = 0
            Top = 0
            Width = 392
            Height = 328
            AutoScroll = False
            AutoSize = True
            TabOrder = 0
          end
        end
        object tsRecurrYearly: TTabSheet
          Caption = 'Yearly'
          ImageIndex = 3
          inline RecurrYearly: TEvSchedYearlyFrm
            Left = 0
            Top = 0
            Width = 392
            Height = 240
            AutoScroll = False
            AutoSize = True
            TabOrder = 0
          end
        end
        object tsRecurrOnce: TTabSheet
          Caption = 'Once'
          inline RecurrOnce: TEvSchedOnceFrm
            Left = 0
            Top = 0
            Width = 392
            Height = 75
            AutoScroll = False
            AutoSize = True
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Task'
      ImageIndex = 2
      TabVisible = False
      object evLabel11: TevLabel
        Left = 16
        Top = 21
        Width = 57
        Height = 13
        Caption = 'Task type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cbTaskType: TevDBComboBox
        Left = 87
        Top = 19
        Width = 196
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 0
        UnboundDataType = wwDefault
        OnChange = NotifyChange
      end
    end
  end
end
