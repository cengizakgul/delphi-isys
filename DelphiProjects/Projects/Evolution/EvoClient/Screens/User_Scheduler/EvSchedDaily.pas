unit EvSchedDaily;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ISBasicClasses,  StdCtrls, Spin, EvSched,
  isSchedule, EvUIComponents;

type
  TEvSchedDailyFrm = class(TEvSchedFrm)
    gDaily: TevGroupBox;
    lEvery: TevLabel;
    lDailyDays: TevLabel;
    lStartTime: TevLabel;
    edEvery: TevSpinEdit;
    edStartTime: TevDateTimePicker;
    gbRange: TevGroupBox;
    evLabel9: TevLabel;
    edBeginDate: TevDateTimePicker;
    edEndDate: TevDateTimePicker;
    cbEndDate: TevCheckBox;
    procedure NotifyChange(Sender: TObject);
    procedure cbEndDateClick(Sender: TObject);
  protected
    procedure DoSetControls; override;
    procedure DoCommitChanges; override;
  end;

implementation

{$R *.dfm}


{ TEvSchedDailyFrm }

procedure TEvSchedDailyFrm.DoSetControls;
begin
  inherited;

  edStartTime.Time := FEvent.StartTime;
  edEvery.Value := (FEvent as IisScheduleEventDaily).EveryNbrDay;
  edBeginDate.DateTime := FEvent.StartDate;

  if FEvent.EndDate <> 0 then
    edEndDate.Date := FEvent.EndDate
  else
    edEndDate.Date := Date + 1;
  cbEndDate.Checked := FEvent.EndDate <> 0;
  cbEndDateClick(nil);
end;


procedure TEvSchedDailyFrm.DoCommitChanges;
begin
  inherited;

  FEvent.StartTime := edStartTime.Time;
  (FEvent as IisScheduleEventDaily).EveryNbrDay := edEvery.Value;
  FEvent.StartDate := edBeginDate.Date;

  if cbEndDate.Checked then
    FEvent.EndDate := edEndDate.Date
  else
    FEvent.EndDate := 0;
end;

procedure TEvSchedDailyFrm.NotifyChange(Sender: TObject);
begin
  DoNotifyChange;
end;

procedure TEvSchedDailyFrm.cbEndDateClick(Sender: TObject);
begin
  inherited;
  edEndDate.Enabled:= cbEndDate.Checked;
end;

end.
