inherited EvSchedYearlyFrm: TEvSchedYearlyFrm
  Width = 392
  Height = 240
  object gbRange: TevGroupBox
    Left = 0
    Top = 164
    Width = 392
    Height = 76
    Caption = 'Range of Recurrence'
    TabOrder = 0
    object evLabel9: TevLabel
      Left = 11
      Top = 18
      Width = 53
      Height = 13
      Caption = 'Begin Date'
    end
    object edBeginDate: TevDateTimePicker
      Left = 11
      Top = 39
      Width = 122
      Height = 21
      Date = 39996.000000000000000000
      Format = 'ddd M/d/yyyy'
      Time = 39996.000000000000000000
      TabOrder = 0
      OnChange = NotifyChange
    end
    object cbEndDate: TevCheckBox
      Left = 231
      Top = 14
      Width = 97
      Height = 17
      Caption = 'End Date:'
      TabOrder = 1
      OnClick = cbEndDateClick
    end
    object edEndDate: TevDateTimePicker
      Left = 231
      Top = 39
      Width = 121
      Height = 21
      Date = 42102.000000000000000000
      Format = 'ddd M/d/yyyy'
      Time = 42102.000000000000000000
      Enabled = False
      TabOrder = 2
      OnChange = NotifyChange
    end
  end
  object gbYearly: TevGroupBox
    Left = 0
    Top = 0
    Width = 391
    Height = 147
    Caption = 'Yearly Recurrence'
    TabOrder = 1
    object evLabel5: TevLabel
      Left = 16
      Top = 34
      Width = 48
      Height = 13
      Caption = 'Start Time'
      FocusControl = edStartTime
    end
    object lEvery: TevLabel
      Left = 192
      Top = 34
      Width = 27
      Height = 13
      Caption = 'Every'
    end
    object lDailyDays: TevLabel
      Left = 285
      Top = 34
      Width = 31
      Height = 13
      Caption = 'year(s)'
    end
    object rbDay: TevRadioButton
      Left = 28
      Top = 71
      Width = 43
      Height = 17
      Caption = 'Day'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbDayClick
    end
    object rbThe: TevRadioButton
      Left = 28
      Top = 108
      Width = 46
      Height = 17
      Caption = 'The'
      TabOrder = 1
      OnClick = rbDayClick
    end
    object edDay: TevComboBox
      Left = 81
      Top = 69
      Width = 69
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      TabOrder = 2
      OnChange = NotifyChange
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31'
        'Last')
    end
    object cbThe: TevComboBox
      Left = 81
      Top = 106
      Width = 70
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      OnChange = NotifyChange
      Items.Strings = (
        'first'
        'second'
        'third'
        'fourth'
        'last')
    end
    object cbDayOfWeek: TevComboBox
      Left = 158
      Top = 106
      Width = 103
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 4
      OnChange = NotifyChange
      Items.Strings = (
        'Monday'
        'Tuesday'
        'Wednesday'
        'Thursday'
        'Friday'
        'Saturday'
        'Sunday')
    end
    object edStartTime: TevDateTimePicker
      Left = 82
      Top = 31
      Width = 90
      Height = 21
      Date = 37435.040277777780000000
      Format = 'h:mm tt'
      Time = 37435.040277777780000000
      DateMode = dmUpDown
      Kind = dtkTime
      TabOrder = 5
      OnChange = NotifyChange
    end
    object cbMonth1: TevComboBox
      Left = 158
      Top = 69
      Width = 103
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      DropDownCount = 12
      ItemHeight = 13
      TabOrder = 6
      OnChange = NotifyChange
      Items.Strings = (
        'January'
        'February'
        'March'
        'April'
        'May'
        'June'
        'July'
        'August'
        'September'
        'October'
        'November'
        'December')
    end
    object cbMonth2: TevComboBox
      Left = 268
      Top = 106
      Width = 103
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      DropDownCount = 12
      ItemHeight = 13
      TabOrder = 7
      OnChange = NotifyChange
      Items.Strings = (
        'January'
        'February'
        'March'
        'April'
        'May'
        'June'
        'July'
        'August'
        'September'
        'October'
        'November'
        'December')
    end
    object edEvery: TevSpinEdit
      Left = 227
      Top = 31
      Width = 52
      Height = 22
      MaxValue = 9999
      MinValue = 1
      TabOrder = 8
      Value = 1
      OnChange = NotifyChange
    end
  end
end
