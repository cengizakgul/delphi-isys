unit EvSchedMonthly;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ComCtrls, ISBasicClasses,  StdCtrls, isSchedule,
  IsTypes, EvSched, EvUIComponents;

type
  TEvSchedMonthlyFrm = class(TEvSchedFrm)
    gbMonthly: TevGroupBox;
    lMonthlyMonths2: TevLabel;
    evLabel5: TevLabel;
    rbMonthlyDay: TevRadioButton;
    rbMonthlyThe: TevRadioButton;
    edMonthly: TevComboBox;
    cbMonthlyFirst: TevComboBox;
    cbDayOfWeek: TevComboBox;
    cbApr: TevCheckBox;
    cbMar: TevCheckBox;
    cbFeb: TevCheckBox;
    cbJan: TevCheckBox;
    cbMay: TevCheckBox;
    cbJun: TevCheckBox;
    cbJul: TevCheckBox;
    cbAug: TevCheckBox;
    cbSep: TevCheckBox;
    cbOct: TevCheckBox;
    cbNov: TevCheckBox;
    cbDec: TevCheckBox;
    edStartTime: TevDateTimePicker;
    gbRange: TevGroupBox;
    evLabel9: TevLabel;
    edBeginDate: TevDateTimePicker;
    cbEndDate: TevCheckBox;
    edEndDate: TevDateTimePicker;
    procedure rbMonthlyDayClick(Sender: TObject);
    procedure NotifyChange(Sender: TObject);
    procedure cbEndDateClick(Sender: TObject);
  private
    function GetEvent: IisScheduleEventMonthly;  
  protected
    procedure DoSetControls; override;
    procedure DoCommitChanges; override;
  end;

implementation

{$R *.dfm}

{ TEvSchedMonthlyFrm }

procedure TEvSchedMonthlyFrm.DoCommitChanges;
begin
  inherited;

  GetEvent.StartTime := edStartTime.Time;
  GetEvent.StartDate := edBeginDate.Date;

  if cbEndDate.Checked then
    FEvent.EndDate := edEndDate.Date
  else
    FEvent.EndDate := 0;

  GetEvent.DayOfMonth := TDayNumber(edMonthly.ItemIndex + 1);

  if rbMonthlyThe.Checked then
  begin
    GetEvent.WeekOfMonth := TWeekOfMonth(cbMonthlyFirst.ItemIndex + 1);
    GetEvent.DayOfWeek := TDayOfWeek(cbDayOfWeek.ItemIndex + 1);
  end
  else
  begin
    GetEvent.WeekOfMonth := wmUnknown;
    GetEvent.DayOfWeek := dwUnknown;
  end;

  GetEvent.Months := [];
  if cbJan.Checked then
    GetEvent.Months := GetEvent.Months + [mJanuary];
  if cbFeb.Checked then
    GetEvent.Months := GetEvent.Months + [mFebruary];
  if cbMar.Checked then
    GetEvent.Months := GetEvent.Months + [mMarch];
  if cbApr.Checked then
    GetEvent.Months := GetEvent.Months + [mApril];
  if cbMay.Checked then
    GetEvent.Months := GetEvent.Months + [mMay];
  if cbJun.Checked then
    GetEvent.Months := GetEvent.Months + [mJune];
  if cbJul.Checked then
    GetEvent.Months := GetEvent.Months + [mJuly];
  if cbAug.Checked then
    GetEvent.Months := GetEvent.Months + [mAugust];
  if cbSep.Checked then
    GetEvent.Months := GetEvent.Months + [mSeptember];
  if cbOct.Checked then
    GetEvent.Months := GetEvent.Months + [mOctober];
  if cbNov.Checked then
    GetEvent.Months := GetEvent.Months + [mNovember];
  if cbDec.Checked then
    GetEvent.Months := GetEvent.Months + [mDecember];
end;

procedure TEvSchedMonthlyFrm.DoSetControls;
begin
  inherited;

  edStartTime.Time := GetEvent.StartTime;

  rbMonthlyDay.Checked := GetEvent.DayOfMonth > 0;
  rbMonthlyThe.Checked := not rbMonthlyDay.Checked;
  edMonthly.ItemIndex := GetEvent.DayOfMonth - 1;
  cbMonthlyFirst.ItemIndex := Ord(GetEvent.WeekOfMonth) - 1;
  cbDayOfWeek.ItemIndex := Ord(GetEvent.DayOfWeek) - 1;

  edMonthly.Enabled := rbMonthlyDay.Checked;
  cbMonthlyFirst.Enabled := rbMonthlyThe.Checked;
  cbDayOfWeek.Enabled := rbMonthlyThe.Checked;

  cbJan.Checked := mJanuary in GetEvent.Months;
  cbFeb.Checked := mFebruary in GetEvent.Months;
  cbMar.Checked := mMarch in GetEvent.Months;
  cbApr.Checked := mApril in GetEvent.Months;
  cbMay.Checked := mMay in GetEvent.Months;
  cbJun.Checked := mJune in GetEvent.Months;
  cbJul.Checked := mJuly in GetEvent.Months;
  cbAug.Checked := mAugust in GetEvent.Months;
  cbSep.Checked := mSeptember in GetEvent.Months;
  cbOct.Checked := mOctober in GetEvent.Months;
  cbNov.Checked := mNovember in GetEvent.Months;
  cbDec.Checked := mDecember in GetEvent.Months;

  edBeginDate.DateTime := GetEvent.StartDate;
  if FEvent.EndDate <> 0 then
    edEndDate.Date := FEvent.EndDate
  else
    edEndDate.Date := Date + 1;
  cbEndDate.Checked := FEvent.EndDate <> 0;
  cbEndDateClick(nil);
end;

procedure TEvSchedMonthlyFrm.rbMonthlyDayClick(Sender: TObject);
begin
  edMonthly.Enabled := rbMonthlyDay.Checked;
  cbMonthlyFirst.Enabled := rbMonthlyThe.Checked;
  cbDayOfWeek.Enabled := rbMonthlyThe.Checked;

  if rbMonthlyDay.Checked then
  begin
    cbMonthlyFirst.ItemIndex := -1;
    cbDayOfWeek.ItemIndex := -1;
    edMonthly.ItemIndex := 0;
  end
  else
  begin
    edMonthly.ItemIndex := -1;
    cbMonthlyFirst.ItemIndex := 0;
    cbDayOfWeek.ItemIndex := 0;
  end;

  DoNotifyChange;
end;

function TEvSchedMonthlyFrm.GetEvent: IisScheduleEventMonthly;
begin
  Result := FEvent as IisScheduleEventMonthly;
end;

procedure TEvSchedMonthlyFrm.NotifyChange(Sender: TObject);
begin
  DoNotifyChange;
end;

procedure TEvSchedMonthlyFrm.cbEndDateClick(Sender: TObject);
begin
  inherited;
  edEndDate.Enabled:= cbEndDate.Checked;
end;

end.
