// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit EvUserSchedEditEvent;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ISBasicClasses,  EvSched, EvUserSchedEvent,
  SDDClasses, isSchedule, SDataStructure, EvContext, EvStreamUtils,
  isBaseClasses, EvUtils, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TevUserSchedEditEventFrm = class(TForm)
    EventFrm: TEvUserSchedEventFrm;
    btnOK: TevButton;
    btnCancel: TevButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    function  SetEventID(const AEventID: String): Boolean;
    procedure OnEventChange(Sender: TObject);    
  end;

  procedure ShowUserSchedEvent(const AEventID: String);

implementation

{$R *.DFM}


procedure ShowUserSchedEvent(const AEventID: String);
var
  Frm: TevUserSchedEditEventFrm;
begin
  Frm := TevUserSchedEditEventFrm.Create(Application);
  try
    if Frm.SetEventID(AEventID) then
      Frm.ShowModal;
  finally
    Frm.Free;
  end;
end;

procedure TevUserSchedEditEventFrm.FormCreate(Sender: TObject);
begin
  EventFrm.OnChange := OnEventChange;
  
  DM_SERVICE_BUREAU.SB_USER_NOTICE.DisableControls;
  DM_SERVICE_BUREAU.SB_USER_NOTICE.CheckDSCondition('SB_USER_NBR = ' + IntToStr(Context.UserAccount.InternalNbr));
  DM_SERVICE_BUREAU.SB_USER_NOTICE.Open;
end;

procedure TevUserSchedEditEventFrm.FormDestroy(Sender: TObject);
begin
  DM_SERVICE_BUREAU.SB_USER_NOTICE.EnableControls;
end;

function TevUserSchedEditEventFrm.SetEventID(const AEventID: String): Boolean;
var
  S: IisStream;
  Evt: IisScheduleEvent;
begin
  btnOK.Enabled := False;
  Result := False;

  if DM_SERVICE_BUREAU.SB_USER_NOTICE.Locate('SB_USER_NOTICE_NBR', AEventID, []) then
    if not DM_SERVICE_BUREAU.SB_USER_NOTICE.TASK.IsNull then
    begin
      S := TisStream.Create;
      S := DM_SERVICE_BUREAU.SB_USER_NOTICE.GetBlobData('TASK');
      S.Position := 0;
      try
        Evt := ObjectFactory.CreateInstanceFromStream(S) as IisScheduleEvent;
        Evt.LastRunDate := DM_SERVICE_BUREAU.SB_USER_NOTICE.LAST_DISMISS.AsDateTime;
        EventFrm.Event := Evt;

        EventFrm.Subject := DM_SERVICE_BUREAU.SB_USER_NOTICE.NAME.AsString;
        if not DM_SERVICE_BUREAU.SB_USER_NOTICE.IsBlobLoaded('NOTES') then
           DM_SERVICE_BUREAU.SB_USER_NOTICE.GetBlobData('NOTES');
        EventFrm.Notes := DM_SERVICE_BUREAU.SB_USER_NOTICE.NOTES.AsString;

        Caption := EventFrm.Subject;

        Result := True;
      except
        EventFrm.Event := nil;
      end;
    end;
end;

procedure TevUserSchedEditEventFrm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  S: IisStream;
begin
  if ModalResult = mrOK then
  begin
    DM_SERVICE_BUREAU.SB_USER_NOTICE.Edit;
    DM_SERVICE_BUREAU.SB_USER_NOTICE.NAME.AsString := EventFrm.Subject;
    DM_SERVICE_BUREAU.SB_USER_NOTICE.NOTES.AsString := EventFrm.Notes;

    DM_SERVICE_BUREAU.SB_USER_NOTICE.LAST_DISMISS.Clear;
    DM_SERVICE_BUREAU.SB_USER_NOTICE.NEXT_REMINDER.Clear;

    S := (EventFrm.Event as IisInterfacedObject).AsStream;
    S.Position := 0;
    DM_SERVICE_BUREAU.SB_USER_NOTICE.UpdateBlobData('TASK',S);

    DM_SERVICE_BUREAU.SB_USER_NOTICE.Post;
    ctx_DataAccess.PostDataSets([DM_SERVICE_BUREAU.SB_USER_NOTICE]);

    Context.UserScheduler.RefreshTaskList;
  end;
end;

procedure TevUserSchedEditEventFrm.OnEventChange(Sender: TObject);
begin
  Caption := EventFrm.Subject;
  btnOK.Enabled := True;
end;

end.
