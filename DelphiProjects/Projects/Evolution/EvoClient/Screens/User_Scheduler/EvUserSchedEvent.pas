unit EvUserSchedEvent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ISBasicClasses,  StdCtrls, Spin, ExtCtrls,
  EvSchedDaily, EvSchedWeekly, EvSchedMonthly, EvSchedYearly, EvSchedOnce,
  isSchedule, EvSched, wwdblook, SFieldCodeValues, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, EvConsts, isTypes, EvUIComponents,
  isUIwwDBComboBox, isUIEdit;

type
  TEvUserSchedEventFrm = class(TEvSchedFrm)
    evPageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    evLabel11: TevLabel;
    TabSheet2: TTabSheet;
    cbTaskType: TevDBComboBox;
    pcRecurrence: TevPageControl;
    tsRecurrDaily: TTabSheet;
    RecurrDaily: TEvSchedDailyFrm;
    tsRecurrWeekly: TTabSheet;
    RecurrWeekly: TEvSchedWeeklyFrm;
    tsRecurrMonthly: TTabSheet;
    RecurrMonthly: TEvSchedMonthlyFrm;
    tsRecurrYearly: TTabSheet;
    RecurrYearly: TEvSchedYearlyFrm;
    tsRecurrOnce: TTabSheet;
    RecurrOnce: TEvSchedOnceFrm;
    chbActive: TevCheckBox;
    evLabel1: TevLabel;
    edSubject: TevEdit;
    lSchedInfo: TevLabel;
    lRuns: TevLabel;
    evLabel13: TevLabel;
    cbReminder: TevDBComboBox;
    evLabel2: TevLabel;
    edNotes: TevMemo;
    lblSchedule: TevLabel;
    procedure pcRecurrenceChange(Sender: TObject);
    procedure NotifyChange(Sender: TObject);
    procedure pcRecurrenceChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure evPageControl1Change(Sender: TObject);
  private
    function  GetNotes: String;
    function  GetSubject: String;
    procedure SetNotes(const AValue: String);
    procedure SetSubject(const AValue: String);
    procedure UpdateSchedInfo;
  protected
    procedure DoNotifyChange; override;
    procedure DoSetControls; override;
    procedure DoCommitChanges; override;
  public
    procedure CreateEvents;  
    procedure AfterConstruction; override;
    property  Subject: String read GetSubject write SetSubject;
    property  Notes: String read GetNotes write SetNotes;
  end;

implementation

uses isBaseClasses;

{$R *.dfm}

{ TEvUserSchedEventFrm }

procedure TEvUserSchedEventFrm.AfterConstruction;
begin
  inherited;
  RecurrDaily.OnChange := NotifyChange;
  RecurrWeekly.OnChange := NotifyChange;
  RecurrMonthly.OnChange := NotifyChange;
  RecurrYearly.OnChange := NotifyChange;
  RecurrOnce.OnChange := NotifyChange;

  CreateEvents;
  
  pcRecurrence.ActivePage := tsRecurrDaily;
  pcRecurrence.OnChange(nil);
end;

procedure TEvUserSchedEventFrm.CreateEvents;
begin
  FEventDaily:= TisScheduleEventDaily.Create;
  FEventDaily.StartTime := Now;
  FEventDaily.StartDate := Date;
  FEventWeekly:= TisScheduleEventWeekly.Create;
  FEventWeekly.StartTime := Now;
  FEventWeekly.StartDate := Date;
  FEventMonthly:= TisScheduleEventMonthly.Create;
  FEventMonthly.DayOfMonth := 1;
  FEventMonthly.StartTime := Now;
  FEventMonthly.StartDate := Date;
  FEventYearly:= TisScheduleEventYearly.Create;
  FEventYearly.DayOfMonth := 1;
  FEventYearly.Month := mJanuary;
  FEventYearly.StartTime := Now;
  FEventYearly.StartDate := Date;
  FEventOnce:= TisScheduleEventOnce.Create;
  FEventOnce.StartTime := Now;
  FEventOnce.StartDate := Date;
end;

procedure TEvUserSchedEventFrm.DoCommitChanges;
begin
  inherited;
  TEvSchedFrm(pcRecurrence.ActivePage.Controls[0]).CommitChanges;

  FEvent.TaskID := cbTaskType.Value;
  FEvent.Enabled := chbActive.Checked;
  FEvent.TaskParams.AddValue('Reminder', cbReminder.Value);
end;


function TEvUserSchedEventFrm.GetNotes: String;
begin
  Result := edNotes.Text;
end;

function TEvUserSchedEventFrm.GetSubject: String;
begin
  Result := edSubject.Text;
end;

procedure TEvUserSchedEventFrm.DoSetControls;
begin
  inherited;

  cbTaskType.Items.Text := UserSchedTask_ComboChoices;
  cbReminder.Items.Text := UserSchedReminder_ComboChoices;

  if not Assigned(FEvent) then
  begin
    pcRecurrence.ActivePage := tsRecurrDaily;
    pcRecurrence.OnChange(nil);
  end;

  if Supports(FEvent, IisScheduleEventDaily) then
  begin
    pcRecurrence.ActivePage := tsRecurrDaily;
    FEventDaily:= (FEvent  as IisScheduleEventDaily);
  end
  else if Supports(FEvent, IisScheduleEventWeekly) then
  begin
    pcRecurrence.ActivePage := tsRecurrWeekly;
    FEventWeekly:= (FEvent as IisScheduleEventWeekly);
  end
  else if Supports(FEvent, IisScheduleEventMonthly) then
  begin
    pcRecurrence.ActivePage := tsRecurrMonthly;
    FEventMonthly:= (FEvent as IisScheduleEventMonthly);
  end
  else if Supports(FEvent, IisScheduleEventYearly) then
  begin
    pcRecurrence.ActivePage := tsRecurrYearly;
    FEventYearly:= (FEvent as IisScheduleEventYearly);
  end
  else if Supports(FEvent, IisScheduleEventOnce) then
  begin
    pcRecurrence.ActivePage := tsRecurrOnce;
    FEventOnce:= (FEvent as IisScheduleEventOnce);
  end;

  TEvSchedFrm(pcRecurrence.ActivePage.Controls[0]).Event := FEvent;

  cbTaskType.Value := FEvent.TaskID;
  chbActive.Checked := FEvent.Enabled;
  cbReminder.Value := FEvent.TaskParams.TryGetValue('Reminder', 0);

  UpdateSchedInfo;
end;

procedure TEvUserSchedEventFrm.SetNotes(const AValue: String);
begin
  FSettingControls := True;
  try
    edNotes.Text := AValue;
  finally
    FSettingControls := False;
  end;
end;

procedure TEvUserSchedEventFrm.SetSubject(const AValue: String);
begin
  FSettingControls := True;
  try
    edSubject.Text := AValue;
  finally
    FSettingControls := False;
  end;
end;

procedure TEvUserSchedEventFrm.pcRecurrenceChange(Sender: TObject);
begin

  if pcRecurrence.ActivePage = tsRecurrDaily then
    FEvent := FEventDaily
  else if pcRecurrence.ActivePage = tsRecurrWeekly then
    FEvent := FEventWeekly
  else if pcRecurrence.ActivePage = tsRecurrMonthly then
    FEvent := FEventMonthly
  else if pcRecurrence.ActivePage = tsRecurrYearly then
    FEvent := FEventYearly
  else if pcRecurrence.ActivePage = tsRecurrOnce then
    FEvent := FEventOnce;

  TEvSchedFrm(pcRecurrence.ActivePage.Controls[0]).Event := FEvent;
  DoNotifyChange;
end;

procedure TEvUserSchedEventFrm.NotifyChange(Sender: TObject);
begin
  DoNotifyChange;
end;

procedure TEvUserSchedEventFrm.UpdateSchedInfo;
var
  d: TDateTime;
  Evt: IisScheduleEvent;
  s: String;
begin
  Evt := Event;
  lSchedInfo.Caption := 'Schedule: ' + Evt.GetSchedInfo;

  if Evt.LastRunDate <> 0 then
    s := 'Last run: ' + FormatDateTime('mm/dd/yyyy  h:nn AM/PM', Evt.LastRunDate)
  else
    s := '';

  d := Evt.GetClosestStartTimeTo(Now);
  if d <> 0 then
  begin
    if s <> '' then
      s := s + '    ';
    s := s + 'Next run: ' + FormatDateTime('mm/dd/yyyy  h:nn AM/PM', d);
  end;

  lRuns.Caption := s;
end;

procedure TEvUserSchedEventFrm.DoNotifyChange;
begin
  inherited;
  if not FSettingControls then
    UpdateSchedInfo;
end;

procedure TEvUserSchedEventFrm.pcRecurrenceChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;

  if pcRecurrence.ActivePage = tsRecurrDaily then
  FEventDaily := (FEvent  as IisScheduleEventDaily)
  else if pcRecurrence.ActivePage = tsRecurrWeekly then
   FEventWeekly:= (FEvent as IisScheduleEventWeekly)
  else if pcRecurrence.ActivePage = tsRecurrMonthly then
    FEventMonthly:= (FEvent as IisScheduleEventMonthly)
  else if pcRecurrence.ActivePage = tsRecurrYearly then
    FEventYearly:= (FEvent as IisScheduleEventYearly)
  else if pcRecurrence.ActivePage = tsRecurrOnce then
    FEventOnce:= (FEvent as IisScheduleEventOnce);
end;


procedure TEvUserSchedEventFrm.evPageControl1Change(Sender: TObject);
begin
  inherited;
  if pcRecurrence.ActivePage = tsRecurrDaily then
  begin
   FEvent := FEventDaily;
   RecurrDaily.cbEndDate.Checked:=  FEvent.EndDate <> 0;
  end
  else if pcRecurrence.ActivePage = tsRecurrWeekly then
  begin
   FEvent := FEventWeekly;
   RecurrWeekly.cbEndDate.Checked:=  FEvent.EndDate <> 0;
  end
  else if pcRecurrence.ActivePage = tsRecurrMonthly then
  begin
   FEvent := FEventMonthly;
   RecurrMonthly.cbEndDate.Checked:=  FEvent.EndDate <> 0;
  end
  else if pcRecurrence.ActivePage = tsRecurrYearly then
  begin
   FEvent := FEventYearly;
   RecurrYearly.cbEndDate.Checked:=  FEvent.EndDate <> 0;
  end
  else if pcRecurrence.ActivePage = tsRecurrOnce then
    FEvent := FEventOnce;

  TEvSchedFrm(pcRecurrence.ActivePage.Controls[0]).Event := FEvent;

end;

end.
