inherited EDIT_IM_M2_BASE: TEDIT_IM_M2_BASE
  inherited PageControl1: TevPageControl
    ActivePage = TabSheet1
    inherited TabSheet1: TTabSheet
      inherited evPanel1: TevPanel
        Width = 427
        Height = 237
        inherited evPanel5: TevPanel
          Top = 105
          Width = 427
          Height = 132
          Visible = True
          inherited EvBevel2: TEvBevel
            Height = 128
          end
          inherited pnlSetup: TevPanel
            Width = 296
            Height = 128
            inherited EvBevel1: TEvBevel
              Width = 296
            end
            inherited pnlSetupControls: TevPanel
              Width = 296
              Height = 89
            end
          end
        end
        inherited pnlConnectionInputHolder: TevPanel
          Width = 427
          Height = 105
          inline ConnectDatabaseFileNameFrame: TConnectDatabaseFileNameFrame
            Left = 2
            Top = 2
            Width = 423
            Height = 101
            Align = alClient
            TabOrder = 0
            inherited BitBtn1: TevBitBtn
              OnClick = ConnectDatabaseFileNameFrameBitBtn1Click
            end
          end
        end
      end
    end
    inherited TabSheet5: TTabSheet
      inherited viewerFrame: TTableSpaceViewFrame
        inherited evPanel2: TevPanel
          inherited dgView: TevDBGrid
            Width = 624
            Height = 334
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited LogFrame: TImportLoggerViewFrame
        inherited pnlLog: TevPanel
          inherited pgLogView: TevPageControl
            Width = 661
            Height = 399
            inherited tbshDevView: TTabSheet
              inherited pnlDevView: TevPanel
                Width = 653
                Height = 373
                inherited mmDevView: TevMemo
                  Width = 653
                  Height = 373
                end
              end
            end
            inherited tbshUserTextView: TTabSheet
              inherited pnlUserTextView: TevPanel
                Width = 653
                Height = 373
                inherited mmUserTextView: TevMemo
                  Width = 653
                  Height = 373
                end
              end
            end
          end
          inherited LoggerRichView: TImportLoggerRichViewFrame
            inherited pnlUserView: TevPanel
              inherited pnlBottom: TevPanel
                Height = 145
                inherited evSplitter2: TevSplitter
                  Height = 145
                end
                inherited pnlLeft: TevPanel
                  Height = 145
                  inherited mmDetails: TevMemo
                    Height = 120
                  end
                end
                inherited pnlRight: TevPanel
                  Height = 145
                  inherited mmContext: TevMemo
                    Height = 120
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
