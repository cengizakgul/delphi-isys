object MEEImportParamFrame: TMEEImportParamFrame
  Left = 0
  Top = 0
  Width = 409
  Height = 259
  TabOrder = 0
  object cbTerm: TevCheckBox
    Left = 24
    Top = 8
    Width = 161
    Height = 17
    Caption = 'Import only active employees'
    TabOrder = 0
  end
  object gbLiab: TevGroupBox
    Left = 8
    Top = 40
    Width = 393
    Height = 209
    Caption = 'Liabilities'
    TabOrder = 1
    object lblAdjustmentType: TevLabel
      Left = 16
      Top = 72
      Width = 79
      Height = 13
      Caption = 'Adjustment Type'
    end
    object lblLiabilityStatus: TevLabel
      Left = 256
      Top = 256
      Width = 67
      Height = 13
      Caption = 'Liability Status'
      Color = clTeal
      ParentColor = False
      Visible = False
    end
    object evLabel4: TevLabel
      Left = 16
      Top = 20
      Width = 320
      Height = 13
      Caption = 
        'Liabilities processed prior or equal to this date will be marked' +
        ' as paid'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object cbAdjustmentType: TevDBComboBox
      Left = 16
      Top = 88
      Width = 129
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = True
      AutoDropDown = True
      DropDownCount = 8
      ItemHeight = 0
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 0
      UnboundDataType = wwDefault
    end
    object cbLiabilityStatus: TevDBComboBox
      Left = 256
      Top = 272
      Width = 121
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = False
      AutoDropDown = True
      Color = clTeal
      DropDownCount = 8
      ItemHeight = 0
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 1
      UnboundDataType = wwDefault
      Visible = False
    end
    object rgImpounded2: TevDBRadioGroup
      Left = 17
      Top = 257
      Width = 129
      Height = 41
      Caption = 'Collected for Quarterlies'
      Color = clTeal
      Columns = 2
      DataField = 'IMPOUNDED'
      Items.Strings = (
        'Yes'
        'No')
      ParentColor = False
      TabOrder = 2
      Values.Strings = (
        'Y'
        'N')
      Visible = False
    end
    object dtProcessDate: TevDateTimePicker
      Left = 16
      Top = 36
      Width = 129
      Height = 21
      Date = 38533.000000000000000000
      Time = 38533.000000000000000000
      TabOrder = 3
    end
    object evGroupBox1: TevGroupBox
      Left = 168
      Top = 72
      Width = 137
      Height = 89
      Caption = 'Due Date'
      TabOrder = 4
      object evLabel12: TevLabel
        Left = 17
        Top = 40
        Width = 23
        Height = 13
        Caption = 'Date'
      end
      object cbDueDate: TevCheckBox
        Left = 16
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Use Due Date?'
        TabOrder = 0
      end
      object dtDueDate: TevDateTimePicker
        Left = 16
        Top = 56
        Width = 105
        Height = 21
        Date = 37046.000000000000000000
        Time = 37046.000000000000000000
        TabOrder = 1
      end
    end
    object rgImpounded: TevRadioGroup
      Left = 16
      Top = 120
      Width = 129
      Height = 41
      Caption = 'Collected for Quarterlies'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Yes'
        'No')
      TabOrder = 5
    end
    object cbOnlyThisYear: TevCheckBox
      Left = 16
      Top = 176
      Width = 321
      Height = 17
      Caption = 'Import tax liabilities only from current year'
      TabOrder = 6
    end
  end
end
