object ConnectM3Frame: TConnectM3Frame
  Left = 0
  Top = 0
  Width = 395
  Height = 176
  TabOrder = 0
  object lblHost: TevLabel
    Left = 8
    Top = 16
    Width = 22
    Height = 13
    Caption = 'Host'
  end
  object lblUserId: TevLabel
    Left = 8
    Top = 64
    Width = 34
    Height = 13
    Caption = 'User Id'
  end
  object lblPassword: TevLabel
    Left = 8
    Top = 88
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object lblDatabase: TevLabel
    Left = 8
    Top = 40
    Width = 46
    Height = 13
    Caption = 'Database'
  end
  object lblCompany: TevLabel
    Left = 7
    Top = 120
    Width = 44
    Height = 13
    Caption = 'Company'
  end
  object evButton1: TevButton
    Left = 80
    Top = 144
    Width = 121
    Height = 25
    Caption = 'Connect'
    TabOrder = 0
    OnClick = evButton1Click
  end
  object edHost: TevEdit
    Left = 80
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object edDatabase: TevEdit
    Left = 80
    Top = 32
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object edUserId: TevEdit
    Left = 80
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object edPassword: TevEdit
    Left = 80
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object edCompany: TevEdit
    Left = 80
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object bbCompany: TevBitBtn
    Left = 208
    Top = 111
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 6
    OnClick = bbCompanyClick
  end
end
