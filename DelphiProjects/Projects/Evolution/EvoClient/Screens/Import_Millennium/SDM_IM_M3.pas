unit SDM_IM_M3;

interface

uses
  SysUtils, Classes, genericImport, DB, ADODB,  EvUtils, SDataStructure,
  Variants, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, SFieldCodeValues, EvContext,
  ISDataAccessComponents, EvDataAccessComponents, gdystrset, isBasicUtils,
  SDataDictSystem, evExceptions, EvUIComponents, EvClientDataSet;

type
  TM3ConnectionInfo = record
    Host: string;
    InitialCatalog: string;
    UserId: string;
    Password: string;
    Company: string;
  end;

  IM3DataModule = interface
  ['{970EE26F-4B53-4817-86DA-965FEC8658B7}']
    procedure FilterByEmpStatus( bFilter: boolean; status: Variant );
    procedure FilterLiabilities( bFilter: boolean; afterDate: TDateTime );
  end;

  TDM_IM_M3 = class(TDataModule, ITablespace, IExternalDataModule, IM3DataModule)
    ACCompany: TADOConnection;
    CInfo: TADOQuery;
    CEthnicity: TADOQuery;
    cdsEthnicity: TevClientDataSet;
    cdsEthnicityCode: TStringField;
    cdsEthnicityName: TStringField;
    CEmpStatus: TADOQuery;
    cdsEmpStatus: TevClientDataSet;
    StringField1: TStringField;
    StringField2: TStringField;
    CFreq: TADOQuery;
    cdsPayFreq: TevClientDataSet;
    StringField3: TStringField;
    StringField4: TStringField;
    CEmpType: TADOQuery;
    cdsEmpType: TevClientDataSet;
    StringField5: TStringField;
    StringField6: TStringField;
    CTax: TADOQuery;
    cdsCoTaxes: TevClientDataSet;
    StringField7: TStringField;
    StringField8: TStringField;
    cdsCoTaxesType: TStringField;
    cdsCoTaxesSubType: TStringField;
    EInfo: TADOQuery;
    SRatePer: TADOQuery;
    cdsRatePer: TevClientDataSet;
    StringField9: TStringField;
    StringField10: TStringField;
    CEEOClass: TADOQuery;
    ETax: TADOQuery;
    ERate: TADOQuery;
    CRateCodes: TADOQuery;
    cdsRateCode: TevClientDataSet;
    cdsRateCodeNumber: TIntegerField;
    SReciprocal: TADOQuery;                                
    cdsReciprocal: TevClientDataSet;
    StringField11: TStringField;
    StringField12: TStringField;
    EDs: TADOQuery;
    EDirDep: TADOQuery;
    EDed: TADOQuery;
    EEarn: TADOQuery;
    CCalcCodes: TADOQuery;
    cdsEDCalc: TevClientDataSet;
    StringField13: TStringField;
    StringField14: TStringField;
    CAgency: TADOQuery;
    EYtd: TADOQuery;
    CEthnicityethnicity: TStringField;
    CEthnicitydescription: TStringField;
    CEmpStatusstatus: TStringField;
    CEmpStatusdescription: TStringField;
    CFreqfrequency: TStringField;
    CFreqdescription: TStringField;
    CEmpTypeempType: TStringField;
    CEmpTypedescription: TStringField;
    CTaxtcode: TStringField;
    CTaxdescription: TStringField;
    SRatePerratePer: TStringField;
    SRatePerdescription: TStringField;
    CRateCodesrateCode: TStringField;
    CRateCodesdescription: TStringField;
    SReciprocalreciprocalCode: TStringField;
    SReciprocaldescription: TStringField;
    CCalcCodescode: TStringField;
    CCalcCodesdescription: TStringField;
    CEEOClasseeoClass: TStringField;
    CEEOClassdescription: TStringField;
    EDseord: TStringField;
    EDscode: TStringField;
    EDsdescription: TStringField;
    CAgencyid: TStringField;
    CAgencyname: TStringField;
    CAgencyco: TStringField;
    EDsco: TStringField;
    CEEOClassco: TStringField;
    CCalcCodesco: TStringField;
    CRateCodesco: TStringField;
    CTaxco: TStringField;
    CEmpTypeco: TStringField;
    CFreqco: TStringField;
    CEmpStatusco: TStringField;
    CEthnicityco: TStringField;
    CDept1: TADOQuery;
    CDept2: TADOQuery;
    CDept3: TADOQuery;
    CDept4: TADOQuery;
    CDept5: TADOQuery;
    cdsEvoDBDT: TevClientDataSet;
    cdsM3Cat: TevClientDataSet;
    StringField18: TStringField;
    cdsEvoDBDTDBDT: TStringField;
    cdsM3Catnumber: TIntegerField;
    CAllDept: TADOQuery;
    cdsEDCalcTargetField: TStringField;
    EDirDepNum: TADOQuery;
    CoMDLocals: TADOQuery;
    cdsMDMaritalStatuses: TevClientDataSet;
    cdsMDMaritalStatusesSTATUS_TYPE: TStringField;
    cdsMDMaritalStatusesSTATUS_DESCRIPTION: TStringField;
    CoMDLocalstcode: TStringField;
    CoMDLocalsdescription: TStringField;
    cdsM3Catdesc: TStringField;
    cdsEvoDBDTlevel: TIntegerField;
    CTaxLiab: TADOQuery;
    procedure CRateCodesFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure EmpStatusFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure CTaxLiabFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    FLogger: IImportLogger;
    FConnectionString: string;
    FCustomCompanyNumber: string;
    FEmpStatus: TStringSet;
    FLiabilityDate: TDateTime;
  protected
    {IExternalDataModule}
    procedure OpenConnection( aConnectionString: string );
    procedure CloseConnection;
    function Connected: boolean;
    function ConnectionString: string;
    function CustomCompanyNumber: string;
    procedure SetLogger( aLogger: IImportLogger );
    {IM3DataModule}
    procedure FilterByEmpStatus( bFilter: boolean; status: Variant );
    procedure FilterLiabilities( bFilter: boolean; afterDate: TDateTime );
  public
    {ITableSpace}
    function GetTable( const name: string ): TDataSet;
    procedure GetAvailableTables( const list: TStrings );
  end;

function ParseConnectionString( aConnStr: string ): TM3ConnectionInfo;
function ComposeConnectionString( info: TM3ConnectionInfo ): string;
function ComposeADOConnectionString( info: TM3ConnectionInfo ): string;

implementation

uses
  DateUtils;

{$R *.dfm}

{ TDM_IM_M3 }

procedure TDM_IM_M3.CloseConnection;
var
  i: integer;
begin
  for i := 0 to ComponentCount - 1 do
    try
      if Components[i] is TCustomADODataSet then
      begin
        if TCustomADODataSet(Components[i]).Active then
          TCustomADODataSet(Components[i]).Close;
      end
      else if Components[i] is TevClientDataSet then
        if TevClientDataSet(Components[i]).Active then
          TevClientDataSet(Components[i]).Close;
    except
      on e: Exception do FLogger.LogDebug( 'Error occured while closing connection. Ignored.', e.Message );
      //dont raise
    end;

  ACCompany.Close;
  FConnectionString := '';
end;

function TDM_IM_M3.Connected: boolean;
begin
  Result := ACCompany.Connected;
end;

function TDM_IM_M3.ConnectionString: string;
begin
  Result := FConnectionString;
end;

function TDM_IM_M3.CustomCompanyNumber: string;
begin
  Result := FCustomCompanyNumber;
end;

procedure TDM_IM_M3.GetAvailableTables(const list: TStrings);
var
  i: integer;
begin
  for i := 0 to ComponentCount-1 do
    if (Components[i] is TCustomADODataSet) or
        (Components[i] is TevClientDataSet) and InSet( Components[i].Name, ['cdsM3Cat', 'cdsDBDT'])  then
      list.Add(Components[i].Name);
  list.Add('EInfoSalary');
  list.Add('EInfoFix');
end;

function TDM_IM_M3.GetTable(const name: string): TDataSet;
var
  c: TComponent;
  tn: string;
begin
  tn := name;
  if (tn = 'EInfoSalary') or (tn = 'EInfoFix') then
    tn := 'EInfo';
  c := FindComponent( tn );
  if not assigned(c) {or not (c is TSDQuery)} then
    raise EevException.CreateFmt( 'Unknown table requested: <%s>',[name]);
  Result := c as TDataSet;
end;

procedure OpenClientForCompany( custom_company_number: string );
begin
  DM_TEMPORARY.TMP_CO.DataRequired('' );
  if DM_TEMPORARY.TMP_CO.Locate('CUSTOM_COMPANY_NUMBER', custom_company_number, []) then
    ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO['CL_NBR'])
  else
    raise EevException.CreateFmt( 'Company <%s> doesn''t exist in Evolution database', [custom_company_number] );
  DM_COMPANY.CO.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_SYSTEM.SY_HR_EEO.DataRequired();
  DM_SYSTEM.SY_STATE_MARITAL_STATUS.DataRequired();
  DM_SYSTEM.SY_STATES.DataRequired();
  DM_COMPANY.CO_STATES.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_COMPANY.CO_SUI.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_CLIENT.CL_AGENCY.DataRequired();
  DM_CLIENT.CL_E_DS.DataRequired();
end;

procedure ComboChoiceToCDS( aComboChoices: string; ds: TevClientDataSet);
var
  t: TStringList;
  i: integer;
  s: string;
begin
  t := TStringList.Create;
  try
    t.Text := aComboChoices;
    for i := 0 to Pred(t.Count) do
    begin
      s := t[i];
      ds.Append;
      ds['Name'] := GetNextStrValue(s, #9);
      ds['Code'] := s;
      ds.Post;
    end;
  finally
    FreeAndNil( t );
  end;
end;

procedure Append( ds: TevClientDataSet; aFields: string; vals: array of Variant );
begin
  ds.Append;
  if Length(vals) > 1 then
    ds[aFields] := VarArrayOf(vals)
  else
    ds[aFields] := vals[0];
  ds.Post;
end;

procedure TDM_IM_M3.OpenConnection(aConnectionString: string);
const
  sText = 'Opening M3 database';
var
  i, n: integer;
  st: Variant;
  t: TStringList;
  j: integer;
  info: TM3ConnectionInfo;
  s,s1: string;
  MD_SY_STATES_NBR: Variant;
  ms: TSY_STATE_MARITAL_STATUS;

begin
  if not Connected or (FConnectionString <> aConnectionString) then
  begin
    FLogger.LogEntry( sText );
    try
      try
        ctx_StartWait( sText, ComponentCount-1 );
        try
          CloseConnection;
          try
            FConnectionString := aConnectionString;
            info := ParseConnectionString( FConnectionString );
            ACCompany.ConnectionString := ComposeADOConnectionString( info );
            ACCompany.Open;
            FCustomCompanyNumber := info.company;
            EYtd.Parameters.ParamByName('Year').Value := YearOf(Date);
            for i := 0 to ComponentCount - 1 do
              if Components[i] is TADOQuery then
              try
                for j := 0 to TADOQuery(Components[i]).Parameters.Count-1 do
                  if TADOQuery(Components[i]).Parameters[j].Name = 'co' then
                     TADOQuery(Components[i]).Parameters[j].Value := FCustomCompanyNumber;
                TADOQuery(Components[i]).Open;
                ctx_UpdateWait( sText, i );
              except
                FLogger.StopExceptionAndWarnFmt( 'Failed to read data', [] );
              end
              else if Components[i] is TevClientDataSet then
              try
                TevClientDataSet(Components[i]).CreateDataSet;
              except
                FLogger.StopExceptionAndWarnFmt( 'Internal error: failed to create client dataset',[] );
              end;
            OpenClientForCompany( FCustomCompanyNumber );

            ComboChoiceToCDS( Ethnicity_ComboChoices, cdsEthnicity );
            ComboChoiceToCDS( EE_TerminationCode_ComboChoices, cdsEmpStatus );
            ComboChoiceToCDS( PayFrequencyType_ComboChoices, cdsPayFreq );
            ComboChoiceToCDS( PositionStatus_ComboChoices, cdsEmpType );
            ComboChoiceToCDS( StateReciprocal_ComboChoices, cdsReciprocal );
//            ComboChoiceToCDS( ScheduledCalcMethod_ComboBoxChoices, cdsEDCalc );


            t := TStringList.Create;
            try
              t.Text := ScheduledCalcMethod_ComboBoxChoices;
              for i := 0 to Pred(t.Count) do
              begin
                s := t[i];
                s1 := GetNextStrValue(s, #9);
                Append( cdsEDCalc, 'Name;Code;TargetField', [s1, s, 'Amount'] ); //TargetField is part of the key - it's value is used in the import code!
                Append( cdsEDCalc, 'Name;Code;TargetField', [s1, s, '%'] );
              end;

              Append( cdsCoTaxes, 'Type;SubType;Name', ['F','FE','Federal'] );
              Append( cdsCoTaxes, 'Type;SubType;Name', ['F', 'OE', 'EE OASDI'] );
              Append( cdsCoTaxes, 'Type;SubType;Name', ['F', 'OR', 'ER OASDI'] );
              Append( cdsCoTaxes, 'Type;SubType;Name', ['F', 'ME', 'EE Medicare'] );
              Append( cdsCoTaxes, 'Type;SubType;Name', ['F', 'MR', 'ER Medicare'] );
              Append( cdsCoTaxes, 'Type;SubType;Name', ['F', 'FU', 'FUI'] );
              Append( cdsCoTaxes, 'Type;SubType;Name', ['F', 'EI', 'EIC'] );

              DM_COMPANY.CO_STATES.First;
              while not DM_COMPANY.CO_STATES.Eof do
              begin
                st := DM_COMPANY.CO_STATES.STATE.Value;
                Append( cdsCoTaxes, 'Type;Code;SubType;Name', ['S', st, 'ST', st + ' State'] );
                Append( cdsCoTaxes, 'Type;Code;SubType;Name', ['S', st, 'SE', st + ' EE SDI'] );
                Append( cdsCoTaxes, 'Type;Code;SubType;Name', ['S', st, 'SR', st + ' ER SDI'] );
                DM_COMPANY.CO_STATES.Next;
              end;

              DM_COMPANY.CO_SUI.First;
              while not DM_COMPANY.CO_SUI.Eof do
              begin
                cdsCoTaxes.Append;
                cdsCoTaxes['Type'] := 'U';
                cdsCoTaxes['Code'] := DM_COMPANY.CO_SUI.SY_SUI_NBR.AsString;
                cdsCoTaxes['SubType'] := DM_COMPANY.CO_SUI.FieldbyName('STATE').AsString;
                cdsCoTaxes['Name'] := DM_COMPANY.CO_SUI.FieldbyName('SUI_Name').AsString;
                cdsCoTaxes.Post;
                DM_COMPANY.CO_SUI.Next;
              end;

              DM_COMPANY.CO_LOCAL_TAX.First;
              while not DM_COMPANY.CO_LOCAL_TAX.Eof do
              begin
                cdsCoTaxes.Append;
                cdsCoTaxes['Type'] := 'Z';
                cdsCoTaxes['Code'] := DM_COMPANY.CO_LOCAL_TAX.SY_LOCALS_NBR.AsString;
//                cdsCoTaxes['SubType'] := DM_COMPANY.CO_LOCAL_TAX.FieldbyName('LocalState').AsString;
                cdsCoTaxes['Name'] := DM_COMPANY.CO_LOCAL_TAX.FieldbyName('LocalName').AsString;
                cdsCoTaxes.Post;
                DM_COMPANY.CO_LOCAL_TAX.Next;
              end;

              Append( cdsRatePer, 'Code;Name', ['H','Hour'] );
              Append( cdsRatePer, 'Code;Name', ['D','Day'] );
              Append( cdsRatePer, 'Code;Name', ['W','Week'] );

              n := CRateCodes.RecordCount;
              for i := 2 to n + 1 do
                if not CRateCodes.Locate('rateCode', i, []) then
                begin
                  cdsRateCode.Append;
                  cdsRateCode['Number'] := i;
                  cdsRateCode.Post;
                end;
              CRateCodes.Filtered := True;

              //cdsMDMaritalStatuses.CreateDataSet;
              MD_SY_STATES_NBR := DM_SYSTEM.SY_STATES.Lookup('STATE','MD' ,'SY_STATES_NBR');
              ms := DM_SYSTEM.SY_STATE_MARITAL_STATUS;
              ms.First;
              while not ms.Eof do
              begin
                if ms.SY_STATES_NBR.Value = MD_SY_STATES_NBR then
                  Append( cdsMDMaritalStatuses, 'STATUS_TYPE;STATUS_DESCRIPTION', [ms.STATUS_TYPE.Value, ms.STATUS_DESCRIPTION.Value]);
                ms.Next;
              end;

              CInfo.First;
              Append( cdsM3Cat, 'Category;number;desc', ['CDept1',1, CInfo['cc1Desc']] );
              Append( cdsM3Cat, 'Category;number;desc', ['CDept2',2, CInfo['cc2Desc']] );
              Append( cdsM3Cat, 'Category;number;desc', ['CDept3',3, CInfo['cc3Desc']] );
              Append( cdsM3Cat, 'Category;number;desc', ['CDept4',4, CInfo['cc4Desc']] );
              Append( cdsM3Cat, 'Category;number;desc', ['CDept5',5, CInfo['cc5Desc']] );

              Append( cdsEvoDBDT, 'DBDT;level', ['Division',1] );
              Append( cdsEvoDBDT, 'DBDT;level', ['Branch',2] );
              Append( cdsEvoDBDT, 'DBDT;level', ['Department',3] );
              Append( cdsEvoDBDT, 'DBDT;level', ['Team',4] );

            finally
              t.Free;
            end;

          except
            CloseConnection;
            raise;
          end;
          if EDs.Active then
          begin
            if EDs.FindField('eord') <> nil then
              EDs.FindField('eord').DisplayWidth := 10;
            if EDs.FindField('code') <> nil then
              EDs.FindField('code').DisplayWidth := 10;
          end;
        finally
          ctx_EndWait;
        end;
      except
        FLogger.PassthroughExceptionAndWarnFmt( sImportFailedToOpenConnection, [] );
      end;
    finally
      FLogger.LogExit;
    end;
  end;
end;

procedure TDM_IM_M3.SetLogger(aLogger: IImportLogger);
begin
  FLogger := aLogger;
end;

procedure TDM_IM_M3.CRateCodesFilterRecord(DataSet: TDataSet; var Accept: Boolean);
var
  i, err: Integer;
begin
  Val( Trim(DataSet['rateCode']), i, err);
  Accept := (err <> 0){not a number} or (i < 2);
end;

procedure TDM_IM_M3.FilterByEmpStatus(bFilter: boolean; status: Variant);
var
  i: integer;
begin
  if bFilter then
    if VarIsArray( status ) then
    begin
      SetLength(FEmpStatus, VarArrayHighBound(status, 1) );
      for i := 0 to VarArrayHighBound(status, 1) do
        FEmpStatus[i] := status[i];
    end
    else
    begin
      if VarIsNull(status) or VarIsEmpty(status) then
        raise EevException.Create('Null or Empty EmpStatus filter');
      SetLength(FEmpStatus, 1);
      FEmpStatus[0] := status;
    end
  else
    SetLength(FEmpStatus, 0);

  EInfo.Filtered := false;
  ETax.Filtered := false;
  ERate.Filtered := false;
  EDirDep.Filtered := false;
  EDed.Filtered := false;
  EEarn.Filtered := false;
  EYTD.Filtered := false;

  EInfo.Filtered := bFilter;
  ETax.Filtered := bFilter;
  ERate.Filtered := bFilter;
  EDirDep.Filtered := bFilter;
  EDed.Filtered := bFilter;
  EEarn.Filtered := bFilter;
  EYTD.Filtered := bFilter;
end;

procedure TDM_IM_M3.EmpStatusFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := InSet( DataSet.FieldByName('empStatus').AsString, FEmpStatus );
end;

const
  sHostAlias = 'Host';
  sInitialCatalogAlias = 'InitialCatalog';
  sUserIdAlias = 'UserId';
  sPasswordAlias = 'Password';
  sCompanyAlias = 'Company';

function ParseConnectionString( aConnStr: string ): TM3ConnectionInfo;
var
  sl: TStringList;
begin
  sl := TStringList.Create;
  try
    try
      sl.Text := StringReplace( aConnStr, ';', #13, [rfReplaceAll]);
      Result.Host := sl.Values[sHostAlias];
      Result.InitialCatalog := sl.Values[sInitialCatalogAlias];
      Result.UserId := sl.Values[sUserIdAlias];
      Result.Password := sl.Values[sPasswordAlias];
      Result.Company := sl.Values[sCompanyAlias];
    except
    end;
  finally
    FreeAndNil( sl );
  end;
end;

function ComposeConnectionString( info: TM3ConnectionInfo ): string;
var
  sl: TStringList;
begin
  Result := '';
  sl := TStringList.Create;
  try
    try
      if trim(info.Host) <> '' then
        sl.Values[sHostAlias] := info.Host;
      if trim(info.InitialCatalog) <> '' then
        sl.Values[sInitialCatalogAlias] := info.InitialCatalog;
      if trim(info.UserId) <> '' then
        sl.Values[sUserIdAlias] := info.UserId;
      if trim(info.Password) <> '' then
        sl.Values[sPasswordAlias] := info.Password;
      if trim(info.Company) <> '' then
        sl.Values[sCompanyAlias] := info.Company;
      Result := sl.Text;
    except
    end;
  finally
    FreeAndNil( sl );
  end;
end;

function ComposeADOConnectionString( info: TM3ConnectionInfo ): string;
begin
//  Result := Format('Provider=SQLOLEDB.1;Password=%s;Persist Security Info=True;User ID=%s;Initial Catalog=%s;Data Source=%s',
//                   [info.Password, info.UserId, info.InitialCatalog, info.Host]);
//  Result := 'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=millennium;Data Source=home';
  Result := 'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=millennium';
end;

procedure TDM_IM_M3.FilterLiabilities(bFilter: boolean;
  afterDate: TDateTime);
begin
  FLiabilityDate := afterDate;
  CTaxLiab.Filtered := false;
  CTaxLiab.Filtered := bFilter;
end;

procedure TDM_IM_M3.CTaxLiabFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := DataSet.FieldByName('liabilityDate').AsDateTime >= FLiabilityDate;
end;

end.
