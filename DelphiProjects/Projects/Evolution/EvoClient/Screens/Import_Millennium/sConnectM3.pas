// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sConnectM3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sImportVisualController, StdCtrls, ISBasicClasses, 
  Buttons, SDM_IM_M3, EvUIComponents;

type
  TConnectM3Frame = class(TFrame,IConnectionStringInputFrame)
    evButton1: TevButton;
    edHost: TevEdit;
    edDatabase: TevEdit;
    edUserId: TevEdit;
    edPassword: TevEdit;
    edCompany: TevEdit;
    lblHost: TevLabel;
    lblUserId: TevLabel;
    lblPassword: TevLabel;
    lblDatabase: TevLabel;
    lblCompany: TevLabel;
    bbCompany: TevBitBtn;
    procedure evButton1Click(Sender: TObject);
    procedure bbCompanyClick(Sender: TObject);
  private
    FController: IImportVisualController;
    function BuildConnectionInfo: TM3ConnectionInfo;
    procedure FillControls( info: TM3ConnectionInfo );
    {IConnectionStringInputFrame}
    procedure ConnectionStringChanged;
    procedure SetController( aController: IImportVisualController );
    procedure SetDefaultConnectionString( aDefConnStr: string );
  end;

implementation

uses
  sM3CompanyList;
{$R *.dfm}

{ TConnectMSSQLDatabaseFrame }

procedure TConnectM3Frame.ConnectionStringChanged;
begin
  if assigned(FController) and (trim(FController.ConnectionString)<>'') then
    FillControls( ParseConnectionString( FController.ConnectionString ) );
end;

procedure TConnectM3Frame.SetController(
  aController: IImportVisualController);
begin
  FController := AController;
  ConnectionStringChanged;
end;

procedure TConnectM3Frame.SetDefaultConnectionString(
  aDefConnStr: string);
begin
  FillControls( ParseConnectionString( aDefConnStr ) );
end;

procedure TConnectM3Frame.evButton1Click(Sender: TObject);
begin
  FController.SetConnectionString( ComposeConnectionString( BuildConnectionInfo ) );
end;

function TConnectM3Frame.BuildConnectionInfo: TM3ConnectionInfo;
begin
  Result.Host := edHost.Text;
  Result.InitialCatalog := edDatabase.Text;
  Result.UserId := edUserId.Text;
  Result.Password := edPassword.Text;
  Result.Company := edCompany.Text;
end;

procedure TConnectM3Frame.FillControls(info: TM3ConnectionInfo);
begin
  edHost.Text := info.Host;
  edDatabase.Text := info.InitialCatalog;
  edUserId.Text := info.UserId;
  edPassword.Text := info.Password;
  edCompany.Text := info.Company;
end;

procedure TConnectM3Frame.bbCompanyClick(Sender: TObject);
begin
  with TM3CompaniesForm.Create(Self) do
  try
    ACCompany.ConnectionString := ComposeADOConnectionString( BuildConnectionInfo );
    ACCompany.Connected := true;
    Cinfo.Active := true;
    if ShowModal = mrOk then
    begin
      edCompany.Text := Cinfo['co'];
    end;
  finally
    Free;
  end;
end;

end.
