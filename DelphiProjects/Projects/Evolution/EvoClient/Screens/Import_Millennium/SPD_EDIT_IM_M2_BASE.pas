// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_M2_BASE;

interface

uses
  Windows, SPD_EDIT_IM_BASE, Dialogs, ImgList, Controls, ISBasicClasses,
   Classes, ActnList, DB, Wwdatsrc, sImportQueue,
  sImportLoggerView, StdCtrls, Forms, sTablespaceView, Buttons, ExtCtrls,
  ComCtrls, sConnectDatabaseByFileName, EvUIComponents, LMDCustomButton,
  LMDButton, isUILMDButton;

type
  TEDIT_IM_M2_BASE = class(TEDIT_IM_BASE )
    ConnectDatabaseFileNameFrame: TConnectDatabaseFileNameFrame;
    procedure ConnectDatabaseFileNameFrameBitBtn1Click(Sender: TObject);
  end;

implementation

{$R *.DFM}

{ TEDIT_IM_ADP_BASE }


procedure TEDIT_IM_M2_BASE.ConnectDatabaseFileNameFrameBitBtn1Click(
  Sender: TObject);
begin
  inherited;
  ConnectDatabaseFileNameFrame.BitBtn1Click(Sender);
end;

end.
