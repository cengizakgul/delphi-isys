object DM_IM_M2: TDM_IM_M2
  OldCreateOrder = False
  Left = 354
  Top = 82
  Height = 676
  Width = 557
  object ACCompany: TADOConnection
    ConnectionString = 
      'Data Source=E:\job\ISystems\db\m2\3177.mdb;Jet OLEDB:Database Pa' +
      'ssword=admin'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 40
    Top = 24
  end
  object CDeptTied: TADOTable
    Connection = ACCompany
    CursorType = ctStatic
    IndexFieldNames = 'cc1;cc2;cc3;cc4;cc5'
    TableName = 'CDeptTied'
    Left = 160
    Top = 24
  end
  object CDeptUntied: TADOTable
    Connection = ACCompany
    CursorType = ctStatic
    TableName = 'CDeptUntied'
    Left = 224
    Top = 24
  end
  object CInfo: TADOTable
    Connection = ACCompany
    CursorType = ctStatic
    TableName = 'CInfo'
    Left = 104
    Top = 24
  end
  object Ethnicity: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'Ethnicity'#39' and code<>'#39#39)
    Left = 336
    Top = 16
  end
  object cdsEthnicity: TevClientDataSet
    Left = 400
    Top = 16
    object cdsEthnicityCode: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object cdsEthnicityName: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object EmpStatus: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'Emp Status'#39' and code<>'#39#39)
    Left = 336
    Top = 64
  end
  object cdsEmpStatus: TevClientDataSet
    Left = 400
    Top = 64
    object StringField1: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField2: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object PayFreq: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select frequency, description from CFreq')
    Left = 336
    Top = 112
  end
  object cdsPayFreq: TevClientDataSet
    Left = 400
    Top = 112
    object StringField3: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField4: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object EmpType: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'Emp Type'#39' and code<>'#39#39)
    Left = 336
    Top = 160
  end
  object cdsEmpType: TevClientDataSet
    Left = 400
    Top = 160
    object StringField5: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField6: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object CoTaxes: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select tcode, description from CTax')
    Left = 336
    Top = 208
  end
  object cdsCoTaxes: TevClientDataSet
    Left = 400
    Top = 208
    object cdsCoTaxesType: TStringField
      FieldName = 'Type'
      Size = 1
    end
    object StringField7: TStringField
      FieldName = 'Code'
      Size = 256
    end
    object cdsCoTaxesSubType: TStringField
      FieldName = 'SubType'
      Size = 2
    end
    object StringField8: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object EInfo: TADOTable
    Connection = ACCompany
    CursorType = ctStatic
    TableName = 'EInfo'
    Left = 104
    Top = 88
    object EInfos_Generation: TIntegerField
      FieldName = 's_Generation'
    end
    object EInfos_Lineage: TBlobField
      FieldName = 's_Lineage'
    end
    object EInfoco: TWideStringField
      FieldName = 'co'
      Size = 10
    end
    object EInfoid: TWideStringField
      FieldName = 'id'
      Size = 9
    end
    object EInfolastName: TWideStringField
      FieldName = 'lastName'
      Size = 40
    end
    object EInfofirstName: TWideStringField
      FieldName = 'firstName'
      Size = 40
    end
    object EInfomiddleName: TWideStringField
      FieldName = 'middleName'
    end
    object EInfosalutation: TWideStringField
      FieldName = 'salutation'
      Size = 10
    end
    object EInfosurname: TWideStringField
      FieldName = 'surname'
      Size = 10
    end
    object EInfonickname: TWideStringField
      FieldName = 'nickname'
      Size = 15
    end
    object EInfopriorLastName: TWideStringField
      FieldName = 'priorLastName'
      Size = 50
    end
    object EInfoaddress1: TWideStringField
      FieldName = 'address1'
      Size = 40
    end
    object EInfoaddress2: TWideStringField
      FieldName = 'address2'
      Size = 40
    end
    object EInfocity: TWideStringField
      FieldName = 'city'
      Size = 40
    end
    object EInfostate: TWideStringField
      FieldName = 'state'
      Size = 5
    end
    object EInfozip: TWideStringField
      FieldName = 'zip'
      Size = 10
    end
    object EInfocounty: TWideStringField
      FieldName = 'county'
      Size = 30
    end
    object EInfocountry: TWideStringField
      FieldName = 'country'
      Size = 30
    end
    object EInfohomePhone: TWideStringField
      FieldName = 'homePhone'
      Size = 15
    end
    object EInfossn: TWideStringField
      FieldName = 'ssn'
      Size = 11
    end
    object EInfobirthDate: TDateTimeField
      FieldName = 'birthDate'
    end
    object EInfosex: TWideStringField
      FieldName = 'sex'
      Size = 10
    end
    object EInfoethnicity: TWideStringField
      FieldName = 'ethnicity'
      Size = 1
    end
    object EInfomaritalStatus: TWideStringField
      FieldName = 'maritalStatus'
      Size = 10
    end
    object EInfodisability: TBooleanField
      FieldName = 'disability'
    end
    object EInfodisabilityDesc: TWideStringField
      FieldName = 'disabilityDesc'
      Size = 50
    end
    object EInfoveteran: TBooleanField
      FieldName = 'veteran'
    end
    object EInfoveteranDesc: TWideStringField
      FieldName = 'veteranDesc'
      Size = 50
    end
    object EInfosmoker: TBooleanField
      FieldName = 'smoker'
    end
    object EInfoempStatus: TWideStringField
      FieldName = 'empStatus'
      Size = 10
    end
    object EInfoempType: TWideStringField
      FieldName = 'empType'
      Size = 10
    end
    object EInfopayGroup: TWideStringField
      FieldName = 'payGroup'
      Size = 50
    end
    object EInfohireDate: TDateTimeField
      FieldName = 'hireDate'
    end
    object EInforehireDate: TDateTimeField
      FieldName = 'rehireDate'
    end
    object EInfoadjSeniorityDate: TDateTimeField
      FieldName = 'adjSeniorityDate'
    end
    object EInfotermDate: TDateTimeField
      FieldName = 'termDate'
    end
    object EInfotermReason: TWideStringField
      FieldName = 'termReason'
      Size = 10
    end
    object EInfocc: TWideStringField
      FieldName = 'cc'
      Size = 60
    end
    object EInfocc1: TWideStringField
      FieldName = 'cc1'
      Size = 10
    end
    object EInfocc2: TWideStringField
      FieldName = 'cc2'
      Size = 10
    end
    object EInfocc3: TWideStringField
      FieldName = 'cc3'
      Size = 10
    end
    object EInfocc4: TWideStringField
      FieldName = 'cc4'
      Size = 10
    end
    object EInfocc5: TWideStringField
      FieldName = 'cc5'
      Size = 10
    end
    object EInfopositionCode: TWideStringField
      FieldName = 'positionCode'
      Size = 30
    end
    object EInfopositionInfoLocked: TBooleanField
      FieldName = 'positionInfoLocked'
    end
    object EInfotitle: TWideStringField
      FieldName = 'title'
      Size = 30
    end
    object EInfoeeoClass: TWideStringField
      FieldName = 'eeoClass'
      Size = 10
    end
    object EInfowcc: TWideStringField
      FieldName = 'wcc'
      Size = 10
    end
    object EInfoflsaOtExempt: TBooleanField
      FieldName = 'flsaOtExempt'
    end
    object EInfoworkPhone: TWideStringField
      FieldName = 'workPhone'
      Size = 15
    end
    object EInfoworkPhoneExt: TWideStringField
      FieldName = 'workPhoneExt'
      Size = 5
    end
    object EInfomailStop: TWideStringField
      FieldName = 'mailStop'
      Size = 10
    end
    object EInfoemailAddress: TWideStringField
      FieldName = 'emailAddress'
      Size = 50
    end
    object EInfoclock: TWideStringField
      FieldName = 'clock'
      Size = 9
    end
    object EInfomiscCheck1: TBooleanField
      FieldName = 'miscCheck1'
    end
    object EInfomiscCheck2: TBooleanField
      FieldName = 'miscCheck2'
    end
    object EInfomiscCheck3: TBooleanField
      FieldName = 'miscCheck3'
    end
    object EInfomiscCheck4: TBooleanField
      FieldName = 'miscCheck4'
    end
    object EInfomiscCheck5: TBooleanField
      FieldName = 'miscCheck5'
    end
    object EInfotaxForm: TWideStringField
      FieldName = 'taxForm'
      Size = 5
    end
    object EInfopension: TBooleanField
      FieldName = 'pension'
    end
    object EInfostatutory: TBooleanField
      FieldName = 'statutory'
    end
    object EInfodeceased: TBooleanField
      FieldName = 'deceased'
    end
    object EInfodeferredComp: TBooleanField
      FieldName = 'deferredComp'
    end
    object EInfolegalRep: TBooleanField
      FieldName = 'legalRep'
    end
    object EInfodomesticEmpl: TBooleanField
      FieldName = 'domesticEmpl'
    end
    object EInfoseasonal: TBooleanField
      FieldName = 'seasonal'
    end
    object EInfoi9Verified: TBooleanField
      FieldName = 'i9Verified'
    end
    object EInfoi9ReVerify: TDateTimeField
      FieldName = 'i9ReVerify'
    end
    object EInfocitizenship: TWideStringField
      FieldName = 'citizenship'
      Size = 30
    end
    object EInfovisaType: TWideStringField
      FieldName = 'visaType'
      Size = 10
    end
    object EInfovisaExpiration: TDateTimeField
      FieldName = 'visaExpiration'
    end
    object EInfounion: TWideStringField
      FieldName = 'union'
      Size = 10
    end
    object EInfounionDate: TDateTimeField
      FieldName = 'unionDate'
    end
    object EInfounionInitFees: TBooleanField
      FieldName = 'unionInitFees'
    end
    object EInfounionDues: TBooleanField
      FieldName = 'unionDues'
    end
    object EInfosupervisorId: TWideStringField
      FieldName = 'supervisorId'
      Size = 9
    end
    object EInfosupervisorName: TWideStringField
      FieldName = 'supervisorName'
      Size = 25
    end
    object EInfopayGrade: TWideStringField
      FieldName = 'payGrade'
      Size = 10
    end
    object EInfobaseRate: TFloatField
      FieldName = 'baseRate'
    end
    object EInforatePer: TWideStringField
      FieldName = 'ratePer'
      Size = 10
    end
    object EInfosalary: TFloatField
      FieldName = 'salary'
    end
    object EInfodefaultHours: TFloatField
      FieldName = 'defaultHours'
    end
    object EInfopayFrequency: TWideStringField
      FieldName = 'payFrequency'
      Size = 10
    end
    object EInfoannualSalary: TFloatField
      FieldName = 'annualSalary'
    end
    object EInfoautoPay: TWideStringField
      FieldName = 'autoPay'
      Size = 10
    end
    object EInfolastRaiseDate: TDateTimeField
      FieldName = 'lastRaiseDate'
    end
    object EInfolastRaiseAmount: TFloatField
      FieldName = 'lastRaiseAmount'
    end
    object EInfolastRaiseReason: TWideStringField
      FieldName = 'lastRaiseReason'
      Size = 10
    end
    object EInfonextRaiseDate: TDateTimeField
      FieldName = 'nextRaiseDate'
    end
    object EInfolastReviewDate: TDateTimeField
      FieldName = 'lastReviewDate'
    end
    object EInfolastReviewRating: TWideStringField
      FieldName = 'lastReviewRating'
      Size = 10
    end
    object EInfonextReviewDate: TDateTimeField
      FieldName = 'nextReviewDate'
    end
    object EInfouser1: TWideStringField
      FieldName = 'user1'
      Size = 50
    end
    object EInfouser2: TWideStringField
      FieldName = 'user2'
      Size = 50
    end
    object EInfouser3: TWideStringField
      FieldName = 'user3'
      Size = 50
    end
    object EInfouser4: TWideStringField
      FieldName = 'user4'
      Size = 50
    end
    object EInfouser5: TWideStringField
      FieldName = 'user5'
      Size = 50
    end
    object EInfouser6: TWideStringField
      FieldName = 'user6'
      Size = 50
    end
    object EInfouser7: TWideStringField
      FieldName = 'user7'
      Size = 50
    end
    object EInfouser8: TWideStringField
      FieldName = 'user8'
      Size = 50
    end
    object EInfoee401kDeferral: TFloatField
      FieldName = 'ee401kDeferral'
    end
    object EInfoee401kCalc: TWideStringField
      FieldName = 'ee401kCalc'
      Size = 10
    end
    object EInfoee401kContinue: TBooleanField
      FieldName = 'ee401kContinue'
    end
    object EInfoee401kEligibleDate: TDateTimeField
      FieldName = 'ee401kEligibleDate'
    end
    object EInfoee401kStatus: TWideStringField
      FieldName = 'ee401kStatus'
      Size = 50
    end
    object EInfoee401kSuspendDate: TDateTimeField
      FieldName = 'ee401kSuspendDate'
    end
    object EInfoer401kMatch: TFloatField
      FieldName = 'er401kMatch'
    end
    object EInfohighComp: TBooleanField
      FieldName = 'highComp'
    end
    object EInfoowner: TBooleanField
      FieldName = 'owner'
    end
    object EInfoownerPercent: TFloatField
      FieldName = 'ownerPercent'
    end
    object EInfoownerRelated: TBooleanField
      FieldName = 'ownerRelated'
    end
    object EInfoownerSSN: TWideStringField
      FieldName = 'ownerSSN'
      Size = 11
    end
    object EInfojobCode: TWideStringField
      FieldName = 'jobCode'
      Size = 50
    end
    object EInfos_GUID: TGuidField
      FieldName = 's_GUID'
      FixedChar = True
      Size = 38
    end
    object EInfotipped: TWideStringField
      FieldName = 'tipped'
      Size = 5
    end
    object EInfoshift: TWideStringField
      FieldName = 'shift'
      Size = 255
    end
    object EInfoPayGradeDesc: TStringField
      FieldKind = fkLookup
      FieldName = 'PayGradeDesc'
      LookupDataSet = PayGrade
      LookupKeyFields = 'code'
      LookupResultField = 'description'
      KeyFields = 'payGrade'
      Size = 80
      Lookup = True
    end
  end
  object RatePer: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'Rate Per'#39' and code<>'#39#39)
    Left = 336
    Top = 256
  end
  object cdsRatePer: TevClientDataSet
    Left = 400
    Top = 256
    object StringField9: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField10: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object PayGrade: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'Pay Grade'#39' and code<>'#39#39)
    Left = 488
    Top = 16
  end
  object EEO: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'EEO Class'#39' and code<>'#39#39)
    Left = 488
    Top = 80
  end
  object ETax: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from ETax t, EInfo e, CTax c'
      'where t.id=e.id and t.tcode=c.tcode'
      'order by c.taxType')
    Left = 160
    Top = 88
  end
  object ERate: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from ERate t, EInfo e'
      'where t.id=e.id')
    Left = 216
    Top = 88
  end
  object RateCode: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = RateCodeFilterRecord
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'Rate Code'#39' and code<>'#39#39)
    Left = 336
    Top = 312
  end
  object cdsRateCode: TevClientDataSet
    Left = 400
    Top = 312
    object cdsRateCodeNumber: TIntegerField
      FieldName = 'Number'
    end
  end
  object Reciprocal: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'Reciprocal'#39' and code<>'#39#39)
    Left = 336
    Top = 360
  end
  object cdsReciprocal: TevClientDataSet
    Left = 400
    Top = 360
    object StringField11: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField12: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object EDs: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '#39'E'#39' as eord, ecode as code, description from cearn'
      'union '
      'select '#39'D'#39' as eord, dcode, description from cded ')
    Left = 488
    Top = 144
  end
  object EDirDep: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from EDirDep t, EInfo e'
      'where t.id=e.id')
    Left = 104
    Top = 144
  end
  object EDed: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from EDed t, EInfo e'
      'where t.id=e.id'
      'order by t.id, t.dcode, t.startdate desc')
    Left = 160
    Top = 144
  end
  object EEarn: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from EEarn t, EInfo e'
      'where t.id=e.id'
      'order by t.id, t.ecode, t.startdate desc')
    Left = 216
    Top = 144
  end
  object DedCalc: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'Ded Calc'#39' and code<>'#39#39)
    Left = 336
    Top = 416
  end
  object cdsDedCalc: TevClientDataSet
    Left = 400
    Top = 416
    object StringField13: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField14: TStringField
      FieldName = 'Name'
      Size = 40
    end
    object cdsDedCalcTargetField: TStringField
      FieldName = 'TargetField'
      Size = 10
    end
  end
  object EarnCalc: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select code, description from CTbl '
      'where tblId='#39'Earn Calc'#39' and code<>'#39#39)
    Left = 336
    Top = 464
  end
  object cdsEarnCalc: TevClientDataSet
    Left = 400
    Top = 464
    object StringField15: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField16: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object Agencies: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select id, name from c3pcheck')
    Left = 488
    Top = 200
  end
  object EYtd: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Year'
        DataType = ftInteger
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from EYtd t, EInfo e'
      'where t.id=e.id and Year=:Year')
    Left = 104
    Top = 200
  end
  object EDirDepNum: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select t.id, e.empStatus, count(t.id) as DDNum'
      'from EDirDep t, EInfo e'
      'where t.id=e.id'
      'group by t.id, e.empStatus'
      'order by count(t.id) desc')
    Left = 104
    Top = 264
  end
  object CoMDLocals: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select tcode, description from CTax where taxType = '#39'local'#39' and ' +
        'tcode like '#39'MD%'#39)
    Left = 312
    Top = 520
  end
  object cdsMDMaritalStatuses: TevClientDataSet
    Left = 416
    Top = 520
    object cdsMDMaritalStatusesSTATUS_TYPE: TStringField
      FieldName = 'STATUS_TYPE'
      Size = 2
    end
    object cdsMDMaritalStatusesSTATUS_DESCRIPTION: TStringField
      FieldName = 'STATUS_DESCRIPTION'
      Size = 40
    end
  end
  object CTaxLiab: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = CTaxLiabFilterRecord
    Parameters = <>
    SQL.Strings = (
      
        'select c.*, c.transactiontype as transtype from CTaxLiability c ' +
        'where c.transactiontype = '#39'L'#39' '
      '')
    Left = 104
    Top = 328
  end
  object CAllDept: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select d1.cc1, s2.cc2, s2.cc3, s2.cc4, s2.cc5,'
      'd1.name as name1, s2.name2, s2.name3,  s2.name4, s2.name5'
      'from CDeptUntied d1 left outer join  '
      ''
      '(select d2.cc2, s3.cc3, s3.cc4, s3.cc5,'
      'd2.name as name2, s3.name3,  s3.name4, s3.name5'
      'from CDeptUntied d2 left outer join'
      ''
      '(select d3.cc3, s4.cc4, s4.cc5,'
      'd3.name as name3, s4.name4, s4.name5'
      'from CDeptUntied d3 left outer join'
      ''
      '(select d4.cc4, s5.cc5, '
      'd4.name as name4, s5.name5'
      'from CDeptUntied d4 left outer join'
      ''
      '(select d5.cc5, '
      'd5.name as name5'
      'from CdeptTied d5 '
      'where not d5.cc5 is null) s5 '
      ''
      'on (d4.cc4 = s5.cc5) or (1=1) where not d4.cc4 is null) s4'
      ''
      'on  (d3.cc3 = s4.cc4) or (1=1) where not d3.cc3 is null ) s3 '
      ''
      'on (d2.cc2 = s3.cc3) or (1=1) where not d2.cc2 is null) s2'
      ''
      'on (d1.cc1 = s2.cc2) or (1=1) where not d1.cc1 is null')
    Left = 104
    Top = 496
  end
  object cdsM3Cat: TevClientDataSet
    FieldDefs = <
      item
        Name = 'Category'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'number'
        DataType = ftInteger
      end>
    Left = 48
    Top = 448
    object StringField18: TStringField
      FieldName = 'Category'
      Size = 10
    end
    object cdsM3Catnumber: TIntegerField
      FieldName = 'number'
    end
    object cdsM3Catdesc: TStringField
      FieldKind = fkCalculated
      FieldName = 'desc'
      Size = 10
      Calculated = True
    end
  end
  object cdsEvoDBDT: TevClientDataSet
    Left = 160
    Top = 448
    object cdsEvoDBDTDBDT: TStringField
      FieldName = 'DBDT'
      Size = 10
    end
    object cdsEvoDBDTlevel: TIntegerField
      FieldName = 'level'
    end
  end
end
