// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_M2_CO_AND_EE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_IM_M2_BASE, ImgList, ISBasicClasses, 
  ActnList, DB, Wwdatsrc, sImportQueue, sImportLoggerView, StdCtrls,
  sTablespaceView, Buttons, ExtCtrls, ComCtrls, sConnectDatabaseByFileName,
  sImportVisualController, genericImport, SM2ImportDef, sMCommonImportDef, sMEEImportParam,
  gdyBinderVisual, EvUtils, SDataStructure, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_IM_M2_CO_AND_EE = class(TEDIT_IM_M2_BASE)
    ParamInput: TMEEImportParamFrame;
    tsEthnicity: TTabSheet;
    BinderFrame1: TBinderFrame;
    tsEmpStatus: TTabSheet;
    BinderFrame2: TBinderFrame;
    tsPayFreq: TTabSheet;
    BinderFrame3: TBinderFrame;
    tsEmpType: TTabSheet;
    BinderFrame4: TBinderFrame;
    tsTaxes: TTabSheet;
    BinderFrame5: TBinderFrame;
    tsRatePer: TTabSheet;
    BinderFrame6: TBinderFrame;
    tsEEO: TTabSheet;
    BinderFrame7: TBinderFrame;
    tsRateCode: TTabSheet;
    BinderFrame8: TBinderFrame;
    tsReciprocal: TTabSheet;
    BinderFrame9: TBinderFrame;
    tsEDs: TTabSheet;
    BinderFrame10: TBinderFrame;
    tsDedCalc: TTabSheet;
    tsEarnCalc: TTabSheet;
    BinderFrame11: TBinderFrame;
    BinderFrame12: TBinderFrame;
    tsAgencies: TTabSheet;
    BinderFrame13: TBinderFrame;
    pnlInfo: TevPanel;
    lblInfo: TevLabel;
    tsMDLocals: TTabSheet;
    BinderFrame14: TBinderFrame;
    tsDBDT: TTabSheet;
    BinderFrame15: TBinderFrame;
    pnlDBDTMapUsed: TevPanel;
    procedure ConnectDatabaseFileNameFrameBitBtn1Click(Sender: TObject);
  public
    procedure Activate; override;
  end;

implementation

{$R *.dfm}


procedure TEDIT_IM_M2_CO_AND_EE.Activate;
begin
  inherited;
  ParamInput.Activate;
end;

procedure TEDIT_IM_M2_CO_AND_EE.ConnectDatabaseFileNameFrameBitBtn1Click(
  Sender: TObject);
var
  ivc: IImportVisualController;
  aImport: IImport;
begin
//  ParamInput.Activate;
  aImport := CreateM2CO_AND_EEImport( LogFrame.LoggerKeeper.Logger );
  ivc := TImportVisualController.Create( LogFrame.LoggerKeeper, aImport, ParamInput, ViewerFrame );
  SetController( ivc );
  ivc.SetConnectionStringInputFrame( ConnectDatabaseFileNameFrame );

  inherited;

  ivc.BindingKeeper(sEthnicityBindingName).SetTables( aImport.DataModule.GetTable('Ethnicity'),  aImport.DataModule.GetTable('cdsEthnicity'));
  ivc.BindingKeeper(sEthnicityBindingName).SetVisualBinder( BinderFrame1 );

  ivc.BindingKeeper(sEmpStatusBindingName).SetTables( aImport.DataModule.GetTable('EmpStatus'),  aImport.DataModule.GetTable('cdsEmpStatus'));
  ivc.BindingKeeper(sEmpStatusBindingName).SetVisualBinder( BinderFrame2 );

  ivc.BindingKeeper(sPayFreqBindingName).SetTables( aImport.DataModule.GetTable('PayFreq'),  aImport.DataModule.GetTable('cdsPayFreq'));
  ivc.BindingKeeper(sPayFreqBindingName).SetVisualBinder( BinderFrame3 );

  ivc.BindingKeeper(sEmpTypeBindingName).SetTables( aImport.DataModule.GetTable('EmpType'),  aImport.DataModule.GetTable('cdsEmpType'));
  ivc.BindingKeeper(sEmpTypeBindingName).SetVisualBinder( BinderFrame4 );

  ivc.BindingKeeper(sTaxesBindingName).SetTables( aImport.DataModule.GetTable('CoTaxes'),  aImport.DataModule.GetTable('cdsCoTaxes'));
  ivc.BindingKeeper(sTaxesBindingName).SetVisualBinder( BinderFrame5 );

  ivc.BindingKeeper(sRatePerBindingName).SetTables( aImport.DataModule.GetTable('RatePer'),  aImport.DataModule.GetTable('cdsRatePer'));
  ivc.BindingKeeper(sRatePerBindingName).SetVisualBinder( BinderFrame6 );

  DM_SYSTEM.SY_HR_EEO.EnsureHasMetadata;
  ivc.BindingKeeper(sEEOBindingName).SetTables( aImport.DataModule.GetTable('EEO'), DM_SYSTEM.SY_HR_EEO );
  ivc.BindingKeeper(sEEOBindingName).SetVisualBinder( BinderFrame7 );

  ivc.BindingKeeper(sRateCodeBindingName).SetTables( aImport.DataModule.GetTable('RateCode'),  aImport.DataModule.GetTable('cdsRateCode'));
  ivc.BindingKeeper(sRateCodeBindingName).SetVisualBinder( BinderFrame8 );

  ivc.BindingKeeper(sReciprocalBindingName).SetTables( aImport.DataModule.GetTable('Reciprocal'),  aImport.DataModule.GetTable('cdsReciprocal'));
  ivc.BindingKeeper(sReciprocalBindingName).SetVisualBinder( BinderFrame9 );

  DM_COMPANY.CO_E_D_CODES.EnsureHasMetadata;
  ivc.BindingKeeper(sEDBindingName).SetTables( aImport.DataModule.GetTable('EDs'), DM_COMPANY.CO_E_D_CODES );
  ivc.BindingKeeper(sEDBindingName).SetVisualBinder( BinderFrame10 );

  ivc.BindingKeeper(sDedBindingName).SetTables( aImport.DataModule.GetTable('DedCalc'),  aImport.DataModule.GetTable('cdsDedCalc'));
  ivc.BindingKeeper(sDedBindingName).SetVisualBinder( BinderFrame11 );

  ivc.BindingKeeper(sEarnBindingName).SetTables( aImport.DataModule.GetTable('EarnCalc'),  aImport.DataModule.GetTable('cdsEarnCalc'));
  ivc.BindingKeeper(sEarnBindingName).SetVisualBinder( BinderFrame12 );

  DM_CLIENT.CL_AGENCY.EnsureHasMetadata;
  ivc.BindingKeeper(sAgencyBindingName).SetTables( aImport.DataModule.GetTable('Agencies'), DM_CLIENT.CL_AGENCY );
  ivc.BindingKeeper(sAgencyBindingName).SetVisualBinder( BinderFrame13 );

  ivc.BindingKeeper(sDBDTBindingName).SetTables( aImport.DataModule.GetTable('cdsM3Cat'), aImport.DataModule.GetTable('cdsEvoDBDT') );
  ivc.BindingKeeper(sDBDTBindingName).SetVisualBinder( BinderFrame15 );

  ivc.BindingKeeper(sMDLocalsBindingName).SetTables( aImport.DataModule.GetTable('CoMDLocals'), aImport.DataModule.GetTable('cdsMDMaritalStatuses') );
  ivc.BindingKeeper(sMDLocalsBindingName).SetVisualBinder( BinderFrame14 );

  with GetDDStat(aImport.DataModule) do
  begin
    lblInfo.Caption := msg;
    if need > has then
      lblInfo.Font.Color := clRed
    else
      lblInfo.ParentFont := true;
  end;

  pnlDBDTMapUsed.Visible := aImport.DataModule.GetTable('CInfo').FieldByName('CCLevelsTied').AsBoolean;
end;

initialization
  RegisterClass(TEDIT_IM_M2_CO_AND_EE);

end.
