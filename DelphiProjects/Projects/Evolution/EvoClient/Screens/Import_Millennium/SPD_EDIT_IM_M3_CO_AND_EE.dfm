inherited EDIT_IM_M3_CO_AND_EE: TEDIT_IM_M3_CO_AND_EE
  inherited PageControl1: TevPageControl
    ActivePage = TabSheet1
    MultiLine = True
    inherited TabSheet1: TTabSheet
      inherited evPanel1: TevPanel
        Width = 427
        Height = 201
        inherited evPanel5: TevPanel
          Top = 209
          Width = 427
          Height = 297
          Visible = True
          inherited EvBevel2: TEvBevel
            Height = 293
          end
          inherited pnlSetup: TevPanel
            Width = 296
            Height = 293
            inherited EvBevel1: TEvBevel
              Width = 296
            end
            inherited pnlSetupControls: TevPanel
              Width = 296
              Height = 254
              inline ParamInput: TMEEImportParamFrame
                Left = 8
                Top = 8
                Width = 553
                Height = 237
                TabOrder = 0
                inherited cbTerm: TevCheckBox
                  Left = 8
                  Top = 18
                end
                inherited gbLiab: TevGroupBox
                  Left = 184
                  Top = 0
                  Width = 353
                end
              end
            end
          end
        end
        inherited pnlConnectionInputHolder: TevPanel
          Width = 427
          Height = 209
          inherited ConnectMSSQLDatabaseFrame: TConnectM3Frame
            Width = 423
            Height = 175
            Align = alTop
          end
          object pnlInfo: TevPanel
            Left = 2
            Top = 177
            Width = 423
            Height = 30
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object lblInfo: TevLabel
              Left = 8
              Top = 9
              Width = 3
              Height = 13
            end
          end
        end
      end
    end
    inherited TabSheet5: TTabSheet
      inherited viewerFrame: TTableSpaceViewFrame
        Height = 201
        inherited evPanel2: TevPanel
          Height = 160
          inherited dgView: TevDBGrid
            Height = 411
          end
        end
      end
    end
    object DBDT: TTabSheet [2]
      Caption = 'DBDT'
      ImageIndex = 16
      inline BinderFrame14: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 224
          end
          inherited pnlTopLeft: TevPanel
            Height = 224
            inherited dgLeft: TevDBGrid
              Height = 199
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 486
            Height = 224
            inherited evPanel4: TevPanel
              Width = 486
            end
            inherited dgRight: TevDBGrid
              Width = 486
              Height = 199
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 756
            inherited evPanel5: TevPanel
              Width = 756
            end
            inherited dgBottom: TevDBGrid
              Width = 756
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 756
          end
        end
      end
    end
    object tsEthnicity: TTabSheet [3]
      Caption = 'Ethnicity'
      ImageIndex = 4
      inline BinderFrame1: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 163
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 148
            Height = 0
            inherited evPanel4: TevPanel
              Width = 148
            end
            inherited dgRight: TevDBGrid
              Width = 148
              Height = 163
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 418
            inherited evPanel5: TevPanel
              Width = 418
            end
            inherited dgBottom: TevDBGrid
              Width = 418
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 418
          end
        end
      end
    end
    object tsEmpStatus: TTabSheet [4]
      Caption = 'Emp Status'
      ImageIndex = 5
      inline BinderFrame2: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 148
          end
          inherited pnlTopLeft: TevPanel
            Height = 148
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 374
            Height = 148
            inherited evPanel4: TevPanel
              Width = 374
            end
            inherited dgRight: TevDBGrid
              Width = 374
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 644
            inherited evPanel5: TevPanel
              Width = 644
            end
            inherited dgBottom: TevDBGrid
              Width = 644
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 644
          end
        end
      end
    end
    object tsPayFreq: TTabSheet [5]
      Caption = 'Pay Frequency'
      ImageIndex = 6
      inline BinderFrame3: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 148
          end
          inherited pnlTopLeft: TevPanel
            Height = 148
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 374
            Height = 148
            inherited evPanel4: TevPanel
              Width = 374
            end
            inherited dgRight: TevDBGrid
              Width = 374
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 644
            inherited evPanel5: TevPanel
              Width = 644
            end
            inherited dgBottom: TevDBGrid
              Width = 644
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 644
          end
        end
      end
    end
    object tsEmpType: TTabSheet [6]
      Caption = 'Emp Type'
      ImageIndex = 7
      inline BinderFrame4: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 224
          end
          inherited pnlTopLeft: TevPanel
            Height = 224
            inherited dgLeft: TevDBGrid
              Height = 199
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 486
            Height = 224
            inherited evPanel4: TevPanel
              Width = 486
            end
            inherited dgRight: TevDBGrid
              Width = 486
              Height = 199
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 756
            inherited evPanel5: TevPanel
              Width = 756
            end
            inherited dgBottom: TevDBGrid
              Width = 756
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 756
          end
        end
      end
    end
    object tsTaxes: TTabSheet [7]
      Caption = 'Taxes'
      inline BinderFrame5: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 224
          end
          inherited pnlTopLeft: TevPanel
            Height = 224
            inherited dgLeft: TevDBGrid
              Height = 199
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 486
            Height = 224
            inherited evPanel4: TevPanel
              Width = 486
            end
            inherited dgRight: TevDBGrid
              Width = 486
              Height = 199
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 756
            inherited evPanel5: TevPanel
              Width = 756
            end
            inherited dgBottom: TevDBGrid
              Width = 756
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 756
          end
        end
      end
    end
    object tsMDLocals: TTabSheet [8]
      Caption = 'MD Locals'
      ImageIndex = 17
      inline BinderFrame12: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 272
          end
          inherited pnlTopLeft: TevPanel
            Height = 272
            inherited dgLeft: TevDBGrid
              Height = 247
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 438
            Height = 272
            inherited evPanel4: TevPanel
              Width = 438
            end
            inherited dgRight: TevDBGrid
              Width = 438
              Height = 247
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 708
            inherited evPanel5: TevPanel
              Width = 708
            end
            inherited dgBottom: TevDBGrid
              Width = 708
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 708
          end
        end
      end
    end
    object tsRatePer: TTabSheet [9]
      Caption = 'Rate Per'
      ImageIndex = 9
      inline BinderFrame6: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 224
          end
          inherited pnlTopLeft: TevPanel
            Height = 224
            inherited dgLeft: TevDBGrid
              Height = 199
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 486
            Height = 224
            inherited evPanel4: TevPanel
              Width = 486
            end
            inherited dgRight: TevDBGrid
              Width = 486
              Height = 199
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 756
            inherited evPanel5: TevPanel
              Width = 756
            end
            inherited dgBottom: TevDBGrid
              Width = 756
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 756
          end
        end
      end
    end
    object tsEEO: TTabSheet [10]
      Caption = 'EEO Class'
      ImageIndex = 10
      inline BinderFrame7: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 177
          end
          inherited pnlTopLeft: TevPanel
            Height = 177
            inherited dgLeft: TevDBGrid
              Height = 152
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 384
            Height = 177
            inherited evPanel4: TevPanel
              Width = 384
            end
            inherited dgRight: TevDBGrid
              Width = 384
              Height = 152
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 654
            inherited evPanel5: TevPanel
              Width = 654
            end
            inherited dgBottom: TevDBGrid
              Width = 654
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 654
          end
        end
      end
    end
    object tsRateCode: TTabSheet [11]
      Caption = 'Rate Code'
      ImageIndex = 11
      inline BinderFrame8: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 148
          end
          inherited pnlTopLeft: TevPanel
            Height = 148
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 381
            Height = 148
            inherited evPanel4: TevPanel
              Width = 381
            end
            inherited dgRight: TevDBGrid
              Width = 381
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 651
            inherited evPanel5: TevPanel
              Width = 651
            end
            inherited dgBottom: TevDBGrid
              Width = 651
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 651
          end
        end
      end
    end
    object tsReciprocal: TTabSheet [12]
      Caption = 'Reciprocal'
      ImageIndex = 12
      inline BinderFrame9: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 148
            Height = 0
            inherited evPanel4: TevPanel
              Width = 148
            end
            inherited dgRight: TevDBGrid
              Width = 148
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 418
            inherited evPanel5: TevPanel
              Width = 418
            end
            inherited dgBottom: TevDBGrid
              Width = 418
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 418
          end
        end
      end
    end
    object tsEDs: TTabSheet [13]
      Caption = 'E/Ds'
      ImageIndex = 13
      inline BinderFrame10: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 144
          end
          inherited pnlTopLeft: TevPanel
            Height = 144
            inherited dgLeft: TevDBGrid
              Height = 119
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 394
            Height = 144
            inherited evPanel4: TevPanel
              Width = 394
            end
            inherited dgRight: TevDBGrid
              Width = 394
              Height = 119
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 664
            inherited evPanel5: TevPanel
              Width = 664
            end
            inherited dgBottom: TevDBGrid
              Width = 664
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 664
          end
        end
      end
    end
    object tsEDCalc: TTabSheet [14]
      Caption = 'E/D Calc Type'
      ImageIndex = 14
      inline BinderFrame11: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 145
          end
          inherited pnlTopLeft: TevPanel
            Height = 145
            inherited dgLeft: TevDBGrid
              Height = 120
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 165
            Height = 145
            inherited evPanel4: TevPanel
              Width = 165
            end
            inherited dgRight: TevDBGrid
              Width = 165
              Height = 120
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 435
            inherited evPanel5: TevPanel
              Width = 435
            end
            inherited dgBottom: TevDBGrid
              Width = 435
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 435
          end
        end
      end
    end
    object tsAgencies: TTabSheet [15]
      Caption = 'Agencies'
      ImageIndex = 16
      inline BinderFrame13: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 120
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 213
            Height = 0
            inherited evPanel4: TevPanel
              Width = 213
            end
            inherited dgRight: TevDBGrid
              Width = 213
              Height = 120
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 483
            inherited evPanel5: TevPanel
              Width = 483
            end
            inherited dgBottom: TevDBGrid
              Width = 483
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 483
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited LogFrame: TImportLoggerViewFrame
        Height = 72
        inherited pnlLog: TevPanel
          Height = 72
          inherited LoggerRichView: TImportLoggerRichViewFrame
            Height = 72
            inherited pnlUserView: TevPanel
              Height = 72
              inherited pnlBottom: TevPanel
                Height = 139
                inherited evSplitter2: TevSplitter
                  Height = 139
                end
                inherited pnlLeft: TevPanel
                  Height = 139
                  inherited mmDetails: TevMemo
                    Height = 114
                  end
                end
                inherited pnlRight: TevPanel
                  Height = 139
                  inherited mmContext: TevMemo
                    Height = 114
                  end
                end
              end
            end
          end
        end
      end
    end
    inherited tbshQueue: TTabSheet
      inherited ImportQueueFrame: TImportQueueFrame
        Height = 201
        inherited evPanel2: TevPanel
          Height = 144
          inherited lvQueue: TevListView
            Height = 144
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 573
    Top = 50
  end
  inherited wwdsList: TevDataSource
    Left = 498
    Top = 18
  end
  inherited evActionList1: TevActionList
    Left = 368
    Top = 8
  end
end
