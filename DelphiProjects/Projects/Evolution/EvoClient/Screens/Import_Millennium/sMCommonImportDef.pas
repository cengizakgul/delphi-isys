unit sMCommonImportDef;

interface

uses
  genericImportDefinitionImpl, genericImport, classes, genericImportBaseImpl, 
  EvContext, isVCLBugFix, evExceptions, IEMap, EvCommonInterfaces, EvDataset, EvClientDataSet;

const
  sParamCheckDate = 'PR.CHECK_DATE';
  sParamBatchFrequency = 'PR_BATCH.FREQUENCY';
  sParamQuarter = 'Quarter';
  sParamNeedPrevQuarters = 'NeedPrevQuarters';

  sAdjustmentType = 'AdjustmentType';
//  sLiabStatus = 'LiabStatus';
  sDueDate = 'DueDate';
  sImpounded = 'Impounded';
  sProcessDate = 'ProcessDate';
  sOnlyThisYear = 'OnlyThisYear';

const
  sEthnicityBindingName = 'Ethnicity';
  sEmpStatusBindingName = 'Emp Status';
  sPayFreqBindingName = 'Pay Frequency';
  sEmpTypeBindingName = 'Emp Type';
  sTaxesBindingName = 'Taxes';
  sRatePerBindingName = 'Rate Per';
  sEEOBindingName = 'EEO Class';
  sRateCodeBindingName = 'Rate Code';
  sReciprocalBindingName = 'Reciprocal';
  sEDBindingName = 'EDs-new';
  sAgencyBindingName = 'Agencies';
  sMDLocalsBindingName = 'MDLocals';
  sDBDTBindingName = 'DBDT-new';

type
  TIntegerArray = array of integer;
  TVariantArray = array of Variant;

  IMillenniumImportDefinition = interface
['{AB983A8D-38A0-4464-86EA-583A19A847FC}']
    procedure BeforeEEStatesImport;
    procedure AfterEEStatesImport;
  end;

  TMillenniumImportDef = class (TImportDefinition, IMillenniumImportDefinition)
  private
    CheckLineNbr: Integer;

    FCreatedEEStates: TStringList;
    FEEStatesWithMaritalStatus: TStringList;

    procedure SetDBDTDefaults(const ds: TevClientDataSet);
    function MapED( eord: char; val: Variant): Variant;
    function FirstSyMaritalStatus(var SyStateMaritalStatusNbr: Integer): Variant;
  private
    {IMillenniumImportDefinition}
    procedure BeforeEEStatesImport;
    procedure AfterEEStatesImport;

  protected
    function MapDedCalcMethod( val: Variant): Variant; virtual; abstract;
    function MapEarnCalcMethod( val: Variant): Variant; virtual; abstract;

    procedure GetAmounts( const Input : TFieldValues; var amount: Variant; var hours: Variant );
    function EEEDs(const TableName: string; const Input, Keys, Output: TFieldValues; const value: Variant): boolean;
    procedure ImportDedRateAmount( calcCode, Value: Variant; Output: TFieldValues; import100percAsDeductWholeCheck: boolean );
    procedure ImportPrimaryRate(const Input, Keys, Output: TFieldValues);
    procedure ImportEEAdditionalInfo(const Input, Keys, Output: TFieldValues);
    procedure ImportEERate(const Input, Keys, Output: TFieldValues);

    function DBDTMap: TVariantArray;
    procedure HelpImportDBDT(const Input, Output: TFieldValues; m3table, desttable: string );
  public
    constructor Create(const AIEMap: IIEMap); override;
  published
    procedure E1_STATE(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);
    procedure PrenoteDate(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure EDirDep(const Input, Keys, Output: TFieldValues);
    procedure EETax(const Input, Keys, Output: TFieldValues);
    procedure CTaxLiab(const Input, Keys, Output: TFieldValues);
    procedure EEDed(const Input, Keys, Output: TFieldValues);
    procedure EEEarn(const Input, Keys, Output: TFieldValues);
    procedure Ytd(const Input, Keys, Output: TFieldValues);
    procedure EInfoSalary(const Input, Keys, Output: TFieldValues);
    procedure EInfoFix(const Input, Keys, Output: TFieldValues);
    procedure CAllDept(const Input, Keys, Output: TFieldValues);

    procedure AI_EE_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_SCHEDULED_E_DS(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_BATCH(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_LINES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_LOCALS(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_BRANCH(const ds: TevClientDataSet; const Keys: TFieldValues);
    procedure AI_CO_DEPARTMENT(const ds: TevClientDataSet; const Keys: TFieldValues);
    procedure AI_CO_DIVISION(const ds: TevClientDataSet; const Keys: TFieldValues);
    procedure AI_CO_TEAM(const ds: TevClientDataSet; const Keys: TFieldValues);
    procedure AI_CO_TAX_DEPOSITS(const ds: TevClientDataSet; const Keys: TFieldValues);
  end;

  TMillenniumImport = class (TImportBase)
  protected
    procedure RunPrImport( aImportDefinition: IImportDefinition; aParams: TStrings );
  end;

const
  sHiddenDivisionCode   = '     Hidden Division';
  sHiddenBranchCode     = '       Hidden Branch';
  sHiddenDepartmentCode = '   Hidden Department';


type
  TDDStat = record
    need: integer;
    has: integer;
    msg: string;
  end;

function GetDDStat(DataModule: IExternalDataModule): TDDStat;
function EmptyStrAsNull( s: string): Variant; //just to fix clumsy piece of code

implementation

uses
  variants, evconsts, simportHelpers, sysutils, db,
  sdatastructure, evutils, gdystrset, windows, gdyBinder,
  strutils, dateutils;

const
  MConversion: array [0..28] of TConvertDesc =
  (
    (Table: 'ETax'; Field:'id'; Func: ConvertEECustomNumber),
    (Table: 'EDirDep'; Field:'id'; Func: ConvertEECustomNumber),
    (Table: 'EDed'; Field:'id'; Func: ConvertEECustomNumber),
    (Table: 'EEarn'; Field:'id'; Func: ConvertEECustomNumber),
    (Table: 'EYtd'; Field:'id'; Func: ConvertEECustomNumber),
    (Table: 'EInfo'; Field:'id'; Func: ConvertEECustomNumber),
    (Table: 'EInfoSalary'; Field:'id'; Func: ConvertEECustomNumber),
    (Table: 'EInfoFix'; Field:'id'; Func: ConvertEECustomNumber),
    (Table: 'ERate'; Field:'id'; Func: ConvertEECustomNumber),

    (Table: 'CAllDept'; Field:'cc1'; Func: ConvertDBDTCustomNumber),
    (Table: 'CAllDept'; Field:'cc2'; Func: ConvertDBDTCustomNumber),
    (Table: 'CAllDept'; Field:'cc3'; Func: ConvertDBDTCustomNumber),
    (Table: 'CAllDept'; Field:'cc4'; Func: ConvertDBDTCustomNumber),
    (Table: 'CAllDept'; Field:'cc5'; Func: ConvertDBDTCustomNumber),

    (Table: 'EInfo'; Field:'cc1'; Func: ConvertDBDTCustomNumber),
    (Table: 'EInfo'; Field:'cc2'; Func: ConvertDBDTCustomNumber),
    (Table: 'EInfo'; Field:'cc3'; Func: ConvertDBDTCustomNumber),
    (Table: 'EInfo'; Field:'cc4'; Func: ConvertDBDTCustomNumber),
    (Table: 'EInfo'; Field:'cc5'; Func: ConvertDBDTCustomNumber),

    (Table: 'ERate'; Field:'cc1'; Func: ConvertDBDTCustomNumber),
    (Table: 'ERate'; Field:'cc2'; Func: ConvertDBDTCustomNumber),
    (Table: 'ERate'; Field:'cc3'; Func: ConvertDBDTCustomNumber),
    (Table: 'ERate'; Field:'cc4'; Func: ConvertDBDTCustomNumber),
    (Table: 'ERate'; Field:'cc5'; Func: ConvertDBDTCustomNumber),

    //avoid default conversion (trimming) -- these codes goes to mapping without trimming
    //so we must always get them untouched to be able to use mapping
    //!!Default conversion touches nearly all fields including ones used in mapping and we have to explicitly say that
    //these fields should go verbatim. Actually we should change import engine and extend this conversion to values going to mapping tool.
    (Table: 'EDed'; Field:'dcode'; Func: Verbatim),
    (Table: 'EEarn'; Field:'ecode'; Func: Verbatim),
    (Table: 'EYtd'; Field:'detcode'; Func: Verbatim),

    //These fields may have 0 as their value.
    //VarToStr and other functions that calls VarBstrFromDate under the hood give '12:00:00 AM' instead of '12/30/1899'
    //TDateField value (ftDate; there is also ftTime and ftDateTime) is converted
    //to Variant of type varDate (which actually means date-time and there is no special variant type for date)
    //and then this variant is converted to string.
    //So values of the fields below are converted to string manually using shortDateFormat (as TDateField.AsVariant does)
    (Table: 'EDed'; Field:'startDate'; Func: DateAsString),
    (Table: 'EEarn'; Field:'startDate'; Func: DateAsString)
  );

  MKeys: array [0..32] of TKeySourceDesc =
  (
    (ExternalTable: 'ETax'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'ETax'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'id' ),

    (ExternalTable: 'EDirDep'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EDirDep'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'id' ),
    (ExternalTable: 'EDirDep'; EvoKeyTable: 'EE_DIRECT_DEPOSIT'; EvoKeyField: 'EE_ABA_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'transit' ),
    (ExternalTable: 'EDirDep'; EvoKeyTable: 'EE_DIRECT_DEPOSIT'; EvoKeyField: 'EE_BANK_ACCOUNT_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'account' ),

    (ExternalTable: 'CTaxLiab'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),

    (ExternalTable: 'EDed'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EDed'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'id' ),
    (ExternalTable: 'EDed'; EvoKeyTable: 'EE_SCHEDULED_E_DS'; EvoKeyField: 'EFFECTIVE_START_DATE'; HowToGet: ksFromThisTable; ValueSourceField: 'startDate' ),

    (ExternalTable: 'EEarn'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EEarn'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'id' ),
    (ExternalTable: 'EEarn'; EvoKeyTable: 'EE_SCHEDULED_E_DS'; EvoKeyField: 'EFFECTIVE_START_DATE'; HowToGet: ksFromThisTable; ValueSourceField: 'startDate' ),


    (ExternalTable: 'EYtd'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EYtd'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'id' ),
    (ExternalTable: 'EYtd'; EvoKeyTable: 'PR'; EvoKeyField: 'CHECK_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EYtd'; EvoKeyTable: 'PR'; EvoKeyField: 'RUN_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EYtd'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_BEGIN_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EYtd'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_END_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EYtd'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'FREQUENCY'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EYtd'; EvoKeyTable: 'PR_CHECK'; EvoKeyField: 'CHECK_TYPE'; HowToGet: ksFixedValue ),

    (ExternalTable: 'EInfoFix'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EInfoFix'; EvoKeyTable: 'CL_PERSON'; EvoKeyField: 'SOCIAL_SECURITY_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'ssn' ),
    (ExternalTable: 'EInfoFix'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'id' ),

    (ExternalTable: 'EInfoSalary'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EInfoSalary'; EvoKeyTable: 'CL_PERSON'; EvoKeyField: 'SOCIAL_SECURITY_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'ssn' ),
    (ExternalTable: 'EInfoSalary'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'id' ),

    (ExternalTable: 'EInfo'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EInfo'; EvoKeyTable: 'CL_PERSON'; EvoKeyField: 'SOCIAL_SECURITY_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'ssn' ),
    (ExternalTable: 'EInfo'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'id' ),

    (ExternalTable: 'ERate'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'ERate'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'id' ),

    (ExternalTable: 'CAllDept'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue )

  );

  MRowImport: array [0..10] of TRowImportDesc =
  (
    ( ExternalTable: 'ETax'; CustomFunc: 'EETax'),
    ( ExternalTable: 'EDirDep'; CustomFunc: 'EDirDep'),
    ( ExternalTable: 'CTaxLiab'; CustomFunc: 'CTaxLiab'),
    ( ExternalTable: 'EDed'; CustomFunc: 'EEDed'),
    ( ExternalTable: 'EEarn'; CustomFunc: 'EEEarn'),
    ( ExternalTable: 'EYtd'; CustomFunc: 'Ytd'),
    ( ExternalTable: 'EInfoSalary'; CustomFunc: 'EInfoSalary'),
    ( ExternalTable: 'EInfoFix'; CustomFunc: 'EInfoFix'),
    ( ExternalTable: 'CAllDept'; CustomFunc: 'CAllDept'),
    ( ExternalTable: 'EInfo'; CustomFunc: 'EEAdditionalInfo'),
    ( ExternalTable: 'ERate'; CustomFunc: 'EERate')
  );

  MMap: array [0..36] of TMapRecord =
  (
// EE_DIRECT_DEPOSIT
    (ITable: 'EDirDep'; IField: 'ACCOUNT'; ETable: 'EE_DIRECT_DEPOSIT'; EField: 'EE_BANK_ACCOUNT_NUMBER'; MapType: mtDirect),
    (ITable: 'EDirDep'; IField: 'TRANSIT'; ETable: 'EE_DIRECT_DEPOSIT'; EField: 'EE_ABA_NUMBER'; MapType: mtDirect),
    (ITable: 'EDirDep'; IField: 'CHECKING'; ETable: 'EE_DIRECT_DEPOSIT'; EField: 'EE_BANK_ACCOUNT_TYPE'; MapType: mtMap;
                                  MapValues: 'True=' + RECEIVING_ACCT_TYPE_CHECKING + ',' +
                                             'False=' + RECEIVING_ACCT_TYPE_SAVING  ),
    (ITable: 'EDirDep'; IField: 'PRENOTEDATE'; MapType: mtCustom; FuncName: 'PrenoteDate'),

    //CL_PERSON
    (ITable: 'EInfo'; IField: 'SSN'; ETable: 'CL_PERSON'; EField: 'SOCIAL_SECURITY_NUMBER'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'LASTNAME'; ETable: 'CL_PERSON'; EField: 'LAST_NAME'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'FIRSTNAME'; ETable: 'CL_PERSON'; EField: 'FIRST_NAME'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'MIDDLENAME'; ETable: 'CL_PERSON'; EField: 'MIDDLE_INITIAL'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'SEX'; ETable: 'CL_PERSON'; EField: 'GENDER'; MapType: mtMap;
                                  MapValues: 'M=' + GROUP_BOX_MALE + ',' +
                                             'F=' + GROUP_BOX_FEMALE; MapDefault: GROUP_BOX_UNKOWN),
    (ITable: 'EInfo'; IField: 'BIRTHDATE'; ETable: 'CL_PERSON'; EField: 'BIRTH_DATE'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'ADDRESS1'; ETable: 'CL_PERSON'; EField: 'ADDRESS1'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'ADDRESS2'; ETable: 'CL_PERSON'; EField: 'ADDRESS2'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'CITY'; ETable: 'CL_PERSON'; EField: 'CITY'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'STATE'; MapType: mtCustom; FuncName: 'E1_STATE'),
    (ITable: 'EInfo'; IField: 'ZIP'; ETable: 'CL_PERSON'; EField: 'ZIP_CODE'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'COUNTY'; ETable: 'CL_PERSON'; EField: 'COUNTY'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'HOMEPHONE'; ETable: 'CL_PERSON'; EField: 'PHONE1'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'WORKPHONE'; ETable: 'CL_PERSON'; EField: 'PHONE2'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'EMAILADDRESS'; ETable: 'CL_PERSON'; EField: 'E_MAIL_ADDRESS'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'SMOKER'; ETable: 'CL_PERSON'; EField: 'SMOKER'; MapType: mtMap;
                                  MapValues: 'True=' + GROUP_BOX_YES
                                              + ',False=' + GROUP_BOX_NO),
    (ITable: 'EInfo'; IField: 'VETERAN'; ETable: 'CL_PERSON'; EField: 'VETERAN'; MapType: mtMap;
                                  MapValues: 'True=' + GROUP_BOX_YES
                                              + ',False=' + GROUP_BOX_NO),
    (ITable: 'EInfo'; IField: 'I9VERIFIED'; ETable: 'CL_PERSON'; EField: 'I9_ON_FILE'; MapType: mtMap;
                                  MapValues: 'True=' + GROUP_BOX_YES
                                              + ',False=' + GROUP_BOX_NO),
    //  EE
    (ITable: 'EInfo'; IField: 'ID'; ETable: 'EE'; EField: 'CUSTOM_EMPLOYEE_NUMBER'; MapType: mtDirect ),

    (ITable: 'EInfo'; IField: 'TAXFORM'; ETable: 'EE'; EField: 'COMPANY_OR_INDIVIDUAL_NAME'; MapType: mtMap;
                                                        MapValues: 'W2=' + GROUP_BOX_INDIVIDUAL + ',' +
                                                                   '1099M=' + GROUP_BOX_COMPANY + ',' +
                                                                   '1099R=' + GROUP_BOX_COMPANY;
                                                        MapDefault: GROUP_BOX_INDIVIDUAL),
    (ITable: 'EInfo'; IField: 'CLOCK'; ETable: 'EE'; EField: 'TIME_CLOCK_NUMBER'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'TERMDATE'; ETable: 'EE'; EField: 'CURRENT_TERMINATION_DATE'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'LASTREVIEWDATE'; ETable: 'EE'; EField: 'REVIEW_DATE'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'NEXTREVIEWDATE'; ETable: 'EE'; EField: 'NEXT_REVIEW_DATE'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'ANNUALSALARY'; ETable: 'EE'; EField: 'CALCULATED_SALARY'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'TIPPED'; ETable: 'EE'; EField: 'TIPPED_DIRECTLY'; MapType: mtMap;
                                                        MapValues: 'D=' + GROUP_BOX_YES;
                                                        MapDefault: GROUP_BOX_NO),
    (ITable: 'EInfo'; IField: 'SMOKER'; ETable: 'CL_PERSON'; EField: 'SMOKER'; MapType: mtMap;
                                  MapValues: 'True=' + GROUP_BOX_YES
                                              + ',False=' + GROUP_BOX_NO),
    (ITable: 'EInfo'; IField: 'STATUTORY'; ETable: 'EE'; EField: 'W2_STATUTORY_EMPLOYEE'; MapType: mtMap;
                                  MapValues: 'True=' + GROUP_BOX_YES
                                              + ',False=' + GROUP_BOX_NO),
    (ITable: 'EInfo'; IField: 'DECEASED'; ETable: 'EE'; EField: 'W2_DECEASED'; MapType: mtMap;
                                  MapValues: 'True=' + GROUP_BOX_YES
                                              + ',False=' + GROUP_BOX_NO),
    (ITable: 'EInfo'; IField: 'DEFERREDCOMP'; ETable: 'EE'; EField: 'W2_DEFERRED_COMP'; MapType: mtMap;
                                  MapValues: 'True=' + GROUP_BOX_YES
                                              + ',False=' + GROUP_BOX_NO),
    (ITable: 'EInfo'; IField: 'PENSION'; ETable: 'EE'; EField: 'W2_PENSION'; MapType: mtMap;
                                  MapValues: 'True=' + GROUP_BOX_YES
                                              + ',False=' + GROUP_BOX_NO),

    (ITable: 'EInfo'; IField: 'DEFAULTHOURS'; ETable: 'EE'; EField: 'STANDARD_HOURS'; MapType: mtDirect),
    (ITable: 'EInfo'; IField: 'WCC'; ETable: 'EE'; EField: 'CO_WORKERS_COMP_NBR'; MapType: mtLookup; LookupTable: 'CO_WORKERS_COMP'; LookupField: 'WORKERS_COMP_CODE')
  );

constructor TMillenniumImportDef.Create(const AIEMap: IIEMap);
begin
  inherited Create(AIEMap);

  RegisterConvertDescs( MConversion );
  RegisterKeySourceDescs( MKeys );
  RegisterRowImport(MRowImport);
  RegisterMap( MMap );
end;

procedure TMillenniumImportDef.E1_STATE(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
var
  v: Variant;
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'));
  Output.Add( 'CL_PERSON', 'STATE', Value );
  if not VarIsNull(Value) then
    Output.Add( 'CL_PERSON', 'RESIDENTIAL_STATE_NBR', FImpEngine.IDS('SY_STATES').Lookup('STATE', Value, 'SY_STATES_NBR') )
  else
  begin
    Logger.LogWarningFmt('Employee State is empty. Residential State is set to company Home State',[]);

    if FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').IsNull then
      raise EevException.CreateFmt( 'Please define company Home State for %s', [ FImpEngine.IDS('CO').FieldByName('CUSTOM_COMPANY_NUMBER').AsString ] );

    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'));
    v := FImpEngine.IDS('CO_STATES').Lookup('CO_STATES_NBR', FImpEngine.IDS('CO')['HOME_CO_STATES_NBR'], 'STATE');
    Output.Add( 'CL_PERSON', 'RESIDENTIAL_STATE_NBR', FImpEngine.IDS('SY_STATES').Lookup('STATE', v, 'SY_STATES_NBR') );
  end
end;

procedure TMillenniumImportDef.PrenoteDate(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);
begin
  if (VarToStr(Value) = '') or (Value < Date) then
    Output.Add('EE_DIRECT_DEPOSIT', 'IN_PRENOTE', GROUP_BOX_NO)
  else
    Output.Add('EE_DIRECT_DEPOSIT', 'IN_PRENOTE', GROUP_BOX_YES);
end;

procedure TMillenniumImportDef.EDirDep(const Input, Keys, Output: TFieldValues);
var
  v: Variant;
  bFound: boolean;
begin
  //direct deposits are imported using map table
  //here we create Scheduled E/D for DD that we are importing now

  // CO, EE, EE_DIRECT_DEPOSIT tables are restricted (in TImportDefinition.ExecuteRowCustomFunc)
  FImpEngine.CurrentDataRequired( FImpEngine.IDS('CO_E_D_CODES'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
  FImpEngine.CurrentDataRequired( FImpEngine.IDS('CL_E_DS') );
  FImpEngine.CurrentDataRequired( FImpEngine.IDS('EE_SCHEDULED_E_DS'), 'EE_NBR='+VarToStr(FImpEngine.IDS('EE')['EE_NBR']) );

  bFound := false;
  FImpEngine.IDS('CO_E_D_CODES').First;
  while not FImpEngine.IDS('CO_E_D_CODES').Eof do
  begin
    v := FImpEngine.IDS('CL_E_DS').Lookup( 'CL_E_DS_NBR', FImpEngine.IDS('CO_E_D_CODES')['CL_E_DS_NBR'], 'E_D_CODE_TYPE' );
    if not VarIsNull(v) and (v = ED_DIRECT_DEPOSIT) then
    begin
      //for each DD E/D
      if VarIsNull( FImpEngine.IDS('EE_SCHEDULED_E_DS').Lookup( 'CL_E_DS_NBR', FImpEngine.IDS('CO_E_D_CODES')['CL_E_DS_NBR'], 'EE_SCHEDULED_E_DS_NBR') ) then
      begin
        //we have not used this D1 E/D for this employee
        //Actually trigger allows to have scheduled E/Ds with the same code if the effective periods are not overlapping
        //In case of importing of new company I just ignore this fact
        bFound := true;
        break;
      end
    end;
    FImpEngine.IDS('CO_E_D_CODES').Next;
  end;
  if bFound then
  begin
    Output.Add( 'EE_SCHEDULED_E_DS', 'CL_E_DS_NBR', FImpEngine.IDS('CO_E_D_CODES')['CL_E_DS_NBR'] );

    ImportDedRateAmount( Input['EDirDep.amountCode'], Input['EDirDep.amount'], Output, true );

    Output.Add( 'EE_SCHEDULED_E_DS', 'PRIORITY_NUMBER', Input['EDirDep.sequence'] );
    Output.Add( 'EE_SCHEDULED_E_DS', 'EFFECTIVE_START_DATE', EncodeDate( YearOf(Now), 1, 1) );

    if Input['EDirDep.excludespecial'] = true then
      Output.Add( 'EE_SCHEDULED_E_DS', 'FREQUENCY', SCHED_FREQ_SCHEDULED_PAY )
    else
      Output.Add( 'EE_SCHEDULED_E_DS', 'FREQUENCY', SCHED_FREQ_DAILY );
    // EE_SCHEDULED_E_DS.EE_DIRECT_DEPOSIT_NBR is filled automatically because we have key in MKeys array for EE_DIRECT_DEPOSIT
  end
  else
    Logger.LogErrorFmt( 'Cannot create scheduled E/D for Direct Deposit: not enough D1 Deductions',[]);
//    Logger.LogErrorFmt( 'Cannot create scheduled E/D for Direct Deposit (ABA Number <%s>, Account <%s>)',
//      [VarToStr(Input['EDirDep.transit']), VarToStr(Input['EDirDep.account']) ]);
end;

procedure TMillenniumImportDef.EInfoFix(const Input, Keys, Output: TFieldValues);
var
  v: Variant;
  st: Variant;
  stm: string;
  SyStateMaritalStatusNbr: Integer;
begin
  if FImpEngine.IDS('EE').RecordCount = 0 then //previous pass failed to import this employee
    Exit;

  if FImpEngine.IDS('EE').FieldByName('HOME_TAX_EE_STATES_NBR').IsNull then
  begin
//    ODS( 'EE.RecordCount: %d, EE.EE_NBR.AsString = %s', [IDS('EE').RecordCount, IDS('EE').FieldByName('EE_NBR').AsString]);
    if FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').IsNull then
      raise EevException.CreateFmt( 'Please define company Home State for %s', [ FImpEngine.IDS('CO').FieldByName('CUSTOM_COMPANY_NUMBER').AsString ] );

    v := FImpEngine.IDS('CO')['HOME_CO_STATES_NBR'];
    Keys.Add('EE_STATES', 'CO_STATES_NBR', v);
    Output.Add('EE', 'HOME_TAX_EE_STATES_NBR', VarArrayOf(['EE_STATES', 'CO_STATES_NBR', v, 'EE_STATES_NBR']));

    if FImpEngine.IDS('EE')['COMPANY_OR_INDIVIDUAL_NAME'] <> GROUP_BOX_COMPANY then
      Logger.LogWarning( 'Added missing employee''s Home State. Company Home state used by default.');

    FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE_STATES'), 'EE_NBR=' + FImpEngine.IDS('EE').FieldByName('EE_NBR').AsString);
    //I don't want to override imported STATE_MARITAL_STATUS in case when
    //this state was imported but not set as home state
    //(condition ETax.defaultTax = 'True' was never true )
    if not FImpEngine.IDS('EE_STATES').Locate('CO_STATES_NBR', v, []) then
    begin
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
      st := FImpEngine.IDS('CO_STATES').Lookup('CO_STATES_NBR', v, 'STATE'); //must succeed
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'), 'STATE = '''+ VarToStr(st)+'''');
     FImpEngine. CurrentDataRequired(FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + FImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsString);

      if FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Locate('STATUS_DESCRIPTION', 'Single', [loCaseInsensitive]) then
      begin
        stm := FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['STATUS_TYPE'];
        SyStateMaritalStatusNbr := FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['SY_STATE_MARITAL_STATUS_NBR'];
      end
      else
        stm := FirstSyMaritalStatus(SyStateMaritalStatusNbr);
      Output.Add('EE_STATES', 'STATE_MARITAL_STATUS', stm );
      Output.Add('EE_STATES', 'SY_STATE_MARITAL_STATUS_NBR', SyStateMaritalStatusNbr );
      if assigned(FEEStatesWithMaritalStatus) then
        FEEStatesWithMaritalStatus.Add( FImpEngine.IDS('EE').FieldByName('custom_employee_number').AsString + '@' + VarToStr(st) );

      Output.Add('EE_STATES', 'STATE_NUMBER_WITHHOLDING_ALLOW', 0);

      if FImpEngine.IDS('EE')['COMPANY_OR_INDIVIDUAL_NAME'] <> GROUP_BOX_COMPANY then
        Logger.LogWarningFmt( 'Employee %s: Marital Status of ''%s'' (with 0 exemptions) has been set up for %s',[FImpEngine.IDS('EE').FieldByName('CUSTOM_EMPLOYEE_NUMBER').AsString, (stm), VarToStr(st)]);
    end
  end
end;

function TMillenniumImportDef.FirstSyMaritalStatus(var SyStateMaritalStatusNbr: Integer): Variant;
begin
  FImpEngine.IDS('SY_STATE_MARITAL_STATUS').IndexFieldNames := 'STATUS_TYPE';//'STATUS_DESCRIPTION';
  try
    FImpEngine.IDS('SY_STATE_MARITAL_STATUS').First;
    //assume that we always have at least one state marital status
    Result := FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['STATUS_TYPE'];
    SyStateMaritalStatusNbr := FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['SY_STATE_MARITAL_STATUS_NBR'];
  finally
    FImpEngine.IDS('SY_STATE_MARITAL_STATUS').IndexFieldNames := '';
  end;
end;

procedure TMillenniumImportDef.AI_EE_STATES(const ds: TevClientDataSet; const Keys: TFieldValues);
var
  st: Variant;
  stm: Variant;
  fedm: string;
  SyStateMaritalStatusNbr: Integer;
begin
  ds.FieldByName('IMPORTED_MARITAL_STATUS').AsString := FImpEngine.IDS('EE').FieldByName('FEDERAL_MARITAL_STATUS').AsString;

  //RE 28171 set default value
  //RE 32661
  //make sure that iemap isn't using CO_STATES, SY_STATES and SY_STATE_MARITAL_STATUS tables
  //!!it is better to clone cursor though
  Assert( not InList('CO_STATES', Keys.Tables) );
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
  st := FImpEngine.IDS('CO_STATES').Lookup('CO_STATES_NBR', Keys.Value['EE_STATES.CO_STATES_NBR'], 'STATE' );
  if assigned(FCreatedEEStates) then
    FCreatedEEStates.Add( FImpEngine.IDS('EE').FieldByName('custom_employee_number').AsString + '@' + VarToStr(st) );
  if not VarIsNull(st) then
  begin
    Assert( not InList('SY_STATES', Keys.Tables) );
    Assert( not InList('SY_STATE_MARITAL_STATUS', Keys.Tables) );

    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'), 'STATE = '''+ VarToStr(st)+'''');
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + FImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsString);
    fedm := FImpEngine.IDS('EE').FieldByName('FEDERAL_MARITAL_STATUS').AsString;

    if FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Locate('STATUS_TYPE', fedm, []) or
       FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Locate('STATUS_TYPE', fedm + ' ', []) then
    begin
      stm := FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['STATUS_TYPE'];
      SyStateMaritalStatusNbr := FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['SY_STATE_MARITAL_STATUS_NBR'];
    end
    else
      stm := FirstSyMaritalStatus(SyStateMaritalStatusNbr);
    ds['STATE_MARITAL_STATUS'] := stm;
    ds['SY_STATE_MARITAL_STATUS_NBR'] := SyStateMaritalStatusNbr;
  end;

  if UpperCase(Self.ClassName) = 'TM3IMPORTDEF' then
    ds.FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE
  else
    ds.FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE;

  //RE 28026 default value for SUI State
  ds['SUI_APPLY_CO_STATES_NBR'] := Keys.Value['EE_STATES.CO_STATES_NBR'];

  //RE 29029
  //Output.Add('EE_STATES', 'SDI_APPLY_CO_STATES_NBR', v) moved from EETax here;
  //this line was called each time we encounter 'S' type
  //Thus we can get this behaviour when inserting states from another places
  ds['SDI_APPLY_CO_STATES_NBR'] := Keys.Value['EE_STATES.CO_STATES_NBR'];

{  //#RE 26916
  ds.FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE;
  ds.FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE;
  ds.FieldByName('ER_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE;
  ds.FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_EXCLUDE;
}
end;

procedure TMillenniumImportDef.EETax(const Input, Keys, Output: TFieldValues);
  procedure HelpImportTaxOverride( outtable, outfieldpfx: string );
  begin
    if (VarToStr(Input['ETax.additionalAmount']) <> '') and (Input['ETax.additionalAmount'] <> 0) then
    begin
      if Input['ETax.overrideTaxCalc'] = true then
        Output.Add( outtable, outfieldpfx + 'TYPE', OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT)
      else
        Output.Add( outtable, outfieldpfx + 'TYPE', OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT);
      Output.Add( outtable, outfieldpfx + 'VALUE', Input['ETax.additionalAmount']);
    end
    else if (VarToStr(Input['ETax.additionalPercentage']) <> '') and (Input['ETax.additionalPercentage'] <> 0) then
    begin
      if Input['ETax.overrideTaxCalc'] = true then
        Output.Add( outtable, outfieldpfx + 'TYPE', OVERRIDE_VALUE_TYPE_REGULAR_PERCENT)
      else
        Output.Add( outtable, outfieldpfx + 'TYPE', OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT);
      Output.Add( outtable, outfieldpfx + 'VALUE', Input['ETax.additionalPercentage']);
    end;
  end;

  function GetAZMaritalStatus: Variant;
{  const
    mm: array [0..4]of record s: string; t: integer end = (
      (s: '10'; t: 10),
      (s: '20'; t: 23),
      (s: '17'; t: 19),
      (s: '22'; t: 25),
      (s: '28'; t: 31)
    );}
  begin
    FImpEngine.CurrentDataRequired( FImpEngine.IDS('SY_STATES'), 'STATE = ''AZ'' ' );
    FImpEngine.CurrentDataRequired( FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + FImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsString );
//    Result := IDS('SY_STATE_MARITAL_STATUS').Lookup( 'STATUS_DESCRIPTION', VarToStr(Input['ETax.additionalPercentage']) + ' Percent', 'STATUS_TYPE' );
    Result := FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Lookup( 'STATUS_TYPE', Input['ETax.additionalPercentage'], 'STATUS_TYPE' );
  end;
var
  v, value: Variant;
  State: string;
  StMarStat: string;
  azms: Variant;
  esms: String;
begin
  value := Matchers.Get(sMDLocalsBindingName).RightMatch( Input['ETax.tcode'] );
  if not VarIsNull(value) then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'));
    v := FImpEngine.IDS('CO_STATES').Lookup('CO_NBR;STATE', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], 'MD']), 'CO_STATES_NBR');
    Keys.Add('EE_STATES', 'CO_STATES_NBR', v);

    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + FImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsString);
    if FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Locate('STATUS_TYPE', value, []) then
      OutPut.Add('EE_STATES', 'SY_STATE_MARITAL_STATUS_NBR', FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['SY_STATE_MARITAL_STATUS_NBR']);

    Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', value );

    if assigned(FEEStatesWithMaritalStatus) then
      FEEStatesWithMaritalStatus.Add( FImpEngine.IDS('EE').FieldByName('custom_employee_number').AsString + '@' + 'MD' );
    Exit;
  end;

  value := Matchers.Get(sTaxesBindingName).RightMatch( Input['ETax.tcode'] );
  if not VarIsNull(value) then
  begin
    if value[0] = 'F' then
    begin
      if value[2] = 'FE' then
      begin
         Output.Add('EE', 'EXEMPT_EXCLUDE_EE_FED', GROUP_BOX_INCLUDE);
         Output.Add('EE', 'FEDERAL_MARITAL_STATUS', Input['ETax.filingStatus']);
         Output.Add('EE', 'NUMBER_OF_DEPENDENTS', Input['ETax.exemptions']);

         HelpImportTaxOverride( 'EE', 'OVERRIDE_FED_TAX_' );
      end
      else if value[2] = 'OE' then
        Output.Add('EE', 'EXEMPT_EMPLOYEE_OASDI', GROUP_BOX_NO)
      else if value[2] = 'OR' then
        Output.Add('EE', 'EXEMPT_EMPLOYER_OASDI', GROUP_BOX_NO)
      else if value[2] = 'ME' then
        Output.Add('EE', 'EXEMPT_EMPLOYEE_MEDICARE', GROUP_BOX_NO)
      else if value[2] = 'MR' then
        Output.Add('EE', 'EXEMPT_EMPLOYER_MEDICARE', GROUP_BOX_NO)
      else if value[2] = 'FU' then
        Output.Add('EE', 'EXEMPT_EMPLOYER_FUI', GROUP_BOX_NO)
      else if value[2] = 'EI' then
        Output.Add('EE', 'EIC', Input['ETax.filingStatus']);
    end
    else if value[0] = 'S' then
    begin
      State := value[1];
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'));
      v := FImpEngine.IDS('CO_STATES').Lookup('CO_NBR;STATE', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], Value[1]]), 'CO_STATES_NBR');
      Keys.Add('EE_STATES', 'CO_STATES_NBR', v);
{  #RE 26916
      if value[2] = 'SE' then
        Output.Add('EE_STATES', 'EE_SDI_EXEMPT_EXCLUDE', GROUP_BOX_INCLUDE)
      else if value[2] = 'SR' then
        Output.Add('EE_STATES', 'ER_SDI_EXEMPT_EXCLUDE', GROUP_BOX_INCLUDE)
      else }
      if value[2] = 'ST' then
      begin
        if VarToStr(Input['ETax.defaultTax']) = 'True' then
          Output.Add('EE', 'HOME_TAX_EE_STATES_NBR', VarArrayOf(['EE_STATES', 'CO_STATES_NBR', v, 'EE_STATES_NBR']));

        v := Matchers.Get(sReciprocalBindingName).RightMatch( Input['ETax.reciprocal'] );
        if VarToStr(v) <> '' then
          Output.Add('EE_STATES', 'RECIPROCAL_METHOD', v);
        Output.Add('EE_STATES', 'STATE_EXEMPT_EXCLUDE', GROUP_BOX_INCLUDE);

        if (State = 'AZ') and (VarToStr(Input['ETax.additionalPercentage']) <> '') and (Input['ETax.additionalPercentage'] <> 0) then //RE# 26870
        begin
          if VarIsNull( GetAZMaritalStatus ) then //have percentage that cannot be imported as marital status
          begin
            Output.Add( 'EE_STATES', 'OVERRIDE_STATE_TAX_TYPE', OVERRIDE_VALUE_TYPE_REGULAR_PERCENT );
            Output.Add( 'EE_STATES', 'OVERRIDE_STATE_TAX_VALUE', Input['ETax.additionalPercentage'] );
          end //else it is imported as marital status
        end
        else
          HelpImportTaxOverride( 'EE_STATES', 'OVERRIDE_STATE_TAX_' );
        Output.Add('EE_STATES', 'STATE_NUMBER_WITHHOLDING_ALLOW', Input['ETax.exemptions']);

        StMarStat := VarToStr(Input['ETax.filingStatus']);
        if State = 'AR' then
        begin
          if StMarStat = '0' then
            esms := 'S0'
          else if StMarStat = 'H' then
            esms := 'D'
          else if StMarStat = 'MS' then
            esms := 'B'
          else if StMarStat = 'MJ' then
            esms := 'C'
          else if StMarStat = 'S' then
            esms  := 'A'
        end
        else if State = 'AZ' then
        begin
          azms := GetAZMaritalStatus;
          if VarIsNull(azms) then
          begin
            azms := '10';
            {
            CurrentDataRequired(IDS('SY_STATES'), 'STATE = ''AZ''');
            CurrentDataRequired(IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + IntToStr(IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsInteger));
            IDS('SY_STATE_MARITAL_STATUS').IndexFieldNames := 'STATUS_DESCRIPTION';
            IDS('SY_STATE_MARITAL_STATUS').First;
            azms := IDS('SY_STATE_MARITAL_STATUS')['STATUS_TYPE'];
            }
          end;
          esms := azms;
        end
        else if State = 'CA' then
          if StMarStat = 'S' then
            esms := 'SM'
          else if (StMarStat = 'M') and (Input['ETax.exemptions'] >= 2) then
            esms := 'M2'
          else
            esms := StMarStat
        else if State = 'CT' then
          if StMarStat = 'E' then
          begin
            esms := 'A';
            Output.Add('EE_STATES', 'STATE_EXEMPT_EXCLUDE', GROUP_BOX_EXEMPT)
          end
          else
            esms := StMarStat
        else if (State = 'DC') or
                (State = 'DE') then
          if StMarStat = 'MJ' then
            esms := 'M'
          else if StMarStat = 'MS' then
            esms := 'A'
          else
            esms := StMarStat
        else if State = 'GA' then
        begin
          if StMarStat = 'S' then
            esms := 'A'
          else if StMarStat = 'H' then
            esms := 'D'
          else if StMarStat = 'MS' then
            esms := 'B'
          else if StMarStat = 'MJ1' then
            esms := 'C'
          else if StMarStat = 'MJ2' then
            esms := 'B1'
        end
        else if State = 'IA' then
          if Input['ETax.exemptions'] >= 2 then
            esms := 'S2'
          else
            esms := 'S'
        else if State = 'LA' then
        begin
          if StMarStat = 'S' then
            esms := Input['ETax.exemptions']
          else if StMarStat = 'M' then
            esms := '2'
        end
        else if State = 'MA' then
        begin
          if StMarStat = 'S' then
            esms := '0'
          else if StMarStat = 'M' then
            esms := '1'
          else if StMarStat = 'H' then
            esms := 'H0'
        end
        else if State = 'MD' then
          esms := '1' // default, as it should be attached through locals
        else if State = 'ME' then
          if StMarStat = 'MD' then
            esms := 'A'
          else
            esms := StMarStat
        else if (State = 'MI') or
                (State = 'OH') or
                (State = 'VA') or
                (State = 'PA') then
          esms := 'SM'
{
        else if State = 'MN' then
          if StMarStat = 'M' then
            Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', 'M1' )
          else if StMarStat = 'MS' then
            Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', 'M0' )
          else if (StMarStat = 'H') and (Input['ETax.exemptions'] < 5) then
            Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', 'H2' )
          else
            Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', StMarStat )
}
        else if State = 'MN' then
        begin
          if (StMarStat = 'M') or (StMarStat = 'S') then
            esms := StMarStat;
        end
        else if State = 'NC' then
        begin
          if StMarStat = 'M' then
            esms := 'MW'
          else if StMarStat = 'H' then
            esms := 'HH'
        end
        else if State = 'NJ' then
          if (StMarStat >= 'A') and (StMarStat <= 'E') then
            esms := StMarStat
          else
            esms := 'A'
        else if State = 'OK' then
          if StMarStat = 'MS' then
            esms := 'S'
          else if StMarStat = 'MD' then
            esms := 'D'
          else if StMarStat = 'MJ' then
            esms := 'M'
          else
            esms := StMarStat
        else if State = 'OR' then
          if StMarStat = 'H' then
            esms := 'M'
          else
            esms := StMarStat
        else if State = 'PR' then
          if StMarStat = 'O' then
            esms := 'M0'
          else if StMarStat = 'H' then
            esms := 'MH'
          else if StMarStat = 'MJ' then
            esms := 'M'
          else
            esms := 'StMarStat'
        else if State = 'SC' then
          if Input['ETax.exemptions'] >= 1 then
            esms := '1'
          else
            esms := '0'
        else if State = 'WV' then
          if StMarStat = 'MD' then
            esms := 'MM'
          else
            esms := StMarStat
        else if StMarStat <> '' then
          esms := StMarStat
        else
          esms := 'S';

        FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'), 'STATE = '''+ VarToStr(state)+'''');
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + FImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsString);
        if FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Locate('STATUS_TYPE', esms, []) then
          OutPut.Add('EE_STATES', 'SY_STATE_MARITAL_STATUS_NBR', FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['SY_STATE_MARITAL_STATUS_NBR']);

        Output.Add('EE_STATES', 'STATE_MARITAL_STATUS', esms);

        if assigned(FEEStatesWithMaritalStatus) then
          if Output.HasValue('EE_STATES.STATE_MARITAL_STATUS') then
            FEEStatesWithMaritalStatus.Add( FImpEngine.IDS('EE').FieldByName('custom_employee_number').AsString + '@' + State );

      end //if value[2] = 'ST'
//      else
        //Assert(false);
    end
    else if value[0] = 'U' then
    begin
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE_STATES'), 'EE_NBR=' + FImpEngine.IDS('EE').FieldByName('EE_NBR').AsString);
      if FImpEngine.IDS('CO_STATES').Locate('STATE', value[2], []) then
        if FImpEngine.IDS('EE_STATES').Locate('CO_STATES_NBR', FImpEngine.IDS('CO_STATES')['CO_STATES_NBR'], []) then
        begin
          Keys.Add( 'EE_STATES', 'CO_STATES_NBR', FImpEngine.IDS('CO_STATES')['CO_STATES_NBR'] );
          Output.Add( 'EE_STATES', 'SUI_APPLY_CO_STATES_NBR', FImpEngine.IDS('CO_STATES')['CO_STATES_NBR'] );
        end
        else if FImpEngine.IDS('EE_STATES').RecordCount > 0 then
        begin
          FImpEngine.IDS('EE_STATES').First;
          Keys.Add( 'EE_STATES', 'CO_STATES_NBR', FImpEngine.IDS('EE_STATES')['CO_STATES_NBR'] );
          Output.Add( 'EE_STATES', 'SUI_APPLY_CO_STATES_NBR', FImpEngine.IDS('CO_STATES')['CO_STATES_NBR'] );
        end;
    end
    else if value[0] = 'Z' then
    begin
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_LOCAL_TAX'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']));
      Output.Add( 'EE_LOCALS', 'CO_LOCAL_TAX_NBR', FImpEngine.IDS('CO_LOCAL_TAX').Lookup('SY_LOCALS_NBR', Value[1], 'CO_LOCAL_TAX_NBR') );
      HelpImportTaxOverride( 'EE_LOCALS', 'OVERRIDE_LOCAL_TAX_' );
    end;
  end;
end;

procedure TMillenniumImport.RunPrImport(aImportDefinition: IImportDefinition; aParams: TStrings);
  function GenerateFixedKeys( const aPrDef: TPrDef ): string;
  begin
    Result := 'CO.CUSTOM_COMPANY_NUMBER=' + DataModule.CustomCompanyNumber
              +#13#10+'PR.CHECK_DATE=' + DateToStr(aPrDef.CheckDate)
              +#13#10+'PR.RUN_NUMBER=1'
              +#13#10+'PR_BATCH.PERIOD_BEGIN_DATE=' + DateToStr(aPrDef.PeriodBeginDate)
              +#13#10+'PR_BATCH.PERIOD_END_DATE=' + DateToStr(aPrDef.PeriodEndDate)
              +#13#10+'PR_BATCH.FREQUENCY=' + aParams.Values[sParamBatchFrequency]
              +#13#10+'PR_CHECK.CHECK_TYPE=' + aPrDef.CheckType
              ;
  end;

  function HaveAnyDataForQuarter( quarter: integer ): boolean;
  var
    eytd: TDataSet;
    hfield, afield: string;
  begin
    Result := false;
    eytd := DataModule.GetTable('EYTD');
    hfield := 'qhours' + inttostr(quarter);
    afield := 'qamount' + inttostr(quarter);
    eytd.First;
    while not eytd.Eof do
    begin
      if not eytd.FieldByName(hfield).IsNull and (eytd[hfield] <> 0)
         or not eytd.FieldByName(afield).IsNull and (eytd[afield] <> 0) then
      begin
        Result := true;
        break;
      end;
      eytd.Next;
    end;
  end;
var
  i: Integer;
  PrDefs: TPrDefs;
  taskname: string;
begin
  CalcPayrollDefs( StrToDate(aParams.Values[sParamCheckDate]), PrDefs );
  for i := 0 to high(PrDefs) do
  begin
    aImportDefinition.SetParam( sParamNeedPrevQuarters, PrDefs[i].IncludePrevQuarters );
    aImportDefinition.SetParam( sParamQuarter, PrDefs[i].Quarter );

    if PrDefs[i].IncludePrevQuarters then
    begin
      if PrDefs[i].Quarter = 1 then
        taskname := Format('Importing YTD (quarter: 1)', [])
      else
        taskname := Format('Importing YTD (quarters: 1 to %d)', [PrDefs[i].Quarter]);
    end
    else
    begin
      if not HaveAnyDataForQuarter(PrDefs[i].Quarter) then
      begin
        Logger.LogWarningFmt('There is no data in EYTD table for %d quarter. Payroll was not created.', [PrDefs[i].Quarter]);
        continue;
      end;
      taskname := Format('Importing YTD (quarter: %d)', [PrDefs[i].Quarter]);
    end;

    aImportDefinition.SetFixedKeys( GenerateFixedKeys( PrDefs[i] ) );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EYtd', taskname );
  end;
end;

procedure TMillenniumImportDef.GetAmounts(const Input: TFieldValues; var amount: Variant; var hours: Variant);
  procedure IncVar(var v: Variant; v1: Variant);
  begin
    if VarIsNull(v) then
      v := v1
    else if not VarIsNull(v1) then
      v := v + v1;
  end;
var
  q: integer;
  bNeedPrev: boolean;
  i: integer;
begin
  q := GetParam(sParamQuarter);
  bNeedPrev := GetParam(sParamNeedPrevQuarters);
  if bNeedPrev then
  begin
    amount := Null;
    hours := Null;
    for i := 1 to q do
    begin
      IncVar( amount, Input['EYtd.qamount'+inttostr(i)] );
      IncVar( hours, Input['EYtd.qhours'+inttostr(i)] );
    end
  end
  else
  begin
    amount := Input['EYtd.qamount'+inttostr(q)];
    hours := Input['EYtd.qhours'+inttostr(q)];
  end
end;

function GetDDStat(DataModule: IExternalDataModule): TDDStat;
var
  v: Variant;
  EDirDepNum: TDataSet;
begin
  Result.need := 0;
  EDirDepNum := DataModule.GetTable('EDirDepNum');
  EDirDepNum.First;
  while not EDirDepNum.Eof do
  begin
    if Result.need < EDirDepNum.FieldByName('DDNum').AsInteger then
      Result.need := EDirDepNum.FieldByName('DDNum').AsInteger;
    EDirDepNum.Next;
  end;
  Result.has := 0;
  DM_COMPANY.CO_E_D_CODES.First;
  while not DM_COMPANY.CO_E_D_CODES.Eof do
  begin
    v := DM_CLIENT.CL_E_DS.Lookup( 'CL_E_DS_NBR', DM_COMPANY.CO_E_D_CODES.CL_E_DS_NBR.Value, 'E_D_CODE_TYPE' );
    if not VarIsNull(v) and (v = ED_DIRECT_DEPOSIT) then
      inc(Result.has);
    DM_COMPANY.CO_E_D_CODES.Next;
  end;
  if Result.need <= Result.has then
    Result.msg := Format('You have enough ''D1'' E/Ds to create Scheduled E/Ds for all imported Direct Deposits.',
        [ Result.has])
  else
    Result.msg := Format('You will need at least %d D1 E/Ds to create Scheduled E/Ds for imported Direct Deposits. You have only %d. See details in EDirDepNum table.',
        [ Result.need, Result.has]);
end;


procedure TMillenniumImportDef.AI_EE_SCHEDULED_E_DS(const ds: TevClientDataSet;
  const Keys: TFieldValues);
  procedure SetDefault( fname: string; srcfname: string = '' );
  begin
    if srcfname = '' then
      srcfname := 'SD_'+fname;
    if not VarIsNull( FImpEngine.IDS('CL_E_DS')[srcfname] ) then
      ds[fname] := FImpEngine.IDS('CL_E_DS')[srcfname];
  end;
var
  Q: IevQuery;
begin
  ds['EFFECTIVE_START_DATE'] := Date;
  ds['FREQUENCY'] := SCHED_FREQ_DAILY;
  ds['EXCLUDE_WEEK_1'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_2'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_3'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_4'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_5'] := GROUP_BOX_NO;
  if FImpEngine.IDS('CL_E_DS')['SCHEDULED_DEFAULTS'] = GROUP_BOX_YES then
  begin
    SetDefault( 'FREQUENCY' );
    SetDefault( 'EFFECTIVE_START_DATE' );
    SetDefault( 'EXCLUDE_WEEK_1' );
    SetDefault( 'EXCLUDE_WEEK_2' );
    SetDefault( 'EXCLUDE_WEEK_3' );
    SetDefault( 'EXCLUDE_WEEK_4' );
    SetDefault( 'EXCLUDE_WEEK_5' );
    SetDefault( 'CALCULATION_TYPE', 'SD_CALCULATION_METHOD' );
    SetDefault( 'EXPRESSION' );
    SetDefault( 'CL_E_D_GROUPS_NBR', 'CL_E_D_GROUPS_NBR' );
    SetDefault( 'CL_AGENCY_NBR', 'DEFAULT_CL_AGENCY_NBR' );
    SetDefault( 'AMOUNT' );
    SetDefault( 'PERCENTAGE', 'SD_RATE' );
     /////////////////////////////////////
    SetDefault( 'WHICH_CHECKS');
    SetDefault( 'PRIORITY_NUMBER');
    SetDefault( 'ALWAYS_PAY');
    SetDefault( 'PLAN_TYPE');
    SetDefault( 'MAX_AVG_AMT_CL_E_D_GROUPS_NBR','SD_MAX_AVG_AMT_GRP_NBR');
    SetDefault( 'MAX_AVG_HRS_CL_E_D_GROUPS_NBR','SD_MAX_AVG_HRS_GRP_NBR' );
    SetDefault( 'MAX_AVERAGE_HOURLY_WAGE_RATE', 'SD_MAX_AVERAGE_HOURLY_WAGE_RATE');
    SetDefault( 'THRESHOLD_E_D_GROUPS_NBR','SD_THRESHOLD_E_D_GROUPS_NBR');
    SetDefault( 'THRESHOLD_AMOUNT', 'SD_THRESHOLD_AMOUNT' );
    SetDefault( 'DEDUCTIONS_TO_ZERO', 'SD_DEDUCTIONS_TO_ZERO' );
    Q := TevQuery.Create('SELECT CL_E_DS_NBR, CO_BENEFIT_SUBTYPE_NBR FROM CO_E_D_CODES WHERE {AsOfNow<CO_E_D_CODES>} and CL_E_DS_NBR=' + IntToStr(FImpEngine.IDS('CL_E_DS')['CL_E_DS_NBR']));
    Q.Execute;
    if Q.Result.RecordCount > 0 then
      ds['CO_BENEFIT_SUBTYPE_NBR'] := Q.Result.FieldByName('CO_BENEFIT_SUBTYPE_NBR').Value;
    /////////////////////////////////////

  end
end;

procedure TMillenniumImportDef.AI_PR(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds.FieldByName('CHECK_DATE_STATUS').AsString := DATE_STATUS_IGNORE; //as in rapid import
  ds.FieldByName('SCHEDULED').AsString := GROUP_BOX_NO; //as in rapid import
  ds.FieldByName('PAYROLL_TYPE').AsString := PAYROLL_TYPE_IMPORT;
  ds.FieldByName('STATUS').AsString := PAYROLL_STATUS_PENDING; // as default
  ds.FieldByName('STATUS_DATE').AsDateTime := SysTime; //Date or DateTime?
  ds.FieldByName('EXCLUDE_ACH').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_TAX_DEPOSITS').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_AGENCY').AsString := GROUP_BOX_YES;
  ds.FieldByName('MARK_LIABS_PAID_DEFAULT').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_BILLING').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_R_C_B_0R_N').AsString := GROUP_BOX_BOTH2;
  ds.FieldByName('INVOICE_PRINTED').AsString := INVOICE_PRINT_STATUS_calculated_do_not_print;

  ds.FieldByName('SCHEDULED_CALL_IN_DATE').Value := Date;
  ds.FieldByName('SCHEDULED_PROCESS_DATE').AsDateTime := Date;
  ds.FieldByName('SCHEDULED_DELIVERY_DATE').AsDateTime := Date;
  ds.FieldByName('INVOICE_PRINTED').AsString := INVOICE_PRINT_STATUS_calculated_do_not_print;
  ds.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
end;

procedure TMillenniumImportDef.AI_PR_BATCH(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds.FieldByName('PERIOD_BEGIN_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
  ds.FieldByName('PERIOD_END_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
  ds.FieldByName('PAY_SALARY').AsString := GROUP_BOX_NO;
  ds.FieldByName('PAY_STANDARD_HOURS').AsString := GROUP_BOX_NO;
  ds.FieldByName('LOAD_DBDT_DEFAULTS').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
end;

procedure TMillenniumImportDef.AI_PR_CHECK(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds.FieldByName('PAYMENT_SERIAL_NUMBER').AsInteger := ctx_PayrollCalculation.GetNextPaymentSerialNbr;
  ds.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString := GetCustomBankAccountNumber(FImpEngine);
  ds.FieldByName('EXCLUDE_DD').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_DD_EXCEPT_NET').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_TIME_OFF_ACCURAL').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
  ds.FieldByName('EXCLUDE_AUTO_DISTRIBUTION').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_SCH_E_D_FROM_AGCY_CHK').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_SCH_E_D_EXCEPT_PENSION').AsString := GROUP_BOX_NO;
  ds.FieldByName('PRORATE_SCHEDULED_E_DS').AsString := GROUP_BOX_NO;
  ds.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
  ds.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_CLEARED;
  ds.FieldByName('STATUS_CHANGE_DATE').AsDateTime := Now;
  ds.FieldByName('EXCLUDE_FROM_AGENCY').AsString := GROUP_BOX_YES;
  ds.FieldByName('CHECK_TYPE_945').AsString := GROUP_BOX_NO;
  ds.FieldByName('CALCULATE_OVERRIDE_TAXES').AsString := GROUP_BOX_NO;

  ds.FieldByName('TAX_FREQUENCY').AsString := FImpEngine.IDS('EE').FieldByName('PAY_FREQUENCY').AsString; //as Roman said
end;

procedure TMillenniumImportDef.AI_PR_CHECK_LINES(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds['LINE_TYPE'] := CHECK_LINE_TYPE_USER; 
  ds['CO_DIVISION_NBR'] := FImpEngine.IDS('EE')['CO_DIVISION_NBR'];
  ds['CO_BRANCH_NBR'] := FImpEngine.IDS('EE')['CO_BRANCH_NBR'];
  ds['CO_DEPARTMENT_NBR'] := FImpEngine.IDS('EE')['CO_DEPARTMENT_NBR'];
  ds['EE_STATES_NBR'] := FImpEngine.IDS('EE')['HOME_TAX_EE_STATES_NBR'];
  ds['EE_SUI_STATES_NBR'] := FImpEngine.IDS('EE')['HOME_TAX_EE_STATES_NBR'];
  ds['AGENCY_STATUS'] := CHECK_LINE_AGENCY_STATUS_IGNORE;
end;

procedure TMillenniumImportDef.AI_PR_CHECK_LOCALS(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds['EXCLUDE_LOCAL'] := GROUP_BOX_NO;
end;

procedure TMillenniumImportDef.AI_PR_CHECK_STATES(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds['TAX_AT_SUPPLEMENTAL_RATE'] := GROUP_BOX_NO;
  ds['EXCLUDE_STATE'] := GROUP_BOX_NO;
  ds['EXCLUDE_SDI'] := GROUP_BOX_NO;
  ds['EXCLUDE_SUI'] := GROUP_BOX_NO;
  ds['EXCLUDE_ADDITIONAL_STATE'] := GROUP_BOX_YES;
  ds['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_NONE;
end;

procedure TMillenniumImportDef.AI_CO_TAX_DEPOSITS(const ds: TevClientDataSet; const Keys: TFieldValues);
begin
//  ds['STATUS'] := TAX_DEPOSIT_STATUS_PENDING;
//  ds['STATUS_DATE'] := Now;
end;

procedure TMillenniumImportDef.CTaxLiab(const Input, Keys, Output: TFieldValues);
  procedure ImportLiability( table: string; tag: string );
  var
    liabStatus: string;
    ref: string;
    p: integer;
    dt: TDateTime;
  begin
    if not VarIsNull( GetParam(sDueDate) ) then
      Output.Add( table, 'DUE_DATE', GetParam(sDueDate) )
    else
      Output.Add( table, 'DUE_DATE', Input['CTaxLiab.liabilityDate'] );

//    Output.Add( table, 'DUE_DATE', Input['CTaxLiab.liabilityDate'] );


    Output.Add( table, 'AMOUNT', Input['CTaxLiab.amount'] );
    Output.Add( table, 'ADJUSTMENT_TYPE', GetParam(sAdjustmentType) );

    Output.Add( table, 'THIRD_PARTY', GROUP_BOX_NO );

    p := Input['CTaxLiab.process'];
    p := p div 100;
    dt := EncodeDate( p div 10000, (p mod 10000) div 100, p mod 100 );

 //   ODS( VarToStr(Input['CTaxLiab.process']) + ' becomes '+ DateToStr(dt) );

    if dt <= StrToDate(GetParam(sProcessDate)) then
      liabStatus := TAX_DEPOSIT_STATUS_PAID
    else
      liabStatus := TAX_DEPOSIT_STATUS_PENDING;

    Output.Add( table, 'STATUS', liabStatus );
    Output.Add( table, 'IMPOUNDED', GetParam(sImpounded) );
    Output.Add( table, 'STATUS_DATE', Now );

    Assert( Length(liabStatus) = 1 );
    if liabStatus[1] in [TAX_DEPOSIT_STATUS_PAID, TAX_DEPOSIT_STATUS_PAID_WO_FUNDS, TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE] then
    begin
      ref := {'Import ' + }tag + ' ' + vartostr(Input['CTaxLiab.process']);
      Keys.Add( 'CO_TAX_DEPOSITS', 'TAX_PAYMENT_REFERENCE_NUMBER', ref ); //so that CO_*_TAX_LIABILITIES will have CO_TAX_DEPOSITS_NBR filled
      Output.Add( 'CO_TAX_DEPOSITS', 'STATUS', TAX_DEPOSIT_STATUS_PAID );
      Output.Add( 'CO_TAX_DEPOSITS', 'STATUS_DATE', Now );
    end;

    Keys.Add( table, 'CHECK_DATE', Input['CTaxLiab.liabilityDate'] );
//    Output.Add( table, 'CHECK_DATE', Input['CTaxLiab.liabilityDate'] );
  end;

  procedure ImportFedLiability( taxType: string; tag: string );
  begin
    ImportLiability( 'CO_FED_TAX_LIABILITIES', tag );
//    Output.Add( 'CO_FED_TAX_LIABILITIES', 'TAX_TYPE', taxType );
    Keys.Add( 'CO_FED_TAX_LIABILITIES', 'TAX_TYPE', taxType ); //after ImportLiability! need topological sort in iemap for keys!!!
  end;
  procedure ImportStateLiability( state: Variant; taxType: string; tag: string );
  begin
    Keys.Add( 'CO_STATES', 'STATE', state );
    Output.Add( 'CO_STATE_TAX_LIABILITIES', 'SY_STATE_DEPOSIT_FREQ_NBR', VarArrayOf(['CO_STATES', 'STATE', state, 'SY_STATE_DEPOSIT_FREQ_NBR']) );
    Output.Add( 'CO_STATE_TAX_LIABILITIES', 'TAX_TYPE', taxType );
    ImportLiability( 'CO_STATE_TAX_LIABILITIES', VarToStr(state) + ' ' + tag );
//    Keys.Add( 'CO_STATE_TAX_LIABILITIES', 'STATE', state );
  end;
  procedure ImportSUILiability( SySUINbr: Variant; tag: string );
  begin
    Keys.Add( 'CO_SUI', 'SY_SUI_NBR', SySUINbr );
    ImportLiability( 'CO_SUI_LIABILITIES', tag );
  end;
  procedure ImportLocalLiability( SyLocalsNbr: Variant );
  begin
    Keys.Add( 'CO_LOCAL_TAX', 'SY_LOCALS_NBR', SyLocalsNbr );
    ImportLiability( 'CO_LOCAL_TAX_LIABILITIES', 'Local ' + VarToStr(SyLocalsNbr)  );
  end;
var
  value: Variant; //'Type;Code;SubType'
begin
  value := Matchers.Get(sTaxesBindingName).RightMatch( Input['CTaxLiab.tcode'] );
  if not VarIsNull(value) then
  begin
    if value[0] = 'F' then
    begin
      if value[2] = 'FE' then
        ImportFedLiability( TAX_LIABILITY_TYPE_FEDERAL, 'FED' )
      else if value[2] = 'OE' then
        ImportFedLiability( TAX_LIABILITY_TYPE_EE_OASDI, 'FED' )
      else if value[2] = 'OR' then
        ImportFedLiability( TAX_LIABILITY_TYPE_ER_OASDI, 'FED' )
      else if value[2] = 'ME' then
        ImportFedLiability( TAX_LIABILITY_TYPE_EE_MEDICARE, 'FED' )
      else if value[2] = 'MR' then
        ImportFedLiability( TAX_LIABILITY_TYPE_ER_MEDICARE, 'FED' )
      else if value[2] = 'FU' then
        ImportFedLiability( TAX_LIABILITY_TYPE_ER_FUI, 'FUI' )
      else if value[2] = 'EI' then
        ImportFedLiability( TAX_LIABILITY_TYPE_EE_EIC, 'FED' )
      else
        Assert(false);
    end
    else if value[0] = 'S' then
    begin
      if value[2] = 'SE' then
        ImportStateLiability( value[1], TAX_LIABILITY_TYPE_EE_SDI, 'SDI' )
      else if value[2] = 'SR' then
        ImportStateLiability( value[1], TAX_LIABILITY_TYPE_ER_SDI, 'SDI' )
      else if value[2] = 'ST' then
        ImportStateLiability( value[1], TAX_LIABILITY_TYPE_STATE, 'ST' )
      else
        Assert(false);
    end
    else if value[0] = 'U' then
    begin
      ImportSUILiability( value[1], VarToStr(value[2]{state}) + ' SUI' );
    end
    else if value[0] = 'Z' then
    begin
      ImportLocalLiability( value[1] );
    end
    else
      Assert(false);
  end;
end;

function TMillenniumImportDef.EEEDs(const TableName: string; const Input, Keys, Output: TFieldValues; const value: Variant): boolean;
var
  CL_E_DS_NBR: Variant;
  startDate: TDateTime;
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_E_DS'));
  CL_E_DS_NBR := FImpEngine.IDS('CL_E_DS').Lookup('CUSTOM_E_D_CODE_NUMBER', value, 'CL_E_DS_NBR');

  if VarIsNull(Input[TableName + '.endDate']) then // EE_SCHEDULED_E_DS BeforeInsert trigger is buggy - it doesn't check the condition below
  begin
    startDate := VarToDateTime( Input[TableName + '.startDate'] );
    FImpEngine.CurrentDataRequired( FImpEngine.IDS('EE_SCHEDULED_E_DS'), 'EE_NBR='+VarToStr(FImpEngine.IDS('EE')['EE_NBR']) + ' and CL_E_DS_NBR='+VarToStr(CL_E_DS_NBR) );
    FImpEngine.IDS('EE_SCHEDULED_E_DS').First;
    while not FImpEngine.IDS('EE_SCHEDULED_E_DS').Eof do
    begin
      if FImpEngine.IDS('EE_SCHEDULED_E_DS')['EFFECTIVE_START_DATE'] <> startDate then
        if VarIsNull( FImpEngine.IDS('EE_SCHEDULED_E_DS')['EFFECTIVE_END_DATE'] ) or
           (FImpEngine.IDS('EE_SCHEDULED_E_DS')['EFFECTIVE_END_DATE'] >= startDate) then
        begin
          Result :=  false;
          Exit;
//        raise EevException.Create('Wrong EInfo.endDate (Null). Same ED scheduled more than once can not overlap');
        end;
      FImpEngine.IDS('EE_SCHEDULED_E_DS').Next;
    end
  end;

  if Input[TableName + '.startDate'] = '12/30/1899' then // = 0
    Logger.LogWarningFmt('Employee %s has suspicious %s start date: 12/30/1899',[VarToStr(Input[TableName + '.ID']),VarToStr(value)]);

  Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', value);

  Output.Add( 'EE_SCHEDULED_E_DS', 'CL_E_DS_NBR', CL_E_DS_NBR );
  Output.Add( 'EE_SCHEDULED_E_DS', 'EFFECTIVE_START_DATE', Input[TableName + '.startDate'] );
  Output.Add( 'EE_SCHEDULED_E_DS', 'EFFECTIVE_END_DATE', Input[TableName + '.endDate'] );
  Output.Add( 'EE_SCHEDULED_E_DS', 'PRIORITY_NUMBER', Input[TableName + '.priority'] );
  Output.Add( 'EE_SCHEDULED_E_DS', 'ANNUAL_MAXIMUM_AMOUNT', Input[TableName + '.annualMaximum'] );
  Output.Add( 'EE_SCHEDULED_E_DS', 'MAXIMUM_PAY_PERIOD_AMOUNT', Input[TableName + '.Maximum'] );
  Output.Add( 'EE_SCHEDULED_E_DS', 'MINIMUM_PAY_PERIOD_AMOUNT', Input[TableName + '.Minimum'] );
  Output.Add( 'EE_SCHEDULED_E_DS', 'CL_AGENCY_NBR', Matchers.Get(sAgencyBindingName).RightMatch( Input[TableName + '.thirdParty'] ) ); //thirdParty is a alias for agency
  if Input[TableName + '.goal'] <> 0 then
  begin
    Output.Add( 'EE_SCHEDULED_E_DS', 'TARGET_ACTION', SCHED_ED_TARGET_REMOVE );
    Output.Add( 'EE_SCHEDULED_E_DS', 'TARGET_AMOUNT', Input[TableName + '.goal'] );
    Output.Add( 'EE_SCHEDULED_E_DS', 'BALANCE', Input[TableName + '.paidTowardsGoal'] );
  end;
  Result := true;
end;

procedure TMillenniumImportDef.ImportDedRateAmount( calcCode, Value: Variant; Output: TFieldValues; import100percAsDeductWholeCheck: boolean );
var
  v: Variant;
begin
  v := MapDedCalcMethod( calcCode );
  if not VarIsNull(v) then
  begin
    if SameText( v[1], 'Amount' ) then //TargetField -- Re #26457
    begin
      Output.Add( 'EE_SCHEDULED_E_DS', 'CALCULATION_TYPE', v[0] );
      Output.Add( 'EE_SCHEDULED_E_DS', 'AMOUNT', Value )
    end
    else
      if import100percAsDeductWholeCheck and (Value = 100) then
      begin
        Output.Add( 'EE_SCHEDULED_E_DS', 'CALCULATION_TYPE', CALC_METHOD_NONE );
        Output.Add( 'EE_SCHEDULED_E_DS', 'DEDUCT_WHOLE_CHECK', GROUP_BOX_YES );
      end
      else
      begin
        Output.Add( 'EE_SCHEDULED_E_DS', 'CALCULATION_TYPE', v[0] );
        Output.Add( 'EE_SCHEDULED_E_DS', 'PERCENTAGE', Value );
      end
  end
  else
  begin
    Output.Add( 'EE_SCHEDULED_E_DS', 'CALCULATION_TYPE', CALC_METHOD_FIXED );
    Output.Add( 'EE_SCHEDULED_E_DS', 'AMOUNT', Value );
  end
end;

procedure TMillenniumImportDef.EEDed(const Input, Keys, Output: TFieldValues);
var
  value: Variant;
begin
  value := MapED('D', Input['EDed.dcode'] );
  if not VarIsNull(value) then
    if EEEDs('EDed', Input, Keys, Output, value) then
      ImportDedRateAmount( Input['EDed.calcCode'], Input['EDed.rate'], Output, false );
end;

procedure TMillenniumImportDef.EEEarn(const Input, Keys, Output: TFieldValues);
var
  value: Variant;
begin
  value := MapED('E', Input['EEarn.ecode'] );
  if not VarIsNull(value) then
    if EEEDs('EEarn', Input, Keys, Output, value) then
    begin
      Output.Add( 'EE_SCHEDULED_E_DS', 'AMOUNT', Input['EEarn.amount'] );
      Output.Add( 'EE_SCHEDULED_E_DS', 'PERCENTAGE', Input['EEarn.rate'] );
      value := MapEarnCalcMethod(Input['EEarn.calcCode']);
      if VarToStr(Value) <> '' then
        Output.Add( 'EE_SCHEDULED_E_DS', 'CALCULATION_TYPE', value );
    end
end;

function ZeroAsNull( v: Variant): Variant;
begin
  Result := v;
  if Result = 0 then
    Result := Null;
end;

procedure TMillenniumImportDef.Ytd(const Input, Keys, Output: TFieldValues);
var
  value: Variant;
  amount: Variant;
  hours: Variant;
  det: char;
begin
  GetAmounts( Input, amount, hours );

  det := GetSingleChar( Input['EYtd.det'], 'EYtd.det', ['T','E','D']);

  if det = 'T' then
  begin
    value := Matchers.Get(sTaxesBindingName).RightMatch( Input['EYtd.detcode'] );
    if not VarIsNull(value) then
    begin
      if value[0] = 'F' then
      begin
        if value[2] = 'FE' then
          Output.Add( 'PR_CHECK', 'FEDERAL_TAX', amount )
        else if value[2] = 'OE' then
          Output.Add( 'PR_CHECK', 'EE_OASDI_TAX', amount )
        else if value[2] = 'OR' then
          Output.Add( 'PR_CHECK', 'ER_OASDI_TAX', ZeroAsNull(amount) )
        else if value[2] = 'ME' then
          Output.Add( 'PR_CHECK', 'EE_MEDICARE_TAX', amount )
        else if value[2] = 'MR' then
          Output.Add( 'PR_CHECK', 'ER_MEDICARE_TAX', ZeroAsNull(amount) )
        else if value[2] = 'FU' then
          Output.Add( 'PR_CHECK', 'ER_FUI_TAX', ZeroAsNull(amount) )
        else if value[2] = 'EI' then
          Output.Add( 'PR_CHECK', 'EE_EIC_TAX', amount );
      end
      else if value[0] = 'S' then
      begin
       FImpEngine. CurrentDataRequired(FImpEngine.IDS('CO_STATES') );
        Keys.Add('EE_STATES', 'CO_STATES_NBR', FImpEngine.IDS('CO_STATES').Lookup('CO_NBR;STATE', VarArrayOf([FImpEngine.IDS('CO')['CO_NBR'], Value[1]]), 'CO_STATES_NBR'));
        if value[2] = 'ST' then
          Output.Add( 'PR_CHECK_STATES', 'STATE_TAX', amount )
        else if value[2] = 'SE' then
          Output.Add( 'PR_CHECK_STATES', 'EE_SDI_TAX', amount )
        else
          Output.Add( 'PR_CHECK_STATES', 'ER_SDI_TAX', ZeroAsNull(amount) );
      end
      else if value[0] = 'U' then
      begin
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_SUI'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']));
        Keys.Add( 'PR_CHECK_SUI', 'CO_SUI_NBR', FImpEngine.IDS('CO_SUI').Lookup('SY_SUI_NBR', Value[1], 'CO_SUI_NBR') );
        Output.Add( 'PR_CHECK_SUI', 'SUI_TAX', ZeroAsNull(amount) );
      end
      else if value[0] = 'Z' then
      begin
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_LOCAL_TAX'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']));
        Keys.Add( 'EE_LOCALS', 'CO_LOCAL_TAX_NBR', FImpEngine.IDS('CO_LOCAL_TAX').Lookup('SY_LOCALS_NBR', Value[1], 'CO_LOCAL_TAX_NBR') );
        Output.Add( 'PR_CHECK_LOCALS', 'LOCAL_TAX', amount );
      end;
    end;
  end
  else
  begin
    value := MapED( det, Input['EYtd.detcode']);
    if not VarIsNull(value) then
    begin
      Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', value);
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('CL_E_DS'));
      Output.Add( 'PR_CHECK_LINES', 'CL_E_DS_NBR', FImpEngine.IDS('CL_E_DS').Lookup('CUSTOM_E_D_CODE_NUMBER', value, 'CL_E_DS_NBR') );
      Output.Add( 'PR_CHECK_LINES', 'HOURS_OR_PIECES', hours );
      Output.Add( 'PR_CHECK_LINES', 'AMOUNT', amount );
      Inc(CheckLineNbr);
      Keys.Add( 'PR_CHECK_LINES', 'E_D_DIFFERENTIAL_AMOUNT', CheckLineNbr );
    end;
  end;
end;

procedure TMillenniumImportDef.AI_EE(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  if UpperCase(Self.ClassName) = 'TM3IMPORTDEF' then
  begin
    ds.FieldByName('EXEMPT_EXCLUDE_EE_FED').AsString := GROUP_BOX_INCLUDE;
    ds.FieldByName('EXEMPT_EMPLOYEE_OASDI').AsString := GROUP_BOX_NO;
    ds.FieldByName('EXEMPT_EMPLOYER_OASDI').AsString := GROUP_BOX_NO;
    ds.FieldByName('EXEMPT_EMPLOYEE_MEDICARE').AsString := GROUP_BOX_NO;
    ds.FieldByName('EXEMPT_EMPLOYER_MEDICARE').AsString := GROUP_BOX_NO;
    ds.FieldByName('EXEMPT_EMPLOYER_FUI').AsString := GROUP_BOX_NO;
  end
  else begin
    ds.FieldByName('EXEMPT_EXCLUDE_EE_FED').AsString := GROUP_BOX_EXCLUDE;
    ds.FieldByName('EXEMPT_EMPLOYEE_OASDI').AsString := GROUP_BOX_YES;
    ds.FieldByName('EXEMPT_EMPLOYER_OASDI').AsString := GROUP_BOX_YES;
    ds.FieldByName('EXEMPT_EMPLOYEE_MEDICARE').AsString := GROUP_BOX_YES;
    ds.FieldByName('EXEMPT_EMPLOYER_MEDICARE').AsString := GROUP_BOX_YES;
    ds.FieldByName('EXEMPT_EMPLOYER_FUI').AsString := GROUP_BOX_YES;
  end;

  ds.FieldByName('FEDERAL_MARITAL_STATUS').AsString := GROUP_BOX_SINGLE;
  ds.FieldByName('NUMBER_OF_DEPENDENTS').AsInteger := 0;
  ds.FieldByName('OVERRIDE_FED_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
  ds.FieldByName('EIC').AsString := EIC_NONE;
  ds['NEW_HIRE_REPORT_SENT'] := NEW_HIRE_REPORT_COMP_BY_PREDECESSOR;
end;

procedure TMillenniumImportDef.EInfoSalary(const Input, Keys,
  Output: TFieldValues);
begin
  if not VarIsNull(Input['EInfoSalary.Salary']) and (Input['EInfoSalary.Salary'] <> 0) then //look at the inverse condition in the ImportPrimaryRate
  begin
    Output.Add('EE', 'SALARY_AMOUNT', Input['EInfoSalary.Salary'] );
    Keys.Add('EE_RATES', 'RATE_NUMBER', 0);
    Output.Add('EE_RATES', 'RATE_AMOUNT', 0);
    Output.Add('EE_RATES', 'PRIMARY_RATE', GROUP_BOX_YES);
  end;
end;

procedure TMillenniumImportDef.ImportPrimaryRate(const Input, Keys,
  Output: TFieldValues);
var
  value: Variant;
begin
  value := Matchers.Get(sRatePerBindingName).RightMatch( Input['EInfo.ratePer'] );
  if InSet(VarToStr(value), ['H', 'D', 'W'] ) then
  begin
    Keys.Add('EE_RATES', 'RATE_NUMBER', 1);
    case VarToStr(value)[1] of
      'H': Output.Add('EE_RATES', 'RATE_AMOUNT', Input['EInfo.baseRate']);
      'D': Output.Add('EE_RATES', 'RATE_AMOUNT', RoundAll(Input['EInfo.baseRate']/8, 2));
      'W': if (VarToStr(Input['EInfo.defaultHours']) <> '') and (Input['EInfo.defaultHours'] <> 0) then
             Output.Add('EE_RATES', 'RATE_AMOUNT', RoundAll(Input['EInfo.baseRate']/Input['EInfo.defaultHours'], 2))
           else
             Output.Add('EE_RATES', 'RATE_AMOUNT', RoundAll(Input['EInfo.baseRate']/40, 2));
    end;
    if VarIsNull(Input['EInfo.Salary']) or (Input['EInfo.Salary'] = 0) then //look at the inverse condition in the EInfoSalary
      Output.Add('EE_RATES', 'PRIMARY_RATE', GROUP_BOX_YES)
    else
      Output.Add('EE_RATES', 'PRIMARY_RATE', GROUP_BOX_NO);
  end
end;

procedure TMillenniumImportDef.ImportEERate(const Input, Keys,
  Output: TFieldValues);
var
  value: Variant;
begin
  value := null;
  try
    value := IntToStr(Input['ERate.rateCode']);
    if value < 2 then
      AbortEx;
  except
    value := Matchers.Get(sRateCodeBindingName).RightMatch( Input['ERate.rateCode'] );
  end;
  if VarToStr(value) = '' then
    Exit;
  Keys.Add('EE_RATES', 'RATE_NUMBER', value);

  value := Matchers.Get(sRatePerBindingName).RightMatch( Input['ERate.ratePer'] );
  if (VarToStr(value) = '') or (value = 'H') then
    Output.Add('EE_RATES', 'RATE_AMOUNT', Input['ERate.rate'])
  else if value = 'D' then
    Output.Add('EE_RATES', 'RATE_AMOUNT', RoundAll(Input['ERate.Rate']/8, 2))
  else if value = 'W' then
    if FImpEngine.IDS('EE').FieldbyName('STANDARD_HOURS').AsFloat <> 0 then
      Output.Add('EE_RATES', 'RATE_AMOUNT', RoundAll(Input['ERate.rate']/FImpEngine.IDS('EE').FieldbyName('STANDARD_HOURS').AsFloat, 2))
    else
      Output.Add('EE_RATES', 'RATE_AMOUNT', RoundAll(Input['ERate.rate']/40, 2));

  if VarToStr(Input['ERate.jobCode']) <> '' then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_JOBS'),
      Format('(CO_NBR=%s) and (DESCRIPTION=''%s'')',
        [VarToStr(FImpEngine.IDS('CO')['CO_NBR']), VarToStr(Input['ERate.jobCode']) ]) );
    Output.Add('EE_RATES', 'CO_JOBS_NBR', FImpEngine.IDS('CO_JOBS')['CO_JOBS_NBR']);
  end;
end;

const
  evo: array [0..3] of record table: string; field: string; printfield: string; hidden: string end =
  (
    (table: 'CO_DIVISION'; field: 'CUSTOM_DIVISION_NUMBER'; printfield: 'PRINT_DIV_ADDRESS_ON_CHECKS'; hidden: sHiddenDivisionCode ),
    (table: 'CO_BRANCH'; field: 'CUSTOM_BRANCH_NUMBER'; printfield: 'PRINT_BRANCH_ADDRESS_ON_CHECK'; hidden: sHiddenBranchCode ),
    (table: 'CO_DEPARTMENT'; field: 'CUSTOM_DEPARTMENT_NUMBER'; printfield: 'PRINT_DEPT_ADDRESS_ON_CHECKS'; hidden: sHiddenDepartmentCode ),
    (table: 'CO_TEAM'; field: 'CUSTOM_TEAM_NUMBER'; printfield: 'PRINT_GROUP_ADDRESS_ON_CHECK')
  );

procedure TMillenniumImportDef.SetDBDTDefaults(const ds: TevClientDataSet);
var
  i: integer;
begin
  if FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').IsNull then
  begin
    ds.Cancel;  //!! wtf?!
    RaiseMissingValue( 'Please define company Home State for %s', [ VarToStr(FImpEngine.IDS('CO')['CUSTOM_COMPANY_NUMBER']) ] );
  end;
  ds.FieldByName('HOME_CO_STATES_NBR').AsInteger := FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').AsInteger;
  ds.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  ds.FieldByName('PAY_FREQUENCY_HOURLY').AsString :=  FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
  ds.FieldByName('PAY_FREQUENCY_SALARY').AsString := FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_SALARY_DEFAULT').AsString;

  for i := low(evo) to high(evo) do
    if evo[i].table = ds.Name then
    begin
      ds.FieldByName(evo[i].printfield).AsString := EXCLUDE_REPT_AND_ADDRESS_ON_CHECK;
      break;
    end
end;

procedure TMillenniumImportDef.AI_CO_BRANCH(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  SetDBDTDefaults(ds);
end;

procedure TMillenniumImportDef.AI_CO_DEPARTMENT(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  SetDBDTDefaults(ds);
end;

procedure TMillenniumImportDef.AI_CO_DIVISION(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  SetDBDTDefaults(ds);
end;

procedure TMillenniumImportDef.AI_CO_TEAM(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  SetDBDTDefaults(ds);
end;

function TMillenniumImportDef.DBDTMap: TVariantArray;
var
  m: IMatcher;
  h: integer;
begin
  m := Matchers.Get(sDBDTBindingName);
  SetLength(Result,4);
  Result[0] := m.LeftMatch(1);
  Result[1] := m.LeftMatch(2);
  Result[2] := m.LeftMatch(3);
  Result[3] := m.LeftMatch(4);
  h := 3;
  while (h > 0) and VarIsNull(Result[h]) do
    dec(h);
  SetLength(Result, h+1);
end;

procedure TMillenniumImportDef.HelpImportDBDT(const Input, Output: TFieldValues; m3table, desttable: string );
var
  i: integer;
  m: TVariantArray;
  dbdt: array [0..3] of Variant;
  value: Variant;
  cNotNullCC: integer;
begin
  for i := 0 to 3 do
    dbdt[i] := Null;

  m := DBDTMap;
  
  cNotNullCC := 0;
  for i := 0 to high(m) do
    if not VarIsNull(m[i]) then
      if not VarIsNull(Input[m3table+'.cc'+inttostr(m[i])]) and (trim( Input[m3table+'.cc'+inttostr(m[i])] ) <> '_') then //!!
        inc(cNotNullCC);

  if cNotNullCC > 0 then
    for i := 0 to high(m) do
    begin
      if i = 0 then
        FImpEngine.CurrentDataRequired( FImpEngine.IDS(evo[i].table), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) )
      else
        FImpEngine.CurrentDataRequired( FImpEngine.IDS(evo[i].table), evo[i-1].table + '_NBR=' + VarToStr(dbdt[i-1]) );
      if VarIsNull(m[i]) then
        value := evo[i].hidden
      else
        value := Input[m3table+'.cc'+inttostr(m[i])];
      dbdt[i] := FImpEngine.IDS(evo[i].table).Lookup( evo[i].field, value, evo[i].table + '_NBR' );
      if VarIsNull(dbdt[i]) then
      begin
        Logger.LogWarningFmt( 'Cannot find <%s> (from cc%s) in %s', [VarToStr(value), VarToStr(m[i]), evo[i].table ] );
        break;
      end
    end;

  for i := 0 to 3 do
    Output.Add( desttable, evo[i].table+'_NBR', dbdt[i] );
end;

procedure TMillenniumImportDef.CAllDept(const Input, Keys, Output: TFieldValues);
var
  i: integer;
  m: TVariantArray;
  bNotAllNull: boolean;
begin
  m := DBDTMap;
  bNotAllNull := false;
  for i := 0 to high(m) do
    if not VarIsNull(m[i]) and (trim(Input['CAllDept.cc'+inttostr(m[i])]) <> '_') then
    begin
      bNotAllNull := true;
      break;
    end;

  if not bNotAllNull then
    Exit;

  for i := 0 to high(m) do
    if VarIsNull(m[i]) then
    begin
      Keys.Add( evo[i].table, evo[i].field, evo[i].hidden);
      Output.Add( evo[i].table, evo[i].printfield, EXCLUDE_REPT_AND_ADDRESS_ON_CHECK);
    end
    else
    begin
      Keys.Add( evo[i].table, evo[i].field, Input['CAllDept.cc'+inttostr(m[i])] );
      Output.Add( evo[i].table, 'NAME', Input['CAllDept.NAME'+inttostr(m[i])]);
      Output.Add( evo[i].table, evo[i].printfield, INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK);
    end
end;

//all but dbdt
procedure TMillenniumImportDef.ImportEEAdditionalInfo(const Input, Keys,
  Output: TFieldValues);
var
  hd: Variant;
begin
  Output.AddNotNull( 'CL_PERSON', 'ETHNICITY', Matchers.Get(sEthnicityBindingName).RightMatch( Input['EInfo.Ethnicity'] ) );
  Output.AddNotNull( 'EE', 'CURRENT_TERMINATION_CODE', Matchers.Get(sEmpStatusBindingName).RightMatch( Input['EInfo.empStatus'] ) );
  Output.AddNotNull( 'EE', 'PAY_FREQUENCY', Matchers.Get(sPayFreqBindingName).RightMatch( Input['EInfo.PayFrequency'] ) );
  Output.AddNotNull( 'EE', 'POSITION_STATUS', Matchers.Get(sEmpTypeBindingName).RightMatch( Input['EInfo.empType'] ) );

  if LeftStr(Input['EInfo.SSN'], 3) = 'xxx' then
    Logger.LogWarningFmt('Employee %s does not have a correct Social Security Number. Please change it.', [Input['EInfo.ID']]);
  if VarToStr(Input['EInfo.Address1']) = '' then
  begin
    Output.Add('CL_PERSON', 'ADDRESS1', '<ADDRESS>');
    Logger.LogWarningFmt('Employee %s does not have a correct address. Please change it.', [Input['EInfo.ID']]);
  end;
  if VarToStr(Input['EInfo.CITY']) = '' then
  begin
    Output.Add('CL_PERSON', 'CITY', '<CITY>');
    Logger.LogWarningFmt('Employee %s does not have a correct city. Please change it.', [Input['EInfo.ID']]);
  end;
  if VarToStr(Input['EInfo.ZIP']) = '' then
  begin
    Output.Add('CL_PERSON', 'ZIP_CODE', '<ZIP>');
    Logger.LogWarningFmt('Employee %s does not have a correct zip code. Please change it.', [Input['EInfo.ID']]);
  end;

  if VarToStr(Input['EInfo.HIREDATE']) <> '' then
    hd := Input['EInfo.HIREDATE']
  else
  begin
    hd := EncodeDate( YearOf(Now), 1, 1);
    Logger.LogWarningFmt('Employee %s does not have a correct hire date. Please change it.', [Input['EInfo.ID']]);
  end;
  Output.Add( 'EE', 'ORIGINAL_HIRE_DATE', hd );
  if VarToStr(Input['EInfo.REHIREDATE']) = '' then
    Output.Add( 'EE', 'CURRENT_HIRE_DATE', hd )
  else
    Output.Add( 'EE', 'CURRENT_HIRE_DATE', Input['EInfo.REHIREDATE'] );

  if VarToStr(Input['EInfo.TITLE']) <> '' then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_HR_POSITIONS'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
    if FImpEngine.IDS('CO_HR_POSITIONS').Locate('DESCRIPTION', Input['EInfo.TITLE'], [loCaseInsensitive]) then
      Output.Add( 'EE', 'CO_HR_POSITIONS_NBR', FImpEngine.IDS('CO_HR_POSITIONS')['CO_HR_POSITIONS_NBR'] )
    else
      Logger.LogWarningFmt('Position %s for Employee %s could not be found in Evolution', [Input['EInfo.TITLE'], Input['EInfo.ID']]);
  end;

  Output.AddNotNull('EE', 'SY_HR_EEO_NBR', Matchers.Get(sEEOBindingName).RightMatch( Input['EInfo.eeoClass'] ));
  ImportPrimaryRate(Input, Keys, Output);
end;

function TMillenniumImportDef.MapED(eord: char; val: Variant): Variant;
begin
  Result := Matchers.Get(sEDBindingName).RightMatch( VarArrayOf( [eord, val ]) );
end;

function EmptyStrAsNull( s: string): Variant;
begin
  if s = '' then
    Result := Null
  else
    Result := s;
end;

procedure TMillenniumImportDef.AfterEEStatesImport;
var
  i: integer;
  s: string;
  p: integer;
  ee: string;
  state: string;
begin
  Assert( assigned(FCreatedEEStates) );
  Assert( assigned(FEEStatesWithMaritalStatus) );

  for i := 0 to FCreatedEEStates.Count-1 do
  begin
    s := FCreatedEEStates[i];
    if FEEStatesWithMaritalStatus.IndexOf(s) = -1 then
    begin
      //s must contain '@'
      for p := Length(s) downto 1 do
        if s[p] = '@' then
          break;
      ee := copy(s, 1, p-1);
      state := copy(s, p + 1, MaxInt);
      Logger.LogWarningFmt('Employee %s: State Marital Status in %s has default value.', [ee, state]);
    end
  end;
  FreeAndNil( FCreatedEEStates );
  FreeAndNil( FEEStatesWithMaritalStatus );
end;

procedure TMillenniumImportDef.BeforeEEStatesImport;
begin
  Assert( not assigned(FCreatedEEStates) );
  Assert( not assigned(FEEStatesWithMaritalStatus) );
  FCreatedEEStates := TStringList.Create;
  FEEStatesWithMaritalStatus := TStringList.Create;
  FEEStatesWithMaritalStatus.Sorted := true;
end;

end.
