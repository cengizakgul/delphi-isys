unit SM2ImportDef;

interface

uses
  genericImport, genericImportBaseImpl, Classes, SDM_IM_M2, genericImportDefinitionImpl,
  gdyBinder, sImportVisualController, sImportHelpers,  IEMap, Variants,
  EvConsts, StrUtils, DB, SysUtils, EvUtils, DateUtils, typinfo, sMCommonImportDef;

const
  sDedBindingName = 'DedCalc-new';
//  sDedBindingName = 'DedCalc';
  sEarnBindingName = 'EarnCalc';

function CreateM2CO_AND_EEImport( aLogger: IImportLogger ): IImport;
function CreateM2PRImport( aLogger: IImportLogger ): IImport;

implementation

const
  M2Conversion: array [0..3] of TConvertDesc =
  (
    (Table: 'CDeptTied'; Field:'cc1'; Func: ConvertDBDTCustomNumber),
    (Table: 'CDeptTied'; Field:'cc2'; Func: ConvertDBDTCustomNumber),
    (Table: 'CDeptTied'; Field:'cc3'; Func: ConvertDBDTCustomNumber),
    (Table: 'CDeptTied'; Field:'cc4'; Func: ConvertDBDTCustomNumber)
  );

  M2Keys: array [0..0] of TKeySourceDesc =
  (
    (ExternalTable: 'CDeptTied'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue )
  );

  M2RowImport: array [0..0] of TRowImportDesc =
  (
    ( ExternalTable: 'CDeptTied'; CustomFunc: 'DBDTTied')
  );

type

  TM2ImportDef = class( TMillenniumImportDef )
  private
    bDeptTied: Boolean;
  protected
    function MapDedCalcMethod( val: Variant): Variant; override;
    function MapEarnCalcMethod( val: Variant): Variant; override;
  public
    constructor Create(const AIEMap: IIEMap); override;
  published
//    procedure DBDTUntied(const Input, Keys, Output: TFieldValues);
    procedure DBDTTied(const Input, Keys, Output: TFieldValues);
    procedure EEAdditionalInfo(const Input, Keys, Output: TFieldValues);
    procedure EERate(const Input, Keys, Output: TFieldValues);
  end;

const
  M2CO_AND_EEimport: TImportDesc = (
    ImportName: 'M2 Employee Import';
    RequiredMatchersNames: sEthnicityBindingName + #13#10 +
                           sEmpStatusBindingName + #13#10 +
                           sPayFreqBindingName + #13#10 +
                           sEmpTypeBindingName + #13#10 +
                           sTaxesBindingName + #13#10 +
                           sRatePerBindingName + #13#10 +
                           sEEOBindingName + #13#10 +
                           sRateCodeBindingName + #13#10 +
                           sReciprocalBindingName + #13#10 +
                           sEDBindingName + #13#10 +
                           sDedBindingName + #13#10 +
                           sEarnBindingName + #13#10 +
                           sAgencyBindingName + #13#10 +
                           sDBDTBindingName + #13#10 +
                           sMDLocalsBindingName ;

    ExternalDataModuleImplClass: TDM_IM_M2;
    ImportDefinitionImplClass: TM2ImportDef
  );
  M2PRimport: TImportDesc = (
    ImportName: 'M2 Payroll Import';
    RequiredMatchersNames: sTaxesBindingName + #13#10 +
                           sEmpStatusBindingName + #13#10 +
                           sEDBindingName;
    ExternalDataModuleImplClass: TDM_IM_M2;
    ImportDefinitionImplClass: TM2ImportDef
  );
type
  TM2CO_AND_EEImport = class (TMillenniumImport)
  protected
    procedure DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings ); override;
  end;

  TM2PRImport = class (TMillenniumImport)
  protected
    procedure DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings ); override;
  end;

const
  M2BindingDesc: array [0..14] of TBindingDesc =
    (
      ( Name: sEthnicityBindingName;
        Caption: 'Mapped Ethnicity';
        LeftDesc: ( Caption: 'M2 Ethnicity'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution Ethnicity'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sEmpStatusBindingName;
        Caption: 'Mapped Emp Status';
        LeftDesc: ( Caption: 'M2 Emp Status'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution Emp Status'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sPayFreqBindingName;
        Caption: 'Mapped Pay Frequency';
        LeftDesc: ( Caption: 'M2 Pay Frequency'; Unique: true; KeyFields: 'Frequency'; ListFields: 'Frequency;Description');
        RightDesc: ( Caption: 'Evolution Pay Frequency'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sEmpTypeBindingName;
        Caption: 'Mapped Emp Type';
        LeftDesc: ( Caption: 'M2 Emp Type'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution Emp Type'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sTaxesBindingName;
        Caption: 'Mapped Taxes';
        LeftDesc: ( Caption: 'M2 Taxes'; Unique: true; KeyFields: 'tcode'; ListFields: 'tcode;Description');
        RightDesc: ( Caption: 'Evolution Taxes'; Unique: false; KeyFields: 'Type;Code;SubType'; ListFields: 'Type;Name')
      ),
      ( Name: sRatePerBindingName;
        Caption: 'Mapped Rate Per';
        LeftDesc: ( Caption: 'M2 Rate Per'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution Rate Per'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sEEOBindingName;
        Caption: 'Mapped EEO Class';
        LeftDesc: ( Caption: 'M2 EEO Class'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution EEO Class'; Unique: false; KeyFields: 'SY_HR_EEO_NBR'; ListFields: 'SY_HR_EEO_NBR;Description')
      ),
      ( Name: sRateCodeBindingName;
        Caption: 'Mapped Rate Code';
        LeftDesc: ( Caption: 'M2 Rate Code'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution Rate Number'; Unique: true; KeyFields: 'number'; ListFields: 'number')
      ),
      ( Name: sReciprocalBindingName;
        Caption: 'Mapped Reciprocal';
        LeftDesc: ( Caption: 'M2 Reciprocal'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution Reciprocal'; Unique: false; KeyFields: 'code'; ListFields: 'code;name')
      ),
      ( Name: sEDBindingName;
        Caption: 'Mapped E/Ds';
        LeftDesc: ( Caption: 'M2 E/Ds'; Unique: true; KeyFields: 'eord;code'; ListFields: 'eord;code;Description');
        RightDesc: ( Caption: 'Evolution E/Ds'; Unique: false; KeyFields: 'ED_Lookup'; ListFields: 'ED_Lookup;CodeDescription')
      ),
      ( Name: sDedBindingName;
        Caption: 'Mapped Ded Calc Type';
        LeftDesc: ( Caption: 'M2 Ded Calc Type'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution Ded Calc Type'; Unique: false; KeyFields: 'code;TargetField'; ListFields: 'code;name;TargetField')
      ),
      ( Name: sEarnBindingName;
        Caption: 'Mapped Earn Calc Type';
        LeftDesc: ( Caption: 'M2 Earn Calc Type'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution Earn Calc Type'; Unique: false; KeyFields: 'code'; ListFields: 'code;name')
      ),
      ( Name: sAgencyBindingName;
        Caption: 'Mapped Agencies';
        LeftDesc: ( Caption: 'M2 Payees'; Unique: true; KeyFields: 'id'; ListFields: 'id;name');
        RightDesc: ( Caption: 'Evolution Agencies'; Unique: true; KeyFields: 'CL_AGENCY_NBR'; ListFields: 'Agency_Name')
      ),
      ( Name: sDBDTBindingName;
        Caption: 'Mapped DBDT';
        LeftDesc: ( Caption: 'M2 CC'; Unique: true; KeyFields: 'Number'; ListFields: 'Category;Number;Desc');
        RightDesc: ( Caption: 'Evolution DBDT'; Unique: true; KeyFields: 'Level'; ListFields: 'Level;DBDT')
      ),
      ( Name: sMDLocalsBindingName;
        Caption: 'Mapped MD locals';
        LeftDesc: ( Caption: 'M2 MD Locals'; Unique: true; KeyFields: 'tcode'; ListFields: 'tcode;Description');
        RightDesc: ( Caption: 'Evolution MD Marital Statuses'; Unique: true; KeyFields: 'STATUS_TYPE'; ListFields: 'STATUS_TYPE;STATUS_DESCRIPTION')
      )
    );

function CreateM2CO_AND_EEImport( aLogger: IImportLogger ): IImport;
begin
  Result := TM2CO_AND_EEImport.Create( aLogger, M2CO_AND_EEimport, M2BindingDesc );
end;

function CreateM2PRImport( aLogger: IImportLogger ): IImport;
begin
  Result := TM2PRImport.Create( aLogger, M2PRimport, M2BindingDesc );
end;

{ TM2CO_AND_EEImport }

procedure TM2CO_AND_EEImport.DoRunImport(
  aImportDefinition: IImportDefinition; aParams: TStrings);
var
  EETerm: Boolean;
  empStatus: variant;
  sFilter: string;
  i: Integer;
  m2dm: IM2DataModule;
begin
  empStatus := aImportDefinition.Matchers.Get(sEmpStatusBindingName).LeftMatch( EE_TERM_ACTIVE );
  if not VarIsArray(empStatus) then
    sFilter := 'empStatus=''' + empStatus + ''''
  else
  begin
    sFilter := '';
    for i := 0 to VarArrayHighBound(empStatus, 1) do
      sFilter := sFilter + ' or empStatus=''' + empStatus[i] + '''';
    if sFilter <> '' then
      sFilter := RightStr(sFilter, Length(sFilter) - 4);
  end;

  EETerm := aParams.Values[sEETermedParam] = 'Y';
  with DataModule.GetTable('EInfo') do
  begin
    Filter := sFilter;
    Filtered := EETerm;
  end;
  with DataModule.GetTable('ETax') do
  begin
    Filter := sFilter;
    Filtered := EETerm;
  end;
  with DataModule.GetTable('ERate') do
  begin
    Filter := sFilter;
    Filtered := EETerm;
  end;
  with DataModule.GetTable('EDirDep') do
  begin
    Filter := sFilter;
    Filtered := EETerm;
  end;
  with DataModule.GetTable('EDed') do
  begin
    Filter := sFilter;
    Filtered := EETerm;
  end;
  with DataModule.GetTable('EEarn') do
  begin
    Filter := sFilter;
    Filtered := EETerm;
  end;

  m2dm := DataModuleInstance as IM2DataModule; //DataModule as IM2DataModule
  // the best way to handle this is to inherit each import DataModule from DM with ref counting
  m2dm.FilterLiabilities( aParams.Values[sOnlyThisYear] = 'Y', EncodeDate(YearOf(Now),1,1) );


  inherited;
  aImportDefinition.SetFixedKeys( 'CO.CUSTOM_COMPANY_NUMBER=' + DataModule.CustomCompanyNumber);

  aImportDefinition.SetParam( sAdjustmentType, EmptyStrAsNull(aParams.Values[sAdjustmentType]) );
  aImportDefinition.SetParam( sDueDate, EmptyStrAsNull(aParams.Values[sDueDate]) );
  aImportDefinition.SetParam( sImpounded, EmptyStrAsNull(aParams.Values[sImpounded]) );
  aImportDefinition.SetParam( sProcessDate, EmptyStrAsNull(aParams.Values[sProcessDate]) );
  if DataModule.GetTable('CInfo').FieldByName('CCLevelsTied').AsBoolean then
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'CDeptTied', 'Importing DBDT' )
  else
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'CAllDept', 'Importing DBDT', true );

  (aImportDefinition as IMillenniumImportDefinition).BeforeEEStatesImport;
  try
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EInfo', 'Importing employees' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EInfoSalary', 'Importing salaries' );

    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'ETax', 'Importing EE taxes' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EInfoFix', 'Creating missing home states' );
  finally
    (aImportDefinition as IMillenniumImportDefinition).AfterEEStatesImport;
  end;

  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'ERate', 'Importing EE rates' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EDirDep', 'Importing direct deposits' );

  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EDed', 'Importing deductions' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EEarn', 'Importing earnings' );

  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'CTaxLiab', 'Importing tax liabilities', true );

end;

{ TM2PRImport }

procedure TM2PRImport.DoRunImport(aImportDefinition: IImportDefinition; aParams: TStrings);
var
  EETerm: Boolean;
  empStatus: variant;
  sFilter: string;
  i: Integer;
begin
  empStatus := aImportDefinition.Matchers.Get(sEmpStatusBindingName).LeftMatch( EE_TERM_ACTIVE );
  if not VarIsArray(empStatus) then
    sFilter := 'empStatus=''' + empStatus + ''''
  else
  begin
    sFilter := '';
    for i := 0 to VarArrayHighBound(empStatus, 1) do
      sFilter := sFilter + ' or empStatus=''' + empStatus[i] + '''';
    if sFilter <> '' then
      sFilter := RightStr(sFilter, Length(sFilter) - 4);
  end;

  EETerm := aParams.Values[sEETermedParam] = 'Y';
  with DataModule.GetTable('EYtd') do
  begin
    Filter := sFilter;
    Filtered := EETerm;
  end;
  inherited;
  RunPrImport( aImportDefinition, aParams );
end;

{ TM2ImportDef }

procedure TM2ImportDef.EEAdditionalInfo(const Input, Keys, Output: TFieldValues);
var
  division: Variant;
  branch: Variant;
  department: Variant;
  team: Variant;
begin
  ImportEEAdditionalInfo( Input, Keys, Output);

  if not bDeptTied then
    HelpImportDBDT( Input, Output, 'EInfo', 'EE' )
  else
  begin
    division := null;
    branch := null;
    department := null;
    team := null;
    if VarToStr(Input['EInfo.cc5']) = '' then
    begin
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DIVISION'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
      division := FImpEngine.IDS('CO_DIVISION').Lookup( 'CUSTOM_DIVISION_NUMBER', Input['EInfo.cc1'], 'CO_DIVISION_NBR' );
      if not VarIsNull(division) then
      begin
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_BRANCH'), 'CO_DIVISION_NBR='+VarToStr(division) );
        branch := FImpEngine.IDS('CO_BRANCH').Lookup( 'CUSTOM_BRANCH_NUMBER', Input['EInfo.cc2'], 'CO_BRANCH_NBR' );
      end;
      if not VarIsNull(branch) then
      begin
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DEPARTMENT'), 'CO_BRANCH_NBR='+VarToStr(branch) );
        department := FImpEngine.IDS('CO_DEPARTMENT').Lookup( 'CUSTOM_DEPARTMENT_NUMBER', Input['EInfo.cc3'], 'CO_DEPARTMENT_NBR' );
      end;
      if not VarIsNull(department) then
      begin
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_TEAM'), 'CO_DEPARTMENT_NBR='+VarToStr(department) );
        team := FImpEngine.IDS('CO_TEAM').Lookup( 'CUSTOM_TEAM_NUMBER', Input['EInfo.cc4'], 'CO_TEAM_NBR' );
      end;
    end;
    Output.Add( 'EE', 'CO_DIVISION_NBR', division );
    Output.Add( 'EE', 'CO_BRANCH_NBR', branch );
    Output.Add( 'EE', 'CO_DEPARTMENT_NBR', department );
    Output.Add( 'EE', 'CO_TEAM_NBR', team );
  end;
end;

constructor TM2ImportDef.Create(const AIEMap: IIEMap);
begin
  inherited Create(AIEMap);
  
  RegisterConvertDescs( M2Conversion );
  RegisterKeySourceDescs( M2Keys );
  RegisterRowImport(M2RowImport);
//  RegisterMap( M2Map );
end;

procedure TM2ImportDef.DBDTTied(const Input, Keys, Output: TFieldValues);
begin
  bDeptTied := True;
  if Trim(VarToStr(Input['CDeptTied.cc1'])) <> '_' then
    Keys.Add( 'CO_DIVISION', 'CUSTOM_DIVISION_NUMBER', Input['CDeptTied.cc1']);
  if Trim(VarToStr(Input['CDeptTied.cc2'])) <> '_' then
    Keys.Add( 'CO_BRANCH', 'CUSTOM_BRANCH_NUMBER', Input['CDeptTied.cc2']);
  if Trim(VarToStr(Input['CDeptTied.cc3'])) <> '_' then
    Keys.Add( 'CO_DEPARTMENT', 'CUSTOM_DEPARTMENT_NUMBER', Input['CDeptTied.cc3']);
  if Trim(VarToStr(Input['CDeptTied.cc4'])) <> '_' then
    Keys.Add( 'CO_TEAM', 'CUSTOM_TEAM_NUMBER', Input['CDeptTied.cc4']);

  if VarToStr(Input['CDeptTied.cc5']) <> '' then
    // we don't have level 5 in Evo
  else if Trim(VarToStr(Input['CDeptTied.cc4'])) <> '_' then
  begin
    Output.Add( 'CO_TEAM', 'NAME', Input['CDeptTied.NAME']);
    Output.Add( 'CO_TEAM', 'PRINT_GROUP_ADDRESS_ON_CHECK', INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK);
  end
  else if Trim(VarToStr(Input['CDeptTied.cc3'])) <> '_' then
  begin
    Output.Add( 'CO_DEPARTMENT', 'NAME', Input['CDeptTied.NAME']);
    Output.Add( 'CO_DEPARTMENT', 'PRINT_DEPT_ADDRESS_ON_CHECKS', INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK);
  end
  else if Trim(VarToStr(Input['CDeptTied.cc2'])) <> '_' then
  begin
    Output.Add( 'CO_BRANCH', 'NAME', Input['CDeptTied.NAME']);
    Output.Add( 'CO_BRANCH', 'PRINT_BRANCH_ADDRESS_ON_CHECK', INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK);
  end
  else if Trim(VarToStr(Input['CDeptTied.cc1'])) <> '_' then
  begin
    Output.Add( 'CO_DIVISION', 'NAME', Input['CDeptTied.NAME']);
    Output.Add( 'CO_DIVISION', 'PRINT_DIV_ADDRESS_ON_CHECKS', INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK);
  end;
end;

procedure TM2ImportDef.EERate(const Input, Keys, Output: TFieldValues);
var
  division: Variant;
  branch: Variant;
  department: Variant;
  team: Variant;
begin
  ImportEERate( Input, Keys, Output );

  if not bDeptTied then
    HelpImportDBDT( Input, Output, 'ERate', 'EE_RATES' )
  else
  begin
    division := null;
    branch := null;
    department := null;
    team := null;
    if VarToStr(Input['ERate.cc5']) = '' then
    begin
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DIVISION'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
      division := FImpEngine.IDS('CO_DIVISION').Lookup( 'CUSTOM_DIVISION_NUMBER', Input['ERate.cc1'], 'CO_DIVISION_NBR' );
      if not VarIsNull(division) then
      begin
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_BRANCH'), 'CO_DIVISION_NBR='+VarToStr(division) );
        branch := FImpEngine.IDS('CO_BRANCH').Lookup( 'CUSTOM_BRANCH_NUMBER', Input['ERate.cc2'], 'CO_BRANCH_NBR' );
      end;
      if not VarIsNull(branch) then
      begin
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DEPARTMENT'), 'CO_BRANCH_NBR='+VarToStr(branch) );
        department := FImpEngine.IDS('CO_DEPARTMENT').Lookup( 'CUSTOM_DEPARTMENT_NUMBER', Input['ERate.cc3'], 'CO_DEPARTMENT_NBR' );
      end;
      if not VarIsNull(department) then
      begin
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_TEAM'), 'CO_DEPARTMENT_NBR='+VarToStr(department) );
        team := FImpEngine.IDS('CO_TEAM').Lookup( 'CUSTOM_TEAM_NUMBER', Input['ERate.cc4'], 'CO_TEAM_NBR' );
      end;
    end;
    Output.Add( 'EE_RATES', 'CO_DIVISION_NBR', division );
    Output.Add( 'EE_RATES', 'CO_BRANCH_NBR', branch );
    Output.Add( 'EE_RATES', 'CO_DEPARTMENT_NBR', department );
    Output.Add( 'EE_RATES', 'CO_TEAM_NBR', team );
  end
end;

function TM2ImportDef.MapDedCalcMethod(val: Variant): Variant;
begin
  Result := Matchers.Get(sDedBindingName).RightMatch( val );
end;

function TM2ImportDef.MapEarnCalcMethod(val: Variant): Variant;
begin
  Result := Matchers.Get(sEarnBindingName).RightMatch( val );
end;

end.
