unit SM3ImportDef;

interface

uses
  genericImport, genericImportBaseImpl, Classes, SDM_IM_M3, genericImportDefinitionImpl,
  gdyBinder, sImportVisualController, sImportHelpers,  IEMap, Variants,
  EvConsts, StrUtils, DB, SysUtils, EvUtils, DateUtils, sMCommonImportDef, evExceptions;

const
  sEDCalcBindingName = 'EDCalc-new';

function CreateM3CO_AND_EEImport( aLogger: IImportLogger ): IImport;
function CreateM3PRImport( aLogger: IImportLogger ): IImport;

implementation

uses
  typinfo;

type
  TM3ImportDef = class( TMillenniumImportDef )
  protected
    function MapDedCalcMethod( val: Variant): Variant; override;
    function MapEarnCalcMethod( val: Variant): Variant; override;
  public
    constructor Create(const AIEMap: IIEMap); override;
  published
    procedure EEAdditionalInfo(const Input, Keys, Output: TFieldValues);
    procedure EERate(const Input, Keys, Output: TFieldValues);
  end;

const
  M3CO_AND_EEimport: TImportDesc = (
    ImportName: 'M3 Employee Import';
    RequiredMatchersNames: sEthnicityBindingName + #13#10 +
                           sEmpStatusBindingName + #13#10 +
                           sPayFreqBindingName + #13#10 +
                           sEmpTypeBindingName + #13#10 +
                           sTaxesBindingName + #13#10 +
                           sRatePerBindingName + #13#10 +
                           sEEOBindingName + #13#10 +
                           sRateCodeBindingName + #13#10 +
                           sReciprocalBindingName + #13#10 +
                           sEDBindingName + #13#10 +
                           sEDCalcBindingName + #13#10 +
                           sAgencyBindingName + #13#10 +
                           sDBDTBindingName + #13#10 +
                           sMDLocalsBindingName ;
    ExternalDataModuleImplClass: TDM_IM_M3;
    ImportDefinitionImplClass: TM3ImportDef
  );
  M3PRimport: TImportDesc = (
    ImportName: 'M3 Payroll Import';
    RequiredMatchersNames: sTaxesBindingName + #13#10 +
                           sEmpStatusBindingName + #13#10 +
                           sEDBindingName;
    ExternalDataModuleImplClass: TDM_IM_M3;
    ImportDefinitionImplClass: TM3ImportDef
  );
type
  TM3Import = class (TMillenniumImport)
  protected
    procedure Prepare( aImportDefinition: IImportDefinition; aParams: TStrings );
  end;

  TM3CO_AND_EEImport = class (TM3Import)
  protected
    procedure DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings ); override;
  end;

  TM3PRImport = class (TM3Import)
  protected
    procedure DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings ); override;
  end;

const
  M3BindingDesc: array [0..13] of TBindingDesc =
    (
      ( Name: sEthnicityBindingName;
        Caption: 'Mapped Ethnicity';
        LeftDesc: ( Caption: 'M3 Ethnicity'; Unique: true; KeyFields: 'ethnicity'; ListFields: 'ethnicity;Description');
        RightDesc: ( Caption: 'Evolution Ethnicity'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sEmpStatusBindingName;
        Caption: 'Mapped Emp Status';
        LeftDesc: ( Caption: 'M3 Emp Status'; Unique: true; KeyFields: 'status'; ListFields: 'status;Description');
        RightDesc: ( Caption: 'Evolution Emp Status'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sPayFreqBindingName;
        Caption: 'Mapped Pay Frequency';
        LeftDesc: ( Caption: 'M3 Pay Frequency'; Unique: true; KeyFields: 'Frequency'; ListFields: 'Frequency;Description');
        RightDesc: ( Caption: 'Evolution Pay Frequency'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sEmpTypeBindingName;
        Caption: 'Mapped Emp Type';
        LeftDesc: ( Caption: 'M3 Emp Type'; Unique: true; KeyFields: 'empType'; ListFields: 'empType;Description');
        RightDesc: ( Caption: 'Evolution Emp Type'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sTaxesBindingName;
        Caption: 'Mapped Taxes';
        LeftDesc: ( Caption: 'M3 Taxes'; Unique: true; KeyFields: 'tcode'; ListFields: 'tcode;Description');
        RightDesc: ( Caption: 'Evolution Taxes'; Unique: false; KeyFields: 'Type;Code;SubType'; ListFields: 'Type;Name')
      ),
      ( Name: sRatePerBindingName;
        Caption: 'Mapped Rate Per';
        LeftDesc: ( Caption: 'M3 Rate Per'; Unique: true; KeyFields: 'ratePer'; ListFields: 'ratePer;Description');
        RightDesc: ( Caption: 'Evolution Rate Per'; Unique: false; KeyFields: 'Code'; ListFields: 'Code;Name')
      ),
      ( Name: sEEOBindingName;
        Caption: 'Mapped EEO Class';
        LeftDesc: ( Caption: 'M3 EEO Class'; Unique: true; KeyFields: 'eeoClass'; ListFields: 'EeoClass;Description');
        RightDesc: ( Caption: 'Evolution EEO Class'; Unique: false; KeyFields: 'SY_HR_EEO_NBR'; ListFields: 'SY_HR_EEO_NBR;Description')
      ),
      ( Name: sRateCodeBindingName;
        Caption: 'Mapped Rate Code';
        LeftDesc: ( Caption: 'M3 Rate Code'; Unique: true; KeyFields: 'ratecode'; ListFields: 'rateCode;Description');
        RightDesc: ( Caption: 'Evolution Rate Number'; Unique: true; KeyFields: 'number'; ListFields: 'number')
      ),
      ( Name: sReciprocalBindingName;
        Caption: 'Mapped Reciprocal';
        LeftDesc: ( Caption: 'M3 Reciprocal'; Unique: true; KeyFields: 'reciprocalCode'; ListFields: 'reciprocalCode;Description');
        RightDesc: ( Caption: 'Evolution Reciprocal'; Unique: false; KeyFields: 'code'; ListFields: 'code;name')
      ),
      ( Name: sEDBindingName;
        Caption: 'Mapped E/Ds';
        LeftDesc: ( Caption: 'M3 E/Ds'; Unique: true; KeyFields: 'eord;code'; ListFields: 'eord;code;Description');
        RightDesc: ( Caption: 'Evolution E/Ds'; Unique: false; KeyFields: 'ED_Lookup'; ListFields: 'ED_Lookup;CodeDescription')
      ),
      ( Name: sEDCalcBindingName;
        Caption: 'Mapped E/D Calc Type';
        LeftDesc: ( Caption: 'M3 E/D Calc Type'; Unique: true; KeyFields: 'code'; ListFields: 'code;Description');
        RightDesc: ( Caption: 'Evolution E/D Calc Type (TargetField is used only for Deductions)'; Unique: false; KeyFields: 'code;TargetField'; ListFields: 'code;name;TargetField')
      ),
      ( Name: sAgencyBindingName;
        Caption: 'Mapped Agencies';
        LeftDesc: ( Caption: 'M3 Payees'; Unique: true; KeyFields: 'id'; ListFields: 'id;name');
        RightDesc: ( Caption: 'Evolution Agencies'; Unique: true; KeyFields: 'CL_AGENCY_NBR'; ListFields: 'Agency_Name')
      ),
      ( Name: sDBDTBindingName;
        Caption: 'Mapped DBDT';
        LeftDesc: ( Caption: 'M3 Categories'; Unique: true; KeyFields: 'Number'; ListFields: 'Category;Number;Desc');
        RightDesc: ( Caption: 'Evolution DBDT'; Unique: true; KeyFields: 'Level'; ListFields: 'Level;DBDT')
      ),
      ( Name: sMDLocalsBindingName;
        Caption: 'Mapped MD locals';
        LeftDesc: ( Caption: 'M3 MD Locals'; Unique: true; KeyFields: 'tcode'; ListFields: 'tcode;Description');
        RightDesc: ( Caption: 'Evolution MD Marital Statuses'; Unique: true; KeyFields: 'STATUS_TYPE'; ListFields: 'STATUS_TYPE;STATUS_DESCRIPTION')
      )
    );

function CreateM3CO_AND_EEImport( aLogger: IImportLogger ): IImport;
begin
  Result := TM3CO_AND_EEImport.Create( aLogger, M3CO_AND_EEimport, M3BindingDesc );
end;

function CreateM3PRImport( aLogger: IImportLogger ): IImport;
begin
  Result := TM3PRImport.Create( aLogger, M3PRimport, M3BindingDesc );
end;

{ TM3Import }

procedure TM3Import.Prepare(aImportDefinition: IImportDefinition; aParams: TStrings);
var
  empStatus: variant;
  m3dm: IM3DataModule;
begin
  m3dm := DataModuleInstance as IM3DataModule; //DataModule as IM3DataModule
  // the best way to handle this is to inherit each import DataModule from DM with ref counting

  if aParams.Values[sEETermedParam] = 'Y' then
  begin
    empStatus := aImportDefinition.Matchers.Get(sEmpStatusBindingName).LeftMatch( EE_TERM_ACTIVE );
    if VarIsNull(empStatus) then
      raise EevException.Create( 'Employee status mapping doen''t have Active status mapped.');
    m3dm.FilterByEmpStatus( true, empStatus )
  end
  else
    m3dm.FilterByEmpStatus( false, Null );

  m3dm.FilterLiabilities( aParams.Values[sOnlyThisYear] = 'Y', EncodeDate(YearOf(Now),1,1) );
end;

{ TM3CO_AND_EEImport }

procedure TM3CO_AND_EEImport.DoRunImport(aImportDefinition: IImportDefinition; aParams: TStrings);
begin
  Prepare( aImportDefinition, aParams );
  aImportDefinition.SetFixedKeys( 'CO.CUSTOM_COMPANY_NUMBER=' + DataModule.CustomCompanyNumber);

  aImportDefinition.SetParam( sAdjustmentType, EmptyStrAsNull(aParams.Values[sAdjustmentType]) );
  aImportDefinition.SetParam( sDueDate, EmptyStrAsNull(aParams.Values[sDueDate]) );
  aImportDefinition.SetParam( sImpounded, EmptyStrAsNull(aParams.Values[sImpounded]) );
  aImportDefinition.SetParam( sProcessDate, EmptyStrAsNull(aParams.Values[sProcessDate]) );

  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'CAllDept', 'Importing DBDT' );
  (aImportDefinition as IMillenniumImportDefinition).BeforeEEStatesImport;
  try
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EInfo', 'Importing employees' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EInfoSalary', 'Importing salaries' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'ETax', 'Importing EE taxes' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EInfoFix', 'Creating missing home states' );
  finally
    (aImportDefinition as IMillenniumImportDefinition).AfterEEStatesImport;
  end;
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'ERate', 'Importing EE rates' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EDirDep', 'Importing direct deposits' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EDed', 'Importing deductions' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EEarn', 'Importing earnings' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'CTaxLiab', 'Importing tax liabilities', true );
end;


{ TM3PRImport }

procedure TM3PRImport.DoRunImport(aImportDefinition: IImportDefinition; aParams: TStrings);
begin
  Prepare( aImportDefinition, aParams );
  RunPrImport( aImportDefinition, aParams );;
end;

{ TM3ImportDef }

procedure TM3ImportDef.EEAdditionalInfo(const Input, Keys, Output: TFieldValues);
begin
  ImportEEAdditionalInfo( Input, Keys, Output);
  HelpImportDBDT( Input, Output, 'EInfo', 'EE' );
end;

constructor TM3ImportDef.Create(const AIEMap: IIEMap);
begin
  inherited Create(AIEMap);
//  RegisterConvertDescs( M3Conversion );
//  RegisterKeySourceDescs( M3Keys );
//  RegisterRowImport(M3RowImport);
//  RegisterMap( M3Map );
end;

procedure TM3ImportDef.EERate(const Input, Keys, Output: TFieldValues);
begin
  ImportEERate( Input, Keys, Output );
  HelpImportDBDT( Input, Output, 'ERate', 'EE_RATES' );
end;

function TM3ImportDef.MapDedCalcMethod(val: Variant): Variant;
begin
  Result := Matchers.Get(sEDCalcBindingName).RightMatch( val );
end;

function TM3ImportDef.MapEarnCalcMethod(val: Variant): Variant;
begin
  Result := Matchers.Get(sEDCalcBindingName).RightMatch( val );
end;

end.
