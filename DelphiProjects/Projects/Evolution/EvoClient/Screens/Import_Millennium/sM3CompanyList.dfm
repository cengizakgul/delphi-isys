object M3CompaniesForm: TM3CompaniesForm
  Left = 460
  Top = 203
  Width = 287
  Height = 305
  Caption = 'M3 Companies'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 0
    Width = 279
    Height = 237
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object EvBevel1: TEvBevel
      Left = 0
      Top = 227
      Width = 279
      Height = 10
      Align = alBottom
      Shape = bsBottomLine
    end
    object evDBGrid1: TevDBGrid
      Left = 0
      Top = 0
      Width = 279
      Height = 227
      DisableThemesInTitle = False
      Selected.Strings = (
        'co'#9'10'#9'Company'#9#9
        'name'#9'50'#9'Name'#9#9)
      IniAttributes.Delimiter = ';;'
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = evDataSource1
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
    end
  end
  object evPanel2: TevPanel
    Left = 0
    Top = 237
    Width = 279
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      279
      41)
    object evBitBtn1: TevBitBtn
      Left = 109
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
    object evBitBtn2: TevBitBtn
      Left = 194
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object CInfo: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select co, name from CInfo ')
    Left = 104
    Top = 24
    object CInfoco: TStringField
      DisplayLabel = 'Company'
      FieldName = 'co'
      Size = 10
    end
    object CInfoname: TStringField
      DisplayLabel = 'Name'
      FieldName = 'name'
      Size = 50
    end
  end
  object ACCompany: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=millennium;Data Source=home'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 40
    Top = 24
  end
  object evDataSource1: TevDataSource
    DataSet = CInfo
    Left = 96
    Top = 136
  end
end
