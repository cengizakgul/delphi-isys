// Screens of "M2 Import"
unit scr_Import_Millennium;

interface

uses
  sImportMillenniumScr,
  SPD_EDIT_IM_M2_BASE,
  SPD_EDIT_IM_M2_CO_AND_EE,
  SPD_EDIT_IM_M2_PR,
  sConnectDatabaseByFileName,
  sM2PrImportParam,
  SDM_IM_M2,
  SM2ImportDef,
  SM3ImportDef,
  SDM_IM_M3,
  sConnectM3,
  SPD_EDIT_IM_M3_BASE,
  SPD_EDIT_IM_M3_CO_AND_EE,
  SPD_EDIT_IM_M3_PR,
  sM3CompanyList,
  sMCommonImportDef,
  sMEEImportParam;

implementation

end.
