// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_M3_PR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_IM_M3_BASE, ImgList, ISBasicClasses, 
  ActnList, DB, Wwdatsrc, sImportQueue, sImportLoggerView, StdCtrls,
  sTablespaceView, Buttons, ExtCtrls, ComCtrls, sConnectDatabaseByFileName,
  sImportVisualController, genericImport, SM3ImportDef, sImportParam,
  sM2PrImportParam, gdyBinderVisual, EvUtils, SDataStructure, sMCommonImportDef,
  sConnectM3, EvUIComponents, LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_IM_M3_PR = class(TEDIT_IM_M3_BASE)
    ParamInput: TM2PrImportParamFrame;
    tsTaxes: TTabSheet;
    tsEDs: TTabSheet;
    BinderFrame1: TBinderFrame;
    BinderFrame2: TBinderFrame;
    tsEmpStatus: TTabSheet;
    BinderFrame3: TBinderFrame;
  public
    procedure Activate; override;
  end;

implementation

{$R *.dfm}

procedure TEDIT_IM_M3_PR.Activate;
var
  ivc: IImportVisualController;
  aImport: IImport;
begin
  ParamInput.Activate;
  aImport := CreateM3PRImport( LogFrame.LoggerKeeper.Logger );
  ivc := TImportVisualController.Create( LogFrame.LoggerKeeper, aImport, ParamInput, ViewerFrame );
  SetController( ivc );
  ivc.SetConnectionStringInputFrame( ConnectMSSQLDatabaseFrame );

  inherited;

  ivc.BindingKeeper(sEmpStatusBindingName).SetTables( aImport.DataModule.GetTable('CEmpStatus'),  aImport.DataModule.GetTable('cdsEmpStatus'));
  ivc.BindingKeeper(sEmpStatusBindingName).SetVisualBinder( BinderFrame3 );

  ivc.BindingKeeper(sTaxesBindingName).SetTables( aImport.DataModule.GetTable('CTax'),  aImport.DataModule.GetTable('cdsCoTaxes'));
  ivc.BindingKeeper(sTaxesBindingName).SetVisualBinder( BinderFrame1 );

  DM_COMPANY.CO_E_D_CODES.EnsureHasMetadata;
  ivc.BindingKeeper(sEDBindingName).SetTables( aImport.DataModule.GetTable('EDs'), DM_COMPANY.CO_E_D_CODES );
  ivc.BindingKeeper(sEDBindingName).SetVisualBinder( BinderFrame2 );

end;

initialization
  RegisterClass(TEDIT_IM_M3_PR);

end.
