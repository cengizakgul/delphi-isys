unit sM3CompanyList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ISBasicClasses,  Buttons, DB,
  Grids, Wwdbigrd, Wwdbgrid, ADODB, Wwdatsrc, EvUIComponents;

type
  TM3CompaniesForm = class(TForm)
    evPanel1: TevPanel;
    evPanel2: TevPanel;
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    EvBevel1: TEvBevel;
    CInfo: TADOQuery;
    ACCompany: TADOConnection;
    evDBGrid1: TevDBGrid;
    CInfoco: TStringField;
    CInfoname: TStringField;
    evDataSource1: TevDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  M3CompaniesForm: TM3CompaniesForm;

implementation

{$R *.dfm}

end.
