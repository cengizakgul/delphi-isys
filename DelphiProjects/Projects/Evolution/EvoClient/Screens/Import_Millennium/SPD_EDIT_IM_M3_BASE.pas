// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_M3_BASE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_IM_BASE, ImgList,  ActnList, Db, Wwdatsrc,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, genericImport,
  sTablespaceView, 
  sImportQueue, sImportLoggerView,
  ISBasicClasses, sConnectM3, EvUIComponents, LMDCustomButton, LMDButton,
  isUILMDButton;

type
  TEDIT_IM_M3_BASE = class(TEDIT_IM_BASE)
    ConnectMSSQLDatabaseFrame: TConnectM3Frame;
  end;


implementation

{$R *.DFM}

{ TEDIT_IM_M3_BASE }

end.
