// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sConnectDatabaseByFileName;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  Buttons, sImportVisualController, ISBasicClasses, EvUIComponents;

type
  TConnectDatabaseFileNameFrame= class(TFrame, IConnectionStringInputFrame )
    BitBtn1: TevBitBtn;
    lblSource: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    evEdit2: TevEdit;
    evEdit3: TevEdit;
    odFile: TOpenDialog;
    evLabel1: TevLabel;
    evEdit1: TevEdit;
    evSpeedButton1: TevSpeedButton;
    procedure BitBtn1Click(Sender: TObject);
    procedure evButton1Click(Sender: TObject);
    procedure evEdit1Change(Sender: TObject);
  private
    { Private declarations }
    FController: IImportVisualController;
    {IConnectionStringInputFrame}
    procedure ConnectionStringChanged;
    procedure SetController( aController: IImportVisualController );
    procedure SetDefaultConnectionString( aDefConnStr: string );

  public
    { Public declarations }
  end;

implementation

uses
  evutils;
{$R *.DFM}

procedure TConnectDatabaseFileNameFrame.BitBtn1Click(Sender: TObject);
begin
  FController.SetConnectionString('Data Source=' + evEdit1.Text + ';' + 'User ID=' + evEdit2.Text + ';' +
                                  'Jet OLEDB:Database Password=' + evEdit3.Text);
  BitBtn1.Enabled := False;
  evEdit2.Enabled := False;
  evEdit3.Enabled := False;
end;

procedure TConnectDatabaseFileNameFrame.ConnectionStringChanged;
var
  t: TStringList;
begin
  if assigned(FController) and (trim(FController.ConnectionString) <> '') then
  begin
    t := TStringList.Create;
    try
      t.Text := StringReplace(FController.ConnectionString, ';', #13, [rfReplaceAll]);
      lblSource.Caption := 'Importing from ' + t.Values['Data Source'];
    finally
      t.Free;
    end;
  end
  else
    lblSource.Caption := 'No database specified';
end;

procedure TConnectDatabaseFileNameFrame.SetController(
  aController: IImportVisualController);
begin
  FController := AController;
  ConnectionStringChanged;
end;

procedure TConnectDatabaseFileNameFrame.SetDefaultConnectionString(
  aDefConnStr: string);
begin
//do nothing
end;

procedure TConnectDatabaseFileNameFrame.evButton1Click(Sender: TObject);
begin
  if odFile.Execute then
  begin
    evEdit1.Text := odFile.FileName;
    if assigned(FController) then
      FController.Disconnect;
    evEdit1Change(nil);
    evEdit2.Enabled := True;
    evEdit3.Enabled := True;
  end;
end;

procedure TConnectDatabaseFileNameFrame.evEdit1Change(Sender: TObject);
begin
  BitBtn1.Enabled := (Trim(evEdit1.Text) <> ''){ and
                     (Trim(evEdit2.Text) <> '') and
                     (Trim(evEdit3.Text) <> '')};
end;

end.
