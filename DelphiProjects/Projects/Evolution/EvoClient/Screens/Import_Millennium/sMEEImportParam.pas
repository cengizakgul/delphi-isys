// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sMEEImportParam;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,  sImportVisualcontroller, EvBasicUtils,
  ISBasicClasses, ExtCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb, DBCtrls,
  ComCtrls, EvUIComponents;

type
  TMEEImportParamFrame = class(TFrame, IParamInputFrame)
    cbTerm: TevCheckBox;
    gbLiab: TevGroupBox;
    cbAdjustmentType: TevDBComboBox;
    lblAdjustmentType: TevLabel;
    cbLiabilityStatus: TevDBComboBox;
    lblLiabilityStatus: TevLabel;
    rgImpounded2: TevDBRadioGroup;
    dtProcessDate: TevDateTimePicker;
    evLabel4: TevLabel;
    evGroupBox1: TevGroupBox;
    evLabel12: TevLabel;
    cbDueDate: TevCheckBox;
    dtDueDate: TevDateTimePicker;
    rgImpounded: TevRadioGroup;
    cbOnlyThisYear: TevCheckBox;
  private
  public
    procedure Activate;

    procedure ParamsToControls( aParams: TStrings );
    procedure ControlsToParams( aParams: TStrings );
  end;

implementation

uses
  SFieldCodeValues, EvConsts, sMCommonImportDef;

{$R *.dfm}

{ TImportParamFrame }

procedure PopulateComboBox( cb: TevDBCombobox; C: string );
begin
  cb.MapList := True;
  cb.Items.Text := C;
end;

procedure TMEEImportParamFrame.Activate;
begin
  PopulateComboBox( cbAdjustmentType, TaxLiabilityAdjustmentType_ComboChoices );
  PopulateComboBox( cbLiabilityStatus, TaxDepositStatus_ComboChoices );
//  rgImpounded.Items.Text := GroupBox_YesNo_ComboChoices;
end;

procedure TMEEImportParamFrame.ControlsToParams(aParams: TStrings);
begin
  aParams.Values[sEETermedParam] := BoolToYN(cbTerm.Checked);

  aParams.Values[sAdjustmentType] := cbAdjustmentType.Value;
//  aParams.Values[sLiabStatus] := cbLiabilityStatus.Value;
  if rgImpounded.ItemIndex = 0 then
    aParams.Values[sImpounded] := GROUP_BOX_YES
  else
    aParams.Values[sImpounded] := GROUP_BOX_NO;

  aParams.Values[sProcessDate] := DateToStr(dtProcessDate.Date);

  if cbDueDate.Checked then
    aParams.Values[sDueDate] := DateToStr(dtDueDate.Date)
  else
    aParams.Values[sDueDate] := '';

  aParams.Values[sOnlyThisYear] := BoolToYN(cbOnlyThisYear.Checked);
end;

procedure TMEEImportParamFrame.ParamsToControls(aParams: TStrings);
begin
  cbTerm.Checked := aParams.Values[sEETermedParam] = 'Y';

  cbAdjustmentType.Value := aParams.Values[sAdjustmentType];

{  if trim(aParams.Values[sLiabStatus]) = '' then
    cbLiabilityStatus.Value := TAX_DEPOSIT_STATUS_PENDING
  else
    cbLiabilityStatus.Value := aParams.Values[sLiabStatus];
}
//  rgImpounded.Value := aParams.Values[sImpounded];
  if aParams.Values[sImpounded] = GROUP_BOX_YES then
    rgImpounded.ItemIndex := 0
  else if aParams.Values[sImpounded] = GROUP_BOX_NO then
    rgImpounded.ItemIndex := 1
  else
    rgImpounded.ItemIndex := 1;


  if trim(aParams.Values[sProcessDate]) = '' then
    dtProcessDate.Date := Date
  else
    dtProcessDate.Date := StrToDate(aParams.Values[sProcessDate]);

  cbDueDate.Checked := trim(aParams.Values[sDueDate]) <> '';
  if trim(aParams.Values[sDueDate]) = '' then
    dtDueDate.Date := Date
  else
    dtDueDate.Date := StrToDate(aParams.Values[sDueDate]);

  cbOnlyThisYear.Checked := aParams.Values[sOnlyThisYear] = 'Y';
end;

end.
