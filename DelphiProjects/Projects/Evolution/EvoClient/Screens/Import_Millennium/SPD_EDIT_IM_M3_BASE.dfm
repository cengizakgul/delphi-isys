inherited EDIT_IM_M3_BASE: TEDIT_IM_M3_BASE
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited evPanel1: TevPanel
        inherited evPanel5: TevPanel
          Top = 185
          Height = 64
          inherited EvBevel2: TEvBevel
            Height = 60
          end
          inherited pnlSetup: TevPanel
            Height = 60
            inherited pnlSetupControls: TevPanel
              Height = 21
            end
          end
        end
        inherited pnlConnectionInputHolder: TevPanel
          Height = 185
          inline ConnectMSSQLDatabaseFrame: TConnectM3Frame
            Left = 2
            Top = 2
            Width = 431
            Height = 181
            Align = alClient
            TabOrder = 0
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited LogFrame: TImportLoggerViewFrame
        inherited pnlLog: TevPanel
          inherited LoggerRichView: TImportLoggerRichViewFrame
            inherited pnlUserView: TevPanel
              inherited pnlBottom: TevPanel
                Height = 397
                inherited evSplitter2: TevSplitter
                  Height = 397
                end
                inherited pnlLeft: TevPanel
                  Height = 397
                  inherited mmDetails: TevMemo
                    Height = 372
                  end
                end
                inherited pnlRight: TevPanel
                  Height = 397
                  inherited mmContext: TevMemo
                    Height = 372
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
