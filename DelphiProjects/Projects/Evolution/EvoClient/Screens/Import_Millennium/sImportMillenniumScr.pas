// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportMillenniumScr;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPackageEntry, ExtCtrls, Menus,  ImgList, EvCommonInterfaces,
  ComCtrls, ToolWin, StdCtrls, Buttons, ActnList, ISBasicClasses, EvMainboard;

type
  TM2Frm = class(TFramePackageTmpl, IevImportMillenniumScreens)
  protected
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
  end;

implementation

uses SPD_EDIT_IM_M2_CO_AND_EE, SPD_EDIT_IM_M2_PR, SPD_EDIT_IM_M3_CO_AND_EE,
  SPD_EDIT_IM_M3_PR;

{$R *.DFM}

var
  M2Frm: TM2Frm;

function GetImportMillenniumScreens: IevImportMillenniumScreens;
begin
  if not Assigned(M2Frm) then
    M2Frm := TM2Frm.Create(nil);
  Result := M2Frm;
end;


{ TPaychoiceFrm }

function TM2Frm.PackageCaption: string;
begin
  Result := 'M2';
end;

function TM2Frm.PackageSortPosition: string;
begin
  Result := '';
end;

procedure TM2Frm.UserPackageStructure;
begin
  AddStructure('\&Company\Imports\M2 Employees|481', TEDIT_IM_M2_CO_AND_EE.ClassName );
  AddStructure('\&Company\Imports\M2 Payrolls|482', TEDIT_IM_M2_PR.ClassName );
  AddStructure('\&Company\Imports\M3 Employees|483', TEDIT_IM_M3_CO_AND_EE.ClassName );
  AddStructure('\&Company\Imports\M3 Payrolls|484', TEDIT_IM_M3_PR.ClassName );
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetImportMillenniumScreens, IevImportMillenniumScreens, 'Screens Import Millennium');

finalization
  FreeAndNil(M2Frm);

end.
