// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_M3_CO_AND_EE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SPD_EDIT_IM_M3_BASE, ImgList, ISBasicClasses, 
  ActnList, DB, Wwdatsrc, sImportQueue, sImportLoggerView, StdCtrls,
  sTablespaceView, Buttons, ExtCtrls, ComCtrls,
  sImportVisualController, genericImport, sMEEImportParam,
  gdyBinderVisual, EvUtils, SDataStructure, sConnectM3, sMCommonImportDef, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_IM_M3_CO_AND_EE = class(TEDIT_IM_M3_BASE, IConnectionStatusListener)
    ParamInput: TMEEImportParamFrame;
    tsEthnicity: TTabSheet;
    BinderFrame1: TBinderFrame;
    tsEmpStatus: TTabSheet;
    BinderFrame2: TBinderFrame;
    tsPayFreq: TTabSheet;
    BinderFrame3: TBinderFrame;
    tsEmpType: TTabSheet;
    BinderFrame4: TBinderFrame;
    tsTaxes: TTabSheet;
    BinderFrame5: TBinderFrame;
    tsRatePer: TTabSheet;
    BinderFrame6: TBinderFrame;
    tsEEO: TTabSheet;
    BinderFrame7: TBinderFrame;
    tsRateCode: TTabSheet;
    BinderFrame8: TBinderFrame;
    tsReciprocal: TTabSheet;
    BinderFrame9: TBinderFrame;
    tsEDs: TTabSheet;
    BinderFrame10: TBinderFrame;
    tsEDCalc: TTabSheet;
    BinderFrame11: TBinderFrame;
    tsAgencies: TTabSheet;
    BinderFrame13: TBinderFrame;
    DBDT: TTabSheet;
    BinderFrame14: TBinderFrame;
    pnlInfo: TevPanel;
    lblInfo: TevLabel;
    tsMDLocals: TTabSheet;
    BinderFrame12: TBinderFrame;
  private
    procedure ConnectionChanged( DataModule: IExternalDataModule );
  protected
    procedure SetController( aController: IImportVisualController ); override;
  public
    procedure Activate; override;
  end;

implementation

uses
  sM3ImportDef, SPD_EDIT_IM_BASE;
{$R *.dfm}

procedure TEDIT_IM_M3_CO_AND_EE.Activate;
var
  ivc: IImportVisualController;
  aImport: IImport;
begin
  ParamInput.Activate;	
  aImport := CreateM3CO_AND_EEImport( LogFrame.LoggerKeeper.Logger );
  ivc := TImportVisualController.Create( LogFrame.LoggerKeeper, aImport, ParamInput, ViewerFrame );
  SetController( ivc );
  ivc.SetConnectionStringInputFrame( ConnectMSSQLDatabaseFrame  );

  inherited;

  ivc.BindingKeeper(sEthnicityBindingName).SetTables( aImport.DataModule.GetTable('CEthnicity'),  aImport.DataModule.GetTable('cdsEthnicity'));
  ivc.BindingKeeper(sEthnicityBindingName).SetVisualBinder( BinderFrame1 );

  ivc.BindingKeeper(sEmpStatusBindingName).SetTables( aImport.DataModule.GetTable('CEmpStatus'),  aImport.DataModule.GetTable('cdsEmpStatus'));
  ivc.BindingKeeper(sEmpStatusBindingName).SetVisualBinder( BinderFrame2 );

  ivc.BindingKeeper(sPayFreqBindingName).SetTables( aImport.DataModule.GetTable('CFreq'),  aImport.DataModule.GetTable('cdsPayFreq'));
  ivc.BindingKeeper(sPayFreqBindingName).SetVisualBinder( BinderFrame3 );

  ivc.BindingKeeper(sEmpTypeBindingName).SetTables( aImport.DataModule.GetTable('CEmpType'),  aImport.DataModule.GetTable('cdsEmpType'));
  ivc.BindingKeeper(sEmpTypeBindingName).SetVisualBinder( BinderFrame4 );

  ivc.BindingKeeper(sTaxesBindingName).SetTables( aImport.DataModule.GetTable('CTax'),  aImport.DataModule.GetTable('cdsCoTaxes'));
  ivc.BindingKeeper(sTaxesBindingName).SetVisualBinder( BinderFrame5 );

  ivc.BindingKeeper(sRatePerBindingName).SetTables( aImport.DataModule.GetTable('SRatePer'),  aImport.DataModule.GetTable('cdsRatePer'));
  ivc.BindingKeeper(sRatePerBindingName).SetVisualBinder( BinderFrame6 );

  DM_SYSTEM.SY_HR_EEO.EnsureHasMetadata;
  ivc.BindingKeeper(sEEOBindingName).SetTables( aImport.DataModule.GetTable('CEEOClass'), DM_SYSTEM.SY_HR_EEO );
  ivc.BindingKeeper(sEEOBindingName).SetVisualBinder( BinderFrame7 );

  ivc.BindingKeeper(sRateCodeBindingName).SetTables( aImport.DataModule.GetTable('CRateCodes'),  aImport.DataModule.GetTable('cdsRateCode'));
  ivc.BindingKeeper(sRateCodeBindingName).SetVisualBinder( BinderFrame8 );

  ivc.BindingKeeper(sReciprocalBindingName).SetTables( aImport.DataModule.GetTable('SReciprocal'),  aImport.DataModule.GetTable('cdsReciprocal'));
  ivc.BindingKeeper(sReciprocalBindingName).SetVisualBinder( BinderFrame9 );

  DM_COMPANY.CO_E_D_CODES.EnsureHasMetadata;
  ivc.BindingKeeper(sEDBindingName).SetTables( aImport.DataModule.GetTable('EDs'), DM_COMPANY.CO_E_D_CODES );
  ivc.BindingKeeper(sEDBindingName).SetVisualBinder( BinderFrame10 );

  ivc.BindingKeeper(sEDCalcBindingName).SetTables( aImport.DataModule.GetTable('CCalcCodes'),  aImport.DataModule.GetTable('cdsEDCalc'));
  ivc.BindingKeeper(sEDCalcBindingName).SetVisualBinder( BinderFrame11 );

  DM_CLIENT.CL_AGENCY.EnsureHasMetadata;
  ivc.BindingKeeper(sAgencyBindingName).SetTables( aImport.DataModule.GetTable('CAgency'), DM_CLIENT.CL_AGENCY );
  ivc.BindingKeeper(sAgencyBindingName).SetVisualBinder( BinderFrame13 );

  ivc.BindingKeeper(sDBDTBindingName).SetTables( aImport.DataModule.GetTable('cdsM3Cat'), aImport.DataModule.GetTable('cdsEvoDBDT') );
  ivc.BindingKeeper(sDBDTBindingName).SetVisualBinder( BinderFrame14 );

  ivc.BindingKeeper(sMDLocalsBindingName).SetTables( aImport.DataModule.GetTable('CoMDLocals'), aImport.DataModule.GetTable('cdsMDMaritalStatuses') );
  ivc.BindingKeeper(sMDLocalsBindingName).SetVisualBinder( BinderFrame12 );
end;
                                                           
procedure TEDIT_IM_M3_CO_AND_EE.ConnectionChanged(
  DataModule: IExternalDataModule);
begin
  if DataModule.Connected then
    with GetDDStat(DataModule) do
    begin
      lblInfo.Caption := msg;
      if need > has then
        lblInfo.Font.Color := clRed
      else
        lblInfo.ParentFont := true;
    end
  else
  begin
    lblInfo.Caption := '';
    lblInfo.ParentFont := true;
  end;
end;

procedure TEDIT_IM_M3_CO_AND_EE.SetController(
  aController: IImportVisualController);
begin
  if assigned(FController) then
    FController.AdviseConnectionStatusListener( nil );
  inherited;
  if assigned(aController) then
    aController.AdviseConnectionStatusListener( Self );
end;

initialization
  RegisterClass(TEDIT_IM_M3_CO_AND_EE);

end.
