inherited EDIT_IM_M2_PR: TEDIT_IM_M2_PR
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited evPanel1: TevPanel
        inherited evPanel5: TevPanel
          inherited pnlSetup: TevPanel
            inherited pnlSetupControls: TevPanel
              inline ParamInput: TM2PrImportParamFrame
                Left = 56
                Top = 32
                Width = 249
                Height = 89
                TabOrder = 0
              end
            end
          end
        end
      end
    end
    inherited TabSheet5: TTabSheet
      inherited viewerFrame: TTableSpaceViewFrame
        inherited evPanel2: TevPanel
          inherited dgView: TevDBGrid
            Width = 435
            Height = 201
          end
        end
      end
    end
    object tsEmpStatus: TTabSheet [2]
      Caption = 'Emp Status'
      ImageIndex = 6
      inline BinderFrame3: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 237
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 148
            Height = 0
            inherited evPanel4: TevPanel
              Width = 148
            end
            inherited dgRight: TevDBGrid
              Width = 148
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 418
            inherited evPanel5: TevPanel
              Width = 418
            end
            inherited dgBottom: TevDBGrid
              Width = 418
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 418
          end
        end
      end
    end
    object tsTaxes: TTabSheet [3]
      Caption = 'Taxes'
      ImageIndex = 4
      inline BinderFrame1: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 237
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 154
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 148
            Height = 0
            inherited evPanel4: TevPanel
              Width = 148
            end
            inherited dgRight: TevDBGrid
              Width = 148
              Height = 154
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 418
            inherited evPanel5: TevPanel
              Width = 418
            end
            inherited dgBottom: TevDBGrid
              Width = 418
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 418
          end
        end
      end
    end
    object tsEDs: TTabSheet [4]
      Caption = 'EDs'
      ImageIndex = 5
      inline BinderFrame2: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 237
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 154
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 148
            Height = 0
            inherited evPanel4: TevPanel
              Width = 148
            end
            inherited dgRight: TevDBGrid
              Width = 148
              Height = 154
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 418
            inherited evPanel5: TevPanel
              Width = 418
            end
            inherited dgBottom: TevDBGrid
              Width = 418
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 418
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited LogFrame: TImportLoggerViewFrame
        inherited pnlLog: TevPanel
          inherited LoggerRichView: TImportLoggerRichViewFrame
            inherited pnlUserView: TevPanel
              inherited pnlBottom: TevPanel
                Height = 1
                inherited evSplitter2: TevSplitter
                  Height = 1
                end
                inherited pnlLeft: TevPanel
                  Height = 1
                end
                inherited pnlRight: TevPanel
                  Height = 1
                end
              end
            end
          end
        end
      end
    end
  end
end
