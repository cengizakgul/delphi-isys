object DM_IM_M3: TDM_IM_M3
  OldCreateOrder = False
  Left = 343
  Top = 40
  Height = 676
  Width = 681
  object ACCompany: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=millennium;Data Source=home'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 40
    Top = 24
  end
  object CInfo: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select * from CInfo where CInfo.co = :co')
    Left = 104
    Top = 24
  end
  object CEthnicity: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select co, ethnicity, description from CEthnicity'
      'where co = :co')
    Left = 336
    Top = 16
    object CEthnicityethnicity: TStringField
      FieldName = 'ethnicity'
      Size = 10
    end
    object CEthnicitydescription: TStringField
      FieldName = 'description'
      Size = 200
    end
    object CEthnicityco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object cdsEthnicity: TevClientDataSet
    Left = 400
    Top = 16
    object cdsEthnicityCode: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object cdsEthnicityName: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object CEmpStatus: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select co, status, description from CEmpStatus'
      'where co = :co')
    Left = 336
    Top = 64
    object CEmpStatusstatus: TStringField
      FieldName = 'status'
    end
    object CEmpStatusdescription: TStringField
      FieldName = 'description'
      Size = 200
    end
    object CEmpStatusco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object cdsEmpStatus: TevClientDataSet
    Left = 400
    Top = 64
    object StringField1: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField2: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object CFreq: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select co, frequency, description from CFreq'
      'where co = :co')
    Left = 336
    Top = 112
    object CFreqfrequency: TStringField
      FieldName = 'frequency'
      Size = 10
    end
    object CFreqdescription: TStringField
      FieldName = 'description'
      Size = 200
    end
    object CFreqco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object cdsPayFreq: TevClientDataSet
    Left = 400
    Top = 112
    object StringField3: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField4: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object CEmpType: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select co, empType, description from cEmpType where co = :co')
    Left = 336
    Top = 160
    object CEmpTypeempType: TStringField
      FieldName = 'empType'
      Size = 10
    end
    object CEmpTypedescription: TStringField
      FieldName = 'description'
      Size = 200
    end
    object CEmpTypeco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object cdsEmpType: TevClientDataSet
    Left = 400
    Top = 160
    object StringField5: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField6: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object CTax: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select co, tcode, description from CTax where co = :co')
    Left = 336
    Top = 208
    object CTaxtcode: TStringField
      FieldName = 'tcode'
      Size = 10
    end
    object CTaxdescription: TStringField
      FieldName = 'description'
      Size = 200
    end
    object CTaxco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object cdsCoTaxes: TevClientDataSet
    Left = 400
    Top = 208
    object cdsCoTaxesType: TStringField
      FieldName = 'Type'
      Size = 1
    end
    object StringField7: TStringField
      FieldName = 'Code'
      Size = 256
    end
    object cdsCoTaxesSubType: TStringField
      FieldName = 'SubType'
      Size = 2
    end
    object StringField8: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object EInfo: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = EmpStatusFilterRecord
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select EInfo.*, CPayGrades.description as PayGradeDesc '
      'from EInfo left outer join CPaygrades '
      'on EInfo.payGrade = CPayGrades.payGrade '
      
        'where EInfo.co = :co and (CPayGrades.co = :co or CPayGrades.co i' +
        's null)')
    Left = 104
    Top = 88
  end
  object SRatePer: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select ratePer, description from SRatePer'
      '')
    Left = 296
    Top = 256
    object SRatePerratePer: TStringField
      FieldName = 'ratePer'
      Size = 10
    end
    object SRatePerdescription: TStringField
      FieldName = 'description'
      Size = 200
    end
  end
  object cdsRatePer: TevClientDataSet
    Left = 400
    Top = 256
    object StringField9: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField10: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object CEEOClass: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select co, eeoClass, description from CEEOCLass'
      'where co = :co')
    Left = 488
    Top = 32
    object CEEOClasseeoClass: TStringField
      FieldName = 'eeoClass'
      Size = 10
    end
    object CEEOClassdescription: TStringField
      FieldName = 'description'
      Size = 200
    end
    object CEEOClassco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object ETax: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = EmpStatusFilterRecord
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from ETax t, EInfo e, CTax c'
      
        'where t.id=e.id and t.tcode=c.tcode and t.co = :co and e.co = :c' +
        'o and c.co = :co'
      'order by  c.taxType ')
    Left = 160
    Top = 88
  end
  object ERate: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = EmpStatusFilterRecord
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from ERate t, EInfo e'
      'where t.id=e.id and e.co = :co and t.id = :co')
    Left = 216
    Top = 88
  end
  object CRateCodes: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = CRateCodesFilterRecord
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select co, rateCode, description from CRateCodes '
      'where co = :co')
    Left = 336
    Top = 312
    object CRateCodesrateCode: TStringField
      FieldName = 'rateCode'
      Size = 10
    end
    object CRateCodesdescription: TStringField
      FieldName = 'description'
      Size = 200
    end
    object CRateCodesco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object cdsRateCode: TevClientDataSet
    Left = 400
    Top = 312
    object cdsRateCodeNumber: TIntegerField
      FieldName = 'Number'
    end
  end
  object SReciprocal: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select reciprocalCode, description from SReciprocal'
      '')
    Left = 304
    Top = 360
    object SReciprocalreciprocalCode: TStringField
      FieldName = 'reciprocalCode'
      Size = 10
    end
    object SReciprocaldescription: TStringField
      FieldName = 'description'
      Size = 200
    end
  end
  object cdsReciprocal: TevClientDataSet
    Left = 400
    Top = 360
    object StringField11: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField12: TStringField
      FieldName = 'Name'
      Size = 40
    end
  end
  object EDs: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      
        'select co, '#39'E'#39' as eord, ecode as code, description from cearn wh' +
        'ere co = :co'
      'union '
      
        'select co, '#39'D'#39' as eord, dcode, description from cded where co = ' +
        ':co'
      '')
    Left = 488
    Top = 144
    object EDseord: TStringField
      FieldName = 'eord'
      ReadOnly = True
      Size = 1
    end
    object EDscode: TStringField
      FieldName = 'code'
      Size = 10
    end
    object EDsdescription: TStringField
      FieldName = 'description'
      Size = 200
    end
    object EDsco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object EDirDep: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = EmpStatusFilterRecord
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from EDirDep t, EInfo e'
      'where t.id=e.id and e.co = :co and t.co = :co')
    Left = 104
    Top = 144
  end
  object EDed: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = EmpStatusFilterRecord
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select t.*, e.empStatus, t.agency as thirdParty'
      'from EDed t, EInfo e'
      'where t.id=e.id and e.co = :co and t.co = :co'
      'order by t.id, t.dcode, t.startdate desc')
    Left = 104
    Top = 200
  end
  object EEarn: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = EmpStatusFilterRecord
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select t.*, e.empStatus, t.agency as thirdParty'
      'from EFringe t, EInfo e'
      'where t.id=e.id and e.co = :co and t.co = :co'
      'order by t.id, t.ecode, t.startdate desc')
    Left = 160
    Top = 200
  end
  object CCalcCodes: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select co,  code, description from CCalcCodes'
      'where co = :co ')
    Left = 336
    Top = 416
    object CCalcCodescode: TStringField
      FieldName = 'code'
    end
    object CCalcCodesdescription: TStringField
      FieldName = 'description'
      Size = 200
    end
    object CCalcCodesco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object cdsEDCalc: TevClientDataSet
    Left = 400
    Top = 416
    object StringField13: TStringField
      FieldName = 'Code'
      Size = 1
    end
    object StringField14: TStringField
      FieldName = 'Name'
      Size = 40
    end
    object cdsEDCalcTargetField: TStringField
      FieldName = 'TargetField'
      Size = 10
    end
  end
  object CAgency: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select co, id, name from CAgency where co = :co')
    Left = 488
    Top = 200
    object CAgencyid: TStringField
      FieldName = 'id'
      Size = 10
    end
    object CAgencyname: TStringField
      FieldName = 'name'
      Size = 50
    end
    object CAgencyco: TStringField
      FieldName = 'co'
      Size = 10
    end
  end
  object EYtd: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = EmpStatusFilterRecord
    Parameters = <
      item
        Name = 'Year'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select t.*, e.empStatus'
      'from EYtd t, EInfo e'
      'where t.id=e.id and Year=:Year and e.co = :co and t.co = :co')
    Left = 104
    Top = 280
  end
  object CDept1: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select * from CDept1 where co = :co')
    Left = 24
    Top = 360
  end
  object CDept2: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select * from CDept2 where co = :co')
    Left = 64
    Top = 360
  end
  object CDept3: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select * from CDept3 where co = :co')
    Left = 104
    Top = 360
  end
  object CDept4: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select * from CDept4 where co = :co')
    Left = 144
    Top = 360
  end
  object CDept5: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select * from CDept5 where co = :co')
    Left = 184
    Top = 360
  end
  object cdsEvoDBDT: TevClientDataSet
    Left = 160
    Top = 448
    object cdsEvoDBDTDBDT: TStringField
      FieldName = 'DBDT'
      Size = 10
    end
    object cdsEvoDBDTlevel: TIntegerField
      FieldName = 'level'
    end
  end
  object cdsM3Cat: TevClientDataSet
    FieldDefs = <
      item
        Name = 'Category'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'number'
        DataType = ftInteger
      end>
    Left = 48
    Top = 448
    object StringField18: TStringField
      FieldName = 'Category'
      Size = 10
    end
    object cdsM3Catnumber: TIntegerField
      FieldName = 'number'
    end
    object cdsM3Catdesc: TStringField
      FieldKind = fkCalculated
      FieldName = 'desc'
      Size = 10
      Calculated = True
    end
  end
  object CAllDept: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end
      item
        Name = 'co'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT DISTINCT e.cc1, e.cc2, e.cc3, e.cc4, e.cc5, CDept1.name a' +
        's name1, CDept2.name as name2, CDept3.name as name3, CDept4.name' +
        ' as name4, CDept5.name as name5'
      'FROM         EInfo e LEFT OUTER JOIN'
      
        '                      CDept1 ON e.cc1 = CDept1.cc1 AND CDept1.co' +
        ' = :co LEFT OUTER JOIN'
      
        '                      CDept2 ON e.cc2 = CDept2.cc2 AND CDept2.co' +
        ' = :co LEFT OUTER JOIN'
      
        '                      CDept3 ON e.cc3 = CDept3.cc3 AND CDept3.co' +
        ' = :co LEFT OUTER JOIN'
      
        '                      CDept4 ON e.cc4 = CDept4.cc4 AND CDept4.co' +
        ' = :co LEFT OUTER JOIN'
      
        '                      CDept5 ON e.cc5 = CDept5.cc5 AND CDept5.co' +
        ' = :co'
      'WHERE     (e.co = :co)'
      'UNION ALL'
      
        'SELECT DISTINCT e.cc1, e.cc2, e.cc3, e.cc4, e.cc5, CDept1.name a' +
        's name1, CDept2.name as name2, CDept3.name as name3, CDept4.name' +
        ' as name4, CDept5.name as name5'
      'FROM         ERate e LEFT OUTER JOIN'
      
        '                      CDept1 ON e.cc1 = CDept1.cc1 AND CDept1.co' +
        ' = :co LEFT OUTER JOIN'
      
        '                      CDept2 ON e.cc2 = CDept2.cc2 AND CDept2.co' +
        ' = :co LEFT OUTER JOIN'
      
        '                      CDept3 ON e.cc3 = CDept3.cc3 AND CDept3.co' +
        ' = :co LEFT OUTER JOIN'
      
        '                      CDept4 ON e.cc4 = CDept4.cc4 AND CDept4.co' +
        ' = :co LEFT OUTER JOIN'
      
        '                      CDept5 ON e.cc5 = CDept5.cc5 AND CDept5.co' +
        ' = :co'
      'WHERE     (e.co = :co)'
      '')
    Left = 96
    Top = 504
  end
  object EDirDepNum: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select t.id, e.empStatus, count(t.id) as DDNum'
      'from EDirDep t, EInfo e'
      'where t.id=e.id and t.co = :co and e.co = :co'
      'group by t.id, e.empStatus'
      'order by count(t.id) desc')
    Left = 40
    Top = 152
  end
  object CoMDLocals: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'co'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select tcode, description from CTax '
      'where taxType = '#39'local'#39' and tcode like '#39'MD%'#39
      'and co = :co')
    Left = 312
    Top = 520
    object CoMDLocalstcode: TStringField
      FieldName = 'tcode'
      Size = 10
    end
    object CoMDLocalsdescription: TStringField
      FieldName = 'description'
      Size = 200
    end
  end
  object cdsMDMaritalStatuses: TevClientDataSet
    Left = 416
    Top = 520
    object cdsMDMaritalStatusesSTATUS_TYPE: TStringField
      FieldName = 'STATUS_TYPE'
      Size = 2
    end
    object cdsMDMaritalStatusesSTATUS_DESCRIPTION: TStringField
      FieldName = 'STATUS_DESCRIPTION'
      Size = 40
    end
  end
  object CTaxLiab: TADOQuery
    Connection = ACCompany
    CursorType = ctStatic
    OnFilterRecord = CTaxLiabFilterRecord
    Parameters = <>
    SQL.Strings = (
      'select c.* from CTaxLiability c where c.transtype = '#39'L'#39
      '')
    Left = 160
    Top = 288
  end
end
