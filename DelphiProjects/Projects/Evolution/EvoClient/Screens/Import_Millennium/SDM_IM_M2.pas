unit SDM_IM_M2;
                
interface

uses
  SysUtils, Classes, genericImport, DB, ADODB,  EvUtils, SDataStructure,
  Variants, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, SFieldCodeValues, EvContext,
  ISDataAccessComponents, EvDataAccessComponents, SDataDictSystem, isBasicUtils,
  evExceptions, EvUIComponents, EvClientDataSet;

//Notes
// CAllDept: jet doesn't support more than one join so I've built this dreadful query
  
type
  IM2DataModule = interface
['{B28600EC-A35E-49D3-A8E2-219B824EA850}']
    procedure FilterLiabilities( bFilter: boolean; afterDate: TDateTime );
  end;

  TDM_IM_M2 = class(TDataModule, ITablespace, IExternalDataModule, IM2DataModule)
    ACCompany: TADOConnection;
    CDeptTied: TADOTable;
    CDeptUntied: TADOTable;
    CInfo: TADOTable;
    Ethnicity: TADOQuery;
    cdsEthnicity: TevClientDataSet;
    cdsEthnicityCode: TStringField;
    cdsEthnicityName: TStringField;
    EmpStatus: TADOQuery;
    cdsEmpStatus: TevClientDataSet;
    StringField1: TStringField;
    StringField2: TStringField;
    PayFreq: TADOQuery;
    cdsPayFreq: TevClientDataSet;
    StringField3: TStringField;
    StringField4: TStringField;
    EmpType: TADOQuery;
    cdsEmpType: TevClientDataSet;
    StringField5: TStringField;
    StringField6: TStringField;
    CoTaxes: TADOQuery;
    cdsCoTaxes: TevClientDataSet;
    StringField7: TStringField;
    StringField8: TStringField;
    cdsCoTaxesType: TStringField;
    cdsCoTaxesSubType: TStringField;
    EInfo: TADOTable;
    RatePer: TADOQuery;
    cdsRatePer: TevClientDataSet;
    StringField9: TStringField;
    StringField10: TStringField;
    PayGrade: TADOQuery;
    EInfoPayGradeDesc: TStringField;
    EInfos_Generation: TIntegerField;
    EInfos_Lineage: TBlobField;
    EInfoco: TWideStringField;
    EInfoid: TWideStringField;
    EInfolastName: TWideStringField;
    EInfofirstName: TWideStringField;
    EInfomiddleName: TWideStringField;
    EInfosalutation: TWideStringField;
    EInfosurname: TWideStringField;
    EInfonickname: TWideStringField;
    EInfopriorLastName: TWideStringField;
    EInfoaddress1: TWideStringField;
    EInfoaddress2: TWideStringField;
    EInfocity: TWideStringField;
    EInfostate: TWideStringField;
    EInfozip: TWideStringField;
    EInfocounty: TWideStringField;
    EInfocountry: TWideStringField;
    EInfohomePhone: TWideStringField;
    EInfossn: TWideStringField;
    EInfobirthDate: TDateTimeField;
    EInfosex: TWideStringField;
    EInfoethnicity: TWideStringField;
    EInfomaritalStatus: TWideStringField;
    EInfodisability: TBooleanField;
    EInfodisabilityDesc: TWideStringField;
    EInfoveteran: TBooleanField;
    EInfoveteranDesc: TWideStringField;
    EInfosmoker: TBooleanField;
    EInfoempStatus: TWideStringField;
    EInfoempType: TWideStringField;
    EInfopayGroup: TWideStringField;
    EInfohireDate: TDateTimeField;
    EInforehireDate: TDateTimeField;
    EInfoadjSeniorityDate: TDateTimeField;
    EInfotermDate: TDateTimeField;
    EInfotermReason: TWideStringField;
    EInfocc: TWideStringField;
    EInfocc1: TWideStringField;
    EInfocc2: TWideStringField;
    EInfocc3: TWideStringField;
    EInfocc4: TWideStringField;
    EInfocc5: TWideStringField;
    EInfopositionCode: TWideStringField;
    EInfopositionInfoLocked: TBooleanField;
    EInfotitle: TWideStringField;
    EInfoeeoClass: TWideStringField;
    EInfowcc: TWideStringField;
    EInfoflsaOtExempt: TBooleanField;
    EInfoworkPhone: TWideStringField;
    EInfoworkPhoneExt: TWideStringField;
    EInfomailStop: TWideStringField;
    EInfoemailAddress: TWideStringField;
    EInfoclock: TWideStringField;
    EInfomiscCheck1: TBooleanField;
    EInfomiscCheck2: TBooleanField;
    EInfomiscCheck3: TBooleanField;
    EInfomiscCheck4: TBooleanField;
    EInfomiscCheck5: TBooleanField;
    EInfotaxForm: TWideStringField;
    EInfopension: TBooleanField;
    EInfostatutory: TBooleanField;
    EInfodeceased: TBooleanField;
    EInfodeferredComp: TBooleanField;
    EInfolegalRep: TBooleanField;
    EInfodomesticEmpl: TBooleanField;
    EInfoseasonal: TBooleanField;
    EInfoi9Verified: TBooleanField;
    EInfoi9ReVerify: TDateTimeField;
    EInfocitizenship: TWideStringField;
    EInfovisaType: TWideStringField;
    EInfovisaExpiration: TDateTimeField;
    EInfounion: TWideStringField;
    EInfounionDate: TDateTimeField;
    EInfounionInitFees: TBooleanField;
    EInfounionDues: TBooleanField;
    EInfosupervisorId: TWideStringField;
    EInfosupervisorName: TWideStringField;
    EInfopayGrade: TWideStringField;
    EInfobaseRate: TFloatField;
    EInforatePer: TWideStringField;
    EInfosalary: TFloatField;
    EInfodefaultHours: TFloatField;
    EInfopayFrequency: TWideStringField;
    EInfoannualSalary: TFloatField;
    EInfoautoPay: TWideStringField;
    EInfolastRaiseDate: TDateTimeField;
    EInfolastRaiseAmount: TFloatField;
    EInfolastRaiseReason: TWideStringField;
    EInfonextRaiseDate: TDateTimeField;
    EInfolastReviewDate: TDateTimeField;
    EInfolastReviewRating: TWideStringField;
    EInfonextReviewDate: TDateTimeField;
    EInfouser1: TWideStringField;
    EInfouser2: TWideStringField;
    EInfouser3: TWideStringField;
    EInfouser4: TWideStringField;
    EInfouser5: TWideStringField;
    EInfouser6: TWideStringField;
    EInfouser7: TWideStringField;
    EInfouser8: TWideStringField;
    EInfoee401kDeferral: TFloatField;
    EInfoee401kCalc: TWideStringField;
    EInfoee401kContinue: TBooleanField;
    EInfoee401kEligibleDate: TDateTimeField;
    EInfoee401kStatus: TWideStringField;
    EInfoee401kSuspendDate: TDateTimeField;
    EInfoer401kMatch: TFloatField;
    EInfohighComp: TBooleanField;
    EInfoowner: TBooleanField;
    EInfoownerPercent: TFloatField;
    EInfoownerRelated: TBooleanField;
    EInfoownerSSN: TWideStringField;
    EInfojobCode: TWideStringField;
    EInfos_GUID: TGuidField;
    EInfotipped: TWideStringField;
    EInfoshift: TWideStringField;
    EEO: TADOQuery;
    ETax: TADOQuery;
    ERate: TADOQuery;
    RateCode: TADOQuery;
    cdsRateCode: TevClientDataSet;
    cdsRateCodeNumber: TIntegerField;
    Reciprocal: TADOQuery;
    cdsReciprocal: TevClientDataSet;
    StringField11: TStringField;
    StringField12: TStringField;
    EDs: TADOQuery;
    EDirDep: TADOQuery;
    EDed: TADOQuery;
    EEarn: TADOQuery;
    DedCalc: TADOQuery;
    cdsDedCalc: TevClientDataSet;
    StringField13: TStringField;
    StringField14: TStringField;
    EarnCalc: TADOQuery;
    cdsEarnCalc: TevClientDataSet;
    StringField15: TStringField;
    StringField16: TStringField;
    Agencies: TADOQuery;
    EYtd: TADOQuery;
    cdsDedCalcTargetField: TStringField;
    EDirDepNum: TADOQuery;
    CoMDLocals: TADOQuery;
    cdsMDMaritalStatuses: TevClientDataSet;
    cdsMDMaritalStatusesSTATUS_TYPE: TStringField;
    cdsMDMaritalStatusesSTATUS_DESCRIPTION: TStringField;
    CTaxLiab: TADOQuery;
    CAllDept: TADOQuery;
    cdsM3Cat: TevClientDataSet;
    StringField18: TStringField;
    cdsM3Catnumber: TIntegerField;
    cdsM3Catdesc: TStringField;
    cdsEvoDBDT: TevClientDataSet;
    cdsEvoDBDTDBDT: TStringField;
    cdsEvoDBDTlevel: TIntegerField;
    procedure RateCodeFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure CTaxLiabFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    FLogger: IImportLogger;
    FConnectionString: string;
    FCustomCompanyNumber: string;
    FLiabilityDate: TDateTime;
  protected
    {IExternalDataModule}
    procedure OpenConnection( aConnectionString: string );
    procedure CloseConnection;
    function Connected: boolean;
    function ConnectionString: string;
    function CustomCompanyNumber: string;
    procedure SetLogger( aLogger: IImportLogger );
    {IM2DataModule}
    procedure FilterLiabilities( bFilter: boolean; afterDate: TDateTime );
  public
    {ITableSpace}
    function GetTable( const name: string ): TDataSet;
    procedure GetAvailableTables( const list: TStrings );
  end;

implementation

uses
  DateUtils;

{$R *.dfm}

{ TDM_IM_M2 }

procedure TDM_IM_M2.CloseConnection;
var
  i: integer;
begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TCustomADODataSet then
    try
      if TCustomADODataSet(Components[i]).Active then
        TCustomADODataSet(Components[i]).Close;
    except
      on e: Exception do FLogger.LogDebug( 'Error occured while closing connection. Ignored.', e.Message );
      //dont raise
    end;
  ACCompany.Close;
  FConnectionString := '';
end;

function TDM_IM_M2.Connected: boolean;
begin
  Result := ACCompany.Connected;
end;

function TDM_IM_M2.ConnectionString: string;
begin
  Result := FConnectionString;
end;

function TDM_IM_M2.CustomCompanyNumber: string;
begin
  Result := FCustomCompanyNumber;
end;

procedure TDM_IM_M2.GetAvailableTables(const list: TStrings);
var
  i: integer;
begin
  for i := 0 to ComponentCount-1 do
    if Components[i] is TCustomADODataSet then
      list.Add(Components[i].Name);
end;

function TDM_IM_M2.GetTable(const name: string): TDataSet;
var
  c: TComponent;
  tn: string;
begin
  tn := name;
  if (tn = 'EInfoSalary') or (tn = 'EInfoFix') then
    tn := 'EInfo';
  c := FindComponent( tn );
  if not assigned(c) {or not (c is TSDQuery)} then
    raise EevException.CreateFmt( 'Unknown table requested: <%s>',[name]);
  Result := c as TDataSet;
end;

procedure OpenClientForCompany( custom_company_number: string );
begin
  DM_TEMPORARY.TMP_CO.DataRequired('' );
  if DM_TEMPORARY.TMP_CO.Locate('CUSTOM_COMPANY_NUMBER', custom_company_number, []) then
    ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO['CL_NBR'])
  else
    raise EevException.CreateFmt( 'Company <%s> doesn''t exist in Evolution database', [custom_company_number] );
  DM_COMPANY.CO.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_SYSTEM.SY_HR_EEO.DataRequired();
  DM_SYSTEM.SY_STATE_MARITAL_STATUS.DataRequired();
  DM_SYSTEM.SY_STATES.DataRequired();
  DM_COMPANY.CO_STATES.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_COMPANY.CO_SUI.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_COMPANY.CO_E_D_CODES.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_CLIENT.CL_AGENCY.DataRequired();
  DM_CLIENT.CL_E_DS.DataRequired();
end;

procedure Append( ds: TevClientDataSet; aFields: string; vals: array of Variant );
begin
  ds.Append;
  if Length(vals) > 1 then
    ds[aFields] := VarArrayOf(vals)
  else
    ds[aFields] := vals[0];
  ds.Post;
end;

procedure TDM_IM_M2.OpenConnection(aConnectionString: string);
const
  sText = 'Opening M2 database';
var
  t: TStringList;
  i, n: integer;
  s: string;
  s1: string;
  MD_SY_STATES_NBR: Variant;
  ms: TSY_STATE_MARITAL_STATUS;
begin
  if not Connected or (FConnectionString <> aConnectionString) then
  begin
    FLogger.LogEntry( sText );
    try
      try
        ctx_StartWait( sText, ComponentCount-1 );
        try
          CloseConnection;
          try
            ACCompany.ConnectionString := aConnectionString;
            ACCompany.Open;
            FConnectionString := aConnectionString;
            t := TStringList.Create;
            try
              t.Text := StringReplace(aConnectionString, ';', #13, [rfReplaceAll]);
              FCustomCompanyNumber := UpperCase(ChangeFileExt(ExtractFileName(t.Values['Data Source']), ''));
            finally
              t.Free;
            end;
            EYtd.Parameters.ParamByName('Year').Value := YearOf(Date);
            for i := 0 to ComponentCount - 1 do
              if Components[i] is TCustomADODataSet then
              try
                TCustomADODataSet(Components[i]).Open;
                ctx_UpdateWait( sText, i );
              except
                FLogger.StopExceptionAndWarnFmt( 'Failed to read data', [] );
              end;
            OpenClientForCompany( FCustomCompanyNumber );
            t := TStringList.Create;
            try
              t.Text := Ethnicity_ComboChoices;
              cdsEthnicity.CreateDataSet;
              for i := 0 to Pred(t.Count) do
              begin
                s := t[i];
                cdsEthnicity.Append;
                cdsEthnicity['Name'] := GetNextStrValue(s, #9);
                cdsEthnicity['Code'] := s;
                cdsEthnicity.Post;
              end;

              t.Text := EE_TerminationCode_ComboChoices;
              cdsEmpStatus.CreateDataSet;
              for i := 0 to Pred(t.Count) do
              begin
                s := t[i];
                cdsEmpStatus.Append;
                cdsEmpStatus['Name'] := GetNextStrValue(s, #9);
                cdsEmpStatus['Code'] := s;
                cdsEmpStatus.Post;
              end;

              t.Text := PayFrequencyType_ComboChoices;
              cdsPayFreq.CreateDataSet;
              for i := 0 to Pred(t.Count) do
              begin
                s := t[i];
                cdsPayFreq.Append;
                cdsPayFreq['Name'] := GetNextStrValue(s, #9);
                cdsPayFreq['Code'] := s;
                cdsPayFreq.Post;
              end;

              t.Text := PositionStatus_ComboChoices;
              cdsEmpType.CreateDataSet;
              for i := 0 to Pred(t.Count) do
              begin
                s := t[i];
                cdsEmpType.Append;
                cdsEmpType['Name'] := GetNextStrValue(s, #9);
                cdsEmpType['Code'] := s;
                cdsEmpType.Post;
              end;

              cdsCoTaxes.CreateDataSet;
              cdsCoTaxes.Append;
              cdsCoTaxes['Type'] := 'F';
              cdsCoTaxes['SubType'] := 'FE';
              cdsCoTaxes['Name'] := 'Federal';
              cdsCoTaxes.Post;
              cdsCoTaxes.Append;
              cdsCoTaxes['Type'] := 'F';
              cdsCoTaxes['SubType'] := 'OE';
              cdsCoTaxes['Name'] := 'EE OASDI';
              cdsCoTaxes.Post;
              cdsCoTaxes.Append;
              cdsCoTaxes['Type'] := 'F';
              cdsCoTaxes['SubType'] := 'OR';
              cdsCoTaxes['Name'] := 'ER OASDI';
              cdsCoTaxes.Post;
              cdsCoTaxes.Append;
              cdsCoTaxes['Type'] := 'F';
              cdsCoTaxes['SubType'] := 'ME';
              cdsCoTaxes['Name'] := 'EE Medicare';
              cdsCoTaxes.Post;
              cdsCoTaxes.Append;
              cdsCoTaxes['Type'] := 'F';
              cdsCoTaxes['SubType'] := 'MR';
              cdsCoTaxes['Name'] := 'ER Medicare';
              cdsCoTaxes.Post;
              cdsCoTaxes.Append;
              cdsCoTaxes['Type'] := 'F';
              cdsCoTaxes['SubType'] := 'FU';
              cdsCoTaxes['Name'] := 'FUI';
              cdsCoTaxes.Post;
              cdsCoTaxes.Append;
              cdsCoTaxes['Type'] := 'F';
              cdsCoTaxes['SubType'] := 'EI';
              cdsCoTaxes['Name'] := 'EIC';
              cdsCoTaxes.Post;
              DM_COMPANY.CO_STATES.First;
              while not DM_COMPANY.CO_STATES.Eof do
              begin
                cdsCoTaxes.Append;
                cdsCoTaxes['Type'] := 'S';
                cdsCoTaxes['Code'] := DM_COMPANY.CO_STATES.STATE.Value;
                cdsCoTaxes['SubType'] := 'ST';
                cdsCoTaxes['Name'] := DM_COMPANY.CO_STATES.STATE.Value + ' State';
                cdsCoTaxes.Post;
                cdsCoTaxes.Append;
                cdsCoTaxes['Type'] := 'S';
                cdsCoTaxes['Code'] := DM_COMPANY.CO_STATES.STATE.Value;
                cdsCoTaxes['SubType'] := 'SE';
                cdsCoTaxes['Name'] := DM_COMPANY.CO_STATES.STATE.Value + ' EE SDI';
                cdsCoTaxes.Post;
                cdsCoTaxes.Append;
                cdsCoTaxes['Type'] := 'S';
                cdsCoTaxes['Code'] := DM_COMPANY.CO_STATES.STATE.Value;
                cdsCoTaxes['SubType'] := 'SR';
                cdsCoTaxes['Name'] := DM_COMPANY.CO_STATES.STATE.Value + ' ER SDI';
                cdsCoTaxes.Post;
                DM_COMPANY.CO_STATES.Next;
              end;
              DM_COMPANY.CO_SUI.First;
              while not DM_COMPANY.CO_SUI.Eof do
              begin
                cdsCoTaxes.Append;
                cdsCoTaxes['Type'] := 'U';
                cdsCoTaxes['Code'] := DM_COMPANY.CO_SUI.SY_SUI_NBR.AsString;
                cdsCoTaxes['SubType'] := DM_COMPANY.CO_SUI.FieldbyName('STATE').AsString;
                cdsCoTaxes['Name'] := DM_COMPANY.CO_SUI.FieldbyName('SUI_Name').AsString;
                cdsCoTaxes.Post;
                DM_COMPANY.CO_SUI.Next;
              end;
              DM_COMPANY.CO_LOCAL_TAX.First;
              while not DM_COMPANY.CO_LOCAL_TAX.Eof do
              begin
                cdsCoTaxes.Append;
                cdsCoTaxes['Type'] := 'Z';
                cdsCoTaxes['Code'] := DM_COMPANY.CO_LOCAL_TAX.SY_LOCALS_NBR.AsString;
//                cdsCoTaxes['SubType'] := DM_COMPANY.CO_LOCAL_TAX.FieldbyName('LocalName').AsString;
                cdsCoTaxes['Name'] := DM_COMPANY.CO_LOCAL_TAX.FieldbyName('LocalName').AsString;
                cdsCoTaxes.Post;
                DM_COMPANY.CO_LOCAL_TAX.Next;
              end;

              cdsRatePer.CreateDataSet;
              cdsRatePer.Append;
              cdsRatePer['Code'] := 'H';
              cdsRatePer['Name'] := 'Hour';
              cdsRatePer.Post;
              cdsRatePer.Append;
              cdsRatePer['Code'] := 'D';
              cdsRatePer['Name'] := 'Day';
              cdsRatePer.Post;
              cdsRatePer.Append;
              cdsRatePer['Code'] := 'W';
              cdsRatePer['Name'] := 'Week';
              cdsRatePer.Post;

              n := RateCode.RecordCount;
              cdsRateCode.CreateDataSet;
              for i := 2 to n + 1 do
                if not RateCode.Locate('Code', i, []) then
                begin
                  cdsRateCode.Append;
                  cdsRateCode['Number'] := i;
                  cdsRateCode.Post;
                end;
              RateCode.Filtered := True;

              t.Text := StateReciprocal_ComboChoices;
              cdsReciprocal.CreateDataSet;
              for i := 0 to Pred(t.Count) do
              begin
                s := t[i];
                cdsReciprocal.Append;
                cdsReciprocal['Name'] := GetNextStrValue(s, #9);
                cdsReciprocal['Code'] := s;
                cdsReciprocal.Post;
              end;

              t.Text := ScheduledCalcMethod_ComboBoxChoices;
              cdsDedCalc.CreateDataSet;
              for i := 0 to Pred(t.Count) do
              begin
                s := t[i];
                s1 := GetNextStrValue(s, #9);
//                Append( cdsDedCalc, 'Name;Code', [s1, s] );
                Append( cdsDedCalc, 'Name;Code;TargetField', [s1, s, 'Amount'] ); //TargetField is part of the key - it's value is used in the import code!
                Append( cdsDedCalc, 'Name;Code;TargetField', [s1, s, '%'] );
              end;

              t.Text := ScheduledCalcMethod_ComboBoxChoices;
              cdsEarnCalc.CreateDataSet;
              for i := 0 to Pred(t.Count) do
              begin
                s := t[i];
                cdsEarnCalc.Append;
                cdsEarnCalc['Name'] := GetNextStrValue(s, #9);
                cdsEarnCalc['Code'] := s;
                cdsEarnCalc.Post;
              end;
            finally
              t.Free;
            end;

            cdsMDMaritalStatuses.CreateDataSet;
            MD_SY_STATES_NBR := DM_SYSTEM.SY_STATES.Lookup('STATE','MD' ,'SY_STATES_NBR'); 
            ms := DM_SYSTEM.SY_STATE_MARITAL_STATUS;
            ms.First;
            while not ms.Eof do
            begin
              if ms.SY_STATES_NBR.Value = MD_SY_STATES_NBR then
                Append( cdsMDMaritalStatuses, 'STATUS_TYPE;STATUS_DESCRIPTION', [ms.STATUS_TYPE.Value, ms.STATUS_DESCRIPTION.Value]);
              ms.Next;
            end;

            CInfo.First;
            cdsM3Cat.CreateDataSet;
            Append( cdsM3Cat, 'Category;number;desc', ['CDept1',1, CInfo['cc1Desc']] );
            Append( cdsM3Cat, 'Category;number;desc', ['CDept2',2, CInfo['cc2Desc']] );
            Append( cdsM3Cat, 'Category;number;desc', ['CDept3',3, CInfo['cc3Desc']] );
            Append( cdsM3Cat, 'Category;number;desc', ['CDept4',4, CInfo['cc4Desc']] );
            Append( cdsM3Cat, 'Category;number;desc', ['CDept5',5, CInfo['cc5Desc']] );
            
            cdsEvoDBDT.CreateDataSet;
            Append( cdsEvoDBDT, 'DBDT;level', ['Division',1] );
            Append( cdsEvoDBDT, 'DBDT;level', ['Branch',2] );
            Append( cdsEvoDBDT, 'DBDT;level', ['Department',3] );
            Append( cdsEvoDBDT, 'DBDT;level', ['Team',4] );

          except
            CloseConnection;
            raise;
          end;
          if EDs.Active then
          begin
            if EDs.FindField('eord') <> nil then
              EDs.FindField('eord').DisplayWidth := 10;
            if EDs.FindField('code') <> nil then
              EDs.FindField('code').DisplayWidth := 10;
          end;
        finally
          ctx_EndWait;
        end;
      except
        FLogger.PassthroughExceptionAndWarnFmt( sImportFailedToOpenConnection, [] );
      end;
    finally
      FLogger.LogExit;
    end;
  end;
end;

procedure TDM_IM_M2.SetLogger(aLogger: IImportLogger);
begin
  FLogger := aLogger;
end;

procedure TDM_IM_M2.RateCodeFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
var
  i: Integer;
begin
  Accept := True;
  try
    i := StrToInt(Trim(DataSet['Code']));
    Accept := i < 2;
  except
  end;
end;

procedure TDM_IM_M2.FilterLiabilities(bFilter: boolean; afterDate: TDateTime);
begin
  FLiabilityDate := afterDate;
  if CTaxLiab.Filtered then
    CTaxLiab.Filtered := false;
  CTaxLiab.Filtered := bFilter;
end;

procedure TDM_IM_M2.CTaxLiabFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := DataSet.FieldByName('liabilityDate').AsDateTime >= FLiabilityDate;
end;

end.
