inherited EDIT_IM_M2_CO_AND_EE: TEDIT_IM_M2_CO_AND_EE
  inherited PageControl1: TevPageControl
    MultiLine = True
    inherited TabSheet1: TTabSheet
      inherited evPanel1: TevPanel
        Height = 201
        inherited evPanel5: TevPanel
          Top = 129
          Height = 72
          inherited EvBevel2: TEvBevel
            Height = 68
          end
          inherited pnlSetup: TevPanel
            Height = 68
            inherited pnlSetupControls: TevPanel
              Height = 29
              inline ParamInput: TMEEImportParamFrame
                Left = 8
                Top = 8
                Width = 433
                Height = 309
                TabOrder = 0
              end
            end
          end
        end
        inherited pnlConnectionInputHolder: TevPanel
          Height = 129
          inherited ConnectDatabaseFileNameFrame: TConnectDatabaseFileNameFrame
            Height = 103
            Align = alTop
            inherited lblSource: TevLabel
              Left = 11
            end
            inherited evLabel2: TevLabel
              Left = 432
            end
            inherited evLabel3: TevLabel
              Left = 432
            end
            inherited BitBtn1: TevBitBtn
              Left = 10
            end
            inherited evEdit2: TevEdit
              Left = 512
            end
            inherited evEdit3: TevEdit
              Left = 512
            end
          end
          object pnlInfo: TevPanel
            Left = 2
            Top = 105
            Width = 423
            Height = 22
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object lblInfo: TevLabel
              Left = 8
              Top = 4
              Width = 3
              Height = 13
            end
          end
        end
      end
    end
    inherited TabSheet5: TTabSheet
      inherited viewerFrame: TTableSpaceViewFrame
        Height = 201
        inherited evPanel2: TevPanel
          Height = 160
          inherited dgView: TevDBGrid
            Height = 411
          end
        end
      end
    end
    object tsDBDT: TTabSheet [2]
      Caption = 'DBDT'
      ImageIndex = 18
      inline BinderFrame15: TBinderFrame
        Left = 0
        Top = 17
        Width = 427
        Height = 184
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 146
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 149
            Height = 0
            inherited evPanel4: TevPanel
              Width = 149
            end
            inherited dgRight: TevDBGrid
              Width = 149
              Height = 146
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 419
            inherited evPanel5: TevPanel
              Width = 419
            end
            inherited dgBottom: TevDBGrid
              Width = 419
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 419
          end
        end
      end
      object pnlDBDTMapUsed: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 17
        Align = alTop
        BevelOuter = bvNone
        Caption = 
          'This mapping will not be used because CInfo.CCLevelsTied is true' +
          '. DBDTs will be imported from CDeptTied table as is.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
    end
    object tsEthnicity: TTabSheet [3]
      Caption = 'Ethnicity'
      ImageIndex = 4
      inline BinderFrame1: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 163
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 148
            Height = 0
            inherited evPanel4: TevPanel
              Width = 148
            end
            inherited dgRight: TevDBGrid
              Width = 148
              Height = 163
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 418
            inherited evPanel5: TevPanel
              Width = 418
            end
            inherited dgBottom: TevDBGrid
              Width = 418
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 418
          end
        end
      end
    end
    object tsEmpStatus: TTabSheet [4]
      Caption = 'Emp Status'
      ImageIndex = 5
      inline BinderFrame2: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 148
          end
          inherited pnlTopLeft: TevPanel
            Height = 148
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 374
            Height = 148
            inherited evPanel4: TevPanel
              Width = 374
            end
            inherited dgRight: TevDBGrid
              Width = 374
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 644
            inherited evPanel5: TevPanel
              Width = 644
            end
            inherited dgBottom: TevDBGrid
              Width = 644
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 644
          end
        end
      end
    end
    object tsPayFreq: TTabSheet [5]
      Caption = 'Pay Frequency'
      ImageIndex = 6
      inline BinderFrame3: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 148
          end
          inherited pnlTopLeft: TevPanel
            Height = 148
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 374
            Height = 148
            inherited evPanel4: TevPanel
              Width = 374
            end
            inherited dgRight: TevDBGrid
              Width = 374
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 644
            inherited evPanel5: TevPanel
              Width = 644
            end
            inherited dgBottom: TevDBGrid
              Width = 644
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 644
          end
        end
      end
    end
    object tsEmpType: TTabSheet [6]
      Caption = 'Emp Type'
      ImageIndex = 7
      inline BinderFrame4: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 180
          end
          inherited pnlTopLeft: TevPanel
            Height = 180
            inherited dgLeft: TevDBGrid
              Height = 155
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 374
            Height = 180
            inherited evPanel4: TevPanel
              Width = 374
            end
            inherited dgRight: TevDBGrid
              Width = 374
              Height = 155
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 644
            inherited evPanel5: TevPanel
              Width = 644
            end
            inherited dgBottom: TevDBGrid
              Width = 644
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 644
          end
        end
      end
    end
    object tsTaxes: TTabSheet [7]
      Caption = 'Taxes'
      inline BinderFrame5: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 242
          end
          inherited pnlTopLeft: TevPanel
            Height = 242
            inherited dgLeft: TevDBGrid
              Height = 217
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 461
            Height = 242
            inherited evPanel4: TevPanel
              Width = 461
            end
            inherited dgRight: TevDBGrid
              Width = 461
              Height = 217
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 731
            inherited evPanel5: TevPanel
              Width = 731
            end
            inherited dgBottom: TevDBGrid
              Width = 731
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 731
          end
        end
      end
    end
    object tsMDLocals: TTabSheet [8]
      Caption = 'MD Locals'
      ImageIndex = 17
      inline BinderFrame14: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 196
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 288
            Height = 0
            inherited evPanel4: TevPanel
              Width = 288
            end
            inherited dgRight: TevDBGrid
              Width = 288
              Height = 196
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 558
            inherited evPanel5: TevPanel
              Width = 558
            end
            inherited dgBottom: TevDBGrid
              Width = 558
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 558
          end
        end
      end
    end
    object tsRatePer: TTabSheet [9]
      Caption = 'Rate Per'
      ImageIndex = 9
      inline BinderFrame6: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 177
          end
          inherited pnlTopLeft: TevPanel
            Height = 177
            inherited dgLeft: TevDBGrid
              Height = 152
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 384
            Height = 177
            inherited evPanel4: TevPanel
              Width = 384
            end
            inherited dgRight: TevDBGrid
              Width = 384
              Height = 152
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 654
            inherited evPanel5: TevPanel
              Width = 654
            end
            inherited dgBottom: TevDBGrid
              Width = 654
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 654
          end
        end
      end
    end
    object tsEEO: TTabSheet [10]
      Caption = 'EEO Class'
      ImageIndex = 10
      inline BinderFrame7: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 177
          end
          inherited pnlTopLeft: TevPanel
            Height = 177
            inherited dgLeft: TevDBGrid
              Height = 152
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 384
            Height = 177
            inherited evPanel4: TevPanel
              Width = 384
            end
            inherited dgRight: TevDBGrid
              Width = 384
              Height = 152
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 654
            inherited evPanel5: TevPanel
              Width = 654
            end
            inherited dgBottom: TevDBGrid
              Width = 654
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 654
          end
        end
      end
    end
    object tsRateCode: TTabSheet [11]
      Caption = 'Rate Code'
      ImageIndex = 11
      inline BinderFrame8: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 148
          end
          inherited pnlTopLeft: TevPanel
            Height = 148
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 381
            Height = 148
            inherited evPanel4: TevPanel
              Width = 381
            end
            inherited dgRight: TevDBGrid
              Width = 381
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 651
            inherited evPanel5: TevPanel
              Width = 651
            end
            inherited dgBottom: TevDBGrid
              Width = 651
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 651
          end
        end
      end
    end
    object tsReciprocal: TTabSheet [12]
      Caption = 'Reciprocal'
      ImageIndex = 12
      inline BinderFrame9: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 221
          end
          inherited pnlTopLeft: TevPanel
            Height = 221
            inherited dgLeft: TevDBGrid
              Height = 196
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 678
            Height = 221
            inherited evPanel4: TevPanel
              Width = 678
            end
            inherited dgRight: TevDBGrid
              Width = 678
              Height = 196
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 948
            inherited evPanel5: TevPanel
              Width = 948
            end
            inherited dgBottom: TevDBGrid
              Width = 948
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 948
          end
        end
      end
    end
    object tsEDs: TTabSheet [13]
      Caption = 'E/Ds'
      ImageIndex = 13
      inline BinderFrame10: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 320
          end
          inherited pnlTopLeft: TevPanel
            Height = 320
            inherited dgLeft: TevDBGrid
              Height = 295
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 422
            Height = 320
            inherited evPanel4: TevPanel
              Width = 422
            end
            inherited dgRight: TevDBGrid
              Width = 422
              Height = 295
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 692
            inherited evPanel5: TevPanel
              Width = 692
            end
            inherited dgBottom: TevDBGrid
              Width = 692
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 692
          end
        end
      end
    end
    object tsDedCalc: TTabSheet [14]
      Caption = 'Ded Calc Type'
      ImageIndex = 14
      inline BinderFrame11: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 148
          end
          inherited pnlTopLeft: TevPanel
            Height = 148
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 374
            Height = 148
            inherited evPanel4: TevPanel
              Width = 374
            end
            inherited dgRight: TevDBGrid
              Width = 374
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 644
            inherited evPanel5: TevPanel
              Width = 644
            end
            inherited dgBottom: TevDBGrid
              Width = 644
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 644
          end
        end
      end
    end
    object tsEarnCalc: TTabSheet [15]
      Caption = 'Earn Calc Type'
      ImageIndex = 15
      inline BinderFrame12: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 123
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 288
            Height = 0
            inherited evPanel4: TevPanel
              Width = 288
            end
            inherited dgRight: TevDBGrid
              Width = 288
              Height = 123
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 558
            inherited evPanel5: TevPanel
              Width = 558
            end
            inherited dgBottom: TevDBGrid
              Width = 558
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 558
          end
        end
      end
    end
    object tsAgencies: TTabSheet [16]
      Caption = 'Agencies'
      ImageIndex = 16
      inline BinderFrame13: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 201
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 221
          end
          inherited pnlTopLeft: TevPanel
            Height = 221
            inherited dgLeft: TevDBGrid
              Height = 196
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 678
            Height = 221
            inherited evPanel4: TevPanel
              Width = 678
            end
            inherited dgRight: TevDBGrid
              Width = 678
              Height = 196
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 948
            inherited evPanel5: TevPanel
              Width = 948
            end
            inherited dgBottom: TevDBGrid
              Width = 948
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 948
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited LogFrame: TImportLoggerViewFrame
        Height = 72
        inherited pnlLog: TevPanel
          Height = 72
          inherited LoggerRichView: TImportLoggerRichViewFrame
            Height = 72
            inherited pnlUserView: TevPanel
              Height = 72
              inherited pnlBottom: TevPanel
                Height = 102
                inherited evSplitter2: TevSplitter
                  Height = 102
                end
                inherited pnlLeft: TevPanel
                  Height = 102
                  inherited mmDetails: TevMemo
                    Height = 77
                  end
                end
                inherited pnlRight: TevPanel
                  Height = 102
                  inherited mmContext: TevMemo
                    Height = 77
                  end
                end
              end
            end
          end
        end
      end
    end
    inherited tbshQueue: TTabSheet
      inherited ImportQueueFrame: TImportQueueFrame
        Height = 201
        inherited evPanel2: TevPanel
          Height = 144
          inherited lvQueue: TevListView
            Height = 144
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 525
  end
end
