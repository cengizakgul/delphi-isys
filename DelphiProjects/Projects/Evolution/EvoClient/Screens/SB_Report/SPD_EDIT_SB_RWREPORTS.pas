// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_RWREPORTS;

interface

uses
   SFrameEntry, StdCtrls, Mask, wwdbedit, Controls, Grids,
  Wwdbigrd, Wwdbgrid, ComCtrls, Classes, Db, Wwdatsrc, SDataStructure,
  EvConsts, EvUtils, Wwdotdot, Wwdbcomb, ISZippingRoutines, EvContext,
  DBCtrls, Menus, sysutils, Forms, Dialogs,
  EvSecElement, SSecurityInterface, SPackageEntry, Buttons, SDDClasses,
  ISBasicClasses, EvStreamUtils, SDataDictbureau, EvDataAccessComponents, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIDBMemo, isUIwwDBEdit,
  isUIwwDBComboBox
  ,SSBReportCollection
  ,EvCommonInterfaces, EvDataSet
  ,StrUtils
  ;

type
  TEDIT_SB_RWREPORTS = class(TFrameEntry)
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    grReports: TevDBGrid;
    TabSheet2: TTabSheet;
    Label1: TevLabel;
    wwDBEdit1: TevDBEdit;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evLabel1: TevLabel;
    wwcbReport_Type: TevDBComboBox;
    evLabel2: TevLabel;
    EvDBMemo1: TEvDBMemo;
    evDBComboBox1: TevDBComboBox;
    PopupMenu1: TPopupMenu;
    miCompile: TMenuItem;
    miConversion: TMenuItem;
    btnRWDesigner: TevBitBtn;
    evDBComboBox2: TevDBComboBox;
    evLabel3: TevLabel;
    btnExportReport: TevBitBtn;
    sdExport: TSaveDialog;
    odImport: TOpenDialog;
    ExportReportsToFile1: TMenuItem;
    ImportReportsfromCollectionFile1: TMenuItem;
    btnImportReport: TevBitBtn;
    procedure btnRWDesignerClick(Sender: TObject);
    procedure grReportsDblClick(Sender: TObject);
    procedure miCompileClick(Sender: TObject);
    procedure miConversionClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure btnExportReportClick(Sender: TObject);
    procedure Count(Sender: TObject);
    procedure miReportsCollectionExport(Sender: TObject);
    procedure miReportsCollectionImport(Sender: TObject);
  private
    FCustomView: Boolean;
    fl_report_changed: Boolean;
    FPrevBeforePost:  TDataSetNotifyEvent;
    fImportCollectionDeltaNames:TStringList;

    procedure BeforePost(ADataSet: TDataSet);
    procedure CompressReport;
    procedure LoadNewReportItem(Sender: TObject;
      var ImportedReportType: string; jImportedClienNumber,
      ImportedNbr: integer; var ReportName: string;
      var AssignedNewNbr: integer);
    function ReportNameOrderHead(ProposedName: string): string;

  protected
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure SetReadOnly(Value: Boolean); override;
    function GetIfReadOnly: Boolean; override;
    procedure OnGetSecurityStates(const States: TSecurityStateList); override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure Activate; override;
    procedure Deactivate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

{$R *.DFM}

const
  ctReadOnlyWithExport = 'E';

procedure TEDIT_SB_RWREPORTS.btnRWDesignerClick(Sender: TObject);
var
  MS1, MS2: IEvDualStream;
  Notes, ClName, AncClName: String;

  procedure ShDsgn;
  begin
    MS2.Position := 0;
    ctx_RWDesigner.ShowDesigner(MS2, wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString, Notes, ClName, AncClName);
  end;

begin
  MS1 := TEvDualStreamHolder.CreateInMemory;
  MS2 := TEvDualStreamHolder.CreateInMemory;

  if fl_report_changed then
  begin
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).SaveToStream(MS1.RealStream);
    MS1.Position := 0;
  end
  else
  begin
    ctx_StartWait('Opening report...');
    try
      if wwdsDetail.DataSet.State in [dsBrowse, dsEdit] then
        try
          ctx_RWLocalEngine.GetReportResources(
            wwdsDetail.DataSet.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsInteger,
            CH_DATABASE_SERVICE_BUREAU, MS1);
        except
          on E: Exception do
          begin
            MS1.Clear;
            EvErrMessage(E.Message);
          end;
        end;
    finally
      ctx_EndWait;
    end;
  end;

  MS2.RealStream.CopyFrom(MS1.RealStream, 0);
  ShDsgn;

  if not wwDBEdit1.ReadOnly then
  begin
    MS1.Position := 0;
    MS2.Position := 0;

    if (MS1.Size <> MS2.Size) or not CompareMem(MS1.Memory, MS2.Memory, MS1.Size) then
    begin
      grReports.Enabled := False;

      if not (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) then
        wwdsDetail.DataSet.Edit;

      TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromStream(MS2.RealStream);
      if Length(Notes) > 0 then
        Notes := Notes + #13#13;
      Notes := Notes + 'Last update: ' + DateTimeToStr(Now);
      TBlobField(wwdsDetail.DataSet.FieldByName('NOTES')).AsString := Notes;
      wwdsDetail.DataSet.FieldByName('Class_Name').AsString := ClName;
      wwdsDetail.DataSet.FieldByName('Ancestor_Class_Name').AsString := AncClName;
      fl_report_changed := true;
    end;
  end;
end;


function TEDIT_SB_RWREPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  if not FCustomView then
    if not DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Active then
    begin
      if ctx_DataAccess.SB_CUSTOM_VIEW.Active then
        ctx_DataAccess.SB_CUSTOM_VIEW.Close;

      with TExecDSWrapper.Create('SelectHist') do
      begin
        SetMacro('Columns', 'SB_REPORT_WRITER_REPORTS_NBR, REPORT_DESCRIPTION, REPORT_TYPE, NOTES, NOTES REPORT_FILE, EFFECTIVE_DATE, EFFECTIVE_UNTIL, REC_VERSION, MEDIA_TYPE, ANCESTOR_CLASS_NAME, CLASS_NAME');
        SetMacro('TableName', 'SB_REPORT_WRITER_REPORTS');
        SetParam('StatusDate', ctx_DataAccess.AsOfDate);

        ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
        ctx_DataAccess.SB_CUSTOM_VIEW.Open;
        DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Data := ctx_DataAccess.SB_CUSTOM_VIEW.Data;
        ctx_DataAccess.SB_CUSTOM_VIEW.Close;
      end;
      FCustomView := True;
    end
    else
      FCustomView := False;

  Result := DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS;
end;


procedure TEDIT_SB_RWREPORTS.grReportsDblClick(Sender: TObject);
begin
  inherited;
  btnRWDesigner.Click;
end;

procedure TEDIT_SB_RWREPORTS.miCompileClick(Sender: TObject);
var
  MS: IEvDualStream;
  err: String;

  procedure CompileRep;
  var
    ClName, AncClName: String;
  begin
    MS.Clear;

    ctx_RWLocalEngine.GetReportResources(
      wwdsDetail.DataSet.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsInteger,
      CH_DATABASE_SERVICE_BUREAU, MS);
    MS.Position := 0;

    try
      ctx_RWLocalEngine.CompileReport(MS, ClName, AncClName);
    except
      on E: Exception do
        err := E.Message;
    end;

    if Length(err) > 0 then
      Exit;

    wwdsDetail.DataSet.Edit;
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromStream(MS.RealStream);
    CompressReport;
    wwdsDetail.DataSet.FieldByName('Class_Name').AsString := ClName;
    wwdsDetail.DataSet.FieldByName('Ancestor_Class_Name').AsString := AncClName;
    wwdsDetail.DataSet.Post;
    ctx_DataAccess.PostDataSets([TevClientDataSet(wwdsDetail.DataSet)]);
  end;

begin
  if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) or wwDBEdit1.ReadOnly then
    Exit;

  MS := TEvDualStreamHolder.Create;
  TevClientDataSet(wwdsDetail.DataSet).DisableControls;
  Screen.Cursor := crAppStart;

  try
    err := '';
    ctx_StartWait('Compiling reports...');
    while grReports.SelectedList.Count > 0 do
    begin
      wwdsDetail.DataSet.GotoBookmark(grReports.SelectedList[0]);
      ctx_UpdateWait('Reports left: ' + IntToStr(grReports.SelectedList.Count) + #13 +
        wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString);
      Application.ProcessMessages;
      CompileRep;
      if Length(err) > 0 then
        Break;
      grReports.SelectedList.Delete(0);
    end
  finally
    ctx_EndWait;
    TevClientDataSet(wwdsDetail.DataSet).EnableControls;
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.Cancel;
    Screen.Cursor := crArrow;
  end;

  if Length(err) > 0 then
  begin
    EvErrMessage(Err);
    btnRWDesigner.Click;
  end;
end;


procedure TEDIT_SB_RWREPORTS.SetReadOnly(Value: Boolean);
var
  c: ISecurityElement;
begin
  inherited;
  c := FindSecurityElement(Self, ClassName);
  if not(Assigned(c) and (c.SecurityState = stReadOnly)) then
    btnRWDesigner.SecurityRO := False;

  miCompile.Enabled := not Value;
  miConversion.Enabled := not Value;
  EvDBMemo1.SecurityRO := True;

  btnExportReport.SecurityRO := not (SecurityState[1] in [ctReadOnlyWithExport, ctEnabled]);
end;



procedure TEDIT_SB_RWREPORTS.miConversionClick(Sender: TObject);
const ResFile = 'C:\NotCorrectedReports.txt';
var
  MS1, MS2: IEvDualStream;
  Conv, NotConv, Skipped: Integer;
  F: TextFile;
  lErrorMessage: String;

  function ConvertRep: Boolean;
  var
    ClName, AncClName: String;
  begin
    Result := False;
    lErrorMessage := '';

    MS2.Clear;
    ctx_RWLocalEngine.GetReportResources(
      wwdsDetail.DataSet.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsInteger,
      CH_DATABASE_SERVICE_BUREAU, MS2);
    MS2.Position := 0;

    try
      Result := ctx_RWDesigner.ReCreateWizardReport(MS2);
      if Result then
      begin
        Result := False;
        MS2.Position := 0;
        ctx_RWLocalEngine.CompileReport(MS2, ClName, AncClName);
        Result := True;
      end;
    except
      on E: Exception do
        lErrorMessage := E.Message;
    end;

    if Result then
    begin
      MS2.Position := 0;
      MS1.Clear;
      DeflateStream(MS2.RealStream, MS1.RealStream);
      wwdsDetail.DataSet.Edit;
      TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromStream(MS1.RealStream);
      wwdsDetail.DataSet.FieldByName('Class_Name').AsString := ClName;
      wwdsDetail.DataSet.FieldByName('Ancestor_Class_Name').AsString := AncClName;
      wwdsDetail.DataSet.Post;
      ctx_DataAccess.PostDataSets([TevClientDataSet(wwdsDetail.DataSet)]);
    end;
  end;

begin
  if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) or wwDBEdit1.ReadOnly then
    Exit;

  Conv := 0;
  NotConv := 0;
  Skipped := 0;
  MS1 := TEvDualStreamHolder.Create;
  MS2 := TEvDualStreamHolder.Create;
  TevClientDataSet(wwdsDetail.DataSet).DisableControls;
  Screen.Cursor := crAppStart;
  AssignFile(F, ResFile);
  Rewrite(F);
  try
    ctx_StartWait('Converting reports...', grReports.SelectedList.Count);

    while grReports.SelectedList.Count > 0 do
    begin
      wwdsDetail.DataSet.GotoBookmark(grReports.SelectedList[0]);
      ctx_UpdateWait('Reports left: ' + IntToStr(grReports.SelectedList.Count) +
       '     Corrected: ' + IntToStr(Conv) + '     Produced errors: ' + IntToStr(NotConv) + '     Skipped: ' + IntToStr(Skipped) + #13#13 +
       'Current Report: ' + wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString, Conv + NotConv + Skipped);
      Application.ProcessMessages;
      if not ConvertRep then
      begin
        if lErrorMessage <> '' then
        begin
          Inc(NotConv);
          Writeln(F, wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString +
            StringOfChar(' ', 50 - Length(wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString)) + lErrorMessage);
        end
        else
          Inc(Skipped);
      end
      else
        Inc(Conv);
      grReports.SelectedList.Delete(0);
    end;

  finally
    ctx_EndWait;
    CloseFile(F);
    if NotConv = 0 then
      DeleteFile(ResFile);
    TevClientDataSet(wwdsDetail.DataSet).EnableControls;
    wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.Cancel;
    Screen.Cursor := crArrow;
  end;

  if NotConv > 0 then
    RunShellViewer(ResFile, False);
end;



procedure TEDIT_SB_RWREPORTS.Activate;
begin
  inherited;
  FPrevBeforePost := DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.BeforePost;
  DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.BeforePost := BeforePost;

  fl_report_changed := False;
end;

procedure TEDIT_SB_RWREPORTS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind = NavOK then
  begin
    grReports.Enabled := True;
  end

  else if Kind = NavCancel then
    grReports.Enabled := True;
end;

constructor TEDIT_SB_RWREPORTS.Create(AOwner: TComponent);
begin
  inherited;
  FCustomView := False;
end;


procedure TEDIT_SB_RWREPORTS.Deactivate;
begin
  if FCustomView then
  begin
    DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.Close;
    FCustomView := False;
  end;

  DM_SERVICE_BUREAU.SB_REPORT_WRITER_REPORTS.BeforePost := FPrevBeforePost;

  inherited;
end;

procedure TEDIT_SB_RWREPORTS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if wwdsDetail.DataSet.State = dsBrowse then
    fl_report_changed := False;
end;

procedure TEDIT_SB_RWREPORTS.btnExportReportClick(Sender: TObject);
var
  FS: IEvDualStream;
begin
  sdExport.FileName := NormalizeFileName(Trim(wwdsDetail.DataSet.FieldByName('REPORT_DESCRIPTION').AsString) + '.rwr');

  if not sdExport.Execute then
    Exit;

  sdExport.FileName := ChangeFileExt(sdExport.FileName, '.rwr');

  FS := TEvDualStreamHolder.CreateFromNewFile(sdExport.FileName);

  if fl_report_changed then
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).SaveToStream(FS.RealStream)

  else
  begin
    ctx_StartWait('Opening report...');
    try
      if wwdsDetail.DataSet.State in [dsBrowse, dsEdit] then
        ctx_RWLocalEngine.GetReportResources(
          wwdsDetail.DataSet.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsInteger,
          CH_DATABASE_SERVICE_BUREAU, FS);
    finally
      ctx_EndWait;
    end;
  end;

EvMessage('Report has been successfully exported to file');
end;


procedure TEDIT_SB_RWREPORTS.Count(Sender: TObject);
begin
  if not odImport.Execute then
    Exit;

  if not (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) then
    wwdsDetail.DataSet.Edit;
  TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromFile(odImport.FileName);
  fl_report_changed := True;

  EvMessage('Report has been successfully imported from file');
end;

procedure TEDIT_SB_RWREPORTS.CompressReport;
var
  MS1, MS2: TMemoryStream;
begin
  MS1 := TMemoryStream.Create;
  MS2 := TMemoryStream.Create;
  try
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).SaveToStream(MS1);
    MS1.Position := 0;
    DeflateStream(MS1, MS2);
    MS2.Position := 0;
    TBlobField(wwdsDetail.DataSet.FieldByName('REPORT_FILE')).LoadFromStream(MS2);
  finally
    MS1.Free;
    MS2.Free;
  end;
end;

procedure TEDIT_SB_RWREPORTS.BeforePost(ADataSet: TDataSet);
begin
  if Assigned(FPrevBeforePost) then
    FPrevBeforePost(ADataSet);

  if fl_report_changed then
    CompressReport;
end;


function TEDIT_SB_RWREPORTS.GetIfReadOnly: Boolean;
begin
  Result := (SecurityState = ctReadOnlyWithExport) or inherited GetIfReadOnly;
end;

procedure TEDIT_SB_RWREPORTS.OnGetSecurityStates(
  const States: TSecurityStateList);
begin
  States.InsertAfter(ctEnabled, ctReadOnlyWithExport, 'Read only with export');
end;

procedure TEDIT_SB_RWREPORTS.miReportsCollectionExport(Sender: TObject);
var
  iRepCollection : IEvReportCollectionHelper;
begin
  iRepCollection := TEvRepCollectionHelper.Create();
// Service Bureau pass 0 as client number
  iRepCollection.SaveReportCollection(grReports, CH_DATABASE_SERVICE_BUREAU, 0, False);
end;

procedure TEDIT_SB_RWREPORTS.miReportsCollectionImport(Sender: TObject);
var
  iRepCollection : IEvReportCollectionHelper;
begin
  fImportCollectionDeltaNames := TStringList.Create;
  try
    iRepCollection := SSBReportCollection.TEvRepCollectionHelper.Create();
// Service Bureau pass 0 as client number
    iRepCollection.LoadCollection(grReports, CH_DATABASE_SERVICE_BUREAU, 0, wwdsDetail.Dataset,LoadNewReportItem);
  finally
    fImportCollectionDeltaNames.Free;
  end;
end;

function TEDIT_SB_RWREPORTS.ReportNameOrderHead(ProposedName:string):string;
var
  s: string;
  Q: iEvQuery;
  sFixedProposedName:string;
  jCopy: integer;
  aList:TstringList;
  sCheck: string;
begin
    sFixedProposedName := AnsiReplaceStr(ProposedName,'''','''''');
    if TEvRepCollectionHelper.LineHasCopyHeader(sFixedProposedName) then
      sFixedProposedName := System.Copy(sFixedProposedName, Length('Copy ZZ - ')+1, Length(sFixedProposedName));


    s := 'SELECT t1.REPORT_DESCRIPTION, t1.SB_REPORT_WRITER_REPORTS_NBR FROM Sb_Report_Writer_Reports t1 '
+ ' where t1.REPORT_DESCRIPTION ='''+ sFixedProposedName
+ ''' or t1.REPORT_DESCRIPTION like ''Copy __ - '
+ AnsiLeftStr(sFixedProposedName,40 - Length('Copy ZZ - ') - 1)
+ ''''
+ ' or t1.REPORT_DESCRIPTION like '''
+ TEvRepCollectionHelper.DoubleApostropheSQLStrLeft('Copy __ - '+sFixedProposedName, 40)
+ ''''
+ ' order by t1.SB_REPORT_WRITER_REPORTS_NBR';
// it is supposed to be absolute search, not relying on current situation
//        + 'where {AsOfNow<t1>}'
         ;
   aList := TStringList.Create;
   try
      // accept all names that were altered for imported collection already
      // they will be invisible for direct SQL query yet - transaction not confirmed
      // but we should check against them, too
      aList.Text := TEvRepCollectionHelper.ListOfRelatedNamesfromDelta(
                                                  FImportCollectionDeltaNames
                                                 ,sFixedProposedName
                                                 ,40
                                                 );

      Q := TevQuery.Create(s);
      Q.Execute;
      Q.Result.First;
      while NOT Q.Result.EOF do
      begin
        aList.Add(Q.Result.Fields[0].AsString);
        Q.Result.Next;
      end;
      jCopy := aList.Count;

// final check in case numbers are not in order or missing
      sCheck := TEvRepCollectionHelper.CopyNameVariationEx(ProposedName,jCopy, 40);
      while
            (aList.IndexOf( sCheck ) >=0)
            or
            ( FImportCollectionDeltaNames.IndexOf(sCheck) >= 0)
      do
      begin
           jCopy := jCopy + 1;
           sCheck := TEvRepCollectionHelper.CopyNameVariationEx(ProposedName,jCopy, 40);
      end;
      Result := sCheck;

   finally
     aList.Free;
   end;

end;


procedure TEDIT_SB_RWREPORTS.LoadNewReportItem( Sender: TObject;
                                            var ImportedReportType: string;
                                                jImportedClienNumber:integer;
                                                ImportedNbr:integer;
                                            var ReportName:string;
                                            var AssignedNewNbr:integer
                                              );
var
  bOldRequired: boolean;
  g: TGuid;
begin
  CreateGuid(g);
  bOldRequired := wwdsDetail.Dataset.FieldByName('REPORT_DESCRIPTION').Required;
  try
    wwdsDetail.Dataset.FieldByName('REPORT_DESCRIPTION').Required := False;
    wwdsDetail.Dataset.Insert;
    // Report description generally supposed to be unique
    wwdsDetail.Dataset.FieldByName('REPORT_DESCRIPTION').AsString := GuidToString(g);
    wwdsDetail.Dataset.FieldByName('REPORT_TYPE').AsString := ImportedReportType;
    // generates new NBR by table's generator in SA and negative pseudo-index in Evolution
    wwdsDetail.Dataset.Post;
    wwdsDetail.Dataset.Edit;
    AssignedNewNbr := wwdsDetail.Dataset.Fields[0].AsInteger; // retrieved inew autoinc value

    // we need to detect presence of the same name - checked within call
    ReportName :=  ReportNameOrderHead( ReportName );

    wwdsDetail.Dataset.FieldByName('REPORT_DESCRIPTION').AsString := ReportName;

    // populate list of delta-altered names
    fImportCollectionDeltaNames.Add(ReportName);
  finally
    wwdsDetail.Dataset.FieldByName('REPORT_DESCRIPTION').Required := bOldRequired;
  end;
end;

initialization
  RegisterClass(TEDIT_SB_RWREPORTS);

end.
