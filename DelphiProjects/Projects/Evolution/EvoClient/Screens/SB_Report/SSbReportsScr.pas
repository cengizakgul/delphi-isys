// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SSbReportsScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls, Graphics,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList,
  ISBasicClasses, EvMainboard, EvCommonInterfaces, EvUIComponents,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

type
  TSbReportsFrm = class(TFramePackageTmpl, IevSBReportsScreens)
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;
  end;

implementation

{$R *.DFM}

uses SPD_EDIT_SB_RWREPORTS, SPD_EDIT_SB_MULTICLIENT_REPORTS;

var
  SBReportsFrm: TSbReportsFrm;

function GetSBReportsScreens: IevSBReportsScreens;
begin
  if not Assigned(SBReportsFrm) then
    SBReportsFrm := TSbReportsFrm.Create(nil);
  Result := SBReportsFrm;
end;



function TSbReportsFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TSbReportsFrm.PackageCaption: string;
begin
  Result := 'S/B Rep&ort';
end;

function TSbReportsFrm.PackageSortPosition: string;
begin
  Result := '009';
end;

procedure TSbReportsFrm.UserPackageStructure;
begin
  AddStructure('RW Reports|200', 'TEDIT_SB_RWREPORTS');
  AddStructure('Multiclient Reports (Old)|300', 'TSB_MULTICLIENT_REPORT');
  AddStructure('Multiclient Reports (New)|400', 'TEDIT_SB_MULTICLIENT_REPORTS');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetSBReportsScreens, IevSBReportsScreens, 'Screens SB Reports');

finalization
  SbReportsFrm.Free;

end.
