// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_SB_MULTICLIENT_REPORT;

interface

uses
  SFrameEntry,  StdCtrls, Grids, Wwdbigrd, Wwdbgrid, Controls,
  ExtCtrls, ComCtrls, Classes, Db, Wwdatsrc, SDataStructure, EvContext,
   EvConsts, SysUtils, DBCtrls, EvTypes, Buttons, Forms, Variants,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, SDDClasses,
  ISDataAccessComponents, EvDataAccessComponents, SDataDicttemp,
  SDataDictsystem, SDataDictbureau, EvUIComponents, EvClientDataSet;

type
  TSB_MULTICLIENT_REPORT = class(TFrameEntry)
    Panel1: TevPanel;
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    Panel3: TevPanel;
    Panel4: TevPanel;
    wwdsClient: TevDataSource;
    wwgdClient: TevDBGrid;
    wwdsReport: TevDataSource;
    Splitter1: TevSplitter;
    chbPreview: TevCheckBox;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    dsMain: TevClientDataSet;
    dsMaintype: TStringField;
    dsMainnbr: TIntegerField;
    dsMainreport_description: TStringField;
    DM_TEMPORARY: TDM_TEMPORARY;
    CoReports: TevClientDataSet;
    CoReportsCL_NBR: TIntegerField;
    CoReportsCO_NBR: TIntegerField;
    CoReportsSB_REPORTS_NBR: TIntegerField;
    CoReportsTYPE: TStringField;
    evPanel1: TevPanel;
    wwDBGrid1: TevDBGrid;
    EvDBMemo1: TEvDBMemo;
    dsMainnotes: TBlobField;
    Button2: TevBitBtn;
    btnFilter: TevBitBtn;
    btnUnFilter: TevBitBtn;
    Button3: TevBitBtn;
    chbQueue: TevCheckBox;
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnUnFilterClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure CoReportsCalcFields(DataSet: TDataSet);
  private
    bFiltered: Boolean;
    procedure RefreshCo;
    procedure FilterList(DataSet: TDataSet; var Accept: Boolean);
    procedure FillMainDataSet(ADataSet: TevClientDataSet; AType: Char);
  public
    procedure Activate; override;
    procedure Deactivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  SB_MULTICLIENT_REPORT: TSB_MULTICLIENT_REPORT;

implementation

uses EvUtils, SReportSettings;

{$R *.DFM}

procedure TSB_MULTICLIENT_REPORT.Button3Click(Sender: TObject);
var
  i, j, n: Integer;
  Params: TrwReportParams;
  BM: TBookMark;
  prv: TrwReportDestination;
  ClPar: TrwReportParam;
  CoPar: TrwReportParam;
  aCl, aCo: Array of Integer;
  mr: TModalResult;
begin
  if chbPreview.Checked then
    prv := rdtPreview
  else
    prv := rdtPrinter;

  wwgdClient.DataSource.DataSet.DisableControls;
  ctx_StartWait('Preparation to run report');
  try
    with wwdsClient.DataSet do
    begin
      SetLength(aCl, wwgdClient.SelectedList.Count);
      SetLength(aCo, wwgdClient.SelectedList.Count);

      j := 0;
      First;
      while not Eof and (j <= wwgdClient.SelectedList.Count - 1) do
      begin
        BM := GetBookmark;
        for i := 0 to wwgdClient.SelectedList.Count - 1 do
          if CompareBookmarks(BM, wwgdClient.SelectedList.Items[i]) = 0 then
          begin
            aCl[j] := DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsInteger;
            aCo[j] := DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger;
            Inc(j);
            break;
          end;
        FreeBookmark(BM);
        Next;
      end;
    end;

    //sort
    for i := Low(aCl) to High(aCl) - 1 do
      for j := i + 1 to High(aCl) do
        if aCl[i] > aCl[j] then
        begin
          n := aCl[i];
          aCl[i] := aCl[j];
          aCl[j] := n;

          n := aCo[i];
          aCo[i] := aCo[j];
          aCo[j] := n;
        end;

    finally
      wwgdClient.DataSource.DataSet.EnableControls;
      ctx_EndWait;
    end;

    Params := TrwReportParams.Create;
    try
      ClPar := Params.Add('Clients', varArrayCreate([0, wwgdClient.SelectedList.Count - 1], varVariant));
      CoPar := Params.Add('Companies', varArrayCreate([0, wwgdClient.SelectedList.Count - 1], varVariant));

      for i := Low(aCl) to High(aCl) do
      begin
        ClPar.SetArrItemValue(i, aCl[i]);
        CoPar.SetArrItemValue(i, aCo[i]);
      end;

      Params.Add('SetupMode', False);
      Params.Add('ReportName', wwdsReport.DataSet.FieldByName('report_description').AsString);

      mr := ctx_RWLocalEngine.ShowModalInputForm(
        wwdsReport.DataSet.FieldByName('nbr').AsInteger,
        wwdsReport.DataSet.FieldByName('type').AsString,
        Params, True, False);

      if mr in [mrOk, mrNone] then
      begin
        ctx_RWLocalEngine.StartGroup;
        try
          ctx_RWLocalEngine.CalcPrintReport(wwdsReport.DataSet.FieldByName('nbr').AsInteger,
            wwdsReport.DataSet.FieldByName('type').AsString, Params,
            wwdsReport.DataSet.FieldByName('report_description').AsString);
        finally
          if chbQueue.Checked then
            ctx_RWLocalEngine.EndGroupForTask(prv)
          else
            ctx_RWLocalEngine.EndGroup(prv);
        end;
      end;

    finally
      Params.Free;
    end;
end;

procedure TSB_MULTICLIENT_REPORT.Activate;
begin
  ctx_RWLocalEngine.BeginWork;
  dsMain.CreateDataSet;
  dsMain.Open;

  ctx_DataAccess.SY_CUSTOM_VIEW.Close;
  ctx_DataAccess.SB_CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
    SetMacro('Columns', 'SY_REPORT_WRITER_REPORTS_NBR, report_description, notes');
    SetMacro('TableName', 'SY_REPORT_WRITER_REPORTS');
    SetMacro('Condition', 'report_type = ''' + SYSTEM_REPORT_TYPE_MULTICLIENT + '''');
    ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.SY_CUSTOM_VIEW.Open;
  end;
  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
    SetMacro('Columns', 'SB_REPORT_WRITER_REPORTS_NBR, report_description, notes');
    SetMacro('TableName', 'SB_REPORT_WRITER_REPORTS');
    SetMacro('Condition', 'report_type = ''' + SYSTEM_REPORT_TYPE_MULTICLIENT + '''');
    ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.SB_CUSTOM_VIEW.Open;
  end;

  FillMainDataSet(ctx_DataAccess.SB_CUSTOM_VIEW, CH_DATABASE_SERVICE_BUREAU);
  FillMainDataSet(ctx_DataAccess.SY_CUSTOM_VIEW, CH_DATABASE_SYSTEM);

  ctx_DataAccess.SY_CUSTOM_VIEW.Close;
  ctx_DataAccess.SB_CUSTOM_VIEW.Close;

  inherited;
  DM_TEMPORARY.TMP_CO.OnFilterRecord := FilterList;
end;

procedure TSB_MULTICLIENT_REPORT.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;



  AddDSWithCheck([DM_TEMPORARY.TMP_CO {, ctx_DataAccess.SY_CUSTOM_VIEW, ctx_DataAccess.SB_CUSTOM_VIEW }{, DM_SERVICE_BUREAU.SB_REPORTS}], SupportDataSets, '');
end;

procedure TSB_MULTICLIENT_REPORT.Button2Click(Sender: TObject);
begin
  inherited;
  wwgdClient.SelectAll;
end;

procedure TSB_MULTICLIENT_REPORT.FillMainDataSet(ADataSet: TevClientDataSet; AType: Char);
begin
  ADataSet.First;
  while not ADataSet.Eof do
  begin
    dsMain.Append;
    dsMain.FieldByName('type').Value := AType;
    dsMain.FieldByName('notes').Assign(ADataSet.FieldByName('notes'));
    if AType = CH_DATABASE_SYSTEM then
      dsMain.FieldByName('nbr').Value := ADataSet.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').AsInteger
    else
      dsMain.FieldByName('nbr').Value := ADataSet.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').AsInteger;
    dsMain.FieldByName('report_description').Value := ADataSet.FieldByName('report_description').AsString +
                  ' (' + AType + dsMain.FieldByName('nbr').AsString + ')';
    dsMain.Post;
    ADataSet.Next;
  end;
end;

procedure TSB_MULTICLIENT_REPORT.FilterList(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := not bFiltered or not CoReports.Active or CoReports.FindKey([DataSet['CL_NBR'], DataSet['CO_NBR'], dsMain['NBR'], dsMain['Type']]);
end;

procedure TSB_MULTICLIENT_REPORT.Deactivate;
begin
  ctx_RWLocalEngine.EndWork;
  DM_TEMPORARY.TMP_CO.OnFilterRecord := nil;
  inherited;
end;

procedure TSB_MULTICLIENT_REPORT.btnUnFilterClick(Sender: TObject);
begin
  inherited;
  bFiltered := False;
  if DM_TEMPORARY.TMP_CO.Filter = '' then
    DM_TEMPORARY.TMP_CO.Filtered := False
  else if DM_TEMPORARY.TMP_CO.Filtered then
    RefreshCo;
end;

procedure TSB_MULTICLIENT_REPORT.btnFilterClick(Sender: TObject);
begin
  inherited;
  ctx_StartWait;
  try
    if not CoReports.Active then
      with TExecDSWrapper.Create('GenericSelect'), ctx_DataAccess.TEMP_CUSTOM_VIEW do
      begin
        SetMacro('Columns', '*');
        SetMacro('TableName', 'TMP_CO_REPORTS');
        Close;
        DataRequest(AsVariant);
        Open;
        CoReports.Data := Data
      end;

    bFiltered := True;
    if DM_TEMPORARY.TMP_CO.Filtered then
      RefreshCo
    else
      DM_TEMPORARY.TMP_CO.Filtered := True;
  finally
    ctx_EndWait;
  end;
end;

procedure TSB_MULTICLIENT_REPORT.RefreshCo;
begin
  DM_TEMPORARY.TMP_CO.DisableControls;
  try
    DM_TEMPORARY.TMP_CO.Filtered := False;
    DM_TEMPORARY.TMP_CO.Filtered := True;
  finally
    DM_TEMPORARY.TMP_CO.EnableControls;
  end;
end;

procedure TSB_MULTICLIENT_REPORT.CoReportsCalcFields(DataSet: TDataSet);
begin
  inherited;
{  DM_SERVICE_BUREAU.SB_REPORTS.Locate('SB_REPORTS_NBR', DataSet['SB_REPORTS_NBR'], []);
  if not DM_SERVICE_BUREAU.SB_REPORTS.FieldByName('SB_REPORT_WRITER_REPORTS_NBR').IsNull then
  begin
    DataSet['NBR'] := DM_SERVICE_BUREAU.SB_REPORTS['SB_REPORT_WRITER_REPORTS_NBR'];
    DataSet['Type'] := CH_DATABASE_SERVICE_BUREAU;
  end
  else
  begin
    DataSet['NBR'] := DM_SERVICE_BUREAU.SB_REPORTS['SY_REPORTS_NBR'];
    DataSet['Type'] := CH_DATABASE_SYSTEM;
  end;}
end;

initialization
  RegisterClass(TSB_MULTICLIENT_REPORT);

end.
