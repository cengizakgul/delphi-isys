inherited SB_MULTICLIENT_REPORT: TSB_MULTICLIENT_REPORT
  object Panel1: TevPanel [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object PageControl1: TevPageControl
      Left = 1
      Top = 1
      Width = 433
      Height = 264
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Select Client/Company'
        object Panel3: TevPanel
          Left = 0
          Top = 36
          Width = 425
          Height = 200
          Align = alClient
          TabOrder = 0
          object Splitter1: TevSplitter
            Left = 120
            Top = 1
            Width = 4
            Height = 198
            Align = alRight
          end
          object wwgdClient: TevDBGrid
            Left = 1
            Top = 1
            Width = 119
            Height = 198
            DisableThemesInTitle = False
            Selected.Strings = (
              'CUSTOM_COMPANY_NUMBER'#9'10'#9'Number'#9'F'
              'NAME'#9'40'#9'Name'#9'F'
              'TERMINATION_CODE_DESC'#9'15'#9'Status'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TSB_MULTICLIENT_REPORT\wwgdClient'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = wwdsClient
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
          end
          object evPanel1: TevPanel
            Left = 124
            Top = 1
            Width = 300
            Height = 198
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object wwDBGrid1: TevDBGrid
              Left = 0
              Top = 0
              Width = 300
              Height = 109
              DisableThemesInTitle = False
              Selected.Strings = (
                'REPORT_DESCRIPTION'#9'40'#9'Report title'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TSB_MULTICLIENT_REPORT\wwDBGrid1'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = wwdsReport
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              UseTFields = True
              PaintOptions.AlternatingRowColor = clCream
            end
            object EvDBMemo1: TEvDBMemo
              Left = 0
              Top = 109
              Width = 300
              Height = 89
              Align = alBottom
              DataField = 'notes'
              DataSource = wwdsReport
              ReadOnly = True
              TabOrder = 1
            end
          end
        end
        object Panel4: TevPanel
          Left = 0
          Top = 0
          Width = 425
          Height = 36
          Align = alTop
          TabOrder = 1
          object chbPreview: TevCheckBox
            Left = 433
            Top = 2
            Width = 94
            Height = 17
            Caption = 'Show Preview'
            TabOrder = 0
          end
          object Button2: TevBitBtn
            Left = 8
            Top = 6
            Width = 137
            Height = 25
            Caption = 'Select All Companies'
            TabOrder = 1
            OnClick = Button2Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD0000
              008DDDDDDDDD888888FD88888888000000888888888D888888FD666666680000
              0086888DDDDF888888FD666EEE80FFFF7708888888F8DDDDDD8F6666660FFFFF
              F708888DDD8DDDDDDD8F666EEE07FFFFF70888888D8DDDDDDD8F66666680FFFF
              7708888DDDD8DDDDDD8F666EEEE0FF770086888888D8DDDD88FD66666660F000
              866D888888D8D888FDDDDDDDDDD0F08D68DDDDDDDDD8D8FDDDDDD0000DD0F08D
              68DDD8888DD8D8FD8DDDD0D80DD0F08D68DDD8D88DD8D8FD8DDDD08D0DD0F088
              68DDD88D8DD8D8FD8DDDD0000DD806EE68DDD8888DDD8F888DDDDDDDDDDDDEEE
              68DDDDDDDDDDD8888DDDDDDDDDDDDDDE6DDDDDDDDDDDDDD88DDD}
            NumGlyphs = 2
          end
          object btnFilter: TevBitBtn
            Left = 152
            Top = 6
            Width = 121
            Height = 25
            Caption = 'Filter For Report'
            TabOrder = 2
            OnClick = btnFilterClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D00000004444
              4444D8888888F8888888D000000088777774DD888888FDDDDDD8DD0000700087
              7774DD888D888FFDDDD8DD007FF787044444DDD8D888888F8888DDD0FFFFF708
              7774DDDD8888888FDDD8DDD0FFFFFF077774DDDD8888888DDDD8DDD0FFFFF044
              4444DDDD888888D88888D2DD0FF708777774DDDDD888DFDDDDD8DA2D40FF0877
              7774D8DDDD888FDDDDDDDDAD220FF0222222DD8D88D888F8888822DD2AA0FF0A
              AAA2DDDD8DDD888DDDD8AADD2AAA000AAAA288DD8DDDDDDDDDD8DD2D22222222
              2222DDDD888888888888D2AD477777777774DD8DDDDDDDDDDDDDDADD47777777
              7774D8DD8DDDDDDDDDD8DDDD444444444444DDDD888888888888}
            NumGlyphs = 2
          end
          object btnUnFilter: TevBitBtn
            Left = 280
            Top = 6
            Width = 121
            Height = 25
            Caption = 'Clear Report Filter'
            TabOrder = 3
            OnClick = btnUnFilterClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88D44448844
              4444DDDD8888888888888008477800877774DFFD8DDDFFDDDDD88FF08780FF87
              7774D88F8DDF88DDDDD88FFF080FFF844444D888FDF888D88888D8FFF0FFF877
              7774DD888F888DDDDDD8DD8FFFFF87777774DDD88888DDDDDDD8DD80FFF08444
              4444DDDF888FD8888888D80FFFFF08777774DDF88888FDDDDDD880FFF8FFF087
              7774DF888D888FDDDDDD8FFF828FFF822222D888D8D888D888888FF82AA2FF2A
              AAA2D88D8DDD88DDDDD8D88D2AAA22AAAAA2DDDD8DDDDDDDDDD8DDDD22222222
              2222DDDD888888888888DDDD477777777774DDDDDDDDDDDDDDDDDDDD47777777
              7774DDDD8DDDDDDDDDD8DDDD444444444444DDDD888888888888}
            NumGlyphs = 2
          end
          object Button3: TevBitBtn
            Left = 578
            Top = 6
            Width = 97
            Height = 25
            Caption = 'Run Report'
            TabOrder = 4
            OnClick = Button3Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD28DDDD
              DDDDDDDDDDFDDDDDDDDDDDDDD2A28DDDDDDDDDDDDF8FDDDDDDDDDDDD2AAA28DD
              DDDDDDDDF888FDDDDDDDDDD2AAAA28DDDDDDDDDF8888FDDDDDDDDD2AAAAAA200
              000DDDF888888F88888DD2AA882AA2877778DF88DDD88FDDDDD82A88888AAA28
              8778F8DD88D888F88DD8D8888778AA287778DD888DDD88FDDDD8D8888712AAA2
              8778D8888D8D888FDDD8D88887778AA28778D8888DDDD88FDDD8D88888888AAA
              2888D8888888D888F888D888877778AA2878D8888DDDDD88F8D8D88878FFF7A2
              878DD888D8DDDD8F8D8DD8870000000878DDD88D888888D8D8DDD87777777777
              8DDDD8DDDDDDDDDD8DDDDD8888888888DDDDDD8888888888DDDD}
            NumGlyphs = 2
          end
          object chbQueue: TevCheckBox
            Left = 433
            Top = 18
            Width = 136
            Height = 17
            Caption = 'Run through the Queue'
            Checked = True
            State = cbChecked
            TabOrder = 5
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 96
  end
  inherited wwdsDetail: TevDataSource
    Left = 520
    Top = 96
  end
  inherited wwdsList: TevDataSource
    Left = 82
    Top = 114
  end
  object wwdsClient: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    Left = 184
    Top = 112
  end
  object wwdsReport: TevDataSource
    AutoEdit = False
    DataSet = dsMain
    Left = 216
    Top = 96
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 288
    Top = 144
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 325
    Top = 97
  end
  object dsMain: TevClientDataSet
    Left = 244
    Top = 76
    object dsMaintype: TStringField
      FieldName = 'type'
      Visible = False
      Size = 1
    end
    object dsMainnbr: TIntegerField
      FieldName = 'nbr'
      Visible = False
    end
    object dsMainreport_description: TStringField
      DisplayWidth = 64
      FieldName = 'report_description'
      Size = 64
    end
    object dsMainnotes: TBlobField
      DisplayWidth = 10
      FieldName = 'notes'
    end
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 362
    Top = 81
  end
  object CoReports: TevClientDataSet
    IndexFieldNames = 'CL_NBR; CO_NBR; REPORT_WRITER_REPORTS_NBR; REPORT_LEVEL'
    OnCalcFields = CoReportsCalcFields
    Left = 407
    Top = 100
    object CoReportsCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object CoReportsCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object CoReportsSB_REPORTS_NBR: TIntegerField
      FieldName = 'REPORT_WRITER_REPORTS_NBR'
    end
    object CoReportsTYPE: TStringField
      FieldName = 'REPORT_LEVEL'
      Size = 1
    end
  end
end
