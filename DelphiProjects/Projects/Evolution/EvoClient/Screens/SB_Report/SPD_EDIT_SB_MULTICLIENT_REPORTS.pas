unit SPD_EDIT_SB_MULTICLIENT_REPORTS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SFrameEntry, DB, Wwdatsrc, ISBasicClasses, 
  ComCtrls, SDDClasses, SDataStructure, Grids, Wwdbigrd, EvContext,
  Wwdbgrid, StdCtrls, Mask, wwdbedit, kbmMemTable, ISKbmMemDataSet,
  EvUtils, EvConsts, Buttons, ExtCtrls, wwdblook, SReportSettings, DBCtrls,
  SSecurityInterface, EvTypes, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictbureau, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_MULTICLIENT_REPORTS = class(TFrameEntry)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    pcReports: TevPageControl;
    tsBrowse: TTabSheet;
    tsDetail: TTabSheet;
    Label1: TevLabel;
    wwDBEdit1: TevDBEdit;
    evLabel1: TevLabel;
    cbReport: TevDBLookupCombo;
    csReports: TevClientDataSet;
    btnInpForm: TBitBtn;
    memNote: TEvDBMemo;
    gbASCII: TevGroupBox;
    sbBrowse: TevSpeedButton;
    dbeFile: TevDBEdit;
    chbAddFile: TevCheckBox;
    chbDuplexing: TevCheckBox;
    Panel4: TevPanel;
    evPanel1: TevPanel;
    chbPreview: TevCheckBox;
    chbQueue: TevCheckBox;
    Button3: TevBitBtn;
    evPanel2: TevPanel;
    grReports: TevDBGrid;
    procedure cbReportChange(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure btnInpFormClick(Sender: TObject);
    procedure sbBrowseClick(Sender: TObject);
    procedure dbeFileChange(Sender: TObject);
    procedure chbAddFileClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure chbDuplexingClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    SY_DS: TevClientDataSet;
    SB_DS: TevClientDataSet;
    FInParams: TrwReportParams;
    procedure CreateReportSourceDataSet;
    procedure EditMode(AEnabled: Boolean);
    procedure SyncRunParams;
  protected
    function  GetDefaultDataSet: TevClientDataSet; override;
    procedure SetReadOnly(Value: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   Activate; override;
    procedure   DeActivate; override;
    procedure   ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TEDIT_SB_MULTICLIENT_REPORTS }

procedure TEDIT_SB_MULTICLIENT_REPORTS.Activate;
begin
  ctx_RWLocalEngine.BeginWork;
  SY_DS := nil;
  SB_DS := nil;
  CreateReportSourceDataSet;
  wwdsDetailDataChange(nil, nil);
  inherited;
end;


function TEDIT_SB_MULTICLIENT_REPORTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.cbReportChange(Sender: TObject);
begin
  EditMode(True);
  btnInpForm.Enabled := (cbReport.LookupValue <> '');

  if not gbASCII.Enabled and (dbeFile.Text <> '') then
    dbeFile.Text := '';

  if wwdsDetail.DataSet.State <> dsBrowse then
    DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.DESCRIPTION.AsString := csReports.FieldByName('cName').AsString;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.CreateReportSourceDataSet;

  procedure AddDBReports(ALevel: String);
  var
    KeyFld, h, h1: String;
    DS: TevClientDataSet;
  begin
    KeyFld := '_REPORT_WRITER_REPORTS_NBR';
    case ALevel[1] of
      CH_DATABASE_SYSTEM:  begin
              KeyFld := 'SY' + KeyFld;
              DS := SY_DS;
            end;

      CH_DATABASE_SERVICE_BUREAU:
            begin
              KeyFld := 'SB' + KeyFld;
              DS := SB_DS;
            end;
    else
      DS := nil;
    end;

    DS.First;
    while not DS.Eof do
    begin
      if ctx_RWLocalEngine.RWClassInheritsFrom(DS.FieldByName('Ancestor_Class_Name').AsString, 'TrwlCustomMultiClientReport') and (ALevel = CH_DATABASE_SERVICE_BUREAU) or
         (ALevel = CH_DATABASE_SYSTEM) and DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Locate('SY_REPORT_WRITER_REPORTS_NBR', DS.FieldByName(KeyFld).AsInteger, []) then
      begin
        h1 := DS.FieldByName('report_description').AsString;
        h := h1 + ' (' + ALevel[1] + DS.FieldByName(KeyFld).AsString + ')';
        csReports.Append;
        csReports.FieldByName('nbr').AsInteger := DS.FieldByName(KeyFld).AsInteger;
        csReports.FieldByName('level').AsString := ALevel;
        csReports.FieldByName('Key').AsString := DS.FieldByName(KeyFld).AsString + ALevel;
        csReports.FieldByName('name').AsString := h;
        csReports.FieldByName('cName').AsString := h1;
        csReports.FieldByName('notes').Value := DS.FieldByName('notes').Value;
        csReports.FieldByName('Type').AsString := DS.FieldByName('REPORT_TYPE').AsString[1];
        csReports.Post;
      end;

      DS.Next;
    end;
  end;

begin
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Open;
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filter := 'SY_REPORT_GROUPS_NBR = 6';
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filtered := False;
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filtered := True;

  if not Assigned(SY_DS) then
  begin
    SY_DS := TevClientDataSet.Create(nil);

    if ctx_DataAccess.SY_CUSTOM_VIEW.Active then
      ctx_DataAccess.SY_CUSTOM_VIEW.Close;

    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('Columns', 'SY_REPORT_WRITER_REPORTS_NBR, REPORT_DESCRIPTION, REPORT_TYPE, NOTES, Ancestor_Class_Name');
      SetMacro('TableName', 'SY_REPORT_WRITER_REPORTS');
      SetMacro('Condition', 'Ancestor_Class_Name <> ''''');
      ctx_DataAccess.SY_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.SY_CUSTOM_VIEW.Open;
      SY_DS.Data := ctx_DataAccess.SY_CUSTOM_VIEW.Data;
      ctx_DataAccess.SY_CUSTOM_VIEW.Close;
    end;
  end;

  if not Assigned(SB_DS) then
  begin
    SB_DS := TevClientDataSet.Create(nil);

    if ctx_DataAccess.SB_CUSTOM_VIEW.Active then
      ctx_DataAccess.SB_CUSTOM_VIEW.Close;

    with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
    begin
      SetMacro('Columns', 'SB_REPORT_WRITER_REPORTS_NBR, REPORT_DESCRIPTION, REPORT_TYPE, NOTES, Ancestor_Class_Name');
      SetMacro('TableName', 'SB_REPORT_WRITER_REPORTS');
      SetMacro('Condition', 'Ancestor_Class_Name <> ''''');
      ctx_DataAccess.SB_CUSTOM_VIEW.DataRequest(AsVariant);
      ctx_DataAccess.SB_CUSTOM_VIEW.Open;
      SB_DS.Data := ctx_DataAccess.SB_CUSTOM_VIEW.Data;
      ctx_DataAccess.SB_CUSTOM_VIEW.Close;
    end;
  end;

  csReports.DisableControls;
  csReports.Close;
  csReports.CreateDataSet;

  AddDBReports(CH_DATABASE_SYSTEM);
  AddDBReports(CH_DATABASE_SERVICE_BUREAU);

  csReports.EnableControls;

  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filter := '';
  DM_SYSTEM_MISC.SY_REPORT_GROUP_MEMBERS.Filtered := False;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.wwdsDetailDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;

  if (wwdsDetail.DataSet.State = dsBrowse) and csReports.Active then
  begin
    wwdsDetail.Tag := 1;
    try
      if csReports.Locate('key',
        DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_NBR.AsString +
        DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_LEVEL.AsString, []) then
      begin
        cbReport.LookupField := 'key';   // bugfix, don't touch
        cbReport.LookupValue := csReports.FieldByName('key').AsString;
        btnInpForm.Enabled := True;
      end

      else
      begin
        cbReport.LookupValue :='';
        memNote.Clear;
        btnInpForm.Enabled := False;
      end;

      if not DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.INPUT_PARAMS.IsNull then
        try
          FInParams.ReadFromBlobField(DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.INPUT_PARAMS);
        except
          FInParams.Clear;
        end
      else
        FInParams.Clear;

      SyncRunParams;  
    finally
      wwdsDetail.Tag := 0;
    end;
  end;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.EditMode(AEnabled: Boolean);
begin
  if wwdsDetail.Tag = 0 then
    if (wwdsDetail.DataSet.State = dsBrowse) and AEnabled then
      wwdsDetail.DataSet.Edit
    else if (wwdsDetail.DataSet.State in [dsEdit, dsInsert]) and not AEnabled then
      wwdsDetail.DataSet.Cancel;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.btnInpFormClick(Sender: TObject);
var
  Res: TModalResult;
begin
  try
    FInParams.Add('SetupMode', True);
    FInParams.Add('ReportName', wwdsDetail.DataSet.FieldByName('DESCRIPTION').AsString);

    Res := ctx_RWLocalEngine.ShowModalInputForm(csReports.FieldByName('NBR').AsInteger,
      csReports.FieldByName('LEVEL').AsString, FInParams, False);
  finally
    if not DM_COMPANY.CO.Active then
      DM_COMPANY.CO.Active := True;
  end;

  if not GetIfReadOnly then
  begin
    if (Res = mrOk) and (wwdsDetail.DataSet.State = dsBrowse) then
      wwdsDetail.DataSet.Edit;
  end
  else
    if not DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.INPUT_PARAMS.IsNull then
      try
        FInParams.ReadFromBlobField(DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.INPUT_PARAMS);
      except
        FInParams.Clear;
      end
    else
      FInParams.Clear;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.SetReadOnly(Value: Boolean);
begin
  inherited;
  cbReport.ReadOnly := cbReport.ReadOnly or (SecurityState <> stEnabled);
end;

constructor TEDIT_SB_MULTICLIENT_REPORTS.Create(AOwner: TComponent);
begin
  inherited;
  FInParams := TrwReportParams.Create;
end;

destructor TEDIT_SB_MULTICLIENT_REPORTS.Destroy;
begin
  FreeAndNil(FInParams);
  inherited;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.DeActivate;
begin
  ctx_RWLocalEngine.EndWork;
  FreeAndNil(SY_DS);
  FreeAndNil(SB_DS);
  inherited;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  if Kind = 10 then
  begin
    FInParams.Duplexing := chbDuplexing.Checked;
    FInParams.FileName := dbeFile.Text;

    if cbReport.LookupValue <> '' then
    begin
      DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_NBR.AsInteger := csReports.FieldByName('NBR').AsInteger;
      DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_LEVEL.AsString := csReports.FieldByName('LEVEL').AsString;
    end
    else
    begin
      DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_NBR.Clear;
      DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_LEVEL.Clear;
    end;

    FInParams.SaveToBlobField(DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.INPUT_PARAMS);
  end;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.sbBrowseClick(Sender: TObject);
var
  TempStr: String;
begin
  inherited;
  TempStr := RunFolderDialog('Please, select an output path:', dbeFile.Text);
  dbeFile.Text := TempStr;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.dbeFileChange(Sender: TObject);
var
   h: String;
begin
  EditMode(True);

  h := Trim(dbeFile.Text);

  if Length(h) = 0 then
    chbAddFile.Checked := False
  else
    chbAddFile.Checked := (h[Length(h)] = '+');
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.chbAddFileClick(Sender: TObject);
var
  h: string;
begin
  EditMode(True);
  
  h := Trim(dbeFile.Text);
  if h = '' then
  begin
    chbAddFile.Checked := False;
    Exit;
  end;

  if Length(h) = 0 then
    Exit;

  if h[Length(h)] = '+' then
    Delete(h, Length(h), 1);

  if chbAddFile.Checked then
    h := h + '+';

  dbeFile.Text := h;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.SyncRunParams;
begin
  chbDuplexing.Checked := FInParams.Duplexing;
  dbeFile.Text := FInParams.FileName;
  chbAddFile.Checked := FInParams.AddToExistFile;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;

  if wwdsDetail.DataSet.State = dsInsert then
  begin
    SyncRunParams;
    cbReport.LookupValue :='';
    memNote.Clear;
    btnInpForm.Enabled := False;
  end;
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.chbDuplexingClick(Sender: TObject);
begin
  EditMode(True);
end;

procedure TEDIT_SB_MULTICLIENT_REPORTS.Button3Click(Sender: TObject);
var
  Params: TrwReportParams;
  prv: TrwReportDestination;
  mr: TModalResult;
  rn: String;
begin
  if chbPreview.Checked then
    prv := rdtPreview
  else
    prv := rdtPrinter;

  Params := TrwReportParams.Create;
  try
    rn := DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.DESCRIPTION.AsString +
      ' (' + DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_LEVEL.AsString +
      DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_NBR.AsString + ')';
      

    Params.Assign(FInParams);
    Params.Add('SetupMode', False);
    Params.Add('ReportName', rn);

    mr := ctx_RWLocalEngine.ShowModalInputForm(
      DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_NBR.AsInteger,
      DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_LEVEL.AsString,
      Params, True, False);

    if mr in [mrOk, mrNone] then
    begin
      ctx_RWLocalEngine.StartGroup;
      try
        ctx_RWLocalEngine.CalcPrintReport(
          DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_NBR.AsInteger,
          DM_SERVICE_BUREAU.SB_MULTICLIENT_REPORTS.REPORT_LEVEL.AsString,
          Params, rn);
      finally
        if chbQueue.Checked then
          ctx_RWLocalEngine.EndGroupForTask(prv)
        else
          ctx_RWLocalEngine.EndGroup(prv);
      end;
    end;

  finally
    FreeAndNil(Params);
  end;
end;

initialization
  RegisterClass(TEDIT_SB_MULTICLIENT_REPORTS);

end.
