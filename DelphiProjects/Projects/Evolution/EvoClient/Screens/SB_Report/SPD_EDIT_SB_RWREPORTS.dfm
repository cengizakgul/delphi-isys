inherited EDIT_SB_RWREPORTS: TEDIT_SB_RWREPORTS
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Reports'
      object grReports: TevDBGrid
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        DisableThemesInTitle = False
        ControlType.Strings = (
          'REPORT_TYPE;CustomEdit;evDBComboBox1')
        Selected.Strings = (
          'REPORT_DESCRIPTION'#9'40'#9'Name'#9'F'
          'REPORT_TYPE'#9'13'#9'Type'#9'F'
          'RWDescription'#9'40'#9'Description'#9'F'
          'SB_REPORT_WRITER_REPORTS_NBR'#9'10'#9'Nbr'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_RWREPORTS\grReports'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        PopupMenu = PopupMenu1
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = grReportsDblClick
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object evDBComboBox1: TevDBComboBox
        Left = 216
        Top = 96
        Width = 177
        Height = 21
        HelpContext = 10514
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'REPORT_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Normal'#9'N'
          'Multiclient'#9'M'
          'Tax Return'#9'R'
          'Tax Return Multiclient'#9'L'
          'Check'#9'C'
          'ASCII File'#9'A'
          'SB Global'#9'B'
          'Tax Coupon'#9'T'
          'HR'#9'H'
          'Mail Merge'#9'E')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Detail'
      ImageIndex = 1
      object Label1: TevLabel
        Left = 6
        Top = 19
        Width = 65
        Height = 13
        Caption = 'Description'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evLabel1: TevLabel
        Left = 6
        Top = 46
        Width = 29
        Height = 13
        Caption = 'Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evLabel2: TevLabel
        Left = 6
        Top = 75
        Width = 34
        Height = 13
        Caption = 'Notes'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evLabel3: TevLabel
        Left = 278
        Top = 46
        Width = 67
        Height = 13
        Caption = 'Media Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object wwDBEdit1: TevDBEdit
        Left = 80
        Top = 16
        Width = 449
        Height = 21
        DataField = 'REPORT_DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwcbReport_Type: TevDBComboBox
        Left = 80
        Top = 44
        Width = 177
        Height = 21
        HelpContext = 10514
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'REPORT_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Items.Strings = (
          'Normal'#9'N'
          'Multiclient'#9'M'
          'Tax Return'#9'R'
          'Tax Return Multiclient'#9'L'
          'Check'#9'C'
          'ASCII File'#9'A'
          'SB Global'#9'B'
          'Tax Coupon'#9'T'
          'HR'#9'H'
          'Mail Merge'#9'E')
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object EvDBMemo1: TEvDBMemo
        Left = 6
        Top = 91
        Width = 672
        Height = 230
        DataField = 'NOTES'
        DataSource = wwdsDetail
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 4
      end
      object btnRWDesigner: TevBitBtn
        Left = 536
        Top = 14
        Width = 145
        Height = 25
        Caption = 'Report Writer Designer'
        TabOrder = 1
        OnClick = btnRWDesignerClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
          8888DFFFFFFFFFFFFFFF4444444444444448888F8F8F8F8F8F8F24FF4F4F4F4F
          4F48D88888888888888F224FFF7F7F7F7F488D8888888888888F2A24FFF44444
          FF488DD8888FDDDD888F2AA24FFF4874FF488DDD8888FDDD888F2AA204FFF484
          FF488DD8D8888FDD888F2AA2074FFF44FF488DD88D8888FD888F2AA20004FFF4
          FF488DD88DD8888F888F2A3207F74FFFFF488D888DDD8888888F33B3000004FF
          FF4888D88D88D888888F3BB30FFFF74FFF488DD88DDDDD88888FD3B07FF00074
          FF48D8D8DDD888D8888FD3207FFFFFF04F48D888DDDDDDDD888FD0A200000000
          D448DD8D88888888D88FDDA2DDDDDDDDDD4DDD8DDDDDDDDDDD8D}
        NumGlyphs = 2
        Margin = 0
      end
      object evDBComboBox2: TevDBComboBox
        Left = 352
        Top = 44
        Width = 177
        Height = 21
        HelpContext = 10514
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'MEDIA_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object btnExportReport: TevBitBtn
        Left = 6
        Top = 331
        Width = 145
        Height = 25
        Caption = 'Export Report To File'
        TabOrder = 5
        OnClick = btnExportReportClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDD88888888
          8888DDDDDDDDDDDDDDDDDDD8487487774448DDDD88D88DDD888DDDD448748777
          4448DDD888D88DDD888DDDD4487487774448DDD888D88DDD888DDDD448777777
          4448DDD888DDDDDD888DDDD4444444444448DDD888888888888DDDD442444444
          4448DDD88F888888888D88847A2777777748DDD8D8FDDDDDDD8D22222AA277FF
          FF48FFFFF88FDDDDDD8DAAAAAAAA277FFF4888888888FDDDDD8DAAAAAAAAA277
          FF48888888888FDDDD8DAAAAAAAAAA7FFF488888888888DDDD8DAAAAAAAAA7FF
          FF48888888888DDDDD8DAAAAAAAA8888884D88888888D888888DDDDDDAADDDDD
          DDDDDDDDD88DDDDDDDDDDDDDDADDDDDDDDDDDDDDD8DDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object btnImportReport: TevBitBtn
        Left = 162
        Top = 331
        Width = 145
        Height = 25
        Caption = 'Import Report From File'
        TabOrder = 6
        OnClick = Count
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDD8
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDD838DDDDDDDDDDDDDDFDDDDDDDDDDDDD83B
          38DDDDDDDDDDDDF8FDDDD888888883BBB38DD8888888DF888FDD848747773BBB
          BB38888D8DDDF88888FD44874773BBBBBBB3888D8DDF8888888F4487777744BB
          B3DD888DDDDDDD888FDD4444444444BBB3DD888888888D888FDD4444444444BB
          B3DD888888888D888FDD4777777777BBB3DD8DDDDDDDDD888FDD47FFF73333BB
          B3DD8DDDDDFFFF888FDD47FFF7BBBBBBB3DD8DDDDD8888888FDD47FFF7BBBBBB
          B3DD8DDDDD8888888FDD47FFF7BBBBBB3DDD8DDDDD888888FDDD47FFFFFFFF48
          DDDD8DDDDDDDDDDDDDDD888888888888DDDD88888888888DDDDD}
        NumGlyphs = 2
        Margin = 0
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_REPORT_WRITER_REPORTS.SB_REPORT_WRITER_REPORTS
    OnStateChange = wwdsDetailStateChange
    MasterDataSource = nil
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 316
    Top = 32
  end
  object PopupMenu1: TPopupMenu
    Left = 196
    Top = 208
    object miCompile: TMenuItem
      Caption = 'Compile Selection'
      OnClick = miCompileClick
    end
    object miConversion: TMenuItem
      Caption = 'Correct Selected Reports'
      OnClick = miConversionClick
    end
    object ExportReportsToFile1: TMenuItem
      Caption = 'Export Selected Reports To File'
      OnClick = miReportsCollectionExport
    end
    object ImportReportsfromCollectionFile1: TMenuItem
      Caption = 'Import Reports from Collection File'
      OnClick = miReportsCollectionImport
    end
  end
  object sdExport: TSaveDialog
    Filter = 'RW Reports (*.rwr)|*.rwr|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Export Report To File'
    Left = 72
    Top = 296
  end
  object odImport: TOpenDialog
    Filter = 'RW Reports (*.rwr)|*.rwr|All Files (*.*)|*.*'
    Title = 'Import Report From File'
    Left = 216
    Top = 296
  end
end
