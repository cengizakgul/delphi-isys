inherited EDIT_SB_MULTICLIENT_REPORTS: TEDIT_SB_MULTICLIENT_REPORTS
  object pcReports: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 277
    ActivePage = tsBrowse
    Align = alClient
    TabOrder = 0
    object tsBrowse: TTabSheet
      Caption = 'Reports'
      object Panel4: TevPanel
        Left = 0
        Top = 0
        Width = 435
        Height = 36
        Align = alTop
        TabOrder = 0
        object evPanel1: TevPanel
          Left = 172
          Top = 1
          Width = 262
          Height = 34
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object chbPreview: TevCheckBox
            Left = 4
            Top = 1
            Width = 94
            Height = 17
            Caption = 'Show Preview'
            TabOrder = 0
          end
          object chbQueue: TevCheckBox
            Left = 4
            Top = 17
            Width = 136
            Height = 17
            Caption = 'Run through the Queue'
            Checked = True
            State = cbChecked
            TabOrder = 1
          end
          object Button3: TevBitBtn
            Left = 159
            Top = 4
            Width = 97
            Height = 25
            Caption = 'Run Report'
            TabOrder = 2
            OnClick = Button3Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD28DDDD
              DDDDDDDDDDFDDDDDDDDDDDDDD2A28DDDDDDDDDDDDF8FDDDDDDDDDDDD2AAA28DD
              DDDDDDDDF888FDDDDDDDDDD2AAAA28DDDDDDDDDF8888FDDDDDDDDD2AAAAAA200
              000DDDF888888F88888DD2AA882AA2877778DF88DDD88FDDDDD82A88888AAA28
              8778F8DD88D888F88DD8D8888778AA287778DD888DDD88FDDDD8D8888712AAA2
              8778D8888D8D888FDDD8D88887778AA28778D8888DDDD88FDDD8D88888888AAA
              2888D8888888D888F888D888877778AA2878D8888DDDDD88F8D8D88878FFF7A2
              878DD888D8DDDD8F8D8DD8870000000878DDD88D888888D8D8DDD87777777777
              8DDDD8DDDDDDDDDD8DDDDD8888888888DDDDDD8888888888DDDD}
            NumGlyphs = 2
          end
        end
      end
      object evPanel2: TevPanel
        Left = 0
        Top = 36
        Width = 435
        Height = 213
        Align = alClient
        TabOrder = 1
        object grReports: TevDBGrid
          Left = 1
          Top = 1
          Width = 433
          Height = 211
          DisableThemesInTitle = False
          Selected.Strings = (
            'DESCRIPTION'#9'60'#9'Description'#9#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
          IniAttributes.SectionName = 'TEDIT_SB_MULTICLIENT_REPORTS\grReports'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = wwdsDetail
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
      end
    end
    object tsDetail: TTabSheet
      Caption = 'Detail'
      ImageIndex = 1
      object Label1: TevLabel
        Left = 8
        Top = 8
        Width = 65
        Height = 13
        Caption = 'Description'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object evLabel1: TevLabel
        Left = 8
        Top = 48
        Width = 39
        Height = 13
        Caption = '~Report'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object wwDBEdit1: TevDBEdit
        Left = 90
        Top = 8
        Width = 294
        Height = 21
        DataField = 'DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object cbReport: TevDBLookupCombo
        Left = 90
        Top = 48
        Width = 295
        Height = 21
        HelpContext = 50501
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'64'#9'NAME'#9'F')
        LookupTable = csReports
        LookupField = 'Key'
        Style = csDropDownList
        DropDownCount = 16
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        OnChange = cbReportChange
      end
      object btnInpForm: TBitBtn
        Left = 400
        Top = 46
        Width = 137
        Height = 25
        Caption = 'Report Parameters'
        TabOrder = 2
        OnClick = btnInpFormClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
          8888DFFFFFFFFFFFFFFF4444444444444448888F8F8F8F8F8F8FD4FF4F4F4F4F
          4F48D88888888888888F664FFF7F7F7F7F488D8888888888888F6EE4FFF44444
          FF488DD88888FDDD888F6E884FFF4874FF488D8D88888FDD888F6E8EE4FFF484
          FF488D88D88888FD888F6E899E4FFF44FF488D8DDD88888F888F6E8EE994FFF4
          FF488D888DD88888888F6E8CCEE14FFFFF488D8DD88D8888888F6E8EECCEE4FF
          FF488D8DDDDDD888888F6E8EEEECE64FFF488DD8888DDD88888F6EE8888EE674
          FF488DDDDDDDD8D8888F6EEEEEEEE6784F488888888888DD888F666666666678
          D448DDDDDDDDDDD8D88FDD8888888888DD4DDD8888888888DD8D}
        NumGlyphs = 2
      end
      object memNote: TEvDBMemo
        Left = 8
        Top = 80
        Width = 528
        Height = 125
        DataField = 'notes'
        DataSource = wwdsList
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 3
      end
      object gbASCII: TevGroupBox
        Left = 8
        Top = 223
        Width = 529
        Height = 54
        Caption = 'Output ASCII file name'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        object sbBrowse: TevSpeedButton
          Left = 374
          Top = 20
          Width = 23
          Height = 22
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD7F7D7F7D7F
            7DDDDDDFDDDFDDDFDDDDDD80FD80FD80FDDDDD88FD88FD88FDDDDD087D087D08
            7DDDDDD8DDD8DDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
          OnClick = sbBrowseClick
          ShortCut = 0
        end
        object dbeFile: TevDBEdit
          Left = 11
          Top = 20
          Width = 364
          Height = 21
          HelpContext = 50502
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          OnChange = dbeFileChange
        end
        object chbAddFile: TevCheckBox
          Left = 411
          Top = 22
          Width = 108
          Height = 17
          Caption = 'Add to existing file'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = chbAddFileClick
        end
      end
      object chbDuplexing: TevCheckBox
        Left = 8
        Top = 291
        Width = 73
        Height = 17
        Caption = 'Duplexing'
        TabOrder = 5
        OnClick = chbDuplexingClick
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_MULTICLIENT_REPORTS.SB_MULTICLIENT_REPORTS
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
  end
  inherited wwdsList: TevDataSource
    DataSet = csReports
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 232
    Top = 16
  end
  object csReports: TevClientDataSet
    Tag = -1
    ControlType.Strings = (
      'Select;CheckBox;Yes;No')
    FieldDefs = <
      item
        Name = 'NBR'
        DataType = ftInteger
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 64
      end
      item
        Name = 'LEVEL'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'NOTES'
        DataType = ftMemo
      end
      item
        Name = 'KEY'
        DataType = ftString
        Size = 16
      end
      item
        Name = 'Type'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'cName'
        DataType = ftString
        Size = 64
      end>
    Left = 292
    Top = 17
  end
end
