// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDM_IM_PAYSUITE;
{$include gdy.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  genericImport, SDataStructure, db,   DateUtils, kbmMemTable,
  ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents, EvContext,
  EvDataAccessComponents, evExceptions, EvUIComponents, EvClientDataSet;

type
  TDM_IM_PAYSUITE = class(TDataModule, ITablespace, IExternalDataModule)
    E1: TevClientDataSet;
    E1F_NAME: TStringField;
    CV: TevClientDataSet;
    CP: TevClientDataSet;
    CE: TevClientDataSet;
    CD: TevClientDataSet;
    ET: TevClientDataSet;
    E1L_NAME: TStringField;
    E1E1_ID: TIntegerField;
    eds: TevClientDataSet;
    edseord: TStringField;
    edsCODE: TStringField;
    edsNAME: TStringField;
    edsDESCRIPTION: TStringField;
    EA_checklines: TevClientDataSet;
    EA_checklinesE1_ID: TIntegerField;
    EA_checklinesREC_TYPE: TStringField;
    EA_checklinesCODE_TYPE: TStringField;
    EA_checklinesCODE: TStringField;
    EA_checklinesCUR_ACCUM: TFloatField;
    EA_checklinesMTD_ACCUM: TFloatField;
    EA_checklinesQTD_ACCUM: TFloatField;
    EA_checklinesYTD_ACCUM: TFloatField;
    EA_checklinesYTD_less_QTD: TBCDField;
    EA_EY_state: TevClientDataSet;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    BCDField1: TBCDField;
    EA_EY_stateTAX_TABLE: TStringField;
    EA_EY_stateTAX_TYPE: TStringField;
    locals: TevClientDataSet;
    localsLocal: TStringField;
    localsDESCRIPTION: TStringField;
    localsEIN: TStringField;
    localsState: TStringField;
    ETE1_TT1_CODE_1: TStringField;
    ETCD_TT1_CODE: TStringField;
    ETCODE: TStringField;
    EA_EY_local: TevClientDataSet;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    StringField4: TStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    BCDField2: TBCDField;
    StringField5: TStringField;
    EA_EY_localCD_TT1_CODE: TStringField;
    EA_EY_sui: TevClientDataSet;
    IntegerField3: TIntegerField;
    StringField6: TStringField;
    StringField7: TStringField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    BCDField3: TBCDField;
    StringField8: TStringField;
    ED: TevClientDataSet;
    eeo_codes: TevClientDataSet;
    eeo_codesEEOC_CODE: TStringField;
    EDCD_HOW_TYPE: TStringField;
    EDDED_SCHED_1: TStringField;
    EDDED_SCHED_2: TStringField;
    EDDED_SCHED_3: TStringField;
    EDDED_SCHED_4: TStringField;
    EDDED_SCHED_5: TStringField;
    EDCD_WHEN_TYPE: TStringField;
    EB: TevClientDataSet;
    S10_DB: TevClientDataSet;
    EBCODE: TStringField;
    S10_DBS10_ID: TIntegerField;
    EDS10_ID: TIntegerField;
    EDEB_S10_ACT_NUM: TStringField;
    EBBANK_ABA: TIntegerField;
    EY: TevClientDataSet;
    EYYTD_less_QTD: TBCDField;
    EA: TevClientDataSet;
    BCDField4: TBCDField;
    EDEB_CHECK_SAVE: TStringField;
    procedure E1CalcFields(DataSet: TDataSet);
    procedure E1FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure EACalcFields(DataSet: TDataSet);
    procedure ETCalcFields(DataSet: TDataSet);
    procedure EDCalcFields(DataSet: TDataSet);
    procedure EBCalcFields(DataSet: TDataSet);
    procedure S10_DBCalcFields(DataSet: TDataSet);
    procedure CVFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure ETFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    IDList: string;
    FCustomCompanyNumber: string;
    FConnectionString: string;
    FConnected: boolean;
    FLogger: IImportLogger;
    procedure CreateCDS(aFileName: string; cds: TevClientDataSet);
    procedure FillSecondaryDataSets;
  protected
    {IExternalDataModule}
    procedure OpenConnection( aConnectionString: string );
    procedure CloseConnection;
    function Connected: boolean;
    function ConnectionString: string;
    function CustomCompanyNumber: string;
    function Tablespace: ITablespace;
    procedure SetLogger( aLogger: IImportLogger );
  public
    {ITableSpace}
    function GetTable( const name: string ): TDataSet;
    procedure GetAvailableTables( const list: TStrings );
  end;


implementation
uses
  evutils, gdycommon, clarion, cldb, typinfo, gdystrset{$ifdef D6_UP},
  variants{$endif};
{$R *.DFM}

procedure OpenClientForCompany( custom_company_number: string );
begin
  DM_TEMPORARY.TMP_CO.DataRequired('' );
  if DM_TEMPORARY.TMP_CO.Locate('CUSTOM_COMPANY_NUMBER', custom_company_number, []) then
    ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO['CL_NBR'])
  else
    raise EevException.CreateFmt( 'Company <%s> doesn''t exist in Evolution database', [custom_company_number] );
  DM_COMPANY.CO.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_CLIENT.CL_E_DS.DataRequired('' );
  DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('' );
  DM_SYSTEM_HR.SY_HR_EEO.DataRequired('' );

  DM_COMPANY.CO_SUI.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
  DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR='+VarToStr(DM_TEMPORARY.TMP_CO['CO_NBR']) );
end;

function RemovePrefix( const afieldName: String ): string;
begin
  Result := Trim(Copy(afieldName, Pos(':', afieldName) + 1, Length(afieldName)));
  Assert( Result <> '' );
end;

function IsDateField( const fname: string ): boolean;
begin
  Result := Pos('_DT', AnsiUpperCase(fname) ) > 0;
end;

procedure CreateFields( const cds: TEvClientDataSet; const ct: TctClarion );
  function ClarionToFieldType( const ctf: TctField ): TFieldType;
  begin
    case ctf.GetFieldType of
      FLD_BYTE,
      FLD_SHORT: Result := ftSmallInt;
      FLD_LONG: if IsDateField(RemovePrefix(ctf.GetFieldName)) then
                  Result := ftDate
                else
                  Result := ftInteger;
      FLD_REAL: Result := ftFloat;
      FLD_DECIMAL: Result := ftBCD;
      FLD_STRING,
      FLD_PICTURE: Result := ftstring;
      FLD_GROUP: Result := ftUnknown; //silently skip
    else
      raise EevException.CreateFmt( 'Unknown clarion field type in table <%s>',[ct.Filename] );
    end;
  end;

  procedure CreateField( const ctf: TctField );
    procedure DoCreateField( pfx: string );
    var
      aDataType: TFieldType;
      f: TField;
      fname: string;
    begin
      aDataType := ClarionToFieldType(ctf);
      fname := RemovePrefix(ctf.GetFieldName) + pfx;
      if (aDataType <> ftUnknown) and not assigned(cds.FindField(fname)) then
      begin
        f := DefaultFieldClasses[aDataType].Create( cds );
        f.FieldName := fname;
        f.Name := cds.Name + f.FieldName;
        case ctf.GetFieldType of
          FLD_DECIMAL:
            begin
              (f as TBCDField).Precision := ctf.GetDecSig + ctf.GetDecDec;
              f.Size := ctf.GetDecDec;
            end;
          FLD_STRING,
          FLD_PICTURE:
              f.Size := ctf.GetFieldSize;
        end;
//      if f.DataType = ftDate then
//        f.Size :=
        f.DataSet := cds;
      end
    end;
  var
    i: integer;
  begin
    if not ctf.IsArray then
      DoCreateField( '' )
    else
      for i := 1 to ct.Arrays[ctf.GetArrayNumber-1].GetDim(0) do
        DoCreateField( '_'+inttostr(i) );
  end;
var
  i: integer;
begin
  for i := 0 to ct.GetFieldCount-1 do
    CreateField( ct.Fields[i] );
  cds.CreateDataSet;
end;
//        ODS('AddFieldDef: <%s> datatype=<%s> precision=%d size=%d',[Name, GetEnumName( typeinfo(TFieldType), ord(DataType)), Precision, Size]);

procedure FillDataSet( const cds: TEvClientDataSet; const ct: TctClarion );
var
  ctc: TCtCursor;
  function ShouldReadRecord: boolean;
  var
    i: integer;
  begin
    Result := true;
    for i := 0 to ct.GetFieldCount-1 do
      if AnsiSameText(RemovePrefix(ct.Fields[i].GetFieldName), 'REC_TYPE') and
         (ctc.GetString( ct.Fields[i] ) <> 'C' ) then
      begin
        Result := false;
        break;
      end
  end;
  procedure SetFieldValue( fname: string; val: Variant );
  begin
    if IsDateField( fname ) then
      if val <> 0 then
        cds.fieldByName(fname).AsDateTime := strtoint(VarToStr(val))-DELTA_DAYS
      else
        cds[fname] := Null
    else
      cds[fname] := val;
  end;
var
  i, j: integer;
  v, v1: Variant;
begin
  ctc := TCtCursor.Create(ct, $FFFF, coFastForw);
  try
    ctc.GotoFirst;
    while not ctc.EOF do
    begin
      if ShouldReadRecord then
      begin
        cds.Append;
        try
          for i := 0 to ct.GetFieldCount-1 do
            if ct.Fields[i].GetFieldType <> FLD_GROUP then
            begin
              v := ctc.GetVariant( ct.Fields[i] );
              if (cds.Name = 'EB') and (Trim(ct.Fields[i].GetFieldName) = 'EB:S10_ID') and (v < 0) then
              begin
                v := Abs(v) - ctc.GetVariant( ct.FieldByName('EB:E1_ID') );
                v1 := v mod 100 + 100;
                v := (v - v1) div (v mod 100 * 100);
                SetFieldValue( 'S10_ID', v );
              end
              else if not ct.Fields[i].IsArray then
                SetFieldValue( RemovePrefix(ct.Fields[i].GetFieldName), v )
              else
                for j := VarArrayLowBound(v, 1) to VarArrayHighBound(v, 1) do
                  SetFieldValue( RemovePrefix(ct.Fields[i].GetFieldName)+'_'+inttostr(j+1), v[j] )
            end;
          cds.Post;
        except
          cds.Cancel;
          raise;
        end;
      end;
      ctc.GotoNext;
    end
  finally
    FreeAndNil( ctc );
  end;
  cds.MergeChangeLog;
end;

{ TDM_IM_PAYSUITE }
procedure TDM_IM_PAYSUITE.CreateCDS( aFileName: string; cds: TevClientDataSet );
var
  ct: TctClarion;
begin
  try
    ct := TctClarion.Create( nil );
    try
      ct.FileName := aFileName;
      ct.Read_Only := True;
      ct.Exclusive := False;
      ct.Open;
      cds.SkipFieldCheck := True;
      if not cds.Active then
        CreateFields( cds, ct )
      else
        cds.EmptyDataSet;
      FillDataSet( cds, ct );
    finally
      FreeAndNil( ct );
    end;
  except
    if cds.Active then
      cds.EmptyDataSet;
    raise;
  end;
end;

procedure TDM_IM_PAYSUITE.OpenConnection(aConnectionString: string);
  function GetCustomCompanyNumber: string;
  var
    CV: TDataSet;
  begin
    CV := GetTable('CV');
    CV.First;
    if not (CV.EOf and CV.Bof) then
      if not CV.FieldByName('BCL_CODE').IsNull then
        Result := VarToStr(CV['BCL_CODE'])
      else
        raise EevException.Create('CV table has null BCL_CODE field. Cannot determinate custom company number.')
    else
      raise EevException.Create('CV table is empty. Cannot determinate custom company number.');
  end;
  function GetFilePrefix: string;
  var
    Status: Integer;
    SearchRec: TSearchRec;
    clientpath: string;
    run: string;
  begin
    run := '';
    Result := '';
    clientpath := WithoutTrailingSlash(ParseConnectionString(aConnectionString).ClientDBPath);
    Status := FindFirst(clientpath+'\*cv.dat', faAnyFile and not faDirectory, SearchRec);
    try
      if Status = 0 then
      begin
        repeat
          if Copy(SearchRec.Name, 5, 2) > run then
          begin
            run := Copy(SearchRec.Name, 5, 2);
            Result := clientpath + '\' + copy( SearchRec.Name, 1, Length(SearchRec.Name)-6)
          end;
        until FindNext(SearchRec) <> 0;
      end
      else
        raise EevException.Create( 'Cannot find CV table in '+clientpath+' directory' );
    finally
      FindClose(SearchRec);
    end;
//    Result := WithoutTrailingSlash(adatabaseName)+'\'+ExtractFileName(WithoutTrailingSlash(adatabaseName))+'35';
  end;
var
  pfx: string;
  systempfx: string;
const
  tableList: array [0..9] of string =
  (
   'CV', 'CP', {'CT',  }'CD', 'CE', 'E1', 'ET', 'EB', 'ED', 'EY', 'EA'
  );
  systemTableList: array [0..0] of string =
  (
     'S10_DB'
  );
  sText = 'Opening PaySuite database';
var
  i: integer;
  c: integer;
  dt: TDateTime;
  by: TDateTime;
  st: string;
begin
  if not Connected or (FConnectionString <> aConnectionString) then
  begin
    FLogger.LogEntry( sText );
    try
      try
        ctx_StartWait( sText, Length(tableList)+Length(systemTableList) );
        c := 0;
        try
          CloseConnection;
          try
            systempfx := WithoutTrailingSlash(ParseConnectionString(aConnectionString).SystemDBPath) + '\';
            FLogger.LogContextItem( sciExtSysDatabase, systempfx );
            for i := low(systemTableList) to high(systemTableList) do
            try
              CreateCDS( systempfx + systemTableList[i] + '.dat', GetTable( systemTableList[i] ) as TevClientDataSet);
              ctx_UpdateWait( sText, c );
              inc(c);
            except
              FLogger.StopExceptionAndWarnFmt( 'Failed to read %s.dat', [systempfx + systemTableList[i]] );
            end;
            pfx := GetFilePrefix;
            FLogger.LogContextItem( sciExtDatabase, pfx );
            for i := low(tableList) to high(tableList) do
            try
              CreateCDS( pfx + tablelist[i] + '.dat', GetTable( tablelist[i] ) as TevClientDataSet);
              ctx_UpdateWait( sText, c );
              inc(c);
            except
              if tablelist[i] <> 'EB'{!!7902} then
                FLogger.StopExceptionAndWarnFmt( 'Failed to read %s.dat', [pfx + tablelist[i]] );
            end;

            E1.First;
            by := StartOfTheYear(Date);
            IDList := ',';
            while not E1.Eof do
            begin
              dt := E1.FieldByName('TERM_DT').AsDateTime;
              st := E1.FieldByName('STATUS').AsString;
              if (st <> 'A') and
                 (st <> 'R') and
                 (st <> 'N') and
                 (dt <> 0) and (dt < by) then
                IDList := IDList + E1.FieldByName('E1_ID').AsString + ',';
              E1.Next;
            end;

            FCustomCompanyNumber := GetCustomCompanyNumber;
            OpenClientForCompany( FCustomCompanyNumber );
            FillSecondaryDataSets;
            FConnected := true;
            FConnectionString := aConnectionString;
          except
            CloseConnection;
            raise;
          end;
        finally
          ctx_EndWait;
        end;
      except
        FLogger.PassthroughExceptionAndWarnFmt( sImportFailedToOpenConnection, [] );
      end;
    finally
      FLogger.LogExit;
    end;
  end;
end;

procedure TDM_IM_PAYSUITE.CloseConnection;
begin
  FConnected := false;
  FConnectionString := '';
end;

function TDM_IM_PAYSUITE.Connected: boolean;
begin
  Result := FConnected;
end;

function TDM_IM_PAYSUITE.CustomCompanyNumber: string;
begin
  Result := FCustomCompanyNumber;
end;

procedure TDM_IM_PAYSUITE.GetAvailableTables(const list: TStrings);
var
  i: integer;
begin
  for i := 0 to ComponentCount-1 do
    if Components[i] is TEvClientDataSet then
      list.Add(Components[i].Name);
end;

function TDM_IM_PAYSUITE.GetTable(const name: string): TDataSet;
var
  c: TComponent;
  tn: string;
begin
  tn := name;
  if AnsiSameText( tn, 'E1_fixup') then
    tn := 'E1'
  else if AnsiSameText( tn, 'E1_payroll_fixup') then
    tn := 'E1'
  else if AnsiSameText( tn, 'EB_np') then
    tn := 'EB';
  c := FindComponent( tn );
  if not assigned(c) or not (c is TevClientDataSet) then
    raise EevException.CreateFmt( 'Unknown table requested: <%s>',[name]);
  Result := c as TDataSet;
end;

function TDM_IM_PAYSUITE.Tablespace: ITablespace;
begin
  Result := Self;
end;

procedure TDM_IM_PAYSUITE.E1CalcFields(DataSet: TDataSet);
begin
  DataSet['F_NAME'] := DataSet['F_NAME_6'] + DataSet['F_NAME_10'];
  DataSet['L_NAME'] := DataSet['L_NAME_10'] + DataSet['L_NAME_6'];
  DataSet['E1_ID'] := DataSet['ID'];
end;

procedure TDM_IM_PAYSUITE.E1FilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
var
  emp_num: string;
begin
  emp_num := trim(DataSet.FieldByName('EMP_NUM').AsString);
  Accept := (length(emp_num) < 2) or (emp_num[1]<>'9') or (emp_num[2]<>'9');  //not (trim(EMP_NUM) like '99%')

  if Accept and Boolean(DataSet.Tag) then
    Accept := Pos(',' + DataSet.FieldByName('E1_ID').AsString + ',', IDList) = 0;
end;

procedure AddCurrentRecord( cds: TDataSet; src: TDataSet );
var
  i: integer;
begin
  cds.Append;
  try
    for i := 0 to cds.FieldCount-1 do
      cds.Fields[i].Value := src[cds.Fields[i].FieldName];
  except
    cds.Cancel;
    raise;
  end;
  cds.Post;
end;

procedure TDM_IM_PAYSUITE.FillSecondaryDataSets;
  procedure AddToEDS( const eord, acode, aname, adescription: Variant );
  begin
    with eds do
    begin
      Append;
      try
        FieldValues['eord;code;name;description'] := VarArrayOf([eord, acode, aname, adescription]);
        Post;
      except
        Cancel;
        raise;
      end;
    end;
  end;
var
  unique_locals: TStringSet;
  procedure AddToLocals( local, description, ein: Variant );
  var
    s: string;
  begin
    s := trim(VarToStr(local));
    if (Length(s) > 0) and not InSet( s, unique_locals )  then
    begin
      with locals do
      begin
        Append;
        try
          FieldValues['Local;State;DESCRIPTION;EIN'] := VarArrayOf( [s, UpperCase( Copy( s, 1, 2 ) ), description, ein] );
          Post;
        except
          Cancel;
          raise;
        end;
      end;
      SetInclude( unique_locals, s );
    end;
  end;
  procedure Prepare( cds: TEvClientDataSet );
  begin
    if not cds.Active then
      cds.CreateDataSet
    else
      cds.EmptyDataSet;
  end;
  function IsLocalCDCode( cd_code: Variant ): boolean;
  var
    code: string;
  begin
    code := trim(UpperCase(VarToStr(cd_code)));
    Result := (Length(code) = 2) and ((code[1]='L') and (code[2] in ['1'..'9']) or (code[1]='J') and (code[2] in ['1'..'6']));
  end;
var
  i: integer;
  eeoc: set of char;
  c: char;
  s: string;
begin
  Prepare( eds );
  Prepare( EA_checklines );
  Prepare( EA_EY_state );
  Prepare( EA_EY_local );
  Prepare( EA_EY_sui );
  Prepare( locals );
  Prepare( eeo_codes );

  unique_locals := nil;

  CD.First;
  while not CD.Eof do
  begin
    if CD['TAX_DED'] = 'D' then
      AddToEDS( 'D', CD['CODE'], CD['NAME'], CD['Description'])
    else if (CD['TAX_DED'] = 'T') and IsLocalCDCode(CD['CODE']) then
      AddToLocals( CD['TT1_CODE'], CD['DESCRIPTION'], Null{!!});
    CD.Next;
  end;
  if EB.Active then //7902
    if not VarIsNull( EB.Lookup( 'CPD_CODE', 'NP', 'CPD_CODE' ) ) then
      AddToEDS( 'D', 'NP', '', 'Net Payroll' );

  CE.First;
  while not CE.Eof do
  begin
    AddToEDS( 'E', CE['CODE'], CE['NAME'], CE['Description']);
    CE.Next;
  end;

  if EY.Active then
  begin
    EY.First;
    while not EY.Eof do
    begin
      if InSet( trim(VarToStr(EY['CODE_TYPE'])), ['X','T'] ) and
         InSet( trim(VarToStr(EY['CODE'])), ['ST','DD','DI'] ) {and (trim(VarToStr(EY['TAX_TYPE']))='ST')} then
      begin
        with EA_EY_state do
        begin
          Append;
          try
            for i := 0 to FieldCount-1 do
              if assigned(EY.FindField(Fields[i].FieldName)) then
                Fields[i].Value := EY[Fields[i].FieldName];
  //          FieldValues['TAX_TABLE'] := E1.Lookup( 'E1_ID', EY['E1_ID'], 'LIVE_STATE');
  //          FieldValues['TAX_TYPE'] := 'ST';
            Post;
          except
            Cancel;
            raise;
          end;
        end;
      end
      else if InSet( trim(VarToStr(EY['CODE_TYPE'])), ['X','T'] ) and
              InSet( trim(VarToStr(EY['CODE'])), ['LC','J1','J2','J3','J4','J5','J6','NW']){and (trim(VarToStr(EY['TAX_TYPE']))='LC')} then
      begin
        with EA_EY_local do
        begin
          Append;
          try
            for i := 0 to FieldCount-1 do
              if assigned(EY.FindField(Fields[i].FieldName)) then
                Fields[i].Value := EY[Fields[i].FieldName];
  //          FieldValues['TAX_TABLE'] := E1.Lookup( 'E1_ID', EY['E1_ID'], 'TT1_CODE_1');
            FieldValues['CD_TT1_CODE'] := CD.Lookup( 'CODE', EY['CODE'], 'TT1_CODE');
            Post;
          except
            Cancel;
            raise;
          end;
        end;
      end
      else if InSet( trim(VarToStr(EY['CODE_TYPE'])), ['X','T'] ) and
              InSet( trim(VarToStr(EY['CODE'])), ['UI','UD','TR'] ) {and (trim(VarToStr(EY['TAX_TYPE']))='UC')} then
      begin
        with EA_EY_sui do
        begin
          Append;
          try
            for i := 0 to FieldCount-1 do
              if assigned(EY.FindField(Fields[i].FieldName)) then
                Fields[i].Value := EY[Fields[i].FieldName];
  //          FieldValues['TAX_TABLE'] := VarToStr(E1.Lookup( 'E1_ID', EY['E1_ID'], 'WORK_STATE'))+'UC';
            Post;
          except
            Cancel;
            raise;
          end;
        end;
      end;

      EY.Next;
    end;
  end;

  EA.First;
  while not EA.Eof do
  begin
    if InSet( trim(VarToStr(EA['CODE_TYPE'])), ['$','H','D'] ) then
      AddCurrentRecord( EA_checklines, EA );
    if InSet( trim(VarToStr(EA['CODE_TYPE'])), ['X','T'] ) and
       InSet( trim(VarToStr(EA['CODE'])), ['ST','DD','DI'] ) and
       not EA_EY_state.Locate('E1_ID;CODE;CODE_TYPE', EA['E1_ID;CODE;CODE_TYPE'], []) then
    begin
      with EA_EY_state do
      begin
        Append;
        try
          for i := 0 to FieldCount-1 do
            if assigned(EA.FindField(Fields[i].FieldName)) then
              Fields[i].Value := EA[Fields[i].FieldName];
          FieldValues['TAX_TABLE'] := E1.Lookup( 'E1_ID', EA['E1_ID'], 'LIVE_STATE');
//          FieldValues['TAX_TYPE'] := EA['CODE'];
          Post;
        except
          Cancel;
          raise;
        end;
      end;
    end
    else if InSet( trim(VarToStr(EA['CODE_TYPE'])), ['X','T'] ) and
            InSet( trim(VarToStr(EA['CODE'])), ['LC','J1','J2','J3','J4','J5','J6','L1','L2','L3','L4','L5','L6','L7','L8','L9','NW']) and
            not EA_EY_local.Locate('E1_ID;CODE;CODE_TYPE', EA['E1_ID;CODE;CODE_TYPE'], []) then
    begin
      with EA_EY_local do
      begin
        Append;
        try
          for i := 0 to FieldCount-1 do
            if assigned(EA.FindField(Fields[i].FieldName)) then
              Fields[i].Value := EA[Fields[i].FieldName];
          FieldValues['TAX_TABLE'] := E1.Lookup( 'E1_ID', EA['E1_ID'], 'TT1_CODE_1');
          FieldValues['CD_TT1_CODE'] := CD.Lookup( 'CODE', EA['CODE'], 'TT1_CODE');
          Post;
        except
          Cancel;
          raise;
        end;
      end;
    end
    else if InSet( trim(VarToStr(EA['CODE_TYPE'])), ['X','T'] ) and
            InSet( trim(VarToStr(EA['CODE'])), ['UI','UD','TR'] ) and
            not EA_EY_sui.Locate('E1_ID;CODE;CODE_TYPE', EA['E1_ID;CODE;CODE_TYPE'], []) then
    begin
      with EA_EY_sui do
      begin
        Append;
        try
          for i := 0 to FieldCount-1 do
            if assigned(EA.FindField(Fields[i].FieldName)) then
              Fields[i].Value := EA[Fields[i].FieldName];
          FieldValues['TAX_TABLE'] := VarToStr(E1.Lookup( 'E1_ID', EA['E1_ID'], 'WORK_STATE'));
          Post;
        except
          Cancel;
          raise;
        end;
      end;
    end;

    EA.Next;
  end;

  eeoc := [];
  E1.First;
  while not E1.Eof do
  begin
    s := trim(E1.FieldByName('EEOC_CODE').AsString);
    if Length(s) = 1 then
      Include( eeoc, s[1] );
    AddToLocals( E1['TT1_CODE_1'], Null{!!}, Null{!!} );
    E1.Next;
  end;
  for c := low(char) to high(char) do
    if c in eeoc then
      with eeo_codes do
      begin
        Append;
        try
          FieldByName('EEOC_CODE').AsString := c;
          Post;
        except
          Cancel;
          raise;
        end;
      end;
  eeo_codes.MergeChangeLog;

  eds.MergeChangeLog;
  locals.MergeChangeLog;
  EA_checklines.MergeChangeLog;
  EA_EY_state.MergeChangeLog;
  EA_EY_local.MergeChangeLog;
  EA_EY_sui.MergeChangeLog;
end;

procedure TDM_IM_PAYSUITE.EACalcFields(DataSet: TDataSet);
begin
  DataSet['YTD_less_QTD'] := DataSet['YTD_ACCUM'] - DataSet['QTD_ACCUM'];
end;

procedure TDM_IM_PAYSUITE.ETCalcFields(DataSet: TDataSet);
begin
  DataSet['CODE'] := DataSet['CPD_CODE'];
end;

function TDM_IM_PAYSUITE.ConnectionString: string;
begin
  Result := FConnectionString;
end;

{
object eeo_codes: TevClientDataSet
  object eeo_codesEEOC_CODE: TStringField
    FieldName = 'EEOC_CODE'
    Size = 1
  end
end

}
procedure TDM_IM_PAYSUITE.EDCalcFields(DataSet: TDataSet);
  function sa2s( sa: array of string; prefix, delim: string ): string;
  var
    i: integer;
  begin
    Result := '';
    for i := low(sa) to high(sa) do
    begin
      if Result <> '' then
        Result := Result + delim;
      Result := Result + prefix + sa[i];
    end
  end;
  procedure SetFieldsToNull( ds: TDataSet; sa: array of string; prefix: string );
  var
    i: integer;
  begin
    for i := low(sa) to high(sa) do
      ds[prefix+sa[i]] := Null;
  end;
var
  v: Variant;
const
  fn: array [0..6] of string = ( 'HOW_TYPE', 'WHEN_TYPE', 'DED_SCHED_1', 'DED_SCHED_2', 'DED_SCHED_3', 'DED_SCHED_4', 'DED_SCHED_5' );
  ebfn: array [0..2] of string = ( 'ACT_NUM', 'S10_BANK_ABA', 'CHECK_SAVE' );
begin
  if DataSet['CODE_TYPE'] = 'D' then
  begin
    v := CD.Lookup( 'TAX_DED;CODE', VarArrayOf(['D',DataSet['CODE']]), sa2s(fn, '', ';') );
    if not VarIsNull( v ) and VarIsArray( v ) then
      DataSet[sa2s(fn, 'CD_', ';')] := v
    else
      SetFieldsToNull( DataSet, fn, 'CD_' );
    if EB.Active then //7902
    begin
      v := EB.Lookup( 'E1_ID;CPD_CODE', DataSet['E1_ID;CODE'], sa2s(ebfn, '', ';') );
      if not VarIsNull( v ) and VarIsArray( v ) then
        DataSet[sa2s(ebfn, 'EB_', ';')] := v
      else
        SetFieldsToNull( DataSet, ebfn, 'EB_' );
    end
    else
      SetFieldsToNull( DataSet, ebfn, 'EB_' );
  end
  else
  begin
    SetFieldsToNull( DataSet, fn, 'CD_' );
    SetFieldsToNull( DataSet, ebfn, 'EB_' );
  end
end;

procedure TDM_IM_PAYSUITE.EBCalcFields(DataSet: TDataSet);
begin
  DataSet['CODE'] := DataSet['CPD_CODE'];
end;

procedure TDM_IM_PAYSUITE.S10_DBCalcFields(DataSet: TDataSet);
begin
  DataSet['S10_ID'] := DataSet['ID'];
end;

procedure TDM_IM_PAYSUITE.CVFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := trim(DataSet.FieldByName('CODE').AsString) <> '99';
end;

procedure TDM_IM_PAYSUITE.SetLogger(aLogger: IImportLogger);
begin
  FLogger := aLogger;
end;

procedure TDM_IM_PAYSUITE.ETFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept := Pos(',' + DataSet.FieldByName('E1_ID').AsString + ',', IDList) = 0;
end;

end.


