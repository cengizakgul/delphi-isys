object DM_IM_PAYSUITE: TDM_IM_PAYSUITE
  OldCreateOrder = False
  Left = 313
  Top = 232
  Height = 579
  Width = 741
  object E1: TevClientDataSet
    OnCalcFields = E1CalcFields
    OnFilterRecord = E1FilterRecord
    Filtered = True
    Left = 128
    Top = 96
    object E1F_NAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'F_NAME'
      Size = 16
      Calculated = True
    end
    object E1L_NAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'L_NAME'
      Size = 16
      Calculated = True
    end
    object E1E1_ID: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'E1_ID'
      Calculated = True
    end
  end
  object CV: TevClientDataSet
    OnFilterRecord = CVFilterRecord
    Filtered = True
    Left = 48
    Top = 96
  end
  object CP: TevClientDataSet
    Left = 88
    Top = 96
  end
  object CE: TevClientDataSet
    IndexFieldNames = 'CODE'
    Left = 392
    Top = 72
  end
  object CD: TevClientDataSet
    IndexFieldNames = 'CODE'
    Left = 352
    Top = 72
  end
  object ET: TevClientDataSet
    IndexFieldNames = 'E1_ID'
    OnCalcFields = ETCalcFields
    OnFilterRecord = ETFilterRecord
    Left = 168
    Top = 96
    object ETE1_TT1_CODE_1: TStringField
      FieldKind = fkLookup
      FieldName = 'E1_TT1_CODE_1'
      LookupDataSet = E1
      LookupKeyFields = 'ID'
      LookupResultField = 'TT1_CODE_1'
      KeyFields = 'E1_ID'
      Size = 6
      Lookup = True
    end
    object ETCD_TT1_CODE: TStringField
      FieldKind = fkLookup
      FieldName = 'CD_TT1_CODE'
      LookupDataSet = CD
      LookupKeyFields = 'CODE'
      LookupResultField = 'TT1_CODE'
      KeyFields = 'CPD_CODE'
      Size = 6
      Lookup = True
    end
    object ETCODE: TStringField
      FieldKind = fkCalculated
      FieldName = 'CODE'
      Size = 2
      Calculated = True
    end
  end
  object eds: TevClientDataSet
    Left = 216
    Top = 8
    object edseord: TStringField
      DisplayWidth = 1
      FieldName = 'eord'
      Size = 1
    end
    object edsCODE: TStringField
      FieldName = 'CODE'
      Size = 2
    end
    object edsNAME: TStringField
      FieldName = 'NAME'
      Size = 7
    end
    object edsDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 25
    end
  end
  object EA_checklines: TevClientDataSet
    IndexFieldNames = 'E1_ID;CODE;CODE_TYPE'
    OnCalcFields = EACalcFields
    OnFilterRecord = ETFilterRecord
    Left = 88
    Top = 264
    object EA_checklinesE1_ID: TIntegerField
      FieldName = 'E1_ID'
    end
    object EA_checklinesREC_TYPE: TStringField
      FieldName = 'REC_TYPE'
      Size = 1
    end
    object EA_checklinesCODE_TYPE: TStringField
      FieldName = 'CODE_TYPE'
      Size = 1
    end
    object EA_checklinesCODE: TStringField
      FieldName = 'CODE'
      Size = 2
    end
    object EA_checklinesCUR_ACCUM: TFloatField
      FieldName = 'CUR_ACCUM'
    end
    object EA_checklinesMTD_ACCUM: TFloatField
      FieldName = 'MTD_ACCUM'
    end
    object EA_checklinesQTD_ACCUM: TFloatField
      FieldName = 'QTD_ACCUM'
    end
    object EA_checklinesYTD_ACCUM: TFloatField
      FieldName = 'YTD_ACCUM'
    end
    object EA_checklinesYTD_less_QTD: TBCDField
      FieldKind = fkCalculated
      FieldName = 'YTD_less_QTD'
      Size = 15
      Calculated = True
    end
  end
  object EA_EY_state: TevClientDataSet
    IndexFieldNames = 'E1_ID;CODE'
    OnCalcFields = EACalcFields
    OnFilterRecord = ETFilterRecord
    Left = 160
    Top = 264
    object IntegerField1: TIntegerField
      FieldName = 'E1_ID'
    end
    object StringField2: TStringField
      FieldName = 'CODE_TYPE'
      Size = 1
    end
    object StringField3: TStringField
      FieldName = 'CODE'
      Size = 2
    end
    object FloatField3: TFloatField
      FieldName = 'QTD_ACCUM'
    end
    object FloatField4: TFloatField
      FieldName = 'YTD_ACCUM'
    end
    object BCDField1: TBCDField
      FieldKind = fkCalculated
      FieldName = 'YTD_less_QTD'
      Size = 15
      Calculated = True
    end
    object EA_EY_stateTAX_TABLE: TStringField
      FieldName = 'TAX_TABLE'
    end
    object EA_EY_stateTAX_TYPE: TStringField
      FieldName = 'TAX_TYPE'
    end
  end
  object locals: TevClientDataSet
    Left = 256
    Top = 8
    object localsLocal: TStringField
      DisplayWidth = 30
      FieldName = 'Local'
      Size = 255
    end
    object localsDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 255
    end
    object localsEIN: TStringField
      FieldName = 'EIN'
      Size = 255
    end
    object localsState: TStringField
      FieldKind = fkCalculated
      FieldName = 'State'
      Size = 2
      Calculated = True
    end
  end
  object EA_EY_local: TevClientDataSet
    IndexFieldNames = 'E1_ID;CODE'
    OnCalcFields = EACalcFields
    OnFilterRecord = ETFilterRecord
    Left = 272
    Top = 264
    object IntegerField2: TIntegerField
      FieldName = 'E1_ID'
    end
    object StringField1: TStringField
      FieldName = 'CODE_TYPE'
      Size = 1
    end
    object StringField4: TStringField
      FieldName = 'CODE'
      Size = 2
    end
    object FloatField1: TFloatField
      FieldName = 'QTD_ACCUM'
    end
    object FloatField2: TFloatField
      FieldName = 'YTD_ACCUM'
    end
    object BCDField2: TBCDField
      FieldKind = fkCalculated
      FieldName = 'YTD_less_QTD'
      Size = 15
      Calculated = True
    end
    object StringField5: TStringField
      FieldName = 'TAX_TABLE'
    end
    object EA_EY_localCD_TT1_CODE: TStringField
      FieldKind = fkLookup
      FieldName = 'CD_TT1_CODE'
      LookupDataSet = CD
      LookupKeyFields = 'CODE'
      LookupResultField = 'TT1_CODE'
      KeyFields = 'CODE'
      Size = 6
      Lookup = True
    end
  end
  object EA_EY_sui: TevClientDataSet
    IndexFieldNames = 'E1_ID;CODE'
    OnCalcFields = EACalcFields
    OnFilterRecord = ETFilterRecord
    Left = 360
    Top = 264
    object IntegerField3: TIntegerField
      FieldName = 'E1_ID'
    end
    object StringField6: TStringField
      FieldName = 'CODE_TYPE'
      Size = 1
    end
    object StringField7: TStringField
      FieldName = 'CODE'
      Size = 2
    end
    object FloatField5: TFloatField
      FieldName = 'QTD_ACCUM'
    end
    object FloatField6: TFloatField
      FieldName = 'YTD_ACCUM'
    end
    object BCDField3: TBCDField
      FieldKind = fkCalculated
      FieldName = 'YTD_less_QTD'
      Size = 15
      Calculated = True
    end
    object StringField8: TStringField
      FieldName = 'TAX_TABLE'
    end
  end
  object ED: TevClientDataSet
    IndexFieldNames = 'E1_ID;CODE_TYPE;CODE'
    OnCalcFields = EDCalcFields
    OnFilterRecord = ETFilterRecord
    Left = 352
    Top = 128
    object EDCD_HOW_TYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'CD_HOW_TYPE'
      Size = 1
      Calculated = True
    end
    object EDDED_SCHED_1: TStringField
      FieldKind = fkCalculated
      FieldName = 'CD_DED_SCHED_1'
      Size = 1
      Calculated = True
    end
    object EDDED_SCHED_2: TStringField
      FieldKind = fkCalculated
      FieldName = 'CD_DED_SCHED_2'
      Size = 1
      Calculated = True
    end
    object EDDED_SCHED_3: TStringField
      FieldKind = fkCalculated
      FieldName = 'CD_DED_SCHED_3'
      Size = 1
      Calculated = True
    end
    object EDDED_SCHED_4: TStringField
      FieldKind = fkCalculated
      FieldName = 'CD_DED_SCHED_4'
      Size = 1
      Calculated = True
    end
    object EDDED_SCHED_5: TStringField
      FieldKind = fkCalculated
      FieldName = 'CD_DED_SCHED_5'
      Size = 1
      Calculated = True
    end
    object EDCD_WHEN_TYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'CD_WHEN_TYPE'
      Size = 1
      Calculated = True
    end
    object EDS10_ID: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'EB_S10_BANK_ABA'
      Calculated = True
    end
    object EDEB_S10_ACT_NUM: TStringField
      FieldKind = fkCalculated
      FieldName = 'EB_ACT_NUM'
      Size = 17
      Calculated = True
    end
    object EDEB_CHECK_SAVE: TStringField
      FieldKind = fkCalculated
      FieldName = 'EB_CHECK_SAVE'
      Size = 1
      Calculated = True
    end
  end
  object eeo_codes: TevClientDataSet
    Left = 384
    Top = 8
    object eeo_codesEEOC_CODE: TStringField
      FieldName = 'EEOC_CODE'
      Size = 1
    end
  end
  object EB: TevClientDataSet
    IndexFieldNames = 'E1_ID;CPD_CODE'
    OnCalcFields = EBCalcFields
    OnFilterRecord = ETFilterRecord
    Left = 208
    Top = 96
    object EBCODE: TStringField
      FieldKind = fkCalculated
      FieldName = 'CODE'
      Size = 2
      Calculated = True
    end
    object EBBANK_ABA: TIntegerField
      FieldKind = fkLookup
      FieldName = 'S10_BANK_ABA'
      LookupDataSet = S10_DB
      LookupKeyFields = 'ID'
      LookupResultField = 'BANK_ABA'
      KeyFields = 'S10_ID'
      Lookup = True
    end
  end
  object S10_DB: TevClientDataSet
    OnCalcFields = S10_DBCalcFields
    Left = 248
    Top = 96
    object S10_DBS10_ID: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'S10_ID'
      Calculated = True
    end
  end
  object EY: TevClientDataSet
    IndexFieldNames = 'E1_ID;CODE_TYPE;CODE'
    OnCalcFields = EACalcFields
    OnFilterRecord = ETFilterRecord
    Left = 104
    Top = 152
    object EYYTD_less_QTD: TBCDField
      FieldKind = fkCalculated
      FieldName = 'YTD_less_QTD'
      Size = 15
      Calculated = True
    end
  end
  object EA: TevClientDataSet
    IndexFieldNames = 'E1_ID;CODE_TYPE;CODE'
    OnCalcFields = EACalcFields
    OnFilterRecord = ETFilterRecord
    Left = 16
    Top = 264
    object BCDField4: TBCDField
      FieldKind = fkCalculated
      FieldName = 'YTD_less_QTD'
      Size = 15
      Calculated = True
    end
  end
end
