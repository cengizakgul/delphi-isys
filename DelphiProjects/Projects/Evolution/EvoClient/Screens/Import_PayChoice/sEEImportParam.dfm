object EEImportParamFrame: TEEImportParamFrame
  Left = 0
  Top = 0
  Width = 350
  Height = 109
  TabOrder = 0
  object evGroupBox1: TevGroupBox
    Left = 0
    Top = -8
    Width = 337
    Height = 73
    Caption = 'Scheduled E/Ds Import'
    TabOrder = 0
    object evLabel3: TevLabel
      Left = 16
      Top = 19
      Width = 282
      Height = 13
      Caption = 'Target Actions to be populated for imported Target Amounts'
    end
    object evcbTargetAction: TevDBComboBox
      Left = 16
      Top = 36
      Width = 305
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = True
      AllowClearKey = False
      AutoDropDown = True
      DropDownCount = 8
      ItemHeight = 0
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 0
      UnboundDataType = wwDefault
    end
  end
  object cbTerm: TevCheckBox
    Left = 24
    Top = 80
    Width = 201
    Height = 17
    Caption = 'Import only active employees'
    TabOrder = 1
  end
end
