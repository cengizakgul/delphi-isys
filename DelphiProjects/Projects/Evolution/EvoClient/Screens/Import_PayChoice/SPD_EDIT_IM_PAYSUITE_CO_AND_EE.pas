// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_PAYSUITE_CO_AND_EE;           

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList,  ActnList, Db,
  Wwdatsrc, StdCtrls, Buttons, ExtCtrls, ComCtrls,
  SDataStructure, gdyBinderVisual, gdybinder, SPD_EDIT_IM_PAYSUITE_BASE,
  sTablespaceView, genericImport, sImportDatabasePathInput, sEEImportParam,
  sImportQueue, sImportSystemAndUserDBPathsInput, sImportLoggerView,
  SDDClasses, ISBasicClasses, SDataDictsystem, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_IM_PAYSUITE_CO_AND_EE = class(TEDIT_IM_PAYSUITE_BASE)
    DM_SYSTEM_LOCAL: TDM_SYSTEM_LOCAL;
    TabSheet3: TTabSheet;
    BinderFrame1: TBinderFrame;
    tvshSchedEDsMapping: TTabSheet;
    BinderFrame2: TBinderFrame;
    ParamInput: TEEImportParamFrame;
  public
    { Public declarations }
    procedure Activate; override;
  end;

implementation

{$R *.DFM}
uses
  sPaysuiteImportDef, sPayImportDef, sImportVisualController, evUtils;

procedure TEDIT_IM_PAYSUITE_CO_AND_EE.Activate;
  procedure InitController( aImport: IImport );
  var
    ivc: IImportVisualController;
  begin
    DM_COMPANY.CO_LOCAL_TAX.EnsureHasMetadata;
    DM_CLIENT.CL_E_DS.EnsureHasMetadata;

    ivc := TImportVisualController.Create( LogFrame.LoggerKeeper, aImport, ParamInput, ViewerFrame );
    ivc.SetConnectionStringInputFrame( ConnectionStringInput );
    ivc.BindingKeeper(sLocalsBindingName).SetTables( aImport.DataModule.GetTable('locals'), DM_COMPANY.CO_LOCAL_TAX );
    ivc.BindingKeeper(sLocalsBindingName).SetVisualBinder( BinderFrame1 );
    ivc.BindingKeeper(sEDBindingName).SetAutoMapper( TEDAutoMapper.Create );
    ivc.BindingKeeper(sEDBindingName).SetTables( aImport.DataModule.GetTable('eds'), DM_CLIENT.CL_E_DS );
    ivc.BindingKeeper(sEDBindingName).SetVisualBinder( BinderFrame2 );
    SetController( ivc );
  end;
begin
  ParamInput.Activate;
  inherited;
  InitController( CreatePaysuiteCO_AND_EEImport(LogFrame.LoggerKeeper.Logger) );
end;

initialization
  RegisterClass(TEDIT_IM_PAYSUITE_CO_AND_EE);

end.
