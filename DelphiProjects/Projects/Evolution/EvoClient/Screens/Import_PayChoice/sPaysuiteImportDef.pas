// Copyright � 2000-2004 iSystems LLC. All rights reserved.
{$include gdy.inc}
unit sPaysuiteImportDef;
// not implemented importr from EY
// locals.EIN field is not populated

interface

uses
  genericImport;

function CreatePaysuiteCO_AND_EEImport( aLogger: IImportLogger ): IImport;
function CreatePaysuitePRImport( aLogger: IImportLogger ): IImport;

implementation

uses
  sysutils, evUtils, math, gdyBinderHelpers, sDataStructure, gdystrset,
  EvConsts, iemap, sImportHelpers, genericImportDefinitionImpl,
   SDM_IM_PAYSUITE, gdyBinder{$ifdef D6_UP}, variants{$endif},
  sPayImportDef;


const
  CLARION_TRUE = 'Y';
  sMapClarionBool2Char =  'Y=' + GROUP_BOX_YES + ',' + 'N=' + GROUP_BOX_NO;
  sMapClarionBool2Char_Not =  'Y=' + GROUP_BOX_NO + ',' + 'N=' + GROUP_BOX_YES;

function WorkaroundNotReadingNullValues( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  Result := Value;
  if VarType( Result ) in [varInteger, varDouble] then
  begin
    if Result = 0 then
      Result := Null;
  end
  else
    Assert( false, 'Unexpected vartype in WorkaroundNotReadingNullValues: '+inttostr(VarType(Result)) );
end;

const
  PaysuiteConversion: array [0..1] of TConvertDesc =
  (
    (Table: 'E1'; Field: 'PHONE'; Func: WorkaroundNotReadingNullValues), //!! workaround for not reading Null value correctly
    (Table: 'E1'; Field: 'CLOCK_NUM'; Func: WorkaroundNotReadingNullValues) //!! workaround for not reading Null value correctly
  );
//    (Table: ''; Field:'', Func: )
{
  PaysuiteKeys: array [0..0] of TKeySourceDesc =
  (
  );
}
  PaysuiteRowImport: array [0..0] of TRowImportDesc =
  (
    ( ExternalTable: 'E1'; CustomFunc: 'PS_E1_STD_HOURS_AND_STD_HOURS_SW')
  );
  //  ( ExternalTable: ''; CustomFunc: '')

  PaysuiteMap: array [0..1] of TMapRecord =
  (
    (ITable: 'E1'; IField: 'OT_STATUS'; ETable: 'EE'; EField: 'FLSA_EXEMPT'; MapType: mtMap; MapValues: sMapClarionBool2Char_Not ),
    (ITable: 'E1'; IField: 'W2_PENSION'; ETable: 'EE'; EField: 'W2_PENSION'; MapType: mtMap; MapValues: sMapClarionBool2Char )
  );
//  (ITable: 'E1'; IField: ''; ETable: 'EE'; EField: ''; MapType: mtDirect)


type
  TPaysuiteImportDef = class( TPayImportDefinition )
  public
    constructor Create(const AIEMap: IIEMap); override;
  published
    procedure PS_E1_STD_HOURS_AND_STD_HOURS_SW(const Input, Keys, Output: TFieldValues);
  end;

{TPaysuiteImportDef}
constructor TPaysuiteImportDef.Create(const AIEMap: IIEMap);
begin
  inherited Create(AIEMap);
  
  RegisterMap( PaysuiteMap );
  RegisterConvertDescs( PaysuiteConversion );
//  RegisterKeySourceDescs( PaysuiteKeys );
  RegisterRowImport( PaysuiteRowImport );
end;

procedure TPaysuiteImportDef.PS_E1_STD_HOURS_AND_STD_HOURS_SW(const Input, Keys, Output: TFieldValues);
begin
  if Input['E1.STD_HOURS_SW'] = CLARION_TRUE then
    Output.Add( 'EE', 'STANDARD_HOURS', Input['E1.STD_HOURS'] );
end;


const
  PaysuiteBindingDescs: array [0..1] of TBindingDesc = (
    ( Name: sLocalsBindingName;
      Caption: 'Mapped Locals';
      LeftDesc: ( Caption: 'Paysuite Locals'; Unique: true; KeyFields: 'Local'; ListFields: {State;}'Local');
      RightDesc: ( Caption: 'Evolution Company Locals'; Unique: false; KeyFields: 'SY_LOCALS_NBR'; ListFields: 'LocalState;LocalName')
    ),
    ( Name: sEDBindingName;
      Caption: 'Mapped E/Ds';
      LeftDesc: ( Caption: 'Paysuite E/Ds'; Unique: true; KeyFields: 'eord;code'; ListFields: 'eord;CODE;NAME;DESCRIPTION');
      RightDesc: ( Caption: 'Evolution Client E/Ds'; Unique: false; KeyFields: 'CUSTOM_E_D_CODE_NUMBER'; ListFields: 'CUSTOM_E_D_CODE_NUMBER;DESCRIPTION')
//      ExtraFields: 'object SCHEDULED: TstringField FieldNam e = ''SCHEDULED'' Size = 1 end'
    ){,
    ( Name: sSUIBindingName;
      Caption: 'Mapped SUI';
      LeftDesc: ( Caption: 'Paysuite SUI'; Unique: true; KeyFields: 'CODE'; ListFields: 'CODE');
      RightDesc: ( Caption: 'Evolution Company SUI'; Unique: false; KeyFields: 'SY_SUI_NBR'; ListFields: 'DESCRIPTION')
    ),
    ( Name: sEEOBindingName;
      Caption: 'Mapped EEO Codes';
      LeftDesc: ( Caption: 'Paysuite EEO Codes'; Unique: true; KeyFields: 'EEOC_CODE'; ListFields: 'EEOC_CODE');
      RightDesc: ( Caption: 'Evolution EEO Codes'; Unique: false; KeyFields: 'SY_HR_EEO_NBR'; ListFields: 'DESCRIPTION')
    ) }
  );

const
  paysuiteCO_AND_EEimport: TImportDesc = (
    ImportName: sPaySuiteEmployeeImport;
    RequiredMatchersNames: sLocalsBindingName
//                            + #13#10 + sEEOBindingName
                            + #13#10 + sEDBindingName;
    ExternalDataModuleImplClass: TDM_IM_PAYSUITE;
    ImportDefinitionImplClass: TPaysuiteImportDef
  );
  paysuitePRimport: TImportDesc = (
    ImportName: sPaySuitePayrollImport;
    RequiredMatchersNames: sEDBindingName
                            + #13#10 + sLocalsBindingName;
    ExternalDataModuleImplClass: TDM_IM_PAYSUITE;
    ImportDefinitionImplClass: TPaysuiteImportDef
  );

function CreatePaysuiteCO_AND_EEImport( aLogger: IImportLogger ): IImport;
begin
  Result := TPayCO_AND_EEImport.Create( aLogger, paysuiteCO_AND_EEimport, PaysuiteBindingDescs );
end;

function CreatePaysuitePRImport( aLogger: IImportLogger ): IImport;
begin
  Result := TPayPRImport.Create( aLogger, paysuitePRimport, PaysuiteBindingDescs );
end;


end.






