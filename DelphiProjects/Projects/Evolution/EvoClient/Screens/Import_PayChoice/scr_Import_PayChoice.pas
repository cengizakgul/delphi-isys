// Screens of "PayChoice Import"
unit scr_Import_PayChoice;

interface

uses
  sImportPaychoiceScr,
  sPrImportParam,
  SDM_IM_PAYSUITE,
  sEEImportParam,
  sPayImportDef,
  sPaysuiteImportDef,
  SPD_EDIT_IM_PAYSUITE_BASE,
  SPD_EDIT_IM_PAYSUITE_CO_AND_EE,
  SPD_EDIT_IM_PAYSUITE_PR;

implementation

end.
