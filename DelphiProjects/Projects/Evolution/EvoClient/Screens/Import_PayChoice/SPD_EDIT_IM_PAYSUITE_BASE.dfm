inherited EDIT_IM_PAYSUITE_BASE: TEDIT_IM_PAYSUITE_BASE
  inherited PageControl1: TevPageControl
    inherited TabSheet1: TTabSheet
      inherited evPanel1: TevPanel
        Width = 427
        Height = 238
        inherited evPanel5: TevPanel
          Top = 145
          Width = 427
          Height = 93
          inherited EvBevel2: TEvBevel
            Height = 421
          end
          inherited pnlSetup: TevPanel
            Width = 787
            Height = 421
            inherited EvBevel1: TEvBevel
              Width = 787
            end
            inherited pnlSetupControls: TevPanel
              Width = 787
              Height = 382
            end
          end
        end
        inherited pnlConnectionInputHolder: TevPanel
          Width = 427
          Height = 145
          inline ConnectionStringInput: TImportSystemAndUserDBPathsInputFrame
            Left = 2
            Top = 2
            Width = 423
            Height = 141
            Align = alClient
            TabOrder = 0
            inherited evPanel3: TevPanel
              Width = 423
              Height = 141
              inherited evBitBtn1: TevBitBtn
                Width = 105
              end
              inherited evBitBtn2: TevBitBtn
                Width = 105
              end
            end
          end
        end
      end
    end
    inherited TabSheet5: TTabSheet
      inherited viewerFrame: TTableSpaceViewFrame
        inherited evPanel2: TevPanel
          inherited dgView: TevDBGrid
            Width = 918
            Height = 529
            PaintOptions.AlternatingRowColor = 14544093
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited LogFrame: TImportLoggerViewFrame
        inherited pnlLog: TevPanel
          inherited pgLogView: TevPageControl
            Width = 447
            Height = 165
          end
          inherited LoggerRichView: TImportLoggerRichViewFrame
            inherited pnlUserView: TevPanel
              inherited pnlBottom: TevPanel
                Height = 203
                inherited evSplitter2: TevSplitter
                  Height = 203
                end
                inherited pnlLeft: TevPanel
                  Height = 203
                  inherited mmDetails: TevMemo
                    Height = 178
                  end
                end
                inherited pnlRight: TevPanel
                  Height = 203
                  inherited mmContext: TevMemo
                    Height = 178
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  inherited OpenDialog2: TOpenDialog
    Top = 64
  end
end
