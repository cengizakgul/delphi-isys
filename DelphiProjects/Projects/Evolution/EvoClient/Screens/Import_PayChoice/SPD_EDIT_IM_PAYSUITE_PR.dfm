inherited EDIT_IM_PAYSUITE_PR: TEDIT_IM_PAYSUITE_PR
  inherited PageControl1: TevPageControl
    ActivePage = TabSheet4
    inherited TabSheet1: TTabSheet
      inherited evPanel1: TevPanel
        inherited evPanel5: TevPanel
          Visible = True
          inherited EvBevel2: TEvBevel
            Height = 89
          end
          inherited pnlSetup: TevPanel
            Width = 296
            Height = 89
            inherited EvBevel1: TEvBevel
              Width = 296
            end
            inherited pnlSetupControls: TevPanel
              Width = 296
              Height = 50
              inline ParamInput: TPrImportParamFrame
                Left = 11
                Top = 13
                Width = 294
                Height = 92
                TabOrder = 0
              end
            end
          end
          inherited evBitBtn8: TevBitBtn
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888800000
              0000D88888D888888888848747707FFF7FF0888D8DD8FDDDFDD8448747707FF0
              7FF0888D8DD8FDD8FDD8448777707F707770888DDDD8FDF8FFF8444444407000
              00F0888888D8F88888D8444444407FF07FF0888888D8FDD8FDD8477778807FF0
              FFF08DDDDDD8FDD8DDD847FF9990777777708DDD88D8FFFFFFF847FF99900000
              00008DDD88D88888888847FF9991AAA2EEE68DDD88DDDDDDDDDD47FF9991AAA2
              EEE68DDD888D888D888D88889991AAA26666888D888D888DDDDDDDDD1111AAA2
              8888DDDDDDDD888DDDDDDDDDD888AAA28DDDDDDDDDDD888DDDDDDDDDDDDDAAA2
              8DDDDDDDDDDD888DDDDDDDDDDDDD22228DDDDDDDDDDDDDDDDDDD}
            NumGlyphs = 2
          end
          inherited evBitBtn2: TevBitBtn
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
              DDDDDFFFFFFFFFFDDDDD000000000008DDDD88888888888FDDDD0B0333333330
              8DDD8D8888888888FDDD0B903333333308DD8DD8888888888FDD0B9903333333
              308D8D8D8888888888FD0B999033333333088D88D8888888888F0B9991000000
              00008D888D88888888880B9991AAA2EEE68D8D888DDDDDDDDDDD0B9991AAA2EE
              E68D8D888D888D888D8DD09991AAA2EEE68DDD888D888D888D8DDD9991AAA226
              668DDD888D888DDDDD8DDD1111AAA288888DDDDDDD888D88888DDDD888AAA28D
              DDDDDDD88D888D8DDDDDDDDDDDAAA28DDDDDDDDDDD888D8DDDDDDDDDDD22228D
              DDDDDDDDDDDDDD8DDDDDDDDDDDD8888DDDDDDDDDDDD8888DDDDD}
            NumGlyphs = 2
          end
          inherited evBitBtn5: TevBitBtn
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDD4488666666668DDDFFDDFFFFFFFFDDDDC466FFFFFFF6
              668D8DFF8888888FFFDDC4FFFFFF666777688D888888FFF888FDC466FFF60000
              000D8D8F888FDDDDDDDDC4D76667FF2FFF688DD8FFF888D888D844DF771FFF2F
              FF68DDD888D888D888D8DDDFFF1FFF2FFF68DDDDD8D888D888D8DDDFC71FFF2F
              FF68DDD8F8D888D888D8DDDFC71FFF2FFF68DDD8F8D888D888D8FCDFC7FC7F26
              66688FD8FD8F88DDDDD8DFC77FC7FF288888D8FDD8F888D8D888DDDD111FFF28
              DDDDDDDDDDD888D8DDDDDFCFCFC7FF28DDDDD8F8F8F888D8DDDDFCDFCDFC2228
              DDDD8FD8FD8FDDD8DDDDDDDFCDDD8888DDDDDDD8FDDD8888DDDD}
          end
        end
        inherited pnlConnectionInputHolder: TevPanel
          inherited ConnectionStringInput: TImportSystemAndUserDBPathsInputFrame
            inherited evPanel3: TevPanel
              inherited bbSystemDB: TevBitBtn
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000008888
                  8888DDDDDDDD88888888D003333008877778DFFFFFFDD88DDDD83BBBBBB38887
                  7778F888888FD88DDDD8BB0000BB0007777888FDDD88FDDDDDD8BB3008888800
                  777888FDD8DDD8DDDDD8BB3300338800777888FFFFFFD8DDDDD8BBBBBBBB0007
                  777888888888FDDDDDD8BB3D88BB3887777888FDDD88F88DDDD8BB3D88BB3887
                  777888FD8D88F88DDDD8BB3333BB3887777888FFFF88F88DDDD8DBBBBBB37887
                  7778D888888FD88DDDD8DDDD887778877778DDDDDDDDD88DDDD8DDDD86666887
                  7778DDDD8DDDD88DDDD8DDDD888888877778DDDD8888888DDDD8DDDDDDDDD866
                  6668DDDDDDDDD8DDDDD8DDDDDDDDD8888888DDDDDDDDD8888888}
              end
              inherited bbClientDB: TevBitBtn
                Left = 0
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                  8888DFFFFFFFFFFD8888CCCCCCCCCC8777788888888888FDDDD8CCCCCFCCC887
                  777888888D888FDDDDD8DCCCFFCC88877778D888DD88FD8DDDD8DDCB33C87887
                  7778DD8DFF8FD88DDDD8DDDBBB8778877778DDFDDDFDD88DDDD8DD4BB1B87887
                  7778DD8DD8DFD88DDDD8D1BBBBBB88877778D8DDDDDDF88DDDD811BBBBBB8887
                  777888DDDDDDFD8DDDD8111B4B4B18877778888D8D8D8FDDDDD8111BBBBB1887
                  7778888DDDDD8FDDDDD81111BBB1188777788888DDD88FDDDDD8111111116887
                  777888888888FD8DDDD88111111888877778D888888FD88DDDD8D81111DDD866
                  6668DD8888FDD8DDDDD8DDDDDDDDD8888888DDDDDDDDD8888888}
              end
              inherited evBitBtn1: TevBitBtn
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD000000000
                  0776DDDFFFFFFFFFFDDD0002222206620000FFF88888F888FFFF222AAAAA2EE6
                  666688F88888F8888888AA2AAA000EE6EEEE88D888FFF8888888DDDAAA2EEEE6
                  0778DDD888F88888FDDDDDD2220666667778DDD888D88888DDD8DDDD88777887
                  7778DDDDDDDDDDDDDDD8DDDD887778877778DDDD88DDD88DDDD8DDDD88777887
                  7778DDDD88DDD88DDDD8DDDD887778877778DDDD88DDD88DDDD8DDDD88777887
                  7778DDDD88DDD88DDDD8DDDD887778877778DDDD88DDD88DDDD8DDDD86666887
                  7778DDDD8DDDD88DDDD8DDDD888888877778DDDD8888888DDDD8DDDDDDDDD866
                  6668DDDDDDDDD8DDDDD8DDDDDDDDD8888888DDDDDDDDD8888888}
              end
              inherited evBitBtn2: TevBitBtn
                Glyph.Data = {
                  76010000424D7601000000000000760000002800000020000000100000000100
                  0400000000000001000000000000000000001000000000000000000000000000
                  80000080000000808000800000008000800080800000C0C0C000808080000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000007800
                  00D6DDFFFFFFDDFFFFDD0033333378666600FF88888FDD888FFF33BBBBB3706E
                  E6668F88888DDD888888B3BBB033666EE6EE8D888FDDFF888888DDBBB3776EEE
                  E606DD888FDD88888FDDDD33337766666676DD888DDD88888DD8DDDD88777887
                  7776DDDDDDDDDDDDDDD8DDDD667776677776DDDD88DDD88DDDD8DDDD66777667
                  7776DDDD88DDD88DDDD8DDDD667776677776DDDD88DDD88DDDD8DDDD66777667
                  7776DDDD88DDD88DDDD8DDDD667776677776DDDD88DDD88DDDD8DDDD6FFFF667
                  7776DDDD8DDDD88DDDD8DDDD666666677776DDDD8888888DDDD8DDDDDDDDD6FF
                  FFF6DDDDDDDDD8DDDDD8DDDDDDDDD6666666DDDDDDDDD8888888}
              end
            end
          end
        end
      end
    end
    inherited TabSheet5: TTabSheet
      inherited viewerFrame: TTableSpaceViewFrame
        inherited evPanel2: TevPanel
          inherited dgView: TevDBGrid
            Width = 759
            Height = 410
          end
        end
      end
    end
    object TabSheet4: TTabSheet [2]
      Caption = 'Locals Mapping'
      ImageIndex = -1
      inline BinderFrame2: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 0
          end
          inherited pnlTopLeft: TevPanel
            Height = 0
            inherited dgLeft: TevDBGrid
              Height = 162
              PaintOptions.AlternatingRowColor = 14544093
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 140
            Height = 0
            inherited evPanel4: TevPanel
              Width = 140
            end
            inherited dgRight: TevDBGrid
              Width = 140
              Height = 162
              PaintOptions.AlternatingRowColor = 14544093
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 410
            inherited evPanel5: TevPanel
              Width = 410
            end
            inherited dgBottom: TevDBGrid
              Width = 410
              PaintOptions.AlternatingRowColor = 14544093
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 410
          end
        end
      end
    end
    object TabSheet3: TTabSheet [3]
      Caption = 'E/D Mapping'
      ImageIndex = 3
      inline BinderFrame1: TBinderFrame
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        TabOrder = 0
        inherited evSplitter2: TevSplitter
          Top = 0
          Width = 410
        end
        inherited pnltop: TevPanel
          Width = 410
          Height = 0
          inherited evSplitter1: TevSplitter
            Height = 196
          end
          inherited pnlTopLeft: TevPanel
            Height = 196
            inherited dgLeft: TevDBGrid
              Height = 171
              PaintOptions.AlternatingRowColor = 14544093
            end
          end
          inherited pnlTopRight: TevPanel
            Width = 426
            Height = 196
            inherited evPanel4: TevPanel
              Width = 426
            end
            inherited dgRight: TevDBGrid
              Width = 426
              Height = 171
              PaintOptions.AlternatingRowColor = 14544093
            end
          end
        end
        inherited evPanel1: TevPanel
          Top = 5
          Width = 410
          inherited pnlbottom: TevPanel
            Width = 696
            inherited evPanel5: TevPanel
              Width = 696
            end
            inherited dgBottom: TevDBGrid
              Width = 696
              PaintOptions.AlternatingRowColor = 14544093
            end
          end
          inherited pnlMiddle: TevPanel
            Width = 696
          end
        end
      end
    end
    inherited TabSheet2: TTabSheet
      inherited LogFrame: TImportLoggerViewFrame
        inherited pnlLog: TevPanel
          inherited pgLogView: TevPageControl
            Width = 446
            Height = 153
          end
          inherited LoggerRichView: TImportLoggerRichViewFrame
            inherited pnlUserView: TevPanel
              inherited pnlBottom: TevPanel
                Height = 84
                inherited evSplitter2: TevSplitter
                  Height = 84
                end
                inherited pnlLeft: TevPanel
                  Height = 84
                  inherited mmDetails: TevMemo
                    Height = 59
                  end
                end
                inherited pnlRight: TevPanel
                  Height = 84
                  inherited mmContext: TevMemo
                    Height = 59
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  inherited evActionList1: TevActionList
    Left = 400
    Top = 104
  end
  inherited SaveDialog1: TSaveDialog
    Left = 332
    Top = 40
  end
end
