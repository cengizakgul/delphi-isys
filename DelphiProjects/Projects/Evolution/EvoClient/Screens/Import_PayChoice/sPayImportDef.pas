// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sPayImportDef;
{$include gdy.inc}

//postprocess
//  pr check lines
// Rate_of_Pay - ���� Amount  �� Hours_or_pieces, ���� ��������� - �� ����. �������� ������ ��� Earnings
// net = wages - earnings (tips)

interface
uses
  gdyBinder, genericImport, genericImportDefinitionImpl, sImportHelpers, EvContext,
   classes, db, genericImportBaseImpl, StrUtils, sImportVisualController,
  evExceptions, IEMap, EvCOmmonInterfaces, EvDataset, EvUIUtils, EvClientDataSet;

const
  sParamCheckDate = 'PR.CHECK_DATE';
  sParamBatchFrequency = 'PR_BATCH.FREQUENCY';
  sEDTargetAction = 'EE_SCHEDULED_E_DS.TARGET_ACTION';
  sParamPrAmountField = 'PrAmountField';
  sHasDBDT = 'HasDBDT';

const
  sLocalsBindingName = 'Locals';
  sEDBindingName = 'EDs';
//  sSUIBindingName = 'SUI';
//  sEEOBindingName = 'EEO';

const
  sHiddenDivisionCode='     Hidden Division';

type
  TEDAutoMapper = class(TInterfacedObject, IAutoMapper )
  private
    function MapToRightKeys( aLeftTable: TDataSet; bd: TBindingDesc; aRightTable: TDataSet ): Variant;
  end;

  TPayImportDefinition = class(TImportDefinition)
  protected
    CheckLineNbr: Integer;
    procedure HelpImportSTATE_MARITAL_STATUS(const Input, Keys, Output: TFieldValues; target_state: string );
    function SetCustomEDCodeKeyForSchedEDS( Keys: TFieldValues; eord, code: string ): boolean; //returns success indicator
    procedure HelpImportDDReference( Output: TFieldValues; aba, act, atype: Variant );
  public
    constructor Create(const AIEMap: IIEMap); override;
    destructor Destroy; override;
  published
    procedure E1_payroll_fixup_ALL(const Input, Keys, Output: TFieldValues);
    procedure EB_np_ALL(const Input, Keys, Output: TFieldValues);
    procedure E1_FEDERAL_MARITAL_STATUS(const Input, Keys, Output: TFieldValues);
    procedure E1_FEDERAL_MARITAL_STATUS_to_SUI_STATE(const Input, Keys, Output: TFieldValues);
    procedure E1__TO__HOME_STATE_MARITAL_STATUS(const Input, Keys, Output: TFieldValues);
    procedure E1__TO__SUI_STATE_MARITAL_STATUS(const Input, Keys, Output: TFieldValues);
    procedure E1_PR_STATUS(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure E1_CDV_CODE_AND_CDN_CODE(const Input, Keys, Output: TFieldValues);
    procedure E1_PAY_FREQ(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);
    procedure E1_STATUS(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);
    procedure E1_NEXTRAISE_DT(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);
    procedure E1_HIRE_DT(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);
    procedure E1_STATE(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);
    procedure E1_EEOC_CODE(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
    procedure ET_ALL(const Input, Keys, Output: TFieldValues);
    procedure EA_ALL(const Input, Keys, Output: TFieldValues);
    procedure EA_checklines_ALL(const Input, Keys, Output: TFieldValues);
    procedure EA_EY_state_ALL(const Input, Keys, Output: TFieldValues);
    procedure EA_EY_local_ALL(const Input, Keys, Output: TFieldValues);
    procedure EA_EY_sui_ALL(const Input, Keys, Output: TFieldValues);
    procedure ED_ALL(const Input, Keys, Output: TFieldValues);
    procedure EB_IN_PRENOTE(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);

    procedure AI_CL_E_DS(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_SUI(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_BRANCH(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_DEPARTMENT(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_DIVISION(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_LOCAL_TAX(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CO_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_CL_PERSON(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_LOCALS(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_RATES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_BATCH(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_LINES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_LOCALS(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_PR_CHECK_SUI(const ds: TevClientDataSet; const Keys: TFieldValues );
    procedure AI_EE_SCHEDULED_E_DS(const ds: TevClientDataSet; const Keys: TFieldValues );
  end;

  TPayCO_AND_EEImport = class (TImportBase)
  protected
    procedure DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings ); override;
  end;

  TPayPRImport = class (TImportBase)
  protected
    procedure DoRunImport( aImportDefinition: IImportDefinition; aParams: TStrings ); override;
  end;

implementation

uses
  sysutils, EvConsts, sDataStructure, gdystrset, controls,
  evutils{$ifdef D6_UP},variants{$endif}, gdycommon, DateUtils;

function ConvertAddress( const aLogger: IImportLogger; const Value: Variant ): Variant;
begin
  if not VarIsNull( Value ) then
  begin
    Result := CapitalizeWords(VarToStr(Value));
    Result := StringReplace(Result + ' ', 'Sw ', 'SW ', [rfReplaceAll]);
    Result := StringReplace(Result, 'Nw ', 'NW ', [rfReplaceAll]);
    Result := StringReplace(Result, 'Se ', 'SE ', [rfReplaceAll]);
    Result := Trim(StringReplace(Result, 'Ne ', 'NE ', [rfReplaceAll]));
  end
  else
    Result := Value;
end;

function ConvertExTaxType( const aLogger: IImportLogger; const Value: Variant ): Variant;
var
  ettstr: string;
begin
  if not VarIsNull( Value ) then
  begin
    ettstr := AnsiUpperCase(trim(VarToStr(Value)));
    Result := Null;
    if Length(ettstr) > 0 then
    begin
      if Length(ettstr) = 1 then
        case ettstr[1] of
          'E': Result := OVERRIDE_VALUE_TYPE_ADDITIONAL_AMOUNT;
          '%': Result := OVERRIDE_VALUE_TYPE_ADDITIONAL_PERCENT;
          'F': Result := OVERRIDE_VALUE_TYPE_REGULAR_AMOUNT;
          'P': Result := OVERRIDE_VALUE_TYPE_REGULAR_PERCENT;
          'W': Result := OVERRIDE_VALUE_TYPE_NONE;
        end
    end
    else
      Result := OVERRIDE_VALUE_TYPE_NONE;
    if VarIsNull(Result) then
    begin
      Result := OVERRIDE_VALUE_TYPE_NONE;
      aLogger.LogWarningFmt( 'Unknown value <%s> - converted to OVERRIDE_VALUE_TYPE_NONE', [ettstr] );
    end;
  end
  else
    Result := OVERRIDE_VALUE_TYPE_NONE;
end;

function ConvertZip( const aLogger: IImportLogger; const Value: Variant ): Variant;
var
  zip: string;
  s: string;
  i: integer;
  function zerotail: boolean;
  var
    i: integer;
  begin
    Result := true;
    for i := 6 to Length(zip) do
      if zip[i] <> '0' then
      begin
        Result := false;
        break;
      end
  end;
begin
  zip := '';
  if not VarIsNull( Value ) then
  begin
    s := VarToStr(Value);
    for i := 1 to Length(s) do
      if s[i] in ['0'..'9'] then
        zip := zip + s[i];
    if Length(zip) <= 5 then
      Result := PadStringLeft( zip, '0', 5 )
    else
    begin
      zip := PadStringLeft( zip, '0', 9 );
      if zerotail then
        Result := copy( zip, 1, 5 )
      else
        Result := Format('%s%s%s%s%s-%s', [zip[1],zip[2],zip[3],zip[4],zip[5],copy(zip,6,9999) ]);
    end
  end
  else
    Result := Value;
end;

function ConvertBankABA( const aLogger: IImportLogger; const Value: Variant ): Variant;
var
  aba: string;
begin
  if not VarIsNull( Value ) then
  begin
    aba := PadStringLeft( trim(VarToStr(Value)), '0', 9 );
    if HashTotalABAOK(aba) = 0 then
      Result := aba
    else
      raise EevException.Create( 'Invalid ABA code: <'+VarToStr(Value)+'>' );
  end
  else
    Result := Value;
end;


{
function ConvertBankABA( const Value: Variant ): Variant;
var
  aba: string;
begin
  if not VarIsNull( Value ) then
  begin
    aba := trim(VarToStr(Value));
    if Length(aba) = 8 then
      aba := aba + inttostr( HashTotalABA(aba) );
    if Length(aba) = 0 then
      Result := Null
    else if (Length(aba) = 9) and (HashTotalABAOK(aba) = 0) then
      Result := aba
    else
      raise EevException.Create( 'Invalid ABA code: <'+aba+'>' );
  end
  else
    Result := Value;
end;
}
const

  PayConversion: array [0..26] of TConvertDesc =
  (
    (Table: 'CV'; Field:'CODE'; Func: ConvertDBDTCustomNumber),
    (Table: 'CP'; Field:'CODE'; Func: ConvertDBDTCustomNumber),
    (Table: 'CP'; Field:'CDV_CODE'; Func: ConvertDBDTCustomNumber),
    (Table: 'CV'; Field:'BNA_NAME'; Func: ConvertName),  //It seems that this field exists in the Paychoice only
    (Table: 'CP'; Field:'NAME'; Func: ConvertName),

    (Table: 'E1'; Field:'CDV_CODE'; Func: ConvertDBDTCustomNumber),
    (Table: 'E1'; Field:'CDN_CODE'; Func: ConvertDBDTCustomNumber),
    (Table: 'E1'; Field:'EMP_NUM'; Func: ConvertEECustomNumber),
    (Table: 'E1'; Field:'FED_DEP'; Func: ConvertToCardinal),
    (Table: 'E1'; Field:'STATE_DEP'; Func: ConvertToCardinal),
    (Table: 'E1'; Field:'SS_NUM'; Func: ConvertSSN),
    (Table: 'E1'; Field:'ZIP'; Func: ConvertZip),
    (Table: 'E1'; Field:'PHONE'; Func: ConvertPhone),
    (Table: 'E1'; Field:'F_NAME'; Func: ConvertName),
    (Table: 'E1'; Field:'L_NAME'; Func: ConvertName),
    (Table: 'E1'; Field:'TITLE'; Func: ConvertName),
    (Table: 'E1'; Field:'ADDRESS_1'; Func: ConvertAddress),
    (Table: 'E1'; Field:'ADDRESS_2'; Func: ConvertAddress),
    (Table: 'E1'; Field:'CITY'; Func: ConvertName),

    (Table: 'E1_fixup'; Field:'EMP_NUM'; Func: ConvertEECustomNumber),
    (Table: 'E1_fixup'; Field:'SS_NUM'; Func: ConvertSSN),
    (Table: 'E1_payroll_fixup'; Field:'EMP_NUM'; Func: ConvertEECustomNumber),

    (Table: 'ET'; Field: 'EX_TAX_TYPE'; Func: ConvertExTaxType),
    (Table: 'S10_DB'; Field: 'BANK_ABA'; Func: ConvertBankABA),
    (Table: 'EB'; Field: 'S10_BANK_ABA'; Func: ConvertBankABA),
    (Table: 'EB_np'; Field: 'S10_BANK_ABA'; Func: ConvertBankABA),
    (Table: 'ED'; Field: 'EB_S10_BANK_ABA'; Func: ConvertBankABA)
  );
//    (Table: ''; Field:'', Func: )


  PayKeys: array [0..78] of TKeySourceDesc =
  (
    (ExternalTable: 'CV'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'CV'; EvoKeyTable: 'CO_DIVISION'; EvoKeyField: 'CUSTOM_DIVISION_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'CV'; EvoKeyTable: 'CO_BRANCH'; EvoKeyField: 'CUSTOM_BRANCH_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'CODE' ),

    (ExternalTable: 'CP'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'CP'; EvoKeyTable: 'CO_DIVISION'; EvoKeyField: 'CUSTOM_DIVISION_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'CP'; EvoKeyTable: 'CO_BRANCH'; EvoKeyField: 'CUSTOM_BRANCH_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'CDV_CODE' ),
    (ExternalTable: 'CP'; EvoKeyTable: 'CO_DEPARTMENT'; EvoKeyField: 'CUSTOM_DEPARTMENT_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'CODE' ),

    (ExternalTable: 'E1'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'E1'; EvoKeyTable: 'CL_PERSON'; EvoKeyField: 'SOCIAL_SECURITY_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'SS_NUM' ),
    (ExternalTable: 'E1'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'EMP_NUM' ),
    (ExternalTable: 'E1'; ExternalField: 'WORK_STATE'; EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksFromThisTable; ValueSourceField: 'LIVE_STATE' ),
    (ExternalTable: 'E1'; ExternalField: 'LIVE_STATE'; EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksFromThisTable; ValueSourceField: 'LIVE_STATE' ),
    (ExternalTable: 'E1'; ExternalField: 'STATE_DEP';  EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksFromThisTable; ValueSourceField: 'LIVE_STATE' ),
    (ExternalTable: 'E1'; ExternalField: 'STATE_DEP@1';  EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksFromThisTable; ValueSourceField: 'WORK_STATE' ),

    (ExternalTable: 'E1_fixup'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'E1_fixup'; EvoKeyTable: 'CL_PERSON'; EvoKeyField: 'SOCIAL_SECURITY_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'SS_NUM' ),
    (ExternalTable: 'E1_fixup'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'EMP_NUM' ),

    (ExternalTable: 'ET'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'ET'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'EMP_NUM'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),
    (ExternalTable: 'ET'; EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksLookup; ValueSourceField: 'LIVE_STATE'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),

    (ExternalTable: 'EA'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'EMP_NUM'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),
    (ExternalTable: 'EA'; EvoKeyTable: 'PR'; EvoKeyField: 'CHECK_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA'; EvoKeyTable: 'PR'; EvoKeyField: 'RUN_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_BEGIN_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_END_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'FREQUENCY'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA'; EvoKeyTable: 'PR_CHECK'; EvoKeyField: 'CHECK_TYPE'; HowToGet: ksFixedValue ),

    (ExternalTable: 'EA_checklines'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_checklines'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'EMP_NUM'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),
    (ExternalTable: 'EA_checklines'; EvoKeyTable: 'PR'; EvoKeyField: 'CHECK_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_checklines'; EvoKeyTable: 'PR'; EvoKeyField: 'RUN_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_checklines'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_BEGIN_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_checklines'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_END_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_checklines'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'FREQUENCY'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_checklines'; EvoKeyTable: 'PR_CHECK'; EvoKeyField: 'CHECK_TYPE'; HowToGet: ksFixedValue ),

    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'EMP_NUM'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),
    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'PR'; EvoKeyField: 'CHECK_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'PR'; EvoKeyField: 'RUN_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_BEGIN_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_END_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'FREQUENCY'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'PR_CHECK'; EvoKeyField: 'CHECK_TYPE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksFromThisTable; ValueSourceField: 'TAX_TABLE' ),
    (ExternalTable: 'EA_EY_state'; EvoKeyTable: 'EE_STATES'; HowToGet: ksEmpty ),

    //SY_LOCALS and EE_LOCALS keys are set up in rowImportFunc
    (ExternalTable: 'EA_EY_local'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_local'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'EMP_NUM'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),
    (ExternalTable: 'EA_EY_local'; EvoKeyTable: 'PR'; EvoKeyField: 'CHECK_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_local'; EvoKeyTable: 'PR'; EvoKeyField: 'RUN_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_local'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_BEGIN_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_local'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_END_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_local'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'FREQUENCY'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_local'; EvoKeyTable: 'PR_CHECK'; EvoKeyField: 'CHECK_TYPE'; HowToGet: ksFixedValue ),

    //CO_SUI keys are set up in rowImportFunc
    (ExternalTable: 'EA_EY_sui'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_sui'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'EMP_NUM'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),
    (ExternalTable: 'EA_EY_sui'; EvoKeyTable: 'PR'; EvoKeyField: 'CHECK_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_sui'; EvoKeyTable: 'PR'; EvoKeyField: 'RUN_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_sui'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_BEGIN_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_sui'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_END_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_sui'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'FREQUENCY'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EA_EY_sui'; EvoKeyTable: 'PR_CHECK'; EvoKeyField: 'CHECK_TYPE'; HowToGet: ksFixedValue ),

    (ExternalTable: 'EB'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EB'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'EMP_NUM'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),
    (ExternalTable: 'EB'; EvoKeyTable: 'EE_DIRECT_DEPOSIT'; EvoKeyField: 'EE_ABA_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'BANK_ABA'; KeyField: 'S10_ID'; LookupTable: 'S10_DB'; LookupKeyField: 'S10_ID' ),
    (ExternalTable: 'EB'; EvoKeyTable: 'EE_DIRECT_DEPOSIT'; EvoKeyField: 'EE_BANK_ACCOUNT_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'ACT_NUM' ),
    (ExternalTable: 'EB'; EvoKeyTable: 'EE_DIRECT_DEPOSIT'; EvoKeyField: 'EE_BANK_ACCOUNT_TYPE'; HowToGet: ksFromThisTable; ValueSourceField: 'CHECK_SAVE' ),

    //CL_E_DS keys are set up in rowImportFunc
    (ExternalTable: 'ED'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'ED'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'EMP_NUM'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),

    //CL_E_DS keys are set up in rowImportFunc
    (ExternalTable: 'EB_np'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'EB_np'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksLookup; ValueSourceField: 'EMP_NUM'; KeyField: 'E1_ID'; LookupTable: 'E1'; LookupKeyField: 'E1_ID' ),

    //EE_STATES key is set in rowImportFunc
    (ExternalTable: 'E1_payroll_fixup'; EvoKeyTable: 'CO'; EvoKeyField: 'CUSTOM_COMPANY_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'E1_payroll_fixup'; EvoKeyTable: 'EE'; EvoKeyField: 'CUSTOM_EMPLOYEE_NUMBER'; HowToGet: ksFromThisTable; ValueSourceField: 'EMP_NUM' ),
    (ExternalTable: 'E1_payroll_fixup'; EvoKeyTable: 'PR'; EvoKeyField: 'CHECK_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'E1_payroll_fixup'; EvoKeyTable: 'PR'; EvoKeyField: 'RUN_NUMBER'; HowToGet: ksFixedValue ),
    (ExternalTable: 'E1_payroll_fixup'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_BEGIN_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'E1_payroll_fixup'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'PERIOD_END_DATE'; HowToGet: ksFixedValue ),
    (ExternalTable: 'E1_payroll_fixup'; EvoKeyTable: 'PR_BATCH'; EvoKeyField: 'FREQUENCY'; HowToGet: ksFixedValue ),
    (ExternalTable: 'E1_payroll_fixup'; EvoKeyTable: 'PR_CHECK'; EvoKeyField: 'CHECK_TYPE'; HowToGet: ksFixedValue )
  );

//    (ITable: 'E1'; IField: 'M_STATUS_S'; ETable: 'EE_STATES'; EField: 'STATE_MARITAL_STATUS'; MapType: mtCustom; FuncName: 'E1_'
//    (ExternalTable: 'E1'; ExternalField: 'M_STATUS_S'; EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksFromThisTable; ValueSourceField: 'LIVE_STATE' ),
//    (ExternalTable: 'E1'; ExternalField: 'M_STATUS_S@1'; EvoKeyTable: 'CO_STATES'; EvoKeyField: 'STATE'; HowToGet: ksFromThisTable; ValueSourceField: 'WORK_STATE' ),

  PayRowImport: array [0..13] of TRowImportDesc =
  (
    ( ExternalTable: 'E1'; CustomFunc: 'E1_FEDERAL_MARITAL_STATUS'),
    ( ExternalTable: 'E1'; CustomFunc: 'E1_FEDERAL_MARITAL_STATUS_to_SUI_STATE'),
    ( ExternalTable: 'E1'; CustomFunc: 'E1__TO__HOME_STATE_MARITAL_STATUS'),
    ( ExternalTable: 'E1'; CustomFunc: 'E1__TO__SUI_STATE_MARITAL_STATUS'),
    ( ExternalTable: 'E1'; CustomFunc: 'E1_CDV_CODE_AND_CDN_CODE'),
    ( ExternalTable: 'ED'; CustomFunc: 'ED_ALL'),
    ( ExternalTable: 'ET'; CustomFunc: 'ET_ALL'),
    ( ExternalTable: 'EA'; CustomFunc: 'EA_ALL'),
    ( ExternalTable: 'EA_checklines'; CustomFunc: 'EA_checklines_ALL'),
    ( ExternalTable: 'EA_EY_state'; CustomFunc: 'EA_EY_state_ALL'),
    ( ExternalTable: 'EA_EY_local'; CustomFunc: 'EA_EY_local_ALL'),
    ( ExternalTable: 'EA_EY_sui'; CustomFunc: 'EA_EY_sui_ALL'),
    ( ExternalTable: 'EB_np'; CustomFunc: 'EB_np_ALL'),
    ( ExternalTable: 'E1_payroll_fixup'; CustomFunc: 'E1_payroll_fixup_ALL')
  );
  //  ( ExternalTable: ''; CustomFunc: '')

  PayMap: array [0..41] of TMapRecord =
  (
    (ITable: 'CV'; IField: 'CODE'; ETable: 'CO_BRANCH'; EField: 'CUSTOM_BRANCH_NUMBER'; MapType: mtDirect ),
    (ITable: 'CV'; IField: 'BNA_NAME'; ETable: 'CO_BRANCH'; EField: 'NAME'; MapType: mtDirect ), //It seems that this field exists in the Paychoice only

    (ITable: 'CP'; IField: 'CODE'; ETable: 'CO_DEPARTMENT'; EField: 'CUSTOM_DEPARTMENT_NUMBER';  MapType: mtDirect ),
    (ITable: 'CP'; IField: 'NAME'; ETable: 'CO_DEPARTMENT'; EField: 'NAME';  MapType: mtDirect ),

    //CL_PERSON
    (ITable: 'E1'; IField: 'SS_NUM'; ETable: 'CL_PERSON'; EField: 'SOCIAL_SECURITY_NUMBER'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'L_NAME'; ETable: 'CL_PERSON'; EField: 'LAST_NAME'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'F_NAME'; ETable: 'CL_PERSON'; EField: 'FIRST_NAME'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'M_NAME'; ETable: 'CL_PERSON'; EField: 'MIDDLE_INITIAL'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'SEX'; ETable: 'CL_PERSON'; EField: 'GENDER'; MapType: mtMap;
                                  MapValues: 'M=' + GROUP_BOX_MALE + ',' +
                                             'F=' + GROUP_BOX_FEMALE + ',' +
                                             'N=' + GROUP_BOX_UNKOWN ),
    (ITable: 'E1'; IField: 'BIRTH_DT'; ETable: 'CL_PERSON'; EField: 'BIRTH_DATE'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'ADDRESS_1'; ETable: 'CL_PERSON'; EField: 'ADDRESS1'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'ADDRESS_2'; ETable: 'CL_PERSON'; EField: 'ADDRESS2'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'CITY'; ETable: 'CL_PERSON'; EField: 'CITY'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'STATE'; MapType: mtCustom; FuncName: 'E1_STATE'),
    (ITable: 'E1'; IField: 'ZIP'; ETable: 'CL_PERSON'; EField: 'ZIP_CODE'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'PHONE'; ETable: 'CL_PERSON'; EField: 'PHONE1'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'RACE_CODE'; ETable: 'CL_PERSON'; EField: 'ETHNICITY'; MapType: mtMap;
                                  MapValues: 'W=' + ETHNIC_CAUCASIAN
                                              + ',B=' + ETHNIC_AFRICAN
                                              + ',A=' + ETHNIC_ASIAN
                                              + ',I=' + ETHNIC_INDIAN
                                              + ',N=' +  ETHNIC_NOT_APPLICATIVE
                                             ), //ETHNIC_HISPANIC, ETHNIC_OTHER
    (ITable: 'E1'; IField: 'EEOC_CODE'; MapType: mtCustom; FuncName: 'E1_EEOC_CODE' ),
    //  EE
    (ITable: 'E1'; IField: 'EMP_NUM'; ETable: 'EE'; EField: 'CUSTOM_EMPLOYEE_NUMBER'; MapType: mtDirect ),

    (ITable: 'E1'; IField: 'FED_DEP'; ETable: 'EE'; EField: 'NUMBER_OF_DEPENDENTS'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'CLOCK_NUM'; ETable: 'EE'; EField: 'TIME_CLOCK_NUMBER'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'HIRE_DT'; MapType: mtCustom; FuncName: 'E1_HIRE_DT' ),
    (ITable: 'E1'; IField: 'STATUS'; MapType: mtCustom; FuncName: 'E1_STATUS'),
    (ITable: 'E1'; IField: 'TERM_DT'; ETable: 'EE'; EField: 'CURRENT_TERMINATION_DATE'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'SALARY'; ETable: 'EE'; EField: 'SALARY_AMOUNT'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'PR_STATUS'; MapType: mtCustom; FuncName: 'E1_PR_STATUS'),
    (ITable: 'E1'; IField: 'LASTRAISE_DT'; ETable: 'EE'; EField: 'REVIEW_DATE'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'NEXTRAISE_DT'; MapType: mtCustom; FuncName: 'E1_NEXTRAISE_DT'),
    (ITable: 'E1'; IField: 'PAY_FREQ'; MapType: mtCustom; FuncName: 'E1_PAY_FREQ' ),
    (ITable: 'E1'; IField: 'ANN_SALARY'; ETable: 'EE'; EField: 'CALCULATED_SALARY'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'RATES_1'; ETable: 'EE_RATES'; EField: 'RATE_AMOUNT'; KeyField: 'RATE_NUMBER'; KeyValue: '1'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'RATES_2'; ETable: 'EE_RATES'; EField: 'RATE_AMOUNT'; KeyField: 'RATE_NUMBER'; KeyValue: '2'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'RATES_3'; ETable: 'EE_RATES'; EField: 'RATE_AMOUNT'; KeyField: 'RATE_NUMBER'; KeyValue: '3'; MapType: mtDirect),
    // EE_STATES
    (ITable: 'E1'; IField: 'STATE_DEP'; ETable: 'EE_STATES'; EField: 'STATE_NUMBER_WITHHOLDING_ALLOW'; MapType: mtDirect),
    (ITable: 'E1'; IField: 'WORK_STATE'; ETable: 'EE_STATES'; EField: 'SUI_APPLY_CO_STATES_NBR'; MapType: mtLookup; LookupTable: 'CO_STATES'; LookupField: 'STATE'),
    (ITable: 'E1'; IField: 'LIVE_STATE'; ETable: 'EE_STATES'; EField: 'SDI_APPLY_CO_STATES_NBR'; MapType: mtLookup; LookupTable: 'CO_STATES'; LookupField: 'STATE'),
    (ITable: 'E1'; IField: 'STATE_DEP@1'; ETable: 'EE_STATES'; EField: 'STATE_NUMBER_WITHHOLDING_ALLOW'; MapType: mtDirect),

    (ITable: 'E1_fixup'; IField: 'LIVE_STATE'; ETable: 'EE'; EField: 'HOME_TAX_EE_STATES_NBR'; MapType: mtLookup; LookupTable: 'EE_STATES'; LookupField: 'CO_STATES_NBR'; LookupTable1: 'CO_STATES'; LookupField1: 'STATE'),

    (ITable: 'EB'; IField: 'ACT_NUM'; ETable: 'EE_DIRECT_DEPOSIT'; EField: 'EE_BANK_ACCOUNT_NUMBER'; MapType: mtDirect),
    (ITable: 'EB'; IField: 'BANK_ABA'; ETable: 'EE_DIRECT_DEPOSIT'; EField: 'EE_ABA_NUMBER'; MapType: mtDirect),
    (ITable: 'EB'; IField: 'CHECK_SAVE'; ETable: 'EE_DIRECT_DEPOSIT'; EField: 'EE_BANK_ACCOUNT_TYPE'; MapType: mtMap;
                                  MapValues: 'C=' + RECEIVING_ACCT_TYPE_CHECKING + ',' +
                                             'S=' + RECEIVING_ACCT_TYPE_SAVING  ),
    (ITable: 'EB'; IField: 'PRENOTE_DT'; MapType: mtCustom; FuncName: 'EB_IN_PRENOTE' )
  );
//  (ITable: 'E1'; IField: ''; ETable: 'EE'; EField: ''; MapType: mtDirect)


{ TPayImportDefinition }


constructor TPayImportDefinition.Create(const AIEMap: IIEMap);
begin
  inherited Create(AIEMap);
  
  RegisterConvertDescs( PayConversion );
  RegisterKeySourceDescs( PayKeys );
  RegisterRowImport( PayRowImport );
  RegisterMap( PayMap );
end;

destructor TPayImportDefinition.Destroy;
begin
  inherited;
end;

procedure TPayImportDefinition.AI_CO_DIVISION(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  if FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').IsNull then
  begin
    ds.Cancel;
    RaiseMissingValue( 'Please define company Home State for %s', [ VarToStr(FImpEngine.IDS('CO')['CUSTOM_COMPANY_NUMBER']) ] );
  end;
  ds.FieldByName('HOME_CO_STATES_NBR').AsInteger := FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').AsInteger;
  ds.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  ds.FieldByName('PRINT_DIV_ADDRESS_ON_CHECKS').AsString := INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK;
  ds.FieldByName('PAY_FREQUENCY_HOURLY').AsString :=  FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
  ds.FieldByName('PAY_FREQUENCY_SALARY').AsString := FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
end;

procedure TPayImportDefinition.AI_CO_BRANCH(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  if FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').IsNull then
  begin
    ds.Cancel;
    RaiseMissingValue( 'Please define company Home State for %s', [VarToStr(FImpEngine.IDS('CO')['CUSTOM_COMPANY_NUMBER'])] );
  end;
  ds.FieldByName('HOME_CO_STATES_NBR').AsInteger := FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').AsInteger;
  ds.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  ds.FieldByName('PRINT_BRANCH_ADDRESS_ON_CHECK').AsString := INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK;
  ds.FieldByName('PAY_FREQUENCY_HOURLY').AsString :=  FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
  ds.FieldByName('PAY_FREQUENCY_SALARY').AsString := FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
end;

procedure TPayImportDefinition.AI_CO_DEPARTMENT(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  if FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').IsNull then
  begin
    ds.Cancel;
    RaiseMissingValue( 'Please define company Home State for %s', [ VarToStr(FImpEngine.IDS('CO')['CUSTOM_COMPANY_NUMBER']) ] );
  end;
  ds.FieldByName('HOME_CO_STATES_NBR').AsInteger := FImpEngine.IDS('CO').FieldByName('HOME_CO_STATES_NBR').AsInteger;
  ds.FieldByName('HOME_STATE_TYPE').AsString := HOME_STATE_TYPE_DEFAULT;
  ds.FieldByName('PRINT_DEPT_ADDRESS_ON_CHECKS').AsString := INCLUDE_REPT_AND_EXCLUDE_ADDRESS_ON_CHECK;
  ds.FieldByName('PAY_FREQUENCY_HOURLY').AsString :=  FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
  ds.FieldByName('PAY_FREQUENCY_SALARY').AsString := FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_SALARY_DEFAULT').AsString;
end;

procedure TPayImportDefinition.AI_CL_E_DS(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds.Cancel;
  RaiseMissingParent( 'Please manually create E/D %s at client level.', [VarToStr(Keys['CL_E_DS.CUSTOM_E_D_CODE_NUMBER'])] );
end;

procedure TPayImportDefinition.AI_CO_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds.Cancel;
  RaiseMissingParent( 'Please manually create state %s at company level.', [VarToStr(Keys['CO_STATES.STATE'])] );
end;

procedure TPayImportDefinition.AI_CO_LOCAL_TAX(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds.Cancel;
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_LOCALS'));
  RaiseMissingParent('Please manually create local %s at company level.', [FImpEngine.IDS('SY_LOCALS').Lookup('SY_LOCALS_NBR', VarToStr(Keys['SY_LOCALS.SY_LOCALS_NBR']), 'NAME')] );
end;

procedure TPayImportDefinition.AI_CO(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds.Cancel;
  RaiseMissingParent('Please manually create company %s.', [VarToStr(Keys['CO.CUSTOM_COMPANY_NUMBER'])] );
end;

procedure TPayImportDefinition.AI_CO_SUI(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds.Cancel;
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_SUI'));
  RaiseMissingParent('Please manually create SUI <%s> at company level.', [FImpEngine.IDS('SY_SUI').Lookup('SY_SUI_NBR', VarToStr(Keys['CO_SUI.SY_SUI_NBR']), 'SUI_TAX_NAME')] ); //!!
end;

procedure TPayImportDefinition.E1_FEDERAL_MARITAL_STATUS(const Input, Keys, Output: TFieldValues);
begin
  Output.Add( 'EE', 'FEDERAL_MARITAL_STATUS', Input['E1.M_STATUS'] );
  Output.Add( 'EE_STATES', 'IMPORTED_MARITAL_STATUS', Input['E1.M_STATUS'] );
  Keys.Add( 'CO_STATES', 'STATE', Input['E1.LIVE_STATE'] );

  if VarToStr(Input['E1.TITLE']) <> '' then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_HR_POSITIONS'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
    if FImpEngine.IDS('CO_HR_POSITIONS').Locate('DESCRIPTION', Input['E1.TITLE'], [loCaseInsensitive]) then
      Output.Add( 'EE', 'CO_HR_POSITIONS_NBR', FImpEngine.IDS('CO_HR_POSITIONS')['CO_HR_POSITIONS_NBR'] )
    else
      Logger.LogWarningFmt('Position %s for Employee %s could not be found in Evolution', [Input['E1.TITLE'], Input['E1.EMP_NUM']]);
  end;

  if LeftStr(Input['E1.SS_NUM'], 3) = 'xxx' then
    Logger.LogWarningFmt('Employee %s does not have a correct Social Security Number. Please change it.', [Input['E1.EMP_NUM']]);
  if VarToStr(Input['E1.ADDRESS_1']) = '' then
  begin
    Output.Add('CL_PERSON', 'ADDRESS1', '<ADDRESS>');
    Logger.LogWarningFmt('Employee %s does not have a correct address. Please change it.', [Input['E1.EMP_NUM']]);
  end;
  if VarToStr(Input['E1.CITY']) = '' then
  begin
    Output.Add('CL_PERSON', 'CITY', '<CITY>');
    Logger.LogWarningFmt('Employee %s does not have a correct city. Please change it.', [Input['E1.EMP_NUM']]);
  end;
  if (VarToStr(Input['E1.ZIP']) = '') or (VarToStr(Input['E1.ZIP']) = '0') then
  begin
    Output.Add('CL_PERSON', 'ZIP_CODE', '<ZIP>');
    Logger.LogWarningFmt('Employee %s does not have a correct zip code. Please change it.', [Input['E1.EMP_NUM']]);
  end;
end;

procedure TPayImportDefinition.E1_FEDERAL_MARITAL_STATUS_to_SUI_STATE( const Input, Keys, Output: TFieldValues);
begin
  Output.Add( 'EE_STATES', 'IMPORTED_MARITAL_STATUS', Input['E1.M_STATUS'] );
  Keys.Add( 'CO_STATES', 'STATE', Input['E1.WORK_STATE'] );
end;

procedure TPayImportDefinition.HelpImportSTATE_MARITAL_STATUS(const Input, Keys, Output: TFieldValues; target_state: string);
type
  string2 = string[2];
var
  state: string2;
  sms: char;
  esms: string2;
  Tmp: Integer;

  function dep: cardinal;
  begin
    Result := cardinal(StrToInt(Trim(VarToStr(Input['E1.STATE_DEP']))));
  end;
  function sub: string2; //Sub Income Tax State Code
  begin
    Result := UpperCase(trim(VarToStr(Input['E1.SUB_ST_CD'])));
  end;
  function sub_as_cardinal: cardinal; //Sub Income Tax State Code
  begin
    Result := strtoint( Trim(sub) );
  end;
  procedure put; overload;
  begin
    esms := sms;
  end;
  procedure put( truePart: string ); overload;
  begin
    esms := truePart;
  end;
  procedure put( cond: boolean; truePart: string ); overload;
  begin
    if cond then
      put( truePart );
  end;
  procedure put( cond: boolean; truePart, falsePart: string ); overload;
  begin
    if cond then
      put( truePart )
    else
      put( falsePart );
  end;
begin
  sms := GetSingleChar( Input['E1.M_STATUS_S'], 'E1.M_STATUS_S', [] );
  state := UpperCase(trim(VarToStr(Input['E1.LIVE_STATE'])));
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'), 'STATE=''' + state + '''' );
  if FImpEngine.IDS('SY_STATES').RecordCount <> 1 then
    RaiseUnknownCode( 'Cannot identify state by <%s>', [state] );

  esms := '';
  if state = 'AL' then
    case sms of
      'S': put( dep = 0, '0', 'S' );
      'M': put( dep = 0, '0', 'M' );
      'H': put;
    end
  else if state = 'AR' then
    case sms of
      'S': put( 'A' );
      'M': put( 'B' );
      'H': put( 'D' );
    end
  else if state = 'AZ' then
  begin
    if InSet( sub, ['10', '20', '22', '28', '32', '17']) then
      put( sub )
  end
  else if state = 'CA' then
    case sms of
      'S': put( 'SM' );
      'M': put( dep < 2, 'M', 'M2' );
      'H': put;
    end
  else if state = 'NV' then
    case sms of
      'S': put;
      'M': put;
      'H': put;
    end
                              // S => S , M => M //'MN' moved out here as Roma said
  else if InSet( state, ['CO', 'IL', 'MT', 'ND', 'NE', 'NH', 'RI', 'SD', 'TN', 'TX', 'UT', 'VT', 'WA', 'WV', 'WY'] ) then
    case sms of
      'S': put;
      'M': put;
    end
  else if InSet( state,  ['CT', 'GA'] ) then
  begin
    case sms of
      'A'..'F': put;
      'H': put('B');
      'S': put('F');
      'M': put('B');
    end;
  end
  else if InSet( state, ['DE', 'DC'] ) then  // S,H => S, M => A
    case sms of
      'S': put;
      'M': put( 'A' );
      'H': put( 'S' );
    end
  else if InSet( state, ['HI', 'ID', 'IN', 'KS', 'KY', 'NY', 'NM', 'ME'] ) then
    case sms of
      'S': put;
      'M': put;
      'H': put( 'S' );
    end
  else if state = 'IA' then
    put( dep <= 2, 'S', 'S2' )
{
  else if state = 'IL' then // moved out here,  as in Roma's e-mail

    case dep of
      0: put( '0' );
      1: put( 'S' );
      else put( 'M' );
    end
}
  else if state = 'LA' then
  begin
    case sms of
      'S', 'H': put( '1' );
      'M': put( '2' );
    end
  end
  else if state = 'MA' then
    case sms of
      'S', 'M':
        case dep of
          0: put( '0' );
          1: put( '1' );
          else put( '2' );
        end;
      'H':
        case dep of
          0: put( 'H0' );
          1: put( 'H1' );
          else put( 'H2' );
        end
    end
  else if state = 'MD' then
  begin
    put( 'N' );
{    if IsCardinal( sub ) then
      case sub_as_cardinal of
        10: put( 'RA' );
        25: put( 'RB' );
        30: put( 'RC' );
        40: put( 'RD' );
        45: put( 'RE' );
        50: put( 'RF' );
        55: put( 'RG' );
        60: put( 'RH' );
      end}
  end
  else if state = 'MN' then
    case sms of
      'S', 'M', 'H': put;
    end
  else if state = 'MS' then
    case sms of
      'S': put( 'S0' );
      'M': put;
      'H': put;
    end
  else if state = 'MO' then
    case sms of
      'S': put;
      'M': put( 'M1' );
      'H': if dep <= 4 then
             put( 'H' )
           else
             put( 'H2' );
    end
  else if state = 'NJ' then
    case sms of
      'S': put( 'A' );
      'M': put( 'B' );
      'H': put( 'B' );
    end
  else if InSet( state, ['MI', 'OH', 'PA', 'VA'] ) then // S,M,H =>  'SM' //'ND' moved out here as Roma said in e-mail
    put( 'SM' )
  else if state = 'NC' then
    case sms of
      'S': put;
      'M': put( 'MW' );
      'H': put( 'HH' );
    end
  else if state = 'OK' then
    case sms of
      'S': put;
      'H': put( 'M' );
      'M': put;
    end
  else if state = 'FL' then
    case sms of
      'S': put;
      'H': put( 'S' );
      'M': put;
    end
  else if state = 'OR' then
    case sms of
      'S': put( dep <= 2, 'S', 'M' );
      'M': put;
      'H': put( 'M' );
    end
  else if state = 'PR' then
    case sms of
      'S': put;
      'M': put( 'MS' );
      'H': put( 'MH' );
    end
  else if state = 'SC' then
    case dep of
      0: put( '0' );
      else put( '1' );
    end
  else if state = 'WI' then
    case sms of
      'S': put( dep = 0, 'S0', 'S' );
      'M': put( dep = 0, 'M0', 'M' );
    end;
  if esms <> '' then
  begin

    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR='+vartostr(FImpEngine.IDS('SY_STATES')['SY_STATES_NBR']) );
    FImpEngine.IDS('SY_STATE_MARITAL_STATUS').Locate('STATUS_TYPE', esms, []);
    Tmp := FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['SY_STATE_MARITAL_STATUS_NBR'];
    Output.Add('EE_STATES', 'SY_STATE_MARITAL_STATUS_NBR', Tmp);

    Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', esms );
    Keys.Add( 'CO_STATES', 'STATE', Input[target_state] );

  end
  else
    raise EevException.CreateFmt( 'Cannot map state marital status: STATE=<%s> M_STATUS_S=<%s> SUB_ST_CD=<%s> STATE_DEP=<%d>', [state, sms, sub, dep] );
end;

procedure TPayImportDefinition.E1__TO__HOME_STATE_MARITAL_STATUS(const Input, Keys, Output: TFieldValues);
begin
  HelpImportSTATE_MARITAL_STATUS( Input, Keys, Output, 'E1.LIVE_STATE' );
end;

procedure TPayImportDefinition.E1__TO__SUI_STATE_MARITAL_STATUS(const Input, Keys, Output: TFieldValues);
begin
  HelpImportSTATE_MARITAL_STATUS( Input, Keys, Output, 'E1.WORK_STATE' );
end;

procedure TPayImportDefinition.E1_PR_STATUS(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
var
  val: char;
  ps: Variant;
begin
  val := GetSingleChar( Value, Table+'.'+Field, ['F','P','T','1','2','H','S','D'] );
  Output.Add( 'CL_PERSON', 'EIN_OR_SOCIAL_SECURITY_NUMBER', Iff( val = '2', GROUP_BOX_EIN, GROUP_BOX_SSN) );
  Output.Add( 'EE', 'COMPANY_OR_INDIVIDUAL_NAME', Iff( val in ['1','2'], GROUP_BOX_COMPANY, GROUP_BOX_INDIVIDUAL) );
  if val = 'F' then
    ps := POSITION_STATUS_FULL
  else if val = 'P' then
    ps := POSITION_STATUS_PART
  else if val = 'T' then
    ps := POSITION_STATUS_FULL
  else
    ps := Null;
  Output.Add( 'EE', 'POSITION_STATUS', ps );
  Output.Add( 'EE', 'W2_STATUTORY_EMPLOYEE', Iff( val = 'S', GROUP_BOX_YES, GROUP_BOX_NO ) );
  Output.Add( 'EE', 'W2_DECEASED', Iff( val = 'D', GROUP_BOX_YES, GROUP_BOX_NO ) );
end;

procedure TPayImportDefinition.E1_STATUS(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
var
  val: char;
  nhrs: Variant;
  ctc: Variant;
begin
  val := GetSingleChar( Value, Table+'.'+Field, ['N','A','I','R','T'] );
  case val of
    'N': nhrs := NEW_HIRE_REPORT_PENDING;
//    'A','I','R','T': nhrs := NEW_HIRE_REPORT_COMPLETED;  //   changed By CA ticket : 51007
    'A','I','R','T': nhrs := NEW_HIRE_REPORT_COMP_BY_PREDECESSOR;
  end;
  case val of
    'A','R','N': ctc := EE_TERM_ACTIVE;
    'I': ctc := EE_TERM_TRANSFER;
    'T': ctc := EE_TERM_TERMINATED;
  end;
  Output.Add( 'EE', 'NEW_HIRE_REPORT_SENT', nhrs );
  Output.Add( 'EE', 'CURRENT_TERMINATION_CODE', ctc );
end;

procedure TPayImportDefinition.E1_PAY_FREQ(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
begin
  if trim(VarToStr( Value )) <> '' then
    Output.Add( 'EE', 'PAY_FREQUENCY', Value );
end;

procedure TPayImportDefinition.E1_NEXTRAISE_DT(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
begin
  Output.Add( 'EE', 'NEXT_REVIEW_DATE', Value );
  Output.Add( 'EE', 'NEXT_RAISE_DATE', Value );
end;

procedure TPayImportDefinition.E1_HIRE_DT(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
begin
  if not VarIsNull(Value) then
  begin
    Output.Add( 'EE', 'ORIGINAL_HIRE_DATE', Value );
    Output.Add( 'EE', 'CURRENT_HIRE_DATE', Value );
  end
  else
  begin
    Output.Add( 'EE', 'ORIGINAL_HIRE_DATE', Date );
    Output.Add( 'EE', 'CURRENT_HIRE_DATE', Date );
  end
end;

procedure TPayImportDefinition.E1_CDV_CODE_AND_CDN_CODE(const Input, Keys, Output: TFieldValues);
var
  division: Variant;
  branch: Variant;
  department: Variant;
begin
//      patchedIEmap.IDS('CO').Lookup( 'CUSTOM_COMPANY_NUMBER', vaCDBDT.Value['E1.BCL_CODE'], 'CO_NBR' );]
//  EvMessage( VarTostr(Input['E1.CDV_CODE'])+' '+VartoStr(Input['E1.CDN_CODE']));
  if GetParam(sHasDBDT) then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DIVISION'), 'CO_NBR='+VarToStr(FImpEngine.IDS('CO')['CO_NBR']) );
    division := FImpEngine.IDS('CO_DIVISION').Lookup( 'CUSTOM_DIVISION_NUMBER', sHiddenDivisionCode, 'CO_DIVISION_NBR' );

    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_BRANCH'), 'CO_DIVISION_NBR='+VarToStr(division) );
    branch := FImpEngine.IDS('CO_BRANCH').Lookup( 'CUSTOM_BRANCH_NUMBER', Input['E1.CDV_CODE'], 'CO_BRANCH_NBR' );

    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_DEPARTMENT'), 'CO_BRANCH_NBR='+VarToStr(branch) );
    department := FImpEngine.IDS('CO_DEPARTMENT').Lookup( 'CUSTOM_DEPARTMENT_NUMBER', Input['E1.CDN_CODE'], 'CO_DEPARTMENT_NBR' );

    Output.Add( 'EE', 'CO_DIVISION_NBR', division );
    Output.Add( 'EE', 'CO_BRANCH_NBR', branch );
    Output.Add( 'EE', 'CO_DEPARTMENT_NBR', department );
  end;
end;

procedure TPayImportDefinition.E1_EEOC_CODE(const Table, Field: string; const Value: Variant; const Keys: TFieldValues; const Output: TFieldValues);
const
  EEOmap: array [0..9] of record code: char; descr: string end =
  (
    (code: '1'; descr: 'Officials And Managers'),
    (code: '2'; descr: 'Professional'),
    (code: '3'; descr: 'Technicians'),
    (code: '4'; descr: 'Sales Workers'),
    (code: '5'; descr: 'Office Clerical'),
    (code: '6'; descr: 'Craft Workers - Skilled'),
    (code: '7'; descr: 'Operatives - Semi-Skilled'),
    (code: '8'; descr: 'Laborers - Unskilled'),
    (code: '9'; descr: 'Service Workers'),
    (code: 'N'; descr: 'No EEO Class Assigned')
  );
var
  syhreeocnbr: Variant;
  i: integer;
  code: string;
  descr: string;
begin
  Assert( not VarIsEmpty(Value) );
  if not VarIsNull(Value)  then
  begin
    code := GetSingleChar( Value, Table + '.' + Field, ['1'..'9', 'N']{keep in sync with EEOmap} );
    for i := low(EEOmap) to high(EEOmap) do
      if EEOMap[i].code = code then
      begin
        descr := trim(EEOMap[i].descr);
        break;
      end;
    FImpEngine.CurrentDataRequired( FImpEngine.IDS('SY_HR_EEO') );
    with FImpEngine.IDS('SY_HR_EEO') do
    begin
      First;
      while not Eof do
      begin
        if AnsiSameText( trim( FieldValues['DESCRIPTION'] ), descr ) then
        begin
          syhreeocnbr := FieldValues['SY_HR_EEO_NBR'];
          break;
        end;
        Next;
      end;
    end;
    if not VarIsEmpty(syhreeocnbr) then
      Output.Add( 'EE', 'SY_HR_EEO_NBR', syhreeocnbr )
    else
      Logger.LogErrorFmt( 'Unable to locate Nbr of EEO code <%s>', [descr] )
  end
end;


procedure TPayImportDefinition.ET_ALL(const Input, Keys, Output: TFieldValues);
var
  code: string;
  taxblock: string;
  local, sylocalsnbr: variant;

  function tbTranslate1(taxblock: string): string;
  begin
    if taxblock = 'Y' then
      Result := GROUP_BOX_INCLUDE
    else if (taxblock = 'N') or
            (taxblock = 'I') or
            (taxblock = 'D') then
      Result := GROUP_BOX_EXEMPT
    else
      Result := GROUP_BOX_EXCLUDE;
  end;

  function tbTranslate2(taxblock: string): string;
  begin
    if taxblock = 'Y' then
      Result := GROUP_BOX_NO
    else
      Result := GROUP_BOX_YES;
  end;
begin
  code := UpperCase( trim( VarToStr(Input['ET.CODE']) ) );
  taxblock := UpperCase( trim( VarToStr(Input['ET.TAX_BLOCK']) ) );
(*      if code = 'EI' {???} then
  begin
    Output.Add( 'EE', 'EIC', EIC_ );
    Result := true;
  end
  else *)
  if code = 'FW'{federal tax} then
  begin
    Output.Add( 'EE', 'OVERRIDE_FED_TAX_TYPE', Input['ET.EX_TAX_TYPE'] );
    if Input['ET.EX_TAX_TYPE'] <> OVERRIDE_VALUE_TYPE_NONE then
      Output.Add( 'EE', 'OVERRIDE_FED_TAX_VALUE', Input['ET.EX_TAX'] )
    else
      Output.Add( 'EE', 'OVERRIDE_FED_TAX_VALUE', Null );
    Output.Add( 'EE', 'EXEMPT_EXCLUDE_EE_FED', tbTranslate1(taxblock) );
  end
  else if code = 'FI'{EE_OASDI} then
  begin
    Output.Add( 'EE', 'EXEMPT_EMPLOYEE_OASDI', tbTranslate2(taxblock) );
    Output.Add( 'EE', 'EXEMPT_EMPLOYER_OASDI', tbTranslate2(taxblock) );
    Output.Add( 'EE', 'EXEMPT_EMPLOYER_FUI', tbTranslate2(taxblock) );
  end
  else if code = 'MC'{EE Medicare} then
  begin
    Output.Add( 'EE', 'EXEMPT_EMPLOYEE_MEDICARE', tbTranslate2(taxblock) );
    Output.Add( 'EE', 'EXEMPT_EMPLOYER_MEDICARE', tbTranslate2(taxblock) );
  end
  else if code = 'ST' then
  begin
    Output.Add( 'EE_STATES', 'OVERRIDE_STATE_TAX_TYPE', Input['ET.EX_TAX_TYPE'] );
    if Input['ET.EX_TAX_TYPE'] <> OVERRIDE_VALUE_TYPE_NONE then
      Output.Add( 'EE_STATES', 'OVERRIDE_STATE_TAX_VALUE', Input['ET.EX_TAX'] )
    else
      Output.Add( 'EE_STATES', 'OVERRIDE_STATE_TAX_VALUE', Null );

    Output.Add( 'EE_STATES', 'STATE_EXEMPT_EXCLUDE', tbTranslate1(taxblock) );
  end
  else if code = 'DD' then
  begin
    Output.Add( 'EE_STATES', 'EE_SDI_EXEMPT_EXCLUDE', tbTranslate1(taxblock) );
    Output.Add( 'EE_STATES', 'ER_SDI_EXEMPT_EXCLUDE', tbTranslate1(taxblock) );
  end
  else if code = 'UI' then
  begin
    Output.Add( 'EE_STATES', 'EE_SUI_EXEMPT_EXCLUDE', tbTranslate1(taxblock) );
    Output.Add( 'EE_STATES', 'ER_SUI_EXEMPT_EXCLUDE', tbTranslate1(taxblock) );
  end
  else if InSet( code, ['LC','J1','J2','J3','J4','J5','J6', 'L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8', 'L9'] ) then
  begin
    local := null;
    if code = 'LC' then
    begin
      if LeftStr(Input['ET.E1_TT1_CODE_1'], 2) = 'MD' then
      begin
        Keys.Add( 'CO_STATES', 'STATE', 'MD' );
        taxblock := Input['ET.E1_TT1_CODE_1'];
        if taxblock = 'MDALEG' then
          taxblock := '1'
        else if taxblock = 'MDANNE' then
          taxblock := '2'
        else if taxblock = 'MDBALT' then
          taxblock := '3'
        else if taxblock = 'MDBLTC' then
          taxblock := '4'
//        else if taxblock = 'MDCARL' then
//          taxblock := '5'
        else if taxblock = 'MDCALV' then
          taxblock := '5'
        else if taxblock = 'MDCARR' then
          taxblock := '6'
        else if taxblock = 'MDCECL' then
          taxblock := '8'
        else if taxblock = 'MDCHAR' then
          taxblock := '24'
        else if taxblock = 'MDDRCH' then
          taxblock := '9'
        else if taxblock = 'MDFRED' then
          taxblock := '10'
//        else if taxblock = 'MDGARR' then
//          taxblock := '12'
        else if taxblock = 'MDHARF' then
          taxblock := '23'
        else if taxblock = 'MDHWRD' then
          taxblock := '12'
        else if taxblock = 'MDKENT' then
          taxblock := '13'
        else if taxblock = 'MDMONT' then
          taxblock := '14'
        else if taxblock = 'MDPRGR' then
          taxblock := '15'
        else if taxblock = 'MDQUAN' then
          taxblock := '16'
        else if taxblock = 'MDSOMR' then
          taxblock := '25'
        else if taxblock = 'MDSTMR' then
          taxblock := '17'
        else if taxblock = 'MDTALB' then
          taxblock := '18'
        else if taxblock = 'MDWASH' then
          taxblock := '19'
        else if taxblock = 'MDWICO' then
          taxblock := '20'
        else if taxblock = 'MDWORC' then
          taxblock := '21'
        else
          raise EevException.CreateFmt( 'Cannot map state marital status: STATE=MD M_STATUS=<%s>', [taxblock] );
        Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', taxblock );
      end
      else
        local := Input['ET.E1_TT1_CODE_1'];
    end
    else
      local := Input['ET.CD_TT1_CODE'];
    if not VarIsEmpty(local) and not VarIsNull(local) then
    begin
      sylocalsnbr := Matchers.Get(sLocalsBindingName).RightMatch( local );
      if not VarIsNull(sylocalsnbr) then
      begin
        Keys.Add( 'SY_LOCALS', 'SY_LOCALS_NBR', sylocalsnbr );
        Keys.Add( 'CO_LOCAL_TAX', '', Unassigned );
        Output.Add( 'EE_LOCALS', '', Unassigned );
      end;
    end;
  end;
//  else
//    Assert( InSet( code, ['EI', 'FR', 'EM', 'FU', 'UI'] ), 'Unknown value in ET.CODE: <'+code+'>' );
end;

procedure TPayImportDefinition.AI_CL_PERSON(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds.FieldByName('EIN_OR_SOCIAL_SECURITY_NUMBER').AsString := GROUP_BOX_SSN;
// not a good idea to set description to 'home' by default
//  ds.FieldByName('DESCRIPTION1').AsString := 'home';
  ds.FieldByName('SMOKER').AsString := GROUP_BOX_NO;
  ds.FieldByName('VETERAN').AsString := GROUP_BOX_NOT_APPLICATIVE;
  ds.FieldByName('VIETNAM_VETERAN').AsString := GROUP_BOX_NOT_APPLICATIVE;
  ds.FieldByName('DISABLED_VETERAN').AsString := GROUP_BOX_NOT_APPLICATIVE;
  ds.FieldByName('MILITARY_RESERVE').AsString := GROUP_BOX_NOT_APPLICATIVE;
  ds.FieldByName('I9_ON_FILE').AsString := GROUP_BOX_YES;
end;

procedure TPayImportDefinition.AI_EE(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds.FieldByName('COMPANY_OR_INDIVIDUAL_NAME').AsString := GROUP_BOX_INDIVIDUAL; //already default
  ds.FieldByName('TIPPED_DIRECTLY').AsString := GROUP_BOX_NO; //already default
  ds.FieldByName('PAY_FREQUENCY').AsString := FImpEngine.IDS('CO').FieldByName('PAY_FREQUENCY_HOURLY_DEFAULT').AsString;
  if FImpEngine.IDS('CO').FieldByName('DISTRIBUTE_DEDUCTIONS_DEFAULT').AsString =  CO_DISTRIBUTE_BOTH then
    ds.FieldByName('DISTRIBUTE_TAXES').AsString := DISTRIBUTE_BOTH
  else
    ds.FieldByName('DISTRIBUTE_TAXES').AsString := FImpEngine.IDS('CO').FieldByName('DISTRIBUTE_DEDUCTIONS_DEFAULT').AsString;
  ds.FieldByName('W2_TYPE').AsString := W2_TYPE_FEDERAL; //already default
  ds.FieldByName('BASE_RETURNS_ON_THIS_EE').AsString := GROUP_BOX_YES; //already default
  ds['EIC'] := EIC_NONE; //already default
  ds['OVERRIDE_FED_TAX_TYPE'] := OVERRIDE_VALUE_TYPE_NONE; //roma
//  ds['PAY_FREQUENCY'] := FREQUENCY_TYPE_MONTHLY; //temporary
end;

procedure TPayImportDefinition.AI_EE_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  Assert( FImpEngine.IDS('CO_STATES')['STATE'] = Keys['CO_STATES.STATE'] );
  ds['SUI_APPLY_CO_STATES_NBR'] := FImpEngine.IDS('CO_STATES')['CO_STATES_NBR'];
  ds['SDI_APPLY_CO_STATES_NBR'] := FImpEngine.IDS('CO_STATES')['CO_STATES_NBR'];
  ds.FieldByName('STATE_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE; //already default
  ds.FieldByName('OVERRIDE_STATE_TAX_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE; //already default
  ds.FieldByName('EE_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE; //already default
  ds.FieldByName('EE_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE; //already default
  ds.FieldByName('ER_SDI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE; //already default
  ds.FieldByName('ER_SUI_EXEMPT_EXCLUDE').AsString := GROUP_BOX_INCLUDE; //already default
  ds.FieldByName('RECIPROCAL_METHOD').AsString := STATE_RECIPROCAL_TYPE_NONE; //already default
  ds.FieldByName('CALCULATE_TAXABLE_WAGES_1099').AsString := GROUP_BOX_NO; //already default
end;

procedure TPayImportDefinition.AI_EE_LOCALS(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
//  ds['EXEMPT_EXCLUDE'] := ;
end;

//copied from Dima's code
procedure TPayImportDefinition.AI_EE_RATES(const ds: TevClientDataSet; const Keys: TFieldValues );
var
  i: Integer;
begin
  for i := 0 to High(Keys.Fields) do
    if Keys.Fields[i] = 'PRIMARY_RATE' then
    begin
      ds['RATE_NUMBER'] := 1;
      Break;
    end
    else if Keys.Fields[i] = 'RATE_NUMBER' then
    begin
      if Keys.Values[i] = '1' then
        ds['PRIMARY_RATE'] := GROUP_BOX_YES;
      Break;
    end;
end;

procedure TPayImportDefinition.AI_PR(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds.FieldByName('CHECK_DATE_STATUS').AsString := DATE_STATUS_IGNORE; //as in rapid import
  ds.FieldByName('SCHEDULED').AsString := GROUP_BOX_NO; //as in rapid import
  ds.FieldByName('PAYROLL_TYPE').AsString := PAYROLL_TYPE_IMPORT;
  ds.FieldByName('STATUS').AsString := PAYROLL_STATUS_PENDING; // as default
  ds.FieldByName('STATUS_DATE').AsDateTime := Now; //Date or DateTime?
  ds.FieldByName('EXCLUDE_ACH').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_TAX_DEPOSITS').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_AGENCY').AsString := GROUP_BOX_YES;
  ds.FieldByName('MARK_LIABS_PAID_DEFAULT').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_BILLING').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_R_C_B_0R_N').AsString := GROUP_BOX_BOTH2;
  ds.FieldByName('INVOICE_PRINTED').AsString := INVOICE_PRINT_STATUS_calculated_do_not_print;

  ds.FieldByName('SCHEDULED_CALL_IN_DATE').Value := Date;
  ds.FieldByName('SCHEDULED_PROCESS_DATE').AsDateTime := Date;
  ds.FieldByName('SCHEDULED_DELIVERY_DATE').AsDateTime := Date;
  ds.FieldByName('INVOICE_PRINTED').AsString := INVOICE_PRINT_STATUS_calculated_do_not_print;
  ds.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
//  ds.FieldByName('RUN_NUMBER').AsInteger := 1;
end;

procedure TPayImportDefinition.AI_PR_BATCH(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds.FieldByName('PERIOD_BEGIN_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
  ds.FieldByName('PERIOD_END_DATE_STATUS').AsString := DATE_STATUS_IGNORE;
  ds.FieldByName('PAY_SALARY').AsString := GROUP_BOX_NO;
  ds.FieldByName('PAY_STANDARD_HOURS').AsString := GROUP_BOX_NO;
  ds.FieldByName('LOAD_DBDT_DEFAULTS').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_TIME_OFF').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
//  ds.FieldByName('PERIOD_BEGIN_DATE').AsString := FParams.Values[sParamCheckDate]; //temp!!!
//  ds.FieldByName('FREQUENCY').AsString := FParams.Values[sParamBatchFrequency];
end;

procedure TPayImportDefinition.AI_PR_CHECK(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds.FieldByName('PAYMENT_SERIAL_NUMBER').AsInteger := ctx_PayrollCalculation.GetNextPaymentSerialNbr;
  ds.FieldByName('CUSTOM_PR_BANK_ACCT_NUMBER').AsString := GetCustomBankAccountNumber(FImpEngine);
//  ds.FieldByName('CHECK_TYPE').AsString := CheckType; //as key now //  CHECK_TYPE2_YTD    CHECK_TYPE2_QTD
  ds.FieldByName('CALCULATE_OVERRIDE_TAXES').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_DD').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_DD_EXCEPT_NET').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_TIME_OFF_ACCURAL').AsString := TIME_OFF_EXCLUDE_ACCRUAL_ACCRUAL;
  ds.FieldByName('EXCLUDE_AUTO_DISTRIBUTION').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_ALL_SCHED_E_D_CODES').AsString := GROUP_BOX_NO;
  ds.FieldByName('EXCLUDE_SCH_E_D_FROM_AGCY_CHK').AsString := GROUP_BOX_YES;
  ds.FieldByName('EXCLUDE_SCH_E_D_EXCEPT_PENSION').AsString := GROUP_BOX_NO;
  ds.FieldByName('PRORATE_SCHEDULED_E_DS').AsString := GROUP_BOX_NO;
  ds.FieldByName('OR_CHECK_FEDERAL_TYPE').AsString := OVERRIDE_VALUE_TYPE_NONE;
{  // already default
    (T: 'PR_CHECK'; F: 'EXCLUDE_FEDERAL'; C: GroupBox_YesNo_ComboChoices; G: ''; D: GROUP_BOX_NO),
    (T: 'PR_CHECK'; F: 'EXCLUDE_ADDITIONAL_FEDERAL'; C: GroupBox_YesNo_ComboChoices; G: ''; D: GROUP_BOX_NO),
    (T: 'PR_CHECK'; F: 'EXCLUDE_EMPLOYEE_OASDI'; C: GroupBox_YesNo_ComboChoices; G: ''; D: GROUP_BOX_NO),
    (T: 'PR_CHECK'; F: 'EXCLUDE_EMPLOYER_OASDI'; C: GroupBox_YesNo_ComboChoices; G: ''; D: GROUP_BOX_NO),
    (T: 'PR_CHECK'; F: 'EXCLUDE_EMPLOYEE_MEDICARE'; C: GroupBox_YesNo_ComboChoices; G: ''; D: GROUP_BOX_NO),
    (T: 'PR_CHECK'; F: 'EXCLUDE_EMPLOYER_MEDICARE'; C: GroupBox_YesNo_ComboChoices; G: ''; D: GROUP_BOX_NO),
    (T: 'PR_CHECK'; F: 'EXCLUDE_EMPLOYEE_EIC'; C: GroupBox_YesNo_ComboChoices; G: ''; D: GROUP_BOX_NO),
    (T: 'PR_CHECK'; F: 'EXCLUDE_EMPLOYER_FUI'; C: GroupBox_YesNo_ComboChoices; G: ''; D: GROUP_BOX_NO),
    (T: 'PR_CHECK'; F: 'TAX_AT_SUPPLEMENTAL_RATE'; C: GroupBox_YesNo_ComboChoices; G: ''; D: GROUP_BOX_NO),
}
  ds.FieldByName('CHECK_STATUS').AsString := CHECK_STATUS_CLEARED;
  ds.FieldByName('STATUS_CHANGE_DATE').AsDateTime := Now;
  ds.FieldByName('EXCLUDE_FROM_AGENCY').AsString := GROUP_BOX_YES;
  ds.FieldByName('EE_OASDI_TAXABLE_TIPS').Value := 0.0;
  ds.FieldByName('ER_OASDI_TAXABLE_TIPS').Value := 0.0;
  ds.FieldByName('FEDERAL_SHORTFALL').Value := 0.0;
  ds.FieldByName('CHECK_TYPE_945').AsString := GROUP_BOX_NO;
  ds.FieldByName('UPDATE_BALANCE').AsString := GROUP_BOX_NO;

  ds.FieldByName('TAX_FREQUENCY').AsString := FImpEngine.IDS('EE').FieldByName('PAY_FREQUENCY').AsString; //as Roman said
  ds['FEDERAL_TAX'] := 0;
  ds['EE_MEDICARE_TAX'] := 0;
  ds['ER_OASDI_TAX'] := 0;

end;

procedure TPayImportDefinition.AI_PR_CHECK_LINES(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds['CO_DIVISION_NBR'] := FImpEngine.IDS('EE')['CO_DIVISION_NBR'];
  ds['CO_BRANCH_NBR'] := FImpEngine.IDS('EE')['CO_BRANCH_NBR'];
  ds['CO_DEPARTMENT_NBR'] := FImpEngine.IDS('EE')['CO_DEPARTMENT_NBR'];
  ds['EE_STATES_NBR'] := FImpEngine.IDS('EE')['HOME_TAX_EE_STATES_NBR'];
  ds['EE_SUI_STATES_NBR'] := FImpEngine.IDS('EE')['HOME_TAX_EE_STATES_NBR'];
  ds['AGENCY_STATUS'] := CHECK_LINE_AGENCY_STATUS_IGNORE;
end;

procedure TPayImportDefinition.AI_PR_CHECK_STATES(const ds: TevClientDataSet; const Keys: TFieldValues );
begin
  ds['TAX_AT_SUPPLEMENTAL_RATE'] := GROUP_BOX_NO;
  ds['EXCLUDE_STATE'] := GROUP_BOX_NO;
  ds['EXCLUDE_SDI'] := GROUP_BOX_NO;
  ds['EXCLUDE_SUI'] := GROUP_BOX_NO;
  ds['EXCLUDE_ADDITIONAL_STATE'] := GROUP_BOX_YES;
  ds['STATE_OVERRIDE_TYPE'] := OVERRIDE_VALUE_TYPE_NONE;
  ds['EE_SDI_TAX'] := 0;
  ds['STATE_TAX'] := 0;
 //!! why Er SDI tax is not zeroed too? ds['ER_SDI_TAX'] := 0;
end;

procedure TPayImportDefinition.AI_PR_CHECK_LOCALS( const ds: TevClientDataSet; const Keys: TFieldValues);
begin
  ds['EXCLUDE_LOCAL'] := GROUP_BOX_NO;
  ds['LOCAL_TAX'] := 0;
end;

procedure TPayImportDefinition.AI_PR_CHECK_SUI(const ds: TevClientDataSet;
  const Keys: TFieldValues);
begin
  ds['SUI_TAX'] := 0;
end;


const
  sKnownCodes: set of char = [
    'T'{taxes},
    'X'{taxable},
    '$'{earnings},
    'H'{hours},
    'D'{deductions},
    '#'{gross},
    'S'{special YTD},
    'W'{weeks}
  ];

procedure TPayImportDefinition.EA_ALL(const Input, Keys, Output: TFieldValues);
var
  code_type: char;
  code: string;
  amt: Variant;
  fieldname: string;
  tablename: string;
  vasource: string;
begin
  vasource := 'EA.';
  code_type := GetSingleChar( Input[vasource+'CODE_TYPE'], vasource+'CODE_TYPE', sKnownCodes );
  code := UpperCase(trim(VarToStr(Input[vasource+'CODE'])));
  amt := UpperCase(trim(VarToStr(Input[vasource+GetParam(sParamPrAmountField)])));
  fieldname := '';
  tablename := 'PR_CHECK';
  case code_type of
    '#'{gross}: fieldname := 'GROSS_WAGES';
//7827
    'X'{taxable}: if code = 'FW'{federal tax} then
                    fieldname := 'FEDERAL_TAXABLE_WAGES'
                  else if code = 'FI'{EE_OASDI} then
                    fieldname := 'EE_OASDI_TAXABLE_WAGES'  //and EE_OASDI_TAXABLE_TIPS <= EE_OASDI_TAXABLE_WAGES - Earnings, ������� Tips
                  else if code = 'MC'{EE Medicare} then
                    fieldname := 'EE_MEDICARE_TAXABLE_WAGES'
                  else if code = 'FR'{ER OASDI} then
                    fieldname := 'ER_OASDI_TAXABLE_WAGES'
                  else if code = 'EM'{ER Medicare} then
                    fieldname := 'ER_MEDICARE_TAXABLE_WAGES'
                  else if code = 'FU'{ER FUI} then
                    fieldname := 'ER_FUI_TAXABLE_WAGES';
//                    else if not InSet( code, ['UI', 'ST', 'LC', 'L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8', 'L9', ] )  then
//                    RaiseUnknownCode( 'Unknown value in EA.CODE for CODE_TYPE = X: <%s>',[code] );

    'T'{taxes}: if code = 'FW'{federal tax} then
                    fieldname := 'FEDERAL_TAX'
                  else if code = 'FI'{EE_OASDI} then
                    fieldname := 'EE_OASDI_TAX'
                  else if code = 'MC'{EE Medicare} then
                    fieldname := 'EE_MEDICARE_TAX'
                  else if code = 'FR'{ER OASDI} then
                    fieldname := 'ER_OASDI_TAX'
                  else if code = 'EM'{ER Medicare} then
                    fieldname := 'ER_MEDICARE_TAX'
                  else if code = 'FU'{ER FUI} then
                    fieldname := 'ER_FUI_TAX'
                  else if code = 'BU'{ER FUI} then
                    fieldname := 'OR_CHECK_BACK_UP_WITHHOLDING';
//                    else if not InSet( code, ['UI', 'ST', 'LC', 'L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'L7', 'L8', 'L9'] )  then
//                      RaiseUnknownCode( 'Unknown value in EA.CODE for CODE_TYPE = T: <%s>',[code] );
  end;
  if fieldname <> '' then
    Output.Add( tablename, fieldname, amt );
end;

procedure TPayImportDefinition.EA_checklines_ALL(const Input, Keys, Output: TFieldValues);
var
  code_type: char;
  code: string;
  amt: Variant;
  fieldname: string;
  tablename: string;
  eord: string;
  ed_custom_number: Variant;
  vasource: string;
begin
  vasource := 'EA_checklines.';
  code_type := GetSingleChar( Input[vasource+'CODE_TYPE'], vasource+'CODE_TYPE', sKnownCodes );
  code := UpperCase(trim(VarToStr(Input[vasource+'CODE'])));
  amt := UpperCase(trim(VarToStr(Input[vasource+GetParam(sParamPrAmountField)])));
  fieldname := '';
  tablename := '';
  case code_type of
    '$'{earnings},'H'{hours},'D'{deductions}:
                  begin
                    if code_type = 'D' then
                      eord := 'D'
                    else
                      eord := 'E';
                    ed_custom_number := Matchers.Get(sEDBindingName).RightMatch( VarArrayOf( [eord, code] ) );
                    if not VarIsNull( ed_custom_number ) then
                    begin
                      //S( 'ED: [%s,%s] => <%s>', [eord, code, Vartostr(ed_custom_number)]);
                      Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', ed_custom_number);
                      if code_type in ['D','$'] then
                        fieldname := 'AMOUNT'
                      else
                        fieldname := 'HOURS_OR_PIECES';
                      tablename := 'PR_CHECK_LINES';
                      if not (UpperCase(VarToStr(ed_custom_number))[1] in ['D','M']) then
                        Output.Add( tablename, 'RATE_NUMBER', -1 ); //as Roma said
                      Output.Add( tablename, 'LINE_TYPE', CHECK_LINE_TYPE_USER );
                      Inc(CheckLineNbr);
                      Keys.Add( tablename, 'E_D_DIFFERENTIAL_AMOUNT', CheckLineNbr );
                    end
                  end;
  end;
  if fieldname <> '' then
    Output.Add( tablename, fieldname, amt );
end;

procedure TPayImportDefinition.EA_EY_state_ALL(const Input, Keys, Output: TFieldValues);
var
  code_type: char;
//  code: string;
  amt: Variant;
  vasource: string;
begin
  vasource := 'EA_EY_state.';
  if (Input[vasource+'TAX_TABLE'] = 'NY') and
     (Input[vasource+'CODE'] = 'DD') then
    Exit;
  code_type := GetSingleChar( Input[vasource+'CODE_TYPE'], vasource+'CODE_TYPE', sKnownCodes );
//    code := UpperCase(trim(VarToStr(Input[vasource+'CODE'])));
  amt := UpperCase(trim(VarToStr(Input[vasource+GetParam(sParamPrAmountField)])));

  FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE_STATES'), 'EE_NBR = ' + IntToStr(FImpEngine.IDS('EE').FieldByName('EE_NBR').AsInteger) + ' and CO_STATES_NBR = ' + IntToStr(FImpEngine.IDS('CO_STATES').FieldByName('CO_STATES_NBR').AsInteger));
  if FImpEngine.IDS('EE_STATES').RecordCount = 0 then
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'), 'STATE = ''' + Input[vasource+'TAX_TABLE'] + '''');
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATE_MARITAL_STATUS'), 'SY_STATES_NBR = ' + IntToStr(FImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsInteger));
    FImpEngine.IDS('SY_STATE_MARITAL_STATUS').IndexFieldNames := 'SY_STATE_MARITAL_STATUS_NBR';
    FImpEngine.IDS('SY_STATE_MARITAL_STATUS').First;
    Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', FImpEngine.IDS('SY_STATE_MARITAL_STATUS')['STATUS_TYPE']);
  end;

  case code_type of
    'T'{taxes}:
       if Input[vasource+'CODE'] = 'ST' then
         Output.Add( 'PR_CHECK_STATES', 'STATE_TAX', amt )
       else if Input[vasource+'CODE'] = 'DD' then
         Output.Add( 'PR_CHECK_STATES', 'EE_SDI_TAX', amt )
       else if Input[vasource+'CODE'] = 'DI' then
         Output.Add( 'PR_CHECK_STATES', 'ER_SDI_TAX', amt );
//7827
    'X'{taxable}:
       if Input[vasource+'CODE'] = 'ST' then
         Output.Add( 'PR_CHECK_STATES', 'STATE_TAXABLE_WAGES', amt )
       else if Input[vasource+'CODE'] = 'DD' then
         Output.Add( 'PR_CHECK_STATES', 'EE_SDI_TAXABLE_WAGES', amt )
       else if Input[vasource+'CODE'] = 'DI' then
         Output.Add( 'PR_CHECK_STATES', 'ER_SDI_TAXABLE_WAGES', amt );
  end;
end;
//PR_CHECK_LOCALS

procedure TPayImportDefinition.EA_EY_local_ALL(const Input, Keys,
  Output: TFieldValues);
var
  code_type: char;
  code: string;
  amt: Variant;
  vasource: string;
  local, sylocalsnbr, taxblock: variant;
begin
  vasource := 'EA_EY_local.';
  code_type := GetSingleChar( Input[vasource+'CODE_TYPE'], vasource+'CODE_TYPE', sKnownCodes );
  code := UpperCase(trim(VarToStr(Input[vasource+'CODE'])));
  amt := UpperCase(trim(VarToStr(Input[vasource+GetParam(sParamPrAmountField)])));
  local := null;
  sylocalsnbr := null;
  if code = 'NW' then
    sylocalsnbr := 118
  else
  begin
    if code = 'LC' then
    begin
      taxblock := Input[vasource+'TAX_TABLE'];
      if LeftStr(taxblock, 2) = 'MD' then
      begin
        if taxblock = 'MDALEG' then
          taxblock := '1'
        else if taxblock = 'MDANNE' then
          taxblock := '2'
        else if taxblock = 'MDBALT' then
          taxblock := '3'
        else if taxblock = 'MDBLTC' then
          taxblock := '4'
//        else if taxblock = 'MDCARL' then
//          taxblock := '5'
        else if taxblock = 'MDCALV' then
          taxblock := '5'
        else if taxblock = 'MDCARR' then
          taxblock := '6'
        else if taxblock = 'MDCECL' then
          taxblock := '8'
        else if taxblock = 'MDCHAR' then
          taxblock := '24'
        else if taxblock = 'MDDRCH' then
          taxblock := '9'
        else if taxblock = 'MDFRED' then
          taxblock := '10'
//        else if taxblock = 'MDGARR' then
//          taxblock := '12'
        else if taxblock = 'MDHARF' then
          taxblock := '23'
        else if taxblock = 'MDHWRD' then
          taxblock := '12'
        else if taxblock = 'MDKENT' then
          taxblock := '13'
        else if taxblock = 'MDMONT' then
          taxblock := '14'
        else if taxblock = 'MDPRGR' then
          taxblock := '15'
        else if taxblock = 'MDQUAN' then
          taxblock := '16'
        else if taxblock = 'MDSOMR' then
          taxblock := '25'
        else if taxblock = 'MDSTMR' then
          taxblock := '17'
        else if taxblock = 'MDTALB' then
          taxblock := '18'
        else if taxblock = 'MDWASH' then
          taxblock := '19'
        else if taxblock = 'MDWICO' then
          taxblock := '20'
        else if taxblock = 'MDWORC' then
          taxblock := '21'
        else
          taxblock := null;
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_STATES'), 'CO_NBR = ' + FImpEngine.IDS('CO').FieldByName('CO_NBR').AsString + ' and STATE = ''MD''');
        FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE_STATES'), 'EE_NBR = ' + IntToStr(FImpEngine.IDS('EE').FieldByName('EE_NBR').AsInteger) + ' and CO_STATES_NBR = ' + IntToStr(FImpEngine.IDS('CO_STATES').FieldByName('CO_STATES_NBR').AsInteger));
        if FImpEngine.IDS('EE_STATES').RecordCount = 0 then
        begin
          Keys.Add( 'CO_STATES', 'STATE', 'MD' );
          Output.Add( 'EE_STATES', 'STATE_MARITAL_STATUS', taxblock );
        end;

        FImpEngine.CurrentDataRequired(FImpEngine.IDS('PR_CHECK_STATES'), 'EE_STATES_NBR = ' + IntToStr(FImpEngine.IDS('EE_STATES').FieldByName('EE_STATES_NBR').AsInteger) + ' and PR_NBR = ' + IntToStr(FImpEngine.IDS('PR').FieldByName('PR_NBR').AsInteger));

        case code_type of
          'T'{taxes}:
            begin
             amt := amt + FImpEngine.IDS('PR_CHECK_STATES').FieldByName('STATE_TAX').AsFloat;
             Output.Add( 'PR_CHECK_STATES', 'STATE_TAX', amt );
            end;
      //7827
          'X'{taxable}:
            begin
             amt := amt + FImpEngine.IDS('PR_CHECK_STATES').FieldByName('STATE_TAXABLE_WAGES').AsFloat;
             Output.Add( 'PR_CHECK_STATES', 'STATE_TAXABLE_WAGES', amt );
            end;
        end;
        local := null;
      end
      else
        local := Input[vasource+'TAX_TABLE'] 
    end
    else
      local := Input[vasource+'CD_TT1_CODE'];
    Assert( not VarIsEmpty(local) );
    if not VarIsNull(local) then
      sylocalsnbr := Matchers.Get(sLocalsBindingName).RightMatch( local );
  end;
  if not VarIsNull(sylocalsnbr) then
  begin
    Keys.Add( 'SY_LOCALS', 'SY_LOCALS_NBR', sylocalsnbr );
    Keys.Add( 'CO_LOCAL_TAX', '', Unassigned );
    Keys.Add( 'EE_LOCALS', '', Unassigned );

    FImpEngine.CurrentDataRequired(FImpEngine.IDS('CO_LOCAL_TAX'),
      Format('(CO_NBR=%s) and (SY_LOCALS_NBR = %s)',
        [VarToStr(FImpEngine.IDS('CO')['CO_NBR']), VarToStr(sylocalsnbr)] ) );
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE_LOCALS'), 'EE_NBR = ' + IntToStr(FImpEngine.IDS('EE').FieldByName('EE_NBR').AsInteger) + ' and CO_LOCAL_TAX_NBR = ' + IntToStr(FImpEngine.IDS('CO_LOCAL_TAX').FieldByName('CO_LOCAL_TAX_NBR').AsInteger));
    if FImpEngine.IDS('EE_LOCALS').RecordCount = 0 then
      Output.Add( 'EE_LOCALS', 'LOCAL_ENABLED', GROUP_BOX_NO);

    case code_type of
      'T'{taxes}:
           Output.Add( 'PR_CHECK_LOCALS', 'LOCAL_TAX', amt );
//7827
      'X'{taxable}:
         begin
           Output.Add( 'PR_CHECK_LOCALS', 'LOCAL_TAXABLE_WAGE', amt );
         end;

    end;
  end;
end;

procedure TPayImportDefinition.E1_payroll_fixup_ALL(const Input, Keys, Output: TFieldValues);
begin
  if FImpEngine.IDS('PR_CHECK').FieldByName('PR_CHECK_NBR').AsString <> '' then
  begin
    FImpEngine.CurrentDataRequired( FImpEngine.IDS('PR_CHECK_STATES'), 'PR_CHECK_NBR=' + FImpEngine.IDS('PR_CHECK').FieldByName('PR_CHECK_NBR').AsString);
    if FImpEngine.IDS('PR_CHECK_STATES').RecordCount = 0 then
    begin
      Keys.Add('EE_STATES', 'EE_STATES_NBR', FImpEngine.IDS('EE')['HOME_TAX_EE_STATES_NBR'] );
      Output.Add( 'PR_CHECK_STATES', 'STATE_TAX', 0);
      Output.Add( 'PR_CHECK_STATES', 'EE_SDI_TAX', 0);
      Output.Add( 'PR_CHECK_STATES', 'ER_SDI_TAX', 0);
      //taxable wages are null
    end;
  end;
end;

procedure TPayImportDefinition.EA_EY_sui_ALL(const Input, Keys,
  Output: TFieldValues);
var
  code_type: char;
  code: string;
  amt: Variant;
  vasource: string;
  sui, sysuinbr: variant;
begin
  vasource := 'EA_EY_sui.';
  code_type := GetSingleChar( Input[vasource+'CODE_TYPE'], vasource+'CODE_TYPE', sKnownCodes );
  code := UpperCase(trim(VarToStr(Input[vasource+'CODE'])));
  amt := UpperCase(trim(VarToStr(Input[vasource+GetParam(sParamPrAmountField)])));
  sui := Input[vasource+'TAX_TABLE'];

  sysuinbr := null;
  if (sui = 'CA') and (Code = 'TR') then
    sysuinbr := 64
  else if (sui = 'NJ') and (Code = 'UD') then
    sysuinbr := 89
  else if (sui = 'NJ') and (Code = 'UI') then
    sysuinbr := 90
  else
  begin
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'));
    if not FImpEngine.IDS('SY_STATES').Locate('STATE', sui, []) then
      Exit;
    FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_SUI'), 'SY_STATES_NBR = ' + FImpEngine.IDS('SY_STATES').FieldByName('SY_STATES_NBR').AsString);
    if code = 'UI' then
      sysuinbr := FImpEngine.IDS('SY_SUI').Lookup('EE_ER_OR_EE_OTHER_OR_ER_OTHER', GROUP_BOX_ER, 'SY_SUI_NBR')
    else if code = 'UD' then
      sysuinbr := FImpEngine.IDS('SY_SUI').Lookup('EE_ER_OR_EE_OTHER_OR_ER_OTHER', GROUP_BOX_EE, 'SY_SUI_NBR');
  end;

  if not VarIsNull(sysuinbr) then
  begin
    Keys.Add( 'CO_SUI', 'SY_SUI_NBR', sysuinbr );
    case code_type of
      'T'{taxes}:
           Output.Add( 'PR_CHECK_SUI', 'SUI_TAX', amt );
//7827
      'X'{taxable}:
         begin
           Output.Add( 'PR_CHECK_SUI', 'SUI_TAXABLE_WAGES', amt );
         end;
    end;
  end;
end;

function TPayImportDefinition.SetCustomEDCodeKeyForSchedEDS(Keys: TFieldValues; eord, code: string): boolean; //returns success indicator
var
  ed_custom_number: Variant;
begin
  ed_custom_number := Matchers.Get(sEDBindingName).RightMatch( VarArrayOf( [eord, code] ) );
  Result := not VarIsNull( ed_custom_number );
  if Result then
  begin
    //ODS( 'ED: [%s,%s] => <%s>', [eord, code, Vartostr(ed_custom_number)]);
    Keys.AddFirst( 'CL_E_DS', 'CUSTOM_E_D_CODE_NUMBER', ed_custom_number);
    //now check that (EE_NBR;CL_E_DS) is enough to uniquely identify record and give user friendly message if any
    //works only for already existing ambiguities, doesn't help in case of mapping that maps different PS codes to the same Evo code;
    //added user-friendly exception in iemap
    FImpEngine.CurrentDataRequired( FImpEngine.IDS('CL_E_DS'), 'CUSTOM_E_D_CODE_NUMBER='''+VarToStr(ed_custom_number)+'''' );
    if FImpEngine.IDS('CL_E_DS').RecordCount = 1 then
    begin
      FImpEngine.CurrentDataRequired(FImpEngine.IDS('EE_SCHEDULED_E_DS'), Format('(EE_NBR=%d) and (CL_E_DS_NBR=%d)',[FImpEngine.IDS('EE').FieldByName('EE_NBR').AsInteger, FImpEngine.IDS('CL_E_DS').FieldByName('CL_E_DS_NBR').AsInteger] ) );
      if FImpEngine.IDS('EE_SCHEDULED_E_DS').RecordCount > 1 then
        raise EevException.CreateFmt( 'Employee <%s> has more than one ED <%s> scheduled (in Evolution database). Don''t know which scheduled ED should be used for import', [VarToStr(FImpEngine.IDS('EE')['CUSTOM_EMPLOYEE_NUMBER']), VarToStr(ed_custom_number)] );
{                if iemap.IDS('CL_E_DS')['SCHEDULED_DEFAULTS'] = GROUP_BOX_YES then
        Keys.Add( 'EE_SCHEDULED_E_DS', 'EFFECTIVE_START_DATE', iemap.IDS('CL_E_DS')['SD_EFFECTIVE_START_DATE'] )
      else
        Keys.Add( 'EE_SCHEDULED_E_DS', 'EFFECTIVE_START_DATE', Null ); //create new record cause field is not null and is initialized in AI_SCHEDDULED_E_DS
      Keys.Add( 'EE_SCHEDULED_E_DS', 'EFFECTIVE_END_DATE', Null );
}
    end
    else
      ; //we get error message from AI_CL_E_DS
  end
end;

procedure TPayImportDefinition.HelpImportDDReference(Output: TFieldValues; aba, act, atype: Variant);
begin
  if not VarIsNull( aba ) and not VarIsNull( act ) then
  begin
    FImpEngine.CurrentDataRequired(
      FImpEngine.IDS('EE_DIRECT_DEPOSIT'),
      Format( '(EE_NBR=%d) and (EE_ABA_NUMBER=''%s'') and (EE_BANK_ACCOUNT_NUMBER=''%s'') and (EE_BANK_ACCOUNT_TYPE=''%s'')',
               [FImpEngine.IDS('EE').FieldByName('EE_NBR').AsInteger, aba, act, atype])
    );
    Assert( FImpEngine.IDS('EE_DIRECT_DEPOSIT').RecordCount <= 1 );
    case FImpEngine.IDS('EE_DIRECT_DEPOSIT').RecordCount of
      0: Logger.LogWarningFmt( 'Unable to locate Ee Direct Deposit for EE_ABA_NUMBER = <%s> and EE_BANK_ACCOUNT_NUMBER = <%s>', [aba, act] );
      1: Output.Add( 'EE_SCHEDULED_E_DS', 'EE_DIRECT_DEPOSIT_NBR', FImpEngine.IDS('EE_DIRECT_DEPOSIT')['EE_DIRECT_DEPOSIT_NBR'] )
    else
      Logger.LogWarningFmt( 'Unable to uniquely identify Ee Direct Deposit for EE_ABA_NUMBER = <%s> and EE_BANK_ACCOUNT_NUMBER = <%s>', [aba, act] );
    end
  end
end;

procedure TPayImportDefinition.EB_np_ALL(const Input, Keys, Output: TFieldValues);
begin
  if (Input['EB_np.CODE'] = 'NP') and SetCustomEDCodeKeyForSchedEDS( Keys, 'D', 'NP') then
  begin
    Output.Add( 'EE_SCHEDULED_E_DS', 'CALCULATION_TYPE', CALC_METHOD_NONE );
    Output.Add( 'EE_SCHEDULED_E_DS', 'DEDUCT_WHOLE_CHECK', GROUP_BOX_YES );
    HelpImportDDReference( Output, Input['EB_np.S10_BANK_ABA'], Input['EB_np.ACT_NUM'], Input['EB_np.CHECK_SAVE'] );
  end
end;

procedure TPayImportDefinition.ED_ALL(const Input, Keys, Output: TFieldValues);
var
  code_type: char;
  how_type: char;
  when_type: string;
  calc_type: char;

  procedure ImportScheduledDeduction;
  var
    amt, perc, tg_amt: Variant;
    amtfield, percfield: string;
    weeks: set of 1..5;
    wi: integer;
    which_checks: Variant;
  begin
    how_type := GetSingleChar( Input['ED.CD_HOW_TYPE'], 'ED.CD_HOW_TYPE', [] );
    when_type := trim(VarToStr( Input['ED.CD_WHEN_TYPE'] ));
    amt := Null;
    perc := Null;
    amtfield := 'AMOUNT';
    percfield := 'PERCENTAGE';
    case how_type of
      'P': begin
             perc := Input['ED.AMOUNT'];
             calc_type := CALC_METHOD_DISP_EARN;
           end;
      '$': begin
             amt := Input['ED.AMOUNT'];
             calc_type := CALC_METHOD_FIXED;
           end;
      'C': begin
             perc := Input['ED.AMOUNT'];
             calc_type := CALC_METHOD_RATE_HOURS;
           end;
    else
      RaiseUnknownCode(  'Don''t know how to import scheduled deduction with CD.HOW_TYPE = <%s>',[how_type] );
    end;
    if FImpEngine.IDS('CL_E_DS')['SCHEDULED_DEFAULTS'] <> GROUP_BOX_YES then
      Output.Add( 'EE_SCHEDULED_E_DS', 'CALCULATION_TYPE', calc_type )
    else
      Output.Add( 'EE_SCHEDULED_E_DS', 'CALCULATION_TYPE', FImpEngine.IDS('CL_E_DS')['SD_CALCULATION_METHOD'] );
    if when_type = '1' then
    begin
      amtfield := 'THRESHOLD_AMOUNT';
      percfield := '';
    end
    else if when_type = '4' then
    begin
      amtfield := 'ANNUAL_MAXIMUM_AMOUNT';
      percfield := '';
    end
    else if when_type = '5' then
    begin
      amtfield := 'MAXIMUM_PAY_PERIOD_AMOUNT';
      percfield := 'MAXIMUM_PAY_PERIOD_PERCENTAGE';
    end
    else if when_type = '6' then
    begin
      amtfield := 'MINIMUM_PAY_PERIOD_AMOUNT';
      percfield := 'MINIMUM_PAY_PERIOD_PERCENTAGE';
    end;
    if amtfield <> '' then
      Output.Add( 'EE_SCHEDULED_E_DS', amtfield, amt );
    if percfield <> '' then
      Output.Add( 'EE_SCHEDULED_E_DS', percfield, perc );

    tg_amt := Input['ED.GOAL'];
    if tg_amt = 0 then
      tg_amt := null;
    Output.Add( 'EE_SCHEDULED_E_DS', 'TARGET_AMOUNT', tg_amt );
    if not VarIsNull(tg_amt) and (tg_amt <> 0) then
      Output.Add( 'EE_SCHEDULED_E_DS', 'TARGET_ACTION', GetParam(sEDTargetAction) )
    else
      Output.Add( 'EE_SCHEDULED_E_DS', 'TARGET_ACTION', SCHED_ED_TARGET_NONE);

    weeks := [];
    for wi := 1 to 5 do
      if VarToStr(Input['ED.CD_DED_SCHED_'+inttostr(wi)]) <> '' then
        Include(weeks, wi);

    if weeks = [1,2,3,4,5] then
      which_checks := GROUP_BOX_ALL
    else if weeks = [1,2] then
      which_checks := GROUP_BOX_FIRST
    else if weeks = [3] then
      which_checks := GROUP_BOX_CLOSEST_TO_15
    else if weeks = [4,5] then
      which_checks := GROUP_BOX_LAST
    else
      Logger.LogWarning( 'Unable to deduce value of EE_SCHEDULED_E_DS.WHICH_CHECKS from values of DED_SCHED_1..DED_SCHED_5 fields' );
    if not VarIsEmpty(which_checks) then
      Output.Add( 'EE_SCHEDULED_E_DS', 'WHICH_CHECKS', which_checks );
    HelpImportDDReference( Output, Input['ED.EB_S10_BANK_ABA'], Input['ED.EB_ACT_NUM'], Input['ED.EB_CHECK_SAVE'] );
  end;
var
  code: string;
  eord: string;
begin
  code_type := GetSingleChar( Input['ED.CODE_TYPE'], 'ED.CODE_TYPE', sKnownCodes );
  code := UpperCase(trim(VarToStr(Input['ED.CODE'])));
  case code_type of
    '$'{earnings}, 'D'{deductions}:
      begin
        if code_type = 'D' then
          eord := 'D'
        else
          eord := 'E';
//          if VarToStr(Matchers.Get(sEDBindingName).ExtraDataByLeftKey( VarArrayOf( [eord, code] ), 'SCHEDULED' )) = 'Y' then
        if SetCustomEDCodeKeyForSchedEDS( Keys, eord, code ) then
          if eord = 'D' then
            ImportScheduledDeduction
          else //'E'
            Output.Add( 'EE_SCHEDULED_E_DS', 'AMOUNT', Input['ED.AMOUNT'] );
      end;
  else
    RaiseUnknownCode(  'Don''t know how to import sched E/D with this ED.CODE_TYPE: <%s>',[VarToStr(Input['ED.CODE_TYPE'])] );
  end; //of case
end;

procedure TPayImportDefinition.AI_EE_SCHEDULED_E_DS(const ds: TevClientDataSet; const Keys: TFieldValues);
  procedure SetDefault( fname: string; srcfname: string = '' );
  begin
    if srcfname = '' then
      srcfname := 'SD_'+fname;
    if not VarIsNull( FImpEngine.IDS('CL_E_DS')[srcfname] ) then
      ds[fname] := FImpEngine.IDS('CL_E_DS')[srcfname];
  end;
var
  Q: IevQuery;
begin
  ds['EFFECTIVE_START_DATE'] := Date;
  ds['FREQUENCY'] := SCHED_FREQ_DAILY;
  ds['EXCLUDE_WEEK_1'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_2'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_3'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_4'] := GROUP_BOX_NO;
  ds['EXCLUDE_WEEK_5'] := GROUP_BOX_NO;
  if FImpEngine.IDS('CL_E_DS')['SCHEDULED_DEFAULTS'] = GROUP_BOX_YES then
  begin
    SetDefault( 'FREQUENCY' );
    SetDefault( 'EFFECTIVE_START_DATE' );
    SetDefault( 'EXCLUDE_WEEK_1' );
    SetDefault( 'EXCLUDE_WEEK_2' );
    SetDefault( 'EXCLUDE_WEEK_3' );
    SetDefault( 'EXCLUDE_WEEK_4' );
    SetDefault( 'EXCLUDE_WEEK_5' );
    SetDefault( 'CALCULATION_TYPE', 'SD_CALCULATION_METHOD' );
    SetDefault( 'EXPRESSION' );
    SetDefault( 'CL_E_D_GROUPS_NBR', 'CL_E_D_GROUPS_NBR' );
    SetDefault( 'CL_AGENCY_NBR', 'DEFAULT_CL_AGENCY_NBR' );
    SetDefault( 'AMOUNT' );
    SetDefault( 'PERCENTAGE', 'SD_RATE' );
     /////////////////////////////////////
    SetDefault( 'WHICH_CHECKS');
    SetDefault( 'PRIORITY_NUMBER');
    SetDefault( 'ALWAYS_PAY');
    SetDefault( 'PLAN_TYPE');
    SetDefault( 'MAX_AVG_AMT_CL_E_D_GROUPS_NBR','SD_MAX_AVG_AMT_GRP_NBR');
    SetDefault( 'MAX_AVG_HRS_CL_E_D_GROUPS_NBR','SD_MAX_AVG_HRS_GRP_NBR' );
    SetDefault( 'MAX_AVERAGE_HOURLY_WAGE_RATE', 'SD_MAX_AVERAGE_HOURLY_WAGE_RATE');
    SetDefault( 'THRESHOLD_E_D_GROUPS_NBR','SD_THRESHOLD_E_D_GROUPS_NBR');
    SetDefault( 'THRESHOLD_AMOUNT', 'SD_THRESHOLD_AMOUNT' );
    SetDefault( 'DEDUCTIONS_TO_ZERO', 'SD_DEDUCTIONS_TO_ZERO' );
    Q := TevQuery.Create('SELECT CL_E_DS_NBR, CO_BENEFIT_SUBTYPE_NBR FROM CO_E_D_CODES WHERE {AsOfNow<CO_E_D_CODES>} and CL_E_DS_NBR=' + IntToStr(FImpEngine.IDS('CL_E_DS')['CL_E_DS_NBR']));
    Q.Execute;
    if Q.Result.RecordCount > 0 then
      ds['CO_BENEFIT_SUBTYPE_NBR'] := Q.Result.FieldByName('CO_BENEFIT_SUBTYPE_NBR').Value;
    /////////////////////////////////////
  end
end;

procedure TPayImportDefinition.EB_IN_PRENOTE(const Table, Field: string; const Value: Variant; const Keys, Output: TFieldValues);
var
  inprenote: Variant;
begin
  if trim(VarToStr(Value)) = ''  then
    inprenote := GROUP_BOX_YES
  else
    inprenote := GROUP_BOX_NO;
//  ODS('prenote dt: ' +trim(VarToStr(Value)));
  Output.Add( 'EE_DIRECT_DEPOSIT', 'IN_PRENOTE', inprenote );
end;


procedure TPayImportDefinition.E1_STATE(const Table, Field: string;
  const Value: Variant; const Keys, Output: TFieldValues);
begin
  FImpEngine.CurrentDataRequired(FImpEngine.IDS('SY_STATES'));
  Output.Add( 'CL_PERSON', 'STATE', Value );
  Output.Add( 'CL_PERSON', 'RESIDENTIAL_STATE_NBR', FImpEngine.IDS('SY_STATES').Lookup('STATE', Value, 'SY_STATES_NBR') );
end;

{ TPayCO_AND_EEImport }

procedure TPayCO_AND_EEImport.DoRunImport(aImportDefinition: IImportDefinition; aParams: TStrings);
  function CompanyHasDBDT: boolean;
  var
    ds: TDataSet;
  begin
    Result := false;
    ds := DataModule.GetTable('CV');
    ds.First;
    while not ds.Eof do
    begin
      if trim(ds.FieldByName('Code').AsString) <> '' then
      begin
        Result := true;
        Exit;
      end;
      ds.Next;
    end;

    ds := DataModule.GetTable('CP');
    ds.First;
    while not ds.Eof do
    begin
      if (trim(ds.FieldByName('Code').AsString) <> '') or (trim(ds.FieldByName('Name').AsString) <> '') then
      begin
        Result := true;
        Exit;
      end;
      ds.Next;
    end;
  end;
var
  EETerm: Boolean;
  hasDBDT: boolean;
begin
  EETerm := aParams.Values[sEETermedParam] = 'Y';
  DataModule.GetTable('E1').Tag := Integer(EETerm);
  DataModule.GetTable('E1').Filtered := False;
  DataModule.GetTable('E1').Filtered := True;
  DataModule.GetTable('ET').Filtered := EETerm;
  DataModule.GetTable('EA').Filtered := EETerm;
  DataModule.GetTable('EA_checklines').Filtered := EETerm;
  DataModule.GetTable('EA_EY_state').Filtered := EETerm;
  DataModule.GetTable('EA_EY_local').Filtered := EETerm;
  DataModule.GetTable('EA_EY_sui').Filtered := EETerm;
  DataModule.GetTable('EB').Filtered := EETerm;
  DataModule.GetTable('ED').Filtered := EETerm;

  aImportDefinition.SetParam( sEDTargetAction, aParams.Values[sEDTargetAction] );
  aImportDefinition.SetFixedKeys( 'CO.CUSTOM_COMPANY_NUMBER=' + DataModule.CustomCompanyNumber+#13#10'CO_DIVISION.CUSTOM_DIVISION_NUMBER='+sHiddenDivisionCode );

  hasDBDT := CompanyHasDBDT;
  aImportDefinition.SetParam( sHasDBDT, hasDBDT);
  if hasDBDT then
  begin
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'CV', 'Importing divisions to Evolution branches' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'CP', 'Importing departments' );
  end
  else
    Logger.LogEvent('Both CV and CP tables don''t have non-empty CODE or NAME fields. Branch and Department import skipped.');

  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'E1', 'Importing employees' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'E1_fixup', 'Importing employee''s references to home and SUI states' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'ET', 'Importing employee''s taxes' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EB', 'Importing EE Direct Deposit' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EB_np', 'Importing Net Payroll' );
  ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'ED', 'Importing employee''s scheduled E/Ds' );
end;

type
  TPrDef = record
    AmountField: string;
    CheckDate: TDate;
    PeriodBeginDate: TDate;
    PeriodEndDate: TDate;
    CheckType: char;
    TaskName: string;
  end;
  TPrDefs = array of TPrDef;

function DateIsIn1stQuarter( aDate: TDate ): boolean;
var
  y, m, d: word;
begin
  DecodeDate( aDate, y, m, d );
  Result := m in [1..3];
end;

function DaysPerMonth(AYear, AMonth: Integer): Integer;
const
  DaysInMonth: array[1..12] of Integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
begin
  Result := DaysInMonth[AMonth];
  if (AMonth = 2) and IsLeapYear(AYear) then Inc(Result); { leap-year Feb is special }
end;

function DateIsQuarterEnd( aDate: TDate ): boolean;
var
  y, m, d: word;
begin
  DecodeDate( aDate, y, m, d );
  Result := ( m in [3,6,9,12] ) and ( d = DaysPerMonth(y, m) );
end;

procedure  CalcPayrollDefs( aCheckDate: TDate; var aPrDefs: TPrDefs );
var
  y, m, d: Word;
  EndOfQuarterMonth: Word;
begin
  DecodeDate( aCheckDate, y, m, d );
  if DateIsIn1stQuarter( aCheckDate ) or DateIsQuarterEnd( aCheckDate ) then // ytd only
  begin
    SetLength( aPrDefs, 1 );
    with aPrDefs[0] do
    begin
      AmountField := 'YTD_ACCUM';
      PeriodBeginDate := EncodeDate( y, 1, 1);
      PeriodEndDate := aCheckDate;
      CheckDate := PeriodEndDate;
      CheckType := CHECK_TYPE2_MANUAL;//CHECK_TYPE2_YTD;
      TaskName := 'Importing YTD';
    end;
  end
  else
  begin
    SetLength( aPrDefs, 2 );
    EndOfQuarterMonth := ( ( (m-1) div 3 ) * 3 - 1 ) + 1; // we are not in the 1st quarter
    with aPrDefs[0] do
    begin
      AmountField := 'YTD_less_QTD';
      PeriodBeginDate := EncodeDate( y, 1, 1);
      PeriodEndDate := EncodeDate( y, EndOfQuarterMonth, DaysPerMonth(y, EndOfQuarterMonth) );
      CheckDate := PeriodEndDate;
      CheckType := CHECK_TYPE2_MANUAL; //CHECK_TYPE2_YTD;
      TaskName := 'Importing YTD-QTD as YTD';
    end;
    with aPrDefs[1] do
    begin
      AmountField := 'QTD_ACCUM';
      PeriodBeginDate := EncodeDate( y, EndOfQuarterMonth + 1, 1);
      PeriodEndDate := aCheckDate;
      CheckDate := PeriodEndDate;
      CheckType := CHECK_TYPE2_MANUAL; //CHECK_TYPE2_QTD;
      TaskName := 'Importing QTD';
    end;
  end;
end;

{ TPayPRImport }

procedure TPayPRImport.DoRunImport(aImportDefinition: IImportDefinition; aParams: TStrings);
  function GenerateFixedKeys( const aPrDef: TPrDef ): string;
  begin
    Result := 'CO.CUSTOM_COMPANY_NUMBER=' + DataModule.CustomCompanyNumber
              +#13#10+'PR.CHECK_DATE=' + DateToStr(aPrDef.CheckDate)
              +#13#10+'PR.RUN_NUMBER=1'
              +#13#10+'PR_BATCH.PERIOD_BEGIN_DATE=' + DateToStr(aPrDef.PeriodBeginDate)
              +#13#10+'PR_BATCH.PERIOD_END_DATE=' + DateToStr(aPrDef.PeriodEndDate)
              +#13#10+'PR_BATCH.FREQUENCY=' + aParams.Values[sParamBatchFrequency]
              +#13#10+'PR_CHECK.CHECK_TYPE=' + aPrDef.CheckType
              ;
  end;
var
  PrDefs: TPrDefs;
  i: integer;
  EETerm: Boolean;
begin
  EETerm := aParams.Values[sEETermedParam] = 'Y';
  DataModule.GetTable('E1').Tag := Integer(EETerm);
  DataModule.GetTable('E1').Filtered := False;
  DataModule.GetTable('E1').Filtered := True;
  DataModule.GetTable('ET').Filtered := EETerm;
  DataModule.GetTable('EA').Filtered := EETerm;
  DataModule.GetTable('EA_checklines').Filtered := EETerm;
  DataModule.GetTable('EA_EY_state').Filtered := EETerm;
  DataModule.GetTable('EA_EY_local').Filtered := EETerm;
  DataModule.GetTable('EA_EY_sui').Filtered := EETerm;
  DataModule.GetTable('EB').Filtered := EETerm;
  DataModule.GetTable('ED').Filtered := EETerm;

  CalcPayrollDefs( StrToDate(aParams.Values[sParamCheckDate]), PrDefs );
  for i := 0 to high(PrDefs) do
  begin
    aImportDefinition.SetParam( sParamPrAmountField, PrDefs[i].AmountField );
    aImportDefinition.SetFixedKeys( GenerateFixedKeys( PrDefs[i] ) );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EA', PrDefs[i].TaskName +' (federal taxes)' ); //and taxable wages (*7827*)
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EA_checklines', PrDefs[i].TaskName +' (E/Ds)' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EA_EY_state', PrDefs[i].TaskName +' (state taxes and taxable wages)' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EA_EY_local', PrDefs[i].TaskName +' (local taxes and taxable wages)' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'EA_EY_sui', PrDefs[i].TaskName +' (SUI taxes and taxable wages)' );
    ImportTable(FImpEngine, Logger, DataModule, aImportDefinition, 'E1_payroll_fixup', PrDefs[i].TaskName +' (attaching missing states)' );
  end;
end;

{ TEDAutoMapper }

function TEDAutoMapper.MapToRightKeys( aLeftTable: TDataSet; bd: TBindingDesc; aRightTable: TDataSet): Variant;
begin
  Result := aRightTable.Lookup( 'CUSTOM_E_D_CODE_NUMBER', UpperCase(Vartostr(aLeftTable['eord']) + Vartostr(aLeftTable['CODE'])), bd.RightDesc.KeyFields );
end;

end.


