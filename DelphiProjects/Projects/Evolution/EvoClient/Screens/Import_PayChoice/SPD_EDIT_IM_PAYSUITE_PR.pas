// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_PAYSUITE_PR;

interface                                                  


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_IM_PAYSUITE_BASE, ImgList,  ActnList, Db, Wwdatsrc,
  sTablespaceView, StdCtrls, Buttons, ExtCtrls, ComCtrls,
  sPrImportParam, gdyBinderVisual, genericImport,
  sImportQueue, sImportSystemAndUserDBPathsInput, sImportLoggerView,
  ISBasicClasses, EvUIComponents, LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_IM_PAYSUITE_PR = class(TEDIT_IM_PAYSUITE_BASE)
    TabSheet3: TTabSheet;
    BinderFrame1: TBinderFrame;
    TabSheet4: TTabSheet;
    BinderFrame2: TBinderFrame;
    ParamInput: TPrImportParamFrame;
  public
    { Public declarations }
    procedure Activate; override;
  end;


implementation

uses
  sPaysuiteImportDef, gdyBinder, sPayImportDef, sDataStructure,
  sImportVisualController, evUtils;

{$R *.DFM}

{ TEDIT_IM_PAYSUITE_PR }

procedure TEDIT_IM_PAYSUITE_PR.Activate;
  procedure InitController( aImport: IImport );
  var
    ivc: IImportVisualController;
  begin
    DM_CLIENT.CL_E_DS.EnsureHasMetadata;
    DM_COMPANY.CO_LOCAL_TAX.EnsureHasMetadata;
    DM_COMPANY.CO_SUI.EnsureHasMetadata;
    
    ivc := TImportVisualController.Create(
              LogFrame.LoggerKeeper,
              aImport,
              ParamInput,
              ViewerFrame
           );
    ivc.SetConnectionStringInputFrame( ConnectionStringInput );

    ivc.BindingKeeper(sEDBindingName).SetAutoMapper( TEDAutoMapper.Create );
    ivc.BindingKeeper(sEDBindingName).SetTables( aImport.DataModule.GetTable('eds'), DM_CLIENT.CL_E_DS );
    ivc.BindingKeeper(sEDBindingName).SetVisualBinder( BinderFrame1 );

    ivc.BindingKeeper(sLocalsBindingName).SetTables( aImport.DataModule.GetTable('locals'), DM_COMPANY.CO_LOCAL_TAX );
    ivc.BindingKeeper(sLocalsBindingName).SetVisualBinder( BinderFrame2 );

    SetController( ivc );
  end;
begin
  ParamInput.Activate;
  inherited;
  InitController( CreatePaysuitePRImport(LogFrame.LoggerKeeper.Logger) );
end;

initialization
  RegisterClass(TEDIT_IM_PAYSUITE_PR);


end.
