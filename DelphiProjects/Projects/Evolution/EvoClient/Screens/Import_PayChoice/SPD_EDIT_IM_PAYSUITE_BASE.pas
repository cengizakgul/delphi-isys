// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_IM_PAYSUITE_BASE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPD_EDIT_IM_BASE, ImgList,  ActnList, Db, Wwdatsrc,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, genericImport,
  sPayImportDef, sTablespaceView, sImportDatabasePathInput,
  sImportQueue, sImportSystemAndUserDBPathsInput, sImportLoggerView,
  ISBasicClasses, EvUIComponents, LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_IM_PAYSUITE_BASE = class(TEDIT_IM_BASE)
    ConnectionStringInput: TImportSystemAndUserDBPathsInputFrame;
  end;


implementation

{$R *.DFM}

{ TEDIT_IM_PAYSUITE_BASE }

end.
