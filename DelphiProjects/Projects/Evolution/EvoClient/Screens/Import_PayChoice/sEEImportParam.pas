// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sEEImportParam;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, wwdbedit, Wwdotdot, Wwdbcomb,  ComCtrls,
  sImportVisualcontroller, EvBasicUtils, ISBasicClasses, EvUIComponents,
  isUIwwDBComboBox;

type
  TEEImportParamFrame = class(TFrame, IParamInputFrame )
    evGroupBox1: TevGroupBox;
    evcbTargetAction: TevDBComboBox;
    evLabel3: TevLabel;
    cbTerm: TevCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Activate;
    procedure ParamsToControls( aParams: TStrings );
    procedure ControlsToParams( aParams: TStrings );
  end;

implementation

uses
  SFieldCodeValues, EvConsts, sPayImportDef;
{$R *.DFM}

procedure PopulateComboBox( cb: TevDBCombobox; C: string );
begin
  cb.MapList := True;
  cb.AllowClearKey := false;
  cb.Items.Text := C;
end;

{ TEEImportParamFrame }

procedure TEEImportParamFrame.Activate;
begin
  PopulateComboBox( evcbTargetAction, ScheduledEDTarget_ComboChoices );
end;

procedure TEEImportParamFrame.ControlsToParams(aParams: TStrings);
begin
  aParams.Values[sEDTargetAction] := evcbTargetAction.Value;
  aParams.Values[sEETermedParam] := BoolToYN(cbTerm.Checked);
end;

procedure TEEImportParamFrame.ParamsToControls(aParams: TStrings);
begin
  cbTerm.Checked := aParams.Values[sEETermedParam] = 'Y';
  if trim(aParams.Values[sEDTargetAction]) = '' then
    evcbTargetAction.Value := SCHED_ED_TARGET_REMOVE
  else
    evcbTargetAction.Value := aParams.Values[sEDTargetAction]
end;

end.
