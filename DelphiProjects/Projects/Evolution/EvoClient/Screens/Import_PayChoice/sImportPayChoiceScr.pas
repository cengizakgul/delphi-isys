// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit sImportPaychoiceScr;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SPackageEntry, ExtCtrls, Menus,  ImgList, EvCommonInterfaces,
  ComCtrls, ToolWin, StdCtrls, Buttons, ActnList, ISBasicClasses, EvMainboard,
  EvContext;

type
  TPaychoiceFrm = class(TFramePackageTmpl, IevImportPayChoiceScreens)
  protected
    function  InitPackage: Boolean; override;
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
  end;

implementation

{$R *.DFM}

var
  PaychoiceFrm: TPaychoiceFrm;

function GetImportPayChoiceScreens: IevImportPayChoiceScreens;
begin
  if not Assigned(PaychoiceFrm) then
    PaychoiceFrm := TPaychoiceFrm.Create(nil);
  Result := PaychoiceFrm;
end;


{ TPaychoiceFrm }

function TPaychoiceFrm.InitPackage: Boolean;
begin
  if Context.License.PaysuiteImport then
    Result := inherited InitPackage
  else
    Result := False;
end;

function TPaychoiceFrm.PackageCaption: string;
begin
  Result := 'PCPS';
end;

function TPaychoiceFrm.PackageSortPosition: string;
begin
  Result := '200';
end;

procedure TPaychoiceFrm.UserPackageStructure;
begin
  AddStructure('\&Company\Imports|060');
  AddStructure('\&Company\Imports\PaySuite Employees|283', 'TEDIT_IM_PAYSUITE_CO_AND_EE');
  AddStructure('\&Company\Imports\PaySuite Payrolls|284', 'TEDIT_IM_PAYSUITE_PR');

end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetImportPayChoiceScreens, IevImportPayChoiceScreens, 'Screens Import PayChoice');

finalization
  FreeAndNil(PaychoiceFrm);

end.
