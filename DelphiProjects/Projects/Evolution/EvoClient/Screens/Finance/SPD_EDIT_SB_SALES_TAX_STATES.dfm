inherited EDIT_SB_SALES_TAX_STATES: TEDIT_SB_SALES_TAX_STATES
  Width = 522
  Height = 482
  object dtxtSTATE: TevDBText [0]
    Left = 88
    Top = 8
    Width = 73
    Height = 13
    DataField = 'STATE'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lablSalesTaxState: TevLabel [1]
    Left = 8
    Top = 8
    Width = 75
    Height = 13
    Caption = 'Sales Tax State'
  end
  object PageControl1: TevPageControl [2]
    Left = 8
    Top = 24
    Width = 513
    Height = 401
    HelpContext = 22503
    ActivePage = tshtBrowse
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object lablState: TevLabel
        Left = 8
        Top = 272
        Width = 34
        Height = 16
        Caption = '~State'
        FocusControl = dedtState
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablTax_ID: TevLabel
        Left = 8
        Top = 320
        Width = 41
        Height = 16
        Caption = '~Tax ID'
        FocusControl = dedtTax_ID
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablPercentage: TevLabel
        Left = 56
        Top = 272
        Width = 58
        Height = 13
        Caption = '% Sales Tax'
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 131
        Height = 265
        DisableThemesInTitle = False
        Selected.Strings = (
          'STATE'#9'2'#9'State'#9'No')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_SALES_TAX_STATES\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object dedtState: TevDBEdit
        Left = 8
        Top = 288
        Width = 33
        Height = 21
        DataField = 'STATE'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtTax_ID: TevDBEdit
        Left = 8
        Top = 336
        Width = 122
        Height = 21
        DataField = 'STATE_TAX_ID'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePercentage: TevDBEdit
        Left = 56
        Top = 288
        Width = 73
        Height = 21
        DataField = 'SALES_TAX_PERCENTAGE'
        DataSource = wwdsDetail
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
end
