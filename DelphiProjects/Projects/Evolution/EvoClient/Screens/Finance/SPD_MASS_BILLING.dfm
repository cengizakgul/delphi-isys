inherited MASS_BILLING: TMASS_BILLING
  object pcMain: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 443
    Height = 277
    ActivePage = tsCreate
    Align = alClient
    TabOrder = 0
    OnChange = pcMainChange
    object tsCompany: TTabSheet
      Caption = 'Companies'
      object Splitter: TevSplitter
        Left = 320
        Top = 0
        Height = 524
      end
      object grdSelectCompanies: TevDBGrid
        Left = 0
        Top = 0
        Width = 320
        Height = 524
        DisableThemesInTitle = False
        Selected.Strings = (
          'CUSTOM_COMPANY_NUMBER'#9'12'#9'Number'#9'F'
          'NAME'#9'25'#9'Name'#9'F'
          'FEIN'#9'9'#9'Fein'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TMASS_BILLING\grdSelectCompanies'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsList
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = grdSelectCompaniesDblClick
        OnKeyDown = grdSelectCompaniesKeyDown
        PaintOptions.AlternatingRowColor = clCream
      end
      object pnlInvoice: TevPanel
        Left = 323
        Top = 0
        Width = 428
        Height = 524
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object grdInvoices: TevDBGrid
          Left = 0
          Top = 46
          Width = 428
          Height = 432
          DisableThemesInTitle = False
          Selected.Strings = (
            'INVOICE_NUMBER'#9'6'#9'Invoice Number'#9'F'
            'INVOICE_DATE'#9'15'#9'Invoice Date'#9'F'
            'CUSTOM_COMPANY_NUMBER'#9'13'#9'Company Number'#9'F'
            'COMPANY_NAME'#9'25'#9'Company Name'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
          IniAttributes.SectionName = 'TMASS_BILLING\grdInvoices'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = wwdsMaster
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          Visible = False
          OnDblClick = grdInvoicesDblClick
          OnKeyDown = grdInvoicesKeyDown
          PaintOptions.AlternatingRowColor = clCream
        end
        object pnlOpenInv: TevPanel
          Left = 0
          Top = 0
          Width = 428
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object btnOpenInv: TevBitBtn
            Left = 16
            Top = 9
            Width = 113
            Height = 25
            Caption = 'Show invoices'
            TabOrder = 0
            OnClick = btnOpenInvClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD6666666606
              66DDDD888888FF8FF8DDDD677777003006DDDD8DDDFF88888FFD666666003B33
              300888888F88DD88888F6FFF703BBB3333308DDDD8DDDD8888886FF670BBBB33
              33308DD8D8DDDD8888886FFF70BBB3B333308DDDD8DDD8D888886FF670B33BBB
              B3308DD8D8D88DDDD8886FFF703BBBBBBB308DDDD88DDDDDDD886FF667003BBB
              30088DD88D88DDDDD88D6FFFFF77003006DD8DDDDDDD88D88DDD6FF666667706
              76DD8DD88888DD8DD8DD6FFFFFFFFF7676DD8DDDDDDDDDD8D8DD6FF666666FF6
              76DD8DD888888DD8D8DD6FFFFFFFFFF666DD8DDDDDDDDDD888DD6FFFFFFFFFF6
              DDDD8DDDDDDDDDD8DDDD666666666666DDDD888888888888DDDD}
            NumGlyphs = 2
          end
        end
        object pnlPrint: TevPanel
          Left = 0
          Top = 478
          Width = 428
          Height = 46
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object btnPrint: TevBitBtn
            Left = 16
            Top = 9
            Width = 113
            Height = 25
            Caption = 'Pr&int selected'
            TabOrder = 0
            OnClick = btnPrintClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
              08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
              8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
              8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
              8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
              78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
              7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
              DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
              DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
            NumGlyphs = 2
          end
          object cbPreview: TevCheckBox
            Left = 139
            Top = 13
            Width = 97
            Height = 17
            Caption = 'Preview'
            TabOrder = 1
          end
        end
      end
    end
    object tsDetails: TTabSheet
      Caption = 'Details'
      ImageIndex = 1
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 621
        Height = 499
        DisableThemesInTitle = False
        Selected.Strings = (
          'SERVICE_NAME'#9'40'#9'Service Name'#9'F'
          'QUANTITY'#9'10'#9'Quantity'#9'F'
          'DISCOUNT'#9'10'#9'Discount'#9'F'
          'TAX'#9'10'#9'Tax'#9'F'
          'AMOUNT'#9'10'#9'Amount'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TMASS_BILLING\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
    end
    object tsCreate: TTabSheet
      Caption = 'Create'
      ImageIndex = 2
      object lvSelected: TevListView
        Left = 0
        Top = 0
        Width = 286
        Height = 249
        Align = alLeft
        Color = clBtnFace
        Columns = <
          item
            Caption = 'Number'
            Width = 80
          end
          item
            Caption = 'Name'
            Width = 200
          end>
        ReadOnly = True
        SortType = stText
        TabOrder = 0
        TabStop = False
        ViewStyle = vsReport
      end
      object pnlServices: TevPanel
        Left = 286
        Top = 0
        Width = 149
        Height = 249
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object grdCreate: TevDBGrid
          Left = 0
          Top = 0
          Width = 149
          Height = 169
          DisableThemesInTitle = False
          Selected.Strings = (
            'SERVICE_NAME'#9'40'#9'Name'
            'SERVICE_TYPE'#9'1'#9'Type')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
          IniAttributes.SectionName = 'TMASS_BILLING\grdCreate'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = dsServices
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object pnlCreate: TevPanel
          Left = 0
          Top = 169
          Width = 149
          Height = 80
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          OnResize = pnlCreateResize
          object evLabel1: TevLabel
            Left = 16
            Top = 4
            Width = 93
            Height = 13
            Caption = 'ACH Effective Date'
          end
          object evLabel2: TevLabel
            Left = 138
            Top = 4
            Width = 53
            Height = 13
            Caption = 'ACH Notes'
          end
          object btnCreate: TevBitBtn
            Left = 15
            Top = 45
            Width = 115
            Height = 24
            Caption = 'Create &invoices'
            TabOrder = 2
            OnClick = btnCreateClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD8888888868
              88DDDD8888888FFDD8DDDD8FFF77866668DDDD8DDDDFF88FFDDDDD8F88866E66
              666DDD8D8DF88D888FFDDD8F766EEE666668DD8DDF8DDD88888FDD8F86EEEE66
              6668DD8DD8DDDD88888FDD8F76EEE6E66668DD8DD8DDD8D8888FDD8F86E66EEE
              E668DD8DD8D88DDDD88FDD87766EEEEEEE68DD8DD88DDDDDDD8DDD8FC7866EEE
              668DDDD8FDD88DDD88DDDD8FC777866688DDDDD8FDDDD888DDDDFC7FC7FC7768
              78DD8FD8FD8FDDDDD8DDDFC77FC7FF77F8DDD8FDD8FDDDDDD8DDDD77777FFFFF
              F8DDDDDDDDDDDDDDD8DDDFCFCFC7888888DDD8F8F8FD888888DDFCDFCDFCDDDD
              DDDD8FD8FD8FDDDDDDDDDDDFCDDDDDDDDDDDDDD8FDDDDDDDDDDD}
            NumGlyphs = 2
          end
          object evDateTimePicker1: TevDateTimePicker
            Left = 15
            Top = 20
            Width = 115
            Height = 21
            Date = 38593.000000000000000000
            Time = 38593.000000000000000000
            TabOrder = 0
          end
          object evMemo1: TevMemo
            Left = 138
            Top = 20
            Width = 259
            Height = 49
            Lines.Strings = (
              'Global Billing')
            TabOrder = 1
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = cdsInvoices
    OnDataChange = wwdsMasterDataChange
  end
  inherited wwdsDetail: TevDataSource
    DataSet = cdsInvoiceDetails
    MasterDataSource = nil
  end
  inherited wwdsList: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    OnDataChange = wwdsListDataChange
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 249
    Top = 21
  end
  object cdsInvoices: TevClientDataSet
    Left = 142
    Top = 66
    object cdsInvoicesCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsInvoicesCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdsInvoicesCO_BILLING_HISTORY_NBR: TIntegerField
      FieldName = 'CO_BILLING_HISTORY_NBR'
    end
    object cdsInvoicesINVOICE_NUMBER: TIntegerField
      FieldName = 'INVOICE_NUMBER'
    end
    object cdsInvoicesINVOICE_DATE: TDateTimeField
      FieldName = 'INVOICE_DATE'
    end
    object cdsInvoicesCUSTOM_COMPANY_NUMBER: TStringField
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdsInvoicesCOMPANY_NAME: TStringField
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
  end
  object cdsInvoiceDetails: TevClientDataSet
    OnCalcFields = cdsInvoiceDetailsCalcFields
    Left = 196
    Top = 66
    object cdsInvoiceDetailsCO_BILLING_HISTORY_DETAIL_NBR: TIntegerField
      FieldName = 'CO_BILLING_HISTORY_DETAIL_NBR'
    end
    object cdsInvoiceDetailsCO_BILLING_HISTORY_NBR: TIntegerField
      FieldName = 'CO_BILLING_HISTORY_NBR'
    end
    object cdsInvoiceDetailsCO_SERVICES_NBR: TIntegerField
      FieldName = 'CO_SERVICES_NBR'
    end
    object cdsInvoiceDetailsQUANTITY: TFloatField
      FieldName = 'QUANTITY'
      DisplayFormat = '#,##0.00'
    end
    object cdsInvoiceDetailsAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object cdsInvoiceDetailsTAX: TFloatField
      FieldName = 'TAX'
      DisplayFormat = '#,##0.00'
    end
    object cdsInvoiceDetailsDISCOUNT: TFloatField
      FieldName = 'DISCOUNT'
      DisplayFormat = '#,##0.00'
    end
    object cdsInvoiceDetailsCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdsInvoiceDetailsNAME: TStringField
      FieldName = 'NAME'
      LookupDataSet = DM_CO_SERVICES.CO_SERVICES
      LookupKeyFields = 'CO_SERVICES_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CO_SERVICES_NBR'
      Size = 40
    end
    object cdsInvoiceDetailsSERVICE_NAME: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'SERVICE_NAME'
      Size = 40
    end
    object cdsInvoiceDetailsSB_SERVICES_NBR: TIntegerField
      FieldName = 'SB_SERVICES_NBR'
    end
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 291
    Top = 21
  end
  object dsServices: TevDataSource
    DataSet = DM_SB_SERVICES.SB_SERVICES
    Left = 244
    Top = 69
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 165
    Top = 29
  end
end
