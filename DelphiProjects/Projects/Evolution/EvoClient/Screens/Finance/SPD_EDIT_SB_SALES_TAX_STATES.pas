// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_SALES_TAX_STATES;

interface

uses SFrameEntry, Db,   Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Wwdatsrc, SDataStructure, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_SALES_TAX_STATES = class(TFrameEntry)
    PageControl1: TevPageControl;
    tshtBrowse: TTabSheet;
    dtxtSTATE: TevDBText;
    lablSalesTaxState: TevLabel;
    wwDBGrid1: TevDBGrid;
    lablState: TevLabel;
    lablTax_ID: TevLabel;
    lablPercentage: TevLabel;
    dedtState: TevDBEdit;
    dedtTax_ID: TevDBEdit;
    wwdePercentage: TevDBEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
  end;

var
  EDIT_SB_SALES_TAX_STATES: TEDIT_SB_SALES_TAX_STATES;

implementation

{$R *.DFM}

function TEDIT_SB_SALES_TAX_STATES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_SALES_TAX_STATES;
end;

function TEDIT_SB_SALES_TAX_STATES.GetInsertControl: TWinControl;
begin
  Result := dedtSTATE;
end;

initialization
  RegisterClass(TEDIT_SB_SALES_TAX_STATES);

end.
