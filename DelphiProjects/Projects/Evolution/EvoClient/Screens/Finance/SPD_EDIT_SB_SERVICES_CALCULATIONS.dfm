inherited EDIT_SB_SERVICES_CALCULATIONS: TEDIT_SB_SERVICES_CALCULATIONS
  Width = 812
  Height = 572
  object dtxtSERVICE_NAME: TevDBText [0]
    Left = 136
    Top = 8
    Width = 473
    Height = 13
    DataField = 'SERVICE_NAME'
    DataSource = wwdsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lablSalesTaxState: TevLabel [1]
    Left = 16
    Top = 8
    Width = 112
    Height = 13
    Caption = 'Service Bureau Service'
  end
  object PageControl1: TevPageControl [2]
    Left = 16
    Top = 32
    Width = 593
    Height = 425
    HelpContext = 23003
    ActivePage = tshtCalculation_Details
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object Splitter1: TevSplitter
        Left = 281
        Top = 0
        Height = 397
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 281
        Height = 397
        DisableThemesInTitle = False
        Selected.Strings = (
          'SERVICE_NAME'#9'40'#9'Service Name')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_SERVICES_CALCULATIONS\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsMaster
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object Panel1: TevPanel
        Left = 284
        Top = 0
        Width = 301
        Height = 397
        Align = alClient
        Caption = 'Panel1'
        TabOrder = 1
        object wwDBGrid2: TevDBGrid
          Left = 1
          Top = 1
          Width = 296
          Height = 395
          DisableThemesInTitle = False
          Selected.Strings = (
            'MINIMUM_QUANTITY'#9'10'#9'Min Qty'
            'MAXIMUM_QUANTITY'#9'10'#9'Max Qty'#9'F'
            'PER_ITEM_RATE'#9'10'#9'Per Item Rate'
            'FLAT_AMOUNT'#9'10'#9'Flat Amount')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
          IniAttributes.SectionName = 'TEDIT_SB_SERVICES_CALCULATIONS\wwDBGrid2'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alLeft
          DataSource = wwdsDetail
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
      end
    end
    object tshtCalculation_Details: TTabSheet
      Caption = 'Details'
      object Bevel1: TEvBevel
        Left = 0
        Top = 168
        Width = 585
        Height = 225
      end
      object lablMinimum_Quantity: TevLabel
        Left = 16
        Top = 268
        Width = 83
        Height = 13
        Caption = 'Minimum Quantity'
      end
      object lablBegin_Date1: TevLabel
        Left = 416
        Top = 216
        Width = 68
        Height = 13
        Caption = 'Effective Date'
        Visible = False
      end
      object lablMaximum_Quantity: TevLabel
        Left = 13
        Top = 300
        Width = 86
        Height = 13
        Caption = 'Maximum Quantity'
      end
      object lablPer_Item_Rate: TevLabel
        Left = 35
        Top = 332
        Width = 65
        Height = 13
        Caption = 'Per Item Rate'
      end
      object lablFlat_Amount: TevLabel
        Left = 33
        Top = 236
        Width = 73
        Height = 13
        Caption = '~Flat Amount'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablFuture_Flat_Amount: TevLabel
        Left = 112
        Top = 216
        Width = 34
        Height = 13
        Caption = 'Current'
      end
      object lablBegin_Date4: TevLabel
        Left = 264
        Top = 216
        Width = 22
        Height = 13
        Caption = 'Next'
        Visible = False
      end
      object wwDBGrid3: TevDBGrid
        Left = 0
        Top = 0
        Width = 585
        Height = 161
        DisableThemesInTitle = False
        Selected.Strings = (
          'MINIMUM_QUANTITY'#9'10'#9'Min Qty'
          'MAXIMUM_QUANTITY'#9'10'#9'Max Qty'
          'PER_ITEM_RATE'#9'10'#9'Per Item Rate'
          'FLAT_AMOUNT'#9'10'#9'Flat Amount')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_SERVICES_CALCULATIONS\wwDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object wwdeFuture_Flat_Amount_Begin_Date: TevDBDateTimePicker
        Left = 416
        Top = 232
        Width = 81
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'NEXT_FLAT_AMOUNT_BEGIN_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 9
        Visible = False
      end
      object wwdeFuture_Minimum_Begin_Date: TevDBDateTimePicker
        Left = 416
        Top = 264
        Width = 81
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'NEXT_MIN_QUANTITY_BEGIN_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 10
        Visible = False
      end
      object wwdeFuture_Maximum_Begin_Date: TevDBDateTimePicker
        Left = 416
        Top = 296
        Width = 81
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'NEXT_MAX_QUANTITY_BEGIN_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 11
        Visible = False
      end
      object wwdpFuture_Per_Item_Begin_Date: TevDBDateTimePicker
        Left = 416
        Top = 328
        Width = 81
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'NEXT_PER_ITEM_BEGIN_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 12
        Visible = False
      end
      object wwdeFlat_Amount: TevDBEdit
        Left = 112
        Top = 232
        Width = 89
        Height = 21
        DataField = 'FLAT_AMOUNT'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeMinimum_Quantity: TevDBEdit
        Left = 112
        Top = 264
        Width = 89
        Height = 21
        DataField = 'MINIMUM_QUANTITY'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeMaximum_Quantity: TevDBEdit
        Left = 112
        Top = 296
        Width = 89
        Height = 21
        DataField = 'MAXIMUM_QUANTITY'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdePer_Item_Rate: TevDBEdit
        Left = 112
        Top = 328
        Width = 89
        Height = 21
        DataField = 'PER_ITEM_RATE'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeNext_Flat_Amount: TevDBEdit
        Left = 264
        Top = 232
        Width = 89
        Height = 21
        DataField = 'NEXT_FLAT_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 5
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object wwdeNext_Minimum_Quantity: TevDBEdit
        Left = 264
        Top = 264
        Width = 89
        Height = 21
        DataField = 'NEXT_MINIMUM_QUANTITY'
        DataSource = wwdsDetail
        TabOrder = 6
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object wwdeNext_Maximum_Quantity: TevDBEdit
        Left = 264
        Top = 296
        Width = 89
        Height = 21
        DataField = 'NEXT_MAXIMUM_QUANTITY'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 7
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object wwdeNext_Per_Item_Rate: TevDBEdit
        Left = 264
        Top = 328
        Width = 89
        Height = 21
        DataField = 'NEXT_PER_ITEM_RATE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 8
        UnboundDataType = wwDefault
        Visible = False
        WantReturns = False
        WordWrap = False
      end
      object evBitBtn1: TevBitBtn
        Left = 16
        Top = 356
        Width = 185
        Height = 25
        Caption = 'Services'
        TabOrder = 13
        OnClick = evBitBtn1Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DFFFFF0FFFFF
          F2A0DDDDDD8FDDD8F88DFFFFFF0FFFFF2AAADDDDDD8FDD8F8888FFFFF0FFFFF2
          AAAD8DDDD8FDD8F8888D8FFFF0FFFF8DDADDD888D8F88FDDD8DDD88BBB3888DD
          D0DDDDD8DD8FDDDDDDDD000BB33300000A0DDDD8D888FDDDD8DDAA2BBBBB3AAA
          AAA088D8DDDD8F88888DAA2BBBB33AAAAAAA88D8DDD88F888888A2BBBBBB3AAA
          AAAD8D8DDDDD8F88888DD0BBBBBB3DDDDADDD8DDDDDD8FDDD8DDD0B0B33B3DDD
          D0DDD8D8D8FD8FDDDDDD0000BBBBB0000A0DD888DDDDD8FDD8DDA0000BBBB0AA
          AAA0D8888DDDD8F8888DA000000000AAAAAAD888888888F88888A00000000AAA
          AAADD88888888F88888DDD000000DDDDDADDDD888888FDDDD8DD}
        NumGlyphs = 2
      end
      object evBitBtn2: TevBitBtn
        Left = 201
        Top = 293
        Width = 57
        Height = 25
        Caption = 'Unlim.'
        TabOrder = 14
        OnClick = Button1Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDFFFDDDDDFFFDDDDD222DDDDD22
          2DDDDF888FDDDF888FDDD2AAA2DDD2AAA2DDF88888DDF88888FD2AAAA22D2AAA
          A228888888DF8888888D2A888222AA888A2888DDDDF888DDD88D28DDDD2AA8DD
          D8288FDDDF888DDDDF8D82DDD2AA82DDD22888FFF888D8FFF88D82222AA88222
          22288888888D8888888D8A22AA8D8A222A8DD88888DDD88888DDD8AAA8DDD8AA
          A8DDDD888DDDDD888DDDDD888DDDDD888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object evBitBtn3: TevBitBtn
        Left = 353
        Top = 293
        Width = 57
        Height = 25
        Caption = 'Unlim.'
        TabOrder = 15
        Visible = False
        OnClick = Button2Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDFFFDDDDDFFFDDDDD222DDDDD22
          2DDDDF888FDDDF888FDDD2AAA2DDD2AAA2DDF88888DDF88888FD2AAAA22D2AAA
          A228888888DF8888888D2A888222AA888A2888DDDDF888DDD88D28DDDD2AA8DD
          D8288FDDDF888DDDDF8D82DDD2AA82DDD22888FFF888D8FFF88D82222AA88222
          22288888888D8888888D8A22AA8D8A222A8DD88888DDD88888DDD8AAA8DDD8AA
          A8DDDD888DDDDD888DDDDD888DDDDD888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SB_SERVICES.SB_SERVICES
    Top = 50
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_SERVICES_CALCULATIONS.SB_SERVICES_CALCULATIONS
    Top = 58
  end
  inherited wwdsList: TevDataSource
    Top = 50
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 312
    Top = 16
  end
end
