inherited EDIT_SB_SERVICES: TEDIT_SB_SERVICES
  Width = 660
  Height = 545
  object dtxtSERVICE_NAME: TevDBText [0]
    Left = 136
    Top = 8
    Width = 473
    Height = 13
    DataField = 'SERVICE_NAME'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lablSalesTaxState: TevLabel [1]
    Left = 16
    Top = 8
    Width = 112
    Height = 13
    Caption = 'Service Bureau Service'
  end
  object pctlEDIT_SB_SERVICES: TevPageControl [2]
    Left = 16
    Top = 32
    Width = 593
    Height = 466
    HelpContext = 23524
    ActivePage = tshtDetails
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 313
        Height = 385
        DisableThemesInTitle = False
        Selected.Strings = (
          'SERVICE_NAME'#9'40'#9'Name'
          'SERVICE_TYPE'#9'1'#9'Type')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_SERVICES\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tshtDetails: TTabSheet
      Caption = 'Details'
      object Bevel1: TEvBevel
        Left = 0
        Top = 0
        Width = 585
        Height = 438
        Align = alClient
      end
      object lablService: TevLabel
        Left = 24
        Top = 16
        Width = 45
        Height = 16
        Caption = '~Service'
        FocusControl = dedtService
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablType: TevLabel
        Left = 24
        Top = 56
        Width = 33
        Height = 16
        Caption = '~Type'
        FocusControl = wwcbType
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablFrequency: TevLabel
        Left = 24
        Top = 96
        Width = 59
        Height = 16
        Caption = '~Frequency'
        FocusControl = wwcbFrequency
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablMonth_Number: TevLabel
        Left = 24
        Top = 136
        Width = 70
        Height = 13
        Caption = 'Month Number'
        FocusControl = wwseMonth_Number
      end
      object lablBased_Type: TevLabel
        Left = 24
        Top = 176
        Width = 81
        Height = 16
        Caption = '~Based on Type'
        FocusControl = wwcbBased_Type
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablMinimum_Amount: TevLabel
        Left = 24
        Top = 224
        Width = 80
        Height = 13
        Caption = 'Minimum Amount'
      end
      object lablMaximum_Amount: TevLabel
        Left = 120
        Top = 224
        Width = 83
        Height = 13
        Caption = 'Maximum Amount'
      end
      object lablReport: TevLabel
        Left = 312
        Top = 16
        Width = 32
        Height = 13
        Caption = 'Report'
        FocusControl = wwlcReport
      end
      object Label1: TevLabel
        Left = 181
        Top = 56
        Width = 65
        Height = 13
        Caption = 'Product Code'
        FocusControl = wwdeProduct_Code
      end
      object Label2: TevLabel
        Left = 105
        Top = 136
        Width = 69
        Height = 13
        Caption = 'Week Number'
        FocusControl = wwcbWeek
      end
      object evLabel1: TevLabel
        Left = 312
        Top = 248
        Width = 45
        Height = 13
        Caption = 'Tax Type'
        FocusControl = evDBComboBox1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel2: TevLabel
        Left = 24
        Top = 296
        Width = 66
        Height = 13
        Caption = 'A/R Account '
        FocusControl = edtAR_Acct
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel3: TevLabel
        Left = 24
        Top = 339
        Width = 78
        Height = 13
        Caption = 'Income Account'
        FocusControl = edtINC_ACCT
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object evLabel4: TevLabel
        Left = 24
        Top = 384
        Width = 23
        Height = 13
        Caption = 'Item '
        FocusControl = edtItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dedtService: TevDBEdit
        Left = 24
        Top = 32
        Width = 249
        Height = 21
        HelpContext = 23525
        DataField = 'SERVICE_NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object drgpCommision: TevDBRadioGroup
        Left = 312
        Top = 64
        Width = 185
        Height = 50
        HelpContext = 23533
        Caption = '~Commission'
        Columns = 2
        DataField = 'COMMISSION'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 11
        Values.Strings = (
          'Y'
          'N')
      end
      object wwcbType: TevDBComboBox
        Left = 24
        Top = 72
        Width = 145
        Height = 21
        HelpContext = 23526
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'SERVICE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object wwcbFrequency: TevDBComboBox
        Left = 24
        Top = 112
        Width = 145
        Height = 21
        HelpContext = 23527
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'FREQUENCY'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object wwseMonth_Number: TevDBSpinEdit
        Left = 24
        Top = 152
        Width = 41
        Height = 21
        HelpContext = 23528
        Increment = 1.000000000000000000
        MaxValue = 12.000000000000000000
        MinValue = 1.000000000000000000
        Value = 1.000000000000000000
        DataField = 'MONTH_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
      end
      object wwcbBased_Type: TevDBComboBox
        Left = 24
        Top = 192
        Width = 247
        Height = 21
        HelpContext = 23529
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'BASED_ON_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object wwlcReport: TevDBLookupCombo
        Left = 312
        Top = 32
        Width = 185
        Height = 21
        HelpContext = 23532
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NDescription'#9'60'#9'NDescription'#9'F')
        DataField = 'SB_REPORTS_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SB_REPORTS.SB_REPORTS
        LookupField = 'SB_REPORTS_NBR'
        Style = csDropDownList
        TabOrder = 10
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object drgpSales_Taxable: TevDBRadioGroup
        Left = 312
        Top = 128
        Width = 185
        Height = 50
        HelpContext = 23534
        Caption = '~Sales Taxable'
        Columns = 2
        DataField = 'SALES_TAXABLE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 13
        Values.Strings = (
          'Y'
          'N')
      end
      object wwdeMinimum_Amount: TevDBEdit
        Left = 24
        Top = 240
        Width = 81
        Height = 21
        HelpContext = 23530
        DataField = 'MINIMUM_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeMaximum_Amount: TevDBEdit
        Left = 120
        Top = 240
        Width = 81
        Height = 21
        HelpContext = 23531
        DataField = 'MAXIMUM_AMOUNT'
        DataSource = wwdsDetail
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeProduct_Code: TevDBEdit
        Left = 180
        Top = 72
        Width = 94
        Height = 21
        HelpContext = 23530
        DataField = 'PRODUCT_CODE'
        DataSource = wwdsDetail
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object CheckBox1: TevCheckBox
        Left = 24
        Top = 275
        Width = 97
        Height = 16
        Caption = 'Tiered billing'
        TabOrder = 9
        OnClick = CheckBox1Click
      end
      object wwcbWeek: TevDBComboBox
        Left = 105
        Top = 152
        Width = 145
        Height = 21
        HelpContext = 23527
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'WEEK_NUMBER'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 5
        UnboundDataType = wwDefault
      end
      object evBitBtn1: TevBitBtn
        Left = 312
        Top = 324
        Width = 185
        Height = 25
        Caption = 'Services Calculations'
        TabOrder = 12
        OnClick = evBitBtn1Click
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DFFF0FFFF8DD
          DDDDD888D8888FDDDDDDFFFF0FFF8DDDDDDD8888D888FDDDDDDDFFF0FFF8DDDD
          DDDD888D888FDDDDDDDD88B338888888888DD8D88FD88888888D88BB33777777
          7778D8DD88FDDDDDDDD888BB338888788878D8DD88F888D888D88BBBB3888878
          88788DDDD8F888D888D803B3B3877777777888D8D88FDDDDDDD800BBBB088871
          117888DDDD8F88D888D8000BB30888711178888DDD8F88D888D8000000877777
          7778888888FDDDDDDDD8800008BBBBBB3B78D8888F888888D8D8888887BBBBBB
          BB788DDDDD88888888D887777733333333788DDDDDDDDDDDDDD8877777777777
          77788DDDDDDDDDDDDDD8D88888888888888DD88888888888888D}
        NumGlyphs = 2
        Margin = 0
      end
      object evDBComboBox1: TevDBComboBox
        Left = 312
        Top = 264
        Width = 185
        Height = 21
        HelpContext = 23529
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'TAX_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 15
        UnboundDataType = wwDefault
      end
      object rgPartnerBilling: TevDBRadioGroup
        Left = 312
        Top = 189
        Width = 185
        Height = 50
        HelpContext = 23534
        Caption = '~Partner Billing'
        Columns = 2
        DataField = 'PARTNER_BILLING'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 14
      end
      object edtAR_Acct: TevDBEdit
        Left = 24
        Top = 312
        Width = 249
        Height = 21
        HelpContext = 23525
        DataField = 'AR_ACCT'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        TabOrder = 16
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object edtINC_ACCT: TevDBEdit
        Left = 24
        Top = 355
        Width = 249
        Height = 21
        HelpContext = 23525
        DataField = 'INC_ACCT'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        TabOrder = 17
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object edtItem: TevDBEdit
        Left = 24
        Top = 400
        Width = 249
        Height = 21
        HelpContext = 23525
        DataField = 'ITEM'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        TabOrder = 18
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 2
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_SERVICES.SB_SERVICES
    OnDataChange = wwdsDataChange
    Left = 206
    Top = 10
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 312
    Top = 8
  end
end
