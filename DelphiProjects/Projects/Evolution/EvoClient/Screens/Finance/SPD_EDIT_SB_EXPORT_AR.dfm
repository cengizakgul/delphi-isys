inherited EDIT_SB_EXPORT_AR: TEDIT_SB_EXPORT_AR
  Width = 871
  Height = 622
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 871
    Height = 622
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Browse'
      object Panel1: TevPanel
        Left = 0
        Top = 0
        Width = 863
        Height = 33
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
        TabOrder = 0
        object Button2: TevBitBtn
          Left = 168
          Top = 4
          Width = 177
          Height = 25
          Caption = 'Export Selected Companies'
          TabOrder = 0
          OnClick = Button1Click
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDD6666666666666DDD8888888888888DDD6FFF6FFF6FFF
            6DDD8DDD8DDD8DDD8DDD6FFF6FFF6FFF6DDD8DDD8DDD8DDD8DDD6FFF6FFF6FFF
            6DDD8DDD8DDD8DDD8DDD6444644464446DDD8888888888888DDD644464446444
            6DDD8888888888888DDD6444644464446DDD8888888888888DDD677767776777
            6DDD8DDD8DDD8DDD8DDD6F7767776777688D8DFFFFFFFFFFFFFD6F7000000000
            00008DF88888888888886660AAA2AAA2AAA088F8DDD8DDD8DDD86660AAA2AAA2
            AAA088F8DDD8DDD8DDD8DD80AAA2AAA2AAA0DDF8DDD8DDD8DDD8DD8000000000
            0000DDF8888888888888DDD0000000000000DDD8888888888888}
          NumGlyphs = 2
          Margin = 0
        end
        object Button1: TevBitBtn
          Left = 8
          Top = 4
          Width = 153
          Height = 25
          Caption = 'Export All Companies'
          TabOrder = 1
          OnClick = Button2Click
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDD6666666666666DDD8888888888888DDD6FFF6FFF6FFF
            6DDD8DDD8DDD8DDD8DDD6F7767776777688D8DFFFFFFFFFFFFFD6F7000000000
            00008DF88888888888886F70AAA2AAA2AAA08DF8DDD8DDD8DDD86F70AAA2AAA2
            AAA08DF8DDD8DDD8DDD86F70AAA2AAA2AAA08DF8DDD8DDD8DDD86F70AAA2AAA2
            AAA08DF8DDD8DDD8DDD86F70AAA2AAA2AAA08DF8DDD8DDD8DDD86F70AAA2AAA2
            AAA08DF8DDD8DDD8DDD86660AAA2AAA2AAA088F8DDD8DDD8DDD86660AAA2AAA2
            AAA088F8DDD8DDD8DDD8DD80AAA2AAA2AAA0DDF8DDD8DDD8DDD8DD8000000000
            0000DDF8888888888888DDD0000000000000DDD8888888888888}
          NumGlyphs = 2
          Margin = 0
        end
      end
      object wwgdCompanies: TevDBGrid
        Left = 0
        Top = 33
        Width = 329
        Height = 561
        DisableThemesInTitle = False
        Selected.Strings = (
          'CUSTOM_COMPANY_NUMBER'#9'20'#9'Number'#9'F'
          'NAME'#9'40'#9'Name'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_EXPORT_AR\wwgdCompanies'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = dsData
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Export'
      ImageIndex = 1
      OnShow = TabSheet2Show
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 863
        Height = 121
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object Label1: TevLabel
          Left = 16
          Top = 48
          Width = 48
          Height = 13
          Caption = 'Start Date'
        end
        object Label2: TevLabel
          Left = 160
          Top = 48
          Width = 45
          Height = 13
          Caption = 'End Date'
        end
        object Label4: TevLabel
          Left = 3
          Top = 105
          Width = 63
          Height = 13
          Caption = 'Company List'
        end
        object Label3: TevLabel
          Left = 199
          Top = 105
          Width = 38
          Height = 13
          Caption = 'Preview'
        end
        object evLabel1: TevLabel
          Left = 16
          Top = 8
          Width = 57
          Height = 13
          Caption = 'Export Type'
        end
        object evLabel2: TevLabel
          Left = 160
          Top = 8
          Width = 47
          Height = 13
          Caption = 'G/L Code'
        end
        object wwDBDateTimePicker1: TevDBDateTimePicker
          Left = 16
          Top = 64
          Width = 121
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          Epoch = 1950
          ShowButton = True
          TabOrder = 0
        end
        object wwDBDateTimePicker2: TevDBDateTimePicker
          Left = 160
          Top = 64
          Width = 121
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          Epoch = 1950
          ShowButton = True
          TabOrder = 1
        end
        object Button3: TevBitBtn
          Left = 304
          Top = 19
          Width = 97
          Height = 25
          Caption = 'Start Export'
          TabOrder = 2
          OnClick = Button3Click
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD28D
            DDDDDDDDDDDDD8FDDDDD888888888228DDDDDDDDDDDDD88FDDDD4444444442A2
            8DDD888888FFF888FDDD4FFFFF2222AA28DD8DDDDD8888888FDD4FFFFF2AAAAA
            A28D8DDDDD88888888FD4444442AAAAAAA2888888D888888888F4FFFFF2AAAAA
            AAA28DDDDD88888888884FFFFF2AAAAAAA2D8DDDDD888888888D4444442AAAAA
            A2DD88888D88888888DD4FFFFF2222AA2DDD8DDDDD8888888DDD4FFFFFFFF2A2
            DDDD8DDDDDDDD888DDDD444444444224DDDD88888888D88DDDDD4FFFFFFFF2F4
            DDDD8DDDDDDDD8D8DDDD4FFFFFFFFFF4DDDD8DDDDDDDDDD8DDDD444444444444
            DDDD888888888888DDDD444444444444DDDD888888888888DDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object Button4: TevBitBtn
          Left = 304
          Top = 61
          Width = 99
          Height = 25
          Caption = 'Finish Export'
          TabOrder = 3
          OnClick = Button4Click
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD88DDDD
            D88DDDDDDDDDDDDDDDDD888888118888811DDDDDDDFFDDDDDFFD444444991444
            199D88888D88F888F88D4FFFF7999171999D8DDDDD888FDF888D4FFFFF799919
            99DD8DDDDDD888F888DD4444444499999DDD8888888D88888DDD4FFFFFF71999
            18DD8DDDDDDDF888FDDD4FFFFF719999918D8DDDDDDF88888FDD444444199949
            991D888888F888D888FD4FFFF7999FF4999D8DDDDD888DDD888D4FFFF7997FF4
            D99D8DDDDD88DDD8D88D444444444444DDDD888888DD8888DDDD4FFFFFFFFFF4
            DDDD8DDDDDDDDDD8DDDD4FFFFFFFFFF4DDDD8DDDDDDDDDD8DDDD444444444444
            DDDD888888888888DDDD444444444444DDDD888888888888DDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object evcbType: TevComboBox
          Left = 16
          Top = 24
          Width = 121
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 4
          OnChange = evcbTypeChange
          Items.Strings = (
            'Business Works'
            'MAS 90')
        end
        object evEdit1: TevEdit
          Left = 160
          Top = 24
          Width = 121
          Height = 21
          MaxLength = 9
          TabOrder = 5
          OnExit = evEdit1Exit
        end
        object cbSales: TevCheckBox
          Left = 160
          Top = 27
          Width = 113
          Height = 17
          Caption = 'Include Sales Tax'
          TabOrder = 6
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 121
        Width = 863
        Height = 473
        Align = alClient
        Caption = 'Panel3'
        TabOrder = 1
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 195
          Height = 471
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'Panel4'
          TabOrder = 0
          object ScrollBox2: TScrollBox
            Left = 0
            Top = 0
            Width = 195
            Height = 471
            Align = alClient
            TabOrder = 0
            object ListBox2: TListBox
              Left = -1
              Top = -1
              Width = 300
              Height = 4000
              ItemHeight = 13
              TabOrder = 0
            end
          end
        end
        object wwDBGrid1: TevDBGrid
          Left = 196
          Top = 1
          Width = 666
          Height = 471
          DisableThemesInTitle = False
          Selected.Strings = (
            'CL_NBR'#9'10'#9'Cl Nbr'#9
            'CUSTOM_CLIENT_NUMBER'#9'20'#9'Custom Client Number'#9
            'CL_NAME'#9'40'#9'Cl Name'#9
            'CO_NBR'#9'10'#9'Co Nbr'#9
            'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom Company Number'#9
            'CO_NAME'#9'40'#9'Co Name'#9
            'INVOICE_NUMBER'#9'10'#9'Invoice Number'#9
            'INVOICE_DATE'#9'10'#9'Invoice Date'#9
            'INVOICE_AMOUNT'#9'10'#9'Invoice Amount'#9
            'INVOICE_DISCOUNT'#9'10'#9'Invoice Discount'#9
            'INVOICE_TAX'#9'10'#9'Invoice Tax'#9
            'CHECK_DATE'#9'10'#9'Check Date'#9
            'ACT_PRINTED'#9'1'#9'Act Printed'#9'F'
            'ADJUSTMENT'#9'1'#9'Adjustment'#9'F'
            'AMOUNT'#9'10'#9'Amount'#9'F'
            'BATCH_FAX'#9'1'#9'Batch Fax'#9'F'
            'BATCH_NUMBER'#9'5'#9'Batch Number'#9'F'
            'CM_INVOICE_REF'#9'7'#9'Cm Invoice Ref'#9'F'
            'COMMENT_DESCRIP'#9'50'#9'Comment Descrip'#9'F'
            'COMMISSION_AMOUNT'#9'10'#9'Commission Amount'#9'F'
            'COMMISSION_RATE'#9'10'#9'Commission Rate'#9'F'
            'COMMISSIONABLE'#9'1'#9'Commissionable'#9'F'
            'COST'#9'10'#9'Cost'#9'F'
            'COST_OF_SALES'#9'10'#9'Cost Of Sales'#9'F'
            'COST_SALES_ACCNT'#9'9'#9'Cost Sales Accnt'#9'F'
            'COST_SUBJ_COMM'#9'10'#9'Cost Subj Comm'#9'F'
            'CUSTOMER_PO_NO'#9'15'#9'Customer Po No'#9'F'
            'DESCRIPTION'#9'30'#9'Description'#9'F'
            'DISCOUNT_DUE_DATE'#9'6'#9'Discount Due Date'#9'F'
            'DIVISION_NUMBER'#9'2'#9'Division Number'#9'F'
            'DT'#9'2'#9'Dt'#9'F'
            'FAX_NUMBER'#9'17'#9'Fax Number'#9'F'
            'FREIGHT'#9'10'#9'Freight'#9'F'
            'INVALID_TX_CALC'#9'1'#9'Invalid Tx Calc'#9'F'
            'INVENTORY_ACCNT'#9'9'#9'Inventory Accnt'#9'F'
            'INVOICE_COMMENT'#9'30'#9'Invoice Comment'#9'F'
            'INVOICE_TYPE'#9'2'#9'Invoice Type'#9'F'
            'JOB_NUMBER'#9'37'#9'Job Number'#9'F'
            'MISC_GL_ACCT'#9'9'#9'Misc Gl Acct'#9'F'
            'OPEN_STRING'#9'16'#9'Open String'#9'F'
            'OPEN_STRING_2'#9'9'#9'Open String 2'#9'F'
            'OPEN_STRING_3'#9'14'#9'Open String 3'#9'F'
            'PRICE'#9'10'#9'Price'#9'F'
            'PRINT_INVOICE'#9'1'#9'Print Invoice'#9'F'
            'QUANTITY'#9'10'#9'Quantity'#9'F'
            'REP_INV_REF'#9'4'#9'Rep Inv Ref'#9'F'
            'RETENTION_AMOUNT'#9'10'#9'Retention Amount'#9'F'
            'SALES_ACCNT'#9'9'#9'Sales Accnt'#9'F'
            'SALES_CODE'#9'6'#9'Sales Code'#9'F'
            'SALES_PERSON_NO'#9'30'#9'Sales Person No'#9'F'
            'SALES_SUBJ_COMM'#9'10'#9'Sales Subj Comm'#9'F'
            'SALES_TAX'#9'10'#9'Sales Tax'#9'F'
            'SHIPPING_METHOD'#9'15'#9'Shipping Method'#9'F'
            'SUBJ_TO_EXEMPT'#9'1'#9'Subj To Exempt'#9'F'
            'TAX_CLASS'#9'2'#9'Tax Class'#9'F'
            'TAX_SCHEDULE'#9'9'#9'Tax Schedule'#9'F'
            'TAXABLE_AMOUNT'#9'10'#9'Taxable Amount'#9'F'
            'TERMS_CODE'#9'2'#9'Terms Code'#9'F'
            'UNIT_MEASURE'#9'4'#9'Unit Measure'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_SB_EXPORT_AR\wwDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = wwDataSource2
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 720
    Top = 82
  end
  inherited wwdsDetail: TevDataSource
    Left = 720
  end
  inherited wwdsList: TevDataSource
    Left = 674
    Top = 58
  end
  object wwDataSource2: TevDataSource
    DataSet = wwClientDataSet1
    Left = 564
    Top = 200
  end
  object OpenDialog1: TOpenDialog
    Left = 748
    Top = 16
  end
  object wwRecordViewDialog1: TwwRecordViewDialog
    DataSource = wwDataSource3
    FormPosition.Left = 0
    FormPosition.Top = 0
    FormPosition.Width = 0
    FormPosition.Height = 0
    NavigatorButtons = [nbsFirst, nbsPrior, nbsNext, nbsLast, nbsInsert, nbsDelete, nbsEdit, nbsPost, nbsCancel, nbsRefresh, nbsPriorPage, nbsNextPage, nbsSaveBookmark, nbsRestoreBookmark]
    ControlOptions = []
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'MS Sans Serif'
    LabelFont.Style = []
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Caption = 'Record View'
    NavigatorFlat = True
    Left = 252
    Top = 376
  end
  object wwDataSource3: TevDataSource
    Left = 364
    Top = 376
  end
  object SaveDialog1: TSaveDialog
    Left = 676
    Top = 24
  end
  object dsData: TevDataSource
    DataSet = DM_TMP_CO.TMP_CO
    Left = 756
    Top = 80
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 756
    Top = 112
  end
  object evCDTmp: TevClientDataSet
    Left = 716
    Top = 48
  end
  object wwClientDataSet1: TevClientDataSet
    Left = 564
    Top = 64
    object wwClientDataSet1CL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object wwClientDataSet1CUSTOM_CLIENT_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object wwClientDataSet1CL_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'CL_NAME'
      Size = 40
    end
    object wwClientDataSet1CO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
    object wwClientDataSet1CUSTOM_COMPANY_NUMBER: TStringField
      DisplayWidth = 20
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object wwClientDataSet1CO_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'CO_NAME'
      Size = 40
    end
    object wwClientDataSet1INVOICE_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'INVOICE_NUMBER'
    end
    object wwClientDataSet1INVOICE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'INVOICE_DATE'
    end
    object wwClientDataSet1INVOICE_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'INVOICE_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1INVOICE_DISCOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'INVOICE_DISCOUNT'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1INVOICE_TAX: TFloatField
      DisplayWidth = 10
      FieldName = 'INVOICE_TAX'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1CHECK_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'CHECK_DATE'
    end
    object wwClientDataSet1INVOICE_TYPE: TStringField
      FieldName = 'INVOICE_TYPE'
      Size = 2
    end
    object wwClientDataSet1DIVISION_NUMBER: TStringField
      FieldName = 'DIVISION_NUMBER'
      Size = 2
    end
    object wwClientDataSet1TERMS_CODE: TStringField
      FieldName = 'TERMS_CODE'
      Size = 2
    end
    object wwClientDataSet1TAX_SCHEDULE: TStringField
      FieldName = 'TAX_SCHEDULE'
      Size = 9
    end
    object wwClientDataSet1SALES_PERSON_NO: TStringField
      FieldName = 'SALES_PERSON_NO'
      Size = 4
    end
    object wwClientDataSet1SALES_PERSON_NAME: TStringField
      FieldName = 'SALES_PERSON_NAME'
      Size = 30
    end
    object wwClientDataSet1PRINT_INVOICE: TStringField
      FieldName = 'PRINT_INVOICE'
      Size = 1
    end
    object wwClientDataSet1SHIPPING_METHOD: TStringField
      FieldName = 'SHIPPING_METHOD'
      Size = 15
    end
    object wwClientDataSet1CUSTOMER_PO_NO: TStringField
      FieldName = 'CUSTOMER_PO_NO'
      Size = 15
    end
    object wwClientDataSet1CM_INVOICE_REF: TStringField
      FieldName = 'CM_INVOICE_REF'
      Size = 7
    end
    object wwClientDataSet1INVOICE_COMMENT: TStringField
      FieldName = 'INVOICE_COMMENT'
      Size = 30
    end
    object wwClientDataSet1ADJUSTMENT: TStringField
      FieldName = 'ADJUSTMENT'
      Size = 1
    end
    object wwClientDataSet1REP_INV_REF: TStringField
      FieldName = 'REP_INV_REF'
      Size = 4
    end
    object wwClientDataSet1ACT_PRINTED: TStringField
      FieldName = 'ACT_PRINTED'
      Size = 1
    end
    object wwClientDataSet1JOB_NUMBER: TStringField
      FieldName = 'JOB_NUMBER'
      Size = 37
    end
    object wwClientDataSet1DISCOUNT_DUE_DATE: TStringField
      FieldName = 'DISCOUNT_DUE_DATE'
      Size = 6
    end
    object wwClientDataSet1BATCH_NUMBER: TStringField
      FieldName = 'BATCH_NUMBER'
      Size = 5
    end
    object wwClientDataSet1INVALID_TX_CALC: TStringField
      FieldName = 'INVALID_TX_CALC'
      Size = 1
    end
    object wwClientDataSet1BATCH_FAX: TStringField
      FieldName = 'BATCH_FAX'
      Size = 1
    end
    object wwClientDataSet1FAX_NUMBER: TStringField
      FieldName = 'FAX_NUMBER'
      Size = 17
    end
    object wwClientDataSet1OPEN_STRING: TStringField
      FieldName = 'OPEN_STRING'
      Size = 16
    end
    object wwClientDataSet1COMMISSION_RATE: TFloatField
      FieldName = 'COMMISSION_RATE'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1TAXABLE_AMOUNT: TFloatField
      FieldName = 'TAXABLE_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1FREIGHT: TFloatField
      FieldName = 'FREIGHT'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1SALES_TAX: TFloatField
      FieldName = 'SALES_TAX'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1COST_OF_SALES: TFloatField
      FieldName = 'COST_OF_SALES'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1SALES_SUBJ_COMM: TFloatField
      FieldName = 'SALES_SUBJ_COMM'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1COST_SUBJ_COMM: TFloatField
      FieldName = 'COST_SUBJ_COMM'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1COMMISSION_AMOUNT: TFloatField
      FieldName = 'COMMISSION_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1RETENTION_AMOUNT: TFloatField
      FieldName = 'RETENTION_AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1SALES_CODE: TStringField
      FieldName = 'SALES_CODE'
      Size = 10
    end
    object wwClientDataSet1DESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object wwClientDataSet1SALES_ACCNT: TStringField
      FieldName = 'SALES_ACCNT'
      Size = 9
    end
    object wwClientDataSet1COST_SALES_ACCNT: TStringField
      FieldName = 'COST_SALES_ACCNT'
      Size = 9
    end
    object wwClientDataSet1INVENTORY_ACCNT: TStringField
      FieldName = 'INVENTORY_ACCNT'
      Size = 9
    end
    object wwClientDataSet1UNIT_MEASURE: TStringField
      FieldName = 'UNIT_MEASURE'
      Size = 4
    end
    object wwClientDataSet1SUBJ_TO_EXEMPT: TStringField
      FieldName = 'SUBJ_TO_EXEMPT'
      Size = 1
    end
    object wwClientDataSet1COMMISSIONABLE: TStringField
      FieldName = 'COMMISSIONABLE'
      Size = 1
    end
    object wwClientDataSet1TAX_CLASS: TStringField
      FieldName = 'TAX_CLASS'
      Size = 2
    end
    object wwClientDataSet1OPEN_STRING_2: TStringField
      FieldName = 'OPEN_STRING_2'
      Size = 9
    end
    object wwClientDataSet1QUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
    object wwClientDataSet1PRICE: TFloatField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1COST: TFloatField
      FieldName = 'COST'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1AMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object wwClientDataSet1MISC_GL_ACCT: TStringField
      FieldName = 'MISC_GL_ACCT'
      Size = 9
    end
    object wwClientDataSet1COMMENT_DESCRIP: TStringField
      FieldName = 'COMMENT_DESCRIP'
      Size = 50
    end
    object wwClientDataSet1OPEN_STRING_3: TStringField
      FieldName = 'OPEN_STRING_3'
      Size = 14
    end
    object wwClientDataSet1DT: TStringField
      FieldName = 'DT'
      Size = 2
    end
  end
end
