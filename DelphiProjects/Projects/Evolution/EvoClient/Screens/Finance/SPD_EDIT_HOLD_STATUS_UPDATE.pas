// Copyright � 2000-2009 iSystems LLC. All rights reserved.
unit SPD_EDIT_HOLD_STATUS_UPDATE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFieldCodeValues, Mask, wwdbedit, Wwdotdot, Wwdbcomb, 
  wwdbdatetimepicker, StdCtrls, ComCtrls, EvUtils, SDataStructure,
  ISBasicClasses, Spin, ExtCtrls, evConsts, EvUIComponents,
  isUIwwDBComboBox;

type
  TEDIT_HOLD_STATUS_UPDATE = class(TForm)
    OpenDialog1: TOpenDialog;
    Button12: TButton;
    Button13: TButton;
    evcbLiabStatus: TevDBComboBox;
    evLabel8: TevLabel;
    evcbCreditHold: TevDBComboBox;
    evLabel6: TevLabel;

    Button1: TButton;
  private

    { Private declarations }
  public
    { Public declarations }
    State, BDate, EDate, FreqType, ACHKey: String;
  end;

var
  EDIT_HOLD_STATUS_UPDATE: TEDIT_HOLD_STATUS_UPDATE;

implementation

{$R *.DFM}

end.
