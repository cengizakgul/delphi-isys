// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_EXPORT_AR;

interface

uses SFrameEntry, Db,   Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit, EvContext,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Variants,
  Classes, Wwdatsrc, SDataStructure, wwDialog, wwrcdvw, Forms,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvCommonInterfaces,
  SDDClasses, ISDataAccessComponents, EvDataAccessComponents, SDataDicttemp,
  EvMainboard, EvUIUtils, EvUIComponents, EvClientDataSet, isUIEdit,
  isUIwwDBDateTimePicker, LMDCustomButton, LMDButton, isUILMDButton;

type
  TEDIT_SB_EXPORT_AR = class(TFrameEntry)
    PageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TevPanel;
    wwgdCompanies: TevDBGrid;
    wwDataSource2: TevDataSource;
    OpenDialog1: TOpenDialog;
    wwRecordViewDialog1: TwwRecordViewDialog;
    wwDataSource3: TevDataSource;
    SaveDialog1: TSaveDialog;
    dsData: TevDataSource;
    DM_TEMPORARY: TDM_TEMPORARY;
    evCDTmp: TevClientDataSet;
    Button2: TevBitBtn;
    Button1: TevBitBtn;
    Panel2: TPanel;
    Label1: TevLabel;
    wwDBDateTimePicker1: TevDBDateTimePicker;
    Label2: TevLabel;
    wwDBDateTimePicker2: TevDBDateTimePicker;
    Button3: TevBitBtn;
    Button4: TevBitBtn;
    Label4: TevLabel;
    Label3: TevLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    wwDBGrid1: TevDBGrid;
    ScrollBox2: TScrollBox;
    ListBox2: TListBox;
    evcbType: TevComboBox;
    wwClientDataSet1: TevClientDataSet;
    wwClientDataSet1CL_NBR: TIntegerField;
    wwClientDataSet1CUSTOM_CLIENT_NUMBER: TStringField;
    wwClientDataSet1CL_NAME: TStringField;
    wwClientDataSet1CO_NBR: TIntegerField;
    wwClientDataSet1CUSTOM_COMPANY_NUMBER: TStringField;
    wwClientDataSet1CO_NAME: TStringField;
    wwClientDataSet1INVOICE_NUMBER: TIntegerField;
    wwClientDataSet1INVOICE_DATE: TDateField;
    wwClientDataSet1INVOICE_AMOUNT: TFloatField;
    wwClientDataSet1INVOICE_DISCOUNT: TFloatField;
    wwClientDataSet1INVOICE_TAX: TFloatField;
    wwClientDataSet1CHECK_DATE: TDateField;
    wwClientDataSet1INVOICE_TYPE: TStringField;
    wwClientDataSet1DIVISION_NUMBER: TStringField;
    wwClientDataSet1TERMS_CODE: TStringField;
    wwClientDataSet1TAX_SCHEDULE: TStringField;
    wwClientDataSet1SALES_PERSON_NO: TStringField;
    wwClientDataSet1PRINT_INVOICE: TStringField;
    wwClientDataSet1SHIPPING_METHOD: TStringField;
    wwClientDataSet1CUSTOMER_PO_NO: TStringField;
    wwClientDataSet1CM_INVOICE_REF: TStringField;
    wwClientDataSet1INVOICE_COMMENT: TStringField;
    wwClientDataSet1ADJUSTMENT: TStringField;
    wwClientDataSet1REP_INV_REF: TStringField;
    wwClientDataSet1ACT_PRINTED: TStringField;
    wwClientDataSet1JOB_NUMBER: TStringField;
    wwClientDataSet1DISCOUNT_DUE_DATE: TStringField;
    wwClientDataSet1BATCH_NUMBER: TStringField;
    wwClientDataSet1INVALID_TX_CALC: TStringField;
    wwClientDataSet1BATCH_FAX: TStringField;
    wwClientDataSet1FAX_NUMBER: TStringField;
    wwClientDataSet1OPEN_STRING: TStringField;
    wwClientDataSet1COMMISSION_RATE: TFloatField;
    wwClientDataSet1TAXABLE_AMOUNT: TFloatField;
    wwClientDataSet1FREIGHT: TFloatField;
    wwClientDataSet1SALES_TAX: TFloatField;
    wwClientDataSet1COST_OF_SALES: TFloatField;
    wwClientDataSet1SALES_SUBJ_COMM: TFloatField;
    wwClientDataSet1COST_SUBJ_COMM: TFloatField;
    wwClientDataSet1COMMISSION_AMOUNT: TFloatField;
    wwClientDataSet1RETENTION_AMOUNT: TFloatField;
    wwClientDataSet1SALES_CODE: TStringField;
    wwClientDataSet1DESCRIPTION: TStringField;
    wwClientDataSet1SALES_ACCNT: TStringField;
    wwClientDataSet1COST_SALES_ACCNT: TStringField;
    wwClientDataSet1INVENTORY_ACCNT: TStringField;
    wwClientDataSet1UNIT_MEASURE: TStringField;
    wwClientDataSet1SUBJ_TO_EXEMPT: TStringField;
    wwClientDataSet1COMMISSIONABLE: TStringField;
    wwClientDataSet1TAX_CLASS: TStringField;
    wwClientDataSet1OPEN_STRING_2: TStringField;
    wwClientDataSet1QUANTITY: TIntegerField;
    wwClientDataSet1PRICE: TFloatField;
    wwClientDataSet1COST: TFloatField;
    wwClientDataSet1AMOUNT: TFloatField;
    wwClientDataSet1MISC_GL_ACCT: TStringField;
    wwClientDataSet1COMMENT_DESCRIP: TStringField;
    wwClientDataSet1OPEN_STRING_3: TStringField;
    wwClientDataSet1DT: TStringField;
    evEdit1: TevEdit;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    wwClientDataSet1SALES_PERSON_NAME: TStringField;
    cbSales: TevCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure evcbTypeChange(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
    procedure evEdit1Exit(Sender: TObject);
  private
    { Private declarations }
    FFileName: string;
    FileDirectory: String;
    procedure GatherRequiredData(BeginDate, EndDate: string);
    procedure GatherBillingInfo;
    procedure OpenClients;
    function WriteToFile(OutPut: string): Boolean;
    procedure SetFileName(const Value: string);
    procedure GetFileNameAndLocation;
    function PrepareARLine(DS: TevClientDataSet): String;
    function PrepareMASLine(DS: TevClientDataSet): String;
  public
    { Public declarations }
    property FileName: string read FFileName write SetFileName;
    procedure Activate; override;
    procedure Deactivate; override;    
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_EXPORT_AR: TEDIT_SB_EXPORT_AR;

implementation

uses EvConsts, EvUtils, SysUtils;

{$R *.DFM}

procedure TEDIT_SB_EXPORT_AR.Button1Click(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  //need to walk down the selected list
  ListBox2.Clear;
  with wwgdCompanies, wwgdCompanies.datasource.dataset do
  begin
    DisableControls; {Disable controls to improve performance}
    try
      for I := 0 to SelectedList.Count - 1 do
      begin
        GotoBookmark(SelectedList.items[I]);
        ListBox2.Items.Add(FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' - ' + FieldByName('NAME').AsString);
      end;
    finally
      EnableControls; { Re-enable controls }
    end;
  end;
  TabSheet2.TabVisible := True;
  PageControl1.ActivePage := TabSheet2;
end;

procedure TEDIT_SB_EXPORT_AR.GatherRequiredData(BeginDate,
  EndDate: string);
begin
  ctx_DataAccess.CUSTOM_VIEW.Close;
  with TExecDSWrapper.Create('ExportAR') do
  begin
    SetParam('CoNbr', wwgdCompanies.DataSource.DataSet.FieldByName('CO_NBR').AsInteger);
    SetParam('BeginDate', StrToDateTime(BeginDate));
    SetParam('EndDate', StrToDateTime(EndDate));
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
  end;
  ctx_DataAccess.CUSTOM_VIEW.Open;
  evCDTmp.Data := ctx_DataAccess.CUSTOM_VIEW.Data;
end;

procedure TEDIT_SB_EXPORT_AR.GatherBillingInfo;
var
  dtInvoiceDate: TDateTime;
  t: TEvClientDataSet;
  CoNbr: string;
  SrvNbr: Integer;
  CustNbr: Integer;
  Amount: Double;
  Quanty: Integer;
  open_fltr, retr_fltr: String;
begin
  DM_PAYROLL.PR.DataRequired('ALL');
  DM_CLIENT.CL.DataRequired('ALL');
  DM_COMPANY.CO.DataRequired('ALL');
  DM_COMPANY.CO_SALESPERSON.DataRequired('ALL');
  open_fltr := DM_SERVICE_BUREAU.SB_SERVICES.OpenCondition;
  retr_fltr := DM_SERVICE_BUREAU.SB_SERVICES.RetrieveCondition;
  DM_COMPANY.CO_SALESPERSON.AddIndex('1', 'CO_SALESPERSON_NBR', []);
  DM_COMPANY.CO_SALESPERSON.IndexName := '1';
  t := TEvClientDataSet.Create(nil);
  wwClientDataSet1.DisableControls;
  try
    with evCDTmp do//ctx_DataAccess.CUSTOM_VIEW do
    begin
      First;
      while not Eof do
      begin
        with t do
        begin
          if not Active or not Locate('INVOICE_NUMBER', evCDTmp.FieldByName('Invoice_Number').AsInteger, []) then
            Data := ctx_PayrollProcessing.ProcessBilling(evCDTmp.FieldByName('Pr_Nbr').AsInteger, false,
              evCDTmp.FieldByName('Invoice_Number').AsInteger, True)[0];
          if Locate('INVOICE_NUMBER', evCDTmp.FieldByName('Invoice_Number').AsInteger, []) then
          begin
            dtInvoiceDate := FieldByName('Invoice_Date').AsDateTime;
            if evcbType.ItemIndex = 0 then
            begin
              wwClientDataSet1.Insert;
              wwClientDataSet1.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := FieldByName('custom_company_number').AsString;
              wwClientDataSet1.FieldByName('INVOICE_NUMBER').AsInteger := FieldByName('Invoice_Number').AsInteger;
              wwClientDataSet1.FieldByName('INVOICE_DATE').AsDateTime := dtInvoiceDate;
              wwClientDataSet1.FieldByName('INVOICE_AMOUNT').AsFloat := FieldByName('Invoice_Amount').AsFloat;
              wwClientDataSet1.FieldByName('INVOICE_DISCOUNT').AsFloat := FieldByName('Invoice_Discount').AsFloat;
              wwClientDataSet1.FieldByName('INVOICE_TAX').AsFloat := FieldByName('Invoice_Tax').AsFloat;
              wwClientDataSet1.FieldByName('CHECK_DATE').AsDateTime := FieldByName('Check_Date').AsDateTime;
              wwClientDataSet1.FieldByName('CO_NBR').AsInteger := FieldByName('CO_NBR').AsInteger;
              CoNbr := FieldByName('CO_NBR').AsString;
              if CoNbr <> '' then
                wwClientDataSet1.FieldByName('CO_NAME').AsString := DM_COMPANY.CO.Lookup('CO_NBR',
                     FieldByName('CO_NBR').AsInteger, 'NAME');
              wwClientDataSet1.FieldByName('CL_NBR').AsInteger := FieldByName('CL_NBR').AsInteger;
              wwClientDataSet1.FieldByName('CUSTOM_CLIENT_NUMBER').AsString := DM_CLIENT.CL.Lookup('CL_NBR',
                FieldByName('CL_NBR').AsInteger, 'CUSTOM_CLIENT_NUMBER');
              wwClientDataSet1.FieldByName('CL_NAME').AsString := DM_CLIENT.CL.Lookup('CL_NBR',
                FieldByName('CL_NBR').AsInteger, 'NAME');
              wwClientDataSet1.Post;
            end
            else
            begin
              DM_COMPANY.CO_BILLING_HISTORY.DataRequired('CO_BILLING_HISTORY_NBR='+FieldByName('CO_BILLING_HISTORY_NBR').AsString);
              DM_SERVICE_BUREAU.SB_SERVICES.DataRequired('ALL');
              DM_COMPANY.CO_SERVICES.DataRequired('ALL');
              DM_COMPANY.CO_BILLING_HISTORY_DETAIL.DataRequired('CO_BILLING_HISTORY_NBR='+FieldByName('CO_BILLING_HISTORY_NBR').AsString);
              while not DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Eof do
              begin
                wwClientDataSet1.Insert;
                wwClientDataSet1.FieldByName('CUSTOM_COMPANY_NUMBER').AsString := FieldByName('custom_company_number').AsString;
                wwClientDataSet1.FieldByName('INVOICE_NUMBER').AsInteger := FieldByName('Invoice_Number').AsInteger;
                wwClientDataSet1.FieldByName('INVOICE_DATE').AsDateTime := dtInvoiceDate;
                wwClientDataSet1.FieldByName('INVOICE_AMOUNT').AsFloat := FieldByName('Invoice_Amount').AsFloat;
                wwClientDataSet1.FieldByName('INVOICE_DISCOUNT').AsFloat := FieldByName('Invoice_Discount').AsFloat;
                wwClientDataSet1.FieldByName('INVOICE_TAX').AsFloat := FieldByName('Invoice_Tax').AsFloat;
                wwClientDataSet1.FieldByName('CHECK_DATE').AsDateTime := FieldByName('Check_Date').AsDateTime;
                wwClientDataSet1.FieldByName('CO_NBR').AsInteger := FieldByName('CO_NBR').AsInteger;

                wwClientDataSet1.FieldByName('INVOICE_TYPE').AsString := 'IN';
                wwClientDataSet1.FieldByName('DIVISION_NUMBER').AsString := '04';
                wwClientDataSet1.FieldByName('TERMS_CODE').AsString := '00';
                wwClientDataSet1.FieldByName('TAX_SCHEDULE').AsString := '         ';
                wwClientDataSet1.FieldByName('PRINT_INVOICE').AsString := 'Y';
                wwClientDataSet1.FieldByName('SHIPPING_METHOD').AsString := '               ';
                wwClientDataSet1.FieldByName('CUSTOMER_PO_NO').AsString := '               ';
                wwClientDataSet1.FieldByName('CM_INVOICE_REF').AsString := '       ';
                wwClientDataSet1.FieldByName('INVOICE_COMMENT').AsString := '                              ';
                wwClientDataSet1.FieldByName('ADJUSTMENT').AsString := 'N';
                wwClientDataSet1.FieldByName('REP_INV_REF').AsString := '    ';
                wwClientDataSet1.FieldByName('ACT_PRINTED').AsString := 'Y';
                wwClientDataSet1.FieldByName('JOB_NUMBER').AsString := '                                     ';
                wwClientDataSet1.FieldByName('DISCOUNT_DUE_DATE').AsString := '000000';
                wwClientDataSet1.FieldByName('BATCH_NUMBER').AsString := '00000';
                wwClientDataSet1.FieldByName('INVALID_TX_CALC').AsString := 'N';
                wwClientDataSet1.FieldByName('BATCH_FAX').AsString := 'N';
                wwClientDataSet1.FieldByName('FAX_NUMBER').AsString := '                 ';
                wwClientDataSet1.FieldByName('OPEN_STRING').AsString := '                ';
                wwClientDataSet1.FieldByName('COMMISSION_RATE').AsFloat := 0;
                wwClientDataSet1.FieldByName('TAXABLE_AMOUNT').AsFloat := 0;
                wwClientDataSet1.FieldByName('FREIGHT').AsFloat := 0;
                wwClientDataSet1.FieldByName('COST_OF_SALES').AsFloat := 0;
                wwClientDataSet1.FieldByName('SALES_SUBJ_COMM').AsFloat := 0;
                wwClientDataSet1.FieldByName('COST_SUBJ_COMM').AsFloat := 0;
                wwClientDataSet1.FieldByName('COMMISSION_AMOUNT').AsFloat := 0;
                wwClientDataSet1.FieldByName('RETENTION_AMOUNT').AsFloat := 0;

                if DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('SB_SERVICES_NBR').IsNull then
                begin
                  DM_COMPANY.CO_SERVICES.DataRequired('CO_SERVICES_NBR='+QuotedStr(DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('CO_SERVICES_NBR').AsString));
                  SrvNbr := DM_COMPANY.CO_SERVICES.FieldByName('SB_SERVICES_NBR').AsInteger;
                end
                else
                  SrvNbr := DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('SB_SERVICES_NBR').AsInteger;

                DM_SERVICE_BUREAU.SB_SERVICES.DataRequired('SB_SERVICES_NBR='+QuotedStr(IntToStr(SrvNbr)));

                wwClientDataSet1.FieldByName('SALES_CODE').AsString := DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('PRODUCT_CODE').AsString;
                wwClientDataSet1.FieldByName('DESCRIPTION').AsString := DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SERVICE_NAME').AsString;
                wwClientDataSet1.FieldByName('SALES_ACCNT').AsString := evEdit1.Text;
                wwClientDataSet1.FieldByName('COST_SALES_ACCNT').AsString := evEdit1.Text;
                wwClientDataSet1.FieldByName('INVENTORY_ACCNT').AsString := evEdit1.Text;
                wwClientDataSet1.FieldByName('UNIT_MEASURE').AsString := 'EA  ';
                wwClientDataSet1.FieldByName('SUBJ_TO_EXEMPT').AsString := 'N';
                wwClientDataSet1.FieldByName('COMMISSIONABLE').AsString := 'N';

                if DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SALES_TAXABLE').AsString = 'N' then
                  wwClientDataSet1.FieldByName('TAX_CLASS').AsString  := 'NT'
                else
                  wwClientDataSet1.FieldByName('TAX_CLASS').AsString  := 'T ';

                wwClientDataSet1.FieldByName('OPEN_STRING').AsString := '         ';
                Quanty := DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('QUANTITY').AsInteger;
                Amount := DM_COMPANY.CO_BILLING_HISTORY_DETAIL.FieldByName('AMOUNT').AsFloat;
                wwClientDataSet1.FieldByName('QUANTITY').AsInteger := Quanty;
                wwClientDataSet1.FieldByName('COST').AsFloat := 0;

                if Quanty <> 0 then
                  wwClientDataSet1.FieldByName('PRICE').AsFloat := Amount/Quanty;

                wwClientDataSet1.FieldByName('AMOUNT').AsFloat := Amount;
                wwClientDataSet1.FieldByName('MISC_GL_ACCT').AsString := '000000000';
                wwClientDataSet1.FieldByName('COMMENT_DESCRIP').AsString := '                                                  ';
                wwClientDataSet1.FieldByName('OPEN_STRING').AsString := '              ';
                wwClientDataSet1.FieldByName('DT').AsString := 'DT';

                CoNbr := FieldByName('CO_NBR').AsString;
                if CoNbr <> '' then
                  wwClientDataSet1.FieldByName('CO_NAME').AsString := DM_COMPANY.CO.Lookup('CO_NBR',
                       FieldByName('CO_NBR').AsInteger, 'NAME');
                wwClientDataSet1.FieldByName('CL_NBR').AsInteger := FieldByName('CL_NBR').AsInteger;
                wwClientDataSet1.FieldByName('CUSTOM_CLIENT_NUMBER').AsString := DM_CLIENT.CL.Lookup('CL_NBR',
                  FieldByName('CL_NBR').AsInteger, 'CUSTOM_CLIENT_NUMBER');

                wwClientDataSet1.FieldByName('SALES_PERSON_NO').AsString := '    ';
                if DM_COMPANY.CO_SALESPERSON.RecordCount > 0 then
                begin
                  CustNbr := DM_COMPANY.CO_SALESPERSON.FieldByName('SB_USER_NBR').AsInteger;
                  DM_SERVICE_BUREAU.SB_USER.DataRequired('SB_USER_NBR='+QuotedStr(IntToStr(CustNbr)));
                  wwClientDataSet1.FieldByName('SALES_PERSON_NAME').AsString := DM_SERVICE_BUREAU.SB_USER.FieldByName('FIRST_NAME').AsString + ' ' + DM_SERVICE_BUREAU.SB_USER.FieldByName('LAST_NAME').AsString;
                end
                else
                 wwClientDataSet1.FieldByName('SALES_PERSON_NAME').AsString := PadStringRight('', ' ', 30);
                wwClientDataSet1.FieldByName('CL_NAME').AsString := DM_CLIENT.CL.Lookup('CL_NBR',
                  FieldByName('CL_NBR').AsInteger, 'NAME');
                wwClientDataSet1.Post;
                DM_COMPANY.CO_BILLING_HISTORY_DETAIL.Next;
              end;
            end;
          end;
        end;
        Next;
      end;
    end;
    if DM_PAYROLL.PR.Active then
      DM_PAYROLL.PR.Close;
  finally
    t.Free;
    wwClientDataSet1.EnableControls;
    DM_COMPANY.CO_SALESPERSON.DeleteIndex('1');
    DM_SERVICE_BUREAU.SB_SERVICES.DataRequired(open_fltr);
    DM_SERVICE_BUREAU.SB_SERVICES.RetrieveCondition := retr_fltr;
  end;
end;

procedure TEDIT_SB_EXPORT_AR.OpenClients;
var
  I: Integer;
  CoName, CustomCoNbr: string;
begin
  wwgdCompanies.DataSource.DataSet.DisableControls;
  try
    for I := 0 to ListBox2.Items.Count - 1 do
    begin
      CustomCoNbr := Copy(ListBox2.Items.Strings[I], 1, Pos(' - ', ListBox2.Items.Strings[I]) - 1);
      CoName := Copy(ListBox2.Items.Strings[I], Pos(' - ', ListBox2.Items.Strings[I]) + 3, Length(ListBox2.Items.Strings[I]));
      CoName := QuotedStr(CoName);
      Delete(CoName, 1, 1);
      Delete(CoName, Length(CoName), 1);
      if wwgdCompanies.DataSource.DataSet.Locate('CUSTOM_COMPANY_NUMBER', VarArrayOf([CustomCoNbr]), []) then
      begin
        ctx_UpdateWait('Company (' + IntToStr(Succ(i)) + ' of ' + IntToStr(ListBox2.Items.Count) +  ') ' + Trim(CustomCoNbr) + ' ' + CoName);
        ctx_DataAccess.OpenClient(wwgdCompanies.DataSource.DataSet.FieldByName('CL_NBR').AsInteger);
        GatherRequiredData(wwDBDateTimePicker1.Text, wwDBDateTimePicker2.Text);
        if evCDTmp.RecordCount = 0 then
          Continue;
        DM_PAYROLL.PR.DataRequired('ALL');
        DM_COMPANY.CO.DataRequired('CO_NBR='+wwgdCompanies.DataSource.DataSet.FieldByName('CO_NBR').AsString);
        GatherBillingInfo;
      end;
    end;
  finally
    wwgdCompanies.DataSource.DataSet.EnableControls;
  end;
end;

procedure TEDIT_SB_EXPORT_AR.Button3Click(Sender: TObject);
begin
  inherited;
  ctx_StartWait;
  wwClientDataSet1.EmptyDataSet;
  try
    OpenClients;
    Button4.Enabled := (wwClientDataSet1.RecordCount > 0);
  finally
    ctx_EndWait;
  end;
end;

procedure TEDIT_SB_EXPORT_AR.Button2Click(Sender: TObject);
begin
  inherited;
  //need to walk down the selected list
  ListBox2.Clear;
  wwgdCompanies.UnselectAll; //SelectedList.Clear;
  with wwgdCompanies, wwgdCompanies.datasource.dataset do
  begin
    DisableControls; {Disable controls to improve performance}
    try
      ListBox2.Clear;
      SelectedList.Clear;
      First;
      while not Eof do
      begin
        ListBox2.Items.Add(FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' - ' + FieldByName('NAME').AsString);
        Next;
      end;
    finally
      EnableControls; { Re-enable controls }
    end;
  end;
  TabSheet2.TabVisible := True;
  PageControl1.ActivePage := TabSheet2;
end;

procedure TEDIT_SB_EXPORT_AR.Button4Click(Sender: TObject);
var
  MyList: TStringList;
  I: Integer;
  MyTransactionManager: TTransactionManager;
  LineOut, Tmp: string;
begin
  inherited;
  ctx_StartWait;
  try
    MyList := TStringList.Create;
    MyTransactionManager := TTransactionManager.Create(Application);
    MyTransactionManager.Initialize([DM_COMPANY.CO_BILLING_HISTORY]);
    with wwClientDataSet1 do
    begin
      First;
      while not eof do
      begin
        Tmp := FieldByName('CL_NBR').AsString + '=' + FieldByName('CO_NBR').AsString;
        if MyList.IndexOf(Tmp) = -1 then
          MyList.Add(FieldByName('CL_NBR').AsString + '=' + FieldByName('CO_NBR').AsString);
        Next;
      end;

      for I := 0 to MyList.Count - 1 do
      begin
        Tmp := MyList[I];
        Filter := 'CL_NBR=''' + Copy(Tmp, 1, Pos('=', Tmp)-1) + ''' and CO_NBR='''+ Copy(Tmp, Pos('=',tmp)+1, Length(Tmp))+'''';
        Filtered := True;

        First;
        while not Eof do
        begin
          ctx_DataAccess.OpenClient(StrToInt(MyList.Names[I]));
          DM_COMPANY.CO.DataRequired('CO_NBR=' + Copy(MyList[I], Pos('=',MyList[I])+1,Length(MyList[I])));
          DM_COMPANY.CO_BILLING_HISTORY.DataRequired('CO_NBR=' + Copy(MyList[I], Pos('=',MyList[I])+1,Length(MyList[I])));

          if DM_COMPANY.CO_BILLING_HISTORY.Locate('CO_NBR;INVOICE_NUMBER;INVOICE_DATE', VarArrayOf([FieldByName('CO_NBR').AsString,
            FieldByName('INVOICE_NUMBER').AsString, FieldByName('INVOICE_DATE').AsString]), []) then
          begin

            case evcbType.ItemIndex of
             0: LineOut := PrepareARLine(wwClientDataSet1);
             1: LineOut := PrepareMASLine(wwClientDataSet1);
            end;
            //ACTUALLY DUMP LINEOUT TO THE FILE
            if WriteToFile(LineOut) then
            begin
              DM_COMPANY.CO_BILLING_HISTORY.Edit;
              DM_COMPANY.CO_BILLING_HISTORY.FieldByName('EXPORTED_TO_A_R').AsString := EXPORTED_TO_A_R_STATUS_YES;
              DM_COMPANY.CO_BILLING_HISTORY.Post;
            end;
          end;
          Next;
        end;
        try
          MyTransactionManager.ApplyUpdates;
        except
          MyTransactionManager.CancelUpdates;
        end;
      end;
    end;
  finally
    wwClientDataSet1.Filtered := False;
    Button4.Enabled := False;
    ctx_EndWait;
  end;
end;

function TEDIT_SB_EXPORT_AR.WriteToFile(OutPut: string): Boolean;
var
  F: TextFile;
  WriteSuccessful: Boolean;
begin

  WriteSuccessful := False;
  if FileExists(FileName) then
  begin
    AssignFile(F, FileName);
    try
      Append(F);
      WriteLn(F, OutPut);
      Flush(F);
      CloseFile(F);
      WriteSuccessful := True;
    except
      WriteSuccessful := False;
    end;
  end
  else
    if not FileExists(FileName) then
  begin
    AssignFile(F, FileName);
    try
      ReWrite(F);
      WriteLn(F, OutPut);
      Flush(F);
      CloseFile(F);
      WriteSuccessful := True;
    except
      WriteSuccessful := False;
    end;
  end;
  result := WriteSuccessful;
end;

procedure TEDIT_SB_EXPORT_AR.SetFileName(const Value: string);
begin
  FFileName := Value;
end;

procedure TEDIT_SB_EXPORT_AR.GetFileNameAndLocation;
var
  sDay, sMonth: string;
  Day, Month, Year: Word;
begin

  DecodeDate(now, Year, Month, Day);
  sDay := IntToStr(Day);
  sMonth := IntToStr(Month);
  if Length(sDay) = 1 then
    Insert('0', sDay, 1);

  if Length(sMonth) = 1 then
    Insert('0', sMonth, 1);

  FileDirectory := DM_SERVICE_BUREAU.SB.FieldByName('AR_IMPORT_DIRECTORY').AsString;

  if (FileDirectory = '') or (not DirectoryExists(FileDirectory)) then
  begin
    SaveDialog1.FileName := TFileName(sMonth + sDay + '.txt');
    SaveDialog1.Execute;
    FileName := SaveDialog1.FileName;
    //PageControl1.Visible := False;
    //EvMessage('Please go to the Service Bureau screen and set the location to save A/R export file!');
  end
  else
  begin
    if FileDirectory[SizeOf(FileDirectory)+1] <> '\' then
      FileDirectory := FileDirectory+'\';
    FileName := FileDirectory + sMonth + sDay + '.txt';
    TabSheet1.TabVisible := True;
  end;
end;

procedure TEDIT_SB_EXPORT_AR.Deactivate;
begin
  inherited;
  DM_TEMPORARY.TMP_CO.Filter := '';
  DM_TEMPORARY.TMP_CO.Filtered := False;
end;

procedure TEDIT_SB_EXPORT_AR.Activate;
var
 sFilter : String;
begin
  inherited;
  wwClientDataSet1.CreateDataSet;
  wwDBDateTimePicker1.Date := Now;
  wwDBDateTimePicker2.Date := Now;
  FileName := '';
  GetFileNameAndLocation;
  TabSheet2.TabVisible := False;
  Button4.Enabled := False;

  sFilter := GetReadOnlyClintCompanyListFilter(false);
  if sFilter <> '' then
  begin
    DM_TEMPORARY.TMP_CO.Filter := sFilter;
    DM_TEMPORARY.TMP_CO.Filtered := true;
  end;
end;

procedure TEDIT_SB_EXPORT_AR.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS([DM_TEMPORARY.TMP_CO, DM_SERVICE_BUREAU.SB], SupportDataSets);
end;

function TEDIT_SB_EXPORT_AR.PrepareARLine(DS: TevClientDataSet): String;
var
  Total: Double;
begin

  if cbSales.Checked then
    Total := DS.FieldByName('INVOICE_AMOUNT').AsFloat + DS.FieldByName('INVOICE_TAX').AsFloat
  else
    Total := DS.FieldByName('INVOICE_AMOUNT').AsFloat;

  with DS do
  begin
    Result := '"' + FieldByName('CUSTOM_COMPANY_NUMBER').AsString + '","' + FieldByName('INVOICE_NUMBER').AsString + '","' +
                    FormatDateTime('mm/dd/yy', FieldByName('INVOICE_DATE').AsDateTime) + '","' + FormatFloat('0.00',
                    Total) + '","","","' + FormatFloat('0.00', FieldByName('INVOICE_DISCOUNT').AsFloat) + '","' +
                    '","",""';
  end;
end;

function TEDIT_SB_EXPORT_AR.PrepareMASLine(DS: TevClientDataSet): String;
var
  S: String;
begin
  with DS do
  begin

    S := PadLeft(wwClientDataSet1.FieldByName('INVOICE_NUMBER').AsString, '0', 7);
    S := S + wwClientDataSet1.FieldByName('INVOICE_TYPE').AsString;
    S := S + wwClientDataSet1.FieldByName('DIVISION_NUMBER').AsString;
    S := S + PadRight(wwClientDataSet1.FieldByName('CUSTOM_COMPANY_NUMBER').AsString, ' ', 7);
    S := S + PadRight(wwClientDataSet1.FieldByName('CO_NAME').AsString, ' ', 30);
    S := S + FormatDateTime('MMDDYY', wwClientDataSet1.FieldByName('INVOICE_DATE').AsDateTime);
    S := S + wwClientDataSet1.FieldByName('TERMS_CODE').AsString;
    S := S + PadRight(wwClientDataSet1.FieldByName('TAX_SCHEDULE').AsString, ' ', 9);
    S := S + PadRight(wwClientDataSet1.FieldByName('SALES_PERSON_NO').AsString, ' ', 4);
    S := S + PadRight(wwClientDataSet1.FieldByName('SALES_PERSON_NAME').AsString, ' ', 30);
    S := S + wwClientDataSet1.FieldByName('PRINT_INVOICE').AsString;
    S := S + PadRight(wwClientDataSet1.FieldByName('SHIPPING_METHOD').AsString, ' ', 15);
    S := S + PadRight(wwClientDataSet1.FieldByName('CUSTOMER_PO_NO').AsString, ' ', 15);
    S := S + PadRight(wwClientDataSet1.FieldByName('CM_INVOICE_REF').AsString, ' ', 7);
    S := S + PadRight(wwClientDataSet1.FieldByName('INVOICE_COMMENT').AsString, ' ', 30);
    S := S + wwClientDataSet1.FieldByName('ADJUSTMENT').AsString;
    S := S + PadRight(wwClientDataSet1.FieldByName('REP_INV_REF').AsString, ' ', 4);
    S := S + wwClientDataSet1.FieldByName('ACT_PRINTED').AsString;
    S := S + PadLeft(wwClientDataSet1.FieldByName('JOB_NUMBER').AsString, ' ', 37);
    S := S + FormatDateTime('MMDDYY', wwClientDataSet1.FieldByName('INVOICE_DATE').AsDateTime);
    S := S + wwClientDataSet1.FieldByName('DISCOUNT_DUE_DATE').AsString;
    S := S + wwClientDataSet1.FieldByName('BATCH_NUMBER').AsString;
    S := S + wwClientDataSet1.FieldByName('INVALID_TX_CALC').AsString;
    S := S + wwClientDataSet1.FieldByName('BATCH_FAX').AsString;
    S := S + PadLeft(wwClientDataSet1.FieldByName('FAX_NUMBER').AsString, ' ', 17);
    S := S + PadLeft(wwClientDataSet1.FieldByName('OPEN_STRING').AsString, ' ', 16);
    S := S + FormatFloat('0000.000" ";0000.000"-";', wwClientDataSet1.FieldByName('COMMISSION_RATE').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('TAXABLE_AMOUNT').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('INVOICE_AMOUNT').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('FREIGHT').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('INVOICE_TAX').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('COST_OF_SALES').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('SALES_SUBJ_COMM').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('COST_SUBJ_COMM').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('COMMISSION_AMOUNT').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('RETENTION_AMOUNT').AsFloat);
    S := S + PadRight(wwClientDataSet1.FieldByName('SALES_CODE').AsString, ' ',10);
    S := S + PadRight(wwClientDataSet1.FieldByName('DESCRIPTION').AsString, ' ', 30);
    S := S + PadRight(wwClientDataSet1.FieldByName('SALES_ACCNT').AsString, ' ', 9);
    S := S + PadRight(wwClientDataSet1.FieldByName('COST_SALES_ACCNT').AsString, ' ', 9);
    S := S + PadRight(wwClientDataSet1.FieldByName('INVENTORY_ACCNT').AsString, ' ', 9);
    S := S + PadRight(wwClientDataSet1.FieldByName('UNIT_MEASURE').AsString, ' ', 4);
    S := S + wwClientDataSet1.FieldByName('SUBJ_TO_EXEMPT').AsString;
    S := S + wwClientDataSet1.FieldByName('COMMISSIONABLE').AsString;
    S := S + PadRight(wwClientDataSet1.FieldByName('TAX_CLASS').AsString, ' ', 2);
    S := S + PadRight(wwClientDataSet1.FieldByName('OPEN_STRING').AsString, ' ', 9);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('QUANTITY').AsInteger);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('PRICE').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('COST').AsFloat);
    S := S + FormatFloat('00000000.00" ";00000000.00"-"', wwClientDataSet1.FieldByName('AMOUNT').AsFloat);
    S := S + PadRight(wwClientDataSet1.FieldByName('MISC_GL_ACCT').AsString, '0', 9);
    S := S + PadRight(wwClientDataSet1.FieldByName('COMMENT_DESCRIP').AsString, ' ', 50);
    S := S + PadRight(wwClientDataSet1.FieldByName('OPEN_STRING').AsString, ' ', 14);
    S := S + wwClientDataSet1.FieldByName('DT').AsString;
    Result := S;
  end;
end;


procedure TEDIT_SB_EXPORT_AR.evcbTypeChange(Sender: TObject);
begin
  inherited;
  mb_AppSettings.AsInteger['Settings\ARType'] := evcbType.ItemIndex;
  evLabel2.Visible := evcbType.ItemIndex = 1;
  evEdit1.Visible := evcbType.ItemIndex = 1;
  cbSales.Visible := evcbType.ItemIndex = 0;
end;

procedure TEDIT_SB_EXPORT_AR.TabSheet2Show(Sender: TObject);
var
  RegIndex: Integer;
begin
  inherited;
  RegIndex := mb_AppSettings.AsInteger['Settings\ARType'];
  evcbType.ItemIndex := RegIndex;
  evEdit1.Text := mb_AppSettings['Settings\GLCode'];
  evLabel2.Visible := evcbType.ItemIndex = 1;
  evEdit1.Visible := evcbType.ItemIndex = 1;
  cbSales.Visible := evcbType.ItemIndex = 0;  
end;

procedure TEDIT_SB_EXPORT_AR.evEdit1Exit(Sender: TObject);
begin
  inherited;
  mb_AppSettings['Settings\GLCode'] := evEdit1.Text;
end;

initialization
  RegisterClass(TEDIT_SB_EXPORT_AR);

end.
