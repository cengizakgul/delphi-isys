// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_BANK_ACCOUNT_REGISTER;

interface

uses SFrameEntry, Db,   EvUtils, Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit, EvBasicUtils,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Variants,
  Windows, Classes, Wwdatsrc, SDataStructure, SysUtils, SReportSettings, EvTypes,
  Spin, SDDClasses, kbmMemTable, ISKbmMemDataSet, EvContext,
  ISBasicClasses, EvDataAccessComponents, ISDataAccessComponents,
  SDataDicttemp, SDataDictbureau, SPD_MemoEditor, EvExceptions, SSecurityInterface,
  SPD_EDIT_HOLD_STATUS_UPDATE, isBaseClasses, EvMainboard, EvUIUtils, EvUIComponents, EvClientDataSet,
  isUIEdit, LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton,
  isUIwwDBDateTimePicker, isUIwwDBEdit, isUIwwDBComboBox,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton, EvDataset;

type
  TBankMergeFormat = (bfGeneric2000, bfKeyBank2002);
  TEDIT_SB_BANK_ACCOUNT_REGISTER = class(TFrameEntry)
    pctlEDIT_SB_BANK_ACCOUNTS: TevPageControl;
    tsht1: TTabSheet;
    wwdgTeam: TevDBGrid;
    tsht2: TTabSheet;
    tsht3: TTabSheet;
    tsht2CompFilterGroup: TevGroupBox;
    tsht2CompFilterGroupGrid: TevDBGrid;
    tsht2DateFilterGroup: TevGroupBox;
    tsht2CompFilterGroupRbAll: TevRadioButton;
    tsht2CompFilterGroupRbSelect: TevRadioButton;
    tsht2DateFilterGroupRbAll: TevRadioButton;
    tsht2DateFilterGroupRbPeriod: TevRadioButton;
    tsht2DateFilterGroupDateFrom: TevDateTimePicker;
    tsht2DateFilterGroupDateTill: TevDateTimePicker;
    tsht3wwDBGrid: TevDBGrid;
    wwClientDataSetClCo: TevClientDataSet;
    wwDataSourceClCo: TevDataSource;
    CO_MANUAL_ACH_NBR: TevLabel;
    tsht3DbLookupManualACH: TevDBLookupCombo;
    tsht3DbLookupClient: TevDBLookupCombo;
    l1: TevLabel;
    Label1: TevLabel;
    Label2: TevLabel;
    tsht3DbLookupPrACH: TevDBLookupCombo;
    Label3: TevLabel;
    tsht3DbLookupTaxACH: TevDBLookupCombo;
    tsht3DbLookupCompany: TevDBLookupCombo;
    wwClientDataSetCo: TevClientDataSet;
    tsht4: TTabSheet;
    grpTwImport: TevGroupBox;
    spbBankMergeSourceFile: TevSpeedButton;
    Label4: TevLabel;
    spbBankMergeReportDir: TevSpeedButton;
    Label5: TevLabel;
    editBankMergeSourceFile: TevEdit;
    editBankMergeReportFile: TevEdit;
    ckboxBankMergeFirst: TevCheckBox;
    dlgBankMergeSourceFile: TOpenDialog;
    dlgBankMergeStorageFile: TSaveDialog;
    tsht4BankName: TevLabel;
    tsht3wwDBComboManualType: TevDBComboBox;
    tsht3wwDBComboType: TevDBComboBox;
    tsht3wwDBComboStatus: TevDBComboBox;
    Label6: TevLabel;
    editBankMergeStorageFile: TevEdit;
    spbBankMergeStorageDir: TevSpeedButton;
    dlgBankMergeReportFile: TSaveDialog;
    Label7: TevLabel;
    editStorageReportDateFrom: TevDateTimePicker;
    Label8: TevLabel;
    editStorageReportDateTill: TevDateTimePicker;
    Label9: TevLabel;
    cboStorageReportTransferType: TevComboBox;
    wwStorageDataSet: TevClientDataSet;
    wwStorageDataSetType: TStringField;
    wwStorageDataSetCompNumber: TStringField;
    wwStorageDataSetCheckNumber: TIntegerField;
    wwStorageDataSetClearDate: TDateField;
    wwStorageDataSetAmount: TCurrencyField;
    wwStorageDataSetProblem: TStringField;
    wwStorageDataSource: TevDataSource;
    tsht5: TTabSheet;
    tsht5BalanceGroup: TevGroupBox;
    tsht5BankName: TevLabel;
    tsht5FilterParam: TevLabel;
    tsht5RadioBasedOn: TRadioGroup;
    tsht5RadioSortBy: TRadioGroup;
    tsht5RadioSummaryType: TRadioGroup;
    tsht5RadioTransactionStatus: TRadioGroup;
    tsht5CompanyRadioOnlyWithTrans: TRadioGroup;
    tsht5RadioCreditDebit: TRadioGroup;
    tsht5RadioCompanyPageBreak: TRadioGroup;
    GroupBox1: TevGroupBox;
    DBEdit1: TevDBEdit;
    Label10: TevLabel;
    GroupBox2: TevGroupBox;
    Label11: TevLabel;
    edDefaultCrearedDate: TevDBDateTimePicker;
    pProcessBar: TevPanel;
    lProcessBarLabel: TevLabel;
    pbProgressBar: TProgressBar;
    rgSourceFileType: TRadioGroup;
    lTMP_CO_BANK_ACCOUNT_REGISTER: TevClientDataSet;
    lTMP_CO_BANK_ACCOUNT_REGISTERCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCL_NBR: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCO_NBR: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERSB_BANK_ACCOUNTS_NBR: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERPR_CHECK_NBR: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCHECK_SERIAL_NUMBER: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCHECK_DATE: TDateField;
    lTMP_CO_BANK_ACCOUNT_REGISTERSTATUS: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERSTATUS_DATE: TDateTimeField;
    lTMP_CO_BANK_ACCOUNT_REGISTERTRANSACTION_EFFECTIVE_DATE: TDateTimeField;
    lTMP_CO_BANK_ACCOUNT_REGISTERAMOUNT: TFloatField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCLEARED_AMOUNT: TFloatField;
    lTMP_CO_BANK_ACCOUNT_REGISTERTYPE: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERMANUAL_TYPE: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCO_MANUAL_ACH_NBR: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCO_TAX_PAYMENT_ACH_NBR: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCO_PR_ACH_NBR: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERPROCESS_DATE: TDateTimeField;
    lTMP_CO_BANK_ACCOUNT_REGISTERNOTES: TBlobField;
    lTMP_CO_BANK_ACCOUNT_REGISTERFILLER: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCLEARED_DATE: TDateField;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_TEMPORARY: TDM_TEMPORARY;
    Label12: TLabel;
    edMergeUpToDate: TevDateTimePicker;
    TempDataSet: TevClientDataSet;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField1: TIntegerField;
    DateTimeField1: TDateField;
    CurrencyField1: TCurrencyField;
    StringField3: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERPayee: TStringField;
    bOpenRegister: TevButton;
    evRadioGroup1: TevRadioGroup;
    btnMarkCleared: TevBitBtn;
    btnMarkUncleared: TevBitBtn;
    Button1: TevBitBtn;
    btnTwImportGo: TevBitBtn;
    btnTwImportView: TevBitBtn;
    btnStorageScan: TevBitBtn;
    btnStorageReport: TevBitBtn;
    btnStorageReportPreview: TevBitBtn;
    tsht5ReportRunBtn: TevBitBtn;
    cbCompPosOr: TevCheckBox;
    edCompPosOr: TevSpinEdit;
    evCheckBox1: TevCheckBox;
    lTMP_CO_BANK_ACCOUNT_REGISTERPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField;
    gbDateFilterOptions: TRadioGroup;
    gbClearedFilterOptons: TevRadioGroup;
    lTMP_CO_BANK_ACCOUNT_REGISTERCLCustNum: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCoCustNum: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCONAME: TStringField;
    btnNotes: TevBitBtn;
    CreditHoldBtn: TButton;
    lTMP_CO_BANK_ACCOUNT_REGISTERBANK_ACCOUNT_REGISTER_NAME: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERGROUP_IDENTIFIER: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERTRACE_NUMBER: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERCO_TAX_DEPOSITS_NBR: TIntegerField;
    cdGroupIdentifierTotals: TevClientDataSet;
    cdGroupIdentifierTotalsGROUP_IDENTIFIER: TStringField;
    cdGroupIdentifierTotalsCL_NBR: TIntegerField;
    cdGroupIdentifierTotalsAMOUNT: TCurrencyField;
    tshtCon: TTabSheet;
    evgrdConsolidate: TevDBGrid;
    dsConsolidate: TevDataSource;
    cdGroupIdentifierTotalsGroupIdentifierCount: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTERGroupIdentifierCount: TIntegerField;
    evRelatedTransaction: TevBitBtn;
    cdCoBankAccRegDetails: TevClientDataSet;
    IntegerField2: TIntegerField;
    cdCoBankAccRegDetailsCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField;
    cdCoBankAccRegDetailsCLIENT_CO_BANK_ACC_REG_NBR: TIntegerField;
    lTMP_CO_BANK_ACCOUNT_REGISTEROneCheckBarNbr: TIntegerField;
    cdConsolidatedBar: TevClientDataSet;
    cdConsolidatedBarCLCustNum: TStringField;
    cdConsolidatedBarCoCustNum: TStringField;
    cdConsolidatedBarCONAME: TStringField;
    cdConsolidatedBarCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField;
    cdConsolidatedBarCL_NBR: TIntegerField;
    cdConsolidatedBarCO_NBR: TIntegerField;
    cdConsolidatedBarSB_BANK_ACCOUNTS_NBR: TIntegerField;
    cdConsolidatedBarPR_CHECK_NBR: TIntegerField;
    cdConsolidatedBarCHECK_SERIAL_NUMBER: TIntegerField;
    cdConsolidatedBarCHECK_DATE: TDateField;
    cdConsolidatedBarSTATUS: TStringField;
    cdConsolidatedBarSTATUS_DATE: TDateField;
    cdConsolidatedBarTRANSACTION_EFFECTIVE_DATE: TDateField;
    cdConsolidatedBarAMOUNT: TFloatField;
    cdConsolidatedBarCLEARED_AMOUNT: TFloatField;
    cdConsolidatedBarREGISTER_type: TStringField;
    cdConsolidatedBarMANUAL_TYPE: TStringField;
    cdConsolidatedBarCO_MANUAL_ACH_NBR: TIntegerField;
    cdConsolidatedBarCO_TAX_PAYMENT_ACH_NBR: TIntegerField;
    cdConsolidatedBarCO_PR_ACH_NBR: TIntegerField;
    cdConsolidatedBarPROCESS_DATE: TDateField;
    cdConsolidatedBarCLEARED_DATE: TDateField;
    cdConsolidatedBarPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField;
    conTabManualType: TevDBComboBox;
    conTabStatus: TevDBComboBox;
    conTabRegistryType: TevDBComboBox;
    cdConsolidatedBarCO_TAX_DEPOSITS_NBR: TIntegerField;
    cdConsolidatedBarGROUP_IDENTIFIER: TStringField;
    cdGroupIdentifierTotalsCO_NBR: TIntegerField;
    cdClientBarList: TevClientDataSet;
    cdClientBarListCL_NBR: TIntegerField;
    cdClientBarListCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField;
    cdClientBarListCHANGED: TIntegerField;
    cdClientBarListCLEARED_DATE: TDateTimeField;
    cdClientBarListROLLBACK_CLEAR_DATE: TDateTimeField;
    cdClientBarListROLLBACK_RECORD: TStringField;
    cbShowProcessedRecords: TevCheckBox;
    tshtProc: TTabSheet;
    pnlProc: TPanel;
    evgrdProcessed: TevDBGrid;
    dsProcessed: TevDataSource;
    cdProcessed: TevClientDataSet;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    IntegerField3: TIntegerField;
    DateField1: TDateField;
    StringField7: TStringField;
    DateField2: TDateField;
    DateField3: TDateField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    DateField4: TDateField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    DateField5: TDateField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    IntegerField9: TIntegerField;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    IntegerField12: TIntegerField;
    IntegerField13: TIntegerField;
    procTabManualType: TevDBComboBox;
    procTabStatus: TevDBComboBox;
    procTabRegistryType: TevDBComboBox;
    cdProcessedBarList: TevClientDataSet;
    cdProcessedBarCL_NBR: TIntegerField;
    cdProcessedBarCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField;
    cdClientBarListNEWRECORD: TStringField;
    btnProcMarkCleared: TevBitBtn;
    btnProcMarkUncleared: TevBitBtn;
    cbGridUpdates: TevCheckBox;
    lTMP_CO_BANK_ACCOUNT_REGISTERRecordHide: TStringField;
    evGroupBox1: TevGroupBox;
    evLabel1: TevLabel;
    evrbCompCode: TevRadioButton;
    evrbCompName: TevRadioButton;
    edCompCodeLength: TevSpinEdit;
    lTMP_CO_BANK_ACCOUNT_REGISTERCLNAME: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERNOTES_POPULATED: TStringField;
    lTMP_CO_BANK_ACCOUNT_REGISTERNotesUpdated: TStringField;
    procedure tsht2CompFilterGroupRbClick(Sender: TObject);
    procedure tsht2DateFilterGroupRbClick(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure tsht3DbLookupClientChange(Sender: TObject);
    procedure tsht3DbLookupCompanyChange(Sender: TObject);
    procedure btnTwImportGoClick(Sender: TObject);
    procedure spbBankMergeSourceFileClick(Sender: TObject);
    procedure spbBankMergeReportDirClick(Sender: TObject);
    procedure btnTwImportViewClick(Sender: TObject);
    procedure spbBankMergeStorageDirClick(Sender: TObject);
    procedure ckboxBankMergeFirstClick(Sender: TObject);
    procedure btnStorageScanClick(Sender: TObject);
    procedure btnStorageReportClick(Sender: TObject);
    procedure cboStorageReportTransferTypeChange(Sender: TObject);
    procedure wwdgTeamRowChanged(Sender: TObject);
    procedure tsht5ReportRunBtnClick(Sender: TObject);
    procedure tsht5RadioSortByClick(Sender: TObject);
    procedure tsht3wwDBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure tsht3wwDBGridFieldChanged(Sender: TObject; Field: TField);
    procedure pctlEDIT_SB_BANK_ACCOUNTSChange(Sender: TObject);
    procedure pctlEDIT_SB_BANK_ACCOUNTSChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnMarkClearedClick(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure lTMP_CO_BANK_ACCOUNT_REGISTERBeforePost(DataSet: TDataSet);
    procedure lTMP_CO_BANK_ACCOUNT_REGISTERAfterInsert(DataSet: TDataSet);
    procedure tsht2DateFilterGroupDateTillChange(Sender: TObject);
    procedure rgSourceFileTypeClick(Sender: TObject);
    procedure lTMP_CO_BANK_ACCOUNT_REGISTERCalcFields(DataSet: TDataSet);
    procedure evRadioGroup1Click(Sender: TObject);
    procedure cbCompPosOrClick(Sender: TObject);
    procedure tsht3wwDBGridDblClick(Sender: TObject);
    procedure bOpenRegisterClick(Sender: TObject);
    procedure btnNotesClick(Sender: TObject);
    procedure tsht3wwDBGridMultiSelectRecord(Grid: TwwDBGrid;
      Selecting: Boolean; var Accept: Boolean);
    procedure tsht3wwDBGridRowChanged(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure CreditHoldBtnClick(Sender: TObject);
    procedure evRelatedTransactionClick(Sender: TObject);
    procedure tshtConExit(Sender: TObject);
    procedure btnProcMarkClearedClick(Sender: TObject);
    procedure tsht3wwDBGridFilterChange(Sender: TObject; DataSet: TDataSet;
      NewFilter: String);
    procedure evrbCompCodeClick(Sender: TObject);
    procedure evrbCompNameClick(Sender: TObject);
    procedure BarDataSetPost(DataSet: TDataSet);    
  private
    { Private declarations }
    sFilter, sFilterWithPre, sCurrentFilter, sCompanyFilter : string;
    bRefResh : boolean;
    FMyApplyUpdatesReconcileList: TStrings;
    cdConsolidatedCoList  : TevClientDataSet;
    procedure UpdateFilter(const ForceUpdate: Boolean = False);
    procedure MyApplyUpdates;
    procedure OnMyApplyUpdatesReconcile(const Field: TField; const OldValue, NewValue: Integer);
    procedure EnableTmpAchLookups;
    procedure DisableTmpAchLookups;
    procedure UnselectBARGrid;
    procedure ClearConsolidatedCompany(ds : TevClientDataSet);
    procedure SetcdConsolidatedCoList;
    function  CreateFilterfromDataset(cd: TevClientDataSet; const fName , rName : String):string;
    procedure SetDefaultDatasets;
    procedure GetcdProcessed;
    procedure UpdatecdClientBarList(const Values: array of const);
    procedure UpdatecdClientBarListRecord(const Field : String);

    procedure ProcessBarRecords(ds : TevClientDataSet);
    function  GetCoBankAccRegDetails(const Filter : String) : Boolean;
    procedure getcdConsolidatedBar(const filter : string; const filtertype : integer);
    procedure ReplaceInPosition(var s: string; const news: string; const iPos, iLen: Word);
  protected
    procedure CoordinateWithMaster;
    procedure OnGetSecurityStates(const States: TSecurityStateList); override;
  public
    { Public declarations }
    procedure AfterClick(Kind: Integer); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure BankFileMerge(const BF: TBankMergeFormat; const AccNbr: Integer; const AccName, BankFileName, ReportFileName, StorageFileName: string; const FirstTime:
      Boolean; const BankFileIsDebitFile: Boolean; const CompNumberStartingPos, codeTotalCharacter: Integer);

    procedure Activate; override;
    procedure DeActivate; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
  end;

var
  EDIT_SB_BANK_ACCOUNT_REGISTER: TEDIT_SB_BANK_ACCOUNT_REGISTER;

implementation

uses ShellApi, EvConsts, SFieldCodeValues, Graphics, Forms, SPackageEntry,
     EvCommonInterfaces, ISBasicUtils;


{$R *.DFM}

const
  ctEnabledSRo = 'S';


procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht2CompFilterGroupRbClick(
  Sender: TObject);
begin
  inherited;
  tsht2CompFilterGroupGrid.Enabled := tsht2CompFilterGroupRbSelect.Checked;
  if tsht2CompFilterGroupGrid.Enabled then
    tsht2CompFilterGroupGrid.Color := clWindow
  else
    tsht2CompFilterGroupGrid.Color := clBtnFace;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht2DateFilterGroupRbClick(
  Sender: TObject);
begin
  inherited;
  tsht2DateFilterGroupDateFrom.Enabled := tsht2DateFilterGroupRbPeriod.Checked;
  tsht2DateFilterGroupDateTill.Enabled := tsht2DateFilterGroupDateFrom.Enabled;
  gbDateFilterOptions.Enabled := tsht2DateFilterGroupRbPeriod.Checked;
  gbClearedFilterOptons.Enabled := tsht2DateFilterGroupRbPeriod.Checked;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.UpdateFilter(const ForceUpdate: Boolean = False);
var
  i: Integer;
  pBookMark: TBookmark;
  s: string;
  ds: TDataSet;
  sdf, sdt: ShortString;
begin
  if (not tsht2DateFilterGroupRbAll.Checked and not tsht2DateFilterGroupRbPeriod.Checked)
    or (not tsht2CompFilterGroupRbAll.Checked and not tsht2CompFilterGroupRbSelect.Checked)
    or not Assigned(wwdsMaster.DataSet)
    or ((pctlEDIT_SB_BANK_ACCOUNTS.ActivePage <> tsht3) and not ForceUpdate) then
  begin
    sFilter := '-';
    sCompanyFilter := '';
    sFilterWithPre :='';
  end
  else
  begin
    sFilter := '(##SB_BANK_ACCOUNTS_NBR=' + wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString + ')';
    if tsht2DateFilterGroupRbPeriod.Checked then
    begin
      sdf := ''''+ DateToStr(tsht2DateFilterGroupDateFrom.Date)+ '''';
      sdt := ''''+ DateToStr(tsht2DateFilterGroupDateTill.Date)+ '''';
      case gbDateFilterOptions.ItemIndex of
      0: sFilter := sFilter+ ' and ##TRANSACTION_EFFECTIVE_DATE between '+ sdf+ ' and '+ sdt;
      1: sFilter := sFilter+ ' and ##CHECK_DATE between '+ sdf+ ' and '+ sdt;
      2: sFilter := sFilter+ ' and ##CLEARED_DATE between '+ sdf+ ' and '+ sdt;
      3: sFilter := sFilter+ ' and ((##status = '''+ BANK_REGISTER_STATUS_Outstanding+ ''' and ##CHECK_DATE between '+ sdf+ ' and '+ sdt+ ')'+
        ' or (##status = '''+ BANK_REGISTER_STATUS_Cleared+ ''' and ##CLEARED_DATE between '+ sdf+ ' and '+ sdt+ '))';
      end;
      case gbClearedFilterOptons.ItemIndex of
      0: ;
      1: sFilter := sFilter+ ' and ##CLEARED_DATE is not null';
      2: sFilter := sFilter+ ' and ##CLEARED_DATE is null';
      3: sFilter := sFilter+ ' and ##STATUS = ''' + BANK_REGISTER_STATUS_Pending + '''';
      4: sFilter := sFilter+ ' and ##STATUS = ''' + BANK_REGISTER_STATUS_WriteOff + '''';
      end;
    end;
    if tsht2CompFilterGroupRbSelect.Checked then
    begin
      ds := tsht2CompFilterGroupGrid.DataSource.DataSet;
      if tsht2CompFilterGroupGrid.SelectedList.Count = 0 then
        s := '(##co_nbr = ' + IntToStr(ds['co_nbr']) + ' and ##cl_nbr = ' + IntToStr(ds['cl_nbr']) + ')'
      else
      begin
        ds.DisableControls;
        pBookMark := ds.GetBookmark;
        try
          s := '';
          for i := 0 to tsht2CompFilterGroupGrid.SelectedList.Count - 1 do
          begin
            ds.GotoBookmark(tsht2CompFilterGroupGrid.SelectedList.Items[i]);
            if s <> '' then s := s + ' or ';
            s := s + '(##co_nbr = ' + IntToStr(ds['co_nbr']) + ' and ##cl_nbr = ' + IntToStr(ds['cl_nbr']) + ')';
          end;
        finally
          ds.GotoBookmark(pBookMark);
          ds.FreeBookmark(pBookMark);
          ds.EnableControls;
        end;
      end;
      sFilter := sFilter + ' and (' + s + ')';
      sCompanyFilter := ' and (' + s + ')';
    end;
    s := sFilter;
    sCompanyFilter := StringReplace(sCompanyFilter, '##', '', [rfReplaceAll]);
    sFilter := StringReplace(s, '##', '', [rfReplaceAll]);
    sFilterWithPre := StringReplace(s, '##', 'a.', [rfReplaceAll]);
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.SetDefaultDatasets;
begin
  if not cdClientBarList.Active then
    cdClientBarList.CreateDataSet
  else
    cdClientBarList.EmptyDataSet;

  if not cdProcessedBarList.Active then
    cdProcessedBarList.CreateDataSet
  else
    cdProcessedBarList.EmptyDataSet;

  pnlProc.Visible := False;
  tshtProc.TabVisible := False;
end;


procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.CoordinateWithMaster;
var
  i: Integer;
  sDepFilter, s : String;
  sIndexName, sIndexFields, sDescription: String;
  SortIndexOptions: TIndexOptions;
  OldRecNo: integer;

begin
  UpdateFilter;
  if Assigned(wwdsDetail.DataSet) and (sFilter <> '-') and
      (not wwdsDetail.DataSet.Active or (sFilter <> sCurrentFilter) or bRefResh) then
  begin
      if (wwdsDetail.DataSet as TevClientDataset).IndexName = 'SORT' then
      begin
        i := (wwdsDetail.DataSet as TevClientDataset).IndexDefs.IndexOf('SORT');
        if i <> -1 then
        begin
          sIndexName := 'SORT';
          sIndexFields := (wwdsDetail.DataSet as TevClientDataset).IndexDefs[i].Fields;
          sDescription := (wwdsDetail.DataSet as TevClientDataset).IndexDefs[i].DescFields;
          SortIndexOptions := (wwdsDetail.DataSet as TevClientDataset).IndexDefs[i].Options;
        end;
      end
      else
        sIndexName := '';
      OldRecNo := wwdsDetail.DataSet.RecNo;

      ctx_StartWait;
      try

        if sFilter <> sCurrentFilter then
        begin
            sDepFilter := 'TRACE_NUMBER is not null ' + sCompanyFilter;
            DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.DataRequired(sDepFilter);

            s := 'select GROUP_IDENTIFIER, CL_NBR, CO_NBR, sum(AMOUNT) Amount, count(GROUP_IDENTIFIER) GroupIdentifierCount ' +
                   ' from TMP_CO_BANK_ACCOUNT_REGISTER ' +
                     ' where AMOUNT <> 0 and GROUP_IDENTIFIER is not null and GROUP_IDENTIFIER <> '''' and ' +
                       sFilter +
                     ' group by CL_NBR, CO_NBR, GROUP_IDENTIFIER ';

            cdGroupIdentifierTotals.Close;
            with TExecDSWrapper.Create(s) do
            begin
                ctx_DataAccess.GetCustomData(cdGroupIdentifierTotals, AsVariant);
            end;

            GetCoBankAccRegDetails('1=1');
        end;

        wwdsDetail.DataSet.Close;
        Assert(wwdsDetail.DataSet is TEvClientDataSet);
        TEvClientDataSet(wwdsDetail.DataSet).CustomAfterPost := BarDataSetPost;        
        TEvClientDataSet(wwdsDetail.DataSet).ProviderName := DM_TEMPORARY.TMP_CO_BANK_ACCOUNT_REGISTER.ProviderName;
        TEvClientDataSet(wwdsDetail.DataSet).DataRequired(sFilter + GetReadOnlyClintCompanyListFilter);

        lTMP_CO_BANK_ACCOUNT_REGISTER.UserFilter := '';
        if cbGridUpdates.Checked then
          lTMP_CO_BANK_ACCOUNT_REGISTER.UserFilter := ' RecordHide = ''N'' ';
        lTMP_CO_BANK_ACCOUNT_REGISTER.UserFiltered := cbGridUpdates.Checked;

        if not bRefResh then
        begin
          SetDefaultDatasets;

          for i := 0 to wwdsDetail.DataSet.FieldCount-1 do
            wwdsDetail.DataSet.Fields[i].Required := False;
          sCurrentFilter := sFilter;
          wwdsDetail.DataSet.First;
        end;
      finally
        ctx_EndWait;

        if sIndexName = 'SORT' then
        begin
          (wwdsDetail.DataSet as TevClientDataset).IndexDefs.Update;
          i := (wwdsDetail.DataSet as TevClientDataset).IndexDefs.IndexOf('SORT');
          if i <> -1 then
          begin
            (wwdsDetail.DataSet as TevClientDataset).DeleteIndex('SORT');
            (wwdsDetail.DataSet as TevClientDataset).IndexDefs.Update;
          end;
          (wwdsDetail.DataSet as TevClientDataset).AddIndex(sIndexname, sIndexFields, SortIndexOptions, sDescription);
          (wwdsDetail.DataSet as TevClientDataset).IndexDefs.Update;
          (wwdsDetail.DataSet as TevClientDataset).IndexName := 'SORT';
        end;
        if OldRecno <= wwdsDetail.DataSet.RecordCount then
         wwdsDetail.DataSet.RecNo := OldRecNo
        else
          wwdsDetail.DataSet.Last;

        bRefResh := false;
      end;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.wwdsDataChange(Sender: TObject;
  Field: TField);
var
  i: Integer;
begin
  inherited;
  for i := 0 to tsht3wwDBGrid.FieldCount - 1 do
    if (tsht3wwDBGrid.Fields[i].FieldName <> 'STATUS')
      and (tsht3wwDBGrid.Fields[i].FieldName <> 'TRANSACTION_EFFECTIVE_DATE')
      and (tsht3wwDBGrid.Fields[i].FieldName <> 'CLEARED_DATE')
      and (tsht3wwDBGrid.Fields[i].FieldName <> 'CLEARED_AMOUNT') then
      tsht3wwDBGrid.Fields[i].ReadOnly := not (wwdsDetail.State = dsInsert);
  tsht3DbLookupClient.ReadOnly := not (wwdsDetail.State = dsInsert);
  tsht3DbLookupCompany.ReadOnly := not (wwdsDetail.State = dsInsert);
  tsht3DbLookupManualACH.ReadOnly := not (wwdsDetail.State = dsInsert);
  tsht3DbLookupPrACH.ReadOnly := not (wwdsDetail.State = dsInsert);
  tsht3DbLookupTaxACH.ReadOnly := not (wwdsDetail.State = dsInsert);

  tsht3DbLookupManualACH.Enabled := (wwdsDetail.State = dsInsert);
  tsht3DbLookupPrACH.Enabled := (wwdsDetail.State = dsInsert);
  tsht3DbLookupTaxACH.Enabled := (wwdsDetail.State = dsInsert);

end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht3DbLookupClientChange(
  Sender: TObject);
begin
  inherited;
  if (tsht3DbLookupClient.LookupValue <> '') {and (wwdsDetail.State = dsInsert)} then
  begin
    if wwdsDetail.State in [dsInsert, dsEdit] then
      tsht3DbLookupClient.PerformSearch;
    wwClientDataSetCo.Filter := 'CL_NBR = ' + tsht3DbLookupClient.LookupValue;
    wwClientDataSetCo.Filtered := True;
    if wwClientDataSetCo.Locate('CO_NBR', wwdsDetail.DataSet['CO_NBR'], [])
    or (wwdsDetail.State in [dsInsert, dsEdit]) then
      tsht3DbLookupCompany.Text := wwClientDataSetCo.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
    if wwdsDetail.State in [dsInsert, dsEdit] then
    begin
      if wwdsDetail.State in [dsInsert] then
        tsht3DbLookupCompany.PerformSearch;
      EnableTmpAchLookups;
    end;
  end
  else
  begin
    wwClientDataSetCo.Filtered := False;
    DisableTmpAchLookups;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht3DbLookupCompanyChange(
  Sender: TObject);

begin
  inherited;
  if (tsht3DbLookupCompany.LookupValue <> '') and (wwdsDetail.State in [dsInsert, dsEdit]) then
    EnableTmpAchLookups
  else
    DisableTmpAchLookups;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.btnTwImportGoClick(
  Sender: TObject);
var
  FBF: TBankMergeFormat;
  s: string;
  i, codeTotalCharacter : Integer;
begin
  inherited;
  Assert(DM_SERVICE_BUREAU.SB_BANKS.Locate('SB_BANKS_NBR', wwdsMaster.DataSet['SB_BANKS_NBR'], []));
  s := ExtractFromFiller(DM_SERVICE_BUREAU.SB_BANKS.FieldByName('FILLER').AsString, 1, 1);
  if s = '1' then
    FBF := bfKeyBank2002
  else
    FBF := bfGeneric2000;
  SaveComponentState(editBankMergeSourceFile, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
  SaveComponentState(editBankMergeReportFile, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
  SaveComponentState(editBankMergeStorageFile, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
  SaveComponentState(cbCompPosOr, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
  SaveComponentState(edCompPosOr, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
  SaveComponentState(evrbCompCode, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
  SaveComponentState(evrbCompName, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
  SaveComponentState(edCompCodeLength, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);

  editStorageReportDateFrom.Enabled := False;
  editStorageReportDateTill.Enabled := False;
  cboStorageReportTransferType.Enabled := False;
  btnStorageReport.Enabled := False;
  btnStorageReportPreview.Enabled := False;
  sCurrentFilter := '-';
  pProcessBar.Visible := True;
  if cbCompPosOr.Checked then
    i := edCompPosOr.Value
  else
    i := 20;

  codeTotalCharacter := edCompCodeLength.Value;
  if evrbCompName.Checked and (edCompCodeLength.Value > 15) then
     codeTotalCharacter := 15;

  SetDefaultDatasets;

  ctx_StartWait;
  try
    BankFileMerge(FBF, wwdsMaster.DataSet['SB_BANK_ACCOUNTS_NBR'], tsht4BankName.Caption, editBankMergeSourceFile.Text,
      editBankMergeReportFile.Text, editBankMergeStorageFile.Text, ckboxBankMergeFirst.Checked,
      rgSourceFileType.ItemIndex = 0, i, codeTotalCharacter);
  finally
    pProcessBar.Visible := False;
    ctx_EndWait;

    if cbShowProcessedRecords.Checked then
    begin
       pnlProc.Visible := false;
       tshtProc.TabVisible := true;
    end
    else
      SetDefaultDatasets;
  end;
  cboStorageReportTransferType.ItemIndex := -1;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.spbBankMergeSourceFileClick(
  Sender: TObject);
begin
  inherited;
  dlgBankMergeSourceFile.FileName := editBankMergeSourceFile.Text;
  if dlgBankMergeSourceFile.Execute then
    editBankMergeSourceFile.Text := dlgBankMergeSourceFile.FileName;
end;


procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.ClearConsolidatedCompany(ds : TevClientDataSet);
  Procedure Process(Changed : String;Status : char);
  var
    s : String;
    pList: TStringList;
  begin
      ds.Filter :='CHANGED = ' + Changed;
      ds.Filtered := True;
      if ds.RecordCount > 0 then
      begin
        pList := ds.GetFieldValueList('CO_TAX_DEPOSITS_NBR',True);
        try
          s := pList.CommaText;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('CO_TAX_DEPOSITS_NBR in ('+ s + ')');
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
          while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Eof do
          begin
            ds.Locate('CO_TAX_DEPOSITS_NBR',DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.fieldByName('CO_TAX_DEPOSITS_NBR').AsInteger,[]);

            cdProcessedBarList.AppendRecord([ctx_DataAccess.ClientID, DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger]);

            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['status'] := Status;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['status_date'] := Date;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['CLEARED_DATE'] := ds['CLEARED_DATE'];
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
          end;
        finally
          pList.Free;
        end;
      end;
  end;
begin
  Process('1', BANK_REGISTER_STATUS_Cleared);
  Process('2', BANK_REGISTER_STATUS_Dead);
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.SetcdConsolidatedCoList;
begin
    cdConsolidatedCoList.FieldDefs.Add('CO_TAX_DEPOSITS_NBR', ftInteger, 0, True);
    cdConsolidatedCoList.FieldDefs.Add('CHANGED', ftInteger, 0, True);
    cdConsolidatedCoList.FieldDefs.Add('CLEARED_DATE', ftDateTime, 0, false);
    cdConsolidatedCoList.CreateDataSet;
    cdConsolidatedCoList.LogChanges := False;
    cdConsolidatedCoList.AddIndex('idxDepNbr', 'CO_TAX_DEPOSITS_NBR', []);
    cdConsolidatedCoList.IndexDefs.Update;
    cdConsolidatedCoList.IndexName := 'idxDepNbr';
end;

function TEDIT_SB_BANK_ACCOUNT_REGISTER.GetCoBankAccRegDetails(const Filter : String):Boolean;
begin
    Result := false;
    if DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger > 0 then
    begin
      ctx_DataAccess.OpenClient(DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger);

      cdCoBankAccRegDetails.ProviderName := DM_COMPANY.CO_BANK_ACC_REG_DETAILS.ProviderName;
      cdCoBankAccRegDetails.Close;
      with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
      begin
        SetMacro('TABLENAME', 'CO_BANK_ACC_REG_DETAILS');
        SetMacro('COLUMNS', 'CO_BANK_ACCOUNT_REGISTER_NBR, CL_NBR, CLIENT_CO_BANK_ACC_REG_NBR ');
        SetMacro('CONDITION', Filter);
        ctx_DataAccess.GetCustomData(cdCoBankAccRegDetails, AsVariant);
      end;
      Result := True;
    end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.ProcessBarRecords(ds : TevClientDataSet);
var
  i: Integer;
  sCondition: string;
  sConMiscCheck, sConACH : string;

  ConDepNbr : Integer;

  sBookMarkStr: TBookmarkStr;
  Nbr: Integer;

  procedure PostUpdatedRecords;
  begin
      if ctx_DataAccess.ClientID <> 0 then
      begin
        with DM_PAYROLL.PR do
        begin
          First;
          while not Eof do
          begin
            ctx_DataAccess.UnlockPR;
            Next;
          end;
        end;

        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER, DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_MISCELLANEOUS_CHECKS]);
        if cdConsolidatedCoList.RecordCount > 0 then
        begin
           ClearConsolidatedCompany(cdConsolidatedCoList);
           ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
        end;

        with DM_PAYROLL.PR do
        begin
          First;
          while not Eof do
          begin
            ctx_DataAccess.LockPR(True);
            Next;
          end;
        end;

        ctx_DataAccess.UnlockPRSimple;
      end;
  end;

begin
    cdConsolidatedCoList := TevClientDataSet.Create(nil);
    try
      SetcdConsolidatedCoList;

      ctx_DataAccess.OpenClient(0);
      Nbr := -1;
      ds.First;
      while not ds.Eof do
      begin
        if (ds['CL_NBR'] <> ctx_DataAccess.ClientID)
           or (ds['CO_BANK_ACCOUNT_REGISTER_NBR'] > Nbr) then
        begin
          PostUpdatedRecords;

          sBookMarkStr := ds.Bookmark;
          ctx_DataAccess.OpenClient(ds['CL_NBR']);
          sCondition := '-1';
          cdConsolidatedCoList.Filter := '';
          cdConsolidatedCoList.Filtered := false;
          cdConsolidatedCoList.EmptyDataSet;
          i := 0;
          while (ds['CL_NBR'] = ctx_DataAccess.ClientID)
          and not ds.Eof
          and (i < 500) do
          begin
            Nbr := ds['CO_BANK_ACCOUNT_REGISTER_NBR'];
            sCondition := sCondition+ ','+ IntToStr(Nbr);
            Inc(i);
            ds.Next;
          end;

          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('CO_BANK_ACCOUNT_REGISTER_NBR in ('+ sCondition+ ')');

          sCondition := '-1';
          sConMiscCheck := '-1';
          sConACH := '-1';
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.First;
          while not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Eof do
          begin
            if not VarIsNull(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldValues['PR_CHECK_NBR']) then
              sCondition := sCondition+ ','+ IntToStr(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldValues['PR_CHECK_NBR']);

            if not VarIsNull(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldValues['PR_MISCELLANEOUS_CHECKS_NBR']) then
              sConMiscCheck := sConMiscCheck+ ','+ IntToStr(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldValues['PR_MISCELLANEOUS_CHECKS_NBR']);

            if not VarIsNull(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldValues['CO_TAX_PAYMENT_ACH_NBR']) then
              sConACH := sConACH+ ','+ IntToStr(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldValues['CO_TAX_PAYMENT_ACH_NBR']);

            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Next;
          end;

          DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR in ('+ sCondition+ ')');
          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR in ('+ sConMiscCheck+ ')');
          DM_PAYROLL.CO_TAX_PAYMENT_ACH.DataRequired('CO_TAX_PAYMENT_ACH_NBR in ('+ sConACH+ ')');

          sCondition := '-1';
          with DM_PAYROLL.PR_CHECK do
          begin
            First;
            while not Eof do
            begin
              sCondition := sCondition+ ','+ IntToStr(FieldValues['PR_NBR']);
              Next;
            end
          end;
          with DM_PAYROLL.PR_MISCELLANEOUS_CHECKS do
          begin
            First;
            while not Eof do
            begin
              sCondition := sCondition+ ','+ IntToStr(FieldValues['PR_NBR']);
              Next;
            end
          end;
          DM_PAYROLL.PR.DataRequired('PR_NBR in ('+ sCondition+ ')');
          ds.Bookmark := sBookMarkStr;
        end;

        if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Locate('CO_BANK_ACCOUNT_REGISTER_NBR', ds['CO_BANK_ACCOUNT_REGISTER_NBR'], []) then
        begin
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;

          ConDepNbr := -1;
          if not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').IsNull then
          begin
            if DM_COMPANY.PR_MISCELLANEOUS_CHECKS.Locate('PR_MISCELLANEOUS_CHECKS_NBR',
                DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsInteger, []) then
              if not DM_COMPANY.PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').isNull then
                  ConDepNbr := DM_COMPANY.PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
          end
          else
          if not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_PAYMENT_ACH_NBR').IsNull then
          begin
            if DM_COMPANY.CO_TAX_PAYMENT_ACH.Locate('CO_TAX_PAYMENT_ACH_NBR',
                DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_PAYMENT_ACH_NBR').AsInteger, []) then
              if not DM_COMPANY.CO_TAX_PAYMENT_ACH.FieldByName('CO_TAX_DEPOSITS_NBR').isNull then
                  ConDepNbr := DM_COMPANY.CO_TAX_PAYMENT_ACH.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
          end
          else
            if not DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
              ConDepNbr := DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;

          cdProcessedBarList.AppendRecord([ctx_DataAccess.ClientID, DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger]);

          if ds['CHANGED'] = 1 then
           DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['status'] := BANK_REGISTER_STATUS_Cleared
          else
          if ds['CHANGED'] = 2 then
           DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['status'] := BANK_REGISTER_STATUS_Dead
          else
           DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['status'] := BANK_REGISTER_STATUS_Outstanding;

          if (ConDepNbr <> -1) and (not cdConsolidatedCoList.Locate('CO_TAX_DEPOSITS_NBR', ConDepNbr, [])) then
              cdConsolidatedCoList.AppendRecord([ConDepNbr,ds['CHANGED'], ds['CLEARED_DATE']]);

          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['status_date'] := Date;
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['CLEARED_DATE'] := ds['CLEARED_DATE'];
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;

          if DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['PR_CHECK_NBR'], []) then
          begin
              DM_PAYROLL.PR_CHECK.Edit;
              if ds['CHANGED'] = 1 then
                DM_PAYROLL.PR_CHECK['CHECK_STATUS'] := CHECK_STATUS_CLEARED
              else
              if ds['CHANGED'] = 2 then
                DM_PAYROLL.PR_CHECK['CHECK_STATUS'] := CHECK_STATUS_DEAD
              else
                DM_PAYROLL.PR_CHECK['CHECK_STATUS'] := CHECK_STATUS_OUTSTANDING;

              DM_PAYROLL.PR_CHECK.Post
          end;
          if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Locate('PR_MISCELLANEOUS_CHECKS_NBR', DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['PR_MISCELLANEOUS_CHECKS_NBR'], []) then
          begin
              DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Edit;
              if ds['CHANGED'] = 1 then
                DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['CHECK_STATUS'] := CHECK_STATUS_CLEARED
              else
              if ds['CHANGED'] = 2 then
                DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['CHECK_STATUS'] := CHECK_STATUS_DEAD
              else
                DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['CHECK_STATUS'] := CHECK_STATUS_OUTSTANDING;

              DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Post
          end;
        end
        else
          raise EMergeException.CreateHelp('Data mismatch between temporary tables and client '+ IntToStr(ctx_DataAccess.ClientID), IDH_MergeException);
        ds.Next;
      end;
      PostUpdatedRecords;

    finally
     cdConsolidatedCoList.Free;
    end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.BankFileMerge(const BF: TBankMergeFormat; const AccNbr: Integer; const AccName, BankFileName, ReportFileName, StorageFileName:
  string; const FirstTime: Boolean; const BankFileIsDebitFile: Boolean; const CompNumberStartingPos, codeTotalCharacter: Integer);

  function DateFromStr(const s: string): TDate;
  var
    m, d, y: Word;
  begin
    y := StrToInt(Copy(s, 1, 4));
    m := StrToInt(Copy(s, 5, 2));
    d := StrToInt(Copy(s, 7, 2));
    Result := EncodeDate(y, m, d);
  end;

  function ReadField(Data: string; FieldNbr: Integer): string;
  begin
    while FieldNbr > 1 do
    begin
      Fetch(Data, ',');
      Dec(FieldNbr);
    end;
    Result := Fetch(Data, ',');
    while (Result <> '') do
      case Result[1] of
      '"':
        begin
          Delete(Result, 1, 1);
          Break;
        end;
      ' ':
        Delete(Result, 1, 1);
      else
        Break;
      end;
    while (Result <> '') do
      case Result[Length(Result)] of
      '"':
        begin
          Delete(Result, Length(Result), 1);
          Break;
        end;
      ' ':
        Delete(Result, Length(Result), 1);
      else
        Break;
      end;
  end;

  function PadLeft(S: string; Count: Integer; Pad: Char): string;
  var
    Buf: string;
  begin
    Buf := S;
    while Length(Buf) < Count do
    begin
      Buf := Pad + Buf;
    end;
    PadLeft := Buf;
  end;

var
  {bf,} rf, sf: TextFile;
  sData, sSubData: string;
  sTransType: string[3];
  vCheckNumber: Variant;
  sCompNumber: string[40];
  sEftpsOrIrs: string;
  sTraceNumber : String[15];
  sBatchId: string[10];
  cAmount: Currency;
  dClear, dAdjustedClear: TDate;
  dsExpt: TEvBasicClientDataSet;
  iDeadAmount, iEftpsCleared, iChecksCleared, iDirDepCleared, iAchCredited, iExceptionCounter, iTotalExceptionCounter: LongInt;
  cDeadAmount, cEftpsCleared, cChecksCleared, cDirDepCleared, cAchCredited, cExceptionAmount, cTotalExceptionAmout: Currency;
  iPageCounter, iLineCounter: Word;
  MyTransManager: TTransactionManager;
  lTmpRegister: TevClientDataSet;
  OneCheckClearList, OneCheckDeadList, GroupIdentifierClearList, GroupIdentifierDeadList : IisStringList;

  procedure MarkIt;
  begin
   if DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger = lTmpRegister.FieldByName('CL_NBR').AsInteger then
      OneCheckClearList.Add(lTmpRegister.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsString);
   if lTmpRegister['CHANGED'] = 9 then
      GroupIdentifierClearList.Add(lTmpRegister.FieldByName('GROUP_IDENTIFIER').AsString);
   lTmpRegister.Edit;
   lTmpRegister['CLEARED_DATE'] := dClear;
   lTmpRegister['CHANGED'] := 1;
   lTmpRegister.Post;

  end;

  procedure MarkItDead;
  begin
   if DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger = lTmpRegister.FieldByName('CL_NBR').AsInteger then
      OneCheckDeadList.Add(lTmpRegister.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsString);
   if lTmpRegister['CHANGED'] = 9 then
      GroupIdentifierDeadList.Add(lTmpRegister.FieldByName('GROUP_IDENTIFIER').AsString);
   lTmpRegister.Edit;
   lTmpRegister['CLEARED_DATE'] := dClear;
   lTmpRegister['CHANGED'] := 2;
   lTmpRegister.Post;

  end;

  procedure FindBatch;
  var
    lAmount: Currency;
  begin
    lTmpRegister.IndexName := 'DirectWithdrBatch';
    lTmpRegister.SetRange([sBatchId], [sBatchId]);
    lTmpRegister.Filter := '';
    lAmount := 0;
    if lTmpRegister.RecordCount = 0 then
      raise EMergeException.CreateHelp('Failed to find the batch', IDH_MergeException);
    lTmpRegister.First;
    while not lTmpRegister.Eof do
    begin
      lAmount := lAmount+ lTmpRegister['AMOUNT'];
      lTmpRegister.Next;
    end;
    if lAmount = cAmount then
    begin
      lTmpRegister.First;
      while not lTmpRegister.Eof do
      begin
        MarkIt;
        lTmpRegister.Next;
      end;
    end
    else
      raise EMergeException.CreateHelp('Batch total doesn''t match', IDH_MergeException);
  end;

  procedure FindDirWith;
  var
    ResultCoNumber, ResultClNumber : integer;
    sTraceFilter : String;
    tmpCL_NBR, tmpCO_NBR : integer;

    function GetCompNumber : Integer;
    var
      bCompanyMatch : Boolean;
    begin
       result := 0;
       bCompanyMatch := false;
       DM_TEMPORARY.TMP_CO.FindNearest([sCompNumber]);
       if StartsWith(DM_TEMPORARY.TMP_CO[DM_TEMPORARY.TMP_CO.IndexName], sCompNumber) then
       begin
         Inc(Result);
         if (DM_TEMPORARY.TMP_CO.IndexName = 'CUSTOM_COMPANY_NUMBER') and
            (Trim(DM_TEMPORARY.TMP_CO.FieldByName(DM_TEMPORARY.TMP_CO.IndexName).AsString) = Trim(sCompNumber)) then
            bCompanyMatch := True;

         tmpCL_NBR := DM_TEMPORARY.TMP_CO['CL_NBR'];
         tmpCO_NBR := DM_TEMPORARY.TMP_CO['CO_NBR'];
         if not bCompanyMatch then
         begin
           if not DM_TEMPORARY.TMP_CO.Eof then
           begin
             DM_TEMPORARY.TMP_CO.Next;
             if StartsWith(DM_TEMPORARY.TMP_CO[DM_TEMPORARY.TMP_CO.IndexName], sCompNumber) then
               Inc(Result);
           end;
         end;
       end;
    end;

    function GetClientNumber : Integer;
    begin
       result := 0;
       DM_TEMPORARY.TMP_CL.FindNearest([sCompNumber]);
       if StartsWith(DM_TEMPORARY.TMP_CL[DM_TEMPORARY.TMP_CL.IndexName], sCompNumber) then
       begin
         Inc(Result);
         tmpCL_NBR := DM_TEMPORARY.TMP_CL['CL_NBR'];
         if not DM_TEMPORARY.TMP_CL.Eof then
         begin
           DM_TEMPORARY.TMP_CL.Next;
           if StartsWith(DM_TEMPORARY.TMP_CL[DM_TEMPORARY.TMP_CL.IndexName], sCompNumber) then
             Inc(Result);
         end;
       end;
    end;


  begin
    if sBatchId <> '' then
      FindBatch
    else
    begin
        tmpCL_NBR := 0;
        tmpCO_NBR := 0;
        ResultClNumber := -1;
        if (DM_TEMPORARY.TMP_CO.IndexName <> 'CUSTOM_COMPANY_NUMBER' ) then
        begin
         DM_TEMPORARY.TMP_CO.IndexName := 'NAME';
         ResultCoNumber := GetCompNumber;
         if (ResultCoNumber = 0) or (ResultCoNumber <> 1) then
         begin
            DM_TEMPORARY.TMP_CO.IndexName := 'BANK_ACCOUNT_REGISTER_NAME';
            ResultCoNumber := GetCompNumber;
         end;
        end
        else
         ResultCoNumber := GetCompNumber;

        if ResultCoNumber <> 1 then
        begin
           ResultClNumber := GetClientNumber;
           if ResultClNumber = 1 then
              ResultCoNumber := ResultClNumber;
        end;

        if ResultCoNumber = 0 then
          raise EMergeException.CreateHelp('Failed to find the company', IDH_MergeException)
        else if ResultCoNumber <> 1 then
          raise EMergeException.CreateHelp('More than one company', IDH_MergeException);

         if ResultClNumber = 1 then
         begin
          lTmpRegister.IndexName := 'DirectWithdrClient';
          lTmpRegister.SetRange([tmpCL_NBR],[tmpCL_NBR]);
         end
         else
         begin
          lTmpRegister.IndexName := 'DirectWithdrComp';
          lTmpRegister.SetRange([tmpCL_NBR, tmpCO_NBR],[tmpCL_NBR, tmpCO_NBR]);
         end;
        if Length(Trim(sTraceNumber)) > 0 then
           sTraceFilter := ' and TRACE_NUMBER = ''' + Trim(sTraceNumber) + '''';
        lTmpRegister.Filter := 'AMOUNT = ' + CurrToStr(cAmount) + ' and CHECK_DATE <= ''' + DateToStr(dAdjustedClear) + '''' + sTraceFilter;
        if lTmpRegister.RecordCount = 0 then
          raise EMergeException.CreateHelp('Failed to find the check', IDH_MergeException);
        if lTmpRegister.RecordCount <> 1 then
          raise EMergeException.CreateHelp('More than one check', IDH_MergeException);
        MarkIt;
    end;
  end;

  procedure FindEFTPSDirWith;
  var
    sTraceFilter : String;
  begin
    if sBatchId <> '' then
      FindBatch
    else
      with DM_TEMPORARY do
      begin
        lTmpRegister.IndexName := 'DirectWithdrComp';
        lTmpRegister.CancelRange;
        sTraceFilter := '';
        if Length(Trim(sTraceNumber)) > 0 then
           sTraceFilter := ' and TRACE_NUMBER = ''' + Trim(sTraceNumber) + '''';
        lTmpRegister.Filter := 'AMOUNT = ' + CurrToStr(cAmount) + ' and CHECK_DATE <= ''' + DateToStr(dAdjustedClear) + '''' + sTraceFilter;
        if lTmpRegister.RecordCount = 0 then
          raise EMergeException.CreateHelp('Failed to find the EFTPS record', IDH_MergeException);
        if lTmpRegister.RecordCount <> 1 then
          raise EMergeException.CreateHelp('More than one EFTPS record', IDH_MergeException);
        MarkIt;
      end;
  end;

  function CheckSign(const c: Currency): Currency;
  begin
    if BankFileIsDebitFile then
      Result := -c
    else
      Result := c;
  end;


  procedure FindDeadStatus;
  begin

    with DM_TEMPORARY do
    begin
      lTmpRegister.CancelRange;
      lTmpRegister.Filter := 'CHECK_SERIAL_NUMBER = ' + IntToStr(vCheckNumber) + ' and CHECK_DATE <= ''' +
          DateToStr(dAdjustedClear) + '''';
      if lTmpRegister.RecordCount = 0 then
          raise EMergeException.CreateHelp('Failed to find the check', IDH_MergeException)
      else
      if (lTmpRegister.RecordCount <> 1) then
      begin
        lTmpRegister.Filter := 'AMOUNT = ' + CurrToStr(cAmount) + ' and CHECK_SERIAL_NUMBER = ' + IntToStr(vCheckNumber) +
          ' and CHECK_DATE <= ''' + DateToStr(dAdjustedClear) + '''';
        if lTmpRegister.RecordCount = 0 then
           raise EMergeException.CreateHelp('Failed to find the check', IDH_MergeException)
        else
        if lTmpRegister.RecordCount <> 1 then
          raise EMergeException.CreateHelp('More than one check. First has been cleared.', IDH_MergeException)
        else
          MarkItDead;
      end
      else
        if lTmpRegister.FieldByName('AMOUNT').AsCurrency = cAmount then
          MarkItDead
        else
          raise EMergeException.CreateHelp('Amount is different', IDH_MergeException);
    end;
  end;

  procedure FindCheck;
  begin
    with DM_TEMPORARY do
    begin
      lTmpRegister.CancelRange;
      if VarIsNull(vCheckNumber) then
        lTmpRegister.Filter := 'AMOUNT = ' + CurrToStr(cAmount) + ' and CHECK_DATE <= ''' + DateToStr(dAdjustedClear) + ''''
      else
        lTmpRegister.Filter := 'CHECK_SERIAL_NUMBER = ' + IntToStr(vCheckNumber) + ' and CHECK_DATE <= ''' +
          DateToStr(dAdjustedClear) + '''';
      if lTmpRegister.RecordCount = 0 then
      begin
        lTmpRegister.CancelRange;
        if lTmpRegister.RecordCount = 0 then
          raise EMergeException.CreateHelp('Failed to find the check', IDH_MergeException)
        else
          raise EMergeException.CreateHelp('Wrong check status', IDH_MergeException);
      end
      else
      if (lTmpRegister.RecordCount <> 1) then
      begin
        if VarIsNull(vCheckNumber) then
          raise EMergeException.CreateHelp('More than one check. No one has been cleared.', IDH_MergeException);
        lTmpRegister.Filter := 'AMOUNT = ' + CurrToStr(cAmount) + ' and CHECK_SERIAL_NUMBER = ' + IntToStr(vCheckNumber) +
          ' and CHECK_DATE <= ''' + DateToStr(dAdjustedClear) + '''';
        if lTmpRegister.RecordCount = 0 then
        begin
          lTmpRegister.CancelRange;
          if lTmpRegister.RecordCount = 0 then
            raise EMergeException.CreateHelp('Failed to find the check', IDH_MergeException)
          else
            raise EMergeException.CreateHelp('Wrong check status', IDH_MergeException);
        end;
        if lTmpRegister.RecordCount <> 1 then
        begin
          Markit;
          raise EMergeException.CreateHelp('More than one check. First has been cleared.', IDH_MergeException);
        end;
        MarkIt;
      end
      else
        if lTmpRegister.FieldByName('AMOUNT').AsCurrency = cAmount then
        MarkIt
      else
        raise EMergeException.CreateHelp('Amount is different', IDH_MergeException);
    end;
  end;

  procedure CustWriteLn(const s: string = '');
  begin
    if iLineCounter = 0 then
    begin
      Inc(iPageCounter);
      Writeln(rf, Format('%-40s%30s', ['Check merge report for ' + AccName, DateTimeToStr(Now) + ' Page #' + IntToStr(iPageCounter)]));
      Writeln(rf);
    end;
    Writeln(rf, s);
    Inc(iLineCounter);
    if iLineCounter > 78 then
    begin
      Write(rf, #12);
      iLineCounter := 0;
    end;
  end;

  function IsSupportedType(aNumber: Integer): Boolean;
  const
    AcceptableTransTypes : array[1..9] of integer = (165, 195, 252, 301, 399, 455, 475, 575, 698);
  var
    i: Integer;
  begin
    Result := False;
    for i := Low(AcceptableTransTypes) to High(AccepTableTransTypes) do
    begin
      if aNumber = AcceptableTransTypes[i] then
      begin
        Result := True;
        Break;
      end;
    end;
  end;

  procedure PrintType(const TransType, SubType: string; var TransCount: LongInt; var TransAmount: Currency);
  begin
    if TransType = '455' then
    begin
      CustWriteLn(Format('%11s    %9s      %10s      %12s     %-30s', ['Company #', 'Batch  Id', 'ClearDate', 'Amount', 'Problem']));
      CustWriteLn(Format('%11s    %9s      %10s      %12s     %-30s', ['---------', '---------', '---------', '------', '-------']));
    end
    else
      if TransType = '475' then
    begin
      CustWriteLn(Format('%11s    %9s      %12s     %-30s', ['Check #', 'ClearDate', 'Amount', 'Problem']));
      CustWriteLn(Format('%11s    %9s      %12s     %-30s', ['-------', '---------', '------', '-------']));
    end
    else
      if TransType = '000' then
    begin
      CustWriteLn(Format('%11s    %9s      %12s     %-30s', ['Check #', 'ClearDate', 'Amount', 'Problem']));
      CustWriteLn(Format('%11s    %9s      %12s     %-30s', ['-------', '---------', '------', '-------']));
    end
    else
    begin
      CustWriteLn(Format('%11s      %10s      %12s     %-30s', ['ClearDate', 'Batch  Id', 'Amount', 'Problem']));
      CustWriteLn(Format('%11s      %10s      %12s     %-30s', ['---------', '---------', '------', '-------']));
    end;
    TransCount := 0;
    TransAmount := 0;
    dsExpt.First;
    while not dsExpt.Eof do
    begin
      if (dsExpt['type'] = TransType)
      and ((dsExpt['CompNumber'] = SubType)
        or ((SubType = '') and (dsExpt['CompNumber'] <> 'EFTP'))
        or ((SubType = 'EFTPS/IRS') and ((dsExpt['CompNumber'] = 'EFTPS') or (dsExpt['CompNumber'] = 'IRS')))) then
      begin
        if TransType = '455' then
          CustWriteLn(Format('%11s    %9s      %10s      %12m     %-30s', [dsExpt['CompNumber'], dsExpt['BatchId'], DateToStr(dsExpt['ClearDate']),
            Currency(dsExpt['Amount']), dsExpt['Message']]))
        else
          if TransType = '475' then
          CustWriteLn(Format('%11s    %9s      %12m     %-30s', [dsExpt.FieldByName('CheckNumber').AsString,
            DateToStr(dsExpt['ClearDate']), Currency(dsExpt['Amount']), dsExpt['Message']]))
        else
          if TransType = '000' then
          CustWriteLn(Format('%11s    %9s      %12m     %-30s', [dsExpt.FieldByName('CheckNumber').AsString,
            DateToStr(dsExpt['ClearDate']), Currency(dsExpt['Amount']), dsExpt['Message']]))
        else
          CustWriteLn(Format('%11s      %10s      %12m     %-30s', [DateToStr(dsExpt['ClearDate']), dsExpt['BatchId'], Currency(dsExpt['Amount']), dsExpt['Message']]));
        Inc(TransCount);
        TransAmount := TransAmount + Abs(dsExpt['Amount']);
      end
      else if TransType = 'OTHER' then
      begin
        if not IsSupportedType(dsExpt.FieldByName('type').AsInteger) then
        begin
          CustWriteLn(Format('%11s      %10s      %12m     %-30s', [DateToStr(dsExpt['ClearDate']), dsExpt['BatchId'], Currency(dsExpt['Amount']), dsExpt['Message']]));
          Inc(TransCount);
          TransAmount := TransAmount + Abs(dsExpt['Amount']);
        end;
      end;
      dsExpt.Next;
    end;
    CustWriteLn;
  end;
  function CheckEmpty(const s: ShortString): ShortString;
  begin
    if s <> '' then
      Result := s
    else
      Result := '0';
  end;
  procedure AssignedChanged(const S, D: TEvBasicClientDataSet; const FName: ShortString);
  begin
    if not VarIsEmpty(S.FieldByName(FName).NewValue) then
      D[FName] := S.FieldByName(FName).NewValue;
  end;

  procedure FindOneCheckAndGroupIdentifierBarRecords;

    Procedure ProcessOneCheck(Filter : String; Status : integer);
    var
      tmpBarNbr : integer;
    begin

      if GetCoBankAccRegDetails(Filter) then
      begin
        lTmpRegister.Filter := '';
        lTmpRegister.Filtered := false;
        tmpBarNbr := 0;
        while not cdCoBankAccRegDetails.Eof do
        begin
           if tmpBarNbr <> cdCoBankAccRegDetails.fieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger then
           begin
             dClear :=  now;
             if lTmpRegister.Locate('CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR',
                     VarArrayOf([cdCoBankAccRegDetails.fieldByName('CL_NBR').AsInteger,
                              cdCoBankAccRegDetails.fieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger]) ,[]) then
                    dClear :=  lTmpRegister['CLEARED_DATE'];
             tmpBarNbr := cdCoBankAccRegDetails.fieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger;
           end;
           if lTmpRegister.Locate('CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR',
                     VarArrayOf([cdCoBankAccRegDetails.fieldByName('CL_NBR').AsInteger,
                                 cdCoBankAccRegDetails.fieldByName('CLIENT_CO_BANK_ACC_REG_NBR').AsInteger]) ,[]) then
           begin
             lTmpRegister.Edit;
             lTmpRegister['CLEARED_DATE'] := dClear;
             lTmpRegister['CHANGED'] := Status;
             lTmpRegister.Post;
           end;
           cdCoBankAccRegDetails.Next;
        end;
      end;

    end;

    Procedure ProcessGroupIdentifier(ClearList : IisStringList;Status : integer);
    var
      i : integer;
      loop : boolean;
    begin
        lTmpRegister.Filter := 'CHANGED in ( 0 ) ';
        lTmpRegister.Filtered := True;
        for i := 0 to ClearList.Count - 1 do
        begin
           dClear :=  now;
           if lTmpRegister.Locate('GROUP_IDENTIFIER',ClearList[i] ,[]) then
               dClear :=  lTmpRegister['CLEARED_DATE'];

          loop := false;
          repeat
             if lTmpRegister.Locate('GROUP_IDENTIFIER',ClearList[i] ,[]) then
             begin
                   lTmpRegister.Edit;
                   lTmpRegister['CLEARED_DATE'] := dClear;
                   lTmpRegister['CHANGED'] := Status;
                   lTmpRegister.Post;
             end
             else
              loop := true;

          until loop;
        end;
    end;

  begin
    if OneCheckClearList.Count > 0 then
      ProcessOneCheck(BuildINsqlStatement('CO_BANK_ACCOUNT_REGISTER_NBR', OneCheckClearList.CommaText),1);
    if OneCheckDeadList.Count > 0 then
      ProcessOneCheck(BuildINsqlStatement('CO_BANK_ACCOUNT_REGISTER_NBR', OneCheckDeadList.CommaText),2);

    if GroupIdentifierClearList.Count > 0 then
      ProcessGroupIdentifier(GroupIdentifierClearList,1);
    if GroupIdentifierDeadList.Count > 0 then
      ProcessGroupIdentifier(GroupIdentifierDeadList,2);

  end;

var
  lList: TStringList;
  i: Integer;
  s: string;
begin
  lProcessBarLabel.Caption := 'Loading...';
  pbProgressBar.Position := 0;
  pProcessBar.Update;
  lList := TStringList.Create;
  ctx_DataAccess.SkipPrCheckPostCheck := True;
  try
    if BankFileName = '' then
      raise EMergeException.CreateHelp('Empty bank file name is given', IDH_MergeException);
    if ReportFileName = '' then
      raise EMergeException.CreateHelp('Empty report file name is given', IDH_MergeException);
    if StorageFileName = '' then
      raise EMergeException.CreateHelp('Empty storage file name is given', IDH_MergeException);
    lList.LoadFromFile(BankFileName);
    try
      ForceDirectories(ExtractFilePath(ReportFileName));
      AssignFile(rf, ReportFileName);
      if FirstTime or not FileExists(ReportFileName) then
        Rewrite(rf)
      else
        Append(rf);
    except
      on E: Exception do
      begin
        E.Message := 'Can''t open report file'#13 + E.Message;
        raise;
      end;
    end;
    try
      ForceDirectories(ExtractFilePath(StorageFileName));
      AssignFile(sf, StorageFileName);
      if {FirstTime or} not FileExists(StorageFileName) then
        Rewrite(sf)
      else
        Append(sf);
    except
      on E: Exception do
      begin
        E.Message := 'Can''t open report file'#13 + E.Message;
        raise;
      end;
    end;

    MyTransManager := TTransactionManager.Create(nil);
    try
      dsExpt := TEvBasicClientDataSet.Create(nil);
      try
        dsExpt.FieldDefs.Clear;
        dsExpt.FieldDefs.Add('Type', ftString, SizeOf(sTransType), True);
        dsExpt.FieldDefs.Add('CompNumber', ftString, SizeOf(sCompNumber), False);
        dsExpt.FieldDefs.Add('CheckNumber', ftInteger, 0, False);
        dsExpt.FieldDefs.Add('ClearDate', ftDate, 0, True);
        dsExpt.FieldDefs.Add('Amount', ftCurrency, 0, True);
        dsExpt.FieldDefs.Add('Message', ftString, 30, False);
        dsExpt.FieldDefs.Add('BatchId', ftString, 10, False);
        dsExpt.IndexDefs.Add('1', 'TYPE;COMPNUMBER;CHECKNUMBER', []);
        dsExpt.IndexName := '1';
        dsExpt.CreateDataSet;
        ctx_DataAccess.UnlockPRSimple;

        OneCheckClearList := TisStringList.Create;
        OneCheckDeadList  := TisStringList.Create;
        GroupIdentifierClearList := TisStringList.Create;
        GroupIdentifierDeadList := TisStringList.Create;

        lTmpRegister := TevClientDataSet.Create(nil);
        try
          UpdateFilter(True);
          if evrbCompCode.Checked then
          begin
            DM_TEMPORARY.TMP_CO.IndexDefs.Add('CUSTOM_COMPANY_NUMBER', 'CUSTOM_COMPANY_NUMBER', [ixCaseInsensitive]);
            DM_TEMPORARY.TMP_CO.IndexDefs.Update;
            DM_TEMPORARY.TMP_CO.IndexName := 'CUSTOM_COMPANY_NUMBER';

            DM_TEMPORARY.TMP_CL.IndexDefs.Add('CUSTOM_CLIENT_NUMBER', 'CUSTOM_CLIENT_NUMBER', [ixCaseInsensitive]);
            DM_TEMPORARY.TMP_CL.IndexDefs.Update;
            DM_TEMPORARY.TMP_CL.IndexName := 'CUSTOM_CLIENT_NUMBER';
          end
          else
          begin
            DM_TEMPORARY.TMP_CO.IndexDefs.Add('NAME', 'NAME', [ixCaseInsensitive]);
            DM_TEMPORARY.TMP_CO.IndexDefs.Add('BANK_ACCOUNT_REGISTER_NAME', 'BANK_ACCOUNT_REGISTER_NAME', [ixCaseInsensitive]);
            DM_TEMPORARY.TMP_CO.IndexDefs.Update;
            DM_TEMPORARY.TMP_CO.IndexName := 'NAME';

            DM_TEMPORARY.TMP_CL.IndexDefs.Add('NAME', 'NAME', [ixCaseInsensitive]);
            DM_TEMPORARY.TMP_CL.IndexDefs.Update;
            DM_TEMPORARY.TMP_CL.IndexName := 'NAME';
          end;

          lTmpRegister.ProviderName := ctx_DataAccess.TEMP_CUSTOM_VIEW.ProviderName;
          s:=
            ' select a.CO_BANK_ACCOUNT_REGISTER_NBR, a.CL_NBR, a.CO_NBR, a.CHECK_DATE, a.CHECK_SERIAL_NUMBER, a.AMOUNT, a.CHECK_DATE CLEARED_DATE, CAST(StrCopy(a.FILLER, 17, 10) as VARCHAR(10)) BATCHID, CAST(0 as INTEGER) CHANGED, a.GROUP_IDENTIFIER, b.TRACE_NUMBER ' +
               ' from TMP_CO_BANK_ACCOUNT_REGISTER a ' +
                  ' LEFT JOIN TMP_CO_TAX_DEPOSITS b ' +
                   ' ON a.CL_NBR= b.CL_NBR and a.CO_NBR = b.CO_NBR and a.CO_TAX_DEPOSITS_NBR = b.CO_TAX_DEPOSITS_NBR ' +
                    '  where  a.STATUS = :STATUS and ' + sFilterWithPre +
            ' union  ALL ' +
            ' select -1, c.CL_NBR, c.CO_NBR, c.CHECK_DATE, -1 , sum(c.AMOUNT) Amount, CAST(''1/1/1990'' as date) CLEARED_DATE, CAST('''' as VARCHAR(10)) BATCHID, CAST(''9'' as INTEGER) CHANGED, c.GROUP_IDENTIFIER, CAST(null as VARCHAR(15)) TRACE_NUMBER ' +
               ' from TMP_CO_BANK_ACCOUNT_REGISTER c ' +
                 ' where c.STATUS = :STATUS AND c.AMOUNT <> 0 and c.GROUP_IDENTIFIER is not null and GROUP_IDENTIFIER <> '''' and  ' + sFilter +
               ' group by c.CL_NBR, c.CO_NBR, c.GROUP_IDENTIFIER, c.CHECK_DATE ' +
                ' HAVING count(GROUP_IDENTIFIER) > 1 ';
          with TExecDSWrapper.Create(s) do
          begin
            SetParam('STATUS', BANK_REGISTER_STATUS_Outstanding);
            lTmpRegister.DataRequest(AsVariant);
          end;

          lTmpRegister.Open;

          lTmpRegister.IndexDefs.Add('DirectWithdrComp', 'CL_NBR;CO_NBR', []);
          lTmpRegister.IndexDefs.Add('DirectWithdrBatch', 'BATCHID', []);
          lTmpRegister.IndexDefs.Add('DirectWithdrClient', 'CL_NBR', []);
          lTmpRegister.Filter := '';
          lTmpRegister.Filtered := True;

          iChecksCleared := 0;
          iDirDepCleared := 0;
          iAchCredited := 0;
          cChecksCleared := 0;
          cDirDepCleared := 0;
          cAchCredited := 0;
          iTotalExceptionCounter := 0;
          cTotalExceptionAmout := 0;
          iEftpsCleared := 0;
          cEftpsCleared := 0;
          cDeadAmount := 0;
          iDeadAmount := 0;

          pbProgressBar.Max := lList.Count;
          lProcessBarLabel.Caption := 'Analyzing...';
          pProcessBar.Refresh;
          pProcessBar.Update;
          for i := 0 to lList.Count-1 do
          begin
            sData := lList[i];
            pbProgressBar.Position := i;
            pProcessBar.Update;
            case BF of
            bfKeyBank2002:
              begin
                dClear := StrToDate(Fetch(sData, ','));
                Fetch(sData, ',');
                Fetch(sData, ',');
                Fetch(sData, ',');
                s := Fetch(sData, ',');
                if (Length(s) = 4) and (s[1] = '0') then
                  sTransType := Copy(s, 2, 3)
                else
                  sTransType := s;
                Fetch(sData, ',');
                cAmount := CheckSign(StrToCurr(Fetch(sData, ',')));
                sSubData := Fetch(sData, ',');
                Fetch(sData, ',');
                Fetch(sData, ',');
                Fetch(sData, ',');
                Fetch(sData, ',');
                Fetch(sData, ',');
                vCheckNumber := Trim(Fetch(sData, ','));
                if vCheckNumber <> '' then
                  vCheckNumber := StrToInt(vCheckNumber)
                else
                  vCheckNumber := Null;
                sTraceNumber := Trim(Fetch(sData, ','));
              end;
            bfGeneric2000:
              begin
                sTraceNumber := ReadField(sData, 12);
                sSubData := ReadField(sData, 11);
                cAmount := CheckSign(StrToCurr(ReadField(sData, 8)));
                sTransType := ReadField(sData, 6);
                if Trim(ReadField(sData, 10)) <> '' then
                  vCheckNumber := StrToInt(Trim(ReadField(sData, 10)))
                else
                  vCheckNumber := Null;
                dClear := DateFromStr(ReadField(sData, 1));
              end
            else
              Assert(False);
            end;
            if Copy(sSubData, CompNumberStartingPos+ 5, 1) = '*' then
            begin
              sBatchId := Trim(Copy(sSubData, CompNumberStartingPos+ 6, 10));
              sCompNumber := Trim(Copy(sSubData, CompNumberStartingPos, 5));
              sEftpsOrIrs := sCompNumber;
            end
            else
            begin
              sCompNumber := Trim(Copy(sSubData, CompNumberStartingPos, 255));
              sBatchId := '';
              if Pos(' ', sCompNumber) = 0 then
                 sEftpsOrIrs := Copy(sCompNumber, 1, length(sCompNumber))
              else
                 sEftpsOrIrs := Copy(sCompNumber, 1, Pos(' ', sCompNumber) - 1);
              sCompNumber := Trim(Copy(sCompNumber, 1, codeTotalCharacter));
            end;
            dAdjustedClear := dClear + 10;
            try
              if dClear > edMergeUpToDate.DateTime then
                raise EMergeException.CreateHelp('Cleared date is out of up to date', IDH_MergeException);
              if (sTransType = '455') and (sEftpsOrIrs <> 'EFTPS') and (sEftpsOrIrs <> 'IRS') then
              begin
                FindDirWith;
                Inc(iDirDepCleared);
                cDirDepCleared := cDirDepCleared + Abs(cAmount);
              end
              else
              if (sTransType = '455') and ((sEftpsOrIrs = 'EFTPS') or (sEftpsOrIrs = 'IRS')) then
              begin
                FindEFTPSDirWith;
                Inc(iEftpsCleared);
                cEftpsCleared := cEftpsCleared + Abs(cAmount);
              end
              else
              if sTransType = '475' then
              begin
                FindCheck;
                Inc(iChecksCleared);
                cChecksCleared := cChecksCleared + Abs(cAmount);
              end
              else
              if (sTransType = '165') and
                   (sEftpsOrIrs <> 'EFTP') and
                    (sEftpsOrIrs <> 'EFTPS') and
                     (sEftpsOrIrs <> 'IRS') then
              begin
                FindDirWith;
                Inc(iAchCredited);
                cAchCredited := cAchCredited + Abs(cAmount);
              end
              else
              if (sTransType = '000') and ( not VarIsNull(vCheckNumber)) then
              begin
                FindDeadStatus;
                Inc(iDeadAmount);
                cDeadAmount := cDeadAmount + Abs(cAmount);
              end
              else
                raise EMergeException.CreateHelp('Unknown transfer type', IDH_MergeException);
            except
              on E: EMergeException do
              begin
                dsExpt.AppendRecord([sTransType, sCompNumber, vCheckNumber, dClear, cAmount, E.Message, sBatchId]);
                Writeln(sf, sTransType: 3, sCompNumber: 4, ConvertNull(vCheckNumber, ''): 10, FormatDateTime('MM/DD/YYYY', dClear): 10, cAmount: 15: 2,
                  E.Message: 40, sBatchId: 10);
                Inc(iTotalExceptionCounter);
                cTotalExceptionAmout := cTotalExceptionAmout + Abs(cAmount);
              end;
            end;
          end;

          iPageCounter := 0;
          iLineCounter := 0;

          pbProgressBar.Position := 0;
          lProcessBarLabel.Caption := 'Storing totals...';
          pProcessBar.Refresh;
          pProcessBar.Update;
          CustWriteln('EXCEPTIONS REPORT');
          CustWriteln;
          CustWriteln('Credit File Direct Deposits:');
          PrintType('165', '', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Cleared counter:    %12u    Cleared amount:    %15m', [iAchCredited, cAchCredited]));
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;
          CustWriteLn('Credit File Wire Transfers:');
          PrintType('195', '', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;
          CustWriteLn('Credit File Debit Reversals:');
          PrintType('252', '', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;
          CustWriteLn('Credit File Regular Deposits:');
          PrintType('301', '', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;
          CustWriteLn('Credit File Sweeps:');
          PrintType('399', '', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;
          if rgSourceFileType.ItemIndex = 1 then
          begin
            CustWriteLn('Other Credit:');
            PrintType('OTHER', 'Credit', iExceptionCounter, cExceptionAmount);
            CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
            CustWriteLn;
          end;
          CustWriteLn('Debit File Direct Withdrawals:');
          PrintType('455', '', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Cleared counter:    %12u    Cleared amount:    %15m', [iDirDepCleared, cDirDepCleared]));
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;
          CustWriteLn('Debit File EFTPS Direct Withdrawals:');
          PrintType('455', 'EFTPS/IRS', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Cleared counter:    %12u    Cleared amount:    %15m', [iEftpsCleared, cEftpsCleared]));
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;
          CustWriteLn('Debit File Checks:');
          PrintType('475', '', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Cleared counter:    %12u    Cleared amount:    %15m', [iChecksCleared, cChecksCleared]));
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;

          if iDeadAmount > 0 then
          begin
            CustWriteLn('Debit File Checks Dead:');
            PrintType('000', '', iExceptionCounter, cExceptionAmount);
            CustWriteLn(Format('  Dead counter:    %12u    Dead amount:    %15m', [iDeadAmount, cDeadAmount]));
            CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
            CustWriteLn;
          end;
          {CustWriteLn('Debit File Duplicate Checks:');
          PrintType('475','DUPL',iExceptionCounter,cExceptionAmount);
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m',[iExceptionCounter, cExceptionAmount]));
          CustWriteLn;}
          //CustWriteLn('Debit File Cleared Checks:');
          //CustWriteLn;
          CustWriteLn('Debit File Sweeps:');
          PrintType('575', '', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;
          CustWriteLn('Debit File Service Charges:');
          PrintType('698', '', iExceptionCounter, cExceptionAmount);
          CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
          CustWriteLn;
          if rgSourceFileType.ItemIndex = 0 then
          begin
            CustWriteLn('Other Debit:');
            PrintType('OTHER', 'Debit', iExceptionCounter, cExceptionAmount);
            CustWriteLn(Format('  Uncleared counter:  %12u    Uncleared amount:  %15m', [iExceptionCounter, cExceptionAmount]));
            CustWriteLn;
          end;
          CustWriteLn(Format('      Total cleared counter:   %12u          Total Cleared amount   %15m', [iChecksCleared + iDirDepCleared+ iAchCredited,
            cChecksCleared + cDirDepCleared+ cAchCredited]));
          CustWriteLn(Format('      Total Uncleared counter: %12u          Total Uncleared amount %15m', [iTotalExceptionCounter,
            cTotalExceptionAmout]));
          with DM_SERVICE_BUREAU do
          begin
            SB_BANK_ACCOUNTS.DisableControls;
            try
              SB_BANK_ACCOUNTS.Close;
              SB_BANK_ACCOUNTS.DataRequired;
              MyTransManager.Initialize([SB_BANK_ACCOUNTS]);
              if SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', AccNbr, []) then
              begin
                SB_BANK_ACCOUNTS.Edit;
                sData := ConvertNull(SB_BANK_ACCOUNTS['FILLER'], '');
                if not FirstTime then
                begin
                  iChecksCleared := iChecksCleared + StrToIntDef(Trim(Copy(sData, 1, 15)), 0);
                  cChecksCleared := cChecksCleared + StrToCurrDef(Trim(Copy(sData, 16, 15)), 0);
                  iTotalExceptionCounter := iTotalExceptionCounter + StrToIntDef(Trim(Copy(sData, 31, 15)), 0);
                  cTotalExceptionAmout := cTotalExceptionAmout + StrToCurrDef(Trim(Copy(sData, 46, 15)), 0);
                end;
                ReplaceInPosition(sData, IntToStr(iChecksCleared + iDirDepCleared), 1, 15);
                ReplaceInPosition(sData, CurrToStr(cChecksCleared + cDirDepCleared), 16, 15);
                ReplaceInPosition(sData, IntToStr(iTotalExceptionCounter), 31, 15);
                ReplaceInPosition(sData, CurrToStr(cTotalExceptionAmout), 46, 15);
                SB_BANK_ACCOUNTS['FILLER'] := sData;
                SB_BANK_ACCOUNTS.Post;
              end;
              MyTransManager.ApplyUpdates;
              CustWriteLn(Format('Tally total cleared counter:   %12u    Tally total Cleared amount   %15m', [iChecksCleared +
                iDirDepCleared, cChecksCleared + cDirDepCleared]));
              CustWriteLn(Format('Tally total Uncleared counter: %12u    Tally total Uncleared amount %15m', [iTotalExceptionCounter,
                cTotalExceptionAmout]));
            finally
              if SB_BANK_ACCOUNTS.State = dsEdit then SB_BANK_ACCOUNTS.Cancel;
              SB_BANK_ACCOUNTS.EnableControls;
            end;
          end;
          CustWriteLn;
          lTmpRegister.CancelRange;
          lTmpRegister.Filter := '';
          lTmpRegister.Filtered := false;

          lTmpRegister.IndexFieldNames := 'CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR';
            //  One Check Payment for many clients
          FindOneCheckAndGroupIdentifierBarRecords;

          lTmpRegister.Filter := 'CHANGED in ( 1, 2 )  and CO_BANK_ACCOUNT_REGISTER_NBR <> -1 ';
          lTmpRegister.Filtered := True;
          if lTmpRegister.RecordCount > 0 then
          try

            ProcessBarRecords(lTmpRegister);

          except
            if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Active and (DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.ChangeCount > 0) then
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CancelUpdates;
            raise;
          end
          else
            CustWriteLn('There are no changes to client databases');
        finally
          ctx_DataAccess.LockPRSimple;
          DM_TEMPORARY.TMP_CO.IndexDefs.Clear;
          DM_TEMPORARY.TMP_CL.IndexDefs.Clear;
          DM_TEMPORARY.TMP_CO.IndexName := '';
          DM_TEMPORARY.TMP_CL.IndexName := '';
          lTmpRegister.Free;
        end;
      finally
        CustWriteln(#12);
        CloseFile(rf);
        CloseFile(sf);
        dsExpt.CancelUpdates;
        dsExpt.Free;
      end;
    finally
      if MyTransManager.IsModified then MyTransManager.CancelUpdates;
      MyTransManager.Free;
    end;
  finally
    ctx_DataAccess.SkipPrCheckPostCheck := False;
    lList.Free;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.spbBankMergeReportDirClick(
  Sender: TObject);
begin
  inherited;
  dlgBankMergeReportFile.FileName := editBankMergeReportFile.Text;
  if dlgBankMergeReportFile.Execute then
    editBankMergeReportFile.Text := dlgBankMergeReportFile.FileName;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.btnTwImportViewClick(
  Sender: TObject);
begin
  inherited;
  ShellExecute(0, 'open', 'notepad.exe', PChar(editBankMergeReportFile.Text), '', SW_SHOWNORMAL);
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.spbBankMergeStorageDirClick(
  Sender: TObject);
begin
  inherited;
  dlgBankMergeStorageFile.FileName := editBankMergeStorageFile.Text;
  if dlgBankMergeStorageFile.Execute then
    editBankMergeStorageFile.Text := dlgBankMergeStorageFile.FileName;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.ckboxBankMergeFirstClick(
  Sender: TObject);
begin
  inherited;
  editBankMergeStorageFile.Enabled := ckboxBankMergeFirst.Checked;
  spbBankMergeStorageDir.Enabled := editBankMergeStorageFile.Enabled;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.btnStorageScanClick(
  Sender: TObject);
var
  sf: TextFile;
  s: string;
  i: Integer;
begin
  inherited;
  wwStorageDataSet.Close;
  wwStorageDataSet.CreateDataSet;

  if editBankMergeStorageFile.Text = '' then
    raise EInvalidParameters.CreateHelp('Empty storage file name is given', IDH_InvalidParameters);
  AssignFile(sf, editBankMergeStorageFile.Text);
  try
    Reset(sf);
  except
    on E: Exception do
    begin
      E.Message := 'Can''t open storage file'#13 + E.Message;
      raise;
    end;
  end;
  cboStorageReportTransferType.Items.Clear;
  try
    while not Eof(sf) do
    begin
      Readln(sf, s);
      if (StrToDate(Copy(s, 18, 10)) >= editStorageReportDateFrom.Date) and (StrToDate(Copy(s, 18, 10)) <= editStorageReportDateTill.Date)
        then
      begin
        wwStorageDataSet.Append;
        wwStorageDataSet['type'] := Trim(Copy(s, 1, 3));
        if Trim(Copy(s, 4, 4)) = '' then
          wwStorageDataSet['CompNumber'] := Null
        else
          wwStorageDataSet['CompNumber'] := Trim(Copy(s, 4, 4));
        if Trim(Copy(s, 8, 10)) = '' then
          wwStorageDataSet['CheckNumber'] := Null
        else
          wwStorageDataSet['CheckNumber'] := Trim(Copy(s, 8, 10));
        wwStorageDataSet['ClearDate'] := StrToDate(Copy(s, 18, 10));
        wwStorageDataSet['Amount'] := StrToCurr(Trim(Copy(s, 28, 15)));
        wwStorageDataSet['Problem'] := Trim(Copy(s, 43, 40));
        wwStorageDataSet.Post;
        if (wwStorageDataSet['type'] = '475') and (wwStorageDataSet['CompNumber'] = 'EFTP') then
        begin
          if cboStorageReportTransferType.Items.IndexOf('475EFTP') = -1 then
            cboStorageReportTransferType.Items.Add('475EFTP');
        end
        else
          if cboStorageReportTransferType.Items.IndexOf(wwStorageDataSet['type']) = -1 then
          cboStorageReportTransferType.Items.Add(wwStorageDataSet['type']);
      end;
    end;
    for i := 0 to cboStorageReportTransferType.Items.Count - 1 do
      if cboStorageReportTransferType.Items[i] = '165' then
        cboStorageReportTransferType.Items[i] := '165 - Credit File Direct Deposits'
      else
        if cboStorageReportTransferType.Items[i] = '195' then
        cboStorageReportTransferType.Items[i] := '195 - Credit File Wire Transfers'
      else
        if cboStorageReportTransferType.Items[i] = '252' then
        cboStorageReportTransferType.Items[i] := '252 - Credit File Debit Reversals'
      else
        if cboStorageReportTransferType.Items[i] = '301' then
        cboStorageReportTransferType.Items[i] := '301 - Credit File Regular Deposits'
      else
        if cboStorageReportTransferType.Items[i] = '399' then
        cboStorageReportTransferType.Items[i] := '399 - Credit File Sweeps'
      else
        if cboStorageReportTransferType.Items[i] = '455' then
        cboStorageReportTransferType.Items[i] := '455 - Debit File Direct Withdrawals'
      else
        if cboStorageReportTransferType.Items[i] = '455EFTP' then
        cboStorageReportTransferType.Items[i] := '455EFTP - Debit File EFTPS Direct Withdrawals'
      else
        if cboStorageReportTransferType.Items[i] = '475' then
        cboStorageReportTransferType.Items[i] := '475 - Debit File Checks'
      else
        if cboStorageReportTransferType.Items[i] = '575' then
        cboStorageReportTransferType.Items[i] := '575 - Debit File Sweeps'
      else
        if cboStorageReportTransferType.Items[i] = '698' then
        cboStorageReportTransferType.Items[i] := '698 - Debit File Service Charges';
    editStorageReportDateFrom.Enabled := True;
    editStorageReportDateTill.Enabled := True;
    cboStorageReportTransferType.Enabled := True;
    cboStorageReportTransferType.Text := '';
    btnStorageReport.Enabled := True;
    btnStorageReportPreview.Enabled := True;
  finally
    wwStorageDataSet.MergeChangeLog;
    CloseFile(sf);
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.btnStorageReportClick(Sender: TObject);
var
  ReportParams: TrwReportParams;
  I: Integer;
  h1, h2: String;
  prv: TrwReportDestination;

begin
  inherited;
  if not TempDataSet.Active then
    TempDataSet.CreateDataSet
  else
    TempDataSet.EmptyDataSet;

  ReportParams := TrwReportParams.Create;
  DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('REPORT_DESCRIPTION=''Storage Report''');

  if cboStorageReportTransferType.Text = '' then
  begin
    h1 := wwdsMaster.DataSet['Bank_Name_Lookup'] + ' - ' + wwdsMaster.DataSet['CUSTOM_BANK_ACCOUNT_NUMBER'];
    h2 := 'Exception report for "ALL" transfer types in period ' +
      DateToStr(editStorageReportDateFrom.Date) + ' - ' + DateToStr(editStorageReportDateTill.Date);
  end

  else
  begin
    h1 := wwdsMaster.DataSet['Bank_Name_Lookup'] + ' - ' + wwdsMaster.DataSet['CUSTOM_BANK_ACCOUNT_NUMBER'];
    h2 := 'Exception report for "' + cboStorageReportTransferType.Text + '" transfer type in period ' +
      DateToStr(editStorageReportDateFrom.Date) + ' - ' + DateToStr(editStorageReportDateTill.Date);
  end;

  ReportParams.Add('Account', h1, False);
  ReportParams.Add('Title', h2, False);

  wwStorageDataSet.IndexFieldNames := 'TYPE;ClearDate;CheckNumber';

  if cboStorageReportTransferType.Text = '' then
    wwStorageDataSet.Filter := ''
  else if Copy(cboStorageReportTransferType.Text, 1, 7) = '455EFTP' then
    wwStorageDataSet.Filter := 'type = ''475'' and CompNumber = ''EFTP'' and ClearDate >= ''' + DateToStr(editStorageReportDateFrom.Date) +
      ''' and ClearDate <= ''' + DateToStr(editStorageReportDateTill.Date) + ''''
  else
    wwStorageDataSet.Filter := 'type = ''' + Copy(cboStorageReportTransferType.Text, 1, 3) + ''' and ClearDate >= ''' +
      DateToStr(editStorageReportDateFrom.Date) + ''' and ClearDate <= ''' + DateToStr(editStorageReportDateTill.Date) + '''';

  wwStorageDataSet.Filtered := True;

  wwStorageDataSet.First;
  while not wwStorageDataSet.Eof do
  begin
    TempDataSet.Insert;
    for I := 0  to wwStorageDataSet.FieldCount - 1 do
    begin
      TempDataSet.Fields.Fields[I].Value := wwStorageDataSet.Fields.Fields[I].Value;
    end;
    TempDataSet.Post;
    wwStorageDataSet.Next;
  end;

  if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'Storage Report', [loCaseInsensitive]) then
     raise EMissingReport.CreateHelp('"Storage Report" report does not exist in the database!', IDH_MissingReport);

  try
    ReportParams.Add('DataSets', DataSetsToVarArray([TempDataSet]), False);
    if Sender = btnStorageReportPreview then
      prv := rdtPreview
    else
      prv := rdtPrinter;

    if TempDataSet.RecordCount > 0 then
    begin
      ctx_RWLocalEngine.StartGroup;
      try
        ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').Value,
             CH_DATABASE_SYSTEM, ReportParams);
      finally
        ctx_RWLocalEngine.EndGroupForTask(prv);
      end;
    end;
  finally
    wwStorageDataSet.Filtered := False;
    ReportParams.Free;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.cboStorageReportTransferTypeChange(
  Sender: TObject);
begin
  inherited;
  btnStorageReport.Enabled := True;
  btnStorageReportPreview.Enabled := True;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.wwdgTeamRowChanged(
  Sender: TObject);
begin
  inherited;
  if Assigned(wwdsMaster.DataSet) and not VarIsNull(wwdsMaster.DataSet['SB_BANK_ACCOUNTS_NBR']) then
  begin
    RestoreComponentState(editBankMergeSourceFile, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
    RestoreComponentState(editBankMergeReportFile, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
    RestoreComponentState(editBankMergeStorageFile, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
    RestoreComponentState(cbCompPosOr, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
    RestoreComponentState(edCompPosOr, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
    RestoreComponentState(evrbCompCode, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
    RestoreComponentState(evrbCompName, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
    RestoreComponentState(edCompCodeLength, wwdsMaster.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
    if not (evrbCompCode.Checked or evrbCompName.Checked) then
       evrbCompCode.Checked := True;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht5ReportRunBtnClick(
  Sender: TObject);

  function GetSelectedCompanies: string;
  var
    ds: TDataSet;
    pBookMark: TBookmark;
    i: Integer;
  begin
    ds := tsht2CompFilterGroupGrid.DataSource.DataSet;
    if tsht2CompFilterGroupGrid.SelectedList.Count = 0 then
    begin
      Result := IntToStr(ds['co_nbr'])+','+ IntToStr(ds['cl_nbr']);
    end
    else
    begin
      ds.DisableControls;
      pBookMark := ds.GetBookmark;
      try
        Result := '';
        for i := 0 to tsht2CompFilterGroupGrid.SelectedList.Count - 1 do
        begin
          ds.GotoBookmark(tsht2CompFilterGroupGrid.SelectedList.Items[i]);
          if Result <> '' then
            Result := Result + #1;
          Result := Result + IntToStr(ds['co_nbr'])+','+ IntToStr(ds['cl_nbr']);
        end;
      finally
        ds.GotoBookmark(pBookMark);
        ds.FreeBookmark(pBookMark);
        ds.EnableControls;
      end;
    end;
  end;

  procedure GetAllCompanies(RP: TrwReportParams);
  var
    ds: TDataSet;
    co, cl: Variant;
  begin
    ds := tsht2CompFilterGroupGrid.DataSource.DataSet;
    ds.DisableControls;
    try
      co := varArrayCreate([0, ds.RecordCount - 1], varVariant);
      cl := varArrayCreate([0, ds.RecordCount - 1], varVariant);
      ds.First;
      while not ds.Eof do
      begin
        co[ds.RecNo - 1] := ds['co_nbr'];
        cl[ds.RecNo - 1] := ds['cl_nbr'];
        ds.Next;
      end;
    finally
      ds.EnableControls;
    end;

    RP.Add('Clients', cl, False);
    RP.Add('Companies', co, False);
  end;

  function GetOptionCaption(r: TRadioGroup): string;
  begin
    Result := StringReplace(r.Caption + ' - ' + r.Items[r.ItemIndex], '&&', '&', [rfReplaceAll]);
  end;
  function CheckSortByAndGetOptionalCaption(r: TRadioGroup): string;
  begin
    if tsht5RadioSortBy.ItemIndex = 2 then
      Result := GetOptionCaption(r)
    else
      Result := '';
  end;

  procedure PrintCheckReconBalanceReport;
  var
    ReportParams: TrwReportParams;
  begin
    ReportParams := TrwReportParams.Create;
    try
      DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('REPORT_DESCRIPTION=''Check Recon Balance Report''');
      if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'Check Recon Balance Report', [loCaseInsensitive]) then
        raise EMissingReport.CreateHelp('"Check Recon Balance Report" report does not exist in the database!', IDH_MissingReport);

      ReportParams.Add('RP0', IntToStr(tsht5RadioBasedOn.ItemIndex), False);
      ReportParams.Add('RP1', IntToStr(tsht5RadioCreditDebit.ItemIndex), False);
      ReportParams.Add('RP2', IntToStr(tsht5RadioTransactionStatus.ItemIndex), False);
      ReportParams.Add('RP3', DateToStr(tsht2DateFilterGroupDateFrom.Date), False);
      ReportParams.Add('RP4', DateToStr(tsht2DateFilterGroupDateTill.Date), False);
      ReportParams.Add('RP5', IntToStr(tsht5RadioSortBy.ItemIndex), False);
      ReportParams.Add('RP6', IntToStr(tsht5RadioTransactionStatus.ItemIndex), False);
      ReportParams.Add('RP7', IntToStr(tsht5RadioSummaryType.ItemIndex), False);
      ReportParams.Add('RP8', IntToStr(tsht5RadioCompanyPageBreak.ItemIndex), False);
      ReportParams.Add('RP9', wwdsMaster.Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString, False);
      ReportParams.Add('RP10', IntToStr(Integer(tsht2DateFilterGroupRbPeriod.Checked)), False);
      ReportParams.Add('RP11', IntToStr(Integer(tsht2DateFilterGroupRbAll.Checked)), False);
      ReportParams.Add('RP12', IntToStr(Integer(tsht2CompFilterGroupRbSelect.Checked)), False);
      ReportParams.Add('RP13', IntToStr(Integer(tsht2CompFilterGroupRbAll.Checked)), False);
      ReportParams.Add('RP14', GetSelectedCompanies, False);
      ReportParams.Add('RP15', wwdsMaster.DataSet['Bank_Name_Lookup']+ ' - '+ wwdsMaster.DataSet['CUSTOM_BANK_ACCOUNT_NUMBER'], False);
      ReportParams.Add('RP16', tsht5FilterParam.Caption, False);
      ReportParams.Add('RP17', GetOptionCaption(tsht5RadioBasedOn), False);
      ReportParams.Add('RP18', GetOptionCaption(tsht5RadioTransactionStatus), False);
      ReportParams.Add('RP19', GetOptionCaption(tsht5RadioCreditDebit), False);
      ReportParams.Add('RP20', GetOptionCaption(tsht5RadioSortBy), False);
      ReportParams.Add('RP21', CheckSortByAndGetOptionalCaption(tsht5RadioSummaryType), False);
      ReportParams.Add('RP22', CheckSortByAndGetOptionalCaption(tsht5CompanyRadioOnlyWithTrans), False);
      ReportParams.Add('RP23', IntToStr(Integer(evCheckBox1.Checked)), False);

      ctx_RWLocalEngine.StartGroup;
      try
        ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS['SY_REPORT_WRITER_REPORTS_NBR'],
          CH_DATABASE_SYSTEM, ReportParams, 'Check Recon Balance Report');
      finally
        ctx_RWLocalEngine.EndGroupForTask(rdtPreview);
      end;
    finally
      ReportParams.Free;
    end;
  end;

  procedure PrintCheckReconBalSummaryReport;
  var
    ReportParams: TrwReportParams;
    S: String;
    co, cl: Variant;
    n, i: Integer;
  begin
    ReportParams := TrwReportParams.Create;
    try
      DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('REPORT_DESCRIPTION=''Check Recon Bal Summary Report''');
      if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'Check Recon Bal Summary Report', [loCaseInsensitive]) then
        raise EMissingReport.CreateHelp('"Check Recon Balance Report" report does not exist in the database!', IDH_MissingReport);

      if tsht5RadioBasedOn.ItemIndex = 0 then
        ReportParams.Add('UseDate', 'CHECK_DATE', False)
      else
        ReportParams.Add('UseDate', 'TRANSACTION_EFFECTIVE_DATE', False);

      ReportParams.Add('dat_b', tsht2DateFilterGroupDateFrom.Date);
      ReportParams.Add('dat_e', tsht2DateFilterGroupDateTill.Date);
      ReportParams.Add('BankNbr', wwdsMaster.Dataset.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString, False);

      with tsht2CompFilterGroupGrid.DataSource.DataSet do
      begin
        if tsht2CompFilterGroupRbSelect.Checked then
        begin
          S := GetSelectedCompanies;

          n := 0;
          for i := 1 to Length(S) do
            if S[i] = #1 then
              Inc(n);

          if n = 0 then
            n := 1;

          cl := varArrayCreate([0, n], varVariant);
          co := varArrayCreate([0, n], varVariant);

          i := 0;
          while Pos(#1, S) > 0 do
          begin
            cl[i] := System.Copy(S, Pos(',',S)+1,Pos(#1, S)-Pos(',',S)-1);
            co[i] := System.Copy(S, 1, Pos(',',S)-1);
            Inc(i);
            System.Delete(S, 1, Pos(#1, S));
          end;

          cl[i] := System.Copy(S, Pos(',',S)+1,Length(S));
          co[i] := System.Copy(S, 1, Pos(',',S)-1);

          ReportParams.Add('Clients', cl, False);
          ReportParams.Add('Companies', co, False);
        end
        else
          GetAllCompanies(ReportParams);
      end;

      ctx_RWLocalEngine.StartGroup;
      try
        ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS['SY_REPORT_WRITER_REPORTS_NBR'],
          CH_DATABASE_SYSTEM, ReportParams, 'Check Recon Balance Summary Report');
      finally
        ctx_RWLocalEngine.EndGroupForTask(rdtPreview);
      end;

    finally
      ReportParams.Free;
    end;
  end;
begin
  inherited;
  if evRadioGroup1.ItemIndex = 0 then
    PrintCheckReconBalanceReport
  else
    PrintCheckReconBalSummaryReport;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht5RadioSortByClick(
  Sender: TObject);
begin
  inherited;
  tsht5CompanyRadioOnlyWithTrans.Visible := tsht5RadioSortBy.ItemIndex = 2;
  tsht5RadioSummaryType.Visible := tsht5RadioSortBy.ItemIndex = 2;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht3wwDBGridKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = 118) and (btnMarkCleared.Enabled) then
    btnMarkCleared.Click
  else
  if (Key = 117) and (btnMarkUnCleared.Enabled) then
    btnMarkUnCleared.Click
  else
  if ((Key = 13) and (Shift = [ssShift])) and (wwdsDetail.State in [dsEdit, dsInsert]) then
    MyApplyUpdates
  else
  if ((Key = 46) and (Shift = [ssCtrl]))
    and not VarIsNull(wwdsDetail.DataSet['cl_nbr'])
    and (EvMessage('You are about to physically delete transaction. Proceed?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
  begin
    wwdsDetail.DataSet.Delete;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.MyApplyUpdates;
var
  ClientNbr: Integer;
  procedure ApplyUpdates;
  begin
    if ClientNbr <> -1 then
    begin
      with DM_PAYROLL.PR do
      begin
        First;
        while not Eof do
        begin
          ctx_DataAccess.UnlockPR;
          Next;
        end;
      end;
      DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.OnRecordReconcileEvent := OnMyApplyUpdatesReconcile;
      try
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER, DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_MISCELLANEOUS_CHECKS]);
      finally
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.OnRecordReconcileEvent := nil;
      end;
      with DM_PAYROLL.PR do
      begin
        First;
        while not Eof do
        begin
          ctx_DataAccess.LockPR(True);
          Next;
        end;
      end;
      ctx_DataAccess.CommitNestedTransaction;
    end;
  end;
  procedure CancelUpdates;
  begin
    if ClientNbr <> -1 then
    begin
      ctx_DataAccess.CancelDataSets([DM_COMPANY.CO_BANK_ACCOUNT_REGISTER, DM_PAYROLL.PR_CHECK, DM_PAYROLL.PR_MISCELLANEOUS_CHECKS]);
    end;
  end;
  procedure AssignRecord(const Dataset: TEvBasicClientDataSet);
  var
    i: Integer;
  begin
    with DM_COMPANY.CO_BANK_ACCOUNT_REGISTER do
    begin
      for i := 0 to DataSet.FieldCount - 1 do
        if DataSet.Fields[i].DataType <> ftBlob then
        begin
          if (VarType(DataSet.Fields[i].NewValue) <> varEmpty)
          and (FindField(DataSet.Fields[i].FieldName) <> nil) then
            FieldByName(DataSet.Fields[i].FieldName).Assign(DataSet.Fields[i]);
        end
        else
          if (DataSet.FieldByName('NotesUpdated').AsString = 'Y') and
             (FindField(DataSet.Fields[i].FieldName) <> nil) then
            FieldByName(DataSet.Fields[i].FieldName).Assign(DataSet.Fields[i]);
    end;
  end;
  procedure ReconcileTempTable;
  var
    s: string;
    i: Integer;
    iClNbr, iOldValue, iNewValue: Integer;
  begin
    for i := 0 to FMyApplyUpdatesReconcileList.Count-1 do
    begin
      s := FMyApplyUpdatesReconcileList[i];
      iClNbr := StrToIntDef(Fetch(s), 0);
      iOldValue := StrToIntDef(Fetch(s), 0);
      iNewValue := StrToIntDef(Fetch(s), 0);
      Assert(wwdsDetail.DataSet.Locate('CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR', VarArrayOf([iClNbr, iOldValue]), []));
      wwdsDetail.DataSet.Edit;
      wwdsDetail.DataSet.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger := iNewValue;
      wwdsDetail.DataSet.Post;
    end;
  end;
  procedure RefreshDetailDatasetAfterInsert;
  var
    i, lKey: integer;
  begin
    ctx_StartWait;
    try
      if Assigned(lTMP_CO_BANK_ACCOUNT_REGISTER.FindField('CO_BANK_ACCOUNT_REGISTER_NBR')) then
        lKey := lTMP_CO_BANK_ACCOUNT_REGISTER.FindField('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger
      else
        lKey := -1;

      lTMP_CO_BANK_ACCOUNT_REGISTER.Close;
      lTMP_CO_BANK_ACCOUNT_REGISTER.ProviderName := DM_TEMPORARY.TMP_CO_BANK_ACCOUNT_REGISTER.ProviderName;
      lTMP_CO_BANK_ACCOUNT_REGISTER.DataRequired(sCurrentFilter);
      for i := 0 to lTMP_CO_BANK_ACCOUNT_REGISTER.FieldCount-1 do
        lTMP_CO_BANK_ACCOUNT_REGISTER.Fields[i].Required := False;

      if lKey > -1 then
        lTMP_CO_BANK_ACCOUNT_REGISTER.Locate('CO_BANK_ACCOUNT_REGISTER_NBR', lKey, [])
      else
        lTMP_CO_BANK_ACCOUNT_REGISTER.First;
    finally
      ctx_EndWait;
    end;
  end;
var
  s: string;
  cd2: TEvBasicClientDataSet;
  cd3: TEvBasicClientDataSet;
  cd3RecNo: longint;
begin
  FMyApplyUpdatesReconcileList.Clear;
  ctx_DataAccess.SkipPrCheckPostCheck := True;
  cd2 := wwdsDetail.DataSet as TEvBasicClientDataSet;
  cd2.DisableControls;
  try
    tsht3wwDBGrid.FlushChanges;
    cd2.CheckBrowseMode;
    if cd2.ChangeCount > 0 then
    begin
      ClientNbr := -1;
      cd3 := TEvBasicClientDataSet.Create(nil);
      try
        cd3.IndexFieldNames := 'CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR';
        cd3.Delta := cd2.Delta;
        cd3.First;
        try
          while not cd3.Eof do
          begin
            if not VarIsNull(cd3['CL_NBR']) then
            begin
              if (cd3['CL_NBR'] <> ClientNbr) then
              begin
                ApplyUpdates;
                ctx_DataAccess.UnlockPRSimple;
                ClientNbr := cd3['CL_NBR'];
                cd3RecNo := cd3.RecNo;
                ctx_DataAccess.OpenClient(ClientNbr);
                ctx_DataAccess.StartNestedTransaction([dbtClient]);
                s := '-1';
                while (cd3['CL_NBR'] = ClientNbr) and not cd3.Eof  do
                begin
                  if not VarIsNull(cd3['CO_BANK_ACCOUNT_REGISTER_NBR'])
                  and (cd3.CompleteUpdateStatus in [usModified, usDeleted]) then
                    s := s+ ','+ IntToStr(cd3['CO_BANK_ACCOUNT_REGISTER_NBR']);
                  cd3.Next;
                end;
                cd3.RecNo := cd3RecNo;
                cd3.Resync([]);
                DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('CO_BANK_ACCOUNT_REGISTER_NBR in ('+ s+ ')');
                s := '-1';
                with DM_COMPANY.CO_BANK_ACCOUNT_REGISTER do
                begin
                  First;
                  while not Eof do
                  begin
                    if not VarIsNull(FieldValues['PR_CHECK_NBR']) then
                      s := s+ ','+ IntToStr(FieldValues['PR_CHECK_NBR']);
                    Next;
                  end
                end;
                DM_PAYROLL.PR_CHECK.DataRequired('PR_CHECK_NBR in ('+ s+ ')');
                s := '-1';
                with DM_COMPANY.CO_BANK_ACCOUNT_REGISTER do
                begin
                  First;
                  while not Eof do
                  begin
                    if not VarIsNull(FieldValues['PR_MISCELLANEOUS_CHECKS_NBR']) then
                      s := s+ ','+ IntToStr(FieldValues['PR_MISCELLANEOUS_CHECKS_NBR']);
                    Next;
                  end
                end;
                DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR in ('+ s+ ')');
                s := '-1';
                with DM_PAYROLL.PR_CHECK do
                begin
                  First;
                  while not Eof do
                  begin
                    s := s+ ','+ IntToStr(FieldValues['PR_NBR']);
                    Next;
                  end
                end;
                with DM_PAYROLL.PR_MISCELLANEOUS_CHECKS do
                begin
                  First;
                  while not Eof do
                  begin
                    s := s+ ','+ IntToStr(FieldValues['PR_NBR']);
                    Next;
                  end
                end;
                DM_PAYROLL.PR.DataRequired('PR_NBR in ('+ s+ ')');
              end;
              while (cd3['CL_NBR'] = ClientNbr) and not cd3.Eof do
              begin
                case cd3.CompleteUpdateStatus of
                usModified:
                  begin
                    Assert(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Locate('CO_BANK_ACCOUNT_REGISTER_NBR', cd3['CO_BANK_ACCOUNT_REGISTER_NBR'], []));
                    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
                    AssignRecord(cd3);
                    if not VarIsNull(DM_PAYROLL.PR_CHECK['PR_NBR'])
                    and DM_PAYROLL.PR_CHECK.Locate('PR_CHECK_NBR', cd3['PR_CHECK_NBR'], [])
                    and DM_PAYROLL.PR.Locate('PR_NBR', DM_PAYROLL.PR_CHECK['PR_NBR'], [])
                    and (VarType(cd3.FieldByName('STATUS').NewValue) <> varEmpty) then
                    begin
                      DM_PAYROLL.PR_CHECK.Edit;
                      if cd3.FieldByName('STATUS').AsString = BANK_REGISTER_STATUS_Cleared then
                        DM_PAYROLL.PR_CHECK['CHECK_STATUS'] := CHECK_STATUS_CLEARED
                      else
                      if cd3.FieldByName('STATUS').AsString = BANK_REGISTER_STATUS_Dead then
                        DM_PAYROLL.PR_CHECK['CHECK_STATUS'] := CHECK_STATUS_DEAD
                      else
                      if cd3.FieldByName('STATUS').AsString = BANK_REGISTER_STATUS_Stop_payment then
                        DM_PAYROLL.PR_CHECK['CHECK_STATUS'] := CHECK_STATUS_STOP_PAYMENT
                      else
                      if cd3.FieldByName('STATUS').AsString = BANK_REGISTER_STATUS_Outstanding then
                        DM_PAYROLL.PR_CHECK['CHECK_STATUS'] := CHECK_STATUS_OUTSTANDING;
                      if DM_PAYROLL.PR_CHECK.Modified then
                        DM_PAYROLL.PR_CHECK.Post
                      else
                        DM_PAYROLL.PR_CHECK.Cancel;
                    end;
                    if not VarIsNull(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['PR_MISCELLANEOUS_CHECKS_NBR']) then
                      if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Locate('PR_MISCELLANEOUS_CHECKS_NBR', DM_COMPANY.CO_BANK_ACCOUNT_REGISTER['PR_MISCELLANEOUS_CHECKS_NBR'], [])
                      and DM_PAYROLL.PR.Locate('PR_NBR', DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['PR_NBR'], [])
                      and (VarType(cd3.FieldByName('STATUS').NewValue) <> varEmpty) then
                      begin
                        DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Edit;
                        if cd3.FieldByName('STATUS').AsString = BANK_REGISTER_STATUS_Cleared then
                          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['CHECK_STATUS'] := CHECK_STATUS_CLEARED
                        else
                        if cd3.FieldByName('STATUS').AsString = BANK_REGISTER_STATUS_Dead then
                          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['CHECK_STATUS'] := CHECK_STATUS_DEAD
                        else
                        if cd3.FieldByName('STATUS').AsString = BANK_REGISTER_STATUS_Stop_payment then
                          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['CHECK_STATUS'] := CHECK_STATUS_STOP_PAYMENT
                        else
                        if cd3.FieldByName('STATUS').AsString = BANK_REGISTER_STATUS_Outstanding then
                          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS['CHECK_STATUS'] := CHECK_STATUS_OUTSTANDING;
                        if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Modified then
                          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Post
                        else
                          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.Cancel;
                      end;
                    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
                  end;
                usDeleted:
                  begin
                    Assert(DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Locate('CO_BANK_ACCOUNT_REGISTER_NBR', cd3['CO_BANK_ACCOUNT_REGISTER_NBR'], []));
                    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Delete;
                  end;
                usInserted:
                  begin
                    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Append;
                    AssignRecord(cd3);
                    DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
                  end;
                end;
                cd3.Next;
              end;
            end
            else
              cd3.Next;
          end;
          ApplyUpdates;
          ReconcileTempTable;
          cd2.MergeChangeLog;
          // after insetring a new record somehow it does not appear in DM_COMPANY.CO_BANK_ACCOUNT_REGISTER
          // when calling DataRequired with that new record "_nbr" in filter expression
          // until refresh lTmp_CO_BANK_ACCOUNT_REGISTER dataset
          // (ticket #64883)
          if cd3.CompleteUpdateStatus = usInserted then
          begin
            RefreshDetailDatasetAfterInsert;
            UnselectBARGrid;
          end;
        except
          CancelUpdates;
          ctx_DataAccess.RollbackNestedTransaction;
          raise;
        end;
      finally
        cd3.Free;
      end;
    end;
  finally
    cd2.EnableControls;
    wwdsDetailStateChange(nil);
    ctx_DataAccess.LockPRSimple;
    ctx_DataAccess.SkipPrCheckPostCheck := False;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.FormDestroy(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 0 to tsht3wwDBGrid.FieldCount - 1 do
    if (tsht3wwDBGrid.Fields[i].FieldName <> 'STATUS')
      and (tsht3wwDBGrid.Fields[i].FieldName <> 'TRANSACTION_EFFECTIVE_DATE')
      and (tsht3wwDBGrid.Fields[i].FieldName <> 'CLEARED_AMOUNT') then
      tsht3wwDBGrid.Fields[i].ReadOnly := False;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.Button1Click(Sender: TObject);
var
  v: Variant;
  i: Integer;
begin
  inherited;
  if (wwdsDetail.DataSet['REGISTER_type'] = BANK_REGISTER_TYPE_Manual)
    and (wwdsDetail.DataSet['REGISTER_type'] <> BANK_REGISTER_STATUS_Cleared)
    and (wwdsDetail.DataSet['manual_type'] <> BANK_REGISTER_MANUALTYPE_MadeByMistake_Void)
  then
  begin
    if (EvMessage('You are about to void the transaction. Proceed?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
    begin
      wwdsDetail.DataSet.Tag := 1;
      try
        v := VarArrayCreate([0, wwdsDetail.DataSet.FieldCount - 1], varVariant);
        for i := 0 to wwdsDetail.DataSet.FieldCount - 1 do
          v[i] := wwdsDetail.DataSet.Fields[i].Value;
        wwdsDetail.DataSet.Edit;
        wwdsDetail.DataSet['manual_type'] := BANK_REGISTER_MANUALTYPE_MadeByMistake_Void;
        wwdsDetail.DataSet.Insert;
        for i := 0 to wwdsDetail.DataSet.FieldCount - 1 do
          wwdsDetail.DataSet.Fields[i].Value := v[i];
        wwdsDetail.DataSet['CO_BANK_ACCOUNT_REGISTER_NBR'] := Null;
        wwdsDetail.DataSet['manual_type'] := BANK_REGISTER_MANUALTYPE_MadeByMistake_Void;
        wwdsDetail.DataSet['amount'] := -wwdsDetail.DataSet['amount'];
        MyApplyUpdates;
      finally
        wwdsDetail.DataSet.Tag := 0;
      end;
    end;
  end
  else
    raise EUpdateError.CreateHelp('Transaction should have "manual" type and not to be already voided or cleared', IDH_ConsistencyViolation);
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht3wwDBGridFieldChanged(
  Sender: TObject; Field: TField);
begin
  inherited;
  if (Field.FieldName = 'CLEARED_DATE') or (Field.FieldName = 'CLEARED_AMOUNT') then
  begin
    wwdsDetail.Dataset['STATUS'] := BANK_REGISTER_STATUS_Cleared;
    tsht3wwDBGrid.RefreshDisplay;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.GetcdProcessed;
var
  sProcessFilter : String;
begin
    sProcessFilter := CreateFilterfromDataset(cdProcessedBarList,'CO_BANK_ACCOUNT_REGISTER_NBR','CO_BANK_ACCOUNT_REGISTER_NBR');
    if sProcessFilter <> '' then
    begin
      cdProcessed.Close;
      cdProcessed.ProviderName := DM_TEMPORARY.TMP_CO_BANK_ACCOUNT_REGISTER.ProviderName;
      cdProcessed.DataRequired(sProcessFilter);
    end;
end;




procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.pctlEDIT_SB_BANK_ACCOUNTSChange(
  Sender: TObject);
begin
  inherited;
  if pctlEDIT_SB_BANK_ACCOUNTS.ActivePage = tsht3 then
    CoordinateWithMaster
  else
    if pctlEDIT_SB_BANK_ACCOUNTS.ActivePage = tsht4 then
      tsht4BankName.Caption := StringReplace(wwdsMaster.DataSet['Bank_Name_Lookup'],'&','&&',[rfReplaceAll]) + ' - ' + wwdsMaster.DataSet['CUSTOM_BANK_ACCOUNT_NUMBER']
  else
    if pctlEDIT_SB_BANK_ACCOUNTS.ActivePage = tsht5 then
  begin
    tsht5BankName.Caption := StringReplace(wwdsMaster.DataSet['Bank_Name_Lookup'],'&','&&',[rfReplaceAll]) + ' - ' + wwdsMaster.DataSet['CUSTOM_BANK_ACCOUNT_NUMBER'];
    if tsht2CompFilterGroupRbAll.Checked then
      tsht5FilterParam.Caption := 'All Companies.'
    else
      tsht5FilterParam.Caption := 'Selected Companies.';
    if not tsht2DateFilterGroupRbAll.Checked then
      tsht5FilterParam.Caption := tsht5FilterParam.Caption + ' Date Range: ' + DateToStr(tsht2DateFilterGroupDateFrom.Date) + '-' +
        DateToStr(tsht2DateFilterGroupDateTill.Date);
  end
  else
  if pctlEDIT_SB_BANK_ACCOUNTS.ActivePage = tshtProc then
    GetcdProcessed;

  LockWindowUpdate(0);
end;


procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.pctlEDIT_SB_BANK_ACCOUNTSChanging(
  Sender: TObject; var AllowChange: Boolean);
begin
  inherited;
  Assert(LockWindowUpdate(Handle));
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.UpdatecdClientBarList(const Values: array of const);
var
  I: Integer;
begin
  cdClientBarList.Edit;
  for I := 2 to High(Values) do
      cdClientBarList.Fields[I].AssignValue(Values[I]);
  cdClientBarList.Post;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.btnMarkClearedClick(
  Sender: TObject);
var
  dateVar : variant;
  i, pChanged : Integer;
  bGroupIdentifierCount : Boolean;
  OneCheckList, GroupIdentifierList : IisStringList;
  dCoBankAccRegDetails, cdGroupIdentifier : TevClientDataSet;


    Procedure GetOneCheckList(Filter : String);
    begin
        Assert(DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger <> 0);
        ctx_DataAccess.OpenClient(DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger);
        DM_COMPANY.CO_BANK_ACC_REG_DETAILS.DataRequired(Filter);
        DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired(Filter);

        DM_COMPANY.CO_BANK_ACC_REG_DETAILS.First;
        while not DM_COMPANY.CO_BANK_ACC_REG_DETAILS.Eof do
        begin

          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Locate('CO_BANK_ACCOUNT_REGISTER_NBR',
                  VarArrayOf([ DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger]) ,[]);

          if not cdClientBarList.Locate('CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR',
                     VarArrayOf([DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CL_NBR').AsInteger,
                              DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger]) ,[]) then
               cdClientBarList.AppendRecord([DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CL_NBR').AsInteger,
                                               DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CLIENT_CO_BANK_ACC_REG_NBR').AsInteger,
                                                pChanged, dateVar,
                                                DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CLEARED_DATE').Value,
                                                GROUP_BOX_NO, GROUP_BOX_YES])
          else
               UpdatecdClientBarList([DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CL_NBR').AsInteger,
                                               DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CLIENT_CO_BANK_ACC_REG_NBR').AsInteger,
                                                pChanged, dateVar,
                                                DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.FieldByName('CLEARED_DATE').Value,
                                                GROUP_BOX_NO, GROUP_BOX_YES]);


          DM_COMPANY.CO_BANK_ACC_REG_DETAILS.Next;
        end;
    end;
              //   has to merge with   function BuildINsqlStatement
    function BuildINsqlStatementForString(const ALeftSide: String; const AItems: IisStringList): String;
    var
      s: String;
      i, iCounter: Integer;
    begin
      Result := '';
      s := '';
      iCounter := 0;
      for i := 0 to AItems.Count - 1 do
      begin
        if s <> '' then
          s := s + ',';
        s := s + '''' + AItems[i]+ '''';
        Inc(iCounter);
        if iCounter = 1490 then  // Firebird IN statement 1500 limitation
        begin
          AddStrValue(Result, ALeftSide + ' IN (' + s + ')', ' OR ');
          s := '';
          iCounter := 0;
        end;
      end;

      if s <> '' then
        AddStrValue(Result, ALeftSide + ' IN (' + s + ')', ' OR ');

      if Result <> '' then
        Result := '(' + Result + ')';
    end;

    Procedure GetGroupIdentifierList(Filter : String);
    var
      s : String;
    begin
        s := 'select CL_NBR, CO_BANK_ACCOUNT_REGISTER_NBR, GROUP_IDENTIFIER, CLEARED_DATE ' +
               ' from TMP_CO_BANK_ACCOUNT_REGISTER ' +
                 ' where AMOUNT <> 0 and GROUP_IDENTIFIER is not null and GROUP_IDENTIFIER <> '''' and ' +
                   Filter +
                 ' order by CL_NBR, CO_BANK_ACCOUNT_REGISTER_NBR ';
        with TExecDSWrapper.Create(s) do
        begin
            ctx_DataAccess.GetCustomData(cdGroupIdentifier, AsVariant);
        end;
        cdGroupIdentifier.First;
        while not cdGroupIdentifier.Eof do
        begin
          if not cdClientBarList.Locate('CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR',
                     VarArrayOf([cdGroupIdentifier.FieldByName('CL_NBR').AsInteger,
                              cdGroupIdentifier.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger]) ,[]) then
               cdClientBarList.AppendRecord([cdGroupIdentifier.FieldByName('CL_NBR').AsInteger,
                                               cdGroupIdentifier.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger,
                                                pChanged, dateVar,
                                                cdGroupIdentifier.FieldByName('CLEARED_DATE').Value,
                                                GROUP_BOX_NO, GROUP_BOX_YES])
           else
               UpdatecdClientBarList([cdGroupIdentifier.FieldByName('CL_NBR').AsInteger,
                                               cdGroupIdentifier.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger,
                                                pChanged, dateVar,
                                                cdGroupIdentifier.FieldByName('CLEARED_DATE').Value,
                                                GROUP_BOX_NO, GROUP_BOX_YES]);
          cdGroupIdentifier.Next;
        end;
    end;

begin
  inherited;

  if tsht3wwDBGrid.SelectedList.Count = 0 then
  begin
   EvMessage('Please select records from Grid.', mtInformation, [mbOK]);
   exit;
  end;

  UnselectBARGrid;

  wwdsDetail.DataSet.DisableControls;

  dateVar := Trunc(edDefaultCrearedDate.Date);
  pChanged := 1;
  if Sender = btnMarkUnCleared then
  begin
    pChanged := 3;
    dateVar := null;
  end;

  dCoBankAccRegDetails := TevClientDataSet.Create(nil);
  dCoBankAccRegDetails.ProviderName := DM_COMPANY.CO_BANK_ACC_REG_DETAILS.ProviderName;

  cdGroupIdentifier := TevClientDataSet.Create(nil);
  ctx_DataAccess.SkipPrCheckPostCheck := True;
  try
    wwdsDetail.DataSet.CheckBrowseMode;
    OneCheckList := TisStringList.Create;
    GroupIdentifierList := TisStringList.Create;
    bGroupIdentifierCount := false;

    cdClientBarList.Filter := '' ;
    cdClientBarList.Filtered := false;

    ctx_DataAccess.UnlockPRSimple;
    try
        for i := 0 to tsht3wwDBGrid.SelectedList.Count-1 do
        begin
          wwdsDetail.DataSet.GotoBookmark(tsht3wwDBGrid.SelectedList[i]);

          if (wwdsDetail.DataSet.FieldValues['STATUS'] <> BANK_REGISTER_TYPE_Consolidated) or
             (Length(Trim(wwdsDetail.DataSet.FieldByName('GROUP_IDENTIFIER').AsString)) > 0) then
          begin
            if Length(Trim(wwdsDetail.DataSet.FieldByName('GROUP_IDENTIFIER').AsString)) > 0 then
            begin
               if GroupIdentifierList.IndexOf(wwdsDetail.DataSet.FieldByName('GROUP_IDENTIFIER').AsString) =-1 then
               begin
                    GroupIdentifierList.Add(wwdsDetail.DataSet.FieldByName('GROUP_IDENTIFIER').AsString);
                    if not bGroupIdentifierCount then
                      bGroupIdentifierCount := wwdsDetail.DataSet.FieldByName('GroupIdentifierCount').AsInteger <> 1;
               end;
            end
            else
            if DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger = wwdsDetail.DataSet.FieldByName('CL_NBR').AsInteger then
               OneCheckList.Add(wwdsDetail.DataSet.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsString);

            if Length(Trim(wwdsDetail.DataSet.FieldByName('GROUP_IDENTIFIER').AsString)) = 0 then
            begin
              if not cdClientBarList.Locate('CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR',
                       VarArrayOf([wwdsDetail.DataSet.FieldByName('CL_NBR').AsInteger,
                                 wwdsDetail.DataSet.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger]) ,[]) then
                 cdClientBarList.AppendRecord([wwdsDetail.DataSet.FieldByName('CL_NBR').AsInteger,
                                               wwdsDetail.DataSet.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger,
                                                pChanged, dateVar,
                                                 wwdsDetail.DataSet.FieldByName('CLEARED_DATE').Value,
                                                 GROUP_BOX_NO, GROUP_BOX_YES])
              else
                 UpdatecdClientBarList([wwdsDetail.DataSet.FieldByName('CL_NBR').AsInteger,
                                               wwdsDetail.DataSet.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger,
                                                pChanged, dateVar,
                                                 wwdsDetail.DataSet.FieldByName('CLEARED_DATE').Value,
                                                 GROUP_BOX_NO, GROUP_BOX_YES]);
            end;

            if cbGridUpdates.Checked then
            begin
              wwdsDetail.DataSet.Edit;
              wwdsDetail.DataSet.FieldByName('RecordHide').AsString := GROUP_BOX_YES;
              wwdsDetail.DataSet.Post;
            end;
          end;
        end;
    finally
       try
         if GroupIdentifierList.Count > 0 then
         begin
           if bGroupIdentifierCount then
              bGroupIdentifierCount := not (EvMessage('There are other transactions attached to this item.  You will clear all of them. Are you sure?', mtConfirmation, [mbYes, mbNo]) = mrYes);
           if not bGroupIdentifierCount then
              GetGroupIdentifierList(BuildINsqlStatementForString('GROUP_IDENTIFIER', GroupIdentifierList));
         end;

         if OneCheckList.Count > 0 then
            GetOneCheckList(BuildINsqlStatement('CO_BANK_ACCOUNT_REGISTER_NBR', OneCheckList.CommaText));

         cdClientBarList.Filter := 'CHANGED in ( 1, 3 ) and NEWRECORD = ''' + GROUP_BOX_YES + '''' ;
         cdClientBarList.Filtered := True;

         if cdClientBarList.RecordCount > 0 then
         try
            ProcessBarRecords(cdClientBarList);
         except
            if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Active and (DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.ChangeCount > 0) then
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CancelUpdates;
            raise;
         end;
       finally
         ctx_DataAccess.LockPRSimple;
       end;
    end;

  finally
     ctx_DataAccess.SkipPrCheckPostCheck := False;
     dCoBankAccRegDetails.Free;
     cdGroupIdentifier.Free;

     if not cbGridUpdates.Checked then
        bRefResh := true;
     UpdatecdClientBarListRecord('NEWRECORD');

     pnlProc.Visible := True;
     tshtProc.TabVisible := true;

     CoordinateWithMaster;

     wwdsDetail.DataSet.EnableControls;
     tsht3wwDBGrid.UnselectAll;

  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.UpdatecdClientBarListRecord(const Field : String);
begin
     if (cdClientBarList.RecordCount> 0) and (Field <> '')   then
     begin
       cdClientBarList.First;
       while not cdClientBarList.Eof do
       begin
         cdClientBarList.Edit;
         cdClientBarList.FieldByName(Field).AsString := GROUP_BOX_NO;
         cdClientBarList.Post;
       end;
     end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.btnProcMarkClearedClick(
  Sender: TObject);
var
 i, pChanged: integer;
 dateVar : variant;

  procedure CheckRolledbackRecord;
  begin
     cdClientBarList.Filter := 'ROLLBACK_RECORD = ''' + GROUP_BOX_YES + '''';
     cdClientBarList.Filtered := True;
     UpdatecdClientBarListRecord('ROLLBACK_RECORD');
  end;

begin
  inherited;

   ctx_DataAccess.UnlockPRSimple;

   CheckRolledbackRecord;
   cdClientBarList.Filter := '';
   cdClientBarList.Filtered := false;

   dateVar := Trunc(edDefaultCrearedDate.Date);
   pChanged := 1;
   if Sender = btnProcMarkUnCleared then
   begin
     pChanged := 3;
     dateVar := null;
   end;

   ctx_DataAccess.SkipPrCheckPostCheck := True;
   try
     for i := 0 to evgrdProcessed.SelectedList.Count-1 do
     begin
        cdProcessed.GotoBookmark(evgrdProcessed.SelectedList[i]);

        if cdClientBarList.Locate('CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR',
                   VarArrayOf([cdProcessed.FieldByName('CL_NBR').AsInteger,
                                cdProcessed.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger]) ,[]) then
        begin
           if cdClientBarList.FieldByName('CHANGED').AsInteger <> pChanged then
           begin
             cdClientBarList.Edit;
             cdClientBarList.FieldByName('ROLLBACK_RECORD').AsString := GROUP_BOX_YES;
             cdClientBarList.FieldByName('CHANGED').AsInteger := pChanged;
             cdClientBarList.FieldByName('CLEARED_DATE').Value := dateVar;

             if (pChanged = 1) and
                 (cdClientBarList.FieldByName('ROLLBACK_CLEAR_DATE').Value <> null) then
                cdClientBarList.FieldByName('CLEARED_DATE').Value :=
                    cdClientBarList.FieldByName('ROLLBACK_CLEAR_DATE').Value;

             cdClientBarList.Post;
           end;
        end;
     end;

     cdClientBarList.Filter := 'CHANGED in ( 1, 3 ) and ROLLBACK_RECORD = ''' + GROUP_BOX_YES + '''';
     cdClientBarList.Filtered := True;
     if cdClientBarList.RecordCount > 0 then
     try
        if not cbGridUpdates.Checked then
           bRefResh := true;
        ProcessBarRecords(cdClientBarList);
        GetcdProcessed;
     except
        if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Active and (DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.ChangeCount > 0) then
          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CancelUpdates;
        raise;
     end;
   finally
     ctx_DataAccess.LockPRSimple;
     ctx_DataAccess.SkipPrCheckPostCheck := False;
   end;

end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.wwdsDetailStateChange(
  Sender: TObject);
var
  b: Boolean;
begin
  inherited;
  tsht3DbLookupManualACH.ReadOnly := not (wwdsDetail.State = dsInsert);
  tsht3DbLookupPrACH.ReadOnly := not (wwdsDetail.State = dsInsert);
  tsht3DbLookupTaxACH.ReadOnly := not (wwdsDetail.State = dsInsert);
  tsht3DbLookupManualACH.Enabled := (wwdsDetail.State = dsInsert);
  tsht3DbLookupPrACH.Enabled := (wwdsDetail.State = dsInsert);
  tsht3DbLookupTaxACH.Enabled := (wwdsDetail.State = dsInsert);

  if wwdsDetail.State in [dsEdit, dsInsert] then
  begin
    tsht3wwDBGrid.Options := tsht3wwDBGrid.Options - [dgMultiSelect];
  end
  else
  begin
    tsht3wwDBGrid.Options := tsht3wwDBGrid.Options + [dgMultiSelect];
  end;
  with Owner as TFramePackageTmpl do
  begin
    b := (wwdsDetail.State in [dsEdit, dsInsert]) or ((wwdsDetail.State in [dsBrowse]) and (TEvClientDataset(wwdsDetail.DataSet).ChangeCount > 0));
    SetButton(NavOK, b);
    SetButton(NavCancel, b);
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.lTMP_CO_BANK_ACCOUNT_REGISTERAfterInsert(
  DataSet: TDataSet);
begin
  inherited;
  DataSet['SB_BANK_ACCOUNTS_NBR'] := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS['SB_BANK_ACCOUNTS_NBR'];
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.lTMP_CO_BANK_ACCOUNT_REGISTERCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if lTMP_CO_BANK_ACCOUNT_REGISTERRecordHide.IsNull then
     lTMP_CO_BANK_ACCOUNT_REGISTERRecordHide.AsString :='N';

  if Length(Trim(lTMP_CO_BANK_ACCOUNT_REGISTER.FieldByName('GROUP_IDENTIFIER').AsString)) > 0 then
    if cdGroupIdentifierTotals.Locate('GROUP_IDENTIFIER',lTMP_CO_BANK_ACCOUNT_REGISTERGROUP_IDENTIFIER.AsString,[]) then
          lTMP_CO_BANK_ACCOUNT_REGISTERGroupIdentifierCount.AsInteger :=
                 cdGroupIdentifierTotals['GroupIdentifierCount'];

  if cdCoBankAccRegDetails.Active then
    if cdCoBankAccRegDetails.Locate('CL_NBR;CLIENT_CO_BANK_ACC_REG_NBR',
              VarArrayOf([lTMP_CO_BANK_ACCOUNT_REGISTERCL_NBR.AsInteger,
                            lTMP_CO_BANK_ACCOUNT_REGISTERCO_BANK_ACCOUNT_REGISTER_NBR.AsInteger]) ,[]) then
          lTMP_CO_BANK_ACCOUNT_REGISTEROneCheckBarNbr.AsInteger :=
                       cdCoBankAccRegDetails.fieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger;

  //  if tax payment co selected...
  if DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger =  lTMP_CO_BANK_ACCOUNT_REGISTERCL_NBR.AsInteger then
     lTMP_CO_BANK_ACCOUNT_REGISTEROneCheckBarNbr.AsInteger :=
            lTMP_CO_BANK_ACCOUNT_REGISTERCO_BANK_ACCOUNT_REGISTER_NBR.AsInteger
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.BarDataSetPost(DataSet: TDataSet);
var
  Handled: boolean;
begin
 Handled:=false;
 ButtonClicked(NavCommit, Handled);
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.lTMP_CO_BANK_ACCOUNT_REGISTERBeforePost(
  DataSet: TDataSet);
var
  OldClNbr : integer;
begin
  inherited;
  if VarIsNull(DataSet['cl_nbr']) or VarIsNull(DataSet['co_nbr']) then
    raise EUpdateError.CreateHelp('Client and company fields are required', IDH_ConsistencyViolation);
  if DataSet.State = dsInsert then
  begin
    oldClNbr := ctx_dbAccess.GetCurrentClientNbr;
    ctx_DataAccess.OpenClient(DataSet['cl_nbr']);
    try
      DataSet['STATUS_DATE'] := Now;
      DataSet['CO_BANK_ACCOUNT_REGISTER_NBR'] := ctx_DBAccess.GetNewKeyValue('CO_BANK_ACCOUNT_REGISTER');
    finally
      ctx_DataAccess.OpenClient(oldClNbr);
    end;
    if DataSet['MANUAL_TYPE'] = BANK_REGISTER_MANUALTYPE_None then
      raise EUpdateError.CreateHelp('Manual type needs to be set', IDH_ConsistencyViolation);
    if (DataSet.Tag = 0) and (DataSet['MANUAL_TYPE'] = BANK_REGISTER_MANUALTYPE_MadeByMistake_Void) then
      raise EUpdateError.CreateHelp('This manual type isn''t allowed', IDH_ConsistencyViolation);
    Assert(DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Locate('SB_BANK_ACCOUNTS_NBR', Dataset['SB_BANK_ACCOUNTS_NBR'], []));
  end;
  if VarIsNull(DataSet['TRANSACTION_EFFECTIVE_DATE']) then
    DataSet['TRANSACTION_EFFECTIVE_DATE'] := DataSet['CHECK_DATE'];
  {s := DataSet.FieldByName('FILLER').AsString;
  ReplaceInPosition(s, DataSet.FieldByName('CLEARED_DATE').AsString, 1, 10);
  DataSet.FieldByName('FILLER').AsString := s;}
  if not DataSet.ControlsDisabled then
    DataSet['status_date'] := Date;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  case Kind of
  NavOK, NavCommit:
    begin
      MyApplyUpdates;
      if Assigned(lTMP_CO_BANK_ACCOUNT_REGISTER.CustomAfterCommit) then
        lTMP_CO_BANK_ACCOUNT_REGISTER.CustomAfterCommit(lTMP_CO_BANK_ACCOUNT_REGISTER);
      Handled := True;
    end;
  NavCancel:
    begin
      wwdsDetail.DataSet.Cancel;
      TevClientDataset(wwdsDetail.DataSet).CancelUpdates;
      Handled := True;
      wwdsDetailStateChange(nil);
    end;

  NavHistory:
    ctx_DataAccess.OpenClient( lTMP_CO_BANK_ACCOUNT_REGISTER.FieldByName('CL_NBR').AsInteger );
  else
    inherited;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.AfterClick(Kind: Integer);
begin
  inherited;
  case Kind of
    NavInsert:
    begin
      if wwdsDetail.DataSet.State = dsInsert then
         wwdsDetail.DataSet.FieldByName('NOTES_POPULATED').AsString := GROUP_BOX_NO;
    end;
  end;
end;

function TEDIT_SB_BANK_ACCOUNT_REGISTER.GetInsertControl: TWinControl;
begin
  Result := tsht3DbLookupClient;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Close;
  AddDS([DM_SERVICE_BUREAU.SB_BANKS, DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS,
    DM_TEMPORARY.TMP_CL, DM_TEMPORARY.TMP_CO], SupportDataSets);
end;



procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.Activate;
var
  y, m, d: Word;
  a: TArrayDS;
  s: String;
begin
  inherited;
  if SecurityState = ctEnabledSRo then
  begin
     tsht4.TabVisible := false;
     SetControlsReccuring(tsht3, 'SecurityRO', Integer(True));
     (Owner as TFramePackageTmpl).SetButton(NavInsert, False);
     (Owner as TFramePackageTmpl).SetButton(NavDelete, False);
  end;

  FMyApplyUpdatesReconcileList := TStringList.Create;
  edDefaultCrearedDate.DateTime := Date;
  tsht3wwDBComboManualType.Items.Text := GroupBox_BANK_REGISTER_ManualType_ComboChoices;
  tsht3wwDBComboStatus.Items.Text := GroupBox_Bank_Register_Status_ComboChoices;
  tsht3wwDBComboType.Items.Text := GroupBox_Bank_Register_Type_ComboChoices;

  conTabManualType.Items.Text := GroupBox_BANK_REGISTER_ManualType_ComboChoices;
  conTabStatus.Items.Text := GroupBox_Bank_Register_Status_ComboChoices;
  conTabRegistryType.Items.Text := GroupBox_Bank_Register_Type_ComboChoices;

  procTabManualType.Items.Text := GroupBox_BANK_REGISTER_ManualType_ComboChoices;
  procTabStatus.Items.Text := GroupBox_Bank_Register_Status_ComboChoices;
  procTabRegistryType.Items.Text := GroupBox_Bank_Register_Type_ComboChoices;

  tsht2DateFilterGroupDateFrom.DateTime := GetBeginMonth(Date);
  tsht2DateFilterGroupDateTill.DateTime := GetEndMonth(Date);
  edMergeUpToDate.DateTime := tsht2DateFilterGroupDateTill.DateTime;
  DecodeDate(Date, y, m, d);
  if d <> 1 then
  begin
    Dec(m);
    if m = 0 then
    begin
      m := 12;
      Dec(y);
    end;
  end;
  editStorageReportDateFrom.DateTime := EncodeDate(y, m, 1);
  Inc(m);
  if m = 13 then
  begin
    m := 1;
    Inc(y);
  end;
  editStorageReportDateTill.DateTime := EncodeDate(y, m, 1) - 1;
  tsht2DateFilterGroupRbClick(nil);
  tsht2CompFilterGroupRbClick(nil);
  with ctx_DataAccess.TEMP_CUSTOM_VIEW do
  try
    Close;

    s := GetReadOnlyClintCompanyFilter(false,'T1.','T2.',false,true);
    with TExecDSWrapper.Create('GenericSelect2WithCondition') do
    begin
      SetMacro('Table1', 'TMP_CL');
      SetMacro('Table2', 'TMP_CO');
      SetMacro('Columns', 't2.cl_nbr, t2.co_nbr, T1.name cl_name, t1.custom_client_number cl_number, ' +
        'T2.name, t2.custom_company_number');
      SetMacro('JoinField', 'CL');
      SetMacro('CONDITION', s);
      DataRequest(AsVariant);
    end;
    Open;
    wwClientDataSetClCo.Data := Data;
  finally
    Close;
  end;
  wwClientDataSetCo.Data := DM_TEMPORARY.TMP_CO.Data;
  sCurrentFilter := '-';
  with Owner as TFramePackageTmpl do
  begin
    NavigationDataSet := lTMP_CO_BANK_ACCOUNT_REGISTER;
    DeleteDataSet := NavigationDataSet;
    SetLength(a, 0);
    AddDS(NavigationDataSet, a);
    InsertDataSets := a;
  end;

  cbGridUpdates.Checked := mb_AppSettings.AsBoolean['Settings\BARupdateOption']
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht2DateFilterGroupDateTillChange(
  Sender: TObject);
begin
  inherited;
  edMergeUpToDate.DateTime := tsht2DateFilterGroupDateTill.DateTime;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.rgSourceFileTypeClick(
  Sender: TObject);
begin
  inherited;
  cboStorageReportTransferType.ItemIndex := -1;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;
end;


procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.evRadioGroup1Click(
  Sender: TObject);
begin
  inherited;
  tsht5RadioSummaryType.Visible := (evRadioGroup1.ItemIndex = 0);
  tsht5RadioCreditDebit.Visible := (evRadioGroup1.ItemIndex = 0);
  tsht5RadioCompanyPageBreak.Visible := (evRadioGroup1.ItemIndex = 0);
  tsht5RadioTransactionStatus.Visible := (evRadioGroup1.ItemIndex = 0);
  evCheckBox1.Visible := tsht5RadioTransactionStatus.Visible;
  tsht5RadioSortBy.Visible := (evRadioGroup1.ItemIndex = 0);
  tsht5CompanyRadioOnlyWithTrans.Visible := (evRadioGroup1.ItemIndex = 0);
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.cbCompPosOrClick(Sender: TObject);
begin
  inherited;
  edCompPosOr.Visible := cbCompPosOr.Checked;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht3wwDBGridDblClick(
  Sender: TObject);
var
  ds: TevClientDataSet;
  s: string;
begin
  inherited;
  ds := TevDBGrid(Sender).DataSource.Dataset as TevClientDataSet;
  if ds <> nil then
  begin
    try
      ctx_DataAccess.OpenClient(ds['CL_NBR']);
      if not VarIsNull(ds['PR_CHECK_NBR']) then
      begin
        dm_payroll.PR_CHECK.DataRequired('PR_CHECK_NBR='+ VarToStr(ds['PR_CHECK_NBR']));
        if DM_PAYROLL.PR_CHECK.RecordCount <> 1 then
          raise EInconsistentData.CreateHelp('Referenced check does not exists in the client', IDH_InconsistentData);
        dm_payroll.PR.DataRequired('PR_NBR='+ dm_payroll.PR_CHECK.PR_NBR.AsString);
        dm_employee.ee.DataRequired('EE_NBR='+ dm_payroll.PR_CHECK.EE_NBR.AsString);
        dm_client.CL_PERSON.DataRequired('CL_PERSON_NBR='+ dm_employee.ee.CL_PERSON_NBR.AsString);
        s := 'Check '+ dm_payroll.PR_CHECK.PAYMENT_SERIAL_NUMBER.AsString+
          ' issued to '+ dm_client.CL_PERSON.FIRST_NAME.AsString+ ' '+ dm_client.CL_PERSON.LAST_NAME.AsString+ #13+
          'In payroll '+ dm_payroll.PR.CHECK_DATE.AsString+ '-'+ dm_payroll.PR.RUN_NUMBER.AsString;
      end
      else if not VarIsNull(ds['PR_MISCELLANEOUS_CHECKS_NBR']) then
      begin
        dm_payroll.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR='+ VarToStr(ds['PR_MISCELLANEOUS_CHECKS_NBR']));
        if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.RecordCount <> 1 then
          raise EInconsistentData.CreateHelp('Referenced check does not exists in the client', IDH_InconsistentData);
        dm_payroll.PR.DataRequired('PR_NBR='+ dm_payroll.PR_MISCELLANEOUS_CHECKS.PR_NBR.AsString);
        if dm_payroll.PR_MISCELLANEOUS_CHECKS.CL_AGENCY_NBR.IsNull then
        begin
          DM_SYSTEM.SY_GLOBAL_AGENCY.Activate;
          s := 'Misc Check '+ dm_payroll.PR_MISCELLANEOUS_CHECKS.PAYMENT_SERIAL_NUMBER.AsString+
            ' issued to system agency '+ DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(dm_payroll.PR_MISCELLANEOUS_CHECKS.SY_GLOBAL_AGENCY_NBR.AsInteger, 'AGENCY_NAME')+ #13+
            'In payroll '+ dm_payroll.PR.CHECK_DATE.AsString+ '-'+ dm_payroll.PR.RUN_NUMBER.AsString;
        end
        else
        begin
          dm_client.CL_AGENCY.DataRequired('CL_AGENCY_NBR='+ dm_payroll.PR_MISCELLANEOUS_CHECKS.CL_AGENCY_NBR.AsString);
          DM_SERVICE_BUREAU.SB_AGENCY.Activate;
          s := 'Misc Check '+ dm_payroll.PR_MISCELLANEOUS_CHECKS.PAYMENT_SERIAL_NUMBER.AsString+
            ' issued to S/B agency '+ DM_SERVICE_BUREAU.SB_AGENCY.ShadowDataSet.CachedLookup(dm_client.CL_AGENCY.SB_AGENCY_NBR.AsInteger, 'NAME')+ #13+
            'In payroll '+ dm_payroll.PR.CHECK_DATE.AsString+ '-'+ dm_payroll.PR.RUN_NUMBER.AsString;
        end;
      end
      else if not VarIsNull(ds['CO_TAX_PAYMENT_ACH_NBR']) then
      begin
        dm_company.CO_TAX_PAYMENT_ACH.DataRequired('CO_TAX_PAYMENT_ACH_NBR='+ VarToStr(ds['CO_TAX_PAYMENT_ACH_NBR']));
        if dm_company.CO_TAX_PAYMENT_ACH.RecordCount <> 1 then
          raise EInconsistentData.CreateHelp('Referenced ach does not exists in the client', IDH_InconsistentData);
        dm_company.CO_TAX_DEPOSITS.DataRequired('CO_TAX_DEPOSITS_NBR='+ dm_company.CO_TAX_PAYMENT_ACH.CO_TAX_DEPOSITS_NBR.AsString);
        ctx_DataAccess.Activate([DM_SYSTEM.SY_GLOBAL_AGENCY, DM_SYSTEM.SY_GL_AGENCY_FIELD_OFFICE]);
        s := 'Tax ACH '+ dm_company.CO_TAX_PAYMENT_ACH.ACH_DATE.AsString+
          ' reference '+ dm_company.CO_TAX_DEPOSITS.TAX_PAYMENT_REFERENCE_NUMBER.AsString+ #13+
          'For '+ DM_SYSTEM.SY_GLOBAL_AGENCY.ShadowDataSet.CachedLookup(
            StrToIntDef(DM_SYSTEM.SY_GL_AGENCY_FIELD_OFFICE.ShadowDataSet.CachedLookup(
            dm_company.CO_TAX_DEPOSITS.SY_GL_AGENCY_FIELD_OFFICE_NBR.AsInteger, 'SY_GLOBAL_AGENCY_NBR'), 0), 'AGENCY_NAME')+ ' - '+
            DM_SYSTEM.SY_GL_AGENCY_FIELD_OFFICE.ShadowDataSet.CachedLookup(
            dm_company.CO_TAX_DEPOSITS.SY_GL_AGENCY_FIELD_OFFICE_NBR.AsInteger, 'ADDITIONAL_NAME')
      end
      else if not VarIsNull(ds['CO_PR_ACH_NBR']) then
      begin
        dm_company.CO_PR_ACH.DataRequired('CO_PR_ACH_NBR='+ VarToStr(ds['CO_PR_ACH_NBR']));
        if dm_company.CO_PR_ACH.RecordCount <> 1 then
          raise EInconsistentData.CreateHelp('Referenced ach does not exists in the client', IDH_InconsistentData);
        dm_payroll.PR.DataRequired('PR_NBR='+ dm_company.CO_PR_ACH.PR_NBR.AsString);
        s := 'Payroll ACH '+ dm_company.CO_PR_ACH.ACH_DATE.AsString+ #13+
          'For payroll '+ dm_payroll.PR.CHECK_DATE.AsString+ '-'+ dm_payroll.PR.RUN_NUMBER.AsString;
      end
      else if not VarIsNull(ds['CO_MANUAL_ACH_NBR']) then
      begin
        dm_company.CO_MANUAL_ACH.DataRequired('CO_MANUAL_ACH_NBR='+ VarToStr(ds['CO_MANUAL_ACH_NBR']));
        if dm_company.CO_MANUAL_ACH.RecordCount <> 1 then
          raise EInconsistentData.CreateHelp('Referenced ach does not exists in the client', IDH_InconsistentData);
        s := 'Manual ACH '+ dm_company.CO_MANUAL_ACH.PURPOSE_NOTES.AsString;
      end
      else
        s := 'There is no information available';
    finally
//TODO?      DisconnectFromSpdServer;
    end;
    if s <> '' then
      EvMessage(s);

    EnableTmpAchLookups;

  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.OnMyApplyUpdatesReconcile(
  const Field: TField; const OldValue, NewValue: Integer);
begin
  if Field.FieldName = 'CO_BANK_ACCOUNT_REGISTER_NBR' then
    FMyApplyUpdatesReconcileList.Append(MakeString([ctx_DataAccess.ClientID, OldValue, NewValue]));
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.DeActivate;
begin
  inherited;
  FreeAndNil(FMyApplyUpdatesReconcileList);
  mb_AppSettings.AsBoolean['Settings\BARupdateOption'] := cbGridUpdates.Checked;


end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.bOpenRegisterClick(
  Sender: TObject);
begin
  inherited;
  sCurrentFilter := '';
  bRefResh := false;
  tsht3.TabVisible := true;
  pctlEDIT_SB_BANK_ACCOUNTS.ActivatePage(tsht3);
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.btnNotesClick(Sender: TObject);
var frmNotes: TMemoEditor;
    Notes, sql: String;
    Q: IevQuery;
begin
  inherited;

  if ctx_DataAccess.ClientID <>  wwdsDetail.DataSet.FieldByName('CL_NBR').AsInteger then
     ctx_DataAccess.OpenClient(wwdsDetail.DataSet.FieldByName('CL_NBR').AsInteger);

  sql:= 'select a.NOTES from CO_BANK_ACCOUNT_REGISTER a ' +
      'where {AsOfNow<a>} and a.CO_BANK_ACCOUNT_REGISTER_NBR = :nbr ';
  Q := TevQuery.Create(sql);
  Q.Params.AddValue('nbr', wwdsDetail.DataSet.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger);
  Q.Execute;

  frmNotes := TMemoEditor.Create(Self);
  frmNotes.Text := TBlobField(Q.Result.Fields[0]).AsString;
  if frmNotes.ShowModal = mrOK then begin
    Notes := Trim(frmNotes.Text);
    wwdsDetail.Edit;
    TBlobField(wwdsDetail.DataSet.FieldByName('NOTES')).AsString := Notes;
    if Length(Notes) > 0 then
     wwdsDetail.DataSet.FieldByName('NOTES_POPULATED').AsString := GROUP_BOX_YES
    else
     wwdsDetail.DataSet.FieldByName('NOTES_POPULATED').AsString := GROUP_BOX_NO;

    wwdsDetail.DataSet.FieldByName('NotesUpdated').AsString := GROUP_BOX_YES;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht3wwDBGridMultiSelectRecord(
  Grid: TwwDBGrid; Selecting: Boolean; var Accept: Boolean);
begin
  inherited;

  if Selecting then btnNotes.Enabled := tsht3wwDBGrid.SelectedList.Count = 0;
  if not(Selecting) then btnNotes.Enabled := tsht3wwDBGrid.SelectedList.Count = 2;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht3wwDBGridRowChanged(
  Sender: TObject);
begin
  inherited;
  DisableTmpAchLookups;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.EnableTmpAchLookups;

  procedure Process(ds: TDataSet);
  begin
    ds.Filter := 'CL_NBR = '
      + tsht3DbLookupClient.LookupValue
      + ' and CO_NBR = '
      + tsht3DbLookupCompany.LookupValue;
    ds.Filtered := True;
    TEvClientDataSet(ds).DataRequired(ds.Filter);
  end;

begin
  if (tsht3DbLookupClient.LookupValue <> '') and (tsht3DbLookupCompany.LookupValue <> '') then
    with DM_TEMPORARY do
    begin
      Process(TMP_CO_MANUAL_ACH);
      Process(TMP_CO_PR_ACH);
      Process(TMP_CO_TAX_PAYMENT_ACH);

      tsht3DbLookupManualACH.ReadOnly := not (wwdsDetail.State = dsInsert);
      tsht3DbLookupPrACH.ReadOnly := not (wwdsDetail.State = dsInsert);
      tsht3DbLookupTaxACH.ReadOnly := not (wwdsDetail.State = dsInsert);

      tsht3DbLookupManualACH.Enabled := (wwdsDetail.State = dsInsert);
      tsht3DbLookupPrACH.Enabled := (wwdsDetail.State = dsInsert);
      tsht3DbLookupTaxACH.Enabled := (wwdsDetail.State = dsInsert);


//      tsht3DbLookupManualACH.Enabled := True;
//      tsht3DbLookupPrACH.Enabled := True;
//      tsht3DbLookupTaxACH.Enabled := True;
    end
  else
    DisableTmpAchLookups;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.DisableTmpAchLookups;
begin
  with DM_TEMPORARY do
  begin
    TMP_CO_MANUAL_ACH.Close;
    TMP_CO_PR_ACH.Close;
    TMP_CO_TAX_PAYMENT_ACH.Close;

//    tsht3DbLookupManualACH.Enabled := False;
//    tsht3DbLookupPrACH.Enabled := False;
//    tsht3DbLookupTaxACH.Enabled := False;
    tsht3DbLookupManualACH.ReadOnly := not (wwdsDetail.State = dsInsert);
    tsht3DbLookupPrACH.ReadOnly := not (wwdsDetail.State = dsInsert);
    tsht3DbLookupTaxACH.ReadOnly := not (wwdsDetail.State = dsInsert);
    tsht3DbLookupManualACH.Enabled := (wwdsDetail.State = dsInsert);
    tsht3DbLookupPrACH.Enabled := (wwdsDetail.State = dsInsert);
    tsht3DbLookupTaxACH.Enabled := (wwdsDetail.State = dsInsert);

  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.UnselectBARGrid;
begin
  if tsht3wwDBGrid.SelectedList.Count = 1 then
  begin
    tsht3wwDBGrid.UnselectAll;
    tsht3wwDBGrid.SelectRecord;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.OnGetSecurityStates(const States: TSecurityStateList);
const
  secScreenRo: TAtomContextRecord = (Tag: ctEnabledSRo; Caption: 'ReadOnly'; IconName: ''; Priority: 200);
begin
  with secScreenRo do
    States.InsertAfter(ctEnabled, Tag[1], Caption, IconName);
end;


procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.wwdsDetailDataChange(
  Sender: TObject; Field: TField);
var
  PaymentType: String;
begin
  inherited;
  PaymentType := lTMP_CO_BANK_ACCOUNT_REGISTER.FieldByName('MANUAL_TYPE').AsString;

  tsht3DbLookupClient.Enabled := not
  ((PaymentType = BANK_REGISTER_MANUALTYPE_None) or
   (PaymentType = BANK_REGISTER_MANUALTYPE_NonSystemCheck) or
   (PaymentType = BANK_REGISTER_MANUALTYPE_SystemCheck) or
   (PaymentType = BANK_REGISTER_MANUALTYPE_Other) or
   (PaymentType = BANK_REGISTER_MANUALTYPE_WireTransfer)
  );

  tsht3DbLookupCompany.Enabled := tsht3DbLookupClient.Enabled;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.CreditHoldBtnClick(
  Sender: TObject);
var
  UtilHelper: TEDIT_HOLD_STATUS_UPDATE;
  LibStatus, CreditHold: string;
  i, R: Integer;
begin
  UtilHelper := TEDIT_HOLD_STATUS_UPDATE.Create(Self);
  try
    UtilHelper.Caption := 'Set NSF and Credit Hold';
    UtilHelper.evcbCreditHold.Items.Text := Credit_Hold_Level_ComboChoices;
    UtilHelper.evcbCreditHold.MapList := True;
    UtilHelper.evcbCreditHold.Value := CREDIT_HOLD_LEVEL_NONE;

    UtilHelper.evcbLiabStatus.Items.Text := CreditHoldAndStatus_ComboChoices;
    UtilHelper.evcbLiabStatus.MapList := True;
    UtilHelper.evcbLiabStatus.Value := TAX_DEPOSIT_STATUS_IMPOUNDED;

    repeat
      R := UtilHelper.ShowModal;
      CreditHold := UtilHelper.evcbCreditHold.Value;
      LibStatus := UtilHelper.evcbLiabStatus.Value;

      if R = mrOk then // update credit hold
      begin
        ctx_StartWait;
        try
          for i := 0 to tsht3wwDBGrid.SelectedList.Count - 1 do
          begin
            lTMP_CO_BANK_ACCOUNT_REGISTER.GotoBookmark(tsht3wwDBGrid.SelectedList.Items[I]);
            ctx_DataAccess.OpenClient(lTMP_CO_BANK_ACCOUNT_REGISTER.FieldByName('CL_NBR').AsInteger);
            DM_COMPANY.CO.DataRequired('CO_NBR=' + lTMP_CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_NBR').AsString);
            if ConvertNull(DM_COMPANY.CO['CREDIT_HOLD'],'') <> CreditHold then
            begin
              DM_COMPANY.CO.Edit;
              DM_COMPANY.CO['CREDIT_HOLD'] := CreditHold;
              DM_COMPANY.CO.Post;
            end;
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO]);
          end;
        finally
          ctx_EndWait;
        end;
      end
      else
      if R = mrYes then // update status
      begin
        ctx_StartWait;
        try
          for i := 0 to tsht3wwDBGrid.SelectedList.Count - 1 do
          begin
            lTMP_CO_BANK_ACCOUNT_REGISTER.GotoBookmark(tsht3wwDBGrid.SelectedList.Items[I]);
            ctx_DataAccess.OpenClient(lTMP_CO_BANK_ACCOUNT_REGISTER.FieldByName('CL_NBR').AsInteger);
            DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.DataRequired('CO_BANK_ACCOUNT_REGISTER_NBR='+
              lTMP_CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsString);

            if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.STATUS.AsString = BANK_REGISTER_STATUS_Pending then
            begin
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Edit;
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.STATUS.AsString := BANK_REGISTER_STATUS_Outstanding;
              DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.Post;
              lTMP_CO_BANK_ACCOUNT_REGISTER.Edit;
              lTMP_CO_BANK_ACCOUNT_REGISTER.FieldByName('STATUS').AsString := BANK_REGISTER_STATUS_Outstanding;
              lTMP_CO_BANK_ACCOUNT_REGISTER.Post;
              lTMP_CO_BANK_ACCOUNT_REGISTER.MergeChangeLog;
              (Owner as TFramePackageTmpl).ClickButton(NavOK);
              (Owner as TFramePackageTmpl).ClickButton(NavCommit);
            end;

            if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.REGISTER_TYPE.AsString = BANK_REGISTER_TYPE_TaxImpound then
            begin

              if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.AsString <> '' then
              begin
                DM_COMPANY.CO_PR_ACH.DataRequired('CO_PR_ACH_NBR='+
                  DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.AsString);

                DM_COMPANY.CO_FED_TAX_LIABILITIES.DataRequired('PR_NBR='+
                  DM_COMPANY.CO_PR_ACH.PR_NBR.AsString);

                DM_COMPANY.CO_FED_TAX_LIABILITIES.First;
                while not DM_COMPANY.CO_FED_TAX_LIABILITIES.Eof do
                begin
                  if DM_COMPANY.CO_FED_TAX_LIABILITIES.TAX_TYPE.AsString <> TAX_LIABILITY_TYPE_COBRA_CREDIT then
                  begin
                    DM_COMPANY.CO_FED_TAX_LIABILITIES.Edit;

                    if (LibStatus = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED)
                    or (LibStatus = TAX_DEPOSIT_STATUS_IMPOUNDED) then
                      DM_COMPANY.CO_FED_TAX_LIABILITIES.IMPOUNDED.AsString := GROUP_BOX_YES
                    else
                    if (LibStatus <> TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED)
                    and (LibStatus <> TAX_DEPOSIT_STATUS_IMPOUNDED) then
                      DM_COMPANY.CO_FED_TAX_LIABILITIES.IMPOUNDED.AsString := GROUP_BOX_NO;

                    DM_COMPANY.CO_FED_TAX_LIABILITIES.STATUS.AsString := LibStatus;
                    DM_COMPANY.CO_FED_TAX_LIABILITIES.Post;
                  end;
                  DM_COMPANY.CO_FED_TAX_LIABILITIES.Next;
                end;
              end;

              if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.AsString <> '' then
              begin
                DM_COMPANY.CO_PR_ACH.DataRequired('CO_PR_ACH_NBR='+
                  DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.AsString);
                DM_COMPANY.CO_STATE_TAX_LIABILITIES.DataRequired('PR_NBR='+
                  DM_COMPANY.CO_PR_ACH.PR_NBR.AsString);
                DM_COMPANY.CO_STATE_TAX_LIABILITIES.First;
                while not DM_COMPANY.CO_STATE_TAX_LIABILITIES.Eof do
                begin
                  DM_COMPANY.CO_STATE_TAX_LIABILITIES.Edit;
                  if (LibStatus = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED)
                  or (LibStatus = TAX_DEPOSIT_STATUS_IMPOUNDED) then
                    DM_COMPANY.CO_STATE_TAX_LIABILITIES.IMPOUNDED.AsString := GROUP_BOX_YES
                  else
                  if (LibStatus <> TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED)
                  and (LibStatus <> TAX_DEPOSIT_STATUS_IMPOUNDED) then
                    DM_COMPANY.CO_STATE_TAX_LIABILITIES.IMPOUNDED.AsString := GROUP_BOX_NO;

                  DM_COMPANY.CO_STATE_TAX_LIABILITIES['STATUS'] := LibStatus;
                  DM_COMPANY.CO_STATE_TAX_LIABILITIES.Post;
                  DM_COMPANY.CO_STATE_TAX_LIABILITIES.Next;
                end;
              end;

              if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.AsString <> '' then
              begin
                DM_COMPANY.CO_PR_ACH.DataRequired('CO_PR_ACH_NBR='+
                  DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.AsString);
                DM_COMPANY.CO_SUI_LIABILITIES.DataRequired('PR_NBR='+
                  DM_COMPANY.CO_PR_ACH.PR_NBR.AsString);
                DM_COMPANY.CO_SUI_LIABILITIES.First;
                while not DM_COMPANY.CO_SUI_LIABILITIES.Eof do
                begin
                  DM_COMPANY.CO_SUI_LIABILITIES.Edit;
                  if (LibStatus = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED)
                  or (LibStatus = TAX_DEPOSIT_STATUS_IMPOUNDED) then
                    DM_COMPANY.CO_SUI_LIABILITIES.IMPOUNDED.AsString := GROUP_BOX_YES
                  else
                  if (LibStatus <> TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED)
                  and (LibStatus <> TAX_DEPOSIT_STATUS_IMPOUNDED) then
                    DM_COMPANY.CO_SUI_LIABILITIES.IMPOUNDED.AsString := GROUP_BOX_NO;

                  DM_COMPANY.CO_SUI_LIABILITIES['STATUS'] := LibStatus;
                  DM_COMPANY.CO_SUI_LIABILITIES.Post;
                  DM_COMPANY.CO_SUI_LIABILITIES.Next;
                end;
              end;

              if DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.AsString <> '' then
              begin
                DM_COMPANY.CO_PR_ACH.DataRequired('CO_PR_ACH_NBR='+
                  DM_COMPANY.CO_BANK_ACCOUNT_REGISTER.CO_PR_ACH_NBR.AsString);
                DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.DataRequired('PR_NBR='+
                  DM_COMPANY.CO_PR_ACH.PR_NBR.AsString);
                DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.First;
                while not DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Eof do
                begin
                  DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Edit;
                  if (LibStatus = TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED)
                  or (LibStatus = TAX_DEPOSIT_STATUS_IMPOUNDED) then
                    DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.IMPOUNDED.AsString := GROUP_BOX_YES
                  else
                  if (LibStatus <> TAX_DEPOSIT_STATUS_MANUALLY_IMPOUNDED)
                  and (LibStatus <> TAX_DEPOSIT_STATUS_IMPOUNDED) then
                    DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.IMPOUNDED.AsString := GROUP_BOX_NO;

                  DM_COMPANY.CO_LOCAL_TAX_LIABILITIES['STATUS'] := LibStatus;
                  DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Post;
                  DM_COMPANY.CO_LOCAL_TAX_LIABILITIES.Next;
                end;
              end;
            end;
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO, DM_COMPANY.CO_FED_TAX_LIABILITIES, DM_COMPANY.CO_STATE_TAX_LIABILITIES,
                          DM_COMPANY.CO_SUI_LIABILITIES, DM_COMPANY.CO_LOCAL_TAX_LIABILITIES,
                          DM_COMPANY.CO_BANK_ACCOUNT_REGISTER]);
          end;
        finally
          ctx_EndWait;
        end;
      end;
      if (R = mrOk) or (R = mrYes) then
      begin
        if EvMessage('Would you like to make any other changes?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
          R := mrCancel;
      end;
    until (R = mrCancel);
  finally
    UtilHelper.Free;
  end;
end;

function TEDIT_SB_BANK_ACCOUNT_REGISTER.CreateFilterfromDataset(cd: TevClientDataSet; const fName , rName : String):string;
var
  pClNbr : integer;
  BarList : IisStringList;

  function CreateFilter(const filter :string):string;
  begin
   result := filter;
   if result <> '' then
       result := Result + ' or ';
       result := Result +  ' (CL_NBR = ' + intToStr(pClNbr) + ' and ' +
                  BuildINsqlStatement(rName, BarList.CommaText) + ') ';
  end;

begin
   result := '';
   BarList := TisStringList.Create;
   pClNbr := -1;
   cd.First;
   while not cd.Eof do
   begin
     if pClNbr <> cd.FieldByName('CL_NBR').AsInteger then
     begin
       if pClNbr <> -1 then
       begin
          result := CreateFilter(result);
          BarList.Clear;
       end;
       pClNbr := cd.FieldByName('CL_NBR').AsInteger;
     end;
     BarList.Add(cd.FieldByName(fName).AsString);
     cd.Next;
   end;
   if BarList.Count > 0 then
     result := CreateFilter(result);
end;


procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.getcdConsolidatedBar(const filter : string; const filtertype : integer);
var
  cdTaxDepList : TevClientDataSet;

  sfilter, sfiltertaxdep : string;
  ConDepNbr : integer;

  currentcl_nbr : integer;
  fCL_NBR, fPR_MISCELLANEOUS_CHECKS_NBR,  fCO_TAX_PAYMENT_ACH_NBR,
  fCO_TAX_DEPOSITS_NBR,       fCO_BANK_ACCOUNT_REGISTER_NBR : TField;

  procedure SetcdTaxDepList;
  begin
      cdTaxDepList.FieldDefs.Add('CL_NBR', ftInteger, 0, True);
      cdTaxDepList.FieldDefs.Add('CO_TAX_DEPOSITS_NBR', ftInteger, 0, True);
      cdTaxDepList.CreateDataSet;
      cdTaxDepList.LogChanges := False;
      cdTaxDepList.AddIndex('idxDepNbr', 'CL_NBR;CO_TAX_DEPOSITS_NBR', []);
      cdTaxDepList.IndexDefs.Update;
      cdTaxDepList.IndexName := 'idxDepNbr';
  end;

  procedure getConsolidatedCompanyList;
  begin
        if currentcl_nbr <> fCL_NBR.AsInteger then
        begin
          Assert(fCL_NBR.AsInteger <> 0);
          ctx_DataAccess.OpenClient(fCL_NBR.AsInteger);
          currentcl_nbr := fCL_NBR.AsInteger;
        end;

        sfiltertaxdep := '';
        ConDepNbr := -1;
        if not fPR_MISCELLANEOUS_CHECKS_NBR.IsNull then
        begin
          DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired('PR_MISCELLANEOUS_CHECKS_NBR = ' + fPR_MISCELLANEOUS_CHECKS_NBR.AsString);
          if (DM_COMPANY.PR_MISCELLANEOUS_CHECKS.RecordCount > 0) and
             (not DM_COMPANY.PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').isNull) then
            ConDepNbr := DM_COMPANY.PR_MISCELLANEOUS_CHECKS.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
        end
        else
        if not fCO_TAX_PAYMENT_ACH_NBR.IsNull then
        begin
          DM_PAYROLL.CO_TAX_PAYMENT_ACH.DataRequired('CO_TAX_PAYMENT_ACH_NBR = ' + fCO_TAX_PAYMENT_ACH_NBR.AsString);
          if (DM_PAYROLL.CO_TAX_PAYMENT_ACH.RecordCount > 0) and
             (not DM_COMPANY.CO_TAX_PAYMENT_ACH.FieldByName('CO_TAX_DEPOSITS_NBR').isNull) then
             ConDepNbr := DM_COMPANY.CO_TAX_PAYMENT_ACH.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
        end
        else
        if not fCO_TAX_DEPOSITS_NBR.IsNull then
           ConDepNbr := fCO_TAX_DEPOSITS_NBR.AsInteger;

        if (ConDepNbr <> -1) and (not cdTaxDepList.Locate('CL_NBR;CO_TAX_DEPOSITS_NBR',
                                  VarArrayOf([fCL_NBR.AsInteger ,ConDepNbr]), [])) then
          cdTaxDepList.AppendRecord([fCL_NBR.AsInteger, ConDepNbr]);
  end;

var
 s : String;

begin
     currentcl_nbr := 0;
     if filtertype = 1 then
       sfilter := filter
     else
     if filtertype = 2 then
     begin
        Assert(DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger <> 0);
        ctx_DataAccess.OpenClient(DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsInteger);
        DM_COMPANY.CO_BANK_ACC_REG_DETAILS.DataRequired(Filter);

        cdTaxDepList := TevClientDataSet.Create(nil);
        try
         SetcdTaxDepList;
         DM_COMPANY.CO_BANK_ACC_REG_DETAILS.IndexName :='CL_NBR;CLIENT_CO_BANK_ACC_REG_NBR';
         DM_COMPANY.CO_BANK_ACC_REG_DETAILS.First;
         sfilter := '(CL_NBR = ' + DM_SERVICE_BUREAU.SB.FieldByName('SB_CL_NBR').AsString + ' and ' +
                       ' CO_BANK_ACCOUNT_REGISTER_NBR = ' + DM_COMPANY.CO_BANK_ACC_REG_DETAILS.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsString  + ') ';
         s := CreateFilterfromDataset(DM_COMPANY.CO_BANK_ACC_REG_DETAILS,'CLIENT_CO_BANK_ACC_REG_NBR','CO_BANK_ACCOUNT_REGISTER_NBR');
         if s <> '' then
             sfilter := sfilter + ' or ';
         sfilter := sfilter + s;
         cdConsolidatedBar.Close;
         cdConsolidatedBar.ProviderName := DM_TEMPORARY.TMP_CO_BANK_ACCOUNT_REGISTER.ProviderName;
         cdConsolidatedBar.DataRequired(sfilter);

         cdConsolidatedBar.First;
         fCL_NBR :=  cdConsolidatedBar.FieldByName('CL_NBR');
         fPR_MISCELLANEOUS_CHECKS_NBR :=  cdConsolidatedBar.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR');
         fCO_TAX_PAYMENT_ACH_NBR := cdConsolidatedBar.FieldByName('CO_TAX_PAYMENT_ACH_NBR');
         fCO_TAX_DEPOSITS_NBR := cdConsolidatedBar.FieldByName('CO_TAX_DEPOSITS_NBR');
         fCO_BANK_ACCOUNT_REGISTER_NBR := cdConsolidatedBar.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR');

         while not cdConsolidatedBar.Eof do
         begin
          if Assigned(fPR_MISCELLANEOUS_CHECKS_NBR) and Assigned(fCO_TAX_PAYMENT_ACH_NBR) and
             Assigned(fCO_TAX_DEPOSITS_NBR) and Assigned(fCO_BANK_ACCOUNT_REGISTER_NBR) then
              getConsolidatedCompanyList;

          cdConsolidatedBar.Next;
         end;

         s := CreateFilterfromDataset(cdTaxDepList,'CO_TAX_DEPOSITS_NBR','CO_TAX_DEPOSITS_NBR');
         if s <> '' then
            sfilter := sfilter + ' or ';
         sfilter := sfilter + s;
        finally
         cdTaxDepList.Free;
        end;
     end
     else
     begin
        cdTaxDepList := TevClientDataSet.Create(nil);
        try
          SetcdTaxDepList;
          if filtertype = 3 then
          begin
            fCL_NBR :=  wwdsDetail.DataSet.FieldByName('CL_NBR');
            fPR_MISCELLANEOUS_CHECKS_NBR :=  DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR');
            fCO_TAX_PAYMENT_ACH_NBR := DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_PAYMENT_ACH_NBR');
            fCO_TAX_DEPOSITS_NBR := DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_TAX_DEPOSITS_NBR');
            fCO_BANK_ACCOUNT_REGISTER_NBR := DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR');
          end
          else
          begin
            fCL_NBR :=  wwdsDetail.DataSet.FieldByName('CL_NBR');
            fPR_MISCELLANEOUS_CHECKS_NBR :=  wwdsDetail.DataSet.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR');
            fCO_TAX_PAYMENT_ACH_NBR := wwdsDetail.DataSet.FieldByName('CO_TAX_PAYMENT_ACH_NBR');
            fCO_TAX_DEPOSITS_NBR := wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR');
            fCO_BANK_ACCOUNT_REGISTER_NBR := wwdsDetail.DataSet.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR');
          end;
          if Assigned(fPR_MISCELLANEOUS_CHECKS_NBR) and Assigned(fCO_TAX_PAYMENT_ACH_NBR) and
             Assigned(fCO_TAX_DEPOSITS_NBR) and Assigned(fCO_BANK_ACCOUNT_REGISTER_NBR) then
             getConsolidatedCompanyList;
          sfilter := '(CL_NBR = ' + fCL_NBR.AsString + ' and CO_BANK_ACCOUNT_REGISTER_NBR = ' + fCO_BANK_ACCOUNT_REGISTER_NBR.AsString + ' ) ';
          if  cdTaxDepList.RecordCount > 0 then
             sfilter := sfilter + ' or ' + CreateFilterfromDataset(cdTaxDepList,'CO_TAX_DEPOSITS_NBR','CO_TAX_DEPOSITS_NBR');
        finally
         cdTaxDepList.Free;
        end;
     end;

     cdConsolidatedBar.Close;
     cdConsolidatedBar.ProviderName := DM_TEMPORARY.TMP_CO_BANK_ACCOUNT_REGISTER.ProviderName;
     cdConsolidatedBar.DataRequired(sfilter);
end;


procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.evRelatedTransactionClick(
  Sender: TObject);
begin
  inherited;
  if tsht3wwDBGrid.SelectedList.Count = 1 then
  begin
    wwdsDetail.DataSet.GotoBookmark(tsht3wwDBGrid.SelectedList[0]);

    if Length(Trim(wwdsDetail.DataSet.FieldByName('GROUP_IDENTIFIER').AsString)) > 0 then
        getcdConsolidatedBar('GROUP_IDENTIFIER is not null and GROUP_IDENTIFIER <> '''' and  GROUP_IDENTIFIER = ''' +
                wwdsDetail.DataSet.FieldByName('GROUP_IDENTIFIER').AsString + '''', 1)
    else
    if wwdsDetail.DataSet.FieldByName('OneCheckBarNbr').AsInteger > 0 then
        getcdConsolidatedBar('CO_BANK_ACCOUNT_REGISTER_NBR = ' + wwdsDetail.DataSet.FieldByName('OneCheckBarNbr').AsString, 2)
    else
    if wwdsDetail.DataSet.FieldByName('REGISTER_TYPE').AsString[1] =  BANK_REGISTER_TYPE_Consolidated then
    begin
       if not wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').IsNull then
       begin
           ctx_DataAccess.OpenClient(wwdsDetail.DataSet.FieldByName('CL_NBR').AsInteger);

           DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.DataRequired(
                   'CO_TAX_DEPOSITS_NBR = ' + wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsString +
                ' and REGISTER_TYPE <> ''' + BANK_REGISTER_TYPE_Consolidated + '''');
           if DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.RecordCount > 0 then
           begin
              DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.First;
              if cdCoBankAccRegDetails.Active and cdCoBankAccRegDetails.Locate('CL_NBR;CLIENT_CO_BANK_ACC_REG_NBR',
                       VarArrayOf([wwdsDetail.DataSet.FieldByName('CL_NBR').AsInteger,
                              DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsInteger]) ,[]) then
                  getcdConsolidatedBar('CO_BANK_ACCOUNT_REGISTER_NBR = ' + cdCoBankAccRegDetails.fieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsString, 2)
              else
                  getcdConsolidatedBar('CO_BANK_ACCOUNT_REGISTER_NBR = ' + DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsString, 3)
           end
           else
           begin
                 DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.DataRequired(
                       'CO_TAX_DEPOSITS_NBR = ' + wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsString );
                 if DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.RecordCount > 0 then
                 begin
                    DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.DataRequired(
                           'PR_MISCELLANEOUS_CHECKS_NBR = ' + DM_PAYROLL.PR_MISCELLANEOUS_CHECKS.FieldByName('PR_MISCELLANEOUS_CHECKS_NBR').AsString +
                        ' and REGISTER_TYPE <> ''' + BANK_REGISTER_TYPE_Consolidated + '''');

                    if DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.RecordCount > 0 then
                       getcdConsolidatedBar('CO_BANK_ACCOUNT_REGISTER_NBR = ' + DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsString, 3)
                 end
                 else
                 begin
                   DM_PAYROLL.CO_TAX_PAYMENT_ACH.DataRequired(
                         'CO_TAX_DEPOSITS_NBR = ' + wwdsDetail.DataSet.FieldByName('CO_TAX_DEPOSITS_NBR').AsString );
                   if DM_PAYROLL.CO_TAX_PAYMENT_ACH.RecordCount > 0 then
                   begin
                      DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.DataRequired(
                             'CO_TAX_PAYMENT_ACH_NBR = ' + DM_PAYROLL.CO_TAX_PAYMENT_ACH.fieldByName('CO_TAX_PAYMENT_ACH_NBR') .AsString +
                          ' and REGISTER_TYPE <> ''' + BANK_REGISTER_TYPE_Consolidated + '''');

                      if DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.RecordCount > 0 then
                         getcdConsolidatedBar('CO_BANK_ACCOUNT_REGISTER_NBR = ' + DM_PAYROLL.CO_BANK_ACCOUNT_REGISTER.FieldByName('CO_BANK_ACCOUNT_REGISTER_NBR').AsString, 3)
                   end
                 end;
           end;
       end;
    end
    else
    begin
        getcdConsolidatedBar('', 4)
    end;

    tshtCon.TabVisible := true;
    pctlEDIT_SB_BANK_ACCOUNTS.ActivatePage(tshtCon);
  end
  else EvMessage('Please select one record from Grid.', mtInformation, [mbOK]);
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tshtConExit(Sender: TObject);
begin
  inherited;
  tshtCon.TabVisible := False;

end;




procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.tsht3wwDBGridFilterChange(
  Sender: TObject; DataSet: TDataSet; NewFilter: String);
var
 sAnd : string;
begin
  inherited;
  if cbGridUpdates.Checked then
  begin
    sAnd := '';
    if Length(Trim(DataSet.Filter)) > 0 then
       sAnd := ' and ';
    DataSet.Filter := DataSet.Filter  + sAnd +  ' RecordHide = ''N'' ';
    wwdsDetail.DataSet.Filtered := True;
  end;

end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.evrbCompCodeClick(
  Sender: TObject);
begin
  inherited;
  edCompCodeLength.MaxValue := 20;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.evrbCompNameClick(
  Sender: TObject);
begin
  inherited;
  edCompCodeLength.MaxValue := 15;
  if edCompCodeLength.Value > 15 then
     edCompCodeLength.Value := 15;
end;

procedure TEDIT_SB_BANK_ACCOUNT_REGISTER.ReplaceInPosition(var s: string;
  const news: string; const iPos, iLen: Word);
begin
  s := Copy(s, 1, iPos - 1) + Copy(News + StringOfChar(' ', iLen), 1, iLen) + Copy(s, iPos + iLen, 2048);
end;

initialization
  RegisterClass(TEDIT_SB_BANK_ACCOUNT_REGISTER);

end.
