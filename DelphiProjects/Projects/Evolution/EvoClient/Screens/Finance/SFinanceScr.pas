// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SFinanceScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls, Graphics,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList, EvCommonInterfaces,
  EvMainboard, ISBasicClasses, EvUIComponents, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton;

type
  TFinanceFrm = class(TFramePackageTmpl, IevFinanceScreens)
  protected
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;
  end;

implementation

var
  FinanceFrm: TFinanceFrm;

{$R *.DFM}

{ TAdminFrm }

function GetFinanceScreens: IevFinanceScreens;
begin
  if not Assigned(FinanceFrm) then
    FinanceFrm := TFinanceFrm.Create(nil);
  Result := FinanceFrm;

end;

function TFinanceFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TFinanceFrm.PackageCaption: string;
begin
  Result := '&Finance';
end;

function TFinanceFrm.PackageSortPosition: string;
begin
  Result := '008';
end;

procedure TFinanceFrm.UserPackageStructure;
begin
  AddStructure('Bank Accounts|100', 'TEDIT_SB_BANK_ACCOUNTS');
  AddStructure('Bank Account Register|150', 'TEDIT_SB_BANK_ACCOUNT_REGISTER');
  AddStructure('Export to A/R|160', 'TEDIT_SB_EXPORT_AR');
  AddStructure('Sales Tax States|200', 'TEDIT_SB_SALES_TAX_STATES');
  AddStructure('Services|300', 'TEDIT_SB_SERVICES');
  AddStructure('Services Calculations|400', 'TEDIT_SB_SERVICES_CALCULATIONS');
  AddStructure('Global Billing|500', 'TMASS_BILLING');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetFinanceScreens, IevFinanceScreens, 'Screens Finance');

finalization
  FinanceFrm.Free;

end.
 