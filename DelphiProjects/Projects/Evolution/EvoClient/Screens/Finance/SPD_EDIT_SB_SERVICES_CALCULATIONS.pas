// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_SERVICES_CALCULATIONS;

interface

uses SFrameEntry, Db,   Dialogs, DBCtrls, SysUtils, 
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Wwdatsrc, SDataStructure, SDDClasses, SDataDictbureau,
  ISBasicClasses, EvExceptions, EvUIComponents, EvUtils, EvClientDataSet;

type
  TEDIT_SB_SERVICES_CALCULATIONS = class(TFrameEntry)
    PageControl1: TevPageControl;
    tshtBrowse: TTabSheet;
    tshtCalculation_Details: TTabSheet;
    lablMinimum_Quantity: TevLabel;
    lablBegin_Date1: TevLabel;
    lablMaximum_Quantity: TevLabel;
    lablPer_Item_Rate: TevLabel;
    lablFlat_Amount: TevLabel;
    lablFuture_Flat_Amount: TevLabel;
    lablBegin_Date4: TevLabel;
    dtxtSERVICE_NAME: TevDBText;
    lablSalesTaxState: TevLabel;
    wwDBGrid1: TevDBGrid;
    Splitter1: TevSplitter;
    Panel1: TevPanel;
    wwDBGrid2: TevDBGrid;
    wwDBGrid3: TevDBGrid;
    Bevel1: TEvBevel;
    wwdeFuture_Flat_Amount_Begin_Date: TevDBDateTimePicker;
    wwdeFuture_Minimum_Begin_Date: TevDBDateTimePicker;
    wwdeFuture_Maximum_Begin_Date: TevDBDateTimePicker;
    wwdpFuture_Per_Item_Begin_Date: TevDBDateTimePicker;
    wwdeFlat_Amount: TevDBEdit;
    wwdeMinimum_Quantity: TevDBEdit;
    wwdeMaximum_Quantity: TevDBEdit;
    wwdePer_Item_Rate: TevDBEdit;
    wwdeNext_Flat_Amount: TevDBEdit;
    wwdeNext_Minimum_Quantity: TevDBEdit;
    wwdeNext_Maximum_Quantity: TevDBEdit;
    wwdeNext_Per_Item_Rate: TevDBEdit;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evBitBtn1: TevBitBtn;
    evBitBtn2: TevBitBtn;
    evBitBtn3: TevBitBtn;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure evBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Activate; override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_SERVICES_CALCULATIONS: TEDIT_SB_SERVICES_CALCULATIONS;

implementation

{$R *.DFM}


uses
  SPackageEntry;


procedure TEDIT_SB_SERVICES_CALCULATIONS.ButtonClicked(Kind: Integer; var Handled: Boolean);
begin
  inherited;
  if (Kind in [NavOK,NavCommit]) then
  begin
    if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    begin
      if (Trim(wwdeFlat_Amount.Text) = '') then
          raise EUpdateError.CreateHelp('Flat Amount can''t be empty.', IDH_ConsistencyViolation);
    end;      
  end;
end;

function TEDIT_SB_SERVICES_CALCULATIONS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_SERVICES_CALCULATIONS;
end;

function TEDIT_SB_SERVICES_CALCULATIONS.GetInsertControl: TWinControl;
begin
  Result := wwdeFlat_Amount;
end;

procedure TEDIT_SB_SERVICES_CALCULATIONS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SERVICE_BUREAU.SB_SERVICES, SupportDataSets);
end;

procedure TEDIT_SB_SERVICES_CALCULATIONS.Button1Click(Sender: TObject);
begin
  inherited;
  wwdsDetail.Edit;
  wwdeMaximum_Quantity.Field.AsFloat := 999999999;
end;

procedure TEDIT_SB_SERVICES_CALCULATIONS.Button2Click(Sender: TObject);
begin
  inherited;
  wwdsDetail.Edit;
  wwdeNext_Maximum_Quantity.Field.AsFloat := 999999999;
end;

procedure TEDIT_SB_SERVICES_CALCULATIONS.evBitBtn1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_SB_SERVICES');
end;

procedure TEDIT_SB_SERVICES_CALCULATIONS.Activate;
begin
  inherited;
  wwdsMaster.DoDataChange(Self, nil);
end;

initialization
  RegisterClass(TEDIT_SB_SERVICES_CALCULATIONS);

end.
