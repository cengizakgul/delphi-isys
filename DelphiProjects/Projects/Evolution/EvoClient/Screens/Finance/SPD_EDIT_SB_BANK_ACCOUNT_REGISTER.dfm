inherited EDIT_SB_BANK_ACCOUNT_REGISTER: TEDIT_SB_BANK_ACCOUNT_REGISTER
  Height = 450
  object pctlEDIT_SB_BANK_ACCOUNTS: TevPageControl [0]
    Left = 8
    Top = 19
    Width = 689
    Height = 438
    HelpContext = 21014
    ActivePage = tsht3
    TabOrder = 0
    OnChange = pctlEDIT_SB_BANK_ACCOUNTSChange
    OnChanging = pctlEDIT_SB_BANK_ACCOUNTSChanging
    object tsht1: TTabSheet
      Caption = 'Browse'
      object wwdgTeam: TevDBGrid
        Left = 0
        Top = 0
        Width = 681
        Height = 410
        DisableThemesInTitle = False
        Selected.Strings = (
          'Bank_Name_Lookup'#9'25'#9'Bank name lookup'#9'F'
          'CUSTOM_BANK_ACCOUNT_NUMBER'#9'10'#9'Custom bank account #'#9'F'
          'BANK_ACCOUNT_TYPE'#9'1'#9'Type'#9'F'
          'NAME_DESCRIPTION'#9'15'#9'Description')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_BANK_ACCOUNT_REGISTER\wwdgTeam'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        OnRowChanged = wwdgTeamRowChanged
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsMaster
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tsht2: TTabSheet
      Caption = 'Filter'
      object tsht2CompFilterGroup: TevGroupBox
        Left = 0
        Top = 0
        Width = 345
        Height = 388
        Caption = 'Company filter'
        TabOrder = 0
        object tsht2CompFilterGroupGrid: TevDBGrid
          Left = 9
          Top = 72
          Width = 321
          Height = 307
          DisableThemesInTitle = False
          Selected.Strings = (
            'CUSTOM_COMPANY_NUMBER'#9'5'#9'Company #'#9'F'
            'NAME'#9'15'#9'Company name'#9'F'
            'CL_NUMBER'#9'5'#9'Client #'#9'F'
            'CL_NAME'#9'15'#9'Client name'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TEDIT_SB_BANK_ACCOUNT_REGISTER\tsht2CompFilterGroupGrid'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = wwDataSourceClCo
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 2
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object tsht2CompFilterGroupRbAll: TevRadioButton
          Left = 8
          Top = 16
          Width = 113
          Height = 17
          Caption = 'All companies'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = tsht2CompFilterGroupRbClick
        end
        object tsht2CompFilterGroupRbSelect: TevRadioButton
          Left = 8
          Top = 48
          Width = 113
          Height = 17
          Caption = 'Select companies'
          TabOrder = 1
          OnClick = tsht2CompFilterGroupRbClick
        end
      end
      object tsht2DateFilterGroup: TevGroupBox
        Left = 360
        Top = 0
        Width = 219
        Height = 328
        Caption = 'Date filter'
        TabOrder = 1
        object tsht2DateFilterGroupRbAll: TevRadioButton
          Left = 8
          Top = 17
          Width = 89
          Height = 17
          Caption = 'Do not filter'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = tsht2DateFilterGroupRbClick
        end
        object tsht2DateFilterGroupRbPeriod: TevRadioButton
          Left = 9
          Top = 41
          Width = 72
          Height = 17
          Caption = 'Filter'
          TabOrder = 1
          OnClick = tsht2DateFilterGroupRbClick
        end
        object tsht2DateFilterGroupDateFrom: TevDateTimePicker
          Left = 121
          Top = 14
          Width = 89
          Height = 21
          Date = 36481.709159259300000000
          Time = 36481.709159259300000000
          TabOrder = 2
        end
        object tsht2DateFilterGroupDateTill: TevDateTimePicker
          Left = 121
          Top = 38
          Width = 89
          Height = 21
          Date = 36481.709159259300000000
          Time = 36481.709159259300000000
          TabOrder = 3
          OnChange = tsht2DateFilterGroupDateTillChange
        end
        object gbDateFilterOptions: TRadioGroup
          Left = 12
          Top = 71
          Width = 198
          Height = 117
          Caption = 'Date filter options (effective in Register)'
          ItemIndex = 0
          Items.Strings = (
            'Use Effective date'
            'Use Check date'
            'Use Cleared date'
            'Use Check and Cleared dates')
          TabOrder = 4
        end
        object gbClearedFilterOptons: TevRadioGroup
          Left = 12
          Top = 192
          Width = 199
          Height = 126
          Caption = 'Status filter options'
          ItemIndex = 0
          Items.Strings = (
            'Any'
            'Cleared Only'
            'Not Cleared Only'
            'Pending'
            'Write Off')
          TabOrder = 5
        end
      end
      object bOpenRegister: TevButton
        Left = 360
        Top = 361
        Width = 168
        Height = 25
        Caption = 'Open Register With Filter'
        TabOrder = 2
        OnClick = bOpenRegisterClick
        Color = clBlack
        Margin = 0
      end
      object cbGridUpdates: TevCheckBox
        Left = 360
        Top = 334
        Width = 249
        Height = 17
        Caption = 'View Updates on Processed Records Tab Only'
        TabOrder = 3
      end
    end
    object tsht3: TTabSheet
      Caption = 'Register'
      ImageIndex = 2
      TabVisible = False
      object CO_MANUAL_ACH_NBR: TevLabel
        Left = 204
        Top = 208
        Width = 106
        Height = 13
        Caption = 'Company manual ACH'
        FocusControl = tsht3DbLookupManualACH
      end
      object l1: TevLabel
        Left = 8
        Top = 208
        Width = 68
        Height = 13
        Caption = 'Client #, name'
        FocusControl = tsht3DbLookupClient
      end
      object Label1: TevLabel
        Left = 8
        Top = 256
        Width = 86
        Height = 13
        Caption = 'Company #, name'
        FocusControl = tsht3DbLookupCompany
      end
      object Label2: TevLabel
        Left = 204
        Top = 256
        Width = 102
        Height = 13
        Caption = 'Company payroll ACH'
        FocusControl = tsht3DbLookupPrACH
      end
      object Label3: TevLabel
        Left = 400
        Top = 208
        Width = 129
        Height = 13
        Caption = 'Company tax payment ACH'
        FocusControl = tsht3DbLookupTaxACH
      end
      object tsht3wwDBGrid: TevDBGrid
        Left = 0
        Top = 0
        Width = 673
        Height = 193
        DisableThemesInTitle = False
        ControlType.Strings = (
          'STATUS;CustomEdit;tsht3wwDBComboStatus;F'
          'REGISTER_type;CustomEdit;tsht3wwDBComboType;F'
          'MANUAL_TYPE;CustomEdit;tsht3wwDBComboManualType;F')
        Selected.Strings = (
          'CLCustNum'#9'9'#9'CL #'#9'F'
          'CoCustNum'#9'12'#9'CO #'#9'F'
          'CONAME'#9'40'#9'Co Name'#9'F'
          'CHECK_SERIAL_NUMBER'#9'11'#9'Check serial #'#9'F'
          'CHECK_DATE'#9'10'#9'Check date'#9'F'
          'STATUS'#9'5'#9'Status'#9'F'
          'TRANSACTION_EFFECTIVE_DATE'#9'11'#9'Effective date'#9'F'
          'AMOUNT'#9'10'#9'Amount'#9'F'
          'CLEARED_AMOUNT'#9'12'#9'Cleared amount'#9'F'
          'CLEARED_DATE'#9'10'#9'Cleared date'#9'F'
          'REGISTER_type'#9'8'#9'Type'#9'F'
          'MANUAL_TYPE'#9'9'#9'Payment Type'#9'F'
          'PROCESS_DATE'#9'10'#9'Process date'#9'F'
          'BANK_ACCOUNT_REGISTER_NAME'#9'15'#9'Name for Cash Management'#9'F'
          'CLNAME'#9'40'#9'Client Name'#9'F'
          'TRACE_NUMBER'#9'15'#9'Trace number'#9'F'
          'GROUP_IDENTIFIER'#9'15'#9'Group Identifier'#9'F'
          'NOTES_POPULATED'#9'1'#9'Notes'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_BANK_ACCOUNT_REGISTER\tsht3wwDBGrid'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        OnMultiSelectRecord = tsht3wwDBGridMultiSelectRecord
        OnRowChanged = tsht3wwDBGridRowChanged
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        KeyOptions = [dgEnterToTab, dgAllowInsert]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = tsht3wwDBGridDblClick
        OnKeyDown = tsht3wwDBGridKeyDown
        OnFieldChanged = tsht3wwDBGridFieldChanged
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        OnFilterChange = tsht3wwDBGridFilterChange
        NoFire = False
      end
      object tsht3DbLookupManualACH: TevDBLookupCombo
        Left = 204
        Top = 224
        Width = 170
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'CO_MANUAL_ACH_NBR'#9'10'#9'Nbr'#9'F'
          'DEBIT_DATE'#9'30'#9'Date'#9'F')
        DataField = 'CO_MANUAL_ACH_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_TMP_CO_MANUAL_ACH.TMP_CO_MANUAL_ACH
        LookupField = 'CO_MANUAL_ACH_NBR'
        Style = csDropDownList
        Enabled = False
        TabOrder = 3
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object tsht3DbLookupClient: TevDBLookupCombo
        Left = 8
        Top = 224
        Width = 170
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'CUSTOM_CLIENT_NUMBER'#9'20'#9'Custom client #'#9'F'
          'NAME'#9'40'#9'Name'#9'F')
        DataField = 'CL_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_TMP_CL.TMP_CL
        LookupField = 'CL_NBR'
        Style = csDropDownList
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        ShowMatchText = True
        OnChange = tsht3DbLookupClientChange
      end
      object tsht3DbLookupPrACH: TevDBLookupCombo
        Left = 204
        Top = 272
        Width = 170
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'CO_PR_ACH_NBR'#9'10'#9'Nbr'#9'F'
          'ACH_DATE'#9'30'#9'Date'#9'F')
        DataField = 'CO_PR_ACH_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_TMP_CO_PR_ACH.TMP_CO_PR_ACH
        LookupField = 'CO_PR_ACH_NBR'
        Style = csDropDownList
        Enabled = False
        TabOrder = 4
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object tsht3DbLookupTaxACH: TevDBLookupCombo
        Left = 400
        Top = 224
        Width = 170
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'CO_TAX_PAYMENT_ACH_NBR'#9'10'#9'Nbr'#9'F'
          'ACH_DATE'#9'30'#9'Date'#9'F')
        DataField = 'CO_TAX_PAYMENT_ACH_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_TMP_CO_TAX_PAYMENT_ACH.TMP_CO_TAX_PAYMENT_ACH
        LookupField = 'CO_TAX_PAYMENT_ACH_NBR'
        Style = csDropDownList
        Enabled = False
        TabOrder = 5
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object tsht3DbLookupCompany: TevDBLookupCombo
        Left = 8
        Top = 272
        Width = 170
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'CUSTOM_COMPANY_NUMBER'#9'20'#9'Custom company #'#9'F'
          'NAME'#9'40'#9'Name'#9'F')
        DataField = 'CO_NBR'
        DataSource = wwdsDetail
        LookupTable = wwClientDataSetCo
        LookupField = 'CO_NBR'
        Style = csDropDownList
        TabOrder = 2
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
        ShowMatchText = True
        OnChange = tsht3DbLookupCompanyChange
      end
      object tsht3wwDBComboManualType: TevDBComboBox
        Left = 424
        Top = 56
        Width = 121
        Height = 21
        TabStop = False
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'MANUAL_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 6
        UnboundDataType = wwDefault
      end
      object tsht3wwDBComboType: TevDBComboBox
        Left = 424
        Top = 104
        Width = 121
        Height = 21
        TabStop = False
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'REGISTER_type'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object tsht3wwDBComboStatus: TevDBComboBox
        Left = 424
        Top = 80
        Width = 121
        Height = 21
        TabStop = False
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'STATUS'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 8
        UnboundDataType = wwDefault
      end
      object GroupBox1: TevGroupBox
        Left = 8
        Top = 304
        Width = 281
        Height = 79
        Caption = 'Additional info'
        TabOrder = 9
        object Label10: TevLabel
          Left = 8
          Top = 16
          Width = 54
          Height = 13
          Caption = 'Status date'
        end
        object DBEdit1: TevDBEdit
          Left = 96
          Top = 16
          Width = 177
          Height = 21
          DataField = 'STATUS_DATE'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object CreditHoldBtn: TButton
          Left = 96
          Top = 46
          Width = 177
          Height = 25
          Caption = 'Credit Hold and Status Update'
          TabOrder = 1
          OnClick = CreditHoldBtnClick
        end
      end
      object GroupBox2: TevGroupBox
        Left = 296
        Top = 304
        Width = 281
        Height = 81
        Caption = 'Clearing'
        TabOrder = 10
        object Label11: TevLabel
          Left = 8
          Top = 16
          Width = 96
          Height = 13
          Caption = 'Default cleared date'
        end
        object edDefaultCrearedDate: TevDBDateTimePicker
          Left = 120
          Top = 16
          Width = 153
          Height = 21
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          Epoch = 1950
          ShowButton = True
          TabOrder = 0
        end
      end
      object btnMarkCleared: TevBitBtn
        Left = 304
        Top = 352
        Width = 129
        Height = 25
        Caption = 'Mark cleared (F7)'
        TabOrder = 11
        OnClick = btnMarkClearedClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDFFFDDDDDFFFDDDD6668DDDDD66
          68DDDD888FDDDF888FDDDEEE68DDD6EEE68DDD8DD8FDD8DDD8DDDDEEE68DDE8D
          DE8DDDD8DD8FD8DDD8DDDDDEEE68DE8DDE8DDDDD8DD8F8FFF8DDDDDDEEE68E66
          6E8DDDDDD8DD8F888DDDDDDDDEEE68EEE8DDDDDFFF8DD8FDDDDDDD6668EEE68D
          DDDDDDF888F8DD8FDDDDD6EEE68EEE68DDDDDD8DDD8D8DD8FDDDDE8DDE8DEEE6
          8DDDDD8DDD8DD8DD8FDDDE8DDE8DDEEE68DDDD8FFF8DDD8DD8DDDE666E8DDDEE
          E68DDDD888DDDDD888DDDDEEE8DDDDDEEE8DDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object btnMarkUncleared: TevBitBtn
        Left = 440
        Top = 352
        Width = 129
        Height = 25
        Caption = 'Mark Outstanding (F6)'
        TabOrder = 12
        OnClick = btnMarkClearedClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDFFFDDDDDDFFFDDD3338DDDDD33
          38DDDD888FDDDDF888FDDBBB38DDD3BBB38DDDD888FDDD8DDD8DDDBBB38DDB8D
          DB8DDDDD888FDD8DDD8DDDDBBB38DB8DDB8DDDDDD888FD8FFF8DDDDDBBB38B33
          3B8DDDDDDD888FD888DDDDDDDBBB38BBB8DDDDDFFFD888FDDDDDDD3338BBB38D
          DDDDDDF888FD888FDDDDD3BBB38BBB38DDDDDD8DDD8DD888FDDDDB8DDB8DBBB3
          8DDDDD8DDD8DDD888FDDDB8DDB8DDBBB38DDDD8FFF8DDDD888FDDB333B8DDDBB
          B38DDDD888DDDDDD888DDDBBB8DDDDDBBB8DDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object Button1: TevBitBtn
        Left = 584
        Top = 352
        Width = 91
        Height = 25
        Caption = 'Void trans.'
        TabOrder = 13
        OnClick = Button1Click
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD00DDDDDD0
          DDDDDDDDFDDDDDDFDDDDDD0040DDDD040DDDDDDF8FDDDDF8FDDDD004F400004F
          400DDDF888F88F888F8D0C4FFF4004FFF4CDDD88888FF88888DDCCC4FFF44FFF
          4CCD8DD8888888888D8DD0004FFFFFF4CCCDD88D88888888D88D887774FFFF4D
          DDDD8D88D888888DDDDD887704FFFF40DDDD8D88F888888FDDDD88804FFFFFF4
          0DDD8DDF88888888FDDDD804FFF44FFF40DDD8F8888888888FDD884FFF4774FF
          F40D8D88888DD88888DD8874F4E6784F4CC088D888D8DD888D8D887E4EE678C4
          CCCC88DD8D88D8D8DDD8887EEEE678CCCCCD88D8D888D88D8D8D7877777778DD
          CCDD88DDDDDDD8DD88DDD88888888DDDCDDDD88888888DDD8DDD}
        NumGlyphs = 2
        Margin = 0
      end
      object btnNotes: TevBitBtn
        Left = 584
        Top = 313
        Width = 91
        Height = 25
        Caption = 'Notes'
        TabOrder = 14
        OnClick = btnNotesClick
        Color = clBlack
        Margin = 0
      end
      object evRelatedTransaction: TevBitBtn
        Left = 584
        Top = 269
        Width = 91
        Height = 25
        Caption = 'Consolidated List'
        TabOrder = 15
        OnClick = evRelatedTransactionClick
        Color = clBlack
        Margin = 0
      end
    end
    object tshtCon: TTabSheet
      Caption = 'Consolidated'
      ImageIndex = 5
      TabVisible = False
      OnExit = tshtConExit
      object evgrdConsolidate: TevDBGrid
        Left = 0
        Top = 0
        Width = 681
        Height = 410
        DisableThemesInTitle = False
        ControlType.Strings = (
          'MANUAL_TYPE;CustomEdit;conTabManualType;F'
          'STATUS;CustomEdit;conTabStatus;F'
          'REGISTER_type;CustomEdit;conTabRegistryType;F')
        Selected.Strings = (
          'CLCustNum'#9'15'#9'CL #'#9'F'#9
          'CoCustNum'#9'20'#9'CO #'#9'F'#9
          'CONAME'#9'40'#9'Co Name'#9'F'#9
          'CHECK_SERIAL_NUMBER'#9'10'#9'Check serial #'#9'F'#9
          'CHECK_DATE'#9'10'#9'Check date'#9'F'#9
          'STATUS'#9'1'#9'Status'#9'F'#9
          'STATUS_DATE'#9'10'#9'Status date'#9'F'#9
          'TRANSACTION_EFFECTIVE_DATE'#9'10'#9'Transaction Effective Date'#9#9
          'AMOUNT'#9'10'#9'Amount'#9'F'#9
          'CLEARED_AMOUNT'#9'10'#9'Cleared amount'#9'F'#9
          'CLEARED_DATE'#9'10'#9'Cleared date'#9'F'#9
          'REGISTER_type'#9'1'#9'Register type'#9'F'#9
          'MANUAL_TYPE'#9'1'#9'Manual type'#9'F'#9
          'GROUP_IDENTIFIER'#9'15'#9'Group Identifier'#9'F'#9
          'PROCESS_DATE'#9'10'#9'Process date'#9'F'#9
          'PR_CHECK_NBR'#9'10'#9'Pr Check Nbr'#9#9
          'SB_BANK_ACCOUNTS_NBR'#9'10'#9'Sb Bank Accounts Nbr'#9#9
          'CO_TAX_DEPOSITS_NBR'#9'10'#9'Co Tax Deposits Nbr'#9'F'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_BANK_ACCOUNT_REGISTER\evgrdConsolidate'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsConsolidate
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object conTabManualType: TevDBComboBox
        Left = 424
        Top = 56
        Width = 121
        Height = 21
        TabStop = False
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'MANUAL_TYPE'
        DataSource = dsConsolidate
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 1
        UnboundDataType = wwDefault
      end
      object conTabStatus: TevDBComboBox
        Left = 424
        Top = 80
        Width = 121
        Height = 21
        TabStop = False
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'STATUS'
        DataSource = dsConsolidate
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object conTabRegistryType: TevDBComboBox
        Left = 424
        Top = 104
        Width = 121
        Height = 21
        TabStop = False
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'REGISTER_type'
        DataSource = dsConsolidate
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
    end
    object tshtProc: TTabSheet
      Caption = 'Processed Records'
      ImageIndex = 6
      TabVisible = False
      object pnlProc: TPanel
        Left = 0
        Top = 375
        Width = 681
        Height = 35
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object btnProcMarkCleared: TevBitBtn
          Left = 111
          Top = 6
          Width = 129
          Height = 25
          Caption = 'Mark cleared'
          TabOrder = 0
          OnClick = btnProcMarkClearedClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDFFFDDDDDFFFDDDD6668DDDDD66
            68DDDD888FDDDF888FDDDEEE68DDD6EEE68DDD8DD8FDD8DDD8DDDDEEE68DDE8D
            DE8DDDD8DD8FD8DDD8DDDDDEEE68DE8DDE8DDDDD8DD8F8FFF8DDDDDDEEE68E66
            6E8DDDDDD8DD8F888DDDDDDDDEEE68EEE8DDDDDFFF8DD8FDDDDDDD6668EEE68D
            DDDDDDF888F8DD8FDDDDD6EEE68EEE68DDDDDD8DDD8D8DD8FDDDDE8DDE8DEEE6
            8DDDDD8DDD8DD8DD8FDDDE8DDE8DDEEE68DDDD8FFF8DDD8DD8DDDE666E8DDDEE
            E68DDDD888DDDDD888DDDDEEE8DDDDDEEE8DDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object btnProcMarkUncleared: TevBitBtn
          Left = 290
          Top = 6
          Width = 129
          Height = 25
          Caption = 'Mark Outstanding'
          TabOrder = 1
          OnClick = btnProcMarkClearedClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDFFFDDDDDDFFFDDD3338DDDDD33
            38DDDD888FDDDDF888FDDBBB38DDD3BBB38DDDD888FDDD8DDD8DDDBBB38DDB8D
            DB8DDDDD888FDD8DDD8DDDDBBB38DB8DDB8DDDDDD888FD8FFF8DDDDDBBB38B33
            3B8DDDDDDD888FD888DDDDDDDBBB38BBB8DDDDDFFFD888FDDDDDDD3338BBB38D
            DDDDDDF888FD888FDDDDD3BBB38BBB38DDDDDD8DDD8DD888FDDDDB8DDB8DBBB3
            8DDDDD8DDD8DDD888FDDDB8DDB8DDBBB38DDDD8FFF8DDDD888FDDB333B8DDDBB
            B38DDDD888DDDDDD888DDDBBB8DDDDDBBB8DDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
      end
      object evgrdProcessed: TevDBGrid
        Left = 0
        Top = 0
        Width = 681
        Height = 375
        DisableThemesInTitle = False
        ControlType.Strings = (
          'MANUAL_TYPE;CustomEdit;procTabManualType;F'
          'STATUS;CustomEdit;procTabStatus;F'
          'REGISTER_type;CustomEdit;procTabRegistryType;F')
        Selected.Strings = (
          'CLCustNum'#9'15'#9'CL #'#9'F'#9
          'CoCustNum'#9'20'#9'CO #'#9'F'#9
          'CONAME'#9'40'#9'Co Name'#9'F'#9
          'CHECK_SERIAL_NUMBER'#9'10'#9'Check serial #'#9'F'#9
          'CHECK_DATE'#9'10'#9'Check date'#9'F'#9
          'STATUS'#9'1'#9'Status'#9'F'#9
          'STATUS_DATE'#9'10'#9'Status date'#9'F'#9
          'TRANSACTION_EFFECTIVE_DATE'#9'10'#9'Transaction Effective Date'#9#9
          'AMOUNT'#9'10'#9'Amount'#9'F'#9
          'CLEARED_AMOUNT'#9'10'#9'Cleared amount'#9'F'#9
          'CLEARED_DATE'#9'10'#9'Cleared date'#9'F'#9
          'REGISTER_type'#9'1'#9'Register type'#9'F'#9
          'MANUAL_TYPE'#9'1'#9'Manual type'#9'F'#9
          'GROUP_IDENTIFIER'#9'15'#9'Group Identifier'#9'F'#9
          'PROCESS_DATE'#9'10'#9'Process date'#9'F'#9
          'PR_CHECK_NBR'#9'10'#9'Pr Check Nbr'#9#9
          'SB_BANK_ACCOUNTS_NBR'#9'10'#9'Sb Bank Accounts Nbr'#9#9
          'CO_TAX_DEPOSITS_NBR'#9'10'#9'Co Tax Deposits Nbr'#9'F'#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_BANK_ACCOUNT_REGISTER\evgrdProcessed'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsProcessed
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object procTabManualType: TevDBComboBox
        Left = 424
        Top = 56
        Width = 121
        Height = 21
        TabStop = False
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'MANUAL_TYPE'
        DataSource = dsProcessed
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 2
        UnboundDataType = wwDefault
      end
      object procTabStatus: TevDBComboBox
        Left = 424
        Top = 80
        Width = 121
        Height = 21
        TabStop = False
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'STATUS'
        DataSource = dsProcessed
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object procTabRegistryType: TevDBComboBox
        Left = 424
        Top = 104
        Width = 121
        Height = 21
        TabStop = False
        ShowButton = True
        Style = csDropDownList
        MapList = True
        AllowClearKey = False
        AutoDropDown = True
        DataField = 'REGISTER_type'
        DataSource = dsProcessed
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 4
        UnboundDataType = wwDefault
      end
    end
    object tsht4: TTabSheet
      Caption = 'Merge'
      ImageIndex = 3
      object grpTwImport: TevGroupBox
        Left = 8
        Top = 8
        Width = 585
        Height = 342
        Caption = 'Merge'
        TabOrder = 0
        object spbBankMergeSourceFile: TevSpeedButton
          Left = 426
          Top = 56
          Width = 23
          Height = 22
          HideHint = True
          AutoSize = False
          OnClick = spbBankMergeSourceFileClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          ParentColor = False
          ShortCut = 0
        end
        object Label4: TevLabel
          Left = 8
          Top = 56
          Width = 79
          Height = 13
          Caption = 'Source file name'
        end
        object spbBankMergeReportDir: TevSpeedButton
          Left = 426
          Top = 197
          Width = 23
          Height = 22
          HideHint = True
          AutoSize = False
          OnClick = spbBankMergeReportDirClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          ParentColor = False
          ShortCut = 0
        end
        object Label5: TevLabel
          Left = 8
          Top = 197
          Width = 122
          Height = 13
          Caption = 'Exception report file name'
        end
        object tsht4BankName: TevLabel
          Left = 8
          Top = 24
          Width = 109
          Height = 16
          Caption = 'tsht4BankName'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TevLabel
          Left = 8
          Top = 245
          Width = 130
          Height = 13
          Caption = 'Exception storage file name'
        end
        object spbBankMergeStorageDir: TevSpeedButton
          Left = 426
          Top = 245
          Width = 23
          Height = 22
          Enabled = False
          HideHint = True
          AutoSize = False
          OnClick = spbBankMergeStorageDirClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          ParentColor = False
          ShortCut = 0
        end
        object Label7: TevLabel
          Left = 8
          Top = 277
          Width = 53
          Height = 13
          Caption = 'Date range'
        end
        object Label8: TevLabel
          Left = 240
          Top = 277
          Width = 6
          Height = 13
          Caption = '--'
        end
        object Label9: TevLabel
          Left = 8
          Top = 309
          Width = 79
          Height = 13
          Caption = 'Transaction type'
        end
        object Label12: TLabel
          Left = 8
          Top = 80
          Width = 81
          Height = 13
          Caption = 'Merge up to date'
        end
        object editBankMergeSourceFile: TevEdit
          Left = 149
          Top = 56
          Width = 273
          Height = 21
          TabOrder = 0
        end
        object editBankMergeReportFile: TevEdit
          Left = 149
          Top = 197
          Width = 273
          Height = 21
          TabOrder = 5
        end
        object ckboxBankMergeFirst: TevCheckBox
          Left = 8
          Top = 221
          Width = 145
          Height = 17
          Caption = 'First merge in the month'
          TabOrder = 6
          OnClick = ckboxBankMergeFirstClick
        end
        object editBankMergeStorageFile: TevEdit
          Left = 149
          Top = 236
          Width = 273
          Height = 21
          Enabled = False
          TabOrder = 7
        end
        object editStorageReportDateFrom: TevDateTimePicker
          Left = 149
          Top = 269
          Width = 81
          Height = 21
          Date = 36495.730276388900000000
          Time = 36495.730276388900000000
          Enabled = False
          TabOrder = 8
        end
        object editStorageReportDateTill: TevDateTimePicker
          Left = 261
          Top = 269
          Width = 81
          Height = 21
          Date = 36495.730276388900000000
          Time = 36495.730276388900000000
          Enabled = False
          TabOrder = 9
        end
        object cboStorageReportTransferType: TevComboBox
          Left = 149
          Top = 309
          Width = 273
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          Enabled = False
          ItemHeight = 0
          Sorted = True
          TabOrder = 10
          OnChange = cboStorageReportTransferTypeChange
        end
        object rgSourceFileType: TRadioGroup
          Left = 149
          Top = 104
          Width = 112
          Height = 55
          Caption = 'Source file type'
          ItemIndex = 0
          Items.Strings = (
            'Debit file'
            'Credit file')
          TabOrder = 2
          OnClick = rgSourceFileTypeClick
        end
        object edMergeUpToDate: TevDateTimePicker
          Left = 149
          Top = 80
          Width = 89
          Height = 21
          Date = 36481.709159259300000000
          Time = 36481.709159259300000000
          TabOrder = 1
        end
        object btnTwImportGo: TevBitBtn
          Left = 464
          Top = 56
          Width = 105
          Height = 25
          Caption = 'Merge'
          TabOrder = 11
          OnClick = btnTwImportGoClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D00000000000
            0DDDD888888888888DDDD0FFFFFFFF730DDDD8DDDDDDDDDF8DDDD0FFFFFFF73B
            0888D8DDDDDDDDF8DDDDD000000083BB3338D88888888F88FFFDD0FFFFF73BBB
            BBB8D8DDDDDDF888888DD0FFFF73BBBBBBB8D8DDDDDF8888888DD000008BBBBB
            BBB8D88888D88888888DD0FFFFF7BBBBBBB8D8DDDDDD8888888DD0FFFFFF7BBB
            BBB8D8DDDDDDD888888DD888888888BB8DDDD88888888D88DDDDD8777777778B
            8DDDD8DDDDDDDDD8DDDDD877777777778DDDD8DDDDDDDDDD8DDDD88888888888
            8DDDD888888888888DDDD877777777778DDDD8DDDDDDDDDD8DDDD87777777777
            8DDDD8DDDDDDDDDD8DDDD888888888888DDDD888888888888DDD}
          NumGlyphs = 2
          Margin = 0
        end
        object btnTwImportView: TevBitBtn
          Left = 464
          Top = 197
          Width = 105
          Height = 25
          Caption = 'View report file'
          TabOrder = 12
          OnClick = btnTwImportViewClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D888888888DD
            DD8DDDDDDDDDDDDDDDDD8272777228000CC8D88888888888D88F2272777228F7
            CCCDD8DDDDDDDDDD888F22777772287CCCDDD8DDDDDDDDD888FD2222222228CC
            C0DDD8DDDDDDDD888FDD2FFFFFCCC4CC70DDD8DDDD888888F8DD2FFFFC872C47
            F0DDD8DDD8FDD88FD8DD2FFFC87728C7F0DDD8DD8FDDDD8FD8DD2FFFC87728C7
            F0DDD8DD8FDDDD8FD8DD2222C82227C7F0DDD8DD8FDDDD8FD8DDD8777C877C7F
            F0DDD8DDD8FDD8FDD8DDD0FFF7CCC77770DDD8DDDD888FDDD8DDD0FFFFFFF700
            00DDD8DDDDDDDD8888DDD0FFFFFFF70FF8DDD8DDDDDDDD8DD8DDD0FFFFFFF70F
            8DDDD8DDDDDDDD8D8DDDD00000000008DDDDD88888888888DDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object btnStorageScan: TevBitBtn
          Left = 464
          Top = 245
          Width = 105
          Height = 25
          Caption = 'Read storage'
          TabOrder = 13
          OnClick = btnStorageScanClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D4444444448D
            DDDDDFFFFFFFFFFDDDDDD4FFFFFFF4888DDDD888888888FDDDDDD4FFFFFFF400
            088DD888888888F88DDDD4F44444F46EE008D88DDDDD88FDD88DD4FFFFFFF46E
            E660D888888888FDD888D4F44444F4866660D88DDDDD88F88888D4FFFFFFF46E
            E880D888888888FDD888D4F44444F46EE660D88DDDDD88FDD888D4FFFFFFF486
            6660D888888888F88888D4444444446EE880D888888888FDD888DDDDDD6EFFEE
            E660DDDDDDDDDDDDD888DDDDDD6EF7666660DDDDDD8DDD888888DDDDDD667EEE
            E880DDDDDD88DDDDD888DDDDDD6EEEEEEEE0DDDDDD8DDDDDDDD8DDDDDDD66EEE
            E66DDDDDDDD88DDDD88DDDDDDDDDD6666DDDDDDDDDDDD8888DDD}
          NumGlyphs = 2
          Margin = 0
        end
        object btnStorageReport: TevBitBtn
          Left = 464
          Top = 277
          Width = 105
          Height = 25
          Caption = 'Print report'
          TabOrder = 14
          OnClick = btnStorageReportClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
            08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
            8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
            8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
            8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
            78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
            7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
            DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
            DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object btnStorageReportPreview: TevBitBtn
          Left = 464
          Top = 309
          Width = 105
          Height = 25
          Caption = 'Preview report'
          TabOrder = 15
          OnClick = btnStorageReportClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            D8DDDDDDDDDDDDDDDDDD000000000000CC8D88888888888D88FD0FFFFFFFFF7C
            CCDD8DDDDDDDDDD888FD0FFFFFFFF7CCCDDD8DDDDDDDDD888FDD0FFFF7777CCC
            0DDD8DDDDDDDD888FDDD0FFF7CCC4CC70DDD8DDDD888888F8DDD0FF7C877C47F
            0DDD8DDD8FDD88FD8DDD0FFC87777C7F0DDD8DD8FDDDD8FD8DDD0FFC87777C7F
            0DDD8DD8FDDDD8FD8DDD0FFC87777C7F0DDD8DD8FDDDD8FD8DDD0FFFC877C7FF
            0DDD8DDD8FDD8FDD8DDD0FFFFCCC77770DDD8DDDD888FDDD8DDD0FFFFFFF7000
            0DDD8DDDDDDDD8888DDD0FFFFFFF70FF8DDD8DDDDDDDD8DD8DDD0FFFFFFF70F8
            DDDD8DDDDDDDD8D8DDDD00000000008DDDDD88888888888DDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object cbCompPosOr: TevCheckBox
          Left = 150
          Top = 168
          Width = 235
          Height = 17
          Caption = 'Override company number starting position'
          TabOrder = 3
          OnClick = cbCompPosOrClick
        end
        object edCompPosOr: TevSpinEdit
          Left = 414
          Top = 167
          Width = 41
          Height = 22
          MaxValue = 200
          MinValue = 1
          TabOrder = 4
          Value = 20
          Visible = False
        end
        object cbShowProcessedRecords: TevCheckBox
          Left = 270
          Top = 84
          Width = 162
          Height = 17
          Caption = 'Show processed records'
          TabOrder = 16
        end
        object evGroupBox1: TevGroupBox
          Left = 270
          Top = 103
          Width = 225
          Height = 59
          Caption = 'Source file contains '
          TabOrder = 17
          object evLabel1: TevLabel
            Left = 7
            Top = 37
            Width = 127
            Height = 13
            Caption = 'Total number of characters'
          end
          object evrbCompCode: TevRadioButton
            Left = 7
            Top = 15
            Width = 113
            Height = 17
            Caption = 'Company code'
            Checked = True
            TabOrder = 0
            TabStop = True
            OnClick = evrbCompCodeClick
          end
          object evrbCompName: TevRadioButton
            Left = 110
            Top = 15
            Width = 113
            Height = 17
            Caption = 'Company name'
            TabOrder = 1
            OnClick = evrbCompNameClick
          end
          object edCompCodeLength: TevSpinEdit
            Left = 143
            Top = 32
            Width = 41
            Height = 22
            MaxValue = 20
            MinValue = 1
            TabOrder = 2
            Value = 9
          end
        end
      end
      object pProcessBar: TevPanel
        Left = 168
        Top = 354
        Width = 323
        Height = 53
        TabOrder = 1
        Visible = False
        object lProcessBarLabel: TevLabel
          Left = 8
          Top = 7
          Width = 6
          Height = 13
          Caption = '--'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object pbProgressBar: TProgressBar
          Left = 11
          Top = 24
          Width = 302
          Height = 13
          Smooth = True
          TabOrder = 0
        end
      end
    end
    object tsht5: TTabSheet
      Caption = 'Reports'
      ImageIndex = 4
      object tsht5BalanceGroup: TevGroupBox
        Left = 8
        Top = 8
        Width = 577
        Height = 401
        Caption = 'Balance report'
        TabOrder = 0
        object tsht5BankName: TevLabel
          Left = 8
          Top = 24
          Width = 109
          Height = 16
          Caption = 'tsht4BankName'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object tsht5FilterParam: TevLabel
          Left = 8
          Top = 48
          Width = 113
          Height = 16
          Caption = 'tsht5FilterParam'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object tsht5RadioBasedOn: TRadioGroup
          Left = 8
          Top = 134
          Width = 265
          Height = 54
          Caption = 'Based On'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Check Date'
            'Effective Date')
          TabOrder = 1
        end
        object tsht5RadioSortBy: TRadioGroup
          Left = 296
          Top = 144
          Width = 273
          Height = 65
          Caption = 'Sort By'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Amount'
            'Check #'
            'Company  #, Date'
            'Date, Company #')
          TabOrder = 7
          OnClick = tsht5RadioSortByClick
        end
        object tsht5RadioSummaryType: TRadioGroup
          Left = 8
          Top = 195
          Width = 265
          Height = 62
          Caption = 'Summary Type'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Summary'
            'Summary By Type'
            'Detail')
          TabOrder = 2
        end
        object tsht5RadioTransactionStatus: TRadioGroup
          Left = 296
          Top = 72
          Width = 273
          Height = 65
          Caption = 'Transaction Status'
          Columns = 2
          ItemIndex = 2
          Items.Strings = (
            'Cleared'
            'Outstanding'
            'Both')
          TabOrder = 5
        end
        object tsht5CompanyRadioOnlyWithTrans: TRadioGroup
          Left = 296
          Top = 216
          Width = 273
          Height = 65
          Caption = 'List Companies'
          ItemIndex = 0
          Items.Strings = (
            'All With Non-Zero Balance'
            'Only With Transactions')
          TabOrder = 8
          Visible = False
        end
        object tsht5RadioCreditDebit: TRadioGroup
          Left = 8
          Top = 267
          Width = 265
          Height = 62
          Caption = 'Filter On Credit/Debit'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Credit && Debit'
            'Credit Only'
            'Debit Only')
          TabOrder = 3
        end
        object tsht5RadioCompanyPageBreak: TRadioGroup
          Left = 8
          Top = 339
          Width = 265
          Height = 54
          Caption = 'Page Break On New Company'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Yes'
            'No')
          TabOrder = 4
        end
        object evRadioGroup1: TevRadioGroup
          Left = 8
          Top = 72
          Width = 265
          Height = 57
          Caption = 'Report'
          ItemIndex = 0
          Items.Strings = (
            'Check Recon Balance Report'
            'Check Recon Bal Summary')
          TabOrder = 0
          OnClick = evRadioGroup1Click
        end
        object tsht5ReportRunBtn: TevBitBtn
          Left = 464
          Top = 368
          Width = 105
          Height = 25
          Caption = 'Print report'
          TabOrder = 9
          OnClick = tsht5ReportRunBtnClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000000000
            08DDDD8888888888FDDDF87777777777808DD8DDDDDDDDDD8FDDF88888888877
            8808D888888888DD88FDF877777777778880D8DDDDDDDDDD888FF871A2777777
            8880D8D8D8DDDDDD8888F877777777778880D8DDDDDDDDDD8888F88888888888
            8880D888888888888888F877777777778880D8DDDDDDDDDD8888F7878FFFFFF8
            78808D8D8DDDDDD8D888FF4878000000078088D8D88888888D88FFF787777777
            7770888D8DDDDDDDDDD8FF4448888888888D88DDD8888888888DFFFFF777778D
            DDDD8888888888FDDDDDFF444444FF8DDDDD88DDDDDD88FDDDDDFFFFFFFFFF8D
            DDDD8888888888FDDDDDFFFFFFFFFFDDDDDD8888888888DDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
        object evCheckBox1: TevCheckBox
          Left = 435
          Top = 111
          Width = 117
          Height = 17
          Caption = 'Apply Filter To All'
          TabOrder = 6
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SB_BANK_ACCOUNTS.SB_BANK_ACCOUNTS
    Left = 229
    Top = 2
  end
  inherited wwdsDetail: TevDataSource
    DataSet = lTMP_CO_BANK_ACCOUNT_REGISTER
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = nil
    Left = 278
    Top = 114
  end
  inherited wwdsList: TevDataSource
    Top = 34
  end
  object wwClientDataSetClCo: TevClientDataSet
  end
  object wwDataSourceClCo: TevDataSource
    DataSet = wwClientDataSetClCo
    Left = 120
    Top = 152
  end
  object wwClientDataSetCo: TevClientDataSet
    Left = 288
  end
  object dlgBankMergeSourceFile: TOpenDialog
    Title = 'Bank merge file'
    Left = 628
    Top = 89
  end
  object dlgBankMergeStorageFile: TSaveDialog
    DefaultExt = 'sto'
    Title = 'Bank merge storage file'
    Left = 636
    Top = 153
  end
  object dlgBankMergeReportFile: TSaveDialog
    DefaultExt = 'txt'
    Title = 'Bank merge report file'
    Left = 652
    Top = 129
  end
  object wwStorageDataSet: TevClientDataSet
    Left = 635
    Top = 193
    object wwStorageDataSetType: TStringField
      FieldName = 'type'
      Size = 3
    end
    object wwStorageDataSetCompNumber: TStringField
      FieldName = 'CompNumber'
      Size = 4
    end
    object wwStorageDataSetCheckNumber: TIntegerField
      FieldName = 'CheckNumber'
    end
    object wwStorageDataSetClearDate: TDateField
      FieldName = 'ClearDate'
    end
    object wwStorageDataSetAmount: TCurrencyField
      FieldName = 'Amount'
    end
    object wwStorageDataSetProblem: TStringField
      FieldName = 'Problem'
      Size = 40
    end
  end
  object wwStorageDataSource: TevDataSource
    DataSet = wwStorageDataSet
    Left = 636
    Top = 227
  end
  object lTMP_CO_BANK_ACCOUNT_REGISTER: TevClientDataSet
    PictureMasks.Strings = (
      'AMOUNT'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]}'#9'T'#9'F'
      
        'CLEARED_AMOUNT'#9'[-]{{#[#][#]{{;,###*[;,###]},*#}[.*2[#]]},.#*1[#]' +
        '}'#9'T'#9'F')
    FieldDefs = <
      item
        Name = 'CLCustNum'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CoCustNum'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CONAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CO_BANK_ACCOUNT_REGISTER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SB_BANK_ACCOUNTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_CHECK_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_SERIAL_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_DATE'
        Attributes = [faRequired]
        DataType = ftDate
      end
      item
        Name = 'STATUS'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDate
      end
      item
        Name = 'TRANSACTION_EFFECTIVE_DATE'
        Attributes = [faRequired]
        DataType = ftDateTime
      end
      item
        Name = 'AMOUNT'
        Attributes = [faRequired]
        DataType = ftFloat
      end
      item
        Name = 'CLEARED_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'REGISTER_type'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MANUAL_TYPE'
        Attributes = [faRequired]
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_MANUAL_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TAX_PAYMENT_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_PR_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PROCESS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'NOTES'
        DataType = ftBlob
        Size = 1
      end
      item
        Name = 'FILLER'
        DataType = ftString
        Size = 512
      end
      item
        Name = 'CLEARED_DATE'
        DataType = ftDate
      end
      item
        Name = 'Payee'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'PR_MISCELLANEOUS_CHECKS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'BANK_ACCOUNT_REGISTER_NAME'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'GROUP_IDENTIFIER'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'TRACE_NUMBER'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'CO_TAX_DEPOSITS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'GroupIdentifierDesc'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'GroupIdentifierCount'
        DataType = ftInteger
      end
      item
        Name = 'OneCheckBarNbr'
        DataType = ftInteger
      end>
    AfterInsert = lTMP_CO_BANK_ACCOUNT_REGISTERAfterInsert
    BeforePost = lTMP_CO_BANK_ACCOUNT_REGISTERBeforePost
    OnCalcFields = lTMP_CO_BANK_ACCOUNT_REGISTERCalcFields
    Left = 184
    Top = 368
    object lTMP_CO_BANK_ACCOUNT_REGISTERCLCustNum: TStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'CLCustNum'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Lookup = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCoCustNum: TStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'CoCustNum'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR'
      Lookup = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCONAME: TStringField
      DisplayLabel = 'Co Name'
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'CONAME'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'Cl_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'Cl_NBR;CO_NBR'
      Size = 40
      Lookup = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCLNAME: TStringField
      DisplayLabel = 'Client Name'
      FieldKind = fkLookup
      FieldName = 'CLNAME'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'CL_NBR'
      Size = 40
      Lookup = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_BANK_ACCOUNT_REGISTER_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERSB_BANK_ACCOUNTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_BANK_ACCOUNTS_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERPR_CHECK_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCHECK_SERIAL_NUMBER: TIntegerField
      DisplayWidth = 10
      FieldName = 'CHECK_SERIAL_NUMBER'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCHECK_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'CHECK_DATE'
      Required = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERSTATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATUS'
      Required = True
      Size = 1
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERSTATUS_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'STATUS_DATE'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERTRANSACTION_EFFECTIVE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'TRANSACTION_EFFECTIVE_DATE'
      Required = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERAMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'AMOUNT'
      Required = True
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCLEARED_AMOUNT: TFloatField
      DisplayWidth = 10
      FieldName = 'CLEARED_AMOUNT'
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERTYPE: TStringField
      DisplayWidth = 1
      FieldName = 'REGISTER_type'
      Required = True
      Size = 1
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERMANUAL_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'MANUAL_TYPE'
      Required = True
      Size = 1
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCO_MANUAL_ACH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_MANUAL_ACH_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCO_TAX_PAYMENT_ACH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_PAYMENT_ACH_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCO_PR_ACH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_PR_ACH_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERPROCESS_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'PROCESS_DATE'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERNOTES: TBlobField
      DisplayWidth = 10
      FieldName = 'NOTES'
      Size = 1
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERFILLER: TStringField
      DisplayWidth = 512
      FieldName = 'FILLER'
      Size = 512
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCLEARED_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'CLEARED_DATE'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERPayee: TStringField
      DisplayWidth = 40
      FieldKind = fkInternalCalc
      FieldName = 'Payee'
      Size = 40
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_MISCELLANEOUS_CHECKS_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERBANK_ACCOUNT_REGISTER_NAME: TStringField
      DisplayWidth = 15
      FieldKind = fkLookup
      FieldName = 'BANK_ACCOUNT_REGISTER_NAME'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'BANK_ACCOUNT_REGISTER_NAME'
      KeyFields = 'CL_NBR;CO_NBR'
      Size = 15
      Lookup = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERGROUP_IDENTIFIER: TStringField
      DisplayWidth = 15
      FieldName = 'GROUP_IDENTIFIER'
      Size = 15
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERTRACE_NUMBER: TStringField
      DisplayWidth = 15
      FieldKind = fkLookup
      FieldName = 'TRACE_NUMBER'
      LookupDataSet = DM_TMP_CO_TAX_DEPOSITS.TMP_CO_TAX_DEPOSITS
      LookupKeyFields = 'CL_NBR;CO_NBR;CO_TAX_DEPOSITS_NBR'
      LookupResultField = 'TRACE_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR;CO_TAX_DEPOSITS_NBR'
      Size = 15
      Lookup = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERCO_TAX_DEPOSITS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_DEPOSITS_NBR'
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERGroupIdentifierCount: TIntegerField
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'GroupIdentifierCount'
      Calculated = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTEROneCheckBarNbr: TIntegerField
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'OneCheckBarNbr'
      Calculated = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERRecordHide: TStringField
      FieldKind = fkCalculated
      FieldName = 'RecordHide'
      Size = 1
      Calculated = True
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERNOTES_POPULATED: TStringField
      FieldName = 'NOTES_POPULATED'
      Size = 1
    end
    object lTMP_CO_BANK_ACCOUNT_REGISTERNotesUpdated: TStringField
      FieldKind = fkCalculated
      FieldName = 'NotesUpdated'
      Size = 1
      Calculated = True
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 400
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 432
  end
  object TempDataSet: TevClientDataSet
    Left = 635
    Top = 265
    object StringField1: TStringField
      DisplayWidth = 3
      FieldName = 'type'
      Size = 3
    end
    object StringField2: TStringField
      DisplayWidth = 4
      FieldName = 'CompNumber'
      Size = 4
    end
    object IntegerField1: TIntegerField
      DisplayWidth = 10
      FieldName = 'CheckNumber'
    end
    object DateTimeField1: TDateField
      DisplayWidth = 10
      FieldName = 'ClearDate'
    end
    object CurrencyField1: TCurrencyField
      DisplayWidth = 10
      FieldName = 'Amount'
    end
    object StringField3: TStringField
      DisplayWidth = 40
      FieldName = 'Problem'
      Size = 40
    end
  end
  object cdGroupIdentifierTotals: TevClientDataSet
    Left = 240
    Top = 368
    object cdGroupIdentifierTotalsGROUP_IDENTIFIER: TStringField
      FieldName = 'GROUP_IDENTIFIER'
      Size = 15
    end
    object cdGroupIdentifierTotalsCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdGroupIdentifierTotalsCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdGroupIdentifierTotalsAMOUNT: TCurrencyField
      FieldName = 'AMOUNT'
    end
    object cdGroupIdentifierTotalsGroupIdentifierCount: TIntegerField
      FieldName = 'GroupIdentifierCount'
    end
  end
  object dsConsolidate: TevDataSource
    DataSet = cdConsolidatedBar
    Left = 272
    Top = 264
  end
  object cdCoBankAccRegDetails: TevClientDataSet
    Left = 288
    Top = 368
    object cdCoBankAccRegDetailsCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField
      FieldName = 'CO_BANK_ACCOUNT_REGISTER_NBR'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdCoBankAccRegDetailsCLIENT_CO_BANK_ACC_REG_NBR: TIntegerField
      FieldName = 'CLIENT_CO_BANK_ACC_REG_NBR'
    end
  end
  object cdConsolidatedBar: TevClientDataSet
    FieldDefs = <
      item
        Name = 'SB_BANK_ACCOUNTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_CHECK_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_SERIAL_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_DATE'
        DataType = ftDate
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDate
      end
      item
        Name = 'TRANSACTION_EFFECTIVE_DATE'
        DataType = ftDate
      end
      item
        Name = 'AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'CLEARED_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'REGISTER_type'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MANUAL_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PROCESS_DATE'
        DataType = ftDate
      end
      item
        Name = 'CLEARED_DATE'
        DataType = ftDate
      end
      item
        Name = 'CO_BANK_ACCOUNT_REGISTER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_MANUAL_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TAX_PAYMENT_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_PR_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_MISCELLANEOUS_CHECKS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TAX_DEPOSITS_NBR'
        DataType = ftInteger
      end>
    Left = 312
    Top = 264
    object cdConsolidatedBarCLCustNum: TStringField
      DisplayLabel = 'CL #'
      DisplayWidth = 15
      FieldKind = fkLookup
      FieldName = 'CLCustNum'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Size = 15
      Lookup = True
    end
    object cdConsolidatedBarCoCustNum: TStringField
      DisplayLabel = 'CO #'
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'CoCustNum'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR'
      Lookup = True
    end
    object cdConsolidatedBarCONAME: TStringField
      DisplayLabel = 'Co Name'
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'CONAME'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'Cl_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'Cl_NBR;CO_NBR'
      Size = 40
      Lookup = True
    end
    object cdConsolidatedBarCHECK_SERIAL_NUMBER: TIntegerField
      DisplayLabel = 'Check serial #'
      DisplayWidth = 10
      FieldName = 'CHECK_SERIAL_NUMBER'
    end
    object cdConsolidatedBarCHECK_DATE: TDateField
      DisplayLabel = 'Check date'
      DisplayWidth = 10
      FieldName = 'CHECK_DATE'
    end
    object cdConsolidatedBarSTATUS: TStringField
      DisplayLabel = 'Status'
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object cdConsolidatedBarSTATUS_DATE: TDateField
      DisplayLabel = 'Status date'
      DisplayWidth = 10
      FieldName = 'STATUS_DATE'
    end
    object cdConsolidatedBarTRANSACTION_EFFECTIVE_DATE: TDateField
      DisplayWidth = 10
      FieldName = 'TRANSACTION_EFFECTIVE_DATE'
    end
    object cdConsolidatedBarAMOUNT: TFloatField
      DisplayLabel = 'Amount'
      DisplayWidth = 10
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object cdConsolidatedBarCLEARED_AMOUNT: TFloatField
      DisplayLabel = 'Cleared amount'
      DisplayWidth = 10
      FieldName = 'CLEARED_AMOUNT'
      DisplayFormat = '#,##0.00'
      EditFormat = '#,##0.00'
    end
    object cdConsolidatedBarCLEARED_DATE: TDateField
      DisplayLabel = 'Cleared date'
      DisplayWidth = 10
      FieldName = 'CLEARED_DATE'
    end
    object cdConsolidatedBarREGISTER_type: TStringField
      DisplayLabel = 'Register type'
      DisplayWidth = 1
      FieldName = 'REGISTER_type'
      Size = 1
    end
    object cdConsolidatedBarMANUAL_TYPE: TStringField
      DisplayLabel = 'Manual type'
      DisplayWidth = 1
      FieldName = 'MANUAL_TYPE'
      Size = 1
    end
    object cdConsolidatedBarGROUP_IDENTIFIER: TStringField
      DisplayWidth = 15
      FieldName = 'GROUP_IDENTIFIER'
      Size = 15
    end
    object cdConsolidatedBarPROCESS_DATE: TDateField
      DisplayLabel = 'Process date'
      DisplayWidth = 10
      FieldName = 'PROCESS_DATE'
    end
    object cdConsolidatedBarPR_CHECK_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_NBR'
    end
    object cdConsolidatedBarSB_BANK_ACCOUNTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_BANK_ACCOUNTS_NBR'
    end
    object cdConsolidatedBarCO_TAX_DEPOSITS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_DEPOSITS_NBR'
    end
    object cdConsolidatedBarCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_BANK_ACCOUNT_REGISTER_NBR'
      Visible = False
    end
    object cdConsolidatedBarCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
      Visible = False
    end
    object cdConsolidatedBarCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Visible = False
    end
    object cdConsolidatedBarCO_MANUAL_ACH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_MANUAL_ACH_NBR'
      Visible = False
    end
    object cdConsolidatedBarCO_TAX_PAYMENT_ACH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_PAYMENT_ACH_NBR'
      Visible = False
    end
    object cdConsolidatedBarCO_PR_ACH_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_PR_ACH_NBR'
      Visible = False
    end
    object cdConsolidatedBarPR_MISCELLANEOUS_CHECKS_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_MISCELLANEOUS_CHECKS_NBR'
      Visible = False
    end
  end
  object cdClientBarList: TevClientDataSet
    IndexFieldNames = 'CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR'
    Left = 376
    Top = 312
    object cdClientBarListCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdClientBarListCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField
      FieldName = 'CO_BANK_ACCOUNT_REGISTER_NBR'
    end
    object cdClientBarListCHANGED: TIntegerField
      FieldName = 'CHANGED'
    end
    object cdClientBarListCLEARED_DATE: TDateTimeField
      FieldName = 'CLEARED_DATE'
    end
    object cdClientBarListROLLBACK_CLEAR_DATE: TDateTimeField
      FieldName = 'ROLLBACK_CLEAR_DATE'
    end
    object cdClientBarListROLLBACK_RECORD: TStringField
      FieldName = 'ROLLBACK_RECORD'
      Size = 1
    end
    object cdClientBarListNEWRECORD: TStringField
      FieldName = 'NEWRECORD'
      Size = 1
    end
  end
  object dsProcessed: TevDataSource
    DataSet = cdProcessed
    Left = 272
    Top = 296
  end
  object cdProcessed: TevClientDataSet
    FieldDefs = <
      item
        Name = 'SB_BANK_ACCOUNTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_CHECK_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_SERIAL_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'CHECK_DATE'
        DataType = ftDate
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDate
      end
      item
        Name = 'TRANSACTION_EFFECTIVE_DATE'
        DataType = ftDate
      end
      item
        Name = 'AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'CLEARED_AMOUNT'
        DataType = ftFloat
      end
      item
        Name = 'REGISTER_type'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'MANUAL_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PROCESS_DATE'
        DataType = ftDate
      end
      item
        Name = 'CLEARED_DATE'
        DataType = ftDate
      end
      item
        Name = 'CO_BANK_ACCOUNT_REGISTER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_MANUAL_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TAX_PAYMENT_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_PR_ACH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PR_MISCELLANEOUS_CHECKS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TAX_DEPOSITS_NBR'
        DataType = ftInteger
      end>
    Left = 312
    Top = 296
    object StringField4: TStringField
      DisplayLabel = 'CL #'
      DisplayWidth = 15
      FieldKind = fkLookup
      FieldName = 'CLCustNum'
      LookupDataSet = DM_TMP_CL.TMP_CL
      LookupKeyFields = 'CL_NBR'
      LookupResultField = 'CUSTOM_CLIENT_NUMBER'
      KeyFields = 'CL_NBR'
      Size = 15
      Lookup = True
    end
    object StringField5: TStringField
      DisplayLabel = 'CO #'
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'CoCustNum'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'CL_NBR;CO_NBR'
      LookupResultField = 'CUSTOM_COMPANY_NUMBER'
      KeyFields = 'CL_NBR;CO_NBR'
      Lookup = True
    end
    object StringField6: TStringField
      DisplayLabel = 'Co Name'
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'CONAME'
      LookupDataSet = DM_TMP_CO.TMP_CO
      LookupKeyFields = 'Cl_NBR;CO_NBR'
      LookupResultField = 'NAME'
      KeyFields = 'Cl_NBR;CO_NBR'
      Size = 40
      Lookup = True
    end
    object IntegerField3: TIntegerField
      DisplayLabel = 'Check serial #'
      DisplayWidth = 10
      FieldName = 'CHECK_SERIAL_NUMBER'
    end
    object DateField1: TDateField
      DisplayLabel = 'Check date'
      DisplayWidth = 10
      FieldName = 'CHECK_DATE'
    end
    object StringField7: TStringField
      DisplayLabel = 'Status'
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object DateField2: TDateField
      DisplayLabel = 'Status date'
      DisplayWidth = 10
      FieldName = 'STATUS_DATE'
    end
    object DateField3: TDateField
      DisplayWidth = 10
      FieldName = 'TRANSACTION_EFFECTIVE_DATE'
    end
    object FloatField1: TFloatField
      DisplayLabel = 'Amount'
      DisplayWidth = 10
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
      EditFormat = '###0.00'
    end
    object FloatField2: TFloatField
      DisplayLabel = 'Cleared amount'
      DisplayWidth = 10
      FieldName = 'CLEARED_AMOUNT'
      DisplayFormat = '#,##0.00'
      EditFormat = '#,##0.00'
    end
    object DateField4: TDateField
      DisplayLabel = 'Cleared date'
      DisplayWidth = 10
      FieldName = 'CLEARED_DATE'
    end
    object StringField8: TStringField
      DisplayLabel = 'Register type'
      DisplayWidth = 1
      FieldName = 'REGISTER_type'
      Size = 1
    end
    object StringField9: TStringField
      DisplayLabel = 'Manual type'
      DisplayWidth = 1
      FieldName = 'MANUAL_TYPE'
      Size = 1
    end
    object StringField10: TStringField
      DisplayWidth = 15
      FieldName = 'GROUP_IDENTIFIER'
      Size = 15
    end
    object DateField5: TDateField
      DisplayLabel = 'Process date'
      DisplayWidth = 10
      FieldName = 'PROCESS_DATE'
    end
    object IntegerField4: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_CHECK_NBR'
    end
    object IntegerField5: TIntegerField
      DisplayWidth = 10
      FieldName = 'SB_BANK_ACCOUNTS_NBR'
    end
    object IntegerField6: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_DEPOSITS_NBR'
    end
    object IntegerField7: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_BANK_ACCOUNT_REGISTER_NBR'
      Visible = False
    end
    object IntegerField8: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
      Visible = False
    end
    object IntegerField9: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
      Visible = False
    end
    object IntegerField10: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_MANUAL_ACH_NBR'
      Visible = False
    end
    object IntegerField11: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_PAYMENT_ACH_NBR'
      Visible = False
    end
    object IntegerField12: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_PR_ACH_NBR'
      Visible = False
    end
    object IntegerField13: TIntegerField
      DisplayWidth = 10
      FieldName = 'PR_MISCELLANEOUS_CHECKS_NBR'
      Visible = False
    end
  end
  object cdProcessedBarList: TevClientDataSet
    IndexFieldNames = 'CL_NBR;CO_BANK_ACCOUNT_REGISTER_NBR'
    Left = 408
    Top = 312
    object cdProcessedBarCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdProcessedBarCO_BANK_ACCOUNT_REGISTER_NBR: TIntegerField
      FieldName = 'CO_BANK_ACCOUNT_REGISTER_NBR'
    end
  end
end
