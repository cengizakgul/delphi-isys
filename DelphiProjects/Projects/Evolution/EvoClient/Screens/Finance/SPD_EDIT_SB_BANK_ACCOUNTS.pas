// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_BANK_ACCOUNTS;

interface

uses SFrameEntry, Db,   Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Wwdatsrc, SDataStructure, EvTypes, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, SDDClasses, EvConsts,
  ISBasicClasses, SDataDictbureau, EvContext, SPackageEntry, evCommonInterfaces,
  evMainboard, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIwwDBLookupCombo,
  isUIwwDBComboBox, isUIDBImage, isUIwwDBEdit;

type
  TEDIT_SB_BANK_ACCOUNTS = class(TFrameEntry)
    pctlEDIT_SB_BANK_ACCOUNTS: TevPageControl;
    tshtBrowse: TTabSheet;
    tshtBank_Account_Detail: TTabSheet;
    lablBank_Account_Nbr: TevLabel;
    lablDescription: TevLabel;
    lablAccount_Type: TevLabel;
    lablACH_Originator: TevLabel;
    lablLogo: TevLabel;
    lablSignature: TevLabel;
    lablCustom_Header_Record: TevLabel;
    lablBeginning_Balance: TevLabel;
    dedtBank_Account_Nbr: TevDBEdit;
    dedtDescription: TevDBEdit;
    dimgLogo: TevDBImage;
    dimgSignature: TevDBImage;
    dedtCustom_Header_Record: TevDBEdit;
    grbxCheck_Information: TevGroupBox;
    lablNext_Available_Check_Nbr: TevLabel;
    lablEnd_Check_Nbr: TevLabel;
    lablFuture_Begin_Check_Nbr: TevLabel;
    lablFuture_End_Check_Nbr: TevLabel;
    dedtNext_Available_Check_Nbr: TevDBEdit;
    dedtEnd_Check_Nbr: TevDBEdit;
    dedtFuture_Begin_Check_Nbr: TevDBEdit;
    dedtFuture_End_Check_Nbr: TevDBEdit;
    drgpBank_Account_Type: TevDBRadioGroup;
    drgpBank_Returns: TevDBRadioGroup;
    wwdgTeam: TevDBGrid;
    dtxtNAME: TevDBText;
    lablTeam: TevLabel;
    wwlcACH_Originator: TevDBLookupCombo;
    bbtnLoad_Signature: TevBitBtn;
    bbtnLoad_Logo: TevBitBtn;
    odlgGetBitmap: TOpenDialog;
    Splitter1: TevSplitter;
    wwdgTeamMembers: TevDBGrid;
    wwdeBeginning_Balance: TevDBEdit;
    Label1: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    cdBanks: TevClientDataSet;
    tsBlocks: TTabSheet;
    rgSuppressOffset: TevDBRadioGroup;
    rgBlockNegativeChecks: TevDBRadioGroup;
    Bevel1: TBevel;
    GroupBox1: TevGroupBox;
    evLabel1: TevLabel;
    Label2: TevLabel;
    wwDBEdit1: TevDBEdit;
    wwDBEdit2: TevDBEdit;
    evLabel2: TevLabel;
    dtpLockDate: TevDateTimePicker;
    evCheckBox1: TevDBCheckBox;
    evCheckBox2: TevDBCheckBox;
    evCheckBox3: TevDBCheckBox;
    evCheckBox4: TevDBCheckBox;
    evCheckBox5: TevDBCheckBox;
    evCheckBox6: TevDBCheckBox;
    evCheckBox7: TevDBCheckBox;
    evDBEdit1: TevDBEdit;
    evLabel3: TevLabel;
    evDBRadioGroup1: TevDBRadioGroup;

    procedure bbtnLoad_SignatureClick(Sender: TObject);
    procedure bbtnLoad_LogoClick(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure dtpLockDateChange(Sender: TObject);
    procedure ResultFIELDS_SB_BANK_Function;
    procedure wwdgTeamMembersAfterDrawCell(Sender: TwwCustomDBGrid;
      DrawCellInfo: TwwCustomDrawGridCellInfo);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure Activate; override;
    procedure AfterClick(Kind: Integer); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function DeleteMessage: Word; override;
  end;

var
  EDIT_SB_BANK_ACCOUNTS: TEDIT_SB_BANK_ACCOUNTS;

implementation

{$R *.DFM}

uses Graphics, EvUtils, SysUtils, EvDataset, EvStreamUtils;

function TEDIT_SB_BANK_ACCOUNTS.DeleteMessage: Word;
begin
  Result := mrNone;
  if EvMessage('Are you sure you want to delete this bank account?', mtConfirmation, mbOKCancel) = mrOK then
    Result := mrOK;
end;

procedure TEDIT_SB_BANK_ACCOUNTS.AfterClick(Kind: Integer);
begin
  inherited;
  ResultFIELDS_SB_BANK_Function;
end;

procedure TEDIT_SB_BANK_ACCOUNTS.bbtnLoad_SignatureClick(Sender: TObject);
var
  sStream: IisStream;
begin
  inherited;
  if odlgGetBitmap.Execute then
  begin
    CheckFileSize(odlgGetBitmap.FileName, 700);
    if not (dimgSignature.DataSource.DataSet.State in [dsInsert, dsEdit]) then
      dimgSignature.DataSource.DataSet.Edit;

    sStream := TisStream.CreateFromFile(odlgGetBitmap.FileName);
    TevClientDataset(dimgSignature.DataSource.DataSet).UpdateBlobData('SIGNATURE_SB_BLOB_NBR', sStream);
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNTS.bbtnLoad_LogoClick(Sender: TObject);
var
  sStream : IisStream;
begin
  inherited;
  if odlgGetBitmap.Execute then
  begin
    CheckFileSize(odlgGetBitmap.FileName, 700);
    if not (dimgLogo.DataSource.DataSet.State in [dsInsert, dsEdit]) then
      dimgLogo.DataSource.DataSet.Edit;

    sStream := TisStream.CreateFromFile(odlgGetBitmap.FileName);
    TevClientDataset(dimgLogo.DataSource.DataSet).UpdateBlobData('LOGO_SB_BLOB_NBR', sStream);
  end;
end;

function TEDIT_SB_BANK_ACCOUNTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS;
end;

function TEDIT_SB_BANK_ACCOUNTS.GetInsertControl: TWinControl;
begin
  Result := dedtBank_Account_Nbr;
end;

procedure TEDIT_SB_BANK_ACCOUNTS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SERVICE_BUREAU.SB_BANKS, SupportDataSets);
end;

procedure TEDIT_SB_BANK_ACCOUNTS.Activate;
begin
  inherited;
  cdBanks.Data := DM_SERVICE_BUREAU.SB_BANKS.Data;
  ResultFIELDS_SB_BANK_Function;
end;

procedure TEDIT_SB_BANK_ACCOUNTS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
var
  FillerDateStr: string;
begin
  inherited;
  if not Assigned(Field) and (dtpLockDate.Tag = 0) then
  begin
    dtpLockDate.Tag := 1;
    try
      FillerDateStr := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 61, 10);
      if FillerDateStr <> '' then
      try
        dtpLockDate.Date := StrToDate(FillerDateStr);
        dtpLockDate.Checked := True;
      except
        dtpLockDate.Checked := False;
      end
      else
        dtpLockDate.Checked := False;
    finally
      dtpLockDate.Tag := 0;
    end;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNTS.wwdsDetailUpdateData(Sender: TObject);
begin
  inherited;
  if dtpLockDate.Checked then
    wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, DateToStr(dtpLockDate.Date), 61, 10)
  else
    wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, '', 61, 10);
end;

procedure TEDIT_SB_BANK_ACCOUNTS.dtpLockDateChange(Sender: TObject);
begin
  inherited;
  if dtpLockDate.Tag = 0 then
  begin
    dtpLockDate.Tag := 1;
    try
      wwdsDetail.Edit;
    finally
      dtpLockDate.Tag := 0;
    end;
  end;
end;

procedure TEDIT_SB_BANK_ACCOUNTS.ResultFIELDS_SB_BANK_Function;
Begin
  if ctx_AccountRights.Functions.GetState('FIELDS_SB_BANK_') <> stEnabled then
   Begin
    (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
    (Owner as TFramePackageTmpl).btnNavDelete.Enabled := false;
    dedtBank_Account_Nbr.Enabled := False;
   end;
End;

procedure TEDIT_SB_BANK_ACCOUNTS.wwdgTeamMembersAfterDrawCell(
  Sender: TwwCustomDBGrid; DrawCellInfo: TwwCustomDrawGridCellInfo);
begin
  inherited;
  ResultFIELDS_SB_BANK_Function;
end;

procedure TEDIT_SB_BANK_ACCOUNTS.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
  LockFlag: IevGlobalFlagInfo;
  Q: IevQuery;
begin
  if Kind = NavCommit then
  begin
    LockFlag := mb_GlobalFlagsManager.GetLockedFlag(GF_SB_PAYROLL_OPERATION);
    if Assigned(LockFlag) then
      raise EevException.Create('Payroll processing is assigning check numbers.'#13'Please click refresh button and try again.');
  end;

  case Kind of
  NavCommit, NavOK:
    begin
      if (wwdsDetail.DataSet.RecordCount > 0) then
      begin
        Q := TevQuery.Create(
          'SELECT NEXT_AVAILABLE_CHECK_NUMBER FROM SB_BANK_ACCOUNTS WHERE {AsOfNow<SB_BANK_ACCOUNTS>} AND SB_BANK_ACCOUNTS_NBR='+
            wwdsDetail.DataSet.FieldByName('SB_BANK_ACCOUNTS_NBR').AsString);
        Q.Execute;
        if Q.Result.RecordCount > 0 then
        begin
          if Q.Result.FieldByName('NEXT_AVAILABLE_CHECK_NUMBER').AsInteger <>
             wwdsDetail.DataSet.FieldByName('NEXT_AVAILABLE_CHECK_NUMBER').OldValue then
              raise EevException.Create('This record has been updated by another user. Please refresh the screen and make your changes again.');
        end;
      end;  
    end
  end;

  inherited;
end;

initialization
  RegisterClass(TEDIT_SB_BANK_ACCOUNTS);

end.
