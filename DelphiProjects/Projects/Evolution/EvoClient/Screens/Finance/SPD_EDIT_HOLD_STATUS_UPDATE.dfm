object EDIT_HOLD_STATUS_UPDATE: TEDIT_HOLD_STATUS_UPDATE
  Left = 567
  Top = 290
  Width = 316
  Height = 200
  Caption = 'Credit Hold And Status Update'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel8: TevLabel
    Left = 16
    Top = 64
    Width = 67
    Height = 13
    Caption = 'Liability Status'
  end
  object evLabel6: TevLabel
    Left = 16
    Top = 16
    Width = 52
    Height = 13
    Caption = 'Credit Hold'
  end
  object Button12: TButton
    Left = 176
    Top = 96
    Width = 113
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 0
  end
  object Button13: TButton
    Left = 176
    Top = 32
    Width = 113
    Height = 25
    Caption = 'Update Credit Hold'
    ModalResult = 1
    TabOrder = 1
  end
  object evcbLiabStatus: TevDBComboBox
    Left = 16
    Top = 80
    Width = 145
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = False
    AllowClearKey = False
    AutoDropDown = True
    DropDownCount = 8
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 2
    UnboundDataType = wwDefault
    Glowing = False
  end
  object evcbCreditHold: TevDBComboBox
    Left = 16
    Top = 32
    Width = 145
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = False
    AllowClearKey = False
    AutoDropDown = True
    DropDownCount = 8
    ItemHeight = 0
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 3
    UnboundDataType = wwDefault
    Glowing = False
  end
  object Button1: TButton
    Left = 176
    Top = 64
    Width = 113
    Height = 25
    Caption = 'Update Status'
    ModalResult = 6
    TabOrder = 4
  end
  object OpenDialog1: TOpenDialog
    Left = 65
    Top = 121
  end
end
