// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_MASS_BILLING;

interface

uses
  Windows, SFrameEntry, Classes, Db, Wwdatsrc,  Controls, ExtCtrls,
  Grids, Wwdbigrd, Wwdbgrid, SDataStructure, StdCtrls, ComCtrls,
  EvUtils, SysUtils, SReportSettings, EvTypes, EvConsts,
  Buttons, SDDClasses, kbmMemTable, ISKbmMemDataSet, EvContext,
  ISBasicClasses, Dialogs, EvDataAccessComponents, ISDataAccessComponents,
  SDataDictbureau, SDataDictclient, SDataDicttemp, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton;

type
  TMASS_BILLING = class(TFrameEntry)
    DM_TEMPORARY: TDM_TEMPORARY;
    pcMain: TevPageControl;
    tsCompany: TTabSheet;
    Splitter: TevSplitter;
    grdSelectCompanies: TevDBGrid;
    pnlInvoice: TevPanel;
    grdInvoices: TevDBGrid;
    cdsInvoices: TevClientDataSet;
    cdsInvoicesCO_NBR: TIntegerField;
    cdsInvoicesCL_NBR: TIntegerField;
    cdsInvoicesCO_BILLING_HISTORY_NBR: TIntegerField;
    cdsInvoicesINVOICE_NUMBER: TIntegerField;
    cdsInvoicesINVOICE_DATE: TDateTimeField;
    cdsInvoicesCUSTOM_COMPANY_NUMBER: TStringField;
    cdsInvoicesCOMPANY_NAME: TStringField;
    pnlOpenInv: TevPanel;
    cdsInvoiceDetails: TevClientDataSet;
    tsDetails: TTabSheet;
    wwDBGrid1: TevDBGrid;
    cdsInvoiceDetailsCO_BILLING_HISTORY_DETAIL_NBR: TIntegerField;
    cdsInvoiceDetailsCO_BILLING_HISTORY_NBR: TIntegerField;
    cdsInvoiceDetailsCO_SERVICES_NBR: TIntegerField;
    cdsInvoiceDetailsQUANTITY: TFloatField;
    cdsInvoiceDetailsAMOUNT: TFloatField;
    cdsInvoiceDetailsTAX: TFloatField;
    cdsInvoiceDetailsDISCOUNT: TFloatField;
    cdsInvoiceDetailsCL_NBR: TIntegerField;
    cdsInvoiceDetailsNAME: TStringField;
    DM_COMPANY: TDM_COMPANY;
    pnlPrint: TevPanel;
    tsCreate: TTabSheet;
    lvSelected: TevListView;
    pnlServices: TevPanel;
    grdCreate: TevDBGrid;
    pnlCreate: TevPanel;
    dsServices: TevDataSource;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    cdsInvoiceDetailsSERVICE_NAME: TStringField;
    cdsInvoiceDetailsSB_SERVICES_NBR: TIntegerField;
    btnOpenInv: TevBitBtn;
    btnPrint: TevBitBtn;
    btnCreate: TevBitBtn;
    cbPreview: TevCheckBox;
    evLabel1: TevLabel;
    evDateTimePicker1: TevDateTimePicker;
    evLabel2: TevLabel;
    evMemo1: TevMemo;
    procedure grdSelectCompaniesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure wwdsListDataChange(Sender: TObject; Field: TField);
    procedure btnOpenInvClick(Sender: TObject);
    procedure wwdsMasterDataChange(Sender: TObject; Field: TField);
    procedure grdInvoicesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grdInvoicesDblClick(Sender: TObject);
    procedure grdSelectCompaniesDblClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure pcMainChange(Sender: TObject);
    procedure btnCreateClick(Sender: TObject);
    procedure cdsInvoiceDetailsCalcFields(DataSet: TDataSet);
    procedure pnlCreateResize(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

implementation

{$R *.DFM}

{ TMASS_BILLING }

procedure TMASS_BILLING.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  if not cdsInvoices.Active then
    cdsInvoices.CreateDataSet;
  if not cdsInvoiceDetails.Active then
    cdsInvoiceDetails.CreateDataSet;
  NavigationDataSet := cdsInvoices;
//  AddDS(DM_TEMPORARY.TMP_CO, SupportDataSets);
  AddDSWithCheck(DM_TEMPORARY.TMP_CO, SupportDataSets, GetReadOnlyClintCompanyListFilter(false));
  if not Assigned(wwdsList.DataSet) then
    wwdsList.DataSet := DM_TEMPORARY.TMP_CO;
  AddDS(DM_SERVICE_BUREAU.SB_SERVICES, SupportDataSets);
end;

procedure TMASS_BILLING.grdSelectCompaniesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (Shift = [ssCtrl]) and (DM_TEMPORARY.TMP_CO.RecordCount > 0) then
    btnOpenInv.Click;
end;

procedure TMASS_BILLING.wwdsListDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  btnOpenInv.Enabled := Assigned(wwdsList.DataSet) and wwdsList.DataSet.Active and
                        (wwdsList.DataSet.RecordCount > 0);
end;

procedure TMASS_BILLING.btnOpenInvClick(Sender: TObject);
var
  i: Integer;
  Mark: TBookmarkStr;

  procedure GetInvoicesData;
  begin
    if not cdsInvoiceDetails.Active or
       not cdsInvoiceDetails.Locate('CL_NBR', DM_TEMPORARY.TMP_CO['CL_NBR'], []) then
      with TExecDSWrapper.Create('GetInvoiceDetailsForCompany'), ctx_DataAccess.CUSTOM_VIEW do
      begin
        ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsInteger);
        DataRequest(AsVariant);
        Close;
        Open;
        if not cdsInvoiceDetails.Active then
          cdsInvoiceDetails.Data := Data
        else
          cdsInvoiceDetails.AppendData(Data, False);
        Close;
      end;

    cdsInvoiceDetails.RetrieveCondition := '';
    with TExecDSWrapper.Create('GetInvoicesForCompany'), ctx_DataAccess.CUSTOM_VIEW do
    begin
      SetParam('CoNbr', DM_TEMPORARY.TMP_CO['CO_NBR']);
      ctx_DataAccess.OpenClient(DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsInteger);
      DataRequest(AsVariant);
      Close;
      Open;
      if not cdsInvoices.Active then
        cdsInvoices.Data := Data
      else
        cdsInvoices.AppendData(Data, False);
      Close;
    end;
  end;

begin
  inherited;
  cdsInvoices.Close;
  cdsInvoiceDetails.Close;
  ctx_StartWait;
  Mark := DM_TEMPORARY.TMP_CO.Bookmark;
  DM_TEMPORARY.TMP_CO.DisableControls;
  try
    if grdSelectCompanies.SelectedList.Count = 0 then
      GetInvoicesData
    else
      for i := 0 to Pred(grdSelectCompanies.SelectedList.Count) do
      begin
        DM_TEMPORARY.TMP_CO.GotoBookmark(grdSelectCompanies.SelectedList[i]);
        GetInvoicesData;
      end;
  finally
    DM_TEMPORARY.TMP_CO.Bookmark := Mark;
    DM_TEMPORARY.TMP_CO.EnableControls;
    ctx_EndWait;
  end;
  grdInvoices.Visible := True;
  cdsInvoices.First;
end;

procedure TMASS_BILLING.wwdsMasterDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  cdsInvoiceDetails.RetrieveCondition := 'CL_NBR = ' + IntToStr(cdsInvoices.FieldByName('CL_NBR').AsInteger)  + ' and ' +
                                         'CO_BILLING_HISTORY_NBR = ' + IntToStr(cdsInvoices.FieldByName('CO_BILLING_HISTORY_NBR').AsInteger);
  btnPrint.Enabled := Assigned(wwdsMaster.DataSet) and wwdsMaster.DataSet.Active and
                        (wwdsMaster.DataSet.RecordCount > 0);
  cbPreview.Enabled := btnPrint.Enabled;
end;

procedure TMASS_BILLING.grdInvoicesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (Shift = [ssCtrl]) and (cdsInvoices.RecordCount > 0) then
    pcMain.ActivatePage(tsDetails);
end;

procedure TMASS_BILLING.grdInvoicesDblClick(Sender: TObject);
begin
  inherited;
  if (cdsInvoices.RecordCount > 0) then
    pcMain.ActivatePage(tsDetails);
end;

procedure TMASS_BILLING.grdSelectCompaniesDblClick(Sender: TObject);
begin
  inherited;
  if DM_TEMPORARY.TMP_CO.RecordCount > 0 then
    btnOpenInv.Click;
end;

procedure TMASS_BILLING.btnPrintClick(Sender: TObject);
var
  i: Integer;
  Mark: TBookmarkStr;

  procedure PrintInvoice;
  var
    ReportParams: TrwReportParams;
    v: Variant;
    CO_INVOICE_MASTER, CO_INVOICE_DETAIL: TevClientDataSet;
  begin
    ctx_UpdateWait('Printing invoice ' + cdsInvoices.FieldByName('INVOICE_NUMBER').AsString +
                             ' for ' + cdsInvoices.FieldByName('COMPANY_NAME').AsString);
    ctx_DataAccess.OpenClient(cdsInvoices.FieldByName('CL_NBR').AsInteger);
    v := ctx_PayrollProcessing{(NotQueued)}.ProcessBilling(0, False,
                               cdsInvoices.FieldByName('INVOICE_NUMBER').AsInteger, True);
    ReportParams := TrwReportParams.Create;
    CO_INVOICE_MASTER := TevClientDataSet.Create(nil);
    CO_INVOICE_DETAIL := TevClientDataSet.Create(nil);
    try
      CO_INVOICE_MASTER.Data := v[0];
      CO_INVOICE_DETAIL.Data := v[1];

      DM_COMPANY.CO.DataRequired('CO_NBR=' + cdsInvoices.FieldByName('CO_NBR').AsString);
      if not DM_COMPANY.CO.FieldByName('NUMBER_OF_INVOICE_COPIES').IsNull then
        ReportParams.Copies := DM_COMPANY.CO.FieldByName('NUMBER_OF_INVOICE_COPIES').AsInteger;
      ReportParams.Add('DataSets', DataSetsToVarArray([CO_INVOICE_MASTER, CO_INVOICE_DETAIL]), False);
      ReportParams.Add('PrintNote', '1', False);

      ctx_RWLocalEngine.StartGroup;
      try
        ctx_RWLocalEngine{(NotQueued)}.CalcPrintReport('Invoice', ReportParams);
      finally
        if cbPreview.Checked then
          ctx_RWLocalEngine.EndGroup(rdtPreview)
        else
          ctx_RWLocalEngine.EndGroup(rdtPrinter);
      end;

    finally
      ReportParams.Free;
      CO_INVOICE_MASTER.Free;
      CO_INVOICE_DETAIL.Free;
    end;
  end;

begin
  inherited;
  ctx_StartWait;
  Mark := cdsInvoices.Bookmark;
  cdsInvoices.DisableControls;
  try
    if grdInvoices.SelectedList.Count = 0 then
      PrintInvoice
    else
      for i := 0 to Pred(grdInvoices.SelectedList.Count) do
      begin
        cdsInvoices.GotoBookmark(grdInvoices.SelectedList[i]);
        PrintInvoice;
      end;
  finally
    cdsInvoices.Bookmark := Mark;
    cdsInvoices.EnableControls;
    ctx_EndWait;
  end;
end;

procedure TMASS_BILLING.pcMainChange(Sender: TObject);
var
  i: Integer;
  Mark: TBookmarkStr;

  procedure CopyCompanyInfo;
  begin
    with lvSelected.Items.Add do
    begin
      Caption := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
      SubItems.Add(DM_TEMPORARY.TMP_CO.FieldByName('NAME').AsString);
    end;
  end;

begin
  inherited;
  if pcMain.ActivePage = tsCreate then
  begin
    lvSelected.Items.Clear;
    Mark := DM_TEMPORARY.TMP_CO.Bookmark;
    DM_TEMPORARY.TMP_CO.DisableControls;
    try
      if grdSelectCompanies.SelectedList.Count = 0 then
        CopyCompanyInfo
      else
        for i := 0 to Pred(grdSelectCompanies.SelectedList.Count) do
        begin
          DM_TEMPORARY.TMP_CO.GotoBookmark(grdSelectCompanies.SelectedList[i]);
          CopyCompanyInfo;
        end;
    finally
      DM_TEMPORARY.TMP_CO.Bookmark := Mark;
      DM_TEMPORARY.TMP_CO.EnableControls;
    end;
    btnCreate.Enabled := (lvSelected.Items.Count > 0);
  end;
end;

procedure TMASS_BILLING.btnCreateClick(Sender: TObject);
var
  i: Integer;
  Mark, MarkCo: TBookmarkStr;
  t: TStringList;
  bManualACH : Boolean;
  Amount: Real;

  procedure CopyServiceInfo;
  begin
    t.Add(DM_SERVICE_BUREAU.SB_SERVICES.FieldByName('SB_SERVICES_NBR').AsString);
  end;

  function CreateInvoice: Real;
  begin
    ctx_UpdateWait('Creating invoice for ' + DM_TEMPORARY.TMP_CO.FieldByName('NAME').AsString);
    Result := ctx_PayrollProcessing.ProcessBillingForServices(DM_TEMPORARY.TMP_CO.FieldByName('CL_NBR').AsInteger,
                                DM_TEMPORARY.TMP_CO.FieldByName('CO_NBR').AsInteger,
                                t.Text, evDateTimePicker1.Date, evMemo1.Text, bManualACH);
  end;

begin
  inherited;
  ctx_StartWait;
  Mark := DM_SERVICE_BUREAU.SB_SERVICES.Bookmark;
  DM_SERVICE_BUREAU.SB_SERVICES.DisableControls;
  MarkCo := DM_TEMPORARY.TMP_CO.Bookmark;
  DM_TEMPORARY.TMP_CO.DisableControls;
  t := TStringList.Create;
  try
    if grdCreate.SelectedList.Count = 0 then
      CopyServiceInfo
    else
      for i := 0 to Pred(grdCreate.SelectedList.Count) do
      begin
        DM_SERVICE_BUREAU.SB_SERVICES.GotoBookmark(grdCreate.SelectedList[i]);
        CopyServiceInfo;
      end;

    bManualACH := False;
    Amount := 0;
    if grdSelectCompanies.SelectedList.Count = 0 then
      Amount := CreateInvoice
    else
      for i := 0 to Pred(grdSelectCompanies.SelectedList.Count) do
      begin
        DM_TEMPORARY.TMP_CO.GotoBookmark(grdSelectCompanies.SelectedList[i]);
        Amount := CreateInvoice;
      end;

    if Amount = 0 then
    begin
      ctx_EndWait;    
      EvMessage('No invoice created.');
      Exit;
    end;

    if bManualACH and
       (EvMessage('Would you like to send the Manual ACH file now?', mtConfirmation, [mbYes, mbNo]) = mrYes) then
             ActivateFrameAs('Operations - Manual ACH', [], 1);


  finally
    DM_TEMPORARY.TMP_CO.Bookmark := MarkCo;
    DM_TEMPORARY.TMP_CO.EnableControls;
    DM_SERVICE_BUREAU.SB_SERVICES.Bookmark := Mark;
    DM_SERVICE_BUREAU.SB_SERVICES.EnableControls;
    ctx_EndWait;
    t.Free;
  end;

  if grdInvoices.Visible then
  begin
    grdInvoices.UnselectAll; //SelectedList.Clear;
    btnOpenInv.Click;
  end;
end;

procedure TMASS_BILLING.cdsInvoiceDetailsCalcFields(DataSet: TDataSet);
begin
  inherited;
  if not DataSet.FieldByName('CO_SERVICES_NBR').IsNull then
    DataSet['SERVICE_NAME'] := DataSet['NAME']
  else
    DataSet['SERVICE_NAME'] := DM_SERVICE_BUREAU.SB_SERVICES.Lookup('SB_SERVICES_NBR', DataSet['SB_SERVICES_NBR'], 'SERVICE_NAME');
end;

procedure TMASS_BILLING.pnlCreateResize(Sender: TObject);
begin
  inherited;
  evMemo1.Width := pnlCreate.Width - evMemo1.Left - 13;
end;

procedure TMASS_BILLING.Activate;
begin
  inherited;
  evDateTimePicker1.Date := Date;
end;

initialization
  RegisterClass(TMASS_BILLING);

end.
