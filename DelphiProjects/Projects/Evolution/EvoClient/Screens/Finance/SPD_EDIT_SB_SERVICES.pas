// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_SERVICES;

interface

uses SFrameEntry, Db,   Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Classes, Wwdatsrc, SDataStructure, Wwdbspin, SDDClasses, 
  ISBasicClasses, SDataDictbureau, EvUIComponents, EvUtils, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIwwDBLookupCombo,
  isUIwwDBComboBox, isUIwwDBEdit, SysUtils, Variants;

type
  TEDIT_SB_SERVICES = class(TFrameEntry)
    pctlEDIT_SB_SERVICES: TevPageControl;
    tshtBrowse: TTabSheet;
    tshtDetails: TTabSheet;
    lablService: TevLabel;
    lablType: TevLabel;
    lablFrequency: TevLabel;
    lablMonth_Number: TevLabel;
    lablBased_Type: TevLabel;
    lablMinimum_Amount: TevLabel;
    lablMaximum_Amount: TevLabel;
    lablReport: TevLabel;
    dedtService: TevDBEdit;
    dtxtSERVICE_NAME: TevDBText;
    lablSalesTaxState: TevLabel;
    wwDBGrid1: TevDBGrid;
    drgpCommision: TevDBRadioGroup;
    wwcbType: TevDBComboBox;
    wwcbFrequency: TevDBComboBox;
    wwseMonth_Number: TevDBSpinEdit;
    wwcbBased_Type: TevDBComboBox;
    wwlcReport: TevDBLookupCombo;
    drgpSales_Taxable: TevDBRadioGroup;
    Bevel1: TEvBevel;
    wwdeMinimum_Amount: TevDBEdit;
    wwdeMaximum_Amount: TevDBEdit;
    wwdeProduct_Code: TevDBEdit;
    Label1: TevLabel;
    CheckBox1: TevCheckBox;
    Label2: TevLabel;
    wwcbWeek: TevDBComboBox;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    evBitBtn1: TevBitBtn;
    evLabel1: TevLabel;
    evDBComboBox1: TevDBComboBox;
    rgPartnerBilling: TevDBRadioGroup;
    evLabel2: TevLabel;
    edtAR_Acct: TevDBEdit;
    evLabel3: TevLabel;
    edtINC_ACCT: TevDBEdit;
    evLabel4: TevLabel;
    edtItem: TevDBEdit;
    procedure CheckBox1Click(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure evBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_SERVICES: TEDIT_SB_SERVICES;

implementation

uses
  SPackageEntry;

{$R *.DFM}

procedure TEDIT_SB_SERVICES.CheckBox1Click(Sender: TObject);
begin
  if CheckBox1.Focused then
    if CheckBox1.Checked and (Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 1, 1) <> 'Y') then
    begin
      if wwdsDetail.DataSet.State = dsBrowse then
        if wwdsDetail.DataSet.RecordCount = 0 then
          wwdsDetail.DataSet.Insert
        else
          wwdsDetail.DataSet.Edit;
      wwdsDetail.DataSet.FieldByName('Filler').AsString := 'Y' + Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 2, Pred(wwdsDetail.DataSet.FieldByName('Filler').Size));
    end
    else if not CheckBox1.Checked and (Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 1, 1) = 'Y') then
    begin
      if wwdsDetail.DataSet.State = dsBrowse then
        if wwdsDetail.DataSet.RecordCount = 0 then
          wwdsDetail.DataSet.Insert
        else
          wwdsDetail.DataSet.Edit;
      wwdsDetail.DataSet.FieldByName('Filler').AsString := 'N' + Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 2, Pred(wwdsDetail.DataSet.FieldByName('Filler').Size));
    end;
end;

procedure TEDIT_SB_SERVICES.wwdsDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) and wwdsDetail.DataSet.Active then
    CheckBox1.Checked := Copy(wwdsDetail.DataSet.FieldByName('Filler').AsString, 1, 1) = 'Y';
end;

function TEDIT_SB_SERVICES.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_SERVICES;
end;

function TEDIT_SB_SERVICES.GetInsertControl: TWinControl;
begin
  Result := dedtService;
end;

procedure TEDIT_SB_SERVICES.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SERVICE_BUREAU.SB_REPORTS, SupportDataSets);
end;

procedure TEDIT_SB_SERVICES.evBitBtn1Click(Sender: TObject);
begin
  inherited;
  (Owner as TFramePackageTmpl).OpenFrame('TEDIT_SB_SERVICES_CALCULATIONS')

end;

procedure TEDIT_SB_SERVICES.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
 s: string;
 
begin

  if (Kind = NavOK) then
  begin
    if wwdsDetail.DataSet.State in [dsEdit, dsInsert] then
    begin
      wwdsDetail.DataSet.UpdateRecord;
      if Trim(VarToStr(wwdsDetail.DataSet.FieldByName('AR_ACCT').NewValue)) <> '' then
         wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, VarToStr(wwdsDetail.DataSet.FieldByName('AR_ACCT').NewValue), 10,50)
      else
      begin
        if not wwdsDetail.DataSet.FieldByName('FILLER').IsNull then
        begin
          s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 10,50);
          if trim(s) <> '' then
             wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, '', 10, 50);
        end;
      end;

      if Trim(VarToStr(wwdsDetail.DataSet.FieldByName('INC_ACCT').NewValue)) <> '' then
         wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, VarToStr(wwdsDetail.DataSet.FieldByName('INC_ACCT').NewValue), 60,50)
      else
      begin
        if not wwdsDetail.DataSet.FieldByName('FILLER').IsNull then
        begin
          s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 60,50);
          if trim(s) <> '' then
             wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, '', 60, 50);
        end;
      end;

      if Trim(VarToStr(wwdsDetail.DataSet.FieldByName('ITEM').NewValue)) <> '' then
         wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, VarToStr(wwdsDetail.DataSet.FieldByName('ITEM').NewValue), 110,70)
      else
      begin
        if not wwdsDetail.DataSet.FieldByName('FILLER').IsNull then
        begin
          s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, 110,70);
          if trim(s) <> '' then
             wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString, '', 110, 70);
        end;
      end;
    end;
  end;

  inherited;
end;


initialization
  RegisterClass(TEDIT_SB_SERVICES);

end.
