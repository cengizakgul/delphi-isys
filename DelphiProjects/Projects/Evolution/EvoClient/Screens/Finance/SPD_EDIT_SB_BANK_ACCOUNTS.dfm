inherited EDIT_SB_BANK_ACCOUNTS: TEDIT_SB_BANK_ACCOUNTS
  Width = 789
  Height = 498
  object dtxtNAME: TevDBText [0]
    Left = 48
    Top = 8
    Width = 561
    Height = 17
    DataField = 'NAME'
    DataSource = wwdsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lablTeam: TevLabel [1]
    Left = 8
    Top = 8
    Width = 25
    Height = 13
    Caption = 'Bank'
  end
  object Bevel1: TBevel [2]
    Left = 0
    Top = 0
    Width = 789
    Height = 33
    Align = alTop
    Style = bsRaised
  end
  object pctlEDIT_SB_BANK_ACCOUNTS: TevPageControl [3]
    Left = 0
    Top = 33
    Width = 789
    Height = 465
    HelpContext = 21014
    ActivePage = tshtBank_Account_Detail
    Align = alClient
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object Splitter1: TevSplitter
        Left = 281
        Top = 0
        Height = 416
      end
      object wwdgTeam: TevDBGrid
        Left = 0
        Top = 0
        Width = 281
        Height = 416
        DisableThemesInTitle = False
        Selected.Strings = (
          'ABA_NUMBER'#9'9'#9'ABA Number'
          'NAME'#9'20'#9'Name'
          'STATE'#9'2'#9'State')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_BANK_ACCOUNTS\wwdgTeam'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsMaster
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwdgTeamMembers: TevDBGrid
        Left = 284
        Top = 0
        Width = 242
        Height = 416
        DisableThemesInTitle = False
        Selected.Strings = (
          'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'Bank Account Number'
          'BANK_ACCOUNT_TYPE'#9'1'#9'Account Type'
          'NAME_DESCRIPTION'#9'40'#9'Description')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_BANK_ACCOUNTS\wwdgTeamMembers'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        OnAfterDrawCell = wwdgTeamMembersAfterDrawCell
        NoFire = False
      end
    end
    object tshtBank_Account_Detail: TTabSheet
      Caption = 'Details'
      object lablBank_Account_Nbr: TevLabel
        Left = 8
        Top = 11
        Width = 128
        Height = 13
        Caption = '~Bank Account Number'
        FocusControl = dedtBank_Account_Nbr
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablDescription: TevLabel
        Left = 152
        Top = 11
        Width = 53
        Height = 13
        Caption = 'Description'
        FocusControl = dedtDescription
      end
      object lablAccount_Type: TevLabel
        Left = 152
        Top = 53
        Width = 80
        Height = 13
        Caption = '~Account Type'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablACH_Originator: TevLabel
        Left = 8
        Top = 283
        Width = 85
        Height = 13
        Caption = '~ACH Originator'
        FocusControl = wwlcACH_Originator
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablLogo: TevLabel
        Left = 264
        Top = 113
        Width = 24
        Height = 13
        Caption = 'Logo'
        FocusControl = dimgLogo
      end
      object lablSignature: TevLabel
        Left = 16
        Top = 131
        Width = 45
        Height = 13
        Caption = 'Signature'
        FocusControl = dimgSignature
      end
      object lablCustom_Header_Record: TevLabel
        Left = 8
        Top = 323
        Width = 111
        Height = 13
        Caption = 'Custom Header Record'
        FocusControl = dedtCustom_Header_Record
      end
      object lablBeginning_Balance: TevLabel
        Left = 8
        Top = 59
        Width = 89
        Height = 13
        Caption = 'Beginning Balance'
      end
      object Label1: TevLabel
        Left = 256
        Top = 283
        Width = 31
        Height = 13
        Caption = 'ABA #'
      end
      object evLabel2: TevLabel
        Left = 8
        Top = 371
        Width = 112
        Height = 13
        Caption = 'Account Locked Up To'
        FocusControl = dedtCustom_Header_Record
      end
      object evLabel3: TevLabel
        Left = 139
        Top = 373
        Width = 111
        Height = 13
        Caption = 'Recurring Wire Number'
      end
      object dedtBank_Account_Nbr: TevDBEdit
        Left = 8
        Top = 27
        Width = 129
        Height = 21
        HelpContext = 21002
        DataField = 'CUSTOM_BANK_ACCOUNT_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtDescription: TevDBEdit
        Left = 152
        Top = 27
        Width = 225
        Height = 21
        HelpContext = 21003
        DataField = 'NAME_DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dimgLogo: TevDBImage
        Left = 264
        Top = 128
        Width = 113
        Height = 108
        HelpContext = 21013
        DataField = 'LOGO_SB_BLOB_NBR'
        DataSource = wwdsDetail
        Stretch = True
        TabOrder = 13
      end
      object dimgSignature: TevDBImage
        Left = 16
        Top = 147
        Width = 233
        Height = 89
        HelpContext = 21012
        DataField = 'SIGNATURE_SB_BLOB_NBR'
        DataSource = wwdsDetail
        Stretch = True
        TabOrder = 11
      end
      object dedtCustom_Header_Record: TevDBEdit
        Left = 8
        Top = 339
        Width = 369
        Height = 21
        HelpContext = 21007
        DataField = 'CUSTOM_HEADER_RECORD'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 17
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object grbxCheck_Information: TevGroupBox
        Left = 384
        Top = 155
        Width = 153
        Height = 225
        HelpContext = 21011
        Caption = 'Check Information'
        TabOrder = 21
        TabStop = True
        object lablNext_Available_Check_Nbr: TevLabel
          Left = 16
          Top = 27
          Width = 130
          Height = 13
          Caption = '~Next Available Number'
          FocusControl = dedtNext_Available_Check_Nbr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablEnd_Check_Nbr: TevLabel
          Left = 16
          Top = 75
          Width = 73
          Height = 13
          Caption = 'Ending Number'
          FocusControl = dedtEnd_Check_Nbr
        end
        object lablFuture_Begin_Check_Nbr: TevLabel
          Left = 16
          Top = 123
          Width = 120
          Height = 13
          Caption = 'Future Beginning Number'
          FocusControl = dedtFuture_Begin_Check_Nbr
        end
        object lablFuture_End_Check_Nbr: TevLabel
          Left = 16
          Top = 171
          Width = 106
          Height = 13
          Caption = 'Future Ending Number'
          FocusControl = dedtFuture_End_Check_Nbr
        end
        object dedtNext_Available_Check_Nbr: TevDBEdit
          Left = 16
          Top = 43
          Width = 64
          Height = 21
          DataField = 'NEXT_AVAILABLE_CHECK_NUMBER'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtEnd_Check_Nbr: TevDBEdit
          Left = 16
          Top = 91
          Width = 64
          Height = 21
          DataField = 'END_CHECK_NUMBER'
          DataSource = wwdsDetail
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtFuture_Begin_Check_Nbr: TevDBEdit
          Left = 16
          Top = 139
          Width = 64
          Height = 21
          DataField = 'NEXT_BEGIN_CHECK_NUMBER'
          DataSource = wwdsDetail
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object dedtFuture_End_Check_Nbr: TevDBEdit
          Left = 16
          Top = 187
          Width = 64
          Height = 21
          DataField = 'NEXT_END_CHECK_NUMBER'
          DataSource = wwdsDetail
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object drgpBank_Account_Type: TevDBRadioGroup
        Left = 384
        Top = 19
        Width = 153
        Height = 75
        HelpContext = 21009
        Caption = '~Bank Account Type'
        DataField = 'BANK_ACCOUNT_TYPE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Checking'
          'Savings'
          'Money Market')
        ParentFont = False
        TabOrder = 19
        Values.Strings = (
          'C'
          'S'
          'M')
      end
      object drgpBank_Returns: TevDBRadioGroup
        Left = 384
        Top = 99
        Width = 153
        Height = 50
        HelpContext = 21010
        Caption = '~Bank Returns'
        Columns = 2
        DataField = 'BANK_RETURNS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 20
        Values.Strings = (
          'Y'
          'N')
      end
      object wwlcACH_Originator: TevDBLookupCombo
        Left = 8
        Top = 299
        Width = 241
        Height = 21
        HelpContext = 21005
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME')
        DataField = 'ACH_ORIGIN_SB_BANKS_NBR'
        DataSource = wwdsDetail
        LookupTable = cdBanks
        LookupField = 'SB_BANKS_NBR'
        Style = csDropDownList
        TabOrder = 15
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object bbtnLoad_Signature: TevBitBtn
        Left = 16
        Top = 243
        Width = 233
        Height = 25
        Caption = 'Load Signature'
        TabOrder = 12
        OnClick = bbtnLoad_SignatureClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
          DDDDDFFFFFFFFFFDDDDD800000000008DDDDF8888888888FDDDD3B0333333330
          8DDD8D8888888888FDDD3BB03333333308DD8DD8888888888FDD3BBB03333333
          308D8DDD8888888888FD3BBBB033333333088DDDD8888888888F3BBBBB000000
          00008DDDDD88888888883B3443BBB4B43DDD8DD88DDDDDDDDDDD3B4BB4333384
          DDDD8D8DD8DDDDD8DDDD3B4BB4DD48D4DDDD8D8DD8DD8DD8DDDDD33434D4DD48
          84D8DDD8D8D8DD8DD8DDD8DD84D4D4D4D4D4DDDDD8D8D8D8D8D8DD4D4DD4D4D4
          D4D4DD8D8DD8D8D8D8D8DDD4DD8DDD48DD48DDD8DDDDDD8DDD8DDD4D8DD48DDD
          8DDDDD8DDDD8DDDDDDDDDD84DDDD4DDDDDDDDDD8DDDD8DDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object bbtnLoad_Logo: TevBitBtn
        Left = 264
        Top = 243
        Width = 113
        Height = 25
        Caption = 'Load Logo'
        TabOrder = 14
        OnClick = bbtnLoad_LogoClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
          DDDDDFFFFFFFFFFDDDDD800000000008DDDDF8888888888FDDDD3B0333333330
          8DDD8D8888888888FDDD3B803333333308DD8DD8888888888FDD384B03333333
          308D8D8F8888888888FD384BB033333333088D8FD8888888888F3B8448000000
          00008DD88F88888888883BBBB844F4BB3DDD8DDDDDDDDDDDDDDD3BBBB8FF4433
          DDDD8DDDDDFF88F8FDDD3BBB844444DDDDDD8DDDD88888FDDDDDD33384FF44DD
          DDDDD888D8FD88FDDDDDDDDDD84444DDDDDDDDDDDD8888FDDDDDDDDDDD844DDD
          DDDDDDDDDDD88FDDDDDDDDDDD84444DDDDDDDDDDDD8888FDDDDDDDDDD84444DD
          DDDDDDDDDD8888FDDDDDDDDDDD844DDDDDDDDDDDDDD88FDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object wwdeBeginning_Balance: TevDBEdit
        Left = 8
        Top = 75
        Width = 129
        Height = 21
        HelpContext = 21008
        DataField = 'BEGINNING_BALANCE'
        DataSource = wwdsDetail
        Picture.PictureMask = 
          '{{{#[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]],({{#' +
          '[#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]),[-]{{#[' +
          '#][#]{{;,###*[;,###]},*#}[.*#]},.#*#}[E[[+,-]#[#][#]]]}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBLookupCombo1: TevDBLookupCombo
        Left = 256
        Top = 299
        Width = 121
        Height = 21
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'ABA_NUMBER'#9'9'#9'ABA_NUMBER'#9'F')
        DataField = 'ACH_ORIGIN_SB_BANKS_NBR'
        DataSource = wwdsDetail
        LookupTable = cdBanks
        LookupField = 'SB_BANKS_NBR'
        Style = csDropDownList
        Enabled = False
        TabOrder = 16
        AutoDropDown = True
        ShowButton = False
        PreciseEditRegion = False
        AllowClearKey = False
      end
      object GroupBox1: TevGroupBox
        Left = 544
        Top = 19
        Width = 137
        Height = 126
        Caption = 'EFT Info'
        TabOrder = 22
        object evLabel1: TevLabel
          Left = 8
          Top = 24
          Width = 64
          Height = 13
          Caption = 'Batch Filer ID'
        end
        object Label2: TevLabel
          Left = 8
          Top = 72
          Width = 87
          Height = 13
          Caption = 'Master Inquiry PIN'
        end
        object wwDBEdit1: TevDBEdit
          Left = 8
          Top = 40
          Width = 121
          Height = 21
          DataField = 'BATCH_FILER_ID'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit2: TevDBEdit
          Left = 8
          Top = 88
          Width = 121
          Height = 21
          DataField = 'MASTER_INQUIRY_PIN'
          DataSource = wwdsDetail
          TabOrder = 1
          UnboundDataType = wwDefault
          UsePictureMask = False
          WantReturns = False
          WordWrap = False
        end
      end
      object dtpLockDate: TevDateTimePicker
        Left = 8
        Top = 387
        Width = 97
        Height = 21
        Date = 37420.000000000000000000
        Time = 37420.000000000000000000
        ShowCheckbox = True
        TabOrder = 18
        OnChange = dtpLockDateChange
      end
      object evCheckBox1: TevDBCheckBox
        Left = 152
        Top = 69
        Width = 97
        Height = 13
        Caption = 'Operating'
        DataField = 'OPERATING_ACCOUNT'
        DataSource = wwdsDetail
        TabOrder = 3
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object evCheckBox2: TevDBCheckBox
        Left = 152
        Top = 82
        Width = 97
        Height = 17
        Caption = 'Billing'
        DataField = 'BILLING_ACCOUNT'
        DataSource = wwdsDetail
        TabOrder = 4
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object evCheckBox3: TevDBCheckBox
        Left = 152
        Top = 96
        Width = 97
        Height = 17
        Caption = 'ACH'
        DataField = 'ACH_ACCOUNT'
        DataSource = wwdsDetail
        TabOrder = 5
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object evCheckBox4: TevDBCheckBox
        Left = 251
        Top = 55
        Width = 97
        Height = 17
        Caption = 'Trust'
        DataField = 'TRUST_ACCOUNT'
        DataSource = wwdsDetail
        TabOrder = 6
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object evCheckBox5: TevDBCheckBox
        Left = 251
        Top = 69
        Width = 97
        Height = 17
        Caption = 'Tax'
        DataField = 'TAX_ACCOUNT'
        DataSource = wwdsDetail
        TabOrder = 7
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object evCheckBox6: TevDBCheckBox
        Left = 251
        Top = 82
        Width = 97
        Height = 17
        Caption = 'OBC'
        DataField = 'OBC_ACCOUNT'
        DataSource = wwdsDetail
        TabOrder = 8
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object evCheckBox7: TevDBCheckBox
        Left = 251
        Top = 96
        Width = 97
        Height = 17
        Caption = 'Workers Comp'
        DataField = 'WORKERS_COMP_ACCOUNT'
        DataSource = wwdsDetail
        TabOrder = 9
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object evDBEdit1: TevDBEdit
        Left = 139
        Top = 389
        Width = 121
        Height = 21
        DataField = 'RECCURING_WIRE_NUMBER'
        DataSource = wwdsDetail
        TabOrder = 23
        UnboundDataType = wwDefault
        UsePictureMask = False
        WantReturns = False
        WordWrap = False
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 544
        Top = 157
        Width = 153
        Height = 140
        HelpContext = 11513
        Caption = '~Bank Check'
        DataField = 'BANK_CHECK'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 24
      end
    end
    object tsBlocks: TTabSheet
      Caption = 'Blocks'
      ImageIndex = 2
      object rgSuppressOffset: TevDBRadioGroup
        Left = 7
        Top = 9
        Width = 202
        Height = 64
        HelpContext = 21010
        Caption = '~Suppress Offset Amount'
        Columns = 2
        DataField = 'SUPPRESS_OFFSET_ACCOUNT'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Values.Strings = (
          'Y'
          'N')
      end
      object rgBlockNegativeChecks: TevDBRadioGroup
        Left = 7
        Top = 76
        Width = 202
        Height = 50
        HelpContext = 21010
        Caption = '~Block Negative Direct Deposits'
        Columns = 2
        DataField = 'BLOCK_NEGATIVE_CHECKS'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'Y'
          'N')
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SB_BANKS.SB_BANKS
    Left = 149
    Top = 2
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_BANK_ACCOUNTS.SB_BANK_ACCOUNTS
    OnDataChange = wwdsDetailDataChange
    OnUpdateData = wwdsDetailUpdateData
  end
  inherited wwdsList: TevDataSource
    Top = 2
  end
  object odlgGetBitmap: TOpenDialog
    Filter = 'Windows BitMap (*.bmp)|*.bmp'
    Options = [ofReadOnly, ofHideReadOnly]
    Left = 272
    Top = 227
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 272
    Top = 16
  end
  object cdBanks: TevClientDataSet
    Left = 344
    Top = 16
  end
end
