// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SClientFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons,  StdCtrls, Grids, Wwdbigrd, Wwdbgrid, Db,
   Wwdatsrc, SDataStructure, SDDClasses, isBaseClasses,
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvDataAccessComponents,
  ISDataAccessComponents, SDataDicttemp, EvUIComponents, EvClientDataSet,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton;

const
  CM_REFRESH = WM_USER + 1;

type
  TBeforeClientAttach = procedure () of object;

  TClientFrame = class(TFrame)
    gClientsAvail: TevDBGrid;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    gClientsSelected: TevDBGrid;
    sbAdd: TevSpeedButton;
    sbRemove: TevSpeedButton;
    DM_TEMPORARY: TDM_TEMPORARY;
    cdClientsAvail: TevClientDataSet;
    cdClientsAvailCL_NBR: TIntegerField;
    cdClientsAvailNUMBER: TStringField;
    cdClientsAvailNAME: TStringField;
    dsClientsSelected: TevDataSource;
    cdClientsSelected: TevClientDataSet;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    dsDetail: TevDataSource;
    dsMaster: TevDataSource;
    procedure sbAddClick(Sender: TObject);
    procedure sbRemoveClick(Sender: TObject);
    procedure gClientsAvailDblClick(Sender: TObject);
    procedure gClientsSelectedDblClick(Sender: TObject);
  private
    FNeedToBeRefreshed: Boolean;
    FBeforeClientAttach: TBeforeClientAttach;
    FActiveMode: Boolean;
    FSB_USER_NBR: Integer;
    FSB_SEC_GROUPS_NBR: Integer;
    FOnChange: TNotifyEvent;
    procedure RefreshInfo;
    procedure SetActiveMode(const Value: Boolean);
    procedure CMRefresh(var Message: TMessage); message CM_REFRESH;
    procedure SetSB_USER_NBR(const Value: Integer);
    procedure SetSB_SEC_GROUPS_NBR(const Value: Integer);
    procedure RefreshData;
  public
    property  BeforeClientAttach: TBeforeClientAttach read FBeforeClientAttach write FBeforeClientAttach;
    property  OnChange: TNotifyEvent read FOnChange write FOnChange;
    procedure InvalidateData;
    property  ActiveMode: Boolean read FActiveMode write SetActiveMode;
    property  SB_USER_NBR: Integer read FSB_USER_NBR write SetSB_USER_NBR;
    property  SB_SEC_GROUPS_NBR: Integer read FSB_SEC_GROUPS_NBR write SetSB_SEC_GROUPS_NBR;
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TClientFrame.sbAddClick(Sender: TObject);
var
  i: Integer;
  sMasterFieldName: string;
begin
  if Assigned(BeforeClientAttach) then
    BeforeClientAttach;
  sMasterFieldName := dsMaster.DataSet.Name+ '_NBR';
  cdClientsAvail.DisableControls;
  cdClientsSelected.DisableControls;
  DM_SERVICE_BUREAU.SB_SEC_CLIENTS.DisableControls;
  try
    dsMaster.DataSet.CheckBrowseMode;
    for i := 0 to gClientsAvail.SelectedList.Count-1 do
    begin
      cdClientsAvail.GotoBookmark(gClientsAvail.SelectedList[i]);
      cdClientsSelected.Append;
      cdClientsSelected['CL_NBR;NUMBER;NAME'] := cdClientsAvail['CL_NBR;NUMBER;NAME'];
      cdClientsSelected.Post;
      cdClientsAvail.Delete;
      DM_SERVICE_BUREAU.SB_SEC_CLIENTS.Append;
      DM_SERVICE_BUREAU.SB_SEC_CLIENTS['CL_NBR'] := cdClientsSelected['CL_NBR'];
      DM_SERVICE_BUREAU.SB_SEC_CLIENTS[sMasterFieldName] := dsMaster.DataSet[sMasterFieldName];
      DM_SERVICE_BUREAU.SB_SEC_CLIENTS.Post;
    end;
    gClientsAvail.UnselectAll;
  finally
    DM_SERVICE_BUREAU.SB_SEC_CLIENTS.EnableControls;
    cdClientsSelected.EnableControls;
    cdClientsAvail.EnableControls;
  end;

  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TClientFrame.sbRemoveClick(Sender: TObject);
var
  i: Integer;
begin
  cdClientsAvail.DisableControls;
  cdClientsSelected.DisableControls;
  DM_SERVICE_BUREAU.SB_SEC_CLIENTS.DisableControls;
  try
    for i := 0 to gClientsSelected.SelectedList.Count-1 do
    begin
      cdClientsSelected.GotoBookmark(gClientsSelected.SelectedList[i]);
      cdClientsAvail.Append;
      cdClientsAvail['CL_NBR;NUMBER;NAME'] := cdClientsSelected['CL_NBR;NUMBER;NAME'];
      cdClientsAvail.Post;
      cdClientsSelected.Delete;
      if DM_SERVICE_BUREAU.SB_SEC_CLIENTS.Locate('CL_NBR', cdClientsAvail['CL_NBR'], []) then
        DM_SERVICE_BUREAU.SB_SEC_CLIENTS.Delete;
    end;
    gClientsSelected.UnselectAll;
  finally
    DM_SERVICE_BUREAU.SB_SEC_CLIENTS.EnableControls;
    cdClientsSelected.EnableControls;
    cdClientsAvail.EnableControls;
  end;

  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TClientFrame.gClientsAvailDblClick(Sender: TObject);
begin
  sbAdd.Click;
end;

procedure TClientFrame.gClientsSelectedDblClick(Sender: TObject);
begin
  sbRemove.Click;
end;

procedure TClientFrame.SetActiveMode(const Value: Boolean);
begin
  FActiveMode := Value;
  RefreshData;
end;

procedure TClientFrame.RefreshInfo;
var
  ds: TDataSet;
  d: TDataSource;
  SelClients: IisStringList;
  Fld1, Fld2, Fld3: TField;
begin
  cdClientsAvail.DisableControls;
  cdClientsSelected.DisableControls;
  DM_TEMPORARY.TMP_CL.DisableControls;
  try
    gClientsAvail.UnselectAll;
    gClientsSelected.UnselectAll;

    cdClientsAvail.Close;
    cdClientsAvail.CreateDataSet;
    cdClientsSelected.Close;
    cdClientsSelected.CreateDataSet;

    SelClients := TisStringList.CreateUnique;

    dsDetail.DataSet.First;
    Fld1 := dsDetail.DataSet.FieldByName('CL_NBR');
    while not dsDetail.DataSet.Eof do
    begin
      SelClients.Add(Fld1.AsString);
      dsDetail.DataSet.Next;
    end;

    DM_TEMPORARY.TMP_CL.First;
    Fld1 := DM_TEMPORARY.TMP_CL.FieldByName('CL_NBR');
    Fld2 := DM_TEMPORARY.TMP_CL.FieldByName('CUSTOM_CLIENT_NUMBER');
    Fld3 := DM_TEMPORARY.TMP_CL.FieldByName('NAME');
    while not DM_TEMPORARY.TMP_CL.Eof do
    begin
      if SelClients.IndexOf(Fld1.AsString) <> -1 then
        ds := cdClientsSelected
      else
        ds := cdClientsAvail;
      ds.AppendRecord([Fld1.AsInteger, Fld2.AsString, Fld3.AsString]);
      DM_TEMPORARY.TMP_CL.Next;
    end;
    SelClients := nil;

    d := gClientsAvail.DataSource;
    gClientsAvail.DataSource := nil;
    gClientsAvail.DataSource := d;

    d := gClientsSelected.DataSource;
    gClientsSelected.DataSource := nil;
    gClientsSelected.DataSource := d;
  finally
    cdClientsAvail.EnableControls;
    cdClientsSelected.EnableControls;
    DM_TEMPORARY.TMP_CL.EnableControls;
  end;
end;

procedure TClientFrame.RefreshData;
begin
  if FNeedToBeRefreshed and ActiveMode then
  begin
    FNeedToBeRefreshed := False;
    PostMessage(Handle, CM_REFRESH, 0, 0);
  end;
end;

procedure TClientFrame.CMRefresh(var Message: TMessage);
begin
  RefreshInfo;
end;

procedure TClientFrame.SetSB_SEC_GROUPS_NBR(const Value: Integer);
begin
  if FSB_SEC_GROUPS_NBR <> Value then
  begin
    FSB_USER_NBR := 0;
    FSB_SEC_GROUPS_NBR := Value;
    InvalidateData;
    RefreshData;
  end;
end;

procedure TClientFrame.SetSB_USER_NBR(const Value: Integer);
begin
  if FSB_USER_NBR <> Value then
  begin
    FSB_SEC_GROUPS_NBR := 0;
    FSB_USER_NBR := Value;
    InvalidateData;
    RefreshData;
  end;
end;

procedure TClientFrame.InvalidateData;
begin
  FNeedToBeRefreshed := True;
end;

end.
