inherited EDIT_SB_USER: TEDIT_SB_USER
  Width = 754
  Height = 442
  object pctlEDIT_SB_USER: TevPageControl [0]
    Left = 0
    Top = 39
    Width = 754
    Height = 403
    HelpContext = 37502
    ActivePage = tshtUser_Detail
    Align = alClient
    TabOrder = 0
    TabStop = False
    OnChange = pctlEDIT_SB_USERChange
    OnChanging = pctlEDIT_SB_USERChanging
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object wwdgEDIT_SB_USER: TevDBGrid
        Left = 0
        Top = 0
        Width = 746
        Height = 375
        DisableThemesInTitle = False
        Selected.Strings = (
          'USER_ID'#9'20'#9'User ID'#9'F'
          'FIRST_NAME'#9'20'#9'First Name'#9'F'
          'LAST_NAME'#9'30'#9'Last Name'#9'F'
          'ACTIVE_USER'#9'1'#9'Active'#9'F'
          'ClientNumber'#9'20'#9'Client Number'#9'F'
          'SB_USER_NBR'#9'10'#9'User Nbr'#9'F'
          'StereoType'#9'10'#9'Type   '#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_USER\wwdgEDIT_SB_USER'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnCalcCellColors = wwdgEDIT_SB_USERCalcCellColors
        OnDblClick = wwdgEDIT_SB_USERDblClick
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tshtUser_Detail: TTabSheet
      Caption = 'Details'
      object lablUser_Signature: TevLabel
        Left = 16
        Top = 197
        Width = 70
        Height = 13
        Caption = 'User Signature'
      end
      object lablLast_Name: TevLabel
        Left = 16
        Top = 56
        Width = 60
        Height = 16
        Caption = '~Last Name'
        FocusControl = dedtLast_Name
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablFirst_Name: TevLabel
        Left = 16
        Top = 104
        Width = 59
        Height = 16
        Caption = '~First Name'
        FocusControl = dedtFirst_Name
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablMiddle_Initial: TevLabel
        Left = 176
        Top = 104
        Width = 24
        Height = 13
        Caption = 'Initial'
        FocusControl = dedtMiddle_Initial
      end
      object lablDepartment: TevLabel
        Left = 352
        Top = 165
        Width = 64
        Height = 16
        Caption = '~Department'
        FocusControl = wwcbDepartment
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablPassword_Change_Date: TevLabel
        Left = 352
        Top = 121
        Width = 121
        Height = 16
        Caption = '~Password Change Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablSecurity_Level: TevLabel
        Left = 496
        Top = 121
        Width = 76
        Height = 16
        Caption = '~Security Level'
        FocusControl = wwcbSecurity_Level
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablUser_ID: TevLabel
        Left = 16
        Top = 8
        Width = 45
        Height = 16
        Caption = '~User ID'
        FocusControl = dedtUser_ID
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TevLabel
        Left = 16
        Top = 152
        Width = 69
        Height = 13
        Caption = 'E-mail Address'
        FocusControl = dedtEmail_Address
      end
      object evLabel1: TevLabel
        Left = 208
        Top = 24
        Width = 94
        Height = 13
        Caption = '(no spaces allowed)'
      end
      object lablAccountant: TevLabel
        Left = 16
        Top = 307
        Width = 55
        Height = 13
        Caption = 'Accountant'
      end
      object lAutoSec: TLabel
        Left = 472
        Top = 319
        Width = 175
        Height = 26
        Caption = 'Security rights of this account configured automatically'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object evLabel2: TevLabel
        Left = 496
        Top = 165
        Width = 101
        Height = 16
        Caption = '~Analytics Personnel'
        FocusControl = cbAnalyticsPersonel
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dedtLast_Name: TevDBEdit
        Left = 16
        Top = 72
        Width = 184
        Height = 21
        DataField = 'LAST_NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtFirst_Name: TevDBEdit
        Left = 16
        Top = 120
        Width = 145
        Height = 21
        DataField = 'FIRST_NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtMiddle_Initial: TevDBEdit
        Left = 176
        Top = 120
        Width = 25
        Height = 21
        DataField = 'MIDDLE_INITIAL'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object drgpActive_User: TevDBRadioGroup
        Left = 352
        Top = 16
        Width = 180
        Height = 38
        Caption = '~Active User'
        Columns = 2
        DataField = 'ACTIVE_USER'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 7
        Values.Strings = (
          'Y'
          'N')
        OnEnter = drgpActive_UserEnter
      end
      object wwcbDepartment: TevDBComboBox
        Left = 352
        Top = 181
        Width = 129
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'DEPARTMENT'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 11
        UnboundDataType = wwDefault
        OnChange = wwcbDepartmentChange
      end
      object wwcbSecurity_Level: TevDBComboBox
        Left = 496
        Top = 137
        Width = 129
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'SECURITY_LEVEL'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 10
        UnboundDataType = wwDefault
        OnDropDown = wwcbSecurity_LevelDropDown
      end
      object Panel1: TevPanel
        Left = 352
        Top = 60
        Width = 273
        Height = 54
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 8
        object lablPassword: TevLabel
          Left = 8
          Top = 6
          Width = 55
          Height = 16
          Caption = '~Password'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablConfirmPassword: TevLabel
          Left = 144
          Top = 6
          Width = 93
          Height = 16
          Caption = '~Confirm Password'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object editPassword: TevEdit
          Left = 8
          Top = 24
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 0
          OnChange = editPasswordChange
        end
        object editConfirmPassword: TevEdit
          Left = 144
          Top = 24
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 1
          OnChange = editPasswordChange
        end
      end
      object dedtUser_ID: TevDBEdit
        Left = 16
        Top = 24
        Width = 185
        Height = 21
        DataField = 'USER_ID'
        DataSource = wwdsDetail
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdpPassword_Change_Date: TevDBDateTimePicker
        Left = 352
        Top = 137
        Width = 129
        Height = 21
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        CalendarAttributes.PopupYearOptions.StartYear = 2000
        DataField = 'PASSWORD_CHANGE_DATE'
        DataSource = wwdsDetail
        Epoch = 1950
        ShowButton = True
        TabOrder = 9
      end
      object dedtEmail_Address: TevDBEdit
        Left = 16
        Top = 168
        Width = 145
        Height = 21
        DataField = 'EMAIL_ADDRESS'
        DataSource = wwdsDetail
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object bbtnLoad: TevBitBtn
        Left = 256
        Top = 213
        Width = 75
        Height = 25
        Hint = 'Load signature graphic'
        Caption = 'Load'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = bbtnLoadClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
          DDDDDFFFFFFFFFFDDDDD800000000008DDDDF8888888888FDDDD3B0333333330
          8DDD8D8888888888FDDD3BB03333333308DD8DD8888888888FDD3BBB03333333
          308D8DDD8888888888FD3BBBB033333333088DDDD8888888888F3BBBBB000000
          00008DDDDD88888888883B3443BBB4B43DDD8DD88DDDDDDDDDDD3B4BB4333384
          DDDD8D8DD8DDDDD8DDDD3B4BB4DD48D4DDDD8D8DD8DD8DD8DDDDD33434D4DD48
          84D8DDD8D8D8DD8DD8DDD8DD84D4D4D4D4D4DDDDD8D8D8D8D8D8DD4D4DD4D4D4
          D4D4DD8D8DD8D8D8D8D8DDD4DD8DDD48DD48DDD8DDDDDD8DDD8DDD4D8DD48DDD
          8DDDDD8DDDD8DDDDDDDDDD84DDDD4DDDDDDDDDD8DDDD8DDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object gbUserClientDb: TevGroupBox
        Left = 352
        Top = 207
        Width = 273
        Height = 103
        Caption = '~User'#39's stereotype'
        TabOrder = 12
        object rbAllClients: TevRadioButton
          Left = 8
          Top = 18
          Width = 81
          Height = 17
          Caption = 'S/B internal'
          TabOrder = 0
          OnClick = rbOneClientClick
        end
        object rbOneClient: TevRadioButton
          Left = 8
          Top = 53
          Width = 81
          Height = 17
          Caption = 'Remote'
          TabOrder = 2
          OnClick = rbOneClientClick
        end
        object wwlcClientNumber: TevDBLookupCombo
          Left = 89
          Top = 49
          Width = 174
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'CUSTOM_CLIENT_NUMBER'#9'10'#9'CUSTOM_CLIENT_NUMBER'#9'F'
            'NAME'#9'40'#9'NAME'#9'F')
          LookupTable = DM_TMP_CL.TMP_CL
          LookupField = 'CL_NBR'
          Style = csDropDownList
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnChange = wwlcClientNumberChange
          OnEnter = wwlcClientNumberEnter
          OnExit = wwlcClientNumberExit
        end
        object rbWebUser: TevRadioButton
          Left = 8
          Top = 36
          Width = 81
          Height = 17
          Caption = 'Web'
          TabOrder = 1
          OnClick = rbOneClientClick
        end
        object chbEvolutionPayroll: TevDBCheckBox
          Left = 8
          Top = 79
          Width = 169
          Height = 17
          Caption = 'Evolution Payroll'
          DataField = 'EVOLUTION_PRODUCT'
          DataSource = wwdsDetail
          PopupMenu = pmEvoProduct
          TabOrder = 4
          ValueChecked = 'P'
          ValueUnchecked = 'N'
        end
      end
      object btnSecRights: TevBitBtn
        Left = 352
        Top = 320
        Width = 113
        Height = 25
        Caption = 'User rights'
        TabOrder = 13
        OnClick = btnSecRightsClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDD8
          88DDDDDDDDDDDDDFFFDDD44444444D80008DD8888888DDF888FD44444444480B
          BB0888888888DF88888F4444444443B333B088888888D88DDD8844444F4443B0
          03B088888D88D88FFF88444F4444D3BBBBB0888D8888D888888D4444444DDD3B
          BB0D8888888DDD8888DDDD4744DDDDD3B0DDDD8D88DDDDD88FDDD47FF74DDDD3
          B0DDD8DDDD8DDDD88FDD44FF744DDDD3B0DD88DDD88DDDD88FDD44FFFF4DDDD3
          B08888DDDD8DDDD88FDD444F444DDDD3B000888D888DDDD88FFF444FFF4DDDD3
          BBB0888DDD8DDDD8888F4444444DDDD3BBB08888888DDDD8888F4444444DDDD3
          B0338888888DDDD88FDDD44444DDDDDD38DDD88888DDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object wwlcAccountant: TevDBLookupCombo
        Left = 16
        Top = 323
        Width = 236
        Height = 21
        HelpContext = 10563
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'NAME'#9'40'#9'NAME'
          'ADDRESS1'#9'30'#9'ADDRESS1'
          'ADDRESS2'#9'30'#9'ADDRESS2'
          'CITY'#9'20'#9'CITY'
          'STATE'#9'3'#9'STATE'
          'ZIP_CODE'#9'10'#9'ZIP_CODE')
        DataField = 'SB_ACCOUNTANT_NBR'
        DataSource = wwdsDetail
        LookupTable = DM_SB_ACCOUNTANT.SB_ACCOUNTANT
        LookupField = 'SB_ACCOUNTANT_NBR'
        Style = csDropDownList
        TabOrder = 6
        AutoDropDown = True
        ShowButton = True
        PreciseEditRegion = False
        AllowClearKey = True
      end
      object pBlock: TevPanel
        Left = 0
        Top = 335
        Width = 746
        Height = 40
        Align = alBottom
        Alignment = taLeftJustify
        Caption = 
          '    This account is blocked because of too many unsuccessful log' +
          'in attempts'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 14
        Visible = False
        object evButton1: TevButton
          Left = 456
          Top = 8
          Width = 121
          Height = 25
          Caption = 'Unblock account'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = evButton1Click
          Color = clBlack
          Margin = 0
        end
      end
      object evDBRadioGroup1: TevDBRadioGroup
        Left = 176
        Top = 152
        Width = 113
        Height = 37
        Caption = 'HR Personnel '
        Columns = 2
        DataField = 'HR_PERSONNEL'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 15
        Values.Strings = (
          'Y'
          'N')
        OnEnter = drgpActive_UserEnter
      end
      object dimgUserSignature: TevDBImage
        Left = 16
        Top = 211
        Width = 233
        Height = 89
        HelpContext = 21012
        DataField = 'USER_SIGNATURE'
        DataSource = wwdsDetail
        Stretch = True
        TabOrder = 16
      end
      object cbAnalyticsPersonel: TevDBComboBox
        Left = 496
        Top = 181
        Width = 129
        Height = 21
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = False
        AutoDropDown = True
        AutoSize = False
        DataField = 'ANALYTICS_PERSONNEL'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 17
        UnboundDataType = wwDefault
        OnChange = wwcbDepartmentChange
      end
    end
    object tshtOptions: TTabSheet
      Caption = 'Options'
      ImageIndex = 2
      TabVisible = False
      object Label1: TevLabel
        Left = 8
        Top = 152
        Width = 80
        Height = 16
        Caption = '~User Functions'
        FocusControl = dmemUserFunctions
      end
      object Label2: TevLabel
        Left = 8
        Top = 24
        Width = 108
        Height = 16
        Caption = '~User Update Options'
        FocusControl = dmemUserOptions
      end
      object dmemUserFunctions: TDBMemo
        Left = 8
        Top = 168
        Width = 233
        Height = 89
        DataField = 'USER_FUNCTIONS'
        DataSource = wwdsDetail
        TabOrder = 0
      end
      object dmemUserOptions: TDBMemo
        Left = 8
        Top = 40
        Width = 233
        Height = 89
        DataField = 'USER_UPDATE_OPTIONS'
        DataSource = wwdsDetail
        TabOrder = 1
      end
    end
    object tshtUser_Groups: TTabSheet
      Caption = 'Groups'
      ImageIndex = 3
      object Label5: TevLabel
        Left = 8
        Top = 8
        Width = 78
        Height = 13
        Caption = 'Available groups'
      end
      object Label6: TevLabel
        Left = 352
        Top = 8
        Width = 78
        Height = 13
        Caption = 'Assigned groups'
      end
      object SpeedButton1: TevSpeedButton
        Left = 279
        Top = 288
        Width = 74
        Height = 22
        Caption = 'Add'
        HideHint = True
        AutoSize = False
        OnClick = AddGroup
        NumGlyphs = 2
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
          DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
          DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
          78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
          8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
          F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
          F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
          DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
          DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
        Layout = blGlyphRight
        ParentColor = False
        ShortCut = 0
      end
      object SpeedButton2: TevSpeedButton
        Left = 279
        Top = 328
        Width = 74
        Height = 22
        Caption = 'Remove'
        HideHint = True
        AutoSize = False
        OnClick = RemoveFromGroup
        NumGlyphs = 2
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
          DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
          DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
          78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
          448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
          F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
          F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
          44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
          DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
        ParentColor = False
        ShortCut = 0
      end
      object wwDBGroupGridAvail: TevDBGrid
        Left = 0
        Top = 32
        Width = 273
        Height = 409
        DisableThemesInTitle = False
        Selected.Strings = (
          'NAME'#9'40'#9'Group name'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_USER\wwDBGroupGridAvail'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwGroups
        MultiSelectOptions = [msoShiftSelect]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = AddGroup
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object wwDBGroupGridSelected: TevDBGrid
        Left = 360
        Top = 39
        Width = 273
        Height = 402
        DisableThemesInTitle = False
        Selected.Strings = (
          'NAME'#9'40'#9'Group name'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_USER\wwDBGroupGridSelected'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwGroupsSel
        MultiSelectOptions = [msoShiftSelect]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = RemoveFromGroup
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tsClients: TTabSheet
      Caption = 'Clients'
      ImageIndex = 4
      OnHide = tsClientsHide
      OnShow = tsClientsShow
      inline ClientFrame1: TClientFrame
        Left = 0
        Top = 0
        Width = 746
        Height = 375
        Align = alClient
        TabOrder = 0
        inherited sbAdd: TevSpeedButton
          Top = 288
          Width = 74
          OnClick = ClientFrame1sbAddClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
            DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
            DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
            78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
            8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
            F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
            F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
            DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
            DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
          Layout = blGlyphRight
        end
        inherited sbRemove: TevSpeedButton
          Left = 280
          Top = 328
          Width = 73
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
            DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
            DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
            78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
            448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
            F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
            F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
            44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
            DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
        end
        inherited gClientsAvail: TevDBGrid
          Left = 0
          Width = 273
          Height = 409
        end
        inherited gClientsSelected: TevDBGrid
          Left = 360
          Width = 273
          Height = 409
        end
      end
    end
    object tsDBDT: TTabSheet
      Caption = 'D/B/D/T'
      ImageIndex = 5
      OnHide = tsDBDTHide
      OnShow = tsDBDTShow
      inline DBDTFrame1: TDBDTFrame
        Left = 0
        Top = 0
        Width = 746
        Height = 375
        Align = alClient
        TabOrder = 0
        inherited Panel1: TPanel
          Width = 746
          Height = 375
          inherited tv: TevTreeView
            Width = 742
            Height = 371
          end
        end
        inherited StatesImageList: TevImageList
          Bitmap = {
            494C010104000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
            0000000000003600000028000000400000003000000001002000000000000030
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000FF000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000080000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF0000000000000000000000000000000000808080008080
            8000808080008080800000008000000080000000800080808000808080008080
            8000808080008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            FF00000000000000FF00000000000000FF00000000000000FF00000000000000
            FF0000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            000000000000000000000000FF000000FF000000FF0000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000808080008080
            8000000000008080800000008000000080000000800000000000808080000000
            0000808080008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            00000000FF00000000000000FF00000000000000FF00000000000000FF000000
            00000000FF00FFFFFF0000000000000000000000000000000000FFFFFF000000
            0000000000000000FF000000FF000000FF000000FF0000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000808080000000
            0000808080000000800000008000000080000000800080808000000000008080
            8000000000008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            FF00000000000000FF00000000000000FF00000000000000FF00000000000000
            FF0000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            0000000000000000FF00000000000000FF000000FF0000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000808080008080
            8000000000000000800000000000000080000000800000000000808080000000
            0000808080008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            00000000FF00000000000000FF00000000000000FF00000000000000FF000000
            00000000FF00FFFFFF0000000000000000000000000000000000FFFFFF000000
            00000000FF000000FF0000000000000000000000FF000000FF00000000000000
            000000000000FFFFFF0000000000000000000000000000000000808080000000
            0000000080000000800080808000000000000000800000008000000000008080
            8000000000008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            FF00000000000000FF00000000000000FF00000000000000FF00000000000000
            FF0000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            00000000FF000000000000000000000000000000FF000000FF00000000000000
            000000000000FFFFFF0000000000000000000000000000000000808080008080
            8000000080008080800000000000808080000000800000008000808080000000
            0000808080008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            00000000FF00000000000000FF00000000000000FF00000000000000FF000000
            00000000FF00FFFFFF0000000000000000000000000000000000FFFFFF000000
            FF000000FF000000000000000000000000000000FF000000FF00000000000000
            000000000000FFFFFF0000000000000000000000000000000000808080000000
            8000000080000000000080808000000000000000800000008000000000008080
            8000000000008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            FF00000000000000FF00000000000000FF00000000000000FF00000000000000
            FF0000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            000000000000000000000000000000000000000000000000FF00000000000000
            000000000000FFFFFF0000000000000000000000000000000000808080008080
            8000000000008080800000000000808080000000000000008000808080000000
            0000808080008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            00000000FF00000000000000FF00000000000000FF00000000000000FF000000
            00000000FF00FFFFFF0000000000000000000000000000000000FFFFFF000000
            000000000000000000000000000000000000000000000000FF000000FF000000
            000000000000FFFFFF0000000000000000000000000000000000808080000000
            0000808080000000000080808000000000008080800000008000000080008080
            8000000000008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            FF00000000000000FF00000000000000FF00000000000000FF00000000000000
            FF0000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            000000000000000000000000000000000000000000000000FF000000FF000000
            000000000000FFFFFF0000000000000000000000000000000000808080008080
            8000000000008080800000000000808080000000000000008000000080000000
            0000808080008080800000000000000000000000000000000000FFFFFF000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000FFFFFF0000000000000000000000000000000000FFFFFF000000
            00000000FF00000000000000FF00000000000000FF00000000000000FF000000
            00000000FF00FFFFFF0000000000000000000000000000000000FFFFFF000000
            00000000000000000000000000000000000000000000000000000000FF000000
            000000000000FFFFFF0000000000000000000000000000000000808080000000
            0000808080000000000080808000000000008080800000000000000080008080
            8000000000008080800000000000000000000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
            FF00FFFFFF00FFFFFF0000000000000000000000000000000000808080008080
            8000808080008080800080808000808080008080800080808000000080000000
            8000808080008080800000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            FF00000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            8000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000FF000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000008000000000000000000000000000424D3E000000000000003E000000
            2800000040000000300000000100010000000000800100000000000000000000
            000000000000000000000000FFFFFF0000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000FFFFFFFFFFFFFFFF8001800180018001
            80018001800180019FF98AA99C7988519FF99551987990299FF98AA99A798A51
            9FF99551933991299FF98AA9973982119FF99551873985299FF98AA99FB98A91
            9FF995519F9995099FF98AA99F998A919FF995519FD995498001800180018001
            8001800180018001FFFFFFFFFFF7FFF700000000000000000000000000000000
            000000000000}
        end
      end
    end
  end
  object Panel2: TevPanel [1]
    Left = 0
    Top = 0
    Width = 754
    Height = 39
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label3: TevLabel
      Left = 8
      Top = 8
      Width = 34
      Height = 13
      Caption = 'UserId:'
    end
    object dtxtUserId: TevDBText
      Left = 48
      Top = 8
      Width = 65
      Height = 17
      DataField = 'USER_ID'
      DataSource = wwdsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dtxtLastName: TevDBText
      Left = 128
      Top = 8
      Width = 97
      Height = 17
      DataField = 'LAST_NAME'
      DataSource = wwdsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dtxtFirstName: TevDBText
      Left = 248
      Top = 8
      Width = 113
      Height = 17
      DataField = 'FIRST_NAME'
      DataSource = wwdsDetail
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  inherited wwdsDetail: TevDataSource
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
    OnUpdateData = wwdsDetailUpdateData
    MasterDataSource = nil
  end
  object odlgGetSignature: TOpenDialog
    Filter = 'Windows BitMap (*.bmp)|*.bmp'
    Title = 'Load user signature graphic'
    Left = 244
    Top = 24
  end
  object wwGroups: TevDataSource
    DataSet = wwGroupsDS
    Left = 344
  end
  object wwGroupsSelDS: TevClientDataSet
    Left = 304
    Top = 32
    object wwGroupsSelDSSB_SEC_GROUPS_NBR: TIntegerField
      FieldName = 'SB_SEC_GROUPS_NBR'
    end
    object wwGroupsSelDSNAME: TStringField
      DisplayWidth = 40
      FieldName = 'NAME'
      Size = 40
    end
  end
  object wwGroupsSel: TevDataSource
    DataSet = wwGroupsSelDS
    Left = 344
    Top = 32
  end
  object wwGroupsDS: TevClientDataSet
    Left = 304
    Top = 32
    object IntegerField1: TIntegerField
      FieldName = 'SB_SEC_GROUPS_NBR'
    end
    object StringField1: TStringField
      DisplayWidth = 40
      FieldName = 'NAME'
      Size = 40
    end
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 392
    Top = 8
  end
  object odlgSentry: TOpenDialog
    Left = 480
    Top = 8
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 424
    Top = 8
  end
  object pmEvoProduct: TevPopupMenu
    Left = 245
    Top = 125
    object miEvoProductCopyTo: TMenuItem
      Caption = 'Copy To...'
      OnClick = miEvoProductCopyToClick
    end
  end
end
