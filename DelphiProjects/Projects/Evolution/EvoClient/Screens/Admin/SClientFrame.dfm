object ClientFrame: TClientFrame
  Left = 0
  Top = 0
  Width = 616
  Height = 382
  TabOrder = 0
  object evLabel1: TevLabel
    Left = 8
    Top = 8
    Width = 76
    Height = 13
    Caption = 'Available clients'
  end
  object evLabel2: TevLabel
    Left = 352
    Top = 8
    Width = 75
    Height = 13
    Caption = 'Selected clients'
  end
  object sbAdd: TevSpeedButton
    Left = 279
    Top = 304
    Width = 59
    Height = 22
    Caption = 'Add'
    HideHint = True
    AutoSize = False
    OnClick = sbAddClick
    ParentColor = False
    ShortCut = 0
  end
  object sbRemove: TevSpeedButton
    Left = 279
    Top = 344
    Width = 59
    Height = 22
    Caption = 'Remove'
    HideHint = True
    AutoSize = False
    OnClick = sbRemoveClick
    ParentColor = False
    ShortCut = 0
  end
  object gClientsAvail: TevDBGrid
    Left = 8
    Top = 32
    Width = 265
    Height = 337
    DisableThemesInTitle = False
    Selected.Strings = (
      'NUMBER'#9'10'#9'Cl. #'#9'F'
      'NAME'#9'30'#9'Cl. name'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TClientFrame\gClientsAvail'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsClientsAvail
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnDblClick = gClientsAvailDblClick
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    DefaultSort = 'NUMBER'
    NoFire = False
  end
  object gClientsSelected: TevDBGrid
    Left = 344
    Top = 32
    Width = 265
    Height = 337
    DisableThemesInTitle = False
    Selected.Strings = (
      'NUMBER'#9'10'#9'Cl. #'#9'F'
      'NAME'#9'30'#9'Cl. name'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TClientFrame\gClientsSelected'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsClientsSelected
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    OnDblClick = gClientsSelectedDblClick
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    DefaultSort = 'NUMBER'
    NoFire = False
  end
  object DM_TEMPORARY: TDM_TEMPORARY
    Left = 264
    Top = 16
  end
  object dsClientsAvail: TevDataSource
    DataSet = cdClientsAvail
    Left = 264
    Top = 80
  end
  object cdClientsAvail: TevClientDataSet
    Left = 264
    Top = 48
    object cdClientsAvailCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdClientsAvailNUMBER: TStringField
      FieldName = 'NUMBER'
    end
    object cdClientsAvailNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
  end
  object dsClientsSelected: TevDataSource
    DataSet = cdClientsSelected
    Left = 304
    Top = 80
  end
  object cdClientsSelected: TevClientDataSet
    Left = 304
    Top = 48
    object IntegerField1: TIntegerField
      FieldName = 'CL_NBR'
    end
    object StringField1: TStringField
      FieldName = 'NUMBER'
    end
    object StringField2: TStringField
      FieldName = 'NAME'
      Size = 40
    end
  end
  object dsDetail: TevDataSource
    MasterDataSource = dsMaster
    Left = 264
    Top = 128
  end
  object dsMaster: TevDataSource
    Left = 264
    Top = 160
  end
end
