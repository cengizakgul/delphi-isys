// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SDBDTFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc,  ComCtrls, ImgList, Menus, Variants, EvContext,
  ISBasicClasses, ExtCtrls, EvCommonInterfaces, evConsts, EvUIComponents, EvClientDataSet, EvDataset;

const
  CM_REFRESH = WM_USER + 1;

type
  TNodeRecord = record
    //Hint: ShortString;
    ClientNbr: Integer;
    Level: Integer; // Client;Company;DBDT = -1;0;1234
    Nbr: Integer;
    Disabled: Boolean;
  end;
  PNodeRecord = ^TNodeRecord;

  TDBDTFrame = class(TFrame)
    dsMaster: TevDataSource;
    StatesImageList: TevImageList;
    dsClients: TevDataSource;
    dsDetail: TevDataSource;
    mItemPopup: TevPopupMenu;
    miRemove: TMenuItem;
    Panel1: TPanel;
    tv: TevTreeView;
    procedure tvExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure tvCollapsed(Sender: TObject; Node: TTreeNode);
    procedure tvClick(Sender: TObject);
    procedure tvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure tvCollapsing(Sender: TObject; Node: TTreeNode;
      var AllowCollapse: Boolean);
    procedure tvMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure miRemoveClick(Sender: TObject);
    procedure tvMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FNeedToBeRefreshed: Boolean;
    FClickPoint: TPoint;
    FAllowCollapsing: Boolean;
    FActiveMode: Boolean;
    FSB_USER_NBR: Integer;
    FSB_SEC_GROUPS_NBR: Integer;
    function AddNode(const Node: TTreeNode; const Title: string; const ClNbr: Integer; const Level: Integer; const Nbr: Integer; const Disabled, HasChildren: Boolean): TTreeNode;
    function IsResticted(const nr: TNodeRecord): Boolean;
    procedure RefreshTreeBranch(const Node: TTreeNode);
    function GetNodeRecord(const Node: TTreeNode): PNodeRecord;
    procedure DeleteNode(const Node: TTreeNode);
    procedure ChangeNodeStateTo(const Node: TTreeNode; const Disabled: Boolean);
    function NodeRecordToFilterText(const nr: TNodeRecord): string;
    procedure RefreshInfo;
    procedure CMRefresh(var Message: TMessage); message CM_REFRESH;
    procedure RefreshData;

    procedure SetActiveMode(const Value: Boolean);
    procedure SetSB_USER_NBR(const Value: Integer);
    procedure SetSB_SEC_GROUPS_NBR(const Value: Integer);
  public
    procedure InvalidateData;
    property  ActiveMode: Boolean read FActiveMode write SetActiveMode;
    property  SB_USER_NBR: Integer read FSB_USER_NBR write SetSB_USER_NBR;
    property  SB_SEC_GROUPS_NBR: Integer read FSB_SEC_GROUPS_NBR write SetSB_SEC_GROUPS_NBR;
  end;

implementation

uses SDataStructure, EvUtils;

{$R *.DFM}

function TDBDTFrame.AddNode(const Node: TTreeNode; const Title: string; const ClNbr: Integer; const Level: Integer; const Nbr: Integer; const Disabled, HasChildren: Boolean): TTreeNode;
var
  R: PNodeRecord;
begin
  New(R);
  R^.ClientNbr := ClNbr;
  R^.Level := Level;
  R^.Nbr := Nbr;
  R^.Disabled := Disabled;
  Result := tv.Items.AddChildObject(Node, Title, R);
  Result.ImageIndex := 0;
  Result.SelectedIndex := 0;
  Result.HasChildren := HasChildren;
end;

procedure TDBDTFrame.DeleteNode(const Node: TTreeNode);
var
  R: PNodeRecord;
begin
  R := Node.Data;
  Node.Data := nil;
  Dispose(R);
  Node.Delete;
end;

procedure TDBDTFrame.tvExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);

var
  nr: TNodeRecord;
  ht: THitTests;
  p: TPoint;

  Q: IevQuery;
  s: String;
  pCoNbr, pDivision, pBranch, pDepartment, pTeam : Integer;
  nc, nd, nb, np: TTreeNode;

  function CreateNode(n:TTreeNode; const Level: Integer;const title: string; const nbr: integer; const hasChildren: boolean):TTreeNode;
  begin
    result := AddNode(n, title, ctx_DataAccess.ClientID, Level, nbr, False, False);
    GetNodeRecord(result)^.Disabled := IsResticted(GetNodeRecord(result)^);
    result.HasChildren := hasChildren;
  end;

begin
  p := tv.ScreenToClient(Mouse.CursorPos);
  ht := tv.GetHitTestInfoAt(p.x, p.y);
  AllowExpansion := not (htOnIcon in ht);
  if AllowExpansion then
  begin
    nr := GetNodeRecord(Node)^;
    if nr.Level = -1 then
    begin
      ctx_DataAccess.OpenClient(nr.ClientNbr);

      s:=  'select C.NAME CoName, D.NAME DivName, B.NAME BranchName, P.NAME DepName, T.NAME TeamName, ' +
                  'C.CUSTOM_COMPANY_NUMBER, ' +
                  'C.CO_NBR, D.CO_DIVISION_NBR, B.CO_BRANCH_NBR, P.CO_DEPARTMENT_NBR, T.CO_TEAM_NBR ' +
              'from CO C ' +
              'left outer join CO_DIVISION D ' +
                'on C.CO_NBR=D.CO_NBR and {AsOfNow<D>} ' +
              'left outer join CO_BRANCH B ' +
                'on B.CO_DIVISION_NBR=D.CO_DIVISION_NBR and {AsOfNow<B>}  ' +
              'left outer join CO_DEPARTMENT P ' +
                'on P.CO_BRANCH_NBR=B.CO_BRANCH_NBR and {AsOfNow<P>}  ' +
              'left outer join CO_TEAM T ' +
                'on T.CO_DEPARTMENT_NBR=P.CO_DEPARTMENT_NBR and {AsOfNow<T>}  ' +
              'where {AsOfNow<C>}  ' +
                'order by C.CO_NBR,D.CO_DIVISION_NBR, B.CO_BRANCH_NBR, P.CO_DEPARTMENT_NBR, T.CO_TEAM_NBR ';
      Q := TevQuery.Create(s);
      Q.Execute;

      pCoNbr:= 0;
      pDivision:= 0;
      pBranch:= 0;
      pDepartment:= 0;
      pTeam := 0;
      nc:= nil;
      nd:= nil;
      nb:= nil;
      np:= nil;

      tv.Items.BeginUpdate;
      try

        Q.Result.First;
        while not Q.Result.Eof do
        begin
          if Q.Result.FieldByName('CO_NBR').AsInteger <> pCoNbr then
          begin
            pCoNbr := Q.Result.FieldByName('CO_NBR').AsInteger;
            nc:= CreateNode(Node, 0, Trim(Q.Result.FieldByName('CUSTOM_COMPANY_NUMBER').AsString) + ' - ' + Trim(Q.Result.FieldByName('CoName').AsString),
                      pCoNbr, not Q.Result.FieldByName('CO_DIVISION_NBR').IsNull);
          end;

          if Q.Result.FieldByName('CO_DIVISION_NBR').AsInteger <> pDivision then
          begin
            pDivision := Q.Result.FieldByName('CO_DIVISION_NBR').AsInteger;
            nd:= CreateNode(nc, 1, Trim(Q.Result.FieldByName('DivName').AsString),
                      pDivision, not Q.Result.FieldByName('CO_BRANCH_NBR').IsNull);
          end;

          if  Q.Result.FieldByName('CO_BRANCH_NBR').AsInteger <> pBranch then
          begin
            pBranch := Q.Result.FieldByName('CO_BRANCH_NBR').AsInteger;
            nb:= CreateNode(nd, 2, Trim(Q.Result.FieldByName('BranchName').AsString),
                       pBranch, not Q.Result.FieldByName('CO_DEPARTMENT_NBR').IsNull);
          end;

          if  Q.Result.FieldByName('CO_DEPARTMENT_NBR').AsInteger <> pDepartment then
          begin
            pDepartment := Q.Result.FieldByName('CO_DEPARTMENT_NBR').AsInteger;
            np:= CreateNode(nb, 3, Trim(Q.Result.FieldByName('DepName').AsString),
                      pDepartment, not Q.Result.FieldByName('CO_TEAM_NBR').IsNull);
          end;

          if  Q.Result.FieldByName('CO_TEAM_NBR').AsInteger <> pTeam then
          begin
            pTeam := Q.Result.FieldByName('CO_TEAM_NBR').AsInteger;
            CreateNode(np, 4, Trim(Q.Result.FieldByName('TeamName').AsString), pTeam, false);
          end;

          Q.Result.Next;
        end;

        RefreshTreeBranch(Node);
        tv.AlphaSort;
      finally
        tv.Items.EndUpdate;
      end;
    end;
  end;
end;

procedure TDBDTFrame.tvCollapsed(Sender: TObject; Node: TTreeNode);
  procedure Process(const Node: TTreeNode; const DeleteItself: Boolean = True);
  var
    i: Integer;
  begin
    for i := Node.Count-1 downto 0 do
      Process(Node.Item[i]);
    if DeleteItself then DeleteNode(Node);
  end;
begin
  if GetNodeRecord(Node).Level = -1 then
  begin
    tv.Items.BeginUpdate;
    try
      Process(Node, False);
    finally
      tv.Items.EndUpdate;
    end;
    Node.HasChildren := True;
  end;
end;

procedure TDBDTFrame.tvClick(Sender: TObject);
  procedure Process(const Node: TTreeNode; const Disabled: Boolean);
  var
    i: Integer;
  begin
    for i := 0 to Node.Count-1 do
      Process(Node.Item[i], Disabled);
    ChangeNodeStateTo(Node, Disabled);
  end;
var
  p: TPoint;
  ht: THitTests;
  n: TTreeNode;
  nr, nr2: PNodeRecord;
begin
  p := FClickPoint;
  ht := tv.GetHitTestInfoAt(p.x, p.y);
  if htOnIcon in ht then
  begin
    n := tv.GetNodeAt(p.x, p.y);
    nr := GetNodeRecord(n);
    if nr^.Level <> -1 then
    begin
      ctx_StartWait;
      try
        nr2 := GetNodeRecord(n.Parent);
        if not nr^.Disabled or not nr2^.Disabled then
        begin
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.DisableControls;
          try
            Process(n, not nr^.Disabled);
          finally
            DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.EnableControls;
          end;
          RefreshTreeBranch(n);
        end;
      finally
        ctx_EndWait;
      end;
    end;
  end;
end;

function TDBDTFrame.IsResticted(const nr: TNodeRecord): Boolean;
begin
  Result := DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.Locate('DATABASE_TYPE;CL_NBR;FILTER_TYPE;TABLE_NAME;CUSTOM_EXPR', VarArrayOf(['C', nr.ClientNbr, nr.Level, '-', NodeRecordToFilterText(nr)]), []);
end;

function TDBDTFrame.GetNodeRecord(const Node: TTreeNode): PNodeRecord;
begin
  Result := PNodeRecord(Node.Data);
end;

procedure TDBDTFrame.tvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  ;
end;

procedure TDBDTFrame.RefreshTreeBranch(const Node: TTreeNode);
  function CheckNode(const Node: TTreeNode; const ParentIsDisabled: Boolean): Boolean;
  var
    i: Integer;
    IsDisabled: Boolean;
  begin
    IsDisabled := GetNodeRecord(Node)^.Disabled;
    Result := GetNodeRecord(Node)^.Disabled;
    for i := 0 to Node.Count-1 do
      Result := CheckNode(Node.Item[i], IsDisabled or ParentIsDisabled) or Result;
    if ParentIsDisabled then
      Node.ImageIndex := 3
    else
    if IsDisabled then
      Node.ImageIndex := 2
    else
    if Result then
      Node.ImageIndex := 1
    else
      Node.ImageIndex := 0;
    Node.SelectedIndex := Node.ImageIndex;
  end;
var
  n: TTreeNode;
begin
  n := Node;
  while Assigned(n.Parent) do
    n := n.Parent;
  if CheckNode(n, False) then
    n.ImageIndex := 1
  else
    n.ImageIndex := 0;
  n.SelectedIndex := n.ImageIndex;
  tv.Invalidate;
end;

procedure TDBDTFrame.tvCollapsing(Sender: TObject; Node: TTreeNode;
  var AllowCollapse: Boolean);
var
  ht: THitTests;
  p: TPoint;
begin
  p := tv.ScreenToClient(Mouse.CursorPos);
  AllowCollapse := FAllowCollapsing;
  if not AllowCollapse then
  begin
    ht := tv.GetHitTestInfoAt(p.x, p.y);
    AllowCollapse := not (htOnIcon in ht);
  end;
end;

procedure TDBDTFrame.ChangeNodeStateTo(const Node: TTreeNode;
  const Disabled: Boolean);
var
  nr: PNodeRecord;
  s: string;
  sMasterFieldName: string;
begin
  sMasterFieldName := dsMaster.DataSet.Name+ '_NBR';
  nr := GetNodeRecord(Node);
  if nr^.Level <> -1 then
  begin
    if nr^.Disabled <> Disabled then
    begin
      nr^.Disabled := Disabled;
      s := NodeRecordToFilterText(nr^);
      if nr^.Disabled then
      begin
        if not DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.Locate('DATABASE_TYPE;CL_NBR;FILTER_TYPE;TABLE_NAME;CUSTOM_EXPR', VarArrayOf(['C', nr^.ClientNbr, nr^.Level, '-', s]), []) then
        begin
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.Append;
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS['DATABASE_TYPE;CL_NBR;FILTER_TYPE;TABLE_NAME;CUSTOM_EXPR'] := VarArrayOf(['C', nr^.ClientNbr, nr^.Level, '-', s]);
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS[sMasterFieldName] := dsMaster.DataSet[sMasterFieldName];
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.Post;
        end;
      end
      else
      begin
        if DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.Locate('DATABASE_TYPE;CL_NBR;FILTER_TYPE;TABLE_NAME;CUSTOM_EXPR', VarArrayOf(['C', nr^.ClientNbr, nr^.Level, '-', s]), []) then
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.Delete;
      end;
    end;
  end;
end;

procedure TDBDTFrame.tvMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
  function GetLevelTitle(const Level: Integer): string;
  begin
    case Level of
    0:
      Result := 'company';
    1:
      Result := 'division';
    2:
      Result := 'branch';
    3:
      Result := 'department';
    4:
      Result := 'team';
    -1:
      Result := 'client database';
    else
      Result := 'N/A';
    end;
  end;
var
  n: TTreeNode;
  nr: TNodeRecord;
  ht: THitTests;
  s: string;
const
  sChangeMessage = #13'Doubleclick on icon box to change rights';
begin
  s := '';
  ht := tv.GetHitTestInfoAt(X, Y);
  if htOnItem in ht then
  begin
    n := tv.GetNodeAt(X, Y);
    nr := GetNodeRecord(n)^;
    s := 'It is a '+ GetLevelTitle(nr.Level);
    case n.ImageIndex of
    0:
      s := s+ #13'Access is granted in full'+ sChangeMessage;
    1:
      s := s+ #13'Access is granted, with some limitations'+ sChangeMessage;
    2:
      s := s+ #13'Access is denied'+ sChangeMessage;
    3:
      s := s+ #13'Access is denied on upper level';
    end;
  end;
  tv.Hint := s;
end;

function TDBDTFrame.NodeRecordToFilterText(const nr: TNodeRecord): string;
begin
  Result := IntToStr(nr.Nbr);
end;

procedure TDBDTFrame.miRemoveClick(Sender: TObject);
var
  nr: TNodeRecord;
begin
  nr := GetNodeRecord(tv.Selected)^;
  with DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS do
  begin
    DisableControls;
    try
      while Locate('DATABASE_TYPE;CL_NBR', VarArrayOf(['C', nr.ClientNbr]), []) do
        Delete;
    finally
      EnableControls;
    end;
    FAllowCollapsing := True;
    try
      with tv.Selected do
      begin
        ImageIndex := 0;
        SelectedIndex := 0;
        Collapse(True);
      end;
    finally
      FAllowCollapsing := False;
    end;
  end;
end;

procedure TDBDTFrame.tvMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  n: TTreeNode;
begin
  FClickPoint.X := X;
  FClickPoint.Y := Y;
  if Button = mbRight then
  begin
    n := tv.GetNodeAt(X, Y);
    if Assigned(n) and (GetNodeRecord(n)^.Level = -1) then
    begin
      tv.Selected := n;
      mItemPopup.Popup(tv.ClientOrigin.x+ X, tv.ClientOrigin.y+ Y);
    end;
  end;
end;

procedure TDBDTFrame.RefreshInfo;
var
  i: Integer;
  n: TTreeNode;
  ClNbr: Integer;
  Rights: IevSecAccountRights;
  StateInfo: IevSecStateInfo;

  procedure DeleteNode(const Node: TTreeNode);
  var
    R: PNodeRecord;
  begin
    R := Node.Data;
    Node.Data := nil;
    Dispose(R);
  end;

begin
  if SB_USER_NBR <> 0 then
    Rights := ctx_Security.GetUserRights(SB_USER_NBR)
  else if SB_SEC_GROUPS_NBR <> 0 then
    Rights := ctx_Security.GetGroupRights(SB_SEC_GROUPS_NBR)
  else
    Rights := nil;

  tv.Items.BeginUpdate;
  try
    for i := tv.Items.Count-1 downto 0 do
      DeleteNode(tv.Items[i]);
    tv.Items.Clear;

    if Assigned(Rights) then
    begin
      DM_TEMPORARY.TMP_CL.First;
      while not DM_TEMPORARY.TMP_CL.Eof do
      begin
        ClNbr := DM_TEMPORARY.TMP_CL.CL_NBR.AsInteger;
        StateInfo := Rights.Clients.GetStateInfo(IntToStr(ClNbr));
        if Assigned(StateInfo) and (StateInfo.State = stEnabled) then
        begin
          n := AddNode(nil, DM_TEMPORARY.TMP_CL['CUSTOM_CLIENT_NUMBER']+ ' - '+ DM_TEMPORARY.TMP_CL['NAME'], ClNbr, -1, 0, False, True);
          if DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.Locate('DATABASE_TYPE;CL_NBR;TABLE_NAME', VarArrayOf(['C', ClNbr, '-']), []) then
          begin
            n.ImageIndex := 1;
            n.SelectedIndex := 1;
          end;
        end;
        DM_TEMPORARY.TMP_CL.Next;
      end;
    end;
    tv.AlphaSort;
  finally
    tv.Items.EndUpdate;
  end;
end;

procedure TDBDTFrame.SetActiveMode(const Value: Boolean);
begin
  FActiveMode := Value;
  RefreshData;
end;

procedure TDBDTFrame.SetSB_USER_NBR(const Value: Integer);
begin
  if FSB_USER_NBR <> Value then
  begin
    FSB_SEC_GROUPS_NBR := 0;
    FSB_USER_NBR := Value;
    InvalidateData;
    RefreshData;
  end;
end;

procedure TDBDTFrame.SetSB_SEC_GROUPS_NBR(const Value: Integer);
begin
  if FSB_SEC_GROUPS_NBR <> Value then
  begin
    FSB_USER_NBR := 0;
    FSB_SEC_GROUPS_NBR := Value;
    InvalidateData;
    RefreshData;
  end;
end;

procedure TDBDTFrame.RefreshData;
begin
  if FNeedToBeRefreshed and ActiveMode then
  begin
    FNeedToBeRefreshed := False;
    PostMessage(Handle, CM_REFRESH, 0, 0);
  end;
end;

procedure TDBDTFrame.CMRefresh(var Message: TMessage);
begin
  RefreshInfo;
end;

procedure TDBDTFrame.InvalidateData;
begin
  FNeedToBeRefreshed := True;
end;

end.
