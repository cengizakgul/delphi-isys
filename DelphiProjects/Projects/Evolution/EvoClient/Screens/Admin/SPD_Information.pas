// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Information;

interface

uses
  Windows, Messages, SFrameEntry, Classes, Db, Wwdatsrc, 
  Controls, Forms,  InfoListFrame, ISBasicClasses;

type
  TInformationFrame = class(TFrameEntry)
    InfoList: TInfoList;
  public
    procedure Activate; override;
  end;

implementation

{$R *.DFM}

{ TInformationFrame }

procedure TInformationFrame.Activate;
begin
  InfoList.Activate;
end;

initialization
  RegisterCLass(TInformationFrame);

end.
