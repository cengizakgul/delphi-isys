// Screens of "Admin"
unit scr_Admin;

interface

uses
  SAdminUsersScr,
  SClientFrame,
  SDBDTFrame,
  SPD_EDIT_SB_USER,
  SPD_EDIT_SB_GROUP,
  SPD_Settings,
  SPD_Information,
  SPD_Locks;

implementation

end.
