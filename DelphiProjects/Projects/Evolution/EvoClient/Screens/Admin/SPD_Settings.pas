// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_Settings;

interface

uses
  Windows, Messages, SFrameEntry, Classes, Db, Wwdatsrc, 
  Controls, Forms, StdCtrls, Spin, EvConsts, Graphics,
  SysUtils, EvUtils, EvSendMail, CheckLst, isBasicUtils,
  SPackageEntry, SDataStructure, wwdbdatetimepicker, EvContext,
  ExtCtrls, ComCtrls, EvBasicUtils, Grids, ValEdit, Dialogs, EvTypes,
  ISBasicClasses, Buttons, ImgList, isBaseClasses, EvMainboard,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, SReportSettings,
  EvDataAccessComponents, Wwdbigrd, Wwdbgrid, EvStreamUtils,
  EvStatisticsViewerFrm, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, ColorGrd, isUIEdit, EvInitApp,
  Mask, wwdbedit, isUIwwDBEdit, DBCtrls, isUIFashionPanel, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, isUIDBImage, isHTTP, isUtils;

type
  TSettingsFrame = class(TFrameEntry)
    pcMain: TevPageControl;
    tsEvolution: TTabSheet;
    tsExternal: TTabSheet;
    oDialog: TOpenDialog;
    evBitBtn1: TevBitBtn;
    evImageList1: TevImageList;
    dsPrinterOffsets: TevClientDataSet;
    dscPrinterOffsets: TevDataSource;
    dsPrinterOffsetsPrinter: TStringField;
    dsPrinterOffsetsVOffset: TIntegerField;
    dsPrinterOffsetsHOffset: TIntegerField;
    dsPrinterOffsetsStorageName: TStringField;
    tsStatistics: TTabSheet;
    frmStatistics: TevStatisticsViewerFrm;
    dsExternal: TEvClientDataSet;
    dscExternal: TevDataSource;
    dsExternalName: TStringField;
    dsExternalPath: TStringField;
    dsExternalPicture: TBlobField;
    dsExternalDashboard: TBooleanField;
    dsExternalDefault: TBooleanField;
    dsExternalNbr: TIntegerField;
    isUIFashionPanel1: TisUIFashionPanel;
    grExternal: TevDBGrid;
    isUIFashionPanel2: TisUIFashionPanel;
    evLabel5: TevLabel;
    evLabel6: TevLabel;
    evLabel3: TevLabel;
    edName: TevDBEdit;
    edPath: TevDBEdit;
    chbDashboard: TevDBCheckBox;
    chbDefault: TevDBCheckBox;
    edNbr: TevDBEdit;
    btnAddLink: TevSpeedButton;
    btnDeleteLink: TevSpeedButton;
    sbBrowse: TevSpeedButton;
    pnlPict: TevPanel;
    imgLinkPict: TevImage;
    evLabel7: TevLabel;
    isUIFashionPanel3: TisUIFashionPanel;
    Label1: TLabel;
    cbDuplexPrinting: TevComboBox;
    cbAlwaysDoPreview: TevCheckBox;
    cbAlwaysShowASCIIFileDialog: TevCheckBox;
    evLabel1: TevLabel;
    cbChecksPrinter: TevComboBox;
    evLabel2: TevLabel;
    cbReportsPrinter: TevComboBox;
    evLabel4: TevLabel;
    evDBGrid1: TevDBGrid;
    btnCalibrate: TevBitBtn;
    isUIFashionPanel4: TisUIFashionPanel;
    cbAutoJump: TevCheckBox;
    chDumpParams: TevCheckBox;
    chbStatatistics: TevCheckBox;
    cbShowEffectivePeriods: TevCheckBox;
    chbNetLatency: TevCheckBox;
    procedure evBitBtn1Click(Sender: TObject);
    procedure dsPrinterOffsetsBeforeInsert(DataSet: TDataSet);
    procedure dsPrinterOffsetsBeforeDelete(DataSet: TDataSet);
    procedure dsPrinterOffsetsAfterPost(DataSet: TDataSet);
    procedure btnCalibrateClick(Sender: TObject);
    procedure tsStatisticsShow(Sender: TObject);
    procedure btnAddLinkClick(Sender: TObject);
    procedure btnDeleteLinkClick(Sender: TObject);
    procedure sbBrowseClick(Sender: TObject);
    procedure dsExternalAfterPost(DataSet: TDataSet);
    procedure dsExternalBeforePost(DataSet: TDataSet);
    procedure edPathExit(Sender: TObject);
    procedure dsExternalNewRecord(DataSet: TDataSet);
    procedure dscExternalDataChange(Sender: TObject; Field: TField);
    procedure imgLinkPictDblClick(Sender: TObject);
    procedure edPathKeyPress(Sender: TObject; var Key: Char);
    procedure edNameChange(Sender: TObject);
    procedure edNameExit(Sender: TObject);
    procedure chbDashboardChange(Sender: TObject);
  private
    FExternalModified: Boolean;
    procedure SyncPicture(const AOnlyForFile: Boolean);
    procedure ExtLinksPostPendingChanges;
  public
    procedure Activate; override;
    procedure Deactivate; override;
  end;

implementation

uses SSecurityInterface, isSettings, EvCommonInterfaces, Math;

{$R *.DFM}


{ TSettingsFrame }

procedure TSettingsFrame.Activate;
var
  i: Integer;
  PictData: IisStream;
  Links: IisParamsCollection;
begin
  inherited;

  Links := GetExternalLinks;

  dsExternal.CreateDataSet;
  dsExternal.IndexDefs.Add('Nbr', 'Nbr', []);

  dsExternal.Tag := 1;
  dsExternal.DisableControls;
  try
    for i := 0 to Links.Count - 1 do
    begin
      dsExternal.Append;
      dsExternal.FieldByName('Default').AsBoolean := Links[i].Value['Default'];
      dsExternal.FieldByName('Nbr').AsInteger := i + 1;
      dsExternal.FieldByName('Name').AsString := Links[i].Value['Name'];
      dsExternal.FieldByName('Path').AsString := Links[i].Value['Path'];
      dsExternal.FieldByName('Dashboard').AsBoolean := Links[i].Value['Dashboard'];

      PictData := IInterface(Links[i].Value['Icon']) as IisStream;
      if Assigned(PictData) and (PictData.Size > 0) then
        TBlobField(dsExternal.FieldByName('Picture')).LoadFromStream(PictData.RealStream);

      dsExternal.Post;
    end;
    dsExternal.First;
  finally
    dsExternal.EnableControls;
  end;
  dsExternal.Tag := 0;

  FExternalModified := False;

  pcMain.ActivePage := tsEvolution;

  dsPrinterOffsets.Tag := 1;
  dsPrinterOffsets.CreateDataSet;
  dsPrinterOffsets.DisableControls;
  try
    for i := 0 to Mainboard.MachineInfo.Printers.Count - 1 do
    begin
      dsPrinterOffsets.Append;
      dsPrinterOffsets.FieldByName('Printer').AsString := Mainboard.MachineInfo.Printers[i].Name;
      dsPrinterOffsets.FieldByName('VOffset').AsInteger := Mainboard.MachineInfo.Printers[i].VOffset;
      dsPrinterOffsets.FieldByName('HOffset').AsInteger := Mainboard.MachineInfo.Printers[i].HOffset;
      dsPrinterOffsets.Post;
    end;
    dsPrinterOffsets.First;
  finally
    dsPrinterOffsets.EnableControls;
    dsPrinterOffsets.Tag := 0;    
  end;

  btnCalibrate.Enabled := dsPrinterOffsets.RecordCount > 0;

  for i := 0 to Mainboard.MachineInfo.Printers.Count - 1 do
  begin
    cbChecksPrinter.Items.Add(Mainboard.MachineInfo.Printers[i].Name);
    cbReportsPrinter.Items.Add(Mainboard.MachineInfo.Printers[i].Name);
  end;

  cbChecksPrinter.ItemIndex := cbChecksPrinter.Items.IndexOf(mb_AppSettings['Settings\ChecksPrinter']);
  cbReportsPrinter.ItemIndex := cbReportsPrinter.Items.IndexOf(mb_AppSettings['Settings\ReportsPrinter']);
  cbDuplexPrinting.ItemIndex := mb_AppSettings.AsInteger['Settings\DuplexPrinting'];

  cbAutoJump.Checked := mb_AppSettings['Settings\AutoJump'] = 'Y';
  cbShowEffectivePeriods.Checked := mb_AppSettings['Settings\ShowEffectivePeriods'] = 'Y';
  cbAlwaysDoPreview.Checked := mb_AppSettings.AsBoolean['Settings\AlwaysDoPreview'];
  cbAlwaysShowASCIIFileDialog.Checked := mb_AppSettings.AsBoolean['Settings\AlwaysShowASCIIFileDialog'];

  chDumpParams.Visible := IsQAMode;
  if chDumpParams.Visible then
    chDumpParams.Checked := mb_AppSettings.AsBoolean['Settings\DumpRWParams'];

  chbNetLatency.Visible := IsQAMode and not IsStandAlone;
  if chbNetLatency.Visible then
    chbNetLatency.Checked := mb_TCPClient.ArtificialLatency > 0;

  chbStatatistics.Checked := ctx_Statistics.Enabled;
end;

procedure TSettingsFrame.Deactivate;
var
  Links: IisParamsCollection;
  LV: IisListOfValues;
  Pict: IisStream;
begin
  inherited;

  ExtLinksPostPendingChanges;

  Links := TisParamsCollection.Create;
  dsExternal.DisableControls;
  try
    dsExternal.First;
    while not dsExternal.Eof do
    begin
      LV := Links.AddParams(dsExternal.FieldByName('Nbr').AsString);
      LV.AddValue('Name', dsExternal.FieldByName('Name').AsString);
      LV.AddValue('Path', dsExternal.FieldByName('Path').AsString);
      LV.AddValue('Dashboard', dsExternal.FieldByName('Dashboard').AsBoolean);
      LV.AddValue('Default', dsExternal.FieldByName('Default').AsBoolean);

      if not dsExternal.FieldByName('Picture').IsNull then
      begin
        Pict := TisStream.Create;
        TBlobField(dsExternal.FieldByName('Picture')).SaveToStream(Pict.RealStream);
      end
      else
        Pict := nil;
      LV.AddValue('Icon', Pict);

      dsExternal.Next;
    end;
  finally
    dsExternal.EnableControls;
  end;
  SaveExternalLinks(Links);

  mb_AppSettings['Settings\ChecksPrinter'] := cbChecksPrinter.Text;
  mb_AppSettings['Settings\ReportsPrinter'] := cbReportsPrinter.Text;
  mb_AppSettings.AsInteger['Settings\DuplexPrinting'] := cbDuplexPrinting.ItemIndex;

  if cbAutoJump.Checked then
    mb_AppSettings['Settings\AutoJump'] := 'Y'
  else
    mb_AppSettings['Settings\AutoJump'] := 'N';

  if cbShowEffectivePeriods.Checked then
    mb_AppSettings['Settings\ShowEffectivePeriods'] := 'Y'
  else
    mb_AppSettings['Settings\ShowEffectivePeriods'] := 'N';

  mb_AppSettings.AsBoolean['Settings\AlwaysDoPreview'] := cbAlwaysDoPreview.Checked;
  mb_AppSettings.AsBoolean['Settings\AlwaysShowASCIIFileDialog'] := cbAlwaysShowASCIIFileDialog.Checked;

  if chDumpParams.Visible then
    mb_AppSettings.AsBoolean['Settings\DumpRWParams'] := chDumpParams.Checked;

  if chbNetLatency.Visible then
  begin
    mb_AppSettings.AsInteger['Settings\NetworkLatency'] := IIF(chbNetLatency.Checked, 100, 0);
    mb_TCPClient.ArtificialLatency := mb_AppSettings.AsInteger['Settings\NetworkLatency'];
  end;

  ctx_Statistics.Enabled := chbStatatistics.Checked;
  mb_AppSettings.AsBoolean['Settings\CollectStatistics'] := ctx_Statistics.Enabled;

  if FExternalModified then
  begin
    SendMessage(Application.MainForm.Handle, WM_RECREATE_MENUS, 0, 0);
    FExternalModified := True;
  end;
end;

procedure TSettingsFrame.evBitBtn1Click(Sender: TObject);
var
  FileName: string;
  F: TextFile;
  t: TStringList;
begin
  inherited;
  FileName := AppTempFolder + '_SysInfo.txt';
  AssignFile(F, FileName);
  Rewrite(F);
  try
    Writeln(F, 'Reporting');
    Writeln(F, '   '+cbAlwaysDoPreview.Caption+' - '+BoolToYN(cbAlwaysDoPreview.Checked));
    Writeln(F, '');
    Writeln(F, 'Printing');
    Writeln(F, '   Checks Printer  - ('+cbChecksPrinter.Text+')');
    Writeln(F, '   Reports Printer - ('+cbReportsPrinter.Text+')');
    Writeln(F, '');
    Writeln(F, 'Screen Behavior');
    writeln(F, '   '+cbAutoJump.Caption+' - '+BoolToYN(cbAutoJump.Checked));
    writeln(F, '   '+cbShowEffectivePeriods.Caption+' - '+BoolToYN(cbShowEffectivePeriods.Checked));
    CloseFile(F);
    t := TStringList.Create;
    t.Add(FileName);
    try
      MailTo(nil, nil, nil, t, 'Evolution Workstation Settings', '');
    finally
      t.Free;
    end;
  finally
    DeleteFile(FileName);
  end;
end;

procedure TSettingsFrame.dsPrinterOffsetsBeforeInsert(DataSet: TDataSet);
begin
  if dsPrinterOffsets.Tag = 0 then
    Abort;
end;

procedure TSettingsFrame.dsPrinterOffsetsBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;

procedure TSettingsFrame.dsPrinterOffsetsAfterPost(DataSet: TDataSet);
var
  Prn: IevPrinterInfo;
begin
  if dsPrinterOffsets.Tag = 0 then
  begin
    Prn := Mainboard.MachineInfo.Printers.FindPrinter(dsPrinterOffsets.FieldByName('Printer').AsString);
    Prn.VOffset := dsPrinterOffsets.FieldByName('VOffset').AsInteger;
    Prn.HOffset := dsPrinterOffsets.FieldByName('HOffset').AsInteger;
    Mainboard.MachineInfo.Printers.Save;
  end;
end;

procedure TSettingsFrame.btnCalibrateClick(Sender: TObject);
var
  Params: TrwReportParams;
  Res: TrwReportResults;
  ResData: IevDualStream;
begin
  inherited;
  Params := TrwReportParams.Create;
  try
    Params.Add('Printer', dsPrinterOffsets.FieldByName('Printer').AsString, False);
    Params.Add('VOffset', dsPrinterOffsets.FieldByName('VOffset').AsInteger, False);
    Params.Add('HOffset', dsPrinterOffsets.FieldByName('HOffset').AsInteger, False);
        
    ctx_RWLocalEngine.StartGroup;
    ctx_RWLocalEngine.CalcPrintReport(1800, 'S', Params, 'Printer Calibration Report');
    ResData := ctx_RWLocalEngine.EndGroup(rdtNone);

    Res := TrwReportResults.Create(ResData);
    try
      ctx_RWLocalEngine.Print(Res, dsPrinterOffsets.FieldByName('Printer').AsString);
    finally
      Res.Free;
    end;

  finally
    Params.Free;
  end;

  EvMessage('Calibration report has been printed. Adjust vertical and horizontal offsets if corner marks are off.');
end;

procedure TSettingsFrame.tsStatisticsShow(Sender: TObject);
begin
  frmStatistics.InitForFiles;
end;

procedure TSettingsFrame.btnAddLinkClick(Sender: TObject);
begin
  grExternal.SetFocus;
  ExtLinksPostPendingChanges;
  dsExternal.Append;
  edNbr.SetFocus;
end;

procedure TSettingsFrame.btnDeleteLinkClick(Sender: TObject);
begin
  dsExternal.Delete;
end;

procedure TSettingsFrame.sbBrowseClick(Sender: TObject);
begin
  edPath.SetFocus;

  if oDialog.Execute then
  begin
    if dsExternal.State = dsBrowse then
      dsExternal.Edit;

    dsExternal.FieldByName('Path').AsString := oDialog.FileName;
    if dsExternal.FieldByName('Name').AsString = '' then
      dsExternal.FieldByName('Name').AsString := ChangeFileExt(ExtractFileName(oDialog.FileName), '');
    SyncPicture(True);
    ExtLinksPostPendingChanges;    
  end;
end;

procedure TSettingsFrame.dsExternalAfterPost(DataSet: TDataSet);
var
  RecN: Integer;
  IndexName: String;
  ToggleDefault: Boolean;
  bm: String;
begin
  if dsExternal.Tag = 0 then
  begin
    FExternalModified := True;

    ToggleDefault := dsExternal.FieldByName('Default').AsBoolean;
    bm := dsExternal.Bookmark;
    RecN := dsExternal.RecNo;
    IndexName := dsExternal.IndexName;
    dsExternal.IndexName := 'Nbr';
    dsExternal.Tag := 1;
    dsExternal.DisableControls;
    try
      dsExternal.First;
      while not dsExternal.Eof do
      begin
        dsExternal.Edit;
        if ToggleDefault and (dsExternal.Bookmark <> bm) then
          dsExternal.FieldByName('Default').AsBoolean := False;
        dsExternal.FieldByName('Nbr').AsInteger := dsExternal.RecNo;
        dsExternal.Post;
        dsExternal.Next;
      end;
      dsExternal.IndexName := IndexName;
      dsExternal.RecNo := RecN;
    finally
      dsExternal.EnableControls;
      dsExternal.Tag := 0;
    end;
  end;
end;

procedure TSettingsFrame.dsExternalBeforePost(DataSet: TDataSet);
begin
  SyncPicture(True);
  dscExternalDataChange(nil, nil);  
end;

procedure TSettingsFrame.SyncPicture(const AOnlyForFile: Boolean);
var
  S: IisStream;
  path: String;
  err: string;
begin
  Path := dsExternal.FieldByName('Path').AsString;

  if StartsWith(Path, 'http://') or StartsWith(Path, 'https://') then
  begin
    if AOnlyForFile then
      Exit;
    ctx_StartWait('Communicating with Web server...');
    try
      err := '';
      try
        S := TisHttpClient.GetFavIcon(Path, nil);

        // try to load to the Icon
        if Assigned(S) and (S.Size > 0) then
        begin
          S.Position := 0;
          imgLinkPict.Picture.Icon.LoadFromStream(S.RealStream);
        end;  
      except
        on e: Exception do
        begin
          err := e.Message;
          S.Clear;
        end;
      end;
    finally
      ctx_EndWait;
    end;
    if err <> '' then
    begin
      if Pos('authorization required', LowerCase(err)) > 0 then
        err := 'Cannot load the icon! This site requires authorization.'
      else
        err := 'Cannot load the icon! Got error message: ' + err;

      EvMessage(err, mtError, [mbOK]);
    end;
  end
  else if Path <> '' then
    S := GetFileIcon(Path);

  if Assigned(S) and (S.Size > 0) then
  begin
    S.Position := 0;
    TBlobField(dsExternal.FieldByName('Picture')).LoadFromStream(S.RealStream);
  end
  else
    dsExternal.FieldByName('Picture').Clear;
end;

procedure TSettingsFrame.edPathExit(Sender: TObject);
begin
  if dsExternal.State in [dsEdit, dsInsert] then
  begin
    SyncPicture(True);
    ExtLinksPostPendingChanges;
  end;
end;

procedure TSettingsFrame.dsExternalNewRecord(DataSet: TDataSet);
begin
  dsExternal.FieldByName('Nbr').AsInteger := dsExternal.RecordCount + 1;
  dsExternal.FieldByName('Dashboard').AsBoolean := False;
  dsExternal.FieldByName('Default').AsBoolean := False;
  dscExternalDataChange(nil, nil);
end;

procedure TSettingsFrame.dscExternalDataChange(Sender: TObject; Field: TField);
var
  S: IisStream;
  Fld: TBlobField;
  path: String;
begin
  if not Assigned(Field) then
  begin
    Fld := TBlobField(dsExternal.FieldByName('Picture'));
    if not Fld.IsNull then
    begin
      S := TisStream.Create;
      Fld.SaveToStream(S.RealStream);
      S.Position := 0;

      imgLinkPict.Picture.Icon.LoadFromStream(S.RealStream);
      imgLinkPict.Width := imgLinkPict.Picture.Icon.Width;
      imgLinkPict.Height := imgLinkPict.Picture.Icon.Height;
      imgLinkPict.Left := (pnlPict.ClientWidth - imgLinkPict.Width) div 2;
      imgLinkPict.Top := (pnlPict.ClientHeight - imgLinkPict.Height) div 2;
    end
    else
      imgLinkPict.Picture.Assign(nil);

    path := dsExternal.FieldByName('Path').AsString;
    chbDefault.Enabled := StartsWith(path, 'http://') or StartsWith(path, 'https://');
  end;
end;

procedure TSettingsFrame.imgLinkPictDblClick(Sender: TObject);
begin
  if dsExternal.State = dsBrowse then
    dsExternal.Edit;

  edPath.UpdateRecord;
  SyncPicture(False);
  ExtLinksPostPendingChanges;
end;

procedure TSettingsFrame.edPathKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_RETURN then
  begin
    (Sender as TevDBEdit).UpdateRecord;
    if Assigned((Sender as TevDBEdit).OnExit) then
      (Sender as TevDBEdit).OnExit(nil);
  end;
end;

procedure TSettingsFrame.ExtLinksPostPendingChanges;
begin
  if (dsExternal.Tag = 0) and (dsExternal.State in [dsEdit, dsInsert]) then
    dsExternal.Post;
end;

procedure TSettingsFrame.edNameChange(Sender: TObject);
begin
  (Sender as TevDBEdit).UpdateRecord;
end;

procedure TSettingsFrame.edNameExit(Sender: TObject);
begin
  ExtLinksPostPendingChanges;
end;

procedure TSettingsFrame.chbDashboardChange(Sender: TObject);
begin
  (Sender as TevDBCheckBox).UpdateRecord;
  ExtLinksPostPendingChanges;
end;

initialization
  RegisterCLass(TSettingsFrame);

end.
