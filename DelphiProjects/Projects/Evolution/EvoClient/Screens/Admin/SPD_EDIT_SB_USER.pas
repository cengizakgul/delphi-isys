// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_USER;

interface

uses Windows,  Db, Dialogs, DBCtrls, Graphics, Messages,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit,
  SFrameEntry, 
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Variants,
  Classes, Wwdatsrc, SDataStructure, SDBDTFrame, Forms, SClientFrame, EvTypes,
  SDDClasses, kbmMemTable, ISKbmMemDataSet, ISBasicClasses, EvContext, EvCommonInterfaces,
  EvDataAccessComponents, ISDataAccessComponents, EvStreamUtils, DateUtils,
  SDataDicttemp, isVCLBugFix, EvMainboard, EvBasicUtils, EvDataSet, EvExceptions,
  SDataDictbureau, EvUIUtils, EvUIComponents, EvClientDataSet, isUIDBImage,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, SPD_SBUserChoiceQuery,
  isUIwwDBLookupCombo, LMDCustomButton, LMDButton, isUILMDButton,
  isUIwwDBDateTimePicker, isUIEdit, isUIwwDBComboBox, isUIwwDBEdit, Menus;

const
  CM_CLOSE_APP = WM_USER + 200;

type
  TEDIT_SB_USER = class(TFrameEntry)
    pctlEDIT_SB_USER: TevPageControl;
    tshtBrowse: TTabSheet;
    tshtUser_Detail: TTabSheet;
    lablUser_Signature: TevLabel;
    lablLast_Name: TevLabel;
    lablFirst_Name: TevLabel;
    lablMiddle_Initial: TevLabel;
    lablDepartment: TevLabel;
    lablPassword_Change_Date: TevLabel;
    lablSecurity_Level: TevLabel;
    dedtLast_Name: TevDBEdit;
    dedtFirst_Name: TevDBEdit;
    dedtMiddle_Initial: TevDBEdit;
    drgpActive_User: TevDBRadioGroup;
    wwdgEDIT_SB_USER: TevDBGrid;
    wwcbDepartment: TevDBComboBox;
    wwcbSecurity_Level: TevDBComboBox;
    Panel1: TevPanel;
    lablPassword: TevLabel;
    editPassword: TevEdit;
    lablConfirmPassword: TevLabel;
    editConfirmPassword: TevEdit;
    lablUser_ID: TevLabel;
    dedtUser_ID: TevDBEdit;
    wwdpPassword_Change_Date: TevDBDateTimePicker;
    tshtOptions: TTabSheet;
    dmemUserFunctions: TDBMemo;
    Label1: TevLabel;
    dmemUserOptions: TDBMemo;
    Label2: TevLabel;
    Panel2: TevPanel;
    Label3: TevLabel;
    dtxtUserId: TevDBText;
    dtxtLastName: TevDBText;
    dtxtFirstName: TevDBText;
    Label4: TevLabel;
    dedtEmail_Address: TevDBEdit;
    bbtnLoad: TevBitBtn;
    odlgGetSignature: TOpenDialog;
    tshtUser_Groups: TTabSheet;
    wwDBGroupGridAvail: TevDBGrid;
    wwGroups: TevDataSource;
    Label5: TevLabel;
    wwDBGroupGridSelected: TevDBGrid;
    Label6: TevLabel;
    SpeedButton1: TevSpeedButton;
    SpeedButton2: TevSpeedButton;
    wwGroupsSelDS: TevClientDataSet;
    wwGroupsSel: TevDataSource;
    wwGroupsSelDSSB_SEC_GROUPS_NBR: TIntegerField;
    wwGroupsSelDSNAME: TStringField;
    wwGroupsDS: TevClientDataSet;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    gbUserClientDb: TevGroupBox;
    rbAllClients: TevRadioButton;
    rbOneClient: TevRadioButton;
    wwlcClientNumber: TevDBLookupCombo;    
    DM_TEMPORARY: TDM_TEMPORARY;
    tsClients: TTabSheet;
    tsDBDT: TTabSheet;
    ClientFrame1: TClientFrame;
    DBDTFrame1: TDBDTFrame;
    evLabel1: TevLabel;
    btnSecRights: TevBitBtn;
    lablAccountant: TevLabel;
    wwlcAccountant: TevDBLookupCombo;
    pBlock: TevPanel;
    evButton1: TevButton;
    rbWebUser: TevRadioButton;
    odlgSentry: TOpenDialog;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    lAutoSec: TLabel;
    evDBRadioGroup1: TevDBRadioGroup;
    dimgUserSignature: TevDBImage;
    evLabel2: TevLabel;
    cbAnalyticsPersonel: TevDBComboBox;
    chbEvolutionPayroll: TevDBCheckBox;
    pmEvoProduct: TevPopupMenu;
    miEvoProductCopyTo: TMenuItem;

    procedure drgpActive_UserEnter(Sender: TObject);
    procedure wwdsDetailUpdateData(Sender: TObject);
    procedure editPasswordChange(Sender: TObject);
    procedure wwdsDetailStateChange(Sender: TObject);
    procedure wwlcClientNumberChange(Sender: TObject);
    procedure bbtnLoadClick(Sender: TObject);
    procedure btnSecRightsClick(Sender: TObject);
    procedure AddGroup(Sender: TObject);
    procedure RemoveFromGroup(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure rbOneClientClick(Sender: TObject);
    procedure wwdgEDIT_SB_USERDblClick(Sender: TObject);
    procedure pctlEDIT_SB_USERChange(Sender: TObject);
    procedure wwlcClientNumberExit(Sender: TObject);
    procedure pctlEDIT_SB_USERChanging(Sender: TObject;
          var AllowChange: Boolean);
    procedure ClientFrame1sbAddClick(Sender: TObject);
    procedure evButton1Click(Sender: TObject);
    procedure wwcbDepartmentChange(Sender: TObject);
    procedure wwlcClientNumberEnter(Sender: TObject);
    procedure wwcbSecurity_LevelDropDown(Sender: TObject);
    procedure wwdgEDIT_SB_USERCalcCellColors(Sender: TObject;
      Field: TField; State: TGridDrawState; Highlight: Boolean;
      AFont: TFont; ABrush: TBrush);
    procedure tsDBDTHide(Sender: TObject);
    procedure tsDBDTShow(Sender: TObject);
    procedure tsClientsShow(Sender: TObject);
    procedure tsClientsHide(Sender: TObject);
    procedure miEvoProductCopyToClick(Sender: TObject);
  private
    FCurrentUserClNbr: ShortString;
    FGroupsAreLoaded: Boolean;
    FSelectedUser: Integer;
    procedure LoadGroups;
    procedure CheckBeforeGroupEditing;
    procedure UpdateUserFunctions;
    procedure SetButtonsState;
    procedure OnClientsChange(Sender: TObject);
    procedure CMCloseApp(var Message: TMessage); message CM_CLOSE_APP;
    procedure SetStereoType;
    procedure CopyTo(const aFieldName, aFieldCaption: string);    
  public
    function GetSystemDefaultGroupNbr: Integer;
    function GetDefaultDataSet: TevClientDataSet; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure AfterClick(Kind: Integer); override;
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
  end;

implementation

uses EvConsts, EvUtils,
  SPD_EDIT_SECURITY, sSecurityClassDefs, SysUtils, SPackageEntry;

{$R *.DFM}

{**************************************}
{* TEDIT_SB_USER.drgpACTIVE_USEREnter *}
{**************************************}

procedure TEDIT_SB_USER.drgpActive_UserEnter(Sender: TObject);
begin
  inherited;
  { forcing selection when in insert mode -
    DBRadioGroup won't show focus when tabbed to when in insert mode }
  if drgpACTIVE_USER.Value = '' then
    drgpACTIVE_USER.ItemIndex := 0;
end;

procedure TEDIT_SB_USER.wwdsDetailUpdateData(Sender: TObject);
var
  s: string;
  cd: TEvClientDataSet;
begin
  inherited;
  if ((ctx_AccountRights.Level = slManager)
  and (wwcbSecurity_Level.Value <> USER_SEC_LEVEL_MANAGER)
  and (wwcbSecurity_Level.Value <> USER_SEC_LEVEL_SUPERVISOR)
  and (wwcbSecurity_Level.Value <> USER_SEC_LEVEL_USER))
  or ((ctx_AccountRights.Level = slSupervisor)
  and (wwcbSecurity_Level.Value <> USER_SEC_LEVEL_SUPERVISOR)
  and (wwcbSecurity_Level.Value <> USER_SEC_LEVEL_USER))
  or ((ctx_AccountRights.Level = slSBUser)
  and (wwcbSecurity_Level.Value <> USER_SEC_LEVEL_USER)) then
  begin // just for redundency
    EvErrMessage('You can not modify users with security level higher than yours');
    AbortEx;
    Exit;
  end;
  if Pos(' ', dedtUser_ID.Text) <> 0 then
  begin
    EvErrMessage('User ID should not have any space characters in it!');
    AbortEx;
    Exit;
  end;
  if (editPassword.Text = '') or (editConfirmPassword.Text = '') then
  begin
    EvErrMessage('Password needs to be entered and confirmed!');
    AbortEx;
    Exit;
  end;
  if editPassword.Text <> editConfirmPassword.Text then
  begin
    EvErrMessage('The password does not match the confirmation password.');
    AbortEx;
    Exit;
  end;
  if ConvertNull(wwdsDetail.DataSet.FieldByName('USER_PASSWORD').AsString, '') <> editPassword.Text then
  begin
    try
      ctx_Security.ValidatePassword(editPassword.Text);
    except
      on E: ESecurity do
      begin
        E.Details := 'Password requirements:'#13#13 + ctx_Security.GetPasswordRules.Value['Description'];
        raise
      end
      else
        raise;
    end;

    if wwdsDetail.DataSet.FieldByName('USER_PASSWORD').AsString <> HashPassword(editPassword.Text) then
    begin
      wwdsDetail.DataSet.FieldByName('USER_PASSWORD').AsString := HashPassword(editPassword.Text);
      editPassword.Text := wwdsDetail.DataSet.FieldByName('USER_PASSWORD').AsString;
      editConfirmPassword.Text := editPassword.Text;
    end;

    if AnsiSameText(Context.UserAccount.User, wwdsDetail.DataSet.FieldByName('USER_ID').AsString) then
      PostMessage(Handle, CM_CLOSE_APP, 0, 0);
  end;

  if not (rbAllClients.Checked or rbOneClient.Checked or rbWebUser.Checked) then
  begin
    EvErrMessage('User has to have a client access rights chosen.');
    AbortEx;
    Exit;
  end;

  if rbOneClient.Checked and (wwlcClientNumber.LookupValue = '') then
  begin
    EvErrMessage('Client user has to have a client number chosen.');
    AbortEx;
    Exit;
  end;

  if (wwdsDetail.DataSet.FieldByName('DEPARTMENT').AsString <> '') and (wwdsDetail.DataSet.FieldByName('DEPARTMENT').AsString[1] =
    USER_DEPARTMENT_CLIENT) and (wwlcClientNumber.LookupValue = '') then
  begin
    if not rbWebUser.Checked then
    begin
      EvErrMessage('Client user has to have a client number chosen.');
      AbortEx;
      Exit;
    end
    else
      EvMessage('Don''t forget to attach client(s) to the ' + dedtUser_ID.Text);
  end;

  cd := TevClientDataSet.Create(Self);
  try
    cd.CloneCursor(TevClientDataSet(wwdsDetail.DataSet), True);
    cd.Filter := 'SB_USER_NBR<>'+ IntToStr(wwdsDetail.DataSet['SB_USER_NBR']);
    cd.Filtered := True;
    if cd.Locate(dedtUser_ID.DataField, wwdsDetail.DataSet[dedtUser_ID.DataField], [loCaseInsensitive]) then
    begin
      EvErrMessage('User ID is duplicated');
      AbortEx;
    end;
  finally
    cd.Free;
  end;

  if rbOneClient.Checked then
    s := wwlcClientNumber.LookupValue
  else if rbAllClients.Checked then
    s := '-1'
  else
    s := '-2';

  if ExtractFromFiller(wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString, 1, 8) <> s then
    wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString :=
      PutIntoFiller(wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString, s, 1, 8);

  if drgpActive_User.Values[drgpActive_User.ItemIndex] <> drgpActive_User.DataSource.Dataset[drgpActive_User.DataField] then
    drgpActive_User.DataSource.DataSet[drgpActive_User.DataField] := drgpActive_User.Values[drgpActive_User.ItemIndex];

  wwdsDetail.DataSet['USER_UPDATE_OPTIONS'] := '-';
end;

procedure TEDIT_SB_USER.editPasswordChange(Sender: TObject);
begin
  inherited;
  with wwdsDetail.DataSet do
    if not FieldByName('USER_PASSWORD').IsNull then
      if FieldByName('USER_PASSWORD').AsString <> editPassword.Text then
        if not (State in [dsInsert, dsEdit]) then
        begin
          if RecordCount = 0 then
            Insert
          else
            Edit;
        end;
end;

procedure TEDIT_SB_USER.wwdsDetailStateChange(Sender: TObject);
begin
  inherited;
  if Assigned(wwdsDetail.DataSet) then
  begin
    if (wwdsDetail.DataSet.State = dsInsert) then
    begin
      editPassword.Text := '';
      editConfirmPassword.Text := editPassword.Text;
      wwlcClientNumber.LookupValue := '';
      rbAllClients.Checked := False;
      rbOneClient.Checked := False;
      if Context.UserAccount.AccountType = uatRemote then
      begin
        rbOneClient.Checked := True;
        wwlcClientNumber.LookupValue := FCurrentUserClNbr;
      end;
      wwdsDetail.DataSet['SECURITY_LEVEL'] := USER_SEC_LEVEL_USER;
      wwdsDetail.DataSet['PASSWORD_CHANGE_DATE'] := Date+ Trunc(DM_SERVICE_BUREAU.SB.USER_PASSWORD_DURATION_IN_DAYS.Value);
      wwdsDetail.DataSet['USER_UPDATE_OPTIONS'] := '-';
      wwdsDetail.DataSet['USER_FUNCTIONS'] := PutIntoFiller('', '-', 9, 1);
      LoadGroups;
    end
    else
    if wwdsDetail.DataSet.State = dsBrowse then
    begin
      LoadGroups;
    end;
    rbAllClients.Enabled := Context.UserAccount.AccountType in [uatServiceBureau, uatSBAdmin];
    rbWebUser.Enabled := Context.UserAccount.AccountType <> uatRemote;
  end;
end;

procedure TEDIT_SB_USER.wwlcClientNumberChange(Sender: TObject);
begin
  inherited;
  with wwdsDetail.DataSet do
    if (Self.Tag = 1) and (ExtractFromFiller(wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString, 1, 8) <> wwlcClientNumber.LookupValue) then
      UpdateUserFunctions;
end;

procedure TEDIT_SB_USER.bbtnLoadClick(Sender: TObject);
var
  aStream: IisStream;
begin
  inherited;

  if odlgGetSignature.Execute then
  begin
    CheckFileSize(odlgGetSignature.FileName, 700);

    if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
      wwdsDetail.DataSet.Edit;

    aStream := TisStream.CreateFromFile(odlgGetSignature.FileName);
    TevClientDataSet(wwdsDetail.DataSet).UpdateBlobData('USER_SIGNATURE', aStream);
  end;
end;

procedure TEDIT_SB_USER.btnSecRightsClick(Sender: TObject);
var
  o: TEDIT_SECURITY;
  c: IevSecurityStructure;
begin
  inherited;

  ctx_StartWait('Loading security information');
  try
    c := ctx_Security.GetUserSecStructure(wwdsDetail.DataSet.FieldByName('SB_USER_NBR').AsInteger);
  finally
    ctx_EndWait;
  end;

  o := TEDIT_SECURITY.Create(nil);
  try
    o.Caption := 'User Security rights ('+ wwdsDetail.DataSet.FieldByName('USER_ID').AsString + ')';
    o.SBUserNbr := wwdsDetail.DataSet.FieldByName('SB_USER_NBR').AsInteger;
    o.Collection := c.Obj;
    o.btnSave.Enabled := not GetIfReadOnly;
    ctx_UpdateWait;
    if o.ShowModal = mrOk then
    begin
      c.Obj := o.Collection;
      ctx_Security.SetUserSecStructure(wwdsDetail.DataSet.FieldByName('SB_USER_NBR').AsInteger, c);
    end
    else
      c.Obj := o.Collection;
  finally
    o.Free;
  end;
end;

procedure TEDIT_SB_USER.LoadGroups;
var
  cd: TevClientDataset;
begin
  wwGroupsSelDS.DisableControls;
  wwGroupsDS.DisableControls;
  try
    if wwGroupsSelDS.Active and (wwGroupsSelDS.ChangeCount > 0) then wwGroupsSelDS.CancelUpdates;
    wwGroupsSelDS.Close;
    wwGroupsSelDS.CreateDataSet;

    if wwGroupsDS.Active and (wwGroupsDS.ChangeCount > 0) then wwGroupsDS.CancelUpdates;
    wwGroupsDS.Close;
    wwGroupsDS.CreateDataSet;

    if (Context.UserAccount.InternalNbr = wwdsDetail.DataSet['SB_USER_NBR']) and (Context.UserAccount.AccountType = uatSBAdmin) then
    begin
      DM_SERVICE_BUREAU.SB_SEC_GROUPS.First;
      while not DM_SERVICE_BUREAU.SB_SEC_GROUPS.Eof do
      begin
        wwGroupsSelDS.AppendRecord([DM_SERVICE_BUREAU.SB_SEC_GROUPS['SB_SEC_GROUPS_NBR'], DM_SERVICE_BUREAU.SB_SEC_GROUPS['NAME']]);
        DM_SERVICE_BUREAU.SB_SEC_GROUPS.Next;
      end;
    end
    else
    begin
      with DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS, DM_SERVICE_BUREAU do
        if Active then
        begin
          cd := TevClientDataSet.Create(nil);
          try
            cd.CloneCursor(DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS, True);
            SB_SEC_GROUPS.First;
            while not SB_SEC_GROUPS.Eof do
            begin
              if Context.UserAccount.AccountType = uatSBAdmin then
                wwGroupsDS.AppendRecord([SB_SEC_GROUPS['SB_SEC_GROUPS_NBR'], SB_SEC_GROUPS['NAME']])
              else
              begin
                if (cd.Locate('SB_USER_NBR;SB_SEC_GROUPS_NBR', VarArrayOf([Context.UserAccount.InternalNbr, SB_SEC_GROUPS['SB_SEC_GROUPS_NBR']]), [])) then
                  wwGroupsDS.AppendRecord([SB_SEC_GROUPS['SB_SEC_GROUPS_NBR'], SB_SEC_GROUPS['NAME']]);
              end;

              SB_SEC_GROUPS.Next;
            end;
            wwGroupsDS.MergeChangeLog;

            if (wwdsDetail.DataSet['SB_USER_NBR'] = Context.UserAccount.InternalNbr) and (Context.UserAccount.AccountType = uatSBAdmin) then
            begin
              wwGroupsDS.First;
              while not wwGroupsDS.eof do
              begin
                wwGroupsSelDS.AppendRecord([wwGroupsDS['SB_SEC_GROUPS_NBR'], wwGroupsDS['NAME']]);
                wwGroupsDS.Delete;
              end;
            end
            else
            begin
              SB_SEC_GROUPS.First;
              First;
              while not Eof do
              begin
                if FieldValues['SB_USER_NBR'] = wwdsDetail.DataSet['SB_USER_NBR'] then
                begin
                  if wwGroupsDS.Locate('SB_SEC_GROUPS_NBR', FieldValues['SB_SEC_GROUPS_NBR'], []) then
                  begin
                    wwGroupsSelDS.AppendRecord([FieldValues['SB_SEC_GROUPS_NBR'], wwGroupsDS['NAME']]);
                    wwGroupsDS.Delete;
                  end;
                end;
                Next;
              end;
            end;
          finally
            cd.Free;
          end;
        end;
    end;
  finally
    wwGroupsSelDS.First;
    wwGroupsSelDS.EnableControls;
    wwGroupsDS.First;
    wwGroupsDS.EnableControls;
  end;
end;

procedure TEDIT_SB_USER.AddGroup(Sender: TObject);
begin
  if (wwGroupsSelDS.RecordCount = 1) then
  begin
    if EvMessage('You are about to assign multiple security groups for this user.  Are you sure you want to continue?', mtConfirmation, mbOKCancel, mbCancel, 0) = mrCancel then
      Exit;
  end;

  inherited;
  CheckBeforeGroupEditing;
  if wwDBGroupGridAvail.DataSource.DataSet.Active and not VarIsNull(wwDBGroupGridAvail.DataSource.DataSet['SB_SEC_GROUPS_NBR']) then
    if not wwGroupsSelDS.Locate('SB_SEC_GROUPS_NBR', wwDBGroupGridAvail.DataSource.DataSet['SB_SEC_GROUPS_NBR'], []) then
    begin
      try
        wwGroupsSelDS.AppendRecord([wwGroupsDS['SB_SEC_GROUPS_NBR'], wwGroupsDS['NAME']]);
        wwGroupsDS.Delete;
        if not DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Locate('SB_SEC_GROUPS_NBR;SB_USER_NBR', VarArrayOf([wwGroupsSelDS['SB_SEC_GROUPS_NBR'],
          wwdsDetail.DataSet['SB_USER_NBR']]), []) then
        begin
          DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Append;
          try
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS['SB_SEC_GROUPS_NBR'] := wwGroupsSelDS['SB_SEC_GROUPS_NBR'];
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS['SB_USER_NBR'] := wwdsDetail.DataSet['SB_USER_NBR'];
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Post;

            DBDTFrame1.InvalidateData;
          except
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Cancel;
          end;
        end;
      finally
      end;
    end;
end;

procedure TEDIT_SB_USER.RemoveFromGroup(Sender: TObject);
begin
  inherited;
  CheckBeforeGroupEditing;
  if wwDBGroupGridSelected.DataSource.DataSet.Active and not VarIsNull(wwDBGroupGridSelected.DataSource.DataSet['SB_SEC_GROUPS_NBR'])
    and (wwDBGroupGridSelected.DataSource.DataSet['SB_SEC_GROUPS_NBR'] <>  GetSystemDefaultGroupNbr)
  then
    if not wwGroupsDS.Locate('SB_SEC_GROUPS_NBR', wwDBGroupGridSelected.DataSource.DataSet['SB_SEC_GROUPS_NBR'], []) then
    begin
      wwGroupsDS.AppendRecord([wwGroupsSelDS['SB_SEC_GROUPS_NBR'], wwGroupsSelDS['NAME']]);
      wwGroupsSelDS.Delete;
      if DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Locate('SB_SEC_GROUPS_NBR;SB_USER_NBR', VarArrayOf([wwGroupsDS['SB_SEC_GROUPS_NBR'],
        wwdsDetail.DataSet['SB_USER_NBR']]), []) then
      begin
        DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Delete;

        DBDTFrame1.InvalidateData;
      end;
    end;
end;

procedure TEDIT_SB_USER.CheckBeforeGroupEditing;
begin
  if wwdsDetail.DataSet.State = dsInsert then
  try
    wwdsDetail.DataSet.Post;
    SetButtonsState;
    (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  except
    raise EUpdateError.CreateHelp('Fill out all detail page', IDH_ConsistencyViolation);
  end;
end;

procedure TEDIT_SB_USER.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS, EditDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_SEC_CLIENTS, EditDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS, EditDataSets, '');

  AddDSWithCheck([DM_SERVICE_BUREAU.SB_SEC_GROUPS, DM_SERVICE_BUREAU.SB, DM_SERVICE_BUREAU.SB_ACCOUNTANT,
    DM_TEMPORARY.TMP_CL], SupportDataSets, '');
  DM_TEMPORARY.TMP_CL.IndexFieldNames := 'CUSTOM_CLIENT_NUMBER';
end;

function TEDIT_SB_USER.GetInsertControl: TWinControl;
begin
  Result := dedtUser_ID;
end;

procedure TEDIT_SB_USER.Activate;
var
  i: Integer;
  sAccessCond, s : string;

begin
  ClientFrame1.OnChange := OnClientsChange;
  DM_SERVICE_BUREAU.SB_USER.DeleteChildren := True;
  inherited;
  ClientFrame1.ActiveMode := False;
  DBDTFrame1.ActiveMode := False;
  ClientFrame1.dsDetail.DataSet := DM_SERVICE_BUREAU.SB_SEC_CLIENTS;
  ClientFrame1.dsMaster.DataSet := GetDefaultDataSet;
  DBDTFrame1.dsMaster.DataSet := GetDefaultDataSet;
  DBDTFrame1.dsClients.DataSet := DM_SERVICE_BUREAU.SB_SEC_CLIENTS;
  DBDTFrame1.dsDetail.DataSet := DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS;
  with wwcbSecurity_Level.Items do
    for i := Count - 1 downto 0 do
      if ((ctx_AccountRights.Level = slManager)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_MANAGER)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_SUPERVISOR)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_USER))
      or ((ctx_AccountRights.Level = slSupervisor)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_SUPERVISOR)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_USER))
      or ((ctx_AccountRights.Level = slSBUser)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_USER)) then
        Delete(i);

  s := '';
  if wwdsDetail.DataSet.Locate('SB_USER_NBR', VarArrayOf([Context.UserAccount.InternalNbr]), []) then
  begin
      FCurrentUserClNbr := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString, 1, 8);
      if (FCurrentUserClNbr = '') or ( FCurrentUserClNbr = '-1' ) or ( FCurrentUserClNbr = '-2' ) then
        FCurrentUserClNbr := '';

      if (Context.UserAccount.AccountType = uatRemote) then
        s := '(USER_FUNCTIONS like ''%'+ FCurrentUserClNbr+ '%'' or USER_FUNCTIONS = '''+ FCurrentUserClNbr+ ''')';
      case ctx_AccountRights.Level of
      slAdministrator:
        sAccessCond := '';
      slManager:
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_MANAGER)+ ','+
          QuotedStr(USER_SEC_LEVEL_SUPERVISOR)+ ','+
          QuotedStr(USER_SEC_LEVEL_USER)+ ')';
      slSupervisor:
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_SUPERVISOR)+ ','+
          QuotedStr(USER_SEC_LEVEL_USER)+ ')';
      slSBUser:
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_USER)+ ')';
      slEmployee:
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_EMPLOYEE)+ ')';
      else
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_NONE)+ ')';
      end;

      if sAccessCond <> '' then
        if s <> '' then
          s := s+ ' and '+ sAccessCond
        else
          s := sAccessCond;
  end;

  if Length(s) > 0 then
  begin
    DM_SERVICE_BUREAU.SB_USER.DataRequired(s);
  end;
  if ctx_AccountRights.Level = slSBUser then
  begin
    DM_SERVICE_BUREAU.SB_USER.DataRequired('SB_USER_NBR = ' + IntToStr(Context.UserAccount.InternalNbr));
  end;
  
  cbAnalyticsPersonel.Enabled := DM_SERVICE_BUREAU.SB.ANALYTICS_LICENSE.Value = 'Y';
  evLabel2.Enabled := cbAnalyticsPersonel.Enabled;
end;

function TEDIT_SB_USER.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_USER;
end;

function TEDIT_SB_USER.GetSystemDefaultGroupNbr: Integer;
var
      s : String;
      Q : IevQuery;
begin
     result := 0;
     s :=  'Select SB_SEC_GROUPS_NBR From SB_SEC_GROUPS Where ' +
     '{AsOfNow<SB_SEC_GROUPS>} and Name = ''' + sSystemDefaultGroup + '''';
     Q := TevQuery.Create(s);
     Q.Execute;
     if Q.Result.RecordCount > 0 then
      Result := Q.Result.Fields[0].AsInteger;
end;


procedure TEDIT_SB_USER.ButtonClicked(Kind: Integer; var Handled: Boolean);
var
 cdFilters: IevDataSet;
 FilterType: String;
 DefaultGroupNbr: integer;

  function GetUserFilters: IevDataSet;
  var
    Q: IevQuery;
  begin
     Q := TevQuery.Create('GenericSelectCurrentWithCondition');
     Q.Macros.AddValue('Columns', 'SB_SEC_GROUPS_NBR, DATABASE_TYPE, TABLE_NAME, FILTER_TYPE, CUSTOM_EXPR, CL_NBR');
     Q.Macros.AddValue('TableName', 'SB_SEC_ROW_FILTERS');
     Q.Macros.AddValue('Condition', 'SB_USER_NBR = '+IntToStr(Context.UserAccount.InternalNbr));
     Q.Execute;
     Result := Q.Result;
  end;

begin

  if (Kind = NavInsert) then
  begin
   tshtUser_Groups.TabVisible := False;
   tsClients.TabVisible := False;
   tsDBDT.TabVisible := False;
   pctlEDIT_SB_USER.ActivatePage(tshtUser_Detail);
  End;

  if (Kind = NavOK) then
  begin
    if wwdsDetail.DataSet.State in [dsInsert] then
    begin
      DefaultGroupNbr:= GetSystemDefaultGroupNbr;

      if DefaultGroupNbr <> 0 then
      begin
          DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Append;
          try
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS['SB_SEC_GROUPS_NBR'] := DefaultGroupNbr;
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS['SB_USER_NBR'] := wwdsDetail.DataSet['SB_USER_NBR'];
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Post;
          except
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Cancel;
          end;
      end;

      cdFilters := GetUserFilters;
      cdFilters.First;
      while not cdFilters.Eof do
      begin
        FilterType := cdFilters.FieldByName('FILTER_TYPE').AsString;
        if ((cdFilters['DATABASE_TYPE'] = 'C') and
            (cdFilters.FieldByName('SB_SEC_GROUPS_NBR').IsNull)) and
               ((FilterType = '0') or (FilterType = '1') or (FilterType = '2') or
                (FilterType = '3') or (FilterType = '4')) then
        begin
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.Append;
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS['DATABASE_TYPE'] := cdFilters['DATABASE_TYPE'];
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS['TABLE_NAME'] := cdFilters['TABLE_NAME'];
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS['FILTER_TYPE'] := cdFilters['FILTER_TYPE'];
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS['CL_NBR'] := cdFilters['CL_NBR'];
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS['CUSTOM_EXPR'] := cdFilters['CUSTOM_EXPR'];
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS['SB_USER_NBR'] :=
                   wwdsDetail.DataSet.FieldByName('SB_USER_NBR').AsInteger;
          DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS.Post;
          cdFilters.Next;
        end;
      end;
    end;
  end;
end;

procedure TEDIT_SB_USER.AfterClick(Kind: Integer);
begin
  inherited;
  if (wwdsDetail.DataSet['SB_USER_NBR'] <> null) and (wwdsDetail.DataSet.FieldByName('SB_USER_NBR').AsInteger < 0)
    and (Kind <> NavCommit) then
    btnSecRights.Enabled := false
  else
  begin
    SetButtonsState;
    btnSecRights.Enabled := btnSecRights.Enabled and (Kind <> NavInsert);
  end;

  // Notify security cache to refresh this account
  // TODO!!!
  // This is a workaround! This notification should be generated in DB Access on RP side
  if Kind = NavCommit then
    Mainboard.GlobalCallbacks.NotifySecurityChange('Account ' + EncodeUserAtDomain(wwdsDetail.DataSet.FieldByName('USER_ID').AsString, Context.UserAccount.Domain));

  if (Kind in [NavOk, NavCommit, NavCancel, NavAbort]) then
  begin
   tshtUser_Groups.TabVisible := True;
   tsClients.TabVisible := True;
   tsDBDT.TabVisible := True;
   SetStereoType;
  end;

end;

procedure TEDIT_SB_USER.SetStereoType;
var
  s: string;
begin
    s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString, 1, 8);
    gbUserClientDb.Tag := 1;
    try
      if s = '-1' then
      begin
        rbAllClients.Checked := True;
        wwlcClientNumber.LookupValue := '';
        wwlcClientNumber.Enabled := False;
      end
      else if s = '-2' then
      begin
        rbWebUser.Checked := True;
        wwlcClientNumber.LookupValue := '';
        wwlcClientNumber.Enabled := True;
      end
      else if s <> '' then
      begin
        rbOneClient.Checked := True;
        wwlcClientNumber.LookupValue := s;
        wwlcClientNumber.Enabled := True;
      end
      else
      begin
        rbAllClients.Checked := False;
        rbWebUser.Checked := False;
        rbOneClient.Checked := False;
        wwlcClientNumber.LookupValue := '';
        wwlcClientNumber.Enabled := False;
      end;
    finally
      gbUserClientDb.Tag := 0;
    end;

    rbAllClients.Enabled := Context.UserAccount.AccountType in [uatServiceBureau, uatSBAdmin];
    rbWebUser.Enabled := Context.UserAccount.AccountType <> uatRemote;

end;

procedure TEDIT_SB_USER.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (wwdsDetail.DataSet.State = dsBrowse) and
     (FSelectedUser <> wwdsDetail.DataSet.FieldByName('SB_USER_NBR').AsInteger) then
  begin
    FSelectedUser := wwdsDetail.DataSet.FieldByName('SB_USER_NBR').AsInteger;
    evButton1.Enabled := True;
    pBlock.Visible := wwdsDetail.DataSet.FieldByName('WRONG_PSWD_ATTEMPTS').AsInteger >= 3;
    editPassword.Text := ConvertNull(wwdsDetail.DataSet.FieldByName('USER_PASSWORD').AsString, '');
    editConfirmPassword.Text := editPassword.Text;

    if pctlEDIT_SB_USER.ActivePage = tshtBrowse then
      FGroupsAreLoaded := False
    else
      LoadGroups;

    SetStereoType;

    DBDTFrame1.SB_USER_NBR := FSelectedUser;
    ClientFrame1.SB_USER_NBR := FSelectedUser;
  end;

  SetButtonsState;

  if (wwdsDetail.DataSet.State <> dsInsert) and
     (wwdsDetail.DataSet['SB_USER_NBR'] = Context.UserAccount.InternalNbr) and (Context.UserAccount.AccountType = uatSBAdmin) then
    wwdsDetail.DataSet.FieldByName('USER_ID').ReadOnly := True
  else
    wwdsDetail.DataSet.FieldByName('USER_ID').ReadOnly := False;
end;

procedure TEDIT_SB_USER.rbOneClientClick(Sender: TObject);
var
  s: string;
begin
  inherited;
  s := ExtractFromFiller(wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString, 1, 8);
  with wwdsDetail.DataSet do
    if (rbAllClients.Checked and (s <> '-1') or
        rbOneClient.Checked and ((s = '-1') or (s = '-2') or (s = '') ) or
        rbWebUser.Checked and (s <> '-2'))
    and (gbUserClientDb.Tag = 0)
    and not (State in [dsInsert, dsEdit]) then
    begin
      if RecordCount = 0 then
        Insert
      else
        Edit;
    end;
  wwlcClientNumber.Enabled := rbOneClient.Checked or (wwcbDepartment.Value = USER_DEPARTMENT_CLIENT);

  if wwdsDetail.DataSet.State in [dsInsert, dsEdit] then
    UpdateUserFunctions;
end;

procedure TEDIT_SB_USER.wwdgEDIT_SB_USERDblClick(Sender: TObject);
begin
  inherited;
  btnSecRights.Click;
end;

procedure TEDIT_SB_USER.DeActivate;
begin
  inherited;
  DM_SERVICE_BUREAU.SB_USER.DeleteChildren := False;
  wwdsDetail.DataSet.Filtered := False;
  wwdsDetail.DataSet.Filter := '';
end;

procedure TEDIT_SB_USER.pctlEDIT_SB_USERChange(Sender: TObject);
begin
  inherited;
  if not FGroupsAreLoaded then
  begin
    FGroupsAreLoaded := True;
    LoadGroups;
  end;
end;


procedure TEDIT_SB_USER.UpdateUserFunctions;
var
  s: string;
begin
  if rbOneClient.Checked then
    s := wwlcClientNumber.LookupValue
  else if rbAllClients.Checked then
    s := '-1'
  else
    s := '-2';

  if ExtractFromFiller(wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString, 1, 8) <> s then
  begin
    if not (wwdsDetail.DataSet.State in [dsInsert, dsEdit]) then
      wwdsDetail.DataSet.Edit;
    wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString :=
      PutIntoFiller(wwdsDetail.DataSet.FieldByName('USER_FUNCTIONS').AsString, s, 1, 8);
  end;
end;

procedure TEDIT_SB_USER.wwlcClientNumberExit(Sender: TObject);
begin
  inherited;
  Self.Tag := 0;
end;

procedure TEDIT_SB_USER.pctlEDIT_SB_USERChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  CheckBeforeGroupEditing;
end;

procedure TEDIT_SB_USER.ClientFrame1sbAddClick(Sender: TObject);
begin
  inherited;

  //    Added By CA.
  // If you want to add any client to  Web or Remote users,  this one has to be under control..
  // This message about this.

  if (DBDTFrame1.dsMaster.DataSet.FieldByName('StereoType').AsString ='Web') and
     (DM_SERVICE_BUREAU.SB_SEC_CLIENTS.RecordCount = 1)  then
  begin
    if EvMessage('(Web User) You are about to grant access to multiple clients for this user.  Are you sure you want to continue?', mtConfirmation, mbOKCancel, mbCancel, 0) = mrCancel then
      Exit;
  end;

  if (DBDTFrame1.dsMaster.DataSet.FieldByName('StereoType').AsString ='Remote') and
     (DM_SERVICE_BUREAU.SB_SEC_CLIENTS.RecordCount = 1)  then
  begin
    if EvMessage('(Remote User) You are about to grant access to multiple clients for this user.  Are you sure you want to continue?', mtConfirmation, mbOKCancel, mbCancel, 0) = mrCancel then
      Exit;
  end;
  if DBDTFrame1.dsMaster.DataSet.FieldByName('StereoType').AsString ='' then
  begin
    if EvMessage('(Please Enter StereoType option) You are about to grant access to multiple clients for this user.  Are you sure you want to continue?', mtConfirmation, mbOKCancel, mbCancel, 0) = mrCancel then
      Exit;
  end;

  ClientFrame1.sbAddClick(Sender);

end;

procedure TEDIT_SB_USER.evButton1Click(Sender: TObject);
var
  sPassword: String;
begin
  sPassword := ctx_Security.GenerateNewPassword;
  if EvDialog('Password', 'Please type a temporary password', sPassword) then
  begin
    ctx_Security.ValidatePassword(sPassword);
    if wwdsDetail.DataSet.State = dsBrowse then
      wwdsDetail.DataSet.Edit;

    DM_SERVICE_BUREAU.SB_USER.WRONG_PSWD_ATTEMPTS.AsInteger := 0;
    
    DM_SERVICE_BUREAU.SB_USER.LOGIN_QUESTION1.Clear;
    DM_SERVICE_BUREAU.SB_USER.LOGIN_ANSWER1.Clear;
    DM_SERVICE_BUREAU.SB_USER.LOGIN_QUESTION2.Clear;
    DM_SERVICE_BUREAU.SB_USER.LOGIN_ANSWER2.Clear;
    DM_SERVICE_BUREAU.SB_USER.LOGIN_QUESTION3.Clear;
    DM_SERVICE_BUREAU.SB_USER.LOGIN_ANSWER3.Clear;

    DM_SERVICE_BUREAU.SB_USER.SEC_QUESTION1.Clear;
    DM_SERVICE_BUREAU.SB_USER.SEC_ANSWER1.Clear;
    DM_SERVICE_BUREAU.SB_USER.SEC_QUESTION2.Clear;
    DM_SERVICE_BUREAU.SB_USER.SEC_ANSWER2.Clear;

    DM_SERVICE_BUREAU.SB_USER.PASSWORD_CHANGE_DATE.AsDateTime := DateOf(SysTime);

    editPassword.Text := sPassword;
    editConfirmPassword.Text := sPassword;
    evButton1.Enabled := False;
  end;
end;


procedure TEDIT_SB_USER.wwcbDepartmentChange(Sender: TObject);
begin
  inherited;
  wwlcClientNumber.Enabled := rbOneClient.Checked or (wwcbDepartment.Value = USER_DEPARTMENT_CLIENT);
end;

procedure TEDIT_SB_USER.wwlcClientNumberEnter(Sender: TObject);
begin
  inherited;
  Self.Tag := 1;
end;

procedure TEDIT_SB_USER.wwcbSecurity_LevelDropDown(Sender: TObject);
var
  i: integer;
begin
  inherited;
  with wwcbSecurity_Level.Items do
    for i := Count - 1 downto 0 do
      if ((ctx_AccountRights.Level = slManager)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_MANAGER)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_SUPERVISOR)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_USER))
      or ((ctx_AccountRights.Level = slSupervisor)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_SUPERVISOR)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_USER))
      or ((ctx_AccountRights.Level = slSBUser)
      and (Strings[i][Length(Strings[i])] <> USER_SEC_LEVEL_USER)) then
        Delete(i);
end;

procedure TEDIT_SB_USER.SetButtonsState;
begin
  if (wwdsDetail.DataSet['SB_USER_NBR'] = Context.UserAccount.InternalNbr) and (Context.UserAccount.AccountType = uatSBAdmin) then
  begin
    btnSecRights.Enabled := False;
    SpeedButton1.Enabled := False;
    SpeedButton2.Enabled := False;
    ClientFrame1.sbAdd.Enabled := False;
    ClientFrame1.sbRemove.Enabled := False;
    DBDTFrame1.tv.Enabled := False;
    lAutoSec.Visible := True;
  end
  else
  begin
    btnSecRights.Enabled := (wwdsDetail.DataSet.State = dsBrowse) and (wwdsDetail.DataSet.RecordCount > 0);
    SpeedButton1.Enabled := True;
    SpeedButton2.Enabled := True;
    ClientFrame1.sbAdd.Enabled := True;
    ClientFrame1.sbRemove.Enabled := True;
    DBDTFrame1.tv.Enabled := True;
    lAutoSec.Visible := False;    
  end;
end;

procedure TEDIT_SB_USER.wwdgEDIT_SB_USERCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  if (wwdsDetail.DataSet['SB_USER_NBR'] = Context.UserAccount.InternalNbr) and (Context.UserAccount.AccountType = uatSBAdmin) then
    AFont.Style := [fsBold];
end;

procedure TEDIT_SB_USER.tsDBDTHide(Sender: TObject);
begin
  inherited;
  DBDTFrame1.ActiveMode := False;
end;

procedure TEDIT_SB_USER.tsDBDTShow(Sender: TObject);
begin
  inherited;
  DBDTFrame1.ActiveMode := True;
end;

procedure TEDIT_SB_USER.tsClientsShow(Sender: TObject);
begin
  inherited;
  ClientFrame1.ActiveMode := True;
end;

procedure TEDIT_SB_USER.tsClientsHide(Sender: TObject);
begin
  inherited;
  ClientFrame1.ActiveMode := False;
end;

procedure TEDIT_SB_USER.OnClientsChange(Sender: TObject);
begin
  DBDTFrame1.InvalidateData;
end;

procedure TEDIT_SB_USER.CMCloseApp(var Message: TMessage);
begin
  EvMessage('You have changed your password. Evolution will now close. You will need to login with your new password.');
  (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  ctx_EvolutionGUI.PasswordChangeClose;
end;

procedure TEDIT_SB_USER.miEvoProductCopyToClick(Sender: TObject);
begin
  inherited;
  CopyTo('EVOLUTION_PRODUCT', 'Evolution Payroll');
end;

procedure TEDIT_SB_USER.CopyTo(const aFieldName, aFieldCaption: string);
var
  k: integer;
  SBUserNbr: string;
  lFieldValue: variant;
begin
  SBUserNbr := DM_SERVICE_BUREAU.SB_USER.FieldByName('SB_USER_NBR').AsString;
  lFieldValue := DM_SERVICE_BUREAU.SB_USER.FieldByName( aFieldName ).Value;
  if SBUserNbr <> '' then
  begin
    with TSBUserChoiceQuery.Create( Self ) do
    try
      SetFilter('SB_USER_NBR <> ' + SBUserNbr);
      dgChoiceList.Options := dgChoiceList.Options + [dgMultiSelect];
      Caption := 'SB Users';
      btnNo.Caption := '&Cancel';
      btnYes.Caption := 'C&opy';
      btnAll.Caption := 'Copy to &all';
      ConfirmAllMessage := 'This operation will copy the '+ aFieldCaption +' for all SB Users. Are you sure you want to do this?';
      if (ShowModal = mrYes) and (dgChoiceList.SelectedList.Count>0) then
      begin
        ctx_StartWait('Processing...');
        DM_SERVICE_BUREAU.SB_USER.DisableControls;
        try
          for k := 0 to dgChoiceList.SelectedList.Count - 1 do
          begin
            cdsChoiceList.GotoBookmark( dgChoiceList.SelectedList[k] );
            DM_SERVICE_BUREAU.SB_USER.Locate('SB_USER_NBR', cdsChoiceList.FieldByName('SB_USER_NBR').AsString, []);
            DM_SERVICE_BUREAU.SB_USER.Edit;
            DM_SERVICE_BUREAU.SB_USER[ aFieldName ] := lFieldValue;
            DM_SERVICE_BUREAU.SB_USER.Post;
          end;
        finally
          DM_SERVICE_BUREAU.SB_USER.EnableControls;
          ctx_EndWait;
          DM_SERVICE_BUREAU.SB_USER.Locate('SB_USER_NBR', SBUserNbr, []);
        end;
      end;
    finally
      Free;
    end;
  end;
end;

initialization
  RegisterClass(TEDIT_SB_USER);

end.
