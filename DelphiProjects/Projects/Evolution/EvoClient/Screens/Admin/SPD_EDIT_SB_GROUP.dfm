inherited EDIT_SB_SEC_GROUP: TEDIT_SB_SEC_GROUP
  Width = 589
  object PC: TevPageControl [0]
    Left = 8
    Top = 32
    Width = 657
    Height = 473
    ActivePage = tsDBDT
    TabOrder = 0
    OnChanging = PCChanging
    object tsBrowse: TTabSheet
      Caption = 'Browse'
      object Label1: TevLabel
        Left = 8
        Top = 336
        Width = 73
        Height = 13
        Caption = '~Description'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwDBGrid1: TevDBGrid
        Left = 8
        Top = 8
        Width = 601
        Height = 321
        DisableThemesInTitle = False
        Selected.Strings = (
          'NAME'#9'60'#9'Name'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_SEC_GROUP\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsMaster
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = wwDBGrid1DblClick
        PaintOptions.AlternatingRowColor = clCream
      end
      object DBEdit1: TevDBEdit
        Left = 8
        Top = 352
        Width = 433
        Height = 21
        DataField = 'NAME'
        DataSource = wwdsMaster
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object btnSecRights: TevBitBtn
        Left = 480
        Top = 352
        Width = 123
        Height = 25
        Caption = 'Group Rights'
        TabOrder = 2
        OnClick = btnSecRightsClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDD8
          88DDDDDDDDDDDDDFFFDDD000000DDD80008DD888888DDDF888FD000000DDD80B
          BB08888888DDDF88888F000F0DDDD3B333B0888D8DDDD88DDD8800F0DDD443B0
          03B088D8DDD8D88FFF88D0F00D4443BBBBB0D8D88D88D888888D00FFF044443B
          BB0D88DDD8D88D8888DD00FF004F4DD3B0DD88DD88DD8DD88FDD000FF074DDD3
          B0DD888DD8D8DDD88FDD000FF0744DD3B0DD888DD8D88DD88FDD0000007FF4D3
          B088888888DDD8D88FDDD00004FF44D3B000D8888DDD88D88FFFDDDD444FF4D3
          BBB0DDDDD88DD8D8888FDDDD444FF4D3BBB0DDDD888DD8D8888FDDDD444444D3
          B033DDDD888888D88FDDDDDDD4444DDD38DDDDDDD8888DDDDDDD}
        NumGlyphs = 2
      end
    end
    object tsClients: TTabSheet
      Caption = 'Clients'
      ImageIndex = 1
      OnHide = tsClientsHide
      OnShow = tsClientsShow
      inline ClientFrame1: TClientFrame
        Left = 0
        Top = 0
        Width = 649
        Height = 445
        Align = alClient
        TabOrder = 0
        inherited sbAdd: TevSpeedButton
          Left = 271
          Width = 74
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
            DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
            DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
            78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
            8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
            F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
            F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
            DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
            DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
          NumGlyphs = 2
        end
        inherited sbRemove: TevSpeedButton
          Left = 271
          Width = 74
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
            DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
            DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
            78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
            448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
            F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
            F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
            44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
            DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
          NumGlyphs = 2
        end
        inherited gClientsAvail: TevDBGrid
          Width = 257
          OnDblClick = ClientFrame1gClientsAvailDblClick
        end
        inherited gClientsSelected: TevDBGrid
          Left = 352
          Width = 257
          OnDblClick = ClientFrame1gClientsSelectedDblClick
        end
      end
    end
    object tsDBDT: TTabSheet
      Caption = 'D/B/D/T'
      ImageIndex = 2
      OnHide = tsDBDTHide
      OnShow = tsDBDTShow
      inline DBDTFrame1: TDBDTFrame
        Left = 0
        Top = 0
        Width = 649
        Height = 445
        Align = alClient
        TabOrder = 0
        inherited Panel1: TPanel
          Width = 649
          Height = 445
          inherited tv: TevTreeView
            Width = 645
            Height = 441
          end
        end
      end
    end
    object thsUsers: TTabSheet
      Caption = 'Users'
      ImageIndex = 3
      object SpeedButton1: TevSpeedButton
        Left = 284
        Top = 288
        Width = 73
        Height = 22
        Caption = 'Add'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDD8DD
          DDDDDDDDDDDDD8DDDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8878
          DDDDDDDDDDDDD8D8DDDDDD88888848778DDDDD88888DFDDD8DDDDD8777774487
          78DDDD8DDDDD8FDDD8DDD88888884F48778DDDDDDDDD88FDDD8DD44444444FF4
          8778DFFFFFFF888FDDD8D4FFFFFFFFFF488DD88888888888FD8DD4FFFFFFFFFF
          F48DD888888888888FDDD4FFFFFFFFFFFF4DD8888888888888FDD4FFFFFFFFFF
          F4DDD8888888888888DDD4FFFFFFFFFF4DDDD888888888888DDDD44444444FF4
          DDDDD88888888888DDDDDDDDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDD44DD
          DDDDDDDDDDDD88DDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDD}
        Layout = blGlyphRight
        NumGlyphs = 2
        OnClick = AddUser
        ShortCut = 0
      end
      object SpeedButton2: TevSpeedButton
        Left = 284
        Top = 328
        Width = 73
        Height = 22
        Caption = 'Remove'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
          DDDDDDDDDD8DDDDDDDDDDDDDD88DDDDDDDDDDDDDD88DDDDDDDDDDDDD8788DDDD
          DDDDDDDD8D8DDDDDDDDDDDD87748888888DDDDD8DDDF888888DDDD8774487777
          78DDDD8DDD8FDDDDD8DDD8774F488888888DD8DDD88FDDDDD8DD8774FF444444
          448D8DDD888FFFFFFFFDD84FFFFFFFFFF48DD8D88888888888FDD4FFFFFFFFFF
          F48DDD888888888888FD4FFFFFFFFFFFF48DD8888888888888FDD4FFFFFFFFFF
          F48DD8888888888888FDDD4FFFFFFFFFF48DDD888888888888FDDDD4FF444444
          44DDDDD88888888888DDDDDD4F4DDDDDDDDDDDDD888DDDDDDDDDDDDDD44DDDDD
          DDDDDDDDD88DDDDDDDDDDDDDDD4DDDDDDDDDDDDDDD8DDDDDDDDD}
        NumGlyphs = 2
        OnClick = RemoveFromUser
        ShortCut = 0
      end
      object Label6: TevLabel
        Left = 352
        Top = 8
        Width = 71
        Height = 13
        Caption = 'Assigned users'
      end
      object Label5: TevLabel
        Left = 8
        Top = 8
        Width = 71
        Height = 13
        Caption = 'Available users'
      end
      object wwDBUserGridAvail: TevDBGrid
        Left = 3
        Top = 28
        Width = 275
        Height = 405
        DisableThemesInTitle = False
        Selected.Strings = (
          'USER_ID'#9'20'#9'User Id'#9'F'
          'StereoType'#9'10'#9'Stereotype'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_SEC_GROUP\wwDBUserGridAvail'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwUsers
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = AddUser
        PaintOptions.AlternatingRowColor = clCream
      end
      object wwDBUserGridSelected: TevDBGrid
        Left = 364
        Top = 27
        Width = 275
        Height = 402
        DisableThemesInTitle = False
        Selected.Strings = (
          'USER_ID'#9'20'#9'User Id'#9'F'
          'StereoType'#9'10'#9'Stereotype'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_SEC_GROUP\wwDBUserGridSelected'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwUsersSel
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = RemoveFromUser
        PaintOptions.AlternatingRowColor = clCream
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    OnDataChange = wwdsDataChange
  end
  inherited wwdsDetail: TevDataSource
    OnDataChange = wwdsDetailDataChange
    MasterDataSource = nil
  end
  object wwUsersSelDS: TevClientDataSet
    Left = 352
    Top = 8
    object wwUsersSelDSSB_USER_NBR: TIntegerField
      FieldName = 'SB_USER_NBR'
    end
    object wwUsersSelDSUSER_ID: TStringField
      FieldName = 'USER_ID'
    end
    object wwUsersSelDSStereoType: TStringField
      FieldName = 'StereoType'
      Size = 10
    end
  end
  object wwUsersDS: TevClientDataSet
    FieldDefs = <
      item
        Name = 'SB_USER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'USER_ID'
        DataType = ftString
        Size = 20
      end>
    Left = 304
    Top = 8
    object wwUsersDSSB_USER_NBR: TIntegerField
      FieldName = 'SB_USER_NBR'
    end
    object wwUsersDSUSER_ID: TStringField
      FieldName = 'USER_ID'
    end
    object wwUsersDSStereoType: TStringField
      FieldName = 'StereoType'
      Size = 10
    end
  end
  object wwUsers: TevDataSource
    DataSet = wwUsersDS
    Left = 400
    Top = 8
  end
  object wwUsersSel: TevDataSource
    DataSet = wwUsersSelDS
    Left = 440
    Top = 16
  end
end
