// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_GROUP;

interface

uses SFrameEntry, Windows, Db,   Dialogs, DBCtrls,
  Buttons, StdCtrls, wwdblook, wwdbdatetimepicker, ExtCtrls, wwdbedit,
  Wwdotdot, Wwdbcomb, Mask, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls, Variants,
  Classes, Wwdatsrc, SDataStructure, SDBDTFrame, Forms, SClientFrame, EvTypes,
  EvContext, EvCommonInterfaces, ISBasicClasses, isVCLBugFix, Menus,
  kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, LMDCustomButton,
  LMDButton, isUILMDButton, isUIwwDBEdit;

type
  TEDIT_SB_SEC_GROUP = class(TFrameEntry)
    PC: TevPageControl;
    tsBrowse: TTabSheet;
    wwDBGrid1: TevDBGrid;
    Label1: TevLabel;
    DBEdit1: TevDBEdit;
    tsClients: TTabSheet;
    tsDBDT: TTabSheet;
    ClientFrame1: TClientFrame;
    DBDTFrame1: TDBDTFrame;
    btnSecRights: TevBitBtn;
    thsUsers: TTabSheet;
    wwDBUserGridAvail: TevDBGrid;
    wwDBUserGridSelected: TevDBGrid;
    SpeedButton1: TevSpeedButton;
    SpeedButton2: TevSpeedButton;
    Label6: TevLabel;
    Label5: TevLabel;
    wwUsersSelDS: TevClientDataSet;
    wwUsersDS: TevClientDataSet;
    wwUsers: TevDataSource;
    wwUsersSel: TevDataSource;
    wwUsersDSSB_USER_NBR: TIntegerField;
    wwUsersSelDSSB_USER_NBR: TIntegerField;
    wwUsersDSUSER_ID: TStringField;
    wwUsersSelDSUSER_ID: TStringField;
    wwUsersDSStereoType: TStringField;
    wwUsersSelDSStereoType: TStringField;
    procedure btnSecRightsClick(Sender: TObject);
    procedure wwdsDataChange(Sender: TObject; Field: TField);
    procedure wwDBGrid1DblClick(Sender: TObject);
    procedure PCChanging(Sender: TObject; var AllowChange: Boolean);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure AddUser(Sender: TObject);
    procedure RemoveFromUser(Sender: TObject);
    procedure tsClientsHide(Sender: TObject);
    procedure tsClientsShow(Sender: TObject);
    procedure tsDBDTHide(Sender: TObject);
    procedure tsDBDTShow(Sender: TObject);
    procedure ClientFrame1gClientsAvailDblClick(Sender: TObject);
    procedure ClientFrame1gClientsSelectedDblClick(Sender: TObject);

  private
    { Private declarations }
    procedure CheckBrowseMode;
    function IsSystemRecord: Boolean;
    procedure LoadUsers;
    procedure CheckBeforeGroupEditing;
  protected
    FAskedAlready: Boolean;
    function GetDataSetConditions(sName: string): string; override;
    procedure AfterNewClientAttached;
    procedure OnClientsChange(Sender: TObject);
  public
    { Public declarations }
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure Activate; override;
    procedure DeActivate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_SEC_GROUP: TEDIT_SB_SEC_GROUP;

implementation

uses SPD_EDIT_SECURITY, EvUtils, sSecurityClassDefs, SysUtils, SPackageEntry, EvConsts;

{$R *.DFM}

procedure TEDIT_SB_SEC_GROUP.Activate;
var
  sCondition, sAccessCond, s : String;
begin
  inherited;
  ClientFrame1.ActiveMode := False;
  DBDTFrame1.ActiveMode := False;
  FAskedAlready := False;
  DM_SERVICE_BUREAU.SB_SEC_GROUPS.DeleteChildren := True;
  wwdsMaster.DataSet := DM_SERVICE_BUREAU.SB_SEC_GROUPS;
  ClientFrame1.dsDetail.DataSet := DM_SERVICE_BUREAU.SB_SEC_CLIENTS;
  ClientFrame1.dsMaster.DataSet := GetDefaultDataSet;
  ClientFrame1.BeforeClientAttach := AfterNewClientAttached;
  ClientFrame1.OnChange := OnClientsChange;
  DBDTFrame1.dsMaster.DataSet := GetDefaultDataSet;
  DBDTFrame1.dsClients.DataSet := DM_SERVICE_BUREAU.SB_SEC_CLIENTS;
  DBDTFrame1.dsDetail.DataSet := DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS;


  sCondition := '';
  if (Context.UserAccount.AccountType = uatRemote) and
     (DM_SERVICE_BUREAU.SB_USER.Locate('SB_USER_NBR', VarArrayOf([Context.UserAccount.InternalNbr]), [])) then
  begin
      s := ExtractFromFiller(DM_SERVICE_BUREAU.SB_USER.FieldByName('USER_FUNCTIONS').AsString, 1, 8);
      if (s = '') or ( s = '-1' ) or ( s = '-2' ) then
        s := '';

      sCondition := '(USER_FUNCTIONS like ''%'+ s + '%'' or USER_FUNCTIONS = ''' + s + ''')';

      case ctx_AccountRights.Level of
      slAdministrator:
        sAccessCond := '';
      slManager:
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_MANAGER)+ ','+
          QuotedStr(USER_SEC_LEVEL_SUPERVISOR)+ ','+
          QuotedStr(USER_SEC_LEVEL_USER)+ ')';
      slSupervisor:
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_SUPERVISOR)+ ','+
          QuotedStr(USER_SEC_LEVEL_USER)+ ')';
      slSBUser:
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_USER)+ ')';
      slEmployee:
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_EMPLOYEE)+ ')';
      else
        sAccessCond := 'SECURITY_LEVEL in ('+
          QuotedStr(USER_SEC_LEVEL_NONE)+ ')';
      end;

      if sAccessCond <> '' then
        if sCondition <> '' then
          sCondition := sCondition+ ' and '+ sAccessCond
        else
          sCondition := sAccessCond;

      if Length(sCondition) > 0 then
      begin
        DM_SERVICE_BUREAU.SB_USER.Filter := sCondition;
        DM_SERVICE_BUREAU.SB_USER.Filtered := True;
        LoadUsers;
      end;
  end;

end;

procedure TEDIT_SB_SEC_GROUP.btnSecRightsClick(Sender: TObject);
var
  o: TEDIT_SECURITY;
  c: IevSecurityStructure;
begin
  if AnsiSameText(wwdsDetail.DataSet.FieldByName('NAME').AsString, 'ACCESS TO NEW CLIENTS') then
     exit;

  inherited;
  CheckBrowseMode;
  o := TEDIT_SECURITY.Create(nil);
  try
    o.Caption := 'Group Security rights ('+ wwdsDetail.DataSet.FieldByName('NAME').AsString + ')';
    ctx_StartWait('Loading security information');
    try
      c := ctx_Security.GetGroupSecStructure(wwdsDetail.DataSet.FieldByName('SB_SEC_GROUPS_NBR').AsInteger);
    finally
      ctx_EndWait;
    end;
    o.Collection := c.Obj;
    o.btnSave.Enabled := not GetIfReadOnly;
    ctx_UpdateWait;
    if o.ShowModal = mrOk then
    begin
      c.Obj := o.Collection;
      ctx_Security.SetGroupSecStructure(wwdsDetail.DataSet.FieldByName('SB_SEC_GROUPS_NBR').AsInteger, c);
    end
    else
      c.Obj := o.Collection;
  finally
    o.Free;
  end;
end;

function TEDIT_SB_SEC_GROUP.GetDataSetConditions(sName: string): string;
begin
  if (sName = GetDefaultDataSet.Name) and (Context.UserAccount.AccountType <> uatSBAdmin) then
    Result := 'exists (select s.SB_USER_NBR from SB_SEC_GROUP_MEMBERS s where s.SB_USER_NBR = '+ IntToStr(Context.UserAccount.InternalNbr)+
      ' and s.SB_SEC_GROUPS_NBR = SB_SEC_GROUPS.SB_SEC_GROUPS_NBR and {AsOfNow<s>})'
  else
    Result := inherited GetDataSetConditions(sName);
end;

function TEDIT_SB_SEC_GROUP.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_SEC_GROUPS;
end;

function TEDIT_SB_SEC_GROUP.GetInsertControl: TWinControl;
begin
  Result := DBEdit1;
end;

procedure TEDIT_SB_SEC_GROUP.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS, EditDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_SEC_CLIENTS, EditDataSets, '');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_SEC_ROW_FILTERS, EditDataSets, '');
  AddDSWithCheck([DM_TEMPORARY.TMP_CL,DM_SERVICE_BUREAU.SB_USER], SupportDataSets, '');
end;

procedure TEDIT_SB_SEC_GROUP.wwdsDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if (wwdsMaster.DataSet['SB_SEC_GROUPS_NBR'] <> null) and (wwdsMaster.DataSet.FieldByName('SB_SEC_GROUPS_NBR').AsInteger < 0) then
  begin
    btnSecRights.Enabled := false;
    tsClients.Enabled := false;
    tsDBDT.Enabled := false;
    thsUsers.Enabled := false;
  end
  else
  begin
    btnSecRights.Enabled := (wwdsMaster.DataSet.State = dsBrowse) and (wwdsMaster.DataSet.RecordCount > 0);
    tsClients.Enabled := btnSecRights.Enabled;
    if IsSystemRecord then
       tsDBDT.Enabled := false
    else
       tsDBDT.Enabled := btnSecRights.Enabled;

    thsUsers.Enabled := btnSecRights.Enabled;
  end;
  if btnSecRights.Enabled then
     btnSecRights.Enabled := not AnsiSameText(wwdsDetail.DataSet.FieldByName('NAME').AsString, 'ACCESS TO NEW CLIENTS');
end;

procedure TEDIT_SB_SEC_GROUP.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;

  if IsSystemRecord then
  begin
    tsDBDT.Enabled := false;
    SetReadOnly(True);
  end
  else
  begin
    tsDBDT.Enabled := btnSecRights.Enabled;
    ApplySecurity;
  end;

  if AnsiSameText(wwdsDetail.DataSet.FieldByName('NAME').AsString, 'ACCESS TO NEW CLIENTS') then
  begin
    DBEdit1.Enabled := false;
    btnSecRights.Enabled := false;
  end
  else
  begin
    DBEdit1.Enabled := true;
    btnSecRights.Enabled := True;
  end;



  LoadUsers;

  DBDTFrame1.SB_SEC_GROUPS_NBR := wwdsDetail.DataSet.FieldByName('SB_SEC_GROUPS_NBR').AsInteger;
  ClientFrame1.SB_SEC_GROUPS_NBR := wwdsDetail.DataSet.FieldByName('SB_SEC_GROUPS_NBR').AsInteger;
end;

procedure TEDIT_SB_SEC_GROUP.wwDBGrid1DblClick(Sender: TObject);
begin
  inherited;

  if (not IsSystemRecord) and
     (not AnsiSameText(wwdsDetail.DataSet.FieldByName('NAME').AsString, 'ACCESS TO NEW CLIENTS')) then
     btnSecRights.Click;
end;

procedure TEDIT_SB_SEC_GROUP.DeActivate;
begin
  inherited;
  DM_SERVICE_BUREAU.SB_SEC_GROUPS.DeleteChildren := False;
end;

procedure TEDIT_SB_SEC_GROUP.AfterNewClientAttached;
begin
  if not FAskedAlready then
    if EvMessage('All users with this group will be able to access this Client.'#13'If remote Clients have access to this group,'#13'then ALL remote users will have access to this Client.'#13#13'Are you sure you want to add this Client?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
      AbortEx
    else
      FAskedAlready := True;
end;

procedure TEDIT_SB_SEC_GROUP.PCChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  CheckBrowseMode;
end;

procedure TEDIT_SB_SEC_GROUP.CheckBrowseMode;
begin
  if (PC.ActivePage = tsBrowse) and (wwdsMaster.State = dsInsert) then
  begin
    wwdsMaster.DataSet.Post;
    ctx_DataAccess.PostDataSets([wwdsMaster.DataSet as TevClientDataSet]);
  end;
end;

procedure TEDIT_SB_SEC_GROUP.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if (Kind = 6) and IsSystemRecord then
    Handled := True;
end;

function TEDIT_SB_SEC_GROUP.IsSystemRecord: Boolean;
begin
  Result := AnsiSameText(wwdsDetail.DataSet.FieldByName('NAME').AsString, sSystemDefaultGroup);
end;

procedure TEDIT_SB_SEC_GROUP.LoadUsers;
var
  cd: TevClientDataset;
begin
  try
    if ctx_AccountRights.Level = slSBUser then
    begin
      DM_SERVICE_BUREAU.SB_USER.Filtered := True;
      DM_SERVICE_BUREAU.SB_USER.Filter := 'SB_USER_NBR = ' + IntToStr(Context.UserAccount.InternalNbr);
    end;

    if wwUsersSelDS.Active and (wwUsersSelDS.ChangeCount > 0) then wwUsersSelDS.CancelUpdates;
    wwUsersSelDS.Close;
    wwUsersSelDS.CreateDataSet;
    with DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS, DM_SERVICE_BUREAU do
      if Active then
      begin
        cd := TevClientDataSet.Create(nil);
        try
          cd.CloneCursor(DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS, True);
          wwUsersDS.DisableControls;
          if wwUsersDS.Active and (wwUsersDS.ChangeCount > 0) then wwUsersDS.CancelUpdates;
          wwUsersDS.Close;
          wwUsersDS.CreateDataSet;
          SB_USER.First;
          while not SB_USER.Eof do
          begin
            if not AnsiSameText(SB_USER['USER_ID'], ctx_DomainInfo.AdminAccount) then
              wwUsersDS.AppendRecord([SB_USER['SB_USER_NBR'], SB_USER['USER_ID'], SB_USER['StereoType']]);
            SB_USER.Next;
          end;
          wwUsersDS.MergeChangeLog;
          SB_USER.First;
          try
            First;
            while not Eof do
            begin
              if FieldValues['SB_SEC_GROUPS_NBR'] = wwdsDetail.DataSet['SB_SEC_GROUPS_NBR'] then
              begin
                if wwUsersDS.Locate('SB_USER_NBR', FieldValues['SB_USER_NBR'], []) then
                begin
                  wwUsersSelDS.AppendRecord([FieldValues['SB_USER_NBR'], wwUsersDS['USER_ID'], wwUsersDS['StereoType']]);
                  wwUsersDS.Delete;
                end;
              end;
              Next;
            end;
          finally
            wwUsersDS.First;
            wwUsersDS.EnableControls;
          end;
        finally
          cd.Free;
        end;
      end;
  finally
    wwUsersSelDS.First;
    wwUsersSelDS.EnableControls;
    wwDBUserGridAvail.UnselectAll;
    wwDBUserGridSelected.UnselectAll;
  end;
end;

procedure TEDIT_SB_SEC_GROUP.AddUser(Sender: TObject);
var
 i : integer;
begin
  inherited;
  if IsSystemRecord then
     exit;

  CheckBeforeGroupEditing;

  for i := 0 to wwDBUserGridAvail.SelectedList.Count - 1 do
  begin
   wwDBUserGridAvail.DataSource.DataSet.GotoBookmark(wwDBUserGridAvail.SelectedList.Items[i]);

   if wwDBUserGridAvail.DataSource.DataSet.Active and not VarIsNull(wwDBUserGridAvail.DataSource.DataSet['SB_USER_NBR']) then
    if not wwUsersSelDS.Locate('SB_USER_NBR', wwDBUserGridAvail.DataSource.DataSet['SB_USER_NBR'], []) then
    begin
      try
        wwUsersSelDS.AppendRecord([wwUsersDS['SB_USER_NBR'], wwUsersDS['USER_ID'],wwUsersDS['StereoType'] ]);
        wwUsersDS.Delete;
        if not DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Locate('SB_SEC_GROUPS_NBR;SB_USER_NBR',
            VarArrayOf([wwdsDetail.DataSet['SB_SEC_GROUPS_NBR'],wwUsersSelDS['SB_USER_NBR']]), []) then
        begin
          DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Append;
          try
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS['SB_SEC_GROUPS_NBR'] := wwdsDetail.DataSet['SB_SEC_GROUPS_NBR'];
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS['SB_USER_NBR'] := wwUsersSelDS['SB_USER_NBR'];
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Post;
          except
            DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Cancel;
          end;
        end;
      finally
      end;
    end;
  end;

end;

procedure TEDIT_SB_SEC_GROUP.RemoveFromUser(Sender: TObject);
var
  i : integer;
begin
  inherited;
  if IsSystemRecord then
     exit;

  CheckBeforeGroupEditing;

  for i := 0 to wwDBUserGridSelected.SelectedList.Count - 1 do
  begin
   wwDBUserGridSelected.DataSource.DataSet.GotoBookmark(wwDBUserGridSelected.SelectedList.Items[i]);
   if not IsSystemRecord then
     if wwDBUserGridSelected.DataSource.DataSet.Active and not VarIsNull(wwDBUserGridSelected.DataSource.DataSet['SB_USER_NBR']) then
      if not wwUsersDS.Locate('SB_USER_NBR', wwDBUserGridSelected.DataSource.DataSet['SB_USER_NBR'], []) then
      begin
        wwUsersDS.AppendRecord([wwUsersSelDS['SB_USER_NBR'], wwUsersSelDS['USER_ID'], wwUsersSelDS['StereoType']]);
        wwUsersSelDS.Delete;
        if DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Locate('SB_SEC_GROUPS_NBR;SB_USER_NBR',
           VarArrayOf([wwdsDetail.DataSet['SB_SEC_GROUPS_NBR'],wwUsersDS['SB_USER_NBR']]), []) then
          DM_SERVICE_BUREAU.SB_SEC_GROUP_MEMBERS.Delete;
      end;
  end;

end;

procedure TEDIT_SB_SEC_GROUP.CheckBeforeGroupEditing;
begin
  if wwdsDetail.DataSet.State = dsInsert then
  try
    wwdsDetail.DataSet.Post;
    btnSecRights.Enabled := True;
    (Owner as TFramePackageTmpl).ClickButton(NavCommit);
  except
    raise EUpdateError.CreateHelp('Fill out all detail page', IDH_ConsistencyViolation);
  end;
end;

procedure TEDIT_SB_SEC_GROUP.tsClientsHide(Sender: TObject);
begin
  ClientFrame1.ActiveMode := False;
end;

procedure TEDIT_SB_SEC_GROUP.tsClientsShow(Sender: TObject);
begin
  ClientFrame1.ActiveMode := True;
end;

procedure TEDIT_SB_SEC_GROUP.tsDBDTHide(Sender: TObject);
begin
  DBDTFrame1.ActiveMode := False;
end;

procedure TEDIT_SB_SEC_GROUP.tsDBDTShow(Sender: TObject);
begin
  DBDTFrame1.ActiveMode := True;
end;

procedure TEDIT_SB_SEC_GROUP.OnClientsChange(Sender: TObject);
begin
  DBDTFrame1.InvalidateData;
end;

procedure TEDIT_SB_SEC_GROUP.ClientFrame1gClientsAvailDblClick(
  Sender: TObject);
begin
  inherited;
  if IsSystemRecord then
     exit;

  ClientFrame1.gClientsAvailDblClick(Sender);

end;

procedure TEDIT_SB_SEC_GROUP.ClientFrame1gClientsSelectedDblClick(
  Sender: TObject);
begin
  inherited;
  if IsSystemRecord then
     exit;

  ClientFrame1.gClientsSelectedDblClick(Sender);

end;

initialization
  RegisterClass(TEDIT_SB_SEC_GROUP);

end.

