inherited SettingsFrame: TSettingsFrame
  Width = 890
  Height = 654
  object pcMain: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 890
    Height = 654
    ActivePage = tsExternal
    Align = alClient
    TabOrder = 0
    object tsEvolution: TTabSheet
      Caption = 'Settings'
      object evBitBtn1: TevBitBtn
        Left = 10
        Top = 531
        Width = 99
        Height = 41
        Caption = 'Send'
        TabOrder = 0
        OnClick = evBitBtn1Click
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
          8888DFFFFFFFFFFFFFFF77FFFFFFFFFFF778888888888888888FFF77FFFFFFF7
          7FF88D88DDDDDDD88D8FFFFF77FFF77FFFF88DDD88DDD88DDD8FFFFFFF767FFF
          FFF88DDDDD888DDDDD8FFFFFF66E66FFFFF88DDDD88D88DDDD8FFFF66EEEEE66
          FFF88DD88DDDDD88DD8FF66EEEE6EEEE66F8888DDDD8DDDD888F60EEEE666EEE
          E06D88DDDD888DDDD88D60EEE66666EEE06DD8DDD88888DDD8DDD0EE6666666E
          E0DDD8DD8888888DD8DDD0EEEE666EEEE0DDD8DDDD888DDDD8DDD0EEEE666EEE
          E0DDD8DDDD888DDDD8DDD0EEEEEEEEEEE0DDD8DDDDDDDDDDD8DDD06666666666
          60DDD8888888888888DDD0000000000000DDD8888888888888DD}
        NumGlyphs = 2
        Margin = 0
      end
      object isUIFashionPanel3: TisUIFashionPanel
        Left = 10
        Top = 10
        Width = 655
        Height = 365
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'isUIFashionPanel3'
        Color = 14737632
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Workstation Printing'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object Label1: TLabel
          Left = 15
          Top = 36
          Width = 71
          Height = 13
          Caption = 'Duplex Printing'
        end
        object evLabel1: TevLabel
          Left = 15
          Top = 84
          Width = 69
          Height = 13
          Caption = 'Checks Printer'
        end
        object evLabel2: TevLabel
          Left = 340
          Top = 84
          Width = 70
          Height = 13
          Caption = 'Reports Printer'
        end
        object evLabel4: TevLabel
          Left = 15
          Top = 139
          Width = 71
          Height = 13
          Caption = 'Printers Offsets'
        end
        object cbDuplexPrinting: TevComboBox
          Left = 15
          Top = 54
          Width = 167
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          Items.Strings = (
            'Only Reports with Duplexing'
            'Always'
            'Never')
        end
        object cbAlwaysDoPreview: TevCheckBox
          Left = 201
          Top = 55
          Width = 174
          Height = 20
          Caption = 'Always do report preview'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object cbAlwaysShowASCIIFileDialog: TevCheckBox
          Left = 394
          Top = 55
          Width = 239
          Height = 20
          Caption = 'Always show ASCII File Save Dialog'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object cbChecksPrinter: TevComboBox
          Left = 15
          Top = 102
          Width = 290
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 3
        end
        object cbReportsPrinter: TevComboBox
          Left = 340
          Top = 102
          Width = 290
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 4
        end
        object evDBGrid1: TevDBGrid
          Left = 15
          Top = 159
          Width = 616
          Height = 134
          DisableThemesInTitle = False
          Selected.Strings = (
            'Printer'#9'60'#9'Printer'#9#9
            'VOffset'#9'10'#9'Vertical Offset'#9#9
            'HOffset'#9'10'#9'Horizontal Offset'#9#9
            'Calibrate'#9'20'#9'Calibrate'#9#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TSettingsFrame\evDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = dscPrinterOffsets
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          ReadOnly = False
          TabOrder = 5
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          UseTFields = True
          PaintOptions.AlternatingRowColor = clCream
          PaintOptions.ActiveRecordColor = clBlack
          NoFire = False
        end
        object btnCalibrate: TevBitBtn
          Left = 15
          Top = 308
          Width = 161
          Height = 25
          Caption = 'Print calibration report'
          TabOrder = 6
          OnClick = btnCalibrateClick
          Color = clBlack
          Margin = 0
        end
      end
      object isUIFashionPanel4: TisUIFashionPanel
        Left = 11
        Top = 384
        Width = 654
        Height = 134
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'isUIFashionPanel4'
        Color = 14737632
        TabOrder = 2
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Miscellaneous Settings'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object cbAutoJump: TevCheckBox
          Left = 15
          Top = 36
          Width = 223
          Height = 17
          Caption = 'Jump To Detail Tab Automatically'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object chDumpParams: TevCheckBox
          Left = 305
          Top = 68
          Width = 223
          Height = 17
          Caption = 'Dump Report Parameters into file'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          Visible = False
        end
        object chbStatatistics: TevCheckBox
          Left = 15
          Top = 68
          Width = 223
          Height = 17
          Caption = 'Collect performance statistics'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object cbShowEffectivePeriods: TevCheckBox
          Left = 305
          Top = 36
          Width = 159
          Height = 17
          Caption = 'Show Dynamic Fields'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object chbNetLatency: TevCheckBox
          Left = 15
          Top = 97
          Width = 254
          Height = 17
          Caption = 'Network transmission latency (100 ms)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
      end
    end
    object tsExternal: TTabSheet
      Caption = 'External'
      ImageIndex = 1
      object isUIFashionPanel1: TisUIFashionPanel
        Left = 10
        Top = 10
        Width = 741
        Height = 360
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'isUIFashionPanel1'
        Color = 14737632
        TabOrder = 0
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'External Links'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object grExternal: TevDBGrid
          Left = 12
          Top = 36
          Width = 709
          Height = 304
          DisableThemesInTitle = False
          ControlType.Strings = (
            'Dashboard;CheckBox;True;False'
            'Default;CheckBox;True;False')
          Selected.Strings = (
            'Nbr'#9'3'#9'#'#9'F'
            'Name'#9'30'#9'Name'#9'F'
            'Dashboard'#9'10'#9'Dashboard'#9'F'
            'Default'#9'8'#9'Default'#9'F'
            'Path'#9'55'#9'Path'#9'F')
          IniAttributes.Enabled = False
          IniAttributes.SaveToRegistry = False
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TSettingsFrame\evDBGrid2'
          IniAttributes.Delimiter = ';;'
          IniAttributes.CheckNewFields = False
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = dscExternal
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = 14544093
          PaintOptions.ActiveRecordColor = clBlack
          DefaultSort = 'Nbr'
          NoFire = False
        end
      end
      object isUIFashionPanel2: TisUIFashionPanel
        Left = 10
        Top = 378
        Width = 741
        Height = 219
        BevelOuter = bvNone
        BorderWidth = 12
        Caption = 'isUIFashionPanel2'
        Color = 14737632
        ParentBackground = True
        TabOrder = 1
        RoundRect = True
        ShadowDepth = 8
        ShadowSpace = 8
        ShowShadow = True
        ShadowColor = clSilver
        TitleColor = clGrayText
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWhite
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = [fsBold]
        Title = 'Details'
        LineWidth = 0
        LineColor = clWhite
        Theme = ttCustom
        object evLabel5: TevLabel
          Left = 15
          Top = 79
          Width = 28
          Height = 13
          Caption = 'Name'
        end
        object evLabel6: TevLabel
          Left = 15
          Top = 113
          Width = 49
          Height = 13
          Caption = 'Path/URL'
        end
        object evLabel3: TevLabel
          Left = 15
          Top = 45
          Width = 36
          Height = 13
          Caption = 'Order #'
        end
        object btnAddLink: TevSpeedButton
          Left = 599
          Top = 41
          Width = 115
          Height = 25
          Caption = 'Create'
          HideHint = True
          AutoSize = False
          OnClick = btnAddLinkClick
          NumGlyphs = 2
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFF5F5F5DADADACCCCCCCCCCCCCCCCCCCCCCCCDADADAF5F5F5FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F6F6DCDBDBCDCCCCCD
            CCCCCDCCCCCDCCCCDCDBDBF7F6F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFDDDDDDA3C0B3369D6E008C4B008B4A008B4A008C4B369D6EA3C0B3E1E1
            E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDEDEDEBDBCBC9191917D7C7C7C
            7B7B7C7B7B7D7C7C919191BDBCBCE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            E1E1E144A27700905001A16900AA7600AB7700AB7700AA7601A16900905055A8
            82E1E1E1FFFFFFFFFFFFFFFFFFFFFFFFE2E2E29796968180809393939C9C9C9D
            9D9D9D9D9D9C9C9C9393938180809E9E9EE2E2E2FFFFFFFFFFFFFFFFFFF5F5F5
            55A88200915202AC7700C38C00D69918DEA818DEA800D69900C38C01AB760092
            5355A882F5F5F5FFFFFFFFFFFFF7F6F69E9E9E8282829E9E9EB4B4B4C5C5C5CE
            CECECECECEC5C5C5B4B4B49D9D9D8383839E9E9EF7F6F6FFFFFFFFFFFFAECBBE
            0090510FB48302D29900D69B00D193FFFFFFFFFFFF00D19300D69B00D19801AB
            76009050AECBBEFFFFFFFFFFFFC8C8C7828181A6A6A6C2C2C1C5C5C5C0C0C0FF
            FFFFFFFFFFC0C0C0C5C5C5C1C1C19D9D9D818080C8C8C7FFFFFFFFFFFF369D6C
            16AB7811C99700D49A00D29700CD8EFFFFFFFFFFFF00CD8E00D29700D59B00C1
            8C01A169369E6EFFFFFFFFFFFF9090909D9D9DBABABAC4C3C3C2C2C1BCBCBBFF
            FFFFFFFFFFBCBCBBC2C2C1C4C4C4B2B2B2939393929292FFFFFFFFFFFF008A48
            38C49C00D19800CD9200CB8E00C787FFFFFFFFFFFF00C78700CB8E00CE9300D0
            9A00AB76008C4BFFFFFFFFFFFF7B7B7BB8B7B7C1C1C1BDBCBCBABABAB6B6B5FF
            FFFFFFFFFFB6B6B5BABABABEBDBDC0C0C09D9D9D7D7C7CFFFFFFFFFFFF008946
            51D2AF12D4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CF
            9700AD78008B4AFFFFFFFFFFFF7A7979C7C7C7C5C5C5FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBF9F9F9E7C7B7BFFFFFFFFFFFF008845
            66DDBE10D0A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00CD
            9700AD78008B4AFFFFFFFFFFFF797979D3D2D2C2C2C1FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEBDBD9F9F9E7C7B7BFFFFFFFFFFFF008846
            76E0C500CA9800C59000C48E00C187FFFFFFFFFFFF00C18700C48E00C79300CB
            9900AB76008C4BFFFFFFFFFFFF797979D7D6D6BBBBBBB6B6B5B5B5B5B2B1B1FF
            FFFFFFFFFFB2B1B1B5B5B5B8B8B8BDBCBC9D9D9D7D7C7CFFFFFFFFFFFF41A675
            59C9A449DEBC00C79400C79400C38EFFFFFFFFFFFF00C38E00C89600CB9A06C1
            9000A16840A878FFFFFFFFFFFF999999BEBDBDD3D2D2B8B8B8B8B8B8B4B4B4FF
            FFFFFFFFFFB4B4B4B9B9B9BDBCBCB2B2B29393939B9B9BFFFFFFFFFFFFCCE8DB
            0A9458ADF8E918D0A700C49400C290FFFFFFFFFFFF00C39100C79905C89B18B7
            87009050CCE9DCFFFFFFFFFFFFE5E5E5868585F3F2F2C3C2C2B6B6B5B3B3B3FF
            FFFFFFFFFFB5B5B5B9B9B9BABABAAAA9A9818080E6E6E6FFFFFFFFFFFFFFFFFF
            55B185199C63BCFFF75DE4C900C39700BF9000C09100C49822CAA231C2970393
            556ABD96FFFFFFFFFFFFFFFFFFFFFFFFA5A5A58E8E8EFBFBFBDADADAB6B6B5B2
            B1B1B2B2B2B7B6B6BEBDBDB5B5B5858484B2B2B2FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF6ABB940E965974D5B69FF3E092EFDA79E5CA5DD6B52EB58603915255B3
            88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B0B0878787CBCBCBECECECE8
            E7E7DCDBDBCBCBCBA8A8A7828282A7A7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFCCE8DB44A97700874400874300874400894644AA7ACCE9DCFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E59C9C9C78787778
            78777878777A79799D9D9DE6E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          Flat = True
          ParentColor = False
          ShortCut = 0
        end
        object btnDeleteLink: TevSpeedButton
          Left = 598
          Top = 75
          Width = 115
          Height = 25
          Caption = 'Delete'
          HideHint = True
          AutoSize = False
          OnClick = btnDeleteLinkClick
          NumGlyphs = 2
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000120B0000120B00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFE1E1E1CECECECCCCCCCCCCCCCCCCCCCECECEE1E1E1FFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E2CFCFCFCDCCCCCD
            CCCCCDCCCCCFCFCFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            F1F1F1CCCCCC7079C7313FC02B3BBE2B3ABE2B3BBE313FC07079C7CCCCCCF1F1
            F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F2F2CDCCCC8F8F8F6D6D6D6B6B6B6B
            6B6B6B6B6B6D6D6D8F8F8FCDCCCCF3F2F2FFFFFFFFFFFFFFFFFFFFFFFFF1F1F1
            A1A5CA2B3BBE4A5BE26175FC697DFF697CFF697DFF6175FC4A5BE22B3BBEA1A5
            CAF1F1F1FFFFFFFFFFFFFFFFFFF3F2F2AFAFAF6B6B6B898989A1A0A0A6A6A6A6
            A6A6A6A6A6A1A0A08989896B6B6BAFAFAFF3F2F2FFFFFFFFFFFFFFFFFFA1A5CA
            2F3FC2596DF66276FF6074FE5F73FE5F73FD5F73FE6074FE6276FF596DF62F3F
            C2A1A5CAFFFFFFFFFFFFFFFFFFAFAFAF6E6E6E9A9A9AA2A2A2A1A1A1A1A0A0A0
            9F9FA1A0A0A1A1A1A2A2A29A9A9A6E6E6EAFAFAFFFFFFFFFFFFFE1E1E12C3CBF
            5669F45D71FC5B6FFA5A6EF95A6EF95A6EF95A6EF95A6EF95B6FFA5D71FC5669
            F42C3CBFE1E1E1FFFFFFE2E2E26C6C6C9797979F9F9E9D9D9D9C9C9C9C9C9C9C
            9C9C9C9C9C9C9C9C9D9D9D9F9F9E9797976C6C6CE2E2E2FFFFFF717AC74256DE
            576DFB5369F85268F75267F75267F75267F75267F75267F75268F75369F8576D
            FB4256DE717AC7FFFFFF9090908685859C9C9C99999998989897979797979797
            97979797979797979898989999999C9C9C868585909090FFFFFF3241C04E64F4
            4C63F7425AF43E56F43D55F43D55F43D55F43D55F43D55F43E56F4425AF44C63
            F74E64F43241C0FFFFFF6E6E6E9595949695959090908F8F8F8E8E8E8E8E8E8E
            8E8E8E8E8E8E8E8E8F8F8F9090909695959595946E6E6EFFFFFF2C3CBF5369F8
            3E56F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E56
            F35369F82C3CBFFFFFFF6C6C6C9999998E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8E9999996C6C6CFFFFFF2B3BBF6378F7
            334DF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF334D
            F06378F72B3BBFFFFFFF6B6B6BA1A0A0898989FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF898989A1A0A06B6B6BFFFFFF2A39BF8696F8
            2F4BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F4B
            EE8696F82A39BFFFFFFF6B6B6BB2B2B2878787FFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF878787B2B2B26B6B6BFFFFFF2F3EC1A1ACF4
            3852ED2D48EC2B46EB2A46EB2A46EB2A46EB2A46EB2A46EB2B46EB2D48EC3852
            EDA1ACF42F3EC1FFFFFF6D6D6DBFBFBF8A8A8A86858585848485848485848485
            84848584848584848584848685858A8A8ABFBFBF6D6D6DFFFFFF838DDB6F7CDD
            8494F52E4AE9334DE9354FEA3650EA3650EA3650EA354FEA334DE92E4AE98494
            F56F7CDD838DDBFFFFFFA3A3A3999999B0AFAF85848486868687878787878787
            8787878787878787868686858484B0AFAF999999A3A3A3FFFFFFFFFFFF2737BF
            9AA7F07F90F3324CE92D49E7304CE8314CE8304CE82D49E7324CE97F90F39AA7
            F02737BFFFFFFFFFFFFFFFFFFF6A6A6ABBBBBBADADAD86858583838386858586
            8585868585838383868585ADADADBBBBBB6A6A6AFFFFFFFFFFFFFFFFFFC5CAEF
            2F3FC397A3EF9EACF76075ED3E57E92441E53E57E96075ED9EACF797A3EF2F3F
            C3C5CAEFFFFFFFFFFFFFFFFFFFD4D4D46F6F6FB8B7B7C0C0C09B9B9B8A8A8A80
            807F8A8A8A9B9B9BC0C0C0B8B7B76F6F6FD4D4D4FFFFFFFFFFFFFFFFFFFFFFFF
            C5CAEF2737BF6A77DC9EA9F2AFBAF8AFBBF8AFBAF89EA9F26A77DC2737BFC5CA
            EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46A6A6A969595BDBCBCCACACACB
            CBCBCACACABDBCBC9695956A6A6AD4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF838DDB2E3EC22737BF2737BF2737BF2E3EC2838DDBFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A36E6E6E6A6A6A6A
            6A6A6A6A6A6E6E6EA3A3A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          Flat = True
          ParentColor = False
          ShortCut = 0
        end
        object sbBrowse: TevSpeedButton
          Left = 545
          Top = 109
          Width = 21
          Height = 21
          HideHint = True
          AutoSize = False
          OnClick = sbBrowseClick
          NumGlyphs = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD7F7D7F7D7F
            7DDDDDDFDDDFDDDFDDDDDD80FD80FD80FDDDDD88FD88FD88FDDDDD087D087D08
            7DDDDDD8DDD8DDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          ParentColor = False
          ShortCut = 0
        end
        object evLabel7: TevLabel
          Left = 392
          Top = 161
          Width = 283
          Height = 13
          Caption = 'Double click the picture box to retrieve it from the Path/URL'
        end
        object edName: TevDBEdit
          Left = 87
          Top = 75
          Width = 226
          Height = 21
          DataField = 'Name'
          DataSource = dscExternal
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          OnChange = edNameChange
          OnExit = edNameExit
          OnKeyPress = edPathKeyPress
          Glowing = False
        end
        object edPath: TevDBEdit
          Left = 87
          Top = 109
          Width = 457
          Height = 21
          DataField = 'Path'
          DataSource = dscExternal
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          UsePictureMask = False
          WantReturns = False
          WordWrap = False
          OnChange = edNameChange
          OnExit = edPathExit
          OnKeyPress = edPathKeyPress
          Glowing = False
        end
        object chbDashboard: TevDBCheckBox
          Left = 87
          Top = 178
          Width = 125
          Height = 17
          Caption = 'Show in Dashboard'
          DataField = 'Dashboard'
          DataSource = dscExternal
          TabOrder = 4
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          OnChange = chbDashboardChange
        end
        object chbDefault: TevDBCheckBox
          Left = 87
          Top = 143
          Width = 173
          Height = 17
          Caption = 'Default page in Web Browser'
          DataField = 'Default'
          DataSource = dscExternal
          TabOrder = 3
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          OnChange = chbDashboardChange
        end
        object edNbr: TevDBEdit
          Left = 86
          Top = 41
          Width = 35
          Height = 21
          DataField = 'Nbr'
          DataSource = dscExternal
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          OnChange = edNameChange
          OnExit = edNameExit
          OnKeyPress = edPathKeyPress
          Glowing = False
        end
        object pnlPict: TevPanel
          Left = 335
          Top = 145
          Width = 48
          Height = 48
          BevelOuter = bvLowered
          ParentBackground = False
          TabOrder = 5
          OnDblClick = imgLinkPictDblClick
          object imgLinkPict: TevImage
            Left = 0
            Top = 0
            Width = 46
            Height = 46
            Hint = 'Double click to update the picture'
            ParentShowHint = False
            ShowHint = True
            OnDblClick = imgLinkPictDblClick
          end
        end
      end
    end
    object tsStatistics: TTabSheet
      Caption = 'Performance Statistics'
      ImageIndex = 2
      OnShow = tsStatisticsShow
      inline frmStatistics: TevStatisticsViewerFrm
        Left = 0
        Top = 0
        Width = 882
        Height = 626
        Align = alClient
        AutoScroll = False
        TabOrder = 0
        inherited Splitter1: TSplitter
          Top = 434
          Width = 882
        end
        inherited Splitter2: TSplitter
          Left = 551
          Height = 374
        end
        inherited Label3: TLabel
          Width = 882
        end
        inherited pnlFileList: TPanel
          Width = 882
        end
        inherited tvEvents: TTreeView
          Width = 551
          Height = 374
        end
        inherited Panel2: TPanel
          Top = 441
          Width = 882
          inherited Label2: TLabel
            Width = 882
          end
          inherited mEventInfo: TMemo
            Width = 882
          end
        end
        inherited pnlChart: TPanel
          Left = 560
          Height = 374
          inherited Splitter3: TSplitter
            Top = 247
          end
          inherited Chart: TChart
            Height = 247
          end
          inherited lvSummary: TListView
            Top = 254
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 642
    Top = 65534
  end
  inherited wwdsDetail: TevDataSource
    Left = 699
    Top = 65534
  end
  inherited wwdsList: TevDataSource
    Left = 591
    Top = 65534
  end
  object oDialog: TOpenDialog
    Filter = 'All files|*.*'
    Options = [ofEnableSizing]
    Left = 320
    Top = 201
  end
  object evImageList1: TevImageList
    Left = 304
    Top = 160
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000800000008000000080
      0000008000000080000000800000008000000080000000800000008000000080
      0000008000000080000000800000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000800000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000800000000000000000000080808000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000800000C0C0C000C0C0
      C000C0C0C00000FFFF000080800000FFFF000080800000FFFF000080800000FF
      FF0000808000C0C0C00000800000000000000000000080808000C0C0C000C0C0
      C000C0C0C0000080800080808000008080008080800000808000808080000080
      800080808000C0C0C00080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000800000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000800000000000000000000080808000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000800000008000000080
      0000008000000080000000800000008000000080000000800000008000000080
      0000008000000080000000800000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000800000008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000800000008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000800000008000FF0000000000000000008000000080000000
      FF000000FF0000000000000000000000000000000000000000000000000000FF
      000000FF000000FF000000FF0000800000008080800000FF000000FF000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF0000000000000000000000000000000000000000000000000000FF
      000000FF000000000000000000000000000000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      800000008000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      80000000800000000000000000000000000000000000000000000000000000FF
      000000FF0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FF000000FF000000FF000000FF0000FFFFFF000000
      0000000000000000000000000000000000008080800080808000808080008080
      800080808000FFFFFF0000800000008000000080000000800000FFFFFF008080
      8000808080008080800080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF000000FF000000FF000000FF00
      0000FF000000FFFFFF0000FF000000FF000000FF000000FF0000FFFFFF00FF00
      0000FF000000FF000000FF000000FF0000008000000080000000800000008000
      000080000000FFFFFF0000800000008000000080000000800000FFFFFF008000
      0000800000008000000080000000800000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF00FFFFFF0000800000008000000080000000800000FFFFFF000000
      FF000000FF0000000000000000000000000000000000000000000000000000FF
      000000FF0000FFFFFF0000800000008000000080000000800000FFFFFF0000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF0000000000000000000000000000000000000000000000000000FF
      000000FF0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF0000000000000000000000000000000000000000000000000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0080018001000000008001800100000000
      8001800100000000800180010000000080018001000000008001800100000000
      FE7FFE7F00000000FE7FFE7F00000000E007E00700000000E007E00700000000
      E007E0070000000000000000000000000000000000000000E007E00700000000
      E007E00700000000E007E0070000000000000000000000000000000000000000
      000000000000}
  end
  object dsPrinterOffsets: TevClientDataSet
    BeforeInsert = dsPrinterOffsetsBeforeInsert
    AfterPost = dsPrinterOffsetsAfterPost
    BeforeDelete = dsPrinterOffsetsBeforeDelete
    Left = 446
    Top = 162
    object dsPrinterOffsetsPrinter: TStringField
      DisplayWidth = 60
      FieldName = 'Printer'
      Size = 255
    end
    object dsPrinterOffsetsVOffset: TIntegerField
      DisplayLabel = 'Vertical Offset'
      DisplayWidth = 10
      FieldName = 'VOffset'
    end
    object dsPrinterOffsetsHOffset: TIntegerField
      DisplayLabel = 'Horizontal Offset'
      DisplayWidth = 10
      FieldName = 'HOffset'
    end
    object dsPrinterOffsetsStorageName: TStringField
      FieldName = 'StorageName'
      Size = 32
    end
  end
  object dscPrinterOffsets: TevDataSource
    DataSet = dsPrinterOffsets
    Left = 496
    Top = 138
  end
  object dsExternal: TevClientDataSet
    FieldDefs = <
      item
        Name = 'Nbr'
        DataType = ftInteger
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'Path'
        DataType = ftString
        Size = 1024
      end
      item
        Name = 'Picture'
        DataType = ftBlob
      end
      item
        Name = 'Dashboard'
        DataType = ftBoolean
      end
      item
        Name = 'Default'
        DataType = ftBoolean
      end>
    BeforePost = dsExternalBeforePost
    AfterPost = dsExternalAfterPost
    OnNewRecord = dsExternalNewRecord
    Left = 168
    Top = 295
    object dsExternalNbr: TIntegerField
      FieldName = 'Nbr'
    end
    object dsExternalName: TStringField
      DisplayWidth = 30
      FieldName = 'Name'
      Size = 80
    end
    object dsExternalPath: TStringField
      FieldName = 'Path'
      Size = 1024
    end
    object dsExternalPicture: TBlobField
      FieldName = 'Picture'
    end
    object dsExternalDashboard: TBooleanField
      FieldName = 'Dashboard'
    end
    object dsExternalDefault: TBooleanField
      FieldName = 'Default'
    end
  end
  object dscExternal: TevDataSource
    DataSet = dsExternal
    OnDataChange = dscExternalDataChange
    Left = 209
    Top = 291
  end
end
