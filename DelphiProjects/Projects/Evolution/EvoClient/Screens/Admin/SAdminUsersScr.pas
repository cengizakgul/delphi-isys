// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SAdminUsersScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList,
  graphics, SysUtils, ISBasicClasses, EvCommonInterfaces,
  EvMainboard, isBaseClasses, EvContext, EvConsts,
  EvUIComponents, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, EvUIUtils, isUtils, evStreamUtils, EvInitApp,
  isBasicUtils;

type
  TAdminUsersFrm = class(TFramePackageTmpl, IevAdminUsersScreens)
  private
    FExtLinksPict: TList;
    procedure ClearExtLinksPict;
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;
  public
    destructor Destroy; override;
  end;

implementation

{$R *.DFM}

var
  AdminUsersFrm: TAdminUsersFrm;


function GetAdminUsersScreens: IevAdminUsersScreens;
begin
  if not Assigned(AdminUsersFrm) then
    AdminUsersFrm := TAdminUsersFrm.Create(nil);
  Result := AdminUsersFrm;
end;


{ TAdminUsersFrm }

procedure TAdminUsersFrm.ClearExtLinksPict;
var
  i: Integer;
begin
  if Assigned(FExtLinksPict) then
  begin
    for i := 0 to FExtLinksPict.Count - 1 do
      TObject(FExtLinksPict[i]).Free;

    FreeAndNil(FExtLinksPict);
  end;     
end;

destructor TAdminUsersFrm.Destroy;
begin
  ClearExtLinksPict;
  inherited;
end;

function TAdminUsersFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TAdminUsersFrm.PackageCaption: string;
begin
  Result := 'A&dmin';
end;

function TAdminUsersFrm.PackageSortPosition: string;
begin
  Result := '006';
end;

procedure TAdminUsersFrm.UserPackageStructure;
var
  i: Integer;
  Links: IisParamsCollection;
  Bitmap: TBitmap;
  PictData: IisStream;
begin
  AddStructure('Security|100');
  AddStructure('Security\Groups|1000', 'TEDIT_SB_SEC_GROUP'{, false, GetBitmap(2)});
  AddStructure('Security\Users|1010', 'TEDIT_SB_USER'{, false, GetBitmap(1)});
  AddStructure('System Information|400', 'TInformationFrame');
  AddStructure('Workstation Settings|500', 'TSettingsFrame');
  AddStructure('System Locks|50', 'TLocksFrame');
  AddStructure('\&Misc\Web Browser|900', 'TEVWEBBROWSERFRM');

  ClearExtLinksPict;
  if not (IsStandalone and AppSwitches.ValueExists('GETSEC')) then
  begin
    FExtLinksPict := TList.Create;
    Links := GetExternalLinks;
    for i := 0 to Links.Count - 1 do
    begin
      Bitmap := nil;
      PictData := IInterface(Links[i].Value['Icon']) as IisStream;
      if Assigned(PictData) and (PictData.Size > 0) then
      begin
        PictData := IconToBitmap(PictData);
        if Assigned(PictData) and (PictData.Size > 0) then
        begin
          Bitmap := TBitmap.Create;
          PictData.Position := 0;
          Bitmap.LoadFromStream(PictData.RealStream);
          FExtLinksPict.Add(Bitmap);
        end;
      end;

      AddStructure('\&Misc\' + Links[i].Value['Name'] + '|' + Format('%3.3d', [901 + i]), Links[i].Value['Path'], False, Bitmap, True);
    end;
  end;  
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetAdminUsersScreens, IevAdminUsersScreens, 'Screens Admin Users');

finalization
  AdminUsersFrm.Free;

end.
