// Copyright � 2000-2008 iSystems LLC. All rights reserved.
unit SPD_Locks;

interface

uses
  Windows, Messages, SFrameEntry, Classes, Db, Wwdatsrc, 
  Controls, Forms, StdCtrls, Spin, EvConsts,
  Printers, SysUtils, EvUtils, EvSendMail, CheckLst, isBasicUtils,
  SPackageEntry, SDataStructure, wwdbdatetimepicker, EvContext,
  ExtCtrls, ComCtrls, EvBasicUtils, Grids, ValEdit, Dialogs, EvTypes,
  ISBasicClasses, Buttons, ImgList, isBaseClasses, EvMainboard, Wwdbigrd,
  Wwdbgrid, Graphics, EvCommonInterfaces, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvUIComponents, EvClientDataSet;

const
  MY_MSG = WM_USER;

type
  TLocksFrame = class(TFrameEntry)
    pcMain: TevPageControl;
    tsEvolution: TTabSheet;
    FlagsGrid: TevDBGrid;
    evPanel1: TevPanel;
    RefreshBtn: TevBitBtn;
    FlagsDataSource: TevDataSource;
    UnlockBtn: TevBitBtn;
    ImageList1: TImageList;
    FlagsDataSet: TevClientDataSet;
    procedure vleExternalSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure RefreshBtnClick(Sender: TObject);
    procedure UnlockBtnClick(Sender: TObject);
  private
    procedure GetFlags;
    procedure Refresh(var Message: TMessage); message MY_MSG;
  public
    procedure Activate; override;
    procedure Deactivate; override;
    function CanClose: Boolean; override;
  end;

implementation

uses SSecurityInterface;

{$R *.DFM}

{ TLocksFrame }

procedure TLocksFrame.Activate;
begin
  FlagsDataSet.FieldDefs.Add('FLAGTYPE', ftString, 40);
  FlagsDataSet.FieldDefs.Add('DESCRIPTION', ftString, 40);
  FlagsDataSet.FieldDefs.Add('LOCKTIME', ftDateTime);
  FlagsDataSet.FieldDefs.Add('USERNAME', ftString, 20);
  FlagsDataSet.FieldDefs.Add('TAG', ftString, 20);
  FlagsDataSet.FieldDefs.Add('FLAGTYPE1', ftString, 80);
  
  FlagsDataSet.CreateDataSet;

  FlagsGrid.Selected.Text :=

          'FLAGTYPE'#9'20'#9'Flag Type'#9'F' + #13#10 +
          'DESCRIPTION'#9'18'#9'Description'#9'F' + #13#10 +
          'LOCKTIME'#9'25'#9'Lock Time'#9'F' + #13#10 +
          'USERNAME'#9'20'#9'User Name'#9'F' + #13#10 +
          'TAG'#9'20'#9'Tag'#9'F';

  FlagsDataSet.Open;
  GetFlags;
end;

procedure TLocksFrame.GetFlags;
var
  FM: IevGlobalFlagsManager;
  FL: IisList;
  FI: IevGlobalFlagInfo;
  i: Integer;

  procedure GetFlagTypeString(const FlagType: String);
  var
    s, s1, c: String;
  begin
    if copy(FlagType, 1, Length(GF_CO_PAYROLL_OPERATION)) = GF_CO_PAYROLL_OPERATION then
    begin
      FlagsDataSet.FieldByName('FLAGTYPE').AsString := 'Payroll processing';
      s := FI.Tag;
      c := copy(s, 1, pos('_', s) - 1);
      s := copy(s, pos('_', s) + 1, 1000);
      FlagsDataSet.FieldByName('DESCRIPTION').AsString := 'Client #'+ c + '/Company #' + s;
    end
    else if copy(FlagType, 1, Length(GF_CL_PAYROLL_OPERATION)) = GF_CL_PAYROLL_OPERATION then
    begin
      FlagsDataSet.FieldByName('FLAGTYPE').AsString := 'Payroll processing';
      FlagsDataSet.FieldByName('DESCRIPTION').AsString := 'Client #'+ FI.Tag;
    end
    else if copy(FlagType, 1, Length(GF_SB_ACH_PROCESS)) = GF_SB_ACH_PROCESS then
    begin
      FlagsDataSet.FieldByName('FLAGTYPE').AsString := 'ACH file processing';
      FlagsDataSet.FieldByName('DESCRIPTION').AsString := '';
    end
    else
    if FlagType = 'ReturnPrintQueue' then
    begin
      FlagsDataSet.FieldByName('FLAGTYPE').AsString := 'Tax Return Print Queue';
      s1 := FI.Tag;
      if pos(';', s1) > 0 then
      begin
        s := 'CL_NBR = #' + copy(s1, 1, pos(';', s1)-1);
        s1 := copy(s1, pos(';', s1)+1, 10000);
        if pos(';', s1) > 0 then
          s := s + ';CO_NBR = #' + copy(s1, pos(';', s1)+1, 100);
      end;
      FlagsDataSet.FieldByName('DESCRIPTION').AsString := s;
    end
    else
    if FlagType = GF_SB_PAYROLL_OPERATION then
    begin
      FlagsDataSet.FieldByName('FLAGTYPE').AsString := 'Assigning check numbers';
      FlagsDataSet.FieldByName('DESCRIPTION').AsString := '';
    end
    else
    begin
      FlagsDataSet.FieldByName('FLAGTYPE').AsString := FlagType;
      FlagsDataSet.FieldByName('DESCRIPTION').AsString := FI.Tag;
    end;
  end;

begin
  FlagsDataSet.DisableControls;
  try
    FlagsDataSet.EmptyDataSet;
    FM := mb_GlobalFlagsManager;
    FL := FM.GetLockedFlagList;
    for i := 0 to FL.Count - 1 do
    begin
      FI := FL[i] as IevGlobalFlagInfo;
      FlagsDataSet.Insert;
      FlagsDataSet.FieldByName('TAG').AsString := FI.Tag;
      FlagsDataSet.FieldByName('FLAGTYPE1').AsString := FI.FlagType;
      FlagsDataSet.FieldByName('LOCKTIME').AsDateTime := FI.LockTime;
      GetFlagTypeString(FI.FlagType);
      FlagsDataSet.FieldByName('USERNAME').AsString := FI.UserName;
      FlagsDataSet.Post;
    end;

    FlagsDataSource.DataSet := FlagsDataSet;
  finally
    FlagsDataSet.EnableControls;
    FlagsGrid.Refresh;
  end;
end;

procedure TLocksFrame.Deactivate;
begin
end;

procedure TLocksFrame.Refresh(var Message: TMessage);
begin
//
end;

procedure TLocksFrame.vleExternalSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
//
end;

function TLocksFrame.CanClose: Boolean;
begin
  Result := True;
end;

procedure TLocksFrame.RefreshBtnClick(Sender: TObject);
begin
  inherited;
  GetFlags;
end;

procedure TLocksFrame.UnlockBtnClick(Sender: TObject);
var
  FM: IevGlobalFlagsManager;
  FL: IisList;
  FI: IevGlobalFlagInfo;
  i, j: Integer;
  mes: String;
begin
  inherited;
  if FlagsDataSet.IsEmpty then
    Exit;

  if FlagsGrid.SelectedList.Count > 1 then
    mes := Format('Do you want to kill %d locks?',[FlagsGrid.SelectedList.Count])
  else
    mes := 'Do you want to kill this lock?';

  if MessageBox(Handle, PChar(mes), 'Unlock', MB_YESNO + MB_ICONWARNING + MB_DEFBUTTON2) = mrYes then
  begin
    FM := mb_GlobalFlagsManager;
    FL := FM.GetLockedFlagList;
    if FlagsGrid.SelectedList.Count = 0 then
    begin
      for i := 0 to FL.Count - 1 do
      begin
        FI := FL[i] as IevGlobalFlagInfo;
        if (FlagsDataSet.FieldByName('TAG').AsString = FI.Tag)
        and (FlagsDataSet.FieldByName('FLAGTYPE1').AsString = FI.FlagType) then
          FM.ForcedUnlock(FI.FlagType, FI.Tag);
      end;
      FlagsDataSet.Delete;
    end
    else
    begin
      for j := 0 to FlagsGrid.SelectedList.Count - 1 do
      begin
        FlagsDataSet.GotoBookmark(FlagsGrid.SelectedList[j]);
        for i := 0 to FL.Count - 1 do
        begin
          FI := FL[i] as IevGlobalFlagInfo;
          if (FlagsDataSet.FieldByName('TAG').AsString = FI.Tag)
          and (FlagsDataSet.FieldByName('FLAGTYPE1').AsString = FI.FlagType) then
            FM.ForcedUnlock(FI.FlagType, FI.Tag);
        end;
        FlagsDataSet.Delete;
      end;
    end;
  end;
//  GetFlags;
end;

initialization
  RegisterCLass(TLocksFrame);

end.
