// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_ProcessCombinedLiabilitiesAndDeposits;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, ExtCtrls, EvBasicUtils,
  SPD_FRAME_4_IP_GRIDS, Grids, Wwdbigrd, Wwdbgrid, Db, Wwdatsrc,
  Mask, wwdbedit, Wwdotdot, Wwdbcomb, wwdblook, SFrameEntry, EvDataAccessComponents,
   Variants, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvContext, EvUIComponents, EvClientDataSet;

type
  TProcessCombinedLiabilitiesAndDeposits = class(TFrameEntry)
    PageControl1: TevPageControl;
    tsLiability: TTabSheet;
    tsDeposit: TTabSheet;
    pnlLiabilityControls: TevPanel;
    ComboBox2: TevComboBox;
    ComboBox3: TevComboBox;
    pnlDepositControls: TevPanel;
    Frame_4_DB_GRIDS1: TFrame_4_DB_GRIDS;
    Frame_4_DB_GRIDS2: TFrame_4_DB_GRIDS;
    CreateManualRecords: TTabSheet;
    tsLiabilityDetail: TTabSheet;
    tsDepositDetail: TTabSheet;
    tsCreateManualLiabilities: TTabSheet;
    dtpkrFrom: TevDateTimePicker;
    dtpkrTo: TevDateTimePicker;
    BitBtn10: TevBitBtn;
    ManualNotebook: TNotebook;
    BitBtn6: TevBitBtn;
    BitBtn8: TevBitBtn;
    DateTimePicker1: TevDateTimePicker;
    Bevel1: TEvBevel;
    lblCompanyNameAndNumber: TevLabel;
    lblClientNameAndNumber: TevLabel;
    Label7: TevLabel;
    Memo1: TevMemo;
    tsCreateTaxDepositRecord: TTabSheet;
    BitBtn9: TevBitBtn;
    Bevel2: TEvBevel;
    Bevel3: TEvBevel;
    Label8: TevLabel;
    DateTimePicker2: TevDateTimePicker;
    ManualNotebook2: TNotebook;
    wwDBGrid3: TevDBGrid;
    BitBtn5: TevBitBtn;
    BitBtn15: TevBitBtn;
    wwDBGrid4: TevDBGrid;
    wwDBGrid5: TevDBGrid;
    BitBtn16: TevBitBtn;
    BitBtn17: TevBitBtn;
    BitBtn18: TevBitBtn;
    BitBtn19: TevBitBtn;
    BitBtn13: TevBitBtn;
    Label9: TevLabel;
    Label10: TevLabel;
    Label11: TevLabel;
    cbCompanyNumber: TevComboBox;
    BitBtn14: TevBitBtn;
    BitBtn20: TevBitBtn;
    BitBtn21: TevBitBtn;
    Bevel4: TEvBevel;
    cbDateFrom: TevCheckBox;
    Bevel5: TEvBevel;
    Bevel6: TEvBevel;
    cdLiabTypes: TevClientDataSet;
    cdLiabTypesTaxName: TStringField;
    cdLiabTypesTaxAmount: TCurrencyField;
    cdLiabTypesTaxTable: TIntegerField;
    cdLiabTypesTaxType: TStringField;
    dsLiabTypes: TevDataSource;
    wwDBGrid2: TevDBGrid;
    StaticText1: TStaticText;
    cdLiabTypesTaxNbr: TIntegerField;
    cdAgencies: TevClientDataSet;
    cdAgenciesName: TStringField;
    cdAgenciesAgencyNbr: TIntegerField;
    dsAgencies: TevDataSource;
    Bevel7: TEvBevel;
    lblCompanyNameAndNumber2: TevLabel;
    lblClientNameAndNumber2: TevLabel;
    Label1: TevLabel;
    cbDepositType: TevComboBox;
    cdLiabTypesTaxStatus: TStringField;
    wwDBComboBox1: TevDBComboBox;
    Label2: TevLabel;
    lTot940: TevLabel;
    Label12: TevLabel;
    lTot941: TevLabel;
    Label3: TevLabel;
    Label4: TevLabel;
    cdLiabSelected: TevClientDataSet;
    cdLiabAvail: TevClientDataSet;
    dsLiabAvail: TevDataSource;
    dsLiabSelected: TevDataSource;
    cdLiabAvailDUE_DATE: TDateField;
    cdLiabAvailAmount: TCurrencyField;
    cdLiabAvailName: TStringField;
    cdLiabAvailTaxTable: TIntegerField;
    cdLiabAvailNbr: TIntegerField;
    cdLiabSelectedDUE_DATE: TDateField;
    cdLiabSelectedName: TStringField;
    cdLiabSelectedAmount: TCurrencyField;
    cdLiabSelectedTaxTable: TIntegerField;
    cdLiabSelectedNbr: TIntegerField;
    sbtnMoveThemAll: TevBitBtn;
    Memo2: TevMemo;
    Label5: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    cdLiabTypesTaxAdjType: TStringField;
    cdLiabTypesTaxThirdParty: TStringField;
    wwDBComboBox2: TevDBComboBox;
    BitBtn1: TevBitBtn;
    crdepPaymDate: TevDateTimePicker;
    Label6: TevLabel;
    crdepStatusDate: TevDateTimePicker;
    Label13: TevLabel;
    Label15: TevLabel;
    crdepRef: TevEdit;
    cdLiabAvailCheck_date: TDateTimeField;
    cdLiabAvailStatus: TStringField;
    cdLiabSelectedCheck_date: TDateTimeField;
    cdLiabSelectedStatus: TStringField;
    Label14: TevLabel;
    cdLiabTypesTaxCollected: TStringField;
    evBitBtn1: TevBitBtn;
    cbAutoAttachDeposit: TevCheckBox;
    cdLiabTypesAgencyNbr: TIntegerField;
    btnOpenCompany: TevBitBtn;
    btnCloseCompany: TevBitBtn;
    btnAddLiab: TevBitBtn;
    btnAddDepos: TevBitBtn;
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn14xClick(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure cdLiabTypesBeforeScroll(DataSet: TDataSet);
    procedure cdLiabTypesAfterScroll(DataSet: TDataSet);
    procedure cdLiabTypesAfterPost(DataSet: TDataSet);
    procedure BitBtn17Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure sbtnMoveThemAllClick(Sender: TObject);
    procedure wwDBGrid4DblClick(Sender: TObject);
    procedure wwDBGrid5DblClick(Sender: TObject);
    procedure wwDBGrid2Enter(Sender: TObject);
    procedure wwDBGrid2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure crdepStatusDateChange(Sender: TObject);
  private
    { Private declarations }
    FDataAdded: boolean;
    iCL_NBR, iCO_NBR: Integer;
    FComboValue: string;
    FOldStatus: Variant;
    FOldAdjType: Variant;
    FOldThirdParty: Variant;
    FOldCollected: Variant;
    function GetDataReadIn: boolean;
    procedure CalcDepositTotals;
    {procedure Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick(Sender: TObject;
      AFieldName: String);
    procedure Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes(
      Sender: TObject; AFieldName: String; AFont: TFont; ABrush: TBrush;
      var ATitleAlignment: TAlignment);}
  public
    { Public declarations }
    procedure UpdateScreenStatus;
    procedure ChangeLiabilityDataSets;
    procedure ClearView;
    property DataReadIn: boolean read GetDataReadIn;
    property DataAdded: boolean read FDataAdded;
    function IsLiabilityCanBeDeleted: boolean;
    function IsLiabilityCanBeEdited: boolean;
    function IsLiabilityCanBeUnpaid: boolean;
    function IsLiabilityCanBePaid: boolean;
    procedure Activate; override;
    procedure Deactivate; override;
  end;

var
  ProcessCombinedLiabilitiesAndDeposits: TProcessCombinedLiabilitiesAndDeposits;

implementation

uses
  SPD_PCLD_DM, EvConsts, SFieldCodeValues, EvUtils, EvTypes, SDataStructure, SMiscCheckProcedures;

{$R *.DFM}

function TProcessCombinedLiabilitiesAndDeposits.IsLiabilityCanBeDeleted: boolean;
begin
  result := True;
end;

function TProcessCombinedLiabilitiesAndDeposits.IsLiabilityCanBeEdited: boolean;
begin
  result := True;
end;

function TProcessCombinedLiabilitiesAndDeposits.IsLiabilityCanBeUnpaid: boolean;
begin
  result := True;
end;

function TProcessCombinedLiabilitiesAndDeposits.IsLiabilityCanBePaid: boolean;
begin
  result := True;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.UpdateScreenStatus;
var
  TempActivePage: TTabSheet;
begin
  dtpkrFrom.Enabled := not DataReadIn;
  dtpkrTo.Enabled := dtpkrFrom.Enabled;
  cbDateFrom.Enabled := dtpkrFrom.Enabled;
  btnOpenCompany.Enabled := dtpkrFrom.Enabled;
  cbCompanyNumber.Enabled := dtpkrFrom.Enabled;

  btnCloseCompany.Enabled := DataReadIn or DataAdded;
  ComboBox2.Enabled := btnCloseCompany.Enabled;
  ComboBox3.Enabled := btnCloseCompany.Enabled;
  btnAddDepos.Enabled := btnCloseCompany.Enabled and (iCO_NBR <> 0);
  btnAddLiab.Enabled := btnAddDepos.Enabled;
  Frame_4_DB_GRIDS1.Visible := DataReadIn or DataAdded;
  Frame_4_DB_GRIDS2.Visible := DataReadIn or DataAdded;
  with PageControl1 do
  begin
    TempActivePage := ActivePage;
    if (ActivePage = tsLiability) or (ActivePage = tsLiabilityDetail) then
    begin
      tsLiability.TabVisible := True;
      //tsLiabilityDetail.TabVisible := True ;
      tsDeposit.TabVisible := True;
      tsDepositDetail.TabVisible := False;
      tsCreateManualLiabilities.TabVisible := False;
      tsCreateTaxDepositRecord.TabVisible := False;
    end;
    if (ActivePage = tsDeposit) or (ActivePage = tsDepositDetail) then
    begin
      tsLiability.TabVisible := True;
      tsLiabilityDetail.TabVisible := False;
      tsDeposit.TabVisible := True;
      //tsDepositDetail.TabVisible := True ;
      tsCreateManualLiabilities.TabVisible := False;
      tsCreateTaxDepositRecord.TabVisible := False;
    end;
    if (ActivePage = tsCreateManualLiabilities) then
    begin
      tsLiability.TabVisible := False;
      tsLiabilityDetail.TabVisible := False;
      tsDeposit.TabVisible := False;
      tsDepositDetail.TabVisible := False;
      tsCreateManualLiabilities.TabVisible := True;
      tsCreateTaxDepositRecord.TabVisible := False;
    end;
    if (ActivePage = tsCreateTaxDepositRecord) then
    begin
      tsLiability.TabVisible := False;
      tsLiabilityDetail.TabVisible := False;
      tsDeposit.TabVisible := False;
      tsDepositDetail.TabVisible := False;
      tsCreateManualLiabilities.TabVisible := False;
      tsCreateTaxDepositRecord.TabVisible := True;
    end;
    ActivePage := TempActivePage;
  end;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.Activate;
begin
  inherited;
  if not assigned(CombineLiabilityDM) then
    CombineLiabilityDM := TCombineLiabilityDM.Create(self);
  PageControl1.ActivePage := tsLiability;
  CombineLiabilityDM.PopulateCompanyNumbers(cbCompanyNumber.Items);
  cbCompanyNumber.ItemIndex := 0;
  ClearView;
  UpdateScreenStatus;
  Frame_4_DB_GRIDS1.wwDataSource1.DataSet := CombineLiabilityDM.wwcsFEIN;
  Frame_4_DB_GRIDS1.wwDataSource2.DataSet := CombineLiabilityDM.wwcsDeposit;
  Frame_4_DB_GRIDS1.wwDataSource3.DataSet := CombineLiabilityDM.wwcsDepositSubTot1;
  Frame_4_DB_GRIDS1.wwDataSource4.DataSet := CombineLiabilityDM.wwcsDepositSubTot2;
  Frame_4_DB_GRIDS2.wwDataSource1.DataSet := CombineLiabilityDM.wwcsFEIN;
  ChangeLiabilityDataSets;
  dtpkrTo.DateTime := GetEndQuarter(Date);
  dtpkrFrom.DateTime := GetBeginQuarter(Date);
  {with Frame_4_DB_GRIDS2 do
  begin
    wwDBGrid1.OnTitleButtonClick := Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick;
    wwDBGrid1.OnCalcTitleAttributes := Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes;
    wwDBGrid2.OnTitleButtonClick := Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick;
    wwDBGrid2.OnCalcTitleAttributes := Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes;
    wwDBGrid3.OnTitleButtonClick := Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick;
    wwDBGrid3.OnCalcTitleAttributes := Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes;
    wwDBGrid4.OnTitleButtonClick := Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick;
    wwDBGrid4.OnCalcTitleAttributes := Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes;
  end;
  with Frame_4_DB_GRIDS1 do
  begin
    wwDBGrid1.OnTitleButtonClick := Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick;
    wwDBGrid1.OnCalcTitleAttributes := Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes;
    wwDBGrid2.OnTitleButtonClick := Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick;
    wwDBGrid2.OnCalcTitleAttributes := Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes;
    wwDBGrid3.OnTitleButtonClick := Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick;
    wwDBGrid3.OnCalcTitleAttributes := Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes;
    wwDBGrid4.OnTitleButtonClick := Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick;
    wwDBGrid4.OnCalcTitleAttributes := Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes;
  end;}
end;

function TProcessCombinedLiabilitiesAndDeposits.GetDataReadIn: boolean;
begin
  result := CombineLiabilityDM.DataReadIn;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn12Click(
  Sender: TObject);
var
  F, T: string;
begin
  inherited;
  if cbDateFrom.Checked then
    F := FormatDateTime('mm/dd/yyyy', dtpkrFrom.Date)
  else
    F := '';
  T := FormatDateTime('mm/dd/yyyy', dtpkrTo.Date);
  CombineLiabilityDM.ConvertCompanyNumber(cbCompanyNumber.Text, iCL_NBR, iCO_NBR);
  ctx_StartWait;
  try
    CombineLiabilityDM.ReadRecords(F, T, iCL_NBR, iCO_NBR);
  finally
    ctx_EndWait;
  end;
  UpdateScreenStatus;
end;

function GetToken(const s: string; var p: Integer; out token: string): Boolean;
var
  i, j: Integer;
begin
  i := Length(s);
  j := p;
  while (j <= i) and not (s[j] in [#9, #13]) do
    Inc(j);
  token := Copy(s, p, j - p);
  p := j + 1;
  Result := p > i;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn1Click(
  Sender: TObject);

procedure Prepare;
  var
    i, AgencyNbr: Integer;
    s1, s2: string;
  begin
    inherited;
    cdLiabTypes.Tag := 1;
    ctx_StartWait;
    try
      DM_TEMPORARY.TMP_CO.DataRequired('ALL');
      DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([iCL_NBR, iCO_NBR]), []);
      DM_TEMPORARY.TMP_CL.Locate('CL_NBR', iCL_NBR, []);
      lblCompanyNameAndNumber.Caption := 'Client ' + DM_TEMPORARY.TMP_CL['custom_client_number'] + ' ' + DM_TEMPORARY.TMP_CL['name'];
      lblClientNameAndNumber.Caption := 'Company ' + DM_TEMPORARY.TMP_CO['custom_company_number'] + ' ' + DM_TEMPORARY.TMP_CO['name'];
      cdLiabTypes.Close;
      cdLiabTypes.CreateDataSet;
      ctx_DataAccess.OpenClient(iCL_NBR);
      DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(iCO_NBR));
      DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + IntToStr(iCO_NBR));
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + IntToStr(iCO_NBR));
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + IntToStr(iCO_NBR));
      DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
      DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
      DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');
      DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.DataRequired('ALL');
      DM_PAYROLL.PR.DataRequired('CO_NBR=' + IntToStr(iCO_NBR));
      wwDBLookupCombo1.LookupTable := DM_PAYROLL.PR;
      wwDBLookupCombo1.LookupField := 'PR_NBR';
      wwDBComboBox1.Items.Clear;
      wwDBComboBox1.Items.Append(#9);
      i := 1;
      while not GetToken(TaxDepositStatus_ComboChoices, i, s1) do
      begin
        GetToken(TaxDepositStatus_ComboChoices, i, s2);
        wwDBComboBox1.Items.Append(s1 + #9 + s2);
      end;
      wwDBComboBox2.Items.Clear;
      wwDBComboBox2.Items.Append(#9);
      i := 1;
      while not GetToken(TaxLiabilityAdjustmentType_ComboChoices, i, s1) do
      begin
        GetToken(TaxLiabilityAdjustmentType_ComboChoices, i, s2);
        wwDBComboBox2.Items.Append(s1 + #9 + s2);
      end;
      i := 1;
      while not GetToken(FedTaxLiabilityType_ComboChoices, i, s1) do
      begin
        GetToken(FedTaxLiabilityType_ComboChoices, i, s2);
        if s2 <> TAX_LIABILITY_TYPE_ER_FUI then
          AgencyNbr := DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR',
            DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'], 'SY_GLOBAL_AGENCY_NBR')
        else
          AgencyNbr := DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR',
            DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'], 'SY_GLOBAL_AGENCY_NBR');
        cdLiabTypes.AppendRecord([s1, Null, Null, Null, 'N', Ord(ttFed), s2, Null, 'N', AgencyNbr]);
      end;
      with DM_COMPANY.CO_STATES, DM_SYSTEM_STATE do
      begin
        First;
        while not Eof do
        begin
          Assert(SY_STATES.Locate('STATE', FieldValues['STATE'], []));
          i := 1;
          while not GetToken(StateTaxLiabilityType_ComboChoices, i, s1) do
          begin
            GetToken(StateTaxLiabilityType_ComboChoices, i, s2);
            if ((FieldValues['STATE'] = 'NY') and (s2 = TAX_LIABILITY_TYPE_STATE))
            or ((FieldValues['STATE'] <> 'NY') and
              ((s2 = TAX_LIABILITY_TYPE_STATE)
              or ((s2 = TAX_LIABILITY_TYPE_EE_SDI) and not VarIsNull(SY_STATES['EE_SDI_RATE']))
              or ((s2 = TAX_LIABILITY_TYPE_ER_SDI) and not VarIsNull(SY_STATES['ER_SDI_RATE'])))) then
              cdLiabTypes.AppendRecord([FieldValues['STATE'] + '-' + s1, Null, Null, Null, 'N', Ord(ttState), s2,
                FieldValues['CO_STATES_NBR'], 'N', SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR']]);
          end;
          Next;
        end;
      end;
      with DM_COMPANY.CO_SUI, DM_SYSTEM_STATE do
      begin
        First;
        while not Eof do
        begin
          Assert(SY_SUI.Locate('SY_SUI_NBR', FieldValues['SY_SUI_NBR'], []));
          cdLiabTypes.AppendRecord([FieldValues['DESCRIPTION'], Null, Null, Null, 'N', Ord(ttSUI), Null, FieldValues['CO_SUI_NBR'], 'N', SY_SUI['SY_SUI_TAX_PMT_AGENCY_NBR']]);
          Next;
        end;
      end;
      with DM_COMPANY.CO_LOCAL_TAX, DM_SYSTEM_LOCAL, DM_SYSTEM_STATE do
      begin
        First;
        while not Eof do
        begin
          Assert(SY_LOCALS.Locate('SY_LOCALS_NBR', FieldValues['SY_LOCALS_NBR'], []));
          Assert(SY_STATES.Locate('SY_STATES_NBR', SY_LOCALS['SY_STATES_NBR'], []));
          cdLiabTypes.AppendRecord([SY_STATES['STATE']+ '-'+ SY_LOCALS['NAME'], Null, Null, Null, 'N',
            Ord(ttLoc), Null, FieldValues['CO_LOCAL_TAX_NBR'], 'N', SY_LOCALS['SY_LOCAL_TAX_PMT_AGENCY_NBR']]);
          Next;
        end;
      end;
      cdLiabTypes.First;
      cdLiabTypes.Edit;
      cdLiabTypes['taxstatus'] := TAX_DEPOSIT_STATUS_PENDING;
      cdLiabTypes.Post;
    finally
      ctx_EndWait;
      cdLiabTypes.Tag := 0;
    end;
  end;
begin
  inherited;
  DateTimePicker1.Date := Date;
  DateTimePicker2.Date := Date;
  Prepare;
  lTot940.Caption := '0';
  lTot941.Caption := '0';
  PageControl1.ActivePage := tsCreateManualLiabilities;
  ManualNotebook.PageIndex := 1;
  DateTimePicker1.SetFocus;
  UpdateScreenStatus;
  DateTimePicker1.SetFocus;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.ClearView;
begin
  CombineLiabilityDM.Clear;
  FDataAdded := False;
  UpdateScreenStatus;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn11Click(
  Sender: TObject);
begin
  inherited;
  ClearView;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn6Click(
  Sender: TObject);

function GetNull(const s: string): Variant;
  begin
    if s = '' then
      Result := Null
    else
      Result := s;
  end;
var
  d: TDateTime;
  vPR_NBR: Variant;
  CheckDate: TDateTime;
  cdDeposits: TEvBasicClientDataSet;
  DepositNbr: Variant;
begin
  inherited;
  cdDeposits := TEvBasicClientDataSet.Create(nil);
  try
    cdDeposits.FieldDefs.Add('AgencyNbr', ftInteger);
    cdDeposits.FieldDefs.Add('DepositNbr', ftInteger);
    cdDeposits.CreateDataSet;
    cdDeposits.AddIndex('1', 'AgencyNbr', [ixUnique]);
    cdDeposits.IndexName := '1';
    cdDeposits.LogChanges := False;
    with DM_COMPANY do
    try
      CO_FED_TAX_LIABILITIES.DataRequired('CO_NBR=-1');
      CO_STATE_TAX_LIABILITIES.DataRequired('CO_NBR=-1');
      CO_SUI_LIABILITIES.DataRequired('CO_NBR=-1');
      CO_LOCAL_TAX_LIABILITIES.DataRequired('CO_NBR=-1');
      CO_TAX_DEPOSITS.DataRequired('CO_NBR=-1');
      if wwDBLookupCombo1.LookupValue = '' then
      begin
        vPR_NBR := Null;
        CheckDate := Trunc(DateTimePicker1.DateTime);
      end
      else
      begin
        vPR_NBR := StrToInt(wwDBLookupCombo1.LookupValue);
        Assert(DM_PAYROLL.PR.Locate('PR_NBR', vPR_NBR, []));
        CheckDate := DM_PAYROLL.PR['CHECK_DATE'];
      end;
      cdLiabTypes.DisableControls;
      try
        cdLiabTypes.First;
        while not cdLiabTypes.Eof do
        begin
          if not (VarIsNull(cdLiabTypes['TaxAmount']) or (cdLiabTypes['TaxAmount'] = 0)) then
          begin
            if cbAutoAttachDeposit.Checked and not VarIsNull(cdLiabTypes['TaxAgencyNbr'])
            and ((cdLiabTypes['TaxStatus'] = TAX_DEPOSIT_STATUS_PAID) or (cdLiabTypes['TaxStatus'] = TAX_DEPOSIT_STATUS_PAID_SEND_NOTICE) or (cdLiabTypes['TaxStatus'] = TAX_DEPOSIT_STATUS_PAID_WO_FUNDS))
            then
            begin
              if cdDeposits.FindKey([cdLiabTypes['TaxAgencyNbr']]) then
                DepositNbr := cdDeposits['DepositNbr']
              else
              begin
                CO_TAX_DEPOSITS.Append;
                CO_TAX_DEPOSITS.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString := 'Manual ' + DateToStr(Now);
                CO_TAX_DEPOSITS.FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PAID;
                //CO_TAX_DEPOSITS.FieldByName('STATUS_DATE').AsDateTime := crdepStatusDate.DateTime;
                CO_TAX_DEPOSITS.FieldByName('PR_NBR').Value := Null;
                CO_TAX_DEPOSITS.FieldByName('DEPOSIT_TYPE').AsString := TAX_DEPOSIT_TYPE_NOTICE;
                CO_TAX_DEPOSITS.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').AsInteger := GetGlobalAgencyFieldOffice(cdLiabTypes['TaxAgencyNbr']);
                CO_TAX_DEPOSITS.Post;
                cdDeposits.AppendRecord([cdLiabTypes['TaxAgencyNbr'], CO_TAX_DEPOSITS['CO_TAX_DEPOSITS_NBR']]);
                DepositNbr := CO_TAX_DEPOSITS['CO_TAX_DEPOSITS_NBR'];
              end;
            end
            else
              DepositNbr := Null;
            case cdLiabTypes['TaxTable'] of
              Ord(ttFed):
                with CO_FED_TAX_LIABILITIES do
                begin
                  Append;
                  FieldValues['CO_NBR'] := iCO_NBR;
                  FieldValues['AMOUNT'] := cdLiabTypes['TaxAmount'];
                  d := Trunc(DateTimePicker2.DateTime);
                  FieldValues['DUE_DATE'] := d;
                  FieldValues['CHECK_DATE'] := CheckDate;
                  FieldValues['STATUS'] := cdLiabTypes['TaxStatus'];
                  FieldValues['TAX_TYPE'] := cdLiabTypes['TaxType'];
                  FieldValues['THIRD_PARTY'] := cdLiabTypes['TaxThirdParty'];
                  FieldValues['IMPOUNDED'] := cdLiabTypes['TaxCollected'];
                  FieldValues['PR_NBR'] := vPR_NBR;
                  FieldValues['ADJUSTMENT_TYPE'] := GetNull(ConvertNull(cdLiabTypes['TaxAdjType'], ''));
                  FieldValues['CO_TAX_DEPOSITS_NBR'] := DepositNbr;
                  Post;
                end;
              Ord(ttState):
                with CO_STATE_TAX_LIABILITIES do
                begin
                  Append;
                  FieldValues['CO_NBR'] := iCO_NBR;
                  FieldValues['CO_STATES_NBR'] := cdLiabTypes['TaxNbr'];
                  FieldValues['AMOUNT'] := cdLiabTypes['TaxAmount'];
                  d := Trunc(DateTimePicker2.DateTime);
                  FieldValues['DUE_DATE'] := d;
                  d := Trunc(DateTimePicker1.DateTime);
                  FieldValues['CHECK_DATE'] := d;
                  FieldValues['STATUS'] := cdLiabTypes['TaxStatus'];
                  FieldValues['TAX_TYPE'] := cdLiabTypes['TaxType'];
                  FieldValues['THIRD_PARTY'] := cdLiabTypes['TaxThirdParty'];
                  FieldValues['IMPOUNDED'] := cdLiabTypes['TaxCollected'];
                  FieldValues['PR_NBR'] := vPR_NBR;
                  FieldValues['ADJUSTMENT_TYPE'] := GetNull(ConvertNull(cdLiabTypes['TaxAdjType'], ''));
                  FieldValues['CO_TAX_DEPOSITS_NBR'] := DepositNbr;
                  Post;
                end;
              Ord(ttSUI):
                with CO_SUI_LIABILITIES do
                begin
                  Append;
                  FieldValues['CO_NBR'] := iCO_NBR;
                  FieldValues['CO_SUI_NBR'] := cdLiabTypes['TaxNbr'];
                  FieldValues['AMOUNT'] := cdLiabTypes['TaxAmount'];
                  d := Trunc(DateTimePicker2.DateTime);
                  FieldValues['DUE_DATE'] := d;
                  d := Trunc(DateTimePicker1.DateTime);
                  FieldValues['CHECK_DATE'] := d;
                  FieldValues['STATUS'] := cdLiabTypes['TaxStatus'];
                  FieldValues['THIRD_PARTY'] := cdLiabTypes['TaxThirdParty'];
                  FieldValues['IMPOUNDED'] := cdLiabTypes['TaxCollected'];
                  FieldValues['PR_NBR'] := vPR_NBR;
                  FieldValues['ADJUSTMENT_TYPE'] := GetNull(ConvertNull(cdLiabTypes['TaxAdjType'], ''));
                  FieldValues['CO_TAX_DEPOSITS_NBR'] := DepositNbr;
                  Post;
                end;
              Ord(ttLoc):
                with CO_LOCAL_TAX_LIABILITIES do
                begin
                  Append;
                  FieldValues['CO_NBR'] := iCO_NBR;
                  FieldValues['CO_LOCAL_TAX_NBR'] := cdLiabTypes['TaxNbr'];
                  FieldValues['AMOUNT'] := cdLiabTypes['TaxAmount'];
                  d := Trunc(DateTimePicker2.DateTime);
                  FieldValues['DUE_DATE'] := d;
                  d := Trunc(DateTimePicker1.DateTime);
                  FieldValues['CHECK_DATE'] := d;
                  FieldValues['STATUS'] := cdLiabTypes['TaxStatus'];
                  FieldValues['THIRD_PARTY'] := cdLiabTypes['TaxThirdParty'];
                  FieldValues['IMPOUNDED'] := cdLiabTypes['TaxCollected'];
                  FieldValues['PR_NBR'] := vPR_NBR;
                  FieldValues['ADJUSTMENT_TYPE'] := GetNull(ConvertNull(cdLiabTypes['TaxAdjType'], ''));
                  FieldValues['CO_TAX_DEPOSITS_NBR'] := DepositNbr;
                  Post;
                end;
            end;
          end;
          cdLiabTypes.Next;
        end;
      finally
        cdLiabTypes.EnableControls;
      end;
      ctx_DataAccess.PostDataSets([CO_TAX_DEPOSITS,
        CO_FED_TAX_LIABILITIES,
          CO_STATE_TAX_LIABILITIES,
          CO_SUI_LIABILITIES,
          CO_LOCAL_TAX_LIABILITIES]);
    except
      ctx_DataAccess.CancelDataSets([CO_TAX_DEPOSITS,
        CO_FED_TAX_LIABILITIES,
          CO_STATE_TAX_LIABILITIES,
          CO_SUI_LIABILITIES,
          CO_LOCAL_TAX_LIABILITIES]);
      raise;
    end;
  finally
    cdDeposits.Free;
  end;
  if Sender = BitBtn1 then
  begin
    cdLiabTypes.DisableControls;
    cdLiabTypes.Tag := 1;
    try
      cdLiabTypes.First;
      while not cdLiabTypes.Eof do
      begin
        cdLiabTypes.Edit;
        cdLiabTypes['TaxAmount'] := Null;
        cdLiabTypes['TaxStatus'] := TAX_DEPOSIT_STATUS_PENDING;
        cdLiabTypes['TaxAdjType'] := Null;
        cdLiabTypes['TaxThirdParty'] := 'N';
        cdLiabTypes['TaxCollected'] := 'N';
        cdLiabTypes.Post;
        cdLiabTypes.Next;
      end;
      cdLiabTypes.First;
    finally
      cdLiabTypes.EnableControls;
      cdLiabTypes.Tag := 0;
    end;
    lTot940.Caption := '0';
    lTot941.Caption := '0';
    PageControl1.ActivePage := tsCreateManualLiabilities;
    ManualNotebook.PageIndex := 1;
    DateTimePicker1.SetFocus;
    UpdateScreenStatus;
  end
  else
  begin
    FDataAdded := False;
    PageControl1.ActivePage := tsLiability;
    //UpdateScreenStatus ;
    btnCloseCompany.Click;
    btnOpenCompany.Click;
  end;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.PageControl1Change(
  Sender: TObject);
begin
  inherited;
  UpdateScreenStatus;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.ChangeLiabilityDataSets;
begin
  with Frame_4_DB_GRIDS2 do
  begin
    if ComboBox2.Text = 'Check date' then
    begin
      wwDataSource2.DataSet := CombineLiabilityDM.wwcsCheckDateTotal;
      wwDataSource3.DataSet := CombineLiabilityDM.wwcsCheckDateSubTot1;
      wwDataSource4.DataSet := CombineLiabilityDM.wwcsCheckDateSubTot2;
    end
    else
    begin
      wwDataSource2.DataSet := CombineLiabilityDM.wwcsTaxTypeTotal;
      wwDataSource3.DataSet := CombineLiabilityDM.wwcsTaxTypeSubTot1;
      wwDataSource4.DataSet := CombineLiabilityDM.wwcsTaxTypeSubTot2;
    end;
  end;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.ComboBox2Change(
  Sender: TObject);
begin
  inherited;
  ChangeLiabilityDataSets;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.Deactivate;
begin
  CombineLiabilityDM.Destroy;
  CombineLiabilityDM := nil;
  inherited;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn9Click(
  Sender: TObject);
begin
  inherited;
  ManualNotebook2.PageIndex := 0;
  PageControl1.ActivePage := tsCreateTaxDepositRecord;
  UpdateScreenStatus;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn14xClick(
  Sender: TObject);

procedure Prepare;
    procedure AddAgency(const Nbr: Variant);
    begin
      with DM_SYSTEM_MISC do
      begin
        if SY_GLOBAL_AGENCY.Locate('SY_GLOBAL_AGENCY_NBR', Nbr, [])
          and not cdAgencies.Locate('AgencyNbr', Nbr, []) then
          cdAgencies.AppendRecord([SY_GLOBAL_AGENCY['AGENCY_NAME'], Nbr]);
      end;
    end;
  var
    i: Integer;
    s1, s2: string;
  begin
    inherited;
    ctx_StartWait;
    try
      DM_TEMPORARY.TMP_CO.DataRequired('ALL');
      DM_TEMPORARY.TMP_CO.Locate('CL_NBR;CO_NBR', VarArrayOf([iCL_NBR, iCO_NBR]), []);
      DM_TEMPORARY.TMP_CL.Locate('CL_NBR', iCL_NBR, []);
      lblCompanyNameAndNumber2.Caption := 'Client ' + DM_TEMPORARY.TMP_CL['custom_client_number'] + ' ' + DM_TEMPORARY.TMP_CL['name'];
      lblClientNameAndNumber2.Caption := 'Company ' + DM_TEMPORARY.TMP_CO['custom_company_number'] + ' ' + DM_TEMPORARY.TMP_CO['name'];
      cdAgencies.Close;
      cdAgencies.CreateDataSet;
      ctx_DataAccess.OpenClient(iCL_NBR);
      DM_COMPANY.CO.DataRequired('CO_NBR=' + IntToStr(iCO_NBR));
      DM_COMPANY.CO_SUI.DataRequired('CO_NBR=' + IntToStr(iCO_NBR));
      DM_COMPANY.CO_LOCAL_TAX.DataRequired('CO_NBR=' + IntToStr(iCO_NBR));
      DM_COMPANY.CO_STATES.DataRequired('CO_NBR=' + IntToStr(iCO_NBR));
      DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');
      DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
      DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.DataRequired('ALL');
      DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.DataRequired('ALL');
      i := 1;
      FComboValue := '';
      cbDepositType.Items.Clear;
      while not GetToken(TaxDepositType_ComboChoices, i, s1) do
      begin
        GetToken(TaxDepositType_ComboChoices, i, s2);
        cbDepositType.Items.Append(s1);
        FComboValue := FComboValue + s2;
      end;
      cbDepositType.ItemIndex := 0;
      AddAgency(DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR',
        DM_COMPANY.CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'], 'SY_GLOBAL_AGENCY_NBR'));
      AddAgency(DM_SYSTEM_FEDERAL.SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR',
        DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY'], 'SY_GLOBAL_AGENCY_NBR'));
      //AddAgency(DM_COMPANY.CO['FUI_SY_TAX_PAYMENT_AGENCY']);
      with DM_COMPANY.CO_STATES, DM_SYSTEM_STATE do
      begin
        First;
        while not Eof do
        begin
          Assert(SY_STATES.Locate('STATE', FieldValues['STATE'], []));
          AddAgency(SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR']);
          Next;
        end;
      end;
      with DM_COMPANY.CO_SUI, DM_SYSTEM_STATE do
      begin
        First;
        while not Eof do
        begin
          Assert(SY_SUI.Locate('SY_SUI_NBR', FieldValues['SY_SUI_NBR'], []));
          AddAgency(SY_SUI['SY_SUI_TAX_PMT_AGENCY_NBR']);
          Next;
        end;
      end;
      with DM_COMPANY.CO_LOCAL_TAX, DM_SYSTEM_LOCAL do
      begin
        First;
        while not Eof do
        begin
          Assert(SY_LOCALS.Locate('SY_LOCALS_NBR', FieldValues['SY_LOCALS_NBR'], []));
          AddAgency(SY_LOCALS['SY_LOCAL_TAX_PMT_AGENCY_NBR']);
          Next;
        end;
      end;
      cdAgencies.First;
    finally
      ctx_EndWait;
    end;
  end;
begin
  inherited;
  Prepare;
  crdepPaymDate.DateTime := Date;
  crdepStatusDate.DateTime := Now;
  PageControl1.ActivePage := tsCreateTaxDepositRecord;
  ManualNotebook2.PageIndex := 0;
  wwDBGrid3.SetFocus;
  UpdateScreenStatus;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn5Click(
  Sender: TObject);
begin
  inherited;
  PageControl1.ActivePage := tsLiability;
  UpdateScreenStatus;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn15Click(
  Sender: TObject);

function GetDescr(const CodeValues, Code: string): string;
  var
    s1, s2: string;
    i: Integer;
  begin
    Result := 'N/A';
    i := 1;
    while not GetToken(CodeValues, i, s1) do
    begin
      GetToken(CodeValues, i, s2);
      if s2 = Code then
      begin
        Result := s1;
        Break;
      end;
    end;
  end;
var
  AgencyNbr: Variant;
  s: string;
begin
  inherited;
  if VarIsNull(cdAgencies['AgencyNbr']) then
  begin // close the page, go back to normal mode
    BitBtn5.Click; // cancel btn
    Exit;
  end;
  cdLiabAvail.Close;
  cdLiabAvail.CreateDataSet;
  cdLiabSelected.Close;
  cdLiabSelected.CreateDataSet;
  CalcDepositTotals; // update total caption on screen
  ctx_StartWait;
  try
    AgencyNbr := cdAgencies['AgencyNbr'];
    with DM_COMPANY, DM_SYSTEM_LOCAL, DM_SYSTEM_STATE, DM_SYSTEM_FEDERAL do
    begin
      if cbDateFrom.Checked then
        s := 'CO_TAX_DEPOSITS_NBR is null and CO_NBR =' + IntToStr(iCO_NBR) + ' and check_date between ''' + FormatDateTime('mm/dd/yyyy',
          dtpkrFrom.DateTime) + ''' and ''' + FormatDateTime('mm/dd/yyyy', dtpkrTo.DateTime) + ''''
      else
        s := 'CO_TAX_DEPOSITS_NBR is null and CO_NBR =' + IntToStr(iCO_NBR) + ' and check_date <= ''' + FormatDateTime('mm/dd/yyyy',
          dtpkrTo.DateTime) + '''';
      CO_FED_TAX_LIABILITIES.DataRequired(s);
      CO_STATE_TAX_LIABILITIES.DataRequired(s);
      CO_SUI_LIABILITIES.DataRequired(s);
      CO_LOCAL_TAX_LIABILITIES.DataRequired(s);
      CO_TAX_DEPOSITS.DataRequired('CO_NBR=-1');
      with CO_FED_TAX_LIABILITIES do
      begin
        First;
        while not Eof do
        begin
          if ((SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', CO['SY_FED_TAX_PAYMENT_AGENCY_NBR'],
            'SY_GLOBAL_AGENCY_NBR') = AgencyNbr)
              and (FieldValues['TAX_TYPE'] <> TAX_LIABILITY_TYPE_ER_FUI))
            or ((SY_FED_TAX_PAYMENT_AGENCY.Lookup('SY_FED_TAX_PAYMENT_AGENCY_NBR', CO['FUI_SY_TAX_PAYMENT_AGENCY'], 'SY_GLOBAL_AGENCY_NBR')
              = AgencyNbr)
              and (FieldValues['TAX_TYPE'] = TAX_LIABILITY_TYPE_ER_FUI)) then
          begin
            cdLiabAvail.AppendRecord(
              [FieldValues['DUE_DATE'],
              FieldValues['CHECK_DATE'],
                GetDescr(FedTaxLiabilityType_ComboChoices, FieldValues['TAX_TYPE']),
                ReturnDescription(TaxDepositStatus_ComboChoices, FieldValues['STATUS']),
                FieldValues['AMOUNT'],
                Ord(ttFed),
                FieldValues['CO_FED_TAX_LIABILITIES_NBR']
                ]);
          end;
          Next;
        end;
      end;
      with CO_STATE_TAX_LIABILITIES do
      begin
        First;
        while not Eof do
        begin
          Assert(CO_STATES.Locate('CO_STATES_NBR', FieldValues['CO_STATES_NBR'], []));
          Assert(SY_STATES.Locate('STATE', CO_STATES['STATE'], []));
          if (SY_STATES['SY_STATE_TAX_PMT_AGENCY_NBR'] = AgencyNbr) then
          begin
            cdLiabAvail.AppendRecord(
              [FieldValues['DUE_DATE'],
              FieldValues['CHECK_DATE'],
                GetDescr(CO_STATES['STATE'] + '- ' + StateTaxLiabilityType_ComboChoices, FieldValues['TAX_TYPE']),
                ReturnDescription(TaxDepositStatus_ComboChoices, FieldValues['STATUS']),
                FieldValues['AMOUNT'],
                Ord(ttState),
                FieldValues['CO_STATE_TAX_LIABILITIES_NBR']
                ]);
          end;
          Next;
        end;
      end;
      with CO_SUI_LIABILITIES do
      begin
        First;
        while not Eof do
        begin
          Assert(CO_SUI.Locate('CO_SUI_NBR', FieldValues['CO_SUI_NBR'], []));
          Assert(SY_SUI.Locate('SY_SUI_NBR', CO_SUI['SY_SUI_NBR'], []));
          if (SY_SUI['SY_SUI_TAX_PMT_AGENCY_NBR'] = AgencyNbr) then
          begin
            cdLiabAvail.AppendRecord(
              [FieldValues['DUE_DATE'],
              FieldValues['CHECK_DATE'],
                SY_SUI['SUI_TAX_NAME'],
                ReturnDescription(TaxDepositStatus_ComboChoices, FieldValues['STATUS']),
                FieldValues['AMOUNT'],
                Ord(ttSui),
                FieldValues['CO_SUI_LIABILITIES_NBR']
                ]);
          end;
          Next;
        end;
      end;
      with CO_LOCAL_TAX_LIABILITIES do
      begin
        First;
        while not Eof do
        begin
          Assert(CO_LOCAL_TAX.Locate('CO_LOCAL_TAX_NBR', FieldValues['CO_LOCAL_TAX_NBR'], []));
          Assert(SY_LOCALS.Locate('SY_LOCALS_NBR', CO_LOCAL_TAX['SY_LOCALS_NBR'], []));
          if (SY_LOCALS['SY_LOCAL_TAX_PMT_AGENCY_NBR'] = AgencyNbr) then
          begin
            cdLiabAvail.AppendRecord(
              [FieldValues['DUE_DATE'],
              FieldValues['CHECK_DATE'],
                SY_LOCALS['NAME'],
                ReturnDescription(TaxDepositStatus_ComboChoices, FieldValues['STATUS']),
                FieldValues['AMOUNT'],
                Ord(ttLoc),
                FieldValues['CO_LOCAL_TAX_LIABILITIES_NBR']
                ]);
          end;
          Next;
        end;
      end;
      cdLiabAvail.First;
    end;
  finally
    ctx_EndWait;
  end;
  ManualNotebook2.PageIndex := 1;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn19Click(
  Sender: TObject);
var
  ds: TDataSet;
  TaxDepNbr: Integer;
begin
  inherited;
  cdLiabSelected.DisableControls;
  with DM_COMPANY do
  try
    try
      with CO_TAX_DEPOSITS do
      begin
        Append;
        if crdepRef.Text = '' then
          FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString := 'Manual ' + DateToStr(Now)
        else
          FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString := crdepRef.Text;
        FieldByName('STATUS').AsString := TAX_DEPOSIT_STATUS_PAID;
        FieldByName('STATUS_DATE').AsDateTime := crdepStatusDate.DateTime;
        FieldByName('PR_NBR').Value := Null;
        FieldByName('DEPOSIT_TYPE').AsString := FComboValue[cbDepositType.ItemIndex + 1];
        FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').AsInteger := GetGlobalAgencyFieldOffice(cdAgencies['AgencyNbr']);
        Post;
        TaxDepNbr := FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
      end;
      cdLiabSelected.First;
      while not cdLiabSelected.Eof do
      begin
        case cdLiabSelected['TaxTable'] of
          Ord(ttFed):
            ds := CO_FED_TAX_LIABILITIES;
          Ord(ttState):
            ds := CO_STATE_TAX_LIABILITIES;
          Ord(ttSui):
            ds := CO_SUI_LIABILITIES;
          Ord(ttLoc):
            ds := CO_LOCAL_TAX_LIABILITIES;
        else
          ds := nil;
        end;
        Assert(Assigned(ds));
        Assert(ds.Locate(ds.Name + '_NBR', cdLiabSelected['Nbr'], []));
        ds.Edit;
        ds['STATUS'] := TAX_DEPOSIT_STATUS_PAID;
        ds['CO_TAX_DEPOSITS_NBR'] := TaxDepNbr;
        ds.Post;
        cdLiabSelected.Next;
      end;
      ctx_DataAccess.PostDataSets([
        CO_TAX_DEPOSITS,
          CO_FED_TAX_LIABILITIES,
          CO_STATE_TAX_LIABILITIES,
          CO_SUI_LIABILITIES,
          CO_LOCAL_TAX_LIABILITIES
          ]);
    except
      ctx_DataAccess.CancelDataSets([
        CO_FED_TAX_LIABILITIES,
          CO_STATE_TAX_LIABILITIES,
          CO_SUI_LIABILITIES,
          CO_LOCAL_TAX_LIABILITIES,
          CO_TAX_DEPOSITS]);
      raise;
    end;
  finally
    cdLiabSelected.EnableControls;
  end;
  if Sender = BitBtn19 then
  begin
    PageControl1.ActivePage := tsLiability;
    btnCloseCompany.Click;
    btnOpenCompany.Click;
    UpdateScreenStatus;
  end
  else
    btnAddDepos.Click;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn7Click(
  Sender: TObject);
begin
  inherited;
  FDataAdded := False;
  PageControl1.ActivePage := tsLiability;
  UpdateScreenStatus;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.cdLiabTypesBeforeScroll(
  DataSet: TDataSet);
begin
  inherited;
  if DataSet.Tag = 0 then
  begin
    FOldStatus := DataSet.FieldByName('TaxStatus').Value;
    FOldAdjType := DataSet.FieldByName('TaxAdjType').Value;
    FOldThirdParty := DataSet.FieldByName('TaxThirdParty').Value;
    FOldCollected := DataSet.FieldByName('TaxCollected').Value;
  end;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.cdLiabTypesAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  if DataSet.Tag = 0 then
    if DataSet.FieldByName('TaxStatus').IsNull then
    begin
      DataSet.Edit;
      DataSet.FieldByName('TaxStatus').Value := FOldStatus;
      DataSet.FieldByName('TaxAdjType').Value := FOldAdjType;
      DataSet.FieldByName('TaxThirdParty').Value := FOldThirdParty;
      DataSet.FieldByName('TaxCollected').Value := FOldCollected;
      DataSet.Post;
    end;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.cdLiabTypesAfterPost(
  DataSet: TDataSet);
var
  c940, c941: Currency;
begin
  inherited;
  if DataSet.Tag = 0 then
  begin
    c940 := 0;
    c941 := 0;
    with TEvBasicClientDataSet.Create(nil) do
    try
      CloneCursor(TEvBasicClientDataSet(DataSet), False);
      First;
      while not Eof do
      begin
        if (ConvertNull(FieldValues['TaxAmount'], 0) <> 0)
          and (FieldValues['TaxTable'] = Ord(ttFed)) then
        begin
          if FieldValues['TaxType'] = TAX_LIABILITY_TYPE_ER_FUI then
            c941 := c941 + FieldValues['TaxAmount']
          else
            c940 := c940 + FieldValues['TaxAmount'];
        end;
        Next;
      end;
    finally
      Free;
    end;
    lTot940.Caption := CurrToStr(c940);
    lTot941.Caption := CurrToStr(c941);
  end;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn17Click(
  Sender: TObject);
begin
  inherited;
  if not VarIsNull(cdLiabAvail['Nbr']) then
  begin
    cdLiabSelected.Append;
    cdLiabSelected['DUE_DATE;check_date;Name;status;Amount;TaxTable;Nbr'] :=
      cdLiabAvail['DUE_DATE;check_date;Name;status;Amount;TaxTable;Nbr'];
    cdLiabSelected.Post;
    cdLiabAvail.Delete;
  end;
  CalcDepositTotals;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.BitBtn16Click(
  Sender: TObject);
begin
  inherited;
  if not VarIsNull(cdLiabSelected['Nbr']) then
  begin
    cdLiabAvail.Append;
    cdLiabAvail['DUE_DATE;check_date;Name;status;Amount;TaxTable;Nbr'] :=
      cdLiabSelected['DUE_DATE;check_date;Name;status;Amount;TaxTable;Nbr'];
    cdLiabAvail.Post;
    cdLiabSelected.Delete;
  end;
  CalcDepositTotals;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.sbtnMoveThemAllClick(
  Sender: TObject);
begin
  inherited;
  cdLiabAvail.DisableControls;
  cdLiabSelected.DisableControls;
  try
    cdLiabAvail.First;
    while not cdLiabAvail.Eof do
    begin
      if not VarIsNull(cdLiabAvail['Nbr']) then
      begin
        cdLiabSelected.Append;
        cdLiabSelected['DUE_DATE;check_date;Name;status;Amount;TaxTable;Nbr'] :=
          cdLiabAvail['DUE_DATE;check_date;Name;status;Amount;TaxTable;Nbr'];
        cdLiabSelected.Post;
        cdLiabAvail.Delete;
      end
      else
        cdLiabAvail.Next;
    end;
  finally
    cdLiabAvail.EnableControls;
    cdLiabSelected.EnableControls;
  end;
  CalcDepositTotals;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.wwDBGrid4DblClick(
  Sender: TObject);
begin
  inherited;
  BitBtn17.Click;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.wwDBGrid5DblClick(
  Sender: TObject);
begin
  inherited;
  BitBtn16.Click;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.CalcDepositTotals;
var
  c: Currency;
begin
  with TISBasicClientDataSet.Create(nil) do
  try
    c := 0;
    CloneCursor(cdLiabSelected, False);
    First;
    while not Eof do
    begin
      c := c + FieldValues['Amount'];
      Next;
    end;
  finally
    Free;
  end;
  Label14.Caption := 'Total selected : ' + CurrToStr(c);
end;

{procedure TProcessCombinedLiabilitiesAndDeposits.Frame_4_DB_GRIDS2wwDBGrid2TitleButtonClick(
  Sender: TObject; AFieldName: String);
begin
  inherited;
  with (Sender as TevDBGrid).DataSource.DataSet as TEvBasicClientDataSet do
  begin
    DisableControls;
    try
      if MasterFields = '' then
        IndexFieldNames := AFieldName
      else
        IndexFieldNames := MasterFields+ ';'+ AFieldName;
    finally
      EnableControls;
    end;
  end;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.Frame_4_DB_GRIDS2wwDBGrid2CalcTitleAttributes(
  Sender: TObject; AFieldName: String; AFont: TFont; ABrush: TBrush;
  var ATitleAlignment: TAlignment);
begin
  inherited;
  with (Sender as TevDBGrid).DataSource.DataSet as TEvBasicClientDataSet do
  begin
    if Pos(AFieldName, IndexFieldNames) <> 0  then
    begin
      AFont.Color := clBlue;
      ABrush.Color := clYellow ;
    end;
  end;
end;}

procedure TProcessCombinedLiabilitiesAndDeposits.wwDBGrid2Enter(
  Sender: TObject);
begin
  inherited;
  wwDBGrid2.ForceKeyOptions([dgAllowDelete,dgAllowInsert]);
end;

procedure TProcessCombinedLiabilitiesAndDeposits.wwDBGrid2KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then
  begin
    cdLiabTypes.CheckBrowseMode;
    cdLiabTypes.Next;
    wwDBGrid2.SetActiveField('TaxAmount');
  end
  else
  if Key = 121 then
    BitBtn6.Click
  else
  if Key = 107 then
    BitBtn1.Click
  else
    inherited;
end;

procedure TProcessCombinedLiabilitiesAndDeposits.crdepStatusDateChange(
  Sender: TObject);
begin
  inherited;
  crdepStatusDate.Time := 0;
end;

initialization
  RegisterClass(TPROCESSCOMBINEDLIABILITIESANDDEPOSITS);

end.
