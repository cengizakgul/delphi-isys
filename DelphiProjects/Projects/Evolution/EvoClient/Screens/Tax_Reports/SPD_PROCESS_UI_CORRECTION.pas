// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PROCESS_UI_CORRECTION;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, Wwdbigrd, Wwdbgrid, Wwdatsrc, EvBasicUtils, EvContext,
   StdCtrls, ComCtrls, SFrameEntry,  Spin, EvTypes,
  Buttons, Variants, kbmMemTable, ISKbmMemDataSet, ISBasicClasses,
  ISDataAccessComponents, EvDataAccessComponents, EvExceptions, EvUIComponents, EvClientDataSet;

type
  TPROCESS_UI_CORRECTION = class(TFrameEntry)
    cdClCo: TevClientDataSet;
    cdClCoCL_NBR: TIntegerField;
    cdClCoCUSTOM_CLIENT_NUMBER: TStringField;
    cdClCoCLIENT_NAME: TStringField;
    cdClCoCO_NBR: TIntegerField;
    cdClCoCUSTOM_COMPANY_NUMBER: TStringField;
    cdClCoCOMPANY_NAME: TStringField;
    cdClCoTAX_SERVICE: TStringField;
    dsClCo: TevDataSource;
    Pc: TevPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    cbMonth: TevComboBox;
    wwDBGrid1: TevDBGrid;
    cbLoadFUI: TevCheckBox;
    cdCorrections: TevClientDataSet;
    dsTaxes: TevDataSource;
    cdCorrectionsNUMBER: TStringField;
    cdCorrectionsNAME: TStringField;
    cdCorrectionsTaxType: TStringField;
    cdCorrectionsTaxDesc: TStringField;
    cdCorrectionsTotalCalc: TCurrencyField;
    cdCorrectionsTotalReal: TCurrencyField;
    cdCorrectionsTotalDiff: TCurrencyField;
    cdCorrectionsDo: TBooleanField;
    wwDBGrid2: TevDBGrid;
    cdCorrectionscl_nbr: TIntegerField;
    cdCorrectionsco_nbr: TIntegerField;
    cdCorrectionsNbr: TIntegerField;
    cdCorrectionsDone: TBooleanField;
    cdClCoLOAD: TBooleanField;
    cdClCoDESCRI: TStringField;
    cbConsolidate: TevCheckBox;
    cdClCoTAX_TYPE: TStringField;
    cdClCoTAX_TYPE_DESCR: TStringField;
    edYear: TevSpinEdit;
    cdCorrectionsTAX_SERVICE: TStringField;
    cbLoadMO: TevCheckBox;
    Button4: TevBitBtn;
    Button5: TevBitBtn;
    Button1: TevBitBtn;
    Button3: TevBitBtn;
    Button2: TevBitBtn;
    procedure wwDBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure Button1Click(Sender: TObject);
    procedure cbMonthChange(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    FHolder: TComponent;
    FEndDate: TDateTime;
    procedure UpdateAll(const Value: Boolean);
    procedure TurnFlagInCompanyTable;
  public
    { Public declarations }
    procedure Activate; override;
  end;

var
  PROCESS_UI_CORRECTION: TPROCESS_UI_CORRECTION;

implementation

{$R *.DFM}

uses
  EvConsts, EvUtils, SDataStructure, SReportSettings, SDM_MISC_LIAB_CORRECT, isBaseClasses;

const
  sCompany = 'Company';
  sConsolidation = 'Consolidation';

procedure TPROCESS_UI_CORRECTION.Activate;
var
  d: TevClientDataSet;
  ReadOnlyClientList : IisStringList;

  function RevertTaxType(const TaxType: Char): Char;
  begin
    Result := ' ';
    case TaxType of
    CONSOLIDATION_TYPE_ALL_REPORTS:
      Result := '-';
    CONSOLIDATION_TYPE_STATE:
      Result := CONSOLIDATION_TYPE_FED;
    CONSOLIDATION_TYPE_FED:
      Result := CONSOLIDATION_TYPE_STATE;
    else
      Assert(False);
    end;
  end;
  procedure AddRecord(const Consolid: string; const TaxType: Char);
  var
    i: Integer;
    f: TField;
  begin
    if TaxType <> '-' then
    begin
      cdClCo.Append;
      for i := 0 to cdClCo.FieldCount-1 do
      begin
        f := d.FindField(cdClCo.Fields[i].FieldName);
        if Assigned(f) then
          cdClCo.Fields[i].Assign(f);
      end;
      cdClCo['DESCR'] := Consolid;
      cdClCo['TAX_TYPE'] := TaxType;
      case TaxType of
      CONSOLIDATION_TYPE_ALL_REPORTS:
        cdClCo['TAX_TYPE_DESCR'] := 'All';
      CONSOLIDATION_TYPE_STATE:
        cdClCo['TAX_TYPE_DESCR'] := 'State';
      CONSOLIDATION_TYPE_FED:
        cdClCo['TAX_TYPE_DESCR'] := 'Fed';
      end;
      cdClCo.Post;
    end;
  end;
begin
  inherited;
  cbMonth.Items.Text := Items_Months;
  FHolder := TComponent.Create(Self);
  cdClCo.IndexDefs.Add('SORT', 'CUSTOM_CLIENT_NUMBER', []);
  cdClCo.IndexDefs.Update;
  cdClCo.IndexName := 'SORT';
  cdClCo.CreateDataSet;
  cdClCo.LogChanges := False;
  d := ctx_DataAccess.TEMP_CUSTOM_VIEW;
  with d do
  begin
    Close;
    DataRequest(TExecDSWrapper.Create('ConsolidationsFull').AsVariant);
    Open;

    ReadOnlyClientList := GetReadOnlyClientCompanyList;
    First;
    while not Eof do
    begin
      if (ReadOnlyClientList.IndexOf(d.fieldByName('CL_NBR').AsString+';'+d.fieldByName('CO_NBR').AsString) = -1) then
      begin
        if VarIsNull(FieldValues['CL_CO_CONSOLIDATION_NBR']) then
          AddRecord(sCompany, CONSOLIDATION_TYPE_ALL_REPORTS)
        else
        begin
          AddRecord(sCompany, RevertTaxType(string(FieldValues['CONSOLIDATION_TYPE'])[1]));
          if FieldValues['CO_NBR'] = FieldValues['PRIMARY_CO_NBR'] then
            AddRecord(sConsolidation, string(FieldValues['CONSOLIDATION_TYPE'])[1]);
        end;
      end;
      Next;
    end;
    Close;
  end;
  cdClCo.First;
  wwDBGrid1.SelectRecord;

  if GetMonth(Date) = 1 then
  begin
    cbMonth.ItemIndex := 12-1;
    edYear.Value := GetYear(Date)-1;
  end
  else
  begin
    cbMonth.ItemIndex := GetMonth(Date)-2 ;
    edYear.Value := GetYear(Date);
  end;
  cbMonthChange(nil);
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
end;

procedure TPROCESS_UI_CORRECTION.wwDBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  inherited;
  {if ConvertNull(cdClCo['LOAD'], False) then
  begin
    ABrush.Color := clYellow;
    if (ConvertNull(cdClCo['FUI_DO'], False) and (ConvertNull(cdClCo['FUI_DIFF'], 0) <> 0))
    or (ConvertNull(cdClCo['SUI_DO'], False) and (ConvertNull(cdClCo['SUI_DIFF'], 0) <> 0)) then
      ABrush.Color := clRed;
  end;}
end;

procedure TPROCESS_UI_CORRECTION.Button1Click(Sender: TObject);
begin
  cdClCo.CheckBrowseMode;
  FEndDate := GetEndMonth(EncodeDate(edYear.Value, cbMonth.ItemIndex+ 1, 1));
  cdCorrections.Close;
  cdCorrections.CreateDataSet;
  FHolder.Free;
  FHolder := TComponent.Create(Self);
  inherited;
  ctx_StartWait;
  with TevClientDataSet.Create(nil) do
  try
    CloneCursor(cdClCo, False);
    IndexFieldNames := 'CL_NBR';
    Resync([]);
    First;
    while not Eof do
    begin
      if ConvertNull(FieldValues['LOAD'], False) then
      begin
        ctx_UpdateWait('Loading client '+ FieldByName('CLIENT_NAME').AsString);
        with TDM_MISC_LIAB_CORRECT.Create(FHolder, FieldValues['CL_NBR'], FieldValues['CO_NBR'], SbSendsTaxPaymentsForCompany(FieldValues['TAX_SERVICE']), FEndDate,
          cbLoadFUI.Checked, cbLoadMO.Checked, GetEndQuarter(FEndDate) = FEndDate, True) do
        begin
          cdTaxes.First;
          while not cdTaxes.Eof do
          begin
            {if (cdTaxes['TaxType'] = Ord(ttFui)) and not cbLoadFUI.Checked then
              cdTaxes.Delete
            else}
            begin
              cdCorrections.Append;
              cdCorrections['CL_NBR;CO_NBR;TAX_SERVICE;NUMBER;NAME'] := FieldValues['CL_NBR;CO_NBR;TAX_SERVICE;CUSTOM_COMPANY_NUMBER;COMPANY_NAME'];
              cdCorrections['NBR;TOTALREAL;TOTALCALC;TOTALDIFF;TAXDESC'] := cdTaxes['NBR;TOTALREAL;TOTALCALC;TOTALDIFF;TAXDESC'];
              cdCorrections['TAXTYPE'] := '-';
              cdCorrections['DO'] := False;
              cdCorrections.Post;
              cdTaxes.Edit;
              cdTaxes['ToDo'] := False;
              cdTaxes.Post;
              cdTaxes.Next;
            end;
          end;
        end;
      end;
      Next;
    end;
    cdCorrections.MergeChangeLog;
  finally
    Free;
    ctx_EndWait;
  end;
  if cdCorrections.RecordCount = 0 then
    TurnFlagInCompanyTable;
  Button2.Enabled := cdCorrections.RecordCount <> 0;
  Pc.ActivePage := ts2;
  ts2.TabVisible := True;
  ts1.TabVisible := False;
end;

procedure TPROCESS_UI_CORRECTION.cbMonthChange(Sender: TObject);
begin
  inherited;
  cbLoadFUI.Checked := cbMonth.ItemIndex = 12-1;
  cbLoadFUI.Enabled := not cbLoadFUI.Checked;
end;

procedure TPROCESS_UI_CORRECTION.Button3Click(Sender: TObject);
begin
  inherited;
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
end;

procedure TPROCESS_UI_CORRECTION.Button2Click(Sender: TObject);
var
  CurrClNbr, CurrCoNbr: Integer;
  bClicked: Boolean;
  i: Integer;
  ReportParams: TrwReportParams;
  DM_MISC_LIAB_CORRECT: TDM_MISC_LIAB_CORRECT;
  d: TevClientDataSet;
begin
  inherited;
  cdCorrections.CheckBrowseMode;
  CurrClNbr := -1;
  CurrCoNbr := -1;
  DM_MISC_LIAB_CORRECT := nil;
  ctx_StartWait('Correcting...');
  d := TevClientDataSet.Create(nil);
  with d do
  try
    CloneCursor(cdCorrections, False);
    MergeChangeLog;
    bClicked := RecordCount = 0;
    IndexFieldNames := 'CL_NBR;CO_NBR';
    Resync([]);
    First;
    while not Eof do
    begin
      if FieldValues['DO'] then
      begin
        bClicked := True;
        if (CurrClNbr <> FieldValues['CL_NBR'])
        or (CurrCoNbr <> FieldValues['CO_NBR']) then
        begin
          if Assigned(DM_MISC_LIAB_CORRECT) then
          try
            DM_MISC_LIAB_CORRECT.ApplyChanges;
            MergeChangeLog;
          except
            CancelUpdates;
            raise;
          end;
          CurrClNbr := FieldValues['CL_NBR'];
          CurrCoNbr := FieldValues['CO_NBR'];
          for i := 0 to FHolder.ComponentCount-1 do
            if (TDM_MISC_LIAB_CORRECT(FHolder.Components[i]).ClNbr = CurrClNbr)
            and (TDM_MISC_LIAB_CORRECT(FHolder.Components[i]).CoNbr = CurrCoNbr) then
            begin
              DM_MISC_LIAB_CORRECT := TDM_MISC_LIAB_CORRECT(FHolder.Components[i]);
              Break;
            end;
        end;
        Assert(DM_MISC_LIAB_CORRECT.cdTaxes.Locate('Nbr', FieldValues['Nbr'], []));
        DM_MISC_LIAB_CORRECT.cdTaxes.Edit;
        DM_MISC_LIAB_CORRECT.cdTaxes['ToDo'] := True;
        DM_MISC_LIAB_CORRECT.cdTaxes.Post;
        Edit;
        FieldValues['Done'] := True;
        Post;
      end
      else
      begin
        Edit;
        FieldValues['Done'] := False;
        Post;
      end;
      Next;
    end;
    if Assigned(DM_MISC_LIAB_CORRECT) then
    try
      DM_MISC_LIAB_CORRECT.ApplyChanges;
      MergeChangeLog;
    except
      CancelUpdates;
      raise;
    end;
    if not bClicked then
      raise EInvalidParameters.CreateHelp('You have not picked up any lines to correct', IDH_InvalidParameters);
  finally
    Free;

    ctx_EndWait;
    if cdCorrections.Active then
    begin
      ReportParams := TrwReportParams.Create;
      DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('ALL');
      if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'UI Corrections', [loCaseInsensitive]) then
          raise EMissingReport.CreateHelp('"UI Correction" report does not exist in the database!', IDH_MissingReport);
      ReportParams.Add('DataSets', DataSetsToVarArray([cdCorrections]));

      ctx_RWLocalEngine.StartGroup;
      try
        ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').Value,
           CH_DATABASE_SYSTEM, ReportParams);
      finally
        ctx_RWLocalEngine.EndGroup(rdtPrinter);
      end;

      ReportParams.Free;
    end;

//    if cdTaxes.Active then
//      PrintCustomReport('rbUICorrection', VarArrayOf([cdTaxes.Data]));
  end;
  TurnFlagInCompanyTable;
  UpdateAll(False);
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
end;

procedure TPROCESS_UI_CORRECTION.Button4Click(Sender: TObject);
begin
  inherited;
  UpdateAll(True);
end;

procedure TPROCESS_UI_CORRECTION.Button5Click(Sender: TObject);
begin
  inherited;
  UpdateAll(False);
end;

procedure TPROCESS_UI_CORRECTION.UpdateAll(const Value: Boolean);
var
  i: Integer;
  s: string;
begin
  i := cdClCo.RecNo;
  cdClCo.DisableControls;
  s := cdClCo.IndexName;
  cdClCo.IndexName := '';
  with cdClCo do
  try
    First;
    while not Eof do
    begin
      Edit;
      FieldValues['LOAD'] := Value;
      Post;
      Next;
    end;
  finally
    cdClCo.IndexName := s;
    cdClCo.RecNo := i;
    cdClCo.EnableControls;
  end;
end;

procedure TPROCESS_UI_CORRECTION.wwDBGrid1DblClick(Sender: TObject);
var
  d: TDateTime;
begin
  inherited;
  Exit;
  ctx_DataAccess.OpenClient(cdClCo['cl_nbr']);
  with ctx_DataAccess.CUSTOM_VIEW do
  begin
    Close;
    with TExecDSWrapper.Create('ConsolidationQuarterSuiWages') do
    begin
      d := StrToDateTime('1/1/2000');
      SetParam('BegDate', d);
      d := StrToDateTime('3/1/2000');
      SetParam('EndDate', d);
      SetParam('CoNbr', cdClCo['co_nbr']);
      DataRequest(AsVariant);
    end;
    Open;
  end;
end;

procedure TPROCESS_UI_CORRECTION.TurnFlagInCompanyTable;
  procedure Process(const CoNbr: Integer; const TaxType: Char);
  begin
    if DM_COMPANY.CO.Locate('CO_NBR', CoNbr, []) then
    begin
      DM_COMPANY.CO.Edit;
      DM_COMPANY.CO.FieldByName('LAST_SUI_CORRECTION').AsDateTime := FEndDate;
      if cbLoadFUI.Checked then
        DM_COMPANY.CO.FieldByName('LAST_FUI_CORRECTION').AsDateTime := FEndDate;
      DM_COMPANY.CO.Post;
    end;
  end;
var
  CurrClNbr: Integer;
begin
  CurrClNbr := -1;
  DM_TEMPORARY.TMP_CL_CO_CONSOLIDATION.DataRequired('ALL');
  ctx_StartWait;
  with TevClientDataSet.Create(nil) do
  try
    CloneCursor(cdClCo, False);
    IndexFieldNames := 'CL_NBR';
    Resync([]);
    First;
    while not Eof do
    begin
      if ConvertNull(FieldValues['LOAD'], False) then
      begin
        ctx_UpdateWait('Marking client '+ FieldByName('CLIENT_NAME').AsString);
        if CurrClNbr <> FieldValues['CL_NBR'] then
        begin
          if CurrClNbr <> -1 then
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO]);
          ctx_DataAccess.OpenClient(FieldValues['CL_NBR']);
          DM_COMPANY.CO.DataRequired('ALL');
        end;
        CurrClNbr := FieldValues['CL_NBR'];
        if not cdCorrections.Locate('CL_NBR;CL_NBR;DONE', VarArrayOf([FieldValues['CL_NBR'], FieldValues['CO_NBR'], False]), []) then
        begin
          if cdClCo['DESCR'] = sConsolidation then
          begin
            DM_TEMPORARY.TMP_CL_CO_CONSOLIDATION.First;
            while not DM_TEMPORARY.TMP_CL_CO_CONSOLIDATION.Eof do
            begin
              if (DM_TEMPORARY.TMP_CL_CO_CONSOLIDATION['CL_NBR'] = FieldValues['CL_NBR'])
              and (DM_TEMPORARY.TMP_CL_CO_CONSOLIDATION['PRIMARY_CO_NBR'] = FieldValues['CO_NBR']) then
                Process(FieldValues['CO_NBR'], string(FieldValues['TAX_TYPE'])[1]);
              DM_TEMPORARY.TMP_CL_CO_CONSOLIDATION.Next;
            end;
          end
          else
            Process(FieldValues['CO_NBR'], string(FieldValues['TAX_TYPE'])[1]);
        end;
      end;
      Next;
    end;
    if CurrClNbr <> -1 then
      ctx_DataAccess.PostDataSets([DM_COMPANY.CO]);
  finally
    Free;
    ctx_EndWait;
  end;
  cdCorrections.MergeChangeLog;
  Pc.ActivePage := ts2;
  ts2.TabVisible := True;
  ts1.TabVisible := False;
end;

initialization
  RegisterClass(TPROCESS_UI_CORRECTION);

end.
