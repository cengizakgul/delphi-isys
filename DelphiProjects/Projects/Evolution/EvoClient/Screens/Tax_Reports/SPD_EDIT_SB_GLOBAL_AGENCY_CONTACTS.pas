// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_GLOBAL_AGENCY_CONTACTS;

interface

uses
  SFrameEntry, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Forms, Dialogs, Db, Wwdatsrc, ExtCtrls, Grids, Wwdbigrd,
  Wwdbgrid, ComCtrls, StdCtrls, DBCtrls, Mask, wwdbedit, Buttons,
   SDataStructure, EvUIComponents, EvUtils, EvClientDataSet;
       
type
  TEDIT_SB_GLOBAL_AGENCY_CONTACTS = class(TFrameEntry)
    PageControl1: TevPageControl;
    tsBrowse: TTabSheet;
    tsDetail: TTabSheet;
    wwDBGrid1: TevDBGrid;
    Splitter1: TevSplitter;
    wwDBGrid2: TevDBGrid;
    lablContact_Name: TevLabel;
    dedtContact_Name: TevDBEdit;
    lablPhone: TevLabel;
    lablFax: TevLabel;
    lablE_Mail_Address: TevLabel;
    dedtE_Mail_Address: TevDBEdit;
    lablNotes: TevLabel;
    Label6: TevLabel;
    dtxtAgency_Name: TevDBText;
    dmemNotes: TEvDBMemo;
    wwdePhone: TevDBEdit;
    wwdeFax: TevDBEdit;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    Bevel1: TBevel;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_GLOBAL_AGENCY_CONTACTS: TEDIT_SB_GLOBAL_AGENCY_CONTACTS;

implementation

{$R *.DFM}

function TEDIT_SB_GLOBAL_AGENCY_CONTACTS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_GLOBAL_AGENCY_CONTACTS;
end;

function TEDIT_SB_GLOBAL_AGENCY_CONTACTS.GetInsertControl: TWinControl;
begin
  Result := dedtContact_Name;
end;

procedure TEDIT_SB_GLOBAL_AGENCY_CONTACTS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDS(DM_SYSTEM_MISC.SY_GLOBAL_AGENCY, SupportDataSets);
end;

initialization
  RegisterClass(TEDIT_SB_GLOBAL_AGENCY_CONTACTS);

end.
