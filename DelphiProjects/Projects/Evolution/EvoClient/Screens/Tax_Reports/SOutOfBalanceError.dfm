object OutOfBalanceError: TOutOfBalanceError
  Left = 162
  Top = 183
  Width = 711
  Height = 610
  Caption = 'Out Of Balance Details'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inline BalanceReportScreen: TBalanceReportScreen
    Top = 61
    Width = 703
    Height = 522
    TabOrder = 1
    Visible = True
    inherited Pc: TevPageControl
      Width = 703
      Height = 522
      inherited ts1: TTabSheet
        inherited wwDBGrid1: TevDBGrid
          Left = 11
        end
      end
      inherited ts2: TTabSheet
        inherited evSplitter1: TevSplitter
          Height = 494
        end
        inherited evDBGrid1: TevDBGrid
          Height = 494
        end
        inherited evDBGrid2: TevDBGrid
          Width = 387
          Height = 494
        end
      end
      inherited ts3: TTabSheet
        inherited gWageDetails: TevDBGrid
          Width = 695
          Height = 494
        end
      end
      inherited ts4: TTabSheet
        inherited gLiabDetails: TevDBGrid
          Width = 695
          Height = 494
        end
      end
    end
    inherited cdsSummary: TevClientDataSet
      OnFilterRecord = BalanceReportScreencdsSummaryFilterRecord
    end
  end
  object pnl: TevPanel
    Left = 0
    Top = 0
    Width = 703
    Height = 61
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    TabOrder = 0
    object lblErrorMsg: TevLabel
      Left = 12
      Top = 12
      Width = 52
      Height = 13
      Caption = 'lblErrorMsg'
    end
    object cbFilter: TevCheckBox
      Left = 303
      Top = 16
      Width = 142
      Height = 17
      Caption = 'Show only out of balance'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = cbFilterClick
    end
    object btnClose: TevBitBtn
      Left = 456
      Top = 12
      Width = 75
      Height = 25
      Caption = 'Close'
      Default = True
      TabOrder = 1
      OnClick = btnCloseClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
        DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
        9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
        DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
        DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
        DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
        91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
        999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
      NumGlyphs = 2
    end
  end
end
