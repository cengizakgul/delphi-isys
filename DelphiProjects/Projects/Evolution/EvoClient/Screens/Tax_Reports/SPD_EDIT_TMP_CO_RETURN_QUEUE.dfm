inherited EDIT_TMP_CO_TAX_RETURN_QUEUE: TEDIT_TMP_CO_TAX_RETURN_QUEUE
  Width = 819
  Height = 538
  object evPanel2: TevPanel [0]
    Left = 0
    Top = 0
    Width = 819
    Height = 110
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object gbTop: TevGroupBox
      Left = 0
      Top = 0
      Width = 689
      Height = 111
      Caption = 'Select records'
      TabOrder = 0
      object Label1: TevLabel
        Left = 8
        Top = 38
        Width = 24
        Height = 13
        Caption = 'Type'
      end
      object Label2: TevLabel
        Left = 288
        Top = 38
        Width = 53
        Height = 13
        Caption = 'Date range'
      end
      object Label3: TevLabel
        Left = 437
        Top = 38
        Width = 6
        Height = 13
        Caption = '--'
      end
      object Label4: TevLabel
        Left = 288
        Top = 14
        Width = 30
        Height = 13
        Caption = 'Status'
      end
      object Label5: TevLabel
        Left = 8
        Top = 14
        Width = 50
        Height = 13
        Caption = 'Frequency'
      end
      object Label6: TevLabel
        Left = 8
        Top = 62
        Width = 55
        Height = 13
        Caption = 'Tax service'
      end
      object Label19: TevLabel
        Left = 288
        Top = 62
        Width = 37
        Height = 13
        Caption = 'Filter on'
      end
      object Bevel1: TEvBevel
        Left = 616
        Top = 8
        Width = 9
        Height = 59
        Shape = bsLeftLine
      end
      object Bevel2: TEvBevel
        Left = 536
        Top = 67
        Width = 9
        Height = 27
        Shape = bsLeftLine
      end
      object Bevel3: TEvBevel
        Left = 537
        Top = 62
        Width = 80
        Height = 6
        Shape = bsBottomLine
      end
      object evLabel6: TevLabel
        Left = 288
        Top = 38
        Width = 30
        Height = 13
        Caption = 'Period'
      end
      object cbQuarters: TevComboBox
        Left = 352
        Top = 38
        Width = 177
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 7
        OnChange = cbQuartersChange
      end
      object cbType: TevComboBox
        Left = 72
        Top = 38
        Width = 193
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 4
        OnChange = cbTypeChange
      end
      object cbDateFrom: TevDateTimePicker
        Left = 352
        Top = 38
        Width = 81
        Height = 21
        Date = 36601.619315972200000000
        Time = 36601.619315972200000000
        TabOrder = 8
        OnChange = cbDateFromChange
      end
      object cbDateTill: TevDateTimePicker
        Left = 448
        Top = 38
        Width = 81
        Height = 21
        Date = 36601.619596412000000000
        Time = 36601.619596412000000000
        TabOrder = 9
        OnCloseUp = cbDateTillCloseUp
        OnChange = cbDateTillChange
      end
      object cbStatus: TevComboBox
        Left = 352
        Top = 14
        Width = 177
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 6
        OnChange = cbStatusChange
      end
      object cbTaxService: TevComboBox
        Left = 72
        Top = 62
        Width = 193
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 5
        OnChange = cbTaxServiceChange
      end
      object DateKindSelectionPanel: TevPanel
        Left = 352
        Top = 62
        Width = 177
        Height = 25
        BevelOuter = bvLowered
        TabOrder = 10
        object cbDateIsDueDate: TevRadioButton
          Left = 7
          Top = 5
          Width = 66
          Height = 17
          Caption = 'Due date'
          TabOrder = 0
          OnClick = cbDateIsDueDateClick
        end
        object cbDateIsPeriodEndDate: TevRadioButton
          Left = 95
          Top = 5
          Width = 76
          Height = 17
          Caption = 'Period end'
          TabOrder = 1
          OnClick = cbDateIsPeriodEndDateClick
        end
      end
      object cbMultiDelete: TevCheckBox
        Left = 540
        Top = 72
        Width = 60
        Height = 17
        Caption = 'Multi del'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
        TabOrder = 14
        OnClick = cbMultiDeleteClick
      end
      object btnAdd: TevBitBtn
        Left = 624
        Top = 42
        Width = 57
        Height = 26
        Caption = 'Add'
        TabOrder = 12
        OnClick = btnAddClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDD8888DDDDDDDDDDDDFFFFDDDDDDDDDDD22228DD
          DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
          DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDD8888AAA2888
          888DDFFFFF888FFFFFFD22222AAA2222228DD8888888888888FDAAAAAAAAAAAA
          A28DD8888888888888FDAAAAAAAAAAAAA28DD8888888888888FDAAAAAAAAAAAA
          A2DDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
          DDDDDDDDDD888FDDDDDDDDDDDAAA28DDDDDDDDDDDD888FDDDDDDDDDDDAAA28DD
          DDDDDDDDDD888FDDDDDDDDDDDAAA2DDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object btnDel: TevBitBtn
        Left = 624
        Top = 75
        Width = 57
        Height = 26
        Caption = 'Del'
        Enabled = False
        TabOrder = 13
        OnClick = btnDelClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD88888888888
          888DDFFFFFFFFFFFFFFD444444444444448DD8888888888888FDCCCCCCCCCCCC
          C48DD8888888888888FDCCCCCCCCCCCCC48DD8888888888888FDCCCCCCCCCCCC
          C4DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object cbSimpleMode: TevCheckBox
        Left = 535
        Top = 40
        Width = 72
        Height = 17
        Caption = 'Brief mode'
        Checked = True
        Color = clBtnFace
        ParentColor = False
        State = cbChecked
        TabOrder = 11
        OnClick = cbSimpleModeClick
      end
      object cbTypeNot: TevCheckBox
        Left = 35
        Top = 37
        Width = 37
        Height = 17
        Alignment = taLeftJustify
        Caption = 'not'
        TabOrder = 15
        OnClick = cbTypeNotClick
      end
      object btnApplySelect: TevBitBtn
        Left = 535
        Top = 9
        Width = 75
        Height = 25
        Caption = 'Select'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 16
        OnClick = btnApplySelectClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD0000
          008DDDDDDDDD888888FD0000DDDD0000008D8888DDDD888888FD00D0DDD80000
          008D88D8DDDF888888FD0D80DD80FFFF77088D88DDF8DDDDDD8F0000DD0FFFFF
          F7088888DD8DDDDDDD8FDDDDDD07FFFFF708DDDDDD8DDDDDDD8F0000DD80FFFF
          77088888DDD8DDDDDD8F00D0DDD0FF77008D88D8DDD8DDDD88FD0D80DDD0F000
          8DDD8D88DDD8D888FDDD0000DDD0F08DDDDD8888DDD8D8FDDDDDDDDDDDD0F08D
          DDDDDDDDDDD8D8FDDDDD0000DDD0F08DDDDD8888DDD8D8FDDDDD00D0DDD0F08D
          DDDD88D8DDD8D8FDDDDD0D80DDD808DDDDDD8D88DDDD8FDDDDDD0000DDDDDDDD
          DDDD8888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object btnEnlist: TevBitBtn
        Left = 624
        Top = 9
        Width = 57
        Height = 26
        Caption = 'Enlist'
        TabOrder = 17
        OnClick = btnEnlistClick
        Color = clBlack
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD888888
          888DDDDDDDFFFFFFFFFDDDDDDD8000000000DDDDDDF888888888DDDDDD80FFFF
          FFF0DDDDDDF8DDDDDDD866666660FFFFFFF0888888F8DDDDDDD8666EEE60F888
          88F0888DDDF8D88888D866666660FFFFFFF0888888F8DDDDDDD8666EEE60F888
          88F0888DDDF8D88888D866666660FFFFFFF0888888F8DDDDDDD8666EEE60F888
          88F0888DDDF8D88888D866666660FFFFFFF0888888F8DDDDDDD8DDDD6D80FFFF
          FFF0DDDD8DF8DDDDDDD8DDDD6DD00000000088DD8DD888888888EEDD6DDDDDDD
          DDDD8D888DDDDDDDDDDDEEEE6DDDDFCFCFCD8DDD8DDDD8F8F8FDDEEE6DDDFCDF
          CDFCD88D8DDD8FD8FD8FDDDE6DDDDDDFCDDDDDD88DDDDDD8FDDD}
        NumGlyphs = 2
        Margin = 0
      end
      object cbAnnually: TevCheckBox
        Left = 72
        Top = 16
        Width = 65
        Height = 17
        Caption = 'Ann'
        TabOrder = 0
        OnClick = cbTypeNotClick
      end
      object cbQuarterly: TevCheckBox
        Left = 120
        Top = 16
        Width = 65
        Height = 17
        Caption = 'Qtr'
        TabOrder = 1
        OnClick = cbTypeNotClick
      end
      object cbMonthly: TevCheckBox
        Left = 160
        Top = 16
        Width = 65
        Height = 17
        Caption = 'Mnth'
        TabOrder = 2
        OnClick = cbTypeNotClick
      end
      object cbSemiMonthly: TevCheckBox
        Left = 208
        Top = 16
        Width = 65
        Height = 17
        Caption = 'S-Mnth'
        TabOrder = 3
        OnClick = cbTypeNotClick
      end
      object evUseVMR: TevCheckBox
        Left = 8
        Top = 90
        Width = 97
        Height = 17
        Caption = 'Send to VMR'
        TabOrder = 18
      end
      object chbAutorefreshStatus: TevCheckBox
        Left = 288
        Top = 90
        Width = 153
        Height = 17
        Caption = 'Auto refresh lock status'
        Checked = True
        State = cbChecked
        TabOrder = 19
        OnClick = chbAutorefreshStatusClick
      end
    end
  end
  object PCMain: TevPageControl [1]
    Left = 0
    Top = 110
    Width = 819
    Height = 428
    ActivePage = tsListOption
    Align = alClient
    TabOrder = 1
    object tsListOption: TTabSheet
      Caption = 'Company filter'
      object tsht2CompFilterGroup: TevGroupBox
        Left = 0
        Top = 57
        Width = 811
        Height = 27
        Align = alTop
        TabOrder = 0
        object tsCompFilterGroupAll: TevRadioButton
          Left = 8
          Top = 8
          Width = 113
          Height = 17
          Caption = 'All companies'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = tsCompFilterGroupSelectClick
        end
        object tsCompFilterGroupSelect: TevRadioButton
          Left = 208
          Top = 8
          Width = 113
          Height = 17
          Caption = 'Select companies'
          TabOrder = 1
          OnClick = tsCompFilterGroupSelectClick
        end
      end
      object evPanel13: TevPanel
        Left = 0
        Top = 0
        Width = 811
        Height = 57
        Align = alTop
        TabOrder = 1
        object evLabel10: TevLabel
          Left = 5
          Top = 11
          Width = 104
          Height = 13
          Caption = 'Custom Client Number'
        end
        object evLabel11: TevLabel
          Left = 5
          Top = 35
          Width = 60
          Height = 13
          Caption = 'Client Name '
        end
        object evLabel14: TevLabel
          Left = 319
          Top = 35
          Width = 78
          Height = 13
          Caption = 'Company Name '
        end
        object evLabel13: TevLabel
          Left = 319
          Top = 11
          Width = 122
          Height = 13
          Caption = 'Custom Company Number'
        end
        object edCusClNbr: TevEdit
          Left = 123
          Top = 4
          Width = 185
          Height = 21
          TabOrder = 0
        end
        object edClName: TevEdit
          Left = 123
          Top = 27
          Width = 185
          Height = 21
          TabOrder = 1
        end
        object edCusCoNbr: TevEdit
          Left = 455
          Top = 5
          Width = 185
          Height = 21
          TabOrder = 2
        end
        object edCoName: TevEdit
          Left = 455
          Top = 29
          Width = 185
          Height = 21
          TabOrder = 3
        end
        object btnCompanyFilter: TevButton
          Left = 650
          Top = 24
          Width = 97
          Height = 25
          Caption = 'Company Filter'
          TabOrder = 4
          OnClick = btnCompanyFilterClick
          Color = clBlack
          Margin = 0
        end
      end
      object grdCompFilter: TevDBGrid
        Left = 0
        Top = 84
        Width = 811
        Height = 316
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\grdCompFilter'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsCompanyFilter
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 2
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tsTaxReturn: TTabSheet
      Caption = 'Tax Return'
      ImageIndex = 1
      object Pc: TevPageControl
        Left = 0
        Top = 0
        Width = 811
        Height = 400
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        OnChange = PcChange
        OnChanging = PcChanging
        object TabSheet1: TTabSheet
          Caption = 'General queue'
          object wwDBGrid3: TevDBGrid
            Left = 0
            Top = 0
            Width = 803
            Height = 372
            DisableThemesInTitle = False
            ControlType.Strings = (
              'TAX_SERVICE;CheckBox;Y;N'
              'SYSTEM_TAX_TYPE;CustomEdit;cbGrid3ReturnType'
              'SY_REPORTS_NBR;CustomEdit;cbGrid3ReportDescr'
              'STATUS;CustomEdit;cbGrid3Status'
              'RETURN_FREQUENCY;CustomEdit;cbGrid3Freq')
            Selected.Strings = (
              'CONSOLIDATION_DESCR'#9'1'#9'C'#9'F'
              'TAX_SERVICE'#9'1'#9'Tax'#9'F'
              'co_number'#9'7'#9'Comp. #'#9'F'
              'cl_number'#9'7'#9'Client #'#9'F'
              'CO_NAME'#9'15'#9'Company'#9'F'
              'PERIOD_END_DATE'#9'10'#9'Period end'#9'F'
              'SYSTEM_TAX_TYPE'#9'11'#9'Return type'#9'F'
              'SY_REPORTS_DESC'#9'20'#9'Report'#9'F'
              'AgencyName'#9'30'#9'Agency Name'#9'F'
              'STATUS'#9'10'#9'Status'#9'F'
              'STATUS_DATE'#9'10'#9'Status date'#9'F'
              'DUE_DATE'#9'10'#9'Due date'#9'F'
              'RETURN_FREQUENCY'#9'1'#9'Fr'#9'F'
              'AGENCY_COPY_PRINTED'#9'1'#9'A'#9'F'
              'CLIENT_COPY_PRINTED'#9'1'#9'C'#9'F'
              'SB_COPY_PRINTED'#9'1'#9'S'#9'F'
              'PRIORITY2'#9'1'#9'Pr'#9'F'
              'taken'#9'10'#9'Taken by'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\wwDBGrid3'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsQueue
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgShowCellHint, dgDblClickColSizing]
            ParentShowHint = False
            ShowHint = False
            TabOrder = 3
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnCalcCellColors = wwDBGrid3CalcCellColors
            OnDblClick = wwDBGrid3DblClick
            OnCreateHintWindow = wwDBGrid3CreateHintWindow
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object cbGrid3ReturnType: TevDBComboBox
            Left = 464
            Top = 40
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'SYSTEM_TAX_TYPE'
            DataSource = dsQueue
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 0
            UnboundDataType = wwDefault
          end
          object cbGrid3Status: TevDBComboBox
            Left = 464
            Top = 64
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATUS'
            DataSource = dsQueue
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
          end
          object cbGrid3Freq: TevDBComboBox
            Left = 464
            Top = 88
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'RETURN_FREQUENCY'
            DataSource = dsQueue
            DropDownCount = 8
            ItemHeight = 13
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
            Visible = False
          end
        end
        object tsCompanies: TTabSheet
          Caption = 'Summary by company'
          ImageIndex = 6
          object evPanel11: TevPanel
            Left = 0
            Top = 453
            Width = 925
            Height = 26
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object Button1: TevButton
              Left = 6
              Top = 4
              Width = 75
              Height = 21
              Caption = 'Add to queue'
              TabOrder = 0
              OnClick = Button1Click
              Color = clBlack
              Margin = 0
            end
          end
          object evPanel12: TevPanel
            Left = 0
            Top = 0
            Width = 925
            Height = 453
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object wwDBGrid5: TevDBGrid
              Left = 0
              Top = 0
              Width = 925
              Height = 453
              DisableThemesInTitle = False
              ControlType.Strings = (
                'TAX_SERVICE;CheckBox;Y;N'
                'processed;CheckBox;True;False'
                'onhold;CheckBox;True;False')
              Selected.Strings = (
                'CONSOLIDATION'#9'1'#9'C'#9'F'
                'co_number'#9'10'#9'Comp. #'#9'F'
                'CO_NAME'#9'20'#9'Company'#9'F'
                'TAX_SERVICE'#9'1'#9'Tax'#9'F'
                'processed'#9'5'#9'Processed'#9'F'
                'onhold'#9'5'#9'Onhold'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\wwDBGrid5'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Align = alClient
              DataSource = dsSummary
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgShowCellHint, dgDblClickColSizing]
              TabOrder = 0
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnCreateHintWindow = wwDBGrid3CreateHintWindow
              PaintOptions.AlternatingRowColor = clCream
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
          end
        end
        object tsDetail: TTabSheet
          Caption = 'Details'
          Enabled = False
          ImageIndex = 1
          object Label8: TevLabel
            Left = 8
            Top = 48
            Width = 53
            Height = 16
            Caption = '~Company'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label10: TevLabel
            Left = 8
            Top = 276
            Width = 39
            Height = 16
            Caption = '~Status'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label7: TevLabel
            Left = 8
            Top = 24
            Width = 35
            Height = 16
            Caption = '~Client'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label11: TevLabel
            Left = 8
            Top = 72
            Width = 79
            Height = 16
            Caption = '~Report agency'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label12: TevLabel
            Left = 8
            Top = 96
            Width = 92
            Height = 16
            Caption = '~Report field office'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label15: TevLabel
            Left = 8
            Top = 244
            Width = 84
            Height = 16
            Caption = '~Period end date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label13: TevLabel
            Left = 8
            Top = 316
            Width = 106
            Height = 16
            Caption = '~Agency copy printed'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label16: TevLabel
            Left = 192
            Top = 316
            Width = 96
            Height = 16
            Caption = '~Client copy printed'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label17: TevLabel
            Left = 376
            Top = 316
            Width = 89
            Height = 16
            Caption = '~S.b. copy printed'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label18: TevLabel
            Left = 296
            Top = 244
            Width = 53
            Height = 16
            Caption = '~Due date'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label14: TevLabel
            Left = 8
            Top = 300
            Width = 54
            Height = 13
            Caption = 'Status date'
          end
          object evLabel1: TevLabel
            Left = 8
            Top = 340
            Width = 91
            Height = 13
            Caption = 'Produce magmedia'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object cbDetailCo: TevDBLookupCombo
            Left = 128
            Top = 48
            Width = 361
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'company_name'#9'30'#9'Company name'#9'F'
              'custom_company_number'#9'20'#9'Custom company #'#9'F'
              'tax_service'#9'1'#9'Tax service'#9'F'
              'Cons'#9'1'#9'Cons'#9'F')
            DataField = 'CO_NBR'
            DataSource = dsQueue
            LookupField = 'CO_NBR'
            Options = [loTitles]
            Style = csDropDownList
            TabOrder = 1
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnChange = cbDetailCoChange
          end
          object cbDetailStatus: TevDBComboBox
            Left = 128
            Top = 268
            Width = 361
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'STATUS'
            DataSource = dsQueue
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 7
            UnboundDataType = wwDefault
          end
          object cbDetailCl: TevDBLookupCombo
            Left = 128
            Top = 24
            Width = 361
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'name'#9'30'#9'Client name'#9'F'
              'custom_client_number'#9'20'#9'Custom client #'#9'F')
            DataField = 'CL_NBR'
            DataSource = dsQueue
            LookupField = 'cl_nbr'
            Style = csDropDownList
            TabOrder = 0
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnCloseUp = cbDetailClCloseUp
          end
          object cbDetailAgCopyPrinted: TDBCheckBox
            Left = 136
            Top = 316
            Width = 17
            Height = 17
            DataField = 'AGENCY_COPY_PRINTED'
            DataSource = dsQueue
            TabOrder = 9
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object cbDetailClCopyPrinted: TDBCheckBox
            Left = 312
            Top = 316
            Width = 25
            Height = 17
            DataField = 'CLIENT_COPY_PRINTED'
            DataSource = dsQueue
            TabOrder = 10
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object cbDetailSbCopyPrinted: TDBCheckBox
            Left = 488
            Top = 316
            Width = 17
            Height = 17
            DataField = 'SB_COPY_PRINTED'
            DataSource = dsQueue
            TabOrder = 11
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object cdDetailPeriodEndDate: TevDBDateTimePicker
            Left = 128
            Top = 244
            Width = 121
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            Color = clBtnFace
            DataField = 'PERIOD_END_DATE'
            DataSource = dsQueue
            Epoch = 1950
            ReadOnly = True
            ShowButton = True
            TabOrder = 5
            OnCloseUp = cdDetailPeriodEndDateCloseUp
            OnChange = cdDetailPeriodEndDateChange
            OnExit = cdDetailPeriodEndDateExit
          end
          object cbDetailDueDate: TevDBDateTimePicker
            Left = 368
            Top = 244
            Width = 121
            Height = 21
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            CalendarAttributes.PopupYearOptions.StartYear = 2000
            Color = clBtnFace
            DataField = 'DUE_DATE'
            DataSource = dsQueue
            Epoch = 1950
            ReadOnly = True
            ShowButton = True
            TabOrder = 6
          end
          object cbDetailStatusDate: TevDBEdit
            Left = 128
            Top = 292
            Width = 153
            Height = 21
            TabStop = False
            Color = clBtnFace
            DataField = 'STATUS_DATE'
            DataSource = dsQueue
            ReadOnly = True
            TabOrder = 8
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
            Glowing = False
          end
          object btnOk: TevBitBtn
            Left = 326
            Top = 353
            Width = 75
            Height = 25
            Caption = 'Save'
            TabOrder = 13
            OnClick = btnOkClick
            Color = clBlack
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD8888
              8888DDDDDDDDFFFFFFFFDDDDDDD8C7477CC8DDDDDDDF8D8DD88F0000000CC777
              7CC8888888D88DDDD88F7787778CCCCCCCC8DD8DDDD88888888F7787778C7777
              77C8DD8DDDD8DDDDDD8F7777777C7FFFFFC8DDDDDDD8DDDDDD8F7777777C7FFF
              FFC8DDDDDDD8DDDDDD8F7777777C7FFFFFC8DDDDDDD8DDDDDD8F7770667CCCCC
              CCC7DDD8D8D88888888D7770776676767766DDD8DD8DDDDDDDDD780007777666
              7777DD888DDDD888DDDD7077707777777777D88888DDDDDDDDDD707778077787
              7787D888888DDD8DDD8D00FFF88000000000D8DDD888D8888888DD0777880DDD
              DDDDDD8888888DDDDDDDDDD0777880DDDDDDDDD8888888DDDDDD}
            NumGlyphs = 2
            Margin = 0
          end
          object btnCancel: TevBitBtn
            Left = 414
            Top = 353
            Width = 75
            Height = 25
            Caption = 'Cancel'
            TabOrder = 14
            OnClick = btnCancelClick
            Color = clBlack
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
              DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
              9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
              DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
              DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
              DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
              91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
              999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
            NumGlyphs = 2
            Margin = 0
          end
          object mEditMode: TevMemo
            Left = 504
            Top = 329
            Width = 169
            Height = 68
            BorderStyle = bsNone
            Color = clBtnFace
            Lines.Strings = (
              'You either have  to save or '
              'cancel  changes before you '
              'can switch to another tab or '
              'use top part of screen.')
            ReadOnly = True
            TabOrder = 16
          end
          object DetailMagmedia: TevDBCheckBox
            Left = 136
            Top = 340
            Width = 17
            Height = 17
            DataField = 'PRODUCE_ASCII_FILE'
            DataSource = dsQueue
            Enabled = False
            TabOrder = 12
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object cbConsol: TevCheckBox
            Left = 504
            Top = 52
            Width = 97
            Height = 17
            Caption = 'Consolidation'
            TabOrder = 15
          end
          object bMultiEnlist: TevBitBtn
            Left = 504
            Top = 24
            Width = 75
            Height = 25
            Caption = 'Multiple'
            TabOrder = 17
            OnClick = bMultiEnlistClick
            Color = clBlack
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD888888
              888DDDDDDDFFFFFFFFFDDDDDDD8000000000DDDDDDF888888888DDDDDD80FFFF
              FFF0DDDDDDF8DDDDDDD866666660FFFFFFF0888888F8DDDDDDD8666EEE60F888
              88F0888DDDF8D88888D866666660FFFFFFF0888888F8DDDDDDD8666EEE60F888
              88F0888DDDF8D88888D866666660FFFFFFF0888888F8DDDDDDD8666EEE60F888
              88F0888DDDF8D88888D866666660FFFFFFF0888888F8DDDDDDD8DDDD6D80FFFF
              FFF0DDDD8DF8DDDDDDD8DDDD6DD00000000088DD8DD888888888EEDD6DDDDDDD
              DDDD8D888DDDDDDDDDDDEEEE6DDDDFCFCFCD8DDD8DDDD8F8F8FDDEEE6DDDFCDF
              CDFCD88D8DDD8FD8FD8FDDDE6DDDDDDFCDDDDDD88DDDDDD8FDDD}
            NumGlyphs = 2
            Margin = 0
          end
          object grdFieldOfficeReports: TevDBGrid
            Left = 128
            Top = 124
            Width = 360
            Height = 112
            DisableThemesInTitle = False
            Selected.Strings = (
              'description'#9'60'#9'Description'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\grdFieldOfficeReports'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsFieldOfficeReports
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgShowCellHint, dgDblClickColSizing]
            TabOrder = 4
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object cbDetailReportAgency: TevDBLookupCombo
            Left = 128
            Top = 72
            Width = 361
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'AGENCY_NAME'#9'30'#9'Name'#9'F')
            DataField = 'SY_GLOBAL_AGENCY_NBR'
            DataSource = dsQueue
            LookupField = 'SY_GLOBAL_AGENCY_NBR'
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnCloseUp = cbDetailReportAgencyCloseUp
          end
          object cbDetailReportFieldOffice: TevDBLookupCombo
            Left = 128
            Top = 96
            Width = 361
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'ADDITIONAL_NAME'#9'30'#9'Name'#9'F')
            DataField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
            DataSource = dsQueue
            LookupField = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
            Style = csDropDownList
            TabOrder = 3
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = False
            OnCloseUp = cbDetailReportFieldOfficeCloseUp
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Edit queue'
          ImageIndex = 2
          object Splitter2: TSplitter
            Left = 0
            Top = 150
            Width = 803
            Height = 3
            Cursor = crVSplit
            Align = alBottom
          end
          object Panel5: TPanel
            Left = 0
            Top = 153
            Width = 803
            Height = 219
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            DesignSize = (
              803
              219)
            object BitBtn3: TevBitBtn
              Left = 0
              Top = 2
              Width = 33
              Height = 25
              TabOrder = 0
              TabStop = False
              OnClick = BitBtn3Click
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000010000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
                DDDDDDDDDDFDDDDDDDDDDDDDD868DDDDDDDDDDDDDF8FDDDDDDDDDDDD86E68DDD
                DDDDDDDDF888FDDDDDDDDDD86EEE68DDDDDDDDDF88888FDDDDDDDD86EEEEE68D
                DDDDDDF8888888FDDDDDD86EEEEEEE68DDDDDF888888888FDDDD86EEEEEEEEE6
                8DDDF88888888888FDDD6666EEEEE6666DDD8888888888888DDDDDD6EEEEE6DD
                DDDDDDD8888888DDDDDDDDD6E6666688888DDDD8888888FFFFFFDDD6E6000000
                0000DDD8888888888888DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6E60FFFFF
                FFF0DDD8888DDDDDDDD8DDD6EE0000000000DDD8888888888888DDD6EEEEE6DD
                DDDDDDD8888888DDDDDDDDD6666666DDDDDDDDD8888888DDDDDD}
              NumGlyphs = 2
              Margin = 0
            end
            object BitBtn2: TevBitBtn
              Left = 40
              Top = 2
              Width = 33
              Height = 25
              TabOrder = 1
              TabStop = False
              OnClick = BitBtn2Click
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD8888888DD
                DDDDDDDFFFFFFFDDDDDDDDD6666666DDDDDDDDD8888888DDDDDDDDD6E6666688
                888DDDD8888888FFFFFFDDD6E60000000000DDD8888888888888DDD6E60FFFFF
                FFF0DDD8888DDDDDDDD8DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6EE000000
                0000DDD8888888888888DDD6EEEEE6DDDDDDDDD8888888DDDDDD8886EEEEE688
                8DDDFFF8888888FFFDDD6666EEEEE6666DDD8888888888888DDDD6EEEEEEEEE6
                DDDDD88888888888DDDDDD6EEEEEEE6DDDDDDD888888888DDDDDDDD6EEEEE6DD
                DDDDDDD8888888DDDDDDDDDD6EEE6DDDDDDDDDDD88888DDDDDDDDDDDD6E6DDDD
                DDDDDDDDD888DDDDDDDDDDDDDD6DDDDDDDDDDDDDDD8DDDDDDDDD}
              NumGlyphs = 2
              Margin = 0
            end
            object BitBtn4: TevBitBtn
              Left = 98
              Top = 2
              Width = 33
              Height = 25
              TabOrder = 2
              TabStop = False
              OnClick = BitBtn4Click
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
                DDDDDDDDDDFDDDDDDDDDDDDDD868DDDDDDDDDDDDDF8FDDDDDDDDDDDD86E68DDD
                DDDDDDDDF888FDDDDDDDDDD86EEE68DDDDDDDDDF88888FDDDDDDDD86E6666888
                888DDDF8888888FFFFFFD86EE60000000000DF8888888888888886EEE60FFFFF
                FFF0F888888DDDDDDDD86666E60FFFFFFFF08888888DDDDDDDD8DDD6E6000000
                0000DDD8888888888888DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6E60FFFFF
                FFF0DDD8888DDDDDDDD8DDD6E60000000000DDD8888888888888DDD6E60FFFFF
                FFF0DDD8888DDDDDDDD8DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6EE000000
                0000DDD8888888888888DDD6666666DDDDDDDDD8888888DDDDDD}
              NumGlyphs = 2
              Margin = 0
            end
            object BitBtn1: TevBitBtn
              Left = 138
              Top = 2
              Width = 33
              Height = 25
              TabOrder = 3
              TabStop = False
              OnClick = BitBtn1Click
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD8888888DD
                DDDDDDDFFFFFFFDDDDDDDDD6666666DDDDDDDDD8888888DDDDDDDDD6E6666688
                888DDDD8888888FFFFFFDDD6E60000000000DDD8888888888888DDD6E60FFFFF
                FFF0DDD8888DDDDDDDD8DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6E6000000
                0000DDD8888888888888DDD6E60FFFFFFFF0DDD8888DDDDDDDD88886E60FFFFF
                FFF0FFF8888DDDDDDDD86666E600000000008888888888888888D6EEE60FFFFF
                FFF0D888888DDDDDDDD8DD6EE60FFFFFFFF0DD88888DDDDDDDD8DDD6EE000000
                0000DDD8888888888888DDDD6EEE6DDDDDDDDDDD88888DDDDDDDDDDDD6E6DDDD
                DDDDDDDDD888DDDDDDDDDDDDDD6DDDDDDDDDDDDDDD8DDDDDDDDD}
              NumGlyphs = 2
              Margin = 0
            end
            object wwDBGrid2: TevDBGrid
              Left = 0
              Top = 28
              Width = 15276
              Height = 191
              DisableThemesInTitle = False
              ControlInfoInDataSet = True
              ControlType.Strings = (
                'TAX_SERVICE;CheckBox;Y;N')
              Selected.Strings = (
                'CONSOLIDATION_DESCR'#9'1'#9'C'#9'F'
                'TAX_SERVICE'#9'1'#9'Tax'#9'F'
                'co_number'#9'7'#9'Comp. #'#9'F'
                'cl_number'#9'7'#9'Client #'#9'F'
                'CO_NAME'#9'15'#9'Company'#9'F'
                'PERIOD_END_DATE'#9'10'#9'Period end'#9'F'
                'SYSTEM_TAX_TYPE'#9'11'#9'Return type'#9'F'
                'SY_REPORTS_DESC'#9'20'#9'Report'#9'F'
                'AgencyName'#9'30'#9'Agency Name'#9'F'
                'STATUS'#9'10'#9'Status'#9'F'
                'STATUS_DATE'#9'10'#9'Status date'#9'F'
                'DUE_DATE'#9'10'#9'Due date'#9'F'
                'RETURN_FREQUENCY'#9'1'#9'Fr'#9'F'
                'AGENCY_COPY_PRINTED'#9'1'#9'A'#9'F'
                'CLIENT_COPY_PRINTED'#9'1'#9'C'#9'F'
                'SB_COPY_PRINTED'#9'1'#9'S'#9'F'
                'PRIORITY2'#9'10'#9'Pr'#9'F')
              IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
              IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\wwDBGrid2'
              IniAttributes.Delimiter = ';;'
              ExportOptions.ExportType = wwgetSYLK
              ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
              TitleColor = clBtnFace
              FixedCols = 0
              ShowHorzScrollBar = True
              Anchors = [akLeft, akTop, akRight, akBottom]
              DataSource = dsSelectedQueue
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgShowCellHint, dgDblClickColSizing]
              TabOrder = 4
              TitleAlignment = taLeftJustify
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              TitleLines = 1
              OnCalcCellColors = wwDBGrid2CalcCellColors
              OnDblClick = wwDBGrid2DblClick
              OnCreateHintWindow = wwDBGrid3CreateHintWindow
              PaintOptions.AlternatingRowColor = clCream
              PaintOptions.ActiveRecordColor = clBlack
              NoFire = False
            end
            object cbGrid2Freq: TevDBComboBox
              Left = 448
              Top = 56
              Width = 121
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 5
              UnboundDataType = wwDefault
              Visible = False
            end
            object cbGrid2Status: TevDBComboBox
              Left = 448
              Top = 80
              Width = 121
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 6
              UnboundDataType = wwDefault
            end
            object cbGrid2ReturnType: TevDBComboBox
              Left = 448
              Top = 104
              Width = 121
              Height = 21
              ShowButton = True
              Style = csDropDownList
              MapList = True
              AllowClearKey = False
              AutoDropDown = True
              DropDownCount = 8
              ItemHeight = 0
              Picture.PictureMaskFromDataSet = False
              Sorted = False
              TabOrder = 7
              UnboundDataType = wwDefault
            end
          end
          object wwDBGrid1: TevDBGrid
            Left = 0
            Top = 0
            Width = 803
            Height = 150
            DisableThemesInTitle = False
            ControlType.Strings = (
              'SYSTEM_TAX_TYPE;CustomEdit;cbGrid1Type'
              'STATUS;CustomEdit;cbGrid1Status'
              'RETURN_FREQUENCY;CustomEdit;cbGrid1Freq'
              'TAX_SERVICE;CheckBox;Y;N')
            Selected.Strings = (
              'CONSOLIDATION_DESCR'#9'1'#9'C'#9'F'
              'TAX_SERVICE'#9'1'#9'Tax'#9'F'
              'co_number'#9'7'#9'Comp. #'#9'F'
              'cl_number'#9'7'#9'Client #'#9'F'
              'CO_NAME'#9'15'#9'Company'#9'F'
              'PERIOD_END_DATE'#9'10'#9'Period end'#9'F'
              'SYSTEM_TAX_TYPE'#9'11'#9'Return type'#9'F'
              'SY_REPORTS_DESC'#9'20'#9'Report'#9'F'
              'AgencyName'#9'30'#9'Agency Name'#9'F'
              'STATUS'#9'10'#9'Status'#9'F'
              'STATUS_DATE'#9'10'#9'Status date'#9'F'
              'DUE_DATE'#9'10'#9'Due date'#9'F'
              'RETURN_FREQUENCY'#9'1'#9'Fr'#9'F'
              'AGENCY_COPY_PRINTED'#9'1'#9'A'#9'F'
              'CLIENT_COPY_PRINTED'#9'1'#9'C'#9'F'
              'SB_COPY_PRINTED'#9'1'#9'S'#9'F'
              'PRIORITY2'#9'1'#9'Pr'#9'F'
              'taken'#9'10'#9'Taken by'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\wwDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsQueue
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgShowCellHint, dgDblClickColSizing]
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnCalcCellColors = wwDBGrid3CalcCellColors
            OnDblClick = wwDBGrid1DblClick
            OnCreateHintWindow = wwDBGrid3CreateHintWindow
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object cbGrid1Freq: TevDBComboBox
            Left = 456
            Top = 48
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
            Visible = False
          end
          object cbGrid1Status: TevDBComboBox
            Left = 456
            Top = 72
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object cbGrid1Type: TevDBComboBox
            Left = 456
            Top = 96
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
        end
        object tsEe: TTabSheet
          Caption = 'EE select'
          ImageIndex = 7
          TabVisible = False
          object lEeClient: TevLabel
            Left = 0
            Top = 0
            Width = 41
            Height = 13
            Caption = 'lEeClient'
          end
          object lEeCompany: TevLabel
            Left = 0
            Top = 16
            Width = 59
            Height = 13
            Caption = 'lEeCompany'
          end
          object lEeCons: TevLabel
            Left = 0
            Top = 32
            Width = 39
            Height = 13
            Caption = 'lEeCons'
          end
          object lEeStatusDate: TLabel
            Left = 576
            Top = 35
            Width = 88
            Height = 13
            Caption = 'Since 11/11/1122'
          end
          object evDBGrid1: TevDBGrid
            Left = 0
            Top = 56
            Width = 681
            Height = 313
            DisableThemesInTitle = False
            ControlType.Strings = (
              'SELECTED;CheckBox;True;False'
              'CURRENT_TERMINATION_CODE;CustomEdit;cbEEStatus;F')
            Selected.Strings = (
              'CUSTOM_EMPLOYEE_NUMBER'#9'9'#9'Custom Employee Number'#9#9
              'LAST_NAME'#9'20'#9'Last Name'#9#9
              'FIRST_NAME'#9'15'#9'First Name'#9#9
              
                'CURRENT_TERMINATION_CODE'#9'1'#9'Status                               ' +
                '          '#9'F'
              'SELECTED'#9'5'#9'Selected'#9#9)
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\evDBGrid1'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
            DataSource = dsEe
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnExit = evDBGrid1Exit
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object bEeMarkAll: TevButton
            Left = 400
            Top = 8
            Width = 75
            Height = 25
            Caption = 'Select all'
            TabOrder = 1
            OnClick = bEeMarkAllClick
            Color = clBlack
            Margin = 0
          end
          object bEeClearAll: TevButton
            Left = 488
            Top = 8
            Width = 75
            Height = 25
            Caption = 'Unselect all'
            TabOrder = 2
            OnClick = bEeMarkAllClick
            Color = clBlack
            Margin = 0
          end
          object bEeMarkChanged: TevButton
            Left = 576
            Top = 8
            Width = 75
            Height = 25
            Caption = 'Only changed'
            TabOrder = 3
            OnClick = bEeMarkChangedClick
            Color = clBlack
            Margin = 0
          end
          object cbEEStatus: TevDBComboBox
            Left = 416
            Top = 240
            Width = 169
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'CURRENT_TERMINATION_CODE'
            DataSource = dsEe
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 4
            UnboundDataType = wwDefault
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Printing settings'
          ImageIndex = 4
          object Label20: TevLabel
            Left = 8
            Top = 240
            Width = 223
            Height = 13
            Caption = 'Path for magmedia files (if any will be produced)'
          end
          object evLabel3: TevLabel
            Left = 368
            Top = 16
            Width = 106
            Height = 13
            Caption = 'Print additional copies:'
          end
          object Label9: TLabel
            Left = 8
            Top = 339
            Width = 30
            Height = 13
            Caption = 'Printer'
          end
          object cbSettingsPrintAgencyCopy: TevCheckBox
            Left = 8
            Top = 16
            Width = 121
            Height = 17
            Caption = 'Print agency copy'
            TabOrder = 0
          end
          object cbSettingsPrintClientCopy: TevCheckBox
            Left = 8
            Top = 40
            Width = 121
            Height = 17
            Caption = 'Print client copy'
            TabOrder = 1
          end
          object cbSettingsPrintSbCopy: TevCheckBox
            Left = 8
            Top = 64
            Width = 145
            Height = 17
            Caption = 'Print service bureau copy'
            TabOrder = 2
          end
          object cbSettingsPrintOrder: TevRadioGroup
            Left = 8
            Top = 104
            Width = 329
            Height = 65
            Caption = 'Sort by'
            ItemIndex = 0
            Items.Strings = (
              '1st copy of all reports, then 2nd copy of all reports if needed'
              'All copies of one report, then all copies of next report...')
            TabOrder = 3
          end
          object cbSettingsCredit: TevRadioGroup
            Left = 8
            Top = 176
            Width = 329
            Height = 57
            Caption = 'If you have tax credit'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Apply to next qtr.'
              'Ask for refund')
            TabOrder = 4
          end
          object cbSettingsMagmediaPath: TevEdit
            Left = 8
            Top = 256
            Width = 329
            Height = 21
            TabOrder = 5
            Text = 'C:\Magmedia\'
          end
          object cbSeparate: TevCheckBox
            Left = 368
            Top = 104
            Width = 177
            Height = 17
            Caption = 'Separate parts with blank sheet'
            Checked = True
            State = cbChecked
            TabOrder = 7
          end
          object cbRefreshQueue: TevCheckBox
            Left = 368
            Top = 128
            Width = 209
            Height = 17
            Caption = 'Refresh queue before and after print'
            Checked = True
            State = cbChecked
            TabOrder = 8
            Visible = False
          end
          object Memo1: TMemo
            Left = 384
            Top = 144
            Width = 177
            Height = 33
            BorderStyle = bsNone
            Color = clBtnFace
            Lines.Strings = (
              '(Recommended if you print from two '
              'or more computers)')
            ReadOnly = True
            TabOrder = 9
            Visible = False
          end
          object cbEeSort: TevRadioGroup
            Left = 368
            Top = 175
            Width = 313
            Height = 41
            Caption = 'Sort EE W-2'#39's By'
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Last name'
              'SSN')
            TabOrder = 10
          end
          object edCopies: TevSpinEdit
            Left = 488
            Top = 16
            Width = 49
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 6
            Value = 0
          end
          object cbWagesFilter: TevRadioGroup
            Left = 368
            Top = 264
            Width = 321
            Height = 81
            Caption = 'Filter by Taxable Wages for Magmedia'
            ItemIndex = 1
            Items.Strings = (
              'All'
              'With Wages'
              'With No Wages')
            TabOrder = 12
          end
          object cbW2Sort: TevRadioGroup
            Left = 368
            Top = 216
            Width = 313
            Height = 41
            Caption = 'Sort W-2'#39's By'
            Columns = 5
            ItemIndex = 0
            Items.Strings = (
              'None'
              'Division'
              'Branch'
              'Departm'
              'Team')
            TabOrder = 11
          end
          object cbHideSSN: TevCheckBox
            Left = 168
            Top = 16
            Width = 121
            Height = 17
            Caption = 'Hide SSN'
            TabOrder = 13
          end
          object cbEMail: TevCheckBox
            Left = 8
            Top = 288
            Width = 177
            Height = 17
            Alignment = taLeftJustify
            Caption = 'Pre-Process E-Mail Notification'
            TabOrder = 14
            OnClick = cbEMailClick
          end
          object eEMail: TevEdit
            Left = 8
            Top = 308
            Width = 157
            Height = 21
            Enabled = False
            TabOrder = 15
          end
          object cbEmailSendRule: TevComboBox
            Left = 169
            Top = 308
            Width = 168
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 16
            Items.Strings = (
              'Always'
              'Successful'
              'Exceptions'
              'Exceptions/Warnings')
          end
          object cbPrinter: TevComboBox
            Left = 8
            Top = 358
            Width = 329
            Height = 21
            BevelKind = bkFlat
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 17
            Items.Strings = (
              'Only Reports with Duplexing'
              'Always'
              'Never')
          end
        end
        object tsSimple: TTabSheet
          Caption = 'Brief queue view (paper)'
          ImageIndex = 8
          object Panel1: TPanel
            Left = 0
            Top = 309
            Width = 803
            Height = 63
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object evLabel2: TevLabel
              Left = 8
              Top = 12
              Width = 74
              Height = 13
              Caption = 'Q.E.C. adj. limit:'
            end
            object evLabel5: TevLabel
              Left = 8
              Top = 40
              Width = 45
              Height = 13
              Caption = 'Log path:'
            end
            object EvBevel2: TEvBevel
              Left = 439
              Top = 8
              Width = 10
              Height = 50
              Shape = bsLeftLine
            end
            object EvBevel4: TEvBevel
              Left = 575
              Top = 8
              Width = 10
              Height = 50
              Shape = bsLeftLine
            end
            object EvBevel3: TEvBevel
              Left = 687
              Top = 8
              Width = 10
              Height = 50
              Shape = bsLeftLine
            end
            object edQeCleanupLimit: TevEdit
              Left = 89
              Top = 9
              Width = 49
              Height = 21
              TabOrder = 0
              Text = '0.10'
              OnChange = edQeCleanupLimitChange
            end
            object edQeCleanupReports: TevCheckBox
              Left = 153
              Top = 12
              Width = 77
              Height = 17
              Caption = 'PR Reports'
              TabOrder = 1
            end
            object edQePath: TevEdit
              Left = 64
              Top = 37
              Width = 259
              Height = 21
              TabOrder = 2
              Text = '-'
              OnChange = edQeCleanupLimitChange
            end
            object cbFilter: TevComboBox
              Left = 448
              Top = 8
              Width = 121
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 3
              OnChange = cbFilterChange
              Items.Strings = (
                'Show all'
                'Show reds'
                'Show greens'
                'Show cyan'
                'Show whites')
            end
            object bPreProcess: TevBitBtn
              Left = 232
              Top = 8
              Width = 89
              Height = 25
              Caption = 'Pre-Process'
              TabOrder = 4
              OnClick = bOneStepPreProcessClick
              OnMouseDown = bPreProcessMouseDown
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD6DD6DDDDD
                D88DDDDDDDDDDDDDDDDDDD6E66E6DDDD8CC8DDD8DD8DDDDD88FDDDEEEEEEDDD8
                CCCDDD888888DDD888FDD66EEEE6688CCC88DDD8888DDD888FDDDEEE686668CC
                C7F8D888DDDDD888F88DDEEE66CCC4CC78F8D888D888888FDD8DDD6E6C867C47
                FFF8DDDD8FDD88F8888DDDEEC6EE78C788F8DDD8FD8DD8FDDD8DD33EC8E777C7
                FFF8DDD8FD8DD8F8888DDBB3C87888C788F8D8D8FDDDD8FDDD8D33BB3C888C7F
                FFF8DD8D8FDD8F88888DBBB3D7CCC78888F8888DD888FDDDDD8DBBB33F777FFF
                FFF8888DDDDD8888888DD3BB3FF8888888F8DD88D88DDDDDDD8DDBBB3FFFFFFF
                FFF8D888D8888888888DDDBD3FFFFFFFFFFDDD8DD8888888888D}
              NumGlyphs = 2
              Margin = 0
            end
            object bProcess: TevBitBtn
              Tag = 1
              Left = 592
              Top = 8
              Width = 81
              Height = 25
              Caption = 'Process'
              TabOrder = 5
              OnClick = bProcessClick
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD0DD0DDDDD
                DDDDDDDFDDFDDDDDDDDDDD0E00E0DDDDDDDDDDF8FF8FDDDDDDDDDDEEEEEE8888
                8888DD888888FFFFFFFFD00EEEE007FFFFF8DFF8888FFD88888FDEEE07EEE7FF
                FFF8D888FD888D88888FDEEE00EEE74444F8D888FF888DDDDD8FDD0EEEE07FFF
                FFF8DDF8888FD888888FDDEEEEEE744444F8DD888888DDDDDD8FD03E77E7FFFF
                FFF8DFD8DD8D8888888FDBB33F74444444F8D88DD8DDDDDDDD8F00BB3FFFFFFF
                FFF8FF88D8888888888FBBB0DFF4444444F8888FD88DDDDDDD8FBBB00FFFFFFF
                FFF8888FF8888888888FD0BB3FF4444444F8DF88D88DDDDDDD8FDBBB3FFFFFFF
                FFF8D888D8888888888FDDBD3FFFFFFFFFFDDD8DD8888888888D}
              NumGlyphs = 2
              Margin = 0
            end
            object bOneStepRePrint: TevBitBtn
              Left = 704
              Top = 8
              Width = 81
              Height = 25
              Caption = 'Reprint'
              TabOrder = 6
              OnClick = bOneStepRePrintClick
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD28DDDD
                DDDDDDDDDDFDDDDDDDDDDDDDD2A28DDDDDDDDDDDDF8FDDDDDDDDDDDD2AAA28DD
                DDDDDDDDF888FDDDDDDDDDD2AAAA28DDDDDDDDDF8888FDDDDDDDDD2AAAAAA200
                000DDDF888888F88888DD2AA882AA2877778DF88DDD88FDDDDD82A88888AAA28
                8778F8DD88D888F88DD8D8888778AA287778DD888DDD88FDDDD8D8888712AAA2
                8778D8888D8D888FDDD8D88887778AA28778D8888DDDD88FDDD8D88888888AAA
                2888D8888888D888F888D888877778AA2878D8888DDDDD88F8D8D88878FFF7A2
                878DD888D8DDDD8F8D8DD8870000000878DDD88D888888D8D8DDD87777777777
                8DDDD8DDDDDDDDDD8DDDDD8888888888DDDDDD8888888888DDDD}
              NumGlyphs = 2
              Margin = 0
            end
            object bManualRecon: TevBitBtn
              Left = 328
              Top = 8
              Width = 106
              Height = 25
              Caption = 'Manual Reconciled'
              TabOrder = 7
              OnClick = bManualReconClick
              Color = clBlack
              NumGlyphs = 2
              Margin = 0
            end
          end
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 803
            Height = 309
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Splitter1: TSplitter
              Left = 361
              Top = 0
              Height = 309
            end
            object Panel3: TPanel
              Left = 0
              Top = 0
              Width = 361
              Height = 309
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object gOneStepCoList: TevDBGrid
                Left = 0
                Top = 14
                Width = 361
                Height = 169
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'TAX_SERVICE;CheckBox;Y;N'
                  'paper_processed;CheckBox;True;False'
                  'onhold;CheckBox;True;False')
                Selected.Strings = (
                  'CONSOLIDATION'#9'1'#9'C'#9'F'
                  'co_number'#9'10'#9'Comp. #'#9'F'
                  'CO_NAME'#9'20'#9'Company'#9'F'
                  'TAX_SERVICE'#9'1'#9'Tax'#9'F'
                  'paper_processed'#9'5'#9'Processed'#9'F'
                  'onhold'#9'5'#9'Onhold'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\gOneStepCoList'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsSummary
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgShowCellHint, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                OnCalcCellColors = gOneStepCoListCalcCellColors
                OnDblClick = gOneStepCoListDblClick
                OnCreateHintWindow = wwDBGrid3CreateHintWindow
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object evPanel10: TevPanel
                Left = 0
                Top = 0
                Width = 361
                Height = 14
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object evLabel4: TevLabel
                  Left = 0
                  Top = 0
                  Width = 52
                  Height = 13
                  Caption = 'Companies'
                end
              end
            end
            object Panel4: TPanel
              Left = 364
              Top = 0
              Width = 439
              Height = 309
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object evDBGrid2: TevDBGrid
                Left = 0
                Top = 14
                Width = 439
                Height = 295
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'STATUS;CustomEdit;cbBriefStatus'
                  'SB_COPY_PRINTED;CheckBox;Y;N'
                  'CLIENT_COPY_PRINTED;CheckBox;Y;N'
                  'AGENCY_COPY_PRINTED;CheckBox;Y;N')
                Selected.Strings = (
                  'SY_REPORTS_DESC'#9'20'#9'Descr.'#9'F'
                  'PERIOD_END_DATE'#9'10'#9'Period end'#9'F'
                  'STATUS'#9'10'#9'Status'#9'F'
                  'SB_COPY_PRINTED'#9'1'#9'S'#9'F'
                  'CLIENT_COPY_PRINTED'#9'1'#9'C'#9'F'
                  'AGENCY_COPY_PRINTED'#9'1'#9'A'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\evDBGrid2'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsBriefReportList
                TabOrder = 1
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object cbBriefStatus: TevDBComboBox
                Left = 119
                Top = 104
                Width = 121
                Height = 21
                ShowButton = True
                Style = csDropDownList
                MapList = True
                AllowClearKey = False
                AutoDropDown = True
                DataField = 'STATUS'
                DataSource = dsBriefReportList
                DropDownCount = 8
                ItemHeight = 0
                Picture.PictureMaskFromDataSet = False
                Sorted = False
                TabOrder = 0
                UnboundDataType = wwDefault
              end
              object evPanel9: TevPanel
                Left = 0
                Top = 0
                Width = 439
                Height = 14
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 2
                object lBriefReturns: TevLabel
                  Left = 0
                  Top = 0
                  Width = 72
                  Height = 13
                  Caption = 'Returns to print'
                  ShowAccelChar = False
                end
              end
            end
          end
        end
        object tsSimpleMagmedia: TTabSheet
          Caption = 'Brief queue view (magmedia)'
          ImageIndex = 9
          object evPanel6: TevPanel
            Left = 0
            Top = 0
            Width = 803
            Height = 331
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object evSplitter1: TevSplitter
              Left = 273
              Top = 0
              Height = 70
            end
            object evPanel1: TevPanel
              Left = 0
              Top = 0
              Width = 273
              Height = 70
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object gBriefMagmediaList: TevDBGrid
                Left = 0
                Top = 17
                Width = 273
                Height = 53
                DisableThemesInTitle = False
                Selected.Strings = (
                  'SY_REPORTS_DESC'#9'20'#9'Return'#9'F'
                  'GlAgencyFOName'#9'20'#9'Return'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\gBriefMagmediaList'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object evPanel4: TevPanel
                Left = 0
                Top = 0
                Width = 273
                Height = 17
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object evLabel7: TevLabel
                  Left = 0
                  Top = 0
                  Width = 84
                  Height = 13
                  Caption = 'Magmedia returns'
                end
              end
            end
            object evPanel3: TevPanel
              Left = 276
              Top = 0
              Width = 637
              Height = 70
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object gBriefMagmediaCompanyList: TevDBGrid
                Left = 0
                Top = 17
                Width = 637
                Height = 53
                DisableThemesInTitle = False
                ControlType.Strings = (
                  'TAX_SERVICE;CheckBox;Y;N')
                Selected.Strings = (
                  'CONSOLIDATION_DESCR'#9'1'#9'C'#9'F'
                  'TAX_SERVICE'#9'1'#9'Tax'#9'F'
                  'co_number'#9'10'#9'Comp. #'#9'F'
                  'cl_number'#9'10'#9'Client #'#9'F'
                  'CO_NAME'#9'25'#9'Company'#9'F')
                IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
                IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\gBriefMagmediaCompanyList'
                IniAttributes.Delimiter = ';;'
                ExportOptions.ExportType = wwgetSYLK
                ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                TabOrder = 0
                TitleAlignment = taLeftJustify
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                OnCalcCellColors = gBriefMagmediaCompanyListCalcCellColors
                OnCreateHintWindow = gBriefMagmediaCompanyListCreateHintWindow
                PaintOptions.AlternatingRowColor = clCream
                PaintOptions.ActiveRecordColor = clBlack
                NoFire = False
              end
              object evPanel5: TevPanel
                Left = 0
                Top = 0
                Width = 637
                Height = 17
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object evLabel8: TevLabel
                  Left = 8
                  Top = 0
                  Width = 113
                  Height = 13
                  Caption = 'Companies calculate for'
                end
              end
            end
          end
          object evPanel7: TevPanel
            Left = 0
            Top = 331
            Width = 803
            Height = 41
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object cbMmFilter: TevComboBox
              Left = 344
              Top = 8
              Width = 121
              Height = 21
              BevelKind = bkFlat
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 0
              OnChange = cbMmFilterChange
              Items.Strings = (
                'Show all'
                'Show reds'
                'Show greens'
                'Show cyan'
                'Show whites'
                '')
            end
            object evButton1: TevBitBtn
              Left = 480
              Top = 8
              Width = 89
              Height = 25
              Caption = 'Process'
              TabOrder = 1
              OnClick = evButton1Click
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD0DD0DDDDD
                DDDDDDDFDDFDDDDDDDDDDD0E00E0DDDDDDDDDDF8FF8FDDDDDDDDDDEEEEEE8888
                8888DD888888FFFFFFFFD00EEEE007FFFFF8DFF8888FFD88888FDEEE07EEE7FF
                FFF8D888FD888D88888FDEEE00EEE74444F8D888FF888DDDDD8FDD0EEEE07FFF
                FFF8DDF8888FD888888FDDEEEEEE744444F8DD888888DDDDDD8FD03E77E7FFFF
                FFF8DFD8DD8D8888888FDBB33F74444444F8D88DD8DDDDDDDD8F00BB3FFFFFFF
                FFF8FF88D8888888888FBBB0DFF4444444F8888FD88DDDDDDD8FBBB00FFFFFFF
                FFF8888FF8888888888FD0BB3FF4444444F8DF88D88DDDDDDD8FDBBB3FFFFFFF
                FFF8D888D8888888888FDDBD3FFFFFFFFFFDDD8DD8888888888D}
              NumGlyphs = 2
              Margin = 0
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Process/Print'
          ImageIndex = 3
          object wwDBGrid4: TevDBGrid
            Left = 0
            Top = 0
            Width = 803
            Height = 334
            DisableThemesInTitle = False
            ControlType.Strings = (
              'SYSTEM_TAX_TYPE;CustomEdit;cbGrid4ReturnType'
              'STATUS;CustomEdit;cbGrid4Status'
              'RETURN_FREQUENCY;CustomEdit;cbGrid4Freq'
              'TAX_SERVICE;CheckBox;Y;N')
            Selected.Strings = (
              'CONSOLIDATION_DESCR'#9'1'#9'C'#9'F'
              'TAX_SERVICE'#9'1'#9'Tax'#9'F'
              'co_number'#9'7'#9'Comp. #'#9'F'
              'cl_number'#9'7'#9'Client #'#9'F'
              'CO_NAME'#9'15'#9'Company'#9'F'
              'PERIOD_END_DATE'#9'10'#9'Period end'#9'F'
              'SYSTEM_TAX_TYPE'#9'11'#9'Return type'#9'F'
              'SY_REPORTS_DESC'#9'20'#9'Report'#9'F'
              'AgencyName'#9'30'#9'Agency Name'#9'F'
              'STATUS'#9'10'#9'Status'#9'F'
              'STATUS_DATE'#9'10'#9'Status date'#9'F'
              'DUE_DATE'#9'10'#9'Due date'#9'F'
              'RETURN_FREQUENCY'#9'1'#9'Fr'#9'F'
              'AGENCY_COPY_PRINTED'#9'1'#9'A'#9'F'
              'CLIENT_COPY_PRINTED'#9'1'#9'C'#9'F'
              'SB_COPY_PRINTED'#9'1'#9'S'#9'F'
              'PRIORITY2'#9'10'#9'Pr'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\wwDBGrid4'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsSelectedQueue
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgShowCellHint, dgDblClickColSizing]
            PopupMenu = menuHist
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnCalcCellColors = wwDBGrid2CalcCellColors
            OnCreateHintWindow = wwDBGrid3CreateHintWindow
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
          object cbGrid4Freq: TevDBComboBox
            Left = 456
            Top = 48
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 1
            UnboundDataType = wwDefault
            Visible = False
          end
          object cbGrid4Status: TevDBComboBox
            Left = 456
            Top = 72
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 2
            UnboundDataType = wwDefault
          end
          object cbGrid4ReturnType: TevDBComboBox
            Left = 456
            Top = 96
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 3
            UnboundDataType = wwDefault
          end
          object evPanel8: TevPanel
            Left = 0
            Top = 334
            Width = 803
            Height = 38
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 4
            object cbPreviewOn: TevCheckBox
              Left = 615
              Top = 5
              Width = 65
              Height = 17
              Caption = 'Preview'
              TabOrder = 2
              OnClick = cbPreviewOnClick
            end
            object btnProcess: TevBitBtn
              Left = 440
              Top = 9
              Width = 75
              Height = 25
              Caption = 'Process'
              TabOrder = 0
              OnClick = btnProcessClick
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD0DD0DDDDD
                DDDDDDDFDDFDDDDDDDDDDD0E00E0DDDDDDDDDDF8FF8FDDDDDDDDDDEEEEEE8888
                8888DD888888FFFFFFFFD00EEEE007FFFFF8DFF8888FFD88888FDEEE07EEE7FF
                FFF8D888FD888D88888FDEEE00EEE74444F8D888FF888DDDDD8FDD0EEEE07FFF
                FFF8DDF8888FD888888FDDEEEEEE744444F8DD888888DDDDDD8FD03E77E7FFFF
                FFF8DFD8DD8D8888888FDBB33F74444444F8D88DD8DDDDDDDD8F00BB3FFFFFFF
                FFF8FF88D8888888888FBBB0DFF4444444F8888FD88DDDDDDD8FBBB00FFFFFFF
                FFF8888FF8888888888FD0BB3FF4444444F8DF88D88DDDDDDD8FDBBB3FFFFFFF
                FFF8D888D8888888888FDDBD3FFFFFFFFFFDDD8DD8888888888D}
              NumGlyphs = 2
              Margin = 0
            end
            object btnReprint: TevBitBtn
              Left = 528
              Top = 9
              Width = 75
              Height = 25
              Caption = 'Reprint'
              TabOrder = 1
              OnClick = btnReprintClick
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD28DDDD
                DDDDDDDDDDFDDDDDDDDDDDDDD2A28DDDDDDDDDDDDF8FDDDDDDDDDDDD2AAA28DD
                DDDDDDDDF888FDDDDDDDDDD2AAAA28DDDDDDDDDF8888FDDDDDDDDD2AAAAAA200
                000DDDF888888F88888DD2AA882AA2877778DF88DDD88FDDDDD82A88888AAA28
                8778F8DD88D888F88DD8D8888778AA287778DD888DDD88FDDDD8D8888712AAA2
                8778D8888D8D888FDDD8D88887778AA28778D8888DDDD88FDDD8D88888888AAA
                2888D8888888D888F888D888877778AA2878D8888DDDDD88F8D8D88878FFF7A2
                878DD888D8DDDD8F8D8DD8870000000878DDD88D888888D8D8DDD87777777777
                8DDDD8DDDDDDDDDD8DDDDD8888888888DDDDDD8888888888DDDD}
              NumGlyphs = 2
              Margin = 0
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Message log'
          ImageIndex = 5
          DesignSize = (
            803
            372)
          object cbMessages: TListBox
            Left = 0
            Top = 35
            Width = 803
            Height = 337
            Align = alClient
            ItemHeight = 13
            TabOrder = 1
            OnDblClick = cbMessagesDblClick
          end
          object MessageLogSaveButton: TevButton
            Left = 41075
            Top = 3
            Width = 75
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Save log'
            TabOrder = 0
            OnClick = MessageLogSaveButtonClick
            Color = clBlack
            Margin = 0
          end
          object pnlTopMessagesTab: TPanel
            Left = 0
            Top = 0
            Width = 803
            Height = 35
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            object SpeedButton1: TSpeedButton
              Left = 376
              Top = 5
              Width = 65
              Height = 22
              Caption = 'Clear log'
              OnClick = SpeedButton1Click
            end
            object btnSend: TevSpeedButton
              Left = 4
              Top = 3
              Width = 23
              Height = 22
              Hint = 'Send e-mail'
              HideHint = True
              AutoSize = False
              OnClick = btnSendClick
              NumGlyphs = 2
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
                DDDDDDDDDDDDDDDDDDDD444444444448DDDD88888888888DDDDD4FFFF7777748
                888D8DDDDDDDDDDDDDDD4FFF700000000000DDDDDFFFFFFFFFFF0F008BBBBBBB
                BB808D8888888888888F4FF7B88BB7BB88B0DDDD8DD88888DD8F4000BBB87878
                BBB0D888888D8D8D888F4FF7B7788B8877B08DDD888DD8DD888F4F00788BBBBB
                88708D888DD88888DD8F4FF78BBBBBBBBB8D8DDD88888888888D4FF444444748
                DDDD8DDDDDDDDDDDDDDD4FFFFFFFFF48DDDD8DD888888D8DDDDD4FF444444F48
                DDDD8DDDDDDDDD8DDDDD4FFFFFFFFF48DDDD8DD888888D8DDDDD4FFFFFFFFF48
                DDDD8DDDDDDDDD8DDDDD44444444444DDDDD88888888888DDDDD}
              ParentColor = False
              ShortCut = 0
            end
            object cbDontPopupMessages: TevCheckBox
              Left = 40
              Top = 8
              Width = 281
              Height = 17
              Caption = 'Do Not Pop up Messages (batch mode)'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
          end
        end
        object tsXMLAggregation: TTabSheet
          Caption = 'XML Aggregation'
          ImageIndex = 10
          object pnlBottom: TevPanel
            Left = 0
            Top = 454
            Width = 1114
            Height = 37
            Align = alBottom
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 0
            object btnRunReport: TevBitBtn
              Left = 8
              Top = 6
              Width = 97
              Height = 25
              Caption = 'Run Report'
              TabOrder = 0
              OnClick = btnRunReportClick
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD28DDDD
                DDDDDDDDDDFDDDDDDDDDDDDDD2A28DDDDDDDDDDDDF8FDDDDDDDDDDDD2AAA28DD
                DDDDDDDDF888FDDDDDDDDDD2AAAA28DDDDDDDDDF8888FDDDDDDDDD2AAAAAA200
                000DDDF888888F88888DD2AA882AA2877778DF88DDD88FDDDDD82A88888AAA28
                8778F8DD88D888F88DD8D8888778AA287778DD888DDD88FDDDD8D8888712AAA2
                8778D8888D8D888FDDD8D88887778AA28778D8888DDDD88FDDD8D88888888AAA
                2888D8888888D888F888D888877778AA2878D8888DDDDD88F8D8D88878FFF7A2
                878DD888D8DDDD8F8D8DD8870000000878DDD88D888888D8D8DDD87777777777
                8DDDD8DDDDDDDDDD8DDDDD8888888888DDDDDD8888888888DDDD}
              NumGlyphs = 2
              Margin = 0
            end
          end
          object DBGridXMLEnvelopeReports: TevDBGrid
            Left = 0
            Top = 0
            Width = 1114
            Height = 454
            DisableThemesInTitle = False
            Selected.Strings = (
              'report_description'#9'40'#9'Report title'#9'F'#9)
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\DBGridXMLEnvelopeReports'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsXMLEnvelopeReports
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            UseTFields = True
            PaintOptions.AlternatingRowColor = clCream
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object tsMEF: TTabSheet
          Caption = 'MEF'
          ImageIndex = 11
          object evPanel14: TevPanel
            Left = 0
            Top = 335
            Width = 803
            Height = 37
            Align = alBottom
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 0
            object btnMefFile: TevBitBtn
              Left = 8
              Top = 6
              Width = 97
              Height = 25
              Caption = 'Create File'
              TabOrder = 0
              OnClick = btnMefFileClick
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD28DDDD
                DDDDDDDDDDFDDDDDDDDDDDDDD2A28DDDDDDDDDDDDF8FDDDDDDDDDDDD2AAA28DD
                DDDDDDDDF888FDDDDDDDDDD2AAAA28DDDDDDDDDF8888FDDDDDDDDD2AAAAAA200
                000DDDF888888F88888DD2AA882AA2877778DF88DDD88FDDDDD82A88888AAA28
                8778F8DD88D888F88DD8D8888778AA287778DD888DDD88FDDDD8D8888712AAA2
                8778D8888D8D888FDDD8D88887778AA28778D8888DDDD88FDDD8D88888888AAA
                2888D8888888D888F888D888877778AA2878D8888DDDDD88F8D8D88878FFF7A2
                878DD888D8DDDD8F8D8DD8870000000878DDD88D888888D8D8DDD87777777777
                8DDDD8DDDDDDDDDD8DDDDD8888888888DDDDDD8888888888DDDD}
              NumGlyphs = 2
              Margin = 0
            end
          end
          object evPanel15: TevPanel
            Left = 0
            Top = 0
            Width = 803
            Height = 37
            Align = alTop
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 1
            object dblookupMEF: TevDBLookupCombo
              Left = 48
              Top = 8
              Width = 221
              Height = 21
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'LookUpName'#9'40'#9'Name'#9'F')
              LookupTable = cdLookupMEF
              LookupField = 'LookUpName'
              Options = [loTitles]
              Style = csDropDownList
              TabOrder = 0
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = False
              OnChange = dblookupMEFChange
            end
          end
          object grdMEF: TevDBCheckGrid
            Left = 0
            Top = 37
            Width = 803
            Height = 298
            DisableThemesInTitle = False
            Selected.Strings = (
              'Co_Number'#9'20'#9'Company #'#9#9
              'CO_Name'#9'40'#9'Company Name'#9#9
              'Tax_Return'#9'20'#9'Tax Return'#9#9
              'ProcessDate'#9'18'#9'Processdate'#9#9
              'LookUpName'#9'40'#9'Lookupname'#9#9
              'CO_TAX_RETURN_QUEUE_NBR'#9'10'#9'Co Tax Return Queue Nbr'#9#9
              'CL_NBR'#9'10'#9'Cl Nbr'#9#9
              'REPORT_DESCRIPTION'#9'40'#9'Report Description'#9#9
              'CO_NBR'#9'10'#9'Co Nbr'#9'F'#9)
            IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
            IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\grdMEF'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsMEF
            MultiSelectOptions = [msoShiftSelect]
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            ReadOnly = True
            TabOrder = 2
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
        object tsACAFilling: TTabSheet
          Caption = 'ACA Filing'
          ImageIndex = 12
          object evPanel16: TevPanel
            Left = 0
            Top = 0
            Width = 803
            Height = 37
            Align = alTop
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 0
            object dblookupACA: TevDBLookupCombo
              Left = 48
              Top = 8
              Width = 221
              Height = 21
              DropDownAlignment = taLeftJustify
              Selected.Strings = (
                'LookUpName'#9'40'#9'Name'#9'F')
              LookupTable = cdLookupACA
              LookupField = 'LookUpName'
              Options = [loTitles]
              Style = csDropDownList
              TabOrder = 0
              AutoDropDown = True
              ShowButton = True
              PreciseEditRegion = False
              AllowClearKey = False
              OnChange = dblookupACAChange
            end
            object cbTestFile: TevCheckBox
              Left = 320
              Top = 8
              Width = 64
              Height = 17
              Caption = 'Test File'
              TabOrder = 1
              OnClick = chbAutorefreshStatusClick
            end
            object cbReplacement: TevCheckBox
              Left = 400
              Top = 8
              Width = 145
              Height = 17
              Caption = 'Submission Replacement'
              TabOrder = 2
              OnClick = cbReplacementClick
            end
            object cbTransmissionReplacement: TevCheckBox
              Left = 568
              Top = 8
              Width = 145
              Height = 17
              Caption = 'Transmission Replacement'
              TabOrder = 3
              OnClick = cbTransmissionReplacementClick
            end
            object edOriginalReceiptId: TevEdit
              Left = 760
              Top = 4
              Width = 174
              Height = 21
              Enabled = False
              TabOrder = 4
            end
          end
          object evPanel17: TevPanel
            Left = 0
            Top = 335
            Width = 803
            Height = 37
            Align = alBottom
            BevelInner = bvRaised
            BevelOuter = bvLowered
            TabOrder = 1
            object btnACAFile: TevBitBtn
              Left = 8
              Top = 6
              Width = 97
              Height = 25
              Caption = 'Create File'
              TabOrder = 0
              OnClick = btnACAFileClick
              Color = clBlack
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD28DDDD
                DDDDDDDDDDFDDDDDDDDDDDDDD2A28DDDDDDDDDDDDF8FDDDDDDDDDDDD2AAA28DD
                DDDDDDDDF888FDDDDDDDDDD2AAAA28DDDDDDDDDF8888FDDDDDDDDD2AAAAAA200
                000DDDF888888F88888DD2AA882AA2877778DF88DDD88FDDDDD82A88888AAA28
                8778F8DD88D888F88DD8D8888778AA287778DD888DDD88FDDDD8D8888712AAA2
                8778D8888D8D888FDDD8D88887778AA28778D8888DDDD88FDDD8D88888888AAA
                2888D8888888D888F888D888877778AA2878D8888DDDDD88F8D8D88878FFF7A2
                878DD888D8DDDD8F8D8DD8870000000878DDD88D888888D8D8DDD87777777777
                8DDDD8DDDDDDDDDD8DDDDD8888888888DDDDDD8888888888DDDD}
              NumGlyphs = 2
              Margin = 0
            end
          end
          object grdACA: TevDBCheckGrid
            Left = 0
            Top = 37
            Width = 803
            Height = 298
            DisableThemesInTitle = False
            Selected.Strings = (
              'Co_Number'#9'20'#9'Company #'#9#9
              'CO_Name'#9'40'#9'Company Name'#9#9
              'Tax_Return'#9'20'#9'Tax Return'#9#9
              'ProcessDate'#9'18'#9'Processdate'#9#9
              'LookUpName'#9'40'#9'Lookupname'#9#9
              'CO_TAX_RETURN_QUEUE_NBR'#9'10'#9'Co Tax Return Queue Nbr'#9#9
              'CL_NBR'#9'10'#9'Cl Nbr'#9#9
              'REPORT_DESCRIPTION'#9'40'#9'Report Description'#9#9
              'CO_NBR'#9'10'#9'Co Nbr'#9'F'#9)
            IniAttributes.FileName = 'SOFTWARE\delphi32\Grids\'
            IniAttributes.SectionName = 'TEDIT_TMP_CO_TAX_RETURN_QUEUE\grdACA'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = dsACA
            MultiSelectOptions = [msoShiftSelect]
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
            ReadOnly = True
            TabOrder = 2
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = 14544093
            PaintOptions.ActiveRecordColor = clBlack
            NoFire = False
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 112
    Top = 64
  end
  inherited wwdsDetail: TevDataSource
    Left = 272
    Top = 64
  end
  inherited wwdsList: TevDataSource
    Left = 146
    Top = 66
  end
  object cdQueue: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N'
      'SYSTEM_TAX_TYPE;CustomEdit;cbGrid3ReturnType'
      'STATUS;CustomEdit;cbGrid3Status'
      'SY_REPORTS_NBR;CustomEdit;cbGrid3ReportDescr'
      'AGENCY_COPY_PRINTED;CheckBox;Y;N'
      'CLIENT_COPY_PRINTED;CheckBox;Y;N'
      'SB_COPY_PRINTED;CheckBox;Y;N')
    FieldDefs = <
      item
        Name = 'CL_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CO_TAX_RETURN_QUEUE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TAX_SERVICE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SYSTEM_TAX_TYPE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PERIOD_END_DATE'
        DataType = ftDate
      end
      item
        Name = 'STATUS'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'STATUS_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'DUE_DATE'
        DataType = ftDate
      end
      item
        Name = 'RETURN_FREQUENCY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SB_COPY_PRINTED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CLIENT_COPY_PRINTED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'AGENCY_COPY_PRINTED'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CO_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'SY_GLOBAL_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_GL_AGENCY_REPORT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_REPORTS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_REPORTS_DESC'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'HIDDEN'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRIORITY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'co_number'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'taken'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UserNumber'
        DataType = ftInteger
      end
      item
        Name = 'cl_number'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CONSOLIDATION_DESCR'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'CONSOLIDATION_DESCR2'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'CONSOLIDATION'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'KEEP_ONHOLD'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'PRIORITY2'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'HAVE_ACOPY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'HAVE_CCOPY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'HAVE_SCOPY'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'SY_REPORTS_GROUP_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CL_CO_CONSOLIDATION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'PRODUCE_ASCII_FILE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'GlAgencyFOName'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'HOLD_RETURN_QUEUE'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'CREATION_DATE'
        DataType = ftDateTime
      end
      item
        Name = 'SY_REPORT_WRITER_REPORTS_NBR'
        DataType = ftInteger
      end>
    AfterScroll = cdQueueAfterScroll
    Left = 480
    object cdQueueCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object cdQueueCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
    object cdQueueCO_TAX_RETURN_QUEUE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_RETURN_QUEUE_NBR'
    end
    object cdQueueTAX_NON_TAX: TStringField
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      Size = 1
    end
    object cdQueueSYSTEM_TAX_TYPE: TStringField
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'SYSTEM_TAX_TYPE'
      Size = 1
    end
    object cdQueuePERIOD_END_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'PERIOD_END_DATE'
    end
    object cdQueueSTATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object cdQueueSTATUS_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'STATUS_DATE'
    end
    object cdQueueDUE_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'DUE_DATE'
    end
    object cdQueueRETURN_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'RETURN_FREQUENCY'
      Size = 1
    end
    object cdQueueSB_COPY_PRINTED: TStringField
      DisplayWidth = 1
      FieldName = 'SB_COPY_PRINTED'
      Size = 1
    end
    object cdQueueCLIENT_COPY_PRINTED: TStringField
      DisplayWidth = 1
      FieldName = 'CLIENT_COPY_PRINTED'
      Size = 1
    end
    object cdQueueAGENCY_COPY_PRINTED: TStringField
      DisplayWidth = 1
      FieldName = 'AGENCY_COPY_PRINTED'
      Size = 1
    end
    object cdQueueCO_NAME: TStringField
      DisplayWidth = 40
      FieldName = 'CO_NAME'
      Size = 40
    end
    object cdQueueSY_GLOBAL_AGENCY_NBR: TIntegerField
      DisplayWidth = 10
      FieldKind = fkInternalCalc
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
    end
    object cdQueueSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      DisplayWidth = 10
      FieldKind = fkInternalCalc
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
    end
    object cdQueueSY_GL_AGENCY_REPORT_NBR: TIntegerField
      DisplayWidth = 10
      FieldKind = fkInternalCalc
      FieldName = 'SY_GL_AGENCY_REPORT_NBR'
    end
    object cdQueueSY_REPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldKind = fkInternalCalc
      FieldName = 'SY_REPORTS_NBR'
    end
    object cdQueueSY_REPORTS_DESC: TStringField
      DisplayWidth = 40
      FieldKind = fkInternalCalc
      FieldName = 'SY_REPORTS_DESC'
      LookupKeyFields = 'SY_REPORTS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SY_REPORTS_NBR'
      Size = 40
    end
    object cdQueueHIDDEN: TStringField
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'HIDDEN'
      Size = 1
    end
    object cdQueuePRIORITY: TStringField
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'PRIORITY'
      Size = 1
    end
    object cdQueueco_number: TStringField
      DisplayWidth = 20
      FieldName = 'co_number'
    end
    object cdQueuetaken: TStringField
      DisplayWidth = 20
      FieldKind = fkInternalCalc
      FieldName = 'taken'
    end
    object cdQueueUserNumber: TIntegerField
      DisplayWidth = 10
      FieldKind = fkInternalCalc
      FieldName = 'UserNumber'
    end
    object cdQueuecl_number: TStringField
      DisplayWidth = 20
      FieldName = 'cl_number'
    end
    object cdQueueCONSOLIDATION_DESCR: TStringField
      DisplayWidth = 80
      FieldKind = fkInternalCalc
      FieldName = 'CONSOLIDATION_DESCR'
      Size = 80
    end
    object cdQueueCONSOLIDATION_DESCR2: TStringField
      DisplayWidth = 80
      FieldKind = fkInternalCalc
      FieldName = 'CONSOLIDATION_DESCR2'
      Size = 80
    end
    object cdQueueCONSOLIDATION: TStringField
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'CONSOLIDATION'
      Size = 1
    end
    object cdQueueKEEP_ONHOLD: TStringField
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'KEEP_ONHOLD'
      Size = 1
    end
    object cdQueuePRIORITY2: TStringField
      DisplayWidth = 50
      FieldKind = fkInternalCalc
      FieldName = 'PRIORITY2'
      Size = 50
    end
    object cdQueueHAVE_ACOPY: TStringField
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'HAVE_ACOPY'
      Size = 1
    end
    object cdQueueHAVE_CCOPY: TStringField
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'HAVE_CCOPY'
      Size = 1
    end
    object cdQueueHAVE_SCOPY: TStringField
      DisplayWidth = 1
      FieldKind = fkInternalCalc
      FieldName = 'HAVE_SCOPY'
      Size = 1
    end
    object cdQueueSY_REPORTS_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_REPORTS_GROUP_NBR'
    end
    object cdQueueCL_CO_CONSOLIDATION_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_CO_CONSOLIDATION_NBR'
    end
    object cdQueuePRODUCE_ASCII_FILE: TStringField
      DisplayWidth = 1
      FieldName = 'PRODUCE_ASCII_FILE'
      Size = 1
    end
    object cdQueueGlAgencyFOName: TStringField
      DisplayWidth = 30
      FieldKind = fkInternalCalc
      FieldName = 'GlAgencyFOName'
      Size = 30
    end
    object cdQueueHOLD_RETURN_QUEUE: TStringField
      DisplayWidth = 1
      FieldName = 'HOLD_RETURN_QUEUE'
      Size = 1
    end
    object cdQueueCREATION_DATE: TDateTimeField
      DisplayWidth = 18
      FieldKind = fkInternalCalc
      FieldName = 'CREATION_DATE'
    end
    object cdQueueSY_REPORT_WRITER_REPORTS_NBR: TIntegerField
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'SY_REPORT_WRITER_REPORTS_NBR'
      Calculated = True
    end
    object cdQueueAgencyName: TStringField
      FieldKind = fkCalculated
      FieldName = 'AgencyName'
      Size = 40
      Calculated = True
    end
  end
  object cdSelectedQueue: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N'
      'SYSTEM_TAX_TYPE;CustomEdit;cbGrid2ReturnType'
      'STATUS;CustomEdit;cbGrid2Status'
      'SY_REPORTS_NBR;CustomEdit;cbGrid2ReportDesc'
      'AGENCY_COPY_PRINTED;CheckBox;Y;N'
      'CLIENT_COPY_PRINTED;CheckBox;Y;N'
      'SB_COPY_PRINTED;CheckBox;Y;N')
    AfterScroll = cdSelectedQueueAfterScroll
    Left = 512
    object cdSelectedQueueCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object cdSelectedQueueCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdSelectedQueueCO_TAX_RETURN_QUEUE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_RETURN_QUEUE_NBR'
    end
    object cdSelectedQueueTAX_SERVICE: TStringField
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      Size = 1
    end
    object cdSelectedQueueSY_GLOBAL_AGENCY_NBR: TIntegerField
      FieldName = 'SY_GLOBAL_AGENCY_NBR'
    end
    object cdSelectedQueueSYSTEM_TAX_TYPE: TStringField
      DisplayWidth = 1
      FieldName = 'SYSTEM_TAX_TYPE'
      Size = 1
    end
    object cdSelectedQueuePERIOD_END_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'PERIOD_END_DATE'
    end
    object cdSelectedQueueSTATUS: TStringField
      DisplayWidth = 1
      FieldName = 'STATUS'
      Size = 1
    end
    object cdSelectedQueueSTATUS_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'STATUS_DATE'
    end
    object cdSelectedQueueDUE_DATE: TDateField
      DisplayWidth = 18
      FieldName = 'DUE_DATE'
    end
    object cdSelectedQueueRETURN_FREQUENCY: TStringField
      DisplayWidth = 1
      FieldName = 'RETURN_FREQUENCY'
      Size = 1
    end
    object cdSelectedQueueSY_GL_AGENCY_REPORT_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_REPORT_NBR'
    end
    object cdSelectedQueueSB_COPY_PRINTED: TStringField
      FieldName = 'SB_COPY_PRINTED'
      Size = 1
    end
    object cdSelectedQueueCLIENT_COPY_PRINTED: TStringField
      FieldName = 'CLIENT_COPY_PRINTED'
      Size = 1
    end
    object cdSelectedQueueAGENCY_COPY_PRINTED: TStringField
      FieldName = 'AGENCY_COPY_PRINTED'
      Size = 1
    end
    object cdSelectedQueueCO_NAME: TStringField
      FieldName = 'CO_NAME'
      Size = 40
    end
    object cdSelectedQueueSY_REPORTS_NBR: TIntegerField
      FieldName = 'SY_REPORTS_NBR'
    end
    object cdSelectedQueueSY_REPORTS_DESC: TStringField
      FieldName = 'SY_REPORTS_DESC'
      Size = 40
    end
    object cdSelectedQueueco_number: TStringField
      FieldName = 'co_number'
    end
    object cdSelectedQueuePRIORITY: TStringField
      FieldName = 'PRIORITY'
      Size = 1
    end
    object cdSelectedQueuecl_number: TStringField
      FieldName = 'cl_number'
    end
    object cdSelectedQueueCONSOLIDATION_DESCR: TStringField
      FieldName = 'CONSOLIDATION_DESCR'
      Size = 80
    end
    object cdSelectedQueueHAVE_ACOPY: TStringField
      FieldName = 'HAVE_ACOPY'
      Size = 1
    end
    object cdSelectedQueueHAVE_CCOPY: TStringField
      FieldName = 'HAVE_CCOPY'
      Size = 1
    end
    object cdSelectedQueueHAVE_SCOPY: TStringField
      FieldName = 'HAVE_SCOPY'
      Size = 1
    end
    object cdSelectedQueuePRIORITY2: TStringField
      FieldName = 'PRIORITY2'
      Size = 50
    end
    object cdSelectedQueuePRODUCE_ASCII_FILE: TStringField
      FieldName = 'PRODUCE_ASCII_FILE'
      Size = 1
    end
    object cdSelectedQueueCL_BLOB_NBR: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'CL_BLOB_NBR'
    end
    object cdSelectedQueueCREATION_DATE: TDateTimeField
      FieldKind = fkInternalCalc
      FieldName = 'CREATION_DATE'
    end
    object cdSelectedQueueSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
    end
    object cdSelectedQueueSY_REPORT_WRITER_REPORTS_NB: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SY_REPORT_WRITER_REPORTS_NBR'
      Calculated = True
    end
    object cdSelectedQueueAgencyName: TStringField
      FieldKind = fkCalculated
      FieldName = 'AgencyName'
      Size = 40
      Calculated = True
    end
  end
  object dsQueue: TevDataSource
    DataSet = cdQueue
    OnStateChange = dsQueueStateChange
    OnDataChange = dsQueueDataChange
    Left = 544
  end
  object dsSelectedQueue: TevDataSource
    DataSet = cdSelectedQueue
    Left = 576
  end
  object MessageLogSaveDialog: TSaveDialog
    DefaultExt = 'log'
    FileName = 'ReturnQueueProcessingErrorMessages.log'
    Filter = 'Log files|*.log'
    InitialDir = 'c:\'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofNoReadOnlyReturn, ofEnableSizing]
    Title = 'Save error messages to file...'
    Left = 356
  end
  object cdSummary: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N')
    IndexDefs = <
      item
        Name = 'I_NBR'
        Fields = 'cl_nbr;co_nbr;consolidation'
        Options = [ixUnique]
      end
      item
        Name = 'SORT'
        Fields = 'co_number'
      end>
    Left = 448
    object cdSummaryCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object cdSummaryCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdSummaryTAX_SERVICE: TStringField
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      Size = 1
    end
    object cdSummaryCO_NAME: TStringField
      FieldName = 'CO_NAME'
      Size = 40
    end
    object cdSummaryco_number: TStringField
      FieldName = 'co_number'
    end
    object cdSummaryprocessed: TBooleanField
      FieldName = 'processed'
    end
    object cdSummaryonhold: TBooleanField
      FieldName = 'onhold'
    end
    object cdSummaryCONSOLIDATION: TStringField
      FieldName = 'CONSOLIDATION'
      Size = 80
    end
    object cdSummaryErrorMessage: TStringField
      FieldName = 'ErrorMessage'
      Size = 255
    end
    object cdSummaryPreProcessDate: TDateTimeField
      FieldName = 'PreProcessDate'
    end
    object cdSummaryCL_CO_CONSOLIDATION_NBR: TIntegerField
      FieldName = 'CL_CO_CONSOLIDATION_NBR'
    end
    object cdSummarypaper_processed: TBooleanField
      FieldName = 'paper_processed'
    end
    object cdSummaryhas_paper: TBooleanField
      FieldName = 'has_paper'
    end
  end
  object dsSummary: TevDataSource
    DataSet = cdSummary
    OnDataChange = dsSummaryDataChange
    Left = 576
    Top = 32
  end
  object cdHeldList: TevClientDataSet
    IndexName = 'ITAG'
    IndexDefs = <
      item
        Name = 'ITAG'
        Fields = 'TAG'
        Options = [ixUnique]
      end>
    Left = 448
    Top = 32
  end
  object cdConsolidation: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N')
    Left = 416
  end
  object cdClCoInfo: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N')
    Left = 416
    Top = 32
  end
  object cdAnswers: TevClientDataSet
    Left = 384
    object cdAnswersCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
    end
    object cdAnswersCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdAnswersSTOP: TBooleanField
      FieldName = 'STOP'
    end
  end
  object cdEe: TevClientDataSet
    IndexDefs = <
      item
        Name = 'MAIN'
        Fields = 'CL_NBR;CO_NBR;CONSOLIDATION;PERIOD_END'
      end>
    Left = 384
    Top = 32
    object cdEeCUSTOM_EMPLOYEE_NUMBER: TStringField
      DisplayWidth = 9
      FieldName = 'CUSTOM_EMPLOYEE_NUMBER'
      Size = 512
    end
    object cdEeLAST_NAME: TStringField
      DisplayWidth = 20
      FieldName = 'LAST_NAME'
    end
    object cdEeFIRST_NAME: TStringField
      DisplayWidth = 15
      FieldName = 'FIRST_NAME'
      Size = 15
    end
    object cdEeSELECTED: TBooleanField
      DisplayWidth = 5
      FieldName = 'SELECTED'
    end
    object cdEeCURRENT_TERMINATION_CODE: TStringField
      DisplayWidth = 1
      FieldName = 'CURRENT_TERMINATION_CODE'
      Size = 1
    end
    object cdEeCONSOLIDATION: TBooleanField
      FieldName = 'CONSOLIDATION'
      Visible = False
    end
    object cdEeCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object cdEeCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object cdEePERIOD_END: TDateField
      FieldName = 'PERIOD_END'
      Visible = False
    end
    object cdEeCL_PERSON_NBR: TIntegerField
      FieldName = 'CL_PERSON_NBR'
      Visible = False
    end
    object cdEeEE_NBR: TIntegerField
      FieldName = 'EE_NBR'
      Visible = False
    end
    object cdEeSSN: TStringField
      FieldName = 'SSN'
      Visible = False
      Size = 40
    end
    object cdEeW2: TStringField
      FieldName = 'W2'
      Size = 1
    end
    object cdEeWebPassword: TStringField
      FieldName = 'WEB_PASSWORD'
      Visible = False
    end
    object cdEeEMail: TStringField
      FieldName = 'E_MAIL_ADDRESS'
      Visible = False
      Size = 80
    end
    object cdEeEE_NBR_LIST: TStringField
      FieldName = 'EE_NBR_LIST'
      Size = 512
    end
  end
  object dsEe: TevDataSource
    DataSet = cdEe
    Left = 176
    Top = 64
  end
  object dsBriefReportList: TevDataSource
    DataSet = cdBriefReportList
    Left = 80
    Top = 64
  end
  object cdBriefReportList: TevClientDataSet
    Left = 352
    Top = 32
  end
  object cdCoList: TevClientDataSet
    OnCalcFields = cdCoListCalcFields
    Left = 312
    Top = 32
    object cdCoListcl_nbr: TIntegerField
      FieldName = 'cl_nbr'
    end
    object cdCoListco_nbr: TIntegerField
      FieldName = 'co_nbr'
    end
    object cdCoListPRIMARY_CO_NBR: TIntegerField
      FieldName = 'PRIMARY_CO_NBR'
    end
    object cdCoListCL_CO_CONSOLIDATION_NBR: TIntegerField
      FieldName = 'CL_CO_CONSOLIDATION_NBR'
    end
    object cdCoListcustom_company_number: TStringField
      FieldName = 'custom_company_number'
    end
    object cdCoListcompany_name: TStringField
      FieldName = 'company_name'
      Size = 40
    end
    object cdCoListtax_service: TStringField
      FieldName = 'tax_service'
      Size = 1
    end
    object cdCoListCONS: TStringField
      FieldKind = fkCalculated
      FieldName = 'CONS'
      Size = 1
      Calculated = True
    end
  end
  object cdProcessed: TevClientDataSet
    Left = 312
  end
  object cdBriefMagmediaList: TevClientDataSet
    IndexName = '2'
    IndexDefs = <
      item
        Name = '1'
        Fields = 'SY_REPORTS_GROUP_NBR'
      end
      item
        Name = '2'
        Fields = 'SY_GL_AGENCY_REPORT_NBR'
      end>
    Left = 40
    Top = 64
    object cdBriefMagmediaListSY_REPORTS_NBR: TIntegerField
      FieldName = 'SY_REPORTS_NBR'
    end
    object cdBriefMagmediaListSY_REPORTS_DESC: TStringField
      FieldName = 'SY_REPORTS_DESC'
      LookupKeyFields = 'SY_REPORTS_NBR'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SY_REPORTS_NBR'
      Size = 40
    end
    object cdBriefMagmediaListSY_REPORTS_GROUP_NBR: TIntegerField
      FieldName = 'SY_REPORTS_GROUP_NBR'
    end
    object cdBriefMagmediaListGlAgencyFOName: TStringField
      FieldName = 'GlAgencyFOName'
      Size = 30
    end
    object cdBriefMagmediaListSY_GL_AGENCY_REPORT_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_REPORT_NBR'
    end
  end
  object cdBriefMagmediaCompanyList: TevClientDataSet
    IndexName = 'SORT'
    IndexDefs = <
      item
        Name = 'SORT'
        Fields = 'SY_REPORTS_GROUP_NBR;SY_GL_AGENCY_REPORT_NBR;CO_NUMBER'
        Options = [ixNonMaintained]
      end>
    OnCalcFields = cdBriefMagmediaCompanyListCalcFields
    Left = 8
    Top = 32
    object cdBriefMagmediaCompanyListCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object cdBriefMagmediaCompanyListCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdBriefMagmediaCompanyListCO_TAX_RETURN_QUEUE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_RETURN_QUEUE_NBR'
    end
    object cdBriefMagmediaCompanyListTAX_SERVICE: TStringField
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      Size = 1
    end
    object cdBriefMagmediaCompanyListCO_NAME: TStringField
      FieldName = 'CO_NAME'
      Size = 40
    end
    object cdBriefMagmediaCompanyListco_number: TStringField
      FieldName = 'co_number'
    end
    object cdBriefMagmediaCompanyListUserNumber: TIntegerField
      FieldName = 'UserNumber'
    end
    object cdBriefMagmediaCompanyListCONSOLIDATION_DESCR: TStringField
      FieldName = 'CONSOLIDATION_DESCR'
      Size = 80
    end
    object cdBriefMagmediaCompanyListCONSOLIDATION_DESCR2: TStringField
      FieldName = 'CONSOLIDATION_DESCR2'
      Size = 80
    end
    object cdBriefMagmediaCompanyListCONSOLIDATION: TStringField
      FieldName = 'CONSOLIDATION'
      Size = 1
    end
    object cdBriefMagmediaCompanyListSY_REPORTS_GROUP_NBR: TIntegerField
      FieldName = 'SY_REPORTS_GROUP_NBR'
    end
    object cdBriefMagmediaCompanyListPRODUCE_ASCII_FILE: TStringField
      FieldName = 'PRODUCE_ASCII_FILE'
      Size = 1
    end
    object cdBriefMagmediaCompanyListColor: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Color'
      Calculated = True
    end
    object cdBriefMagmediaCompanyListSY_GL_AGENCY_REPORT_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_REPORT_NBR'
    end
  end
  object cdStatuses: TevClientDataSet
    IndexFieldNames = 'CL_NBR;CO_NBR'
    Left = 280
  end
  object menuHist: TevPopupMenu
    OnPopup = menuHistPopup
    Left = 736
    Top = 88
  end
  object cdXMLEnvelopeReports: TevClientDataSet
    FieldDefs = <
      item
        Name = 'nbr'
        DataType = ftInteger
      end
      item
        Name = 'report_description'
        DataType = ftString
        Size = 64
      end>
    Left = 699
    Top = 6
    object cdXMLEnvelopeReportsreport_description: TStringField
      DisplayLabel = 'Report title'
      DisplayWidth = 40
      FieldName = 'report_description'
      Size = 64
    end
    object cdXMLEnvelopeReportsnbr: TIntegerField
      FieldName = 'nbr'
      Visible = False
    end
  end
  object dsXMLEnvelopeReports: TevDataSource
    DataSet = cdXMLEnvelopeReports
    Left = 735
    Top = 5
  end
  object dsCompanyFilter: TevDataSource
    DataSet = cdCompanyFilter
    Left = 624
    Top = 224
  end
  object cdCompanyFilter: TevClientDataSet
    Left = 584
    Top = 224
  end
  object cdSyReportsGroup: TevClientDataSet
    Left = 704
    Top = 56
  end
  object cdSyReports: TevClientDataSet
    Left = 704
    Top = 96
  end
  object dsFieldOfficeReports: TevDataSource
    DataSet = cdFieldOfficeReports
    Left = 560
    Top = 304
  end
  object cdFieldOfficeReports: TevClientDataSet
    FieldDefs = <
      item
        Name = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'SY_GL_AGENCY_REPORT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SY_REPORTS_GROUP_NBR '
        DataType = ftInteger
      end>
    OnCalcFields = cdFieldOfficeReportsCalcFields
    Left = 520
    Top = 304
    object cdFieldOfficeReportsSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
    end
    object cdFieldOfficeReportsNAME: TStringField
      DisplayWidth = 60
      FieldName = 'NAME'
      Size = 60
    end
    object cdFieldOfficeReportsSY_GL_AGENCY_REPORT_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_GL_AGENCY_REPORT_NBR'
    end
    object cdFieldOfficeReportsSY_REPORTS_GROUP_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'SY_REPORTS_GROUP_NBR '
    end
    object cdFieldOfficeReportsSYSTEM_TAX_TYPE: TStringField
      FieldName = 'SYSTEM_TAX_TYPE'
      Size = 1
    end
    object cdFieldOfficeReportsDEPOSIT_FREQUENCY: TStringField
      FieldName = 'DEPOSIT_FREQUENCY'
      Size = 1
    end
    object cdFieldOfficeReportsRETURN_FREQUENCY: TStringField
      FieldName = 'RETURN_FREQUENCY'
      Size = 1
    end
    object cdFieldOfficeReportsdescription: TStringField
      FieldKind = fkCalculated
      FieldName = 'description'
      Size = 60
      Calculated = True
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Text files(*.txt)|*.txt|Any files|*.*'
    Left = 792
    Top = 216
  end
  object PopupMenu1: TPopupMenu
    Left = 464
    Top = 304
    object LoadCompaniesfromTextFile1: TMenuItem
      Caption = 'Load Companies from Text File'
      OnClick = LoadCompaniesfromTextFile1Click
    end
  end
  object dsMEF: TevDataSource
    DataSet = cdMEF
    Left = 695
    Top = 269
  end
  object cdMEF: TevClientDataSet
    FieldDefs = <
      item
        Name = 'nbr'
        DataType = ftInteger
      end
      item
        Name = 'report_description'
        DataType = ftString
        Size = 64
      end>
    Left = 659
    Top = 270
    object cdMEFCo_Number: TStringField
      DisplayLabel = 'Company #'
      DisplayWidth = 20
      FieldName = 'Co_Number'
    end
    object cdMEFCO_Name: TStringField
      DisplayLabel = 'Company Name'
      DisplayWidth = 40
      FieldName = 'CO_Name'
      Size = 40
    end
    object cdMEFTax_Return: TStringField
      DisplayWidth = 20
      FieldName = 'Tax_Return'
    end
    object cdMEFProcessDate: TDateTimeField
      DisplayWidth = 18
      FieldName = 'ProcessDate'
    end
    object cdMEFLookUpName: TStringField
      DisplayWidth = 40
      FieldName = 'LookUpName'
      Size = 40
    end
    object cdMEFCO_TAX_RETURN_QUEUE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_RETURN_QUEUE_NBR'
    end
    object cdMEFCL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object cdMEFREPORT_DESCRIPTION: TStringField
      DisplayWidth = 40
      FieldName = 'REPORT_DESCRIPTION'
      Size = 40
    end
    object cdMEFCO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
  end
  object cdLookupMEF: TevClientDataSet
    FieldDefs = <
      item
        Name = 'nbr'
        DataType = ftInteger
      end
      item
        Name = 'report_description'
        DataType = ftString
        Size = 64
      end>
    Left = 659
    Top = 302
    object cdLookupMEFLookUpName: TStringField
      FieldName = 'LookUpName'
      Size = 40
    end
  end
  object dsLookupMEF: TevDataSource
    DataSet = cdLookupMEF
    Left = 695
    Top = 301
  end
  object cdACA: TevClientDataSet
    FieldDefs = <
      item
        Name = 'nbr'
        DataType = ftInteger
      end
      item
        Name = 'report_description'
        DataType = ftString
        Size = 64
      end>
    Left = 771
    Top = 278
    object cdACACo_Number: TStringField
      DisplayLabel = 'Company #'
      DisplayWidth = 20
      FieldName = 'Co_Number'
    end
    object cdACACO_Name: TStringField
      DisplayLabel = 'Company Name'
      DisplayWidth = 40
      FieldName = 'CO_Name'
      Size = 40
    end
    object cdACATax_Return: TStringField
      DisplayWidth = 20
      FieldName = 'Tax_Return'
    end
    object cdACAProcessDate: TDateTimeField
      DisplayWidth = 18
      FieldName = 'ProcessDate'
    end
    object cdACALookUpName: TStringField
      DisplayWidth = 40
      FieldName = 'LookUpName'
      Size = 40
    end
    object cdACACO_TAX_RETURN_QUEUE_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_TAX_RETURN_QUEUE_NBR'
    end
    object cdACACL_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CL_NBR'
    end
    object cdACAREPORT_DESCRIPTION: TStringField
      DisplayWidth = 40
      FieldName = 'REPORT_DESCRIPTION'
      Size = 40
    end
    object cdACACO_NBR: TIntegerField
      DisplayWidth = 10
      FieldName = 'CO_NBR'
    end
  end
  object dsACA: TevDataSource
    DataSet = cdACA
    Left = 807
    Top = 277
  end
  object cdLookupACA: TevClientDataSet
    FieldDefs = <
      item
        Name = 'nbr'
        DataType = ftInteger
      end
      item
        Name = 'report_description'
        DataType = ftString
        Size = 64
      end>
    Left = 771
    Top = 318
    object cdLookupACALookUpName: TStringField
      FieldName = 'LookUpName'
      Size = 40
    end
  end
  object dsLookupACA: TevDataSource
    DataSet = cdLookupACA
    Left = 807
    Top = 317
  end
end
