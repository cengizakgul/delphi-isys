object Frame_4_DB_GRIDS: TFrame_4_DB_GRIDS
  Left = 0
  Top = 0
  Width = 596
  Height = 406
  TabOrder = 0
  object Splitter3: TevSplitter
    Left = 0
    Top = 179
    Width = 596
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object Panel3: TevPanel
    Left = 0
    Top = 0
    Width = 596
    Height = 179
    Align = alTop
    Caption = 'Panel3'
    TabOrder = 0
    object Splitter2: TevSplitter
      Left = 301
      Top = 1
      Height = 177
    end
    object GroupBox1: TevGroupBox
      Left = 1
      Top = 1
      Width = 300
      Height = 177
      Align = alLeft
      Caption = 'GroupBox1'
      TabOrder = 0
      object wwDBGrid1: TevDBGrid
        Left = 2
        Top = 15
        Width = 296
        Height = 160
        IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
        IniAttributes.SectionName = 'TFrame_4_DB_GRIDS\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwDataSource1
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = clCream
      end
    end
    object GroupBox2: TevGroupBox
      Left = 304
      Top = 1
      Width = 291
      Height = 177
      Align = alClient
      Caption = 'GroupBox2'
      TabOrder = 1
      object wwDBGrid2: TevDBGrid
        Left = 2
        Top = 15
        Width = 287
        Height = 160
        IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
        IniAttributes.SectionName = 'TFrame_4_DB_GRIDS\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwDataSource2
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = clCream
      end
    end
  end
  object Panel4: TevPanel
    Left = 0
    Top = 182
    Width = 596
    Height = 224
    Align = alClient
    Caption = 'Panel4'
    TabOrder = 1
    object Splitter1: TevSplitter
      Left = 301
      Top = 1
      Height = 222
    end
    object GroupBox3: TevGroupBox
      Left = 1
      Top = 1
      Width = 300
      Height = 222
      Align = alLeft
      Caption = 'GroupBox3'
      TabOrder = 0
      object wwDBGrid3: TevDBGrid
        Left = 2
        Top = 15
        Width = 296
        Height = 205
        IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
        IniAttributes.SectionName = 'TFrame_4_DB_GRIDS\wwDBGrid3'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwDataSource3
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = clCream
      end
    end
    object GroupBox4: TevGroupBox
      Left = 304
      Top = 1
      Width = 291
      Height = 222
      Align = alClient
      Caption = 'GroupBox4'
      TabOrder = 1
      object wwDBGrid4: TevDBGrid
        Left = 2
        Top = 15
        Width = 287
        Height = 205
        IniAttributes.FileName = 'SOFTWARE\Evolution\Delphi32\Misc\'
        IniAttributes.SectionName = 'TFrame_4_DB_GRIDS\wwDBGrid4'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwDataSource4
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        UseTFields = True
        PaintOptions.AlternatingRowColor = clCream
      end
    end
  end
  object wwDataSource1: TevDataSource
    Left = 72
    Top = 64
  end
  object wwDataSource2: TevDataSource
    Left = 148
    Top = 64
  end
  object wwDataSource3: TevDataSource
    Left = 288
    Top = 56
  end
  object wwDataSource4: TevDataSource
    Left = 464
    Top = 56
  end
end
