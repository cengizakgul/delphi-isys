unit SPopupCompanySelectHelper;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  StdCtrls, Variants, EvBasicUtils, ISUtils, ISZippingRoutines, Printers,
  ComCtrls, Grids, Wwdbigrd, Wwdbgrid, Buttons, wwdblook, DBCtrls, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, ExtCtrls,  SFrameEntry, EvContext, EvMainboard,
  wwdbdatetimepicker,
//  SCompanyMuliSelectScreen,
  Spin, EvTypes, Menus,
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, Wwdatsrc, isBaseClasses, EvCommonInterfaces,
  EvDataAccessComponents, ISDataAccessComponents, isBasicUtils, isVCLBugFix, EvExceptions,ISErrorUtils,
  EvDataSet, SDDClasses, SDataDictsystem, SDataStructure, evUtils, EvSendMail, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, isUIwwDBEdit,
  isUIwwDBDateTimePicker, isUIwwDBLookupCombo, isUIwwDBComboBox, isUIEdit,
  LMDCustomButton, LMDButton, isUILMDButton, EvStreamUtils
  ,StrUtils
  ;

type

  TPopupCompanySelectHelper = class(TObject)
  public
    class procedure SelectCompanyListFromFile(  grdCompanyList: TevDBGrid; BoolToSetField:TBooleanField );
  end;


implementation

class procedure TPopupCompanySelectHelper.SelectCompanyListFromFile(  grdCompanyList: TevDBGrid; BoolToSetField:TBooleanField );
var
  iListNumbers: IisStringList;
  j: integer;
  jSuccess, jFailure: integer;
  CurrentBookMark:TBookmark;
  sSearchValue: string;
  sMessage: string;

  odCompNbr : TOpenDialog;
begin
   if grdCompanyList.DataSource.DataSet.RecordCount = 0 then Exit;

   CurrentBookMark := grdCompanyList.DataSource.DataSet.GetBookmark;

   grdCompanyList.SelectedList.Clear;

   jSuccess := 0; jFailure := 0; sMessage := '';

   odCompNbr := TOpenDialog.Create(grdCompanyList);
   try
     odCompNbr.Filter := 'Text files(.txt)|*.txt|Any files|*.*';
     if odCompNbr.Execute then
     begin
       iListNumbers := TisStringList.Create();
       iListNumbers.LoadFromFile(odCompNbr.FileName);
       for j := 0 to iListNumbers.Count-1 do
       begin
          sSearchValue := Trim(iListNumbers[j]);
          if Length(sSearchValue) > 0 then
          begin
          // yes, here is assumption that Custom Compamy Number, displayesd as Comp. #, is unique
              if grdCompanyList.DataSource.DataSet.Locate('CUSTOM_COMPANY_NUMBER', sSearchValue,[loCaseInsensitive]) then
              begin
                grdCompanyList.SelectRecord();// in classic Delphi it would be //DBGrid.SelectedRows.CurrentRowSelected := True;
                if Assigned( BoolToSetField) then
                begin
                  grdCompanyList.DataSource.DataSet.Edit;
                  grdCompanyList.DataSource.DataSet.FieldByName(BoolToSetField.FieldName).AsBoolean := True;
                  grdCompanyList.DataSource.DataSet.Post;
                end;
                Inc(jSuccess);
              end
              else
                Inc (jFailure);
          end;
       end;

       if jSuccess > 0 then
        sMessage := sMessage + Format('Selected %d companies',[grdCompanyList.SelectedList.Count]);
       if jFailure > 0 then
        sMessage := smessage + #13#10 + Format('Failed to find %d companies',[jFailure]);
       sMessage := Trim(sMessage);
     end;

     grdCompanyList.DataSource.DataSet.GotoBookmark(CurrentBookMark);
     if Length(sMessage) > 0 then
       ShowMessage(sMessage);
   finally
     odCompNbr.Free;
   end;
end;


end.
