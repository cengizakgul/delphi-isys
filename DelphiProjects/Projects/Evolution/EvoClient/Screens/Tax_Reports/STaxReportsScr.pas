// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit STaxReportsScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls, EvCommonInterfaces,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList,
  EvUtils, EvTypes, SDataStructure, Graphics, ISBasicClasses, EvMainboard,
  EvExceptions, EvUIComponents, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TTaxReportsFrm = class(TFramePackageTmpl, IevTaxReportsScreens)
  protected
    function  PackageCaption: string; override;
    function  PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  ActivatePackage(WorkPlace: TWinControl): Boolean; override;
    function  PackageBitmap: TBitmap; override;
  end;


implementation

{$R *.DFM}

var
  TaxReportsFrm: TTaxReportsFrm;

function GetTaxReportsScreens: IevTaxReportsScreens;
begin
  if not Assigned(TaxReportsFrm) then
    TaxReportsFrm := TTaxReportsFrm.Create(nil);
  Result := TaxReportsFrm;
end;


{ TSystemFrm }

function TTaxReportsFrm.ActivatePackage(WorkPlace: TWinControl): Boolean;
begin
  Result := inherited ActivatePackage(WorkPlace);
  if Result then
  begin
    DM_TEMPORARY.TMP_CL.Activate;
    if DM_TEMPORARY.TMP_CL.RecordCount = 0 then
      raise ECanNotPerformOperation.CreateHelp('There are no clients set up', IDH_CanNotPerformOperation);
  end;
end;

function TTaxReportsFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TTaxReportsFrm.PackageCaption: string;
begin
  Result := 'T&ax reports';
end;

function TTaxReportsFrm.PackageSortPosition: string;
begin
  Result := '160';
end;

procedure TTaxReportsFrm.UserPackageStructure;
begin
  AddStructure('Process Liabilities and Deposits|030', 'TProcessCombinedLiabilitiesAndDeposits');
  AddStructure('Tax adjustments|040', 'TPROCESS_UI_CORRECTION');
  AddStructure('Tax returns|050', 'TEDIT_TMP_CO_TAX_RETURN_QUEUE');
  AddStructure('Balance information|055', 'TBalanceReportScreen');
  AddStructure('Global Agency Contacts|060', 'TEDIT_SB_GLOBAL_AGENCY_CONTACTS');
end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetTaxReportsScreens, IevTaxReportsScreens, 'Screens TaxReports');

finalization
  TaxReportsFrm.Free;

end.
