// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_FRAME_4_IP_GRIDS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc, ExtCtrls, Grids, Wwdbigrd, Wwdbgrid, StdCtrls, 
  ISBasicClasses, EvUIComponents;

type
  TFrame_4_DB_GRIDS = class(TFrame)
    wwDataSource1: TevDataSource;
    wwDataSource2: TevDataSource;
    wwDataSource3: TevDataSource;
    wwDataSource4: TevDataSource;
    Panel3: TevPanel;
    Splitter2: TevSplitter;
    Panel4: TevPanel;
    Splitter1: TevSplitter;
    Splitter3: TevSplitter;
    GroupBox1: TevGroupBox;
    wwDBGrid1: TevDBGrid;
    GroupBox2: TevGroupBox;
    wwDBGrid2: TevDBGrid;
    GroupBox3: TevGroupBox;
    wwDBGrid3: TevDBGrid;
    GroupBox4: TevGroupBox;
    wwDBGrid4: TevDBGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

end.
