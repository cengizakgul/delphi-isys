object CompanyMuliSelectScreen: TCompanyMuliSelectScreen
  Left = 396
  Top = 318
  BorderStyle = bsSingle
  Caption = 'Select companies'
  ClientHeight = 440
  ClientWidth = 645
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object wwDBGrid1: TevDBGrid
    Left = 5
    Top = 2
    Width = 636
    Height = 401
    DisableThemesInTitle = False
    ControlType.Strings = (
      'LOAD;CheckBox;True;False'
      'TAX_SERVICE;CheckBox;Y;N')
    Selected.Strings = (
      'LOAD'#9'5'#9'Load'#9'F'
      'DESCR'#9'15'#9'Description'#9'T'
      'CUSTOM_CLIENT_NUMBER'#9'10'#9'Client #'#9'T'
      'CLIENT_NAME'#9'25'#9'Client name'#9'T'
      'CUSTOM_COMPANY_NUMBER'#9'10'#9'Comp. #'#9'T'
      'COMPANY_NAME'#9'25'#9'Company name'#9'T'
      'TAX_SERVICE'#9'1'#9'Tax'#9'T')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TCompanyMuliSelectScreen\wwDBGrid1'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsClCo
    PopupMenu = PopupMenu1
    ReadOnly = False
    TabOrder = 0
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    DefaultSort = 'CUSTOM_COMPANY_NUMBER'
    NoFire = False
  end
  object bOk: TevButton
    Left = 464
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    TabOrder = 3
    OnClick = bOkClick
    Color = clBlack
    Margin = 0
  end
  object bCancel: TevButton
    Left = 568
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
    Color = clBlack
    Margin = 0
  end
  object bUnSelectAll: TevButton
    Left = 340
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Unselect all'
    TabOrder = 2
    OnClick = bUnSelectAllClick
    Color = clBlack
    Margin = 0
  end
  object bSelectAll: TevButton
    Left = 260
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Select all'
    TabOrder = 1
    OnClick = bSelectAllClick
    Color = clBlack
    Margin = 0
  end
  object dsClCo: TevDataSource
    DataSet = cdClCo
    Left = 56
  end
  object cdClCo: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N'
      'FUI_DO;CheckBox;True;False'
      'LOAD;CheckBox;True;False'
      'SUI_DO;CheckBox;True;False')
    Filtered = True
    Left = 24
    object cdClCoLOAD: TBooleanField
      DisplayLabel = 'Load'
      DisplayWidth = 5
      FieldName = 'LOAD'
    end
    object cdClCoDESCRI: TStringField
      FieldName = 'DESCR'
      Size = 15
    end
    object cdClCoCUSTOM_CLIENT_NUMBER: TStringField
      DisplayLabel = 'Client #'
      DisplayWidth = 7
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdClCoCLIENT_NAME: TStringField
      DisplayLabel = 'Client name'
      DisplayWidth = 15
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdClCoCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Comp. #'
      DisplayWidth = 7
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdClCoCOMPANY_NAME: TStringField
      DisplayLabel = 'Company name'
      DisplayWidth = 15
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdClCoTAX_SERVICE: TStringField
      DisplayLabel = 'Tax'
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      FixedChar = True
      Size = 1
    end
    object cdClCoCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object cdClCoCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object cdClCocbDetailSystemReportLookupTableSY_REPORTS_GROUP_NBR: TIntegerField
      FieldName = 'CL_CO_CONSOLIDATION_NBR'
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 376
    Top = 144
    object LoadCompaniesfromTextFile1: TMenuItem
      Caption = 'Load Companies from Text File'
      OnClick = LoadCompaniesfromTextFile1Click
    end
  end
end
