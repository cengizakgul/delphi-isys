object CombineLiabilityDM: TCombineLiabilityDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 452
  Top = 332
  Height = 479
  Width = 741
  object wwcsFEIN: TevClientDataSet
    Left = 24
    Top = 88
    object wwcsFEINFEIN: TStringField
      DisplayLabel = 'EIN'
      DisplayWidth = 10
      FieldName = 'FEIN'
      Size = 10
    end
    object wwcsFEINClientName: TStringField
      DisplayLabel = 'Client Name'
      DisplayWidth = 40
      FieldName = 'ClientName'
      Size = 40
    end
    object wwcsFEINFEIN_ID: TIntegerField
      FieldName = 'FEIN_ID'
      Visible = False
    end
    object wwcsFEINCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
  end
  object wwcsTaxTypeTotal: TevClientDataSet
    IndexFieldNames = 'FEIN_ID'
    MasterFields = 'FEIN_ID'
    MasterSource = dsFEIN
    Left = 112
    Top = 8
    object wwcsTaxTypeTotalFEIN_ID: TIntegerField
      FieldName = 'FEIN_ID'
      Visible = False
    end
    object wwcsTaxTypeTotalTaxTypeTotId: TIntegerField
      FieldName = 'TaxTypeTotId'
      Visible = False
    end
    object wwcsTaxTypeTotalAgency: TStringField
      FieldName = 'Agency'
    end
    object wwcsTaxTypeTotalTotal: TCurrencyField
      FieldName = 'Total'
    end
    object wwcsTaxTypeTotalDataSetType: TStringField
      FieldName = 'DataSetType'
      Visible = False
    end
  end
  object wwcsTaxTypeSubTot1: TevClientDataSet
    IndexFieldNames = 'TaxTypeTotId'
    MasterFields = 'TaxTypeTotId'
    MasterSource = dsTaxTypeTotal
    Left = 248
    Top = 8
    object wwcsTaxTypeSubTot1Total: TCurrencyField
      FieldName = 'Total'
    end
    object wwcsTaxTypeSubTot1CheckDate: TDateField
      DisplayLabel = 'Check Date'
      FieldName = 'CheckDate'
    end
    object wwcsTaxTypeSubTot1DueDate: TDateField
      DisplayLabel = 'Due Date'
      FieldName = 'DueDate'
    end
    object wwcsTaxTypeSubTot1Company: TStringField
      DisplayLabel = 'Company nbr'
      FieldName = 'CoNumber'
      Size = 10
    end
    object wwcsTaxTypeSubTot1RunNumber: TStringField
      DisplayLabel = 'Run nbr'
      FieldName = 'RunNumber'
      Size = 10
    end
    object wwcsTaxTypeSubTot1TaxTypeTotId: TIntegerField
      FieldName = 'TaxTypeTotId'
      Visible = False
    end
    object wwcsTaxTypeSubTot1TaxTypSubTot1Id: TIntegerField
      FieldName = 'TaxTypeSubTot1Id'
      Visible = False
    end
    object wwcsTaxTypeSubTot1DepositGroup: TStringField
      DisplayLabel = 'Deposit Group'
      FieldName = 'DepositGroup'
    end
    object wwcsTaxTypeSubTot1PR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Visible = False
    end
    object wwcsTaxTypeSubTot1ID: TIntegerField
      FieldName = 'ID'
      Visible = False
    end
  end
  object wwcsTaxTypeSubTot2: TevClientDataSet
    IndexFieldNames = 'TaxTypeSubTot1Id'
    MasterFields = 'TaxTypeSubTot1Id'
    MasterSource = dsTaxTypeSubTot1
    Left = 376
    Top = 8
    object wwcsTaxTypeSubTot2Total: TCurrencyField
      FieldName = 'Total'
    end
    object wwcsTaxTypeSubTot2Taxsubtype: TStringField
      DisplayLabel = 'Tax Subtype'
      FieldName = 'Taxsubtype'
      Size = 10
    end
    object wwcsTaxTypeSubTot2TaxTypeSubTot1Id: TIntegerField
      FieldName = 'TaxTypeSubTot1Id'
      Visible = False
    end
    object wwcsTaxTypeSubTot2DepositGroup: TStringField
      DisplayLabel = 'Deposit Group'
      FieldName = 'DepositGroup'
    end
    object wwcsTaxTypeSubTot2ID: TIntegerField
      FieldName = 'ID'
      Visible = False
    end
  end
  object wwcsCheckDateSubTot2: TevClientDataSet
    IndexFieldNames = 'CheckDateSubTot1Id'
    MasterFields = 'CheckDateSubTot1Id'
    MasterSource = dsCheckDateSubTot1
    Left = 400
    Top = 104
    object CurrencyField1: TCurrencyField
      DisplayWidth = 10
      FieldName = 'Total'
    end
    object StringField1: TStringField
      DisplayLabel = 'Tax Subtype'
      DisplayWidth = 10
      FieldName = 'Taxsubtype'
      Size = 10
    end
    object wwcsCheckDateSubTot2CheckDateSubTot1Id: TIntegerField
      FieldName = 'CheckDateSubTot1Id'
      Visible = False
    end
    object wwcsCheckDateSubTot2DepositGroup: TStringField
      DisplayLabel = 'Deposit Group'
      FieldName = 'DepositGroup'
    end
    object wwcsCheckDateSubTot2ID: TIntegerField
      FieldName = 'ID'
      Visible = False
    end
  end
  object wwcsDeposit: TevClientDataSet
    IndexFieldNames = 'FEIN_ID'
    MasterFields = 'FEIN_ID'
    MasterSource = dsFEIN
    Left = 104
    Top = 232
    object wwcsDepositDepositDate: TDateField
      DisplayLabel = 'Deposit Date'
      DisplayWidth = 10
      FieldName = 'DepositDate'
    end
    object wwcsDepositDepositGroup: TStringField
      DisplayLabel = 'Deposit Group'
      DisplayWidth = 20
      FieldName = 'DepositGroup'
    end
    object wwcsDepositTotal: TCurrencyField
      DisplayWidth = 10
      FieldName = 'Total'
    end
    object wwcsDepositFEIN_ID: TIntegerField
      DisplayWidth = 10
      FieldName = 'FEIN_ID'
      Visible = False
    end
    object wwcsDepositDepositTotId: TIntegerField
      FieldName = 'DepositTotId'
      Visible = False
    end
  end
  object wwcsCheckDateSubTot1: TevClientDataSet
    IndexFieldNames = 'CheckDateTotId'
    MasterFields = 'CheckDateTotId'
    MasterSource = dsCheckDateTotal
    Left = 280
    Top = 136
    object StringField2: TStringField
      DisplayWidth = 20
      FieldName = 'Agency'
    end
    object CurrencyField2: TCurrencyField
      DisplayWidth = 10
      FieldName = 'Total'
    end
    object wwcsCheckDateSubTot1CheckDateSubTot1Id: TIntegerField
      FieldName = 'CheckDateSubTot1Id'
      Visible = False
    end
    object wwcsCheckDateSubTot1CheckDateTotId: TIntegerField
      FieldName = 'CheckDateTotId'
      Visible = False
    end
    object wwcsCheckDateSubTot1DepositGroup: TStringField
      DisplayLabel = 'Deposit Group'
      FieldName = 'DepositGroup'
    end
    object wwcsCheckDateSubTot1DataSetType: TStringField
      FieldName = 'DataSetType'
      Visible = False
      Size = 5
    end
    object wwcsCheckDateSubTot1ID: TIntegerField
      FieldName = 'ID'
      Visible = False
    end
  end
  object wwcsCheckDateTotal: TevClientDataSet
    IndexFieldNames = 'FEIN_ID'
    MasterFields = 'FEIN_ID'
    MasterSource = dsFEIN
    Left = 136
    Top = 128
    object DateField1: TDateField
      DisplayLabel = 'Due Date'
      DisplayWidth = 10
      FieldName = 'DueDate'
    end
    object CurrencyField3: TCurrencyField
      DisplayWidth = 10
      FieldName = 'Total'
    end
    object DateField2: TDateField
      DisplayLabel = 'Check Date'
      DisplayWidth = 10
      FieldName = 'CheckDate'
    end
    object StringField3: TStringField
      DisplayLabel = 'Company nbr'
      DisplayWidth = 10
      FieldName = 'CoNumber'
      Size = 10
    end
    object StringField4: TStringField
      DisplayLabel = 'Run nbr'
      DisplayWidth = 10
      FieldName = 'RunNumber'
      Size = 10
    end
    object wwcsCheckDateTotalFEIN_ID: TIntegerField
      FieldName = 'FEIN_ID'
      Visible = False
    end
    object wwcsCheckDateTotalCheckDateTotId: TIntegerField
      FieldName = 'CheckDateTotId'
      Visible = False
    end
    object wwcsCheckDateTotalPR_NBR: TIntegerField
      FieldName = 'PR_NBR'
      Visible = False
    end
    object wwcsCheckDateTotalCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
  end
  object dsFEIN: TwwDataSource
    DataSet = wwcsFEIN
    Left = 72
    Top = 88
  end
  object dsTaxTypeTotal: TwwDataSource
    DataSet = wwcsTaxTypeTotal
    Left = 160
    Top = 8
  end
  object dsTaxTypeSubTot1: TwwDataSource
    DataSet = wwcsTaxTypeSubTot1
    Left = 296
    Top = 8
  end
  object dsCheckDateTotal: TwwDataSource
    DataSet = wwcsCheckDateTotal
    Left = 176
    Top = 128
  end
  object dsCheckDateSubTot1: TwwDataSource
    DataSet = wwcsCheckDateSubTot1
    Left = 320
    Top = 136
  end
  object dsDeposit: TwwDataSource
    DataSet = wwcsDeposit
    Left = 152
    Top = 232
  end
  object wwcsDepositSubTot1: TevClientDataSet
    IndexFieldNames = 'DepositTotId'
    MasterFields = 'DepositTotId'
    MasterSource = dsDeposit
    Left = 240
    Top = 232
    object wwcsDepositSubTot1TaxDepositReference: TStringField
      DisplayLabel = 'Tax Deposit Reference'
      DisplayWidth = 20
      FieldName = 'TaxDepositReference'
    end
    object wwcsDepositSubTot1Status: TStringField
      DisplayWidth = 1
      FieldName = 'Status'
      Size = 1
    end
    object wwcsDepositSubTot1DepositType: TStringField
      DisplayLabel = 'Deposit Type'
      DisplayWidth = 20
      FieldName = 'Deposit_Type'
    end
    object wwcsDepositSubTot1SY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField
      DisplayLabel = 'Global Agency Field Office'
      DisplayWidth = 10
      FieldName = 'SY_GL_AGENCY_FIELD_OFFICE_NBR'
    end
    object wwcsDepositSubTot1DepositSubTot1Id: TIntegerField
      FieldName = 'DepositSubTot1Id'
      Visible = False
    end
    object wwcsDepositSubTot1DepositTotId: TIntegerField
      FieldName = 'DepositTotId'
      Visible = False
    end
    object wwcsDepositSubTot1Total: TCurrencyField
      FieldName = 'Total'
    end
    object wwcsDepositSubTot1CL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object wwcsDepositSubTot1CO_TAX_DEPOSITS_NBR: TIntegerField
      FieldName = 'CO_TAX_DEPOSITS_NBR'
      Visible = False
    end
    object wwcsDepositSubTot1DepositDate: TDateField
      FieldName = 'DepositDate'
      Visible = False
    end
    object wwcsDepositSubTot1DepositGroup: TStringField
      FieldName = 'DepositGroup'
      Visible = False
    end
  end
  object dsDepositSubTot1: TwwDataSource
    DataSet = wwcsDepositSubTot1
    Left = 280
    Top = 232
  end
  object wwcsDepositSubTot2: TevClientDataSet
    IndexFieldNames = 'DepositSubTot1Id'
    MasterFields = 'DepositSubTot1Id'
    MasterSource = dsDepositSubTot1
    Left = 384
    Top = 232
    object wwcsDepositSubTot2DueDate: TDateField
      DisplayLabel = 'Due Date'
      DisplayWidth = 10
      FieldName = 'DueDate'
    end
    object wwcsDepositSubTot2CheckDate: TDateField
      DisplayLabel = 'Check Date'
      DisplayWidth = 10
      FieldName = 'CheckDate'
    end
    object wwcsDepositSubTot2CoNumber: TStringField
      DisplayLabel = 'Company nbr'
      DisplayWidth = 10
      FieldName = 'CoNumber'
      Size = 10
    end
    object wwcsDepositSubTot2RunNumber: TStringField
      DisplayLabel = 'Run nbr'
      DisplayWidth = 10
      FieldName = 'RunNumber'
      Size = 10
    end
    object wwcsDepositSubTot2TaxType: TStringField
      DisplayLabel = 'Tax Type'
      DisplayWidth = 20
      FieldName = 'TaxType'
    end
    object wwcsDepositSubTot2Amount: TCurrencyField
      DisplayWidth = 10
      FieldName = 'Amount'
    end
    object wwcsDepositSubTot2DepositSubTot1Id: TIntegerField
      FieldName = 'DepositSubTot1Id'
      Visible = False
    end
  end
  object wwClientDataSet1: TevClientDataSet
    Left = 160
    Top = 320
  end
  object wwClientDataSet2: TevClientDataSet
    Left = 240
    Top = 320
  end
end
