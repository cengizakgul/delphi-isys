object EDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD: TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD
  Left = 733
  Top = 196
  Width = 861
  Height = 529
  ActiveControl = rgPeriod
  Caption = 'Enlist company or consolidation to queue '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TEvBevel
    Left = 8
    Top = 86
    Width = 815
    Height = 9
    Shape = bsTopLine
  end
  object evLabel1: TevLabel
    Left = 8
    Top = 52
    Width = 22
    Height = 13
    Caption = 'Year'
  end
  object evMsg: TevLabel
    Left = 635
    Top = 20
    Width = 192
    Height = 13
    Caption = 'Click to update the Company list  '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btnGo: TevButton
    Left = 697
    Top = 458
    Width = 85
    Height = 25
    Caption = 'Enlist Selected '
    TabOrder = 6
    Visible = False
    OnClick = btnGoClick
    Color = clBlack
    Margin = 0
  end
  object grdCompanyList: TevDBGrid
    Left = 9
    Top = 90
    Width = 824
    Height = 360
    DisableThemesInTitle = False
    ControlType.Strings = (
      'TERMINATION_CODE;CustomEdit;cbTerminationcode;F')
    Selected.Strings = (
      'TYPE_DESCR'#9'14'#9'Description'
      'CUSTOM_CLIENT_NUMBER'#9'10'#9'Client #'
      'CLIENT_NAME'#9'25'#9'Client name'#9'F'
      'CUSTOM_COMPANY_NUMBER'#9'10'#9'Company #'
      'COMPANY_NAME'#9'30'#9'Company name'
      'TAX_SERVICE'#9'1'#9'Tax'
      'TERMINATION_CODE'#9'15'#9'Status'#9'F'
      'LAST_PROCESSED_DATE'#9'18'#9'Last Check Date'#9'F')
    IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
    IniAttributes.SectionName = 'TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD\grdCompanyList'
    IniAttributes.Delimiter = ';;'
    ExportOptions.ExportType = wwgetSYLK
    ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    DataSource = dsClCo
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
    PopupMenu = popGridMenu
    TabOrder = 7
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 1
    Visible = False
    OnCalcTitleAttributes = grdCompanyListCalcTitleAttributes
    OnTitleButtonClick = grdCompanyListTitleButtonClick
    OnMouseUp = grdCompanyListMouseUp
    OnCalcTitleImage = grdCompanyListCalcTitleImage
    PaintOptions.AlternatingRowColor = clCream
    PaintOptions.ActiveRecordColor = clBlack
    NoFire = False
  end
  object cbMonth: TevComboBox
    Left = 162
    Top = 11
    Width = 137
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = cbMonthChange
    Items.Strings = (
      'January'
      'February'
      'March'
      'April'
      'May'
      'June'
      'July'
      'August'
      'September'
      'October'
      'November'
      'December')
  end
  object bLoad: TevButton
    Left = 668
    Top = 45
    Width = 114
    Height = 25
    Caption = 'Load'
    TabOrder = 2
    OnClick = bLoadClick
    Color = clBlack
    Margin = 0
  end
  object cbLoadAll: TevCheckBox
    Left = 329
    Top = 8
    Width = 193
    Height = 17
    Caption = 'Load All Regardless Payroll Statuses'
    Checked = True
    State = cbChecked
    TabOrder = 3
    OnClick = cbLoadAllClick
  end
  object edYear: TevSpinEdit
    Left = 78
    Top = 47
    Width = 65
    Height = 22
    MaxValue = 2100
    MinValue = 1999
    TabOrder = 1
    Value = 2000
  end
  object cbActiveOnly: TevCheckBox
    Left = 329
    Top = 26
    Width = 193
    Height = 17
    Caption = 'Load Active Companies Only'
    TabOrder = 4
    OnClick = cbSemiMthlyClick
  end
  object cbSemiMthly: TevCheckBox
    Left = 329
    Top = 64
    Width = 193
    Height = 17
    Caption = 'Semi-Mthly 1st Half Only'
    Enabled = False
    TabOrder = 5
    OnClick = cbSemiMthlyClick
  end
  object cbNotEnlisted: TevCheckBox
    Left = 329
    Top = 45
    Width = 193
    Height = 17
    Caption = 'Load Companies Not Enlisted '
    Enabled = False
    TabOrder = 8
    OnClick = cbSemiMthlyClick
  end
  object rgPeriod: TevRadioGroup
    Left = 8
    Top = 4
    Width = 135
    Height = 34
    Caption = 'Period '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Month'
      'Quarter')
    TabOrder = 9
    OnClick = rgPeriodClick
  end
  object btnEnlistAll: TevButton
    Left = 593
    Top = 458
    Width = 85
    Height = 25
    Caption = 'Enlist All'
    TabOrder = 10
    Visible = False
    OnClick = btnEnlistAllClick
    Color = clBlack
    Margin = 0
  end
  object btnEnlistActive: TevButton
    Left = 489
    Top = 458
    Width = 85
    Height = 25
    Caption = 'Enlist Active'
    TabOrder = 11
    Visible = False
    OnClick = btnEnlistActiveClick
    Color = clBlack
    Margin = 0
  end
  object cbTerminationcode: TevDBComboBox
    Left = 368
    Top = 240
    Width = 121
    Height = 21
    ShowButton = True
    Style = csDropDownList
    MapList = True
    AllowClearKey = False
    AutoDropDown = True
    DataField = 'TERMINATION_CODE'
    DataSource = dsClCo
    DropDownCount = 8
    ItemHeight = 0
    Items.Strings = (
      'Active'#9'A'
      'Inactive'#9'N'
      'Out of Business'#9'D'
      'Other Service'#9'L'
      'Seasonal Active'#9'C'
      'Seasonal Inactive'#9'S'
      'In House'#9'I'
      'Sold'#9'O'
      'Sample'#9'T'
      'Never Processed'#9'P'
      'In Conversion'#9'V')
    Picture.PictureMaskFromDataSet = False
    Sorted = False
    TabOrder = 12
    UnboundDataType = wwDefault
  end
  object cdClCo: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N')
    Left = 280
    Top = 136
    object cdClCoTYPE_DESCR: TStringField
      DisplayLabel = 'Description'
      DisplayWidth = 14
      FieldKind = fkInternalCalc
      FieldName = 'TYPE_DESCR'
      Size = 15
    end
    object cdClCoCUSTOM_CLIENT_NUMBER: TStringField
      DisplayLabel = 'Client #'
      DisplayWidth = 10
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdClCoCLIENT_NAME: TStringField
      DisplayLabel = 'Client name'
      DisplayWidth = 25
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdClCoCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Company #'
      DisplayWidth = 10
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdClCoCOMPANY_NAME: TStringField
      DisplayLabel = 'Company name'
      DisplayWidth = 30
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdClCoTAX_SERVICE: TStringField
      DisplayLabel = 'Tax'
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      FixedChar = True
      Size = 1
    end
    object cdClCoCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object cdClCoCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object cdClCoFEIN: TStringField
      FieldName = 'FEIN'
      Visible = False
      Size = 9
    end
    object cdClCoTRUST_SERVICE: TStringField
      FieldName = 'TRUST_SERVICE'
      Visible = False
      FixedChar = True
      Size = 1
    end
    object cdClCoTERMINATION_CODE: TStringField
      FieldName = 'TERMINATION_CODE'
      Visible = False
      FixedChar = True
      Size = 1
    end
    object cdClCoTYPE: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'TYPE'
      Visible = False
      Size = 1
    end
    object cdClCoLAST_CHECK_DATE: TDateTimeField
      FieldName = 'LAST_CHECK_DATE'
    end
    object cdClCoNORMAL_CHECK_DATE: TDateTimeField
      FieldName = 'NORMAL_CHECK_DATE'
    end
    object cdClCoLAST_PROCESSED_DATE: TDateTimeField
      FieldName = 'LAST_PROCESSED_DATE'
    end
  end
  object dsClCo: TevDataSource
    DataSet = cdClCo
    Left = 312
    Top = 136
  end
  object cdCheckDates: TevClientDataSet
    IndexName = '1'
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N')
    IndexDefs = <
      item
        Name = '1'
        Fields = 'CL_NBR;CO_NBR'
      end>
    Left = 376
    Top = 136
    object IntegerField1: TIntegerField
      FieldName = 'CL_NBR'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CO_NBR'
    end
    object cdCheckDatesSCHEDULED_CHECK_DATE: TDateTimeField
      FieldName = 'SCHEDULED_CHECK_DATE'
    end
    object cdCheckDatesNORMAL_CHECK_DATE: TDateTimeField
      FieldName = 'NORMAL_CHECK_DATE'
    end
    object cdCheckDatesLAST_PROCESSED_DATE: TDateTimeField
      FieldName = 'LAST_PROCESSED_DATE'
    end
  end
  object cdConsolidation: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N')
    Left = 408
    Top = 136
  end
  object cdClCoInfo: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N')
    Left = 440
    Top = 136
  end
  object popGridMenu: TPopupMenu
    Left = 624
    Top = 208
    object miSelectFromFile: TMenuItem
      Caption = 'Load Companies from Text File'
      OnClick = miSelectFromFileClick
    end
  end
end
