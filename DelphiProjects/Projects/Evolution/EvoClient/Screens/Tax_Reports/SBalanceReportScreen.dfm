inherited BalanceReportScreen: TBalanceReportScreen
  object Pc: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    ActivePage = ts1
    Align = alClient
    TabOrder = 0
    OnChange = PcChange
    object ts1: TTabSheet
      Caption = 'Select companies'
      object cbQuarter: TevComboBox
        Left = 8
        Top = 408
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          '1'#39'st quarter'
          '2'#39'nd quarter'
          '3'#39'rd quarter'
          '4'#39'th quarter')
      end
      object cbYear: TevEdit
        Left = 168
        Top = 408
        Width = 49
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 2
        Text = '2000'
      end
      object UpDown1: TUpDown
        Left = 217
        Top = 409
        Width = 13
        Height = 19
        Min = 1999
        Max = 2100
        Position = 2000
        TabOrder = 5
        OnClick = UpDown1Click
      end
      object wwDBGrid1: TevDBGrid
        Left = 8
        Top = 0
        Width = 677
        Height = 401
        DisableThemesInTitle = False
        ControlType.Strings = (
          'LOAD;CheckBox;True;False'
          'TAX_SERVICE;CheckBox;Y;N')
        Selected.Strings = (
          'LOAD'#9'5'#9'Load'#9'F'
          'DESCR'#9'15'#9'Description'#9'T'
          'TAX_TYPE_DESCR'#9'5'#9'Tax type'#9'F'
          'CUSTOM_CLIENT_NUMBER'#9'10'#9'Client #'#9'T'
          'CLIENT_NAME'#9'25'#9'Client name'#9'T'
          'CUSTOM_COMPANY_NUMBER'#9'10'#9'Comp. #'#9'T'
          'COMPANY_NAME'#9'25'#9'Company name'#9'T'
          'TAX_SERVICE'#9'1'#9'Tax'#9'T')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TBalanceReportScreen\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
        DataSource = dsClCo
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object cbConsolidate: TevCheckBox
        Left = 336
        Top = 408
        Width = 65
        Height = 17
        Caption = 'Consolid.'
        Checked = True
        State = cbChecked
        TabOrder = 4
        Visible = False
      end
      object cbWholeYear: TCheckBox
        Left = 240
        Top = 408
        Width = 81
        Height = 17
        Caption = 'Whole year'
        TabOrder = 3
        OnClick = cbWholeYearClick
      end
      object Button4: TevBitBtn
        Left = 408
        Top = 408
        Width = 89
        Height = 25
        Caption = 'Select all'
        TabOrder = 6
        OnClick = Button4Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD0000
          008DDDDDDDDD888888FD0000DDDD0000008D8888DDDD888888FD00D0DDD80000
          008D88D8DDDF888888FD0D80DD80FFFF77088D88DDF8DDDDDD8F0000DD0FFFFF
          F7088888DD8DDDDDDD8FDDDDDD07FFFFF708DDDDDD8DDDDDDD8F0000DD80FFFF
          77088888DDD8DDDDDD8F00D0DDD0FF77008D88D8DDD8DDDD88FD0D80DDD0F000
          8DDD8D88DDD8D888FDDD0000DDD0F08DDDDD8888DDD8D8FDDDDDDDDDDDD0F08D
          DDDDDDDDDDD8D8FDDDDD0000DDD0F08DDDDD8888DDD8D8FDDDDD00D0DDD0F08D
          DDDD88D8DDD8D8FDDDDD0D80DDD808DDDDDD8D88DDDD8FDDDDDD0000DDDDDDDD
          DDDD8888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object Button5: TevBitBtn
        Left = 504
        Top = 408
        Width = 89
        Height = 25
        Caption = 'Unselect all'
        TabOrder = 7
        OnClick = Button5Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDD0000DDDDDDDDDDDD8888DDDDDDDDDDDD00D0DD88DDDD
          D88D88D8DDDDDDDDDDDD0D80DD008DDD80088D88DDFFDDDDDFFD0000D8FF08D8
          0FF88888DD88FDDDF88DDDDDD8FFF080FFF8DDDDDD888FDF888D0000DD8FFF0F
          FF8D8888DDD888F888DD00D0DDD8FFFFF8DD88D8DDDD88888DDD0D80DDD80FFF
          08DD8D88DDDDF888FDDD0000DD80FFFFF08D8888DDDF88888FDDDDDDD80FFF8F
          FF08DDDDDDF888D888FD0000D8FFF8D8FFF88888DD888DDD888D00D0D8FF8DDD
          8FF888D8DD88DDDDD88D0D80DD88DDDDD88D8D88DDDDDDDDDDDD0000DDDDDDDD
          DDDD8888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object Button1: TevBitBtn
        Left = 608
        Top = 408
        Width = 75
        Height = 25
        Caption = 'Load'
        TabOrder = 8
        OnClick = Button1Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D44444444444
          4DDDD888888888888DDDD4FFFF7FFFFF4DDDD8DDDDDDDDDD8DDDD4FFF708FFFF
          4DDDD8DDDDFDDDDD8DDDD4FF70A08FFF4DDDD8DDDF8FDDDD8DDDD4F70AAA08FF
          4888D8DDF888FDDD8D88D470AAAAA08F4778D8DF88888FDD8DD8D47AAAAAAA08
          4778D8D8888888FD8DD8D4777AAA07774778D8DDD888FDDD8DD8D4FF7AAA0888
          4778D8DDD888FDDD8DD8D4FF7AAA00000078D8DDD888FFFFFFD8D4444AAAAAAA
          A078D888D88888888FD8D4444AAAAAAAA078D888D88888888FD8DDDD8AAAAAAA
          A078DDDDD88888888FD8DDDD877777777778DDDD8DDDDDDDDDD8DDDD88888888
          8888DDDD888888888888DDDD888888888888DDDD888888888888}
        NumGlyphs = 2
      end
    end
    object ts2: TTabSheet
      Caption = 'Balance summary'
      ImageIndex = 1
      object evSplitter1: TevSplitter
        Left = 305
        Top = 0
        Height = 465
      end
      object evDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 305
        Height = 465
        DisableThemesInTitle = False
        Selected.Strings = (
          'ClNumber'#9'8'#9'Client #'#9'F'
          'ClName'#9'15'#9'Cl. name'#9'F'
          'CoNumber'#9'8'#9'Comp. #'#9'F'
          'CoName'#9'15'#9'Name'#9'F'
          'OutBalance'#9'5'#9'Out Of Balance'#9'F'
          'OutBalWages'#9'5'#9'Negative Wages'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TBalanceReportScreen\evDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = dsCoSum
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object evDBGrid2: TevDBGrid
        Left = 308
        Top = 0
        Width = 268
        Height = 465
        DisableThemesInTitle = False
        Selected.Strings = (
          'TextDescription'#9'20'#9'Description'#9'F'
          'AmountWages'#9'10'#9'Wages'#9'F'
          'AmountTaxLiab'#9'10'#9'Tax liab'#9'F'
          'AmountTaxHist'#9'10'#9'Tax hist'#9'F'
          'DifLiabHist'#9'10'#9'Diff. liab hist'#9'F'
          'TaxPercent'#9'10'#9'Tax %'#9'F'
          'TaxAmount'#9'10'#9'Tax Amount'#9'F'
          'DifWageHist'#9'10'#9'Diff. wage hist'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TBalanceReportScreen\evDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsSummary
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = evDBGrid2DblClick
        PaintOptions.AlternatingRowColor = clCream
        DefaultSort = '-'
      end
    end
    object ts3: TTabSheet
      Caption = 'Wages details'
      ImageIndex = 2
      object gWageDetails: TevDBGrid
        Left = 0
        Top = 0
        Width = 796
        Height = 500
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TBalanceReportScreen\gWageDetails'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsWageDetails
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
    end
    object ts4: TTabSheet
      Caption = 'Liab details'
      ImageIndex = 3
      object gLiabDetails: TevDBGrid
        Left = 0
        Top = 0
        Width = 796
        Height = 500
        DisableThemesInTitle = False
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TBalanceReportScreen\gLiabDetails'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = dsLiabDetails
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
    end
  end
  object cdClCo: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N'
      'FUI_DO;CheckBox;True;False'
      'LOAD;CheckBox;True;False'
      'SUI_DO;CheckBox;True;False')
    Filtered = True
    Left = 24
    object cdClCoLOAD: TBooleanField
      DisplayLabel = 'Load'
      DisplayWidth = 5
      FieldKind = fkInternalCalc
      FieldName = 'LOAD'
    end
    object cdClCoDESCRI: TStringField
      FieldName = 'DESCR'
      Size = 15
    end
    object cdClCoCUSTOM_CLIENT_NUMBER: TStringField
      DisplayLabel = 'Client #'
      DisplayWidth = 7
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdClCoCLIENT_NAME: TStringField
      DisplayLabel = 'Client name'
      DisplayWidth = 15
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdClCoCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Comp. #'
      DisplayWidth = 7
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdClCoCOMPANY_NAME: TStringField
      DisplayLabel = 'Company name'
      DisplayWidth = 15
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdClCoTAX_SERVICE: TStringField
      DisplayLabel = 'Tax'
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      FixedChar = True
      Size = 1
    end
    object cdClCoCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object cdClCoCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object cdClCoCONSOL: TBooleanField
      FieldName = 'CONSOLIDATED'
    end
  end
  object dsClCo: TevDataSource
    DataSet = cdClCo
    Left = 56
  end
  object dsSummary: TevDataSource
    DataSet = cdsSummary
    Left = 396
    Top = 32
  end
  object dsCoSum: TevDataSource
    DataSet = cdsCoSum
    Left = 396
    Top = 72
  end
  object cdsCoSum: TevClientDataSet
    AfterScroll = cdsCoSumAfterScroll
    Left = 361
    Top = 72
    object cdsCoSumClName: TStringField
      FieldName = 'ClName'
      Size = 40
    end
    object cdsCoSumCoName: TStringField
      FieldName = 'CoName'
      Size = 40
    end
    object cdsCoSumClNumber: TStringField
      FieldName = 'ClNumber'
    end
    object cdsCoSumCoNumber: TStringField
      FieldName = 'CoNumber'
    end
    object cdsCoSumClNbr: TIntegerField
      FieldName = 'ClNbr'
    end
    object cdsCoSumCoNbr: TIntegerField
      FieldName = 'CoNbr'
    end
    object cdsCoSumConsolidation: TBooleanField
      FieldName = 'Consolidation'
    end
    object cdsCoSumOutBalance: TBooleanField
      FieldName = 'OutBalance'
    end
    object cdsCoSumOutBalWages: TBooleanField
      FieldName = 'OutBalWages'
    end
    object cdsCoSumMargin: TFloatField
      FieldName = 'Margin'
    end
    object cdsCoSumOutBalance_: TBooleanField
      FieldName = 'OutBalance_'
    end
    object cdsCoSumOutBalance_EE_ER: TBooleanField
      FieldName = 'OutBalance_EE_ER'
    end
  end
  object cdsSummary: TevClientDataSet
    IndexName = 'cdsSummaryIndex'
    FieldDefs = <
      item
        Name = 'TaxAmount'
        DataType = ftCurrency
      end
      item
        Name = 'ClNbr'
        DataType = ftInteger
      end
      item
        Name = 'CoNbr'
        DataType = ftInteger
      end
      item
        Name = 'SortKey'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'TextDescription'
        DataType = ftString
        Size = 80
      end
      item
        Name = 'AmountWages'
        DataType = ftCurrency
      end
      item
        Name = 'AmountTaxHist'
        DataType = ftCurrency
      end
      item
        Name = 'AmountTaxLiab'
        DataType = ftCurrency
      end
      item
        Name = 'WagesDSName'
        DataType = ftString
        Size = 256
      end
      item
        Name = 'LiabDSName'
        DataType = ftString
        Size = 256
      end
      item
        Name = 'DifLiabHist'
        DataType = ftCurrency
      end
      item
        Name = 'DifWageHist'
        DataType = ftCurrency
      end
      item
        Name = 'ClName'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CoName'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'ClNumber'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CoNumber'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Consolidation'
        DataType = ftBoolean
      end
      item
        Name = 'TaxPercent'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'cdsSummaryIndex'
        Fields = 'ClNbr;CoNbr;SortKey'
      end>
    Left = 364
    Top = 34
    object cdsSummaryTaxAmount: TCurrencyField
      FieldName = 'TaxAmount'
    end
    object cdsSummaryClNbr: TIntegerField
      FieldName = 'ClNbr'
    end
    object cdsSummaryCoNbr: TIntegerField
      FieldName = 'CoNbr'
    end
    object cdsSummarySortKey: TStringField
      FieldName = 'SortKey'
      Size = 50
    end
    object cdsSummaryTextDescription: TStringField
      FieldName = 'TextDescription'
      Size = 80
    end
    object cdsSummaryAmountWages: TCurrencyField
      FieldName = 'AmountWages'
    end
    object cdsSummaryAmountTaxHist: TCurrencyField
      FieldName = 'AmountTaxHist'
    end
    object cdsSummaryAmountTaxLiab: TCurrencyField
      FieldName = 'AmountTaxLiab'
    end
    object cdsSummaryWagesDSName: TStringField
      FieldName = 'WagesDSName'
      Size = 256
    end
    object cdsSummaryLiabDSName: TStringField
      FieldName = 'LiabDSName'
      Size = 256
    end
    object cdsSummaryDifLiabHist: TCurrencyField
      FieldName = 'DifLiabHist'
    end
    object cdsSummaryDifWageHist: TCurrencyField
      FieldName = 'DifWageHist'
    end
    object cdsSummaryClName: TStringField
      FieldName = 'ClName'
      Size = 40
    end
    object cdsSummaryCoName: TStringField
      FieldName = 'CoName'
      Size = 40
    end
    object cdsSummaryClNumber: TStringField
      FieldName = 'ClNumber'
    end
    object cdsSummaryCoNumber: TStringField
      FieldName = 'CoNumber'
    end
    object cdsSummaryConsolidation: TBooleanField
      FieldName = 'Consolidation'
    end
    object cdsSummaryTaxPercent: TFloatField
      FieldName = 'TaxPercent'
      DisplayFormat = '#,##0.00'
    end
    object cdsSummaryMargin: TFloatField
      FieldName = 'Margin'
    end
    object cdsSummaryAmountEE_Tax_: TCurrencyField
      FieldName = 'AmountEE_Tax_'
    end
    object cdsSummaryAmountER_Tax_: TCurrencyField
      FieldName = 'AmountER_Tax_'
    end
    object cdsSummaryDifWageHist_EE_ER: TCurrencyField
      FieldName = 'DifWageHist_EE_ER'
    end
    object cdsSummaryFICAEE_Wages_: TCurrencyField
      FieldName = 'FICAEE_Wages_'
    end
    object cdsSummaryFICADifWageHist_: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'FICADifWageHist_'
      Calculated = True
    end
  end
  object dsWageDetails: TevDataSource
    Left = 396
    Top = 112
  end
  object dsLiabDetails: TevDataSource
    Left = 396
    Top = 152
  end
  object Provider: TisProvider
    Left = 364
    Top = 111
  end
end
