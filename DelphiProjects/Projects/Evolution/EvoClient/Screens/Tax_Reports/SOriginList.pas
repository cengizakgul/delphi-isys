unit SOriginList;
interface
uses
  evStreamUtils,
  ContNrs,
  MSXML2_TLB, evExceptions;

type
  TSubmissionManifest = class(TObject)
  private

    FSubmissionId: String;
    FEFIN: string;
    FGovernmentCode: String;
    FSubmissionType: String;
    FTaxYear: String;
    FTaxPeriodBeginDate: String;
    FTaxPeriodEndDate: String;
    FEIN: String;

    function GetXML: string;

    procedure SetEFIN(const Value: string);
  protected
    class function CreateXMLDomDocument2: IXMLDomDocument2;
  public
    constructor Create(AOriginId: string);
    destructor Destroy; override;

    property SubmissionId: string read FSubmissionId write FSubmissionId;
    property EFIN: string  read FEFIN write SetEFIN;
    property GovernmentCode: string read FGovernmentCode write FGovernmentCode;
    property SubmissionType: string read FSubmissionType write FSubmissionType;
    property TaxYear: string read FTaxYear write FTaxYear;
    property TaxPeriodBeginDate: string read FTaxPeriodBeginDate write FTaxPeriodBeginDate;
    property TaxPeriodEndDate: string read FTaxPeriodEndDate write FTaxPeriodEndDate;
    property EIN: string read FEIN write FEIN;

    property XML: string read GetXML;
  end;

  TSubmissionList = class(TObject)
  private
    FList: TObjectList;
    function GetItems(Index: integer): TSubmissionManifest;
    procedure SetItems(Index: integer; const Value: TSubmissionManifest);
    function GetCount: integer;
  protected
    property List: TObjectList read FList;
  public
    constructor Create;
    destructor Destroy; override;
    property Count: integer read GetCount;
    function Add(Item: TSubmissionManifest): integer;
    function Remove(Item: TSubmissionManifest): integer;
    function Find(const submissionId: string): TSubmissionManifest;
    property Items[Index: integer]: TSubmissionManifest read GetItems write SetItems; default;
  end;

implementation
uses
  SXMLHelper, SysUtils;


constructor TSubmissionManifest.Create;
begin
  inherited Create;
end;

destructor TSubmissionManifest.Destroy;
begin
  inherited;
end;

class function TSubmissionManifest.CreateXMLDomDocument2: IXMLDomDocument2;
begin
  Result := CoDOMDocument60.Create;
end;

function TSubmissionManifest.GetXML: string;
var
  XMLDOMDocument2: IXMLDOMDocument2;
  pRoot: IXMLDOMElement;
  pAttribute: IXMLDOMAttribute;
begin
  XMLDOMDocument2 := CreateXMLDomDocument2;

  pRoot := XMLDOMDocument2.createElement('IRSSubmissionManifest');

  pAttribute := XMLDOMDocument2.createAttribute('xmlns:efile');
  pAttribute.value := 'http://www.irs.gov/efile';
  pRoot.setAttributeNode(pAttribute);

  pAttribute := XMLDOMDocument2.createAttribute('xmlns');
  pAttribute.value := 'http://www.irs.gov/efile';
  pRoot.setAttributeNode(pAttribute);


  TXMLHelper.AppendChild(XMLDomDocument2, pRoot, 'SubmissionId', 3, FSubmissionId);
  TXMLHelper.AppendChild(XMLDomDocument2, pRoot, 'EFIN', 3, FEFIN);  
  TXMLHelper.AppendChild(XMLDomDocument2, pRoot, 'TaxYr', 3, FTaxYear);
  TXMLHelper.AppendChild(XMLDomDocument2, pRoot, 'GovernmentCd', 3, FGovernmentCode);
  TXMLHelper.AppendChild(XMLDomDocument2, pRoot, 'FederalSubmissionTypeCd', 3, FSubmissionType);
  TXMLHelper.AppendChild(XMLDomDocument2, pRoot, 'TaxPeriodBeginDt', 3, FTaxPeriodBeginDate);
  TXMLHelper.AppendChild(XMLDomDocument2, pRoot, 'TaxPeriodEndDt', 3, FTaxPeriodEndDate);
  TXMLHelper.AppendChild(XMLDomDocument2, pRoot, 'TIN', 3, FEIN);


  XMLDomDocument2.appendChild(pRoot);

  Result := CorrectXMLHeader + #13#10 +  XMLDomDocument2.XML;
end;

procedure TSubmissionManifest.SetEFIN(const Value: string);
const
  InvalidMsg = 'Invalid EFINNumber';
  LegalChars = ['0'..'9'];
  MinLength = 6;
  MaxLength = 6;
var
  I: integer;
begin
  if Length(Value) < MinLength then
    raise EevException.Create(InvalidMsg);
  if Length(Value) > MaxLength then
    raise EevException.Create(InvalidMsg);
  for I := 1 to Length(Value) do
    if not (Value[I] in LegalChars) then
      raise EevException.Create(InvalidMsg);

  FEFIN := Value;
end;

constructor TSubmissionList.Create;
begin
  inherited Create;
  FList := TObjectList.Create(True);
end;

destructor TSubmissionList.Destroy;
begin
  FreeAndNil(FList);
  inherited;
end;

function TSubmissionList.Add(Item: TSubmissionManifest): integer;
begin
  Result := FList.Add(Item);
end;

function TSubmissionList.Remove(Item: TSubmissionManifest): integer;
begin
  Result := FList.Remove(Item);
end;

function TSubmissionList.GetCount: integer;
begin
  Result := FList.Count;
end;

function TSubmissionList.GetItems(Index: integer): TSubmissionManifest;
begin
  Result := FList[Index] as TSubmissionManifest;
end;

procedure TSubmissionList.SetItems(Index: integer; const Value: TSubmissionManifest);
begin
  FList[Index] := Value;
end;

function TSubmissionList.Find(const submissionId: string): TSubmissionManifest;
var
  I: integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
  begin
    if SameText(Items[I].SubmissionId, submissionId) then
    begin
      Result := Items[I];
      Exit;
    end;
  end;
end;

end.

