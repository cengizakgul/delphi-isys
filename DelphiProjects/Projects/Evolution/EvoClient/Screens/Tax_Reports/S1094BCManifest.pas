unit S1094BCManifest;
interface
uses
  evStreamUtils,
  ContNrs,
  MSXML2_TLB, evExceptions;

type
  T1094SubmissionManifest = class(TObject)
  private
    FUniqueTransmissionId: String;
    FTimestamp: string;

    FPaymentYr: String;
    FPriorYearDataInd: String;

    FEFIN: String;

    FTransmissionTypeCd: String;
    FTestFileCd: String;
    FOriginalReceiptId: String;

    FBusinessNameLine1Txt: String;
    FBusinessNameLine2Txt: String;
    FCompanyNm: String;
    FAddressLine1Txt: String;
    FAddressLine2Txt: String;
    FCityNm: String;
    FUSStateCd: String;
    FUSZIPCd: String;
    FUSZIPExtensionCd: String;
    FCPersonFirstNm: String;
    FCPersonMiddleNm: String;
    FCPersonLastNm: String;

    FCContactPhoneNum: String;

    FVendorCd: String;
    FVPersonFirstNm: String;
    FVPersonMiddleNm: String;
    FVPersonLastNm: String;

    FVContactPhoneNum: String;

    FTotalPayeeRecordCnt: String;
    FTotalPayerRecordCnt: String;

    FSoftwareId: String;   // ?
    FFormTypeCd: String;
    FBinaryFormatCd: String;
    FChecksumAugmentationNum: String;
    FAttachmentByteSizeNum: String;
    FDocumentSystemFileNm: String;

    function GetXML: string;

  protected
    class function CreateXMLDomDocument2: IXMLDomDocument2;
  public
    constructor Create;
    destructor Destroy; override;

    property UniqueTransmissionId: string read FUniqueTransmissionId write FUniqueTransmissionId;
    property Timestamp: string read FTimestamp write FTimestamp;

    property PaymentYr: string read FPaymentYr write FPaymentYr;
    property PriorYearDataInd: string read FPriorYearDataInd write FPriorYearDataInd;
    property EFIN: string  read FEFIN write FEFIN;
    property TransmissionTypeCd: string read FTransmissionTypeCd write FTransmissionTypeCd;
    property TestFileCd: string read FTestFileCd write FTestFileCd;
    property OriginalReceiptId: string read FOriginalReceiptId write FOriginalReceiptId;

    property BusinessNameLine1Txt: string read FBusinessNameLine1Txt write FBusinessNameLine1Txt;
    property BusinessNameLine2Txt: string read FBusinessNameLine2Txt write FBusinessNameLine2Txt;

    property CompanyNm: string read FCompanyNm write FCompanyNm;
    property AddressLine1Txt: string read FAddressLine1Txt write FAddressLine1Txt;
    property AddressLine2Txt: string read FAddressLine2Txt write FAddressLine2Txt;
    property CityNm: string read FCityNm write FCityNm;
    property USStateCd: string read FUSStateCd write FUSStateCd;
    property USZIPCd: string read FUSZIPCd write FUSZIPCd;
    property USZIPExtensionCd: string read FUSZIPExtensionCd write FUSZIPExtensionCd;
    property CPersonFirstNm: string read FCPersonFirstNm write FCPersonFirstNm;
    property CPersonMiddleNm: string read FCPersonMiddleNm write FCPersonMiddleNm;
    property CPersonLastNm: string read FCPersonLastNm write FCPersonLastNm;
    property CContactPhoneNum: string read FCContactPhoneNum write FCContactPhoneNum;

    property VendorCd: string read FVendorCd write FVendorCd;
    property VPersonFirstNm: string read FVPersonFirstNm write FVPersonFirstNm;
    property VPersonMiddleNm: string read FVPersonMiddleNm write FVPersonMiddleNm;
    property VPersonLastNm: string read FVPersonLastNm write FVPersonLastNm;
    property VContactPhoneNum: string read FVContactPhoneNum write FVContactPhoneNum;

    property TotalPayeeRecordCnt: string read FTotalPayeeRecordCnt write FTotalPayeeRecordCnt;
    property TotalPayerRecordCnt: string read FTotalPayerRecordCnt write FTotalPayerRecordCnt;
    property SoftwareId: string read FSoftwareId write FSoftwareId;
    property FormTypeCd: string read FFormTypeCd write FFormTypeCd;
    property BinaryFormatCd: string read FBinaryFormatCd write FBinaryFormatCd;
    property ChecksumAugmentationNum: string read FChecksumAugmentationNum write FChecksumAugmentationNum;
    property AttachmentByteSizeNum: string read FAttachmentByteSizeNum write FAttachmentByteSizeNum;
    property DocumentSystemFileNm: string read FDocumentSystemFileNm write FDocumentSystemFileNm;

    property XML: string read GetXML;
  end;

implementation
uses
  SXMLHelper, SysUtils;


constructor T1094SubmissionManifest.Create;
begin
  inherited Create;
end;

destructor T1094SubmissionManifest.Destroy;
begin
  inherited;
end;

class function T1094SubmissionManifest.CreateXMLDomDocument2: IXMLDomDocument2;
begin
  Result := CoDOMDocument60.Create;
end;

function T1094SubmissionManifest.GetXML: string;
var
  XMLDOMDocument2: IXMLDOMDocument2;
  pRoot, p, p1, p2, p3: IXMLDOMElement;
  pAttribute: IXMLDOMAttribute;
begin
  XMLDOMDocument2 := CreateXMLDomDocument2;

  pRoot := XMLDOMDocument2.createElement('p:ACAUIBusinessHeader');

  pAttribute := XMLDOMDocument2.createAttribute('xmlns:xsi');
  pAttribute.value := 'http://www.w3.org/2001/XMLSchema-instance';
  pRoot.setAttributeNode(pAttribute);

  pAttribute := XMLDOMDocument2.createAttribute('xmlns');
  pAttribute.value := 'zxcv';
  pRoot.setAttributeNode(pAttribute);

  pAttribute := XMLDOMDocument2.createAttribute('xmlns:wsu');
  pAttribute.value := 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
  pRoot.setAttributeNode(pAttribute);

  pAttribute := XMLDOMDocument2.createAttribute('xmlns:irs');
  pAttribute.value := 'urn:us:gov:treasury:irs:common';
  pRoot.setAttributeNode(pAttribute);

  pAttribute := XMLDOMDocument2.createAttribute('xsi:schemaLocation');
  pAttribute.value := 'urn:us:gov:treasury:irs:msg:acauibusinessheader IRS-ACAUserInterfaceHeaderMessage.xsd';
  pRoot.setAttributeNode(pAttribute);

  pAttribute := XMLDOMDocument2.createAttribute('xmlns:p');
  pAttribute.value := 'urn:us:gov:treasury:irs:msg:acauibusinessheader';
  pRoot.setAttributeNode(pAttribute);

  p1 := XMLDOMDocument2.createElement('acaBusHeader:ACABusinessHeader');

  TXMLHelper.AppendChild(XMLDomDocument2, p1, 'UniqueTransmissionId', 3, UniqueTransmissionId);
  TXMLHelper.AppendChild(XMLDomDocument2, p1, 'Timestamp', 3, Timestamp, True);
  pRoot.appendChild(p1);

  p := XMLDOMDocument2.createElement('ACATransmitterManifestReqDtl');

  TXMLHelper.AppendChild(XMLDomDocument2, p, 'PaymentYr', 3, FPaymentYr);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'PriorYearDataInd', 3, FPriorYearDataInd);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'EIN', 3, FEFIN);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'TransmissionTypeCd', 3, FTransmissionTypeCd);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'TestFileCd', 3, FTestFileCd, True);
  if FOriginalReceiptId <> '' then
      TXMLHelper.AppendChild(XMLDomDocument2, p, 'OriginalReceiptId', 3, FOriginalReceiptId , True);

  p1 := XMLDOMDocument2.createElement('TransmitterNameGrp');
  TXMLHelper.AppendChild(XMLDomDocument2, p1, 'BusinessNameLine1Txt', 4, BusinessNameLine1Txt);
  p.appendChild(p1);

  p1 := XMLDOMDocument2.createElement('CompanyInformationGrp');
  TXMLHelper.AppendChild(XMLDomDocument2, p1, 'CompanyNm', 4, CompanyNm, True);
    p2 := XMLDOMDocument2.createElement('MailingAddressGrp');
     p3 := XMLDOMDocument2.createElement('USAddressGrp');
     TXMLHelper.AppendChild(XMLDomDocument2, p3, 'AddressLine1Txt', 6, AddressLine1Txt);
     if Trim(AddressLine2Txt) <> '' then
        TXMLHelper.AppendChild(XMLDomDocument2, p3, 'AddressLine2Txt', 6, AddressLine2Txt);

     TXMLHelper.AppendChild(XMLDomDocument2, p3, 'CityNm', 6, CityNm);
     TXMLHelper.AppendChild(XMLDomDocument2, p3, 'USStateCd', 6, USStateCd);
     TXMLHelper.AppendChild(XMLDomDocument2, p3, 'USZIPCd', 6, USZIPCd);
     if Trim(USZIPExtensionCd) <> '' then
       TXMLHelper.AppendChild(XMLDomDocument2, p3, 'USZIPExtensionCd', 6, USZIPExtensionCd, True);
     p2.appendChild(p3);
    p1.appendChild(p2);
     p2 := XMLDOMDocument2.createElement('ContactNameGrp');
     if CPersonFirstNm <> '' then
       TXMLHelper.AppendChild(XMLDomDocument2, p2, 'PersonFirstNm', 5,CPersonFirstNm);
     if CPersonMiddleNm <> '' then
       TXMLHelper.AppendChild(XMLDomDocument2, p2, 'PersonMiddleNm', 5, CPersonMiddleNm);
     if CPersonLastNm <> '' then
       TXMLHelper.AppendChild(XMLDomDocument2, p2, 'PersonLastNm', 5, CPersonLastNm);

    p1.appendChild(p2);
    TXMLHelper.AppendChild(XMLDomDocument2, p1, 'ContactPhoneNum', 4,CContactPhoneNum, true);
  p.appendChild(p1);

  p1 := XMLDOMDocument2.createElement('VendorInformationGrp');
    TXMLHelper.AppendChild(XMLDomDocument2, p1, 'VendorCd', 4, VendorCd, true);
    p2 := XMLDOMDocument2.createElement('ContactNameGrp');
    if VPersonFirstNm <> '' then
      TXMLHelper.AppendChild(XMLDomDocument2, p2, 'PersonFirstNm', 5,VPersonFirstNm);
    if VPersonMiddleNm <> '' then
      TXMLHelper.AppendChild(XMLDomDocument2, p2, 'PersonMiddleNm', 5, VPersonMiddleNm);
    if VPersonLastNm <> '' then
      TXMLHelper.AppendChild(XMLDomDocument2, p2, 'PersonLastNm', 5, VPersonLastNm);
    p1.appendChild(p2);
    TXMLHelper.AppendChild(XMLDomDocument2, p1, 'ContactPhoneNum', 4,VContactPhoneNum, true);
  p.appendChild(p1);

  TXMLHelper.AppendChild(XMLDomDocument2, p, 'TotalPayeeRecordCnt', 3, FTotalPayeeRecordCnt);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'TotalPayerRecordCnt', 3, FTotalPayerRecordCnt);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'SoftwareId', 3, FSoftwareId);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'FormTypeCd', 3, FFormTypeCd);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'BinaryFormatCd', 3, FBinaryFormatCd);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'ChecksumAugmentationNum', 3, FChecksumAugmentationNum);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'AttachmentByteSizeNum', 3, FAttachmentByteSizeNum);
  TXMLHelper.AppendChild(XMLDomDocument2, p, 'DocumentSystemFileNm', 3, FDocumentSystemFileNm, true);
  pRoot.appendChild(p);

  XMLDomDocument2.appendChild(pRoot);

  Result := CorrectXMLHeader + #13#10 +  XMLDomDocument2.XML;

  Result := StringReplace(Result, 'xmlns="zxcv"', 'xmlns="urn:us:gov:treasury:irs:ext:aca:air:7.0" xmlns:acaBusHeader="urn:us:gov:treasury:irs:msg:acabusinessheader"', [rfReplaceAll]);

  Result := StringReplace(Result, 'Timestamp', 'irs:Timestamp', [rfReplaceAll]);
  Result := StringReplace(Result, 'EIN', 'irs:EIN', [rfReplaceAll]);
  Result := StringReplace(Result, 'CityNm', 'irs:CityNm', [rfReplaceAll]);

  Result := StringReplace(Result, 'USZIPCd', 'irs:USZIPCd', [rfReplaceAll]);
  Result := StringReplace(Result, 'USZIPExtensionCd', 'irs:USZIPExtensionCd', [rfReplaceAll]);
  Result := StringReplace(Result, 'SuffixNm', 'irs:SuffixNm', [rfReplaceAll]);

  Result := StringReplace(Result, 'BinaryFormatCd', 'irs:BinaryFormatCd', [rfReplaceAll]);
  Result := StringReplace(Result, 'ChecksumAugmentationNum', 'irs:ChecksumAugmentationNum', [rfReplaceAll]);
  Result := StringReplace(Result, 'AttachmentByteSizeNum', 'irs:AttachmentByteSizeNum', [rfReplaceAll]);

end;

end.

