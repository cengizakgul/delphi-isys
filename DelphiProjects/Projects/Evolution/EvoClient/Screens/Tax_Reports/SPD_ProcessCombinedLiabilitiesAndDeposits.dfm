inherited ProcessCombinedLiabilitiesAndDeposits: TProcessCombinedLiabilitiesAndDeposits
  object PageControl1: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 435
    Height = 266
    ActivePage = tsLiability
    Align = alClient
    MultiLine = True
    TabOrder = 0
    OnChange = PageControl1Change
    object tsLiability: TTabSheet
      Caption = 'Liabilitites'
      object pnlLiabilityControls: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 63
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object Label9: TevLabel
          Left = 168
          Top = 2
          Width = 54
          Height = 13
          Caption = 'Company #'
        end
        object Label10: TevLabel
          Left = 8
          Top = 2
          Width = 23
          Height = 13
          Caption = 'From'
        end
        object Label11: TevLabel
          Left = 88
          Top = 2
          Width = 13
          Height = 13
          Caption = 'To'
        end
        object Bevel4: TEvBevel
          Left = 0
          Top = 0
          Width = 361
          Height = 64
          Shape = bsFrame
        end
        object Bevel5: TEvBevel
          Left = 359
          Top = 0
          Width = 114
          Height = 64
          Shape = bsFrame
        end
        object Bevel6: TEvBevel
          Left = 471
          Top = 0
          Width = 151
          Height = 64
          Shape = bsFrame
        end
        object ComboBox2: TevComboBox
          Left = 368
          Top = 9
          Width = 97
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 4
          OnChange = ComboBox2Change
          Items.Strings = (
            'Check date'
            'Tax Type')
        end
        object ComboBox3: TevComboBox
          Left = 368
          Top = 32
          Width = 97
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 5
          Visible = False
          Items.Strings = (
            'Outstanding'
            'Not-outstanding'
            'Both')
        end
        object dtpkrFrom: TevDateTimePicker
          Left = 8
          Top = 14
          Width = 81
          Height = 21
          Date = 36515.547249189800000000
          Time = 36515.547249189800000000
          TabOrder = 0
        end
        object dtpkrTo: TevDateTimePicker
          Left = 88
          Top = 14
          Width = 81
          Height = 21
          Date = 36515.547242245400000000
          Time = 36515.547242245400000000
          TabOrder = 1
        end
        object cbCompanyNumber: TevComboBox
          Left = 168
          Top = 14
          Width = 185
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          Sorted = True
          TabOrder = 2
        end
        object cbDateFrom: TevCheckBox
          Left = 8
          Top = 40
          Width = 249
          Height = 17
          Caption = 'Don'#39't show unpaid liab. before date range'
          TabOrder = 3
        end
        object btnOpenCompany: TevBitBtn
          Left = 232
          Top = 37
          Width = 59
          Height = 25
          Caption = 'Open'
          TabOrder = 6
          OnClick = BitBtn12Click
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D88888888888
            8888DDDDDDDDDDDDDDDD0000000000000008888888888888888D0FFF8FFF8FF8
            FF088DDD8DDD8DD8DD8D07778777877877088DDD8DDD8DD8DD8D0FFF8FFF8FF8
            FF088DDD8DDD8DD8DD8D07778777877877088DDD8DDD8DD8DD8D0FFF8FFF8FF8
            FF088DDD8DDD8DD8DD8D07778778877877088DDD8DDF8DD8DD8D088888848888
            8808888888F8F888888D0888884E4888880888888F888F88888D000004EEE400
            000D8888F88888F8888DDDD84EEEEE48DDDDDDDF8888888FDDDDDD84EEEEEEE4
            8DDDDDF888888888FDDDD84EEEEEEEEE48DDDF88888888888FDD84EEEEEEEEEE
            E48DF8888888888888FD4EEEEEEEEEEEEE4D888888888888888D}
          NumGlyphs = 2
        end
        object btnCloseCompany: TevBitBtn
          Left = 296
          Top = 37
          Width = 57
          Height = 25
          Caption = 'Close'
          TabOrder = 7
          OnClick = BitBtn11Click
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D44444444444
            44DDDFFFFFFFFFFFFFFDDEEEEEEEEEEEEEDDD8888888888888DDDDEEEEEEEEEE
            EDDDDD88888888888DDDDDDEEEEEEEEEDDDDDDD888888888DDDDDDDDEEEEEEED
            DDDDDDDD8888888DDDDDD8888EEEEE888888DDDDD88888DDDDDD000000EEE000
            000888888D888D88888D0FFF8FFE8FF8FF088DDD8DD8DDD8DD8D077787778778
            77088DDD8DDD8DD8DD8D0FFF8FFF8FF8FF088DDD8DDD8DD8DD8D077787778778
            77088DDD8DDD8DD8DD8D0FFF8FFF8FF8FF088DDD8DDD8DD8DD8D077787778778
            77088DDD8DDD8DD8DD8D0888888888888808888888888888888D088888888888
            8808888888888888888D000000000000000D888888888888888D}
          NumGlyphs = 2
        end
        object btnAddLiab: TevBitBtn
          Left = 488
          Top = 8
          Width = 113
          Height = 25
          Caption = 'Add liabilities'
          TabOrder = 8
          OnClick = BitBtn1Click
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD222DDDDDD
            DDDDDDDFFFDDDDDDDDDDDDDAA2DDDDD888DDDDD88FDDDDDDDDDDDDDAA2DDDD84
            448DDDD88FDDDDD888DD222AA222284E4448FFF88FFFFD8D888DAAAAAAAA24EE
            444488888888F8DD8888AAAAAAAA28DDDDD888888888F8DDDDD8DDDAA2DDD8DD
            DDD8DDD88FDDD8DDDDD8DDDAA2DDDD8DDD8DDDD88FDDDD8DDD8DDDDAA2DDDD8D
            DD8DDDD88FDDDD8DDD8DDDDDDD8DDDD8D8DDDDDDDDDDDDD8D8DDDDDDDD48DDD8
            48DDDDDDDD8DDDD888DDDDDDD4448DDD4DDDDDDDD888DDDD8DDD44444E444444
            4444888888888888888888888E4488888884DDDDD888DDDDDDD8DDDDDD48DDDD
            DD48DDDDDD8DDDDDDD8DDDDDDD48DDDDDDD4DDDDDD8DDDDDDDD8}
          NumGlyphs = 2
        end
        object btnAddDepos: TevBitBtn
          Left = 488
          Top = 32
          Width = 113
          Height = 25
          Caption = 'Add deposit'
          TabOrder = 9
          OnClick = BitBtn14xClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD222DDDDDD
            DDDDDDDFFFDDDDDDDDDD888AA2DDD008DDDD88D88FDDD88DDDDDDDDAA2DD07F0
            8DDDDDD88FDD8DD8DDDD222AA22227FF08DDFFF88FFFFDDD8DDDAAAAAAAA207F
            F08D88888888F8DDD8DDAAAAAAAA2D07FF0D88888888FD8DDD8DDDDAA2DDDDD0
            770DDDD88FDDDDD8DD8D888AA2DDDDDD00DD88D88FDDDDDD88DDDDDAA2DDDDDD
            DDDDDDD88FDDDDDDDDDD0000000000000000888DD88888888888000000000000
            00008888888888888888770F70F707F0F700DD8DD8DD8DD8DD887F07707F0F70
            7700DD8DD8DD8DD8DD88F70F70770770F700DD8DD8DD8DD8DD88000000000000
            0000888888888888888800000000000000008888888888888888}
          NumGlyphs = 2
        end
      end
      inline Frame_4_DB_GRIDS2: TFrame_4_DB_GRIDS
        Left = 0
        Top = 63
        Width = 427
        Height = 175
        Align = alClient
        TabOrder = 1
        inherited Splitter3: TevSplitter
          Width = 410
        end
        inherited Panel3: TevPanel
          Width = 410
          inherited GroupBox1: TevGroupBox
            Caption = 'Companies'
            inherited wwDBGrid1: TevDBGrid
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            end
          end
          inherited GroupBox2: TevGroupBox
            Width = 105
            Caption = 'Subtotals by payroll/check date'
            inherited wwDBGrid2: TevDBGrid
              Width = 101
            end
          end
        end
        inherited Panel4: TevPanel
          Width = 410
          Height = 0
          inherited Splitter1: TevSplitter
            Height = 232
          end
          inherited GroupBox3: TevGroupBox
            Height = 232
            Caption = 'Selected payroll by agency'
            inherited wwDBGrid3: TevDBGrid
              Height = 215
            end
          end
          inherited GroupBox4: TevGroupBox
            Width = 105
            Height = 232
            Caption = 'Selected liabilities'
            inherited wwDBGrid4: TevDBGrid
              Width = 101
              Height = 215
            end
          end
        end
      end
    end
    object tsDeposit: TTabSheet
      Caption = 'Deposits'
      ImageIndex = 1
      object pnlDepositControls: TevPanel
        Left = 0
        Top = 0
        Width = 427
        Height = 49
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        Visible = False
        DesignSize = (
          427
          49)
        object BitBtn10: TevBitBtn
          Left = 1701
          Top = 16
          Width = 25
          Height = 25
          Hint = 'Create Liability Record'
          Anchors = [akTop, akRight]
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
        end
        object BitBtn9: TevBitBtn
          Left = 1669
          Top = 16
          Width = 25
          Height = 25
          Hint = 'Create Liability Record'
          Anchors = [akTop, akRight]
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BitBtn9Click
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
        end
        object BitBtn13: TevBitBtn
          Left = 1621
          Top = 16
          Width = 41
          Height = 25
          Hint = 'Create Liability Record'
          Anchors = [akTop, akRight]
          Caption = '<->'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BitBtn14xClick
          NumGlyphs = 2
        end
        object BitBtn14: TevBitBtn
          Left = 1290
          Top = 16
          Width = 41
          Height = 25
          Hint = 'Create Liability Record'
          Anchors = [akTop, akRight]
          Caption = '<->'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BitBtn14xClick
          NumGlyphs = 2
        end
        object BitBtn20: TevBitBtn
          Left = 1338
          Top = 16
          Width = 25
          Height = 25
          Hint = 'Create Liability Record'
          Anchors = [akTop, akRight]
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BitBtn1Click
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
        end
        object BitBtn21: TevBitBtn
          Left = 1370
          Top = 16
          Width = 25
          Height = 25
          Hint = 'Create Liability Record'
          Anchors = [akTop, akRight]
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
        end
      end
      inline Frame_4_DB_GRIDS1: TFrame_4_DB_GRIDS
        Left = 0
        Top = 49
        Width = 427
        Height = 189
        Align = alClient
        TabOrder = 1
        inherited Splitter3: TevSplitter
          Width = 427
        end
        inherited Panel3: TevPanel
          Width = 427
          inherited GroupBox1: TevGroupBox
            Caption = 'Companies'
            inherited wwDBGrid1: TevDBGrid
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            end
          end
          inherited GroupBox2: TevGroupBox
            Width = 414
            Caption = 'Deposit groups'
            inherited wwDBGrid2: TevDBGrid
              Width = 410
            end
          end
        end
        inherited Panel4: TevPanel
          Width = 427
          Height = 7
          inherited Splitter1: TevSplitter
            Height = 262
          end
          inherited GroupBox3: TevGroupBox
            Height = 262
            Caption = 'Deposits'
            inherited wwDBGrid3: TevDBGrid
              Height = 245
            end
          end
          inherited GroupBox4: TevGroupBox
            Width = 414
            Height = 262
            Caption = 'Paid liabilities'
            inherited wwDBGrid4: TevDBGrid
              Width = 410
              Height = 245
            end
          end
        end
        inherited wwDataSource4: TevDataSource
          Left = 528
          Top = 64
        end
      end
    end
    object CreateManualRecords: TTabSheet
      Caption = 'CreateManualRecords'
      ImageIndex = 3
      TabVisible = False
    end
    object tsLiabilityDetail: TTabSheet
      Caption = 'Liability Detail'
      ImageIndex = 4
      TabVisible = False
      object Memo1: TevMemo
        Left = 128
        Top = 56
        Width = 185
        Height = 89
        Lines.Strings = (
          'Same a current liabilities')
        TabOrder = 0
      end
    end
    object tsDepositDetail: TTabSheet
      Caption = 'Deposit Record Detail'
      ImageIndex = 5
      TabVisible = False
    end
    object tsCreateManualLiabilities: TTabSheet
      Caption = 'Create Manual Liability Records'
      ImageIndex = 6
      object Bevel1: TEvBevel
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
      end
      object ManualNotebook: TNotebook
        Left = 0
        Top = 0
        Width = 427
        Height = 238
        Align = alClient
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'EnterCompany'
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Detail'
          object Bevel3: TEvBevel
            Left = 0
            Top = 0
            Width = 427
            Height = 49
            Align = alTop
          end
          object lblCompanyNameAndNumber: TevLabel
            Left = 9
            Top = 4
            Width = 205
            Height = 16
            Caption = 'lblCompanyNameAndNumber'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblClientNameAndNumber: TevLabel
            Left = 9
            Top = 28
            Width = 179
            Height = 16
            Caption = 'lblClientNameAndNumber'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label7: TevLabel
            Left = 8
            Top = 56
            Width = 82
            Height = 13
            Caption = 'Enter check date'
          end
          object Label8: TevLabel
            Left = 208
            Top = 56
            Width = 70
            Height = 13
            Caption = 'Enter due date'
          end
          object Label2: TevLabel
            Left = 704
            Top = 216
            Width = 63
            Height = 13
            Caption = 'Total for 941:'
          end
          object lTot940: TevLabel
            Left = 704
            Top = 232
            Width = 44
            Height = 13
            Caption = 'lTot940'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TevLabel
            Left = 704
            Top = 256
            Width = 63
            Height = 13
            Caption = 'Total for 940:'
          end
          object lTot941: TevLabel
            Left = 704
            Top = 272
            Width = 44
            Height = 13
            Caption = 'lTot941'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label5: TevLabel
            Left = 408
            Top = 56
            Width = 31
            Height = 13
            Caption = 'Payroll'
          end
          object wwDBGrid2: TevDBGrid
            Left = 9
            Top = 97
            Width = 684
            Height = 345
            DisableThemesInTitle = False
            ControlInfoInDataSet = True
            Selected.Strings = (
              'TaxName'#9'40'#9'Tax'#9'F'
              'TaxAmount'#9'10'#9'Amount'
              'TaxStatus'#9'20'#9'Status'#9'F'
              'TaxAdjType'#9'20'#9'Adj. type'#9'F'
              'TaxThirdParty'#9'5'#9'3'#39'rd prty'#9'F'
              'TaxCollected'#9'5'#9'Collected'#9'F')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TProcessCombinedLiabilitiesAndDeposits\wwDBGrid2'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 1
            ShowHorzScrollBar = True
            DataSource = dsLiabTypes
            KeyOptions = [dgEnterToTab]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            ReadOnly = False
            TabOrder = 3
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnEnter = wwDBGrid2Enter
            OnKeyDown = wwDBGrid2KeyDown
            PaintOptions.AlternatingRowColor = clCream
            Sorting = False
          end
          object BitBtn6: TevBitBtn
            Left = 704
            Top = 336
            Width = 81
            Height = 25
            Caption = 'OK'
            Default = True
            TabOrder = 6
            OnClick = BitBtn6Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
              DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
              DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
              DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
              DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
              DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
              2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
              A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
              AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
            NumGlyphs = 2
          end
          object BitBtn8: TevBitBtn
            Left = 704
            Top = 400
            Width = 81
            Height = 25
            Cancel = True
            Caption = 'Cancel'
            TabOrder = 8
            OnClick = BitBtn7Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
              DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
              9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
              DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
              DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
              DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
              91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
              999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
            NumGlyphs = 2
          end
          object DateTimePicker1: TevDateTimePicker
            Left = 98
            Top = 56
            Width = 85
            Height = 21
            Date = 36515.615404513890000000
            Time = 36515.615404513890000000
            TabOrder = 0
          end
          object DateTimePicker2: TevDateTimePicker
            Left = 288
            Top = 56
            Width = 85
            Height = 21
            Date = 36515.615404513890000000
            Time = 36515.615404513890000000
            TabOrder = 1
          end
          object StaticText1: TStaticText
            Left = 8
            Top = 79
            Width = 290
            Height = 17
            Caption = 
              'Fill out amount to create liabilities or leave them empty to ski' +
              'p'
            TabOrder = 4
          end
          object wwDBComboBox1: TevDBComboBox
            Left = 112
            Top = 224
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TaxStatus'
            DataSource = dsLiabTypes
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 9
            UnboundDataType = wwDefault
          end
          object wwDBLookupCombo1: TevDBLookupCombo
            Left = 448
            Top = 56
            Width = 161
            Height = 21
            DropDownAlignment = taLeftJustify
            Selected.Strings = (
              'CHECK_DATE'#9'10'#9'Check date'
              'RUN_NUMBER'#9'4'#9'Run #')
            Options = [loColLines, loTitles]
            Style = csDropDownList
            TabOrder = 2
            AutoDropDown = True
            ShowButton = True
            PreciseEditRegion = False
            AllowClearKey = True
            ShowMatchText = True
          end
          object wwDBComboBox2: TevDBComboBox
            Left = 112
            Top = 256
            Width = 121
            Height = 21
            ShowButton = True
            Style = csDropDownList
            MapList = True
            AllowClearKey = False
            AutoDropDown = True
            DataField = 'TaxStatus'
            DataSource = dsLiabTypes
            DropDownCount = 8
            ItemHeight = 0
            Picture.PictureMaskFromDataSet = False
            Sorted = False
            TabOrder = 10
            UnboundDataType = wwDefault
          end
          object BitBtn1: TevBitBtn
            Left = 704
            Top = 368
            Width = 81
            Height = 25
            Caption = 'One more'
            Default = True
            TabOrder = 7
            OnClick = BitBtn6Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD2DDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDD8DDDDDDDDDDDDD2AA2DD
              DDDDDDDDDDD88DDDDDDDDDDDD2AAAA2DDDDDDDDDDD8888DDDDDDDDDD2AAAAA2D
              DDDDDDDDD88888DDDDDDDDD2AAAFAAA2DDDDDDDD8888888DDDDDDD2A288DAAA2
              DDDDDDD8888D888DDDDDD2A4448DFAAA2DDDDD8FFFDD8888DDDDDD7EE48DDAAA
              2DDDDDD88FDDD888DDDDD88EE48888AAA2DDDDD88FDDD8888DDD444EE44448AA
              A2DDFFF88FFFFD888DDDEEEEEEEE48FAAA2D88888888FD8888DDEEEEEEEE4DDA
              AA2D88888888FDD888DDDDDEE48DDDDFAAA2DDD88FDDDDD8888DDDDEE48DDDDD
              AAA2DDD88FDDDDDD888DDDDEE4DDDDDDFFFDDDD88FDDDDDD888D}
            NumGlyphs = 2
          end
          object cbAutoAttachDeposit: TevCheckBox
            Left = 704
            Top = 304
            Width = 89
            Height = 17
            Caption = 'Create deposit'
            Checked = True
            State = cbChecked
            TabOrder = 5
          end
        end
      end
    end
    object tsCreateTaxDepositRecord: TTabSheet
      Caption = 'Create tax deposit record'
      ImageIndex = 6
      object Bevel2: TEvBevel
        Left = 0
        Top = 49
        Width = 427
        Height = 189
        Align = alClient
      end
      object Bevel7: TEvBevel
        Left = 0
        Top = 0
        Width = 427
        Height = 49
        Align = alTop
      end
      object lblCompanyNameAndNumber2: TevLabel
        Left = 9
        Top = 4
        Width = 205
        Height = 16
        Caption = 'lblCompanyNameAndNumber'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblClientNameAndNumber2: TevLabel
        Left = 9
        Top = 28
        Width = 179
        Height = 16
        Caption = 'lblClientNameAndNumber'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ManualNotebook2: TNotebook
        Left = 0
        Top = 49
        Width = 427
        Height = 189
        Align = alClient
        PageIndex = 1
        TabOrder = 0
        object TPage
          Left = 0
          Top = 0
          Caption = 'Default'
          object Label1: TevLabel
            Left = 8
            Top = 8
            Width = 166
            Height = 13
            Caption = 'Select tax deposit type and agency'
          end
          object Label6: TevLabel
            Left = 296
            Top = 112
            Width = 67
            Height = 13
            Caption = 'Payment Date'
            Visible = False
          end
          object Label13: TevLabel
            Left = 296
            Top = 136
            Width = 56
            Height = 13
            Caption = 'Status Date'
          end
          object Label15: TevLabel
            Left = 296
            Top = 168
            Width = 60
            Height = 13
            Caption = 'Reference #'
          end
          object wwDBGrid3: TevDBGrid
            Left = 8
            Top = 56
            Width = 265
            Height = 329
            DisableThemesInTitle = False
            Selected.Strings = (
              'Name'#9'40'#9'Agency name'#9'T')
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TProcessCombinedLiabilitiesAndDeposits\wwDBGrid3'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsAgencies
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            PaintOptions.AlternatingRowColor = clCream
          end
          object BitBtn5: TevBitBtn
            Left = 526
            Top = 320
            Width = 75
            Height = 25
            Cancel = True
            Caption = 'Cancel'
            TabOrder = 2
            OnClick = BitBtn5Click
            Glyph.Data = {
              DE010000424DDE01000000000000760000002800000024000000120000000100
              0400000000006801000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              333333333333333333333333000033338833333333333333333F333333333333
              0000333911833333983333333388F333333F3333000033391118333911833333
              38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
              911118111118333338F3338F833338F3000033333911111111833333338F3338
              3333F8330000333333911111183333333338F333333F83330000333333311111
              8333333333338F3333383333000033333339111183333333333338F333833333
              00003333339111118333333333333833338F3333000033333911181118333333
              33338333338F333300003333911183911183333333383338F338F33300003333
              9118333911183333338F33838F338F33000033333913333391113333338FF833
              38F338F300003333333333333919333333388333338FFF830000333333333333
              3333333333333333333888330000333333333333333333333333333333333333
              0000}
            NumGlyphs = 2
          end
          object BitBtn15: TevBitBtn
            Left = 526
            Top = 288
            Width = 75
            Height = 25
            Caption = 'Next'
            TabOrder = 3
            OnClick = BitBtn15Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              3333333333333333333333333333333333333FFF333333333333000333333333
              3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
              3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
              0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
              BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
              33337777773FF733333333333300033333333333337773333333333333333333
              3333333333333333333333333333333333333333333333333333333333333333
              3333333333333333333333333333333333333333333333333333}
            NumGlyphs = 2
          end
          object cbDepositType: TevComboBox
            Left = 8
            Top = 26
            Width = 209
            Height = 21
            Style = csDropDownList
            ItemHeight = 0
            Sorted = True
            TabOrder = 0
          end
          object Memo2: TevMemo
            Left = 296
            Top = 56
            Width = 241
            Height = 41
            BorderStyle = bsNone
            Color = clBtnFace
            Lines.Strings = (
              'All agencies which company uses are shown.'
              'Some of them may not have unpaid liabilities '
              'in selected date range.')
            TabOrder = 4
          end
          object crdepPaymDate: TevDateTimePicker
            Left = 386
            Top = 112
            Width = 85
            Height = 21
            Date = 36515.615404513890000000
            Time = 36515.615404513890000000
            TabOrder = 5
            Visible = False
          end
          object crdepStatusDate: TevDateTimePicker
            Left = 386
            Top = 136
            Width = 85
            Height = 21
            Date = 36515.615404513890000000
            Time = 36515.615404513890000000
            TabOrder = 6
            OnChange = crdepStatusDateChange
          end
          object crdepRef: TevEdit
            Left = 384
            Top = 168
            Width = 121
            Height = 21
            TabOrder = 7
          end
        end
        object TPage
          Left = 0
          Top = 0
          Caption = 'Select liabilities'
          object Label3: TevLabel
            Left = 8
            Top = 8
            Width = 84
            Height = 13
            Caption = 'Available liabilities'
          end
          object Label4: TevLabel
            Left = 8
            Top = 136
            Width = 83
            Height = 13
            Caption = 'Selected liabilities'
          end
          object Label14: TevLabel
            Left = 8
            Top = 352
            Width = 38
            Height = 13
            Caption = 'Label14'
          end
          object wwDBGrid4: TevDBGrid
            Left = 8
            Top = 24
            Width = 561
            Height = 97
            DisableThemesInTitle = False
            Selected.Strings = (
              'DUE_DATE'#9'10'#9'Due date'#9'F'
              'Check_date'#9'18'#9'Check date'#9'F'
              'Name'#9'20'#9'Description'#9'F'
              'Status'#9'15'#9'Status'#9'F'
              'Amount'#9'10'#9'Amount'#9)
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TProcessCombinedLiabilitiesAndDeposits\wwDBGrid4'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsLiabAvail
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 0
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnDblClick = wwDBGrid4DblClick
            PaintOptions.AlternatingRowColor = clCream
          end
          object wwDBGrid5: TevDBGrid
            Left = 8
            Top = 152
            Width = 601
            Height = 193
            DisableThemesInTitle = False
            Selected.Strings = (
              'DUE_DATE'#9'10'#9'Due date'
              'Check_date'#9'18'#9'Check date'#9'F'
              'Name'#9'20'#9'Description'
              'Status'#9'15'#9'Status'#9
              'Amount'#9'10'#9'Amount'#9)
            IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
            IniAttributes.SectionName = 'TProcessCombinedLiabilitiesAndDeposits\wwDBGrid5'
            IniAttributes.Delimiter = ';;'
            ExportOptions.ExportType = wwgetSYLK
            ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
            TitleColor = clBtnFace
            FixedCols = 0
            ShowHorzScrollBar = True
            DataSource = dsLiabSelected
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
            TabOrder = 1
            TitleAlignment = taLeftJustify
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 1
            OnDblClick = wwDBGrid5DblClick
            PaintOptions.AlternatingRowColor = clCream
          end
          object BitBtn16: TevBitBtn
            Left = 576
            Top = 24
            Width = 33
            Height = 25
            TabOrder = 2
            TabStop = False
            OnClick = BitBtn16Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD8888888DD
              DDDDDDDFFFFFFFDDDDDDDDD6666666DDDDDDDDD8888888DDDDDDDDD6E6666688
              888DDDD8888888FFFFFFDDD6E60000000000DDD8888888888888DDD6E60FFFFF
              FFF0DDD8888DDDDDDDD8DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6EE000000
              0000DDD8888888888888DDD6EEEEE6DDDDDDDDD8888888DDDDDD8886EEEEE688
              8DDDFFF8888888FFFDDD6666EEEEE6666DDD8888888888888DDDD6EEEEEEEEE6
              DDDDD88888888888DDDDDD6EEEEEEE6DDDDDDD888888888DDDDDDDD6EEEEE6DD
              DDDDDDD8888888DDDDDDDDDD6EEE6DDDDDDDDDDD88888DDDDDDDDDDDD6E6DDDD
              DDDDDDDDD888DDDDDDDDDDDDDD6DDDDDDDDDDDDDDD8DDDDDDDDD}
            NumGlyphs = 2
          end
          object BitBtn17: TevBitBtn
            Left = 576
            Top = 56
            Width = 33
            Height = 25
            TabOrder = 3
            TabStop = False
            OnClick = BitBtn17Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
              DDDDDDDDDDFDDDDDDDDDDDDDD868DDDDDDDDDDDDDF8FDDDDDDDDDDDD86E68DDD
              DDDDDDDDF888FDDDDDDDDDD86EEE68DDDDDDDDDF88888FDDDDDDDD86EEEEE68D
              DDDDDDF8888888FDDDDDD86EEEEEEE68DDDDDF888888888FDDDD86EEEEEEEEE6
              8DDDF88888888888FDDD6666EEEEE6666DDD8888888888888DDDDDD6EEEEE6DD
              DDDDDDD8888888DDDDDDDDD6E6666688888DDDD8888888FFFFFFDDD6E6000000
              0000DDD8888888888888DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6E60FFFFF
              FFF0DDD8888DDDDDDDD8DDD6EE0000000000DDD8888888888888DDD6EEEEE6DD
              DDDDDDD8888888DDDDDDDDD6666666DDDDDDDDD8888888DDDDDD}
            NumGlyphs = 2
          end
          object BitBtn18: TevBitBtn
            Left = 528
            Top = 360
            Width = 75
            Height = 25
            Cancel = True
            Caption = 'Cancel'
            TabOrder = 6
            OnClick = BitBtn5Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDDDDDDDDDDDDDDDDDDDD1DDDDDDDDDDDDDDDFDDDDDDDDDDDDDDD91DDDDDDD
              DDDDDDD8FDDDDDDDDDDDDDD91DDDDDDD1DDDDDD8FDDDDDDDFDDDDDDD91DDDDD1
              9DDDDDDD8FDDDDDF8DDDDDDD991DDDD19DDDDDDD88FDDDDF8DDDDDDDD991DD19
              DDDDDDDDD88FDDF8DDDDDDDDDD991199DDDDDDDDDD88FF88DDDDDDDDDD99999D
              DDDDDDDDDD88888DDDDDDDDDD119991DDDDDDDDDDFF888FDDDDDDDD119999991
              DDDDDDDFF888888FDDDDDD19999999991DDDDDF888888888FDDDDD99999DD999
              91DDDD88888DD8888FDDDD9999DDDD99991DDD8888DDDD8888FDDD99DDDDDDD9
              999DDD88DDDDDDD8888DDDDDDDDDDDDD99DDDDDDDDDDDDDD88DD}
            NumGlyphs = 2
          end
          object BitBtn19: TevBitBtn
            Left = 350
            Top = 360
            Width = 75
            Height = 25
            Caption = 'OK'
            Default = True
            TabOrder = 4
            OnClick = BitBtn19Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDD2DDDD
              DDDDDDDDDDDFDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDF8FDDDDDDDDDDDD2AA2DDD
              DDDDDDDDDF88FDDDDDDDDDDD2AAAA2DDDDDDDDDDF8888FDDDDDDDDD2AAAAA2DD
              DDDDDDDF88888FDDDDDDDD2AAAFAAA2DDDDDDDF8888888FDDDDDD2AAFFDAAA2D
              DDDDDF8888D888FDDDDD2AAFDDDFAAA2DDDDF888DDD8888FDDDDDFFDDDDDAAA2
              DDDDD88DDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAAA
              2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDAA
              A2DDDDDDDDDDDD888FDDDDDDDDDDDDFAAA2DDDDDDDDDDD8888FDDDDDDDDDDDDA
              AA2DDDDDDDDDDDD888FDDDDDDDDDDDDFFFDDDDDDDDDDDDD888DD}
            NumGlyphs = 2
          end
          object sbtnMoveThemAll: TevBitBtn
            Left = 576
            Top = 96
            Width = 33
            Height = 25
            TabOrder = 7
            TabStop = False
            OnClick = sbtnMoveThemAllClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD8DDDDD
              DDDDDDDDDDFDDDDDDDDDDDDDD868DDDDDDDDDDDDDF8FDDDDDDDDDDDD86E68DDD
              DDDDDDDDF888FDDDDDDDDDD86EEE68DDDDDDDDDF88888FDDDDDDDD86E6666888
              888DDDF8888888FFFFFFD86EE60000000000DF8888888888888886EEE60FFFFF
              FFF0F888888DDDDDDDD86666E60FFFFFFFF08888888DDDDDDDD8DDD6E6000000
              0000DDD8888888888888DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6E60FFFFF
              FFF0DDD8888DDDDDDDD8DDD6E60000000000DDD8888888888888DDD6E60FFFFF
              FFF0DDD8888DDDDDDDD8DDD6E60FFFFFFFF0DDD8888DDDDDDDD8DDD6EE000000
              0000DDD8888888888888DDD6666666DDDDDDDDD8888888DDDDDD}
            NumGlyphs = 2
          end
          object evBitBtn1: TevBitBtn
            Left = 432
            Top = 360
            Width = 81
            Height = 25
            Caption = 'One more'
            Default = True
            TabOrder = 5
            OnClick = BitBtn19Click
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD2DDD
              DDDDDDDDDDDDDDDDDDDDDDDDDDD2A2DDDDDDDDDDDDDD8DDDDDDDDDDDDD2AA2DD
              DDDDDDDDDDD88DDDDDDDDDDDD2AAAA2DDDDDDDDDDD8888DDDDDDDDDD2AAAAA2D
              DDDDDDDDD88888DDDDDDDDD2AAAFAAA2DDDDDDDD8888888DDDDDDD2A288DAAA2
              DDDDDDD8888D888DDDDDD2A4448DFAAA2DDDDD8FFFDD8888DDDDDD7EE48DDAAA
              2DDDDDD88FDDD888DDDDD88EE48888AAA2DDDDD88FDDD8888DDD444EE44448AA
              A2DDFFF88FFFFD888DDDEEEEEEEE48FAAA2D88888888FD8888DDEEEEEEEE4DDA
              AA2D88888888FDD888DDDDDEE48DDDDFAAA2DDD88FDDDDD8888DDDDEE48DDDDD
              AAA2DDD88FDDDDDD888DDDDEE4DDDDDDFFFDDDD88FDDDDDD888D}
            NumGlyphs = 2
          end
        end
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 149
    Top = 90
  end
  inherited wwdsDetail: TevDataSource
    Left = 206
    Top = 98
  end
  inherited wwdsList: TevDataSource
    Left = 106
    Top = 82
  end
  object cdLiabTypes: TevClientDataSet
    ControlType.Strings = (
      'TaxStatus;CustomEdit;wwDBComboBox1;F'
      'TaxThirdParty;CheckBox;Y;N'
      'TaxCollected;CheckBox;Y;N'
      'TaxAdjType;CustomEdit;wwDBComboBox2')
    FieldDefs = <
      item
        Name = 'TaxName'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TaxAmount'
        DataType = ftCurrency
      end
      item
        Name = 'TaxStatus'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TaxAdjType'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TaxThirdParty'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TaxTable'
        DataType = ftInteger
      end
      item
        Name = 'TaxType'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TaxNbr'
        DataType = ftInteger
      end
      item
        Name = 'TaxCollected'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TaxAgencyNbr'
        DataType = ftInteger
      end>
    AfterPost = cdLiabTypesAfterPost
    BeforeScroll = cdLiabTypesBeforeScroll
    AfterScroll = cdLiabTypesAfterScroll
    Left = 52
    Top = 376
    object cdLiabTypesTaxName: TStringField
      DisplayLabel = 'Tax'
      DisplayWidth = 40
      FieldName = 'TaxName'
      Size = 40
    end
    object cdLiabTypesTaxAmount: TCurrencyField
      DisplayLabel = 'Amount'
      DisplayWidth = 10
      FieldName = 'TaxAmount'
    end
    object cdLiabTypesTaxStatus: TStringField
      DisplayWidth = 15
      FieldName = 'TaxStatus'
      Size = 1
    end
    object cdLiabTypesTaxAdjType: TStringField
      DisplayWidth = 1
      FieldName = 'TaxAdjType'
      Size = 1
    end
    object cdLiabTypesTaxThirdParty: TStringField
      DisplayWidth = 1
      FieldName = 'TaxThirdParty'
      Size = 1
    end
    object cdLiabTypesTaxTable: TIntegerField
      FieldName = 'TaxTable'
      Visible = False
    end
    object cdLiabTypesTaxType: TStringField
      FieldName = 'TaxType'
      Visible = False
      Size = 1
    end
    object cdLiabTypesTaxNbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'TaxNbr'
      Visible = False
    end
    object cdLiabTypesTaxCollected: TStringField
      FieldName = 'TaxCollected'
      Size = 1
    end
    object cdLiabTypesAgencyNbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'TaxAgencyNbr'
      Visible = False
    end
  end
  object dsLiabTypes: TevDataSource
    DataSet = cdLiabTypes
    Left = 20
    Top = 376
  end
  object cdAgencies: TevClientDataSet
    FieldDefs = <
      item
        Name = 'Name'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'AgencyNbr'
        DataType = ftInteger
      end>
    Left = 52
    Top = 408
    object cdAgenciesName: TStringField
      DisplayLabel = 'Agency name'
      DisplayWidth = 40
      FieldName = 'Name'
      Size = 40
    end
    object cdAgenciesAgencyNbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'AgencyNbr'
      Visible = False
    end
  end
  object dsAgencies: TevDataSource
    DataSet = cdAgencies
    Left = 20
    Top = 408
  end
  object cdLiabSelected: TevClientDataSet
    FieldDefs = <
      item
        Name = 'DUE_DATE'
        DataType = ftDate
      end
      item
        Name = 'Check_date'
        DataType = ftDateTime
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Status'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Amount'
        DataType = ftCurrency
      end
      item
        Name = 'TaxTable'
        DataType = ftInteger
      end
      item
        Name = 'Nbr'
        DataType = ftInteger
      end>
    Left = 52
    Top = 337
    object cdLiabSelectedDUE_DATE: TDateField
      DisplayLabel = 'Due date'
      DisplayWidth = 10
      FieldName = 'DUE_DATE'
    end
    object cdLiabSelectedCheck_date: TDateTimeField
      DisplayWidth = 18
      FieldName = 'Check_date'
    end
    object cdLiabSelectedName: TStringField
      DisplayLabel = 'Description'
      DisplayWidth = 20
      FieldName = 'Name'
      Size = 40
    end
    object cdLiabSelectedStatus: TStringField
      DisplayWidth = 15
      FieldName = 'Status'
      Size = 15
    end
    object cdLiabSelectedAmount: TCurrencyField
      DisplayWidth = 10
      FieldName = 'Amount'
      DisplayFormat = '#,##0.00'
    end
    object cdLiabSelectedTaxTable: TIntegerField
      FieldName = 'TaxTable'
      Visible = False
    end
    object cdLiabSelectedNbr: TIntegerField
      FieldName = 'Nbr'
      Visible = False
    end
  end
  object cdLiabAvail: TevClientDataSet
    FieldDefs = <
      item
        Name = 'DUE_DATE'
        DataType = ftDate
      end
      item
        Name = 'Check_date'
        DataType = ftDateTime
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Status'
        DataType = ftString
        Size = 15
      end
      item
        Name = 'Amount'
        DataType = ftCurrency
      end
      item
        Name = 'TaxTable'
        DataType = ftInteger
      end
      item
        Name = 'Nbr'
        DataType = ftInteger
      end>
    Left = 52
    Top = 305
    object cdLiabAvailDUE_DATE: TDateField
      DisplayLabel = 'Due date'
      DisplayWidth = 10
      FieldName = 'DUE_DATE'
    end
    object cdLiabAvailCheck_date: TDateTimeField
      FieldName = 'Check_date'
    end
    object cdLiabAvailName: TStringField
      DisplayWidth = 40
      FieldName = 'Name'
      Size = 40
    end
    object cdLiabAvailStatus: TStringField
      FieldName = 'Status'
      Size = 15
    end
    object cdLiabAvailAmount: TCurrencyField
      DisplayWidth = 10
      FieldName = 'Amount'
      DisplayFormat = '#,##0.00'
    end
    object cdLiabAvailTaxTable: TIntegerField
      DisplayWidth = 10
      FieldName = 'TaxTable'
    end
    object cdLiabAvailNbr: TIntegerField
      DisplayWidth = 10
      FieldName = 'Nbr'
    end
  end
  object dsLiabAvail: TevDataSource
    DataSet = cdLiabAvail
    Left = 20
    Top = 305
  end
  object dsLiabSelected: TevDataSource
    DataSet = cdLiabSelected
    Left = 20
    Top = 337
  end
end
