// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SBalanceReportScreen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, Wwdbigrd, Wwdbgrid, Wwdatsrc, EvBasicUtils, EvContext,
   StdCtrls, ComCtrls, SFrameEntry,  SBalanceInformation,
  ExtCtrls, Buttons, Variants, ISBasicClasses, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvExceptions, EvUIComponents,
  EvUIUtils, EvClientDataSet;

type
  TBalanceReportScreen = class(TFrameEntry)
    cdClCo: TevClientDataSet;
    cdClCoCL_NBR: TIntegerField;
    cdClCoCUSTOM_CLIENT_NUMBER: TStringField;
    cdClCoCLIENT_NAME: TStringField;
    cdClCoCO_NBR: TIntegerField;
    cdClCoCUSTOM_COMPANY_NUMBER: TStringField;
    cdClCoCOMPANY_NAME: TStringField;
    cdClCoTAX_SERVICE: TStringField;
    dsClCo: TevDataSource;
    Pc: TevPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    cbQuarter: TevComboBox;
    cbYear: TevEdit;
    UpDown1: TUpDown;
    wwDBGrid1: TevDBGrid;
    cdClCoLOAD: TBooleanField;
    cdClCoDESCRI: TStringField;
    cbConsolidate: TevCheckBox;
    cdClCoCONSOL: TBooleanField;
    dsSummary: TevDataSource;
    dsCoSum: TevDataSource;
    evDBGrid1: TevDBGrid;
    cdsCoSum: TevClientDataSet;
    cdsCoSumClName: TStringField;
    cdsCoSumCoName: TStringField;
    cdsCoSumClNumber: TStringField;
    cdsCoSumCoNumber: TStringField;
    cdsCoSumClNbr: TIntegerField;
    cdsCoSumCoNbr: TIntegerField;
    cdsCoSumConsolidation: TBooleanField;
    cdsCoSumOutBalance: TBooleanField;
    evSplitter1: TevSplitter;
    evDBGrid2: TevDBGrid;
    cdsSummary: TevClientDataSet;
    cbWholeYear: TCheckBox;
    ts3: TTabSheet;
    gWageDetails: TevDBGrid;
    dsWageDetails: TevDataSource;
    ts4: TTabSheet;
    dsLiabDetails: TevDataSource;
    gLiabDetails: TevDBGrid;
    cdsSummaryClNbr: TIntegerField;
    cdsSummaryCoNbr: TIntegerField;
    cdsSummarySortKey: TStringField;
    cdsSummaryTextDescription: TStringField;
    cdsSummaryAmountWages: TCurrencyField;
    cdsSummaryAmountTaxHist: TCurrencyField;
    cdsSummaryAmountTaxLiab: TCurrencyField;
    cdsSummaryWagesDSName: TStringField;
    cdsSummaryLiabDSName: TStringField;
    cdsSummaryDifLiabHist: TCurrencyField;
    cdsSummaryDifWageHist: TCurrencyField;
    cdsSummaryClName: TStringField;
    cdsSummaryCoName: TStringField;
    cdsSummaryClNumber: TStringField;
    cdsSummaryCoNumber: TStringField;
    cdsSummaryConsolidation: TBooleanField;
    cdsSummaryTaxPercent: TFloatField;
    Provider: TisProvider;
    cdsSummaryTaxAmount: TCurrencyField;
    cdsSummaryMargin: TFloatField;
    cdsCoSumMargin: TFloatField;
    Button4: TevBitBtn;
    Button5: TevBitBtn;
    Button1: TevBitBtn;
    cdsCoSumOutBalWages: TBooleanField;
    cdsSummaryAmountEE_Tax_: TCurrencyField;
    cdsSummaryAmountER_Tax_: TCurrencyField;
    cdsSummaryDifWageHist_EE_ER: TCurrencyField;
    cdsCoSumOutBalance_: TBooleanField;
    cdsCoSumOutBalance_EE_ER: TBooleanField;
    cdsSummaryFICAEE_Wages_: TCurrencyField;
    cdsSummaryFICADifWageHist_: TCurrencyField;
    procedure UpDown1Click(Sender: TObject; Button: TUDBtnType);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure cdsCoSumAfterScroll(DataSet: TDataSet);
    procedure cbWholeYearClick(Sender: TObject);
    procedure PcChange(Sender: TObject);
    procedure evDBGrid2DblClick(Sender: TObject);
  private
    { Private declarations }
    FBegDate, FEndDate: TDateTime;
    FBalanceInformation: TBalanceDM;
    procedure UpdateAll(const Value: Boolean);
  public
    { Public declarations }
    procedure Activate; override;
    procedure DeActivate; override;
    procedure ActivateForQueue(BalanceDM: TBalanceDM);
  end;

var
  BalanceReportScreen: TBalanceReportScreen;

implementation

{$R *.DFM}

uses
  SFieldCodeValues, EvUtils, SDataStructure, EvTypes, isBaseClasses, EvConsts;

const
  sCompany = 'Company';
  sConsolidation = 'Consolidation';

procedure TBalanceReportScreen.Activate;
var
  d: TevClientDataSet;
  ReadOnlyClientList : IisStringList;

  procedure AddRecord(const Title: string; const Cons: Boolean);
  var
    i: Integer;
    f: TField;
  begin
    cdClCo.Append;
    for i := 0 to cdClCo.FieldCount-1 do
    begin
      f := d.FindField(cdClCo.Fields[i].FieldName);
      if Assigned(f) then
        cdClCo.Fields[i].Assign(f);
    end;
    cdClCo['DESCR'] := Title;
    cdClCo['CONSOLIDATED'] := Cons;
    cdClCo.Post;
  end;
begin
  inherited;
  cdClCo.CreateDataSet;
  cdClCo.LogChanges := False;
  d := ctx_DataAccess.TEMP_CUSTOM_VIEW;
  d.Close;
  d.DataRequest(TExecDSWrapper.Create('ConsolidationsFull').AsVariant);
  d.Open;

  ReadOnlyClientList := GetReadOnlyClientCompanyList;

  d.First;
  while not d.Eof do
  begin
    if (ReadOnlyClientList.IndexOf(d.fieldByName('CL_NBR').AsString+';'+d.fieldByName('CO_NBR').AsString) = -1) then
    begin
      if VarIsNull(d.FieldValues['CL_CO_CONSOLIDATION_NBR']) then
        AddRecord(sCompany, False)
      else
      begin
        AddRecord(sCompany, False);
        if d.FieldValues['CO_NBR'] = d.FieldValues['PRIMARY_CO_NBR'] then
          AddRecord(sConsolidation, True);
      end;
    end;
    d.Next;
  end;
  d.Close;
  cdClCo.First;
  wwDBGrid1.SelectRecord;
  cbQuarter.ItemIndex := GetQuarterNumber(Date)-1;
  UpDown1.Position := GetYear(Date);
  UpDown1Click(nil, btNext);
  Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;
  ts3.TabVisible := False;
  ts4.TabVisible := False;
end;

procedure TBalanceReportScreen.UpDown1Click(Sender: TObject;
  Button: TUDBtnType);
begin
  inherited;
  cbYear.Text := IntToStr(UpDown1.Position);
end;

procedure TBalanceReportScreen.Button1Click(Sender: TObject);
var
  a: array of TCompanyDescr;
begin
  cdClCo.CheckBrowseMode;
  if cbWholeYear.Checked then
  begin
    FBegDate := EncodeDate(StrToInt(cbYear.Text), 1, 1);
    FEndDate := GetEndYear(FBegDate);
  end
  else
  begin
    FBegDate := EncodeDate(StrToInt(cbYear.Text), cbQuarter.ItemIndex*3+ 1, 1);
    FEndDate := GetEndQuarter(FBegDate);
  end;
  inherited;
  ctx_StartWait;
  SetLength(a, 0);
  with TevClientDataSet.Create(nil) do
  try
    CloneCursor(cdClCo, False);
    IndexFieldNames := 'CL_NBR';
    First;
    while not Eof do
    begin
      if ConvertNull(FieldValues['LOAD'], False) then
      begin
        SetLength(a, Length(a)+ 1);
        with a[High(a)] do
        begin
          ClNbr := FieldValues['CL_NBR'];
          CoNbr := FieldValues['CO_NBR'];
          Consolidation := FieldValues['CONSOLIDATED'];
        end;
      end;
      Next;
    end;
    if Length(a) = 0 then
      raise EInvalidParameters.CreateHelp('Nothing is selected', IDH_InvalidParameters);
    FBalanceInformation := GetBalanceInformation(nil, a, FBegDate, FEndDate);
    FBalanceInformation.cdsCoSum.First;
    Provider.DataSet := FBalanceInformation.cdsCoSum;
    cdsCoSum.Data := Provider.Data;
    FBalanceInformation.cdsSummary.First;
    Provider.DataSet := FBalanceInformation.cdsSummary;
    cdsSummary.Data := Provider.Data;
  finally
    Free;
    ctx_EndWait;
  end;
  Pc.ActivePage := ts2;
  ts2.TabVisible := True;
  //ts1.TabVisible := False;
end;

procedure TBalanceReportScreen.Button3Click(Sender: TObject);
begin
  inherited;
  {Pc.ActivePage := ts1;
  ts1.TabVisible := True;
  ts2.TabVisible := False;}
end;

procedure TBalanceReportScreen.Button4Click(Sender: TObject);
begin
  inherited;
  UpdateAll(True);
end;

procedure TBalanceReportScreen.Button5Click(Sender: TObject);
begin
  inherited;
  UpdateAll(False);
end;

procedure TBalanceReportScreen.UpdateAll(const Value: Boolean);
begin
  with TevClientDataSet.Create(nil) do
  try
    CloneCursor(cdClCo, False);
    First;
    while not Eof do
    begin
      Edit;
      FieldValues['LOAD'] := Value;
      Post;
      Next;
    end;
  finally
    Free
  end;
end;

procedure TBalanceReportScreen.cdsCoSumAfterScroll(DataSet: TDataSet);
begin
  inherited;
  cdsSummary.Filter := 'CLNBR = '+ IntToStr(DataSet['CLNBR'])+ ' and CONBR = '+
    IntToStr(DataSet['CONBR'])+ ' and CONSOLIDATION = '+ BoolToStr[Boolean(DataSet['CONSOLIDATION'])];
  cdsSummary.Filtered := True;
end;

procedure TBalanceReportScreen.DeActivate;
begin
  FBalanceInformation.Free;
  inherited;
end;

procedure TBalanceReportScreen.cbWholeYearClick(Sender: TObject);
begin
  inherited;
  cbQuarter.Enabled := not cbWholeYear.Checked;
end;

procedure TBalanceReportScreen.PcChange(Sender: TObject);
begin
  inherited;
  if Pc.ActivePage = ts1 then
  begin
    ts2.TabVisible := False;
    ts3.TabVisible := False;
    ts4.TabVisible := False;
    FBalanceInformation.Free;
    FBalanceInformation := nil;
  end;
end;

procedure TBalanceReportScreen.evDBGrid2DblClick(Sender: TObject);
var
  s: ShortString;
  Excemption: array of ShortString;
begin
  inherited;
  SetLength(Excemption, 6);
  Excemption[0] := 'CL_NBR';
  Excemption[1] := 'CO_NBR';
  Excemption[2] := 'CLIENT_NAME';
  Excemption[3] := 'CUSTOM_CLIENT_NUMBER';
  Excemption[4] := 'CUSTOM_COMPANY_NUMBER';
  Excemption[5] := 'COMPANY_NAME';

  s := cdsSummary.FieldByName('WagesDsName').AsString;
  if s <> '' then
  begin
    gWageDetails.Selected.Clear;
    dsWageDetails.DataSet := FBalanceInformation.FindComponent(s) as TevClientDataSet;
    dsWageDetails.DataSet.Filter := 'CL_NBR='+ IntToStr(cdsSummary['ClNbr'])+ ' and CO_NBR='+ IntToStr(cdsSummary['CoNbr']);
    dsWageDetails.DataSet.Filtered := True;
    SetSelectedFromFields(gWageDetails, dsWageDetails.DataSet as TevClientDataSet, Excemption);
    ts3.TabVisible := True;
  end
  else
    ts3.TabVisible := False;
  s := cdsSummary.FieldByName('LiabDsName').AsString;
  if s <> '' then
  begin
    gLiabDetails.Selected.Clear;
    dsLiabDetails.DataSet := FBalanceInformation.FindComponent(s) as TevClientDataSet;
    dsLiabDetails.DataSet.Filter := 'CL_NBR='+ IntToStr(cdsSummary['ClNbr'])+ ' and CO_NBR='+ IntToStr(cdsSummary['CoNbr']);
    dsLiabDetails.DataSet.Filtered := True;
    SetSelectedFromFields(gLiabDetails, dsLiabDetails.DataSet as TevClientDataSet, Excemption);
    ts4.TabVisible := True;
  end
  else
    ts4.TabVisible := False;
  if ts3.TabVisible then
    Pc.ActivatePage(ts3)
  else
  if ts4.TabVisible then
    Pc.ActivatePage(ts4)
  else
    raise ECanNotPerformOperation.CreateHelp('There is no details for this row', IDH_CanNotPerformOperation);
end;

procedure TBalanceReportScreen.ActivateForQueue(BalanceDM: TBalanceDM);
begin
  FBalanceInformation := BalanceDM;
  FBalanceInformation.cdsCoSum.First;
  Provider.DataSet := FBalanceInformation.cdsCoSum;
  cdsCoSum.Data := Provider.Data;
  FBalanceInformation.cdsSummary.First;
  Provider.DataSet := FBalanceInformation.cdsSummary;
  cdsSummary.Data := Provider.Data;
  ts1.TabVisible := False;
  evDBGrid1.Visible := False;
  evSplitter1.Visible := False;
  ts3.TabVisible := False;
  ts4.TabVisible := False;
end;

initialization
  RegisterClass(TBalanceReportScreen);

end.
