unit SXMLReportWrapper;
interface
uses
  Classes,
  MSXML2_TLB;

type
  TXMLReportWrapper = class(TObject)
  private
    FReportList: TInterfaceList;
    function GetXML: string;
  protected
    property ReportList: TInterfaceList read FReportList;
    class function CreateXMLDomDocument2: IXMLDomDocument2;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddXMLReport(XMLReport: string);
    class function ReadXMLFromFile(FileName: string): string;
    property XML: string read GetXML;
  end;

implementation
uses
//  XMLDoc,
  SysUtils;

constructor TXMLReportWrapper.Create;
begin
  inherited Create;
  FReportList := TInterfaceList.Create;
end;

destructor TXMLReportWrapper.Destroy;
begin
  FreeAndNil(FReportList);
  inherited Destroy;
end;

class function TXMLReportWrapper.ReadXMLFromFile(FileName: string): string;
var
  F: TextFile;
  C: Char;
begin
  begin
    Result := '';
    AssignFile(F, FileName);
    try
      Reset(F);
      while not EOF(F) do
      begin
        Read(F, C);
        Result := Result + C;
      end;
    finally
      CloseFile(F);
    end;
  end;
end;

class function TXMLReportWrapper.CreateXMLDomDocument2: IXMLDomDocument2;
// Isolates the versioning from the implementation
begin
  Result := CoDOMDocument60.Create;
end;

procedure TXMLReportWrapper.AddXMLReport(XMLReport: string);
var
  XMLDomDocument2: IXMLDomDocument2;
begin
  XMLDomDocument2 := CreateXMLDomDocument2;
  XMLDomDocument2.loadXML(XMLReport);
  ReportList.Add(XMLDomDocument2);
end;

function TXMLReportWrapper.GetXML: string;
var
  XMLDOMDocument2: IXMLDOMDocument2;
  Root: IXMLDOMElement;
  XMLDOMAttribute: IXMLDOMAttribute;
  XMLDOMText: IXMLDOMText;
begin
  XMLDOMDocument2 := CreateXMLDomDocument2;
  Root := XMLDOMDocument2.createElement('RootElement');

  XMLDOMAttribute := XMLDOMDocument2.createAttribute('Attr01');
  XMLDOMAttribute.value := 'Hello';
  Root.setAttributeNode(XMLDomAttribute);

  XMLDOMText := XMLDOMDocument2.createTextNode('Bleep');
  Root.appendChild(XMLDOMText);

  XMLDomDocument2.appendChild(Root);
  Result := XMLDomDocument2.XML;
end;

end.

