inherited EDIT_SB_GLOBAL_AGENCY_CONTACTS: TEDIT_SB_GLOBAL_AGENCY_CONTACTS
  object Label6: TevLabel [0]
    Left = 8
    Top = 8
    Width = 36
    Height = 13
    Caption = 'Agency'
  end
  object dtxtAgency_Name: TevDBText [1]
    Left = 64
    Top = 8
    Width = 545
    Height = 17
    DataField = 'AGENCY_NAME'
    DataSource = wwdsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel1: TBevel [2]
    Left = 0
    Top = 0
    Width = 443
    Height = 33
    Align = alTop
    Style = bsRaised
  end
  object PageControl1: TevPageControl [3]
    Left = 0
    Top = 33
    Width = 443
    Height = 244
    HelpContext = 38503
    ActivePage = tsDetail
    Align = alClient
    TabOrder = 0
    object tsBrowse: TTabSheet
      Caption = 'Browse'
      object Splitter1: TevSplitter
        Left = 321
        Top = 0
        Width = 3
        Height = 392
        Cursor = crHSplit
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 321
        Height = 392
        Selected.Strings = (
          'AGENCY_NAME'#9'40'#9'Agency Name')
        IniAttributes.FileName = 'DELPHI32.ini'
        IniAttributes.SectionName = 'EDIT_SB_GLOBAL_AGENCY_CONTACTSwwDBGrid1'
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alLeft
        DataSource = wwdsMaster
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
      object wwDBGrid2: TevDBGrid
        Left = 324
        Top = 0
        Width = 262
        Height = 392
        Selected.Strings = (
          'CONTACT_NAME'#9'30'#9'Contact Name'
          'PHONE'#9'20'#9'Phone')
        IniAttributes.FileName = 'DELPHI32.ini'
        IniAttributes.SectionName = 'EDIT_SB_GLOBAL_AGENCY_CONTACTSwwDBGrid2'
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
    end
    object tsDetail: TTabSheet
      Caption = 'Details'
      object lablContact_Name: TevLabel
        Left = 16
        Top = 16
        Width = 105
        Height = 13
        Caption = '~Contact Name'
        FocusControl = dedtContact_Name
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablPhone: TevLabel
        Left = 16
        Top = 64
        Width = 31
        Height = 13
        Caption = 'Phone'
        FocusControl = wwdePhone
      end
      object lablFax: TevLabel
        Left = 144
        Top = 64
        Width = 17
        Height = 13
        Caption = 'Fax'
        FocusControl = wwdeFax
      end
      object lablE_Mail_Address: TevLabel
        Left = 16
        Top = 112
        Width = 28
        Height = 13
        Caption = 'E-mail'
        FocusControl = dedtE_Mail_Address
      end
      object lablNotes: TevLabel
        Left = 16
        Top = 160
        Width = 28
        Height = 13
        Caption = 'Notes'
        FocusControl = dmemNotes
      end
      object dedtContact_Name: TevDBEdit
        Left = 16
        Top = 32
        Width = 249
        Height = 21
        DataField = 'CONTACT_NAME'
        DataSource = wwdsDetail
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dedtE_Mail_Address: TevDBEdit
        Left = 16
        Top = 128
        Width = 249
        Height = 21
        DataField = 'E_MAIL_ADDRESS'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object dmemNotes: TEvDBMemo
        Left = 16
        Top = 176
        Width = 385
        Height = 113
        DataField = 'NOTES'
        DataSource = wwdsDetail
        TabOrder = 4
      end
      object wwdePhone: TevDBEdit
        Left = 16
        Top = 80
        Width = 121
        Height = 21
        DataField = 'PHONE'
        DataSource = wwdsDetail
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeFax: TevDBEdit
        Left = 144
        Top = 80
        Width = 121
        Height = 21
        DataField = 'FAX'
        DataSource = wwdsDetail
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
    Top = 32
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_GLOBAL_AGENCY_CONTACTS.SB_GLOBAL_AGENCY_CONTACTS
    Top = 32
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 320
    Top = 16
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 368
    Top = 16
  end
end
