object EDIT_MANUAL_RECONCILED: TEDIT_MANUAL_RECONCILED
  Left = 487
  Top = 305
  Width = 419
  Height = 153
  Caption = 'Manual Reconciled'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblNotes: TevLabel
    Left = 16
    Top = 15
    Width = 250
    Height = 13
    Caption = 'The notes entered will update all selected companies'
  end
  object bOk: TevButton
    Left = 16
    Top = 76
    Width = 129
    Height = 25
    Caption = 'Ok'
    TabOrder = 0
    OnClick = bOkClick
  end
  object edtMessage: TevEdit
    Left = 16
    Top = 39
    Width = 372
    Height = 21
    MaxLength = 60
    TabOrder = 1
  end
  object btnCancel: TevButton
    Left = 259
    Top = 76
    Width = 129
    Height = 25
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = btnCancelClick
  end
end
