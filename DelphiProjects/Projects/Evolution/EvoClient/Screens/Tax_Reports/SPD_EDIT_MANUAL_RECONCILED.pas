// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_MANUAL_RECONCILED;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, wwdblook, SDataStructure, ComCtrls, ExtCtrls,
  SDDClasses, SDataDictsystem, ISBasicClasses, EvUIComponents;

type
  TEDIT_MANUAL_RECONCILED = class(TForm)
    bOk: TevButton;
    lblNotes: TevLabel;
    edtMessage: TevEdit;
    btnCancel: TevButton;
    procedure bOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TEDIT_MANUAL_RECONCILED.bOkClick(Sender: TObject);
begin
//    EvMessage('This utility will automatically create QEC that will only clean up SUI taxes.  It will also automatically process these QEC payrolls, even if there are Employee taxes that were cleaned up.');
    ModalResult := mrOk;
end;

procedure TEDIT_MANUAL_RECONCILED.btnCancelClick(Sender: TObject);
begin
    ModalResult := mrCancel;
end;

end.
