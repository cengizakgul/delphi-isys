unit SReturnTransmissionFile94X;
interface
uses
  SXMLHelper, Classes, DateUtils, evStreamUtils,
  MSXML2_TLB,  SOriginList, evExceptions,  ISBasicUtils,  ISZippingRoutines, AbZipTyp;

resourcestring
  MimeHeader =
    'MIME-Version: 1.0'#13#10 +
    'Content-Type: multipart/related; boundary="MIME94xBoundary"; type="text/xml"'#13#10 +
    'Content-Description: Transmission File containing one Submission.'#13#10 +
    'X-eFileRoutingCode: MEF'#13#10;

  MimeSubHeader =
    #13#10'--MIME94xBoundary'#13#10 +
    'Content-Type: "text/xml"; charset=UTF-8'#13#10 +
    'Content-Transfer-Encoding: 8bit'#13#10 +
    'Content-Location: %s'#13#10#13#10;

  MimeFooter =
    '--MIME94xBoundary'#13#10 +
    'Content-Type: application/octet-steam'#13#10 +
    'Content-Transfer-Encoding: Binary'#13#10 +
    'Content-Location: %s'#13#10#13#10;

  MimeFooterlast =#13#10#13#10'--MIME94xBoundary'#13#10;

  TransmissionEnvelope =
    '<?xml version="1.0" encoding="UTF-8"?>'#13#10 +
    '<SOAP:Envelope xmlns="http://www.irs.gov/efile"'#13#10 +
    'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'#13#10 +
    'xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/"'#13#10 +
    'xmlns:efile="' + EfileNamespace + '"'#13#10 +
    'xsi:schemaLocation="http://schemas.xmlsoap.org/soap/envelope/ ../message/SOAP.xsd' +
    '  http://www.irs.gov/efile ../message/efileMessageIFA.xsd">'#13#10 +
    '    <SOAP:Header>'#13#10 +
    '        <efile:IFATransmissionHeader>'#13#10 +
    '            <TransmissionId></TransmissionId>'#13#10 +
    '            <TransmissionTs></TransmissionTs>'#13#10 +
    '            <TransmitterDetail>'#13#10 +
    '                <ETIN></ETIN>'#13#10 +
    '            </TransmitterDetail>'#13#10 +
    '        </efile:IFATransmissionHeader>'#13#10 +
    '    </SOAP:Header>'#13#10 +
    '    <SOAP:Body>'#13#10 +
    '       <efile:TransmissionManifest>'#13#10 +
    '        <efile:SubmissionDataList>'#13#10 +
    '          <Cnt></Cnt>'#13#10 +
    '        </efile:SubmissionDataList>'#13#10 +
    '       </efile:TransmissionManifest>'#13#10 +
    '    </SOAP:Body>'#13#10 +
    '</SOAP:Envelope>';


type
  TReturnTransmissionFile94X = class(TObject)
  // This is what you should create. Load with 941 XML documents using
  // AddXMLReport, then use AsMime to get as a Mime file that will be accepted
  // by the IRS.
  private
    FpathMEF: string;
    FpathXML: string;
    FpathManifest: string;
    FpathSubmission: String;
    FpathResult: string;
    FContentLocation: String;

    FxmlFileName: string;

    FTransmissionId: string;
    FEFIN: string;
    FETIN: string;
    FsequenceNumber: String;
    FMEFType: string;

    FSubmissionList: TSubmissionList;

    function GetCounter: integer;

    function GetAsMimeString: string;
    procedure SetTransmissionId(const Value: string);
    procedure SetETIN(const Value: string);
    procedure SetEFIN(const Value: string);
    function GetSoapEnvelope: IXMLDomDocument2;

    function CreateReportFileName(ReportName, Path: string): string;
    function GetSequenceNumber: string;

  protected
    property SubmissionList: TSubmissionList read FSubmissionList;
    property SoapEnvelope: IXMLDomDocument2 read GetSoapEnvelope;
  public
    constructor Create(const path, mType, aEFIN, aETIN:string);
    destructor Destroy; override;

    procedure AddManifest(const XMLReport, aEIN: string; const dFrom, dTo: TDateTime);
    procedure AddXMLReport(XMLReport: TStream; const aReportDescription, aEIN: String; const dFrom, dTo: TDateTime);

    procedure ClearPath;
    procedure PostResult;

    property TransmissionId: string read FTransmissionId  write SetTransmissionId;
    property ETIN: string read FETIN write SetETIN;
    property EFIN: string read FEFIN write SetEFIN;
    property MEFType: string read FMEFType write FMEFType;

    property pathMEF: string read FpathMEF write FpathMEF;
    property pathXML: string read FpathXML write FpathXML;
    property pathManifest: string read FpathManifest write FpathManifest;
    property pathSubmission: string read FpathSubmission write FpathSubmission;
    property pathResult: string read FpathResult write FpathResult;
    property ContentLocation: string read FContentLocation write FContentLocation;

    property xmlFileName: string read FxmlFileName write FxmlFileName;

    property Counter: integer read GetCounter;
    property sequenceNumber: string read FsequenceNumber write FsequenceNumber;

    property AsMimeString: string read GetAsMimeString;
  end;

implementation
uses
  Windows, SysUtils;

constructor TReturnTransmissionFile94X.Create(const path, mType, aEFIN, aETIN:string);
begin
  inherited Create;

  FSubmissionList := TSubmissionList.Create;
  MEFType := mType;

  pathMEF := 'c:\mef\';
  pathXML := 'c:\mef\xml\';
  pathManifest := 'c:\mef\manifest\';
  pathSubmission := 'c:\SubmissionZips\';
  ContentLocation := StringReplace('Envelope'+ MEFType, ' ', '_', [rfReplaceAll]);

  ForceDirectories(pathXML);
  ForceDirectories(pathManifest);
  pathSubmission:= NormalizePath(ExpandFileName(pathSubmission));
  ForceDirectories(pathSubmission);
  ClearDir(pathSubmission);
  pathResult:= NormalizePath(ExpandFileName(path));
  EFIN := aEFIN;
  ETIN := aETIN;

  sequenceNumber:= '';

  TransmissionId:= TXMLHelper.GenerateTransmissionId;
end;

destructor TReturnTransmissionFile94X.Destroy;
begin
  FreeAndNil(FSubmissionList);
  inherited Destroy;
end;

function TReturnTransmissionFile94X.GetSequenceNumber:string;
begin
 repeat
   result := '0'+FormatDateTime('hhnnss', Now);
 until sequenceNumber <> result;
 sequenceNumber:= result;
end;

procedure TReturnTransmissionFile94X.ClearPath;
begin
    ClearDir(pathXML);
    ClearDir(pathManifest);
end;

function TReturnTransmissionFile94X.CreateReportFileName(ReportName, Path: string): string;
begin
  Result := Path + StringReplace(StringReplace(StringReplace(StringReplace(ReportName, '/', '-', [rfReplaceAll]), ':', '-', [rfReplaceAll]), '\', '-', [rfReplaceAll]), ' ', '_', [rfReplaceAll]);
end;

procedure TReturnTransmissionFile94X.AddXMLReport(XMLReport: TStream; const aReportDescription, aEIN: String; const dFrom, dTo: TDateTime);
const
  EmptyXMLMsg = 'Cannot generate empty XML report';
var
  StringStream: TStringStream;
  F: TEvFileStream;
begin
    if (not Assigned(XMLReport)) then
      raise EevException.Create(EmptyXMLMsg);
    if Assigned(XMLReport) and (XMLReport.Size <= 0) then
      raise EevException.Create(EmptyXMLMsg);

    ClearPath;

    xmlFileName := ChangeFileExt(CreateReportFileName(aReportDescription + '_' + TXMLHelper.GenerateTimestamp, pathXML), '.xml');

    F := TEvFileStream.Create(xmlFileName, fmCreate);
    try
      F.CopyFrom(XMLReport, XMLReport.Size);
    finally
      F.Free;
    end;

    StringStream := TStringStream.Create('');
    try
      StringStream.CopyFrom(XMLReport, 0);
      AddManifest(StringStream.DataString, aEIN, dFrom, dTo);
    finally
      FreeAndNil(StringStream);
    end;
end;

procedure TReturnTransmissionFile94X.AddManifest(const XMLReport: string; const aEIN: string; const dFrom, dTo: TDateTime);
var
  manifestXML: TSubmissionManifest;
  sXml: String;
  manifestFileName, submissionZipFile: String;
  submissionID: String;

begin
  submissionID:=  EFIN + FormatDateTime('yyyy', Now) + Format('%.3d', [DayOfTheYear(Now)]) +  GetSequenceNumber;
  manifestXML:= SubmissionList.Find(submissionID);
  if not Assigned(manifestXML) then
  begin
    manifestXML:= TSubmissionManifest.Create(submissionID);
    try
      manifestXML.SubmissionId := submissionID;
      manifestXML.EFIN := EFIN;
      manifestXML.GovernmentCode := 'IRS';
      manifestXML.SubmissionType := Copy(MEFType, 1, 3);
      manifestXML.TaxYear:= FormatDateTime('yyyy', dTo);
      manifestXML.TaxPeriodBeginDate:= FormatDateTime('yyyy-mm-dd', dFrom);
      manifestXML.TaxPeriodEndDate:= FormatDateTime('yyyy-mm-dd', dTo);
      manifestXML.EIN:= aEIN;

      sXml:= TXMLHelper.FixXML(manifestXML.XML);

      manifestFileName:= ChangeFileExt(CreateReportFileName('manifest', pathManifest), '.xml');
      TXMLHelper.SaveToFile(sXml, manifestFileName);

      submissionZipFile:= pathSubmission + submissionID + '.zip';
      AddToFile(submissionZipFile, pathMEF, '*.*', '',true);

      SubmissionList.Add(manifestXML);
    except
      FreeAndNil(manifestXML);
      raise;
    end;
  end;
end;

function TReturnTransmissionFile94X.GetCounter: integer;
begin
  result:= 0;
  if Assigned(FSubmissionList) then
     result:= SubmissionList.Count;
end;

procedure TReturnTransmissionFile94X.PostResult;
var
 s, resultFileName, resultZipFile: String;
 MS1: IisStream;
 F: TextFile;
begin
   resultZipFile := ChangeFileExt(CreateReportFileName(MEFType + '_' + TXMLHelper.GenerateTimestamp, pathResult), '.zip');
   AddToFile(resultZipFile, pathSubmission, '*.*', '',false, smStored);

   MS1 := TisStream.Create;
   MS1.LoadFromFile(resultZipFile);
   MS1.Position := 0;

   s:= AsMimeString;

   resultFileName:= ChangeFileExt(CreateReportFileName(MEFType + '_' + TXMLHelper.GenerateTimestamp, pathResult), '.tf');
   try
      AssignFile(F, resultFileName);
      try
        Rewrite(F);
        Write(F, Format(s, [ExtractFileName(resultZipFile)]));
        Write(F, MS1.AsString);
        Write(F, MimeFooterlast);
      finally
        CloseFile(F);
      end;
   finally
     FSubmissionList.Free;    //  for new submission
     FSubmissionList := TSubmissionList.Create;
     TransmissionId:= TXMLHelper.GenerateTransmissionId;
     ClearPath;
     ClearDir(pathSubmission);
   end;
end;

function TReturnTransmissionFile94X.GetSoapEnvelope: IXMLDomDocument2;
var
  I: integer;
  Timestamp: string;
  XMLDomDocument2: IXMLDomDocument2;
  XMLDOMNode, TransmissionManifest: IXMLDOMNode;
  OriginHeaderReference: IXMLDOMElement;
begin
  Timestamp := TXMLHelper.GenerateTimestamp;
  XMLDomDocument2 := TXMLHelper.CreateXMLDomDocument2;
  XMLDomDocument2.loadXML(TransmissionEnvelope);
  XMLDomDocument2.setProperty('SelectionNamespaces',
    'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
    'xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/" ' +
    'xmlns:efile="http://www.irs.gov/efile"');

  XMLDOMNode := XMLDomDocument2.SelectSingleNode(
    '//SOAP:Envelope/SOAP:Header/efile:IFATransmissionHeader/efile:TransmissionId');
  XMLDOMNode.text := TransmissionId;
  XMLDOMNode := XMLDomDocument2.SelectSingleNode(
    '//SOAP:Envelope/SOAP:Header/efile:IFATransmissionHeader/efile:TransmissionTs');
  XMLDOMNode.Text := Timestamp;
  XMLDOMNode := XMLDomDocument2.SelectSingleNode(
    '//SOAP:Envelope/SOAP:Header/efile:IFATransmissionHeader/efile:TransmitterDetail/efile:ETIN');
  XMLDOMNode.Text := ETIN;

  XMLDOMNode := XMLDomDocument2.SelectSingleNode('//SOAP:Envelope/SOAP:Body/efile:TransmissionManifest/efile:SubmissionDataList/efile:Cnt');
  XMLDOMNode.Text := IntToStr(SubmissionList.Count);

  TransmissionManifest := XMLDomDocument2.SelectSingleNode('//SOAP:Envelope/SOAP:Body/efile:TransmissionManifest/efile:SubmissionDataList');

  for I := 0 to SubmissionList.Count - 1 do
  begin
    OriginHeaderReference := TXMLHelper.AppendChild(XMLDomDocument2, TransmissionManifest, 'SubmissionData', 4);
    TXMLHelper.AppendChild(XMLDomDocument2, OriginHeaderReference, 'SubmissionId', 5, SubmissionList[I].SubmissionId);
    TXMLHelper.AppendChild(XMLDomDocument2, OriginHeaderReference, 'ElectronicPostmarkTs', 5, Timestamp, True);
  end;

  TransmissionManifest.appendChild(XMLDOMDocument2.createTextNode(#13#10#9#9#9));

  Result := XMLDomDocument2;
end;

function TReturnTransmissionFile94X.GetAsMimeString: string;
begin
  Result := MimeHeader + Format(MimeSubHeader, [ContentLocation]) +
  TXMLHelper.FixXML(SoapEnvelope.xml);
  Result := Result + #13#10 + MimeFooter;
end;

procedure TReturnTransmissionFile94X.SetTransmissionId(const Value: string);
const
  InvalidMsg = 'Invalid TransmissionId';
  LegalChars = ['A'..'Z', 'a'..'z', '0'..'9', ';', '.', '-'];
var
  I: integer;
begin
  for I := 1 to Length(Value) do
    if not (Value[I] in LegalChars) then
      raise EevException.Create(InvalidMsg);
  FTransmissionId := Value;
end;

procedure TReturnTransmissionFile94X.SetETIN(const Value: string);
const
  InvalidMsg = 'Invalid ETIN';
  LegalChars = ['0'..'9'];
  MinLength = 5;
  MaxLength = 5;
var
  I: integer;
begin
  if Length(Value) < MinLength then
    raise EevException.Create(InvalidMsg);
  if Length(Value) > MaxLength then
    raise EevException.Create(InvalidMsg);
  for I := 1 to Length(Value) do
    if not (Value[I] in LegalChars) then
      raise EevException.Create(InvalidMsg);
  FETIN := Value;
end;

procedure TReturnTransmissionFile94X.SetEFIN(const Value: string);
const
  InvalidMsg = 'Invalid EFINNumber';
  LegalChars = ['0'..'9'];
  MinLength = 6;
  MaxLength = 6;
var
  I: integer;
begin
  if Length(Value) < MinLength then
    raise EevException.Create(InvalidMsg);
  if Length(Value) > MaxLength then
    raise EevException.Create(InvalidMsg);
  for I := 1 to Length(Value) do
    if not (Value[I] in LegalChars) then
      raise EevException.Create(InvalidMsg);

  FEFIN := Value;
end;

end.

