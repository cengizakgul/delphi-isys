unit SReturnTransmissionFile1094;
interface
uses
  SXMLHelper, Classes, DateUtils, evStreamUtils,
  MSXML2_TLB,  S1094BCManifest, evExceptions,  ISBasicUtils, EvBasicUtils, SDataStructure,
  EvCommonInterfaces, EvDataset, StrUtils;

type
  TReturnTransmissionFile1094 = class(TObject)
  private
    xmlResultFile : IisStream;
    FpathACA: string;

    FxmlFileName: string;

    FEFIN: string;
    FTCC: string;
    FTestFileCd: string;
    FSoftwareID: String;
    FFormTypeCd: String;
    FOriginalReceiptId: String;

    FACAType: string;
    FCntStartString: string;
    FCntEndString: string;
    FTransmissionTypeCd: String;


    FSize: Int64;
    FCount1094: Integer;
    FCount1095: Integer;
    FCount: Integer;
    FUniqueHeader:Boolean;


    function GetSize: Int64;
    function GetCounter1094: integer;
    function GetCounter1095: integer;
    function GetCount: integer;


    function CreateReportFileName(ReportName, Path: string): string;

  protected

  public
    constructor Create(const path, mType, aEFIN, aTCC:string;
                          const bTestFile, bReplacement, bTransmissionReplacement:Boolean; const edOriginalReceiptId: String);
    destructor Destroy; override;

    Procedure IncCount;
    procedure SetDefaultValues;
    procedure AddXMLReport(XMLReport: TStream; const dFrom, dTo: TDateTime);
    procedure CreateManifestFile(const dFrom, dTo: TDateTime);
    function EncodeEntities(const Data: string; const bHypehens: boolean=false;
                                                 const bAmpersand: boolean=True;
                                                 const bslash:Boolean= false): string;

    procedure PostResult(const dFrom, dTo: TDateTime);

    property EFIN: string read FEFIN write FEFIN;
    property TransmissionTypeCd: string read FTransmissionTypeCd write FTransmissionTypeCd;

    property TCC: string read FTCC write FTCC;
    property TestFileCd: string read FTestFileCd write FTestFileCd;
    property OriginalReceiptId: string read FOriginalReceiptId write FOriginalReceiptId;
    property SoftwareID: string read FSoftwareID write FSoftwareID;
    property FormTypeCd: string read FFormTypeCd write FFormTypeCd;

    property ACAType: string read FACAType write FACAType;
    property CntStartString: string read FCntStartString write FCntStartString;
    property CntEndString: string read FCntEndString write FCntEndString;

    property pathACA: string read FpathACA write FpathACA;

    property xmlFileName: string read FxmlFileName write FxmlFileName;

    property Size: Int64 read GetSize;
    property Counter1094: integer read GetCounter1094;
    property Counter1095: integer read GetCounter1095;
    property Count: integer read GetCount;

    property UniqueHeader: Boolean read FUniqueHeader write FUniqueHeader;

  end;

implementation

uses
  Windows, SysUtils, SEncryptionRoutines, kbmMemTable, DB;


constructor TReturnTransmissionFile1094.Create(const path, mType, aEFIN, aTCC:string;
                                               const bTestFile, bReplacement, bTransmissionReplacement:Boolean; const edOriginalReceiptId: String);
begin
  inherited Create;

  ACAType := mType;

//Software ID for 1094/1095B is 15A0000402 (Should be hardcoded in file)
//Software ID for 1094/1095C is 15A0000401 (Should be hardcoded in file)

  SoftwareID:= '15A0000402';
  FormTypeCd:= '1094/1095B';
  ACAType := '1094B_';
  CntStartString:='<Form1095BAttachedCnt>';
  CntEndString:='</Form1095BAttachedCnt>';

  TransmissionTypeCd:= 'O';
  if Contains(mType, 'Corrections') then
     TransmissionTypeCd:= 'C'
  else
  if bReplacement or bTransmissionReplacement then
     TransmissionTypeCd:= 'R';

  if StartsWith(mType, 'Tax Return-1095C') then
  begin
     SoftwareID:= '15A0000401';
     FormTypeCd:= '1094/1095C';
     ACAType := '1094C_';
     CntStartString:='<Form1095CAttachedCnt>';
     CntEndString:='</Form1095CAttachedCnt>';
  end;

  pathACA := NormalizePath(NormalizePath(ExpandFileName(path)) + 'ACA1094\');
  ForceDirectories(pathACA);

  EFIN := aEFIN;
  TCC := aTCC;

  TestFileCd:= 'P';
  if bTestFile then
     TestFileCd:= 'T';

  OriginalReceiptId:= '';
  if bTransmissionReplacement then
     OriginalReceiptId:= edOriginalReceiptId;

  xmlResultFile := TisStream.Create;
  SetDefaultValues;
end;

destructor TReturnTransmissionFile1094.Destroy;
begin
  inherited Destroy;
end;

procedure TReturnTransmissionFile1094.SetDefaultValues;
begin
  FSize := 0;
  FCount1094:= 0;
  FCount1095:= 0;
  FCount:= 0;
  FUniqueHeader:= false;

  xmlFileName := ChangeFileExt(CreateReportFileName(ACAType + 'Request' + '_' + TCC + '_'+  TXMLHelper.GenerateTimestamp1094, pathACA), '.xml');
  xmlResultFile.AsString := '';
end;


procedure TReturnTransmissionFile1094.AddXMLReport(XMLReport: TStream; const dFrom, dTo: TDateTime);
const
  EmptyXMLMsg = 'Cannot generate empty XML report';

var
  StringStream: TStringStream;
  pStart, pEnd: Integer;
  pStr: String;
begin
    if (not Assigned(XMLReport)) then
      raise EevException.Create(EmptyXMLMsg);
    if Assigned(XMLReport) and (XMLReport.Size <= 0) then
      raise EevException.Create(EmptyXMLMsg);

    XMLReport.Position:= 0;
    StringStream := TStringStream.Create('');
    try
      StringStream.CopyFrom(XMLReport, 0);

     xmlResultFile.AsString := xmlResultFile.AsString  +  StringStream.DataString;
      try
        pStart:= AnsiPos(CntStartString, StringStream.DataString);
        if pStart > 0 then
        begin
          pEnd:= AnsiPos(CntEndString, StringStream.DataString);
          pStr:= copy(StringStream.DataString, pStart + 22, pEnd - pStart - 22);
          if Trim(pStr) <> '' then
            FCount1095 := FCount1095 + StrToInt(pStr);

          FSize:= FSize +   XMLReport.Size;
          Inc(FCount1094);
        end;
      finally
      end;
    finally
      FreeAndNil(StringStream);
    end;
end;

function TReturnTransmissionFile1094.CreateReportFileName(ReportName, Path: string): string;
begin
  Result := Path + StringReplace(StringReplace(StringReplace(StringReplace(ReportName, '/', '-', [rfReplaceAll]), ':', '-', [rfReplaceAll]), '\', '-', [rfReplaceAll]), ' ', '_', [rfReplaceAll]);
end;

procedure TReturnTransmissionFile1094.PostResult(const dFrom, dTo: TDateTime);
begin
   CreateManifestFile(dFrom, dTo);
   SetDefaultValues;
end;

function TReturnTransmissionFile1094.EncodeEntities(const Data: string; const bHypehens: boolean=false;
                                                                         const bAmpersand: boolean=True;
                                                                         const bslash:Boolean= false): string;
begin
  Result :=Trim(Data);
  Result :=StringReplace(Result, '"', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '.', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '!', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '@', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '#', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '$', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '%', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '^', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '*', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '+', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '=', '', [rfReplaceAll]);
  Result:= StringReplace(Result, ':', '', [rfReplaceAll]);
  Result:= StringReplace(Result, ';', '', [rfReplaceAll]);
  Result:= StringReplace(Result, ',', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '~', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '''', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '<', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '>', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '_', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '\', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '[', '', [rfReplaceAll]);
  Result:= StringReplace(Result, ']', '', [rfReplaceAll]);
  Result:= StringReplace(Result, '?', '', [rfReplaceAll]);
  if bHypehens then
     Result:= StringReplace(Result, '-', '', [rfReplaceAll]);
  if bAmpersand then
  begin
   Result:= StringReplace(Result, '&', '', [rfReplaceAll]);
   Result:= StringReplace(Result, '(', '', [rfReplaceAll]);
   Result:= StringReplace(Result, ')', '', [rfReplaceAll]);
  end;
  if bslash then
     Result:= StringReplace(Result, '/', '', [rfReplaceAll]);
end;

procedure TReturnTransmissionFile1094.CreateManifestFile(const dFrom, dTo: TDateTime);
var
  manifestXML: T1094SubmissionManifest;
  sXml, sGuid: String;
  manifestFileName: String;

  UniqueTransmissionId: String;
  g: TGuid;
  F: IEvDualStream;
  Hash: String;
  Q: IevQuery;

  function GetUserSignuture: boolean;
  Begin
    Result := false;
    Q := TevQuery.Create('select FIRST_NAME, MIDDLE_INITIAL, LAST_NAME from SB_USER where {AsOfNow<SB_USER>} and UpperCase(USER_ID) = ''SIGNATURE''');
    Q.Execute;
    if Q.Result.RecordCount = 1 then
       Result := true;
  end;

  function getZipExt(const zip: string): String;
  var
   l: Integer;
  begin
    l:=AnsiPos('-', zip);
    result:= '';
    if l > 0 then
     result:= copy(zip, l + 1, Length(zip) - l);
  end;
begin

  if  ACAType = '1094B_' then
     xmlResultFile.AsString := xmlResultFile.AsString + '</n1:Form109495BTransmittalUpstream>'
  else
     xmlResultFile.AsString := xmlResultFile.AsString + '</n1:Form109495CTransmittalUpstream>';
  xmlResultFile.SaveToFile(xmlFileName);

  CreateGuid(g);
  sGuid:= GuidToString(g);
  sGuid:= StringReplace(sGuid, '{', '', [rfReplaceAll]);
  sGuid:= StringReplace(sGuid, '}', '', [rfReplaceAll]);
  UniqueTransmissionId:= sGuid + ':SYS12:'+ TCC + '::T';

  F := TevDualStreamHolder.Create;
  F.LoadFromFile(xmlFileName);
  Hash:= CalcMD5(F.RealStream);

    manifestXML:= T1094SubmissionManifest.Create;
    try
        manifestXML.UniqueTransmissionId := UniqueTransmissionId;
        manifestXML.Timestamp := TXMLHelper.GenerateTimestamp;


        manifestXML.PaymentYr := FormatDateTime('yyyy', dTo);
        manifestXML.PriorYearDataInd := '0';
        manifestXML.EFIN :=  EFIN;
        manifestXML.TransmissionTypeCd:= TransmissionTypeCd;

        manifestXML.TestFileCd:= TestFileCd;
        manifestXML.OriginalReceiptId:= OriginalReceiptId;

        manifestXML.BusinessNameLine1Txt:= EncodeEntities(DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString, false, false);
        manifestXML.CompanyNm:= EncodeEntities(DM_SERVICE_BUREAU.SB.FieldByName('SB_NAME').AsString, false, false);
        manifestXML.AddressLine1Txt:= EncodeEntities(DM_SERVICE_BUREAU.SB.FieldByName('ADDRESS1').AsString);
        manifestXML.AddressLine2Txt:= EncodeEntities(DM_SERVICE_BUREAU.SB.FieldByName('ADDRESS2').AsString);
        manifestXML.CityNm:= EncodeEntities(DM_SERVICE_BUREAU.SB.FieldByName('CITY').AsString, true);

        manifestXML.USStateCd:= DM_SERVICE_BUREAU.SB.FieldByName('STATE').AsString;
        manifestXML.USZIPCd:= LeftStr(DM_SERVICE_BUREAU.SB.fieldByName('ZIP_CODE').asString ,5);
        if getZipExt(DM_SERVICE_BUREAU.SB.fieldByName('ZIP_CODE').asString) <> '' then
           manifestXML.USZIPExtensionCd:= getZipExt(DM_SERVICE_BUREAU.SB.fieldByName('ZIP_CODE').asString);
        if GetUserSignuture then
        begin
         if not Q.Result.FieldByName('FIRST_NAME').IsNull then
            manifestXML.CPersonFirstNm:= EncodeEntities(Q.Result.FieldByName('FIRST_NAME').asString, false, True, True);
         if not Q.Result.FieldByName('MIDDLE_INITIAL').IsNull then
            manifestXML.CPersonMiddleNm:= EncodeEntities(Q.Result.FieldByName('MIDDLE_INITIAL').asString, false, True, True);
         if not Q.Result.FieldByName('LAST_NAME').IsNull then
            manifestXML.CPersonLastNm:= EncodeEntities(Q.Result.FieldByName('LAST_NAME').asString, false, True, True);
        end;
        manifestXML.CContactPhoneNum:= DM_SERVICE_BUREAU.SB.FieldValues['PHONE'];
        manifestXML.CContactPhoneNum:= StringReplace(manifestXML.CContactPhoneNum, '(', '', [rfReplaceAll]);
        manifestXML.CContactPhoneNum:= StringReplace(manifestXML.CContactPhoneNum, ')', '', [rfReplaceAll]);
        manifestXML.CContactPhoneNum:= StringReplace(manifestXML.CContactPhoneNum, '-', '', [rfReplaceAll]);
        manifestXML.CContactPhoneNum:= StringReplace(manifestXML.CContactPhoneNum, ' ', '', [rfReplaceAll]);
        manifestXML.CContactPhoneNum:=Copy(manifestXML.CContactPhoneNum, 1,10);


        manifestXML.VendorCd:= 'V';
        if not Q.Result.FieldByName('FIRST_NAME').IsNull then
           manifestXML.VPersonFirstNm:= 'Asia';
        if not Q.Result.FieldByName('LAST_NAME').IsNull then
           manifestXML.VPersonLastNm:= 'Roque';

        manifestXML.VContactPhoneNum:= '8026558347';

        manifestXML.TotalPayeeRecordCnt:= IntToStr(Counter1095);
        manifestXML.TotalPayerRecordCnt:= IntToStr(Counter1094);

        manifestXML.SoftwareId:= SoftwareID;
        manifestXML.FormTypeCd:= FormTypeCd;

        manifestXML.BinaryFormatCd:= 'application/xml';

        manifestXML.ChecksumAugmentationNum:= Hash;

        manifestXML.AttachmentByteSizeNum:=  IntToStr(xmlResultFile.Size);
        manifestXML.DocumentSystemFileNm:= ChangeFileExt(ExtractFileName(xmlFileName), '.xml');

        sXml:= TXMLHelper.FixXML(manifestXML.XML);

        manifestFileName:= ChangeFileExt(CreateReportFileName('Manifest_' + ExtractFileName(xmlFileName), pathACA), '.xml');
        TXMLHelper.SaveToFile(sXml, manifestFileName);
    finally
      FreeAndNil(manifestXML);
    end;
End;

function TReturnTransmissionFile1094.GetCounter1094: integer;
begin
  result:= FCount1094;
end;

function TReturnTransmissionFile1094.GetCounter1095: integer;
begin
  result:= FCount1095;
end;

function TReturnTransmissionFile1094.GetSize: Int64;
begin
  result:= FSize;
end;

function TReturnTransmissionFile1094.GetCount: integer;
begin
  result:= FCount;
end;

Procedure TReturnTransmissionFile1094.IncCount;
begin
    inc(FCount)
end;

end.

