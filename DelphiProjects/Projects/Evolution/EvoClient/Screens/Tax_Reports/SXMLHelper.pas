unit SXMLHelper;
interface
uses
  MSXML2_TLB;

const
  EfileNamespace = 'http://www.irs.gov/efile';
  IncorrectXMLHeader = '<?xml version="1.0"?>';
  CorrectXMLHeader = '<?xml version="1.0" encoding="UTF-8"?>';

  InvalidOriginMsg = 'Invalid OriginId';
  LegalOriginChars = ['A'..'Z', 'a'..'z', '0'..'9', ';', '.', '-'];
  MinOriginLength = 1;
  MaxOriginLength = 30;

  InvalidContentLocationMsg = 'Invalid ContentLocation';
  LegalContentLocationChars = ['A'..'Z', 'a'..'z', '0'..'9', ';', '.', '-'];
  MinContentLocationLength = 1;
  MaxContentLocationLength = 30;


  s1094B1 = '<n1:Form109495BTransmittalUpstream xmlns="urn:us:gov:treasury:irs:ext:aca:air:7.0" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xxxccc';
  s1094B2 = 'xmlns:n1="urn:us:gov:treasury:irs:msg:form1094-1095Btransmitterupstreammessage" xsi:schemaLocation="urn:us:gov:treasury:irs:msg:form1094-1095Btransmitterupstreammessage IRS-Form1094-1095BTransmitterUpstreamMessage.xsd">';

  s1094C1 = '<n1:Form109495CTransmittalUpstream xmlns="urn:us:gov:treasury:irs:ext:aca:air:7.0" xmlns:irs="urn:us:gov:treasury:irs:common" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xxxvvv';
  s1094C2 = 'xmlns:n1="urn:us:gov:treasury:irs:msg:form1094-1095Ctransmitterupstreammessage" xsi:schemaLocation="urn:us:gov:treasury:irs:msg:form1094-1095Ctransmitterupstreammessage IRS-Form1094-1095CTransmitterUpstreamMessage.xsd">';

type
  TXMLHelper = class(TObject)
  private
    class function PaddWithZeros(S: string; FinalSize: integer): string;
  public
    class function GenerateGMTDelta: string;
    class function GenerateTimestamp: string;
    class function GenerateTimestamp1094: string;

    class function GenerateTransmissionId: string;
    class function GenerateContentLocation: string;
    class function CreateXMLDOMDocument2: IXMLDOMDocument2;
    class function AppendChild(const XMLDOMDocument2: IXMLDOMDocument2;
      const Ancestor: IXMLDOMNode; const ChildName: string;
      const Level: integer; const Text: string = ''; const bLast:Boolean= false): IXMLDOMElement;

    class procedure AppendAttribute(const XMLDomDocument2: IXMLDomDocument2;
      const Ancestor: IXMLDOMElement; const AttributeName,
      AttributeValue: string);
    class function FixXML(const XML: string): string;
    class function FixXMLReport(const XML: string; const mefType: Boolean=false): string;
    class function FixXMLReportACA(const XML: string; const bHeader, bReplacement: Boolean): string;
    class function FixXMLReportACASubmissionID(const XML: string; const Count:Integer): string;
    class function LoadFromFile(FileName: string): string;
    class procedure SaveToFile(S: string; FileName: string);
  end;

implementation
uses
  SyncObjs,  Windows,  SysUtils, ISBasicUtils;

var
  ContentLocationID: integer = 0;
  GenerateContentLocationCriticalSection: TCriticalSection;

class procedure TXMLHelper.AppendAttribute(
  const XMLDomDocument2: IXMLDomDocument2; const Ancestor: IXMLDOMElement;
  const AttributeName, AttributeValue: string);
var
  XMLDOMAttribute: IXMLDOMAttribute;
begin
  XMLDOMAttribute := XMLDOMDocument2.createAttribute(AttributeName);
  XMLDOMAttribute.value := AttributeValue;
  Ancestor.setAttributeNode(XMLDOMAttribute);
end;

class function TXMLHelper.AppendChild(
  const XMLDOMDocument2: IXMLDOMDocument2; const Ancestor: IXMLDOMNode;
  const ChildName: string; const Level: integer; const Text: string = ''; const bLast:Boolean= false): IXMLDOMElement;
var
  I: integer;
  S, S1: string;
  Child: IXMLDOMNode;
begin

  S1 := #13#10;
  for I := 0 to Level - 2 do
    S1 := S1 + #9;
  S := S1 + #9;
  Child := XMLDomDocument2.createNode(NODE_ELEMENT, Childname, '');
//        EfileNamespace);
  Ancestor.appendChild(XMLDOMDocument2.createTextNode(S));
  Ancestor.appendChild(Child);
  if Text <> '' then
  begin
    Child.appendChild(XMLDOMDocument2.createTextNode(Text));
    if bLast then
       Ancestor.appendChild(XMLDOMDocument2.createTextNode(S1));
  end;
  Result := Child as IXMLDOMElement;
end;

class function TXMLHelper.CreateXMLDOMDocument2: IXMLDOMDocument2;
// Isolates the versioning of CoDOMDocument from the implementation
begin
  try
    Result := CoDOMDocument60.Create;
  except
    on E: Exception do
    begin
      E.Message := 'MSXML library not installed'#13#10 +
        'Please install MSXML library 6.0 or greater'#13#10#13#10 +
        E.Message;
      raise;
    end
    else
      raise;
  end;
end;

class function TXMLHelper.FixXML(const XML: string): string;
// The below code is to re-add the encoding parameter that IRS requires and
// MSXML library removes (as it is supposed to be the default)
begin
  Result := StringReplace(XML, IncorrectXMLHeader, CorrectXMLHeader, [rfReplaceAll]);
  Result := StringReplace(Result, ' xmlns="">', '>', [rfReplaceAll]);
  Result := StringReplace(Result, 'efile:', '', [rfReplaceAll]);
end;

class function TXMLHelper.FixXMLReport(const XML: string; const mefType: Boolean=false): string;
var
 sDocument: string;
begin

  Result := StringReplace(XML, 'xmlns="http://www.irs.gov/efile"', 'xmlns:efile="http://www.irs.gov/efile" xmlns="http://www.irs.gov/efile"', [rfReplaceAll]);

  sDocument:= '<IRS941ScheduleB';
  if mefType then
  begin
    sDocument:= '<IRS940ScheduleA';
    Result := StringReplace(Result, '<CreditReductionStateInd referenceDocumentId="">X</CreditReductionStateInd>', '<CreditReductionStateInd referenceDocumentId="Doc2">X</CreditReductionStateInd>', [rfReplaceAll]);
    Result := StringReplace(Result, '<MultiStateInd referenceDocumentId="">X</MultiStateInd>', '<MultiStateInd referenceDocumentId="Doc2">X</MultiStateInd> ', [rfReplaceAll]);
    Result := StringReplace(Result, '<ReturnData documentCnt="1">','<ReturnData documentCnt="2">', [rfReplaceAll]);
  end;

  if AnsiPos(sDocument, Result) = 0 then
     Result := StringReplace(Result, '<ReturnData documentCnt="2">', '<ReturnData documentCnt="1">', [rfReplaceAll]);
end;

class function TXMLHelper.FixXMLReportACA(const XML: string; const bHeader, bReplacement: Boolean): string;
var
   pStart, pEnd: Integer;
begin

  Result := StringReplace(XML, 'xmlns="http://www.irs.gov/efile"', 'xmlns:efile="http://www.irs.gov/efile" xmlns="http://www.irs.gov/efile"', [rfReplaceAll]);

  if not bHeader then
  begin
    Result := StringReplace(Result, '<Form109495BTransmittalUpstream xmlns="urn:us:gov:treasury:irs:msg:form1094-1095Btransmitterupstreammessage" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
                    , s1094B1
                    , [rfReplaceAll]);
    Result := StringReplace(Result, 'xxxccc'
                    , s1094B2
                    , [rfReplaceAll]);

    Result := StringReplace(Result, '<Form109495CTransmittalUpstream xmlns="urn:us:gov:treasury:irs:msg:form1094-1095Ctransmitterupstreammessage" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
                    , s1094C1
                    , [rfReplaceAll]);

    Result := StringReplace(Result, 'xxxvvv'
                    , s1094C2
                    , [rfReplaceAll]);
  end
  else
  begin
    Result := StringReplace(Result, IncorrectXMLHeader, '', [rfReplaceAll]);
    Result := StringReplace(Result, CorrectXMLHeader, '', [rfReplaceAll]);

    Result := StringReplace(Result, '<Form109495BTransmittalUpstream xmlns="urn:us:gov:treasury:irs:msg:form1094-1095Btransmitterupstreammessage" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
                    , '', [rfReplaceAll]);
    Result := StringReplace(Result, '<Form109495CTransmittalUpstream xmlns="urn:us:gov:treasury:irs:msg:form1094-1095Ctransmitterupstreammessage" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
                    , '', [rfReplaceAll]);
  end;

  Result := StringReplace(Result, 'TaxYr>', 'irs:TaxYr>', [rfReplaceAll]);
  Result := StringReplace(Result, 'TINRequestTypeCd>', 'irs:TINRequestTypeCd>', [rfReplaceAll]);
  Result := StringReplace(Result, '<EIN', '<irs:EIN', [rfReplaceAll]);
  Result := StringReplace(Result, '/EIN', '/irs:EIN', [rfReplaceAll]);
  Result := StringReplace(Result, 'EmployerEIN', 'irs:EmployerEIN', [rfReplaceAll]);
  Result := StringReplace(Result, 'SSN>', 'irs:SSN>', [rfReplaceAll]);
  Result := StringReplace(Result, 'USZIPCd>', 'irs:USZIPCd>', [rfReplaceAll]);
  Result := StringReplace(Result, 'USZIPCdExt>', 'irs:USZIPCdExt>', [rfReplaceAll]);
  Result := StringReplace(Result, 'CityNm>', 'irs:CityNm>', [rfReplaceAll]);
  Result := StringReplace(Result, 'SignatureDt>', 'irs:SignatureDt>', [rfReplaceAll]);
  Result := StringReplace(Result, 'BirthDt>', 'irs:BirthDt>', [rfReplaceAll]);
  Result := StringReplace(Result, 'USZIPExtensionCd>', 'irs:USZIPExtensionCd>', [rfReplaceAll]);

  Result := StringReplace(Result, '</Form109495BTransmittalUpstream>', '', [rfReplaceAll]);
  Result := StringReplace(Result, '</Form109495CTransmittalUpstream>', '', [rfReplaceAll]);

  if not bReplacement then
  begin
    pStart:= AnsiPos('<OriginalUniqueSubmissionId>', Result);
    if pStart > 0 then
    begin
      pEnd:= AnsiPos('</OriginalUniqueSubmissionId>', Result);
      if (pEnd > 0) and (pEnd > pStart) then
       Delete(Result,pStart, (pEnd - pStart) + 29);
    end;
  end;
end;

class function TXMLHelper.FixXMLReportACASubmissionID(
  const XML: string; const Count:Integer): string;
var
 s: String;
begin
  s:= InttoStr(Count);
  Result := StringReplace(XML, '<SubmissionId>1', '<SubmissionId>' + s, [rfReplaceAll]);
end;

class function TXMLHelper.PaddWithZeros(S: string; FinalSize: integer): string;
begin
  Result := S;
  while Length(Result) < FinalSize do
    Result := '0' + Result;
end;

class function TXMLHelper.GenerateGMTDelta: string;
var
  TimeZone: TTimeZoneInformation;
  Bias: LongInt;
  H, M: integer;
begin
  GetTimeZoneInformation(TimeZone);
  Bias := TimeZone.Bias;
  H := Bias div -60;
  M := Abs(Bias mod 60);
  if H >= 0 then
    Result := '+'
  else
    Result := '-';
  Result := Result +
    PaddWithZeros(IntToStr(Abs(H)), 2) + ':' +
    PaddWithZeros(IntToStr(M), 2);
end;

class function TXMLHelper.LoadFromFile(FileName: string): string;
 // Helper function to load XML from file. Great for testing, but no use in
 // production.
var
  F: TextFile;
  C: Char;
begin
  Result := '';
  AssignFile(F, FileName);
  try
    Reset(F);
    while not EOF(F) do
    begin
      Read(F, C);
      Result := Result + C;
    end;
  finally
    CloseFile(F);
  end;
end;

class procedure TXMLHelper.SaveToFile(S, FileName: string);
var
  F: TextFile;
begin
  AssignFile(F, FileName);
  try
    Rewrite(F);
    Write(F, S);
  finally
    CloseFile(F);
  end;
end;

class function TXMLHelper.GenerateTimestamp: string;
begin
  Result := FormatDateTime('yyyy-mm-dd"T"hh:mm:ss', Now) + GenerateGMTDelta;
end;

class function TXMLHelper.GenerateTimestamp1094: string;
var
  d: TDateTime;
begin
  d:= UTCToGMTDateTime(NowUTC);
  Result := FormatDateTime('yyyymmdd"T"hhmmsszzz"Z"', d);
end;


class function TXMLHelper.GenerateTransmissionId: string;
begin
  Result := FormatDateTime('yyyymmddhhmmss', Now);
end;

class function TXMLHelper.GenerateContentLocation: string;
var
  ID : integer;
begin
  GenerateContentLocationCriticalSection.Enter;
  try
    Inc(ContentLocationID);
    ID := ContentLocationID;
  finally
    GenerateContentLocationCriticalSection.Leave;
  end;
  Result := PaddWithZeros(IntToStr(ID), MaxContentLocationLength);
end;

initialization
  GenerateContentLocationCriticalSection := TCriticalSection.Create;
finalization
  FreeAndNil(GenerateContentLocationCriticalSection);
end.

