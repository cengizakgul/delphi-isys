// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_PCLD_DM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Wwdatsrc,  EvTypes,  Variants, 
  kbmMemTable, ISKbmMemDataSet, ISBasicClasses, ISDataAccessComponents,
  EvDataAccessComponents, SDataDictClient, EvExceptions, EvUIComponents, EvClientDataSet;

type
  TCombineLiabilityDM = class(TDataModule)
    wwcsFEIN: TEvClientDataSet;
    wwcsFEINFEIN: TStringField;
    wwcsFEINClientName: TStringField;
    wwcsTaxTypeTotal: TEvClientDataSet;
    wwcsTaxTypeTotalAgency: TStringField;
    wwcsTaxTypeTotalTotal: TCurrencyField;
    wwcsTaxTypeSubTot1: TEvClientDataSet;
    wwcsTaxTypeSubTot1DueDate: TDateField;
    wwcsTaxTypeSubTot1CheckDate: TDateField;
    wwcsTaxTypeSubTot1RunNumber: TStringField;
    wwcsTaxTypeSubTot1Company: TStringField;
    wwcsTaxTypeSubTot1Total: TCurrencyField;
    wwcsTaxTypeSubTot2: TEvClientDataSet;
    wwcsTaxTypeSubTot2Total: TCurrencyField;
    wwcsTaxTypeSubTot2Taxsubtype: TStringField;
    wwcsCheckDateSubTot2: TEvClientDataSet;
    CurrencyField1: TCurrencyField;
    StringField1: TStringField;
    wwcsDeposit: TEvClientDataSet;
    wwcsCheckDateSubTot1: TEvClientDataSet;
    StringField2: TStringField;
    CurrencyField2: TCurrencyField;
    wwcsCheckDateTotal: TEvClientDataSet;
    CurrencyField3: TCurrencyField;
    DateField1: TDateField;
    DateField2: TDateField;
    StringField3: TStringField;
    StringField4: TStringField;
    wwcsFEINFEIN_ID: TIntegerField;
    dsFEIN: TwwDataSource;
    dsTaxTypeTotal: TwwDataSource;
    dsTaxTypeSubTot1: TwwDataSource;
    wwcsTaxTypeTotalFEIN_ID: TIntegerField;
    wwcsTaxTypeSubTot1TaxTypeTotId: TIntegerField;
    wwcsTaxTypeTotalTaxTypeTotId: TIntegerField;
    wwcsTaxTypeSubTot1TaxTypSubTot1Id: TIntegerField;
    wwcsTaxTypeSubTot2TaxTypeSubTot1Id: TIntegerField;
    wwcsCheckDateTotalFEIN_ID: TIntegerField;
    wwcsCheckDateTotalCheckDateTotId: TIntegerField;
    dsCheckDateTotal: TwwDataSource;
    dsCheckDateSubTot1: TwwDataSource;
    wwcsCheckDateSubTot1CheckDateSubTot1Id: TIntegerField;
    wwcsCheckDateSubTot1CheckDateTotId: TIntegerField;
    wwcsCheckDateSubTot2CheckDateSubTot1Id: TIntegerField;
    wwcsDepositTotal: TCurrencyField;
    wwcsDepositFEIN_ID: TIntegerField;
    wwcsDepositDepositGroup: TStringField;
    wwcsDepositDepositTotId: TIntegerField;
    dsDeposit: TwwDataSource;
    wwcsDepositSubTot1: TEvClientDataSet;
    dsDepositSubTot1: TwwDataSource;
    wwcsDepositSubTot2: TEvClientDataSet;
    wwcsDepositSubTot1TaxDepositReference: TStringField;
    wwcsDepositSubTot1DepositSubTot1Id: TIntegerField;
    wwcsDepositSubTot1DepositTotId: TIntegerField;
    wwcsDepositSubTot2DepositSubTot1Id: TIntegerField;
    wwcsDepositSubTot2DueDate: TDateField;
    wwcsDepositSubTot2CheckDate: TDateField;
    wwcsDepositSubTot2CoNumber: TStringField;
    wwcsDepositSubTot2RunNumber: TStringField;
    wwcsDepositDepositDate: TDateField;
    wwcsDepositSubTot2TaxType: TStringField;
    wwClientDataSet1: TEvClientDataSet;
    wwClientDataSet2: TEvClientDataSet;
    wwcsDepositSubTot1Status: TStringField;
    wwcsDepositSubTot1DepositType: TStringField;
    wwcsDepositSubTot1SY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField;
    wwcsDepositSubTot1Total: TCurrencyField;
    wwcsDepositSubTot1CL_NBR: TIntegerField;
    wwcsDepositSubTot1CO_TAX_DEPOSITS_NBR: TIntegerField;
    wwcsDepositSubTot1DepositDate: TDateField;
    wwcsDepositSubTot1DepositGroup: TStringField;
    wwcsFEINCL_NBR: TIntegerField;
    wwcsDepositSubTot2Amount: TCurrencyField;
    wwcsCheckDateTotalPR_NBR: TIntegerField;
    wwcsCheckDateTotalCL_NBR: TIntegerField;
    wwcsCheckDateSubTot1DepositGroup: TStringField;
    wwcsCheckDateSubTot2DepositGroup: TStringField;
    wwcsCheckDateSubTot1DataSetType: TStringField;
    wwcsCheckDateSubTot1ID: TIntegerField;
    wwcsCheckDateSubTot2ID: TIntegerField;
    wwcsTaxTypeSubTot1DepositGroup: TStringField;
    wwcsTaxTypeSubTot2DepositGroup: TStringField;
    wwcsTaxTypeSubTot1PR_NBR: TIntegerField;
    wwcsTaxTypeTotalDataSetType: TStringField;
    wwcsTaxTypeSubTot1ID: TIntegerField;
    wwcsTaxTypeSubTot2ID: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FDataReadIn: boolean;
    Count_FEIN_ID: integer;
    Count_DepositTotId: integer;
    Count_DepositSubTot1Id: integer;
    Count_CheckDateTotId: integer;
    Count_CheckDateSubTot1Id: integer;
    Count_TaxTypeTotId: integer;
    Count_TaxTypeSubTot1Id: integer;

  public
    { Public declarations }
    procedure Clear;
    procedure LocateCORecord(iCL_NBR, iCO_NBR: integer);
    procedure LocateCoTaxDepositRecord(CL_NBR, CO_TAX_DEPOSITS_NBR: integer);
    function GetDepositTotId(CL_NBR, CO_NBR: integer; DepositDate: TDateTime; DepositGroup: string): integer;
    function GetFEIN_ID(iCL_NBR, iCO_NBR: integer): integer;
    procedure AddDepositDetail(CL_NBR, CO_TAX_DEPOSITS_NBR: integer; CheckDate, DueDate, CoNumber, RunNumber, TaxType: string; Amount:
      Currency);
    procedure ReadRecords(FromDate, toDate: string; CL_NBR, CO_NBR: integer);
    procedure ConvertCompanyNumber(CompanyItem: string; var iCL_NBR, iCO_NBR: integer);
    property DataReadIn: boolean read FDataReadIn;
    procedure PopulateCompanyNumbers(aList: TStrings);
    function GetCompanyNumberString: string;
    procedure CalculateTotals;
  end;

var
  CombineLiabilityDM: TCombineLiabilityDM;

implementation

uses
  EvConsts, EvUtils, SDataStructure, isBaseClasses;
{$R *.DFM}

procedure TCombineLiabilityDM.PopulateCompanyNumbers(aList: TStrings);
var
  ReadOnlyClientList : IisStringList;
begin
  aList.Clear;
  aList.Add(' <all>');

  ReadOnlyClientList := GetReadOnlyClientCompanyList;

  with DM_TEMPORARY.TMP_CO do
  begin
    First;
    while not EOF do
    begin
      if (ReadOnlyClientList.IndexOf(fieldByName('CL_NBR').AsString+';'+fieldByName('CO_NBR').AsString) = -1) then
        aList.Add(GetCompanyNumberString);
      Next;
    end;
  end;

end;

function TCombineLiabilityDM.GetCompanyNumberString: string;
begin
  with DM_TEMPORARY.TMP_CO do
    result := FieldByName('CUSTOM_COMPANY_NUMBER').AsString + ' : ' + FieldByName('NAME').AsString;
end;

function TCombineLiabilityDM.GetDepositTotId(CL_NBR, CO_NBR: integer; DepositDate: TDateTime; DepositGroup: string): integer;
var
  FEIN_ID: integer;
begin
  FEIN_ID := GetFEIN_ID(CL_NBR, CO_NBR);
  with wwcsDeposit do
  begin
    if not Locate('FEIN_ID;DepositDate;DepositGroup', VarArrayOf([FEIN_ID, FormatDateTime('mm/dd/yyyy', DepositDate), DepositGroup]), [])
      then
    begin
      Count_DepositTotId := Count_DepositTotId + 1;
      Insert;
      FieldByName('FEIN_ID').AsInteger := FEIN_ID;
      FieldByName('DepositTotId').AsInteger := Count_DepositTotId;
      FieldByName('DepositDate').AsDateTime := DepositDate;
      FieldByName('DepositGroup').AsString := DepositGroup;
      Post;
    end;
    result := FieldByName('DepositTotId').AsInteger;
  end;
end;

procedure TCombineLiabilityDM.LocateCORecord(iCL_NBR, iCO_NBR: integer);
begin
  with DM_TEMPORARY.TMP_CO do
  begin

    if not Locate('CL_NBR;CO_NBR', VarArrayOf([iCL_NBR, iCO_NBR]), []) then
      raise EInconsistentData.CreateHelp('Company not found CL_NBR =' + intToStr(iCL_NBR) + ' CO_NBR=' + IntToStr(iCO_NBR), IDH_InconsistentData);
  end;
end;

function TCombineLiabilityDM.GetFEIN_ID(iCL_NBR, iCO_NBR: integer): integer;
var
  sFEIN: string;
begin
  with DM_TEMPORARY.TMP_CO do
  begin
    LocateCORecord(iCL_NBR, iCO_NBR);

    sFEIN := FieldByName('FEIN').AsString;

    if not wwcsFEIN.Locate('CL_NBR;FEIN', VarArrayOf([iCL_NBR, sFEIN]), []) then
    begin

      if not DM_TEMPORARY.TMP_CL.Locate('CL_NBR', VarArrayOf([iCL_NBR]), []) then
        raise EInconsistentData.CreateHelp('client not found CL_NBR =' + intToStr(iCL_NBR), IDH_InconsistentData);

      Count_FEIN_ID := Count_FEIN_ID + 1;

      wwcsFEIN.Insert;
      wwcsFEIN.FieldByName('FEIN_ID').AsInteger := Count_FEIN_ID;
      wwcsFEIN.FieldByName('CL_NBR').AsInteger := iCL_NBR;
      wwcsFEIN.FieldByName('FEIN').AsString := sFEIN;
      wwcsFEIN.FieldByName('ClientName').AsString := DM_TEMPORARY.TMP_CL.FieldByName('NAME').AsString;
      wwcsFEIN.Post;
    end;

    result := wwcsFEIN.FieldByName('FEIN_ID').AsInteger;
  end;
end;

procedure TCombineLiabilityDM.LocateCoTaxDepositRecord(CL_NBR, CO_TAX_DEPOSITS_NBR: integer);
begin
  if not wwcsDepositSubTot1.Locate('CL_NBR;CO_TAX_DEPOSITS_NBR', VarArrayOf([CL_NBR, CO_TAX_DEPOSITS_NBR]), []) then
    raise EInconsistentData.CreateHelp('TaxDepositRecord not found CL_NBR =' + intToStr(CL_NBR) + ' CO_TAX_DEPOSITS_NBR=' +
      IntToStr(CO_TAX_DEPOSITS_NBR), IDH_InconsistentData);
end;

procedure TCombineLiabilityDM.AddDepositDetail(CL_NBR, CO_TAX_DEPOSITS_NBR: integer; CheckDate, DueDate, CoNumber, RunNumber, TaxType:
  string; Amount: Currency);
begin
  if not wwcsDepositSubTot1.Locate('CL_NBR;CO_TAX_DEPOSITS_NBR', VarArrayOf([CL_NBR, CO_TAX_DEPOSITS_NBR]), []) then
    raise EInconsistentData.CreateHelp('TaxDepositRecord not found CL_NBR =' + intToStr(CL_NBR) + ' CO_TAX_DEPOSITS_NBR=' +
      IntToStr(CO_TAX_DEPOSITS_NBR), IDH_InconsistentData);

  with wwcsDepositSubTot2 do
  begin
    Insert;
    FieldByName('CheckDate').AsString := CheckDate;
    FieldByName('DueDate').AsString := DueDate;
    FieldByName('CoNumber').AsString := CoNumber;
    FieldByName('RunNumber').AsString := RunNumber;
    FieldByName('TaxType').AsString := TaxType;
    FieldByName('Amount').AsCurrency := Amount;
    Post;
  end;
end;

procedure TCombineLiabilityDM.CalculateTotals;
var
  T1, T2: Currency;
begin
  with wwcsFEIN do
  begin
    First;
    while not Eof do
    begin
      with wwcsCheckDateTotal do
      begin
        First;
        while not Eof do
        begin
          T1 := 0;
          with wwcsCheckDateSubTot1 do
          begin
            First;
            while not EOF do
            begin
              if FieldbyName('ID').AsInteger = 0 then
              begin
                T2 := 0;
                with wwcsCheckDateSubTot2 do
                begin
                  First;
                  while not EOF do
                  begin
                    T2 := T2 + FieldByName('total').AsCurrency;
                    Next;
                  end;
                end;
                Edit;
                FieldByName('Total').AsCurrency := T2;
                Post;
              end;
              T1 := T1 + FieldByName('Total').AsCurrency;
              Next;
            end;
          end;
          Edit;
          FieldByName('Total').AsCurrency := T1;
          Post;
          Next;
        end;
      end;
      with wwcsTaxTypeTotal do
      begin
        First;
        while not Eof do
        begin
          T1 := 0;
          with wwcsTaxTypeSubTot1 do
          begin
            First;
            while not EOF do
            begin
              if FieldbyName('ID').AsInteger = 0 then
              begin
                T2 := 0;
                with wwcsTaxTypeSubTot2 do
                begin
                  First;
                  while not EOF do
                  begin
                    T2 := T2 + FieldByName('total').AsCurrency;
                    Next;
                  end;
                end;
                Edit;
                FieldByName('Total').AsCurrency := T2;
                Post;
              end;
              T1 := T1 + FieldByName('Total').AsCurrency;
              Next;
            end;
          end;
          Edit;
          FieldByName('Total').AsCurrency := T1;
          Post;
          Next;
        end;
      end;
      with wwcsDeposit do
      begin
        First;
        while not Eof do
        begin
          T1 := 0;
          with wwcsDepositSubTot1 do
          begin
            First;
            while not EOF do
            begin
              T2 := 0;
              with wwcsDepositSubTot2 do
              begin
                First;
                while not EOF do
                begin
                  T2 := T2 + FieldByName('Amount').AsCurrency;
                  Next;
                end;
              end;
              Edit;
              FieldByName('Total').AsCurrency := T2;
              Post;
              T1 := T1 + T2;
              Next;
            end;
          end;
          Edit;
          FieldByName('Total').AsCurrency := T1;
          Post;
          Next;
        end;
      end;
      Next;
    end;
  end;
end;

procedure TCombineLiabilityDM.ReadRecords(FromDate, toDate: string; CL_NBR, CO_NBR: integer);
var
  S, F1, F2: string;

  procedure AddTaxDeposits(acs: TEvClientDataSet; DataSetType: string);
  var
    CL_NBR, CO_NBR, CO_TAX_DEPOSITS_NBR: Integer;
    PR_NBR: Variant;
    DepositDate: TDateTime;
    DepositGroup: string;
    DepositTotId: integer;
    CONumber: string;
    TaxType: string;
    RunNumber: string;
    FEIN_ID: integer;
    CheckDate, DueDate: string;
    CheckDateTotId: integer;
    CheckDateSubTot1Id: integer;
    TaxTypeTotId: integer;
    TaxTypeSubTot1Id: integer;
    Agency: string;
    TaxSubType: string;
    ID, SubTot1Id: integer;
    SubTot1DepositGroup: string;
    Amount: currency;
  begin
    with acs do
    begin
      First;
      while not EOF do
      begin
        CL_NBR := FieldByName('CL_NBR').AsInteger;
        CO_NBR := FieldByName('CO_NBR').AsInteger;
        PR_NBR := FieldByName('PR_NBR').Value;
        CheckDate := FormatDateTime('mm/dd/yyyy', FieldByName('CHECK_DATE').AsDateTime);
        DueDate := FormatDateTime('mm/dd/yyyy', FieldByName('DUE_DATE').AsDateTime);
        Amount := FieldByName('Amount').AsCurrency;
        FEIN_ID := GetFEIN_ID(CL_NBR, CO_NBR);
        CO_TAX_DEPOSITS_NBR := FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger;
        LocateCORecord(CL_NBR, CO_NBR);
        CONumber := DM_TEMPORARY.TMP_CO.FieldByName('CUSTOM_COMPANY_NUMBER').AsString;
        TaxType := 'xx';
        RunNumber := '';
        Agency := '';
        TaxSubType := '';
        if not VarIsNull(PR_NBR) then
        begin
          if wwcsCheckDateTotal.Locate('CL_NBR;PR_NBR', VarArrayOf([CL_NBR, PR_NBR]), []) then
            RunNumber := wwcsCheckDateTotal.FieldByName('RunNumber').AsString
          else
          begin
            DM_TEMPORARY.TMP_PR.DataRequired('(PR_NBR=' + IntToStr(PR_NBR) + ' and CL_NBR=' + IntToStr(CL_NBR) + ')');
            if not (DM_TEMPORARY.TMP_PR.RecordCount = 1) then
              raise EInconsistentData.CreateHelp('Payroll not found CL_NBR =' + intToStr(CL_NBR) + ' PR_NBR=' + IntToStr(PR_NBR), IDH_InconsistentData);
            RunNumber := DM_TEMPORARY.TMP_PR.FieldByName('RUN_NUMBER').AsString;
          end;
        end;
        ID := 0; // to avoid warning message during compilation
        if DataSetType = 'Fed' then
        begin
          ID := FieldByName('CO_FED_TAX_LIABILITIES_NBR').AsInteger;
          TaxType := ConvertCode(TCO_FED_TAX_LIABILITIES, 'TAX_TYPE', FieldByName('TAX_TYPE').AsString);
          if FieldByName('TAX_TYPE').AsString = TAX_LIABILITY_TYPE_ER_FUI then
            Agency := 'Fed 940'
          else
          begin
            Agency := 'Fed 941';
            TaxSubType := TaxType;
          end;
        end;
        if DataSetType = 'State' then
        begin
          ID := FieldByName('CO_STATE_TAX_LIABILITIES_NBR').AsInteger;
          TaxType := FieldByName('STATE').AsString + ' - ' + ConvertCode(TCO_STATE_TAX_LIABILITIES, 'TAX_TYPE',
            FieldByName('TAX_TYPE').AsString);
          Agency := TaxType;
        end;
        if DataSetType = 'SUI' then
        begin
          ID := FieldByName('CO_SUI_LIABILITIES_NBR').AsInteger;
          if not DM_SYSTEM_STATE.SY_SUI.Locate('SY_SUI_NBR', FieldByName('SY_SUI_NBR').AsInteger, []) then
            raise EInconsistentData.CreateHelp('System SUI is not set for company ' + ConvertNull(DM_TEMPORARY.TMP_CO.Lookup('CL_NBR;CO_NBR',
              VarArrayOf([CL_NBR, FieldValues['CO_NBR']]), 'NAME'), 'N/A') {+ FieldValues['DESCRIPTION']}, IDH_InconsistentData);
          TaxType := DM_SYSTEM_STATE.SY_SUI.FieldByName('SUI_TAX_NAME').AsString;
          Agency := TaxType;
        end;
        if DataSetType = 'Local' then
        begin
          ID := FieldByName('CO_LOCAL_TAX_LIABILITIES_NBR').AsInteger;
          if not DM_SYSTEM_LOCAL.SY_LOCALS.Locate('SY_LOCALS_NBR', FieldByName('SY_LOCALS_NBR').AsInteger, []) then
            raise EInconsistentData.CreateHelp('System locals is not set for company ' + ConvertNull(DM_TEMPORARY.TMP_CO.Lookup('CL_NBR;CO_NBR',
              VarArrayOf([CL_NBR, FieldValues['CO_NBR']]), 'NAME'), 'N/A') {+ FieldValues['DESCRIPTION']}, IDH_InconsistentData);
          Assert(DM_SYSTEM_STATE.SY_STATES.Locate('SY_STATES_NBR', DM_SYSTEM_LOCAL.SY_LOCALS['SY_STATES_NBR'], []));
          TaxType := DM_SYSTEM_STATE.SY_STATES['STATE']+ '-'+ DM_SYSTEM_LOCAL.SY_LOCALS.FieldByName('NAME').AsString;
          Agency := TaxType;
        end;
        DepositGroup := '';
        if CO_TAX_DEPOSITS_NBR > 0 then
        begin
          if wwcsDepositSubTot1.Locate('CL_NBR;CO_TAX_DEPOSITS_NBR', VarArrayOf([CL_NBR, CO_TAX_DEPOSITS_NBR]), []) then
            DepositGroup := wwcsDepositSubTot1.FieldByName('DepositGroup').AsString
          else
          begin
            DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.DataRequired('(CL_NBR=' + IntToStr(CL_NBR) + ' and CO_TAX_DEPOSITS_NBR=' + IntToStr(CO_TAX_DEPOSITS_NBR) + ')');
            if not (DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.RecordCount = 1) then
              raise EInconsistentData.CreateHelp('TaxDepositRecord not found CL_NBR =' + intToStr(CL_NBR) + ' CO_TAX_DEPOSITS_NBR=' +
                IntToStr(CO_TAX_DEPOSITS_NBR), IDH_InconsistentData);
            DepositDate := DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('STATUS_DATE').AsDateTime;
            DepositGroup := DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('STATUS_DATE').AsString; // ?!?
            DepositTotId := GetDepositTotId(CL_NBR, CO_NBR, DepositDate, DepositGroup);
            Count_DepositSubTot1Id := Count_DepositSubTot1Id + 1;
            wwcsDepositSubTot1.Insert;
            wwcsDepositSubTot1.FieldByName('TaxDepositReference').AsString :=
              DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('TAX_PAYMENT_REFERENCE_NUMBER').AsString;
            wwcsDepositSubTot1.FieldByName('Status').AsString := DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('Status').AsString;
            wwcsDepositSubTot1.FieldByName('Deposit_type').AsString :=
              DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('DEPOSIT_TYPE').AsString;
            wwcsDepositSubTot1.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').AsInteger :=
              DM_TEMPORARY.TMP_CO_TAX_DEPOSITS.FieldByName('SY_GL_AGENCY_FIELD_OFFICE_NBR').AsInteger;
            wwcsDepositSubTot1.FieldByName('CL_NBR').AsInteger := CL_NBR;
            wwcsDepositSubTot1.FieldByName('DepositDate').AsDateTime := DepositDate;
            wwcsDepositSubTot1.FieldByName('DepositGroup').AsString := DepositGroup;
            wwcsDepositSubTot1.FieldByName('CO_TAX_DEPOSITS_NBR').AsInteger := CO_TAX_DEPOSITS_NBR;
            wwcsDepositSubTot1.FieldByName('DepositTotId').AsInteger := DepositTotId;
            wwcsDepositSubTot1.FieldByName('DepositSubTot1Id').AsInteger := Count_DepositSubTot1Id;
            wwcsDepositSubTot1.Post;
          end;
          AddDepositDetail(
            CL_NBR,
            CO_TAX_DEPOSITS_NBR,
            CheckDate,
            DueDate,
            CONumber,
            RunNumber,
            TaxType,
            FieldByName('Amount').AsCurrency);
        end;
        if not wwcsCheckDateTotal.Locate('FEIN_ID;CheckDate;DueDate;RunNumber;PR_NBR', VarArrayOf([FEIN_ID, CheckDate, DueDate, RunNumber,
          PR_NBR]), []) then
        begin
          Count_CheckDateTotId := Count_CheckDateTotId + 1;
          wwcsCheckDateTotal.Insert;
          wwcsCheckDateTotal.FieldByName('CL_NBR').AsInteger := CL_NBR;
          wwcsCheckDateTotal.FieldByName('CheckDateTotId').AsInteger := Count_CheckDateTotId;
          wwcsCheckDateTotal.FieldByName('FEIN_ID').AsInteger := FEIN_ID;
          wwcsCheckDateTotal.FieldByName('RunNumber').AsString := RunNumber;
          wwcsCheckDateTotal.FieldByName('CoNumber').AsString := CoNumber;
          wwcsCheckDateTotal.FieldByName('DueDate').AsString := DueDate;
          wwcsCheckDateTotal.FieldByName('CheckDate').AsString := CheckDate;
          wwcsCheckDateTotal.FieldByName('PR_NBR').Value := PR_NBR;
          wwcsCheckDateTotal.Post;
        end;
        CheckDateTotId := wwcsCheckDateTotal.FieldByName('CheckDateTotId').AsInteger;
        SubTot1DepositGroup := '';
        SubTot1Id := 0;
        if Length(TaxSubType) = 0 then
        begin
          SubTot1DepositGroup := DepositGroup;
          SubTot1Id := ID;
        end;
        if not wwcsCheckDateSubTot1.Locate('CheckDateTotId;DepositGroup;Agency;DataSetType;ID', VarArrayOf([CheckDateTotId,
          SubTot1DepositGroup, Agency, DataSetType, SubTot1Id]), []) then
        begin
          Count_CheckDateSubTot1Id := Count_CheckDateSubTot1Id + 1;
          wwcsCheckDateSubTot1.Insert;
          wwcsCheckDateSubTot1.FieldByName('CheckDateSubTot1Id').AsInteger := Count_CheckDateSubTot1Id;
          wwcsCheckDateSubTot1.FieldByName('CheckDateTotId').AsInteger := CheckDateTotId;
          wwcsCheckDateSubTot1.FieldByName('DepositGroup').AsString := SubTot1DepositGroup;
          wwcsCheckDateSubTot1.FieldByName('Agency').AsString := Agency;
          wwcsCheckDateSubTot1.FieldByName('DataSetType').AsString := DataSetType;
          wwcsCheckDateSubTot1.FieldByName('ID').AsInteger := SubTot1Id;
          if SubTot1Id > 0 then
          begin
            wwcsCheckDateSubTot1.FieldByName('Total').AsCurrency := Amount;
          end;
          wwcsCheckDateSubTot1.Post;
        end;
        CheckDateSubTot1Id := wwcsCheckDateSubTot1.FieldByName('CheckDateSubTot1Id').AsInteger;
        if Length(TaxSubType) > 0 then
        begin
          wwcsCheckDateSubTot2.Insert;
          wwcsCheckDateSubTot2.FieldByName('CheckDateSubTot1Id').AsInteger := CheckDateSubTot1Id;
          wwcsCheckDateSubTot2.FieldByName('ID').AsInteger := ID;
          wwcsCheckDateSubTot2.FieldByName('DepositGroup').AsString := DepositGroup;
          wwcsCheckDateSubTot2.FieldByName('TaxSubType').AsString := TaxSubType;
          wwcsCheckDateSubTot2.FieldByName('Total').AsCurrency := Amount;
          wwcsCheckDateSubTot2.Post;
        end;
        if not wwcsTaxTypeTotal.Locate('FEIN_ID;Agency;DataSetType', VarArrayOf([FEIN_ID, Agency, DataSetType]), []) then
        begin
          Count_TaxTypeTotId := Count_TaxTypeTotId + 1;
          wwcsTaxTypeTotal.Insert;
          wwcsTaxTypeTotal.FieldByName('TaxTypeTotId').AsInteger := Count_TaxTypeTotId;
          wwcsTaxTypeTotal.FieldByName('FEIN_ID').AsInteger := FEIN_ID;
          wwcsTaxTypeTotal.FieldByName('Agency').AsString := Agency;
          wwcsTaxTypeTotal.FieldByName('DataSetType').AsString := DataSetType;
          wwcsTaxTypeTotal.Post;
        end;
        TaxTypeTotId := wwcsTaxTypeTotal.FieldByName('TaxTypeTotId').AsInteger;
        if not wwcsTaxTypeSubTot1.Locate('TaxTypeTotId;PR_NBR', VarArrayOf([TaxTypeTotId, PR_NBR]), []) then
        begin
          Count_TaxTypeSubTot1Id := Count_TaxTypeSubTot1Id + 1;
          wwcsTaxTypeSubTot1.Insert;
          wwcsTaxTypeSubTot1.FieldByName('PR_NBR').Value := PR_NBR;
          wwcsTaxTypeSubTot1.FieldByName('TaxTypeSubTot1Id').AsInteger := Count_TaxTypeSubTot1Id;
          wwcsTaxTypeSubTot1.FieldByName('TaxTypeTotId').AsInteger := TaxTypeTotId;
          wwcsTaxTypeSubTot1.FieldByName('DepositGroup').AsString := SubTot1DepositGroup;
          wwcsTaxTypeSubTot1.FieldByName('ID').AsInteger := SubTot1Id;
          wwcsTaxTypeSubTot1.FieldByName('RunNumber').AsString := RunNumber;
          wwcsTaxTypeSubTot1.FieldByName('CoNumber').AsString := CoNumber;
          wwcsTaxTypeSubTot1.FieldByName('DueDate').AsString := DueDate;
          wwcsTaxTypeSubTot1.FieldByName('CheckDate').AsString := CheckDate;
          if SubTot1Id > 0 then
            wwcsTaxTypeSubTot1.FieldByName('Total').AsCurrency := Amount;
          wwcsTaxTypeSubTot1.Post;
        end;
        TaxTypeSubTot1Id := wwcsTaxTypeSubTot1.FieldByName('TaxTypeSubTot1Id').AsInteger;
        if Length(TaxSubType) > 0 then
        begin
          wwcsTaxTypeSubTot2.Insert;
          wwcsTaxTypeSubTot2.FieldByName('TaxTypeSubTot1Id').AsInteger := TaxTypeSubTot1Id;
          wwcsTaxTypeSubTot2.FieldByName('ID').AsInteger := ID;
          wwcsTaxTypeSubTot2.FieldByName('DepositGroup').AsString := DepositGroup;
          wwcsTaxTypeSubTot2.FieldByName('TaxSubType').AsString := TaxSubType;
          wwcsTaxTypeSubTot2.FieldByName('Total').AsCurrency := Amount;
          wwcsTaxTypeSubTot2.Post;
        end;
        Next;
      end;
    end;
  end;
begin
  S := '';
  if not (CL_NBR = 0) then
    S := 'CL_NBR = ' + IntToStr(CL_NBR) + ' and ' + 'CO_NBR=' + IntToStr(CO_NBR);

  if Length(FromDate) > 0 then
  begin
    F1 := '(DUE_DATE >= ''' + FromDate + ''')';
    F2 := '(CHECK_DATE >= ''' + FromDate + ''')';
  end;

  if Length(ToDate) > 0 then
  begin
    if Length(F1) > 0 then
    begin
      F1 := F1 + ' and ';
      F2 := F2 + ' and ';
    end;
    F1 := F1 + '(DUE_DATE <= ''' + ToDate + ''')';
    F2 := F2 + '(CHECK_DATE <= ''' + ToDate + ''')';
  end;

  if Length(F1) > 0 then
    F1 := '((' + F1 + ') or (' + F2 + '))';

  if Length(F1) > 0 then
  begin
    if length(S) > 0 then
      S := S + ' and ';
    S := S + F1;
  end;
  S := '(' + S + ') and THIRD_PARTY=''N''';

  S := S + GetReadOnlyClintCompanyListFilter;
  DM_TEMPORARY.TMP_CO_FED_TAX_LIABILITIES.DataRequired(S);
  DM_TEMPORARY.TMP_CO_STATE_TAX_LIABILITIES.DataRequired(S);
  DM_TEMPORARY.TMP_CO_LOCAL_TAX_LIABILITIES.DataRequired(S);
  DM_TEMPORARY.TMP_CO_SUI_LIABILITIES.DataRequired(S);

  AddTaxDeposits(DM_TEMPORARY.TMP_CO_FED_TAX_LIABILITIES, 'Fed');
  AddTaxDeposits(DM_TEMPORARY.TMP_CO_STATE_TAX_LIABILITIES, 'State');
  AddTaxDeposits(DM_TEMPORARY.TMP_CO_LOCAL_TAX_LIABILITIES, 'Local');
  AddTaxDeposits(DM_TEMPORARY.TMP_CO_SUI_LIABILITIES, 'SUI');
  CalculateTotals;

  DM_TEMPORARY.TMP_CO_FED_TAX_LIABILITIES.Close;
  DM_TEMPORARY.TMP_CO_STATE_TAX_LIABILITIES.Close;
  DM_TEMPORARY.TMP_CO_LOCAL_TAX_LIABILITIES.Close;
  DM_TEMPORARY.TMP_CO_SUI_LIABILITIES.Close;

  FDataReadIn := True;

end;

procedure TCombineLiabilityDM.ConvertCompanyNumber(CompanyItem: string; var iCL_NBR, iCO_NBR: integer);
begin
  iCL_NBR := 0;
  iCO_NBR := 0;

  if CompanyItem = ' <all>' then
    exit;

  with DM_TEMPORARY.TMP_CO do
  begin
    First;
    while not EOF do
    begin
      if GetCompanyNumberString = CompanyItem then
      begin
        iCL_NBR := FieldByName('CL_NBR').AsInteger;
        iCO_NBR := FieldByName('CO_NBR').AsInteger;
        break;
      end;
      Next;
    end;
  end;

end;

procedure TCombineLiabilityDM.Clear;
begin
  wwcsDepositSubTot2.EmptyDataSet;
  wwcsDepositSubTot1.EmptyDataSet;
  wwcsDeposit.EmptyDataSet;
  wwcsFEIN.EmptyDataSet;
  wwcsCheckDateTotal.EmptyDataSet;
  wwcsCheckDateSubTot1.EmptyDataSet;
  wwcsCheckDateSubTot2.EmptyDataSet;
  wwcsTaxTypeTotal.EmptyDataSet;
  wwcsTaxTypeSubTot1.EmptyDataSet;
  wwcsTaxTypeSubTot2.EmptyDataSet;

  Count_FEIN_ID := 0;
  Count_DepositTotId := 0;
  Count_DepositSubTot1Id := 0;
  Count_CheckDateTotId := 0;
  Count_CheckDateSubTot1Id := 0;
  Count_TaxTypeTotId := 0;
  Count_TaxTypeSubTot1Id := 0;
  FDataReadIn := False;
end;

procedure TCombineLiabilityDM.DataModuleCreate(Sender: TObject);
begin
  FDataReadIn := False;

  DM_TEMPORARY.TMP_CO.DataRequired('ALL');
  DM_TEMPORARY.TMP_CL.DataRequired('ALL');
  DM_TEMPORARY.TMP_CO_STATES.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_SUI.DataRequired('ALL');
  DM_SYSTEM_STATE.SY_STATES.DataRequired('ALL');
  DM_SYSTEM_LOCAL.SY_LOCALS.DataRequired('ALL');

  if not wwcsFEIN.Active then
    wwcsFEIN.CreateDataSet;
  if not wwcsDeposit.Active then
    wwcsDeposit.CreateDataSet;
  if not wwcsDepositSubTot1.Active then
    wwcsDepositSubTot1.CreateDataSet;
  if not wwcsDepositSubTot2.Active then
    wwcsDepositSubTot2.CreateDataSet;
  if not wwcsCheckDateTotal.Active then
    wwcsCheckDateTotal.CreateDataSet;
  if not wwcsCheckDateSubTot1.Active then
    wwcsCheckDateSubTot1.CreateDataSet;
  if not wwcsCheckDateSubTot2.Active then
    wwcsCheckDateSubTot2.CreateDataSet;

  if not wwcsTaxTypeTotal.Active then
    wwcsTaxTypeTotal.CreateDataSet;
  if not wwcsTaxTypeSubTot1.Active then
    wwcsTaxTypeSubTot1.CreateDataSet;
  if not wwcsTaxTypeSubTot2.Active then
    wwcsTaxTypeSubTot2.CreateDataSet;
end;

end.
