// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SOutOfBalanceError;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,  ExtCtrls, SFrameEntry, SBalanceReportScreen, db, EvUtils,
  Buttons, EvUIComponents;

type
  TOutOfBalanceError = class(TForm)
    BalanceReportScreen: TBalanceReportScreen;
    pnl: TevPanel;
    lblErrorMsg: TevLabel;
    cbFilter: TevCheckBox;
    btnClose: TevBitBtn;
    procedure btnCloseClick(Sender: TObject);
    procedure cbFilterClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BalanceReportScreencdsSummaryFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses SBalanceInformation;

{$R *.DFM}

procedure TOutOfBalanceError.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TOutOfBalanceError.cbFilterClick(Sender: TObject);
begin
  BalanceReportScreen.cdsSummary.Filtered := cbFilter.Checked;
end;

procedure TOutOfBalanceError.FormShow(Sender: TObject);
begin
  BalanceReportScreen.cdsSummary.Filtered := True;
end;

procedure TOutOfBalanceError.BalanceReportScreencdsSummaryFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := not DataSet.FieldByName('DifLiabHist').IsNull and
            (Abs(RoundTwo(DataSet['DifLiabHist'])) > ConvertNull(DataSet['Margin'], 0)) or
            not DataSet.FieldByName('DifWageHist').IsNull and
            (Abs(RoundTwo(DataSet['DifWageHist'])) > ConvertNull(DataSet['Margin'], 0));
end;

end.
