// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_TMP_CO_RETURN_QUEUE;

interface
// Tax Return Queue (comment added so that if you search for that term this
// file shows up)

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,  StdCtrls, Variants, EvBasicUtils, ISUtils, ISZippingRoutines, Printers,
  ComCtrls, Grids, Wwdbigrd, Wwdbgrid, Buttons, wwdblook, DBCtrls, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, ExtCtrls,  SFrameEntry, EvContext, EvMainboard,
  wwdbdatetimepicker, SCompanyMuliSelectScreen, Spin, EvTypes, Menus,
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, Wwdatsrc, isBaseClasses, EvCommonInterfaces,
  EvDataAccessComponents, ISDataAccessComponents, isBasicUtils, isVCLBugFix, EvExceptions,ISErrorUtils,
  EvDataSet, SDDClasses, SDataDictsystem, SDataStructure, evUtils, EvSendMail, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, isUIwwDBEdit,
  isUIwwDBDateTimePicker, isUIwwDBLookupCombo, isUIwwDBComboBox, isUIEdit,
  LMDCustomButton, LMDButton, isUILMDButton, EvStreamUtils
  ,StrUtils
  ,SPopupCompanySelectHelper, SReturnTransmissionFile94X, SReturnTransmissionFile1094,  SXMLHelper;

type
  TEDIT_TMP_CO_TAX_RETURN_QUEUE = class(TFrameEntry)
    cdQueue: TevClientDataSet;
    cdSelectedQueue: TevClientDataSet;
    cdQueueCL_NBR: TIntegerField;
    cdQueueCO_TAX_RETURN_QUEUE_NBR: TIntegerField;
    cdQueueTAX_NON_TAX: TStringField;
    cdQueueSYSTEM_TAX_TYPE: TStringField;
    cdQueueSTATUS: TStringField;
    cdQueueSTATUS_DATE: TDateTimeField;
    cdQueuePERIOD_END_DATE: TDateField;
    cdQueueDUE_DATE: TDateField;
    cdQueueRETURN_FREQUENCY: TStringField;
    dsQueue: TevDataSource;
    dsSelectedQueue: TevDataSource;
    cdSelectedQueueCL_NBR: TIntegerField;
    cdSelectedQueueCO_TAX_RETURN_QUEUE_NBR: TIntegerField;
    cdSelectedQueueTAX_SERVICE: TStringField;
    cdSelectedQueueSYSTEM_TAX_TYPE: TStringField;
    cdSelectedQueuePERIOD_END_DATE: TDateField;
    cdSelectedQueueSTATUS: TStringField;
    cdSelectedQueueSTATUS_DATE: TDateTimeField;
    cdSelectedQueueDUE_DATE: TDateField;
    cdSelectedQueueRETURN_FREQUENCY: TStringField;
    cdQueueSY_GL_AGENCY_REPORT_NBR: TIntegerField;
    cdQueueSB_COPY_PRINTED: TStringField;
    cdQueueCLIENT_COPY_PRINTED: TStringField;
    cdQueueAGENCY_COPY_PRINTED: TStringField;
    cdSelectedQueueSY_GL_AGENCY_REPORT_NBR: TIntegerField;
    cdSelectedQueueSB_COPY_PRINTED: TStringField;
    cdSelectedQueueCLIENT_COPY_PRINTED: TStringField;
    cdSelectedQueueAGENCY_COPY_PRINTED: TStringField;
    cdQueueCO_NAME: TStringField;
    cdSelectedQueueCO_NAME: TStringField;
    cdQueueCO_NBR: TIntegerField;
    cdQueueSY_GLOBAL_AGENCY_NBR: TIntegerField;
    cdQueueSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField;
    cdQueueSY_REPORTS_NBR: TIntegerField;
    cdSelectedQueueSY_REPORTS_NBR: TIntegerField;
    cdQueueSY_REPORTS_DESC: TStringField;
    cdSelectedQueueSY_REPORTS_DESC: TStringField;
    cdQueueHIDDEN: TStringField;
    cdSelectedQueueCO_NBR: TIntegerField;
    MessageLogSaveDialog: TSaveDialog;
    cdQueuePRIORITY: TStringField;
    cdSelectedQueuePRIORITY: TStringField;
    cdQueueco_number: TStringField;
    cdSelectedQueueco_number: TStringField;
    cdSummary: TevClientDataSet;
    cdSummaryCL_NBR: TIntegerField;
    cdSummaryCO_NBR: TIntegerField;
    cdSummaryTAX_SERVICE: TStringField;
    cdSummaryCO_NAME: TStringField;
    cdSummaryco_number: TStringField;
    cdSummaryprocessed: TBooleanField;
    cdSummaryonhold: TBooleanField;
    dsSummary: TevDataSource;
    cdHeldList: TevClientDataSet;
    cdQueuetaken: TStringField;
    cdQueueUserNumber: TIntegerField;
    cdQueuecl_number: TStringField;
    cdSelectedQueuecl_number: TStringField;
    cdQueueCONSOLIDATION_DESCR: TStringField;
    cdConsolidation: TevClientDataSet;
    cdClCoInfo: TevClientDataSet;
    cdQueueCONSOLIDATION: TStringField;
    cdQueueKEEP_ONHOLD: TStringField;
    cdSelectedQueueCONSOLIDATION_DESCR: TStringField;
    cdQueuePRIORITY2: TStringField;
    cdSelectedQueuePRIORITY2: TStringField;
    cdQueueHAVE_ACOPY: TStringField;
    cdQueueHAVE_CCOPY: TStringField;
    cdQueueHAVE_SCOPY: TStringField;
    cdQueuePRODUCE_ASCII_FILE: TStringField;
    cdSelectedQueueHAVE_ACOPY: TStringField;
    cdSelectedQueueHAVE_CCOPY: TStringField;
    cdSelectedQueueHAVE_SCOPY: TStringField;
    cdAnswers: TevClientDataSet;
    cdAnswersCL_NBR: TIntegerField;
    cdAnswersCO_NBR: TIntegerField;
    cdAnswersSTOP: TBooleanField;
    cdSummaryCONSOLIDATION: TStringField;
    cdEe: TevClientDataSet;
    cdEeCL_NBR: TIntegerField;
    cdEeCO_NBR: TIntegerField;
    cdEeLAST_NAME: TStringField;
    cdEeFIRST_NAME: TStringField;
    cdEeCL_PERSON_NBR: TIntegerField;
    cdEeSELECTED: TBooleanField;
    dsEe: TevDataSource;
    cdEeCONSOLIDATION: TBooleanField;
    cdEePERIOD_END: TDateField;
    dsBriefReportList: TevDataSource;
    cdEeCUSTOM_EMPLOYEE_NUMBER: TStringField;
    cdSummaryErrorMessage: TStringField;
    cdBriefReportList: TevClientDataSet;
    cdQueueCONSOLIDATION_DESCR2: TStringField;
    cdQueueSY_REPORTS_GROUP_NBR: TIntegerField;
    cdQueueCL_CO_CONSOLIDATION_NBR: TIntegerField;
    cdCoList: TevClientDataSet;
    cdCoListcl_nbr: TIntegerField;
    cdCoListco_nbr: TIntegerField;
    cdCoListCL_CO_CONSOLIDATION_NBR: TIntegerField;
    cdCoListcustom_company_number: TStringField;
    cdCoListcompany_name: TStringField;
    cdCoListCONS: TStringField;
    cdCoListtax_service: TStringField;
    cdSummaryPreProcessDate: TDateTimeField;
    cdSummaryCL_CO_CONSOLIDATION_NBR: TIntegerField;
    cdProcessed: TevClientDataSet;
    cdSelectedQueueSY_GLOBAL_AGENCY_NBR: TIntegerField;
    evPanel2: TevPanel;
    gbTop: TevGroupBox;
    Label1: TevLabel;
    Label2: TevLabel;
    Label3: TevLabel;
    Label4: TevLabel;
    Label5: TevLabel;
    Label6: TevLabel;
    Label19: TevLabel;
    Bevel1: TEvBevel;
    Bevel2: TEvBevel;
    Bevel3: TEvBevel;
    evLabel6: TevLabel;
    cbQuarters: TevComboBox;
    cbType: TevComboBox;
    cbDateFrom: TevDateTimePicker;
    cbDateTill: TevDateTimePicker;
    cbStatus: TevComboBox;
    cbTaxService: TevComboBox;
    DateKindSelectionPanel: TevPanel;
    cbDateIsDueDate: TevRadioButton;
    cbDateIsPeriodEndDate: TevRadioButton;
    cbMultiDelete: TevCheckBox;
    btnAdd: TevBitBtn;
    btnDel: TevBitBtn;
    cbSimpleMode: TevCheckBox;
    cbTypeNot: TevCheckBox;
    cdBriefMagmediaList: TevClientDataSet;
    cdBriefMagmediaCompanyList: TevClientDataSet;
    cdBriefMagmediaListSY_REPORTS_NBR: TIntegerField;
    cdBriefMagmediaListSY_REPORTS_DESC: TStringField;
    cdBriefMagmediaListSY_REPORTS_GROUP_NBR: TIntegerField;
    cdBriefMagmediaCompanyListCL_NBR: TIntegerField;
    cdBriefMagmediaCompanyListCO_TAX_RETURN_QUEUE_NBR: TIntegerField;
    cdBriefMagmediaCompanyListTAX_SERVICE: TStringField;
    cdBriefMagmediaCompanyListCO_NAME: TStringField;
    cdBriefMagmediaCompanyListco_number: TStringField;
    cdBriefMagmediaCompanyListUserNumber: TIntegerField;
    cdBriefMagmediaCompanyListCONSOLIDATION_DESCR: TStringField;
    cdBriefMagmediaCompanyListCONSOLIDATION_DESCR2: TStringField;
    cdBriefMagmediaCompanyListCONSOLIDATION: TStringField;
    cdBriefMagmediaCompanyListSY_REPORTS_GROUP_NBR: TIntegerField;
    cdBriefMagmediaCompanyListColor: TIntegerField;
    cdBriefMagmediaCompanyListCO_NBR: TIntegerField;
    cdBriefMagmediaCompanyListPRODUCE_ASCII_FILE: TStringField;
    cdQueueGlAgencyFOName: TStringField;
    cdBriefMagmediaListGlAgencyFOName: TStringField;
    btnApplySelect: TevBitBtn;
    btnEnlist: TevBitBtn;
    cdCoListPRIMARY_CO_NBR: TIntegerField;
    cbAnnually: TevCheckBox;
    cbQuarterly: TevCheckBox;
    cbMonthly: TevCheckBox;
    cdStatuses: TevClientDataSet;
    cdQueueHOLD_RETURN_QUEUE: TStringField;
    menuHist: TevPopupMenu;
    cdSelectedQueueCL_BLOB_NBR: TIntegerField;
    cdSummarypaper_processed: TBooleanField;
    cdSummaryhas_paper: TBooleanField;
    cdEeEE_NBR: TIntegerField;
    cdEeSSN: TStringField;
    cbSemiMonthly: TevCheckBox;
    evUseVMR: TevCheckBox;
    cdXMLEnvelopeReports: TevClientDataSet;
    cdXMLEnvelopeReportsnbr: TIntegerField;
    cdXMLEnvelopeReportsreport_description: TStringField;
    dsXMLEnvelopeReports: TevDataSource;
    cdSelectedQueueSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField;
    PCMain: TevPageControl;
    tsListOption: TTabSheet;
    tsTaxReturn: TTabSheet;
    Pc: TevPageControl;
    TabSheet1: TTabSheet;
    wwDBGrid3: TevDBGrid;
    cbGrid3ReturnType: TevDBComboBox;
    cbGrid3Status: TevDBComboBox;
    cbGrid3Freq: TevDBComboBox;
    tsCompanies: TTabSheet;
    evPanel11: TevPanel;
    Button1: TevButton;
    evPanel12: TevPanel;
    wwDBGrid5: TevDBGrid;
    tsDetail: TTabSheet;
    Label8: TevLabel;
    Label10: TevLabel;
    Label7: TevLabel;
    Label11: TevLabel;
    Label12: TevLabel;
    Label15: TevLabel;
    Label13: TevLabel;
    Label16: TevLabel;
    Label17: TevLabel;
    Label18: TevLabel;
    Label14: TevLabel;
    evLabel1: TevLabel;
    cbDetailCo: TevDBLookupCombo;
    cbDetailStatus: TevDBComboBox;
    cbDetailCl: TevDBLookupCombo;
    cbDetailAgCopyPrinted: TDBCheckBox;
    cbDetailClCopyPrinted: TDBCheckBox;
    cbDetailSbCopyPrinted: TDBCheckBox;
    cdDetailPeriodEndDate: TevDBDateTimePicker;
    cbDetailDueDate: TevDBDateTimePicker;
    cbDetailStatusDate: TevDBEdit;
    btnOk: TevBitBtn;
    btnCancel: TevBitBtn;
    mEditMode: TevMemo;
    DetailMagmedia: TevDBCheckBox;
    cbConsol: TevCheckBox;
    bMultiEnlist: TevBitBtn;
    TabSheet3: TTabSheet;
    Splitter2: TSplitter;
    Panel5: TPanel;
    BitBtn3: TevBitBtn;
    BitBtn2: TevBitBtn;
    BitBtn4: TevBitBtn;
    BitBtn1: TevBitBtn;
    wwDBGrid2: TevDBGrid;
    cbGrid2Freq: TevDBComboBox;
    cbGrid2Status: TevDBComboBox;
    cbGrid2ReturnType: TevDBComboBox;
    wwDBGrid1: TevDBGrid;
    cbGrid1Freq: TevDBComboBox;
    cbGrid1Status: TevDBComboBox;
    cbGrid1Type: TevDBComboBox;
    tsEe: TTabSheet;
    lEeClient: TevLabel;
    lEeCompany: TevLabel;
    lEeCons: TevLabel;
    lEeStatusDate: TLabel;
    evDBGrid1: TevDBGrid;
    bEeMarkAll: TevButton;
    bEeClearAll: TevButton;
    bEeMarkChanged: TevButton;
    TabSheet4: TTabSheet;
    Label20: TevLabel;
    evLabel3: TevLabel;
    cbSettingsPrintAgencyCopy: TevCheckBox;
    cbSettingsPrintClientCopy: TevCheckBox;
    cbSettingsPrintSbCopy: TevCheckBox;
    cbSettingsPrintOrder: TevRadioGroup;
    cbSettingsCredit: TevRadioGroup;
    cbSettingsMagmediaPath: TevEdit;
    cbSeparate: TevCheckBox;
    cbRefreshQueue: TevCheckBox;
    Memo1: TMemo;
    cbEeSort: TevRadioGroup;
    edCopies: TevSpinEdit;
    cbWagesFilter: TevRadioGroup;
    cbW2Sort: TevRadioGroup;
    cbHideSSN: TevCheckBox;
    tsSimple: TTabSheet;
    Panel1: TPanel;
    evLabel2: TevLabel;
    evLabel5: TevLabel;
    EvBevel2: TEvBevel;
    EvBevel4: TEvBevel;
    EvBevel3: TEvBevel;
    edQeCleanupLimit: TevEdit;
    edQeCleanupReports: TevCheckBox;
    edQePath: TevEdit;
    cbFilter: TevComboBox;
    bPreProcess: TevBitBtn;
    bProcess: TevBitBtn;
    bOneStepRePrint: TevBitBtn;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    gOneStepCoList: TevDBGrid;
    evPanel10: TevPanel;
    evLabel4: TevLabel;
    Panel4: TPanel;
    evDBGrid2: TevDBGrid;
    cbBriefStatus: TevDBComboBox;
    evPanel9: TevPanel;
    lBriefReturns: TevLabel;
    tsSimpleMagmedia: TTabSheet;
    evPanel6: TevPanel;
    evSplitter1: TevSplitter;
    evPanel1: TevPanel;
    gBriefMagmediaList: TevDBGrid;
    evPanel4: TevPanel;
    evLabel7: TevLabel;
    evPanel3: TevPanel;
    gBriefMagmediaCompanyList: TevDBGrid;
    evPanel5: TevPanel;
    evLabel8: TevLabel;
    evPanel7: TevPanel;
    cbMmFilter: TevComboBox;
    evButton1: TevBitBtn;
    TabSheet2: TTabSheet;
    wwDBGrid4: TevDBGrid;
    cbGrid4Freq: TevDBComboBox;
    cbGrid4Status: TevDBComboBox;
    cbGrid4ReturnType: TevDBComboBox;
    evPanel8: TevPanel;
    cbPreviewOn: TevCheckBox;
    btnProcess: TevBitBtn;
    btnReprint: TevBitBtn;
    TabSheet5: TTabSheet;
    cbMessages: TListBox;
    MessageLogSaveButton: TevButton;
    tsXMLAggregation: TTabSheet;
    pnlBottom: TevPanel;
    btnRunReport: TevBitBtn;
    DBGridXMLEnvelopeReports: TevDBGrid;
    dsCompanyFilter: TevDataSource;
    cdCompanyFilter: TevClientDataSet;
    tsht2CompFilterGroup: TevGroupBox;
    tsCompFilterGroupAll: TevRadioButton;
    tsCompFilterGroupSelect: TevRadioButton;
    cdSyReportsGroup: TevClientDataSet;
    cdQueueSY_REPORT_WRITER_REPORTS_NBR: TIntegerField;
    cdSelectedQueueSY_REPORT_WRITER_REPORTS_NB: TIntegerField;
    evPanel13: TevPanel;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    edCusClNbr: TevEdit;
    edClName: TevEdit;
    evLabel14: TevLabel;
    evLabel13: TevLabel;
    edCusCoNbr: TevEdit;
    edCoName: TevEdit;
    btnCompanyFilter: TevButton;
    grdCompFilter: TevDBGrid;
    cbEMail: TevCheckBox;
    eEMail: TevEdit;
    cbEmailSendRule: TevComboBox;
    cbPrinter: TevComboBox;
    Label9: TLabel;
    chbAutorefreshStatus: TevCheckBox;
    pnlTopMessagesTab: TPanel;
    SpeedButton1: TSpeedButton;
    cbDontPopupMessages: TevCheckBox;
    btnSend: TevSpeedButton;
    cdSyReports: TevClientDataSet;
    grdFieldOfficeReports: TevDBGrid;
    dsFieldOfficeReports: TevDataSource;
    cdFieldOfficeReports: TevClientDataSet;
    cdFieldOfficeReportsNAME: TStringField;
    cdFieldOfficeReportsSY_GL_AGENCY_REPORT_NBR: TIntegerField;
    cdFieldOfficeReportsSY_REPORTS_GROUP_NBR: TIntegerField;
    cdFieldOfficeReportsSY_GL_AGENCY_FIELD_OFFICE_NBR: TIntegerField;
    cbDetailReportAgency: TevDBLookupCombo;
    cbDetailReportFieldOffice: TevDBLookupCombo;
    cdFieldOfficeReportsSYSTEM_TAX_TYPE: TStringField;
    cdFieldOfficeReportsDEPOSIT_FREQUENCY: TStringField;
    cdFieldOfficeReportsRETURN_FREQUENCY: TStringField;
    cdFieldOfficeReportsdescription: TStringField;
    cdBriefMagmediaListSY_GL_AGENCY_REPORT_NBR: TIntegerField;
    cdBriefMagmediaCompanyListSY_GL_AGENCY_REPORT_NBR: TIntegerField;
    cdQueueAgencyName: TStringField;
    cdSelectedQueueAgencyName: TStringField;
    cdEeCURRENT_TERMINATION_CODE: TStringField;
    cbEEStatus: TevDBComboBox;
    cdEeW2: TStringField;
    bManualRecon: TevBitBtn;
    OpenDialog1: TOpenDialog;
    PopupMenu1: TPopupMenu;
    LoadCompaniesfromTextFile1: TMenuItem;
    tsMEF: TTabSheet;
    evPanel14: TevPanel;
    btnMefFile: TevBitBtn;
    evPanel15: TevPanel;
    dblookupMEF: TevDBLookupCombo;
    dsMEF: TevDataSource;                                           
    cdMEF: TevClientDataSet;
    cdLookupMEF: TevClientDataSet;
    dsLookupMEF: TevDataSource;
    cdMEFCo_Number: TStringField;
    cdMEFCO_Name: TStringField;
    cdMEFTax_Return: TStringField;
    cdMEFProcessDate: TDateTimeField;
    cdMEFLookUpName: TStringField;
    cdLookupMEFLookUpName: TStringField;
    cdMEFCO_TAX_RETURN_QUEUE_NBR: TIntegerField;
    cdMEFCL_NBR: TIntegerField;
    cdMEFREPORT_DESCRIPTION: TStringField;
    cdMEFCO_NBR: TIntegerField;
    grdMEF: TevDBCheckGrid;
    cdEeEE_NBR_LIST: TStringField;
    tsACAFilling: TTabSheet;
    evPanel16: TevPanel;
    dblookupACA: TevDBLookupCombo;
    evPanel17: TevPanel;
    btnACAFile: TevBitBtn;
    grdACA: TevDBCheckGrid;
    cdACA: TevClientDataSet;
    cdACACo_Number: TStringField;
    cdACACO_Name: TStringField;
    cdACATax_Return: TStringField;
    cdACAProcessDate: TDateTimeField;
    cdACALookUpName: TStringField;
    cdACACO_TAX_RETURN_QUEUE_NBR: TIntegerField;
    cdACACL_NBR: TIntegerField;
    cdACAREPORT_DESCRIPTION: TStringField;
    cdACACO_NBR: TIntegerField;
    dsACA: TevDataSource;
    cdLookupACA: TevClientDataSet;
    cdLookupACALookUpName: TStringField;
    dsLookupACA: TevDataSource;
    cbTestFile: TevCheckBox;
    cbReplacement: TevCheckBox;
    cbTransmissionReplacement: TevCheckBox;
    edOriginalReceiptId: TevEdit;
    procedure btnApplySelectClick(Sender: TObject);
    procedure cbDetailClCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure cbDetailReportAgencyCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure PcChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure cdDetailPeriodEndDateCloseUp(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
    procedure wwDBGrid2DblClick(Sender: TObject);
    procedure wwDBGrid2CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure wwDBGrid3CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure PcChange(Sender: TObject);
    procedure MessageLogSaveButtonClick(Sender: TObject);
    procedure cbMessagesDblClick(Sender: TObject);
    procedure wwDBGrid3DblClick(Sender: TObject);
    procedure btnEnlistClick(Sender: TObject);
    procedure cdQueueAfterScroll(DataSet: TDataSet);
    procedure cbMultiDeleteClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure wwDBGrid3CreateHintWindow(Sender: TObject;
      HintWindow: TwwGridHintWindow; AField: TField; R: TRect;
      var WordWrap: Boolean; var MaxWidth, MaxHeight: Integer;
      var DoDefault: Boolean);
    procedure dsQueueDataChange(Sender: TObject; Field: TField);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure dsQueueStateChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbDetailCoChange(Sender: TObject);
    procedure cdSelectedQueueAfterScroll(DataSet: TDataSet);
    procedure bEeMarkAllClick(Sender: TObject);
    procedure bEeMarkChangedClick(Sender: TObject);
    procedure bOneStepPreProcessClick(Sender: TObject);
    procedure cbSimpleModeClick(Sender: TObject);
    procedure bOneStepRePrintClick(Sender: TObject);
    procedure cbDateTillChange(Sender: TObject);
    procedure cbDateTillCloseUp(Sender: TObject);
    procedure gOneStepCoListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure bProcessClick(Sender: TObject);
    procedure gOneStepCoListDblClick(Sender: TObject);
    procedure cbDateIsPeriodEndDateClick(Sender: TObject);
    procedure cbDateIsDueDateClick(Sender: TObject);
    procedure edQeCleanupLimitChange(Sender: TObject);
    procedure bMultiEnlistClick(Sender: TObject);
    procedure cdDetailPeriodEndDateChange(Sender: TObject);
    procedure cbFreqChange(Sender: TObject);
    procedure cdDetailPeriodEndDateExit(Sender: TObject);
    procedure cdCoListCalcFields(DataSet: TDataSet);
    procedure cbTypeChange(Sender: TObject);
    procedure cbTaxServiceChange(Sender: TObject);
    procedure cbStatusChange(Sender: TObject);
    procedure cbDateFromChange(Sender: TObject);
    procedure cbFilterChange(Sender: TObject);
    procedure bPreProcessMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbPreviewOnClick(Sender: TObject);
    procedure btnProcessClick(Sender: TObject);
    procedure btnReprintClick(Sender: TObject);
    procedure cbQuartersChange(Sender: TObject);
    procedure gBriefMagmediaCompanyListCreateHintWindow(Sender: TObject;
      HintWindow: TwwGridHintWindow; AField: TField; R: TRect;
      var WordWrap: Boolean; var MaxWidth, MaxHeight: Integer;
      var DoDefault: Boolean);
    procedure gBriefMagmediaCompanyListCalcCellColors(Sender: TObject;
      Field: TField; State: TGridDrawState; Highlight: Boolean;
      AFont: TFont; ABrush: TBrush);
    procedure evButton1Click(Sender: TObject);
    procedure cdBriefMagmediaCompanyListCalcFields(DataSet: TDataSet);
    procedure cbMmFilterChange(Sender: TObject);
    procedure cbTypeNotClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure dsSummaryDataChange(Sender: TObject; Field: TField);
    procedure btnRunReportClick(Sender: TObject);
    procedure btnCompanyFilterClick(Sender: TObject);
    procedure tsCompFilterGroupSelectClick(Sender: TObject);
    procedure cbEMailClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure chbAutorefreshStatusClick(Sender: TObject);
    procedure cdFieldOfficeReportsCalcFields(DataSet: TDataSet);
    procedure cbDetailReportFieldOfficeCloseUp(Sender: TObject;
      LookupTable, FillTable: TDataSet; modified: Boolean);
    procedure bManualReconClick(Sender: TObject);
    procedure menuHistPopup(Sender: TObject);
    procedure LoadCompaniesfromTextFile1Click(Sender: TObject);
    procedure dblookupMEFChange(Sender: TObject);
    procedure btnMefFileClick(Sender: TObject);
    procedure evDBGrid1Exit(Sender: TObject);
    procedure dblookupACAChange(Sender: TObject);
    procedure btnACAFileClick(Sender: TObject);
    procedure cbTransmissionReplacementClick(Sender: TObject);
    procedure cbReplacementClick(Sender: TObject);
  private
    rwrBlankNumber : integer;

    sFilter: String;
    sCondition : String;
    FFlagList: TStringList;

    FForcePreprocess: Boolean;
    FReturnTypeCodeValues,
    FReturnStatusCodeValues,
    FTaxServiceCodeValues: string;
    FFlagTypeName: string;
    FCompanyMuliSelectScreen: TCompanyMuliSelectScreen;
    FParamsAreChanges: Boolean;
    FConditionList: TStringList;
    procedure SetMefFilter;
    procedure SetAcaFilter;
    procedure FillComboBox(const cb: TevComboBox; var CodeValues: string; const Constant: string);
    procedure FillOutsFilter;   
    procedure CalcDueDate;
    function PeriodEndDate(const GlAgencyReportNbr: Integer):TDateTime;
    function GenericCalcDueDate(const GlAgencyNbr: Integer; const GlAgencyReportNbr: Integer; const EndDate: TDateTime): TDateTime;
    function MakeHalfTone(const C: TColor): TColor;
    procedure GotoOutOfEditMode;
    procedure AddEe(const Consolidation: Boolean; const ClNbr, CoNbr: Integer; const PeriodEnd: TDateTime; const ChangedAfter: TDateTime = 0);
    procedure AddReport;
    function cdSelectedQueueStatusDate: TDateTime;
    function GetAffectedPersons(const CoNbr: Integer; const IsConsolidation: Boolean; const DateSince, QuarterEnd: TDateTime): Variant;
    procedure FilterEe;
    procedure RefreshBriefViewList;
    function cdCompanyName(const ds: TevClientDataSet): string;
    procedure CheckBeforeAction;
    procedure CheckCoFlag(const PERIOD_END_DATE: TDateTime);
    procedure PrintProcessed(const ds: TevClientDataSet; const PreView: Boolean);
    procedure CheckEnableDeleteFunction;
    procedure SetParamsAreChanges(const Value: Boolean);
    property ParamsAreChanges: Boolean read FParamsAreChanges write SetParamsAreChanges;
    procedure HistMenuClick(Sender: TObject);
    function CreateReportFileName(ReportName, Path: string): string;
    procedure InitXMLEnvelopeReportsDS;
    procedure InitLookupMEFDS;
    procedure InitLookupACADS;
    procedure RefreshFlagList;

    procedure AddMessage(aMsg: string);
    function GetReportNumber(cd: TevClientDataSet; ReportsGroupNbr: integer; PeriodEndDate : TDateTime): Integer;
    procedure SendErrorMessageAndScreen(const FMessage, FDetails, FDateTime: string; const IncludeScreen: Boolean);
    function  GetTaxReturnAudit(const ANbr: Integer): IevDataSet;
    function  GetClBlobData(const ANbr: Integer): IisStream;
    function  GetTaxReturnLastClBlobNbr(const ANbr: Integer): integer;
    procedure LoadComaniesXFilter(isFilterList: iIsStringList);
    procedure PopupLoadCompVisibility;

  protected
      procedure OnGetSecurityStates(const States: TSecurityStateList); override;
      procedure SetReadOnly(Value: Boolean); override;
  public
    { Public declarations }
    procedure Activate; override;
    procedure DeActivate; override;
    function CanClose: Boolean; override;
    procedure OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char); override;
  end;

var
  EDIT_TMP_CO_TAX_RETURN_QUEUE: TEDIT_TMP_CO_TAX_RETURN_QUEUE;

implementation

uses
  EvConsts, SFieldCodeValues,
  SReportSettings, SReturnQueueEnlistProcedures,
  SPD_EDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD,
  SBalanceInformation, SOutOfBalanceError, SPD_EDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG,
  SDM_MISC_LIAB_CORRECT, SSecurityInterface, SPD_EDIT_MANUAL_RECONCILED,
  DateUtils, JPeg;

const
  strSeparateReturnsWithBlankSheet = 'SeparateReturnsWithBlankSheet';
  strAutoRefreshQueue = 'AutoRefreshQueue';
  mOutOfBalance = 'Out of balance';

{$R *.DFM}

{ TEDIT_TMP_CO_TAX_RETURN_QUEUE }

const
  TypeName = 'ReturnPrintQueue';
  ctEnabledReprintOnly = 'P';

type
  THistoryMenuItem = class(TMenuItem)
  public
    CL_BLOB_NBR: Variant;
    ChangeDate: TDateTime;
  end;

function GetTag(const ds: TISBasicClientDataSet): string;
begin
  Result := IntToStr(ds['cl_nbr'])+';'+IntToStr(ds['CO_TAX_RETURN_QUEUE_NBR'])+';'+IntToStr(ds['co_nbr']);
end;

function TEDIT_TMP_CO_TAX_RETURN_QUEUE.GetAffectedPersons(const CoNbr: Integer; const IsConsolidation: Boolean; const DateSince, QuarterEnd: TDateTime): Variant;
begin
  with TExecDSWrapper.Create('AffectedPersonsTemplate') do
  begin
    if IsConsolidation then
      SetMacro('INSTATEMENT', 'select CO.CO_NBR from CO ' +
        'join CL_CO_CONSOLIDATION N on CO.CL_CO_CONSOLIDATION_NBR=N.CL_CO_CONSOLIDATION_NBR ' +
        'where N.PRIMARY_CO_NBR=:CoNbr and N.CONSOLIDATION_TYPE in (''' + CONSOLIDATION_TYPE_ALL_REPORTS +
        ''',''' + CONSOLIDATION_TYPE_FED + ''') ' +
       ''' and {AsOfNow<CO>} and {AsOfNow<N>}')
    else
      SetMacro('INSTATEMENT', ':CoNbr');
    SetParam('ProcessDate', DateSince);
    SetParam('CheckDate', QuarterEnd);
    SetParam('CoNbr', CoNbr);
    if ctx_DataAccess.CUSTOM_VIEW.Active then
      ctx_DataAccess.CUSTOM_VIEW.Close;
    ctx_DataAccess.CUSTOM_VIEW.DataRequest(AsVariant);
    ctx_DataAccess.CUSTOM_VIEW.Open;
    if ctx_DataAccess.CUSTOM_VIEW.RecordCount = 0 then
      Result := Null
    else
    begin
      Result := VarArrayCreate([ctx_DataAccess.CUSTOM_VIEW.RecordCount], varInteger);
      ctx_DataAccess.CUSTOM_VIEW.First;
      while not ctx_DataAccess.CUSTOM_VIEW.EOF do
      begin
        Result[ctx_DataAccess.CUSTOM_VIEW.RecNo - 1] := ctx_DataAccess.CUSTOM_VIEW.FieldByName('CL_PERSON_NBR').AsInteger;
        ctx_DataAccess.CUSTOM_VIEW.Next;
      end;
    end;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.FillComboBox(const cb: TevComboBox;
  var CodeValues: string; const Constant: string);
var
  i: Integer;
  s1, s2: string;
begin
  i := 1;
  cb.Items.Clear;
  cb.Items.Append('- Any -');
  CodeValues := '';
  while not GetToken(Constant, i, s1) do
  begin
    GetToken(Constant, i, s2);
    cb.Items.Append(s1);
    CodeValues := CodeValues + s2;
  end;
  cb.ItemIndex := 0;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.Activate;
  procedure ActivateIndex(const d: TevClientDataset; const FName: string);
  begin
    Assert(d.FindField(FName) <> nil);
    d.AddIndex('NBR', FName, [ixUnique]);
    d.IndexDefs.Update;
    d.IndexName := 'NBR';
  end;
var
  d: TDateTime;
  s : String;
  Q : IevQuery;

begin
  inherited;

  cbPrinter.Items.Assign(Printer.Printers);
  cbPrinter.Sorted := True;
  cbPrinter.ItemIndex := cbPrinter.Items.IndexOf(mb_AppSettings['Settings\ReportsPrinter']);

  FFlagList := TStringList.Create;
  FFlagList.Sorted := True;

  cdHeldList.FieldDefs.Add('TypeName', ftString, 40, True);
  cdHeldList.FieldDefs.Add('Tag', ftString, 50, False);
  cdHeldList.FieldDefs.Add('UserName', ftString, 20, True);
  cdHeldList.CreateDataSet;

  ctx_RWLocalEngine.BeginWork;

  tsCompFilterGroupSelectClick(nil);  

  FConditionList := TStringList.Create;
  FConditionList.Duplicates := dupIgnore;
  FConditionList.Sorted := True;

  InitXMLEnvelopeReportsDS;
  InitLookupMEFDS;
  InitLookupACADS;

  FFlagTypeName := '';

  cbSeparate.Checked := mb_AppSettings.GetValue(strSeparateReturnsWithBlankSheet, True);
  cbRefreshQueue.Checked := mb_AppSettings.GetValue(strAutoRefreshQueue, True);
  edQePath.Text := mb_AppSettings.GetValue('QuaterEndCleanupReportPath', 'c:\');
  edQeCleanupReports.Checked := mb_AppSettings['QuaterEndCleanupReports'] = 'Y';

  cdAnswers.CreateDataSet;
  cbFilter.ItemIndex := 0;
  cbMmFilter.ItemIndex := 0;
  cbDateIsPeriodEndDate.Checked := True;

  eEMail.Text := Context.UserAccount.EMail;;
  cbEmailSendRule.ItemIndex :=0;

  ctx_StartWait('Loading data...');
  try
     rwrBlankNumber := 0;

     s:= 'select a.SY_REPORT_WRITER_REPORTS_NBR from SY_REPORT_WRITER_REPORTS a ' +
         'where {AsOfNow<a>} and a.REPORT_DESCRIPTION like ''Blank%''';
     Q := TevQuery.Create(s);
     Q.Execute;
     if Q.Result.Fields[0].AsInteger > 0 then
       rwrBlankNumber := Q.Result.Fields[0].AsInteger;

    DM_SERVICE_BUREAU.SB.DataRequired('ALL');

    with TExecDSWrapper.Create('Consolidations') do
    begin
      ctx_DataAccess.GetTmpCustomData(cdConsolidation, AsVariant);
    end;

    cdConsolidation.AddIndex('NBR', 'CL_NBR;PRIMARY_CO_NBR', []);
    cdConsolidation.IndexDefs.Update;
    cdConsolidation.IndexName := 'NBR';

    ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
    ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(TExecDSWrapper.Create('SelectClientsCompanies').AsVariant);
    ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;

    cdClCoInfo.Data := ctx_DataAccess.TEMP_CUSTOM_VIEW.Data;
    cdClCoInfo.AddIndex('NBR', 'CL_NBR;CO_NBR', [ixUnique]);
    cdClCoInfo.IndexDefs.Update;
    cdClCoInfo.IndexName := 'NBR';

    with TExecDSWrapper.Create('ConsolidationsFull') do
    begin
      ctx_DataAccess.GetTmpCustomData(cdCoList, AsVariant);
    end;


    ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;

    tsEe.TabVisible := False;
    Pc.ActivePage := TabSheet1;
    cbGrid2Status.Items.Text := TaxReturnStatus_ComboChoices;
    cbBriefStatus.Items.Text := TaxReturnStatus_ComboChoices;
    cbGrid2ReturnType.Items.Text := TaxReturnType_ComboChoices;
    cbGrid2Freq.Items.Text := ReturnFrequencyType_ComboChoices;
    cbGrid3Status.Items.Text := TaxReturnStatus_ComboChoices;
    cbGrid3ReturnType.Items.Text := TaxReturnType_ComboChoices;
    cbGrid3Freq.Items.Text := ReturnFrequencyType_ComboChoices;
    cbGrid4Status.Items.Text := TaxReturnStatus_ComboChoices;
    cbGrid4ReturnType.Items.Text := TaxReturnType_ComboChoices;
    cbGrid4Freq.Items.Text := ReturnFrequencyType_ComboChoices;
    cbGrid1Status.Items.Text := TaxReturnStatus_ComboChoices;
    cbGrid1Type.Items.Text := TaxReturnType_ComboChoices;
    cbGrid1Freq.Items.Text := ReturnFrequencyType_ComboChoices;
    cbDetailStatus.Items.Text := TaxReturnStatus_ComboChoices;

    cdEe.CreateDataSet;
    cdEe.LogChanges := False;
    cdSelectedQueue.CreateDataSet;
    cdSelectedQueue.LogChanges := False;
    FillComboBox(cbType, FReturnTypeCodeValues, TaxReturnType_ComboChoices);

    cbQuarterly.Checked := True;
    FillComboBox(cbTaxService, FTaxServiceCodeValues, Tax_Service_ComboChoices);
    FillComboBox(cbStatus, FReturnStatusCodeValues, TaxReturnStatus_ComboChoices);

    cbEEStatus.Items.Text := EE_TerminationCode_ComboChoices;

    DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.DataRequired('ALL');
    DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.DataRequired('ALL');
    DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.DataRequired('ALL');


    DM_TEMPORARY.TMP_CL.DataRequired('ALL');

    cbDetailCl.LookupTable := TISBasicClientDataSet.Create(Self);
    TISBasicClientDataSet(cbDetailCl.LookupTable).Data := DM_TEMPORARY.TMP_CL.Data;
    TISBasicClientDataSet(cbDetailCl.LookupTable).IndexFieldNames := 'CUSTOM_CLIENT_NUMBER;NAME';

    cbDetailCo.LookupTable := CreateLookupDataset(cdCoList, Self);
    TISBasicClientDataSet(cbDetailCo.LookupTable).IndexFieldNames := 'CL_NBR;CUSTOM_COMPANY_NUMBER;company_NAME';
    TISBasicClientDataSet(cbDetailCo.LookupTable).MasterFields := 'CL_NBR';
    TISBasicClientDataSet(cbDetailCo.LookupTable).MasterSource := TDataSource.Create(Self);
    TISBasicClientDataSet(cbDetailCo.LookupTable).MasterSource.DataSet := cbDetailCl.LookupTable;


    cbDetailReportAgency.LookupTable := TISBasicClientDataSet.Create(Self);
    TISBasicClientDataSet(cbDetailReportAgency.LookupTable).Data := DM_SYSTEM_MISC.SY_GLOBAL_AGENCY.Data;
    TISBasicClientDataSet(cbDetailReportAgency.LookupTable).IndexFieldNames := 'AGENCY_NAME';

    cbDetailReportFieldOffice.LookupTable := TISBasicClientDataSet.Create(Self);
    TISBasicClientDataSet(cbDetailReportFieldOffice.LookupTable).Data := DM_SYSTEM_MISC.SY_GL_AGENCY_FIELD_OFFICE.Data;
    TISBasicClientDataSet(cbDetailReportFieldOffice.LookupTable).IndexFieldNames := 'SY_GLOBAL_AGENCY_NBR;ADDITIONAL_NAME';
    TISBasicClientDataSet(cbDetailReportFieldOffice.LookupTable).MasterFields := 'SY_GLOBAL_AGENCY_NBR';
    TISBasicClientDataSet(cbDetailReportFieldOffice.LookupTable).MasterSource := TDataSource.Create(Self);
    TISBasicClientDataSet(cbDetailReportFieldOffice.LookupTable).MasterSource.DataSet := cbDetailReportAgency.LookupTable;


    s := 'select a.SY_GL_AGENCY_FIELD_OFFICE_NBR, a.SY_GL_AGENCY_REPORT_NBR, a.SYSTEM_TAX_TYPE, ' +
         'a.DEPOSIT_FREQUENCY, a.RETURN_FREQUENCY, b.NAME, b.SY_REPORTS_GROUP_NBR ' +
         'from SY_GL_AGENCY_REPORT a, SY_REPORTS_GROUP b ' +
         'where {AsOfNow<a>} and {AsOfNow<b>} and ' +
         'a.SY_REPORTS_GROUP_NBR = b.SY_REPORTS_GROUP_NBR ';
    if not cdFieldOfficeReports.Active then
    begin
       with TExecDSWrapper.Create(s) do
       begin
           ctx_DataAccess.GetSyCustomData(cdFieldOfficeReports, AsVariant);
       end;
    end;


    gBriefMagmediaList.DataSource := TDataSource.Create(Self);
    gBriefMagmediaList.DataSource.DataSet := cdBriefMagmediaList;
    gBriefMagmediaCompanyList.DataSource := TDataSource.Create(Self);
    gBriefMagmediaCompanyList.DataSource.DataSet := cdBriefMagmediaCompanyList;
    cdBriefMagmediaCompanyList.MasterSource := gBriefMagmediaList.DataSource;
    cdBriefMagmediaCompanyList.MasterFields := 'SY_REPORTS_GROUP_NBR;SY_GL_AGENCY_REPORT_NBR';


    cbDateFrom.DateTime := GetBeginQuarter(GetBeginMonth(GetBeginMonth(Date)-1)-1);
    cbDateTill.DateTime := GetEndQuarter(cbDateFrom.DateTime);
    if GetEndYear(cbDateTill.DateTime) = cbDateTill.DateTime then
      cbAnnually.Checked := True;
    d := GetBeginMonth(GetBeginMonth(Date)-1)-1;
    with cbQuarters.Items do
    begin
      Clear;
      Add('1st quarter '+ IntToStr(GetYear(d)-1));
      Add('2nd quarter '+ IntToStr(GetYear(d)-1));
      Add('3rd quarter '+ IntToStr(GetYear(d)-1));
      Add('4th quarter '+ IntToStr(GetYear(d)-1));
      Add('1st quarter '+ IntToStr(GetYear(d)));
      Add('2nd quarter '+ IntToStr(GetYear(d)));
      Add('3rd quarter '+ IntToStr(GetYear(d)));
      Add('4th quarter '+ IntToStr(GetYear(d)));
      Add('1st quarter '+ IntToStr(GetYear(d)+1));
      Add('2nd quarter '+ IntToStr(GetYear(d)+1));
      Add('3rd quarter '+ IntToStr(GetYear(d)+1));
      Add('4th quarter '+ IntToStr(GetYear(d)+1));
    end;
    cbQuarters.ItemIndex := GetQuarterNumber(d)+ 4- 1;
    cbQuarters.OnChange(nil);
    cbType.OnChange(nil);


    PC.Visible := False;
    cbSimpleMode.OnClick(nil);

    chbAutorefreshStatus.Checked := mb_AppSettings.AsInteger['TRQAutorefreshLocks'] <> 0;
    chbAutorefreshStatus.OnClick(nil);
  finally
    ctx_EndWait;
  end;
end;


Function TEDIT_TMP_CO_TAX_RETURN_QUEUE.GetReportNumber(cd: TevClientDataSet; ReportsGroupNbr: integer; PeriodEndDate : TDateTime): Integer;
begin
   result := 0;
   cd.FindNearest([ReportsGroupNbr]);
   while (cd['SY_REPORTS_GROUP_NBR'] = ReportsGroupNbr) and
         (not cd.Eof) do
   begin
     if (VarIsNull(cd['ACTIVE_YEAR_FROM']) and VarIsNull(cd['ACTIVE_YEAR_TO'])) or
        ((cd['ACTIVE_YEAR_FROM'] <= PeriodEndDate) and
         (VarIsNull(cd['ACTIVE_YEAR_TO']))) or
        ((cd['ACTIVE_YEAR_FROM'] <= PeriodEndDate) and
         (cd['ACTIVE_YEAR_TO'] >= PeriodEndDate)) then
      begin
       Result := cd['SY_REPORTS_NBR'];
       break;
      end;
      cd.Next;
   end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnApplySelectClick(
  Sender: TObject);
var
  cdPreSummary: TISBasicClientDataSet;

  procedure GetConsolidationDescr(const Cl_Nbr, Co_Nbr, SY_GL_AGENCY_REPORT_NBR: Integer; const Date_: TDateTime; var Descr, Descr2, Status: string; const PRODUCE_ASCII_FILE : string);
  begin
    if not cdClCoInfo.FindKey([Cl_Nbr, Co_Nbr]) then
      Exit; // Shoudn't happend.
    if cdPreSummary.FindKey([Cl_Nbr, Co_Nbr, SY_GL_AGENCY_REPORT_NBR, Date_]) then
      Descr := 'Main: '+ cdClCoInfo['CUSTOM_COMPANY_NUMBER']+ ' '+ cdClCoInfo['COMPANY_NAME']+ ' - On hold'#13
    else
      Descr := 'Main: '+ cdClCoInfo['CUSTOM_COMPANY_NUMBER']+ ' '+ cdClCoInfo['COMPANY_NAME']+ #13;
    Descr2 := 'Main: '+ cdClCoInfo['CUSTOM_COMPANY_NUMBER']+ ' '+ cdClCoInfo['COMPANY_NAME']+ #13;
    if cdConsolidation.FindKey([Cl_Nbr, Co_Nbr]) then
    try
      Descr := ReturnDescription(Co_Consolidation_Type_ComboChoices, cdConsolidation['CONSOLIDATION_TYPE'])+ #13+ Descr;
      Descr2 := ReturnDescription(Co_Consolidation_Type_ComboChoices, cdConsolidation['CONSOLIDATION_TYPE'])+ #13+ Descr2;
      while (cdConsolidation['PRIMARY_CO_NBR'] = Co_Nbr) and (cdConsolidation['CL_NBR'] = Cl_Nbr) and not cdConsolidation.Eof do
      begin
        if (cdConsolidation['PRIMARY_CO_NBR'] <> cdConsolidation['CO_NBR'])
        and cdClCoInfo.FindKey([cdConsolidation['CL_NBR'], cdConsolidation['CO_NBR']]) then
        begin
          if (cdPreSummary.FindKey([Cl_Nbr, cdConsolidation['CO_NBR'], SY_GL_AGENCY_REPORT_NBR, Date_])) and
           ( PRODUCE_ASCII_FILE = GROUP_BOX_NO ) then
          begin
            Status := GROUP_BOX_YES;
            Descr := Descr+ ' '+ cdClCoInfo['CUSTOM_COMPANY_NUMBER']+ ' '+ cdClCoInfo['COMPANY_NAME']+ ' - On hold'#13;
          end
          else
            Descr := Descr+ ' '+ cdClCoInfo['CUSTOM_COMPANY_NUMBER']+ ' '+ cdClCoInfo['COMPANY_NAME']+ #13;
          Descr2 := Descr2+ ' '+ cdClCoInfo['CUSTOM_COMPANY_NUMBER']+ ' '+ cdClCoInfo['COMPANY_NAME']+ #13;
        end;
        cdConsolidation.Next;
      end;
    finally
      cdConsolidation.Filtered := False;
    end;
  end;

  procedure MakeAllButSelectReadOnly(aDataSet: TevClientDataSet; IsReadOnly: boolean);
  var
    I: integer;
  begin
    with aDataSet do
      for I := 0 to FieldCount - 1 do
        with Fields[I] do
          if not (FieldName = 'Select') then
            ReadOnly := IsReadOnly;
  end;

var
  sp: string;
  function Position(const i: Integer; const def: Char = ' '): Char;
  begin
    if Length(sp) >= i then
      Result := sp[i]
    else
      Result := def;
  end;
var
  s: string;
  d: TDateTime;
  b: TBookMarkStr;
  sConsol, sConsol2: string;
  sStatus: string;
  cdQueueAfterScrollStorage: TDataSetNotifyEvent;
  lCurrentUserName: string;
  vLocateValues: Variant;
  LockedFlagList: IisList;
  FlagInfo: IevGlobalFlagInfo;
  i: Integer;
  pREPORT_TYPE : String;
  QMef, QAca: IevQuery;

begin
  inherited;
  ParamsAreChanges := False;
  lCurrentUserName := UpperCase(Context.UserAccount.User);
  ctx_StartWait('Updating queue...');

  try
    LockedFlagList := mb_GlobalFlagsManager.GetLockedFlagList(TypeName);
    cdHeldList.EmptyDataSet;

    for i := 0 to LockedFlagList.Count - 1 do
    begin
      FlagInfo := LockedFlagList[i] as IevGlobalFlagInfo;
      cdHeldList.Append;
      cdHeldList['TypeName'] := FlagInfo.FlagType;
      cdHeldList['Tag'] := FlagInfo.Tag;
      cdHeldList['UserName'] := UpperCase(FlagInfo.UserName);
      cdHeldList.Post;
    end;

    RefreshFlagList;

    wwDBGrid3.UnselectAll;
    wwDBGrid1.UnselectAll;
    cdQueueAfterScrollStorage := cdQueue.AfterScroll;
    cdSelectedQueue.DisableControls;
    dsQueue.DataSet := nil;
    if cdSelectedQueue.RecordCount > 0 then
      b := cdSelectedQueue.Bookmark;
    cdSelectedQueue.EmptyDataSet;

    FillOutsFilter;       
    PCMain.ActivatePage(tsTaxReturn);

    if cbSimpleMode.Checked then tsDetail.TabVisible := False;

    try

     s := 'select t1.SY_REPORTS_GROUP_NBR, t1.SY_GL_AGENCY_REPORT_NBR, '+
      't1.AGENCY_COPY, t1.SB_COPY, t1.CL_COPY, '+
      't1.NAME DESCRIPTION, t2.ACTIVE_REPORT, t2.SY_REPORTS_NBR, '+
      't3.SY_REPORT_WRITER_REPORTS_NBR, '+
      't4.ADDITIONAL_NAME, '+
      'T6.AGENCY_NAME, '+
      'T4.SY_GL_AGENCY_FIELD_OFFICE_NBR, '+
      't3.REPORT_TYPE, t4.SY_GLOBAL_AGENCY_NBR, t5.RETURN_FREQUENCY, t5.SYSTEM_TAX_TYPE ' +
      'from SY_GL_AGENCY_REPORT T5, SY_GL_AGENCY_FIELD_OFFICE T4, SY_REPORT_WRITER_REPORTS T3, '+
      'SY_REPORTS T2, SY_REPORTS_GROUP T1, SY_GLOBAL_AGENCY T6 ' +
      'where T1.SY_REPORTS_NBR = T2.SY_REPORTS_NBR and '+
      'T5.SY_GL_AGENCY_REPORT_NBR = T1.SY_GL_AGENCY_REPORT_NBR and '+
      'T4.SY_GL_AGENCY_FIELD_OFFICE_NBR = T5.SY_GL_AGENCY_FIELD_OFFICE_NBR and '+
      'T3.SY_REPORT_WRITER_REPORTS_NBR = T2.SY_REPORT_WRITER_REPORTS_NBR and '+
      'T6.SY_GLOBAL_AGENCY_NBR = t4.SY_GLOBAL_AGENCY_NBR AND ' +
      '{AsOfNow<T1>} and {AsOfNow<T2>} and {AsOfNow<T3>} and ' +
      '{AsOfNow<T4>} and {AsOfNow<T6>} ' +
      'and {AsOfNow<T5>} ' +
      'and T1.SY_REPORTS_GROUP_NBR <= 20000 ' +
      ' union ' +
      'select t1.SY_REPORTS_GROUP_NBR, t5.SY_GL_AGENCY_REPORT_NBR, t1.AGENCY_COPY, t1.SB_COPY, t1.CL_COPY, ' +
       't1.NAME  DESCRIPTION,null ACTIVE_REPORT,null SY_REPORTS_NBR,  null SY_REPORT_WRITER_REPORTS_NBR, ' +
       't4.ADDITIONAL_NAME, T6.AGENCY_NAME, T4.SY_GL_AGENCY_FIELD_OFFICE_NBR, ' +
       'null REPORT_TYPE, t4.SY_GLOBAL_AGENCY_NBR, t5.RETURN_FREQUENCY, t5.SYSTEM_TAX_TYPE ' +
       'from SY_GL_AGENCY_REPORT T5, SY_GL_AGENCY_FIELD_OFFICE T4, SY_REPORTS_GROUP T1, SY_GLOBAL_AGENCY T6 ' +
       ' where ' +
        'T1.SY_REPORTS_GROUP_NBR > 20000 and ' +
        'T5.SY_REPORTS_GROUP_NBR = T1.SY_REPORTS_GROUP_NBR and ' +
        'T4.SY_GL_AGENCY_FIELD_OFFICE_NBR = T5.SY_GL_AGENCY_FIELD_OFFICE_NBR and ' +
        'T6.SY_GLOBAL_AGENCY_NBR = t4.SY_GLOBAL_AGENCY_NBR and ' +
        '{AsOfNow<T1>} and {AsOfNow<T4>} and ' +
        '{AsOfNow<T6>} and {AsOfNow<T5>} ';


     if not cdSyReportsGroup.Active then
     begin
        with TExecDSWrapper.Create(s) do
        begin
          ctx_DataAccess.GetSyCustomData(cdSyReportsGroup, AsVariant);
        end;
        cdSyReportsGroup.AddIndex('NBR2', 'SY_REPORTS_GROUP_NBR', []);
        cdSyReportsGroup.IndexDefs.Update;

        cdSyReportsGroup.AddIndex('NBR', 'SY_GL_AGENCY_REPORT_NBR', []);
        cdSyReportsGroup.IndexDefs.Update;
        cdSyReportsGroup.IndexName := 'NBR';
     end;
     s :=
     'select t2.DESCRIPTION, t2.ACTIVE_YEAR_FROM, t2.ACTIVE_YEAR_TO, t2.ACTIVE_REPORT, t2.SY_REPORTS_NBR, ' +
      't3.SY_REPORT_WRITER_REPORTS_NBR, t3.REPORT_TYPE, t2.SY_REPORTS_GROUP_NBR ' +
       'from SY_REPORT_WRITER_REPORTS T3, SY_REPORTS T2 ' +
        'where T3.SY_REPORT_WRITER_REPORTS_NBR = T2.SY_REPORT_WRITER_REPORTS_NBR and ' +
         '{AsOfNow<T2>} and {AsOfNow<T3>} and ' +
         '((t2.ACTIVE_YEAR_FROM is null and t2.ACTIVE_YEAR_TO is null) or ' +
         '(t2.ACTIVE_YEAR_FROM <= :ENDDATE and t2.ACTIVE_YEAR_TO is null) or ' +
         '(:ENDDATE >= t2.ACTIVE_YEAR_FROM and :BEGDATE <= t2.ACTIVE_YEAR_TO ) or ' +
         '(t2.ACTIVE_YEAR_FROM >= :BEGDATE) )';

      if cdSyReports.Active then
         cdSyReports.Close;
      with TExecDSWrapper.Create(s) do
      begin
         d := Trunc(cbDateFrom.DateTime);
         SetParam('BEGDATE', d);
         d := Trunc(cbDateTill.DateTime);
         SetParam('ENDDATE', d);
         ctx_DataAccess.GetSyCustomData(cdSyReports, AsVariant);
      end;
      Assert(cdSyReports.FindField('SY_REPORTS_GROUP_NBR') <> nil);
      if cdSyReports.IndexDefs.IndexOf('NBR') < 0 then
      begin
        cdSyReports.AddIndex('NBR', 'SY_REPORTS_GROUP_NBR;ACTIVE_YEAR_FROM', []);
        cdSyReports.IndexDefs.Update;
        cdSyReports.IndexName := 'NBR';
      end;

      cdQueueAfterScrollStorage := cdQueue.AfterScroll;
      cdQueue.AfterScroll := nil;

      with TExecDSWrapper.Create('ConsolidationsPreProcessStatus') do
      begin
        ctx_DataAccess.GetTmpCustomData(cdStatuses, AsVariant);
      end;
      with TExecDSWrapper.Create('ReturnsQueue') do
      begin
        s := '';
        if cbTaxService.ItemIndex <> 0 then
        begin
          s := ' and TAX_SERVICE = :TAX_SERVICE';
          SetParam('TAX_SERVICE', FTaxServiceCodeValues[cbTaxService.ItemIndex]);
        end;

        if Length(Trim(sFilter))> 0 then
               s := s + sFilter
          else if Length(Trim(sCondition))> 0 then
               s := s + sCondition;

        s := s + GetReadOnlyClintCompanyFilter(True,'t1.','t2.');

        SetMacro('COND', s);

        if cbDateIsDueDate.Checked then
          SetMacro('Date', 'DUE_DATE')
        else
        if cbDateIsPeriodEndDate.Checked then
          SetMacro('Date', 'PERIOD_END_DATE')
        else
          Assert(False);
        d := Trunc(cbDateFrom.DateTime);
        SetParam('BEGDATE', d);
        d := Trunc(cbDateTill.DateTime);
        SetParam('ENDDATE', d);
        cdQueue.Close;
        ctx_DataAccess.GetTmpCustomData(cdQueue, AsVariant);

        cdQueue.LogChanges := False;

      end;
      if cdSummary.Active then
      begin
        vLocateValues := cdSummary['CL_NBR;CO_NBR;CONSOLIDATION'];
        vLocateValues[2] := ConvertNull(vLocateValues[2], '');
      end
      else
        vLocateValues := Null;

      QMef := TevQuery.Create('select  b.SY_REPORT_WRITER_REPORTS_NBR, a.NAME, b.REPORT_DESCRIPTION from  SY_REPORT_GROUPS a, SY_REPORT_WRITER_REPORTS b, SY_REPORT_GROUP_MEMBERS c ' +
                                'where UPPERCASE(a.NAME) like ''%MEF'' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                                'a.SY_REPORT_GROUPS_NBR = c.SY_REPORT_GROUPS_NBR and ' +
                                'c.SY_REPORT_WRITER_REPORTS_NBR = b.SY_REPORT_WRITER_REPORTS_NBR ');
      QMef.Execute;
      QMef.Result.IndexFieldNames := 'SY_REPORT_WRITER_REPORTS_NBR';

      cdMEF.Close;
      cdMEF.RetrieveCondition := '';
      cdMEF.CreateDataSet;
      MakeAllButSelectReadOnly(cdMEF,false);
      cdMEF.LogChanges := False;
      cdMEF.DisableControls;

      QAca := TevQuery.Create('select  b.SY_REPORT_WRITER_REPORTS_NBR, a.NAME, b.REPORT_DESCRIPTION from  SY_REPORT_GROUPS a, SY_REPORT_WRITER_REPORTS b, SY_REPORT_GROUP_MEMBERS c ' +
                                'where a.NAME like ''%Tax Return-1095%'' and {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                                'a.SY_REPORT_GROUPS_NBR = c.SY_REPORT_GROUPS_NBR and b.REPORT_TYPE = ''X'' and ' +
                                'c.SY_REPORT_WRITER_REPORTS_NBR = b.SY_REPORT_WRITER_REPORTS_NBR ');
      QAca.Execute;
      QAca.Result.IndexFieldNames := 'SY_REPORT_WRITER_REPORTS_NBR';

      cdACA.Close;
      cdACA.RetrieveCondition := '';
      cdACA.CreateDataSet;
      MakeAllButSelectReadOnly(cdACA,false);
      cdACA.LogChanges := False;
      cdACA.DisableControls;

      cdSummary.Close;
      cdSummary.RetrieveCondition := '';
      cdSummary.CreateDataSet;
      cdSummary.LogChanges := False;
      cdSummary.DisableControls;
      cdQueue.RetrieveCondition := '';
      TDataSet(cdQueue).Filtered := False;
      s := cdQueue.IndexName;
      cdQueue.IndexName := '';
      cdPreSummary := TISBasicClientDataSet.Create(nil);
      try
        cdPreSummary.FieldDefs.Add('CL_NBR', ftInteger);
        cdPreSummary.FieldDefs.Add('CO_NBR', ftInteger);
        cdPreSummary.FieldDefs.Add('SY_GL_AGENCY_REPORT_NBR', ftInteger);
        cdPreSummary.FieldDefs.Add('PERIOD_END_DATE', ftDateTime);
        cdPreSummary.CreateDataSet;
        cdPreSummary.LogChanges := False;
        cdPreSummary.AddIndex('NBR', 'CL_NBR;CO_NBR;SY_GL_AGENCY_REPORT_NBR;PERIOD_END_DATE', [ixUnique]);
        cdPreSummary.IndexDefs.Update;
        cdPreSummary.IndexName := 'NBR';
        cdQueue.First;
        while not cdQueue.Eof do
        begin
          if cdQueue['SY_REPORTS_GROUP_NBR'] <> 0 then
          begin
            if (cdSyReportsGroup.IndexName <> 'NBR2' ) then
              cdSyReportsGroup.IndexName := 'NBR2'
          end
          else
           if (cdSyReportsGroup.IndexName <> 'NBR' ) then
              cdSyReportsGroup.IndexName := 'NBR';

          if (((cdQueue['SY_REPORTS_GROUP_NBR'] <> 0)   and
             cdSyReportsGroup.FindKey([cdQueue['SY_REPORTS_GROUP_NBR']]))
             or
              ((cdQueue['SY_REPORTS_GROUP_NBR'] = 0) and
             cdSyReportsGroup.FindKey([cdQueue['SY_GL_AGENCY_REPORT_NBR']])))
             then
          begin
            if (not (cbAnnually.Checked and (cdSyReportsGroup['RETURN_FREQUENCY'] = FREQUENCY_TYPE_ANNUAL))
            and not (cbQuarterly.Checked and (cdSyReportsGroup['RETURN_FREQUENCY'] = FREQUENCY_TYPE_QUARTERLY))
            and not (cbMonthly.Checked and (cdSyReportsGroup['RETURN_FREQUENCY'] = FREQUENCY_TYPE_MONTHLY))
            and not (cbSemiMonthly.Checked and (cdSyReportsGroup['RETURN_FREQUENCY'] = FREQUENCY_TYPE_SEMI_MONTHLY))) then
            begin
              cdQueue.Delete;
              Continue;
            end;
            case cbType.ItemIndex of
            0: ;
            3: if ((cdSyReportsGroup['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_EE_EeCopy_1095B) and
                   (cdSyReportsGroup['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_EE_EeCopy_1095C) and
                   (cdSyReportsGroup['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE_EeCopy) and
                   (cdSyReportsGroup['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE_EeCopy_W2)
              and (cdSyReportsGroup['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE)) xor cbTypeNot.Checked then
              begin
              cdQueue.Delete;
              Continue;
              end;
            else if ((cdSyReportsGroup['SYSTEM_TAX_TYPE'] <> FReturnTypeCodeValues[cbType.ItemIndex]) xor cbTypeNot.Checked) then
              begin
                cdQueue.Delete;
                Continue;
              end;
            end;

          if cdQueue['SY_REPORTS_GROUP_NBR'] = 0 then
          begin
           if GetReportNumber(cdSyReports, cdSyReportsGroup['SY_REPORTS_GROUP_NBR'], cdQueuePERIOD_END_DATE.AsDateTime) = 0 then
           begin
             cdQueue.Delete;
             Continue;
           end;
           pREPORT_TYPE := string(cdSyReports['REPORT_TYPE'])[1];
          end
          else
           pREPORT_TYPE := string(cdSyReportsGroup['REPORT_TYPE'])[1];

            if not (pREPORT_TYPE[1] in [SYSTEM_REPORT_TYPE_TAX_RETURN, SYSTEM_REPORT_TYPE_ASCII, SYSTEM_REPORT_TYPE_TAX_RETURN_MULTICLIENT, SYSTEM_REPORT_TYPE_TAX_RETURN_XML]) then
            begin
               cdQueue.Delete;
               Continue;
            end;

            cdQueue.Edit;
            if cdQueue['SY_REPORTS_GROUP_NBR'] =  0 then
            begin
              cdQueuePRIORITY.AsString := cdSyReports['ACTIVE_REPORT'];
              cdQueueSY_REPORTS_NBR.AsInteger := cdSyReports['SY_REPORTS_NBR'];
              cdQueueSY_REPORT_WRITER_REPORTS_NBR.AsInteger := cdSyReports['SY_REPORT_WRITER_REPORTS_NBR'];
            end
            else
            begin
              cdQueuePRIORITY.AsString := cdSyReportsGroup['ACTIVE_REPORT'];
              cdQueueSY_REPORTS_NBR.AsInteger := cdSyReportsGroup['SY_REPORTS_NBR'];
              cdQueueSY_REPORT_WRITER_REPORTS_NBR.AsInteger := cdSyReportsGroup['SY_REPORT_WRITER_REPORTS_NBR'];
              cdQueueSY_GL_AGENCY_REPORT_NBR.AsInteger := cdSyReportsGroup['SY_GL_AGENCY_REPORT_NBR'];
            end;

            cdQueueSY_REPORTS_DESC.AsString := cdSyReportsGroup.FieldByName('DESCRIPTION').AsString;

            cdQueueCONSOLIDATION.AsString := BoolToYN(not cdQueueCL_CO_CONSOLIDATION_NBR.IsNull);
            cdQueueHAVE_ACOPY.AsString := cdSyReportsGroup['AGENCY_COPY'];
            cdQueueHAVE_SCOPY.AsString := cdSyReportsGroup['SB_COPY'];
            cdQueueHAVE_CCOPY.AsString := cdSyReportsGroup['CL_COPY'];


            if pREPORT_TYPE[1] in [SYSTEM_REPORT_TYPE_ASCII, SYSTEM_REPORT_TYPE_TAX_RETURN_MULTICLIENT] then
              cdQueuePRODUCE_ASCII_FILE.AsString := GROUP_BOX_YES
            else
            begin
              if pREPORT_TYPE[1] in [SYSTEM_REPORT_TYPE_TAX_RETURN_XML] then
                cdQueuePRODUCE_ASCII_FILE.AsString := GROUP_BOX_XML
              else
                cdQueuePRODUCE_ASCII_FILE.AsString := GROUP_BOX_NO;
            end;

            if cdQueueHOLD_RETURN_QUEUE.AsString = GROUP_BOX_YES then
            begin
              cdQueueSTATUS.AsString := TAX_RETURN_STATUS_HOLD;
              cdQueueSTATUS_DATE.Clear;
              cdQueueKEEP_ONHOLD.AsString := GROUP_BOX_YES;
            end;
            if (cdQueueCL_CO_CONSOLIDATION_NBR.IsNull) and (cdQueueSTATUS.AsString <> TAX_RETURN_STATUS_PROCESSED)
            and not cdPreSummary.FindKey([cdQueueCL_NBR.AsInteger, cdQueueCO_NBR.AsInteger, cdQueueSY_GL_AGENCY_REPORT_NBR.AsInteger, cdQueuePERIOD_END_DATE.AsDateTime]) then
              cdPreSummary.AppendRecord([cdQueueCL_NBR.AsInteger, cdQueueCO_NBR.AsInteger, cdQueueSY_GL_AGENCY_REPORT_NBR.AsInteger, cdQueuePERIOD_END_DATE.AsDateTime]);

            cdQueueRETURN_FREQUENCY.AsString := cdSyReportsGroup['RETURN_FREQUENCY'];
            cdQueueSYSTEM_TAX_TYPE.AsString := cdSyReportsGroup['SYSTEM_TAX_TYPE'];
            cdQueueSY_GL_AGENCY_FIELD_OFFICE_NBR.AsInteger := cdSyReportsGroup['SY_GL_AGENCY_FIELD_OFFICE_NBR'];
            cdQueueSY_GLOBAL_AGENCY_NBR.AsInteger := cdSyReportsGroup['SY_GLOBAL_AGENCY_NBR'];
            cdQueueGlAgencyFOName.AsString := cdSyReportsGroup['ADDITIONAL_NAME'];
            cdQueueAgencyName.AsString := cdSyReportsGroup['AGENCY_NAME'];

            if string(cdSyReportsGroup['SYSTEM_TAX_TYPE'])[1] in [TAX_RETURN_TYPE_STATE, TAX_RETURN_TYPE_SUI, TAX_RETURN_TYPE_LOCAL] then
                cdQueuePRIORITY2.AsString := TAX_RETURN_TYPE_STATE+ '-'+
                  cdSyReportsGroup['AGENCY_NAME']+ '-'+
                  cdSyReportsGroup['SYSTEM_TAX_TYPE']+ '-'+
                  cdQueuePRIORITY.AsString
            else
                cdQueuePRIORITY2.AsString := cdSyReportsGroup['SYSTEM_TAX_TYPE']+ '-'+
                  cdSyReportsGroup['AGENCY_NAME']+ '-'+
                  cdQueuePRIORITY.AsString;

            if cdHeldList.FindKey([GetTag(cdQueue)]) then
              cdQueue['Taken'] := cdHeldList['UserName'];

          end
          else
            if not cbAnnually.Checked or not cbQuarterly.Checked or not cbMonthly.Checked then
            begin
              cdQueue.Delete;
              Continue;
            end
            else
            begin
              cdQueue.Edit;
              cdQueueSTATUS.AsString := TAX_RETURN_STATUS_HOLD;
              cdQueueSTATUS_DATE.Clear;
              cdQueueKEEP_ONHOLD.AsString := GROUP_BOX_YES;
              cdQueueCONSOLIDATION.AsString := GROUP_BOX_NO;
            end;
          if cdQueueTaken.AsString = lCurrentUserName then
            cdQueueHIDDEN.AsString := GROUP_BOX_YES
          else
            cdQueueHIDDEN.AsString := GROUP_BOX_NO;
          cdQueue.Post;
          cdQueue.Next;
        end;
        cdQueue.First;
        while not cdQueue.Eof do
        begin
          if (cdQueueCONSOLIDATION.AsString <> GROUP_BOX_NO)
          and (cdQueueKEEP_ONHOLD.AsString <> GROUP_BOX_YES) then
          begin
            cdQueue.Edit;
            sStatus := GROUP_BOX_NO;
            GetConsolidationDescr(cdQueueCL_NBR.AsInteger, cdQueueCO_NBR.AsInteger, cdQueueSY_GL_AGENCY_REPORT_NBR.AsInteger, cdQueuePERIOD_END_DATE.AsDateTime, sConsol, sConsol2, sStatus, cdQueuePRODUCE_ASCII_FILE.AsString);
            cdQueueCONSOLIDATION_DESCR.AsString := sConsol;
            cdQueueCONSOLIDATION_DESCR2.AsString := sConsol2;
            cdQueueKEEP_ONHOLD.AsString := sStatus;
            if sStatus = GROUP_BOX_YES then
              cdQueueSTATUS.AsString := TAX_RETURN_STATUS_HOLD;
            cdQueue.Post;
          end;
          if (cbStatus.ItemIndex <> 0) and (cdQueueSTATUS.AsString <> FReturnStatusCodeValues[cbStatus.ItemIndex]) then
            cdQueue.Delete
          else
            cdQueue.Next;
        end;

        cdQueue.First;
        cdSummary.IndexName := 'I_NBR';
        while not cdQueue.Eof do
        begin
          if not cdSummary.FindKey([cdQueuecl_nbr.AsInteger, cdQueueco_nbr.AsInteger, cdQueueconsolidation_descr2.AsVariant]) then
          begin
            cdSummary.Append;
            cdSummary['cl_nbr;co_nbr;co_name;co_number;tax_service;CL_CO_CONSOLIDATION_NBR'] := cdQueue['cl_nbr;co_nbr;co_name;co_number;tax_service;CL_CO_CONSOLIDATION_NBR'];
            cdSummaryconsolidation.AsVariant := cdQueueconsolidation_descr2.AsVariant;
            cdSummary['has_paper;paper_processed;processed;onhold'] := VarArrayOf([False, True, True, False]);
            Assert(cdStatuses.FindKey([cdQueuecl_nbr.AsInteger, cdQueueco_nbr.AsInteger]));
            if cdSummaryconsolidation.AsString <> '' then
            begin
              cdSummaryErrorMessage.AsString := ConvertNull(cdStatuses['LAST_PREPROCESS_MESSAGE_CONS'], '');
              cdSummaryPreProcessDate.AsDateTime := ConvertNull(cdStatuses['LAST_PREPROCESS_CONS'], 0);
            end
            else
            begin
              cdSummaryErrorMessage.AsString := ConvertNull(cdStatuses['LAST_PREPROCESS_MESSAGE'], '');
              cdSummaryPreProcessDate.AsDateTime := ConvertNull(cdStatuses['LAST_PREPROCESS'], 0);
            end;
          end
          else
            cdSummary.Edit;
          if cdQueuePRODUCE_ASCII_FILE.AsString = GROUP_BOX_NO then
            cdSummaryhas_paper.AsBoolean := True;
          if cdQueueSTATUS.AsString <> TAX_RETURN_STATUS_PROCESSED then
          begin
            cdSummaryprocessed.AsBoolean := False;
            if cdQueuePRODUCE_ASCII_FILE.AsString = GROUP_BOX_NO then
              cdSummarypaper_processed.AsBoolean := False;
          end;
          if cdQueueSTATUS.AsString = TAX_RETURN_STATUS_HOLD then
            cdSummaryonhold.AsBoolean := True;
          cdSummary.Post;

          if  (cdQueue['STATUS'] = TAX_RETURN_STATUS_PROCESSED) and
              (QMef.Result.RecordCount > 0) and
              QMef.Result.Locate('SY_REPORT_WRITER_REPORTS_NBR',cdQueue['SY_REPORT_WRITER_REPORTS_NBR'],[]) then
          begin
                cdMEF.Append;
                cdMEF['co_number;co_name;Tax_Return;ProcessDate'] := cdQueue['co_number;co_name; SY_REPORTS_DESC;STATUS_DATE'];
                cdMEF['LookUpName;REPORT_DESCRIPTION'] := QMef.Result['NAME;REPORT_DESCRIPTION'];
                cdMEF['CO_TAX_RETURN_QUEUE_NBR;CL_NBR;CO_NBR'] := cdQueue['CO_TAX_RETURN_QUEUE_NBR;CL_NBR;CO_NBR'];
                cdMEF.post;
          end;
          if  (cdQueue['STATUS'] = TAX_RETURN_STATUS_PROCESSED) and
              (QAca.Result.RecordCount > 0) and
              QAca.Result.Locate('SY_REPORT_WRITER_REPORTS_NBR',cdQueue['SY_REPORT_WRITER_REPORTS_NBR'],[]) then
          begin
                cdACA.Append;
                cdACA['co_number;co_name;Tax_Return;ProcessDate'] := cdQueue['co_number;co_name; SY_REPORTS_DESC;STATUS_DATE'];
                cdACA['LookUpName;REPORT_DESCRIPTION'] := QAca.Result['NAME;REPORT_DESCRIPTION'];
                cdACA['CO_TAX_RETURN_QUEUE_NBR;CL_NBR;CO_NBR'] := cdQueue['CO_TAX_RETURN_QUEUE_NBR;CL_NBR;CO_NBR'];
                cdACA.post;
          end;

          cdQueue.Next;
        end;
        cdSummary.IndexName := 'SORT';
        cbFilter.OnChange(nil);
        cbMmFilter.OnChange(nil);

        RefreshBriefViewList;

        cdQueue.RetrieveCondition :='HIDDEN = '''+ GROUP_BOX_YES+ '''';
        cdSelectedQueue.Data := CreateLookupData(cdQueue);
        cdSelectedQueue.First;
        while not cdSelectedQueue.Eof do
        begin
          AddReport;
          cdSelectedQueue.Next;
        end;
        cdSelectedQueue.First;
      finally
        cdPreSummary.Free;
        cdQueue.IndexName := s;
        cdQueue.RetrieveCondition := '';    // refresh issue?
        cdQueue.RetrieveCondition := 'HIDDEN = '''+ GROUP_BOX_NO+ '''';
        if not VarIsNull(vLocateValues) then
        begin
          vLocateValues[2] := QuotedStr(vLocateValues[2]);
          vLocateValues[2] := Copy(vLocateValues[2], 2, Length(vLocateValues[2])-2);
          cdSummary.Locate('CL_NBR;CO_NBR;CONSOLIDATION', vLocateValues, [])
        end
        else
          cdSummary.First;

        cdSummary.EnableControls;
        MakeAllButSelectReadOnly(cdMEF,true);
        MakeAllButSelectReadOnly(cdACA,true);
        cdLookupMEF.First;
        cdLookupACA.First;
        SetMefFilter;
        SetAcaFilter;
        cdMEF.EnableControls;
        cdACA.EnableControls;
        grdMEF.UnSelectAll;
        grdACA.UnSelectAll;

        if cdSelectedQueue.RecordCount > 0 then
        try
          cdSelectedQueue.Bookmark := b;
        except
        end;
      end;
    finally
      cdSelectedQueue.EnableControls;
      cdQueue.AssignDataFrom(cdQueue); // fixes strange multiselection problem
      cdQueue.AfterScroll := cdQueueAfterScrollStorage;
      dsQueue.DataSet := cdQueue;
    end;
  finally
    ctx_EndWait;
  end;
  cdSelectedQueueAfterScroll(nil);
  if not cdProcessed.Active then
  begin
    cdProcessed.Data := CreateLookupData(cdSelectedQueue);
    cdProcessed.fieldbyname('CL_BLOB_NBR').ReadOnly := False;
    cdProcessed.EmptyDataSet;
    cdProcessed.LogChanges := False;
  end;
  FFlagTypeName :=   'TaxPreProc'+ FormatDateTime('MMDDYYYY', cbDateTill.DateTime);
  PC.Visible := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbDetailClCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if Modified
  and (cbDetailCo.LookupValue <> '')
  and not cbDetailCo.LookupTable.Locate(cbDetailCo.LookupField, StrToInt(cbDetailCo.LookupValue), []) then
    cbDetailCo.Clear;
end;



procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbDetailReportAgencyCloseUp(
  Sender: TObject; LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if Modified
  and (cbDetailReportFieldOffice.LookupValue <> '')
  and not cbDetailReportFieldOffice.LookupTable.Locate(cbDetailReportFieldOffice.LookupField, StrToInt(cbDetailReportFieldOffice.LookupValue), []) then
  begin
    cbDetailReportFieldOffice.Clear;
    cbDetailReportFieldOffice.OnCloseUp(nil, nil, nil, True);
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbDetailReportFieldOfficeCloseUp(
  Sender: TObject; LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  inherited;
  if Modified  then
  begin
   if (cbDetailReportFieldOffice.Text <> '') and (cbDetailReportFieldOffice.LookupValue <> '') then
    cdFieldOfficeReports.Filter := 'SY_GL_AGENCY_FIELD_OFFICE_NBR = ' + cbDetailReportFieldOffice.LookupValue
   else
    cdFieldOfficeReports.Filter := 'SY_GL_AGENCY_FIELD_OFFICE_NBR = -1 ';
   cdFieldOfficeReports.Filtered := True;
  end;
end;


function TEDIT_TMP_CO_TAX_RETURN_QUEUE.PeriodEndDate(const GlAgencyReportNbr: Integer):TDateTime;
begin
  Result := cdQueue['PERIOD_END_DATE'];
  if (not VarIsNull(cdQueue['PERIOD_END_DATE']) and (cdQueue['PERIOD_END_DATE'] <> 0))
    and not VarIsNull(GlAgencyReportNbr) then
    if DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.Locate('SY_GL_AGENCY_REPORT_NBR', GlAgencyReportNbr, []) then
    begin
     if DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT['RETURN_FREQUENCY'] = FREQUENCY_TYPE_ANNUAL then
      Result := GetEndYear(cdQueue['PERIOD_END_DATE'])
     else
     if DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT['RETURN_FREQUENCY'] = FREQUENCY_TYPE_QUARTERLY then
      Result := GetEndQuarter(cdQueue['PERIOD_END_DATE'])
     else
     if DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT['RETURN_FREQUENCY'] = FREQUENCY_TYPE_MONTHLY then
      Result := GetEndMonth(cdQueue['PERIOD_END_DATE'])
     else
     if DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT['RETURN_FREQUENCY'] = FREQUENCY_TYPE_SEMI_MONTHLY then
      begin
       if GetDay(cdQueue['PERIOD_END_DATE']) < 16 then
         Result := GetBeginMonth(cdQueue['PERIOD_END_DATE'])+ 14 // the 15th
       else
         Result := GetEndMonth(cdQueue['PERIOD_END_DATE']);
      end
     end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.CalcDueDate;
begin
  if (cdQueue.State in [dsEdit, dsInsert])
  and (not VarIsNull(cdQueue['PERIOD_END_DATE']) and (cdQueue['PERIOD_END_DATE'] <> 0))
  and not VarIsNull(cdQueue['SY_GL_AGENCY_REPORT_NBR']) then
    cdQueue['DUE_DATE'] := GenericCalcDueDate(cdQueue['SY_GLOBAL_AGENCY_NBR'], cdQueue['SY_GL_AGENCY_REPORT_NBR'], cdQueue['PERIOD_END_DATE']);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.PcChanging(
  Sender: TObject; var AllowChange: Boolean);
begin
  inherited;
  if (Pc.ActivePage = tsDetail)
  and (dsQueue.State in [dsEdit, dsInsert]) then
    AllowChange := False;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdDetailPeriodEndDateCloseUp(
  Sender: TObject);
begin
  inherited;
  if cdQueue.State in [dsEdit, dsInsert] then
    cdDetailPeriodEndDate.UpdateRecord;
  CalcDueDate;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.BitBtn3Click(Sender: TObject);

  procedure Process;
  var
    i: Integer;
    FlagInfo: IevGlobalFlagInfo;
  begin
    if not VarIsNull(cdQueue['CL_NBR'])
    and (cdQueue['STATUS'] <> TAX_RETURN_STATUS_HOLD)
    and (VarIsNull(cdQueue['Taken'])) then
    begin
      if not mb_GlobalFlagsManager.TryLock(TypeName, GetTag(cdQueue), FlagInfo) then
      begin
        if Assigned(Sender) then
          EvMessage('Another user has already held the record');
        cdQueue.Edit;
        cdQueue['Taken'] := FlagInfo.UserName;
        cdQueue.Post;
      end
      else
      begin
        cdSelectedQueue.Append;
        for i := 0 to cdSelectedQueue.FieldCount-1 do
          if cdSelectedQueue.Fields[i].FieldKind in [fkData, fkInternalCalc] then
            if cdSelectedQueue.Fields[i].FieldName <> 'CL_BLOB_NBR' then
              cdSelectedQueue.Fields[i].Value := cdQueue[cdSelectedQueue.Fields[i].FieldName];
        cdSelectedQueue.Post;
        cdQueue.Edit;
        cdQueue['HIDDEN'] := GROUP_BOX_YES;
        cdQueue.Post;
        AddReport;
      end;
    end;
  end;
var
  i: Integer;
  iRecNo: Integer;
begin
  inherited;
  iRecNo := cdQueue.RecNo;
  if cdQueue.RecordCount > 0 then
  begin
    cdQueue.DisableControls;
    try
      if wwDBGrid1.SelectedList.Count > 1 then
      begin
        for i := wwDBGrid1.SelectedList.Count-1 downto 0 do
        begin
          wwDBGrid1.DataSource.DataSet.GotoBookmark(wwDBGrid1.SelectedList[i]);
          Process;
        end;
      end
      else
        Process;
    finally
      cdQueue.EnableControls;
    end;
    wwDBGrid1.UnselectAll;
    cdQueue.RecNo := iRecNo;
    wwDBGrid1.SelectRecord;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.BitBtn2Click(Sender: TObject);
var
  i: Integer;
  cd: TevClientDataSet;
  procedure Process;
  begin
    if cd.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', cdSelectedQueue['CL_NBR;CO_TAX_RETURN_QUEUE_NBR'], []) then
    begin
      cd.Edit;
      cd['HIDDEN'] := GROUP_BOX_NO;
      cd['STATUS;SB_COPY_PRINTED;CLIENT_COPY_PRINTED;AGENCY_COPY_PRINTED'] := cdSelectedQueue['STATUS;SB_COPY_PRINTED;CLIENT_COPY_PRINTED;AGENCY_COPY_PRINTED'];
      cd['Taken'] := Null;
      cd.Post;
    end;
    mb_GlobalFlagsManager.ForcedUnlock(TypeName, GetTag(cdSelectedQueue));
    cdSelectedQueue.Delete;
  end;
begin
  inherited;
  if cdSelectedQueue.RecordCount > 0 then
  begin
    cd := TevClientDataSet.Create(nil);
    cd.CloneCursor(cdQueue, True);
    cdQueue.DisableControls;
    try
      if wwDBGrid2.SelectedList.Count > 1 then
      begin
        for i := wwDBGrid2.SelectedList.Count-1 downto 0 do
        begin
          wwDBGrid2.DataSource.DataSet.GotoBookmark(wwDBGrid2.SelectedList[i]);
          Process;
        end;
      end
      else
        Process;
    finally
      cdQueue.EnableControls;
      wwDBGrid1.UnselectAll;
      cdQueue.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', cd['CL_NBR;CO_TAX_RETURN_QUEUE_NBR'], []);
      wwDBGrid1.SelectRecord;
      wwDBGrid1.Refresh;
      cd.Free;
    end;
    wwDBGrid2.UnselectAll;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.BitBtn4Click(Sender: TObject);
var
  i: Integer;
  cd: TevClientDataSet;
  FlagInfo: IevGlobalFlagInfo;
begin
  inherited;
  if EvMessage('You are about to move all reports to the queue.'#13'This operation may take a while.'#13'Proceed?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Exit;
  cdQueue.DisableControls;
  cdSelectedQueue.DisableControls;
  cd := TevClientDataSet.Create(nil);
  try
    cd.CloneCursor(cdQueue, True);
    cd.IndexFieldNames := 'CL_NBR;CO_TAX_RETURN_QUEUE_NBR';
    cd.UserFilter := cdQueue.UserFilter;
    cd.UserFiltered := cdQueue.UserFiltered;
    cd.First;
    while not cd.Eof do
    begin
      if (cd['HIDDEN'] = GROUP_BOX_NO) then
        if (cd['STATUS'] <> TAX_RETURN_STATUS_HOLD) then
          if (ConvertNull(cd['Taken'], '') = '') then
          begin
            if not mb_GlobalFlagsManager.TryLock(TypeName, GetTag(cd), FlagInfo) then
            begin
              EvMessage('Another user has already held one of the records');
              cd.Edit;
              cd['Taken'] := FlagInfo.UserName;
              cd.Post;
            end
            else
            begin
              cdSelectedQueue.Append;
              for i := 0 to cdSelectedQueue.FieldCount-1 do
                if cdSelectedQueue.Fields[i].FieldKind in [fkData, fkInternalCalc] then
                  if cdSelectedQueue.Fields[i].FieldName <> 'CL_BLOB_NBR' then
                    cdSelectedQueue.Fields[i].Value := cd[cdSelectedQueue.Fields[i].FieldName];
              cdSelectedQueue.Post;
              cd.Edit;
              cd['HIDDEN'] := GROUP_BOX_YES;
              cd.Post;
            end;
          end;
      cd.Next;
    end;
  finally
    cd.Free;
    cdQueue.EnableControls;
    cdSelectedQueue.EnableControls;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.BitBtn1Click(Sender: TObject);
var
  cd: TevClientDataSet;
begin
  inherited;
  wwDBGrid1.UnselectAll;
  wwDBGrid2.UnselectAll;
  cdSelectedQueue.DisableControls;
  cdQueue.DisableControls;
  cd := TevClientDataSet.Create(nil);
  try
    cd.CloneCursor(cdQueue, True);

    cd.IndexFieldNames := 'CL_NBR;CO_TAX_RETURN_QUEUE_NBR';
    cdSelectedQueue.First;
    while not cdSelectedQueue.Eof do
    begin
      if cd.FindKey([cdSelectedQueue['CL_NBR'], cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR']]) then
      begin
        cd.Edit;
        cd['HIDDEN'] := GROUP_BOX_NO;
        cd['STATUS;SB_COPY_PRINTED;CLIENT_COPY_PRINTED;AGENCY_COPY_PRINTED'] := cdSelectedQueue['STATUS;SB_COPY_PRINTED;CLIENT_COPY_PRINTED;AGENCY_COPY_PRINTED'];
        cd['Taken'] := Null;
        cd.Post;
      end;
      mb_GlobalFlagsManager.ForcedUnlock(TypeName, GetTag(cdSelectedQueue));
      cdSelectedQueue.Delete;
    end;
  finally
    cdSelectedQueue.EnableControls;
    cdQueue.EnableControls;

    cdQueue.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', cd['CL_NBR;CO_TAX_RETURN_QUEUE_NBR'], []);

    wwDBGrid1.SelectRecord;
    wwDBGrid1.Refresh;

    cd.Free;

  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.wwDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  BitBtn3.Click;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.wwDBGrid2DblClick(Sender: TObject);
begin
  inherited;
  BitBtn2.Click;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.wwDBGrid2CalcCellColors(
  Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  inherited;

  if not VarIsNull(Field.Dataset['STATUS']) then
    with Field.Dataset do
      if not(cbSettingsPrintAgencyCopy.Checked
      or cbSettingsPrintClientCopy.Checked
      or (edCopies.Value <> 0)
      or cbSettingsPrintSbCopy.Checked) then
        ABrush.Color := MakeHalfTone(ABrush.Color)
      else
      if not VarIsNull(Field.Dataset['CL_BLOB_NBR']) then
        ABrush.Color := clRed;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.wwDBGrid3CalcCellColors(
  Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  inherited;
  if (UpperCase(Field.FullName) = 'TAKEN')
  and (Field.AsString <> '') then
    ABrush.Color := $008080FF
  else
    if not VarIsNull(Field.Dataset['STATUS']) then
      if not (string(Field.Dataset['STATUS'])[1] in [TAX_RETURN_STATUS_UNPROCESSED]) then
        ABrush.Color := MakeHalfTone(ABrush.Color);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.PcChange(Sender: TObject);
begin
  inherited;
  if Pc.ActivePage = tsDetail then
  begin
    tsDetail.Enabled := (cdQueue.State = dsInsert)
      or (cdQueue.Active and (cdQueue.RecordCount > 0));
    cbDetailCl.Enabled := cdQueue.State = dsInsert;
    cbDetailStatus.Enabled := cdQueue['KEEP_ONHOLD'] <> GROUP_BOX_YES;
  end
  else
  if Pc.ActivePage = tsCompanies then
    cdSummary.RetrieveCondition := ''
  else
  if Pc.ActivePage = tsSimple then
  begin
    cdSummary.RetrieveCondition := 'has_paper';
  end
  else
  if Pc.ActivePage = tsEe then
  begin
    if cdEe.RecordCount = 0 then
    begin
      ctx_StartWait;
      try
        AddEe(ConvertNull(cdSelectedQueue['CONSOLIDATION_DESCR'], '') <> '', cdSelectedQueue['CL_NBR'], cdSelectedQueue['CO_NBR'], cdQueue['PERIOD_END_DATE'], cdSelectedQueueStatusDate);
      finally
        ctx_EndWait;
      end;
    end;
  end
  else
  if Pc.ActivePage = tsEe then
  begin
    edOriginalReceiptId.Enabled:= false;
    cbTransmissionReplacement.Checked:= false;
  end;
  CheckEnableDeleteFunction;
end;

function TEDIT_TMP_CO_TAX_RETURN_QUEUE.MakeHalfTone(
  const C: TColor): TColor;
begin
  Result := ColorToRGB(C);
  Result :=
    ((Result mod 256) * 5 div 6) +
    ((Result div 256 mod 256) * 5 div 6) * 256 +
    ((Result div 256 div 256 mod 256) * 5 div 6) * 256 * 256;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.MessageLogSaveButtonClick(
  Sender: TObject);
begin
  inherited;
  if MessageLogSaveDialog.Execute then
  begin
    cbMessages.Items.SaveToFile(MessageLogSaveDialog.FileName);
    EvMessage('Saved.');
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbMessagesDblClick(
  Sender: TObject);
var
  i: Integer;
begin
  inherited;
  i := cbMessages.ItemIndex;
  if i <> -1 then
    EvMessage(cbMessages.Items[i]);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.wwDBGrid3DblClick(Sender: TObject);
begin
  inherited;
  Pc.ActivePage := tsDetail;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnEnlistClick(Sender: TObject);
begin
  inherited;
  with TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.Create(nil) do
  try
    if ShowModal = mrOk then
      btnApplySelect.Click; // refresh
  finally
    Free;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdQueueAfterScroll(
  DataSet: TDataSet);
  procedure Process(const c: TevDBLookupCombo);
  var
    s: string;
  begin
    if Assigned(c.LookupTable) then
    begin
      s := c.Selected[0];
      s := Copy(s, 1, Pos(#9, s)-1);
      c.Text := c.LookupTable.FieldByName(s).AsString;
    end;
  end;
begin
  if DataSet.ControlsDisabled then
    Exit;
  inherited;
  if (cdQueue.State = dsBrowse)
  and (cdQueue.RecordCount > 0) then
  begin

    Process(cbDetailCl);
    Process(cbDetailCo);
    Process(cbDetailReportAgency);
    Process(cbDetailReportFieldOffice);

    if (cbDetailReportFieldOffice.Text <> '') and (cbDetailReportFieldOffice.LookupValue <> '') then
     cdFieldOfficeReports.Filter := 'SY_GL_AGENCY_FIELD_OFFICE_NBR = ' + cbDetailReportFieldOffice.LookupValue
    else
     cdFieldOfficeReports.Filter := 'SY_GL_AGENCY_FIELD_OFFICE_NBR = -1 ';
    cdFieldOfficeReports.Filtered := True;
    if (not VarIsNull(cdQueue['SY_GL_AGENCY_REPORT_NBR'])) and
       (cdFieldOfficeReports.RecordCount > 0) then
    begin
      cdFieldOfficeReports.Locate('SY_GL_AGENCY_FIELD_OFFICE_NBR;SY_GL_AGENCY_REPORT_NBR',
               cdQueue['SY_GL_AGENCY_FIELD_OFFICE_NBR;SY_GL_AGENCY_REPORT_NBR'], []);
    end;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbMultiDeleteClick(
  Sender: TObject);
begin
  inherited;
  if cbMultiDelete.Checked then
    wwDBGrid3.Options := wwDBGrid3.Options+ [dgMultiSelect]
  else
  begin
    wwDBGrid3.Options := wwDBGrid3.Options- [dgMultiSelect];
    wwDBGrid3.UnselectAll;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.Button1Click(Sender: TObject);
var
  i: Integer;
  sMsg: string;
  LastChoice:integer;
  sIndexName: string;
  FlagInfo: IevGlobalFlagInfo;
begin
  inherited;
  LastChoice:=mrNone;
  if not VarIsNull(cdSummary['cl_nbr']) then
  begin
    cdQueue.DisableControls;
    cdQueue.Resync([]);
    sIndexName := cdQueue.IndexName;
    cdQueue.IndexName := '';
    cdQueue.IndexFieldNames := '';
    cdQueue.Filtered := False;
    try
      cdQueue.First;
      while not cdQueue.Eof do
      begin
        if (cdQueue['HIDDEN'] = GROUP_BOX_NO)
        and (cdQueue['cl_nbr'] = cdSummary['cl_nbr'])
        and (ConvertNull(cdQueue['consolidation_descr2'], '') = ConvertNull(cdSummary['consolidation'], ''))
        and (cdQueue['co_nbr'] = cdSummary['co_nbr'])
        and (cdQueue['status'] <> TAX_RETURN_STATUS_HOLD)
        and (Assigned(Sender) or (cdQueue['PRODUCE_ASCII_FILE'] = GROUP_BOX_NO)) then
        begin
          if not mb_GlobalFlagsManager.TryLock(TypeName, GetTag(cdQueue), FlagInfo) then
          begin
            cdQueue.Edit;
            cdQueue['Taken'] := FlagInfo.UserName;
            cdQueue.Post;
            if LastChoice <> mrNoToAll then
            begin
              sMsg := 'Report '+ ConvertNull(cdQueue['SY_REPORTS_DESC'], 'N/A')+ ' for company "'+ ConvertNull(cdQueue['CO_NAME'], 'N/A')+
                      '"'#13'is taken by user "'+UpperCase(FlagInfo.UserName)+ '" and cannot be processed by you.'+
                      #13'Do you want to stop now and fix it first?';
              LastChoice := EvMessage(sMsg, mtConfirmation, [mbYes, mbNo,mbNoToAll]);
            end;
            if LastChoice = mrYes then
              AbortEx
            else
               cbMessages.Items.Append('Report '+ ConvertNull(cdQueue['SY_REPORTS_DESC'], 'N/A')+ ' for company "'+ ConvertNull(cdQueue['CO_NAME'], 'N/A')+ '"'#13'is taken by user "'+ UpperCase(FlagInfo.UserName)+ '" and was skipped');
            cdQueue.Next;
          end
          else
          begin
            cdSelectedQueue.Append;
            for i := 0 to cdSelectedQueue.FieldCount-1 do
              if cdSelectedQueue.Fields[i].FieldKind in [fkData, fkInternalCalc] then
                if cdSelectedQueue.Fields[i].FieldName <> 'CL_BLOB_NBR' then
                  cdSelectedQueue.Fields[i].Value := cdQueue[cdSelectedQueue.Fields[i].FieldName];
            cdSelectedQueue.Post;
            cdQueue.Edit;
            cdQueue['HIDDEN'] := GROUP_BOX_YES; //  because of hidden field, we dont use cdQueue.Next command....
            cdQueue.Post;
            AddReport;
          end;
        end
        else
        cdQueue.Next;
      end;
    finally
      cdQueue.Filtered := True;
      cdQueue.IndexName := sIndexName;
      cdQueue.First;
      cdQueue.EnableControls;
    end;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.wwDBGrid3CreateHintWindow(
  Sender: TObject; HintWindow: TwwGridHintWindow; AField: TField; R: TRect;
  var WordWrap: Boolean; var MaxWidth, MaxHeight: Integer;
  var DoDefault: Boolean);
begin
  MaxHeight := MaxHeight* 12;
  inherited;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.dsQueueDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  CheckEnableDeleteFunction;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnOkClick(Sender: TObject);
  procedure Process(const ClNbr, CoNbr: Integer; ConsNbr: Variant);
  var
   i : integer;
  begin
    try
      ctx_DataAccess.OpenClient(ClNbr);
      DM_COMPANY.CO.DataRequired('ALL');
      if cdQueue.State = dsInsert then
        DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR = -1')
      else
      if cdQueue.State = dsEdit then
        DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR = '+ IntToStr(cdQueue['CO_TAX_RETURN_QUEUE_NBR']))
      else
        Assert(False);

      cdQueue.UpdateRecord;
      cbDetailStatus.UpdateRecord;
      if grdFieldOfficeReports.SelectedList.Count >= 1 then
      begin
        for i := grdFieldOfficeReports.SelectedList.Count-1 downto 0 do
        begin
          grdFieldOfficeReports.DataSource.DataSet.GotoBookmark(grdFieldOfficeReports.SelectedList[i]);

          if cdQueue.State = dsInsert then
            DM_COMPANY.CO_TAX_RETURN_QUEUE.Insert
          else
            DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;

          DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_NBR'] := CoNbr;
          DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] := cdQueue['STATUS'];

          DM_COMPANY.CO_TAX_RETURN_QUEUE['SY_REPORTS_GROUP_NBR'] := 0;
          DM_COMPANY.CO_TAX_RETURN_QUEUE['SY_GL_AGENCY_REPORT_NBR'] := grdFieldOfficeReports.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'];

          DM_COMPANY.CO_TAX_RETURN_QUEUE['DUE_DATE'] := GenericCalcDueDate(cbDetailReportAgency.LookupTable['SY_GLOBAL_AGENCY_NBR'], grdFieldOfficeReports.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR'], cdQueue['PERIOD_END_DATE']);
          DM_COMPANY.CO_TAX_RETURN_QUEUE['CL_CO_CONSOLIDATION_NBR'] := ConsNbr;

          DM_COMPANY.CO_TAX_RETURN_QUEUE['PERIOD_END_DATE'] := PeriodEndDate(grdFieldOfficeReports.DataSource.DataSet['SY_GL_AGENCY_REPORT_NBR']);
          DM_COMPANY.CO_TAX_RETURN_QUEUE['CLIENT_COPY_PRINTED'] := cdQueue['CLIENT_COPY_PRINTED'];
          DM_COMPANY.CO_TAX_RETURN_QUEUE['AGENCY_COPY_PRINTED'] := cdQueue['AGENCY_COPY_PRINTED'];
          DM_COMPANY.CO_TAX_RETURN_QUEUE['SB_COPY_PRINTED'] := cdQueue['SB_COPY_PRINTED'];
          DM_COMPANY.CO_TAX_RETURN_QUEUE['PRODUCE_ASCII_FILE'] := cdQueue['PRODUCE_ASCII_FILE'];
          DM_COMPANY.CO_TAX_RETURN_QUEUE.Post;
        end;
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE]);
      end;
    except
      if DM_COMPANY.CO_TAX_RETURN_QUEUE.State in [dsEdit, dsInsert] then
        DM_COMPANY.CO_TAX_RETURN_QUEUE.Cancel;
      raise;
    end;
  end;
begin
  inherited;
  try
    if (((cbDetailCl.LookupValue = '') or (cbDetailCo.LookupValue = '')) and cbDetailCo.Enabled)
    or (cdDetailPeriodEndDate.Date = 0)
    or (cbDetailStatus.ItemIndex = -1) then
      raise EInvalidParameters.CreateHelp('Set all required fields first', IDH_InvalidParameters);

    if grdFieldOfficeReports.SelectedList.Count = 0 then
      raise EInvalidParameters.CreateHelp('Agency report you''ve chosen doesn''t have any reports', IDH_InvalidParameters);

    if cbDetailCo.Enabled then
    begin
      if cbConsol.Visible and cbConsol.Checked then
         Process(StrToInt(cbDetailCl.LookupValue), cdQueue['CO_NBR'], cbDetailCo.LookupTable['CL_CO_CONSOLIDATION_NBR'])
      else
         Process(StrToInt(cbDetailCl.LookupValue), cdQueue['CO_NBR'], Null);
    end
    else
    begin
     FCompanyMuliSelectScreen.cdClCo.First;
     while not FCompanyMuliSelectScreen.cdClCo.Eof do
     begin
       if FCompanyMuliSelectScreen.cdClCo['LOAD'] then
           Process(FCompanyMuliSelectScreen.cdClCo['CL_NBR'], FCompanyMuliSelectScreen.cdClCo['CO_NBR'], FCompanyMuliSelectScreen.cdClCo['CL_CO_CONSOLIDATION_NBR']);
       FCompanyMuliSelectScreen.cdClCo.Next;
     end;
    end;

    cbConsol.Visible := False;
    cdQueue.Cancel;
    btnApplySelect.Click;
    cdQueue.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', VarArrayOf([ctx_DataAccess.ClientID, DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_TAX_RETURN_QUEUE_NBR']]), []);
    GotoOutOfEditMode;
  finally
  end;
end;


procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnCancelClick(Sender: TObject);
begin
  inherited;
  cdQueue.Cancel;
  GotoOutOfEditMode;
  if (cdQueue.RecordCount > 0 ) then cbDetailCo.SetFocus;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.GotoOutOfEditMode;
begin
  tsDetail.Enabled := cdQueue.Active and (cdQueue.RecordCount > 0);
  cbDetailCl.Enabled := False;
  cbDetailCo.Enabled := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnAddClick(Sender: TObject);
begin
  inherited;

  if cbSimpleMode.Checked then exit;
  if not PC.Visible then exit;

  tsDetail.TabVisible := True;
  PC.ActivatePage(tsDetail);
  tsDetail.Enabled := true;
  cbDetailCl.Enabled := True;
  cbDetailCo.Clear;
  cbDetailCl.SetFocus;
  cdQueue.Insert;
  cdQueue['SB_COPY_PRINTED'] := GROUP_BOX_NO;
  cdQueue['CLIENT_COPY_PRINTED'] := GROUP_BOX_NO;
  cdQueue['AGENCY_COPY_PRINTED'] := GROUP_BOX_NO;
  cdQueue['PRODUCE_ASCII_FILE'] := GROUP_BOX_NO;
  cdQueue['PERIOD_END_DATE'] := cbDateTill.DateTime;
  cdQueue['STATUS'] := TAX_RETURN_STATUS_UNPROCESSED;
  cdQueuePERIOD_END_DATE.AsDateTime := Trunc(cbDateTill.DateTime);
  cdDetailPeriodEndDate.DateTime := Trunc(cbDateTill.DateTime);
  cbDetailReportAgency.Clear;
  cbDetailReportFieldOffice.Clear;
  cdFieldOfficeReports.Filter := 'SY_GL_AGENCY_FIELD_OFFICE_NBR = -1 ';
  cdFieldOfficeReports.Filtered := True;

end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnDelClick(Sender: TObject);
const
  NoRightsMessage = 'You have no security rights to delete processed returns';
  procedure SubDelete(const ds: TDataSet);
  var
    s: string;
    ClNbr: Integer;
    procedure SubSubDelete;
    begin
      if s <> '' then
      begin
        ctx_DataAccess.OpenClient(ClNbr);
        ctx_DataAccess.StartNestedTransaction([dbtClient]);
        try
          DM_COMPANY.CO.DataRequired('ALL');
          DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR in ('+ s+ ')');
          DM_COMPANY.CO_TAX_RETURN_RUNS.DataRequired('CO_TAX_RETURN_QUEUE_NBR in ('+ s+ ')');

          while DM_COMPANY.CO_TAX_RETURN_QUEUE.RecordCount > 0 do
          begin
            if (DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] <> TAX_RETURN_STATUS_UNPROCESSED)
            and (ctx_AccountRights.Functions.GetState('RETURN_DELETE') <> stEnabled) then
              raise ENoRights.CreateHelp(NoRightsMessage, IDH_SecurityViolation);

            DM_COMPANY.CO_TAX_RETURN_RUNS.Filter := 'CO_TAX_RETURN_QUEUE_NBR = ' + intToStr(DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_TAX_RETURN_QUEUE_NBR']);
            DM_COMPANY.CO_TAX_RETURN_RUNS.Filtered := True;
            while DM_COMPANY.CO_TAX_RETURN_RUNS.RecordCount > 0 do
              DM_COMPANY.CO_TAX_RETURN_RUNS.Delete;

            DM_COMPANY.CO_TAX_RETURN_QUEUE.Delete;
          end;
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_RUNS, DM_COMPANY.CO_TAX_RETURN_QUEUE]);
          ctx_DataAccess.CommitNestedTransaction;
        except
          ctx_DataAccess.RollbackNestedTransaction;
          raise;
        end;
        s := '';
      end;
    end;
  begin
    ClNbr := -1;
    s := '';
    ds.First;
    while not ds.Eof do
    begin
      if ClNbr <> ds['CL_NBR'] then
      begin
        SubSubDelete;
        ClNbr := ds['CL_NBR'];
      end;
      if s <> '' then
        s := s+ ',';
      s := s+ IntToStr(ds['Q_NBR']);
      ds.Next;
    end;
    SubSubDelete;
  end;
var
  MarkedRows: TISBasicClientDataSet;
  i: Integer;
begin
  inherited;
  if EvMessage('Delete record(s).  Are you sure?', mtConfirmation, [mbYes, mbNo]) = mrYes then
  begin
    if cbMultiDelete.Checked then
    begin
      cdQueue.DisableControls;
      ctx_StartWait('Deleting...');
      MarkedRows := TISBasicClientDataSet.Create(nil);
      try
        MarkedRows.FieldDefs.Add('CL_NBR', ftInteger, 0, True);
        MarkedRows.FieldDefs.Add('Q_NBR', ftInteger, 0, True);
        MarkedRows.CreateDataSet;
        MarkedRows.LogChanges := False;
        MarkedRows.IndexFieldNames := 'CL_NBR';
        for i := 0 to wwDBGrid3.SelectedList.Count-1 do
        begin
          cdQueue.GotoBookmark(wwDBGrid3.SelectedList[i]);
          MarkedRows.AppendRecord([cdQueue['CL_NBR'], cdQueue['CO_TAX_RETURN_QUEUE_NBR']]);
        end;
        SubDelete(MarkedRows);
      finally
        MarkedRows.Free;
        ctx_EndWait;
        cdQueue.EnableControls;
      end;
      wwDBGrid3.UnSelectAll;
      btnApplySelect.Click;
    end
    else
    begin
      ctx_DataAccess.OpenClient(cdQueue['CL_NBR']);
      ctx_DataAccess.StartNestedTransaction([dbtClient]);
      try
        DM_COMPANY.CO.DataRequired('ALL');
        DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR = '+ IntToStr(cdQueue['CO_TAX_RETURN_QUEUE_NBR']));
        if (DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] <> TAX_RETURN_STATUS_UNPROCESSED)
        and (ctx_AccountRights.Functions.GetState('RETURN_DELETE') <> stEnabled) then
          raise ENoRights.CreateHelp(NoRightsMessage, IDH_SecurityViolation);

        DM_COMPANY.CO_TAX_RETURN_RUNS.DataRequired('CO_TAX_RETURN_QUEUE_NBR =' + intToStr(DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_TAX_RETURN_QUEUE_NBR']));
        while DM_COMPANY.CO_TAX_RETURN_RUNS.RecordCount > 0 do
          DM_COMPANY.CO_TAX_RETURN_RUNS.Delete;

        DM_COMPANY.CO_TAX_RETURN_QUEUE.Delete;
        ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_RUNS, DM_COMPANY.CO_TAX_RETURN_QUEUE]);

        ctx_DataAccess.CommitNestedTransaction;
      except
        ctx_DataAccess.RollbackNestedTransaction;
        raise;
      end;

      cdQueue.Delete;
    end;
  end;
  AbortEx;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.dsQueueStateChange(
  Sender: TObject);
begin
  inherited;
  btnOk.Visible := dsQueue.State in [dsInsert, dsEdit];
  btnCancel.Visible := btnOk.Visible;
  mEditMode.Visible := btnOk.Visible;
  btnAdd.Visible := not btnOk.Visible;
  gbTop.Enabled := not btnOk.Visible;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  procedure ClickIfEnabled(const b: TevBitBtn);
  var
    c: TWinControl;
    f: Boolean;
  begin
    c := b;
    f := c <> nil;
    while (c <> nil) and f do
    begin
      f := c.Enabled and c.Visible and f;
      c := c.Parent;
    end;
    if f then
      b.Click;
  end;
begin
  inherited;
  if (Shift = [ssCtrl]) and (Key = 46) then
  begin
    ClickIfEnabled(btnDel); // Del
    Key := 0;
  end
  else
  if Shift = [] then
  begin
    case Key of
    45: ClickIfEnabled(btnAdd); // Ins
    121: ClickIfEnabled(btnOk); //f10
    27: ClickIfEnabled(btnCancel); //Esc
    end;
    Key := 0;
  end;
end;

function TEDIT_TMP_CO_TAX_RETURN_QUEUE.CanClose: Boolean;
begin
  Result := not btnOk.Visible or not PC.Visible;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbDetailCoChange(Sender: TObject);
begin
  inherited;
  if cdQueue.State = dsInsert then
  begin
    cdQueue.UpdateRecord;
  end;
  if cdQueue.State in [dsInsert, dsEdit] then
  begin
    cbConsol.Visible := cbDetailCo.LookupTable['PRIMARY_CO_NBR'] = cbDetailCo.LookupTable['CO_NBR'];
    cbConsol.Checked := True;
    cbConsol.Enabled := True;
  end
  else
  if cdQueue.State in [dsBrowse] then
  begin
    cbConsol.Visible := True;
    cbConsol.Checked := cbDetailCo.LookupTable['PRIMARY_CO_NBR'] = cbDetailCo.LookupTable['CO_NBR'];
    cbConsol.Enabled := False;
  end
  else
    cbConsol.Visible := False;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.AddEe(const Consolidation: Boolean;
  const ClNbr, CoNbr: Integer; const PeriodEnd: TDateTime; const ChangedAfter: TDateTime);
var
  s: string;
  v: Variant;
  cd: TevClientDataSet;
begin
  s := '';
  if Consolidation then
  begin
    cdConsolidation.First;
    while not cdConsolidation.Eof do
    begin
      if (cdConsolidation['PRIMARY_CO_NBR'] = CoNbr)
      and (cdConsolidation['CL_NBR'] = ClNbr) then
      begin
        if s <> '' then s := s+ ' or ';
        s := s+ '(co_nbr='+ IntToStr(cdConsolidation['CO_NBR'])+ ')';
      end;
      cdConsolidation.Next;
    end;
  end
  else
    s := 'co_nbr='+ IntToStr(CoNbr);
  ctx_DataAccess.OpenClient(ClNbr);
  cd := TevClientDataSet.Create(Self);
  with TExecDSWrapper.Create('GenericSelect2CurrentWithCondition') do
  begin
    SetMacro('CONDITION', '('+ s+ ')');
    SetMacro('COLUMNS', 't1.CL_PERSON_NBR, t1.CUSTOM_EMPLOYEE_NUMBER, t1.EE_NBR, t2.FIRST_NAME, t2.LAST_NAME, t2.SOCIAL_SECURITY_NUMBER, t1.CURRENT_TERMINATION_CODE, t1.W2, t2.WEB_PASSWORD, t2.E_MAIL_ADDRESS');
    SetMacro('TABLE1', 'EE');
    SetMacro('TABLE2', 'CL_PERSON');
    SetMacro('JOINFIELD', 'CL_PERSON');
    cd.IndexFieldNames := 'EE_NBR';
    ctx_DataAccess.GetCustomData(cd, AsVariant);
  end;
  cdEe.DisableControls;
  cdEe.Filtered := False;
  try
    cd.First;
    while not cd.Eof do
    begin
      v := VarArrayOf([ClNbr, CoNbr, Consolidation, PeriodEnd, cd['CL_PERSON_NBR']]);
      if not cdEe.Locate('CL_NBR;CO_NBR;CONSOLIDATION;Period_End;CL_PERSON_NBR', v, []) then
      begin
        cdEe.Append;
        cdEe['CL_NBR;CO_NBR;CONSOLIDATION;Period_End;CL_PERSON_NBR'] := v;
        cdEe['FIRST_NAME;LAST_NAME;SSN;CUSTOM_EMPLOYEE_NUMBER;EE_NBR;CURRENT_TERMINATION_CODE;W2;WEB_PASSWORD;E_MAIL_ADDRESS;EE_NBR_LIST']
            := cd['FIRST_NAME;LAST_NAME;SOCIAL_SECURITY_NUMBER;CUSTOM_EMPLOYEE_NUMBER;EE_NBR;CURRENT_TERMINATION_CODE;W2;WEB_PASSWORD;E_MAIL_ADDRESS;EE_NBR'];
        cdEe['SELECTED'] := True;//ChangedAfter = 0;
        cdEe.Post;
      end
      else
      begin
        cdEe.Edit;
        cdEe['CUSTOM_EMPLOYEE_NUMBER'] := cdEe['CUSTOM_EMPLOYEE_NUMBER']+ '; '+ cd['CUSTOM_EMPLOYEE_NUMBER'];
        cdEe['EE_NBR_LIST'] := cdEe['EE_NBR_LIST']+ ', '+ IntToStr(cd['EE_NBR']);        
        cdEe.Post;
      end;
      cd.Next;
    end;
  finally
    cdEe.Filtered := True;
    cdEe.EnableControls;
    cd.Free;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.AddReport;
begin
  cdSelectedQueueAfterScroll(nil);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdSelectedQueueAfterScroll(
  DataSet: TDataSet);
var
  d: TDateTime;
begin
  if cdSelectedQueue.ControlsDisabled then
    Exit;
  tsEe.TabVisible :=  ((cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_1095_CORRECTED) or
                      ((cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED_EE) or
                       (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED_EE_EeCopy) or
                       (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED_EE_EeCopy_W2) or
                       (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_EE_EeCopy_1095B) or
                       (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_EE_EeCopy_1095C) )
                   and (cdSelectedQueue['PRODUCE_ASCII_FILE'] = GROUP_BOX_NO)) and not cbSimpleMode.Checked;
  tsEe.Enabled := tsEe.TabVisible;
  if tsEe.Enabled then
  begin
    lEeClient.Caption := 'Client: '+ ConvertNull(cdSelectedQueue['CL_NUMBER'], '');
    lEeCompany.Caption := 'Company: '+ ConvertNull(cdSelectedQueue['CO_NUMBER'], '')+ ' - '+ ConvertNull(cdSelectedQueue['CO_NAME'], '');
    lEeCons.Caption := ConvertNull(cdSelectedQueue['CONSOLIDATION_DESCR'], '');
    if lEeCons.Caption <> '' then
      lEeCons.Caption := 'Consolidation: '+ lEeCons.Caption;
    d := cdSelectedQueueStatusDate;
    bEeMarkChanged.Enabled := d <> 0;
    lEeStatusDate.Visible := bEeMarkChanged.Enabled;
    lEeStatusDate.Caption := 'Since '+ DateToStr(d);
  end;
  if tsEe.Enabled then
    FilterEe;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.bEeMarkAllClick(Sender: TObject);
begin
  inherited;
  cdEe.DisableControls;
  try
    cdEe.IndexName := '';
    cdEe.First;
    while not cdEe.Eof do
    begin
      cdEe.Edit;
      cdEe['SELECTED'] := Sender = bEeMarkAll;
      cdEe.CheckBrowseMode;
      cdEe.Next;
    end;
    cdEe.First;
  finally
    cdEe.EnableControls;
  end;
end;

function TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdSelectedQueueStatusDate: TDateTime;
begin
  if (cdSelectedQueue['STATUS'] = TAX_RETURN_STATUS_PROCESSED) then
    Result := cdSelectedQueue['STATUS_DATE']
  else
    Result := 0;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.bEeMarkChangedClick(
  Sender: TObject);
var
  v: Variant;
  i: Integer;
begin
  inherited;
  bEeMarkAllClick(nil); // unmark them all
  v := GetAffectedPersons(cdSelectedQueue['CL_NBR'], cdSelectedQueue['CO_NBR'],
    cdSelectedQueueStatusDate, cdSelectedQueue['PERIOD_END_DATE']);
  if not VarIsNull(v) then
  begin
    cdEe.DisableControls;
    try
      for i := VarArrayLowBound(v, 1) to VarArrayHighBound(v, 1) do
      begin
        if cdEe.Locate('CL_PERSON_NBR', v[i], []) then
        begin
          cdEe.Edit;
          cdEe['SELECTED'] := True;
          cdEe.Post;
        end
        else
          Assert(False, 'GetAffectedPersons returned unknown person');
      end;
      cdEe.First;
    finally
      cdEe.EnableControls;
    end;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.bManualReconClick(Sender: TObject);
var
  f: TEDIT_MANUAL_RECONCILED;
  d: TDataSet;
  i, ClNbr: integer;
begin
  inherited;
  ClNbr :=-1;
  f := TEDIT_MANUAL_RECONCILED.Create(nil);
  try
    if f.ShowModal = mrOk then
    begin
      d := gOneStepCoList.DataSource.DataSet;
      d.DisableControls;
      ctx_StartWait('Updating Manual Reconciled Fields...');
      try

          for i := 0 to gOneStepCoList.SelectedList.Count-1 do
          begin
            d.GotoBookmark(gOneStepCoList.SelectedList.Items[i]);
            if ClNbr <> d['CL_NBR'] then
            begin
              if ClNbr <> -1 then
                 ctx_DataAccess.PostDataSets([DM_COMPANY.CO]);
              ClNbr := d['CL_NBR'];
              ctx_DataAccess.OpenClient(ClNbr);
              DM_COMPANY.CO.DataRequired('ALL');
            end;

            if DM_COMPANY.CO.Active and DM_COMPANY.CO.Locate('CO_NBR',d['CO_NBR'],[]) and
              ((Trim(DM_COMPANY.CO.FieldByName('LAST_PREPROCESS_MESSAGE').AsString) <> '') or
                   (DM_COMPANY.CO.FieldByName('LAST_PREPROCESS').AsDateTime <> GetEndMonth(Trunc(cbDateTill.DateTime)))) then
            begin
               DM_COMPANY.CO.Edit;
               DM_COMPANY.CO.FieldValues['LAST_PREPROCESS_MESSAGE'] := MANUALLY_RECONCILED + '-' + f.edtMessage.Text;
               DM_COMPANY.CO.FieldByName('LAST_PREPROCESS').AsDateTime := GetEndMonth(Trunc(cbDateTill.DateTime));
               DM_COMPANY.CO.post;
            end;
          end;
          if ClNbr <> -1 then
             ctx_DataAccess.PostDataSets([DM_COMPANY.CO]);
      finally
        ctx_EndWait;
        d.EnableControls;
      end;
    end;
  finally
    FreeAndNil(f);
     if ClNbr <> -1 then
        btnApplySelectClick(sender);
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.bOneStepPreProcessClick(
  Sender: TObject);
var
  BegDate, EndDate: TDateTime;
  d: TDataSet;
  i, j: Integer;
  t: IevPreprocessQuarterEndTask;
  sTaxAdjLimit: Extended;
begin
  inherited;
  if gOneStepCoList.SelectedList.Count = 0 then
    raise EInvalidParameters.CreateHelp('Please, select at least one company first', IDH_InvalidParameters);
  mb_AppSettings['QuaterEndCleanupReportPath'] := edQePath.Text;
  mb_AppSettings['QuaterEndCleanupReports'] := BoolToYN(edQeCleanupReports.Checked);
  EndDate := GetEndMonth(Trunc(cbDateTill.DateTime));
  BegDate := GetBeginQuarter(EndDate);

  sTaxAdjLimit := 0;
  if edQeCleanupReports.Checked then
    if not EvNbrDialog('Tax Adjustment Threshold', 'Enter the Tax Adjustment Threshold amount', sTaxAdjLimit) then
      Exit;

  d := gOneStepCoList.DataSource.DataSet;
  d.DisableControls;
  ctx_StartWait('Creating task ...');
  try
    t := mb_TaskQueue.CreateTask(QUEUE_TASK_PREPROCESS_QUARTER_END, True) as IevPreprocessQuarterEndTask;
    t.BegDate := BegDate;
    t.EndDate := EndDate;
    t.QecAdjustmentLimit := StrToCurr(edQeCleanupLimit.Text);
    t.QecProduceReports := edQeCleanupReports.Checked;
    t.TaxAdjustmentLimit := sTaxAdjLimit;
    t.SendEmailNotification := cbEMail.Checked;
    t.NotificationEmail :=  eEMail.Text;
    t.EmailSendRule := TevEmailSendRule(cbEmailSendRule.ItemIndex);

    for i := 0 to gOneStepCoList.SelectedList.Count-1 do
    begin
      d.GotoBookmark(gOneStepCoList.SelectedList.Items[i]);
      if ConvertNull(d['CONSOLIDATION'], '') = '' then // skip consolidates
      begin
        j := t.Filter.Count- 1; // looking for duplicates
        while j >= 0 do
          with t.Filter[j] do
            if (ClNbr = d['CL_NBR'])
            and (CoNbr = d['CO_NBR'])
            and (Consolidation = False) then
              Break
            else
              Dec(j);
        if j < 0 then
          with t.Filter.Add do
          begin
            ClNbr := d['CL_NBR'];
            CoNbr := d['CO_NBR'];
            Consolidation := False;
            Caption := d['co_number'];
          end;
      end;
    end;

    if t.Filter.Count = 0 then
    with t.Filter.Add do
    begin
      ClNbr := -1;
      CoNbr := -1;
      Consolidation := False;
      Caption := '';
    end;

    ctx_EvolutionGUI.AddTaskQueue(t);

    RefreshFlagList;
  finally
    ctx_EndWait;
    d.EnableControls;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbSimpleModeClick(Sender: TObject);
begin
  inherited;
  if cbSimpleMode.Checked and cdQueue.Active then
    RefreshBriefViewList;
  TabSheet1.TabVisible := not cbSimpleMode.Checked;
  TabSheet2.TabVisible := not cbSimpleMode.Checked;
  tsXMLAggregation.TabVisible := not cbSimpleMode.Checked and (SecurityState <> ctEnabledReprintOnly);
  TabSheet3.TabVisible := not cbSimpleMode.Checked;
  tsCompanies.TabVisible := not cbSimpleMode.Checked;

  tsDetail.TabVisible := not cbSimpleMode.Checked;

  tsMEF.TabVisible := not cbSimpleMode.Checked;
  tsACAFilling.TabVisible := tsMEF.TabVisible;

  tsSimple.TabVisible := cbSimpleMode.Checked;
  tsSimpleMagmedia.TabVisible := cbSimpleMode.Checked;
  if cbSimpleMode.Checked then
  begin
    Pc.ActivatePage(tsSimple);
    cbFilter.OnChange(nil);
    cbMmFilter.OnChange(nil);
  end
  else
  begin
    Pc.ActivatePage(TabSheet1);
    cdSummary.RetrieveCondition := '';
  end;
  cbDateFrom.Enabled := not cbSimpleMode.Checked;
  cbDateIsDueDate.Enabled := not cbSimpleMode.Checked;
  if not cbDateIsDueDate.Enabled and cbDateIsDueDate.Checked then
    cbDateIsPeriodEndDate.Checked := True;
  cdSelectedQueueAfterScroll(nil);
  cbQuarters.Visible := cbSimpleMode.Checked;
  evLabel6.Visible := cbSimpleMode.Checked;
  Label2.Visible := not cbSimpleMode.Checked;
  Label3.Visible := not cbSimpleMode.Checked;
  Label19.Visible := not cbSimpleMode.Checked;
  DateKindSelectionPanel.Visible := not cbSimpleMode.Checked;
  cbDateFrom.Visible := not cbSimpleMode.Checked;
  cbDateTill.Visible := not cbSimpleMode.Checked;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.bOneStepRePrintClick(
  Sender: TObject);
  function CheckUnProcessed: Boolean;
  begin
    Result := False;
    with cdBriefReportList do
    begin
      First;
      while not Eof and not Result do
      begin
        Result := (FieldValues['STATUS'] <> TAX_RETURN_STATUS_PROCESSED) and (FieldValues['STATUS'] <> TAX_RETURN_STATUS_SHOULD_BE_REPROCESSED);
        Next;
      end;
    end;
  end;
var
  i: Integer;
  d: TDataSet;
  b: Boolean;
begin
  inherited;
  if gOneStepCoList.SelectedList.Count = 0 then
    raise EInvalidParameters.CreateHelp('Please, select at least one company first', IDH_InvalidParameters);
  cbPreviewOn.Checked := False;
  d := gOneStepCoList.DataSource.DataSet;
  BitBtn1.Click;
  d.DisableControls;
  cdBriefReportList.DisableControls;
  b := False;
  try
    ctx_StartWait('Checking selected companies...', gOneStepCoList.SelectedList.Count);
    try
      for i := 0 to gOneStepCoList.SelectedList.Count-1 do
      begin
        d.GotoBookmark(gOneStepCoList.SelectedList.Items[i]);
        begin
          if CheckUnProcessed then
          begin
            EvMessage(cdCompanyName(cdSummary)+ ' has unprocessed return(s)'#13'You can reprint processed returns only', mtWarning, [mbOK]);
            Continue;
          end;
          b := True;
          Button1.Click; // add the company
        end;
      end;
    finally
      ctx_EndWait;
    end;
    if b then
      btnReprintClick(nil);
    gOneStepCoList.UnselectAll;
  finally
    BitBtn1.Click;
    RefreshBriefViewList;
    if not b then
      EvMessage('You have to pick at least one pre-processed (green colored) company');
    cdBriefReportList.EnableControls;
    d.EnableControls;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbDateTillChange(Sender: TObject);
begin
  inherited;
  if cbSimpleMode.Checked and (cbDateTill.Tag = 0) then
  begin
    cbDateTill.Tag := 1;
    try
      cbDateTill.DateTime := GetEndMonth(cbDateTill.DateTime);
      cbDateFrom.DateTime := cbDateTill.DateTime;
    finally
      cbDateTill.Tag := 0;
    end;
  end;
  ParamsAreChanges := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbDateTillCloseUp(Sender: TObject);
begin
  inherited;
  cbDateTillChange(nil);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.gOneStepCoListCalcCellColors(
  Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
var
  s: string;
begin
  inherited;
  if Assigned(Field) then
    if Field.DataSet.Active
    and (Field.DataSet.RecordCount > 0) then
    begin

      s := MakeString([Field.DataSet['CL_NBR'], Field.DataSet['CO_NBR'], BoolToYN(Field.DataSet.FieldByName('CONSOLIDATION').AsString <> '')] , ';');



      if FFlagList.IndexOf(s) <> -1 then
      begin
        AFont.Color := clBlue;
        ABrush.Color := clYellow;
      end

      else if (Field.DataSet.FieldByName('PreProcessDate').AsDateTime = GetEndMonth(Trunc(cbDateTill.DateTime))) and
               (StartsWith(Trim(Field.DataSet.FieldByName('ErrorMessage').AsString), MANUALLY_RECONCILED)) then
      begin
        AFont.Color := clWhite;
        ABrush.Color :=  rgb(0,255,255)
      end

      else if (Field.DataSet.FieldByName('PreProcessDate').AsDateTime = GetEndMonth(Trunc(cbDateTill.DateTime)))
               and (Field.DataSet.FieldByName('ErrorMessage').AsString <> '') then
      begin
        AFont.Color := clWhite;
        ABrush.Color := clRed;
      end

      else if (Field.DataSet.FieldByName('PreProcessDate').AsDateTime = GetEndMonth(Trunc(cbDateTill.DateTime)))
               and (Field.DataSet.FieldByName('ErrorMessage').AsString = '') then
      begin
        AFont.Color := clWhite;
        ABrush.Color := clGreen;
      end;
    end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.bProcessClick(
  Sender: TObject);
var
  i: Integer;
  b: Boolean;
  s: string;
  bDontAskProcessed: Boolean;
begin
  inherited;

  cbMessages.Items.Clear;
  cbMessages.ScrollWidth := 0;
  cbMessages.Items.Add('Process started '+ DateTimeToStr(Now));
  BitBtn1.Click;
  cdSummary.DisableControls;
  cdBriefReportList.DisableControls;
  bDontAskProcessed := False;
  b := False;
  try

   if gOneStepCoList.SelectedList.Count > 0 then
   begin
    if not cbSettingsPrintAgencyCopy.Checked
      and not cbSettingsPrintSbCopy.Checked
      and not cbSettingsPrintClientCopy.Checked
      and (edCopies.Value = 0) then
        raise ECanNotPerformOperation.CreateHelp('You have to set copies you want to print first.', IDH_CanNotPerformOperation);

    ctx_StartWait('Building list of returns to print...');
    try
      for i := 0 to gOneStepCoList.SelectedList.Count-1 do
      begin
        cdSummary.GotoBookmark(gOneStepCoList.SelectedList.Items[i]);
        if not ((cdSummaryPreProcessDate.AsDateTime = GetEndMonth(Trunc(cbDateTill.DateTime)))
            and ( (cdSummaryErrorMessage.AsString = '') or
                    StartsWith(Trim(cdSummaryErrorMessage.AsString), MANUALLY_RECONCILED)) ) and not bDontAskProcessed then
          case EvMessage(cdCompanyName(cdSummary)+ ' hasn''t been pre-processed'#13'Do you really want to process it?',
            mtWarning, [mbYes, mbNo, mbYesToAll]) of
          mrYes: ;
          mrYesToAll:
            bDontAskProcessed := True;
          mrNo:
              Continue;
          end;
        begin
          ctx_DataAccess.OpenClient(cdSummary['CL_NBR']);
          s := '-1';
          with cdBriefReportList do
          begin
            First;
            while not Eof do
            begin
              if FieldValues['STATUS'] = TAX_RETURN_STATUS_PROCESSED then
                s := s+ ','+ IntToStr(FieldValues['CO_TAX_RETURN_QUEUE_NBR']);
              Next;
            end;
          end;
          if s <> '-1' then
          begin
            if (ctx_AccountRights.Functions.GetState('RETURN_DELETE') <> stEnabled) then
              raise ENoRights.CreateHelp('You have no security rights to reprocess returns', IDH_SecurityViolation);
            if EvMessage(cdCompanyName(cdSummary)+ ' has processed return(s)'#13'Do you really want to reprocess them?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
              Continue;
            DM_COMPANY.CO_TAX_RETURN_QUEUE.Close;
            DM_COMPANY.CO_TAX_RETURN_QUEUE.IndexName := '';
            DM_COMPANY.CO_TAX_RETURN_QUEUE.IndexFieldNames := '';
            DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR in ('+ s+ ')');
            DM_COMPANY.CO.DataRequired('');
            DM_COMPANY.CO_TAX_RETURN_QUEUE.DisableControls;
            try
              DM_COMPANY.CO_TAX_RETURN_QUEUE.First;
              while not DM_COMPANY.CO_TAX_RETURN_QUEUE.Eof do
              begin
                try
                  DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
                except
                  on E: Exception do
                  begin
                    E.Message := Format('Company #%s %s generated error: %s', [
                      DM_COMPANY.CO.ShadowDataSet.CachedLookup(DM_COMPANY.CO_TAX_RETURN_QUEUE.CO_NBR.AsInteger,
                        'CUSTOM_COMPANY_NUMBER'),
                      DM_COMPANY.CO.ShadowDataSet.CachedLookup(DM_COMPANY.CO_TAX_RETURN_QUEUE.CO_NBR.AsInteger,
                        'NAME'),
                      E.Message]);
                    raise;
                  end;
                end;
                DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] := TAX_RETURN_STATUS_SHOULD_BE_REPROCESSED;
                if cdQueue.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', VarArrayOf([ctx_DataAccess.ClientID, DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_TAX_RETURN_QUEUE_NBR']]), []) then
                begin
                  cdQueue.Edit;
                  cdQueue['STATUS'] := TAX_RETURN_STATUS_SHOULD_BE_REPROCESSED;
                  cdQueue.Post;
                end;
                DM_COMPANY.CO_TAX_RETURN_QUEUE.Post;
                DM_COMPANY.CO_TAX_RETURN_QUEUE.Next;
              end;
            finally
              DM_COMPANY.CO_TAX_RETURN_QUEUE.EnableControls;
            end;
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE]);
          end;
          b := True;
          Button1Click(nil); // add the company
        end;
      end;
    finally
      ctx_EndWait;
    end;
    if b then
      btnProcessClick(Sender);
    gOneStepCoList.UnselectAll;

   end
   else
      EvMessage('You have to pick at least one pre-processed (green colored) company');
  finally
    BitBtn1.Click;
    RefreshBriefViewList;
    cdBriefReportList.EnableControls;
    cdSummary.EnableControls;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.gOneStepCoListDblClick(
  Sender: TObject);
var
  d: TDataSet;
  dm: TBalanceDM;
begin
  inherited;
  d := gOneStepCoList.DataSource.DataSet;
  if (cdSummaryErrorMessage.AsString <> '')
  and (cdSummaryPreProcessDate.AsDateTime = GetEndMonth(Trunc(cbDateTill.DateTime))) then
  begin
    dm := Application.MainForm.FindComponent(TBalanceDM.ClassName + 'CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' + d.FieldByName('CO_NBR').AsString + BoolToStr[d.FieldByName('CONSOLIDATION').AsString<>''] + 'Q') as TBalanceDM;
    if not Assigned(dm) then
      dm := Application.MainForm.FindComponent(TBalanceDM.ClassName + 'CL' + IntToStr(ctx_DataAccess.ClientID) + 'CO' + d.FieldByName('CO_NBR').AsString + BoolToStr[d.FieldByName('CONSOLIDATION').AsString<>''] + 'A') as TBalanceDM;
    if (Pos(mOutOfBalance, cdSummaryErrorMessage.AsString) = 0) or
       not Assigned(dm) then
      EvMessage(cdSummaryErrorMessage.AsString)
    else
      with TOutOfBalanceError.Create(nil) do
      try
        Caption := cdSummaryCO_NAME.AsString;
        lblErrorMsg.Caption := cdSummaryErrorMessage.AsString;
        BalanceReportScreen.ActivateForQueue(dm);
        ShowModal;
      finally
        Free;
      end;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbDateIsPeriodEndDateClick(
  Sender: TObject);
begin
  inherited;
  if cbDateIsPeriodEndDate.Checked then
    Label2.Caption := 'Period end in';
  ParamsAreChanges := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbDateIsDueDateClick(
  Sender: TObject);
begin
  inherited;
  if cbDateIsDueDate.Checked then
    Label2.Caption := 'Due date in';
  ParamsAreChanges := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.edQeCleanupLimitChange(Sender: TObject);
begin
  inherited;
  try
    StrToFloat(edQeCleanupLimit.Text);
  except
    edQeCleanupLimit.Text := '0.25';
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.FilterEe;
var
  s: ShortString;
begin
  if ConvertNull(cdSelectedQueue['CONSOLIDATION_DESCR'], '') <> '' then
    s := 'TRUE'
  else
    s := 'FALSE';
  if not VarIsNull(cdSelectedQueue['CL_NBR']) then
  begin
    cdEe.Filter := 'CL_NBR='+ IntToStr(cdSelectedQueue['CL_NBR'])+ ' and CO_NBR='+ IntToStr(cdSelectedQueue['CO_NBR'])+
      ' and CONSOLIDATION = '+s+ ' and PERIOD_END='''+ DateToStr(cdSelectedQueue['PERIOD_END_DATE'])+ '''';
    cdEe.Filtered := True;
    if cdEe.RecordCount = 0 then
      AddEe(ConvertNull(cdSelectedQueue['CONSOLIDATION_DESCR'], '') <> '', cdSelectedQueue['CL_NBR'], cdSelectedQueue['CO_NBR'], cdSelectedQueue['PERIOD_END_DATE'], cdSelectedQueueStatusDate);
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.DeActivate;
begin
  Mainboard.GlobalCallbacks.Unsubscribe('', cetGlobalFlagEvent, TypeName + '.*');
  FreeAndNil(FFlagList);
  FreeAndNil(FConditionList);
  mb_AppSettings.AsBoolean[strSeparateReturnsWithBlankSheet] := cbSeparate.Checked;
  mb_AppSettings.AsBoolean[strAutoRefreshQueue] := cbRefreshQueue.Checked;
  ctx_RWLocalEngine.EndWork;
  inherited;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.RefreshBriefViewList;
var
  scdBriefReportListRetrCond, scdBriefMagmediaListIndexName: string;
  d: TevClientDataSet;
begin
  d := TevClientDataSet.Create(nil);
  try
    d.CloneCursor(cdQueue, True);
    d.IndexName := '';
    d.Filtered := True;
    try
      scdBriefReportListRetrCond := cdBriefReportList.RetrieveCondition;
      d.Filter := 'PRODUCE_ASCII_FILE = '''+ GROUP_BOX_NO+ '''';
      cdBriefReportList.Data := CreateLookupData(d);
    finally
      cdBriefReportList.RetrieveCondition := '';
      cdBriefReportList.RetrieveCondition := scdBriefReportListRetrCond;
    end;
    d.Filter := 'PRODUCE_ASCII_FILE = '''+ GROUP_BOX_YES+ '''';
    cdBriefMagmediaList.DisableControls;
    cdBriefMagmediaCompanyList.DisableControls;
    scdBriefReportListRetrCond := cdBriefMagmediaCompanyList.RetrieveCondition;
    cdBriefMagmediaCompanyList.RetrieveCondition := '';
    scdBriefMagmediaListIndexName := cdBriefMagmediaList.IndexName;
    cdBriefMagmediaList.IndexName := '2';
    try
      cdBriefMagmediaCompanyList.Data := CreateLookupData(d);
      if cdBriefMagmediaList.Active then
        cdBriefMagmediaList.EmptyDataSet
      else
        cdBriefMagmediaList.CreateDataSet;

      d.First;
      while not d.Eof do
      begin
          if d['SY_REPORTS_GROUP_NBR'] = 0 then
          begin
           if (cdBriefMagmediaList.IndexName <> '2' ) then
              cdBriefMagmediaList.IndexName := '2'
          end
          else
            if (cdBriefMagmediaList.IndexName <> '1' ) then
              cdBriefMagmediaList.IndexName := '1';

          if (((d['SY_REPORTS_GROUP_NBR'] <> 0) and
               (not cdBriefMagmediaList.FindKey([d['SY_REPORTS_GROUP_NBR']])))
             or
             ((d['SY_REPORTS_GROUP_NBR'] = 0) and
              (not cdBriefMagmediaList.FindKey([d['SY_GL_AGENCY_REPORT_NBR']]))))
             then
             cdBriefMagmediaList.AppendRecord([d['SY_REPORTS_NBR'], d['SY_REPORTS_DESC'], d['SY_REPORTS_GROUP_NBR'], d['GlAgencyFOName'], d['SY_GL_AGENCY_REPORT_NBR']]);

        d.Next;
      end;
      cdBriefMagmediaList.First;
    finally
      cdBriefMagmediaList.IndexName := scdBriefMagmediaListIndexName;
      cdBriefMagmediaCompanyList.RetrieveCondition := scdBriefReportListRetrCond;
      cdBriefMagmediaCompanyList.EnableControls;
      cdBriefMagmediaList.EnableControls;
    end;
  finally
    d.Free;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.bMultiEnlistClick(Sender: TObject);
begin
  inherited;
  if not Assigned(FCompanyMuliSelectScreen) then
    FCompanyMuliSelectScreen := TCompanyMuliSelectScreen.Create(Self);
  with FCompanyMuliSelectScreen do
    if ShowModal = mrOk then
    begin
      cbDetailCl.Enabled := False;
      cbDetailCo.Enabled := False;
    end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdDetailPeriodEndDateChange(
  Sender: TObject);
begin
  inherited;
  if cdQueue.State in [dsEdit, dsInsert] then
    cdDetailPeriodEndDate.UpdateRecord;
  CalcDueDate;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbFreqChange(Sender: TObject);
begin
  inherited;
  bPreProcess.Enabled := cbQuarterly.Checked;
  ParamsAreChanges := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdDetailPeriodEndDateExit(
  Sender: TObject);
begin
  inherited;
  if cdQueue.State in [dsEdit, dsInsert] then
    cdDetailPeriodEndDate.UpdateRecord;
  CalcDueDate;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdCoListCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  if DataSet.FieldByName('CL_CO_CONSOLIDATION_NBR').IsNull then
    DataSet['CONS'] := GROUP_BOX_NO
  else
    DataSet['CONS'] := GROUP_BOX_YES;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbTypeChange(Sender: TObject);
begin
  inherited;
  ParamsAreChanges := True;
  cbTypeNot.Enabled := cbType.ItemIndex <> 0;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbTaxServiceChange(
  Sender: TObject);
begin
  inherited;
  ParamsAreChanges := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbStatusChange(Sender: TObject);
begin
  inherited;
  ParamsAreChanges := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbDateFromChange(Sender: TObject);
begin
  inherited;
  ParamsAreChanges := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbFilterChange(Sender: TObject);
begin
  inherited;
  with cdSummary do
    case cbFilter.ItemIndex of
    0: RetrieveCondition := 'has_paper';
    1: RetrieveCondition := 'has_paper and PreProcessDate = '+ QuotedStr(DateToStr(GetEndMonth(Trunc(cbDateTill.DateTime)))) +
      ' and ErrorMessage <> '+ QuotedStr('') + ' and not ErrorMessage like ''%Manually Reconciled%''';
    2: RetrieveCondition := 'has_paper and PreProcessDate = '+ QuotedStr(DateToStr(GetEndMonth(Trunc(cbDateTill.DateTime)))) +
      ' and ErrorMessage = '+ QuotedStr('');
    3: RetrieveCondition := 'has_paper and ErrorMessage like ''%Manually Reconciled%''';
    4: RetrieveCondition := 'has_paper and PreProcessDate <> '+ QuotedStr(DateToStr(GetEndMonth(Trunc(cbDateTill.DateTime))));
    else
      Assert(False);
    end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.bPreProcessMouseDown(
  Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
  if Shift = [ssShift..ssLeft] then
  begin
    FForcePreprocess := (EvMessage('Do you want to force selected company to preprocess?', mtConfirmation, [mbYes, mbNo]) = mrYes);
    bPreProcess.Click;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbPreviewOnClick(Sender: TObject);
begin
  inherited;
  if cbPreviewOn.Checked then
    btnReprint.Caption := 'Preview'
  else
    btnReprint.Caption := 'Reprint';
end;

function TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdCompanyName(const ds: TevClientDataSet): string;
var
  FieldName: string;
begin
  FieldName := 'CONSOLIDATION_DESCR';
  if ds.FindField(FieldName) = nil then
    FieldName := 'CONSOLIDATION';
  if ConvertNull(ds[FieldName], '') = '' then
    Result := 'Company '
  else
    Result := 'Consolidation ';
  Result := Result+ ds['CO_NUMBER']+ ' - '+ ds['CO_NAME'];
end;

function TEDIT_TMP_CO_TAX_RETURN_QUEUE.CreateReportFileName(ReportName, Path: string): string;
begin
  Result := Path + StringReplace(StringReplace(StringReplace(
    ReportName, '/', '-', [rfReplaceAll]),
    ':', '-', [rfReplaceAll]), '\', '-', [rfReplaceAll]); // !!!
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnProcessClick(Sender: TObject);
var
  ReportSettings, ReportSettingsClone: TrwReportParams;
  QueueReportResults, EEQueueReportResults, r,rEE : TrwReportResults;
  ReportResult, EEReportResult: TrwReportResult;
  Rep : TrwRepParams;
  CompressedSignedStream, EECompressedSignedStream: IevDualStream;
  vCl, vCo, vCons, vClCo: Variant;
  sy_Report_Writer_Reports_Nbr : integer;
  sTag, sException : String;
  sIndexName, sFilter, tCondition : string;
  W2Count : integer;
  W2CountControl : Boolean;

  procedure CalcPeriodRange(var FBegDate, FEndDate: TDateTime);
  var
    newTime : TDateTime;
  begin
    case string(cdSelectedQueue['RETURN_FREQUENCY'])[1] of
    FREQUENCY_TYPE_SEMI_MONTHLY:
      if GetDay(cdSelectedQueue['PERIOD_END_DATE']) < 16 then
      begin
        FBegDate := GetBeginMonth(cdSelectedQueue['PERIOD_END_DATE']);
        FEndDate := FBegDate+ 14; // the 15th
      end
      else
      begin
        FBegDate := GetBeginMonth(cdSelectedQueue['PERIOD_END_DATE'])+ 15; // the 16th
        FEndDate := GetEndMonth(cdSelectedQueue['PERIOD_END_DATE']);
      end;
    FREQUENCY_TYPE_MONTHLY:
      begin
        FBegDate := GetBeginMonth(cdSelectedQueue['PERIOD_END_DATE']);
        FEndDate := GetEndMonth(cdSelectedQueue['PERIOD_END_DATE']);
      end;
    FREQUENCY_TYPE_QUARTERLY:
      begin
        FBegDate := GetBeginQuarter(cdSelectedQueue['PERIOD_END_DATE']);
        FEndDate := GetEndQuarter(cdSelectedQueue['PERIOD_END_DATE']);
      end;
    FREQUENCY_TYPE_ANNUAL:
      begin
        FBegDate := GetBeginYear(cdSelectedQueue['PERIOD_END_DATE']);
        FEndDate := GetEndYear(cdSelectedQueue['PERIOD_END_DATE']);
      end;
    else
      Assert(False);
    end;
    newTime := EncodeTime(0, 0, 0, 0);
    ReplaceTime(FBegDate, newTime);
    newTime := EncodeTime(23, 59, 59, 0);
    ReplaceTime(FEndDate, newTime);
  end;


  Procedure TaxReturnRunUpdate(var retNbr, eeNbr : integer; rResult : TrwReportResults; rCompressed: IevDualStream);
  Begin
      if DM_COMPANY.CO_TAX_RETURN_RUNS.Locate('CO_TAX_RETURN_QUEUE_NBR;EE_NBR', VarArrayOf([retNbr, eeNbr]), []) then
      begin
            if rResult[0].PagesInfo.Count <> 0 then
            begin
              DM_COMPANY.CO_TAX_RETURN_RUNS.Edit;
              DM_COMPANY.CO_TAX_RETURN_RUNS.UpdateBlobData('CL_BLOB_NBR', rCompressed);
              DM_COMPANY.CO_TAX_RETURN_RUNS.CHANGE_DATE.AsDateTime := SysTime;
            end
            else
              DM_COMPANY.CO_TAX_RETURN_RUNS.Delete;
      end
      else
      begin
            if rResult[0].PagesInfo.Count <> 0 then
            begin
              DM_COMPANY.CO_TAX_RETURN_RUNS.Append;
              DM_COMPANY.CO_TAX_RETURN_RUNS.CO_TAX_RETURN_QUEUE_NBR.AsInteger := retNbr;
              DM_COMPANY.CO_TAX_RETURN_RUNS.EE_NBR.AsInteger := eeNbr;
              DM_COMPANY.CO_TAX_RETURN_RUNS.UpdateBlobData('CL_BLOB_NBR', rCompressed);
              DM_COMPANY.CO_TAX_RETURN_RUNS.CHANGE_DATE.AsDateTime := SysTime;
            end;
      end;
      if DM_COMPANY.CO_TAX_RETURN_RUNS.State in [dsInsert, dsEdit] then
      begin
            DM_COMPANY.CO_TAX_RETURN_RUNS.Post;
            if W2CountControl then
            begin
              inc(W2Count);
              if W2Count > 500 then
              begin
                ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE, DM_COMPANY.CO_TAX_RETURN_RUNS]);
                W2Count := 0;
              end;
            end;
      end;
  end;

  procedure ClTaxReturnProcess;
  var
   i, j, k : Integer;
   pClNbr, iCoTaxRetNbr, iEeNbr: Integer;
   eeProcessed : Boolean;
  begin
     pClNbr := -1;
     QueueReportResults := nil;
     QueueReportResults := TrwReportResults.Create;
     EEQueueReportResults := TrwReportResults.Create;

     try
        QueueReportResults.SetFromStream(ctx_RWLocalEngine.EndGroup(rdtNone));

        DM_COMPANY.CO_TAX_RETURN_RUNS.DataRequired('CO_TAX_RETURN_QUEUE_NBR in ('+ tCondition+ ')');

        if Assigned(QueueReportResults)
        and (QueueReportResults.Count > 0) then
        begin
          for i := 0 to QueueReportResults.Count-1 do
          begin
            ReportResult := QueueReportResults[i];
            if (ReportResult.ErrorMessage = '')  then
            begin
              // No errors and a resultset from reportwriter
              // The next part seems to store that in the database
              sTag := ReportResult.Tag;
              pClNbr := StrToInt(Fetch(sTag));
              iCoTaxRetNbr := StrToInt(Fetch(sTag));
              iEeNbr := StrToIntDef(Fetch(sTag), -1);
              Assert(DM_COMPANY.CO_TAX_RETURN_QUEUE.Locate('CO_TAX_RETURN_QUEUE_NBR', iCoTaxRetNbr, []));
              Assert(cdSelectedQueue.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', VarArrayOf([pClNbr, iCoTaxRetNbr]), []));

              CompressedSignedStream := TEvDualStreamHolder.Create;
              r := TrwReportResults.Create;
              try
                if iEeNbr = -2 then
                begin
                  eeProcessed := False;
                  FilterEe;
                  cdEe.SaveState;
                  try

                    cdEe.Filter := cdEe.Filter+ ' and (selected = True) and W2 in (''B'',''A'',''P'')';
                    cdEe.First;
                    while not cdEe.Eof do
                    begin
                      ctx_RWLocalEngine.StartGroup;

                      ReportSettings := QueueReportResults[i].ReturnValues;
                      ReportSettings.SetValueIfParamExists('ProcessMode', 2);

                      ReportSettings.Add('PrintRights', True);
                      ReportSettings.Add('Employees', VarArrayOf([cdEe['CL_PERSON_NBR']]));

                      with ctx_RWLocalEngine.CalcPrintReport(cdSelectedQueue['SY_REPORT_WRITER_REPORTS_NBR'], CH_DATABASE_SYSTEM, ReportSettings, ConvertNull(cdSelectedQueue['SY_REPORTS_DESC'], '* No name *')) do
                        Tag := MakeString([ctx_DataAccess.ClientID, Integer(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR']), Integer(cdEe['EE_NBR'])]);

                      EEQueueReportResults.SetFromStream(ctx_RWLocalEngine.EndGroup(rdtNone));

                      if Assigned(EEQueueReportResults)
                      and (EEQueueReportResults.Count > 0) then
                      begin
                        for k := 0 to EEQueueReportResults.Count-1 do
                        begin
                          eeProcessed := True;
                          EEReportResult := EEQueueReportResults[k];
                          if (EEReportResult.ErrorMessage = '') and Assigned(EEReportResult.Data) then
                          begin
                            EECompressedSignedStream := TEvDualStreamHolder.Create;
                            rEE := TrwReportResults.Create;
                            try
                              sTag := EEReportResult.Tag;
                              Fetch(sTag);
                              iCoTaxRetNbr := StrToInt(Fetch(sTag));
                              iEeNbr := StrToIntDef(Fetch(sTag), -1);

                              EECompressedSignedStream.WriteInteger($FF);
                              rEE.Add.Assign(EEReportResult);
                              CompressStreamToStream(rEE.GetAsStream, EECompressedSignedStream);
                              EECompressedSignedStream.Position := 0;

                              TaxReturnRunUpdate(iCoTaxRetNbr, iEeNbr, rEE, EECompressedSignedStream);
                            finally
                              rEE.Free;
                              EECompressedSignedStream := nil;
                            end;
                          end;
                        end;
                      end;
                      cdEe.Next;
                    end;
                  finally
                    cdEe.LoadState;
                    if eeProcessed then
                    begin
                     DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
                     DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] := TAX_RETURN_STATUS_PROCESSED;
                     DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS_DATE'] := Now;
                    end;
                  end;
                end
                else
                begin
                  if Assigned(ReportResult.Data) then
                  begin

                   DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
                   DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] := TAX_RETURN_STATUS_PROCESSED;
                   DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS_DATE'] := Now;

                   CompressedSignedStream.WriteInteger($FF);
                   r.Add.Assign(ReportResult);
                   CompressStreamToStream(r.GetAsStream, CompressedSignedStream);
                   CompressedSignedStream.Position := 0;
                   if iEeNbr = -1 then
                   begin
                     DM_COMPANY.CO_TAX_RETURN_RUNS.Append;
                     DM_COMPANY.CO_TAX_RETURN_RUNS.CO_TAX_RETURN_QUEUE_NBR.AsInteger :=
                                DM_COMPANY.CO_TAX_RETURN_QUEUE.CO_TAX_RETURN_QUEUE_NBR.AsInteger;
                     DM_COMPANY.CO_TAX_RETURN_RUNS.UpdateBlobData('CL_BLOB_NBR', CompressedSignedStream);
                     DM_COMPANY.CO_TAX_RETURN_RUNS.CHANGE_DATE.AsDateTime := SysTime;
                     DM_COMPANY.CO_TAX_RETURN_RUNS.Post;

                   end
                   else
                     TaxReturnRunUpdate(iCoTaxRetNbr, iEeNbr,r, CompressedSignedStream);
                  end;
                end;
              finally
                r.Free;
                CompressedSignedStream := nil;
              end;

              if DM_COMPANY.CO_TAX_RETURN_QUEUE.Modified then
                DM_COMPANY.CO_TAX_RETURN_QUEUE.Post
              else
                DM_COMPANY.CO_TAX_RETURN_QUEUE.Cancel;

              Assert(cdSelectedQueue.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', VarArrayOf([pClNbr, iCoTaxRetNbr]), []));
              if not cdProcessed.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', VarArrayOf([pClNbr, iCoTaxRetNbr]), []) then
              begin
                 cdProcessed.Append;
                 for j := 0 to cdProcessed.FieldCount-1 do
                   cdProcessed.Fields[j].Assign(cdSelectedQueue.FieldByName(cdProcessed.Fields[j].FieldName));
                 cdProcessed.Post;
              end;
            end
            else
              if sException <> '' then
                sException := sException+ #13+ ReportResult.ErrorMessage
              else
                sException := ReportResult.ErrorMessage;
          end;
          if pClNbr <> -1 then
            ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE, DM_COMPANY.CO_TAX_RETURN_RUNS]);
        end;

     finally
       QueueReportResults.Free;
       EEQueueReportResults.Free;
     end;
  end;

var

  bFiltered, bDontAskProcessed, bEof: Boolean;
  ClNbr, RecNo, SyReportNbr, i : Integer;
  sBookMark, sBookMark2: TBookMarkStr;
  cdMagmediaList: TISBasicClientDataSet;
  FBegDate, FEndDate: TDateTime;
  ms: IisStream;
  EE_NBR_List: IisStringList;
begin
  inherited;

  CheckBeforeAction;
  cdAnswers.EmptyDataSet;
  if(Sender as TComponent).Tag = 0 then
  begin
     cbMessages.Items.Clear;
     cbMessages.ScrollWidth := 0;
     cbMessages.Items.Add('Process started '+ DateTimeToStr(Now));
  end;
  cdProcessed.EmptyDataSet;
  cdSelectedQueue.DisableControls;
  bDontAskProcessed := False;
  sIndexName := cdSelectedQueue.IndexName;
  sFilter := cdSelectedQueue.Filter;
  bFiltered := cdSelectedQueue.Filtered;
  EE_NBR_List := TisStringList.Create;
  try
    ctx_StartWait('Checking chosen companies...');
    cdSelectedQueue.IndexFieldNames := 'CL_NBR;CO_NBR';
    cdSelectedQueue.Filtered := False;
    try
      cdSelectedQueue.First;
      while not cdSelectedQueue.Eof do
      begin
        try
          CalcPeriodRange(FBegDate, FEndDate);
          if (cdSelectedQueue['RETURN_FREQUENCY'] <> FREQUENCY_TYPE_MONTHLY)  and
             (cdSelectedQueue['RETURN_FREQUENCY'] <> FREQUENCY_TYPE_SEMI_MONTHLY)  then
             CheckCoFlag(FEndDate);
        except
          On E: Exception do
            if cbDontPopupMessages.Checked then
              AddMessage(DateTimeToStr(Now)+ ' '+ cdCompanyName(cdSelectedQueue)+ ' '+ cdSelectedQueue['SY_REPORTS_DESC']+ ': '+ E.Message)
            else
              raise;
        end;
        cdSelectedQueue.Next;
      end;
    finally
      ctx_EndWait;
    end;
    // process paper returns
    ClNbr := -1;
    cdSelectedQueue.IndexFieldNames := 'CL_NBR';

    cdSelectedQueue.Filter :=
      'PRODUCE_ASCII_FILE in ('''+ GROUP_BOX_NO+ ''', '''+ GROUP_BOX_XML +''')';
    cdSelectedQueue.Filtered := True;
    cdSelectedQueue.First;
    if cdSelectedQueue.RecordCount > 0 then
      if not cbSettingsPrintAgencyCopy.Checked
      and not cbSettingsPrintSbCopy.Checked
      and not cbSettingsPrintClientCopy.Checked
      and (edCopies.Value = 0) then
        raise ECanNotPerformOperation.CreateHelp('You have to set copies you want to print first.', IDH_CanNotPerformOperation);
    ctx_StartWait('Calculating paper returns... Please wait...', cdSelectedQueue.RecordCount);
    ctx_StartWait('Preparing...');

       // special filter  to get all EE or seelcted EE List for Paper returns
    DM_SYSTEM.SY_REPORT_GROUP_MEMBERS.DataRequired('SY_REPORT_GROUPS_NBR in (28)');

    try
      sException := '';
      try
        W2CountControl := False;
        W2Count := 0;
        if cdSelectedQueue.RecordCount = 1 then
           W2CountControl := True;
        while not cdSelectedQueue.Eof do
        begin
          try
                if not bDontAskProcessed
                    and (Sender <> nil) // called from brief-mode screen - no questions asked
                     and (cdSelectedQueue['STATUS'] = TAX_RETURN_STATUS_PROCESSED) then
                  case EvMessage(cdCompanyName(cdSelectedQueue)+ ', '+ string(cdSelectedQueue['SY_REPORTS_DESC'])+ ' is already processed'#13'Do you really want to reprocess it?',
                              mtWarning, [mbYes, mbNo, mbYesToAll]) of
                  mrYes: ;
                  mrYesToAll:
                    bDontAskProcessed := True;
                  mrNo:
                    begin
                      cdSelectedQueue.Next;
                      Continue;
                    end;
                  end;

                if ClNbr <> cdSelectedQueue['CL_NBR'] then
                begin
                  if ClNbr <> -1 then
                  begin
                   sBookMark := cdSelectedQueue.Bookmark;
                   ClTaxReturnProcess;
                   cdSelectedQueue.Bookmark := sBookMark;
                  end;

                  ctx_DataAccess.OpenClient(cdSelectedQueue['CL_NBR']);
                  ClNbr := cdSelectedQueue['CL_NBR'];
                  RecNo := cdSelectedQueue.RecNo;
                  tCondition := '-1';
                  while (ClNbr = cdSelectedQueue['CL_NBR']) and not cdSelectedQueue.Eof do
                  begin
                    tCondition := tCondition+ ','+ IntToStr(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR']);
                    cdSelectedQueue.Next;
                  end;
                  cdSelectedQueue.RecNo := RecNo;
                  DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR in ('+ tCondition+ ')');

                  ctx_RWLocalEngine.StartGroup;
                end;

                Assert(DM_COMPANY.CO_TAX_RETURN_QUEUE.Locate('CO_TAX_RETURN_QUEUE_NBR', cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR'], []));
                if DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] = TAX_RETURN_STATUS_PROCESSED then
                begin
                  DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit; // testing for company lock date
                  DM_COMPANY.CO_TAX_RETURN_QUEUE.Cancel;
                end;

                ReportSettings := TrwReportParams.Create;
                try
                    CalcPeriodRange(FBegDate, FEndDate);
                    ReportSettings.Add('Clients', VarArrayOf([cdSelectedQueue['CL_NBR']]));
                    ReportSettings.Add('Companies', VarArrayOf([cdSelectedQueue['CO_NBR']]));
                    ReportSettings.Add('ClientCompany', VarArrayOf([cdSelectedQueue.FieldByName('CL_NBR').AsString + ':' + cdSelectedQueue.FieldByName('CO_NBR').AsString ]));
                    ReportSettings.Add('Consolidation', ConvertNull(cdSelectedQueue['CONSOLIDATION_DESCR'], '') <> '');
                    ReportSettings.Add('Consolidations', VarArrayOf([ReportSettings.ParamByName('Consolidation').Value]));

                    case cbEeSort.ItemIndex of
                      0: ReportSettings.Add('DetailSort', 3);  //Last Name
                      1: ReportSettings.Add('DetailSort', 1);  //SSN
                    else
                      Assert(False)
                    end;

                    if cbW2Sort.ItemIndex > 0 then
                      ReportSettings.Add('GroupSort', cbW2Sort.ItemIndex);

                    DM_PAYROLL.PR.DataRequired('CO_NBR = '+ IntToStr(cdSelectedQueue['CO_NBR'])+
                      ' and CHECK_DATE between '''+ DateToStr(FBegDate)+ ''' and '''+ DateToStr(FEndDate)+ ''''+
                      ' and STATUS in ('''+ PAYROLL_STATUS_PROCESSED+ ''','''+ PAYROLL_STATUS_VOIDED+ ''')');
                    ReportSettings.Add('Payrolls', GetVarArrayFieldValueList(DM_PAYROLL.PR, 'PR_NBR', varInteger, True));

                    ReportSettings.Add('PeriodInfo', 'P'+','+FloatToStr(FBegDate)+','+ FloatToStr(FEndDate));

                    ReportSettings.Add('Dat_b', FBegDate);
                    ReportSettings.Add('Dat_e', FEndDate);
                    ReportSettings.Add('ProcessDate', Now);
                    ReportSettings.Add('AgencyNbr', IntToStr(ConvertNull(cdSelectedQueue['SY_GLOBAL_AGENCY_NBR'], 0)));

                    ReportSettings.Add('AgencyFieldOfficeNbr', IntToStr(ConvertNull(cdSelectedQueue['SY_GL_AGENCY_FIELD_OFFICE_NBR'], 0)));

                    ReportSettings.Add('AskForRefund', cbSettingsCredit.ItemIndex = 1);
                    ReportSettings.FileName := ''; // no magmedia is expected
                    ReportSettings.Add('DueDate', cdSelectedQueue['DUE_DATE']);

                    ReportSettings.Add('SecureReport', cbHideSSN.Checked);

                    if cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED_EE_EeCopy_W2 then
                    begin
                     ReportSettings.Add('ProcessMode', 1);
                     Rep := ctx_RWLocalEngine.CalcPrintReport(cdSelectedQueue['SY_REPORT_WRITER_REPORTS_NBR'], CH_DATABASE_SYSTEM, ReportSettings, ConvertNull(cdSelectedQueue['SY_REPORTS_DESC'], '* No name *'));
                     Rep.Tag := MakeString([ctx_DataAccess.ClientID, Integer(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR']),'-2']);
                     Rep.ReturnParams := True;
                    end
                    else
                    if cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED_EE_EeCopy then
                    begin
                      FilterEe;
                      cdEe.SaveState;
                      try
                        cdEe.Filter := cdEe.Filter + ' and (selected = True) and W2 in (''B'',''A'',''P'')';
                        ReportSettingsClone := TrwReportParams.Create;
                        ReportSettingsClone.Assign(ReportSettings);
                        cdEe.First;
                        while not cdEe.Eof do
                        begin
                          ReportSettingsClone.Add('PrintRights', True);
                          ReportSettingsClone.Add('Employees', VarArrayOf([cdEe['CL_PERSON_NBR']]));
                          with ctx_RWLocalEngine.CalcPrintReport(cdSelectedQueue['SY_REPORT_WRITER_REPORTS_NBR'], CH_DATABASE_SYSTEM, ReportSettingsClone, ConvertNull(cdSelectedQueue['SY_REPORTS_DESC'], '* No name *')) do
                            Tag := MakeString([ctx_DataAccess.ClientID, Integer(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR']), Integer(cdEe['EE_NBR'])]);
                          cdEe.Next;
                        end;
                      finally
                        cdEe.LoadState;
                      end;
                    end
                    else
                    if (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_EE_EeCopy_1095B) or
                       (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_EE_EeCopy_1095C) then
                    begin
                      FilterEe;
                      cdEe.SaveState;
                      try
                        cdEe.Filter := cdEe.Filter + ' and (selected = True)';
                        ReportSettingsClone := TrwReportParams.Create;
                        ReportSettingsClone.Assign(ReportSettings);
                        cdEe.First;
                        while not cdEe.Eof do
                        begin
                            ReportSettingsClone.Add('PrintRights', True);
                            ReportSettingsClone.Add('Employees', VarArrayOf([cdEe['CL_PERSON_NBR']]));
                            with ctx_RWLocalEngine.CalcPrintReport(cdSelectedQueue['SY_REPORT_WRITER_REPORTS_NBR'], CH_DATABASE_SYSTEM, ReportSettingsClone, ConvertNull(cdSelectedQueue['SY_REPORTS_DESC'], '* No name *')) do
                              Tag := MakeString([ctx_DataAccess.ClientID, Integer(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR']), Integer(cdEe['EE_NBR'])]);
                           cdEe.Next;
                        end;
                      finally
                        cdEe.LoadState;
                      end;
                    end
                    else
                    if cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED_EE then
                    begin
                        FilterEe;
                        cdEe.SaveState;
                        try
                          if DM_SYSTEM.SY_REPORT_GROUP_MEMBERS.Locate('SY_REPORT_WRITER_REPORTS_NBR',cdSelectedQueue['SY_REPORT_WRITER_REPORTS_NBR'],[]) then
                          begin
                             cdEe.Filter := cdEe.Filter + ' and (selected = True)  and W2 in (''R'',''A'',''P'') ' ;
                             cdEe.Filtered :=true;
                          end;
                          if (cdEe.RecordCount > 0) then
                          begin
                             ReportSettings.Add('Employees', GetVarArrayFieldValueList(cdEe, 'CL_PERSON_NBR', varInteger, True));
                             with ctx_RWLocalEngine.CalcPrintReport(cdSelectedQueue['SY_REPORT_WRITER_REPORTS_NBR'], CH_DATABASE_SYSTEM, ReportSettings, ConvertNull(cdSelectedQueue['SY_REPORTS_DESC'], '* No name *')) do
                                 Tag := MakeString([ctx_DataAccess.ClientID, Integer(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR'])]);
                          end;
                        finally
                          cdEe.LoadState;
                        end;
                    end
                    else
                    if cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_1095_CORRECTED then
                    begin
                        FilterEe;
                        cdEe.SaveState;
                        try
                          cdEe.Filter := cdEe.Filter + ' and (selected = True) ' ;
                          cdEe.Filtered :=true;
                          ReportSettings.Add('Employees', GetVarArrayFieldValueList(cdEe, 'CL_PERSON_NBR', varInteger, True));
                          with ctx_RWLocalEngine.CalcPrintReport(cdSelectedQueue['SY_REPORT_WRITER_REPORTS_NBR'], CH_DATABASE_SYSTEM, ReportSettings, ConvertNull(cdSelectedQueue['SY_REPORTS_DESC'], '* No name *')) do
                               Tag := MakeString([ctx_DataAccess.ClientID, Integer(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR'])]);
                        finally
                          cdEe.LoadState;
                        end;
                    end
                    else
                      with ctx_RWLocalEngine.CalcPrintReport(cdSelectedQueue['SY_REPORT_WRITER_REPORTS_NBR'], CH_DATABASE_SYSTEM, ReportSettings, ConvertNull(cdSelectedQueue['SY_REPORTS_DESC'], '* No name *')) do
                        Tag := MakeString([ctx_DataAccess.ClientID, Integer(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR'])]);

                finally
                  ReportSettings.Free;
                end;

          except
            on E: Exception do
            begin
              if cbDontPopupMessages.Checked then
                AddMessage(DateTimeToStr(Now)+ ' '+ cdCompanyName(cdSelectedQueue)+ ' '+ cdSelectedQueue['SY_REPORTS_DESC']+ ': '+ E.Message)
              else
                if EvMessage(cdCompanyName(cdSelectedQueue)+ ' '+ string(cdSelectedQueue['SY_REPORTS_DESC'])+ ' stopped with message:'#13+ E.Message+ #13'Do you want to continue with next return?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
                begin
                  e.Message := cdCompanyName(cdSelectedQueue)+ ' '+ cdSelectedQueue['SY_REPORTS_DESC']+ #13+ E.Message;
                  raise;
                end;
            end;
          end;

          cdSelectedQueue.Next;
        end;
      finally
        ctx_UpdateWait;

        if ClNbr <> -1 then
           ClTaxReturnProcess;

      end;

    finally
      ctx_EndWait;
      ctx_EndWait;

      if cdProcessed.RecordCount > 0 then
      begin
        PrintProcessed(cdProcessed, cbPreviewOn.Checked);
        cdProcessed.First;
        while not cdProcessed.Eof do
        begin
          Assert(cdSelectedQueue.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', cdProcessed['CL_NBR;CO_TAX_RETURN_QUEUE_NBR'], []));
          cdSelectedQueue.Edit;
          cdSelectedQueue['AGENCY_COPY_PRINTED;SB_COPY_PRINTED;CLIENT_COPY_PRINTED'] := cdProcessed['AGENCY_COPY_PRINTED;SB_COPY_PRINTED;CLIENT_COPY_PRINTED'];
          if cdSelectedQueue['STATUS'] <> TAX_RETURN_STATUS_PROCESSED then
            cdSelectedQueue['STATUS'] := TAX_RETURN_STATUS_PROCESSED;
          cdSelectedQueue.Post;
          cdProcessed.Next;
        end;
      end;
    end;

    if sException <> '' then
      if cbDontPopupMessages.Checked then
        cbMessages.Items.Text := cbMessages.Items.Text+ sException
      else
        raise EErrorMessageException.CreateHelp(sException, IDH_ReportCalcException);



    // process magmedia returns
    cdSelectedQueue.IndexFieldNames := 'SY_REPORTS_NBR;CL_NBR;CO_TAX_RETURN_QUEUE_NBR';
    cdSelectedQueue.Filter := 'PRODUCE_ASCII_FILE = '''+ GROUP_BOX_YES+ '''';
    cdSelectedQueue.Filtered := True;
    if cdSelectedQueue.RecordCount > 0 then
    begin
      cdMagmediaList := TISBasicClientDataSet.Create(Self);
      cdMagmediaList.FieldDefs.Add('CL_NBR', ftInteger);
      cdMagmediaList.FieldDefs.Add('NBR', ftInteger);
      cdMagmediaList.CreateDataSet;
      cdMagmediaList.LogChanges := False;
      ctx_RWLocalEngine.StartGroup;
      ctx_StartWait('Calculating magmedia returns... Please wait...', cdSelectedQueue.RecordCount);
      ctx_StartWait;
      try
        i := 0;
        bEof := False;
        while not cdSelectedQueue.Eof and not bEof do
        begin
          try
            ctx_UpdateWait(string(cdSelectedQueue['SY_REPORTS_DESC']), i);
            SyReportNbr := cdSelectedQueue['SY_REPORTS_NBR'];
            sBookMark := cdSelectedQueue.Bookmark;
            cdMagmediaList.EmptyDataSet;

            ReportSettings := TrwReportParams.Create;
            try
              vCl := Null;
              CalcPeriodRange(FBegDate, FEndDate);

              ReportSettings.Add('Dat_b', FBegDate);
              ReportSettings.Add('Dat_e', FEndDate);
              ReportSettings.Add('ProcessDate', Now);
              ReportSettings.Add('WagesFilter', cbWagesFilter.ItemIndex);
              ReportSettings.Add('AgencyNbr', IntToStr(ConvertNull(cdSelectedQueue['SY_GLOBAL_AGENCY_NBR'], 0)));

//  Maybe This one will be add        
//             ReportSettings.Add('AgencyFieldOfficeNbr', IntToStr(ConvertNull(cdSelectedQueue['SY_GL_AGENCY_FIELD_OFFICE_NBR'], 0)));


              ReportSettings.Add('AskForRefund', cbSettingsCredit.ItemIndex = 1);
              ReportSettings.FileName := CreateReportFileName(cdSelectedQueue['SY_REPORTS_DESC'], cbSettingsMagmediaPath .Text);
              ReportSettings.Add('DueDate', cdSelectedQueue['DUE_DATE']);
              ReportSettings.Add('Consolidation', not VarIsNull(cdSelectedQueue['CONSOLIDATION_DESCR']));


              sy_Report_Writer_Reports_Nbr := cdSelectedQueue['SY_REPORT_WRITER_REPORTS_NBR'];

              while (SyReportNbr = cdSelectedQueue['SY_REPORTS_NBR']) and not cdSelectedQueue.Eof do
              begin
                Inc(i);
                if not bDontAskProcessed
                and (Sender <> nil) // called from brief-mode screen - no questions asked
                and (cdSelectedQueue['STATUS'] = TAX_RETURN_STATUS_PROCESSED) then
                  case EvMessage(cdCompanyName(cdSelectedQueue)+ ', '+ string(cdSelectedQueue['SY_REPORTS_DESC'])+ ' is already processed'#13'Do you really want to reprocess it?',
                    mtWarning, [mbYes, mbNo, mbYesToAll]) of
                  mrYes: ;
                  mrYesToAll:
                    bDontAskProcessed := True;
                  mrNo:
                    begin
                      cdSelectedQueue.Next;
                      Continue;
                    end;
                  end;

                  if VarIsNull(vCl) then
                  begin
                    vCl := VarArrayOf([cdSelectedQueue['CL_NBR']]);
                    vCo := VarArrayOf([cdSelectedQueue['CO_NBR']]);
                    vClCo := VarArrayOf([cdSelectedQueue.FieldByName('CL_NBR').AsString + ':' + cdSelectedQueue.FieldByName('CO_NBR').AsString]);
                    vCons := VarArrayOf([not VarIsNull(cdSelectedQueue['CONSOLIDATION_DESCR'])]);
                  end
                  else
                  begin
                    VarArrayRedim(vCl, VarArrayHighBound(vCl, 1)+ 1);
                    VarArrayRedim(vCo, VarArrayHighBound(vCo, 1)+ 1);
                    VarArrayRedim(vClCo, VarArrayHighBound(vClCo, 1)+ 1);
                    VarArrayRedim(vCons, VarArrayHighBound(vCons, 1)+ 1);
                    vCl[VarArrayHighBound(vCl, 1)] := cdSelectedQueue['CL_NBR'];
                    vCo[VarArrayHighBound(vCo, 1)] := cdSelectedQueue['CO_NBR'];
                    vClCo[VarArrayHighBound(vClCo, 1)] := cdSelectedQueue.FieldByName('CL_NBR').AsString + ':' + cdSelectedQueue.FieldByName('CO_NBR').AsString;
                    vCons[VarArrayHighBound(vCons, 1)] := not VarIsNull(cdSelectedQueue['CONSOLIDATION_DESCR']);
                  end;
                  cdMagmediaList.AppendRecord([cdSelectedQueue['CL_NBR'], cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR']]);
                  cdSelectedQueue.Next;

              end;
              ReportSettings.Add('Clients', vCl);
              ReportSettings.Add('Companies', vCo);
              ReportSettings.Add('ClientCompany', vClCo);
              ReportSettings.Add('Consolidations', vCons);

              ctx_RWLocalEngine.CalcPrintReport(sy_Report_Writer_Reports_Nbr, CH_DATABASE_SYSTEM, ReportSettings);

              bEof := cdSelectedQueue.Eof;
              sBookMark2 := cdSelectedQueue.Bookmark;
              cdSelectedQueue.Bookmark := sBookMark;
              try
                while (SyReportNbr = cdSelectedQueue['SY_REPORTS_NBR']) and not cdSelectedQueue.Eof do
                begin
                  if cdMagmediaList.Locate('CL_NBR;NBR', cdSelectedQueue['CL_NBR;CO_TAX_RETURN_QUEUE_NBR'], []) then
                  begin
                    ctx_DataAccess.OpenClient(cdSelectedQueue['CL_NBR']);
                    DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR='+ IntToStr(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR']));
                    Assert(DM_COMPANY.CO_TAX_RETURN_QUEUE.RecordCount = 1);
                    DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
                    DM_COMPANY.CO_TAX_RETURN_QUEUE['STATUS'] := TAX_RETURN_STATUS_PROCESSED;
                    DM_COMPANY.CO_TAX_RETURN_QUEUE.Post;
                    ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE]);
                    cdSelectedQueue.Edit;
                    cdSelectedQueue['STATUS'] := TAX_RETURN_STATUS_PROCESSED;
                    cdSelectedQueue.Post;
                  end;
                  cdSelectedQueue.Next;
                end;
              except
                cdSelectedQueue.Bookmark := sBookMark2;
                raise;
              end;
            finally
              ReportSettings.Free;
            end;
          except
            on E: Exception do
            begin
              if cbDontPopupMessages.Checked then
                AddMessage(DateTimeToStr(Now)+ ' Magmedia '+ cdSelectedQueue['SY_REPORTS_DESC']+ ': '+ E.Message)
              else
                if EvMessage('Magmedia '+ string(cdSelectedQueue['SY_REPORTS_DESC'])+ ' stopped with message:'#13+ E.Message+ #13'Do you want to continue with next return?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
                begin
                  e.Message := 'Magmedia '+ cdSelectedQueue['SY_REPORTS_DESC']+ #13+ E.Message;
                  raise;
                end;
            end;
          end;
        end;
      finally

          ms := ctx_RWLocalEngine.EndGroup(rdtNone);

          QueueReportResults := TrwReportResults.Create;
          QueueReportResults.SetFromStream(ms);
          sException := '';
          try
           if Assigned(QueueReportResults)
            and (QueueReportResults.Count > 0) then
           begin
             for i := 0 to QueueReportResults.Count-1 do
             begin
               ReportResult := QueueReportResults[i];
               if (ReportResult.ErrorMessage <> '') or not Assigned(ReportResult.Data) then
                if sException <> '' then
                  sException := sException+ #13+ ReportResult.ErrorMessage
                else
                  sException := ReportResult.ErrorMessage;
             end;
           end;
          finally
           QueueReportResults.Free;
           if sException <> '' then
            if cbDontPopupMessages.Checked then
             cbMessages.Items.Text := cbMessages.Items.Text+ sException
            else
             raise EErrorMessageException.CreateHelp(sException, IDH_ReportCalcException);
          end;

          if Assigned(ms) then
            ctx_RWLocalEngine.Print(ms, cbPrinter.Text);
          ctx_EndWait;
          ctx_EndWait;
          cdMagmediaList.Free;
      end;
    end;
  finally
    cdSelectedQueue.Filter := sFilter;
    cdSelectedQueue.Filtered := bFiltered;
    cdSelectedQueue.IndexName := sIndexName;
    cdSelectedQueue.EnableControls;
    cbMessages.Items.Add('Process ended '+ DateTimeToStr(Now));
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.CheckBeforeAction;
begin
  if ParamsAreChanges then
    raise ECanNotPerformOperation.CreateHelp('You changed filter parameters. Please, click Select to refresh queue before printing.', IDH_CanNotPerformOperation);
  if (cbSettingsMagmediaPath.Text = '')
  or not ForceDirectories(cbSettingsMagmediaPath.Text) then
    raise EInvalidParameters.CreateHelp('Wrong path for magmedia files', IDH_InvalidParameters);
  if Copy(cbSettingsMagmediaPath.Text, Length(cbSettingsMagmediaPath.Text), 1) <> '\' then
    cbSettingsMagmediaPath.Text := cbSettingsMagmediaPath.Text+ '\';
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.CheckCoFlag(
  const PERIOD_END_DATE: TDateTime);
  function TestCondition: Boolean;
  var
   tmpDate : TDateTime;
   newTime : TDateTime;
  begin
    ctx_DataAccess.OpenClient(cdSelectedQueue['CL_NBR']);
    DM_COMPANY.CO.DataRequired('ALL');
    Assert(DM_COMPANY.CO.Locate('CO_NBR', cdSelectedQueue['CO_NBR'], []));
    tmpDate := PERIOD_END_DATE;
    newTime := EncodeTime(0, 0, 0, 0);
    ReplaceTime(tmpDate, newTime);
    Result := (DM_COMPANY.CO.FieldByName('LAST_SUI_CORRECTION').AsDateTime <> tmpDate)
    or ((tmpDate = GetEndYear(tmpDate))
      and (DM_COMPANY.CO.FieldByName('LAST_FUI_CORRECTION').AsDateTime <> tmpDate));
  end;
var
  b: Boolean;
const
  Mess = 'Company is not ready to print returns. You need to pre-process this company first.';
  Mess2 = #13'Do you still want to continue and process/reprint returns for the company?';
begin
  if cdAnswers.Locate('CL_NBR;CO_NBR', cdSelectedQueue['CL_NBR;CO_NBR'], []) then
    b := cdAnswers['STOP']
  else
  begin
    b := TestCondition;
    if not cbDontPopupMessages.Checked then
    begin
      b := b and (EvMessage(cdCompanyName(cdSelectedQueue)+ ': '+ Mess+ Mess2, mtWarning, [mbYes, mbNo]) <> mrYes);
    end;
    cdAnswers.AppendRecord([cdSelectedQueue['CL_NBR'], cdSelectedQueue['CO_NBR'], b]);
  end;
  if b then
      raise ECanNotPerformOperation.CreateFmtHelp(Mess, [cdSelectedQueue['CO_NAME']], IDH_CanNotPerformOperation)
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.PrintProcessed(const ds: TevClientDataSet; const PreView: Boolean);
var
  ReportResults: TrwReportResults;
  ReportResultsVMR: TrwReportResults;
  BlankPage: TrwReportResults;
  Condition: string;

  procedure PrintBlank;
  begin
    if cbSeparate.Checked then
      ReportResults.AddReportResult('-- clear sheet --', rtUnknown, BlankPage[0].Data);
  end;
  procedure PrepareBlank;
  var
    StandartReportSettings: TrwReportParams;
  begin
    if not cbSeparate.Checked then Exit;
    if rwrBlankNumber > 0 then
    begin
      ctx_RWLocalEngine.StartGroup;
      StandartReportSettings := TrwReportParams.Create;
      try
        ctx_RWLocalEngine.CalcPrintReport(rwrBlankNumber, CH_DATABASE_SYSTEM, StandartReportSettings);
      finally
        StandartReportSettings.Free;
        BlankPage := TrwReportResults.Create;
        BlankPage.SetFromStream(ctx_RWLocalEngine.EndGroup(rdtNone));
      end;
    end;
  end;
  function AgencyCopyOn: Boolean;
  begin
    Result := cbSettingsPrintAgencyCopy.Checked
  end;
  function SbCopyOn: Boolean;
  begin
    Result := cbSettingsPrintSbCopy.Checked
  end;
  function ClientCopyOn: Boolean;
  begin
    Result := cbSettingsPrintClientCopy.Checked
  end;

  procedure Process(const i: Integer);
  var
    lFilteredEe: TevClientDataSet;
    BlobStream: IisStream;

    procedure SubProcess(const Level: Char; const AssignVMR: Boolean);
    var
      Result: TrwReportResult;
      i: Integer;
      r: TrwReportResults;
      Layers: TrwPrintableLayers;
    begin
      case Level of
        'A':      Layers := [0, 1];
        'B':      Layers := [0, 2];
        'C':      Layers := [0, 2, 3];
      else
        Layers := [];
      end;

      if (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_EE_EeCopy_1095B) or
         (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_EE_EeCopy_1095C) or
         (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED_EE_EeCopy) or
         (cdSelectedQueue['SYSTEM_TAX_TYPE'] = TAX_RETURN_TYPE_FED_EE_EeCopy_W2) then
      begin
        if AssignVMR then
        begin
          DM_COMPANY.CO_TAX_RETURN_RUNS.Close;

          DM_COMPANY.CO_TAX_RETURN_RUNS.RetrieveCondition := 'CO_TAX_RETURN_QUEUE_NBR in ('+ Condition+ ')';
          ctx_DataAccess.OpenDataSets([DM_COMPANY.CO_TAX_RETURN_RUNS]);

          FilterEe;
          lFilteredEe := CreateLookupDataset(cdEe, Self);
          lFilteredEe.Filter := '(selected = True)';
          lFilteredEe.Filtered := True;
          case cbEeSort.ItemIndex of
            0: lFilteredEe.IndexFieldNames := 'LAST_NAME';  //Last Name
            1: lFilteredEe.IndexFieldNames := 'SSN';  //SSN
          else
            Assert(False)
          end;

          lFilteredEe.First;
          while not lFilteredEe.Eof do
          begin
            if DM_COMPANY.CO_TAX_RETURN_RUNS.Locate('CO_TAX_RETURN_QUEUE_NBR;EE_NBR', VarArrayOf([ds['CO_TAX_RETURN_QUEUE_NBR'], lFilteredEe['EE_NBR']]), []) then
            begin
              if ( (lFilteredEe.FieldByName('WEB_PASSWORD').AsString <> '') and
                   (lFilteredEe.FieldByName('E_MAIL_ADDRESS').AsString <> '') ) then
              begin
                Result := ReportResultsVMR.Add;
                BlobStream := DM_COMPANY.CO_TAX_RETURN_RUNS.GetBlobData('CL_BLOB_NBR');
                i := BlobStream.ReadInteger;
                Assert(i = $FF); // new format
                r := TrwReportResults.Create;
                try
                  r.SetFromStream(UnCompressStreamFromStream(BlobStream));
                  Assert(r.Count > 0);
                  Result.Assign(r[0]);
                finally
                  r.Free;
                end;
                BlobStream := nil;

                Result.ReportName := ds['SY_REPORTS_DESC']+ '-'+ ds['co_number']+ '-'+ lFilteredEe[lFilteredEe.IndexFieldNames];
                Result.Layers := Layers;
                Result.TaxReturnVmrTag := MakeString([ctx_DataAccess.ClientID, DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_NBR'], lFilteredEe['EE_NBR']], ';');
                Result.VmrCoNbr := DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_NBR'];
                Result.VmrEventDate := cbDateTill.DateTime;
                Result.VmrConsolidation := ConvertNull(cdSelectedQueue['CONSOLIDATION_DESCR'], '') <> '';
              end;
            end;
            lFilteredEe.Next;
          end;
        end;
      end
      else
      begin
        Result := ReportResults.Add;
        i := BlobStream.ReadInteger;
        if i = $FF then // new format
        begin
          r := TrwReportResults.Create;
          try
            r.SetFromStream(UnCompressStreamFromStream(BlobStream));
            Assert(r.Count > 0);
            Result.Assign(r[0]);
          finally
            r.Free;
          end;
        end
        else
        begin
          Result.ReportType := TReportType(i);
          Result.Data := TEvDualStreamHolder.Create;
          InflateStream(BlobStream.RealStream, Result.Data.RealStream);
        end;
        BlobStream := nil;

        Result.ReportName := ds['SY_REPORTS_DESC']+ '-'+ ds['co_number'];
        Result.Layers := Layers;
        if AssignVMR then
        begin
          Result.TaxReturnVmrTag := MakeString([ctx_DataAccess.ClientID, DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_NBR']], ';');
          Result.VmrCoNbr := DM_COMPANY.CO_TAX_RETURN_QUEUE['CO_NBR'];
          Result.VmrEventDate := cbDateTill.DateTime;
          Result.VmrConsolidation := ConvertNull(cdSelectedQueue['CONSOLIDATION_DESCR'], '') <> '';
        end;
      end;
    end;

  begin
    ctx_UpdateWait(cdCompanyName(ds)+ #13+ string(ds['SY_REPORTS_DESC']));
    try
      Assert(DM_COMPANY.CO_TAX_RETURN_QUEUE.Locate('CO_TAX_RETURN_QUEUE_NBR', ds['CO_TAX_RETURN_QUEUE_NBR'], []));
      Assert(cdSelectedQueue.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', ds['CL_NBR;CO_TAX_RETURN_QUEUE_NBR'], []));

      BlobStream := nil;
      if (cdSelectedQueue['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_EE_EeCopy_1095B) and
         (cdSelectedQueue['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_EE_EeCopy_1095C) and
         (cdSelectedQueue['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE_EeCopy) and
         (cdSelectedQueue['SYSTEM_TAX_TYPE'] <> TAX_RETURN_TYPE_FED_EE_EeCopy_W2) then
      begin
        if not ds.FieldByName('CL_BLOB_NBR').IsNull then
          BlobStream:= GetClBlobData(ds.FieldByName('CL_BLOB_NBR').AsInteger)
        else
          BlobStream := GetClBlobData(GetTaxReturnLastClBlobNbr(DM_COMPANY.CO_TAX_RETURN_QUEUE.FieldByName('CO_TAX_RETURN_QUEUE_NBR').AsInteger));
      end;

      if ds['PRODUCE_ASCII_FILE'] = GROUP_BOX_XML then begin
        SubProcess('A', False);
        DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
        DM_COMPANY.CO_TAX_RETURN_QUEUE['AGENCY_COPY_PRINTED'] := GROUP_BOX_YES;
      end else begin
        case i of
        1: if ds['HAVE_ACOPY'] = GROUP_BOX_NO then
           begin
             DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
             DM_COMPANY.CO_TAX_RETURN_QUEUE['AGENCY_COPY_PRINTED'] := GROUP_BOX_YES;
           end
           else
           if AgencyCopyOn then
           begin
             SubProcess('A', False);
             DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
             DM_COMPANY.CO_TAX_RETURN_QUEUE['AGENCY_COPY_PRINTED'] := GROUP_BOX_YES;
           end;
        2: if ds['HAVE_SCOPY'] = GROUP_BOX_NO then
           begin
             DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
             DM_COMPANY.CO_TAX_RETURN_QUEUE['SB_COPY_PRINTED'] := GROUP_BOX_YES;
           end
           else
           if SbCopyOn then
           begin
             SubProcess('B', False);
             DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
             DM_COMPANY.CO_TAX_RETURN_QUEUE['SB_COPY_PRINTED'] := GROUP_BOX_YES;
           end;
        3: if ds['HAVE_CCOPY'] = GROUP_BOX_NO then
           begin
             DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
             DM_COMPANY.CO_TAX_RETURN_QUEUE['CLIENT_COPY_PRINTED'] := GROUP_BOX_YES;
           end
           else
           if ClientCopyOn then
           begin
             SubProcess('C', True);
             DM_COMPANY.CO_TAX_RETURN_QUEUE.Edit;
             DM_COMPANY.CO_TAX_RETURN_QUEUE['CLIENT_COPY_PRINTED'] := GROUP_BOX_YES;
           end;
        else
          SubProcess('B', False);
        end;
      end;
      if DM_COMPANY.CO_TAX_RETURN_QUEUE.State = dsEdit then
        if DM_COMPANY.CO_TAX_RETURN_QUEUE.Modified then
        begin
          DM_COMPANY.CO_TAX_RETURN_QUEUE.Post;
          ds.Edit;
          ds['AGENCY_COPY_PRINTED;SB_COPY_PRINTED;CLIENT_COPY_PRINTED'] := DM_COMPANY.CO_TAX_RETURN_QUEUE['AGENCY_COPY_PRINTED;SB_COPY_PRINTED;CLIENT_COPY_PRINTED'];
          ds.Post;
        end
        else
          DM_COMPANY.CO_TAX_RETURN_QUEUE.Cancel;
    except
      On E: Exception do
        if cbDontPopupMessages.Checked then
          AddMessage(DateTimeToStr(Now)+ ' '+ cdCompanyName(ds)+ ' '+ ds['SY_REPORTS_DESC']+ ': '+ E.Message)
        else
        if EvMessage(cdCompanyName(ds)+ ' '+ string(ds['SY_REPORTS_DESC'])+ ' failed to reprint with message:'#13+ E.Message+ #13'Do you want to continue with next return?', mtConfirmation, [mbYes, mbNo]) <> mrYes then
        begin
          e.Message := cdCompanyName(ds)+ ' '+ ds['SY_REPORTS_DESC']+ #13+ E.Message;
          raise;
        end;
    end;
  end;


var
  ClNbr, CoNbr, SyReportNbr: Integer;
  BookMark: TBookmarkStr;
  i, j: Integer;
  HasXMLReports: boolean;
  VMRResult : boolean;
begin
  HasXMLReports := False;
  PrepareBlank;
  ReportResults := TrwReportResults.Create;
  ReportResultsVMR := TrwReportResults.Create;
  ctx_StartWait('Printing paper returns... Please wait...', ds.RecordCount);
  ctx_StartWait;
  try
    ds.First;
    while not ds.Eof do
    begin
      if ds['PRODUCE_ASCII_FILE']= GROUP_BOX_XML then
        HasXMLReports := True;
      ds.Next;
    end;

    case cbSettingsPrintOrder.ItemIndex of
    0:
      begin
        j := 0;
        ds.IndexFieldNames := 'co_number;cl_nbr;co_nbr;PRIORITY2;CO_TAX_RETURN_QUEUE_NBR';
        ds.First;
        while not ds.Eof do
        begin
          ctx_UpdateWait('', j);
          BookMark := ds.Bookmark;
          ClNbr := ds['CL_NBR'];
          CoNbr := ds['CO_NBR'];
          FConditionList.Clear;
          while (ClNbr = ds['CL_NBR']) and (CoNbr = ds['CO_NBR']) and not ds.Eof do
          begin
            FConditionList.Append(IntToStr(ds['CO_TAX_RETURN_QUEUE_NBR']));
            Inc(j);
            ds.Next;
          end;
          ds.Bookmark := BookMark;
          Condition := FConditionList.CommaText;
          ctx_DataAccess.OpenClient(ClNbr);

          DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR in ('+ Condition+ ')');
          DM_COMPANY.CO_TAX_RETURN_QUEUE.First;

          i := 1; //TS: This to avoid compiler warnings...
          if ds['PRODUCE_ASCII_FILE']= GROUP_BOX_XML then
          begin
            ds.Bookmark := BookMark;
            while (ClNbr = ds['CL_NBR']) and (CoNbr = ds['CO_NBR']) and not ds.Eof do
            begin
              Process(i);
              ds.Next;
            end;
          end
          else
          begin
            for i := 1 to 3+ edCopies.Value do
            begin
              ds.Bookmark := BookMark;
              while (ClNbr = ds['CL_NBR']) and (CoNbr = ds['CO_NBR']) and not ds.Eof do
              begin
                Process(i);
                ds.Next;
              end;
            end;
          end;
          Sleep(1000);
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE]);
          if not HasXMLReports then PrintBlank;
        end;
      end;
    1:
      begin
        j := 0;
        ds.IndexFieldNames := 'PRIORITY2;SY_REPORTS_DESC;co_number;cl_nbr;co_nbr;CO_TAX_RETURN_QUEUE_NBR';
        ds.First;
        SyReportNbr := ds['SY_REPORTS_NBR'];
        while not ds.Eof do
        begin
          ctx_UpdateWait('', j);
          if SyReportNbr <> ds['SY_REPORTS_NBR'] then
          begin
            SyReportNbr := ds['SY_REPORTS_NBR'];
            if not HasXMLReports then PrintBlank;

          end;
          ctx_DataAccess.OpenClient(ds['CL_NBR']);
          DM_COMPANY.CO_TAX_RETURN_QUEUE.DataRequired('CO_TAX_RETURN_QUEUE_NBR='+ IntToStr(ds['CO_TAX_RETURN_QUEUE_NBR']));

          Condition:= IntToStr(ds['CO_TAX_RETURN_QUEUE_NBR']);

          Assert(DM_COMPANY.CO_TAX_RETURN_QUEUE.RecordCount = 1);
          for i := 1 to 3+ edCopies.Value do
          begin
            Process(i);
          end;
          Sleep(1000);
          ctx_DataAccess.PostDataSets([DM_COMPANY.CO_TAX_RETURN_QUEUE]);
          Inc(j);
          ds.Next;
        end;
      end;
    else
      Assert(False)
    end;
  finally
    try
      if PreView then
        ctx_RWLocalEngine.Preview(ReportResults)
      else
      begin
          ReportResults.Descr := 'Tax Returns for '+ DateToStr(cbDateTill.DateTime);
          ReportResultsVMR.Descr := 'Tax Returns for '+ DateToStr(cbDateTill.DateTime);
          if (evUseVMR.Checked) and (ctx_VmrEngine <> nil) then
          begin
            VMRResult := ctx_VmrEngine.VmrPostProcessReportResult(ReportResults);
            VMRResult := ( ctx_VmrEngine.VmrPostProcessReportResult(ReportResultsVMR) or VMRResult );
            if VMRResult then
              AddMessage(DateTimeToStr(Now)+ ' '+ cdCompanyName(ds)+ ': Some of the returns have been submitted to mail room');
          end;

          ctx_RWLocalEngine.Print(ReportResults, cbPrinter.Text);
      end;
    finally
      ctx_EndWait;
      ctx_EndWait;
      BlankPage.Free;
      ReportResults.Free;
      ReportResultsVMR.Free;
    end;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnReprintClick(Sender: TObject);
var
  sIndexName, sFilter: string;
  bFiltered: Boolean;
  cdClone: TevClientDataSet;
begin
  inherited;
  CheckBeforeAction;
  if not cbSettingsPrintAgencyCopy.Checked
  and not cbSettingsPrintSbCopy.Checked
  and not cbSettingsPrintClientCopy.Checked
  and (edCopies.Value = 0) then
    raise ECanNotPerformOperation.CreateHelp('You have to set copies you want to print first.', IDH_CanNotPerformOperation);
  cdSelectedQueue.DisableControls;
  sIndexName := cdSelectedQueue.IndexName;
  sFilter := cdSelectedQueue.Filter;
  bFiltered := cdSelectedQueue.Filtered;
  try
  // reprint paper ones
    cdSelectedQueue.Filter := 'PRODUCE_ASCII_FILE IN ('''+ GROUP_BOX_NO +
      ''', ''' + GROUP_BOX_XML + ''') ' +
      'and (STATUS = '''+ TAX_RETURN_STATUS_PROCESSED + ''' or STATUS = ''' +
      TAX_RETURN_STATUS_SHOULD_BE_REPROCESSED + ''') and ' +
      'SYSTEM_TAX_TYPE <> ''' + TAX_RETURN_TYPE_EE_EeCopy_1095B + ''' and ' +
      'SYSTEM_TAX_TYPE <> ''' + TAX_RETURN_TYPE_EE_EeCopy_1095C + ''' and ' +
      'SYSTEM_TAX_TYPE <> ''' + TAX_RETURN_TYPE_FED_EE_EeCopy + ''' and ' +
      'SYSTEM_TAX_TYPE <> ''' + TAX_RETURN_TYPE_FED_EE_EeCopy_W2 + '''';


    cdSelectedQueue.Filtered := True;
    cdClone := CreateLookupDataset(cdSelectedQueue, Self);
    PrintProcessed(cdClone, cbPreviewOn.Checked);
    cdClone.Free;

  finally
    cdSelectedQueue.Filter := sFilter;
    cdSelectedQueue.Filtered := bFiltered;
    cdSelectedQueue.IndexName := sIndexName;
    cdSelectedQueue.EnableControls;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbQuartersChange(Sender: TObject);
var
  d: TDateTime;
  y, m: Word;
begin
  inherited;
  d := GetBeginMonth(GetBeginMonth(Date)-1)-1;
  y := GetYear(d)- 1+ cbQuarters.ItemIndex div 4;
  m := (cbQuarters.ItemIndex mod 4 * 3)+ 1;
  d := EncodeDate(y, m, 1);
  cbDateTill.DateTime := GetEndQuarter(d);
  cbDateFrom.DateTime := GetBeginQuarter(d);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.gBriefMagmediaCompanyListCreateHintWindow(
  Sender: TObject; HintWindow: TwwGridHintWindow; AField: TField; R: TRect;
  var WordWrap: Boolean; var MaxWidth, MaxHeight: Integer;
  var DoDefault: Boolean);
begin
  MaxHeight := MaxHeight* 12;
  inherited;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.gBriefMagmediaCompanyListCalcCellColors(
  Sender: TObject; Field: TField; State: TGridDrawState;
  Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  inherited;
  if Assigned(Field) then
    if Field.DataSet.Active
    and (Field.DataSet.RecordCount > 0) then
      case Field.DataSet.FieldByName('Color').AsInteger of
      0: ;
      1:
        begin
          AFont.Color := clWhite;
          ABrush.Color := clRed;
        end;
      2:
        begin
          AFont.Color := clWhite;
          ABrush.Color := clGreen;
        end;
      3:
        begin
          AFont.Color := clWhite;
          ABrush.Color :=  rgb(0,255,255)
        end;
      end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.evButton1Click(Sender: TObject);
var
  i, GroupNbr: Integer;
  b: Boolean;
  bookmark: TBookmark;
  bDontAskProcessed: Boolean;
  bDontAskProcessedComp: TISBasicClientDataSet;
  cdCompanyList : TevClientDataSet;
begin
  inherited;

  GroupNbr := cdBriefMagmediaList['SY_GL_AGENCY_REPORT_NBR'];
  BitBtn1.Click;

  //   Master - Detail logic doesnt work when we use DisableControl command...
  //  I used filter instead of Master Detail Logic in this procedure...

  cdCompanyList := TevClientDataset.Create(nil);
  cdCompanyList.CloneCursor(cdBriefMagmediaCompanyList, False);
  cdCompanyList.MasterSource := nil;
  cdCompanyList.MasterFields := '';

  cdBriefMagmediaList.DisableControls;
  cdBriefMagmediaCompanyList.DisableControls;
  bDontAskProcessedComp := TISBasicClientDataSet.Create(nil);
  bDontAskProcessed := False;
  b := False;
  try
    bDontAskProcessedComp.FieldDefs.Add('CL_NBR', ftInteger);
    bDontAskProcessedComp.FieldDefs.Add('CO_NBR', ftInteger);
    bDontAskProcessedComp.FieldDefs.Add('DO', ftBoolean);
    bDontAskProcessedComp.CreateDataSet;
    bDontAskProcessedComp.AddIndex('1', 'CL_NBR;CO_NBR', [ixUnique]);
    bDontAskProcessedComp.IndexDefs.Update;
    bDontAskProcessedComp.IndexName := '1';
    ctx_StartWait('Building list of returns to create...');
    try
      cdBriefMagmediaList.First;
      while not cdBriefMagmediaList.Eof do
      begin
        bookmark := cdBriefMagmediaList.GetBookmark;
        try
          for i := 0 to gBriefMagmediaList.SelectedList.Count-1 do
            if cdBriefMagmediaList.CompareBookmarks(bookmark, gBriefMagmediaList.SelectedList.Items[i]) = 0 then
            begin
              if  (cdBriefMagmediaList['SY_REPORTS_GROUP_NBR'] <> 0 ) then
               cdCompanyList.Filter:= 'SY_REPORTS_GROUP_NBR' + ' = ' + IntToStr(cdBriefMagmediaList['SY_REPORTS_GROUP_NBR'])
              else
               cdCompanyList.Filter:= 'SY_GL_AGENCY_REPORT_NBR' + ' = ' + IntToStr(cdBriefMagmediaList['SY_GL_AGENCY_REPORT_NBR']);

              cdCompanyList.Filtered := True;
              cdCompanyList.First;
              while not cdCompanyList.Eof do
              begin
                if (cdCompanyList['Color'] <> 2)
                and not bDontAskProcessed then
                  if bDontAskProcessedComp.FindKey([cdCompanyList['CL_NBR'], cdCompanyList['CO_NBR']]) then
                  begin
                    if not bDontAskProcessedComp['DO'] then
                    begin
                      cdCompanyList.Next;
                      Continue;
                    end;
                  end
                  else
                    case EvMessage(cdCompanyName(cdCompanyList)+ ' hasn''t been pre-processed'#13'Do you really want to process it?',
                      mtWarning, [mbYes, mbNo, mbYesToAll]) of
                    mrYes:
                      bDontAskProcessedComp.AppendRecord([cdCompanyList['CL_NBR'], cdCompanyList['CO_NBR'], True]);
                    mrYesToAll:
                      bDontAskProcessed := True;
                    mrNo:
                      begin
                        bDontAskProcessedComp.AppendRecord([cdCompanyList['CL_NBR'], cdCompanyList['CO_NBR'], False]);
                        cdCompanyList.Next;
                        Continue;
                      end;
                    end;
                b := True;
                Assert(cdQueue.Locate('CL_NBR;CO_TAX_RETURN_QUEUE_NBR', cdCompanyList['CL_NBR;CO_TAX_RETURN_QUEUE_NBR'], []));
                BitBtn3Click(nil);
                cdCompanyList.Next;
              end;
            end;
        finally
          cdBriefMagmediaList.FreeBookmark(bookmark);
        end;
        cdBriefMagmediaList.Next;
      end;
    finally
      ctx_EndWait;
    end;
    if b then
      btnProcessClick(Sender);
    gBriefMagmediaList.UnselectAll;
  finally
    BitBtn1.Click;
    RefreshBriefViewList;

    cdBriefMagmediaList.Locate('SY_GL_AGENCY_REPORT_NBR', GroupNbr, []);

    cdBriefMagmediaCompanyList.Filter:= '';
    cdBriefMagmediaCompanyList.Filtered := False;
    cdBriefMagmediaList.EnableControls;
    cdBriefMagmediaCompanyList.EnableControls;
    bDontAskProcessedComp.Free;
    cdCompanyList.Free;

    if not b then
      EvMessage('You did not process anything');
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdBriefMagmediaCompanyListCalcFields(
  DataSet: TDataSet);
var
  ErrMessage: string;
  PreProcessData: TDateTime;
begin
  inherited;
  Assert(cdStatuses.FindKey([DataSet['cl_nbr'], DataSet['co_nbr']]));
  if ConvertNull(DataSet['consolidation_descr2'], '') <> '' then
  begin
    ErrMessage := ConvertNull(cdStatuses['LAST_PREPROCESS_MESSAGE_CONS'], '');
    PreProcessData := ConvertNull(cdStatuses['LAST_PREPROCESS_CONS'], 0);
  end
  else
  begin
    ErrMessage := ConvertNull(cdStatuses['LAST_PREPROCESS_MESSAGE'], '');
    PreProcessData := ConvertNull(cdStatuses['LAST_PREPROCESS'], 0);
  end;
  DataSet['Color'] := 0;
  if (PreProcessData = GetEndMonth(Trunc(cbDateTill.DateTime))) and
      StartsWith(Trim(ErrMessage), MANUALLY_RECONCILED) then
    DataSet['Color'] := 3
  else
  if (PreProcessData = GetEndMonth(Trunc(cbDateTill.DateTime)))
  and (ErrMessage <> '') then
    DataSet['Color'] := 1
  else
  if (PreProcessData = GetEndMonth(Trunc(cbDateTill.DateTime)))
  and (ErrMessage = '') then
    DataSet['Color'] := 2;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbMmFilterChange(Sender: TObject);
begin
  inherited;
  case cbMmFilter.ItemIndex of
  0:
    cdBriefMagmediaCompanyList.RetrieveCondition := '';
  1:
    cdBriefMagmediaCompanyList.RetrieveCondition := 'COLOR = 1';
  2:
    cdBriefMagmediaCompanyList.RetrieveCondition := 'COLOR = 2';
  3:
    cdBriefMagmediaCompanyList.RetrieveCondition := 'COLOR = 3';
  4:
    cdBriefMagmediaCompanyList.RetrieveCondition := 'COLOR = 0';
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.CheckEnableDeleteFunction;
begin
  btnDel.Enabled := (cdQueue.State = dsBrowse)
    and (SecurityState <> ctEnabledReprintOnly)
    and (cdQueue.RecordCount > 0)
    and (PC.ActivePage = TabSheet1);
  cbMultiDelete.Enabled := btnDel.Enabled;
end;


function TEDIT_TMP_CO_TAX_RETURN_QUEUE.GenericCalcDueDate(
  const GlAgencyNbr: Integer; const GlAgencyReportNbr: Integer; const EndDate: TDateTime): TDateTime;
begin
  if DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT.Locate('SY_GL_AGENCY_REPORT_NBR', GlAgencyReportNbr, []) then
  begin
    Result := CalculateReturnDueDate(
      GlAgencyNbr,
      DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT['RETURN_FREQUENCY'],
      DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT['WHEN_DUE_TYPE'],
      EndDate,
      DM_SYSTEM_MISC.SY_GL_AGENCY_REPORT['NUMBER_OF_DAYS_FOR_WHEN_DUE'],);
  end
  else
    Result := 0;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.SetParamsAreChanges(
  const Value: Boolean);
begin
  FParamsAreChanges := Value;
  if Value then
    btnApplySelect.Caption := 'Select'
  else
    btnApplySelect.Caption := 'Refresh';
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbTypeNotClick(Sender: TObject);
begin
  inherited;
  ParamsAreChanges := True;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnSendClick(Sender: TObject);
begin
  inherited;
  SendErrorMessageAndScreen(cbMessages.Items.Text, '', DateTimeToStr(Now), True);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.dsSummaryDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
  if cdSummary.Active
  and not cdSummary.ControlsDisabled
  and not VarIsNull(cdSummary['cl_nbr'])
  and cbSimpleMode.Checked then
    begin
      if cdSummary.FieldByName('consolidation').IsNull then
        cdBriefReportList.RetrieveCondition := 'cl_nbr='+ IntToStr(cdSummary['cl_nbr'])+
          ' and co_nbr='+ IntToStr(cdSummary['co_nbr'])+
          ' and consolidation_descr2 is null'
      else
        cdBriefReportList.RetrieveCondition := 'cl_nbr='+ IntToStr(cdSummary['cl_nbr'])+
          ' and co_nbr='+ IntToStr(cdSummary['co_nbr'])+
          ' and consolidation_descr2='+ QuotedStr(cdSummary.FieldByName('consolidation').AsString);
      lBriefReturns.Caption := 'Returns to print for '+ cdSummary['CO_NAME'];
    end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.OnGetSecurityStates(const States: TSecurityStateList);
const
  secScreenReprintOnly: TAtomContextRecord = (Tag: ctEnabledReprintOnly; Caption: 'Enabled Reprint Only'; IconName: ''; Priority: 200);
begin
  with secScreenReprintOnly do
    States.InsertAfter(ctEnabled, Tag[1], Caption, IconName);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.SetReadOnly(Value: Boolean);
begin
  inherited;
  if SecurityState = ctEnabledReprintOnly then
  begin
    btnEnlist.Enabled := False;
    btnAdd.Enabled := False;
    btnDel.Enabled := False;
    btnOk.Enabled := False;
    bMultiEnlist.Enabled := False;
    bPreProcess.Enabled := False;
    bProcess.Enabled := False;
    btnProcess.Enabled := False;
    evButton1.Enabled := False;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.InitXMLEnvelopeReportsDS;
begin
  with TExecDSWrapper.Create('GenericSelectCurrentWithCondition') do
  begin
    SetMacro('Columns', 'SY_REPORT_WRITER_REPORTS_NBR nbr, report_description');
    SetMacro('TableName', 'SY_REPORT_WRITER_REPORTS');
    SetMacro('Condition', '( ANCESTOR_CLASS_NAME=''TrmlRXTaxReturnEnvelope'' OR ANCESTOR_CLASS_NAME=''TrmlRATaxReturnEnvelope'') ');

    ctx_DataAccess.GetSyCustomData(cdXMLEnvelopeReports, AsVariant);
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnRunReportClick(Sender: TObject);
var
  ReportResults: TrwReportResults;
  Params: TrwReportParams;
  mr: TModalResult;
  lFileName: string;
  F: TEvFileStream;
  ms: IisStream;
begin
  cdXMLEnvelopeReports.DisableControls;
  try
    lFileName := ChangeFileExt(CreateReportFileName(
        cdXMLEnvelopeReports.FieldByName('report_description').AsString + '_' + FormatDateTime('mm-dd-yy_hh-mm-ss', Now),
        cbSettingsMagmediaPath.Text), '.xml');
    Params := TrwReportParams.Create;
    try
      Params.Add('SetupMode', False);
      Params.Add('ReportName', cdXMLEnvelopeReports.FieldByName('report_description').AsString);

      mr := ctx_RWLocalEngine.ShowModalInputForm(
        cdXMLEnvelopeReports.FieldByName('nbr').AsInteger, 'S', Params, True, False);

      if mr in [mrOk, mrNone] then
      begin
        ctx_RWLocalEngine.StartGroup;
        try
          ctx_RWLocalEngine.CalcPrintReport(cdXMLEnvelopeReports.FieldByName('nbr').AsInteger,
            'S', Params,
            cdXMLEnvelopeReports.FieldByName('report_description').AsString);
        finally
          ms := ctx_RWLocalEngine.EndGroup(rdtNone);
        end;
      end;

    finally
      Params.Free;
    end;
  finally
    cdXMLEnvelopeReports.EnableControls;
  end;

  ReportResults := TrwReportResults.Create;
  try
    ReportResults.SetFromStream(ms);
    if (ReportResults.Count > 0) then
    begin
      F := TEvFileStream.Create(lFileName, fmCreate);
      try
        F.CopyFrom(ReportResults[0].Data.RealStream, ReportResults[0].Data.RealStream.Size);
      finally
        F.Free;
      end;
    end;
  finally
    ReportResults.Free;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.LoadComaniesXFilter(isFilterList:iIsStringList);
const
  COMLIMIT = 50;
var
 Condition, CheckAnd : String;
 oDSwrap : IExecDSWrapper;
 iMidDS : IevDataSetHolder;
 SpecCondition : String;
 j: integer;
 jLimit: integer;
 sPartialCondition: String;
 k: integer;
begin

  Condition :='';
  CheckAnd := '';
  sCondition :='';

  if Length(Trim(edCusClNbr.Text))> 0 then
     begin
      Condition := Condition + ' LOWER(t1.Custom_client_Number) like LOWER('''+'%'+Trim(edCusClNbr.Text)+ '%'''+')';
      CheckAnd :=' and';
     end;

  if Length(Trim(edClName.Text))> 0 then
     begin
      Condition := Condition + CheckAnd + ' LOWER(t1.Name) like LOWER('''+'%'+Trim(edClName.Text)+ '%'''+')';
      CheckAnd :=' and';
     end;

  if Length(Trim(edCusCoNbr.Text))> 0 then
     begin
      Condition := Condition + CheckAnd + ' LOWER(t2.Custom_company_Number) like LOWER('''+'%'+Trim(edCusCoNbr.Text)+ '%'''+')';
      CheckAnd :=' and';
     end;

  if Length(Trim(edCoName.Text))> 0 then
      Condition := Condition + CheckAnd + ' LOWER(t2.Name) like LOWER('''+'%'+Trim(edCoName.Text)+ '%'''+')';


  if Length(Trim(Condition))= 0 then
         Condition := ' t1.Custom_client_Number like ''%'''
    else sCondition := ' and '+ Condition;

  Condition := Condition + GetReadOnlyClintCompanyFilter(Length(Trim(Condition))<> 0,'t1.','t2.');


    oDSWrap := TExecDSWrapper.Create('GenericSelect2WithCondition');// do
    oDSWrap.Macros.AddValue('COLUMNS', 't1.Custom_Client_Number,t1.Name ClientName,t2.custom_company_number,t2.name,t1.cl_nbr,t2.co_nbr ');
    oDSWrap.Macros.AddValue('TABLE1', 'TMP_CL');
    oDSWrap.Macros.AddValue('TABLE2', 'TMP_CO');
    oDSWrap.Macros.AddValue('JOINFIELD', 'CL');

  if NOT Assigned( isFilterList ) then
  begin
        oDSWrap.Macros.AddValue('Condition', Condition );
        ctx_DataAccess.GetTmpCustomData(cdCompanyFilter, oDSWrap);
  end
  else
  begin
     // collect first item of the list to initialize structures
     if isFilterList.Count > 0 then
     begin
       SpecCondition := Condition +' and t2.custom_company_number='''+isFilterList[0]+'''';
       oDSWrap.Macros.AddValue('Condition', SpecCondition );
       ctx_DataAccess.GetTmpCustomData(cdCompanyFilter, oDSWrap);
     end;
// collect the rest of the list ...
//     Load all the rest by chunks of COMLIMIT size
     iMidDS := TevDataSetHolder.Create(nil);
      k := 0;
      j:=0;
      jLimit := 1 + COMLIMIT - 1;
      while j < isFilterList.Count - 1 do
      begin
        sPartialCondition := ' and t2.custom_company_number in ( ';
        while (j < jLimit) and (j < isFilterList.Count - 1) do
        begin
            k := k + 1; j := j + 1;
            sPartialCondition := sPartialCondition + ''''+isFilterList[j]+''',';
        end;
        sPartialCondition := LeftStr(sPartialCondition, Length(sPartialCondition) - 1) +' )';
        if k > 0 then
        begin
            SpecCondition := Condition +sPartialCondition;
            oDSWrap.Macros.Value['Condition'] := SpecCondition ;
            ctx_DataAccess.GetTmpCustomData(iMidDS.Dataset, oDSWrap);
            iMidDS.DataSet.CopyRecords(iMidDS.Dataset,cdCompanyFilter,0,True,True);
        end;
        jLimit := jLimit + COMLimit-1;
        k := 0;
      end;
  end;
end;


procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnCompanyFilterClick(Sender: TObject);
begin
  LoadComaniesXFilter(nil);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.tsCompFilterGroupSelectClick(
  Sender: TObject);
begin
  inherited;

  grdCompFilter.Enabled := tsCompFilterGroupSelect.Checked;
  if grdCompFilter.Enabled then
    grdCompFilter.Color := clWindow
  else
    grdCompFilter.Color := clBtnFace;

  PopupLoadCompVisibility();
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.PopupLoadCompVisibility();
begin
  if tsCompFiltergroupSelect.Checked then
    grdCompFilter.PopupMenu := PopupMenu1
  else
    grdCompFilter.PopupMenu := nil;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.FillOutsFilter;
var
  i: Integer;
  s : string;
  pBookMark: TBookmark;
  ds: TDataSet;
begin

   sFilter :='';
   if tsCompFilterGroupSelect.Checked then
    begin
      ds := grdCompFilter.DataSource.DataSet;
      if grdCompFilter.SelectedList.Count = 0 then
        s := '(t2.co_nbr=' + IntToStr(ds['co_nbr']) + ' and t2.cl_nbr=' + IntToStr(ds['cl_nbr']) + ')'
      else
      begin
        ds.DisableControls;
        pBookMark := ds.GetBookmark;
        try
          s := '';
          for i := 0 to grdCompFilter.SelectedList.Count - 1 do
          begin
            ds.GotoBookmark(grdCompFilter.SelectedList.Items[i]);
            if s <> '' then s := s + ' or ';
            s := s + '(t2.co_nbr=' + IntToStr(ds['co_nbr']) + ' and t2.cl_nbr=' + IntToStr(ds['cl_nbr']) + ')';
          end;
        finally
          ds.GotoBookmark(pBookMark);
          ds.FreeBookmark(pBookMark);
          ds.EnableControls;
        end;
      end;
      sFilter := sFilter + ' and (' + s + ')';
    end;

end;


procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.OnGlobalFlagChange(const AFlagInfo: IevGlobalFlagInfo; const AOperation: Char);
var
  BM: TBookmark;
  i: Integer;
begin
  inherited;

  if AFlagInfo.FlagType = TypeName then
  begin
    if not cdQueue.Active then
      exit;
    cdQueue.DisableControls;
    try
      BM := cdQueue.GetBookmark;
      try
        cdQueue.First;
        while not cdQueue.Eof do
        begin
          if AFlagInfo.Tag = GetTag(cdQueue) then
          begin
            cdQueue.Edit;
            if AOperation = 'L' then
              cdQueue['Taken'] := AFlagInfo.UserName
            else if AOperation = 'U' then
              cdQueue['Taken'] := Null;

            cdQueue.Post;
          end;

          cdQueue.Next;
        end;

        cdQueue.GotoBookmark(BM);
      finally
        cdQueue.FreeBookmark(BM);
      end;
    finally
      cdQueue.EnableControls;
    end;
  end

  else if StartsWith(AFlagInfo.FlagType, 'TaxPreProc') then
  begin
    i := FFlagList.IndexOf(AFlagInfo.Tag);
    if (AOperation = 'S') and (i = -1) then
      FFlagList.Append(AFlagInfo.Tag)
    else if (AOperation = 'R') and (i <> -1) then
      FFlagList.Delete(i);
    gOneStepCoList.Refresh;      
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbEMailClick(Sender: TObject);
begin
  inherited;
  eEMail.Enabled := cbEMail.Enabled and cbEMail.Checked;
  cbEmailSendRule.Enabled := cbEMail.Enabled and cbEMail.Checked;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.RefreshFlagList;
var
  LockedFlagList: IisList;
  FlagInfo: IevGlobalFlagInfo;
  i: integer;
begin
  LockedFlagList := mb_GlobalFlagsManager.GetSetFlagList;
  FFlagList.Clear;
  for i := 0 to LockedFlagList.Count - 1 do
  begin
    FlagInfo := LockedFlagList[i] as IevGlobalFlagInfo;

    if StartsWith(FlagInfo.FlagType, 'TaxPreProc') then
      FFlagList.Append(FlagInfo.Tag);
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  cbMessages.Items.Clear;
  cbMessages.ScrollWidth := 0;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.chbAutorefreshStatusClick(
  Sender: TObject);
begin
  mb_AppSettings.AsInteger['TRQAutorefreshLocks'] := Ord(chbAutorefreshStatus.Checked);

  if chbAutorefreshStatus.Checked then
    Mainboard.GlobalCallbacks.Subscribe('', cetGlobalFlagEvent, TypeName + '.*')
  else
    Mainboard.GlobalCallbacks.Unsubscribe('', cetGlobalFlagEvent, TypeName + '.*');
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.AddMessage(aMsg: string);
var
  MsgWidth: integer;
begin
  with cbMessages do
  begin
    MsgWidth := Canvas.TextWidth( aMsg ) + 50;
    if MsgWidth > cbMessages.Width then
      if MsgWidth > ScrollWidth then
        ScrollWidth := MsgWidth;
    cbMessages.Items.Append(aMsg);
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cdFieldOfficeReportsCalcFields(
  DataSet: TDataSet);
var
  s: string;
begin
  inherited;
  s := DataSet.FieldByName('NAME').AsString;
  s := s + '- ' + ReturnDescription(TaxReturnType_ComboChoices, DataSet.FieldByName('SYSTEM_TAX_TYPE').AsString);
  s := s + '- ' + ReturnDescription(ReturnFrequencyType_ComboChoices, DataSet.FieldByName('RETURN_FREQUENCY').AsString);
  s := s + '- ' + ReturnDescription(ReturnLocalDepFrequencyType_ComboChoices, DataSet.FieldByName('DEPOSIT_FREQUENCY').AsString);
  Dataset['description'] := s;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.SendErrorMessageAndScreen(
  const FMessage, FDetails, FDateTime: string;
  const IncludeScreen: Boolean);
var
  DetailName, FileName, h: string;
  t: TStringList;
  b: TBitMap;
  j: TJPEGImage;
  F: TextFile;
begin
  FileName := AppTempFolder + '_ErrorMessage.jpg';
  DetailName := AppTempFolder + '_ErrorDetails.txt';

  t := TStringList.Create;
  try
    if IncludeScreen then
    begin
      b := TBitMap.Create;
      j := TJPEGImage.Create;
      try
        ScreenShot(0, 0, Screen.Width, Screen.Height, b);
        j.CompressionQuality := 50;
        j.Assign(b);
        j.SaveToFile(FileName);
        t.Add(FileName);
      finally
        b.Free;
        j.Free;
      end;
    end;
    if Length(FDetails) > 0 then
    begin
      AssignFile(F, DetailName);
      try
        Rewrite(F);
        Writeln(F, AdjustLineBreaks(FMessage));
        Write(F, AdjustLineBreaks(FDetails));
      finally
        CloseFile(F);
      end;
      t.Add(DetailName);
    end;
    h := FMessage;
    MailTo(nil, nil, nil, t, ExtractFileName(Application.ExeName) + ' error - ' + FDateTime,
           AdjustLineBreaks(h));
  finally
    t.Free;
  end;
end;


function TEDIT_TMP_CO_TAX_RETURN_QUEUE.GetClBlobData(const ANbr: Integer): IisStream;
var
  Q: IevQuery;
Begin
   Result := TisStream.Create;
   if  ANbr > 0 then
   begin
     Q := TevQuery.Create('select DATA from CL_BLOB where CL_BLOB_NBR = :nbr ');
     Q.Params.AddValue('nbr', ANbr);
     Q.Execute;
     if Q.Result.RecordCount > 0 then
     begin
        (Q.Result.vclDataSet.FieldByName('DATA') as TBlobField).SaveToStream(Result.RealStream);
        Result.Position := 0;
     end;
   end;
end;

function TEDIT_TMP_CO_TAX_RETURN_QUEUE.GetTaxReturnLastClBlobNbr(const ANbr: Integer): integer;
var
  Q: IevQuery;
Begin
   Result := -1;
   Q := TevQuery.Create('select CL_BLOB_NBR from CO_TAX_RETURN_RUNS ' +
                          ' where CO_TAX_RETURN_QUEUE_NBR = :nbr order by change_date desc  ROWS 1 ');
   Q.Params.AddValue('nbr', ANbr);
   Q.Execute;
   if Q.Result.RecordCount > 0 then
      Result := Q.Result.Fields[0].AsInteger;
end;


function TEDIT_TMP_CO_TAX_RETURN_QUEUE.GetTaxReturnAudit(const ANbr: Integer): IevDataSet;
var
  Q: IevQuery;
begin
   Q := TevQuery.Create('select CL_BLOB_NBR, CHANGE_DATE ' +
         ' from CO_TAX_RETURN_RUNS where CO_TAX_RETURN_QUEUE_NBR = :nbr ');
   Q.Params.AddValue('nbr', ANbr);
   Q.Execute;
   Result := Q.Result;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.HistMenuClick(Sender: TObject);
begin
  cdSelectedQueue.Edit;
  cdSelectedQueue['CL_BLOB_NBR'] := THistoryMenuItem(Sender).CL_BLOB_NBR;
  cdSelectedQueue.Post;
  wwDBGrid4.Refresh;
end;


procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.menuHistPopup(Sender: TObject);
var
  AuditData: IevDataSet;
  mi: THistoryMenuItem;
begin
  ctx_DataAccess.OpenClient(cdSelectedQueue['CL_NBR']);

  AuditData := GetTaxReturnAudit(cdSelectedQueue['CO_TAX_RETURN_QUEUE_NBR']);
  menuHist.Items.Clear;
  AuditData.IndexFieldNames := 'CHANGE_DATE';
  AuditData.Last;
  while not AuditData.BOF do
  begin
    if AuditData['CL_BLOB_NBR'] <> null then
    begin
      mi := THistoryMenuItem.Create(menuHist);
      mi.Caption := 'Processed on '+ DateTimeToStr(AuditData['CHANGE_DATE']);
      mi.ChangeDate := AuditData['CHANGE_DATE'];
      mi.CL_BLOB_NBR := Integer(AuditData['CL_BLOB_NBR']);
      if mi.CL_BLOB_NBR = wwDBGrid4.DataSource.DataSet.FieldByName('CL_BLOB_NBR').AsInteger then
        mi.Default := True;
      mi.OnClick := HistMenuClick;
      menuHist.Items.Add(mi);
    end;
    AuditData.Prior;
  end;

  if menuHist.Items.Count = 0 then
    AbortEx;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.LoadCompaniesfromTextFile1Click(
  Sender: TObject);
begin
  inherited;
  SPopupCompanySelectHelper.TPopupCompanySelectHelper.SelectCompanyListFromFile(grdCompFilter, nil);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.evDBGrid1Exit(Sender: TObject);
begin
  inherited;
    cdEe.CheckBrowseMode;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.InitLookupMEFDS;
begin
  with TExecDSWrapper.Create('select DISTINCT a.NAME LookUpName from SY_REPORT_GROUPS a, SY_REPORT_WRITER_REPORTS b, SY_REPORT_GROUP_MEMBERS c ' +
                     'where UPPERCASE(a.NAME) like ''%MEF'' and  {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                     'a.SY_REPORT_GROUPS_NBR = c.SY_REPORT_GROUPS_NBR and ' +
                     'c.SY_REPORT_WRITER_REPORTS_NBR = b.SY_REPORT_WRITER_REPORTS_NBR') do
    ctx_DataAccess.GetSyCustomData(cdLookupMEF, AsVariant);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.SetMefFilter;
Begin
    dblookupMEF.Text := ConvertNull(cdLookupMEF.FieldValues['LookUpName'],'');
    cdMEF.Filter:= 'LookUpName = '''+ ConvertNull(cdLookupMEF.FieldValues['LookUpName'],'-1') + '''';
    cdMEF.Filtered := True;
End;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.dblookupMEFChange(Sender: TObject);
begin
  inherited;
  SetMefFilter;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnMefFileClick(Sender: TObject);
var
  ReportResults: TrwReportResults;
  BlobStream: IisStream;
  xmlFile: TReturnTransmissionFile94X;
  Q: IevQuery;

  Procedure GetReportResult;
  var
    Res: TrwReportResult;
    i: Integer;
    r: TrwReportResults;
  begin
      Res := ReportResults.Add;
      i := BlobStream.ReadInteger;
      if i = $FF then // new format
      begin
        r := TrwReportResults.Create;
        try
          r.SetFromStream(UnCompressStreamFromStream(BlobStream));
          Assert(r.Count > 0);
          Res.Assign(r[0]);
        finally
          r.Free;
        end;
      end
      else
      begin
        Res.ReportType := TReportType(i);
        Res.Data := TEvDualStreamHolder.Create;
        InflateStream(BlobStream.RealStream, Res.Data.RealStream);
      end;
      BlobStream := nil;
  end;

  function GetEfileNumber: boolean;
  Begin
    Result := false;
    Q := TevQuery.Create('select ADDRESS1 EFIN, city ETIN from SB_ACCOUNTANT where NAME = ''EFILE''');
    Q.Execute;
    if Q.Result.RecordCount = 1 then
       Result := true;
  end;
var
 i: integer;
 mefType: Boolean;

begin
  inherited;

  if not GetEfileNumber then
     raise EevException.Create('Invalid ETIN and EFIN');

  cdMEF.DisableControls;
  if grdMEF.SelectedList.Count > 0 then
  begin
     ctx_StartWait('Loading data...');
     mefType:= false;
     if UpperCase(Trim(dblookupMEF.Text)) = '940 MEF' then
        mefType:= True;

    xmlFile := TReturnTransmissionFile94X.Create(cbSettingsMagmediaPath.Text, dblookupMEF.Text, Q.Result.Fields[0].AsString, Q.Result.Fields[1].AsString);
    try

      for i := 0 to grdMEF.SelectedList.Count - 1 do
      begin
        cdMEF.GotoBookmark(grdMEF.SelectedList[i]);

        ctx_DataAccess.OpenClient(cdMEF.FieldByName('CL_NBR').asInteger);
        DM_COMPANY.CO.DataRequired('CO_NBR='+ cdMEF.FieldByName('CO_NBR').AsString );

        ReportResults := TrwReportResults.Create;
        try
          BlobStream := GetClBlobData(GetTaxReturnLastClBlobNbr(cdMEF.FieldByName('CO_TAX_RETURN_QUEUE_NBR').asInteger));
          GetReportResult;

          if (ReportResults.Count > 0) then
          begin
           ReportResults[0].Data.AsString:= TXMLHelper.FixXMLReport(ReportResults[0].Data.AsString, mefType);
           xmlFile.AddXMLReport(ReportResults[0].Data.RealStream, cdMEF.FieldByName('report_description').AsString,
                                                       DM_COMPANY.CO.FieldByName('FEIN').AsString, cbDateFrom.DateTime, cbDateTill.DateTime);
           if xmlFile.Counter > 99 then
             xmlFile.PostResult;

         end;
       finally
          ReportResults.Free;
        end;
      end;
    finally
      if xmlFile.Counter > 0 then
         xmlFile.PostResult;

      xmlFile.Free;

      ctx_EndWait;
      cdMEF.EnableControls;
    end;
  end;

end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.InitLookupACADS;
begin
  with TExecDSWrapper.Create('select DISTINCT a.NAME LookUpName from SY_REPORT_GROUPS a, SY_REPORT_WRITER_REPORTS b, SY_REPORT_GROUP_MEMBERS c ' +
                     'where a.NAME like ''%Tax Return-1095%'' and  {AsOfNow<a>} and {AsOfNow<b>} and {AsOfNow<c>} and ' +
                     'a.SY_REPORT_GROUPS_NBR = c.SY_REPORT_GROUPS_NBR and b.REPORT_TYPE = ''X'' and ' +
                     'c.SY_REPORT_WRITER_REPORTS_NBR = b.SY_REPORT_WRITER_REPORTS_NBR') do

    ctx_DataAccess.GetSyCustomData(cdLookupACA, AsVariant);
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.SetAcaFilter;
Begin
    dblookupACA.Text := ConvertNull(cdLookupACA.FieldValues['LookUpName'],'');
    cdACA.Filter:= 'LookUpName = '''+ ConvertNull(cdLookupACA.FieldValues['LookUpName'],'-1') + '''';
    cdACA.Filtered := True;
End;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.dblookupACAChange(Sender: TObject);
begin
  inherited;
  SetAcaFilter;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.btnACAFileClick(Sender: TObject);
var
  ReportResults: TrwReportResults;
  BlobStream: IisStream;
  xmlFile: TReturnTransmissionFile1094;
  Q: IevQuery;

  Procedure GetReportResult;
  var
    Res: TrwReportResult;
    i: Integer;
    r: TrwReportResults;
  begin
      Res := ReportResults.Add;
      i := BlobStream.ReadInteger;
      if i = $FF then // new format
      begin
        r := TrwReportResults.Create;
        try
          r.SetFromStream(UnCompressStreamFromStream(BlobStream));
          Assert(r.Count > 0);
          Res.Assign(r[0]);
        finally
          r.Free;
        end;
      end
      else
      begin
        Res.ReportType := TReportType(i);
        Res.Data := TEvDualStreamHolder.Create;
        InflateStream(BlobStream.RealStream, Res.Data.RealStream);
      end;
      BlobStream := nil;
  end;

  function GetEfileNumber: boolean;
  Begin
    Result := false;
    Q := TevQuery.Create('select ADDRESS1 EFIN, city ETIN, Description2 TCC from SB_ACCOUNTANT where {AsOfNow<SB_ACCOUNTANT>} and NAME = ''EFILE''');
    Q.Execute;
    if Q.Result.RecordCount = 1 then
       Result := true;
  end;
var
 i: integer;
 tmpSize: int64;
 tmpCount: Integer;
 tmpXML: String;
begin
  inherited;

  if not GetEfileNumber then
     raise EevException.Create('Invalid ETIN and EFIN and TCC');

  cdACA.DisableControls;
  if grdACA.SelectedList.Count > 0 then
  begin
    ctx_StartWait('Loading data...');

    xmlFile := TReturnTransmissionFile1094.Create(cbSettingsMagmediaPath.Text, dblookupACA.Text,DM_SERVICE_BUREAU.SB.FieldByName('EIN_NUMBER').AsString, Q.Result.Fields[2].AsString,
                                                   cbTestFile.Checked, cbReplacement.Checked, cbTransmissionReplacement.Checked, edOriginalReceiptId.Text);
    try

      for i := 0 to grdACA.SelectedList.Count - 1 do
      begin
        cdACA.GotoBookmark(grdACA.SelectedList[i]);

        ctx_DataAccess.OpenClient(cdACA.FieldByName('CL_NBR').asInteger);
        DM_COMPANY.CO.DataRequired('CO_NBR='+ cdACA.FieldByName('CO_NBR').AsString );

        ReportResults := TrwReportResults.Create;
        try
          BlobStream := GetClBlobData(GetTaxReturnLastClBlobNbr(cdACA.FieldByName('CO_TAX_RETURN_QUEUE_NBR').asInteger));
          GetReportResult;

          if (ReportResults.Count > 0) then
          begin
           tmpXML:= TXMLHelper.FixXMLReportACA(ReportResults[0].Data.AsString, xmlFile.UniqueHeader, cbReplacement.Checked);
           if not xmlFile.UniqueHeader then
             xmlFile.UniqueHeader:= True;

           tmpCount:= Length(IntToStr(xmlFile.Count + 1)) -1;
           tmpSize:= Length(tmpXML);

           if xmlFile.Size + tmpSize + tmpCount  > 104857500 then
           begin
             xmlFile.PostResult(cbDateFrom.DateTime, cbDateTill.DateTime);
             ReportResults[0].Data.AsString:= TXMLHelper.FixXMLReportACA(ReportResults[0].Data.AsString, xmlFile.UniqueHeader, cbReplacement.Checked);
             xmlFile.UniqueHeader:= True;
           end
           else
             ReportResults[0].Data.AsString := tmpXML;

           xmlFile.IncCount;
           ReportResults[0].Data.AsString:= TXMLHelper.FixXMLReportACASubmissionID(ReportResults[0].Data.AsString,xmlFile.Count);

           xmlFile.AddXMLReport(ReportResults[0].Data.RealStream, cbDateFrom.DateTime, cbDateTill.DateTime);
         end;
        finally
          ReportResults.Free;
        end;
      end;
    finally
      if xmlFile.Size > 0 then
         xmlFile.PostResult(cbDateFrom.DateTime, cbDateTill.DateTime);

      xmlFile.Free;
      grdACA.UnSelectAll;
      ctx_EndWait;
      cdACA.EnableControls;
    end;
  end;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbTransmissionReplacementClick(
  Sender: TObject);
begin
  inherited;
  if not cbTransmissionReplacement.Checked then
    edOriginalReceiptId.Text:= '';

  edOriginalReceiptId.Enabled:= cbTransmissionReplacement.Checked;

  if cbReplacement.Checked then
     if cbTransmissionReplacement.Checked then
       cbReplacement.Checked:= false;
end;

procedure TEDIT_TMP_CO_TAX_RETURN_QUEUE.cbReplacementClick(
  Sender: TObject);
begin
  inherited;
  if cbTransmissionReplacement.Checked then
     if cbReplacement.Checked then
       cbTransmissionReplacement.Checked:= false;
end;

initialization
  RegisterClass(TEDIT_TMP_CO_TAX_RETURN_QUEUE);

end.




