// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, wwdblook,  Wwdatsrc, Variants, EvBasicUtils, EvContext,
  Grids, Wwdbigrd, Wwdbgrid, Db, ExtCtrls,  Spin, EvTypes,
  ISBasicClasses, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvExceptions, EvCommonInterfaces, Mask, wwdbedit,
  Wwdotdot, Wwdbcomb, isUIwwDBComboBox, LMDCustomButton, LMDButton,
  isUILMDButton, EvUIComponents, EvClientDataSet, EvUIUtils, //Menus
  SPopupCompanySelectHelper, Menus
  ;

type
  TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD = class(TForm)
    btnGo: TevButton;
    cdClCo: TevClientDataSet;
    grdCompanyList: TevDBGrid;
    dsClCo: TevDataSource;
    cdClCoCL_NBR: TIntegerField;
    cdClCoCUSTOM_CLIENT_NUMBER: TStringField;
    cdClCoCLIENT_NAME: TStringField;
    cdClCoCO_NBR: TIntegerField;
    cdClCoCUSTOM_COMPANY_NUMBER: TStringField;
    cdClCoCOMPANY_NAME: TStringField;
    cdClCoFEIN: TStringField;
    cdClCoTAX_SERVICE: TStringField;
    cdClCoTRUST_SERVICE: TStringField;
    cdClCoTERMINATION_CODE: TStringField;
    cdCheckDates: TevClientDataSet;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    cdConsolidation: TevClientDataSet;
    cbMonth: TevComboBox;
    bLoad: TevButton;
    Bevel1: TEvBevel;
    cdClCoTYPE: TStringField;
    cdClCoTYPE_DESCR: TStringField;
    cdClCoInfo: TevClientDataSet;
    cbLoadAll: TevCheckBox;
    edYear: TevSpinEdit;
    cbActiveOnly: TevCheckBox;
    cbSemiMthly: TevCheckBox;
    evLabel1: TevLabel;
    cdCheckDatesNORMAL_CHECK_DATE: TDateTimeField;
    cdCheckDatesSCHEDULED_CHECK_DATE: TDateTimeField;
    cdClCoLAST_CHECK_DATE: TDateTimeField;
    cbNotEnlisted: TevCheckBox;
    rgPeriod: TevRadioGroup;
    btnEnlistAll: TevButton;
    btnEnlistActive: TevButton;
    cbTerminationcode: TevDBComboBox;
    cdClCoNORMAL_CHECK_DATE: TDateTimeField;
    cdCheckDatesLAST_PROCESSED_DATE: TDateTimeField;
    cdClCoLAST_PROCESSED_DATE: TDateTimeField;
    evMsg: TevLabel;
    popGridMenu: TPopupMenu;
    miSelectFromFile: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
    procedure bLoadClick(Sender: TObject);
    procedure cbMonthChange(Sender: TObject);
    procedure grdCompanyListTitleButtonClick(Sender: TObject;
      AFieldName: String);
    procedure grdCompanyListMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure grdCompanyListCalcTitleAttributes(Sender: TObject;
      AFieldName: String; AFont: TFont; ABrush: TBrush;
      var ATitleAlignment: TAlignment);
    procedure grdCompanyListCalcTitleImage(Sender: TObject; Field: TField;
      var TitleImageAttributes: TwwTitleImageAttributes);
    procedure cbLoadAllClick(Sender: TObject);
    procedure rgPeriodClick(Sender: TObject);
    procedure btnEnlistAllClick(Sender: TObject);
    procedure btnEnlistActiveClick(Sender: TObject);
    procedure cbSemiMthlyClick(Sender: TObject);
    procedure miSelectFromFileClick(Sender: TObject);
  private
    { Private declarations }
    d: TDateTime;
    FShift: TShiftState;

  public
    { Public declarations }
  end;


var
  EDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD: TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD;

implementation

uses
  EvUtils, EvConsts, SDataStructure, SPD_EDIT_TMP_CO_RETURN_QUEUE, ImgList,
  SReturnQueueEnlistProcedures, isBaseClasses;

{$R *.DFM}



procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.FormCreate(
  Sender: TObject);
begin
  rgPeriodClick(nil);

  cdCheckDates.CreateDataSet;
  with TExecDSWrapper.Create('Consolidations') do
  begin
    ctx_DataAccess.GetTmpCustomData(cdConsolidation, AsVariant);
  end;  


  ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
  ctx_DataAccess.TEMP_CUSTOM_VIEW.DataRequest(TExecDSWrapper.Create('SelectClientsCompanies').AsVariant);
  ctx_DataAccess.TEMP_CUSTOM_VIEW.Open;
  cdClCoInfo.Data := ctx_DataAccess.TEMP_CUSTOM_VIEW.Data;
  ctx_DataAccess.TEMP_CUSTOM_VIEW.Close;
  cdClCo.CreateDataSet;
end;


procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.btnEnlistActiveClick(
  Sender: TObject);
begin
  cdClCo.DisableControls;
  try
   grdCompanyList.UnselectAll;
   cdClCo.First;
   while not cdClCo.Eof do
   begin
     if cdClCo.FieldByName('TERMINATION_CODE').AsString[1] in [TERMINATION_ACTIVE, TERMINATION_SEASONAL_ACTIVE] then
       grdCompanyList.SelectRecord;
     cdClCo.Next;
   end;
  finally
    cdClCo.First;
    cdClCo.EnableControls;
    btnGoClick(Sender);
  end;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.btnEnlistAllClick(
  Sender: TObject);
begin
 grdCompanyList.SelectAll;
 btnGoClick(Sender);
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.btnGoClick(
  Sender: TObject);
var
  i, j: Integer;
  c: array of Integer;
  co: Variant;
  log: TStringList;
  ConsolidationType, Scope: string;
begin
  log := TStringList.Create;
  cdClCo.DisableControls;
  try
    for i := 0 to grdCompanyList.SelectedList.Count-1 do
    begin
      ctx_StartWait('Processing company ' + cdClCo.FieldByName('COMPANY_NAME').AsString);
      try
        cdClCo.GotoBookmark(grdCompanyList.SelectedList[i]);
        ctx_DataAccess.OpenClient(cdClCo['CL_NBR']);
        DM_COMPANY.CO.DataRequired('ALL');
        if DM_COMPANY.CO.RecordCount > 1 then
          DM_COMPANY.CO.Locate('CO_NBR', cdClCo['CO_NBR'], []);

        if DM_COMPANY.CO['AUTO_ENLIST'] = ReturnAutoEnlist_All then
        begin
          cdConsolidation.Filter := 'CL_NBR = '+ IntToStr(cdClCo['CL_NBR']);
          cdConsolidation.Filtered := True;
          try
            if (cdConsolidation.RecordCount > 0) and
                cdConsolidation.Locate('CO_NBR', cdClCo['CO_NBR'], []) then
            begin
              cdConsolidation.First;
              if (cdClCo['TYPE'] <> '-') then
              begin
                SetLength(c, 1);
                c[0] := cdConsolidation['PRIMARY_CO_NBR'];
                co := cdConsolidation['CL_CO_CONSOLIDATION_NBR'];
                ConsolidationType := cdConsolidation['CONSOLIDATION_TYPE'];
                Scope := cdConsolidation['SCOPE'];
                j := 1;
                while not cdConsolidation.Eof do
                begin
                  if cdConsolidation['PRIMARY_CO_NBR'] <> cdConsolidation['CO_NBR'] then
                  begin
                    SetLength(c, j+1);
                    c[j] := cdConsolidation['CO_NBR'];
                    Inc(j);
                  end;
                  cdConsolidation.Next;
                end;
              end
              else
              begin
                SetLength(c, 1);
                c[0] := cdClCo['CO_NBR'];
                co := Null;
                ConsolidationType := cdConsolidation['CONSOLIDATION_TYPE'];
                Scope :=cdConsolidation['SCOPE'];
              end;
            end
            else
            begin
              SetLength(c, 1);
              c[0] := cdClCo['CO_NBR'];
              co := Null;
              ConsolidationType := CONSOLIDATION_TYPE_ALL_REPORTS;
              Scope :='';
            end;
          finally
            cdConsolidation.Filtered := False;
          end;
          try
            if cbSemiMthly.Checked then
               EnlistCompany(c, GetBeginMonth(d)+ 14, co, ConsolidationType, Scope)
            else
               EnlistCompany(c, GetEndMonth(d), co, ConsolidationType, Scope);
          except
            on E: Exception do
              log.Append(E.Message);
          end;
        end;
      finally
        ctx_EndWait;
      end;
    end;
    if log.Count <> 0 then
      EvMessage(log.Text);
  finally
    cdClCo.EnableControls;
    log.Free;
  end;
  ModalResult := mrOk;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.bLoadClick(
  Sender: TObject);
var
 cdTMP_CO_TAX_RETURN : TevClientDataSet;

  function CheckCoNotEnlisted(const CLNbr, CONbr : integer):Boolean;
  begin
   Result := True;

   if cbNotEnlisted.Checked and
      cdTMP_CO_TAX_RETURN.Locate('CL_NBR;CO_NBR', VarArrayOf([CLNbr,CONbr]), []) then
     Result := false;

  end;

  function CheckLastProcessedScheduledPayroll:Boolean;
  begin
   Result := False;
   if (not cbLoadAll.Checked) and (not cdCheckDates.FieldByName('NORMAL_CHECK_DATE').IsNull) then
         Result := True;
  end;

  procedure AssignRecord(const cd1, cd2: TevClientDataSet);
  var
    i: Integer;
    f: TField;
  begin
    for i := 0 to cd1.FieldCount-1 do
    begin
      f := cd2.FindField(cd1.Fields[i].FieldName);
      if Assigned(f) then
        f.Assign(cd1.Fields[i]);
    end;
  end;
var
  d1, d2: string[10];
  Q : IevDataSet;
begin

  if rgPeriod.ItemIndex = 0 then
  begin
    d := EncodeDate(edYear.Value, cbMonth.ItemIndex+ 1, 1);
    d1 := DateToStr(d);
    d2 := DateToStr(GetEndMonth(d));
  end
  else
  begin
    d  := EncodeDate(edYear.Value, (cbMonth.ItemIndex * 3) + 1, 1);
    d1 := DateToStr(d);
    d2 := DateToStr(GetEndQuarter(d));
    d  := GetEndQuarter(d);
  end;

  DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.Close;
  DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.DataRequired('SCHEDULED_CHECK_DATE between '''+ d1+ ''' and '''+ d2+ '''');

  while cdCheckDates.RecordCount > 0 do
    cdCheckDates.Delete;
  cdCheckDates.LogChanges := False;
  DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.First;
  while not DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.Eof do
  begin
    if cdCheckDates.Locate('CL_NBR;CO_NBR', DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.FieldValues['CL_NBR;CO_NBR'], []) then
    begin
      if (cdCheckDates['SCHEDULED_CHECK_DATE'] < DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.FieldValues['SCHEDULED_CHECK_DATE']) then
      begin
        cdCheckDates.Edit;
        cdCheckDates['SCHEDULED_CHECK_DATE;NORMAL_CHECK_DATE'] := DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.FieldValues['SCHEDULED_CHECK_DATE;NORMAL_CHECK_DATE'];
        if not DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.FieldByName('NORMAL_CHECK_DATE').IsNull then
           cdCheckDates['LAST_PROCESSED_DATE'] := DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.FieldValues['NORMAL_CHECK_DATE'];
        cdCheckDates.Post;
      end;
    end
    else
    begin
      cdCheckDates.Append;
      cdCheckDates['CL_NBR;CO_NBR;SCHEDULED_CHECK_DATE;NORMAL_CHECK_DATE'] := DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.FieldValues['CL_NBR;CO_NBR;SCHEDULED_CHECK_DATE;NORMAL_CHECK_DATE'];
      cdCheckDates['CL_NBR;CO_NBR;SCHEDULED_CHECK_DATE;NORMAL_CHECK_DATE;LAST_PROCESSED_DATE'] := DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.FieldValues['CL_NBR;CO_NBR;SCHEDULED_CHECK_DATE;NORMAL_CHECK_DATE;NORMAL_CHECK_DATE'];
      cdCheckDates.Post;
    end;
    DM_TEMPORARY.TMP_PR_SCHEDULED_EVENT.Next;
  end;

  cdClCo.DisableControls;

  cdTMP_CO_TAX_RETURN := TevClientDataSet.Create(nil);
  cdTMP_CO_TAX_RETURN.ProviderName := DM_TEMPORARY.TMP_CO_TAX_RETURN_QUEUE.ProviderName;
  cdTMP_CO_TAX_RETURN.DataRequired('PERIOD_END_DATE = '''+ d2+ '''');
  try
    grdCompanyList.UnselectAll;
    while cdClCo.RecordCount > 0 do
      cdClCo.Delete;
    cdClCo.LogChanges := False;
    cdConsolidation.First;
    while not cdConsolidation.Eof do
    begin
      if VarIsNull(cdConsolidation['PRIMARY_CO_NBR']) then
        raise EInvalidParameters.CreateHelp('Primary company is not set for client consolidation'#13+ cdClCoInfo.Lookup('CL_NBR', cdConsolidation['CL_NBR'], 'CLIENT_NAME'), IDH_InvalidParameters);

      if cbLoadAll.Checked or cbNotEnlisted.Checked then
      begin
        if not cdClCo.Locate('CL_NBR;CO_NBR;TYPE', cdConsolidation['CL_NBR;PRIMARY_CO_NBR;CONSOLIDATION_TYPE'], [])
        and cdClCoInfo.Locate('CL_NBR;CO_NBR', cdConsolidation['CL_NBR;PRIMARY_CO_NBR'], [])
        and CheckCoNotEnlisted(cdConsolidation.FieldByName('CL_NBR').asInteger,cdConsolidation.FieldByName('PRIMARY_CO_NBR').asinteger) then
          if not cbActiveOnly.Checked or
          ((cdClCoInfo['termination_code'] = TERMINATION_ACTIVE) or (cdClCoInfo['termination_code'] = TERMINATION_SEASONAL_ACTIVE)) then
          begin
            cdClCo.Append;
            cdClCo['TYPE'] := cdConsolidation['CONSOLIDATION_TYPE'];
            cdClCo['TYPE_DESCR'] := 'Consolidation';
            if cdCheckDates.Locate('CL_NBR;CO_NBR', cdConsolidation['CL_NBR;CO_NBR'], []) then
            begin
               cdClCo['LAST_CHECK_DATE'] := cdCheckDates['SCHEDULED_CHECK_DATE'];
               cdClCo['NORMAL_CHECK_DATE'] := cdCheckDates['NORMAL_CHECK_DATE'];
               cdClCo['LAST_PROCESSED_DATE'] := cdCheckDates['LAST_PROCESSED_DATE'];
            end;

            AssignRecord(cdClCoInfo, cdClCo);
            cdClCo.Post;
          end;
      end
      else
      begin
        if cdCheckDates.Locate('CL_NBR;CO_NBR', cdConsolidation['CL_NBR;CO_NBR'], []) then
        begin
          if CheckLastProcessedScheduledPayroll then
          begin
            if not cdClCo.Locate('CL_NBR;CO_NBR;TYPE', cdConsolidation['CL_NBR;PRIMARY_CO_NBR;CONSOLIDATION_TYPE'], [])
            and cdClCoInfo.Locate('CL_NBR;CO_NBR', cdConsolidation['CL_NBR;PRIMARY_CO_NBR'], []) then
              if not cbActiveOnly.Checked or
                 ((cdClCoInfo['termination_code'] = TERMINATION_ACTIVE) or (cdClCoInfo['termination_code'] = TERMINATION_SEASONAL_ACTIVE)) then
              begin
                cdClCo.Append;
                cdClCo['TYPE'] := cdConsolidation['CONSOLIDATION_TYPE'];
                cdClCo['TYPE_DESCR'] := 'Consolidation';
                cdClCo['LAST_CHECK_DATE'] := cdCheckDates['SCHEDULED_CHECK_DATE'];
                cdClCo['NORMAL_CHECK_DATE'] := cdCheckDates['NORMAL_CHECK_DATE'];
               cdClCo['LAST_PROCESSED_DATE'] := cdCheckDates['LAST_PROCESSED_DATE'];
                AssignRecord(cdClCoInfo, cdClCo);
                cdClCo.Post;
              end;
          end
          else
          begin
            if cdClCo.Locate('CL_NBR;CO_NBR;TYPE', cdConsolidation['CL_NBR;PRIMARY_CO_NBR;CONSOLIDATION_TYPE'], []) then
              cdClCo.Delete;
          end;
        end;
      end;
      cdConsolidation.Next;
    end;
    if cbLoadAll.Checked or cbNotEnlisted.Checked then
    begin
      cdClCoInfo.First;
      while not cdClCoInfo.Eof do
      begin
        if not cdClCo.Locate('CL_NBR;CO_NBR;TYPE', VarArrayOf([cdClCoInfo['CL_NBR'], cdClCoInfo['CO_NBR'], '-']), [])
           and CheckCoNotEnlisted(cdClCoInfo.FieldByName('CL_NBR').AsInteger, cdClCoInfo.fieldByName('CO_NBR').AsInteger) then
          if not cbActiveOnly.Checked or
          ((cdClCoInfo['termination_code'] = TERMINATION_ACTIVE) or (cdClCoInfo['termination_code'] = TERMINATION_SEASONAL_ACTIVE)) then
          begin
            cdClCo.Append;
            cdClCo['TYPE'] := '-';
            cdClCo['TYPE_DESCR'] := 'Company';
            if cdCheckDates.Locate('CL_NBR;CO_NBR', cdClCoInfo['CL_NBR;CO_NBR'], []) then
            begin
               cdClCo['LAST_CHECK_DATE'] := cdCheckDates['SCHEDULED_CHECK_DATE'];
               cdClCo['NORMAL_CHECK_DATE'] := cdCheckDates['NORMAL_CHECK_DATE'];
               cdClCo['LAST_PROCESSED_DATE'] := cdCheckDates['LAST_PROCESSED_DATE'];
            end;
            AssignRecord(cdClCoInfo, cdClCo);
            cdClCo.Post;
          end;
        cdClCoInfo.Next;
      end;
    end
    else
    begin
      cdCheckDates.First;
      while not cdCheckDates.Eof do
      begin
        if CheckLastProcessedScheduledPayroll and
        (not cdClCo.Locate('CL_NBR;CO_NBR;TYPE', VarArrayOf([cdCheckDates['CL_NBR'], cdCheckDates['CO_NBR'], '-']), []))
        and cdClCoInfo.Locate('CL_NBR;CO_NBR', cdCheckDates['CL_NBR;CO_NBR'], []) then
          if not cbActiveOnly.Checked or
          ((cdClCoInfo['termination_code'] = TERMINATION_ACTIVE) or (cdClCoInfo['termination_code'] = TERMINATION_SEASONAL_ACTIVE)) then
          begin
            cdClCo.Append;
            cdClCo['TYPE'] := '-';
            cdClCo['TYPE_DESCR'] := 'Company';
            cdClCo['LAST_CHECK_DATE'] := cdCheckDates['SCHEDULED_CHECK_DATE'];
            cdClCo['NORMAL_CHECK_DATE'] := cdCheckDates['NORMAL_CHECK_DATE'];
            cdClCo['LAST_PROCESSED_DATE'] := cdCheckDates['LAST_PROCESSED_DATE'];
            AssignRecord(cdClCoInfo, cdClCo);
            cdClCo.Post;
          end;
        cdCheckDates.Next;
      end;
    end;

    if CheckReadOnlyClientCompanyFunction then
    begin
      Q := GetReadOnlyClintCompanyDataset;
      Q.First;
      while not Q.Eof do
      begin
        while cdClCo.Locate('CL_NBR;CO_NBR', Q.FieldValues['CL_NBR;CO_NBR'], []) do
           cdClCo.Delete;
        Q.Next;
      end;
    end;

    cdClCo.First;
  finally
    cdTMP_CO_TAX_RETURN.Free;
    cdClCo.EnableControls;
  end;
  grdCompanyList.SelectRecord;
  grdCompanyList.Visible := True;
  grdCompanyList.SetFocus;
  grdCompanyList.ShowHorzScrollBar := False;
  grdCompanyList.ShowHorzScrollBar := True;
  btnGo.Visible := True;
  btnEnlistAll.Visible := True;
  btnEnlistActive.Visible := True;
  evMsg.Visible := false;  
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.cbMonthChange(
  Sender: TObject);
begin
  grdCompanyList.Visible := False;
  btnGo.Visible := False;
  btnEnlistAll.Visible := false;
  btnEnlistActive.Visible := false;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.grdCompanyListTitleButtonClick(
  Sender: TObject; AFieldName: String);

  function CheckSubStr(const Substr, Str: string): Boolean;
  begin
    Result := Pos(';' + Substr + ';', ';' + Str + ';') <> 0;
  end;

  procedure DeleteSubStrFrom(const Substr, MasterStr: string; var Str: string);
  var
    i: Integer;
  begin
    i := Pos(';' + Substr + ';', ';' + Str + ';');
    if i <> 0 then
    begin
      if ssCtrl in FShift then
      begin
        Str := ';' + Str + ';';
        Str := Copy(Str, 1, i - 1) + ';' + Copy(Str, i + Length(Substr) + 2, 255);
        if (Str <> '') and (Str[1] = ';') then
          Delete(Str, 1, 1);
        if (Str <> '') and (Str[Length(Str)] = ';') then
          Delete(Str, Length(Str), 1);
      end
      else
        Str := MasterStr;
    end;
  end;

  procedure AddSubStrTo(const Substr, MasterStr: string; var Str: string);
  begin
    if ssCtrl in FShift then
    begin
      if not CheckSubStr(Substr, Str) then
      begin
        if Str <> '' then
          Str := Str + ';';
        Str := Str + Substr;
      end;
    end
    else
    begin
      if MasterStr <> '' then
        Str := MasterStr + ';' + Substr
      else
        Str := Substr;
    end;
  end;
var
  s, ds: string;
  i: Integer;
begin
  with (Sender as TevDBGrid).DataSource.DataSet as TevClientDataSet do
    if FieldByName(AFieldName).FieldKind in [fkData, fkInternalCalc] then
    begin
      DisableControls;
      try
        IndexDefs.Update;
        i := IndexDefs.IndexOf('SORT');
        if i <> -1 then
        begin
          s := IndexDefs[i].Fields;
          if ixDescending in IndexDefs[i].Options then
            ds := IndexDefs[i].Fields
          else
            ds := IndexDefs[i].DescFields;
        end
        else
        begin
          s := '';
          ds := '';
        end;
        if CheckSubStr(AFieldName, ds) then
        begin
          DeleteSubStrFrom(AFieldName, '', ds);
          DeleteSubStrFrom(AFieldName, MasterFields, s);
        end
        else
          if CheckSubStr(AFieldName, s) then
        begin
          AddSubStrTo(AFieldName, MasterFields, s);
          AddSubStrTo(AFieldName, '', ds);
        end
        else
        begin
          AddSubStrTo(AFieldName, MasterFields, s);
          AddSubStrTo('', '', ds);
        end;
        IndexFieldNames := MasterFields;
        if i <> -1 then
          DeleteIndex('SORT');
        AddIndex('SORT', s, [ixCaseInsensitive], ds);
        IndexDefs.Update;
        IndexName := 'SORT';
      finally
        EnableControls;
      end;
    end;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.grdCompanyListMouseUp(
  Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  FShift := Shift;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.grdCompanyListCalcTitleAttributes(
  Sender: TObject; AFieldName: String; AFont: TFont; ABrush: TBrush;
  var ATitleAlignment: TAlignment);
var
  s: string;
  i: Integer;
begin
  with (Sender as TevDBGrid).DataSource.DataSet as TevClientDataSet do
  begin
    if not (FieldByName(AFieldName).FieldKind in [fkData, fkInternalCalc]) then
    begin
      ABrush.Color := ColorToRGB(ABrush.Color);
      ABrush.Color :=
        ((ABrush.Color mod 256) * 5 div 6) +
        ((ABrush.Color div 256 mod 256) * 5 div 6) * 256 +
        ((ABrush.Color div 256 div 256 mod 256) * 5 div 6) * 256 * 256;
    end
    else
      if IndexName = 'SORT' then
    begin
      i := IndexDefs.IndexOf(IndexName);
      s := IndexDefs[i].Fields + ';' + IndexDefs[i].DescFields;
      if Pos(';' + AFieldName + ';', ';' + s + ';') <> 0 then
      begin
        AFont.Color := clBlue;
        ABrush.Color := clYellow;
      end;
    end;
  end;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.grdCompanyListCalcTitleImage(
  Sender: TObject; Field: TField;
  var TitleImageAttributes: TwwTitleImageAttributes);
var
  i: Integer;
begin
  with (Sender as TevDBGrid).DataSource.DataSet as TevClientDataSet do
  begin
    TitleImageAttributes.ImageIndex := -1;
    TitleImageAttributes.Alignment := taRightJustify;
    if IndexName = 'SORT' then
    begin
      i := IndexDefs.IndexOf(IndexName);
      if ((ixDescending in IndexDefs[i].Options)
        and (Pos(';' + Field.FieldName + ';', ';' + IndexDefs[i].Fields + ';') <> 0))
        or (Pos(';' + Field.FieldName + ';', ';' + IndexDefs[i].DescFields + ';') <> 0) then
        TitleImageAttributes.ImageIndex := 0
      else
        if Pos(';' + Field.FieldName + ';', ';' + IndexDefs[i].Fields + ';') <> 0 then
        TitleImageAttributes.ImageIndex := 1;
    end;
  end;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.cbLoadAllClick(
  Sender: TObject);
begin
 if (rgPeriod.ItemIndex = 0) and (not cbLoadAll.Checked) and (not cbNotEnlisted.Checked) then
   cbSemiMthly.Enabled := True
 else
 begin
    cbSemiMthly.Enabled := False;
    cbSemiMthly.state := cbUnchecked;
 end;

 if (not cbLoadAll.Checked) then
    cbNotEnlisted.Enabled := True
 else
 begin
    cbNotEnlisted.Enabled := False;
    cbNotEnlisted.state := cbUnchecked;
 end;

 evMsg.Visible := True;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.rgPeriodClick(
  Sender: TObject);
begin
  if rgPeriod.ItemIndex = 0 then
  begin
   cbMonth.Items.CommaText := 'January,February,March,April,May,June,July,August,September,October,November,December';
   cbMonth.ItemIndex := GetMonth(Date)-1;
   if cbMonth.ItemIndex = 0 then
   begin
      edYear.Value := GetYear(Date)-1;
      cbMonth.ItemIndex := 11;
   end
   else
      edYear.Value := GetYear(Date);
  end
  else
  begin
   cbMonth.Items.Clear;
   cbMonth.Items.Add('1st Quarter');
   cbMonth.Items.Add('2nd Quarter');
   cbMonth.Items.Add('3rd Quarter');
   cbMonth.Items.Add('4th Quarter');
   cbMonth.ItemIndex := GetQuarterNumber(Date) - 1;
   if cbMonth.ItemIndex = 0 then
   begin
      edYear.Value := GetYear(Date)-1;
      cbMonth.ItemIndex := 3;
   end
   else
      edYear.Value := GetYear(Date);
  end;
end;
procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.cbSemiMthlyClick(
  Sender: TObject);
begin
  evMsg.Visible := True;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ENLIST_WIZARD.miSelectFromFileClick(
  Sender: TObject);
begin
  SPopupCompanySelectHelper.TPopupCompanySelectHelper.SelectCompanyListFromFile(grdCompanyList, nil);
end;

end.
