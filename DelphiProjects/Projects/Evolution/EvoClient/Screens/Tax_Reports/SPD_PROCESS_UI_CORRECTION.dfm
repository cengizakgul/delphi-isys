inherited PROCESS_UI_CORRECTION: TPROCESS_UI_CORRECTION
  object Pc: TevPageControl [0]
    Left = 0
    Top = 0
    Width = 695
    Height = 465
    ActivePage = ts1
    TabOrder = 0
    object ts1: TTabSheet
      Caption = 'Select companies'
      object cbMonth: TevComboBox
        Left = 8
        Top = 408
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        OnChange = cbMonthChange
        Items.Strings = (
          '1'#39'st quarter'
          '2'#39'nd quarter'
          '3'#39'rd quarter'
          '4'#39'th quarter')
      end
      object wwDBGrid1: TevDBGrid
        Left = 8
        Top = 0
        Width = 685
        Height = 401
        DisableThemesInTitle = False
        ControlType.Strings = (
          'LOAD;CheckBox;True;False'
          'TAX_SERVICE;CheckBox;Y;N')
        Selected.Strings = (
          'LOAD'#9'5'#9'Load'#9'F'
          'DESCR'#9'15'#9'Description'#9'T'
          'TAX_TYPE_DESCR'#9'5'#9'Tax type'#9'F'
          'CUSTOM_CLIENT_NUMBER'#9'10'#9'Client #'#9'T'
          'CLIENT_NAME'#9'25'#9'Client name'#9'T'
          'CUSTOM_COMPANY_NUMBER'#9'10'#9'Comp. #'#9'T'
          'COMPANY_NAME'#9'25'#9'Company name'#9'T'
          'TAX_SERVICE'#9'1'#9'Tax'#9'T')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TPROCESS_UI_CORRECTION\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
        DataSource = dsClCo
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        OnDblClick = wwDBGrid1DblClick
        PaintOptions.AlternatingRowColor = clCream
      end
      object cbLoadFUI: TevCheckBox
        Left = 240
        Top = 408
        Width = 65
        Height = 17
        Caption = 'Load FUI'
        TabOrder = 3
        Visible = False
      end
      object cbConsolidate: TevCheckBox
        Left = 552
        Top = 408
        Width = 63
        Height = 17
        Caption = 'Consolid.'
        Checked = True
        State = cbChecked
        TabOrder = 5
        Visible = False
      end
      object edYear: TevSpinEdit
        Left = 160
        Top = 408
        Width = 73
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 2
        Value = 0
      end
      object cbLoadMO: TevCheckBox
        Left = 308
        Top = 408
        Width = 65
        Height = 17
        Caption = 'Load MO'
        TabOrder = 4
      end
      object Button4: TevBitBtn
        Left = 376
        Top = 408
        Width = 75
        Height = 25
        Caption = 'Select all'
        TabOrder = 6
        OnClick = Button4Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDD0000
          008DDDDDDDDD888888FD0000DDDD0000008D8888DDDD888888FD00D0DDD80000
          008D88D8DDDF888888FD0D80DD80FFFF77088D88DDF8DDDDDD8F0000DD0FFFFF
          F7088888DD8DDDDDDD8FDDDDDD07FFFFF708DDDDDD8DDDDDDD8F0000DD80FFFF
          77088888DDD8DDDDDD8F00D0DDD0FF77008D88D8DDD8DDDD88FD0D80DDD0F000
          8DDD8D88DDD8D888FDDD0000DDD0F08DDDDD8888DDD8D8FDDDDDDDDDDDD0F08D
          DDDDDDDDDDD8D8FDDDDD0000DDD0F08DDDDD8888DDD8D8FDDDDD00D0DDD0F08D
          DDDD88D8DDD8D8FDDDDD0D80DDD808DDDDDD8D88DDDD8FDDDDDD0000DDDDDDDD
          DDDD8888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object Button5: TevBitBtn
        Left = 456
        Top = 408
        Width = 89
        Height = 25
        Caption = 'Unselect all'
        TabOrder = 7
        OnClick = Button5Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDD0000DDDDDDDDDDDD8888DDDDDDDDDDDD00D0DD88DDDD
          D88D88D8DDDDDDDDDDDD0D80DD008DDD80088D88DDFFDDDDDFFD0000D8FF08D8
          0FF88888DD88FDDDF88DDDDDD8FFF080FFF8DDDDDD888FDF888D0000DD8FFF0F
          FF8D8888DDD888F888DD00D0DDD8FFFFF8DD88D8DDDD88888DDD0D80DDD80FFF
          08DD8D88DDDDF888FDDD0000DD80FFFFF08D8888DDDF88888FDDDDDDD80FFF8F
          FF08DDDDDDF888D888FD0000D8FFF8D8FFF88888DD888DDD888D00D0D8FF8DDD
          8FF888D8DD88DDDDD88D0D80DD88DDDDD88D8D88DDDDDDDDDDDD0000DDDDDDDD
          DDDD8888DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object Button1: TevBitBtn
        Left = 616
        Top = 408
        Width = 67
        Height = 25
        Caption = 'Load'
        TabOrder = 8
        OnClick = Button1Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D44444444444
          4DDDD888888888888DDDD4FFFF7FFFFF4DDDD8DDDDDDDDDD8DDDD4FFF708FFFF
          4DDDD8DDDDFDDDDD8DDDD4FF70A08FFF4DDDD8DDDF8FDDDD8DDDD4F70AAA08FF
          4888D8DDF888FDDD8D88D470AAAAA08F4778D8DF88888FDD8DD8D47AAAAAAA08
          4778D8D8888888FD8DD8D4777AAA07774778D8DDD888FDDD8DD8D4FF7AAA0888
          4778D8DDD888FDDD8DD8D4FF7AAA00000078D8DDD888FFFFFFD8D4444AAAAAAA
          A078D888D88888888FD8D4444AAAAAAAA078D888D88888888FD8DDDD8AAAAAAA
          A078DDDDD88888888FD8DDDD877777777778DDDD8DDDDDDDDDD8DDDD88888888
          8888DDDD888888888888DDDD888888888888DDDD888888888888}
        NumGlyphs = 2
      end
    end
    object ts2: TTabSheet
      Caption = 'Selected tax for correction'
      ImageIndex = 1
      object wwDBGrid2: TevDBGrid
        Left = 0
        Top = 0
        Width = 685
        Height = 401
        DisableThemesInTitle = False
        ControlType.Strings = (
          'Do;CheckBox;True;False')
        Selected.Strings = (
          'NUMBER'#9'15'#9'Cust #'#9'T'
          'NAME'#9'35'#9'Name'#9'T'
          'TaxDesc'#9'17'#9'Taxdesc'#9'T'
          'TotalCalc'#9'10'#9'As is now'#9'T'
          'TotalReal'#9'10'#9'Should be'#9'T'
          'TotalDiff'#9'10'#9'Diff'#9'T'
          'Do'#9'5'#9'Correct'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TPROCESS_UI_CORRECTION\wwDBGrid2'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsTaxes
        ReadOnly = False
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object Button3: TevBitBtn
        Left = 512
        Top = 408
        Width = 75
        Height = 25
        Caption = 'Back'
        TabOrder = 1
        OnClick = Button3Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDA2DDDDDDDDDDDDDDFFDDDDDDDDDDDDAAA
          2DDDDDDDDDDDDFFD8DDDDDDDDDDAAAAA2DDDDDDDDDDFFDDD8DDDDDDDDAAAAAAA
          2DDDDDDDDFFDDDDD8DDDDDDAAAAAAAAA2DDDDDDFFDDDDDDD8DDDD222AAAAAAAA
          2DDDDFF8DDDDDDDD8DDDDD2222AAAAAA2DDDDD8888DDDDDD8DDDDDDD2222AAAA
          2DDDDDDD8888DDDD8DDDDDDDDD2222AA2DDDDDDDDD8888DD8DDDDDDDDDDD2222
          2DDDDDDDDDDD88888DDDDDDDDDDDDD222DDDDDDDDDDDDD888DDDDDDDDDDDDDDD
          2DDDDDDDDDDDDDDD8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        NumGlyphs = 2
      end
      object Button2: TevBitBtn
        Left = 608
        Top = 408
        Width = 75
        Height = 25
        Caption = 'Correct'
        TabOrder = 2
        OnClick = Button2Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDD08888DDDDDDDDDDD88888DDDDDDDDDDDF7FFF6088888
          8DDDDDDDD8F888888DDDFF7F676088888888DDDD888F8888888877FF6F760008
          8888DDDD8D88FFF8888888886FF76660D88888888DD8888FD888DDDDD6766666
          0DDDDDDDD8D88888FDDDDDDDDD67767660DDDDDDDD8DD8D88FDD000DDD676F77
          660D888DDD8D8D8D88FDDDD0DD66FFF77660DDD8DD88DDD8D88FD00DDDD67FFF
          7766D88DDDD88DDD8D88DD000DDD67FFF776DD888DDD88DDD8D8DDDDDDDDD67F
          FF77DDDDDDDDD88DDD8D000DD000DD67FFF7888DD888DD88DDD8DD0DD0DD0DD6
          7FFFDD8DD8DD8DD88DDDDDD0DD0DD0DD67FFDDD8DD8DD8DD88DD}
        NumGlyphs = 2
      end
    end
  end
  object cdClCo: TevClientDataSet
    ControlType.Strings = (
      'TAX_SERVICE;CheckBox;Y;N'
      'FUI_DO;CheckBox;True;False'
      'LOAD;CheckBox;True;False'
      'SUI_DO;CheckBox;True;False')
    Filtered = True
    Left = 40
    Top = 56
    object cdClCoLOAD: TBooleanField
      DisplayLabel = 'Load'
      DisplayWidth = 5
      FieldKind = fkInternalCalc
      FieldName = 'LOAD'
    end
    object cdClCoDESCRI: TStringField
      FieldName = 'DESCR'
      Size = 15
    end
    object cdClCoCUSTOM_CLIENT_NUMBER: TStringField
      DisplayLabel = 'Client #'
      DisplayWidth = 7
      FieldName = 'CUSTOM_CLIENT_NUMBER'
    end
    object cdClCoCLIENT_NAME: TStringField
      DisplayLabel = 'Client name'
      DisplayWidth = 15
      FieldName = 'CLIENT_NAME'
      Size = 40
    end
    object cdClCoCUSTOM_COMPANY_NUMBER: TStringField
      DisplayLabel = 'Comp. #'
      DisplayWidth = 7
      FieldName = 'CUSTOM_COMPANY_NUMBER'
    end
    object cdClCoCOMPANY_NAME: TStringField
      DisplayLabel = 'Company name'
      DisplayWidth = 15
      FieldName = 'COMPANY_NAME'
      Size = 40
    end
    object cdClCoTAX_SERVICE: TStringField
      DisplayLabel = 'Tax'
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      FixedChar = True
      Size = 1
    end
    object cdClCoCL_NBR: TIntegerField
      FieldName = 'CL_NBR'
      Visible = False
    end
    object cdClCoCO_NBR: TIntegerField
      FieldName = 'CO_NBR'
      Visible = False
    end
    object cdClCoTAX_TYPE: TStringField
      FieldName = 'TAX_TYPE'
      Size = 1
    end
    object cdClCoTAX_TYPE_DESCR: TStringField
      FieldName = 'TAX_TYPE_DESCR'
      Size = 5
    end
  end
  object dsClCo: TevDataSource
    DataSet = cdClCo
    Left = 56
  end
  object cdCorrections: TevClientDataSet
    Left = 384
    object cdCorrectionsNUMBER: TStringField
      FieldName = 'NUMBER'
    end
    object cdCorrectionsNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object cdCorrectionsTaxType: TStringField
      FieldName = 'TaxType'
      Size = 3
    end
    object cdCorrectionsNbr: TIntegerField
      FieldName = 'Nbr'
    end
    object cdCorrectionsTaxDesc: TStringField
      DisplayWidth = 15
      FieldName = 'TaxDesc'
    end
    object cdCorrectionsTotalCalc: TCurrencyField
      FieldName = 'TotalCalc'
    end
    object cdCorrectionsTotalReal: TCurrencyField
      FieldName = 'TotalReal'
    end
    object cdCorrectionsTotalDiff: TCurrencyField
      FieldName = 'TotalDiff'
    end
    object cdCorrectionsDo: TBooleanField
      FieldName = 'Do'
    end
    object cdCorrectionscl_nbr: TIntegerField
      FieldName = 'cl_nbr'
    end
    object cdCorrectionsco_nbr: TIntegerField
      FieldName = 'co_nbr'
    end
    object cdCorrectionsDone: TBooleanField
      FieldName = 'Done'
    end
    object cdCorrectionsTAX_SERVICE: TStringField
      DisplayLabel = 'Tax'
      DisplayWidth = 1
      FieldName = 'TAX_SERVICE'
      FixedChar = True
      Size = 1
    end
  end
  object dsTaxes: TevDataSource
    DataSet = cdCorrections
    Left = 440
  end
end
