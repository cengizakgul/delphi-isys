// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ISUtils,
  ExtCtrls,  SBalanceInformation, StdCtrls, EvTypes, EvConsts, EvContext,
  ISBasicClasses, EvExceptions, EvUIComponents, EvClientDataSet;

type
  TEDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG = class(TForm)
    bOk: TevButton;
    EvBevel1: TEvBevel;
    Memo: TevMemo;
    evImage1: TevImage;
    bPrint: TevButton;
    procedure FormShow(Sender: TObject);
    procedure bPrintClick(Sender: TObject);
  private
    { Private declarations }
    FLog: TStringList;
  public
    { Public declarations }
    class procedure ShowLog(Log: TStringList);
  end;
  TEDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG_Record = class
    ClNbr, CoNbr: Integer;
    CoString: string;
    BalanceDM: TBalanceDM;
  end;

implementation

{$R *.DFM}

uses SReportSettings, EvUtils, SDataStructure, Db;

{ TEDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG }

class procedure TEDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG.ShowLog(
  Log: TStringList);
var
  f: TEDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG;
begin
  f := TEDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG.Create(nil);
  try
    f.FLog := Log;
    f.ShowModal;
  finally
    f.Free;
  end;
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG.FormShow(Sender: TObject);
begin
  Memo.Lines.Assign(FLog);
end;

procedure TEDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG.bPrintClick(Sender: TObject);
var
  cd, cd2: TevClientDataSet;
  i: Integer;
  ReportParams: TrwReportParams;
begin
  ReportParams := TrwReportParams.Create;
  cd := TevClientDataSet.Create(nil);
  cd2 := TevClientDataSet.Create(nil);
  try
    cd2.FieldDefs.Add('ClNbr', ftInteger);
    cd2.FieldDefs.Add('CoNbr', ftInteger);
    cd2.FieldDefs.Add('CoString', ftString, 40);
    cd2.FieldDefs.Add('Reason', ftString, 80);
    cd2.CreateDataSet;
    cd2.LogChanges := False;
    
    DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.DataRequired('ALL');
    if not DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.Locate('REPORT_DESCRIPTION', 'Pre-Process Log Report', [loCaseInsensitive]) then
      raise EMissingReport.CreateHelp('"Pre-Process Log Report" report does not exist in the database!', IDH_MissingReport);

    for i := 0 to FLog.Count- 1 do
      with TEDIT_TMP_CO_RETURN_QUEUE_ERROR_LOG_Record(FLog.Objects[i]) do
      begin
        cd2.AppendRecord([ClNbr, CoNbr, CoString, FLog[i]]);
        if Assigned(BalanceDM) then
          cd.AppendData(CreateLookupData(BalanceDM.cdsSummary), False);
      end;

    ReportParams.Add('DataSets', DataSetsToVarArray([cd2, cd]));

    ctx_RWLocalEngine.StartGroup;
    try
      ctx_RWLocalEngine.CalcPrintReport(DM_SYSTEM_MISC.SY_REPORT_WRITER_REPORTS.FieldByName('SY_REPORT_WRITER_REPORTS_NBR').Value,
         CH_DATABASE_SYSTEM, ReportParams);
    finally
      ctx_RWLocalEngine.EndGroup(rdtPreview);
    end;

  finally
    cd2.Free;
    cd.Free;
    ReportParams.Free;
  end;
end;

end.
