// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SCompanyMuliSelectScreen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db,   Wwdatsrc, StdCtrls, Grids, EvConsts,
  Wwdbigrd, Wwdbgrid, EvTypes, Variants, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, ISBasicClasses, EvExceptions,
  EvContext, EvUIComponents, EvClientDataSet, LMDCustomButton, LMDButton,
  isUILMDButton, Menus
  ,SPopupCompanySelectHelper
  ;

type
  TCompanyMuliSelectScreen = class(TForm)
    wwDBGrid1: TevDBGrid;
    bOk: TevButton;
    bCancel: TevButton;
    dsClCo: TevDataSource;
    cdClCo: TevClientDataSet;
    cdClCoLOAD: TBooleanField;
    cdClCoDESCRI: TStringField;
    cdClCoCUSTOM_CLIENT_NUMBER: TStringField;
    cdClCoCLIENT_NAME: TStringField;
    cdClCoCUSTOM_COMPANY_NUMBER: TStringField;
    cdClCoCOMPANY_NAME: TStringField;
    cdClCoTAX_SERVICE: TStringField;
    cdClCoCL_NBR: TIntegerField;
    cdClCoCO_NBR: TIntegerField;
    bUnSelectAll: TevButton;
    bSelectAll: TevButton;
    cdClCocbDetailSystemReportLookupTableSY_REPORTS_GROUP_NBR: TIntegerField;
    PopupMenu1: TPopupMenu;
    LoadCompaniesfromTextFile1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure bOkClick(Sender: TObject);
    procedure UpdateAll(const Value: Boolean);
    procedure bSelectAllClick(Sender: TObject);
    procedure bUnSelectAllClick(Sender: TObject);
    procedure LoadCompaniesfromTextFile1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
  sCompany = 'Company';
  sConsolidation = 'Consolidation';

implementation

{$R *.DFM}

uses
  EvUtils;

procedure TCompanyMuliSelectScreen.FormCreate(Sender: TObject);
var
  d: TevClientDataSet;
  procedure AddRecord(const Consolid: string);
  var
    i: Integer;
    f: TField;
  begin
    cdClCo.Append;
    for i := 0 to cdClCo.FieldCount-1 do
    begin
      f := d.FindField(cdClCo.Fields[i].FieldName);
      if Assigned(f) then
        cdClCo.Fields[i].Assign(f);
    end;
    cdClCo['DESCR'] := Consolid;
    if Consolid = sCompany then
      cdClCo['CL_CO_CONSOLIDATION_NBR'] := Null;
    cdClCo['LOAD'] := False;
    cdClCo.Post;
  end;
begin
  cdClCo.CreateDataSet;
  cdClCo.LogChanges := False;
  d := ctx_DataAccess.TEMP_CUSTOM_VIEW;
  with d do
  begin
    Close;
    with TExecDSWrapper.Create('ConsolidationsFull') do
    begin
      SetMacro('CONSCOND', 'and n.SCOPE='''+ CONSOLIDATION_SCOPE_ALL+ '''');
      DataRequest(AsVariant);
    end;
    Open;
    First;
    while not Eof do
    begin
      if VarIsNull(FieldValues['CL_CO_CONSOLIDATION_NBR']) then
        AddRecord(sCompany)
      else
      begin
        AddRecord(sCompany);
        if FieldValues['CO_NBR'] = FieldValues['PRIMARY_CO_NBR'] then
          AddRecord(sConsolidation);
      end;
      Next;
    end;
    Close;
  end;
  cdClCo.First;
end;

procedure TCompanyMuliSelectScreen.bOkClick(Sender: TObject);
var
  bMarked: Boolean;
begin
  bMarked := False;
  cdClCo.DisableControls;
  try
    cdClCo.First;
    while not cdClCo.Eof do
    begin
      if cdClCo['LOAD'] then
      begin
        bMarked := True;
        Break;
      end;
      cdClCo.Next;
    end;
  finally
    cdClCo.EnableControls;
  end;
  if bMarked then
    ModalResult := mrOk
  else
    raise EInvalidParameters.CreateHelp('You have to mark at least one company', IDH_InvalidParameters);
end;

procedure TCompanyMuliSelectScreen.UpdateAll(const Value: Boolean);
var
  d: TevClientDataSet;
  s: string;
begin
  s := cdClCo.IndexName;
  cdClCo.DisableControls;
  d := TevClientDataSet.Create(nil);
  with d do
  try
    cdClCo.IndexName := '';
    CloneCursor(cdClCo, True);
    Resync([]);
    First;
    while not Eof do
    begin
      Edit;
      FieldValues['LOAD'] := Value;
      Post;
      Next;
    end;
  finally
    Free;
    cdClCo.IndexName := s;
    cdClCo.EnableControls;
  end;
end;

procedure TCompanyMuliSelectScreen.bSelectAllClick(Sender: TObject);
begin
  UpdateAll(True);
end;

procedure TCompanyMuliSelectScreen.bUnSelectAllClick(Sender: TObject);
begin
  UpdateAll(False);
end;

procedure TCompanyMuliSelectScreen.LoadCompaniesfromTextFile1Click(
  Sender: TObject);
begin
  TPopupCompanySelectHelper.SelectCompanyListFromFile(wwDBGrid1, cdClCoLOAD);
end;

end.
