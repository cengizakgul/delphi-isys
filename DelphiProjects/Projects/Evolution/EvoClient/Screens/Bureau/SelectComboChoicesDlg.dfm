object frmSelectComboChoices: TfrmSelectComboChoices
  Left = 755
  Top = 335
  BorderStyle = bsDialog
  Caption = 'Select ACA Status to be reviewed for Forms Type update'
  ClientHeight = 198
  ClientWidth = 417
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    417
    198)
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 130
    Top = 161
    Width = 76
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 215
    Top = 161
    Width = 76
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object gbACAStatus: TevGroupBox
    Left = 8
    Top = 7
    Width = 400
    Height = 147
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = 'ACA Status'
    TabOrder = 2
  end
end
