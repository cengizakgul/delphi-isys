object TEDIT_SELF_ADJUST_FLAG: TTEDIT_SELF_ADJUST_FLAG
  Left = 247
  Top = 218
  Width = 962
  Height = 656
  Caption = 'Import SUI Rates'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel2: TevPanel
    Left = 0
    Top = 0
    Width = 946
    Height = 77
    Align = alTop
    TabOrder = 0
    object evLabel3: TevLabel
      Left = 24
      Top = 16
      Width = 78
      Height = 13
      Caption = 'Input File Folder '
    end
    object spdImportFile: TevSpeedButton
      Left = 534
      Top = 13
      Width = 25
      Height = 25
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
        0000008484000084840000848400008484000084840000848400008484000084
        84000084840000000000FF00FF00FF00FF00FF00FF00FF00FF000000000000FF
        FF00000000000084840000848400008484000084840000848400008484000084
        8400008484000084840000000000FF00FF00FF00FF00FF00FF0000000000FFFF
        FF0000FFFF000000000000848400008484000084840000848400008484000084
        840000848400008484000084840000000000FF00FF00FF00FF000000000000FF
        FF00FFFFFF0000FFFF0000000000008484000084840000848400008484000084
        84000084840000848400008484000084840000000000FF00FF0000000000FFFF
        FF0000FFFF00FFFFFF0000FFFF00000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000000000000000FF
        FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000FF
        FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
        000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00000000000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00
        FF00FF00FF00FF00FF0000000000FF00FF0000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
        00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Margin = 3
      OnClick = spdImportFileClick
      ShortCut = 0
    end
    object evLabel1: TevLabel
      Left = 96
      Top = 47
      Width = 375
      Height = 13
      Caption = 'The following companies will be updated when you click APPLY.  '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ImportFileEdit: TevEdit
      Left = 114
      Top = 14
      Width = 410
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object btnApply: TevBitBtn
      Left = 744
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Apply'
      TabOrder = 1
      OnClick = btnApplyClick
    end
    object btnCancel: TevBitBtn
      Left = 856
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Cancel'
      TabOrder = 2
      OnClick = btnCancelClick
    end
  end
  object evPanel1: TevPanel
    Left = 0
    Top = 77
    Width = 946
    Height = 541
    Align = alClient
    Caption = 'evPanel1'
    TabOrder = 1
    object ImportSUIGrid: TevDBGrid
      Left = 1
      Top = 1
      Width = 944
      Height = 539
      DisableThemesInTitle = False
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TTEDIT_SELF_ADJUST_FLAG\ImportSUIGrid'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = ImportSelfAdjustFlagDataSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgProportionalColResize, dgDblClickColSizing]
      ParentFont = False
      TabOrder = 0
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      UseTFields = True
      PaintOptions.AlternatingRowColor = clCream
    end
  end
  object ImportFileDlg: TOpenDialog
    Filter = 
      'Comma delimited file (*.csv)|*.csv|Comma delimited file (*.txt)|' +
      '*.txt'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 600
    Top = 8
  end
  object ImportSelfAdjustFlagDataSource: TDataSource
    Left = 689
    Top = 9
  end
end
