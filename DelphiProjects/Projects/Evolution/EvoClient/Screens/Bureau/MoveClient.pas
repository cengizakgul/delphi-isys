unit MoveClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, wwdblook, ISBasicClasses,  SDDClasses,
  SDataStructure, EvUtils, DB, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, Wwdatsrc,
  Grids, Wwdbigrd, Wwdbgrid, ComCtrls, iniFiles, SDataDictclient,
  SDataDictbureau, EvContext, EvUIComponents, EvClientDataSet,
  isUIwwDBLookupCombo;

type
  TFormMoveClient = class(TForm)
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_CLIENT: TDM_CLIENT;
    wwlcAccountant_Number: TevDBLookupCombo;
    lablCustomer_Service_Team: TevLabel;
    wwlcCustomer_Service_Team: TevDBLookupCombo;
    wwlcCustomer_Service_Rep: TevDBLookupCombo;
    lablCustomer_Service_Rep: TevLabel;
    wwDBLookupCombo1: TevDBLookupCombo;
    Label1x: TevLabel;
    lablDelivery_Company: TevLabel;
    lablCover_Sheet_Report: TevLabel;
    evLabel1: TevLabel;
    evGroupBox1: TevGroupBox;
    evGroupBox2: TevGroupBox;
    evLabel4: TevLabel;
    edMedia2: TevDBLookupCombo;
    evLabel9: TevLabel;

    cdsSbBankAccountsForTrust: TevClientDataSet;
    cdsSbBankAccountsForTax: TevClientDataSet;
    cdsSbBankAccountsForBilling: TevClientDataSet;
    cdsSbBankAccountsForWC: TevClientDataSet;
    cdsSbBankAccountsForOBC: TevClientDataSet;
    evGroupBox3: TevGroupBox;
    DM_COMPANY: TDM_COMPANY;
    cdTypes: TevClientDataSet;
    cdTypesSB_DELIVERY_METHOD_NBR: TIntegerField;
    cdTypesCLASS_NAME: TStringField;
    cdTypesNAME: TStringField;
    evDataSource2: TevDataSource;
    evGroupBox5: TevGroupBox;
    evDBGrid1: TevDBGrid;
    evDataSource1: TevDataSource;
    evGroupBox6: TevGroupBox;
    evLabel8: TevLabel;
    evDBGrid3: TevDBGrid;
    evGroupBox7: TevGroupBox;
    evLabel10: TevLabel;
    evDBGrid5: TevDBGrid;
    evDBLookupCombo4: TevDBLookupCombo;
    evDataSource3: TevDataSource;
    evDataSource4: TevDataSource;
    evDataSource5: TevDataSource;
    evGroupBox8: TevGroupBox;
    evLabel11: TevLabel;
    evDBGrid6: TevDBGrid;
    evGroupBox9: TevGroupBox;
    evLabel12: TevLabel;
    evDBGrid8: TevDBGrid;
    evDBLookupCombo6: TevDBLookupCombo;
    evPageControl1: TevPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    evDBLookupCombo7: TevDBLookupCombo;
    DS_Service: TevClientDataSet;
    DS_ServiceSB_SERVICES_NBR: TIntegerField;
    evGroupBox11: TevGroupBox;
    evLabel6: TevLabel;
    evDBGrid13: TevDBGrid;
    DS_banks: TevClientDataSet;
    StringField1: TStringField;
    evDataSource7: TevDataSource;
    DS_banksOLD_SB_BANK_ACCOUNT_NBR: TIntegerField;
    DS_bankstype: TStringField;
    DS_ServiceOLD_SB_SERVICES_NBR: TIntegerField;
    DS_ServiceNAME: TStringField;
    DS_USER: TevClientDataSet;
    StringField2: TStringField;
    DS_USEROLD_SB_USER_NBR: TIntegerField;
    DS_USERSB_USER_NBR: TIntegerField;
    cdsSbBankAccountsForAch: TevClientDataSet;
    evDBLookupCombo5: TevDBLookupCombo;
    DS_banksSB_BANK_ACCOUNT_NBR: TIntegerField;
    evGroupBox12: TevGroupBox;
    evLabel7: TevLabel;
    evDBGrid15: TevDBGrid;
    evDBLookupCombo1: TevDBLookupCombo;
    evDBLookupCombo3: TevDBLookupCombo;
    DS_Accountant: TevClientDataSet;
    StringField3: TStringField;
    evDataSource8: TevDataSource;
    DS_AccountantOLD_SB_ACCOUNTANT_NBR: TIntegerField;
    DS_AccountantSB_ACCOUNTANT_NBR: TIntegerField;
    evGroupBox13: TevGroupBox;
    evLabel5: TevLabel;
    evDBGrid17: TevDBGrid;
    wwlcSB_Referrals_Nbr: TevDBLookupCombo;
    DS_Referrals: TevClientDataSet;
    StringField4: TStringField;
    evDataSource9: TevDataSource;
    DS_ReferralsOLD_SB_REFERRALS_NBR: TIntegerField;
    DS_ReferralsSB_REFERRALS_NBR: TIntegerField;
    Button1: TButton;
    DS_Client: TevClientDataSet;
    evDataSource10: TevDataSource;
    DS_ClientOLD_SB_ACCOUNTANT_NBR: TIntegerField;
    DS_ClientSB_ACCOUNTANT_NBR: TIntegerField;
    DS_ClientOLD_CUST_SERVICE_SB_USER_NBR: TIntegerField;
    DS_ClientCUST_SERVICE_SB_USER_NBR: TIntegerField;
    DS_ClientOLD_CSR_SB_TEAM_NBR: TIntegerField;
    DS_ClientCSR_SB_TEAM_NBR: TIntegerField;
    edType: TevDBLookupCombo;
    evDataSource11: TevDataSource;
    evDBGrid7: TevDBGrid;
    evDataSource12: TevDataSource;
    DS_bank: TevClientDataSet;
    evDataSource13: TevDataSource;
    DS_bankOLD_SB_BANKS_NBR: TIntegerField;
    DS_bankSB_BANKS_NBR: TIntegerField;
    DS_agency: TevClientDataSet;
    evDataSource14: TevDataSource;
    DS_agencyOLD_SB_AGENCY_NBR: TIntegerField;
    DS_agencySB_AGENCY_NBR: TIntegerField;
    DS_delivery_service: TevClientDataSet;
    evDataSource6: TevDataSource;
    DS_delivery_serviceNAME: TStringField;
    DS_delivery_serviceSB_DELIVERY_COMPANY_SVCS_NBR: TIntegerField;
    DS_delivery_serviceOLD_SB_DELIVERY_SVCS_NBR: TIntegerField;
    DS_bankBANKACCOUNTNBR: TStringField;
    Button2: TButton;
    SaveDialog1: TSaveDialog;
    Button3: TButton;
    OpenDialog1: TOpenDialog;
    evDBGrid2: TevDBGrid;
    evDBGrid4: TevDBGrid;
    DS_media: TevClientDataSet;
    evDataSource15: TevDataSource;
    evDataSource16: TevDataSource;
    DS_method: TevClientDataSet;
    DS_mediaDESCRIPTION: TStringField;
    DS_mediaOLD_SB_MEDIA_TYPE_NBR: TIntegerField;
    DS_mediaSB_MEDIA_TYPE_NBR: TIntegerField;
    DS_methodDESCRIPTION: TStringField;
    DS_methodOLD_SB_DELIVERY_METHOD_NBR: TIntegerField;
    DS_methodSB_DELIVERY_METHOD_NBR: TIntegerField;

    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PostChange(Sender: TObject;
      LookupTable, FillTable: TDataSet; modified: Boolean);
    procedure evDBGrid4DrawDataCell(Sender: TObject; const Rect: TRect;
      Field: TField; State: TGridDrawState);
    procedure evDBLookupCombo5BeforeDropDown(Sender: TObject);
    procedure evDBLookupCombo5Change(Sender: TObject);
    procedure wwlcSB_Referrals_NbrBeforeDropDown(Sender: TObject);
    procedure wwlcAccountant_NumberCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure wwlcCustomer_Service_RepCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure wwlcCustomer_Service_TeamCloseUp(Sender: TObject;
      LookupTable, FillTable: TDataSet; modified: Boolean);
    procedure edMediaCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure edTypeCloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure evType2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure edMedia2CloseUp(Sender: TObject; LookupTable,
      FillTable: TDataSet; modified: Boolean);
    procedure Button3Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;
function findlength(value_ini, value_dataset: string): integer;
implementation

{$R *.dfm}

procedure TFormMoveClient.FormCreate(Sender: TObject);
begin
  DM_SERVICE_BUREAU.SB_ACCOUNTANT.DataRequired();
  DM_SERVICE_BUREAU.SB_TEAM.DataRequired();
  DM_SERVICE_BUREAU.SB_USER.DataRequired();
  DM_SERVICE_BUREAU.SB_AGENCY.DataRequired();

  DM_SERVICE_BUREAU.SB_REPORTS.DataRequired();
  DM_SERVICE_BUREAU.SB_MEDIA_TYPE.DataRequired();
  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.DataRequired();
  DM_SERVICE_BUREAU.SB_DELIVERY_COMPANY_SVCS.DataRequired();
  DM_CLIENT.CL_BANK_ACCOUNT.DataRequired('');
  DM_SERVICE_BUREAU.SB_OTHER_SERVICE.DataRequired();
  DM_SERVICE_BUREAU.SB_REFERRALS.DataRequired();
  DM_SERVICE_BUREAU.SB_SERVICES.DataRequired();
  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.DataRequired();
  DM_CLIENT.CL.DataRequired('');
  DM_CLIENT.CL_AGENCY.DataRequired('');
  DM_SERVICE_BUREAU.SB_BANKS.DataRequired();
  DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.DataRequired();
  DM_CLIENT.CL_3_PARTY_SICK_PAY_ADMIN.DataRequired('');
  DM_CLIENT.CL_DELIVERY_METHOD.DataRequired('');
  DM_CLIENT.CO_SERVICES.DataRequired('');
  DM_CLIENT.CO_SALESPERSON.DataRequired('');

  DS_Client.CreateDataSet;
  DS_Client.LogChanges := false;

  DS_Client.appendRecord([DM_CLIENT.CL['SB_ACCOUNTANT_NUMBER'], DM_CLIENT.CL['SB_ACCOUNTANT_NUMBER'],
    DM_CLIENT.CL['CUSTOMER_SERVICE_SB_USER_NBR'], DM_CLIENT.CL['CUSTOMER_SERVICE_SB_USER_NBR'],
      DM_CLIENT.CL['CSR_SB_TEAM_NBR'], DM_CLIENT.CL['CSR_SB_TEAM_NBR']]); //,
    {  DM_CLIENT.CL_MAIL_BOX_GROUP['PRIM_SB_DELIVERY_METHOD_NBR'], DM_CLIENT.CL_MAIL_BOX_GROUP['PRIM_SB_DELIVERY_METHOD_NBR'],
      DM_CLIENT.CL_MAIL_BOX_GROUP['PRIM_SB_MEDIA_TYPE_NBR'], DM_CLIENT.CL_MAIL_BOX_GROUP['PRIM_SB_MEDIA_TYPE_NBR'],
      DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_SB_DELIVERY_METHOD_NBR'], DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_SB_DELIVERY_METHOD_NBR'],
      DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_SB_MEDIA_TYPE_NBR'], DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_SB_MEDIA_TYPE_NBR']]); }
  if not VarIsNull(DM_CLIENT.CL['SB_ACCOUNTANT_NUMBER']) then
    wwlcAccountant_Number.LookupTable := DM_SERVICE_BUREAU.SB_ACCOUNTANT;
  if not VarIsNull(DM_CLIENT.CL['CUSTOMER_SERVICE_SB_USER_NBR']) then
    wwlcCustomer_Service_Rep.LookupTable := DM_SERVICE_BUREAU.SB_USER;
  if not VarIsNull(DM_CLIENT.CL['CSR_SB_TEAM_NBR']) then
    wwlcCustomer_Service_Team.LookupTable := DM_SERVICE_BUREAU.SB_TEAM;

  DM_CLIENT.CL_MAIL_BOX_GROUP.DataRequired('');

  cdTypes.CreateDataSet;
  cdTypes.LogChanges := False;
  DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.First;
  while not DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Eof do
  begin
    cdTypes.AppendRecord([DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['SB_DELIVERY_METHOD_NBR'],
      DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['CLASS_NAME'],
        DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['luServiceName'] + ' - ' +
        DM_SERVICE_BUREAU.SB_DELIVERY_METHOD['luMethodName']]);
    DM_SERVICE_BUREAU.SB_DELIVERY_METHOD.Next;
  end;
  cdTypes.IndexFieldNames := 'NAME';

  edMedia2.LookupTable := DM_SERVICE_BUREAU.SB_MEDIA_TYPE;
  DS_media.CreateDataSet;
  DS_media.LogChanges := False;
  DS_method.CreateDataSet;
  DS_method.LogChanges := False;
  DM_CLIENT.CL_MAIL_BOX_GROUP.First;
  while not DM_CLIENT.CL_MAIL_BOX_GROUP.Eof do
  begin
    if not DS_media.Locate('SB_MEDIA_TYPE_NBR', DM_CLIENT.CL_MAIL_BOX_GROUP.fieldbyname('PRIM_SB_MEDIA_TYPE_NBR').Value, []) then
      DS_media.AppendRecord([DM_CLIENT.CL_MAIL_BOX_GROUP['DESCRIPTION'],
        DM_CLIENT.CL_MAIL_BOX_GROUP['PRIM_SB_MEDIA_TYPE_NBR'], DM_CLIENT.CL_MAIL_BOX_GROUP['PRIM_SB_MEDIA_TYPE_NBR']]);
    if not DS_media.Locate('SB_MEDIA_TYPE_NBR', DM_CLIENT.CL_MAIL_BOX_GROUP.fieldbyname('SEC_SB_MEDIA_TYPE_NBR').Value, []) then
      DS_media.AppendRecord([DM_CLIENT.CL_MAIL_BOX_GROUP['DESCRIPTION'],
        DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_SB_MEDIA_TYPE_NBR'], DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_SB_MEDIA_TYPE_NBR']]);
    if not DS_method.Locate('SB_DELIVERY_METHOD_NBR', DM_CLIENT.CL_MAIL_BOX_GROUP.fieldbyname('PRIM_SB_DELIVERY_METHOD_NBR').Value, []) then
      DS_method.AppendRecord([DM_CLIENT.CL_MAIL_BOX_GROUP['DESCRIPTION'],
        DM_CLIENT.CL_MAIL_BOX_GROUP['PRIM_SB_DELIVERY_METHOD_NBR'], DM_CLIENT.CL_MAIL_BOX_GROUP['PRIM_SB_DELIVERY_METHOD_NBR']]);
    if not DS_method.Locate('SB_DELIVERY_METHOD_NBR', DM_CLIENT.CL_MAIL_BOX_GROUP.fieldbyname('SEC_SB_DELIVERY_METHOD_NBR').Value, []) then
      DS_method.AppendRecord([DM_CLIENT.CL_MAIL_BOX_GROUP['DESCRIPTION'],
        DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_SB_DELIVERY_METHOD_NBR'], DM_CLIENT.CL_MAIL_BOX_GROUP['SEC_SB_DELIVERY_METHOD_NBR']]);
    DM_CLIENT.CL_MAIL_BOX_GROUP.Next;
  end;
  DM_CLIENT.CL_MAIL_BOX_GROUP.first;

  edType.LookupTable := cdTypes;

  DS_service.CreateDataSet;
  DS_service.LogChanges := False;
  DM_CLIENT.CO_SERVICES.First;
  while not DM_CLIENT.CO_SERVICES.Eof do
  begin
    if not DS_service.Locate('SB_SERVICES_NBR', DM_CLIENT.CO_SERVICES.fieldbyname('SB_SERVICES_NBR').Value, []) then
      DS_service.AppendRecord([DM_CLIENT.CO_SERVICES['SB_SERVICES_NBR'],
        DM_CLIENT.CO_SERVICES['NAME'], DM_CLIENT.CO_SERVICES['SB_SERVICES_NBR']]);

    DM_CLIENT.CO_SERVICES.Next;
  end;
  DM_CLIENT.CO_SERVICES.first;

  DS_agency.CreateDataSet;
  DS_agency.LogChanges := False;
  DM_CLIENT.CL_AGENCY.First;
  while not DM_CLIENT.CL_AGENCY.Eof do
  begin
    if not DS_agency.Locate('SB_AGENCY_NBR', DM_CLIENT.CL_AGENCY.fieldbyname('SB_AGENCY_NBR').Value, []) then
      DS_agency.AppendRecord([DM_CLIENT.CL_AGENCY['SB_AGENCY_NBR'],
        DM_CLIENT.CL_AGENCY['SB_AGENCY_NBR']]);

    DM_CLIENT.CL_AGENCY.Next;
  end;
  DM_CLIENT.CL_3_PARTY_SICK_PAY_ADMIN.First;
  while not DM_CLIENT.CL_3_PARTY_SICK_PAY_ADMIN.Eof do
  begin
    if not DS_agency.Locate('SB_AGENCY_NBR', DM_CLIENT.CL_3_PARTY_SICK_PAY_ADMIN.fieldbyname('SB_AGENCY_NBR').Value, []) then
      DS_agency.AppendRecord([DM_CLIENT.CL_3_PARTY_SICK_PAY_ADMIN['SB_AGENCY_NBR'],
        DM_CLIENT.CL_3_PARTY_SICK_PAY_ADMIN['SB_AGENCY_NBR']]);

    DM_CLIENT.CL_3_PARTY_SICK_PAY_ADMIN.Next;
  end;
  DS_agency.first;

  DS_delivery_service.CreateDataSet;
  DS_delivery_service.LogChanges := False;
  DM_CLIENT.CL_DELIVERY_METHOD.First;
  while not DM_CLIENT.CL_AGENCY.Eof do
  begin
    if not DS_delivery_service.Locate('SB_DELIVERY_COMPANY_SVCS_NBR', DM_CLIENT.CL_DELIVERY_METHOD.fieldbyname('SB_DELIVERY_COMPANY_SVCS_NBR').Value, []) then
      DS_delivery_service.AppendRecord([DM_CLIENT.CL_DELIVERY_METHOD['NAME'], DM_CLIENT.CL_DELIVERY_METHOD['SB_DELIVERY_COMPANY_SVCS_NBR'],
        DM_CLIENT.CL_DELIVERY_METHOD['SB_DELIVERY_COMPANY_SVCS_NBR']]);

    DM_CLIENT.CL_DELIVERY_METHOD.Next;
  end;
  DS_delivery_service.first;
  evDBLookupCombo7.LookupTable := DM_SERVICE_BUREAU.SB_DELIVERY_COMPANY_SVCS;
  evDBLookupCombo4.LookupTable := DM_SERVICE_BUREAU.SB_AGENCY;
  DS_bank.CreateDataSet;
  DS_bank.LogChanges := False;
  DM_CLIENT.CL_BANK_ACCOUNT.First;
  while not DM_CLIENT.CL_BANK_ACCOUNT.Eof do
  begin
    if not DS_bank.Locate('SB_BANKS_NBR', DM_CLIENT.CL_BANK_ACCOUNT.fieldbyname('SB_BANKS_NBR').Value, []) then
      DS_bank.AppendRecord([DM_CLIENT.CL_BANK_ACCOUNT['CUSTOM_BANK_ACCOUNT_NUMBER'],
     //   DM_CLIENT.CL_BANK_ACCOUNT['BANK_NAME'],DM_CLIENT.CL_BANK_ACCOUNT['ABA_NBR_LOOKUP'],
        DM_CLIENT.CL_BANK_ACCOUNT['SB_BANKS_NBR'],
          DM_CLIENT.CL_BANK_ACCOUNT['SB_BANKS_NBR']]);

    DM_CLIENT.CL_BANK_ACCOUNT.Next;
  end;
  DM_CLIENT.CL_BANK_ACCOUNT.first;

  wwDBLookupCombo1.LookupTable := DM_SERVICE_BUREAU.SB_BANKS;

  DS_BANKS.CreateDataSet;
  DS_BANKS.LogChanges := False;
  DS_accountant.CreateDataSet;
  DS_accountant.LogChanges := False;
  DS_referrals.CreateDataSet;
  DS_referrals.LogChanges := False;
  DS_user.CreateDataSet;
  DS_user.LogChanges := False;
  DM_CLIENT.CO.First;
  while not DM_CLIENT.CO.Eof do
  begin
    if not VarIsNull(DM_CLIENT.CO['ACH_SB_BANK_ACCOUNT_NBR']) then
      if not DS_BANKS.Locate('SB_BANK_ACCOUNT_NBR', DM_CLIENT.CO.fieldbyname('ACH_SB_BANK_ACCOUNT_NBR').Value, []) then
        DS_BANKS.AppendRecord([DM_CLIENT.CO['ACH_SB_BANK_ACCOUNT_NBR'],
          DM_CLIENT.CO['NAME'], DM_CLIENT.CO['ACH_SB_BANK_ACCOUNT_NBR'], 'ACH']);
    if not VarIsNull(DM_CLIENT.CO['BILLING_SB_BANK_ACCOUNT_NBR']) then
      if not DS_BANKS.Locate('SB_BANK_ACCOUNT_NBR', DM_CLIENT.CO.fieldbyname('BILLING_SB_BANK_ACCOUNT_NBR').Value, []) then
        DS_BANKS.AppendRecord([DM_CLIENT.CO['BILLING_SB_BANK_ACCOUNT_NBR'],
          DM_CLIENT.CO['NAME'], DM_CLIENT.CO['BILLING_SB_BANK_ACCOUNT_NBR'], 'BILLING']);
    if not VarIsNull(DM_CLIENT.CO['SB_OBC_ACCOUNT_NBR']) then
      if not DS_BANKS.Locate('SB_BANK_ACCOUNT_NBR', DM_CLIENT.CO.fieldbyname('SB_OBC_ACCOUNT_NBR').Value, []) then
        DS_BANKS.AppendRecord([DM_CLIENT.CO['SB_OBC_ACCOUNT_NBR'],
          DM_CLIENT.CO['NAME'], DM_CLIENT.CO['SB_OBC_ACCOUNT_NBR'], 'OBC']);
    if not VarIsNull(DM_CLIENT.CO['TAX_SB_BANK_ACCOUNT_NBR']) then
      if not DS_BANKS.Locate('SB_BANK_ACCOUNT_NBR', DM_CLIENT.CO.fieldbyname('TAX_SB_BANK_ACCOUNT_NBR').Value, []) then
        DS_BANKS.AppendRecord([DM_CLIENT.CO['TAX_SB_BANK_ACCOUNT_NBR'],
          DM_CLIENT.CO['NAME'], DM_CLIENT.CO['TAX_SB_BANK_ACCOUNT_NBR'], 'TAX']);
    if not VarIsNull(DM_CLIENT.CO['TRUST_SB_ACCOUNT_NBR']) then
      if not DS_BANKS.Locate('SB_BANK_ACCOUNT_NBR', DM_CLIENT.CO.fieldbyname('TRUST_SB_ACCOUNT_NBR').Value, []) then
        DS_BANKS.AppendRecord([DM_CLIENT.CO['TRUST_SB_ACCOUNT_NBR'],
          DM_CLIENT.CO['NAME'], DM_CLIENT.CO['TRUST_SB_ACCOUNT_NBR'], 'TRUST']);
    if not VarIsNull(DM_CLIENT.CO['W_COMP_SB_BANK_ACCOUNT_NBR']) then
      if not DS_BANKS.Locate('SB_BANK_ACCOUNT_NBR', DM_CLIENT.CO.fieldbyname('W_COMP_SB_BANK_ACCOUNT_NBR').Value, []) then
        DS_BANKS.AppendRecord([DM_CLIENT.CO['W_COMP_SB_BANK_ACCOUNT_NBR'],
          DM_CLIENT.CO['NAME'], DM_CLIENT.CO['W_COMP_SB_BANK_ACCOUNT_NBR'], 'W_COMP']);
    if not VarIsNull(DM_CLIENT.CO['SB_ACCOUNTANT_NBR']) then
      DS_accountant.AppendRecord([DM_CLIENT.CO['CUSTOMER_SERVICE_SB_USER_NBR'],
        DM_CLIENT.CO['NAME'], DM_CLIENT.CO['SB_ACCOUNTANT_NBR']]);
    if not VarIsNull(DM_CLIENT.CO['SB_REFERRALS_NBR']) then
      DS_accountant.AppendRecord([DM_CLIENT.CO['SB_REFERRALS_NBR'],
        DM_CLIENT.CO['NAME'], DM_CLIENT.CO['SB_REFERRALS_NBR']]);
    if not VarIsNull(DM_CLIENT.CO['CUSTOMER_SERVICE_SB_USER_NBR']) then
      DS_user.AppendRecord([DM_CLIENT.CO['CUSTOMER_SERVICE_SB_USER_NBR'],
        DM_CLIENT.CO['NAME'], DM_CLIENT.CO['CUSTOMER_SERVICE_SB_USER_NBR']]);
    DM_CLIENT.CO.Next;

    DM_CLIENT.CO.Next;
  end;
  DM_CLIENT.CO.first;

  while not DM_CLIENT.CO_SALESPERSON.Eof do
  begin
    if not DS_user.Locate('SB_USER_NBR', DM_CLIENT.CO_SALESPERSON.fieldbyname('SB_USER_NBR').Value, []) then
      DS_user.AppendRecord([DM_CLIENT.CO_SALESPERSON['SB_USER_NBR'], 'Sales Person',
        DM_CLIENT.CO_SALESPERSON['SB_USER_NBR']]);

    DM_CLIENT.CO_SALESPERSON.Next;
  end;

  evDBLookupCombo6.LookupTable := DM_SERVICE_BUREAU.SB_USER;
  evDBLookupCombo3.LookupTable := DM_SERVICE_BUREAU.SB_ACCOUNTANT;

  cdsSbBankAccountsForAch.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForTrust.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForTax.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForBilling.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForOBC.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
  cdsSbBankAccountsForWC.Data := DM_SERVICE_BUREAU.SB_BANK_ACCOUNTS.Data;
end;

procedure TFormMoveClient.Button1Click(Sender: TObject);
var
  tmp: string;
begin
  DM_CLIENT.CL_MAIL_BOX_GROUP.edit;
  DM_CLIENT.CL.Edit;
  if DS_client.FieldByName('old_sb_accountant_nbr').AsString <> DS_client.FieldByName('sb_accountant_nbr').AsString then
    DM_CLIENT.CL.FieldByName('SB_ACCOUNTANT_NUMBER').AsString := DS_client.FieldByName('sb_accountant_nbr').AsString;
  if DS_client.FieldByName('old_cust_service_sb_user_nbr').AsString <> DS_client.FieldByName('cust_service_sb_user_nbr').AsString then
    DM_CLIENT.CL.FieldByName('CUSTOMER_SERVICE_SB_USER_NBR').AsString := DS_client.FieldByName('cust_service_sb_user_nbr').AsString;
  if DS_client.FieldByName('old_CSR_SB_TEAM_NBR').AsString <> DS_client.FieldByName('CSR_SB_TEAM_NBR').AsString then
    DM_CLIENT.CL.FieldByName('CSR_SB_TEAM_NBR').AsString := DS_client.FieldByName('CSR_SB_TEAM_NBR').AsString;
  DM_CLIENT.CL.Post;

  ctx_DataAccess.PostDataSets([DM_client.CL]);

  DM_client.CL_MAIL_BOX_GROUP.First;
  while not DM_client.CL_MAIL_BOX_GROUP.Eof do
  begin
    if DS_media.Locate('old_sb_media_type_nbr', DM_client.CL_MAIL_BOX_GROUP.fieldbyname('prim_sb_media_type_nbr').Value, []) then
      if DS_media.fieldbyname('sb_media_type_nbr').asinteger <> DS_media.fieldbyname('old_sb_media_type_nbr').asinteger then
      begin
        DM_client.CL_MAIL_BOX_GROUP.Edit;
        DM_client.CL_MAIL_BOX_GROUP['prim_sb_media_type_nbr'] := DS_media['sb_media_type_nbr'];
        DM_client.CL_MAIL_BOX_GROUP.Post;
      end;
    if DS_media.Locate('old_sb_media_type_nbr', DM_client.CL_MAIL_BOX_GROUP.fieldbyname('sec_sb_media_type_nbr').Value, []) then
      if DS_media.fieldbyname('sb_media_type_nbr').asinteger <> DS_media.fieldbyname('old_sb_media_type_nbr').asinteger then
      begin
        DM_client.CL_MAIL_BOX_GROUP.Edit;
        DM_client.CL_MAIL_BOX_GROUP['sec_sb_media_type_nbr'] := DS_media['sb_media_type_nbr'];
        DM_client.CL_MAIL_BOX_GROUP.Post;
      end;
    if DS_method.Locate('old_sb_delivery_method_nbr', DM_client.CL_MAIL_BOX_GROUP.fieldbyname('prim_sb_delivery_method_nbr').Value, []) then
      if DS_method.fieldbyname('sb_delivery_method_nbr').asinteger <> DS_method.fieldbyname('old_sb_delivery_method_nbr').asinteger then
      begin
        DM_client.CL_MAIL_BOX_GROUP.Edit;
        DM_client.CL_MAIL_BOX_GROUP['prim_sb_delivery_method_nbr'] := DS_method['sb_delivery_method_nbr'];
        DM_client.CL_MAIL_BOX_GROUP.Post;
      end;
    if DS_method.Locate('old_sb_delivery_method_nbr', DM_client.CL_MAIL_BOX_GROUP.fieldbyname('sec_sb_delivery_method_nbr').Value, []) then
      if DS_method.fieldbyname('sb_delivery_method_nbr').asinteger <> DS_method.fieldbyname('old_sb_delivery_method_nbr').asinteger then
      begin
        DM_client.CL_MAIL_BOX_GROUP.Edit;
        DM_client.CL_MAIL_BOX_GROUP['sec_sb_delivery_method_nbr'] := DS_method['sb_delivery_method_nbr'];
        DM_client.CL_MAIL_BOX_GROUP.Post;
      end;
    DM_client.CL_MAIL_BOX_GROUP.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_client.CL_MAIL_BOX_GROUP]);

  DM_client.CL_BANK_ACCOUNT.First;
  while not DM_client.CL_BANK_ACCOUNT.Eof do
  begin
    if DS_bank.Locate('old_sb_banks_nbr', DM_client.CL_BANK_ACCOUNT.fieldbyname('sb_banks_nbr').Value, []) then
      if DS_bank.fieldbyname('sb_banks_nbr').asinteger <> DS_bank.fieldbyname('old_sb_banks_nbr').asinteger then
      begin
        DM_client.CL_BANK_ACCOUNT.Edit;
        DM_client.CL_BANK_ACCOUNT['sb_banks_nbr'] := DS_bank['sb_banks_nbr'];
        DM_client.CL_BANK_ACCOUNT.Post;
      end;
    DM_client.CL_BANK_ACCOUNT.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_client.CL_BANK_ACCOUNT]);

  DM_client.CL_AGENCY.First;
  while not DM_client.CL_AGENCY.Eof do
  begin
    if DS_agency.Locate('old_sb_agency_nbr', DM_client.CL_AGENCY.fieldbyname('sb_agency_nbr').Value, []) then
      if DS_agency.fieldbyname('sb_agency_nbr').asinteger <> DS_agency.fieldbyname('old_sb_agency_nbr').asinteger then
      begin
        DM_client.CL_AGENCY.Edit;
        DM_client.CL_AGENCY['sb_agency_nbr'] := DS_agency['sb_agency_nbr'];
        DM_client.CL_AGENCY.Post;
      end;
    DM_client.CL_AGENCY.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_client.CL_AGENCY]);

  DM_client.CL_3_PARTY_SICK_PAY_ADMIN.First;
  while not DM_client.CL_3_PARTY_SICK_PAY_ADMIN.Eof do
  begin
    if DS_agency.Locate('old_sb_agency_nbr', DM_client.CL_3_PARTY_SICK_PAY_ADMIN.fieldbyname('sb_agency_nbr').Value, []) then
      if DS_agency.fieldbyname('sb_agency_nbr').asinteger <> DS_agency.fieldbyname('old_sb_agency_nbr').asinteger then
      begin
        DM_client.CL_3_PARTY_SICK_PAY_ADMIN.Edit;
        DM_client.CL_3_PARTY_SICK_PAY_ADMIN['sb_agency_nbr'] := DS_agency['sb_agency_nbr'];
        DM_client.CL_3_PARTY_SICK_PAY_ADMIN.Post;
      end;
    DM_client.CL_3_PARTY_SICK_PAY_ADMIN.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_client.CL_3_PARTY_SICK_PAY_ADMIN]);

  DM_client.CL_DELIVERY_METHOD.first;
  while not DM_client.CL_DELIVERY_METHOD.Eof do
  begin
    if DS_delivery_service.Locate('old_sb_delivery_company_svcs_nbr', DM_client.CL_DELIVERY_METHOD.fieldbyname('sb_delivery_company_svcs_nbr').Value, []) then
      if DS_delivery_service.fieldbyname('old_sb_delivery_company_svcs_nbr').asinteger <> DS_delivery_service.fieldbyname('sb_delivery_company_svcs_nbr').asinteger then
      begin
        DM_client.CL_DELIVERY_METHOD.Edit;
        DM_client.CL_DELIVERY_METHOD['sb_delivery_company_svcs_nbr'] := DS_delivery_service['sb_delivery_company_svcs_nbr'];
        DM_client.CL_DELIVERY_METHOD.Post;
      end;
    DM_client.CL_DELIVERY_METHOD.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_client.CL_DELIVERY_METHOD]);

  DM_client.CO.First;
  while not DM_client.CO.Eof do
  begin
    if DS_user.Locate('old_sb_user_nbr', DM_client.CO.fieldbyname('CUSTOMER_SERVICE_SB_USER_NBR').Value, []) then
      if DS_user.fieldbyname('old_sb_user_nbr').asinteger <> DS_user.fieldbyname('sb_user_nbr').asinteger then
      begin
        DM_client.CO.Edit;
        DM_client.CO['CUSTOMER_SERVICE_SB_USER_NBR'] := DS_user['sb_user_nbr'];
        DM_client.CO.Post;
      end;
    if DS_referrals.Locate('old_sb_referrals_nbr', DM_client.CO.fieldbyname('sb_referrals_nbr').Value, []) then
      if DS_referrals.fieldbyname('old_sb_referrals_nbr').asinteger <> DS_referrals.fieldbyname('sb_referrals_nbr').asinteger then
      begin
        DM_client.CO.Edit;
        DM_client.CO['sb_referrals_nbr'] := DS_referrals['sb_referrals_nbr'];
        DM_client.CO.Post;
      end;
    if DS_accountant.Locate('old_sb_accountant_nbr', DM_client.CO.fieldbyname('sb_accountant_nbr').Value, []) then
      if DS_accountant.fieldbyname('old_sb_accountant_nbr').asinteger <> DS_accountant.fieldbyname('sb_accountant_nbr').asinteger then
      begin
        DM_client.CO.Edit;
        DM_client.CO['sb_accountant_nbr'] := DS_accountant['sb_accountant_nbr'];
        DM_client.CO.Post;
      end;
    if DS_banks.Locate('old_sb_bank_account_nbr', DM_client.CO.fieldbyname('ach_sb_bank_account_nbr').Value, []) then
      if DS_banks.fieldbyname('old_sb_bank_account_nbr').asinteger <> DS_banks.fieldbyname('sb_bank_account_nbr').asinteger then
      begin
        DM_client.CO.Edit;
        DM_client.CO['ach_sb_bank_account_nbr'] := DS_banks['sb_bank_account_nbr'];
        DM_client.CO.Post;
      end;
    if DS_banks.Locate('old_sb_bank_account_nbr', DM_client.CO.fieldbyname('billing_sb_bank_account_nbr').Value, []) then
      if DS_banks.fieldbyname('old_sb_bank_account_nbr').asinteger <> DS_banks.fieldbyname('sb_bank_account_nbr').asinteger then
      begin
        DM_client.CO.Edit;
        DM_client.CO['billing_sb_bank_account_nbr'] := DS_banks['sb_bank_account_nbr'];
        DM_client.CO.Post;
      end;
    if DS_banks.Locate('old_sb_bank_account_nbr', DM_client.CO.fieldbyname('tax_sb_bank_account_nbr').Value, []) then
      if DS_banks.fieldbyname('old_sb_bank_account_nbr').asinteger <> DS_banks.fieldbyname('sb_bank_account_nbr').asinteger then
      begin
        DM_client.CO.Edit;
        DM_client.CO['tax_sb_bank_account_nbr'] := DS_banks['sb_bank_account_nbr'];
        DM_client.CO.Post;
      end;
    if DS_banks.Locate('old_sb_bank_account_nbr', DM_client.CO.fieldbyname('TRUST_SB_ACCOUNT_NBR').Value, []) then
      if DS_banks.fieldbyname('old_sb_bank_account_nbr').asinteger <> DS_banks.fieldbyname('sb_bank_account_nbr').asinteger then
      begin
        DM_client.CO.Edit;
        DM_client.CO['TRUST_SB_ACCOUNT_NBR'] := DS_banks['sb_bank_account_nbr'];
        DM_client.CO.Post;
      end;
    if DS_banks.Locate('old_sb_bank_account_nbr', DM_client.CO.fieldbyname('W_COMP_SB_BANK_ACCOUNT_NBR').Value, []) then
      if DS_banks.fieldbyname('old_sb_bank_account_nbr').asinteger <> DS_banks.fieldbyname('sb_bank_account_nbr').asinteger then
      begin
        DM_client.CO.Edit;
        DM_client.CO['W_COMP_SB_BANK_ACCOUNT_NBR'] := DS_banks['sb_bank_account_nbr'];
        DM_client.CO.Post;
      end;
    if DS_banks.Locate('old_sb_bank_account_nbr', DM_client.CO.fieldbyname('SB_OBC_ACCOUNT_NBR').Value, []) then
      if DS_banks.fieldbyname('old_sb_bank_account_nbr').asinteger <> DS_banks.fieldbyname('sb_bank_account_nbr').asinteger then
      begin
        DM_client.CO.Edit;
        DM_client.CO['SB_OBC_ACCOUNT_NBR'] := DS_banks['sb_bank_account_nbr'];
        DM_client.CO.Post;
      end;
    DM_client.CO.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_client.CO]);

  DM_client.CO_SALESPERSON.First;
  while not DM_client.CO_SALESPERSON.Eof do
  begin
    if DS_user.Locate('old_sb_user_nbr', DM_client.CO_SALESPERSON.fieldbyname('sb_user_nbr').Value, []) then
      if DS_user.fieldbyname('old_sb_user_nbr').asinteger <> DS_user.fieldbyname('sb_user_nbr').asinteger then
      begin
        DM_client.CO_SALESPERSON.Edit;
        DM_client.CO_SALESPERSON['sb_user_nbr'] := DS_user['sb_user_nbr'];
        DM_client.CO_SALESPERSON.Post;
      end;
    DM_client.CO_SALESPERSON.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_client.CO_SALESPERSON]);

  DM_client.CO_SERVICES.First;
  while not DM_client.CO_SERVICES.Eof do
  begin
    if DS_service.Locate('old_sb_services_nbr', DM_client.CO_SERVICES.fieldbyname('sb_services_nbr').Value, []) then
      if DS_service.fieldbyname('old_sb_services_nbr').asinteger <> DS_service.fieldbyname('sb_services_nbr').asinteger then
      begin
        DM_client.CO_SERVICES.Edit;
        DM_client.CO_SERVICES['sb_services_nbr'] := DS_service['sb_services_nbr'];
        DM_client.CO_SERVICES.Post;
      end;
    DM_client.CO_SERVICES.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_client.CO_SERVICES]);

  DM_CLIENT.CO_BANK_ACCOUNT_REGISTER.DataRequired('');
  DM_CLIENT.CO_BANK_ACCOUNT_REGISTER.First;
  while not DM_CLIENT.CO_BANK_ACCOUNT_REGISTER.Eof do
  begin
    if DS_banks.Locate('old_sb_bank_account_nbr', DM_CLIENT.CO_BANK_ACCOUNT_REGISTER.fieldbyname('SB_BANK_ACCOUNTs_NBR').Value, []) then
      if DS_banks.fieldbyname('old_sb_bank_account_nbr').asinteger <> DS_banks.fieldbyname('sb_bank_account_nbr').asinteger then
      begin
        DM_client.CO_BANK_ACCOUNT_REGISTER.Edit;
        DM_client.CO_BANK_ACCOUNT_REGISTER['SB_BANK_ACCOUNTs_NBR'] := DS_banks['sb_bank_account_nbr'];
        DM_client.CO_BANK_ACCOUNT_REGISTER.Post;
      end;
    DM_CLIENT.CO_BANK_ACCOUNT_REGISTER.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_CLIENT.CO_BANK_ACCOUNT_REGISTER]);

  DM_CLIENT.CO_MANUAL_ACH.DataRequired('');
  DM_CLIENT.CO_MANUAL_ACH.First;
  while not DM_CLIENT.CO_MANUAL_ACH.Eof do
  begin
    if DS_banks.Locate('old_sb_bank_account_nbr', DM_CLIENT.CO_MANUAL_ACH.fieldbyname('FROM_SB_BANK_ACCOUNTs_NBR').Value, []) then
      if DS_banks.fieldbyname('old_sb_bank_account_nbr').asinteger <> DS_banks.fieldbyname('sb_bank_account_nbr').asinteger then
      begin
        DM_client.CO_MANUAL_ACH.Edit;
        DM_client.CO_MANUAL_ACH['FROM_SB_BANK_ACCOUNTs_NBR'] := DS_banks['sb_bank_account_nbr'];
        DM_client.CO_MANUAL_ACH.Post;
      end;
    if DS_banks.Locate('old_sb_bank_account_nbr', DM_CLIENT.CO_MANUAL_ACH.fieldbyname('TO_SB_BANK_ACCOUNTs_NBR').Value, []) then
      if DS_banks.fieldbyname('old_sb_bank_account_nbr').asinteger <> DS_banks.fieldbyname('sb_bank_account_nbr').asinteger then
      begin
        ctx_DataAccess.CheckACHstatus := False;
        DM_client.CO_MANUAL_ACH.Edit;
        tmp := DM_client.CO_MANUAL_ACH.FieldByName('STATUS').AsString;
        DM_client.CO_MANUAL_ACH['TO_SB_BANK_ACCOUNTs_NBR'] := DS_banks['sb_bank_account_nbr'];
        DM_client.CO_MANUAL_ACH.Post;
      end;
    DM_CLIENT.CO_MANUAL_ACH.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_CLIENT.CO_MANUAL_ACH]);
  ctx_DataAccess.CheckACHstatus := True;

  DM_CLIENT.CO_BILLING_HISTORY_DETAIL.DataRequired('');
  DM_CLIENT.CO_BILLING_HISTORY_DETAIL.First;
  while not DM_CLIENT.CO_BILLING_HISTORY_DETAIL.Eof do
  begin
    if ds_service.Locate('old_sb_services_nbr', DM_CLIENT.CO_BILLING_HISTORY_DETAIL.fieldbyname('SB_services_NBR').Value, []) then
      if ds_service.fieldbyname('old_sb_services_nbr').asinteger <> ds_service.fieldbyname('sb_services_nbr').asinteger then
      begin
        DM_client.CO_BILLING_HISTORY_DETAIL.Edit;
        DM_client.CO_BILLING_HISTORY_DETAIL['SB_services_NBR'] := ds_service['sb_services_nbr'];
        DM_client.CO_BILLING_HISTORY_DETAIL.Post;
      end;
    DM_CLIENT.CO_BILLING_HISTORY_DETAIL.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_CLIENT.CO_BILLING_HISTORY_DETAIL]);

  DM_CLIENT.PR.DataRequired('');
  DM_CLIENT.PR.First;
  while not DM_CLIENT.PR.Eof do
  begin
    if ds_user.Locate('old_sb_user_nbr', DM_CLIENT.PR.fieldbyname('CR_APPROVE_SB_user_NBR').Value, []) then
      if ds_user.fieldbyname('old_sb_user_nbr').asinteger <> ds_user.fieldbyname('sb_user_nbr').asinteger then
      begin
        DM_client.PR.Edit;
        DM_client.PR['CR_APPROVE_SB_user_NBR'] := ds_user['sb_user_nbr'];
        DM_client.PR.Post;
      end;
    DM_CLIENT.PR.Next;
  end;
  ctx_DataAccess.PostDataSets([DM_CLIENT.PR]);

  if MessageDlg('Do you want to save the mapping to a file?', mtConfirmation, mbYesNoCancel, 0) = mrYes then
    if savedialog1.Execute then
      if savedialog1.FileName <> '' then
      begin
        if pos('.', savedialog1.FileName) <> 0 then
          savedialog1.FileName := copy(savedialog1.FileName, 1, pos('.', savedialog1.FileName) - 1);
        button2Click(nil);
      end;
  ModalResult := mrOK;

end;

procedure TFormMoveClient.FormShow(Sender: TObject);
begin
  if ds_agency.RecordCount = 0 then
    evDBLookupCombo4.Enabled := false;
  if ds_bank.RecordCount = 0 then
    wwDBLookupCombo1.Enabled := false;
  if DS_delivery_service.RecordCount = 0 then
    evDBLookupCombo7.Enabled := false;
  if ds_user.RecordCount = 0 then
    evDBLookupCombo6.Enabled := false;

  if ds_service.RecordCount = 0 then
    evDBLookupCombo1.Enabled := false;
  if ds_accountant.RecordCount = 0 then
    evDBLookupCombo3.Enabled := false;
  if DS_Referrals.RecordCount = 0 then
    wwlcSB_Referrals_Nbr.Enabled := false;
  if ds_banks.RecordCount = 0 then
    evDBLookupCombo5.Enabled := false;
  if ds_method.RecordCount = 0 then
    edType.Enabled := false;
  if ds_media.RecordCount = 0 then
    edMedia2.Enabled := false;

end;

procedure TFormMoveClient.PostChange(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  DS_bank.Edit;
  DS_bank.FieldByName('SB_BANKS_NBR').AsString := wwDBLookupCombo1.LookupValue;
end;

procedure TFormMoveClient.evDBGrid4DrawDataCell(Sender: TObject;
  const Rect: TRect; Field: TField; State: TGridDrawState);
begin
   {If gdFocused in State then
    with (Sender as TevDBGrid).Canvas do begin
      Brush.Color := clRed;
      FillRect(Rect);
      TextOut(Rect.Left, Rect.Top, Field.AsString);
    end;  }

end;
type
  THack = class(TevDBGrid);

procedure TFormMoveClient.evDBLookupCombo5BeforeDropDown(Sender: TObject);
begin
  if DS_Banks.FieldByName('type').AsString = 'OBC' then
    evDBLookupCombo5.LookupTable := cdsSbBankAccountsForOBC;
  if DS_Banks.FieldByName('type').AsString = 'TAX' then
    evDBLookupCombo5.LookupTable := cdsSbBankAccountsForTax;
  if DS_Banks.FieldByName('type').AsString = 'ACH' then
    evDBLookupCombo5.LookupTable := cdsSbBankAccountsForAch;
  if DS_Banks.FieldByName('type').AsString = 'TRUST' then
    evDBLookupCombo5.LookupTable := cdsSbBankAccountsForTrust;
  if DS_Banks.FieldByName('type').AsString = 'BILLING' then
    evDBLookupCombo5.LookupTable := cdsSbBankAccountsForBilling;
  if DS_Banks.FieldByName('type').AsString = 'W_COMP' then
    evDBLookupCombo5.LookupTable := cdsSbBankAccountsForWC;
  evDBLookupCombo5.LookupField := 'SB_BANK_ACCOUNTS_NBR';
end;

procedure TFormMoveClient.evDBLookupCombo5Change(Sender: TObject);
begin
  if evDBLookupCombo5.LookupValue <> '' then
  begin
    DS_Banks.Edit;
    DS_Banks.FieldByName('SB_BANK_ACCOUNT_NBR').AsString := evDBLookupCombo5.LookupValue;
  end;
end;

procedure TFormMoveClient.wwlcSB_Referrals_NbrBeforeDropDown(
  Sender: TObject);
begin
  wwlcSB_Referrals_Nbr.LookupTable := DM_SERVICE_BUREAU.SB_REFERRALS;
end;

procedure TFormMoveClient.wwlcAccountant_NumberCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  ds_client.Edit;
  ds_client.FieldByName('sb_accountant_nbr').AsString := wwlcAccountant_Number.LookupValue;

end;

procedure TFormMoveClient.Button2Click(Sender: TObject);
var
 // t: TStringList;
  i: Integer;
  s, s1, s2, tmp, tmp1: string;
 // sg: TStringStream;
  inilogfile: TmemInifile;
  f: file;
begin
 // t := TStringList.Create;
  if fileexists(savedialog1.FileName + '.ini') then
  begin
    assignfile(f, savedialog1.FileName + '.ini');
    rename(f, savedialog1.FileName + formatdatetime('yymmddhhmm', now) + '.ini');
  end;
  inilogfile := TMemIniFile.Create(savedialog1.FileName + '.ini'); //ExtractFilePath(Application.ExeName) +'movelog.ini');

  for i := 0 to ds_client.FieldCount - 1 do
  begin
    s1 := '';
    s2 := '';
    tmp := (ds_client.FieldDefs[i].Name);
    s1 := ds_client.Fieldbyname(ds_client.FieldDefs[i].Name).asstring;

    if pos('OLD_', uppercase(tmp)) <> 0 then
    begin
      tmp := (ds_client.FieldDefs[i + 1].Name);
      s2 := ds_client.Fieldbyname(ds_client.FieldDefs[i + 1].Name).asstring;
      if (s1 <> '') and (s2 <> '') and (s1 <> s2) then
      begin
        inilogfile.WriteString('DS_CLIENT', tmp, s1 + '||' + s2);
      end;
    end
    else
      continue;
  end;

  for i := 0 to DS_bank.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(DS_bank.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_bank.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_bank.First;
  s := '';
  while not ds_bank.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_bank.fieldbyname(tmp).AsString;
    s2 := ds_bank.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then
      s := s + s1 + '||' + s2 + ',';
    ds_bank.Next;

  end;
  inilogfile.WriteString('DS_BANK', tmp1, s);

  for i := 0 to ds_method.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(ds_method.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_method.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_method.First;
  s := '';
  while not ds_method.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_method.fieldbyname(tmp).AsString;
    s2 := ds_method.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then
      s := s + s1 + '||' + s2 + ',';
    ds_method.Next;

  end;
  inilogfile.WriteString('DS_METHOD', tmp1, s);

  for i := 0 to ds_media.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(ds_media.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_media.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_media.First;
  s := '';
  while not ds_media.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_media.fieldbyname(tmp).AsString;
    s2 := ds_media.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then
      s := s + s1 + '||' + s2 + ',';
    ds_media.Next;

  end;
  inilogfile.WriteString('DS_MEDIA', tmp1, s);

  for i := 0 to ds_agency.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(ds_agency.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_agency.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_agency.First;
  s := '';
  while not ds_agency.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_agency.fieldbyname(tmp).AsString;
    s2 := ds_agency.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then //
      s := s + s1 + '||' + s2 + ',';
    ds_agency.Next;
  end;
  inilogfile.WriteString('DS_AGENCY', tmp1, s);

  for i := 0 to ds_delivery_service.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(ds_delivery_service.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_delivery_service.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_delivery_service.First;
  s := '';
  while not ds_delivery_service.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_delivery_service.fieldbyname(tmp).AsString;
    s2 := ds_delivery_service.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then //
      s := s + s1 + '||' + s2 + ',';
    ds_delivery_service.Next;
  end;
  inilogfile.WriteString('DS_DELIVERY_SERVICE', tmp1, s);

  for i := 0 to ds_user.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(ds_user.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_user.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_user.First;
  s := '';
  while not ds_user.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_user.fieldbyname(tmp).AsString;
    s2 := ds_user.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then //and (s1<>s2)
      s := s + s1 + '||' + s2 + ',';
    ds_user.Next;
  end;
  inilogfile.WriteString('DS_USER', tmp1, s);

  for i := 0 to ds_service.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(ds_service.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_service.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_service.First;
  s := '';
  while not ds_service.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_service.fieldbyname(tmp).AsString;
    s2 := ds_service.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then //and (s1<>s2)
      s := s + s1 + '||' + s2 + ',';
    ds_service.Next;
  end;
  inilogfile.WriteString('DS_SERVICE', tmp1, s);

  for i := 0 to ds_Accountant.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(ds_Accountant.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_Accountant.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_Accountant.First;
  s := '';
  while not ds_Accountant.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_Accountant.fieldbyname(tmp).AsString;
    s2 := ds_Accountant.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then //and (s1<>s2)
      s := s + s1 + '||' + s2 + ',';
    ds_Accountant.Next;
  end;
  inilogfile.WriteString('DS_ACCOUNTANT', tmp1, s);

  for i := 0 to ds_referrals.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(ds_referrals.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_referrals.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_referrals.First;
  s := '';
  while not ds_referrals.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_referrals.fieldbyname(tmp).AsString;
    s2 := ds_referrals.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then //and (s1<>s2)
      s := s + s1 + '||' + s2 + ',';
    ds_referrals.Next;
  end;
  inilogfile.WriteString('DS_REFERRALS', tmp1, s);

  for i := 0 to ds_banks.FieldCount - 1 do
  begin
    if pos('OLD_', uppercase(ds_banks.FieldDefs[i].Name)) <> 0 then
    begin
      tmp := (ds_banks.FieldDefs[i].Name);
      tmp1 := copy(tmp, 5, length(tmp) - 4);
    end
    else
      continue;
  end;
  ds_banks.First;
  s := '';
  while not ds_banks.Eof do
  begin
    s1 := '';
    s2 := '';
    s1 := ds_banks.fieldbyname(tmp).AsString;
    s2 := ds_banks.fieldbyname(tmp1).AsString;
    if (s1 <> '') and (s2 <> '') and (s1 <> s2) then //and (s1<>s2)
      s := s + s1 + '||' + s2 + ',';
    ds_banks.Next;
  end;
  inilogfile.WriteString('DS_BANKS', tmp1, s);

  inilogfile.UpdateFile;
  inilogfile.Free;
  //  t.Free;

end;

procedure TFormMoveClient.wwlcCustomer_Service_RepCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  ds_client.Edit;
  ds_client.FieldByName('Cust_Service_Sb_User_nbr').AsString := wwlcCustomer_Service_Rep.LookupValue;

end;

procedure TFormMoveClient.wwlcCustomer_Service_TeamCloseUp(Sender: TObject;
  LookupTable, FillTable: TDataSet; modified: Boolean);
begin
  ds_client.Edit;
  ds_client.FieldByName('Csr_Sb_Team_nbr').AsString := wwlcCustomer_Service_Team.LookupValue;

end;

procedure TFormMoveClient.edMediaCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
//  ds_client.Edit;
//  ds_client.FieldByName('Prim_Sb_Media_Type_nbr').AsString := edMedia.LookupValue;

end;

procedure TFormMoveClient.edTypeCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
//  ds_client.Edit;
//  ds_client.FieldByName('Prim_Sb_Delivery_Method_nbr').AsString := edType.LookupValue;
end;

procedure TFormMoveClient.evType2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
//  ds_client.Edit;
//  ds_client.FieldByName('Sec_Sb_Delivery_Method_nbr').AsString := evType2.LookupValue;

end;

procedure TFormMoveClient.edMedia2CloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
//  ds_client.Edit;
//  ds_client.FieldByName('Sec_Sb_Media_Type_nbr').AsString := edMedia2.LookupValue;

end;

function findlength(value_ini, value_dataset: string): integer;
var
  i, j: integer;
begin
  result := 0;
  i := pos(value_dataset + '||', value_ini);
  if i <> 0 then
  begin
    j := i + length(value_dataset) + 2;
    while j < length(value_ini) do
    begin
      if value_ini[j] <> ',' then
      begin
        result := result + 1;
        j := j + 1;
      end
      else
        exit;
    end;
  end;
end;

procedure TFormMoveClient.Button3Click(Sender: TObject);
var
  sectionItems, sectionContentKeys: Tstrings;
  inilogfile: TmemInifile;
  i, j: integer;
  value_ini, value_dataset: string;
begin
  if OpenDialog1.Execute then
    if OpenDialog1.FileName <> '' then
    begin
      sectionItems := TstringList.Create;
      sectionContentKeys := TstringList.Create;
      inilogfile := TMemIniFile.Create(OpenDialog1.FileName);
      inilogfile.ReadSections(sectionItems);

      with inilogfile do
        for i := 0 to sectionItems.Count - 1 do
        begin
          if sectionItems[i] = 'DS_CLIENT' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              value_dataset := DS_CLIENT.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
              if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
              begin
                DS_CLIENT.edit;
                DS_CLIENT.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, pos(value_dataset + '||', value_ini) + length(value_dataset) + 2, length(value_ini) - pos('||', value_ini));

              end;
            end;
          end;
          if sectionItems[i] = 'DS_MEDIA' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_media.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_media.Eof do
              begin
                value_dataset := DS_media.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_media.edit;
                  DS_media.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_media.Next;
              end;
            end;
          end;
          if sectionItems[i] = 'DS_METHOD' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_method.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_method.Eof do
              begin
                value_dataset := DS_method.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_method.edit;
                  DS_method.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_method.Next;
              end;
            end;
          end;
          if sectionItems[i] = 'DS_BANK' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_BANK.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_BANK.Eof do
              begin
                value_dataset := DS_BANK.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_BANK.edit;
                  DS_BANK.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_BANK.Next;
              end;
            end;
          end;
          if sectionItems[i] = 'DS_AGENCY' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_AGENCY.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_AGENCY.Eof do
              begin
                value_dataset := DS_AGENCY.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_AGENCY.edit;
                  DS_AGENCY.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_AGENCY.Next;
              end;
            end;
          end;
          if sectionItems[i] = 'DS_USER' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_USER.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_USER.Eof do
              begin
                value_dataset := DS_USER.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_USER.edit;
                  DS_USER.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_USER.Next;
              end;
            end;
          end;
          if sectionItems[i] = 'DS_DELIVERY_SERVICE' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_DELIVERY_SERVICE.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_DELIVERY_SERVICE.Eof do
              begin
                value_dataset := DS_DELIVERY_SERVICE.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_DELIVERY_SERVICE.edit;
                  DS_DELIVERY_SERVICE.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_DELIVERY_SERVICE.Next;
              end;
            end;
          end;
          if sectionItems[i] = 'DS_SERVICE' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_SERVICE.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_SERVICE.Eof do
              begin
                value_dataset := DS_SERVICE.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_SERVICE.edit;
                  DS_SERVICE.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_SERVICE.Next;
              end;
            end;
          end;
          if sectionItems[i] = 'DS_ACCOUNTANT' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_ACCOUNTANT.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_ACCOUNTANT.Eof do
              begin
                value_dataset := DS_ACCOUNTANT.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_ACCOUNTANT.edit;
                  DS_ACCOUNTANT.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_ACCOUNTANT.Next;
              end;
            end;
          end;
          if sectionItems[i] = 'DS_REFERRALS' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_REFERRALS.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_REFERRALS.Eof do
              begin
                value_dataset := DS_REFERRALS.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_REFERRALS.edit;
                  DS_REFERRALS.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_REFERRALS.Next;
              end;
            end;
          end;
          if sectionItems[i] = 'DS_BANKS' then
          begin
            ReadSection(sectionItems[i], sectionContentKeys);
            for j := 0 to sectionContentKeys.Count - 1 do
            begin
              DS_BANKS.First;
              value_ini := Readstring(sectionItems[i], sectionContentKeys[j], '');
              while not DS_BANKS.Eof do
              begin
                value_dataset := DS_BANKS.fieldbyname('Old_' + sectionContentKeys[j]).asstring;
                if (value_dataset <> '') and (pos(value_dataset + '||', value_ini) <> 0) then
                begin
                  DS_BANKS.edit;
                  DS_BANKS.fieldbyname(sectionContentKeys[j]).asstring := copy(value_ini, length(value_dataset + '||') + pos(value_dataset + '||', value_ini), findlength(value_ini, value_dataset));
                end;
                DS_BANKS.Next;
              end;

            end;
            inilogfile.Free;
            sectionItems.Free;
            sectionContentKeys.free;
          end;

        end;
    end;
end;
end.

