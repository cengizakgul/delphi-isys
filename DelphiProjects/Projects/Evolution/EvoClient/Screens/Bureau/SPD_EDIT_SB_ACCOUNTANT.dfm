inherited EDIT_SB_ACCOUNTANT: TEDIT_SB_ACCOUNTANT
  Width = 716
  Height = 509
  object pctlEDIT_SB_ACCOUNTANT: TevPageControl [0]
    Left = 8
    Top = 16
    Width = 609
    Height = 441
    HelpContext = 33501
    ActivePage = tshtAccountant_Detail
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object wwdgEDIT_SB_ACCOUNTANT: TevDBGrid
        Left = 0
        Top = 0
        Width = 601
        Height = 409
        DisableThemesInTitle = False
        Selected.Strings = (
          'NAME'#9'40'#9'Name'
          'CITY'#9'20'#9'City'
          'STATE'#9'3'#9'State')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_ACCOUNTANT\wwdgEDIT_SB_ACCOUNTANT'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tshtAccountant_Detail: TTabSheet
      Caption = 'Details'
      object bevSBaccountant1: TEvBevel
        Left = 0
        Top = 0
        Width = 601
        Height = 413
        Align = alClient
      end
      object grbxPrimary_Contact: TevGroupBox
        Left = 304
        Top = 8
        Width = 273
        Height = 121
        Caption = 'Primary Contact'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object lablName2: TevLabel
          Left = 16
          Top = 24
          Width = 89
          Height = 13
          AutoSize = False
          Caption = '~Name'
          FocusControl = dedtName2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablDescription2: TevLabel
          Left = 144
          Top = 64
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = dedtDescription2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablPhone1: TevLabel
          Left = 16
          Top = 64
          Width = 31
          Height = 13
          Caption = 'Phone'
          FocusControl = wwdePhone1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dedtName2: TevDBEdit
          Left = 16
          Top = 40
          Width = 241
          Height = 21
          DataField = 'CONTACT1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtDescription2: TevDBEdit
          Left = 144
          Top = 80
          Width = 112
          Height = 21
          DataField = 'DESCRIPTION1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePhone1: TevDBEdit
          Left = 16
          Top = 80
          Width = 121
          Height = 21
          DataField = 'PHONE1'
          DataSource = wwdsDetail
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object gbSB_Accountant1: TevGroupBox
        Left = 8
        Top = 8
        Width = 281
        Height = 241
        Caption = 'Accountant'
        TabOrder = 0
        object lablName: TevLabel
          Left = 16
          Top = 24
          Width = 161
          Height = 13
          AutoSize = False
          Caption = '~Accountant Name'
          Color = clBtnFace
          FocusControl = dedtName
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object lablAddress1: TevLabel
          Left = 16
          Top = 104
          Width = 47
          Height = 13
          Caption = 'Address 1'
          FocusControl = dedtAddress1
        end
        object lablAddress2: TevLabel
          Left = 16
          Top = 144
          Width = 47
          Height = 13
          Caption = 'Address 2'
          FocusControl = dedtAddress2
        end
        object lablCity: TevLabel
          Left = 16
          Top = 184
          Width = 17
          Height = 13
          Caption = 'City'
          FocusControl = dedtCity
        end
        object lablState: TevLabel
          Left = 144
          Top = 184
          Width = 25
          Height = 13
          Caption = 'State'
          FocusControl = dedtState
        end
        object lablZip_Code: TevLabel
          Left = 192
          Top = 184
          Width = 43
          Height = 13
          Caption = 'Zip Code'
          FocusControl = wwdeZip_Code
        end
        object lablPrint_Name: TevLabel
          Left = 16
          Top = 64
          Width = 177
          Height = 13
          AutoSize = False
          Caption = '~Name for Reports (Alias)'
          FocusControl = wwdePrint_Name
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dedtName: TevDBEdit
          Left = 16
          Top = 40
          Width = 249
          Height = 21
          DataField = 'NAME'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtAddress1: TevDBEdit
          Left = 16
          Top = 120
          Width = 249
          Height = 21
          DataField = 'ADDRESS1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtAddress2: TevDBEdit
          Left = 16
          Top = 160
          Width = 249
          Height = 21
          DataField = 'ADDRESS2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtCity: TevDBEdit
          Left = 16
          Top = 200
          Width = 121
          Height = 21
          DataField = 'CITY'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeZip_Code: TevDBEdit
          Left = 192
          Top = 200
          Width = 73
          Height = 21
          DataField = 'ZIP_CODE'
          DataSource = wwdsDetail
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtState: TevDBEdit
          Left = 144
          Top = 200
          Width = 41
          Height = 21
          CharCase = ecUpperCase
          DataField = 'STATE'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&,@}'
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePrint_Name: TevDBEdit
          Left = 16
          Top = 80
          Width = 249
          Height = 21
          DataField = 'PRINT_NAME'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object gbSB_Accountant2: TevGroupBox
        Left = 304
        Top = 136
        Width = 273
        Height = 113
        Caption = 'Other Contact'
        TabOrder = 2
        object lablName3: TevLabel
          Left = 16
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Name'
          FocusControl = dedtName3
        end
        object lablPhone2: TevLabel
          Left = 16
          Top = 64
          Width = 31
          Height = 13
          Caption = 'Phone'
          FocusControl = wwdePhone2
        end
        object lablDescription3: TevLabel
          Left = 144
          Top = 64
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = dedtDescription3
        end
        object dedtName3: TevDBEdit
          Left = 16
          Top = 40
          Width = 241
          Height = 21
          DataField = 'CONTACT2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtDescription3: TevDBEdit
          Left = 144
          Top = 80
          Width = 112
          Height = 21
          DataField = 'DESCRIPTION2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePhone2: TevDBEdit
          Left = 16
          Top = 80
          Width = 121
          Height = 21
          DataField = 'PHONE2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object gbSB_Accountant3: TevGroupBox
        Left = 8
        Top = 256
        Width = 569
        Height = 145
        Caption = 'Other Information'
        TabOrder = 3
        object lablDescription: TevLabel
          Left = 144
          Top = 72
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = dedtDescription
        end
        object lablFax: TevLabel
          Left = 16
          Top = 72
          Width = 17
          Height = 13
          Caption = 'Fax'
          FocusControl = wwdeFax
        end
        object lablE_Mail: TevLabel
          Left = 16
          Top = 24
          Width = 28
          Height = 13
          Caption = 'E-mail'
          FocusControl = dedtE_Mail
        end
        object lablSignature: TevLabel
          Left = 296
          Top = 24
          Width = 45
          Height = 13
          Caption = 'Signature'
          FocusControl = dimgSignature
        end
        object dedtDescription: TevDBEdit
          Left = 144
          Top = 88
          Width = 112
          Height = 21
          DataField = 'FAX_DESCRIPTION'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeFax: TevDBEdit
          Left = 16
          Top = 88
          Width = 121
          Height = 21
          DataField = 'FAX'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dedtE_Mail: TevDBEdit
          Left = 16
          Top = 40
          Width = 244
          Height = 21
          DataField = 'E_MAIL_ADDRESS'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object dimgSignature: TevDBImage
          Left = 296
          Top = 40
          Width = 257
          Height = 65
          HelpContext = 21012
          DataField = 'SIGNATURE'
          DataSource = wwdsDetail
          Stretch = True
          TabOrder = 3
        end
        object bbtnLoad_Signature: TevBitBtn
          Left = 296
          Top = 112
          Width = 257
          Height = 25
          Caption = 'Load Signature'
          TabOrder = 4
          OnClick = bbtnLoad_SignatureClick
          Color = clBlack
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D8888888888D
            DDDDDFFFFFFFFFFDDDDD800000000008DDDDF8888888888FDDDD3B0333333330
            8DDD8D8888888888FDDD3BB03333333308DD8DD8888888888FDD3BBB03333333
            308D8DDD8888888888FD3BBBB033333333088DDDD8888888888F3BBBBB000000
            00008DDDDD88888888883B3443BBB4B43DDD8DD88DDDDDDDDDDD3B4BB4333384
            DDDD8D8DD8DDDDD8DDDD3B4BB4DD48D4DDDD8D8DD8DD8DD8DDDDD33434D4DD48
            84D8DDD8D8D8DD8DD8DDD8DD84D4D4D4D4D4DDDDD8D8D8D8D8D8DD4D4DD4D4D4
            D4D4DD8D8DD8D8D8D8D8DDD4DD8DDD48DD48DDD8DDDDDD8DDD8DDD4D8DD48DDD
            8DDDDD8DDDD8DDDDDDDDDD84DDDD4DDDDDDDDDD8DDDD8DDDDDDD}
          NumGlyphs = 2
          Margin = 0
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
    Left = 190
  end
  object odlgGetBitmap: TOpenDialog
    Filter = 'Windows BitMap (*.bmp)|*.bmp'
    Options = [ofReadOnly, ofHideReadOnly]
    Left = 528
    Top = 307
  end
end
