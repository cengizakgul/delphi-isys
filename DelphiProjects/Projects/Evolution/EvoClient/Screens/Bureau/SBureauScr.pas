// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SBureauScr;

interface

uses
  SPackageEntry, ComCtrls,  StdCtrls, Graphics,
  Controls, Buttons, Classes, ExtCtrls, Menus, ImgList, ToolWin, ActnList,
  EvCommonInterfaces, EvMainboard, ISBasicClasses,
  EvUIComponents, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton;

type
  TBureauFrm = class(TFramePackageTmpl, IevBureauScreens)
  protected
    function PackageCaption: string; override;
    function PackageSortPosition: string; override;
    procedure UserPackageStructure; override;
    function  PackageBitmap: TBitmap; override;
  end;

implementation

var
  BureauFrm: TBureauFrm;

{$R *.DFM}

function GetBureauScreens: IevBureauScreens;
begin
  if not Assigned(BureauFrm) then
    BureauFrm := TBureauFrm.Create(nil);
  Result := BureauFrm;
end;

function TBureauFrm.PackageBitmap: TBitmap;
begin
  Result := GetBitmap(0);
end;

function TBureauFrm.PackageCaption: string;
begin
  Result := '&Bureau';
end;

function TBureauFrm.PackageSortPosition: string;
begin
  Result := '010';
end;

procedure TBureauFrm.UserPackageStructure;
begin
  AddStructure('Accountants|100', 'TEDIT_SB_ACCOUNTANT');
  AddStructure('Agency|200', 'TEDIT_SB_AGENCY');
//  AddStructure('Agency reports|250', 'TEDIT_SB_AGENCY_REPORTS');
  AddStructure('Banks|300', 'TEDIT_SB_BANKS');
  AddStructure('Company Default Reports|350', 'TEDIT_SB_REPORTS');
  AddStructure('Delivery Company|400', 'TEDIT_SB_DELIVERY_COMPANY');
  AddStructure('Delivery Company Services|500', 'TEDIT_SB_DELIVERY_COMPANY_SVCS');
  AddStructure('Holidays|500', 'TEDIT_SB_HOLIDAYS');
  AddStructure('Referrals|600', 'TEDIT_SB_REFERRALS');
  AddStructure('ACA ALE Group|625', 'TEDIT_SB_ACA_GROUP');
  AddStructure('Enlist Groups|650', 'TEDIT_SB_ENLIST_GROUPS');
  AddStructure('Utilities|700', 'TEDIT_SB_UTILS');
  AddStructure('Other Services|750', 'TEDIT_SB_OTHER_SERVICE');
  AddStructure('Vendors\Custom Vendors|800', 'TEDIT_SB_CUSTOM_VENDORS');
  AddStructure('Vendors\Vendors|810', 'TEDIT_SB_VENDOR');
  AddStructure('Vendors\Vendor Detail|820', 'TEDIT_SB_VENDOR_DETAIL');

  AddStructure('Analytics\Enabled Dashboards|900', 'TEDIT_SB_ENABLED_DASHBOARDS');


end;

initialization
  Mainboard.ModuleRegister.RegisterModule(@GetBureauScreens, IevBureauScreens, 'Screens Bureau');

finalization
  BureauFrm.Free;

end.
