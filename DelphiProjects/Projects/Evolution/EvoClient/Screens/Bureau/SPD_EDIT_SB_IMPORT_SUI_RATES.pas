// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_IMPORT_SUI_RATES;

{$WARN UNIT_PLATFORM OFF}
{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFieldCodeValues, Mask, wwdbedit, Wwdotdot, Wwdbcomb, 
  StdCtrls, ComCtrls, EvUtils, SDataStructure, Variants, EvTypes,
  ISBasicClasses, Spin, ExtCtrls, evConsts, Grids, Wwdbigrd, Wwdbgrid, DB,
  Wwdatsrc, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents, ISBasicUtils,
  EvDataAccessComponents, SDDClasses, SDataDicttemp, isBaseClasses, EVContext,
  EvCommonInterfaces, EvDataset, Buttons, ImgList, EvStreamUtils, EvUIUtils, EvUIComponents, EvClientDataSet,
  LMDCustomButton, LMDButton, isUILMDButton, isUIEdit, LMDBaseControl,
  LMDBaseGraphicControl, LMDBaseGraphicButton, LMDCustomSpeedButton,
  LMDSpeedButton, isUISpeedButton, Math;

type
  TTEDIT_SB_IMPORT_SUI_RATES = class(TForm)
    evPanel1: TevPanel;
    btnExit: TButton;
    evPanel2: TevPanel;
    Wizard: TNotebook;
    btnBack: TButton;
    btnNext: TButton;
    evLabel1: TevLabel;
    evLabel3: TevLabel;
    evLabel4: TevLabel;
    evLabel5: TevLabel;
    ImportFileEdit: TevEdit;
    ExceptionFileEdit: TevEdit;
    OutputFileEdit: TevEdit;
    spdImportFile: TevSpeedButton;
    spdOutputFile: TevSpeedButton;
    spdExceptionFile: TevSpeedButton;
    evLabel6: TevLabel;
    StatesCombo: TevComboBox;
    evLabel7: TevLabel;
    evGroupBox1: TevGroupBox;
    evLabel8: TevLabel;
    evDateTimePicker1: TevDateTimePicker;
    evGroupBox2: TevGroupBox;
    rbState: TevRadioButton;
    rbSUI: TevRadioButton;
    evGroupBox3: TevGroupBox;
    cbLeading: TevRadioButton;
    cbTrailing: TevRadioButton;
    cbBoth: TevRadioButton;
    evPanel3: TevPanel;
    evPanel4: TevPanel;
    evLabel2: TevLabel;
    evPanel5: TevPanel;
    evPanel6: TevPanel;
    evPanel8: TevPanel;
    evPanel9: TevPanel;
    evPanel10: TevPanel;
    Splitter1: TSplitter;
    evPanel7: TevPanel;
    evPanel11: TevPanel;
    evLabel9: TevLabel;
    MappedSUIGrid: TevDBGrid;
    evLabel10: TevLabel;
    evLabel11: TevLabel;
    evPanel12: TevPanel;
    evPanel13: TevPanel;
    ImportSUIGrid: TevDBGrid;
    EvolutionSUIGrid: TevDBGrid;
    btnAdd: TevBitBtn;
    btnRemove: TevBitBtn;
    btnRemoveAll: TevBitBtn;
    ImportFileDlg: TOpenDialog;
    rb1Q: TevRadioButton;
    rb2Q: TevRadioButton;
    rb3Q: TevRadioButton;
    rb4Q: TevRadioButton;
    ImportSUIDataSource: TDataSource;
    EvolutionSUIDataSource: TDataSource;
    MappedSUIDataSource: TDataSource;
    procedure btnExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure spdOutputFileClick(Sender: TObject);
    procedure spdImportFileClick(Sender: TObject);
    procedure spdExceptionFileClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure btnRemoveAllClick(Sender: TObject);
  private
    { Private declarations }
    UniqueSUINames: IisStringList;
    SUIs: IisListOfValues;
    SyStatesNbr: integer;
    dsImportSUI: IevDataSet;
    dsEvolutionSUI: IevDataSet;
    dsMappedSUI: IevDataSet;

    procedure DoImport(var AErrors: integer; var AImported: integer; var AErrorFileName: String; var ALogFilename: String);
    function  DoCheckParameters: boolean;
    function  DoCheckMapping: boolean;
    function  DoPreloadData: boolean;
    procedure DoFillUpGrids;
    function  PrepareEINForSearch(const AEIN: String): String;
  public
    { Public declarations }
  end;

  procedure ShowImportSUIRatesScreen;

implementation

{$R *.DFM}

uses FileCtrl, EvCSVReader, DateUtils, IsErrorUtils;

type
  ISUIDataReader = interface(IEvExchangeDataReader)
  ['{3860BE06-4F6D-4D08-82EC-FF7658A86350}']
    function ReadAllRowsData(const AStream: IevDualStream): IisListOfValues;
  end;

  TSUICSVReader = class(TevCSVReader, ISUIDataReader)
  private
  protected
  public
    function ReadAllRowsData(const AStream: IevDualStream): IisListOfValues;
  end;

  IRateInfo = interface
  ['{E44DF7FF-9EF2-47C9-84C3-8CC18370FBE9}']
    procedure SetEIN(const Value: String);
    procedure SetRate(const Value: Double);
    procedure SetSUIType(const Value: String);
    procedure SetEINForSearch(const Value: String);
    procedure SetField(const Value: integer);
    procedure SetRow(const Value: integer);
    procedure SetSySUINbr(const Value: integer);

    function GetEIN: String;
    function GetEINForSearch: String;
    function GetRate: Double;
    function GetSUIType: String;
    function GetField: integer;
    function GetRow: integer;
    function GetSySUINbr: integer;
    function GetClNbrs: IisStringList;
    function GetCoStatesNbrs: IisStringList;

    property EIN: String read GetEIN write SetEIN;
    property EINForSearch: String read GetEINForSearch write SetEINForSearch;
    property SUIType: String read GetSUIType write SetSUIType;
    property Rate: Double read GetRate write SetRate;
    property Row: integer read GetRow write SetRow;
    property SySUINbr: integer read GetSySUINbr write SetSySUINbr;
    property Field: integer read GetField write SetField;
    property ClNbrs: IisStringList read GetClNbrs;
    property CoStatesNbrs: IisStringList read GetCoStatesNbrs;
  end;

  TRateInfo = class(TisInterfacedObject, IRateInfo)
  private
    FRate: Double;
    FEIN: String;
    FSUIType: String;
    FEINForSearch: String;
    FRow: integer;
    FField: integer;
    FSySUINbr: integer;
    FCoStatesNbrs: IisStringList;
    FClNbrs: IisStringList;
    procedure SetEIN(const Value: String);
    procedure SetRate(const Value: Double);
    procedure SetSUIType(const Value: String);
    procedure SetEINForSearch(const Value: String);
    procedure SetField(const Value: integer);
    procedure SetRow(const Value: integer);
    procedure SetSySUINbr(const Value: integer);

    function GetEIN: String;
    function GetRate: Double;
    function GetSUIType: String;
    function GetEINForSearch: String;
    function GetField: integer;
    function GetRow: integer;
    function GetSySUINbr: integer;
    function GetClNbrs: IisStringList;
    function GetCoStatesNbrs: IisStringList;
  protected
    procedure  DoOnConstruction; override;
  public
    property EIN: String read GetEIN write SetEIN;
    property EINForSearch: String read GetEINForSearch write SetEINForSearch;
    property SUIType: String read GetSUIType write SetSUIType;
    property Rate: Double read GetRate write SetRate;
    property Row: integer read GetRow write SetRow;
    property Field: integer read GetField write SetField;
    property SySUINbr: integer read GetSySUINbr write SetSySUINbr;
    property ClNbr: IisStringList read GetClNbrs;
    property CoStatesNbr: IisStringList read GetCoStatesNbrs;
  end;

var
  ImportSUIratesForm: TTEDIT_SB_IMPORT_SUI_RATES;

procedure ShowImportSUIRatesScreen;
begin
  ImportSUIratesForm := TTEDIT_SB_IMPORT_SUI_RATES.Create(nil);
  try
    ImportSUIratesForm.StatesCombo.ItemIndex := -1;
    ImportSUIratesForm.ShowModal;
  finally
    FreeAndNil(ImportSUIratesForm);
  end;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.btnExitClick(Sender: TObject);
begin
  Exit;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.FormCreate(Sender: TObject);
var
  Q: IevQuery;
begin
  Wizard.PageIndex := 0;
  btnBack.Enabled := False;
  btnNext.Enabled := True;
  UniqueSUINames := TisStringList.Create;
  SUIs := TisListOfValues.Create;

  Q := TevQuery.Create('GenericSelectCurrentOrdered');
  Q.Macros.AddValue('Columns', 'STATE');
  Q.Macros.AddValue('TableName', 'SY_STATES');
  Q.Macros.AddValue('Order', 'STATE');
  Q.Execute;

  Q.Result.First;
  while not Q.Result.eof do
  begin
    StatesCombo.Items.Add(Q.Result['STATE']);
    Q.Result.Next;
  end;

  StatesCombo.ItemIndex := 0;

  dsImportSUI := TevDataSet.Create;
  dsImportSUI.vclDataSet.FieldDefs.Add('SUI', ftString, 50, true);
  ImportSUIDataSource.DataSet := dsImportSUI.vclDataSet;

  dsEvolutionSUI := TevDataSet.Create;
  dsEvolutionSUI.vclDataSet.FieldDefs.Add('SUI Tax Name', ftString, 50, true);
  EvolutionSUIDataSource.DataSet := dsEvolutionSUI.vclDataSet;

  dsMappedSUI := TevDataSet.Create;
  dsMappedSUI.vclDataSet.FieldDefs.Add('SUI', ftString, 50, true);
  dsMappedSUI.vclDataSet.FieldDefs.Add('SUI Tax Name', ftString, 50, true);
  MappedSUIDataSource.DataSet := dsMappedSUI.vclDataSet;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.btnNextClick(Sender: TObject);
var
  iErrors, iImported: integer;
  sLogFile, sErrorsFile: String;
begin
  if Wizard.PageIndex = 0 then
  begin
    if not DoCheckParameters then
      exit;
    if not DoPreloadData then
      exit;
    DoFillUpGrids;
    Wizard.PageIndex := 1;
    btnBack.Enabled := True;
    btnNext.Enabled := True;
    btnNext.Caption := 'Import';
  end
  else
  begin
    if DoCheckMapping then
      try
        DoImport(iErrors, iImported, sErrorsFile, sLogFile);
        if iErrors = 0 then
          EvMessage('Import finished without errors' + #13#10 +'SUIs updated: ' + IntToStr(iImported) + #13#10
            + 'Log file: ' + sLogFile, mtWarning, [mbOk], mbOk)
        else
          EvMessage('Import finished with errors' + #13#10 +'SUIs updated: ' + IntToStr(iImported) + #13#10 +
            'Errors found: ' + IntTOStr(iErrors) + #13#10 +
            'Log file: ' + sLogFile + #13#10 + 'Errors file: ' + sErrorsFile, mtWarning, [mbOk], mbOk);
        ModalResult := mrOK;
      except
        on E: Exception do
        begin
          ModalResult := mrCancel;
          raise;
        end;
      end;
  end;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.btnBackClick(Sender: TObject);
begin
  if Wizard.PageIndex = 1 then
  begin
    Wizard.PageIndex := 0;
    btnBack.Enabled := False;
    btnNext.Enabled := True;
    btnNext.Caption := 'Next >';
  end;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.DoImport(var AErrors: integer; var AImported: integer; var AErrorFileName: String; var ALogFilename: String);
var
  iMonth: Word;
  ImportDate: TDateTime;
  qSySUI, qTmpCoStates, qCoStates, qCo, Q: IevQuery;
  LogFileName, ExceptionFileName: String;
  S: String;
  iErrorsCount, iSUIImportedCount, iImportedToClient: integer;
  Output, Errors: TextFile;
  i, j, k: integer;
  bFound: boolean;
  RateInfo: IRateInfo;
  UniqueEINs: IisStringList;
  UniqueCLNbrs: IisStringList;
  dsCoSUI: TevClientDataSet;
  Tbl: TClass;
  oldUpdateDate: Integer;
  LV: IisListOfValues;

begin
  S := FormatDateTime('mm-dd-yyyy hh-nn', Now);
  LogFileName := NormalizePath(OutputFileEdit.Text) + StatesCombo.Items[StatesCombo.ItemIndex] +
    '-SUI Update Log ' + S + '.txt';
  ExceptionFileName := NormalizePath(ExceptionFileEdit.Text) + StatesCombo.Items[StatesCombo.ItemIndex] +
    '-SUI Update Errors ' + S + '.txt';

  ALogFilename := LogFileName;
  AErrorFileName := ExceptionFileName;
  AErrors := 0;
  AImported := 0;

  iErrorsCount := 0;
  iSUIImportedCount := 0;

  // calsulating AsOfDate
  if rb1Q.Checked then
    iMonth := 1
  else if rb2Q.Checked then
    iMonth := 4
  else if rb3Q.Checked then
    iMonth := 7
  else
    iMonth := 10;
  ImportDate := EncodeDate(YearOf(evDateTimePicker1.Date), iMonth, 1);


  oldUpdateDate := ctx_DataAccess.AsOfDate;
  ctx_DataAccess.AsOfDate := Floor(ImportDate);
  try

    AssignFile(Errors, ExceptionFileName);
    Rewrite(Errors);
    try
      try
        Writeln(Errors, '          Import SUI Rates' + #13#10 + StringOfChar('-', 40));

        AssignFile(Output, LogFileName);
        Rewrite(Output);
        try
          Writeln(Output, '          Import SUI Rates' + #13#10 + StringOfChar('-', 40));
          Writeln(Output, 'Input File: ' + ImportFileEdit.Text);
          Writeln(Output, 'State: ' + StatesCombo.Items[StatesCombo.ItemIndex]);
          Writeln(Output, 'Effective Date: ' + DateToStr(ImportDate));

          if rbState.Checked then
            Writeln(Output, 'EIN Type: State')
          else
            Writeln(Output, 'EIN Type: SUI');

          if cbLeading.Checked then
            S := 'Leading'
          else if cbTrailing.Checked then
            S := 'Trailing'
          else
            S := 'Both';
          Writeln(Output, 'Ignore zeros in EIN: ' + S + #13#10 + StringOfChar('-', 40));

          qSySUI := TevQuery.Create('GenericSelectCurrentWithCondition');
          qSySUI.Macros.AddValue('Columns', 'SY_SUI_NBR, SUI_TAX_NAME, GLOBAL_RATE');
          qSySUI.Macros.AddValue('TableName', 'SY_SUI');
          qSySUI.Macros.AddValue('CONDITION', 'SY_STATES_NBR=' + IntToStr(SyStatesNbr));
          qSySUI.Execute;

          qTmpCoStates := TevQuery.Create('GenericSelectWithCondition');
          qTmpCoStates.Macros.AddValue('Columns', 'CO_STATES_NBR, CL_NBR, CO_NBR, STATE_EIN, SUI_EIN');
          qTmpCoStates.Macros.AddValue('TableName', 'TMP_CO_STATES');
          qTmpCoStates.Macros.AddValue('CONDITION', 'STATE=''' + StatesCombo.Items[StatesCombo.ItemIndex] + '''');
          qTmpCoStates.Execute;

          // checking that GLOBAL_RATE is NULL
          dsMappedSUI.First;
          while not dsMappedSUI.eof do
          begin
            bFound := qSySUI.Result.Locate('SUI_TAX_NAME', dsMappedSUI['SUI Tax Name'], [loCaseInsensitive]);
            CheckCondition(bFound, 'Cannot find record in SY_SUI by SUI_TAX_NAME. Value: ' + dsMappedSUI['SUI Tax Name']);
            if qSySUI.Result['GLOBAL_RATE'] <> null then
            begin
              S := dsMappedSUI['SUI'];
              // do two passes to save order of rows in error file
              for i := 0 to SUIs.Count - 1 do
              begin
                RateInfo := IInterface(SUIs.Values[i].Value) as IRateInfo;
                if RateInfo.SUIType = S then
                begin
                  Writeln(Errors, 'Row #' + IntToStr(RateInfo.Row + 1) + '. Field #' + IntToStr(RateInfo.Field + 1)  +
                    '. SUI is not imported due existing GLOBAL_RATE. SUI: ''' + dsMappedSUI['SUI'] +
                    '''. Mapped to SUI: ''' + dsMappedSUI['SUI Tax Name'] + '''');
                  Inc(iErrorsCount);
                end;
              end;
              for i := SUIs.Count - 1 downto 0 do
              begin
                RateInfo := IInterface(SUIs.Values[i].Value) as IRateInfo;
                if RateInfo.SUIType = S then
                  SUIs.DeleteValue(i);
              end;
              for i := UniqueSUINames.Count - 1 downto 0 do
              begin
                if UniqueSUINames[i] = S then
                begin
                  UniqueSUINames.Delete(i);
                  break;
                end;
              end;
            end
            else
            begin
              // setting SY_SUI_NBR according to mapping
              for i := 0 to SUIs.Count - 1 do
              begin
                RateInfo := IInterface(SUIs.Values[i].Value) as IRateInfo;
                if RateInfo.SUIType = dsMappedSUI['SUI'] then
                   RateInfo.SySUINbr := qSySUI.Result['SY_SUI_NBR'];
              end;
            end;

            dsMappedSUI.Next;
          end;  // while

          // searching for EINS in TMP_CO_STATES and save them in tmpDS
          UniqueEINs := TisStringList.Create;
          for i := 0 to SUIs.Count - 1 do
          begin
            RateInfo := IInterface(SUIs.Values[i].Value) as IRateInfo;
            if UniqueEINs.IndexOf(RateInfo.EINForSearch) = -1 then
              UniqueEINs.Add(RateInfo.EINForSearch);
          end;

          UniqueCLNbrs := TisStringList.Create;
          for i := UniqueEINs.Count - 1 downto 0 do
          begin
            qTmpCoStates.Result.First;
            bFound := False;
            while not qTmpCoStates.Result.eof do
            begin
              if rbSUI.Checked then
                S := PrepareEINForSearch(ConvertNull(qTmpCoStates.Result['SUI_EIN'], ''))
              else
                S := PrepareEINForSearch(ConvertNull(qTmpCoStates.Result['STATE_EIN'], ''));
              if (S <> '') and (s = UniqueEINs[i]) then
              begin
                bFound := True;
                if UniqueCLNbrs.IndexOf(IntToStr(qTmpCoStates.Result['CL_NBR'])) = -1 then
                  UniqueCLNbrs.Add(IntToStr(qTmpCoStates.Result['CL_NBR']));
                for j := 0 to SUIs.Count - 1 do
                begin
                  RateInfo := IInterface(SUIs.Values[j].Value) as IRateInfo;
                  if RateInfo.EINForSearch = S then
                  begin
                    RateInfo.ClNbrs.Add(IntToStr(qTmpCoStates.Result['CL_NBR']));
                    RateInfo.CoStatesNbrs.Add(IntToStr(qTmpCoStates.Result['CO_STATES_NBR']));
                  end;
                end;
              end;
              qTmpCoStates.Result.Next;
            end;  // while

            if not bFound then
            begin
              // do two passes to save order of rows in error file
              for j := 0 to SUIs.Count - 1 do
              begin
                RateInfo := IInterface(SUIs.Values[j].Value) as IRateInfo;
                if RateInfo.EINForSearch = UniqueEINs[i] then
                begin
                  Writeln(Errors, 'Row #' + IntToStr(RateInfo.Row + 1) + '. Field #' + IntToStr(RateInfo.Field + 1)  +
                    '. SUI is not imported because EIN was not found. EIN: ''' + RateInfo.EIN + '''');
                  Inc(iErrorsCount);
                end;
              end;
              for j := SUIs.Count - 1 downto 0 do
              begin
                RateInfo := IInterface(SUIs.Values[j].Value) as IRateInfo;
                if RateInfo.EINForSearch = UniqueEINs[i] then
                  SUIs.DeleteValue(j);
              end;
              UniqueEINs.Delete(i);
            end;
          end;  // for

          // opening clients and doing actual import
          for i := 0 to UniqueCLNbrs.Count - 1 do
          begin
            try
              ctx_DataAccess.OpenClient(StrToInt(UniqueCLNbrs[i]));
              Q := TevQuery.Create('GenericSelectCurrent');
              Q.Macros.AddValue('Columns', 'CL_NBR');
              Q.Macros.AddValue('TableName', 'CL');
              Q.Execute;

              qCo := TevQuery.Create('GenericSelectCurrent');
              qCo.Macros.AddValue('Columns', '*');
              qCo.Macros.AddValue('TableName', 'CO');
              qCo.Execute;
            except
              on E: Exception do
              begin
                Inc(iErrorsCount);
                Writeln(Errors, 'Unexpected error. Import to client cancelled. CL_NBR=' + IntToStr(ctx_DataAccess.ClientId) + '. Message: ' + E.Message + #13#10#13#10 + GetErrorDetails(E));
              end;
            end;

            qCoStates := TevQuery.Create('GenericSelectWithCondition');
            qCoStates.Macros.AddValue('Columns', 'CO_STATES_NBR, CO_NBR, STATE_EIN, SUI_EIN');
            qCoStates.Macros.AddValue('TableName', 'CO_STATES');
            qCoStates.Macros.AddValue('CONDITION', '{AsOfNow<CO_STATES>} and STATE=''' + StatesCombo.Items[StatesCombo.ItemIndex] + '''');
            qCoStates.Execute;

            Tbl := GetClass('TCO_SUI');
            CheckCondition(Assigned(Tbl), 'Cannot get class: TCO_SUI');
            dsCoSUI := TddTableClass(Tbl).Create(nil);
            try
              dsCoSUI.SkipFieldCheck := True;
              dsCoSUI.ProviderName := 'CO_SUI_PROV';
              dsCoSUI.Active := False;
              dsCoSUI.DataRequired;

              for j := 0 to SUIs.Count - 1 do
              begin
                RateInfo := IInterface(SUIs.Values[j].Value) as IRateInfo;
                if RateInfo.ClNbrs.IndexOf(UniqueCLNbrs[i]) <> -1 then
                begin
                  // doble checking CO_STATES against TMP_CO_STATES
                  for k := RateInfo.ClNbrs.Count - 1 downto 0 do
                  begin
                    if RateInfo.ClNbrs[k] = UniqueCLNbrs[i] then
                    begin
                      bFound := qCoStates.Result.Locate('CO_STATES_NBR', StrToInt(RateInfo.CoStatesNbrs[k]), [loCaseInsensitive]);
                      if not bFound then
                      begin
                        RateInfo.ClNbrs.Delete(k);
                        RateInfo.CoStatesNbrs.Delete(k);
                        Writeln(Errors, 'Row #' + IntToStr(RateInfo.Row + 1) + '. Field #' + IntToStr(RateInfo.Field + 1)  +
                          '. SUI is not imported to client #' + UniqueCLNbrs[i] + ' because CO_STATES_NBR in CO_STATES table does not match TMP_CO_STATES table');
                        Inc(iErrorsCount);
                      end
                      else
                      begin
                        // checking EIN again
                        if rbSUI.Checked then
                          S := PrepareEINForSearch(ConvertNull(qCoStates.Result['SUI_EIN'], ''))
                        else
                          S := PrepareEINForSearch(ConvertNull(qCoStates.Result['STATE_EIN'], ''));
                        if not ((S <> '') and (s = Rateinfo.EINForSearch)) then
                        begin
                          RateInfo.ClNbrs.Delete(k);
                          RateInfo.CoStatesNbrs.Delete(k);
                          Writeln(Errors, 'Row #' + IntToStr(RateInfo.Row + 1) + '. Field #' + IntToStr(RateInfo.Field + 1)  +
                            '. SUI is not imported to client #' + UniqueCLNbrs[i] + ' because SUI in CO_STATES table does not match TMP_CO_STATES table');
                          Inc(iErrorsCount);
                        end;
                      end;
                    end;
                  end;  // for k

                  // actual import
                  ctx_DataAccess.StartNestedTransaction([dbtClient]);
                  try
                    for k := RateInfo.ClNbrs.Count - 1 downto 0 do
                    begin
                      if RateInfo.ClNbrs[k] = UniqueCLNbrs[i] then
                      begin
                        bFound := dsCoSUI.Locate('CO_STATES_NBR; SY_SUI_NBR',  VarArrayOf([RateInfo.CoStatesNbrs[k], RateInfo.SySUINbr]), []);
                        if bFound then
                        begin
                          try
                            LV := TisListOfValues.Create;
                            LV.AddValue('RATE', RateInfo.Rate);

                            ctx_DBAccess.UpdateFieldVersion('CO_SUI', dsCoSUI.FieldByName('CO_SUI_NBR').AsInteger,
                              EmptyDate, EmptyDate, ImportDate, DayBeforeEndOfTime, LV);

                            Inc(iSUIImportedCount);
                            qCo.Result.Locate('CO_NBR', dsCoSUI.FieldByName('CO_NBR').AsVariant, []);

                            Writeln(Output, 'Row #' + IntToStr(RateInfo.Row + 1) + '. Field #' + IntToStr(RateInfo.Field + 1)  +
                              '. SUI is imported to client#' + UniqueCLNbrs[i] + ', CO#' + VarToStr(qCo.Result['CUSTOM_COMPANY_NUMBER']) +
                              ' "' + VarToStr(qCo.Result['Name']) + '", FEIN#' + VarToStr(qCo.Result['FEIN']) +
                              ', SUI EIN#' + VarToStr(qCoStates.Result['SUI_EIN']));
                          except
                            on EE: Exception do
                            begin
                              Writeln(Errors, 'Row #' + IntToStr(RateInfo.Row + 1) + '. Field #' + IntToStr(RateInfo.Field + 1)  +
                                '. SUI is not imported to client #' + UniqueCLNbrs[i] + ' due error. Message: ' + EE.Message);
                            //  dsCoSUI.Cancel;
                            end;
                          end;
                        end
                        else
                        begin
                          Writeln(Errors, 'Row #' + IntToStr(RateInfo.Row + 1) + '. Field #' + IntToStr(RateInfo.Field + 1)  +
                            '. SUI is not imported to client #' + UniqueCLNbrs[i] + ' because EIN/State not found in CO_SUI table');
                          Inc(iErrorsCount);
                        end;
                      end;
                    end; // k
                    ctx_DataAccess.CommitNestedTransaction;
                  except
                    on EE: Exception do
                    begin
                      ctx_DataAccess.RollbackNestedTransaction;
                      Writeln(Errors, 'Cannot save changes for client #' + UniqueCLNbrs[i] + '. Error: ' + EE.Message);
                      Inc(iErrorsCount);
                    end;
                  end;
                end;
              end;  // for j
             finally
              FreeAndNil(dsCoSUI);
            end;
          end;  // for i

          if iErrorsCount = 0 then
            Writeln(Output, StringOfChar('-', 40) + #13#10 + IntTOStr(iErrorsCount) + ' error(s) found.')
          else
            Writeln(Output, StringOfChar('-', 40) + #13#10 + IntTOStr(iErrorsCount) + ' error(s) found.');
          Writeln(Output, IntToStr(iSUIImportedCount) + ' rate(s) updated');
        finally
          CloseFile(Output);
        end;
      except
        on E: Exception do
        begin
          Writeln(Errors, 'Fatal error. Import cancelled. Message: ' + E.Message + #13#10#13#10 + GetErrorDetails(E));
          raise;
        end;
      end;

      Writeln(Errors, StringOfChar('-', 40) + #13#10 + IntTOStr(iErrorsCount) + ' error(s) found.');
    finally
      AErrors := iErrorsCount;
      AImported :=iSUIImportedCount;
      CloseFile(Errors);
    end;
  finally
    ctx_DataAccess.AsOfDate := oldUpdateDate;
  end;
end;

function TTEDIT_SB_IMPORT_SUI_RATES.DoCheckParameters: boolean;
begin
  Result := False;

  if StatesCombo.ItemIndex = -1 then
  begin
    ShowMessage('Please, select state!');
    exit;
  end;

  if (ImportFileEdit.Text = '') or Not FileExists(ImportFileEdit.Text) then
  begin
    ShowMessage('Please, select import file!');
    exit;
  end;

  if (OutputFileEdit.Text = '') or Not DirectoryExists(OutputFileEdit.Text) then
  begin
    ShowMessage('Please, select output file folder!');
    exit;
  end;

  if (ExceptionFileEdit.Text = '') or Not DirectoryExists(ExceptionFileEdit.Text) then
  begin
    ShowMessage('Please, select exception file folder!');
    exit;
  end;

  if not (rb1Q.Checked or rb2Q.Checked or rb3Q.Checked or rb4Q.Checked) then
  begin
    ShowMessage('Please, select quarter!');
    exit;
  end;

  Result := True;
end;

function TTEDIT_SB_IMPORT_SUI_RATES.DoCheckMapping: boolean;
begin
  Result := False;

  if dsImportSUI.RecordCount > 0 then
  begin
    ShowMessage('Please, map all SUI');
    exit;
  end;

  Result := True;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.spdOutputFileClick(Sender: TObject);
var
  Dir: String;
begin
  Dir := OutputFileEdit.Text;
  if SelectDirectory('Please, select the folder where output file will be located', '', Dir) then
    OutputFileEdit.Text := Dir;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.spdImportFileClick(Sender: TObject);
begin
  ImportFileDlg.FileName := ImportFileEdit.Text;
  if ImportFileDlg.Execute then
  begin
    ImportFileEdit.Text := ImportFileDlg.FileName;
    if OutputFileEdit.Text = '' then
      OutputFileEdit.Text := ExtractFilePath(ImportFileEdit.Text);
    if ExceptionFileEdit.Text = '' then
      ExceptionFileEdit.Text := ExtractFilePath(ImportFileEdit.Text);
  end;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.spdExceptionFileClick(Sender: TObject);
var
  Dir: String;
begin
  Dir := ExceptionFileEdit.Text;
  if SelectDirectory('Please, select the folder where exception file will be located', '', Dir) then
    ExceptionFileEdit.Text := Dir;
end;

function TTEDIT_SB_IMPORT_SUI_RATES.DoPreloadData: boolean;
var
  Stream: IevDualStream;
  Reader: ISUIDataReader;
  RowsData: IisListOfValues;
  OneRowData: IisStringList;
  i, j: integer;
  sEIN, sEINForSearch: String;
  Info: IRateInfo;
begin
  Result := False;
  Stream := TevDualStreamHolder.CreateFromFile(ImportFileEdit.Text, False, False, False);
  Reader := TSUICSVReader.Create;
  RowsData := Reader.ReadAllRowsData(Stream);

  // logical check of number of fields
  if RowsData.Count = 0 then
  begin
    ShowMessage('Error in input file. No data found');
    exit;
  end;
  for i := 0 to RowsData.Count - 1 do
  begin
    OneRowData := IInterface(RowsData.Values[i].Value) as IisStringList;
    if OneRowData.Count = 0 then
    begin
      ShowMessage('Error in input file. Row #' + IntToStr(i + 1) + ' has no data');
      exit;
    end;
    if (OneRowData.Count = 1) or (((OneRowData.Count - 1) mod 2) <> 0) then
    begin
      ShowMessage('Error in input file. Row #' + IntToStr(i + 1) + ' has wrong number of fields');
      exit;
    end;
    for j := 0 to OneRowData.Count - 1 do
    begin
      if Trim(OneRowData[j]) = '' then
      begin
        ShowMessage('Error in input file. Row #' + IntToStr(i + 1) + ' Field #' + IntToStr(j+1) + '. Field''s value is empty');
        exit;
      end;
    end; // for j
  end;  // for i

  // loading data to lists
  SUIs.Clear;
  UniqueSUINames.Clear;
  for i := 0 to RowsData.Count - 1 do
  begin
    OneRowData := IInterface(RowsData.Values[i].Value) as IisStringList;
    sEIN := Trim(OneRowData[0]);
    sEINForSearch := PrepareEINForSearch(sEIN);
    j := 1;
    repeat
      Info := TRateInfo.Create;
      Info.EIN := sEIN;
      Info.EINForSearch := sEINForSearch;
      Info.SUIType := Trim(OneRowData[j]);
      Info.Row := i;
      Info.Field := j;
      Inc(j);
      try
        Info.Rate := StrToFloat(Trim(OneRowData[j]));
        Inc(j);
      except
        on E:Exception do
        begin
          ShowMessage('Error in input file. Row #' + IntToStr(i + 1) + ' Field #' + IntToStr(j+1) + '. Cannot get rate. Error: ' + E.Message);
          exit;
        end;
      end;

      if Info.EINForSearch <> '' then
        SUIs.AddValue(IntToStr(SUIs.Count + 1), Info)
      else
      begin
        ShowMessage('Error in input file. Row #' + IntToStr(i + 1) + 'EIN will be empty after removing non-digital characters and leading/trailing zeros. EIN: ' + sEIN);
        exit;
      end;

      if UniqueSUINames.IndexOf(Info.SUIType) = -1 then
        UniqueSUINames.Add(Info.SUIType);
    until j > OneRowData.Count - 1;
  end;

  Result := True;
end;

{ SUICSVReader }

function TSUICSVReader.ReadAllRowsData(const AStream: IevDualStream): IisListOfValues;
begin
  Result := GetAllRowsData(AStream, false);
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.DoFillUpGrids;
var
  i: integer;
  Q: IevQuery;
begin
  dsImportSUI.vclDataSet.Close;
  dsImportSUI.vclDataSet.DisableControls;
  try
    dsImportSUI.vclDataSet.Open;
    for i := 0 to UniqueSUINames.Count - 1 do
      try
        dsImportSUI.Append;
        dsImportSUI['SUI'] := UniqueSUINames[i];
        dsImportSUI.Post;
      except
        on E: Exception do
        begin
          dsImportSUI.vclDataset.Cancel;
          raise;
        end;
      end;
    dsImportSUI.First;
  finally
    dsImportSUI.vclDataSet.EnableControls;
  end;

  // getting SY_STATES_NBR by state's abbr
  Q := TevQuery.Create('GenericSelectCurrentWithCondition');
  Q.Macros.AddValue('Columns', 'SY_STATES_NBR');
  Q.Macros.AddValue('TableName', 'SY_STATES');
  Q.Macros.AddValue('CONDITION', 'STATE=''' + StatesCombo.Items[StatesCombo.ItemIndex] + '''');
  Q.Execute;
  CheckCondition(Q.Result.RecordCount > 0, 'State not found: ' + StatesCombo.Items[StatesCombo.ItemIndex]);
  SyStatesNbr := Q.Result['SY_STATES_NBR'];

  dsEvolutionSUI.vclDataSet.Close;
  dsEvolutionSUI.vclDataSet.DisableControls;
  try
    Q := TevQuery.Create('GenericSelectCurrentOrdered');
    Q.Macros.AddValue('Columns', 'SUI_TAX_NAME, SY_STATES_NBR');
    Q.Macros.AddValue('TableName', 'SY_SUI');
    Q.Macros.AddValue('Order', 'SUI_TAX_NAME');
    Q.Execute;

    Q.Result.First;
    dsEvolutionSUI.vclDataSet.Open;
    while not Q.Result.eof do
    begin
      if Q.Result['SY_STATES_NBR'] = SyStatesNbr then
      begin
        dsEvolutionSUI.Append;
        dsEvolutionSUI['SUI Tax Name'] := Q.Result['SUI_TAX_NAME'];
        dsEvolutionSUI.Post;
      end;
      Q.Result.Next;
    end;
    dsEvolutionSUI.First;
  finally
    dsEvolutionSUI.vclDataSet.EnableControls;
  end;

  dsMappedSUI.vclDataSet.Close;
  dsMappedSUI.vclDataSet.Open;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.FormDestroy(Sender: TObject);
begin
  dsImportSUI.vclDataset.Close;
  ImportSUIDataSource.DataSet := nil;

  dsEvolutionSUI.vclDataSet.Close;
  EvolutionSUIDataSource.DataSet := nil;

  dsMappedSUI.vclDataSet.Close;
  MappedSUIDataSource.DataSet := nil;
end;

{ TRateInfo }

procedure TRateInfo.DoOnConstruction;
begin
  inherited;
  FSySUINbr := -1;
  FCoStatesNbrs := TisStringList.Create;
  FClNbrs := TisStringList.Create;
end;

function TRateInfo.GetClNbrs: IisStringList;
begin
  Result := FClNbrs;
end;

function TRateInfo.GetCoStatesNbrs: IisStringList;
begin
  Result := FCoStatesNbrs;
end;

function TRateInfo.GetEIN: String;
begin
  Result := FEIN;
end;

function TRateInfo.GetEINForSearch: String;
begin
  Result := FEINForSearch;
end;

function TRateInfo.GetField: integer;
begin
  Result := FRow;
end;

function TRateInfo.GetRate: Double;
begin
  Result := FRate;
end;

function TRateInfo.GetRow: integer;
begin
  Result := FRow;
end;

function TRateInfo.GetSUIType: String;
begin
  Result := FSUIType;
end;

function TRateInfo.GetSySUINbr: integer;
begin
  Result := FSySUINbr;
end;

procedure TRateInfo.SetEIN(const Value: String);
begin
  FEIN := Value;
end;

procedure TRateInfo.SetEINForSearch(const Value: String);
begin
  FEINForSearch := Value;
end;

procedure TRateInfo.SetField(const Value: integer);
begin
  FField := Value;
end;

procedure TRateInfo.SetRate(const Value: Double);
begin
  FRate := Value;
end;

procedure TRateInfo.SetRow(const Value: integer);
begin
  FRow := Value;
end;

procedure TRateInfo.SetSUIType(const Value: String);
begin
  FSUIType := Value;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.btnAddClick(Sender: TObject);
begin
  if dsImportSUI.RecordCount > 0 then
  begin
    dsMappedSUI.Append;
    dsMappedSUI['SUI'] := dsImportSUI['SUI'];
    dsMappedSUI['SUI Tax Name'] := dsEvolutionSUI['SUI Tax Name'];
    dsMappedSUI.Post;
    dsImportSUI.Delete;
  end;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.btnRemoveClick(Sender: TObject);
begin
  if dsMappedSUI.RecordCount > 0 then
  begin
    dsImportSUI.Append;
    dsImportSUI['SUI'] := dsMappedSUI['SUI'];
    dsImportSUI.Post;
    dsMappedSUI.Delete;
  end;
end;

procedure TTEDIT_SB_IMPORT_SUI_RATES.btnRemoveAllClick(Sender: TObject);
begin
  if dsMappedSUI.RecordCount > 0 then
  begin
    dsMappedSUI.First;
    while not dsMappedSUI.eof do
    begin
      dsImportSUI.Append;
      dsImportSUI['SUI'] := dsMappedSUI['SUI'];
      dsImportSUI.Post;
      dsMappedSUI.Delete;
    end;
  end;
end;

function TTEDIT_SB_IMPORT_SUI_RATES.PrepareEINForSearch(const AEIN: String): String;
var
 i, j: integer;
 S: String;
 bLeadingNumberGet: boolean;
begin
  S := Trim(AEIN);
  Result := '';

  bLeadingNumberGet := false;
  for i := 1 to Length(S) do
    if Pos(S[i], '0123456789') > 0 then
    begin
      if cbLeading.Checked or cbBoth.Checked then
      begin
        if S[i] <> '0' then
        begin
          bLeadingNumberGet := True;
          Result := Result + S[i];
        end
        else
        begin
          if bLeadingNumberGet then
            Result := Result + S[i];
        end;
      end
      else
        Result := Result + S[i];
    end;

  if cbTrailing.Checked or cbBoth.Checked then
  begin
    j := -1;
    for i := Length(Result) downto 1 do
      if Result[i] <> '0' then
      begin
        j := i;
        break;
      end;

    if j > 0 then
      Result := Copy(Result, 1, j);
  end;
end;

procedure TRateInfo.SetSySUINbr(const Value: integer);
begin
  FSySUINbr := Value;
end;

end.
