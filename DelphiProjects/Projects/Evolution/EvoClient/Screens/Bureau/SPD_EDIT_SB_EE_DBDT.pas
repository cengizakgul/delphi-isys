// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_EE_DBDT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SFieldCodeValues, Mask, wwdbedit, Wwdotdot, Wwdbcomb, 
  wwdbdatetimepicker, StdCtrls, ComCtrls, EvUtils, SDataStructure, Variants,
  ISBasicClasses, Spin, ExtCtrls, evConsts, Grids, Wwdbigrd, Wwdbgrid, DB,
  Wwdatsrc, kbmMemTable, ISKbmMemDataSet, ISDataAccessComponents,
  EvDataAccessComponents, EvContext, EvUIUtils, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_EE_DBDT = class(TForm)
    wwcsEEDBDT: TevClientDataSet;
    wwdsEEDBDT: TevDataSource;
    evPanel1: TevPanel;
    evcbMoveCheck: TevDBComboBox;
    Label3: TLabel;
    evgrDBDT: TevDBGrid;
    evrgCreditHold: TevRadioGroup;
    btnExit: TButton;
    btnApply: TButton;
    evTermDate: TevDateTimePicker;
    Label1: TLabel;
    procedure evcbMoveCheckChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    Procedure AskEEDBDT;
    procedure btnApplyClick(Sender: TObject);
    function  CreateInStatementDBDT(Grid: TevDBGrid) : String;
    procedure btnExitClick(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
    State, BDate, EDate, FreqType, ACHKey: String;

  end;

var
  EDIT_SB_EE_DBDT: TEDIT_SB_EE_DBDT;

implementation

{$R *.DFM}

Procedure TEDIT_SB_EE_DBDT.AskEEDBDT;
var
  eedbdtQuery : string;
begin
    case evcbMoveCheck.ItemIndex of
        0:eedbdtQuery := 'SelectDIVWithName';
        1:eedbdtQuery := 'SelectDBWithName';
        2:eedbdtQuery := 'SelectDBDWithName';
        3:eedbdtQuery := 'SelectDBDTWithName';
    end;

  with TExecDSWrapper.Create(eedbdtQuery) do
  begin
    SetParam('CoNbr', DM_COMPANY.CO['CO_NBR']);
    ctx_DataAccess.GetCustomData(wwcsEEDBDT, AsVariant);
  end;

  wwcsEEDBDT.DisableControls;
  evgrDBDT.RestoreDesignSelected;
  evgrDBDT.RefreshDisplay;
  wwcsEEDBDT.EnableControls;

end;

procedure TEDIT_SB_EE_DBDT.evcbMoveCheckChange(Sender: TObject);
begin
  AskEEDBDT;
end;

procedure TEDIT_SB_EE_DBDT.FormShow(Sender: TObject);
begin
  AskEEDBDT;
  evTermDate.Date := Now();
end;

function TEDIT_SB_EE_DBDT.CreateInStatementDBDT(Grid: TevDBGrid) : String;
var
  i: Integer;
  Nbr: TBookmarkStr;
  fDiv   : TField;
  fBranch: TField;
  fDep   : TField;
  fTeam  : TField;
begin
  Grid.DataSource.DataSet.DisableControls;
  Nbr := Grid.DataSource.DataSet.Bookmark;

  try
    fBranch:= nil;
    fDep   := nil;
    fTeam  := nil;

    fDiv     := Grid.DataSource.DataSet.FieldByName('CO_DIVISION_NBR');
    if evcbMoveCheck.ItemIndex > 0 then
      fBranch  := Grid.DataSource.DataSet.FieldByName('CO_BRANCH_NBR');
    if evcbMoveCheck.ItemIndex > 1 then
      fDep     := Grid.DataSource.DataSet.FieldByName('CO_DEPARTMENT_NBR');
    if evcbMoveCheck.ItemIndex > 2 then
      fTeam    := Grid.DataSource.DataSet.FieldByName('CO_TEAM_NBR');
    Result := '';
    for i := 1 to Grid.SelectedList.Count do
    begin
      Grid.DataSource.DataSet.GotoBookmark(Grid.SelectedList[Pred(i)]);
      if Length(trim(Result))> 0 then
      begin
        Result := Result + ' or ';
      end;
      case evcbMoveCheck.ItemIndex of
         0:Result := Result + '(CO_DIVISION_NBR=' + fDiv.AsString + ' )';
         1:Result := Result + '(CO_DIVISION_NBR=' + fDiv.AsString + ' and CO_BRANCH_NBR='+fBranch.AsString +')';
         2:Result := Result + '(CO_DIVISION_NBR=' + fDiv.AsString + ' and CO_BRANCH_NBR='+fBranch.AsString +' and CO_DEPARTMENT_NBR='+fDep.AsString+')';
         3:Result := Result + '(CO_DIVISION_NBR=' + fDiv.AsString + ' and CO_BRANCH_NBR='+fBranch.AsString +' and CO_DEPARTMENT_NBR='+fDep.AsString+' and CO_TEAM_NBR='+fTeam.AsString+')';
      end;
    end;
  finally
    Grid.DataSource.DataSet.Bookmark := Nbr;
    Grid.DataSource.DataSet.EnableControls;
  end;
end;


procedure TEDIT_SB_EE_DBDT.btnApplyClick(Sender: TObject);
var
 ConditionList : String;
begin
    ConditionList:= CreateInStatementDBDT(evgrDBDT);
    if (Length(ConditionList) > 0) and (evrgCreditHold.ItemIndex >=0) then
    Begin
      if evrgCreditHold.ItemIndex = 0 then
        ConditionList := ConditionList + ' and CURRENT_TERMINATION_CODE = ''M'' '
      else
        ConditionList := ConditionList + ' and CURRENT_TERMINATION_CODE = ''A'' ';

        DM_COMPANY.EE.DataRequired(ConditionList);
        DM_COMPANY.EE.First;
        while not DM_COMPANY.EE.Eof do
        begin

            DM_COMPANY.EE.Edit;
            if evrgCreditHold.ItemIndex = 0 then
             Begin
              DM_COMPANY.EE.FieldValues['CURRENT_TERMINATION_CODE'] := EE_TERM_ACTIVE;
              DM_COMPANY.EE.FieldValues['CURRENT_TERMINATION_DATE'] := null;
             end
            else
             Begin
              DM_COMPANY.EE.FieldValues['CURRENT_TERMINATION_CODE'] := EE_TERM_TERMINATED;
              DM_COMPANY.EE.FieldValues['CURRENT_TERMINATION_DATE'] := evTermDate.Date;
             end;
            DM_COMPANY.EE.Post;

            DM_COMPANY.EE.Next;

        end;
        ctx_DataAccess.PostDataSets([DM_COMPANY.EE]);

        EvMessage('Processed Finished Successfully!', mtWarning, [mbOK]);
    end
    else
      EvMessage('Please select some DBDT Level!', mtWarning, [mbOK]);
end;

procedure TEDIT_SB_EE_DBDT.btnExitClick(Sender: TObject);
begin
 Exit;
end;

end.
