// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_OTHER_SERVICE;

interface

uses SFrameEntry,  StdCtrls, Mask, wwdbedit, Controls, Grids,
  Wwdbigrd, Wwdbgrid, Classes, Db, Wwdatsrc, ISBasicClasses, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_OTHER_SERVICE = class(TFrameEntry)
    wwDBGrid1: TevDBGrid;
    dedtOtherService: TevDBEdit;
    lablName: TevLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
  end;

implementation

uses SDataStructure;

{$R *.DFM}

function TEDIT_SB_OTHER_SERVICE.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_OTHER_SERVICE;
end;

function TEDIT_SB_OTHER_SERVICE.GetInsertControl: TWinControl;
begin
  Result := dedtOtherService;
end;

initialization
  RegisterClass(TEDIT_SB_OTHER_SERVICE);

end.
