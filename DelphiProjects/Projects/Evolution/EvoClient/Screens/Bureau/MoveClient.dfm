object FormMoveClient: TFormMoveClient
  Left = 480
  Top = 84
  Width = 858
  Height = 693
  BorderIcons = [biSystemMenu]
  Caption = 'FormMoveClient'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel9: TevLabel
    Left = 696
    Top = 8
    Width = 85
    Height = 13
    Caption = 'S/B Cover Report'
    Visible = False
  end
  object lablCover_Sheet_Report: TevLabel
    Left = 699
    Top = 64
    Width = 94
    Height = 13
    Caption = 'Cover Sheet Report'
  end
  object evPageControl1: TevPageControl
    Left = 8
    Top = 8
    Width = 841
    Height = 649
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Client'
      object evGroupBox3: TevGroupBox
        Left = 0
        Top = 4
        Width = 825
        Height = 133
        Caption = 'Client '
        TabOrder = 0
        object lablCustomer_Service_Team: TevLabel
          Left = 511
          Top = 13
          Width = 113
          Height = 13
          Caption = 'Customer Service Team'
        end
        object lablCustomer_Service_Rep: TevLabel
          Left = 255
          Top = 11
          Width = 106
          Height = 13
          Caption = 'Customer Service Rep'
          FocusControl = wwlcCustomer_Service_Rep
        end
        object Label1x: TevLabel
          Left = 7
          Top = 12
          Width = 84
          Height = 13
          Caption = 'Client Accountant'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object wwlcAccountant_Number: TevDBLookupCombo
          Left = 7
          Top = 28
          Width = 210
          Height = 21
          HelpContext = 5003
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME')
          DataField = 'SB_ACCOUNTANT_NBR'
          DataSource = evDataSource12
          LookupField = 'SB_ACCOUNTANT_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnCloseUp = wwlcAccountant_NumberCloseUp
        end
        object wwlcCustomer_Service_Team: TevDBLookupCombo
          Left = 511
          Top = 30
          Width = 209
          Height = 21
          HelpContext = 5014
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TEAM_DESCRIPTION'#9'40'#9'TEAM_DESCRIPTION')
          DataField = 'CSR_SB_TEAM_NBR'
          DataSource = evDataSource12
          LookupField = 'SB_TEAM_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
          OnCloseUp = wwlcCustomer_Service_TeamCloseUp
        end
        object wwlcCustomer_Service_Rep: TevDBLookupCombo
          Left = 258
          Top = 28
          Width = 209
          Height = 21
          HelpContext = 5015
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'FIRST_NAME'#9'20'#9'FIRST_NAME'
            'LAST_NAME'#9'30'#9'LAST_NAME'
            'MIDDLE_INITIAL'#9'1'#9'MIDDLE_INITIAL')
          DataField = 'CUST_SERVICE_SB_USER_NBR'
          DataSource = evDataSource12
          LookupField = 'SB_USER_NBR'
          Style = csDropDownList
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
          OnCloseUp = wwlcCustomer_Service_RepCloseUp
        end
      end
      object evGroupBox7: TevGroupBox
        Left = 501
        Top = 140
        Width = 324
        Height = 169
        Caption = 'Client Agency/Third Party Sick Admin'
        TabOrder = 1
        object evLabel10: TevLabel
          Left = 9
          Top = 20
          Width = 84
          Height = 13
          Caption = 'SB Agency Name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBGrid5: TevDBGrid
          Left = 10
          Top = 42
          Width = 303
          Height = 116
          DisableThemesInTitle = False
          Selected.Strings = (
            'OLD_SB_AGENCY_NBR'#9'20'#9'Old Sb Agency Nbr'#9'F'
            'SB_AGENCY_NBR'#9'20'#9'Sb Agency Nbr'#9'T')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid5'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource14
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object evDBLookupCombo4: TevDBLookupCombo
          Left = 98
          Top = 14
          Width = 215
          Height = 21
          HelpContext = 12014
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'Name'#9'F'
            'ADDRESS1'#9'30'#9'Address1'#9'F'
            'CITY'#9'20'#9'City'#9'F'
            'STATE'#9'2'#9'State'#9'F'
            'ZIP_CODE'#9'10'#9'Zip Code'#9'F')
          DataField = 'SB_AGENCY_NBR'
          DataSource = evDataSource14
          LookupField = 'SB_AGENCY_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnCloseUp = PostChange
        end
      end
      object evGroupBox5: TevGroupBox
        Left = 5
        Top = 464
        Width = 628
        Height = 153
        Caption = 'Client Delivery Method'
        TabOrder = 2
        object lablDelivery_Company: TevLabel
          Left = 10
          Top = 16
          Width = 94
          Height = 13
          Caption = 'SB Delivery Service'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBGrid1: TevDBGrid
          Left = 10
          Top = 41
          Width = 591
          Height = 104
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'40'#9'Name'#9#9
            'SB_DELIVERY_COMPANY_SVCS_NBR'#9'10'#9'Sb Delivery Company Svcs Nbr'#9#9
            
              'OLD_SB_DELIVERY_COMPANY_SVCS_NBR'#9'10'#9'Old Sb Delivery Company Svcs' +
              ' Nbr'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid1'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource6
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object evDBLookupCombo7: TevDBLookupCombo
          Left = 106
          Top = 14
          Width = 265
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NameAndService'#9'40'#9'NameAndService'#9'F')
          DataField = 'SB_DELIVERY_COMPANY_SVCS_NBR'
          DataSource = evDataSource6
          LookupField = 'SB_DELIVERY_COMPANY_SVCS_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnCloseUp = PostChange
        end
      end
      object evGroupBox6: TevGroupBox
        Left = 5
        Top = 140
        Width = 484
        Height = 170
        Caption = 'Client Bank Account'
        TabOrder = 3
        object evLabel8: TevLabel
          Left = 9
          Top = 14
          Width = 85
          Height = 13
          Caption = 'SB Bank Account'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object wwDBLookupCombo1: TevDBLookupCombo
          Left = 96
          Top = 14
          Width = 273
          Height = 21
          HelpContext = 7010
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'Name'
            'ABA_NUMBER'#9'9'#9'ABA Number'
            'CITY'#9'20'#9'City'
            'STATE'#9'2'#9'State')
          DataField = 'SB_BANKS_NBR'
          DataSource = evDataSource13
          LookupField = 'SB_BANKS_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
        object evDBGrid3: TevDBGrid
          Left = 7
          Top = 42
          Width = 466
          Height = 118
          DisableThemesInTitle = False
          Selected.Strings = (
            'BANK ACCOUNT NBR'#9'40'#9'Bank account nbr'#9'F'
            'OLD_SB_BANKS_NBR'#9'20'#9'Old Sb Banks Nbr'#9'F'
            'SB_BANKS_NBR'#9'20'#9'Sb Banks Nbr'#9'T')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid3'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource13
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
      end
      object evGroupBox1: TevGroupBox
        Left = 392
        Top = 314
        Width = 433
        Height = 141
        Caption = 'Primary/Secondary Delivery Method'
        TabOrder = 4
        object evLabel1: TevLabel
          Left = 13
          Top = 13
          Width = 77
          Height = 13
          Caption = 'Delivery Method'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edType: TevDBLookupCombo
          Left = 94
          Top = 13
          Width = 251
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME'#9'F')
          DataField = 'SB_DELIVERY_METHOD_NBR'
          DataSource = evDataSource16
          LookupField = 'SB_DELIVERY_METHOD_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnCloseUp = edTypeCloseUp
        end
        object evDBGrid4: TevDBGrid
          Left = 10
          Top = 42
          Width = 383
          Height = 87
          DisableThemesInTitle = False
          Selected.Strings = (
            'DESCRIPTION'#9'40'#9'Description'#9#9
            'OLD_SB_DELIVERY_METHOD_NBR'#9'10'#9'Old Sb Delivery Method Nbr'#9#9
            'SB_DELIVERY_METHOD_NBR'#9'10'#9'Sb Delivery Method Nbr'#9#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid4'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource16
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
      end
      object evGroupBox2: TevGroupBox
        Left = 8
        Top = 314
        Width = 377
        Height = 141
        Caption = 'Primary/Secondary Delivery Media'
        TabOrder = 5
        object evLabel4: TevLabel
          Left = 8
          Top = 14
          Width = 70
          Height = 13
          Caption = 'Delivery Media'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edMedia2: TevDBLookupCombo
          Left = 95
          Top = 13
          Width = 250
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'luMediaName'#9'40'#9'luMediaName'#9'F')
          DataField = 'SB_MEDIA_TYPE_NBR'
          DataSource = evDataSource15
          LookupField = 'SB_MEDIA_TYPE_NBR'
          Style = csDropDownList
          TabOrder = 0
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnCloseUp = edMedia2CloseUp
        end
        object evDBGrid2: TevDBGrid
          Left = 10
          Top = 42
          Width = 359
          Height = 87
          DisableThemesInTitle = False
          Selected.Strings = (
            'DESCRIPTION'#9'40'#9'Description'#9#9
            'OLD_SB_MEDIA_TYPE_NBR'#9'10'#9'Old Sb Media Type Nbr'#9#9
            'SB_MEDIA_TYPE_NBR'#9'10'#9'Sb Media Type Nbr'#9#9)
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid2'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource15
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
      end
      object evDBGrid7: TevDBGrid
        Left = 8
        Top = 64
        Width = 745
        Height = 57
        DisableThemesInTitle = False
        Selected.Strings = (
          'OLD_SB_ACCOUNTANT_NBR'#9'10'#9'Old Sb Accountant Nbr'#9#9
          'SB_ACCOUNTANT_NBR'#9'10'#9'Sb Accountant Nbr'#9#9
          'OLD_CUST_SERVICE_SB_USER_NBR'#9'10'#9'Old Cust Service Sb User Nbr'#9#9
          'CUST_SERVICE_SB_USER_NBR'#9'10'#9'Cust Service Sb User Nbr'#9#9
          'OLD_CSR_SB_TEAM_NBR'#9'10'#9'Old Csr Sb Team Nbr'#9#9
          'CSR_SB_TEAM_NBR'#9'10'#9'Csr Sb Team Nbr'#9#9)
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TFormMoveClient\evDBGrid7'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = evDataSource12
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 6
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
      end
      object Button3: TButton
        Left = 688
        Top = 555
        Width = 97
        Height = 25
        Caption = 'Load mapping file'
        TabOrder = 7
        OnClick = Button3Click
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Company'
      ImageIndex = 1
      object evGroupBox8: TevGroupBox
        Left = 0
        Top = 222
        Width = 425
        Height = 163
        Caption = 'Company Accountant'
        TabOrder = 0
        object evLabel11: TevLabel
          Left = 11
          Top = 28
          Width = 72
          Height = 13
          Caption = 'SB Accountant'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBGrid6: TevDBGrid
          Left = 8
          Top = 48
          Width = 405
          Height = 105
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'40'#9'COMPANY NAME'#9'F'
            'SB_ACCOUNTANT_NBR'#9'15'#9'SB ACCOUNTANT NBR'#9'F'
            'OLD_SB_ACCOUNTANT_NBR'#9'10'#9'OLD NBR'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid6'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource8
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object evDBLookupCombo3: TevDBLookupCombo
          Left = 96
          Top = 26
          Width = 210
          Height = 21
          HelpContext = 5003
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME')
          DataField = 'SB_ACCOUNTANT_NBR'
          DataSource = evDataSource8
          LookupField = 'SB_ACCOUNTANT_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
      object evGroupBox9: TevGroupBox
        Left = 0
        Top = 4
        Width = 424
        Height = 214
        Caption = 'CSR SB User/Company Sales Person'
        TabOrder = 1
        object evLabel12: TevLabel
          Left = 9
          Top = 20
          Width = 39
          Height = 13
          Caption = 'SB User'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBGrid8: TevDBGrid
          Left = 8
          Top = 48
          Width = 401
          Height = 153
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'40'#9'COMPANY NAME'#9'F'
            'SB_USER_NBR'#9'13'#9'SB USER NBR'#9'F'
            'OLD_SB_USER_NBR'#9'10'#9'OLD NBR'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid8'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource5
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object evDBLookupCombo6: TevDBLookupCombo
          Left = 65
          Top = 18
          Width = 239
          Height = 21
          HelpContext = 12014
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'FIRST_NAME'#9'20'#9'FIRST_NAME'#9'F'
            'LAST_NAME'#9'30'#9'LAST_NAME'#9'F'
            'MIDDLE_INITIAL'#9'1'#9'MIDDLE_INITIAL'#9'F')
          DataField = 'SB_USER_NBR'
          DataSource = evDataSource5
          LookupField = 'SB_USER_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
      object evGroupBox11: TevGroupBox
        Left = 0
        Top = 392
        Width = 889
        Height = 225
        Caption = 'Company OBC/Billing/Tax/Ach/Trust/WC Bank Accounts'
        TabOrder = 2
        object evLabel6: TevLabel
          Left = 9
          Top = 20
          Width = 85
          Height = 13
          Caption = 'SB Bank Account'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBGrid13: TevDBGrid
          Left = 8
          Top = 50
          Width = 873
          Height = 159
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'40'#9'COMPANY NAME'#9'F'
            'type'#9'10'#9'Type'#9'F'
            'OLD_SB_BANK_ACCOUNT_NBR'#9'20'#9'OLD SB BANK ACCOUNT NBR'#9'F'
            'SB_BANK_ACCOUNT_NBR'#9'20'#9'Sb Bank Account Nbr'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid13'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource7
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object evDBLookupCombo5: TevDBLookupCombo
          Left = 98
          Top = 20
          Width = 455
          Height = 21
          HelpContext = 10581
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME_DESCRIPTION'#9'40'#9'NAME_DESCRIPTION'
            'CUSTOM_BANK_ACCOUNT_NUMBER'#9'20'#9'CUSTOM_BANK_ACCOUNT_NUMBER'
            'Bank_Name_Lookup'#9'40'#9'Bank_Name_Lookup'#9'F')
          DataField = 'SB_BANK_ACCOUNT_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = True
          OnChange = evDBLookupCombo5Change
          OnBeforeDropDown = evDBLookupCombo5BeforeDropDown
        end
        object Button2: TButton
          Left = 710
          Top = 180
          Width = 75
          Height = 25
          Caption = 'Save INI file'
          TabOrder = 2
          Visible = False
          OnClick = Button2Click
        end
      end
      object evGroupBox12: TevGroupBox
        Left = 429
        Top = 4
        Width = 449
        Height = 213
        Caption = 'Company Service'
        TabOrder = 3
        object evLabel7: TevLabel
          Left = 9
          Top = 22
          Width = 50
          Height = 13
          Caption = 'SB Sevice'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBGrid15: TevDBGrid
          Left = 8
          Top = 46
          Width = 433
          Height = 152
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'40'#9'SERVICE NAME'#9'F'
            'SB_SERVICES_NBR'#9'10'#9'SB SERVICES NBR'#9'T'
            'OLD_SB_SERVICES_NBR'#9'10'#9'OLD NBR'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid15'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource4
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object evDBLookupCombo1: TevDBLookupCombo
          Left = 72
          Top = 21
          Width = 241
          Height = 21
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'SERVICE_NAME'#9'40'#9'SERVICE_NAME')
          DataField = 'SB_SERVICES_NBR'
          DataSource = evDataSource4
          LookupTable = DM_SB_SERVICES.SB_SERVICES
          LookupField = 'SB_SERVICES_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
        end
      end
      object evGroupBox13: TevGroupBox
        Left = 429
        Top = 222
        Width = 453
        Height = 164
        Caption = 'Company Referral'
        TabOrder = 4
        object evLabel5: TevLabel
          Left = 9
          Top = 28
          Width = 54
          Height = 13
          Caption = 'SB Referral'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object evDBGrid17: TevDBGrid
          Left = 8
          Top = 47
          Width = 438
          Height = 105
          DisableThemesInTitle = False
          Selected.Strings = (
            'NAME'#9'40'#9'COMPANY NAME'#9'F'
            'SB_REFERRALS_NBR'#9'10'#9'Sb Referrals Nbr'#9#9
            'OLD_SB_REFERRALS_NBR'#9'10'#9'OLD NBR'#9'F')
          IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
          IniAttributes.SectionName = 'TFormMoveClient\evDBGrid17'
          IniAttributes.Delimiter = ';;'
          ExportOptions.ExportType = wwgetSYLK
          ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          DataSource = evDataSource9
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
          TabOrder = 0
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          PaintOptions.AlternatingRowColor = clCream
        end
        object wwlcSB_Referrals_Nbr: TevDBLookupCombo
          Left = 74
          Top = 27
          Width = 212
          Height = 21
          HelpContext = 10506
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'NAME'#9'40'#9'NAME')
          DataField = 'SB_REFERRALS_NBR'
          DataSource = evDataSource9
          LookupField = 'SB_REFERRALS_NBR'
          Style = csDropDownList
          TabOrder = 1
          AutoDropDown = True
          ShowButton = True
          PreciseEditRegion = False
          AllowClearKey = False
          OnBeforeDropDown = wwlcSB_Referrals_NbrBeforeDropDown
        end
      end
      object Button1: TButton
        Left = 710
        Top = 555
        Width = 75
        Height = 25
        Caption = 'Save'
        TabOrder = 5
        OnClick = Button1Click
      end
    end
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 832
    Top = 389
  end
  object DM_CLIENT: TDM_CLIENT
    Left = 784
    Top = 389
  end
  object cdsSbBankAccountsForTrust: TevClientDataSet
    Filter = 'TRUST_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 36
    Top = 591
  end
  object cdsSbBankAccountsForTax: TevClientDataSet
    Filter = 'TAX_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 100
    Top = 591
  end
  object cdsSbBankAccountsForBilling: TevClientDataSet
    Filter = 'BILLING_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 68
    Top = 591
  end
  object cdsSbBankAccountsForWC: TevClientDataSet
    Filter = 'WORKERS_COMP_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 132
    Top = 591
  end
  object cdsSbBankAccountsForOBC: TevClientDataSet
    Filter = 'OBC_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 164
    Top = 591
  end
  object DM_COMPANY: TDM_COMPANY
    Left = 808
    Top = 389
  end
  object cdTypes: TevClientDataSet
    FieldDefs = <
      item
        Name = 'SB_DELIVERY_METHOD_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CLASS_NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end>
    Left = 200
    Top = 592
    object cdTypesSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_METHOD_NBR'
    end
    object cdTypesCLASS_NAME: TStringField
      FieldName = 'CLASS_NAME'
      Size = 40
    end
    object cdTypesNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
  end
  object evDataSource2: TevDataSource
    DataSet = DM_CL_AGENCY.CL_AGENCY
    Left = 40
    Top = 621
  end
  object evDataSource1: TevDataSource
    DataSet = DM_CL_BANK_ACCOUNT.CL_BANK_ACCOUNT
    Top = 621
  end
  object evDataSource3: TevDataSource
    DataSet = DM_CL_DELIVERY_METHOD.CL_DELIVERY_METHOD
    Left = 80
    Top = 629
  end
  object evDataSource4: TevDataSource
    DataSet = DS_Service
    Left = 616
    Top = 160
  end
  object evDataSource5: TevDataSource
    DataSet = DS_USER
    Left = 336
    Top = 128
  end
  object DS_Service: TevClientDataSet
    IndexFieldNames = 'SB_SERVICES_NBR'
    FieldDefs = <
      item
        Name = 'SB_SERVICES_NBR'
        DataType = ftInteger
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'OLD_SB_SERVICES_NBR'
        DataType = ftInteger
      end>
    Left = 584
    Top = 160
    object DS_ServiceSB_SERVICES_NBR: TIntegerField
      FieldName = 'SB_SERVICES_NBR'
    end
    object DS_ServiceNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'NAME'
      Calculated = True
    end
    object DS_ServiceOLD_SB_SERVICES_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_SB_SERVICES_NBR'
      Calculated = True
    end
  end
  object DS_banks: TevClientDataSet
    FieldDefs = <
      item
        Name = 'OLD_SB_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SB_BANK_ACCOUNT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'type'
        DataType = ftString
        Size = 10
      end>
    Left = 336
    Top = 432
    object DS_banksOLD_SB_BANK_ACCOUNT_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_SB_BANK_ACCOUNT_NBR'
      Calculated = True
    end
    object StringField1: TStringField
      FieldName = 'Name'
    end
    object DS_banksSB_BANK_ACCOUNT_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SB_BANK_ACCOUNT_NBR'
      Calculated = True
    end
    object DS_bankstype: TStringField
      FieldKind = fkCalculated
      FieldName = 'type'
      Size = 10
      Calculated = True
    end
  end
  object evDataSource7: TevDataSource
    DataSet = DS_banks
    Left = 368
    Top = 432
  end
  object DS_USER: TevClientDataSet
    FieldDefs = <
      item
        Name = 'NAME'
        DataType = ftString
        Size = 20
      end>
    OpenWithLookups = False
    Left = 376
    Top = 128
    object DS_USEROLD_SB_USER_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_SB_USER_NBR'
      Calculated = True
    end
    object StringField2: TStringField
      FieldKind = fkInternalCalc
      FieldName = 'NAME'
    end
    object DS_USERSB_USER_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SB_USER_NBR'
      Calculated = True
    end
  end
  object cdsSbBankAccountsForAch: TevClientDataSet
    Filter = 'ACH_ACCOUNT = '#39'Y'#39
    Filtered = True
    Left = 4
    Top = 591
  end
  object DS_Accountant: TevClientDataSet
    FieldDefs = <
      item
        Name = 'OLD_SB_ACCOUNTANT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SB_ACCOUNTANT_NBR'
        DataType = ftInteger
      end>
    Left = 80
    Top = 344
    object DS_AccountantOLD_SB_ACCOUNTANT_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_SB_ACCOUNTANT_NBR'
      Calculated = True
    end
    object StringField3: TStringField
      FieldKind = fkCalculated
      FieldName = 'NAME'
      Calculated = True
    end
    object DS_AccountantSB_ACCOUNTANT_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SB_ACCOUNTANT_NBR'
      Calculated = True
    end
  end
  object evDataSource8: TevDataSource
    DataSet = DS_Accountant
    Left = 112
    Top = 344
  end
  object DS_Referrals: TevClientDataSet
    FieldDefs = <
      item
        Name = 'OLD_SB_REFERRALS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'NAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SB_REFERRALS_NBR'
        DataType = ftInteger
      end>
    Left = 472
    Top = 352
    object DS_ReferralsOLD_SB_REFERRALS_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_SB_REFERRALS_NBR'
      Calculated = True
    end
    object StringField4: TStringField
      FieldKind = fkCalculated
      FieldName = 'NAME'
      Calculated = True
    end
    object DS_ReferralsSB_REFERRALS_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SB_REFERRALS_NBR'
      Calculated = True
    end
  end
  object evDataSource9: TevDataSource
    DataSet = DS_Referrals
    Left = 504
    Top = 352
  end
  object DS_Client: TevClientDataSet
    FieldDefs = <
      item
        Name = 'OLD_SB_ACCOUNTANT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SB_ACCOUNTANT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'OLD_CUST_SERVICE_SB_USER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'CUST_SERVICE_SB_USER_NBR'
        DataType = ftInteger
      end
      item
        Name = 'OLD_CSR_SB_TEAM_NBR'
        DataType = ftInteger
      end>
    OpenWithLookups = False
    Left = 24
    Top = 216
    object DS_ClientOLD_SB_ACCOUNTANT_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_SB_ACCOUNTANT_NBR'
      Calculated = True
    end
    object DS_ClientSB_ACCOUNTANT_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SB_ACCOUNTANT_NBR'
      Calculated = True
    end
    object DS_ClientOLD_CUST_SERVICE_SB_USER_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_CUST_SERVICE_SB_USER_NBR'
      Calculated = True
    end
    object DS_ClientCUST_SERVICE_SB_USER_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CUST_SERVICE_SB_USER_NBR'
      Calculated = True
    end
    object DS_ClientOLD_CSR_SB_TEAM_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_CSR_SB_TEAM_NBR'
      Calculated = True
    end
    object DS_ClientCSR_SB_TEAM_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CSR_SB_TEAM_NBR'
      Calculated = True
    end
  end
  object evDataSource10: TevDataSource
    DataSet = DM_CL.CL
    Left = 56
    Top = 216
  end
  object evDataSource11: TevDataSource
    DataSet = DM_CL_MAIL_BOX_GROUP.CL_MAIL_BOX_GROUP
    Left = 16
    Top = 376
  end
  object evDataSource12: TevDataSource
    DataSet = DS_Client
    Left = 32
    Top = 544
  end
  object DS_bank: TevClientDataSet
    FieldDefs = <
      item
        Name = 'BANK ACCOUNT NBR'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'OLD_SB_BANKS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SB_BANKS_NBR'
        DataType = ftInteger
      end>
    OpenWithLookups = False
    Left = 696
    Top = 96
    object DS_bankBANKACCOUNTNBR: TStringField
      FieldKind = fkCalculated
      FieldName = 'BANK ACCOUNT NBR'
      Size = 40
      Calculated = True
    end
    object DS_bankOLD_SB_BANKS_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_SB_BANKS_NBR'
      Calculated = True
    end
    object DS_bankSB_BANKS_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SB_BANKS_NBR'
      Calculated = True
    end
  end
  object evDataSource13: TevDataSource
    DataSet = DS_bank
    Left = 728
    Top = 96
  end
  object DS_agency: TevClientDataSet
    FieldDefs = <
      item
        Name = 'OLD_SB_AGENCY_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SB_AGENCY_NBR'
        DataType = ftInteger
      end>
    Left = 664
    Top = 288
    object DS_agencyOLD_SB_AGENCY_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_SB_AGENCY_NBR'
      Calculated = True
    end
    object DS_agencySB_AGENCY_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SB_AGENCY_NBR'
      Calculated = True
    end
  end
  object evDataSource14: TevDataSource
    DataSet = DS_agency
    Left = 696
    Top = 288
  end
  object DS_delivery_service: TevClientDataSet
    FieldDefs = <
      item
        Name = 'NAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'SB_DELIVERY_COMPANY_SVCS_NBR'
        DataType = ftInteger
      end
      item
        Name = 'OLD_SB_DELIVERY_COMPANY_SVCS_NBR'
        DataType = ftInteger
      end>
    Left = 656
    Top = 416
    object DS_delivery_serviceNAME: TStringField
      FieldName = 'NAME'
      Size = 40
    end
    object DS_delivery_serviceSB_DELIVERY_COMPANY_SVCS_NBR: TIntegerField
      FieldName = 'SB_DELIVERY_COMPANY_SVCS_NBR'
    end
    object DS_delivery_serviceOLD_SB_DELIVERY_SVCS_NBR: TIntegerField
      FieldName = 'OLD_SB_DELIVERY_COMPANY_SVCS_NBR'
    end
  end
  object evDataSource6: TevDataSource
    DataSet = DS_delivery_service
    Left = 688
    Top = 416
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'ini'
    Left = 336
  end
  object OpenDialog1: TOpenDialog
    Filter = 'extension|*.ini'
    Left = 368
  end
  object DS_media: TevClientDataSet
    FieldDefs = <
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'OLD_SB_MEDIA_TYPE_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SB_MEDIA_TYPE_NBR'
        DataType = ftInteger
      end>
    Left = 64
    Top = 512
    object DS_mediaDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object DS_mediaOLD_SB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'OLD_SB_MEDIA_TYPE_NBR'
    end
    object DS_mediaSB_MEDIA_TYPE_NBR: TIntegerField
      FieldName = 'SB_MEDIA_TYPE_NBR'
    end
  end
  object evDataSource15: TevDataSource
    DataSet = DS_media
    Left = 96
    Top = 512
  end
  object evDataSource16: TevDataSource
    DataSet = DS_method
    Left = 504
    Top = 528
  end
  object DS_method: TevClientDataSet
    FieldDefs = <
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'OLD_SB_DELIVERY_METHOD_NBR'
        DataType = ftInteger
      end
      item
        Name = 'SB_DELIVERY_METHOD_NBR'
        DataType = ftInteger
      end>
    Left = 472
    Top = 528
    object DS_methodDESCRIPTION: TStringField
      FieldKind = fkCalculated
      FieldName = 'DESCRIPTION'
      Size = 40
      Calculated = True
    end
    object DS_methodOLD_SB_DELIVERY_METHOD_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OLD_SB_DELIVERY_METHOD_NBR'
      Calculated = True
    end
    object DS_methodSB_DELIVERY_METHOD_NBR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SB_DELIVERY_METHOD_NBR'
      Calculated = True
    end
  end
end
