inherited EDIT_SB_DELIVERY_COMPANY_SVCS: TEDIT_SB_DELIVERY_COMPANY_SVCS
  object lablDelivery_Company: TevLabel [0]
    Left = 16
    Top = 8
    Width = 88
    Height = 13
    Caption = 'Delivery Company:'
  end
  object dtxtDeliver_Company: TevDBText [1]
    Left = 112
    Top = 8
    Width = 201
    Height = 17
    DataField = 'NAME'
    DataSource = wwdsMaster
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object PageControl1: TevPageControl [2]
    Left = 16
    Top = 32
    Width = 593
    Height = 433
    HelpContext = 35502
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Browse'
      object lablServices: TevLabel
        Left = 296
        Top = 355
        Width = 41
        Height = 13
        Caption = 'Services'
        FocusControl = dedtDescription
      end
      object lablReference_Fee: TevLabel
        Left = 512
        Top = 355
        Width = 71
        Height = 13
        Caption = 'Reference Fee'
      end
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 289
        Height = 404
        Selected.Strings = (
          'NAME'#9'40'#9'Name')
        IniAttributes.FileName = 'DELPHI32.ini'
        IniAttributes.SectionName = 'EDIT_SB_DELIVERY_COMPANY_SVCSwwDBGrid1'
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsMaster
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
      object wwDBGrid2: TevDBGrid
        Left = 296
        Top = 0
        Width = 289
        Height = 348
        Selected.Strings = (
          'DESCRIPTION'#9'40'#9'Description')
        IniAttributes.FileName = 'DELPHI32.ini'
        IniAttributes.SectionName = 'EDIT_SB_DELIVERY_COMPANY_SVCSwwDBGrid2'
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        IndicatorColor = icBlack
      end
      object dedtDescription: TevDBEdit
        Left = 296
        Top = 371
        Width = 209
        Height = 21
        DataField = 'DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwdeReference_Fee: TevDBEdit
        Left = 512
        Top = 371
        Width = 65
        Height = 21
        DataField = 'REFERENCE_FEE'
        DataSource = wwdsDetail
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    DataSet = DM_SB_DELIVERY_COMPANY.SB_DELIVERY_COMPANY
    Top = 26
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_DELIVERY_COMPANY_SVCS.SB_DELIVERY_COMPANY_SVCS
    Top = 34
  end
  inherited wwdsList: TevDataSource
    Left = 98
    Top = 34
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 288
    Top = 16
  end
end
