unit UpdateEEFICAFlag;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, SDDClasses, SDataDictbureau, SDataStructure, wwdblook,
  isBaseClasses,
  ISBasicClasses,  EvTypes, EvUtils, DBCtrls, EvConsts, EvExceptions, EvUIComponents,
  isUIwwDBLookupCombo, isUIEdit, ComCtrls, LMDCustomButton, LMDButton,
  isUILMDButton, ExtCtrls, LMDBaseControl, LMDBaseGraphicControl,
  LMDBaseGraphicButton, LMDCustomSpeedButton, LMDSpeedButton,
  isUISpeedButton, EvUIUtils;

type
  TdlgUpdateEEFICAFlag =  class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    odFICA: TOpenDialog;
    rgUpdateFICA: TevRadioGroup;
    edtFileName: TevEdit;
    evLabel1: TevLabel;
    btnFileName: TevSpeedButton;
    edtEffective: TevDateTimePicker;
    evLabel2: TevLabel;
    procedure btnFileNameClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgUpdateEEFICAFlag: TdlgUpdateEEFICAFlag;

implementation

{$R *.dfm}

procedure TdlgUpdateEEFICAFlag.btnFileNameClick(Sender: TObject);
begin
    if odFICA.Execute then
    edtFileName.Text := odFICA.FileName;
end;

procedure TdlgUpdateEEFICAFlag.OKBtnClick(Sender: TObject);
begin
   if (rgUpdateFICA.ItemIndex = 1) and  (Trim(edtFileName.Text) = '') then
   begin
     EvMessage('Please select import file');
     ModalResult := mrNone;
   end;
end;

end.
