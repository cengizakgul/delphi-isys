// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_AGENCY;

interface

uses SFrameEntry, DBCtrls,  ExtCtrls, wwdblook, Wwdotdot,
  Wwdbcomb, StdCtrls, Mask, wwdbedit, Controls, Grids, Wwdbigrd, Wwdbgrid,
  ComCtrls, Classes, Db, Wwdatsrc, SDataStructure, EvUIComponents, EvClientDataSet;

type  
  TEDIT_SB_AGENCY = class(TFrameEntry)
    pctlEDIT_SB_AGENCY: TevPageControl;
    tshtBrowse: TTabSheet;
    TabSheet1: TTabSheet;
    lablName: TevLabel;
    lablAddress1: TevLabel;
    lablAddress2: TevLabel;
    lablCity: TevLabel;
    lablCounty: TevLabel;
    lablFax: TevLabel;
    lablE_Mail: TevLabel;
    dedtName: TevDBEdit;
    dedtAddress1: TevDBEdit;
    dedtAddress2: TevDBEdit;
    dedtCity: TevDBEdit;
    dedtCounty: TevDBEdit;
    dedtFax_Description: TevDBEdit;
    dedtE_Mail: TevDBEdit;
    wwdeFax: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    wwdgEDIT_SB_AGENCY: TevDBGrid;
    TabSheet2: TTabSheet;
    lablAgency_Type: TevLabel;
    lablNotes: TevLabel;
    dmemNotes: TEvDBMemo;
    lablModem_Nbr: TevLabel;
    wwdeModem_Nbr: TevDBEdit;
    lablAccount_Nbr: TevLabel;
    dedtAccount_Nbr: TevDBEdit;
    lablBank: TevLabel;
    wwcbAgency_Type: TevDBComboBox;
    drgpAccount_Type: TevDBRadioGroup;
    drgpNegative_Direct_Deposit_Allowed: TevDBRadioGroup;
    dedtState: TevDBEdit;
    wwlcBank: TevDBLookupCombo;
    wwDataSource1: TevDataSource;
    lablFax_Description: TevLabel;
    grbxPrimary_Contact: TevGroupBox;
    lablName1: TevLabel;
    dedtName1: TevDBEdit;
    lablPhone1: TevLabel;
    wwedPhone1: TevDBEdit;
    lablDescription1: TevLabel;
    dedtDescription1: TevDBEdit;
    grbxSecondary_Contact: TevGroupBox;
    lablName2: TevLabel;
    lablPhone2: TevLabel;
    lablDescription2: TevLabel;
    dedtName2: TevDBEdit;
    dedtDescription2: TevDBEdit;
    wwdePhone2: TevDBEdit;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    lablPrint_Name: TevLabel;
    wwdePrint_Name: TevDBEdit;
    Panel1: TevPanel;
    lablAgency: TevLabel;
    dtxtNAME: TevDBText;
    bevSBagency1: TEvBevel;
    bevSBagency2: TEvBevel;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
  end;

var
  EDIT_SB_AGENCY: TEDIT_SB_AGENCY;

implementation

uses EvUtils;

{$R *.DFM}

function TEDIT_SB_AGENCY.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_AGENCY;
end;

function TEDIT_SB_AGENCY.GetInsertControl: TWinControl;
begin
  Result := dedtNAME;
end;

procedure TEDIT_SB_AGENCY.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_BANKS, SupportDataSets, '');
end;

initialization
  RegisterClass(TEDIT_SB_AGENCY);

end.
