inherited EDIT_SB_BANKS: TEDIT_SB_BANKS
  Width = 810
  Height = 581
  object lablSB_BANKS_NBR: TevLabel [0]
    Left = 16
    Top = 4
    Width = 25
    Height = 13
    Caption = 'Bank'
  end
  object dtxtName: TevDBText [1]
    Left = 112
    Top = 4
    Width = 497
    Height = 17
    DataField = 'NAME'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dtxtSB_Banks_Nbr: TevDBText [2]
    Left = 48
    Top = 4
    Width = 49
    Height = 17
    DataField = 'SB_BANKS_NBR'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object pctlEDIT_SB_BANKS: TevPageControl [3]
    Left = 4
    Top = 32
    Width = 617
    Height = 419
    HelpContext = 21502
    ActivePage = tshtBanks
    TabOrder = 0
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object wwdgEDIT_SB_BANKS: TevDBGrid
        Left = 0
        Top = 0
        Width = 609
        Height = 393
        DisableThemesInTitle = False
        Selected.Strings = (
          'ABA_NUMBER'#9'9'#9'ABA Number'
          'NAME'#9'40'#9'Name'
          'CITY'#9'20'#9'City'
          'STATE'#9'2'#9'State')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_BANKS\wwdgEDIT_SB_BANKS'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tshtBanks: TTabSheet
      Caption = 'Details'
      object bevSB_BANKS2: TEvBevel
        Left = 0
        Top = 0
        Width = 609
        Height = 391
        Align = alClient
      end
      object lablName: TevLabel
        Left = 16
        Top = 16
        Width = 37
        Height = 16
        Caption = '~Name'
        FocusControl = wwdeName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablAddress1: TevLabel
        Left = 16
        Top = 112
        Width = 47
        Height = 13
        Caption = 'Address 1'
        FocusControl = wwdeAddress1
      end
      object lablAddress2: TevLabel
        Left = 16
        Top = 160
        Width = 47
        Height = 13
        Caption = 'Address 2'
        FocusControl = wwdeAddress2
      end
      object lablCity: TevLabel
        Left = 16
        Top = 208
        Width = 17
        Height = 13
        Caption = 'City'
        FocusControl = wwdeCity
      end
      object lablFax: TevLabel
        Left = 16
        Top = 256
        Width = 17
        Height = 13
        Caption = 'Fax'
        FocusControl = wwdeFax
      end
      object lablE_Mail: TevLabel
        Left = 16
        Top = 304
        Width = 29
        Height = 13
        Caption = 'E-Mail'
        FocusControl = wwdeE_Mail
      end
      object lablState: TevLabel
        Left = 152
        Top = 208
        Width = 25
        Height = 13
        Caption = 'State'
        FocusControl = dedtState
      end
      object lablZip_Code: TevLabel
        Left = 192
        Top = 208
        Width = 43
        Height = 13
        Caption = 'Zip Code'
        FocusControl = wwdeZip_Code
      end
      object lablFax_Description: TevLabel
        Left = 144
        Top = 256
        Width = 53
        Height = 13
        Caption = 'Description'
        FocusControl = wwdeFax_Description
      end
      object lablPrint_Name: TevLabel
        Left = 16
        Top = 64
        Width = 61
        Height = 16
        Caption = '~Print Name'
        FocusControl = wwdePrint_Name
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object wwdeName: TevDBEdit
        Left = 16
        Top = 32
        Width = 257
        Height = 21
        DataField = 'NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeAddress1: TevDBEdit
        Left = 16
        Top = 128
        Width = 257
        Height = 21
        DataField = 'ADDRESS1'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeAddress2: TevDBEdit
        Left = 16
        Top = 176
        Width = 257
        Height = 21
        DataField = 'ADDRESS2'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeCity: TevDBEdit
        Left = 16
        Top = 224
        Width = 129
        Height = 21
        DataField = 'CITY'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeZip_Code: TevDBEdit
        Left = 192
        Top = 224
        Width = 81
        Height = 21
        DataField = 'ZIP_CODE'
        DataSource = wwdsDetail
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object grbxSecondary_Contact: TevGroupBox
        Left = 296
        Top = 160
        Width = 305
        Height = 129
        Caption = 'Secondary Contact'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
        object lablName2: TevLabel
          Left = 16
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Name'
        end
        object lablPhone2: TevLabel
          Left = 16
          Top = 72
          Width = 31
          Height = 13
          Caption = 'Phone'
        end
        object lablDescription2: TevLabel
          Left = 144
          Top = 72
          Width = 53
          Height = 13
          Caption = 'Description'
        end
        object wwdeName2: TevDBEdit
          Left = 16
          Top = 40
          Width = 273
          Height = 21
          DataField = 'CONTACT2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePhone2: TevDBEdit
          Left = 16
          Top = 88
          Width = 121
          Height = 21
          DataField = 'PHONE2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeDescription2: TevDBEdit
          Left = 144
          Top = 88
          Width = 145
          Height = 21
          DataField = 'DESCRIPTION2'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object wwdeFax: TevDBEdit
        Left = 16
        Top = 272
        Width = 121
        Height = 21
        DataField = 'FAX'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
        TabOrder = 7
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeFax_Description: TevDBEdit
        Left = 144
        Top = 272
        Width = 129
        Height = 21
        DataField = 'FAX_DESCRIPTION'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdeE_Mail: TevDBEdit
        Left = 16
        Top = 320
        Width = 257
        Height = 21
        DataField = 'E_MAIL_ADDRESS'
        DataSource = wwdsDetail
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object grbxPrimary_Contact: TevGroupBox
        Left = 296
        Top = 16
        Width = 305
        Height = 129
        Caption = 'Primary Contact'
        TabOrder = 10
        object lablName1: TevLabel
          Left = 16
          Top = 24
          Width = 37
          Height = 16
          Caption = '~Name'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablPhone1: TevLabel
          Left = 16
          Top = 72
          Width = 31
          Height = 13
          Caption = 'Phone'
        end
        object lablDescription1: TevLabel
          Left = 144
          Top = 72
          Width = 53
          Height = 13
          Caption = 'Description'
        end
        object wwdeName1: TevDBEdit
          Left = 16
          Top = 40
          Width = 273
          Height = 21
          DataField = 'CONTACT1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdePhone: TevDBEdit
          Left = 16
          Top = 88
          Width = 121
          Height = 21
          DataField = 'PHONE1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeDescription1: TevDBEdit
          Left = 144
          Top = 88
          Width = 145
          Height = 21
          DataField = 'DESCRIPTION1'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object dedtState: TevDBEdit
        Left = 152
        Top = 224
        Width = 33
        Height = 21
        CharCase = ecUpperCase
        DataField = 'STATE'
        DataSource = wwdsDetail
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePrint_Name: TevDBEdit
        Left = 16
        Top = 80
        Width = 257
        Height = 21
        DataField = 'PRINT_NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
    object tshtAccount_Info: TTabSheet
      Caption = 'Account Info'
      object Bevel1: TEvBevel
        Left = 0
        Top = 0
        Width = 609
        Height = 391
        Align = alClient
      end
      object lablAddenda: TevLabel
        Left = 176
        Top = 136
        Width = 43
        Height = 13
        Caption = 'Addenda'
      end
      object lablCheck_Template: TevLabel
        Left = 176
        Top = 88
        Width = 78
        Height = 13
        Caption = 'Check Template'
      end
      object Label1: TevLabel
        Left = 32
        Top = 204
        Width = 77
        Height = 13
        Caption = 'Branch Identifier'
        FocusControl = wwdeBranch_Identifier
      end
      object wwdeAddenda: TevDBEdit
        Left = 176
        Top = 152
        Width = 185
        Height = 21
        DataField = 'ADDENDA'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dmemCheck_Template: TEvDBMemo
        Left = 176
        Top = 104
        Width = 185
        Height = 21
        DataField = 'CHECK_TEMPLATE'
        DataSource = wwdsDetail
        TabOrder = 2
      end
      object drgpUse_Check_Template: TevDBRadioGroup
        Left = 176
        Top = 16
        Width = 185
        Height = 57
        Caption = '~Use Check Template'
        Columns = 2
        DataField = 'USE_CHECK_TEMPLATE'
        DataSource = wwdsDetail
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          'Yes'
          'No')
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          'Y'
          'N')
      end
      object grbxABA_Information: TevGroupBox
        Left = 16
        Top = 16
        Width = 129
        Height = 169
        Caption = 'ABA Information'
        TabOrder = 0
        object lablABA_Nbr: TevLabel
          Left = 16
          Top = 20
          Width = 81
          Height = 13
          AutoSize = False
          Caption = '~ABA Number'
          FocusControl = wwdeABA_Nbr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablTop_ABA_Nbr: TevLabel
          Left = 16
          Top = 68
          Width = 52
          Height = 16
          Caption = '~Top ABA'
          FocusControl = wwdeTop_ABA_Nbr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lablBottom_ABA_Nbr: TevLabel
          Left = 16
          Top = 116
          Width = 81
          Height = 13
          AutoSize = False
          Caption = '~Bottom ABA'
          FocusControl = wwdeBottom_ABA_Nbr
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object wwdeABA_Nbr: TevDBEdit
          Left = 16
          Top = 36
          Width = 97
          Height = 21
          DataField = 'ABA_NUMBER'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
          UsePictureMask = False
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeTop_ABA_Nbr: TevDBEdit
          Left = 16
          Top = 84
          Width = 97
          Height = 21
          DataField = 'TOP_ABA_NUMBER'
          DataSource = wwdsDetail
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object wwdeBottom_ABA_Nbr: TevDBEdit
          Left = 16
          Top = 132
          Width = 97
          Height = 21
          DataField = 'BOTTOM_ABA_NUMBER'
          DataSource = wwdsDetail
          Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
      end
      object gbABA2: TevGroupBox
        Left = 384
        Top = 16
        Width = 193
        Height = 169
        Caption = 'MICR Line'
        TabOrder = 4
        object lablMICR_Account_Start_Position: TevLabel
          Left = 16
          Top = 24
          Width = 135
          Height = 13
          Caption = 'MICR Account Start Position'
        end
        object lablMICR_Check_Nbr_Start_Position: TevLabel
          Left = 16
          Top = 80
          Width = 146
          Height = 13
          Caption = 'MICR Check Nbr Start Position'
        end
        object wwseMICR_Account_Start_Position: TevDBSpinEdit
          Left = 16
          Top = 40
          Width = 57
          Height = 21
          Increment = 1.000000000000000000
          DataField = 'MICR_ACCOUNT_START_POSITION'
          DataSource = wwdsDetail
          TabOrder = 0
          UnboundDataType = wwDefault
        end
        object wwseMICR_Check_Nbr_Start_Position: TevDBSpinEdit
          Left = 16
          Top = 96
          Width = 57
          Height = 21
          Increment = 1.000000000000000000
          DataField = 'MICR_CHECK_NUMBER_START_POSITN'
          DataSource = wwdsDetail
          TabOrder = 1
          UnboundDataType = wwDefault
        end
      end
      object wwdeBranch_Identifier: TevDBEdit
        Left = 32
        Top = 220
        Width = 97
        Height = 21
        DataField = 'BRANCH_IDENTIFIER'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object cbAllowHyphens: TevDBCheckBox
        Left = 176
        Top = 222
        Width = 97
        Height = 17
        Caption = 'Allow Hyphens'
        DataField = 'ALLOW_HYPHENS'
        DataSource = wwdsDetail
        TabOrder = 6
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
    object tsSettings: TTabSheet
      Caption = 'Settings'
      ImageIndex = 3
      object lCTS: TevLabel
        Left = 18
        Top = 102
        Width = 35
        Height = 13
        Caption = 'CTS ID'
        FocusControl = eCTS
      end
      object rgMergeFileFormat: TevRadioGroup
        Left = 16
        Top = 16
        Width = 185
        Height = 73
        Caption = '~Merge file format'
        Items.Strings = (
          'Generic 2000'
          'Keybank 2002')
        TabOrder = 0
        OnClick = rgMergeFileFormatClick
      end
      object eCTS: TevDBEdit
        Left = 64
        Top = 99
        Width = 136
        Height = 21
        DataField = 'CtsId'
        DataSource = wwdsDetail
        MaxLength = 6
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Top = 2
  end
  inherited wwdsDetail: TevDataSource
    OnDataChange = wwdsDetailDataChange
    OnUpdateData = wwdsDetailUpdateData
    MasterDataSource = nil
  end
  inherited wwdsList: TevDataSource
    Left = 82
    Top = 2
  end
end
