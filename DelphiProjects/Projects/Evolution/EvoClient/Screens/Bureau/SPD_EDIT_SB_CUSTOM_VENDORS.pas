// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_CUSTOM_VENDORS;

interface

uses
  SFrameEntry,  SDataStructure, DBCtrls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet, SDDClasses,
  SDataDictsystem, isUIwwDBEdit, ISBasicClasses, Wwdbcomb,
  isUIwwDBComboBox, wwdblook, isUIwwDBLookupCombo, SDataDictbureau, EvUtils, Variants, SPackageEntry,
  EvConsts, EvExceptions;

type
  TEDIT_SB_CUSTOM_VENDORS = class(TFrameEntry)
    wwDBGrid1: TevDBGrid;
    evLabel1: TevLabel;
    edtName: TevDBEdit;
    evLabel2: TevLabel;
    lcbCategories: TevDBLookupCombo;
    cbType: TevDBComboBox;
    Label14: TevLabel;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    procedure ButtonClicked(Kind: Integer; var Handled: Boolean); override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    function GetInsertControl: TWinControl; override;
    { Public declarations }
  end;

implementation

{$R *.DFM}

function TEDIT_SB_CUSTOM_VENDORS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_CUSTOM_VENDORS;
end;

procedure TEDIT_SB_CUSTOM_VENDORS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_VENDOR_CATEGORIES, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_VENDOR, SupportDataSets, 'ALL');
end;

procedure TEDIT_SB_CUSTOM_VENDORS.ButtonClicked(Kind: Integer;
  var Handled: Boolean);
begin
  inherited;
  if Kind = NavDelete then
    if DM_SERVICE_BUREAU.SB_VENDOR.Locate('VENDORS_LEVEL;VENDOR_NBR',
                       VarArrayOf([ LEVEL_BUREAU, DM_SERVICE_BUREAU.SB_CUSTOM_VENDORS.FieldByName('SB_CUSTOM_VENDORS_NBR').AsVariant]),[]) then
         raise EUpdateError.CreateHelp('This Custom Vendor is attached to the Service Bureau, it must be removed before it can be deleted.', IDH_ConsistencyViolation);
end;

function TEDIT_SB_CUSTOM_VENDORS.GetInsertControl: TWinControl;
begin
  Result := edtName;
end;

initialization
  RegisterClass(TEDIT_SB_CUSTOM_VENDORS);

end.
