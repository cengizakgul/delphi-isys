// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit MCTTaxRateUpdate;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, EvTypes,
     Buttons, ExtCtrls, SDataStructure,  wwdblook, EvUtils, Spin,
     EvExceptions, SDDClasses, SDataDictsystem, ISBasicClasses, EvUIComponents;

type
  TMCTTaxRateUpdate = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    evLabel3: TevLabel;
    cbQuarter: TevComboBox;
    edYear: TevSpinEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses DateUtils;

{$R *.dfm}

procedure TMCTTaxRateUpdate.FormCreate(Sender: TObject);
begin
  edYear.Value := YearOf(Date);
end;

end.
