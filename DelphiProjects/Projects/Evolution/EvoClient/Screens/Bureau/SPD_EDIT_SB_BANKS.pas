// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_BANKS;

interface

uses Classes, SFrameEntry, Wwdbspin,  ExtCtrls, DBCtrls,
  StdCtrls, Mask, wwdbedit, Controls, Grids, Wwdbigrd, Wwdbgrid, ComCtrls,
  Db, Wwdatsrc, ISBasicClasses, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_BANKS = class(TFrameEntry)
    pctlEDIT_SB_BANKS: TevPageControl;
    tshtBrowse: TTabSheet;
    wwdgEDIT_SB_BANKS: TevDBGrid;
    tshtBanks: TTabSheet;
    lablSB_BANKS_NBR: TevLabel;
    lablName: TevLabel;
    lablAddress1: TevLabel;
    lablAddress2: TevLabel;
    lablCity: TevLabel;
    lablFax: TevLabel;
    lablE_Mail: TevLabel;
    wwdeName: TevDBEdit;
    wwdeAddress1: TevDBEdit;
    wwdeAddress2: TevDBEdit;
    wwdeCity: TevDBEdit;
    wwdeZip_Code: TevDBEdit;
    wwdeFax: TevDBEdit;
    wwdeFax_Description: TevDBEdit;
    wwdeE_Mail: TevDBEdit;
    grbxSecondary_Contact: TevGroupBox;
    lablName2: TevLabel;
    lablPhone2: TevLabel;
    lablDescription2: TevLabel;
    wwdeName2: TevDBEdit;
    wwdePhone2: TevDBEdit;
    wwdeDescription2: TevDBEdit;
    tshtAccount_Info: TTabSheet;
    lablAddenda: TevLabel;
    wwdeAddenda: TevDBEdit;
    lablCheck_Template: TevLabel;
    dmemCheck_Template: TEvDBMemo;
    drgpUse_Check_Template: TevDBRadioGroup;
    grbxABA_Information: TevGroupBox;
    lablABA_Nbr: TevLabel;
    lablTop_ABA_Nbr: TevLabel;
    lablBottom_ABA_Nbr: TevLabel;
    wwdeABA_Nbr: TevDBEdit;
    wwdeTop_ABA_Nbr: TevDBEdit;
    wwdeBottom_ABA_Nbr: TevDBEdit;
    lablState: TevLabel;
    lablZip_Code: TevLabel;
    lablFax_Description: TevLabel;
    grbxPrimary_Contact: TevGroupBox;
    lablName1: TevLabel;
    wwdeName1: TevDBEdit;
    lablPhone1: TevLabel;
    wwdePhone: TevDBEdit;
    lablDescription1: TevLabel;
    wwdeDescription1: TevDBEdit;
    dedtState: TevDBEdit;
    lablPrint_Name: TevLabel;
    wwdePrint_Name: TevDBEdit;
    dtxtName: TevDBText;
    dtxtSB_Banks_Nbr: TevDBText;
    bevSB_BANKS2: TEvBevel;
    gbABA2: TevGroupBox;
    lablMICR_Account_Start_Position: TevLabel;
    wwseMICR_Account_Start_Position: TevDBSpinEdit;
    lablMICR_Check_Nbr_Start_Position: TevLabel;
    wwseMICR_Check_Nbr_Start_Position: TevDBSpinEdit;
    Bevel1: TEvBevel;
    wwdeBranch_Identifier: TevDBEdit;
    Label1: TevLabel;
    rgMergeFileFormat: TevRadioGroup;
    lCTS: TevLabel;
    eCTS: TevDBEdit;
    cbAllowHyphens: TevDBCheckBox;
    procedure rgMergeFileFormatClick(Sender: TObject);
    procedure wwdsDetailDataChange(Sender: TObject; Field: TField);
    procedure wwdsDetailUpdateData(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
  end;

var
  EDIT_SB_BANKS: TEDIT_SB_BANKS;

implementation

uses SDataStructure, EvUtils, SysUtils;

{$R *.DFM}

function TEDIT_SB_BANKS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_BANKS;
end;

function TEDIT_SB_BANKS.GetInsertControl: TWinControl;
begin
  Result := wwdeName;
end;

procedure TEDIT_SB_BANKS.rgMergeFileFormatClick(Sender: TObject);
begin
  inherited;
  if rgMergeFileFormat.Tag = 0 then
  begin
    rgMergeFileFormat.Tag := 1;
    try
      wwdsDetail.Edit;
    finally
      rgMergeFileFormat.Tag := 0;
    end;
  end;
end;

procedure TEDIT_SB_BANKS.wwdsDetailDataChange(Sender: TObject;
  Field: TField);
var
  s: string;
  c: Char;
begin
  inherited;
  if rgMergeFileFormat.Tag = 0 then
  begin
    rgMergeFileFormat.Tag := 1;
    try
      s := wwdsDetail.DataSet.FieldByName('FILLER').AsString+ ' ';
      c := s[1];
      case c of
      '0'..'1':
        rgMergeFileFormat.ItemIndex := Ord(c)- Ord('0');
      else
        rgMergeFileFormat.ItemIndex := 0;
      end;
    finally
      rgMergeFileFormat.Tag := 0;
    end;
  end;
end;

procedure TEDIT_SB_BANKS.wwdsDetailUpdateData(Sender: TObject);
begin
  inherited;
  wwdsDetail.DataSet.FieldByName('FILLER').AsString := PutIntoFiller(wwdsDetail.DataSet.FieldByName('FILLER').AsString,
    IntToStr(rgMergeFileFormat.ItemIndex), 1, 1);
end;

initialization
  RegisterClass(TEDIT_SB_BANKS);

end.
