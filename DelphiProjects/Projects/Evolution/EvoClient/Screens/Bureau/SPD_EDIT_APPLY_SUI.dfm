object EDIT_APPLY_SUI: TEDIT_APPLY_SUI
  Left = 592
  Top = 191
  Width = 424
  Height = 324
  Caption = 'Apply SUI to Processed Payrolls '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 16
    Top = 90
    Width = 25
    Height = 13
    Caption = 'State'
  end
  object evLabel2: TevLabel
    Left = 16
    Top = 150
    Width = 46
    Height = 13
    Caption = 'Due Date'
  end
  object evLabel3: TevLabel
    Left = 16
    Top = 60
    Width = 111
    Height = 13
    Caption = 'Check Date End Date  '
  end
  object evLabel4: TevLabel
    Left = 16
    Top = 120
    Width = 18
    Height = 13
    Caption = 'SUI'
  end
  object evLabel5: TevLabel
    Left = 16
    Top = 31
    Width = 125
    Height = 13
    Caption = 'Check Date Begin Date    '
  end
  object edState: TevDBLookupCombo
    Left = 144
    Top = 82
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'STATE'#9'2'#9'STATE'#9'F')
    LookupTable = DM_SY_STATES.SY_STATES
    LookupField = 'STATE'
    Style = csDropDownList
    TabOrder = 2
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
    OnChange = edStateChange
  end
  object edDueDate: TevDateTimePicker
    Left = 144
    Top = 142
    Width = 121
    Height = 21
    Date = 37644.000000000000000000
    Time = 37644.000000000000000000
    TabOrder = 4
  end
  object edEndCheckDate: TevDateTimePicker
    Left = 144
    Top = 52
    Width = 121
    Height = 21
    Date = 37644.000000000000000000
    Time = 37644.000000000000000000
    TabOrder = 1
  end
  object bOk: TevButton
    Left = 152
    Top = 196
    Width = 75
    Height = 25
    Caption = 'Apply'
    TabOrder = 5
    OnClick = bOkClick
  end
  object edSui: TevDBLookupCombo
    Left = 144
    Top = 112
    Width = 121
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'SUI_TAX_NAME'#9'40'#9'Sui Tax Name'#9'F')
    LookupTable = DM_SY_SUI.SY_SUI
    LookupField = 'SY_SUI_NBR'
    Style = csDropDownList
    TabOrder = 3
    AutoDropDown = True
    ShowButton = True
    PreciseEditRegion = False
    AllowClearKey = False
  end
  object edBeginCheckDate: TevDateTimePicker
    Left = 144
    Top = 23
    Width = 121
    Height = 21
    Date = 37644.000000000000000000
    Time = 37644.000000000000000000
    TabOrder = 0
  end
  object DM_SYSTEM_STATE: TDM_SYSTEM_STATE
    Left = 48
    Top = 224
  end
end
