object EDIT_ZERO_ACH_PAYMENT: TEDIT_ZERO_ACH_PAYMENT
  Left = 459
  Top = 277
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Zero ACH payment parameters'
  ClientHeight = 219
  ClientWidth = 341
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object evLabel1: TevLabel
    Left = 8
    Top = 40
    Width = 49
    Height = 13
    Caption = 'Due Date:'
  end
  object evLabel2: TevLabel
    Left = 8
    Top = 16
    Width = 39
    Height = 13
    Caption = 'Agency:'
  end
  object evLabel3: TevLabel
    Left = 8
    Top = 64
    Width = 71
    Height = 13
    Caption = 'Effective Date:'
  end
  object evLabel4: TevLabel
    Left = 8
    Top = 88
    Width = 81
    Height = 13
    Caption = 'Period End Date:'
  end
  object evLabel5: TevLabel
    Left = 8
    Top = 112
    Width = 92
    Height = 13
    Caption = 'Sequence Number:'
    Visible = False
  end
  object dtpDueDate: TevDBDateTimePicker
    Left = 120
    Top = 40
    Width = 145
    Height = 21
    CalendarAttributes.Font.Charset = DEFAULT_CHARSET
    CalendarAttributes.Font.Color = clWindowText
    CalendarAttributes.Font.Height = -11
    CalendarAttributes.Font.Name = 'MS Sans Serif'
    CalendarAttributes.Font.Style = []
    Epoch = 1950
    ShowButton = True
    TabOrder = 1
  end
  object evButton1: TevButton
    Left = 256
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    TabOrder = 5
    OnClick = evButton1Click
  end
  object Memo1: TMemo
    Left = 120
    Top = 144
    Width = 209
    Height = 33
    TabStop = False
    BorderStyle = bsNone
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Lines.Strings = (
      'Note: This utility makes zero ACH '
      'files for EFT credit companies only.')
    ParentFont = False
    ReadOnly = True
    TabOrder = 6
  end
  object evDBLookupCombo1: TevDBLookupCombo
    Left = 120
    Top = 16
    Width = 209
    Height = 21
    DropDownAlignment = taLeftJustify
    Selected.Strings = (
      'AGENCY_NAME'#9'40'#9'AGENCY_NAME'#9'F')
    LookupTable = DM_SY_GLOBAL_AGENCY.SY_GLOBAL_AGENCY
    LookupField = 'SY_GLOBAL_AGENCY_NBR'
    Style = csDropDownList
    TabOrder = 0
    AutoDropDown = True
    ShowButton = True
    AllowClearKey = False
  end
  object dtpEffectiveDate: TevDBDateTimePicker
    Left = 120
    Top = 64
    Width = 145
    Height = 21
    CalendarAttributes.Font.Charset = DEFAULT_CHARSET
    CalendarAttributes.Font.Color = clWindowText
    CalendarAttributes.Font.Height = -11
    CalendarAttributes.Font.Name = 'MS Sans Serif'
    CalendarAttributes.Font.Style = []
    Epoch = 1950
    ShowButton = True
    TabOrder = 2
  end
  object dtpPeriodEndDate: TevDBDateTimePicker
    Left = 120
    Top = 88
    Width = 145
    Height = 21
    CalendarAttributes.Font.Charset = DEFAULT_CHARSET
    CalendarAttributes.Font.Color = clWindowText
    CalendarAttributes.Font.Height = -11
    CalendarAttributes.Font.Name = 'MS Sans Serif'
    CalendarAttributes.Font.Style = []
    Epoch = 1950
    ShowButton = True
    TabOrder = 3
  end
  object edPaymentSeqNumber: TevSpinEdit
    Left = 120
    Top = 112
    Width = 41
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 0
    Visible = False
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 32
    Top = 184
  end
end
