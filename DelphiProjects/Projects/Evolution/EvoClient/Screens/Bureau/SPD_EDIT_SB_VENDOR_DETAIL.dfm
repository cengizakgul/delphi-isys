inherited EDIT_SB_VENDOR_DETAIL: TEDIT_SB_VENDOR_DETAIL
  Width = 863
  Height = 591
  object lblVendor: TevLabel [0]
    Left = 60
    Top = 9
    Width = 57
    Height = 13
    Caption = 'Vendor    '
    FocusControl = dedtDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object evLabel2: TevLabel [1]
    Left = 12
    Top = 9
    Width = 46
    Height = 13
    Caption = 'Vendor    '
    FocusControl = dedtDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object pgSB_Vendor_Detail: TevPageControl [2]
    Left = 8
    Top = 40
    Width = 702
    Height = 473
    HelpContext = 33501
    ActivePage = tshtBrowse
    TabOrder = 0
    OnChange = pgSB_Vendor_DetailChange
    OnChanging = pgSB_Vendor_DetailChanging
    object tshtBrowse: TTabSheet
      Caption = 'Browse'
      object wwdgEDIT_SB_ACCOUNTANT: TevDBGrid
        Left = 0
        Top = 0
        Width = 601
        Height = 409
        DisableThemesInTitle = False
        Selected.Strings = (
          'NAME'#9'80'#9'Name'#9'F'
          'LEVEL_DESC'#9'8'#9'LEVEL'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_VENDOR_DETAIL\wwdgEDIT_SB_ACCOUNTANT'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsSBVendor
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = clCream
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tshtVendorDetail: TTabSheet
      Caption = 'Details'
      object bevSBaccountant1: TEvBevel
        Left = 0
        Top = 0
        Width = 694
        Height = 445
        Align = alClient
      end
      object grdVedorDetail: TevDBGrid
        Left = 13
        Top = 11
        Width = 334
        Height = 230
        HelpContext = 14001
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'DETAIL'#9'40'#9'Detail'#9#9
          'DETAIL_LEVEL_DESC'#9'8'#9'LEVEL'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_VENDOR_DETAIL\grdVedorDetail'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object grdDetailValue: TevDBGrid
        Left = 361
        Top = 11
        Width = 320
        Height = 230
        HelpContext = 14001
        TabStop = False
        DisableThemesInTitle = False
        Selected.Strings = (
          'DETAIL_VALUES'#9'80'#9'Detail Values'#9'F')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_VENDOR_DETAIL\grdDetailValue'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = dsSBVendorDetailValues
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
      object gbSB_Accountant1: TevGroupBox
        Left = 13
        Top = 256
        Width = 334
        Height = 175
        Caption = 'Vendor Detail'
        TabOrder = 2
        object Label1: TevLabel
          Left = 12
          Top = 25
          Width = 36
          Height = 16
          Caption = '~Detail'
          FocusControl = dedtDetail
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dedtDetail: TevDBEdit
          Left = 12
          Top = 41
          Width = 256
          Height = 21
          HelpContext = 14001
          DataField = 'DETAIL'
          DataSource = wwdsDetail
          Picture.PictureMaskFromDataSet = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        object edCategory: TevDBRadioGroup
          Left = 12
          Top = 74
          Width = 180
          Height = 37
          HelpContext = 35076
          Caption = '~Level'
          Columns = 2
          DataField = 'DETAIL_LEVEL'
          DataSource = wwdsDetail
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 1
          Values.Strings = (
            'Y'
            'N')
        end
        object evDBRadioGroup1: TevDBRadioGroup
          Left = 12
          Top = 120
          Width = 180
          Height = 37
          HelpContext = 35076
          Caption = '~CO Detail Required'
          Columns = 2
          DataField = 'CO_DETAIL_REQUIRED'
          DataSource = wwdsDetail
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Items.Strings = (
            'Yes'
            'No')
          ParentFont = False
          TabOrder = 2
          Values.Strings = (
            'Y'
            'N')
        end
      end
      object evGroupBox1: TevGroupBox
        Left = 361
        Top = 256
        Width = 320
        Height = 175
        Caption = 'Vendor Detail Value'
        TabOrder = 3
        object evLabel1: TevLabel
          Left = 13
          Top = 25
          Width = 36
          Height = 16
          Caption = '~Value'
          FocusControl = dedtValue
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object dedtValue: TevDBEdit
          Left = 10
          Top = 41
          Width = 292
          Height = 21
          HelpContext = 14001
          DataField = 'DETAIL_VALUES'
          DataSource = dsSBVendorDetailValues
          Picture.PictureMaskFromDataSet = False
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          Glowing = False
        end
        inline MiniNavigationFrame: TMiniNavigationFrame
          Left = 15
          Top = 74
          Width = 282
          Height = 25
          TabOrder = 1
          inherited SpeedButton1: TevSpeedButton
            Width = 115
            Caption = 'Create'
            Glyph.Data = {00000000}
          end
          inherited SpeedButton2: TevSpeedButton
            Left = 164
            Width = 115
            Caption = 'Delete'
            Glyph.Data = {00000000}
          end
        end
      end
    end
  end
  inherited wwdsDetail: TevDataSource
    DataSet = DM_SB_VENDOR_DETAIL.SB_VENDOR_DETAIL
    OnStateChange = wwdsDetailStateChange
    OnDataChange = wwdsDetailDataChange
  end
  object DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU
    Left = 528
    Top = 40
  end
  object dsSBVendor: TevDataSource
    DataSet = DM_SB_VENDOR.SB_VENDOR
    OnDataChange = dsSBVendorDataChange
    Left = 800
    Top = 88
  end
  object dsSBVendorDetailValues: TevDataSource
    DataSet = DM_SB_VENDOR_DETAIL_VALUES.SB_VENDOR_DETAIL_VALUES
    OnStateChange = dsSBVendorDetailValuesStateChange
    Left = 800
    Top = 120
  end
  object DM_SYSTEM_MISC: TDM_SYSTEM_MISC
    Left = 784
    Top = 184
  end
end
