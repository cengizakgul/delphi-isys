// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_CHANGE_TAX_PAY_METHOD;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, wwdblook,  SDataStructure, ComCtrls, ExtCtrls,
  SDDClasses, SDataDictsystem, ISBasicClasses, EvBasicUtils, EvUIComponents,
  LMDCustomButton, LMDButton, isUILMDButton, isUIwwDBLookupCombo, Mask,
  wwdbedit, Wwdotdot, Wwdbcomb, isUIwwDBComboBox;

type
  TEDIT_CHANGE_TAX_PAY_METHOD = class(TForm)
    evLabel1: TevLabel;
    edState: TevDBLookupCombo;
    evLabel4: TevLabel;
    edLocal: TevDBLookupCombo;
    evLabel5: TevLabel;
    evLabel2: TevLabel;
    evLabel3: TevLabel;
    btnOk: TButton;
    btnCancel: TButton;
    cbCState: TevDBComboBox;
    cbCSui: TevDBComboBox;
    cbNLocal: TevDBComboBox;
    cbCLocal: TevDBComboBox;
    cbNSui: TevDBComboBox;
    cbNState: TevDBComboBox;
    DM_SYSTEM_STATE: TDM_SYSTEM_STATE;
    procedure FormCreate(Sender: TObject);
    procedure edStateChange(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure edLocalChange(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils,EvConsts, SFieldCodeValues, DB;

{$R *.DFM}

procedure TEDIT_CHANGE_TAX_PAY_METHOD.FormCreate(Sender: TObject);
begin
  DM_SYSTEM_STATE.SY_STATES.Activate;
  DM_SYSTEM_STATE.SY_LOCALS.Activate;

  cbCState.Items.Text :=  TaxPaymentMethod_ComboChoices;
  cbCSui.Items.Text :=  TaxPaymentMethod_ComboChoices;
  cbCLocal.Items.Text :=  TaxPaymentMethod_ComboChoices;
  cbNState.Items.Text :=  TaxPaymentMethod_ComboChoices;
  cbNSui.Items.Text :=  TaxPaymentMethod_ComboChoices;
  cbNLocal.Items.Text :=  TaxPaymentMethod_ComboChoices;

end;

procedure TEDIT_CHANGE_TAX_PAY_METHOD.edStateChange(Sender: TObject);
begin
  DM_SYSTEM_STATE.SY_LOCALS.Filter :='SY_STATES_NBR ='+ IntToStr(DM_SYSTEM_STATE.SY_STATES['SY_STATES_NBR']);
  DM_SYSTEM_STATE.SY_LOCALS.Filtered :=True;

  edLocal.Enabled := DM_SYSTEM_STATE.SY_STATES.fieldByName('SY_STATES_NBR').AsInteger > 0;
  cbCState.Enabled := edLocal.Enabled;
  cbNState.Enabled := edLocal.Enabled;
  cbCSui.Enabled := edLocal.Enabled;
  cbNSui.Enabled := edLocal.Enabled;

  edLocal.Clear;
  cbCState.Clear;
  cbNState.Clear;
  cbCSui.Clear;
  cbNSui.Clear;
  cbCLocal.Clear;
  cbNLocal.Clear;
end;


procedure TEDIT_CHANGE_TAX_PAY_METHOD.btnOkClick(Sender: TObject);
begin
  if ((edState.DisplayValue <> '') and (cbCState.Value <> '') and (cbNState.Value <> '' )) or
     ((edState.DisplayValue <> '') and (cbCSui.Value <> '') and (cbNSui.Value <> '' )) or
     ((edLocal.DisplayValue <> '') and (cbCLocal.Value <> '') and (cbNLocal.Value <> '' )) then
       ModalResult := mrOk;

end;

procedure TEDIT_CHANGE_TAX_PAY_METHOD.edLocalChange(Sender: TObject);
begin
  cbCLocal.Enabled := edLocal.DisplayValue <> '';
  cbNLocal.Enabled := cbCLocal.Enabled;
  if not cbCLocal.Enabled then
  begin
    cbCLocal.Clear;
    cbNLocal.Clear;
  end;
end;

procedure TEDIT_CHANGE_TAX_PAY_METHOD.FormDeactivate(Sender: TObject);
begin
  DM_SYSTEM_STATE.SY_LOCALS.Filter :='';
  DM_SYSTEM_STATE.SY_LOCALS.Filtered :=false;
end;

end.
