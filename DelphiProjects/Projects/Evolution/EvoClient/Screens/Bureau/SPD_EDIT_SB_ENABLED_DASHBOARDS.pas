// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_ENABLED_DASHBOARDS;

interface

uses
  SFrameEntry,  SDataStructure, DBCtrls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet, SDDClasses,
  SDataDictsystem, isUIwwDBEdit, ISBasicClasses, SDataDictbureau, EvUtils,
  Forms, SPD_MiniNavigationFrame, LMDCustomButton, LMDButton, isUILMDButton, Variants, EvConsts,
  LMDBaseControl, LMDBaseGraphicControl, LMDBaseGraphicButton,
  LMDCustomSpeedButton, LMDSpeedButton, isUISpeedButton, kbmMemTable,
  ISKbmMemDataSet, ISDataAccessComponents, EvDataAccessComponents, EvContext, SPackageEntry;

type
  TEDIT_SB_ENABLED_DASHBOARDS = class(TFrameEntry)
    grdSbEnanledDashboards: TevDBGrid;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    grdSyDashboards: TevDBGrid;
    dsSyDashboard: TevDataSource;
    btnAddDashboards: TevSpeedButton;
    btnDelDashbords: TevSpeedButton;
    cdSyDashboards: TevClientDataSet;
    cdSyDashboardsDESCRIPTION: TStringField;
    cdSyDashboardsSY_DASHBOARDS_NBR: TIntegerField;
    procedure btnAddDashboardsClick(Sender: TObject);
    procedure btnDelDashbordsClick(Sender: TObject);
  private
    FSBAnalytics: boolean;
    procedure CreatecdSyDashboards;
  protected
    function GetIfReadOnly: Boolean; override;
    function GetDefaultDataSet: TevClientDataSet; override;
  public
    { Public declarations }
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure AfterClick( Kind: Integer ); override;
  end;

implementation

{$R *.DFM}

function TEDIT_SB_ENABLED_DASHBOARDS.GetIfReadOnly: Boolean;
begin
  Result := inherited GetIfReadOnly;

  if (not Result) then
    result:= FSBAnalytics;
end;

procedure TEDIT_SB_ENABLED_DASHBOARDS.AfterClick(Kind: Integer);
begin
  inherited;
  if Kind in [NavAbort] then
  begin
   DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.DisableControls;
   cdSyDashboards.DisableControls;
   try
     DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.DataRequired('ALL');
     CreatecdSyDashboards;
   finally
     DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.First;
     cdSyDashboards.First;
     DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.EnableControls;
     cdSyDashboards.EnableControls;
   end;
  end;
end;

procedure TEDIT_SB_ENABLED_DASHBOARDS.CreatecdSyDashboards;
  procedure RemoveSelectedDashboards;
  begin
       DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.First;
       while not DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Eof do
       begin
        if (DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS['DASHBOARD_LEVEL'] = LEVEL_SYSTEM)  and
           cdSyDashboards.Locate('SY_DASHBOARDS_NBR', DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS['DASHBOARD_NBR'],[]) then
             cdSyDashboards.Delete;
        DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Next;
       end;
  end;
var
 s : String;

begin
     s := 'select SY_DASHBOARDS_NBR, DESCRIPTION from  SY_DASHBOARDS where {AsOfNow<SY_DASHBOARDS>} and RELEASED = ''Y''';
     with TExecDSWrapper.Create(s) do
     begin
       cdSyDashboards.Close;
       ctx_DataAccess.GetSyCustomData(cdSyDashboards, AsVariant);
     end;
     cdSyDashboards.LogChanges := False;

     RemoveSelectedDashboards;
end;

procedure TEDIT_SB_ENABLED_DASHBOARDS.Activate;
begin
  inherited;
  CreatecdSyDashboards;

end;

function TEDIT_SB_ENABLED_DASHBOARDS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS;
end;

procedure TEDIT_SB_ENABLED_DASHBOARDS.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_DASHBOARDS, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_SYSTEM_MISC.SY_ANALYTICS_TIER, SupportDataSets, 'ALL');

  FSBAnalytics:= GetSBAnalyticsLicense;
end;

procedure TEDIT_SB_ENABLED_DASHBOARDS.btnAddDashboardsClick(
  Sender: TObject);
var
 i : integer;
begin
  inherited;

  DM_SYSTEM_MISC.SY_DASHBOARDS.DisableControls;
  try
    if grdSyDashboards.SelectedList.Count > 0 then
    begin
      for i := grdSyDashboards.SelectedList.Count-1 downto 0 do
      begin
        grdSyDashboards.DataSource.DataSet.GotoBookmark(grdSyDashboards.SelectedList[i]);

        if not DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Locate('DASHBOARD_LEVEL;DASHBOARD_NBR',
                       VarArrayOf([ LEVEL_SYSTEM, cdSyDashboards.FieldByName('SY_DASHBOARDS_NBR').AsVariant]),[]) then
        begin
          DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Insert;
          DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.FieldByName('DASHBOARD_LEVEL').AsString := LEVEL_SYSTEM;
          DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.FieldByName('DASHBOARD_NBR').AsInteger := cdSyDashboards.FieldByName('SY_DASHBOARDS_NBR').AsInteger;
          DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Post;
          cdSyDashboards.Delete;
        end;
      end;
    end;
   finally
     DM_SYSTEM_MISC.SY_DASHBOARDS.EnableControls;
     grdSyDashboards.UnselectAll;
   end;

end;

procedure TEDIT_SB_ENABLED_DASHBOARDS.btnDelDashbordsClick(
  Sender: TObject);
var
 i : integer;
begin
  inherited;

   DM_SERVICE_BUREAU.SB_DASHBOARDS.DisableControls;
   try
     if grdSbEnanledDashboards.SelectedList.Count > 0 then
     begin
        for i := grdSbEnanledDashboards.SelectedList.Count-1 downto 0 do
        begin
          grdSbEnanledDashboards.DataSource.DataSet.GotoBookmark(grdSbEnanledDashboards.SelectedList[i]);

          if DM_SYSTEM_MISC.SY_DASHBOARDS.Locate('SY_DASHBOARDS_NBR', DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.FieldByName('DASHBOARD_NBR').asString,[]) then
          begin
           cdSyDashboards.Insert;
           cdSyDashboards.FieldByName('SY_DASHBOARDS_NBR').AsInteger := DM_SYSTEM_MISC.SY_DASHBOARDS.FieldByName('SY_DASHBOARDS_NBR').asInteger;
           cdSyDashboards.FieldByName('DESCRIPTION').AsString := DM_SYSTEM_MISC.SY_DASHBOARDS.FieldByName('DESCRIPTION').asString;
           cdSyDashboards.Post;
          end;

          DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.Delete;
        end;
     end;
   finally
     DM_SERVICE_BUREAU.SB_ENABLED_DASHBOARDS.EnableControls;
     grdSbEnanledDashboards.UnselectAll;
   end;

end;

initialization
  RegisterClass(TEDIT_SB_ENABLED_DASHBOARDS);

end.
