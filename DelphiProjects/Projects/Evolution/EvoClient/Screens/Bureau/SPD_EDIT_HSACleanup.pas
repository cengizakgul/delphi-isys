// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_HSACleanup;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, wwdblook,  SDataStructure, ComCtrls, ExtCtrls,
  SDDClasses, SDataDictsystem, ISBasicClasses, EvUIUtils, EvUIComponents,
  SDataDictclient, isUIwwDBLookupCombo, LMDCustomButton, LMDButton,
  isUILMDButton, isUIEdit, isUIFashionPanel;

type
  TEDIT_HSACleanup = class(TForm)
    DM_CLIENT: TDM_CLIENT;
    fpShiftSummary: TisUIFashionPanel;
    evLabel5: TevLabel;
    evLabel3: TevLabel;
    evLabel1: TevLabel;
    evLabel2: TevLabel;
    evLabel4: TevLabel;
    bOk: TevButton;
    edtMultEDCode: TevDBLookupCombo;
    edtEEContEDCode: TevDBLookupCombo;
    edtEECathEDCode: TevDBLookupCombo;
    edtERContEDCode: TevDBLookupCombo;
    edtOptERContEDCode: TevDBLookupCombo;

    procedure bOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses EvUtils;

{$R *.DFM}

procedure TEDIT_HSACleanup.bOkClick(Sender: TObject);
begin
    ModalResult := mrOk;
end;

end.
