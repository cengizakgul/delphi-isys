// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_REFERRALS;

interface

uses SFrameEntry,  StdCtrls, Mask, wwdbedit, Controls, Grids,
  Wwdbigrd, Wwdbgrid, Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet;

type
  TEDIT_SB_REFERRALS = class(TFrameEntry)
    wwDBGrid1: TevDBGrid;
    dedtReferral: TevDBEdit;
    lablName: TevLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDefaultDataSet: TevClientDataSet; override;
    function GetInsertControl: TWinControl; override;
  end;

var
  EDIT_SB_REFERRALS: TEDIT_SB_REFERRALS;

implementation

uses SDataStructure;

{$R *.DFM}

function TEDIT_SB_REFERRALS.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_REFERRALS;
end;

function TEDIT_SB_REFERRALS.GetInsertControl: TWinControl;
begin
  Result := dedtReferral;
end;

initialization
  RegisterClass(TEDIT_SB_REFERRALS);

end.
