object EDIT_SB_EE_DBDT: TEDIT_SB_EE_DBDT
  Left = 228
  Top = 174
  Width = 570
  Height = 418
  Caption = 'Change EE Active Status'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object evPanel1: TevPanel
    Left = 0
    Top = 0
    Width = 554
    Height = 380
    Align = alClient
    Caption = 'evPanel1'
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 19
      Width = 59
      Height = 13
      Caption = 'DBDT Level'
    end
    object Label1: TLabel
      Left = 304
      Top = 328
      Width = 81
      Height = 13
      Caption = 'Termination Date'
    end
    object evcbMoveCheck: TevDBComboBox
      Left = 96
      Top = 11
      Width = 170
      Height = 21
      ShowButton = True
      Style = csDropDownList
      MapList = False
      AllowClearKey = False
      AutoDropDown = True
      DropDownCount = 8
      ItemHeight = 0
      Items.Strings = (
        'Division'
        'Branch'
        'Department'
        'Team')
      ItemIndex = 3
      Picture.PictureMaskFromDataSet = False
      Sorted = False
      TabOrder = 0
      UnboundDataType = wwDefault
      OnChange = evcbMoveCheckChange
    end
    object evgrDBDT: TevDBGrid
      Left = 9
      Top = 38
      Width = 544
      Height = 266
      DisableThemesInTitle = False
      Selected.Strings = (
        'DNAME'#9'20'#9'DIVISION NAME '#9'F'
        'BNAME'#9'20'#9'BRANCH NAME'#9'F'
        'PNAME'#9'20'#9'DEPARTMENT NAME'#9'F'
        'TNAME'#9'20'#9'TEAM NAME'#9'F'
        'DIVISION_NBR'#9'10'#9'Division Nbr'#9'F'
        'BRANCH_NBR'#9'10'#9'Branch Nbr'#9'F'
        'DEPARTMENT_NBR'#9'10'#9'Department Nbr'#9'F'
        'TEAM_NBR'#9'10'#9'Team Nbr'#9'F')
      IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
      IniAttributes.SectionName = 'TEDIT_SB_EE_DBDT\evgrDBDT'
      IniAttributes.Delimiter = ';;'
      ExportOptions.ExportType = wwgetSYLK
      ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      DataSource = wwdsEEDBDT
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgMultiSelect, dgTrailingEllipsis, dgDblClickColSizing]
      TabOrder = 1
      TitleAlignment = taLeftJustify
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 1
      PaintOptions.AlternatingRowColor = clCream
    end
    object evrgCreditHold: TevRadioGroup
      Left = 8
      Top = 305
      Width = 281
      Height = 41
      Columns = 2
      Items.Strings = (
        'Set Active'
        'Set Term')
      TabOrder = 2
    end
    object btnExit: TButton
      Left = 8
      Top = 350
      Width = 75
      Height = 25
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 3
      OnClick = btnExitClick
    end
    object btnApply: TButton
      Left = 192
      Top = 350
      Width = 75
      Height = 25
      Caption = 'Done'
      ModalResult = 1
      TabOrder = 4
      OnClick = btnApplyClick
    end
    object evTermDate: TevDateTimePicker
      Left = 413
      Top = 320
      Width = 121
      Height = 21
      Date = 39413.000000000000000000
      Time = 39413.000000000000000000
      TabOrder = 5
    end
  end
  object wwcsEEDBDT: TevClientDataSet
    FieldDefs = <
      item
        Name = 'CUSTOM_DIVISION_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_BRANCH_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_DEPARTMENT_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CUSTOM_TEAM_NUMBER'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DIVISION_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DEPARTMENT_NBR'
        DataType = ftInteger
      end
      item
        Name = 'BRANCH_NBR'
        DataType = ftInteger
      end
      item
        Name = 'TEAM_NBR'
        DataType = ftInteger
      end
      item
        Name = 'DNAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'BNAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PNAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'TNAME'
        DataType = ftString
        Size = 20
      end>
    Left = 336
    Top = 32
  end
  object wwdsEEDBDT: TevDataSource
    DataSet = wwcsEEDBDT
    Left = 376
    Top = 32
  end
end
