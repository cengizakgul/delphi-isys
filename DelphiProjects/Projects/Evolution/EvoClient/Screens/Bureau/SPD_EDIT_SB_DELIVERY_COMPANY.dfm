inherited EDIT_SB_DELIVERY_COMPANY: TEDIT_SB_DELIVERY_COMPANY
  Width = 729
  Height = 616
  object Label1: TevLabel [0]
    Left = 16
    Top = 8
    Width = 88
    Height = 13
    Caption = 'Delivery Company:'
  end
  object DBText1: TevDBText [1]
    Left = 112
    Top = 8
    Width = 65
    Height = 17
    DataField = 'NAME'
    DataSource = wwdsDetail
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object PageControl1: TevPageControl [2]
    Left = 8
    Top = 32
    Width = 593
    Height = 433
    HelpContext = 36003
    ActivePage = tshtDelivery_Company_Detail
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Browse'
      object wwDBGrid1: TevDBGrid
        Left = 0
        Top = 0
        Width = 281
        Height = 404
        DisableThemesInTitle = False
        Selected.Strings = (
          'NAME'#9'40'#9'Name')
        IniAttributes.FileName = 'SOFTWARE\Evolution\delphi32\\Misc\'
        IniAttributes.SectionName = 'TEDIT_SB_DELIVERY_COMPANY\wwDBGrid1'
        IniAttributes.Delimiter = ';;'
        ExportOptions.ExportType = wwgetSYLK
        ExportOptions.Options = [esoShowHeader, esoDblQuoteFields, esoClipboard]
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = True
        DataSource = wwdsDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgDblClickColSizing]
        TabOrder = 0
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        PaintOptions.AlternatingRowColor = 14544093
        PaintOptions.ActiveRecordColor = clBlack
        NoFire = False
      end
    end
    object tshtDelivery_Company_Detail: TTabSheet
      Caption = 'Details'
      object bevSB_DELIVERY1: TEvBevel
        Left = 0
        Top = 0
        Width = 585
        Height = 405
        Align = alClient
      end
      object lablName: TevLabel
        Left = 16
        Top = 11
        Width = 37
        Height = 16
        Caption = '~Name'
        FocusControl = dedtName
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lablDelivery_Contact: TevLabel
        Left = 16
        Top = 59
        Width = 78
        Height = 13
        Caption = 'Delivery Contact'
        FocusControl = dedtDelivery_Contact
      end
      object lablPhone: TevLabel
        Left = 16
        Top = 107
        Width = 31
        Height = 13
        Caption = 'Phone'
        FocusControl = wwdePhone
      end
      object lablPhone_Type: TevLabel
        Left = 152
        Top = 107
        Width = 58
        Height = 13
        Caption = 'Phone Type'
        FocusControl = wwcbPhone_Type
      end
      object lablSupplies_Contact: TevLabel
        Left = 312
        Top = 59
        Width = 80
        Height = 13
        Caption = 'Supplies Contact'
        FocusControl = dedtSupplies_Contact
      end
      object lablPhone2: TevLabel
        Left = 312
        Top = 107
        Width = 31
        Height = 13
        Caption = 'Phone'
        FocusControl = wwdePhone2
      end
      object lablPhone_Type2: TevLabel
        Left = 448
        Top = 107
        Width = 58
        Height = 13
        Caption = 'Phone Type'
        FocusControl = wwcbPhone_Type2
      end
      object lablWeb_Site: TevLabel
        Left = 312
        Top = 11
        Width = 44
        Height = 13
        Caption = 'Web Site'
        FocusControl = dedtWeb_Site
      end
      object lablNotes: TevLabel
        Left = 16
        Top = 187
        Width = 28
        Height = 13
        Caption = 'Notes'
      end
      object bevSB_DELIVERY2: TEvBevel
        Left = 16
        Top = 163
        Width = 553
        Height = 6
        Style = bsRaised
      end
      object dedtName: TevDBEdit
        Left = 16
        Top = 27
        Width = 257
        Height = 21
        HelpContext = 36003
        DataField = 'NAME'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtDelivery_Contact: TevDBEdit
        Left = 16
        Top = 75
        Width = 257
        Height = 21
        HelpContext = 36003
        DataField = 'DELIVERY_CONTACT'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtSupplies_Contact: TevDBEdit
        Left = 312
        Top = 75
        Width = 257
        Height = 21
        HelpContext = 36003
        DataField = 'SUPPLIES_CONTACT'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object dedtWeb_Site: TevDBEdit
        Left = 312
        Top = 27
        Width = 257
        Height = 21
        HelpContext = 36003
        DataField = 'WEB_SITE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '*{&*?[{ ,*'#39'*~}],@}'
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwcbPhone_Type: TevDBComboBox
        Left = 152
        Top = 123
        Width = 121
        Height = 21
        HelpContext = 36003
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = True
        AutoDropDown = True
        DataField = 'DELIVERY_CONTACT_PHONE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 3
        UnboundDataType = wwDefault
      end
      object wwcbPhone_Type2: TevDBComboBox
        Left = 448
        Top = 123
        Width = 121
        Height = 21
        HelpContext = 36003
        ShowButton = True
        Style = csDropDownList
        MapList = False
        AllowClearKey = True
        AutoDropDown = True
        DataField = 'SUPPLIES_CONTACT_PHONE_TYPE'
        DataSource = wwdsDetail
        DropDownCount = 8
        ItemHeight = 0
        Picture.PictureMaskFromDataSet = False
        Sorted = False
        TabOrder = 7
        UnboundDataType = wwDefault
      end
      object dmemNotes: TEvDBMemo
        Left = 16
        Top = 203
        Width = 553
        Height = 169
        HelpContext = 36003
        DataField = 'DELIVERY_COMPANY_NOTES'
        DataSource = wwdsDetail
        TabOrder = 8
      end
      object wwdePhone: TevDBEdit
        Left = 16
        Top = 123
        Width = 123
        Height = 21
        HelpContext = 36003
        DataField = 'DELIVERY_CONTACT_PHONE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
      object wwdePhone2: TevDBEdit
        Left = 312
        Top = 123
        Width = 124
        Height = 21
        HelpContext = 36003
        DataField = 'SUPPLIES_CONTACT_PHONE'
        DataSource = wwdsDetail
        Picture.PictureMaskFromDataSet = False
        Picture.PictureMask = '{(*3{#}) ,*3{#}-, }*3{#}-*4{#}'
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
        Glowing = False
      end
    end
  end
  inherited wwdsMaster: TevDataSource
    Left = 149
    Top = 34
  end
  inherited wwdsDetail: TevDataSource
    MasterDataSource = nil
  end
  inherited wwdsList: TevDataSource
    Top = 26
  end
end
