// Copyright � 2000-2004 iSystems LLC. All rights reserved.
unit SPD_EDIT_SB_VENDOR;

interface

uses
  SFrameEntry,  SDataStructure, DBCtrls, StdCtrls, Mask,
  wwdbedit, Wwdotdot, ExtCtrls, Controls, Grids, Wwdbigrd, Wwdbgrid,
  Classes, Db, Wwdatsrc, EvUIComponents, EvClientDataSet, SDDClasses,
  SDataDictsystem, isUIwwDBEdit, ISBasicClasses, SDataDictbureau, EvUtils,
  Forms, SPD_MiniNavigationFrame, LMDCustomButton, LMDButton, isUILMDButton, Variants,
  EvConsts, SPackageEntry, kbmMemTable, ISKbmMemDataSet,
  ISDataAccessComponents, EvDataAccessComponents, EvContext, EvCommonInterfaces,
  SysUtils, EvDataset, EvExceptions;

type
  TEDIT_SB_VENDOR = class(TFrameEntry)
    grdSbVendor: TevDBGrid;
    DM_SYSTEM_MISC: TDM_SYSTEM_MISC;
    DM_SERVICE_BUREAU: TDM_SERVICE_BUREAU;
    dsSyVendor: TevDataSource;
    dsSbCustomVendor: TevDataSource;
    ebSyVendorSelect: TevBitBtn;
    evBtnSyVendorDelete: TevBitBtn;
    ebSbVendorSelect: TevBitBtn;
    grdSyVendor: TevDBGrid;
    grdSbCustomVendor: TevDBGrid;
    dsSBVendor: TevDataSource;
    cdSyVendor: TevClientDataSet;
    cdSbCustomVendor: TevClientDataSet;
    cdSyVendorSY_VENDORS_NBR: TIntegerField;
    cdSyVendorVENDOR_NAME: TStringField;
    procedure ebSyVendorSelectClick(Sender: TObject);
    procedure evBtnSyVendorDeleteClick(Sender: TObject);
    procedure ebSbVendorSelectClick(Sender: TObject);
  protected
    function GetDefaultDataSet: TevClientDataSet; override;
    { Public declarations }
  public
    procedure Activate; override;
    procedure RetrieveDataSets(var NavigationDataSet: TevClientDataSet;
                               var InsertDataSets, EditDataSets: TArrayDS;
                               var DeleteDataSet: TevClientDataSet;
                               var SupportDataSets: TArrayDS); override;
    procedure OnSetButton(Kind: Integer; var Value: Boolean); override;
  end;

implementation

{$R *.DFM}

function TEDIT_SB_VENDOR.GetDefaultDataSet: TevClientDataSet;
begin
  Result := DM_SERVICE_BUREAU.SB_VENDOR;
end;

procedure TEDIT_SB_VENDOR.Activate;

  procedure RemoveSelectedVendors;
  begin
       DM_SERVICE_BUREAU.SB_VENDOR.First;
       while not DM_SERVICE_BUREAU.SB_VENDOR.Eof do
       begin
        if DM_SERVICE_BUREAU.SB_VENDOR['VENDORS_LEVEL'] = LEVEL_BUREAU then
        begin
          if cdSbCustomVendor.Locate('SB_CUSTOM_VENDORS_NBR', DM_SERVICE_BUREAU.SB_VENDOR['VENDOR_NBR'],[]) then
             cdSbCustomVendor.Delete
        end
        else
        begin
          if cdSyVendor.Locate('SY_VENDORS_NBR', DM_SERVICE_BUREAU.SB_VENDOR['VENDOR_NBR'],[]) then
             cdSyVendor.Delete
        end;
        DM_SERVICE_BUREAU.SB_VENDOR.Next;
       end;
  end;

var
 s : String;
begin
  inherited;
     s := 'select SY_VENDORS_NBR, VENDOR_NAME from  SY_VENDORS where {AsOfNow<SY_VENDORS>} ';
     with TExecDSWrapper.Create(s) do
     begin
       cdSyVendor.Close;
       ctx_DataAccess.GetSyCustomData(cdSyVendor, AsVariant);
     end;
     cdSyVendor.LogChanges := False;

     s := 'select SB_CUSTOM_VENDORS_NBR, VENDOR_NAME from SB_CUSTOM_VENDORS where {AsOfNow<SB_CUSTOM_VENDORS>} ';
     with TExecDSWrapper.Create(s) do
     begin
       cdSbCustomVendor.Close;
       ctx_DataAccess.GetSbCustomData(cdSbCustomVendor, AsVariant);
     end;
     cdSbCustomVendor.LogChanges := False;

     RemoveSelectedVendors;

end;


procedure TEDIT_SB_VENDOR.RetrieveDataSets(
  var NavigationDataSet: TevClientDataSet; var InsertDataSets,
  EditDataSets: TArrayDS; var DeleteDataSet: TevClientDataSet;
  var SupportDataSets: TArrayDS);
begin
  inherited;
  AddDSWithCheck(DM_SYSTEM_MISC.SY_VENDORS, SupportDataSets, 'ALL');
  AddDSWithCheck(DM_SERVICE_BUREAU.SB_CUSTOM_VENDORS, SupportDataSets, 'ALL');
end;

procedure TEDIT_SB_VENDOR.OnSetButton(Kind: Integer;
  var Value: Boolean);
begin
  inherited;

 (Owner as TFramePackageTmpl).btnNavInsert.Enabled := false;
 (Owner as TFramePackageTmpl).btnNavDelete.Enabled := false;

end;



procedure TEDIT_SB_VENDOR.ebSyVendorSelectClick(Sender: TObject);
var
 i : integer;
begin
  inherited;

  cdSyVendor.DisableControls;
  try
    if grdSyVendor.SelectedList.Count > 0 then
    begin
      for i := grdSyVendor.SelectedList.Count-1 downto 0 do
      begin
        grdSyVendor.DataSource.DataSet.GotoBookmark(grdSyVendor.SelectedList[i]);

        if not DM_SERVICE_BUREAU.SB_VENDOR.Locate('VENDORS_LEVEL;VENDOR_NBR',
                       VarArrayOf([ LEVEL_SYSTEM, grdSyVendor.DataSource.DataSet['SY_VENDORS_NBR']]),[]) then
        begin
          DM_SERVICE_BUREAU.SB_VENDOR.Insert;
          DM_SERVICE_BUREAU.SB_VENDOR.FieldByName('VENDORS_LEVEL').AsString := LEVEL_SYSTEM;
          DM_SERVICE_BUREAU.SB_VENDOR.FieldByName('VENDOR_NBR').AsInteger := grdSyVendor.DataSource.DataSet['SY_VENDORS_NBR'];
          DM_SERVICE_BUREAU.SB_VENDOR.Post;
          cdSyVendor.Delete;
        end;
      end;
    end;
   finally
     cdSyVendor.EnableControls;
     grdSyVendor.UnselectAll;
   end;
end;

procedure TEDIT_SB_VENDOR.ebSbVendorSelectClick(Sender: TObject);
var
 i : integer;
begin
  inherited;

  cdSbCustomVendor.DisableControls;
  try
    if grdSbCustomVendor.SelectedList.Count > 0 then
    begin
      for i := grdSbCustomVendor.SelectedList.Count-1 downto 0 do
      begin
        grdSbCustomVendor.DataSource.DataSet.GotoBookmark(grdSbCustomVendor.SelectedList[i]);

        if not DM_SERVICE_BUREAU.SB_VENDOR.Locate('VENDORS_LEVEL;VENDOR_NBR',
                       VarArrayOf([ LEVEL_BUREAU, grdSbCustomVendor.DataSource.DataSet['SB_CUSTOM_VENDORS_NBR']]),[]) then
        begin
          DM_SERVICE_BUREAU.SB_VENDOR.Insert;
          DM_SERVICE_BUREAU.SB_VENDOR.FieldByName('VENDORS_LEVEL').AsString := LEVEL_BUREAU;
          DM_SERVICE_BUREAU.SB_VENDOR.FieldByName('VENDOR_NBR').AsInteger := grdSbCustomVendor.DataSource.DataSet['SB_CUSTOM_VENDORS_NBR'];
          DM_SERVICE_BUREAU.SB_VENDOR.Post;
          cdSbCustomVendor.Delete;
        end;
      end;
    end;
   finally
     cdSbCustomVendor.EnableControls;
     grdSbCustomVendor.UnselectAll;
   end;
end;

procedure TEDIT_SB_VENDOR.evBtnSyVendorDeleteClick(Sender: TObject);

  function CheckSbVendorInUSe:boolean;
   var
     s: string;
     Q: IEvQuery;
  begin
    s:= 'select count(SB_VENDOR_DETAIL_NBR) from SB_VENDOR_DETAIL ' +
            'where {AsOfNow<SB_VENDOR_DETAIL>} and SB_VENDOR_NBR = ' +
               IntToStr(DM_SERVICE_BUREAU.SB_VENDOR.FieldByName('SB_VENDOR_NBR').AsInteger);;
    Q := TevQuery.Create(s);
    Q.Execute;
    result := Q.Result.Fields[0].AsInteger > 0;
  end;

var
 i : integer;
begin
  inherited;

   DM_SERVICE_BUREAU.SB_VENDOR.DisableControls;
   try
     if grdSbVendor.SelectedList.Count > 0 then
     begin
        for i := grdSbVendor.SelectedList.Count-1 downto 0 do
        begin
          grdSbVendor.DataSource.DataSet.GotoBookmark(grdSbVendor.SelectedList[i]);

          if DM_SERVICE_BUREAU.SB_VENDOR.Locate('VENDORS_LEVEL;VENDOR_NBR',
                       VarArrayOf([grdSbVendor.DataSource.DataSet['VENDORS_LEVEL'], grdSbVendor.DataSource.DataSet['VENDOR_NBR']]),[]) then
          begin
            if CheckSbVendorInUSe then
               raise EUpdateError.CreateHelp('Please delete all the Details.', IDH_ConsistencyViolation);

            if grdSbVendor.DataSource.DataSet['VENDORS_LEVEL'] = LEVEL_BUREAU then
            begin
             cdSbCustomVendor.Insert;
             cdSbCustomVendor.FieldByName('VENDOR_NAME').AsString := DM_SERVICE_BUREAU.SB_VENDOR.FieldByName('NAME').asString;
             cdSbCustomVendor.FieldByName('SB_CUSTOM_VENDORS_NBR').AsInteger := DM_SERVICE_BUREAU.SB_VENDOR.FieldByName('VENDOR_NBR').asInteger;
             cdSbCustomVendor.Post;
            end
            else
            begin
             cdSyVendor.Insert;
             cdSyVendor.FieldByName('VENDOR_NAME').AsString := DM_SERVICE_BUREAU.SB_VENDOR.FieldByName('NAME').asString;
             cdSyVendor.FieldByName('SY_VENDORS_NBR').AsInteger := DM_SERVICE_BUREAU.SB_VENDOR.FieldByName('VENDOR_NBR').asInteger;
             cdSyVendor.Post;
            end;

            DM_SERVICE_BUREAU.SB_VENDOR.Delete;
          end;
        end;
     end;
   finally
     DM_SERVICE_BUREAU.SB_VENDOR.EnableControls;
     grdSbVendor.UnselectAll;
   end;
end;

initialization
  RegisterClass(TEDIT_SB_VENDOR);

end.
